﻿// Decompiled with JetBrains decompiler
// Type: RebateByRechargeRewards
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class RebateByRechargeRewards
{
  public List<RebateByRechargeRewards.RewardItemInfo> rewardItemInfoList = new List<RebateByRechargeRewards.RewardItemInfo>();
  private int targetRechargeAmount;
  private bool rebated;
  private int step;

  public int TargetRechargeAmount
  {
    get
    {
      return this.targetRechargeAmount;
    }
    set
    {
      this.targetRechargeAmount = value;
    }
  }

  public bool Rebated
  {
    get
    {
      return this.rebated;
    }
    set
    {
      this.rebated = value;
    }
  }

  public int Step
  {
    get
    {
      return this.step;
    }
    set
    {
      this.step = value;
    }
  }

  public class RewardItemInfo
  {
    public string rewardItemId;
    public int rewardItemCount;
    public bool hasSpecialEffect;
  }
}
