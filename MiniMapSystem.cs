﻿// Decompiled with JetBrains decompiler
// Type: MiniMapSystem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MiniMapSystem : MonoBehaviour
{
  public static readonly Vector2 Inner = new Vector2(400f, 400f);
  public static readonly Vector2 Outter = new Vector2(650f, 500f);
  private GameObjectPool m_Pool = new GameObjectPool();
  private List<MiniMapController> m_Controllers = new List<MiniMapController>();
  public const string MINI_MAP_PATH = "Prefab/Kingdom/MiniMap/";
  public MiniMapCamera TheCamera;
  public MiniMapLandMark Yourself;
  public MiniMapLandMark Leader;
  public MiniMapLandMark Member;
  public MiniMapLandMark Fortress;
  public MiniMapLandMark Enemy;
  public MiniMapLandMark EnemyFortress;
  private static MiniMapSystem m_Instance;
  private bool m_ShowResource;
  private bool m_ShowAlliance;
  private System.Action<bool> m_ResourceVisiblityChanged;
  private System.Action<bool> m_AllianceVisiblityChanged;
  private System.Action<bool> m_AllianceWarVisiblityChanged;

  private void Awake()
  {
    MiniMapSystem.m_Instance = this;
  }

  public static MiniMapSystem Instance
  {
    get
    {
      return MiniMapSystem.m_Instance;
    }
  }

  public static void Init()
  {
    UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad("Prefab/Kingdom/MiniMap/MiniMapSystem", (System.Type) null) as GameObject);
    if (WorldMapData.Instance.Wonderlist == null || WorldMapData.Instance.Wonderlist.Count != 0)
      return;
    WorldMapData.Instance.Initialize();
  }

  public void Open()
  {
    if ((UnityEngine.Object) UIManager.inst != (UnityEngine.Object) null)
      UIManager.inst.publicHUD.SwitchToPart(GAME_LOCATION.MINI_MAP);
    this.m_Pool.Initialize(AssetManager.Instance.HandyLoad("Prefab/Kingdom/MiniMap/MiniMapKingdom", (System.Type) null) as GameObject, this.gameObject);
    Coordinate location = PVPSystem.Instance.Map.CurrentKXY;
    if (!PVPMapData.MapData.IsKingdomActive(location.K))
    {
      Vector3 pixelPosition = PVPMapData.MapData.ConvertTileKXYToPixelPosition(location);
      if (location.X == 0)
        pixelPosition.x -= PVPMapData.MapData.PixelsPerQuarterTile.x;
      if (location.Y == 0)
        pixelPosition.y += PVPMapData.MapData.PixelsPerQuarterTile.y;
      location = PVPMapData.MapData.ConvertPixelPositionToTileKXY(pixelPosition);
      PVPSystem.Instance.Map.CurrentKXY = location;
    }
    this.LoadKingdom(location.K, (System.Action) (() =>
    {
      this.TheCamera.OnClicked = new System.Action<Coordinate>(this.OnMiniCameraClicked);
      this.TheCamera.MoveTo(location.X, location.Y);
    }));
  }

  public void Close()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    MiniMapSystem.m_Instance = (MiniMapSystem) null;
    this.Clear9Slices();
  }

  public void Show()
  {
    if ((UnityEngine.Object) UIManager.inst != (UnityEngine.Object) null)
      UIManager.inst.publicHUD.SwitchToPart(GAME_LOCATION.MINI_MAP);
    this.gameObject.SetActive(true);
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  public void ShowResources()
  {
    this.m_ShowResource = true;
    this.PublishResourceVisibilityChangedEvent(this.m_ShowResource);
  }

  public void HideResources()
  {
    this.m_ShowResource = false;
    this.PublishResourceVisibilityChangedEvent(this.m_ShowResource);
  }

  public void ShowAlliance()
  {
    this.m_ShowAlliance = true;
    this.PublishAllianceVisibilityChangedEvent(this.m_ShowAlliance);
  }

  public void HideAlliance()
  {
    this.m_ShowAlliance = false;
    this.PublishAllianceVisibilityChangedEvent(this.m_ShowAlliance);
  }

  private void PublishResourceVisibilityChangedEvent(bool visible)
  {
    if (this.m_ResourceVisiblityChanged == null)
      return;
    this.m_ResourceVisiblityChanged(this.m_ShowResource);
  }

  private void PublishAllianceVisibilityChangedEvent(bool visible)
  {
    if (this.m_AllianceVisiblityChanged == null)
      return;
    this.m_AllianceVisiblityChanged(this.m_ShowAlliance);
  }

  public void ShowAllianceWarEnemy()
  {
    this.PublishAllianceWarVisibilityChangedEvent(true);
  }

  public void HideAllianceWarEnemy()
  {
    this.PublishAllianceWarVisibilityChangedEvent(false);
  }

  private void PublishAllianceWarVisibilityChangedEvent(bool visible)
  {
    if (this.m_AllianceWarVisiblityChanged == null)
      return;
    this.m_AllianceWarVisiblityChanged(visible);
  }

  public void OnMiniCameraClicked(Coordinate coordinate)
  {
    this.Close();
    PVPSystem.Instance.Show(true);
    UIManager.inst.publicHUD.SwitchToPart(GAME_LOCATION.KINGDOM);
    if (!PVPSystem.Instance.IsAvailable)
      return;
    PVPSystem.Instance.Map.CurrentKXY = coordinate;
    PVPSystem.Instance.Map.GotoLocation(coordinate, false);
  }

  private void LoadKingdom(int kingdomID, System.Action callback)
  {
    ActiveKingdom center = PVPMapData.MapData.FindKingdomByID(kingdomID);
    ActiveKingdom left = PVPMapData.MapData.FindKingdomByLocation(center.KingdomX - 1, center.KingdomY);
    ActiveKingdom right = PVPMapData.MapData.FindKingdomByLocation(center.KingdomX + 1, center.KingdomY);
    ActiveKingdom top = PVPMapData.MapData.FindKingdomByLocation(center.KingdomX, center.KingdomY + 1);
    ActiveKingdom bottom = PVPMapData.MapData.FindKingdomByLocation(center.KingdomX, center.KingdomY - 1);
    ActiveKingdom[] activeKingdomArray = new ActiveKingdom[5]
    {
      center,
      left,
      right,
      top,
      bottom
    };
    ArrayList arrayList = new ArrayList();
    for (int index = 0; index < activeKingdomArray.Length; ++index)
    {
      if (activeKingdomArray[index] != null)
        arrayList.Add((object) activeKingdomArray[index].WorldID);
    }
    MiniMapController C = this.Load9Slices(center.KingdomX, center.KingdomY);
    this.m_AllianceWarVisiblityChanged = new System.Action<bool>(C.OnAllianceWarVisiblityChanged);
    this.PublishAllianceWarVisibilityChangedEvent(false);
    Hashtable postData = new Hashtable();
    postData[(object) "world_ids"] = (object) arrayList;
    MessageHub.inst.GetPortByAction("wonder:loadWonderMapData").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.m_ResourceVisiblityChanged = new System.Action<bool>(C.OnResourceVisiblityChanged);
      this.m_AllianceVisiblityChanged = new System.Action<bool>(C.OnAllianceVisiblityChanged);
      this.PublishResourceVisibilityChangedEvent(this.m_ShowResource);
      this.PublishAllianceVisibilityChangedEvent(this.m_ShowAlliance);
      C.Activate(center.WorldID == PlayerData.inst.playerCityData.cityLocation.K, center.WorldID == PVPSystem.Instance.Map.CurrentKXY.K);
      C.UpdateArrow(left, right, bottom, top);
      this.TheCamera.Center = C;
      this.TheCamera.Kingdom = center;
      ActiveKingdom kingdomById = PVPMapData.MapData.FindKingdomByID(PlayerData.inst.playerCityData.cityLocation.K);
      for (int index = 0; index < this.m_Controllers.Count; ++index)
        this.m_Controllers[index].MarkMyKingdom(kingdomById);
      if (callback != null)
        callback();
      Hashtable hashtable = data as Hashtable;
      if (hashtable == null)
        return;
      WonderFlagManager.Instance.Parse((object) hashtable);
    }), true);
  }

  public void SwitchKingdom(ActiveKingdom kingdom)
  {
    int x = kingdom.KingdomX - this.TheCamera.Kingdom.KingdomX;
    int y = kingdom.KingdomY - this.TheCamera.Kingdom.KingdomY;
    this.LoadKingdom(kingdom.WorldID, (System.Action) (() => this.TheCamera.SwitchKingdom(MiniMapController.ConvertKXYToWorldPoint(kingdom.KingdomX, kingdom.KingdomY, 640 - 300 * x, 1280 + 850 * y), (System.Action) null)));
  }

  private MiniMapController Load9Slices(int KingdomX, int KingdomY)
  {
    this.Clear9Slices();
    MiniMapController component1 = this.m_Pool.AddChild(this.gameObject).GetComponent<MiniMapController>();
    MiniMapController component2 = this.m_Pool.AddChild(this.gameObject).GetComponent<MiniMapController>();
    MiniMapController component3 = this.m_Pool.AddChild(this.gameObject).GetComponent<MiniMapController>();
    MiniMapController component4 = this.m_Pool.AddChild(this.gameObject).GetComponent<MiniMapController>();
    MiniMapController component5 = this.m_Pool.AddChild(this.gameObject).GetComponent<MiniMapController>();
    MiniMapController component6 = this.m_Pool.AddChild(this.gameObject).GetComponent<MiniMapController>();
    MiniMapController component7 = this.m_Pool.AddChild(this.gameObject).GetComponent<MiniMapController>();
    MiniMapController component8 = this.m_Pool.AddChild(this.gameObject).GetComponent<MiniMapController>();
    MiniMapController component9 = this.m_Pool.AddChild(this.gameObject).GetComponent<MiniMapController>();
    component1.Reset(KingdomX, KingdomY);
    component2.Reset(KingdomX - 1, KingdomY);
    component3.Reset(KingdomX + 1, KingdomY);
    component4.Reset(KingdomX, KingdomY + 1);
    component5.Reset(KingdomX, KingdomY - 1);
    component6.Reset(KingdomX - 1, KingdomY + 1);
    component7.Reset(KingdomX + 1, KingdomY + 1);
    component8.Reset(KingdomX - 1, KingdomY - 1);
    component9.Reset(KingdomX + 1, KingdomY - 1);
    this.m_Controllers.Add(component1);
    this.m_Controllers.Add(component2);
    this.m_Controllers.Add(component3);
    this.m_Controllers.Add(component4);
    this.m_Controllers.Add(component5);
    this.m_Controllers.Add(component6);
    this.m_Controllers.Add(component7);
    this.m_Controllers.Add(component8);
    this.m_Controllers.Add(component9);
    return component1;
  }

  private void Clear9Slices()
  {
    using (List<MiniMapController>.Enumerator enumerator = this.m_Controllers.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.m_Pool.Release(enumerator.Current.gameObject);
    }
    this.m_Controllers.Clear();
  }
}
