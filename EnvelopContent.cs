﻿// Decompiled with JetBrains decompiler
// Type: EnvelopContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIWidget))]
[AddComponentMenu("NGUI/Examples/Envelop Content")]
public class EnvelopContent : MonoBehaviour
{
  public Transform targetRoot;
  public int padLeft;
  public int padRight;
  public int padBottom;
  public int padTop;
  private bool mStarted;

  private void Start()
  {
    this.mStarted = true;
    this.Execute();
  }

  private void OnEnable()
  {
    if (!this.mStarted)
      return;
    this.Execute();
  }

  [ContextMenu("Execute")]
  public void Execute()
  {
    if ((Object) this.targetRoot == (Object) this.transform)
      Debug.LogError((object) "Target Root object cannot be the same object that has Envelop Content. Make it a sibling instead.", (Object) this);
    else if (NGUITools.IsChild(this.targetRoot, this.transform))
    {
      Debug.LogError((object) "Target Root object should not be a parent of Envelop Content. Make it a sibling instead.", (Object) this);
    }
    else
    {
      Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.transform.parent, this.targetRoot, false, true);
      float x = relativeWidgetBounds.min.x + (float) this.padLeft;
      float y = relativeWidgetBounds.min.y + (float) this.padBottom;
      float num1 = relativeWidgetBounds.max.x + (float) this.padRight;
      float num2 = relativeWidgetBounds.max.y + (float) this.padTop;
      this.GetComponent<UIWidget>().SetRect(x, y, num1 - x, num2 - y);
      this.BroadcastMessage("UpdateAnchors", SendMessageOptions.DontRequireReceiver);
    }
  }
}
