﻿// Decompiled with JetBrains decompiler
// Type: ChatMenuPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;

public class ChatMenuPopup : Popup
{
  public const string POP_TYPE = "Chat/ChatMenuPopup";
  public UIButton buttonReportIcon;
  public UIButton muteBtn;
  public UIButton reportAdminBtn;
  public UIButton blockBtn;
  public UIButton unblockBtn;
  public UIButton modMail;
  public UIButton profile;
  public UIButton sendMail;
  public UIButton addContact;
  public UIButton modButton;
  public UIGrid buttonContainer;
  private long _personalChannelId;
  private long _personalUid;
  private string _personalName;
  private string _senderCustomIcon;
  private int _personMuteAdmin;
  private int _personIcon;
  private string _content;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    ChatMenuPopup.Paramer paramer = orgParam as ChatMenuPopup.Paramer;
    if (paramer != null)
    {
      this._personalChannelId = paramer.personalChannelId;
      this._personalUid = paramer.personalUid;
      this._personalName = paramer.personalName;
      this._senderCustomIcon = paramer.senderCustomIcon;
      this._personMuteAdmin = paramer.personMuteAdmin;
      this._personIcon = paramer.personIcon;
      this._content = paramer.content;
    }
    this.Show(this._personalChannelId, this._personalUid, this._personalName, this._senderCustomIcon, this._personMuteAdmin, this._personIcon);
    EventDelegate.Add(this.modMail.onClick, new EventDelegate.Callback(this.OnModMail));
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    EventDelegate.Remove(this.modMail.onClick, new EventDelegate.Callback(this.OnModMail));
  }

  public void Show(long personalChannelId, long personalUid, string personalName, string senderCustomIcon, int personMuteAdmin, int personIcon)
  {
    if (personMuteAdmin != 0)
    {
      this.reportAdminBtn.gameObject.SetActive(true);
      this.buttonReportIcon.gameObject.SetActive(false);
      this.muteBtn.gameObject.SetActive(false);
      this.modMail.gameObject.SetActive(false);
      this.blockBtn.gameObject.SetActive(false);
      this.unblockBtn.gameObject.SetActive(false);
      this.profile.gameObject.SetActive(false);
      this.sendMail.gameObject.SetActive(false);
      this.addContact.gameObject.SetActive(false);
      this.modButton.gameObject.SetActive(false);
    }
    else
    {
      this.buttonReportIcon.gameObject.SetActive(!string.IsNullOrEmpty(senderCustomIcon));
      this.muteBtn.gameObject.SetActive(PlayerData.inst.userData.isMuteAdmin);
      this.reportAdminBtn.gameObject.SetActive(personMuteAdmin == 1);
      this.modMail.gameObject.SetActive(PlayerData.inst.moderatorInfo.IsModerator);
      this.blockBtn.gameObject.SetActive(!DBManager.inst.DB_Contacts.blocking.Contains(personalUid));
      this.unblockBtn.gameObject.SetActive(DBManager.inst.DB_Contacts.blocking.Contains(personalUid));
      this.profile.gameObject.SetActive(true);
      this.sendMail.gameObject.SetActive(true);
      this.addContact.gameObject.SetActive(true);
      this.modButton.gameObject.SetActive(true);
    }
    this.buttonContainer.repositionNow = true;
    this.buttonContainer.Reposition();
  }

  public void Hide()
  {
    this.OnCloseClick();
  }

  public void OnUserProfileClick()
  {
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
    {
      uid = this._personalUid
    }, 1 != 0, 1 != 0, 1 != 0);
    this.Hide();
  }

  public void OnSendMailClick()
  {
    PlayerData.inst.mail.ComposeMail(new List<string>()
    {
      this._personalName
    }, (string) null, (string) null, 0 != 0);
    this.Hide();
  }

  public void OnAddContactClick()
  {
    ContactManager.AddContact(this._personalUid, (System.Action<bool, object>) null);
    this.Hide();
  }

  public void OnBlockClick()
  {
    ContactManager.BlockUser(this._personalUid, (System.Action<bool, object>) null);
    this.Hide();
  }

  public void OnUnBlockClick()
  {
    ContactManager.UnblockUser(this._personalUid, (System.Action<bool, object>) null);
    this.Hide();
  }

  public void OnCloseClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnReportBtnClicked()
  {
    string str1 = ScriptLocalization.Get("id_uppercase_report", true);
    string str2 = ScriptLocalization.Get("player_profile_avatar_report_confirm_description", true);
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = str1,
      Content = str2,
      Okay = ScriptLocalization.Get("id_uppercase_confirm", true),
      OkayCallback = new System.Action(this.OnConfirmReportIcon)
    });
    this.Hide();
  }

  public void OnMuteBtnClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    HubPort portByAction = MessageHub.inst.GetPortByAction("Player:loadUserBasicInfo");
    Hashtable postData = new Hashtable();
    postData[(object) "target_uid"] = (object) this._personalUid;
    portByAction.SendRequest(postData, (System.Action<bool, object>) ((arg1, arg2) => UIManager.inst.OpenPopup("Chat/ChatMutePopup", (Popup.PopupParameter) new ChatMutePopup.Paramer()
    {
      personalChannelId = this._personalChannelId,
      personalName = this._personalName,
      senderCustomIcon = this._senderCustomIcon,
      personalUid = this._personalUid
    })), true);
  }

  protected void OnConfirmReportIcon()
  {
    Hashtable postData = new Hashtable()
    {
      {
        (object) "report_uid",
        (object) this._personalUid
      }
    };
    RequestManager.inst.SendRequest("Player:reportIcon", postData, new System.Action<bool, object>(this.OnReportCallback), true);
  }

  protected void OnReportCallback(bool result, object data)
  {
    if (!result)
      return;
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_avatar_report_success", true), (System.Action) null, 4f, true);
  }

  public void OnReportAdminBtnClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    UIManager.inst.OpenPopup("Chat/ChatReportAdminPopup", (Popup.PopupParameter) new ChatReportAdminPopup.Paramer()
    {
      personalChannelId = this._personalChannelId,
      personalName = this._personalName,
      senderCustomIcon = this._senderCustomIcon,
      personalUid = this._personalUid,
      personMuteAdmin = this._personMuteAdmin,
      personIcon = this._personIcon
    });
  }

  public void OnModClicked()
  {
    Hashtable postData = new Hashtable()
    {
      {
        (object) "opp_uid",
        (object) this._personalUid
      },
      {
        (object) "content",
        (object) this._content
      }
    };
    RequestManager.inst.SendRequest("Player:mod", postData, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    }), true);
  }

  public void OnModMail()
  {
    PlayerData.inst.mail.SendModMail(this._personalUid, new List<string>()
    {
      this._personalName
    });
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  public class Paramer : Popup.PopupParameter
  {
    public long personalChannelId;
    public long personalUid;
    public string personalName;
    public string senderCustomIcon;
    public int personMuteAdmin;
    public int personIcon;
    public string content;
  }
}
