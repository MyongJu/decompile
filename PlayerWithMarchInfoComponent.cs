﻿// Decompiled with JetBrains decompiler
// Type: PlayerWithMarchInfoComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class PlayerWithMarchInfoComponent : ComponentRenderBase
{
  public string lv = string.Empty;
  private Color32 COORDS_COLOR = new Color32((byte) 0, (byte) 0, (byte) 225, byte.MaxValue);
  public Icon icon;
  public UIButton actionBtn;
  public UIButton detailBtn;
  public UILabel m_LeftTimeLabel;
  public UISprite m_ProgressForeground;
  public UISlider m_ProgressBar;
  public string purpose;
  public WatchEntity m_Entity;
  private PlayerWithMarchInfoComponentData d;

  public MarchData marchData { protected set; get; }

  public RallyData rallyData { protected set; get; }

  public override void Init()
  {
    base.Init();
    this.d = this.data as PlayerWithMarchInfoComponentData;
    string str1 = (this.d.acronym == null ? (string) null : "(" + this.d.acronym + ")") + this.d.user_name;
    Utils.GetRGBFromColor32(this.COORDS_COLOR);
    string str2 = "[0000ff]";
    int watchtowerLevel = WatchtowerUtilities.GetWatchtowerLevel();
    string str3 = string.Empty;
    if (!string.IsNullOrEmpty(this.d.x))
    {
      int result = 0;
      int.TryParse(this.d.k, out result);
      if (MapUtils.IsPitWorld(result))
        str3 = string.Format("{0}{1}{2} X:{3}{4}{5} Y:{6}{7}{8}", (object) str2, (object) Utils.XLAT("id_fire_kingdom"), (object) "[-]", (object) str2, (object) this.d.x, (object) "[-]", (object) str2, (object) this.d.y, (object) "[-]");
      else
        str3 = string.Format("K:{0}{1}{2} X:{3}{4}{5} Y:{6}{7}{8}", (object) str2, (object) this.d.k, (object) "[-]", (object) str2, (object) this.d.x, (object) "[-]", (object) str2, (object) this.d.y, (object) "[-]");
    }
    else if ((UnityEngine.Object) this.icon.labels[3] != (UnityEngine.Object) null)
      this.icon.labels[3].gameObject.SetActive(false);
    string str4 = string.Empty;
    if (!string.IsNullOrEmpty(this.d.end_x))
    {
      int result = 0;
      int.TryParse(this.d.k, out result);
      if (MapUtils.IsPitWorld(result))
        str4 = string.Format("{0}{1}{2} X:{3}{4}{5} Y:{6}{7}{8}", (object) str2, (object) Utils.XLAT("id_fire_kingdom"), (object) "[-]", (object) str2, (object) this.d.end_x, (object) "[-]", (object) str2, (object) this.d.end_y, (object) "[-]");
      else
        str4 = string.Format("K:{0}{1}{2} X:{3}{4}{5} Y:{6}{7}{8}", (object) str2, (object) this.d.k, (object) "[-]", (object) str2, (object) this.d.end_x, (object) "[-]", (object) str2, (object) this.d.end_y, (object) "[-]");
    }
    string str5 = watchtowerLevel < 3 ? Utils.XLAT("watchtower_march_unlock_level3_requirement") : Utils.XLAT("watchtower_march_arriving_in") + Utils.FormatTime(int.Parse(this.d.end_time) - NetServerTime.inst.ServerTimestamp, false, false, true);
    this.UpdateMarchType(watchtowerLevel);
    string str6 = "Texture/Hero/Portrait_Icon/player_portrait_icon_" + this.d.portrait;
    if (this.d.npc_uid > 0)
    {
      NPC_Info dataByUid = ConfigManager.inst.DB_NPC.GetDataByUid((long) this.d.npc_uid);
      str6 = "Texture/Hero/Portrait_Icon/" + dataByUid.iconPath;
      str1 = dataByUid.LOC_Name;
    }
    this.icon.FeedData((IComponentData) new IconData(str6, new string[6]
    {
      str1,
      this.lv,
      this.purpose,
      str3,
      str4,
      str5
    }));
    CustomIconLoader.Instance.requestCustomIcon(this.icon.texture, str6, this.d.icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.icon.texture, this.d.lord_title, 1);
    this.OnSecondHandler(NetServerTime.inst.ServerTimestamp);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
  }

  private void OnSecondHandler(int timeStamp)
  {
    if (this.m_Entity.m_Type == WatchType.March)
    {
      if (WatchtowerUtilities.GetWatchtowerLevel() >= 3)
      {
        int startTime = this.marchData.startTime;
        int endTime = this.marchData.endTime;
        this.m_ProgressBar.value = Mathf.Clamp01((float) (timeStamp - startTime) / (float) (endTime - startTime));
        int num = endTime - timeStamp;
        this.m_LeftTimeLabel.text = Utils.XLAT("watchtower_march_arriving_in") + Utils.FormatTime(num <= 0 ? 0 : num, false, false, true);
      }
      else
      {
        this.m_ProgressBar.value = 0.0f;
        this.m_LeftTimeLabel.text = Utils.XLAT("watchtower_march_unlock_level3_requirement");
      }
    }
    else if (WatchtowerUtilities.GetWatchtowerLevel() >= 3)
    {
      int startTime = this.rallyData.waitTimeDuration.startTime;
      int endTime = this.rallyData.waitTimeDuration.endTime;
      this.m_ProgressBar.value = Mathf.Clamp01((float) (timeStamp - startTime) / (float) (endTime - startTime));
      int num = endTime - timeStamp;
      this.m_LeftTimeLabel.text = Utils.XLAT("watchtower_march_rallying_time_remaining") + Utils.FormatTime(num <= 0 ? 0 : num, false, false, true);
    }
    else
    {
      this.m_ProgressBar.value = 0.0f;
      this.m_LeftTimeLabel.text = Utils.XLAT("watchtower_march_unlock_level3_requirement");
    }
  }

  public override void Dispose()
  {
    base.Dispose();
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
    this.icon.Dispose();
  }

  public void OnGotoTargetClick()
  {
    if (string.IsNullOrEmpty(this.d.end_x))
      return;
    if (!MapUtils.CanGotoTarget(int.Parse(this.d.k)))
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    }
    else
    {
      PVPMap.PendingGotoRequest = new Coordinate(int.Parse(this.d.k), int.Parse(this.d.end_x), int.Parse(this.d.end_y));
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
        CitadelSystem.inst.OnLoadWorldMap();
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }

  public void OnGotoOwnerClick()
  {
    if (string.IsNullOrEmpty(this.d.x))
      return;
    if (!MapUtils.CanGotoTarget(int.Parse(this.d.k)))
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    }
    else
    {
      PVPMap.PendingGotoRequest = new Coordinate(int.Parse(this.d.k), int.Parse(this.d.x), int.Parse(this.d.y));
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
        CitadelSystem.inst.OnLoadWorldMap();
      if ((bool) ((UnityEngine.Object) UIManager.inst.tileCamera))
        UIManager.inst.tileCamera.StartAutoZoom();
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }

  public void SetData(WatchEntity entity)
  {
    this.m_Entity = entity;
    if (this.m_Entity.m_Type == WatchType.March)
      this.marchData = DBManager.inst.DB_March.Get(this.m_Entity.m_Id);
    else
      this.rallyData = DBManager.inst.DB_Rally.Get(this.m_Entity.m_Id);
    this.UpdateProgressBar(WatchtowerUtilities.GetWatchtowerLevel());
  }

  private void UpdateProgressBar(int towerLevel)
  {
    if (towerLevel >= 3)
      this.m_ProgressForeground.color = this.m_Entity.GetColor();
    else
      this.m_LeftTimeLabel.text = Utils.XLAT("watchtower_march_unlock_level3_requirement");
  }

  private void UpdateMarchType(int towerLevel)
  {
    if (towerLevel < 0)
      this.purpose = "Activity";
    else if (this.m_Entity.m_Type == WatchType.March)
    {
      if (this.marchData.type == MarchData.MarchType.trade)
        this.purpose = Utils.XLAT("watchtower_march_trade");
      else if (this.marchData.type == MarchData.MarchType.reinforce)
        this.purpose = Utils.XLAT("watchtower_march_reinforce");
      else if (this.marchData.type == MarchData.MarchType.fortress_reinforce)
        this.purpose = Utils.XLAT("watchtower_march_reinforce");
      else if (this.marchData.type == MarchData.MarchType.wonder_reinforce)
        this.purpose = Utils.XLAT("watchtower_march_reinforce");
      else if (this.marchData.type == MarchData.MarchType.scout)
        this.purpose = Utils.XLAT("watchtower_march_scout");
      else if (this.marchData.isNormalAttack)
        this.purpose = Utils.XLAT("watchtower_march_attack");
      else if (this.marchData.type == MarchData.MarchType.rally_attack)
        this.purpose = Utils.XLAT("watchtower_march_rally");
      else if (this.marchData.type == MarchData.MarchType.fortress_attack)
        this.purpose = Utils.XLAT("watchtower_march_attack");
      else
        this.purpose = Utils.XLAT("watchtower_march_attack");
    }
    else
      this.purpose = Utils.XLAT("watchtower_march_rallying");
  }
}
