﻿// Decompiled with JetBrains decompiler
// Type: ItemBoostResAttachment
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ItemBoostResAttachment : CityTouchCircleBtnAttachment
{
  public UILabel header;
  public UISprite type;

  public override void SeedData(CityTouchCircleBtnAttachment.AttachmentData ad)
  {
    ItemBoostResAttachment.IBRAData ibraData = ad as ItemBoostResAttachment.IBRAData;
    if (ibraData == null)
      return;
    this.header.text = ibraData.content;
    this.type.spriteName = ibraData.typename;
  }

  public class IBRAData : CityTouchCircleBtnAttachment.AttachmentData
  {
    public string content;
    public string typename;
  }
}
