﻿// Decompiled with JetBrains decompiler
// Type: QuestRefreshDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using UI;

public class QuestRefreshDlg : Popup
{
  public UILabel itemName;
  public UILabel ownedCount;
  public UILabel buyButtonCost;
  public UITexture itemImage;
  public UIButton getAndUseButton;
  public UIButton useButton;
  private string itemNeeded;
  public ItemStaticInfo itemInfo;
  public ShopStaticInfo shopInfo;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    QuestRefreshDlg.Parameter parameter = orgParam as QuestRefreshDlg.Parameter;
    if (parameter == null)
      return;
    this.itemNeeded = parameter.itemNeed;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.shopInfo = ConfigManager.inst.DB_Shop.GetShopData(this.itemNeeded);
    this.itemInfo = ConfigManager.inst.DB_Items.GetItem(this.shopInfo.Item_InternalId);
    int itemCount = ItemBag.Instance.GetItemCount(this.itemInfo.ID);
    this.itemName.text = ScriptLocalization.Get(this.itemInfo.Loc_Name_Id, true);
    this.ownedCount.text = "Owned: " + (object) itemCount;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.itemImage, this.itemInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.buyButtonCost.text = ItemBag.Instance.GetShopItemPrice(this.shopInfo.ID).ToString();
    if (itemCount == 0)
    {
      this.getAndUseButton.gameObject.SetActive(true);
      this.useButton.gameObject.SetActive(false);
    }
    else
    {
      this.getAndUseButton.gameObject.SetActive(false);
      this.useButton.gameObject.SetActive(true);
    }
  }

  private void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.itemImage);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Clear();
  }

  public void OnCloseClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void RenderInfo()
  {
  }

  private void ItemUsedCallback(bool ret, object data)
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnGetAndUseClick()
  {
    ItemBag.Instance.BuyAndUseShopItem(this.shopInfo.internalId, new System.Action<bool, object>(this.ItemUsedCallback), (Hashtable) null, 1);
  }

  public void OnUseClick()
  {
    ItemBag.Instance.UseItem(this.itemInfo.internalId, 1, (Hashtable) null, new System.Action<bool, object>(this.ItemUsedCallback));
  }

  private void OnPlayerStateChanged(Events.PlayerStatChangedArgs obj)
  {
    this.RenderInfo();
  }

  public class Parameter : Popup.PopupParameter
  {
    public string titleText;
    public string itemNeed;
  }
}
