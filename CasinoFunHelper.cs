﻿// Decompiled with JetBrains decompiler
// Type: CasinoFunHelper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class CasinoFunHelper
{
  private const string CASINO_SHARE_TIME = "casino_share_time";
  public bool isFreeOnceTime;
  private static CasinoFunHelper _inst;
  private List<KeyValuePair<int, List<int>>> jackpotFakeSavedData;

  public static CasinoFunHelper Instance
  {
    get
    {
      if (CasinoFunHelper._inst == null)
        CasinoFunHelper._inst = new CasinoFunHelper();
      return CasinoFunHelper._inst;
    }
  }

  public string FreeSpinLeftTime
  {
    get
    {
      return Utils.FormatTime(NetServerTime.inst.TodayLeftTimeUTC(), false, false, true);
    }
  }

  public bool isFreeSpinRoulette()
  {
    StatsData statsData = DBManager.inst.DB_Stats.Get("casino_play");
    if (statsData == null)
      return true;
    bool flag = NetServerTime.inst.IsToday((double) statsData.mtime);
    if (!flag && this.isFreeOnceTime)
      return false;
    return !flag;
  }

  public void ShowFirstCasinoScene()
  {
    MessageHub.inst.GetPortByAction("casino:getChest").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable inData = data as Hashtable;
      ArrayList data1 = new ArrayList();
      int outData1 = -1;
      int outData2 = -1;
      if (inData == null || inData[(object) "chest_id"] == null)
        return;
      DatabaseTools.UpdateData(inData, "chest_id", ref outData1);
      DatabaseTools.UpdateData(inData, "shuffle", ref outData2);
      DatabaseTools.CheckAndParseOrgData(inData[(object) "opened"], out data1);
      if (outData1 == -1)
        UIManager.inst.OpenDlg("Casino/CasinoDlg", (UI.Dialog.DialogParameter) null, true, true, true);
      else
        UIManager.inst.OpenDlg("Casino/CasinoPickCardDlg", (UI.Dialog.DialogParameter) new CasinoPickCardDlg.Parameter()
        {
          hitChestId = outData1,
          openedCells = data1,
          canShuffle = (outData2 != 1)
        }, true, true, true);
    }), true);
  }

  public void ShareCasinoCardDetails(bool isKingdomChannel = false)
  {
    if (NetServerTime.inst.ServerTimestamp - PlayerPrefsEx.GetIntByUid("casino_share_time", 0) < 300)
      UIManager.inst.toast.Show(Utils.XLAT("toast_share_cooldown_notice"), (System.Action) null, 4f, false);
    else
      MessageHub.inst.GetPortByAction("casino:share").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        Hashtable inData = data as Hashtable;
        long outData = -1;
        if (inData == null || inData[(object) nameof (ret)] == null)
          return;
        DatabaseTools.UpdateData(inData, nameof (ret), ref outData);
        if (outData == -1L || !ChatMessageManager.SendCasinoCardView(NetServerTime.inst.ServerTimestamp, PlayerData.inst.uid, outData, isKingdomChannel))
          return;
        PlayerPrefsEx.SetIntByUid("casino_share_time", NetServerTime.inst.ServerTimestamp);
      }), true);
  }

  public void PlayParticles(GameObject obj)
  {
    if ((UnityEngine.Object) obj == (UnityEngine.Object) null)
      return;
    obj.SetActive(true);
    foreach (ParticleSystem componentsInChild in obj.GetComponentsInChildren<ParticleSystem>())
      componentsInChild.Play();
  }

  public void StopParticles(GameObject obj)
  {
    if ((UnityEngine.Object) obj == (UnityEngine.Object) null)
      return;
    foreach (ParticleSystem componentsInChild in obj.GetComponentsInChildren<ParticleSystem>())
      componentsInChild.Stop();
    obj.SetActive(false);
  }

  public void ShowJackpotScene()
  {
    JackpotPayload.Instance.RequestServerData((System.Action<bool, object>) ((ret, data) => UIManager.inst.OpenDlg("Jackpot/JackpotDlg", (UI.Dialog.DialogParameter) null, true, true, true)));
  }

  public List<KeyValuePair<int, List<int>>> JackpotFakeSavedData
  {
    get
    {
      if (ConfigManager.inst.DB_CasinoJackpot.JackpotData.Count < ConfigManager.inst.DB_CasinoJackpot.JackpotSlotCount)
        return ConfigManager.inst.DB_CasinoJackpot.GetJackpotFakeData();
      return ConfigManager.inst.DB_CasinoJackpot.JackpotData;
    }
  }

  public void GambleJackpot(string betType, System.Action<int, int> callback)
  {
    MessageHub.inst.GetPortByAction("jackpot:gamble").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "gamble_type",
        (object) betType
      }
    }, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      int outData1 = 0;
      int outData2 = 0;
      Hashtable hashtable = arg2 as Hashtable;
      if (hashtable != null)
      {
        DatabaseTools.UpdateData(hashtable, "type", ref outData1);
        DatabaseTools.UpdateData(hashtable, "id", ref outData2);
      }
      if (outData1 == 3 && outData2 == 0)
        outData2 = -1;
      if (callback != null)
        callback(outData1, outData2);
      JackpotPayload.Instance.Decode(hashtable);
    }), true);
  }

  public struct BetType
  {
    public const string GOLD = "gold";
    public const string OTHER = "other";
  }

  public struct WinType
  {
    public const int NO_WIN = 0;
    public const int X1_WIN = 1;
    public const int X5_WIN = 2;
    public const int JACKPOT_WIN = 3;
  }
}
