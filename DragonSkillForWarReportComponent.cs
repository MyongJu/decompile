﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillForWarReportComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class DragonSkillForWarReportComponent : ComponentRenderBase
{
  public Icon dragon;
  public IconListComponent skillList;

  public override void Init()
  {
    base.Init();
    DragonSkillForWarReportComponentData data1 = this.data as DragonSkillForWarReportComponentData;
    if (data1 == null || data1.level == null)
    {
      this.gameObject.SetActive(false);
    }
    else
    {
      this.dragon.FeedData((IComponentData) new IconData(DragonUtils.GetDragonIconPath(int.Parse(data1.tendency), int.Parse(data1.level)), new bool[1]
      {
        data1.level != null
      }, new string[1]{ data1.level }));
      List<IconData> data2 = new List<IconData>();
      if (data1.skill != null)
      {
        for (int index = 0; index < data1.skill.Count; ++index)
        {
          string s = data1.skill[index].ToString();
          int result = 0;
          if (data1.skill_star != null && index < data1.skill_star.Count)
            int.TryParse(data1.skill_star[index], out result);
          if (int.Parse(s) != 0)
          {
            ConfigDragonSkillMainInfo skillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo(int.Parse(s));
            ConfigDragonSkillGroupInfo dragonSkillGroupInfo = ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupInfo(skillMainInfo.group_id);
            ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(data1.artifact);
            string str = string.Empty;
            if (artifactInfo != null && dragonSkillGroupInfo.learn_method == 0)
            {
              int num = !(artifactInfo.activeEffect == "light_wand") ? (!(dragonSkillGroupInfo.type == ConfigDragonSkillGroupInfo.DARK) ? 0 : 1) : (!(dragonSkillGroupInfo.type == ConfigDragonSkillGroupInfo.LIGHT) ? 0 : 1);
              if (num > 0)
                str = "+" + (object) num;
            }
            bool[] visibles = new bool[1]{ result > 0 };
            IconData iconData = new IconData(dragonSkillGroupInfo.IconPath, visibles, new string[3]
            {
              skillMainInfo.level <= 0 ? "0" : skillMainInfo.level.ToString(),
              str,
              result.ToString()
            });
            data2.Add(iconData);
          }
        }
      }
      this.skillList.FeedData((IComponentData) new IconListComponentData(data2));
    }
  }
}
