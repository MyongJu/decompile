﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsGroupInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class MerlinTrialsGroupInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "open_day")]
  public string openDay;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "image")]
  public string image;
  [Config(Name = "is_with_dragon")]
  public int useDragon;
  [Config(Name = "effect")]
  public string effectPath;
  [Config(Name = "background_color")]
  public string backgroundColor;
  private List<int> openDays;

  public List<int> OpenDays
  {
    get
    {
      if (this.openDays == null)
      {
        this.openDays = new List<int>();
        string openDay = this.openDay;
        char[] chArray = new char[1]{ ',' };
        foreach (string s in openDay.Split(chArray))
        {
          int result;
          if (int.TryParse(s, out result))
            this.openDays.Add(result);
        }
      }
      return this.openDays;
    }
  }

  public string LocName
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }

  public string ImagePath
  {
    get
    {
      return "Texture/IAP/" + this.image;
    }
  }

  public string EffectPath
  {
    get
    {
      return "Prefab/VFX/MerlinTrials/" + this.effectPath;
    }
  }

  public string DetailEffectPath
  {
    get
    {
      return string.Format("Prefab/VFX/MerlinTrials/{0}_detail", (object) this.effectPath);
    }
  }

  public string TypeIconPath
  {
    get
    {
      return this.image + "_small";
    }
  }
}
