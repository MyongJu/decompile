﻿// Decompiled with JetBrains decompiler
// Type: ItemTipHeroChipInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class ItemTipHeroChipInfo : MonoBehaviour
{
  private List<HeroBookBenefitItem> _AllBenefitLabel = new List<HeroBookBenefitItem>();
  public UITexture icon;
  public UITexture background;
  public UILabel itemName;
  public UILabel itemDesc;
  public GameObject unKnow;
  public GameObject normal;
  public UITable tabel;
  [SerializeField]
  private UILabel heroLevel;
  public HeroBookBenefitItem _HeroBookBenefitItemTemplate;
  [SerializeField]
  private UILabel _LabelCareer;
  [SerializeField]
  private UITable _TableDetail;
  [SerializeField]
  private UILabel _LabelScore;
  [SerializeField]
  private UITable _TableHeroBaseBenefit;
  [SerializeField]
  private UITable _TableHeroStarBenefit;
  [SerializeField]
  private UITable _TableHeroSuitBenefit;
  [SerializeField]
  private GameObject _RootHeroSuitBenefit;
  [SerializeField]
  private GameObject _RootHeroSuitBenefitBg;
  [SerializeField]
  private UILabel _LabelSuitName;
  private int itemId;
  private bool _LayoutDirty;

  public void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
    BuilderFactory.Instance.Release((UIWidget) this.background);
  }

  public void SetData(int itemId)
  {
    this.itemId = itemId;
    ParliamentHeroInfo byChipId = ConfigManager.inst.DB_ParliamentHero.GetByChipId(itemId);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    this.itemName.text = itemStaticInfo.LocName;
    this.itemDesc.text = itemStaticInfo.LocDescription;
    BuilderFactory.Instance.Build((UIWidget) this.icon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.background, Utils.GetQualityImagePath(itemStaticInfo.Quality), (System.Action<bool>) null, true, false, true, string.Empty);
    Utils.SetItemName(this.itemName, itemId);
    if (byChipId == null)
    {
      NGUITools.SetActive(this.unKnow, true);
      NGUITools.SetActive(this.normal, false);
    }
    else
    {
      NGUITools.SetActive(this.unKnow, false);
      NGUITools.SetActive(this.normal, true);
      LegendCardData legendCardData = (LegendCardData) null;
      if (byChipId == null)
        return;
      ParliamentInfo parliamentInfo = ConfigManager.inst.DB_Parliament.Get(byChipId.parliamentPosition);
      if (parliamentInfo != null)
        this._LabelCareer.text = parliamentInfo.Name;
      ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(byChipId.quality.ToString());
      if (parliamentHeroQualityInfo == null)
        D.error((object) string.Format("cannot find parliament hero quality info: {0}", (object) byChipId.quality));
      else if (parliamentHeroQualityInfo == null)
      {
        D.error((object) string.Format("cannot find parliament info: {0}", (object) byChipId.parliamentPosition));
      }
      else
      {
        this._LabelScore.text = ScriptLocalization.GetWithPara("hero_score_level_num", new Dictionary<string, string>()
        {
          {
            "0",
            byChipId.InitScore.ToString()
          }
        }, true);
        this.heroLevel.text = ScriptLocalization.GetWithPara("hero_xp_level_num", new Dictionary<string, string>()
        {
          {
            "0",
            (legendCardData != null ? legendCardData.Level : 1).ToString()
          },
          {
            "1",
            parliamentHeroQualityInfo.maxLevel.ToString()
          }
        }, true);
        this.DestoryAllBenefitItem();
        this.UpdateHeroBaseBenefit(legendCardData, byChipId);
        this.UpdateHeroStarBenefit(legendCardData, byChipId);
        this.UpdateHeroSuitBenefit(legendCardData, byChipId);
        this._TableDetail.Reposition();
        this._LayoutDirty = true;
      }
    }
  }

  protected void UpdateHeroBaseBenefit(LegendCardData legendCardData, ParliamentHeroInfo parliamentHeroInfo)
  {
    int level = legendCardData != null ? legendCardData.Level : 1;
    this.CreateBenefitItem(this._TableHeroBaseBenefit.transform, string.Empty, parliamentHeroInfo.levelBenefit, parliamentHeroInfo.GetLevelBenefitByLevel(level)).SetLineEnabled(false);
    this._TableHeroBaseBenefit.Reposition();
  }

  protected void UpdateHeroStarBenefit(LegendCardData legendCardData, ParliamentHeroInfo parliamentHeroInfo)
  {
    HeroBookBenefitItem heroBookBenefitItem = (HeroBookBenefitItem) null;
    for (int index = 0; index < parliamentHeroInfo.AllStarBenefit.Length; ++index)
    {
      Dictionary<string, string> para = new Dictionary<string, string>()
      {
        {
          "0",
          parliamentHeroInfo.AllStarRequirement[index].ToString()
        }
      };
      if (parliamentHeroInfo.AllStarBenefit[index] != 0)
      {
        heroBookBenefitItem = this.CreateBenefitItem(this._TableHeroStarBenefit.transform, ScriptLocalization.GetWithPara("hero_star_level_num", para, true), parliamentHeroInfo.AllStarBenefit[index], parliamentHeroInfo.AllStarBenefitValue[index]);
        heroBookBenefitItem.SetTextColor(Color.gray);
      }
    }
    this._TableHeroStarBenefit.Reposition();
    if (!((UnityEngine.Object) heroBookBenefitItem != (UnityEngine.Object) null))
      return;
    heroBookBenefitItem.SetLineEnabled(false);
  }

  protected void UpdateHeroSuitBenefit(LegendCardData legendCardData, ParliamentHeroInfo parliamentHeroInfo)
  {
    ParliamentSuitGroupInfo parliamentSuitGroupInfo = ConfigManager.inst.DB_ParliamentSuitGroup.Get(parliamentHeroInfo.heroSuitGroup);
    if (parliamentSuitGroupInfo != null)
    {
      this._RootHeroSuitBenefit.SetActive(true);
      this._RootHeroSuitBenefitBg.SetActive(true);
      this._LabelSuitName.text = parliamentSuitGroupInfo.Name;
      HeroBookBenefitItem heroBookBenefitItem = (HeroBookBenefitItem) null;
      for (int index = 0; index < parliamentSuitGroupInfo.AllSuitBenefit.Length; ++index)
      {
        if (parliamentSuitGroupInfo.AllSuitBenefit[index] != 0)
        {
          heroBookBenefitItem = this.CreateBenefitItem(this._TableHeroSuitBenefit.transform, ScriptLocalization.GetWithPara("hero_appoint_num_group_benefits", new Dictionary<string, string>()
          {
            {
              "0",
              parliamentSuitGroupInfo.AllSuitRequirement[index].ToString()
            },
            {
              "1",
              parliamentSuitGroupInfo.Name
            }
          }, true), parliamentSuitGroupInfo.AllSuitBenefit[index], parliamentSuitGroupInfo.AllSuitBenefitValue[index]);
          heroBookBenefitItem.SetTextColor(Color.gray);
        }
      }
      this._TableHeroSuitBenefit.Reposition();
      if (!((UnityEngine.Object) heroBookBenefitItem != (UnityEngine.Object) null))
        return;
      heroBookBenefitItem.SetLineEnabled(false);
    }
    else
    {
      this._RootHeroSuitBenefit.SetActive(false);
      this._RootHeroSuitBenefitBg.SetActive(false);
    }
  }

  private void LateUpdate()
  {
    if (!this._LayoutDirty)
      return;
    this._LayoutDirty = false;
    this._TableHeroBaseBenefit.Reposition();
    this._TableHeroStarBenefit.Reposition();
    this._TableHeroSuitBenefit.Reposition();
    this._TableDetail.Reposition();
    this.tabel.Reposition();
  }

  protected HeroBookBenefitItem CreateBenefitItem(Transform parent, string condition, int benefitId, float value)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._HeroBookBenefitItemTemplate.gameObject);
    gameObject.transform.SetParent(parent);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    HeroBookBenefitItem component = gameObject.GetComponent<HeroBookBenefitItem>();
    this._AllBenefitLabel.Add(component);
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitId];
    string str1 = dbProperty == null ? "unknow" : dbProperty.Name;
    string str2 = dbProperty == null ? value.ToString() : dbProperty.ConvertToDisplayString((double) value, true, true);
    if (string.IsNullOrEmpty(condition))
      component.SetData(string.Format("{0} {1}", (object) str1, (object) str2));
    else
      component.SetData(string.Format("{0} {1} {2}", (object) condition, (object) str1, (object) str2));
    return component;
  }

  protected void DestoryAllBenefitItem()
  {
    using (List<HeroBookBenefitItem>.Enumerator enumerator = this._AllBenefitLabel.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HeroBookBenefitItem current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
        {
          current.transform.SetParent((Transform) null);
          UnityEngine.Object.DestroyObject((UnityEngine.Object) current.gameObject);
        }
      }
    }
    this._AllBenefitLabel.Clear();
  }
}
