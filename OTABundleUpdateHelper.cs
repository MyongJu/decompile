﻿// Decompiled with JetBrains decompiler
// Type: OTABundleUpdateHelper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

internal class OTABundleUpdateHelper
{
  private static List<string> priorityKeys = new List<string>()
  {
    "dragon_knight",
    "troop",
    "monster"
  };
  private static Dictionary<string, OTABundleUpdateHelper.Priority> priorityMap = new Dictionary<string, OTABundleUpdateHelper.Priority>()
  {
    {
      "dragon_knight",
      OTABundleUpdateHelper.Priority.DragonKnight
    },
    {
      "troop",
      OTABundleUpdateHelper.Priority.Troop
    },
    {
      "monster",
      OTABundleUpdateHelper.Priority.Monster
    }
  };

  public static void GetOrderedOtaversionStack(List<string> list, ref Stack<string> otaBundleStack)
  {
    List<OTABundleUpdateHelper.PriorityOtaBundle> priorityOtaBundleList = new List<OTABundleUpdateHelper.PriorityOtaBundle>();
    using (List<string>.Enumerator enumerator1 = list.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        string current1 = enumerator1.Current;
        using (List<string>.Enumerator enumerator2 = OTABundleUpdateHelper.priorityKeys.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            string current2 = enumerator2.Current;
            if (current1.Contains(current2) && OTABundleUpdateHelper.priorityMap.ContainsKey(current2))
              priorityOtaBundleList.Add(new OTABundleUpdateHelper.PriorityOtaBundle()
              {
                priority = OTABundleUpdateHelper.Priority.DragonKnight,
                bundlename = current1
              });
            else
              priorityOtaBundleList.Add(new OTABundleUpdateHelper.PriorityOtaBundle()
              {
                priority = OTABundleUpdateHelper.Priority.Normal,
                bundlename = current1
              });
          }
        }
      }
    }
    priorityOtaBundleList.Sort((Comparison<OTABundleUpdateHelper.PriorityOtaBundle>) ((x, y) => x.priority.CompareTo((object) y.priority)));
    using (List<OTABundleUpdateHelper.PriorityOtaBundle>.Enumerator enumerator = priorityOtaBundleList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        OTABundleUpdateHelper.PriorityOtaBundle current = enumerator.Current;
        otaBundleStack.Push(current.bundlename);
      }
    }
  }

  private enum Priority
  {
    Normal = 0,
    DragonKnight = 10, // 0x0000000A
    Troop = 15, // 0x0000000F
    Monster = 20, // 0x00000014
  }

  private struct PriorityOtaBundle
  {
    public OTABundleUpdateHelper.Priority priority;
    public string bundlename;
  }
}
