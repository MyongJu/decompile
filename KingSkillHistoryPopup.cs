﻿// Decompiled with JetBrains decompiler
// Type: KingSkillHistoryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class KingSkillHistoryPopup : Popup
{
  private List<KingSkillHistoryItem> _allKingSkillHistoryItem = new List<KingSkillHistoryItem>();
  [SerializeField]
  private UITable _table;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private KingSkillHistoryItem _kingSkillHistoryItemTemplate;
  [SerializeField]
  private GameObject _rootTagNoRecord;
  private ArrayList _allRecords;
  private bool _dirty;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._kingSkillHistoryItemTemplate.gameObject.SetActive(false);
    RequestManager.inst.SendRequest("king:loadKingSkillLog", (Hashtable) null, new System.Action<bool, object>(this.OnRequestCallback), true);
  }

  protected void OnRequestCallback(bool result, object data)
  {
    if (!result)
      return;
    this._allRecords = data as ArrayList;
    this.UpdateList();
    this._dirty = true;
  }

  protected void LateUpdate()
  {
    if (!this._dirty)
      return;
    this._dirty = false;
    this._table.repositionNow = true;
    this._table.Reposition();
  }

  protected void UpdateList()
  {
    this.DestoryAllKingSkillHistoryItem();
    if (this._allRecords != null)
    {
      for (int index1 = 0; index1 < this._allRecords.Count; ++index1)
      {
        Hashtable allRecord = this._allRecords[index1] as Hashtable;
        if (allRecord != null)
        {
          long result1 = 0;
          int result2 = 0;
          long.TryParse(allRecord[(object) "time"].ToString(), out result1);
          int.TryParse(allRecord[(object) "skill_id"].ToString(), out result2);
          KingSkillInfo byId = ConfigManager.inst.DB_KingSkill.GetById(result2);
          if (byId != null)
          {
            ArrayList arrayList = allRecord[(object) "params"] as ArrayList;
            Dictionary<string, string> para = new Dictionary<string, string>();
            if (arrayList != null)
            {
              for (int index2 = 0; index2 < arrayList.Count; ++index2)
                para.Add(index2.ToString(), arrayList[index2].ToString());
            }
            string Term = string.Empty;
            string skillType = byId.SkillType;
            if (skillType != null)
            {
              // ISSUE: reference to a compiler-generated field
              if (KingSkillHistoryPopup.\u003C\u003Ef__switch\u0024map76 == null)
              {
                // ISSUE: reference to a compiler-generated field
                KingSkillHistoryPopup.\u003C\u003Ef__switch\u0024map76 = new Dictionary<string, int>(5)
                {
                  {
                    "get_gold",
                    0
                  },
                  {
                    "after_war_training",
                    1
                  },
                  {
                    "find_target",
                    2
                  },
                  {
                    "force_teleport",
                    3
                  },
                  {
                    "send_troop_forbidden",
                    4
                  }
                };
              }
              int num;
              // ISSUE: reference to a compiler-generated field
              if (KingSkillHistoryPopup.\u003C\u003Ef__switch\u0024map76.TryGetValue(skillType, out num))
              {
                switch (num)
                {
                  case 0:
                    Term = "throne_king_skill_1_history_description";
                    break;
                  case 1:
                    Term = "throne_king_skill_2_history_description";
                    break;
                  case 2:
                    Term = "throne_king_skill_5_history_description";
                    break;
                  case 3:
                    Term = "throne_king_skill_3_history_description";
                    break;
                  case 4:
                    Term = "throne_king_skill_4_history_description";
                    break;
                }
              }
            }
            this.CreateKingSkillHistoryItem().SetDetail(Utils.FormatTimeToUTC(result1) + "\r\n" + ScriptLocalization.GetWithPara(Term, para, true));
          }
        }
      }
    }
    this._rootTagNoRecord.gameObject.SetActive(this._allRecords.Count <= 0);
    this._table.repositionNow = true;
    this._table.Reposition();
    this._scrollView.movement = UIScrollView.Movement.Unrestricted;
    this._scrollView.ResetPosition();
    this._scrollView.currentMomentum = Vector3.zero;
    this._scrollView.movement = UIScrollView.Movement.Vertical;
  }

  protected void DestoryAllKingSkillHistoryItem()
  {
    using (List<KingSkillHistoryItem>.Enumerator enumerator = this._allKingSkillHistoryItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this._allKingSkillHistoryItem.Clear();
  }

  protected KingSkillHistoryItem CreateKingSkillHistoryItem()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._kingSkillHistoryItemTemplate.gameObject);
    gameObject.SetActive(true);
    gameObject.transform.SetParent(this._table.transform);
    gameObject.transform.localScale = Vector3.one;
    KingSkillHistoryItem component = gameObject.GetComponent<KingSkillHistoryItem>();
    this._allKingSkillHistoryItem.Add(component);
    return component;
  }

  public void OnButtonCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }
}
