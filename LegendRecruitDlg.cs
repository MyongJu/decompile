﻿// Decompiled with JetBrains decompiler
// Type: LegendRecruitDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class LegendRecruitDlg : UI.Dialog
{
  private Color normalColor = new Color(0.9411765f, 0.8705882f, 0.7411765f, 1f);
  private int _selectedIndex = -1;
  public UITexture legendIcon;
  public LegendSkillNormalItem[] skills;
  private LegendData _legendData;
  public UILabel legendName;
  public UILabel legenLevel;
  public UILabel currentXp;
  public UILabel power;
  public UIProgressBar upgradeProgressBar;
  private int _legendConfigId;
  public UILabel costSliver;
  public UIButton recruitButton;
  public GameObject skillList;
  public GameObject legendHistory;
  public GameObject skillListNormalBt;
  public GameObject skillListSelectedBt;
  public GameObject legendHistroyNormalBt;
  public GameObject legendHistroySelectedBt;
  public GameObject infantry;
  public GameObject ranged;
  public GameObject cavalry;
  public GameObject siege;
  public UIGrid grid;
  public UILabel legendStroy;
  public UILabel title;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    LegendRecruitDlg.LegendRecruitDlgParameter recruitDlgParameter = orgParam as LegendRecruitDlg.LegendRecruitDlgParameter;
    if (recruitDlgParameter != null)
    {
      this._legendConfigId = recruitDlgParameter.legendConfigId;
      this._legendData = DBManager.inst.DB_Legend.GetLegendDataByLegendID(this._legendConfigId);
    }
    this.UpdateUI();
    this.UpdateRecruitButton();
    base.OnShow(orgParam);
    this.AddEventHandler();
    this.SelectedIndex = 0;
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Clear();
    this.RemoveEventHandler();
  }

  public void OnDisbandBtnPress()
  {
  }

  public void ConfirmReCruitLegend()
  {
    if (!ConfigManager.inst.DB_Legend.GetLegendInfo(this._legendConfigId).cost.IsSilverEnough())
      GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.SILVER);
    else if (DBManager.inst.DB_Legend.IsMaxLegendCount())
    {
      if (DBManager.inst.DB_Legend.IsMaxSlot())
        UIManager.inst.toast.Show(ScriptLocalization.Get("legend_max_slot", true), (System.Action) null, 4f, false);
      else
        UIManager.inst.OpenPopup("GetMoreLegend", (Popup.PopupParameter) new GetMoreLegendDlg.GetMoreSlotDialogParamer());
    }
    else
      MessageHub.inst.GetPortByAction("Legend:recruit").SendRequest(new Hashtable()
      {
        {
          (object) "city_id",
          (object) PlayerData.inst.cityId
        },
        {
          (object) "building_id",
          (object) CityManager.inst.mTavern.mID
        },
        {
          (object) "legend_config_id",
          (object) this._legendConfigId
        },
        {
          (object) "uid",
          (object) PlayerData.inst.uid
        }
      }, new System.Action<bool, object>(this.RecruiteCallBack), true);
  }

  private void UpdateRecruitButton()
  {
    long currentResource = (long) PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
    LegendInfo legendInfo = ConfigManager.inst.DB_Legend.GetLegendInfo(this._legendConfigId);
    bool flag = (long) legendInfo.gold <= currentResource;
    int requirementBuildingLevel = this.GetRequirementBuildingLevel(legendInfo);
    BuildingController buildingByType = CitadelSystem.inst.GetBuildingByType("dragon_lair");
    int num = 0;
    if ((UnityEngine.Object) null != (UnityEngine.Object) buildingByType)
      num = buildingByType.mBuildingItem.mLevel;
    bool state = flag && num >= requirementBuildingLevel;
    if (DBManager.inst.DB_Legend.CheckLegendIsUnderRecruit(this._legendConfigId))
      return;
    NGUITools.SetActive(this.recruitButton.gameObject, state);
  }

  private int GetRequirementBuildingLevel(LegendInfo legendInfo)
  {
    using (List<Requirements.RequireValuePair>.Enumerator enumerator = legendInfo.require.RequireBuildings().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuildingInfo data = ConfigManager.inst.DB_Building.GetData(enumerator.Current.internalID);
        if (data != null && "dragon_lair" == data.Type)
          return data.Building_Lvl;
      }
    }
    return 0;
  }

  private void RecruiteCallBack(bool success, object result)
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
    UIManager.inst.toast.Show(string.Format(ScriptLocalization.Get("toast_summon_success", true), (object) this.legendName.text), (System.Action) null, 4f, false);
  }

  private void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.legendIcon);
    foreach (LegendSkillNormalItem skill in this.skills)
      skill.Clear();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_User.onDataChanged += new System.Action<long>(this.OnUserDataChanged);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_User.onDataChanged -= new System.Action<long>(this.OnUserDataChanged);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
  }

  private void Process(int time)
  {
    this.UpdateRecruitButton();
  }

  private void OnUserDataChanged(long uid)
  {
    this.UpdateUI();
  }

  public void OnTimeInfoPress()
  {
  }

  public void SelectedSkillList()
  {
    this.SelectedIndex = 0;
  }

  public void SelectedLegendHistory()
  {
    this.SelectedIndex = 1;
  }

  public int SelectedIndex
  {
    get
    {
      return this._selectedIndex;
    }
    protected set
    {
      if (this._selectedIndex == value)
        return;
      this._selectedIndex = value;
      this.ChangeTabContent();
    }
  }

  private void ChangeTabContent()
  {
    if (this.SelectedIndex == 0)
    {
      NGUITools.SetActive(this.skillList.gameObject, true);
      NGUITools.SetActive(this.legendHistory.gameObject, false);
      NGUITools.SetActive(this.skillListSelectedBt.gameObject, true);
      NGUITools.SetActive(this.legendHistroySelectedBt.gameObject, false);
    }
    else
    {
      NGUITools.SetActive(this.skillList.gameObject, false);
      NGUITools.SetActive(this.legendHistory.gameObject, true);
      NGUITools.SetActive(this.skillListSelectedBt.gameObject, false);
      NGUITools.SetActive(this.legendHistroySelectedBt.gameObject, true);
    }
  }

  private void UpdateUI()
  {
    this.Clear();
    this.title.text = ScriptLocalization.Get("hero_altar_uppercase_hero", true);
    DBManager.inst.DB_Legend.GetTotalUnderRecruitLegends();
    NGUITools.SetActive(this.recruitButton.gameObject, !DBManager.inst.DB_Legend.CheckLegendIsUnderRecruit(this._legendConfigId));
    Utils.GetUITextPath();
    LegendInfo legendInfo = ConfigManager.inst.DB_Legend.GetLegendInfo(this._legendConfigId);
    int power = legendInfo.power;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.legendIcon, legendInfo.Image, (System.Action<bool>) null, true, false, string.Empty);
    int index1 = 0;
    ArrayList skills = legendInfo.Skills;
    if (this._legendData != null)
    {
      for (int index2 = 0; index2 < this._legendData.Skills.Length; ++index2)
      {
        LegendSkillInfo skillInfo = ConfigManager.inst.DB_LegendSkill.GetSkillInfo(this._legendData.Skills[index2]);
        this.skills[index1].SetData(skillInfo);
        ++index1;
      }
    }
    else
    {
      foreach (object obj in skills)
      {
        int result = 0;
        if (int.TryParse(obj.ToString(), out result))
        {
          LegendSkillInfo skillInfo = ConfigManager.inst.DB_LegendSkill.GetSkillInfo(result);
          if (this._legendData == null)
            power += skillInfo.power;
          this.skills[index1].SetData(skillInfo);
          ++index1;
        }
      }
    }
    this.legendName.text = legendInfo.Loc_Name;
    int nextXp = 0;
    int num1 = 0;
    if (this._legendData != null)
    {
      num1 = (int) this._legendData.Xp;
      power = (int) this._legendData.Power;
    }
    this.legenLevel.text = ConfigManager.inst.DB_LegendPoint.GetLegendLevelByXP((long) num1, out nextXp).ToString();
    if (nextXp <= 0 && this._legendData != null)
      nextXp = (int) this._legendData.Xp;
    float num2 = 0.0f;
    if (nextXp > 0 && this._legendData != null)
      num2 = (float) this._legendData.Xp / (float) nextXp;
    this.upgradeProgressBar.value = num2;
    this.currentXp.text = num1.ToString() + "/" + nextXp.ToString();
    this.power.text = power.ToString();
    this.costSliver.text = legendInfo.cost.silver.ToString();
    if (legendInfo.cost.IsSilverEnough())
      this.costSliver.color = this.normalColor;
    else
      this.costSliver.color = Color.red;
    NGUITools.SetActive(this.infantry, legendInfo.Infantry > 0);
    NGUITools.SetActive(this.ranged, legendInfo.Ranged > 0);
    NGUITools.SetActive(this.cavalry, legendInfo.Cavalry > 0);
    NGUITools.SetActive(this.siege, legendInfo.Siege > 0);
    this.legendStroy.text = legendInfo.Loc_Desc;
    this.grid.repositionNow = true;
    this.grid.Reposition();
  }

  public class LegendRecruitDlgParameter : UI.Dialog.DialogParameter
  {
    public int legendConfigId;
  }
}
