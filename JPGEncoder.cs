﻿// Decompiled with JetBrains decompiler
// Type: JPGEncoder
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class JPGEncoder
{
  public int[] ZigZag = new int[64]
  {
    0,
    1,
    5,
    6,
    14,
    15,
    27,
    28,
    2,
    4,
    7,
    13,
    16,
    26,
    29,
    42,
    3,
    8,
    12,
    17,
    25,
    30,
    41,
    43,
    9,
    11,
    18,
    24,
    31,
    40,
    44,
    53,
    10,
    19,
    23,
    32,
    39,
    45,
    52,
    54,
    20,
    22,
    33,
    38,
    46,
    51,
    55,
    60,
    21,
    34,
    37,
    47,
    50,
    56,
    59,
    61,
    35,
    36,
    48,
    49,
    57,
    58,
    62,
    63
  };
  private int[] YTable = new int[64];
  private int[] UVTable = new int[64];
  private float[] fdtbl_Y = new float[64];
  private float[] fdtbl_UV = new float[64];
  private int[] std_dc_luminance_nrcodes = new int[17]
  {
    0,
    0,
    1,
    5,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    0,
    0,
    0,
    0,
    0,
    0
  };
  private int[] std_dc_luminance_values = new int[12]
  {
    0,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11
  };
  private int[] std_ac_luminance_nrcodes = new int[17]
  {
    0,
    0,
    2,
    1,
    3,
    3,
    2,
    4,
    3,
    5,
    5,
    4,
    4,
    0,
    0,
    1,
    125
  };
  private int[] std_ac_luminance_values = new int[162]
  {
    1,
    2,
    3,
    0,
    4,
    17,
    5,
    18,
    33,
    49,
    65,
    6,
    19,
    81,
    97,
    7,
    34,
    113,
    20,
    50,
    129,
    145,
    161,
    8,
    35,
    66,
    177,
    193,
    21,
    82,
    209,
    240,
    36,
    51,
    98,
    114,
    130,
    9,
    10,
    22,
    23,
    24,
    25,
    26,
    37,
    38,
    39,
    40,
    41,
    42,
    52,
    53,
    54,
    55,
    56,
    57,
    58,
    67,
    68,
    69,
    70,
    71,
    72,
    73,
    74,
    83,
    84,
    85,
    86,
    87,
    88,
    89,
    90,
    99,
    100,
    101,
    102,
    103,
    104,
    105,
    106,
    115,
    116,
    117,
    118,
    119,
    120,
    121,
    122,
    131,
    132,
    133,
    134,
    135,
    136,
    137,
    138,
    146,
    147,
    148,
    149,
    150,
    151,
    152,
    153,
    154,
    162,
    163,
    164,
    165,
    166,
    167,
    168,
    169,
    170,
    178,
    179,
    180,
    181,
    182,
    183,
    184,
    185,
    186,
    194,
    195,
    196,
    197,
    198,
    199,
    200,
    201,
    202,
    210,
    211,
    212,
    213,
    214,
    215,
    216,
    217,
    218,
    225,
    226,
    227,
    228,
    229,
    230,
    231,
    232,
    233,
    234,
    241,
    242,
    243,
    244,
    245,
    246,
    247,
    248,
    249,
    250
  };
  private int[] std_dc_chrominance_nrcodes = new int[17]
  {
    0,
    0,
    3,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    0,
    0,
    0,
    0
  };
  private int[] std_dc_chrominance_values = new int[12]
  {
    0,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11
  };
  private int[] std_ac_chrominance_nrcodes = new int[17]
  {
    0,
    0,
    2,
    1,
    2,
    4,
    4,
    3,
    4,
    7,
    5,
    4,
    4,
    0,
    1,
    2,
    119
  };
  private int[] std_ac_chrominance_values = new int[162]
  {
    0,
    1,
    2,
    3,
    17,
    4,
    5,
    33,
    49,
    6,
    18,
    65,
    81,
    7,
    97,
    113,
    19,
    34,
    50,
    129,
    8,
    20,
    66,
    145,
    161,
    177,
    193,
    9,
    35,
    51,
    82,
    240,
    21,
    98,
    114,
    209,
    10,
    22,
    36,
    52,
    225,
    37,
    241,
    23,
    24,
    25,
    26,
    38,
    39,
    40,
    41,
    42,
    53,
    54,
    55,
    56,
    57,
    58,
    67,
    68,
    69,
    70,
    71,
    72,
    73,
    74,
    83,
    84,
    85,
    86,
    87,
    88,
    89,
    90,
    99,
    100,
    101,
    102,
    103,
    104,
    105,
    106,
    115,
    116,
    117,
    118,
    119,
    120,
    121,
    122,
    130,
    131,
    132,
    133,
    134,
    135,
    136,
    137,
    138,
    146,
    147,
    148,
    149,
    150,
    151,
    152,
    153,
    154,
    162,
    163,
    164,
    165,
    166,
    167,
    168,
    169,
    170,
    178,
    179,
    180,
    181,
    182,
    183,
    184,
    185,
    186,
    194,
    195,
    196,
    197,
    198,
    199,
    200,
    201,
    202,
    210,
    211,
    212,
    213,
    214,
    215,
    216,
    217,
    218,
    226,
    227,
    228,
    229,
    230,
    231,
    232,
    233,
    234,
    242,
    243,
    244,
    245,
    246,
    247,
    248,
    249,
    250
  };
  private BitString[] bitcode = new BitString[(int) ushort.MaxValue];
  private int[] category = new int[(int) ushort.MaxValue];
  private int bytepos = 7;
  public ByteArray byteout = new ByteArray();
  private int[] DU = new int[64];
  private float[] YDU = new float[64];
  private float[] UDU = new float[64];
  private float[] VDU = new float[64];
  private BitString[] YDC_HT;
  private BitString[] UVDC_HT;
  private BitString[] YAC_HT;
  private BitString[] UVAC_HT;
  private int bytenew;
  public bool isDone;
  private BitmapData image;
  private int sf;

  public JPGEncoder(Texture2D texture, float quality)
  {
    this.image = new BitmapData(texture);
    if ((double) quality <= 0.0)
      quality = 1f;
    if ((double) quality > 100.0)
      quality = 100f;
    if ((double) quality < 50.0)
      this.sf = (int) (5000.0 / (double) quality);
    else
      this.sf = (int) (200.0 - (double) quality * 2.0);
  }

  private void initQuantTables(int sf)
  {
    int[] numArray1 = new int[64]
    {
      16,
      11,
      10,
      16,
      24,
      40,
      51,
      61,
      12,
      12,
      14,
      19,
      26,
      58,
      60,
      55,
      14,
      13,
      16,
      24,
      40,
      57,
      69,
      56,
      14,
      17,
      22,
      29,
      51,
      87,
      80,
      62,
      18,
      22,
      37,
      56,
      68,
      109,
      103,
      77,
      24,
      35,
      55,
      64,
      81,
      104,
      113,
      92,
      49,
      64,
      78,
      87,
      103,
      121,
      120,
      101,
      72,
      92,
      95,
      98,
      112,
      100,
      103,
      99
    };
    for (int index = 0; index < 64; ++index)
    {
      float num = Mathf.Floor((float) (((double) (numArray1[index] * sf) + 50.0) / 100.0));
      if ((double) num < 1.0)
        num = 1f;
      else if ((double) num > (double) byte.MaxValue)
        num = (float) byte.MaxValue;
      this.YTable[this.ZigZag[index]] = (int) num;
    }
    int[] numArray2 = new int[64]
    {
      17,
      18,
      24,
      47,
      99,
      99,
      99,
      99,
      18,
      21,
      26,
      66,
      99,
      99,
      99,
      99,
      24,
      26,
      56,
      99,
      99,
      99,
      99,
      99,
      47,
      66,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99,
      99
    };
    for (int index = 0; index < 64; ++index)
    {
      float num = Mathf.Floor((float) (((double) (numArray2[index] * sf) + 50.0) / 100.0));
      if ((double) num < 1.0)
        num = 1f;
      else if ((double) num > (double) byte.MaxValue)
        num = (float) byte.MaxValue;
      this.UVTable[this.ZigZag[index]] = (int) num;
    }
    float[] numArray3 = new float[8]
    {
      1f,
      1.38704f,
      1.306563f,
      1.175876f,
      1f,
      0.785695f,
      0.5411961f,
      0.2758994f
    };
    int index1 = 0;
    for (int index2 = 0; index2 < 8; ++index2)
    {
      for (int index3 = 0; index3 < 8; ++index3)
      {
        this.fdtbl_Y[index1] = (float) (1.0 / ((double) this.YTable[this.ZigZag[index1]] * (double) numArray3[index2] * (double) numArray3[index3] * 8.0));
        this.fdtbl_UV[index1] = (float) (1.0 / ((double) this.UVTable[this.ZigZag[index1]] * (double) numArray3[index2] * (double) numArray3[index3] * 8.0));
        ++index1;
      }
    }
  }

  private BitString[] computeHuffmanTbl(int[] nrcodes, int[] std_table)
  {
    int num = 0;
    int index1 = 0;
    BitString[] bitStringArray = new BitString[256];
    for (int index2 = 1; index2 <= 16; ++index2)
    {
      for (int index3 = 1; index3 <= nrcodes[index2]; ++index3)
      {
        bitStringArray[std_table[index1]] = new BitString();
        bitStringArray[std_table[index1]].val = num;
        bitStringArray[std_table[index1]].len = index2;
        ++index1;
        ++num;
      }
      num *= 2;
    }
    return bitStringArray;
  }

  private void initHuffmanTbl()
  {
    this.YDC_HT = this.computeHuffmanTbl(this.std_dc_luminance_nrcodes, this.std_dc_luminance_values);
    this.UVDC_HT = this.computeHuffmanTbl(this.std_dc_chrominance_nrcodes, this.std_dc_chrominance_values);
    this.YAC_HT = this.computeHuffmanTbl(this.std_ac_luminance_nrcodes, this.std_ac_luminance_values);
    this.UVAC_HT = this.computeHuffmanTbl(this.std_ac_chrominance_nrcodes, this.std_ac_chrominance_values);
  }

  private void initCategoryfloat()
  {
    int num1 = 1;
    int num2 = 2;
    for (int index1 = 1; index1 <= 15; ++index1)
    {
      for (int index2 = num1; index2 < num2; ++index2)
      {
        this.category[(int) short.MaxValue + index2] = index1;
        this.bitcode[(int) short.MaxValue + index2] = new BitString()
        {
          len = index1,
          val = index2
        };
      }
      for (int index2 = -(num2 - 1); index2 <= -num1; ++index2)
      {
        this.category[(int) short.MaxValue + index2] = index1;
        this.bitcode[(int) short.MaxValue + index2] = new BitString()
        {
          len = index1,
          val = num2 - 1 + index2
        };
      }
      num1 <<= 1;
      num2 <<= 1;
    }
  }

  public byte[] GetBytes()
  {
    if (this.isDone)
      return this.byteout.GetAllBytes();
    Debug.LogError((object) "JPEGEncoder not complete, cannot get bytes!");
    return new byte[1];
  }

  private void writeBits(BitString bs)
  {
    int val = bs.val;
    int num = bs.len - 1;
    while (num >= 0)
    {
      if ((val & (int) Convert.ToUInt32(1 << num)) != 0)
        this.bytenew |= (int) Convert.ToUInt32(1 << this.bytepos);
      --num;
      --this.bytepos;
      if (this.bytepos < 0)
      {
        if (this.bytenew == (int) byte.MaxValue)
        {
          this.writeByte(byte.MaxValue);
          this.writeByte((byte) 0);
        }
        else
          this.writeByte((byte) this.bytenew);
        this.bytepos = 7;
        this.bytenew = 0;
      }
    }
  }

  private void writeByte(byte value)
  {
    this.byteout.writeByte(value);
  }

  private void writeWord(int value)
  {
    this.writeByte((byte) (value >> 8 & (int) byte.MaxValue));
    this.writeByte((byte) (value & (int) byte.MaxValue));
  }

  private float[] fDCTQuant(float[] data, float[] fdtbl)
  {
    int index1 = 0;
    for (int index2 = 0; index2 < 8; ++index2)
    {
      float num1 = data[index1] + data[index1 + 7];
      float num2 = data[index1] - data[index1 + 7];
      float num3 = data[index1 + 1] + data[index1 + 6];
      float num4 = data[index1 + 1] - data[index1 + 6];
      float num5 = data[index1 + 2] + data[index1 + 5];
      float num6 = data[index1 + 2] - data[index1 + 5];
      float num7 = data[index1 + 3] + data[index1 + 4];
      float num8 = data[index1 + 3] - data[index1 + 4];
      float num9 = num1 + num7;
      float num10 = num1 - num7;
      float num11 = num3 + num5;
      float num12 = num3 - num5;
      data[index1] = num9 + num11;
      data[index1 + 4] = num9 - num11;
      float num13 = (float) (((double) num12 + (double) num10) * 0.70710676908493);
      data[index1 + 2] = num10 + num13;
      data[index1 + 6] = num10 - num13;
      float num14 = num8 + num6;
      float num15 = num6 + num4;
      float num16 = num4 + num2;
      float num17 = (float) (((double) num14 - (double) num16) * 0.382683426141739);
      float num18 = 0.5411961f * num14 + num17;
      float num19 = 1.306563f * num16 + num17;
      float num20 = num15 * 0.7071068f;
      float num21 = num2 + num20;
      float num22 = num2 - num20;
      data[index1 + 5] = num22 + num18;
      data[index1 + 3] = num22 - num18;
      data[index1 + 1] = num21 + num19;
      data[index1 + 7] = num21 - num19;
      index1 += 8;
    }
    int index3 = 0;
    for (int index2 = 0; index2 < 8; ++index2)
    {
      float num1 = data[index3] + data[index3 + 56];
      float num2 = data[index3] - data[index3 + 56];
      float num3 = data[index3 + 8] + data[index3 + 48];
      float num4 = data[index3 + 8] - data[index3 + 48];
      float num5 = data[index3 + 16] + data[index3 + 40];
      float num6 = data[index3 + 16] - data[index3 + 40];
      float num7 = data[index3 + 24] + data[index3 + 32];
      float num8 = data[index3 + 24] - data[index3 + 32];
      float num9 = num1 + num7;
      float num10 = num1 - num7;
      float num11 = num3 + num5;
      float num12 = num3 - num5;
      data[index3] = num9 + num11;
      data[index3 + 32] = num9 - num11;
      float num13 = (float) (((double) num12 + (double) num10) * 0.70710676908493);
      data[index3 + 16] = num10 + num13;
      data[index3 + 48] = num10 - num13;
      float num14 = num8 + num6;
      float num15 = num6 + num4;
      float num16 = num4 + num2;
      float num17 = (float) (((double) num14 - (double) num16) * 0.382683426141739);
      float num18 = 0.5411961f * num14 + num17;
      float num19 = 1.306563f * num16 + num17;
      float num20 = num15 * 0.7071068f;
      float num21 = num2 + num20;
      float num22 = num2 - num20;
      data[index3 + 40] = num22 + num18;
      data[index3 + 24] = num22 - num18;
      data[index3 + 8] = num21 + num19;
      data[index3 + 56] = num21 - num19;
      ++index3;
    }
    for (int index2 = 0; index2 < 64; ++index2)
      data[index2] = Mathf.Round(data[index2] * fdtbl[index2]);
    return data;
  }

  private void writeAPP0()
  {
    this.writeWord(65504);
    this.writeWord(16);
    this.writeByte((byte) 74);
    this.writeByte((byte) 70);
    this.writeByte((byte) 73);
    this.writeByte((byte) 70);
    this.writeByte((byte) 0);
    this.writeByte((byte) 1);
    this.writeByte((byte) 1);
    this.writeByte((byte) 0);
    this.writeWord(1);
    this.writeWord(1);
    this.writeByte((byte) 0);
    this.writeByte((byte) 0);
  }

  private void writeSOF0(int width, int height)
  {
    this.writeWord(65472);
    this.writeWord(17);
    this.writeByte((byte) 8);
    this.writeWord(height);
    this.writeWord(width);
    this.writeByte((byte) 3);
    this.writeByte((byte) 1);
    this.writeByte((byte) 17);
    this.writeByte((byte) 0);
    this.writeByte((byte) 2);
    this.writeByte((byte) 17);
    this.writeByte((byte) 1);
    this.writeByte((byte) 3);
    this.writeByte((byte) 17);
    this.writeByte((byte) 1);
  }

  private void writeDQT()
  {
    this.writeWord(65499);
    this.writeWord(132);
    this.writeByte((byte) 0);
    for (int index = 0; index < 64; ++index)
      this.writeByte((byte) this.YTable[index]);
    this.writeByte((byte) 1);
    for (int index = 0; index < 64; ++index)
      this.writeByte((byte) this.UVTable[index]);
  }

  private void writeDHT()
  {
    this.writeWord(65476);
    this.writeWord(418);
    this.writeByte((byte) 0);
    for (int index = 0; index < 16; ++index)
      this.writeByte((byte) this.std_dc_luminance_nrcodes[index + 1]);
    for (int index = 0; index <= 11; ++index)
      this.writeByte((byte) this.std_dc_luminance_values[index]);
    this.writeByte((byte) 16);
    for (int index = 0; index < 16; ++index)
      this.writeByte((byte) this.std_ac_luminance_nrcodes[index + 1]);
    for (int index = 0; index <= 161; ++index)
      this.writeByte((byte) this.std_ac_luminance_values[index]);
    this.writeByte((byte) 1);
    for (int index = 0; index < 16; ++index)
      this.writeByte((byte) this.std_dc_chrominance_nrcodes[index + 1]);
    for (int index = 0; index <= 11; ++index)
      this.writeByte((byte) this.std_dc_chrominance_values[index]);
    this.writeByte((byte) 17);
    for (int index = 0; index < 16; ++index)
      this.writeByte((byte) this.std_ac_chrominance_nrcodes[index + 1]);
    for (int index = 0; index <= 161; ++index)
      this.writeByte((byte) this.std_ac_chrominance_values[index]);
  }

  private void writeSOS()
  {
    this.writeWord(65498);
    this.writeWord(12);
    this.writeByte((byte) 3);
    this.writeByte((byte) 1);
    this.writeByte((byte) 0);
    this.writeByte((byte) 2);
    this.writeByte((byte) 17);
    this.writeByte((byte) 3);
    this.writeByte((byte) 17);
    this.writeByte((byte) 0);
    this.writeByte((byte) 63);
    this.writeByte((byte) 0);
  }

  private float processDU(float[] CDU, float[] fdtbl, float DC, BitString[] HTDC, BitString[] HTAC)
  {
    BitString bs1 = HTAC[0];
    BitString bs2 = HTAC[240];
    float[] numArray = this.fDCTQuant(CDU, fdtbl);
    for (int index = 0; index < 64; ++index)
      this.DU[this.ZigZag[index]] = (int) numArray[index];
    int num1 = (int) ((double) this.DU[0] - (double) DC);
    DC = (float) this.DU[0];
    if (num1 == 0)
    {
      this.writeBits(HTDC[0]);
    }
    else
    {
      this.writeBits(HTDC[this.category[(int) short.MaxValue + num1]]);
      this.writeBits(this.bitcode[(int) short.MaxValue + num1]);
    }
    int index1 = 63;
    while (index1 > 0 && this.DU[index1] == 0)
      --index1;
    if (index1 == 0)
    {
      this.writeBits(bs1);
      return DC;
    }
    for (int index2 = 1; index2 <= index1; ++index2)
    {
      int num2 = index2;
      while (this.DU[index2] == 0 && index2 <= index1)
        ++index2;
      int num3 = index2 - num2;
      if (num3 >= 16)
      {
        for (int index3 = 1; index3 <= num3 / 16; ++index3)
          this.writeBits(bs2);
        num3 &= 15;
      }
      this.writeBits(HTAC[num3 * 16 + this.category[(int) short.MaxValue + this.DU[index2]]]);
      this.writeBits(this.bitcode[(int) short.MaxValue + this.DU[index2]]);
    }
    if (index1 != 63)
      this.writeBits(bs1);
    return DC;
  }

  private void RGB2YUV(BitmapData img, int xpos, int ypos)
  {
    int index1 = 0;
    for (int index2 = 0; index2 < 8; ++index2)
    {
      for (int index3 = 0; index3 < 8; ++index3)
      {
        Color pixelColor = img.getPixelColor(xpos + index3, img.height - (ypos + index2));
        float num1 = pixelColor.r * (float) byte.MaxValue;
        float num2 = pixelColor.g * (float) byte.MaxValue;
        float num3 = pixelColor.b * (float) byte.MaxValue;
        this.YDU[index1] = (float) (0.29899999499321 * (double) num1 + 0.587000012397766 * (double) num2 + 57.0 / 500.0 * (double) num3 - 128.0);
        this.UDU[index1] = (float) (-0.168740004301071 * (double) num1 + -0.331259995698929 * (double) num2 + 0.5 * (double) num3);
        this.VDU[index1] = (float) (0.5 * (double) num1 + -0.418689996004105 * (double) num2 + -0.0813099965453148 * (double) num3);
        ++index1;
      }
    }
  }

  public void doEncoding()
  {
    this.isDone = false;
    this.initHuffmanTbl();
    this.initCategoryfloat();
    this.initQuantTables(this.sf);
    this.encode();
    this.isDone = true;
    this.image = (BitmapData) null;
  }

  private void encode()
  {
    this.byteout = new ByteArray();
    this.bytenew = 0;
    this.bytepos = 7;
    this.writeWord(65496);
    this.writeAPP0();
    this.writeDQT();
    this.writeSOF0(this.image.width, this.image.height);
    this.writeDHT();
    this.writeSOS();
    float DC1 = 0.0f;
    float DC2 = 0.0f;
    float DC3 = 0.0f;
    this.bytenew = 0;
    this.bytepos = 7;
    int ypos = 0;
    while (ypos < this.image.height)
    {
      int xpos = 0;
      while (xpos < this.image.width)
      {
        this.RGB2YUV(this.image, xpos, ypos);
        DC1 = this.processDU(this.YDU, this.fdtbl_Y, DC1, this.YDC_HT, this.YAC_HT);
        DC2 = this.processDU(this.UDU, this.fdtbl_UV, DC2, this.UVDC_HT, this.UVAC_HT);
        DC3 = this.processDU(this.VDU, this.fdtbl_UV, DC3, this.UVDC_HT, this.UVAC_HT);
        xpos += 8;
      }
      ypos += 8;
    }
    if (this.bytepos >= 0)
      this.writeBits(new BitString()
      {
        len = this.bytepos + 1,
        val = (1 << this.bytepos + 1) - 1
      });
    this.writeWord(65497);
    this.isDone = true;
  }
}
