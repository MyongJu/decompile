﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillUpgradePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonSkillUpgradePopup : Popup
{
  private bool[] itemsConfirm = new bool[3];
  private List<DragonSkillPropertyChangeItem> _allPropertyChangeItem = new List<DragonSkillPropertyChangeItem>();
  private const string RESET_SINGLE_SHOP_ID = "shopitem_dragon_skill_reset_single";
  private const string RESET_VFX_PATH = "Prefab/VFX/fx_dragon_xidian_02";
  public UILabel desLabel;
  public UILabel titleLabe;
  public UITexture iconTexture;
  public UILabel levelLabel;
  public DragonSkillRequire[] items;
  public UIButton resetSkillBtn;
  public UIButton upgradeBtn;
  public GameObject newskillUnlockGo;
  public UILabel upgradeLabel;
  public UILabel levelFullLabel;
  public GameObject fullGameobject;
  public GameObject contentGo;
  public UILabel extralLevel;
  public GameObject unlockGameobject;
  public UILabel unlockMessage;
  public ParticleSystem vfx;
  public IVfx currentScreenVfx;
  public UITable contentTable;
  public GameObject m_rootStarLevel;
  public UILabel m_labelStarLevel;
  public DragonSkillPropertyChangeItem propertyChangeItemTemplate;
  public UITable propertyChangeTable;
  public UIScrollView propertyChangeScrollView;
  public UILabel maxLevel;
  public UILabel currentLevel;
  public UILabel nextLevel;
  public GameObject rootLevelMax;
  public GameObject rootLevelNotMax;
  public DragonSkillReward currentPower;
  public DragonSkillReward nextPower;
  private DragonSkillUpgradePopup.Parameter parameter;
  private int dragonSkillGroupID;
  private ConfigDragonSkillMainInfo nextMainInfo;
  private ConfigDragonSkillMainInfo currentMainInfo;
  private ConfigDragonSkillGroupInfo skillGroupInfo;
  private DragonController controller;
  private DragonData myDragon;
  private bool levelConfirm;
  private bool darkPointConfirm;
  private bool lightPoitnConfirm;
  private bool isTopLevelofGroup;
  private int itemsAvailable;
  private int topLevelOfGroup;

  public void OnCloseButtonClick()
  {
    this.CloseSelf();
  }

  public void OnGetMoreLightPointClick()
  {
    ShopStaticInfo shopData = ConfigManager.inst.DB_Shop.GetShopData("shopitem_dragon_light_small");
    UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) new ItemUseOrBuyPopup.Parameter()
    {
      shop_id = shopData.internalId,
      type = ItemUseOrBuyPopup.Parameter.Type.UseItem,
      onUseOrBuyCallBack = new System.Action<bool, object>(this.useItemCallBack)
    });
  }

  public void OnGetMoreDarkPointClick()
  {
    ShopStaticInfo shopData = ConfigManager.inst.DB_Shop.GetShopData("shopitem_dragon_dark_small");
    UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) new ItemUseOrBuyPopup.Parameter()
    {
      shop_id = shopData.internalId,
      type = ItemUseOrBuyPopup.Parameter.Type.UseItem,
      onUseOrBuyCallBack = new System.Action<bool, object>(this.useItemCallBack)
    });
  }

  public void OnGetMoreItemsClick()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
    this.CloseSelf();
  }

  private void CloseSelf()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void useItemCallBack(bool success, object data)
  {
    this.UpdatUI();
  }

  private void Init()
  {
    this.UpdatUI();
    this.AddEventListener();
  }

  private void Dispose()
  {
    this.RemoveEventListener();
    BuilderFactory.Instance.Release((UIWidget) this.iconTexture);
    int index = 0;
    for (int length = this.items.Length; index < length; ++index)
      BuilderFactory.Instance.Release((UIWidget) this.items[index].iconTexture);
    BuilderFactory.Instance.Release((UIWidget) this.currentPower.iconTextrue);
    BuilderFactory.Instance.Release((UIWidget) this.nextPower.iconTextrue);
  }

  private void InitData()
  {
    this.myDragon = PlayerData.inst.dragonData;
    this.controller = PlayerData.inst.dragonController;
    this.dragonSkillGroupID = this.parameter.dragonSkillGroupID;
    this.skillGroupInfo = ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupInfo(this.dragonSkillGroupID);
    this.topLevelOfGroup = this.controller.GetTopLevelofGroup(this.dragonSkillGroupID, false);
    this.currentMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfoOfGrounpByLevel(this.dragonSkillGroupID, Mathf.Max(1, this.topLevelOfGroup), false);
    this.nextMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfoOfGrounpByLevel(this.dragonSkillGroupID, this.topLevelOfGroup + 1, false);
    this.isTopLevelofGroup = this.nextMainInfo == null;
    if (this.nextMainInfo != null)
    {
      int finalData1 = ConfigManager.inst.DB_BenefitCalc.GetFinalData(this.nextMainInfo.light, "calc_dragon_light_cost");
      int finalData2 = ConfigManager.inst.DB_BenefitCalc.GetFinalData(this.nextMainInfo.dark, "calc_dragon_dark_cost");
      this.levelConfirm = this.nextMainInfo.req_dragon_level <= this.myDragon.Level;
      this.lightPoitnConfirm = (long) finalData1 <= this.myDragon.LightPoint;
      this.darkPointConfirm = (long) finalData2 <= this.myDragon.DarkPoint;
      int[] numArray1 = new int[3]
      {
        this.nextMainInfo.item_id1,
        this.nextMainInfo.item_id2,
        this.nextMainInfo.item_id3
      };
      int[] numArray2 = new int[3]
      {
        this.nextMainInfo.item_count1,
        this.nextMainInfo.item_count2,
        this.nextMainInfo.item_count3
      };
      int index = 0;
      for (int length = numArray1.Length; index < length; ++index)
      {
        if (numArray2[index] > 0)
        {
          int itemCount = ItemBag.Instance.GetItemCount(numArray1[index]);
          this.itemsConfirm[index] = numArray2[index] <= itemCount;
        }
        else
          this.itemsConfirm[index] = true;
      }
    }
    this.levelLabel.text = this.currentMainInfo.level.ToString();
  }

  private bool NeedUnlockSkill
  {
    get
    {
      List<long> skillDataByGroupId = DBManager.inst.DB_DragonSkill.GetSkillDataByGroupId(this.dragonSkillGroupID);
      return (skillDataByGroupId == null || skillDataByGroupId.Count == 0) && this.skillGroupInfo.learn_method > 0;
    }
  }

  private void UpdatUI()
  {
    this.InitData();
    this.UpdateProperyChangeItems();
    this.DrawLabel();
    if (!this.isTopLevelofGroup)
      this.DrawRequire();
    this.DrawReward();
    this.ConfigBtns();
    this.contentTable.Reposition();
    this.UpdateStarLevel();
  }

  private void UpdateProperyChangeItems()
  {
    bool canLevelUp = this.nextMainInfo != null && this.nextMainInfo.level != this.currentMainInfo.level;
    if (canLevelUp)
    {
      this.currentLevel.text = this.currentMainInfo == null ? string.Empty : string.Format("{0}{1}", (object) Utils.XLAT("id_lv"), (object) this.currentMainInfo.level);
      this.nextLevel.text = this.nextMainInfo == null ? string.Empty : string.Format("{0}{1}", (object) Utils.XLAT("id_lv"), (object) this.nextMainInfo.level);
    }
    else
      this.maxLevel.text = this.currentMainInfo == null ? string.Empty : string.Format("{0}{1}", (object) Utils.XLAT("id_lv"), (object) this.currentMainInfo.level);
    this.rootLevelMax.SetActive(!canLevelUp);
    this.rootLevelNotMax.SetActive(canLevelUp);
    using (List<DragonSkillPropertyChangeItem>.Enumerator enumerator = this._allPropertyChangeItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DragonSkillPropertyChangeItem current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
        {
          current.transform.parent = (Transform) null;
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
        }
      }
    }
    this._allPropertyChangeItem.Clear();
    List<Benefits.BenefitValuePair> benefitValuePairList1 = new List<Benefits.BenefitValuePair>();
    List<Benefits.BenefitValuePair> benefitValuePairList2 = new List<Benefits.BenefitValuePair>();
    if (this.currentMainInfo != null)
    {
      using (List<Benefits.BenefitValuePair>.Enumerator enumerator = this.currentMainInfo.benefit.GetBenefitsPairs().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Benefits.BenefitValuePair current = enumerator.Current;
          benefitValuePairList1.Add(current);
          benefitValuePairList2.Add((Benefits.BenefitValuePair) null);
        }
      }
    }
    if (this.nextMainInfo != null)
    {
      using (List<Benefits.BenefitValuePair>.Enumerator enumerator = this.nextMainInfo.benefit.GetBenefitsPairs().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Benefits.BenefitValuePair current = enumerator.Current;
          bool flag = false;
          for (int index = 0; index < benefitValuePairList1.Count; ++index)
          {
            if (benefitValuePairList1[index] != null && benefitValuePairList1[index].propertyDefinition.InternalID == current.propertyDefinition.InternalID)
            {
              flag = true;
              benefitValuePairList2[index] = current;
              break;
            }
          }
          if (!flag)
          {
            benefitValuePairList1.Add((Benefits.BenefitValuePair) null);
            benefitValuePairList2.Add(current);
          }
        }
      }
    }
    for (int index = 0; index < benefitValuePairList1.Count; ++index)
    {
      GameObject go = Utils.DuplicateGOB(this.propertyChangeItemTemplate.gameObject);
      NGUITools.SetActive(go, true);
      DragonSkillPropertyChangeItem component = go.GetComponent<DragonSkillPropertyChangeItem>();
      component.SetData(benefitValuePairList1[index], benefitValuePairList2[index], canLevelUp);
      this._allPropertyChangeItem.Add(component);
    }
    this.propertyChangeTable.Reposition();
    this.propertyChangeScrollView.ResetPosition();
  }

  private void UpdateStarLevel()
  {
    this.m_rootStarLevel.SetActive(false);
    if (PlayerData.inst.dragonData == null)
      return;
    int skillGroupStarLevel = PlayerData.inst.dragonData.GetSkillGroupStarLevel(this.skillGroupInfo.internalId);
    if (skillGroupStarLevel <= 0)
      return;
    this.m_rootStarLevel.SetActive(true);
    this.m_labelStarLevel.text = skillGroupStarLevel.ToString();
  }

  private void DrawLabel()
  {
    this.titleLabe.text = Utils.XLAT(this.skillGroupInfo.localization_name);
    this.desLabel.text = Utils.XLAT(this.skillGroupInfo.localization_description);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.iconTexture, this.skillGroupInfo.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    int level = this.currentMainInfo.level;
    int extarLevel = DBManager.inst.DB_Artifact.GetExtarLevel(PlayerData.inst.uid, this.skillGroupInfo.internalId);
    this.extralLevel.text = string.Empty;
    bool flag = false;
    if (extarLevel > 0)
    {
      this.extralLevel.text = "+" + (object) extarLevel;
      int num = level + 1;
      flag = true;
    }
    if (this.isTopLevelofGroup)
    {
      this.fullGameobject.SetActive(true);
      this.contentGo.SetActive(false);
      this.levelFullLabel.text = Utils.XLAT("dragon_skill_upgrade_max_level_description");
    }
    else
    {
      this.fullGameobject.SetActive(false);
      this.contentGo.SetActive(true);
    }
  }

  private void DrawRequire()
  {
    int finalData1 = ConfigManager.inst.DB_BenefitCalc.GetFinalData(this.nextMainInfo.light, "calc_dragon_light_cost");
    int finalData2 = ConfigManager.inst.DB_BenefitCalc.GetFinalData(this.nextMainInfo.dark, "calc_dragon_dark_cost");
    int[] numArray1 = new int[3]
    {
      this.nextMainInfo.item_id1,
      this.nextMainInfo.item_id2,
      this.nextMainInfo.item_id3
    };
    int[] numArray2 = new int[3]
    {
      this.nextMainInfo.item_count1,
      this.nextMainInfo.item_count2,
      this.nextMainInfo.item_count3
    };
    int num1 = 0;
    DragonSkillRequire[] items1 = this.items;
    int index1 = num1;
    int num2 = index1 + 1;
    DragonSkillRequire dragonSkillRequire1 = items1[index1];
    BuilderFactory.Instance.HandyBuild((UIWidget) dragonSkillRequire1.iconTexture, DragonUtils.GetDragonIconPath(0, this.nextMainInfo.req_dragon_level), (System.Action<bool>) null, true, false, string.Empty);
    dragonSkillRequire1.contentLabel.text = string.Format(Utils.XLAT("dragon_dragon_lv_num"), (object) this.nextMainInfo.req_dragon_level);
    NGUITools.SetActive(dragonSkillRequire1.confirmGameobjcet, this.levelConfirm);
    NGUITools.SetActive(dragonSkillRequire1.unconfirmGameobject, !this.levelConfirm);
    NGUITools.SetActive(dragonSkillRequire1.getMoreButton.gameObject, false);
    int index2 = 0;
    for (int length = numArray1.Length; index2 < length; ++index2)
    {
      if (num2 >= this.items.Length)
        return;
      DragonSkillRequire dragonSkillRequire2 = this.items[num2++];
      NGUITools.SetActive(dragonSkillRequire2.gameObject, numArray2[index2] > 0);
      if (numArray2[index2] > 0)
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(numArray1[index2]);
        BuilderFactory.Instance.HandyBuild((UIWidget) dragonSkillRequire2.iconTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
        int itemCount = ItemBag.Instance.GetItemCount(numArray1[index2]);
        dragonSkillRequire2.contentLabel.text = this.ConfigColor(this.itemsConfirm[index2], (long) itemCount) + "/" + (object) numArray2[index2];
        NGUITools.SetActive(dragonSkillRequire2.confirmGameobjcet, this.itemsConfirm[index2]);
        NGUITools.SetActive(dragonSkillRequire2.unconfirmGameobject, !this.itemsConfirm[index2]);
        NGUITools.SetActive(dragonSkillRequire2.getMoreButton.gameObject, !this.itemsConfirm[index2]);
        dragonSkillRequire2.getMoreButton.onClick.Clear();
        dragonSkillRequire2.getMoreButton.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnGetMoreItemsClick)));
      }
    }
    if (num2 >= this.items.Length)
      return;
    DragonSkillRequire[] items2 = this.items;
    int index3 = num2;
    int num3 = index3 + 1;
    DragonSkillRequire dragonSkillRequire3 = items2[index3];
    NGUITools.SetActive(dragonSkillRequire3.gameObject, finalData1 > 0);
    if (finalData1 > 0)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) dragonSkillRequire3.iconTexture, "Texture/Dragon/icon_dragon_power_light", (System.Action<bool>) null, true, false, string.Empty);
      dragonSkillRequire3.contentLabel.text = this.ConfigColor(this.lightPoitnConfirm, this.myDragon.LightPoint) + "/" + (object) finalData1;
      NGUITools.SetActive(dragonSkillRequire3.confirmGameobjcet, this.lightPoitnConfirm);
      NGUITools.SetActive(dragonSkillRequire3.unconfirmGameobject, !this.lightPoitnConfirm);
      NGUITools.SetActive(dragonSkillRequire3.getMoreButton.gameObject, !this.lightPoitnConfirm);
      dragonSkillRequire3.getMoreButton.onClick.Clear();
      dragonSkillRequire3.getMoreButton.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnGetMoreLightPointClick)));
    }
    if (num3 >= this.items.Length)
      return;
    DragonSkillRequire[] items3 = this.items;
    int index4 = num3;
    int num4 = index4 + 1;
    DragonSkillRequire dragonSkillRequire4 = items3[index4];
    NGUITools.SetActive(dragonSkillRequire4.gameObject, finalData2 > 0);
    if (finalData2 <= 0)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) dragonSkillRequire4.iconTexture, "Texture/Dragon/icon_dragon_power_dark", (System.Action<bool>) null, true, false, string.Empty);
    dragonSkillRequire4.contentLabel.text = this.ConfigColor(this.darkPointConfirm, this.myDragon.DarkPoint) + "/" + (object) finalData2;
    NGUITools.SetActive(dragonSkillRequire4.confirmGameobjcet, this.darkPointConfirm);
    NGUITools.SetActive(dragonSkillRequire4.unconfirmGameobject, !this.darkPointConfirm);
    NGUITools.SetActive(dragonSkillRequire4.getMoreButton.gameObject, !this.darkPointConfirm);
    dragonSkillRequire4.getMoreButton.onClick.Clear();
    dragonSkillRequire4.getMoreButton.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnGetMoreDarkPointClick)));
  }

  private string ConfigColor(bool comfirm, long source)
  {
    if (!comfirm)
      return "[FF0000FF]" + (object) source + "[-]";
    return source.ToString();
  }

  private void DrawReward()
  {
    if (this.currentMainInfo != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.currentPower.iconTextrue, "Texture/Reward/icon_power", (System.Action<bool>) null, true, false, string.Empty);
      this.currentPower.contentLabel.text = Utils.XLAT("dragon_skill_total_bonus_name");
      this.currentPower.valueLabel.text = this.currentMainInfo.power.ToString();
    }
    if (this.nextMainInfo != null && this.currentMainInfo.level != this.nextMainInfo.level)
    {
      this.nextPower.gameObject.SetActive(true);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.nextPower.iconTextrue, "Texture/Reward/icon_power", (System.Action<bool>) null, true, false, string.Empty);
      this.nextPower.contentLabel.text = Utils.XLAT("dragon_skill_next_rank_name");
      this.nextPower.valueLabel.text = this.nextMainInfo.power.ToString();
    }
    else
      this.nextPower.gameObject.SetActive(false);
  }

  private void ConfigBtns()
  {
    this.upgradeBtn.gameObject.SetActive(true);
    this.resetSkillBtn.gameObject.SetActive(true);
    this.upgradeBtn.isEnabled = this.levelConfirm && this.itemsConfirm[0] && (this.itemsConfirm[1] && this.itemsConfirm[2]) && (this.lightPoitnConfirm && this.darkPointConfirm) && !this.isTopLevelofGroup;
    NGUITools.SetActive(this.newskillUnlockGo, this.NeedUnlockSkill);
    this.upgradeLabel.text = !this.NeedUnlockSkill ? Utils.XLAT("dragon_skill_upgrade_button") : Utils.XLAT("id_uppercase_unlock");
    this.resetSkillBtn.gameObject.SetActive(true);
    this.resetSkillBtn.isEnabled = this.topLevelOfGroup > 1;
    this.unlockGameobject.SetActive(false);
    this.unlockMessage.gameObject.SetActive(false);
  }

  public void OnResetBtnClicked()
  {
    Hashtable hashtable = Utils.Hash((object) "group_id", (object) this.dragonSkillGroupID);
    ShopStaticInfo shopData = ConfigManager.inst.DB_Shop.GetShopData("shopitem_dragon_skill_reset_single");
    UIManager.inst.OpenPopup("UseOrBuyAndUseItemPopup", (Popup.PopupParameter) new UseOrBuyAndUseItemPopup.Parameter()
    {
      titleText = Utils.XLAT("dragon_skill_reset_skill_title"),
      btnText = Utils.XLAT("dragon_skill_reset_skill_button"),
      param = hashtable,
      itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(shopData.Item_InternalId),
      callback = new System.Action<bool, object>(this.OnHandlePayload),
      goldNotEnoughCallback = new System.Action(this.OnGoldNotEnough)
    });
  }

  private void OnGoldNotEnough()
  {
    this.CloseSelf();
  }

  private void OnHandlePayload(bool ret, object data)
  {
    if (!ret)
      return;
    AudioManager.Instance.PlaySound("sfx_city_dragon_skill_reset_all", false);
    this.CloseSelf();
  }

  public void AddScreenVfx()
  {
    if (this.currentScreenVfx == null)
      this.currentScreenVfx = VfxManager.Instance.Create("Prefab/VFX/fx_dragon_xidian_02");
    this.currentScreenVfx.Play(UIManager.inst.ui2DCamera.transform);
  }

  private void AddEventListener()
  {
    this.upgradeBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnUpgradeBtnClicked)));
    this.resetSkillBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnResetBtnClicked)));
  }

  private void RemoveEventListener()
  {
    this.upgradeBtn.onClick.Clear();
    this.resetSkillBtn.onClick.Clear();
  }

  private void OnUpgradeBtnClicked()
  {
    PlayerData.inst.dragonController.UpgradeSkill(this.nextMainInfo.internalId, (System.Action<object>) (obj =>
    {
      this.vfx.Play();
      AudioManager.Instance.PlaySound("sfx_city_dragon_skill_upgrade", false);
      this.UpdatUI();
    }));
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.propertyChangeItemTemplate.gameObject.SetActive(false);
    this.parameter = orgParam as DragonSkillUpgradePopup.Parameter;
    this.Init();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Dispose();
  }

  public class Parameter : Popup.PopupParameter
  {
    public int dragonSkillGroupID;
  }
}
