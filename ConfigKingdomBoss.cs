﻿// Decompiled with JetBrains decompiler
// Type: ConfigKingdomBoss
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigKingdomBoss
{
  private ConfigParse parse = new ConfigParse();
  public Dictionary<string, KingdomBossStaticInfo> datas;
  private Dictionary<int, KingdomBossStaticInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<KingdomBossStaticInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, KingdomBossStaticInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, KingdomBossStaticInfo>) null;
  }

  public bool Contains(int internalId)
  {
    return this.dicByUniqueId.ContainsKey(internalId);
  }

  public bool Contains(string id)
  {
    return this.datas.ContainsKey(id);
  }

  public KingdomBossStaticInfo GetItem(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (KingdomBossStaticInfo) null;
  }

  public KingdomBossStaticInfo GetItem(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (KingdomBossStaticInfo) null;
  }
}
