﻿// Decompiled with JetBrains decompiler
// Type: UISliderColors
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Examples/Slider Colors")]
public class UISliderColors : MonoBehaviour
{
  public Color[] colors = new Color[3]
  {
    Color.red,
    Color.yellow,
    Color.green
  };
  public UISprite sprite;
  private UIProgressBar mBar;
  private UIBasicSprite mSprite;

  private void Start()
  {
    this.mBar = this.GetComponent<UIProgressBar>();
    this.mSprite = this.GetComponent<UIBasicSprite>();
    this.Update();
  }

  private void Update()
  {
    if ((Object) this.sprite == (Object) null || this.colors.Length == 0)
      return;
    float f = (!((Object) this.mBar != (Object) null) ? this.mSprite.fillAmount : this.mBar.value) * (float) (this.colors.Length - 1);
    int index = Mathf.FloorToInt(f);
    Color color = this.colors[0];
    if (index >= 0)
    {
      if (index + 1 < this.colors.Length)
      {
        float t = f - (float) index;
        color = Color.Lerp(this.colors[index], this.colors[index + 1], t);
      }
      else
        color = index >= this.colors.Length ? this.colors[this.colors.Length - 1] : this.colors[index];
    }
    color.a = this.sprite.color.a;
    this.sprite.color = color;
  }
}
