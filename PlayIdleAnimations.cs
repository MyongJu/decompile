﻿// Decompiled with JetBrains decompiler
// Type: PlayIdleAnimations
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("NGUI/Examples/Play Idle Animations")]
public class PlayIdleAnimations : MonoBehaviour
{
  private List<AnimationClip> mBreaks = new List<AnimationClip>();
  private UnityEngine.Animation mAnim;
  private AnimationClip mIdle;
  private float mNextBreak;
  private int mLastIndex;

  private void Start()
  {
    this.mAnim = this.GetComponentInChildren<UnityEngine.Animation>();
    if ((UnityEngine.Object) this.mAnim == (UnityEngine.Object) null)
    {
      Debug.LogWarning((object) (NGUITools.GetHierarchy(this.gameObject) + " has no Animation component"));
      UnityEngine.Object.Destroy((UnityEngine.Object) this);
    }
    else
    {
      IEnumerator enumerator = this.mAnim.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
        {
          AnimationState current = (AnimationState) enumerator.Current;
          if (current.clip.name == "idle")
          {
            current.layer = 0;
            this.mIdle = current.clip;
            this.mAnim.Play(this.mIdle.name);
          }
          else if (current.clip.name.StartsWith("idle"))
          {
            current.layer = 1;
            this.mBreaks.Add(current.clip);
          }
        }
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
      if (this.mBreaks.Count != 0)
        return;
      UnityEngine.Object.Destroy((UnityEngine.Object) this);
    }
  }

  private void Update()
  {
    if ((double) this.mNextBreak >= (double) Time.time)
      return;
    if (this.mBreaks.Count == 1)
    {
      AnimationClip mBreak = this.mBreaks[0];
      this.mNextBreak = Time.time + mBreak.length + UnityEngine.Random.Range(5f, 15f);
      this.mAnim.CrossFade(mBreak.name);
    }
    else
    {
      int index = UnityEngine.Random.Range(0, this.mBreaks.Count - 1);
      if (this.mLastIndex == index)
      {
        ++index;
        if (index >= this.mBreaks.Count)
          index = 0;
      }
      this.mLastIndex = index;
      AnimationClip mBreak = this.mBreaks[index];
      this.mNextBreak = Time.time + mBreak.length + UnityEngine.Random.Range(2f, 8f);
      this.mAnim.CrossFade(mBreak.name);
    }
  }
}
