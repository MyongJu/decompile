﻿// Decompiled with JetBrains decompiler
// Type: ActivityNotStartPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;

public class ActivityNotStartPopup : Popup
{
  public ActivityItemRenderer item;
  public Icon[] icons;
  public UILabel title;
  public UILabel desc;
  public UITexture moreItems;
  private ActivityBaseUIData baseData;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.baseData = (orgParam as ActivityNotStartPopup.Parameter).baseData;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    RoundActivityUIData baseData = this.baseData as RoundActivityUIData;
    this.title.text = baseData.GetName();
    this.desc.text = baseData.GetNormalDesc();
    this.item.FeedData((IComponentData) this.baseData.GetUnstartIconData());
    BuilderFactory.Instance.HandyBuild((UIWidget) this.moreItems, Utils.GetUITextPath() + "item_mystery_box", (System.Action<bool>) null, true, false, string.Empty);
    RoundActivityData data = baseData.Data as RoundActivityData;
    List<int> totalRankTopOneItems = data.TotalRankTopOneItems;
    Dictionary<int, int> topOneRewardItems = data.TotalRankTopOneRewardItems;
    for (int index = 0; index < totalRankTopOneItems.Count && index < this.icons.Length; ++index)
    {
      this.icons[index].FeedData((IComponentData) this.GetIconData(totalRankTopOneItems[index], topOneRewardItems[totalRankTopOneItems[index]]));
      this.icons[index].OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
      this.icons[index].OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
      this.icons[index].OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
    }
    this.RemoveEventHandler();
    this.AddEventHandler();
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  private IconData GetItemIconDatai(int itemId)
  {
    IconData iconData = new IconData();
    iconData.image = Utils.GetUITextPath() + "gatherspeed";
    iconData.contents = new string[2];
    iconData.contents[0] = "Test";
    iconData.contents[1] = "Test";
    iconData.Data = (object) itemId;
    return iconData;
  }

  private void OnStartActivityHandler()
  {
    this.OnCloseHandler();
  }

  private IconData GetIconData(int item_id, int count)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(item_id);
    IconData iconData = new IconData();
    iconData.image = itemStaticInfo.ImagePath;
    iconData.contents = new string[2];
    iconData.contents[0] = itemStaticInfo.LocName;
    iconData.Data = (object) item_id;
    iconData.contents[1] = !(this.baseData.Data as RoundActivityData).IsOpenRanking ? string.Empty : "X " + count.ToString();
    return iconData;
  }

  private void AddEventHandler()
  {
    ActivityManager.Intance.OnTimeLimitActivityStartNewRound += new System.Action(this.OnCloseHandler);
  }

  private void RemoveEventHandler()
  {
    ActivityManager.Intance.OnTimeLimitActivityStartNewRound -= new System.Action(this.OnCloseHandler);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.item.Dispose();
    this.RemoveEventHandler();
    BuilderFactory.Instance.Release((UIWidget) this.moreItems);
    this.baseData = (ActivityBaseUIData) null;
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public ActivityBaseUIData baseData;
  }
}
