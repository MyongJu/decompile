﻿// Decompiled with JetBrains decompiler
// Type: DicePointsBox
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class DicePointsBox
{
  private string _id;
  private int _level;
  private int _score;
  private int _selectedIndex;
  private List<Reward.RewardsValuePair> _rewards;
  private DicePointsBox.BoxState _state;
  private DiceScoreBoxInfo _configInfo;

  public DicePointsBox(DiceScoreBoxInfo info)
  {
    this._configInfo = info;
    this._id = info.id;
    this._level = info.level;
    this._score = info.score;
    this._state = DicePointsBox.BoxState.disabled;
    this._rewards = info.Rewards;
    this._selectedIndex = 0;
  }

  public int SelectedIndex
  {
    set
    {
      this._selectedIndex = value;
    }
    get
    {
      return this._selectedIndex;
    }
  }

  public int Score
  {
    get
    {
      return this._score;
    }
  }

  public int Level
  {
    get
    {
      return this._level;
    }
  }

  public List<Reward.RewardsValuePair> Rewards
  {
    get
    {
      return this._rewards;
    }
  }

  public DicePointsBox.BoxState State
  {
    get
    {
      return this._state;
    }
  }

  public void setState(long score)
  {
    Dictionary<int, int> scoreStatusDict = DicePayload.Instance.TempDiceData.ScoreStatusDict;
    if (scoreStatusDict.ContainsKey(this._configInfo.level - 1))
      this._selectedIndex = scoreStatusDict[this._configInfo.level - 1];
    if ((long) this._configInfo.score > score)
      this._state = DicePointsBox.BoxState.disabled;
    else if (this._selectedIndex > 0)
      this._state = DicePointsBox.BoxState.opened;
    else
      this._state = DicePointsBox.BoxState.active;
  }

  public enum BoxState
  {
    opened,
    disabled,
    active,
  }
}
