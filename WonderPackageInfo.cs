﻿// Decompiled with JetBrains decompiler
// Type: WonderPackageInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class WonderPackageInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "sort")]
  public int sort;
  [Config(Name = "max_number")]
  public int maxNumber;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "Rewards")]
  public Reward reward;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "icon")]
  public string icon;
}
