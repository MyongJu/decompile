﻿// Decompiled with JetBrains decompiler
// Type: PopupTileInfoMiniWonder
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PopupTileInfoMiniWonder : UI.Dialog
{
  private List<BenefitItemRenderer> m_ForPlayerList = new List<BenefitItemRenderer>();
  private List<BenefitItemRenderer> m_ForAllianceList = new List<BenefitItemRenderer>();
  public GameObject m_PlayerPanel;
  public UILabel m_MiniWonderName;
  public UILabel m_Detail;
  public UILabel m_Qualification;
  public UILabel m_AllianceName;
  public UILabel m_UserName;
  public AllianceSymbol m_AllianceSymbol;
  public UIButton m_ProfileButton;
  public UIButton m_AttackButton;
  public UIButton m_ScoutButton;
  public UIButton m_RecallButton;
  public UISprite m_MiniWonderIcon;
  public UIGrid m_Grid;
  public UIScrollView m_ScrollView;
  public UITable m_Benefit;
  public UITable m_BenefitForPlayer;
  public UITable m_BenefitForAlliance;
  public GameObject m_BenefitRenderer;
  private TileData m_TileData;

  public void OnCloseClicked()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnScout()
  {
    PVPSystem.Instance.Controller.OpenScoutDlg(this.m_TileData.Location);
  }

  public void OnAttack()
  {
    PVPSystem.Instance.Controller.AttackMiniWonder(this.m_TileData.Location);
  }

  public void OnRecall()
  {
    GameEngine.Instance.marchSystem.Recall(this.m_TileData.MarchID);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnProfile()
  {
    if (this.m_TileData.OwnerID == PlayerData.inst.uid)
      UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    else
      UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
      {
        uid = this.m_TileData.OwnerID
      }, 1 != 0, 1 != 0, 1 != 0);
  }

  private void UpdateBenefitPanel(MiniWonderData miniWonderData)
  {
    this.UpdateBenefitForPlayer(miniWonderData);
    this.UpdateBenefitForAlliance(miniWonderData);
    Utils.ExecuteAtTheEndOfFrame((System.Action) (() =>
    {
      this.m_BenefitForPlayer.Reposition();
      this.m_BenefitForAlliance.Reposition();
      this.m_Benefit.Reposition();
      this.m_ScrollView.ResetPosition();
    }));
  }

  private void UpdateBenefitForPlayer(MiniWonderData miniWonderData)
  {
    using (List<BenefitItemRenderer>.Enumerator enumerator = this.m_ForPlayerList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BenefitItemRenderer current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ForPlayerList.Clear();
    int num1 = 0;
    for (int index1 = 0; index1 < miniWonderData.BenefitId.Length; ++index1)
    {
      int index2 = miniWonderData.BenefitId[index1];
      double num2 = miniWonderData.BenefitValue[index1];
      PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[index2];
      if (dbProperty != null)
      {
        ++num1;
        BenefitItemRenderer component = this.InstantiateBenefitRenderer(this.m_BenefitForPlayer.transform).GetComponent<BenefitItemRenderer>();
        this.m_ForPlayerList.Add(component);
        component.SetData(num2, dbProperty, dbProperty.IsNegative, BenefitItemRenderer.DESC_COLOR, BenefitItemRenderer.DESC_COLOR, BenefitItemRenderer.DESC_COLOR);
      }
    }
    this.m_BenefitForPlayer.gameObject.SetActive(num1 > 0);
  }

  private void UpdateBenefitForAlliance(MiniWonderData miniWonderData)
  {
    using (List<BenefitItemRenderer>.Enumerator enumerator = this.m_ForAllianceList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BenefitItemRenderer current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ForAllianceList.Clear();
    int num1 = 0;
    for (int index1 = 0; index1 < miniWonderData.AllianceBenefitId.Length; ++index1)
    {
      int index2 = miniWonderData.AllianceBenefitId[index1];
      double num2 = miniWonderData.AllianceBenefitValue[index1];
      PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[index2];
      if (dbProperty != null)
      {
        ++num1;
        BenefitItemRenderer component = this.InstantiateBenefitRenderer(this.m_BenefitForAlliance.transform).GetComponent<BenefitItemRenderer>();
        this.m_ForAllianceList.Add(component);
        component.SetData(num2, dbProperty, dbProperty.IsNegative, BenefitItemRenderer.DESC_COLOR, BenefitItemRenderer.DESC_COLOR, BenefitItemRenderer.DESC_COLOR);
      }
    }
    this.m_BenefitForAlliance.gameObject.SetActive(num1 > 0);
  }

  private GameObject InstantiateBenefitRenderer(Transform parent)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_BenefitRenderer);
    gameObject.SetActive(true);
    gameObject.transform.parent = parent;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
    return gameObject;
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_TileData = (orgParam as PopupTileInfoMiniWonder.Parameter).tileData;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    MiniWonderData data = ConfigManager.inst.DB_MiniWonder.GetData(this.m_TileData.ResourceId);
    bool flag1 = data.MinCityLevel <= PlayerData.inst.playerCityData.level && PlayerData.inst.playerCityData.level <= data.MaxCityLevel;
    this.m_AttackButton.isEnabled = flag1;
    this.m_ScoutButton.isEnabled = flag1;
    this.m_RecallButton.isEnabled = flag1;
    this.m_Qualification.gameObject.SetActive(!flag1);
    this.m_Qualification.text = string.Format(Utils.XLAT("miniwonder_lv1_requirement"), (object) data.MinCityLevel, (object) data.MaxCityLevel);
    this.m_Detail.text = ScriptLocalization.Get(data.ID.Substring(0, 14) + "_description", true);
    this.m_MiniWonderName.text = ScriptLocalization.Get(data.ID + "_name", true);
    this.m_MiniWonderIcon.spriteName = data.Prefab;
    if (this.m_TileData.OwnerID == 0L)
    {
      this.m_Detail.gameObject.SetActive(true);
      this.m_ProfileButton.gameObject.SetActive(false);
      this.m_PlayerPanel.SetActive(false);
      this.m_AttackButton.gameObject.SetActive(true);
      this.m_ScoutButton.gameObject.SetActive(false);
      this.m_RecallButton.gameObject.SetActive(false);
    }
    else
    {
      this.m_Detail.gameObject.SetActive(false);
      this.m_ProfileButton.gameObject.SetActive(true);
      this.m_PlayerPanel.SetActive(true);
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(this.m_TileData.AllianceID);
      UserData userData = DBManager.inst.DB_User.Get(this.m_TileData.OwnerID);
      if (allianceData != null)
      {
        this.m_AllianceName.text = string.Format("[{0}]{1}", (object) allianceData.allianceAcronym, (object) allianceData.allianceName);
        this.m_AllianceSymbol.gameObject.SetActive(true);
        this.m_AllianceSymbol.SetSymbols(allianceData.allianceSymbolCode);
      }
      else
      {
        this.m_AllianceName.text = "NO ALLIANCE";
        this.m_AllianceSymbol.gameObject.SetActive(false);
      }
      this.m_UserName.text = userData == null ? string.Empty : userData.userName;
      if (this.m_TileData.AllianceID != PlayerData.inst.allianceId)
      {
        this.m_AttackButton.gameObject.SetActive(true);
        this.m_ScoutButton.gameObject.SetActive(true);
        this.m_RecallButton.gameObject.SetActive(false);
      }
      else
      {
        bool flag2 = this.m_TileData.OwnerID == PlayerData.inst.uid;
        if (PlayerData.inst.allianceId != 0L)
        {
          this.m_AttackButton.gameObject.SetActive(false);
          this.m_ScoutButton.gameObject.SetActive(false);
        }
        else
        {
          this.m_AttackButton.gameObject.SetActive(!flag2);
          this.m_ScoutButton.gameObject.SetActive(!flag2);
        }
        this.m_RecallButton.gameObject.SetActive(flag2);
      }
    }
    this.m_Grid.Reposition();
    this.UpdateBenefitPanel(data);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public TileData tileData;
  }
}
