﻿// Decompiled with JetBrains decompiler
// Type: BarracksManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BarracksManager
{
  private static BarracksManager inst;
  private BarracksDisplayState currentDisplayState;
  private GameObject flySfx;
  private bool isUpgradeTroopOpen;

  public static BarracksManager Instance
  {
    get
    {
      if (BarracksManager.inst == null)
        BarracksManager.inst = new BarracksManager();
      return BarracksManager.inst;
    }
  }

  public BarracksDisplayState CurrentDisplayState
  {
    get
    {
      return this.currentDisplayState;
    }
    set
    {
      this.currentDisplayState = value;
    }
  }

  public bool IsUpgradeTroopOpen
  {
    get
    {
      return this.isUpgradeTroopOpen;
    }
    set
    {
      this.isUpgradeTroopOpen = value;
    }
  }

  public void ChangeDisplayState()
  {
    if (this.currentDisplayState == BarracksDisplayState.ZERO)
      this.currentDisplayState = BarracksDisplayState.ONE;
    else
      this.currentDisplayState = BarracksDisplayState.ZERO;
  }

  public bool IsUnitUnlocked(int internalID)
  {
    Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(internalID);
    bool flag1 = false;
    bool flag2 = false;
    if (data.Requirement_ID_1.Length > 0)
    {
      string type = ConfigManager.inst.DB_Building.GetData(int.Parse(data.Requirement_ID_1)).Type;
      if ((double) CityManager.inst.GetHighestBuildingLevelFor(type) < data.Requirement_Value_1)
        flag2 = true;
    }
    if (data.Requirement_ID_2.Length > 0)
    {
      TechLevel techLevel = ResearchManager.inst.GetTechLevel(int.Parse(data.Requirement_ID_2));
      if (ResearchManager.inst.Rank(techLevel.Tech) < techLevel.Level)
        flag1 = true;
    }
    if (!flag2)
      return !flag1;
    return false;
  }

  public BarracksManager.BlockedType GetBlockedType(int internalID)
  {
    Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(internalID);
    bool flag1 = false;
    bool flag2 = false;
    if (data.Requirement_ID_1.Length > 0)
    {
      string type = ConfigManager.inst.DB_Building.GetData(int.Parse(data.Requirement_ID_1)).Type;
      if ((double) CityManager.inst.GetHighestBuildingLevelFor(type) < data.Requirement_Value_1)
        flag2 = true;
    }
    if (data.Requirement_ID_2.Length > 0)
    {
      TechLevel techLevel = ResearchManager.inst.GetTechLevel(int.Parse(data.Requirement_ID_2));
      if (ResearchManager.inst.Rank(techLevel.Tech) < techLevel.Level)
        flag1 = true;
    }
    if (flag1 && flag2)
      return BarracksManager.BlockedType.ALL;
    if (flag1)
      return BarracksManager.BlockedType.RESEARCH;
    return flag2 ? BarracksManager.BlockedType.BUILDING : BarracksManager.BlockedType.NONE;
  }

  public bool IsUnitTraining(long buildingID)
  {
    return this.GetJobHandle(buildingID) != null;
  }

  public bool IsWaitingCollection(long buildingID)
  {
    return CityManager.inst.GetBuildingFromID(buildingID).mInBuilding;
  }

  public bool IsBuildingUpgrading(long buildingID)
  {
    return JobManager.Instance.GetJob(CityManager.inst.GetBuildingFromID(buildingID).mBuildingJobID) != null;
  }

  public JobHandle GetJobHandle(long buildingID)
  {
    return JobManager.Instance.GetJob(CityManager.inst.GetBuildingFromID(buildingID).mTrainingJobID) ?? (JobHandle) null;
  }

  public Unit_StatisticsInfo GetCurrentTrainningUnit(long buildingID)
  {
    JobHandle unfinishedJob = JobManager.Instance.GetUnfinishedJob(CityManager.inst.GetBuildingFromID(buildingID).mTrainingJobID);
    if (unfinishedJob != null && unfinishedJob.Data != null)
      return ConfigManager.inst.DB_Unit_Statistics.GetData((unfinishedJob.Data as Hashtable)[(object) "troop_class"].ToString());
    return (Unit_StatisticsInfo) null;
  }

  public string HighestUnitIDCanTrainOfUnitType(string unitType)
  {
    string str = unitType + "_t1";
    for (int index = 1; index <= 10; ++index)
    {
      string uniqueId = unitType + "_t" + (object) index;
      Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(uniqueId);
      if (data != null && this.IsUnitUnlocked(data.internalId))
        str = uniqueId;
    }
    return str;
  }

  public string HighestUnitIDCanTrainOfBuildingType(string buildingType)
  {
    List<Unit_StatisticsInfo> allUnitsCanTrain = this.GetAllUnitsCanTrain(buildingType);
    for (int index = allUnitsCanTrain.Count - 1; index > 0; --index)
    {
      if (this.IsUnitUnlocked(allUnitsCanTrain[index].internalId))
        return allUnitsCanTrain[index].ID;
    }
    return allUnitsCanTrain[0].ID;
  }

  public Unit_StatisticsInfo GetMaxLevelUnitCanTrain(string buildingType)
  {
    Unit_StatisticsInfo unitStatisticsInfo = (Unit_StatisticsInfo) null;
    List<Unit_StatisticsInfo> allUnitsCanTrain = this.GetAllUnitsCanTrain(buildingType);
    if (allUnitsCanTrain.Count > 0)
      unitStatisticsInfo = allUnitsCanTrain[allUnitsCanTrain.Count - 1];
    return unitStatisticsInfo;
  }

  public Unit_StatisticsInfo GetNextLevelUnitCanTrain(string buildingType, int internalId)
  {
    Unit_StatisticsInfo unitStatisticsInfo = (Unit_StatisticsInfo) null;
    List<Unit_StatisticsInfo> allUnitsCanTrain = this.GetAllUnitsCanTrain(buildingType);
    for (int index = 0; index < allUnitsCanTrain.Count; ++index)
    {
      if (allUnitsCanTrain[index].internalId == internalId && index < allUnitsCanTrain.Count - 1)
      {
        unitStatisticsInfo = allUnitsCanTrain[index + 1];
        break;
      }
    }
    return unitStatisticsInfo;
  }

  public BuildingInfo GetRequireBuildingInfo(Unit_StatisticsInfo TSC)
  {
    if (TSC.Requirement_ID_1.Length > 0)
      return ConfigManager.inst.DB_Building.GetData(int.Parse(TSC.Requirement_ID_1));
    return (BuildingInfo) null;
  }

  public void CollectBuilding(long buildingID, System.Action callBack = null)
  {
    CityMapData cityMapData = DBManager.inst.DB_CityMap.Get(new CityMapKey()
    {
      uid = PlayerData.inst.uid,
      cityId = (long) PlayerData.inst.cityId,
      buildingId = buildingID
    });
    int tmpTroopCount = int.Parse(cityMapData.inBuildingData[0].value);
    Unit_StatisticsInfo unit = ConfigManager.inst.DB_Unit_Statistics.GetData(cityMapData.inBuildingData[0].type);
    CityManager.BuildingItem buildingFromId = CityManager.inst.GetBuildingFromID(buildingID);
    BuildingInfo bi = ConfigManager.inst.DB_Building.GetData(buildingFromId.mType, buildingFromId.mLevel);
    BuildingController buildingControler = CitadelSystem.inst.GetBuildControllerFromBuildingItem(buildingFromId);
    Vector3 position = buildingControler.transform.position;
    if (bi.Type != "fortress")
      CitadelSystem.inst.cityTroopAnimManager.SendTroops(CitadelSystem.inst.cityTroopAnimManager.cityPath, position, CitadelSystem.inst.cityTroopAnimManager.endPoint.position, new CityTroopsAnimControler.Parameter()
      {
        troopType = this.GetTroopTypeByBuildingType(bi.Type),
        troopsCount = tmpTroopCount
      });
    if (!this.IsTroopBuilding(bi.Type))
      return;
    AudioManager.Instance.PlaySound("sfx_harvest_" + bi.Type, false);
    MessageHub.inst.GetPortByAction("City:collectTroop").SendRequest(Utils.Hash((object) "city_id", (object) CityManager.inst.GetCityID(), (object) "building_id", (object) buildingID), (System.Action<bool, object>) ((bSuccess, data) =>
    {
      if (!bSuccess)
        return;
      this.Shoot(buildingControler.transform, GameObject.Find("CityPower_VIP/Value").transform);
      if (bi.Type == "fortress")
        UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_trap_training_complete", new Dictionary<string, string>()
        {
          {
            "trap_name",
            Utils.XLAT(unit.Troop_Name_LOC_ID)
          },
          {
            "num",
            tmpTroopCount.ToString()
          }
        }, true), (System.Action) null, 4f, false);
      else
        UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_troop_training_complete", new Dictionary<string, string>()
        {
          {
            "troop_name",
            Utils.XLAT(unit.Troop_Name_LOC_ID)
          },
          {
            "num",
            tmpTroopCount.ToString()
          }
        }, true), (System.Action) null, 4f, false);
      if (callBack == null)
        return;
      callBack();
    }), true);
  }

  private bool IsTroopBuilding(string buildingType)
  {
    return this.GetTroopTypeByBuildingType(buildingType) != TroopType.invalid;
  }

  public TroopType GetTroopTypeByBuildingType(string buildingType)
  {
    string key = buildingType;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (BarracksManager.\u003C\u003Ef__switch\u0024map2C == null)
      {
        // ISSUE: reference to a compiler-generated field
        BarracksManager.\u003C\u003Ef__switch\u0024map2C = new Dictionary<string, int>(6)
        {
          {
            "barracks",
            0
          },
          {
            "range",
            1
          },
          {
            "stables",
            2
          },
          {
            "sanctum",
            3
          },
          {
            "workshop",
            4
          },
          {
            "fortress",
            5
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (BarracksManager.\u003C\u003Ef__switch\u0024map2C.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return TroopType.class_infantry;
          case 1:
            return TroopType.class_ranged;
          case 2:
            return TroopType.class_cavalry;
          case 3:
            return TroopType.class_artyclose;
          case 4:
            return TroopType.class_artyfar;
          case 5:
            return TroopType.class_trap;
        }
      }
    }
    return TroopType.invalid;
  }

  public List<Unit_StatisticsInfo> GetAllUnitsCanTrain(string buildingType)
  {
    List<Unit_StatisticsInfo> infosByBuildingType = ConfigManager.inst.DB_Unit_Statistics.GetAllUnitStaticsticsInfosByBuildingType(buildingType);
    infosByBuildingType.Sort((Comparison<Unit_StatisticsInfo>) ((x, y) => x.Priority.CompareTo(y.Priority)));
    return infosByBuildingType;
  }

  public int GetTotalTrapNum()
  {
    List<Unit_StatisticsInfo> infosByBuildingType = ConfigManager.inst.DB_Unit_Statistics.GetAllUnitStaticsticsInfosByBuildingType("fortress");
    int num = 0;
    using (List<Unit_StatisticsInfo>.Enumerator enumerator = infosByBuildingType.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Unit_StatisticsInfo current = enumerator.Current;
        num += DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).cityTroops.GetTroopsCount(current.ID);
      }
    }
    return num;
  }

  private Vector3 GetBeginPosition(Transform origin)
  {
    Camera ui2Dcamera = UIManager.inst.ui2DCamera;
    Vector3 screenPoint = UIManager.inst.cityCamera.GetComponent<Camera>().WorldToScreenPoint(origin.position);
    screenPoint.z = 0.0f;
    return ui2Dcamera.transform.InverseTransformPoint(ui2Dcamera.ScreenToWorldPoint(screenPoint));
  }

  private void Shoot(Transform from, Transform to)
  {
  }

  private void OnPowerSfxComplete()
  {
    UIManager.inst.publicHUD.heroMiniDlg.PlayAnimation();
    foreach (ParticleSystem componentsInChild in this.flySfx.GetComponentsInChildren<ParticleSystem>())
      componentsInChild.Clear(true);
    this.flySfx.SetActive(false);
  }

  public string GetBuildingUnlockUnitName(string buildingType, int buildingLevel)
  {
    List<Unit_StatisticsInfo> infosByBuildingType = ConfigManager.inst.DB_Unit_Statistics.GetAllUnitStaticsticsInfosByBuildingType(buildingType);
    for (int index = 0; index < infosByBuildingType.Count; ++index)
    {
      if (infosByBuildingType[index].Requirement_Value_1 == (double) buildingLevel)
        return Utils.XLAT(infosByBuildingType[index].Troop_Name_LOC_ID);
    }
    return string.Empty;
  }

  public enum BlockedType
  {
    NONE,
    RESEARCH,
    BUILDING,
    ALL,
  }
}
