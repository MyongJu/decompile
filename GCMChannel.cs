﻿// Decompiled with JetBrains decompiler
// Type: GCMChannel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class GCMChannel : NotificationChannel
{
  public override void Execute(Hashtable source)
  {
    if (source == null || source.Keys.Count == 0)
      return;
    if (source.ContainsKey((object) "message") && source[(object) "message"] != null)
    {
      source = Utils.Json2Object(source[(object) "message"].ToString(), true) as Hashtable;
      if (source != null)
        ;
    }
    if (source == null || !source.ContainsKey((object) "acme") || source[(object) "acme"] == null)
      return;
    string str1 = source[(object) "acme"].ToString();
    string str2 = string.Empty;
    string key = str1;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (GCMChannel.\u003C\u003Ef__switch\u0024map59 == null)
      {
        // ISSUE: reference to a compiler-generated field
        GCMChannel.\u003C\u003Ef__switch\u0024map59 = new Dictionary<string, int>(3)
        {
          {
            "1",
            0
          },
          {
            "2",
            1
          },
          {
            "3",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (GCMChannel.\u003C\u003Ef__switch\u0024map59.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            str2 = "valut";
            break;
          case 1:
            str2 = "reception";
            break;
          case 2:
            str2 = "black_market";
            break;
        }
      }
    }
    if (string.IsNullOrEmpty(str2))
      return;
    NotificationManager.Instance.SetGotoParam("{\"classname\":\"LPFocusConstantBuilding\",\"content\":\"{\\\"buildingType\\\":\\\"" + str2 + "\\\"}\"}");
  }
}
