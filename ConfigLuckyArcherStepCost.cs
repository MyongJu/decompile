﻿// Decompiled with JetBrains decompiler
// Type: ConfigLuckyArcherStepCost
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigLuckyArcherStepCost
{
  private List<LuckyArcherStepCostInfo> _luckyArcherStepCostInfoList = new List<LuckyArcherStepCostInfo>();
  private Dictionary<string, LuckyArcherStepCostInfo> _datas;
  private Dictionary<int, LuckyArcherStepCostInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<LuckyArcherStepCostInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, LuckyArcherStepCostInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._luckyArcherStepCostInfoList.Add(enumerator.Current);
    }
    this._luckyArcherStepCostInfoList.Sort((Comparison<LuckyArcherStepCostInfo>) ((a, b) => int.Parse(a.id).CompareTo(int.Parse(b.id))));
  }

  public List<LuckyArcherStepCostInfo> GetLuckyArcherStepCostInfoList()
  {
    return this._luckyArcherStepCostInfoList;
  }

  public LuckyArcherStepCostInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (LuckyArcherStepCostInfo) null;
  }

  public LuckyArcherStepCostInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (LuckyArcherStepCostInfo) null;
  }
}
