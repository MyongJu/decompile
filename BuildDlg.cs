﻿// Decompiled with JetBrains decompiler
// Type: BuildDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using UI;
using UnityEngine;

public class BuildDlg : UI.Dialog
{
  private ArrayList mCards = new ArrayList();
  public GameObject prefabPanel;
  public ResourceHUD mSilverResource;
  public ResourceHUD mFoodResource;
  public ResourceHUD mWoodResource;
  public ResourceHUD mOreResource;
  public UILabel mGoldLabel;
  public UIScrollView mScrollView;
  public UIAtlas mRural1Atlas;
  public UIAtlas mUrban1Atlas;
  public UILabel mBuildLabel;
  public UILabel mInstBuildLabel;
  public UILabel mCurrentGold;
  public GameObject mBuildingInfoDlg;
  private GameObject _targetPlot;
  private float _totalCardWidth;
  private IEnumerator _timerCR;
  public BuildDlg.TrainTimerBar mTimer;
  public GameObject mCancelBuildPopup;
  public GameObject mCancelUpgradePopup;
  public UILabel mCancelUpgradeMsg;
  private string _refName;
  private string _buildingName;
  private string _buildingIcon;
  private int _level;
  private TimerHUDUIItem _timer;
  private IEnumerator _popupCR;
  private CityManager.BuildingItem _buildingItem;
  private int _zone;
  private PlayerResourceData _playerData;

  public void Show(int zone, PlayerResourceData playerData, GameObject targetPlot)
  {
    this.gameObject.SetActive(true);
    this._targetPlot = targetPlot;
    CityManager inst = CityManager.inst;
    this.mBuildLabel.text = Utils.XLAT("id_uppercase_build");
    this.mInstBuildLabel.text = Utils.XLAT("id_uppercase_instant_build");
    string[] strArray1 = new string[3]
    {
      "farm",
      "lumber_mill",
      "mine"
    };
    string[] strArray2 = new string[3]
    {
      string.Format("{0}{1}1", (object) "farm", (object) "_l"),
      string.Format("{0}{1}1", (object) "lumber_mill", (object) "_l"),
      string.Format("{0}{1}1", (object) "mine", (object) "_l")
    };
    string[] strArray3 = new string[3]
    {
      "Produce Food",
      "Produce Wood",
      "Produce Ore"
    };
    string[] strArray4 = new string[16]
    {
      "house",
      "hospital",
      "marketplace",
      "storehouse",
      "prison",
      "forge",
      "university",
      "barracks",
      "stables",
      "range",
      "sanctum",
      "workshop",
      "embassy",
      "war_rally",
      "watchtower",
      "fortress"
    };
    string[] strArray5 = new string[15]
    {
      string.Format("{0}{1}1", (object) "house", (object) "_l"),
      string.Format("{0}{1}1", (object) "hospital", (object) "_l"),
      string.Format("{0}{1}1", (object) "marketplace", (object) "_l"),
      string.Format("{0}{1}1", (object) "storehouse", (object) "_l"),
      string.Format("{0}{1}1", (object) "prison", (object) "_l"),
      string.Format("{0}{1}1", (object) "forge", (object) "_l"),
      string.Format("{0}{1}1", (object) "university", (object) "_l"),
      string.Format("{0}{1}1", (object) "barracks", (object) "_l"),
      string.Format("{0}{1}1", (object) "barracks", (object) "_l"),
      string.Format("{0}{1}1", (object) "barracks", (object) "_l"),
      string.Format("{0}{1}1", (object) "barracks", (object) "_l"),
      string.Format("{0}{1}1", (object) "barracks", (object) "_l"),
      string.Format("{0}{1}1", (object) "embassy", (object) "_l"),
      string.Format("{0}{1}1", (object) "war_rally", (object) "_l"),
      string.Format("{0}{1}1", (object) "watchtower", (object) "_l")
    };
    string[] strArray6 = new string[15]
    {
      "Produce Silver",
      "Heal Troops",
      "Alliance Trade",
      "Protect Resource",
      "Execute Captured Hero",
      "Craft & Combine",
      "Research",
      "Train Units",
      "Train Units",
      "Train Units",
      "Train Units",
      "Train Units",
      "Reinforcement",
      "Alliance Rally",
      "Alert Incoming march"
    };
    int num = 0;
    string[] strArray7 = strArray1;
    string[] strArray8 = strArray2;
    string[] strArray9 = strArray3;
    if (zone == 2)
    {
      strArray7 = strArray4;
      strArray8 = strArray5;
      strArray9 = strArray6;
    }
    foreach (UnityEngine.Object mCard in this.mCards)
      UnityEngine.Object.Destroy(mCard);
    this.mCards.Clear();
    ArrayList arrayList = new ArrayList();
    int index1 = 0;
    for (int index2 = 0; index2 < strArray7.Length; ++index2)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(strArray7[index2], 1);
      if (data == null)
        D.warn((object) "Building {0} does not exist in Building metrics!", (object) strArray7[index2]);
      else if (!data.Unique || inst.GetHighestBuildingLevelFor(strArray7[index2]) <= -1)
      {
        GameObject gameObject = Utils.DuplicateGOB(this.prefabPanel);
        gameObject.SetActive(true);
        bool bRequirementsOk = BuildDlg.IsRequirementsOk(strArray7[index2], 1);
        ConstructionBuildingBtn component = gameObject.GetComponent<ConstructionBuildingBtn>();
        component.SetDetails(strArray7[index2], "Prefab/UI/BuildingPrefab/ui_" + strArray8[index2], strArray7[index2], strArray9[index2], bRequirementsOk);
        if (bRequirementsOk)
        {
          arrayList.Insert(index1, (object) component);
          ++index1;
        }
        else
          arrayList.Add((object) component);
        this.mCards.Add((object) gameObject);
        ++num;
      }
    }
    float newX = (float) (-(arrayList.Count * 550) / 2 - 275);
    foreach (Component component in arrayList)
    {
      Utils.SetLPX(component.gameObject, newX);
      newX += 550f;
    }
    this._totalCardWidth = newX;
    if ((double) this._totalCardWidth < (double) this.mScrollView.panel.width)
      SpringPanel.Begin(this.mScrollView.panel.cachedGameObject, new Vector3(this._totalCardWidth, 73f, 0.0f), 1000f);
    else
      this.mScrollView.ResetPosition();
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    this.mSilverResource.SetAmounts((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER), 0L);
    this.mFoodResource.SetAmounts((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD), 0L);
    this.mWoodResource.SetAmounts((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD), 0L);
    this.mOreResource.SetAmounts((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE), 0L);
    int currency = GameEngine.Instance.PlayerData.hostPlayer.Currency;
    this.mGoldLabel.text = currency.ToString("N0", (IFormatProvider) CultureInfo.InvariantCulture);
    this.mCurrentGold.text = currency.ToString("N0", (IFormatProvider) CultureInfo.InvariantCulture);
  }

  private void OnResourceChanged(string resourceType, int delta)
  {
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    this.mSilverResource.SetAmounts((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER), 0L);
    this.mFoodResource.SetAmounts((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD), 0L);
    this.mWoodResource.SetAmounts((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD), 0L);
    this.mOreResource.SetAmounts((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE), 0L);
    if (!this.mBuildingInfoDlg.activeSelf)
      return;
    this.OnShowRequirements(this._refName, this._buildingName, this._buildingIcon, this._level);
  }

  private void OnPlayerStatChanged(Events.PlayerStatChangedArgs ev)
  {
    if (ev.Stat != Events.PlayerStat.Currency)
      return;
    this.mCurrentGold.text = ev.NewValue.ToString("N0", (IFormatProvider) CultureInfo.InvariantCulture);
  }

  public void MonitorPopup()
  {
    this.StartCoroutine(this._popupCR = this._MonitorPopup());
  }

  [DebuggerHidden]
  private IEnumerator _MonitorPopup()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BuildDlg.\u003C_MonitorPopup\u003Ec__Iterator31()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void OnBuildingBtnPressed()
  {
    ConstructionBuildingBtn component = UIButton.current.gameObject.GetComponent<ConstructionBuildingBtn>();
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData(component.mBuildingCodeStr, 1);
    int num = 0;
    string buildingIcon = data.Building_ImagePath + "_l" + CityManager.inst.GetIconLevelFromBuildingLevel(num + 1, (string) null).ToString();
    this.OnShowRequirements(component.mBuildingCodeStr, data.Building_LOC_ID, buildingIcon, 1);
  }

  public void OnHardCurrencyBtnPressed()
  {
    UIManager.inst.OpenDlg("RunesShopDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private static bool CheckRequirement(string idStr, int reqValue)
  {
    if (reqValue < 1)
      return true;
    int result = -1;
    int.TryParse(idStr, out result);
    if (ItemBag.Instance.GetItemCountByShopID(result) > 0)
      return true;
    string buildingType = string.Empty;
    if (idStr != string.Empty && ConfigManager.inst.DB_Building.GetData(result) != null)
      buildingType = ConfigManager.inst.DB_Building.GetData(result).Type;
    return CityManager.inst.GetHighestBuildingLevelFor(buildingType) >= reqValue;
  }

  public static bool IsRequirementsOk(string refName, int level)
  {
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData(refName, level);
    return data != null && BuildDlg.CheckRequirement(data.Requirement_ID_1, data.Requirement_Value_1) && (BuildDlg.CheckRequirement(data.Requirement_ID_2, data.Requirement_Value_2) && BuildDlg.CheckRequirement(data.Requirement_ID_3, data.Requirement_Value_3)) && BuildDlg.CheckRequirement(data.Requirement_ID_4, data.Requirement_Value_4);
  }

  private bool BuildItemRequirement(string idStr, int reqValue, ArrayList buttons, ConstructionBuildingStats CBS, ref bool bResourceEmpty)
  {
    bool flag = true;
    if (reqValue > 0)
    {
      int result = -1;
      int.TryParse(idStr, out result);
      if (ItemBag.Instance.GetItemCount(result) > 0)
      {
        int itemCount = ItemBag.Instance.GetItemCount(result);
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(result);
        int num = reqValue;
        string icon = string.Format("Texture/ItemIcons/ui_{0}", (object) itemStaticInfo.ID);
        GameObject gameObject = CBS.AddRequirement(ConstructionBuildingStats.Requirement.ITEM, icon, idStr, (double) itemCount, (double) num);
        buttons.Add((object) gameObject);
        if (itemCount < num)
          bResourceEmpty = true;
      }
      else
      {
        string buildingType = string.Empty;
        if (idStr != string.Empty && ConfigManager.inst.DB_Building.GetData(int.Parse(idStr)) != null)
          buildingType = ConfigManager.inst.DB_Building.GetData(int.Parse(idStr)).Type;
        BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingType + "_" + reqValue.ToString());
        int buildingLevelFor = CityManager.inst.GetHighestBuildingLevelFor(buildingType);
        int num = reqValue;
        GameObject gameObject = data == null ? CBS.AddRequirement(ConstructionBuildingStats.Requirement.BUILDING, string.Empty, idStr + " not exist", (double) buildingLevelFor, (double) num) : CBS.AddRequirement(ConstructionBuildingStats.Requirement.BUILDING, "Prefab/UI/BuildingPrefab/ui_" + data.Building_ImagePath + "_l" + CityManager.inst.GetIconLevelFromBuildingLevel(reqValue, (string) null).ToString(), data.Building_LOC_ID, (double) buildingLevelFor, (double) num);
        if (buildingLevelFor < num)
          flag = false;
        buttons.Add((object) gameObject);
      }
    }
    return flag;
  }

  public void OnShowRequirements(string refName, string buildingName, string buildingIcon, int level)
  {
    this.gameObject.SetActive(true);
    this._refName = refName;
    this._buildingName = buildingName;
    this._buildingIcon = buildingIcon;
    this._level = level;
    this.mCurrentGold.text = Utils.FormatThousands(GameEngine.Instance.PlayerData.hostPlayer.Currency.ToString());
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData(refName, level);
    BuildingInfo buildingInfo = (BuildingInfo) null;
    if (level > 1)
    {
      this.mBuildLabel.text = Utils.XLAT("id_uppercase_upgrade");
      this.mInstBuildLabel.text = Utils.XLAT("id_uppercase_instant_upgrade");
      buildingInfo = ConfigManager.inst.DB_Building.GetData(refName, level - 1);
    }
    if (data == null)
      return;
    int buildingsInFlux = CityManager.inst.GetBuildingsInFlux();
    Hashtable hashtable1 = new Hashtable();
    hashtable1[(object) ItemBag.ItemType.food] = (object) data.Food;
    hashtable1[(object) ItemBag.ItemType.wood] = (object) data.Wood;
    hashtable1[(object) ItemBag.ItemType.silver] = (object) data.Silver;
    hashtable1[(object) ItemBag.ItemType.ore] = (object) data.Ore;
    Hashtable hashtable2 = new Hashtable();
    hashtable2[(object) data.Requirement_ID_1] = (object) data.Requirement_Value_1;
    hashtable2[(object) data.Requirement_ID_2] = (object) data.Requirement_Value_2;
    hashtable2[(object) data.Requirement_ID_3] = (object) data.Requirement_Value_3;
    hashtable2[(object) data.Requirement_ID_4] = (object) data.Requirement_Value_4;
    Hashtable ht = new Hashtable();
    ht[(object) "time"] = (object) data.Build_Time;
    ht[(object) "resources"] = (object) hashtable1;
    ht[(object) "items"] = (object) hashtable2;
    Hashtable instantCost = ItemBag.CalculateInstantCost(ht);
    int cost = ItemBag.CalculateCost(ht);
    double buildTime = data.Build_Time;
    double num1 = DatabaseTools.calc1(buildTime, (double) BenefitLocalDB.inst.Get("prop_construction_time_percent").total, 0.0);
    this.mBuildingInfoDlg.SetActive(true);
    ConstructionBuildingStats component = this.mBuildingInfoDlg.GetComponent<ConstructionBuildingStats>();
    component.SetDetails(this._buildingItem, buildingIcon, "LV." + data.Building_Lvl.ToString() + "/30", buildTime, num1, cost, buildingName, this._targetPlot, data.Building_Lvl, refName, (System.Action) (() =>
    {
      this.mBuildingInfoDlg.SetActive(false);
      this.OnCloseButtonClick();
    }));
    component.mInstBuildDetails = instantCost;
    ArrayList buttons = new ArrayList();
    ArrayList arrayList = new ArrayList();
    int num2 = BuildQueueManager.Instance.AllowedBuildQueue(0.0);
    bool flag = true;
    bool bResourceEmpty = false;
    if (BuildQueueManager.Instance.IsBuildQueueFull(num1))
    {
      GameObject gameObject = component.AddRequirement(ConstructionBuildingStats.Requirement.QUEUE, (string) null, (string) null, (double) num2, (double) buildingsInFlux);
      buttons.Add((object) gameObject);
      component.SetQueueFull();
      if (this.mTimer != null)
        component.SetQueueTimer(this.mTimer);
    }
    if (!this.BuildItemRequirement(data.Requirement_ID_1, data.Requirement_Value_1, buttons, component, ref bResourceEmpty))
      flag = false;
    if (!this.BuildItemRequirement(data.Requirement_ID_2, data.Requirement_Value_2, buttons, component, ref bResourceEmpty))
      flag = false;
    if (!this.BuildItemRequirement(data.Requirement_ID_3, data.Requirement_Value_3, buttons, component, ref bResourceEmpty))
      flag = false;
    if (!this.BuildItemRequirement(data.Requirement_ID_4, data.Requirement_Value_4, buttons, component, ref bResourceEmpty))
      flag = false;
    CityData byUid = DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid);
    if (data.Food > 0.0)
    {
      int currentResource = (int) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD);
      GameObject gameObject = component.AddRequirement(ConstructionBuildingStats.Requirement.FOOD, (string) null, (string) null, (double) currentResource, data.Food);
      buttons.Add((object) gameObject);
      if ((double) currentResource < data.Food)
        bResourceEmpty = true;
    }
    if (data.Wood > 0.0)
    {
      int currentResource = (int) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD);
      GameObject gameObject = component.AddRequirement(ConstructionBuildingStats.Requirement.WOOD, (string) null, (string) null, (double) currentResource, data.Wood);
      buttons.Add((object) gameObject);
      if ((double) currentResource < data.Wood)
        bResourceEmpty = true;
    }
    if (data.Silver > 0.0)
    {
      int currentResource = (int) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
      GameObject gameObject = component.AddRequirement(ConstructionBuildingStats.Requirement.SILVER, (string) null, (string) null, (double) currentResource, data.Silver);
      buttons.Add((object) gameObject);
      if ((double) currentResource < data.Silver)
        bResourceEmpty = true;
    }
    if (data.Ore > 0.0)
    {
      int currentResource = (int) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE);
      GameObject gameObject = component.AddRequirement(ConstructionBuildingStats.Requirement.ORE, (string) null, (string) null, (double) currentResource, data.Ore);
      buttons.Add((object) gameObject);
      if ((double) currentResource < data.Ore)
        bResourceEmpty = true;
    }
    component.mBuildBtn.isEnabled = flag && !bResourceEmpty;
    component.mInstBuidBtn.isEnabled = flag;
    GameObject gameObject1 = component.AddRewards(ConstructionBuildingStats.Rewards.HERO_XP, (string) null, (string) null, (double) (data.Hero_XP - (level <= 1 ? 0 : buildingInfo.Hero_XP)));
    arrayList.Add((object) gameObject1);
    GameObject gameObject2 = component.AddRewards(ConstructionBuildingStats.Rewards.POWER, (string) null, (string) null, (double) (data.Power - (level <= 1 ? 0 : buildingInfo.Power)));
    arrayList.Add((object) gameObject2);
    if (data.Benefit_ID_1.Length > 0)
    {
      GameObject gameObject3 = component.AddRewards(ConstructionBuildingStats.Rewards.DEFAULT, (string) null, data.Benefit_ID_1, data.Benefit_Value_1 - (level <= 1 ? 0.0 : buildingInfo.Benefit_Value_1));
      arrayList.Add((object) gameObject3);
    }
    if (data.Benefit_ID_2.Length > 0)
    {
      GameObject gameObject3 = component.AddRewards(ConstructionBuildingStats.Rewards.DEFAULT, (string) null, data.Benefit_ID_2, data.Benefit_Value_2 - (level <= 1 ? 0.0 : buildingInfo.Benefit_Value_2));
      arrayList.Add((object) gameObject3);
    }
    if (data.Benefit_ID_3.Length > 0)
    {
      GameObject gameObject3 = component.AddRewards(ConstructionBuildingStats.Rewards.DEFAULT, (string) null, data.Benefit_ID_3, data.Benefit_Value_3 - (level <= 1 ? 0.0 : buildingInfo.Benefit_Value_3));
      arrayList.Add((object) gameObject3);
    }
    if (this._buildingItem == null)
      return;
    CityManager.BuildingItem buildingFromId = CityManager.inst.GetBuildingFromID(this._buildingItem.mID);
    if (!((UnityEngine.Object) buildingFromId.mBuildTimer != (UnityEngine.Object) null))
      return;
    this.StartCoroutine(this._timerCR = this.WatchTimer(buildingFromId.mBuildTimer));
  }

  public void OnCloseButtonClick()
  {
    GameEngine.Instance.events.onPlayerStatChanged -= new System.Action<Events.PlayerStatChangedArgs>(this.OnPlayerStatChanged);
    GameEngine.Instance.events.OnResourceCapacityChanged -= new System.Action<string, int>(this.OnResourceChanged);
    if (this._popupCR != null)
      this.StopCoroutine(this._popupCR);
    this._popupCR = (IEnumerator) null;
    MessageHub.inst.GetPortByAction("job_complete").RemoveEvent(new System.Action<object>(this.OnJobCompleted));
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void Update()
  {
  }

  public void SetWaitState(bool bWait)
  {
    if (bWait)
    {
      this.mTimer.mAllRoot.SetActive(true);
      if (this._timer.GetSpeedupState() == TimerHUDUIItem.SpeedupTypes.SPEEDUP_HELP)
      {
        this.mTimer.mHelpRoot.SetActive(true);
        this.mTimer.mSpeedUpRoot.SetActive(false);
      }
      else
      {
        this.mTimer.mSpeedUpRoot.SetActive(true);
        this.mTimer.mHelpRoot.SetActive(false);
      }
    }
    else
    {
      this.mTimer.mAllRoot.SetActive(false);
      this.mTimer.mHelpRoot.SetActive(false);
      this.mTimer.mSpeedUpRoot.SetActive(false);
    }
  }

  [DebuggerHidden]
  private IEnumerator WatchTimer(TimerHUDUIItem timer)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BuildDlg.\u003CWatchTimer\u003Ec__Iterator32()
    {
      timer = timer,
      \u003C\u0024\u003Etimer = timer,
      \u003C\u003Ef__this = this
    };
  }

  public void OnTimerSpeedUpBtnPressed()
  {
    this._buildingItem.mBuildTimer.PushSpeedupBtn();
  }

  public void OnCancelTimerBtnPressed()
  {
    if ((UnityEngine.Object) this._targetPlot == (UnityEngine.Object) null)
    {
      this.mCancelUpgradePopup.SetActive(true);
      this.mCancelUpgradeMsg.text = "[e1f348] Lv." + this._buildingItem.mLevel.ToString() + " " + this._buildingItem.mType + " [-]?";
    }
    else
      this.mCancelBuildPopup.SetActive(true);
  }

  public void OnCancelBuildConfirmed()
  {
    this.mCancelBuildPopup.SetActive(false);
    this.mCancelUpgradePopup.SetActive(false);
    this.StopCoroutine(this._timerCR);
    CityManager.inst.CancelBuild(this._buildingItem.mID, new System.Action(this._CancelBuildFinished));
  }

  public void _CancelBuildFinished()
  {
    this.SetWaitState(false);
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.OnShowRequirements(this._refName, this._buildingName, this._buildingIcon, this._level)));
  }

  public void OnCancelBuildCancel()
  {
    this.mCancelBuildPopup.SetActive(false);
    this.mCancelUpgradePopup.SetActive(false);
  }

  public void OnUniversityBtnPressed()
  {
    UIManager.inst.OpenDlg("Research/ResearchBaseDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnJobCompleted(object result)
  {
    Hashtable hashtable = result as Hashtable;
    int result1 = 0;
    if (hashtable == null || !hashtable.ContainsKey((object) "event_type") || !int.TryParse(hashtable[(object) "event_type"].ToString(), out result1) || result1 != 30 && result1 != 31)
      return;
    this.mTimer.mHelpRoot.SetActive(false);
    this.mTimer.mSpeedUpRoot.SetActive(false);
    this.OnCancelBuildCancel();
    this._timerCR = (IEnumerator) null;
    this.SetWaitState(false);
    Utils.ExecuteInSecs(0.1f, (System.Action) (() =>
    {
      if ((UnityEngine.Object) this._targetPlot != (UnityEngine.Object) null)
        this.Show(this._zone, this._playerData, this._targetPlot);
      if (!this.mBuildingInfoDlg.activeSelf)
        return;
      if (this._buildingItem != null)
      {
        int mLevel = this._buildingItem.mLevel;
        if (mLevel == 30)
        {
          this.OnCloseButtonClick();
          if ((UnityEngine.Object) Utils.FindClass<CitadelSystem>() != (UnityEngine.Object) null)
            Utils.FindClass<CitadelSystem>().OnCityMapClicked();
        }
        this.OnShowRequirements(this._refName, this._buildingName, this._buildingIcon, mLevel + 1);
      }
      else
        this.OnShowRequirements(this._refName, this._buildingName, this._buildingIcon, 1);
    }));
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._buildingItem = BuildingController.mSelectedBuildingItem;
    BuildDlg.Parameter parameter = orgParam as BuildDlg.Parameter;
    this._targetPlot = parameter.targetPlot;
    this._playerData = parameter.playerData;
    this._zone = parameter.zone;
    this._refName = parameter.refName;
    this._buildingName = parameter.buildingName;
    this._buildingIcon = parameter.buildingIcon;
    this._level = parameter.level;
    if ((UnityEngine.Object) this._targetPlot != (UnityEngine.Object) null)
    {
      this.mBuildingInfoDlg.SetActive(false);
      this.Show(this._zone, this._playerData, this._targetPlot);
    }
    MessageHub.inst.GetPortByAction("job_complete").AddEvent(new System.Action<object>(this.OnJobCompleted));
    GameEngine.Instance.events.onPlayerStatChanged += new System.Action<Events.PlayerStatChangedArgs>(this.OnPlayerStatChanged);
    GameEngine.Instance.events.OnResourceCapacityChanged += new System.Action<string, int>(this.OnResourceChanged);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.SetWaitState(false);
    this.OnShowRequirements(this._refName, this._buildingName, this._buildingIcon, this._level);
  }

  [Serializable]
  public class TrainTimerBar
  {
    public GameObject mAllRoot;
    public GameObject mHelpRoot;
    public GameObject mSpeedUpRoot;
    public GameObject mFreeRoot;
    public UISprite mHelpBar;
    public UISprite mSpeedUpBar;
    public UISprite mFreeBar;
    public UISprite mHelpBkg;
    public UISprite mSpeedUpBkg;
    public UISprite mFreeBkg;
    public UILabel mHelpDescription;
    public UILabel mSpeedUpDescription;
    public UILabel mFreeDescription;
    public UILabel mHelpTimeLeftValue;
    public UILabel mSpeedUpTimeLeftValue;
    public UILabel mFreeTimeLeftValue;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int zone;
    public PlayerResourceData playerData;
    public GameObject targetPlot;
    public string refName;
    public string buildingName;
    public string buildingIcon;
    public int level;
  }
}
