﻿// Decompiled with JetBrains decompiler
// Type: ConfigKingdomDefinitions
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;

public class ConfigKingdomDefinitions
{
  public Dictionary<int, KingdomDefinition> data;
  private Dictionary<string, int> dicByUniqueId;

  public ConfigKingdomDefinitions()
  {
    this.data = new Dictionary<int, KingdomDefinition>();
    this.dicByUniqueId = new Dictionary<string, int>();
  }

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exsit", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "loc_name_id");
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          KingdomDefinition kingdom = new KingdomDefinition();
          if (int.TryParse(key.ToString(), out kingdom.internalId))
          {
            ArrayList arr = hashtable[key] as ArrayList;
            if (arr != null)
            {
              kingdom.ID = ConfigManager.GetString(arr, index1);
              kingdom.Name = ScriptLocalization.Get(ConfigManager.GetString(arr, index2), true);
              this.PushData(kingdom);
            }
          }
        }
      }
    }
  }

  public Dictionary<int, KingdomDefinition>.ValueCollection Values
  {
    get
    {
      return this.data.Values;
    }
  }

  private void PushData(KingdomDefinition kingdom)
  {
    if (this.data.ContainsKey(kingdom.internalId))
      return;
    this.data.Add(kingdom.internalId, kingdom);
    if (!this.dicByUniqueId.ContainsKey(kingdom.ID))
      this.dicByUniqueId.Add(kingdom.ID, kingdom.internalId);
    ConfigManager.inst.AddData(kingdom.internalId, (object) kingdom);
  }

  public void Clear()
  {
    if (this.data != null)
      this.data.Clear();
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
  }

  public KingdomDefinition GetData(int internalId)
  {
    if (this.data.ContainsKey(internalId))
      return this.data[internalId];
    return (KingdomDefinition) null;
  }

  public KingdomDefinition this[int internalId]
  {
    get
    {
      return this.GetData(internalId);
    }
  }

  private KingdomDefinition GetData(string ID)
  {
    if (!this.dicByUniqueId.ContainsKey(ID))
      return (KingdomDefinition) null;
    return this.data[this.dicByUniqueId[ID]];
  }

  public KingdomDefinition this[string ID]
  {
    get
    {
      return this.GetData(ID);
    }
  }

  public bool Contains(string ID)
  {
    return this.dicByUniqueId.ContainsKey(ID);
  }
}
