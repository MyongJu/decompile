﻿// Decompiled with JetBrains decompiler
// Type: ArtifactSpecialSaleItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ArtifactSpecialSaleItemRenderer : MonoBehaviour
{
  public UILabel timeLimitedText;
  public UILabel artifactName;
  public UILabel artifactLeftAmountText;
  public UILabel artifactNormalPriceText;
  public UILabel artifactSpecialPriceText;
  public UILabel artifactNoSpecialPriceText;
  public UITexture artifactTexture;
  public UITexture artifactFrame;
  public UIButton normalPriceButton;
  public UIButton specialPriceButton;
  public GameObject artifactDetailNode;
  public GameObject noArtifactSaleNode;
  private ArtifactSpecialSaleData _specialSaleData;
  private Color _priceOriginColor;

  public void SetData(ArtifactSpecialSaleData specialSaleData)
  {
    this._specialSaleData = specialSaleData;
    this._priceOriginColor = this.artifactNoSpecialPriceText.color;
    this.AddEventHandler();
    this.UpdateUI();
  }

  public void ClearData()
  {
    this.RemoveEventHandler();
  }

  public void OnBuyBtnPressed()
  {
    if (this._specialSaleData == null)
      return;
    if (PlayerData.inst.userData.currency.gold < this._specialSaleData.Price)
    {
      Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
    }
    else
    {
      ArtifactShopInfo artifactShopInfo = ConfigManager.inst.DB_ArtifactShop.Get(this._specialSaleData.ConfigId);
      ArtifactSalePayload.Instance.ShowArtifactBuyConfirm(this._specialSaleData.ConfigId, artifactShopInfo == null ? 0L : artifactShopInfo.initialPrice, this._specialSaleData.Price, (System.Action) (() =>
      {
        List<ArtifactSpecialSaleData> specialSaleDataList = ArtifactSalePayload.Instance.GetArtifactSpecialSaleDataList();
        this._specialSaleData = specialSaleDataList == null ? (ArtifactSpecialSaleData) null : specialSaleDataList.Find((Predicate<ArtifactSpecialSaleData>) (x => x.Slot == this._specialSaleData.Slot));
        this.UpdateUI();
      }));
    }
  }

  public void OnArtifactPressed()
  {
    if (this._specialSaleData == null)
      return;
    ArtifactSalePayload.Instance.ShowArtifactDetail(this._specialSaleData.ArtifactId);
  }

  private void UpdateUI()
  {
    NGUITools.SetActive(this.artifactDetailNode, this._specialSaleData != null);
    NGUITools.SetActive(this.noArtifactSaleNode, this._specialSaleData == null);
    if (this._specialSaleData == null)
      return;
    ArtifactShopInfo artifactShopInfo = ConfigManager.inst.DB_ArtifactShop.Get(this._specialSaleData.ConfigId);
    if (artifactShopInfo != null)
    {
      UILabel artifactNormalPriceText = this.artifactNormalPriceText;
      string str1 = Utils.FormatThousands(artifactShopInfo.initialPrice.ToString());
      this.artifactNoSpecialPriceText.text = str1;
      string str2 = str1;
      artifactNormalPriceText.text = str2;
      this.artifactNoSpecialPriceText.color = PlayerData.inst.userData.currency.gold >= artifactShopInfo.initialPrice ? this._priceOriginColor : Color.red;
      NGUITools.SetActive(this.normalPriceButton.gameObject, artifactShopInfo.initialPrice == this._specialSaleData.Price);
      NGUITools.SetActive(this.specialPriceButton.gameObject, artifactShopInfo.initialPrice != this._specialSaleData.Price);
    }
    this.artifactSpecialPriceText.text = Utils.FormatThousands(this._specialSaleData.Price.ToString());
    this.artifactSpecialPriceText.color = PlayerData.inst.userData.currency.gold >= this._specialSaleData.Price ? this._priceOriginColor : Color.red;
    this.artifactLeftAmountText.text = ScriptLocalization.GetWithPara("artifact_temp_remaining_num", new Dictionary<string, string>()
    {
      {
        "0",
        (this._specialSaleData.LeftAmount <= 0 ? "[FF0000]" : "[CFCFCF]") + Utils.FormatThousands(this._specialSaleData.LeftAmount.ToString()) + "[-]"
      }
    }, true);
    this.timeLimitedText.text = Utils.FormatTime(this._specialSaleData.EndTime - NetServerTime.inst.ServerTimestamp, true, false, true);
    ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this._specialSaleData.ArtifactId);
    if (artifactInfo != null)
    {
      this.artifactName.text = artifactInfo.Name;
      BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactFrame, artifactInfo.TimeLimitedArtifactFramePath, (System.Action<bool>) null, true, false, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactTexture, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    }
    this.normalPriceButton.isEnabled = this._specialSaleData.LeftAmount > 0;
    this.specialPriceButton.isEnabled = this._specialSaleData.LeftAmount > 0;
  }

  private void OnSecondEvent(int time)
  {
    if (this._specialSaleData == null)
      return;
    this.timeLimitedText.text = Utils.FormatTime(this._specialSaleData.EndTime - NetServerTime.inst.ServerTimestamp, true, false, true);
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void RemoveEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }
}
