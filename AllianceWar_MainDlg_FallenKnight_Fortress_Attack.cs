﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_FallenKnight_Fortress_Attack
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class AllianceWar_MainDlg_FallenKnight_Fortress_Attack : AllianceWar_MainDlg_Stay_Slot
{
  public override void Setup(long id, AllianceWar_MainDlg_Slot.SlotParameter param)
  {
    if (!(param is AllianceWar_MainDlg_Stay_Slot.StaySlotParameter))
      return;
    this.data.marchId = id;
    MarchData marchData = DBManager.inst.DB_March.Get(id);
    AllianceWarManager.KnightWarData knightWarData = PlayerData.inst.allianceWarManager.GetKnightWarData(id);
    if (knightWarData != null)
    {
      AllianceWar_MainDlg_RallyList.param.ownerName = knightWarData.knightInfo.LOC_Name;
      AllianceWar_MainDlg_RallyList.param.ownerPortrait = knightWarData.knightInfo.iconIndex;
      AllianceWar_MainDlg_RallyList.param.memberCount = knightWarData.knightInfo.para_1;
    }
    AllianceWar_MainDlg_RallyList.param.canJoin = false;
    AllianceWar_MainDlg_RallyList.param.isAttack = true;
    AllianceWar_MainDlg_RallyList.param.rallyType = AllianceWar_MainDlg_RallyList.Params.RallyListType.FALLEN_KNIGHT;
    this.panel.memberList.Setup(AllianceWar_MainDlg_RallyList.param);
    AllianceFortressData dataByCoordinate = DBManager.inst.DB_AllianceFortress.GetDataByCoordinate(marchData.targetLocation);
    if (dataByCoordinate != null)
      this.panel.targetSlot.SetFortress(dataByCoordinate.fortressId);
    this.OnSecond(NetServerTime.inst.ServerTimestamp);
  }

  public new void OnDetailClick()
  {
  }
}
