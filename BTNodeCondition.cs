﻿// Decompiled with JetBrains decompiler
// Type: BTNodeCondition
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class BTNodeCondition : BTNode
{
  private BTNodeCondition.ConditionFunc _check;
  private object _param;

  public BTNodeCondition(BTNodeCondition.ConditionFunc func, object param)
  {
    this._check = func;
    this._param = param;
  }

  public override BTNode Clone()
  {
    BTNodeCondition btNodeCondition = new BTNodeCondition(this._check, this._param);
    btNodeCondition.Parent = this.Parent;
    btNodeCondition.NodeName = this.NodeName;
    btNodeCondition.Status = this.Status;
    return (BTNode) btNodeCondition;
  }

  public override void Execute()
  {
    this.Status = BTNode.NodeStatus.Running;
    if (this._check != null)
      this.Status = !this._check(this._param) ? BTNode.NodeStatus.Failure : BTNode.NodeStatus.Success;
    else
      this.Status = BTNode.NodeStatus.Success;
  }

  public delegate bool ConditionFunc(object param);
}
