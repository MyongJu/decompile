﻿// Decompiled with JetBrains decompiler
// Type: BoostsDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UI;
using UnityEngine;

public class BoostsDlg : UI.Dialog
{
  private Dictionary<string, BoostCellItem> _cellMap = new Dictionary<string, BoostCellItem>();
  public PageTabsMonitor tabs;
  public UIScrollView cellsView;
  public UITable cellsList;
  public UIButton plusBtn;
  public UIButton backBtn;
  public UIButton closeBtn;
  public UILabel moneyLb;
  public BoostCellItem cellPrefab;
  public BoostItem itemPrefab;
  public GameObjectPool cellPool;
  public GameObjectPool itemPool;
  private int _tabIndex;

  public void InitDialog()
  {
    this.InitElements();
    this.cellPool = new GameObjectPool();
    this.cellPool.Initialize(this.cellPrefab.gameObject, this.gameObject);
    this.itemPool = new GameObjectPool();
    this.itemPool.Initialize(this.itemPrefab.gameObject, this.gameObject);
  }

  private void OnDataLoaded()
  {
    this.ShowTab(0);
    DBManager.inst.DB_Local_Benefit.onDataChanged += new System.Action<string>(this.OnBenefitDataChanged);
  }

  private void ShowTab(int tabIndex)
  {
    this._tabIndex = tabIndex;
    this.tabs.SetCurrentTab(tabIndex, true);
    this.RefrashTab();
  }

  private void RefrashTab()
  {
    this.ClearBoostCells();
    foreach (string str in BoostsConst.BoostTypeDict[(BoostsConst.BoostTabType) this._tabIndex])
    {
      BenefitInfo info = DBManager.inst.DB_Local_Benefit.Get(str);
      this.AddBoostCell(BoostCellData.CreateFromInfo(str, info));
    }
    this.cellsList.onReposition += new UITable.OnReposition(this.OnRepostionFinish);
    this.cellsList.repositionNow = true;
  }

  private void OnRepostionFinish()
  {
    this.cellsView.ResetPosition();
    this.cellsView.gameObject.SetActive(false);
    this.cellsView.gameObject.SetActive(true);
    this.cellsList.onReposition -= new UITable.OnReposition(this.OnRepostionFinish);
  }

  private void InitElements()
  {
    this.tabs.onTabSelected += new System.Action<int>(this.OnTabClick);
    this.backBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnBackBtnClick)));
    this.closeBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnCloseBtnClick)));
  }

  private void AddBoostCell(BoostCellData data)
  {
    BoostCellItem component = this.cellPool.Allocate().GetComponent<BoostCellItem>();
    component.Init(this.itemPool, this.cellsView, this.cellsList);
    component.SetData(data);
    component.transform.parent = this.cellsList.transform;
    component.transform.localScale = new Vector3(1f, 1f, 1f);
    this._cellMap.Add(data.key, component);
  }

  private void DeleteBoostCell(string key)
  {
    BoostCellItem boostCellItem;
    if (!this._cellMap.TryGetValue(key, out boostCellItem))
      return;
    boostCellItem.Release();
    this.cellPool.Release(boostCellItem.gameObject);
    this._cellMap.Remove(key);
  }

  private void UpdateBoostCell(string key)
  {
    BoostCellItem boostCellItem;
    if (!this._cellMap.TryGetValue(key, out boostCellItem))
      return;
    BenefitInfo info = DBManager.inst.DB_Local_Benefit.Get(key);
    if ((double) info.total != 0.0)
      boostCellItem.SetData(BoostCellData.CreateFromInfo(key, info));
    else
      this.DeleteBoostCell(key);
  }

  private void ClearBoostCells()
  {
    using (Dictionary<string, BoostCellItem>.ValueCollection.Enumerator enumerator = this._cellMap.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BoostCellItem current = enumerator.Current;
        current.Release();
        this.cellPool.Release(current.gameObject);
      }
    }
    this._cellMap.Clear();
  }

  private void OnCloseBtnClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void OnBackBtnClick()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void OnTabClick(int index)
  {
    if (this._tabIndex == index)
      return;
    this.ShowTab(index);
  }

  private void RequestBenefitData()
  {
    HubPort portByAction = MessageHub.inst.GetPortByAction("player:loadUserBenefits");
    Hashtable postData = new Hashtable();
    postData[(object) "uid"] = (object) PlayerData.inst.uid;
    postData[(object) "city_id"] = (object) PlayerData.inst.cityId;
    portByAction.SendRequest(postData, new System.Action<bool, object>(this.OnBenefitDataLoaded), true);
  }

  private void OnBenefitDataLoaded(bool result, object arg)
  {
    this.OnDataLoaded();
  }

  private void OnPlayerStatChanged(Events.PlayerStatChangedArgs ev)
  {
    if (ev.Stat != Events.PlayerStat.Currency)
      return;
    this.moneyLb.text = ev.NewValue.ToString("N0", (IFormatProvider) CultureInfo.InvariantCulture);
  }

  private void OnBenefitDataChanged(string key)
  {
    this.UpdateBoostCell(key);
  }

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
    this.InitDialog();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.RequestBenefitData();
    this.OnPlayerStatChanged(new Events.PlayerStatChangedArgs(Events.PlayerStat.Currency, 0.0, (double) GameEngine.Instance.PlayerData.hostPlayer.Currency));
    GameEngine.Instance.events.onPlayerStatChanged += new System.Action<Events.PlayerStatChangedArgs>(this.OnPlayerStatChanged);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    DBManager.inst.DB_Local_Benefit.onDataChanged -= new System.Action<string>(this.OnBenefitDataChanged);
    GameEngine.Instance.events.onPlayerStatChanged -= new System.Action<Events.PlayerStatChangedArgs>(this.OnPlayerStatChanged);
  }
}
