﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightDungeonEntryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonKnightDungeonEntryPopup : Popup
{
  private Dictionary<int, DungeonRankItemRenderer> _itemDict = new Dictionary<int, DungeonRankItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public GameObject noRankTipsNode;
  public UILabel dungeonSpirit;
  public UILabel dungeonLevel;
  public UILabel dungeonEndTime;
  public UILabel dungeonEnterButtonText;
  public UILabel dungeonYourRank;
  public UIProgressBar spiritProgress;
  public UITexture dungeonPlayerTexture;
  public UITexture dungeonFinishTexture;
  public UILabel dungeonFinishName;
  public UILabel dungeonFinishHint;
  public UIButton dungeonEnterButton;
  public UIScrollView scrollView;
  public UIGrid grid;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  private ArrayList _dungeonRankList;
  private long _myRank;
  private int _rankEndTime;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DragonKnightDungeonEntryPopup.Parameter parameter = orgParam as DragonKnightDungeonEntryPopup.Parameter;
    if (parameter != null)
    {
      this._dungeonRankList = parameter.dungeonRankList;
      this._myRank = parameter.myRank;
      this._rankEndTime = parameter.rankEndTime;
    }
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.AddEventHandler();
    this.OnSecondHandler(0);
    this.ShowDungeonRankingContent();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.ClearData();
  }

  public void OnForbiddenStoreClick()
  {
    UIManager.inst.OpenDlg("DragonKnight/DungeonAncientStoreDialog", (UI.Dialog.DialogParameter) new DungeonAncientStoreDialog.Parameter()
    {
      group = "dk_dungeon_shop",
      currency = "item_dk_dungeon_coin",
      titleKey = "dragon_knight_store_name"
    }, true, true, true);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnViewRewardsBtnPressed()
  {
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightDungeonRewardsPopup", (Popup.PopupParameter) new DragonKnightDungeonRewardsPopup.Parameter()
    {
      rankFinishTime = this._rankEndTime
    });
  }

  public void OnDungeonEnterBtnPressed()
  {
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L);
    if (dragonKnightData == null)
      return;
    Hashtable htRequest = new Hashtable();
    htRequest[(object) "level"] = (object) 1;
    if (dragonKnightData.IsInDungeon)
      MessageHub.inst.GetPortByAction("DragonKnight:getDungeonInfo").SendRequest((Hashtable) null, new System.Action<bool, object>(this.OnEnterDungeon), true);
    else if (dragonKnightData.MaxDungeonLevel == 0)
      MessageHub.inst.GetPortByAction("DragonKnight:enterNewDungeon").SendRequest(htRequest, new System.Action<bool, object>(this.OnEnterDungeon), true);
    else
      UIManager.inst.OpenPopup("DragonKnight/DragonKnightDungeonPickFloorPopup", (Popup.PopupParameter) new DragonKnightDungeonPickFloorPopup.Parameter()
      {
        currentDungeonLevel = dragonKnightData.MaxDungeonLevel,
        onPickButtonPressed = (System.Action<int>) (pickedDungeonLevel =>
        {
          htRequest[(object) "level"] = (object) pickedDungeonLevel;
          MessageHub.inst.GetPortByAction("DragonKnight:enterNewDungeon").SendRequest(htRequest, new System.Action<bool, object>(this.OnEnterDungeon), true);
        })
      });
  }

  public void OnSpeedupBtnPressed()
  {
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (dragonKnightData == null || dragonKnightData.CurrentDungeonEndTime <= NetServerTime.inst.ServerTimestamp)
      return;
    UIManager.inst.OpenPopup("SpeedUpPopup", (Popup.PopupParameter) new SpeedUpPopUp.Parameter()
    {
      jobId = -1L,
      speedupType = ItemBag.ItemType.dungeon_cd_speedup,
      canUseSpeedupArtifact = false,
      canUseCommonSpeedup = false
    });
  }

  public void OnDungeonFinishBtnPressed()
  {
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightDungeonFinishPopup", (Popup.PopupParameter) null);
  }

  private void OnEnterDungeon(bool ret, object data)
  {
    if (!ret)
      return;
    this.OnCloseBtnPressed();
    GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.DragonKnight;
    DragonKnightSystem.Instance.RoomManager.SetMazeData(DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L));
  }

  private bool IsDungeonReset()
  {
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    bool flag = false;
    if (dragonKnightData != null)
      flag = !dragonKnightData.IsInDungeon & dragonKnightData.CurrentDungeonEndTime > NetServerTime.inst.ServerTimestamp;
    return flag;
  }

  private DungeonRankItemRenderer CreateItemRenderer(string name, string acronym, string portrait, string icon, int lordTitleId, int level, int ranking)
  {
    GameObject gameObject = this._itemPool.AddChild(this.grid.gameObject);
    gameObject.SetActive(true);
    DungeonRankItemRenderer component = gameObject.GetComponent<DungeonRankItemRenderer>();
    component.SetData(name, acronym, portrait, icon, lordTitleId, level, ranking);
    return component;
  }

  private void ClearData()
  {
    using (Dictionary<int, DungeonRankItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.grid.repositionNow = true;
    this.grid.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private void UpdateUI()
  {
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    DragonKnightDungeonData knightDungeonData = DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L);
    if (dragonKnightData == null)
      return;
    DragonKnightLevelInfo byLevel = ConfigManager.inst.DB_DragonKnightLevel.GetByLevel(dragonKnightData.Level);
    string key = "prop_dragon_knight_dungeon_stamina_base_value";
    int dungeonStamina = byLevel.dungeonStamina;
    long finalData = (long) dragonKnightData.GetFinalData(key, dungeonStamina, "calc_dragon_knight_dungeon_stamina");
    long num = !dragonKnightData.IsInDungeon ? finalData : dragonKnightData.Spirit;
    this.dungeonSpirit.text = string.Format("{0}/{1}", (object) num, (object) finalData);
    this.dungeonLevel.text = Utils.XLAT("dragon_knight_floors_explored") + (knightDungeonData == null ? 0 : knightDungeonData.CurrentLevel).ToString();
    this.dungeonEnterButtonText.text = Utils.XLAT(!dragonKnightData.IsInDungeon ? "dragon_knight_uppercase_explore" : "dragon_knight_uppercase_continue_exploring");
    this.dungeonYourRank.text = Utils.XLAT("leader_board_rank") + (this._myRank <= 0L ? "-" : this._myRank.ToString());
    this.spiritProgress.value = num != finalData || finalData != 0L ? (float) num / (float) finalData : 1f;
    NGUITools.SetActive(this.dungeonEndTime.transform.parent.gameObject, dragonKnightData.CurrentDungeonEndTime - NetServerTime.inst.ServerTimestamp > 0);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.dungeonPlayerTexture, "Texture/DragonKnight/Portrait/" + (PlayerData.inst.dragonKnightData.Gender != DragonKnightGender.Male ? "dk_portrait_female" : "dk_portrait_male"), (System.Action<bool>) null, false, true, string.Empty);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_dungeon_finish");
    if (itemStaticInfo != null)
    {
      this.dungeonFinishName.text = Utils.XLAT("dragon_knight_recall_button");
      BuilderFactory.Instance.HandyBuild((UIWidget) this.dungeonFinishTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    }
    NGUITools.SetActive(this.dungeonFinishHint.gameObject, this.IsDungeonReset());
    NGUITools.SetActive(this.dungeonEnterButton.gameObject, !this.IsDungeonReset());
  }

  private void ShowDungeonRankingContent()
  {
    this.ClearData();
    if (this._dungeonRankList != null && this._dungeonRankList.Count > 0)
    {
      for (int key = 0; key < this._dungeonRankList.Count; ++key)
      {
        string empty1 = string.Empty;
        string empty2 = string.Empty;
        string empty3 = string.Empty;
        string empty4 = string.Empty;
        int outData1 = 0;
        int outData2 = 0;
        Hashtable dungeonRank = this._dungeonRankList[key] as Hashtable;
        DatabaseTools.UpdateData(dungeonRank, "name", ref empty1);
        DatabaseTools.UpdateData(dungeonRank, "acronym", ref empty2);
        DatabaseTools.UpdateData(dungeonRank, "portrait", ref empty3);
        DatabaseTools.UpdateData(dungeonRank, "icon", ref empty4);
        DatabaseTools.UpdateData(dungeonRank, "level", ref outData1);
        DatabaseTools.UpdateData(dungeonRank, "lord_title", ref outData2);
        DungeonRankItemRenderer itemRenderer = this.CreateItemRenderer(empty1, empty2, empty3, empty4, outData2, outData1, key + 1);
        this._itemDict.Add(key, itemRenderer);
      }
      NGUITools.SetActive(this.noRankTipsNode, false);
    }
    else
      NGUITools.SetActive(this.noRankTipsNode, true);
    this.Reposition();
  }

  private void OnDungeonDataUpdated(DragonKnightDungeonData dungeonData)
  {
    this.UpdateUI();
  }

  private void OnDragonKnightDataUpdated(DragonKnightData dragonKnightData)
  {
    if (dragonKnightData.CurrentDungeonEndTime > 0)
      return;
    this.UpdateUI();
  }

  private void OnDungeonReset(object data)
  {
    this.UpdateUI();
  }

  private void OnSecondHandler(int time)
  {
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (dragonKnightData != null)
    {
      int time1 = dragonKnightData.CurrentDungeonEndTime - NetServerTime.inst.ServerTimestamp;
      this.dungeonEndTime.text = Utils.FormatTime(time1, true, false, true);
      if (time1 <= 0)
        NGUITools.SetActive(this.dungeonEndTime.transform.parent.gameObject, time1 > 0);
      this.dungeonEnterButtonText.text = Utils.XLAT(dragonKnightData == null || !dragonKnightData.IsInDungeon ? "dragon_knight_uppercase_explore" : "dragon_knight_uppercase_continue_exploring");
    }
    NGUITools.SetActive(this.dungeonFinishHint.gameObject, this.IsDungeonReset());
    NGUITools.SetActive(this.dungeonEnterButton.gameObject, !this.IsDungeonReset());
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
    DBManager.inst.DB_DragonKnightDungeon.onDataUpdate += new System.Action<DragonKnightDungeonData>(this.OnDungeonDataUpdated);
    DBManager.inst.DB_DragonKnight.onDataUpdate += new System.Action<DragonKnightData>(this.OnDragonKnightDataUpdated);
    MessageHub.inst.GetPortByAction("dragon_knight_dungeon_time_out").AddEvent(new System.Action<object>(this.OnDungeonReset));
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
    DBManager.inst.DB_DragonKnightDungeon.onDataUpdate -= new System.Action<DragonKnightDungeonData>(this.OnDungeonDataUpdated);
    DBManager.inst.DB_DragonKnight.onDataUpdate -= new System.Action<DragonKnightData>(this.OnDragonKnightDataUpdated);
    MessageHub.inst.GetPortByAction("dragon_knight_dungeon_time_out").RemoveEvent(new System.Action<object>(this.OnDungeonReset));
  }

  public class Parameter : Popup.PopupParameter
  {
    public ArrayList dungeonRankList;
    public long myRank;
    public int rankEndTime;
  }
}
