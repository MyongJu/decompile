﻿// Decompiled with JetBrains decompiler
// Type: CancelTrainPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CancelTrainPopup : PopupBase
{
  public System.Action onOK;
  public System.Action onNO;
  [SerializeField]
  private UILabel m_TitleLabel;
  [SerializeField]
  private UILabel m_ContentLabel;
  [SerializeField]
  private UILabel m_Content2Label;
  [SerializeField]
  private UILabel m_OkayLabel;
  [SerializeField]
  private UIButton m_OkayButton;
  [SerializeField]
  private UILabel m_NoayLabel;
  [SerializeField]
  private UIButton m_NoayButton;
  [SerializeField]
  private UIButton m_CloseButton;
  [SerializeField]
  private UITexture m_IconTexture;

  public void Initialize(string titleKey, string contentKey, string content2Key, string okKey, string noKey, System.Action okCallback, System.Action noCallBack, long buildingID)
  {
    this.m_TitleLabel.text = ScriptLocalization.Get(titleKey, true);
    CityManager.BuildingItem buildingFromId = CityManager.inst.GetBuildingFromID(buildingID);
    Unit_StatisticsInfo currentTrainningUnit = BarracksManager.Instance.GetCurrentTrainningUnit(buildingID);
    int num = int.Parse((JobManager.Instance.GetJob(buildingFromId.mTrainingJobID).Data as Hashtable)[(object) "troop_count"].ToString());
    Dictionary<string, string> para = new Dictionary<string, string>()
    {
      {
        "troops_name",
        ScriptLocalization.Get(currentTrainningUnit.Troop_Name_LOC_ID, true) + "[-]"
      },
      {
        "NO.",
        string.Format("[{0}]", (object) "ffaa00") + (object) num
      }
    };
    this.m_ContentLabel.text = ScriptLocalization.GetWithPara(contentKey, para, true);
    this.m_Content2Label.text = ScriptLocalization.Get(content2Key, true);
    this.m_OkayLabel.text = ScriptLocalization.Get(okKey, true);
    this.m_NoayLabel.text = ScriptLocalization.Get(noKey, true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_IconTexture, currentTrainningUnit.Troop_ICON, (System.Action<bool>) null, true, false, string.Empty);
    this.onOK = okCallback;
    this.onNO = noCallBack;
  }

  public void OnOK()
  {
    if (this.onOK != null)
      this.onOK();
    this.OnClose();
  }

  public void OnNo()
  {
    if (this.onNO != null)
      this.onNO();
    this.OnClose();
  }

  public void OnClose()
  {
    this.Close();
    BuilderFactory.Instance.Release((UIWidget) this.m_IconTexture);
  }
}
