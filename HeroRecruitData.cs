﻿// Decompiled with JetBrains decompiler
// Type: HeroRecruitData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public abstract class HeroRecruitData
{
  public abstract bool IsFirstUsed();

  public abstract long GetFreeCD();

  public abstract string GetTip();

  public abstract string GetTip2();

  public abstract long GetPrice();

  public abstract long GetCurrency();

  public abstract int GetLeftCD();

  public abstract bool IsFreeNow();

  public abstract bool CanShowTimer();

  public abstract int GetSummonType();

  public abstract void ShowStore(int price);

  public abstract void GotoResultDialog(HeroRecruitPayload payload);

  public abstract string GetIcon();

  public abstract string GetImagePath();

  public LegendTempleData LegendTemple
  {
    get
    {
      return DBManager.inst.DB_LegendTemple.Get(PlayerData.inst.uid);
    }
  }

  public abstract bool IsLocked { get; }
}
