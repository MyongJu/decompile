﻿// Decompiled with JetBrains decompiler
// Type: AlliancePortalInfoPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UI;

public class AlliancePortalInfoPopup : Popup
{
  public UIButton demolishBtn;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateDemolishBtnState();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnOkayBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnDemolishBtnPressed()
  {
    AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
    if (alliancePortalData == null)
      return;
    string content = string.Format(ScriptLocalization.Get("alliance_buildings_confirm_demolish_description", true), (object) ScriptLocalization.Get(AllianceBuildUtils.GetAllianceBuildingName(TileType.AlliancePortal, alliancePortalData.Location), true));
    string title = ScriptLocalization.Get("alliance_buildings_confirm_placement_button", true);
    string left = ScriptLocalization.Get("id_uppercase_confirm", true);
    string right = ScriptLocalization.Get("id_uppercase_cancel", true);
    UIManager.inst.ShowConfirmationBox(title, content, left, right, ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.ConfirmDemolish), (System.Action) null, (System.Action) null);
  }

  private void ConfirmDemolish()
  {
    AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
    if (alliancePortalData == null)
      return;
    Hashtable postData = new Hashtable();
    postData[(object) "k"] = (object) alliancePortalData.Location.K;
    postData[(object) "x"] = (object) alliancePortalData.Location.X;
    postData[(object) "y"] = (object) alliancePortalData.Location.Y;
    MessageHub.inst.GetPortByAction("Map:takeBackAllianceBuilding").SendRequest(postData, (System.Action<bool, object>) null, true);
    this.OnCloseBtnPressed();
  }

  private void UpdateDemolishBtnState()
  {
    this.demolishBtn.isEnabled = Utils.IsR5Member() || Utils.IsR4Member();
  }
}
