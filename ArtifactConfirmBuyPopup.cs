﻿// Decompiled with JetBrains decompiler
// Type: ArtifactConfirmBuyPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class ArtifactConfirmBuyPopup : Popup
{
  private const int ARTIFACT_BUY_MAX_COUNT = 1;
  public UILabel artifactName;
  public UILabel artifactDesc;
  public UITexture artifactTexture;
  public UITexture artifactFrame;
  public UIInput buyCountInput;
  public UISlider buyCountSlider;
  public UILabel artifactNormalPrice;
  public UILabel artifactSpecialPrice;
  public UILabel artifactNoSpecialPrice;
  private int _artifactShopId;
  private long _normalPrice;
  private long _specialPrice;
  private System.Action _onBuyFinished;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    ArtifactConfirmBuyPopup.Parameter parameter = orgParam as ArtifactConfirmBuyPopup.Parameter;
    if (parameter != null)
    {
      this._artifactShopId = parameter.artifactShopId;
      this._normalPrice = parameter.normalPrice;
      this._specialPrice = parameter.specialPrice;
      this._onBuyFinished = parameter.onBuyFinished;
    }
    this.buyCountSlider.value = 1f;
    this.buyCountSlider.enabled = false;
    this.buyCountInput.value = 1.ToString();
    this.UpdateUI();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnBuyBtnPressed()
  {
    ArtifactSalePayload.Instance.BuyLimitedArtifact(this._artifactShopId, 1, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      if (this._onBuyFinished != null)
        this._onBuyFinished();
      ArtifactShopInfo artifactShopInfo = ConfigManager.inst.DB_ArtifactShop.Get(this._artifactShopId);
      if (artifactShopInfo != null)
      {
        ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(artifactShopInfo.artifactId);
        if (artifactInfo != null)
        {
          AudioManager.Instance.StopAndPlaySound("sfx_item_buy");
          ToastManager.Instance.AddBasicToast(artifactInfo.ImagePath, "toast_item_buy_item_title", "toast_artifact_buy_success").SetPriority(-1);
        }
      }
      this.OnCloseBtnPressed();
    }));
  }

  private void UpdateUI()
  {
    NGUITools.SetActive(this.artifactNormalPrice.transform.parent.gameObject, this._specialPrice != this._normalPrice && this._specialPrice >= 0L);
    NGUITools.SetActive(this.artifactNoSpecialPrice.transform.parent.gameObject, this._specialPrice == this._normalPrice || this._specialPrice < 0L);
    ArtifactShopInfo artifactShopInfo = ConfigManager.inst.DB_ArtifactShop.Get(this._artifactShopId);
    if (artifactShopInfo != null)
    {
      ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(artifactShopInfo.artifactId);
      if (artifactInfo != null)
      {
        this.artifactName.text = artifactInfo.Name;
        if (artifactInfo.activeEffect == "magic_source_stone")
          this.artifactDesc.text = artifactInfo.GetSpecialDescription((object) artifactInfo.deleteTypeParam, (object) (int) artifactInfo.activeEffectValue);
        else
          this.artifactDesc.text = artifactInfo.Description;
        BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactFrame, artifactInfo.TimeLimitedArtifactFramePath, (System.Action<bool>) null, true, false, string.Empty);
        BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactTexture, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      }
    }
    UILabel artifactNormalPrice = this.artifactNormalPrice;
    string str1 = Utils.FormatThousands(this._normalPrice.ToString());
    this.artifactNoSpecialPrice.text = str1;
    string str2 = str1;
    artifactNormalPrice.text = str2;
    this.artifactSpecialPrice.text = Utils.FormatThousands(this._specialPrice.ToString());
  }

  public class Parameter : Popup.PopupParameter
  {
    public long specialPrice = -1;
    public int artifactShopId;
    public long normalPrice;
    public System.Action onBuyFinished;
  }
}
