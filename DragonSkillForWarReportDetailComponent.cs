﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillForWarReportDetailComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DragonSkillForWarReportDetailComponent : DragonSkillForWarReportComponent
{
  public UILabel noDragon;

  public override void Init()
  {
    base.Init();
    this.gameObject.SetActive(true);
    if (!(this.data is DragonSkillForWarReportComponentData))
    {
      this.dragon.gameObject.SetActive(false);
      this.skillList.gameObject.SetActive(false);
      this.noDragon.gameObject.SetActive(true);
    }
    else
      this.noDragon.gameObject.SetActive(false);
  }
}
