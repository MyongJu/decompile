﻿// Decompiled with JetBrains decompiler
// Type: ThirdPartyPushManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class ThirdPartyPushManager
{
  private static readonly ThirdPartyPushManager _instance = new ThirdPartyPushManager();
  private AndroidJavaClass brige;

  private ThirdPartyPushManager()
  {
    try
    {
      this.brige = new AndroidJavaClass("com.funplus.kingofavalon.push.PushBrige");
    }
    catch (Exception ex)
    {
      Debug.Log((object) ("Init PushBrige error: " + ex.Message));
      this.brige = (AndroidJavaClass) null;
    }
  }

  public static ThirdPartyPushManager Instance
  {
    get
    {
      return ThirdPartyPushManager._instance;
    }
  }

  public void SetAlias(string fpid)
  {
    if (this.brige == null)
      return;
    this.brige.CallStatic("setAlias", new object[1]
    {
      (object) fpid
    });
  }

  public void UnsetAlias(string fpid)
  {
    if (this.brige == null)
      return;
    this.brige.CallStatic("unsetAlias", new object[1]
    {
      (object) fpid
    });
  }

  public string GetPushService()
  {
    if (this.brige == null)
      return string.Empty;
    return this.brige.CallStatic<string>("getPushService");
  }
}
