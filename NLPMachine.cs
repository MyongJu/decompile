﻿// Decompiled with JetBrains decompiler
// Type: NLPMachine
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Chat;
using Funplus.Internal;
using NLPTrans;
using System;
using System.Collections;
using System.Collections.Generic;

public class NLPMachine
{
  public const int MAX_TIME_OUT = 10;

  public void onMessageReceived(ChatMessage message)
  {
    if (PlayerData.inst.NLPTypeValue != PlayerData.NLPType.TEST || message.traceData.type != ChatTraceData.TraceType.Empty || message.senderUid == PlayerData.inst.uid)
      return;
    this.Translate(message.chatText, Language.TranslateTarget, message.language, message.senderUid.ToString(), ChatDlg.openType.ToString(), (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      Hashtable hashtable1 = Utils.Json2Object(arg2.ToString(), true) as Hashtable;
      if (hashtable1 == null || !hashtable1.ContainsKey((object) "errorCode") || int.Parse(hashtable1[(object) "errorCode"].ToString()) != 0)
        return;
      Hashtable hashtable2 = hashtable1[(object) "translation"] as Hashtable;
      if (hashtable2 == null || !hashtable2.ContainsKey((object) "targetText"))
        return;
      message.translateText = hashtable2[(object) "targetText"].ToString();
      message.showTranslated = true;
      if (hashtable2.ContainsKey((object) "source"))
        message.language = hashtable2[(object) "source"].ToString();
      message.translateType = Language.TranslateTarget;
    }));
  }

  public void Translate(string source, string targetLanguage, string sourceLanguage, string uid, string usingCase, System.Action<bool, object> callback)
  {
    string str = new DateTime(1970, 1, 1).AddSeconds((double) NetServerTime.inst.ServerTimestamp).ToString("yyyy-MM-dd\\THH:mm:ss\\Z");
    IDictionary<string, string> parameters = (IDictionary<string, string>) new Dictionary<string, string>();
    parameters.Add(nameof (source), sourceLanguage);
    parameters.Add("target", targetLanguage);
    parameters.Add("q", source);
    parameters.Add("timeStamp", str);
    parameters.Add("appId", FunplusSettings.FunplusGameId);
    parameters.Add("userId", uid);
    if (PlayerData.inst.NLPEngineValue == PlayerData.NLPEngine.BING)
      parameters.Add("debug", "2");
    NLPParam nlpParam = NLPUtils.PrepareRequest(parameters, "/api/v2/translate", "http://translate.funplusgame.com/api/v2/translate");
    LoaderInfo info = LoaderManager.inst.PopInfo();
    info.Action = "nlpTranslate";
    info.Type = 6;
    info.TimeOut = 10;
    info.AutoRetry = false;
    info.URL = nlpParam.url;
    info.RawPostData = nlpParam.requestContent;
    info.Headers = nlpParam.configHeaders;
    info.OnCallback = callback;
    LoaderBatch batch = LoaderManager.inst.PopBatch();
    batch.Type = info.Type;
    batch.Add(info);
    LoaderManager.inst.Add(batch, false, true);
    batch.processEvent += new System.Action<LoaderInfo>(this.OnOneFileDone);
  }

  protected virtual void OnOneFileDone(LoaderInfo info)
  {
    if (!string.IsNullOrEmpty(info.ErrorMsg))
    {
      D.warn((object) ("[Loading Error]:" + info.ErrorMsg + " Action =" + info.Action));
    }
    else
    {
      if (info.OnCallback == null)
        return;
      info.OnCallback(true, (object) Utils.ByteArray2String(info.ResData));
    }
  }
}
