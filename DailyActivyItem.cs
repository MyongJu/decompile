﻿// Decompiled with JetBrains decompiler
// Type: DailyActivyItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DailyActivyItem : MonoBehaviour
{
  public UILabel title;
  public UISprite lockImage;
  public UILabel currentProgressValue;
  public UILabel CompleteProgressValue;
  public UILabel requireProgressValue;
  public UITexture icon;
  private int currentActivyId;
  private bool isLock;
  public ComplexTypeIcon complexIcon;

  public void DisplayDailyActivy()
  {
    if (this.isLock)
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_daily_reward_requirement_tip", new Dictionary<string, string>()
      {
        {
          "num",
          ConfigManager.inst.DB_DailyActivies.GetDailyActivyInfo(this.currentActivyId).UnlockLevel.ToString()
        }
      }, true), (System.Action) null, 4f, false);
    else
      UIManager.inst.OpenPopup("DailyActivy/DailyActivyDetailPopUp", (Popup.PopupParameter) new DailyActiveDetailPopup.Parameter()
      {
        activy_id = this.currentActivyId
      });
  }

  public void Refresh()
  {
    DailyActivyInfo dailyActivyInfo = ConfigManager.inst.DB_DailyActivies.GetDailyActivyInfo(this.currentActivyId);
    if (dailyActivyInfo == null)
      return;
    this.SetData(dailyActivyInfo);
  }

  public void Clear()
  {
    this.complexIcon.Clear();
  }

  public void SetData(DailyActivyInfo info)
  {
    this.currentActivyId = info.internalId;
    string imageType = info.ImageType;
    ComplexTypeIcon.ImageType type;
    ComplexTypeIcon.SubType subType;
    if (imageType != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DailyActivyItem.\u003C\u003Ef__switch\u0024map4F == null)
      {
        // ISSUE: reference to a compiler-generated field
        DailyActivyItem.\u003C\u003Ef__switch\u0024map4F = new Dictionary<string, int>(2)
        {
          {
            "building",
            0
          },
          {
            "daily",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DailyActivyItem.\u003C\u003Ef__switch\u0024map4F.TryGetValue(imageType, out num))
      {
        switch (num)
        {
          case 0:
            type = ComplexTypeIcon.ImageType.GO;
            subType = ComplexTypeIcon.SubType.Building;
            goto label_8;
          case 1:
            type = ComplexTypeIcon.ImageType.UITexture;
            subType = ComplexTypeIcon.SubType.Daily;
            goto label_8;
        }
      }
    }
    type = ComplexTypeIcon.ImageType.UITexture;
    subType = ComplexTypeIcon.SubType.Items;
label_8:
    this.complexIcon.SetData(info.Image, type, subType);
    DailyActivyData dailyActivyData = DBManager.inst.DB_DailyActives.Get(this.currentActivyId);
    this.title.text = info.Name;
    NGUITools.SetActive(this.currentProgressValue.gameObject, false);
    NGUITools.SetActive(this.CompleteProgressValue.gameObject, false);
    NGUITools.SetActive(this.requireProgressValue.gameObject, false);
    NGUITools.SetActive(this.lockImage.gameObject, false);
    if (info.UnlockLevel > CityManager.inst.mStronghold.mLevel)
    {
      this.isLock = true;
      NGUITools.SetActive(this.lockImage.gameObject, true);
      NGUITools.SetActive(this.currentProgressValue.gameObject, false);
      NGUITools.SetActive(this.CompleteProgressValue.gameObject, false);
      NGUITools.SetActive(this.requireProgressValue.gameObject, true);
      GreyUtility.Grey(this.icon.gameObject);
      this.requireProgressValue.text = ScriptLocalization.GetWithPara("tavern_daily_rewards_unlock_requirement", new Dictionary<string, string>()
      {
        {
          "num",
          info.UnlockLevel.ToString()
        }
      }, true);
    }
    else
    {
      this.isLock = false;
      GreyUtility.Normal(this.icon.gameObject);
      float num = 0.0f;
      int maxScore = info.MaxScore;
      if (dailyActivyData != null)
      {
        num = Mathf.Floor((float) dailyActivyData.Progress / (float) info.Value * (float) info.Score);
        if ((double) num > (double) maxScore)
          num = (float) maxScore;
      }
      NGUITools.SetActive(this.CompleteProgressValue.gameObject, (double) num >= (double) maxScore);
      NGUITools.SetActive(this.currentProgressValue.gameObject, (double) num < (double) maxScore);
      this.currentProgressValue.text = num.ToString() + "/" + maxScore.ToString();
      this.CompleteProgressValue.text = num.ToString() + "/" + maxScore.ToString();
    }
  }
}
