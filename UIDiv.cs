﻿// Decompiled with JetBrains decompiler
// Type: UIDiv
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class UIDiv : UITable
{
  public int Xbound = 500;
  public int rows = 1;

  protected override void RepositionVariableSize(List<Transform> children)
  {
    this.rows = 1;
    float num1 = 0.0f;
    float num2 = 0.0f;
    float a = 0.0f;
    Bounds[] boundsArray = new Bounds[children.Count];
    int num3 = 0;
    int num4 = 0;
    int index1 = 0;
    for (int count = children.Count; index1 < count; ++index1)
    {
      Transform child = children[index1];
      Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(child, !this.hideInactive);
      Vector3 localScale = child.localScale;
      relativeWidgetBounds.min = Vector3.Scale(relativeWidgetBounds.min, localScale);
      relativeWidgetBounds.max = Vector3.Scale(relativeWidgetBounds.max, localScale);
      boundsArray[index1] = relativeWidgetBounds;
      if (++num3 >= this.columns && this.columns > 0)
      {
        num3 = 0;
        ++num4;
      }
    }
    int num5 = 0;
    int num6 = 0;
    Vector2 pivotOffset = NGUIMath.GetPivotOffset(this.cellAlignment);
    int index2 = 0;
    for (int count = children.Count; index2 < count; ++index2)
    {
      Transform child = children[index2];
      Bounds bounds1 = boundsArray[index2];
      Bounds bounds2 = bounds1;
      Bounds bounds3 = bounds1;
      if ((double) num1 + (double) bounds2.size.x + (double) this.padding.x * 2.0 > (double) this.Xbound)
      {
        num5 = 0;
        ++num6;
        ++this.rows;
        float num7 = 0.0f;
        num2 += a + this.padding.y * 2f;
        Vector3 localPosition = child.localPosition;
        localPosition.x = num7 + bounds1.extents.x - bounds1.center.x;
        localPosition.x -= Mathf.Lerp(0.0f, bounds1.max.x - bounds1.min.x - bounds2.max.x + bounds2.min.x, pivotOffset.x) - this.padding.x;
        if (this.direction == UITable.Direction.Down)
        {
          localPosition.y = -num2 - bounds1.extents.y - bounds1.center.y;
          localPosition.y += Mathf.Lerp(bounds1.max.y - bounds1.min.y - bounds3.max.y + bounds3.min.y, 0.0f, pivotOffset.y) - this.padding.y;
        }
        else
        {
          localPosition.y = num2 + bounds1.extents.y - bounds1.center.y;
          localPosition.y -= Mathf.Lerp(0.0f, bounds1.max.y - bounds1.min.y - bounds3.max.y + bounds3.min.y, pivotOffset.y) - this.padding.y;
        }
        child.localPosition = localPosition;
        num1 = num7 + (bounds2.size.x + this.padding.x * 2f);
        a = boundsArray[index2].size.y;
      }
      else
      {
        Vector3 localPosition = child.localPosition;
        localPosition.x = num1 + bounds1.extents.x - bounds1.center.x;
        localPosition.x -= Mathf.Lerp(0.0f, bounds1.max.x - bounds1.min.x - bounds2.max.x + bounds2.min.x, pivotOffset.x) - this.padding.x;
        if (this.direction == UITable.Direction.Down)
        {
          localPosition.y = -num2 - bounds1.extents.y - bounds1.center.y;
          localPosition.y += Mathf.Lerp(bounds1.max.y - bounds1.min.y - bounds3.max.y + bounds3.min.y, 0.0f, pivotOffset.y) - this.padding.y;
        }
        else
        {
          localPosition.y = num2 + bounds1.extents.y - bounds1.center.y;
          localPosition.y -= Mathf.Lerp(0.0f, bounds1.max.y - bounds1.min.y - bounds3.max.y + bounds3.min.y, pivotOffset.y) - this.padding.y;
        }
        child.localPosition = localPosition;
        num1 += bounds2.size.x + this.padding.x * 2f;
        a = Mathf.Max(a, boundsArray[index2].size.y);
      }
    }
    if (this.pivot == UIWidget.Pivot.TopLeft)
      return;
    pivotOffset = NGUIMath.GetPivotOffset(this.pivot);
    Bounds relativeWidgetBounds1 = NGUIMath.CalculateRelativeWidgetBounds(this.transform);
    float num8 = Mathf.Lerp(0.0f, relativeWidgetBounds1.size.x, pivotOffset.x);
    float num9 = Mathf.Lerp(-relativeWidgetBounds1.size.y, 0.0f, pivotOffset.y);
    Transform transform = this.transform;
    for (int index3 = 0; index3 < transform.childCount; ++index3)
    {
      Transform child = transform.GetChild(index3);
      SpringPosition component = child.GetComponent<SpringPosition>();
      if ((Object) component != (Object) null)
      {
        component.target.x -= num8;
        component.target.y -= num9;
      }
      else
      {
        Vector3 localPosition = child.localPosition;
        localPosition.x -= num8;
        localPosition.y -= num9;
        child.localPosition = localPosition;
      }
    }
  }

  [ContextMenu("Execute")]
  public override void Reposition()
  {
    if (Application.isPlaying && !this.mInitDone && NGUITools.GetActive((Behaviour) this))
      this.Init();
    this.mReposition = false;
    Transform transform = this.transform;
    List<Transform> childList = this.GetChildList();
    if (childList.Count > 0)
      this.RepositionVariableSize(childList);
    if (this.keepWithinPanel && (Object) this.mPanel != (Object) null)
    {
      this.mPanel.ConstrainTargetToBounds(transform, true);
      UIScrollView component = this.mPanel.GetComponent<UIScrollView>();
      if ((Object) component != (Object) null)
        component.UpdateScrollbars(true);
    }
    if (this.onReposition == null)
      return;
    this.onReposition();
  }
}
