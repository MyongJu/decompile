﻿// Decompiled with JetBrains decompiler
// Type: AllianceQuestUIPresent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class AllianceQuestUIPresent : BasePresent
{
  private TimerQuestData data;

  public override int IconType
  {
    get
    {
      return 1;
    }
  }

  protected TimerQuestData Data
  {
    get
    {
      if (this.data == null)
        this.data = DBManager.inst.DB_Local_TimerQuest.Get(this.DataID);
      return this.data;
    }
  }

  public bool NeedUpgardeBar
  {
    get
    {
      return this.Data.jobId > 0L;
    }
  }

  protected JobHandle Job
  {
    get
    {
      return JobManager.Instance.GetJob(this.Data.jobId);
    }
  }

  public override void Refresh()
  {
    this.ProgressValue = 0.0f;
    this.ProgressContent = string.Empty;
    if (this.Job == null)
      return;
    this.ProgressValue = (float) (1.0 - (double) this.Job.LeftTime() / (double) this.Job.Duration());
    this.ProgressContent = Utils.FormatTime(this.Job.LeftTime(), false, false, true);
  }
}
