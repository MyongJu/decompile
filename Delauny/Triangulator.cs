﻿// Decompiled with JetBrains decompiler
// Type: Delauny.Triangulator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Delauny
{
  public static class Triangulator
  {
    public static bool TriangulatePolygonSubFunc_InCircle(Vector2 p, Vector2 p1, Vector2 p2, Vector2 p3)
    {
      if ((double) Mathf.Abs(p1.y - p2.y) < 1.40129846432482E-45 && (double) Mathf.Abs(p2.y - p3.y) < 1.40129846432482E-45)
        return false;
      float num1;
      float num2;
      if ((double) Mathf.Abs(p2.y - p1.y) < 1.40129846432482E-45)
      {
        float num3 = (float) (-((double) p3.x - (double) p2.x) / ((double) p3.y - (double) p2.y));
        float num4 = (float) (((double) p2.x + (double) p3.x) * 0.5);
        float num5 = (float) (((double) p2.y + (double) p3.y) * 0.5);
        num1 = (float) (((double) p2.x + (double) p1.x) * 0.5);
        num2 = num3 * (num1 - num4) + num5;
      }
      else if ((double) Mathf.Abs(p3.y - p2.y) < 1.40129846432482E-45)
      {
        float num3 = (float) (-((double) p2.x - (double) p1.x) / ((double) p2.y - (double) p1.y));
        float num4 = (float) (((double) p1.x + (double) p2.x) * 0.5);
        float num5 = (float) (((double) p1.y + (double) p2.y) * 0.5);
        num1 = (float) (((double) p3.x + (double) p2.x) * 0.5);
        num2 = num3 * (num1 - num4) + num5;
      }
      else
      {
        float num3 = (float) (-((double) p2.x - (double) p1.x) / ((double) p2.y - (double) p1.y));
        float num4 = (float) (-((double) p3.x - (double) p2.x) / ((double) p3.y - (double) p2.y));
        float num5 = (float) (((double) p1.x + (double) p2.x) * 0.5);
        float num6 = (float) (((double) p2.x + (double) p3.x) * 0.5);
        float num7 = (float) (((double) p1.y + (double) p2.y) * 0.5);
        float num8 = (float) (((double) p2.y + (double) p3.y) * 0.5);
        num1 = (float) (((double) num3 * (double) num5 - (double) num4 * (double) num6 + (double) num8 - (double) num7) / ((double) num3 - (double) num4));
        num2 = num3 * (num1 - num5) + num7;
      }
      float num9 = p2.x - num1;
      float num10 = p2.y - num2;
      float num11 = (float) ((double) num9 * (double) num9 + (double) num10 * (double) num10);
      float num12 = p.x - num1;
      float num13 = p.y - num2;
      return (double) num12 * (double) num12 + (double) num13 * (double) num13 <= (double) num11;
    }

    public static List<Triangle> TriangulatePolygon(List<Vector3> XZofVertices)
    {
      int count = XZofVertices.Count;
      float x1 = XZofVertices[0].x;
      float y = XZofVertices[0].y;
      float num1 = x1;
      float num2 = y;
      for (int index = 1; index < count; ++index)
      {
        if ((double) XZofVertices[index].x < (double) x1)
          x1 = XZofVertices[index].x;
        else if ((double) XZofVertices[index].x > (double) num1)
          num1 = XZofVertices[index].x;
        if ((double) XZofVertices[index].y < (double) y)
          y = XZofVertices[index].y;
        else if ((double) XZofVertices[index].y > (double) num2)
          num2 = XZofVertices[index].y;
      }
      float num3 = num1 - x1;
      float num4 = num2 - y;
      float num5 = (double) num3 <= (double) num4 ? num4 : num3;
      float x2 = (float) (((double) num1 + (double) x1) * 0.5);
      float num6 = (float) (((double) num2 + (double) y) * 0.5);
      Vector2[] vector2Array = new Vector2[3 + count];
      for (int index = 0; index < count; ++index)
        vector2Array[index] = (Vector2) XZofVertices[index];
      vector2Array[count] = new Vector2(x2 - 2f * num5, num6 - num5);
      vector2Array[count + 1] = new Vector2(x2, num6 + 2f * num5);
      vector2Array[count + 2] = new Vector2(x2 + 2f * num5, num6 - num5);
      List<Triangle> triangleList = new List<Triangle>();
      triangleList.Add(new Triangle(count, count + 1, count + 2));
      for (int point3 = 0; point3 < count; ++point3)
      {
        List<Edge> edgeList = new List<Edge>();
        for (int index = 0; index < triangleList.Count; ++index)
        {
          if (Triangulator.TriangulatePolygonSubFunc_InCircle(vector2Array[point3], vector2Array[triangleList[index].p1], vector2Array[triangleList[index].p2], vector2Array[triangleList[index].p3]))
          {
            edgeList.Add(new Edge(triangleList[index].p1, triangleList[index].p2));
            edgeList.Add(new Edge(triangleList[index].p2, triangleList[index].p3));
            edgeList.Add(new Edge(triangleList[index].p3, triangleList[index].p1));
            triangleList.RemoveAt(index);
            --index;
          }
        }
        if (point3 < count)
        {
          for (int index1 = edgeList.Count - 2; index1 >= 0; --index1)
          {
            for (int index2 = edgeList.Count - 1; index2 >= index1 + 1; --index2)
            {
              if (edgeList[index1].Equals(edgeList[index2]))
              {
                edgeList.RemoveAt(index2);
                edgeList.RemoveAt(index1);
                --index2;
              }
            }
          }
          for (int index = 0; index < edgeList.Count; ++index)
            triangleList.Add(new Triangle(edgeList[index].p1, edgeList[index].p2, point3));
          edgeList.Clear();
        }
      }
      for (int index = triangleList.Count - 1; index >= 0; --index)
      {
        if (triangleList[index].p1 >= count || triangleList[index].p2 >= count || triangleList[index].p3 >= count)
          triangleList.RemoveAt(index);
      }
      triangleList.TrimExcess();
      return triangleList;
    }
  }
}
