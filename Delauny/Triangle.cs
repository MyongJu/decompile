﻿// Decompiled with JetBrains decompiler
// Type: Delauny.Triangle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Delauny
{
  public struct Triangle
  {
    public int p1;
    public int p2;
    public int p3;

    public Triangle(int point1, int point2, int point3)
    {
      this.p1 = point1;
      this.p2 = point2;
      this.p3 = point3;
    }
  }
}
