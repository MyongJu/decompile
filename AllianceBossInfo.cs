﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class AllianceBossInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "stage")]
  public int stage;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "image")]
  public string image;
  [Config(Name = "boss_level")]
  public int bossLevel;
  [Config(Name = "max_round")]
  public int maxRound;
  [Config(Name = "fund")]
  public long fund;
  [Config(Name = "honor")]
  public long honor;
  [Config(Name = "unit_id_1")]
  public string unitId1;
  [Config(Name = "unit_value_1")]
  public int unitValue1;
  [Config(Name = "unit_id_2")]
  public string unitId2;
  [Config(Name = "unit_value_2")]
  public int unitValue2;
  [Config(Name = "unit_id_3")]
  public string unitId3;
  [Config(Name = "unit_value_3")]
  public int unitValue3;
  [Config(Name = "unit_id_4")]
  public string unitId4;
  [Config(Name = "unit_value_4")]
  public int unitValue4;
  [Config(Name = "unit_id_5")]
  public string unitId5;
  [Config(Name = "unit_value_5")]
  public int unitValue5;
  [Config(Name = "unit_id_6")]
  public string unitId6;
  [Config(Name = "unit_value_6")]
  public int unitValue6;
  [Config(Name = "unit_id_7")]
  public string unitId7;
  [Config(Name = "unit_value_7")]
  public int unitValue7;
  [Config(Name = "unit_id_8")]
  public string unitId8;
  [Config(Name = "unit_value_8")]
  public int unitValue8;
  [Config(Name = "unit_id_9")]
  public string unitId9;
  [Config(Name = "unit_value_9")]
  public int unitValue9;
  [Config(Name = "unit_id_10")]
  public string unitId10;
  [Config(Name = "unit_value_10")]
  public int unitValue10;
  [Config(Name = "reward_id_1")]
  public int rewardId1;
  [Config(Name = "reward_value_1")]
  public int rewardValue1;
  [Config(Name = "reward_id_2")]
  public int rewardId2;
  [Config(Name = "reward_value_2")]
  public int rewardValue2;
  [Config(Name = "reward_id_3")]
  public int rewardId3;
  [Config(Name = "reward_value_3")]
  public int rewardValue3;
  [Config(Name = "reward_id_4")]
  public int rewardId4;
  [Config(Name = "reward_value_4")]
  public int rewardValue4;
  [Config(Name = "reward_id_5")]
  public int rewardId5;
  [Config(Name = "reward_value_5")]
  public int rewardValue5;
  [Config(Name = "reward_id_6")]
  public int rewardId6;
  [Config(Name = "reward_value_6")]
  public int rewardValue6;
  protected List<Reward.RewardsValuePair> _AllRewards;

  public int TroopCount
  {
    get
    {
      return this.unitValue1 + this.unitValue2 + this.unitValue3 + this.unitValue4 + this.unitValue5 + this.unitValue6 + this.unitValue7 + this.unitValue8 + this.unitValue9 + this.unitValue10;
    }
  }

  public List<Reward.RewardsValuePair> AllRewards
  {
    get
    {
      if (this._AllRewards == null)
      {
        this._AllRewards = new List<Reward.RewardsValuePair>();
        int[] numArray1 = new int[6]
        {
          this.rewardId1,
          this.rewardId2,
          this.rewardId3,
          this.rewardId4,
          this.rewardId5,
          this.rewardId6
        };
        int[] numArray2 = new int[6]
        {
          this.rewardValue1,
          this.rewardValue2,
          this.rewardValue3,
          this.rewardValue4,
          this.rewardValue5,
          this.rewardValue6
        };
        for (int index = 0; index < numArray1.Length; ++index)
          this._AllRewards.Add(new Reward.RewardsValuePair(numArray1[index], numArray2[index]));
      }
      return this._AllRewards;
    }
  }

  public string ImagePath
  {
    get
    {
      return string.Format("Texture/AllianceBoss/{0}", (object) this.image);
    }
  }

  public string StageImagePath
  {
    get
    {
      return string.Format("Texture/AllianceBoss/alliance_boss_flag_0{0}", (object) (this.stage + 1));
    }
  }
}
