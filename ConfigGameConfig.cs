﻿// Decompiled with JetBrains decompiler
// Type: ConfigGameConfig
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigGameConfig
{
  public bool Builded;
  public Dictionary<int, GameConfigInfo> datas;
  private Dictionary<string, int> dicByUniqueId;

  public ConfigGameConfig()
  {
    this.datas = new Dictionary<int, GameConfigInfo>();
    this.dicByUniqueId = new Dictionary<string, int>();
  }

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exsit", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "value");
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          int result = 0;
          if (int.TryParse(key.ToString(), out result))
          {
            ArrayList arr = hashtable[key] as ArrayList;
            if (arr != null)
              this.Add(new GameConfigInfo(result, ConfigManager.GetString(arr, index1), ConfigManager.GetString(arr, index2)));
          }
        }
        this.Builded = true;
      }
    }
  }

  public Dictionary<int, GameConfigInfo>.ValueCollection Values
  {
    get
    {
      return this.datas.Values;
    }
  }

  private void Add(GameConfigInfo data)
  {
    if (this.datas.ContainsKey(data.InternalID))
      return;
    this.datas.Add(data.InternalID, data);
    if (!this.dicByUniqueId.ContainsKey(data.ID))
      this.dicByUniqueId.Add(data.ID, data.InternalID);
    ConfigManager.inst.AddData(data.InternalID, (object) data);
  }

  public void Clear()
  {
    if (this.datas == null)
      return;
    this.datas.Clear();
  }

  public GameConfigInfo GetData(int internalId)
  {
    if (this.datas.ContainsKey(internalId))
      return this.datas[internalId];
    return (GameConfigInfo) null;
  }

  public GameConfigInfo GetData(string uniqueId)
  {
    if (!this.dicByUniqueId.ContainsKey(uniqueId))
      return (GameConfigInfo) null;
    return this.datas[this.dicByUniqueId[uniqueId]];
  }
}
