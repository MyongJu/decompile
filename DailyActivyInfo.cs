﻿// Decompiled with JetBrains decompiler
// Type: DailyActivyInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class DailyActivyInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "score")]
  public int Score;
  [Config(Name = "value")]
  public int Value;
  [Config(Name = "max_score")]
  public int MaxScore;
  [Config(Name = "unlock_level")]
  public int UnlockLevel;
  [Config(Name = "title")]
  public string LocalName;
  [Config(Name = "description")]
  public string LocalDesc;
  [Config(Name = "image")]
  public string Image;
  [Config(Name = "goto")]
  public int GotoId;
  [Config(Name = "image_type")]
  public string ImageType;

  public string ImagePath
  {
    get
    {
      return this.Image;
    }
  }

  public string Name
  {
    get
    {
      return ScriptLocalization.Get(this.LocalName, true);
    }
  }

  public string Desc
  {
    get
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      if (this.LocalDesc.Equals("daily_reward_casino_description"))
      {
        para.Add("1", this.Score.ToString());
        para.Add("2", this.MaxScore.ToString());
      }
      else
      {
        para.Add("1", this.Value.ToString());
        para.Add("2", this.Score.ToString());
        para.Add("3", this.MaxScore.ToString());
      }
      return ScriptLocalization.GetWithPara(this.LocalDesc, para, true);
    }
  }
}
