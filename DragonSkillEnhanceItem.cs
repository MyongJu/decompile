﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillEnhanceItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;
using UnityEngine;

public class DragonSkillEnhanceItem : MonoBehaviour
{
  [SerializeField]
  private GameObject[] _rootStars;
  [SerializeField]
  private DragonSkillIcon _skillIcon;
  [SerializeField]
  private UIButton _buttonReset;
  [SerializeField]
  private GameObject _rootUnlocked;
  [SerializeField]
  private UILabel _rootLocked;
  [SerializeField]
  private GameObject _rootSelectTag;
  [SerializeField]
  private GameObject _enhanceEffectTemplate;
  [SerializeField]
  private UILabel _labelExternalLevel;
  protected int _skillId;
  protected int _starLevel;
  public System.Action<DragonSkillEnhanceItem> OnClicked;
  public System.Action<DragonSkillEnhanceItem> OnSkillIconClicked;
  public System.Action<DragonSkillEnhanceItem> OnSkillReseted;

  private void OnEnable()
  {
    this._enhanceEffectTemplate.SetActive(false);
  }

  public int StarLevel
  {
    get
    {
      return this._starLevel;
    }
  }

  public int SkillId
  {
    get
    {
      return this._skillId;
    }
  }

  public void SetSkillId(int skillId)
  {
    this._skillId = skillId;
    ConfigDragonSkillMainInfo skillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo(skillId);
    if (skillMainInfo == null)
      return;
    int extarLevel = DBManager.inst.DB_Artifact.GetExtarLevel(PlayerData.inst.uid, skillMainInfo.group_id);
    this._labelExternalLevel.text = extarLevel <= 0 ? string.Empty : "+" + (object) extarLevel;
    DragonData dragonData = PlayerData.inst.dragonData;
    if (dragonData == null)
    {
      D.error((object) "no dragon data");
    }
    else
    {
      this._skillIcon.SetSkillId(skillId);
      this._starLevel = dragonData.GetSkillGroupStarLevel(skillMainInfo.group_id);
      for (int index = 0; index < this._rootStars.Length; ++index)
        this._rootStars[index].SetActive(index < this._starLevel);
      this._buttonReset.isEnabled = this._starLevel > 0;
      bool flag = ConfigManager.inst.DB_ConfigDragonSkillGroup.CheckDragonSkillGroupIsUnLock(skillMainInfo.group_id);
      this._rootUnlocked.SetActive(flag);
      this._rootLocked.gameObject.SetActive(!flag);
      ConfigDragonSkillGroupInfo dragonSkillGroupInfo = ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupInfo(skillMainInfo.group_id);
      if (dragonSkillGroupInfo != null && dragonSkillGroupInfo.learn_method > 0)
        this._rootLocked.text = Utils.XLAT("dragon_skill_scroll_required_tip");
      else
        this._rootLocked.text = Utils.XLAT("dragon_skill_enhance_unlock_description");
    }
  }

  public void SetSelected(bool selected)
  {
    this._rootSelectTag.SetActive(selected);
  }

  public void OnItemIconClicked()
  {
    if (this.OnSkillIconClicked != null)
      this.OnSkillIconClicked(this);
    if (this.OnClicked == null)
      return;
    this.OnClicked(this);
  }

  public void OnItemClicked()
  {
    if (this.OnClicked == null)
      return;
    this.OnClicked(this);
  }

  public void OnResetButtonClicked()
  {
    ShopStaticInfo shopData = ConfigManager.inst.DB_Shop.GetShopData("shopitem_dragon_skill_enhance_reset_single");
    if (shopData != null)
    {
      ConfigDragonSkillMainInfo skillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo(this._skillId);
      if (skillMainInfo == null)
        return;
      Hashtable hashtable = new Hashtable()
      {
        {
          (object) "group_id",
          (object) skillMainInfo.group_id
        }
      };
      UIManager.inst.OpenPopup("UseOrBuyAndUseItemPopup", (Popup.PopupParameter) new UseOrBuyAndUseItemPopup.Parameter()
      {
        param = hashtable,
        itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(shopData.Item_InternalId),
        callback = new System.Action<bool, object>(this.OnResetCallback),
        titleText = Utils.XLAT("dragon_skill_enhance_reset_title"),
        btnText = Utils.XLAT("id_uppercase_confirm")
      });
    }
    else
      D.error((object) "can not find shop item: shopitem_dragon_skill_enhance_reset_single");
  }

  public void PlayEnhancedEffect()
  {
    if (this._starLevel <= 0 || this._starLevel > this._rootStars.Length)
      return;
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._enhanceEffectTemplate);
    gameObject.transform.SetParent(this._enhanceEffectTemplate.transform.parent);
    gameObject.transform.position = this._rootStars[this._starLevel - 1].transform.position;
    gameObject.SetActive(true);
    UnityEngine.Object.Destroy((UnityEngine.Object) gameObject, 3f);
  }

  protected void OnResetCallback(bool result, object data)
  {
    this.SetSkillId(this._skillId);
    if (this.OnSkillReseted == null)
      return;
    this.OnSkillReseted(this);
  }
}
