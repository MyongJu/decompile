﻿// Decompiled with JetBrains decompiler
// Type: WarReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class WarReportPopup : BaseReportPopup
{
  private List<GameObject> pool = new List<GameObject>();
  public float gapX = 100f;
  private Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>> allmytroopdetail = new Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>>();
  private Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>> allothertroopdetail = new Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>>();
  private Dictionary<string, Hashtable> allmydragondetail = new Dictionary<string, Hashtable>();
  private Dictionary<string, Hashtable> allotherdragondetail = new Dictionary<string, Hashtable>();
  private Dictionary<string, List<Hashtable>> allmylegenddetail = new Dictionary<string, List<Hashtable>>();
  private Dictionary<string, List<Hashtable>> allotherlegenddetail = new Dictionary<string, List<Hashtable>>();
  protected bool _showLegend = true;
  private const int Y_OFFSET = 100;
  private WarReportContent wrc;
  public UIScrollView sv;
  public UILabel winLbl;
  public UILabel lossLbl;
  public IconListComponent ilc;
  public GameObject disastrous;
  public GameObject infogroup;
  public GameObject artifactgroup;
  public Icon artifactIcon;
  public WarOverview mywaroverview;
  public WarOverview otherwaroverview;
  public TroopOverview mytroopoverview;
  public TroopOverview othertroopoverview;
  public GameObject boostdetailGroup;
  public BoostInfo myboostinfo;
  public BoostInfo otherboostinfo;
  public UISprite myboostbg;
  public UISprite otherboostbg;
  public GameObject rootAbyss;
  public Transform contentTransform;
  public Transform contentRootForNotAbyss;
  public Transform contentRootForAbyss;
  public GameObject btnGroup;
  public UILabel casultyDes;
  public GameObject casultObj;
  public UILabel labelMerlinTowerDefense;
  public GameObject rootMerlinTowerDefense;
  public GameObject rootMerlinTowerDefenseReward;
  public GameObject rootResouces;
  public MerlinTowerWarReportItem merlinTowerWarReportItem;
  public GameObject objAllianceWarNode;
  protected TroopLeader mytd;
  protected TroopLeader othertd;
  protected bool _isMerlinTrials;
  protected bool _isMerlinTowerFightMonster;

  private void Init()
  {
    List<IconData> res = new List<IconData>();
    this.wrc.SummarizeResourceReward(ref res);
    if (this.wrc.dragon_attr != null)
    {
      IconData iconData1 = new IconData("Texture/ItemIcons/item_dragon_exp", new string[2]
      {
        "+" + this.wrc.dragon_attr.dragon_exp,
        ScriptLocalization.Get("id_dragon_xp", true)
      });
      res.Add(iconData1);
      if (this.wrc.disastrous || this.wrc.IsAttacker())
      {
        IconData iconData2 = new IconData("Texture/Dragon/icon_dragon_power_dark", new string[2]
        {
          "+" + this.wrc.dragon_attr.dark,
          ScriptLocalization.Get("id_dragon_dark_power", true)
        });
        res.Add(iconData2);
      }
      else
      {
        IconData iconData2 = new IconData("Texture/Dragon/icon_dragon_power_light", new string[2]
        {
          "+" + this.wrc.dragon_attr.light,
          ScriptLocalization.Get("id_dragon_light_power", true)
        });
        res.Add(iconData2);
      }
    }
    if ((UnityEngine.Object) this.ilc != (UnityEngine.Object) null)
      this.ilc.FeedData((IComponentData) new IconListComponentData(res));
    NGUITools.SetActive(this.infogroup, !this.wrc.disastrous);
    NGUITools.SetActive(this.disastrous, this.wrc.disastrous);
    NGUITools.SetActive(this.artifactgroup, !this.wrc.disastrous);
    if (this.wrc.disastrous)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_loser", (System.Action<bool>) null, true, true, string.Empty);
    }
    else
    {
      if ((UnityEngine.Object) this.artifactgroup != (UnityEngine.Object) null)
      {
        NGUITools.SetActive(this.objAllianceWarNode, this.maildata.IsInAllianceWar());
        if ((UnityEngine.Object) this.objAllianceWarNode != (UnityEngine.Object) null && this.objAllianceWarNode.activeInHierarchy)
        {
          Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.objAllianceWarNode.transform);
          Utils.SetLPY(this.artifactgroup, this.artifactgroup.transform.localPosition.y - relativeWidgetBounds.size.y);
          Utils.SetLPY(this.infogroup, this.infogroup.transform.localPosition.y - relativeWidgetBounds.size.y);
        }
      }
      if (this.wrc.artifact != 0)
      {
        ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this.wrc.artifact);
        if ((UnityEngine.Object) this.artifactIcon != (UnityEngine.Object) null)
          this.artifactIcon.SetData(artifactInfo.ImagePath, new string[1]
          {
            artifactInfo.Name
          });
      }
      else if ((UnityEngine.Object) this.artifactgroup != (UnityEngine.Object) null)
        Utils.SetLPY(this.infogroup, this.infogroup.transform.localPosition.y + NGUIMath.CalculateRelativeWidgetBounds(this.artifactgroup.transform).size.y);
      NGUITools.SetActive(this.artifactgroup, 0 != this.wrc.artifact);
      this.wrc.IsWinner();
      List<TroopDetail> troopDetailList1;
      List<TroopDetail> troopDetailList2;
      if (this.wrc.IsAttacker())
      {
        this.mytd = this.wrc.attacker;
        this.mytd.SetLegend(this.wrc.legend);
        this.othertd = this.wrc.defender;
        this.othertd.SetLegend(this.wrc.legend);
        troopDetailList1 = this.wrc.attacker_troop_detail;
        troopDetailList2 = this.wrc.defender_troop_detail;
      }
      else
      {
        this.mytd = this.wrc.defender;
        this.mytd.SetLegend(this.wrc.legend);
        this.othertd = this.wrc.attacker;
        this.othertd.SetLegend(this.wrc.legend);
        troopDetailList1 = this.wrc.defender_troop_detail;
        troopDetailList2 = this.wrc.attacker_troop_detail;
      }
      this.SetLegends(troopDetailList1, this.wrc.legend);
      this.SetLegends(troopDetailList2, this.wrc.legend);
      string str1 = this.mytd.acronym == null ? (string) null : "(" + this.mytd.acronym + ")";
      string str2 = this.othertd.acronym == null ? (string) null : "(" + this.othertd.acronym + ")";
      string empty1 = string.Empty;
      string empty2 = string.Empty;
      string source1;
      string source2;
      if (this.maildata.type == MailType.MAIL_TYPE_ANNI_BATTLE_REPORT)
      {
        source1 = str1 + Utils.XLAT(this.mytd.name) + ".K" + (object) this.mytd.world_id;
        if (long.Parse(this.othertd.uid) > 6000000000L)
          source2 = str2 + Utils.XLAT(this.othertd.name);
        else
          source2 = str2 + Utils.XLAT(this.othertd.name) + ".K" + (object) this.othertd.world_id;
      }
      else
      {
        source1 = str1 + this.mytd.name;
        source2 = str2 + this.othertd.name;
      }
      long allPowerLost1 = this.wrc.CalculateAllPowerLost(troopDetailList1);
      long allPowerLost2 = this.wrc.CalculateAllPowerLost(troopDetailList2);
      long allTroopKill1 = this.wrc.CalculateAllTroopKill(troopDetailList1);
      long allTroopKill2 = this.wrc.CalculateAllTroopKill(troopDetailList2);
      long allTroopNotGood1 = this.wrc.CalculateAllTroopNotGood(troopDetailList1);
      long allTroopNotGood2 = this.wrc.CalculateAllTroopNotGood(troopDetailList2);
      long allTroopLost1 = this.wrc.CalculateAllTroopLost(troopDetailList1);
      long allTroopLost2 = this.wrc.CalculateAllTroopLost(troopDetailList2);
      long allTroopWound1 = this.wrc.CalculateAllTroopWound(troopDetailList1);
      long allTroopWound2 = this.wrc.CalculateAllTroopWound(troopDetailList2);
      long allTroopSurvive1 = this.wrc.CalculateAllTroopSurvive(troopDetailList1);
      long allTroopSurvive2 = this.wrc.CalculateAllTroopSurvive(troopDetailList2);
      this.wrc.SummarizeAllTroopDetail(troopDetailList1, ref this.allmytroopdetail);
      this.wrc.SummarizeAllTroopDetail(troopDetailList2, ref this.allothertroopdetail);
      this.wrc.SummarizeAllDragonDetail(troopDetailList1, ref this.allmydragondetail);
      this.wrc.SummarizeAllDragonDetail(troopDetailList2, ref this.allotherdragondetail);
      this.wrc.SummarizeAllLegendDetail(troopDetailList1, ref this.allmylegenddetail);
      this.wrc.SummarizeAllLegendDetail(troopDetailList2, ref this.allotherlegenddetail);
      List<BoostInfo.Data> myboostdetail = new List<BoostInfo.Data>();
      List<BoostInfo.Data> otherboostdetail = new List<BoostInfo.Data>();
      this.mytd.SummarizeBoostDetail(ref myboostdetail);
      this.othertd.SummarizeBoostDetail(ref otherboostdetail);
      myboostdetail.RemoveAll((Predicate<BoostInfo.Data>) (x => (double) x.enhance == 0.0));
      otherboostdetail.RemoveAll((Predicate<BoostInfo.Data>) (x => (double) x.enhance == 0.0));
      otherboostdetail.ForEach((System.Action<BoostInfo.Data>) (x =>
      {
        BoostInfo.Data data = myboostdetail.Find((Predicate<BoostInfo.Data>) (y => y.name == x.name));
        if (data == null || string.IsNullOrEmpty(data.name))
          myboostdetail.Add(new BoostInfo.Data(x)
          {
            enhance = 0.0f,
            isHigher = false,
            showArrow = true
          });
        else
          data.showArrow = false;
      }));
      myboostdetail.ForEach((System.Action<BoostInfo.Data>) (x =>
      {
        BoostInfo.Data data = otherboostdetail.Find((Predicate<BoostInfo.Data>) (y => y.name == x.name));
        if (data == null || string.IsNullOrEmpty(data.name))
        {
          otherboostdetail.Add(new BoostInfo.Data(x)
          {
            enhance = 0.0f
          });
          x.isHigher = true;
          x.showArrow = true;
        }
        else if ((double) x.enhance == 0.0)
        {
          x.isHigher = false;
          x.showArrow = true;
        }
        else
          x.showArrow = false;
      }));
      List<BoostInfo.Data> dataList = new List<BoostInfo.Data>();
      using (List<BoostInfo.Data>.Enumerator enumerator = myboostdetail.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          BoostInfo.Data bd = enumerator.Current;
          BoostInfo.Data data = otherboostdetail.Find((Predicate<BoostInfo.Data>) (x => x.name == bd.name));
          if (data != null && !string.IsNullOrEmpty(data.name))
          {
            if ((double) bd.enhance > 0.0 && (double) data.enhance > 0.0 && (double) bd.enhance != (double) data.enhance)
            {
              bd.isHigher = (double) bd.enhance > (double) data.enhance;
              bd.showArrow = true;
            }
            else if ((double) bd.enhance < 0.0 && (double) data.enhance < 0.0 && (double) bd.enhance != (double) data.enhance)
            {
              bd.isHigher = (double) bd.enhance < (double) data.enhance;
              bd.showArrow = true;
            }
            dataList.Add(data);
          }
        }
      }
      otherboostdetail = dataList;
      if ((bool) ((UnityEngine.Object) this.headerTexture))
        BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, true, true, string.Empty);
      if ((bool) ((UnityEngine.Object) this.coordinate))
        this.coordinate.text = MapUtils.GetCoordinateString(this.wrc.k, this.wrc.x, this.wrc.y);
      if ((bool) ((UnityEngine.Object) this.time))
        this.time.text = Utils.FormatTimeForMail(this.mailtime);
      this.mywaroverview.SeedData(new WarOverview.Data()
      {
        name = Utils.XLAT(source1),
        icon = "Texture/Hero/Portrait_Icon/player_portrait_icon_" + this.mytd.portrait,
        setIcon = this.mytd.icon,
        lordTitleId = this.mytd.lord_title,
        isNpc = string.IsNullOrEmpty(this.mytd.portrait) && string.IsNullOrEmpty(this.mytd.icon),
        k = this.mytd.k,
        x = this.mytd.x,
        y = this.mytd.y,
        powerlost = allPowerLost1,
        dsht = this.mytd.dragon,
        trooplost = allTroopNotGood1,
        mylegend = this.mytd.legends
      });
      WarOverview.Data d1 = new WarOverview.Data();
      d1.name = Utils.XLAT(source2);
      d1.icon = "Texture/Hero/Portrait_Icon/player_portrait_icon_" + this.othertd.portrait;
      d1.isNpc = string.IsNullOrEmpty(this.othertd.portrait) && string.IsNullOrEmpty(this.othertd.icon);
      d1.setIcon = this.othertd.icon;
      d1.lordTitleId = this.othertd.lord_title;
      d1.k = this.othertd.k;
      d1.x = this.othertd.x;
      d1.y = this.othertd.y;
      d1.powerlost = allPowerLost2;
      d1.dsht = this.othertd.dragon;
      d1.trooplost = allTroopNotGood2;
      d1.mylegend = this.othertd.legends;
      if (this.maildata.IsInAllianceWar() && this.othertd.world_id > 0)
        d1.name = string.Format("{0}.k{1}", (object) d1.name, (object) this.othertd.world_id);
      if (this._isMerlinTrials)
      {
        int result = 0;
        int.TryParse(this.othertd.uid, out result);
        MerlinTrialsMainInfo merlinTrialsMainInfo = ConfigManager.inst.DB_MerlinTrialsMain.Get(result);
        if (merlinTrialsMainInfo != null)
          d1.icon = merlinTrialsMainInfo.IconPath;
        d1.isMerlinTrials = true;
      }
      else if (this._isMerlinTowerFightMonster)
      {
        int result = 0;
        int.TryParse(this.othertd.uid, out result);
        MagicTowerMainInfo data = ConfigManager.inst.DB_MagicTowerMain.GetData(result);
        if (data != null)
          d1.icon = data.IconPath;
        d1.isMerlinTower = true;
      }
      this.otherwaroverview.SeedData(d1);
      TroopOverview.Data d2 = new TroopOverview.Data();
      d2.kill = allTroopKill1;
      d2.dragonAoeKill = (long) this.mytd.dragon_skill_aoe_killed;
      d2.dead = allTroopLost1;
      d2.wounded = allTroopWound1;
      d2.survived = allTroopSurvive1;
      this.mytroopoverview.SeedData(d2);
      d2.kill = allTroopKill2;
      d2.dragonAoeKill = (long) this.othertd.dragon_skill_aoe_killed;
      d2.dead = allTroopLost2;
      d2.wounded = allTroopWound2;
      d2.survived = allTroopSurvive2;
      this.othertroopoverview.SeedData(d2);
      float num1 = 0.0f;
      float y1 = NGUIMath.CalculateRelativeWidgetBounds(this.myboostinfo.transform).size.y;
      float num2 = this.myboostinfo.transform.localPosition.y + y1 / 2f;
      using (List<BoostInfo.Data>.Enumerator enumerator = myboostdetail.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          BoostInfo.Data current = enumerator.Current;
          GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.myboostinfo.gameObject, this.myboostinfo.transform.parent);
          this.pool.Add(gameObject);
          gameObject.GetComponent<BoostInfo>().SeedData(current);
          float num3 = NGUIMath.CalculateRelativeWidgetBounds(gameObject.transform).size.y + 15f;
          num1 += num3;
          float num4 = num2 - num3 / 2f;
          Vector3 localPosition = this.myboostinfo.transform.localPosition;
          localPosition.y = num4;
          gameObject.transform.localPosition = localPosition;
          num2 = num4 - num3 / 2f;
        }
      }
      if (myboostdetail.Count != 0)
      {
        this.myboostbg.gameObject.SetActive(true);
        this.myboostbg.height = (int) num1 + (int) y1 / 2;
      }
      else
        this.myboostbg.gameObject.SetActive(false);
      float num5 = 0.0f;
      float num6 = this.otherboostinfo.transform.localPosition.y + y1 / 2f;
      using (List<BoostInfo.Data>.Enumerator enumerator = otherboostdetail.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          BoostInfo.Data current = enumerator.Current;
          GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.otherboostinfo.gameObject, this.otherboostinfo.transform.parent);
          this.pool.Add(gameObject);
          gameObject.GetComponent<BoostInfo>().SeedData(current);
          float num3 = NGUIMath.CalculateRelativeWidgetBounds(gameObject.transform).size.y + 15f;
          num5 += num3;
          float num4 = num6 - num3 / 2f;
          Vector3 localPosition = this.otherboostinfo.transform.localPosition;
          localPosition.y = num4;
          gameObject.transform.localPosition = localPosition;
          num6 = num4 - num3 / 2f;
        }
      }
      if (otherboostdetail.Count != 0)
      {
        this.otherboostbg.gameObject.SetActive(true);
        this.otherboostbg.height = (int) num5 + (int) y1 / 2;
      }
      else
        this.otherboostbg.gameObject.SetActive(false);
      if (this.maildata.IsInAbyss())
      {
        this.rootAbyss.SetActive(true);
        this.contentTransform.SetParent(this.contentRootForAbyss);
      }
      else
      {
        if (this.maildata.type == MailType.MAIL_TYPE_ANNI_BATTLE_REPORT)
        {
          this.casultObj.SetActive(true);
          this.casultyDes.text = Utils.XLAT("mail_1_year_tournament_casualty_description");
          Vector3 localPosition = this.contentRootForNotAbyss.localPosition;
          this.contentRootForNotAbyss.localPosition = new Vector3(localPosition.x, localPosition.y - 100f, localPosition.z);
        }
        else if (this.maildata.IsInMerlinTowerDefense() || this.maildata.IsInMerlinTowerAttack())
        {
          Dictionary<string, string> para = new Dictionary<string, string>();
          if (!this._isMerlinTowerFightMonster)
          {
            para.Add("0", "100");
          }
          else
          {
            int result = 0;
            int.TryParse(this.othertd.uid, out result);
            MagicTowerMainInfo data = ConfigManager.inst.DB_MagicTowerMain.GetData(result);
            if (data != null)
              para.Add("0", ((float) ((1.0 - (double) data.rescueRatio) * 100.0)).ToString());
          }
          this.labelMerlinTowerDefense.text = ScriptLocalization.GetWithPara("mail_pve_boss_troops_healed_description", para, true);
          NGUITools.SetActive(this.rootMerlinTowerDefense, true);
          Vector3 localPosition = this.contentRootForNotAbyss.localPosition;
          this.contentRootForNotAbyss.localPosition = new Vector3(localPosition.x, localPosition.y - 200f, localPosition.z);
        }
        else
        {
          NGUITools.SetActive(this.rootMerlinTowerDefense, false);
          this.casultObj.SetActive(false);
        }
        this.rootAbyss.SetActive(false);
        this.contentTransform.SetParent(this.contentRootForNotAbyss);
      }
      if (this.maildata.IsInMerlinTowerDefense())
      {
        this.merlinTowerWarReportItem.SetData(this.maildata.data);
        NGUITools.SetActive(this.rootMerlinTowerDefenseReward, true);
        NGUITools.SetActive(this.rootResouces, false);
      }
      else
      {
        NGUITools.SetActive(this.rootMerlinTowerDefenseReward, false);
        NGUITools.SetActive(this.rootResouces, true);
      }
      this.contentTransform.localPosition = Vector3.zero;
      this.contentTransform.localScale = Vector3.one;
      this.ArrangeLayout();
      this.HideTemplate(true);
      this.sv.ResetPosition();
    }
  }

  public void SetLegends(List<TroopDetail> troops, List<Hashtable> _legends)
  {
    for (int index = 0; index < troops.Count; ++index)
    {
      if (_legends != null)
      {
        if (troops[index].legends != null)
          troops[index].legends.Clear();
        else
          troops[index].legends = new List<Hashtable>();
        using (List<Hashtable>.Enumerator enumerator = _legends.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Hashtable current = enumerator.Current;
            if (current[(object) "uid"].ToString() == troops[index].uid.ToString())
              troops[index].legends.Add(current);
          }
        }
      }
    }
  }

  public void OnLoadTroopDetail()
  {
    WarReportDetail.Parameter parameter = new WarReportDetail.Parameter();
    if (this.wrc.IsAttacker())
    {
      parameter.pattackertroopdetail = this.allmytroopdetail;
      parameter.pdefendertroopdetail = this.allothertroopdetail;
      parameter.pattackerdragon = this.allmydragondetail;
      parameter.pdefenderdragon = this.allotherdragondetail;
      parameter.pattackerlegend = this.allmylegenddetail;
      parameter.pdefenderlegend = this.allotherlegenddetail;
    }
    else
    {
      parameter.pattackertroopdetail = this.allothertroopdetail;
      parameter.pdefendertroopdetail = this.allmytroopdetail;
      parameter.pattackerdragon = this.allotherdragondetail;
      parameter.pdefenderdragon = this.allmydragondetail;
      parameter.pattackerlegend = this.allotherlegenddetail;
      parameter.pdefenderlegend = this.allmylegenddetail;
    }
    parameter.showLegend = this._showLegend;
    UIManager.inst.OpenPopup("Report/WarReportDetail", (Popup.PopupParameter) parameter);
  }

  private void ArrangeLayout()
  {
    float y1 = this.boostdetailGroup.transform.localPosition.y;
    float y2 = NGUIMath.CalculateRelativeWidgetBounds(this.boostdetailGroup.transform).size.y;
    Vector3 localPosition = this.btnGroup.transform.localPosition;
    localPosition.y = y1 - y2;
    this.btnGroup.transform.localPosition = localPosition;
    this.sv.UpdatePosition();
  }

  private void Reset()
  {
    this.sv.ResetPosition();
    Rigidbody component = this.sv.transform.GetComponent<Rigidbody>();
    if ((UnityEngine.Object) null != (UnityEngine.Object) component)
    {
      component.isKinematic = true;
      component.useGravity = false;
    }
    this.HideTemplate(false);
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        PrefabManagerEx.Instance.Destroy(current);
      }
    }
    this.pool.Clear();
  }

  private void HideTemplate(bool hide)
  {
    this.myboostinfo.gameObject.SetActive(!hide);
    this.otherboostinfo.gameObject.SetActive(!hide);
  }

  public void GotoMycity()
  {
    this.GoToTargetPlace(int.Parse(this.mytd.k), int.Parse(this.mytd.x), int.Parse(this.mytd.y));
  }

  public void GotoEnemycity()
  {
    this.GoToTargetPlace(int.Parse(this.othertd.k), int.Parse(this.othertd.x), int.Parse(this.othertd.y));
  }

  public override void OnShareBtnClicked()
  {
    base.OnShareBtnClicked();
    if (!this.canShare)
      return;
    if (this.maildata.type == MailType.MAIL_TYPE_RALLY_BATTLE_REPORT)
    {
      if (this.wrc.IsAttacker())
        ChatMessageManager.SendWarReport_Rally(this.maildata.mailID, PlayerData.inst.uid, (this.wrc.defender.acronym == null ? (string) null : "(" + this.wrc.defender.acronym + ")") + this.wrc.defender.name, this.wrc.IsWinner());
      else
        ChatMessageManager.SendWarReport_BeRally(this.maildata.mailID, PlayerData.inst.uid, (this.wrc.attacker.acronym == null ? (string) null : "(" + this.wrc.attacker.acronym + ")") + this.wrc.attacker.name, this.wrc.IsWinner());
    }
    else
    {
      if (this.maildata.type != MailType.MAIL_TYPE_PVP_BATTLE_REPORT && this.maildata.type != MailType.MAIL_TYPE_ANNI_BATTLE_REPORT)
        return;
      if (this.wrc.IsAttacker())
        ChatMessageManager.SendWarReport_Attack(this.maildata.mailID, PlayerData.inst.uid, (this.wrc.defender.acronym == null ? (string) null : "(" + this.wrc.defender.acronym + ")") + this.wrc.defender.name, this.wrc.IsWinner());
      else
        ChatMessageManager.SendWarReport_BeAttack(this.maildata.mailID, PlayerData.inst.uid, (this.wrc.attacker.acronym == null ? (string) null : "(" + this.wrc.attacker.acronym + ")") + this.wrc.attacker.name, this.wrc.IsWinner());
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.wrc = JsonReader.Deserialize<WarReport>(Utils.Object2Json((object) this.param.hashtable)).data;
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }
}
