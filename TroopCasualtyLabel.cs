﻿// Decompiled with JetBrains decompiler
// Type: TroopCasualtyLabel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class TroopCasualtyLabel : MonoBehaviour
{
  public UILabel attackersOne;
  public UILabel attackersTwo;
  public UILabel defendersOne;
  public UILabel defendersTwo;
  public UILabel casualtiesOne;
  public UILabel casualtiesTwo;
  public GameObject yourContainer;
  public GameObject theirContainer;

  public void SetData(int attackCount, TroopInfo attacker, int defendCount, TroopInfo defender, int casualties, bool yourAttack)
  {
    UILabel attackersOne = this.attackersOne;
    string str1 = ScriptLocalization.Get(attacker.m_Name, true);
    this.attackersTwo.text = str1;
    string str2 = str1;
    attackersOne.text = str2;
    UILabel defendersOne = this.defendersOne;
    string str3 = ScriptLocalization.Get(defender.m_Name, true);
    this.defendersTwo.text = str3;
    string str4 = str3;
    defendersOne.text = str4;
    UILabel casualtiesOne = this.casualtiesOne;
    string str5 = casualties.ToString();
    this.casualtiesTwo.text = str5;
    string str6 = str5;
    casualtiesOne.text = str6;
    this.yourContainer.SetActive(yourAttack);
    this.theirContainer.SetActive(!yourAttack);
  }
}
