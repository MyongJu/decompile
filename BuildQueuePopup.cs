﻿// Decompiled with JetBrains decompiler
// Type: BuildQueuePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildQueuePopup : Popup
{
  private List<string> items = new List<string>()
  {
    "shopitem_build_queue_middle"
  };
  private int buyCount = 1;
  public List<BuildQueueShopItem> shopItems;
  public GameObject normal;
  public GameObject buildLimit;
  public UILabel needTime;
  public UILabel remainTime;
  public UISprite[] checks;
  public UILabel effectTimeLabel;
  public UILabel goldCostLabel;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.UpdateUI(orgParam as BuildQueuePopup.Parameter);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide((UIControler.UIParameter) null);
  }

  public void UpdateUI(BuildQueuePopup.Parameter param)
  {
    if (param != null && param.showLimit)
    {
      this.buildLimit.SetActive(true);
      this.normal.SetActive(false);
      this.needTime.text = param.needTimeString;
      this.remainTime.text = param.remainTimeString;
    }
    else
    {
      this.buildLimit.SetActive(false);
      this.normal.SetActive(true);
    }
    this.DrawBtn();
  }

  private void DrawBtn()
  {
    this.effectTimeLabel.text = this.FormatTimeDDHH((int) ConfigManager.inst.DB_Items.GetItem(ConfigManager.inst.DB_Shop.GetShopData(this.items[0]).Item_InternalId).Value * this.buyCount);
    int num = ItemBag.Instance.GetShopItemPrice(this.items[0]) * this.buyCount;
    this.goldCostLabel.text = num.ToString();
    if (num > PlayerData.inst.hostPlayer.Currency)
      this.goldCostLabel.color = Color.red;
    else
      this.goldCostLabel.color = Color.yellow;
  }

  public void OnBuyQueueBtnClick()
  {
    if (ItemBag.Instance.GetShopItemPrice(this.items[0]) * this.buyCount > PlayerData.inst.hostPlayer.Currency)
    {
      this.OnCloseBtnClicked();
      Utils.ShowNotEnoughGoldTip();
    }
    else
    {
      ItemBag.Instance.BuyAndUseShopItem(this.items[0], (Hashtable) null, (System.Action<bool, object>) null, this.buyCount);
      this.OnCloseBtnClicked();
    }
  }

  public void OnToggleChange()
  {
  }

  private int SelectedIndex
  {
    get
    {
      int num = 0;
      for (int index = 0; index < this.checks.Length; ++index)
      {
        if ((double) this.checks[index].alpha > 0.0)
        {
          num = index;
          break;
        }
      }
      return num;
    }
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private string FormatTimeDDHH(int time)
  {
    return Utils.XLAT("id_2_days") + " /";
  }

  public class Parameter : Popup.PopupParameter
  {
    public bool showLimit;
    public string needTimeString;
    public string remainTimeString;
    public int buyCount;
  }
}
