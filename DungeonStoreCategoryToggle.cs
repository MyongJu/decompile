﻿// Decompiled with JetBrains decompiler
// Type: DungeonStoreCategoryToggle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DungeonStoreCategoryToggle : MonoBehaviour
{
  public UIToggle _toggle;
  public UILabel _titleBase;
  public UILabel _titleSelected;
  private System.Action<DungeonStoreCategoryToggle> _callback;
  private ShopCommonCategory _category;

  public void SetData(ShopCommonCategory category, System.Action<DungeonStoreCategoryToggle> callback)
  {
    this._callback = callback;
    this._category = category;
    if (this._category == null)
    {
      this._titleBase.text = Utils.XLAT("item_uppercase_all_title");
      this._titleSelected.text = Utils.XLAT("item_uppercase_all_title");
    }
    else
    {
      this._titleBase.text = category.LocName;
      this._titleSelected.text = category.LocName;
    }
  }

  public ShopCommonCategory ShopCategory
  {
    get
    {
      return this._category;
    }
  }

  public void OnToggleChanged()
  {
    if (!this.Selected || this._callback == null)
      return;
    this._callback(this);
  }

  public bool Selected
  {
    get
    {
      return this._toggle.value;
    }
    set
    {
      this._toggle.Set(value);
    }
  }
}
