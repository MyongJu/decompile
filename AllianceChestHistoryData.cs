﻿// Decompiled with JetBrains decompiler
// Type: AllianceChestHistoryData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class AllianceChestHistoryData
{
  public string senderName = string.Empty;
  public int configId;
  public long senderUid;
  public int gold;

  public void Decode(Hashtable hashtable)
  {
    DatabaseTools.UpdateData(hashtable, "config_id", ref this.configId);
    DatabaseTools.UpdateData(hashtable, "sender_uid", ref this.senderUid);
    DatabaseTools.UpdateData(hashtable, "sender_uname", ref this.senderName);
    DatabaseTools.UpdateData(hashtable, "gold", ref this.gold);
  }
}
