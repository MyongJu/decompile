﻿// Decompiled with JetBrains decompiler
// Type: AnimationVFX
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AnimationVFX : VfxBase
{
  private UnityEngine.Animation[] _animations;

  public override void Play(Transform target)
  {
    base.Play(target);
    this.InitOnce();
    for (int index = 0; index < this._animations.Length; ++index)
      this._animations[index].Play(PlayMode.StopAll);
  }

  public override void Stop()
  {
    base.Stop();
    this.InitOnce();
    for (int index = 0; index < this._animations.Length; ++index)
      this._animations[index].Stop();
  }

  private void InitOnce()
  {
    if (this._animations != null)
      return;
    this._animations = this.GetComponentsInChildren<UnityEngine.Animation>(true);
  }
}
