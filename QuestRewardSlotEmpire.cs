﻿// Decompiled with JetBrains decompiler
// Type: QuestRewardSlotEmpire
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class QuestRewardSlotEmpire : MonoBehaviour
{
  private QuestReward _reward;
  [SerializeField]
  private QuestRewardSlotEmpire.Panel panel;

  public void Setup(QuestReward reward)
  {
    this._reward = reward;
    this.panel.icon.spriteName = reward.GetRewardIconName();
    this.panel.amount.text = Utils.FormatShortThousands(reward.GetValue());
    this.panel.rewardName.text = reward.GetRewardTypeName();
    if (this.IsTexture)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.itemIcon, reward.GetRewardIconName(), (System.Action<bool>) null, true, false, string.Empty);
      NGUITools.SetActive(this.panel.icon.gameObject, false);
      NGUITools.SetActive(this.panel.itemIcon.gameObject, true);
    }
    else
    {
      NGUITools.SetActive(this.panel.icon.gameObject, true);
      NGUITools.SetActive(this.panel.itemIcon.gameObject, false);
    }
  }

  private bool IsTexture
  {
    get
    {
      if (!(this._reward is ConsumableReward) && !(this._reward is ExperienceReward) && (!(this._reward is AllianceFundReward) && !(this._reward is AllianceXPReward)))
        return this._reward is AllianceHonourReward;
      return true;
    }
  }

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.panel.itemIcon);
  }

  [Serializable]
  protected class Panel
  {
    public UISprite icon;
    public UILabel type;
    public UILabel amount;
    public UISprite big_icon;
    public UITexture itemIcon;
    public UILabel rewardName;
  }
}
