﻿// Decompiled with JetBrains decompiler
// Type: CommunityDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class CommunityDialog : UI.Dialog
{
  public const string Dialog_Type = "Community/CommunityDialog";
  public GameObject t;
  public UIGrid grid;
  public UIScrollView scrollView;
  private List<Community> datas;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    using (List<Community>.Enumerator enumerator = this.Datas.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Community c = enumerator.Current;
        GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.t);
        gameObject.SetActive(true);
        CommunityTemplate component = gameObject.GetComponent<CommunityTemplate>();
        component.icon.FeedData((IComponentData) new IconData()
        {
          contents = new string[2]
          {
            Utils.XLAT(c.desctiptionKey),
            Utils.XLAT(c.buttonKey)
          },
          image = c.iconPath
        });
        component.button.onClick.Add(new EventDelegate((EventDelegate.Callback) (() => Application.OpenURL(c.linkURL))));
      }
    }
    this.grid.Reposition();
    this.grid.onReposition = (UIGrid.OnReposition) (() => this.scrollView.ResetPosition());
  }

  private List<Community> Datas
  {
    get
    {
      if (this.datas == null)
      {
        this.datas = new List<Community>();
        bool flag1 = false;
        if (PlayerData.inst.userData.language == "ru" || PlayerData.inst.userData.language == "ru_RU")
        {
          flag1 = true;
          this.datas.Add(new Community()
          {
            iconPath = "Texture/Community/icon_vk",
            desctiptionKey = "settings_community_vk_desc",
            buttonKey = "settings_community_vk_button",
            linkURL = "https://vk.com/kingofavalonofficial"
          });
        }
        bool flag2 = PlayerData.inst.userData.language == "zh-CN";
        if (!flag2)
        {
          this.datas.Add(new Community()
          {
            iconPath = "Texture/Community/icon_facebook",
            desctiptionKey = "settings_community_facebook_desc",
            buttonKey = "settings_community_facebook_button",
            linkURL = "https://www.facebook.com/koadw"
          });
          this.datas.Add(new Community()
          {
            iconPath = "Texture/Community/icon_twitter",
            desctiptionKey = "settings_community_twitter_desc",
            buttonKey = "settings_community_twitter_button",
            linkURL = "https://twitter.com/KingofAvalonKOA"
          });
          this.datas.Add(new Community()
          {
            iconPath = "Texture/Community/icon_youtube",
            desctiptionKey = "settings_community_youtube_desc",
            buttonKey = "settings_community_youtube_button",
            linkURL = "https://www.youtube.com/channel/UCgBSQx5JYfekrVOC9a1RoIw"
          });
        }
        else
          this.datas.Add(new Community()
          {
            iconPath = "Texture/Community/icon_weibo",
            desctiptionKey = "settings_community_weibo_desc",
            buttonKey = "settings_community_weibo_button",
            linkURL = "http://weibo.com/kingofavalon"
          });
        this.datas.Add(new Community()
        {
          iconPath = "Texture/Community/icon_forum",
          desctiptionKey = "settings_community_forum_desc",
          buttonKey = "settings_community_forum_button",
          linkURL = "https://forums.funplusgame.com/forums/english-king-of-avalon"
        });
        if (!flag1 && !flag2)
          this.datas.Add(new Community()
          {
            iconPath = "Texture/Community/icon_vk",
            desctiptionKey = "settings_community_vk_desc",
            buttonKey = "settings_community_vk_button",
            linkURL = "https://vk.com/kingofavalonofficial"
          });
      }
      return this.datas;
    }
  }
}
