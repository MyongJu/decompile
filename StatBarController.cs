﻿// Decompiled with JetBrains decompiler
// Type: StatBarController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StatBarController : MonoBehaviour
{
  public UISprite[] mBars;

  public void SetPercent(float percent)
  {
    this.SetCount((int) ((double) this.mBars.Length * (double) percent));
  }

  public void SetCount(int count)
  {
    --count;
    for (int index = 0; index < this.mBars.Length; ++index)
      this.mBars[index].alpha = index > count ? 0.25f : 1f;
  }
}
