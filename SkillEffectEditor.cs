﻿// Decompiled with JetBrains decompiler
// Type: SkillEffectEditor
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SkillEffectEditor : MonoBehaviour, IVfxTarget
{
  public string dragonAssetPath = "Prefab/Dragon/dragon/demo_dragon";
  public string animtionName = "attack_common";
  public int souurceEventId = 1;
  public int targetEventId = 2;
  public float effectTime = -1f;
  public string sourceVfxPath;
  public string targetVfxPath;
  private DragonView _source;
  private SkillEffectPlayer _player;

  private void OnEnable()
  {
  }

  private void OnDisable()
  {
  }

  public void CreateEnviorment()
  {
    DragonViewData dragonViewData = new DragonViewData();
    dragonViewData.mainAsset = this.dragonAssetPath;
    dragonViewData.color = Color.white;
    this._source = new DragonView();
    this._source.Mount(this.transform);
    this._source.Load((SpriteViewData) dragonViewData, true);
  }

  public void PlaySkillEffect()
  {
    SkillEffectData data = new SkillEffectData();
    data.animation = this.animtionName;
    data.sourceVfx = this.sourceVfxPath;
    data.sourceEventId = this.souurceEventId;
    data.targetVfx = this.targetVfxPath;
    data.targetEventId = this.targetEventId;
    data.time = this.effectTime;
    data.faceTarget = true;
    this._player = new SkillEffectPlayer();
    this._player.SetData(data);
    this._player.Play((SpriteView) this._source, (IVfxTarget) this);
    this._player.OnEvent += new SkillEffectEventHandler(this.OnSkillEffectEvent);
  }

  public void PlaySkillEffectWithVector3()
  {
    SkillEffectData data = new SkillEffectData();
    data.animation = this.animtionName;
    data.sourceVfx = this.sourceVfxPath;
    data.sourceEventId = this.souurceEventId;
    data.targetVfx = this.targetVfxPath;
    data.targetEventId = this.targetEventId;
    data.time = this.effectTime;
    data.faceTarget = true;
    this._player = new SkillEffectPlayer();
    this._player.SetData(data);
    this._player.Play((SpriteView) this._source, this.transform.position);
    this._player.OnEvent += new SkillEffectEventHandler(this.OnSkillEffectEvent);
  }

  public void StopSkillEffect()
  {
    this._player.OnEvent -= new SkillEffectEventHandler(this.OnSkillEffectEvent);
    this._player.Stop(true);
  }

  private void OnSkillEffectEvent(SkillEffectEvent.Type type, object param)
  {
    if (type != SkillEffectEvent.Type.OnEnd)
      return;
    Debug.Log((object) "=====Skill Effect End=======");
  }

  public Transform GetDummyPoint(string key)
  {
    return this.transform;
  }

  Transform IVfxTarget.get_transform()
  {
    return this.transform;
  }
}
