﻿// Decompiled with JetBrains decompiler
// Type: LegendAddExpAndDevourDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;

public class LegendAddExpAndDevourDialog : UI.Dialog
{
  public PageTabsMonitor m_Tabs;
  public LegendExpDialog m_ExpPage;
  public LegendDevourDialog m_DevourPage;
  private LegendAddExpAndDevourDialog.Parameter m_Parameter;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as LegendAddExpAndDevourDialog.Parameter;
    this.m_Tabs.onTabSelected += new System.Action<int>(this.OnTabSelected);
    this.m_Tabs.SetCurrentTab(0, true);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.m_Tabs.onTabSelected -= new System.Action<int>(this.OnTabSelected);
    this.m_ExpPage.Hide();
  }

  private void OnTabSelected(int index)
  {
    if (index == 0)
    {
      this.m_ExpPage.Show(this.m_Parameter.legendData);
      this.m_DevourPage.Hide();
    }
    else
    {
      if (index != 1)
        return;
      this.m_DevourPage.Show(this.m_Parameter.legendData);
      this.m_ExpPage.Hide();
    }
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public LegendData legendData;
  }
}
