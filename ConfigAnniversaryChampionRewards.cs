﻿// Decompiled with JetBrains decompiler
// Type: ConfigAnniversaryChampionRewards
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAnniversaryChampionRewards
{
  private ConfigParse parse = new ConfigParse();
  public Dictionary<string, AnniversaryChampionRewardInfo> datas;
  private Dictionary<int, AnniversaryChampionRewardInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<AnniversaryChampionRewardInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, AnniversaryChampionRewardInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, AnniversaryChampionRewardInfo>) null;
  }

  public bool Contains(int internalId)
  {
    return this.dicByUniqueId.ContainsKey(internalId);
  }

  public bool Contains(string id)
  {
    return this.datas.ContainsKey(id);
  }

  public AnniversaryChampionRewardInfo GetItem(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (AnniversaryChampionRewardInfo) null;
  }

  public AnniversaryChampionRewardInfo GetItem(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AnniversaryChampionRewardInfo) null;
  }
}
