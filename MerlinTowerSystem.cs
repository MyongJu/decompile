﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerSystem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class MerlinTowerSystem : MonoBehaviour
{
  [SerializeField]
  private MerlinFloorHud _merlinFloorHud;
  [SerializeField]
  private MerlinKingdomHud _merlinKingdomHud;

  public void Setup()
  {
    this._merlinFloorHud.gameObject.SetActive(false);
    this._merlinKingdomHud.gameObject.SetActive(false);
    if (MerlinTowerPayload.Instance.UserData.IsInTower)
      this.EnterTower();
    else
      this.EnterKingdom();
  }

  public void Shutdown()
  {
    this._merlinFloorHud.Hide((UIControler.UIParameter) null);
    this._merlinKingdomHud.Hide((UIControler.UIParameter) null);
  }

  public void EnterTower()
  {
    this._merlinFloorHud.Show((UIControler.UIParameter) null);
    this._merlinKingdomHud.Hide((UIControler.UIParameter) null);
    UIManager.inst.publicHUD.SwitchToPart(GAME_LOCATION.MERLIN_TOWER_FLOOR);
  }

  public void EnterKingdom()
  {
    this._merlinFloorHud.Hide((UIControler.UIParameter) null);
    this._merlinKingdomHud.Show((UIControler.UIParameter) null);
    UIManager.inst.publicHUD.SwitchToPart(GAME_LOCATION.MERLIN_TOWER_KINGDOM);
  }
}
