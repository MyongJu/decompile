﻿// Decompiled with JetBrains decompiler
// Type: ToastManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UI;
using UnityEngine;

public class ToastManager : MonoBehaviour
{
  public const string BASIC_TOAST = "BASIC_TOAST";
  public const string LVLUP_TOAST = "LVLUP_TOAST";
  public const string WARN_TOAST = "WARN_TOAST";
  private static ToastManager _instance;
  private LinkedList<ToastManager.ToastBlock> _list;
  private LinkedList<ToastManager.ToastBlock> _oneFrameSortQueue;
  private Dictionary<string, string> _template;
  private ToastDialog currentToast;

  private void Awake()
  {
    if ((UnityEngine.Object) ToastManager._instance != (UnityEngine.Object) null && (UnityEngine.Object) ToastManager._instance != (UnityEngine.Object) this)
    {
      Debug.LogError((object) "There should be only one QueueDialogManager instance.");
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    }
    else
    {
      ToastManager._instance = this;
      this._list = new LinkedList<ToastManager.ToastBlock>();
      this._template = new Dictionary<string, string>();
      this._oneFrameSortQueue = new LinkedList<ToastManager.ToastBlock>();
      this.RegisterDialogTemplate("BASIC_TOAST", "Prefab/ToastPrefab/StandardToast");
      this.RegisterDialogTemplate("LVLUP_TOAST", "Prefab/ToastPrefab/LevelUpToast");
      this.RegisterDialogTemplate("WARN_TOAST", "Prefab/ToastPrefab/WarningToast");
    }
  }

  public static ToastManager Instance
  {
    get
    {
      if ((UnityEngine.Object) ToastManager._instance == (UnityEngine.Object) null)
        D.error((object) "QueueDialogManager haven't been created yet.");
      return ToastManager._instance;
    }
  }

  public void RegisterDialogTemplate(string type, string preferbPath)
  {
    this._template.Add(type, preferbPath);
  }

  private void SortAndAddQueuedToasts()
  {
    foreach (ToastManager.ToastBlock toastBlock in (IEnumerable<ToastManager.ToastBlock>) this._oneFrameSortQueue.OrderByDescending<ToastManager.ToastBlock, int>((Func<ToastManager.ToastBlock, int>) (t => t.toastPriority)))
    {
      this._list.AddLast(toastBlock);
      if (this._list.Count == 2 && (UnityEngine.Object) this.currentToast != (UnityEngine.Object) null)
        this.currentToast.Interrupted();
    }
    this._oneFrameSortQueue.Clear();
  }

  private void LateUpdate()
  {
    if (this._oneFrameSortQueue.Count == 1)
    {
      using (LinkedList<ToastManager.ToastBlock>.Enumerator enumerator = this._oneFrameSortQueue.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this._list.AddLast(enumerator.Current);
      }
      this._oneFrameSortQueue.Clear();
    }
    else
    {
      if (this._oneFrameSortQueue.Count <= 1)
        return;
      this.SortAndAddQueuedToasts();
    }
  }

  private ToastDialog GetDialogTemplate(string type)
  {
    string fullname = (string) null;
    ToastDialog toastDialog = (ToastDialog) null;
    this._template.TryGetValue(type, out fullname);
    if (fullname != null)
    {
      toastDialog = (UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad(fullname, (System.Type) null)) as GameObject).GetComponent<ToastDialog>();
      if ((UnityEngine.Object) null == (UnityEngine.Object) toastDialog)
      {
        Debug.LogWarning((object) "null inst!!!");
        return (ToastDialog) null;
      }
      toastDialog.transform.parent = UIManager.inst.ui2DOverlayCamera.transform;
      toastDialog.transform.localScale = Vector3.one;
    }
    return toastDialog;
  }

  public ToastManager.ToastHandle AddToast(string type, object args)
  {
    ToastManager.ToastBlock t = new ToastManager.ToastBlock(type, args);
    this._oneFrameSortQueue.AddLast(t);
    return new ToastManager.ToastHandle(t);
  }

  public ToastManager.ToastHandle AddBasicToast(string imagePath, string title, string content)
  {
    return this.AddToast("BASIC_TOAST", (object) new ToastArgs(new ToastData()
    {
      icon = imagePath,
      title = title,
      content = content
    }, (Dictionary<string, string>) null));
  }

  public ToastManager.ToastHandle AddBasicToast(string identifier, params string[] args)
  {
    if (args != null && (args.Length & 1) == 1)
    {
      Debug.LogError((object) "Argument list must be even");
      return (ToastManager.ToastHandle) null;
    }
    ToastData data = ConfigManager.inst.DB_Toast.GetData(identifier);
    if (data == null)
    {
      Debug.LogError((object) ("Failed to get toast identifier '" + identifier + "'"));
      return (ToastManager.ToastHandle) null;
    }
    Dictionary<string, string> para = new Dictionary<string, string>();
    if (args != null)
    {
      int index = 0;
      while (index < args.Length)
      {
        para.Add(args[index], args[index + 1]);
        index += 2;
      }
    }
    return this.AddToast("BASIC_TOAST", (object) new ToastArgs(data, para));
  }

  public ToastManager.ToastHandle AddBasicToast(string identifier)
  {
    return this.AddBasicToast(identifier, (string[]) null);
  }

  private void UnloadCurToast()
  {
    this.currentToast.gameObject.SetActive(false);
    this.currentToast.transform.parent = (Transform) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.currentToast.gameObject);
    this.currentToast = (ToastDialog) null;
  }

  private void Update()
  {
    if (this._list.Count == 0)
      return;
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.currentToast)
    {
      this.currentToast = this.GetDialogTemplate(this._list.First.Value.toastType);
      if (!((UnityEngine.Object) null != (UnityEngine.Object) this.currentToast))
        return;
      this.currentToast.Init();
      this.currentToast.SeedData(this._list.First.Value.args);
    }
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.currentToast))
      return;
    if (!this.currentToast.isplaying && !this.currentToast.played)
    {
      if (this._list.Count > 1)
        this.currentToast.FastPlay();
      else
        this.currentToast.Play();
    }
    if (this.currentToast.isplaying || this.currentToast.isplaying || !this.currentToast.played)
      return;
    if (this._list.Count > 1 && this._list.First.Next.Value.toastType == this._list.First.Value.toastType)
    {
      this.currentToast.Init();
      this.currentToast.SeedData(this._list.First.Next.Value.args);
    }
    else
      this.UnloadCurToast();
    this._list.RemoveFirst();
  }

  public class ToastBlock
  {
    public string toastType;
    public object args;
    private int _toastPriority;

    public ToastBlock(string type, object args)
    {
      this.toastType = type;
      this.args = args;
    }

    public int toastPriority
    {
      get
      {
        return this._toastPriority;
      }
      set
      {
        this._toastPriority = value;
      }
    }
  }

  public class ToastHandle
  {
    private ToastManager.ToastBlock myToast;

    public ToastHandle(ToastManager.ToastBlock t)
    {
      this.myToast = t;
    }

    public void SetPriority(int prio)
    {
      this.myToast.toastPriority = prio;
    }
  }

  public enum Mode
  {
    Serial,
    Parallel,
  }
}
