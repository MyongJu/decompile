﻿// Decompiled with JetBrains decompiler
// Type: ItemGemUse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using GemConstant;
using System.Collections;
using UI;

public class ItemGemUse : ItemBaseUse
{
  public ItemGemUse(int id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  public ItemGemUse(string id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  public override void UseItem(System.Action<bool, object> resultHandler = null)
  {
    ConfigEquipmentGemInfo dataByItemId = ConfigManager.inst.DB_EquipmentGem.GetDataByItemId(this.Info.internalId);
    if (dataByItemId == null)
      return;
    if (GemManager.Instance.IsGemBagFull(dataByItemId.slotType) && dataByItemId.gemType != 0)
    {
      switch (dataByItemId.slotType)
      {
        case SlotType.lord:
          UIManager.inst.toast.Show(Utils.XLAT("toast_forge_gemstone_armory_full"), (System.Action) null, 4f, false);
          break;
        case SlotType.dragon_knight:
          UIManager.inst.toast.Show(Utils.XLAT("toast_forge_gemstone_dk_armory_full"), (System.Action) null, 4f, false);
          break;
      }
    }
    else
    {
      switch (ItemBaseUse.Check(this.Info.internalId))
      {
        case ItemBaseUse.UseType.DirectlyUse:
          this.DirectlyUse(resultHandler);
          break;
        case ItemBaseUse.UseType.BatchUse:
          if (this.Count > 1)
          {
            ItemUseOrBuyPopup.Parameter parameter = new ItemUseOrBuyPopup.Parameter();
            parameter.item_id = this.Info.internalId;
            if (dataByItemId.gemType != 0)
              parameter.maxCount = GemManager.Instance.GetGemBagLeftCapacity(dataByItemId.slotType);
            UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) parameter);
            break;
          }
          ItemBag.Instance.UseItem(this.Info.internalId, 1, (Hashtable) null, resultHandler);
          break;
        default:
          this.CustomUse(resultHandler);
          break;
      }
    }
  }
}
