﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragonKnightSkill
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigDragonKnightSkill
{
  private List<DragonKnightSkillInfo> dragonKnightSkillInfoList = new List<DragonKnightSkillInfo>();
  private Dictionary<string, DragonKnightSkillInfo> datas;
  private Dictionary<int, DragonKnightSkillInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<DragonKnightSkillInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    Dictionary<string, DragonKnightSkillInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this.dragonKnightSkillInfoList.Add(enumerator.Current);
    }
    this.dragonKnightSkillInfoList.Sort((Comparison<DragonKnightSkillInfo>) ((a, b) => a.sort.CompareTo(b.sort)));
  }

  public List<DragonKnightSkillInfo> GetDragonKnightSkillInfoList()
  {
    return this.dragonKnightSkillInfoList;
  }

  public List<DragonKnightSkillInfo> GetDragonKnightSkillInfoListByStage(int skillStage)
  {
    if (this.dragonKnightSkillInfoList != null)
    {
      List<DragonKnightSkillInfo> all = this.dragonKnightSkillInfoList.FindAll((Predicate<DragonKnightSkillInfo>) (x => x.CurrentStage == skillStage));
      if (all != null)
      {
        all.Sort((Comparison<DragonKnightSkillInfo>) ((a, b) => a.sort.CompareTo(b.sort)));
        return all;
      }
    }
    return (List<DragonKnightSkillInfo>) null;
  }

  public Dictionary<int, List<DragonKnightSkillInfo>> GetDragonKnightAllStageInfo()
  {
    Dictionary<int, List<DragonKnightSkillInfo>> dictionary = new Dictionary<int, List<DragonKnightSkillInfo>>();
    if (this.dragonKnightSkillInfoList != null)
    {
      for (int index = 0; index < this.dragonKnightSkillInfoList.Count; ++index)
      {
        DragonKnightSkillInfo dragonKnightSkillInfo = this.dragonKnightSkillInfoList[index];
        if (!dictionary.ContainsKey(dragonKnightSkillInfo.CurrentStage))
          dictionary.Add(dragonKnightSkillInfo.CurrentStage, this.GetDragonKnightSkillInfoListByStage(dragonKnightSkillInfo.CurrentStage));
      }
    }
    return dictionary;
  }

  public bool IsStageSkillsLocked(int stage)
  {
    List<DragonKnightSkillInfo> skillInfoListByStage = this.GetDragonKnightSkillInfoListByStage(stage);
    if (skillInfoListByStage != null)
    {
      List<DragonKnightSkillInfo> all = skillInfoListByStage.FindAll((Predicate<DragonKnightSkillInfo>) (x => x.CurrentState == DragonKnightSkillInfo.SkillState.LOCKED));
      if (all != null && all.Count == skillInfoListByStage.Count)
        return true;
    }
    return false;
  }

  public bool IsStageSkillsMastered(int stage)
  {
    List<DragonKnightSkillInfo> skillInfoListByStage = this.GetDragonKnightSkillInfoListByStage(stage);
    if (skillInfoListByStage != null)
    {
      List<DragonKnightSkillInfo> all = skillInfoListByStage.FindAll((Predicate<DragonKnightSkillInfo>) (x => x.CurrentState == DragonKnightSkillInfo.SkillState.MASTERED));
      if (all != null && all.Count == skillInfoListByStage.Count)
        return true;
    }
    return false;
  }

  public DragonKnightSkillInfo Get(int interalId)
  {
    if (this.dicByUniqueId.ContainsKey(interalId))
      return this.dicByUniqueId[interalId];
    return (DragonKnightSkillInfo) null;
  }

  public DragonKnightSkillInfo Get(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (DragonKnightSkillInfo) null;
  }
}
