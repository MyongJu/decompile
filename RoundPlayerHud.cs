﻿// Decompiled with JetBrains decompiler
// Type: RoundPlayerHud
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class RoundPlayerHud : MonoBehaviour, IRoundPlayerHud
{
  public DragonKnightScrollBar Foreground;
  public DragonKnightScrollBar Background;
  public NumberController Decrease;
  public NumberController Increase;
  public GameObject Arrow;
  public RoundPlayerBuffBar BuffBar;
  public MeshRenderer LordIcon;
  public GameObject LordInfo;

  public void Initialize()
  {
    this.Foreground.OnTweenFinished = new System.Action(this.OnTweenFinished);
    this.BuffBar.HideAll();
    this.Foreground.Reset();
    this.SetArrow(false);
  }

  private void OnTweenFinished()
  {
    this.Background.SetProgress(this.Foreground.GetProgress());
  }

  public void SetHealthProgress(float progress)
  {
    this.Foreground.SetProgress(progress);
  }

  public void SetHealthNumber(int health, int healthMax)
  {
  }

  public void SetManaProgress(float progress)
  {
  }

  public void SetManaNumber(int mana, int manaMax)
  {
  }

  public void SetLordInfo(UserData userData)
  {
    this.LordInfo.SetActive(userData != null);
    if (userData == null)
      return;
    CustomIconLoader.Instance.requestCustomIcon(this.LordIcon.material, userData.PortraitIconPath, userData.Icon, false);
  }

  public void SetDecreaseHealth(int number, Color color)
  {
    this.Decrease.Play(number.ToString(), color);
  }

  public void SetIncreaseHealth(int number, Color color)
  {
    this.Increase.Play(number.ToString(), color);
  }

  public void SetArrow(bool active)
  {
    this.Arrow.SetActive(active);
  }

  public void SetBuffs(List<BattleBuff> buffs)
  {
    this.BuffBar.SetBuffs(buffs);
  }

  public void DecreaseCDTime()
  {
  }
}
