﻿// Decompiled with JetBrains decompiler
// Type: ConfigExchangeHallSort1
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigExchangeHallSort1
{
  private List<ExchangeHallSort1Info> _exchangeHallSort1InfoList = new List<ExchangeHallSort1Info>();
  private Dictionary<string, ExchangeHallSort1Info> _datas;
  private Dictionary<int, ExchangeHallSort1Info> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ExchangeHallSort1Info, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, ExchangeHallSort1Info>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._exchangeHallSort1InfoList.Add(enumerator.Current);
    }
    this._exchangeHallSort1InfoList.Sort((Comparison<ExchangeHallSort1Info>) ((a, b) => a.priority.CompareTo(b.priority)));
  }

  public ExchangeHallSort1Info Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (ExchangeHallSort1Info) null;
  }

  public ExchangeHallSort1Info Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (ExchangeHallSort1Info) null;
  }

  public List<ExchangeHallSort1Info> GetExchangeHallSort1InfoList()
  {
    return this._exchangeHallSort1InfoList;
  }
}
