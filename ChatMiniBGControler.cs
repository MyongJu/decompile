﻿// Decompiled with JetBrains decompiler
// Type: ChatMiniBGControler
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ChatMiniBGControler : MonoBehaviour
{
  [SerializeField]
  private ChatMiniDlg chatMiniDlg;

  public void OnDragEnd()
  {
    if ((double) Mathf.Abs(UICamera.currentTouch.totalDelta.x) > 75.0)
      this.chatMiniDlg.SwitchChannel();
    else
      this.chatMiniDlg.OnAreaClick();
  }

  public void OnClick()
  {
    this.chatMiniDlg.OnAreaClick();
  }
}
