﻿// Decompiled with JetBrains decompiler
// Type: NotificationSettingPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;
using UnityEngine;

public class NotificationSettingPopup : Popup
{
  public UILabel title;
  public UIGrid grid;
  public UIScrollView scroll;
  public NotificationSettingItem itemPrefab;

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private NotificationSettingItem CreateItem()
  {
    GameObject go = NGUITools.AddChild(this.grid.gameObject, this.itemPrefab.gameObject);
    NGUITools.SetActive(go, true);
    return go.GetComponent<NotificationSettingItem>();
  }

  private void UpdateUI()
  {
    this.title.text = ScriptLocalization.Get("id_notification_title", true);
    this.CreateItem(1);
    this.CreateItem(2);
    this.CreateItem(3);
    this.CreateItem(4);
    this.CreateItem(6);
    this.CreateItem(7);
    this.CreateItem(8);
    this.CreateItem(10);
    this.CreateItem(11);
    this.CreateItem(5);
  }

  private void CreateItem(int type)
  {
    NotificationSettingItem notificationSettingItem = this.CreateItem();
    notificationSettingItem.Selected = this.GetSelected(type);
    notificationSettingItem.title.text = this.GetTitle(type);
    notificationSettingItem.desc.text = this.GetDesc(type);
    notificationSettingItem.type = type;
  }

  private string GetTitle(int type)
  {
    string Term = string.Empty;
    switch (type)
    {
      case 1:
        Term = "notification_war_title";
        break;
      case 2:
        Term = "notification_train_title";
        break;
      case 3:
        Term = "notification_mail_title";
        break;
      case 4:
        Term = "notification_alliance_title";
        break;
      case 5:
        Term = "notification_misc_title";
        break;
      case 6:
        Term = "notification_alliance_chat_title";
        break;
      case 7:
        Term = "notification_gve_title";
        break;
      case 8:
        Term = "notification_auction_title";
        break;
      case 10:
        Term = "notification_world_auction_title";
        break;
      case 11:
        Term = "tower_name";
        break;
    }
    return ScriptLocalization.Get(Term, true);
  }

  private string GetDesc(int type)
  {
    string Term = string.Empty;
    switch (type)
    {
      case 1:
        Term = "notification_war_desc";
        break;
      case 2:
        Term = "notification_train_desc";
        break;
      case 3:
        Term = "notification_mail_desc";
        break;
      case 4:
        Term = "notification_alliance_desc";
        break;
      case 5:
        Term = "notification_misc_desc";
        break;
      case 6:
        Term = "notification_alliance_chat_desc";
        break;
      case 7:
        Term = "notification_gve_desc";
        break;
      case 8:
        Term = "notification_auction_desc";
        break;
      case 10:
        Term = "notification_world_auction_desc";
        break;
      case 11:
        Term = "tower_crystal_mines_accept_notifications_description";
        break;
    }
    return ScriptLocalization.Get(Term, true);
  }

  private bool GetSelected(int value)
  {
    return NotificationManager.Instance.IsOpen(value);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }
}
