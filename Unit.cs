﻿// Decompiled with JetBrains decompiler
// Type: Unit
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class Unit
{
  private string _class;
  private int _count;
  private Unit_StatisticsInfo _definition;

  public Unit(string className, int level, int count)
  {
    this._class = className;
    this._count = count;
    this._definition = ConfigManager.inst.DB_Unit_Statistics.GetData(this._class, level);
  }

  public Unit(string id, int count)
    : this(ConfigManager.inst.DB_Unit_Statistics.GetData(id), count)
  {
  }

  public Unit(Unit_StatisticsInfo unitDef, int count)
  {
    if (unitDef.ID.Contains("defense_vsinfantry"))
      this._class = "defense_vsinfantry";
    else if (unitDef.ID.Contains("defense_vscavalry"))
      this._class = "defense_vscavalry";
    else if (unitDef.ID.Contains("defense_vsranged"))
      this._class = "defense_vsranged";
    else if (unitDef.ID.Contains("defense_vsartilleryclose"))
      this._class = "defense_vsartilleryclose";
    else if (unitDef.ID.Contains("infantry"))
      this._class = "infantry";
    else if (unitDef.ID.Contains("cavalry"))
      this._class = "cavalry";
    else if (unitDef.ID.Contains("ranged"))
      this._class = "ranged";
    else if (unitDef.ID.Contains("mage"))
      this._class = "mage";
    else if (unitDef.ID.Contains("siege"))
      this._class = "siege";
    else if (unitDef.ID.Contains("hero"))
      this._class = "hero";
    else if (unitDef.ID.Contains("boss"))
      this._class = "boss";
    this._count = count;
    this._definition = unitDef;
  }

  public string Class
  {
    get
    {
      return this._class;
    }
  }

  public Unit_StatisticsInfo Definition
  {
    get
    {
      return this._definition;
    }
  }

  public int Count
  {
    get
    {
      return this._count;
    }
    set
    {
      this._count = value;
    }
  }

  public void EnterCity()
  {
    this.UpdateGarrison(this._count);
  }

  public void LeaveCity()
  {
    this.UpdateGarrison(-this._count);
  }

  public void UpdateGarrison(int amount)
  {
    if (!this._definition.Type.Equals("basic"))
      return;
    if (this._definition.ID.Contains("infantry"))
      GameEngine.Instance.PlayerData.CityData.OffsetTroopTypeCount("infantry_t1", amount);
    else if (this._definition.ID.Contains("cavalry"))
      GameEngine.Instance.PlayerData.CityData.OffsetTroopTypeCount("cavalry_t1", amount);
    else if (this._definition.ID.Contains("ranged"))
      GameEngine.Instance.PlayerData.CityData.OffsetTroopTypeCount("ranged_t1", amount);
    else if (this._definition.ID.Contains("mage"))
    {
      GameEngine.Instance.PlayerData.CityData.OffsetTroopTypeCount("mage_t1", amount);
    }
    else
    {
      if (!this._definition.ID.Contains("siege"))
        return;
      GameEngine.Instance.PlayerData.CityData.OffsetTroopTypeCount("siege_t1", amount);
    }
  }

  public int GatherCapacity(int heroID = 0)
  {
    return Unit.GatherCapacity(this._definition, this._count, heroID);
  }

  public static int GatherCapacity(Unit_StatisticsInfo unitDef, int unitCount, int heroID)
  {
    if (unitDef != null && unitDef.Type.Equals("basic"))
    {
      if (unitDef.ID.Contains("infantry"))
        return (int) ((double) unitCount * (double) Property.EffectiveValue((float) unitDef.GatherCapacity, Property.INFANTRY_LOAD, heroID, Effect.ConditionType.all));
      if (unitDef.ID.Contains("cavalry"))
        return (int) ((double) unitCount * (double) Property.EffectiveValue((float) unitDef.GatherCapacity, Property.CAVALRY_LOAD, heroID, Effect.ConditionType.all));
      if (unitDef.ID.Contains("ranged"))
        return (int) ((double) unitCount * (double) Property.EffectiveValue((float) unitDef.GatherCapacity, Property.RANGED_LOAD, heroID, Effect.ConditionType.all));
      if (unitDef.ID.Contains("mage"))
        return (int) ((double) unitCount * (double) Property.EffectiveValue((float) unitDef.GatherCapacity, Property.ARTYCLOSE_LOAD, heroID, Effect.ConditionType.all));
      if (unitDef.ID.Contains("siege"))
        return (int) ((double) unitCount * (double) Property.EffectiveValue((float) unitDef.GatherCapacity, Property.ARTYFAR_LOAD, heroID, Effect.ConditionType.all));
    }
    return 0;
  }
}
