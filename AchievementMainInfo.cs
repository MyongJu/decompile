﻿// Decompiled with JetBrains decompiler
// Type: AchievementMainInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class AchievementMainInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "image")]
  public string Image;
  [Config(Name = "category")]
  public string Category;
  [Config(Name = "loc_name_id")]
  public string LocName;
  [Config(Name = "loc_description_id")]
  public string LocDesc;
}
