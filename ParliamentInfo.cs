﻿// Decompiled with JetBrains decompiler
// Type: ParliamentInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ParliamentInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "unlock_level")]
  public int unlockLevel;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "priority")]
  public int priority;
  [Config(Name = "image")]
  public string image;
  [Config(Name = "position_type")]
  public int positionType;

  public string Name
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }

  public string ImagePath
  {
    get
    {
      return string.Format("Texture/HeroCard/{0}", (object) this.image);
    }
  }

  public int GetPosition()
  {
    int result = 0;
    int.TryParse(this.id, out result);
    return result;
  }
}
