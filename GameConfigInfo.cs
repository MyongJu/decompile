﻿// Decompiled with JetBrains decompiler
// Type: GameConfigInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class GameConfigInfo : IGameData
{
  private int _internalID;
  private string _id;
  private string _value;

  public GameConfigInfo(int internalID, string id, string value)
  {
    this._internalID = internalID;
    this._id = id;
    this._value = value;
  }

  public GameConfigInfo()
  {
  }

  public int InternalID
  {
    get
    {
      return this._internalID;
    }
  }

  public string ID
  {
    get
    {
      return this._id;
    }
  }

  public double Value
  {
    get
    {
      return this.GetDoublValue();
    }
  }

  public string ValueString
  {
    get
    {
      return this._value;
    }
  }

  public int ValueInt
  {
    get
    {
      return this.GetIntValue();
    }
  }

  public long ValueLong
  {
    get
    {
      return this.GetLongValue();
    }
  }

  public string Name
  {
    get
    {
      return "Configuration setting";
    }
  }

  private int GetIntValue()
  {
    int result;
    int.TryParse(this._value, out result);
    return result;
  }

  private double GetDoublValue()
  {
    double result;
    double.TryParse(this._value, out result);
    return result;
  }

  private long GetLongValue()
  {
    long result;
    long.TryParse(this._value, out result);
    return result;
  }
}
