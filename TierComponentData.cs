﻿// Decompiled with JetBrains decompiler
// Type: TierComponentData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class TierComponentData : IComponentData
{
  public string tier;
  public Hashtable list;

  public TierComponentData(string tier, Hashtable list)
  {
    this.tier = tier;
    this.list = list;
  }

  public TierComponentData()
  {
  }
}
