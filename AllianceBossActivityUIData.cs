﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossActivityUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;

public class AllianceBossActivityUIData : ActivityBaseUIData
{
  public override ActivityBaseUIData.ActivityType Type
  {
    get
    {
      return ActivityBaseUIData.ActivityType.AllianceBoss;
    }
  }

  public override string EventKey
  {
    get
    {
      return "alliance_boss";
    }
  }

  public override bool IsStart()
  {
    if (DBManager.inst.DB_AlliancePortal.GetAlliancePortalData() != null)
    {
      switch (AllianceBuildUtils.GetAlliancePortalBossSummonState())
      {
        case AlliancePortalData.BossSummonState.ACTIVATED:
        case AlliancePortalData.BossSummonState.ACTIVATED_TO_RALLY:
          return true;
      }
    }
    return false;
  }

  public override bool IsFinish()
  {
    return !this.IsStart();
  }

  public override IconData GetNormalIconData()
  {
    IconData iconData = new IconData();
    iconData.image = "Texture/Events/icon_alliance_boss";
    iconData.contents = new string[2];
    iconData.contents[0] = Utils.XLAT("alliance_portal_challenge_title");
    iconData.contents[1] = Utils.XLAT("alliance_portal_challenge_one_line_description");
    iconData.Data = (object) this;
    return iconData;
  }

  public override IconData GetADIconData()
  {
    IconData iconData = new IconData();
    iconData.image = "Texture/Events/alliance_boss_image_big";
    iconData.contents = new string[2];
    iconData.contents[0] = Utils.XLAT("alliance_portal_challenge_title");
    iconData.contents[1] = Utils.XLAT("alliance_portal_challenge_one_line_description");
    return iconData;
  }

  public override void DisplayActivityDetail()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceBossDetailsDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }
}
