﻿// Decompiled with JetBrains decompiler
// Type: CreateRoomStepDlg2
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class CreateRoomStepDlg2 : MonoBehaviour
{
  private string newRoomName;
  private bool newRoomIsPrivate;
  [SerializeField]
  private CreateRoomStepDlg2.Panel panel;

  public void OnCloseButtonClick()
  {
    this.Hide();
  }

  public void OnDoneButtonClick()
  {
    MessageHub.inst.GetPortByAction("Chat:createRoom").SendRequest(Utils.Hash((object) "name", (object) this.newRoomName, (object) "is_open", !this.newRoomIsPrivate ? (object) "1" : (object) "2", (object) "is_search", (object) "1", (object) "title", (object) this.panel.input.value), (System.Action<bool, object>) ((result, orgSrc) =>
    {
      if (!result)
        return;
      MessageHub.inst.GetPortByAction("Chat:getInitChatRooms").SendRequest((Hashtable) null, (System.Action<bool, object>) null, true);
    }), true);
    this.Hide();
  }

  public void Show(string roomName, bool isPrivate)
  {
    this.gameObject.SetActive(true);
    this.newRoomName = roomName;
    this.newRoomIsPrivate = isPrivate;
    this.panel.input.value = string.Empty;
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  public void OnTextChanged()
  {
    this.panel.input.value = Utils.GetLimitStr(this.panel.input.value, 99);
  }

  [Serializable]
  protected class Panel
  {
    public UIInput input;
  }
}
