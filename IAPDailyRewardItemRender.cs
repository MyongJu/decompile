﻿// Decompiled with JetBrains decompiler
// Type: IAPDailyRewardItemRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class IAPDailyRewardItemRender : MonoBehaviour
{
  public UILabel itemCount;
  public UITexture itemTexture;
  public UITexture border;
  private int itemId;

  public void SetData(ItemStaticInfo itemStaticInfo, int itemCount)
  {
    this.itemId = itemStaticInfo.internalId;
    this.itemCount.text = itemCount.ToString();
    BuilderFactory.Instance.Build((UIWidget) this.itemTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    Utils.SetItemBackground(this.border, itemStaticInfo.internalId);
  }

  public void Release()
  {
    BuilderFactory.Instance.Release((UIWidget) this.itemTexture);
  }

  public void OnClickHandler()
  {
    Utils.ShowItemTip(this.itemId, this.border.transform, 0L, 0L, 0);
  }

  public void OnReleaseHandler()
  {
    Utils.StopShowItemTip();
  }

  public void OnPressHandler()
  {
    Utils.DelayShowTip(this.itemId, this.border.transform, 0L, 0L, 0);
  }
}
