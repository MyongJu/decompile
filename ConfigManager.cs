﻿// Decompiled with JetBrains decompiler
// Type: ConfigManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class ConfigManager : MonoBehaviour
{
  private const char splitChar = '^';
  public const string HeadersKey = "headers";
  public const char SheetSplitSymbol = '@';
  public const char TypeSplitSymbol = '.';
  public const char ValueSplitSymbol = '=';
  public const char AndSplitSymbol = '&';
  public const char OrSplitSymbol = '|';
  public const int InvalidIndex = -1;
  public const int INVALID_INTENAL_ID = -1;
  private bool bStarted;
  private static ConfigManager _singleton;
  private ConfigDragonKnightTalentBuff _DBDragonKnightTalentBuff;
  private ConfigVerificationReward _DB_VerificationReward;
  private ConfigKingdomBoss _DB_KingdomBoss;
  private ConfigAnniversaryChampionMain _DB_AnniversaryChampionMain;
  private ConfigAnniversaryChampionRewards _DB_AnniversaryChampionRewards;
  private ConfigActivityPriority _DB_ActivityPriority;
  private ConfigDKArenaParticipateCost _DB_DKArenaParticipateCost;
  private ConfigDKArenaParticipateReward _DB_DKArenaParticipateReward;
  private ConfigDKArenaRankReward _DB_DKArenaRankReward;
  private ConfigDKArenaTitleReward _DB_DKArenaTitleReward;
  private ConfigDungeonBuff _DB_DungeonBuff;
  public ConfigMagicStoreRefreshPrice _DB_MagicStoreRefreshPrice;
  private ConfigMagicSelter _DB_MagicSelter;
  private ConfigMagicStore _DB_MagicStore;
  private ConfigMagicTowerMain _DB_MagicTowerMain;
  private ConfigMagicTowerMonster _DB_MagicTowerMonster;
  private ConfigMagicTowerPlayerUnit _DB_MagicTowerPlayerUnit;
  private ConfigMagicTowerShop _DB_MagicTowerShop;
  private ConfigDungeonChest _DB_DungeonChest;
  private ConfigDungeonMonsterMain _dungeonMonsterMain;
  private ConfigDungeonMonsterGroup _dungeonMonsterGroup;
  public ConfigProperties _DB_Properties;
  public ConfigSevenDayQuests _SevenDayQuests;
  public ConfigSevenDayRewards _SevenDayRewards;
  private ConfigDragonKnightPropertyRate _DB_DragonKnight_Property_Rate;
  public ConfigHero_Equipment _DB_Hero_Equipment;
  public ConfigKingdomDefinitions _DB_KingdomDefinitions;
  public ConfigShop _DB_Shop;
  private ConfigShopCommonMain _DB_ShopCommonMain;
  private ConfigShopCommonGroup _DB_ConfigShopCommonGroup;
  private ConfigShopCommonCategory _DB_ConfigShopCommonCategory;
  public ConfigBuilding _DB_Building;
  public ConfigGameConfig _DB_GameConfig;
  private ConfigUnit_Statistics _DB_Unit_Statistics;
  private ConfigKingSkill _DB_KingSkill;
  public ConfigGift_Level _DB_Gift_Level;
  public ConfigKingdomConfig _DB_KingdomConfig;
  public ConfigTechs _DB_Techs;
  public ConfigTechLevels _DB_TechLevels;
  private ConfigAllianceActivityChest _DB_AllianceActivityChest;
  private ConfigAlliancePersonalChest _DB_AlliancePersonalChest;
  private ConfigAllianceTreasuryVaultMain _DB_AllianceTreasuryVaultMain;
  private ConfigAbyssCollectReward _DB_AbyssCollectReward;
  private Config_AllianceWarRewards _DB_AllianceWarRewards;
  private ConfigAllianceWarScoreReward _DB_AllianceWarScoreReward;
  private ConfigAbyssMine _DB_AbyssMine;
  private ConfigAbyssRankReward _DB_AbyssRankReward;
  private ConfigAbyssTeleport _DB_AbyssTeleport;
  private ConfigAbyssSpeedUp _DB_AbyssSpeedUp;
  private ConfigAchievement _DB_Achievement;
  private ConfigAchievementMain _DB_AchievementMain;
  public ConfigQuest _DB_Quest;
  public ConfigAllianceQuest _DB_AllianceQuest;
  public ConfigAllianceShop _DB_AllianceShop;
  public ConfigAlliance_Level _DB_Alliance_Level;
  public ConfigTile_Statistics _DB_Tile_Statistics;
  public ConfigLookup _DB_Lookup;
  public ConfigVIP_Level _DB_VIP_Level;
  public ConfigHeroPoint _DB_Hero_Point;
  private ConfigHeroProfile _DB_HeroProfile;
  public ConfigToast _DB_Toast;
  private ConfigWorldBoss _DB_WorldBoss;
  private ConfigDragonSkillEnhance _DB_DragonSkillEnhance;
  public ConfigWorldEncounter _DB_WorldEncount;
  public ConfigMiniWonder _DB_MiniWonder;
  public ConfigBenefitCalc _DB_BenefitCalc;
  public ConfigAudio _DB_Audio;
  public ConfigItem _DB_Items;
  public ConfigBoost _DB_Boosts;
  public ConfigQuestLinkHUD _DB_QuestLinkHud;
  public ConfigRallySlot _DB_RallySlot;
  public ConfigImage _DB_Images;
  public ConfigLegend _DB_Legend;
  public ConfigLegendPoint _DB_LegendPoint;
  public ConfigLegendSkill _DB_LegendSkill;
  public ConfigLegendTower _DB_LegendTower;
  public ConfigAddress _DB_ConfigAddress;
  public ConfigTalentTree _DB_TalentTree;
  public ConfigPlayerTalent _DB_PlayerTalent;
  public ConfigLegendSlot _DB_LegendSlots;
  public ConfigEffect _DB_Effect;
  public ConfigNpcStore _DB_NpcStore;
  public ConfigRuralBlock _DB_RuralBlock;
  public ConfigBigTimerChest _DB_BigTimerChest;
  public ConfigRuralPlotsUnlock _DB_RuralPlotsUnlock;
  public ConfigNpcStoreRefreshPrice _DB_NpcStoreRefreshPrice;
  public ConfigAllianceTech _DB_AllianceTech;
  public ConfigRuralBuildingLimit _DB_ConfigRuralBuildingLimit;
  public ConfigHeroTalentActive _DB_HeroTalentActive;
  public ConfigDailyActivy _DB_DailyActivies;
  public ConfigFestivalActives _DB_FestivalActives;
  public ConfigFestivalVisit _DB_FestivalVisit;
  public ConfigFestivalExchange _DB_FestivalExchange;
  public ConfigFestivalExchangeReward _DB_FestivalExchangeReward;
  public ConfigDragon _DB_ConfigDragon;
  public ConfigAllianceDonateRewards _DB_ConfigADR;
  public ConfigDragonSkillGroup _DB_ConfigDragonSkillGroup;
  public ConfigDragonSkillMain _DB_ConfigDragonSkillMain;
  public ConfigDragonTendencyBoost _DB_ConfigDragonTendencyBoost;
  public ConfigDragonKnightTalent _DB_DragonKnightTalent;
  public ConfigDragonKnightTalentTree _DB_DragonKnightTalentTree;
  public ConfigDragonKnightTalentActive _DB_DragonKnightTalentActive;
  public ConfigDragonKnightLevel _DB_DragonKnightLevel;
  private ConfigDragonKnightItem _DB_DragonKnightItem;
  public ConfigDragonKnightSkill _DB_DragonKnightSkill;
  public ConfigDragonKnightSkillStage _DB_DragonKnightSkillStage;
  private ConfigDungeonMain _DB_DungeonMain;
  public ConfigDungeonRankingReward _DB_DungeonRankingReward;
  public ConfigSignIn _DB_SignIn;
  public ConfigDropGroup _DB_DropGroup;
  public ConfigDropMain _DB_DropMain;
  public ConfigWishWell _DB_WishWell;
  public ConfigWishWellTimes _DB_WishWellTimes;
  public ConfigGveBoss _DB_GveBoss;
  public ConfigBattleWounded _DB_BattleWounded;
  public ConfigHelp _DB_Help;
  private ConfigPrivilege _DB_Privilege;
  public ConfigNotification _DB_Notifications;
  public ConfigActivityMain _DB_ActivityMain;
  public ConfigActivityMainSub _DB_ActivityMainSub;
  private ConfigPersonalActivityRankReward _DB_PersonalActivtyRankReward;
  private ConfigPersonalActivity _DB_PersonalActivtyReward;
  private ConfigWorldFlag _DB_WorldFlag;
  public ConfigActivityReward _DB_ActivityReward;
  public ConfigActivityRequirement _DB_ActivityRequirement;
  public ConfigActivityRankReward _DB_ActivityRankReward;
  public ConfigAllianceActivityMainSub _DB_AllianceActivityMainSub;
  public ConfigAllianceActivityReward _DB_AllianceActivityReward;
  public ConfigAllianceActivityRequirement _DB_AllianceActivityRequirement;
  public ConfigAllianceActivityRankReward _DB_AllianceActivityRankReward;
  public ConfigFallenKnightScoreReq _DB_FallenKnightScoreReq;
  public ConfigFallenKnightDifficult _DB_FallenKnightDifficult;
  public ConfigFallenKnightMain _DB_FallenKnightMain;
  public ConfigFallenKnightRankReward _DB_FallenKnightRankReward;
  public ConfigCasinoRoulette _DB_CasinoRoulette;
  public ConfigCasinoChest _DB_CasinoChest;
  public ConfigCasinoJackpot _DB_CasinoJackpot;
  public ConfigAllianceBuilding _DB_AllianceBuildings;
  public ConfigAllianceBoss _DB_AllianceBoss;
  public ConfigAllianceBossRankReward _DB_AllianceBossRankReward;
  public ConfigTreasury _DB_Treasury;
  private ConfigTreasureMap _DB_TreasureMap;
  public ConfigKingdomBuff _DB_KingdomBuff;
  public ConfigDiyLotteryMain _DB_DiyLotteryMain;
  public ConfigDiyLotteryGroup _DB_DiyLotteryGroup;
  private ConfigLuckyArcher _DB_LuckyArcher;
  private ConfigLuckyArcherGroup _DB_LuckyArcherGroup;
  private ConfigLuckyArcherRewards _DB_LuckyArcherRewards;
  private ConfigLuckyArcherStepCost _DB_LuckyArcherStepCost;
  private ConfigDiceGroup _DB_DiceGroup;
  private ConfigDiceMain _DB_DiceMain;
  private ConfigDiceScoreBox _DB_DiceScoreBox;
  private ConfigExchangeHall _DB_ExchangeHall;
  private ConfigExchangeHallSort1 _DB_ExchangeHallSort1;
  private ConfigExchangeHallSort2 _DB_ExchangeHallSort2;
  private ConfigExchangeHallSort3 _DB_ExchangeHallSort3;
  private ConfigAnniversaryIapMain _DB_AnniversaryIapMain;
  private ConfigAnniversaryCommunityeReward _DB_AnniversaryCommunityReward;
  private ConfigAnniversaryStrongHoldDisplay _DB_AnniversaryStrongHoldDisplay;
  private ConfigBuildingGloryGroup _DB_BuildingGloryGroup;
  private ConfigBuildingGloryMain _DB_BuildingGloryMain;
  private ConfigParliament _DB_Parliament;
  private ConfigParliamentHero _DB_ParliamentHero;
  private ConfigParliamentHeroCard _DB_ParliamentHeroCard;
  private ConfigParliamentHeroLevel _DB_ParliamentHeroLevel;
  private ConfigParliamentHeroQuality _DB_ParliamentHeroQuality;
  private ConfigParliamentHeroStar _DB_ParliamentHeroStar;
  private ConfigParliamentHeroSummon _DB_ParliamentHeroSummon;
  private ConfigParliamentHeroSummonGroup _DB_ParliamentHeroSummonGroup;
  private ConfigParliamentSuitGroup _DB_ParliamentSuitGroup;
  public ConfigWonderTitle _DB_WonderTitle;
  public ConfigArtifact _DB_Artifact;
  public ConfigArtifactShop _DB_ArtifactShop;
  public ConfigSuperLoginMain _DB_SuperLoginMain;
  public ConfigSuperLoginGroup _DB_SuperLoginGroup;
  public ConfigPlayerProfile _DB_PlayerProfile;
  public ConfigDragonKnightProfile _DB_DragonKnightProfile;
  public ConfigQuestGroupMain _DB_QuestGroupMain;
  private ConfigQuestGroup _DB_QuestGroup;
  private ConfigSimulatorForbidden _DB_SimulatorForbidden;
  private ConfigEquipmentGem _DB_equipmentGem;
  private ConfigEquipmentGemHandbook _DB_gemHandbook;
  private ConfigEquipmentGemHandbookSort _DB_gemHandbookSort;
  private ConfigTavernWheelMain _DB_tavernWheelMain;
  private ConfigTavernWheelGroup _DB_tavernWheelGroup;
  private ConfigTavernWheelGrade _DB_tavernWheelGrade;
  private ConfigEquipmentMain _dragonKnightEquipmentMain;
  private ConfigEquipmentMain _heroEquipmentMain;
  private ConfigEquipmentProperty _dragonKnightEquipmentProperty;
  private ConfigEquipmentProperty _heroEquipmentProperty;
  private ConfigEquipmentScroll _dragonKnightEquipmentScroll;
  private ConfigEquipmentScroll _heroEquipmentScroll;
  private ConfigEquipmentScrollChip _dragonKnightEquipmentScrollChip;
  private ConfigEquipmentScrollChip _heroEquipmentScrollChip;
  private ConfigEquipmentScrollCombine _dragonKnightEquipmentScrollCombine;
  private ConfigEquipmentScrollCombine _heroEquipmentScrollCombine;
  private ConfigItemCompose _DB_ItemCompose;
  public ConfigBuildingIdle _DB_BuildingIdle;
  public ConfigBuildingIdleCommon _DB_BuildingIdleCommon;
  private ConfigIAP_Package _DB_IAPPackage;
  private ConfigIAP_DailyPackage _DB_IAPDailyPackage;
  private ConfigIAP_DailyPackageRewards _DB_IAPDailyPackageRewards;
  private ConfigIAP_DailyPackageSubRewards _DB_IAPDailyPackageSubRewards;
  private ConfigIAP_Platform _DB_IAPPlatform;
  private ConfigIAP_PackageGroup _IAPPackageGroup;
  private ConfigIAP_FakePrice _DB_IAPPrice;
  private ConfigIAP_PaymentLevel _DB_IAPPaymentLevel;
  private ConfigIAP_Gold _DB_IAPGold;
  public ConfigIAP_Rewards _DB_IAPRewards;
  public ConfigIAPMonthCard _DB_IAPMonthCard;
  public ConfigMonthCard _DB_MonthCard;
  public ConfigSubscription _DB_Subscription;
  private ConfigNPC _DB_NPC;
  private ConfigMonsterDisplay _DB_MonsterDisplay;
  private ConfigTempleSkill _DB_TempleSkill;
  private ConfigWonderPackage _DB_WonderPackage;
  private ConfigEquipmentSuitMain _DB_EquipmentSuitMain;
  private ConfigEquipmentSuitEnhance _DB_EquipmentSuitEnhance;
  private ConfigEquipmentSuitGroup _DB_EquipemtSuitGroup;
  private ConfigAltarSummonRewardMain _DB_ConfigAltarSummonRewardMain;
  private ConfigMerlinTrialsGroup _DB_MerlinTrialsGroup;
  private ConfigMerlinTrialsLayer _DB_MerlinTrialsLayer;
  private ConfigMerlinTrialsMain _DB_MerlinTrialsMain;
  private ConfigMerlinTrialsMonster _DB_MerlinTrialsMonster;
  private ConfigMerlinTrialsRewards _DB_MerlinTrialsRewards;
  private ConfigMerlinTrialsShop _DB_MerlinTrialsShop;
  private ConfigMerlinTrialsShopSpecific _DB_MerlinTrialsShopSpecific;
  private ConfigMerlinTrialsPlayerUnit _DB_MerlinTrialsPlayerUnit;
  private ConfigLordTitleMain _DB_LordTitleMain;
  private ConfigLordTitleSort _DB_LordTitleSort;

  public ConfigDragonKnightTalentBuff DBDragonKnightTalentBuff
  {
    get
    {
      if (this._DBDragonKnightTalentBuff == null)
      {
        this._DBDragonKnightTalentBuff = new ConfigDragonKnightTalentBuff();
        object rawData = this.GetRawData("dragon_knight_talent_buff.json");
        if (rawData != null)
          this._DBDragonKnightTalentBuff.BuildDB(rawData);
      }
      return this._DBDragonKnightTalentBuff;
    }
  }

  public ConfigVerificationReward DB_VerificationReward
  {
    get
    {
      if (this._DB_VerificationReward == null)
      {
        this._DB_VerificationReward = new ConfigVerificationReward();
        object rawData = this.GetRawData("real_name_verification_reward.json");
        if (rawData != null)
          this._DB_VerificationReward.BuildDB(rawData);
      }
      return this._DB_VerificationReward;
    }
  }

  public ConfigKingdomBoss DB_KingdomBoss
  {
    get
    {
      if (this._DB_KingdomBoss == null)
      {
        this._DB_KingdomBoss = new ConfigKingdomBoss();
        object rawData = this.GetRawData("kingdom_boss.json");
        if (rawData != null)
          this._DB_KingdomBoss.BuildDB(rawData);
      }
      return this._DB_KingdomBoss;
    }
  }

  public ConfigAnniversaryChampionMain DB_AnniversaryChampionMain
  {
    get
    {
      if (this._DB_AnniversaryChampionMain == null)
      {
        this._DB_AnniversaryChampionMain = new ConfigAnniversaryChampionMain();
        object rawData = this.GetRawData("anniversary_champion_main.json");
        if (rawData != null)
          this._DB_AnniversaryChampionMain.BuildDB(rawData);
      }
      return this._DB_AnniversaryChampionMain;
    }
  }

  public ConfigAnniversaryChampionRewards DB_AnniversaryChampionRewards
  {
    get
    {
      if (this._DB_AnniversaryChampionRewards == null)
      {
        this._DB_AnniversaryChampionRewards = new ConfigAnniversaryChampionRewards();
        object rawData = this.GetRawData("anniversary_champion_reward.json");
        if (rawData != null)
          this._DB_AnniversaryChampionRewards.BuildDB(rawData);
      }
      return this._DB_AnniversaryChampionRewards;
    }
  }

  public ConfigActivityPriority DB_ActivityPriority
  {
    get
    {
      if (this._DB_ActivityPriority == null)
      {
        this._DB_ActivityPriority = new ConfigActivityPriority();
        object rawData = this.GetRawData("activity_priority.json");
        if (rawData != null)
          this._DB_ActivityPriority.BuildDB(rawData);
      }
      return this._DB_ActivityPriority;
    }
  }

  public ConfigDKArenaParticipateCost DB_DKArenaParticipateCost
  {
    get
    {
      if (this._DB_DKArenaParticipateCost == null)
      {
        this._DB_DKArenaParticipateCost = new ConfigDKArenaParticipateCost();
        object rawData = this.GetRawData("dk_arena_participate_cost.json");
        if (rawData != null)
          this._DB_DKArenaParticipateCost.BuildDB(rawData);
      }
      return this._DB_DKArenaParticipateCost;
    }
  }

  public ConfigDKArenaParticipateReward DB_DKArenaParticipateReward
  {
    get
    {
      if (this._DB_DKArenaParticipateReward == null)
      {
        this._DB_DKArenaParticipateReward = new ConfigDKArenaParticipateReward();
        object rawData = this.GetRawData("dk_arena_participate_reward.json");
        if (rawData != null)
          this._DB_DKArenaParticipateReward.BuildDB(rawData);
      }
      return this._DB_DKArenaParticipateReward;
    }
  }

  public ConfigDKArenaRankReward DB_DKArenaRankReward
  {
    get
    {
      if (this._DB_DKArenaRankReward == null)
      {
        this._DB_DKArenaRankReward = new ConfigDKArenaRankReward();
        object rawData = this.GetRawData("dk_arena_rank_reward.json");
        if (rawData != null)
          this._DB_DKArenaRankReward.BuildDB(rawData);
      }
      return this._DB_DKArenaRankReward;
    }
  }

  public ConfigDKArenaTitleReward DB_DKArenaTitleReward
  {
    get
    {
      if (this._DB_DKArenaTitleReward == null)
      {
        this._DB_DKArenaTitleReward = new ConfigDKArenaTitleReward();
        object rawData = this.GetRawData("dk_arena_title_reward.json");
        if (rawData != null)
          this._DB_DKArenaTitleReward.BuildDB(rawData);
      }
      return this._DB_DKArenaTitleReward;
    }
  }

  public ConfigDungeonBuff DB_DungeonBuff
  {
    get
    {
      if (this._DB_DungeonBuff == null)
      {
        this._DB_DungeonBuff = new ConfigDungeonBuff();
        object rawData = this.GetRawData("dungeon_buff.json");
        if (rawData != null)
          this._DB_DungeonBuff.BuildDB(rawData);
      }
      return this._DB_DungeonBuff;
    }
  }

  public ConfigMagicStoreRefreshPrice DB_MagicStoreRefreshPrice
  {
    get
    {
      if (this._DB_MagicStoreRefreshPrice == null)
      {
        this._DB_MagicStoreRefreshPrice = new ConfigMagicStoreRefreshPrice();
        object rawData = this.GetRawData("magic_shop_refresh_price.json");
        if (rawData != null)
          this._DB_MagicStoreRefreshPrice.BuildDB(rawData);
      }
      return this._DB_MagicStoreRefreshPrice;
    }
    set
    {
      this._DB_MagicStoreRefreshPrice = value;
    }
  }

  public ConfigMagicSelter DB_MagicSelter
  {
    get
    {
      if (this._DB_MagicSelter == null)
      {
        this._DB_MagicSelter = new ConfigMagicSelter();
        object rawData = this.GetRawData("magic_smelter.json");
        if (rawData != null)
          this._DB_MagicSelter.BuildDB(rawData);
      }
      return this._DB_MagicSelter;
    }
  }

  public ConfigMagicStore DB_MagicStore
  {
    get
    {
      if ((UnityEngine.Object) this._DB_MagicStore == (UnityEngine.Object) null)
      {
        this._DB_MagicStore = new ConfigMagicStore();
        object rawData = this.GetRawData("magic_shop.json");
        if (rawData != null)
          this._DB_MagicStore.BuildDB(rawData);
      }
      return this._DB_MagicStore;
    }
  }

  public ConfigMagicTowerMain DB_MagicTowerMain
  {
    get
    {
      if (this._DB_MagicTowerMain == null)
      {
        this._DB_MagicTowerMain = new ConfigMagicTowerMain();
        object rawData = this.GetRawData("magic_tower_main.json");
        if (rawData != null)
          this._DB_MagicTowerMain.BuildDB(rawData);
      }
      return this._DB_MagicTowerMain;
    }
  }

  public ConfigMagicTowerMonster DB_MagicTowerMonster
  {
    get
    {
      if (this._DB_MagicTowerMonster == null)
      {
        this._DB_MagicTowerMonster = new ConfigMagicTowerMonster();
        object rawData = this.GetRawData("magic_tower_monster.json");
        if (rawData != null)
          this._DB_MagicTowerMonster.BuildDB(rawData);
      }
      return this._DB_MagicTowerMonster;
    }
  }

  public ConfigMagicTowerPlayerUnit DB_MagicTowerPlayerUnit
  {
    get
    {
      if (this._DB_MagicTowerPlayerUnit == null)
      {
        this._DB_MagicTowerPlayerUnit = new ConfigMagicTowerPlayerUnit();
        object rawData = this.GetRawData("magic_tower_player_unit.json");
        if (rawData != null)
          this._DB_MagicTowerPlayerUnit.BuildDB(rawData);
      }
      return this._DB_MagicTowerPlayerUnit;
    }
  }

  public ConfigMagicTowerShop DB_MagicTowerShop
  {
    get
    {
      if (this._DB_MagicTowerShop == null)
      {
        this._DB_MagicTowerShop = new ConfigMagicTowerShop();
        object rawData = this.GetRawData("magic_tower_shop.json");
        if (rawData != null)
          this._DB_MagicTowerShop.BuildDB(rawData);
      }
      return this._DB_MagicTowerShop;
    }
  }

  public ConfigDungeonChest DB_DungeonChest
  {
    get
    {
      if (this._DB_DungeonChest == null)
      {
        this._DB_DungeonChest = new ConfigDungeonChest();
        object rawData = this.GetRawData("dungeon_chest.json");
        if (rawData != null)
          this._DB_DungeonChest.BuildDB(rawData);
      }
      return this._DB_DungeonChest;
    }
  }

  public ConfigDungeonMonsterMain DungeonMonsterMain
  {
    get
    {
      if (this._dungeonMonsterMain == null)
      {
        this._dungeonMonsterMain = new ConfigDungeonMonsterMain();
        object rawData = this.GetRawData("dungeon_monster_main.json");
        if (rawData != null)
          this._dungeonMonsterMain.BuildDB(rawData);
      }
      return this._dungeonMonsterMain;
    }
  }

  public ConfigDungeonMonsterGroup DungeonMonsterGroup
  {
    get
    {
      if (this._dungeonMonsterGroup == null)
      {
        this._dungeonMonsterGroup = new ConfigDungeonMonsterGroup();
        object rawData = this.GetRawData("dungeon_monster_group.json");
        if (rawData != null)
          this._dungeonMonsterGroup.BuildDB(rawData);
      }
      return this._dungeonMonsterGroup;
    }
  }

  public ConfigProperties DB_Properties
  {
    get
    {
      if (this._DB_Properties == null)
      {
        this._DB_Properties = new ConfigProperties();
        object rawData = this.GetRawData("benefits.json");
        if (rawData != null)
          this._DB_Properties.BuildDB(rawData);
      }
      return this._DB_Properties;
    }
    set
    {
      this._DB_Properties = value;
    }
  }

  public ConfigSevenDayQuests SevenDayQuests
  {
    get
    {
      if (this._SevenDayQuests == null)
      {
        this._SevenDayQuests = new ConfigSevenDayQuests();
        object rawData = this.GetRawData("seven_days_quest.json");
        if (rawData != null)
          this._SevenDayQuests.BuildDB(rawData);
      }
      return this._SevenDayQuests;
    }
    set
    {
      this._SevenDayQuests = value;
    }
  }

  public ConfigSevenDayRewards DB_SevenDayRewards
  {
    get
    {
      if (this._SevenDayRewards == null)
      {
        this._SevenDayRewards = new ConfigSevenDayRewards();
        object rawData = this.GetRawData("seven_days_rewards.json");
        if (rawData != null)
          this._SevenDayRewards.BuildDB(rawData);
      }
      return this._SevenDayRewards;
    }
    set
    {
      this._SevenDayRewards = value;
    }
  }

  public ConfigDragonKnightPropertyRate DBDragonKnightPropertyRate
  {
    get
    {
      if (this._DB_DragonKnight_Property_Rate == null)
      {
        this._DB_DragonKnight_Property_Rate = new ConfigDragonKnightPropertyRate();
        object rawData = this.GetRawData("dragon_knight_property_rate.json");
        if (rawData != null)
          this._DB_DragonKnight_Property_Rate.BuildDB(rawData);
      }
      return this._DB_DragonKnight_Property_Rate;
    }
  }

  public ConfigHero_Equipment DB_Hero_Equipment
  {
    get
    {
      if (this._DB_Properties == null)
      {
        this._DB_Hero_Equipment = new ConfigHero_Equipment();
        object rawData = this.GetRawData("hero_equips.json");
        if (rawData != null)
          this._DB_Hero_Equipment.BuildDB(rawData);
      }
      return this._DB_Hero_Equipment;
    }
    set
    {
      this._DB_Hero_Equipment = value;
    }
  }

  public ConfigKingdomDefinitions DB_KingdomDefinitions
  {
    get
    {
      if (this._DB_KingdomDefinitions == null)
      {
        this._DB_KingdomDefinitions = new ConfigKingdomDefinitions();
        object rawData = this.GetRawData("kingdom_definition.json");
        if (rawData != null)
          this._DB_KingdomDefinitions.BuildDB(rawData);
      }
      return this._DB_KingdomDefinitions;
    }
    set
    {
      this._DB_KingdomDefinitions = value;
    }
  }

  public ConfigShop DB_Shop
  {
    get
    {
      if (this._DB_Shop == null)
      {
        this._DB_Shop = new ConfigShop();
        object rawData = this.GetRawData("shop.json");
        if (rawData != null)
          this._DB_Shop.BuildDB(rawData);
      }
      return this._DB_Shop;
    }
    set
    {
      this._DB_Shop = value;
    }
  }

  public ConfigShopCommonMain DB_ShopCommonMain
  {
    get
    {
      if (this._DB_ShopCommonMain == null)
      {
        this._DB_ShopCommonMain = new ConfigShopCommonMain();
        this._DB_ShopCommonMain.BuildDB(Utils.Json2Object(File.ReadAllText(NetApi.inst.BuildLocalPath("shop_common_main.json")), true));
      }
      return this._DB_ShopCommonMain;
    }
  }

  public ConfigShopCommonGroup DB_ShopCommonGroup
  {
    get
    {
      if (this._DB_ConfigShopCommonGroup == null)
      {
        this._DB_ConfigShopCommonGroup = new ConfigShopCommonGroup();
        this._DB_ConfigShopCommonGroup.BuildDB(Utils.Json2Object(File.ReadAllText(NetApi.inst.BuildLocalPath("shop_common_group.json")), true));
      }
      return this._DB_ConfigShopCommonGroup;
    }
  }

  public ConfigShopCommonCategory DB_ConfigShopCommonCategory
  {
    get
    {
      if (this._DB_ConfigShopCommonCategory == null)
      {
        this._DB_ConfigShopCommonCategory = new ConfigShopCommonCategory();
        this._DB_ConfigShopCommonCategory.BuildDB(Utils.Json2Object(File.ReadAllText(NetApi.inst.BuildLocalPath("shop_common_category.json")), true));
      }
      return this._DB_ConfigShopCommonCategory;
    }
  }

  public ConfigBuilding DB_Building
  {
    get
    {
      if (this._DB_Building == null)
      {
        this._DB_Building = new ConfigBuilding();
        object rawData = this.GetRawData("buildings.json");
        if (rawData != null)
          this._DB_Building.BuildDB(rawData);
      }
      return this._DB_Building;
    }
    set
    {
      this._DB_Building = value;
    }
  }

  public ConfigGameConfig DB_GameConfig
  {
    get
    {
      if (this._DB_GameConfig == null || !this._DB_GameConfig.Builded)
      {
        this._DB_GameConfig = new ConfigGameConfig();
        object rawData = this.GetRawData("game_configs.json");
        if (rawData != null)
          this._DB_GameConfig.BuildDB(rawData);
      }
      return this._DB_GameConfig;
    }
    set
    {
      this._DB_GameConfig = value;
    }
  }

  public ConfigUnit_Statistics DB_Unit_Statistics
  {
    get
    {
      if (this._DB_Unit_Statistics == null)
      {
        this._DB_Unit_Statistics = new ConfigUnit_Statistics();
        object rawData = this.GetRawData("unit_stats.json");
        if (rawData != null)
          this._DB_Unit_Statistics.BuildDB(rawData);
      }
      return this._DB_Unit_Statistics;
    }
  }

  public ConfigKingSkill DB_KingSkill
  {
    get
    {
      if (this._DB_KingSkill == null)
      {
        this._DB_KingSkill = new ConfigKingSkill();
        object rawData = this.GetRawData("king_open_buff.json");
        if (rawData != null)
          this._DB_KingSkill.BuildDB(rawData);
      }
      return this._DB_KingSkill;
    }
  }

  public ConfigGift_Level DB_Gift_Level
  {
    get
    {
      if (this._DB_Gift_Level == null)
        this._DB_Gift_Level = new ConfigGift_Level();
      return this._DB_Gift_Level;
    }
    set
    {
      this._DB_Gift_Level = value;
    }
  }

  public ConfigKingdomConfig DB_KingdomConfig
  {
    get
    {
      if (this._DB_KingdomConfig == null)
      {
        this._DB_KingdomConfig = new ConfigKingdomConfig();
        object rawData = this.GetRawData("unit_stats.json");
        if (rawData != null)
          this._DB_KingdomConfig.BuildDB(rawData);
      }
      return this._DB_KingdomConfig;
    }
    set
    {
      this._DB_KingdomConfig = value;
    }
  }

  public ConfigTechs DB_Techs
  {
    get
    {
      if (this._DB_Techs == null)
      {
        this._DB_Techs = new ConfigTechs();
        object rawData = this.GetRawData("tech.json");
        if (rawData != null)
          this._DB_Techs.BuildDB(rawData);
      }
      return this._DB_Techs;
    }
    set
    {
      this._DB_Techs = value;
    }
  }

  public ConfigTechLevels DB_TechLevels
  {
    get
    {
      if (this._DB_TechLevels == null)
      {
        this._DB_TechLevels = new ConfigTechLevels();
        object rawData = this.GetRawData("researches.json");
        if (rawData != null)
          this._DB_TechLevels.BuildDB(rawData);
      }
      return this._DB_TechLevels;
    }
    set
    {
      this._DB_TechLevels = value;
    }
  }

  public ConfigAllianceActivityChest DB_AllianceActivityChest
  {
    get
    {
      if (this._DB_AllianceActivityChest == null)
      {
        this._DB_AllianceActivityChest = new ConfigAllianceActivityChest();
        object rawData = this.GetRawData("alliance_activity_chest.json");
        if (rawData != null)
          this._DB_AllianceActivityChest.BuildDB(rawData);
      }
      return this._DB_AllianceActivityChest;
    }
  }

  public ConfigAlliancePersonalChest DB_AlliancePersonalChest
  {
    get
    {
      if (this._DB_AlliancePersonalChest == null)
      {
        this._DB_AlliancePersonalChest = new ConfigAlliancePersonalChest();
        object rawData = this.GetRawData("alliance_personal_chest.json");
        if (rawData != null)
          this._DB_AlliancePersonalChest.BuildDB(rawData);
      }
      return this._DB_AlliancePersonalChest;
    }
  }

  public ConfigAllianceTreasuryVaultMain DB_AllianceTreasuryVaultMain
  {
    get
    {
      if (this._DB_AllianceTreasuryVaultMain == null)
      {
        this._DB_AllianceTreasuryVaultMain = new ConfigAllianceTreasuryVaultMain();
        object rawData = this.GetRawData("alliance_treasury_vault_main.json");
        if (rawData != null)
          this._DB_AllianceTreasuryVaultMain.BuildDB(rawData);
      }
      return this._DB_AllianceTreasuryVaultMain;
    }
  }

  public ConfigAbyssCollectReward DB_AbyssCollectReward
  {
    get
    {
      if (this._DB_AbyssCollectReward == null)
      {
        this._DB_AbyssCollectReward = new ConfigAbyssCollectReward();
        object rawData = this.GetRawData("abyss_collect_reward.json");
        if (rawData != null)
          this._DB_AbyssCollectReward.BuildDB(rawData);
      }
      return this._DB_AbyssCollectReward;
    }
  }

  public Config_AllianceWarRewards DB_AllianceWarRewards
  {
    get
    {
      if (this._DB_AllianceWarRewards == null)
      {
        this._DB_AllianceWarRewards = new Config_AllianceWarRewards();
        object rawData = this.GetRawData("alliance_warfare_rank_rewards.json");
        if (rawData != null)
          this._DB_AllianceWarRewards.BuildDB(rawData);
      }
      return this._DB_AllianceWarRewards;
    }
  }

  public ConfigAllianceWarScoreReward DB_AllianceWarScoreReward
  {
    get
    {
      if (this._DB_AllianceWarScoreReward == null)
      {
        this._DB_AllianceWarScoreReward = new ConfigAllianceWarScoreReward();
        object rawData = this.GetRawData("alliance_warfare_score_rewards.json");
        if (rawData != null)
          this._DB_AllianceWarScoreReward.BuildDB(rawData);
      }
      return this._DB_AllianceWarScoreReward;
    }
  }

  public ConfigAbyssMine DB_AbyssMine
  {
    get
    {
      if (this._DB_AbyssMine == null)
      {
        this._DB_AbyssMine = new ConfigAbyssMine();
        object rawData = this.GetRawData("abyss_mine.json");
        if (rawData != null)
          this._DB_AbyssMine.BuildDB(rawData);
      }
      return this._DB_AbyssMine;
    }
  }

  public ConfigAbyssRankReward DB_AbyssRankReward
  {
    get
    {
      if (this._DB_AbyssRankReward == null)
      {
        this._DB_AbyssRankReward = new ConfigAbyssRankReward();
        object rawData = this.GetRawData("abyss_rank_reward.json");
        if (rawData != null)
          this._DB_AbyssRankReward.BuildDB(rawData);
      }
      return this._DB_AbyssRankReward;
    }
  }

  public ConfigAbyssTeleport DB_AbyssTeleport
  {
    get
    {
      if (this._DB_AbyssTeleport == null)
      {
        this._DB_AbyssTeleport = new ConfigAbyssTeleport();
        object rawData = this.GetRawData("abyss_city_teleport.json");
        if (rawData != null)
          this._DB_AbyssTeleport.BuildDB(rawData);
      }
      return this._DB_AbyssTeleport;
    }
  }

  public ConfigAbyssSpeedUp DB_AbyssSpeedUp
  {
    get
    {
      if (this._DB_AbyssSpeedUp == null)
      {
        this._DB_AbyssSpeedUp = new ConfigAbyssSpeedUp();
        object rawData = this.GetRawData("abyss_march_speedup.json");
        if (rawData != null)
          this._DB_AbyssSpeedUp.BuildDB(rawData);
      }
      return this._DB_AbyssSpeedUp;
    }
  }

  public ConfigAchievement DB_Achievement
  {
    get
    {
      if (this._DB_Achievement == null)
      {
        this._DB_Achievement = new ConfigAchievement();
        object rawData = this.GetRawData("achievement.json");
        if (rawData != null)
          this._DB_Achievement.BuildDB(rawData);
      }
      return this._DB_Achievement;
    }
  }

  public ConfigAchievementMain DB_AchievementMain
  {
    get
    {
      if (this._DB_AchievementMain == null)
      {
        this._DB_AchievementMain = new ConfigAchievementMain();
        object rawData = this.GetRawData("achievement_group.json");
        if (rawData != null)
          this._DB_AchievementMain.BuildDB(rawData);
      }
      return this._DB_AchievementMain;
    }
  }

  public ConfigQuest DB_Quest
  {
    get
    {
      if (this._DB_Quest == null)
      {
        this._DB_Quest = new ConfigQuest();
        object rawData1 = this.GetRawData("quests.json");
        if (rawData1 != null)
          this._DB_Quest.BuildDB(rawData1);
        object rawData2 = this.GetRawData("quest_group_main.json");
        if (rawData2 != null)
          this._DB_Quest.BuildDB(rawData2);
      }
      return this._DB_Quest;
    }
    set
    {
      this._DB_Quest = value;
    }
  }

  public ConfigAllianceQuest DB_AllianceQuest
  {
    get
    {
      if (this._DB_AllianceQuest == null)
      {
        this._DB_AllianceQuest = new ConfigAllianceQuest();
        object rawData = this.GetRawData("alliance_quest.json");
        if (rawData != null)
          this._DB_AllianceQuest.BuildDB(rawData);
      }
      return this._DB_AllianceQuest;
    }
    set
    {
      this._DB_AllianceQuest = value;
    }
  }

  public ConfigAllianceShop DB_AllianceShop
  {
    get
    {
      if (this._DB_AllianceShop == null)
      {
        this._DB_AllianceShop = new ConfigAllianceShop();
        object rawData = this.GetRawData("alliance_shop.json");
        if (rawData != null)
          this._DB_AllianceShop.BuildDB(rawData);
      }
      return this._DB_AllianceShop;
    }
    set
    {
      this._DB_AllianceShop = value;
    }
  }

  public ConfigAlliance_Level DB_Alliance_Level
  {
    get
    {
      if (this._DB_Alliance_Level == null)
      {
        this._DB_Alliance_Level = new ConfigAlliance_Level();
        object rawData = this.GetRawData("alliance_levels.json");
        if (rawData != null)
          this._DB_Alliance_Level.BuildDB(rawData);
      }
      return this._DB_Alliance_Level;
    }
    set
    {
      this._DB_Alliance_Level = value;
    }
  }

  public ConfigTile_Statistics DB_Tile_Statistics
  {
    get
    {
      if (this._DB_Tile_Statistics == null)
      {
        this._DB_Tile_Statistics = new ConfigTile_Statistics();
        object rawData = this.GetRawData("tile_stats.json");
        if (rawData != null)
          this._DB_Tile_Statistics.BuildDB(rawData);
      }
      return this._DB_Tile_Statistics;
    }
    set
    {
      this._DB_Tile_Statistics = value;
    }
  }

  public ConfigLookup DB_Lookup
  {
    get
    {
      if (this._DB_Lookup == null)
      {
        this._DB_Lookup = new ConfigLookup();
        object rawData = this.GetRawData("lookup.json");
        if (rawData != null)
          this._DB_Lookup.BuildDB(rawData);
      }
      return this._DB_Lookup;
    }
    set
    {
      this._DB_Lookup = value;
    }
  }

  public ConfigVIP_Level DB_VIP_Level
  {
    get
    {
      if (this._DB_VIP_Level == null)
      {
        this._DB_VIP_Level = new ConfigVIP_Level();
        object rawData = this.GetRawData("vip_level.json");
        if (rawData != null)
          this._DB_VIP_Level.BuildDB(rawData);
      }
      return this._DB_VIP_Level;
    }
    set
    {
      this._DB_VIP_Level = value;
    }
  }

  public ConfigHeroPoint DB_Hero_Point
  {
    get
    {
      if (this._DB_Hero_Point == null)
      {
        this._DB_Hero_Point = new ConfigHeroPoint();
        object rawData = this.GetRawData("hero_point.json");
        if (rawData != null)
          this._DB_Hero_Point.BuildDB(rawData);
      }
      return this._DB_Hero_Point;
    }
    set
    {
      this._DB_Hero_Point = value;
    }
  }

  public ConfigHeroProfile DB_HeroProfile
  {
    get
    {
      if (this._DB_HeroProfile == null)
      {
        this._DB_HeroProfile = new ConfigHeroProfile();
        object rawData = this.GetRawData("hero_profile.json");
        if (rawData != null)
          this._DB_HeroProfile.BuildDB(rawData);
      }
      return this._DB_HeroProfile;
    }
    set
    {
      this._DB_HeroProfile = value;
    }
  }

  public ConfigToast DB_Toast
  {
    get
    {
      if (this._DB_Toast == null)
      {
        this._DB_Toast = new ConfigToast();
        object rawData = this.GetRawData("toast.json");
        if (rawData != null)
          this._DB_Toast.BuildDB(rawData);
      }
      return this._DB_Toast;
    }
    set
    {
      this._DB_Toast = value;
    }
  }

  public ConfigWorldBoss DB_WorldBoss
  {
    get
    {
      if (this._DB_WorldBoss == null)
      {
        this._DB_WorldBoss = new ConfigWorldBoss();
        object rawData = this.GetRawData("world_boss.json");
        if (rawData != null)
          this._DB_WorldBoss.BuildDB(rawData);
      }
      return this._DB_WorldBoss;
    }
    set
    {
      this._DB_WorldBoss = value;
    }
  }

  public ConfigDragonSkillEnhance DB_DragonSkillEnhance
  {
    get
    {
      if (this._DB_DragonSkillEnhance == null)
      {
        this._DB_DragonSkillEnhance = new ConfigDragonSkillEnhance();
        object rawData = this.GetRawData("dragon_skill_enhance.json");
        if (rawData != null)
          this._DB_DragonSkillEnhance.BuildDB(rawData);
      }
      return this._DB_DragonSkillEnhance;
    }
    set
    {
      this._DB_DragonSkillEnhance = value;
    }
  }

  public ConfigWorldEncounter DB_WorldEncount
  {
    get
    {
      if (this._DB_WorldEncount == null)
      {
        this._DB_WorldEncount = new ConfigWorldEncounter();
        object rawData = this.GetRawData("world_encounter.json");
        if (rawData != null)
          this._DB_WorldEncount.BuildDB(rawData);
      }
      return this._DB_WorldEncount;
    }
    set
    {
      this._DB_WorldEncount = value;
    }
  }

  public ConfigMiniWonder DB_MiniWonder
  {
    get
    {
      if (this._DB_MiniWonder == null)
        this._DB_MiniWonder = new ConfigMiniWonder();
      return this._DB_MiniWonder;
    }
    set
    {
      this._DB_MiniWonder = value;
    }
  }

  public ConfigBenefitCalc DB_BenefitCalc
  {
    get
    {
      if (this._DB_BenefitCalc == null)
      {
        this._DB_BenefitCalc = new ConfigBenefitCalc();
        object rawData = this.GetRawData("benefit_calc.json");
        if (rawData != null)
          this._DB_BenefitCalc.BuildDB(rawData);
      }
      return this._DB_BenefitCalc;
    }
    set
    {
      this._DB_BenefitCalc = value;
    }
  }

  public ConfigAudio DB_Audio
  {
    get
    {
      if (this._DB_Audio == null || !this._DB_Audio.Builded)
      {
        this._DB_Audio = new ConfigAudio();
        object rawData = this.GetRawData("audio_setup.json");
        if (rawData != null)
          this._DB_Audio.BuildDB(rawData);
      }
      return this._DB_Audio;
    }
    set
    {
      this._DB_Audio = value;
    }
  }

  public ConfigItem DB_Items
  {
    get
    {
      if (this._DB_Items == null)
      {
        this._DB_Items = new ConfigItem();
        object rawData = this.GetRawData("itemlist.json");
        if (rawData != null)
          this._DB_Items.BuildDB(rawData);
      }
      return this._DB_Items;
    }
    set
    {
      this._DB_Items = value;
    }
  }

  public ConfigBoost DB_Boosts
  {
    get
    {
      if (this._DB_Boosts == null)
      {
        this._DB_Boosts = new ConfigBoost();
        object rawData = this.GetRawData("boosts.json");
        if (rawData != null)
          this._DB_Boosts.BuildDB(rawData);
      }
      return this._DB_Boosts;
    }
    set
    {
      this._DB_Boosts = value;
    }
  }

  public ConfigQuestLinkHUD DB_QuestLinkHud
  {
    get
    {
      if (this._DB_QuestLinkHud == null)
      {
        this._DB_QuestLinkHud = new ConfigQuestLinkHUD();
        object rawData = this.GetRawData("goto.json");
        if (rawData != null)
          this._DB_QuestLinkHud.BuildDB(rawData);
      }
      return this._DB_QuestLinkHud;
    }
    set
    {
      this._DB_QuestLinkHud = value;
    }
  }

  public ConfigRallySlot DB_RallySlot
  {
    get
    {
      if (this._DB_RallySlot == null)
      {
        this._DB_RallySlot = new ConfigRallySlot();
        object rawData = this.GetRawData("rally_slots.json");
        if (rawData != null)
          this._DB_RallySlot.BuildDB(rawData);
      }
      return this._DB_RallySlot;
    }
    set
    {
      this._DB_RallySlot = value;
    }
  }

  public ConfigImage DB_Images
  {
    get
    {
      if (this._DB_Images == null)
      {
        this._DB_Images = new ConfigImage();
        object rawData = this.GetRawData("images_info.json");
        if (rawData != null)
          this._DB_Images.BuildDB(rawData);
      }
      return this._DB_Images;
    }
    set
    {
      this._DB_Images = value;
    }
  }

  public ConfigLegend DB_Legend
  {
    get
    {
      if (this._DB_Legend == null)
        this._DB_Legend = new ConfigLegend();
      return this._DB_Legend;
    }
    set
    {
      this._DB_Legend = value;
    }
  }

  public ConfigLegendPoint DB_LegendPoint
  {
    get
    {
      if (this._DB_LegendPoint == null)
        this._DB_LegendPoint = new ConfigLegendPoint();
      return this._DB_LegendPoint;
    }
    set
    {
      this._DB_LegendPoint = value;
    }
  }

  public ConfigLegendSkill DB_LegendSkill
  {
    get
    {
      if (this._DB_LegendSkill == null)
        this._DB_LegendSkill = new ConfigLegendSkill();
      return this._DB_LegendSkill;
    }
    set
    {
      this._DB_LegendSkill = value;
    }
  }

  public ConfigLegendTower DB_LegendTower
  {
    get
    {
      if (this._DB_LegendTower == null)
      {
        this._DB_LegendTower = new ConfigLegendTower();
        object rawData = this.GetRawData("legend_tower.json");
        if (rawData != null)
          this._DB_LegendTower.BuildDB(rawData);
      }
      return this._DB_LegendTower;
    }
    set
    {
      this._DB_LegendTower = value;
    }
  }

  public ConfigAddress DB_ConfigAddress
  {
    get
    {
      if (this._DB_ConfigAddress == null)
      {
        this._DB_ConfigAddress = new ConfigAddress();
        object rawData = this.GetRawData("ConfigAddress.json");
        if (rawData != null)
          this._DB_ConfigAddress.BuildDB(rawData);
      }
      return this._DB_ConfigAddress;
    }
    set
    {
      this._DB_ConfigAddress = value;
    }
  }

  public ConfigTalentTree DB_TalentTree
  {
    get
    {
      if (this._DB_TalentTree == null)
      {
        this._DB_TalentTree = new ConfigTalentTree();
        object rawData = this.GetRawData("skill.json");
        if (rawData != null)
          this._DB_TalentTree.BuildDB(rawData);
      }
      return this._DB_TalentTree;
    }
    set
    {
      this._DB_TalentTree = value;
    }
  }

  public ConfigPlayerTalent DB_PlayerTalent
  {
    get
    {
      if (this._DB_PlayerTalent == null)
      {
        this._DB_PlayerTalent = new ConfigPlayerTalent();
        object rawData = this.GetRawData("hero_skill.json");
        if (rawData != null)
          this._DB_PlayerTalent.BuildDB(rawData);
      }
      return this._DB_PlayerTalent;
    }
    set
    {
      this._DB_PlayerTalent = value;
    }
  }

  public ConfigLegendSlot DB_LegendSlots
  {
    get
    {
      if (this._DB_LegendSlots == null)
      {
        this._DB_LegendSlots = new ConfigLegendSlot();
        object rawData = this.GetRawData("legend_slots.json");
        if (rawData != null)
          this._DB_LegendSlots.BuildDB(rawData);
      }
      return this._DB_LegendSlots;
    }
    set
    {
      this._DB_LegendSlots = value;
    }
  }

  public ConfigEffect DB_Effect
  {
    get
    {
      if (this._DB_Effect == null)
        this._DB_Effect = new ConfigEffect();
      return this._DB_Effect;
    }
    set
    {
      this._DB_Effect = value;
    }
  }

  public ConfigNpcStore DB_NpcStore
  {
    get
    {
      if (this._DB_NpcStore == null)
      {
        this._DB_NpcStore = new ConfigNpcStore();
        object rawData = this.GetRawData("npc_store.json");
        if (rawData != null)
          this._DB_NpcStore.BuildDB(rawData);
      }
      return this._DB_NpcStore;
    }
    set
    {
      this._DB_NpcStore = value;
    }
  }

  public ConfigRuralBlock DB_RuralBlock
  {
    get
    {
      if (this._DB_RuralBlock == null)
      {
        this._DB_RuralBlock = new ConfigRuralBlock();
        object rawData = this.GetRawData("rural_unblock.json");
        if (rawData != null)
          this._DB_RuralBlock.BuildDB(rawData);
      }
      return this._DB_RuralBlock;
    }
    set
    {
      this._DB_RuralBlock = value;
    }
  }

  public ConfigBigTimerChest DB_BigTimerChest
  {
    get
    {
      if (this._DB_BigTimerChest == null)
      {
        this._DB_BigTimerChest = new ConfigBigTimerChest();
        object rawData = this.GetRawData("big_timer_chest_drop.json");
        if (rawData != null)
          this._DB_BigTimerChest.BuildDB(rawData);
      }
      return this._DB_BigTimerChest;
    }
    set
    {
      this._DB_BigTimerChest = value;
    }
  }

  public ConfigRuralPlotsUnlock DB_RuralPlotsUnlock
  {
    get
    {
      if (this._DB_RuralPlotsUnlock == null)
      {
        this._DB_RuralPlotsUnlock = new ConfigRuralPlotsUnlock();
        object rawData = this.GetRawData("rural_super_unblock.json");
        if (rawData != null)
          this._DB_RuralPlotsUnlock.BuildDB(rawData);
      }
      return this._DB_RuralPlotsUnlock;
    }
    set
    {
      this._DB_RuralPlotsUnlock = value;
    }
  }

  public ConfigNpcStoreRefreshPrice DB_NpcStoreRefreshPrice
  {
    get
    {
      if (this._DB_NpcStoreRefreshPrice == null)
      {
        this._DB_NpcStoreRefreshPrice = new ConfigNpcStoreRefreshPrice();
        object rawData = this.GetRawData("npc_store_refresh_price.json");
        if (rawData != null)
          this._DB_NpcStoreRefreshPrice.BuildDB(rawData);
      }
      return this._DB_NpcStoreRefreshPrice;
    }
    set
    {
      this._DB_NpcStoreRefreshPrice = value;
    }
  }

  public ConfigAllianceTech DB_AllianceTech
  {
    get
    {
      if (this._DB_AllianceTech == null)
      {
        this._DB_AllianceTech = new ConfigAllianceTech();
        object rawData1 = this.GetRawData("alliance_research_main.json");
        if (rawData1 != null)
          this._DB_AllianceTech.BuildDB(rawData1);
        object rawData2 = this.GetRawData("alliance_research_tier.json");
        if (rawData2 != null)
          this._DB_AllianceTech.BuildTierDB(rawData2);
      }
      return this._DB_AllianceTech;
    }
    set
    {
      this._DB_AllianceTech = value;
    }
  }

  public ConfigRuralBuildingLimit DB_ConfigRuralBuildingLimit
  {
    get
    {
      if (this._DB_ConfigRuralBuildingLimit == null)
      {
        this._DB_ConfigRuralBuildingLimit = new ConfigRuralBuildingLimit();
        object rawData = this.GetRawData("rural_building_num.json");
        if (rawData != null)
          this._DB_ConfigRuralBuildingLimit.BuildDB(rawData);
      }
      return this._DB_ConfigRuralBuildingLimit;
    }
    set
    {
      this._DB_ConfigRuralBuildingLimit = value;
    }
  }

  public ConfigHeroTalentActive DB_HeroTalentActive
  {
    get
    {
      if (this._DB_HeroTalentActive == null)
      {
        this._DB_HeroTalentActive = new ConfigHeroTalentActive();
        object rawData = this.GetRawData("hero_skill_active.json");
        if (rawData != null)
          this._DB_HeroTalentActive.BuildDB(rawData);
      }
      return this._DB_HeroTalentActive;
    }
    set
    {
      this._DB_HeroTalentActive = value;
    }
  }

  public ConfigDailyActivy DB_DailyActivies
  {
    get
    {
      if (this._DB_DailyActivies == null)
      {
        this._DB_DailyActivies = new ConfigDailyActivy();
        object rawData1 = this.GetRawData("daily_reward_type.json");
        if (rawData1 != null)
          this._DB_DailyActivies.BuildDB_DailyActivyInfo(rawData1);
        object rawData2 = this.GetRawData("daily_reward.json");
        if (rawData2 != null)
          this._DB_DailyActivies.BuildDB_DailyActiveRewardInfo(rawData2);
        object rawData3 = this.GetRawData("stronghold_level_ratio.json");
        if (rawData3 != null)
          this._DB_DailyActivies.BuildDB_DailyRewardFactor(rawData3);
      }
      return this._DB_DailyActivies;
    }
    set
    {
      this._DB_DailyActivies = value;
    }
  }

  public ConfigFestivalActives DB_FestivalActives
  {
    get
    {
      if (this._DB_FestivalActives == null)
      {
        this._DB_FestivalActives = new ConfigFestivalActives();
        object rawData = this.GetRawData("festival_main.json");
        if (rawData != null)
          this._DB_FestivalActives.BuildDB(rawData);
      }
      return this._DB_FestivalActives;
    }
    set
    {
      this._DB_FestivalActives = value;
    }
  }

  public ConfigFestivalVisit DB_FestivalVisit
  {
    get
    {
      if (this._DB_FestivalVisit == null)
      {
        this._DB_FestivalVisit = new ConfigFestivalVisit();
        object rawData = this.GetRawData("festival_visit.json");
        if (rawData != null)
          this._DB_FestivalVisit.BuildDB(rawData);
      }
      return this._DB_FestivalVisit;
    }
    set
    {
      this._DB_FestivalVisit = value;
    }
  }

  public ConfigFestivalExchange DB_FestivalExchange
  {
    get
    {
      if (this._DB_FestivalExchange == null)
      {
        this._DB_FestivalExchange = new ConfigFestivalExchange();
        object rawData = this.GetRawData("festival_exchange.json");
        if (rawData != null)
          this._DB_FestivalExchange.BuildDB(rawData);
      }
      return this._DB_FestivalExchange;
    }
    set
    {
      this._DB_FestivalExchange = value;
    }
  }

  public ConfigFestivalExchangeReward DB_FestivalExchangeReward
  {
    get
    {
      if (this._DB_FestivalExchangeReward == null)
      {
        this._DB_FestivalExchangeReward = new ConfigFestivalExchangeReward();
        object rawData = this.GetRawData("festival_exchange_reward.json");
        if (rawData != null)
          this._DB_FestivalExchangeReward.BuildDB(rawData);
      }
      return this._DB_FestivalExchangeReward;
    }
    set
    {
      this._DB_FestivalExchangeReward = value;
    }
  }

  public ConfigDragon DB_ConfigDragon
  {
    get
    {
      if (this._DB_ConfigDragon == null)
      {
        this._DB_ConfigDragon = new ConfigDragon();
        object rawData = this.GetRawData("dragon_level.json");
        if (rawData != null)
          this._DB_ConfigDragon.BuildDB(rawData);
      }
      return this._DB_ConfigDragon;
    }
    set
    {
      this._DB_ConfigDragon = value;
    }
  }

  public ConfigAllianceDonateRewards DB_ConfigADR
  {
    get
    {
      if (this._DB_ConfigADR == null)
      {
        this._DB_ConfigADR = new ConfigAllianceDonateRewards();
        object rawData = this.GetRawData("alliance_donate_reward.json");
        if (rawData != null)
          this._DB_ConfigADR.BuildDB(rawData);
      }
      return this._DB_ConfigADR;
    }
    set
    {
      this._DB_ConfigADR = value;
    }
  }

  public ConfigDragonSkillGroup DB_ConfigDragonSkillGroup
  {
    get
    {
      if (this._DB_ConfigDragonSkillGroup == null)
      {
        this._DB_ConfigDragonSkillGroup = new ConfigDragonSkillGroup();
        object rawData = this.GetRawData("dragon_skill_group.json");
        if (rawData != null)
          this._DB_ConfigDragonSkillGroup.BuildDB(rawData);
      }
      return this._DB_ConfigDragonSkillGroup;
    }
    set
    {
      this._DB_ConfigDragonSkillGroup = value;
    }
  }

  public ConfigDragonSkillMain DB_ConfigDragonSkillMain
  {
    get
    {
      if (this._DB_ConfigDragonSkillMain == null)
      {
        this._DB_ConfigDragonSkillMain = new ConfigDragonSkillMain();
        object rawData = this.GetRawData("dragon_skill_main.json");
        if (rawData != null)
          this._DB_ConfigDragonSkillMain.BuildDB(rawData);
      }
      return this._DB_ConfigDragonSkillMain;
    }
    set
    {
      this._DB_ConfigDragonSkillMain = value;
    }
  }

  public ConfigDragonTendencyBoost DB_ConfigDragonTendencyBoost
  {
    get
    {
      if (this._DB_ConfigDragonTendencyBoost == null)
      {
        this._DB_ConfigDragonTendencyBoost = new ConfigDragonTendencyBoost();
        object rawData = this.GetRawData("dragon_tendency_boost.json");
        if (rawData != null)
          this._DB_ConfigDragonTendencyBoost.BuildDB(rawData);
      }
      return this._DB_ConfigDragonTendencyBoost;
    }
    set
    {
      this._DB_ConfigDragonTendencyBoost = value;
    }
  }

  public ConfigDragonKnightTalent DB_DragonKnightTalent
  {
    get
    {
      if (this._DB_DragonKnightTalent == null)
      {
        this._DB_DragonKnightTalent = new ConfigDragonKnightTalent();
        object rawData = this.GetRawData("dragon_knight_talent.json");
        if (rawData != null)
          this._DB_DragonKnightTalent.BuildDB(rawData);
      }
      return this._DB_DragonKnightTalent;
    }
    set
    {
      this._DB_DragonKnightTalent = value;
    }
  }

  public ConfigDragonKnightTalentTree DB_DragonKnightTalentTree
  {
    get
    {
      if (this._DB_DragonKnightTalentTree == null)
      {
        this._DB_DragonKnightTalentTree = new ConfigDragonKnightTalentTree();
        object rawData = this.GetRawData("dragon_knight_talent_tree.json");
        if (rawData != null)
          this._DB_DragonKnightTalentTree.BuildDB(rawData);
      }
      return this._DB_DragonKnightTalentTree;
    }
    set
    {
      this._DB_DragonKnightTalentTree = value;
    }
  }

  public ConfigDragonKnightTalentActive DB_DragonKnightTalentActive
  {
    get
    {
      if (this._DB_DragonKnightTalentActive == null)
      {
        this._DB_DragonKnightTalentActive = new ConfigDragonKnightTalentActive();
        object rawData = this.GetRawData("dragon_knight_talent_active.json");
        if (rawData != null)
          this._DB_DragonKnightTalentActive.BuildDB(rawData);
      }
      return this._DB_DragonKnightTalentActive;
    }
    set
    {
      this._DB_DragonKnightTalentActive = value;
    }
  }

  public ConfigDragonKnightLevel DB_DragonKnightLevel
  {
    get
    {
      if (this._DB_DragonKnightLevel == null)
      {
        this._DB_DragonKnightLevel = new ConfigDragonKnightLevel();
        object rawData = this.GetRawData("dragon_knight_level.json");
        if (rawData != null)
          this._DB_DragonKnightLevel.BuildDB(rawData);
      }
      return this._DB_DragonKnightLevel;
    }
    set
    {
      this._DB_DragonKnightLevel = value;
    }
  }

  public ConfigDragonKnightItem DB_DragonKnightItem
  {
    get
    {
      if (this._DB_DragonKnightItem == null)
      {
        this._DB_DragonKnightItem = new ConfigDragonKnightItem();
        object rawData = this.GetRawData("dragon_knight_item.json");
        if (rawData != null)
          this._DB_DragonKnightItem.BuildDB(rawData);
      }
      return this._DB_DragonKnightItem;
    }
  }

  public ConfigDragonKnightSkill DB_DragonKnightSkill
  {
    get
    {
      if (this._DB_DragonKnightSkill == null)
      {
        this._DB_DragonKnightSkill = new ConfigDragonKnightSkill();
        object rawData = this.GetRawData("dragon_knight_skill.json");
        if (rawData != null)
          this._DB_DragonKnightSkill.BuildDB(rawData);
      }
      return this._DB_DragonKnightSkill;
    }
    set
    {
      this._DB_DragonKnightSkill = value;
    }
  }

  public ConfigDragonKnightSkillStage DB_DragonKnightSkillStage
  {
    get
    {
      if (this._DB_DragonKnightSkillStage == null)
      {
        this._DB_DragonKnightSkillStage = new ConfigDragonKnightSkillStage();
        object rawData = this.GetRawData("dragon_knight_skill_stage.json");
        if (rawData != null)
          this._DB_DragonKnightSkillStage.BuildDB(rawData);
      }
      return this._DB_DragonKnightSkillStage;
    }
    set
    {
      this._DB_DragonKnightSkillStage = value;
    }
  }

  public ConfigDungeonMain DB_DungeonMain
  {
    get
    {
      if (this._DB_DungeonMain == null)
      {
        this._DB_DungeonMain = new ConfigDungeonMain();
        object rawData = this.GetRawData("dungeon_main.json");
        if (rawData != null)
          this._DB_DungeonMain.BuildDB(rawData);
      }
      return this._DB_DungeonMain;
    }
    set
    {
      this._DB_DungeonMain = value;
    }
  }

  public ConfigDungeonRankingReward DB_DungeonRankingReward
  {
    get
    {
      if (this._DB_DungeonRankingReward == null)
      {
        this._DB_DungeonRankingReward = new ConfigDungeonRankingReward();
        object rawData = this.GetRawData("dungeon_ranking_reward.json");
        if (rawData != null)
          this._DB_DungeonRankingReward.BuildDB(rawData);
      }
      return this._DB_DungeonRankingReward;
    }
    set
    {
      this._DB_DungeonRankingReward = value;
    }
  }

  public ConfigSignIn DB_SignIn
  {
    get
    {
      if (this._DB_SignIn == null)
      {
        this._DB_SignIn = new ConfigSignIn();
        object rawData = this.GetRawData("sign_in.json");
        if (rawData != null)
          this._DB_SignIn.BuildDB(rawData);
      }
      return this._DB_SignIn;
    }
    set
    {
      this._DB_SignIn = value;
    }
  }

  public ConfigDropGroup DB_DropGroup
  {
    get
    {
      if (this._DB_DropGroup == null)
      {
        this._DB_DropGroup = new ConfigDropGroup();
        object rawData = this.GetRawData("drop_group.json");
        if (rawData != null)
          this._DB_DropGroup.BuildDB(rawData);
      }
      return this._DB_DropGroup;
    }
    set
    {
      this._DB_DropGroup = value;
    }
  }

  public ConfigDropMain DB_DropMain
  {
    get
    {
      if (this._DB_DropMain == null)
      {
        this._DB_DropMain = new ConfigDropMain();
        object rawData = this.GetRawData("drop_main.json");
        if (rawData != null)
          this._DB_DropMain.BuildDB(rawData);
      }
      return this._DB_DropMain;
    }
    set
    {
      this._DB_DropMain = value;
    }
  }

  public ConfigWishWell DB_WishWell
  {
    get
    {
      if (this._DB_WishWell == null)
      {
        this._DB_WishWell = new ConfigWishWell();
        object rawData = this.GetRawData("wish_well.json");
        if (rawData != null)
          this._DB_WishWell.BuildDB(rawData);
      }
      return this._DB_WishWell;
    }
    set
    {
      this._DB_WishWell = value;
    }
  }

  public ConfigWishWellTimes DB_WishWellTimes
  {
    get
    {
      if (this._DB_WishWellTimes == null)
      {
        this._DB_WishWellTimes = new ConfigWishWellTimes();
        object rawData = this.GetRawData("wish_well_times.json");
        if (rawData != null)
          this._DB_WishWellTimes.BuildDB(rawData);
      }
      return this._DB_WishWellTimes;
    }
    set
    {
      this._DB_WishWellTimes = value;
    }
  }

  public ConfigGveBoss DB_GveBoss
  {
    get
    {
      if (this._DB_GveBoss == null)
      {
        this._DB_GveBoss = new ConfigGveBoss();
        object rawData = this.GetRawData("gve_boss.json");
        if (rawData != null)
          this._DB_GveBoss.BuildDB(rawData);
      }
      return this._DB_GveBoss;
    }
    set
    {
      this._DB_GveBoss = value;
    }
  }

  public ConfigBattleWounded DB_BattleWounded
  {
    get
    {
      if (this._DB_BattleWounded == null)
      {
        this._DB_BattleWounded = new ConfigBattleWounded();
        object rawData = this.GetRawData("battle_wounded.json");
        if (rawData != null)
          this._DB_BattleWounded.BuildDB(rawData);
      }
      return this._DB_BattleWounded;
    }
    set
    {
      this._DB_BattleWounded = value;
    }
  }

  public ConfigHelp DB_Help
  {
    get
    {
      if (this._DB_Help == null)
      {
        this._DB_Help = new ConfigHelp();
        object rawData = this.GetRawData("help_inside.json");
        if (rawData != null)
          this._DB_Help.BuildDB(rawData);
      }
      return this._DB_Help;
    }
    set
    {
      this._DB_Help = value;
    }
  }

  public ConfigPrivilege DB_Privilege
  {
    get
    {
      if (this._DB_Privilege == null)
      {
        this._DB_Privilege = new ConfigPrivilege();
        object rawData = this.GetRawData("vip_right.json");
        if (rawData != null)
          this._DB_Privilege.BuildDB(rawData);
      }
      return this._DB_Privilege;
    }
  }

  public ConfigNotification DB_Notifications
  {
    get
    {
      if (this._DB_Notifications == null)
      {
        this._DB_Notifications = new ConfigNotification();
        object rawData = this.GetRawData("notification.json");
        if (rawData != null)
          this._DB_Notifications.BuildDB(rawData);
      }
      return this._DB_Notifications;
    }
    set
    {
      this._DB_Notifications = value;
    }
  }

  public ConfigActivityMain DB_ActivityMain
  {
    get
    {
      if (this._DB_ActivityMain == null)
      {
        this._DB_ActivityMain = new ConfigActivityMain();
        object rawData = this.GetRawData("activity_main.json");
        if (rawData != null)
          this._DB_ActivityMain.BuildDB(rawData);
      }
      return this._DB_ActivityMain;
    }
    set
    {
      this._DB_ActivityMain = value;
    }
  }

  public ConfigActivityMainSub DB_ActivityMainSub
  {
    get
    {
      if (this._DB_ActivityMainSub == null)
      {
        this._DB_ActivityMainSub = new ConfigActivityMainSub();
        object rawData = this.GetRawData("activity_score_main.json");
        if (rawData != null)
          this._DB_ActivityMainSub.BuildDB(rawData);
      }
      return this._DB_ActivityMainSub;
    }
    set
    {
      this._DB_ActivityMainSub = value;
    }
  }

  public ConfigPersonalActivityRankReward DB_PersonalActivtyRankReward
  {
    get
    {
      if (this._DB_PersonalActivtyRankReward == null)
      {
        this._DB_PersonalActivtyRankReward = new ConfigPersonalActivityRankReward();
        object rawData = this.GetRawData("activity_single_level_rank.json");
        if (rawData != null)
          this._DB_PersonalActivtyRankReward.BuildDB(rawData);
      }
      return this._DB_PersonalActivtyRankReward;
    }
  }

  public ConfigPersonalActivity DB_PersonalActivtyReward
  {
    get
    {
      if (this._DB_PersonalActivtyReward == null)
      {
        this._DB_PersonalActivtyReward = new ConfigPersonalActivity();
        object rawData = this.GetRawData("activity_single_level_main.json");
        if (rawData != null)
          this._DB_PersonalActivtyReward.BuildDB(rawData);
      }
      return this._DB_PersonalActivtyReward;
    }
  }

  public ConfigWorldFlag DB_WorldFlag
  {
    get
    {
      if (this._DB_WorldFlag == null)
      {
        this._DB_WorldFlag = new ConfigWorldFlag();
        object rawData = this.GetRawData("world_flag.json");
        if (rawData != null)
          this._DB_WorldFlag.BuildDB(rawData);
      }
      return this._DB_WorldFlag;
    }
  }

  public ConfigActivityReward DB_ActivityReward
  {
    get
    {
      if (this._DB_ActivityReward == null)
      {
        this._DB_ActivityReward = new ConfigActivityReward();
        object rawData = this.GetRawData("activity_step.json");
        if (rawData != null)
          this._DB_ActivityReward.BuildDB(rawData);
      }
      return this._DB_ActivityReward;
    }
    set
    {
      this._DB_ActivityReward = value;
    }
  }

  public ConfigActivityRequirement DB_ActivityRequirement
  {
    get
    {
      if (this._DB_ActivityRequirement == null)
      {
        this._DB_ActivityRequirement = new ConfigActivityRequirement();
        object rawData = this.GetRawData("activity_score_req.json");
        if (rawData != null)
          this._DB_ActivityRequirement.BuildDB(rawData);
      }
      return this._DB_ActivityRequirement;
    }
    set
    {
      this._DB_ActivityRequirement = value;
    }
  }

  public ConfigActivityRankReward DB_ActivityRankReward
  {
    get
    {
      if (this._DB_ActivityRankReward == null)
      {
        this._DB_ActivityRankReward = new ConfigActivityRankReward();
        object rawData = this.GetRawData("activity_rank_reward.json");
        if (rawData != null)
          this._DB_ActivityRankReward.BuildDB(rawData);
      }
      return this._DB_ActivityRankReward;
    }
    set
    {
      this._DB_ActivityRankReward = value;
    }
  }

  public ConfigAllianceActivityMainSub DB_AllianceActivityMainSub
  {
    get
    {
      if (this._DB_AllianceActivityMainSub == null)
      {
        this._DB_AllianceActivityMainSub = new ConfigAllianceActivityMainSub();
        object rawData = this.GetRawData("alliance_activity_score_main.json");
        if (rawData != null)
          this._DB_AllianceActivityMainSub.BuildDB(rawData);
      }
      return this._DB_AllianceActivityMainSub;
    }
    set
    {
      this._DB_AllianceActivityMainSub = value;
    }
  }

  public ConfigAllianceActivityReward DB_AllianceActivityReward
  {
    get
    {
      if (this._DB_AllianceActivityReward == null)
      {
        this._DB_AllianceActivityReward = new ConfigAllianceActivityReward();
        object rawData = this.GetRawData("alliance_activity_step.json");
        if (rawData != null)
          this._DB_AllianceActivityReward.BuildDB(rawData);
      }
      return this._DB_AllianceActivityReward;
    }
    set
    {
      this._DB_AllianceActivityReward = value;
    }
  }

  public ConfigAllianceActivityRequirement DB_AllianceActivityRequirement
  {
    get
    {
      if (this._DB_AllianceActivityRequirement == null)
      {
        this._DB_AllianceActivityRequirement = new ConfigAllianceActivityRequirement();
        object rawData = this.GetRawData("alliance_activity_score_req.json");
        if (rawData != null)
          this._DB_AllianceActivityRequirement.BuildDB(rawData);
      }
      return this._DB_AllianceActivityRequirement;
    }
    set
    {
      this._DB_AllianceActivityRequirement = value;
    }
  }

  public ConfigAllianceActivityRankReward DB_AllianceActivityRankReward
  {
    get
    {
      if (this._DB_AllianceActivityRankReward == null)
      {
        this._DB_AllianceActivityRankReward = new ConfigAllianceActivityRankReward();
        object rawData = this.GetRawData("alliance_activity_rank_reward.json");
        if (rawData != null)
          this._DB_AllianceActivityRankReward.BuildDB(rawData);
      }
      return this._DB_AllianceActivityRankReward;
    }
    set
    {
      this._DB_AllianceActivityRankReward = value;
    }
  }

  public ConfigFallenKnightScoreReq DB_FallenKnightScoreReq
  {
    get
    {
      if (this._DB_FallenKnightScoreReq == null)
      {
        this._DB_FallenKnightScoreReq = new ConfigFallenKnightScoreReq();
        object rawData = this.GetRawData("event_fallen_knight_score_req.json");
        if (rawData != null)
          this._DB_FallenKnightScoreReq.BuildDB(rawData);
      }
      return this._DB_FallenKnightScoreReq;
    }
    set
    {
      this._DB_FallenKnightScoreReq = value;
    }
  }

  public ConfigFallenKnightDifficult DB_FallenKnightDifficult
  {
    get
    {
      if (this._DB_FallenKnightDifficult == null)
      {
        this._DB_FallenKnightDifficult = new ConfigFallenKnightDifficult();
        object rawData = this.GetRawData("event_fallen_knight_difficult.json");
        if (rawData != null)
          this._DB_FallenKnightDifficult.BuildDB(rawData);
      }
      return this._DB_FallenKnightDifficult;
    }
    set
    {
      this._DB_FallenKnightDifficult = value;
    }
  }

  public ConfigFallenKnightMain DB_FallenKnightMain
  {
    get
    {
      if (this._DB_FallenKnightMain == null)
      {
        this._DB_FallenKnightMain = new ConfigFallenKnightMain();
        object rawData = this.GetRawData("event_fallen_knight_main.json");
        if (rawData != null)
          this._DB_FallenKnightMain.BuildDB(rawData);
      }
      return this._DB_FallenKnightMain;
    }
    set
    {
      this._DB_FallenKnightMain = value;
    }
  }

  public ConfigFallenKnightRankReward DB_FallenKnightRankReward
  {
    get
    {
      if (this._DB_FallenKnightRankReward == null)
      {
        this._DB_FallenKnightRankReward = new ConfigFallenKnightRankReward();
        object rawData = this.GetRawData("event_fallen_knight_rank_reward.json");
        if (rawData != null)
          this._DB_FallenKnightRankReward.BuildDB(rawData);
      }
      return this._DB_FallenKnightRankReward;
    }
    set
    {
      this._DB_FallenKnightRankReward = value;
    }
  }

  public ConfigCasinoRoulette DB_CasinoRoulette
  {
    get
    {
      if (this._DB_CasinoRoulette == null)
      {
        this._DB_CasinoRoulette = new ConfigCasinoRoulette();
        object rawData = this.GetRawData("casino_roulette.json");
        if (rawData != null)
          this._DB_CasinoRoulette.BuildDB(rawData);
      }
      return this._DB_CasinoRoulette;
    }
    set
    {
      this._DB_CasinoRoulette = value;
    }
  }

  public ConfigCasinoChest DB_CasinoChest
  {
    get
    {
      if (this._DB_CasinoChest == null)
      {
        this._DB_CasinoChest = new ConfigCasinoChest();
        object rawData = this.GetRawData("casino_chest.json");
        if (rawData != null)
          this._DB_CasinoChest.BuildDB(rawData);
      }
      return this._DB_CasinoChest;
    }
    set
    {
      this._DB_CasinoChest = value;
    }
  }

  public ConfigCasinoJackpot DB_CasinoJackpot
  {
    get
    {
      if (this._DB_CasinoJackpot == null)
      {
        this._DB_CasinoJackpot = new ConfigCasinoJackpot();
        object rawData = this.GetRawData("jackpot.json");
        if (rawData != null)
          this._DB_CasinoJackpot.BuildDB(rawData);
      }
      return this._DB_CasinoJackpot;
    }
    set
    {
      this._DB_CasinoJackpot = value;
    }
  }

  public ConfigAllianceBuilding DB_AllianceBuildings
  {
    get
    {
      if (this._DB_AllianceBuildings == null)
      {
        this._DB_AllianceBuildings = new ConfigAllianceBuilding();
        object rawData = this.GetRawData("alliance_buildings.json");
        if (rawData != null)
          this._DB_AllianceBuildings.BuildDB(rawData);
      }
      return this._DB_AllianceBuildings;
    }
    set
    {
      this._DB_AllianceBuildings = value;
    }
  }

  public ConfigAllianceBoss DB_AllianceBoss
  {
    get
    {
      if (this._DB_AllianceBoss == null)
      {
        this._DB_AllianceBoss = new ConfigAllianceBoss();
        object rawData = this.GetRawData("alliance_boss.json");
        if (rawData != null)
          this._DB_AllianceBoss.BuildDB(rawData);
      }
      return this._DB_AllianceBoss;
    }
    set
    {
      this._DB_AllianceBoss = value;
    }
  }

  public ConfigAllianceBossRankReward DB_AllianceBossRankReward
  {
    get
    {
      if (this._DB_AllianceBossRankReward == null)
      {
        this._DB_AllianceBossRankReward = new ConfigAllianceBossRankReward();
        object rawData = this.GetRawData("alliance_boss_rank_reward.json");
        if (rawData != null)
          this._DB_AllianceBossRankReward.BuildDB(rawData);
      }
      return this._DB_AllianceBossRankReward;
    }
    set
    {
      this._DB_AllianceBossRankReward = value;
    }
  }

  public ConfigTreasury DB_Treasury
  {
    get
    {
      if (this._DB_Treasury == null)
      {
        this._DB_Treasury = new ConfigTreasury();
        object rawData = this.GetRawData("treasury.json");
        if (rawData != null)
          this._DB_Treasury.BuildDB(rawData);
      }
      return this._DB_Treasury;
    }
    set
    {
      this._DB_Treasury = value;
    }
  }

  public ConfigTreasureMap DB_TreasureMap
  {
    get
    {
      if (this._DB_TreasureMap == null)
      {
        this._DB_TreasureMap = new ConfigTreasureMap();
        object rawData = this.GetRawData("treasure_map.json");
        if (rawData != null)
          this._DB_TreasureMap.BuildDB(rawData);
      }
      return this._DB_TreasureMap;
    }
  }

  public ConfigKingdomBuff DB_KingdomBuff
  {
    get
    {
      if (this._DB_KingdomBuff == null)
      {
        this._DB_KingdomBuff = new ConfigKingdomBuff();
        object rawData = this.GetRawData("kingdom_buff.json");
        if (rawData != null)
          this._DB_KingdomBuff.BuildDB(rawData);
      }
      return this._DB_KingdomBuff;
    }
    set
    {
      this._DB_KingdomBuff = value;
    }
  }

  public ConfigDiyLotteryMain DB_DiyLotteryMain
  {
    get
    {
      if (this._DB_DiyLotteryMain == null)
      {
        this._DB_DiyLotteryMain = new ConfigDiyLotteryMain();
        object rawData = this.GetRawData("diy_lottery_main.json");
        if (rawData != null)
          this._DB_DiyLotteryMain.BuildDB(rawData);
      }
      return this._DB_DiyLotteryMain;
    }
    set
    {
      this._DB_DiyLotteryMain = value;
    }
  }

  public ConfigDiyLotteryGroup DB_DiyLotteryGroup
  {
    get
    {
      if (this._DB_DiyLotteryGroup == null)
      {
        this._DB_DiyLotteryGroup = new ConfigDiyLotteryGroup();
        object rawData = this.GetRawData("diy_lottery_group.json");
        if (rawData != null)
          this._DB_DiyLotteryGroup.BuildDB(rawData);
      }
      return this._DB_DiyLotteryGroup;
    }
    set
    {
      this._DB_DiyLotteryGroup = value;
    }
  }

  public ConfigLuckyArcher DB_LuckyArcher
  {
    get
    {
      if (this._DB_LuckyArcher == null)
      {
        this._DB_LuckyArcher = new ConfigLuckyArcher();
        object rawData = this.GetRawData("lucky_archer_config.json");
        if (rawData != null)
          this._DB_LuckyArcher.BuildDB(rawData);
      }
      return this._DB_LuckyArcher;
    }
    set
    {
      this._DB_LuckyArcher = value;
    }
  }

  public ConfigLuckyArcherGroup DB_LuckyArcherGroup
  {
    get
    {
      if (this._DB_LuckyArcherGroup == null)
      {
        this._DB_LuckyArcherGroup = new ConfigLuckyArcherGroup();
        object rawData = this.GetRawData("lucky_archer_group.json");
        if (rawData != null)
          this._DB_LuckyArcherGroup.BuildDB(rawData);
      }
      return this._DB_LuckyArcherGroup;
    }
    set
    {
      this._DB_LuckyArcherGroup = value;
    }
  }

  public ConfigLuckyArcherRewards DB_LuckyArcherRewards
  {
    get
    {
      if (this._DB_LuckyArcherRewards == null)
      {
        this._DB_LuckyArcherRewards = new ConfigLuckyArcherRewards();
        object rawData = this.GetRawData("lucky_archer_rewards.json");
        if (rawData != null)
          this._DB_LuckyArcherRewards.BuildDB(rawData);
      }
      return this._DB_LuckyArcherRewards;
    }
    set
    {
      this._DB_LuckyArcherRewards = value;
    }
  }

  public ConfigLuckyArcherStepCost DB_LuckyArcherStepCost
  {
    get
    {
      if (this._DB_LuckyArcherStepCost == null)
      {
        this._DB_LuckyArcherStepCost = new ConfigLuckyArcherStepCost();
        object rawData = this.GetRawData("lucky_archer_step_cost.json");
        if (rawData != null)
          this._DB_LuckyArcherStepCost.BuildDB(rawData);
      }
      return this._DB_LuckyArcherStepCost;
    }
    set
    {
      this._DB_LuckyArcherStepCost = value;
    }
  }

  public ConfigDiceGroup DB_DiceGroup
  {
    get
    {
      if (this._DB_DiceGroup == null)
      {
        this._DB_DiceGroup = new ConfigDiceGroup();
        object rawData = this.GetRawData("dice_group.json");
        if (rawData != null)
          this._DB_DiceGroup.BuildDB(rawData);
      }
      return this._DB_DiceGroup;
    }
    set
    {
      this._DB_DiceGroup = value;
    }
  }

  public ConfigDiceMain DB_DiceMain
  {
    get
    {
      if (this._DB_DiceMain == null)
      {
        this._DB_DiceMain = new ConfigDiceMain();
        object rawData = this.GetRawData("dice_main.json");
        if (rawData != null)
          this._DB_DiceMain.BuildDB(rawData);
      }
      return this._DB_DiceMain;
    }
    set
    {
      this._DB_DiceMain = value;
    }
  }

  public ConfigDiceScoreBox DB_DiceScoreBox
  {
    get
    {
      if (this._DB_DiceScoreBox == null)
      {
        this._DB_DiceScoreBox = new ConfigDiceScoreBox();
        object rawData = this.GetRawData("dice_scorebox.json");
        if (rawData != null)
          this._DB_DiceScoreBox.BuildDB(rawData);
      }
      return this._DB_DiceScoreBox;
    }
    set
    {
      this._DB_DiceScoreBox = value;
    }
  }

  public ConfigExchangeHall DB_ExchangeHall
  {
    get
    {
      if (this._DB_ExchangeHall == null)
      {
        this._DB_ExchangeHall = new ConfigExchangeHall();
        object rawData = this.GetRawData("exchange_hall.json");
        if (rawData != null)
          this._DB_ExchangeHall.BuildDB(rawData);
      }
      return this._DB_ExchangeHall;
    }
    set
    {
      this._DB_ExchangeHall = value;
    }
  }

  public ConfigExchangeHallSort1 DB_ExchangeHallSort1
  {
    get
    {
      if (this._DB_ExchangeHallSort1 == null)
      {
        this._DB_ExchangeHallSort1 = new ConfigExchangeHallSort1();
        object rawData = this.GetRawData("exchange_hall_sort_1.json");
        if (rawData != null)
          this._DB_ExchangeHallSort1.BuildDB(rawData);
      }
      return this._DB_ExchangeHallSort1;
    }
    set
    {
      this._DB_ExchangeHallSort1 = value;
    }
  }

  public ConfigExchangeHallSort2 DB_ExchangeHallSort2
  {
    get
    {
      if (this._DB_ExchangeHallSort2 == null)
      {
        this._DB_ExchangeHallSort2 = new ConfigExchangeHallSort2();
        object rawData = this.GetRawData("exchange_hall_sort_2.json");
        if (rawData != null)
          this._DB_ExchangeHallSort2.BuildDB(rawData);
      }
      return this._DB_ExchangeHallSort2;
    }
    set
    {
      this._DB_ExchangeHallSort2 = value;
    }
  }

  public ConfigExchangeHallSort3 DB_ExchangeHallSort3
  {
    get
    {
      if (this._DB_ExchangeHallSort3 == null)
      {
        this._DB_ExchangeHallSort3 = new ConfigExchangeHallSort3();
        object rawData = this.GetRawData("exchange_hall_sort_3.json");
        if (rawData != null)
          this._DB_ExchangeHallSort3.BuildDB(rawData);
      }
      return this._DB_ExchangeHallSort3;
    }
    set
    {
      this._DB_ExchangeHallSort3 = value;
    }
  }

  public ConfigAnniversaryIapMain DB_AnniversaryIapMain
  {
    get
    {
      if (this._DB_AnniversaryIapMain == null)
      {
        this._DB_AnniversaryIapMain = new ConfigAnniversaryIapMain();
        object rawData = this.GetRawData("anniversary_iap_main.json");
        if (rawData != null)
          this._DB_AnniversaryIapMain.BuildDB(rawData);
      }
      return this._DB_AnniversaryIapMain;
    }
    set
    {
      this._DB_AnniversaryIapMain = value;
    }
  }

  public ConfigAnniversaryCommunityeReward DB_AnniversaryCommunityReward
  {
    get
    {
      if (this._DB_AnniversaryCommunityReward == null)
      {
        this._DB_AnniversaryCommunityReward = new ConfigAnniversaryCommunityeReward();
        object rawData = this.GetRawData("anniversary_community_reward.json");
        if (rawData != null)
          this._DB_AnniversaryCommunityReward.BuildDB(rawData);
      }
      return this._DB_AnniversaryCommunityReward;
    }
    set
    {
      this._DB_AnniversaryCommunityReward = value;
    }
  }

  public ConfigAnniversaryStrongHoldDisplay DB_AnniversaryStrongHoldDisplay
  {
    get
    {
      if (this._DB_AnniversaryStrongHoldDisplay == null)
      {
        this._DB_AnniversaryStrongHoldDisplay = new ConfigAnniversaryStrongHoldDisplay();
        object rawData = this.GetRawData("anniversary_stronghold_display.json");
        if (rawData != null)
          this._DB_AnniversaryStrongHoldDisplay.BuildDB(rawData);
      }
      return this._DB_AnniversaryStrongHoldDisplay;
    }
  }

  public ConfigBuildingGloryGroup DB_BuildingGloryGroup
  {
    get
    {
      if (this._DB_BuildingGloryGroup == null)
      {
        this._DB_BuildingGloryGroup = new ConfigBuildingGloryGroup();
        object rawData = this.GetRawData("building_glory_group.json");
        if (rawData != null)
          this._DB_BuildingGloryGroup.BuildDB(rawData);
      }
      return this._DB_BuildingGloryGroup;
    }
    set
    {
      this._DB_BuildingGloryGroup = value;
    }
  }

  public ConfigBuildingGloryMain DB_BuildingGloryMain
  {
    get
    {
      if (this._DB_BuildingGloryMain == null)
      {
        this._DB_BuildingGloryMain = new ConfigBuildingGloryMain();
        object rawData = this.GetRawData("building_glory_main.json");
        if (rawData != null)
          this._DB_BuildingGloryMain.BuildDB(rawData);
      }
      return this._DB_BuildingGloryMain;
    }
    set
    {
      this._DB_BuildingGloryMain = value;
    }
  }

  public ConfigParliament DB_Parliament
  {
    get
    {
      if (this._DB_Parliament == null)
      {
        this._DB_Parliament = new ConfigParliament();
        object rawData = this.GetRawData("parliament.json");
        if (rawData != null)
          this._DB_Parliament.BuildDB(rawData);
      }
      return this._DB_Parliament;
    }
    set
    {
      this._DB_Parliament = value;
    }
  }

  public ConfigParliamentHero DB_ParliamentHero
  {
    get
    {
      if (this._DB_ParliamentHero == null)
      {
        this._DB_ParliamentHero = new ConfigParliamentHero();
        object rawData = this.GetRawData("parliament_hero.json");
        if (rawData != null)
          this._DB_ParliamentHero.BuildDB(rawData);
      }
      return this._DB_ParliamentHero;
    }
    set
    {
      this._DB_ParliamentHero = value;
    }
  }

  public ConfigParliamentHeroCard DB_ParliamentHeroCard
  {
    get
    {
      if (this._DB_ParliamentHeroCard == null)
      {
        this._DB_ParliamentHeroCard = new ConfigParliamentHeroCard();
        object rawData = this.GetRawData("parliament_hero_card.json");
        if (rawData != null)
          this._DB_ParliamentHeroCard.BuildDB(rawData);
      }
      return this._DB_ParliamentHeroCard;
    }
    set
    {
      this._DB_ParliamentHeroCard = value;
    }
  }

  public ConfigParliamentHeroLevel DB_ParliamentHeroLevel
  {
    get
    {
      if (this._DB_ParliamentHeroLevel == null)
      {
        this._DB_ParliamentHeroLevel = new ConfigParliamentHeroLevel();
        object rawData = this.GetRawData("parliament_hero_level.json");
        if (rawData != null)
          this._DB_ParliamentHeroLevel.BuildDB(rawData);
      }
      return this._DB_ParliamentHeroLevel;
    }
    set
    {
      this._DB_ParliamentHeroLevel = value;
    }
  }

  public ConfigParliamentHeroQuality DB_ParliamentHeroQuality
  {
    get
    {
      if (this._DB_ParliamentHeroQuality == null)
      {
        this._DB_ParliamentHeroQuality = new ConfigParliamentHeroQuality();
        object rawData = this.GetRawData("parliament_hero_quality.json");
        if (rawData != null)
          this._DB_ParliamentHeroQuality.BuildDB(rawData);
      }
      return this._DB_ParliamentHeroQuality;
    }
    set
    {
      this._DB_ParliamentHeroQuality = value;
    }
  }

  public ConfigParliamentHeroStar DB_ParliamentHeroStar
  {
    get
    {
      if (this._DB_ParliamentHeroStar == null)
      {
        this._DB_ParliamentHeroStar = new ConfigParliamentHeroStar();
        object rawData = this.GetRawData("parliament_hero_star.json");
        if (rawData != null)
          this._DB_ParliamentHeroStar.BuildDB(rawData);
      }
      return this._DB_ParliamentHeroStar;
    }
    set
    {
      this._DB_ParliamentHeroStar = value;
    }
  }

  public ConfigParliamentHeroSummon DB_ParliamentHeroSummon
  {
    get
    {
      if (this._DB_ParliamentHeroSummon == null)
      {
        this._DB_ParliamentHeroSummon = new ConfigParliamentHeroSummon();
        object rawData = this.GetRawData("parliament_hero_summon.json");
        if (rawData != null)
          this._DB_ParliamentHeroSummon.BuildDB(rawData);
      }
      return this._DB_ParliamentHeroSummon;
    }
    set
    {
      this._DB_ParliamentHeroSummon = value;
    }
  }

  public ConfigParliamentHeroSummonGroup DB_ParliamentHeroSummonGroup
  {
    get
    {
      if (this._DB_ParliamentHeroSummonGroup == null)
      {
        this._DB_ParliamentHeroSummonGroup = new ConfigParliamentHeroSummonGroup();
        object rawData = this.GetRawData("parliament_hero_summon_group.json");
        if (rawData != null)
          this._DB_ParliamentHeroSummonGroup.BuildDB(rawData);
      }
      return this._DB_ParliamentHeroSummonGroup;
    }
    set
    {
      this._DB_ParliamentHeroSummonGroup = value;
    }
  }

  public ConfigParliamentSuitGroup DB_ParliamentSuitGroup
  {
    get
    {
      if (this._DB_ParliamentSuitGroup == null)
      {
        this._DB_ParliamentSuitGroup = new ConfigParliamentSuitGroup();
        object rawData = this.GetRawData("parliament_suit_group.json");
        if (rawData != null)
          this._DB_ParliamentSuitGroup.BuildDB(rawData);
      }
      return this._DB_ParliamentSuitGroup;
    }
    set
    {
      this._DB_ParliamentSuitGroup = value;
    }
  }

  public ConfigWonderTitle DB_WonderTitle
  {
    get
    {
      if (this._DB_WonderTitle == null)
      {
        this._DB_WonderTitle = new ConfigWonderTitle();
        object rawData = this.GetRawData("wonder_position.json");
        if (rawData != null)
          this._DB_WonderTitle.BuildDB(rawData);
      }
      return this._DB_WonderTitle;
    }
    set
    {
      this._DB_WonderTitle = value;
    }
  }

  public ConfigArtifact DB_Artifact
  {
    get
    {
      if (this._DB_Artifact == null)
      {
        this._DB_Artifact = new ConfigArtifact();
        object rawData = this.GetRawData("artifact.json");
        if (rawData != null)
          this._DB_Artifact.BuildDB(rawData);
      }
      return this._DB_Artifact;
    }
    set
    {
      this._DB_Artifact = value;
    }
  }

  public ConfigArtifactShop DB_ArtifactShop
  {
    get
    {
      if (this._DB_ArtifactShop == null)
      {
        this._DB_ArtifactShop = new ConfigArtifactShop();
        this._DB_ArtifactShop.BuildDB(Utils.Json2Object(File.ReadAllText(NetApi.inst.BuildLocalPath("artifact_shop.json")), true));
      }
      return this._DB_ArtifactShop;
    }
    set
    {
      this._DB_ArtifactShop = value;
    }
  }

  public ConfigSuperLoginMain DB_SuperLoginMain
  {
    get
    {
      if (this._DB_SuperLoginMain == null)
      {
        this._DB_SuperLoginMain = new ConfigSuperLoginMain();
        object rawData = this.GetRawData("super_log_in_main.json");
        if (rawData != null)
          this._DB_SuperLoginMain.BuildDB(rawData);
      }
      return this._DB_SuperLoginMain;
    }
    set
    {
      this._DB_SuperLoginMain = value;
    }
  }

  public ConfigSuperLoginGroup DB_SuperLoginGroup
  {
    get
    {
      if (this._DB_SuperLoginGroup == null)
      {
        this._DB_SuperLoginGroup = new ConfigSuperLoginGroup();
        object rawData = this.GetRawData("super_log_in_group.json");
        if (rawData != null)
          this._DB_SuperLoginGroup.BuildDB(rawData);
      }
      return this._DB_SuperLoginGroup;
    }
    set
    {
      this._DB_SuperLoginGroup = value;
    }
  }

  public ConfigPlayerProfile DB_PlayerProfile
  {
    get
    {
      if (this._DB_PlayerProfile == null)
      {
        this._DB_PlayerProfile = new ConfigPlayerProfile();
        object rawData = this.GetRawData("player_profile_info.json");
        if (rawData != null)
          this._DB_PlayerProfile.BuildDB(rawData);
      }
      return this._DB_PlayerProfile;
    }
    set
    {
      this._DB_PlayerProfile = value;
    }
  }

  public ConfigDragonKnightProfile DB_DragonKnightProfile
  {
    get
    {
      if (this._DB_DragonKnightProfile == null)
      {
        this._DB_DragonKnightProfile = new ConfigDragonKnightProfile();
        object rawData = this.GetRawData("dragon_knight_profile_info.json");
        if (rawData != null)
          this._DB_DragonKnightProfile.BuildDB(rawData);
      }
      return this._DB_DragonKnightProfile;
    }
  }

  public ConfigQuestGroupMain DB_QuestGroupMain
  {
    get
    {
      if (this._DB_QuestGroupMain == null)
      {
        this._DB_QuestGroupMain = new ConfigQuestGroupMain();
        object rawData = this.GetRawData("quest_group_main.json");
        if (rawData != null)
          this._DB_QuestGroupMain.BuildDB(rawData);
      }
      return this._DB_QuestGroupMain;
    }
    set
    {
      this._DB_QuestGroupMain = value;
    }
  }

  public ConfigQuestGroup DB_QuestGroup
  {
    get
    {
      if (this._DB_QuestGroup == null)
      {
        this._DB_QuestGroup = new ConfigQuestGroup();
        object rawData = this.GetRawData("quest_group.json");
        if (rawData != null)
          this._DB_QuestGroup.BuildDB(rawData);
      }
      return this._DB_QuestGroup;
    }
    set
    {
      this._DB_QuestGroup = value;
    }
  }

  public ConfigSimulatorForbidden DB_SimulatorForbidden
  {
    get
    {
      if (this._DB_SimulatorForbidden == null)
      {
        this._DB_SimulatorForbidden = new ConfigSimulatorForbidden();
        object rawData = this.GetRawData("simulator_forbidden.json");
        if (rawData != null)
          this._DB_SimulatorForbidden.BuildDB(rawData);
      }
      return this._DB_SimulatorForbidden;
    }
  }

  public ConfigEquipmentGem DB_EquipmentGem
  {
    get
    {
      if (this._DB_equipmentGem == null)
      {
        this._DB_equipmentGem = new ConfigEquipmentGem();
        object rawData = this.GetRawData("equipment_gem.json");
        if (rawData != null)
          this._DB_equipmentGem.BuildDB(rawData);
      }
      return this._DB_equipmentGem;
    }
  }

  public ConfigEquipmentGemHandbook DB_GemHandbook
  {
    get
    {
      if (this._DB_gemHandbook == null)
      {
        this._DB_gemHandbook = new ConfigEquipmentGemHandbook();
        object rawData = this.GetRawData("equipment_gem_handbook.json");
        if (rawData != null)
          this._DB_gemHandbook.BuildDB(rawData);
      }
      return this._DB_gemHandbook;
    }
  }

  public ConfigEquipmentGemHandbookSort DB_GemHandbookSort
  {
    get
    {
      if (this._DB_gemHandbookSort == null)
      {
        this._DB_gemHandbookSort = new ConfigEquipmentGemHandbookSort();
        object rawData = this.GetRawData("equipment_gem_handbook_sort.json");
        if (rawData != null)
          this._DB_gemHandbookSort.BuildDB(rawData);
      }
      return this._DB_gemHandbookSort;
    }
  }

  public ConfigTavernWheelMain DB_TavernWheelMain
  {
    get
    {
      if (this._DB_tavernWheelMain == null)
      {
        this._DB_tavernWheelMain = new ConfigTavernWheelMain();
        object rawData = this.GetRawData("tavern_wheel_main.json");
        if (rawData != null)
          this._DB_tavernWheelMain.BuildDB(rawData);
      }
      return this._DB_tavernWheelMain;
    }
  }

  public ConfigTavernWheelGroup DB_TavernWheelGroup
  {
    get
    {
      if (this._DB_tavernWheelGroup == null)
      {
        this._DB_tavernWheelGroup = new ConfigTavernWheelGroup();
        object rawData = this.GetRawData("tavern_wheel_group.json");
        if (rawData != null)
          this._DB_tavernWheelGroup.BuildDB(rawData);
      }
      return this._DB_tavernWheelGroup;
    }
  }

  public ConfigTavernWheelGrade DB_TavernWheelGrade
  {
    get
    {
      if (this._DB_tavernWheelGrade == null)
      {
        this._DB_tavernWheelGrade = new ConfigTavernWheelGrade();
        object rawData = this.GetRawData("tavern_wheel_grade.json");
        if (rawData != null)
          this._DB_tavernWheelGrade.BuildDB(rawData);
      }
      return this._DB_tavernWheelGrade;
    }
  }

  public ConfigEquipmentMain GetEquipmentMain(BagType type)
  {
    switch (type)
    {
      case BagType.Hero:
        if (this._heroEquipmentMain == null)
        {
          this._heroEquipmentMain = new ConfigEquipmentMain();
          object rawData = this.GetRawData("equipment_main.json");
          if (rawData != null)
            this._heroEquipmentMain.BuildDB(rawData);
        }
        return this._heroEquipmentMain;
      case BagType.DragonKnight:
        if (this._dragonKnightEquipmentMain == null)
        {
          this._dragonKnightEquipmentMain = new ConfigEquipmentMain();
          object rawData = this.GetRawData("dk_equipment_main.json");
          if (rawData != null)
            this._dragonKnightEquipmentMain.BuildDB(rawData);
        }
        return this._dragonKnightEquipmentMain;
      default:
        return (ConfigEquipmentMain) null;
    }
  }

  public ConfigEquipmentProperty GetEquipmentProperty(BagType type)
  {
    switch (type)
    {
      case BagType.Hero:
        if (this._heroEquipmentProperty == null)
        {
          this._heroEquipmentProperty = new ConfigEquipmentProperty();
          object rawData = this.GetRawData("equipment_property.json");
          if (rawData != null)
            this._heroEquipmentProperty.BuildDB(rawData);
        }
        return this._heroEquipmentProperty;
      case BagType.DragonKnight:
        if (this._dragonKnightEquipmentProperty == null)
        {
          this._dragonKnightEquipmentProperty = new ConfigEquipmentProperty();
          object rawData = this.GetRawData("dk_equipment_property.json");
          if (rawData != null)
            this._dragonKnightEquipmentProperty.BuildDB(rawData);
        }
        return this._dragonKnightEquipmentProperty;
      default:
        return (ConfigEquipmentProperty) null;
    }
  }

  public ConfigEquipmentScroll GetEquipmentScroll(BagType type)
  {
    switch (type)
    {
      case BagType.Hero:
        if (this._heroEquipmentScroll == null)
        {
          this._heroEquipmentScroll = new ConfigEquipmentScroll();
          object rawData = this.GetRawData("equipment_scroll.json");
          if (rawData != null)
            this._heroEquipmentScroll.BuildDB(rawData);
        }
        return this._heroEquipmentScroll;
      case BagType.DragonKnight:
        if (this._dragonKnightEquipmentScroll == null)
        {
          this._dragonKnightEquipmentScroll = new ConfigEquipmentScroll();
          object rawData = this.GetRawData("dk_equipment_scroll.json");
          if (rawData != null)
            this._dragonKnightEquipmentScroll.BuildDB(rawData);
        }
        return this._dragonKnightEquipmentScroll;
      default:
        return (ConfigEquipmentScroll) null;
    }
  }

  public ConfigEquipmentScrollChip GetEquipmentScrollChip(BagType type)
  {
    switch (type)
    {
      case BagType.Hero:
        if (this._heroEquipmentScrollChip == null)
        {
          this._heroEquipmentScrollChip = new ConfigEquipmentScrollChip();
          object rawData = this.GetRawData("equipment_scroll_chip.json");
          if (rawData != null)
            this._heroEquipmentScrollChip.BuildDB(rawData);
        }
        return this._heroEquipmentScrollChip;
      case BagType.DragonKnight:
        if (this._dragonKnightEquipmentScrollChip == null)
        {
          this._dragonKnightEquipmentScrollChip = new ConfigEquipmentScrollChip();
          object rawData = this.GetRawData("dk_equipment_scroll_chip.json");
          if (rawData != null)
            this._dragonKnightEquipmentScrollChip.BuildDB(rawData);
        }
        return this._dragonKnightEquipmentScrollChip;
      default:
        return (ConfigEquipmentScrollChip) null;
    }
  }

  public ConfigEquipmentScrollCombine GetEquipmentScrollChipCombine(BagType type)
  {
    switch (type)
    {
      case BagType.Hero:
        if (this._heroEquipmentScrollCombine == null)
        {
          this._heroEquipmentScrollCombine = new ConfigEquipmentScrollCombine();
          object rawData = this.GetRawData("equipment_scroll_combine.json");
          if (rawData != null)
            this._heroEquipmentScrollCombine.BuildDB(rawData);
        }
        return this._heroEquipmentScrollCombine;
      case BagType.DragonKnight:
        if (this._dragonKnightEquipmentScrollCombine == null)
        {
          this._dragonKnightEquipmentScrollCombine = new ConfigEquipmentScrollCombine();
          object rawData = this.GetRawData("dk_equipment_scroll_combine.json");
          if (rawData != null)
            this._dragonKnightEquipmentScrollCombine.BuildDB(rawData);
        }
        return this._dragonKnightEquipmentScrollCombine;
      default:
        return (ConfigEquipmentScrollCombine) null;
    }
  }

  public ConfigItemCompose DB_ItemCompose
  {
    get
    {
      if (this._DB_ItemCompose == null)
      {
        this._DB_ItemCompose = new ConfigItemCompose();
        object rawData = this.GetRawData("item_compose.json");
        if (rawData != null)
          this._DB_ItemCompose.BuildDB(rawData);
      }
      return this._DB_ItemCompose;
    }
    set
    {
      this._DB_ItemCompose = value;
    }
  }

  public ConfigBuildingIdle DB_BuildingIdle
  {
    get
    {
      if (this._DB_BuildingIdle == null)
      {
        this._DB_BuildingIdle = new ConfigBuildingIdle();
        object rawData = this.GetRawData("building_idle.json");
        if (rawData != null)
          this._DB_BuildingIdle.BuildDB(rawData);
      }
      return this._DB_BuildingIdle;
    }
    set
    {
      this._DB_BuildingIdle = value;
    }
  }

  public ConfigBuildingIdleCommon DB_BuildingIdleCommon
  {
    get
    {
      if (this._DB_BuildingIdleCommon == null)
      {
        this._DB_BuildingIdleCommon = new ConfigBuildingIdleCommon();
        object rawData = this.GetRawData("building_idle_common.json");
        if (rawData != null)
          this._DB_BuildingIdleCommon.BuildDB(rawData);
      }
      return this._DB_BuildingIdleCommon;
    }
    set
    {
      this._DB_BuildingIdleCommon = value;
    }
  }

  public ConfigIAP_Package DB_IAPPackage
  {
    get
    {
      if (this._DB_IAPPackage == null)
      {
        this._DB_IAPPackage = new ConfigIAP_Package();
        object rawData = this.GetRawData("iap_package.json");
        if (rawData != null)
          this._DB_IAPPackage.BuildDB(rawData);
      }
      return this._DB_IAPPackage;
    }
    set
    {
      this._DB_IAPPackage = value;
    }
  }

  public ConfigIAP_DailyPackage DB_IAPDailyPackage
  {
    get
    {
      if (this._DB_IAPDailyPackage == null)
      {
        this._DB_IAPDailyPackage = new ConfigIAP_DailyPackage();
        object rawData = this.GetRawData("iap_daily_recharge.json");
        if (rawData != null)
          this._DB_IAPDailyPackage.BuildDB(rawData);
      }
      return this._DB_IAPDailyPackage;
    }
    set
    {
      this._DB_IAPDailyPackage = value;
    }
  }

  public ConfigIAP_DailyPackageRewards DB_IAPDailyPackageRewards
  {
    get
    {
      if (this._DB_IAPDailyPackageRewards == null)
      {
        this._DB_IAPDailyPackageRewards = new ConfigIAP_DailyPackageRewards();
        object rawData = this.GetRawData("iap_daily_recharge_rewards.json");
        if (rawData != null)
          this._DB_IAPDailyPackageRewards.BuildDB(rawData);
      }
      return this._DB_IAPDailyPackageRewards;
    }
    set
    {
      this._DB_IAPDailyPackageRewards = value;
    }
  }

  public ConfigIAP_DailyPackageSubRewards DB_IAPDailyPackageSubRewards
  {
    get
    {
      if (this._DB_IAPDailyPackageSubRewards == null)
      {
        this._DB_IAPDailyPackageSubRewards = new ConfigIAP_DailyPackageSubRewards();
        object rawData = this.GetRawData("iap_daily_recharge_sub_rewards.json");
        if (rawData != null)
          this._DB_IAPDailyPackageSubRewards.BuildDB(rawData);
      }
      return this._DB_IAPDailyPackageSubRewards;
    }
    set
    {
      this._DB_IAPDailyPackageSubRewards = value;
    }
  }

  public ConfigIAP_Platform DB_IAPPlatform
  {
    get
    {
      if (this._DB_IAPPlatform == null)
      {
        this._DB_IAPPlatform = new ConfigIAP_Platform();
        object rawData = this.GetRawData("iap_platform.json");
        if (rawData != null)
          this._DB_IAPPlatform.BuildDB(rawData);
      }
      return this._DB_IAPPlatform;
    }
    set
    {
      this._DB_IAPPlatform = value;
    }
  }

  public ConfigIAP_PackageGroup DB_IAPPackageGroup
  {
    get
    {
      if (this._IAPPackageGroup == null)
      {
        this._IAPPackageGroup = new ConfigIAP_PackageGroup();
        object rawData = this.GetRawData("iap_package_group.json");
        if (rawData != null)
          this._IAPPackageGroup.BuildDB(rawData);
      }
      return this._IAPPackageGroup;
    }
  }

  public ConfigIAP_FakePrice DB_IAPPrice
  {
    get
    {
      if (this._DB_IAPPrice == null)
      {
        this._DB_IAPPrice = new ConfigIAP_FakePrice();
        object rawData = this.GetRawData("iap_price_default.json");
        if (rawData != null)
          this._DB_IAPPrice.BuildDB(rawData);
      }
      return this._DB_IAPPrice;
    }
    set
    {
      this._DB_IAPPrice = value;
    }
  }

  public ConfigIAP_PaymentLevel DB_IAPPaymentLevel
  {
    get
    {
      if (this._DB_IAPPaymentLevel == null)
      {
        this._DB_IAPPaymentLevel = new ConfigIAP_PaymentLevel();
        object rawData = this.GetRawData("iap_payment_level.json");
        if (rawData != null)
          this._DB_IAPPaymentLevel.BuildDB(rawData);
      }
      return this._DB_IAPPaymentLevel;
    }
  }

  public ConfigIAP_Gold DB_IAPGold
  {
    get
    {
      if (this._DB_IAPGold == null)
      {
        this._DB_IAPGold = new ConfigIAP_Gold();
        object rawData = this.GetRawData("iap_gold_package.json");
        if (rawData != null)
          this._DB_IAPGold.BuildDB(rawData);
      }
      return this._DB_IAPGold;
    }
    set
    {
      this._DB_IAPGold = value;
    }
  }

  public ConfigIAP_Rewards DB_IAPRewards
  {
    get
    {
      if (this._DB_IAPRewards == null)
      {
        this._DB_IAPRewards = new ConfigIAP_Rewards();
        object rawData = this.GetRawData("iap_package_rewards.json");
        if (rawData != null)
          this._DB_IAPRewards.BuildDB(rawData);
      }
      return this._DB_IAPRewards;
    }
    set
    {
      this._DB_IAPRewards = value;
    }
  }

  public ConfigIAPMonthCard DB_IAPMonthCard
  {
    get
    {
      if (this._DB_IAPMonthCard == null)
      {
        this._DB_IAPMonthCard = new ConfigIAPMonthCard();
        object rawData = this.GetRawData("iap_month_card.json");
        if (rawData != null)
          this._DB_IAPMonthCard.BuildDB(rawData);
      }
      return this._DB_IAPMonthCard;
    }
    set
    {
      this._DB_IAPMonthCard = value;
    }
  }

  public ConfigMonthCard DB_MonthCard
  {
    get
    {
      if (this._DB_MonthCard == null)
      {
        this._DB_MonthCard = new ConfigMonthCard();
        object rawData = this.GetRawData("month_card.json");
        if (rawData != null)
          this._DB_MonthCard.BuildDB(rawData);
      }
      return this._DB_MonthCard;
    }
    set
    {
      this._DB_MonthCard = value;
    }
  }

  public ConfigSubscription DB_Subscription
  {
    get
    {
      if (this._DB_Subscription == null)
      {
        this._DB_Subscription = new ConfigSubscription();
        object rawData = this.GetRawData("subscription.json");
        if (rawData != null)
          this._DB_Subscription.BuildDB(rawData);
      }
      return this._DB_Subscription;
    }
    set
    {
      this._DB_Subscription = value;
    }
  }

  public ConfigNPC DB_NPC
  {
    get
    {
      if (this._DB_NPC == null)
      {
        this._DB_NPC = new ConfigNPC();
        object rawData = this.GetRawData("npc_info.json");
        if (rawData != null)
          this._DB_NPC.BuildDB(rawData);
      }
      return this._DB_NPC;
    }
  }

  public ConfigMonsterDisplay DB_MonsterDisplay
  {
    get
    {
      if (this._DB_MonsterDisplay == null)
      {
        this._DB_MonsterDisplay = new ConfigMonsterDisplay();
        object rawData = this.GetRawData("monster_display.json");
        if (rawData != null)
          this._DB_MonsterDisplay.BuildDB(rawData);
      }
      return this._DB_MonsterDisplay;
    }
  }

  public ConfigTempleSkill DB_TempleSkill
  {
    get
    {
      if (this._DB_TempleSkill == null)
      {
        this._DB_TempleSkill = new ConfigTempleSkill();
        object rawData = this.GetRawData("alliance_temple_skill.json");
        if (rawData != null)
          this._DB_TempleSkill.BuildDB(rawData);
      }
      return this._DB_TempleSkill;
    }
  }

  public ConfigWonderPackage DB_WonderPackage
  {
    get
    {
      if (this._DB_WonderPackage == null)
      {
        this._DB_WonderPackage = new ConfigWonderPackage();
        object rawData = this.GetRawData("wonder_package.json");
        if (rawData != null)
          this._DB_WonderPackage.BuildDB(rawData);
      }
      return this._DB_WonderPackage;
    }
  }

  public ConfigEquipmentSuitMain DB_EquipmentSuitMain
  {
    get
    {
      if (this._DB_EquipmentSuitMain == null)
      {
        this._DB_EquipmentSuitMain = new ConfigEquipmentSuitMain();
        object rawData = this.GetRawData("equipment_suit_main.json");
        if (rawData != null)
          this._DB_EquipmentSuitMain.BuildDB(rawData);
      }
      return this._DB_EquipmentSuitMain;
    }
  }

  public ConfigEquipmentSuitEnhance DB_EquipmentSuitEnhance
  {
    get
    {
      if (this._DB_EquipmentSuitEnhance == null)
      {
        this._DB_EquipmentSuitEnhance = new ConfigEquipmentSuitEnhance();
        if (File.Exists(NetApi.inst.BuildLocalPath("equipment_suit_enhance.json")))
          this._DB_EquipmentSuitEnhance.BuildDB(Utils.Json2Object(File.ReadAllText(NetApi.inst.BuildLocalPath("equipment_suit_enhance.json")), true));
      }
      return this._DB_EquipmentSuitEnhance;
    }
  }

  public ConfigEquipmentSuitGroup DB_EquipmentSuitGroup
  {
    get
    {
      if (this._DB_EquipemtSuitGroup == null)
      {
        this._DB_EquipemtSuitGroup = new ConfigEquipmentSuitGroup();
        object rawData = this.GetRawData("equipment_suit_group.json");
        if (rawData != null)
          this._DB_EquipemtSuitGroup.BuildDB(rawData);
      }
      return this._DB_EquipemtSuitGroup;
    }
  }

  public ConfigAltarSummonRewardMain DB_ConfigAltarSummonRewardMain
  {
    get
    {
      if (this._DB_ConfigAltarSummonRewardMain == null)
      {
        this._DB_ConfigAltarSummonRewardMain = new ConfigAltarSummonRewardMain();
        object rawData = this.GetRawData("altar_summon_reward_main.json");
        if (rawData != null)
          this._DB_ConfigAltarSummonRewardMain.BuildDB(rawData);
      }
      return this._DB_ConfigAltarSummonRewardMain;
    }
  }

  public ConfigMerlinTrialsGroup DB_MerlinTrialsGroup
  {
    get
    {
      if (this._DB_MerlinTrialsGroup == null)
      {
        this._DB_MerlinTrialsGroup = new ConfigMerlinTrialsGroup();
        object rawData = this.GetRawData("merlin_trials_group.json");
        if (rawData != null)
          this._DB_MerlinTrialsGroup.BuildDB(rawData);
      }
      return this._DB_MerlinTrialsGroup;
    }
  }

  public ConfigMerlinTrialsLayer DB_MerlinTrialsLayer
  {
    get
    {
      if (this._DB_MerlinTrialsLayer == null)
      {
        this._DB_MerlinTrialsLayer = new ConfigMerlinTrialsLayer();
        object rawData = this.GetRawData("merlin_trials_layer.json");
        if (rawData != null)
          this._DB_MerlinTrialsLayer.BuildDB(rawData);
      }
      return this._DB_MerlinTrialsLayer;
    }
  }

  public ConfigMerlinTrialsMain DB_MerlinTrialsMain
  {
    get
    {
      if (this._DB_MerlinTrialsMain == null)
      {
        this._DB_MerlinTrialsMain = new ConfigMerlinTrialsMain();
        object rawData = this.GetRawData("merlin_trials_main.json");
        if (rawData != null)
          this._DB_MerlinTrialsMain.BuildDB(rawData);
      }
      return this._DB_MerlinTrialsMain;
    }
  }

  public ConfigMerlinTrialsMonster DB_MerlinTrialsMonster
  {
    get
    {
      if (this._DB_MerlinTrialsMonster == null)
      {
        this._DB_MerlinTrialsMonster = new ConfigMerlinTrialsMonster();
        object rawData = this.GetRawData("merlin_trials_monster.json");
        if (rawData != null)
          this._DB_MerlinTrialsMonster.BuildDB(rawData);
      }
      return this._DB_MerlinTrialsMonster;
    }
  }

  public ConfigMerlinTrialsRewards DB_MerlinTrialsRewards
  {
    get
    {
      if (this._DB_MerlinTrialsRewards == null)
      {
        this._DB_MerlinTrialsRewards = new ConfigMerlinTrialsRewards();
        object rawData = this.GetRawData("merlin_trials_rewards.json");
        if (rawData != null)
          this._DB_MerlinTrialsRewards.BuildDB(rawData);
      }
      return this._DB_MerlinTrialsRewards;
    }
  }

  public ConfigMerlinTrialsShop DB_MerlinTrialsShop
  {
    get
    {
      if (this._DB_MerlinTrialsShop == null)
      {
        this._DB_MerlinTrialsShop = new ConfigMerlinTrialsShop();
        object rawData = this.GetRawData("merlin_trials_shop.json");
        if (rawData != null)
          this._DB_MerlinTrialsShop.BuildDB(rawData);
      }
      return this._DB_MerlinTrialsShop;
    }
  }

  public ConfigMerlinTrialsShopSpecific DB_MerlinTrialsShopSpecific
  {
    get
    {
      if (this._DB_MerlinTrialsShopSpecific == null)
      {
        this._DB_MerlinTrialsShopSpecific = new ConfigMerlinTrialsShopSpecific();
        object rawData = this.GetRawData("iap_merlin_trials_shop_specific.json");
        if (rawData != null)
          this._DB_MerlinTrialsShopSpecific.BuildDB(rawData);
      }
      return this._DB_MerlinTrialsShopSpecific;
    }
  }

  public ConfigMerlinTrialsPlayerUnit DB_MerlinTrialsPlayerUnit
  {
    get
    {
      if (this._DB_MerlinTrialsPlayerUnit == null)
      {
        this._DB_MerlinTrialsPlayerUnit = new ConfigMerlinTrialsPlayerUnit();
        object rawData = this.GetRawData("merlin_trials_player_unit.json");
        if (rawData != null)
          this._DB_MerlinTrialsPlayerUnit.BuildDB(rawData);
      }
      return this._DB_MerlinTrialsPlayerUnit;
    }
  }

  public ConfigLordTitleMain DB_LordTitleMain
  {
    get
    {
      if (this._DB_LordTitleMain == null)
      {
        this._DB_LordTitleMain = new ConfigLordTitleMain();
        object rawData = this.GetRawData("lord_title_main.json");
        if (rawData != null)
          this._DB_LordTitleMain.BuildDB(rawData);
      }
      return this._DB_LordTitleMain;
    }
  }

  public ConfigLordTitleSort DB_LordTitleSort
  {
    get
    {
      if (this._DB_LordTitleSort == null)
      {
        this._DB_LordTitleSort = new ConfigLordTitleSort();
        object rawData = this.GetRawData("lord_title_sort.json");
        if (rawData != null)
          this._DB_LordTitleSort.BuildDB(rawData);
      }
      return this._DB_LordTitleSort;
    }
  }

  private object GetRawData(string key)
  {
    string path = NetApi.inst.BuildLocalPath(key);
    if (!File.Exists(path))
    {
      if (!NetWorkDetector.Instance.IsReadyRestart())
        NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.ConfigParse, "FileNotFound", key);
      NetWorkDetector.Instance.RestartOrQuitGame(ScriptLocalization.Get("data_error_create_description", true), ScriptLocalization.Get("data_error_title", true), (string) null);
      return (object) null;
    }
    string json = File.ReadAllText(path);
    object obj = (object) null;
    try
    {
      obj = Utils.Json2Object(json, true);
    }
    catch (Exception ex)
    {
      if (!NetWorkDetector.Instance.IsReadyRestart())
        NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.ConfigParse, "ParseFail", ex.Message + ";fileName=" + key);
      File.Delete(path);
      NetWorkDetector.Instance.RestartOrQuitGame(ScriptLocalization.Get("data_error_create_description", true), ScriptLocalization.Get("data_error_title", true), (string) null);
    }
    return obj;
  }

  public static ConfigManager inst
  {
    get
    {
      if ((UnityEngine.Object) ConfigManager._singleton == (UnityEngine.Object) null)
      {
        ConfigManager._singleton = UnityEngine.Object.FindObjectOfType<ConfigManager>();
        if ((UnityEngine.Object) null == (UnityEngine.Object) ConfigManager._singleton)
          throw new ArgumentException("BundleManager hasn't been created yet.");
      }
      return ConfigManager._singleton;
    }
  }

  public void Init()
  {
  }

  public static int GetInt(string[] toks, int index)
  {
    if (index < 0 || index >= toks.Length)
    {
      Debug.LogError((object) ("GetInt " + (object) index + " " + (object) toks.Length));
      return 0;
    }
    if (toks[index].Length > 0)
      return int.Parse(toks[index]);
    return 0;
  }

  public static double GetDouble(string[] toks, int index)
  {
    if (index < 0 || index >= toks.Length)
    {
      Debug.LogError((object) ("GetDouble " + (object) index + " " + (object) toks.Length));
      return 0.0;
    }
    if (toks[index].Length > 0)
      return double.Parse(toks[index]);
    return 0.0;
  }

  public static string GetString(string[] toks, int index)
  {
    if (index >= 0 && index < toks.Length)
      return toks[index];
    Debug.LogError((object) ("GetString " + (object) index + " " + (object) toks.Length));
    return string.Empty;
  }

  public static bool GetBool(ArrayList arr, int index)
  {
    bool result1;
    if (bool.TryParse(ConfigManager.GetString(arr, index), out result1))
      return result1;
    int result2;
    if (int.TryParse(ConfigManager.GetString(arr, index), out result2))
      return result2 == 1;
    return false;
  }

  public static int GetInt(ArrayList arr, int index)
  {
    int result;
    int.TryParse(ConfigManager.GetString(arr, index), out result);
    return result;
  }

  public static float GetFloat(ArrayList arr, int index)
  {
    float result;
    float.TryParse(ConfigManager.GetString(arr, index), out result);
    return result;
  }

  public static double GetDouble(ArrayList arr, int index)
  {
    double result;
    double.TryParse(ConfigManager.GetString(arr, index), out result);
    return result;
  }

  public static string GetString(ArrayList arr, int index)
  {
    if (index >= 0 && index < arr.Count)
      return arr[index].ToString();
    return string.Empty;
  }

  public static object GetRawData(ArrayList arr, int index)
  {
    if (index >= 0 && index < arr.Count)
      return arr[index];
    return (object) string.Empty;
  }

  public static int GetIndex(string[] names, string byName, string secondName = null)
  {
    for (int index = 0; index < names.Length; ++index)
    {
      if (byName.Equals(names[index].Trim()))
        return index;
    }
    if (secondName != null)
    {
      for (int index = 0; index < names.Length; ++index)
      {
        if (secondName.Equals(names[index].Trim()))
          return index;
      }
    }
    Debug.LogError((object) ("GetIndex " + byName));
    return -1;
  }

  public static int GetIndex(ArrayList inHeader, string inKey)
  {
    if (inHeader == null || inHeader.Count <= 0 || (inKey == null || inKey.Length <= 0))
      return -1;
    for (int index = 0; index < inHeader.Count; ++index)
    {
      if (inKey.Equals(inHeader[index] as string))
        return index;
    }
    return -1;
  }

  public void AddData(int key, object data)
  {
  }

  public void ClearData()
  {
    if (this._DB_Items != null)
    {
      this._DB_Items.Clear();
      this._DB_Items = (ConfigItem) null;
    }
    if (this._DB_Properties != null)
    {
      this._DB_Properties.Clear();
      this._DB_Properties = (ConfigProperties) null;
    }
    if (this._DB_Hero_Equipment != null)
    {
      this._DB_Hero_Equipment.Clear();
      this._DB_Hero_Equipment = (ConfigHero_Equipment) null;
    }
    if (this._DB_KingdomDefinitions != null)
    {
      this._DB_KingdomDefinitions.Clear();
      this._DB_KingdomDefinitions = (ConfigKingdomDefinitions) null;
    }
    if (this._DB_Shop != null)
    {
      this._DB_Shop.Clear();
      this._DB_Shop = (ConfigShop) null;
    }
    if (this._DB_ActivityPriority != null)
    {
      this._DB_ActivityPriority.Clear();
      this._DB_ActivityPriority = (ConfigActivityPriority) null;
    }
    if (this._DBDragonKnightTalentBuff != null)
    {
      this._DBDragonKnightTalentBuff.Clear();
      this._DBDragonKnightTalentBuff = (ConfigDragonKnightTalentBuff) null;
    }
    if (this._DB_Building != null)
    {
      this._DB_Building.Clear();
      this._DB_Building = (ConfigBuilding) null;
    }
    if (this._DB_ShopCommonMain != null)
      this._DB_ShopCommonMain = (ConfigShopCommonMain) null;
    if (this._DB_ConfigShopCommonGroup != null)
      this._DB_ConfigShopCommonGroup = (ConfigShopCommonGroup) null;
    if (this._DB_ConfigShopCommonCategory != null)
      this._DB_ConfigShopCommonCategory = (ConfigShopCommonCategory) null;
    if (this._DB_Hero_Point != null)
    {
      this._DB_Hero_Point.Clear();
      this._DB_Hero_Point = (ConfigHeroPoint) null;
    }
    if (this._DB_HeroProfile != null)
    {
      this._DB_HeroProfile.Clear();
      this._DB_HeroProfile = (ConfigHeroProfile) null;
    }
    if (this._DB_GameConfig != null)
    {
      this._DB_GameConfig.Clear();
      this._DB_GameConfig = (ConfigGameConfig) null;
    }
    if (this._DB_Unit_Statistics != null)
    {
      this._DB_Unit_Statistics.Clear();
      this._DB_Unit_Statistics = (ConfigUnit_Statistics) null;
    }
    if (this._DB_KingSkill != null)
    {
      this._DB_KingSkill.Clear();
      this._DB_KingSkill = (ConfigKingSkill) null;
    }
    if (this._DB_Gift_Level != null)
    {
      this._DB_Gift_Level.Clear();
      this._DB_Gift_Level = (ConfigGift_Level) null;
    }
    if (this._DB_KingdomConfig != null)
    {
      this._DB_KingdomConfig.Clear();
      this._DB_KingdomConfig = (ConfigKingdomConfig) null;
    }
    if (this._DB_Techs != null)
    {
      this._DB_Techs.Clear();
      this._DB_Techs = (ConfigTechs) null;
    }
    if (this._DB_TechLevels != null)
    {
      this._DB_TechLevels.Clear();
      this._DB_TechLevels = (ConfigTechLevels) null;
    }
    if (this._DB_Quest != null)
    {
      this._DB_Quest.Clear();
      this._DB_Quest = (ConfigQuest) null;
    }
    this._DB_AllianceQuest = (ConfigAllianceQuest) null;
    if (this._DB_Alliance_Level != null)
    {
      this._DB_Alliance_Level.Clear();
      this._DB_Alliance_Level = (ConfigAlliance_Level) null;
    }
    if (this._DB_Tile_Statistics != null)
    {
      this._DB_Tile_Statistics.Clear();
      this._DB_Tile_Statistics = (ConfigTile_Statistics) null;
    }
    if (this._DB_Lookup != null)
    {
      this._DB_Lookup.Clear();
      this._DB_Lookup = (ConfigLookup) null;
    }
    if (this._DB_Achievement != null)
    {
      this._DB_Achievement.Clear();
      this._DB_Achievement = (ConfigAchievement) null;
    }
    if (this._DB_AchievementMain != null)
    {
      this._DB_AchievementMain.Clear();
      this._DB_AchievementMain = (ConfigAchievementMain) null;
    }
    if (this._DB_AllianceActivityChest != null)
    {
      this._DB_AllianceActivityChest.Clear();
      this._DB_AllianceActivityChest = (ConfigAllianceActivityChest) null;
    }
    if (this._DB_AlliancePersonalChest != null)
    {
      this._DB_AlliancePersonalChest.Clear();
      this._DB_AlliancePersonalChest = (ConfigAlliancePersonalChest) null;
    }
    if (this._DB_AllianceTreasuryVaultMain != null)
    {
      this._DB_AllianceTreasuryVaultMain.Clear();
      this._DB_AllianceTreasuryVaultMain = (ConfigAllianceTreasuryVaultMain) null;
    }
    if (this._DB_AbyssCollectReward != null)
    {
      this._DB_AbyssCollectReward.Clear();
      this._DB_AbyssCollectReward = (ConfigAbyssCollectReward) null;
    }
    if (this._DB_AllianceWarRewards != null)
    {
      this._DB_AllianceWarRewards.Clear();
      this._DB_AllianceWarRewards = (Config_AllianceWarRewards) null;
    }
    if (this._DB_AbyssMine != null)
    {
      this._DB_AbyssMine.Clear();
      this._DB_AbyssMine = (ConfigAbyssMine) null;
    }
    if (this._DB_AbyssRankReward != null)
    {
      this._DB_AbyssRankReward.Clear();
      this._DB_AbyssRankReward = (ConfigAbyssRankReward) null;
    }
    if (this._DB_Toast != null)
    {
      this._DB_Toast.Clear();
      this._DB_Toast = (ConfigToast) null;
    }
    if (this._DB_WorldEncount != null)
    {
      this._DB_WorldEncount.Clear();
      this._DB_WorldEncount = (ConfigWorldEncounter) null;
    }
    if (this._DB_MiniWonder != null)
    {
      this._DB_MiniWonder.Clear();
      this._DB_MiniWonder = (ConfigMiniWonder) null;
    }
    if (this._DB_BenefitCalc != null)
    {
      this._DB_BenefitCalc.Clear();
      this._DB_BenefitCalc = (ConfigBenefitCalc) null;
    }
    if (this._DB_Audio != null)
    {
      this._DB_Audio.Clear();
      this._DB_Audio = (ConfigAudio) null;
    }
    if (this._DB_HeroTalentActive != null)
    {
      this._DB_HeroTalentActive.Clear();
      this._DB_HeroTalentActive = (ConfigHeroTalentActive) null;
    }
    if (this._DB_Legend != null)
    {
      this._DB_Legend.Clear();
      this._DB_Legend = (ConfigLegend) null;
    }
    if (this._DB_LegendPoint != null)
    {
      this._DB_LegendPoint.Clear();
      this._DB_LegendPoint = (ConfigLegendPoint) null;
    }
    if (this._DB_LegendSkill != null)
    {
      this._DB_LegendSkill.Clear();
      this._DB_LegendSkill = (ConfigLegendSkill) null;
    }
    if (this._DB_LegendTower != null)
    {
      this._DB_LegendTower.Clear();
      this._DB_LegendTower = (ConfigLegendTower) null;
    }
    if (this._DB_ConfigAddress != null)
    {
      this._DB_ConfigAddress.Clear();
      this._DB_ConfigAddress = (ConfigAddress) null;
    }
    if (this._DB_Effect != null)
    {
      this._DB_Effect.Clear();
      this._DB_Effect = (ConfigEffect) null;
    }
    if (this._DB_NpcStore != null)
    {
      this._DB_NpcStore.Clear();
      this._DB_NpcStore = (ConfigNpcStore) null;
    }
    if (this._DB_NpcStoreRefreshPrice != null)
    {
      this._DB_NpcStoreRefreshPrice.Clear();
      this._DB_NpcStoreRefreshPrice = (ConfigNpcStoreRefreshPrice) null;
    }
    if (this._DB_ConfigDragon != null)
    {
      this._DB_ConfigDragon.Clear();
      this._DB_ConfigDragon = (ConfigDragon) null;
    }
    if (this._DB_ConfigADR != null)
    {
      this._DB_ConfigADR.Clear();
      this._DB_ConfigADR = (ConfigAllianceDonateRewards) null;
    }
    if (this._DB_ConfigDragonSkillMain != null)
    {
      this._DB_ConfigDragonSkillMain.Clear();
      this._DB_ConfigDragonSkillMain = (ConfigDragonSkillMain) null;
    }
    if (this._DB_DragonKnightItem != null)
    {
      this._DB_DragonKnightItem.Clear();
      this._DB_DragonKnightItem = (ConfigDragonKnightItem) null;
    }
    if (this._DB_ConfigDragonSkillGroup != null)
    {
      this._DB_ConfigDragonSkillGroup.Clear();
      this._DB_ConfigDragonSkillGroup = (ConfigDragonSkillGroup) null;
    }
    if (this._DB_ConfigDragonTendencyBoost != null)
    {
      this._DB_ConfigDragonTendencyBoost.Clear();
      this._DB_ConfigDragonTendencyBoost = (ConfigDragonTendencyBoost) null;
    }
    if (this._DB_Help != null)
    {
      this._DB_Help.Clear();
      this._DB_Help = (ConfigHelp) null;
    }
    if (this._DB_Privilege != null)
    {
      this._DB_Privilege.Clear();
      this._DB_Privilege = (ConfigPrivilege) null;
    }
    if (this._DB_GveBoss != null)
    {
      this._DB_GveBoss.Clear();
      this._DB_GveBoss = (ConfigGveBoss) null;
    }
    if (this._DB_BattleWounded != null)
    {
      this._DB_BattleWounded.Clear();
      this._DB_BattleWounded = (ConfigBattleWounded) null;
    }
    if (this._DB_PlayerProfile != null)
    {
      this._DB_PlayerProfile.Clear();
      this._DB_PlayerProfile = (ConfigPlayerProfile) null;
    }
    if (this._DB_QuestGroup != null)
    {
      this._DB_QuestGroup.Clear();
      this._DB_QuestGroup = (ConfigQuestGroup) null;
    }
    if (this._DB_ItemCompose != null)
    {
      this._DB_ItemCompose.Clear();
      this._DB_ItemCompose = (ConfigItemCompose) null;
    }
    if (this._DB_FestivalActives != null)
    {
      this._DB_FestivalActives.Clear();
      this._DB_FestivalActives = (ConfigFestivalActives) null;
    }
    if (this._IAPPackageGroup != null)
    {
      this._IAPPackageGroup.Clear();
      this._IAPPackageGroup = (ConfigIAP_PackageGroup) null;
    }
    if (this._DB_IAPPrice != null)
    {
      this._DB_IAPPrice.Clear();
      this._DB_IAPPrice = (ConfigIAP_FakePrice) null;
    }
    if (this._DB_IAPPaymentLevel != null)
    {
      this._DB_IAPPaymentLevel.Clear();
      this._DB_IAPPaymentLevel = (ConfigIAP_PaymentLevel) null;
    }
    if (this._DB_IAPPackage != null)
    {
      this._DB_IAPPackage.Clear();
      this._DB_IAPPackage = (ConfigIAP_Package) null;
    }
    if (this._DB_IAPDailyPackage != null)
    {
      this._DB_IAPDailyPackage.Clear();
      this._DB_IAPDailyPackage = (ConfigIAP_DailyPackage) null;
    }
    if (this._DB_IAPDailyPackageRewards != null)
    {
      this._DB_IAPDailyPackageRewards.Clear();
      this._DB_IAPDailyPackageRewards = (ConfigIAP_DailyPackageRewards) null;
    }
    if (this._DB_IAPDailyPackageSubRewards != null)
    {
      this._DB_IAPDailyPackageSubRewards.Clear();
      this._DB_IAPDailyPackageSubRewards = (ConfigIAP_DailyPackageSubRewards) null;
    }
    if (this._DB_IAPPlatform != null)
    {
      this._DB_IAPPlatform.Clear();
      this._DB_IAPPlatform = (ConfigIAP_Platform) null;
    }
    if (this._DB_IAPGold != null)
    {
      this._DB_IAPGold.Clear();
      this._DB_IAPGold = (ConfigIAP_Gold) null;
    }
    if (this._DB_IAPRewards != null)
    {
      this._DB_IAPRewards.Clear();
      this._DB_IAPRewards = (ConfigIAP_Rewards) null;
    }
    if (this._DB_IAPMonthCard != null)
    {
      this._DB_IAPMonthCard.Clear();
      this._DB_IAPMonthCard = (ConfigIAPMonthCard) null;
    }
    if (this._DB_MonthCard != null)
    {
      this._DB_MonthCard.Clear();
      this._DB_MonthCard = (ConfigMonthCard) null;
    }
    this._DB_NPC = (ConfigNPC) null;
    if (this._DB_FestivalExchange != null)
    {
      this._DB_FestivalExchange.Clear();
      this._DB_FestivalExchange = (ConfigFestivalExchange) null;
    }
    if (this._DB_FestivalExchangeReward != null)
    {
      this._DB_FestivalExchangeReward.Clear();
      this._DB_FestivalExchangeReward = (ConfigFestivalExchangeReward) null;
    }
    if (this._DB_TempleSkill != null)
    {
      this._DB_TempleSkill.Clear();
      this._DB_TempleSkill = (ConfigTempleSkill) null;
    }
    if (this._DB_WonderPackage != null)
    {
      this._DB_WonderPackage.Clear();
      this._DB_WonderPackage = (ConfigWonderPackage) null;
    }
    if (this._DB_EquipmentSuitMain != null)
    {
      this._DB_EquipmentSuitMain.Clear();
      this._DB_EquipmentSuitMain = (ConfigEquipmentSuitMain) null;
    }
    if (this._DB_EquipmentSuitEnhance != null)
    {
      this._DB_EquipmentSuitEnhance.Clear();
      this._DB_EquipmentSuitEnhance = (ConfigEquipmentSuitEnhance) null;
    }
    if (this._DB_EquipemtSuitGroup != null)
    {
      this._DB_EquipemtSuitGroup.Clear();
      this._DB_EquipemtSuitGroup = (ConfigEquipmentSuitGroup) null;
    }
    if (this._DB_WorldFlag != null)
    {
      this._DB_WorldFlag.Clear();
      this._DB_WorldFlag = (ConfigWorldFlag) null;
    }
    if (this._DB_DungeonBuff != null)
    {
      this._DB_DungeonBuff.Clear();
      this._DB_DungeonBuff = (ConfigDungeonBuff) null;
    }
    if (this._DB_DungeonChest != null)
    {
      this._DB_DungeonChest.Clear();
      this._DB_DungeonChest = (ConfigDungeonChest) null;
    }
    if (this._DB_DungeonMain != null)
    {
      this._DB_DungeonMain.Clear();
      this._DB_DungeonMain = (ConfigDungeonMain) null;
    }
    if (this._DB_DragonSkillEnhance != null)
    {
      this._DB_DragonSkillEnhance.Clear();
      this._DB_DragonSkillEnhance = (ConfigDragonSkillEnhance) null;
    }
    if (this._DB_TreasureMap != null)
    {
      this._DB_TreasureMap.Clear();
      this._DB_TreasureMap = (ConfigTreasureMap) null;
    }
    this._dungeonMonsterMain = (ConfigDungeonMonsterMain) null;
    this._dungeonMonsterGroup = (ConfigDungeonMonsterGroup) null;
    this._DB_AllianceShop = (ConfigAllianceShop) null;
    if (this._SevenDayQuests != null)
    {
      this._SevenDayQuests.Clear();
      this._SevenDayQuests = (ConfigSevenDayQuests) null;
    }
    if (this._SevenDayRewards != null)
    {
      this._SevenDayRewards.Clear();
      this._SevenDayRewards = (ConfigSevenDayRewards) null;
    }
    if (this._DB_VIP_Level != null)
    {
      this._DB_VIP_Level.Clear();
      this._DB_VIP_Level = (ConfigVIP_Level) null;
    }
    if (this._DB_WorldBoss != null)
    {
      this._DB_WorldBoss.Clear();
      this._DB_WorldBoss = (ConfigWorldBoss) null;
    }
    if (this._DB_TalentTree != null)
    {
      this._DB_TalentTree.Clear();
      this._DB_TalentTree = (ConfigTalentTree) null;
    }
    if (this._DB_PlayerTalent != null)
    {
      this._DB_PlayerTalent.Clear();
      this._DB_PlayerTalent = (ConfigPlayerTalent) null;
    }
    if (this._DB_RuralBlock != null)
    {
      this._DB_RuralBlock.Clear();
      this._DB_RuralBlock = (ConfigRuralBlock) null;
    }
    if (this._DB_BigTimerChest != null)
    {
      this._DB_BigTimerChest.Clear();
      this._DB_BigTimerChest = (ConfigBigTimerChest) null;
    }
    if (this._DB_RuralPlotsUnlock != null)
    {
      this._DB_RuralPlotsUnlock.Clear();
      this._DB_RuralPlotsUnlock = (ConfigRuralPlotsUnlock) null;
    }
    if (this._DB_ConfigRuralBuildingLimit != null)
    {
      this._DB_ConfigRuralBuildingLimit.Clear();
      this._DB_ConfigRuralBuildingLimit = (ConfigRuralBuildingLimit) null;
    }
    if (this._DB_DailyActivies != null)
    {
      this._DB_DailyActivies.Clear();
      this._DB_DailyActivies = (ConfigDailyActivy) null;
    }
    if (this._DB_FestivalVisit != null)
    {
      this._DB_FestivalVisit.Clear();
      this._DB_FestivalVisit = (ConfigFestivalVisit) null;
    }
    if (this._DB_DragonKnightTalent != null)
    {
      this._DB_DragonKnightTalent.Clear();
      this._DB_DragonKnightTalent = (ConfigDragonKnightTalent) null;
    }
    if (this._DB_DragonKnightTalentTree != null)
    {
      this._DB_DragonKnightTalentTree.Clear();
      this._DB_DragonKnightTalentTree = (ConfigDragonKnightTalentTree) null;
    }
    if (this._DB_DragonKnightTalentActive != null)
    {
      this._DB_DragonKnightTalentActive.Clear();
      this._DB_DragonKnightTalentActive = (ConfigDragonKnightTalentActive) null;
    }
    if (this._DB_DragonKnightProfile != null)
    {
      this._DB_DragonKnightProfile.Clear();
      this._DB_DragonKnightProfile = (ConfigDragonKnightProfile) null;
    }
    if (this._DB_QuestGroupMain != null)
    {
      this._DB_QuestGroupMain.Clear();
      this._DB_QuestGroupMain = (ConfigQuestGroupMain) null;
    }
    if (this._DB_equipmentGem != null)
    {
      this._DB_equipmentGem.Clear();
      this._DB_equipmentGem = (ConfigEquipmentGem) null;
    }
    if (this._DB_gemHandbook != null)
    {
      this._DB_gemHandbook.Clear();
      this._DB_gemHandbook = (ConfigEquipmentGemHandbook) null;
    }
    if (this._DB_gemHandbookSort != null)
    {
      this._DB_gemHandbookSort.Clear();
      this._DB_gemHandbookSort = (ConfigEquipmentGemHandbookSort) null;
    }
    if (this._DB_tavernWheelMain != null)
    {
      this._DB_tavernWheelMain.Clear();
      this._DB_tavernWheelMain = (ConfigTavernWheelMain) null;
    }
    if (this._DB_tavernWheelGroup != null)
    {
      this._DB_tavernWheelGroup.Clear();
      this._DB_tavernWheelGroup = (ConfigTavernWheelGroup) null;
    }
    if (this._DB_tavernWheelGrade != null)
    {
      this._DB_tavernWheelGrade.Clear();
      this._DB_tavernWheelGrade = (ConfigTavernWheelGrade) null;
    }
    if (this._DB_SimulatorForbidden != null)
    {
      this._DB_SimulatorForbidden.Clear();
      this._DB_SimulatorForbidden = (ConfigSimulatorForbidden) null;
    }
    if (this._dragonKnightEquipmentMain != null)
    {
      this._dragonKnightEquipmentMain.Clear();
      this._dragonKnightEquipmentMain = (ConfigEquipmentMain) null;
    }
    if (this._heroEquipmentMain != null)
    {
      this._heroEquipmentMain.Clear();
      this._heroEquipmentMain = (ConfigEquipmentMain) null;
    }
    if (this._dragonKnightEquipmentProperty != null)
    {
      this._dragonKnightEquipmentProperty.Clear();
      this._dragonKnightEquipmentProperty = (ConfigEquipmentProperty) null;
    }
    if (this._heroEquipmentProperty != null)
    {
      this._heroEquipmentProperty.Clear();
      this._heroEquipmentProperty = (ConfigEquipmentProperty) null;
    }
    if (this._dragonKnightEquipmentScroll != null)
    {
      this._dragonKnightEquipmentScroll.Clear();
      this._dragonKnightEquipmentScroll = (ConfigEquipmentScroll) null;
    }
    if (this._heroEquipmentScroll != null)
    {
      this._heroEquipmentScroll.Clear();
      this._heroEquipmentScroll = (ConfigEquipmentScroll) null;
    }
    if (this._dragonKnightEquipmentScrollChip != null)
    {
      this._dragonKnightEquipmentScrollChip.Clear();
      this._dragonKnightEquipmentScrollChip = (ConfigEquipmentScrollChip) null;
    }
    if (this._heroEquipmentScrollChip != null)
    {
      this._heroEquipmentScrollChip.Clear();
      this._heroEquipmentScrollChip = (ConfigEquipmentScrollChip) null;
    }
    if (this._dragonKnightEquipmentScrollCombine != null)
    {
      this._dragonKnightEquipmentScrollCombine.Clear();
      this._dragonKnightEquipmentScrollCombine = (ConfigEquipmentScrollCombine) null;
    }
    if (this._heroEquipmentScrollCombine != null)
    {
      this._heroEquipmentScrollCombine.Clear();
      this._heroEquipmentScrollCombine = (ConfigEquipmentScrollCombine) null;
    }
    if (this._DB_BuildingIdle != null)
    {
      this._DB_BuildingIdle.Clear();
      this._DB_BuildingIdle = (ConfigBuildingIdle) null;
    }
    if (this._DB_BuildingIdleCommon != null)
    {
      this._DB_BuildingIdleCommon.Clear();
      this._DB_BuildingIdleCommon = (ConfigBuildingIdleCommon) null;
    }
    if (this._DB_AnniversaryChampionMain != null)
    {
      this._DB_AnniversaryChampionMain.Clear();
      this._DB_AnniversaryChampionMain = (ConfigAnniversaryChampionMain) null;
    }
    if (this._DB_AnniversaryChampionRewards != null)
    {
      this._DB_AnniversaryChampionRewards.Clear();
      this._DB_AnniversaryChampionRewards = (ConfigAnniversaryChampionRewards) null;
    }
    this._DB_MonsterDisplay = (ConfigMonsterDisplay) null;
    this._DB_SuperLoginGroup = (ConfigSuperLoginGroup) null;
    this._DB_SuperLoginMain = (ConfigSuperLoginMain) null;
    this._DB_ArtifactShop = (ConfigArtifactShop) null;
    this._DB_Artifact = (ConfigArtifact) null;
    this._DB_WonderTitle = (ConfigWonderTitle) null;
    this._DB_LuckyArcherStepCost = (ConfigLuckyArcherStepCost) null;
    this._DB_LuckyArcherRewards = (ConfigLuckyArcherRewards) null;
    this._DB_LuckyArcher = (ConfigLuckyArcher) null;
    this._DB_LuckyArcherGroup = (ConfigLuckyArcherGroup) null;
    this._DB_DiyLotteryGroup = (ConfigDiyLotteryGroup) null;
    this._DB_DiyLotteryMain = (ConfigDiyLotteryMain) null;
    this._DB_DiceScoreBox = (ConfigDiceScoreBox) null;
    this._DB_KingdomBuff = (ConfigKingdomBuff) null;
    this._DB_Treasury = (ConfigTreasury) null;
    this._DB_AllianceBossRankReward = (ConfigAllianceBossRankReward) null;
    this._DB_AllianceBoss = (ConfigAllianceBoss) null;
    this._DB_AllianceBuildings = (ConfigAllianceBuilding) null;
    this._DB_CasinoJackpot = (ConfigCasinoJackpot) null;
    this._DB_CasinoChest = (ConfigCasinoChest) null;
    this._DB_CasinoRoulette = (ConfigCasinoRoulette) null;
    this._DB_FallenKnightRankReward = (ConfigFallenKnightRankReward) null;
    this._DB_FallenKnightMain = (ConfigFallenKnightMain) null;
    this._DB_FallenKnightDifficult = (ConfigFallenKnightDifficult) null;
    this._DB_FallenKnightScoreReq = (ConfigFallenKnightScoreReq) null;
    this._DB_AllianceActivityRankReward = (ConfigAllianceActivityRankReward) null;
    this._DB_AllianceActivityRequirement = (ConfigAllianceActivityRequirement) null;
    this._DB_AllianceActivityReward = (ConfigAllianceActivityReward) null;
    this._DB_AllianceActivityMainSub = (ConfigAllianceActivityMainSub) null;
    this._DB_ActivityRankReward = (ConfigActivityRankReward) null;
    this._DB_ActivityRequirement = (ConfigActivityRequirement) null;
    this._DB_ActivityReward = (ConfigActivityReward) null;
    this._DB_PersonalActivtyReward = (ConfigPersonalActivity) null;
    this._DB_PersonalActivtyRankReward = (ConfigPersonalActivityRankReward) null;
    this._DB_ActivityMain = (ConfigActivityMain) null;
    this._DB_Notifications = (ConfigNotification) null;
    this._DB_DropMain = (ConfigDropMain) null;
    this._DB_DropGroup = (ConfigDropGroup) null;
    this._DB_SignIn = (ConfigSignIn) null;
    this._DB_DungeonRankingReward = (ConfigDungeonRankingReward) null;
    this._DB_DragonKnightSkillStage = (ConfigDragonKnightSkillStage) null;
    this._DB_DragonKnightSkill = (ConfigDragonKnightSkill) null;
    this._DB_ActivityMainSub = (ConfigActivityMainSub) null;
    this._DB_DragonKnightLevel = (ConfigDragonKnightLevel) null;
    this._DB_WishWell = (ConfigWishWell) null;
    this._DB_WishWellTimes = (ConfigWishWellTimes) null;
    this._DB_QuestLinkHud = (ConfigQuestLinkHUD) null;
    this._DB_Boosts = (ConfigBoost) null;
    this._DB_RallySlot = (ConfigRallySlot) null;
    this._DB_Images = (ConfigImage) null;
    if (this._DB_DKArenaRankReward != null)
    {
      this._DB_DKArenaRankReward.Clear();
      this._DB_DKArenaRankReward = (ConfigDKArenaRankReward) null;
    }
    if (this._DB_DKArenaParticipateReward != null)
    {
      this._DB_DKArenaParticipateReward.Clear();
      this._DB_DKArenaParticipateReward = (ConfigDKArenaParticipateReward) null;
    }
    if (this._DB_DKArenaParticipateCost != null)
    {
      this._DB_DKArenaParticipateCost.Clear();
      this._DB_DKArenaParticipateCost = (ConfigDKArenaParticipateCost) null;
    }
    if (this._DB_DKArenaTitleReward != null)
    {
      this._DB_DKArenaTitleReward.Clear();
      this._DB_DKArenaTitleReward = (ConfigDKArenaTitleReward) null;
    }
    if (this._DB_MagicSelter != null)
    {
      this._DB_MagicSelter.Clear();
      this._DB_MagicSelter = (ConfigMagicSelter) null;
    }
    if ((UnityEngine.Object) this._DB_MagicStore != (UnityEngine.Object) null)
    {
      this._DB_MagicStore.Clear();
      this._DB_MagicStore = (ConfigMagicStore) null;
    }
    if (this._DB_MagicTowerMain != null)
    {
      this._DB_MagicTowerMain.Clear();
      this._DB_MagicTowerMain = (ConfigMagicTowerMain) null;
    }
    if (this._DB_MagicTowerMonster != null)
    {
      this._DB_MagicTowerMonster.Clear();
      this._DB_MagicTowerMonster = (ConfigMagicTowerMonster) null;
    }
    if (this._DB_MagicTowerPlayerUnit != null)
    {
      this._DB_MagicTowerPlayerUnit.Clear();
      this._DB_MagicTowerPlayerUnit = (ConfigMagicTowerPlayerUnit) null;
    }
    if (this._DB_MagicTowerShop != null)
    {
      this._DB_MagicTowerShop.Clear();
      this._DB_MagicTowerShop = (ConfigMagicTowerShop) null;
    }
    if (this._DB_MagicStoreRefreshPrice != null)
    {
      this._DB_MagicStoreRefreshPrice.Clear();
      this._DB_MagicStoreRefreshPrice = (ConfigMagicStoreRefreshPrice) null;
    }
    if (this._DB_DragonKnight_Property_Rate != null)
    {
      this._DB_DragonKnight_Property_Rate.Clear();
      this._DB_DragonKnight_Property_Rate = (ConfigDragonKnightPropertyRate) null;
    }
    if (this._DB_ConfigAltarSummonRewardMain != null)
    {
      this._DB_ConfigAltarSummonRewardMain.Clear();
      this._DB_ConfigAltarSummonRewardMain = (ConfigAltarSummonRewardMain) null;
    }
    if (this._DB_AnniversaryIapMain != null)
    {
      this._DB_AnniversaryIapMain.Clear();
      this._DB_AnniversaryIapMain = (ConfigAnniversaryIapMain) null;
    }
    if (this._DB_AnniversaryCommunityReward != null)
    {
      this._DB_AnniversaryCommunityReward.Clear();
      this._DB_AnniversaryCommunityReward = (ConfigAnniversaryCommunityeReward) null;
    }
    if (this._DB_KingdomBoss != null)
    {
      this._DB_KingdomBoss.Clear();
      this._DB_KingdomBoss = (ConfigKingdomBoss) null;
    }
    if (this._DB_VerificationReward != null)
    {
      this._DB_VerificationReward.Clear();
      this._DB_VerificationReward = (ConfigVerificationReward) null;
    }
    if (this._DB_AnniversaryStrongHoldDisplay != null)
    {
      this._DB_AnniversaryStrongHoldDisplay.Clear();
      this._DB_AnniversaryStrongHoldDisplay = (ConfigAnniversaryStrongHoldDisplay) null;
    }
    if (this._DB_BuildingGloryGroup != null)
    {
      this._DB_BuildingGloryGroup.Clear();
      this._DB_BuildingGloryGroup = (ConfigBuildingGloryGroup) null;
    }
    if (this._DB_BuildingGloryMain != null)
    {
      this._DB_BuildingGloryMain.Clear();
      this._DB_BuildingGloryMain = (ConfigBuildingGloryMain) null;
    }
    if (this._DB_MerlinTrialsGroup != null)
    {
      this._DB_MerlinTrialsGroup.Clear();
      this._DB_MerlinTrialsGroup = (ConfigMerlinTrialsGroup) null;
    }
    if (this._DB_MerlinTrialsLayer != null)
    {
      this._DB_MerlinTrialsLayer.Clear();
      this._DB_MerlinTrialsLayer = (ConfigMerlinTrialsLayer) null;
    }
    if (this._DB_MerlinTrialsMain != null)
    {
      this._DB_MerlinTrialsMain.Clear();
      this._DB_MerlinTrialsMain = (ConfigMerlinTrialsMain) null;
    }
    if (this._DB_MerlinTrialsMonster != null)
    {
      this._DB_MerlinTrialsMonster.Clear();
      this._DB_MerlinTrialsMonster = (ConfigMerlinTrialsMonster) null;
    }
    if (this._DB_MerlinTrialsRewards != null)
    {
      this._DB_MerlinTrialsRewards.Clear();
      this._DB_MerlinTrialsRewards = (ConfigMerlinTrialsRewards) null;
    }
    if (this._DB_MerlinTrialsShop != null)
    {
      this._DB_MerlinTrialsShop.Clear();
      this._DB_MerlinTrialsShop = (ConfigMerlinTrialsShop) null;
    }
    if (this._DB_MerlinTrialsShopSpecific != null)
    {
      this._DB_MerlinTrialsShopSpecific.Clear();
      this._DB_MerlinTrialsShopSpecific = (ConfigMerlinTrialsShopSpecific) null;
    }
    if (this._DB_MerlinTrialsPlayerUnit != null)
    {
      this._DB_MerlinTrialsPlayerUnit.Clear();
      this._DB_MerlinTrialsPlayerUnit = (ConfigMerlinTrialsPlayerUnit) null;
    }
    if (this._DB_LordTitleMain != null)
    {
      this._DB_LordTitleMain.Clear();
      this._DB_LordTitleMain = (ConfigLordTitleMain) null;
    }
    if (this._DB_LordTitleSort == null)
      return;
    this._DB_LordTitleSort.Clear();
    this._DB_LordTitleSort = (ConfigLordTitleSort) null;
  }
}
