﻿// Decompiled with JetBrains decompiler
// Type: ActivityManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ActivityManager
{
  private Dictionary<int, List<ActivityRankData>> stepRankDatas = new Dictionary<int, List<ActivityRankData>>();
  private Dictionary<int, List<ActivityRankData>> totalRankDatas = new Dictionary<int, List<ActivityRankData>>();
  private Dictionary<string, System.Action> handlers = new Dictionary<string, System.Action>();
  public List<ActivityManager.FestivalActivityData> festivals = new List<ActivityManager.FestivalActivityData>();
  public ActivityManager.FallenKnightData knightData = new ActivityManager.FallenKnightData();
  public PersonalActivityData personalData = new PersonalActivityData();
  public DKArenaActivityData dkArenaActivity = new DKArenaActivityData();
  public WorldBossActivityData worldBoss = new WorldBossActivityData();
  public AllianceWarActivityData allianceWar = new AllianceWarActivityData();
  public KingdomWarActivityData kingdomWarData = new KingdomWarActivityData();
  public PitActivityData pitData = new PitActivityData();
  private List<RoundActivityData> roundActivityData = new List<RoundActivityData>();
  private float lastToastTime = -10f;
  public const int LIMIT_TIME_ID = 0;
  private static ActivityManager _instance;
  private bool initialized;
  private bool _hasNewActivt;

  private ActivityManager()
  {
  }

  public event System.Action OnTimeLimitActivityFinish;

  public event System.Action OnTimeLimitActivityStartNewRound;

  public event System.Action onActivityDataUpdate;

  public event System.Action OnActivtyRefresh;

  public int loc_knightShowTime
  {
    get
    {
      return PlayerPrefsEx.GetIntByUid("activity_kngith_start_show_time");
    }
  }

  public int loc_knightLoaclStartTime
  {
    get
    {
      return PlayerPrefsEx.GetIntByUid("activity_kngith_start_time");
    }
  }

  public int loc_knightLoaclEndTime
  {
    get
    {
      return PlayerPrefsEx.GetIntByUid("activity_kngith_end_time");
    }
  }

  public int loc_goldShowTime
  {
    get
    {
      return PlayerPrefsEx.GetIntByUid("activity_gold_show_time");
    }
  }

  public int loc_goldStartTime
  {
    get
    {
      return PlayerPrefsEx.GetIntByUid("activity_gold_start_time");
    }
  }

  public int loc_goldEndTime
  {
    get
    {
      return PlayerPrefsEx.GetIntByUid("activity_gold_end_time");
    }
  }

  public int loc_monthlySpecialTime
  {
    get
    {
      return PlayerPrefsEx.GetIntByUid("monthly_special_show_time");
    }
  }

  public static ActivityManager Intance
  {
    get
    {
      if (ActivityManager._instance == null)
        ActivityManager._instance = new ActivityManager();
      return ActivityManager._instance;
    }
  }

  public void Init()
  {
    MessageHub.inst.GetPortByAction("knight_start").AddEvent(new System.Action<object>(this.KnightStart));
    MessageHub.inst.GetPortByAction("knight_start_march").AddEvent(new System.Action<object>(this.KnightMarchStart));
    MessageHub.inst.GetPortByAction("knight_finish").AddEvent(new System.Action<object>(this.KnightFinished));
  }

  public void Dispose()
  {
    MessageHub.inst.GetPortByAction("knight_start").RemoveEvent(new System.Action<object>(this.UpdateKnightData));
    MessageHub.inst.GetPortByAction("knight_start_march").RemoveEvent(new System.Action<object>(this.KnightMarchStart));
    MessageHub.inst.GetPortByAction("knight_finish").RemoveEvent(new System.Action<object>(this.KnightFinished));
    this.initialized = false;
    this.handlers.Clear();
  }

  public bool HasNewActivty
  {
    get
    {
      return this._hasNewActivt;
    }
    set
    {
      this._hasNewActivt = value;
    }
  }

  public void RefreshActivty()
  {
    if (this.OnActivtyRefresh == null)
      return;
    this.OnActivtyRefresh();
  }

  public void OnGameModeReady()
  {
    if (this.initialized)
      return;
    this.StopMonitor();
    RequestManager.inst.SendLoader("activity:getIntegralActivityState", (Hashtable) null, new System.Action<bool, object>(this.LoadTimeLimitActivityDataCallBack), false, false);
    this.initialized = true;
  }

  public List<ActivityRankData> GetCurrentRankData(int time, bool isStep = false, bool isPersonal = false)
  {
    if (isStep && this.stepRankDatas.ContainsKey(time))
      return this.stepRankDatas[time];
    if (this.totalRankDatas.ContainsKey(time))
      return this.totalRankDatas[time];
    return (List<ActivityRankData>) null;
  }

  public List<int> GetRankTime(bool isStep = false)
  {
    List<int> intList = new List<int>();
    Dictionary<int, List<ActivityRankData>> dictionary = this.stepRankDatas;
    if (!isStep)
      dictionary = this.totalRankDatas;
    Dictionary<int, List<ActivityRankData>>.KeyCollection.Enumerator enumerator = dictionary.Keys.GetEnumerator();
    while (enumerator.MoveNext())
      intList.Add(enumerator.Current);
    intList.Sort();
    if (!isStep)
      intList.Reverse();
    return intList;
  }

  public bool CanInvade(int tarWorldId)
  {
    if (tarWorldId == PlayerData.inst.userData.current_world_id || this.kingdomWarData == null)
      return false;
    KingdomWarActivityData.KingdomInfo kingdomInfo = this.kingdomWarData.Datas.Find((Predicate<KingdomWarActivityData.KingdomInfo>) (info => info.KingdomId == tarWorldId));
    return this.kingdomWarData.IsInWar && kingdomInfo != null && kingdomInfo.Status == "fighting";
  }

  public void LoadTimeLimitActivityData(System.Action callBack)
  {
    this.StopMonitor();
    this.UpdateHandler("activity:getIntegralActivityState", callBack);
    RequestManager.inst.SendRequest("activity:getIntegralActivityState", (Hashtable) null, new System.Action<bool, object>(this.LoadTimeLimitActivityDataCallBack), true);
  }

  public void LoadStepRankData(System.Action callBack)
  {
    this.UpdateHandler("activity:getStepRank", callBack);
    RequestManager.inst.SendRequest("activity:getStepRank", (Hashtable) null, new System.Action<bool, object>(this.LoadStepRankDataCallBack), true);
  }

  public void LoadPersonalTotalRankData(System.Action callBack)
  {
    this.UpdateHandler("activity:getSingleRank", callBack);
    new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    };
    RequestManager.inst.SendRequest("activity:getSingleRank", (Hashtable) null, new System.Action<bool, object>(this.LoadTotalRankDataCallBack), true);
  }

  public void LoadAllianceStepRankData(System.Action callBack)
  {
    this.UpdateHandler("activity:getAllianceStepRank", callBack);
    RequestManager.inst.SendRequest("activity:getAllianceStepRank", (Hashtable) null, new System.Action<bool, object>(this.LoadStepRankDataCallBack), true);
  }

  public void LoadAllianceTotalRankData(System.Action callBack)
  {
    this.UpdateHandler("activity:getAllianceTotalRank", callBack);
    RequestManager.inst.SendRequest("activity:getAllianceTotalRank", (Hashtable) null, new System.Action<bool, object>(this.LoadTotalRankDataCallBack), true);
  }

  public void CheckPersonalActivity()
  {
    RequestManager.inst.SendLoader("activity:loginCheckSingleActivityState", (Hashtable) null, (System.Action<bool, object>) null, true, false);
  }

  private void UpdateHandler(string key, System.Action callBack)
  {
    if (!this.handlers.ContainsKey(key))
      this.handlers.Add(key, callBack);
    this.handlers[key] = callBack;
  }

  public void LoadTotalRankData(System.Action callBack)
  {
    this.UpdateHandler("activity:getTotalRank", callBack);
    RequestManager.inst.SendRequest("activity:getTotalRank", (Hashtable) null, new System.Action<bool, object>(this.LoadTotalRankDataCallBack), true);
  }

  private void LoadStepRankDataCallBack(bool success, object result)
  {
    if (!success)
      return;
    this.stepRankDatas.Clear();
    this.ParseData(result, this.stepRankDatas);
    if (this.handlers.ContainsKey("activity:getStepRank"))
      this.ExecuteHandler("activity:getStepRank");
    if (!this.handlers.ContainsKey("activity:getAllianceStepRank"))
      return;
    this.ExecuteHandler("activity:getAllianceStepRank");
  }

  private void ExecuteHandler(string key)
  {
    if (!this.handlers.ContainsKey(key) || this.handlers[key] == null)
      return;
    this.handlers[key]();
    this.handlers.Remove(key);
  }

  private void LoadTotalRankDataCallBack(bool success, object result)
  {
    if (!success)
      return;
    this.stepRankDatas.Clear();
    this.ParseData(result, this.totalRankDatas);
    if (this.handlers.ContainsKey("activity:getTotalRank"))
      this.ExecuteHandler("activity:getTotalRank");
    if (this.handlers.ContainsKey("activity:getAllianceTotalRank"))
      this.ExecuteHandler("activity:getAllianceTotalRank");
    if (!this.handlers.ContainsKey("activity:getSingleRank"))
      return;
    this.ExecuteHandler("activity:getSingleRank");
  }

  public void UpdateData(object orgSrc)
  {
    this.LoadTimeLimitActivityDataCallBack(true, orgSrc);
  }

  public void KnightStart(object orgSrc)
  {
    ToastManager.Instance.AddBasicToast("toast_fallen_knight_event_start");
    this.UpdateKnightData(orgSrc);
  }

  public void KnightMarchStart(object orgSrc)
  {
    Hashtable hashtable = orgSrc as Hashtable;
    if (hashtable != null && hashtable.ContainsKey((object) "npc_uid"))
    {
      NPC_Info dataByUid = ConfigManager.inst.DB_NPC.GetDataByUid(long.Parse(hashtable[(object) "npc_uid"].ToString()));
      if ((double) Time.time - (double) this.lastToastTime > 3.0 && dataByUid != null)
      {
        this.lastToastTime = Time.time;
        if (dataByUid.para_2 == "event_fallen_knight_lord")
          ToastManager.Instance.AddBasicToast("toast_fallen_knight_event_attack");
        if (dataByUid.para_2 == "event_fallen_knight_army")
          ToastManager.Instance.AddBasicToast("toast_fallen_knight_event_fortress_attack");
      }
    }
    this.UpdateKnightData(orgSrc);
  }

  public void KnightFinished(object orgSrc)
  {
    this.UpdateKnightData(orgSrc);
  }

  public void UpdateKnightData(object orgSrc)
  {
    if (!this.knightData.Decode(orgSrc) || this.onActivityDataUpdate == null)
      return;
    this.onActivityDataUpdate();
  }

  private void AddRoundActivity(object hash, ActivityBaseData.ActivityType type)
  {
    RoundActivityData roundActivityData = (RoundActivityData) null;
    switch (type)
    {
      case ActivityBaseData.ActivityType.TimeLimit:
        roundActivityData = (RoundActivityData) new TimeLimitActivityData();
        break;
      case ActivityBaseData.ActivityType.AllianceActivity:
        roundActivityData = (RoundActivityData) new AllianceActivityData();
        break;
    }
    if (roundActivityData == null)
      return;
    roundActivityData.Decode(hash as Hashtable);
    if (roundActivityData.IsEmpty())
      return;
    this.roundActivityData.Add(roundActivityData);
  }

  private void LoadTimeLimitActivityDataCallBack(bool success, object result)
  {
    if (success && result != null)
    {
      Hashtable hashtable = result as Hashtable;
      if (hashtable != null)
      {
        this.roundActivityData.Clear();
        if (hashtable.ContainsKey((object) "integral_config"))
          this.AddRoundActivity(hashtable[(object) "integral_config"], ActivityBaseData.ActivityType.TimeLimit);
        if (hashtable.ContainsKey((object) "alliance_activity"))
          this.AddRoundActivity(hashtable[(object) "alliance_activity"], ActivityBaseData.ActivityType.AllianceActivity);
        if (this.roundActivityData.Count > 0)
          this.StartMonitor();
        this.festivals.Clear();
        if (hashtable.ContainsKey((object) "festival_activity"))
        {
          ArrayList arrayList = hashtable[(object) "festival_activity"] as ArrayList;
          if (arrayList != null)
          {
            for (int index = 0; index < arrayList.Count; ++index)
            {
              ActivityManager.FestivalActivityData festivalActivityData = new ActivityManager.FestivalActivityData();
              festivalActivityData.Decode(arrayList[index]);
              this.festivals.Add(festivalActivityData);
            }
          }
        }
        if (hashtable.ContainsKey((object) "knight_activity"))
          this.knightData.Decode(hashtable[(object) "knight_activity"]);
        if (hashtable.ContainsKey((object) "single_activity"))
        {
          this.personalData = new PersonalActivityData();
          this.personalData.Decode(hashtable[(object) "single_activity"] as Hashtable);
        }
        if (hashtable.ContainsKey((object) "kingdom_war_activity"))
        {
          this.kingdomWarData = new KingdomWarActivityData();
          this.kingdomWarData.Decode(hashtable[(object) "kingdom_war_activity"] as Hashtable);
        }
        if (hashtable.ContainsKey((object) "abyss"))
        {
          this.pitData = new PitActivityData();
          this.pitData.Decode(hashtable[(object) "abyss"] as Hashtable);
        }
        if (hashtable.ContainsKey((object) "dk_arena_activity"))
        {
          this.dkArenaActivity = new DKArenaActivityData();
          if (PlayerData.inst.CityData.mStronghold.mLevel >= 15)
            this.dkArenaActivity.Decode(hashtable[(object) "dk_arena_activity"] as Hashtable);
        }
        if (hashtable.ContainsKey((object) "world_boss"))
          this.worldBoss.Decode(hashtable[(object) "world_boss"] as Hashtable);
        if (hashtable.ContainsKey((object) "alliance_competition"))
          this.allianceWar.Decode(hashtable[(object) "alliance_competition"] as Hashtable);
        else
          this.allianceWar.IsActivityOff = true;
        this.CheckNewActivity();
        if (this.onActivityDataUpdate != null)
          this.onActivityDataUpdate();
      }
    }
    this.ExecuteHandler("activity:getIntegralActivityState");
  }

  private void ParseData(object result, Dictionary<int, List<ActivityRankData>> container)
  {
    Hashtable hashtable1 = result as Hashtable;
    container.Clear();
    if (hashtable1 == null)
      return;
    IEnumerator enumerator1 = hashtable1.Keys.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      if (enumerator1.Current != null)
      {
        int result1 = 0;
        if (int.TryParse(enumerator1.Current.ToString(), out result1))
        {
          List<ActivityRankData> activityRankDataList = new List<ActivityRankData>();
          Hashtable hashtable2 = hashtable1[enumerator1.Current] as Hashtable;
          if (hashtable2 != null)
          {
            IEnumerator enumerator2 = hashtable2.Keys.GetEnumerator();
            while (enumerator2.MoveNext())
            {
              if (enumerator2.Current != null)
              {
                ActivityRankData activityRankData = new ActivityRankData();
                if (activityRankData.Decode(enumerator2.Current.ToString(), hashtable2[enumerator2.Current]))
                  activityRankDataList.Add(activityRankData);
              }
            }
            activityRankDataList.Sort(new Comparison<ActivityRankData>(this.CompareRankData));
          }
          container.Add(result1, activityRankDataList);
        }
      }
    }
  }

  private int CompareRankData(ActivityRankData a, ActivityRankData b)
  {
    return a.rank.CompareTo(b.rank);
  }

  private void StartMonitor()
  {
    this.StopMonitor();
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  private void Process(int time)
  {
    this.CheckRoundActivity();
  }

  private void CheckRoundActivity()
  {
    if (this.roundActivityData.Count == 0)
      return;
    for (int index = 0; index < this.roundActivityData.Count; ++index)
    {
      RoundActivityData roundActivityData = this.roundActivityData[index];
      if (roundActivityData.EndTime <= NetServerTime.inst.ServerTimestamp)
      {
        this.StopMonitor();
        if (this.OnTimeLimitActivityFinish == null)
          break;
        this.OnTimeLimitActivityFinish();
        break;
      }
      if (roundActivityData.StartTime == NetServerTime.inst.ServerTimestamp && this.OnTimeLimitActivityStartNewRound != null)
        this.OnTimeLimitActivityStartNewRound();
      if (roundActivityData.IsStart() && roundActivityData.CurrentRound.EndTime <= NetServerTime.inst.ServerTimestamp && (roundActivityData.NextRound() && this.OnTimeLimitActivityStartNewRound != null))
        this.OnTimeLimitActivityStartNewRound();
    }
  }

  public void StopMonitor()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
  }

  public bool isActivityExist
  {
    get
    {
      return (this.isPortalExist || this.roundActivityData.Count != 0 || (this.festivals.Count != 0 || this.knightData.isContainActivity) || (!this.personalData.IsEmpty() || !this.kingdomWarData.IsEmpty() || (this.pitData.IsActivityShouldOpen || !this.dkArenaActivity.IsEmpty())) || !this.worldBoss.IsEmpty() ? 0 : (this.allianceWar.IsEmpty() ? 1 : 0)) == 0;
    }
  }

  private void CheckNewActivity()
  {
    this.HasNewActivty = false;
    if (!this.isActivityExist)
      return;
    int intByUid = PlayerPrefsEx.GetIntByUid("NEW_ACTIVITY_TIME", 0);
    if (intByUid == 0)
    {
      this.HasNewActivty = true;
    }
    else
    {
      if (intByUid <= 0)
        return;
      for (int index = 0; index < this.roundActivityData.Count; ++index)
      {
        RoundActivityData roundActivityData = this.roundActivityData[index];
        if (roundActivityData.StartTime >= intByUid && roundActivityData.IsStart())
        {
          this.HasNewActivty = true;
          return;
        }
      }
      for (int index = 0; index < this.festivals.Count; ++index)
      {
        ActivityManager.FestivalActivityData festival = this.festivals[index];
        if (festival.startTime >= intByUid && festival.startTime < NetServerTime.inst.ServerTimestamp)
        {
          this.HasNewActivty = true;
          return;
        }
      }
      if ((this.kingdomWarData.StartTime < intByUid || !this.kingdomWarData.IsStart()) && (this.pitData.StartTime < intByUid || this.pitData.StartTime > NetServerTime.inst.ServerTimestamp) && ((this.dkArenaActivity.StartTime < intByUid || !this.dkArenaActivity.IsStart()) && (this.worldBoss.StartTime < intByUid || !this.worldBoss.IsStart())) && ((this.personalData.StartTime < intByUid || !this.personalData.IsStart()) && (this.knightData.startTime < intByUid || this.knightData.startTime > NetServerTime.inst.ServerTimestamp) && (!this.allianceWar.IsStart() || this.allianceWar.StartTime < intByUid)))
        return;
      this.HasNewActivty = true;
    }
  }

  public void ContinueDKArean()
  {
    if (!GameEngine.IsReady() || GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.CityMode)
      return;
    UIManager.inst.ShowSystemBlocker("FullScreenWait", (SystemBlocker.SystemBlockerParameter) null);
    UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
    {
      GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.CityMode;
      GameEngine.Instance.OnGameModeReady += new System.Action(this.OnGameReady);
    }));
  }

  private void OnGameReady()
  {
    GameEngine.Instance.OnGameModeReady -= new System.Action(this.OnGameReady);
    if (ActivityManager.Intance.isActivityExist)
      UIManager.inst.OpenDlg("Activity/ActivityMainDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    if (ActivityManager.Intance.dkArenaActivity.IsEmpty())
      return;
    UIManager.inst.OpenDlg("DKArena/DKArenaMainDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public RoundActivityData CurrentTimeLimitActivity
  {
    get
    {
      return this.GetRoundData(ActivityBaseData.ActivityType.TimeLimit);
    }
  }

  public int GetFestivalRemainTime(int activityId)
  {
    for (int index = 0; index < this.festivals.Count; ++index)
    {
      if (this.festivals[index].activityId == activityId)
        return this.festivals[index].endTime - NetServerTime.inst.ServerTimestamp;
    }
    return 0;
  }

  public bool isPortalExist
  {
    get
    {
      AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
      if (alliancePortalData != null)
        return alliancePortalData.CurrentState == AlliancePortalData.State.COMPLETE;
      return false;
    }
  }

  public string GetPortalTimeStr()
  {
    AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
    if (alliancePortalData != null)
    {
      switch (AllianceBuildUtils.GetAlliancePortalBossSummonState())
      {
        case AlliancePortalData.BossSummonState.COOLING_DOWN:
          return ScriptLocalization.GetWithPara("alliance_portal_cooldown_time", new Dictionary<string, string>()
          {
            {
              "0",
              Utils.FormatTime1((int) alliancePortalData.CDTime - NetServerTime.inst.ServerTimestamp)
            }
          }, true);
        case AlliancePortalData.BossSummonState.COOLING_DOWN_OVER:
          return Utils.XLAT("alliance_portal_ready_tip");
        case AlliancePortalData.BossSummonState.CAN_ACTIVATE:
          return Utils.XLAT("alliance_portal_status_cooldown_finished");
        case AlliancePortalData.BossSummonState.ACTIVATED:
        case AlliancePortalData.BossSummonState.ACTIVATED_TO_RALLY:
          return ScriptLocalization.GetWithPara("alliance_portal_close_time", new Dictionary<string, string>()
          {
            {
              "0",
              Utils.FormatTime1((int) alliancePortalData.CDTime - ConfigManager.inst.DB_GameConfig.GetData("alliance_boss_open_cd").ValueInt * 60 * 60 + ConfigManager.inst.DB_GameConfig.GetData("alliance_boss_duration").ValueInt * 60 - NetServerTime.inst.ServerTimestamp)
            }
          }, true);
      }
    }
    return string.Empty;
  }

  public List<RoundActivityRankRewardUIBaseData> GetRoundReward(ActivityBaseData data, bool isTotalReward)
  {
    List<RoundActivityRankRewardUIBaseData> rewardUiBaseDataList = new List<RoundActivityRankRewardUIBaseData>();
    RoundActivityData roundActivityData = data as RoundActivityData;
    ActivityBaseData.ActivityType type = data.Type;
    switch (type)
    {
      case ActivityBaseData.ActivityType.TimeLimit:
        List<ActivityRankRewardInfo> activityRankRewardInfoList1 = !isTotalReward ? ConfigManager.inst.DB_ActivityRankReward.GetRewardInfos(roundActivityData.CurrentRound.ActivitySubConfigID) : ConfigManager.inst.DB_ActivityRankReward.GetTotalRewardInfos();
        for (int index = 0; index < activityRankRewardInfoList1.Count; ++index)
        {
          TimeLimitRankRewardUIData rankRewardUiData = new TimeLimitRankRewardUIData();
          rankRewardUiData.Data = (object) activityRankRewardInfoList1[index];
          rewardUiBaseDataList.Add((RoundActivityRankRewardUIBaseData) rankRewardUiData);
        }
        break;
      case ActivityBaseData.ActivityType.AllianceActivity:
        List<AllianceActivityRankRewardInfo> activityRankRewardInfoList2 = !isTotalReward ? ConfigManager.inst.DB_AllianceActivityRankReward.GetRewardInfos(roundActivityData.CurrentRound.ActivitySubConfigID) : ConfigManager.inst.DB_AllianceActivityRankReward.GetTotalRewardInfos();
        for (int index = 0; index < activityRankRewardInfoList2.Count; ++index)
        {
          AllianceActivityRankRewardUIData rankRewardUiData = new AllianceActivityRankRewardUIData();
          rankRewardUiData.Data = (object) activityRankRewardInfoList2[index];
          rewardUiBaseDataList.Add((RoundActivityRankRewardUIBaseData) rankRewardUiData);
        }
        break;
      default:
        switch (type - 7)
        {
          case ActivityBaseData.ActivityType.None:
            List<PersonalActivityRankReward> totalRewardInfos = ConfigManager.inst.DB_PersonalActivtyRankReward.GetTotalRewardInfos();
            for (int index = 0; index < totalRewardInfos.Count; ++index)
            {
              PersonalActivityRankRewardUIData rankRewardUiData = new PersonalActivityRankRewardUIData();
              rankRewardUiData.Data = (object) totalRewardInfos[index];
              rewardUiBaseDataList.Add((RoundActivityRankRewardUIBaseData) rankRewardUiData);
            }
            break;
          case ActivityBaseData.ActivityType.AllianceBoss:
            List<DKArenaRankReward> totals = ConfigManager.inst.DB_DKArenaRankReward.GetTotals();
            for (int index = 0; index < totals.Count; ++index)
            {
              DKArenaActivityRankRewardUIBaseData rewardUiBaseData = new DKArenaActivityRankRewardUIBaseData();
              rewardUiBaseData.Data = (object) totals[index];
              rewardUiBaseDataList.Add((RoundActivityRankRewardUIBaseData) rewardUiBaseData);
            }
            break;
        }
    }
    return rewardUiBaseDataList;
  }

  public RoundActivityData GetRoundData(ActivityBaseData.ActivityType type)
  {
    for (int index = 0; index < this.roundActivityData.Count; ++index)
    {
      if (this.roundActivityData[index].Type == type)
        return this.roundActivityData[index];
    }
    return (RoundActivityData) null;
  }

  public bool HadTimeLimitActivity()
  {
    return this.GetRoundData(ActivityBaseData.ActivityType.TimeLimit) != null;
  }

  public bool HadAllianceActivity()
  {
    return this.GetRoundData(ActivityBaseData.ActivityType.AllianceActivity) != null;
  }

  public struct ActivityKey
  {
    public const string GOLD_EVENT = "integral_config";
    public const string ALLIANCE_EVENT = "alliance_activity";
    public const string FESTIVAL_EVENT = "festival_activity";
    public const string KNIGHT_EVENT = "knight_activity";
    public const string SINGLE_EVENT = "single_activity";
    public const string KINGDOM_WAR_EVENT = "kingdom_war_activity";
    public const string ABYSS_EVENT = "abyss";
    public const string DK_ARENA_EVENT = "dk_arena_activity";
    public const string ALLIANCE_BOSS_EVENT = "alliance_boss";
    public const string WORLD_BOSS_EVENT = "world_boss";
    public const string ALLIANCE_COMPETITION = "alliance_competition";
  }

  public class FestivalActivityData
  {
    public int activityId;
    public int startTime;
    public int endTime;

    public void Decode(object orgSrc)
    {
      Hashtable inData = orgSrc as Hashtable;
      if (inData == null)
        return;
      DatabaseTools.UpdateData(inData, "activity_id", ref this.activityId);
      DatabaseTools.UpdateData(inData, "start_time", ref this.startTime);
      DatabaseTools.UpdateData(inData, "end_time", ref this.endTime);
    }

    public struct Param
    {
      public const string ACTIVITY_ID = "activity_id";
      public const string START_TIME = "start_time";
      public const string END_TIME = "end_time";
    }
  }

  public class FallenKnightData
  {
    private bool containData;
    private int _marchCount;
    private long _topScore;
    private int _activityId;
    private int _startTime;
    private int _endTime;
    private int _activityTime;
    private Hashtable _history;
    private Hashtable _scoreAndRank;
    private int _isFinished;
    public Coordinate curCoordinate;
    public long lastUpdateTime;

    public bool isContainActivity
    {
      get
      {
        return this.containData;
      }
    }

    public bool isActivity
    {
      get
      {
        if (this.containData && this.activityTime > this.startTime)
          return this.activityTime < this.endTime;
        return false;
      }
    }

    public bool isActivityAbleToStart
    {
      get
      {
        if (this.containData && NetServerTime.inst.ServerTimestamp > this.startTime && NetServerTime.inst.ServerTimestamp < this.endTime)
          return this.activityTime < this.startTime;
        return false;
      }
    }

    public bool isActivityForbidden
    {
      get
      {
        if (this.containData && this.endTime - NetServerTime.inst.ServerTimestamp <= 43200)
          return !this.isActivity;
        return false;
      }
    }

    public bool isActivityOver
    {
      get
      {
        if (this.containData)
          return NetServerTime.inst.ServerTimestamp > this.endTime;
        return false;
      }
    }

    public int marchCount
    {
      get
      {
        return this._marchCount;
      }
    }

    public long topScore
    {
      get
      {
        return this._topScore;
      }
    }

    public int activityId
    {
      get
      {
        return this._activityId;
      }
    }

    public int startTime
    {
      get
      {
        return this._startTime;
      }
    }

    public int endTime
    {
      get
      {
        return this._endTime;
      }
    }

    public int activityTime
    {
      get
      {
        return this._activityTime;
      }
    }

    public Hashtable history
    {
      get
      {
        return this._history;
      }
    }

    public Hashtable scoreAndRank
    {
      get
      {
        return this._scoreAndRank;
      }
    }

    public bool isFinished
    {
      get
      {
        return this._isFinished == 1;
      }
    }

    public bool Decode(object orgSrc)
    {
      Hashtable inData = orgSrc as Hashtable;
      if (inData != null)
      {
        this.containData = true;
        bool flag = false | DatabaseTools.UpdateData(inData, "activity_id", ref this._activityId) | DatabaseTools.UpdateData(inData, "start", ref this._startTime) | DatabaseTools.UpdateData(inData, "end", ref this._endTime) | DatabaseTools.UpdateData(inData, "open_time", ref this._activityTime) | DatabaseTools.UpdateData(inData, "is_finish", ref this._isFinished) | DatabaseTools.UpdateData(inData, "march_count", ref this._marchCount) | DatabaseTools.UpdateData(inData, "top_score", ref this._topScore);
        string empty = string.Empty;
        DatabaseTools.UpdateData(inData, "start_location", ref empty);
        if (!string.IsNullOrEmpty(empty))
        {
          this.curCoordinate.K = PlayerData.inst.playerCityData.cityLocation.K;
          string[] strArray = empty.Split(':');
          if (strArray.Length == 2)
          {
            flag = true;
            this.curCoordinate.X = int.Parse(strArray[0]);
            this.curCoordinate.Y = int.Parse(strArray[1]);
          }
        }
        if (inData[(object) "history"] != null)
          flag |= DatabaseTools.CheckAndParseOrgData(inData[(object) "history"], out this._history);
        if (inData[(object) "score_and_rank"] != null)
          flag |= DatabaseTools.CheckAndParseOrgData(inData[(object) "score_and_rank"], out this._scoreAndRank);
        PlayerPrefsEx.SetIntByUid("activity_kngith_start_time", this._startTime);
        PlayerPrefsEx.SetIntByUid("activity_kngith_end_time", this._endTime);
        PlayerPrefsEx.Save();
        return flag;
      }
      this.containData = false;
      return false;
    }

    public int GetFallenKnightCurrentWaves()
    {
      int activityTime = this.activityTime;
      int num1 = NetServerTime.inst.ServerTimestamp - activityTime;
      if (activityTime == 0 || num1 == 0)
        return 1;
      List<FallenKnightMainInfo> fallenKnightMainList = ConfigManager.inst.DB_FallenKnightMain.GetFallenKnightMainList();
      int num2 = 0;
      for (int index = 0; index < fallenKnightMainList.Count; ++index)
      {
        num2 += fallenKnightMainList[index].InvadeTime;
        if (num1 <= num2)
          return index;
      }
      return fallenKnightMainList.Count;
    }

    public string GetCurrentFallenKnightName(int curWave)
    {
      List<FallenKnightMainInfo> fallenKnightMainList = ConfigManager.inst.DB_FallenKnightMain.GetFallenKnightMainList();
      string str = string.Empty;
      if (curWave <= fallenKnightMainList.Count && curWave > 0)
      {
        NPC_Info data = ConfigManager.inst.DB_NPC.GetData(int.Parse(fallenKnightMainList[curWave - 1].NpcId));
        if (data != null)
          str = Utils.XLAT(data.name);
      }
      return str;
    }

    public int GetFallenKnightRemainedWaves()
    {
      return ConfigManager.inst.DB_FallenKnightMain.GetFallenKnightMainList().Count - this.GetFallenKnightCurrentWaves();
    }

    public int GetFallenKnightTotalInvadeTime()
    {
      List<FallenKnightMainInfo> fallenKnightMainList = ConfigManager.inst.DB_FallenKnightMain.GetFallenKnightMainList();
      int num = 0;
      for (int index = 0; index < fallenKnightMainList.Count; ++index)
        num += fallenKnightMainList[index].InvadeTime;
      return num;
    }

    public string GetNextWaveAppearTime()
    {
      int knightCurrentWaves = this.GetFallenKnightCurrentWaves();
      List<FallenKnightMainInfo> fallenKnightMainList = ConfigManager.inst.DB_FallenKnightMain.GetFallenKnightMainList();
      int num = 0;
      int time;
      if (knightCurrentWaves < fallenKnightMainList.Count)
      {
        for (int index = 0; index <= knightCurrentWaves; ++index)
          num += fallenKnightMainList[index].InvadeTime;
        time = num + this.activityTime - NetServerTime.inst.ServerTimestamp;
      }
      else
        time = 0;
      return Utils.FormatTime(time, true, false, true);
    }

    public struct Param
    {
      public const string ACTIVITY_ID = "activity_id";
      public const string START_TIME = "start";
      public const string END_TIME = "end";
      public const string ACTIVITY_TIME = "open_time";
      public const string HISTORY = "history";
      public const string SCORE_AND_RANK = "score_and_rank";
      public const string IS_FINISHED = "is_finish";
      public const string START_LOCATION = "start_location";
      public const string MARCH_COUNT = "march_count";
      public const string TOP_SCORE = "top_score";
    }
  }
}
