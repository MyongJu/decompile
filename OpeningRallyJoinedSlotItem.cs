﻿// Decompiled with JetBrains decompiler
// Type: OpeningRallyJoinedSlotItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class OpeningRallyJoinedSlotItem : MonoBehaviour
{
  private bool fistTime = true;
  private const string TEXTURE_PATH_PRE = "Texture/Unit/portrait_unit_";
  public UITexture playerIcon;
  public UILabel playerName;
  public UILabel troopsCount;
  public UILabel tip;
  public GameObject OpenContent;
  public UISprite openBg;
  public UISprite normalBg;
  public UITable childContainer;
  private bool isOpen;
  public IconGroup skillGroup;
  public System.Action OnOpenCallBackDelegate;

  public void SetData(int index)
  {
    OpeningRallyJoinedSlotItem.Para pr = new OpeningRallyJoinedSlotItem.Para();
    switch (index)
    {
      case 0:
        pr.iconname = 8;
        pr.name = "intro_rally_member_1";
        pr.troopcount = 300000;
        break;
      case 1:
        pr.iconname = 3;
        pr.name = "intro_rally_member_2";
        pr.troopcount = 300000;
        break;
      case 3:
        pr.iconname = 0;
        pr.name = "intro_rally_member_3";
        pr.troopcount = 300000;
        break;
      case 4:
        pr.iconname = 7;
        pr.name = "intro_rally_member_4";
        pr.troopcount = 300000;
        break;
      case 5:
        pr.iconname = 6;
        pr.name = "intro_rally_member_5";
        pr.troopcount = 300000;
        break;
    }
    this.UpdateUI(pr);
    this.childContainer.repositionNow = true;
    this.childContainer.Reposition();
  }

  private void UpdateUI(OpeningRallyJoinedSlotItem.Para pr)
  {
    this.troopsCount.text = pr.troopcount.ToString();
    this.playerName.text = ScriptLocalization.Get(pr.name, true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.playerIcon, UserData.GetPortraitIconPath(pr.iconname), (System.Action<bool>) null, true, false, string.Empty);
    this.skillGroup.CreateIcon(this.GetSkillIconData());
  }

  private List<IconData> GetSkillIconData()
  {
    List<IconData> iconDataList = new List<IconData>();
    List<Unit_StatisticsInfo> unitStatisticsInfoList = new List<Unit_StatisticsInfo>();
    unitStatisticsInfoList.Add(ConfigManager.inst.DB_Unit_Statistics.GetData("infantry_t10"));
    unitStatisticsInfoList.Add(ConfigManager.inst.DB_Unit_Statistics.GetData("cavalry_t10"));
    unitStatisticsInfoList.Add(ConfigManager.inst.DB_Unit_Statistics.GetData("ranged_t10"));
    unitStatisticsInfoList.Add(ConfigManager.inst.DB_Unit_Statistics.GetData("siege_t10"));
    for (int index = 0; index < unitStatisticsInfoList.Count; ++index)
    {
      Unit_StatisticsInfo unitStatisticsInfo = unitStatisticsInfoList[index];
      IconData iconData = new IconData(string.Format("{0}{1}", (object) "Texture/Unit/portrait_unit_", (object) unitStatisticsInfo.Image), new string[3]
      {
        ScriptLocalization.Get(string.Format("{0}{1}", (object) unitStatisticsInfo.ID, (object) "_name"), true),
        "100,000",
        Utils.GetRomaNumeralsBelowTen(unitStatisticsInfo.Troop_Tier)
      });
      iconDataList.Add(iconData);
    }
    return iconDataList;
  }

  public void OnClickHandler()
  {
    if (!this.gameObject.activeSelf)
      return;
    this.isOpen = !this.isOpen;
    NGUITools.SetActive(this.OpenContent, this.isOpen);
    NGUITools.SetActive(this.normalBg.gameObject, false);
    Utils.ExecuteInSecs(0.01f, (System.Action) (() =>
    {
      if (!this.isOpen)
      {
        this.normalBg.height = (int) byte.MaxValue;
      }
      else
      {
        Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.transform, false);
        if (this.fistTime)
        {
          this.fistTime = false;
          this.normalBg.height = (int) relativeWidgetBounds.size.y;
        }
        else
          this.normalBg.height = (int) relativeWidgetBounds.size.y + 80;
      }
      NGUITools.SetActive(this.normalBg.gameObject, true);
      NGUITools.AddWidgetCollider(this.transform.parent.gameObject);
      this.childContainer.Reposition();
      if (this.OnOpenCallBackDelegate == null)
        return;
      this.OnOpenCallBackDelegate();
    }));
  }

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.playerIcon);
  }

  public class Para
  {
    public int iconname;
    public string name;
    public int troopcount;
  }
}
