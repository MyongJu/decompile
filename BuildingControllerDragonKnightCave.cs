﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerDragonKnightCave
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UI;
using UnityEngine;

public class BuildingControllerDragonKnightCave : BuildingControllerNew
{
  private const string TIMER_BAR_PATH = "Prefab/City/DKMopupTimerBar";
  private const string OPEN_VFX = "Prefab/VFX/fx_dragon_rider";
  private long _vfxhandle;
  private GameObject _raidTimer;

  private void OnEnable()
  {
    this.AddEventHandler();
    this.OnDragonKnightChanged((DragonKnightData) null);
    this.OnJobCreated(DragonKnightUtils.DungeonRaidJobId);
  }

  private void OnDisable()
  {
    this.RemoveEventHandler();
  }

  public override void InitBuilding()
  {
    base.InitBuilding();
  }

  public override void Dispose()
  {
    base.Dispose();
  }

  private void OnDragonKnightChanged(DragonKnightData obj)
  {
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (dragonKnightData == null)
      return;
    if (dragonKnightData.CurrentDungeonEndTime >= NetServerTime.inst.ServerTimestamp || !NetServerTime.inst.IsToday((double) dragonKnightData.CurrentDungeonEndTime))
    {
      if (this._vfxhandle != 0L)
        return;
      this._vfxhandle = VfxManager.Instance.CreateAndPlay("Prefab/VFX/fx_dragon_rider", this.transform);
    }
    else
    {
      if (this._vfxhandle == 0L)
        return;
      VfxManager.Instance.Delete(this._vfxhandle);
      this._vfxhandle = 0L;
    }
  }

  private void OnBuildingSelected()
  {
    this.EnterDragonKightCave();
  }

  private void EnterDragonKightCave()
  {
    AudioManager.Instance.StopAndPlaySound("sfx_city_click_dragon_knight_cave");
    if (DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L) == null)
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_dragon_knight_not_summoned_yet", true), (System.Action) null, 4f, true);
      BuildingControllerDragonKnight componentInChildren = CitadelSystem.inst.BuildingRoot.GetComponentInChildren<BuildingControllerDragonKnight>();
      if (!((UnityEngine.Object) null != (UnityEngine.Object) componentInChildren))
        return;
      LinkerHub.Instance.MountHint(componentInChildren.transform, 0, (object) null);
    }
    else
      DragonKnightUtils.ShowDungeonScene();
  }

  private void OnDragonKnightDungeonReset(object data)
  {
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (dragonKnightData == null || dragonKnightData.CurrentDungeonEndTime > NetServerTime.inst.ServerTimestamp)
      return;
    UIManager.inst.toast.Show(Utils.XLAT("toast_dragon_knight_dungeon_cooldown_complete"), (System.Action) null, 4f, true);
  }

  private void OnJobCreated(long jobId)
  {
    JobHandle job = JobManager.Instance.GetJob(jobId);
    if (job == null || job.GetJobEvent() != JobEvent.JOB_DRAGON_KNIGHT_DUNGEON_RAID || (bool) ((UnityEngine.Object) this._raidTimer))
      return;
    this._raidTimer = AssetManager.Instance.Load("Prefab/City/DKMopupTimerBar", (System.Type) null) as GameObject;
    this._raidTimer = Utils.DuplicateGOB(this._raidTimer, this.transform);
    AssetManager.Instance.UnLoadAsset("Prefab/City/DKMopupTimerBar", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    DKMopupTimerBar[] componentsInChildren = this._raidTimer.GetComponentsInChildren<DKMopupTimerBar>(true);
    if (componentsInChildren.Length <= 0)
      return;
    componentsInChildren[0].SetJob(job);
  }

  private void OnJobRemove(long jobId)
  {
    JobHandle job = JobManager.Instance.GetJob(jobId);
    if (job == null || job.GetJobEvent() != JobEvent.JOB_DRAGON_KNIGHT_DUNGEON_RAID || !(bool) ((UnityEngine.Object) this._raidTimer))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this._raidTimer);
    this._raidTimer = (GameObject) null;
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_DragonKnight.onDataChanged += new System.Action<DragonKnightData>(this.OnDragonKnightChanged);
    JobManager.Instance.OnJobCreated += new System.Action<long>(this.OnJobCreated);
    JobManager.Instance.OnJobRemove += new System.Action<long>(this.OnJobRemove);
    this.onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    MessageHub.inst.GetPortByAction("dragon_knight_dungeon_time_out").AddEvent(new System.Action<object>(this.OnDragonKnightDungeonReset));
  }

  private void RemoveEventHandler()
  {
    if (DBManager.inst.Initialized)
      DBManager.inst.DB_DragonKnight.onDataChanged -= new System.Action<DragonKnightData>(this.OnDragonKnightChanged);
    JobManager.Instance.OnJobCreated -= new System.Action<long>(this.OnJobCreated);
    JobManager.Instance.OnJobRemove -= new System.Action<long>(this.OnJobRemove);
    this.onBuildingSelected -= new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    MessageHub.inst.GetPortByAction("dragon_knight_dungeon_time_out").RemoveEvent(new System.Action<object>(this.OnDragonKnightDungeonReset));
  }
}
