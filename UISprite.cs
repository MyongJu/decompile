﻿// Decompiled with JetBrains decompiler
// Type: UISprite
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[AddComponentMenu("NGUI/UI/NGUI Sprite")]
[ExecuteInEditMode]
public class UISprite : UIBasicSprite
{
  [SerializeField]
  [HideInInspector]
  private bool mFillCenter = true;
  [HideInInspector]
  [SerializeField]
  private UIAtlas mAtlas;
  [SerializeField]
  [HideInInspector]
  private string mSpriteName;
  [NonSerialized]
  protected UISpriteData mSprite;
  [NonSerialized]
  private bool mSpriteSet;

  public override Material material
  {
    get
    {
      if ((UnityEngine.Object) this.mAtlas != (UnityEngine.Object) null)
        return this.mAtlas.spriteMaterial;
      return (Material) null;
    }
  }

  public UIAtlas atlas
  {
    get
    {
      return this.mAtlas;
    }
    set
    {
      if (!((UnityEngine.Object) this.mAtlas != (UnityEngine.Object) value))
        return;
      this.RemoveFromPanel();
      this.mAtlas = value;
      this.mSpriteSet = false;
      this.mSprite = (UISpriteData) null;
      if (string.IsNullOrEmpty(this.mSpriteName) && (UnityEngine.Object) this.mAtlas != (UnityEngine.Object) null && this.mAtlas.spriteList.Count > 0)
      {
        this.SetAtlasSprite(this.mAtlas.spriteList[0]);
        this.mSpriteName = this.mSprite.name;
      }
      if (string.IsNullOrEmpty(this.mSpriteName))
        return;
      string mSpriteName = this.mSpriteName;
      this.mSpriteName = string.Empty;
      this.spriteName = mSpriteName;
      this.MarkAsChanged();
    }
  }

  public string spriteName
  {
    get
    {
      return this.mSpriteName;
    }
    set
    {
      if (string.IsNullOrEmpty(value))
      {
        if (string.IsNullOrEmpty(this.mSpriteName))
          return;
        this.mSpriteName = string.Empty;
        this.mSprite = (UISpriteData) null;
        this.mChanged = true;
        this.mSpriteSet = false;
      }
      else
      {
        if (!(this.mSpriteName != value))
          return;
        this.mSpriteName = value;
        this.mSprite = (UISpriteData) null;
        this.mChanged = true;
        this.mSpriteSet = false;
      }
    }
  }

  public bool isValid
  {
    get
    {
      return this.GetAtlasSprite() != null;
    }
  }

  [Obsolete("Use 'centerType' instead")]
  public bool fillCenter
  {
    get
    {
      return this.centerType != UIBasicSprite.AdvancedType.Invisible;
    }
    set
    {
      if (value == (this.centerType != UIBasicSprite.AdvancedType.Invisible))
        return;
      this.centerType = !value ? UIBasicSprite.AdvancedType.Invisible : UIBasicSprite.AdvancedType.Sliced;
      this.MarkAsChanged();
    }
  }

  public override Vector4 border
  {
    get
    {
      UISpriteData atlasSprite = this.GetAtlasSprite();
      if (atlasSprite == null)
        return base.border;
      return new Vector4((float) atlasSprite.borderLeft, (float) atlasSprite.borderBottom, (float) atlasSprite.borderRight, (float) atlasSprite.borderTop);
    }
  }

  public override float pixelSize
  {
    get
    {
      if ((UnityEngine.Object) this.mAtlas != (UnityEngine.Object) null)
        return this.mAtlas.pixelSize;
      return 1f;
    }
  }

  public override int minWidth
  {
    get
    {
      if (this.type != UIBasicSprite.Type.Sliced && this.type != UIBasicSprite.Type.Advanced)
        return base.minWidth;
      Vector4 vector4 = this.border * this.pixelSize;
      int num = Mathf.RoundToInt(vector4.x + vector4.z);
      UISpriteData atlasSprite = this.GetAtlasSprite();
      if (atlasSprite != null)
        num += atlasSprite.paddingLeft + atlasSprite.paddingRight;
      return Mathf.Max(base.minWidth, (num & 1) != 1 ? num : num + 1);
    }
  }

  public override int minHeight
  {
    get
    {
      if (this.type != UIBasicSprite.Type.Sliced && this.type != UIBasicSprite.Type.Advanced)
        return base.minHeight;
      Vector4 vector4 = this.border * this.pixelSize;
      int num = Mathf.RoundToInt(vector4.y + vector4.w);
      UISpriteData atlasSprite = this.GetAtlasSprite();
      if (atlasSprite != null)
        num += atlasSprite.paddingTop + atlasSprite.paddingBottom;
      return Mathf.Max(base.minHeight, (num & 1) != 1 ? num : num + 1);
    }
  }

  public override Vector4 drawingDimensions
  {
    get
    {
      Vector2 pivotOffset = this.pivotOffset;
      float a1 = -pivotOffset.x * (float) this.mWidth;
      float a2 = -pivotOffset.y * (float) this.mHeight;
      float b1 = a1 + (float) this.mWidth;
      float b2 = a2 + (float) this.mHeight;
      if (this.GetAtlasSprite() != null && this.mType != UIBasicSprite.Type.Tiled)
      {
        int paddingLeft = this.mSprite.paddingLeft;
        int paddingBottom = this.mSprite.paddingBottom;
        int paddingRight = this.mSprite.paddingRight;
        int paddingTop = this.mSprite.paddingTop;
        int num1 = this.mSprite.width + paddingLeft + paddingRight;
        int num2 = this.mSprite.height + paddingBottom + paddingTop;
        float num3 = 1f;
        float num4 = 1f;
        if (num1 > 0 && num2 > 0 && (this.mType == UIBasicSprite.Type.Simple || this.mType == UIBasicSprite.Type.Filled))
        {
          if ((num1 & 1) != 0)
            ++paddingRight;
          if ((num2 & 1) != 0)
            ++paddingTop;
          num3 = 1f / (float) num1 * (float) this.mWidth;
          num4 = 1f / (float) num2 * (float) this.mHeight;
        }
        if (this.mFlip == UIBasicSprite.Flip.Horizontally || this.mFlip == UIBasicSprite.Flip.Both)
        {
          a1 += (float) paddingRight * num3;
          b1 -= (float) paddingLeft * num3;
        }
        else
        {
          a1 += (float) paddingLeft * num3;
          b1 -= (float) paddingRight * num3;
        }
        if (this.mFlip == UIBasicSprite.Flip.Vertically || this.mFlip == UIBasicSprite.Flip.Both)
        {
          a2 += (float) paddingTop * num4;
          b2 -= (float) paddingBottom * num4;
        }
        else
        {
          a2 += (float) paddingBottom * num4;
          b2 -= (float) paddingTop * num4;
        }
      }
      Vector4 vector4 = !((UnityEngine.Object) this.mAtlas != (UnityEngine.Object) null) ? Vector4.zero : this.border * this.pixelSize;
      float num5 = vector4.x + vector4.z;
      float num6 = vector4.y + vector4.w;
      return new Vector4(Mathf.Lerp(a1, b1 - num5, this.mDrawRegion.x), Mathf.Lerp(a2, b2 - num6, this.mDrawRegion.y), Mathf.Lerp(a1 + num5, b1, this.mDrawRegion.z), Mathf.Lerp(a2 + num6, b2, this.mDrawRegion.w));
    }
  }

  public override bool premultipliedAlpha
  {
    get
    {
      if ((UnityEngine.Object) this.mAtlas != (UnityEngine.Object) null)
        return this.mAtlas.premultipliedAlpha;
      return false;
    }
  }

  public UISpriteData GetAtlasSprite()
  {
    if (!this.mSpriteSet)
      this.mSprite = (UISpriteData) null;
    if (this.mSprite == null && (UnityEngine.Object) this.mAtlas != (UnityEngine.Object) null)
    {
      if (!string.IsNullOrEmpty(this.mSpriteName))
      {
        UISpriteData sprite = this.mAtlas.GetSprite(this.mSpriteName);
        if (sprite == null)
          return (UISpriteData) null;
        this.SetAtlasSprite(sprite);
      }
      if (this.mSprite == null && this.mAtlas.spriteList.Count > 0)
      {
        UISpriteData sprite = this.mAtlas.spriteList[0];
        if (sprite == null)
          return (UISpriteData) null;
        this.SetAtlasSprite(sprite);
        if (this.mSprite == null)
        {
          Debug.LogError((object) (this.mAtlas.name + " seems to have a null sprite!"));
          return (UISpriteData) null;
        }
        this.mSpriteName = this.mSprite.name;
      }
    }
    return this.mSprite;
  }

  protected void SetAtlasSprite(UISpriteData sp)
  {
    this.mChanged = true;
    this.mSpriteSet = true;
    if (sp != null)
    {
      this.mSprite = sp;
      this.mSpriteName = this.mSprite.name;
    }
    else
    {
      this.mSpriteName = this.mSprite == null ? string.Empty : this.mSprite.name;
      this.mSprite = sp;
    }
  }

  public override void MakePixelPerfect()
  {
    if (!this.isValid)
      return;
    base.MakePixelPerfect();
    if (this.mType == UIBasicSprite.Type.Tiled)
      return;
    UISpriteData atlasSprite = this.GetAtlasSprite();
    if (atlasSprite == null)
      return;
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null || this.mType != UIBasicSprite.Type.Simple && this.mType != UIBasicSprite.Type.Filled && atlasSprite.hasBorder || !((UnityEngine.Object) mainTexture != (UnityEngine.Object) null))
      return;
    int num1 = Mathf.RoundToInt(this.pixelSize * (float) (atlasSprite.width + atlasSprite.paddingLeft + atlasSprite.paddingRight));
    int num2 = Mathf.RoundToInt(this.pixelSize * (float) (atlasSprite.height + atlasSprite.paddingTop + atlasSprite.paddingBottom));
    if ((num1 & 1) == 1)
      ++num1;
    if ((num2 & 1) == 1)
      ++num2;
    this.width = num1;
    this.height = num2;
  }

  protected override void OnInit()
  {
    if (!this.mFillCenter)
    {
      this.mFillCenter = true;
      this.centerType = UIBasicSprite.AdvancedType.Invisible;
    }
    base.OnInit();
  }

  protected override void OnUpdate()
  {
    base.OnUpdate();
    if (!this.mChanged && this.mSpriteSet)
      return;
    this.mSpriteSet = true;
    this.mSprite = (UISpriteData) null;
    this.mChanged = true;
  }

  public override void OnFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols)
  {
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null)
      return;
    if (this.mSprite == null)
      this.mSprite = this.atlas.GetSprite(this.spriteName);
    if (this.mSprite == null)
      return;
    Rect rect1 = new Rect((float) this.mSprite.x, (float) this.mSprite.y, (float) this.mSprite.width, (float) this.mSprite.height);
    Rect rect2 = new Rect((float) (this.mSprite.x + this.mSprite.borderLeft), (float) (this.mSprite.y + this.mSprite.borderTop), (float) (this.mSprite.width - this.mSprite.borderLeft - this.mSprite.borderRight), (float) (this.mSprite.height - this.mSprite.borderBottom - this.mSprite.borderTop));
    Rect texCoords1 = NGUIMath.ConvertToTexCoords(rect1, mainTexture.width, mainTexture.height);
    Rect texCoords2 = NGUIMath.ConvertToTexCoords(rect2, mainTexture.width, mainTexture.height);
    int size = verts.size;
    this.Fill(verts, uvs, cols, texCoords1, texCoords2);
    if (this.onPostFill == null)
      return;
    this.onPostFill((UIWidget) this, size, verts, uvs, cols);
  }
}
