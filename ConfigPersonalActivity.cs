﻿// Decompiled with JetBrains decompiler
// Type: ConfigPersonalActivity
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigPersonalActivity
{
  private ConfigParse parse = new ConfigParse();
  private List<PersonalActivityReward> list = new List<PersonalActivityReward>();
  private Dictionary<string, PersonalActivityReward> datas;
  private Dictionary<int, PersonalActivityReward> dicByUniqueId;
  private Dictionary<int, PersonalActivityReward> dicByMainInternalID;
  private Dictionary<string, PersonalActivityReward> rewardInfoMap;

  public void BuildDB(object res)
  {
    this.parse.Parse<PersonalActivityReward, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.list.Clear();
    Dictionary<int, PersonalActivityReward>.KeyCollection.Enumerator enumerator = this.dicByUniqueId.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (this.dicByUniqueId[enumerator.Current].IsOpen)
        this.list.Add(this.dicByUniqueId[enumerator.Current]);
    }
    this.list.Sort(new Comparison<PersonalActivityReward>(this.Compare));
  }

  public List<PersonalActivityReward> GetToatalRewards()
  {
    return this.list;
  }

  private int Compare(PersonalActivityReward a, PersonalActivityReward b)
  {
    return a.internalId.CompareTo(b.internalId);
  }

  public PersonalActivityReward GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (PersonalActivityReward) null;
  }

  public PersonalActivityReward GetCurrentRewardByStrongholderLevel()
  {
    int num1 = 4;
    int num2 = -1;
    for (int index = 0; index < this.list.Count; ++index)
    {
      if (this.list[index].StrongholdLevel > PlayerData.inst.CityData.mStronghold.mLevel)
      {
        num2 = index;
        break;
      }
    }
    if (num2 == -1)
      return this.list[this.list.Count - num1];
    int num3 = num2 % num1;
    int index1 = num2 / num1 * num1;
    if (index1 < this.list.Count)
      return this.list[index1];
    return (PersonalActivityReward) null;
  }

  public PersonalActivityReward GetPreRewards(int level)
  {
    int index1 = -1;
    for (int index2 = 0; index2 < this.list.Count; ++index2)
    {
      if (this.list[index2].StrongholdLevel == level)
      {
        index1 = index2 - 1;
        break;
      }
    }
    if (index1 > -1 && index1 < this.list.Count)
      return this.list[index1];
    return (PersonalActivityReward) null;
  }

  public List<PersonalActivityReward> GetCurrentRewards(int interalId, int offset = 5)
  {
    int count = this.list.Count;
    PersonalActivityReward personalActivityReward = this.GetData(interalId);
    bool flag = false;
    if (personalActivityReward != null)
      flag = true;
    List<PersonalActivityReward> personalActivityRewardList = new List<PersonalActivityReward>();
    int num1 = 0;
    if (personalActivityReward == null)
      personalActivityReward = this.GetCurrentRewardByStrongholderLevel();
    if (personalActivityReward != null)
      num1 = this.list.IndexOf(personalActivityReward);
    int num2 = !flag ? num1 : num1 + 1;
    int num3 = num2 + offset;
    if (num3 > this.list.Count)
    {
      for (int index = num2; index < this.list.Count; ++index)
        personalActivityRewardList.Add(this.list[index]);
      return personalActivityRewardList;
    }
    for (int index = num3 - 1; this.list[index].StrongholdLevel <= PlayerData.inst.CityData.mStronghold.mLevel; index = num3 - 1)
    {
      num2 = index;
      num3 = num2 + offset;
      if (num3 > this.list.Count)
      {
        num3 = this.list.Count;
        num2 = num3 - offset;
        if (num2 < 0)
        {
          num2 = 0;
          break;
        }
        break;
      }
    }
    for (int index = num2; index < num3; ++index)
      personalActivityRewardList.Add(this.list[index]);
    return personalActivityRewardList;
  }

  public PersonalActivityReward GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (PersonalActivityReward) null;
  }

  public PersonalActivityReward GetDataByMainInternalID(int internalID)
  {
    return (PersonalActivityReward) null;
  }

  public PersonalActivityReward GetCurrentStepReward(int id, int lev)
  {
    return (PersonalActivityReward) null;
  }

  public PersonalActivityReward GetDataByID(string id)
  {
    if (this.rewardInfoMap == null)
    {
      this.rewardInfoMap = new Dictionary<string, PersonalActivityReward>();
      using (Dictionary<int, PersonalActivityReward>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, PersonalActivityReward> current = enumerator.Current;
          this.rewardInfoMap.Add(current.Value.ID, current.Value);
        }
      }
    }
    return this.rewardInfoMap[id];
  }
}
