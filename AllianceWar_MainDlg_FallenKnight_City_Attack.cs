﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_FallenKnight_City_Attack
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class AllianceWar_MainDlg_FallenKnight_City_Attack : AllianceWar_MainDlg_March_Slot
{
  protected override void SetLeftItems(MarchData marchData, UserData userData, Coordinate location)
  {
    NPC_Info dataByUid = ConfigManager.inst.DB_NPC.GetDataByUid(marchData.ownerUid);
    if (dataByUid != null && userData != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.leftIcon, dataByUid.icon, (System.Action<bool>) null, true, false, string.Empty);
      this.panel.leftName.text = dataByUid.LOC_Name;
      CustomIconLoader.Instance.requestCustomIcon(this.panel.leftIcon, userData.PortraitIconPath, userData.Icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.panel.leftIcon, userData.LordTitle, 27);
    }
    this.panel.leftLocation.text = string.Format("X:{0}, Y:{1}", (object) location.X, (object) location.Y);
    this.data.leftLocation = location;
  }
}
