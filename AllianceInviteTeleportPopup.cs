﻿// Decompiled with JetBrains decompiler
// Type: AllianceInviteTeleportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UI;

public class AllianceInviteTeleportPopup : Popup
{
  private AllianceInviteTeleportPopup.Parameter m_Parameter;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as AllianceInviteTeleportPopup.Parameter;
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnNo()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnYes()
  {
    this.OnNo();
    Hashtable postData = new Hashtable();
    postData[(object) "invite_uid"] = (object) this.m_Parameter.userData.uid;
    MessageHub.inst.GetPortByAction("Alliance:inviteTeleport").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_teleport_invite_sent", true), (System.Action) null, 4f, false);
    }), true);
  }

  public class Parameter : Popup.PopupParameter
  {
    public UserData userData;
  }
}
