﻿// Decompiled with JetBrains decompiler
// Type: BuildingGlorySlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BuildingGlorySlot : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelBenefitName;
  [SerializeField]
  private UILabel _labelCurBenefitValue;
  [SerializeField]
  private UILabel _labelNextBenefitValue;
  [SerializeField]
  private UISprite _spriteArrow;
  private int _benefitId;
  private float _curBenefitValue;
  private float _nextBenefitValue;
  private bool _isMaxGlory;

  public void SetData(int benefitId, float curBenefitValue, float nextBenefitValue, bool isMaxGlory)
  {
    this._benefitId = benefitId;
    this._curBenefitValue = curBenefitValue;
    this._nextBenefitValue = nextBenefitValue;
    this._isMaxGlory = isMaxGlory;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[this._benefitId];
    string str1 = dbProperty == null ? "unknow" : dbProperty.Name;
    string str2 = dbProperty == null ? this._curBenefitValue.ToString() : dbProperty.ConvertToDisplayString((double) this._curBenefitValue, false, true);
    string str3 = dbProperty == null ? this._nextBenefitValue.ToString() : dbProperty.ConvertToDisplayString((double) this._nextBenefitValue, false, true);
    this._labelBenefitName.text = str1;
    this._labelCurBenefitValue.text = str2;
    this._labelNextBenefitValue.text = str3;
    NGUITools.SetActive(this._labelNextBenefitValue.gameObject, !this._isMaxGlory);
    NGUITools.SetActive(this._spriteArrow.gameObject, !this._isMaxGlory);
  }
}
