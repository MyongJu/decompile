﻿// Decompiled with JetBrains decompiler
// Type: PopupScoutDetails
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupScoutDetails : MonoBehaviour
{
  private List<TroopInfoBar> _troopEntries = new List<TroopInfoBar>();
  private Dictionary<Transform, UIRect> _cachedRects = new Dictionary<Transform, UIRect>();
  public PopupScoutReport parent;
  public TroopInfoBar template;
  public UILabel defendingTroopCount;
  public UILabel reinforcingTroopCount;
  public UILabel wallTrapCount;
  public UILabel warRalliedTroopCount;
  public UITable defenderTable;
  public UITable reinforcerTable;
  public UITable wallTrapTable;
  public UITable warRallyTable;
  public Transform defenderListTransform;
  public Transform wallTrapListTransform;
  public Transform reinforcementListTransform;
  public Transform warRallyListTransform;
  public EnvelopContent contentEnveloperDefend;
  public EnvelopContent contentEnveloperReinforce;
  public EnvelopContent contentEnveloperWallTrap;
  public EnvelopContent contentEnveloperWarRally;
  public UIScrollView scrollView;
  private Hashtable _data;
  private bool _shown;

  public void ApplyData(Hashtable data)
  {
    this._data = data;
    if (!this._shown)
      return;
    this.DestroyOldData();
    this.GenerateTroopEntries();
  }

  public void Show()
  {
    this._shown = true;
    this.DestroyOldData();
    this.GenerateTroopEntries();
    this.gameObject.SetActive(true);
  }

  public void Hide()
  {
    this._shown = false;
    this.DestroyOldData();
    this.gameObject.SetActive(false);
  }

  public void OnClosePressed()
  {
    this.OnBackPressed();
    this.parent.OnClosePressed();
  }

  public void OnBackPressed()
  {
    this.Hide();
  }

  private void GenerateTroopEntries()
  {
    Hashtable hashtable = this._data[(object) "troops"] as Hashtable;
    if (hashtable == null)
      return;
    Hashtable troopList1 = hashtable[(object) "defending_troops"] as Hashtable;
    if (troopList1 != null)
      this.ProcessTroopsForTable(troopList1, this.defendingTroopCount, this.defenderTable, this.defenderListTransform);
    Hashtable troopList2 = hashtable[(object) "reinforcing_troops"] as Hashtable;
    if (troopList2 != null)
      this.ProcessTroopsForTable(troopList2, this.reinforcingTroopCount, this.reinforcerTable, this.reinforcementListTransform);
    Hashtable troopList3 = hashtable[(object) "traps"] as Hashtable;
    if (troopList3 != null)
      this.ProcessTroopsForTable(troopList3, this.wallTrapCount, this.wallTrapTable, this.wallTrapListTransform);
    Hashtable troopList4 = hashtable[(object) "war_rallied_troops"] as Hashtable;
    if (troopList4 != null)
      this.ProcessTroopsForTable(troopList4, this.warRalliedTroopCount, this.warRallyTable, this.warRallyListTransform);
    this.defenderTable.onReposition += new UITable.OnReposition(this.OnReposition);
    this.reinforcerTable.onReposition += new UITable.OnReposition(this.OnReposition);
    this.wallTrapTable.onReposition += new UITable.OnReposition(this.OnReposition);
    this.warRallyTable.onReposition += new UITable.OnReposition(this.OnReposition);
  }

  private void OnReposition()
  {
    this.contentEnveloperDefend.Execute();
    this.contentEnveloperReinforce.Execute();
    this.contentEnveloperWallTrap.Execute();
    this.contentEnveloperWarRally.Execute();
    this.scrollView.ResetPosition();
  }

  private void ProcessTroopsForTable(Hashtable troopList, UILabel labelToSet, UITable table, Transform anchor)
  {
    int num = 0;
    if (!troopList.ContainsKey((object) "type") && !troopList.ContainsKey((object) "amount"))
    {
      IEnumerator enumerator = troopList.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current as string;
        if (!current.Contains("_t1"))
          current += "_t1";
        TroopInfo troopInfo = TroopInfoManager.Instance.GetTroopInfo(current);
        int troop = (int) troopList[enumerator.Current];
        this.ApplyTroopEntryToTable(table, troopInfo, anchor, troop);
        num += troop;
      }
    }
    else
    {
      if (troopList.ContainsKey((object) "amount"))
        num = int.Parse(troopList[(object) "amount"] as string);
      if (troopList.ContainsKey((object) "type"))
      {
        foreach (object obj in troopList[(object) "type"] as ArrayList)
        {
          string key = obj as string;
          if (!key.Contains("_t1"))
            key += "_t1";
          TroopInfo troopInfo = TroopInfoManager.Instance.GetTroopInfo(key);
          int count = 0;
          this.ApplyTroopEntryToTable(table, troopInfo, anchor, count);
        }
      }
    }
    if (num <= 0)
      return;
    labelToSet.text = num.ToString();
    anchor.gameObject.SetActive(true);
  }

  private void ApplyTroopEntryToTable(UITable table, TroopInfo info, Transform anchor, int count)
  {
    GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.template.gameObject, table.gameObject.transform);
    TroopInfoBar component1 = gameObject.GetComponent<TroopInfoBar>();
    UISprite component2 = gameObject.GetComponent<UISprite>();
    if (!this._cachedRects.ContainsKey(anchor))
    {
      UIRect component3 = anchor.GetComponent<UIRect>();
      this._cachedRects.Add(anchor, component3);
    }
    component2.leftAnchor = this._cachedRects[anchor].leftAnchor;
    component2.rightAnchor = this._cachedRects[anchor].rightAnchor;
    component1.SetData(info, count, 0);
    gameObject.SetActive(true);
    this._troopEntries.Add(component1);
    table.repositionNow = true;
  }

  private void DestroyOldData()
  {
    for (int index = 0; index < this._troopEntries.Count; ++index)
    {
      GameObject gameObject = this._troopEntries[index].gameObject;
      gameObject.SetActive(false);
      PrefabManagerEx.Instance.Destroy(gameObject);
    }
    this._troopEntries.Clear();
    this.defenderTable.onReposition -= new UITable.OnReposition(this.OnReposition);
    this.reinforcerTable.onReposition -= new UITable.OnReposition(this.OnReposition);
    this.wallTrapTable.onReposition -= new UITable.OnReposition(this.OnReposition);
    this.warRallyTable.onReposition -= new UITable.OnReposition(this.OnReposition);
    this.defenderListTransform.gameObject.SetActive(false);
    this.reinforcementListTransform.gameObject.SetActive(false);
    this.wallTrapListTransform.gameObject.SetActive(false);
    this.warRallyListTransform.gameObject.SetActive(false);
  }
}
