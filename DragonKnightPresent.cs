﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightPresent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class DragonKnightPresent : BasePresent
{
  public override void Refresh()
  {
    if (this.Formula == "rank")
    {
      StatsData statsData = DBManager.inst.DB_Stats.Get("dk_rank_achievement");
      int count = this.Count;
      int num1 = 0;
      if (statsData != null)
        num1 = (int) statsData.count;
      int num2 = num1 > count || num1 == 0 ? 0 : count;
      this.ProgressValue = (float) num2 / (float) count;
      this.ProgressContent = num2.ToString() + "/" + (object) count;
    }
    else
      this.UseStats();
  }
}
