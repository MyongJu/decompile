﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarRankData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class AllianceWarRankData
{
  private List<AllianceWarRankItemData> _allRankItem = new List<AllianceWarRankItemData>();
  private int _startTime;
  private long _score;

  public long Score
  {
    get
    {
      return this._score;
    }
  }

  public int StartTime
  {
    get
    {
      return this._startTime;
    }
  }

  public List<AllianceWarRankItemData> AllRankItemData
  {
    get
    {
      return this._allRankItem;
    }
  }

  public void Decode(object data)
  {
    this._allRankItem.Clear();
    Hashtable inData = data as Hashtable;
    if (inData == null)
      return;
    DatabaseTools.UpdateData(inData, "start_time", ref this._startTime);
    DatabaseTools.UpdateData(inData, "score", ref this._score);
    ArrayList arrayList = inData[(object) "rank"] as ArrayList;
    if (arrayList == null)
      return;
    int num = 1;
    foreach (object obj in arrayList)
    {
      Hashtable data1 = obj as Hashtable;
      if (data1 != null)
      {
        AllianceWarRankItemData allianceWarRankItemData = new AllianceWarRankItemData();
        allianceWarRankItemData.Decode(data1);
        allianceWarRankItemData.Rank = num;
        ++num;
        this._allRankItem.Add(allianceWarRankItemData);
      }
    }
  }
}
