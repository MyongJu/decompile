﻿// Decompiled with JetBrains decompiler
// Type: PVEMonsterTile
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class PVEMonsterTile : PVETile
{
  public Transform m_RootTransform;
  private GameObject m_Model;
  private SkinnedMeshRenderer[] m_SkinnedMeshRenderers;
  private PrefabSpawnRequestEx m_Request;
  private System.Action<GameObject, object> m_Callback;
  private object m_UserData;
  private bool m_Changed;
  private float m_Rotation;
  private bool m_InitRotation;

  private void InitRotationOnce()
  {
    if (this.m_InitRotation)
      return;
    this.m_InitRotation = true;
    UnityEngine.Random.seed = (int) DateTime.UtcNow.Ticks;
    this.m_Rotation = (double) UnityEngine.Random.Range(0.0f, 1f) >= 0.600000023841858 ? -180f : -90f;
  }

  public override void OnInitialize()
  {
    base.OnInitialize();
    this.InitRotationOnce();
  }

  public override void OnFinalize()
  {
    base.OnFinalize();
    this.m_Changed = false;
    if ((UnityEngine.Object) this.m_Model != (UnityEngine.Object) null)
    {
      PrefabManagerEx.Instance.Destroy(this.m_Model);
      this.m_Model = (GameObject) null;
      this.m_SkinnedMeshRenderers = (SkinnedMeshRenderer[]) null;
    }
    if (this.m_Request != null)
    {
      this.m_Request.cancel = true;
      this.m_Request = (PrefabSpawnRequestEx) null;
    }
    this.m_Callback = (System.Action<GameObject, object>) null;
    this.m_UserData = (object) null;
  }

  public void CreateModel(string type, System.Action<GameObject, object> callback = null, object data = null)
  {
    this.OnFinalize();
    this.m_Callback = callback;
    this.m_UserData = data;
    PrefabManagerEx.Instance.AddRouteMapping(type, ConfigWorldEncounter.GetMonsterPrefabPath(type));
    this.m_Request = PrefabManagerEx.Instance.SpawnAsync(type, this.m_RootTransform, new PrefabSpawnRequestCallback(this.OnLoaded), (object) null);
  }

  private void OnLoaded(GameObject go, object userData)
  {
    this.m_Request = (PrefabSpawnRequestEx) null;
    this.m_Model = go;
    this.m_Model.transform.localPosition = Vector3.zero;
    this.m_Model.transform.localRotation = Quaternion.Euler(0.0f, this.m_Rotation, 0.0f);
    this.m_Model.transform.localScale = Vector3.one;
    this.m_SkinnedMeshRenderers = this.m_Model.GetComponentsInChildren<SkinnedMeshRenderer>();
    foreach (MonsterAnimEvent componentsInChild in go.GetComponentsInChildren<MonsterAnimEvent>(true))
      componentsInChild.Reset();
    if (this.m_Callback == null)
      return;
    this.m_Callback(go, this.m_UserData);
    this.m_Callback = (System.Action<GameObject, object>) null;
    this.m_UserData = (object) null;
  }

  protected override void UpdateSortingLayer()
  {
    base.UpdateSortingLayer();
    if (this.m_Request != null || !this.m_Changed || !(bool) ((UnityEngine.Object) this.m_Model))
      return;
    this.m_Changed = false;
    if (this.m_SkinnedMeshRenderers == null)
      return;
    int sortingLayerId = this.calculator.GetSortingLayerID();
    int length = this.m_SkinnedMeshRenderers.Length;
    for (int index = 0; index < length; ++index)
      this.m_SkinnedMeshRenderers[index].sortingLayerID = sortingLayerId;
  }

  public override void SetSortingLayerID(ISortingLayerCalculator calculator)
  {
    base.SetSortingLayerID(calculator);
    this.m_Changed = true;
  }
}
