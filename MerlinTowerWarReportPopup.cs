﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerWarReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTowerWarReportPopup : WarReportPopup
{
  private string _vfxName = string.Empty;
  private const string PVP_RESULT_WIN_VFX = "fx_AnniversaryKingWarPopup_win";
  private const string VFX_PATH = "Prefab/Anniversary/";
  [SerializeField]
  private UILabel _labelBattleResult;
  [SerializeField]
  private UILabel _labelBattleResultDesc;
  [SerializeField]
  private UITexture _textureBattleResult;
  [SerializeField]
  private UITexture _textureBattleFrame;
  [SerializeField]
  private GameObject _rootMerlinTowerAttackReward;
  [SerializeField]
  private UILabel _labelMagicName;
  [SerializeField]
  private UILabel _labelMagicCount;
  [SerializeField]
  private UITexture _textureMagicCrystal;
  [SerializeField]
  private UISprite _spriteMagicFire;
  private Hashtable _mailData;
  private bool _winWar;
  private bool _openAfterBattle;
  private bool _isFightMonster;
  private GameObject _vfxObj;

  public override string Type
  {
    get
    {
      return "MerlinTower/MerlinTowerWarReportPopup";
    }
  }

  protected MagicTowerMainInfo MerlinTowerMainInfo
  {
    get
    {
      if (!this._isFightMonster || this.othertd == null)
        return (MagicTowerMainInfo) null;
      int result = 0;
      int.TryParse(this.othertd.uid, out result);
      return ConfigManager.inst.DB_MagicTowerMain.GetData(result);
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    MerlinTowerWarReportPopup.Parameter parameter = orgParam as MerlinTowerWarReportPopup.Parameter;
    if (parameter != null)
    {
      this._mailData = parameter.mailData;
      this._winWar = parameter.winWar;
      this._openAfterBattle = parameter.openAfterBattle;
      this._isFightMonster = parameter.isFightMonster;
      if (this._mailData != null)
      {
        AbstractMailEntry abstractMailEntry = new AbstractMailEntry();
        abstractMailEntry.ApplyData(this._mailData);
        parameter.hashtable = this._mailData;
        parameter.mail = abstractMailEntry;
      }
    }
    this._showLegend = false;
    this._isMerlinTowerFightMonster = this._isFightMonster;
    base.OnShow((UIControler.UIParameter) parameter);
    if (this._winWar)
      AudioManager.Instance.StopAndPlaySound("sfx_dragon_knight_pvp_win");
    else
      AudioManager.Instance.StopAndPlaySound("sfx_dragon_knight_pvp_lose");
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.DeleteVFX();
    this.CheckStatusAfterClose();
    if (!this._winWar)
      return;
    MagicTowerMainInfo currentTowerMainInfo = MerlinTowerPayload.Instance.UserData.CurrentTowerMainInfo;
    if (currentTowerMainInfo == null)
      return;
    UIManager.inst.OpenPopup("MerlinTower/MerlinTowerBuffStorePopup", (Popup.PopupParameter) new MerlinTowerBuffStorePopup.Parameter()
    {
      layer = currentTowerMainInfo.Level
    });
  }

  public void OnCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void CheckStatusAfterClose()
  {
  }

  private void UpdateUI()
  {
    this.title.text = Utils.XLAT("tower_battle_results_title");
    NGUITools.SetActive(this._rootMerlinTowerAttackReward, false);
    NGUITools.SetActive(this._textureMagicCrystal.gameObject, false);
    NGUITools.SetActive(this._spriteMagicFire.gameObject, false);
    if (this._winWar)
    {
      this._labelBattleResult.text = Utils.XLAT("dragon_knight_pvp_success");
      if (this._isFightMonster)
      {
        Dictionary<string, string> para = new Dictionary<string, string>();
        if (this.MerlinTowerMainInfo != null)
        {
          para.Add("0", this.MerlinTowerMainInfo.Magic.ToString());
          MagicTowerMainInfo dataWithLevel = ConfigManager.inst.DB_MagicTowerMain.GetDataWithLevel(this.MerlinTowerMainInfo.Level + 1);
          if (dataWithLevel != null)
            para.Add("1", dataWithLevel.MineLevel.ToString());
          else
            para.Add("1", this.MerlinTowerMainInfo.MineLevel.ToString());
          this._labelBattleResultDesc.text = ScriptLocalization.GetWithPara("tower_battle_victory_description", para, true);
          if (ConfigManager.inst.DB_Items.GetItem("item_magic_tower_coin_1") != null)
          {
            this._labelMagicName.text = Utils.XLAT("tower_mana_name");
            this._labelMagicCount.text = "+" + this.MerlinTowerMainInfo.Magic.ToString();
            NGUITools.SetActive(this._rootMerlinTowerAttackReward, true);
            NGUITools.SetActive(this._spriteMagicFire.gameObject, true);
          }
        }
      }
      else if (this.maildata.IsInMerlinTowerRevenge())
      {
        this._labelBattleResultDesc.text = Utils.XLAT("tower_loot_name");
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_magic_tower_coin_1");
        if (itemStaticInfo != null)
        {
          int result = 0;
          if (this._mailData != null && this._mailData[(object) "data"] != null)
          {
            Hashtable hashtable = this._mailData[(object) "data"] as Hashtable;
            if (hashtable != null && hashtable.ContainsKey((object) "crystal"))
              int.TryParse(hashtable[(object) "crystal"].ToString(), out result);
          }
          this._labelMagicName.text = itemStaticInfo.LocName;
          this._labelMagicCount.text = "+" + result.ToString();
          EquipmentManager.Instance.ConfigQualityLabelWithColor(this._labelMagicName, itemStaticInfo.Quality);
          NGUITools.SetActive(this._rootMerlinTowerAttackReward, true);
          NGUITools.SetActive(this._textureMagicCrystal.gameObject, true);
        }
      }
      else
        this._labelBattleResultDesc.text = Utils.XLAT("tower_battle_victory_subtitle");
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureBattleResult, "Texture/DragonKnight/DungeonTexture/dragon_knight_pvp_result_win", (System.Action<bool>) null, true, true, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureBattleFrame, "Texture/GUI_Textures/annv_winner", (System.Action<bool>) null, true, true, string.Empty);
      if (this._openAfterBattle)
      {
        this._vfxName = "fx_AnniversaryKingWarPopup_win";
        this.SetVFX(this._vfxName);
        AudioManager.Instance.PlaySound("sfx_dragon_knight_pvp_win", false);
      }
    }
    else
    {
      this._labelBattleResult.text = Utils.XLAT("dragon_knight_pvp_fail");
      if (this._isFightMonster || this.maildata.IsInMerlinTowerRevenge())
      {
        this._labelBattleResultDesc.text = Utils.XLAT("trial_challenge_fail_description");
      }
      else
      {
        Dictionary<string, string> para = new Dictionary<string, string>();
        GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("magic_tower_dressing_time_min");
        if (data != null)
        {
          para.Add("0", Utils.FormatTime(data.ValueInt, true, false, true));
          this._labelBattleResultDesc.text = ScriptLocalization.GetWithPara("tower_battle_lost_troops_resting_description", para, true);
        }
      }
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureBattleResult, "Texture/DragonKnight/DungeonTexture/dragon_knight_pvp_result_lose", (System.Action<bool>) null, true, true, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureBattleFrame, "Texture/GUI_Textures/annv_lost", (System.Action<bool>) null, true, true, string.Empty);
      if (this._openAfterBattle)
        AudioManager.Instance.PlaySound("sfx_dragon_knight_pvp_lose", false);
    }
    this.sv.ResetPosition();
  }

  private void SetVFX(string vfxUsedName)
  {
    if (string.IsNullOrEmpty(vfxUsedName))
      return;
    this._vfxObj = AssetManager.Instance.HandyLoad("Prefab/Anniversary/" + vfxUsedName, typeof (GameObject)) as GameObject;
    if (!((UnityEngine.Object) this._vfxObj != (UnityEngine.Object) null))
      return;
    this._vfxObj = UnityEngine.Object.Instantiate<GameObject>(this._vfxObj);
    this._vfxObj.transform.parent = this._textureBattleResult.transform;
    this._vfxObj.transform.localPosition = Vector3.zero;
    this._vfxObj.transform.localScale = Vector3.one;
  }

  private void DeleteVFX()
  {
    this._vfxName = string.Empty;
    if (!((UnityEngine.Object) this._vfxObj != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this._vfxObj);
    this._vfxObj = (GameObject) null;
  }

  public class Parameter : BaseReportPopup.Parameter
  {
    public Hashtable mailData;
    public bool winWar;
    public bool openAfterBattle;
    public bool isFightMonster;
    public int battleId;
  }
}
