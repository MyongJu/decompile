﻿// Decompiled with JetBrains decompiler
// Type: HeroRecruitDataGold
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class HeroRecruitDataGold : HeroRecruitData
{
  private const string ITEM_LUCKY_DRAW_2 = "item_parliament_hero_summon_coin";

  public override bool IsFirstUsed()
  {
    return (long) NetServerTime.inst.ServerTimestamp > Utils.DateTime2ServerTime(ConfigManager.inst.DB_ParliamentHeroSummonGroup.Get("3").activityEndTime, "_");
  }

  public override long GetFreeCD()
  {
    return 0;
  }

  public override string GetTip()
  {
    return Utils.XLAT("hero_summon_condition_purple_probability_description");
  }

  public override string GetTip2()
  {
    return Utils.XLAT("hero_summon_master_circle_tip");
  }

  public override long GetCurrency()
  {
    return (long) ItemBag.Instance.GetItemCount("item_parliament_hero_summon_coin");
  }

  public override long GetPrice()
  {
    return (long) ConfigManager.inst.DB_ParliamentHeroSummonGroup.Get("3").reqItemCount;
  }

  public override int GetLeftCD()
  {
    return 0;
  }

  public override bool IsFreeNow()
  {
    return false;
  }

  public override bool CanShowTimer()
  {
    return false;
  }

  public override int GetSummonType()
  {
    return 3;
  }

  public override void ShowStore(int price)
  {
    UIManager.inst.OpenPopup("UseItemCannotGoldPopup", (Popup.PopupParameter) new UseItemCannotGoldPopup.Parameter()
    {
      itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_parliament_hero_summon_coin"),
      btnText = Utils.XLAT("id_uppercase_get_more"),
      needvalue = price,
      contentText = Utils.XLAT("hero_summon_insufficient_items_description"),
      titleText = Utils.XLAT("id_uppercase_insufficient_items")
    });
  }

  public override void GotoResultDialog(HeroRecruitPayload payload)
  {
  }

  public override bool IsLocked
  {
    get
    {
      return this.LegendTemple.Locked;
    }
  }

  public override string GetIcon()
  {
    return "icon_hero_recruit_item";
  }

  public override string GetImagePath()
  {
    return "Texture/GUI_Textures/hero_card_recruit_3";
  }
}
