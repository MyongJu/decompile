﻿// Decompiled with JetBrains decompiler
// Type: Config_AllianceWarRewards
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class Config_AllianceWarRewards
{
  private Dictionary<string, AllianceRewardsInfo> m_DataByID;
  private Dictionary<int, AllianceRewardsInfo> m_DataByInternalID;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<AllianceRewardsInfo, string>(result as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
  }

  public AllianceRewardsInfo Get(string id)
  {
    AllianceRewardsInfo allianceRewardsInfo;
    this.m_DataByID.TryGetValue(id, out allianceRewardsInfo);
    return allianceRewardsInfo;
  }

  public AllianceRewardsInfo Get(int internalId)
  {
    AllianceRewardsInfo allianceRewardsInfo;
    this.m_DataByInternalID.TryGetValue(internalId, out allianceRewardsInfo);
    return allianceRewardsInfo;
  }

  public AllianceRewardsInfo GetWithRank(int rank)
  {
    AllianceRewardsInfo allianceRewardsInfo = (AllianceRewardsInfo) null;
    Dictionary<string, AllianceRewardsInfo>.Enumerator enumerator = this.m_DataByID.GetEnumerator();
    while (enumerator.MoveNext())
    {
      int rankMin = enumerator.Current.Value.rank_min;
      int rankMax = enumerator.Current.Value.rank_max;
      if (rank >= rankMin && rank <= rankMax)
      {
        allianceRewardsInfo = enumerator.Current.Value;
        break;
      }
    }
    return allianceRewardsInfo;
  }

  public List<AllianceRewardsInfo> GetDatas()
  {
    List<AllianceRewardsInfo> allianceRewardsInfoList = new List<AllianceRewardsInfo>();
    Dictionary<string, AllianceRewardsInfo>.Enumerator enumerator = this.m_DataByID.GetEnumerator();
    while (enumerator.MoveNext())
      allianceRewardsInfoList.Add(enumerator.Current.Value);
    allianceRewardsInfoList.Sort(new Comparison<AllianceRewardsInfo>(this.Compare));
    return allianceRewardsInfoList;
  }

  private int Compare(AllianceRewardsInfo dataL, AllianceRewardsInfo dataR)
  {
    return Math.Sign(dataL.rank_min - dataR.rank_min);
  }

  public void Clear()
  {
    if (this.m_DataByID != null)
    {
      this.m_DataByID.Clear();
      this.m_DataByID = (Dictionary<string, AllianceRewardsInfo>) null;
    }
    if (this.m_DataByInternalID == null)
      return;
    this.m_DataByInternalID.Clear();
    this.m_DataByInternalID = (Dictionary<int, AllianceRewardsInfo>) null;
  }
}
