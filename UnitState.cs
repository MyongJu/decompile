﻿// Decompiled with JetBrains decompiler
// Type: UnitState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

[Flags]
public enum UnitState
{
  marching = 1,
  rallying = 2,
  occupying = 4,
  gathering = 8,
  guarding = 16, // 0x00000010
  captive = 32, // 0x00000020
}
