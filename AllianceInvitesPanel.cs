﻿// Decompiled with JetBrains decompiler
// Type: AllianceInvitesPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class AllianceInvitesPanel : MonoBehaviour
{
  private int _allianceID = -1;
  public UISprite mAllianceIcon;
  public UILabel mAllianceTAG;
  public UILabel mAllianceName;
  public UILabel mAllianceLevel;
  public UILabel mMembers;
  public UILabel mAlliancePower;
  public UILabel mAllianceGiftLevel;
  public UILabel mMessage;
  public System.Action<int> onInviteAcceptPressed;
  public System.Action<int> onInviteDenyPressed;
  public System.Action<int> onRevokeBtnPressed;
  public System.Action<int> onMessageBtnPressed;

  public void SetDetails(int allianceID, int allianceIcon, string tag, string name, int level, int members, int maxMembers, long power, int giftLevel, string message)
  {
    this._allianceID = allianceID;
    this.mAllianceIcon.spriteName = "shield_" + allianceIcon.ToString();
    this.mAllianceTAG.text = "(" + tag + ")";
    this.mAllianceName.text = name;
    this.mAllianceLevel.text = ScriptLocalization.Get("id_level", true) + " " + level.ToString();
    this.mMembers.text = ScriptLocalization.Get("alliance_members", true) + members.ToString() + "/" + maxMembers.ToString();
    this.mAlliancePower.text = Utils.FormatThousands(power.ToString());
  }

  public int GetID()
  {
    return this._allianceID;
  }

  public void OnAcceptBtnPressed()
  {
    if (this.onInviteAcceptPressed == null)
      return;
    this.onInviteAcceptPressed(this._allianceID);
  }

  public void OnDenyBtnPressed()
  {
    if (this.onInviteDenyPressed == null)
      return;
    this.onInviteDenyPressed(this._allianceID);
  }

  public void OnRevokeBtnPressed()
  {
    if (this.onRevokeBtnPressed == null)
      return;
    this.onRevokeBtnPressed(this._allianceID);
  }

  public void OnMessageBtnPressed()
  {
    if (this.onMessageBtnPressed == null)
      return;
    this.onMessageBtnPressed(this._allianceID);
  }
}
