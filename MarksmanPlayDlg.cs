﻿// Decompiled with JetBrains decompiler
// Type: MarksmanPlayDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MarksmanPlayDlg : UI.Dialog
{
  private Dictionary<string, string> _propsValidTimeParam = new Dictionary<string, string>();
  private Dictionary<int, MarksmanRewardItemRenderer> _itemDict = new Dictionary<int, MarksmanRewardItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public UITexture npcTexture;
  public UITexture propsTexture;
  public UILabel propsText;
  public UILabel propsValidTimeText;
  public UILabel finishedButtonText;
  public UILabel abandonNeedGoldText;
  public UIButton paidAbandonButton;
  public UIButton freeAbandonButton;
  public UIButton continuousShootingButton;
  public UIButton restartShootingButton;
  public GameObject itemPrefab;
  public Transform[] itemParents;
  private string _propsImagePath;
  private Color _abandonButtonTextOriginColor;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._abandonButtonTextOriginColor = this.abandonNeedGoldText.color;
    this.AddEventHandler();
    this.OnSecondEvent(0);
    this.UpdateNpcUI();
    this.UpdatePropsUI();
    this.UpdateAbandonUI();
    this.UpdateButtonUI();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.ClearData();
  }

  public void OnPropsButtonClicked()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  public void OnHelpButtonClicked()
  {
    MarksmanPayload.Instance.ShowMarksmanHelp();
  }

  public void OnGiveUpButtonClicked()
  {
    if (MarksmanPayload.Instance.TempMarksmanData.ShootDone)
    {
      MarksmanPayload instance = MarksmanPayload.Instance;
      System.Action<bool, object> action = (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        MarksmanPayload.Instance.ShowMarksmanPickScene();
      });
      int groupId = -1;
      int num = 0;
      System.Action<bool, object> callback = action;
      instance.RefreshMarksmanRewards(groupId, num != 0, callback);
    }
    else
      UIManager.inst.ShowConfirmationBox(Utils.XLAT("tavern_lucky_archer_uppercase_reset"), Utils.XLAT("tavern_lucky_archer_give_up_description"), ScriptLocalization.Get("id_uppercase_okay", true), (string) null, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) (() =>
      {
        MarksmanPayload instance = MarksmanPayload.Instance;
        System.Action<bool, object> action = (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          MarksmanPayload.Instance.ShowMarksmanPickScene();
        });
        int groupId = -1;
        int num = 0;
        System.Action<bool, object> callback = action;
        instance.RefreshMarksmanRewards(groupId, num != 0, callback);
      }), (System.Action) null, (System.Action) null);
  }

  public void OnRecordButtonClicked()
  {
    MarksmanPayload.Instance.ShowRecordScene();
  }

  public void OnRestartPlayBatchClicked()
  {
    MarksmanPayload.Instance.RefreshMarksmanRewards(-1, true, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.UpdateButtonUI();
      this.UpdateAbandonUI();
      this.UpdateUI();
    }));
  }

  public void OnPlayBatchClicked()
  {
    if (!MarksmanPayload.Instance.IsPropsEnough)
      MarksmanPayload.Instance.ShowPropsNotEnoughConfirm();
    else
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("tavern_lucky_archer_volley_title"),
        Content = Utils.XLAT("tavern_lucky_archer_volley_description"),
        Okay = Utils.XLAT("id_uppercase_okay"),
        OkayCallback = (System.Action) (() => MarksmanPayload.Instance.OpenBatchMarksman((System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          MarksmanData marksmanData = new MarksmanData();
          marksmanData.Decode(data as Hashtable);
          List<MarksmanData.RewardData> rewardDataList1 = new List<MarksmanData.RewardData>();
          List<MarksmanData.RewardData> rewardDataList2 = marksmanData.RewardDataList;
          List<MarksmanData.RewardData> rewardDataList3 = MarksmanPayload.Instance.TempMarksmanData.RewardDataList;
          for (int i = 1; i <= rewardDataList2.Count; ++i)
          {
            MarksmanData.RewardData rewardData1 = rewardDataList2.Find((Predicate<MarksmanData.RewardData>) (x => x.RewardStayIndex == i));
            MarksmanData.RewardData rewardData2 = rewardDataList3.Find((Predicate<MarksmanData.RewardData>) (x => x.RewardStayIndex == i));
            if (rewardData1 != null && rewardData2 != null && (rewardData1.RewardOpenedStep > 0 && rewardData2.RewardOpenedStep <= 0))
              rewardDataList1.Add(rewardData1);
          }
          this.ShowBatchShotArrowAnim(rewardDataList1);
          this.ShowBatchShotExplosion(rewardDataList1);
          MarksmanPayload.Instance.TempMarksmanData.Decode(data as Hashtable);
        })))
      });
  }

  private void ShowBatchShotArrowAnim(List<MarksmanData.RewardData> rewardDataList)
  {
    if (rewardDataList == null || rewardDataList.Count <= 0)
      return;
    AudioManager.Instance.StopAndPlaySound("sfx_city_lucky_archer_shot");
    for (int index = 0; index < rewardDataList.Count; ++index)
    {
      if (this._itemDict.ContainsKey(rewardDataList[index].RewardStayIndex - 1))
        this._itemDict[rewardDataList[index].RewardStayIndex - 1].ShowShotArrowAnim(true);
    }
  }

  private void ShowBatchShotExplosion(List<MarksmanData.RewardData> rewardDataList)
  {
    if (rewardDataList == null || rewardDataList.Count <= 0)
      return;
    Utils.ExecuteInSecs(1f, (System.Action) (() =>
    {
      if ((UnityEngine.Object) null == (UnityEngine.Object) this)
        return;
      this.RefreshAfterShot();
      AudioManager.Instance.StopAndPlaySound("sfx_city_daily_activity_reward");
      List<Item2RewardInfo.Data> rewards = new List<Item2RewardInfo.Data>();
      for (int index = 0; index < rewardDataList.Count; ++index)
      {
        if (this._itemDict.ContainsKey(rewardDataList[index].RewardStayIndex - 1))
          this._itemDict[rewardDataList[index].RewardStayIndex - 1].ShowShotExplosion();
        rewards.Add(new Item2RewardInfo.Data()
        {
          itemid = rewardDataList[index].RewardId,
          count = (float) rewardDataList[index].RewardCount
        });
      }
      DicePayload.Instance.ShowCollectEffect(rewards);
    }));
  }

  private void UpdateNpcUI()
  {
    Utils.SetPortrait(this.npcTexture, "Texture/STATIC_TEXTURE/tutorial_npc_05");
  }

  private void UpdatePropsUI()
  {
    this.propsText.text = Utils.FormatThousands(MarksmanPayload.Instance.TempMarksmanData.PropsCount.ToString());
    this._propsValidTimeParam.Clear();
    this._propsValidTimeParam.Add("0", Utils.FormatTime(MarksmanPayload.Instance.TempMarksmanData.PropsValidTime - NetServerTime.inst.ServerTimestamp, true, false, true));
    this.propsValidTimeText.text = ScriptLocalization.GetWithPara("tavern_lucky_archer_expires_num", this._propsValidTimeParam, true);
    NGUITools.SetActive(this.propsValidTimeText.transform.parent.gameObject, MarksmanPayload.Instance.TempMarksmanData.PropsValidTime > 0);
    if (!(this._propsImagePath != MarksmanPayload.Instance.TempMarksmanData.PropsImagePath))
      return;
    this._propsImagePath = MarksmanPayload.Instance.TempMarksmanData.PropsImagePath;
    MarksmanPayload.Instance.FeedMarksmanTexture(this.propsTexture, this._propsImagePath);
  }

  private void UpdateAbandonUI()
  {
    this.abandonNeedGoldText.text = Utils.FormatThousands(MarksmanPayload.Instance.RefreshNeedGold.ToString());
    if (PlayerData.inst.userData.currency.gold < (long) MarksmanPayload.Instance.RefreshNeedGold)
      this.abandonNeedGoldText.color = Color.red;
    else
      this.abandonNeedGoldText.color = this._abandonButtonTextOriginColor;
    NGUITools.SetActive(this.paidAbandonButton.gameObject, !MarksmanPayload.Instance.IsRefreshFree && !MarksmanPayload.Instance.TempMarksmanData.ShootDone);
    NGUITools.SetActive(this.freeAbandonButton.gameObject, MarksmanPayload.Instance.IsRefreshFree || MarksmanPayload.Instance.TempMarksmanData.ShootDone);
  }

  private void UpdateButtonUI()
  {
    this.finishedButtonText.text = !MarksmanPayload.Instance.TempMarksmanData.ShootDone ? Utils.XLAT("tavern_lucky_archer_uppercase_reset") : Utils.XLAT("tavern_lucky_archer_end_button");
    NGUITools.SetActive(this.continuousShootingButton.gameObject, !MarksmanPayload.Instance.TempMarksmanData.ShootDone);
    NGUITools.SetActive(this.restartShootingButton.gameObject, MarksmanPayload.Instance.TempMarksmanData.ShootDone);
  }

  private void UpdateUI()
  {
    this.ClearData();
    List<MarksmanData.RewardData> rewardDataList = MarksmanPayload.Instance.TempMarksmanData.RewardDataList;
    if (rewardDataList == null)
      return;
    for (int key = 0; key < rewardDataList.Count; ++key)
    {
      if (rewardDataList[key].RewardStayIndex <= this.itemParents.Length && rewardDataList[key].RewardStayIndex > 0)
      {
        MarksmanRewardItemRenderer itemRenderer = this.CreateItemRenderer(rewardDataList[key], MarksmanData.RewardType.PLAY, this.itemParents[rewardDataList[key].RewardStayIndex - 1]);
        this._itemDict.Add(key, itemRenderer);
      }
    }
  }

  private void ClearData()
  {
    using (Dictionary<int, MarksmanRewardItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, MarksmanRewardItemRenderer> current = enumerator.Current;
        current.Value.onMarkFrameClicked -= new System.Action<int>(this.OnMarkFrameClicked);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private MarksmanRewardItemRenderer CreateItemRenderer(MarksmanData.RewardData rewardData, MarksmanData.RewardType rewardType, Transform parent)
  {
    this._itemPool.Initialize(this.itemPrefab, parent.gameObject);
    GameObject gameObject = this._itemPool.AddChild(parent.gameObject);
    gameObject.SetActive(true);
    MarksmanRewardItemRenderer component = gameObject.GetComponent<MarksmanRewardItemRenderer>();
    component.SetData(rewardData, rewardType);
    component.onMarkFrameClicked += new System.Action<int>(this.OnMarkFrameClicked);
    return component;
  }

  private void ShowCollectedEffect(int rewardStayIndex)
  {
    MarksmanData.RewardData rewardData = MarksmanPayload.Instance.TempMarksmanData.RewardDataList.Find((Predicate<MarksmanData.RewardData>) (x => x.RewardStayIndex == rewardStayIndex));
    if (rewardData == null)
      return;
    RewardsCollectionAnimator.Instance.Clear();
    RewardsCollectionAnimator.Instance.items2.Add(new Item2RewardInfo.Data()
    {
      count = (float) rewardData.RewardCount,
      itemid = rewardData.RewardId
    });
    RewardsCollectionAnimator.Instance.CollectItems2(false);
  }

  private void OnMarkFrameClicked(int rewardStayIndex)
  {
    this.RefreshAfterShot();
    if (rewardStayIndex <= 0)
      return;
    this.ShowCollectedEffect(rewardStayIndex);
  }

  private void RefreshAfterShot()
  {
    if (this._itemDict != null)
    {
      Dictionary<int, MarksmanRewardItemRenderer>.ValueCollection.Enumerator enumerator = this._itemDict.Values.GetEnumerator();
      while (enumerator.MoveNext())
        enumerator.Current.RefreshUI();
    }
    this.UpdateButtonUI();
    this.UpdateAbandonUI();
  }

  private void OnItemDataUpdated(int itemId)
  {
    if (itemId != MarksmanPayload.Instance.TempMarksmanData.PropsId)
      return;
    this.UpdatePropsUI();
    this.OnMarkFrameClicked(-1);
  }

  private void OnUserDataUpdated(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.UpdateAbandonUI();
  }

  private void OnSecondEvent(int time)
  {
    this.UpdatePropsUI();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Item.onDataUpdated += new System.Action<int>(this.OnItemDataUpdated);
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdated);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Item.onDataUpdated -= new System.Action<int>(this.OnItemDataUpdated);
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdated);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }
}
