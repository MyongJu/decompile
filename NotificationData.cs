﻿// Decompiled with JetBrains decompiler
// Type: NotificationData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;

public class NotificationData
{
  private DateTime _fireDate = DateTime.Now;
  private string _messageName = ScriptLocalization.Get("id_game_name", true);
  private string _messageTitle = ScriptLocalization.Get("id_game_name", true);
  private string _data = string.Empty;
  public const string VALUT = "1";
  public const string BLACK_MARKET = "3";
  public const string RECEPTION = "2";
  private string _message;
  private bool _isRepeatDay;
  private string soudName;

  public NotificationData()
  {
  }

  public NotificationData(string message)
  {
    this.Message = message;
  }

  public NotificationData(string message, DateTime fireDate)
  {
    this.Message = message;
    this.FireDate = fireDate;
  }

  public DateTime FireDate
  {
    get
    {
      return this._fireDate;
    }
    set
    {
      this._fireDate = value;
    }
  }

  public int RemainTime { get; set; }

  public string MessageName
  {
    get
    {
      return this._messageName;
    }
    set
    {
      this._messageName = value;
    }
  }

  public string MessageTitle
  {
    get
    {
      return this._messageTitle;
    }
    set
    {
      this._messageTitle = value;
    }
  }

  public string Message
  {
    get
    {
      return this._message;
    }
    set
    {
      this._message = value;
    }
  }

  public bool IsRepeatDay
  {
    get
    {
      return this._isRepeatDay;
    }
    set
    {
      this._isRepeatDay = value;
    }
  }

  public string Data
  {
    get
    {
      return this._data;
    }
    set
    {
      this._data = value;
    }
  }

  public string ID { get; set; }

  public string SoundName
  {
    get
    {
      return this.soudName;
    }
    set
    {
      this.soudName = value;
    }
  }

  public void Delay(int hour = 0, int minute = 0, int second = 0)
  {
    this.FireDate = new DateTime(this._fireDate.Year, this._fireDate.Month, this._fireDate.Day, hour, minute, second);
  }

  public string GetJSonContent()
  {
    return string.Empty;
  }
}
