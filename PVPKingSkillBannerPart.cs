﻿// Decompiled with JetBrains decompiler
// Type: PVPKingSkillBannerPart
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class PVPKingSkillBannerPart : MonoBehaviour
{
  private SpriteCreator m_SpriteCreator = new SpriteCreator();
  [SerializeField]
  protected GameObject m_RootNode;
  [SerializeField]
  protected UISpriteMeshProgressBar m_Slider;
  [SerializeField]
  protected TextMesh m_LeftTime;
  [SerializeField]
  protected SpriteRenderer m_SkillIcon;
  private TileData m_TileData;

  private void OnEnable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.SecondTick);
  }

  private void OnDisable()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.SecondTick);
    if ((bool) ((UnityEngine.Object) this.m_SkillIcon))
      this.m_SkillIcon.sprite = (UnityEngine.Sprite) null;
    this.m_SpriteCreator.DestroyAllCreated();
  }

  private void SecondTick(int timeStamp)
  {
    this.UpdateUI();
  }

  public void SetTileData(TileData tile)
  {
    if (tile == this.m_TileData)
      return;
    this.m_TileData = tile;
    this.UpdateUI();
  }

  private bool IsValid
  {
    get
    {
      if (this.m_TileData != null && this.m_TileData.CityData != null)
        return this.m_TileData.CityData.KillSkillEndTime.Count > 0;
      return false;
    }
  }

  private void UpdateUI()
  {
    bool flag = false;
    if (this.IsValid)
    {
      Dictionary<int, long>.Enumerator enumerator = this.m_TileData.CityData.KillSkillEndTime.GetEnumerator();
      if (enumerator.MoveNext())
      {
        KingSkillInfo byId = ConfigManager.inst.DB_KingSkill.GetById(enumerator.Current.Key);
        if (byId != null)
        {
          long num = enumerator.Current.Value - (long) NetServerTime.inst.ServerTimestamp;
          if (num > 0L)
          {
            this.m_SkillIcon.sprite = this.m_SpriteCreator.CreateSprite(byId.ImagePath);
            this.m_LeftTime.text = Utils.FormatTime((int) num, false, false, true);
            this.m_Slider.SetProgress((float) num / (byId.SkillParam1 * 3600f));
            flag |= num > 0L;
          }
        }
      }
    }
    this.m_RootNode.SetActive(flag);
  }
}
