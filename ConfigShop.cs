﻿// Decompiled with JetBrains decompiler
// Type: ConfigShop
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigShop
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, ShopStaticInfo> _shopStaticDatas;
  private Dictionary<int, ShopStaticInfo> _internalId2StatiDatas;
  private Dictionary<int, ShopStaticInfo> _itemId2StatiDatas;

  public void BuildDB(object res)
  {
    Hashtable sources = res as Hashtable;
    if (sources == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      this.parse.Parse<ShopStaticInfo, string>(sources, "ID", out this._shopStaticDatas, out this._internalId2StatiDatas);
      this._itemId2StatiDatas = new Dictionary<int, ShopStaticInfo>();
      Dictionary<int, ShopStaticInfo>.ValueCollection.Enumerator enumerator = this._internalId2StatiDatas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value == 1 && !this._itemId2StatiDatas.ContainsKey(enumerator.Current.Item_InternalId))
          this._itemId2StatiDatas.Add(enumerator.Current.Item_InternalId, enumerator.Current);
      }
    }
  }

  public Dictionary<string, ShopStaticInfo> GetTotalShopStaticDatas()
  {
    return this._shopStaticDatas;
  }

  public ShopStaticInfo GetShopDataByInternalId(int internalId)
  {
    if (this._internalId2StatiDatas.ContainsKey(internalId))
      return this._internalId2StatiDatas[internalId];
    return (ShopStaticInfo) null;
  }

  public ShopStaticInfo GetShopData(string id)
  {
    if (this._shopStaticDatas.ContainsKey(id))
      return this._shopStaticDatas[id];
    return (ShopStaticInfo) null;
  }

  public ShopStaticInfo GetShopInfoByItemInternalId(int itemId)
  {
    if (this._itemId2StatiDatas.ContainsKey(itemId))
      return this._itemId2StatiDatas[itemId];
    return (ShopStaticInfo) null;
  }

  public int GetOneItemPrice(string itemId)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo != null)
      return this.GetOneItemPrice(itemStaticInfo.internalId);
    return 0;
  }

  public int GetOneItemPrice(int itemInternalId)
  {
    ShopStaticInfo byItemInternalId = this.GetShopInfoByItemInternalId(itemInternalId);
    if (byItemInternalId != null)
      return byItemInternalId.Price;
    return 0;
  }

  public void Clear()
  {
    if (this._shopStaticDatas != null)
      this._shopStaticDatas.Clear();
    if (this._internalId2StatiDatas != null)
      this._internalId2StatiDatas.Clear();
    if (this._itemId2StatiDatas == null)
      return;
    this._itemId2StatiDatas.Clear();
  }
}
