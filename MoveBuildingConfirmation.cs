﻿// Decompiled with JetBrains decompiler
// Type: MoveBuildingConfirmation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MoveBuildingConfirmation : MonoBehaviour
{
  public UILabel mBuildingName;
  public UILabel mDescription;
  public UILabel mOwnValue;
  public UILabel mGetAndUsePrice;
  public GameObject mGetAndUseBtn;
  public GameObject mUseBtn;
  private System.Action _onUsePressed;
  private System.Action _onGetUsePressed;
  private System.Action _onClosePressed;

  public void SetDetails(string buildingName, string description, int ownValue, int getAndUsePrice, System.Action onUsePressed, System.Action onGetUsePressed, System.Action onClosePressed)
  {
    this.mBuildingName.text = buildingName;
    this.mOwnValue.text = Utils.XLAT("Own") + ": " + ownValue.ToString();
    this.mGetAndUsePrice.text = getAndUsePrice.ToString();
    this.mGetAndUseBtn.SetActive(ownValue == 0);
    this.mUseBtn.SetActive(ownValue > 0);
    this._onUsePressed = onUsePressed;
    this._onGetUsePressed = onGetUsePressed;
    this._onClosePressed = onClosePressed;
  }

  public void OnUseBtnPressed()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    this._onUsePressed();
  }

  public void OnGetAndUsePressed()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    this._onGetUsePressed();
  }

  public void OnCloseBtnPressed()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    this._onClosePressed();
  }
}
