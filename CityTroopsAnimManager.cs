﻿// Decompiled with JetBrains decompiler
// Type: CityTroopsAnimManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using SimplePath;
using System.Collections.Generic;
using UnityEngine;

public class CityTroopsAnimManager : MonoBehaviour
{
  public Dictionary<TroopType, int> cityTroops = new Dictionary<TroopType, int>();
  private int troopsIDCount = 1;
  private Dictionary<int, CityTroopsAnimControler> _troops;
  public WayPointGraph cityPath;
  public WayPointGraph paradeGroundPath;
  public Transform startPoint1;
  public Transform startPoint2;
  public Transform startPoint3;
  public Transform startPoint4;
  public Transform endPoint;
  private static CityTroopsAnimManager _inst;

  public event System.Action<TroopType, int> onTroopArraived;

  public static CityTroopsAnimManager inst
  {
    get
    {
      return CityTroopsAnimManager._inst;
    }
  }

  public void Awake()
  {
    CityTroopsAnimManager._inst = this;
    this._troops = new Dictionary<int, CityTroopsAnimControler>();
  }

  public void SendTroops(WayPointGraph wayPointGraph, Vector3 startPosition, Vector3 endPosition, CityTroopsAnimControler.Parameter param)
  {
    List<Vector3> points = GraphManager.Search((Graph) wayPointGraph, startPosition, endPosition);
    if (this.cityTroops.ContainsKey(param.troopType))
    {
      Dictionary<TroopType, int> cityTroops;
      TroopType troopType;
      (cityTroops = this.cityTroops)[troopType = param.troopType] = cityTroops[troopType] + param.troopsCount;
    }
    else
      this.cityTroops.Add(param.troopType, param.troopsCount);
    GameObject gameObject = new GameObject("CityTroopAnim");
    gameObject.transform.parent = wayPointGraph.transform;
    CityTroopsAnimControler troopsAnimControler = gameObject.AddComponent<CityTroopsAnimControler>();
    troopsAnimControler.troopReachDes += new System.Action<TroopType, int>(this.OnCityTroopsArrived);
    this._troops.Add(this.troopsIDCount, troopsAnimControler);
    param.ID = this.troopsIDCount;
    ++this.troopsIDCount;
    troopsAnimControler.StartMoving(points, param);
    gameObject.transform.localScale = new Vector3(85f, 85f, 1f) * this._GetFactorOfMovingMode(param.troopType);
    CitadelSystem.inst.paradeGroundManager.FreshCityTroops();
  }

  private float _GetFactorOfMovingMode(TroopType type)
  {
    switch (type)
    {
      case TroopType.class_infantry:
        return 0.8f;
      case TroopType.class_ranged:
      case TroopType.class_cavalry:
        return 1f;
      case TroopType.class_artyfar:
        return 1f;
      default:
        return 1f;
    }
  }

  public void OnCityTroopsArrived(TroopType troopType, int troopCount)
  {
    if (this.cityTroops.ContainsKey(troopType))
    {
      Dictionary<TroopType, int> cityTroops;
      TroopType index;
      (cityTroops = this.cityTroops)[index = troopType] = cityTroops[index] - troopCount;
    }
    if (this.onTroopArraived == null)
      return;
    this.onTroopArraived(troopType, troopCount);
  }

  public void DestoryTroops(int ID)
  {
    if (!this._troops.ContainsKey(ID))
      return;
    this._troops[ID].troopReachDes -= new System.Action<TroopType, int>(this.OnCityTroopsArrived);
    GameObject gameObject = this._troops[ID].gameObject;
    this._troops.Remove(ID);
    UnityEngine.Object.Destroy((UnityEngine.Object) gameObject);
  }

  public void ClearTroops()
  {
    List<int> intList = new List<int>();
    Dictionary<int, CityTroopsAnimControler>.KeyCollection.Enumerator enumerator = this._troops.Keys.GetEnumerator();
    while (enumerator.MoveNext())
      intList.Add(enumerator.Current);
    for (int index = 0; index < intList.Count; ++index)
      this.DestoryTroops(intList[index]);
    this._troops.Clear();
    this.cityTroops.Clear();
  }
}
