﻿// Decompiled with JetBrains decompiler
// Type: Coordinate
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;

public struct Coordinate
{
  public static Coordinate zero = new Coordinate(0, 0, 0);
  public int K;
  public int X;
  public int Y;

  public Coordinate(int k, int x, int y)
  {
    this.K = k;
    this.X = x;
    this.Y = y;
  }

  public override string ToString()
  {
    return MapUtils.GetCoordinateString(this.K, this.X, this.Y);
  }

  public string ToShortString()
  {
    return ScriptLocalization.GetWithPara("kingdom_coordinates_x_y", new Dictionary<string, string>()
    {
      {
        "0",
        this.X.ToString()
      },
      {
        "1",
        this.Y.ToString()
      }
    }, true);
  }

  public override int GetHashCode()
  {
    return this.ToString().GetHashCode();
  }

  public bool Equals(Coordinate other)
  {
    if (this.K == other.K && this.X == other.X)
      return this.Y == other.Y;
    return false;
  }

  public bool Decode(object orgData)
  {
    if (orgData == null)
      return false;
    Hashtable inData = orgData as Hashtable;
    if (inData == null)
      return false;
    return false | DatabaseTools.UpdateData(inData, "k", ref this.K) | DatabaseTools.UpdateData(inData, "x", ref this.X) | DatabaseTools.UpdateData(inData, "y", ref this.Y);
  }

  public bool Decode(object inK, object inX, object inY)
  {
    bool flag = false;
    int result;
    if (inK != null && int.TryParse(inK.ToString(), out result) && result != this.K)
    {
      this.K = result;
      flag = true;
    }
    if (inX != null && int.TryParse(inX.ToString(), out result) && result != this.X)
    {
      this.X = result;
      flag = true;
    }
    if (inY != null && int.TryParse(inY.ToString(), out result) && result != this.Y)
    {
      this.Y = result;
      flag = true;
    }
    return flag;
  }

  public string ShowInfo()
  {
    return string.Format("({0}, {1}, {2})", (object) this.K, (object) this.X, (object) this.Y);
  }

  public struct Params
  {
    public const string KEY = "location";
    public const string LOCATION_K = "k";
    public const string LOCATION_X = "x";
    public const string LOCATION_Y = "y";
  }
}
