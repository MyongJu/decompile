﻿// Decompiled with JetBrains decompiler
// Type: TradingHallBuySlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class TradingHallBuySlot : MonoBehaviour
{
  private Dictionary<string, string> _itemCountParam = new Dictionary<string, string>();
  [SerializeField]
  private UILabel _labelItemName;
  [SerializeField]
  private UILabel _labelItemSaleCount;
  [SerializeField]
  private UILabel _labelEquipmentEnhanced;
  [SerializeField]
  private ItemIconRenderer _itemIconRenderer;
  private TradingHallData.BuyGoodData _buyGoodData;
  private int _realIndex;
  public System.Action<TradingHallData.BuyGoodData> OnCurrentBuySlotClicked;

  public int RealIndex
  {
    get
    {
      return this._realIndex;
    }
  }

  public TradingHallData.BuyGoodData CurBuyGoodData
  {
    get
    {
      return this._buyGoodData;
    }
  }

  public void SetData(TradingHallData.BuyGoodData buyGoodData, int realIndex)
  {
    this._buyGoodData = buyGoodData;
    this._realIndex = realIndex;
    this.UpdateUI();
  }

  public void OnBuySlotClicked()
  {
    if (this.OnCurrentBuySlotClicked == null)
      return;
    this.OnCurrentBuySlotClicked(this._buyGoodData);
  }

  public void OnItemPress()
  {
    if (this._buyGoodData == null)
      return;
    Utils.DelayShowTip(this._buyGoodData.ItemId, this.transform, (long) this._buyGoodData.EquipmentId, 0L, this._buyGoodData.EquipmentEnhanced);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  public void OnItemClick()
  {
    if (this._buyGoodData == null)
      return;
    Utils.ShowItemTip(this._buyGoodData.ItemId, this.transform, (long) this._buyGoodData.EquipmentId, 0L, this._buyGoodData.EquipmentEnhanced);
  }

  private void UpdateUI()
  {
    if (this._buyGoodData == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._buyGoodData.ItemId);
    if (itemStaticInfo != null)
    {
      this._labelItemName.text = itemStaticInfo.LocName;
      this._labelEquipmentEnhanced.text = this._buyGoodData.EquipmentEnhanced <= 0 ? string.Empty : "+" + this._buyGoodData.EquipmentEnhanced.ToString();
      this._labelItemName.text += this._labelEquipmentEnhanced.text;
      this._itemCountParam.Clear();
      this._itemCountParam.Add("0", Utils.FormatThousands(this._buyGoodData.ItemTotalCount.ToString()));
      this._labelItemSaleCount.text = ScriptLocalization.GetWithPara("id_available_num", this._itemCountParam, true);
      EquipmentManager.Instance.ConfigQualityLabelWithColor(this._labelItemName, itemStaticInfo.Quality);
      this._itemIconRenderer.SetData(this._buyGoodData.ItemId, string.Empty, true);
    }
    if (this._buyGoodData.ItemTotalCount <= 0L)
      GreyUtility.Grey(this._itemIconRenderer.gameObject);
    else if (this._buyGoodData.ThisGoodAvailable)
      GreyUtility.Normal(this._itemIconRenderer.gameObject);
    else
      GreyUtility.Custom(this._itemIconRenderer.gameObject, new Color(1f, 0.3137255f, 0.3137255f, 1f));
  }
}
