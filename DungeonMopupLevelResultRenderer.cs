﻿// Decompiled with JetBrains decompiler
// Type: DungeonMopupLevelResultRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class DungeonMopupLevelResultRenderer : MonoBehaviour
{
  public UILabel _level;
  public UITable _overallTable;
  public UIGrid _rewardGrid;
  public UIWidget _background;
  public UIWidget _placeHolder;
  public GameObject _itemPrefab;
  public GameObject _pvpPrefab;
  public bool NeedReposition;

  public void SetData(DragonKnightDungeonData.RaidInfo raidInfo, System.Action callback)
  {
    this._rewardGrid.onReposition = (UIGrid.OnReposition) (() => this.NeedReposition = true);
    this._overallTable.onReposition = (UITable.OnReposition) (() =>
    {
      if (callback == null)
        return;
      callback();
    });
    this._level.text = ScriptLocalization.GetWithPara("dragon_knight_dungeon_floor_num", new Dictionary<string, string>()
    {
      {
        "0",
        raidInfo.level.ToString()
      }
    }, 1 != 0);
    Dictionary<int, int>.Enumerator enumerator = raidInfo.reward.GetEnumerator();
    while (enumerator.MoveNext())
    {
      GameObject gameObject = Utils.DuplicateGOB(this._itemPrefab, this._rewardGrid.transform);
      gameObject.SetActive(true);
      gameObject.GetComponent<DungeonMopupRewardRenderer>().SetData(enumerator.Current.Key, enumerator.Current.Value);
    }
    this._rewardGrid.Reposition();
    DragonKnightPVPResultPopup.Parameter parameter = DragonKnightUtils.LoadMopupHistory(raidInfo.pvp_time);
    if (parameter != null)
    {
      GameObject gameObject = Utils.DuplicateGOB(this._pvpPrefab, this._overallTable.transform);
      gameObject.SetActive(true);
      gameObject.GetComponent<DungeonMopupPVPRenderer>().SetData(parameter);
    }
    this._placeHolder.transform.parent = (Transform) null;
    this._placeHolder.transform.parent = this._overallTable.transform;
    this._overallTable.Reposition();
  }

  private void Update()
  {
    if (!this.NeedReposition)
      return;
    this.NeedReposition = false;
    this._overallTable.Reposition();
  }
}
