﻿// Decompiled with JetBrains decompiler
// Type: AllianceDiplomacyInvitePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceDiplomacyInvitePopup : MonoBehaviour
{
  public UISprite mAllianceIcon;
  public UILabel mAllianceName;
  public System.Action onYes;
  public System.Action onNo;
  public System.Action onNo2;

  public void SetDetails(string icon, string name, System.Action yes, System.Action no, System.Action no2)
  {
    this.mAllianceIcon.spriteName = icon;
    this.mAllianceName.text = name;
    this.onYes = yes;
    this.onNo = no;
    this.onNo2 = no2;
  }

  public void OnYesBtnPressed()
  {
    if (this.onYes == null)
      return;
    this.onYes();
  }

  public void OnNoBtnPressed()
  {
    if (this.onNo == null)
      return;
    this.onNo();
  }

  public void OnNo2BtnPressed()
  {
    if (this.onNo2 == null)
      return;
    this.onNo2();
  }
}
