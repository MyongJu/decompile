﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarNotOpenState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;

public class AllianceWarNotOpenState : AllianceWarBaseState
{
  private const string REGISTER = "alliance_warfare_register";
  private const string UNREGISTER = "alliance_warfare_register_cancel";
  private const string NOT_OPEN_TOAST = "toast_alliance_warfare_not_open";
  private const string PLEASE_WAIT = "alliance_warfare_not_open_yet";
  public UIButton signUpButton;
  public UILabel signUpLabel;
  public UILabel waitLabel;

  public override void Show(bool requestData)
  {
    base.Show(requestData);
  }

  public override void UpdateUI()
  {
    this.instruction.text = Utils.XLAT("alliance_warfare_event_description");
    int time = 0;
    if (NetServerTime.inst.ServerTimestamp < this.allianceWarData.RegisterStartTime)
      time = this.allianceWarData.RegisterStartTime - NetServerTime.inst.ServerTimestamp;
    else if (NetServerTime.inst.ServerTimestamp > this.allianceWarData.FightEndTime)
      time = this.allianceWarData.FightEndTime + this.allianceWarData.Interval - NetServerTime.inst.ServerTimestamp;
    this.state.text = ScriptLocalization.GetWithPara("alliance_warfare_unopened_status", new Dictionary<string, string>()
    {
      {
        "0",
        Utils.FormatTime(time, true, true, true)
      }
    }, true);
    this.signUpLabel.text = Utils.XLAT("alliance_warfare_register");
    this.waitLabel.text = Utils.XLAT("alliance_warfare_not_open_yet");
  }

  public void OnSignUpButtonPressed()
  {
    UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_not_open"), (System.Action) null, 4f, false);
  }

  public override void Hide()
  {
    base.Hide();
  }
}
