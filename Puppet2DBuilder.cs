﻿// Decompiled with JetBrains decompiler
// Type: Puppet2DBuilder
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Puppet2DBuilder : BaseBuilder
{
  private UITexture uiTexture;

  protected override void Prepare()
  {
    if ((Object) this.Target == (Object) null || !((Object) this.uiTexture == (Object) null))
      return;
    this.uiTexture = this.Target.GetComponent<UITexture>();
    this.IsSuccess = false;
    this.uiTexture.mainTexture = this.MissingTexture;
  }

  protected override void Clear()
  {
    if (!((Object) this.uiTexture != (Object) null))
      return;
    this.uiTexture.mainTexture = (Texture) null;
    this.uiTexture = (UITexture) null;
  }

  protected override string GetPath()
  {
    return this.ImageName;
  }

  protected override void ResultHandler(Object result)
  {
    Texture2D texture2D = result as Texture2D;
    if ((Object) texture2D != (Object) null)
    {
      this.IsSuccess = true;
      this.uiTexture.mainTexture = (Texture) texture2D;
    }
    else
    {
      this.IsSuccess = false;
      this.uiTexture.mainTexture = this.MissingTexture;
    }
  }
}
