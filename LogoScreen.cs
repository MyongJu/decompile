﻿// Decompiled with JetBrains decompiler
// Type: LogoScreen
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LogoScreen : MonoBehaviour
{
  public UITexture m_Logo;
  public TweenAlpha m_TweenAlpha;
  private LogoScreen.OnHidden m_OnHidden;

  private void Start()
  {
    if (!((Object) this.m_Logo != (Object) null))
      return;
    this.m_Logo.alpha = 0.0f;
  }

  public void Show(LogoScreen.OnHidden onHidden)
  {
    this.gameObject.SetActive(true);
    this.m_TweenAlpha.enabled = true;
    this.m_TweenAlpha.tweenFactor = 0.0f;
    this.m_OnHidden = onHidden;
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
    if (this.m_OnHidden == null)
      return;
    this.m_OnHidden();
    this.m_OnHidden = (LogoScreen.OnHidden) null;
  }

  public delegate void OnHidden();
}
