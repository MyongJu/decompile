﻿// Decompiled with JetBrains decompiler
// Type: ArtifactReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using UI;

public class ArtifactReportPopup : BaseReportPopup
{
  public UITexture itemicon;
  public Icon icon;
  private int _newArtifact;

  private void Init()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_alliance_hospital", (System.Action<bool>) null, true, false, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.itemicon, "Texture/Artifact/icon_artifact_report", (System.Action<bool>) null, true, false, string.Empty);
    ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this._newArtifact);
    this.icon.SetData(artifactInfo.ImagePath, new string[1]
    {
      artifactInfo.Name
    });
  }

  public void Goto()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._newArtifact = JsonReader.Deserialize<ArtifactReportPopup.Artifact>(Utils.Object2Json(this.param.hashtable[(object) "data"])).artifact;
    this.Init();
  }

  public class Artifact
  {
    public int artifact;
  }
}
