﻿// Decompiled with JetBrains decompiler
// Type: AllianceChangePublicMessagePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Text;
using UI;

public class AllianceChangePublicMessagePopup : Popup
{
  public UILabel title;
  public UILabel tip1;
  public UILabel tip2;
  public UILabel mottonLabel;
  public UIInput inputLabel;
  public UIButton confirmBtn;
  private System.Action publicMessageChangeCallBack;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    AllianceChangePublicMessagePopup.Parameter parameter = orgParam as AllianceChangePublicMessagePopup.Parameter;
    this.publicMessageChangeCallBack = parameter.publicMessageChangeCallBack;
    this.inputLabel.value = parameter.originalAnnouncement;
    this.inputLabel.onValidate = new UIInput.OnValidate(this.InputValidator);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    if (this.publicMessageChangeCallBack == null)
      return;
    this.publicMessageChangeCallBack();
  }

  public void OnConfirmBtnClick()
  {
    MessageHub.inst.GetPortByAction("Alliance:updateAnnouncement").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "content",
        (object) IllegalWordsUtils.Filter(this.mottonLabel.text)
      }
    }, new System.Action<bool, object>(this.UpdageAnnouncementCallback), true);
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdageAnnouncementCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.OnCloseBtnClick();
  }

  private char InputValidator(string text, int charIndex, char addedChar)
  {
    if (Encoding.UTF8.GetBytes(text).Length >= 150 || (int) addedChar == 10)
      return char.MinValue;
    return addedChar;
  }

  public class Parameter : Popup.PopupParameter
  {
    public string originalAnnouncement = string.Empty;
    public System.Action publicMessageChangeCallBack;
  }
}
