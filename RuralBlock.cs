﻿// Decompiled with JetBrains decompiler
// Type: RuralBlock
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class RuralBlock : MonoBehaviour
{
  public float waittime = 1.5f;
  public const string EffectPath = "Prefab/VFX/";
  public const string canUnlockEffectName = "fx_jiesoudikuai";
  public const string unlockEffectName = "fx_jianzhu_yan";
  private const float playtime = 1f;
  public string id;
  private bool show;
  private RuralBlockInfo rbi;
  [HideInInspector]
  public GameObject canUnlockEffect;
  [HideInInspector]
  public GameObject UnlockEffect;

  public bool Show
  {
    get
    {
      CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
      string str = this.rbi.internalId.ToString();
      this.show = cityData.blockInfo != null && cityData.blockInfo.ContainsKey((object) str) && "1" == cityData.blockInfo[(object) str].ToString();
      return this.show;
    }
  }

  public void Dispose()
  {
    DBManager.inst.DB_City.onDataChanged -= new System.Action<long>(this.OnBlockDataChange);
    this.Fade(1f);
    BoxCollider[] componentsInChildren = this.GetComponentsInChildren<BoxCollider>(true);
    if (componentsInChildren == null)
      return;
    foreach (Collider collider in componentsInChildren)
      collider.enabled = true;
  }

  public void Init()
  {
    this.rbi = ConfigManager.inst.DB_RuralBlock.GetData(this.id);
    if (this.rbi != null)
      ;
    if (!this.Show)
    {
      this.gameObject.SetActive(true);
      using (List<int>.Enumerator enumerator = this.rbi.slots.GetEnumerator())
      {
        while (enumerator.MoveNext())
          CityPlotController.SetVisible(enumerator.Current, false);
      }
      this.BroadcastCanUnlockEffect();
      DBManager.inst.DB_City.onDataChanged += new System.Action<long>(this.OnBlockDataChange);
    }
    else
    {
      this.gameObject.SetActive(false);
      using (List<int>.Enumerator enumerator = this.rbi.slots.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          int current = enumerator.Current;
          if (!CitadelSystem.inst.CheckSlotHasBuilding(current))
            CityPlotController.SetVisible(current, true);
        }
      }
      DBManager.inst.DB_City.onDataChanged -= new System.Action<long>(this.OnBlockDataChange);
    }
  }

  private void OnClick()
  {
    if (CitadelSystem.inst.MStronghold.mBuildingItem.mLevel >= this.rbi.reqLevel)
    {
      UIManager.inst.OpenDlg("RuralBlock/RuralBlockUnlockDialog", (UI.Dialog.DialogParameter) new RuralBlockUnlockDialog.Parameter()
      {
        curRbi = this.rbi,
        target = this.gameObject
      }, 1 != 0, 1 != 0, 1 != 0);
    }
    else
    {
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_rural_area_unlock_description", new Dictionary<string, string>()
      {
        {
          "NO.",
          this.rbi.reqLevel.ToString()
        }
      }, true), (System.Action) null, 4f, true);
      this.gameObject.AddComponent<Twinkler>().Twinkle(Twinkler.Mode.ONCE);
      AudioManager.Instance.PlaySound("sfx_rural_locked_rural_area", false);
      Animator component = this.GetComponent<Animator>();
      if ((UnityEngine.Object) null != (UnityEngine.Object) component)
        component.Play("tree_shake");
    }
    LinkerHub.Instance.DisposeHint(this.transform);
  }

  private void OnBlockDataChange(long cityid)
  {
    if (cityid != (long) PlayerData.inst.cityId)
      return;
    this.BroadcastCanUnlockEffect();
  }

  [DebuggerHidden]
  private IEnumerator ClickEffect()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RuralBlock.\u003CClickEffect\u003Ec__Iterator42()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void BroadcastCanUnlockEffect()
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.gameObject || this.rbi.reqLevel > CitadelSystem.inst.MStronghold.mBuildingItem.mLevel || !((UnityEngine.Object) null == (UnityEngine.Object) this.canUnlockEffect))
      return;
    this.canUnlockEffect = UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/VFX/fx_jiesoudikuai", (System.Type) null)) as GameObject;
    this.canUnlockEffect.transform.parent = this.transform;
    this.canUnlockEffect.transform.localPosition = Vector3.zero;
  }

  public void UnlockBlock()
  {
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.canUnlockEffect)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.canUnlockEffect);
    BoxCollider[] componentsInChildren = this.GetComponentsInChildren<BoxCollider>();
    if (componentsInChildren != null)
    {
      foreach (Collider collider in componentsInChildren)
        collider.enabled = false;
    }
    this.StartCoroutine(this.AfterUnlock());
  }

  [DebuggerHidden]
  private IEnumerator AfterUnlock()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RuralBlock.\u003CAfterUnlock\u003Ec__Iterator43()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator ShowPlots()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RuralBlock.\u003CShowPlots\u003Ec__Iterator44()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void Fade(float value)
  {
    foreach (SpriteRenderer componentsInChild in this.GetComponentsInChildren<SpriteRenderer>(true))
    {
      Color color = componentsInChild.color;
      color.a = value;
      componentsInChild.color = color;
    }
  }

  private void OnFadeComplete()
  {
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.UnlockEffect)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.UnlockEffect);
    this.StartCoroutine(this.ShowPlots());
  }
}
