﻿// Decompiled with JetBrains decompiler
// Type: AllianceFortressEmptySlotItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceFortressEmptySlotItem : MonoBehaviour
{
  public GameObject lockContent;
  public GameObject unlockContent;
  public UIButton joinBt;
  public System.Action onClickDelegate;
  private int req_lev;

  public void SetData(string rally_id)
  {
  }

  public void Join()
  {
    PVPSystem.Instance.Controller.ReinforceCurTile();
  }

  public void Lock()
  {
    NGUITools.SetActive(this.lockContent, true);
    NGUITools.SetActive(this.unlockContent, false);
  }

  public void UnLock()
  {
    NGUITools.SetActive(this.lockContent, false);
    NGUITools.SetActive(this.unlockContent, true);
  }

  public void Refresh()
  {
  }

  public void OnLockClickHandler()
  {
  }
}
