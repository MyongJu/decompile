﻿// Decompiled with JetBrains decompiler
// Type: TroopInfoBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class TroopInfoBar : MonoBehaviour
{
  public UILabel troopCount;
  public UILabel level;
  public UISprite image;
  public UILabel troopName;
  public UILabel lostCount;
  public UILabel textDivider;

  public void SetData(TroopInfo info, int count, int lost = 0)
  {
    if (info == null)
    {
      Debug.LogError((object) "Got null info for troop data");
    }
    else
    {
      if (count > 0)
      {
        this.troopCount.text = count.ToString();
        this.troopCount.gameObject.SetActive(true);
      }
      else
        this.troopCount.gameObject.SetActive(false);
      if (lost > 0 && (Object) this.lostCount != (Object) null && (Object) this.textDivider != (Object) null)
      {
        this.lostCount.text = lost.ToString();
        this.lostCount.gameObject.SetActive(true);
        this.textDivider.gameObject.SetActive(true);
      }
      else if ((Object) this.lostCount != (Object) null && (Object) this.textDivider != (Object) null)
      {
        this.lostCount.gameObject.SetActive(false);
        this.textDivider.gameObject.SetActive(false);
      }
      if ((Object) this.level != (Object) null)
        this.level.text = info.m_Level.ToString();
      if ((Object) this.image != (Object) null)
        this.image.spriteName = info.m_Icon;
      if (!((Object) this.troopName != (Object) null))
        return;
      this.troopName.text = ScriptLocalization.Get(info.m_Name, true);
    }
  }
}
