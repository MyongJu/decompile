﻿// Decompiled with JetBrains decompiler
// Type: ItemsForShopNew
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ItemsForShopNew : MonoBehaviour
{
  private Dictionary<int, bool> isInits = new Dictionary<int, bool>();
  private List<int> datas = new List<int>();
  private List<ShopItemRenderer> groupList = new List<ShopItemRenderer>();
  private GameObjectPool pools = new GameObjectPool();
  public ShopItemRenderer ItemPrefab;
  private bool init;
  public UIScrollView scrollView;
  public UIGrid content;
  public System.Action<int> OnItemSelected;
  private ItemBag.ItemType type;
  private int shopId;

  private void InitGroup(GameObject go, int realIndex)
  {
    realIndex = Mathf.Abs(realIndex);
    go.GetComponent<ShopItemRenderer>();
  }

  private void OnBuyHandler(int shopId)
  {
    string decoration = PlayerData.inst.playerCityData.decoration;
    this.shopId = shopId;
    ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(shopId);
    if (dataByInternalId == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(dataByInternalId.Item_InternalId);
    if (itemStaticInfo == null)
      return;
    bool flag = false;
    string Term = "item_confirm_buy_&_use_description";
    if (!string.IsNullOrEmpty(itemStaticInfo.Param1) && !string.IsNullOrEmpty(decoration) && decoration != itemStaticInfo.Param1)
    {
      Term = "stronghold_appearance_confirm_description";
      flag = true;
    }
    this.ShowWarn(ScriptLocalization.Get(Term, true), string.Empty);
  }

  private void ShowWarn(string content, string title = "")
  {
    MessageBoxWith2Buttons.Parameter parameter = new MessageBoxWith2Buttons.Parameter();
    if (string.IsNullOrEmpty(title))
      title = ScriptLocalization.Get("id_uppercase_warning", true);
    parameter.title = title;
    parameter.content = content;
    parameter.yes = ScriptLocalization.Get("id_uppercase_confirm", true);
    parameter.no = ScriptLocalization.Get("id_uppercase_cancel", true);
    parameter.yesCallback = new System.Action(this.BuyAndUse);
    parameter.noCallback = (System.Action) null;
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) parameter);
  }

  private void BuyAndUse()
  {
    ItemBag.Instance.BuyAndUseShopItem(this.shopId, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_stronghold_appearance_buy_success", true), (System.Action) null, 4f, false);
    }), (Hashtable) null, 1);
  }

  private void Start()
  {
    this.Init();
  }

  private void Init()
  {
    if (this.init)
      return;
    this.init = true;
    this.pools.Initialize(this.ItemPrefab.gameObject, this.gameObject);
  }

  public void Dispose()
  {
    this.init = false;
    this.pools.Clear();
  }

  private void OnSelectedHandler(int index)
  {
    this.UpdateUI();
  }

  private void Clear()
  {
    for (int index = 0; index < this.groupList.Count; ++index)
      this.groupList[index].Dispose();
    this.groupList.Clear();
  }

  public void SetData(ItemBag.ItemType type)
  {
    this.type = type;
    this.Refresh();
  }

  public void Refresh()
  {
    this.Init();
    this.OnSelectedHandler(0);
  }

  private void SetView()
  {
    NGUITools.SetActive(this.scrollView.gameObject, true);
  }

  private void UpdateUI()
  {
    this.CreateDatas();
    this.SetView();
    if (!this.isInits.ContainsKey(0))
    {
      this.isInits.Add(0, true);
      for (int index = 0; index < this.datas.Count; ++index)
      {
        ShopItemRenderer group = this.GetGroup();
        group.OnCustomBuy = new System.Action<int>(this.OnBuyHandler);
        group.OnSelectedHandler = new System.Action<ShopItemRenderer>(this.OnSelectedHandler);
        group.FeedData((IComponentData) this.GetIconData(this.datas[index]));
      }
    }
    this.content.Reposition();
    this.scrollView.ResetPosition();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.scrollView.ResetPosition()));
  }

  private ShopItemRenderer GetGroup()
  {
    GameObject go = NGUITools.AddChild(this.content.gameObject, this.ItemPrefab.gameObject);
    ShopItemRenderer component = go.GetComponent<ShopItemRenderer>();
    this.groupList.Add(component);
    NGUITools.SetActive(go, true);
    return component;
  }

  private void OnSelectedHandler(ShopItemRenderer selected)
  {
    for (int index = 0; index < this.groupList.Count; ++index)
    {
      ShopItemRenderer group = this.groupList[index];
      if ((UnityEngine.Object) selected != (UnityEngine.Object) group)
        group.Selected = false;
    }
    ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(selected.Shop_ID);
    if (this.OnItemSelected == null)
      return;
    this.OnItemSelected(dataByInternalId.Item_InternalId);
  }

  private IconData GetIconData(int shopId)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(ConfigManager.inst.DB_Shop.GetShopDataByInternalId(shopId).Item_InternalId);
    IconData iconData = new IconData();
    iconData.image = itemStaticInfo.ImagePath;
    iconData.Data = (object) shopId;
    iconData.contents = new string[2];
    iconData.contents[0] = itemStaticInfo.LocName;
    iconData.contents[1] = itemStaticInfo.LocDescription;
    return iconData;
  }

  private void CreateDatas()
  {
    this.datas.Clear();
    List<ShopStaticInfo> currentSaleShopItems = ItemBag.Instance.GetCurrentSaleShopItems("shop", this.type);
    currentSaleShopItems.Sort(new Comparison<ShopStaticInfo>(this.CompareShopItem));
    for (int index = 0; index < currentSaleShopItems.Count; ++index)
      this.datas.Add(currentSaleShopItems[index].internalId);
  }

  private List<IconData> GetIconDatas()
  {
    List<ShopStaticInfo> currentSaleShopItems = ItemBag.Instance.GetCurrentSaleShopItems("shop", this.type);
    currentSaleShopItems.Sort(new Comparison<ShopStaticInfo>(this.CompareShopItem));
    List<IconData> iconDataList = new List<IconData>();
    for (int index = 0; index < currentSaleShopItems.Count; ++index)
      iconDataList.Add(this.GetIconData(currentSaleShopItems[index].internalId));
    return iconDataList;
  }

  private int CompareShopItem(ShopStaticInfo a, ShopStaticInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }
}
