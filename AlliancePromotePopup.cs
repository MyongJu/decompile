﻿// Decompiled with JetBrains decompiler
// Type: AlliancePromotePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;

public class AlliancePromotePopup : Popup
{
  private AlliancePromotePopup.Parameter m_Parameter;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as AlliancePromotePopup.Parameter;
  }

  public void OnNo()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnYes()
  {
    this.OnNo();
    AllianceManager.Instance.PromoteOrDemote(this.m_Parameter.uid, this.m_Parameter.title, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      ChatMessageManager.SendAllianceMessage(ChatMessageManager.AllianceMessageType.promote, this.m_Parameter.name, AllianceTitle.TitleToRank(this.m_Parameter.oldTitle), AllianceTitle.TitleToRank(this.m_Parameter.title));
      if (this.m_Parameter.onsuccess == null)
        return;
      this.m_Parameter.onsuccess();
    }));
  }

  public class Parameter : Popup.PopupParameter
  {
    public long uid;
    public int title;
    public int oldTitle;
    public string name;
    public System.Action onsuccess;
  }
}
