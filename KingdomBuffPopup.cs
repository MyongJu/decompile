﻿// Decompiled with JetBrains decompiler
// Type: KingdomBuffPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class KingdomBuffPopup : Popup
{
  private Dictionary<int, KingdomBuffItemRenderer> _itemDict = new Dictionary<int, KingdomBuffItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemKingdomBuffPrefab;
  public GameObject itemAllianceBuffPrefab;
  public GameObject itemRoot;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.AddEventHandler();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.ClearData();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.ShowKingdomBuffContent();
  }

  private void ShowKingdomBuffContent()
  {
    this.ClearData();
    this.table.padding.y = KingdomBuffPayload.Instance.KingdomBuffDataList.Find((Predicate<KingdomBuffPayload.KingdomBuffData>) (x => x.buffType == KingdomBuffPayload.KingdomBuffType.KINGDOM)) != null ? 0.0f : -100f;
    this._itemDict.Add(0, this.CreateItemRenderer(KingdomBuffPayload.KingdomBuffType.KINGDOM));
    if (PlayerData.inst.allianceId > 0L)
      this._itemDict.Add(1, this.CreateItemRenderer(KingdomBuffPayload.KingdomBuffType.ALLIANCE));
    Utils.ExecuteInSecs(0.15f, (System.Action) (() => this.Reposition()));
  }

  private void OnKingdomBuffDataUpdated()
  {
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    KingdomBuffPayload.Instance.onKingdomBuffDataUpdated += new System.Action(this.OnKingdomBuffDataUpdated);
  }

  private void RemoveEventHandler()
  {
    KingdomBuffPayload.Instance.onKingdomBuffDataUpdated -= new System.Action(this.OnKingdomBuffDataUpdated);
  }

  private void ClearData()
  {
    using (Dictionary<int, KingdomBuffItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, KingdomBuffItemRenderer> current = enumerator.Current;
        current.Value.ClearData();
        current.Value.onKingdomBuffFinished -= new System.Action(this.OnKingdomBuffFinished);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private KingdomBuffItemRenderer CreateItemRenderer(KingdomBuffPayload.KingdomBuffType buffType)
  {
    switch (buffType)
    {
      case KingdomBuffPayload.KingdomBuffType.KINGDOM:
        this._itemPool.Initialize(this.itemKingdomBuffPrefab, this.itemRoot);
        break;
      case KingdomBuffPayload.KingdomBuffType.ALLIANCE:
        this._itemPool.Initialize(this.itemAllianceBuffPrefab, this.itemRoot);
        break;
    }
    GameObject gameObject = this._itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    KingdomBuffItemRenderer component = gameObject.GetComponent<KingdomBuffItemRenderer>();
    component.SetData(buffType);
    component.onKingdomBuffFinished += new System.Action(this.OnKingdomBuffFinished);
    return component;
  }

  private void OnKingdomBuffFinished()
  {
    this.UpdateUI();
  }
}
