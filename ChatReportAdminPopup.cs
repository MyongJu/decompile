﻿// Decompiled with JetBrains decompiler
// Type: ChatReportAdminPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class ChatReportAdminPopup : Popup
{
  public const string POP_TYPE = "Chat/ChatReportAdminPopup";
  public UIInput contentInputLabel;
  private long _personalChannelId;
  private long _personalUid;
  private string _personalName;
  private string _senderCustomIcon;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    ChatReportAdminPopup.Paramer paramer = orgParam as ChatReportAdminPopup.Paramer;
    if (paramer == null)
      return;
    this._personalChannelId = paramer.personalChannelId;
    this._personalUid = paramer.personalUid;
    this._personalName = paramer.personalName;
    this._senderCustomIcon = paramer.senderCustomIcon;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  public void OnReportAdminBtnClicked()
  {
    if (string.IsNullOrEmpty(this.contentInputLabel.value))
      return;
    MuteAPI.ReportAdmin(this._personalUid, this.contentInputLabel.value, (System.Action) (() => this.OnCloseBtnClicked()));
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Paramer : Popup.PopupParameter
  {
    public long personalChannelId;
    public long personalUid;
    public string personalName;
    public string senderCustomIcon;
    public int personMuteAdmin;
    public int personIcon;
  }
}
