﻿// Decompiled with JetBrains decompiler
// Type: WonderVFX
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class WonderVFX : MonoBehaviour, IConstructionController
{
  private const string vfx1path = "Prefab/VFX/fx_wonder_hit_new";
  private const string vfx2path = "Prefab/VFX/fx_wonder_hit_fire";
  public RendererSortingLayerIdModifier m_PeaceShield;
  public GameObject m_GoldenSword;
  public GameObject m_hit0;
  public GameObject m_hit1;
  private TileData m_TileData;

  public void OnInitialize()
  {
    MessageHub.inst.GetPortByAction("wonder_tower_attack").AddEvent(new System.Action<object>(this.OnWonderTowerAttack));
  }

  public void OnFinalize()
  {
    MessageHub.inst.GetPortByAction("wonder_tower_attack").RemoveEvent(new System.Action<object>(this.OnWonderTowerAttack));
  }

  public void UpdateUI(TileData tile)
  {
    this.m_TileData = tile;
    if (this.m_TileData == null || this.m_TileData.WonderData == null)
      return;
    if ((bool) ((UnityEngine.Object) this.m_PeaceShield))
      this.m_PeaceShield.gameObject.SetActive(this.m_TileData.WonderData.CanShowPeaceShield);
    if (!(bool) ((UnityEngine.Object) this.m_GoldenSword))
      return;
    this.m_GoldenSword.SetActive(this.m_TileData.WonderData.CanShowGoldenSword);
  }

  private void Awake()
  {
    this.OnInitialize();
  }

  private void OnDestroy()
  {
    this.OnFinalize();
  }

  public void Reset()
  {
    if ((bool) ((UnityEngine.Object) this.m_PeaceShield))
      this.m_PeaceShield.gameObject.SetActive(false);
    if (!(bool) ((UnityEngine.Object) this.m_GoldenSword))
      return;
    this.m_GoldenSword.SetActive(false);
  }

  public void SetSortingLayerID(int sortingLayerID)
  {
    if (!(bool) ((UnityEngine.Object) this.m_PeaceShield))
      return;
    this.m_PeaceShield.SortingLayerName = SortingLayer.IDToName(sortingLayerID);
  }

  private void OnWonderTowerAttack(object data)
  {
    Hashtable hashtable = data as Hashtable;
    if (hashtable == null || !hashtable.ContainsKey((object) "tower_id"))
      return;
    this.PlayAttackEffect(int.Parse(hashtable[(object) "tower_id"].ToString()));
  }

  private void PlayAttackEffect(int towerid)
  {
    if (!this.gameObject.activeSelf)
      return;
    string uri = "Prefab/VFX/fx_wonder_hit_new";
    if (towerid == 2 || towerid == 4)
    {
      AudioManager.Instance.PlaySound("sfx_kingdom_wonder_fire_receive", false);
      uri = "Prefab/VFX/fx_wonder_hit_fire";
    }
    else
      AudioManager.Instance.PlaySound("sfx_kingdom_wonder_lightning_receive", false);
    RendererSortingLayerIdModifier component = VfxManager.Instance.Get(VfxManager.Instance.CreateAndPlay(uri, this.transform.FindChild("VFX"))).gameObject.GetComponent<RendererSortingLayerIdModifier>();
    if (!(bool) ((UnityEngine.Object) this.m_PeaceShield))
      return;
    component.SortingLayerName = this.m_PeaceShield.SortingLayerName;
  }
}
