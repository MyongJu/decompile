﻿// Decompiled with JetBrains decompiler
// Type: EquipmentComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class EquipmentComponent : ComponentRenderBase
{
  public UITexture icon;
  public UITexture background;
  public UILabel equipmentScrollName;
  public UILabel levelRequireLabel;
  public GameObject selected;
  public EquipmentComponentData componentData;
  public GameObject isEquiped;
  public UILabel isFreeze;

  public override void Init()
  {
    this.componentData = this.data as EquipmentComponentData;
    this.equipmentScrollName.text = this.componentData.itemInfo.LocName + (this.componentData.enhanceLevel <= 0 ? string.Empty : "+" + this.componentData.enhanceLevel.ToString());
    Utils.SetItemNormalName(this.equipmentScrollName, this.componentData.itemInfo.Quality);
    string str = string.Empty;
    string format = string.Empty;
    if (this.componentData.bagType == BagType.Hero)
    {
      str = this.componentData.equipmentInfo.heroLevelMin > PlayerData.inst.heroData.level ? "[FF0000]" : string.Empty;
      format = Utils.XLAT("forge_equip_requirement");
    }
    else if (this.componentData.bagType == BagType.DragonKnight)
    {
      str = PlayerData.inst.dragonKnightData == null || this.componentData.equipmentInfo.heroLevelMin <= PlayerData.inst.dragonKnightData.Level ? string.Empty : "[FF0000]";
      format = Utils.XLAT("dragon_knight_equip_level_requirement");
    }
    this.levelRequireLabel.text = str + string.Format(format, (object) this.componentData.equipmentInfo.heroLevelMin);
    InventoryItemData myItemByItemId = DBManager.inst.GetInventory(this.componentData.bagType).GetMyItemByItemID(this.componentData.itemId);
    if (myItemByItemId != null)
    {
      if ((UnityEngine.Object) this.isEquiped != (UnityEngine.Object) null)
        this.isEquiped.SetActive(myItemByItemId.isEquipped);
      if ((UnityEngine.Object) this.isFreeze != (UnityEngine.Object) null)
      {
        this.isFreeze.text = myItemByItemId.FreezeHint;
        this.isFreeze.gameObject.SetActive(myItemByItemId.IsFreeze);
      }
    }
    EquipmentManager.Instance.ConfigQualityLabelWithColor(this.equipmentScrollName, this.componentData.equipmentInfo.quality);
    BuilderFactory.Instance.Build((UIWidget) this.icon, this.componentData.itemInfo.ImagePath, (System.Action<bool>) null, true, true, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.background, this.componentData.equipmentInfo.QualityImagePath, (System.Action<bool>) null, true, true, true, string.Empty);
  }

  public override void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
    BuilderFactory.Instance.Release((UIWidget) this.background);
  }

  public bool Select
  {
    set
    {
      if (!((UnityEngine.Object) this.selected != (UnityEngine.Object) null))
        return;
      this.selected.SetActive(value);
    }
  }
}
