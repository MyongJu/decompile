﻿// Decompiled with JetBrains decompiler
// Type: AllianceRewardDistributionPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceRewardDistributionPopup : Popup
{
  private List<AllianceMemberItemData> leaderView = new List<AllianceMemberItemData>();
  private List<AllianceMemberItemData> canBeDistributed = new List<AllianceMemberItemData>();
  private List<AllianceMemberItemData> canNotBeDistributed = new List<AllianceMemberItemData>();
  private List<AllianceMemberItemData> memberView = new List<AllianceMemberItemData>();
  private List<AllianceMemberItemRender> memberRenderList = new List<AllianceMemberItemRender>();
  private List<AllianceRewardItemRender> rewardRenderList = new List<AllianceRewardItemRender>();
  private List<AllianceRewardItemData> rewardDataList = new List<AllianceRewardItemData>();
  private const string POPUP_TITLE = "alliance_warfare_assign_rewards_button";
  private const string MEMBER_TITLE = "alliance_warfare_rank";
  private const string PLAYER_NAME = "alliance_warfare_lord_name";
  private const string MEMBER_SCORE = "alliance_warfare_contributions";
  private const string ONE_GIFT_NOTICE = "alliance_warfare_one_gift_notice";
  private const string ASSIGNED = "toast_alliance_warfare_lord_assigned";
  private const string ZERO_SCORE = "alliance_warfare_zero_score_notice";
  private const string NOT_IN_ALLIANCE = "alliance_warfare_not_in_alliance_notice";
  private const string NOT_ALL_SELECTED = "toast_alliance_warfare_gift_target_choosen";
  private const string NO_ALLIANCE_REWARD = "help_alliance_warfare_no_alliance_reward_hint";
  private const string ALLIANCE_REWARD = "id_alliance_rewards";
  private const string REWARD_NOT_READY = "alliance_warfare_alliance_reward_not_ready";
  public UILabel popupTitle;
  public UILabel memberTitle;
  public UILabel memberName;
  public UILabel memberScore;
  public UILabel condition;
  public UILabel distributeLabel;
  public UIGrid leftGrid;
  public UIGrid rightGrid;
  public GameObject memberTemplate;
  public GameObject rewardTemplate;
  public UIButton distributeBtn;
  public UILabel noAllianceReward;
  private bool rewardNotReady;
  private AllianceRewardData rewardData;
  private AllianceMemberItemRender currentSelectedMember;
  private AllianceRewardItemRender currentSelectedReward;
  private AllianceRewardItemRender distributedReward;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    this.RequestAllianceAward(new System.Action<bool, object>(this.RequestAllianceAwardCallback), true);
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.UpdateStaticElements();
  }

  private bool HasBeenDistributed(long uid)
  {
    bool flag = false;
    if (this.rewardData != null && this.rewardData.reward_list != null)
    {
      for (int index = 0; index < this.rewardData.reward_list.Count; ++index)
      {
        if (uid == this.rewardData.reward_list[index].uid)
        {
          flag = true;
          break;
        }
      }
    }
    return flag;
  }

  private bool HasContributed(long uid)
  {
    bool flag = false;
    if (this.rewardData != null && this.rewardData.member_scores != null)
    {
      Dictionary<string, string>.Enumerator enumerator = this.rewardData.member_scores.GetEnumerator();
      while (enumerator.MoveNext())
      {
        long result;
        long.TryParse(enumerator.Current.Key, out result);
        if (uid == result && long.Parse(enumerator.Current.Value) > 0L)
        {
          flag = true;
          break;
        }
      }
    }
    return flag;
  }

  private bool HasLeftAlliance(long uid)
  {
    bool flag = false;
    string str = uid.ToString();
    if (this.rewardData != null && this.rewardData.leave_users != null)
    {
      for (int index = 0; index < this.rewardData.leave_users.Count; ++index)
      {
        if (str == this.rewardData.leave_users[index])
        {
          flag = true;
          break;
        }
      }
    }
    return flag;
  }

  private bool CanbeDistributed(long uid)
  {
    return !this.HasBeenDistributed(uid) && this.HasContributed(uid) && !this.HasLeftAlliance(uid);
  }

  private long GetScore(long uid)
  {
    long result = 0;
    string key = uid.ToString();
    if (this.rewardData != null && this.rewardData.member_scores != null && this.rewardData.member_scores.ContainsKey(key))
      long.TryParse(this.rewardData.member_scores[key], out result);
    return result;
  }

  private bool IsAllianceLeader()
  {
    return PlayerData.inst.allianceData.creatorId == PlayerData.inst.uid;
  }

  private bool AreAllRewardsDistributed()
  {
    bool flag = true;
    if (this.rewardData != null && this.rewardData.reward_list != null)
    {
      for (int index = 0; index < this.rewardData.reward_list.Count; ++index)
      {
        if (this.rewardData.reward_list[index].uid == 0L)
        {
          flag = false;
          break;
        }
      }
    }
    return flag;
  }

  private void UpdateStaticElements()
  {
    this.memberTitle.text = Utils.XLAT("alliance_warfare_rank");
    this.memberName.text = Utils.XLAT("alliance_warfare_lord_name");
    this.memberScore.text = Utils.XLAT("alliance_warfare_contributions");
    this.condition.text = Utils.XLAT("alliance_warfare_one_gift_notice");
    this.distributeLabel.text = Utils.XLAT("alliance_warfare_assign_rewards_button");
    if (this.IsAllianceLeader())
    {
      this.popupTitle.text = Utils.XLAT("alliance_warfare_assign_rewards_button");
      this.distributeBtn.gameObject.SetActive(true);
    }
    else
    {
      this.popupTitle.text = Utils.XLAT("id_alliance_rewards");
      this.distributeBtn.gameObject.SetActive(false);
      this.condition.gameObject.SetActive(false);
    }
  }

  private void UpdateAllianceMembers()
  {
    this.ClearMemeberLists();
    using (Dictionary<long, AllianceMemberData>.ValueCollection.Enumerator enumerator = PlayerData.inst.allianceData.members.datas.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceMemberData current = enumerator.Current;
        string str = string.Empty;
        UserData userData = DBManager.inst.DB_User.Get(current.uid);
        if (userData != null)
          str = userData.userName;
        AllianceMemberItemData allianceMemberItemData = new AllianceMemberItemData();
        allianceMemberItemData.uid = current.uid;
        allianceMemberItemData.title = current.title;
        allianceMemberItemData.name = str;
        allianceMemberItemData.score = this.GetScore(current.uid);
        if (this.CanbeDistributed(current.uid))
        {
          this.canBeDistributed.Add(allianceMemberItemData);
        }
        else
        {
          if (this.HasBeenDistributed(current.uid))
            allianceMemberItemData.hasBeenDistributed = true;
          else if (this.HasLeftAlliance(current.uid))
            allianceMemberItemData.hasLeftAlliace = true;
          else if (!this.HasContributed(current.uid))
            allianceMemberItemData.hasNotContributed = true;
          this.canNotBeDistributed.Add(allianceMemberItemData);
        }
      }
    }
    this.canBeDistributed.Sort(new Comparison<AllianceMemberItemData>(this.CompareCanBeDistributed));
    this.canNotBeDistributed.Sort(new Comparison<AllianceMemberItemData>(this.CompareCanBeDistributed));
    this.MergeDataList(this.leaderView, this.canBeDistributed, this.canNotBeDistributed);
    this.MergeDataList(this.memberView, this.canBeDistributed, this.canNotBeDistributed);
    this.memberView.Sort(new Comparison<AllianceMemberItemData>(this.CompareCanNotBeDistributed));
    if (PlayerData.inst.allianceData == null)
      return;
    this.ShowMemberView(this.IsAllianceLeader());
  }

  private void MergeDataList(List<AllianceMemberItemData> desList, List<AllianceMemberItemData> firstSrc, List<AllianceMemberItemData> secondSrc)
  {
    desList.AddRange((IEnumerable<AllianceMemberItemData>) firstSrc);
    desList.AddRange((IEnumerable<AllianceMemberItemData>) secondSrc);
  }

  private void ShowMemberView(bool isAllianceLeader)
  {
    List<AllianceMemberItemData> allianceMemberItemDataList = !isAllianceLeader || this.AreAllRewardsDistributed() ? this.memberView : this.leaderView;
    for (int index = 0; index < allianceMemberItemDataList.Count; ++index)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.memberTemplate);
      gameObject.transform.parent = this.leftGrid.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localScale = Vector3.one;
      gameObject.SetActive(true);
      AllianceMemberItemRender component = gameObject.GetComponent<AllianceMemberItemRender>();
      component.OnClickedHandler += new System.Action<AllianceMemberItemRender>(this.OnMemberClickedHandler);
      component.FeedData(allianceMemberItemDataList[index], this.rewardNotReady);
      this.memberRenderList.Add(component);
    }
    this.leftGrid.repositionNow = true;
    this.OnMemberClickedHandler(this.memberRenderList[0]);
  }

  private void UpdateAllianceRewards()
  {
    if (this.rewardData != null && this.rewardData.reward_list != null)
    {
      this.rewardDataList.Clear();
      for (int index1 = 0; index1 < this.rewardData.reward_list.Count; ++index1)
      {
        int index2 = this.rewardData.reward_list[index1].index;
        int itemId = this.rewardData.reward_list[index1].item_id;
        int num = 1;
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
        this.rewardDataList.Add(new AllianceRewardItemData()
        {
          uid = this.rewardData.reward_list[index1].uid,
          count = num,
          itemInfo = itemStaticInfo,
          index = index2,
          itemId = itemId
        });
      }
      this.ShowRewardView();
    }
    if (!this.rewardNotReady)
      return;
    this.noAllianceReward.gameObject.SetActive(true);
    this.noAllianceReward.text = Utils.XLAT("alliance_warfare_alliance_reward_not_ready");
    this.condition.gameObject.SetActive(false);
    this.distributeBtn.gameObject.SetActive(false);
  }

  private void ShowRewardView()
  {
    if (this.rewardDataList.Count > 0)
    {
      this.noAllianceReward.gameObject.SetActive(false);
      for (int index = 0; index < this.rewardDataList.Count; ++index)
      {
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.rewardTemplate);
        gameObject.transform.parent = this.rightGrid.transform;
        gameObject.transform.localPosition = Vector3.zero;
        gameObject.transform.localScale = Vector3.one;
        gameObject.SetActive(true);
        AllianceRewardItemRender component = gameObject.GetComponent<AllianceRewardItemRender>();
        component.OnClickedHandler += new System.Action<AllianceRewardItemRender>(this.OnRewardClickedHandler);
        component.FeedData(this.rewardDataList[index]);
        this.rewardRenderList.Add(component);
      }
      this.rightGrid.repositionNow = true;
    }
    else
    {
      this.noAllianceReward.gameObject.SetActive(true);
      this.noAllianceReward.text = Utils.XLAT("help_alliance_warfare_no_alliance_reward_hint");
      this.condition.gameObject.SetActive(false);
      this.distributeBtn.gameObject.SetActive(false);
    }
  }

  private int CompareCanBeDistributed(AllianceMemberItemData dataL, AllianceMemberItemData dataR)
  {
    if (dataL.score == dataR.score)
      return Math.Sign(dataR.title - dataL.title);
    return Math.Sign(dataR.score - dataL.score);
  }

  private int CompareCanNotBeDistributed(AllianceMemberItemData dataL, AllianceMemberItemData dataR)
  {
    if (dataL.score != dataR.score)
      return Math.Sign(dataR.score - dataL.score);
    if (dataL.title == dataR.title)
      return Math.Sign(dataL.uid - dataR.uid);
    return Math.Sign(dataR.title - dataL.title);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.ClearData();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void ClearData()
  {
    this.ClearMemberRenderList();
    this.ClearRewardRenderList();
    this.ClearMemeberLists();
    this.ClearRewardsLists();
  }

  private void ClearMemeberLists()
  {
    this.canBeDistributed.Clear();
    this.canNotBeDistributed.Clear();
    this.leaderView.Clear();
    this.memberView.Clear();
  }

  private void ClearRewardsLists()
  {
    this.rewardDataList.Clear();
  }

  private void ClearMemberRenderList()
  {
    for (int index = 0; index < this.memberRenderList.Count; ++index)
    {
      this.memberRenderList[index].gameObject.transform.parent = (Transform) null;
      this.memberRenderList[index].gameObject.SetActive(false);
      this.memberRenderList[index].OnClickedHandler -= new System.Action<AllianceMemberItemRender>(this.OnMemberClickedHandler);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.memberRenderList[index].gameObject);
    }
    this.memberRenderList.Clear();
  }

  private void ClearRewardRenderList()
  {
    for (int index = 0; index < this.rewardRenderList.Count; ++index)
    {
      this.rewardRenderList[index].gameObject.transform.parent = (Transform) null;
      this.rewardRenderList[index].gameObject.SetActive(false);
      this.rewardRenderList[index].OnClickedHandler -= new System.Action<AllianceRewardItemRender>(this.OnRewardClickedHandler);
      this.rewardRenderList[index].itemRender.Release();
      UnityEngine.Object.Destroy((UnityEngine.Object) this.rewardRenderList[index].gameObject);
    }
    this.rewardRenderList.Clear();
  }

  private void OnMemberClickedHandler(AllianceMemberItemRender render)
  {
    if (!this.IsAllianceLeader())
      return;
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.currentSelectedMember)
    {
      this.currentSelectedMember = render;
      this.currentSelectedMember.Select = true;
    }
    else
    {
      if (!((UnityEngine.Object) this.currentSelectedMember != (UnityEngine.Object) render))
        return;
      this.currentSelectedMember.Select = false;
      this.currentSelectedMember = render;
      this.currentSelectedMember.Select = true;
    }
  }

  private void OnRewardClickedHandler(AllianceRewardItemRender render)
  {
    if (!this.IsAllianceLeader())
      return;
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.currentSelectedReward)
    {
      this.currentSelectedReward = render;
      this.currentSelectedReward.Select = true;
    }
    else if ((UnityEngine.Object) this.currentSelectedReward != (UnityEngine.Object) render)
    {
      this.currentSelectedReward.Select = false;
      this.currentSelectedReward = render;
      this.currentSelectedReward.Select = true;
    }
    if (this.currentSelectedReward.Data.uid == 0L)
      this.distributeBtn.isEnabled = true;
    else
      this.distributeBtn.isEnabled = false;
  }

  public void OnDistributeBtnClicked()
  {
    if (!this.IsAllianceLeader())
      return;
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.currentSelectedMember && (UnityEngine.Object) null != (UnityEngine.Object) this.currentSelectedReward)
    {
      if (this.HasBeenDistributed(this.currentSelectedMember.Data.uid))
        UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_lord_assigned"), (System.Action) null, 4f, false);
      else if (this.HasLeftAlliance(this.currentSelectedMember.Data.uid))
        UIManager.inst.toast.Show(Utils.XLAT("alliance_warfare_not_in_alliance_notice"), (System.Action) null, 4f, false);
      else if (!this.HasContributed(this.currentSelectedMember.Data.uid))
      {
        UIManager.inst.toast.Show(Utils.XLAT("alliance_warfare_zero_score_notice"), (System.Action) null, 4f, false);
      }
      else
      {
        this.distributedReward = this.currentSelectedReward;
        this.SendDistributeRequest(new System.Action<bool, object>(this.DistributeReqeusetCallback), true);
      }
    }
    else
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_gift_target_choosen"), (System.Action) null, 4f, false);
  }

  private void OnRequestCallback(bool ret, object data)
  {
    if (!ret || this.rewardData == null)
      return;
    this.UpdateAllianceMembers();
    this.UpdateAllianceRewards();
  }

  private void UpdateRewardData()
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.distributedReward))
      return;
    for (int index = 0; index < this.rewardData.reward_list.Count; ++index)
    {
      if (this.rewardData.reward_list[index].item_id == this.distributedReward.Data.itemId && this.rewardData.reward_list[index].index == this.distributedReward.Data.index)
        this.rewardData.reward_list[index].uid = this.currentSelectedMember.Data.uid;
    }
  }

  private void Decode(object orgData)
  {
    if (orgData == null)
      return;
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable.ContainsKey((object) "ret") && hashtable[(object) "ret"] == null)
      this.rewardNotReady = true;
    else
      this.rewardData = JsonReader.Deserialize<AllianceRewardData>(Utils.Object2Json(orgData));
  }

  private void RequestAllianceAwardCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.SendGetAllianceMemberRequest(new System.Action<bool, object>(this.AllianceMemberRequestCallback), false);
  }

  private void RequestAllianceAward(System.Action<bool, object> callback, bool blockScreen = false)
  {
    MessageHub.inst.GetPortByAction("AC:getAllianceReward").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data);
      if (callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }

  private void DistributeReqeusetCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.UpdateRewardData();
    this.ClearData();
    this.currentSelectedMember = (AllianceMemberItemRender) null;
    this.currentSelectedReward = (AllianceRewardItemRender) null;
    this.UpdateAllianceMembers();
    this.UpdateAllianceRewards();
  }

  private void SendDistributeRequest(System.Action<bool, object> callback, bool blockScreen = false)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "idx"] = (object) (this.rewardData.rank + ":" + this.currentSelectedReward.Data.itemId.ToString() + ":" + this.currentSelectedReward.Data.index.ToString());
    postData[(object) "to_uid"] = (object) this.currentSelectedMember.Data.uid;
    MessageHub.inst.GetPortByAction("AC:assignAllianceReward").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }

  private void AllianceMemberRequestCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.UpdateAllianceMembers();
    this.UpdateAllianceRewards();
  }

  private void SendGetAllianceMemberRequest(System.Action<bool, object> callback, bool blockScreen = true)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }
}
