﻿// Decompiled with JetBrains decompiler
// Type: ConfigMerlinTrialsShop
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigMerlinTrialsShop
{
  private List<MerlinTrialsShopInfo> _merlinTrialsShopInfoList = new List<MerlinTrialsShopInfo>();
  private Dictionary<string, MerlinTrialsShopInfo> _datas;
  private Dictionary<int, MerlinTrialsShopInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<MerlinTrialsShopInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, MerlinTrialsShopInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._merlinTrialsShopInfoList.Add(enumerator.Current);
    }
  }

  public void Clear()
  {
    if (this._datas != null)
      this._datas.Clear();
    if (this._dicByUniqueId != null)
      this._dicByUniqueId.Clear();
    if (this._merlinTrialsShopInfoList == null)
      return;
    this._merlinTrialsShopInfoList.Clear();
  }

  public List<MerlinTrialsShopInfo> GetInfoList()
  {
    return this._merlinTrialsShopInfoList;
  }

  public MerlinTrialsShopInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (MerlinTrialsShopInfo) null;
  }

  public MerlinTrialsShopInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (MerlinTrialsShopInfo) null;
  }

  public List<string> GetCategoryList()
  {
    List<string> stringList = new List<string>();
    stringList.Add("all");
    for (int index = 0; index < this._merlinTrialsShopInfoList.Count; ++index)
    {
      if (!stringList.Contains(this._merlinTrialsShopInfoList[index].category))
        stringList.Add(this._merlinTrialsShopInfoList[index].category);
    }
    return stringList;
  }
}
