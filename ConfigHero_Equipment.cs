﻿// Decompiled with JetBrains decompiler
// Type: ConfigHero_Equipment
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigHero_Equipment
{
  public Dictionary<int, Hero_EquipmentData> datas;
  private Dictionary<string, int> dicByUniqueId;

  public ConfigHero_Equipment()
  {
    this.datas = new Dictionary<int, Hero_EquipmentData>();
    this.dicByUniqueId = new Dictionary<string, int>();
  }

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exsit", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        ConfigManager.GetIndex(inHeader, "type");
        ConfigManager.GetIndex(inHeader, "quality");
        int index2 = ConfigManager.GetIndex(inHeader, "essence");
        int[] numArray1 = new int[4];
        int[] numArray2 = new int[4];
        int[] numArray3 = new int[4];
        for (int index3 = 0; index3 < 4; ++index3)
        {
          numArray1[index3] = ConfigManager.GetIndex(inHeader, "benefit_id_" + (object) (index3 + 1));
          numArray2[index3] = ConfigManager.GetIndex(inHeader, "benefit_min_" + (object) (index3 + 1));
          numArray3[index3] = ConfigManager.GetIndex(inHeader, "benefit_max_" + (object) (index3 + 1));
        }
        int index4 = ConfigManager.GetIndex(inHeader, "set_id");
        int index5 = ConfigManager.GetIndex(inHeader, "loc_name_id");
        int index6 = ConfigManager.GetIndex(inHeader, "loc_description_id");
        int index7 = ConfigManager.GetIndex(inHeader, "asset_filename");
        int index8 = ConfigManager.GetIndex(inHeader, "prefab1");
        int index9 = ConfigManager.GetIndex(inHeader, "prefab2");
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          Hero_EquipmentData data = new Hero_EquipmentData();
          int result;
          if (int.TryParse(key.ToString(), out result))
          {
            ArrayList arr = hashtable[key] as ArrayList;
            if (arr != null)
            {
              data.InternalID = result;
              data.ID = ConfigManager.GetString(arr, index1);
              data.Essence = ConfigManager.GetInt(arr, index2);
              for (int index3 = 0; index3 < 4; ++index3)
              {
                if (!string.IsNullOrEmpty(ConfigManager.GetString(arr, numArray1[index3])))
                  data.benefits.Add(new Hero_EquipmentData.Benefit()
                  {
                    benefitId = ConfigManager.GetInt(arr, numArray1[index3]),
                    minValue = ConfigManager.GetFloat(arr, numArray2[index3]),
                    maxValue = ConfigManager.GetFloat(arr, numArray3[index3])
                  });
              }
              data.SetInternalID = ConfigManager.GetInt(arr, index4);
              data.LOC_Name_ID = ConfigManager.GetString(arr, index5);
              data.LOC_Description_ID = ConfigManager.GetString(arr, index6);
              data.Asset_Filename = ConfigManager.GetString(arr, index7);
              data.Prefab1 = ConfigManager.GetString(arr, index8);
              data.Prefab2 = ConfigManager.GetString(arr, index9);
              this.PushData(data.InternalID, data.ID, data);
            }
          }
        }
      }
    }
  }

  public Dictionary<int, Hero_EquipmentData>.ValueCollection Values
  {
    get
    {
      return this.datas.Values;
    }
  }

  private void PushData(int internalId, string uniqueId, Hero_EquipmentData data)
  {
    if (this.datas.ContainsKey(internalId))
      return;
    this.datas.Add(internalId, data);
    if (!this.dicByUniqueId.ContainsKey(uniqueId))
      this.dicByUniqueId.Add(uniqueId, internalId);
    ConfigManager.inst.AddData(internalId, (object) data);
  }

  public void Clear()
  {
    if (this.datas != null)
      this.datas.Clear();
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
  }

  public Hero_EquipmentData GetData(int internalId)
  {
    if (this.datas.ContainsKey(internalId))
      return this.datas[internalId];
    return (Hero_EquipmentData) null;
  }

  public Hero_EquipmentData GetData(string uniqueId)
  {
    if (!this.dicByUniqueId.ContainsKey(uniqueId))
      return (Hero_EquipmentData) null;
    return this.datas[this.dicByUniqueId[uniqueId]];
  }

  public string GetEquipmentAlter(int inInternalId)
  {
    if (this.datas[inInternalId] != null)
      return this.datas[inInternalId].Asset_AlterName;
    return string.Empty;
  }

  public string GetEquipmentName(int inInternalId)
  {
    if (this.datas[inInternalId] != null)
      return this.datas[inInternalId].Asset_Filename;
    return string.Empty;
  }

  public string GetGemsSpriteName(int inInternalId)
  {
    return this.GetEquipmentName(inInternalId);
  }

  public void GetAllCustomization(List<Hero_EquipmentData> ioHeadList, List<Hero_EquipmentData> ioHairList)
  {
  }
}
