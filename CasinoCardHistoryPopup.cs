﻿// Decompiled with JetBrains decompiler
// Type: CasinoCardHistoryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class CasinoCardHistoryPopup : Popup
{
  private float tipShowOffsetX = 0.65f;
  private ArrayList openedCellList = new ArrayList();
  public GameObject tipItem;
  public UILabel tipName;
  public UILabel tipDetails;
  public UILabel buttonText;
  public CasinoCardHistoryPopup.CardItemInfo[] cardItemInfo;
  private long uid;
  private int currentChestId;
  private bool isFromShare;
  private float tipShowOffsetY;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    CasinoCardHistoryPopup.Parameter parameter = orgParam as CasinoCardHistoryPopup.Parameter;
    if (parameter != null)
    {
      this.uid = parameter.uid;
      this.currentChestId = parameter.chestId;
      this.openedCellList = parameter.openedCellList;
      this.isFromShare = parameter.isFromShare;
    }
    this.ShowCardHistoryDetails();
  }

  public void ShowCardHistoryDetails()
  {
    if (this.currentChestId == -1)
      MessageHub.inst.GetPortByAction("casino:getChest").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        Hashtable inData = data as Hashtable;
        if (inData == null || inData[(object) "chest_id"] == null)
          return;
        DatabaseTools.UpdateData(inData, "chest_id", ref this.currentChestId);
        DatabaseTools.CheckAndParseOrgData(inData[(object) "opened"], out this.openedCellList);
        this.ShowAllCardItemData(this.currentChestId, this.openedCellList);
      }), true);
    else
      this.ShowAllCardItemData(this.currentChestId, this.openedCellList);
    this.UpdateShareBtnText();
  }

  public void ShowAllCardItemData(int chestId, ArrayList openedCellList)
  {
    if (chestId == -1)
      return;
    CasinoChestInfo casinoChestInfo = ConfigManager.inst.DB_CasinoChest.GetItem(chestId);
    ConfigManager.inst.DB_CasinoChest.GetCasinoChestInfoList();
    Dictionary<int, CasinoChestInfo.RewardItemInfo> rewardItemInfo = casinoChestInfo.GetRewardItemInfo();
    for (int index = 0; index < this.cardItemInfo.Length; ++index)
    {
      CasinoCardHistoryPopup.CardItemInfo cardItemInfo = this.cardItemInfo[index];
      string path = string.Empty;
      if (cardItemInfo != null && rewardItemInfo[index] != null)
      {
        cardItemInfo.itemId = -1;
        cardItemInfo.type = rewardItemInfo[index].type;
        if (rewardItemInfo[index].type == "item")
        {
          int internalId = !string.IsNullOrEmpty(rewardItemInfo[index].reward) ? int.Parse(rewardItemInfo[index].reward) : -1;
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
          cardItemInfo.itemId = internalId;
          if (itemStaticInfo != null)
            path = itemStaticInfo.ImagePath;
        }
        else
          path = "Texture/ItemIcons/icon_casino_" + rewardItemInfo[index].type.ToLower();
        if (!string.IsNullOrEmpty(path))
          BuilderFactory.Instance.HandyBuild((UIWidget) cardItemInfo.texture, path, (System.Action<bool>) null, true, false, string.Empty);
        if (casinoChestInfo.highlightIndex == index + 1)
          CasinoFunHelper.Instance.PlayParticles(cardItemInfo.highlight);
      }
    }
    this.UpdateChangedItemInfo();
  }

  private void UpdateShareBtnText()
  {
    if (!this.isFromShare)
      this.buttonText.text = Utils.XLAT("tavern_uppercase_share");
    else
      this.buttonText.text = Utils.XLAT("tavern_rewards_invite_to_try");
  }

  private void UpdateShareBtnStatus(bool enable)
  {
    UIButton component = this.buttonText.transform.parent.GetComponent<UIButton>();
    component.isEnabled = enable;
    component.UpdateColor(true);
  }

  private void UpdateChangedItemInfo()
  {
    if (this.openedCellList == null)
      return;
    for (int index = 0; index < this.openedCellList.Count; ++index)
    {
      Hashtable openedCell = this.openedCellList[index] as Hashtable;
      if (openedCell != null)
      {
        int outData1 = -1;
        int outData2 = -1;
        DatabaseTools.UpdateData(openedCell, "open_index", ref outData1);
        DatabaseTools.UpdateData(openedCell, "config_index", ref outData2);
        this.UpdateClickedItemInfo(outData1 - 1, outData2 - 1, index + 1);
      }
    }
  }

  private void UpdateClickedItemInfo(int clickedIndex, int hitIndex, int order)
  {
    if (hitIndex < 0 || hitIndex > this.cardItemInfo.Length - 1 || this.currentChestId == -1)
      return;
    int index = hitIndex;
    string empty = string.Empty;
    CasinoChestInfo casinoChestInfo = ConfigManager.inst.DB_CasinoChest.GetItem(this.currentChestId);
    CasinoChestInfo.RewardItemInfo curItemData = casinoChestInfo.GetRewardItemInfo()[index];
    CasinoCardHistoryPopup.CardItemInfo cardItemInfo = this.cardItemInfo[index];
    if (cardItemInfo != null)
    {
      int num = !string.IsNullOrEmpty(curItemData.reward) ? int.Parse(curItemData.reward) : -1;
      cardItemInfo.itemId = num;
      string rewardItemPath = this.GetRewardItemPath(curItemData);
      if (!string.IsNullOrEmpty(rewardItemPath))
        BuilderFactory.Instance.HandyBuild((UIWidget) cardItemInfo.texture, rewardItemPath, (System.Action<bool>) null, true, false, string.Empty);
      if (casinoChestInfo.highlightIndex == index + 1)
        CasinoFunHelper.Instance.PlayParticles(cardItemInfo.highlight);
    }
    this.cardItemInfo[index].step.text = string.Format(Utils.XLAT("tavern_rewards_step_num"), (object) order.ToString());
    this.cardItemInfo[index].step.transform.parent.gameObject.SetActive(true);
    this.cardItemInfo[index].collected.SetActive(true);
  }

  public string GetRewardItemPath(CasinoChestInfo.RewardItemInfo curItemData)
  {
    string str = string.Empty;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(!string.IsNullOrEmpty(curItemData.reward) ? int.Parse(curItemData.reward) : -1);
    if (itemStaticInfo != null)
      str = itemStaticInfo.ImagePath;
    if (!curItemData.type.Contains("item"))
      str = "Texture/ItemIcons/icon_casino_" + curItemData.type.ToLower();
    return str;
  }

  public void OnCollectedIconPressed(int clickedId)
  {
    this.ShowClickedItemTipInfo(clickedId);
    if (clickedId >= this.cardItemInfo.Length)
      return;
    Vector3 position1 = this.cardItemInfo[clickedId].texture.transform.position;
    Vector3 position2 = this.tipItem.transform.position;
    this.tipItem.transform.position = new Vector3(position1.x - this.tipShowOffsetX, position1.y, position2.z);
    this.tipItem.SetActive(true);
  }

  public void ShowClickedItemTipInfo(int clickedIndex)
  {
    if (clickedIndex > this.cardItemInfo.Length - 1)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.cardItemInfo[clickedIndex].itemId);
    this.tipName.text = "~";
    this.tipDetails.text = "~";
    if (itemStaticInfo != null)
    {
      this.tipName.text = itemStaticInfo.LocName;
      this.tipDetails.text = itemStaticInfo.LocDescription;
    }
    else
    {
      if (this.cardItemInfo[clickedIndex].type.Contains("item"))
        return;
      this.tipName.text = this.cardItemInfo[clickedIndex].type;
      this.tipDetails.text = Utils.XLAT("tavern_cards_rewards_multiply_description");
    }
  }

  public void OnShareBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    if (!this.isFromShare)
      UIManager.inst.OpenPopup("Casino/ConfirmationPopup", (Popup.PopupParameter) new CasinoConfirmationPopup.Parameter()
      {
        type = 0
      });
    else
      CasinoFunHelper.Instance.ShowFirstCasinoScene();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public long uid;
    public int chestId;
    public ArrayList openedCellList;
    public bool isFromShare;
  }

  [Serializable]
  public class CardItemInfo
  {
    [HideInInspector]
    public int itemId;
    [HideInInspector]
    public string type;
    public UITexture texture;
    public UILabel step;
    public GameObject collected;
    public GameObject highlight;
  }
}
