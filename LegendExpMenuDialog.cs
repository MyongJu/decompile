﻿// Decompiled with JetBrains decompiler
// Type: LegendExpMenuDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class LegendExpMenuDialog : UI.Dialog
{
  private List<LegendHeroRenderer> m_LegendList = new List<LegendHeroRenderer>();
  public GameObject m_HeroPrefab;
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  public LegendExpUseSlot[] m_UseSlots;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateLegendList();
    this.UpdatePotions();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.ClearLegendList();
  }

  private void ClearLegendList()
  {
    int count = this.m_LegendList.Count;
    for (int index = 0; index < count; ++index)
    {
      LegendHeroRenderer legend = this.m_LegendList[index];
      legend.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) legend.gameObject);
    }
    this.m_LegendList.Clear();
  }

  private void UpdateLegendList()
  {
    this.ClearLegendList();
    List<LegendData> underRecruitLegends = DBManager.inst.DB_Legend.GetTotalUnderRecruitLegends();
    int count = underRecruitLegends.Count;
    for (int index = 0; index < count; ++index)
    {
      LegendData legendData = underRecruitLegends[index];
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_HeroPrefab);
      gameObject.SetActive(true);
      gameObject.transform.SetParent(this.m_Grid.transform, false);
      LegendHeroRenderer component = gameObject.GetComponent<LegendHeroRenderer>();
      component.SetData(legendData);
      this.m_LegendList.Add(component);
    }
    this.m_Grid.sorting = UIGrid.Sorting.Custom;
    this.m_Grid.onCustomSort = new Comparison<Transform>(LegendExpMenuDialog.Compare);
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  private static int Compare(Transform x, Transform y)
  {
    LegendHeroRenderer component1 = x.GetComponent<LegendHeroRenderer>();
    LegendHeroRenderer component2 = y.GetComponent<LegendHeroRenderer>();
    LegendData legendData1 = component1.GetLegendData();
    LegendData legendData2 = component2.GetLegendData();
    long num1 = (long) (legendData2.Level - legendData1.Level);
    if (num1 != 0L)
      return (int) num1;
    long num2 = legendData2.Power - legendData1.Power;
    if (num2 != 0L)
      return (int) num2;
    return 0;
  }

  private void UpdatePotions()
  {
    for (int index = 0; index < this.m_UseSlots.Length; ++index)
    {
      LegendExpUseSlot useSlot = this.m_UseSlots[index];
      int itemCount = ItemBag.Instance.GetItemCount(LegendPotion.ITEMS[index]);
      useSlot.SetData(ConfigManager.inst.DB_Items.GetItem(LegendPotion.ITEMS[index]), itemCount, (System.Action<ItemStaticInfo>) null);
    }
  }
}
