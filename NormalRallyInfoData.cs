﻿// Decompiled with JetBrains decompiler
// Type: NormalRallyInfoData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;

public class NormalRallyInfoData : RallyInfoData
{
  private bool isReady;
  private RallyBuildingData buildData;
  private bool buildReady;

  private void LoadDataCallBack(long rallyId)
  {
    this.Refresh();
  }

  public RallyBuildingData BuildData
  {
    get
    {
      return this.buildData;
    }
  }

  protected override void Prepare()
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this.DataID);
    if (rallyData == null)
    {
      this.DispatchPrepareFail();
    }
    else
    {
      this.IsDefense = rallyData.IsDefense();
      this.OwerID = rallyData.ownerUid;
      AllianceWarManager.WarData data = PlayerData.inst.allianceWarManager.GetData(this.DataID);
      if (!this.isReady)
      {
        this.isReady = true;
        PlayerData.inst.allianceWarManager.LoadWarDetalData(this.DataID, rallyData.ownerAllianceId, new System.Action<long>(this.LoadDataCallBack));
      }
      else
      {
        if (this.buildData == null)
          this.buildData = RallyBuildingData.Create(rallyData.rallyId);
        if (this.buildData != null && !this.buildReady)
        {
          this.buildReady = true;
          this.buildData.Ready(new System.Action(((RallyInfoData) this).Refresh));
        }
        else
        {
          CityData cityData1 = DBManager.inst.DB_City.Get(rallyData.targetCityId);
          UserData userData;
          AllianceData allianceData;
          long num1;
          if (this.IsDefense)
          {
            userData = DBManager.inst.DB_User.Get(rallyData.targetUid);
            if (userData == null)
            {
              long id = 0;
              if (this.buildData != null)
              {
                id = this.buildData.OwnerID;
                this.TargetIsAllianceBuild = true;
              }
              userData = DBManager.inst.DB_User.Get(id);
            }
            allianceData = DBManager.inst.DB_Alliance.Get(rallyData.targetAllianceId);
            num1 = data.defenseCapacity;
          }
          else
          {
            userData = DBManager.inst.DB_User.Get(rallyData.ownerUid);
            allianceData = DBManager.inst.DB_Alliance.Get(rallyData.ownerAllianceId);
            num1 = data.rallyCapacity;
          }
          if (rallyData.type == RallyData.RallyType.TYPE_RALLY_PVP)
          {
            this.TargetID = rallyData.targetUid;
            this.Type = RallyInfoData.RallyDataType.PVP;
            if (rallyData.fortressId > 0)
              this.TargetID = (long) rallyData.fortressId;
            if (this.buildData != null)
            {
              this.TargetIsAllianceBuild = true;
              this.Type = RallyInfoData.RallyDataType.Building;
            }
          }
          else if (rallyData.type == RallyData.RallyType.TYPE_RALLY_GVE)
          {
            this.TargetID = rallyData.rallyId;
            this.Type = RallyInfoData.RallyDataType.GVE;
          }
          else
          {
            this.TargetID = rallyData.rallyId;
            this.Type = RallyInfoData.RallyDataType.RAB;
          }
          if (userData != null)
          {
            if (allianceData != null)
              this.LeaderName = "[" + allianceData.allianceAcronym + "] " + userData.userName;
            else
              this.LeaderName = userData.userName;
          }
          else if (this.buildData != null)
          {
            if (allianceData != null)
              this.LeaderName = "[" + allianceData.allianceAcronym + "] " + this.buildData.BuildName;
            else
              this.LeaderName = this.buildData.BuildName;
          }
          this.IsPrepare = true;
          this.IsInit = true;
          this.SetMarchList();
          long num2 = 0;
          for (int index = 0; index < this.MarchList.Count; ++index)
          {
            MarchData march = this.MarchList[index];
            num2 += (long) march.troopsInfo.totalCount;
          }
          this.JoinNum = this.MarchList.Count.ToString();
          this.TroopsInfo = Utils.FormatThousands(num2.ToString()) + "/" + Utils.FormatThousands(num1.ToString());
          this.TargetCityID = rallyData.targetCityId;
          if (cityData1 != null)
            this.ReinforceLocation = cityData1.cityLocation;
          this.ReinforceMarchType = MarchAllocDlg.MarchType.joinRally;
          if (this.IsDefense)
          {
            this.ReinforceMarchType = MarchAllocDlg.MarchType.reinforce;
            this.IsJoined = rallyData.targetUid == PlayerData.inst.uid;
            if (this.buildData != null)
            {
              this.ReinforceMarchType = this.buildData.ReinforceMarchType;
              this.ReinforceLocation = this.buildData.Location;
              this.DesTroopCount = (int) (num1 - num2);
              if (this.DesTroopCount < -1)
                this.DesTroopCount = -1;
            }
            if (this.ReinforceLocation.X == 0 && this.ReinforceLocation.Y == 0)
              this.ReinforceLocation = rallyData.location;
            this.EmptySlot = 0;
          }
          else
          {
            CityData cityData2 = DBManager.inst.DB_City.Get(rallyData.ownerCityId);
            this.IsJoined = rallyData.isJoined;
            this.EmptySlot = rallyData.unlockSlotNumber - this.MarchList.Count;
            if (cityData2 != null)
              this.ReinforceLocation = cityData2.cityLocation;
            if (this.Type == RallyInfoData.RallyDataType.GVE)
              this.ReinforceMarchType = MarchAllocDlg.MarchType.joinGveRally;
            else if (this.Type == RallyInfoData.RallyDataType.RAB)
              this.ReinforceMarchType = MarchAllocDlg.MarchType.joinRabRally;
          }
          this.RemainCount = num1 - num2;
        }
      }
    }
  }

  private int CompareMarch(MarchData a, MarchData b)
  {
    return a.marchId.CompareTo(b.marchId);
  }

  private void SetMarchList()
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this.DataID);
    if (!this.IsDefense)
    {
      List<MarchData> marchDataList = new List<MarchData>();
      Dictionary<long, RallySlotInfo>.ValueCollection.Enumerator enumerator = rallyData.slotsInfo_joined.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.marchId > 0L)
        {
          MarchData marchData = DBManager.inst.DB_March.Get(enumerator.Current.marchId);
          if (marchData != null && !marchDataList.Contains(marchData))
            marchDataList.Add(marchData);
        }
      }
      this.MarchList = marchDataList;
    }
    else
    {
      switch (this.Type)
      {
        case RallyInfoData.RallyDataType.PVP:
        case RallyInfoData.RallyDataType.GVE:
        case RallyInfoData.RallyDataType.RAB:
          this.MarchList = DBManager.inst.DB_March.GetReinforceByUid(rallyData.targetUid);
          break;
        case RallyInfoData.RallyDataType.Building:
          this.MarchList = this.buildData.MarchList;
          break;
      }
    }
    this.MarchList.Sort(new Comparison<MarchData>(this.CompareMarch));
  }

  protected override void Process()
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this.DataID);
    this.CanJoin = false;
    switch (rallyData.state)
    {
      case RallyData.RallyState.waitRallying:
        this.CanJoin = true;
        int time1 = rallyData.waitTimeDuration.endTime - Oscillator.Instance.ServerTime.ServerTimestamp;
        if (time1 < 0)
          time1 = 0;
        this.ProgressValue = (float) (1.0 - (double) time1 * (double) rallyData.waitTimeDuration.oneOverTimeDuration);
        this.ProgressContent = ScriptLocalization.Get("war_rally_rallying", true) + " " + Utils.FormatTime(time1, false, false, true);
        break;
      case RallyData.RallyState.waitAttacking:
        this.ProgressValue = 1f;
        this.ProgressContent = ScriptLocalization.Get("war_rally_uppercase_rallied", true);
        this.IsCallBack = true;
        break;
      case RallyData.RallyState.attacking:
        MarchData marchData = DBManager.inst.DB_March.Get(rallyData.rallyMarchId);
        this.IsCallBack = true;
        if (marchData == null)
          break;
        JobManager.Instance.GetJob(marchData.jobId);
        int num1 = marchData.endTime - marchData.startTime;
        int num2 = NetServerTime.inst.ServerTimestamp - marchData.startTime;
        float num3 = 1f - (float) num2 / (float) num1;
        int time2 = num1 - num2;
        if ((double) num3 < 1.0)
        {
          this.ProgressValue = num3;
          this.ProgressContent = ScriptLocalization.Get("war_rally_marching", true) + " " + Utils.FormatTime(time2, false, false, true);
          break;
        }
        this.ProgressValue = 1f;
        this.ProgressContent = ScriptLocalization.Get("war_rally_uppercase_rallied", true);
        break;
      case RallyData.RallyState.done:
        this.ProgressValue = 1f;
        this.ProgressContent = ScriptLocalization.Get("war_rally_uppercase_rallied", true);
        break;
    }
  }
}
