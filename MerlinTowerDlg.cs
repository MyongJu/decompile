﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;
using UnityEngine;

public class MerlinTowerDlg : UI.Dialog
{
  private const string TITLE = "tower_name";
  private const string ENTER_TOWER = "tower_enter_button";
  private const string CONTINUE_EXPLORING = "dragon_knight_uppercase_continue_exploring";
  public UILabel title;
  public UILabel redeem;
  public UILabel enterTower;
  public UITexture currencyIcon;
  public UILabel currencyCount;
  public UILabel labelCurrentLevel;
  public UILabel labelMaxLevel;
  public GameObject rootLeftTime;
  public UILabel labelLeftTime;
  public GameObject rootGatherLeftTime;
  public UILabel labelGatherLeftTime;
  private UserMerlinTowerData userData;
  private bool _requestEnter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    this._requestEnter = false;
    this.userData = MerlinTowerPayload.Instance.UserData;
    RequestManager.inst.AddObserver("magicTower:climbTower", new System.Action<string, bool>(this.OnClimbResponse));
    MessageHub.inst.GetPortByAction("magic_tower_over").AddEvent(new System.Action<object>(this.OnMerlinTowerOver));
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondeTick);
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    RequestManager.inst.RemoveObserver("magicTower:climbTower", new System.Action<string, bool>(this.OnClimbResponse));
    MessageHub.inst.GetPortByAction("magic_tower_over").RemoveEvent(new System.Action<object>(this.OnMerlinTowerOver));
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondeTick);
  }

  protected void OnMerlinTowerOver(object data)
  {
    this.UpdateUI();
  }

  private void OnSecondeTick(int delta)
  {
    this.UpdateTimes();
    if (!this._requestEnter)
      return;
    this._requestEnter = false;
    this.EnterTower();
  }

  private void UpdateTimes()
  {
    UserMerlinTowerData userData = MerlinTowerPayload.Instance.UserData;
    if (userData == null)
      return;
    if (userData.IsInKingdom)
    {
      this.rootLeftTime.SetActive(true);
      JobHandle job = JobManager.Instance.GetJob(userData.mineFieldJobId);
      if (job != null)
        this.labelLeftTime.text = Utils.FormatTime(job.LeftTime(), false, false, true);
    }
    else
      this.rootLeftTime.SetActive(false);
    if (userData.mineJobId != 0L)
    {
      this.rootGatherLeftTime.SetActive(true);
      JobHandle job = JobManager.Instance.GetJob(userData.mineJobId);
      if (job == null)
        return;
      this.labelGatherLeftTime.text = Utils.FormatTime(job.LeftTime(), false, false, true);
    }
    else
      this.rootGatherLeftTime.SetActive(false);
  }

  private void UpdateUI()
  {
    this.title.text = ScriptLocalization.Get("tower_name", true);
    this.UpdateCurrency();
    this.UpdateEnterBtnState();
    this.UpdateTimes();
    UserMerlinTowerData userData = MerlinTowerPayload.Instance.UserData;
    this.labelCurrentLevel.text = userData == null || userData.CurrentTowerMainInfo == null ? "-" : userData.CurrentTowerMainInfo.Level.ToString();
    int maxHistoryLevel = MerlinTowerPayload.Instance.MaxHistoryLevel;
    this.labelMaxLevel.text = maxHistoryLevel <= 0 ? "-" : maxHistoryLevel.ToString();
  }

  private void UpdateEnterBtnState()
  {
    if (this.userData == null || this.userData.towerId == 0)
      this.enterTower.text = ScriptLocalization.Get("tower_enter_button", true);
    else
      this.enterTower.text = ScriptLocalization.Get("dragon_knight_uppercase_continue_exploring", true);
  }

  public void OnViewRankBtnClicked()
  {
    UIManager.inst.OpenDlg("MerlinTower/MerlinTowerRankDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnRedeemBtnClicked()
  {
    UIManager.inst.OpenDlg("DragonKnight/DungeonAncientStoreDialog", (UI.Dialog.DialogParameter) new DungeonAncientStoreDialog.Parameter()
    {
      group = "magic_tower_shop",
      currency = "item_magic_tower_coin_1",
      titleKey = "tower_shop_name"
    }, true, true, true);
  }

  private void UpdateCurrency()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_magic_tower_coin_1");
    int itemCount = ItemBag.Instance.GetItemCount(itemStaticInfo.internalId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.currencyIcon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, true, string.Empty);
    this.currencyCount.text = Utils.FormatThousands(itemCount.ToString());
    this.currencyCount.color = itemCount <= 0 ? Color.red : Color.white;
    this.redeem.text = ScriptLocalization.Get("trial_exchange_button", true);
  }

  public void OnEnterTowerBtnClicked()
  {
    if (MerlinTowerPayload.Instance.UserData.towerId == 0)
    {
      MagicTowerMainInfo dataWithLevel = ConfigManager.inst.DB_MagicTowerMain.GetDataWithLevel(1);
      if (dataWithLevel == null)
        return;
      MerlinTowerPayload.Instance.SendClaimbTowerRequest(dataWithLevel.internalId);
    }
    else
      this._requestEnter = true;
  }

  public void OnClimbResponse(string portType, bool result)
  {
    this._requestEnter = true;
  }

  protected void EnterTower()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    MerlinTowerFacade.RequestEnter(false, 0);
  }
}
