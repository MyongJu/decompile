﻿// Decompiled with JetBrains decompiler
// Type: PlayerProfileDlgDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PlayerProfileDlgDetail : UI.Dialog
{
  private const string IMAGE_PRE = "Texture/Hero/player_portrait_";
  public UILabel title;
  public PlayerProfileInfo info;
  public AllianceSymbol mAllianceSymbol;
  public UITexture image;
  public UIScrollView scrollViewSelf;
  public UIScrollView scrollViewOther;
  public UITable detailTableSelf;
  public UITable detailTableOhter;
  public GameObject otherView;
  public GameObject selfView;
  public UIButton btnSendWonderGift;
  public UIButton btn1;
  public UILabel btnLabel1;
  public UIButton btn2;
  public UILabel btnLabel2;
  public UIButton btn3;
  public UILabel btnLabel3;
  public UIButton btn4;
  public UILabel btnLabel4;
  public Icon contentTemplate;
  public Icon titleTemplate;
  public UISprite titleSprite;
  public UITexture titleTexture;
  public UILabel titleText;
  public UISprite heroProfileNewSprite;
  public UIButton nominateButton;
  public PlayerProfileDetailEquipmentPanel equipmentPanel;
  public ArtifactSlot[] allArtifactSlot;
  public ArtifactTimeLimitedSlot artifactTimeLimitedSlot;
  public UIGrid buttonContainer;
  private long uid;
  private UIScrollView scrollView;
  private UITable detailTable;

  public override void OnOpen(UIControler.UIParameter param)
  {
    this.uid = (param as PlayerProfileDlgDetail.Parameter).uid;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    DBManager.inst.GetInventory(BagType.Hero).ClearThirdPartyInventory();
    DBManager.inst.DB_Gems.ClearThirdPartyInventory();
  }

  public override void OnShow(UIControler.UIParameter param)
  {
    base.OnShow(param);
    PlayerProfileDlgDetail.Parameter parameter = param as PlayerProfileDlgDetail.Parameter;
    if (parameter != null)
      this.uid = parameter.uid;
    this.AddEventHandler();
    this.SendMsg(this.uid);
    this.LoadUserBasicInfo(this.uid);
    this.UpdateState();
  }

  public override void OnHide(UIControler.UIParameter param)
  {
    base.OnHide(param);
    this.btn1.onClick.Clear();
    this.btn2.onClick.Clear();
    this.btn3.onClick.Clear();
    this.btn4.onClick.Clear();
    this.artifactTimeLimitedSlot.ClearData();
    this.RemoveEventHandler();
  }

  public void OnBackBtnClick()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnRenameBtnClick()
  {
    UIManager.inst.OpenPopup("PlayerProfileRenamePopup", (Popup.PopupParameter) new PlayerProfileRenamePopup.Parameter()
    {
      closeCallBack = new System.Action(this.OnRenamePopupClose)
    });
  }

  public void OnNominateBtnClick()
  {
    UserData userData = DBManager.inst.DB_User.Get(this.uid);
    if (userData == null)
      return;
    DB.WonderData wd = DBManager.inst.DB_Wonder.Get((long) userData.world_id);
    if (wd == null || !(wd.State == "protected"))
      return;
    MessageHub.inst.GetPortByAction("wonder:loadTitleInfo").SendRequest(new Hashtable()
    {
      {
        (object) "target_world_id",
        (object) userData.world_id
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      if (data != null)
        ConfigManager.inst.DB_WonderTitle.UpdateWonderTitleData(data as Hashtable, true);
      if (PlayerData.inst.userData.IsKing)
      {
        UIManager.inst.OpenDlg("Wonder/KingdomTitleDlg", (UI.Dialog.DialogParameter) new KingdomTitleDlg.Parameter()
        {
          kingdomId = userData.world_id,
          needNominateUserId = (wd == null || wd.KING_UID != PlayerData.inst.uid || wd.KING_UID == this.uid ? 0L : this.uid)
        }, 1 != 0, 1 != 0, 1 != 0);
      }
      else
      {
        WonderTitleInfo wonderTitleInfo = ConfigManager.inst.DB_WonderTitle.Get(userData == null ? string.Empty : userData.Title.ToString());
        if (wonderTitleInfo == null)
          return;
        UIManager.inst.OpenPopup("Wonder/KingdomTitleInfoPopup", (Popup.PopupParameter) new KingdomTitleInfoPopup.Parameter()
        {
          kingdomId = userData.world_id,
          wonderTitleInfo = wonderTitleInfo,
          showKingBenefit = (userData != null && userData.IsKing),
          needViewInfo = false
        });
      }
    }), true);
  }

  private void OnRenamePopupClose()
  {
    this.UpdateUI();
  }

  public void OnChangePortraitClick()
  {
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileChangePortraitDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnChangeMottoBtnClick()
  {
    UIManager.inst.OpenPopup("PlayerProfileChangeMottoPopup", (Popup.PopupParameter) new PlayerProfileChangeMottoPopup.Parameter()
    {
      closeCallBack = new System.Action(this.UpdateUI)
    });
  }

  public void OnButtonSendWonderGiftClicked()
  {
    UserData userData = DBManager.inst.DB_User.Get(this.uid);
    if (userData == null)
      return;
    long num = PlayerData.inst.userData.world_id != userData.world_id ? 0L : this.uid;
    UIManager.inst.OpenDlg("Wonder/WonderGiftDlg", (UI.Dialog.DialogParameter) new WonderGiftDlg.Parameter()
    {
      receiverId = num
    }, true, true, true);
  }

  private void UpdateUI()
  {
    this.UpdateState();
    this.UpdateHeroInfo();
    this.UpdateTitle();
    this.UpdateSymbol();
    this.UpdateImage();
    this.UpdateBtns();
    this.UpdateHeroProfileNewStatus();
    this.buttonContainer.repositionNow = true;
    this.buttonContainer.Reposition();
  }

  private void UpdateHeroProfileNewStatus()
  {
    if (!(bool) ((UnityEngine.Object) this.heroProfileNewSprite))
      return;
    NGUITools.SetActive(this.heroProfileNewSprite.gameObject, HeroProfileUtils.HasNewHeroProfile());
  }

  private void UpdateState()
  {
    this.otherView.SetActive(this.uid != PlayerData.inst.uid);
    this.selfView.SetActive(this.uid == PlayerData.inst.uid);
    if (this.uid == PlayerData.inst.uid)
    {
      this.otherView.SetActive(false);
      this.selfView.SetActive(true);
      this.scrollView = this.scrollViewSelf;
      this.detailTable = this.detailTableSelf;
    }
    else
    {
      this.otherView.SetActive(true);
      this.selfView.SetActive(false);
      this.scrollView = this.scrollViewOther;
      this.detailTable = this.detailTableOhter;
    }
    this.UpdateArtifactStatus();
  }

  private void UpdateBtns()
  {
    UserData userData = DBManager.inst.DB_User.Get(this.uid);
    if (userData.allianceId != 0L)
    {
      if (userData.allianceId == PlayerData.inst.allianceId)
        this.btn1.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnAllianceBtnClick)));
      else
        this.btn1.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnOtherAllianceBtnClick)));
      this.btnLabel1.text = Utils.XLAT("id_uppercase_alliance");
    }
    else if (PlayerData.inst.allianceId != 0L)
    {
      if (PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid).title >= 4)
        this.btn1.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnInviteAllianceBtnClick)));
      else
        this.btn1.isEnabled = false;
      this.btnLabel1.text = Utils.XLAT("player_profile_alliance_invite");
    }
    else
    {
      this.btn1.isEnabled = false;
      this.btnLabel1.text = Utils.XLAT("id_uppercase_alliance");
    }
    this.btn2.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnCommentBtnClick)));
    this.btnLabel2.text = Utils.XLAT("player_profile_comment");
    this.btn3.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnMailBtnClick)));
    this.btnLabel3.text = Utils.XLAT("player_profile_mail");
    this.btn4.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnManagerBtnClick)));
    this.btnLabel4.text = Utils.XLAT("player_profile_manage");
    this.UpdateNominateBtnStatus();
    if (userData.world_id == PlayerData.inst.userData.world_id && PlayerData.inst.userData.IsKing)
      NGUITools.SetActive(this.btnSendWonderGift.gameObject, true);
    else
      NGUITools.SetActive(this.btnSendWonderGift.gameObject, false);
  }

  private void UpdateNominateBtnStatus()
  {
    UserData userData = DBManager.inst.DB_User.Get(this.uid);
    DB.WonderData wonderData = userData != null ? DBManager.inst.DB_Wonder.Get((long) userData.world_id) : (DB.WonderData) null;
    WonderTitleInfo wonderTitleInfo = ConfigManager.inst.DB_WonderTitle.Get(userData.Title.ToString());
    bool state = wonderData != null && wonderData.State == "protected" && (PlayerData.inst.userData.IsKing || userData.Title > 0);
    NGUITools.SetActive(this.nominateButton.gameObject, state);
    NGUITools.SetActive(this.titleSprite.gameObject, userData.Title <= 0);
    NGUITools.SetActive(this.titleTexture.gameObject, userData.Title > 0);
    this.titleText.text = userData.Title <= 0 ? Utils.XLAT("id_titles") : wonderTitleInfo.Name;
    if (!state || wonderTitleInfo == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.titleTexture, "Texture/Kingdom/" + wonderTitleInfo.icon, (System.Action<bool>) null, true, false, string.Empty);
  }

  private void UpdateTable(Hashtable args)
  {
    Transform transform = this.detailTable.transform;
    while (transform.childCount > 0)
    {
      Transform child = transform.GetChild(0);
      child.gameObject.SetActive(false);
      child.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) child.gameObject);
    }
    if (this.uid == PlayerData.inst.uid)
    {
      this.AddBacicPlayerprofileInfo("combat_stats", args);
      this.AddBacicPlayerprofileInfo("power", args);
      this.AddPlayerprofileInfo("military");
      this.AddPlayerprofileInfo("resource");
      this.AddPlayerprofileInfo("city_development");
      this.AddPlayerprofileInfo("city_defense");
    }
    else
      this.AddBacicPlayerprofileInfo("combat_stats", args);
    this.detailTable.Reposition();
  }

  private void AddBacicPlayerprofileInfo(string category, Hashtable args)
  {
    this.AddInfoTitle(category);
    List<ConfigPlayerProfileInfo> profileInfoByCategory = ConfigManager.inst.DB_PlayerProfile.GetPlayerProfileInfoByCategory(category);
    profileInfoByCategory.Sort((Comparison<ConfigPlayerProfileInfo>) ((x, y) => x.oder.CompareTo(y.oder)));
    for (int index = 0; index < profileInfoByCategory.Count; ++index)
    {
      ConfigPlayerProfileInfo playerProfileInfo = profileInfoByCategory[index];
      GameObject gameObject = NGUITools.AddChild(this.detailTable.gameObject, this.contentTemplate.gameObject);
      gameObject.SetActive(true);
      gameObject.GetComponent<Icon>().FeedData((IComponentData) new IconData((string) null, new string[2]
      {
        playerProfileInfo.Name,
        !args.ContainsKey((object) playerProfileInfo.ID) ? playerProfileInfo.ID : args[(object) playerProfileInfo.ID].ToString()
      }));
    }
    if (!((UnityEngine.Object) this.info.killValue != (UnityEngine.Object) null) || !args.ContainsKey((object) "troops_killed"))
      return;
    this.info.killValue.text = args[(object) "troops_killed"].ToString();
  }

  private void AddPlayerprofileInfo(string category)
  {
    this.AddInfoTitle(category);
    this.AddInfoByCategory(category);
  }

  private void AddInfoByCategory(string category)
  {
    List<ConfigPlayerProfileInfo> profileInfoByCategory = ConfigManager.inst.DB_PlayerProfile.GetPlayerProfileInfoByCategory(category);
    profileInfoByCategory.Sort((Comparison<ConfigPlayerProfileInfo>) ((x, y) => x.oder.CompareTo(y.oder)));
    for (int index = 0; index < profileInfoByCategory.Count; ++index)
    {
      ConfigPlayerProfileInfo playerProfileInfo = profileInfoByCategory[index];
      GameObject gameObject = NGUITools.AddChild(this.detailTable.gameObject, this.contentTemplate.gameObject);
      gameObject.SetActive(true);
      gameObject.GetComponent<Icon>().FeedData((IComponentData) new IconData((string) null, new string[2]
      {
        playerProfileInfo.Name,
        playerProfileInfo.DisplayContent
      }));
    }
  }

  private void AddInfoTitle(string title)
  {
    GameObject gameObject = NGUITools.AddChild(this.detailTable.gameObject, this.titleTemplate.gameObject);
    gameObject.SetActive(true);
    gameObject.GetComponent<Icon>().FeedData((IComponentData) new IconData((string) null, new string[1]
    {
      Utils.XLAT(string.Format("lord_details_{0}_subtitle", (object) title))
    }));
  }

  private void UpdateImage()
  {
    UserData userData = DBManager.inst.DB_User.Get(this.uid);
    if (userData.portrait < 0)
      return;
    Utils.SetPortrait(this.image, userData.PortraitPath);
  }

  private void UpdateTitle()
  {
    this.title.text = ScriptLocalization.Get("player_profile_name", true);
  }

  private void UpdateSymbol()
  {
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(DBManager.inst.DB_User.Get(this.uid).allianceId);
    if (allianceData != null)
    {
      this.mAllianceSymbol.gameObject.SetActive(true);
      this.mAllianceSymbol.SetSymbols(allianceData.allianceSymbolCode);
    }
    else
      this.mAllianceSymbol.gameObject.SetActive(false);
  }

  private void UpdateHeroInfo()
  {
    UserData userData = DBManager.inst.DB_User.Get(this.uid);
    this.equipmentPanel.SetData(userData);
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(userData.allianceId);
    this.info.name.text = ScriptLocalization.Get("player_profile_playername", true) + ":";
    this.info.nameValue.text = userData.userName;
    this.info.alliance.text = ScriptLocalization.Get("player_profile_alliance", true) + ":";
    this.info.allianceValue.text = allianceData == null ? "--" : allianceData.allianceFullName;
    this.info.kiongdom.text = ScriptLocalization.Get("player_profile_kingdom", true) + ":";
    this.info.kingdomValue.text = userData.world_id.ToString();
    this.info.group.text = ScriptLocalization.Get("id_kingdom_group", true) + ":";
    this.info.groupValue.text = userData.groupName;
    this.info.motto.text = ScriptLocalization.Get("player_profile_motto", true) + ":";
    this.info.mottoValue.text = userData.signature;
    this.info.power.text = ScriptLocalization.Get("player_profile_power", true) + ":";
    this.info.powerValue.text = userData.power.ToString();
    this.info.kill.text = ScriptLocalization.Get("player_profile_kills", true) + ":";
    this.scrollView.ResetPosition();
  }

  private void SendMsg(long uid)
  {
    HubPort portByAction = MessageHub.inst.GetPortByAction("player:loadUserProfile");
    Hashtable postData = new Hashtable();
    postData[(object) nameof (uid)] = (object) PlayerData.inst.uid;
    postData[(object) "target_uid"] = (object) uid;
    CityData byUid = DBManager.inst.DB_City.GetByUid(uid);
    postData[(object) "target_city_id"] = (object) (byUid != null ? byUid.cityId : 0L);
    portByAction.SendRequest(postData, new System.Action<bool, object>(this.OnProfileLoaded), true);
  }

  private void LoadUserBasicInfo(long uid)
  {
    HubPort portByAction = MessageHub.inst.GetPortByAction("Player:loadUserBasicInfo");
    Hashtable postData = new Hashtable();
    postData[(object) "target_uid"] = (object) uid;
    portByAction.SendRequest(postData, new System.Action<bool, object>(this.OnBasicInfoLoaded), true);
  }

  private void OnProfileLoaded(bool result, object args)
  {
    if (!result)
      return;
    this.UpdateTable(args as Hashtable);
  }

  private void OnBasicInfoLoaded(bool result, object args)
  {
    if (!result)
      return;
    this.UpdateUI();
  }

  private void OnAllianceBtnClick()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnOtherAllianceBtnClick()
  {
    UserData userData = DBManager.inst.DB_User.Get(this.uid);
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) userData.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("Alliance/AllianceJoinAndApplyDialog", (UI.Dialog.DialogParameter) new AllianceJoinAndApplyDialog.Parameter()
      {
        allianceData = DBManager.inst.DB_Alliance.Get(userData.allianceId),
        justView = (PlayerData.inst.allianceId > 0L)
      }, true, true, true);
    }), true);
  }

  private void OnManagerBtnClick()
  {
    Utils.PopUpCommingSoon();
  }

  private void OnMailBtnClick()
  {
    PlayerData.inst.mail.ComposeMail(new List<string>()
    {
      DBManager.inst.DB_User.Get(this.uid).userName
    }, (string) null, (string) null, 0 != 0);
  }

  public void onCopyNameClick()
  {
    Utils.SetClipboardText(DBManager.inst.DB_User.Get(this.uid).userName);
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_player_profile_copy_name_success", true), (System.Action) null, 1f, false);
  }

  private void OnInviteAllianceBtnClick()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceInviteConfirmPopup", (Popup.PopupParameter) new AllianceInviteConfirmPopup.Parameter()
    {
      uid = this.uid
    });
  }

  private void OnCommentBtnClick()
  {
    Utils.PopUpCommingSoon();
  }

  private void UpdateArtifactStatus()
  {
    foreach (ArtifactSlot artifactSlot in this.allArtifactSlot)
      artifactSlot.RefreshForPlayer(this.uid);
    this.artifactTimeLimitedSlot.RefreshForPlayer(this.uid, false);
  }

  private void OnHeroDataUpdated(long cityId)
  {
    this.UpdateArtifactStatus();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_hero.onDataUpdated -= new System.Action<long>(this.OnHeroDataUpdated);
    DBManager.inst.DB_hero.onDataUpdated += new System.Action<long>(this.OnHeroDataUpdated);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_hero.onDataUpdated -= new System.Action<long>(this.OnHeroDataUpdated);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public long uid;
  }
}
