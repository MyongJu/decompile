﻿// Decompiled with JetBrains decompiler
// Type: ParticleScaler
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class ParticleScaler : MonoBehaviour
{
  public float particleScale = 1f;
  public bool alsoScaleGameobject = true;
  public bool clearOldParticles = true;
  private float prevScale;

  private void Awake()
  {
    this.prevScale = this.particleScale;
  }

  private void LateUpdate()
  {
    this.Rescale();
  }

  private void Rescale()
  {
    if ((double) this.prevScale == (double) this.particleScale || (double) this.particleScale <= 0.0)
      return;
    if (this.alsoScaleGameobject)
      this.transform.localScale = new Vector3(this.particleScale, this.particleScale, this.particleScale);
    float scaleFactor = this.particleScale / this.prevScale;
    this.ScaleLegacySystems(scaleFactor);
    this.ScaleShurikenSystems(scaleFactor);
    this.ScaleTrailRenderers(scaleFactor);
    this.prevScale = this.particleScale;
  }

  private void ScaleShurikenSystems(float scaleFactor)
  {
    foreach (ParticleSystemRenderer componentsInChild in this.GetComponentsInChildren<ParticleSystemRenderer>())
    {
      componentsInChild.cameraVelocityScale *= scaleFactor;
      componentsInChild.lengthScale *= scaleFactor;
      componentsInChild.velocityScale *= scaleFactor;
    }
    foreach (ParticleSystem componentsInChild in this.GetComponentsInChildren<ParticleSystem>())
    {
      componentsInChild.startSpeed *= scaleFactor;
      componentsInChild.startSize *= scaleFactor;
      componentsInChild.gravityModifier *= scaleFactor;
      componentsInChild.Clear();
    }
  }

  private void ScaleLegacySystems(float scaleFactor)
  {
    ParticleEmitter[] componentsInChildren1 = this.GetComponentsInChildren<ParticleEmitter>();
    ParticleAnimator[] componentsInChildren2 = this.GetComponentsInChildren<ParticleAnimator>();
    foreach (ParticleEmitter particleEmitter in componentsInChildren1)
    {
      particleEmitter.minSize *= scaleFactor;
      particleEmitter.maxSize *= scaleFactor;
      particleEmitter.worldVelocity *= scaleFactor;
      particleEmitter.localVelocity *= scaleFactor;
      particleEmitter.rndVelocity *= scaleFactor;
      particleEmitter.ClearParticles();
    }
    foreach (ParticleAnimator particleAnimator in componentsInChildren2)
    {
      particleAnimator.force *= scaleFactor;
      particleAnimator.rndForce *= scaleFactor;
    }
  }

  private void ScaleTrailRenderers(float scaleFactor)
  {
    foreach (TrailRenderer componentsInChild in this.GetComponentsInChildren<TrailRenderer>())
    {
      componentsInChild.startWidth *= scaleFactor;
      componentsInChild.endWidth *= scaleFactor;
    }
  }
}
