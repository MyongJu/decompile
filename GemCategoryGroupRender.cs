﻿// Decompiled with JetBrains decompiler
// Type: GemCategoryGroupRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class GemCategoryGroupRender : MonoBehaviour
{
  private List<GemHandbookComponent> componentList = new List<GemHandbookComponent>();
  public UILabel groupName;
  public UIGrid grid;
  public GameObject iconTemplete;
  public GemDetailedInfoRender detailedInfoRender;
  private List<ConfigEquipmentGemInfo> dataList;
  private GemHandbookComponent current;

  public event System.Action<GemHandbookComponent> OnItemSelected;

  public void FeedData(GemCategoryGroupRender.Parameter param)
  {
    if (param != null)
    {
      this.ClearData();
      this.ClearGrid();
      this.dataList = param.dataList;
      this.groupName.text = param.groupName;
      this.UpdateUI();
    }
    else
      D.error((object) "Parameter can not be null !");
  }

  private void UpdateUI()
  {
    for (int index = 0; index < this.dataList.Count; ++index)
    {
      GameObject go = NGUITools.AddChild(this.grid.gameObject, this.iconTemplete.gameObject);
      GemHandbookComponent component = go.GetComponent<GemHandbookComponent>();
      component.OnSelectedHandler += new System.Action<GemHandbookComponent>(this.OnSelectedHandler);
      this.componentList.Add(component);
      NGUITools.SetActive(go, true);
      component.FeedData(this.dataList[index]);
    }
    this.grid.repositionNow = true;
    if (!((UnityEngine.Object) null == (UnityEngine.Object) this.current))
      return;
    this.detailedInfoRender.gameObject.SetActive(false);
  }

  public void SetFirstAsDefault()
  {
    this.current = this.componentList[0];
    this.OnSelectedHandler(this.current);
  }

  public List<GemHandbookComponent> GetCompnents()
  {
    List<GemHandbookComponent> handbookComponentList = new List<GemHandbookComponent>();
    for (int index = 0; index < this.componentList.Count; ++index)
      handbookComponentList.Add(this.componentList[index]);
    return handbookComponentList;
  }

  private void OnSelectedHandler(GemHandbookComponent selected)
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.current)
    {
      this.current = selected;
      this.current.Select = true;
    }
    else if (this.current.gemConfigInfo.ID != selected.gemConfigInfo.ID)
    {
      this.current.Select = false;
      this.current = selected;
      this.current.Select = true;
    }
    else
      this.current.Select = true;
    this.detailedInfoRender.gameObject.SetActive(true);
    this.detailedInfoRender.Show(this.current);
    if (this.OnItemSelected == null)
      return;
    this.OnItemSelected(this.current);
  }

  public void ClearData()
  {
    if (this.dataList != null)
    {
      this.dataList.Clear();
      this.dataList = (List<ConfigEquipmentGemInfo>) null;
    }
    if (this.componentList.Count <= 0)
      return;
    for (int index = 0; index < this.componentList.Count; ++index)
    {
      this.componentList[index].gameObject.SetActive(false);
      this.componentList[index].gameObject.transform.parent = (Transform) null;
      this.componentList[index].OnSelectedHandler -= new System.Action<GemHandbookComponent>(this.OnSelectedHandler);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.componentList[index].gameObject);
    }
    this.componentList.Clear();
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.grid);
  }

  public class Parameter
  {
    public string groupName;
    public List<ConfigEquipmentGemInfo> dataList;
  }
}
