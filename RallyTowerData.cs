﻿// Decompiled with JetBrains decompiler
// Type: RallyTowerData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;

public class RallyTowerData : RallyBuildingData
{
  private WonderTowerData _tower;

  public RallyTowerData(int x, int y)
    : base(x, y)
  {
  }

  public RallyTowerData(long id)
    : base(id)
  {
  }

  private WonderTowerData Tower
  {
    get
    {
      if (this._tower == null)
        this._tower = DBManager.inst.DB_WonderTower.Get(DBManager.inst.DB_WonderTower.GetWonderTowerTableKey(this.id, (long) this.Location.K));
      return this._tower;
    }
  }

  public override string BuildName
  {
    get
    {
      return ScriptLocalization.Get("throne_minwonder_name", true);
    }
  }

  public override long MaxCount
  {
    get
    {
      return this.Tower.MaxTroopsCount;
    }
  }

  public override List<MarchData> MarchList
  {
    get
    {
      return DBManager.inst.DB_WonderTower.GetMarchsByWonderTowerKey(DBManager.inst.DB_WonderTower.GetWonderTowerTableKey(this.Tower.TOWER_ID, (long) this.Tower.WORLD_ID));
    }
  }

  public override void Ready(System.Action callBack)
  {
    RequestManager.inst.SendRequest("wonder:getWonderCapacity", new Hashtable()
    {
      {
        (object) "world_id",
        (object) this.Tower.WORLD_ID
      },
      {
        (object) "tower_id",
        (object) this.Tower.TOWER_ID
      }
    }, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (arg1)
      {
        Hashtable hashtable = arg2 as Hashtable;
        string empty = string.Empty;
        if (hashtable != null && hashtable.ContainsKey((object) "capacity"))
          empty = hashtable[(object) "capacity"].ToString();
        if (this.Tower != null && !string.IsNullOrEmpty(empty))
        {
          int result;
          if (int.TryParse(empty, out result))
            this.Tower.MaxTroopsCount = (long) result;
          if (this.Tower.MaxTroopsCount < 0L)
            this.Tower.MaxTroopsCount = 0L;
        }
      }
      if (callBack == null)
        return;
      callBack();
    }), true);
  }

  public override MarchAllocDlg.MarchType ReinforceMarchType
  {
    get
    {
      return MarchAllocDlg.MarchType.wonderReinforce;
    }
  }

  public override RallyBuildingData.DataType Type
  {
    get
    {
      return RallyBuildingData.DataType.Tower;
    }
  }
}
