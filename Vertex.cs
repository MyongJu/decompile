﻿// Decompiled with JetBrains decompiler
// Type: Vertex
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public struct Vertex
{
  public Vector3 position;
  public Color color;
  public Vector2 uv;

  public Vertex(Vector3 position, Color color, Vector2 uv)
  {
    this.position = position;
    this.color = color;
    this.uv = uv;
  }
}
