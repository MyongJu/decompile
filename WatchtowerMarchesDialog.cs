﻿// Decompiled with JetBrains decompiler
// Type: WatchtowerMarchesDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;
using UnityEngine;

public class WatchtowerMarchesDialog : UI.Dialog
{
  public UIButton m_BackButton;
  public GameObject m_MarchListPanel;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.SetDialogState(WatchtowerMarchesDialog.DialogState.MarchList);
    WatchtowerMarchListPanel component = this.m_MarchListPanel.GetComponent<WatchtowerMarchListPanel>();
    component.onMarchDetail = new System.Action<Hashtable>(this.OnMarchDetail);
    component.UpdateData();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.SetDialogState(WatchtowerMarchesDialog.DialogState.MarchList);
    WatchtowerMarchListPanel component = this.m_MarchListPanel.GetComponent<WatchtowerMarchListPanel>();
    component.onMarchDetail = new System.Action<Hashtable>(this.OnMarchDetail);
    component.Dispose();
  }

  public void OnBackButtonClick()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void SetDialogState(WatchtowerMarchesDialog.DialogState state)
  {
  }

  private void OnMarchDetail(Hashtable detail)
  {
  }

  public enum DialogState
  {
    MarchList,
    MarchDetail,
  }
}
