﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightEarlyGuideDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;

public class DragonKnightEarlyGuideDlg : UI.Dialog
{
  private const string TITLE = "tutorial_pointer_dragon_spirit_name";
  private const string TIPS = "tutorial_pointer_dk_description";
  public UILabel title;
  public UILabel tips;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.title.text = ScriptLocalization.Get("tutorial_pointer_dragon_spirit_name", true);
    this.tips.text = ScriptLocalization.Get("tutorial_pointer_dk_description", true);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
  }
}
