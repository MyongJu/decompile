﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarMatchState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class AllianceWarMatchState : AllianceWarBaseState
{
  private const string STATE_DIS = "alliance_warfare_matching_status_description";
  public UILabel stateDisLabel;
  public UILabel allianceWarStartTimeLabel;

  public override void Show(bool requestData)
  {
    base.Show(false);
    this.AddEventHandler();
  }

  public override void Hide()
  {
    base.Hide();
    this.RemoveEventHandler();
  }

  public override void UpdateUI()
  {
    this.instruction.text = Utils.XLAT("alliance_warfare_event_description");
    this.state.text = Utils.XLAT("alliance_warfare_matching_status");
    this.stateDisLabel.text = Utils.XLAT("alliance_warfare_matching_status_description");
    this.allianceWarStartTimeLabel.text = ScriptLocalization.GetWithPara("alliance_warfare_this_round_opens_description", new Dictionary<string, string>()
    {
      {
        "0",
        Utils.FormatTimeYYYYMMDDHHMMSS((long) ActivityManager.Intance.allianceWar.FightStartTime, "-")
      }
    }, true);
  }

  private void OnGroupingFinished(object result)
  {
    this.RequestData(true);
  }

  private new void AddEventHandler()
  {
    MessageHub.inst.GetPortByAction("ac_grouping_finished").AddEvent(new System.Action<object>(this.OnGroupingFinished));
  }

  private new void RemoveEventHandler()
  {
    MessageHub.inst.GetPortByAction("ac_grouping_finished").RemoveEvent(new System.Action<object>(this.OnGroupingFinished));
  }
}
