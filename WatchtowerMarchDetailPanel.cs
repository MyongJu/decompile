﻿// Decompiled with JetBrains decompiler
// Type: WatchtowerMarchDetailPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class WatchtowerMarchDetailPanel : MonoBehaviour
{
  public GameObject m_HeroPreviewPrefab;
  public WatchtowerPlayerInfo m_PlayerInfo;
  public WatchtowerArmyInfo m_ArmyInfo;
  public WatchtowerBoostInfo m_BoostInfo;
  private GameObject m_HeroPreviewInstance;
  private WatchtowerHeroPreview m_HeroPreview;

  private void Start()
  {
    this.m_HeroPreviewInstance = Object.Instantiate<GameObject>(this.m_HeroPreviewPrefab);
    this.m_HeroPreview = this.m_HeroPreviewInstance.GetComponent<WatchtowerHeroPreview>();
    this.m_HeroPreview.Hide();
    this.m_HeroPreviewInstance.transform.parent = this.gameObject.transform.parent;
    this.m_HeroPreviewInstance.transform.localPosition = Vector3.zero;
    this.m_HeroPreviewInstance.transform.localRotation = Quaternion.identity;
    this.m_HeroPreviewInstance.transform.localScale = Vector3.one;
  }

  public void OnHeroClick()
  {
    this.m_HeroPreview.Show(HeroGender.Male);
  }

  public void UpdateMarcheDetailData(Hashtable data)
  {
    this.m_PlayerInfo.SetMarchDetailData(data);
    this.m_ArmyInfo.SetMarchDetailData(data);
    this.m_BoostInfo.SetMarchDetailData(data);
  }
}
