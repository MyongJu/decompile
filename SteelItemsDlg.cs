﻿// Decompiled with JetBrains decompiler
// Type: SteelItemsDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class SteelItemsDlg : UI.Dialog
{
  private Dictionary<int, ShortCutItemRenderer> m_ItemList = new Dictionary<int, ShortCutItemRenderer>();
  private GameObjectPool m_Pool = new GameObjectPool();
  public ShortCutItemRenderer itemPrefab;
  public UILabel title;
  public UIGrid grid;
  public UIScrollView scrollView;
  private GameObject m_ItemRoot;

  private void Awake()
  {
    this.m_ItemRoot = this.gameObject;
    this.m_Pool.Initialize(this.itemPrefab.gameObject, this.m_ItemRoot);
  }

  public void OnBackBtPress()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnCloseBackBtPress()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemDataChanged);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnItemDataChanged);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI(true);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Clear();
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataChanged);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnItemDataChanged);
  }

  private void UpdateUI(bool needResetposition = true)
  {
    this.Clear();
    this.scrollView.gameObject.SetActive(false);
    List<ItemStaticInfo> steelItems = ItemBag.Instance.GetSteelItems();
    steelItems.Sort(new Comparison<ItemStaticInfo>(this.CompareItem));
    for (int index = 0; index < steelItems.Count; ++index)
    {
      ItemStaticInfo itemInfo = steelItems[index];
      List<ShopStaticInfo> byItemInternalId = ItemBag.Instance.GetCurrentSaleShopItemsByItem_InternalId(itemInfo.internalId);
      if (byItemInternalId.Count > 0)
        this.CreateItemByShopInfoList(byItemInternalId);
      else if (ItemBag.Instance.GetItemCount(itemInfo.internalId) > 0)
        this.CreateItemByItemInfo(itemInfo);
    }
    this.title.text = ScriptLocalization.Get("id_uppercase_steel", true);
    this.ResetTable(needResetposition);
  }

  private int CompareItem(ItemStaticInfo a, ItemStaticInfo b)
  {
    float num1 = a.Value;
    float num2 = b.Value;
    int itemCount1 = ItemBag.Instance.GetItemCount(a.internalId);
    int itemCount2 = ItemBag.Instance.GetItemCount(b.internalId);
    if (itemCount1 > 0 && itemCount2 == 0)
      return -1;
    if (itemCount1 == 0 && itemCount2 > 0)
      return 1;
    return num1.CompareTo(num2);
  }

  private void ResetTable(bool needResetposition)
  {
    this.scrollView.gameObject.SetActive(true);
    this.grid.Reposition();
    if (!needResetposition)
      return;
    this.scrollView.ResetPosition();
  }

  private void OnRepositionHandler()
  {
    this.grid.onReposition -= new UIGrid.OnReposition(this.OnRepositionHandler);
    this.scrollView.ResetPosition();
  }

  private void CreateItemByShopInfoList(List<ShopStaticInfo> shopList)
  {
    using (List<ShopStaticInfo>.Enumerator enumerator = shopList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShopStaticInfo current = enumerator.Current;
        if (!this.m_ItemList.ContainsKey(current.Item_InternalId))
        {
          this.itemPrefab.gameObject.SetActive(true);
          ShortCutItemRenderer shortCutItemRenderer = this.m_Pool.AddChild(this.grid.gameObject).AddMissingComponent<ShortCutItemRenderer>();
          shortCutItemRenderer.SetShopItemData(current.internalId);
          this.itemPrefab.gameObject.SetActive(false);
          this.m_ItemList.Add(current.Item_InternalId, shortCutItemRenderer);
        }
      }
    }
  }

  private void CreateItemByItemInfo(ItemStaticInfo itemInfo)
  {
    if (this.m_ItemList.ContainsKey(itemInfo.internalId))
      return;
    this.itemPrefab.gameObject.SetActive(true);
    ShortCutItemRenderer shortCutItemRenderer = this.m_Pool.AddChild(this.grid.gameObject).AddMissingComponent<ShortCutItemRenderer>();
    shortCutItemRenderer.SetItemData(itemInfo.internalId);
    this.itemPrefab.gameObject.SetActive(false);
    this.m_ItemList.Add(itemInfo.internalId, shortCutItemRenderer);
  }

  private void OnItemDataChanged(int item_id)
  {
    this.UpdateUI(false);
  }

  private void Clear()
  {
    using (Dictionary<int, ShortCutItemRenderer>.ValueCollection.Enumerator enumerator = this.m_ItemList.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShortCutItemRenderer current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.Dispose();
        this.m_Pool.Release(current.gameObject);
      }
    }
    this.m_ItemList.Clear();
  }
}
