﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerBuffStorePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTowerBuffStorePopup : Popup
{
  private List<MagicTowerShopInfo> benefits = new List<MagicTowerShopInfo>();
  private List<MagicTowerShopInfo> marchCapacityBenefits = new List<MagicTowerShopInfo>();
  private List<MagicTowerShopInfo> nonMarchCapacityBenefits = new List<MagicTowerShopInfo>();
  private List<MerlinTowerBenefitItemRender> renderList = new List<MerlinTowerBenefitItemRender>();
  private const string BUY_BENEFITS = "tower_buy_benefits_title";
  private const string BUY_BENEFITS_DES = "tower_buy_benefits_description";
  private const string BUY_BENEFITS_AFTER_DES = "tower_buy_benefits_after_description";
  private const string OWN_COLON = "id_own_colon";
  private const string NOT_ENOUGH_MANA = "toast_tower_not_enough_mana";
  private const string GUARDIAN_BENEFIT = "tower_floor_guardian_benefit_name";
  private const string ONLY_ONE_BUY = "toast_tower_one_benefit_per_floor";
  private const string TROOP_FULL_NOW = "toast_tower_troops_uninjured_no_buy";
  public UILabel title;
  public UILabel layerTitle;
  public UILabel buyBenefitDes;
  public UILabel buyBenefitAfterDes;
  public UILabel owened;
  public UILabel magicValue;
  public GameObject benefitSlot;
  public UIGrid grid;
  private int layer;
  private int initLayer;
  private long magicCount;
  private bool hasBoughtInCurrentLayer;
  private MerlinTowerBenefitItemRender currentBoughtItem;
  private List<MagicTowerShopInfo> tempBenefits;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    MerlinTowerBuffStorePopup.Parameter parameter = orgParam as MerlinTowerBuffStorePopup.Parameter;
    if (parameter == null)
      return;
    this.initLayer = parameter.layer;
    this.layer = this.initLayer;
    this.tempBenefits = ConfigManager.inst.DB_MagicTowerShop.GetWithLayer(this.initLayer);
    if (this.IsTopLayer())
    {
      Logger.Log("max level reach");
      this.layer = this.initLayer - 1;
      this.tempBenefits = ConfigManager.inst.DB_MagicTowerShop.GetWithLayer(this.layer);
    }
    if (this.ShouldShowPreviousLayerBuff())
    {
      Logger.Log("not attack the monster or attack fail, show last level buffs");
      this.layer = this.initLayer - 1;
      this.tempBenefits = ConfigManager.inst.DB_MagicTowerShop.GetWithLayer(this.layer);
    }
    this.SortBenefits();
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.UpdateStaticLables();
    this.UpdateInitMagicValue();
    this.UpdateBenefits();
    this.DisableAllBuffItems();
  }

  private bool IsTopLayer()
  {
    return MerlinTowerPayload.Instance.UserData.CurrentTowerMainInfo != null && MerlinTowerPayload.Instance.UserData.CurrentTowerMainInfo.TowerMonsterId == 0;
  }

  private bool ShouldShowPreviousLayerBuff()
  {
    return MerlinTowerPayload.Instance.UserData.HasMonster && this.initLayer > 1;
  }

  private void SortBenefits()
  {
    for (int index = 0; index < this.tempBenefits.Count; ++index)
    {
      if (this.tempBenefits[index].type == 2)
        this.marchCapacityBenefits.Add(this.tempBenefits[index]);
      else
        this.nonMarchCapacityBenefits.Add(this.tempBenefits[index]);
    }
    this.benefits.AddRange((IEnumerable<MagicTowerShopInfo>) this.marchCapacityBenefits);
    this.benefits.AddRange((IEnumerable<MagicTowerShopInfo>) this.nonMarchCapacityBenefits);
  }

  private void UpdateStaticLables()
  {
    this.title.text = ScriptLocalization.Get("tower_buy_benefits_title", true);
    this.owened.text = ScriptLocalization.Get("id_own_colon", true);
    this.buyBenefitDes.text = ScriptLocalization.Get("tower_buy_benefits_description", true);
    this.buyBenefitAfterDes.text = ScriptLocalization.Get("tower_buy_benefits_after_description", true);
    this.layerTitle.text = ScriptLocalization.GetWithPara("tower_floor_guardian_benefit_name", new Dictionary<string, string>()
    {
      {
        "0",
        this.layer.ToString()
      }
    }, true);
  }

  private void UpdateInitMagicValue()
  {
    this.magicCount = MerlinTowerPayload.Instance.UserData != null ? MerlinTowerPayload.Instance.UserData.magic : 0L;
    this.UpdateMagicValue(this.magicCount);
  }

  private void UpdateMagicValueAfterBought()
  {
    this.magicCount = MerlinTowerPayload.Instance.BoughtBenefit.magic;
    this.UdpateLocalCache();
    this.UpdateMagicValue(this.magicCount);
  }

  private void UdpateLocalCache()
  {
    if (MerlinTowerPayload.Instance.UserData.shopItems == null)
      MerlinTowerPayload.Instance.UserData.shopItems = new List<int>();
    MerlinTowerPayload.Instance.UserData.shopItems.Clear();
    MerlinTowerPayload.Instance.UserData.shopItems.AddRange((IEnumerable<int>) MerlinTowerPayload.Instance.BoughtBenefit.shopItems);
    MerlinTowerPayload.Instance.UserData.magic = MerlinTowerPayload.Instance.BoughtBenefit.magic;
    MerlinTowerPayload.Instance.UserData.capacity = MerlinTowerPayload.Instance.BoughtBenefit.capacity;
  }

  private void UpdateMagicValue(long value)
  {
    this.magicValue.text = value.ToString();
  }

  private void UpdateBenefits()
  {
    for (int index1 = 0; index1 < this.benefits.Count; ++index1)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.benefitSlot);
      gameObject.transform.parent = this.grid.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localScale = Vector3.one;
      gameObject.SetActive(true);
      MerlinTowerBenefitItemRender component = gameObject.GetComponent<MerlinTowerBenefitItemRender>();
      bool flag = false;
      int num1 = -1;
      float num2 = 0.0f;
      int num3 = 0;
      List<int> shopItems = MerlinTowerPayload.Instance.UserData.shopItems;
      if (shopItems != null)
      {
        for (int index2 = 0; index2 < shopItems.Count; ++index2)
        {
          if (this.benefits[index1].internalId == shopItems[index2])
          {
            num1 = ConfigManager.inst.DB_MagicTowerMain.GetData(this.benefits[index1].MagicTowerMainId).Level;
            if (this.layer == num1)
            {
              this.hasBoughtInCurrentLayer = true;
              break;
            }
            break;
          }
        }
        if (this.benefits[index1].type != 2)
        {
          List<Benefits.BenefitValuePair> benefitsPairs1 = this.benefits[index1].Benefits.GetBenefitsPairs();
          for (int index2 = 0; index2 < shopItems.Count; ++index2)
          {
            MagicTowerShopInfo data1 = ConfigManager.inst.DB_MagicTowerShop.GetData(shopItems[index2]);
            MagicTowerMainInfo data2 = ConfigManager.inst.DB_MagicTowerMain.GetData(data1.MagicTowerMainId);
            if (data1.type != 2)
            {
              List<Benefits.BenefitValuePair> benefitsPairs2 = data1.Benefits.GetBenefitsPairs();
              if (benefitsPairs1[0].internalID == benefitsPairs2[0].internalID)
              {
                flag = true;
                num1 = data2.Level;
                num2 = benefitsPairs2[0].value;
                num3 = data1.price;
                break;
              }
            }
          }
        }
        else
        {
          for (int index2 = 0; index2 < shopItems.Count; ++index2)
          {
            MagicTowerShopInfo data1 = ConfigManager.inst.DB_MagicTowerShop.GetData(shopItems[index2]);
            MagicTowerMainInfo data2 = ConfigManager.inst.DB_MagicTowerMain.GetData(data1.MagicTowerMainId);
            if (data1.type == 2)
            {
              flag = true;
              num1 = data2.Level;
            }
          }
        }
      }
      MerlinTowerBenefitData data = new MerlinTowerBenefitData();
      if (this.benefits[index1].type != 2)
      {
        List<Benefits.BenefitValuePair> benefitsPairs = this.benefits[index1].Benefits.GetBenefitsPairs();
        PropertyDefinition propertyDefinition = benefitsPairs[0].propertyDefinition;
        data.benefitName = propertyDefinition.Name;
        data.benefitIcon = propertyDefinition.ImagePath;
        data.format = propertyDefinition.Format;
        if (flag)
        {
          data.value = num2;
          data.price = num3;
          data.boughtLayer = num1;
        }
        else
        {
          component.OnItemClickedHandler += new System.Action<MerlinTowerBenefitItemRender>(this.OnItemClickedHandler);
          data.value = benefitsPairs[0].value;
          data.price = this.benefits[index1].price;
          data.boughtLayer = -1;
        }
      }
      else
      {
        data.benefitName = this.benefits[index1].description;
        data.benefitIcon = "Texture/Benefits/" + this.benefits[index1].icon;
        data.format = PropertyDefinition.FormatType.Percent;
        data.value = this.benefits[index1].param1;
        data.price = this.benefits[index1].price;
        if (flag && this.hasBoughtInCurrentLayer)
        {
          data.boughtLayer = num1;
        }
        else
        {
          data.boughtLayer = -1;
          component.OnItemClickedHandler += new System.Action<MerlinTowerBenefitItemRender>(this.OnItemClickedHandler);
        }
      }
      data.type = this.benefits[index1].type;
      data.currentLayer = this.layer;
      data.index = index1;
      data.isBought = flag;
      data.boughtInCurrentLayer = this.hasBoughtInCurrentLayer;
      component.FeedData(data);
      this.renderList.Add(component);
    }
  }

  private void OnItemClickedHandler(MerlinTowerBenefitItemRender render)
  {
    if (this.magicCount < (long) render.Data.price)
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_tower_not_enough_mana", true), (System.Action) null, 2f, false);
    else if (this.hasBoughtInCurrentLayer)
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_tower_one_benefit_per_floor", true), (System.Action) null, 2f, false);
    }
    else
    {
      switch ((MerlinTowerBenefitItemRender.BenefitType) render.Data.type)
      {
        case MerlinTowerBenefitItemRender.BenefitType.DirectlyUse:
          this.BuyNoneMarchCapacityBenefit(render);
          break;
        case MerlinTowerBenefitItemRender.BenefitType.MarchCapacity:
          this.BuyMarchCapacityBenefit(render);
          break;
      }
    }
  }

  private void BuyMarchCapacityBenefit(MerlinTowerBenefitItemRender render)
  {
    UIManager.inst.OpenPopup("MerlinTower/MerlinTowerBuybuffWith2ButtonsPopup", (Popup.PopupParameter) new MerlinTowerBuyBuffConfirmPopup.Parameter()
    {
      benefitName = ScriptLocalization.GetWithPara(render.Data.benefitName, new Dictionary<string, string>()
      {
        {
          "0",
          Utils.GetBenefitValueString(render.Data.format, (double) render.Data.value)
        }
      }, true),
      benefitIcon = render.Data.benefitIcon,
      price = render.Data.price,
      value = render.Data.value,
      format = render.Data.format,
      type = 2,
      onBuyClicked = (System.Action) (() =>
      {
        if (MerlinTowerPayload.Instance.IsTroopCapacityFull)
        {
          UIManager.inst.toast.Show(ScriptLocalization.Get("toast_tower_troops_uninjured_no_buy", true), (System.Action) null, 2f, false);
        }
        else
        {
          int index = render.Data.index;
          this.currentBoughtItem = render;
          MerlinTowerPayload.Instance.SendBuyBenefitRequest(new System.Action<bool, object>(this.OnBuyCompleteCallback), this.benefits[index].internalId, true);
        }
      })
    });
  }

  private void BuyNoneMarchCapacityBenefit(MerlinTowerBenefitItemRender render)
  {
    UIManager.inst.OpenPopup("MerlinTower/MerlinTowerBuybuffWith2ButtonsPopup", (Popup.PopupParameter) new MerlinTowerBuyBuffConfirmPopup.Parameter()
    {
      benefitName = (render.Data.benefitName + Utils.GetBenefitValueString(render.Data.format, (double) render.Data.value)),
      benefitIcon = render.Data.benefitIcon,
      price = render.Data.price,
      type = 1,
      onBuyClicked = (System.Action) (() =>
      {
        int index = render.Data.index;
        this.currentBoughtItem = render;
        MerlinTowerPayload.Instance.SendBuyBenefitRequest(new System.Action<bool, object>(this.OnBuyCompleteCallback), this.benefits[index].internalId, true);
      })
    });
  }

  private void OnBuyCompleteCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.hasBoughtInCurrentLayer = true;
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.currentBoughtItem)
    {
      this.currentBoughtItem.Bought = true;
      this.currentBoughtItem.GetComponent<BoxCollider>().enabled = false;
    }
    this.UpdateMagicValueAfterBought();
    this.DisableAllBuffItems();
  }

  private void DisableAllBuffItems()
  {
    if (!this.hasBoughtInCurrentLayer)
      return;
    for (int index = 0; index < this.renderList.Count; ++index)
    {
      if (!this.renderList[index].IsBought && (UnityEngine.Object) this.currentBoughtItem != (UnityEngine.Object) this.renderList[index])
        this.renderList[index].Disable = true;
      this.renderList[index].GetComponent<BoxCollider>().enabled = false;
    }
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void ClearData()
  {
    for (int index = 0; index < this.renderList.Count; ++index)
    {
      this.renderList[index].transform.parent = (Transform) null;
      this.renderList[index].gameObject.SetActive(false);
      this.renderList[index].OnItemClickedHandler -= new System.Action<MerlinTowerBenefitItemRender>(this.OnItemClickedHandler);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.renderList[index].gameObject);
    }
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.ClearData();
  }

  public class Parameter : Popup.PopupParameter
  {
    public int layer;
  }
}
