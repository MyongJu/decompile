﻿// Decompiled with JetBrains decompiler
// Type: MailHistrotyDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UI;
using UnityEngine;

public class MailHistrotyDlg : UI.Dialog
{
  private List<PlayerMessageEntry> sortedgroupMails = new List<PlayerMessageEntry>(10);
  private List<GameObject> contents = new List<GameObject>(10);
  private const float TIME_STAMP = 300f;
  public UITable contentTable;
  public UIScrollView scrollView;
  public UILabel titleLabel;
  public ExtendUIInput inputLabel;
  public UILabel contentLabel;
  public UIButton sendBtn;
  public UIButton nextBtn;
  public UIButton preBtn;
  public UIButton starBtn;
  public UISprite starSprite;
  public UISprite modWarning;
  public MailEntryView myEntry;
  public MailEntryView otherEntry;
  public TimeEntryView timeEntry;
  public BlockEntryView blockEntry;
  public Transform keyboardRelated;
  public Transform keyboardBaseLine;
  private AbstractMailEntry mail;
  private MailController controller;
  private MailCategory curCategory;
  private MailHistrotyDlg.Parameter parameter;
  private long focusMailID;
  private Vector3 originalKeyboardRelatedPosition;

  private void UpdateUI()
  {
    this.InitMailHistoryEntry();
    this.DrawContent();
  }

  private void InitMailHistoryEntry()
  {
    PlayerMessage mail = this.mail as PlayerMessage;
    this.sortedgroupMails.Clear();
    this.sortedgroupMails.AddRange((IEnumerable<PlayerMessageEntry>) mail.GetPlayerMessageEntries());
  }

  private void FocusOnMailID()
  {
    GameObject gameObject = (GameObject) null;
    int num = 0;
    if (!((UnityEngine.Object) gameObject != (UnityEngine.Object) null))
      return;
    this.scrollView.SetDragAmount(0.0f, ((float) num + 0.7f) / (float) this.sortedgroupMails.Count, false);
  }

  private void DrawContent()
  {
    for (int index = 0; index < this.contents.Count; ++index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.contents[index]);
    this.contents.Clear();
    long num = -1;
    for (int index = 0; index < this.sortedgroupMails.Count; ++index)
    {
      PlayerMessageEntry sortedgroupMail = this.sortedgroupMails[index];
      if ((double) (sortedgroupMail.time - num) > 300.0)
      {
        GameObject gameObject = NGUITools.AddChild(this.contentTable.gameObject, this.timeEntry.gameObject);
        gameObject.GetComponent<TimeEntryView>().UpdateUI(Utils.FormatTimeForMail(sortedgroupMail.time));
        this.contents.Add(gameObject);
        gameObject.SetActive(true);
      }
      bool isModerator = PlayerData.inst.moderatorInfo.IsModerator;
      if (sortedgroupMail.fromUID == PlayerData.inst.uid)
      {
        if (sortedgroupMail.blocked)
        {
          GameObject gameObject = NGUITools.AddChild(this.contentTable.gameObject, this.blockEntry.gameObject);
          gameObject.GetComponent<BlockEntryView>().UpdateUI(Utils.XLAT(sortedgroupMail.blockedKey));
          this.contents.Add(gameObject);
          gameObject.SetActive(true);
        }
        else
        {
          GameObject gameObject = NGUITools.AddChild(this.contentTable.gameObject, this.myEntry.gameObject);
          this.contents.Add(gameObject);
          MailEntryView component = gameObject.GetComponent<MailEntryView>();
          if (this.curCategory == MailCategory.Mod && isModerator)
            component.UseModPortrait = true;
          component.UpdateUI(sortedgroupMail);
          component.onUIChanged += new System.Action(this.OnUIChanged);
          gameObject.SetActive(true);
        }
      }
      else
      {
        GameObject gameObject = NGUITools.AddChild(this.contentTable.gameObject, this.otherEntry.gameObject);
        this.contents.Add(gameObject);
        MailEntryView component = gameObject.GetComponent<MailEntryView>();
        if (this.curCategory == MailCategory.Mod && !isModerator)
          component.UseModPortrait = true;
        component.UpdateUI(sortedgroupMail);
        component.onUIChanged += new System.Action(this.OnUIChanged);
        gameObject.SetActive(true);
      }
      num = sortedgroupMail.time;
    }
    this.titleLabel.text = this.mail.sender;
    this.starSprite.gameObject.SetActive(this.mail.isFavorite);
    this.nextBtn.isEnabled = this.controller.CurrentList.GetNextMail(this.mail) != null;
    this.preBtn.isEnabled = this.controller.CurrentList.GetPreMail(this.mail) != null;
    this.sendBtn.isEnabled = !string.IsNullOrEmpty(this.contentLabel.text);
    this.contentTable.repositionNow = true;
    this.contentTable.onReposition += new UITable.OnReposition(this.OnTableReposition);
  }

  private void OnUIChanged()
  {
    this.contentTable.Reposition();
  }

  private void AddEvtsListener()
  {
    this.sendBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnSendBtnClicked)));
    this.preBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnPreBtnClicked)));
    this.nextBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnNextBtnClicked)));
    this.starBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnStarBtnClicked)));
    this.inputLabel.OnKeayboardAreaChange += new ExtendUIInput.KeyboardAreaChange(this.HandleOnKeayboardAreaChange);
    this.inputLabel.onChange.Add(new EventDelegate(new EventDelegate.Callback(this.OnInputChange)));
    this.inputLabel.onValidate = new UIInput.OnValidate(this.InputLabelValidate);
    this.mail.mailUpdated += new System.Action(this.OnMailUpdated);
  }

  private char InputLabelValidate(string text, int charIndex, char addedChar)
  {
    if (this.isValidateCharacter(addedChar))
      return addedChar;
    return char.MinValue;
  }

  private bool isValidateCharacter(char codePoint)
  {
    return char.GetUnicodeCategory(codePoint) != UnicodeCategory.Surrogate;
  }

  private void OnInputChange()
  {
    this.sendBtn.isEnabled = !string.IsNullOrEmpty(this.contentLabel.text);
  }

  private void HandleOnKeayboardAreaChange(Rect area)
  {
    if ((double) area.height > 0.0)
    {
      Vector3 screenPoint1 = UIManager.inst.ui2DCamera.WorldToScreenPoint(this.keyboardRelated.position);
      Vector3 screenPoint2 = UIManager.inst.ui2DCamera.WorldToScreenPoint(this.keyboardBaseLine.position);
      this.keyboardRelated.position = UIManager.inst.ui2DCamera.ScreenToWorldPoint(screenPoint1 + Vector3.up * (area.height - screenPoint2.y));
    }
    else
    {
      this.keyboardRelated.localPosition = this.originalKeyboardRelatedPosition;
      Utils.ExecuteInSecs(0.01f, (System.Action) (() =>
      {
        this.scrollView.MoveRelative(Vector3.up * 100000f);
        this.scrollView.RestrictWithinBounds(true);
      }));
    }
  }

  private void RemoveEvtListener()
  {
    this.preBtn.onClick.Clear();
    this.nextBtn.onClick.Clear();
    this.sendBtn.onClick.Clear();
    this.mail.mailUpdated -= new System.Action(this.OnMailUpdated);
    this.inputLabel.OnKeayboardAreaChange -= new ExtendUIInput.KeyboardAreaChange(this.HandleOnKeayboardAreaChange);
    this.starBtn.onClick.Clear();
    this.inputLabel.onChange.Clear();
    this.sortedgroupMails.Clear();
  }

  private void OnTableReposition()
  {
    this.contentTable.onReposition -= new UITable.OnReposition(this.OnTableReposition);
    this.scrollView.MoveRelative(Vector3.up * 100000f);
    this.scrollView.RestrictWithinBounds(true);
  }

  private void OnMailUpdated()
  {
    this.UpdateUI();
  }

  private void OnPreBtnClicked()
  {
    AbstractMailEntry preMail = this.controller.CurrentList.GetPreMail(this.mail);
    if (preMail == null)
      return;
    this.OnMailGet(preMail);
  }

  private void OnNextBtnClicked()
  {
    AbstractMailEntry nextMail = this.controller.CurrentList.GetNextMail(this.mail);
    if (nextMail == null)
      return;
    this.OnMailGet(nextMail);
  }

  private void OnMailGet(AbstractMailEntry entry)
  {
    this.mail = entry;
    this.UpdateUI();
  }

  private void OnStarBtnClicked()
  {
    this.controller.TagFavToMail(this.mail.mailID, !this.mail.isFavorite, (int) this.mail.category);
    this.UpdateUI();
  }

  private void OnSendBtnClicked()
  {
    int levelLimit = ChatAndMailConstraint.Instance.LevelLimit;
    if (ChatAndMailConstraint.Instance.SwitchState && !ChatAndMailConstraint.Instance.HasBindAccount() && CityManager.inst.mStronghold.mLevel < levelLimit)
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_mail_use_forbidden", new Dictionary<string, string>()
      {
        {
          "0",
          levelLimit.ToString()
        }
      }, true), (System.Action) null, 4f, false);
    else if (PlayerData.inst.userData.IsShutUp)
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("chat_silenced_title"),
        Content = ScriptLocalization.GetWithMultyContents("chat_silenced_description", true, Utils.FormatTime(PlayerData.inst.userData.ShutUpLeftTime, false, false, true)),
        Okay = Utils.XLAT("id_uppercase_okay"),
        OkayCallback = (System.Action) null
      });
    else if (this.curCategory == MailCategory.Mod)
    {
      ArrayList modIds = new ArrayList();
      modIds.Add((object) this.mail.fromUID);
      this.contentLabel.text = Utils.GetLimitStr(this.contentLabel.text, 400);
      this.controller.SendModMails(modIds, this.contentLabel.text, new System.Action(this.OnMailSend));
    }
    else
    {
      ArrayList recipents = new ArrayList();
      recipents.Add((object) this.mail.fromUID);
      this.contentLabel.text = Utils.GetLimitStr(this.contentLabel.text, 400);
      this.controller.SendMail(recipents, this.contentLabel.text, new System.Action(this.OnMailSend));
    }
  }

  private void OnMailSend()
  {
    if (!(bool) ((UnityEngine.Object) this.gameObject))
      return;
    this.mail = this.controller.mailsById[this.mail.mailID];
    this.inputLabel.value = string.Empty;
    this.contentLabel.text = string.Empty;
    this.UpdateUI();
  }

  private void OnBackBtnClicked()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void OnCloseBtnClicked()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnEmojiInputClick()
  {
    EmojiManager.Instance.ShowEmojiInput((System.Action<string>) (obj => this.inputLabel.value += obj));
  }

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
    this.originalKeyboardRelatedPosition = this.keyboardRelated.localPosition;
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.parameter = orgParam as MailHistrotyDlg.Parameter;
    this.mail = (AbstractMailEntry) this.parameter.mail;
    this.curCategory = this.parameter.mailCategory;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.focusMailID = this.parameter.focusMailID;
    this.controller = PlayerData.inst.mail;
    this.inputLabel.value = string.Empty;
    this.contentLabel.text = string.Empty;
    this.UpdateUI();
    this.AddEvtsListener();
    this.modWarning.gameObject.SetActive(false);
    if (this.curCategory != MailCategory.Mod || PlayerData.inst.moderatorInfo.IsModerator)
      return;
    this.modWarning.gameObject.SetActive(true);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEvtListener();
    if (!GameEngine.IsReady())
      return;
    this.controller.MarkMailAsRead((int) this.mail.category, new List<AbstractMailEntry>()
    {
      this.mail
    }, 1 != 0, 1 != 0);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public MailCategory mailCategory = MailCategory.System;
    public PlayerMessage mail;
    public long focusMailID;
  }
}
