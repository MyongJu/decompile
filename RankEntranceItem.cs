﻿// Decompiled with JetBrains decompiler
// Type: RankEntranceItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class RankEntranceItem : MonoBehaviour
{
  private const string ICON_FOLDER_PATH = "Texture/LeaderboardIcons/";
  public const int CATEGORY_ALLIANCE_POWER = 0;
  public const int CATEGORY_ALLIANCE_TROOPS_KILLED = 1;
  public const int CATEGORY_PLAYER_POWER = 2;
  public const int CATEGORY_DRAGON_LEVEL = 3;
  public const int CATEGORY_LORD_LEVEL = 4;
  public const int CATEGORY_PLAYER_TROOPS_KILLED = 5;
  public const int CATEGORY_STRONGHOLD = 6;
  public const int CATEGORY_MAX = 7;
  private const string USER = "user";
  private const string ALLIANCE = "alliance";
  public GameObject player;
  public UITexture playerIcon;
  public AllianceSymbol allianceIcon;
  public UILabel titleText;
  public UILabel nameText;
  public UILabel kingdomText;
  private RankBoardDlg.LeaderBoardType _LeaderBoardType;
  private int optItemID;
  private Hashtable rankFirstInformation;
  private string title;
  private static bool isInit;
  private static Dictionary<int, string> labelKeys;
  private static Dictionary<int, string> categoryMap;

  public bool isAllianceItem
  {
    get
    {
      if (this.optItemID != 0)
        return this.optItemID == 1;
      return true;
    }
  }

  public void Clear()
  {
    NGUITools.SetActive(this.gameObject, false);
  }

  public void shareDataInit()
  {
    RankEntranceItem.labelKeys = new Dictionary<int, string>();
    RankEntranceItem.labelKeys.Add(0, "leaderboards_uppercase_alliance_power");
    RankEntranceItem.labelKeys.Add(1, "leaderboards_uppercase_alliance_kills");
    RankEntranceItem.labelKeys.Add(2, "leaderboards_uppercase_individual_power");
    RankEntranceItem.labelKeys.Add(3, "leaderboards_uppercase_dragon_level");
    RankEntranceItem.labelKeys.Add(4, "leaderboards_uppercase_lord_level");
    RankEntranceItem.labelKeys.Add(5, "leaderboards_uppercase_individual_kills");
    RankEntranceItem.labelKeys.Add(6, "leaderboards_uppercase_stronghold");
    RankEntranceItem.categoryMap = new Dictionary<int, string>();
    RankEntranceItem.categoryMap.Add(0, "power");
    RankEntranceItem.categoryMap.Add(1, "troops_killed");
    RankEntranceItem.categoryMap.Add(2, "power");
    RankEntranceItem.categoryMap.Add(3, "dragon");
    RankEntranceItem.categoryMap.Add(4, "lord_level");
    RankEntranceItem.categoryMap.Add(5, "troops_killed");
    RankEntranceItem.categoryMap.Add(6, "stronghold_level");
  }

  public void InitItem(int id)
  {
    if (!RankEntranceItem.isInit)
    {
      this.shareDataInit();
      RankEntranceItem.isInit = true;
    }
    this.optItemID = id;
    this.titleText.text = ScriptLocalization.Get(RankEntranceItem.labelKeys[this.optItemID], true);
    if (this.isAllianceItem)
    {
      this.player.SetActive(false);
      this.allianceIcon.gameObject.SetActive(true);
    }
    else
    {
      this.allianceIcon.gameObject.SetActive(false);
      this.player.SetActive(true);
    }
  }

  public void SetInfo(Hashtable data, RankBoardDlg.LeaderBoardType leaderBoardType)
  {
    this._LeaderBoardType = leaderBoardType;
    Hashtable hashtable = (Hashtable) null;
    if (data != null)
      hashtable = !this.isAllianceItem ? data[(object) "user"] as Hashtable : data[(object) "alliance"] as Hashtable;
    this.rankFirstInformation = hashtable == null ? (Hashtable) null : hashtable[(object) RankEntranceItem.categoryMap[this.optItemID]] as Hashtable;
    if (this.isAllianceItem)
      this.initAllianceItem();
    else
      this.initPlayerItem();
  }

  public void OnOptionItemClicked()
  {
    this.OpenRankDetailDlg();
  }

  private void initAllianceItem()
  {
    if (this.rankFirstInformation == null)
      return;
    AllianceRankData allianceRankData = RankData.Parse(this.rankFirstInformation) as AllianceRankData;
    if (allianceRankData == null)
      return;
    this.allianceIcon.SetSymbols(allianceRankData.Symbol);
    this.nameText.text = allianceRankData.Name;
    if (!(bool) ((Object) this.kingdomText))
      return;
    this.kingdomText.text = "K" + allianceRankData.KingdomId.ToString();
  }

  private void initPlayerItem()
  {
    if (this.rankFirstInformation == null)
      return;
    PlayerRankData playerRankData = RankData.Parse(this.rankFirstInformation) as PlayerRankData;
    if (playerRankData == null)
      return;
    CustomIconLoader.Instance.requestCustomIcon(this.playerIcon, playerRankData.Image, playerRankData.CustomIconUrl, false);
    this.nameText.text = playerRankData.Name;
    if (!(bool) ((Object) this.kingdomText))
      return;
    this.kingdomText.text = "K" + playerRankData.KingdomId.ToString();
  }

  private void OpenRankDetailDlg()
  {
    Hashtable hashtable = new Hashtable();
    string empty = string.Empty;
    string str;
    if (this.optItemID == 0 || this.optItemID == 1)
    {
      str = "Alliance:loadAllianceLeaderboard";
      hashtable.Add((object) "alliance_id", (object) PlayerData.inst.allianceId);
    }
    else
      str = "Player:loadUserLeaderboard";
    string category = RankEntranceItem.categoryMap[this.optItemID];
    hashtable.Add((object) "category", (object) category);
    LeaderBoardDetailDlg.LeaderBoardDetailDlgParamer detailDlgParamer = new LeaderBoardDetailDlg.LeaderBoardDetailDlgParamer();
    detailDlgParamer.method = str;
    if (this._LeaderBoardType == RankBoardDlg.LeaderBoardType.World)
      detailDlgParamer.method += "ForGlobal";
    detailDlgParamer.param = hashtable;
    detailDlgParamer.optItemID = this.optItemID;
    detailDlgParamer.title = this.titleText.text;
    UIManager.inst.OpenDlg(this._LeaderBoardType != RankBoardDlg.LeaderBoardType.Kingdom ? "LeaderBoard/WorldLeaderBoardDetailDlg" : "LeaderBoard/LeaderBoardDetailDlg", (UI.Dialog.DialogParameter) detailDlgParamer, true, true, true);
  }

  private void ResultHandler(bool success, object result)
  {
    if (!success)
      return;
    LeaderBoardDetailDlg.LeaderBoardDetailDlgParamer detailDlgParamer = new LeaderBoardDetailDlg.LeaderBoardDetailDlgParamer();
    detailDlgParamer.optItemID = this.optItemID;
    detailDlgParamer.datas = this.ParseData(result as Hashtable);
    detailDlgParamer.current = this.GetCurrentRankData(result as Hashtable, detailDlgParamer.datas);
    detailDlgParamer.title = this.titleText.text;
    UIManager.inst.OpenDlg("LeaderBoard/LeaderBoardDetailDlg", (UI.Dialog.DialogParameter) detailDlgParamer, true, true, true);
  }

  private RankData GetCurrentRankData(Hashtable sources, List<RankData> allRankData)
  {
    if (!sources.ContainsKey((object) "mine"))
      return (RankData) null;
    return this.CreateRankData(sources[(object) "mine"] as Hashtable);
  }

  private List<RankData> ParseData(Hashtable sources)
  {
    List<RankData> rankDataList = new List<RankData>();
    if (sources.ContainsKey((object) "ranks"))
    {
      ArrayList source = sources[(object) "ranks"] as ArrayList;
      for (int index = 0; index < source.Count; ++index)
      {
        RankData rankData = this.CreateRankData(source[index] as Hashtable);
        rankDataList.Add(rankData);
      }
    }
    return rankDataList;
  }

  private RankData CreateRankData(Hashtable source)
  {
    RankData rankData;
    if (source.ContainsKey((object) "alliance_id"))
      rankData = (RankData) new AllianceRankData()
      {
        AllianceId = int.Parse(source[(object) "alliance_id"].ToString()),
        Lang = source[(object) "lang"].ToString(),
        Member = int.Parse(source[(object) "member"].ToString())
      };
    else
      rankData = (RankData) new PlayerRankData()
      {
        Portrait = int.Parse(source[(object) "portrait"].ToString())
      };
    rankData.Name = source[(object) "name"].ToString();
    rankData.Score = long.Parse(source[(object) "score"].ToString());
    rankData.Rank = int.Parse(source[(object) "rank"].ToString());
    if (source.ContainsKey((object) "uid"))
      rankData.UID = source[(object) "uid"].ToString();
    if (source.ContainsKey((object) "tag"))
    {
      rankData.Tag = source[(object) "tag"].ToString();
      rankData.Symbol = int.Parse(source[(object) "symbol_code"].ToString());
    }
    return rankData;
  }
}
