﻿// Decompiled with JetBrains decompiler
// Type: UIHideHelper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class UIHideHelper : MonoBehaviour
{
  public const float TweenDelta = 0.2f;
  private bool needRemove;
  public List<HideContext> allControlUIs;

  private void Awake()
  {
    UIHideHelperManager.Instance.AddHelper(this);
  }

  private void OnDestroy()
  {
    UIHideHelperManager.Instance.RemoveHelper(this);
  }

  public bool NeedRemove
  {
    get
    {
      return this.needRemove;
    }
    set
    {
      this.needRemove = value;
    }
  }

  public virtual void Hide()
  {
    if (this.NeedRemove)
      return;
    List<HideContext>.Enumerator enumerator = this.allControlUIs.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Hide();
  }

  public virtual void Show()
  {
    if (this.needRemove)
      return;
    List<HideContext>.Enumerator enumerator = this.allControlUIs.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Show();
  }

  public virtual void QuickShow()
  {
    if (this.needRemove)
      return;
    List<HideContext>.Enumerator enumerator = this.allControlUIs.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.QuickShow();
  }

  public virtual void QuickHide()
  {
    if (this.needRemove)
      return;
    List<HideContext>.Enumerator enumerator = this.allControlUIs.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.QuickHide();
  }
}
