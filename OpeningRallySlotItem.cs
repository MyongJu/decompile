﻿// Decompiled with JetBrains decompiler
// Type: OpeningRallySlotItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class OpeningRallySlotItem : MonoBehaviour
{
  public UILabel index;
  public OpeningRallyEmptySlotItem emtpyItem;
  public OpeningRallyJoinedSlotItem joinItem;
  public System.Action OnOpenCallBackDelegate;

  public void SetData(int index)
  {
    this.index.text = (index + 1).ToString();
    if (index != 2)
    {
      NGUITools.SetActive(this.emtpyItem.gameObject, false);
      NGUITools.SetActive(this.joinItem.gameObject, true);
      this.joinItem.SetData(index);
      this.joinItem.OnOpenCallBackDelegate = this.OnOpenCallBackDelegate;
    }
    else
    {
      NGUITools.SetActive(this.emtpyItem.gameObject, true);
      NGUITools.SetActive(this.joinItem.gameObject, false);
    }
  }

  public void Clear()
  {
    NGUITools.SetActive(this.emtpyItem.gameObject, false);
    NGUITools.SetActive(this.joinItem.gameObject, false);
    this.joinItem.Clear();
  }
}
