﻿// Decompiled with JetBrains decompiler
// Type: AllianceRewardGroupData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class AllianceRewardGroupData
{
  public int rankMin;
  public int rankMax;
  public int fund;
  public int honor;
  public Dictionary<ItemStaticInfo, int> allianceRewards;
  public Dictionary<ItemStaticInfo, int> individualRewards;
}
