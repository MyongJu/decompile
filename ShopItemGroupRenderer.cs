﻿// Decompiled with JetBrains decompiler
// Type: ShopItemGroupRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ShopItemGroupRenderer : MonoBehaviour
{
  public ShopItemRenderer[] renderers;
  public System.Action<bool, int> OnSelectedHandler;
  public System.Action<int> OnCustomBuy;

  public bool Selected { private set; get; }

  public void HideHightLight()
  {
    for (int index = 0; index < this.renderers.Length; ++index)
      this.renderers[index].Selected = false;
    this.Selected = false;
  }

  public void Dispose()
  {
    for (int index = 0; index < this.renderers.Length; ++index)
      this.renderers[index].Dispose();
    this.OnSelectedHandler = (System.Action<bool, int>) null;
  }

  public bool Contains(int shopId)
  {
    for (int index = 0; index < this.renderers.Length; ++index)
    {
      if (this.renderers[index].Shop_ID == shopId)
        return true;
    }
    return false;
  }

  public void SetData(params int[] ids)
  {
    int num = 0;
    UIDragScrollView component = this.gameObject.GetComponent<UIDragScrollView>();
    for (int index = 0; index < ids.Length; ++index)
    {
      if (ids[index] > 0)
      {
        ++num;
        this.renderers[index].FeedData((IComponentData) this.GetIconData(ids[index]));
        this.renderers[index].Selected = false;
        this.renderers[index].OnSelectedHandler = new System.Action<ShopItemRenderer>(this.OnSelected);
        this.renderers[index].OnCustomBuy = this.OnCustomBuy;
        NGUITools.SetActive(this.renderers[index].gameObject, true);
        this.renderers[index].SetScrollView(component.scrollView);
      }
    }
    if (this.renderers.Length <= num)
      return;
    for (int index = num; index < this.renderers.Length; ++index)
      NGUITools.SetActive(this.renderers[index].gameObject, false);
  }

  private void OnSelected(ShopItemRenderer item)
  {
    for (int index = 0; index < this.renderers.Length; ++index)
    {
      if ((UnityEngine.Object) this.renderers[index] != (UnityEngine.Object) item)
        this.renderers[index].Selected = false;
    }
    this.Selected = item.Selected;
    if (this.OnSelectedHandler == null)
      return;
    this.OnSelectedHandler(item.Selected, item.Shop_ID);
  }

  private IconData GetIconData(int shopId)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(ConfigManager.inst.DB_Shop.GetShopDataByInternalId(shopId).Item_InternalId);
    IconData iconData = new IconData();
    iconData.image = itemStaticInfo.ImagePath;
    iconData.Data = (object) shopId;
    iconData.contents = new string[2];
    iconData.contents[0] = itemStaticInfo.LocName;
    iconData.contents[1] = itemStaticInfo.LocDescription;
    return iconData;
  }
}
