﻿// Decompiled with JetBrains decompiler
// Type: ConfigMagicTowerMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigMagicTowerMain
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, MagicTowerMainInfo> datas;
  private Dictionary<int, MagicTowerMainInfo> dicByUniqueId;
  private List<int> _costItems;

  public void BuildDB(object res)
  {
    this.parse.Parse<MagicTowerMainInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  private int SortDelegate(ItemStaticInfo a, ItemStaticInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }

  public MagicTowerMainInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (MagicTowerMainInfo) null;
  }

  public MagicTowerMainInfo GetData(int internalId)
  {
    if (this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (MagicTowerMainInfo) null;
  }

  public MagicTowerMainInfo GetDataWithLevel(int level)
  {
    MagicTowerMainInfo magicTowerMainInfo = (MagicTowerMainInfo) null;
    if (level > 0)
    {
      Dictionary<string, MagicTowerMainInfo>.Enumerator enumerator = this.datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.Level == level)
        {
          magicTowerMainInfo = enumerator.Current.Value;
          break;
        }
      }
    }
    return magicTowerMainInfo;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, MagicTowerMainInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, MagicTowerMainInfo>) null;
  }
}
