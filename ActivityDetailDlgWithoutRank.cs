﻿// Decompiled with JetBrains decompiler
// Type: ActivityDetailDlgWithoutRank
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;

public class ActivityDetailDlgWithoutRank : UI.Dialog
{
  public ActivityStepReward stepReward;
  public ActivityStepRequire stepRequire;
  public UILabel titalLabel;
  public UILabel subTitleLabel;
  public List<UILabel> tabLabels;
  private ActivityMainInfo mainInfo;
  private ActivityRewardInfo rewardInfo;
  private RoundActivityData data;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.data = (orgParam as ActivityDetailDlgWithoutRank.Parameter).data;
    this.mainInfo = ConfigManager.inst.DB_ActivityMain.GetData(ConfigManager.inst.DB_ActivityMainSub.GetData(this.data.CurrentRound.ActivitySubConfigID).ActivityMainID);
    this.rewardInfo = ConfigManager.inst.DB_ActivityReward.GetCurrentStepReward(this.data.CurrentRound.ActivitySubConfigID, this.data.StrongholdLevel);
    this.UpdateUI();
    this.AddEventHandler();
  }

  private void AddEventHandler()
  {
    ActivityManager.Intance.OnTimeLimitActivityStartNewRound += new System.Action(this.StartNewRound);
    ActivityManager.Intance.OnTimeLimitActivityFinish += new System.Action(this.OnFinish);
  }

  private void OnFinish()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void StartNewRound()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void RemoveEventHandler()
  {
    ActivityManager.Intance.OnTimeLimitActivityStartNewRound -= new System.Action(this.StartNewRound);
    ActivityManager.Intance.OnTimeLimitActivityFinish -= new System.Action(this.OnFinish);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
  }

  private void UpdateUI()
  {
    this.ConfigLables();
    this.stepReward.UpdateUI(this.data);
    this.stepRequire.UpdatUI(this.mainInfo.internalId);
  }

  private void ConfigLables()
  {
    this.titalLabel.text = Utils.XLAT("event_center_name");
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("1", this.data.CurrentRound.index.ToString() + "/" + this.data.TotalRounds.ToString());
    ActivityMainSubInfo data = ConfigManager.inst.DB_ActivityMainSub.GetData(this.data.CurrentRound.ActivitySubConfigID);
    this.subTitleLabel.text = ScriptLocalization.GetWithPara("event_stage_num", para, true);
    this.subTitleLabel.text = this.subTitleLabel.text + " " + data.LocName;
    this.tabLabels[0].text = Utils.XLAT("event_my_points") + " " + (object) this.data.CurrentRoundScore;
  }

  public void OnViewRankHistoryBtClick()
  {
    UIManager.inst.OpenPopup("Activity/ActivityRankPopup", (Popup.PopupParameter) new TimeLimitActivityRankPopup.Parameter()
    {
      isStepRank = true,
      data = (ActivityBaseData) this.data
    });
  }

  public void OnTotalRankHistoryBtClick()
  {
    UIManager.inst.OpenPopup("Activity/ActivityRankPopup", (Popup.PopupParameter) new TimeLimitActivityRankPopup.Parameter()
    {
      isStepRank = false,
      data = (ActivityBaseData) this.data
    });
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public RoundActivityData data;
  }
}
