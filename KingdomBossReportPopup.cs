﻿// Decompiled with JetBrains decompiler
// Type: KingdomBossReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class KingdomBossReportPopup : BaseReportPopup
{
  private List<GameObject> pool = new List<GameObject>();
  private List<RewardRender> rewardsrenders = new List<RewardRender>();
  private const string bossiconpath = "Texture/AllianceBoss/";
  private KingdomBossReportContent kbrc;
  private WarMessage warMessage;
  public UIScrollView sv;
  public UITable grid;
  public UITable rewardsContentTable;
  public GameObject rewardRender;
  public UIButton receiveBtn;
  public UITable contentTable;
  public Icon leader;
  public Icon boss;
  public IconListComponent list;
  public GameObject trooptemplate;
  public UILabel content;
  public UISlider damageSlider;
  public UISprite begintroops;
  public UILabel progressDesc;
  private SystemMessage.Reward _reward;

  private void Init()
  {
    KingdomBossStaticInfo kingdomBossStaticInfo = ConfigManager.inst.DB_KingdomBoss.GetItem((int) this.kbrc.defender.uid);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/AllianceBoss/world_boss", (System.Action<bool>) null, true, false, string.Empty);
    this.leader.SetData("Texture/Hero/Portrait_Icon/player_portrait_icon_" + this.kbrc.attacker.portrait, this.kbrc.attacker.icon, this.kbrc.attacker.lord_title, new string[2]
    {
      this.kbrc.attacker.fullname,
      this.kbrc.attacker.site
    });
    this.boss.SetData("Texture/Events/icon_world_boss", new string[3]
    {
      Utils.XLAT("id_lv") + (object) kingdomBossStaticInfo.BossLevel + " " + kingdomBossStaticInfo.LocalName,
      this.kbrc.defender.end_total_troops.ToString(),
      (this.kbrc.defender.start_total_troops - this.kbrc.defender.end_total_troops).ToString()
    });
    if (this.kbrc.defender.original_total_troops == 0L)
      this.kbrc.defender.original_total_troops = this.kbrc.defender.start_total_troops;
    float num = (float) this.kbrc.defender.end_total_troops / (float) this.kbrc.defender.original_total_troops;
    this.damageSlider.value = num;
    this.begintroops.width = (int) (float) ((double) this.kbrc.defender.start_total_troops / (double) this.kbrc.defender.original_total_troops * 574.0);
    this.progressDesc.text = string.Format("{0:0.00%}", (object) num);
    this.trooptemplate.GetComponent<MemberInfoItem>().SeedData(new MemberDetail()
    {
      k = this.kbrc.attacker.k,
      x = this.kbrc.attacker.x,
      y = this.kbrc.attacker.y,
      icon = this.kbrc.attacker.icon,
      acronym = this.kbrc.attacker.acronym,
      name = this.kbrc.attacker.name,
      portrait = this.kbrc.attacker.portrait,
      uid = this.kbrc.attacker.uid.ToString(),
      start_troops = this.kbrc.attacker.start_total_troops,
      kill_troops = this.kbrc.attacker.kill_total_troops
    });
    this.UpdateReward();
    this.grid.Reposition();
    this.rewardsContentTable.Reposition();
    Utils.ExecuteInSecs(1f / 1000f, (System.Action) (() => this.contentTable.repositionNow = true));
    this.receiveBtn.isEnabled = this.warMessage.attachment_status == 1;
    if (!this.receiveBtn.isEnabled)
      return;
    this.receiveBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.HandleReceiveBtnClick)));
  }

  private void UpdateReward()
  {
    this.Clean();
    if (this._reward == null)
    {
      this.rewardsContentTable.gameObject.SetActive(false);
    }
    else
    {
      this.rewardsContentTable.gameObject.SetActive(true);
      this.UpdateResources();
      this.UpdateItems();
      this.UpdateGold();
    }
    this.content.text = ScriptLocalization.Get("mail_subject_kingdom_boss_battle", true);
    this.grid.repositionNow = true;
    this.rewardsContentTable.repositionNow = true;
  }

  private void ConfigReward()
  {
    Hashtable hashtable = this.param.mail.data[(object) "attachment"] as Hashtable;
    if (hashtable == null)
      return;
    this._reward = new SystemMessage.Reward();
    if (hashtable.ContainsKey((object) "item"))
    {
      this._reward.items = new List<SystemMessage.ItemReward>();
      foreach (DictionaryEntry dictionaryEntry in hashtable[(object) "item"] as Hashtable)
        this._reward.items.Add(new SystemMessage.ItemReward()
        {
          internalID = int.Parse(dictionaryEntry.Key.ToString()),
          amount = int.Parse(dictionaryEntry.Value.ToString())
        });
    }
    if (hashtable.ContainsKey((object) "resource"))
    {
      this._reward.resources = new List<SystemMessage.ResourceReward>();
      foreach (DictionaryEntry dictionaryEntry in hashtable[(object) "resource"] as Hashtable)
        this._reward.resources.Add(new SystemMessage.ResourceReward()
        {
          resourceName = dictionaryEntry.Key.ToString(),
          amount = int.Parse(dictionaryEntry.Value.ToString())
        });
    }
    if (hashtable.ContainsKey((object) "item"))
    {
      this._reward.items = new List<SystemMessage.ItemReward>();
      foreach (DictionaryEntry dictionaryEntry in hashtable[(object) "item"] as Hashtable)
        this._reward.items.Add(new SystemMessage.ItemReward()
        {
          internalID = int.Parse(dictionaryEntry.Key.ToString()),
          amount = int.Parse(dictionaryEntry.Value.ToString())
        });
    }
    if (!hashtable.ContainsKey((object) "gold"))
      return;
    int num = 0;
    try
    {
      num = int.Parse(hashtable[(object) "gold"].ToString());
    }
    catch
    {
      D.error((object) "Mail gold is not int num");
    }
    this._reward.goldReward = new SystemMessage.GoldReward()
    {
      amount = num
    };
  }

  private void UpdateResources()
  {
    if (this._reward == null)
      return;
    List<SystemMessage.ResourceReward> resources = this._reward.resources;
    if (resources == null)
      return;
    for (int index = 0; index < resources.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.rewardRender);
      gameObject.SetActive(true);
      RewardRender component = gameObject.GetComponent<RewardRender>();
      component.Feed(resources[index]);
      this.rewardsrenders.Add(component);
    }
  }

  private void UpdateGold()
  {
    if (this._reward == null)
      return;
    SystemMessage.GoldReward goldReward = this._reward.goldReward;
    if (goldReward == null)
      return;
    GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.rewardRender);
    gameObject.SetActive(true);
    RewardRender component = gameObject.GetComponent<RewardRender>();
    component.Feed(goldReward);
    this.rewardsrenders.Add(component);
  }

  private void Clean()
  {
    using (List<RewardRender>.Enumerator enumerator = this.rewardsrenders.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.rewardsrenders.Clear();
  }

  private void UpdateItems()
  {
    if (this._reward == null)
      return;
    List<SystemMessage.ItemReward> items = this._reward.items;
    if (items == null)
      return;
    for (int index = 0; index < items.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.rewardRender);
      gameObject.SetActive(true);
      RewardRender component = gameObject.GetComponent<RewardRender>();
      component.Feed(items[index]);
      this.rewardsrenders.Add(component);
    }
  }

  private void Reset()
  {
    this.sv.ResetPosition();
    Rigidbody component = this.sv.transform.GetComponent<Rigidbody>();
    if ((UnityEngine.Object) null != (UnityEngine.Object) component)
    {
      component.isKinematic = true;
      component.useGravity = false;
    }
    this.HideTemplate(false);
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        PrefabManagerEx.Instance.Destroy(current);
      }
    }
    this.pool.Clear();
  }

  private void HideTemplate(bool hide)
  {
    this.trooptemplate.SetActive(!hide);
  }

  public void GotokingdomBoss()
  {
    this.GoToTargetPlace(int.Parse(this.kbrc.k), int.Parse(this.kbrc.x), int.Parse(this.kbrc.y));
  }

  protected void HandleReceiveBtnClick()
  {
    this.receiveBtn.isEnabled = false;
    this.CancelInvoke();
    PlayerData.inst.mail.API.GainAttachment((int) this.warMessage.category, this.warMessage.mailID, new System.Action<bool, object>(this.GainAttachmentCallBack));
  }

  protected void GainAttachmentCallBack(bool ok, object obj)
  {
    if (!ok)
      return;
    this.warMessage.attachment_status = 2;
    this.CollectRewards();
  }

  private void CollectRewards()
  {
    Hashtable hashtable = this.param.mail.data[(object) "attachment"] as Hashtable;
    if (hashtable == null)
      return;
    RewardData rewardData = JsonReader.Deserialize<RewardData>(Utils.Object2Json((object) hashtable));
    List<IconData> iconDataList = new List<IconData>();
    if (rewardData.item != null)
    {
      RewardsCollectionAnimator.Instance.Clear();
      using (Dictionary<string, int>.Enumerator enumerator = rewardData.item.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int internalId = int.Parse(current.Key);
          int num = current.Value;
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
          RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
          {
            icon = itemStaticInfo.ImagePath,
            count = (float) num
          });
        }
      }
    }
    List<SystemMessage.ResourceReward> resources = this._reward.resources;
    if (resources != null)
    {
      for (int index = 0; index < resources.Count; ++index)
      {
        ResRewardsInfo.Data data = new ResRewardsInfo.Data();
        data.count = resources[index].amount;
        string resourceName = resources[index].resourceName;
        if (resourceName != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (KingdomBossReportPopup.\u003C\u003Ef__switch\u0024mapA2 == null)
          {
            // ISSUE: reference to a compiler-generated field
            KingdomBossReportPopup.\u003C\u003Ef__switch\u0024mapA2 = new Dictionary<string, int>(4)
            {
              {
                "food",
                0
              },
              {
                "wood",
                1
              },
              {
                "ore",
                2
              },
              {
                "silver",
                3
              }
            };
          }
          int num;
          // ISSUE: reference to a compiler-generated field
          if (KingdomBossReportPopup.\u003C\u003Ef__switch\u0024mapA2.TryGetValue(resourceName, out num))
          {
            switch (num)
            {
              case 0:
                data.rt = ResRewardsInfo.ResType.Food;
                break;
              case 1:
                data.rt = ResRewardsInfo.ResType.Wood;
                break;
              case 2:
                data.rt = ResRewardsInfo.ResType.Ore;
                break;
              case 3:
                data.rt = ResRewardsInfo.ResType.Silver;
                break;
            }
          }
        }
        RewardsCollectionAnimator.Instance.Ress.Add(data);
      }
    }
    RewardsCollectionAnimator.Instance.CollectItems(false);
    RewardsCollectionAnimator.Instance.CollectResource(false);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.warMessage = this.param.mail as WarMessage;
    this.kbrc = JsonReader.Deserialize<KingdomBossReportContent>(Utils.Object2Json((object) this.param.hashtable));
    this.ConfigReward();
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }
}
