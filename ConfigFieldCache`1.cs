﻿// Decompiled with JetBrains decompiler
// Type: ConfigFieldCache`1
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.InteropServices;

public class ConfigFieldCache<T>
{
  private ConfigRawData _rawCache;
  private Dictionary<int, T> _cache;
  private System.Type _type;
  private int _index;

  public void Initialize(ConfigRawData rawCache, string key)
  {
    this._rawCache = rawCache;
    this._cache = new Dictionary<int, T>();
    this._type = typeof (T);
    this._index = this._rawCache.GetFieldIndex(key);
  }

  public T Get(int internalId, [Optional] T defaultValue)
  {
    T obj;
    if (!this._cache.TryGetValue(internalId, out obj))
    {
      object target;
      obj = !this._rawCache.TryGetRecordField(this._type, internalId, this._index, out target) ? defaultValue : (T) target;
      this._cache.Add(internalId, obj);
    }
    return obj;
  }
}
