﻿// Decompiled with JetBrains decompiler
// Type: BuffUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class BuffUIData : IBuffUIData
{
  public BoostCategory _category = BoostCategory.none;
  private BoostType _type = BoostType.none;

  public string Icon { get; set; }

  public string Name { get; set; }

  public string IconPath { get; set; }

  public string Desc { get; set; }

  public object Data { get; set; }

  public BoostCategory Category
  {
    get
    {
      return this._category;
    }
    set
    {
      this._category = value;
    }
  }

  public BoostType Type
  {
    get
    {
      return this._type;
    }
    set
    {
      this._type = value;
    }
  }

  public virtual bool ListenerClick()
  {
    return true;
  }

  public virtual void Refresh()
  {
  }

  public virtual int RemainTime()
  {
    return 0;
  }

  public virtual bool IsRunning()
  {
    return false;
  }

  public virtual float GetProgressValue()
  {
    return 0.0f;
  }

  public int GetLeftCdTime()
  {
    if (this.Type == BoostType.peaceshield)
      return PlayerData.inst.playerCityData.LeftPeaceShieldCdTime;
    return 0;
  }
}
