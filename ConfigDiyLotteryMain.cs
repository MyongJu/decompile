﻿// Decompiled with JetBrains decompiler
// Type: ConfigDiyLotteryMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDiyLotteryMain
{
  private List<DiyLotteryMainInfo> _diyLotteryMainInfoList = new List<DiyLotteryMainInfo>();
  private Dictionary<string, DiyLotteryMainInfo> _datas;
  private Dictionary<int, DiyLotteryMainInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<DiyLotteryMainInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, DiyLotteryMainInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._diyLotteryMainInfoList.Add(enumerator.Current);
    }
  }

  public DiyLotteryMainInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (DiyLotteryMainInfo) null;
  }

  public DiyLotteryMainInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (DiyLotteryMainInfo) null;
  }
}
