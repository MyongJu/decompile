﻿// Decompiled with JetBrains decompiler
// Type: AllianceOverview
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;
using UnityEngine;

public class AllianceOverview : Popup
{
  public UILabel m_AllianceNameLabel;
  public UILabel m_AlliancePowerLabel;
  public UILabel m_MemberCountLabel;
  public UILabel m_GiftLevelLabel;
  public UILabel m_HighlordNameLabel;
  public AllianceSymbol m_AllianceSymbol;
  public GameObject m_RootNode;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    AllianceOverview.Parameter param = orgParam as AllianceOverview.Parameter;
    this.m_RootNode.SetActive(false);
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) param.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(postData, (System.Action<bool, object>) ((_param1, _param2) =>
    {
      this.m_RootNode.SetActive(true);
      this.UpdateUI(param.allianceId);
    }), true);
  }

  private void UpdateUI(long allianceId)
  {
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(allianceId);
    this.m_AllianceNameLabel.text = string.Format("({0}) {1}", (object) allianceData.allianceAcronym, (object) allianceData.allianceName);
    this.m_AlliancePowerLabel.text = allianceData.power.ToString();
    this.m_MemberCountLabel.text = string.Format("{0}/{1}", (object) allianceData.memberCount, (object) allianceData.memberMax);
    this.m_GiftLevelLabel.text = ConfigManager.inst.DB_Gift_Level.GetLevelByPoints((int) allianceData.giftPoints).ToString();
    this.m_AllianceSymbol.SetSymbols(allianceData.allianceSymbolCode);
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public long allianceId;
  }
}
