﻿// Decompiled with JetBrains decompiler
// Type: WishWellAnimController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class WishWellAnimController : MonoBehaviour
{
  public UILabel m_Quantity;
  public UILabel m_Critical;
  public UITexture m_Icon;

  public void SetData(string type, int gain, int critical)
  {
    string str1 = Utils.FormatThousands(gain.ToString());
    string str2 = Utils.FormatThousands(critical.ToString());
    this.m_Quantity.text = str1;
    this.m_Critical.text = "x" + str2;
    this.m_Critical.gameObject.SetActive(critical > 1);
    this.SetTexture(type);
  }

  public void OnFinished()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  private void SetTexture(string type)
  {
    string key = type;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (WishWellAnimController.\u003C\u003Ef__switch\u0024mapB0 == null)
    {
      // ISSUE: reference to a compiler-generated field
      WishWellAnimController.\u003C\u003Ef__switch\u0024mapB0 = new Dictionary<string, int>(5)
      {
        {
          "food",
          0
        },
        {
          "wood",
          1
        },
        {
          "ore",
          2
        },
        {
          "silver",
          3
        },
        {
          "steel",
          4
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!WishWellAnimController.\u003C\u003Ef__switch\u0024mapB0.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, "Texture/BuildingConstruction/food_icon", (System.Action<bool>) null, true, false, string.Empty);
        break;
      case 1:
        BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, "Texture/BuildingConstruction/wood_icon", (System.Action<bool>) null, true, false, string.Empty);
        break;
      case 2:
        BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, "Texture/BuildingConstruction/ore_icon", (System.Action<bool>) null, true, false, string.Empty);
        break;
      case 3:
        BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, "Texture/BuildingConstruction/silver_icon", (System.Action<bool>) null, true, false, string.Empty);
        break;
      case 4:
        BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, "Texture/BuildingConstruction/steel_icon", (System.Action<bool>) null, true, false, string.Empty);
        break;
    }
  }
}
