﻿// Decompiled with JetBrains decompiler
// Type: IAPStoreUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public static class IAPStoreUtils
{
  public static void GetPurchaseStats(string id, out long count, out long date)
  {
    count = 0L;
    date = 0L;
    StatsData statsData = DBManager.inst.DB_Stats.Get("iap_" + id);
    if (statsData == null)
      return;
    count = statsData.count;
    date = statsData.mtime;
  }
}
