﻿// Decompiled with JetBrains decompiler
// Type: TileData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UnityEngine;

public class TileData
{
  public BetterList<long> MagicIds = new BetterList<long>();
  public const int INVALID_LEVEL = 0;
  public const int INVALID_VALUE = 0;
  public const string TILE_KEY = "kingdom_map";
  public const int INVALID_REFERENCE = -1;
  public long TileID;
  public long OwnerID;
  public long MarchID;
  public long CityID;
  public int Level;
  public int Value;
  public int ResourceId;
  public TileMagic Magic;
  public Coordinate Location;
  public WorldCoordinate WorldLocation;
  public Vector3 Position;
  public int ReferenceX;
  public int ReferenceY;
  private bool WasMonster;
  public bool WasCity;
  private Coroutine m_AttackCoroutine;
  private Coroutine m_DeathCoroutine;
  private Coroutine m_TeleportCoroutine;
  private PVPFortressBorder m_FortressView;
  private PVPTile m_TileView;
  private long m_TerrainTileID;
  private UserData m_UserData;
  private CityData m_CityData;
  private AllianceFortressData m_FortressData;
  private AllianceWareHouseData m_WareHouseData;
  private AllianceSuperMineData m_SuperMineData;
  private AlliancePortalData m_PortalData;
  private AllianceHospitalData m_HospitalData;
  private AllianceTempleData m_TempleData;
  private WorldBossData m_WorldBossData;
  private KingdomBossData m_KingdomBossData;
  private WonderTowerData m_WonderTowerData;
  private DB.WonderData m_WonderData;
  private DigSiteData m_DigSiteData;
  private ScoutCacheData m_ScoutCacheData;
  private TileType m_Type;
  protected TileData.SpellAbleState m_spellAbleState;
  private static int DEBUG_GUID;
  private int m_GUID;

  public TileData(BlockData block)
  {
    this.Container = block;
    this.Magic = new TileMagic(this);
    this.Reset();
    this.m_GUID = TileData.DEBUG_GUID++;
  }

  public BlockData Container { protected set; get; }

  public TileData.SpellAbleState CurrentSpellAbleState
  {
    get
    {
      return this.m_spellAbleState;
    }
    set
    {
      if (this.m_spellAbleState == value)
        return;
      this.m_spellAbleState = value;
      this.OnUpdate();
    }
  }

  public TileType TileType
  {
    get
    {
      return this.m_Type;
    }
  }

  public PVPTile TileView
  {
    get
    {
      return this.m_TileView;
    }
  }

  public long AllianceID
  {
    get
    {
      if (this.m_UserData != null)
        return this.m_UserData.allianceId;
      if (this.m_FortressData != null)
        return this.m_FortressData.allianceId;
      if (this.m_WareHouseData != null)
        return this.m_WareHouseData.AllianceId;
      if (this.m_SuperMineData != null)
        return this.m_SuperMineData.AllianceId;
      if (this.m_PortalData != null)
        return this.m_PortalData.AllianceId;
      if (this.m_HospitalData != null)
        return this.m_HospitalData.AllianceID;
      if (this.m_TempleData != null)
        return this.m_TempleData.allianceId;
      if (this.m_WonderTowerData != null)
        return this.m_WonderTowerData.OWNER_ALLIANCE_ID;
      return 0;
    }
  }

  public string OwnerName
  {
    get
    {
      if (this.m_UserData != null)
        return this.m_UserData.userName;
      return string.Empty;
    }
  }

  public UserData UserData
  {
    get
    {
      return this.m_UserData;
    }
  }

  public bool UseReference
  {
    get
    {
      if (this.ReferenceX == -1 || this.ReferenceY == -1)
        return false;
      if (this.ReferenceX == this.Location.X)
        return this.ReferenceY != this.Location.Y;
      return true;
    }
  }

  public CityData CityData
  {
    get
    {
      return this.m_CityData;
    }
  }

  public AllianceFortressData FortressData
  {
    get
    {
      return this.m_FortressData;
    }
  }

  public AllianceWareHouseData WareHouseData
  {
    get
    {
      return this.m_WareHouseData;
    }
  }

  public AllianceSuperMineData SuperMineData
  {
    get
    {
      return this.m_SuperMineData;
    }
  }

  public KingdomBossData KingdomBossData
  {
    get
    {
      return this.m_KingdomBossData;
    }
  }

  public AlliancePortalData PortalData
  {
    get
    {
      return this.m_PortalData;
    }
  }

  public AllianceHospitalData HospitalData
  {
    get
    {
      return this.m_HospitalData;
    }
  }

  public AllianceTempleData TempleData
  {
    get
    {
      return this.m_TempleData;
    }
  }

  public WorldBossData WorldBossData
  {
    get
    {
      return this.m_WorldBossData;
    }
  }

  public WonderTowerData WonderTowerData
  {
    get
    {
      return this.m_WonderTowerData;
    }
  }

  public DB.WonderData WonderData
  {
    get
    {
      return this.m_WonderData;
    }
  }

  public DigSiteData DigSiteData
  {
    get
    {
      return this.m_DigSiteData;
    }
  }

  public ScoutCacheData ScoutData
  {
    get
    {
      return this.m_ScoutCacheData;
    }
  }

  public void Reset()
  {
    this.TileID = 0L;
    this.m_TerrainTileID = 0L;
    this.OwnerID = 0L;
    this.MarchID = 0L;
    this.CityID = -1L;
    this.Level = 0;
    this.Value = 0;
    this.m_UserData = (UserData) null;
    this.m_CityData = (CityData) null;
    this.m_FortressData = (AllianceFortressData) null;
    this.m_WareHouseData = (AllianceWareHouseData) null;
    this.m_SuperMineData = (AllianceSuperMineData) null;
    this.m_PortalData = (AlliancePortalData) null;
    this.m_HospitalData = (AllianceHospitalData) null;
    this.m_TempleData = (AllianceTempleData) null;
    this.m_WorldBossData = (WorldBossData) null;
    this.m_KingdomBossData = (KingdomBossData) null;
    this.m_WonderTowerData = (WonderTowerData) null;
    this.m_WonderData = (DB.WonderData) null;
    this.m_DigSiteData = (DigSiteData) null;
    this.m_ScoutCacheData = (ScoutCacheData) null;
    this.ResourceId = 0;
    this.MagicIds.Clear();
    this.Magic.Reset();
    this.ReferenceX = -1;
    this.ReferenceY = -1;
    this.m_Type = TileType.None;
    this.m_spellAbleState = TileData.SpellAbleState.SpellAbleNone;
    this.WasMonster = false;
    this.WasCity = false;
    this.Hide();
    this.OnDestroyFortress();
  }

  public bool IsValid()
  {
    return this.m_Type != TileType.None;
  }

  public bool IsOnBorder
  {
    get
    {
      if (this.Location.X != 0)
        return this.Location.Y == 0;
      return true;
    }
  }

  public bool Teleporting
  {
    get
    {
      if (this.m_CityData != null)
        return this.m_CityData.teleporting;
      return false;
    }
  }

  public void Show()
  {
    if (this.Teleporting || this.WasCity)
      return;
    if (this.m_TileView == null)
      this.m_TileView = new PVPTile(this);
    this.UpdateUI();
    if (this.Container.Viewport.Contains(this.Position, true))
    {
      this.m_TileView.Show();
    }
    else
    {
      this.Magic.Hide();
      this.m_TileView.Hide();
    }
  }

  public void Hide()
  {
    if (this.m_TileView != null)
    {
      this.Magic.Hide();
      this.m_TileView.Hide();
    }
    if (this.m_AttackCoroutine != null)
    {
      Utils.StopCoroutine(this.m_AttackCoroutine);
      this.m_AttackCoroutine = (Coroutine) null;
    }
    if (this.m_DeathCoroutine != null)
    {
      Utils.StopCoroutine(this.m_DeathCoroutine);
      this.m_DeathCoroutine = (Coroutine) null;
    }
    if (this.m_TeleportCoroutine == null)
      return;
    Utils.StopCoroutine(this.m_TeleportCoroutine);
    this.m_TeleportCoroutine = (Coroutine) null;
  }

  public bool GetMultiTileSize(out int outSizeX, out int outSizeY)
  {
    outSizeX = 1;
    outSizeY = 1;
    if (this.m_TileView == null || !((UnityEngine.Object) this.m_TileView.TileAsset != (UnityEngine.Object) null))
      return false;
    outSizeX = this.m_TileView.TileAsset.xSize;
    outSizeY = this.m_TileView.TileAsset.ySize;
    return true;
  }

  public bool IsCenter
  {
    get
    {
      if (this.m_TileView != null && (UnityEngine.Object) this.m_TileView.TileAsset != (UnityEngine.Object) null)
        return this.m_TileView.TileAsset.IsCenter;
      return false;
    }
  }

  public void UpdateTerrain(long tileID)
  {
    this.m_TerrainTileID = tileID;
    if (this.TileID != 0L || !this.IsTerrainType)
      return;
    this.ResetToTerrain(false);
  }

  public void ResetToTerrain(bool resetRefs)
  {
    this.CheckWhatItWas();
    if (resetRefs)
    {
      this.ReferenceX = -1;
      this.ReferenceY = -1;
    }
    this.TileID = this.m_TerrainTileID;
    TileDefinition tileDefinition = (TileDefinition) null;
    this.Container.MapData.Tileset.TryGetValue(this.TileID, out tileDefinition);
    if (tileDefinition != null)
      this.m_Type = tileDefinition.TileType;
    this.Regenerate();
  }

  public void ForceResetToTerrain()
  {
    this.TileID = this.m_TerrainTileID;
    TileDefinition tileDefinition = (TileDefinition) null;
    this.Container.MapData.Tileset.TryGetValue(this.TileID, out tileDefinition);
    if (tileDefinition != null)
      this.m_Type = tileDefinition.TileType;
    this.InternalRegenerate();
  }

  private bool IsMonster
  {
    get
    {
      if (this.m_Type != TileType.Encounter && this.m_Type != TileType.WorldBoss)
        return this.m_Type == TileType.KingdomBoss;
      return true;
    }
  }

  private void CheckWhatItWas()
  {
    if (!this.WasMonster)
      this.WasMonster = this.IsMonster;
    if (this.WasCity)
      return;
    this.WasCity = this.m_Type == TileType.City;
    if (!this.UseReference)
      return;
    TileData tileAt = this.Container.MapData.GetTileAt(this.Location.K, this.ReferenceX, this.ReferenceY);
    if (tileAt == null)
      return;
    this.WasCity |= tileAt.TileType == TileType.City;
    this.WasCity |= tileAt.WasCity;
  }

  public void UpdateServerData(Hashtable ht)
  {
    this.CheckWhatItWas();
    DatabaseTools.UpdateData(ht, "owner_id", ref this.OwnerID);
    this.m_UserData = DBManager.inst.DB_User.Get(this.OwnerID);
    DatabaseTools.UpdateData(ht, "city_id", ref this.CityID);
    this.m_CityData = DBManager.inst.DB_City.Get(this.CityID);
    DatabaseTools.UpdateData(ht, "march_id", ref this.MarchID);
    DatabaseTools.UpdateData(ht, "resource_level", ref this.Level);
    DatabaseTools.UpdateData(ht, "resource_amt", ref this.Value);
    if (ht.ContainsKey((object) "magic_id"))
    {
      this.MagicIds.Clear();
      Hashtable hashtable = ht[(object) "magic_id"] as Hashtable;
      if (hashtable != null)
      {
        IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
        while (enumerator.MoveNext())
        {
          long result = 0;
          if (long.TryParse(enumerator.Key.ToString(), out result))
            this.MagicIds.Add(result);
        }
      }
    }
    bool flag = false;
    int result1 = -1;
    if (ht.ContainsKey((object) "X_REF"))
    {
      int.TryParse(ht[(object) "X_REF"].ToString(), out result1);
      flag |= this.ReferenceX != result1;
      this.ReferenceX = result1;
    }
    int result2 = -1;
    if (ht.ContainsKey((object) "Y_REF"))
    {
      int.TryParse(ht[(object) "Y_REF"].ToString(), out result2);
      flag |= this.ReferenceY != result2;
      this.ReferenceY = result2;
    }
    int result3 = 0;
    if (ht.ContainsKey((object) "resource_id"))
    {
      int.TryParse(ht[(object) "resource_id"].ToString(), out result3);
      flag |= this.ResourceId != result3;
      this.ResourceId = result3;
    }
    if (ht.ContainsKey((object) "resource_type"))
    {
      this.m_Type = (TileType) int.Parse(ht[(object) "resource_type"] as string);
      switch (this.m_Type)
      {
        case TileType.Camp:
        case TileType.Resource1:
        case TileType.Resource2:
        case TileType.Resource3:
        case TileType.Resource4:
        case TileType.PitMine:
          flag = true;
          break;
        case TileType.Wonder:
          this.m_WonderData = DBManager.inst.DB_Wonder.Get((long) this.Location.K);
          flag = true;
          break;
        case TileType.WonderTower:
          this.m_WonderTowerData = DBManager.inst.DB_WonderTower.Get((long) this.ResourceId, (long) this.Location.K);
          flag = true;
          break;
        case TileType.AllianceFortress:
          this.m_FortressData = DBManager.inst.DB_AllianceFortress.Get((long) this.ResourceId);
          flag = true;
          break;
        case TileType.AllianceWarehouse:
          this.m_WareHouseData = DBManager.inst.DB_AllianceWarehouse.Get((long) this.ResourceId);
          flag = true;
          break;
        case TileType.AllianceSuperMine:
          this.m_SuperMineData = DBManager.inst.DB_AllianceSuperMine.Get((long) this.ResourceId);
          flag = true;
          break;
        case TileType.AlliancePortal:
          this.m_PortalData = DBManager.inst.DB_AlliancePortal.Get((long) this.ResourceId);
          flag = true;
          break;
        case TileType.AllianceHospital:
          this.m_HospitalData = DBManager.inst.DB_AllianceHospital.Get((long) this.ResourceId);
          flag = true;
          break;
        case TileType.AllianceTemple:
          this.m_TempleData = DBManager.inst.DB_AllianceTemple.Get((long) this.ResourceId);
          flag = true;
          break;
        case TileType.WorldBoss:
          this.m_WorldBossData = DBManager.inst.DB_WorldBossDB.Get(this.ResourceId);
          flag = true;
          break;
        case TileType.Digsite:
          this.m_DigSiteData = DBManager.inst.DB_DigSite.Get((long) this.ResourceId);
          flag = true;
          break;
        case TileType.KingdomBoss:
          this.m_KingdomBossData = DBManager.inst.DB_KingdomBossDB.Get(this.ResourceId);
          flag = true;
          break;
        default:
          flag = true;
          break;
      }
    }
    if (ht.ContainsKey((object) "scout_info"))
    {
      this.m_ScoutCacheData = new ScoutCacheData();
      this.m_ScoutCacheData.Decode(ht[(object) "scout_info"] as Hashtable);
    }
    else
      this.m_ScoutCacheData = (ScoutCacheData) null;
    this.WasCity &= this.m_Type != TileType.City;
    if (flag)
      this.Regenerate();
    this.OnUpdate();
  }

  private void Regenerate()
  {
    if (this.WasMonster && !this.IsMonster)
      this.m_AttackCoroutine = Utils.ExecuteInSecs(10f, (System.Action) (() =>
      {
        this.m_AttackCoroutine = (Coroutine) null;
        this.Kill();
        this.m_DeathCoroutine = Utils.ExecuteInSecs(5f, (System.Action) (() =>
        {
          this.m_DeathCoroutine = (Coroutine) null;
          this.WasMonster = false;
          this.InternalRegenerate();
        }));
      }));
    else if (this.WasCity)
      this.m_TeleportCoroutine = Utils.ExecuteInSecs(3f, (System.Action) (() =>
      {
        this.m_TeleportCoroutine = (Coroutine) null;
        this.WasCity = false;
        this.InternalRegenerate();
      }));
    else
      this.InternalRegenerate();
  }

  private void InternalRegenerate()
  {
    if (this.m_TileView == null)
      return;
    this.Magic.Hide();
    this.m_TileView.Hide();
    this.Show();
  }

  private void OnUpdate()
  {
    if (this.m_TileView != null)
      this.m_TileView.UpdateUI();
    if (this.m_FortressView == null)
      return;
    this.m_FortressView.UpdateZoneBorderColor();
  }

  public void UpdateUI()
  {
    this.OnUpdate();
  }

  public bool IsResourceType
  {
    get
    {
      if (this.TileType != TileType.Resource1 && this.TileType != TileType.Resource2 && (this.TileType != TileType.Resource3 && this.TileType != TileType.Resource4))
        return this.TileType == TileType.PitMine;
      return true;
    }
  }

  public bool IsTerrainType
  {
    get
    {
      if (this.TileType != TileType.None && this.TileType != TileType.Terrain)
        return this.TileType == TileType.StaticTerrain;
      return true;
    }
  }

  public void ResetMonster()
  {
    if (this.m_TileView == null)
      return;
    this.m_TileView.ResetMonster();
  }

  public void AddAttacker(long marchId)
  {
    if (this.m_TileView == null)
      return;
    this.m_TileView.AddAttacker(marchId);
  }

  public void RemoveAttacker(long marchId)
  {
    if (this.m_TileView == null)
      return;
    this.m_TileView.RemoveAttacker(marchId);
  }

  public void Kill()
  {
    if (this.m_TileView == null)
      return;
    this.m_TileView.Kill();
  }

  public void BuildZoneBorderMesh(ZoneBorderData borderData)
  {
    if (this.m_FortressView == null)
      this.m_FortressView = new PVPFortressBorder();
    this.m_FortressView.BuildZoneBorderMesh(borderData);
  }

  public void OnDestroyFortress()
  {
    if (this.m_FortressView != null)
      this.m_FortressView.Dispose();
    this.m_FortressView = (PVPFortressBorder) null;
  }

  public enum SpellAbleState
  {
    SpellAbleNone,
    SpellAbleTrue,
    SpellAbleFalse,
  }
}
