﻿// Decompiled with JetBrains decompiler
// Type: ConfigGift_Level
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigGift_Level
{
  public Dictionary<int, Gift_LevelInfo> datas;

  public ConfigGift_Level()
  {
    this.datas = new Dictionary<int, Gift_LevelInfo>();
  }

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exsit", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "gift_level");
        int index2 = ConfigManager.GetIndex(inHeader, "total_gift_value");
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          Gift_LevelInfo data = new Gift_LevelInfo();
          if (int.TryParse(key.ToString(), out data.internalId))
          {
            ArrayList arr = hashtable[key] as ArrayList;
            if (arr != null)
            {
              data.Gift_Level = ConfigManager.GetInt(arr, index1);
              data.Total_Gift_Value = ConfigManager.GetInt(arr, index2);
              this.PushData(data.internalId, data);
            }
          }
        }
      }
    }
  }

  public Dictionary<int, Gift_LevelInfo>.ValueCollection Values
  {
    get
    {
      return this.datas.Values;
    }
  }

  private void PushData(int internalId, Gift_LevelInfo data)
  {
    if (this.datas.ContainsKey(internalId))
      return;
    this.datas.Add(internalId, data);
    ConfigManager.inst.AddData(internalId, (object) data);
  }

  public void Clear()
  {
    if (this.datas == null)
      return;
    this.datas.Clear();
  }

  public Gift_LevelInfo GetData(int internalId)
  {
    if (this.datas.ContainsKey(internalId))
      return this.datas[internalId];
    return (Gift_LevelInfo) null;
  }

  public Gift_LevelInfo GetDataByLevel(int level)
  {
    using (Dictionary<int, Gift_LevelInfo>.Enumerator enumerator = this.datas.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, Gift_LevelInfo> current = enumerator.Current;
        if (current.Value.Gift_Level == level)
          return current.Value;
      }
    }
    return (Gift_LevelInfo) null;
  }

  public int GetLevelByPoints(int points)
  {
    int count = this.datas.Count;
    int level = 1;
    for (int index = 0; index < count; ++index)
    {
      Gift_LevelInfo dataByLevel = this.GetDataByLevel(level);
      if (dataByLevel != null && points < dataByLevel.Total_Gift_Value)
        return Math.Max(0, level - 1);
      ++level;
    }
    return count;
  }
}
