﻿// Decompiled with JetBrains decompiler
// Type: WonderHallOfFameDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WonderHallOfFameDetail : MonoBehaviour
{
  private List<WonderKingItem> items = new List<WonderKingItem>();
  public WonderKingItem itemPrefab;
  public UIGrid grid;
  public UIScrollView scrollView;
  private StateUIBaseData _data;
  private bool isInit;
  public GameObject empty;
  private int world_id;
  private bool isDestroy;
  private bool needToScroll;

  public void SetData(StateUIBaseData data)
  {
    this._data = data;
    if (this.isInit)
      return;
    this.isInit = true;
    this.LoadData();
  }

  private void LoadData()
  {
    string action = "wonder:loadKingHistory";
    Hashtable postData;
    if (this._data.Type == StateUIBaseData.DataType.Avalon)
    {
      DB.WonderData data = this._data.Data as DB.WonderData;
      postData = Utils.Hash((object) "target_world_id", (object) data.WORLD_ID);
      this.world_id = data.WORLD_ID;
    }
    else
    {
      WonderTowerData data = this._data.Data as WonderTowerData;
      postData = Utils.Hash((object) "target_world_id", (object) data.WORLD_ID);
      this.world_id = data.WORLD_ID;
    }
    RequestManager.inst.SendRequest(action, postData, new System.Action<bool, object>(this.OnLoadDataCallBack), true);
  }

  private void OnDestroy()
  {
    this.isDestroy = true;
  }

  private void Update()
  {
    if (this.isDestroy || !this.needToScroll)
      return;
    this.needToScroll = false;
    this.scrollView.ResetPosition();
  }

  private void CreateFakeData()
  {
    List<WonderKingUIData> wonderKingUiDataList = new List<WonderKingUIData>();
    for (int index = 0; index < 10; ++index)
      wonderKingUiDataList.Add(new WonderKingUIData()
      {
        Portrait = 1,
        Rank = 1 + index,
        UserName = "TestUserName " + (object) (index + 1),
        Time = (long) NetServerTime.inst.ServerTimestamp
      });
    wonderKingUiDataList.Sort(new Comparison<WonderKingUIData>(this.CompareData));
    this.items.Clear();
    for (int index = 0; index < wonderKingUiDataList.Count; ++index)
      this.GetItem().SetData(wonderKingUiDataList[index]);
    this.grid.repositionNow = true;
    this.grid.Reposition();
    this.needToScroll = true;
  }

  private void OnLoadDataCallBack(bool success, object result)
  {
    if (this.isDestroy || !success)
      return;
    Hashtable hashtable = result as Hashtable;
    if (hashtable == null)
    {
      NGUITools.SetActive(this.empty.gameObject, true);
    }
    else
    {
      List<WonderKingUIData> wonderKingUiDataList = new List<WonderKingUIData>();
      IEnumerator enumerator = hashtable.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        string s = enumerator.Current.ToString();
        object source = hashtable[enumerator.Current];
        WonderKingUIData wonderKingUiData = new WonderKingUIData();
        if (wonderKingUiData.Decode(source))
        {
          long result1 = 0;
          long.TryParse(s, out result1);
          wonderKingUiData.Time = result1;
          wonderKingUiDataList.Add(wonderKingUiData);
        }
      }
      wonderKingUiDataList.Sort(new Comparison<WonderKingUIData>(this.CompareData));
      this.items.Clear();
      DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) this.world_id);
      bool flag = false;
      if (wonderData != null)
        flag = wonderData.KING_UID > 0L;
      for (int index = 0; index < wonderKingUiDataList.Count; ++index)
      {
        WonderKingItem wonderKingItem = this.GetItem();
        if (index == 0 && flag)
          wonderKingUiDataList[index].IsCurrentKing = true;
        wonderKingItem.SetData(wonderKingUiDataList[index]);
      }
      this.grid.repositionNow = true;
      this.grid.Reposition();
      this.needToScroll = true;
    }
  }

  private int CompareData(WonderKingUIData a, WonderKingUIData b)
  {
    if (a.Rank > b.Rank)
      return -1;
    return a.Rank < b.Rank ? 1 : 0;
  }

  public void Clear()
  {
    for (int index = 0; index < this.items.Count; ++index)
      this.items[index].Clear();
    this.items.Clear();
  }

  private WonderKingItem GetItem()
  {
    GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.itemPrefab.gameObject);
    gameObject.SetActive(true);
    WonderKingItem component = gameObject.GetComponent<WonderKingItem>();
    this.items.Add(component);
    return component;
  }
}
