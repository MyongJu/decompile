﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Transport.TTransport
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Thrift.Transport
{
  public abstract class TTransport
  {
    public abstract bool IsOpen { get; }

    public bool Peek()
    {
      return this.IsOpen;
    }

    public abstract void Open();

    public abstract void Close();

    public abstract int Read(byte[] buf, int off, int len);

    public int ReadAll(byte[] buf, int off, int len)
    {
      int num1 = 0;
      while (num1 < len)
      {
        int num2 = this.Read(buf, off + num1, len - num1);
        if (num2 <= 0)
          throw new TTransportException("Cannot read, Remote side has closed");
        num1 += num2;
      }
      return num1;
    }

    public abstract void Write(byte[] buf, int off, int len);

    public virtual void Flush()
    {
    }
  }
}
