﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Transport.TTransportException
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

namespace Thrift.Transport
{
  public class TTransportException : Exception
  {
    protected TTransportException.ExceptionType type;

    public TTransportException()
    {
    }

    public TTransportException(TTransportException.ExceptionType type)
      : this()
    {
      this.type = type;
    }

    public TTransportException(TTransportException.ExceptionType type, string message)
      : base(message)
    {
      this.type = type;
    }

    public TTransportException(string message)
      : base(message)
    {
    }

    public TTransportException.ExceptionType Type
    {
      get
      {
        return this.type;
      }
    }

    public enum ExceptionType
    {
      Unknown,
      NotOpen,
      AlreadyOpen,
      TimedOut,
      EndOfFile,
    }
  }
}
