﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Transport.TSocket
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Rpc;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Thrift.Transport
{
  public class TSocket : TStreamTransport
  {
    private int connectionTimeout = 20;
    private TcpClient client;
    private string host;
    private int port;
    private int timeout;

    public TSocket(TcpClient client)
    {
      this.client = client;
      if (!this.IsOpen)
        return;
      this.inputStream = (Stream) client.GetStream();
      this.outputStream = (Stream) client.GetStream();
    }

    public TSocket(string host, int port)
      : this(host, port, 0, 20)
    {
    }

    public TSocket(string host, int port, int timeout, int connTimeout)
    {
      this.host = host;
      this.port = port;
      this.timeout = timeout;
      this.connectionTimeout = connTimeout;
      this.InitSocket();
    }

    private void InitSocket()
    {
      this.client = new TcpClient();
      TcpClient client = this.client;
      int timeout = this.timeout;
      this.client.SendTimeout = timeout;
      int num = timeout;
      client.ReceiveTimeout = num;
    }

    public int Timeout
    {
      set
      {
        TcpClient client = this.client;
        int num1 = this.timeout = value;
        this.client.SendTimeout = num1;
        int num2 = num1;
        client.ReceiveTimeout = num2;
      }
    }

    public TcpClient TcpClient
    {
      get
      {
        return this.client;
      }
    }

    public string Host
    {
      get
      {
        return this.host;
      }
    }

    public int Port
    {
      get
      {
        return this.port;
      }
    }

    public override bool IsOpen
    {
      get
      {
        if (this.client == null)
          return false;
        return this.client.Connected;
      }
    }

    public override void Open()
    {
      if (this.IsOpen)
        throw new TTransportException(TTransportException.ExceptionType.AlreadyOpen, "Socket already connected");
      if (string.IsNullOrEmpty(this.host))
        throw new TTransportException(TTransportException.ExceptionType.NotOpen, "Cannot open null host");
      if (this.port <= 0)
        throw new TTransportException(TTransportException.ExceptionType.NotOpen, "Cannot open without port");
      if (this.client == null)
        this.InitSocket();
      try
      {
        IAsyncResult asyncResult = this.client.BeginConnect(this.host, this.port, (AsyncCallback) null, (object) null);
        if (!asyncResult.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds((double) this.connectionTimeout)))
        {
          this.client.Close();
          this.client = (TcpClient) null;
          throw new Exception("Connection timeout");
        }
        try
        {
          this.client.EndConnect(asyncResult);
          this.inputStream = (Stream) this.client.GetStream();
          this.outputStream = (Stream) this.client.GetStream();
        }
        catch (Exception ex)
        {
          this.client.Close();
          this.client = (TcpClient) null;
          throw new Exception("Connection error");
        }
      }
      catch
      {
        IAsyncResult asyncResult;
        try
        {
          IPHostEntry hostEntry = Dns.GetHostEntry(this.host);
          if (hostEntry.AddressList.Length <= 0)
            throw new Exception("Connection error 140");
          this.client = new TcpClient(AddressFamily.InterNetworkV6);
          string ipv6 = hostEntry.AddressList[0].ToString();
          if (hostEntry.AddressList[0].AddressFamily.ToString() != ProtocolFamily.InterNetworkV6.ToString())
            ipv6 = CommonService.ConvertToIpv6(hostEntry.AddressList[0].ToString());
          asyncResult = this.client.BeginConnect(ipv6, this.port, (AsyncCallback) null, (object) null);
        }
        catch
        {
          throw new Exception("Connection error147");
        }
        if (!asyncResult.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds((double) this.connectionTimeout)))
        {
          this.client.Close();
          this.client = (TcpClient) null;
          throw new Exception("Connection timeout");
        }
        try
        {
          this.client.EndConnect(asyncResult);
          this.inputStream = (Stream) this.client.GetStream();
          this.outputStream = (Stream) this.client.GetStream();
        }
        catch (Exception ex)
        {
          this.client.Close();
          this.client = (TcpClient) null;
          throw new Exception("Connection error");
        }
      }
    }

    public override void Close()
    {
      base.Close();
      if (this.client == null)
        return;
      this.client.Close();
      this.client = (TcpClient) null;
    }
  }
}
