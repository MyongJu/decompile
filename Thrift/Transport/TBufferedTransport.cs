﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Transport.TBufferedTransport
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.IO;

namespace Thrift.Transport
{
  public class TBufferedTransport : TTransport
  {
    private BufferedStream inputBuffer;
    private BufferedStream outputBuffer;
    private int bufSize;
    private TStreamTransport transport;

    public TBufferedTransport(TStreamTransport transport)
      : this(transport, 1024)
    {
    }

    public TBufferedTransport(TStreamTransport transport, int bufSize)
    {
      this.bufSize = bufSize;
      this.transport = transport;
      this.InitBuffers();
    }

    private void InitBuffers()
    {
      if (this.transport.InputStream != null)
        this.inputBuffer = new BufferedStream(this.transport.InputStream, this.bufSize);
      if (this.transport.OutputStream == null)
        return;
      this.outputBuffer = new BufferedStream(this.transport.OutputStream, this.bufSize);
    }

    public TTransport UnderlyingTransport
    {
      get
      {
        return (TTransport) this.transport;
      }
    }

    public override bool IsOpen
    {
      get
      {
        return this.transport.IsOpen;
      }
    }

    public override void Open()
    {
      this.transport.Open();
      this.InitBuffers();
    }

    public override void Close()
    {
      if (this.inputBuffer != null && this.inputBuffer.CanRead)
        this.inputBuffer.Close();
      if (this.outputBuffer == null || !this.outputBuffer.CanWrite)
        return;
      this.outputBuffer.Close();
    }

    public override int Read(byte[] buf, int off, int len)
    {
      return this.inputBuffer.Read(buf, off, len);
    }

    public override void Write(byte[] buf, int off, int len)
    {
      this.outputBuffer.Write(buf, off, len);
    }

    public override void Flush()
    {
      this.outputBuffer.Flush();
    }
  }
}
