﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Transport.TStreamTransport
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.IO;

namespace Thrift.Transport
{
  public class TStreamTransport : TTransport
  {
    protected Stream inputStream;
    protected Stream outputStream;

    public TStreamTransport()
    {
    }

    public TStreamTransport(Stream inputStream, Stream outputStream)
    {
      this.inputStream = inputStream;
      this.outputStream = outputStream;
    }

    public Stream OutputStream
    {
      get
      {
        return this.outputStream;
      }
    }

    public Stream InputStream
    {
      get
      {
        return this.inputStream;
      }
    }

    public override bool IsOpen
    {
      get
      {
        return true;
      }
    }

    public override void Open()
    {
    }

    public override void Close()
    {
      if (this.inputStream != null)
      {
        this.inputStream.Close();
        this.inputStream = (Stream) null;
      }
      if (this.outputStream == null)
        return;
      this.outputStream.Close();
      this.outputStream = (Stream) null;
    }

    public override int Read(byte[] buf, int off, int len)
    {
      if (this.inputStream == null)
        throw new TTransportException(TTransportException.ExceptionType.NotOpen, "Cannot read from null inputstream");
      return this.inputStream.Read(buf, off, len);
    }

    public override void Write(byte[] buf, int off, int len)
    {
      if (this.outputStream == null)
        throw new TTransportException(TTransportException.ExceptionType.NotOpen, "Cannot write to null outputstream");
      this.outputStream.Write(buf, off, len);
    }

    public override void Flush()
    {
      if (this.outputStream == null)
        throw new TTransportException(TTransportException.ExceptionType.NotOpen, "Cannot flush null outputstream");
      this.outputStream.Flush();
    }
  }
}
