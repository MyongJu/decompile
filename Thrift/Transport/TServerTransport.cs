﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Transport.TServerTransport
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Thrift.Transport
{
  public abstract class TServerTransport
  {
    public abstract void Listen();

    public abstract void Close();

    protected abstract TTransport AcceptImpl();

    public TTransport Accept()
    {
      TTransport ttransport = this.AcceptImpl();
      if (ttransport == null)
        throw new TTransportException("accept() may not return NULL");
      return ttransport;
    }
  }
}
