﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Transport.TServerSocket
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Net;
using System.Net.Sockets;

namespace Thrift.Transport
{
  public class TServerSocket : TServerTransport
  {
    private TcpListener server;
    private int port;
    private int clientTimeout;
    private bool useBufferedSockets;

    public TServerSocket(TcpListener listener)
      : this(listener, 0)
    {
    }

    public TServerSocket(TcpListener listener, int clientTimeout)
    {
      this.server = listener;
      this.clientTimeout = clientTimeout;
    }

    public TServerSocket(int port)
      : this(port, 0)
    {
    }

    public TServerSocket(int port, int clientTimeout)
      : this(port, clientTimeout, false)
    {
    }

    public TServerSocket(int port, int clientTimeout, bool useBufferedSockets)
    {
      this.port = port;
      this.clientTimeout = clientTimeout;
      this.useBufferedSockets = useBufferedSockets;
      try
      {
        this.server = new TcpListener(IPAddress.Any, this.port);
      }
      catch (Exception ex)
      {
        this.server = (TcpListener) null;
        throw new TTransportException("Could not create ServerSocket on port " + (object) port + ".");
      }
    }

    public override void Listen()
    {
      if (this.server == null)
        return;
      try
      {
        this.server.Start();
      }
      catch (SocketException ex)
      {
        throw new TTransportException("Could not accept on listening socket: " + ex.Message);
      }
    }

    protected override TTransport AcceptImpl()
    {
      if (this.server == null)
        throw new TTransportException(TTransportException.ExceptionType.NotOpen, "No underlying server socket.");
      try
      {
        TSocket tsocket = new TSocket(this.server.AcceptTcpClient());
        tsocket.Timeout = this.clientTimeout;
        if (this.useBufferedSockets)
          return (TTransport) new TBufferedTransport((TStreamTransport) tsocket);
        return (TTransport) tsocket;
      }
      catch (Exception ex)
      {
        throw new TTransportException(ex.ToString());
      }
    }

    public override void Close()
    {
      if (this.server == null)
        return;
      try
      {
        this.server.Stop();
      }
      catch (Exception ex)
      {
        throw new TTransportException("WARNING: Could not close server socket: " + (object) ex);
      }
      this.server = (TcpListener) null;
    }
  }
}
