﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Protocol.TField
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Thrift.Protocol
{
  public struct TField
  {
    private string name;
    private TType type;
    private short id;

    public TField(string name, TType type, short id)
    {
      this.name = name;
      this.type = type;
      this.id = id;
    }

    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    public TType Type
    {
      get
      {
        return this.type;
      }
      set
      {
        this.type = value;
      }
    }

    public short ID
    {
      get
      {
        return this.id;
      }
      set
      {
        this.id = value;
      }
    }
  }
}
