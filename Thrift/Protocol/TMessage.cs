﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Protocol.TMessage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Thrift.Protocol
{
  public struct TMessage
  {
    private string name;
    private TMessageType type;
    private int seqID;

    public TMessage(string name, TMessageType type, int seqid)
    {
      this.name = name;
      this.type = type;
      this.seqID = seqid;
    }

    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    public TMessageType Type
    {
      get
      {
        return this.type;
      }
      set
      {
        this.type = value;
      }
    }

    public int SeqID
    {
      get
      {
        return this.seqID;
      }
      set
      {
        this.seqID = value;
      }
    }
  }
}
