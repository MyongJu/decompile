﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Protocol.TProtocolException
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

namespace Thrift.Protocol
{
  internal class TProtocolException : Exception
  {
    public const int UNKNOWN = 0;
    public const int INVALID_DATA = 1;
    public const int NEGATIVE_SIZE = 2;
    public const int SIZE_LIMIT = 3;
    public const int BAD_VERSION = 4;
    public const int NOT_IMPLEMENTED = 5;
    public const int MISSING_REQUIRED_FIELD = 6;
    protected int type_;

    public TProtocolException()
    {
    }

    public TProtocolException(int type)
    {
      this.type_ = type;
    }

    public TProtocolException(int type, string message)
      : base(message)
    {
      this.type_ = type;
    }

    public TProtocolException(string message)
      : base(message)
    {
    }

    public int getType()
    {
      return this.type_;
    }
  }
}
