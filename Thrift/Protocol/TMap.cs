﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Protocol.TMap
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Thrift.Protocol
{
  public struct TMap
  {
    private TType keyType;
    private TType valueType;
    private int count;

    public TMap(TType keyType, TType valueType, int count)
    {
      this.keyType = keyType;
      this.valueType = valueType;
      this.count = count;
    }

    public TType KeyType
    {
      get
      {
        return this.keyType;
      }
      set
      {
        this.keyType = value;
      }
    }

    public TType ValueType
    {
      get
      {
        return this.valueType;
      }
      set
      {
        this.valueType = value;
      }
    }

    public int Count
    {
      get
      {
        return this.count;
      }
      set
      {
        this.count = value;
      }
    }
  }
}
