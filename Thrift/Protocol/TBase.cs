﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Protocol.TBase
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Thrift.Protocol
{
  public interface TBase
  {
    void Read(TProtocol tProtocol);

    void Write(TProtocol tProtocol);
  }
}
