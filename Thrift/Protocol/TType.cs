﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Protocol.TType
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Thrift.Protocol
{
  public enum TType : byte
  {
    Stop = 0,
    Void = 1,
    Bool = 2,
    Byte = 3,
    Double = 4,
    I16 = 6,
    I32 = 8,
    I64 = 10, // 0x0A
    String = 11, // 0x0B
    Struct = 12, // 0x0C
    Map = 13, // 0x0D
    Set = 14, // 0x0E
    List = 15, // 0x0F
  }
}
