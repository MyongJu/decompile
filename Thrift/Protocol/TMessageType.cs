﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Protocol.TMessageType
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Thrift.Protocol
{
  public enum TMessageType
  {
    Call = 1,
    Reply = 2,
    Exception = 3,
    Oneway = 4,
  }
}
