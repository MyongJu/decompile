﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Protocol.TList
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Thrift.Protocol
{
  public struct TList
  {
    private TType elementType;
    private int count;

    public TList(TType elementType, int count)
    {
      this.elementType = elementType;
      this.count = count;
    }

    public TType ElementType
    {
      get
      {
        return this.elementType;
      }
      set
      {
        this.elementType = value;
      }
    }

    public int Count
    {
      get
      {
        return this.count;
      }
      set
      {
        this.count = value;
      }
    }
  }
}
