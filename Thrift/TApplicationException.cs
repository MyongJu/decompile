﻿// Decompiled with JetBrains decompiler
// Type: Thrift.TApplicationException
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using Thrift.Protocol;

namespace Thrift
{
  public class TApplicationException : Exception
  {
    public TApplicationException.ExceptionType type;

    public TApplicationException()
    {
    }

    public TApplicationException(TApplicationException.ExceptionType type)
    {
      this.type = type;
    }

    public TApplicationException(TApplicationException.ExceptionType type, string message)
      : base(message)
    {
      this.type = type;
    }

    public static TApplicationException Read(TProtocol iprot)
    {
      string message = (string) null;
      TApplicationException.ExceptionType type = TApplicationException.ExceptionType.Unknown;
      while (true)
      {
        TField tfield = iprot.ReadFieldBegin();
        if (tfield.Type != TType.Stop)
        {
          switch (tfield.ID)
          {
            case 1:
              if (tfield.Type == TType.String)
              {
                message = iprot.ReadString();
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 2:
              if (tfield.Type == TType.I32)
              {
                type = (TApplicationException.ExceptionType) iprot.ReadI32();
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            default:
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
          }
          iprot.ReadFieldEnd();
        }
        else
          break;
      }
      iprot.ReadStructEnd();
      return new TApplicationException(type, message);
    }

    public void Write(TProtocol oprot)
    {
      TStruct struc = new TStruct(nameof (TApplicationException));
      TField field = new TField();
      oprot.WriteStructBegin(struc);
      if (!string.IsNullOrEmpty(this.Message))
      {
        field.Name = "message";
        field.Type = TType.String;
        field.ID = (short) 1;
        oprot.WriteFieldBegin(field);
        oprot.WriteString(this.Message);
        oprot.WriteFieldEnd();
      }
      field.Name = "type";
      field.Type = TType.I32;
      field.ID = (short) 2;
      oprot.WriteFieldBegin(field);
      oprot.WriteI32((int) this.type);
      oprot.WriteFieldEnd();
      oprot.WriteFieldStop();
      oprot.WriteStructEnd();
    }

    public enum ExceptionType
    {
      Unknown,
      UnknownMethod,
      InvalidMessageType,
      WrongMethodName,
      BadSequenceID,
      MissingResult,
      InternalError,
      ProtocolError,
      InvalidTransform,
      InvalidProtocol,
      UnsupportedClientType,
      LOADSHEDDING,
      TIMEOUT,
      INJECTED_FAILURE,
    }
  }
}
