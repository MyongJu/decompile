﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Server.TThreadPoolServer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Threading;
using Thrift.Protocol;
using Thrift.Transport;

namespace Thrift.Server
{
  public class TThreadPoolServer : TServer
  {
    private const int DEFAULT_MIN_THREADS = 10;
    private const int DEFAULT_MAX_THREADS = 100;
    private volatile bool stop;

    public TThreadPoolServer(TProcessor processor, TServerTransport serverTransport)
      : this(processor, serverTransport, new TTransportFactory(), new TTransportFactory(), (TProtocolFactory) new TBinaryProtocol.Factory(), (TProtocolFactory) new TBinaryProtocol.Factory(), 10, 100, new TServer.LogDelegate(TServer.DefaultLogDelegate))
    {
    }

    public TThreadPoolServer(TProcessor processor, TServerTransport serverTransport, TServer.LogDelegate logDelegate)
      : this(processor, serverTransport, new TTransportFactory(), new TTransportFactory(), (TProtocolFactory) new TBinaryProtocol.Factory(), (TProtocolFactory) new TBinaryProtocol.Factory(), 10, 100, logDelegate)
    {
    }

    public TThreadPoolServer(TProcessor processor, TServerTransport serverTransport, TTransportFactory transportFactory, TProtocolFactory protocolFactory)
      : this(processor, serverTransport, transportFactory, transportFactory, protocolFactory, protocolFactory, 10, 100, new TServer.LogDelegate(TServer.DefaultLogDelegate))
    {
    }

    public TThreadPoolServer(TProcessor processor, TServerTransport serverTransport, TTransportFactory inputTransportFactory, TTransportFactory outputTransportFactory, TProtocolFactory inputProtocolFactory, TProtocolFactory outputProtocolFactory, int minThreadPoolThreads, int maxThreadPoolThreads, TServer.LogDelegate logDel)
      : base(processor, serverTransport, inputTransportFactory, outputTransportFactory, inputProtocolFactory, outputProtocolFactory, logDel)
    {
      if (!ThreadPool.SetMinThreads(minThreadPoolThreads, minThreadPoolThreads))
        throw new Exception("Error: could not SetMinThreads in ThreadPool");
      if (!ThreadPool.SetMaxThreads(maxThreadPoolThreads, maxThreadPoolThreads))
        throw new Exception("Error: could not SetMaxThreads in ThreadPool");
    }

    public override void Serve()
    {
      try
      {
        this.serverTransport.Listen();
      }
      catch (TTransportException ex)
      {
        this.logDelegate("Error, could not listen on ServerTransport: " + (object) ex);
        return;
      }
      while (!this.stop)
      {
        int num1 = 0;
        try
        {
          ThreadPool.QueueUserWorkItem(new WaitCallback(this.Execute), (object) this.serverTransport.Accept());
        }
        catch (TTransportException ex)
        {
          if (this.stop)
          {
            this.logDelegate("TThreadPoolServer was shutting down, caught " + ex.GetType().Name);
          }
          else
          {
            int num2 = num1 + 1;
            this.logDelegate(ex.ToString());
          }
        }
      }
      if (!this.stop)
        return;
      try
      {
        this.serverTransport.Close();
      }
      catch (TTransportException ex)
      {
        this.logDelegate("TServerTransport failed on close: " + ex.Message);
      }
      this.stop = false;
    }

    private void Execute(object threadContext)
    {
      TTransport trans1 = (TTransport) threadContext;
      TTransport trans2 = (TTransport) null;
      TTransport trans3 = (TTransport) null;
      try
      {
        trans2 = this.inputTransportFactory.GetTransport(trans1);
        trans3 = this.outputTransportFactory.GetTransport(trans1);
        TProtocol protocol1 = this.inputProtocolFactory.GetProtocol(trans2);
        TProtocol protocol2 = this.outputProtocolFactory.GetProtocol(trans3);
        while (this.processor.Process(protocol1, protocol2))
          ;
      }
      catch (TTransportException ex)
      {
      }
      catch (Exception ex)
      {
        this.logDelegate("Error: " + (object) ex);
      }
      if (trans2 != null)
        trans2.Close();
      if (trans3 == null)
        return;
      trans3.Close();
    }

    public override void Stop()
    {
      this.stop = true;
      this.serverTransport.Close();
    }
  }
}
