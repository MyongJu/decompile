﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Server.TServer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using Thrift.Protocol;
using Thrift.Transport;

namespace Thrift.Server
{
  public abstract class TServer
  {
    protected TProcessor processor;
    protected TServerTransport serverTransport;
    protected TTransportFactory inputTransportFactory;
    protected TTransportFactory outputTransportFactory;
    protected TProtocolFactory inputProtocolFactory;
    protected TProtocolFactory outputProtocolFactory;
    protected TServer.LogDelegate logDelegate;

    public TServer(TProcessor processor, TServerTransport serverTransport)
      : this(processor, serverTransport, new TTransportFactory(), new TTransportFactory(), (TProtocolFactory) new TBinaryProtocol.Factory(), (TProtocolFactory) new TBinaryProtocol.Factory(), new TServer.LogDelegate(TServer.DefaultLogDelegate))
    {
    }

    public TServer(TProcessor processor, TServerTransport serverTransport, TServer.LogDelegate logDelegate)
      : this(processor, serverTransport, new TTransportFactory(), new TTransportFactory(), (TProtocolFactory) new TBinaryProtocol.Factory(), (TProtocolFactory) new TBinaryProtocol.Factory(), new TServer.LogDelegate(TServer.DefaultLogDelegate))
    {
    }

    public TServer(TProcessor processor, TServerTransport serverTransport, TTransportFactory transportFactory)
      : this(processor, serverTransport, transportFactory, transportFactory, (TProtocolFactory) new TBinaryProtocol.Factory(), (TProtocolFactory) new TBinaryProtocol.Factory(), new TServer.LogDelegate(TServer.DefaultLogDelegate))
    {
    }

    public TServer(TProcessor processor, TServerTransport serverTransport, TTransportFactory transportFactory, TProtocolFactory protocolFactory)
      : this(processor, serverTransport, transportFactory, transportFactory, protocolFactory, protocolFactory, new TServer.LogDelegate(TServer.DefaultLogDelegate))
    {
    }

    public TServer(TProcessor processor, TServerTransport serverTransport, TTransportFactory inputTransportFactory, TTransportFactory outputTransportFactory, TProtocolFactory inputProtocolFactory, TProtocolFactory outputProtocolFactory, TServer.LogDelegate logDelegate)
    {
      this.processor = processor;
      this.serverTransport = serverTransport;
      this.inputTransportFactory = inputTransportFactory;
      this.outputTransportFactory = outputTransportFactory;
      this.inputProtocolFactory = inputProtocolFactory;
      this.outputProtocolFactory = outputProtocolFactory;
      this.logDelegate = logDelegate;
    }

    public abstract void Serve();

    public abstract void Stop();

    protected static void DefaultLogDelegate(string s)
    {
      Console.Error.WriteLine(s);
    }

    public delegate void LogDelegate(string str);
  }
}
