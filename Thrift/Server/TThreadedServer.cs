﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Server.TThreadedServer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Threading;
using Thrift.Collections;
using Thrift.Protocol;
using Thrift.Transport;

namespace Thrift.Server
{
  public class TThreadedServer : TServer
  {
    private const int DEFAULT_MAX_THREADS = 100;
    private volatile bool stop;
    private readonly int maxThreads;
    private Queue<TTransport> clientQueue;
    private THashSet<Thread> clientThreads;
    private object clientLock;
    private Thread workerThread;

    public TThreadedServer(TProcessor processor, TServerTransport serverTransport)
      : this(processor, serverTransport, new TTransportFactory(), new TTransportFactory(), (TProtocolFactory) new TBinaryProtocol.Factory(), (TProtocolFactory) new TBinaryProtocol.Factory(), 100, new TServer.LogDelegate(TServer.DefaultLogDelegate))
    {
    }

    public TThreadedServer(TProcessor processor, TServerTransport serverTransport, TServer.LogDelegate logDelegate)
      : this(processor, serverTransport, new TTransportFactory(), new TTransportFactory(), (TProtocolFactory) new TBinaryProtocol.Factory(), (TProtocolFactory) new TBinaryProtocol.Factory(), 100, logDelegate)
    {
    }

    public TThreadedServer(TProcessor processor, TServerTransport serverTransport, TTransportFactory transportFactory, TProtocolFactory protocolFactory)
      : this(processor, serverTransport, transportFactory, transportFactory, protocolFactory, protocolFactory, 100, new TServer.LogDelegate(TServer.DefaultLogDelegate))
    {
    }

    public TThreadedServer(TProcessor processor, TServerTransport serverTransport, TTransportFactory inputTransportFactory, TTransportFactory outputTransportFactory, TProtocolFactory inputProtocolFactory, TProtocolFactory outputProtocolFactory, int maxThreads, TServer.LogDelegate logDel)
      : base(processor, serverTransport, inputTransportFactory, outputTransportFactory, inputProtocolFactory, outputProtocolFactory, logDel)
    {
      this.maxThreads = maxThreads;
      this.clientQueue = new Queue<TTransport>();
      this.clientLock = new object();
      this.clientThreads = new THashSet<Thread>();
    }

    public override void Serve()
    {
      try
      {
        this.workerThread = new Thread(new ThreadStart(this.Execute));
        this.workerThread.Start();
        this.serverTransport.Listen();
      }
      catch (TTransportException ex)
      {
        this.logDelegate("Error, could not listen on ServerTransport: " + (object) ex);
        return;
      }
      while (!this.stop)
      {
        int num1 = 0;
        try
        {
          TTransport ttransport = this.serverTransport.Accept();
          lock (this.clientLock)
          {
            this.clientQueue.Enqueue(ttransport);
            Monitor.Pulse(this.clientLock);
          }
        }
        catch (TTransportException ex)
        {
          if (this.stop)
          {
            this.logDelegate("TThreadPoolServer was shutting down, caught " + (object) ex);
          }
          else
          {
            int num2 = num1 + 1;
            this.logDelegate(ex.ToString());
          }
        }
      }
      if (!this.stop)
        return;
      try
      {
        this.serverTransport.Close();
      }
      catch (TTransportException ex)
      {
        this.logDelegate("TServeTransport failed on close: " + ex.Message);
      }
      this.stop = false;
    }

    private void Execute()
    {
      while (!this.stop)
      {
        TTransport ttransport;
        Thread thread;
        lock (this.clientLock)
        {
          while (this.clientThreads.Count >= this.maxThreads)
            Monitor.Wait(this.clientLock);
          while (this.clientQueue.Count == 0)
            Monitor.Wait(this.clientLock);
          ttransport = this.clientQueue.Dequeue();
          thread = new Thread(new ParameterizedThreadStart(this.ClientWorker));
          this.clientThreads.Add(thread);
        }
        thread.Start((object) ttransport);
      }
    }

    private void ClientWorker(object context)
    {
      TTransport trans1 = (TTransport) context;
      TTransport trans2 = (TTransport) null;
      TTransport trans3 = (TTransport) null;
      try
      {
        trans2 = this.inputTransportFactory.GetTransport(trans1);
        trans3 = this.outputTransportFactory.GetTransport(trans1);
        TProtocol protocol1 = this.inputProtocolFactory.GetProtocol(trans2);
        TProtocol protocol2 = this.outputProtocolFactory.GetProtocol(trans3);
        while (this.processor.Process(protocol1, protocol2))
          ;
      }
      catch (TTransportException ex)
      {
      }
      catch (Exception ex)
      {
        this.logDelegate("Error: " + (object) ex);
      }
      if (trans2 != null)
        trans2.Close();
      if (trans3 != null)
        trans3.Close();
      lock (this.clientLock)
      {
        this.clientThreads.Remove(Thread.CurrentThread);
        Monitor.Pulse(this.clientLock);
      }
    }

    public override void Stop()
    {
      this.stop = true;
      this.serverTransport.Close();
      this.workerThread.Abort();
      foreach (Thread clientThread in this.clientThreads)
        clientThread.Abort();
    }
  }
}
