﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Server.TSimpleServer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using Thrift.Protocol;
using Thrift.Transport;

namespace Thrift.Server
{
  public class TSimpleServer : TServer
  {
    private bool stop;

    public TSimpleServer(TProcessor processor, TServerTransport serverTransport)
      : base(processor, serverTransport, new TTransportFactory(), new TTransportFactory(), (TProtocolFactory) new TBinaryProtocol.Factory(), (TProtocolFactory) new TBinaryProtocol.Factory(), new TServer.LogDelegate(TServer.DefaultLogDelegate))
    {
    }

    public TSimpleServer(TProcessor processor, TServerTransport serverTransport, TServer.LogDelegate logDel)
      : base(processor, serverTransport, new TTransportFactory(), new TTransportFactory(), (TProtocolFactory) new TBinaryProtocol.Factory(), (TProtocolFactory) new TBinaryProtocol.Factory(), logDel)
    {
    }

    public TSimpleServer(TProcessor processor, TServerTransport serverTransport, TTransportFactory transportFactory)
      : base(processor, serverTransport, transportFactory, transportFactory, (TProtocolFactory) new TBinaryProtocol.Factory(), (TProtocolFactory) new TBinaryProtocol.Factory(), new TServer.LogDelegate(TServer.DefaultLogDelegate))
    {
    }

    public TSimpleServer(TProcessor processor, TServerTransport serverTransport, TTransportFactory transportFactory, TProtocolFactory protocolFactory)
      : base(processor, serverTransport, transportFactory, transportFactory, protocolFactory, protocolFactory, new TServer.LogDelegate(TServer.DefaultLogDelegate))
    {
    }

    public override void Serve()
    {
      try
      {
        this.serverTransport.Listen();
      }
      catch (TTransportException ex)
      {
        this.logDelegate(ex.ToString());
        return;
      }
      while (!this.stop)
      {
        TTransport trans1 = (TTransport) null;
        TTransport trans2 = (TTransport) null;
        try
        {
          TTransport trans3 = this.serverTransport.Accept();
          if (trans3 != null)
          {
            trans1 = this.inputTransportFactory.GetTransport(trans3);
            trans2 = this.outputTransportFactory.GetTransport(trans3);
            TProtocol protocol1 = this.inputProtocolFactory.GetProtocol(trans1);
            TProtocol protocol2 = this.outputProtocolFactory.GetProtocol(trans2);
            while (this.processor.Process(protocol1, protocol2))
              ;
          }
        }
        catch (TTransportException ex)
        {
          if (this.stop)
            this.logDelegate("TSimpleServer was shutting down, caught " + ex.GetType().Name);
        }
        catch (Exception ex)
        {
          this.logDelegate(ex.ToString());
        }
        if (trans1 != null)
          trans1.Close();
        if (trans2 != null)
          trans2.Close();
      }
      if (!this.stop)
        return;
      try
      {
        this.serverTransport.Close();
      }
      catch (TTransportException ex)
      {
        this.logDelegate("TServerTranport failed on close: " + ex.Message);
      }
      this.stop = false;
    }

    public override void Stop()
    {
      this.stop = true;
      this.serverTransport.Close();
    }
  }
}
