﻿// Decompiled with JetBrains decompiler
// Type: Thrift.Collections.THashSet`1
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace Thrift.Collections
{
  public class THashSet<T> : IEnumerable, IEnumerable<T>, ICollection<T>
  {
    private HashSet<T> set = new HashSet<T>();

    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
      return ((IEnumerable<T>) this.set).GetEnumerator();
    }

    public int Count
    {
      get
      {
        return this.set.Count;
      }
    }

    public bool IsReadOnly
    {
      get
      {
        return false;
      }
    }

    public void Add(T item)
    {
      this.set.Add(item);
    }

    public void Clear()
    {
      this.set.Clear();
    }

    public bool Contains(T item)
    {
      return this.set.Contains(item);
    }

    public void CopyTo(T[] array, int arrayIndex)
    {
      this.set.CopyTo(array, arrayIndex);
    }

    public IEnumerator GetEnumerator()
    {
      return (IEnumerator) this.set.GetEnumerator();
    }

    public bool Remove(T item)
    {
      return this.set.Remove(item);
    }
  }
}
