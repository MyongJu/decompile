﻿// Decompiled with JetBrains decompiler
// Type: TalentMiniDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TalentMiniDetailDlg : MonoBehaviour
{
  private TalentMiniDetailDlg.Data _data = new TalentMiniDetailDlg.Data();
  [SerializeField]
  private TalentMiniDetailDlg.Panel panel;

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void FreshPanel()
  {
    if (this._data.skillActiveId == 0)
      return;
    DB.HeroData heroData = PlayerData.inst.heroData;
    if (heroData == null)
      return;
    HeroSkills.Skill skill = heroData.skills.GetSkill((long) this._data.skillInfo.skillId);
    TalentData talentData = DBManager.inst.DB_Talent.Get(this._data.skillInfo.skillId);
    PlayerTalentInfo playerTalentInfo = ConfigManager.inst.DB_PlayerTalent.GetPlayerTalentInfo(this._data.skillInfo.skillId);
    if (playerTalentInfo != null)
    {
      this.panel.title.text = playerTalentInfo.Loc_name;
      Dictionary<string, string> para = new Dictionary<string, string>();
      para.Add("active_time", playerTalentInfo.ActiveTimeInMinutes.ToString());
      para.Add("active_value", playerTalentInfo.activeValue);
      para.Add("cd_time", playerTalentInfo.CdTimeInHour.ToString());
      if (playerTalentInfo.benefit != null && playerTalentInfo.benefit.benefits != null && playerTalentInfo.benefit.benefits.Count > 0)
      {
        Dictionary<string, float>.ValueCollection.Enumerator enumerator = playerTalentInfo.benefit.benefits.Values.GetEnumerator();
        if (enumerator.MoveNext())
          para.Add("benefit_value_1", string.Format("{0}%", (object) (float) ((double) (int) ((double) Mathf.Abs(enumerator.Current) * 1000.0) / 10.0)));
      }
      this.panel.skilLDes.text = ScriptLocalization.GetWithPara(playerTalentInfo.description, para, true);
    }
    this.panel.BT_activeSkill.isEnabled = true;
    this.panel.activeSkillText.text = talentData == null || !talentData.InCurrentGroup(PlayerData.inst.heroData.skillGroupId) ? Utils.XLAT("talent_active_skill_go_to_button") : Utils.XLAT("talent_active_skill_activate_button");
    if (skill != null && talentData != null && talentData.InCurrentGroup(PlayerData.inst.heroData.skillGroupId))
    {
      this.panel.lockIcon.gameObject.SetActive(false);
      this.panel.noSkillWarning.gameObject.SetActive(false);
      this.panel.BT_activeSkill.isEnabled = talentData.InCurrentGroup(PlayerData.inst.heroData.skillGroupId);
      if (skill != null)
      {
        if (skill.jobId != 0L)
        {
          this.panel.skillActive.gameObject.SetActive(true);
          this.panel.skillCoolDown.gameObject.SetActive(false);
          this.panel.skillActive.text = Utils.FormatTime(JobManager.Instance.GetJob(skill.jobId).LeftTime(), false, false, true);
          this.panel.BT_activeSkill.isEnabled = false;
        }
        else
        {
          this.panel.skillActive.gameObject.SetActive(false);
          if (skill.activeTime > NetServerTime.inst.ServerTimestamp)
          {
            this.panel.skillCoolDown.gameObject.SetActive(true);
            this.panel.skillCoolDown.text = Utils.FormatTime(skill.activeTime - NetServerTime.inst.ServerTimestamp, false, false, true);
            this.panel.BT_activeSkill.isEnabled = false;
          }
          else
            this.panel.skillCoolDown.gameObject.SetActive(false);
        }
      }
      else
      {
        this.panel.noSkillWarning.gameObject.SetActive(true);
        this.panel.skillActive.gameObject.SetActive(false);
        this.panel.skillCoolDown.gameObject.SetActive(false);
      }
      GreyUtility.Normal(this.panel.icon.gameObject);
    }
    else
    {
      this.panel.lockIcon.gameObject.SetActive(true);
      this.panel.noSkillWarning.gameObject.SetActive(true);
      this.panel.skillActive.gameObject.SetActive(false);
      this.panel.skillCoolDown.gameObject.SetActive(false);
      GreyUtility.Grey(this.panel.icon.gameObject);
    }
  }

  public void Open(int skillActiveId)
  {
    this.gameObject.SetActive(true);
    this._data.skillActiveId = skillActiveId;
    PlayerTalentInfo playerTalentInfo = ConfigManager.inst.DB_PlayerTalent.GetPlayerTalentInfo(this._data.skillInfo.skillId);
    if (playerTalentInfo != null)
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.icon, playerTalentInfo.imagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.FreshPanel();
  }

  public void Close()
  {
    BuilderFactory.Instance.Release((UIWidget) this.panel.icon);
    this.gameObject.SetActive(false);
  }

  public void OnSecond(int serverTime)
  {
    this.FreshPanel();
  }

  public void OnActiveButtonClick()
  {
    HeroSkills.Skill skill = PlayerData.inst.heroData.skills.GetSkill((long) this._data.skillInfo.skillId);
    TalentData talentData = DBManager.inst.DB_Talent.Get(this._data.skillInfo.skillId);
    if (skill != null && talentData != null && talentData.InCurrentGroup(PlayerData.inst.heroData.skillGroupId))
    {
      RequestManager.inst.SendRequest("Hero:playSkill", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "skill_id", (object) this._data.skillInfo.skillId), (System.Action<bool, object>) ((ret, obj) =>
      {
        TalentMiniDetailDlg.ResData resData = JsonReader.Deserialize<TalentMiniDetailDlg.ResData>(Utils.Object2Json(obj));
        if (resData == null || resData.skill_data == null)
          return;
        RewardsCollectionAnimator.Instance.Clear();
        using (Dictionary<string, int>.Enumerator enumerator = resData.skill_data.GetEnumerator())
        {
          while (enumerator.MoveNext())
            RewardsCollectionAnimator.Instance.Ress.Add(RewardsCollectionAnimator.Instance.GetRRID(enumerator.Current));
        }
        RewardsCollectionAnimator.Instance.CollectResource(true);
      }), true);
    }
    else
    {
      TalentTreeInfo talentTreeInfo = ConfigManager.inst.DB_TalentTree.GetTalentTreeInfo(ConfigManager.inst.DB_PlayerTalent.GetPlayerTalentInfo(this._data.skillInfo.skillId).talentTreeInternalId);
      UIManager.inst.OpenDlg("Talent/TalentTreeDlg", (UI.Dialog.DialogParameter) new TalentTreeDlg.Parameter()
      {
        index = (talentTreeInfo.tree - 1)
      }, 1 != 0, 1 != 0, 1 != 0);
      UIManager.inst.publicHUD.talentMiniDlg.Hide();
    }
  }

  public void OnCloseButtonClick()
  {
    this.Close();
  }

  public void Reset()
  {
    this.panel.title = this.transform.Find("Dialog_Small_S/Title_Label").gameObject.GetComponent<UILabel>();
    this.panel.icon = this.transform.Find("icon/SkillTexture").gameObject.GetComponent<UITexture>();
    this.panel.lockIcon = this.transform.Find("icon/LockIcon").gameObject.transform;
    this.panel.noSkillWarning = this.transform.Find("NoSkillWarning").gameObject.GetComponent<UILabel>();
    this.panel.skillActive = this.transform.Find("SkillActiving").gameObject.GetComponent<UILabel>();
    this.panel.skillCoolDown = this.transform.Find("SkillCoolDown").gameObject.GetComponent<UILabel>();
    this.panel.skilLDes = this.transform.Find("SkillDiscription").gameObject.GetComponent<UILabel>();
    this.panel.BT_close = this.transform.Find("Btn_Close").gameObject.GetComponent<UIButton>();
    this.panel.BT_activeSkill = this.transform.Find("Btn_Active").gameObject.GetComponent<UIButton>();
    this.panel.activeSkillText = this.transform.Find("Btn_Active/Label").gameObject.GetComponent<UILabel>();
  }

  public class ResData
  {
    public Dictionary<string, int> skill_data;
  }

  protected class Data
  {
    private int _skillInteralId;

    public int skillActiveId
    {
      get
      {
        return this._skillInteralId;
      }
      set
      {
        this._skillInteralId = value;
        this.skillInfo = ConfigManager.inst.DB_HeroTalentActive.GetItem(this._skillInteralId);
      }
    }

    public HeroTalentActiveInfo skillInfo { get; private set; }
  }

  [Serializable]
  protected class Panel
  {
    public UITexture icon;
    public Transform lockIcon;
    public UILabel title;
    public UILabel noSkillWarning;
    public UILabel skillActive;
    public UILabel skillCoolDown;
    public UILabel skilLDes;
    public UILabel activeSkillText;
    public UIButton BT_close;
    public UIButton BT_activeSkill;
  }
}
