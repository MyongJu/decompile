﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_City_Rally_Defense
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class AllianceWar_MainDlg_City_Rally_Defense : AllianceWar_MainDlg_Rally_Slot
{
  public override void Setup(long rallyId)
  {
    if (this.IsDestroy)
      return;
    RallyData rallyData = DBManager.inst.DB_Rally.Get(rallyId);
    AllianceWarManager.WarData data = PlayerData.inst.allianceWarManager.GetData(rallyId);
    if (rallyData == null || data == null)
      return;
    this.data.rallyId = rallyId;
    this.SetupAboveList(rallyData, rallyData.targetUid, rallyData.targetAllianceId, data.defenseCount);
    this.SetupBelowList(rallyData, rallyData.ownerUid, rallyData.ownerAllianceId, data.rallyCount);
    this.SetupTargetSlot(rallyData);
    this.RefreshTime();
  }

  protected override void SetupAboveList(RallyData rallyData, long uid, long allianceId, int memberCount)
  {
    UserData userData = DBManager.inst.DB_User.Get(uid);
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(allianceId);
    AllianceWar_MainDlg_RallyList.param.ownerPortrait = 0;
    if (userData != null)
    {
      AllianceWar_MainDlg_RallyList.param.ownerName = allianceData == null ? userData.userName : string.Format("[{0}]{1}", (object) allianceData.allianceAcronym, (object) userData.userName);
      AllianceWar_MainDlg_RallyList.param.ownerPortrait = userData.portrait;
      AllianceWar_MainDlg_RallyList.param.ownerCustomIconUrl = userData.Icon;
      AllianceWar_MainDlg_RallyList.param.ownerLordTitleId = userData.LordTitle;
    }
    AllianceWar_MainDlg_RallyList.param.memberCount = memberCount;
    AllianceWar_MainDlg_RallyList.param.canJoin = true;
    AllianceWar_MainDlg_RallyList.param.isAttack = false;
    AllianceWar_MainDlg_RallyList.param.rallyType = AllianceWar_MainDlg_RallyList.Params.RallyListType.MEMBER;
    this.panel.aboveList.Setup(AllianceWar_MainDlg_RallyList.param);
  }

  protected override void SetupBelowList(RallyData rallyData, long uid, long allianceId, int memberCount)
  {
    UserData userData = DBManager.inst.DB_User.Get(uid);
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(allianceId);
    AllianceWar_MainDlg_RallyList.param.ownerPortrait = 0;
    if (userData != null)
    {
      AllianceWar_MainDlg_RallyList.param.ownerName = allianceData == null ? userData.userName : string.Format("[{0}]{1}", (object) allianceData.allianceAcronym, (object) userData.userName);
      AllianceWar_MainDlg_RallyList.param.ownerPortrait = userData.portrait;
      AllianceWar_MainDlg_RallyList.param.ownerCustomIconUrl = userData.Icon;
      AllianceWar_MainDlg_RallyList.param.ownerLordTitleId = userData.LordTitle;
    }
    AllianceWar_MainDlg_RallyList.param.memberCount = memberCount;
    AllianceWar_MainDlg_RallyList.param.canJoin = false;
    AllianceWar_MainDlg_RallyList.param.isAttack = true;
    AllianceWar_MainDlg_RallyList.param.rallyType = AllianceWar_MainDlg_RallyList.Params.RallyListType.MEMBER;
    this.panel.belowList.Setup(AllianceWar_MainDlg_RallyList.param);
  }
}
