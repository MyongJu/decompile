﻿// Decompiled with JetBrains decompiler
// Type: IAPStorePackageBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class IAPStorePackageBanner : MonoBehaviour
{
  public UITexture m_Post;
  public UISprite m_PostBG;
  public UILabel m_Discount;
  public UILabel m_Price;
  public UILabel m_ItemName;
  public UIToggle m_Toggle;
  public UISprite m_Highlight;
  private IAPStorePackage m_StorePackage;
  private System.Action<IAPStorePackage> m_Callback;
  private TweenAlpha m_Tween;

  public void SetData(IAPStorePackage package, System.Action<IAPStorePackage> callback)
  {
    this.m_StorePackage = package;
    this.m_Callback = callback;
    this.UpdateUI();
  }

  public void OnToggleChanged()
  {
    if (this.m_Toggle.value)
    {
      this.StartHighlight();
      if (this.m_Callback == null)
        return;
      this.m_Callback(this.m_StorePackage);
    }
    else
      this.StopHighlight();
  }

  private void StartHighlight()
  {
    this.StopHighlight();
    this.m_Tween = UITweener.Begin<TweenAlpha>(this.m_Highlight.gameObject, 0.5f);
    this.m_Tween.style = UITweener.Style.PingPong;
    this.m_Tween.from = 0.01f;
    this.m_Tween.to = 1f;
  }

  private void StopHighlight()
  {
    if ((bool) ((UnityEngine.Object) this.m_Tween))
    {
      this.m_Tween.enabled = false;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_Tween);
      this.m_Tween = (TweenAlpha) null;
    }
    this.m_Highlight.alpha = 0.0f;
  }

  public void Select()
  {
    this.m_Toggle.Set(true);
  }

  public IAPStorePackage Package
  {
    get
    {
      return this.m_StorePackage;
    }
  }

  private void UpdateUI()
  {
    IAPPackageInfo package = this.m_StorePackage.package;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Post, this.m_StorePackage.group.GraphPath, (System.Action<bool>) null, true, false, string.Empty);
    Color color = Color.white;
    ColorUtility.TryParseHtmlString(this.m_StorePackage.group.thumbnailBGColor, out color);
    this.m_PostBG.color = color;
    this.m_Discount.text = package.discount.ToString() + "%";
    this.m_Price.text = PaymentManager.Instance.GetFormattedPrice(this.m_StorePackage.package.productId, this.m_StorePackage.package.pay_id);
    this.m_ItemName.text = ScriptLocalization.Get(this.m_StorePackage.group.name, true);
  }
}
