﻿// Decompiled with JetBrains decompiler
// Type: UITextureBuilder
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UITextureBuilder : BaseBuilder
{
  private UITexture uiTexture;

  protected override void Prepare()
  {
    if ((UnityEngine.Object) this.Target == (UnityEngine.Object) null || !((UnityEngine.Object) this.uiTexture == (UnityEngine.Object) null))
      return;
    this.uiTexture = this.Target.GetComponent<UITexture>();
    this.IsSuccess = false;
    this.uiTexture.mainTexture = (Texture) null;
    if (!this.needTempIcon || BundleManager.Instance.IsFileCached(this.ImageName))
      return;
    if ((bool) ((UnityEngine.Object) this.CustomMissingTexture))
      this.uiTexture.mainTexture = this.CustomMissingTexture;
    else
      this.uiTexture.mainTexture = this.MissingTexture;
  }

  protected override void Clear()
  {
    if (!((UnityEngine.Object) this.uiTexture != (UnityEngine.Object) null))
      return;
    this.uiTexture.mainTexture = (Texture) null;
    this.uiTexture = (UITexture) null;
  }

  protected override System.Type GetResourceType()
  {
    return typeof (Texture2D);
  }

  protected override string GetPath()
  {
    return this.ImageName;
  }

  protected override void ResultHandler(UnityEngine.Object result)
  {
    Texture2D texture2D = result as Texture2D;
    if ((UnityEngine.Object) texture2D != (UnityEngine.Object) null)
    {
      this.IsSuccess = true;
      this.uiTexture.mainTexture = (Texture) texture2D;
      if (this.useWidgetSetting)
        return;
      this.uiTexture.keepAspectRatio = UIWidget.AspectRatioSource.Free;
      if (texture2D.width != this.uiTexture.width)
      {
        float num = (float) this.uiTexture.width / (float) texture2D.width;
        this.uiTexture.height = (int) ((double) texture2D.height * (double) num);
      }
      else
      {
        float num = (float) this.uiTexture.height / (float) texture2D.height;
        this.uiTexture.width = (int) ((double) texture2D.width * (double) num);
      }
    }
    else
    {
      this.IsSuccess = false;
      if (!this.needTempIcon)
        return;
      if ((bool) ((UnityEngine.Object) this.CustomMissingTexture))
        this.uiTexture.mainTexture = this.CustomMissingTexture;
      else
        this.uiTexture.mainTexture = this.MissingTexture;
    }
  }
}
