﻿// Decompiled with JetBrains decompiler
// Type: ParliamentHeroCard
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class ParliamentHeroCard : MonoBehaviour
{
  private List<UISprite> _AllStar = new List<UISprite>();
  private const string StarSpriteHighlight = "icon_mail_star_01";
  private const string StarSpriteGrey = "hero_card_star";
  [SerializeField]
  private UISprite _StarTemplate;
  [SerializeField]
  private UITable _StarContainer;
  [SerializeField]
  private GameObject _RootFragmentInfo;
  [SerializeField]
  private UILabel _LabelFragmentCount;
  [SerializeField]
  private UILabel _LabelLevel;
  [SerializeField]
  private UILabel _LabelName;
  [SerializeField]
  private UISprite _SpriteQuality;
  [SerializeField]
  private UITexture _TextureHero;
  [SerializeField]
  private GameObject _RootStar;
  [SerializeField]
  private GameObject _RootLevel;

  protected UISprite CreateStar()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._StarTemplate.gameObject);
    gameObject.transform.SetParent(this._StarContainer.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    UISprite component = gameObject.GetComponent<UISprite>();
    component.color = Color.white;
    this._AllStar.Add(component);
    return component;
  }

  protected void DestroyAllStar()
  {
    using (List<UISprite>.Enumerator enumerator = this._AllStar.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UISprite current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
        {
          current.transform.parent = (Transform) null;
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
        }
      }
    }
    this._AllStar.Clear();
  }

  protected void Reset()
  {
    this._StarTemplate.gameObject.SetActive(false);
    this._RootFragmentInfo.SetActive(false);
    this._LabelLevel.text = "1";
    this._LabelName.text = string.Empty;
    this.DestroyAllStar();
  }

  protected void FillData(LegendCardData legendCardData)
  {
    this._LabelLevel.text = legendCardData.Level.ToString();
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(legendCardData.LegendId);
    if (parliamentHeroInfo != null)
      this.FillData(parliamentHeroInfo);
    for (int index = 0; index < legendCardData.Star && index < this._AllStar.Count; ++index)
      this._AllStar[index].spriteName = "icon_mail_star_01";
  }

  protected void FillData(ParliamentHeroInfo parliamentHeroInfo)
  {
    this._LabelName.text = parliamentHeroInfo.Name;
    this._LabelName.color = parliamentHeroInfo.QualityColor;
    BuilderFactory.Instance.HandyBuild((UIWidget) this._TextureHero, parliamentHeroInfo.BigIcon, (System.Action<bool>) null, true, false, string.Empty);
    this._SpriteQuality.color = parliamentHeroInfo.QualityColor;
    ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(parliamentHeroInfo.quality.ToString());
    if (parliamentHeroQualityInfo == null)
      return;
    for (int index = 0; index < parliamentHeroQualityInfo.maxStar; ++index)
      this.CreateStar().spriteName = index >= parliamentHeroQualityInfo.initialStar ? "hero_card_star" : "icon_mail_star_01";
    this._StarContainer.Reposition();
  }

  protected void FillFragmentData(ParliamentHeroInfo parliamentHeroInfo, int count)
  {
    this._RootFragmentInfo.SetActive(true);
    this._LabelFragmentCount.text = string.Format("X{0}", (object) count);
    this.FillData(parliamentHeroInfo);
  }

  public void SetData(LegendCardData legendCardData)
  {
    this.Reset();
    if (legendCardData == null)
      D.error((object) "ParliamentHeroCard::SetData(LegendCardData:null)");
    else
      this.FillData(legendCardData);
  }

  public void SetData(ParliamentHeroInfo parliamentHeroInfo)
  {
    this.Reset();
    if (parliamentHeroInfo == null)
      D.error((object) "ParliamentHeroCard::SetData(ParliamentHeroInfo:null)");
    else
      this.FillData(parliamentHeroInfo);
  }

  public void SetFragmentData(ParliamentHeroInfo parliamentHeroInfo, int fragmentCount)
  {
    this.Reset();
    if (parliamentHeroInfo == null)
      D.error((object) "ParliamentHeroCard::SetFragmentData(ParliamentHeroInfo:null)");
    else
      this.FillFragmentData(parliamentHeroInfo, fragmentCount);
  }

  public void SetStarVisible(bool visible)
  {
    this._RootStar.SetActive(visible);
  }

  public void SetLevelVisible(bool visible)
  {
    this._RootLevel.SetActive(visible);
  }
}
