﻿// Decompiled with JetBrains decompiler
// Type: HospitalBuildingInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UI;

public class HospitalBuildingInfoDlg : BuildingInfoBaseDlg
{
  public UITexture texture1;
  public UILabel nameLabel1;
  public UILabel valueLabel1;
  public UITexture texture2;
  public UILabel nameLabel2;
  public UILabel valueLabel2;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    int benefitValue1 = (int) this.buildingInfo.Benefit_Value_1;
    this.nameLabel1.text = ConfigManager.inst.DB_Properties[int.Parse(this.buildingInfo.Benefit_ID_1)].Name;
    this.valueLabel1.text = benefitValue1.ToString();
    this.nameLabel2.text = Utils.XLAT("hospital_total_wounded_capacity");
    float total = DBManager.inst.DB_Local_Benefit.Get("prop_healing_capacity_base_value").total;
    string BenefitCalcID = "calc_healing_capacity";
    int finalData = (int) ConfigManager.inst.DB_BenefitCalc.GetFinalData(total, BenefitCalcID);
    string str = (double) finalData < (double) total ? "-" : "+";
    this.valueLabel2.text = "[B4B4B4]" + Utils.ConvertNumberToNormalString((long) total) + "[-][54AE07]" + str + Utils.ConvertNumberToNormalString((long) Math.Abs((float) finalData - total)) + "[-]";
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    BuilderFactory.Instance.Release((UIWidget) this.texture1);
    BuilderFactory.Instance.Release((UIWidget) this.texture2);
    base.OnClose(orgParam);
  }
}
