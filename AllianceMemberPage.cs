﻿// Decompiled with JetBrains decompiler
// Type: AllianceMemberPage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceMemberPage : MonoBehaviour
{
  private List<IAllianceRankHeader> m_HeaderList = new List<IAllianceRankHeader>();
  private List<long> m_ChannelList = new List<long>();
  public UIScrollView m_ScrollView;
  public UITable m_Table;
  public GameObject m_HeaderPrefab;
  public GameObject m_CandidatePrefab;
  private List<long> m_Result;
  private float m_OnlineUpdateTime;

  public void ShowPage()
  {
    if (this.gameObject.activeSelf)
      return;
    this.gameObject.SetActive(true);
    this.ClearData();
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((_param1, _param2) =>
    {
      try
      {
        this.InitializePage();
      }
      catch
      {
      }
    }), true);
  }

  public void HidePage()
  {
    this.gameObject.SetActive(false);
  }

  public void ShowRankView()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceRankPrivilegePopup", (Popup.PopupParameter) null);
  }

  private void InitializePage()
  {
    AllianceData allianceData = PlayerData.inst.allianceData;
    this.AddRankHeader(allianceData, 6, true);
    this.AddRankHeader(allianceData, 4, false);
    this.AddRankHeader(allianceData, 3, false);
    this.AddRankHeader(allianceData, 2, false);
    this.AddRankHeader(allianceData, 1, false);
    this.AddRankHeader(allianceData, 0, false);
    this.m_Table.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  private void AddRankHeader(AllianceData allianceData, int title, bool expand)
  {
    GameObject gameObject = title != 0 ? Utils.DuplicateGOB(this.m_HeaderPrefab, this.m_Table.transform) : Utils.DuplicateGOB(this.m_CandidatePrefab, this.m_Table.transform);
    gameObject.SetActive(true);
    IAllianceRankHeader component = gameObject.GetComponent(typeof (IAllianceRankHeader)) as IAllianceRankHeader;
    component.SetData(allianceData, title, expand, new System.Action(this.OnHeaderClicked));
    this.m_HeaderList.Add(component);
  }

  private void OnHeaderClicked()
  {
    this.m_Table.Reposition();
  }

  private void ClearData()
  {
    using (List<IAllianceRankHeader>.Enumerator enumerator = this.m_HeaderList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IAllianceRankHeader current = enumerator.Current;
        current.gameObject.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_HeaderList.Clear();
  }

  private void Update()
  {
    if (this.m_Result == null)
    {
      this.m_OnlineUpdateTime += Time.deltaTime;
      if ((double) this.m_OnlineUpdateTime < 5.0)
        return;
      this.m_OnlineUpdateTime = 0.0f;
      AllianceMembersContainer members = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId).members;
      this.m_ChannelList.Clear();
      Dictionary<long, AllianceMemberData>.Enumerator enumerator = members.datas.GetEnumerator();
      while (enumerator.MoveNext())
        this.m_ChannelList.Add(DBManager.inst.DB_User.Get(enumerator.Current.Value.uid).channelId);
      PushManager.inst.GetOnlineUsers(this.m_ChannelList, new System.Action<List<long>>(this.GetOnlineUsersCallback));
    }
    else
    {
      int count = this.m_HeaderList.Count;
      for (int index = 0; index < count; ++index)
        this.m_HeaderList[index].UpdateOnlineStatus(this.m_Result);
      this.m_Result = (List<long>) null;
    }
  }

  private void GetOnlineUsersCallback(List<long> result)
  {
    this.m_Result = result;
  }
}
