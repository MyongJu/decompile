﻿// Decompiled with JetBrains decompiler
// Type: AllianceStoreHonorPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceStoreHonorPanel : MonoBehaviour
{
  private Dictionary<AllianceStoreDataKey, AllianceStoreHonorItem> m_ItemDict = new Dictionary<AllianceStoreDataKey, AllianceStoreHonorItem>();
  public UILabel m_Honor;
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  public GameObject m_ItemPrefab;
  public UITexture m_HonorIcon;
  public GameObject m_EmptyTip;

  public void Show()
  {
    this.gameObject.SetActive(true);
    this.ClearData();
    this.UpdateHonor();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_HonorIcon, "Texture/Alliance/alliance_honor", (System.Action<bool>) null, true, false, string.Empty);
    DBManager.inst.DB_User.onDataChanged += new System.Action<long>(this.OnUserDataChanged);
    DBManager.inst.DB_AllianceStore.onDataCreate += new System.Action<AllianceStoreDataKey>(this.OnAllianceStoreCreate);
    DBManager.inst.DB_AllianceStore.onDataChanged += new System.Action<AllianceStoreDataKey>(this.OnAllianceStoreChanged);
    DBManager.inst.DB_AllianceStore.onDataRemove += new System.Action<AllianceStoreDataKey>(this.OnAllianceStoreRemove);
    DBManager.inst.DB_AllianceStore.Clear();
    MessageHub.inst.GetPortByAction("AllianceStore:getAllianceStoreItems").SendRequest((Hashtable) null, new System.Action<bool, object>(this.OnGetStoreItems), true);
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
    this.ClearData();
    DBManager.inst.DB_User.onDataChanged -= new System.Action<long>(this.OnUserDataChanged);
    DBManager.inst.DB_AllianceStore.onDataCreate -= new System.Action<AllianceStoreDataKey>(this.OnAllianceStoreCreate);
    DBManager.inst.DB_AllianceStore.onDataChanged -= new System.Action<AllianceStoreDataKey>(this.OnAllianceStoreChanged);
    DBManager.inst.DB_AllianceStore.onDataRemove -= new System.Action<AllianceStoreDataKey>(this.OnAllianceStoreRemove);
  }

  public void OnShowRecord()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "type"] = (object) 0;
    MessageHub.inst.GetPortByAction("allianceStore:getStoreHistory").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenPopup("Alliance/AllianceStoreRecordPopup", (Popup.PopupParameter) new AllianceStoreHistoryPopup.Parameter()
      {
        payload = data,
        type = AllianceStoreType.Honor
      });
    }), true);
  }

  private void OnGetStoreItems(bool ret, object data)
  {
    if (!ret)
      return;
    this.UpdateData(data as ArrayList);
  }

  private void UpdateData(ArrayList payload)
  {
    this.ClearData();
    List<AllianceStoreData> allianceStoreDataList = DBManager.inst.DB_AllianceStore.GetMyAllianceStoreDataList();
    for (int index = 0; index < allianceStoreDataList.Count; ++index)
    {
      AllianceStoreData allianceStoreData = allianceStoreDataList[index];
      AllianceStoreHonorItem itemRenderer = this.CreateItemRenderer(allianceStoreData);
      this.m_ItemDict.Add(allianceStoreData.dataKey, itemRenderer);
    }
    this.m_Grid.Reposition();
    this.m_EmptyTip.SetActive(this.m_ItemDict.Count == 0);
    this.m_ScrollView.ResetPosition();
  }

  private void ClearData()
  {
    Dictionary<AllianceStoreDataKey, AllianceStoreHonorItem>.Enumerator enumerator = this.m_ItemDict.GetEnumerator();
    while (enumerator.MoveNext())
    {
      AllianceStoreHonorItem allianceStoreHonorItem = enumerator.Current.Value;
      allianceStoreHonorItem.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) allianceStoreHonorItem.gameObject);
    }
    this.m_ItemDict.Clear();
  }

  private AllianceStoreHonorItem CreateItemRenderer(AllianceStoreData allianceStoreData)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
    gameObject.SetActive(true);
    gameObject.transform.parent = this.m_Grid.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
    AllianceStoreHonorItem component = gameObject.GetComponent<AllianceStoreHonorItem>();
    component.SetData(allianceStoreData);
    return component;
  }

  private void OnUserDataChanged(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.UpdateHonor();
  }

  private void OnAllianceStoreCreate(AllianceStoreDataKey dataKey)
  {
    AllianceStoreData allianceStoreData = DBManager.inst.DB_AllianceStore.Get(dataKey);
    AllianceStoreHonorItem itemRenderer = this.CreateItemRenderer(allianceStoreData);
    this.m_ItemDict.Add(allianceStoreData.dataKey, itemRenderer);
    this.m_Grid.Reposition();
    this.m_EmptyTip.SetActive(this.m_ItemDict.Count == 0);
  }

  private void OnAllianceStoreChanged(AllianceStoreDataKey dataKey)
  {
    Dictionary<AllianceStoreDataKey, AllianceStoreHonorItem>.Enumerator enumerator = this.m_ItemDict.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Value.UpdateUI();
  }

  private void OnAllianceStoreRemove(AllianceStoreDataKey dataKey)
  {
    AllianceStoreHonorItem allianceStoreHonorItem = (AllianceStoreHonorItem) null;
    this.m_ItemDict.TryGetValue(dataKey, out allianceStoreHonorItem);
    if (!((UnityEngine.Object) allianceStoreHonorItem != (UnityEngine.Object) null))
      return;
    allianceStoreHonorItem.transform.parent = (Transform) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) allianceStoreHonorItem.gameObject);
    this.m_ItemDict.Remove(dataKey);
    this.m_Grid.Reposition();
    this.m_EmptyTip.SetActive(this.m_ItemDict.Count == 0);
  }

  private void UpdateHonor()
  {
    this.m_Honor.text = Utils.FormatThousands(PlayerData.inst.userData.currency.honor.ToString());
  }
}
