﻿// Decompiled with JetBrains decompiler
// Type: ChatZone
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public enum ChatZone
{
  ZONE_KINGDOM = 0,
  ZONE_MINIMUM = 0,
  ZONE_ALLIANCE = 1,
  ZONE_PRIVATE = 2,
  ZONE_SYSTEM = 3,
  ZONE_COUNT = 4,
}
