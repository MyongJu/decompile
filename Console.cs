﻿// Decompiled with JetBrains decompiler
// Type: Console
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class Console
{
  private const int MAX_LINES = 100;
  private const int MAX_HISTORY = 50;
  private const string COMMAND_OUTPUT_PREFIX = "> ";
  private static Console instance;
  private CommandTree m_commands;
  private List<string> m_output;
  private List<string> m_history;
  private string m_help;
  private Queue<QueuedCommand> m_commandQueue;

  private Console()
  {
    this.m_commands = new CommandTree();
    this.m_output = new List<string>();
    this.m_history = new List<string>();
    this.m_commandQueue = new Queue<QueuedCommand>();
    this.RegisterAttributes();
  }

  public static Console Instance
  {
    get
    {
      if (Console.instance == null)
        Console.instance = new Console();
      return Console.instance;
    }
  }

  public static void Update()
  {
    while (Console.Instance.m_commandQueue.Count > 0)
    {
      QueuedCommand queuedCommand = Console.Instance.m_commandQueue.Dequeue();
      queuedCommand.command(queuedCommand.args);
    }
  }

  public static void Queue(Console.CommandCallback command, string[] args)
  {
    Console.Instance.m_commandQueue.Enqueue(new QueuedCommand()
    {
      command = command,
      args = args
    });
  }

  public static void Run(string str)
  {
    if (str.Length <= 0)
      return;
    Console.LogCommand(str);
    Console.Instance.RecordCommand(str);
    Console.Instance.m_commands.Run(str);
  }

  [ConsoleCommand("clear", "clears console output", false)]
  public static void Clear()
  {
    Console.Instance.m_output.Clear();
  }

  [ConsoleCommand("help", "prints commands", false)]
  public static void Help()
  {
    Console.Log(string.Format("Commands:{0}", (object) Console.Instance.m_help));
  }

  public static string Complete(string partialCommand)
  {
    return Console.Instance.m_commands.Complete(partialCommand);
  }

  public static void LogCommand(string cmd)
  {
    Console.Log("> " + cmd);
  }

  public static void Log(string str)
  {
    Console.Instance.m_output.Add(str);
    if (Console.Instance.m_output.Count <= 100)
      return;
    Console.Instance.m_output.RemoveAt(0);
  }

  public static string Output()
  {
    return string.Join("\n", Console.Instance.m_output.ToArray());
  }

  public static void RegisterCommand(string command, string desc, Console.CommandCallback callback, bool runOnMainThread = true)
  {
    if (command == null || command.Length == 0)
      throw new Exception("Command String cannot be empty");
    Console.Instance.m_commands.Add(command, callback, runOnMainThread);
    Console.Instance.m_help += string.Format("\n{0} : {1}", (object) command, (object) desc);
  }

  private void RegisterAttributes()
  {
    foreach (System.Type type in Assembly.GetExecutingAssembly().GetTypes())
    {
      foreach (MethodInfo method in type.GetMethods(BindingFlags.Static | BindingFlags.Public))
      {
        ConsoleCommandAttribute[] customAttributes = method.GetCustomAttributes(typeof (ConsoleCommandAttribute), true) as ConsoleCommandAttribute[];
        if (customAttributes.Length != 0)
        {
          Console.CommandCallback cmd = (Console.CommandCallback) Delegate.CreateDelegate(typeof (Console.CommandCallback), method, false);
          if (cmd == null)
          {
            System.Action action = (System.Action) Delegate.CreateDelegate(typeof (System.Action), method, false);
            if (action != null)
              cmd = (Console.CommandCallback) (args => action());
          }
          foreach (ConsoleCommandAttribute commandAttribute in customAttributes)
          {
            if (string.IsNullOrEmpty(commandAttribute.m_command))
              Debug.LogError((object) string.Format("Method {0}.{1} needs a valid command name.", (object) type, (object) method.Name));
            else if (cmd == null)
            {
              Debug.LogError((object) string.Format("Method {0}.{1} takes the wrong arguments for a console command.", (object) type, (object) method.Name));
            }
            else
            {
              this.m_commands.Add(commandAttribute.m_command, cmd, commandAttribute.m_runOnMainThread);
              this.m_help += string.Format("\n{0} : {1}", (object) commandAttribute.m_command, (object) commandAttribute.m_help);
            }
          }
        }
      }
    }
  }

  public static string PreviousCommand(int index)
  {
    if (index >= 0 && index < Console.Instance.m_history.Count)
      return Console.Instance.m_history[index];
    return (string) null;
  }

  private void RecordCommand(string command)
  {
    this.m_history.Insert(0, command);
    if (this.m_history.Count <= 50)
      return;
    this.m_history.RemoveAt(this.m_history.Count - 1);
  }

  public delegate void CommandCallback(string[] args);
}
