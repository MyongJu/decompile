﻿// Decompiled with JetBrains decompiler
// Type: ScoutMailEquipmentComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoutMailEquipmentComponent : ComponentRenderBase
{
  public GameObject parent;
  public GameObject element;
  public GameObject container;

  public override void Init()
  {
    base.Init();
    Hashtable equipmentTable = (this.data as ScoutMailEquipmentComponentData).equipmentTable;
    List<ScoutMailEquipmentComponent.EquipmentElement> equipmentElementList = new List<ScoutMailEquipmentComponent.EquipmentElement>();
    foreach (DictionaryEntry dictionaryEntry in equipmentTable)
      equipmentElementList.Add(new ScoutMailEquipmentComponent.EquipmentElement(dictionaryEntry.Key.ToString(), float.Parse(dictionaryEntry.Value.ToString())));
    equipmentElementList.Sort((Comparison<ScoutMailEquipmentComponent.EquipmentElement>) ((x, y) => ConfigManager.inst.DB_Properties[DBManager.inst.DB_Local_Benefit.GetLongID(x.key)].Priority.CompareTo(ConfigManager.inst.DB_Properties[DBManager.inst.DB_Local_Benefit.GetLongID(y.key)].Priority)));
    GameObject parent = NGUITools.AddChild(this.parent, this.container);
    int index = 0;
    while (index < equipmentElementList.Count)
    {
      GameObject gameObject = NGUITools.AddChild(parent, this.element);
      PropertyDefinition dbProperty1 = ConfigManager.inst.DB_Properties[DBManager.inst.DB_Local_Benefit.GetLongID(equipmentElementList[index].key)];
      gameObject.transform.Find("key0").GetComponent<UILabel>().text = dbProperty1.Name;
      gameObject.transform.Find("value0").GetComponent<UILabel>().text = dbProperty1.ConvertToDisplayString((double) equipmentElementList[index].value, true, true);
      if (index + 1 < equipmentElementList.Count)
      {
        PropertyDefinition dbProperty2 = ConfigManager.inst.DB_Properties[DBManager.inst.DB_Local_Benefit.GetLongID(equipmentElementList[index + 1].key)];
        gameObject.transform.Find("key1").GetComponent<UILabel>().text = dbProperty2.Name;
        gameObject.transform.Find("value1").GetComponent<UILabel>().text = dbProperty2.ConvertToDisplayString((double) equipmentElementList[index + 1].value, true, true);
      }
      else
      {
        gameObject.transform.Find("key1").gameObject.SetActive(false);
        gameObject.transform.Find("value1").gameObject.SetActive(false);
      }
      index += 2;
    }
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private class EquipmentElement
  {
    public string key;
    public float value;

    public EquipmentElement(string key, float value)
    {
      this.key = key;
      this.value = value;
    }
  }
}
