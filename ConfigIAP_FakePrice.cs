﻿// Decompiled with JetBrains decompiler
// Type: ConfigIAP_FakePrice
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigIAP_FakePrice
{
  private Dictionary<string, ConfigIAPFakePriceInfo> m_DataByID;
  private Dictionary<int, ConfigIAPFakePriceInfo> m_DataByInternalID;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<ConfigIAPFakePriceInfo, string>(result as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
  }

  public ConfigIAPFakePriceInfo Get(string id)
  {
    ConfigIAPFakePriceInfo iapFakePriceInfo;
    this.m_DataByID.TryGetValue(id, out iapFakePriceInfo);
    return iapFakePriceInfo;
  }

  public ConfigIAPFakePriceInfo Get(int internalId)
  {
    ConfigIAPFakePriceInfo iapFakePriceInfo;
    this.m_DataByInternalID.TryGetValue(internalId, out iapFakePriceInfo);
    return iapFakePriceInfo;
  }

  public void Clear()
  {
    this.m_DataByID = (Dictionary<string, ConfigIAPFakePriceInfo>) null;
    this.m_DataByInternalID = (Dictionary<int, ConfigIAPFakePriceInfo>) null;
  }
}
