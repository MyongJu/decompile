﻿// Decompiled with JetBrains decompiler
// Type: FestivalEventReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using UI;

public class FestivalEventReportPopup : BaseReportPopup
{
  private FestivalEventReportPopup.FestivalEventReportContent ferc;
  public Icon leader;
  public Icon boss;
  public UISlider damageSlider;
  public RewardItemComponent ric;

  private void Init()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, true, false, string.Empty);
    this.leader.SetData("Texture/Hero/Portrait_Icon/player_portrait_icon_" + this.ferc.portrait, this.ferc.icon, this.ferc.lord_title, new string[4]
    {
      this.ferc.fullname,
      ((double) this.ferc.killed).ToString() + "%",
      this.ferc.loss.ToString(),
      this.ferc.injured.ToString()
    });
    WorldBossStaticInfo data = ConfigManager.inst.DB_WorldBoss.GetData(this.ferc.boos_id);
    if (data != null)
    {
      this.boss.SetData(data.ImagePath, data.LocalName, ((double) this.ferc.loss_rate * 100.0).ToString() + "%");
      this.damageSlider.value = this.ferc.loss_rate;
    }
    this.ric.FeedData((IComponentData) this.AnalyzeRewards(this.ferc.reward_data));
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.ferc = JsonReader.Deserialize<FestivalEventReportPopup.FestivalEventReportContent>(Utils.Object2Json(this.param.hashtable[(object) "data"]));
    this.Init();
  }

  public class FestivalEventReportContent
  {
    public string name;
    public string acronym;
    public string portrait;
    public string icon;
    public int lord_title;
    public float killed;
    public int loss;
    public int injured;
    public int boos_id;
    public float loss_rate;
    public RewardData reward_data;

    public string fullname
    {
      get
      {
        if (this.acronym != null)
          return "(" + this.acronym + ")" + this.name;
        return this.name;
      }
    }
  }
}
