﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightBattleScene
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DragonKnightBattleScene : MonoBehaviour
{
  public float CameraWidth = 5f;
  public GameObject PlayerHudPrefab;
  public Camera BattleCamera;
  public DragonKnightBattleDirector Director1v1;
  public DragonKnightBattleDirector Director1v2;
  public DragonKnightBattleDirector Director1v3;
  private DragonKnightBattleDirector _currentDirector;

  public void Startup(Dictionary<long, RoundPlayer> morals, Dictionary<long, RoundPlayer> evils)
  {
    this.BattleCamera.orthographicSize = (float) ((double) this.CameraWidth / 9.0 * 16.0) / (float) Screen.width * (float) Screen.height;
    DragonKnightSystem.Instance.Controller.BattleHud.EnableSkillBar();
    switch (evils.Count)
    {
      case 1:
        this._currentDirector = this.Director1v1;
        break;
      case 2:
        this._currentDirector = this.Director1v2;
        break;
      case 3:
        this._currentDirector = this.Director1v3;
        break;
    }
    if ((Object) this._currentDirector != (Object) null)
      this._currentDirector.Startup(evils);
    Dictionary<long, RoundPlayer>.Enumerator enumerator = morals.GetEnumerator();
    while (enumerator.MoveNext())
    {
      RoundPlayer roundPlayer = enumerator.Current.Value;
      roundPlayer.Startup((UnityEngine.Animation) null, (IRoundPlayerHud) DragonKnightSystem.Instance.Controller.BattleHud.MyHUD);
      roundPlayer.PlayerHud.Initialize();
      roundPlayer.PlayerHud.SetHealthProgress((float) roundPlayer.Health / (float) roundPlayer.HealthMax);
      roundPlayer.PlayerHud.SetHealthNumber(roundPlayer.Health, roundPlayer.HealthMax);
      roundPlayer.PlayerHud.SetManaProgress((float) roundPlayer.Mana / (float) roundPlayer.ManaMax);
      roundPlayer.PlayerHud.SetManaNumber(roundPlayer.Mana, roundPlayer.ManaMax);
    }
  }

  public void Shutdown()
  {
    DragonKnightSystem.Instance.Controller.BattleAudio.StopAll();
    DragonKnightSystem.Instance.Controller.BattleHud.DisableSkillBar();
    if ((Object) this._currentDirector != (Object) null)
      this._currentDirector.Shutdown();
    this._currentDirector = (DragonKnightBattleDirector) null;
  }
}
