﻿// Decompiled with JetBrains decompiler
// Type: BoostMenuDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class BoostMenuDlg : UI.Dialog
{
  private BetterList<BoostContainer> _containers = new BetterList<BoostContainer>();
  private BetterList<BoostContainerData> _totalData = new BetterList<BoostContainerData>();
  private BoostCategory DisplayCategory = BoostCategory.none;
  private int count = 3;
  public BoostContainer containerPrefab;
  public UIScrollView scrollView;
  public UITable container;
  private BoostMenuDlg.BoostMenuDlgParameter param;
  private bool _isDestroy;
  private int index;

  public void OnBackBtPress()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void LoadMagic()
  {
    RequestManager.inst.SendRequest("AllianceTemple:loadMagicBenefit", Utils.Hash((object) "alliance_id", (object) PlayerData.inst.allianceId), new System.Action<bool, object>(this.LoadDataCallBack), true);
  }

  private void OnDestroy()
  {
    this._isDestroy = true;
  }

  private void LoadDataCallBack(bool success, object result)
  {
    if (this._isDestroy)
      return;
    this.UpdateUI();
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    this.OnShow(orgParam);
    this.param = orgParam as BoostMenuDlg.BoostMenuDlgParameter;
    this.Clear();
    this.DisplayCategory = this.param == null ? BoostCategory.none : this.param.DisplayCategory;
    if (PlayerData.inst.allianceId > 0L)
      this.LoadMagic();
    else
      this.UpdateUI();
  }

  private void RestScrollView()
  {
    if (this.param == null)
      this.scrollView.ResetPosition();
    if (this.param == null || !this.param.IsResetPosition)
      return;
    this.scrollView.ResetPosition();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Clear();
  }

  private void UpdateUI()
  {
    if (this.DisplayCategory == BoostCategory.none)
    {
      this.count = 3;
      this.AddBoostContainer(BoostCategory.battle);
      this.AddBoostContainer(BoostCategory.economy);
      this.AddBoostContainer(BoostCategory.general);
    }
    else
    {
      this.count = 1;
      this.AddBoostContainer(this.DisplayCategory);
    }
  }

  private void OnRePositionHandler()
  {
    ++this.index;
    if (this.index != this.count)
      return;
    this.index = 0;
    this.container.repositionNow = true;
    this.container.Reposition();
    this.RestScrollView();
  }

  private void OnRestComplete()
  {
    this.container.onReposition -= new UITable.OnReposition(this.OnRestComplete);
    NGUITools.AddWidgetCollider(this.container.gameObject, true);
  }

  private void AddBoostContainer(BoostCategory category)
  {
    BoostContainerData data = new BoostContainerData(category);
    if (data.IsEmpty())
    {
      --this.count;
    }
    else
    {
      this.containerPrefab.gameObject.SetActive(true);
      BoostContainer boostContainer = NGUITools.AddChild(this.container.gameObject, this.containerPrefab.gameObject).AddMissingComponent<BoostContainer>();
      boostContainer.OnRepositionHandler = new System.Action(this.OnRePositionHandler);
      boostContainer.SetData(data);
      this._totalData.Add(data);
      this._containers.Add(boostContainer);
      this.containerPrefab.gameObject.SetActive(false);
    }
  }

  private void Clear()
  {
    foreach (BoostContainerData boostContainerData in this._totalData)
      boostContainerData.Clear();
    foreach (BoostContainer container in this._containers)
    {
      container.gameObject.SetActive(false);
      container.Clear();
      NGUITools.Destroy((UnityEngine.Object) container.gameObject);
    }
    this._totalData.Clear();
    this._containers.Clear();
  }

  public class BoostMenuDlgParameter : UI.Dialog.DialogParameter
  {
    public BoostCategory DisplayCategory = BoostCategory.none;
    public bool IsResetPosition = true;
  }
}
