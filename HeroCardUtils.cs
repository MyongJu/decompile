﻿// Decompiled with JetBrains decompiler
// Type: HeroCardUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class HeroCardUtils
{
  public static void LoadHeroCardData(System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("legend:getHeroList").SendRequest((Hashtable) null, callback, true);
  }

  public static void LoadHeroCardDataByLoader()
  {
    if (!HeroCardUtils.IsParliamentUnlocked() && PlayerData.inst.allianceId <= 0L)
      return;
    MessageHub.inst.GetPortByAction("legend:getHeroList").SendLoader((Hashtable) null, (System.Action<bool, object>) null, true, false);
  }

  public static void AppointPosition(long uid, int legendId, int position, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("Legend:appointLegend").SendRequest(new Hashtable()
    {
      {
        (object) nameof (uid),
        (object) uid
      },
      {
        (object) "legend_id",
        (object) legendId
      },
      {
        (object) nameof (position),
        (object) position
      }
    }, callback, true);
  }

  public static void ResignPosition(long uid, int position, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("Legend:resignLegend").SendRequest(new Hashtable()
    {
      {
        (object) nameof (uid),
        (object) uid
      },
      {
        (object) nameof (position),
        (object) position
      }
    }, callback, true);
  }

  public static string GetHeroTitle(int legendId)
  {
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(legendId);
    if (parliamentHeroInfo != null)
    {
      ParliamentInfo parliamentInfo = ConfigManager.inst.DB_Parliament.Get(parliamentHeroInfo.ParliamentPosition.ToString());
      if (parliamentInfo != null)
        return parliamentInfo.Name;
    }
    return string.Empty;
  }

  public static int GetHeroMaxLevel(int legendId)
  {
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(legendId);
    if (parliamentHeroInfo != null)
    {
      ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(parliamentHeroInfo.quality.ToString());
      if (parliamentHeroQualityInfo != null)
        return parliamentHeroQualityInfo.maxLevel;
    }
    return 1;
  }

  public static int GetHeroMaxStars(int legendId)
  {
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(legendId);
    if (parliamentHeroInfo != null)
    {
      ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(parliamentHeroInfo.quality.ToString());
      if (parliamentHeroQualityInfo != null)
        return parliamentHeroQualityInfo.maxStar;
    }
    return 1;
  }

  public static int GetCurrentStars(int legendId)
  {
    LegendCardData legendCardData = DBManager.inst.DB_LegendCard.GetLegendCardData(PlayerData.inst.uid, legendId);
    if (legendCardData != null)
      return legendCardData.Star;
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(legendId);
    if (parliamentHeroInfo != null)
    {
      ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(parliamentHeroInfo.quality.ToString());
      if (parliamentHeroQualityInfo != null)
        return parliamentHeroQualityInfo.initialStar;
    }
    return 1;
  }

  public static bool IsHeroStarFull(int legendId)
  {
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(legendId);
    if (parliamentHeroInfo != null)
    {
      ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(parliamentHeroInfo.quality.ToString());
      if (parliamentHeroQualityInfo != null)
        return parliamentHeroQualityInfo.maxStar <= HeroCardUtils.GetCurrentStars(legendId);
    }
    return false;
  }

  public static int GetCurrentFragments(int legendId)
  {
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(legendId);
    if (parliamentHeroInfo != null)
      return parliamentHeroInfo.ChipCount;
    return 0;
  }

  public static int GetNextStarFragments(int legendId)
  {
    ParliamentHeroStarInfo parliamentHeroStarInfo = ConfigManager.inst.DB_ParliamentHeroStar.Get(HeroCardUtils.GetCurrentStars(legendId).ToString());
    if (parliamentHeroStarInfo != null)
      return parliamentHeroStarInfo.nextStarChipReq;
    return 0;
  }

  public static void HeroCardAddExp(LegendCardData legendCardData, long addExp, ref int newLevel, ref long newExp)
  {
    if (legendCardData == null)
      return;
    newLevel = legendCardData.Level;
    newExp = legendCardData.CurrentLevelXP + addExp;
    int legendId = legendCardData.LegendId;
    while (newExp >= HeroCardUtils.GetRequiredExpToNextLevel(legendId, newLevel))
    {
      newExp -= HeroCardUtils.GetRequiredExpToNextLevel(legendId, newLevel);
      ++newLevel;
      if (HeroCardUtils.GetHeroMaxLevel(legendId) == newLevel)
        break;
    }
  }

  public static long GetRequiredExpToNextLevel(int legendId, int level)
  {
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(legendId);
    if (parliamentHeroInfo == null)
      return 0;
    ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(parliamentHeroInfo.quality.ToString());
    if (parliamentHeroQualityInfo == null)
      return 0;
    ParliamentHeroLevelInfo byLevel = ConfigManager.inst.DB_ParliamentHeroLevel.GetByLevel(level + 1);
    if (byLevel == null)
      return 0;
    return (long) ((double) byLevel.requireExp * (double) parliamentHeroQualityInfo.expReqRate);
  }

  public static int GetCurrentHeroSuitActiveCount(int legendId)
  {
    ParliamentHeroInfo parliamentHeroInfo1 = ConfigManager.inst.DB_ParliamentHero.Get(legendId);
    if (parliamentHeroInfo1 != null)
    {
      List<LegendCardData> cardDataListByUid = DBManager.inst.DB_LegendCard.GetLegendCardDataListByUid(PlayerData.inst.uid);
      if (cardDataListByUid != null)
      {
        int num = 0;
        for (int index = 0; index < cardDataListByUid.Count; ++index)
        {
          ParliamentHeroInfo parliamentHeroInfo2 = ConfigManager.inst.DB_ParliamentHero.Get(cardDataListByUid[index].LegendId);
          if (cardDataListByUid[index].PositionAssigned && parliamentHeroInfo2 != null && parliamentHeroInfo2.heroSuitGroup == parliamentHeroInfo1.heroSuitGroup)
            ++num;
        }
        return num;
      }
    }
    return 0;
  }

  public static int GetSuitActiveCount(int suitGroupId)
  {
    int num = 0;
    using (List<LegendCardData>.Enumerator enumerator = DBManager.inst.DB_LegendCard.GetLegendCardDataListByUid(PlayerData.inst.uid).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        LegendCardData current = enumerator.Current;
        if (current.PositionAssigned)
        {
          ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(current.LegendId);
          if (parliamentHeroInfo != null && parliamentHeroInfo.heroSuitGroup == suitGroupId)
            ++num;
        }
      }
    }
    return num;
  }

  public static bool ShouldPositionShowPlus(ParliamentInfo info)
  {
    bool flag1 = PlayerData.inst.CityData.mStronghold.mLevel >= info.unlockLevel;
    bool flag2 = DBManager.inst.DB_LegendCard.GetLegendListByPosition(info.GetPosition()).Count > 0;
    if (flag1)
      return flag2;
    return false;
  }

  public static bool HasFreeSummon()
  {
    LegendTempleData legendTempleData = DBManager.inst.DB_LegendTemple.GetLegendTempleData();
    if (legendTempleData != null)
    {
      using (Dictionary<int, long>.Enumerator enumerator = legendTempleData.FreeCD.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.Value <= (long) NetServerTime.inst.ServerTimestamp)
            return true;
        }
      }
    }
    return false;
  }

  public static bool IsPositionAssigned(int position)
  {
    List<LegendCardData> cardDataListByUid = DBManager.inst.DB_LegendCard.GetLegendCardDataListByUid(PlayerData.inst.uid);
    if (cardDataListByUid != null)
      return cardDataListByUid.Find((Predicate<LegendCardData>) (x =>
      {
        if (x.OriginPosition == position)
          return x.PositionAssigned;
        return false;
      })) != null;
    return false;
  }

  public static bool HasParliamentPositionAssign()
  {
    List<ParliamentInfo> parliamentInfoList = ConfigManager.inst.DB_Parliament.GetParliamentInfoList();
    if (parliamentInfoList != null)
      return parliamentInfoList.Find((Predicate<ParliamentInfo>) (x =>
      {
        if (HeroCardUtils.ShouldPositionShowPlus(x))
          return !HeroCardUtils.IsPositionAssigned(x.GetPosition());
        return false;
      })) != null;
    return false;
  }

  public static bool HasHeroInventoryAvailable()
  {
    List<ParliamentHeroInfo> parliamentHeroInfoList = ConfigManager.inst.DB_ParliamentHero.GetParliamentHeroInfoList();
    if (parliamentHeroInfoList != null)
    {
      for (int index = 0; index < parliamentHeroInfoList.Count; ++index)
      {
        LegendCardData legendCardData = DBManager.inst.DB_LegendCard.GetLegendCardData(PlayerData.inst.uid, parliamentHeroInfoList[index].internalId);
        int chipCount = parliamentHeroInfoList[index].ChipCount;
        if (legendCardData != null)
        {
          ParliamentHeroStarInfo parliamentHeroStarInfo = ConfigManager.inst.DB_ParliamentHeroStar.Get(HeroCardUtils.GetCurrentStars(legendCardData.LegendId).ToString());
          if (chipCount >= HeroCardUtils.GetNextStarFragments(legendCardData.LegendId) && !HeroCardUtils.IsHeroStarFull(legendCardData.LegendId) && (parliamentHeroStarInfo != null && legendCardData.Level >= parliamentHeroStarInfo.nextNeedLevel))
            return true;
        }
        else
        {
          ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(parliamentHeroInfoList[index].quality.ToString());
          if (parliamentHeroQualityInfo != null && chipCount >= parliamentHeroQualityInfo.summonChipsReq)
            return true;
        }
      }
    }
    return false;
  }

  public static int GetStartSuitCount(int currentActiveSuitCount, int[] suits)
  {
    if (suits != null && suits.Length > 0 && currentActiveSuitCount >= suits[0])
    {
      for (int index = 0; index < suits.Length; ++index)
      {
        if (currentActiveSuitCount == suits[index])
          return suits[index];
        if (currentActiveSuitCount < suits[index] && index > 0)
          return suits[index - 1];
      }
    }
    return currentActiveSuitCount;
  }

  public static bool IsParliamentUnlocked()
  {
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("parliament_unlock_level");
    return data == null || data.ValueInt <= PlayerData.inst.CityData.mStronghold.mLevel;
  }
}
