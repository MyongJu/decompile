﻿// Decompiled with JetBrains decompiler
// Type: BuildingIdleInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class BuildingIdleInfo
{
  public long internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "type")]
  public string buildingtype;
  [Config(Name = "intro_1")]
  public string intro_1;
  [Config(Name = "intro_2")]
  public string intro_2;
}
