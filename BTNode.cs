﻿// Decompiled with JetBrains decompiler
// Type: BTNode
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class BTNode
{
  private string _nodeName = string.Empty;
  private BTNode.NodeStatus _status;
  private BTNode _parent;

  public BTNode.NodeStatus Status
  {
    get
    {
      return this._status;
    }
    set
    {
      this._status = value;
    }
  }

  public string NodeName
  {
    get
    {
      return this._nodeName;
    }
    set
    {
      this._nodeName = value;
    }
  }

  public BTNode Parent
  {
    get
    {
      return this._parent;
    }
    set
    {
      this._parent = value;
    }
  }

  public virtual void Execute()
  {
  }

  public virtual BTNode Clone()
  {
    return new BTNode()
    {
      _nodeName = this._nodeName,
      _status = this._status,
      _parent = this._parent
    };
  }

  public virtual void Reset()
  {
    this._status = BTNode.NodeStatus.NotExecuted;
  }

  public enum NodeStatus
  {
    NotExecuted,
    Running,
    Success,
    Failure,
  }
}
