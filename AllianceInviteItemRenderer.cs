﻿// Decompiled with JetBrains decompiler
// Type: AllianceInviteItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class AllianceInviteItemRenderer : MonoBehaviour
{
  public UITexture m_PlayerIcon;
  public UILabel m_PlayerName;
  public UILabel m_PlayerPower;
  public UILabel m_PlayerLanguage;
  private InviteSearchData m_InviteData;

  public void SetData(InviteSearchData inviteData)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_PlayerIcon, "Texture/Hero/player_portrait_" + (object) inviteData.portrait, (System.Action<bool>) null, true, false, string.Empty);
    this.m_PlayerName.text = inviteData.name;
    this.m_PlayerPower.text = Utils.FormatThousands(inviteData.power.ToString());
    this.m_PlayerLanguage.text = Language.Instance.GetLanguageName(inviteData.language);
    this.m_InviteData = inviteData;
  }

  public void OnItemClick()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceInviteConfirmPopup", (Popup.PopupParameter) new AllianceInviteConfirmPopup.Parameter()
    {
      uid = this.m_InviteData.uid
    });
  }
}
