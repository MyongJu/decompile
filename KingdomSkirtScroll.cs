﻿// Decompiled with JetBrains decompiler
// Type: KingdomSkirtScroll
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class KingdomSkirtScroll : MonoBehaviour
{
  [SerializeField]
  private float m_Speed0 = 1f;
  [SerializeField]
  private float m_Speed1 = 1f;
  private Vector2 m_UVCache = new Vector2();
  [SerializeField]
  private MeshRenderer m_Layer0;
  [SerializeField]
  private MeshRenderer m_Layer1;
  private float m_CurrentOffset0;
  private float m_CurrentOffset1;
  private float m_UVScale;

  public void SetKingdom(KingdomData kingdom, float height, KingdomSkirtScroll.Boundary boundary)
  {
    switch (boundary)
    {
      case KingdomSkirtScroll.Boundary.BOTTOM:
        this.transform.localPosition = new Vector3(kingdom.Dimension.x + 0.5f * kingdom.Dimension.width, kingdom.Dimension.y + kingdom.Dimension.height, -0.5f * height);
        this.transform.localRotation = Quaternion.Euler(-90f, 0.0f, 0.0f);
        this.transform.localScale = new Vector3(kingdom.Dimension.width, height, 1f);
        break;
      case KingdomSkirtScroll.Boundary.TOP:
        this.transform.localPosition = new Vector3(kingdom.Dimension.x + 0.5f * kingdom.Dimension.width, kingdom.Dimension.y, -0.5f * height);
        this.transform.localRotation = Quaternion.Euler(-90f, 0.0f, 0.0f);
        this.transform.localScale = new Vector3(kingdom.Dimension.width, height, 1f);
        break;
      case KingdomSkirtScroll.Boundary.LEFT:
        this.transform.localPosition = new Vector3(kingdom.Dimension.x, kingdom.Dimension.y + 0.5f * kingdom.Dimension.height, -0.5f * height);
        this.transform.localRotation = Quaternion.Euler(0.0f, -90f, 90f);
        this.transform.localScale = new Vector3(kingdom.Dimension.width, height, 1f);
        break;
      case KingdomSkirtScroll.Boundary.RIGHT:
        this.transform.localPosition = new Vector3(kingdom.Dimension.x + kingdom.Dimension.width, kingdom.Dimension.y + 0.5f * kingdom.Dimension.height, -0.5f * height);
        this.transform.localRotation = Quaternion.Euler(0.0f, -90f, 90f);
        this.transform.localScale = new Vector3(kingdom.Dimension.width, height, 1f);
        break;
    }
    this.m_UVScale = kingdom.Dimension.width / height;
    this.m_UVCache.Set(this.m_UVScale, 1f);
    this.m_Layer0.material.SetTextureScale("_MainTex", this.m_UVCache);
    this.m_Layer1.material.SetTextureScale("_MainTex", this.m_UVCache);
  }

  private void Update()
  {
    this.m_CurrentOffset0 += this.m_Speed0 * Time.deltaTime;
    this.m_CurrentOffset0 %= this.m_UVScale;
    this.m_CurrentOffset1 += this.m_Speed1 * Time.deltaTime;
    this.m_CurrentOffset1 %= this.m_UVScale;
    if ((Object) this.m_Layer0 != (Object) null)
    {
      this.m_UVCache.Set(this.m_CurrentOffset0, 0.0f);
      this.m_Layer0.material.SetTextureOffset("_MainTex", this.m_UVCache);
    }
    if (!((Object) this.m_Layer1 != (Object) null))
      return;
    this.m_UVCache.Set(this.m_CurrentOffset1, 0.0f);
    this.m_Layer1.material.SetTextureOffset("_MainTex", this.m_UVCache);
  }

  public enum Boundary
  {
    BOTTOM,
    TOP,
    LEFT,
    RIGHT,
  }
}
