﻿// Decompiled with JetBrains decompiler
// Type: ConfigEffect
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigEffect
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, EffectInfo> _effectDatas;
  private Dictionary<int, EffectInfo> _internalEffectDatas;

  public void BuildDB(object res)
  {
    Hashtable sources = res as Hashtable;
    if (sources == null)
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    else
      this.parse.Parse<EffectInfo, string>(sources, "ID", out this._effectDatas, out this._internalEffectDatas);
  }

  public EffectInfo GetEffectInfoByInternalID(int internalID)
  {
    EffectInfo effectInfo = (EffectInfo) null;
    this._internalEffectDatas.TryGetValue(internalID, out effectInfo);
    return effectInfo;
  }

  public void Clear()
  {
    if (this._effectDatas != null)
      this._effectDatas.Clear();
    if (this._internalEffectDatas == null)
      return;
    this._internalEffectDatas.Clear();
  }
}
