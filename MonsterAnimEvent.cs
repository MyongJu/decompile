﻿// Decompiled with JetBrains decompiler
// Type: MonsterAnimEvent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAnimEvent : MonoBehaviour
{
  private StateMachine _stateMachine = new StateMachine();
  private Dictionary<long, long> _attackers = new Dictionary<long, long>();
  private Animator _animator;

  private void Start()
  {
    this._stateMachine.AddState((IState) new MonsterStateAttack(this));
    this._stateMachine.AddState((IState) new MonsterStateDeath(this));
    this._stateMachine.AddState((IState) new MonsterStateIdle(this));
    this.Reset();
  }

  public Animator Animator
  {
    get
    {
      if ((Object) this._animator == (Object) null)
        this._animator = this.GetComponent<Animator>();
      return this._animator;
    }
  }

  public void Reset()
  {
    this._stateMachine.SetState(typeof (MonsterStateIdle).ToString(), (Hashtable) null);
    this._attackers.Clear();
  }

  public void AddAttacker(long marchId)
  {
    this._attackers[marchId] = marchId;
  }

  public void RemoveAttacker(long marchId)
  {
    this._attackers.Remove(marchId);
  }

  public bool HasAttacker
  {
    get
    {
      return this._attackers.Count > 0;
    }
  }

  public StateMachine StateMachine
  {
    get
    {
      return this._stateMachine;
    }
  }

  public void Kill()
  {
    this._stateMachine.SetState(typeof (MonsterStateDeath).ToString(), (Hashtable) null);
  }

  private void Update()
  {
    this._stateMachine.Process();
  }
}
