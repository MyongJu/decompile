﻿// Decompiled with JetBrains decompiler
// Type: BuildingMoveManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingMoveManager : MonoBehaviour
{
  private List<MovableCircle> mcs = new List<MovableCircle>();
  private LayerMask cacheCityCamera = (LayerMask) 0;
  private const string movablecirclepath = "Prefab/City/movablecircle";
  private static BuildingMoveManager _instance;
  private UICamera ccamera;
  private bool isMoving;
  private BuildingMoveManager.MoveState ms;
  private BuildingController selectedBuilding;
  private GameObject movablecircle;

  public static BuildingMoveManager Instance
  {
    get
    {
      if ((UnityEngine.Object) BuildingMoveManager._instance == (UnityEngine.Object) null)
      {
        GameObject gameObject = new GameObject();
        BuildingMoveManager._instance = gameObject.AddComponent<BuildingMoveManager>();
        gameObject.name = nameof (BuildingMoveManager);
      }
      return BuildingMoveManager._instance;
    }
  }

  public void ShowMovableCircle()
  {
    if (this.isMoving)
      return;
    this.isMoving = true;
    this.ccamera = UIManager.inst.cityCamera.GetComponent<UICamera>();
    this.cacheCityCamera = this.ccamera.eventReceiverMask;
    this.ccamera.eventReceiverMask = (LayerMask) (1 << GameEngine.Instance.BuildingMoveLayer);
    this.mcs.Clear();
    using (List<GameObject>.Enumerator enumerator = CitadelSystem.inst.GobBuildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuildingController component1 = enumerator.Current.GetComponent<BuildingController>();
        if ((UnityEngine.Object) null != (UnityEngine.Object) component1 && component1.CanMove)
          PrefabManagerEx.Instance.SpawnAsync(this.movablecircle, Utils.FindChild(component1.gameObject, "movablecircle").transform, (PrefabSpawnRequestCallback) ((obj, data) =>
          {
            MovableCircle component = obj.GetComponent<MovableCircle>();
            component.SetState(MovableCircle.CircleState.Candidate);
            component.onCircleClicked += new System.Action<GameObject>(this.OnMovableSelected);
            this.mcs.Add(component);
          }), (object) null);
      }
    }
    UIManager.inst.publicHUD.ShowBuildingMovePanel(1);
    this.ms = BuildingMoveManager.MoveState.WaitSelect;
  }

  public void OnMovableSelected(GameObject clickedgo)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    BuildingMoveManager.\u003COnMovableSelected\u003Ec__AnonStorey1D2 selectedCAnonStorey1D2_1 = new BuildingMoveManager.\u003COnMovableSelected\u003Ec__AnonStorey1D2();
    // ISSUE: reference to a compiler-generated field
    selectedCAnonStorey1D2_1.clickedgo = clickedgo;
    // ISSUE: reference to a compiler-generated field
    selectedCAnonStorey1D2_1.\u003C\u003Ef__this = this;
    if (this.ms == BuildingMoveManager.MoveState.WaitSelect)
    {
      UIManager.inst.publicHUD.ShowBuildingMovePanel(2);
      this.ms = BuildingMoveManager.MoveState.WaitExchange;
      // ISSUE: reference to a compiler-generated field
      BuildingController component = selectedCAnonStorey1D2_1.clickedgo.GetComponent<BuildingController>();
      List<MovableCircle> movableCircleList = new List<MovableCircle>();
      using (List<MovableCircle>.Enumerator enumerator = this.mcs.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          MovableCircle current = enumerator.Current;
          if (current.GetComponentInParent<BuildingController>().mZone != component.mZone)
            movableCircleList.Add(current);
        }
      }
      using (List<MovableCircle>.Enumerator enumerator = movableCircleList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          MovableCircle current = enumerator.Current;
          PrefabManagerEx.Instance.Destroy(current.gameObject);
          this.mcs.Remove(current);
        }
      }
      movableCircleList.Clear();
      foreach (Component componentsInChild in (component.mZone != CityPlotController.PLOT_TYPE.PLOT_RURAL ? Utils.FindChild(CitadelSystem.inst.MapRoot, "Urban") : Utils.FindChild(CitadelSystem.inst.MapRoot, "Rural")).GetComponentsInChildren<CityPlotController>())
      {
        GameObject child = Utils.FindChild(componentsInChild.gameObject, "movablecircle");
        if (!((UnityEngine.Object) null == (UnityEngine.Object) child))
        {
          // ISSUE: reference to a compiler-generated method
          PrefabManagerEx.Instance.SpawnAsync(this.movablecircle, child.transform, new PrefabSpawnRequestCallback(selectedCAnonStorey1D2_1.\u003C\u003Em__2D2), (object) null);
        }
      }
      this.selectedBuilding = component;
      this.selectedBuilding.GetComponentInChildren<MovableCircle>().SetState(MovableCircle.CircleState.Selected);
    }
    else
    {
      if (this.ms != BuildingMoveManager.MoveState.WaitExchange)
        return;
      string str = ScriptLocalization.Get("move_building_confirm_description", true);
      int valueInt = ConfigManager.inst.DB_GameConfig.GetData("building_move").ValueInt;
      // ISSUE: reference to a compiler-generated field
      BuildingController clickedbc = selectedCAnonStorey1D2_1.clickedgo.GetComponent<BuildingController>();
      if ((UnityEngine.Object) null != (UnityEngine.Object) clickedbc)
      {
        if ((UnityEngine.Object) clickedbc == (UnityEngine.Object) this.selectedBuilding)
          return;
        UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
        {
          confirmButtonClickEvent = (System.Action) (() =>
          {
            this.selectedBuilding.SwapBuilding(clickedbc);
            this.ExitBuildingMove();
          }),
          description = str,
          goldNum = valueInt
        });
      }
      else
        UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
        {
          confirmButtonClickEvent = (System.Action) (() =>
          {
            // ISSUE: variable of a compiler-generated type
            BuildingMoveManager.\u003COnMovableSelected\u003Ec__AnonStorey1D2 selectedCAnonStorey1D2 = selectedCAnonStorey1D2_1;
            // ISSUE: variable of a compiler-generated type
            BuildingMoveManager.\u003COnMovableSelected\u003Ec__AnonStorey1D1 selectedCAnonStorey1D1 = this;
            CityPlotController clickedcpc = clickedgo.GetComponent<CityPlotController>();
            int oldslotid = this.selectedBuilding.mBuildingItem.mSlotid;
            MessageHub.inst.GetPortByAction("City:moveObject").SendRequest(Utils.Hash((object) "building_id", (object) this.selectedBuilding.mBuildingID, (object) "slot_id", (object) clickedcpc.SlotId), (System.Action<bool, object>) ((_param1, _param2) =>
            {
              CityPlotController.SetVisible(oldslotid, true);
              // ISSUE: reference to a compiler-generated field
              selectedCAnonStorey1D1.\u003C\u003Ef__this.selectedBuilding.SetMapLocation(clickedcpc.SlotId);
              // ISSUE: reference to a compiler-generated field
              VfxManager.Instance.CreateAndPlay("Prefab/VFX/fx_jianzhushengji_small", selectedCAnonStorey1D1.\u003C\u003Ef__this.selectedBuilding.transform);
            }), true);
            this.ExitBuildingMove();
          }),
          description = str,
          goldNum = valueInt
        });
    }
  }

  public void ExitBuildingMove()
  {
    if (!this.isMoving)
      return;
    this.isMoving = false;
    this.ccamera.eventReceiverMask = this.cacheCityCamera;
    using (List<MovableCircle>.Enumerator enumerator = this.mcs.GetEnumerator())
    {
      while (enumerator.MoveNext())
        PrefabManagerEx.Instance.Destroy(enumerator.Current.gameObject);
    }
    this.mcs.Clear();
    UIManager.inst.publicHUD.HideBuildingMovePanel();
  }

  public void ShowTargetCircle()
  {
  }

  public void Selected()
  {
  }

  public void Init()
  {
    if (!((UnityEngine.Object) null == (UnityEngine.Object) this.movablecircle))
      return;
    this.movablecircle = AssetManager.Instance.Load("Prefab/City/movablecircle", (System.Type) null) as GameObject;
  }

  public void Dispose()
  {
    AssetManager.Instance.UnLoadAsset("Prefab/City/movablecircle", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    this.movablecircle = (GameObject) null;
  }

  private enum MoveState
  {
    None,
    WaitSelect,
    WaitExchange,
  }
}
