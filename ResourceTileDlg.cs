﻿// Decompiled with JetBrains decompiler
// Type: ResourceTileDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ResourceTileDlg : UI.Dialog
{
  private ResourceTileDlg.Data _data = new ResourceTileDlg.Data();
  [SerializeField]
  private ResourceTileDlg.Panel panel;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    ResourceTileDlg.Parameter parameter = orgParam as ResourceTileDlg.Parameter;
    if (parameter != null)
    {
      this._data.location = parameter.location;
      this.FreshPanel();
    }
    BuilderFactory.Instance.Build((UIWidget) this.panel.TT_noMarchTexture1, "Texture/GUI_Textures/Banner_Instrucitons_Occupy_Res_Tiles", (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.panel.TT_noMarchTexture2, "Texture/GUI_Textures/Banner_Instrucitons_Grab_Res_Tiles", (System.Action<bool>) null, true, false, true, string.Empty);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    BuilderFactory.Instance.Release((UIWidget) this.panel.TT_noMarchTexture1);
    BuilderFactory.Instance.Release((UIWidget) this.panel.TT_noMarchTexture2);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public DynamicMapData CurrentMapData
  {
    get
    {
      if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PitMode)
        return PitMapData.MapData;
      return PVPMapData.MapData;
    }
  }

  public void FreshPanel()
  {
    this._data.tileData = this.CurrentMapData.GetTileAt(this._data.location);
    this._data.marchData = DBManager.inst.DB_March.Get(this._data.tileData.MarchID);
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("num", this._data.tileData.Level.ToString());
    switch (this._data.tileData.TileType)
    {
      case TileType.Resource1:
        this.panel.title.text = ScriptLocalization.GetWithPara("kingdom_gather_farm_title", para, true);
        break;
      case TileType.Resource2:
        this.panel.title.text = ScriptLocalization.GetWithPara("kingdom_gather_sawmill_title", para, true);
        break;
      case TileType.Resource3:
        this.panel.title.text = ScriptLocalization.GetWithPara("kingdom_gather_mine_title", para, true);
        break;
      case TileType.Resource4:
        this.panel.title.text = ScriptLocalization.GetWithPara("kingdom_gather_house_title", para, true);
        break;
      case TileType.PitMine:
        this.panel.title.text = ScriptLocalization.GetWithPara("event_fire_kingdom_dragon_nest_level_title", new Dictionary<string, string>()
        {
          {
            "0",
            this._data.tileData.Level.ToString()
          }
        }, true);
        break;
    }
    if (this._data.marchData == null || this._data.marchData.ownerUid != PlayerData.inst.uid || this._data.marchData != null && this._data.marchData.state != MarchData.MarchState.gathering)
    {
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
    {
      this.panel.NoMarchContainer.gameObject.SetActive(false);
      this.panel.MarchContainer.gameObject.SetActive(true);
      switch (this._data.tileData.TileType)
      {
        case TileType.Resource1:
          this.panel.marchTitle.text = ScriptLocalization.GetWithPara("kingdom_gather_farm_title", para, true);
          break;
        case TileType.Resource2:
          this.panel.marchTitle.text = ScriptLocalization.GetWithPara("kingdom_gather_sawmill_title", para, true);
          break;
        case TileType.Resource3:
          this.panel.marchTitle.text = ScriptLocalization.GetWithPara("kingdom_gather_mine_title", para, true);
          break;
        case TileType.Resource4:
          this.panel.marchTitle.text = ScriptLocalization.GetWithPara("kingdom_gather_house_title", para, true);
          break;
        case TileType.PitMine:
          this.panel.marchTitle.text = ScriptLocalization.GetWithPara("event_fire_kingdom_dragon_nest_level_title", new Dictionary<string, string>()
          {
            {
              "0",
              this._data.tileData.Level.ToString()
            }
          }, true);
          break;
      }
      this.panel.resourceIcon.spriteName = this.GetSpriteNameByTileType(this._data.tileData.TileType);
      this.panel.resourceTotlaCount.text = this._data.tileData.Value.ToString();
      int troopLoad = this._data.marchData.CalculateTroopLoad();
      switch (this._data.tileData.TileType)
      {
        case TileType.Resource1:
          troopLoad /= ConfigManager.inst.DB_GameConfig.GetData("food_load").ValueInt;
          break;
        case TileType.Resource2:
          troopLoad /= ConfigManager.inst.DB_GameConfig.GetData("wood_load").ValueInt;
          break;
        case TileType.Resource3:
          troopLoad /= ConfigManager.inst.DB_GameConfig.GetData("ore_load").ValueInt;
          break;
        case TileType.Resource4:
          troopLoad /= ConfigManager.inst.DB_GameConfig.GetData("silver_load").ValueInt;
          break;
        case TileType.PitMine:
          troopLoad /= ConfigManager.inst.DB_GameConfig.GetData("abyss_load").ValueInt;
          break;
      }
      int num1 = Mathf.Min(troopLoad, this._data.tileData.Value);
      int num2 = this._data.marchData.endTime - NetServerTime.inst.ServerTimestamp;
      this.panel.gatherdCount.text = Utils.FormatThousands(((int) ((double) (NetServerTime.inst.ServerTimestamp - this._data.marchData.startTime) / (double) this._data.marchData.timeDuration.duration * (double) num1)).ToString());
      float baseGatherRate = this.GetBaseGatherRate();
      float num3 = this._data.marchData.timeDuration.oneOverTimeDuration * (float) num1;
      if (this._data.marchData.battleLog != null && this._data.marchData.battleLog.ContainsKey((object) "gather_rate"))
        num3 = float.Parse(this._data.marchData.battleLog[(object) "gather_rate"].ToString());
      if ((double) num3 == 0.0 || (double) num3 < (double) baseGatherRate)
        num3 = baseGatherRate;
      this.panel.gatherSpeed.text = string.Format("{0}/h", (object) Utils.FormatThousands((baseGatherRate * 3600f).ToString()));
      this.panel.gatherSpeedBounes.text = string.Format("+ {0}/h", (object) Utils.FormatThousands(((int) (((double) num3 - (double) baseGatherRate) * 3600.0 + 0.5)).ToString()));
      this.panel.troopCount.text = this._data.marchData.troopsInfo.totalCount.ToString();
      this.panel.getherTime.text = Utils.FormatTime(this._data.marchData.endTime - NetServerTime.inst.ServerTimestamp, false, false, true);
      this.panel.marchDetail.Open((UIControler.UIParameter) new MarchDetailPopUp.Parameter()
      {
        marchId = this._data.marchData.marchId
      });
      if (this._data.tileData.TileType == TileType.PitMine)
      {
        this.panel.gatherSpeedBounes.gameObject.SetActive(false);
        this.panel.buttonAddBounes.SetActive(false);
      }
      else
      {
        this.panel.gatherSpeedBounes.gameObject.SetActive(true);
        this.panel.buttonAddBounes.SetActive(true);
      }
    }
  }

  public void OnSecond(int serverTime)
  {
    this.FreshPanel();
  }

  private float GetBaseGatherRate()
  {
    string uniqueId;
    switch (this._data.tileData.TileType)
    {
      case TileType.Resource1:
        uniqueId = "farm_kingdom_" + (object) this._data.tileData.Level;
        break;
      case TileType.Resource2:
        uniqueId = "lumber_camp_kingdom_" + (object) this._data.tileData.Level;
        break;
      case TileType.Resource3:
        uniqueId = "mine_kingdom_" + (object) this._data.tileData.Level;
        break;
      case TileType.Resource4:
        uniqueId = "estate_kingdom_" + (object) this._data.tileData.Level;
        break;
      case TileType.PitMine:
        AbyssMineInfo data1 = ConfigManager.inst.DB_AbyssMine.GetData(this._data.tileData.ResourceId);
        if (data1 != null)
          return data1.pickingRate;
        D.error((object) string.Format("can not find template for abyss: {0}", (object) this._data.tileData.ResourceId));
        return 0.0f;
      default:
        uniqueId = "farm_kingdom_1";
        break;
    }
    Tile_StatisticsInfo data2 = ConfigManager.inst.DB_Tile_Statistics.GetData(uniqueId);
    if (data2 != null)
      return data2.Gathering_Rate;
    return 0.0f;
  }

  private string GetSpriteNameByTileType(TileType type)
  {
    switch (type)
    {
      case TileType.Resource1:
        return "food_icon";
      case TileType.Resource2:
        return "wood_icon";
      case TileType.Resource3:
        return "ore_icon";
      case TileType.Resource4:
        return "sliver_icon";
      case TileType.PitMine:
        return "dragon_fossil_icon";
      default:
        return "food_icon";
    }
  }

  public void OnBoostButtonClick()
  {
    UIManager.inst.OpenDlg("Boost/Boosts_Items", (UI.Dialog.DialogParameter) new BoostsItemsDlg.BoostListDialogParameter()
    {
      Type = BoostCategory.general,
      SubType = BoostType.gatherspeed
    }, true, true, true);
  }

  public void OnBackButtonClick()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnCloseButtonClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void Reset()
  {
    this.panel.title = this.transform.Find("Container/Background/Title_Label").gameObject.GetComponent<UILabel>();
    this.panel.MarchContainer = this.transform.Find("Container/MarchContainer").gameObject.transform;
    this.panel.marchTitle = this.transform.Find("Container/MarchContainer/Content/Dialog_Screen_Small_L/Dialog_Small_L/Title_Label").gameObject.GetComponent<UILabel>();
    this.panel.resourceIcon = this.transform.Find("Container/MarchContainer/Content/TopInfo/ResAmount/ResIcon").gameObject.GetComponent<UISprite>();
    this.panel.resourceTotlaCount = this.transform.Find("Container/MarchContainer/Content/TopInfo/ResAmount/ResValue").gameObject.GetComponent<UILabel>();
    this.panel.gatherdCount = this.transform.Find("Container/MarchContainer/Content/TopInfo/Gathered/Value").gameObject.GetComponent<UILabel>();
    this.panel.gatherSpeed = this.transform.Find("Container/MarchContainer/Content/TopInfo/GatherSpeed/Value1").gameObject.GetComponent<UILabel>();
    this.panel.gatherSpeedBounes = this.transform.Find("Container/MarchContainer/Content/TopInfo/GatherSpeed/Value2").gameObject.GetComponent<UILabel>();
    this.panel.troopCount = this.transform.Find("Container/MarchContainer/Content/TopInfo/TroopAmount/TroopValue").gameObject.GetComponent<UILabel>();
    this.panel.getherTime = this.transform.Find("Container/MarchContainer/Content/TopInfo/GatherRemainingTime/Value").gameObject.GetComponent<UILabel>();
    this.panel.marchDetail = this.transform.Find("Container/MarchContainer/MarchDetailPopUp").gameObject.GetComponent<MarchDetailPopUp>();
    this.panel.NoMarchContainer = this.transform.Find("Container/NoMarchContainer").gameObject.transform;
    this.panel.noMarchSubTitle1 = this.transform.Find("Container/NoMarchContainer/Occupy Farm/TitleName").gameObject.GetComponent<UILabel>();
    this.panel.noMarchDes1 = this.transform.Find("Container/NoMarchContainer/Occupy Farm/text").gameObject.GetComponent<UILabel>();
    this.panel.noMarchSubTitle2 = this.transform.Find("Container/NoMarchContainer/Grab Farm/TitleName").gameObject.GetComponent<UILabel>();
    this.panel.noMarchDes2 = this.transform.Find("Container/NoMarchContainer/Grab Farm/text").gameObject.GetComponent<UILabel>();
    this.panel.buttonAddBounes = this.transform.Find("Container/MarchContainer/Content/TopInfo/GatherSpeed/Btn_Add_Green").gameObject;
  }

  protected class Data
  {
    public Coordinate location;
    public TileData tileData;
    public MarchData marchData;
  }

  [Serializable]
  protected class Panel
  {
    public UILabel title;
    public Transform MarchContainer;
    public UILabel marchTitle;
    public UISprite resourceIcon;
    public UILabel resourceTotlaCount;
    public UILabel gatherdCount;
    public UILabel gatherSpeed;
    public UILabel gatherSpeedBounes;
    public UILabel troopCount;
    public UILabel getherTime;
    public MarchDetailPopUp marchDetail;
    public Transform NoMarchContainer;
    public UITexture TT_noMarchTexture1;
    public UITexture TT_noMarchTexture2;
    public UILabel noMarchSubTitle1;
    public UILabel noMarchSubTitle2;
    public UILabel noMarchDes1;
    public UILabel noMarchDes2;
    public GameObject buttonAddBounes;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public Coordinate location;
  }
}
