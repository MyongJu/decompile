﻿// Decompiled with JetBrains decompiler
// Type: HeroInventorySlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HeroInventorySlot : MonoBehaviour
{
  [SerializeField]
  private ParliamentHeroItem _parliamentHeroItem;
  [SerializeField]
  private GameObject _redPointNode;
  [SerializeField]
  private UIButton _summonButton;
  [SerializeField]
  private UILabel _heroNameText;
  [SerializeField]
  private UIProgressBar _progressBar;
  [SerializeField]
  private UILabel _progressText;
  [SerializeField]
  private UILabel _heroLevelText;
  [SerializeField]
  private UILabel _heroTitleText;
  [SerializeField]
  private GameObject _heroTitleNode;
  [SerializeField]
  private UISprite _heroFrameSprite;
  [SerializeField]
  private UISprite _heroCanSummonSprite;
  [SerializeField]
  private ParliamentHeroStar _parliamentHeroStar;
  private LegendCardData _legendCardData;
  private ParliamentHeroInfo _parliamentHeroInfo;

  protected bool IsHeroCard
  {
    get
    {
      return this._legendCardData != null;
    }
  }

  protected bool RedPointShow
  {
    get
    {
      int currentFragments = this.GetCurrentFragments();
      int summonFragmentsCount = this.GetSummonFragmentsCount();
      if (!this.IsHeroCard)
        return currentFragments >= summonFragmentsCount;
      ParliamentHeroStarInfo parliamentHeroStarInfo = ConfigManager.inst.DB_ParliamentHeroStar.Get(HeroCardUtils.GetCurrentStars(this._legendCardData.LegendId).ToString());
      if (currentFragments >= this.GetNextStarFragments() && !HeroCardUtils.IsHeroStarFull(this._legendCardData.LegendId) && parliamentHeroStarInfo != null)
        return parliamentHeroStarInfo.nextNeedLevel <= this._legendCardData.Level;
      return false;
    }
  }

  public bool HeroSummonable
  {
    get
    {
      if (!this.IsHeroCard)
        return this.GetCurrentFragments() >= this.GetSummonFragmentsCount();
      return false;
    }
  }

  public void SetData(LegendCardData legendCardData)
  {
    this._legendCardData = legendCardData;
    if (this._legendCardData != null)
      this._parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this._legendCardData.LegendId);
    this.UpdateUI();
  }

  public void SetData(ParliamentHeroInfo parliamentHeroInfo)
  {
    this._parliamentHeroInfo = parliamentHeroInfo;
    this.UpdateUI();
  }

  public void RefreshUI(LegendCardData legendCardData)
  {
    if (this._legendCardData == null || this._legendCardData.LegendId != legendCardData.LegendId)
      return;
    this.SetData(legendCardData);
  }

  public void OnSummonButtonClicked()
  {
    if (this._parliamentHeroInfo == null)
      return;
    MessageHub.inst.GetPortByAction("Legend:chipSummon").SendRequest(new Hashtable()
    {
      {
        (object) "legend_id",
        (object) this._parliamentHeroInfo.internalId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.SetData(DBManager.inst.DB_LegendCard.Get(PlayerData.inst.uid, this._parliamentHeroInfo.internalId));
      this.ShowHeroSummonEffect();
    }), true);
  }

  public void OnHeroCardClicked()
  {
    if (this.IsHeroCard)
    {
      UIManager.inst.OpenPopup("HeroCard/HeroInventoryInfoPopup", (Popup.PopupParameter) new HeroInventoryInfoPopup.Parameter()
      {
        legendCardData = this._legendCardData
      });
    }
    else
    {
      if (this._parliamentHeroInfo == null)
        return;
      UIManager.inst.OpenPopup("ParliamentHero/HeroFragmentDetailPopup", (Popup.PopupParameter) new HeroFragmentDetailPopup.Parameter()
      {
        UserId = PlayerData.inst.uid,
        HeroTemplateId = this._parliamentHeroInfo.internalId
      });
    }
  }

  private void UpdateUI()
  {
    if (this.IsHeroCard)
      this.ShowHeroDetail();
    else
      this.ShowFragmentDetail();
    this._heroNameText.text = this.GetName();
    if (this._parliamentHeroInfo != null)
      this._heroNameText.color = this._parliamentHeroInfo.QualityColor;
    int currentFragments = this.GetCurrentFragments();
    int summonFragmentsCount = this.GetSummonFragmentsCount();
    NGUITools.SetActive(this._redPointNode, this.RedPointShow);
    NGUITools.SetActive(this._summonButton.gameObject, !this.IsHeroCard && currentFragments >= summonFragmentsCount);
    this.CheckStarsStatus();
    this.CheckSummonStatus(!this.IsHeroCard && currentFragments >= summonFragmentsCount);
  }

  private void CheckSummonStatus(bool showHighlight)
  {
    NGUITools.SetActive(this._heroCanSummonSprite.gameObject, showHighlight);
  }

  private void ShowHeroDetail()
  {
    this._parliamentHeroItem.SetData(this._legendCardData, true);
    if (this._parliamentHeroInfo != null)
      this._parliamentHeroItem.SetGrey(this._parliamentHeroInfo.quality, false);
    this._heroLevelText.text = this.GetHeroLevelDescription();
    this._heroTitleText.text = this.GetHeroTitle();
    NGUITools.SetActive(this._progressBar.gameObject, false);
    NGUITools.SetActive(this._heroTitleNode, true);
  }

  private void ShowFragmentDetail()
  {
    this._parliamentHeroItem.SetData(this._parliamentHeroInfo, false, false);
    int currentFragments = this.GetCurrentFragments();
    int summonFragmentsCount = this.GetSummonFragmentsCount();
    this._progressBar.value = (float) currentFragments / (float) summonFragmentsCount;
    this._progressText.text = string.Format("{0}/{1}", (object) currentFragments, (object) summonFragmentsCount);
    NGUITools.SetActive(this._progressBar.gameObject, true);
    NGUITools.SetActive(this._heroTitleNode, false);
  }

  private void CheckStarsStatus()
  {
    if (this._parliamentHeroInfo == null)
      return;
    this._parliamentHeroStar.SetData(this._parliamentHeroInfo, this._legendCardData, false);
    this._heroFrameSprite.color = this._parliamentHeroInfo.QualityColor;
  }

  private void ShowHeroSummonEffect()
  {
    if (this._parliamentHeroInfo == null)
      return;
    HeroRecruitLuckyDrawDialog.Parameter parameter = new HeroRecruitLuckyDrawDialog.Parameter();
    HeroRecruitPayloadData recruitPayloadData = new HeroRecruitPayloadData();
    recruitPayloadData.Decode((object) Utils.Hash((object) "type", (object) "hero", (object) "hero_id", (object) this._parliamentHeroInfo.internalId, (object) "num", (object) 1));
    parameter.heroPayload = recruitPayloadData;
    parameter.showButton = false;
    parameter.heroRecruitData = (HeroRecruitData) new HeroRecruitDataGold();
    UIManager.inst.OpenDlg("HeroCard/HeroRecruitLuckyDrawDialog", (UI.Dialog.DialogParameter) parameter, true, true, true);
  }

  private int GetMaxStars()
  {
    return HeroCardUtils.GetHeroMaxStars(!this.IsHeroCard ? this._parliamentHeroInfo.internalId : this._legendCardData.LegendId);
  }

  private int GetCurrentStars()
  {
    if (this.IsHeroCard)
      return HeroCardUtils.GetCurrentStars(this._legendCardData.LegendId);
    if (this._parliamentHeroInfo != null)
    {
      ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(this._parliamentHeroInfo.quality.ToString());
      if (parliamentHeroQualityInfo != null)
        return parliamentHeroQualityInfo.initialStar;
    }
    return 1;
  }

  private int GetCurrentFragments()
  {
    if (this._parliamentHeroInfo != null)
      return this._parliamentHeroInfo.ChipCount;
    return 0;
  }

  private int GetSummonFragmentsCount()
  {
    if (this._parliamentHeroInfo != null)
    {
      ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(this._parliamentHeroInfo.quality.ToString());
      if (parliamentHeroQualityInfo != null)
        return parliamentHeroQualityInfo.summonChipsReq;
    }
    return 0;
  }

  private int GetNextStarFragments()
  {
    ParliamentHeroStarInfo parliamentHeroStarInfo = ConfigManager.inst.DB_ParliamentHeroStar.Get(this.GetCurrentStars().ToString());
    if (parliamentHeroStarInfo != null)
      return parliamentHeroStarInfo.nextStarChipReq;
    return 0;
  }

  private string GetName()
  {
    if (this._parliamentHeroInfo != null)
      return this._parliamentHeroInfo.Name;
    return string.Empty;
  }

  private int GetHeroLevel()
  {
    if (this.IsHeroCard)
      return this._legendCardData.Level;
    return 1;
  }

  private string GetHeroLevelDescription()
  {
    return ScriptLocalization.GetWithPara("id_lv_num", new Dictionary<string, string>()
    {
      {
        "0",
        this.GetHeroLevel().ToString()
      }
    }, true);
  }

  private string GetHeroTitle()
  {
    if (this.IsHeroCard)
      return HeroCardUtils.GetHeroTitle(this._legendCardData.LegendId);
    return string.Empty;
  }
}
