﻿// Decompiled with JetBrains decompiler
// Type: GemSlotConditions
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using GemConstant;
using System;
using System.Collections.Generic;

public class GemSlotConditions
{
  private List<GemSlotConditions.Condition> list = new List<GemSlotConditions.Condition>();

  public void Parse(string content)
  {
    this.list.Clear();
    if (string.IsNullOrEmpty(content))
      return;
    string str1 = content;
    char[] chArray = new char[1]{ '|' };
    foreach (string str2 in str1.Split(chArray))
    {
      GemSlotConditions.Condition condition = new GemSlotConditions.Condition();
      string[] strArray = str2.Split('#');
      if (strArray.Length == 2)
      {
        int num = int.Parse(strArray[0]);
        condition.requiredLv = int.Parse(strArray[1]);
        if (Enum.IsDefined(typeof (GemType), (object) num))
        {
          condition.gemType = (GemType) num;
          this.list.Add(condition);
        }
        else
          D.error((object) "GemSlotConditions parse error. Not defined type {0}", (object) num);
      }
    }
  }

  public List<GemSlotConditions.Condition> ConditionList
  {
    get
    {
      return this.list;
    }
  }

  public struct Condition
  {
    public GemType gemType;
    public int requiredLv;
  }
}
