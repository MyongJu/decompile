﻿// Decompiled with JetBrains decompiler
// Type: TavernWheelGradeInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class TavernWheelGradeInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "group_id")]
  public int groupId;
  [Config(Name = "num")]
  public int num;
  [Config(Name = "grade")]
  public int grade;
  [Config(Name = "lucky")]
  public int lucky;
  [Config(Name = "lucky1_min")]
  public int lucky1Min;
  [Config(Name = "lucky1_max")]
  public int luck1Max;
  [Config(Name = "effect1")]
  public string effect1;
  [Config(Name = "weight_coe1")]
  public float weightCoe1;
  [Config(Name = "lucky2_min")]
  public int lucky2Min;
  [Config(Name = "lucky2_max")]
  public int luck2Max;
  [Config(Name = "effect2")]
  public string effect2;
  [Config(Name = "weight_coe2")]
  public float weightCoe2;
  [Config(Name = "lucky3_min")]
  public int lucky3Min;
  [Config(Name = "lucky3_max")]
  public int luck3Max;
  [Config(Name = "weight_coe3")]
  public float weightCoe3;
  [Config(Name = "effect3")]
  public string effect3;
  [Config(Name = "lucky4_min")]
  public int lucky4Min;
  [Config(Name = "lucky4_max")]
  public int luck4Max;
  [Config(Name = "weight_coe4")]
  public float weightCoe4;
  [Config(Name = "effect4")]
  public string effect4;
  [Config(Name = "lucky5_min")]
  public int lucky5Min;
  [Config(Name = "lucky5_max")]
  public int luck5Max;
  [Config(Name = "weight_coe5")]
  public float weightCoe5;
  [Config(Name = "effect5")]
  public string effect5;
  [Config(Name = "color")]
  public int color;

  public string GetEffectName(int lucky)
  {
    string str = string.Empty;
    if (lucky >= this.lucky1Min && lucky <= this.luck1Max)
      str = this.effect1;
    if (lucky >= this.lucky2Min && lucky <= this.luck2Max)
      str = this.effect2;
    if (lucky >= this.lucky3Min && lucky <= this.luck3Max)
      str = this.effect3;
    if (lucky >= this.lucky4Min && lucky <= this.luck4Max)
      str = this.effect4;
    if (lucky >= this.lucky5Min && lucky <= this.luck5Max)
      str = this.effect5;
    return str;
  }
}
