﻿// Decompiled with JetBrains decompiler
// Type: SystemMegaphone
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class SystemMegaphone : Hud
{
  public Queue<MegaphoneContent> messageQueue = new Queue<MegaphoneContent>();
  private List<MegaphoneContentItemRenerer> items = new List<MegaphoneContentItemRenerer>();
  private GameObjectPool pools = new GameObjectPool();
  private float moveSpeed = 300f;
  public UIGrid container;
  public UISprite background;
  public MegaphoneContentItemRenerer orgItem;
  public GameObject itemRoot;
  private int curId;
  private int displayCount;
  private bool init;

  public void Init()
  {
    if (this.init)
      return;
    this.init = true;
    this.gameObject.SetActive(false);
    this.displayCount = 0;
    this.pools.Initialize(this.orgItem.gameObject, this.itemRoot);
    this.messageQueue.Clear();
  }

  public void ShowMessage(string userName, string message)
  {
    this.Init();
    this.messageQueue.Enqueue(new MegaphoneContent(userName, message));
    if (!this.gameObject.activeSelf)
      this.Show();
    this.EnqueueDisplayQueue();
  }

  private void Show()
  {
    this.gameObject.SetActive(true);
  }

  private void Hide()
  {
    this.gameObject.SetActive(false);
  }

  private void EnqueueDisplayQueue()
  {
    if (this.items.Count >= 2)
      this.items[0].OnCloseHandler();
    this.ShowItem();
  }

  private void ShowItem()
  {
    if (this.messageQueue.Count <= 0)
      return;
    MegaphoneContent data = this.messageQueue.Dequeue();
    GameObject gameObject = this.pools.AddChild(this.container.gameObject);
    gameObject.SetActive(true);
    MegaphoneContentItemRenerer component = gameObject.GetComponent<MegaphoneContentItemRenerer>();
    component.OnFinish = new System.Action<MegaphoneContentItemRenerer>(this.FinishHandler);
    component.SetData(data);
    this.items.Add(component);
    this.container.repositionNow = true;
    this.container.Reposition();
  }

  private void FinishHandler(MegaphoneContentItemRenerer item)
  {
    if (this.items.Contains(item))
      this.items.Remove(item);
    this.pools.Release(item.gameObject);
    this.container.repositionNow = true;
    this.container.Reposition();
    --this.displayCount;
    if (this.messageQueue.Count <= 0)
      return;
    this.EnqueueDisplayQueue();
  }
}
