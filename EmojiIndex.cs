﻿// Decompiled with JetBrains decompiler
// Type: EmojiIndex
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class EmojiIndex : MonoBehaviour
{
  private List<UIToggle> toggleList = new List<UIToggle>();
  public UIGrid grid;
  public GameObject toggleTemplate;
  private int index;

  public int Index
  {
    set
    {
      this.index = value;
      this.toggleList.ForEach((System.Action<UIToggle>) (obj => obj.Set(false)));
      this.toggleList[this.index].Set(true);
    }
    get
    {
      return this.index;
    }
  }

  public void Init(int count)
  {
    this.toggleList.ForEach((System.Action<UIToggle>) (obj => obj.gameObject.SetActive(false)));
    for (int index = 0; index < count; ++index)
    {
      UIToggle uiToggle;
      if (index < this.toggleList.Count)
      {
        uiToggle = this.toggleList[index];
      }
      else
      {
        uiToggle = NGUITools.AddChild(this.grid.gameObject, this.toggleTemplate).GetComponent<UIToggle>();
        this.toggleList.Add(uiToggle);
      }
      uiToggle.gameObject.name = index.ToString();
      uiToggle.gameObject.SetActive(true);
    }
    this.grid.Reposition();
  }
}
