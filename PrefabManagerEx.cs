﻿// Decompiled with JetBrains decompiler
// Type: PrefabManagerEx
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class PrefabManagerEx : MonoBehaviour
{
  private Dictionary<string, CacheInfoEx> m_CacheDict = new Dictionary<string, CacheInfoEx>();
  private LinkedList m_RequestQueue = new LinkedList();
  private ObjectPool<PrefabSpawnRequestEx> m_RequestPool = new ObjectPool<PrefabSpawnRequestEx>();
  private static PrefabManagerEx m_Instance;

  private void Awake()
  {
    if (!(bool) ((UnityEngine.Object) PrefabManagerEx.m_Instance))
    {
      PrefabManagerEx.m_Instance = this;
    }
    else
    {
      D.error((object) "There must be only one PrefabManagerEx instance.");
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    }
  }

  private void OnDestroy()
  {
    PrefabManagerEx.m_Instance = (PrefabManagerEx) null;
  }

  public static PrefabManagerEx Instance
  {
    get
    {
      if ((UnityEngine.Object) PrefabManagerEx.m_Instance == (UnityEngine.Object) null)
        D.error((object) "PrefabManagerEx singleton is not created yet.");
      return PrefabManagerEx.m_Instance;
    }
  }

  public static bool IsAlive
  {
    get
    {
      return (UnityEngine.Object) PrefabManagerEx.m_Instance != (UnityEngine.Object) null;
    }
  }

  public void ClearCache()
  {
    this.Clear();
    GC.Collect();
    Resources.UnloadUnusedAssets();
  }

  public void AddRouteMapping(string prefabName, string assetPath)
  {
    if (this.m_CacheDict.ContainsKey(prefabName))
      return;
    this.m_CacheDict[prefabName] = new CacheInfoEx()
    {
      prefabName = prefabName,
      assetPath = assetPath
    };
  }

  public PrefabSpawnRequestEx SpawnAsync(string prefabName, Transform parent, PrefabSpawnRequestCallback callback, object userData = null)
  {
    CacheInfoEx cacheInfo = (CacheInfoEx) null;
    this.m_CacheDict.TryGetValue(prefabName, out cacheInfo);
    if (cacheInfo == null)
      return (PrefabSpawnRequestEx) null;
    PrefabSpawnRequestEx prefabSpawnRequestEx = (PrefabSpawnRequestEx) null;
    if (cacheInfo.HasCachedGameObject)
    {
      GameObject go = this.InternalSpawnGameObject(cacheInfo, parent);
      callback(go, userData);
    }
    else
    {
      prefabSpawnRequestEx = this.m_RequestPool.Allocate();
      prefabSpawnRequestEx.Reset();
      prefabSpawnRequestEx.cacheInfo = cacheInfo;
      prefabSpawnRequestEx.callback = callback;
      prefabSpawnRequestEx.userData = userData;
      prefabSpawnRequestEx.parent = parent;
      this.m_RequestQueue.PushBack((LinkedList.Node) prefabSpawnRequestEx);
    }
    return prefabSpawnRequestEx;
  }

  public PrefabSpawnRequestEx SpawnAsync(GameObject prefab, Transform parent, PrefabSpawnRequestCallback callback, object userData = null)
  {
    return this.SpawnAsync(this.FindOrRegisterCacheInfo(prefab).prefabName, parent, callback, userData);
  }

  public GameObject Spawn(GameObject prefab, Transform parent)
  {
    CacheInfoEx registerCacheInfo = this.FindOrRegisterCacheInfo(prefab);
    if (registerCacheInfo != null)
      return this.InternalSpawnGameObject(registerCacheInfo, parent);
    D.warn((object) "Failed to find or register cache info.");
    return (GameObject) null;
  }

  public GameObject Spawn(string prefabName, Transform parent)
  {
    CacheInfoEx orRemapCacheInfo = this.FindOrRemapCacheInfo(prefabName);
    if (orRemapCacheInfo != null)
      return this.InternalSpawnGameObject(orRemapCacheInfo, parent);
    D.warn((object) string.Format("{0}.prefab not found in cache list.", (object) prefabName));
    return (GameObject) null;
  }

  public void Destroy(GameObject go)
  {
    if (!(bool) ((UnityEngine.Object) go))
      return;
    CacheInfoEx cacheInfoEx = (CacheInfoEx) null;
    this.m_CacheDict.TryGetValue(go.name, out cacheInfoEx);
    if (cacheInfoEx == null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) go);
    }
    else
    {
      go.SetActive(false);
      cacheInfoEx.Recycle(go);
    }
  }

  public void CancelAllSpawnRequests()
  {
    while (!this.m_RequestQueue.IsEmpty())
    {
      PrefabSpawnRequestEx o = this.m_RequestQueue.PopFront() as PrefabSpawnRequestEx;
      o.cancel = true;
      this.m_RequestPool.Release(o);
    }
  }

  private void Update()
  {
    this.ProcessSingleSpawnRequest();
  }

  private void ProcessSingleSpawnRequest()
  {
    while (!this.m_RequestQueue.IsEmpty())
    {
      PrefabSpawnRequestEx o = this.m_RequestQueue.PopFront() as PrefabSpawnRequestEx;
      if (o.cancel)
      {
        this.m_RequestPool.Release(o);
      }
      else
      {
        if (o.cacheInfo.state == CacheInfoExState.NotReady)
        {
          o.cacheInfo.state = CacheInfoExState.Loading;
          AssetManager.Instance.LoadAsync(o.cacheInfo.assetPath, new System.Action<UnityEngine.Object, bool>(this.OnAssetLoaded), (System.Type) null);
          this.m_RequestQueue.PushBack((LinkedList.Node) o);
          break;
        }
        if (o.cacheInfo.state == CacheInfoExState.Loading)
        {
          this.m_RequestQueue.PushBack((LinkedList.Node) o);
          break;
        }
        GameObject go = this.InternalSpawnGameObject(o.cacheInfo, o.parent);
        if (o.callback != null)
          o.callback(go, o.userData);
        this.m_RequestPool.Release(o);
        break;
      }
    }
  }

  private void OnAssetLoaded(UnityEngine.Object o, bool ret)
  {
    GameObject prefab = o as GameObject;
    if (!(bool) ((UnityEngine.Object) prefab))
      return;
    CacheInfoEx cacheInfoEx = (CacheInfoEx) null;
    this.m_CacheDict.TryGetValue(prefab.name, out cacheInfoEx);
    if (cacheInfoEx != null)
    {
      cacheInfoEx.Init(prefab, this.transform);
      cacheInfoEx.state = CacheInfoExState.Ready;
    }
    else
      D.error((object) "[PrefabManagerEx]OnAssetLoaded: Cache for {0} doesn't exist.", (object) prefab.name);
  }

  private GameObject InternalSpawnGameObject(CacheInfoEx cacheInfo, Transform parent)
  {
    if (cacheInfo == null)
      return (GameObject) null;
    GameObject gameObject = cacheInfo.Allocate();
    gameObject.transform.parent = parent;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    return gameObject;
  }

  private void Clear()
  {
    using (Dictionary<string, CacheInfoEx>.Enumerator enumerator = this.m_CacheDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.Clear();
    }
    this.m_CacheDict = new Dictionary<string, CacheInfoEx>();
    this.m_RequestQueue = new LinkedList();
    this.m_RequestPool = new ObjectPool<PrefabSpawnRequestEx>();
  }

  private CacheInfoEx FindOrRegisterCacheInfo(GameObject prefab)
  {
    if (!(bool) ((UnityEngine.Object) prefab))
      return (CacheInfoEx) null;
    string name = prefab.name;
    CacheInfoEx cacheInfoEx = (CacheInfoEx) null;
    this.m_CacheDict.TryGetValue(name, out cacheInfoEx);
    if (cacheInfoEx == null)
    {
      cacheInfoEx = new CacheInfoEx();
      cacheInfoEx.prefabName = name;
      cacheInfoEx.assetPath = string.Empty;
      cacheInfoEx.state = CacheInfoExState.Ready;
      this.m_CacheDict[cacheInfoEx.prefabName] = cacheInfoEx;
    }
    if (!(bool) ((UnityEngine.Object) cacheInfoEx.prefab))
      cacheInfoEx.Init(prefab, this.transform);
    return cacheInfoEx;
  }

  private CacheInfoEx FindOrRemapCacheInfo(string prefabName)
  {
    CacheInfoEx cacheInfoEx = (CacheInfoEx) null;
    this.m_CacheDict.TryGetValue(prefabName, out cacheInfoEx);
    if (cacheInfoEx != null)
    {
      GameObject prefab = AssetManager.Instance.Load(cacheInfoEx.assetPath, (System.Type) null) as GameObject;
      if ((bool) ((UnityEngine.Object) prefab))
      {
        cacheInfoEx.Init(prefab, this.transform);
        cacheInfoEx.state = CacheInfoExState.Ready;
      }
    }
    return cacheInfoEx;
  }
}
