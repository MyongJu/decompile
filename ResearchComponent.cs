﻿// Decompiled with JetBrains decompiler
// Type: ResearchComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResearchComponent : ComponentRenderBase
{
  public GameObject parent;
  public GameObject element;
  public GameObject container;

  public override void Init()
  {
    base.Init();
    Hashtable researchTable = (this.data as ResearchComponentData).researchTable;
    List<ResearchComponent.ReaearchElement> reaearchElementList = new List<ResearchComponent.ReaearchElement>();
    foreach (DictionaryEntry dictionaryEntry in researchTable)
      reaearchElementList.Add(new ResearchComponent.ReaearchElement(dictionaryEntry.Key.ToString(), float.Parse(dictionaryEntry.Value.ToString())));
    reaearchElementList.Sort((Comparison<ResearchComponent.ReaearchElement>) ((x, y) => ConfigManager.inst.DB_Properties[DBManager.inst.DB_Local_Benefit.GetLongID(x.key)].Priority.CompareTo(ConfigManager.inst.DB_Properties[DBManager.inst.DB_Local_Benefit.GetLongID(y.key)].Priority)));
    GameObject parent = NGUITools.AddChild(this.parent, this.container);
    int index = 0;
    while (index < reaearchElementList.Count)
    {
      GameObject gameObject = NGUITools.AddChild(parent, this.element);
      PropertyDefinition dbProperty1 = ConfigManager.inst.DB_Properties[DBManager.inst.DB_Local_Benefit.GetLongID(reaearchElementList[index].key)];
      gameObject.transform.Find("key0").GetComponent<UILabel>().text = dbProperty1.Name;
      gameObject.transform.Find("value0").GetComponent<UILabel>().text = dbProperty1.ConvertToDisplayString((double) reaearchElementList[index].value, true, true);
      if (index + 1 < reaearchElementList.Count)
      {
        PropertyDefinition dbProperty2 = ConfigManager.inst.DB_Properties[DBManager.inst.DB_Local_Benefit.GetLongID(reaearchElementList[index + 1].key)];
        gameObject.transform.Find("key1").GetComponent<UILabel>().text = dbProperty2.Name;
        gameObject.transform.Find("value1").GetComponent<UILabel>().text = dbProperty2.ConvertToDisplayString((double) reaearchElementList[index + 1].value, true, true);
      }
      else
      {
        gameObject.transform.Find("key1").gameObject.SetActive(false);
        gameObject.transform.Find("value1").gameObject.SetActive(false);
      }
      index += 2;
    }
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private class ReaearchElement
  {
    public string key;
    public float value;

    public ReaearchElement(string key, float value)
    {
      this.key = key;
      this.value = value;
    }
  }
}
