﻿// Decompiled with JetBrains decompiler
// Type: ConfigProperties
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigProperties
{
  private Dictionary<int, PropertyDefinition> Properties = new Dictionary<int, PropertyDefinition>();
  private Dictionary<string, string> ShortKey2ID = new Dictionary<string, string>();
  private Dictionary<string, int> PropertyIDs = new Dictionary<string, int>();

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "{0} load error - invalid hashtable", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "{0} load error - invalid header hashtable", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "type");
        int index3 = ConfigManager.GetIndex(inHeader, "loc_key");
        int index4 = ConfigManager.GetIndex(inHeader, "target_image");
        int index5 = ConfigManager.GetIndex(inHeader, "image");
        int index6 = ConfigManager.GetIndex(inHeader, "negative");
        int index7 = ConfigManager.GetIndex(inHeader, "priority");
        int index8 = ConfigManager.GetIndex(inHeader, "short_key");
        int index9 = ConfigManager.GetIndex(inHeader, "category");
        int[] numArray = new int[5]
        {
          ConfigManager.GetIndex(inHeader, "subproperty_1"),
          ConfigManager.GetIndex(inHeader, "subproperty_2"),
          ConfigManager.GetIndex(inHeader, "subproperty_3"),
          ConfigManager.GetIndex(inHeader, "subproperty_4"),
          ConfigManager.GetIndex(inHeader, "subproperty_5")
        };
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          int result;
          if (int.TryParse(key.ToString(), out result))
          {
            ArrayList arr = hashtable[key] as ArrayList;
            if (arr != null)
            {
              PropertyDefinition.FormatType formatType = PropertyDefinition.ParseFormatType(ConfigManager.GetString(arr, index2));
              if (formatType == PropertyDefinition.FormatType.Invalid)
                D.error((object) "Bad game data. Invalid type assigned to property {0}: '{1}'", (object) ConfigManager.GetString(arr, index1), (object) ConfigManager.GetString(arr, index2));
              else if (!ConfigManager.GetString(arr, index1).Contains("_value") && !ConfigManager.GetString(arr, index1).Contains("_percent"))
              {
                D.error((object) "Bad game data. Invalid ID format for property: {0}", (object) ConfigManager.GetString(arr, index1));
              }
              else
              {
                PropertyDefinition property = new PropertyDefinition(result, ConfigManager.GetString(arr, index1), formatType, ConfigManager.GetString(arr, index3), ConfigManager.GetString(arr, index4), ConfigManager.GetString(arr, index5), ConfigManager.GetBool(arr, index6), ConfigManager.GetInt(arr, index7), ConfigManager.GetString(arr, index8), ConfigManager.GetString(arr, index9));
                this.Add(property);
                for (int index10 = 0; index10 < numArray.Length; ++index10)
                  property.AddSubproperty(ConfigManager.GetInt(arr, numArray[index10]));
              }
            }
          }
        }
      }
    }
  }

  public Dictionary<int, PropertyDefinition>.ValueCollection Values
  {
    get
    {
      return this.Properties.Values;
    }
  }

  private void Add(PropertyDefinition property)
  {
    if (this.Properties.ContainsKey(property.InternalID))
      return;
    this.Properties.Add(property.InternalID, property);
    if (!this.PropertyIDs.ContainsKey(property.ID))
      this.PropertyIDs.Add(property.ID, property.InternalID);
    if (!string.IsNullOrEmpty(property.ShortKey) && !this.ShortKey2ID.ContainsKey(property.ShortKey))
      this.ShortKey2ID.Add(property.ShortKey, property.ID);
    ConfigManager.inst.AddData(property.InternalID, (object) property);
  }

  public void Clear()
  {
    if (this.Properties != null)
      this.Properties.Clear();
    if (this.PropertyIDs == null)
      return;
    this.PropertyIDs.Clear();
  }

  public PropertyDefinition this[int internalID]
  {
    get
    {
      if (this.Properties.ContainsKey(internalID))
        return this.Properties[internalID];
      return (PropertyDefinition) null;
    }
  }

  public string GetIDByShortKey(string key)
  {
    if (this.ShortKey2ID.ContainsKey(key))
      return this.ShortKey2ID[key];
    return string.Empty;
  }

  public PropertyDefinition this[string ID]
  {
    get
    {
      if (this.PropertyIDs.ContainsKey(ID))
        return this.Properties[this.PropertyIDs[ID]];
      return (PropertyDefinition) null;
    }
  }

  public bool Contains(string ID)
  {
    return this.PropertyIDs.ContainsKey(ID);
  }

  public bool Contains(int internalID)
  {
    return this.Properties.ContainsKey(internalID);
  }
}
