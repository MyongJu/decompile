﻿// Decompiled with JetBrains decompiler
// Type: ConfigParliament
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigParliament
{
  private List<ParliamentInfo> _parliamentInfoList = new List<ParliamentInfo>();
  private Dictionary<string, ParliamentInfo> _datas;
  private Dictionary<int, ParliamentInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ParliamentInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, ParliamentInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._parliamentInfoList.Add(enumerator.Current);
    }
    this._parliamentInfoList.Sort((Comparison<ParliamentInfo>) ((a, b) => a.priority.CompareTo(b.priority)));
  }

  public List<ParliamentInfo> GetParliamentInfoList()
  {
    return this._parliamentInfoList;
  }

  public ParliamentInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (ParliamentInfo) null;
  }

  public ParliamentInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (ParliamentInfo) null;
  }
}
