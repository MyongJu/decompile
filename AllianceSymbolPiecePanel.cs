﻿// Decompiled with JetBrains decompiler
// Type: AllianceSymbolPiecePanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AllianceSymbolPiecePanel : MonoBehaviour
{
  private GameObjectPool m_ItemPool = new GameObjectPool();
  private List<GameObject> m_ItemList = new List<GameObject>();
  private int m_SelectIndex = -1;
  [SerializeField]
  private UIWrapContent m_WrapContent;
  [SerializeField]
  private UIScrollView m_ScrollView;
  [SerializeField]
  private GameObject m_Renderer;
  [SerializeField]
  private UICenterOnChild m_Center;
  private System.Action<GameObject> m_Callback;
  private bool m_NeedUpdate;
  private string[] m_Sprites;

  public void Initialize(string[] sprites, int selectedIndex, System.Action<GameObject> callback)
  {
    this.m_Callback = callback;
    this.m_ItemPool.Initialize(this.m_Renderer, this.gameObject);
    this.m_NeedUpdate = true;
    this.m_SelectIndex = selectedIndex;
    this.m_Sprites = sprites;
  }

  private void Update()
  {
    if (!this.m_NeedUpdate)
      return;
    this.m_NeedUpdate = false;
    this.UpdateData();
  }

  private void UpdateData()
  {
    for (int index = 0; index < this.m_Sprites.Length; ++index)
    {
      GameObject gameObject = this.m_ItemPool.AddChild(this.m_WrapContent.gameObject);
      gameObject.SetActive(true);
      gameObject.GetComponent<UISprite>().spriteName = this.m_Sprites[index];
      gameObject.GetComponent<AllianceSymbolPiece>().index = index;
      this.m_ItemList.Add(gameObject);
    }
    this.m_WrapContent.SortBasedOnScrollMovement();
    this.m_WrapContent.WrapContent();
    this.SetSelectedIndex(this.m_SelectIndex);
    this.m_Center.onCenter = new UICenterOnChild.OnCenterCallback(this.OnCenter);
  }

  private void SetSelectedIndex(int selectedIndex)
  {
    this.CenterOn(this.m_ItemList[selectedIndex].transform);
    this.m_WrapContent.WrapContent();
  }

  private void OnCenter(GameObject go)
  {
    if (!((UnityEngine.Object) go != (UnityEngine.Object) null) || this.m_Callback == null)
      return;
    this.m_Callback(go);
  }

  private void CenterOn(Transform target, Vector3 panelCenter)
  {
    if (!((UnityEngine.Object) target != (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_ScrollView != (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_ScrollView.panel != (UnityEngine.Object) null))
      return;
    Transform cachedTransform = this.m_ScrollView.panel.cachedTransform;
    Vector3 vector3 = cachedTransform.InverseTransformPoint(target.position) - cachedTransform.InverseTransformPoint(panelCenter);
    if (!this.m_ScrollView.canMoveHorizontally)
      vector3.x = 0.0f;
    if (!this.m_ScrollView.canMoveVertically)
      vector3.y = 0.0f;
    vector3.z = 0.0f;
    cachedTransform.localPosition -= vector3;
    this.OnCenter(target.gameObject);
  }

  private void CenterOn(Transform target)
  {
    if (!((UnityEngine.Object) this.m_ScrollView != (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_ScrollView.panel != (UnityEngine.Object) null))
      return;
    Vector3[] worldCorners = this.m_ScrollView.panel.worldCorners;
    Vector3 panelCenter = (worldCorners[2] + worldCorners[0]) * 0.5f;
    this.CenterOn(target, panelCenter);
  }
}
