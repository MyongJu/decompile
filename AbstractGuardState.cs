﻿// Decompiled with JetBrains decompiler
// Type: AbstractGuardState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public abstract class AbstractGuardState : IState
{
  private RoundPlayer _player;

  public AbstractGuardState(RoundPlayer player)
  {
    this._player = player;
  }

  public RoundPlayer Player
  {
    get
    {
      return this._player;
    }
  }

  public abstract string Key { get; }

  public abstract Hashtable Data { get; set; }

  public abstract void OnEnter();

  public abstract void OnProcess();

  public abstract void OnExit();

  public abstract void Dispose();
}
