﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentScrollCombineInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ConfigEquipmentScrollCombineInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "scroll_chip_id")]
  public int scrollChipID;
  [Config(Name = "scroll_id")]
  public int scrollID;
  [Config(Name = "chance")]
  public int chance;
}
