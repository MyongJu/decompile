﻿// Decompiled with JetBrains decompiler
// Type: BuildingComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingComponent : ComponentRenderBase
{
  public GameObject parent;
  public GameObject element;
  public GameObject container;

  public override void Init()
  {
    base.Init();
    BuildingComponentData data = this.data as BuildingComponentData;
    if (data == null)
      return;
    Hashtable buildingTable = data.buildingTable;
    List<BuildingComponent.BuildingElement> buildingElementList = new List<BuildingComponent.BuildingElement>();
    if (buildingElementList == null)
      return;
    foreach (DictionaryEntry dictionaryEntry in buildingTable)
      buildingElementList.Add(new BuildingComponent.BuildingElement(dictionaryEntry.Key.ToString(), float.Parse(dictionaryEntry.Value.ToString())));
    buildingElementList.Sort((Comparison<BuildingComponent.BuildingElement>) ((x, y) => ConfigManager.inst.DB_Properties[DBManager.inst.DB_Local_Benefit.GetLongID(x.key)].Priority.CompareTo(ConfigManager.inst.DB_Properties[DBManager.inst.DB_Local_Benefit.GetLongID(y.key)].Priority)));
    GameObject parent = NGUITools.AddChild(this.parent, this.container);
    int index = 0;
    while (index < buildingElementList.Count)
    {
      GameObject gameObject = NGUITools.AddChild(parent, this.element);
      PropertyDefinition dbProperty1 = ConfigManager.inst.DB_Properties[DBManager.inst.DB_Local_Benefit.GetLongID(buildingElementList[index].key)];
      gameObject.transform.Find("key0").GetComponent<UILabel>().text = dbProperty1.Name;
      gameObject.transform.Find("value0").GetComponent<UILabel>().text = dbProperty1.ConvertToDisplayString((double) buildingElementList[index].value, true, true);
      if (index + 1 < buildingElementList.Count)
      {
        PropertyDefinition dbProperty2 = ConfigManager.inst.DB_Properties[DBManager.inst.DB_Local_Benefit.GetLongID(buildingElementList[index + 1].key)];
        gameObject.transform.Find("key1").GetComponent<UILabel>().text = dbProperty2.Name;
        gameObject.transform.Find("value1").GetComponent<UILabel>().text = dbProperty2.ConvertToDisplayString((double) buildingElementList[index + 1].value, true, true);
      }
      else
      {
        gameObject.transform.Find("key1").gameObject.SetActive(false);
        gameObject.transform.Find("value1").gameObject.SetActive(false);
      }
      index += 2;
    }
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private class BuildingElement
  {
    public string key;
    public float value;

    public BuildingElement(string key, float value)
    {
      this.key = key;
      this.value = value;
    }
  }
}
