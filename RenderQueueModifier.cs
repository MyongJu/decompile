﻿// Decompiled with JetBrains decompiler
// Type: RenderQueueModifier
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RenderQueueModifier : MonoBehaviour
{
  public UIWidget m_target;
  public RenderQueueModifier.RenderType m_type;
  public bool m_updateInScene;
  private Renderer[] _renderers;
  private int _lastQueue;

  private void Start()
  {
    this._renderers = this.GetComponentsInChildren<Renderer>();
  }

  private void OnDrawGizmos()
  {
    if (!this.m_updateInScene)
      return;
    this._lastQueue = 0;
    this._renderers = this.GetComponentsInChildren<Renderer>();
    this.m_updateInScene = false;
    this.UpdateRenderOrder(true);
  }

  private void UpdateRenderOrder(bool editMode)
  {
    if ((Object) this.m_target == (Object) null || (Object) this.m_target.drawCall == (Object) null)
      return;
    int num = this.m_target.drawCall.renderQueue + (this.m_type != RenderQueueModifier.RenderType.FRONT ? -1 : 1);
    if (this._lastQueue == num)
      return;
    this._lastQueue = num;
    foreach (Renderer renderer in this._renderers)
    {
      if (!editMode)
        renderer.material.renderQueue = this._lastQueue;
      else
        renderer.sharedMaterial.renderQueue = this._lastQueue;
    }
  }

  private void Update()
  {
    this.UpdateRenderOrder(false);
  }

  public enum RenderType
  {
    FRONT,
    BACK,
  }
}
