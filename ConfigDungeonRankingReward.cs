﻿// Decompiled with JetBrains decompiler
// Type: ConfigDungeonRankingReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigDungeonRankingReward
{
  private List<DungeonRankingRewardInfo> dungeonRankingRewardInfoList = new List<DungeonRankingRewardInfo>();
  private Dictionary<string, DungeonRankingRewardInfo> datas;
  private Dictionary<int, DungeonRankingRewardInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<DungeonRankingRewardInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    Dictionary<string, DungeonRankingRewardInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this.dungeonRankingRewardInfoList.Add(enumerator.Current);
    }
    this.dungeonRankingRewardInfoList.Sort((Comparison<DungeonRankingRewardInfo>) ((x, y) => x.ranking.CompareTo(y.ranking)));
  }

  public List<DungeonRankingRewardInfo> GetDungeonRankingRewardInfoList()
  {
    return this.dungeonRankingRewardInfoList;
  }

  public DungeonRankingRewardInfo Get(int interalId)
  {
    if (this.dicByUniqueId.ContainsKey(interalId))
      return this.dicByUniqueId[interalId];
    return (DungeonRankingRewardInfo) null;
  }

  public DungeonRankingRewardInfo Get(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (DungeonRankingRewardInfo) null;
  }
}
