﻿// Decompiled with JetBrains decompiler
// Type: LanguageSelectPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class LanguageSelectPopup : Popup
{
  private List<UIToggle> languageToggles = new List<UIToggle>();
  private List<UIToggle> translatorToggles = new List<UIToggle>();
  private int _selectedIndex = -1;
  private int _languageIndex = -1;
  private int _translatorIndex = -1;
  public UIButton closeBtn;
  public UIButton confirm;
  public GameObject languageTemplate;
  public UIGrid languageGrid;
  public GameObject translatorTemplate;
  public UIGrid translatorGrid;
  public UITable languageTable;
  public UIScrollView languageScrollView;
  private bool isDestroy;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    EventDelegate.Add(this.closeBtn.onClick, new EventDelegate.Callback(this.onCloseBtn));
    EventDelegate.Add(this.confirm.onClick, new EventDelegate.Callback(this.onConfirmBtn));
    this.UpdateLanguages();
    this._languageIndex = this.GetLanguageIndexByLocale(LocalizationManager.CurrentLanguageCode);
    this._languageIndex = this._languageIndex >= 0 ? this._languageIndex : this.GetLanguageIndexByLocale("en");
    this.languageToggles[this._languageIndex].Set(true);
    this.languageTable.Reposition();
    this.languageScrollView.ResetPosition();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    EventDelegate.Remove(this.closeBtn.onClick, new EventDelegate.Callback(this.onCloseBtn));
    EventDelegate.Remove(this.confirm.onClick, new EventDelegate.Callback(this.onConfirmBtn));
    base.OnClose(orgParam);
    this.ClearLanguages();
    this.ClearTranslators();
  }

  private void onCloseBtn()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  private void onConfirmBtn()
  {
    AllianceLanguageRenderer component = this.languageToggles[this.GetSelectedLanguageIndex()].GetComponent<AllianceLanguageRenderer>();
    if (ModController.OnLanguageSelect != null)
      ModController.OnLanguageSelect(component.m_Locale);
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  private void UpdateLanguages()
  {
    this.ClearLanguages();
    List<string> localization = Language.Instance.Localization;
    for (int index = 0; index < localization.Count; ++index)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.languageTemplate);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.languageGrid.transform;
      gameObject.transform.localScale = Vector3.one;
      gameObject.GetComponent<AllianceLanguageRenderer>().Set(localization[index]);
      this.languageToggles.Add(gameObject.GetComponent<UIToggle>());
    }
    this.languageGrid.Reposition();
  }

  private void ClearLanguages()
  {
    using (List<UIToggle>.Enumerator enumerator = this.languageToggles.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UIToggle current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.languageToggles.Clear();
  }

  private void OnDestroy()
  {
    this.isDestroy = true;
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
  }

  private void UpdateTranslators()
  {
    this.ClearTranslators();
    List<string> translator = Language.Instance.Translator;
    for (int index = 0; index < translator.Count; ++index)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.translatorTemplate);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.translatorGrid.transform;
      gameObject.transform.localScale = Vector3.one;
      gameObject.GetComponent<AllianceLanguageRenderer>().Set(translator[index]);
      this.translatorToggles.Add(gameObject.GetComponent<UIToggle>());
    }
    this.translatorGrid.Reposition();
  }

  private void ClearTranslators()
  {
    using (List<UIToggle>.Enumerator enumerator = this.translatorToggles.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UIToggle current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.translatorToggles.Clear();
  }

  private int GetLanguageIndexByLocale(string locale)
  {
    for (int index = 0; index < this.languageToggles.Count; ++index)
    {
      if (this.languageToggles[index].GetComponent<AllianceLanguageRenderer>().m_Locale == locale)
        return index;
    }
    return -1;
  }

  private int GetTranslatorIndexByLocale(string locale)
  {
    for (int index = 0; index < this.translatorToggles.Count; ++index)
    {
      if (this.translatorToggles[index].GetComponent<AllianceLanguageRenderer>().m_Locale == locale)
        return index;
    }
    return -1;
  }

  private int GetSelectedLanguageIndex()
  {
    for (int index = 0; index < this.languageToggles.Count; ++index)
    {
      if (this.languageToggles[index].value)
        return index;
    }
    return -1;
  }

  private int GetSelectedTranslatorIndex()
  {
    for (int index = 0; index < this.translatorToggles.Count; ++index)
    {
      if (this.translatorToggles[index].value)
        return index;
    }
    return -1;
  }

  public void OnToggleChanged()
  {
    int languageIndex = this.GetSelectedLanguageIndex();
    if (this._languageIndex == languageIndex || languageIndex == -1)
      return;
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = ScriptLocalization.Get("alliance_confirm_title", true),
      content = ScriptLocalization.Get("settings_changed_restart_game_prompt", true),
      yes = ScriptLocalization.Get("id_uppercase_yes", true),
      no = ScriptLocalization.Get("id_uppercase_no", true),
      yesCallback = (System.Action) (() =>
      {
        AllianceLanguageRenderer item = this.languageToggles[languageIndex].GetComponent<AllianceLanguageRenderer>();
        Hashtable postData = new Hashtable();
        postData[(object) "lang"] = (object) item.m_Locale;
        MessageHub.inst.GetPortByAction("Player:setLanguage").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          this._languageIndex = languageIndex;
          LocalizationManager.CurrentLanguageCode = item.m_Locale;
          GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Deep);
        }), true);
      }),
      noCallback = (System.Action) (() => this.languageToggles[this._languageIndex].Set(true))
    });
  }

  public void OnTranslatorChanged()
  {
    int selectedTranslatorIndex = this.GetSelectedTranslatorIndex();
    if (this._translatorIndex == selectedTranslatorIndex || selectedTranslatorIndex == -1)
      return;
    this._translatorIndex = selectedTranslatorIndex;
    PlayerPrefs.SetString("Translator Locale", this.translatorToggles[selectedTranslatorIndex].GetComponent<AllianceLanguageRenderer>().m_Locale);
  }

  public void OnViewNews()
  {
    Utils.PopUpCommingSoon();
  }

  public void OnViewCommunity()
  {
    UIManager.inst.OpenDlg("Community/CommunityDialog", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnGiftExchange()
  {
    UIManager.inst.OpenPopup("GiftExchange/GiftExchangePopup", (Popup.PopupParameter) null);
  }

  public void OnTermsOfService()
  {
    UIManager.inst.OpenDlg("Community/TermsOfServiceDialog", (UI.Dialog.DialogParameter) new TermsOfServiceDialog.Parameter()
    {
      title = "settings_uppercase_terms_of_service",
      description = "settings_terms_of_service_description"
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnPrivacy()
  {
    UIManager.inst.OpenDlg("Community/TermsOfServiceDialog", (UI.Dialog.DialogParameter) new TermsOfServiceDialog.Parameter()
    {
      title = "settings_uppercase_privacy_policy",
      description = "settings_privacy_policy_description"
    }, 1 != 0, 1 != 0, 1 != 0);
  }
}
