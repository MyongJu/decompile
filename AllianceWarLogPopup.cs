﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarLogPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceWarLogPopup : Popup
{
  private bool shouldLoadMore;
  [SerializeField]
  private AllianceWarLogPopup.Panel panel;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.panel.scrollView.onStoppedMoving += new UIScrollView.OnDragNotification(this.OnStoppedMoving);
    this.panel.scrollView.onDragFinished += new UIScrollView.OnDragNotification(this.OnDragFinished);
    PlayerData.inst.allianceWarManager.LoadLogs(0L, new System.Action(this.FirstRefresh));
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.panel.scrollView.onStoppedMoving -= new UIScrollView.OnDragNotification(this.OnStoppedMoving);
    this.panel.scrollView.onDragFinished -= new UIScrollView.OnDragNotification(this.OnDragFinished);
  }

  public void FirstRefresh()
  {
    this.RefreshPopup();
    this.panel.panel.clipOffset = new Vector2(0.0f, 0.0f);
    this.panel.panel.transform.localPosition = new Vector3(0.0f, -70f, 0.0f);
  }

  public void RefreshPopup()
  {
    if (this._state.currentState < 3)
      return;
    this.panel.waitIcon.gameObject.SetActive(false);
    if (PlayerData.inst.allianceWarManager.logs.Count > this.panel.slots.Count)
    {
      UIWidget component = this.panel.orgSlot.GetComponent<UIWidget>();
      for (int count = this.panel.slots.Count; count < PlayerData.inst.allianceWarManager.logs.Count; ++count)
      {
        GameObject gameObject = NGUITools.AddChild(this.panel.wrapContent.gameObject, this.panel.orgSlot);
        gameObject.SetActive(true);
        gameObject.transform.localPosition = new Vector3(0.0f, (float) (-component.height * count), 0.0f);
        this.panel.slots.Add(gameObject.GetComponent<AllianceWarLogSlot>());
      }
    }
    for (int index = 0; index < PlayerData.inst.allianceWarManager.logs.Count; ++index)
    {
      this.panel.slots[index].gameObject.SetActive(true);
      this.panel.slots[index].Setup(PlayerData.inst.allianceWarManager.logs[index]);
    }
    for (int count = PlayerData.inst.allianceWarManager.logs.Count; count < this.panel.slots.Count; ++count)
      this.panel.slots[count].gameObject.SetActive(false);
  }

  public void DragRefresh()
  {
    PlayerData.inst.allianceWarManager.LoadLogs((long) PlayerData.inst.allianceWarManager.logs.Count, new System.Action(this.RefreshPopup));
    this.panel.waitIcon.gameObject.SetActive(true);
  }

  public void OnInitSlot(GameObject go, int wrapIndex, int realIndex)
  {
    if (Mathf.Abs(realIndex) >= PlayerData.inst.allianceWarManager.logs.Count)
      return;
    go.GetComponent<AllianceWarLogSlot>().Setup(PlayerData.inst.allianceWarManager.logs[Mathf.Abs(realIndex)]);
  }

  public void OnUpdateLog()
  {
    PlayerData.inst.allianceWarManager.LoadLogs(PlayerData.inst.allianceWarManager.lastLogIndex, new System.Action(this.RefreshPopup));
    this.panel.waitIcon.gameObject.SetActive(true);
  }

  private void OnStoppedMoving()
  {
    if (!this.shouldLoadMore)
      return;
    this.shouldLoadMore = false;
    this.DragRefresh();
  }

  public void OnDragFinished()
  {
    Bounds bounds = this.panel.scrollView.bounds;
    Vector3 constrainOffset = this.panel.scrollView.panel.CalculateConstrainOffset((Vector2) bounds.min, (Vector2) bounds.max);
    if (!this.panel.scrollView.canMoveHorizontally)
      constrainOffset.x = 0.0f;
    if (!this.panel.scrollView.canMoveVertically)
      constrainOffset.y = 0.0f;
    if ((double) constrainOffset.y >= -200.0)
      return;
    this.shouldLoadMore = true;
  }

  public void OnCloseButtonClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void Reset()
  {
    this.panel.waitIcon = this.transform.Find("Background/Btn_WaitIcon").gameObject.transform;
    this.panel.BT_Close = this.transform.Find("Background/Btn_Close").gameObject.GetComponent<UIButton>();
    GameObject gameObject = this.transform.Find("Panel").gameObject;
    this.panel.panel = gameObject.GetComponent<UIPanel>();
    this.panel.scrollView = gameObject.GetComponent<UIScrollView>();
    this.panel.wrapContent = this.transform.Find("Panel/UIWrap Content").gameObject;
    this.panel.orgSlot = this.transform.Find("Panel/SlotOrg").gameObject;
  }

  [Serializable]
  public class Panel
  {
    public List<AllianceWarLogSlot> slots = new List<AllianceWarLogSlot>();
    public const int MAX_SLOT_COUNT = 10;
    public Transform waitIcon;
    public UIButton BT_Close;
    public UIPanel panel;
    public UIScrollView scrollView;
    public GameObject wrapContent;
    public GameObject orgSlot;
  }
}
