﻿// Decompiled with JetBrains decompiler
// Type: AuctionHallRecordInfoPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AuctionHallRecordInfoPopup : Popup
{
  private List<EquipmentSuitItem> allEquipmentSuitItem = new List<EquipmentSuitItem>();
  private List<EquipmentGemItem> _allEquipmentGemItem = new List<EquipmentGemItem>();
  public GameObject normalItemNode;
  public GameObject equipmentItemNode;
  public GameObject equipmentScrollItemNode;
  public GameObject artifactItemNode;
  public UITexture auctionItemTexture;
  public UITexture auctionItemBg;
  public UILabel auctionItemName;
  public UILabel auctionItemDesc;
  public EquipmentBenefits benefitContent;
  public EquipmentComponent equipComponent;
  public EquipmentBenefits benefitScrollContent;
  public EquipmentComponent equipScrollComponent;
  public ItemIconRenderer equipScrollItemRenderer;
  public UILabel equipScrollName;
  public UITexture artifactTexture;
  public UILabel artifactName;
  public UILabel artifactProtectTime;
  public UILabel artifactDesc;
  public EquipmentBenefits artifactBenefitContent;
  public UIScrollView artifactDetailScrollView;
  public UITable artifactDetailTable;
  public Transform equipmentSuitConainerForScroll;
  public Transform equipmentGemConainerForScroll;
  public Transform equipmentSuitContainerForEquipment;
  public Transform equipmentGemContainerForEquipment;
  public UITable scrollInfoContainer;
  public UITable equipmentInfoContainer;
  private AuctionRecordItemInfo _itemInfo;
  private AuctionHelper.AuctionItemType _auctionItemType;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    AuctionHallRecordInfoPopup.Parameter parameter = orgParam as AuctionHallRecordInfoPopup.Parameter;
    if (parameter != null)
      this._itemInfo = parameter.auctionRecordItemInfo;
    this._auctionItemType = !this._itemInfo.IsArtifact ? AuctionHelper.Instance.GetAuctionItemType(this._itemInfo.itemId) : AuctionHelper.AuctionItemType.ARTIFACT;
    this.ShowRecordInfoContent();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void ShowRecordInfoContent()
  {
    this.HideAllAuctionDetail();
    switch (this._auctionItemType)
    {
      case AuctionHelper.AuctionItemType.PROPS:
        this.ShowAuctionPropsDetail(this._itemInfo.itemId);
        break;
      case AuctionHelper.AuctionItemType.EQUIPMENT:
      case AuctionHelper.AuctionItemType.DK_EQUIPMENT:
        this.ShowAuctionEquipmentDetail(this._itemInfo.itemId);
        break;
      case AuctionHelper.AuctionItemType.EQUIPMENT_SCROLL:
      case AuctionHelper.AuctionItemType.DK_EQUIPMENT_SCROLL:
        this.ShowAuctionEquipmentScrollDetail(this._itemInfo.itemId);
        break;
      case AuctionHelper.AuctionItemType.ARTIFACT:
        this.ShowAuctionArtifactDetail(this._itemInfo.itemArtifactId);
        break;
    }
  }

  private void ShowAuctionPropsDetail(int itemId)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo != null)
    {
      this.auctionItemDesc.text = itemStaticInfo.LocDescription;
      Utils.SetItemBackground(this.auctionItemBg, itemStaticInfo.internalId);
      Utils.SetItemName(this.auctionItemName, itemStaticInfo.internalId);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.auctionItemTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    }
    NGUITools.SetActive(this.normalItemNode, true);
  }

  private void ShowAuctionEquipmentDetail(int itemId)
  {
    AuctionHelper.AuctionItemType auctionItemType = AuctionHelper.Instance.GetAuctionItemType(itemId);
    BagType bagType = BagType.Hero;
    switch (auctionItemType)
    {
      case AuctionHelper.AuctionItemType.EQUIPMENT:
        bagType = BagType.Hero;
        break;
      case AuctionHelper.AuctionItemType.DK_EQUIPMENT:
        bagType = BagType.DragonKnight;
        break;
    }
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo == null)
      return;
    this.ShowEquipmentDetails(itemStaticInfo.ID + "_0", bagType, false);
  }

  private void ShowAuctionEquipmentScrollDetail(int itemId)
  {
    AuctionHelper.AuctionItemType auctionItemType = AuctionHelper.Instance.GetAuctionItemType(itemId);
    BagType bagType = BagType.Hero;
    switch (auctionItemType)
    {
      case AuctionHelper.AuctionItemType.EQUIPMENT_SCROLL:
        bagType = BagType.Hero;
        break;
      case AuctionHelper.AuctionItemType.DK_EQUIPMENT_SCROLL:
        bagType = BagType.DragonKnight;
        break;
    }
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo == null)
      return;
    ConfigEquipmentScrollInfo dataByItemId = ConfigManager.inst.GetEquipmentScroll(bagType).GetDataByItemID(itemStaticInfo.internalId);
    if (dataByItemId == null)
      return;
    ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(dataByItemId.equipmenID);
    if (data != null)
      this.ShowEquipmentDetails(data.ID + "_0", bagType, true);
    this.equipScrollItemRenderer.ItemId = itemId;
    this.equipScrollName.text = itemStaticInfo.LocName;
    this.equipScrollName.color = this.equipScrollComponent.equipmentScrollName.color;
  }

  private void ShowAuctionArtifactDetail(int itemId)
  {
    ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(itemId);
    if (artifactInfo != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactTexture, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this.artifactName.text = artifactInfo.Name;
      this.artifactProtectTime.text = Utils.FormatTime(259200, false, false, true);
      this.artifactDesc.text = artifactInfo.Description;
      this.artifactBenefitContent.Init(artifactInfo.benefits);
      Utils.ExecuteInSecs(0.01f, (System.Action) (() =>
      {
        this.artifactDetailTable.repositionNow = true;
        this.artifactDetailTable.Reposition();
        this.artifactDetailScrollView.ResetPosition();
      }));
    }
    NGUITools.SetActive(this.artifactItemNode, true);
  }

  private void ShowEquipmentDetails(string equipmentId, BagType bagType, bool isEquipmentScroll = false)
  {
    ConfigEquipmentPropertyInfo data = ConfigManager.inst.GetEquipmentProperty(bagType).GetData(equipmentId);
    if (data == null)
      return;
    if (isEquipmentScroll)
    {
      NGUITools.SetActive(this.equipmentScrollItemNode, true);
      this.equipScrollComponent.FeedData((IComponentData) new EquipmentComponentData(data.equipID, 0, bagType, 0L));
      this.benefitScrollContent.Init(data.benefits);
      this.UpdateEquipmentGem(bagType, data.equipID, this.equipmentGemConainerForScroll);
      this.UpdateEquipmentSuitBenefit(bagType, data.equipID, this.equipmentSuitConainerForScroll);
      this.scrollInfoContainer.repositionNow = true;
      this.scrollInfoContainer.Reposition();
    }
    else
    {
      NGUITools.SetActive(this.equipmentItemNode, true);
      this.equipComponent.FeedData((IComponentData) new EquipmentComponentData(data.equipID, 0, bagType, 0L));
      this.benefitContent.Init(data.benefits);
      this.UpdateEquipmentGem(bagType, data.equipID, this.equipmentGemContainerForEquipment);
      this.UpdateEquipmentSuitBenefit(bagType, data.equipID, this.equipmentSuitContainerForEquipment);
      this.equipmentInfoContainer.repositionNow = true;
      this.equipmentInfoContainer.Reposition();
    }
  }

  private void HideAllAuctionDetail()
  {
    NGUITools.SetActive(this.normalItemNode, false);
    NGUITools.SetActive(this.equipmentItemNode, false);
    NGUITools.SetActive(this.equipmentScrollItemNode, false);
    NGUITools.SetActive(this.artifactItemNode, false);
  }

  private void ClearEquipmentSuitItem()
  {
    using (List<EquipmentSuitItem>.Enumerator enumerator = this.allEquipmentSuitItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EquipmentSuitItem current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
        {
          current.transform.parent = (Transform) null;
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
        }
      }
    }
    this.allEquipmentSuitItem.Clear();
  }

  private void UpdateEquipmentSuitBenefit(BagType bagType, int equipmentId, Transform parent)
  {
    this.ClearEquipmentSuitItem();
    ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(equipmentId);
    if (data == null || data.suitGroup == 0)
      return;
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/UI/Common/EquipmentSuitItem", (System.Type) null)) as GameObject;
    gameObject.transform.SetParent(parent, true);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    EquipmentSuitItem component = gameObject.GetComponent<EquipmentSuitItem>();
    component.SetData(PlayerData.inst.uid, bagType, data.suitGroup, (InventoryItemData) null);
    this.allEquipmentSuitItem.Add(component);
  }

  private void ClearEquipmentGemItem()
  {
    using (List<EquipmentGemItem>.Enumerator enumerator = this._allEquipmentGemItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EquipmentGemItem current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this._allEquipmentGemItem.Clear();
  }

  private void UpdateEquipmentGem(BagType bagType, int equipmentInternalId, Transform parent)
  {
    this.ClearEquipmentGemItem();
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/UI/Common/EquipmentGemItem", (System.Type) null)) as GameObject;
    gameObject.transform.SetParent(parent, true);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    EquipmentGemItem component = gameObject.GetComponent<EquipmentGemItem>();
    component.SetData(bagType, equipmentInternalId, (UIScrollView) null);
    this._allEquipmentGemItem.Add(component);
  }

  public class Parameter : Popup.PopupParameter
  {
    public AuctionRecordItemInfo auctionRecordItemInfo;
  }
}
