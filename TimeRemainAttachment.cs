﻿// Decompiled with JetBrains decompiler
// Type: TimeRemainAttachment
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TimeRemainAttachment : CityTouchCircleBtnAttachment
{
  private int remainTime;
  private int endTime;
  public UILabel header;

  public override void SeedData(CityTouchCircleBtnAttachment.AttachmentData ad)
  {
    TimeRemainAttachment.TRAData traData = ad as TimeRemainAttachment.TRAData;
    if (traData != null)
      this.header.text = Utils.FormatTime(traData.remainTime, false, false, true);
    this.remainTime = traData.remainTime;
    this.endTime = NetServerTime.inst.ServerTimestamp + this.remainTime;
    if (this.remainTime <= 0)
      return;
    this.RefreshTime();
    this.AddEventHandler();
  }

  private void OnDestroy()
  {
    if (!GameEngine.IsAvailable)
      return;
    this.RemoveEventHandler();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSeconde);
  }

  private void OnSeconde(int time)
  {
    this.remainTime = this.endTime - NetServerTime.inst.ServerTimestamp;
    if (this.remainTime < 600)
      this.header.color = Color.red;
    this.RefreshTime();
  }

  private void RefreshTime()
  {
    this.header.text = Utils.FormatTime(this.remainTime, false, false, true);
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSeconde);
  }

  public class TRAData : CityTouchCircleBtnAttachment.AttachmentData
  {
    public int remainTime;
    public int endTime;
  }
}
