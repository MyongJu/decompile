﻿// Decompiled with JetBrains decompiler
// Type: MailQueryArg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class MailQueryArg
{
  public List<MailQueryArg.MailQueryArgs> activeArguments = new List<MailQueryArg.MailQueryArgs>();
  public List<MailType> validTypes = new List<MailType>();
  public bool readStatus;
  public bool favStatus;
  public long startId;
  public int retrievalLimit;
  public string subject;
  public string fromUID;
  public string toUID;
  public long startCtime;
  public int mailState;

  public enum MailQueryArgs
  {
    TypeList,
    ReadStatus,
    FavStatus,
    StartMailId,
    RetrievalLimit,
    Subject,
    FromUID,
    ToUID,
    StartCtime,
    MailState,
  }
}
