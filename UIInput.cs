﻿// Decompiled with JetBrains decompiler
// Type: UIInput
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using ArabicSupport;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[AddComponentMenu("NGUI/UI/Input Field")]
public class UIInput : MonoBehaviour
{
  protected static string mLastIME = string.Empty;
  [NonSerialized]
  public bool selectAllTextOnFocus = true;
  public Color activeTextColor = Color.white;
  public Color caretColor = new Color(1f, 1f, 1f, 0.8f);
  public Color selectionColor = new Color(1f, 0.8745098f, 0.5529412f, 0.5f);
  public List<EventDelegate> onSubmit = new List<EventDelegate>();
  public List<EventDelegate> onChange = new List<EventDelegate>();
  [NonSerialized]
  protected string mDefaultText = string.Empty;
  [NonSerialized]
  protected Color mDefaultColor = Color.white;
  [NonSerialized]
  protected bool mDoInit = true;
  [NonSerialized]
  protected bool mLoadSavedValue = true;
  [NonSerialized]
  protected string mCached = string.Empty;
  [NonSerialized]
  protected int mSelectMe = -1;
  public static UIInput current;
  public static UIInput selection;
  public UILabel label;
  public UIInput.InputType inputType;
  public UIInput.OnReturnKey onReturnKey;
  public UIInput.KeyboardType keyboardType;
  public bool hideInput;
  public UIInput.Validation validation;
  public int characterLimit;
  public string savedAs;
  [SerializeField]
  [HideInInspector]
  private GameObject selectOnTab;
  public UIInput.OnValidate onValidate;
  [HideInInspector]
  [SerializeField]
  protected string mValue;
  [NonSerialized]
  protected float mPosition;
  [NonSerialized]
  protected UIWidget.Pivot mPivot;
  protected static int mDrawStart;
  protected static TouchScreenKeyboard mKeyboard;
  private static bool mWaitForKeyboard;
  [NonSerialized]
  protected int mSelectionStart;
  [NonSerialized]
  protected int mSelectionEnd;
  [NonSerialized]
  protected UITexture mHighlight;
  [NonSerialized]
  protected UITexture mCaret;
  [NonSerialized]
  protected Texture2D mBlankTex;
  [NonSerialized]
  protected float mNextBlink;
  [NonSerialized]
  protected float mLastAlpha;

  public string defaultText
  {
    get
    {
      if (this.mDoInit)
        this.Init();
      return this.mDefaultText;
    }
    set
    {
      if (this.mDoInit)
        this.Init();
      this.mDefaultText = value;
      this.UpdateLabel();
    }
  }

  public bool inputShouldBeHidden
  {
    get
    {
      if (this.hideInput && (UnityEngine.Object) this.label != (UnityEngine.Object) null && !this.label.multiLine)
        return this.inputType != UIInput.InputType.Password;
      return false;
    }
  }

  [Obsolete("Use UIInput.value instead")]
  public string text
  {
    get
    {
      return this.value;
    }
    set
    {
      this.value = value;
    }
  }

  public string value
  {
    get
    {
      if (this.mDoInit)
        this.Init();
      return this.mValue;
    }
    set
    {
      if (this.mDoInit)
        this.Init();
      UIInput.mDrawStart = 0;
      if (Application.platform == RuntimePlatform.BlackBerryPlayer)
        value = value.Replace("\\b", "\b");
      value = this.Validate(value);
      if (this.isSelected && UIInput.mKeyboard != null && this.mCached != value)
      {
        UIInput.mKeyboard.text = value;
        this.mCached = value;
      }
      if (!(this.mValue != value))
        return;
      this.mValue = value;
      this.mLoadSavedValue = false;
      if (this.isSelected)
      {
        if (string.IsNullOrEmpty(value))
        {
          this.mSelectionStart = 0;
          this.mSelectionEnd = 0;
        }
        else
        {
          this.mSelectionStart = value.Length;
          this.mSelectionEnd = this.mSelectionStart;
        }
      }
      else
        this.SaveToPlayerPrefs(value);
      this.UpdateLabel();
      this.ExecuteOnChange();
    }
  }

  [Obsolete("Use UIInput.isSelected instead")]
  public bool selected
  {
    get
    {
      return this.isSelected;
    }
    set
    {
      this.isSelected = value;
    }
  }

  public bool isSelected
  {
    get
    {
      return (UnityEngine.Object) UIInput.selection == (UnityEngine.Object) this;
    }
    set
    {
      if (!value)
      {
        if (!this.isSelected)
          return;
        UICamera.selectedObject = (GameObject) null;
      }
      else
        UICamera.selectedObject = this.gameObject;
    }
  }

  public int cursorPosition
  {
    get
    {
      if (UIInput.mKeyboard != null && !this.inputShouldBeHidden || !this.isSelected)
        return this.value.Length;
      return this.mSelectionEnd;
    }
    set
    {
      if (!this.isSelected || UIInput.mKeyboard != null && !this.inputShouldBeHidden)
        return;
      this.mSelectionEnd = value;
      this.UpdateLabel();
    }
  }

  public int selectionStart
  {
    get
    {
      if (UIInput.mKeyboard != null && !this.inputShouldBeHidden)
        return 0;
      if (this.isSelected)
        return this.mSelectionStart;
      return this.value.Length;
    }
    set
    {
      if (!this.isSelected || UIInput.mKeyboard != null && !this.inputShouldBeHidden)
        return;
      this.mSelectionStart = value;
      this.UpdateLabel();
    }
  }

  public int selectionEnd
  {
    get
    {
      if (UIInput.mKeyboard != null && !this.inputShouldBeHidden || !this.isSelected)
        return this.value.Length;
      return this.mSelectionEnd;
    }
    set
    {
      if (!this.isSelected || UIInput.mKeyboard != null && !this.inputShouldBeHidden)
        return;
      this.mSelectionEnd = value;
      this.UpdateLabel();
    }
  }

  public UITexture caret
  {
    get
    {
      return this.mCaret;
    }
  }

  public string Validate(string val)
  {
    if (string.IsNullOrEmpty(val))
      return string.Empty;
    StringBuilder stringBuilder = new StringBuilder(val.Length);
    for (int index = 0; index < val.Length; ++index)
    {
      char ch = val[index];
      if (this.onValidate != null)
        ch = this.onValidate(stringBuilder.ToString(), stringBuilder.Length, ch);
      else if (this.validation != UIInput.Validation.None)
        ch = this.Validate(stringBuilder.ToString(), stringBuilder.Length, ch);
      if ((int) ch != 0)
        stringBuilder.Append(ch);
    }
    if (this.characterLimit > 0 && stringBuilder.Length > this.characterLimit)
      return stringBuilder.ToString(0, this.characterLimit);
    return stringBuilder.ToString();
  }

  private void Start()
  {
    if ((UnityEngine.Object) this.selectOnTab != (UnityEngine.Object) null)
    {
      if ((UnityEngine.Object) this.GetComponent<UIKeyNavigation>() == (UnityEngine.Object) null)
        this.gameObject.AddComponent<UIKeyNavigation>().onDown = this.selectOnTab;
      this.selectOnTab = (GameObject) null;
      NGUITools.SetDirty((UnityEngine.Object) this);
    }
    if (this.mLoadSavedValue && !string.IsNullOrEmpty(this.savedAs))
      this.LoadValue();
    else
      this.value = this.mValue.Replace("\\n", "\n");
  }

  protected void Init()
  {
    if (!this.mDoInit || !((UnityEngine.Object) this.label != (UnityEngine.Object) null))
      return;
    this.mDoInit = false;
    this.mDefaultText = this.label.text;
    this.mDefaultColor = this.label.color;
    this.label.supportEncoding = false;
    if (this.label.alignment == NGUIText.Alignment.Justified)
    {
      this.label.alignment = NGUIText.Alignment.Left;
      Debug.LogWarning((object) "Input fields using labels with justified alignment are not supported at this time", (UnityEngine.Object) this);
    }
    this.mPivot = this.label.pivot;
    this.mPosition = this.label.cachedTransform.localPosition.x;
    this.UpdateLabel();
  }

  protected void SaveToPlayerPrefs(string val)
  {
    if (string.IsNullOrEmpty(this.savedAs))
      return;
    if (string.IsNullOrEmpty(val))
      PlayerPrefs.DeleteKey(this.savedAs);
    else
      PlayerPrefs.SetString(this.savedAs, val);
  }

  protected virtual void OnSelect(bool isSelected)
  {
    if (isSelected)
      this.OnSelectEvent();
    else
      this.OnDeselectEvent();
  }

  protected void OnSelectEvent()
  {
    UIInput.selection = this;
    if (this.mDoInit)
      this.Init();
    if (!((UnityEngine.Object) this.label != (UnityEngine.Object) null) || !NGUITools.GetActive((Behaviour) this))
      return;
    this.mSelectMe = Time.frameCount;
  }

  protected void OnDeselectEvent()
  {
    if (this.mDoInit)
      this.Init();
    if ((UnityEngine.Object) this.label != (UnityEngine.Object) null && NGUITools.GetActive((Behaviour) this))
    {
      this.mValue = this.value;
      if (UIInput.mKeyboard != null)
      {
        UIInput.mWaitForKeyboard = false;
        UIInput.mKeyboard.active = false;
        UIInput.mKeyboard = (TouchScreenKeyboard) null;
      }
      if (string.IsNullOrEmpty(this.mValue))
      {
        this.label.text = this.mDefaultText;
        this.label.color = this.mDefaultColor;
      }
      else
        this.label.text = this.mValue;
      Input.imeCompositionMode = IMECompositionMode.Auto;
      this.RestoreLabelPivot();
    }
    UIInput.selection = (UIInput) null;
    this.UpdateLabel();
  }

  protected virtual void Update()
  {
    if (!this.isSelected)
      return;
    if (this.mDoInit)
      this.Init();
    if (UIInput.mWaitForKeyboard)
    {
      if (UIInput.mKeyboard != null && !UIInput.mKeyboard.active)
        return;
      UIInput.mWaitForKeyboard = false;
    }
    if (this.mSelectMe != -1 && this.mSelectMe != Time.frameCount)
    {
      this.mSelectMe = -1;
      this.mSelectionEnd = !string.IsNullOrEmpty(this.mValue) ? this.mValue.Length : 0;
      UIInput.mDrawStart = 0;
      this.mSelectionStart = !this.selectAllTextOnFocus ? this.mSelectionEnd : 0;
      this.label.color = this.activeTextColor;
      switch (Application.platform)
      {
        case RuntimePlatform.IPhonePlayer:
        case RuntimePlatform.Android:
        case RuntimePlatform.MetroPlayerX86:
        case RuntimePlatform.MetroPlayerX64:
        case RuntimePlatform.MetroPlayerARM:
        case RuntimePlatform.WP8Player:
        case RuntimePlatform.BlackBerryPlayer:
          TouchScreenKeyboardType keyboardType;
          string text;
          if (this.inputShouldBeHidden)
          {
            TouchScreenKeyboard.hideInput = true;
            keyboardType = (TouchScreenKeyboardType) this.keyboardType;
            text = "|";
          }
          else if (this.inputType == UIInput.InputType.Password)
          {
            TouchScreenKeyboard.hideInput = false;
            keyboardType = TouchScreenKeyboardType.Default;
            text = this.mValue;
            this.mSelectionStart = this.mSelectionEnd;
          }
          else
          {
            TouchScreenKeyboard.hideInput = false;
            keyboardType = (TouchScreenKeyboardType) this.keyboardType;
            text = this.mValue;
            this.mSelectionStart = this.mSelectionEnd;
          }
          UIInput.mWaitForKeyboard = true;
          UIInput.mKeyboard = this.inputType != UIInput.InputType.Password ? TouchScreenKeyboard.Open(text, keyboardType, !this.inputShouldBeHidden && this.inputType == UIInput.InputType.AutoCorrect, this.label.multiLine && !this.hideInput, false, false, this.defaultText) : TouchScreenKeyboard.Open(text, keyboardType, false, false, true);
          break;
        default:
          Vector2 vector2 = (Vector2) (!((UnityEngine.Object) UICamera.current != (UnityEngine.Object) null) || !((UnityEngine.Object) UICamera.current.cachedCamera != (UnityEngine.Object) null) ? this.label.worldCorners[0] : UICamera.current.cachedCamera.WorldToScreenPoint(this.label.worldCorners[0]));
          vector2.y = (float) Screen.height - vector2.y;
          Input.imeCompositionMode = IMECompositionMode.On;
          Input.compositionCursorPos = vector2;
          break;
      }
      this.UpdateLabel();
      if (string.IsNullOrEmpty(Input.inputString))
        return;
    }
    if (UIInput.mKeyboard != null)
    {
      string text = UIInput.mKeyboard.text;
      if (this.inputShouldBeHidden)
      {
        if (text != "|")
        {
          if (!string.IsNullOrEmpty(text))
            this.Insert(text.Substring(1));
          else
            this.DoBackspace();
          UIInput.mKeyboard.text = "|";
        }
      }
      else if (this.mCached != text)
      {
        this.mCached = text;
        this.value = text;
      }
      if (UIInput.mKeyboard.done || !UIInput.mKeyboard.active)
      {
        if (!UIInput.mKeyboard.wasCanceled)
          this.Submit();
        UIInput.mKeyboard = (TouchScreenKeyboard) null;
        this.isSelected = false;
        this.mCached = string.Empty;
      }
    }
    else
    {
      string compositionString = Input.compositionString;
      if (string.IsNullOrEmpty(compositionString) && !string.IsNullOrEmpty(Input.inputString))
      {
        foreach (char ch in Input.inputString)
        {
          if ((int) ch >= 32 && (int) ch != 63232 && ((int) ch != 63233 && (int) ch != 63234) && (int) ch != 63235)
            this.Insert(ch.ToString());
        }
      }
      if (UIInput.mLastIME != compositionString)
      {
        this.mSelectionEnd = !string.IsNullOrEmpty(compositionString) ? this.mValue.Length + compositionString.Length : this.mSelectionStart;
        UIInput.mLastIME = compositionString;
        this.UpdateLabel();
        this.ExecuteOnChange();
      }
    }
    if ((UnityEngine.Object) this.mCaret != (UnityEngine.Object) null && (double) this.mNextBlink < (double) RealTime.time)
    {
      this.mNextBlink = RealTime.time + 0.5f;
      this.mCaret.enabled = !this.mCaret.enabled;
    }
    if (this.isSelected && (double) this.mLastAlpha != (double) this.label.finalAlpha)
      this.UpdateLabel();
    if (!Input.GetKeyDown(KeyCode.Return) && !Input.GetKeyDown(KeyCode.KeypadEnter))
      return;
    if (this.onReturnKey == UIInput.OnReturnKey.NewLine || this.onReturnKey == UIInput.OnReturnKey.Default && this.label.multiLine && (!Input.GetKey(KeyCode.LeftControl) && !Input.GetKey(KeyCode.RightControl)) && this.label.overflowMethod != UILabel.Overflow.ClampContent && this.validation == UIInput.Validation.None)
    {
      this.Insert("\n");
    }
    else
    {
      UICamera.currentScheme = UICamera.ControlScheme.Controller;
      UICamera.currentKey = KeyCode.Return;
      this.Submit();
      UICamera.currentKey = KeyCode.None;
    }
  }

  protected void DoBackspace()
  {
    if (string.IsNullOrEmpty(this.mValue))
      return;
    if (this.mSelectionStart == this.mSelectionEnd)
    {
      if (this.mSelectionStart < 1)
        return;
      --this.mSelectionEnd;
    }
    this.Insert(string.Empty);
  }

  protected virtual void Insert(string text)
  {
    string leftText = this.GetLeftText();
    string rightText = this.GetRightText();
    int length1 = rightText.Length;
    StringBuilder stringBuilder = new StringBuilder(leftText.Length + rightText.Length + text.Length);
    stringBuilder.Append(leftText);
    int index1 = 0;
    for (int length2 = text.Length; index1 < length2; ++index1)
    {
      char ch = text[index1];
      if ((int) ch == 8)
        this.DoBackspace();
      else if (this.characterLimit <= 0 || stringBuilder.Length + length1 < this.characterLimit)
      {
        if (this.onValidate != null)
          ch = this.onValidate(stringBuilder.ToString(), stringBuilder.Length, ch);
        else if (this.validation != UIInput.Validation.None)
          ch = this.Validate(stringBuilder.ToString(), stringBuilder.Length, ch);
        if ((int) ch != 0)
          stringBuilder.Append(ch);
      }
      else
        break;
    }
    this.mSelectionStart = stringBuilder.Length;
    this.mSelectionEnd = this.mSelectionStart;
    int index2 = 0;
    for (int length2 = rightText.Length; index2 < length2; ++index2)
    {
      char ch = rightText[index2];
      if (this.onValidate != null)
        ch = this.onValidate(stringBuilder.ToString(), stringBuilder.Length, ch);
      else if (this.validation != UIInput.Validation.None)
        ch = this.Validate(stringBuilder.ToString(), stringBuilder.Length, ch);
      if ((int) ch != 0)
        stringBuilder.Append(ch);
    }
    this.mValue = stringBuilder.ToString();
    this.UpdateLabel();
    this.ExecuteOnChange();
  }

  protected string GetLeftText()
  {
    int length = Mathf.Min(this.mSelectionStart, this.mSelectionEnd);
    if (string.IsNullOrEmpty(this.mValue) || length < 0)
      return string.Empty;
    return this.mValue.Substring(0, length);
  }

  protected string GetRightText()
  {
    int startIndex = Mathf.Max(this.mSelectionStart, this.mSelectionEnd);
    if (string.IsNullOrEmpty(this.mValue) || startIndex >= this.mValue.Length)
      return string.Empty;
    return this.mValue.Substring(startIndex);
  }

  protected string GetSelection()
  {
    if (string.IsNullOrEmpty(this.mValue) || this.mSelectionStart == this.mSelectionEnd)
      return string.Empty;
    int startIndex = Mathf.Min(this.mSelectionStart, this.mSelectionEnd);
    int num = Mathf.Max(this.mSelectionStart, this.mSelectionEnd);
    return this.mValue.Substring(startIndex, num - startIndex);
  }

  protected int GetCharUnderMouse()
  {
    Vector3[] worldCorners = this.label.worldCorners;
    Ray currentRay = UICamera.currentRay;
    float enter;
    if (new Plane(worldCorners[0], worldCorners[1], worldCorners[2]).Raycast(currentRay, out enter))
      return UIInput.mDrawStart + this.label.GetCharacterIndexAtPosition(currentRay.GetPoint(enter), false);
    return 0;
  }

  protected virtual void OnPress(bool isPressed)
  {
    if (!isPressed || !this.isSelected || !((UnityEngine.Object) this.label != (UnityEngine.Object) null) || UICamera.currentScheme != UICamera.ControlScheme.Mouse && UICamera.currentScheme != UICamera.ControlScheme.Touch)
      return;
    this.selectionEnd = this.GetCharUnderMouse();
    if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
      return;
    this.selectionStart = this.mSelectionEnd;
  }

  protected virtual void OnDrag(Vector2 delta)
  {
    if (!((UnityEngine.Object) this.label != (UnityEngine.Object) null) || UICamera.currentScheme != UICamera.ControlScheme.Mouse && UICamera.currentScheme != UICamera.ControlScheme.Touch)
      return;
    this.selectionEnd = this.GetCharUnderMouse();
  }

  private void OnDisable()
  {
    this.Cleanup();
  }

  protected virtual void Cleanup()
  {
    if ((bool) ((UnityEngine.Object) this.mHighlight))
      this.mHighlight.enabled = false;
    if ((bool) ((UnityEngine.Object) this.mCaret))
      this.mCaret.enabled = false;
    if (!(bool) ((UnityEngine.Object) this.mBlankTex))
      return;
    NGUITools.Destroy((UnityEngine.Object) this.mBlankTex);
    this.mBlankTex = (Texture2D) null;
  }

  public void Submit()
  {
    if (!NGUITools.GetActive((Behaviour) this))
      return;
    this.mValue = this.value;
    if ((UnityEngine.Object) UIInput.current == (UnityEngine.Object) null)
    {
      UIInput.current = this;
      EventDelegate.Execute(this.onSubmit);
      UIInput.current = (UIInput) null;
    }
    this.SaveToPlayerPrefs(this.mValue);
  }

  public void UpdateLabel()
  {
    if (!((UnityEngine.Object) this.label != (UnityEngine.Object) null))
      return;
    if (this.mDoInit)
      this.Init();
    bool isSelected = this.isSelected;
    string str1 = this.value;
    bool flag = string.IsNullOrEmpty(str1) && string.IsNullOrEmpty(Input.compositionString);
    this.label.color = !flag || isSelected ? this.activeTextColor : this.mDefaultColor;
    string str2;
    if (flag)
    {
      str2 = !isSelected ? this.mDefaultText : string.Empty;
      this.RestoreLabelPivot();
    }
    else
    {
      string str3;
      if (this.inputType == UIInput.InputType.Password)
      {
        str3 = string.Empty;
        string str4 = "*";
        if ((UnityEngine.Object) this.label.bitmapFont != (UnityEngine.Object) null && this.label.bitmapFont.bmFont != null && this.label.bitmapFont.bmFont.GetGlyph(42) == null)
          str4 = "x";
        int num = 0;
        for (int length = str1.Length; num < length; ++num)
          str3 += str4;
      }
      else
        str3 = str1;
      int num1 = !isSelected ? 0 : Mathf.Min(str3.Length, this.cursorPosition);
      string str5 = str3.Substring(0, num1);
      if (isSelected)
        str5 += Input.compositionString;
      str2 = str5 + str3.Substring(num1, str3.Length - num1);
      if (isSelected && this.label.overflowMethod == UILabel.Overflow.ClampContent && this.label.maxLineCount == 1)
      {
        int offsetToFit1 = this.label.CalculateOffsetToFit(str2);
        if (offsetToFit1 == 0)
        {
          UIInput.mDrawStart = 0;
          this.RestoreLabelPivot();
        }
        else if (num1 < UIInput.mDrawStart)
        {
          UIInput.mDrawStart = num1;
          this.SetPivotToLeft();
        }
        else if (offsetToFit1 < UIInput.mDrawStart)
        {
          UIInput.mDrawStart = offsetToFit1;
          this.SetPivotToLeft();
        }
        else
        {
          int offsetToFit2 = this.label.CalculateOffsetToFit(str2.Substring(0, num1));
          if (offsetToFit2 > UIInput.mDrawStart)
          {
            UIInput.mDrawStart = offsetToFit2;
            this.SetPivotToRight();
          }
        }
        if (UIInput.mDrawStart != 0)
          str2 = str2.Substring(UIInput.mDrawStart, str2.Length - UIInput.mDrawStart);
      }
      else
      {
        UIInput.mDrawStart = 0;
        this.RestoreLabelPivot();
      }
    }
    if (str2.Length > 0 && NGUIText.IsArabic(str2[0]))
      str2 = ArabicFixer.Fix(str2, false, false);
    this.label.text = str2;
    if (isSelected && (UIInput.mKeyboard == null || this.inputShouldBeHidden))
    {
      int start = this.mSelectionStart - UIInput.mDrawStart;
      int end = this.mSelectionEnd - UIInput.mDrawStart;
      if ((UnityEngine.Object) this.mBlankTex == (UnityEngine.Object) null)
      {
        this.mBlankTex = new Texture2D(2, 2, TextureFormat.ARGB32, false);
        for (int y = 0; y < 2; ++y)
        {
          for (int x = 0; x < 2; ++x)
            this.mBlankTex.SetPixel(x, y, Color.white);
        }
        this.mBlankTex.Apply();
      }
      if (start != end)
      {
        if ((UnityEngine.Object) this.mHighlight == (UnityEngine.Object) null)
        {
          this.mHighlight = NGUITools.AddWidget<UITexture>(this.label.cachedGameObject);
          this.mHighlight.name = "Input Highlight";
          this.mHighlight.mainTexture = (Texture) this.mBlankTex;
          this.mHighlight.fillGeometry = false;
          this.mHighlight.pivot = this.label.pivot;
          this.mHighlight.SetAnchor(this.label.cachedTransform);
        }
        else
        {
          this.mHighlight.pivot = this.label.pivot;
          this.mHighlight.mainTexture = (Texture) this.mBlankTex;
          this.mHighlight.MarkAsChanged();
          this.mHighlight.enabled = true;
        }
      }
      if ((UnityEngine.Object) this.mCaret == (UnityEngine.Object) null)
      {
        this.mCaret = NGUITools.AddWidget<UITexture>(this.label.cachedGameObject);
        this.mCaret.name = "Input Caret";
        this.mCaret.mainTexture = (Texture) this.mBlankTex;
        this.mCaret.fillGeometry = false;
        this.mCaret.pivot = this.label.pivot;
        this.mCaret.SetAnchor(this.label.cachedTransform);
      }
      else
      {
        this.mCaret.pivot = this.label.pivot;
        this.mCaret.mainTexture = (Texture) this.mBlankTex;
        this.mCaret.MarkAsChanged();
        this.mCaret.enabled = true;
      }
      if (start != end)
      {
        this.label.PrintOverlay(start, end, this.mCaret.geometry, this.mHighlight.geometry, this.caretColor, this.selectionColor);
        this.mHighlight.enabled = this.mHighlight.geometry.hasVertices;
      }
      else
      {
        this.label.PrintOverlay(start, end, this.mCaret.geometry, (UIGeometry) null, this.caretColor, this.selectionColor);
        if ((UnityEngine.Object) this.mHighlight != (UnityEngine.Object) null)
          this.mHighlight.enabled = false;
      }
      this.mNextBlink = RealTime.time + 0.5f;
      this.mLastAlpha = this.label.finalAlpha;
    }
    else
      this.Cleanup();
  }

  protected void SetPivotToLeft()
  {
    Vector2 pivotOffset = NGUIMath.GetPivotOffset(this.mPivot);
    pivotOffset.x = 0.0f;
    this.label.pivot = NGUIMath.GetPivot(pivotOffset);
  }

  protected void SetPivotToRight()
  {
    Vector2 pivotOffset = NGUIMath.GetPivotOffset(this.mPivot);
    pivotOffset.x = 1f;
    this.label.pivot = NGUIMath.GetPivot(pivotOffset);
  }

  protected void RestoreLabelPivot()
  {
    if (!((UnityEngine.Object) this.label != (UnityEngine.Object) null) || this.label.pivot == this.mPivot)
      return;
    this.label.pivot = this.mPivot;
  }

  protected char Validate(string text, int pos, char ch)
  {
    if (this.validation == UIInput.Validation.None || !this.enabled)
      return ch;
    if (this.validation == UIInput.Validation.Integer)
    {
      if ((int) ch >= 48 && (int) ch <= 57 || (int) ch == 45 && pos == 0 && !text.Contains("-"))
        return ch;
    }
    else if (this.validation == UIInput.Validation.Float)
    {
      if ((int) ch >= 48 && (int) ch <= 57 || (int) ch == 45 && pos == 0 && !text.Contains("-") || (int) ch == 46 && !text.Contains("."))
        return ch;
    }
    else if (this.validation == UIInput.Validation.Alphanumeric)
    {
      if ((int) ch >= 65 && (int) ch <= 90 || (int) ch >= 97 && (int) ch <= 122 || (int) ch >= 48 && (int) ch <= 57)
        return ch;
    }
    else if (this.validation == UIInput.Validation.Username)
    {
      if ((int) ch >= 65 && (int) ch <= 90)
        return (char) ((int) ch - 65 + 97);
      if ((int) ch >= 97 && (int) ch <= 122 || (int) ch >= 48 && (int) ch <= 57)
        return ch;
    }
    else if (this.validation == UIInput.Validation.Name)
    {
      char ch1 = text.Length <= 0 ? ' ' : text[Mathf.Clamp(pos, 0, text.Length - 1)];
      char ch2 = text.Length <= 0 ? '\n' : text[Mathf.Clamp(pos + 1, 0, text.Length - 1)];
      if ((int) ch >= 97 && (int) ch <= 122)
      {
        if ((int) ch1 == 32)
          return (char) ((int) ch - 97 + 65);
        return ch;
      }
      if ((int) ch >= 65 && (int) ch <= 90)
      {
        if ((int) ch1 != 32 && (int) ch1 != 39)
          return (char) ((int) ch - 65 + 97);
        return ch;
      }
      if ((int) ch == 39)
      {
        if ((int) ch1 != 32 && (int) ch1 != 39 && ((int) ch2 != 39 && !text.Contains("'")))
          return ch;
      }
      else if ((int) ch == 32 && (int) ch1 != 32 && ((int) ch1 != 39 && (int) ch2 != 32) && (int) ch2 != 39)
        return ch;
    }
    return char.MinValue;
  }

  protected void ExecuteOnChange()
  {
    if (!((UnityEngine.Object) UIInput.current == (UnityEngine.Object) null) || !EventDelegate.IsValid(this.onChange))
      return;
    UIInput.current = this;
    EventDelegate.Execute(this.onChange);
    UIInput.current = (UIInput) null;
  }

  public void RemoveFocus()
  {
    this.isSelected = false;
  }

  public void SaveValue()
  {
    this.SaveToPlayerPrefs(this.mValue);
  }

  public void LoadValue()
  {
    if (string.IsNullOrEmpty(this.savedAs))
      return;
    string str = this.mValue.Replace("\\n", "\n");
    this.mValue = string.Empty;
    this.value = !PlayerPrefs.HasKey(this.savedAs) ? str : PlayerPrefs.GetString(this.savedAs);
  }

  public enum InputType
  {
    Standard,
    AutoCorrect,
    Password,
  }

  public enum Validation
  {
    None,
    Integer,
    Float,
    Alphanumeric,
    Username,
    Name,
  }

  public enum KeyboardType
  {
    Default,
    ASCIICapable,
    NumbersAndPunctuation,
    URL,
    NumberPad,
    PhonePad,
    NamePhonePad,
    EmailAddress,
  }

  public enum OnReturnKey
  {
    Default,
    Submit,
    NewLine,
  }

  public delegate char OnValidate(string text, int charIndex, char addedChar);
}
