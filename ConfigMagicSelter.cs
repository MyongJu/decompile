﻿// Decompiled with JetBrains decompiler
// Type: ConfigMagicSelter
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigMagicSelter
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<int, int> _itemId2MagicSelter = new Dictionary<int, int>();
  private Dictionary<string, MagicSelterInfo> datas;
  private Dictionary<int, MagicSelterInfo> dicByUniqueId;
  private List<ItemStaticInfo> _allItems;

  public List<ItemStaticInfo> GetAllDisbandItems(int category = 0)
  {
    this._allItems = new List<ItemStaticInfo>();
    Dictionary<string, MagicSelterInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && (category <= 0 || category == enumerator.Current.Category))
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(enumerator.Current.DisbandItemId);
        if (itemStaticInfo != null && ItemBag.Instance.GetItemCount(enumerator.Current.DisbandItemId) > 0)
        {
          this._allItems.Add(itemStaticInfo);
          if (!this._itemId2MagicSelter.ContainsKey(itemStaticInfo.internalId))
            this._itemId2MagicSelter.Add(itemStaticInfo.internalId, enumerator.Current.internalId);
        }
      }
    }
    this._allItems.Sort(new Comparison<ItemStaticInfo>(this.SortDelegate));
    return this._allItems;
  }

  public List<int> GetDisbandItemFromRewardItem(int rewardId)
  {
    List<int> intList = new List<int>();
    Dictionary<string, MagicSelterInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && enumerator.Current.RewardItemId == rewardId)
        intList.Add(enumerator.Current.DisbandItemId);
    }
    return intList;
  }

  public ItemStaticInfo GetRewardItem(int disbandItemtId)
  {
    return ConfigManager.inst.DB_Items.GetItem(this.GetDataByDisbandItemId(disbandItemtId).RewardItemId);
  }

  public int GetRewardCount(int disbandItemtId, int count)
  {
    if (count <= 0)
      return 0;
    return this.GetDataByDisbandItemId(disbandItemtId).Value * count;
  }

  public MagicSelterInfo GetDataByDisbandItemId(int disbandItemtId)
  {
    return this.GetData(this._itemId2MagicSelter[disbandItemtId]);
  }

  private int SortDelegate(ItemStaticInfo a, ItemStaticInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }

  public void BuildDB(object res)
  {
    this.parse.Parse<MagicSelterInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public MagicSelterInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (MagicSelterInfo) null;
  }

  public MagicSelterInfo GetData(int internalId)
  {
    if (this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (MagicSelterInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, MagicSelterInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, MagicSelterInfo>) null;
  }
}
