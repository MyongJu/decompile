﻿// Decompiled with JetBrains decompiler
// Type: AnniversarySendMarchDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AnniversarySendMarchDlg : UI.Dialog
{
  public Dictionary<string, AnniversarySendMarchDlg.TroopData> troops = new Dictionary<string, AnniversarySendMarchDlg.TroopData>();
  public List<TroopSlider> list = new List<TroopSlider>();
  private int resourceSize = 1;
  private int _slotIndex = 1;
  private AnniversaryBattleRole attakcer = new AnniversaryBattleRole();
  private AnniversaryBattleRole defender = new AnniversaryBattleRole();
  private Hashtable attackTroops = new Hashtable();
  private int desTroopCount = -1;
  private int desLoadCount = -1;
  private int orgDesLoadCount = -1;
  public UILabel label;
  public UILabel troopsCount;
  public UILabel troopLoads;
  public UILabel troopPower;
  public UIButton button;
  public UIButton sendBt;
  public UIButton button1;
  public UIButton sendBt1;
  public UIButton emptyBt;
  public TroopSlider troopItem;
  public UIScrollView view;
  public DragonSlider dragonSlider;
  public UILabel noTroosText;
  public UIGrid listParent;
  private int maxTroopCount;
  public GameObject normal;
  public GameObject hasSlot;
  public UILabel marchTime;
  public UILabel marchTime2;
  private bool _isChangellen;
  private bool isInited;
  private bool isDragonIn;
  private Hashtable _battle;
  private bool _isWin;
  public GameObject[] slots;
  public UIButton[] slotBts;
  public UIButtonBar marchBar;
  public GameObject emptyMarchData;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    AnniversarySendMarchDlg.Parameter parameter = orgParam as AnniversarySendMarchDlg.Parameter;
    this._isChangellen = parameter.isChangellen;
    this._slotIndex = parameter.slotIndex;
    this.HideButtons();
    string empty = string.Empty;
    string Term;
    if (!this._isChangellen)
    {
      Term = "id_uppercase_march";
      NGUITools.SetActive(this.button.gameObject, true);
      NGUITools.SetActive(this.button1.gameObject, true);
    }
    else
    {
      Term = "id_uppercase_march";
      NGUITools.SetActive(this.sendBt.gameObject, true);
      NGUITools.SetActive(this.sendBt1.gameObject, true);
    }
    int time = !this._isChangellen ? 0 : 3;
    UILabel marchTime = this.marchTime;
    string str1 = Utils.FormatTime(time, false, false, true);
    this.marchTime2.text = str1;
    string str2 = str1;
    marchTime.text = str2;
    this.label.text = ScriptLocalization.Get(Term, true);
    this.CheckSlot();
  }

  private void HideButtons()
  {
    NGUITools.SetActive(this.button.gameObject, false);
    NGUITools.SetActive(this.sendBt.gameObject, false);
    NGUITools.SetActive(this.button1.gameObject, false);
    NGUITools.SetActive(this.sendBt1.gameObject, false);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.dragonSlider.onDragonSelectedChanged += new System.Action<bool>(this.OnDragonSelectedChanged);
    this.marchBar.OnSelectedHandler += new System.Action<int>(this.OnSlockSelectedIndex);
    this.FreshPanel();
    this.RefreshData();
  }

  public void FreshPanel()
  {
    if (!this.isInited)
      this.InitTroopList();
    this.FreshDragonData();
    this.CalcTroopLimitCount();
    this.FreshLeftTroopCount();
    this.FreshTroopList();
    this.FreshTroopData();
    this.FreshButtons();
    this.OnDefaultSuggestSelect();
    this.FreshTroopData();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.dragonSlider.onDragonSelectedChanged -= new System.Action<bool>(this.OnDragonSelectedChanged);
    this.marchBar.OnSelectedHandler -= new System.Action<int>(this.OnSlockSelectedIndex);
    this.troops.Clear();
  }

  public void InitTroopList()
  {
    if (this.list.Count > 0)
    {
      for (int index = 0; index < this.list.Count; ++index)
      {
        string id = this.list[index].troopInfo.ID;
        if (this.troops.ContainsKey(id))
          this.troops[id].troopCount = this.list[index].troopCount;
        this.list[index].onValueChange -= new System.Action<string, int>(this.OnTroopCountChanged);
        UnityEngine.Object.Destroy((UnityEngine.Object) this.list[index].gameObject);
      }
      this.list.Clear();
    }
    CityTroopsInfo totalTroops = PlayerData.inst.playerCityData.totalTroops;
    if (totalTroops.troops == null)
      return;
    List<Unit_StatisticsInfo> unitStatisticsInfoList = new List<Unit_StatisticsInfo>();
    Dictionary<string, int>.KeyCollection.Enumerator enumerator = totalTroops.troops.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator.Current);
      if (data != null && data.Type != "trap")
        unitStatisticsInfoList.Add(data);
    }
    unitStatisticsInfoList.Sort((Comparison<Unit_StatisticsInfo>) ((a, b) =>
    {
      if (a.Troop_Tier == b.Troop_Tier)
        return a.Priority.CompareTo(b.Priority);
      return b.Troop_Tier.CompareTo(a.Troop_Tier);
    }));
    for (int index = 0; index < unitStatisticsInfoList.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.listParent.gameObject, this.troopItem.gameObject);
      gameObject.name = "troop_" + (object) (100 + index);
      gameObject.transform.localPosition = new Vector3(0.0f, -250f * (float) (index + 1), 0.0f);
      if (!this.troops.ContainsKey(unitStatisticsInfoList[index].ID))
        this.troops.Add(unitStatisticsInfoList[index].ID, new AnniversarySendMarchDlg.TroopData()
        {
          troopId = unitStatisticsInfoList[index].ID
        });
      TroopSlider component = gameObject.GetComponent<TroopSlider>();
      component.onValueChange += new System.Action<string, int>(this.OnTroopCountChanged);
      this.list.Add(component);
      gameObject.SetActive(false);
    }
    this.noTroosText.gameObject.SetActive(this.list.Count == 0);
    this.CalcTroopLimitCount();
    this.FreshTroopList();
  }

  private void RefreshData()
  {
    AnniversaryManager.AnniversaryData data = AnniversaryManager.Instance.Data;
    if (data == null || this._isChangellen)
      return;
    this.isDragonIn = data.IsWithDragon;
    Dictionary<string, int> troops = data.GetTroops();
    for (int index = 0; index < this.list.Count; ++index)
    {
      if (troops.ContainsKey(this.list[index].troopInfo.ID))
        this.list[index].troopCount = troops[this.list[index].troopInfo.ID];
    }
    this.dragonSlider.isSelected = this.isDragonIn;
    if (PlayerData.inst.dragonData != null && PlayerData.inst.dragonData.MarchId == 0L && GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.DragonKnight)
      this.dragonSlider.isSelected = this.isDragonIn = this.dragonSlider.isSelected;
    else
      this.dragonSlider.isSelected = this.isDragonIn = false;
  }

  public void OnTroopCountChanged(string troopId, int count)
  {
    int num1 = 0;
    for (int index = 0; index < this.list.Count; ++index)
      num1 += this.list[index].troopCount;
    if (num1 > this.maxTroopCount)
    {
      int num2 = num1 - this.maxTroopCount;
      for (int index = this.list.Count - 1; index >= 0; --index)
      {
        if (this.list[index].troopInfo.ID == troopId)
        {
          this.list[index].troopCount -= num2;
          break;
        }
      }
      int maxTroopCount = this.maxTroopCount;
    }
    this.FreshLeftTroopCount();
    this.FreshTroopData();
    this.FreshButtons();
  }

  public void OnDragonSelectedChanged(bool isSelected)
  {
    this.isDragonIn = isSelected;
    this.CalcTroopLimitCount();
    int num = 0;
    for (int index = 0; index < this.list.Count; ++index)
      num += this.list[index].troopCount;
    if (num > this.maxTroopCount)
      this.OnQuickQueueClick();
    this.FreshDragonData();
    this.FreshTroopData();
    this.FreshLeftTroopCount();
  }

  public void OnCityDataChanged(long cityId)
  {
    if (cityId != (long) PlayerData.inst.cityId || TutorialManager.Instance.IsRunning)
      return;
    this.InitTroopList();
    this.FreshTroopData();
    this.FreshButtons();
  }

  public void OnDragonDataChanged(long dragonId)
  {
    if (dragonId != PlayerData.inst.uid)
      return;
    this.OnDragonSelectedChanged(this.isDragonIn);
    this.FreshDragonData();
  }

  private ConfigBenefitCalc.CalcOption GetCalcOption()
  {
    return this.isDragonIn ? ConfigBenefitCalc.CalcOption.dragonAttack | ConfigBenefitCalc.CalcOption.excludeArtifact | ConfigBenefitCalc.CalcOption.excludeTitle : ConfigBenefitCalc.CalcOption.excludeArtifact | ConfigBenefitCalc.CalcOption.excludeTitle;
  }

  private void CalcTroopLimitCount()
  {
    BenefitInfo benefitInfo = DBManager.inst.DB_Local_Benefit.Get("prop_march_capacity_base_value");
    this.maxTroopCount = (int) ((double) benefitInfo.total - (double) benefitInfo.artifact.value);
    this.maxTroopCount = (int) ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) this.maxTroopCount, "calc_march_capacity", this.GetCalcOption());
  }

  public void FreshTroopData()
  {
    int internalId = ConfigManager.inst.DB_Properties["prop_army_load_percent"].InternalID;
    BenefitInfo benefitInfo = DBManager.inst.DB_Local_Benefit.Get(internalId);
    float num1 = 1f;
    if (benefitInfo != null)
      num1 += benefitInfo.total;
    if (this.isDragonIn)
    {
      DragonData dragonData = DBManager.inst.DB_Dragon.Get(PlayerData.inst.uid);
      if (dragonData != null)
        num1 += dragonData.benefits.GetBenefit(internalId);
    }
    int num2 = 0;
    int num3 = 0;
    int num4 = 0;
    for (int index = 0; index < this.list.Count; ++index)
    {
      ConfigManager.inst.DB_BenefitCalc.GetUnitFinalData(this.list[index].troopInfo.ID, ConfigBenefitCalc.UnitCalcType.load | ConfigBenefitCalc.UnitCalcType.speed, ConfigBenefitCalc.CalcOption.none);
      num2 += this.list[index].troopCount;
      num3 += Mathf.RoundToInt((float) (this.list[index].troopInfo.GatherCapacity * this.list[index].troopCount) * num1);
      num4 += this.list[index].troopInfo.Power * this.list[index].troopCount;
    }
    this.troopsCount.text = string.Format("{0} / {1}", (object) Utils.FormatThousands(num2.ToString()), (object) Utils.FormatThousands(this.maxTroopCount.ToString()));
    this.troopLoads.text = Utils.FormatThousands((num3 / this.resourceSize).ToString());
    this.troopPower.text = Utils.FormatThousands(num4.ToString());
    this.FreshButtons();
  }

  public void FreshDragonData()
  {
    this.dragonSlider.Refresh("attack", true);
  }

  public void FreshTroopList()
  {
    CityTroopsInfo totalTroops = PlayerData.inst.playerCityData.totalTroops;
    if (totalTroops.troops == null)
      return;
    List<Unit_StatisticsInfo> unitStatisticsInfoList = new List<Unit_StatisticsInfo>();
    Dictionary<string, int>.KeyCollection.Enumerator enumerator = totalTroops.troops.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator.Current);
      if (data != null && data.Type != "trap")
        unitStatisticsInfoList.Add(data);
    }
    unitStatisticsInfoList.Sort((Comparison<Unit_StatisticsInfo>) ((a, b) =>
    {
      if (a.Troop_Tier == b.Troop_Tier)
        return a.Priority.CompareTo(b.Priority);
      return b.Troop_Tier.CompareTo(a.Troop_Tier);
    }));
    for (int index = 0; index < unitStatisticsInfoList.Count; ++index)
    {
      if (totalTroops.troops[unitStatisticsInfoList[index].ID] > 0)
        this.list[index].gameObject.SetActive(true);
      TroopSlider troopSlider = this.list[index];
      troopSlider.troopInfo = unitStatisticsInfoList[index];
      troopSlider.troopMaxCount = totalTroops.troops[unitStatisticsInfoList[index].ID];
      troopSlider.troopCount = this.troops[unitStatisticsInfoList[index].ID].troopCount;
    }
  }

  private void FreshLeftTroopCount()
  {
    int num1 = 0;
    for (int index = 0; index < this.list.Count; ++index)
      num1 += this.list[index].troopCount;
    int num2 = this.maxTroopCount - num1;
    for (int index = 0; index < this.list.Count; ++index)
      this.list[index].targetCount = this.list[index].troopCount + num2;
  }

  public void FreshButtons()
  {
    for (int index = 0; index < this.list.Count; ++index)
    {
      if (this.list[index].troopCount > 0)
      {
        this.sendBt.isEnabled = true;
        this.sendBt1.isEnabled = true;
        this.button.isEnabled = true;
        this.button1.isEnabled = true;
        return;
      }
    }
    this.sendBt.isEnabled = false;
    this.sendBt1.isEnabled = false;
    this.button.isEnabled = false;
    this.button1.isEnabled = false;
  }

  public void Save()
  {
    Hashtable hashtable = new Hashtable();
    for (int index = 0; index < this.list.Count; ++index)
    {
      if (this.list[index].troopCount > 0)
        hashtable.Add((object) this.list[index].troopInfo.ID, (object) this.list[index].troopCount);
    }
    RequestManager.inst.SendRequest("anniversary:saveKingsConflictTroops", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "dragon",
        (object) (!this.isDragonIn ? 0 : 1)
      },
      {
        (object) "troops",
        (object) hashtable
      }
    }, new System.Action<bool, object>(this.OnSaveCallBack), true);
  }

  private void OnSaveCallBack(bool success, object result)
  {
    if (!success)
      return;
    string content = ScriptLocalization.Get("event_1_year_tournament_benefits_description", true);
    string title = ScriptLocalization.Get("id_uppercase_notice", true);
    ScriptLocalization.Get("id_uppercase_cancel", true);
    string str = ScriptLocalization.Get("id_uppercase_confirm", true);
    UIManager.inst.ShowConfirmationBox(title, content, str, str, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) null, (System.Action) null, (System.Action) null);
    AnniversaryManager.Instance.SetUp(result);
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void SendMarch()
  {
    Hashtable hashtable = new Hashtable();
    for (int index = 0; index < this.list.Count; ++index)
    {
      if (this.list[index].troopCount > 0)
        hashtable.Add((object) this.list[index].troopInfo.ID, (object) this.list[index].troopCount);
    }
    AnniversaryManager.AnniversarySlotData slotData = AnniversaryManager.Instance.GetSlotData(this._slotIndex);
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    postData.Add((object) "slot", (object) this._slotIndex);
    postData.Add((object) "updated_at", (object) slotData.UpdateTime);
    if (slotData != null)
    {
      postData.Add((object) "kuid", (object) slotData.Uid);
    }
    else
    {
      long num = 6000000000L + (long) this._slotIndex;
      postData.Add((object) "kuid", (object) num);
    }
    postData.Add((object) "dragon", (object) (!this.isDragonIn ? 0 : 1));
    postData.Add((object) "troops", (object) hashtable);
    RequestManager.inst.SendRequest("anniversary:challengeKing", postData, new System.Action<bool, object>(this.OnChallengeCallBack), true);
  }

  private void OnChallengeCallBack(bool success, object result)
  {
    AnniversaryManager.Instance.SetUp(result);
    if (success)
    {
      Hashtable hashtable1 = result as Hashtable;
      if (hashtable1 == null || !hashtable1.ContainsKey((object) "battle") || hashtable1[(object) "battle"] == null)
        return;
      Hashtable hashtable2 = hashtable1[(object) "battle"] as Hashtable;
      if (hashtable2 == null)
        return;
      this._battle = hashtable2;
      this.ParseBattleResult();
      AnniversaryManager.AnniversarySlotData slotData = AnniversaryManager.Instance.GetSlotData(this._slotIndex);
      this.attakcer.worldId = PlayerData.inst.userData.world_id;
      this.defender.worldId = slotData.WorldId;
      if (this.defender.worldId <= 0)
        this.defender.worldId = this.attakcer.worldId;
      this.ShowAnimation();
    }
    else
      UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void ParseBattleResult()
  {
    if (this._battle.ContainsKey((object) "attacker") && this._battle[(object) "attacker"] != null)
      this.attakcer.Decode(this._battle[(object) "attacker"]);
    if (this._battle.ContainsKey((object) "defender") && this._battle[(object) "defender"] != null)
      this.defender.Decode(this._battle[(object) "defender"]);
    long result = 0;
    if (this._battle.ContainsKey((object) "winner_uid") && this._battle[(object) "winner_uid"] != null)
      long.TryParse(this._battle[(object) "winner_uid"].ToString(), out result);
    this._isWin = result == this.attakcer.uid;
    AnniversaryChampionMainInfo data = ConfigManager.inst.DB_AnniversaryChampionMain.GetData(this._slotIndex.ToString() + string.Empty);
    string str = string.Empty;
    if (data != null)
      str = data.LocalName;
    if (this._battle.ContainsKey((object) "attacker_troop_detail") && this._battle[(object) "attacker_troop_detail"] != null)
      this.attackTroops = this.GetTroops(this._battle[(object) "attacker_troop_detail"]);
    this.attakcer.troopPower = this.GetTroopPower(this.attackTroops);
    this.defender.troopPower = this.GetTroopPower(this.GetTroops(this._battle[(object) "defender_troop_detail"]));
  }

  private Hashtable GetTroops(object orgData)
  {
    ArrayList arrayList = orgData as ArrayList;
    Hashtable hashtable1 = new Hashtable();
    if (arrayList != null)
    {
      for (int index = 0; index < arrayList.Count; ++index)
      {
        Hashtable hashtable2 = arrayList[index] as Hashtable;
        if (hashtable2 != null && hashtable2.ContainsKey((object) "start_troops") && hashtable2[(object) "start_troops"] != null)
        {
          hashtable1 = hashtable2[(object) "start_troops"] as Hashtable;
          break;
        }
      }
    }
    return hashtable1;
  }

  private void ShowResult()
  {
    AttackShowManager.Instance.onAttackShowTerminate -= new System.Action(this.ShowResult);
    AnniversaryChampionMainInfo data = ConfigManager.inst.DB_AnniversaryChampionMain.GetData(this._slotIndex.ToString() + string.Empty);
    string str = string.Empty;
    if (data != null)
      str = data.LocalName;
    UIManager.inst.OpenPopup("Report/BattleResultPopup", (Popup.PopupParameter) new AnniversaryBattleResultPopup.Parameter()
    {
      isVictory = this._isWin,
      selfBattleRole = this.attakcer,
      otherBattleRole = this.defender,
      slotName = str
    });
  }

  private int GetTroopPower(Hashtable troops)
  {
    int num = 1;
    if (troops != null)
    {
      IEnumerator enumerator = troops.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        string uniqueId = enumerator.Current.ToString();
        string s = troops[enumerator.Current].ToString();
        int result = 0;
        if (int.TryParse(s, out result))
        {
          Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(uniqueId);
          if (data != null)
            num += data.Power * result;
        }
      }
    }
    return num;
  }

  private void ShowAnimation()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
    AttackShowPara attackShowPara = new AttackShowPara();
    int num = this.defender.symbolCode;
    if (this.defender.uid > 6000000000L)
      num = -1;
    attackShowPara.dragon = this.attakcer.dragon;
    attackShowPara.targetAllianceCode = num;
    attackShowPara.targetLevel = this.defender.city_level;
    attackShowPara.targetName = this.defender.Name;
    attackShowPara.troops = this.attackTroops;
    AttackShowManager.Instance.onAttackShowTerminate -= new System.Action(this.ShowResult);
    AttackShowManager.Instance.onAttackShowTerminate += new System.Action(this.ShowResult);
    UIManager.inst.OpenPopup("Report/AnniversaryAttackShowPopup", (Popup.PopupParameter) new AnniversaryAttackShowPopup.Parameter()
    {
      para = attackShowPara
    });
  }

  private void CheckSlot()
  {
    int setMarchSlot = PlayerData.inst.playerCityData.GetSetMarchSlot();
    if (setMarchSlot == 0)
    {
      NGUITools.SetActive(this.normal, true);
      NGUITools.SetActive(this.hasSlot, false);
    }
    else
    {
      bool state = false;
      for (int index = 0; index < setMarchSlot; ++index)
      {
        if (PlayerData.inst.playerCityData.GetMarchSetData(index + 1) != null)
        {
          state = true;
          break;
        }
      }
      NGUITools.SetActive(this.marchBar.gameObject, state);
      NGUITools.SetActive(this.emptyMarchData.gameObject, !state);
      if ((UnityEngine.Object) this.emptyBt != (UnityEngine.Object) null)
        this.emptyBt.isEnabled = false;
      for (int index = 0; index < this.slots.Length; ++index)
      {
        if (index + 1 > setMarchSlot)
        {
          this.slotBts[index].isEnabled = false;
          GreyUtility.Grey(this.slots[index]);
        }
      }
    }
  }

  public void OnSlockSelectedIndex(int index)
  {
    this.OnSlotClick(index + 1);
  }

  public void OnSlot1Click()
  {
    this.OnSlotClick(1);
  }

  public void OnSlot2Click()
  {
    this.OnSlotClick(2);
  }

  public void OnSlot3Click()
  {
    this.OnSlotClick(3);
  }

  public void OnSlotClick(int index)
  {
    if (PlayerData.inst.playerCityData.GetSetMarchSlot() < index)
      return;
    CityData.MarchSetData marchSetData = PlayerData.inst.playerCityData.GetMarchSetData(index);
    if (marchSetData != null)
    {
      DragonData dragonData = PlayerData.inst.dragonData;
      if (dragonData != null && dragonData.MarchId <= 0L)
      {
        this.dragonSlider.isSelected = marchSetData.isDragonIn;
        this.isDragonIn = marchSetData.isDragonIn;
      }
      this.CalcTroopLimitCount();
      int num1 = this.maxTroopCount;
      Dictionary<string, int> troops = marchSetData.troops;
      CityTroopsInfo cityTroops = PlayerData.inst.playerCityData.cityTroops;
      for (int index1 = 0; index1 < this.list.Count; ++index1)
      {
        if (troops.ContainsKey(this.list[index1].troopInfo.ID))
        {
          int a = troops[this.list[index1].troopInfo.ID];
          if (cityTroops.troops.ContainsKey(this.list[index1].troopInfo.ID))
            a = Mathf.Min(a, cityTroops.troops[this.list[index1].troopInfo.ID]);
          int num2 = num1 - a;
          if (num2 <= 0)
            a = num1;
          num1 = num2;
          this.list[index1].troopCount = a;
        }
        else
          this.list[index1].troopCount = 0;
      }
    }
    this.FreshTroopData();
    this.FreshButtons();
  }

  public void OnQuickQueueClick()
  {
    int num1 = 0;
    int num2 = 0;
    this.marchBar.SelectedIndex = -1;
    for (int index = 0; index < this.list.Count; ++index)
      num2 += this.list[index].troopCount;
    if (num2 != 0)
    {
      for (int index = 0; index < this.list.Count; ++index)
        this.list[index].Zero();
    }
    else
    {
      int num3 = this.maxTroopCount - num2;
      for (int index = 0; index < this.list.Count && num3 != 0; ++index)
      {
        if (this.list[index].troopMaxCount - this.list[index].troopCount != 0)
        {
          if (this.list[index].troopMaxCount - this.list[index].troopCount < num3)
          {
            num3 -= this.list[index].troopMaxCount - this.list[index].troopCount;
            this.list[index].Max();
          }
          else
          {
            this.list[index].troopCount += num3;
            num1 = 0;
            break;
          }
        }
      }
    }
    this.FreshTroopData();
  }

  public void OnBoostButtonClick()
  {
    int num1 = PlayerData.inst.playerCityData.marchSlot - DBManager.inst.DB_March.marchOwner.Count;
    this.marchBar.SelectedIndex = -1;
    if (num1 == 0)
      return;
    int num2 = 0;
    for (int index = 0; index < this.list.Count; ++index)
      num2 += this.list[index].troopMaxCount;
    int maxTroopCount = Mathf.CeilToInt((float) num2 / (float) num1);
    if (maxTroopCount > this.maxTroopCount)
      maxTroopCount = this.maxTroopCount;
    float num3 = (float) maxTroopCount / (float) num2;
    int num4 = 0;
    for (int index = 0; index < this.list.Count; ++index)
    {
      int num5 = Mathf.CeilToInt(num3 * (float) this.list[index].troopMaxCount);
      if (num4 + num5 < this.maxTroopCount)
      {
        this.list[index].troopCount = num5;
        num4 += num5;
      }
      else
      {
        this.list[index].troopCount = this.maxTroopCount - num4;
        break;
      }
    }
    this.FreshTroopData();
  }

  private void OnDefaultSuggestSelect()
  {
    this.dragonSlider.isSelected = PlayerData.inst.dragonData == null || PlayerData.inst.dragonData.MarchId != 0L || GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.DragonKnight ? (this.isDragonIn = false) : (this.isDragonIn = true);
    this.CalcTroopLimitCount();
    for (int index = 0; index < this.list.Count; ++index)
      this.list[index].Zero();
    switch (MarchAllocDlg.MarchType.cityAttack)
    {
      case MarchAllocDlg.MarchType.joinRally:
      case MarchAllocDlg.MarchType.joinGveRally:
      case MarchAllocDlg.MarchType.reinforce:
      case MarchAllocDlg.MarchType.gather:
        int num1 = 0;
        int num2 = 0;
        BenefitInfo benefitInfo = DBManager.inst.DB_Local_Benefit.Get(ConfigManager.inst.DB_Properties["prop_army_load_percent"].InternalID);
        float num3 = 1f;
        if (benefitInfo != null)
          num3 += benefitInfo.total;
        for (int index = 0; index < this.list.Count; ++index)
        {
          if (this.orgDesLoadCount - num1 > Mathf.RoundToInt((float) (this.list[index].troopInfo.GatherCapacity * this.list[index].troopMaxCount) * num3))
          {
            if (this.desTroopCount - num2 > this.list[index].troopMaxCount)
            {
              this.list[index].Max();
              num2 += this.list[index].troopCount;
              string BenefitCalcID = string.Format("{0}_load", (object) this.list[index].troopInfo.Troop_Class_ID.Replace("class_", "calc_"));
              int num4 = this.list[index].troopInfo.GatherCapacity * this.list[index].troopCount;
              num1 += Mathf.FloorToInt(ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) num4, BenefitCalcID, this.GetCalcOption()));
            }
            else
            {
              this.list[index].troopCount = this.desTroopCount - num2;
              return;
            }
          }
          else
          {
            int num4 = (int) Mathf.Ceil((float) (this.orgDesLoadCount - num1) / (num3 * (float) this.list[index].troopInfo.GatherCapacity));
            if (this.desTroopCount - num2 > num4)
            {
              this.list[index].troopCount = num4;
              return;
            }
            this.list[index].troopCount = this.desTroopCount - num2;
            return;
          }
        }
        this.FreshTroopData();
        break;
      default:
        this.OnQuickQueueClick();
        break;
    }
  }

  public class TroopData
  {
    public string troopId;
    public int troopCount;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int slotIndex = -1;
    public bool isChangellen;
  }
}
