﻿// Decompiled with JetBrains decompiler
// Type: ConfigImage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigImage
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, ImageInfo> _datas;

  public void BuildDB(object sources)
  {
    this.parse.Parse<ImageInfo, string>(sources as Hashtable, "ID", out this._datas);
  }

  public ImageInfo GetData(string id)
  {
    if (this._datas == null)
      return (ImageInfo) null;
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (ImageInfo) null;
  }

  public bool IsUITexure(string imageName)
  {
    return string.IsNullOrEmpty(this.GetAltasName(imageName));
  }

  public string GetAltasName(string id)
  {
    ImageInfo data = this.GetData(id);
    if (data != null)
      return data.AltasName;
    return string.Empty;
  }
}
