﻿// Decompiled with JetBrains decompiler
// Type: QuestCounter
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class QuestCounter : MonoBehaviour
{
  public UILabel questName;
  public GameObject container;
  private int count;
  private bool isInit;

  private void OnEnable()
  {
    if (!GameEngine.IsAvailable)
      return;
    this.count = PlayerData.inst.QuestManager.GetCompleteQuestsNumber();
    this.UpdateUI();
  }

  private void OnRecommendChangedHandler(int count)
  {
    this.count = count;
    this.UpdateUI();
  }

  private void Update()
  {
    if (this.isInit || !GameEngine.IsReady())
      return;
    this.AddEventHandler();
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    this.isInit = true;
    PlayerData.inst.QuestManager.OnQuestCompletedCountChanged += new System.Action<int>(this.OnRecommendChangedHandler);
  }

  private void OnDataChanged(long internalId)
  {
    if (!GameEngine.IsReady())
      return;
    this.count = PlayerData.inst.QuestManager.GetCompleteQuestsNumber();
    this.UpdateUI();
  }

  private void RemoveEventHandler()
  {
    PlayerData.inst.QuestManager.OnQuestCompletedCountChanged -= new System.Action<int>(this.OnRecommendChangedHandler);
  }

  public void Dispose()
  {
    this.RemoveEventHandler();
  }

  private void UpdateUI()
  {
    this.container.gameObject.SetActive(this.count > 0);
    this.questName.text = this.count.ToString() + string.Empty;
  }
}
