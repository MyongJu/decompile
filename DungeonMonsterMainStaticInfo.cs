﻿// Decompiled with JetBrains decompiler
// Type: DungeonMonsterMainStaticInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class DungeonMonsterMainStaticInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "name")]
  public string Name;
  [Config(Name = "image")]
  public string Image;
  [Config(Name = "attack")]
  public int Attack;
  [Config(Name = "defence")]
  public int Defence;
  [Config(Name = "health")]
  public int Health;
  [Config(Name = "speed")]
  public int Speed;
  [Config(Name = "rebound")]
  public int Rebound;

  public string LocalName
  {
    get
    {
      return ScriptLocalization.Get(this.Name, true);
    }
  }
}
