﻿// Decompiled with JetBrains decompiler
// Type: HeroUseExpItemComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class HeroUseExpItemComponent : ComponentRenderBase
{
  [SerializeField]
  private UITexture _Texture;
  [SerializeField]
  private UILabel _ItemCountLabel;
  [SerializeField]
  private StandardProgressBar _ProcessBar;
  protected ItemStaticInfo itemInfo;
  public System.Action<int> NotifyValueChanged;
  public HeroUseExpItemComponent.GetReachMaxItemCount reachMaxCounter;
  private int _selectedCount;

  public ItemStaticInfo ItemInfo
  {
    get
    {
      return this.itemInfo;
    }
  }

  public int SelectedCount
  {
    set
    {
      if (this._selectedCount == value)
        return;
      int selectedCount = this._selectedCount;
      this._selectedCount = value;
      if (this.reachMaxCounter != null && value > selectedCount)
      {
        int num = this.reachMaxCounter(this.itemInfo);
        if (value > num)
        {
          this._selectedCount = num;
          this._ProcessBar.CurrentCount = num;
        }
      }
      if (this.NotifyValueChanged == null)
        return;
      this.NotifyValueChanged(value);
    }
    get
    {
      return this._selectedCount;
    }
  }

  public override void Init()
  {
    base.Init();
    HeroUseExpItemComponentData data = this.data as HeroUseExpItemComponentData;
    if (data == null)
      return;
    this.itemInfo = data.ItemInfo;
    BuilderFactory.Instance.HandyBuild((UIWidget) this._Texture, this.itemInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    int quantity = DBManager.inst.DB_Item.GetQuantity(this.itemInfo.internalId);
    this._ItemCountLabel.text = quantity.ToString();
    this._ProcessBar.Init(0, quantity);
    this._ProcessBar.onValueChanged += new System.Action<int>(this.OnValueChanged);
  }

  public void OnValueChanged(int count)
  {
    this.SelectedCount = count;
  }

  public override void Dispose()
  {
    base.Dispose();
  }

  public delegate int GetReachMaxItemCount(ItemStaticInfo selectedItemInfo);
}
