﻿// Decompiled with JetBrains decompiler
// Type: DailyActivesDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DailyActivesDlg : Popup
{
  private List<DailyActivyChest> chests = new List<DailyActivyChest>();
  private List<DailyActivyItem> items = new List<DailyActivyItem>();
  private const float MAX_X = 1881f;
  public UIProgressBar progressBar;
  public DailyActivyChest chestPrefab;
  public DailyActivyItem activyItemPrefab;
  public UISprite borderPrefab;
  public GameObject progressBarContainer;
  public UILabel pointLabelPrefab;
  public UILabel title;
  public UILabel currentPoint;
  public UIScrollView scrollView;
  public UIGrid grid;
  private int remainTimeValue;
  public UILabel remainTime;

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnRewardListShow()
  {
    for (int index = 0; index < this.chests.Count; ++index)
      this.chests[index].HideChestEffect();
  }

  public void OnRewardListClose()
  {
    for (int index = 0; index < this.chests.Count; ++index)
      this.chests[index].ShowChestEffect();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.AddEventHandler();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Clear();
  }

  private void UpdateUI()
  {
    this.title.text = ScriptLocalization.Get("tavern_daily_activity_title", true);
    float num = (float) DBManager.inst.DB_DailyActives.CurrentPoint / (float) ConfigManager.inst.DB_DailyActivies.MaxPoint;
    this.progressBar.value = num;
    this.currentPoint.text = DBManager.inst.DB_DailyActives.CurrentPoint.ToString();
    Vector3 localPosition = this.currentPoint.transform.localPosition;
    localPosition.x = this.progressBar.foregroundWidget.transform.localPosition.x + (float) this.progressBar.foregroundWidget.width * num;
    if ((double) localPosition.x > 1881.0)
      localPosition.x = 1881f;
    this.currentPoint.transform.localPosition = localPosition;
    this.remainTimeValue = 86400 - NetServerTime.inst.ServerTimestamp % 86400;
    this.remainTime.text = Utils.FormatTime(this.remainTimeValue, false, false, true);
    this.CreateAcity();
    this.CreateReward();
  }

  private void Clear()
  {
    for (int index = 0; index < this.items.Count; ++index)
      this.items[index].Clear();
    this.items.Clear();
    for (int index = 0; index < this.chests.Count; ++index)
      this.chests[index].Clear();
    this.chests.Clear();
    this.RemoveEventHandler();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_DailyActives.onDataSetChanged += new System.Action(this.OnDataChange);
    DBManager.inst.DB_DailyActives.onPointChanged += new System.Action(this.OnPointChange);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEventHandler);
  }

  public void OnHelpBtClick()
  {
    UIManager.inst.OpenPopup("HelpPopup", (Popup.PopupParameter) new HelpPopup.Parameter()
    {
      id = "help_dragon_skill"
    });
  }

  private void OnSecondEventHandler(int time)
  {
    --this.remainTimeValue;
    if (this.remainTimeValue <= 0)
    {
      this.remainTimeValue = 0;
      this.RemoveEventHandler();
      this.GetNewData();
    }
    this.remainTime.text = Utils.FormatTime(this.remainTimeValue, false, false, true);
  }

  private void GetNewData()
  {
    ActivityManager.Intance.LoadTimeLimitActivityData(new System.Action(this.GetDataCallBack));
  }

  private void GetDataCallBack()
  {
    this.AddEventHandler();
    this.UpdateUI();
  }

  private void OnPointChange()
  {
    for (int index = 0; index < this.chests.Count; ++index)
      this.chests[index].Refresh();
    float num = (float) DBManager.inst.DB_DailyActives.CurrentPoint / (float) ConfigManager.inst.DB_DailyActivies.MaxPoint;
    this.progressBar.value = num;
    this.currentPoint.text = DBManager.inst.DB_DailyActives.CurrentPoint.ToString();
    Vector3 localPosition = this.currentPoint.transform.localPosition;
    localPosition.x = this.progressBar.foregroundWidget.transform.localPosition.x + (float) this.progressBar.foregroundWidget.width * num;
    this.currentPoint.transform.localPosition = localPosition;
    this.remainTime.text = Utils.FormatTime(86400 - NetServerTime.inst.ServerTimestamp % 86400, false, false, true);
  }

  private void OnDataChange()
  {
    List<DailyActivyInfo> data = this.GetData();
    for (int index = 0; index < data.Count; ++index)
    {
      DailyActivyInfo info = data[index];
      this.items[index].SetData(info);
    }
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_DailyActives.onDataSetChanged -= new System.Action(this.OnDataChange);
    DBManager.inst.DB_DailyActives.onPointChanged -= new System.Action(this.OnPointChange);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEventHandler);
  }

  private void CreateAcity()
  {
    List<DailyActivyInfo> data = this.GetData();
    for (int index = 0; index < data.Count; ++index)
    {
      DailyActivyInfo info = data[index];
      GameObject go = NGUITools.AddChild(this.grid.gameObject, this.activyItemPrefab.gameObject);
      DailyActivyItem component = go.GetComponent<DailyActivyItem>();
      NGUITools.SetActive(go, true);
      component.SetData(info);
      this.items.Add(component);
    }
    this.grid.repositionNow = true;
    this.grid.Reposition();
    this.scrollView.ResetPosition();
  }

  private void CreateReward()
  {
    List<DailyActiveRewardInfo> totalRewardList = ConfigManager.inst.DB_DailyActivies.GetTotalRewardList();
    int requrieScore = totalRewardList[totalRewardList.Count - 1].RequrieScore;
    int width = this.progressBar.foregroundWidget.width;
    Vector3 localPosition = this.progressBar.foregroundWidget.transform.localPosition;
    for (int index = 0; index < totalRewardList.Count; ++index)
      this.CreateRewardUI(totalRewardList[index], localPosition, width, index != totalRewardList.Count - 1);
  }

  private void CreateRewardUI(DailyActiveRewardInfo data, Vector3 orgin, int width, bool needBorder = true)
  {
    float x = orgin.x + (float) width * data.Ratio;
    GameObject go1 = NGUITools.AddChild(this.progressBarContainer, this.pointLabelPrefab.gameObject);
    go1.GetComponent<UILabel>().text = data.RequrieScore.ToString();
    NGUITools.SetActive(go1, true);
    go1.transform.localPosition = new Vector3(x, this.pointLabelPrefab.transform.localPosition.y);
    if (needBorder)
    {
      GameObject go2 = NGUITools.AddChild(this.progressBarContainer, this.borderPrefab.gameObject);
      NGUITools.SetActive(go2, true);
      go2.transform.localPosition = new Vector3(x, this.borderPrefab.transform.localPosition.y);
    }
    GameObject go3 = NGUITools.AddChild(this.progressBarContainer, this.chestPrefab.gameObject);
    NGUITools.SetActive(go3, true);
    DailyActivyChest component = go3.GetComponent<DailyActivyChest>();
    component.onCloseHandler = new System.Action(this.OnRewardListClose);
    component.onShowHandler = new System.Action(this.OnRewardListShow);
    component.SetData(data);
    this.chests.Add(component);
    go3.transform.localPosition = new Vector3(x, this.chestPrefab.transform.localPosition.y);
  }

  public List<DailyActiveRewardInfo> GetRewardInfos()
  {
    return ConfigManager.inst.DB_DailyActivies.GetTotalRewardList();
  }

  private List<DailyActivyInfo> GetData()
  {
    List<DailyActivyInfo> totalActivyInfos = ConfigManager.inst.DB_DailyActivies.GetTotalActivyInfos();
    List<DailyActivyInfo> dailyActivyInfoList1 = new List<DailyActivyInfo>();
    List<DailyActivyInfo> dailyActivyInfoList2 = new List<DailyActivyInfo>();
    List<DailyActivyInfo> dailyActivyInfoList3 = new List<DailyActivyInfo>();
    List<DailyActivyInfo> dailyActivyInfoList4 = new List<DailyActivyInfo>();
    List<DailyActivyInfo> dailyActivyInfoList5 = new List<DailyActivyInfo>();
    for (int index = 0; index < totalActivyInfos.Count; ++index)
    {
      DailyActivyInfo data = totalActivyInfos[index];
      if (this.IsLock(data))
      {
        dailyActivyInfoList2.Add(data);
      }
      else
      {
        DailyActivyData dailyActivyData = DBManager.inst.DB_DailyActives.Get(data.internalId);
        if (dailyActivyData != null && dailyActivyData.IsFinish)
          dailyActivyInfoList4.Add(data);
        else if (dailyActivyData == null || dailyActivyData.Progress == 0)
          dailyActivyInfoList3.Add(data);
        else
          dailyActivyInfoList5.Add(data);
      }
    }
    dailyActivyInfoList4.Sort(new Comparison<DailyActivyInfo>(this.CompareItemWithUnLockLevel));
    dailyActivyInfoList2.Sort(new Comparison<DailyActivyInfo>(this.CompareItemWithUnLockLevel));
    dailyActivyInfoList3.Sort(new Comparison<DailyActivyInfo>(this.CompareItemWithScore));
    dailyActivyInfoList5.Sort(new Comparison<DailyActivyInfo>(this.CompareItemWithUpdateTime));
    for (int index = dailyActivyInfoList5.Count - 1; index >= 0; --index)
      dailyActivyInfoList1.Add(dailyActivyInfoList5[index]);
    for (int index = dailyActivyInfoList3.Count - 1; index >= 0; --index)
      dailyActivyInfoList1.Add(dailyActivyInfoList3[index]);
    for (int index = 0; index < dailyActivyInfoList4.Count; ++index)
      dailyActivyInfoList1.Add(dailyActivyInfoList4[index]);
    for (int index = 0; index < dailyActivyInfoList2.Count; ++index)
      dailyActivyInfoList1.Add(dailyActivyInfoList2[index]);
    dailyActivyInfoList5.Clear();
    dailyActivyInfoList3.Clear();
    dailyActivyInfoList2.Clear();
    dailyActivyInfoList4.Clear();
    return dailyActivyInfoList1;
  }

  private int CompareItemWithUnLockLevel(DailyActivyInfo a, DailyActivyInfo b)
  {
    return a.UnlockLevel.CompareTo(b.UnlockLevel);
  }

  private int CompareItemWithUpdateTime(DailyActivyInfo a, DailyActivyInfo b)
  {
    DailyActivyData dailyActivyData1 = DBManager.inst.DB_DailyActives.Get(a.internalId);
    DailyActivyData dailyActivyData2 = DBManager.inst.DB_DailyActives.Get(b.internalId);
    if (dailyActivyData1.UpdateTime == dailyActivyData2.UpdateTime)
      return this.CompareItemWithScore(a, b);
    return dailyActivyData1.UpdateTime.CompareTo(dailyActivyData2.UpdateTime);
  }

  private int CompareItemWithScore(DailyActivyInfo a, DailyActivyInfo b)
  {
    if (a.MaxScore == b.MaxScore)
      return this.CompareItemWithScoreId(a, b);
    return a.MaxScore.CompareTo(b.MaxScore);
  }

  private int CompareItemWithScoreId(DailyActivyInfo a, DailyActivyInfo b)
  {
    return a.internalId.CompareTo(b.internalId);
  }

  private bool IsLock(DailyActivyInfo data)
  {
    return CityManager.inst.mStronghold.mLevel < data.UnlockLevel;
  }
}
