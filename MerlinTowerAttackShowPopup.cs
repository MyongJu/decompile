﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerAttackShowPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;

public class MerlinTowerAttackShowPopup : Popup
{
  public UITexture tex;
  private bool _isWin;
  private Hashtable _warReportData;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    MerlinTowerAttackShowPopup.Parameter parameter = orgParam as MerlinTowerAttackShowPopup.Parameter;
    this._isWin = parameter.isWin;
    this._warReportData = parameter.warReportData;
    MerlinTowerAttachShowManager.Instance.IsWin = this._isWin;
    MerlinTowerAttachShowManager.Instance.Startup(parameter.para, parameter.towerData);
    MerlinTowerAttachShowManager.Instance.onAttackShowBegin += new System.Action(this.ShowRenderTexture);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    MerlinTowerAttachShowManager.Instance.onAttackShowBegin -= new System.Action(this.ShowRenderTexture);
    MerlinTowerAttachShowManager.Instance.Terminate();
    MerlinTowerAttachShowManager.Instance.StopAttackShowCoroutine();
    this.ShowReport();
  }

  public void OnCloseButtonClicked()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  private void ShowRenderTexture()
  {
    this.tex.gameObject.SetActive(true);
  }

  private void ShowReport()
  {
    if (this._warReportData == null)
      return;
    UIManager.inst.OpenPopup("MerlinTower/MerlinTowerWarReportPopup", (Popup.PopupParameter) new MerlinTowerWarReportPopup.Parameter()
    {
      winWar = this._isWin,
      mailData = this._warReportData,
      isFightMonster = true
    });
  }

  public class Parameter : Popup.PopupParameter
  {
    public MerlinTowerAttackShowPara para;
    public UserMerlinTowerData towerData;
    public bool isWin;
    public Hashtable warReportData;
  }
}
