﻿// Decompiled with JetBrains decompiler
// Type: GemItemSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class GemItemSlot : MonoBehaviour
{
  public UITexture m_GemIcon;
  public UILabel m_GemLevel;
  public UILabel m_GemBenefit;
  public UIToggle m_Toggle;
  private GemData m_GemData;
  private System.Action<GemItemSlot> m_Callback;
  public UIGrid grid;

  public void SetData(GemData gemData, System.Action<GemItemSlot> callback)
  {
    this.m_GemData = gemData;
    this.m_Callback = callback;
    this.UpdateUI();
  }

  public void OnToggleChanged()
  {
    if (!this.m_Toggle.value)
      return;
    this.m_Callback(this);
  }

  private UILabel GetBenefitItem()
  {
    GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.m_GemBenefit.gameObject);
    gameObject.SetActive(true);
    return gameObject.GetComponent<UILabel>();
  }

  private void UpdateUI()
  {
    ConfigEquipmentGemInfo data = ConfigManager.inst.DB_EquipmentGem.GetData(this.m_GemData.ConfigId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_GemIcon, data.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    this.m_GemLevel.text = Utils.XLAT("id_lv") + " " + data.level.ToString();
    List<Benefits.BenefitValuePair> benefitsPairs = data.benefits.GetBenefitsPairs();
    for (int index = 0; index < benefitsPairs.Count; ++index)
    {
      Benefits.BenefitValuePair benefitValuePair = benefitsPairs[index];
      PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitValuePair.internalID];
      this.GetBenefitItem().text = dbProperty.Name + " " + dbProperty.ConvertToDisplayString((double) benefitValuePair.value, true, true);
    }
    this.grid.Reposition();
  }

  public ConfigEquipmentGemInfo GemConfig
  {
    get
    {
      return ConfigManager.inst.DB_EquipmentGem.GetData(this.m_GemData.ConfigId);
    }
  }

  public GemData GemData
  {
    get
    {
      return this.m_GemData;
    }
  }

  public bool Selected
  {
    get
    {
      return this.m_Toggle.value;
    }
  }
}
