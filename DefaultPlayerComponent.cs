﻿// Decompiled with JetBrains decompiler
// Type: DefaultPlayerComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;

public class DefaultPlayerComponent : ComponentRenderBase
{
  public UITexture iconTexture;
  public UILabel kxyLabel;
  public UILabel cityDefenceLabel;
  public UILabel playerLabel;
  public UILabel lvLabel;
  public UILabel troopAmountLabel;
  public string lv;
  public string cityDefence;
  private DefaultPlayerComponentData d;

  public DefaultPlayerComponentData PlayerData
  {
    get
    {
      return this.d;
    }
  }

  public override void Init()
  {
    base.Init();
    this.d = this.data as DefaultPlayerComponentData;
    if (this.d == null)
    {
      this.gameObject.SetActive(false);
    }
    else
    {
      CustomIconLoader.Instance.requestCustomIcon(this.iconTexture, UserData.BuildPortraitPath(int.Parse(this.d.portrait)), this.d.icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.iconTexture, this.d.lordTitleId, 1);
      this.kxyLabel.text = string.IsNullOrEmpty(this.d.k) ? string.Empty : string.Format("[u]X:{0} Y:{1}[-]", (object) this.d.x, (object) this.d.y);
      this.cityDefenceLabel.text = this.d.city_defence;
      this.playerLabel.text = string.Format("{0}{1}", !string.IsNullOrEmpty(this.d.acronym) ? (object) ("(" + this.d.acronym + ")") : (object) string.Empty, (object) this.d.user_name);
      this.lvLabel.text = this.lv;
      this.cityDefenceLabel.text = this.cityDefence;
      if (!string.IsNullOrEmpty(this.d.level))
        this.lvLabel.text = this.d.level;
      if (string.IsNullOrEmpty(this.d.troopAmount) || !((UnityEngine.Object) this.troopAmountLabel != (UnityEngine.Object) null))
        return;
      this.troopAmountLabel.text = Utils.XLAT("id_total_troops") + this.d.troopAmount;
    }
  }

  public void OnGotoOwnerClick()
  {
    if (!MapUtils.CanGotoTarget(int.Parse(this.d.k)))
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    }
    else
    {
      PVPMap.PendingGotoRequest = new Coordinate(int.Parse(this.d.k), int.Parse(this.d.x), int.Parse(this.d.y));
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
        CitadelSystem.inst.OnLoadWorldMap();
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }
}
