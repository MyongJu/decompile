﻿// Decompiled with JetBrains decompiler
// Type: CityCamera
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class CityCamera : MonoBehaviour
{
  [Tooltip("An offset for followObject")]
  public Vector3 followOffset = Vector3.zero;
  public float moveSpeed = 20f;
  public float zoomSpeed = 1f;
  [Tooltip("The smooth factor for drag, the bigger mean more smooth")]
  public float dragSmoothFactor = 0.05f;
  [Tooltip("The distance for smooth drag, the bigger mean longer")]
  public float dragSmoothDist = 100f;
  [Tooltip("The smooth factor for drag ease(after your drag), the bigger mean more smooth")]
  public float dragSmoothEaseFactor = 0.3f;
  [Tooltip("The smooth factor for zoom, the bigger mean more smooth")]
  public float zoomSmoothFactor = 0.3f;
  [Tooltip("The bump factor for zoom, the bigger mean more smooth")]
  public float zoomBumpFactor = 0.6f;
  [Tooltip("The start zoom distance")]
  public float zoomStartDis = 0.8f;
  [Tooltip("The minimum zoom distance")]
  public float zoomMinDis = 0.6f;
  [Tooltip("The maximum zoom distance")]
  public float zoomMaxDis = 1.3f;
  [Tooltip("The buffer of zoom in & zoom out")]
  public float zoomBufferDist = 0.1f;
  private bool _lockInput = true;
  private Vector3 _cameraVelocity = Vector3.zero;
  private Vector3 _cameraPreviousDrag = Vector3.zero;
  private Rect _outerBound = new Rect();
  private Vector3 _targetPosition = Vector3.zero;
  private Rect _boundary = new Rect();
  public float boundBuffer = 50f;
  private Rect _innerBound = new Rect();
  public float smoothFactor = 0.85f;
  private Vector3 _curDragSpeed = Vector3.zero;
  private Vector3 _lastDragSpeed = Vector3.zero;
  private Vector3 _moveUnit = Vector3.zero;
  private Stack<CityCamera.CameraArgs> _argStack = new Stack<CityCamera.CameraArgs>();
  [Tooltip("Which object you want to follow")]
  public GameObject followObject;
  private Transform _cacheForm;
  private Camera _camera;
  private bool _draging;
  private bool _rotating;
  private bool _pinching;
  private bool _gestureEvent;
  private bool _chatFocus;
  private float _zoomVelocity;
  private Plane _xyPlane;
  private DragRecognizer _fingerDrag;
  private PinchRecognizer _fingerPinch;
  private CityCamera.Action _lastAction;
  private float _targetZoom;
  private bool _useBoundary;
  public System.Action<Vector2> onCityCameraMove;
  private bool _inInertiaMove;
  private bool _inFocusMove;
  private float _originZoom;
  private Vector3 _pinchWPos;
  private Vector3 _pinchSPos;
  private bool _hasRecord;
  private Vector3 _cachePos;

  public event CityCamera.OnDragStart onDragStart;

  public event System.Action<bool> OnFocusTranslationChange;

  public bool IsInputLocked()
  {
    return this._lockInput;
  }

  public bool LockInput
  {
    set
    {
      this._lockInput = value;
      this.LockGesture(this._lockInput || this._chatFocus);
    }
  }

  public bool ChatFocus
  {
    set
    {
      this._chatFocus = value;
      this.LockGesture(this._lockInput || this._chatFocus);
    }
  }

  public float TargetZoom
  {
    get
    {
      return this._targetZoom;
    }
    set
    {
      this._targetZoom = value;
    }
  }

  public Vector3 TargetPosition
  {
    get
    {
      return this._targetPosition;
    }
  }

  public bool UseBoundary
  {
    get
    {
      return this._useBoundary;
    }
    set
    {
      this._useBoundary = value;
    }
  }

  public Rect Boundary
  {
    get
    {
      return this._boundary;
    }
    set
    {
      this._boundary = value;
      this.RebuildCameraPositionBoundary();
    }
  }

  public void SetTargetPosition(Vector3 inTarget, bool inSmoothTranslation, bool lockInput = true)
  {
    inTarget.z = this.transform.position.z;
    this._targetPosition = !this.UseBoundary ? inTarget : Mathfx.ClampWithRect(inTarget, this._outerBound);
    this.InFocusMoving = inSmoothTranslation;
    if (this.InFocusMoving)
    {
      this.LockInput = lockInput;
      this._lastAction = CityCamera.Action.Drag;
    }
    else
      this._cacheForm.position = this._targetPosition;
  }

  public bool InAction()
  {
    if (!this._draging && !this._rotating)
      return this._pinching;
    return true;
  }

  private void Awake()
  {
    this._cacheForm = this.transform;
    this._fingerDrag = this.GetComponent<DragRecognizer>();
    this._fingerPinch = this.GetComponent<PinchRecognizer>();
    this._xyPlane = new Plane(Vector3.back, Vector3.zero);
    this._camera = this.GetComponent<Camera>();
  }

  private void Start()
  {
    this._targetZoom = Mathf.Clamp(this.zoomStartDis, this.zoomMinDis, this.zoomMaxDis);
    if (!((UnityEngine.Object) this.followObject != (UnityEngine.Object) null))
      return;
    this._cacheForm.position = this.followObject.transform.position + this.followOffset;
  }

  private void OnEnable()
  {
    this.ClearTransition();
    this._chatFocus = false;
    this.LockInput = false;
    this.MeasureUnit();
    this._lastAction = CityCamera.Action.None;
  }

  private void OnDisable()
  {
    this._lastAction = CityCamera.Action.None;
    this._chatFocus = false;
    this.LockInput = true;
  }

  private void LockGesture(bool inLock)
  {
    if (inLock)
    {
      if (!this._gestureEvent)
        return;
      this._fingerDrag.OnGesture -= new GestureRecognizerTS<DragGesture>.GestureEventHandler(this.CameraOnDrag);
      this._fingerDrag.enabled = false;
      if ((double) this.zoomSpeed > 0.0)
      {
        this._fingerPinch.enabled = false;
        this._fingerPinch.OnGesture -= new GestureRecognizerTS<PinchGesture>.GestureEventHandler(this.CameraOnPinch);
      }
      this._gestureEvent = false;
      this._draging = false;
      this._pinching = false;
    }
    else
    {
      if (this._gestureEvent)
        return;
      this._fingerDrag.enabled = true;
      this._fingerDrag.OnGesture += new GestureRecognizerTS<DragGesture>.GestureEventHandler(this.CameraOnDrag);
      if ((double) this.zoomSpeed > 0.0)
      {
        this._fingerPinch.enabled = true;
        this._fingerPinch.OnGesture += new GestureRecognizerTS<PinchGesture>.GestureEventHandler(this.CameraOnPinch);
      }
      this._gestureEvent = true;
    }
  }

  private void RebuildCameraPositionBoundary()
  {
    float num = (float) Screen.width / (float) Screen.height * this._camera.orthographicSize;
    this._outerBound.xMin = this._boundary.xMin + num;
    this._outerBound.xMax = this._boundary.xMax - num;
    float orthographicSize = this._camera.orthographicSize;
    this._outerBound.yMin = this._boundary.yMin + orthographicSize;
    this._outerBound.yMax = this._boundary.yMax - orthographicSize;
    this._innerBound = this._outerBound;
    this._innerBound.xMin += this.boundBuffer;
    this._innerBound.xMax -= this.boundBuffer;
    this._innerBound.yMin += this.boundBuffer;
    this._innerBound.yMax -= this.boundBuffer;
  }

  public void FocusTarget(Transform t, CityCamera.FocusMode m)
  {
    switch (m)
    {
      case CityCamera.FocusMode.Edge:
        Bounds absoluteWidgetBounds = NGUIMath.CalculateAbsoluteWidgetBounds(t);
        Rect rect1 = new Rect(absoluteWidgetBounds.min.x, absoluteWidgetBounds.min.y, absoluteWidgetBounds.size.x, absoluteWidgetBounds.size.y);
        float num = (float) Screen.width / (float) Screen.height * this._camera.orthographicSize;
        float orthographicSize = this._camera.orthographicSize;
        Rect rect2 = new Rect(this.transform.position.x - num, this.transform.position.y - 0.65f * orthographicSize, 2f * num, 1.6f * orthographicSize);
        if (rect2.Contains(rect1.min) && rect2.Contains(rect1.max))
          break;
        Vector2 zero = Vector2.zero;
        if ((double) rect2.xMin > (double) rect1.xMin)
          zero.x = rect1.xMin - rect2.xMin;
        if ((double) rect2.xMax < (double) rect1.xMax)
          zero.x = rect1.xMax - rect2.xMax;
        if ((double) rect2.yMin > (double) rect1.yMin)
          zero.y = rect1.yMin - rect2.yMin;
        if ((double) rect2.yMax < (double) rect1.yMax)
          zero.y = rect1.yMax - rect2.yMax;
        this.SetTargetPosition(new Vector3(this.transform.position.x + zero.x, this.transform.position.y + zero.y), true, true);
        break;
      case CityCamera.FocusMode.Center:
        this.SetTargetPosition(t.position, true, false);
        break;
    }
  }

  private bool IsInViewPort(Transform t)
  {
    return true;
  }

  private void LateUpdate()
  {
    this.UpdateZoom();
    this.UpdateMove();
    if (!this.UseBoundary || this._draging || (this._pinching || this.InTransition) || Mathfx.IsInRect(this._cacheForm.position, this._innerBound))
      return;
    this.SetTargetPosition(Mathfx.ClampWithRect(this._cacheForm.position, this._innerBound), true, false);
  }

  public bool InTransition
  {
    get
    {
      if (!this._inInertiaMove)
        return this._inFocusMove;
      return true;
    }
  }

  private bool InInertiaMoving
  {
    get
    {
      return this._inInertiaMove;
    }
    set
    {
      if (!(this._inInertiaMove = value))
        return;
      this._inFocusMove = false;
    }
  }

  private bool InFocusMoving
  {
    get
    {
      return this._inFocusMove;
    }
    set
    {
      if (this._inFocusMove != value && this.OnFocusTranslationChange != null)
        this.OnFocusTranslationChange(value);
      if (!(this._inFocusMove = value))
        return;
      this._inInertiaMove = false;
    }
  }

  public void ClearTransition()
  {
    this._inFocusMove = this._inInertiaMove = false;
  }

  private void UpdateMove()
  {
    Rect outerBound = this._outerBound;
    if (this._lastAction == CityCamera.Action.Drag && this.InTransition)
    {
      if (this._inInertiaMove)
      {
        Vector3 position = this._cacheForm.position;
        position.x += this._curDragSpeed.x;
        position.y += this._curDragSpeed.y;
        this._curDragSpeed.x *= this.smoothFactor;
        this._curDragSpeed.y *= this.smoothFactor;
        this._cacheForm.position = position;
        if ((double) this._curDragSpeed.sqrMagnitude < 0.00999999977648258)
          this.InInertiaMoving = false;
      }
      else if (this._inFocusMove)
      {
        this._cacheForm.position = (double) this.dragSmoothEaseFactor <= 0.0 ? this._targetPosition : Vector3.SmoothDamp(this._cacheForm.position, this._targetPosition, ref this._cameraVelocity, this.dragSmoothEaseFactor);
        if ((double) (this._targetPosition - this._cacheForm.position).sqrMagnitude < 0.100000001490116)
        {
          this._inFocusMove = false;
          this.LockInput = false;
        }
      }
    }
    else if ((UnityEngine.Object) this.followObject != (UnityEngine.Object) null)
      this._cacheForm.position = this.followObject.transform.position + this.followOffset;
    if (this._draging && (double) this.dragSmoothFactor > 0.0)
    {
      this._cacheForm.position = Vector3.SmoothDamp(this._cameraPreviousDrag, this._cacheForm.position, ref this._cameraVelocity, this.dragSmoothFactor);
      this._cameraPreviousDrag = this._cacheForm.position;
    }
    if (this._useBoundary)
    {
      Vector3 position = this._cacheForm.position;
      position.x = Mathf.Clamp(position.x, outerBound.xMin, outerBound.xMax);
      position.y = Mathf.Clamp(position.y, outerBound.yMin, outerBound.yMax);
      this._cacheForm.position = position;
    }
    if (this.onCityCameraMove != null && Vector3.zero != this._cachePos)
    {
      Vector3 vector3 = this._cacheForm.position - this._cachePos;
      this.onCityCameraMove(new Vector2(vector3.x, vector3.y));
    }
    this._cachePos = this._cacheForm.position;
    this._lastDragSpeed = (this._curDragSpeed + this._lastDragSpeed) / 2f;
  }

  private void CameraOnDrag(DragGesture inGesture)
  {
    if (this._rotating || this._pinching || (bool) ((UnityEngine.Object) this.followObject))
      return;
    if (inGesture.Phase == ContinuousGesturePhase.Started)
    {
      this._lastAction = CityCamera.Action.Drag;
      this._draging = true;
      this.ClearTransition();
      this._cameraPreviousDrag = this._cacheForm.position;
      this._lastDragSpeed = Vector3.zero;
      if (this.onDragStart == null)
        return;
      this.onDragStart();
    }
    else if (inGesture.Phase == ContinuousGesturePhase.Ended)
    {
      this._draging = false;
      if ((double) this._curDragSpeed.sqrMagnitude > 1.0 / 1000.0)
      {
        this.InInertiaMoving = true;
      }
      else
      {
        if ((double) this._lastDragSpeed.sqrMagnitude <= 0.100000001490116)
          return;
        this._curDragSpeed = this._lastDragSpeed;
        this.InInertiaMoving = true;
      }
    }
    else
    {
      if (inGesture.Phase != ContinuousGesturePhase.Updated)
        return;
      Vector3 position1 = this._cacheForm.position;
      this._curDragSpeed.x = inGesture.DeltaMove.x * this._moveUnit.x * this._camera.orthographicSize / this._originZoom;
      this._curDragSpeed.y = inGesture.DeltaMove.y * this._moveUnit.y * this._camera.orthographicSize / this._originZoom;
      position1.x += this._curDragSpeed.x;
      position1.y += this._curDragSpeed.y;
      this._cacheForm.position = position1;
      if (!this._useBoundary)
        return;
      Vector3 position2 = this._cacheForm.position;
      position2.x = Mathf.Clamp(position2.x, this._outerBound.xMin, this._outerBound.xMax);
      position2.y = Mathf.Clamp(position2.y, this._outerBound.yMin, this._outerBound.yMax);
      this._cacheForm.position = position2;
    }
  }

  private void UpdateZoom()
  {
    if (!this._pinching)
      this._targetZoom = Mathf.Clamp(this._targetZoom, this.zoomMinDis, this.zoomMaxDis);
    if (!((UnityEngine.Object) this._camera != (UnityEngine.Object) null))
      return;
    float orthographicSize = this._camera.orthographicSize;
    float smoothTime = this.zoomSmoothFactor;
    if ((double) orthographicSize > (double) this.zoomMaxDis && (double) orthographicSize > (double) this._targetZoom || (double) orthographicSize < (double) this.zoomMinDis && (double) orthographicSize < (double) this._targetZoom)
      smoothTime = this.zoomBumpFactor;
    this._camera.orthographicSize = (double) smoothTime <= 0.0 ? this._targetZoom : Mathf.SmoothDamp(orthographicSize, this._targetZoom, ref this._zoomVelocity, smoothTime, this.zoomSpeed);
    if (!Mathf.Approximately(orthographicSize, this._camera.orthographicSize))
    {
      this.RebuildCameraPositionBoundary();
      this.AdjustPinchPos();
      if (!this.InTransition || !this.UseBoundary)
        return;
      this._targetPosition = Mathfx.ClampWithRect(this._targetPosition, this._outerBound);
    }
    else
    {
      if (this._pinching)
        return;
      this.ClearPinchRecord();
    }
  }

  private void CameraOnPinch(PinchGesture inGesture)
  {
    if (inGesture.Phase == ContinuousGesturePhase.Started)
    {
      this._lastAction = CityCamera.Action.Pinch;
      this.RecordPinchPos((Vector3) inGesture.Position);
      this._pinching = true;
    }
    else if (this._pinching && inGesture.Phase == ContinuousGesturePhase.Ended)
    {
      this._pinching = false;
    }
    else
    {
      if (!this._pinching || inGesture.Phase != ContinuousGesturePhase.Updated)
        return;
      this._targetZoom = Mathf.Clamp(this._targetZoom - inGesture.Delta, this.zoomMinDis - this.zoomBufferDist, this.zoomMaxDis + this.zoomBufferDist);
    }
  }

  private void MeasureUnit()
  {
    float enter1 = 0.0f;
    float enter2 = 0.0f;
    Camera component = this.GetComponent<Camera>();
    Ray ray1 = component.ScreenPointToRay((Vector3) new Vector2(0.0f, 0.0f));
    Ray ray2 = component.ScreenPointToRay((Vector3) new Vector2(1f, 1f));
    if (this._xyPlane.Raycast(ray1, out enter1) && this._xyPlane.Raycast(ray2, out enter2))
    {
      Vector3 vector3_1 = ray1.origin + ray1.direction * enter1;
      Vector3 vector3_2 = ray2.origin + ray2.direction * enter2 - vector3_1;
      this._moveUnit.x = -vector3_2.x;
      this._moveUnit.y = -vector3_2.y;
    }
    this._originZoom = this._camera.orthographicSize;
  }

  private void RecordPinchPos(Vector3 screenPos)
  {
    this._pinchSPos = screenPos;
    Ray ray = this._camera.ScreenPointToRay(screenPos);
    float enter = 0.0f;
    if (this._xyPlane.Raycast(ray, out enter))
      this._pinchWPos = ray.origin + ray.direction * enter;
    this._hasRecord = true;
  }

  private void AdjustPinchPos()
  {
    if (!this._hasRecord || this._lastAction != CityCamera.Action.Pinch)
      return;
    Ray ray = this._camera.ScreenPointToRay(this._pinchSPos);
    float enter = 0.0f;
    Vector3 vector3 = Vector3.zero;
    if (this._xyPlane.Raycast(ray, out enter))
      vector3 = ray.origin + ray.direction * enter;
    this._cacheForm.Translate(this._pinchWPos - vector3);
  }

  private void ClearPinchRecord()
  {
    if (!this._hasRecord)
      return;
    this._hasRecord = false;
  }

  public void SimulatePinch(Vector3 screenpos, float zoomTarget)
  {
    this._lastAction = CityCamera.Action.Pinch;
    this.RecordPinchPos(screenpos);
    this._targetZoom = Mathf.Clamp(zoomTarget, this.zoomMinDis, this.zoomMaxDis);
    this._pinching = false;
  }

  public void PushArg(CityCamera.CameraArgs arg)
  {
    if (arg == null)
      return;
    this.SaveArg();
    this.dragSmoothEaseFactor = arg.dragFactor;
    this.zoomSmoothFactor = arg.zoomFactor;
  }

  public CityCamera.CameraArgs PopArg()
  {
    if (this._argStack.Count == 0)
      return (CityCamera.CameraArgs) null;
    CityCamera.CameraArgs cameraArgs = this._argStack.Pop();
    this.ApplyArg(cameraArgs);
    return cameraArgs;
  }

  public void ApplyArg(CityCamera.CameraArgs arg)
  {
    if (arg == null)
      return;
    this.dragSmoothEaseFactor = arg.dragFactor;
    this.zoomSmoothFactor = arg.zoomFactor;
  }

  public void SaveArg()
  {
    this._argStack.Push(new CityCamera.CameraArgs()
    {
      dragFactor = this.dragSmoothEaseFactor,
      zoomFactor = this.zoomSmoothFactor
    });
  }

  public void RevertArgs()
  {
    if (this._argStack.Count > 0)
      this.ApplyArg(this._argStack.Peek());
    this.ClearArgs();
  }

  public void ClearArgs()
  {
    this._argStack.Clear();
  }

  public enum Action
  {
    None,
    Drag,
    Pinch,
    _Count,
  }

  public enum FocusMode
  {
    Edge,
    Center,
  }

  public class CameraArgs
  {
    public float zoomFactor;
    public float dragFactor;
  }

  public delegate void OnDragStart();
}
