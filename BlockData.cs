﻿// Decompiled with JetBrains decompiler
// Type: BlockData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.IO;
using UnityEngine;

public class BlockData
{
  private Coordinate m_Origin = new Coordinate();
  private WorldCoordinate m_WorldOrigin = new WorldCoordinate();
  private Rect m_Dimension = new Rect();
  public const string TERRAIN_JSON = "TERRAIN";
  public const string REFERENCE_JSON = "REFERENCE";
  public const string X_JSON = "X";
  public const string Y_JSON = "Y";
  public const string X_REF_JSON = "X_REF";
  public const string Y_REF_JSON = "Y_REF";
  private string m_JsonFilePath;
  private TileData[,] m_Tiles;
  private int m_State;
  private bool m_ViewportChanged;
  private bool m_LastDisplaying;
  private DynamicMapData m_MapData;

  public Rect Viewport { protected set; get; }

  public DynamicMapData MapData
  {
    get
    {
      return this.m_MapData;
    }
  }

  public bool IsLoaded
  {
    get
    {
      return this.m_State == 3;
    }
  }

  public bool CanShow
  {
    get
    {
      if ((this.m_State & 1) <= 0)
        return (this.m_State & 2) > 0;
      return true;
    }
  }

  public int BlockID
  {
    get
    {
      return BlockData.CalculateBlockID(this.m_Origin.X, this.m_Origin.Y, this.m_MapData);
    }
  }

  public int KingdomID
  {
    get
    {
      return this.m_Origin.K;
    }
  }

  public Coordinate Origin
  {
    get
    {
      return this.m_Origin;
    }
  }

  public WorldCoordinate WorldOrigin
  {
    get
    {
      return this.m_WorldOrigin;
    }
  }

  private void InitBlock()
  {
    int x = (int) this.m_MapData.TilesPerBlock.x;
    int y = (int) this.m_MapData.TilesPerBlock.y;
    this.m_Tiles = new TileData[x, y];
    for (int index1 = 0; index1 < x; ++index1)
    {
      for (int index2 = 0; index2 < y; ++index2)
      {
        if ((index1 & 1 ^ index2 & 1) == 0)
        {
          TileData tileData = new TileData(this);
          this.m_Tiles[index1, index2] = tileData;
        }
        else
          this.m_Tiles[index1, index2] = (TileData) null;
      }
    }
  }

  public void Reset(Coordinate location, DynamicMapData mapData)
  {
    if (this.m_MapData != mapData)
    {
      this.m_MapData = mapData;
      this.InitBlock();
    }
    int blockXindexByTileX = this.m_MapData.GetBlockXIndexByTileX(location.X);
    int blockYindexByTileY = this.m_MapData.GetBlockYIndexByTileY(location.Y);
    int x = (int) this.m_MapData.TilesPerBlock.x;
    int y = (int) this.m_MapData.TilesPerBlock.y;
    this.m_JsonFilePath = string.Format(mapData.LoadAssetPath + "k{0}_x{1}_y{2}", (object) 1, (object) blockXindexByTileX, (object) blockYindexByTileY);
    this.m_Origin.K = location.K;
    this.m_Origin.X = blockXindexByTileX * x;
    this.m_Origin.Y = blockYindexByTileY * y;
    Vector3 pixelPosition = this.m_MapData.ConvertTileKXYToPixelPosition(this.m_Origin);
    this.m_Dimension.x = pixelPosition.x;
    this.m_Dimension.y = pixelPosition.y;
    this.m_Dimension.width = this.m_MapData.PixelsPerBlock.x;
    this.m_Dimension.height = -this.m_MapData.PixelsPerBlock.y;
    this.m_WorldOrigin = this.m_MapData.ConvertPixelPositionToWorldCoordinate((Vector2) pixelPosition);
    for (int index1 = 0; index1 < x; ++index1)
    {
      for (int index2 = 0; index2 < y; ++index2)
      {
        TileData tile = this.m_Tiles[index1, index2];
        if (tile != null)
        {
          tile.Reset();
          tile.Location = this.m_Origin;
          tile.Location.X += index1;
          tile.Location.Y += index2;
          tile.WorldLocation = this.m_WorldOrigin;
          tile.WorldLocation.X += index1;
          tile.WorldLocation.Y += index2;
          tile.Position.x = this.m_Dimension.x + this.m_MapData.PixelsPerQuarterTile.x * (float) index1;
          tile.Position.y = this.m_Dimension.y - this.m_MapData.PixelsPerQuarterTile.y * (float) index2;
          tile.Position.z = 0.0f;
        }
      }
    }
    this.m_State = 0;
  }

  public void Dispose()
  {
    int x = (int) this.m_MapData.TilesPerBlock.x;
    int y = (int) this.m_MapData.TilesPerBlock.y;
    for (int index1 = 0; index1 < x; ++index1)
    {
      for (int index2 = 0; index2 < y; ++index2)
      {
        TileData tile = this.m_Tiles[index1, index2];
        if (tile != null)
          tile.Reset();
      }
    }
    this.m_Origin = new Coordinate();
    this.m_Dimension = new Rect();
    this.m_State = 0;
  }

  public Rect Dimension
  {
    get
    {
      return this.m_Dimension;
    }
  }

  public void UpdateViewport(Rect viewport)
  {
    this.Viewport = viewport;
    this.m_ViewportChanged = true;
  }

  public void CullAndShow(Transform transform)
  {
    if (!this.m_ViewportChanged || !this.CanShow)
      return;
    int x = (int) this.m_MapData.TilesPerBlock.x;
    int y = (int) this.m_MapData.TilesPerBlock.y;
    bool flag = this.Viewport.Overlaps(this.m_Dimension, true);
    if (flag)
    {
      for (int index1 = 0; index1 < x; ++index1)
      {
        for (int index2 = 0; index2 < y; ++index2)
          this.ShowOrHide(this.Viewport, this.m_Tiles[index1, index2]);
      }
    }
    else if (this.m_LastDisplaying)
    {
      for (int index1 = 0; index1 < x; ++index1)
      {
        for (int index2 = 0; index2 < y; ++index2)
        {
          TileData tile = this.m_Tiles[index1, index2];
          if (tile != null)
            tile.Hide();
        }
      }
    }
    this.m_LastDisplaying = flag;
    this.m_ViewportChanged = false;
  }

  public void UpdateUI()
  {
    int x = (int) this.m_MapData.TilesPerBlock.x;
    int y = (int) this.m_MapData.TilesPerBlock.y;
    for (int index1 = 0; index1 < x; ++index1)
    {
      for (int index2 = 0; index2 < y; ++index2)
        this.UpdateTileUI(this.Viewport, this.m_Tiles[index1, index2]);
    }
  }

  public void BeginLoading()
  {
    if (this.m_State != 0)
      return;
    this.LoadTerrain();
  }

  public void EndLoading()
  {
    this.m_State |= 2;
  }

  public bool ContainsPoint(int k, int x, int y)
  {
    if (this.m_Origin.K != k)
      return false;
    int x1 = (int) this.m_MapData.TilesPerBlock.x;
    int y1 = (int) this.m_MapData.TilesPerBlock.y;
    int num1 = x - this.m_Origin.X;
    int num2 = y - this.m_Origin.Y;
    if (0 <= num1 && num1 < x1 && 0 <= num2)
      return num2 < y1;
    return false;
  }

  public bool ContainsPoint(Coordinate location)
  {
    return this.ContainsPoint(location.K, location.X, location.Y);
  }

  public bool ContainsPoint(Vector2 point)
  {
    return this.ContainsPoint(this.m_MapData.ConvertPixelPositionToTileKXY((Vector3) point));
  }

  public TileData GetTileAt(int k, int x, int y)
  {
    if (this.ContainsPoint(k, x, y))
      return this.m_Tiles[x - this.m_Origin.X, y - this.m_Origin.Y];
    return (TileData) null;
  }

  public TileData GetTileAt(Coordinate location)
  {
    return this.GetTileAt(location.K, location.X, location.Y);
  }

  private TileData GetTileAt(int x, int y)
  {
    return this.GetTileAt(this.m_Origin.K, x, y);
  }

  public TileData GetSingleNearFarmOrSwamill(Coordinate location)
  {
    int x = (int) this.m_MapData.TilesPerBlock.x;
    int y = (int) this.m_MapData.TilesPerBlock.y;
    WorldCoordinate worldCoordinate1 = this.m_MapData.ConvertTileKXYToWorldCoordinate(location);
    TileData tileData = (TileData) null;
    float num1 = float.MaxValue;
    for (int index1 = 0; index1 < x; ++index1)
    {
      for (int index2 = 0; index2 < y; ++index2)
      {
        TileData tile = this.m_Tiles[index1, index2];
        if (tile != null && tile.OwnerID == 0L && (tile.TileType == TileType.Resource1 || tile.TileType == TileType.Resource2))
        {
          WorldCoordinate worldCoordinate2 = this.m_MapData.ConvertTileKXYToWorldCoordinate(tile.Location);
          float num2 = (float) WorldCoordinate.DistanceSquared(worldCoordinate1, worldCoordinate2);
          if ((double) num2 < (double) num1 && !DBManager.inst.DB_March.HasMarchTowardTargetLocation(tile.Location))
          {
            tileData = tile;
            num1 = num2;
          }
        }
      }
    }
    return tileData;
  }

  public TileData GetSingleNearMonster(Coordinate location)
  {
    int x = (int) this.m_MapData.TilesPerBlock.x;
    int y = (int) this.m_MapData.TilesPerBlock.y;
    WorldCoordinate worldCoordinate1 = this.m_MapData.ConvertTileKXYToWorldCoordinate(location);
    TileData tileData = (TileData) null;
    float num1 = float.MaxValue;
    for (int index1 = 0; index1 < x; ++index1)
    {
      for (int index2 = 0; index2 < y; ++index2)
      {
        TileData tile = this.m_Tiles[index1, index2];
        if (tile != null && tile.OwnerID == 0L && (tile.TileType == TileType.Encounter && tile.Level == 1))
        {
          WorldCoordinate worldCoordinate2 = this.m_MapData.ConvertTileKXYToWorldCoordinate(tile.Location);
          float num2 = (float) WorldCoordinate.DistanceSquared(worldCoordinate1, worldCoordinate2);
          if ((double) num2 < (double) num1 && !DBManager.inst.DB_March.HasMarchTowardTargetLocation(tile.Location))
          {
            tileData = tile;
            num1 = num2;
          }
        }
      }
    }
    return tileData;
  }

  private static bool CanTeleport(TileData tileData, bool ignoreWonderArea)
  {
    if (tileData == null || tileData.UseReference)
      return false;
    if (tileData.TileType != TileType.Terrain && (ignoreWonderArea || tileData.TileType != TileType.WonderTree))
      return tileData.TileType == TileType.None;
    return true;
  }

  public TileData GetNearEmptyTileForCity(Coordinate location, bool ignoreWonderArea)
  {
    int x1 = (int) this.m_MapData.TilesPerBlock.x;
    int y1 = (int) this.m_MapData.TilesPerBlock.y;
    int x2 = (int) this.m_MapData.TilesPerKingdom.x;
    int y2 = (int) this.m_MapData.TilesPerKingdom.y;
    WorldCoordinate worldCoordinate1 = this.m_MapData.ConvertTileKXYToWorldCoordinate(location);
    TileData tileData = (TileData) null;
    float num1 = float.MaxValue;
    for (int index1 = 0; index1 < x1; ++index1)
    {
      for (int index2 = 0; index2 < y1; ++index2)
      {
        TileData tile = this.m_Tiles[index1, index2];
        if (tile != null)
        {
          Coordinate location1 = tile.Location;
          Coordinate loc1 = new Coordinate(location1.K, location1.X - 1, location1.Y + 1);
          Coordinate loc2 = new Coordinate(location1.K, location1.X + 1, location1.Y + 1);
          Coordinate loc3 = new Coordinate(location1.K, location1.X, location1.Y + 2);
          if (0 < loc1.X && loc2.X < x2 - 1 && (0 < location1.Y && location1.Y < y2 - 1))
          {
            TileData referenceAt1 = this.m_MapData.GetReferenceAt(loc1);
            TileData referenceAt2 = this.m_MapData.GetReferenceAt(loc2);
            TileData referenceAt3 = this.m_MapData.GetReferenceAt(loc3);
            if (referenceAt1 != null && referenceAt2 != null && (referenceAt3 != null && BlockData.CanTeleport(tile, ignoreWonderArea)) && (BlockData.CanTeleport(referenceAt1, ignoreWonderArea) && BlockData.CanTeleport(referenceAt2, ignoreWonderArea) && BlockData.CanTeleport(referenceAt3, ignoreWonderArea)))
            {
              WorldCoordinate worldCoordinate2 = this.m_MapData.ConvertTileKXYToWorldCoordinate(tile.Location);
              float num2 = (float) WorldCoordinate.DistanceSquared(worldCoordinate1, worldCoordinate2);
              if ((double) num2 < (double) num1)
              {
                tileData = tile;
                num1 = num2;
              }
            }
          }
        }
      }
    }
    return tileData;
  }

  public void ResetToTerrain()
  {
    int x = (int) this.m_MapData.TilesPerBlock.x;
    int y = (int) this.m_MapData.TilesPerBlock.y;
    for (int index1 = 0; index1 < x; ++index1)
    {
      for (int index2 = 0; index2 < y; ++index2)
      {
        TileData tile = this.m_Tiles[index1, index2];
        if (tile != null)
          tile.ForceResetToTerrain();
      }
    }
  }

  private void UpdateTileUI(Rect viewport, TileData tile)
  {
    if (tile == null || !viewport.Contains(tile.Position, true))
      return;
    tile.UpdateUI();
  }

  private void ShowOrHide(Rect viewport, TileData tile)
  {
    if (tile == null)
      return;
    if (viewport.Contains(tile.Position, true))
      tile.Show();
    else
      tile.Hide();
  }

  public static int CalculateBlockID(int tileX, int tileY, DynamicMapData mapData)
  {
    int blockXindexByTileX = mapData.GetBlockXIndexByTileX(tileX);
    int blockYindexByTileY = mapData.GetBlockYIndexByTileY(tileY);
    return (int) mapData.BlocksPerKingdom.x * blockYindexByTileY + blockXindexByTileX + 1;
  }

  public static int CalculateBlockID(int tileX, int tileY, int tilesPerBlockX, int tilesPerBlockY, int blocksPerKingdomX)
  {
    int num1 = tileX / tilesPerBlockX;
    int num2 = tileY / tilesPerBlockY;
    return blocksPerKingdomX * num2 + num1 + 1;
  }

  public static Coordinate CalculateBlockOrigin(Coordinate location, DynamicMapData mapData)
  {
    int blockXindexByTileX = mapData.GetBlockXIndexByTileX(location.X);
    int blockYindexByTileY = mapData.GetBlockYIndexByTileY(location.Y);
    int x = (int) mapData.TilesPerBlock.x;
    int y = (int) mapData.TilesPerBlock.y;
    return new Coordinate()
    {
      K = location.K,
      X = blockXindexByTileX * x,
      Y = blockYindexByTileY * y
    };
  }

  private void LoadTerrain()
  {
    Coordinate requestOrigin = this.m_Origin;
    AssetManager.Instance.LoadAsync(this.m_JsonFilePath, (System.Action<UnityEngine.Object, bool>) ((o, ret) => this.OnTerrainLoaded(this.m_JsonFilePath, o, ret, requestOrigin)), (System.Type) null);
  }

  private void OnTerrainLoaded(string path, UnityEngine.Object o, bool ret, Coordinate requestOrigin)
  {
    if (!ret || !requestOrigin.Equals(this.m_Origin))
      return;
    this.ParseBinary(o);
    AssetManager.Instance.UnLoadAsset(path, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    this.m_State |= 1;
  }

  private void ParseBinary(UnityEngine.Object o)
  {
    byte[] bytes = (o as TextAsset).bytes;
    if (bytes == null)
    {
      D.warn((object) string.Format("{0} is null.", (object) this.m_JsonFilePath));
    }
    else
    {
      using (MemoryStream memoryStream = new MemoryStream(bytes))
      {
        using (BinaryReader reader = new BinaryReader((Stream) memoryStream))
        {
          BlockHeader blockHeader = new BlockHeader();
          blockHeader.Read(reader);
          int x = (int) this.m_MapData.TilesPerBlock.x;
          int y = (int) this.m_MapData.TilesPerBlock.y;
          int index1 = 0;
          for (; index1 < x; ++index1)
          {
            for (int index2 = 0; index2 < y; ++index2)
            {
              TileData tile = this.m_Tiles[index1, index2];
              if (tile != null)
              {
                long tileID = (long) reader.ReadInt16();
                tile.UpdateTerrain(tileID);
              }
            }
          }
          for (int index2 = 0; index2 < blockHeader.referenceCount; ++index2)
          {
            BlockReference blockReference = new BlockReference();
            blockReference.Read(reader);
            TileData tileAt = this.GetTileAt((int) blockReference.x, (int) blockReference.y);
            if (tileAt != null)
            {
              tileAt.ReferenceX = (int) blockReference.xRef;
              tileAt.ReferenceY = (int) blockReference.yRef;
            }
            else
              D.error((object) string.Format("[BlockData]ParseBinary: wrong terrain reference data! JSON: {0} => x{1} y{2}", (object) this.m_JsonFilePath, (object) blockReference.x, (object) blockReference.y));
          }
        }
      }
    }
  }

  public static class LoadState
  {
    public const int Unloaded = 0;
    public const int StaticLoaded = 1;
    public const int DynamicLoaded = 2;
    public const int Loaded = 3;
  }
}
