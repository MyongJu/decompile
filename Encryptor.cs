﻿// Decompiled with JetBrains decompiler
// Type: Encryptor
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

public static class Encryptor
{
  public static bool IsOpen;

  public static void Open(string key)
  {
    Encryptor.IsOpen = true;
    AesCoder.Initialize();
  }

  public static void Close()
  {
    AesCoder.Finalize();
    Encryptor.IsOpen = false;
  }

  public static bool NeedEncrypt(LoaderInfo info)
  {
    return info != null && Encryptor.IsOpen && (info.Type == 2 || info.Type == 1 || info.ForceEncrypt);
  }

  public static byte[] LoaderEncode(LoaderInfo info)
  {
    if (info == null)
      return (byte[]) null;
    string str = Utils.Object2Json((object) info.PostData);
    return !Encryptor.NeedEncrypt(info) ? Utils.Json2ByteArray(str) : AesCoder.Encode(str);
  }

  public static string LoaderDecode(LoaderInfo info)
  {
    if (info == null)
      return (string) null;
    byte[] numArray = info.ResData;
    try
    {
      if (Encryptor.NeedEncrypt(info))
        numArray = AesCoder.Decode2Bytes(numArray);
    }
    catch (Exception ex)
    {
      D.error((object) string.Format("[LoaderDecode] Decode2Bytes error: {0}", (object) ex.Message));
    }
    return Utils.ByteArray2String(Utils.DecodeByGzip(numArray));
  }
}
