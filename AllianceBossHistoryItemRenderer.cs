﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossHistoryItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class AllianceBossHistoryItemRenderer : MonoBehaviour
{
  public UILabel historyItem;
  private int time;
  private string playerName;
  private string bossName;

  public void SetData(int time, string playerName, string bossName)
  {
    this.time = NetServerTime.inst.ServerTimestamp - time;
    this.playerName = playerName;
    this.bossName = bossName;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    int num1 = this.time / 3600 / 24;
    int num2 = this.time / 3600 % 24;
    int num3 = this.time % 3600 / 60;
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("d", num1 <= 0 ? string.Empty : num1.ToString() + Utils.XLAT("chat_time_day"));
    para.Add("h", num2 <= 0 ? string.Empty : num2.ToString() + Utils.XLAT("chat_time_hour"));
    para.Add("m", num3 < 0 ? string.Empty : num3.ToString() + Utils.XLAT("chat_time_min"));
    string withPara = ScriptLocalization.GetWithPara("chat_time_display", para, true);
    para.Clear();
    para.Add("0", withPara);
    para.Add("1", this.playerName);
    para.Add("2", Utils.XLAT(this.bossName));
    this.historyItem.text = ScriptLocalization.GetWithPara("alliance_portal_history_description", para, true);
  }
}
