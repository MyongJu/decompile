﻿// Decompiled with JetBrains decompiler
// Type: ConfigExchangeHall
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigExchangeHall
{
  private List<ExchangeHallInfo> _exchangeHallInfoList = new List<ExchangeHallInfo>();
  private Dictionary<string, ExchangeHallInfo> _datas;
  private Dictionary<int, ExchangeHallInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ExchangeHallInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, ExchangeHallInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._exchangeHallInfoList.Add(enumerator.Current);
    }
  }

  public ExchangeHallInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (ExchangeHallInfo) null;
  }

  public ExchangeHallInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (ExchangeHallInfo) null;
  }

  public List<ExchangeHallInfo> GetExchangeHallInfoList()
  {
    return this._exchangeHallInfoList;
  }

  public List<int> GetCatalog1List()
  {
    List<int> intList = new List<int>();
    for (int index = 0; index < this._exchangeHallInfoList.Count; ++index)
    {
      if (!intList.Contains(this._exchangeHallInfoList[index].catalog1stId))
        intList.Add(this._exchangeHallInfoList[index].catalog1stId);
    }
    intList.Sort((Comparison<int>) ((a, b) =>
    {
      ExchangeHallSort1Info exchangeHallSort1Info1 = ConfigManager.inst.DB_ExchangeHallSort1.Get(a);
      ExchangeHallSort1Info exchangeHallSort1Info2 = ConfigManager.inst.DB_ExchangeHallSort1.Get(b);
      if (exchangeHallSort1Info1 != null && exchangeHallSort1Info2 != null)
        return exchangeHallSort1Info1.priority.CompareTo(exchangeHallSort1Info2.priority);
      return -1;
    }));
    return intList;
  }

  public List<int> GetCatalog2List(int catalog1Id)
  {
    List<int> intList = new List<int>();
    for (int index = 0; index < this._exchangeHallInfoList.Count; ++index)
    {
      if (catalog1Id == this._exchangeHallInfoList[index].catalog1stId && !intList.Contains(this._exchangeHallInfoList[index].catalog2ndId))
        intList.Add(this._exchangeHallInfoList[index].catalog2ndId);
    }
    intList.Sort((Comparison<int>) ((a, b) =>
    {
      ExchangeHallSort2Info exchangeHallSort2Info1 = ConfigManager.inst.DB_ExchangeHallSort2.Get(a);
      ExchangeHallSort2Info exchangeHallSort2Info2 = ConfigManager.inst.DB_ExchangeHallSort2.Get(b);
      if (exchangeHallSort2Info1 != null && exchangeHallSort2Info2 != null)
        return exchangeHallSort2Info1.priority.CompareTo(exchangeHallSort2Info2.priority);
      return -1;
    }));
    return intList;
  }

  public List<int> GetCatalog3List(int catalog1Id, int catalog2Id)
  {
    List<int> intList = new List<int>();
    for (int index = 0; index < this._exchangeHallInfoList.Count; ++index)
    {
      if (catalog1Id == this._exchangeHallInfoList[index].catalog1stId && catalog2Id == this._exchangeHallInfoList[index].catalog2ndId && !intList.Contains(this._exchangeHallInfoList[index].catalog3rdId))
        intList.Add(this._exchangeHallInfoList[index].catalog3rdId);
    }
    intList.Sort((Comparison<int>) ((a, b) =>
    {
      ExchangeHallSort3Info exchangeHallSort3Info1 = ConfigManager.inst.DB_ExchangeHallSort3.Get(a);
      ExchangeHallSort3Info exchangeHallSort3Info2 = ConfigManager.inst.DB_ExchangeHallSort3.Get(b);
      if (exchangeHallSort3Info1 != null && exchangeHallSort3Info2 != null)
        return exchangeHallSort3Info1.priority.CompareTo(exchangeHallSort3Info2.priority);
      return -1;
    }));
    return intList;
  }
}
