﻿// Decompiled with JetBrains decompiler
// Type: LabelBinder
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LabelBinder : MonoBehaviour
{
  public UILabel[] mLabels;

  public void SetLabels(params object[] list)
  {
    for (int index = 0; index < list.Length; ++index)
    {
      if (index < this.mLabels.Length)
        this.mLabels[index].text = list[index].ToString();
    }
  }
}
