﻿// Decompiled with JetBrains decompiler
// Type: HealUnitRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class HealUnitRender : MonoBehaviour
{
  public System.Action<HealUnitRender> onValueChanged;
  public Icon unitIcon;
  public StandardProgressBar progressBar;
  public UILabel totalCountLabel;
  public UIButton disMissBtn;
  private HealTroopInfo info;
  private Unit_StatisticsInfo unit;

  public void Init(HealTroopInfo info)
  {
    this.info = info;
    this.unit = ConfigManager.inst.DB_Unit_Statistics.GetData(info.ID);
    this.progressBar.Init(info.healing, info.total);
    this.progressBar.onValueChanged += new System.Action<int>(this.OnProgressBarValueChange);
    this.unitIcon.FeedData((IComponentData) new IconData(this.unit.Troop_ICON, new string[2]
    {
      Utils.XLAT(this.unit.Troop_Name_LOC_ID),
      Utils.GetRomaNumeralsBelowTen(this.unit.Troop_Tier)
    }));
    this.totalCountLabel.text = "/" + info.total.ToString();
  }

  public bool IsEnable
  {
    set
    {
      this.progressBar.Enabled(value);
      this.disMissBtn.isEnabled = value;
    }
  }

  public void Dispose()
  {
    this.unitIcon.Dispose();
  }

  public void OnDismissBtnClick()
  {
    (UIManager.inst.OpenPopup("DissmissHealTroopPopup", (Popup.PopupParameter) null) as DissmissHealTroopPopup).Init(this.info, DissmissHealTroopPopup.HospitalType.CityHospital);
  }

  public int CurrenCount
  {
    get
    {
      return this.progressBar.CurrentCount;
    }
  }

  private void OnProgressBarValueChange(int count)
  {
    this.info.healing = count;
    if (this.onValueChanged == null)
      return;
    this.onValueChanged(this);
  }
}
