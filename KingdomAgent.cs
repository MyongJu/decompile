﻿// Decompiled with JetBrains decompiler
// Type: KingdomAgent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class KingdomAgent
{
  private LinkedList m_FreeNodeList = new LinkedList();
  private LinkedList m_UsedNodeList = new LinkedList();
  private LinkedList m_RequestList = new LinkedList();
  private ObjectPool<KingdomAgent.KingdomRequest> m_RequestPool = new ObjectPool<KingdomAgent.KingdomRequest>();
  private int m_UsedNodeCount;
  private int m_HitCount;
  private Rect m_Viewport;
  private bool m_ViewportChanged;
  private DynamicMapData m_MapData;

  public KingdomAgent(DynamicMapData mapData)
  {
    this.m_MapData = mapData;
    for (int index = 0; index < 9; ++index)
      this.m_FreeNodeList.PushBack((LinkedList.Node) new KingdomAgent.KingdomNode());
  }

  public void UpdateCenter(WorldCoordinate center)
  {
    WorldCoordinate kingdomCoordinate = this.m_MapData.CalculateKingdomCoordinate(center);
    int x = kingdomCoordinate.X;
    int y = kingdomCoordinate.Y;
    WorldCoordinate kingdomCoords1 = new WorldCoordinate(x, y);
    WorldCoordinate kingdomCoords2 = new WorldCoordinate(x - 1, y);
    WorldCoordinate kingdomCoords3 = new WorldCoordinate(x + 1, y);
    WorldCoordinate kingdomCoords4 = new WorldCoordinate(x, y - 1);
    WorldCoordinate kingdomCoords5 = new WorldCoordinate(x, y + 1);
    WorldCoordinate kingdomCoords6 = new WorldCoordinate(x - 1, y - 1);
    WorldCoordinate kingdomCoords7 = new WorldCoordinate(x - 1, y + 1);
    WorldCoordinate kingdomCoords8 = new WorldCoordinate(x + 1, y - 1);
    WorldCoordinate kingdomCoords9 = new WorldCoordinate(x + 1, y + 1);
    this.Begin();
    this.SortOrRequest(kingdomCoords1);
    this.SortOrRequest(kingdomCoords2);
    this.SortOrRequest(kingdomCoords3);
    this.SortOrRequest(kingdomCoords4);
    this.SortOrRequest(kingdomCoords5);
    this.SortOrRequest(kingdomCoords6);
    this.SortOrRequest(kingdomCoords7);
    this.SortOrRequest(kingdomCoords8);
    this.SortOrRequest(kingdomCoords9);
    this.End();
  }

  public void DrawBlockBorders(Color color)
  {
    LinkedList.Node node = this.m_UsedNodeList.GetFirst();
    for (LinkedList.Node end = this.m_UsedNodeList.GetEnd(); node != end; node = node.m_Next)
      (node as KingdomAgent.KingdomNode).m_Kingdom.DrawBlockBorders(color);
  }

  private void Begin()
  {
    this.m_HitCount = 0;
  }

  private void End()
  {
    for (; this.m_UsedNodeCount > this.m_HitCount; --this.m_UsedNodeCount)
    {
      KingdomAgent.KingdomNode kingdomNode = this.m_UsedNodeList.PopBack() as KingdomAgent.KingdomNode;
      kingdomNode.m_Kingdom.Dispose();
      this.m_FreeNodeList.PushBack((LinkedList.Node) kingdomNode);
    }
    while (!this.m_RequestList.IsEmpty())
    {
      KingdomAgent.KingdomRequest o = this.m_RequestList.PopFront() as KingdomAgent.KingdomRequest;
      KingdomAgent.KingdomNode kingdomNode = this.m_FreeNodeList.PopFront() as KingdomAgent.KingdomNode;
      kingdomNode.m_Kingdom.Reset(o.KingdomX, o.KingdomY, this.m_MapData);
      this.m_UsedNodeList.PushFront((LinkedList.Node) kingdomNode);
      ++this.m_UsedNodeCount;
      this.m_RequestPool.Release(o);
    }
  }

  private void SortOrRequest(WorldCoordinate kingdomCoords)
  {
    KingdomAgent.KingdomNode kingdomNodeAt = this.GetKingdomNodeAt(kingdomCoords);
    if (kingdomNodeAt != null)
    {
      this.m_UsedNodeList.Remove((LinkedList.Node) kingdomNodeAt);
      this.m_UsedNodeList.PushFront((LinkedList.Node) kingdomNodeAt);
      ++this.m_HitCount;
    }
    else
    {
      KingdomAgent.KingdomRequest kingdomRequest = this.m_RequestPool.Allocate();
      kingdomRequest.KingdomX = kingdomCoords.X;
      kingdomRequest.KingdomY = kingdomCoords.Y;
      this.m_RequestList.PushBack((LinkedList.Node) kingdomRequest);
    }
  }

  private KingdomAgent.KingdomNode GetKingdomNodeAt(WorldCoordinate kingdomCoords)
  {
    int x = kingdomCoords.X;
    int y = kingdomCoords.Y;
    LinkedList.Node node = this.m_UsedNodeList.GetFirst();
    for (LinkedList.Node end = this.m_UsedNodeList.GetEnd(); node != end; node = node.m_Next)
    {
      KingdomAgent.KingdomNode kingdomNode = node as KingdomAgent.KingdomNode;
      if (kingdomNode.m_Kingdom.KingdomX == x && kingdomNode.m_Kingdom.KingdomY == y)
        return kingdomNode;
    }
    return (KingdomAgent.KingdomNode) null;
  }

  public void UpdateViewport(Rect viewport)
  {
    this.m_Viewport = viewport;
    this.m_ViewportChanged = true;
  }

  public void CullAndShow(Transform transform)
  {
    if (!this.m_ViewportChanged)
      return;
    LinkedList.Node node = this.m_UsedNodeList.GetFirst();
    for (LinkedList.Node end = this.m_UsedNodeList.GetEnd(); node != end; node = node.m_Next)
      (node as KingdomAgent.KingdomNode).m_Kingdom.CullAndShow(this.m_Viewport, transform);
    this.m_ViewportChanged = false;
  }

  public void Dispose()
  {
    while (!this.m_UsedNodeList.IsEmpty())
    {
      KingdomAgent.KingdomNode kingdomNode = this.m_UsedNodeList.PopFront() as KingdomAgent.KingdomNode;
      kingdomNode.m_Kingdom.Dispose();
      this.m_FreeNodeList.PushBack((LinkedList.Node) kingdomNode);
    }
    while (!this.m_RequestList.IsEmpty())
      this.m_RequestPool.Release(this.m_RequestList.PopFront() as KingdomAgent.KingdomRequest);
    this.m_UsedNodeCount = 0;
    this.m_HitCount = 0;
  }

  private class KingdomNode : LinkedList.Node
  {
    public KingdomData m_Kingdom = new KingdomData();
  }

  private class KingdomRequest : LinkedList.Node
  {
    public int KingdomX;
    public int KingdomY;
  }
}
