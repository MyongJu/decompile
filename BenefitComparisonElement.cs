﻿// Decompiled with JetBrains decompiler
// Type: BenefitComparisonElement
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class BenefitComparisonElement
{
  private string id;
  private float v1;
  private float v2;

  public BenefitComparisonElement(string ID, float value1, float value2)
  {
    this.ID = ID;
    this.value1 = value1;
    this.value2 = value2;
  }

  public string ID
  {
    set
    {
      this.id = value;
    }
    get
    {
      return this.id;
    }
  }

  public float value1
  {
    set
    {
      this.v1 = value;
    }
    get
    {
      return this.v1;
    }
  }

  public float value2
  {
    set
    {
      this.v2 = value;
    }
    get
    {
      return this.v2;
    }
  }
}
