﻿// Decompiled with JetBrains decompiler
// Type: ConfigToast
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigToast
{
  private Dictionary<string, ToastData> _toasts = new Dictionary<string, ToastData>();

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exsit", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "image");
        int index3 = ConfigManager.GetIndex(inHeader, "text");
        int index4 = ConfigManager.GetIndex(inHeader, "title");
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          ToastData data = new ToastData();
          if (int.TryParse(key.ToString(), out data.internalId))
          {
            ArrayList arr = hashtable[key] as ArrayList;
            if (arr != null)
            {
              data.id = ConfigManager.GetString(arr, index1);
              data.icon = ConfigManager.GetString(arr, index2);
              data.title = ConfigManager.GetString(arr, index4);
              data.content = ConfigManager.GetString(arr, index3);
              this.PushData(data.id, data);
            }
          }
        }
      }
    }
  }

  public void PushData(string id, ToastData data)
  {
    if (this._toasts.ContainsKey(id))
      return;
    this._toasts.Add(id, data);
    ConfigManager.inst.AddData(data.internalId, (object) data);
  }

  public void Clear()
  {
    if (this._toasts == null)
      return;
    this._toasts.Clear();
  }

  public ToastData GetData(string type)
  {
    ToastData toastData = (ToastData) null;
    this._toasts.TryGetValue(type, out toastData);
    return toastData;
  }
}
