﻿// Decompiled with JetBrains decompiler
// Type: AllianceChangeSloganHeader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Text;
using UI;
using UnityEngine;

public class AllianceChangeSloganHeader : AllianceCustomizeHeader
{
  [SerializeField]
  private UIInput m_MessageInput;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    this.m_MessageInput.defaultText = ScriptLocalization.Get("alliance_no_motto_description", true);
    this.m_MessageInput.value = this.allianceData.publicMessage;
    this.m_MessageInput.onValidate = new UIInput.OnValidate(this.InputValidator);
  }

  public void OnSave()
  {
    if (IllegalWordsUtils.WarningIllegalWords(this.m_MessageInput.value))
      return;
    AllianceManager.Instance.SetupAllianceInfo(this.allianceData.language, !string.IsNullOrEmpty(this.m_MessageInput.value) ? this.m_MessageInput.value : string.Empty, this.allianceData.isPirvate, new System.Action<bool, object>(this.SetupAllianceCallback));
  }

  private void SetupAllianceCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.m_MessageInput.value = this.allianceData.publicMessage;
    UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_motto_changed"), (System.Action) null, 4f, false);
  }

  private char InputValidator(string text, int charIndex, char addedChar)
  {
    if (Encoding.UTF8.GetBytes(text).Length > 300 || (int) addedChar == 10)
      return char.MinValue;
    return addedChar;
  }
}
