﻿// Decompiled with JetBrains decompiler
// Type: RoomManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class RoomManager
{
  protected Dictionary<RoomEventType, IRoomEventProcessor> _allRoomEventProcessor = new Dictionary<RoomEventType, IRoomEventProcessor>();
  protected Dictionary<int, Room[,]> _allRoom = new Dictionary<int, Room[,]>();
  private List<Room> _searchPath = new List<Room>();
  private int _currentX = 2;
  public const int ROOM_COUNT_PER_COLUMN = 4;
  public const int ROOM_COUNT_PER_ROW = 4;
  public const int DIR_FORWARD = 1;
  public const int DIR_RIGHT = 2;
  public const int DIR_BACKWARD = 4;
  public const int DIR_LEFT = 8;
  public System.Action<bool, bool, bool, bool> OnMoveToOtherRoomEnabled;
  public System.Action<bool> OnMoveToNextFloorEnabled;
  public System.Action<bool> OnMinimapEnabled;
  public System.Action<bool> OnFloorEnabled;
  public System.Action<bool> OnTimeEnabled;
  public System.Action<Room, Room> OnBacktoPreviousRoom;
  public System.Action<Room> OnRoomSearched;
  public System.Action<Room> OnRoomEventComplete;
  public Room CurrentRoom;
  private Room _previousRoom;
  private IRoomEventProcessor _currentRoomEventProcessor;
  private DragonKnightDungeonData _currentDragonKnightDungeonData;
  private int _currentFloor;
  private int _currentY;

  public DragonKnightDungeonData CurrentDragonKnightDungeonData
  {
    get
    {
      return this._currentDragonKnightDungeonData;
    }
  }

  public RoomEventType GetCurrentProcessingEventType()
  {
    using (Dictionary<RoomEventType, IRoomEventProcessor>.Enumerator enumerator = this._allRoomEventProcessor.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<RoomEventType, IRoomEventProcessor> current = enumerator.Current;
        if (current.Value == this._currentRoomEventProcessor)
          return current.Key;
      }
    }
    return RoomEventType.EmptyEvent;
  }

  public void Init()
  {
    this.SetRoomEventProcessor(RoomEventType.BattleEvent, (IRoomEventProcessor) new RoomProcessorBattle());
    this.SetRoomEventProcessor(RoomEventType.PVPEvent, (IRoomEventProcessor) new RoomProcessorPVP());
    this.SetRoomEventProcessor(RoomEventType.ArenaEvent, (IRoomEventProcessor) new RoomProcessorArena());
  }

  protected int ToServerRoomIndex(int x, int y)
  {
    return (3 - y) * 4 + x;
  }

  protected void FromServerRoomIndex(int index, ref int x, ref int y)
  {
    y = 3 - index / 4;
    x = index - (3 - y) * 4;
  }

  public void SetMazeData(DragonKnightDungeonData dungeonData)
  {
    this._currentDragonKnightDungeonData = dungeonData;
    this._previousRoom = (Room) null;
    this.CurrentRoom = (Room) null;
    this._currentRoomEventProcessor = (IRoomEventProcessor) null;
    this._searchPath.Clear();
    this._allRoom.Clear();
    int currentLevel = dungeonData.CurrentLevel;
    Room[,] roomArray = new Room[4, 4];
    this._allRoom.Add(currentLevel, roomArray);
    for (int index1 = 0; index1 < 4; ++index1)
    {
      for (int index2 = 0; index2 < 4; ++index2)
        roomArray[index1, index2] = new Room()
        {
          RoomX = index1,
          RoomY = index2,
          Floor = currentLevel,
          FloorInternalId = dungeonData.LevelId
        };
    }
    this.SyncMazeData(dungeonData);
    int x = 0;
    int y = 0;
    this.FromServerRoomIndex(dungeonData.PreviousGrid, ref x, ref y);
    this._previousRoom = this.GetRoomByPosition(this._currentFloor, x, y);
  }

  public void SyncMazeData(DragonKnightDungeonData dungeonData)
  {
    int currentLevel = dungeonData.CurrentLevel;
    int currentGrid = dungeonData.CurrentGrid;
    Hashtable maze = dungeonData.Maze as Hashtable;
    if (maze == null)
      return;
    int outData1 = 0;
    int outData2 = 0;
    DatabaseTools.UpdateData(maze, "start", ref outData1);
    DatabaseTools.UpdateData(maze, "end", ref outData2);
    ArrayList arrayList1 = maze[(object) "grid"] as ArrayList;
    ArrayList arrayList2 = maze[(object) "crossed"] as ArrayList;
    for (int index1 = 0; index1 < 4; ++index1)
    {
      for (int index2 = 0; index2 < 4; ++index2)
      {
        int serverRoomIndex = this.ToServerRoomIndex(index1, index2);
        Hashtable inData = arrayList1[serverRoomIndex] as Hashtable;
        if (inData != null)
        {
          int outData3 = 0;
          int outData4 = 0;
          string empty = string.Empty;
          DatabaseTools.UpdateData(inData, "access", ref outData3);
          DatabaseTools.UpdateData(inData, "type", ref empty);
          DatabaseTools.UpdateData(inData, "id", ref outData4);
          Room roomByPosition = this.GetRoomByPosition(currentLevel, index1, index2);
          roomByPosition.ClearData();
          roomByPosition.LeftRoom = (outData3 & 8) == 0 ? (Room) null : this.GetRoomByPosition(currentLevel, index1 - 1, index2);
          roomByPosition.RightRoom = (outData3 & 2) == 0 ? (Room) null : this.GetRoomByPosition(currentLevel, index1 + 1, index2);
          roomByPosition.ForwardRoom = (outData3 & 1) == 0 ? (Room) null : this.GetRoomByPosition(currentLevel, index1, index2 + 1);
          roomByPosition.BackwardRoom = (outData3 & 4) == 0 ? (Room) null : this.GetRoomByPosition(currentLevel, index1, index2 - 1);
          roomByPosition.IsExit = serverRoomIndex == outData2;
          roomByPosition.IsSearched = arrayList2 != null && arrayList2.Contains((object) serverRoomIndex.ToString());
          roomByPosition.RoomType = empty;
          string key = empty;
          if (key != null)
          {
            // ISSUE: reference to a compiler-generated field
            if (RoomManager.\u003C\u003Ef__switch\u0024map55 == null)
            {
              // ISSUE: reference to a compiler-generated field
              RoomManager.\u003C\u003Ef__switch\u0024map55 = new Dictionary<string, int>(7)
              {
                {
                  "raid_chest",
                  0
                },
                {
                  "boss",
                  1
                },
                {
                  "monster",
                  1
                },
                {
                  "buff",
                  2
                },
                {
                  "chest",
                  3
                },
                {
                  "pvp",
                  4
                },
                {
                  "arena",
                  4
                }
              };
            }
            int num;
            // ISSUE: reference to a compiler-generated field
            if (RoomManager.\u003C\u003Ef__switch\u0024map55.TryGetValue(key, out num))
            {
              switch (num)
              {
                case 0:
                  Hashtable data = inData[(object) "reward"] as Hashtable;
                  if (data != null)
                  {
                    RoomRaidChest roomRaidChest = new RoomRaidChest();
                    roomRaidChest.Decode(data);
                    roomByPosition.AllRaidChest.Add(roomRaidChest);
                    continue;
                  }
                  continue;
                case 1:
                  roomByPosition.AddMonster(new RoomMonsterGroup()
                  {
                    Type = empty,
                    GroupId = outData4
                  });
                  continue;
                case 2:
                  roomByPosition.AllBuff.Add(outData4);
                  continue;
                case 3:
                  RoomChest chest = new RoomChest();
                  chest.chestId = outData4;
                  Hashtable hashtable1 = inData[(object) "reward"] as Hashtable;
                  if (hashtable1 != null)
                  {
                    int result1 = 0;
                    int result2 = 0;
                    IDictionaryEnumerator enumerator = hashtable1.GetEnumerator();
                    if (enumerator.MoveNext())
                    {
                      int.TryParse(enumerator.Key.ToString(), out result1);
                      int.TryParse(enumerator.Value.ToString(), out result2);
                      chest.itemId = result1;
                      chest.itemCount = result2;
                    }
                  }
                  roomByPosition.AddChest(chest);
                  continue;
                case 4:
                  long result3 = 0;
                  List<int> allSkill = new List<int>();
                  if (inData.ContainsKey((object) "id") && long.TryParse(inData[(object) "id"].ToString(), out result3))
                  {
                    if (inData.ContainsKey((object) "skill"))
                    {
                      ArrayList arrayList3 = inData[(object) "skill"] as ArrayList;
                      if (arrayList3 != null)
                      {
                        for (int index3 = 0; index3 < arrayList3.Count; ++index3)
                        {
                          int result1 = 0;
                          if (int.TryParse(arrayList3[index3].ToString(), out result1))
                            allSkill.Add(result1);
                        }
                      }
                    }
                    roomByPosition.AddPlayer(result3, allSkill);
                  }
                  if (inData.ContainsKey((object) "talent"))
                  {
                    Hashtable hashtable2 = inData[(object) "talent"] as Hashtable;
                    if (hashtable2 != null)
                    {
                      IEnumerator enumerator = hashtable2.Keys.GetEnumerator();
                      try
                      {
                        while (enumerator.MoveNext())
                        {
                          string current = (string) enumerator.Current;
                          int result1 = 0;
                          if (int.TryParse(hashtable2[(object) current].ToString(), out result1))
                            roomByPosition.SetPlayerTalent(current, result1);
                        }
                        continue;
                      }
                      finally
                      {
                        IDisposable disposable = enumerator as IDisposable;
                        if (disposable != null)
                          disposable.Dispose();
                      }
                    }
                    else
                      continue;
                  }
                  else
                    continue;
                default:
                  continue;
              }
            }
          }
        }
      }
    }
    this.FromServerRoomIndex(currentGrid, ref this._currentX, ref this._currentY);
    this._currentFloor = currentLevel;
  }

  public int CurrentFloor
  {
    get
    {
      return this._currentFloor;
    }
  }

  public void StartSearch()
  {
    this.SearchRoom(this._currentFloor, this._currentX, this._currentY, false);
  }

  public void SetRoomEventProcessor(RoomEventType roomEventType, IRoomEventProcessor processor)
  {
    if (this._allRoomEventProcessor.ContainsKey(roomEventType))
      this._allRoomEventProcessor.Remove(roomEventType);
    if (processor == null)
      return;
    this._allRoomEventProcessor.Add(roomEventType, processor);
  }

  protected IRoomEventProcessor GetRoomEventProcessor(RoomEventType roomEventType)
  {
    if (this._allRoomEventProcessor.ContainsKey(roomEventType))
      return this._allRoomEventProcessor[roomEventType];
    return (IRoomEventProcessor) null;
  }

  public Room GetRoomByPosition(int floor, int roomX, int roomY)
  {
    if (this._allRoom.ContainsKey(floor) && this.IsRoomPositionValid(roomX, roomY))
      return this._allRoom[floor][roomX, roomY];
    return (Room) null;
  }

  public int GetSearchedRoomCount(int floor)
  {
    int num = 0;
    Room[,] roomArray;
    this._allRoom.TryGetValue(floor, out roomArray);
    if (roomArray != null)
    {
      for (int index1 = 0; index1 < roomArray.GetLength(0); ++index1)
      {
        for (int index2 = 0; index2 < roomArray.GetLength(1); ++index2)
        {
          if (roomArray[index1, index2].IsSearched)
            ++num;
        }
      }
    }
    return num;
  }

  public void SearchRoom(int floor, int roomX, int roomY, bool clearPath = false)
  {
    Room roomByPosition = this.GetRoomByPosition(floor, roomX, roomY);
    if (roomByPosition != null)
      this.SearchRoom(roomByPosition, clearPath);
    else
      D.error((object) "invalid room position");
  }

  public void SearchRoom(Room room, bool clearPath = false)
  {
    if (clearPath)
      this._searchPath.Clear();
    if (this.CurrentRoom != room)
    {
      if (this.CurrentRoom == null || room == this.CurrentRoom.LeftRoom || (room == this.CurrentRoom.RightRoom || room == this.CurrentRoom.ForwardRoom) || room == this.CurrentRoom.BackwardRoom)
        ;
      if (this.CurrentRoom != null)
        this._previousRoom = this.CurrentRoom;
      this.CurrentRoom = room;
      this._searchPath.Add(room);
    }
    this.CurrentRoom.IsSearched = true;
    IRoomEventProcessor roomEventProcessor = (IRoomEventProcessor) null;
    bool minimapEnabled = true;
    bool floorEnabled = true;
    bool timeEnabled = true;
    if (room.RoomType == "arena")
    {
      minimapEnabled = false;
      floorEnabled = false;
      timeEnabled = false;
      roomEventProcessor = this.GetRoomEventProcessor(RoomEventType.ArenaEvent);
    }
    if (room.RoomType == "pvp")
    {
      minimapEnabled = false;
      roomEventProcessor = this.GetRoomEventProcessor(RoomEventType.PVPEvent);
    }
    if (room.RoomType == "raid_chest")
      roomEventProcessor = this.GetRoomEventProcessor(RoomEventType.RaidChestEvent);
    if (room.ContainsMonsters())
    {
      minimapEnabled = false;
      roomEventProcessor = this.GetRoomEventProcessor(RoomEventType.BattleEvent);
    }
    if (room.ContainsChest())
      roomEventProcessor = this.GetRoomEventProcessor(RoomEventType.OpenChestEvent);
    if (room.ContainsBuff())
      roomEventProcessor = this.GetRoomEventProcessor(RoomEventType.BuffEvent);
    System.Action FinalDo = (System.Action) (() =>
    {
      if (roomEventProcessor != null)
        roomEventProcessor.ProcessRoomEvent(room);
      this.SetMoveToOtherRoomEnabled(roomEventProcessor == null || roomEventProcessor.IsSkipAble());
      if (room.IsExit)
        this.SetMoveToNextFloorEnabled(roomEventProcessor == null || roomEventProcessor.IsSkipAble());
      else
        this.SetMoveToNextFloorEnabled(false);
      if (this.OnRoomSearched != null)
        this.OnRoomSearched(room);
      this.SetMinimapEnabled(minimapEnabled);
      this.SetFloorEnabled(floorEnabled);
      this.SetTimeEnabled(timeEnabled);
      if (roomEventProcessor == null)
      {
        roomEventProcessor = this.GetRoomEventProcessor(RoomEventType.EmptyEvent);
        if (roomEventProcessor != null)
          roomEventProcessor.ProcessRoomEvent(room);
      }
      this._currentRoomEventProcessor = roomEventProcessor;
    });
    if (roomEventProcessor != null || room.IsExit || room.RoomType == "pvp")
      this.SyncSearchPathToServer((System.Action) (() =>
      {
        this.SyncMazeData(this.CurrentDragonKnightDungeonData);
        FinalDo();
      }), (Room) null);
    else
      FinalDo();
  }

  public void CurrentRoomEventCompelted(bool backToPreviousRoom = false)
  {
    if (this.OnRoomEventComplete != null)
      this.OnRoomEventComplete(this.CurrentRoom);
    this.SyncMazeData(this.CurrentDragonKnightDungeonData);
    if (backToPreviousRoom && this.OnBacktoPreviousRoom != null && this._previousRoom != null)
    {
      this.SetMinimapEnabled(true);
      this.OnBacktoPreviousRoom(this.CurrentRoom, this._previousRoom);
    }
    else
      this.SearchRoom(this.CurrentRoom, false);
  }

  public bool IsRoomPositionValid(int roomPositionX, int roomPositionY)
  {
    if (roomPositionX >= 0 && roomPositionX < 4 && roomPositionY >= 0)
      return roomPositionY < 4;
    return false;
  }

  public Room GetNextRoom(int offsetFloor, int roomOffsetX, int roomOffsetY)
  {
    int num1 = roomOffsetX + this.CurrentRoom.RoomX;
    int num2 = roomOffsetY + this.CurrentRoom.RoomY;
    if (this.IsRoomPositionValid(num1, num2))
      return this.GetRoomByPosition(this.CurrentRoom.Floor + offsetFloor, num1, num2);
    return (Room) null;
  }

  public void MoveToNextRoom(int roomOffsetX, int roomOffsetY)
  {
    int num1 = roomOffsetX + this.CurrentRoom.RoomX;
    int num2 = roomOffsetY + this.CurrentRoom.RoomY;
    if (this.IsRoomPositionValid(num1, num2))
      this.SearchRoom(this.CurrentRoom.Floor, num1, num2, false);
    else
      D.error((object) "target position is invalid");
  }

  public void SyncSearchPathToServer(System.Action callback, Room roomFlyTo = null)
  {
    if (this._searchPath.Count <= 1 && roomFlyTo == null)
    {
      if (callback == null)
        return;
      callback();
    }
    else
    {
      List<int> intList = new List<int>();
      if (this._searchPath.Count > 1)
      {
        Room room1 = this._searchPath[0];
        for (int index = 1; index < this._searchPath.Count; ++index)
        {
          Room room2 = this._searchPath[index];
          if (room1.LeftRoom == room2)
            intList.Add(8);
          else if (room1.RightRoom == room2)
            intList.Add(2);
          else if (room1.ForwardRoom == room2)
            intList.Add(1);
          else if (room1.BackwardRoom == room2)
            intList.Add(4);
          else
            D.error((object) "path not continous");
          room1 = room2;
        }
      }
      Hashtable postData = new Hashtable()
      {
        {
          (object) "cross",
          (object) intList
        }
      };
      using (List<int>.Enumerator enumerator = intList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          int current = enumerator.Current;
        }
      }
      if (roomFlyTo != null)
        postData.Add((object) "go_to", (object) this.ToServerRoomIndex(roomFlyTo.RoomX, roomFlyTo.RoomY));
      RequestManager.inst.SendRequest("DragonKnight:dungeonGo", postData, (System.Action<bool, object>) ((result, data) =>
      {
        if (!result)
          return;
        if (this._searchPath.Count > 1)
          this._searchPath.RemoveRange(0, this._searchPath.Count - 1);
        if (callback == null)
          return;
        callback();
      }), true);
    }
  }

  public void RequestMoveToNextFloor()
  {
    RequestManager.inst.SendRequest("DragonKnight:toDungeonNextLevel", (Hashtable) null, new System.Action<bool, object>(this.OnRequestMoveToNextFloorCallback), true);
  }

  protected void OnRequestMoveToNextFloorCallback(bool result, object data)
  {
    if (!result)
      return;
    Room currentRoom = this.CurrentRoom;
    this.SetMazeData(this.CurrentDragonKnightDungeonData);
    Room roomByPosition = this.GetRoomByPosition(this._currentFloor, this._currentX, this._currentY);
    DragonKnightSystem.Instance.Controller.RoomHud.PlayRoomAnimation(currentRoom, roomByPosition, (System.Action) (() => this.StartSearch()));
  }

  protected void SetMoveToOtherRoomEnabled(bool enabled)
  {
    bool flag1 = false;
    bool flag2 = false;
    bool flag3 = false;
    bool flag4 = false;
    if (enabled)
    {
      flag1 = this.CurrentRoom.LeftRoom != null;
      flag2 = this.CurrentRoom.RightRoom != null;
      flag3 = this.CurrentRoom.ForwardRoom != null;
      flag4 = this.CurrentRoom.BackwardRoom != null;
    }
    if (this.OnMoveToOtherRoomEnabled == null)
      return;
    this.OnMoveToOtherRoomEnabled(flag1, flag2, flag3, flag4);
  }

  protected void SetMoveToNextFloorEnabled(bool enabled)
  {
    if (this.OnMoveToNextFloorEnabled == null)
      return;
    this.OnMoveToNextFloorEnabled(enabled);
  }

  protected void SetMinimapEnabled(bool enabled)
  {
    if (this.OnMinimapEnabled == null)
      return;
    this.OnMinimapEnabled(enabled);
  }

  protected void SetFloorEnabled(bool enabled)
  {
    if (this.OnFloorEnabled == null)
      return;
    this.OnFloorEnabled(enabled);
  }

  protected void SetTimeEnabled(bool enabled)
  {
    if (this.OnTimeEnabled == null)
      return;
    this.OnTimeEnabled(enabled);
  }
}
