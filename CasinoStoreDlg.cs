﻿// Decompiled with JetBrains decompiler
// Type: CasinoStoreDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class CasinoStoreDlg : UI.Dialog
{
  private GameObjectPool pools = new GameObjectPool();
  private ItemBag.ItemType[] SHOPITEM_TYPE = new ItemBag.ItemType[2]
  {
    ItemBag.ItemType.casino1,
    ItemBag.ItemType.casino2
  };
  private List<CasinoPropsItemRenderer> lists = new List<CasinoPropsItemRenderer>();
  private List<ShopStaticInfo> datas = new List<ShopStaticInfo>();
  private List<int> items = new List<int>();
  public UIScrollView scroll;
  public GameObject itemRoot;
  public UIGrid grid;
  public UITexture propsTexture;
  public UILabel propsCount;
  public UILabel gold;
  public CasinoPropsItemRenderer itemPrefab;
  private bool init;
  private int displayType;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Initialize();
    CasinoStoreDlg.Parameter parameter = orgParam as CasinoStoreDlg.Parameter;
    if (parameter != null)
      this.displayType = parameter.displayType;
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Clear();
    this.pools.Clear();
    this.RemoveEventHandler();
  }

  public void OnBackBtnPressed()
  {
    if (this.displayType == 0)
      this.OnBackButtonPress();
    else
      CasinoFunHelper.Instance.ShowFirstCasinoScene();
  }

  private void Initialize()
  {
    if (this.init)
      return;
    this.init = true;
    this.pools.Initialize(this.itemPrefab.gameObject, this.itemRoot);
    this.AddEventHandler();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Item.onDataCreated += new System.Action<int>(this.OnDataChangeHandler);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnDataChangeHandler);
  }

  private void Clear()
  {
    for (int index = 0; index < this.lists.Count; ++index)
    {
      this.lists[index].Dispose();
      this.pools.Release(this.lists[index].gameObject);
    }
    this.lists.Clear();
  }

  private void RemoveEventHandler()
  {
    this.init = false;
    DBManager.inst.DB_Item.onDataCreated -= new System.Action<int>(this.OnDataChangeHandler);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnDataChangeHandler);
  }

  private void OnDataChangeHandler(int id)
  {
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.gold.text = PlayerData.inst.hostPlayer.Currency.ToString();
    this.Clear();
    this.CreateData();
    this.scroll.gameObject.SetActive(false);
    for (int index = 0; index < this.items.Count; ++index)
      this.GetItem().SetItemData(this.items[index]);
    for (int index = 0; index < this.datas.Count; ++index)
      this.GetItem().SetShopItemData(this.datas[index].internalId);
    this.grid.repositionNow = true;
    this.grid.Reposition();
    Utils.ExecuteInSecs(1f / 1000f, (System.Action) (() =>
    {
      this.scroll.gameObject.SetActive(true);
      this.scroll.ResetPosition();
    }));
    string str1 = "Texture/ItemIcons/";
    string empty = string.Empty;
    string path;
    string str2;
    if (this.displayType == 0)
    {
      path = str1 + "item_casino_knucklebone_tiny";
      str2 = Utils.XLAT("item_casino_knucklebone_tiny_name") + ": " + PlayerData.inst.userData.copper_coin.ToString();
    }
    else
    {
      path = str1 + "item_casino_rune_stone_tiny";
      str2 = Utils.XLAT("item_casino_rune_stone_tiny_name") + ": " + PlayerData.inst.userData.dragon_coin.ToString();
    }
    BuilderFactory.Instance.HandyBuild((UIWidget) this.propsTexture, path, (System.Action<bool>) null, true, false, string.Empty);
    this.propsCount.text = str2;
  }

  private CasinoPropsItemRenderer GetItem()
  {
    GameObject go = this.pools.AddChild(this.grid.gameObject);
    CasinoPropsItemRenderer component = go.GetComponent<CasinoPropsItemRenderer>();
    NGUITools.SetActive(go, true);
    this.lists.Add(component);
    return component;
  }

  private void InitGameObject(GameObject go, int wrapIndex, int realIndex)
  {
    CasinoPropsItemRenderer component = go.GetComponent<CasinoPropsItemRenderer>();
    realIndex = Mathf.Abs(realIndex);
    if (realIndex >= this.datas.Count)
      return;
    component.SetShopItemData(this.datas[realIndex].internalId);
  }

  private void CreateData()
  {
    this.datas.Clear();
    string category = "resources";
    ItemBag.ItemType itemType = this.SHOPITEM_TYPE[this.displayType];
    List<ShopStaticInfo> currentSaleShopItems = ItemBag.Instance.GetCurrentSaleShopItems("shop", category);
    List<ShopStaticInfo> shopStaticInfoList1 = new List<ShopStaticInfo>();
    List<ShopStaticInfo> shopStaticInfoList2 = new List<ShopStaticInfo>();
    List<ItemStaticInfo> itemStaticInfoList = new List<ItemStaticInfo>();
    Dictionary<long, ConsumableItemData>.ValueCollection.Enumerator enumerator1 = DBManager.inst.DB_Item.Datas.Values.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(enumerator1.Current.internalId);
      if (itemStaticInfo.Type == itemType)
        itemStaticInfoList.Add(itemStaticInfo);
    }
    itemStaticInfoList.Sort(new Comparison<ItemStaticInfo>(this.CompareItem));
    this.items.Clear();
    for (int index = 0; index < itemStaticInfoList.Count; ++index)
      this.items.Add(itemStaticInfoList[index].internalId);
    List<ShopStaticInfo>.Enumerator enumerator2 = currentSaleShopItems.GetEnumerator();
    while (enumerator2.MoveNext())
    {
      if (enumerator2.Current != null)
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(enumerator2.Current.Item_InternalId);
        if (this.items.IndexOf(itemStaticInfo.internalId) <= -1 && itemStaticInfo.Type == itemType)
        {
          if (ItemBag.Instance.GetItemCount(itemStaticInfo.internalId) > 0)
            shopStaticInfoList1.Add(enumerator2.Current);
          else
            shopStaticInfoList2.Add(enumerator2.Current);
        }
      }
    }
    shopStaticInfoList1.Sort(new Comparison<ShopStaticInfo>(this.CompareShopItem));
    shopStaticInfoList2.Sort(new Comparison<ShopStaticInfo>(this.CompareShopItem));
    for (int index = 0; index < shopStaticInfoList1.Count; ++index)
      this.datas.Add(shopStaticInfoList1[index]);
    for (int index = 0; index < shopStaticInfoList2.Count; ++index)
      this.datas.Add(shopStaticInfoList2[index]);
  }

  public void OnAddGoldHandler()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  private int CompareItem(ItemStaticInfo a, ItemStaticInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }

  private int CompareShopItem(ShopStaticInfo a, ShopStaticInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int displayType;
  }
}
