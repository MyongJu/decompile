﻿// Decompiled with JetBrains decompiler
// Type: KingSkillInfoPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UI;
using UnityEngine;

public class KingSkillInfoPopup : Popup
{
  private List<KingSkillDetailItem> _allKingSkillDetailItem = new List<KingSkillDetailItem>();
  [SerializeField]
  private UILabel _labelName;
  [SerializeField]
  private UILabel _labelDesc;
  [SerializeField]
  private UITexture _textureIcon;
  [SerializeField]
  private UITable _table;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UIButton _buttonUse;
  [SerializeField]
  private UIButton _buttonSearch;
  [SerializeField]
  private KingSkillDetailItem _kingSkillDetailItemTemplate;
  [SerializeField]
  private GameObject _rootForSearchMode;
  [SerializeField]
  private GameObject _rootForNormalMode;
  [SerializeField]
  private UILabel _remainCdForSearchMode;
  [SerializeField]
  private UILabel _remianCdForNormalMode;
  [SerializeField]
  private UIInput _inputUserName;
  private KingSkillInfo _kingSkillInfo;
  private bool _meetCondition;
  private bool _needUpdateCd;
  private bool _detailsDirty;
  private string _previousMessage;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._kingSkillInfo = (orgParam as KingSkillInfoPopup.Parameter).SkillInfo;
    this._kingSkillDetailItemTemplate.gameObject.SetActive(false);
    this.UpdateUI();
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondTick);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondTick);
  }

  protected void OnSecondTick(int delta)
  {
    if (!this._needUpdateCd)
      return;
    HallOfKingData hallOfKingData = DBManager.inst.DB_HallOfKing.Get((long) PlayerData.inst.userData.world_id);
    if (hallOfKingData == null)
      return;
    long leftSkillCdTime = hallOfKingData.GetLeftSkillCdTime(this._kingSkillInfo.internalId);
    if (leftSkillCdTime >= 0L)
    {
      UILabel remainCdForSearchMode = this._remainCdForSearchMode;
      string str1 = ScriptLocalization.Get("throne_king_skill_cooldown", true) + Utils.FormatTime((int) leftSkillCdTime, false, false, true);
      this._remianCdForNormalMode.text = str1;
      string str2 = str1;
      remainCdForSearchMode.text = str2;
    }
    else
      this.UpdateUI();
  }

  protected void UpdateUI()
  {
    HallOfKingData hallOfKingData = DBManager.inst.DB_HallOfKing.Get((long) PlayerData.inst.userData.world_id);
    if (hallOfKingData == null)
    {
      D.error((object) "can not find hall of king data.");
    }
    else
    {
      this._labelName.text = this._kingSkillInfo.Loc_Name;
      this._labelDesc.text = ScriptLocalization.GetWithPara(this._kingSkillInfo.Loc_Desc, new Dictionary<string, string>()
      {
        {
          "0",
          this._kingSkillInfo.Cost.ToString()
        },
        {
          "1",
          ((int) ((double) this._kingSkillInfo.SkillParam1 - (double) this._kingSkillInfo.SkillParam2 * (double) hallOfKingData.GetSkillUsedTimes(this._kingSkillInfo.internalId))).ToString()
        },
        {
          "2",
          this._kingSkillInfo.MaxUse.ToString()
        }
      }, false);
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureIcon, this._kingSkillInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this._detailsDirty = true;
      UIButton buttonUse = this._buttonUse;
      bool flag = false;
      this._buttonSearch.isEnabled = flag;
      int num = flag ? 1 : 0;
      buttonUse.isEnabled = num != 0;
      if (this._kingSkillInfo.SkillType == "find_target")
      {
        this._rootForSearchMode.SetActive(true);
        this._rootForNormalMode.SetActive(false);
      }
      else
      {
        this._rootForSearchMode.SetActive(false);
        this._rootForNormalMode.SetActive(true);
      }
    }
  }

  protected void UpdateDetails()
  {
    HallOfKingData hallOfKingData = DBManager.inst.DB_HallOfKing.Get((long) PlayerData.inst.userData.world_id);
    if (hallOfKingData == null)
    {
      D.error((object) "can not find hall of king data.");
    }
    else
    {
      DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) PlayerData.inst.userData.world_id);
      if (wonderData == null)
      {
        D.error((object) "can not find wonder data.");
      }
      else
      {
        this.DestoryAllKingSkillDetailItem();
        long leftSkillCdTime = hallOfKingData.GetLeftSkillCdTime(this._kingSkillInfo.internalId);
        int skillUsedTimes = hallOfKingData.GetSkillUsedTimes(this._kingSkillInfo.internalId);
        this._meetCondition = true;
        if (this._kingSkillInfo.Cost > 0)
        {
          string str = ScriptLocalization.Get("throne_king_skill_currency_needed", true);
          string context;
          if (this._kingSkillInfo.Cost > hallOfKingData.KingdomCoin)
          {
            this._meetCondition = false;
            context = str + string.Format("[ff0000]{0}[-]", (object) this._kingSkillInfo.Cost);
          }
          else
            context = str + string.Format("{0}", (object) this._kingSkillInfo.Cost);
          this.CreateKingSkillDetailItem().SetContext(context);
        }
        if (this._kingSkillInfo.CD > 0)
        {
          string str = ScriptLocalization.Get("throne_king_skill_cooldown", true);
          string context;
          if (leftSkillCdTime > 0L)
          {
            this._meetCondition = false;
            context = str + string.Format("[ff0000]{0}[-]", (object) Utils.FormatTime(this._kingSkillInfo.CD, false, false, true));
          }
          else
            context = str + string.Format("{0}", (object) Utils.FormatTime(this._kingSkillInfo.CD, false, false, true));
          this.CreateKingSkillDetailItem().SetContext(context);
        }
        string str1 = ScriptLocalization.Get("throne_king_skill_times_remaining", true);
        int num1 = this._kingSkillInfo.MaxUse - skillUsedTimes;
        string context1;
        if (num1 <= 0)
        {
          this._meetCondition = false;
          context1 = str1 + string.Format("[ff0000]{0}/{1}[-]", (object) num1, (object) this._kingSkillInfo.MaxUse);
        }
        else
          context1 = str1 + string.Format("{0}/{1}", (object) num1, (object) this._kingSkillInfo.MaxUse);
        this.CreateKingSkillDetailItem().SetContext(context1);
        string str2 = ScriptLocalization.Get("throne_kings_chamber_skill_requirements", true);
        string name = ConfigManager.inst.DB_WonderTitle.Get(1.ToString()).Name;
        string context2;
        if (PlayerData.inst.uid != wonderData.KING_UID)
        {
          this._meetCondition = false;
          context2 = str2 + string.Format("[ff0000]{0}[-]", (object) name);
        }
        else
          context2 = str2 + string.Format("{0}", (object) name);
        this.CreateKingSkillDetailItem().SetContext(context2);
        UIButton buttonUse = this._buttonUse;
        bool meetCondition = this._meetCondition;
        this._buttonSearch.isEnabled = meetCondition;
        int num2 = meetCondition ? 1 : 0;
        buttonUse.isEnabled = num2 != 0;
        this._remainCdForSearchMode.gameObject.SetActive(leftSkillCdTime > 0L);
        this._remianCdForNormalMode.gameObject.SetActive(leftSkillCdTime > 0L);
        this._needUpdateCd = false;
        if (leftSkillCdTime > 0L)
        {
          this._needUpdateCd = true;
          UILabel remainCdForSearchMode = this._remainCdForSearchMode;
          string str3 = ScriptLocalization.Get("throne_king_skill_cooldown", true) + Utils.FormatTime((int) leftSkillCdTime, false, false, true);
          this._remianCdForNormalMode.text = str3;
          string str4 = str3;
          remainCdForSearchMode.text = str4;
        }
        this._table.repositionNow = true;
        this._table.Reposition();
        this._scrollView.movement = UIScrollView.Movement.Unrestricted;
        this._scrollView.ResetPosition();
        this._scrollView.currentMomentum = Vector3.zero;
        this._scrollView.movement = UIScrollView.Movement.Vertical;
      }
    }
  }

  protected void DestoryAllKingSkillDetailItem()
  {
    using (List<KingSkillDetailItem>.Enumerator enumerator = this._allKingSkillDetailItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this._allKingSkillDetailItem.Clear();
  }

  protected KingSkillDetailItem CreateKingSkillDetailItem()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._kingSkillDetailItemTemplate.gameObject);
    gameObject.SetActive(true);
    gameObject.transform.SetParent(this._table.transform);
    gameObject.transform.localScale = Vector3.one;
    KingSkillDetailItem component = gameObject.GetComponent<KingSkillDetailItem>();
    this._allKingSkillDetailItem.Add(component);
    return component;
  }

  protected void Update()
  {
    if (!this._detailsDirty)
      return;
    this._detailsDirty = false;
    this.UpdateDetails();
  }

  public void OnButtonCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnTextChanged()
  {
    string s = this._inputUserName.value.Replace("\n", string.Empty);
    if (Encoding.UTF8.GetBytes(s).Length > 16)
      s = this._previousMessage;
    this._previousMessage = s;
    this._inputUserName.value = s;
    int length = Encoding.UTF8.GetBytes(s).Length;
    this._buttonSearch.isEnabled = true & length >= 3 & length <= 16 && this._meetCondition;
  }

  public void OnButtonUseClicked()
  {
    if (this._kingSkillInfo.SkillType == "after_war_training" || this._kingSkillInfo.SkillType == "get_gold")
    {
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
      Hashtable postData = new Hashtable()
      {
        {
          (object) "skill_id",
          (object) this._kingSkillInfo.internalId
        }
      };
      RequestManager.inst.SendRequest("king:castKingSkill", postData, (System.Action<bool, object>) null, true);
    }
    else if (this._kingSkillInfo.SkillType == "force_teleport" || this._kingSkillInfo.SkillType == "send_troop_forbidden")
    {
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      HallOfKingdomUtils.CastKingSkill(this._kingSkillInfo);
    }
    else
    {
      if (!(this._kingSkillInfo.SkillType == "find_target"))
        return;
      Hashtable postData = new Hashtable()
      {
        {
          (object) "skill_id",
          (object) this._kingSkillInfo.internalId
        },
        {
          (object) "params",
          (object) new Hashtable()
          {
            {
              (object) "target_name",
              (object) this._inputUserName.value
            }
          }
        }
      };
      RequestManager.inst.SendRequest("king:castKingSkill", postData, (System.Action<bool, object>) ((result, data) =>
      {
        if (!result)
          return;
        Hashtable hashtable = data as Hashtable;
        if (hashtable == null || !hashtable.ContainsKey((object) "k") || (!hashtable.ContainsKey((object) "x") || !hashtable.ContainsKey((object) "y")))
          return;
        int result1;
        int.TryParse(hashtable[(object) "k"].ToString(), out result1);
        int result2;
        int.TryParse(hashtable[(object) "x"].ToString(), out result2);
        int result3;
        int.TryParse(hashtable[(object) "y"].ToString(), out result3);
        Coordinate coordinate = new Coordinate(result1, result2, result3);
        if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
          UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
          {
            GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
            PVPMap.PendingGotoRequest = coordinate;
          }));
        else
          PVPSystem.Instance.Map.GotoLocation(coordinate, false);
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
        UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      }), true);
    }
  }

  public class Parameter : Popup.PopupParameter
  {
    public KingSkillInfo SkillInfo;
  }
}
