﻿// Decompiled with JetBrains decompiler
// Type: ConfigActivityMainSub
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigActivityMainSub
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, ActivityMainSubInfo> datas;
  private Dictionary<int, ActivityMainSubInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<ActivityMainSubInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public ActivityMainSubInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (ActivityMainSubInfo) null;
  }

  public ActivityMainSubInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (ActivityMainSubInfo) null;
  }
}
