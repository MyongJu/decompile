﻿// Decompiled with JetBrains decompiler
// Type: MarchPathScroll
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MarchPathScroll : MonoBehaviour
{
  [SerializeField]
  public float m_Speed = 0.223091f;
  private Vector2 m_UVCache = new Vector2();
  [SerializeField]
  private MeshRenderer m_Renderer;
  private float m_CurrentOffset;
  private float m_UVScale;
  private float m_CurrentRealTime;

  public void SetPath(Vector3 startPosition, Vector3 endPosition, Transform parent)
  {
    if (!(bool) ((Object) this.gameObject))
      return;
    this.transform.parent = parent;
    this.transform.localScale = Vector3.one;
    this.transform.position = (startPosition + endPosition) * 0.5f;
    this.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, (float) (-(double) Mathf.Atan2(endPosition.x - startPosition.x, endPosition.y - startPosition.y) * 57.2957801818848));
    if ((Object) this.m_Renderer == (Object) null || (Object) parent == (Object) null)
      return;
    Vector3 vector3 = endPosition - startPosition;
    Vector3 localScale = this.m_Renderer.transform.localScale;
    localScale.Set(localScale.x, vector3.magnitude / parent.lossyScale.x, 1f);
    this.m_Renderer.transform.localScale = localScale;
    this.m_UVScale = localScale.y / localScale.x;
    this.m_UVCache.Set(1f, this.m_UVScale);
    this.m_Renderer.material.SetTextureScale("_MainTex", this.m_UVCache);
    this.m_CurrentRealTime = Time.realtimeSinceStartup;
  }

  private void Update()
  {
    float num = Time.realtimeSinceStartup - this.m_CurrentRealTime;
    this.m_CurrentRealTime = Time.realtimeSinceStartup;
    this.m_CurrentOffset -= this.m_Speed * num;
    this.m_CurrentOffset %= this.m_UVScale;
    if (!((Object) this.m_Renderer != (Object) null))
      return;
    this.m_UVCache.Set(0.0f, this.m_CurrentOffset);
    this.m_Renderer.material.SetTextureOffset("_MainTex", this.m_UVCache);
  }
}
