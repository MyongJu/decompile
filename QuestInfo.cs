﻿// Decompiled with JetBrains decompiler
// Type: QuestInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class QuestInfo
{
  public int internalId;
  public string ID;
  public int Type;
  public int Priority;
  public string Prerequisite;
  public string Image;
  public string Category;
  public int Quality;
  public List<QuestInfo.Requirement> requirements;
  public int Food_Payout;
  public int Wood_Payout;
  public int Ore_Payout;
  public int Silver_Payout;
  public int Gold_PayOut;
  public int Reward_ID_1;
  public int Reward_ID_2;
  public int Reward_Value_1;
  public int Reward_Value_2;
  public int DailyStreak;
  public bool Visible;
  public string Name;
  public string Brief;
  public string Description;
  public int LinkHudId;
  public int power;
  public double normalChance;
  public double refreshChance;
  public string Formula;
  public int Value;
  public string Loc_Name;
  public string Loc_Desc;
  public int groupId;
  public int Hero_XP;
  public int Honor;
  public int Alliance_XP;
  public int Alliance_Fund;
  public int Duration_Time;

  public enum Cat
  {
    Build,
    March,
    Train,
    Research,
    Daily,
    Alliance,
    PVE,
    PVP,
    Resource,
    COUNT,
  }

  public enum RequirementType
  {
    event_building_built,
    pve,
    event_unit_trained,
    timer,
    player,
    resource,
    alliance,
    event_research_finished,
    daily,
    alliance_point,
    daily_point,
    event_march,
    event_alliance_joined,
    event_unit_killed,
    event_pve,
  }

  public class Requirement
  {
    public QuestInfo.RequirementType Requirement_Type;
    public string Requirement_ID;
    public int Requirement_Value;
  }
}
