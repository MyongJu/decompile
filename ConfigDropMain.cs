﻿// Decompiled with JetBrains decompiler
// Type: ConfigDropMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDropMain
{
  private ConfigParseEx<DropMainData> _parse = new ConfigParseEx<DropMainData>();
  private ConfigFieldCache<int> _groupCache = new ConfigFieldCache<int>();

  public void BuildDB(object result)
  {
    this._parse.Initialize(result as Hashtable);
    this._groupCache.Initialize((ConfigRawData) this._parse, "drop_group_id");
  }

  public DropMainData GetByInternalId(int internalId)
  {
    DropMainData dropMainData = this._parse.Get(internalId);
    if (dropMainData != null)
      dropMainData.internalId = internalId;
    return dropMainData;
  }

  public DropMainData GetByUniqueId(string uniqueId)
  {
    return this.GetByInternalId(this._parse.s2i(uniqueId));
  }

  public List<DropMainData> GetDropMainDataListByDropGroupId(int groupId)
  {
    List<DropMainData> dropMainDataList = new List<DropMainData>();
    if (groupId != 0)
    {
      List<int> internalIds = this._parse.InternalIds;
      for (int index = 0; index < internalIds.Count; ++index)
      {
        int internalId = internalIds[index];
        if (this._groupCache.Get(internalId, 0) == groupId)
        {
          DropMainData byInternalId = this.GetByInternalId(internalId);
          dropMainDataList.Add(byInternalId);
        }
      }
    }
    return dropMainDataList;
  }
}
