﻿// Decompiled with JetBrains decompiler
// Type: SevenDaysStoreItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class SevenDaysStoreItemRenderer : MonoBehaviour
{
  private SaleItemData _data;
  private int _day;
  public ItemIconRenderer itemRenderer;
  public UILabel itemName;
  public UIButton buyBt;
  public GameObject discountGo;
  public GameObject normal;
  public UILabel status;
  public GameObject gayGo;
  public GameObject statusGo;
  public UILabel price;
  public UILabel discountPrice;
  public UILabel normalPrice;
  public UILabel storageLabel;
  public UILabel discount;
  public Color normalColor;
  private bool _isDestroy;

  public void SetData(int day, SaleItemData data)
  {
    if (this._day == day)
    {
      this.CheckStatus();
    }
    else
    {
      this._day = day;
      this._data = data;
      this.UpdateUI();
      this.CheckStatus();
    }
  }

  private void CheckStatus()
  {
    if (this._isDestroy)
      return;
    this._data.Remain = DBManager.inst.DB_SevenDays.GetSaleItemDataRemain(this._day, this._data.ItemId, this._data.MaxCount);
    string Term1 = "event_7_days_sale_not_started";
    bool flag1 = false;
    bool flag2 = false;
    bool state = (double) this._data.Discount <= 0.0;
    Color color = Color.green;
    SevenDaysData sevenDaysData = DBManager.inst.DB_SevenDays.GetSevenDaysData(this._day);
    ConfigManager.inst.DB_SevenDayRewards.GetRewardInfo(this._day);
    string Term2 = "tag_in_stock";
    Dictionary<string, string> para = new Dictionary<string, string>();
    string empty = string.Empty;
    if (sevenDaysData != null)
    {
      Color green = Color.green;
      this._data.Remain = DBManager.inst.DB_SevenDays.GetSaleItemDataRemain(this._day, this._data.ItemId, this._data.MaxCount);
      flag2 = sevenDaysData.IsStart;
      bool flag3 = this._data.Remain <= 0;
      flag1 = sevenDaysData.IsExpired;
      if (!flag1)
        flag1 = this._data.Remain == 0;
      if (!sevenDaysData.IsStart)
      {
        color = Color.red;
        NGUITools.SetActive(this.buyBt.gameObject, false);
      }
      else
      {
        color = Color.green;
        NGUITools.SetActive(this.buyBt.gameObject, !flag3 && !sevenDaysData.IsExpired);
      }
      if (flag3)
      {
        Term1 = "id_uppercase_sold_out";
        color = Color.red;
        empty = string.Empty;
      }
      if (sevenDaysData.IsExpired)
      {
        Term1 = "id_expired";
        color = Color.red;
      }
    }
    else
      NGUITools.SetActive(this.buyBt.gameObject, false);
    NGUITools.SetActive(this.statusGo, !this.buyBt.gameObject.activeSelf);
    if (flag1)
      GreyUtility.Grey(this.itemRenderer.gameObject);
    else
      GreyUtility.Normal(this.itemRenderer.gameObject);
    this.status.text = ScriptLocalization.Get(Term1, true);
    this.status.color = color;
    NGUITools.SetActive(this.normal, state);
    NGUITools.SetActive(this.discountGo, !state);
    para.Add("0", empty + (object) this._data.Remain + string.Empty);
    this.storageLabel.text = ScriptLocalization.GetWithPara(Term2, para, true);
    UILabel normalPrice = this.normalPrice;
    string str1 = this._data.Discount.ToString();
    this.price.text = str1;
    string str2 = str1;
    normalPrice.text = str2;
    this.discountPrice.text = this._data.NowPrice.ToString();
    this.OnUserDataUpdate(PlayerData.inst.uid);
    string Term3 = "id_uppercase_hot";
    para.Clear();
    if ((double) this._data.Discount > 0.0)
    {
      Term3 = "tag_percentage_off";
      int num = 100 - Mathf.CeilToInt((float) ((double) this._data.Price / (double) this._data.Discount * 100.0));
      para.Add("0", num.ToString());
    }
    this.discount.text = ScriptLocalization.GetWithPara(Term3, para, true);
  }

  private void OnDestroy()
  {
    this._isDestroy = true;
  }

  private void OnBuyCallback(bool success, object result)
  {
    if (!success)
      return;
    this.CheckStatus();
  }

  private void OnEnable()
  {
    DBManager.inst.DB_SevenDays.onDataChanged += new System.Action<long>(this.OnDataChange);
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdate);
  }

  private void OnUserDataUpdate(long uid)
  {
    if (PlayerData.inst.uid != uid)
      return;
    Color color1 = this.normalColor;
    if ((long) this._data.NowPrice > PlayerData.inst.userData.currency.gold)
      color1 = Color.red;
    UILabel price = this.price;
    Color color2 = color1;
    this.discountPrice.color = color2;
    Color color3 = color2;
    price.color = color3;
    Color color4 = this.normalColor;
    if ((long) this._data.Price > PlayerData.inst.userData.currency.gold)
      color4 = Color.red;
    this.normalPrice.color = color4;
  }

  private void OnDisable()
  {
    if (!GameEngine.IsAvailable)
      return;
    DBManager.inst.DB_SevenDays.onDataChanged -= new System.Action<long>(this.OnDataChange);
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdate);
  }

  private void OnDataChange(long config_id)
  {
    this.CheckStatus();
  }

  public void OnBuyHandler()
  {
    if (this._data.Remain <= 0)
      return;
    if ((long) this._data.NowPrice > PlayerData.inst.userData.currency.gold)
      Utils.ShowNotEnoughGoldTip();
    else
      UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) new ItemUseOrBuyPopup.Parameter()
      {
        item_id = this._data.ItemId,
        maxCount = this._data.Remain,
        price = this._data.NowPrice,
        type = ItemUseOrBuyPopup.Parameter.Type.BuyItem,
        onUseOrBuyCallBack = new System.Action<bool, object>(this.OnBuyCallback)
      });
  }

  private void UpdateUI()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._data.ItemId);
    if (itemStaticInfo == null)
      return;
    IconData iconData = new IconData(itemStaticInfo.ImagePath, new string[1]
    {
      itemStaticInfo.LocName
    });
    this.itemName.text = itemStaticInfo.LocName;
    this.itemRenderer.SetData(this._data.ItemId, string.Empty, false);
  }
}
