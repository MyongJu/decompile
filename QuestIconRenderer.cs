﻿// Decompiled with JetBrains decompiler
// Type: QuestIconRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class QuestIconRenderer : MonoBehaviour
{
  private const int MAX_SIZE = 310;
  public UILabel count;
  public UISprite questIconSprite;
  public UITexture questIconUITexture;
  public GameObject questIconGameObject;
  public GameObject arrow;
  public GameObject countContainer;
  private GameObject uI2DSpriteIcon;
  private long _questId;

  public void SetData(long questId)
  {
    this._questId = questId;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    BasePresent basePresentById = Utils.GetBasePresentByID(this._questId);
    basePresentById.Refresh();
    this.count.text = Utils.FormatShortThousands(basePresentById.Count);
    NGUITools.SetActive(this.countContainer, basePresentById.NeedDisplayCount);
    NGUITools.SetActive(this.arrow, basePresentById.IsUpgarde);
    NGUITools.SetActive(this.questIconSprite.gameObject, false);
    NGUITools.SetActive(this.questIconUITexture.gameObject, false);
    NGUITools.SetActive(this.questIconGameObject, false);
    switch (basePresentById.IconType)
    {
      case 0:
        this.RefreshUISprite(basePresentById.IconPath);
        break;
      case 1:
        this.RefreshUITexture(basePresentById.IconPath);
        break;
      case 2:
        this.RefreshGameObject(basePresentById.IconPath);
        break;
    }
  }

  public void Clear()
  {
    for (int index = 0; index < this.questIconGameObject.transform.childCount; ++index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.questIconGameObject.transform.GetChild(index).gameObject);
    BuilderFactory.Instance.Release((UIWidget) this.questIconUITexture);
  }

  private void RefreshUISprite(string name)
  {
    NGUITools.SetActive(this.questIconSprite.gameObject, true);
    this.questIconSprite.spriteName = name;
  }

  private void RefreshUITexture(string path)
  {
    this.Clear();
    NGUITools.SetActive(this.questIconUITexture.gameObject, true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.questIconUITexture, path, (System.Action<bool>) null, true, false, string.Empty);
  }

  private void RefreshGameObject(string path)
  {
    GameObject prefab = AssetManager.Instance.HandyLoad(path, (System.Type) null) as GameObject;
    NGUITools.SetActive(this.questIconGameObject, true);
    if (!((UnityEngine.Object) prefab != (UnityEngine.Object) null))
      return;
    this.Clear();
    GameObject gameObject = NGUITools.AddChild(this.questIconGameObject, prefab);
    gameObject.GetComponent<UI2DSprite>().depth = 3;
    UI2DSprite component = gameObject.GetComponent<UI2DSprite>();
    component.depth = 4;
    if (component.width > 310)
    {
      float num = 310f / (float) component.width;
      component.width = 310;
      component.height = (int) ((double) component.height * (double) num);
    }
    if (component.height <= 310)
      return;
    float num1 = 310f / (float) component.height;
    component.height = 310;
    component.width = (int) ((double) component.width * (double) num1);
  }
}
