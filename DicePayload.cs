﻿// Decompiled with JetBrains decompiler
// Type: DicePayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;

public class DicePayload
{
  private DiceData _tempDiceData = new DiceData();
  private static DicePayload _instance;

  public static DicePayload Instance
  {
    get
    {
      if (DicePayload._instance == null)
        DicePayload._instance = new DicePayload();
      return DicePayload._instance;
    }
  }

  public DiceData TempDiceData
  {
    get
    {
      if (this._tempDiceData == null)
        this._tempDiceData = new DiceData();
      return this._tempDiceData;
    }
  }

  public string Theme
  {
    get
    {
      DiceGroupInfo diceGroupInfo = ConfigManager.inst.DB_DiceGroup.Get(this.TempDiceData.GroupId);
      if (diceGroupInfo != null)
        return "dice_theme_entrance_" + (object) diceGroupInfo.themeId;
      return "dice_theme_entrance_1";
    }
  }

  public string Title
  {
    get
    {
      DiceGroupInfo diceGroupInfo = ConfigManager.inst.DB_DiceGroup.Get(this.TempDiceData.GroupId);
      if (diceGroupInfo != null)
        return diceGroupInfo.Title;
      return string.Empty;
    }
  }

  public bool EntranceShow
  {
    get
    {
      if (this.TempDiceData.EndTime > NetServerTime.inst.ServerTimestamp)
        return this.TempDiceData.StartTime <= NetServerTime.inst.ServerTimestamp;
      return false;
    }
  }

  public bool HasRewardCollectByScore
  {
    get
    {
      List<DiceScoreBoxInfo> diceScoreBoxList = ConfigManager.inst.DB_DiceScoreBox.DiceScoreBoxList;
      if (diceScoreBoxList != null)
      {
        for (int key = 0; key < diceScoreBoxList.Count; ++key)
        {
          if ((long) diceScoreBoxList[key].score <= this.TempDiceData.Score && this.TempDiceData.ScoreStatusDict.ContainsKey(key) && this.TempDiceData.ScoreStatusDict[key] == 0)
            return true;
        }
      }
      return false;
    }
  }

  public bool HasEnoughGold2Refresh
  {
    get
    {
      return PlayerData.inst.userData.currency.gold >= (long) this.RefreshCost;
    }
  }

  public bool IsRefreshFree
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("dice_refresh_free_daily_number");
      return data != null && data.ValueInt > this.TempDiceData.DiceRewardRefreshTimes;
    }
  }

  public int RefreshCost
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("dice_refresh_costgold");
      if (data != null)
        return data.ValueInt;
      return 0;
    }
  }

  public bool IsDiceOneFree
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("dice_free_daily_number");
      return data != null && data.ValueInt > this.TempDiceData.DicePlayTimes;
    }
  }

  public int DiceOneCost
  {
    get
    {
      DiceGroupInfo diceGroupInfo = ConfigManager.inst.DB_DiceGroup.Get(this.TempDiceData.GroupId);
      if (diceGroupInfo != null)
        return diceGroupInfo.PlayOneCost;
      return 0;
    }
  }

  public int DiceTenCost
  {
    get
    {
      DiceGroupInfo diceGroupInfo = ConfigManager.inst.DB_DiceGroup.Get(this.TempDiceData.GroupId);
      if (diceGroupInfo != null)
        return diceGroupInfo.PlayTenCost;
      return 0;
    }
  }

  public bool HasEnoughProps2PlayOne
  {
    get
    {
      return this.TempDiceData.PropsCount >= (long) this.DiceOneCost;
    }
  }

  public bool HasEnoughProps2PlayTen
  {
    get
    {
      return this.TempDiceData.PropsCount >= (long) this.DiceTenCost;
    }
  }

  public int GetLeftDiceNum(DiceData.RewardData rewardData)
  {
    using (Dictionary<int, DiceData.RewardData>.Enumerator enumerator = this.TempDiceData.RewardsDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, DiceData.RewardData> current = enumerator.Current;
        if (rewardData.IsRewardScore)
        {
          if (rewardData.Amount == current.Value.Amount)
            return current.Key + 1;
        }
        else if (rewardData.ItemId == current.Value.ItemId)
          return current.Key + 1;
      }
    }
    return 0;
  }

  public void ShowDiceScene()
  {
    this.RequestServerData((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("Dice/DicePlayDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    }));
  }

  public void Initialize()
  {
    this.RequestServerData((System.Action<bool, object>) null);
    MessageHub.inst.GetPortByAction("open_craps").AddEvent(new System.Action<object>(this.DiceDataPush));
    MessageHub.inst.GetPortByAction("close_craps").AddEvent(new System.Action<object>(this.DiceDataPush));
  }

  public void Dispose()
  {
    this._tempDiceData = (DiceData) null;
    MessageHub.inst.GetPortByAction("open_craps").RemoveEvent(new System.Action<object>(this.DiceDataPush));
    MessageHub.inst.GetPortByAction("close_craps").RemoveEvent(new System.Action<object>(this.DiceDataPush));
  }

  public void ShowCollectEffect(int itemId, int count)
  {
    RewardsCollectionAnimator.Instance.Clear();
    RewardsCollectionAnimator.Instance.items2.Add(new Item2RewardInfo.Data()
    {
      count = (float) count,
      itemid = itemId
    });
    RewardsCollectionAnimator.Instance.CollectItems2(false);
    AudioManager.Instance.StopAndPlaySound("sfx_rural_resource_boost");
  }

  public void ShowCollectEffect(List<Item2RewardInfo.Data> rewards)
  {
    if (rewards == null || rewards.Count <= 0)
      return;
    RewardsCollectionAnimator.Instance.Clear();
    using (List<Item2RewardInfo.Data>.Enumerator enumerator = rewards.GetEnumerator())
    {
      while (enumerator.MoveNext())
        RewardsCollectionAnimator.Instance.items2.Add(enumerator.Current);
    }
    RewardsCollectionAnimator.Instance.CollectItems2(false);
    AudioManager.Instance.StopAndPlaySound("sfx_rural_resource_boost");
  }

  public void ShowCollectEffect(List<RewardScoreInfo.Data> scores)
  {
    if (scores == null || scores.Count <= 0)
      return;
    RewardsCollectionAnimator.Instance.Clear();
    using (List<RewardScoreInfo.Data>.Enumerator enumerator = scores.GetEnumerator())
    {
      while (enumerator.MoveNext())
        RewardsCollectionAnimator.Instance.scores.Add(enumerator.Current);
    }
    RewardsCollectionAnimator.Instance.CollectScore(false);
    AudioManager.Instance.StopAndPlaySound("sfx_rural_resource_boost");
  }

  public void RefreshDiceData(System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("casino:refreshCraps").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      this.Decode(data as Hashtable);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void PlayDice(int playCount, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("casino:crap").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "number",
        (object) playCount
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      this.UpdateScore(data as Hashtable);
      this.UpdateTodayPlayTimes(data as Hashtable);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void ExchangeDiceReward(int line, int index, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("casino:exchangeCrapsReward").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) nameof (line),
        (object) line
      },
      {
        (object) nameof (index),
        (object) index
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      this.Decode(data as Hashtable);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void RequestServerData(System.Action<bool, object> callback = null)
  {
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    if (callback != null)
      MessageHub.inst.GetPortByAction("casino:getCrapsInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          this.Decode(data as Hashtable);
        if (callback == null)
          return;
        callback(ret, data);
      }), true);
    else
      MessageHub.inst.GetPortByAction("casino:getCrapsInfo").SendLoader(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.Decode(data as Hashtable);
      }), false, false);
  }

  private void DiceDataPush(object data)
  {
    this.TempDiceData.ScoreStatusDict.Clear();
    this.Decode(data as Hashtable);
  }

  private void UpdateScore(Hashtable data)
  {
    if (data == null || !data.ContainsKey((object) "integral"))
      return;
    long result = 0;
    long.TryParse(data[(object) "integral"].ToString(), out result);
    if (this._tempDiceData == null)
      return;
    this._tempDiceData.Score = result;
  }

  private void UpdateTodayPlayTimes(Hashtable data)
  {
    if (data == null || !data.ContainsKey((object) "today_crap"))
      return;
    int result = 0;
    int.TryParse(data[(object) "today_crap"].ToString(), out result);
    if (this._tempDiceData == null)
      return;
    this._tempDiceData.DicePlayTimes = result;
  }

  private void Decode(Hashtable data)
  {
    if (data == null)
      return;
    this.TempDiceData.Decode(data);
  }
}
