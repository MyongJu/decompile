﻿// Decompiled with JetBrains decompiler
// Type: HeroPositionAssignDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HeroPositionAssignDlg : UI.Dialog
{
  private List<HeroBookBenefitItem> _itemBenefitList = new List<HeroBookBenefitItem>();
  private Dictionary<int, HeroPositionAssignSlot> _itemDict = new Dictionary<int, HeroPositionAssignSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private Color _benefitActiveColor = new Color(0.21f, 0.67f, 0.12f);
  [SerializeField]
  private UIScrollView _heroScrollView;
  [SerializeField]
  private UITable _heroTable;
  [SerializeField]
  private GameObject _heroItemPrefab;
  [SerializeField]
  private GameObject _heroItemRoot;
  [SerializeField]
  private GameObject _noHeroHintNode;
  [SerializeField]
  private UILabel _heroPositionName;
  [SerializeField]
  private UIButton _assignButton;
  [SerializeField]
  private UIButton _removeButton;
  [SerializeField]
  private UILabel _assignButtonText;
  [SerializeField]
  private HeroBookBenefitItem _benefitItemPrefab;
  [SerializeField]
  private UITable _heroBaseBenefitTable;
  [SerializeField]
  private UITable _heroStarBenefitTable;
  [SerializeField]
  private UITable _heroSuitBenefitTable;
  [SerializeField]
  private UITable _heroContentTable;
  [SerializeField]
  private UIScrollView _heroContentScrollView;
  [SerializeField]
  private UILabel _heroSuitName;
  [SerializeField]
  private GameObject _heroSuitNode;
  [SerializeField]
  private GameObject _heroSuitFrameNode;
  [SerializeField]
  private GameObject _heroBaseNode;
  [SerializeField]
  private GameObject _heroBaseFrame;
  [SerializeField]
  private GameObject _heroStarNode;
  [SerializeField]
  private GameObject _heroStarFrame;
  private int _heroPosition;
  private LegendCardData _legendCardData;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    HeroPositionAssignDlg.Parameter parameter = orgParam as HeroPositionAssignDlg.Parameter;
    if (parameter != null)
      this._heroPosition = parameter.heroPosition;
    this.UpdatePanelInfo();
    this.UpdateUI();
    this.UpdateLeftUI();
    this.UpdateAssignButtonStatus();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
    this.ClearBenefitData();
  }

  public void OnAssignButtonClicked()
  {
    if (this._legendCardData == null)
      return;
    HeroCardUtils.AppointPosition(PlayerData.inst.uid, this._legendCardData.LegendId, this._legendCardData.OriginPosition, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      PlayerPrefsEx.SetInt("hero_card_is_new_" + this._legendCardData.LegendId.ToString(), 0);
      AudioManager.Instance.StopAndPlaySound("sfx_city_hero_appoint");
      this.UpdateHeroInfo();
      this.UpdateAssignButtonStatus();
      this.UpdateLeftUI();
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_hero_appoint_success", new Dictionary<string, string>()
      {
        {
          "0",
          this._legendCardData.HeroName
        }
      }, true), (System.Action) null, 4f, true);
    }));
  }

  public void OnDismissButtonClicked()
  {
    if (this._legendCardData == null)
      return;
    HeroCardUtils.ResignPosition(PlayerData.inst.uid, this._legendCardData.OriginPosition, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      AudioManager.Instance.StopAndPlaySound("sfx_city_hero_dismiss");
      this.UpdateHeroInfo();
      this.UpdateAssignButtonStatus();
      this.UpdateLeftUI();
    }));
  }

  private void UpdateHeroInfo()
  {
    List<LegendCardData> cardDataListByUid = DBManager.inst.DB_LegendCard.GetLegendCardDataListByUid(PlayerData.inst.uid);
    if (cardDataListByUid == null)
      return;
    using (Dictionary<int, HeroPositionAssignSlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, HeroPositionAssignSlot> keyValue = enumerator.Current;
        LegendCardData legendCardData = cardDataListByUid.Find((Predicate<LegendCardData>) (x => x.LegendId == keyValue.Value.LegendId));
        if (legendCardData != null)
          keyValue.Value.SetData(legendCardData);
      }
    }
  }

  private void UpdateUI()
  {
    this.ClearData();
    List<LegendCardData> cardDataListByUid = DBManager.inst.DB_LegendCard.GetLegendCardDataListByUid(PlayerData.inst.uid);
    if (cardDataListByUid != null && cardDataListByUid.Count > 0)
    {
      List<LegendCardData> all = cardDataListByUid.FindAll((Predicate<LegendCardData>) (x => x.OriginPosition == this._heroPosition));
      if (all != null && all.Count > 0)
      {
        for (int key = 0; key < all.Count; ++key)
        {
          HeroPositionAssignSlot itemRenderer = this.CreateItemRenderer(all[key]);
          this._itemDict.Add(key, itemRenderer);
        }
        if (all.Count > 0 && this._itemDict.ContainsKey(0))
        {
          this._legendCardData = all[0];
          this._itemDict[0].RefreshUI(this._legendCardData);
          this._assignButton.isEnabled = true;
        }
      }
      else
      {
        NGUITools.SetActive(this._noHeroHintNode, true);
        this._assignButton.isEnabled = false;
      }
    }
    else
    {
      NGUITools.SetActive(this._noHeroHintNode, true);
      this._assignButton.isEnabled = false;
    }
    this.Reposition();
  }

  private void UpdatePanelInfo()
  {
    ParliamentInfo parliamentInfo = ConfigManager.inst.DB_Parliament.Get(this._heroPosition.ToString());
    if (parliamentInfo == null)
      return;
    this._heroPositionName.text = parliamentInfo.Name;
  }

  private void ClearData()
  {
    using (Dictionary<int, HeroPositionAssignSlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, HeroPositionAssignSlot> current = enumerator.Current;
        current.Value.OnHeroFramePressed -= new System.Action<LegendCardData>(this.OnHeroFramePressed);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
    this._legendCardData = (LegendCardData) null;
  }

  private void Reposition()
  {
    this._heroTable.repositionNow = true;
    this._heroTable.Reposition();
    this._heroScrollView.ResetPosition();
    this._heroScrollView.gameObject.SetActive(false);
    this._heroScrollView.gameObject.SetActive(true);
  }

  private HeroPositionAssignSlot CreateItemRenderer(LegendCardData legendCardData)
  {
    this._itemPool.Initialize(this._heroItemPrefab, this._heroItemRoot);
    GameObject gameObject = this._itemPool.AddChild(this._heroTable.gameObject);
    gameObject.transform.localScale = Vector3.one * 0.76f;
    gameObject.SetActive(true);
    HeroPositionAssignSlot component = gameObject.GetComponent<HeroPositionAssignSlot>();
    component.SetData(legendCardData);
    component.OnHeroFramePressed += new System.Action<LegendCardData>(this.OnHeroFramePressed);
    return component;
  }

  private void OnHeroFramePressed(LegendCardData legendCardData)
  {
    using (Dictionary<int, HeroPositionAssignSlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.RefreshUI(legendCardData);
    }
    this._legendCardData = legendCardData;
    this.UpdateAssignButtonStatus();
    this.UpdateLeftUI();
  }

  private void UpdateLeftUI()
  {
    NGUITools.SetActive(this._heroBaseNode, this._legendCardData != null);
    NGUITools.SetActive(this._heroBaseFrame, this._legendCardData != null);
    NGUITools.SetActive(this._heroStarNode, this._legendCardData != null);
    NGUITools.SetActive(this._heroStarFrame, this._legendCardData != null);
    NGUITools.SetActive(this._heroSuitNode, this._legendCardData != null);
    NGUITools.SetActive(this._heroSuitFrameNode, this._legendCardData != null);
    if (this._legendCardData == null)
      return;
    this.ClearBenefitData();
    this.UpdateHeroBaseBenefit();
    this.UpdateHeroStarBenefit();
    this.UpdateHeroSuitBenefit();
    this._heroContentTable.Reposition();
    this._heroContentScrollView.ResetPosition();
  }

  private void UpdateHeroBaseBenefit()
  {
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this._legendCardData.LegendId);
    HeroBookBenefitItem heroBookBenefitItem = (HeroBookBenefitItem) null;
    if (parliamentHeroInfo != null)
    {
      heroBookBenefitItem = this.CreateBenefitItem(this._heroBaseBenefitTable.transform, Utils.XLAT("hero_group_benefit_current_level"), parliamentHeroInfo.levelBenefit, parliamentHeroInfo.GetLevelBenefitByLevel(this._legendCardData.Level));
      heroBookBenefitItem.SetTextColor(this._benefitActiveColor);
      if (!this._legendCardData.IsMaxLevel)
      {
        heroBookBenefitItem = this.CreateBenefitItem(this._heroBaseBenefitTable.transform, Utils.XLAT("hero_group_benefit_next_level"), parliamentHeroInfo.levelBenefit, parliamentHeroInfo.GetLevelBenefitByLevel(this._legendCardData.Level + 1));
        heroBookBenefitItem.SetTextColor(Color.grey);
      }
    }
    if ((UnityEngine.Object) heroBookBenefitItem != (UnityEngine.Object) null)
      heroBookBenefitItem.SetLineEnabled(false);
    this._heroBaseBenefitTable.Reposition();
  }

  private void UpdateHeroStarBenefit()
  {
    HeroBookBenefitItem heroBookBenefitItem = (HeroBookBenefitItem) null;
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this._legendCardData.LegendId);
    Dictionary<string, string> para = new Dictionary<string, string>();
    int currentStars = HeroCardUtils.GetCurrentStars(this._legendCardData.LegendId);
    for (int index = 0; index < parliamentHeroInfo.AllStarBenefit.Length; ++index)
    {
      para.Remove("0");
      para.Add("0", parliamentHeroInfo.AllStarRequirement[index].ToString());
      if (parliamentHeroInfo.AllStarBenefit[index] != 0)
      {
        heroBookBenefitItem = this.CreateBenefitItem(this._heroStarBenefitTable.transform, ScriptLocalization.GetWithPara("hero_star_level_num", para, true), parliamentHeroInfo.AllStarBenefit[index], parliamentHeroInfo.AllStarBenefitValue[index]);
        if (currentStars >= parliamentHeroInfo.AllStarRequirement[index])
          heroBookBenefitItem.SetTextColor(this._benefitActiveColor);
        else
          heroBookBenefitItem.SetTextColor(Color.grey);
      }
    }
    if ((UnityEngine.Object) heroBookBenefitItem != (UnityEngine.Object) null)
      heroBookBenefitItem.SetLineEnabled(false);
    this._heroStarBenefitTable.Reposition();
  }

  private void UpdateHeroSuitBenefit()
  {
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this._legendCardData.LegendId);
    if (parliamentHeroInfo == null)
      return;
    ParliamentSuitGroupInfo parliamentSuitGroupInfo = ConfigManager.inst.DB_ParliamentSuitGroup.Get(parliamentHeroInfo.heroSuitGroup);
    if (parliamentSuitGroupInfo != null)
    {
      HeroBookBenefitItem heroBookBenefitItem = (HeroBookBenefitItem) null;
      int num1 = Mathf.Min(HeroCardUtils.GetStartSuitCount(HeroCardUtils.GetCurrentHeroSuitActiveCount(this._legendCardData.LegendId), parliamentSuitGroupInfo.AllSuitRequirement), Mathf.Max(parliamentSuitGroupInfo.AllSuitRequirement));
      string[] strArray = new string[3]
      {
        "hero_group_benefit_current_level",
        "hero_group_benefit_next_level",
        "hero_group_benefit_max_level"
      };
      int num2 = 0;
      int num3 = 3;
      int index1 = 0;
      for (int index2 = 0; index2 < parliamentSuitGroupInfo.AllSuitBenefit.Length; ++index2)
      {
        if (parliamentSuitGroupInfo.AllSuitBenefit[index2] != 0)
        {
          if (num1 <= parliamentSuitGroupInfo.AllSuitRequirement[index2])
          {
            Dictionary<string, string> para = new Dictionary<string, string>()
            {
              {
                "0",
                parliamentSuitGroupInfo.AllSuitRequirement[index2].ToString()
              },
              {
                "1",
                parliamentSuitGroupInfo.Name
              }
            };
            index1 = index2;
            heroBookBenefitItem = this.CreateBenefitItem(this._heroSuitBenefitTable.transform, Utils.XLAT(strArray[num2++]) + ScriptLocalization.GetWithPara("hero_appoint_num_group_benefits", para, true), parliamentSuitGroupInfo.AllSuitBenefit[index2], parliamentSuitGroupInfo.AllSuitBenefitValue[index2]);
            if (num1 == parliamentSuitGroupInfo.AllSuitRequirement[index2] && num2 == 1)
              heroBookBenefitItem.SetTextColor(this._benefitActiveColor);
          }
          if (num3 == num2)
            break;
        }
      }
      if (num2 > 0 && num2 < num3)
      {
        heroBookBenefitItem = this.CreateBenefitItem(this._heroSuitBenefitTable.transform, Utils.XLAT("hero_group_benefit_max_level") + ScriptLocalization.GetWithPara("hero_appoint_num_group_benefits", new Dictionary<string, string>()
        {
          {
            "0",
            parliamentSuitGroupInfo.AllSuitRequirement[index1].ToString()
          },
          {
            "1",
            parliamentSuitGroupInfo.Name
          }
        }, true), parliamentSuitGroupInfo.AllSuitBenefit[index1], parliamentSuitGroupInfo.AllSuitBenefitValue[index1]);
        if (num1 == parliamentSuitGroupInfo.AllSuitRequirement[index1])
          heroBookBenefitItem.SetTextColor(this._benefitActiveColor);
      }
      if ((UnityEngine.Object) heroBookBenefitItem != (UnityEngine.Object) null)
        heroBookBenefitItem.SetLineEnabled(false);
      this._heroSuitName.text = ScriptLocalization.GetWithPara("hero_appoint_group_benefits_name", new Dictionary<string, string>()
      {
        {
          "0",
          parliamentSuitGroupInfo.Name
        }
      }, true);
      this._heroSuitBenefitTable.Reposition();
    }
    else
    {
      NGUITools.SetActive(this._heroSuitNode, false);
      NGUITools.SetActive(this._heroSuitFrameNode, false);
    }
  }

  private void ClearBenefitData()
  {
    for (int index = 0; index < this._itemBenefitList.Count; ++index)
    {
      GameObject gameObject = this._itemBenefitList[index].gameObject;
      gameObject.SetActive(false);
      this._itemPool.Release(gameObject);
    }
    this._itemBenefitList.Clear();
    this._itemPool.Clear();
  }

  private void UpdateAssignButtonStatus()
  {
    this._assignButtonText.text = Utils.XLAT("hero_council_position_appoint");
    if (this._legendCardData == null)
      return;
    NGUITools.SetActive(this._assignButton.gameObject, !this._legendCardData.PositionAssigned);
    NGUITools.SetActive(this._removeButton.gameObject, this._legendCardData.PositionAssigned);
    if (this._legendCardData.PositionAssigned)
      return;
    this._assignButtonText.text = !this.HasPositionAssigned(this._legendCardData.OriginPosition) ? Utils.XLAT("hero_council_position_appoint") : Utils.XLAT("hero_council_position_replace");
  }

  private bool HasPositionAssigned(int position)
  {
    List<LegendCardData> cardDataListByUid = DBManager.inst.DB_LegendCard.GetLegendCardDataListByUid(PlayerData.inst.uid);
    if (cardDataListByUid != null)
      return cardDataListByUid.Find((Predicate<LegendCardData>) (x =>
      {
        if (x.OriginPosition == position)
          return x.PositionAssigned;
        return false;
      })) != null;
    return false;
  }

  private HeroBookBenefitItem CreateBenefitItem(Transform parent, string prefix, int benefitId, float value)
  {
    this._itemPool.Initialize(this._benefitItemPrefab.gameObject, parent.gameObject);
    GameObject gameObject = this._itemPool.AddChild(parent.gameObject);
    gameObject.SetActive(true);
    HeroBookBenefitItem component = gameObject.GetComponent<HeroBookBenefitItem>();
    this._itemBenefitList.Add(component);
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitId];
    string str1 = dbProperty == null ? "unknow" : dbProperty.Name;
    string str2 = dbProperty == null ? value.ToString() : dbProperty.ConvertToDisplayString((double) value, false, true);
    if (string.IsNullOrEmpty(prefix))
      component.SetData(string.Format("{0} {1}", (object) str1, (object) str2));
    else
      component.SetData(string.Format("{0} {1} {2}", (object) prefix, (object) str1, (object) str2));
    return component;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int heroPosition;
  }
}
