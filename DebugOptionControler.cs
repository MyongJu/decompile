﻿// Decompiled with JetBrains decompiler
// Type: DebugOptionControler
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugOptionControler
{
  private const int MarchView3D_Enable_DFV = 1;

  public static bool MarchView3D_Enable
  {
    get
    {
      return PlayerPrefs.GetInt("march_view_3d", 1) == 1;
    }
    set
    {
      PlayerPrefs.SetInt("march_view_3d", !value ? 0 : 1);
    }
  }

  public struct Params
  {
    public const string DEBUG_OPTION_3D_MARCH_VIEW = "march_view_3d";
  }
}
