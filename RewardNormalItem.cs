﻿// Decompiled with JetBrains decompiler
// Type: RewardNormalItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RewardNormalItem : MonoBehaviour
{
  public UILabel rewardName;
  public UITexture itemIcon;
  public UI2DSprite ui2dSprite;
  public UISprite resourceIcon;
  public RewardNormalItem.DisplayTypeEum DisplayType;
  public RewardNormalItem.RewardType rewardType;
  public bool autoLoad;

  private void Start()
  {
    if (!this.autoLoad)
      return;
    this.AutoLoad();
  }

  private void AutoLoad()
  {
    string imagePath = this.GetImagePath(this.rewardType, 0);
    UIWidget uiWidget = this.GetUIWidget();
    if (uiWidget is UISprite)
    {
      this.resourceIcon.spriteName = imagePath;
    }
    else
    {
      if (!((UnityEngine.Object) uiWidget != (UnityEngine.Object) null))
        return;
      BuilderFactory.Instance.HandyBuild(uiWidget, imagePath, (System.Action<bool>) null, true, false, string.Empty);
    }
  }

  public void SetDisplayType(RewardNormalItem.DisplayTypeEum DisplayType)
  {
    this.DisplayType = DisplayType;
  }

  public void SetData(long count, RewardNormalItem.RewardType type = RewardNormalItem.RewardType.none)
  {
    this.rewardName.text = count.ToString();
    this.SetDisplaytype(type);
    this.HideOther();
    if (this.autoLoad)
      return;
    this.AutoLoad();
  }

  public void SetData(RewardNormalItem.RewardType type, string name, string path = "")
  {
    this.SetDisplaytype(type);
    this.rewardName.text = name;
    this.HideOther();
    UIWidget uiWidget = this.GetUIWidget();
    if (string.IsNullOrEmpty(path))
      path = this.GetImagePath(type, 0);
    if (uiWidget is UISprite)
    {
      this.resourceIcon.spriteName = path;
    }
    else
    {
      if (!((UnityEngine.Object) uiWidget != (UnityEngine.Object) null))
        return;
      BuilderFactory.Instance.HandyBuild(uiWidget, path, (System.Action<bool>) null, true, false, string.Empty);
    }
  }

  public void SetData(RewardNormalItem.RewardType type, int count, int item_id = 0)
  {
    this.SetDisplaytype(type);
    this.HideOther();
    if (count <= 0)
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(item_id);
      if (itemStaticInfo != null)
        this.rewardName.text = itemStaticInfo.LocName;
    }
    else
      this.rewardName.text = item_id <= 0 ? string.Empty + Utils.FormatThousands(count.ToString()) : "X";
    string imagePath = this.GetImagePath(type, item_id);
    UIWidget uiWidget = this.GetUIWidget();
    if (uiWidget is UISprite)
    {
      this.resourceIcon.spriteName = imagePath;
    }
    else
    {
      if (!((UnityEngine.Object) uiWidget != (UnityEngine.Object) null))
        return;
      BuilderFactory.Instance.HandyBuild(uiWidget, imagePath, (System.Action<bool>) null, true, false, string.Empty);
    }
  }

  private void SetDisplaytype(RewardNormalItem.RewardType type)
  {
    if (this.DisplayType != RewardNormalItem.DisplayTypeEum.None)
      return;
    switch (type)
    {
      case RewardNormalItem.RewardType.food:
      case RewardNormalItem.RewardType.wood:
      case RewardNormalItem.RewardType.ore:
      case RewardNormalItem.RewardType.silver:
      case RewardNormalItem.RewardType.gold:
      case RewardNormalItem.RewardType.lord_exp:
        this.DisplayType = RewardNormalItem.DisplayTypeEum.UISprite;
        break;
      case RewardNormalItem.RewardType.dragon_exp:
      case RewardNormalItem.RewardType.alliance_fund:
      case RewardNormalItem.RewardType.alliance_exp:
      case RewardNormalItem.RewardType.honor:
        this.DisplayType = RewardNormalItem.DisplayTypeEum.UI2DSprite;
        break;
      default:
        this.DisplayType = RewardNormalItem.DisplayTypeEum.UITexture;
        break;
    }
  }

  public void Clear()
  {
    if ((UnityEngine.Object) this.ui2dSprite != (UnityEngine.Object) null)
      BuilderFactory.Instance.Release((UIWidget) this.ui2dSprite);
    if (!((UnityEngine.Object) this.itemIcon != (UnityEngine.Object) null))
      return;
    BuilderFactory.Instance.Release((UIWidget) this.itemIcon);
  }

  private string GetImagePath(RewardNormalItem.RewardType type, int item_id = 0)
  {
    string str = string.Empty;
    if (item_id > 0)
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(item_id);
      if (itemStaticInfo != null)
        return itemStaticInfo.ImagePath;
    }
    switch (type)
    {
      case RewardNormalItem.RewardType.food:
        str = "food_icon";
        break;
      case RewardNormalItem.RewardType.wood:
        str = "wood_icon";
        break;
      case RewardNormalItem.RewardType.ore:
        str = "ore_icon";
        break;
      case RewardNormalItem.RewardType.silver:
        str = "silver_icon";
        break;
      case RewardNormalItem.RewardType.gold:
        str = "icon_currency_premium";
        break;
      case RewardNormalItem.RewardType.lord_exp:
        str = "xp_icon";
        break;
      case RewardNormalItem.RewardType.alliance_fund:
        str = "Texture/Alliance/alliance_fund";
        break;
      case RewardNormalItem.RewardType.alliance_exp:
        str = "Texture/Alliance/alliance_exp";
        break;
      case RewardNormalItem.RewardType.alliance_donation:
        str = "Texture/Alliance/alliance_donate";
        break;
      case RewardNormalItem.RewardType.honor:
        str = "Texture/Alliance/alliance_honor";
        break;
    }
    return str;
  }

  private UIWidget GetUIWidget()
  {
    UIWidget uiWidget = (UIWidget) null;
    switch (this.DisplayType)
    {
      case RewardNormalItem.DisplayTypeEum.UISprite:
        uiWidget = (UIWidget) this.resourceIcon;
        break;
      case RewardNormalItem.DisplayTypeEum.UI2DSprite:
        uiWidget = (UIWidget) this.ui2dSprite;
        break;
      case RewardNormalItem.DisplayTypeEum.UITexture:
        uiWidget = (UIWidget) this.itemIcon;
        break;
    }
    return uiWidget;
  }

  private void HideOther()
  {
    switch (this.DisplayType)
    {
      case RewardNormalItem.DisplayTypeEum.UISprite:
        this.SetGO((UIWidget) this.ui2dSprite, false);
        this.SetGO((UIWidget) this.itemIcon, false);
        this.SetGO((UIWidget) this.resourceIcon, true);
        break;
      case RewardNormalItem.DisplayTypeEum.UI2DSprite:
        this.SetGO((UIWidget) this.ui2dSprite, true);
        this.SetGO((UIWidget) this.itemIcon, false);
        this.SetGO((UIWidget) this.resourceIcon, false);
        break;
      case RewardNormalItem.DisplayTypeEum.UITexture:
        this.SetGO((UIWidget) this.ui2dSprite, false);
        this.SetGO((UIWidget) this.itemIcon, true);
        this.SetGO((UIWidget) this.resourceIcon, false);
        break;
    }
  }

  private void SetGO(UIWidget widget, bool visable = false)
  {
    if (!((UnityEngine.Object) widget != (UnityEngine.Object) null))
      return;
    NGUITools.SetActive(widget.gameObject, visable);
  }

  public enum DisplayTypeEum
  {
    None,
    UISprite,
    UI2DSprite,
    UITexture,
  }

  public enum RewardType
  {
    none,
    food,
    wood,
    ore,
    silver,
    gold,
    lord_exp,
    dragon_exp,
    alliance_fund,
    alliance_exp,
    alliance_donation,
    honor,
    item,
  }
}
