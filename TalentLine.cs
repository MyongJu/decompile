﻿// Decompiled with JetBrains decompiler
// Type: TalentLine
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TalentLine : MonoBehaviour
{
  private Vector3 activeLineScale = new Vector3(50f, 0.0f, 0.0f);
  private Vector3 activeLinePosition = Vector3.zero;
  public Transform line;
  public UISprite lineSprite;
  public Transform lineActive;
  public bool active;

  public void SetPosition(int startX, int startY, int endX, int endY, bool isActive = false)
  {
    this.line.gameObject.SetActive(true);
    this.lineActive.gameObject.SetActive(false);
    Vector3 vector3_1;
    vector3_1.x = (float) (0.5 * (double) (endX + startX) * 230.0 + 400.0);
    vector3_1.y = (float) ((0.5 * (double) (endY + startY) - 2.0) * 180.0);
    vector3_1.z = 0.0f;
    Vector3 vector3_2 = new Vector3();
    vector3_2.x = (float) ((endX - startX) * 230);
    vector3_2.y = (float) ((endY - startY) * 180);
    this.transform.localPosition = vector3_1;
    this.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, Mathf.Atan2(vector3_2.y, vector3_2.x) * 57.29578f);
    // ISSUE: explicit reference operation
    // ISSUE: variable of a reference type
    Vector3& local1 = @this.activeLinePosition;
    // ISSUE: explicit reference operation
    // ISSUE: variable of a reference type
    Vector3& local2 = @this.activeLineScale;
    int magnitude = (int) vector3_2.magnitude;
    this.lineSprite.width = magnitude;
    double num1;
    float num2 = (float) (num1 = (double) magnitude);
    // ISSUE: explicit reference operation
    (^local2).y = (float) num1;
    double num3 = (double) num2 / 2.0;
    // ISSUE: explicit reference operation
    (^local1).x = (float) num3;
    this.lineActive.gameObject.SetActive(isActive);
    if (!isActive)
      return;
    this.lineActive.localPosition = this.activeLinePosition;
    this.lineActive.localScale = this.activeLineScale;
  }
}
