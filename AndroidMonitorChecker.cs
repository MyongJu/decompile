﻿// Decompiled with JetBrains decompiler
// Type: AndroidMonitorChecker
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UI;
using UnityEngine;

public class AndroidMonitorChecker : MonoBehaviour
{
  private Hashtable _allCpuDeviceInfo = new Hashtable();
  private List<string> _AllSpecialFile;

  public static AndroidMonitorChecker CreateChecker()
  {
    GameObject gameObject = new GameObject(nameof (AndroidMonitorChecker));
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
    return gameObject.AddComponent<AndroidMonitorChecker>();
  }

  private void Start()
  {
    try
    {
      Logger.Log("check_client_request");
      RequestManager.inst.SendLoader("Player:checkClient", this.GetCheckReport(), (System.Action<bool, object>) ((result, data) =>
      {
        Logger.Log("check_client_result");
        if (!(bool) ((UnityEngine.Object) this))
        {
          Logger.Log("check_client_skipped");
        }
        else
        {
          if (result)
          {
            Hashtable hashtable = data as Hashtable;
            if (hashtable != null && hashtable[(object) "check_result"].ToString() == "0")
            {
              System.Action action = (System.Action) (() =>
              {
                Debug.Log((object) "QUIT GAME");
                Application.Quit();
              });
              UIManager.inst.ShowNetErrorBox(ScriptLocalization.Get("account_abnormal_login_title", true), ScriptLocalization.Get("account_abnormal_login_emulator_description", true), ScriptLocalization.Get("id_uppercase_okay", true), string.Empty, NetErrorBlocker.ButtonState.OK_CENTER, action, action, action);
            }
          }
          if (!(bool) ((UnityEngine.Object) this.gameObject))
            return;
          Logger.Log("check_client_destory_gameobject");
          UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
        }
      }), true, false);
    }
    catch (Exception ex)
    {
      Logger.Error((object) ex.Message);
    }
  }

  private void CollectAllCpuInfo()
  {
    this._allCpuDeviceInfo.Clear();
    try
    {
      if (!File.Exists("/proc/cpuinfo"))
        return;
      foreach (string readAllLine in File.ReadAllLines("/proc/cpuinfo"))
      {
        char[] chArray = new char[1]{ ':' };
        string[] strArray = readAllLine.Split(chArray);
        if (strArray.Length >= 2)
        {
          string str1 = strArray[0].Trim();
          string str2 = strArray[1].Trim();
          if (!this._allCpuDeviceInfo.ContainsKey((object) str1))
            this._allCpuDeviceInfo.Add((object) str1, (object) str2);
        }
      }
    }
    catch (Exception ex)
    {
      Debug.LogError((object) string.Format("GetCpuInfo Error: {0}", !string.IsNullOrEmpty(ex.Message) ? (object) ex.Message : (object) "unknow"));
    }
  }

  private string BuildModel
  {
    get
    {
      return new AndroidJavaClass("android.os.Build").GetStatic<string>("MODEL");
    }
  }

  private string BuildDevice
  {
    get
    {
      return new AndroidJavaClass("android.os.Build").GetStatic<string>("DEVICE");
    }
  }

  private List<string> AllSpecialFile
  {
    get
    {
      if (this._AllSpecialFile == null)
      {
        this._AllSpecialFile = new List<string>();
        SimulatorForbiddenInfo simulatorForbiddenInfo = ConfigManager.inst.DB_SimulatorForbidden.Get("file");
        if (simulatorForbiddenInfo != null)
          this._AllSpecialFile.AddRange((IEnumerable<string>) simulatorForbiddenInfo.value.Split(new char[1]
          {
            '|'
          }, StringSplitOptions.RemoveEmptyEntries));
      }
      return this._AllSpecialFile;
    }
  }

  private bool SupportBlueTooth()
  {
    return File.Exists("/system/lib/libbluetooth_jni.so");
  }

  private Hashtable GetCheckReport()
  {
    this.CollectAllCpuInfo();
    Hashtable hashtable = new Hashtable();
    ArrayList arrayList = new ArrayList();
    using (List<string>.Enumerator enumerator = this.AllSpecialFile.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        if (File.Exists(current) || Directory.Exists(current))
          arrayList.Add((object) current);
      }
    }
    hashtable.Add((object) "files", (object) arrayList);
    hashtable.Add((object) "bluetoothSupport", (object) (!this.SupportBlueTooth() ? 0 : 1));
    hashtable.Add((object) "Cpu", (object) this._allCpuDeviceInfo);
    hashtable.Add((object) "android_os_build_model", (object) this.BuildModel);
    hashtable.Add((object) "android_os_build_device", (object) this.BuildDevice);
    return hashtable;
  }
}
