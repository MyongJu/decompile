﻿// Decompiled with JetBrains decompiler
// Type: MailEntryView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using NLPTrans;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MailEntryView : MonoBehaviour
{
  private bool displayOriginalContent = true;
  private string machineTargetText = string.Empty;
  private const string POPUP_PATH = "Mail/MailContactFunctionPopUp";
  public UILabel contentLabel;
  public UIButton portraitBtn;
  public UITexture icon;
  public GameObject frame;
  public UILabel translateTip;
  public GameObject coordinateTemple;
  public System.Action onUIChanged;
  private PlayerMessageEntry currentEntry;
  private string originContent;
  private bool useModPort;

  public bool UseModPortrait
  {
    set
    {
      this.useModPort = value;
    }
  }

  public void UpdateUI(PlayerMessageEntry entry)
  {
    this.currentEntry = entry;
    this.originContent = this.currentEntry.content;
    this.contentLabel.text = !this.currentEntry.sendToAll ? IllegalWordsUtils.Filter(this.originContent) : string.Format("{0}{1}", (object) Utils.XLAT("mail_message_type_sent_to_all"), (object) IllegalWordsUtils.Filter(this.originContent));
    if ((UnityEngine.Object) this.portraitBtn != (UnityEngine.Object) null)
      this.portraitBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnPortraitClick)));
    string str = this.currentEntry.fromUID != PlayerData.inst.uid ? this.currentEntry.fromPortrait : PlayerData.inst.userData.portrait.ToString();
    if (this.useModPort)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, "Texture/Hero/Portrait_Icon/head_Mod", (System.Action<bool>) null, true, false, string.Empty);
      NGUITools.SetActive(this.frame, false);
    }
    else
    {
      NGUITools.SetActive(this.frame, true);
      if (this.currentEntry.fromUID == PlayerData.inst.uid)
      {
        CustomIconLoader.Instance.requestCustomIcon(this.icon, "Texture/Hero/Portrait_Icon/player_portrait_icon_" + str, PlayerData.inst.userData.Icon, false);
        LordTitlePayload.Instance.ApplyUserAvator(this.icon, PlayerData.inst.userData.LordTitle, 5);
      }
      else
      {
        CustomIconLoader.Instance.requestCustomIcon(this.icon, "Texture/Hero/Portrait_Icon/player_portrait_icon_" + this.currentEntry.fromPortrait, this.currentEntry.fromIcon, false);
        LordTitlePayload.Instance.ApplyUserAvator(this.icon, this.currentEntry.fromLordTitleId, 5);
      }
    }
    if ((UnityEngine.Object) this.translateTip != (UnityEngine.Object) null)
      this.translateTip.text = Utils.XLAT("language_status_original");
    UILabelPostProcess.ProcessCoordinate(this.contentLabel, this.coordinateTemple);
    EmojiManager.Instance.ProcessEmojiLabel(this.contentLabel);
  }

  public void OnTranslateClick()
  {
    if (this.displayOriginalContent)
    {
      NLPUtils.Instance.TranslateAsyc(this.currentEntry.content, Language.TranslateTarget, "en", this.currentEntry.fromUID.ToString(), "mail", (System.Action<bool, string, string>) ((arg1, arg2, arg3) =>
      {
        if (!arg1)
          return;
        this.machineTargetText = arg2;
        this.contentLabel.text = (!this.currentEntry.sendToAll ? string.Empty : Utils.XLAT("mail_message_type_sent_to_all")) + ScriptLocalization.Get(arg2, true);
        this.translateTip.text = ScriptLocalization.GetWithMultyContents("chat_original_lang_description", true, Utils.XLAT("translated_lang_" + arg3));
        this.displayOriginalContent = false;
        UILabelPostProcess.ProcessCoordinate(this.contentLabel, this.coordinateTemple);
        EmojiManager.Instance.ProcessEmojiLabel(this.contentLabel);
      }));
    }
    else
    {
      this.displayOriginalContent = true;
      this.contentLabel.text = (!this.currentEntry.sendToAll ? string.Empty : Utils.XLAT("mail_message_type_sent_to_all")) + ScriptLocalization.Get(this.currentEntry.content, true);
      this.translateTip.text = Utils.XLAT("language_status_original");
      UILabelPostProcess.ProcessCoordinate(this.contentLabel, this.coordinateTemple);
      EmojiManager.Instance.ProcessEmojiLabel(this.contentLabel);
    }
    if (this.onUIChanged == null)
      return;
    this.onUIChanged();
  }

  public void OnContentLabelClick()
  {
    UIManager.inst.OpenPopup("ClipboardPopup", (Popup.PopupParameter) new ClipboardPopup.Parameter()
    {
      str = this.currentEntry.content,
      uid = this.currentEntry.fromUID,
      reportAble = false,
      sourceText = this.currentEntry.content,
      machineTargetText = this.machineTargetText,
      sourceLanguage = "en",
      targetLanguage = Language.TranslateTarget
    });
  }

  private void OnPortraitClick()
  {
    MailContactFunctionPopUp contactFunctionPopUp = PopupManager.Instance.Open<MailContactFunctionPopUp>("Mail/MailContactFunctionPopUp");
    contactFunctionPopUp.Initailize(new EventDelegate(new EventDelegate.Callback(this.OnProfileClick)), new EventDelegate(new EventDelegate.Callback(this.OnAddContactClick)), new EventDelegate(new EventDelegate.Callback(this.OnBlockClick)));
    contactFunctionPopUp.AddBanSpeakListener(new EventDelegate(new EventDelegate.Callback(this.OnBanClick)));
    contactFunctionPopUp.AddModMailListener(new EventDelegate(new EventDelegate.Callback(this.OnModMailClick)));
  }

  private void OnProfileClick()
  {
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
    {
      uid = this.currentEntry.fromUID
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private void OnAddContactClick()
  {
    ContactManager.AddContact(this.currentEntry.fromUID, (System.Action<bool, object>) null);
  }

  private void OnBlockClick()
  {
    ContactManager.BlockUser(this.currentEntry.fromUID, (System.Action<bool, object>) null);
  }

  private void OnBanClick()
  {
    HubPort portByAction = MessageHub.inst.GetPortByAction("Player:loadUserBasicInfo");
    Hashtable postData = new Hashtable();
    postData[(object) "target_uid"] = (object) this.currentEntry.fromUID;
    portByAction.SendRequest(postData, (System.Action<bool, object>) ((success, data) =>
    {
      if (!success)
        return;
      ChatMutePopup.Paramer paramer = new ChatMutePopup.Paramer();
      paramer.personalName = string.Empty;
      paramer.personalUid = this.currentEntry.fromUID;
      paramer.senderCustomIcon = this.currentEntry.fromIcon;
      paramer.personLordTitleId = this.currentEntry.fromLordTitleId;
      int.TryParse(this.currentEntry.fromPortrait, out paramer.personIcon);
      UIManager.inst.OpenPopup("Chat/ChatMutePopup", (Popup.PopupParameter) paramer);
    }), true);
  }

  private void OnModMailClick()
  {
    UserData userData = DBManager.inst.DB_User.Get(this.currentEntry.fromUID);
    if (userData != null)
    {
      PlayerData.inst.mail.SendModMail(this.currentEntry.fromUID, new List<string>()
      {
        userData.userName
      });
    }
    else
    {
      HubPort portByAction = MessageHub.inst.GetPortByAction("Player:loadUserBasicInfo");
      Hashtable postData = new Hashtable();
      postData[(object) "target_uid"] = (object) this.currentEntry.fromUID;
      portByAction.SendRequest(postData, (System.Action<bool, object>) ((success, data) =>
      {
        if (!success)
          return;
        userData = DBManager.inst.DB_User.Get(this.currentEntry.fromUID);
        if (userData == null)
          return;
        PlayerData.inst.mail.SendModMail(this.currentEntry.fromUID, new List<string>()
        {
          userData.userName
        });
      }), true);
    }
  }

  private void LoadUserBasicInfo(long uid)
  {
    if (DBManager.inst.DB_User.Get(uid) == null)
    {
      HubPort portByAction = MessageHub.inst.GetPortByAction("Player:loadUserBasicInfo");
      Hashtable postData = new Hashtable();
      postData[(object) "target_uid"] = (object) uid;
      portByAction.SendLoader(postData, new System.Action<bool, object>(this.OnBasicInfoLoaded), true, false);
    }
    else
      this.OnBasicInfoLoaded(true, (object) null);
  }

  private void OnBasicInfoLoaded(bool result, object args)
  {
    UserData userData = DBManager.inst.DB_User.Get(this.currentEntry.fromUID);
    if (userData.portrait < 0)
      return;
    CustomIconLoader.Instance.requestCustomIcon(this.icon, userData.PortraitIconPath, userData.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.icon, userData.LordTitle, 1);
  }
}
