﻿// Decompiled with JetBrains decompiler
// Type: ResearchBaseSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ResearchBaseSlot : MonoBehaviour
{
  public int tree;
  public GameObject researchingEffect;

  public void UpdateEffect(int researchingTree)
  {
    if (researchingTree == this.tree)
      this.researchingEffect.SetActive(true);
    else
      this.researchingEffect.SetActive(false);
  }
}
