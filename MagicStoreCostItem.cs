﻿// Decompiled with JetBrains decompiler
// Type: MagicStoreCostItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MagicStoreCostItem : MonoBehaviour
{
  public UITexture background;
  public UITexture icon;
  public UILabel count;
  private int _itemId;

  private void SetUI(string iconPath, int itemCount, int quality = 0)
  {
    BuilderFactory.Instance.Build((UIWidget) this.icon, iconPath, (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.background, Utils.GetQualityImagePath(quality), (System.Action<bool>) null, true, false, true, string.Empty);
    this.count.text = itemCount.ToString();
  }

  public void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
    BuilderFactory.Instance.Release((UIWidget) this.background);
  }

  public void SetData(int itemId)
  {
    if (this._itemId == itemId)
      return;
    this._itemId = itemId;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo == null)
      return;
    int itemCount = ItemBag.Instance.GetItemCount(this._itemId);
    this.SetUI(itemStaticInfo.ImagePath, itemCount, itemStaticInfo.Quality);
  }

  public void Refresh()
  {
    this.count.text = ItemBag.Instance.GetItemCount(this._itemId).ToString();
  }
}
