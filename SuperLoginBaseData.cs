﻿// Decompiled with JetBrains decompiler
// Type: SuperLoginBaseData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class SuperLoginBaseData
{
  protected Hashtable _userSuperLoginData = new Hashtable();
  protected Dictionary<int, int> _rewardStateDict = new Dictionary<int, int>();
  protected int _startTime;
  protected int _endTime;

  public virtual int StartTime
  {
    get
    {
      return this._startTime;
    }
  }

  public virtual int EndTime
  {
    get
    {
      return this._endTime;
    }
  }

  public virtual Hashtable UserSuperLoginData
  {
    get
    {
      return this._userSuperLoginData;
    }
  }

  public virtual Dictionary<int, int> RewardStateDict
  {
    get
    {
      return this._rewardStateDict;
    }
  }

  public virtual bool Decode(Hashtable data)
  {
    bool flag1 = false;
    if (data == null)
      return false;
    bool flag2 = flag1 | DatabaseTools.UpdateData(data, "start_time", ref this._startTime) | DatabaseTools.UpdateData(data, "end_time", ref this._endTime);
    if (data[(object) "user_data"] != null)
      flag2 |= DatabaseTools.CheckAndParseOrgData((object) (data[(object) "user_data"] as Hashtable), out this._userSuperLoginData);
    if (this._rewardStateDict != null)
    {
      this._rewardStateDict.Clear();
      if (this._userSuperLoginData != null)
      {
        IDictionaryEnumerator enumerator = this._userSuperLoginData.GetEnumerator();
        while (enumerator.MoveNext())
        {
          int result1 = -1;
          int result2 = -1;
          int.TryParse(enumerator.Key.ToString(), out result1);
          int.TryParse(enumerator.Value.ToString(), out result2);
          if (!this._rewardStateDict.ContainsKey(result1))
            this._rewardStateDict.Add(result1, result2);
        }
      }
    }
    return flag2;
  }

  public struct Params
  {
    public const string START_TIME = "start_time";
    public const string END_TIME = "end_time";
    public const string USER_DATA = "user_data";
  }

  public struct SuperLoginGroup
  {
    public const string CHRISTMAS_EVE = "super_log_in_christmas";
    public const string FIRST_ANNIVERSARY = "super_log_in_1_year_celebration";
  }
}
