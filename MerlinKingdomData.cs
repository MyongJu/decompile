﻿// Decompiled with JetBrains decompiler
// Type: MerlinKingdomData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class MerlinKingdomData
{
  public List<MerlinMineData> mine_list = new List<MerlinMineData>();
  public int max_page;
  public int exit_time;
  public int page;
  public int show_index;

  public int LeftTime
  {
    get
    {
      return Mathf.Max(0, this.exit_time - NetServerTime.inst.ServerTimestamp);
    }
  }
}
