﻿// Decompiled with JetBrains decompiler
// Type: RebateHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class RebateHUD : MonoBehaviour
{
  private string vfxName = string.Empty;
  private const string vfxPath = "Prefab/VFX/";
  private const string vfxUsedName = "fx_store_fanli";
  public GameObject rootNode;
  public GameObject tipInfoNode;
  public UILabel tipInfoCount;
  public UILabel timeText;
  private bool start;
  private GameObject vfxObj;

  private void Start()
  {
    this.start = true;
    this.UpdateUI();
  }

  private void OnEnable()
  {
    if (this.start)
      this.UpdateUI();
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void OnDisable()
  {
    if (!GameEngine.IsAvailable || !GameEngine.IsReady() || GameEngine.IsShuttingDown)
      ;
  }

  private void OnSecondEvent(int time)
  {
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (RebateByRechargePayload.Instance.CanShowRebateHUD)
    {
      NGUITools.SetActive(this.rootNode, true);
      this.SetVFX();
    }
    else
    {
      if (this.rootNode.activeInHierarchy)
        NGUITools.SetActive(this.rootNode, false);
      this.DeleteVFX();
    }
    if (RebateByRechargePayload.Instance.CanCollectTotalCount > 0)
    {
      NGUITools.SetActive(this.tipInfoNode, true);
      this.tipInfoCount.text = RebateByRechargePayload.Instance.CanCollectTotalCount.ToString();
    }
    else
    {
      NGUITools.SetActive(this.tipInfoNode, false);
      this.tipInfoCount.text = string.Empty;
    }
    this.timeText.text = Utils.FormatTime(RebateByRechargePayload.Instance.EndTime - NetServerTime.inst.ServerTimestamp, true, false, true);
  }

  public void OnRebateBtnPressed()
  {
    UIManager.inst.OpenDlg("Rebate/RebateByRechargeDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void SetVFX()
  {
    if (!(this.vfxName != "fx_store_fanli"))
      return;
    this.DeleteVFX();
    this.vfxName = "fx_store_fanli";
    this.vfxObj = AssetManager.Instance.HandyLoad("Prefab/VFX/" + this.vfxName, typeof (GameObject)) as GameObject;
    if (!((UnityEngine.Object) this.vfxObj != (UnityEngine.Object) null))
      return;
    this.vfxObj = UnityEngine.Object.Instantiate<GameObject>(this.vfxObj);
    this.vfxObj.transform.parent = this.rootNode.transform;
    this.vfxObj.transform.localPosition = Vector3.zero;
    this.vfxObj.transform.localScale = Vector3.one;
  }

  private void DeleteVFX()
  {
    this.vfxName = string.Empty;
    if (!((UnityEngine.Object) this.vfxObj != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.vfxObj);
    this.vfxObj = (GameObject) null;
  }
}
