﻿// Decompiled with JetBrains decompiler
// Type: AllianceRallyJoinedSlotItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceRallyJoinedSlotItem : MonoBehaviour
{
  private List<AllianceRallyTroopItem> troopItems = new List<AllianceRallyTroopItem>();
  private bool fistTime = true;
  public bool isLock = true;
  public UITexture playerIcon;
  public UILabel playerName;
  public UILabel playerLevel;
  public UILabel troopsCount;
  public UIProgressBar progressBar;
  public UILabel progressLabel;
  public UIButton speedUpBt;
  public UILabel tip;
  public UISprite dragonIcon;
  public GameObject OpenContent;
  public GameObject dragonContent;
  public UISprite openBg;
  public UISprite normalBg;
  public DragonUIInfo dragonInfo;
  public UILabel noDragonLabel;
  public MarchTroopsUIInfo marchInfo;
  public UITable childContainer;
  public UIButton disband;
  public ParliamentHeroItemExtList heroList;
  private long march_id;
  private bool isOpen;
  public System.Action OnOpenCallBackDelegate;

  public RallyInfoData Data { get; set; }

  public void SetData(long march_id, RallyInfoData data)
  {
    this.march_id = march_id;
    this.Data = data;
    MarchData marchData = DBManager.inst.DB_March.Get(march_id);
    JobManager.Instance.GetJob(marchData.jobId);
    this.troopsCount.text = marchData.troopsInfo.totalCount.ToString();
    UserData userData = DBManager.inst.DB_User.Get(marchData.ownerUid);
    if (userData != null)
    {
      this.playerName.text = userData.userName;
      CustomIconLoader.Instance.requestCustomIcon(this.playerIcon, UserData.GetPortraitIconPath(userData.portrait), userData.Icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.playerIcon, userData.LordTitle, 2);
    }
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(marchData.ownerUid);
    if ((UnityEngine.Object) this.playerLevel != (UnityEngine.Object) null)
      this.playerLevel.text = heroData == null ? string.Empty : string.Empty + (object) heroData.level;
    this.RefreshUI();
    this.RefreshTroops();
    this.RefreshDragon();
    this.RefreshHeroCards();
    NGUITools.SetActive(this.disband.gameObject, false);
    this.childContainer.repositionNow = true;
    this.childContainer.Reposition();
    NGUITools.SetActive(this.speedUpBt.gameObject, false);
    if (marchData.state != MarchData.MarchState.marching)
      return;
    if (!this.Data.IsDefense)
    {
      NGUITools.SetActive(this.speedUpBt.gameObject, this.Data.IsJoined);
    }
    else
    {
      if (marchData.ownerUid != PlayerData.inst.uid && this.Data.TargetID != PlayerData.inst.uid)
        return;
      NGUITools.SetActive(this.speedUpBt.gameObject, true);
    }
  }

  private void RefreshUI()
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.march_id);
    if (marchData == null)
      return;
    JobHandle job = JobManager.Instance.GetJob(marchData.jobId);
    NGUITools.SetActive(this.speedUpBt.gameObject, false);
    if (marchData.state == MarchData.MarchState.marching)
    {
      if (!this.Data.IsDefense)
        NGUITools.SetActive(this.speedUpBt.gameObject, this.Data.IsJoined);
      else if (marchData.ownerUid == PlayerData.inst.uid || this.Data.TargetID == PlayerData.inst.uid)
        NGUITools.SetActive(this.speedUpBt.gameObject, true);
      if (job == null)
        return;
      if (!job.IsFinished())
      {
        NGUITools.SetActive(this.progressBar.gameObject, true);
        NGUITools.SetActive(this.tip.gameObject, false);
        this.progressBar.value = 1f - (float) job.LeftTime() / (float) job.Duration();
        string empty = string.Empty;
        this.progressLabel.text = (this.Data.IsDefense ? ScriptLocalization.Get("id_reinforce_marching", true) : ScriptLocalization.Get("id_rally_marching", true)) + " " + Utils.FormatTime(job.LeftTime(), false, false, true);
      }
      else
      {
        NGUITools.SetActive(this.progressBar.gameObject, false);
        NGUITools.SetActive(this.tip.gameObject, true);
        this.tip.text = ScriptLocalization.Get("id_rally_arrived", true);
      }
    }
    else
    {
      NGUITools.SetActive(this.progressBar.gameObject, false);
      NGUITools.SetActive(this.tip.gameObject, true);
      if (!this.Data.IsDefense)
        this.tip.text = ScriptLocalization.Get("id_rally_arrived", true);
      else
        this.tip.text = ScriptLocalization.Get("war_rally_uppercase_reinforced", true);
    }
  }

  public void OnClickHandler()
  {
    if (this.march_id <= 0L)
    {
      RallyData rallyData = DBManager.inst.DB_Rally.Get(this.Data.DataID);
      if (this.isLock)
      {
        string content = ScriptLocalization.Get("toast_rally_slot_lock_tip", true);
        if (rallyData != null)
        {
          if (rallyData.type == RallyData.RallyType.TYPE_RALLY_GVE)
            content = ScriptLocalization.Get("toast_gve_rally_max_slots", true);
          else if (rallyData.type == RallyData.RallyType.TYPE_RALLY_RAB)
            content = ScriptLocalization.Get("toast_portal_monster_rally_max_slots", true);
        }
        UIManager.inst.toast.Show(content, (System.Action) null, 4f, false);
      }
      else
      {
        if (!rallyData.isJoined)
          return;
        UIManager.inst.toast.Show(ScriptLocalization.Get("toast_rally_already_joined_tip", true), (System.Action) null, 4f, false);
      }
    }
    else
    {
      this.isOpen = !this.isOpen;
      NGUITools.SetActive(this.OpenContent, this.isOpen);
      NGUITools.SetActive(this.normalBg.gameObject, false);
      MarchData marchData = DBManager.inst.DB_March.Get(this.march_id);
      NGUITools.SetActive(this.disband.transform.parent.gameObject, false);
      if (this.Data.IsDefense)
      {
        if (marchData.ownerUid == PlayerData.inst.userData.uid || this.Data.TargetID == PlayerData.inst.uid)
          NGUITools.SetActive(this.disband.transform.parent.gameObject, true);
      }
      else if (this.Data.IsCallBack)
        NGUITools.SetActive(this.disband.transform.parent.gameObject, false);
      else if (this.Data.IsMyRally && marchData.ownerUid != PlayerData.inst.uid)
        NGUITools.SetActive(this.disband.transform.parent.gameObject, true);
      Utils.ExecuteInSecs(0.01f, (System.Action) (() =>
      {
        if (!this.isOpen)
        {
          this.normalBg.height = (int) byte.MaxValue;
        }
        else
        {
          Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.transform, false);
          if (this.disband.transform.parent.gameObject.activeSelf)
          {
            if (this.fistTime)
            {
              this.fistTime = false;
              this.normalBg.height = (int) relativeWidgetBounds.size.y + 150;
            }
            else
              this.normalBg.height = (int) relativeWidgetBounds.size.y + 30;
          }
          else if (this.fistTime)
          {
            this.fistTime = false;
            this.normalBg.height = (int) relativeWidgetBounds.size.y;
          }
          else
            this.normalBg.height = (int) relativeWidgetBounds.size.y + 80;
        }
        NGUITools.SetActive(this.normalBg.gameObject, true);
        NGUITools.AddWidgetCollider(this.transform.parent.gameObject);
        this.childContainer.Reposition();
        if (this.OnOpenCallBackDelegate == null)
          return;
        this.OnOpenCallBackDelegate();
      }));
    }
  }

  private void RefreshHeroCards()
  {
    List<LegendCardData> cardDataListByUid = DBManager.inst.DB_LegendCard.GetLegendCardDataListByUid(DBManager.inst.DB_March.Get(this.march_id).ownerUid);
    List<LegendCardData> datas = new List<LegendCardData>();
    for (int index = 0; index < cardDataListByUid.Count; ++index)
    {
      if (ConfigManager.inst.DB_Parliament.Get(ConfigManager.inst.DB_ParliamentHero.Get(cardDataListByUid[index].LegendId).ParliamentPosition.ToString()).positionType == 2 && cardDataListByUid[index].PositionAssigned)
        datas.Add(cardDataListByUid[index]);
    }
    Vector3 localScale = this.heroList.transform.localScale;
    localScale.x = localScale.y = 0.7f;
    if (cardDataListByUid == null || cardDataListByUid.Count == 0)
    {
      this.heroList.gameObject.SetActive(false);
    }
    else
    {
      this.heroList.transform.localScale = localScale;
      this.heroList.gameObject.SetActive(true);
      this.heroList.SetData(datas);
    }
  }

  private void RefreshTroops()
  {
    this.marchInfo.SetData(this.march_id);
  }

  private void RefreshDragon()
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.march_id);
    string skillType = this.Data.Type == RallyInfoData.RallyDataType.GVE || this.Data.Type == RallyInfoData.RallyDataType.RAB ? "attack_monster" : "attack";
    NGUITools.SetActive(this.dragonInfo.gameObject, marchData.withDragon);
    NGUITools.SetActive(this.dragonIcon.gameObject, marchData.withDragon);
    NGUITools.SetActive(this.noDragonLabel.gameObject, !marchData.withDragon);
    if (!marchData.withDragon || !marchData.withDragon)
      return;
    this.dragonInfo.SetDragon(marchData.dragonId, skillType, (int) marchData.dragon.tendency, marchData.dragon.level);
  }

  public void RecallMarch()
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.march_id);
    if (this.Data.IsMyRally)
      UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
      {
        title = ScriptLocalization.Get("id_uppercase_confirm", true),
        description = ScriptLocalization.Get("war_rally_repatriate_confirmation", true),
        leftButtonClickEvent = (System.Action) null,
        rightButtonClickEvent = (System.Action) (() => RequestManager.inst.SendRequest("rally:repatriate", new Hashtable()
        {
          {
            (object) "uid",
            (object) PlayerData.inst.uid
          },
          {
            (object) "rally_id",
            (object) this.Data.DataID
          },
          {
            (object) "cancel_uid",
            (object) marchData.ownerUid
          }
        }, (System.Action<bool, object>) null, true))
      });
    else if (this.Data.TargetID == PlayerData.inst.uid)
    {
      if (marchData.state == MarchData.MarchState.marching && marchData.type == MarchData.MarchType.reinforce)
        UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
        {
          title = ScriptLocalization.Get("account_warning_title", true),
          description = ScriptLocalization.Get("send_troops_back_description", true),
          leftButtonClickEvent = (System.Action) null,
          rightButtonClickEvent = (System.Action) (() => MessageHub.inst.GetPortByAction("PVP:sendIncomingReinforceBack").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "march_id", (object) marchData.marchId, (object) "back_uid", (object) marchData.ownerUid), (System.Action<bool, object>) ((arg1, arg2) =>
          {
            if (!arg1)
              return;
            PlayerData.inst.allianceWarManager.RequestRefreshRallyDetail();
          }), true))
        });
      else
        GameEngine.Instance.marchSystem.SendTroopHome(marchData.ownerUid);
    }
    else
    {
      if (marchData.ownerUid != PlayerData.inst.uid)
        return;
      GameEngine.Instance.marchSystem.Recall(this.march_id);
    }
  }

  public void Refresh()
  {
    this.RefreshUI();
  }

  public void Speeduphandler()
  {
    UIManager.inst.OpenPopup("MarchSpeedUpPopup", (Popup.PopupParameter) new MarchSpeedUpPopup.Parameter()
    {
      marchData = DBManager.inst.DB_March.Get(this.march_id)
    });
  }

  public void Clear()
  {
    for (int index = 0; index < this.troopItems.Count; ++index)
    {
      AllianceRallyTroopItem troopItem = this.troopItems[index];
      troopItem.Clear();
      NGUITools.SetActive(troopItem.gameObject, false);
      UnityEngine.Object.Destroy((UnityEngine.Object) troopItem);
    }
    this.troopItems.Clear();
    this.OnOpenCallBackDelegate = (System.Action) null;
  }
}
