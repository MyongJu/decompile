﻿// Decompiled with JetBrains decompiler
// Type: OpeningRallySlotContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class OpeningRallySlotContainer : MonoBehaviour
{
  private List<OpeningRallySlotItem> items = new List<OpeningRallySlotItem>();
  public OpeningRallySlotItem itemPrefab;
  public UIScrollView scroll;
  public UITable tabel;

  private void Start()
  {
    for (int index = 0; index < 6; ++index)
    {
      OpeningRallySlotItem openingRallySlotItem = this.GetItem();
      this.items.Add(openingRallySlotItem);
      openingRallySlotItem.OnOpenCallBackDelegate = new System.Action(this.OnOpenHandler);
      openingRallySlotItem.SetData(index);
    }
    this.tabel.Reposition();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.scroll.ResetPosition()));
  }

  private void OnOpenHandler()
  {
    this.tabel.repositionNow = true;
    this.tabel.Reposition();
  }

  private OpeningRallySlotItem GetItem()
  {
    GameObject go = Utils.DuplicateGOB(this.itemPrefab.gameObject, this.tabel.transform);
    OpeningRallySlotItem component = go.GetComponent<OpeningRallySlotItem>();
    NGUITools.SetActive(go, true);
    return component;
  }

  public void Clear()
  {
    for (int index = 0; index < this.items.Count; ++index)
    {
      this.items[index].Clear();
      NGUITools.SetActive(this.items[index].gameObject, false);
      this.items[index].transform.parent = this.transform;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.items[index]);
    }
    this.items.Clear();
  }
}
