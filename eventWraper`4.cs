﻿// Decompiled with JetBrains decompiler
// Type: eventWraper`4
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class eventWraper<T1, T2, T3, T4>
{
  private System.Action<T1, T2, T3, T4> m_delegate;

  public void Invoke(T1 param1, T2 param2, T3 param3, T4 param4)
  {
    if (this.m_delegate == null)
      return;
    this.m_delegate(param1, param2, param3, param4);
  }

  public void AddListener(System.Action<T1, T2, T3, T4> listener)
  {
    this.m_delegate -= listener;
    this.m_delegate += listener;
  }

  public void RemoveListener(System.Action<T1, T2, T3, T4> listener)
  {
    this.m_delegate -= listener;
  }
}
