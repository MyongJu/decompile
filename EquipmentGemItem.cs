﻿// Decompiled with JetBrains decompiler
// Type: EquipmentGemItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentGemItem : MonoBehaviour
{
  private List<EquipmentGemSlotItem> _allEquipmentGemSlot = new List<EquipmentGemSlotItem>();
  [SerializeField]
  private EquipmentGemSlotItem _equipmentGemSlotTemplate;
  [SerializeField]
  private UITable _tableContainer;
  public UILabel title;

  public void SetData(BagType bagType, ConfigEquipmentScrollInfo scrollInfo, UIScrollView container = null)
  {
    this._equipmentGemSlotTemplate.gameObject.SetActive(false);
    int num = 0;
    this.SetTitle(bagType);
    if (scrollInfo != null)
    {
      ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(scrollInfo.equipmenID);
      if (data != null)
      {
        List<GemSlotConditions.Condition> conditionList = data.GemSlotConditions.ConditionList;
        for (int index = 0; index < conditionList.Count; ++index)
        {
          GemSlotConditions.Condition condition = conditionList[index];
          if (condition.requiredLv <= 0)
          {
            this.CreateEquipmentGemSlotItem().SetData(index + 1, condition.gemType, (InventoryItemData) null, false, container);
            ++num;
          }
          else
            break;
        }
      }
    }
    this._tableContainer.repositionNow = true;
    this._tableContainer.Reposition();
    if (num > 0)
      return;
    this.gameObject.SetActive(false);
  }

  private void SetTitle(BagType bagType)
  {
    string source = "forge_armory_gemstone_socket_subtitle";
    if (bagType == BagType.DragonKnight)
      source = "forge_armory_dk_runestone_socket_subtitle";
    this.title.text = Utils.XLAT(source);
  }

  public void SetData(BagType bagType, int equipmentInternalId, UIScrollView container = null)
  {
    this._equipmentGemSlotTemplate.gameObject.SetActive(false);
    this.SetTitle(bagType);
    ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(equipmentInternalId);
    int num = 0;
    if (data != null)
    {
      List<GemSlotConditions.Condition> conditionList = data.GemSlotConditions.ConditionList;
      for (int index = 0; index < conditionList.Count; ++index)
      {
        GemSlotConditions.Condition condition = conditionList[index];
        if (condition.requiredLv <= 0)
        {
          this.CreateEquipmentGemSlotItem().SetData(index + 1, condition.gemType, (InventoryItemData) null, false, container);
          ++num;
        }
        else
          break;
      }
    }
    this._tableContainer.repositionNow = true;
    this._tableContainer.Reposition();
    if (num > 0)
      return;
    this.gameObject.SetActive(false);
  }

  public void SetData(InventoryItemData inventoryData, bool interactable = false, UIScrollView container = null)
  {
    this._equipmentGemSlotTemplate.gameObject.SetActive(false);
    this.SetTitle(inventoryData.bagType);
    this.DestoryAllEquipmentGemSlotItem();
    InlaidGems inlaidGems = inventoryData.inlaidGems;
    ConfigEquipmentMainInfo equipment = inventoryData.equipment;
    int num = 0;
    if (equipment != null)
    {
      List<GemSlotConditions.Condition> conditionList = equipment.GemSlotConditions.ConditionList;
      for (int index = 0; index < conditionList.Count; ++index)
      {
        GemSlotConditions.Condition condition = conditionList[index];
        if (condition.requiredLv <= inventoryData.enhanced)
        {
          this.CreateEquipmentGemSlotItem().SetData(index + 1, condition.gemType, inventoryData, interactable, container);
          ++num;
        }
        else
          break;
      }
    }
    this._tableContainer.repositionNow = true;
    this._tableContainer.Reposition();
    if (num > 0)
      return;
    this.gameObject.SetActive(false);
  }

  protected void DestoryAllEquipmentGemSlotItem()
  {
    using (List<EquipmentGemSlotItem>.Enumerator enumerator = this._allEquipmentGemSlot.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EquipmentGemSlotItem current = enumerator.Current;
        if ((bool) ((Object) current))
          Object.Destroy((Object) current.gameObject);
      }
    }
    this._allEquipmentGemSlot.Clear();
  }

  protected EquipmentGemSlotItem CreateEquipmentGemSlotItem()
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this._equipmentGemSlotTemplate.gameObject);
    gameObject.transform.SetParent(this._tableContainer.transform);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    EquipmentGemSlotItem component = gameObject.GetComponent<EquipmentGemSlotItem>();
    this._allEquipmentGemSlot.Add(component);
    return component;
  }
}
