﻿// Decompiled with JetBrains decompiler
// Type: JobHandle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class JobHandle
{
  public Job weakPtr;

  public bool IsNull()
  {
    return this.weakPtr == null;
  }

  public System.Action OnFinished
  {
    get
    {
      if (!this.IsNull())
        return this.weakPtr.OnFinished;
      return (System.Action) null;
    }
    set
    {
      if (this.IsNull())
        return;
      this.weakPtr.OnFinished = value;
    }
  }

  public override string ToString()
  {
    if (this.weakPtr == null)
      return "<NULL>";
    return this.weakPtr.ToString();
  }

  public object Data
  {
    get
    {
      if (!this.IsNull())
        return this.weakPtr.Data;
      return (object) null;
    }
    set
    {
      if (this.IsNull())
        return;
      this.weakPtr.Data = value;
    }
  }

  public bool IsFinished()
  {
    if (!this.IsNull())
      return this.weakPtr.IsFinished();
    return true;
  }

  public JobEvent GetJobEvent()
  {
    if (!this.IsNull())
      return this.weakPtr.GetJobEvent();
    return JobEvent.JOB_UNKNOWN;
  }

  public void Reset(int startTime, int endTime)
  {
    if (this.IsNull())
      return;
    this.weakPtr.Reset(startTime, endTime);
  }

  public void Reset(JobEvent evt, int startTime, int endTime)
  {
    if (this.IsNull())
      return;
    this.weakPtr.Reset(evt, startTime, endTime);
  }

  public bool Speedup(int dt)
  {
    if (!this.IsNull())
      return this.weakPtr.Speedup(dt);
    return false;
  }

  public void Update(int timestamp)
  {
    if (this.IsNull())
      return;
    this.weakPtr.Update(timestamp);
  }

  public int StartTime()
  {
    if (!this.IsNull())
      return this.weakPtr.StartTime();
    return 0;
  }

  public int EndTime()
  {
    if (!this.IsNull())
      return this.weakPtr.EndTime();
    return 0;
  }

  public int Duration()
  {
    if (!this.IsNull())
      return this.weakPtr.Duration();
    return 0;
  }

  public int LeftTime()
  {
    if (!this.IsNull())
      return this.weakPtr.LeftTime();
    return 0;
  }

  public double LeftTimeInMillisecond()
  {
    if (!this.IsNull())
      return this.weakPtr.LeftTimeInMillisecond();
    return 0.0;
  }

  public long GetJobID()
  {
    if (!this.IsNull())
      return this.weakPtr.GetId();
    return 0;
  }

  public long ServerJobID
  {
    get
    {
      if (!this.IsNull())
        return this.weakPtr.ServerJobID;
      return 0;
    }
    set
    {
      if (this.IsNull())
        return;
      this.weakPtr.ServerJobID = value;
    }
  }

  public bool IsClientJob
  {
    get
    {
      if (!this.IsNull())
        return this.weakPtr.ByClient;
      return false;
    }
  }

  public bool IsTrueJob
  {
    get
    {
      if (!this.IsNull())
        return this.weakPtr.IsTrueJob;
      return false;
    }
  }

  public bool IsFakeJob
  {
    get
    {
      if (!this.IsNull())
        return this.weakPtr.IsFakeJob;
      return false;
    }
  }

  public bool IsRealJob
  {
    get
    {
      if (!this.IsNull())
        return this.weakPtr.IsRealJob;
      return false;
    }
  }

  public bool IsHelped()
  {
    if (!this.IsNull())
      return this.weakPtr.IsHelped();
    return true;
  }

  public long MarchID
  {
    get
    {
      if (!this.IsNull())
        return this.weakPtr.MarchID;
      return 0;
    }
  }

  public long RallyID
  {
    get
    {
      if (!this.IsNull())
        return this.weakPtr.RallyID;
      return -1;
    }
  }

  public static implicit operator bool(JobHandle handle)
  {
    if (handle != null)
      return !handle.IsNull();
    return false;
  }
}
