﻿// Decompiled with JetBrains decompiler
// Type: GVERewardsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class GVERewardsPopup : BaseReportPopup
{
  private GVERewardsPopup.GVERewardsContent grc;
  public UIScrollView sv;
  public UILabel content;
  public UILabel basicrewards;
  public UILabel killrewards;
  public UILabel limitrewards;
  public UILabel nolimitrewards;
  public IconListComponent basicilc;
  public IconListComponent killilc;
  public IconListComponent limitilc;
  public GameObject limitGroup;
  public GameObject nolimitGroup;

  private void Init()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, true, false, string.Empty);
    this.content.text = this.param.mail.GetBodyString();
    ConfigGveBossData data = ConfigManager.inst.DB_GveBoss.GetData(this.grc.boss_id);
    this.basicrewards.text = ScriptLocalization.GetWithPara("mail_pve_boss_damage_percent_rewards_description", new Dictionary<string, string>()
    {
      {
        "0",
        this.grc.kill_percents.ToString()
      }
    }, true);
    this.basicilc.FeedData((IComponentData) this.SummarizeRewards(data.BaseRewards.rewards, this.grc.kill_percents * this.grc.rewards_multiplier));
    this.killrewards.text = ScriptLocalization.GetWithPara("mail_pve_boss_victory_rewards_description", new Dictionary<string, string>()
    {
      {
        "0",
        data.boss_level.ToString()
      },
      {
        "1",
        ScriptLocalization.Get(data.name, true)
      }
    }, true);
    IconListComponentData listComponentData = this.SummarizeRewards(data.KillRewards.rewards, this.grc.rewards_multiplier);
    if (this.grc.festival_rewards.Count > 0)
    {
      using (Dictionary<string, int>.Enumerator enumerator = this.grc.festival_rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int internalId = int.Parse(current.Key);
          int num = current.Value;
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
          listComponentData.data.Add(new IconData(itemStaticInfo.ImagePath, new string[2]
          {
            ScriptLocalization.Get(itemStaticInfo.Loc_Name_Id, true),
            "X" + num.ToString()
          })
          {
            Data = (object) internalId
          });
        }
      }
    }
    if (this.grc.kingdom_coin > 0)
    {
      IconData iconData = new IconData("Texture/Alliance/kingdom_coin", new string[2]
      {
        ScriptLocalization.Get("throne_treasury_currency_name", true),
        "X" + this.grc.kingdom_coin.ToString()
      });
      listComponentData.data.Add(iconData);
    }
    this.killilc.FeedData((IComponentData) listComponentData);
    Dictionary<string, string> para = new Dictionary<string, string>()
    {
      {
        "0",
        ConfigManager.inst.DB_Items.GetItem(data.advanced_req_id).LocName
      }
    };
    if (this.grc.has_advanced_rewards == 1)
    {
      this.ShowLimitRewards(true);
      this.limitrewards.text = ScriptLocalization.GetWithPara("mail_pve_boss_special_item_rewards_description", para, true);
      this.limitilc.FeedData((IComponentData) this.SummarizeRewards(data.AdvanceRewards.rewards, 1));
    }
    else
    {
      this.ShowLimitRewards(false);
      this.nolimitrewards.text = ScriptLocalization.GetWithPara("mail_pve_boss_no_special_item_description", para, true);
    }
  }

  private void ShowLimitRewards(bool v)
  {
    this.limitGroup.SetActive(v);
    this.nolimitGroup.SetActive(!v);
  }

  private IconListComponentData SummarizeRewards(Dictionary<string, int> rewards, int scale = 1)
  {
    if (rewards == null)
      return (IconListComponentData) null;
    List<IconData> data = new List<IconData>();
    using (Dictionary<string, int>.Enumerator enumerator = rewards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, int> current = enumerator.Current;
        int internalId = int.Parse(current.Key);
        int num = current.Value * scale;
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
        data.Add(new IconData(itemStaticInfo.ImagePath, new string[2]
        {
          ScriptLocalization.Get(itemStaticInfo.Loc_Name_Id, true),
          "X" + num.ToString()
        })
        {
          Data = (object) internalId
        });
      }
    }
    return new IconListComponentData(data);
  }

  private void Reset()
  {
    this.sv.ResetPosition();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.grc = JsonReader.Deserialize<GVERewardsPopup.GVERewardsContent>(Utils.Object2Json(this.param.hashtable[(object) "data"]));
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }

  public class GVERewardsContent
  {
    public Dictionary<string, int> festival_rewards = new Dictionary<string, int>();
    public int kill_percents;
    public int kingdom_coin;
    public int boss_id;
    public int has_advanced_rewards;
    public int rewards_multiplier;
  }
}
