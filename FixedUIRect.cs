﻿// Decompiled with JetBrains decompiler
// Type: FixedUIRect
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIPanel))]
public class FixedUIRect : MonoBehaviour
{
  public float IPHONE_X_SAVE_RASIO = 0.965f;
  public FixedUIRect.SAVE_AREA type;
  private UIPanel panel;

  private void OnEnable()
  {
    this.FixClipRegion();
  }

  private void Start()
  {
    this.FixClipRegion();
  }

  private void FixClipRegion()
  {
    this.panel = this.GetComponent<UIPanel>();
    if (!((Object) this.panel != (Object) null) || !NativeManager.inst.IsIPHONEX() || !((Object) this.panel.root != (Object) null))
      return;
    this.panel.clipping = UIDrawCall.Clipping.ConstrainButDontClip;
    if (this.type == FixedUIRect.SAVE_AREA.SAVE_AREA_WITH_BOTTOM)
      this.panel.baseClipRegion = new Vector4(0.0f, 0.0f, (float) this.panel.root.manualHeight * 2.018667f, (float) this.panel.root.manualHeight);
    else if (this.type == FixedUIRect.SAVE_AREA.SAVE_AREA)
    {
      this.panel.baseClipRegion = new Vector4(0.0f, 0.0f, (float) this.panel.root.manualHeight * 2.093333f, (float) this.panel.root.manualHeight);
      this.gameObject.transform.localPosition = new Vector3(0.0f, (float) ((1.0 - (double) this.IPHONE_X_SAVE_RASIO) * (double) this.panel.root.manualHeight / 2.0));
      this.gameObject.transform.localScale = this.IPHONE_X_SAVE_RASIO * Vector3.one;
    }
    else
      this.panel.baseClipRegion = new Vector4(0.0f, 0.0f, (float) this.panel.root.manualHeight * 2.165333f, (float) this.panel.root.manualHeight);
  }

  public enum SAVE_AREA
  {
    FULL_SCREEN,
    SAVE_AREA_WITH_BOTTOM,
    SAVE_AREA,
  }
}
