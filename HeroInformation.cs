﻿// Decompiled with JetBrains decompiler
// Type: HeroInformation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class HeroInformation : MonoBehaviour
{
  private List<GameObject> pool = new List<GameObject>();
  public float skillGap = 200f;
  public GameObject heroicon;
  public UITexture icon;
  public GameObject skillTemplate;
  public UILabel heroname;

  public void SeedData(HeroInformation.Data d)
  {
    if (d.Legend == 0L)
    {
      this.heroicon.SetActive(false);
    }
    else
    {
      this.heroicon.SetActive(true);
      LegendInfo legendInfo = ConfigManager.inst.DB_Legend.GetLegendInfo((int) d.Legend);
      if (legendInfo != null)
      {
        BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, legendInfo.Icon, (System.Action<bool>) null, true, false, string.Empty);
        this.heroicon.GetComponentInChildren<UILabel>().text = ScriptLocalization.Get("id_lv", true) + (object) d.level;
        this.heroname.text = legendInfo.Loc_Name;
      }
      for (int index = 0; index < d.Skills.Count; ++index)
      {
        LegendSkillInfo skillInfo = ConfigManager.inst.DB_LegendSkill.GetSkillInfo((int) d.Skills[index]);
        if (legendInfo != null)
        {
          GameObject gameObject = Utils.DuplicateGOB(this.skillTemplate);
          Vector3 localPosition = gameObject.transform.localPosition;
          localPosition.x += (float) index * this.skillGap;
          gameObject.transform.localPosition = localPosition;
          gameObject.SetActive(true);
          this.pool.Add(gameObject);
          BuilderFactory.Instance.HandyBuild((UIWidget) gameObject.GetComponentInChildren<UITexture>(), skillInfo.Image, (System.Action<bool>) null, true, false, string.Empty);
          gameObject.GetComponentInChildren<UILabel>().text = ScriptLocalization.Get("id_lv", true) + (object) skillInfo.Level;
        }
      }
    }
  }

  public void Reset()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        BuilderFactory.Instance.Release((UIWidget) current.GetComponentInChildren<UITexture>());
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.pool.Clear();
  }

  public class Data
  {
    public long Legend;
    public int level;
    public List<long> Skills;
  }
}
