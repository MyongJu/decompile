﻿// Decompiled with JetBrains decompiler
// Type: RewardSelectionRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class RewardSelectionRender : MonoBehaviour
{
  private List<RewardItemData> itemMap = new List<RewardItemData>();
  private List<RewardItemRender> selectedRewards = new List<RewardItemRender>();
  private List<RewardItemRender> rewardRenderList = new List<RewardItemRender>();
  private List<BoothItemRender> boothRenderList = new List<BoothItemRender>();
  private List<BoothItemData> boothDatas = new List<BoothItemData>();
  private Dictionary<int, int> rewardMap = new Dictionary<int, int>();
  private Dictionary<int, RewardPair> rewardPairMap = new Dictionary<int, RewardPair>();
  private Dictionary<int, Reward> mainRewardMap = new Dictionary<int, Reward>();
  private List<int> selectedItems = new List<int>();
  private const string SLOT_NUM = "tavern_chance_slot_number";
  private const string SLOT_FULL = "toast_tavern_wheel_slot_full";
  private const string SLOT_EMPTY = "toast_tavern_wheel_pool_empty";
  private const string ROULETTE_CLOSED = "tavern_chance_closed_description";
  private const string ADD_DIS = "tavern_chance_confirm_add_description";
  private const string SELECT_WARNING = "toast_tavern_wheel_reward_select";
  public UIScrollView topScrollView;
  public UITable topTable;
  public UIScrollView bottomScrollView;
  public UITable bottomTable;
  public GameObject topTemplate;
  public GameObject bottomTemplate;
  public UILabel normalLabel;
  public UILabel selectedLabel;
  public UILabel tips;
  public System.Action<RewardSelectionRender> OnBoothFullHandler;
  private List<Reward> rewardList;
  private List<int> haveSelected;
  private int maxCount;
  private int selectedCount;
  private bool initialized;
  private bool resetRule;
  private bool haveAppliedSelections;
  private int grade;

  public int Grade
  {
    get
    {
      return this.grade;
    }
    set
    {
      this.grade = value;
    }
  }

  public int Index
  {
    get
    {
      return this.grade - 1;
    }
  }

  public List<int> SelectedItems
  {
    get
    {
      return this.selectedItems;
    }
  }

  public void Show(List<int> haveSelected, List<Reward> rewardList, Dictionary<int, Reward> mainRewardMap, int maxCount, int grade)
  {
    this.haveSelected = haveSelected;
    this.Show(rewardList, mainRewardMap, maxCount, grade);
    if (this.haveAppliedSelections)
      return;
    this.ApplySelectedRewards();
  }

  public void Show(List<Reward> rewardList, Dictionary<int, Reward> mainRewardMap, int maxCount, int grade)
  {
    this.gameObject.SetActive(true);
    if (rewardList == null)
      return;
    this.rewardList = rewardList;
    this.mainRewardMap = mainRewardMap;
    this.maxCount = maxCount;
    this.grade = grade;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (!this.initialized)
    {
      this.CreateItems();
      this.initialized = true;
    }
    this.tips.text = ScriptLocalization.GetWithPara("tavern_chance_confirm_add_description", new Dictionary<string, string>()
    {
      {
        "0",
        this.rewardList.Count.ToString()
      },
      {
        "1",
        this.maxCount.ToString()
      }
    }, true);
  }

  private void ApplySelectedRewards()
  {
    for (int index1 = 0; index1 < this.haveSelected.Count; ++index1)
    {
      for (int index2 = 0; index2 < this.rewardRenderList.Count; ++index2)
      {
        TavernWheelMainInfo tavernWheelMainInfo = ConfigManager.inst.DB_TavernWheelMain.Get(this.haveSelected[index1]);
        if (tavernWheelMainInfo != null)
        {
          List<Reward.RewardsValuePair> rewards = tavernWheelMainInfo.rewards.GetRewards();
          if (rewards[0].internalID > 0 && rewards[0].value > 0)
          {
            int itemId = this.rewardRenderList[index2].Data.itemId;
            int num = this.rewardPairMap[this.rewardRenderList[index2].Data.mainId].value;
            if (itemId == rewards[0].internalID && num == rewards[0].value)
            {
              this.OnRewardItemClicked(this.rewardRenderList[index2]);
              break;
            }
          }
        }
      }
    }
    this.haveAppliedSelections = true;
  }

  public void RefreshTabTitles(int grade, int count, int maxCount)
  {
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("0", grade.ToString());
    string str1 = string.Format("({0}/{1})", (object) count, (object) maxCount);
    UILabel normalLabel = this.normalLabel;
    string str2 = ScriptLocalization.GetWithPara("tavern_chance_slot_number", para, true) + str1;
    this.selectedLabel.text = str2;
    string str3 = str2;
    normalLabel.text = str3;
  }

  private void CreateItems()
  {
    this.CreateRewards();
    this.CreateEmpltyBooths();
  }

  private TavernWheelMainInfo GetMainWithReward(int itemId, int itemCount)
  {
    TavernWheelMainInfo tavernWheelMainInfo = (TavernWheelMainInfo) null;
    Dictionary<int, Reward>.Enumerator enumerator = this.mainRewardMap.GetEnumerator();
    while (enumerator.MoveNext())
    {
      Reward.RewardsValuePair reward = enumerator.Current.Value.GetRewards()[0];
      if (reward.internalID > 0 && reward.value > 0 && (reward.internalID == itemId && reward.value == itemCount))
      {
        tavernWheelMainInfo = ConfigManager.inst.DB_TavernWheelMain.Get(enumerator.Current.Key);
        break;
      }
    }
    return tavernWheelMainInfo;
  }

  private void CreateRewards()
  {
    for (int index1 = 0; index1 < this.rewardList.Count; ++index1)
    {
      List<Reward.RewardsValuePair> rewards = this.rewardList[index1].GetRewards();
      for (int index2 = 0; index2 < rewards.Count; ++index2)
      {
        Reward.RewardsValuePair rewardsValuePair = rewards[index2];
        if (rewardsValuePair.internalID > 0 && rewardsValuePair.value > 0)
        {
          RewardItemData rewardItemData = new RewardItemData();
          TavernWheelMainInfo mainWithReward = this.GetMainWithReward(rewardsValuePair.internalID, rewardsValuePair.value);
          if (mainWithReward != null && this.mainRewardMap.ContainsKey(mainWithReward.internalId))
          {
            rewardItemData.itemId = rewardsValuePair.internalID;
            rewardItemData.count = rewardsValuePair.value;
            rewardItemData.mainId = mainWithReward.internalId;
            this.itemMap.Add(rewardItemData);
          }
        }
      }
    }
    for (int index = 0; index < this.itemMap.Count; ++index)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.topTemplate);
      gameObject.transform.parent = this.topTable.transform;
      gameObject.transform.localScale = Vector3.one;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.SetActive(true);
      gameObject.GetComponent<UIDragScrollView>().scrollView = this.topScrollView;
      RewardItemRender component = gameObject.GetComponent<RewardItemRender>();
      RewardItemData data = new RewardItemData();
      data.itemId = this.itemMap[index].itemId;
      data.count = this.itemMap[index].count;
      data.mainId = this.itemMap[index].mainId;
      component.OnItemClickedHandler += new System.Action<RewardItemRender>(this.OnRewardItemClicked);
      component.OnCheckBoxClickedHandler += new System.Action<RewardItemRender>(this.OnCheckBoxClicked);
      component.FeedData(data);
      this.rewardPairMap.Add(data.mainId, new RewardPair()
      {
        internalId = data.itemId,
        value = data.count
      });
      this.rewardRenderList.Add(component);
    }
  }

  private void OnCheckBoxClicked(RewardItemRender render)
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
    {
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    }
    else
    {
      BoothItemRender render1 = (BoothItemRender) null;
      for (int index = 0; index < this.boothRenderList.Count; ++index)
      {
        if (this.boothRenderList[index].Data.itemId == render.Data.itemId && this.boothRenderList[index].Data.itemCount == render.Data.count)
        {
          render1 = this.boothRenderList[index];
          break;
        }
      }
      this.OnBoothItemClicked(render1);
    }
  }

  private void OnRewardItemClicked(RewardItemRender render)
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    else if (!this.isBoothPanelFull())
    {
      ++this.selectedCount;
      render.Select = true;
      this.selectedRewards.Add(render);
      BoothItemData data = new BoothItemData();
      data.itemId = render.Data.itemId;
      data.itemCount = render.Data.count;
      this.boothDatas.Add(data);
      this.UpdateBoothItems(this.selectedCount - 1, data);
      this.RefreshTabTitles(this.grade, this.selectedCount, this.maxCount);
      if (this.selectedCount != this.maxCount)
        return;
      this.selectedItems.Clear();
      for (int index = 0; index < this.selectedRewards.Count; ++index)
      {
        TavernWheelMainInfo mainWithReward = this.GetMainWithReward(this.selectedRewards[index].Data.itemId, this.rewardPairMap[this.selectedRewards[index].Data.mainId].value);
        if (mainWithReward != null)
          this.selectedItems.Add(mainWithReward.internalId);
      }
      if (this.OnBoothFullHandler == null)
        return;
      this.OnBoothFullHandler(this);
    }
    else
      UIManager.inst.toast.Show(Utils.XLAT("toast_tavern_wheel_slot_full"), (System.Action) null, 4f, false);
  }

  private bool ShouldAutoGoTo()
  {
    return this.haveSelected == null || this.resetRule;
  }

  private void CreateEmpltyBooths()
  {
    for (int index = 0; index < this.maxCount; ++index)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.bottomTemplate);
      gameObject.transform.parent = this.bottomTable.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localScale = Vector3.one;
      gameObject.SetActive(true);
      BoothItemRender component = gameObject.GetComponent<BoothItemRender>();
      component.OnBoothItemClickedHandler += new System.Action<BoothItemRender>(this.OnBoothItemClicked);
      component.OnEmptyBoothItemClickedHandler += new System.Action<BoothItemRender>(this.OnEmptyBoothItemClicked);
      component.FeedData((BoothItemData) null);
      this.boothRenderList.Add(component);
    }
  }

  private void ResetBoothItems()
  {
    for (int index = 0; index < this.boothRenderList.Count; ++index)
      this.boothRenderList[index].FeedData((BoothItemData) null);
  }

  private void UpdateBoothItems(int index, BoothItemData data)
  {
    if (index < 0 || index >= this.boothRenderList.Count)
      return;
    this.boothRenderList[index].FeedData(data);
  }

  private void UpdateBoothItems()
  {
    this.ResetBoothItems();
    if (this.boothDatas.Count > this.boothRenderList.Count)
      return;
    for (int index = 0; index < this.boothDatas.Count; ++index)
      this.boothRenderList[index].FeedData(this.boothDatas[index]);
  }

  private void OnEmptyBoothItemClicked(BoothItemRender render)
  {
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_tavern_wheel_reward_select", true), (System.Action) null, 1.5f, false);
  }

  private void OnBoothItemClicked(BoothItemRender render)
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) render)
      return;
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
    {
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    }
    else
    {
      --this.selectedCount;
      if (this.haveSelected != null)
        this.resetRule = true;
      this.RefreshTabTitles(this.grade, this.selectedCount, this.maxCount);
      this.RefreshRewardItems(render);
      this.RefreshBoothItems(render);
    }
  }

  private void RefreshRewardItems(BoothItemRender render)
  {
    for (int index = this.selectedRewards.Count - 1; index >= 0; --index)
    {
      if (this.selectedRewards[index].Data.itemId == render.Data.itemId && this.selectedRewards[index].Data.count == render.Data.itemCount)
      {
        this.selectedRewards[index].Select = false;
        this.selectedRewards.RemoveAt(index);
      }
    }
  }

  private void RefreshBoothItems(BoothItemRender render)
  {
    int index1 = 0;
    for (int index2 = 0; index2 < this.boothDatas.Count; ++index2)
    {
      if (this.boothDatas[index2].itemId == render.Data.itemId && this.boothDatas[index2].itemCount == render.Data.itemCount)
      {
        index1 = index2;
        break;
      }
    }
    this.boothDatas.RemoveAt(index1);
    this.UpdateBoothItems();
  }

  public bool isBoothPanelFull()
  {
    bool flag = true;
    if (this.boothRenderList.Count == 0)
    {
      flag = false;
    }
    else
    {
      for (int index = 0; index < this.boothRenderList.Count; ++index)
      {
        if (!this.boothRenderList[index].IsOccupied)
        {
          flag = false;
          break;
        }
      }
    }
    return flag;
  }

  private void ClearData()
  {
    for (int index = 0; index < this.rewardRenderList.Count; ++index)
    {
      this.rewardRenderList[index].transform.parent = (Transform) null;
      this.rewardRenderList[index].gameObject.SetActive(false);
      this.rewardRenderList[index].OnItemClickedHandler -= new System.Action<RewardItemRender>(this.OnRewardItemClicked);
      this.rewardRenderList[index].OnCheckBoxClickedHandler -= new System.Action<RewardItemRender>(this.OnCheckBoxClicked);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.rewardRenderList[index].gameObject);
    }
    this.rewardRenderList.Clear();
    for (int index = 0; index < this.boothRenderList.Count; ++index)
    {
      this.boothRenderList[index].transform.parent = (Transform) null;
      this.boothRenderList[index].gameObject.SetActive(false);
      this.boothRenderList[index].OnBoothItemClickedHandler -= new System.Action<BoothItemRender>(this.OnBoothItemClicked);
      this.boothRenderList[index].OnEmptyBoothItemClickedHandler -= new System.Action<BoothItemRender>(this.OnEmptyBoothItemClicked);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.boothRenderList[index].gameObject);
    }
    this.boothRenderList.Clear();
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  public void Release()
  {
    this.ClearData();
  }
}
