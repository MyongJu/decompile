﻿// Decompiled with JetBrains decompiler
// Type: ShopItemUI
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UnityEngine;

public class ShopItemUI : MonoBehaviour
{
  public UITexture itemIconTexture;
  public UILabel itemName;
  public UILabel itemPrice;
  public UILabel ownNum;
  public UIButton buyBtn;
  public UIButton useBtn;
  public System.Action<ShopItemUI> onItemClicked;
  private bool isAddedEventHandler;
  private ConsumableItemData _data;

  public ConsumableItemData item
  {
    get
    {
      if (this.ShopStaticInfo != null)
        return DBManager.inst.DB_Item.Get(ConfigManager.inst.DB_Items.GetItem(this.ShopStaticInfo.Item_InternalId).internalId);
      return (ConsumableItemData) null;
    }
  }

  public ShopStaticInfo ShopStaticInfo { protected set; get; }

  public int GetQuantity()
  {
    if (this.item != null)
      return this.item.quantity;
    return 0;
  }

  public string GetCategory()
  {
    if (this.ShopStaticInfo != null)
      return this.ShopStaticInfo.Category;
    return string.Empty;
  }

  public void SetShopStaticInfo(ShopStaticInfo info)
  {
    this.ShopStaticInfo = info;
    this.UpdateUINew();
  }

  public void Clear()
  {
    if (this.isAddedEventHandler)
    {
      this.isAddedEventHandler = false;
      if (Oscillator.IsAvailable)
        Oscillator.Instance.updateEvent -= new System.Action<double>(this.Process);
    }
    BuilderFactory.Instance.Release((UIWidget) this.itemIconTexture);
    this.ShopStaticInfo = (ShopStaticInfo) null;
  }

  public void SetItemData(ConsumableItemData data)
  {
    this._data = data;
    this.UpdateUINew();
  }

  private void UpdateUINew()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.ShopStaticInfo == null ? this._data.internalId : this.ShopStaticInfo.Item_InternalId);
    ConsumableItemData consumableItemData = (DBManager.inst.DB_Item.Get(itemStaticInfo.internalId) ?? this._data) ?? this._data;
    int num = 0;
    if (consumableItemData != null)
      num = consumableItemData.quantity;
    this.ownNum.text = string.Format("x{0}", (object) num);
    this.itemName.text = ScriptLocalization.Get(itemStaticInfo.Loc_Name_Id, true);
    string path = itemStaticInfo.ImagePath;
    if (this.ShopStaticInfo != null)
      path = "Texture/ItemIcons/" + this.ShopStaticInfo.Image;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.itemIconTexture, path, (System.Action<bool>) null, true, false, string.Empty);
    if (this.ShopStaticInfo != null)
    {
      this.itemPrice.text = this.ShopStaticInfo.Price > 0 ? ItemBag.Instance.GetShopItemPrice(this.ShopStaticInfo.ID).ToString() : ScriptLocalization.Get("shop_buy_free", true);
      if ((double) this.ShopStaticInfo.Discount_In_Limit_Time != 0.0 && this.ShopStaticInfo.Limit_Discount_End_Time > (long) NetServerTime.inst.ServerTimestamp && !this.isAddedEventHandler)
      {
        this.isAddedEventHandler = true;
        Oscillator.Instance.updateEvent += new System.Action<double>(this.Process);
      }
    }
    else
    {
      this.itemPrice.text = string.Empty;
      if (Oscillator.IsAvailable)
        Oscillator.Instance.updateEvent -= new System.Action<double>(this.Process);
    }
    if (num > 0)
      this.ownNum.color = ItemStore.NonZeroColor;
    else
      this.ownNum.color = ItemStore.ZeroColor;
  }

  private void Process(double time)
  {
    if (this.ShopStaticInfo.Price <= 0)
      this.itemPrice.text = ScriptLocalization.Get("shop_buy_free", true);
    else
      this.itemPrice.text = ItemBag.Instance.GetShopItemPrice(this.ShopStaticInfo.ID).ToString();
  }

  public void OnBuyClicked()
  {
    if (!ItemBag.Instance.CheckCanBuyShopItem(this.ShopStaticInfo.ID, true))
      return;
    ItemBag.Instance.BuyShopItem(this.ShopStaticInfo.internalId, 1, (Hashtable) null, (System.Action<bool, object>) null);
  }

  public void OnItemClicked()
  {
    if (this.onItemClicked == null)
      return;
    this.onItemClicked(this);
  }

  public void SwitchLocation(bool toStore)
  {
    if (toStore)
    {
      this.SetUseButtonActive(false);
      this.buyBtn.gameObject.SetActive(true);
    }
    else
    {
      this.SetUseButtonActive(true);
      this.buyBtn.gameObject.SetActive(false);
      if (this.GetQuantity() <= 0 || !ItemBag.CanShopItemBeUsedInStore(this.ShopStaticInfo))
        this.SetUseButtonEnable(false);
      else
        this.SetUseButtonEnable(true);
    }
  }

  private void SetUseButtonActive(bool active)
  {
    if (!((UnityEngine.Object) this.useBtn != (UnityEngine.Object) null))
      return;
    this.useBtn.gameObject.SetActive(active);
  }

  private void SetUseButtonEnable(bool enable)
  {
    if (!((UnityEngine.Object) this.useBtn != (UnityEngine.Object) null))
      return;
    this.useBtn.isEnabled = enable;
  }

  public void OnUse()
  {
    if (this.item == null)
      return;
    ItemBag.Instance.UseItem(this.item.internalId, 1, (Hashtable) null, (System.Action<bool, object>) null);
  }

  public void OnBuyAndUse()
  {
    ItemBag.Instance.BuyAndUseShopItem(this.ShopStaticInfo.internalId, 1, (Hashtable) null, (System.Action<bool, object>) null);
  }
}
