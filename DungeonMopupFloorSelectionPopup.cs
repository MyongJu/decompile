﻿// Decompiled with JetBrains decompiler
// Type: DungeonMopupFloorSelectionPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class DungeonMopupFloorSelectionPopup : Popup
{
  public UILabel _staminaNeeded;
  public UILabel _staminaOwned;
  public UIInput _floorSelected;
  public UILabel _floorProvided;
  public UISlider _floorSlider;
  public UIButton _minusButton;
  public UIButton _plusButton;
  public UILabel _staminaPaid;
  private bool _sliding;
  private int _maxLevelAllowed;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  private void UpdateUI()
  {
    this._maxLevelAllowed = DragonKnightUtils.MaxMopupLevelsAllowed;
    this._floorSlider.numberOfSteps = this._maxLevelAllowed;
    this._floorSlider.value = 0.0f;
    this._floorProvided.text = "/" + this._maxLevelAllowed.ToString();
    this._staminaOwned.text = "/" + PlayerData.inst.dragonKnightData.Spirit.ToString();
    if (this._maxLevelAllowed != 1)
      return;
    this._minusButton.isEnabled = false;
    this._plusButton.isEnabled = false;
  }

  private void UpdateSlider(int result)
  {
    result = Mathf.Max(1, Mathf.Min(result, this._maxLevelAllowed));
    this._floorSlider.value = this._maxLevelAllowed > 1 ? (float) (result - 1) / (float) (this._maxLevelAllowed - 1) : 0.0f;
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnInputValueChanged()
  {
    if (this._sliding)
      return;
    int result1;
    int.TryParse(this._floorSelected.value, out result1);
    int result2 = Mathf.Max(1, Mathf.Min(result1, this._maxLevelAllowed));
    this._floorSelected.value = result2.ToString();
    this.UpdateSlider(result2);
  }

  public void OnSliderChanged()
  {
    int level = this.GetLevel();
    this._sliding = true;
    this._floorSelected.value = level.ToString();
    this._sliding = false;
    int mopupStaminaNeeded = DragonKnightUtils.GetMopupStaminaNeeded(level);
    this._staminaNeeded.text = mopupStaminaNeeded.ToString();
    this._staminaPaid.text = mopupStaminaNeeded.ToString();
  }

  public void OnMinusClick()
  {
    this.UpdateSlider(this.GetLevel() - 1);
  }

  public void OnPlusClick()
  {
    this.UpdateSlider(this.GetLevel() + 1);
  }

  public void OnPlunder()
  {
    DragonKnightUtils.MopupMultiLevels(DragonKnightUtils.CurrentDungeonLevel, DragonKnightUtils.GetDungeonData().CurrentLevel + this.GetLevel() - 1, (System.Action<bool>) (ret =>
    {
      if (!ret)
        return;
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
      UIManager.inst.OpenPopup("DragonKnight/DungeonMopupCompletePopup", (Popup.PopupParameter) null);
    }));
  }

  private int GetLevel()
  {
    return Mathf.RoundToInt(this._floorSlider.value * (float) (this._maxLevelAllowed - 1)) + 1;
  }
}
