﻿// Decompiled with JetBrains decompiler
// Type: SettingDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class SettingDlg : UI.Dialog
{
  private List<UIToggle> languageToggles = new List<UIToggle>();
  private List<UIToggle> translatorToggles = new List<UIToggle>();
  private int _selectedIndex = -1;
  private int _languageIndex = -1;
  private int _translatorIndex = -1;
  public UIToggle musicOff;
  public UIToggle sfxOff;
  public UIToggle environmentOff;
  public UIToggle equipmentOn;
  public UIToggle daltonOff;
  public UIToggle prioritySelectionOff;
  public GameObject tabSettingContent;
  public GameObject tabAccountContent;
  public GameObject tabSettingMenuContent;
  public GameObject tabContactUsContent;
  public GameObject tabLanguageContent;
  public UIButton backBt;
  public UITexture settingIcon;
  public UITexture accountIcon;
  public UITexture helpIcon;
  public UITexture strategyIcon;
  public UITexture moreGameIcon;
  public UITexture newsIcon;
  public UITexture notificationIcon;
  public UITexture languageIcon;
  public UITexture giftExchangeIcon;
  public UITexture termsOfServiceIcon;
  public UITexture privacyIcon;
  public UITexture logoutIcon;
  public UITexture verficationIcon;
  public UITexture helpShiftIcon;
  public UIScrollView scrollView;
  public UIGrid buttonsGrid;
  public GameObject verificationBt;
  public GameObject helpShiftBt;
  public GameObject languageTemplate;
  public UIGrid languageGrid;
  public GameObject translatorTemplate;
  public UIGrid translatorGrid;
  public UITable languageTable;
  public UIScrollView languageScrollView;
  private bool _iOSExchangeSwitch;
  public UILabel time;
  private bool isDestroy;

  public int SelectedIndex
  {
    get
    {
      return this._selectedIndex;
    }
    set
    {
      if (this._selectedIndex == value || value >= 5)
        return;
      this._selectedIndex = value;
      this.ChangeView();
    }
  }

  private void LoadIcon()
  {
    string str = "Texture/SettingsIcons/";
    BuilderFactory.Instance.Build((UIWidget) this.settingIcon, str + "icon_settings", (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.helpIcon, str + "icon_game_help", (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.strategyIcon, str + "icon_game_help", (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.moreGameIcon, str + "icon_more_games", (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.newsIcon, str + "icon_community", (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.accountIcon, str + "icon_account", (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.notificationIcon, str + "icon_notification", (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.languageIcon, str + "icon_language", (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.giftExchangeIcon, str + "icon_gift_exchange", (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.termsOfServiceIcon, str + "icon_terms_of_service", (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.privacyIcon, str + "icon_privacy_policy", (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.logoutIcon, str + "icon_quit_account", (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.verficationIcon, str + "icon_real_name", (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.helpShiftIcon, str + "icon_game_help", (System.Action<bool>) null, true, false, true, string.Empty);
  }

  private void RealseIcon()
  {
    BuilderFactory.Instance.Release((UIWidget) this.settingIcon);
    BuilderFactory.Instance.Release((UIWidget) this.helpIcon);
    BuilderFactory.Instance.Release((UIWidget) this.strategyIcon);
    BuilderFactory.Instance.Release((UIWidget) this.moreGameIcon);
    BuilderFactory.Instance.Release((UIWidget) this.newsIcon);
    BuilderFactory.Instance.Release((UIWidget) this.accountIcon);
    BuilderFactory.Instance.Release((UIWidget) this.notificationIcon);
    BuilderFactory.Instance.Release((UIWidget) this.languageIcon);
    BuilderFactory.Instance.Release((UIWidget) this.giftExchangeIcon);
    BuilderFactory.Instance.Release((UIWidget) this.termsOfServiceIcon);
    BuilderFactory.Instance.Release((UIWidget) this.privacyIcon);
    BuilderFactory.Instance.Release((UIWidget) this.logoutIcon);
    BuilderFactory.Instance.Release((UIWidget) this.verficationIcon);
    BuilderFactory.Instance.Release((UIWidget) this.helpShiftIcon);
  }

  private void ChangeView()
  {
    NGUITools.SetActive(this.tabSettingContent, false);
    NGUITools.SetActive(this.tabAccountContent, false);
    NGUITools.SetActive(this.tabSettingMenuContent, false);
    NGUITools.SetActive(this.tabContactUsContent, false);
    NGUITools.SetActive(this.backBt.gameObject, false);
    NGUITools.SetActive(this.tabLanguageContent, false);
    NGUITools.SetActive(this.verificationBt.gameObject, false);
    NGUITools.SetActive(this.helpShiftBt.gameObject, false);
    NGUITools.SetActive(this.uiManagerPanel.BT_Back.gameObject, true);
    switch (this._selectedIndex)
    {
      case 0:
        NGUITools.SetActive(this.tabSettingContent, true);
        NGUITools.SetActive(this.backBt.gameObject, true);
        NGUITools.SetActive(this.uiManagerPanel.BT_Back.gameObject, false);
        Utils.ExecuteInSecs(1f / 1000f, (System.Action) (() => this.scrollView.ResetPosition()));
        break;
      case 1:
        FunplusAccount.Instance.ShowUserCenter();
        break;
      case 2:
        NGUITools.SetActive(this.tabSettingMenuContent, true);
        NGUITools.SetActive(this.helpShiftBt.gameObject, AccountManager.Instance.CanShowHelpShift());
        NGUITools.SetActive(this.verificationBt.gameObject, !AccountManager.Instance.IsVerification() && !CustomDefine.IsSQWanPackage());
        break;
      case 3:
        NGUITools.SetActive(this.tabContactUsContent, true);
        NGUITools.SetActive(this.backBt.gameObject, true);
        NGUITools.SetActive(this.uiManagerPanel.BT_Back.gameObject, false);
        break;
      case 4:
        NGUITools.SetActive(this.tabLanguageContent, true);
        NGUITools.SetActive(this.backBt.gameObject, true);
        NGUITools.SetActive(this.uiManagerPanel.BT_Back.gameObject, false);
        this.languageGrid.Reposition();
        this.translatorGrid.Reposition();
        Utils.ExecuteInSecs(1f / 1000f, (System.Action) (() => this.languageScrollView.ResetPosition()));
        break;
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    SettingDlg.Parameter parameter = orgParam as SettingDlg.Parameter;
    if (parameter != null)
      this._iOSExchangeSwitch = parameter.iOSExchangeSwitch;
    this.UpdateUI();
    this.LoadIcon();
    this.SelectedIndex = 2;
    this.UpdateLanguages();
    this._languageIndex = this.GetLanguageIndexByLocale(PlayerData.inst.userData.language);
    this._languageIndex = this._languageIndex >= 0 ? this._languageIndex : this.GetLanguageIndexByLocale("en");
    this.languageToggles[this._languageIndex].Set(true);
    this.UpdateTranslators();
    this._translatorIndex = this.GetTranslatorIndexByLocale(PlayerPrefs.GetString("Translator Locale"));
    this._translatorIndex = this._translatorIndex >= 0 ? this._translatorIndex : this.GetTranslatorIndexByLocale("en");
    this.translatorToggles[this._translatorIndex].Set(true);
    this.languageTable.Reposition();
    this.languageScrollView.ResetPosition();
    this.UpdateGiftExchange();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.RealseIcon();
    this.ClearLanguages();
    this.ClearTranslators();
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
  }

  private void UpdateGiftExchange()
  {
  }

  private void UpdateLanguages()
  {
    this.ClearLanguages();
    List<string> localization = Language.Instance.Localization;
    for (int index = 0; index < localization.Count; ++index)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.languageTemplate);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.languageGrid.transform;
      gameObject.transform.localScale = Vector3.one;
      gameObject.GetComponent<AllianceLanguageRenderer>().Set(localization[index]);
      this.languageToggles.Add(gameObject.GetComponent<UIToggle>());
    }
    this.languageGrid.Reposition();
  }

  private void ClearLanguages()
  {
    using (List<UIToggle>.Enumerator enumerator = this.languageToggles.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UIToggle current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.languageToggles.Clear();
  }

  private void OnDestroy()
  {
    this.isDestroy = true;
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  private void Process(int timestemp)
  {
    if (this.isDestroy)
      return;
    this.time.text = string.Format("{4}-{0}-{1} {2}:{3}:{5}", (object) DateTime.UtcNow.Month.ToString("00"), (object) DateTime.UtcNow.Day.ToString("00"), (object) DateTime.UtcNow.Hour.ToString("00"), (object) DateTime.UtcNow.Minute.ToString("00"), (object) DateTime.UtcNow.Year.ToString("00"), (object) DateTime.UtcNow.Second.ToString("00")) + " " + ScriptLocalization.Get("setting_system_time", true);
  }

  private void UpdateTranslators()
  {
    this.ClearTranslators();
    List<string> translator = Language.Instance.Translator;
    for (int index = 0; index < translator.Count; ++index)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.translatorTemplate);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.translatorGrid.transform;
      gameObject.transform.localScale = Vector3.one;
      gameObject.GetComponent<AllianceLanguageRenderer>().Set(translator[index]);
      this.translatorToggles.Add(gameObject.GetComponent<UIToggle>());
    }
    this.translatorGrid.Reposition();
  }

  private void ClearTranslators()
  {
    using (List<UIToggle>.Enumerator enumerator = this.translatorToggles.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UIToggle current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.translatorToggles.Clear();
  }

  private int GetLanguageIndexByLocale(string locale)
  {
    for (int index = 0; index < this.languageToggles.Count; ++index)
    {
      if (this.languageToggles[index].GetComponent<AllianceLanguageRenderer>().m_Locale == locale)
        return index;
    }
    return -1;
  }

  private int GetTranslatorIndexByLocale(string locale)
  {
    for (int index = 0; index < this.translatorToggles.Count; ++index)
    {
      if (this.translatorToggles[index].GetComponent<AllianceLanguageRenderer>().m_Locale == locale)
        return index;
    }
    return -1;
  }

  private int GetSelectedLanguageIndex()
  {
    for (int index = 0; index < this.languageToggles.Count; ++index)
    {
      if (this.languageToggles[index].value)
        return index;
    }
    return -1;
  }

  private int GetSelectedTranslatorIndex()
  {
    for (int index = 0; index < this.translatorToggles.Count; ++index)
    {
      if (this.translatorToggles[index].value)
        return index;
    }
    return -1;
  }

  public void OnToggleChanged()
  {
    int languageIndex = this.GetSelectedLanguageIndex();
    if (this._languageIndex == languageIndex || languageIndex == -1)
      return;
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = ScriptLocalization.Get("alliance_confirm_title", true),
      content = ScriptLocalization.Get("settings_changed_restart_game_prompt", true),
      yes = ScriptLocalization.Get("id_uppercase_yes", true),
      no = ScriptLocalization.Get("id_uppercase_no", true),
      yesCallback = (System.Action) (() =>
      {
        AllianceLanguageRenderer item = this.languageToggles[languageIndex].GetComponent<AllianceLanguageRenderer>();
        Hashtable postData = new Hashtable();
        postData[(object) "lang"] = (object) item.m_Locale;
        MessageHub.inst.GetPortByAction("Player:setLanguage").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          this._languageIndex = languageIndex;
          LocalizationManager.CurrentLanguageCode = item.m_Locale;
          GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Deep);
        }), true);
      }),
      noCallback = (System.Action) (() => this.languageToggles[this._languageIndex].Set(true))
    });
  }

  public void OnTranslatorChanged()
  {
    int selectedTranslatorIndex = this.GetSelectedTranslatorIndex();
    if (this._translatorIndex == selectedTranslatorIndex || selectedTranslatorIndex == -1)
      return;
    this._translatorIndex = selectedTranslatorIndex;
    PlayerPrefs.SetString("Translator Locale", this.translatorToggles[selectedTranslatorIndex].GetComponent<AllianceLanguageRenderer>().m_Locale);
  }

  public void OnViewSettingMenu()
  {
    this.SelectedIndex = 2;
  }

  public void OnViewSettingHandler()
  {
    this.SelectedIndex = 0;
  }

  public void OnOpenHelpShift()
  {
    UIManager.inst.OpenPopup("ContactServicePopup ", (Popup.PopupParameter) null);
  }

  public void OnViewNotificationHandler()
  {
    UIManager.inst.OpenPopup("NotificationSettingPopup", (Popup.PopupParameter) null);
  }

  public void OnViewAccountHandler()
  {
    UIManager.inst.OpenDlg("Account/AccountDialog", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnViewContractUSHandler()
  {
    this.SelectedIndex = 3;
  }

  public void OnViewLanguageHandler()
  {
    this.SelectedIndex = 4;
  }

  private void UpdateUI()
  {
    this.musicOff.Set(!SettingManager.Instance.IsMusicOff);
    this.sfxOff.Set(!SettingManager.Instance.IsSFXOff);
    this.environmentOff.Set(!SettingManager.Instance.IsEnvironmentOff);
    this.equipmentOn.Set(PlayerData.inst.userData.IsSettingOn(1));
    this.daltonOff.Set(!SettingManager.Instance.IsDaltonOff);
    this.prioritySelectionOff.Set(SettingManager.Instance.IsPrioritySelectionOff);
    if (!CustomDefine.IsSQWanPackage())
      return;
    NGUITools.SetActive(this.buttonsGrid.transform.Find("settings").gameObject, true);
    NGUITools.SetActive(this.buttonsGrid.transform.Find("account").gameObject, false);
    NGUITools.SetActive(this.buttonsGrid.transform.Find("language").gameObject, false);
    NGUITools.SetActive(this.buttonsGrid.transform.Find("game_help").gameObject, false);
    NGUITools.SetActive(this.buttonsGrid.transform.Find("game_strategy").gameObject, true);
    NGUITools.SetActive(this.buttonsGrid.transform.Find("notification").gameObject, false);
    NGUITools.SetActive(this.buttonsGrid.transform.Find("more_games").gameObject, false);
    NGUITools.SetActive(this.buttonsGrid.transform.Find("community").gameObject, false);
    NGUITools.SetActive(this.buttonsGrid.transform.Find("gift_exchange").gameObject, true);
    NGUITools.SetActive(this.buttonsGrid.transform.Find("real_name_authentication").gameObject, false);
    NGUITools.SetActive(this.buttonsGrid.transform.Find("term_of_services").gameObject, true);
    NGUITools.SetActive(this.buttonsGrid.transform.Find("privacy").gameObject, true);
    NGUITools.SetActive(this.buttonsGrid.transform.Find("logout").gameObject, true);
  }

  public void OnViewMoreGame()
  {
    Utils.PopUpCommingSoon();
  }

  public void OnViewHelp()
  {
    UIManager.inst.OpenPopup("Mod/ModEntrance", (Popup.PopupParameter) null);
  }

  public void OnGameStrategy()
  {
  }

  public void OnViewNews()
  {
    Utils.PopUpCommingSoon();
  }

  public void OnViewCommunity()
  {
    UIManager.inst.OpenDlg("Community/CommunityDialog", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnMusicOffHandler()
  {
    SettingManager.Instance.IsMusicOff = !this.musicOff.value;
  }

  public void OnSFXOffHandler()
  {
    SettingManager.Instance.IsSFXOff = !this.sfxOff.value;
  }

  public void OnEnvironmentDetails()
  {
    if (SettingManager.Instance.IsEnvironmentOff == !this.environmentOff.value)
      return;
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = ScriptLocalization.Get("alliance_confirm_title", true),
      content = ScriptLocalization.Get("settings_changed_restart_game_prompt", true),
      yes = ScriptLocalization.Get("id_uppercase_yes", true),
      no = ScriptLocalization.Get("id_uppercase_no", true),
      yesCallback = (System.Action) (() =>
      {
        SettingManager.Instance.IsEnvironmentOff = !this.environmentOff.value;
        GameEngine.Instance.RestartGame();
      }),
      noCallback = (System.Action) (() => this.environmentOff.value = !this.environmentOff.value)
    });
  }

  public void OnPrioritySelection()
  {
    if (this.prioritySelectionOff.value == SettingManager.Instance.IsPrioritySelectionOff)
      return;
    if (this.prioritySelectionOff.value)
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_settings_tap_select_preference_march_success"), (System.Action) null, 4f, false);
      SettingManager.Instance.IsPrioritySelectionOff = true;
    }
    else
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_settings_tap_select_preference_city_success"), (System.Action) null, 4f, false);
      SettingManager.Instance.IsPrioritySelectionOff = false;
    }
  }

  public void OnShowEquipment()
  {
    if (this.equipmentOn.value == PlayerData.inst.userData.IsSettingOn(1))
      return;
    Hashtable hashtable = new Hashtable();
    hashtable[(object) 1.ToString()] = (object) (!this.equipmentOn.value ? 0 : 1);
    Hashtable postData = new Hashtable();
    postData[(object) "setting"] = (object) hashtable;
    MessageHub.inst.GetPortByAction("Player:setSetting").SendRequest(postData, (System.Action<bool, object>) null, false);
  }

  public void OnGiftExchange()
  {
    UIManager.inst.OpenPopup("GiftExchange/GiftExchangePopup", (Popup.PopupParameter) null);
  }

  public void OnTermsOfService()
  {
    UIManager.inst.OpenDlg("Community/TermsOfServiceDialog", (UI.Dialog.DialogParameter) new TermsOfServiceDialog.Parameter()
    {
      title = "settings_uppercase_terms_of_service",
      description = "settings_terms_of_service_description"
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnPrivacy()
  {
    UIManager.inst.OpenDlg("Community/TermsOfServiceDialog", (UI.Dialog.DialogParameter) new TermsOfServiceDialog.Parameter()
    {
      title = "settings_uppercase_privacy_policy",
      description = "settings_privacy_policy_description"
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnLogout()
  {
  }

  public void OnVerication()
  {
    if (CustomDefine.IsSQWanPackage())
      return;
    UIManager.inst.OpenDlg("VerificationDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnDaltonMode()
  {
    if (SettingManager.Instance.IsDaltonOff == !this.daltonOff.value)
      return;
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = ScriptLocalization.Get("alliance_confirm_title", true),
      content = ScriptLocalization.Get("settings_changed_restart_game_prompt", true),
      yes = ScriptLocalization.Get("id_uppercase_yes", true),
      no = ScriptLocalization.Get("id_uppercase_no", true),
      yesCallback = (System.Action) (() =>
      {
        SettingManager.Instance.IsDaltonOff = !this.daltonOff.value;
        GameEngine.Instance.RestartGame();
      }),
      noCallback = (System.Action) (() => this.daltonOff.value = !this.daltonOff.value)
    });
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public bool iOSExchangeSwitch;
  }
}
