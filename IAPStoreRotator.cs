﻿// Decompiled with JetBrains decompiler
// Type: IAPStoreRotator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class IAPStoreRotator : MonoBehaviour
{
  private List<IAPStorePackagePanel> m_ItemPackageList = new List<IAPStorePackagePanel>();
  private List<UIToggle> m_KnobList = new List<UIToggle>();
  private const float ROTATOR_INTERVAL = 5f;
  public UILabel m_PackageName;
  public GameObject m_PackagePrefab;
  public UIWrapContent m_WrapContent;
  public UICenterOnChild m_Center;
  public UIGrid m_PackageGrid;
  public UIScrollView m_PackageScrollView;
  public GameObject m_KnobPrefab;
  public UIGrid m_KnobGrid;
  private List<IAPStorePackage> m_Packages;
  private float m_RotatorSeconds;
  private bool m_Rotating;
  private TweenPosition m_TweenPosition;

  public void SetPackageList(List<IAPStorePackage> packages)
  {
    this.m_Packages = packages;
    this.UpdateItemPackages();
  }

  public void OnShowPackageDetail()
  {
    UIManager.inst.OpenPopup("IAPBuyConfirmPopup", (Popup.PopupParameter) new IAPBuyConfirmPopup.Parameter()
    {
      package = this.m_Packages[this.GetSelectedIndex()]
    });
  }

  private void StopRotator()
  {
    this.m_RotatorSeconds = 0.0f;
    this.m_Rotating = false;
    if (!(bool) ((Object) this.m_TweenPosition))
      return;
    this.m_TweenPosition.enabled = false;
  }

  private void Update()
  {
    if (this.m_Packages == null || this.m_Packages.Count <= 2)
      this.StopRotator();
    else if (this.m_PackageScrollView.isDragging)
    {
      this.StopRotator();
    }
    else
    {
      if ((double) this.m_RotatorSeconds > 5.0)
      {
        this.m_RotatorSeconds = 0.0f;
        Vector3 localPosition = this.m_WrapContent.transform.localPosition;
        localPosition.x -= 860f;
        this.m_TweenPosition = TweenPosition.Begin(this.m_WrapContent.gameObject, 1f, localPosition, false);
        this.m_TweenPosition.SetOnFinished(new EventDelegate.Callback(this.OnTweenFinished));
        this.m_Rotating = true;
      }
      else
        this.m_RotatorSeconds += Time.deltaTime;
      if (!this.m_Rotating)
        return;
      this.m_WrapContent.WrapContent();
    }
  }

  private void OnTweenFinished()
  {
    this.m_Rotating = false;
    this.m_Center.Recenter();
  }

  private void ClearItemPackages()
  {
    using (List<UIToggle>.Enumerator enumerator = this.m_KnobList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UIToggle current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        Object.Destroy((Object) current.gameObject);
      }
    }
    this.m_KnobList.Clear();
    using (List<IAPStorePackagePanel>.Enumerator enumerator = this.m_ItemPackageList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IAPStorePackagePanel current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        Object.Destroy((Object) current.gameObject);
      }
    }
    this.m_ItemPackageList.Clear();
  }

  private void UpdateItemPackages()
  {
    this.StopRotator();
    this.ClearItemPackages();
    this.m_WrapContent.transform.localPosition = Vector3.zero;
    this.m_WrapContent.SortBasedOnScrollMovement();
    for (int index = 0; index < this.m_Packages.Count; ++index)
    {
      GameObject gameObject = Object.Instantiate<GameObject>(this.m_KnobPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_KnobGrid.transform;
      gameObject.transform.localScale = Vector3.one;
      this.m_KnobList.Add(gameObject.GetComponent<UIToggle>());
    }
    this.m_KnobGrid.Reposition();
    for (int index = 0; index < this.m_Packages.Count; ++index)
    {
      IAPStorePackage package = this.m_Packages[index];
      GameObject gameObject = Object.Instantiate<GameObject>(this.m_PackagePrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_WrapContent.transform;
      gameObject.transform.localScale = Vector3.one;
      IAPStorePackagePanel component = gameObject.GetComponent<IAPStorePackagePanel>();
      component.SetData(package);
      this.m_ItemPackageList.Add(component);
    }
    this.m_Center.onCenter = new UICenterOnChild.OnCenterCallback(this.OnCenter);
    this.m_PackageGrid.Reposition();
    this.m_PackageScrollView.ResetPosition();
  }

  private void OnCenter(GameObject go)
  {
    go.GetComponent<IAPStorePackagePanel>();
    int index = 0;
    this.m_PackageName.text = ScriptLocalization.Get(this.m_Packages[index].group.name, true);
    this.m_KnobList[index].Set(true);
  }

  private int GetSelectedIndex()
  {
    for (int index = 0; index < this.m_KnobList.Count; ++index)
    {
      if (this.m_KnobList[index].value)
        return index;
    }
    return -1;
  }
}
