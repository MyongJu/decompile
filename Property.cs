﻿// Decompiled with JetBrains decompiler
// Type: Property
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class Property
{
  public static readonly string HERO_HEALTH = "prop_hero_health";
  public static readonly string HERO_ATTACK = "prop_hero_attack";
  public static readonly string HERO_DEFENSE = "prop_hero_defense";
  public static readonly string HERO_EXECUTION_TIME = "prop_hero_execution_time";
  public static readonly string SPEEDUP_TIME = "prop_speedup_time";
  public static readonly string FOOD_GENERATION = "prop_food_generation";
  public static readonly string WOOD_GENERATION = "prop_wood_generation";
  public static readonly string ORE_GENERATION = "prop_ore_generation";
  public static readonly string SILVER_GENERATION = "prop_silver_generation";
  public static readonly string FOOD_GATHER = "prop_food_gather";
  public static readonly string WOOD_GATHER = "prop_wood_gather";
  public static readonly string ORE_GATHER = "prop_ore_gather";
  public static readonly string SILVER_GATHER = "prop_silver_gather";
  public static readonly string MARCH_CAPACITY = "prop_march_capacity";
  public static readonly string CONSTRUCTION_TIME = "prop_construction_time";
  public static readonly string RESEARCH_TIME = "prop_research_time";
  public static readonly string CRAFTING_TIME = "prop_crafting_time";
  public static readonly string XP_BONUS = "prop_hero_xp_bonus";
  public static readonly string XP_RETENTION = "prop_hero_xp_retention";
  public static readonly string INFANTRY_HEALTH = "prop_infantry_health";
  public static readonly string INFANTRY_ATTACK = "prop_infantry_attack";
  public static readonly string INFANTRY_DEFENSE = "prop_infantry_defense";
  public static readonly string INFANTRY_SPEED = "prop_infantry_speed";
  public static readonly string INFANTRY_LOAD = "prop_infantry_load";
  public static readonly string INFANTRY_UPKEEP = "prop_infantry_upkeep";
  public static readonly string INFANTRY_TRAINING = "prop_infantry_training_time";
  public static readonly string INFANTRY_HEALING = "prop_infantry_healing_time";
  public static readonly string CAVALRY_HEALTH = "prop_cavalry_health";
  public static readonly string CAVALRY_ATTACK = "prop_cavalry_attack";
  public static readonly string CAVALRY_DEFENSE = "prop_cavalry_defense";
  public static readonly string CAVALRY_SPEED = "prop_cavalry_speed";
  public static readonly string CAVALRY_LOAD = "prop_cavalry_load";
  public static readonly string CAVALRY_UPKEEP = "prop_cavalry_upkeep";
  public static readonly string CAVALRY_TRAINING = "prop_cavalry_training_time";
  public static readonly string CAVALRY_HEALING = "prop_cavalry_healing_time";
  public static readonly string RANGED_HEALTH = "prop_ranged_health";
  public static readonly string RANGED_ATTACK = "prop_ranged_attack";
  public static readonly string RANGED_DEFENSE = "prop_ranged_defense";
  public static readonly string RANGED_SPEED = "prop_ranged_speed";
  public static readonly string RANGED_LOAD = "prop_ranged_load";
  public static readonly string RANGED_UPKEEP = "prop_ranged_upkeep";
  public static readonly string RANGED_TRAINING = "prop_ranged_training_time";
  public static readonly string RANGED_HEALING = "prop_ranged_healing_time";
  public static readonly string ARTYCLOSE_HEALTH = "prop_artyclose_health";
  public static readonly string ARTYCLOSE_ATTACK = "prop_artyclose_attack";
  public static readonly string ARTYCLOSE_DEFENSE = "prop_artyclose_defense";
  public static readonly string ARTYCLOSE_SPEED = "prop_artyclose_speed";
  public static readonly string ARTYCLOSE_LOAD = "prop_artyclose_load";
  public static readonly string ARTYCLOSE_UPKEEP = "prop_artyclose_upkeep";
  public static readonly string ARTYCLOSE_TRAINING = "prop_artyclose_training_time";
  public static readonly string ARTYCLOSE_HEALING = "prop_artyclose_healing_time";
  public static readonly string ARTYFAR_HEALTH = "prop_artyfar_health";
  public static readonly string ARTYFAR_ATTACK = "prop_artyfar_attack";
  public static readonly string ARTYFAR_DEFENSE = "prop_artyfar_defense";
  public static readonly string ARTYFAR_SPEED = "prop_artyfar_speed";
  public static readonly string ARTYFAR_LOAD = "prop_artyfar_load";
  public static readonly string ARTYFAR_UPKEEP = "prop_artyfar_upkeep";
  public static readonly string ARTYFAR_TRAINING = "prop_artyfar_training_time";
  public static readonly string ARTYFAR_HEALING = "prop_artyfar_healing_time";
  public static readonly string TRAP1_HEALTH = "prop_trap1_health";
  public static readonly string TRAP1_ATTACK = "prop_trap1_attack";
  public static readonly string TRAP1_DEFENSE = "prop_trap1_defense";
  public static readonly string TRAP2_HEALTH = "prop_trap2_health";
  public static readonly string TRAP2_ATTACK = "prop_trap2_attack";
  public static readonly string TRAP2_DEFENSE = "prop_trap2_defense";
  public static readonly string TRAP3_HEALTH = "prop_trap3_health";
  public static readonly string TRAP3_ATTACK = "prop_trap3_attack";
  public static readonly string TRAP3_DEFENSE = "prop_trap3_defense";
  public static readonly string TRAP4_HEALTH = "prop_trap4_health";
  public static readonly string TRAP4_ATTACK = "prop_trap4_attack";
  public static readonly string TRAP4_DEFENSE = "prop_trap4_defense";
  public static readonly string QUEST_AUTOCOMPLETE_ALLIANCE = "prop_alliance_quest_autocomplete";
  public static readonly string QUEST_AUTOCOMPLETE_DAILY = "prop_daily_quest_autocomplete";
  public static readonly string QUEST_LIMIT_ALLIANCE = "prop_alliance_quest_limit";
  public static readonly string QUEST_LIMIT_DAILY = "prop_daily_quest_limit";
  public static readonly string MARCH_LIMIT = "prop_march_limit";
  public static readonly string VALUE = "_value";
  public static readonly string PERCENT = "_percent";
  public static readonly float MODIFIER_MIN = 1E-05f;

  public static float EffectiveBonus(string propertyID, int owner = 0, Effect.ConditionType conditions = Effect.ConditionType.all)
  {
    float num = PlayerData.inst.Properties.EffectiveBonus(propertyID, conditions);
    if (owner > 0)
    {
      HeroData heroData = PlayerData.inst.GetHeroData(owner);
      if (heroData != null)
        num += heroData.Properties.EffectiveBonus(propertyID, conditions);
    }
    return num;
  }

  public static float EffectiveValue(float baseValue, string propertyPrefix, int owner = 0, Effect.ConditionType conditions = Effect.ConditionType.all)
  {
    return (float) (((double) baseValue + (double) Property.EffectiveBonus(propertyPrefix + Property.VALUE, owner, conditions)) * (1.0 + (double) Property.EffectiveBonus(propertyPrefix + Property.PERCENT, owner, conditions)));
  }
}
