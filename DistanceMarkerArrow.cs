﻿// Decompiled with JetBrains decompiler
// Type: DistanceMarkerArrow
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class DistanceMarkerArrow : MonoBehaviour
{
  public UILabel m_Distance;

  public void SetDistance(float distance)
  {
    float num = 0.5f * PVPMapData.MapData.PixelsPerTile.x;
    this.m_Distance.text = ((int) ((double) distance / (double) num + 0.5)).ToString() + ScriptLocalization.Get("id_km", true);
  }

  public void OnGoHome()
  {
    PVPSystem.Instance.Map.CurrentKXY = PlayerData.inst.playerCityData.cityLocation;
    PVPSystem.Instance.Map.GotoLocation(PlayerData.inst.playerCityData.cityLocation, false);
  }
}
