﻿// Decompiled with JetBrains decompiler
// Type: ArtifactItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class ArtifactItem : MonoBehaviour
{
  [SerializeField]
  private GameObject _rootTagEquiped;
  [SerializeField]
  private UITexture _textureIcon;
  [SerializeField]
  private GameObject _rootTagHighlight;
  [SerializeField]
  private UILabel _labelName;
  protected ArtifactData _artifactData;
  public System.Action<ArtifactData> OnClicked;

  public ArtifactData ArtifactData
  {
    get
    {
      return this._artifactData;
    }
  }

  public void SetArtifactData(ArtifactData artifactData)
  {
    if (artifactData == null)
    {
      this.gameObject.SetActive(false);
    }
    else
    {
      this.gameObject.SetActive(true);
      this._artifactData = artifactData;
      ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this._artifactData.ArtifactId);
      if (artifactInfo != null)
      {
        BuilderFactory.Instance.HandyBuild((UIWidget) this._textureIcon, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
        if ((bool) ((UnityEngine.Object) this._labelName))
          this._labelName.text = artifactInfo.Name;
      }
      DB.HeroData heroData = PlayerData.inst.heroData;
      this._rootTagEquiped.SetActive(heroData != null && heroData.IsArtifactEquiped(this._artifactData.ArtifactId));
    }
  }

  public void SetHighlight(bool highLight)
  {
    this._rootTagHighlight.SetActive(highLight);
  }

  public void OnButtonClicked()
  {
    if (this.OnClicked == null)
      return;
    this.OnClicked(this._artifactData);
  }
}
