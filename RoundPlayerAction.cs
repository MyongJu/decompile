﻿// Decompiled with JetBrains decompiler
// Type: RoundPlayerAction
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class RoundPlayerAction
{
  private RoundPlayer _owner;
  private BTNode _root;
  private int _skillId;

  public RoundPlayerAction(RoundPlayer owner)
  {
    this._owner = owner;
    this.CreateBTNode();
  }

  public void Play()
  {
    this._root.Reset();
    this._root.Execute();
  }

  public int SkillId
  {
    get
    {
      return this._skillId;
    }
  }

  private void CreateBTNode()
  {
    BTNodeSelector btNodeSelector = new BTNodeSelector();
    btNodeSelector.NodeName = "Root";
    List<int> allSkills = this._owner.GetAllSkills();
    for (int index = 0; index < allSkills.Count; ++index)
    {
      BTNodeSequence btNodeSequence = new BTNodeSequence();
      btNodeSequence.NodeName = "SKILL_" + (object) allSkills[index];
      btNodeSequence.AddNode((BTNode) new BTNodeCondition(new BTNodeCondition.ConditionFunc(this.CheckTarget), (object) allSkills[index]));
      btNodeSequence.AddNode((BTNode) new BTNodeCondition(new BTNodeCondition.ConditionFunc(this.CheckMyHP), (object) allSkills[index]));
      btNodeSequence.AddNode((BTNode) new BTNodeCondition(new BTNodeCondition.ConditionFunc(this.CheckCD), (object) allSkills[index]));
      btNodeSequence.AddNode((BTNode) new BTNodeCondition(new BTNodeCondition.ConditionFunc(this.CheckMp), (object) allSkills[index]));
      btNodeSequence.AddNode((BTNode) new BTNodeAction(new System.Action<object>(this.ActionHandler), (object) allSkills[index]));
      btNodeSelector.AddNode((BTNode) btNodeSequence);
    }
    BTNodeAction btNodeAction = new BTNodeAction(new System.Action<object>(this.ActionHandler), (object) null);
    btNodeAction.NodeName = "NomalAttack";
    btNodeSelector.AddNode((BTNode) btNodeAction);
    this._root = (BTNode) btNodeSelector;
  }

  private void ActionHandler(object param)
  {
    this._skillId = 0;
    if (param == null)
      return;
    this._skillId = (int) param;
  }

  private bool CheckTarget(object param)
  {
    if (!ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo((int) param).IsSingleAttack())
      return BattleManager.Instance.TargetIsMultiple(this._owner);
    return true;
  }

  private bool CheckMyHP(object param)
  {
    return !ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo((int) param).AutoTriggerCheck() || this._owner.Health <= (int) ((double) this._owner.HealthMax * 0.5);
  }

  private bool CheckCD(object param)
  {
    return !this._owner.HasCd((int) param);
  }

  private bool CheckMp(object param)
  {
    return this._owner.CheckMp((int) param);
  }
}
