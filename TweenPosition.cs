﻿// Decompiled with JetBrains decompiler
// Type: TweenPosition
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[AddComponentMenu("NGUI/Tween/Tween Position")]
public class TweenPosition : UITweener
{
  public Vector3 from;
  public Vector3 to;
  [HideInInspector]
  public bool worldSpace;
  private Transform mTrans;
  private UIRect mRect;

  public Transform cachedTransform
  {
    get
    {
      if ((UnityEngine.Object) this.mTrans == (UnityEngine.Object) null)
        this.mTrans = this.transform;
      return this.mTrans;
    }
  }

  [Obsolete("Use 'value' instead")]
  public Vector3 position
  {
    get
    {
      return this.value;
    }
    set
    {
      this.value = value;
    }
  }

  public Vector3 value
  {
    get
    {
      if (this.worldSpace)
        return this.cachedTransform.position;
      return this.cachedTransform.localPosition;
    }
    set
    {
      if ((UnityEngine.Object) this.mRect == (UnityEngine.Object) null || !this.mRect.isAnchored || this.worldSpace)
      {
        if (this.worldSpace)
          this.cachedTransform.position = value;
        else
          this.cachedTransform.localPosition = value;
      }
      else
      {
        value -= this.cachedTransform.localPosition;
        NGUIMath.MoveRect(this.mRect, value.x, value.y);
      }
    }
  }

  private void Awake()
  {
    this.mRect = this.GetComponent<UIRect>();
  }

  protected override void OnUpdate(float factor, bool isFinished)
  {
    this.value = this.from * (1f - factor) + this.to * factor;
  }

  public static TweenPosition Begin(GameObject go, float duration, Vector3 pos)
  {
    TweenPosition tweenPosition = UITweener.Begin<TweenPosition>(go, duration);
    tweenPosition.from = tweenPosition.value;
    tweenPosition.to = pos;
    if ((double) duration <= 0.0)
    {
      tweenPosition.Sample(1f, true);
      tweenPosition.enabled = false;
    }
    return tweenPosition;
  }

  public static TweenPosition Begin(GameObject go, float duration, Vector3 pos, bool worldSpace)
  {
    TweenPosition tweenPosition = UITweener.Begin<TweenPosition>(go, duration);
    tweenPosition.worldSpace = worldSpace;
    tweenPosition.from = tweenPosition.value;
    tweenPosition.to = pos;
    if ((double) duration <= 0.0)
    {
      tweenPosition.Sample(1f, true);
      tweenPosition.enabled = false;
    }
    return tweenPosition;
  }

  [ContextMenu("Set 'From' to current value")]
  public override void SetStartToCurrentValue()
  {
    this.from = this.value;
  }

  [ContextMenu("Set 'To' to current value")]
  public override void SetEndToCurrentValue()
  {
    this.to = this.value;
  }

  [ContextMenu("Assume value of 'From'")]
  private void SetCurrentValueToStart()
  {
    this.value = this.from;
  }

  [ContextMenu("Assume value of 'To'")]
  private void SetCurrentValueToEnd()
  {
    this.value = this.to;
  }
}
