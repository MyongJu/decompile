﻿// Decompiled with JetBrains decompiler
// Type: HelpPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HelpPopup : Popup
{
  [SerializeField]
  private HelpPopup.Panel panel;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    HelpPopup.Parameter parameter = orgParam as HelpPopup.Parameter;
    if (parameter != null)
    {
      if (!string.IsNullOrEmpty(parameter.id))
      {
        HelpInfo data = ConfigManager.inst.DB_Help.GetData(parameter.id);
        if (data != null)
        {
          this.panel.title.text = Utils.XLAT(data.title);
          this.panel.description.text = parameter.descParam != null ? ScriptLocalization.GetWithPara(data.description, parameter.descParam, true) : Utils.XLAT(data.description);
        }
      }
      else
      {
        HelpInfo data = ConfigManager.inst.DB_Help.GetData(parameter.internalId);
        if (data != null)
        {
          this.panel.title.text = Utils.XLAT(data.title);
          this.panel.description.text = parameter.descParam != null ? ScriptLocalization.GetWithPara(data.description, parameter.descParam, true) : Utils.XLAT(data.description);
        }
      }
    }
    this.panel.scrollView.ResetPosition();
  }

  public void OnCloseClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string id = string.Empty;
    public int internalId;
    public Dictionary<string, string> descParam;

    public struct Description
    {
      public const string DragonSkill = "help_dragon_skill";
      public const string DragonAssignSkills = "help_dragon_skill";
      public const string WishingWell = "help_dragon_skill";
      public const string DailyRewards = "help_dragon_skill";
      public const string Hospital = "help_dragon_skill";
      public const string VIP = "help_dragon_skill";
      public const string AllianceStore = "help_dragon_skill";
      public const string AllianceStoreItemList = "help_dragon_skill";
      public const string LimitIntegralEvents1 = "help_dragon_skill";
      public const string LimitIntegralEvents2 = "help_dragon_skill";
      public const string LimitIntegralEvents3 = "help_dragon_skill";
      public const string BuildingInfoBouns = "help_dragon_skill";
      public const string AllianceOverview = "help_dragon_skill";
      public const string AllianceTech = "help_dragon_skill";
      public const string AlianceHelp = "help_dragon_skill";
      public const string AlianceReinforce = "help_dragon_skill";
      public const string AllianceWarRally = "help_dragon_skill";
      public const string AllianceTrading = "help_dragon_skill";
      public const string EquipmentSetEnhancement = "help_equipment_set_enhancement";
    }
  }

  [Serializable]
  protected class Panel
  {
    public UILabel title;
    public UILabel description;
    public UIScrollView scrollView;
  }
}
