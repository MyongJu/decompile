﻿// Decompiled with JetBrains decompiler
// Type: DragonAPI
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class DragonAPI
{
  public DragonController parent;
  private HubPort collectDragon;
  private HubPort rename;
  private HubPort claimlevelup;
  private HubPort upgradeSkill;
  private HubPort updateSkillColum;
  private HubPort resetSkill;

  public void Initialize()
  {
    this.collectDragon = new HubPort("city:collectTroop");
    this.rename = new HubPort("dragon:rename");
    this.upgradeSkill = new HubPort("dragon:upgradeSkill");
    this.updateSkillColum = new HubPort("dragon:updateSkillColumn");
    this.resetSkill = new HubPort("dragon:resetSkill");
    this.claimlevelup = new HubPort("dragon:updateNotifyLastLevel");
  }

  public void CollectDragon(long uid, long city_id, long building_id, System.Action<object> callBack = null)
  {
    this.collectDragon.SendRequest(new Hashtable()
    {
      {
        (object) nameof (uid),
        (object) uid
      },
      {
        (object) nameof (city_id),
        (object) city_id
      },
      {
        (object) nameof (building_id),
        (object) building_id
      }
    }, (System.Action<bool, object>) ((success, obj) =>
    {
      if (!success || callBack == null)
        return;
      callBack((object) (obj as Hashtable));
    }), true);
  }

  public void RenameDragon(long id, string name, System.Action<object> callBack = null)
  {
    this.rename.SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) id
      },
      {
        (object) nameof (name),
        (object) name
      }
    }, (System.Action<bool, object>) ((success, obj) =>
    {
      if (!success || callBack == null)
        return;
      callBack((object) (obj as Hashtable));
    }), true);
  }

  public void ClaimLevelUp(long id, int level)
  {
    this.claimlevelup.SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) id
      },
      {
        (object) nameof (level),
        (object) level
      }
    }, (System.Action<bool, object>) null, true);
  }

  public void UpgradeSkill(long uid, long skillID, System.Action<object> callBack = null)
  {
    this.upgradeSkill.SendRequest(new Hashtable()
    {
      {
        (object) nameof (uid),
        (object) uid
      },
      {
        (object) "skill_id",
        (object) skillID
      }
    }, (System.Action<bool, object>) ((success, obj) =>
    {
      if (!success || callBack == null)
        return;
      callBack((object) (obj as Hashtable));
    }), true);
  }

  public void UpgradeSkillColumn(long uid, int skillColumn, System.Action<object> callBack = null)
  {
    this.updateSkillColum.SendRequest(new Hashtable()
    {
      {
        (object) nameof (uid),
        (object) uid
      },
      {
        (object) "skill_column",
        (object) skillColumn
      }
    }, (System.Action<bool, object>) ((success, obj) =>
    {
      if (!success || callBack == null)
        return;
      callBack((object) (obj as Hashtable));
    }), true);
  }

  public void RetSkill(long id, System.Action<object> callBack = null)
  {
    this.resetSkill.SendRequest(new Hashtable()
    {
      {
        (object) nameof (id),
        (object) id
      }
    }, (System.Action<bool, object>) ((success, obj) =>
    {
      if (!success || callBack == null)
        return;
      callBack((object) (obj as Hashtable));
    }), true);
  }
}
