﻿// Decompiled with JetBrains decompiler
// Type: SignInData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class SignInData
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "times")]
  public int Times;
  [Config(Name = "reward_id_1")]
  public int RewardID1;
  [Config(Name = "reward_id_2")]
  public int RewardID2;
  [Config(Name = "reward_id_3")]
  public int RewardID3;
  [Config(Name = "reward_id_4")]
  public int RewardID4;
  [Config(Name = "image")]
  public string ImageIcon;

  private int GetRewarId(int index)
  {
    switch (index)
    {
      case 0:
        return this.RewardID1;
      case 1:
        return this.RewardID2;
      case 2:
        return this.RewardID3;
      case 3:
        return this.RewardID4;
      default:
        return 0;
    }
  }

  private int GetRewardCount()
  {
    return 4;
  }

  public List<DropMainData> GetRewardList()
  {
    List<DropMainData> dropMainDataList = new List<DropMainData>();
    for (int index = 0; index < this.GetRewardCount(); ++index)
    {
      List<DropMainData> listByDropGroupId = ConfigManager.inst.DB_DropMain.GetDropMainDataListByDropGroupId(this.GetRewarId(index));
      dropMainDataList.AddRange((IEnumerable<DropMainData>) listByDropGroupId);
    }
    return dropMainDataList;
  }
}
