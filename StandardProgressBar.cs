﻿// Decompiled with JetBrains decompiler
// Type: StandardProgressBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StandardProgressBar : MonoBehaviour
{
  protected ProgressBarTweenProxy proxy = new ProgressBarTweenProxy();
  protected int currentCount = -1;
  protected bool autoFix = true;
  public UIButton plusBtn;
  public UIButton minusBtn;
  public UISlider bar;
  public UIInput input;
  public System.Action<int> onValueChanged;
  private StandardProgressBar.ChangeSource mChangeSource;
  protected int maxCount;

  public int CurrentCount
  {
    get
    {
      return this.currentCount;
    }
    set
    {
      if (this.currentCount == value)
        return;
      if (value > this.maxCount)
        value = this.maxCount;
      if (value < 0)
        value = 0;
      this.currentCount = value;
      if (this.mChangeSource != StandardProgressBar.ChangeSource.Slide)
        this.UpdateBarUI(this.CurrentCount);
      if (this.mChangeSource != StandardProgressBar.ChangeSource.Input)
        this.UpdateInputUI(this.CurrentCount);
      if (this.onValueChanged == null)
        return;
      this.onValueChanged(this.currentCount);
    }
  }

  public virtual void Init(int initcount, int maxcount)
  {
    this.maxCount = maxcount;
    if (this.maxCount == 0)
    {
      this.Enabled(false);
      this.maxCount = 1;
    }
    else
      this.Enabled(true);
    this.CurrentCount = initcount;
  }

  public void Enabled(bool sts)
  {
    this.plusBtn.isEnabled = sts;
    this.minusBtn.isEnabled = sts;
    this.bar.enabled = sts;
    this.input.enabled = sts;
  }

  protected virtual void UpdateBarUI(int count)
  {
    if (this.maxCount == 0)
      this.bar.value = 1f;
    else
      this.bar.value = (float) count / (float) this.maxCount;
  }

  protected void UpdateInputUI(int count)
  {
    this.input.value = count.ToString();
  }

  public void OnPlusBtnPressed()
  {
  }

  public void OnMinusBtnPressed()
  {
  }

  public void OnIncreaseButtonDown()
  {
    this.autoFix = false;
    this.proxy.MaxSize = this.maxCount;
    this.proxy.Slider = (UIProgressBar) this.bar;
    this.proxy.Play(1, -1f);
  }

  public void OnIncreaseButtonUp()
  {
    this.autoFix = true;
    this.proxy.Stop();
  }

  public void OnDecreaseButtonDown()
  {
    this.autoFix = false;
    this.proxy.MaxSize = this.maxCount;
    this.proxy.Slider = (UIProgressBar) this.bar;
    this.proxy.Play(-1, -1f);
  }

  public void OnDecreaseButtonUp()
  {
    this.autoFix = true;
    this.proxy.Stop();
  }

  public void OnBarChange()
  {
    if (this.mChangeSource == StandardProgressBar.ChangeSource.None)
      this.mChangeSource = StandardProgressBar.ChangeSource.Slide;
    this.CurrentCount = Mathf.FloorToInt(this.bar.value * (float) this.maxCount);
    if (this.autoFix)
      this.UpdateBarUI(this.CurrentCount);
    if (this.mChangeSource != StandardProgressBar.ChangeSource.Slide)
      return;
    this.mChangeSource = StandardProgressBar.ChangeSource.None;
  }

  public void OnInputChange()
  {
    int result;
    bool flag = int.TryParse(this.input.value, out result);
    if (this.mChangeSource == StandardProgressBar.ChangeSource.None)
      this.mChangeSource = StandardProgressBar.ChangeSource.Input;
    this.CurrentCount = result;
    if (this.mChangeSource == StandardProgressBar.ChangeSource.Input)
      this.mChangeSource = StandardProgressBar.ChangeSource.None;
    if (flag)
      return;
    this.input.value = this.CurrentCount.ToString();
  }

  private enum ChangeSource
  {
    None,
    Slide,
    Input,
  }
}
