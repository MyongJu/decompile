﻿// Decompiled with JetBrains decompiler
// Type: ArtifactDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ArtifactDlg : UI.Dialog
{
  private Dictionary<int, ArtifactSlotItemRenderer> itemDict = new Dictionary<int, ArtifactSlotItemRenderer>();
  private GameObjectPool itemPool = new GameObjectPool();
  private List<ArtifactDlg.ArtifactItemInfo> _artifactInfoList = new List<ArtifactDlg.ArtifactItemInfo>();
  private List<GameObject> _miniMapArtifacts = new List<GameObject>();
  private const float MAP_COORDINATE_LENGTH_X = 1279f;
  private const float MAP_COORDINATE_LENGTH_Y = 2559f;
  public UITexture artifactPrefab;
  public UITexture miniMapTexture;
  public UIScrollView scrollView;
  public UIGrid grid;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  private ArrayList _artifactData;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    ArtifactDlg.Parameter parameter = orgParam as ArtifactDlg.Parameter;
    if (parameter != null)
      this._artifactData = parameter.artifactData;
    this.itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.Decode(this._artifactData);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  private void UpdateUI()
  {
    this.ShowArtifactContent();
    this.ShowArtifactMapContent();
  }

  private void ShowArtifactContent()
  {
    this.ClearData();
    if (this._artifactInfoList != null)
    {
      for (int key = 0; key < this._artifactInfoList.Count; ++key)
      {
        ArtifactSlotItemRenderer itemRenderer = this.CreateItemRenderer(this._artifactInfoList[key]);
        this.itemDict.Add(key, itemRenderer);
      }
    }
    this.Reposition();
  }

  private void ShowArtifactMapContent()
  {
    if (this._artifactInfoList == null || this._artifactInfoList.Count <= 0)
      return;
    int width = this.miniMapTexture.width;
    int height = this.miniMapTexture.height;
    float x1 = this.artifactPrefab.transform.localPosition.x;
    float y1 = this.artifactPrefab.transform.localPosition.y;
    float num1 = 0.0f;
    float num2 = 0.0f;
    for (int index = 0; index < this._artifactInfoList.Count; ++index)
    {
      if (this._artifactInfoList[index].ownerUid > 0L && this._artifactInfoList[index].mapX >= 0 && this._artifactInfoList[index].mapY >= 0)
      {
        GameObject gameObject = Utils.DuplicateGOB(this.artifactPrefab.gameObject, this.artifactPrefab.transform.parent);
        if ((UnityEngine.Object) gameObject != (UnityEngine.Object) null)
        {
          UITexture component = gameObject.GetComponent<UITexture>();
          if ((UnityEngine.Object) component != (UnityEngine.Object) null)
          {
            BuilderFactory.Instance.HandyBuild((UIWidget) component, this._artifactInfoList[index].ArtifactImagePath, (System.Action<bool>) null, true, false, string.Empty);
            num1 = (float) component.width;
            num2 = (float) component.height;
          }
          float num3 = (float) this._artifactInfoList[index].mapX / 1279f;
          float num4 = (float) this._artifactInfoList[index].mapY / 2559f;
          float num5 = num3 * (float) width;
          float num6 = num4 * (float) height;
          float x2 = x1 + num5;
          float y2 = y1 - num6;
          if ((double) num5 > (double) num1)
            x2 -= num1;
          if ((double) num6 > (double) num1)
            y2 += num2;
          gameObject.transform.localPosition = new Vector3(x2, y2, 0.0f);
          gameObject.SetActive(true);
          this._miniMapArtifacts.Add(gameObject);
        }
      }
    }
  }

  private void Decode(ArrayList data)
  {
    this._artifactInfoList.Clear();
    if (data == null)
      return;
    for (int index = 0; index < data.Count; ++index)
    {
      Hashtable inData = data[index] as Hashtable;
      if (inData != null)
      {
        int outData1 = 0;
        long outData2 = 0;
        string empty = string.Empty;
        int outData3 = -1;
        int outData4 = -1;
        int outData5 = -1;
        DatabaseTools.UpdateData(inData, "artifact_id", ref outData1);
        DatabaseTools.UpdateData(inData, "owner_uid", ref outData2);
        DatabaseTools.UpdateData(inData, "username", ref empty);
        DatabaseTools.UpdateData(inData, "map_x", ref outData3);
        DatabaseTools.UpdateData(inData, "map_y", ref outData4);
        DatabaseTools.UpdateData(inData, "current_world_id", ref outData5);
        this._artifactInfoList.Add(new ArtifactDlg.ArtifactItemInfo(outData1, outData2, empty, outData3, outData4, outData5));
      }
    }
  }

  private void ClearData()
  {
    using (Dictionary<int, ArtifactSlotItemRenderer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this.itemPool.Release(gameObject);
      }
    }
    this.itemDict.Clear();
    this.itemPool.Clear();
    this._miniMapArtifacts.ForEach((System.Action<GameObject>) (x => UnityEngine.Object.Destroy((UnityEngine.Object) x)));
    this._miniMapArtifacts.Clear();
  }

  private void Reposition()
  {
    this.grid.repositionNow = true;
    this.grid.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private ArtifactSlotItemRenderer CreateItemRenderer(ArtifactDlg.ArtifactItemInfo artifactItemInfo)
  {
    GameObject gameObject = this.itemPool.AddChild(this.grid.gameObject);
    gameObject.SetActive(true);
    ArtifactSlotItemRenderer component = gameObject.GetComponent<ArtifactSlotItemRenderer>();
    component.SetData(artifactItemInfo);
    return component;
  }

  public class ArtifactItemInfo
  {
    public bool isInHomeKingdom = true;
    public int artifactId;
    public long ownerUid;
    public string ownerName;
    public int mapX;
    public int mapY;

    public ArtifactItemInfo(int artifactId, long ownerUid, string ownerName, int mapX, int mapY, int currentKingdom)
    {
      this.artifactId = artifactId;
      this.ownerUid = ownerUid;
      this.ownerName = ownerName;
      this.mapX = mapX;
      this.mapY = mapY;
      this.isInHomeKingdom = currentKingdom < 0 || currentKingdom == PlayerData.inst.userData.world_id;
    }

    public string ArtifactImagePath
    {
      get
      {
        if (this.artifactId > 0)
        {
          ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this.artifactId);
          if (artifactInfo != null)
            return artifactInfo.ImagePath;
        }
        return string.Empty;
      }
    }
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public ArrayList artifactData;
  }
}
