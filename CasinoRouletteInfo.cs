﻿// Decompiled with JetBrains decompiler
// Type: CasinoRouletteInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class CasinoRouletteInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "lvl_min")]
  public int minLevel;
  [Config(Name = "group_id")]
  public int groupId;
  [Config(Name = "weight")]
  public int weight;
  [Config(Name = "type")]
  public string type;
  [Config(Name = "reward")]
  public string reward;
  [Config(Name = "value")]
  public int value;
}
