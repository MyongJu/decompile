﻿// Decompiled with JetBrains decompiler
// Type: ConfigIAP_DailyPackageSubRewards
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigIAP_DailyPackageSubRewards
{
  private Dictionary<string, IAPDailyPackageSubRewardsInfo> m_DataByID;
  private Dictionary<int, IAPDailyPackageSubRewardsInfo> m_DataByInternalID;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<IAPDailyPackageSubRewardsInfo, string>(result as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
  }

  public IAPDailyPackageSubRewardsInfo Get(string id)
  {
    IAPDailyPackageSubRewardsInfo packageSubRewardsInfo;
    this.m_DataByID.TryGetValue(id, out packageSubRewardsInfo);
    return packageSubRewardsInfo;
  }

  public IAPDailyPackageSubRewardsInfo Get(int internalId)
  {
    IAPDailyPackageSubRewardsInfo packageSubRewardsInfo;
    this.m_DataByInternalID.TryGetValue(internalId, out packageSubRewardsInfo);
    return packageSubRewardsInfo;
  }

  public void Clear()
  {
    if (this.m_DataByID != null)
    {
      this.m_DataByID.Clear();
      this.m_DataByID = (Dictionary<string, IAPDailyPackageSubRewardsInfo>) null;
    }
    if (this.m_DataByInternalID == null)
      return;
    this.m_DataByInternalID.Clear();
    this.m_DataByInternalID = (Dictionary<int, IAPDailyPackageSubRewardsInfo>) null;
  }
}
