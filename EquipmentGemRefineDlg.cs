﻿// Decompiled with JetBrains decompiler
// Type: EquipmentGemRefineDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using GemConstant;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class EquipmentGemRefineDlg : UI.Dialog
{
  public GemEnhanceTopAreaInfo _TopInfo;
  public GameObject _NoGemHint;
  public GemEnhanceComponent _ItemComponentTemplate;
  public GemEnhanceItemSlideComponent _ItemSlideTemplate;
  public UIScrollView _ScrollView;
  public UITable _Container;
  public UITable _Table;
  public UIGrid _Grid;
  public ParticleSystem _ParticleSystem;
  public UILabel title;
  public UILabel tip;
  private long gemId;
  private SlotType slotType;
  private GemData mainGemData;
  private ConfigEquipmentGemInfo gemConfig;
  private List<GemData> edibleGemList;
  private List<GemData> selectedGemList;
  private List<GemEnhanceItemSlideComponent> gemSliderList;
  private List<GemEnhanceComponent> gemComponentList;
  private bool isDirty;

  private void Init()
  {
    this.mainGemData = DBManager.inst.DB_Gems.Get(this.gemId);
    if (this.mainGemData == null)
      return;
    this.gemSliderList = new List<GemEnhanceItemSlideComponent>();
    this.gemComponentList = new List<GemEnhanceComponent>();
    this.selectedGemList = new List<GemData>();
    DBManager.inst.DB_Gems.onDataChanged += new System.Action<long, long>(this.OnGemDataChanged);
    DBManager.inst.DB_Gems.onDataRemoved += new System.Action<long, long>(this.OnGemDataChanged);
  }

  private void UpdateGemDatas()
  {
    this.gemConfig = ConfigManager.inst.DB_EquipmentGem.GetData(this.mainGemData.ConfigId);
    if (this.gemConfig == null)
      return;
    this.edibleGemList = DBManager.inst.DB_Gems.GetEdibleGems(this.slotType, this.gemId);
    this.edibleGemList.Sort(new Comparison<GemData>(this.SortRule));
  }

  private int SortRule(GemData x, GemData y)
  {
    if (x.GemType == GemType.EXP && y.GemType != GemType.EXP)
      return -1;
    if (x.GemType != GemType.EXP && y.GemType == GemType.EXP)
      return 1;
    ConfigEquipmentGemInfo data1 = ConfigManager.inst.DB_EquipmentGem.GetData(x.ConfigId);
    if (data1 == null)
      return 0;
    ConfigEquipmentGemInfo data2 = ConfigManager.inst.DB_EquipmentGem.GetData(y.ConfigId);
    if (data2 == null)
      return 0;
    long num1 = data1.expContain + x.Exp;
    long num2 = data2.expContain + y.Exp;
    if (x.GemType == GemType.EXP && y.GemType == GemType.EXP)
      return -data1.expContain.CompareTo(data2.expContain);
    if (num1 == num2)
      return x.ConfigId.CompareTo(y.ConfigId);
    return num1.CompareTo(num2);
  }

  private void UpdateUI()
  {
    this.UpdateGemDatas();
    this._TopInfo.Init(this.mainGemData);
    NGUITools.SetActive(this._NoGemHint, this.edibleGemList.Count == 0);
    List<GemDataWrapper> gemDataWrapper = GemManager.Instance.GetGemDataWrapper(this.edibleGemList);
    using (List<GemEnhanceItemSlideComponent>.Enumerator enumerator = this.gemSliderList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        NGUITools.SetActive(enumerator.Current.gameObject, false);
    }
    using (List<GemEnhanceComponent>.Enumerator enumerator = this.gemComponentList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        NGUITools.SetActive(enumerator.Current.gameObject, false);
    }
    int index1 = 0;
    int index2 = 0;
    using (List<GemDataWrapper>.Enumerator enumerator = gemDataWrapper.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GemDataWrapper current = enumerator.Current;
        if (current.GemCount > 1)
        {
          GemEnhanceItemSlideComponent itemSlideComponent;
          if (index1 >= this.gemSliderList.Count)
          {
            itemSlideComponent = NGUITools.AddChild(this._Table.gameObject, this._ItemSlideTemplate.gameObject).GetComponent<GemEnhanceItemSlideComponent>();
            this.gemSliderList.Add(itemSlideComponent);
          }
          else
            itemSlideComponent = this.gemSliderList[index1];
          ++index1;
          NGUITools.SetActive(itemSlideComponent.gameObject, true);
          itemSlideComponent.FeedData((IComponentData) new GemEnhanceItemSlideComponentData()
          {
            gemDataWrapper = current
          });
          itemSlideComponent.OnSliderValueChangedHandler = new System.Action<int>(this.OnSliderChanged);
          itemSlideComponent.reachMaxCounter = new GemEnhanceItemSlideComponent.GetReachMaxItemCount(this.GetReachMaxItemCount);
        }
        else
        {
          GemEnhanceComponent enhanceComponent;
          if (index2 >= this.gemComponentList.Count)
          {
            enhanceComponent = NGUITools.AddChild(this._Grid.gameObject, this._ItemComponentTemplate.gameObject).GetComponent<GemEnhanceComponent>();
            this.gemComponentList.Add(enhanceComponent);
          }
          else
            enhanceComponent = this.gemComponentList[index2];
          ++index2;
          NGUITools.SetActive(enhanceComponent.gameObject, true);
          enhanceComponent.FeedData((IComponentData) new GemEnhanceComponentData()
          {
            gemData = current.GemData
          });
          enhanceComponent.Select = false;
          enhanceComponent.OnToggleChangedHandler = new System.Action<GemEnhanceComponent>(this.OnToggleChanged);
        }
      }
    }
    this.Reposition();
  }

  private int GetReachMaxItemCount(ConfigEquipmentGemInfo gemConfig)
  {
    long num = 0;
    using (List<GemEnhanceItemSlideComponent>.Enumerator enumerator = this.gemSliderList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GemEnhanceItemSlideComponent current = enumerator.Current;
        if (current.gameObject.activeSelf && current.GemConfig.internalId != gemConfig.internalId)
          num += current.GemConfig.expContain * (long) current.SelectedCount;
      }
    }
    return Mathf.Max(0, Mathf.CeilToInt((float) (GemManager.Instance.GetExpNeedToMaxLevel(this.mainGemData) - num) / (float) gemConfig.expContain));
  }

  private void Reposition()
  {
    this._Table.Reposition();
    this._Grid.Reposition();
    this._Container.Reposition();
  }

  private void OnSliderChanged(int count)
  {
    this.UpdateTopInfo();
  }

  private long SliderTotalExp
  {
    get
    {
      long num = 0;
      using (List<GemEnhanceItemSlideComponent>.Enumerator enumerator = this.gemSliderList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GemEnhanceItemSlideComponent current = enumerator.Current;
          if (current.gameObject.activeSelf && current.GemConfig != null)
            num += current.GemConfig.expContain * (long) current.SelectedCount;
        }
      }
      return num;
    }
  }

  private void OnToggleChanged(GemEnhanceComponent selected)
  {
    GemData gemData = selected.gemData;
    if (selected.Select)
    {
      if (GemManager.Instance.WillGemReachMaxLevel(this.mainGemData, GemManager.Instance.GetGemListTotalExp(this.selectedGemList) + this.SliderTotalExp))
      {
        UIManager.inst.toast.Show(ScriptLocalization.Get("toast_forge_gemstone_experience_max_reached", true), (System.Action) null, 4f, false);
        selected.Select = false;
        return;
      }
      if (!this.selectedGemList.Contains(gemData))
        this.selectedGemList.Add(gemData);
    }
    else if (this.selectedGemList.Contains(gemData))
      this.selectedGemList.Remove(gemData);
    this.UpdateTopInfo();
  }

  private void UpdateTopInfo()
  {
    this._TopInfo.UpdateUI(GemManager.Instance.GetGemListTotalExp(this.selectedGemList) + this.SliderTotalExp);
  }

  private void OnGemDataChanged(long uid, long gemId)
  {
    this.isDirty = true;
  }

  private void Update()
  {
    if (!this.isDirty)
      return;
    this.UpdateUI();
    this._ScrollView.ResetPosition();
    this.isDirty = false;
  }

  private List<KeyValuePair<long, long>> SelectedGemIds
  {
    get
    {
      List<KeyValuePair<long, long>> keyValuePairList = new List<KeyValuePair<long, long>>();
      using (List<GemEnhanceItemSlideComponent>.Enumerator enumerator = this.gemSliderList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GemEnhanceItemSlideComponent current = enumerator.Current;
          if (current.gameObject.activeSelf && current.GemConfig != null)
          {
            List<KeyValuePair<long, long>> someExpGems = DBManager.inst.DB_Gems.GetSomeExpGems(current.GemConfig, (long) current.SelectedCount);
            keyValuePairList.AddRange((IEnumerable<KeyValuePair<long, long>>) someExpGems);
          }
        }
      }
      if (this.selectedGemList != null)
      {
        using (List<GemData>.Enumerator enumerator = this.selectedGemList.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            GemData current = enumerator.Current;
            keyValuePairList.Add(new KeyValuePair<long, long>(current.ID, 1L));
          }
        }
      }
      return keyValuePairList;
    }
  }

  public void OnEnhanceConfirmClick()
  {
    List<KeyValuePair<long, long>> selectedGemIds = this.SelectedGemIds;
    if (selectedGemIds.Count == 0)
    {
      string Term = "toast_forge_gemstone_select_gemstone";
      if (this.slotType == SlotType.dragon_knight)
        Term = "toast_forge_armory_dk_select_gemstone";
      UIManager.inst.toast.Show(ScriptLocalization.Get(Term, true), (System.Action) null, 4f, false);
    }
    else
      UIManager.inst.OpenPopup("Gem/EquipmentGemEnhanceConfirmPopup", (Popup.PopupParameter) new GemEnhanceConfirmPopup.Parameter()
      {
        mainGemId = this.gemId,
        ateGems = selectedGemIds,
        onRequestFinished = new System.Action(this.OnRequestFinished)
      });
  }

  private void OnRequestFinished()
  {
    this.isDirty = true;
    this.selectedGemList.Clear();
    this.PlayEatGemsEffect();
  }

  private void PlayEatGemsEffect()
  {
    if (!((UnityEngine.Object) this._ParticleSystem != (UnityEngine.Object) null))
      return;
    this._ParticleSystem.Clear();
    this._ParticleSystem.Play(true);
  }

  private void Dispose()
  {
    this._TopInfo.Dispose();
    DBManager.inst.DB_Gems.onDataChanged -= new System.Action<long, long>(this.OnGemDataChanged);
    DBManager.inst.DB_Gems.onDataRemoved -= new System.Action<long, long>(this.OnGemDataChanged);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.gemId = (orgParam as EquipmentGemRefineDlg.Parameter).gemId;
    this.slotType = (orgParam as EquipmentGemRefineDlg.Parameter).slotType;
    string source = "forge_armory_gemstone_refine_title";
    if (this.slotType == SlotType.dragon_knight)
      source = "forge_armory_dk_runestone_refine_title";
    this.title.text = Utils.XLAT(source);
    this.Init();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.Dispose();
    base.OnClose(orgParam);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public long gemId;
    public SlotType slotType;
  }
}
