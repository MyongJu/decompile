﻿// Decompiled with JetBrains decompiler
// Type: BuffTipItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class BuffTipItem : MonoBehaviour
{
  [SerializeField]
  private float _fadeInTime = 0.5f;
  [SerializeField]
  private float _fadeOutTime = 0.5f;
  [SerializeField]
  private float _showTime = 1f;
  [SerializeField]
  private UIPanel _panelRoot;
  [SerializeField]
  private UITexture _textureBuffIcon;
  [SerializeField]
  private UILabel _labelBuffName;
  [SerializeField]
  private UITable _tableContainer;

  public void Play(string iconPath, string buffName)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this._textureBuffIcon, iconPath, (System.Action<bool>) null, true, false, string.Empty);
    this._labelBuffName.text = buffName;
    this._tableContainer.repositionNow = true;
    this._tableContainer.Reposition();
    this.StopAllCoroutines();
    this.StartCoroutine(this.AnimationCoroutine());
  }

  [DebuggerHidden]
  private IEnumerator AnimationCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BuffTipItem.\u003CAnimationCoroutine\u003Ec__Iterator55()
    {
      \u003C\u003Ef__this = this
    };
  }
}
