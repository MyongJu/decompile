﻿// Decompiled with JetBrains decompiler
// Type: MazeRoomAnimation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class MazeRoomAnimation : MonoBehaviour
{
  private const float ANIM_DURATION = 0.3f;
  private const float ORIGION_Y = 472f;
  private const float HEIGHT = 966f;
  [SerializeField]
  private MazeRoomItem _sourceRoomItem;
  [SerializeField]
  private MazeRoomItem _targetRoomItem;
  private string _sourceRoomResource;
  private string _targetRoomResource;

  public void Start()
  {
  }

  private void CreateRoomItem(Room room, ref string currentResource, ref MazeRoomItem roomItem)
  {
    DungeonMainStaticInfo dungeonMainStaticInfo = ConfigManager.inst.DB_DungeonMain.Get(room.FloorInternalId);
    if (!((UnityEngine.Object) roomItem == (UnityEngine.Object) null) && !(currentResource != dungeonMainStaticInfo.RoomBackgroundPath))
      return;
    currentResource = dungeonMainStaticInfo.RoomBackgroundPath;
    if ((bool) ((UnityEngine.Object) roomItem))
      UnityEngine.Object.Destroy((UnityEngine.Object) roomItem.gameObject);
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load(currentResource, (System.Type) null)) as GameObject;
    AssetManager.Instance.UnLoadAsset(currentResource, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    gameObject.transform.SetParent(this.transform);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    roomItem = gameObject.GetComponent<MazeRoomItem>();
    roomItem.FitHorizonal();
  }

  public void SetCurrentRoomData(Room room)
  {
    this.CreateRoomItem(room, ref this._targetRoomResource, ref this._targetRoomItem);
    this._targetRoomItem.SetRoom(room);
  }

  public void PlayAnimation(Room source, Room target, System.Action callback = null, MazeRoomAnimation.AnimationMode animationMode = MazeRoomAnimation.AnimationMode.Scroll)
  {
    this.CreateRoomItem(source, ref this._sourceRoomResource, ref this._sourceRoomItem);
    this.CreateRoomItem(target, ref this._targetRoomResource, ref this._targetRoomItem);
    this._sourceRoomItem.SetRoom(source);
    this._targetRoomItem.SetRoom(target);
    switch (animationMode)
    {
      case MazeRoomAnimation.AnimationMode.Scroll:
        if (source.Floor < target.Floor)
        {
          AudioManager.Instance.PlaySound("sfx_dragon_knight_floor_exit", false);
          this.PlayDownAnimation(source, target);
          break;
        }
        if (source.RoomX < target.RoomX)
        {
          AudioManager.Instance.PlaySound("sfx_dragon_knight_move", false);
          this.PlayRightAnimation(source, target);
          break;
        }
        if (source.RoomX > target.RoomX)
        {
          AudioManager.Instance.PlaySound("sfx_dragon_knight_move", false);
          this.PlayLeftAnimation(source, target);
          break;
        }
        if (source.RoomY < target.RoomY)
        {
          AudioManager.Instance.PlaySound("sfx_dragon_knight_move", false);
          this.PlayForwardAnimation(source, target);
          break;
        }
        if (source.RoomY > target.RoomY)
        {
          AudioManager.Instance.PlaySound("sfx_dragon_knight_move", false);
          this.PlayBackwardAnimation(source, target);
          break;
        }
        break;
      case MazeRoomAnimation.AnimationMode.FadeOutIn:
        this.StartCoroutine(this.PlayFadeOutInAnimation(source, target));
        break;
    }
    this.StartCoroutine(this.AnimationCallbackCoroutine(callback));
  }

  [DebuggerHidden]
  private IEnumerator AnimationCallbackCoroutine(System.Action callback)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MazeRoomAnimation.\u003CAnimationCallbackCoroutine\u003Ec__Iterator58()
    {
      callback = callback,
      \u003C\u0024\u003Ecallback = callback
    };
  }

  public Transform GetRoomGroundTransform()
  {
    if ((bool) ((UnityEngine.Object) this._targetRoomItem))
      return this._targetRoomItem.GetShakeRoot();
    return (Transform) null;
  }

  public void ShakeRoom()
  {
    if (!(bool) ((UnityEngine.Object) this._targetRoomItem))
      return;
    this._targetRoomItem.Shake();
  }

  public void SmallShakeRoom()
  {
    if (!(bool) ((UnityEngine.Object) this._targetRoomItem))
      return;
    this._targetRoomItem.SmallShakeRoom();
  }

  public MazeRoomItem GetTargetRoomItem()
  {
    return this._targetRoomItem;
  }

  public void SetSourceRoomEnabled(bool enabled)
  {
    if (!(bool) ((UnityEngine.Object) this._sourceRoomItem))
      return;
    this._sourceRoomItem.gameObject.SetActive(enabled);
  }

  [DebuggerHidden]
  protected IEnumerator PlayFadeOutInAnimation(Room source, Room target)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MazeRoomAnimation.\u003CPlayFadeOutInAnimation\u003Ec__Iterator59()
    {
      \u003C\u003Ef__this = this
    };
  }

  protected void PlayForwardAnimation(Room source, Room target)
  {
    this.SetSourceRoomEnabled(true);
    this._sourceRoomItem.SetPosition(0.0f, 472f);
    this._sourceRoomItem.AlphaTo(1f, 0.0f, 0.3f);
    this._sourceRoomItem.ScaleTo(1f, 1.4f, 0.3f);
    this._targetRoomItem.SetPosition(0.0f, 472f);
    this._targetRoomItem.AlphaTo(0.0f, 1f, 0.3f);
    this._targetRoomItem.ScaleTo(0.6f, 1f, 0.3f);
  }

  protected void PlayBackwardAnimation(Room source, Room target)
  {
    this.SetSourceRoomEnabled(true);
    this._sourceRoomItem.SetPosition(0.0f, 472f);
    this._sourceRoomItem.AlphaTo(1f, 0.0f, 0.3f);
    this._sourceRoomItem.ScaleTo(1f, 0.6f, 0.3f);
    this._targetRoomItem.SetPosition(0.0f, 472f);
    this._targetRoomItem.AlphaTo(0.0f, 1f, 0.3f);
    this._targetRoomItem.ScaleTo(1.4f, 1f, 0.3f);
  }

  protected void PlayLeftAnimation(Room source, Room target)
  {
    this.SetSourceRoomEnabled(true);
    this._sourceRoomItem.MoveTo(0.0f, 472f, this._sourceRoomItem.RoomXRange, 472f, 0.3f);
    this._sourceRoomItem.SetAlpha(1f);
    this._sourceRoomItem.SetScale(1f);
    this._targetRoomItem.MoveTo(-this._targetRoomItem.RoomXRange, 472f, 0.0f, 472f, 0.3f);
    this._targetRoomItem.SetAlpha(1f);
    this._targetRoomItem.SetScale(1f);
  }

  protected void PlayRightAnimation(Room source, Room target)
  {
    this.SetSourceRoomEnabled(true);
    this._sourceRoomItem.MoveTo(0.0f, 472f, -this._sourceRoomItem.RoomXRange, 472f, 0.3f);
    this._sourceRoomItem.SetAlpha(1f);
    this._sourceRoomItem.SetScale(1f);
    this._targetRoomItem.MoveTo(this._targetRoomItem.RoomXRange, 472f, 0.0f, 472f, 0.3f);
    this._targetRoomItem.SetAlpha(1f);
    this._targetRoomItem.SetScale(1f);
  }

  protected void PlayDownAnimation(Room source, Room target)
  {
    this.SetSourceRoomEnabled(true);
    this._sourceRoomItem.MoveTo(0.0f, 472f, 0.0f, 1438f, 0.3f);
    this._sourceRoomItem.SetAlpha(1f);
    this._sourceRoomItem.SetScale(1f);
    this._targetRoomItem.MoveTo(0.0f, -494f, 0.0f, 472f, 0.3f);
    this._targetRoomItem.SetAlpha(1f);
    this._targetRoomItem.SetScale(1f);
  }

  public enum AnimationMode
  {
    Scroll,
    FadeOutIn,
  }
}
