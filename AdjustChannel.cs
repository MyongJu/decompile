﻿// Decompiled with JetBrains decompiler
// Type: AdjustChannel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class AdjustChannel : NotificationChannel
{
  public override void Execute(Hashtable source)
  {
    if (source == null || source.Keys.Count == 0 || (!source.ContainsKey((object) "label") || source[(object) "label"] == null))
      return;
    AccountManager.Instance.AddGift("label", source[(object) "label"].ToString());
  }
}
