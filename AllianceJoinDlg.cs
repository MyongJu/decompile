﻿// Decompiled with JetBrains decompiler
// Type: AllianceJoinDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceJoinDlg : UI.Dialog
{
  private Dictionary<long, AllianceSearchItemRenderer> m_ItemDict = new Dictionary<long, AllianceSearchItemRenderer>();
  private int m_CurrentPage = 1;
  private int m_LanguageType = 1;
  private bool m_Paging = true;
  public UIInput m_SearchInput;
  public UIViewport m_Viewport;
  public UIGrid m_Grid;
  public UIScrollView m_ScrollView;
  public GameObject m_AllianceItemPrefab;
  public GameObject m_NoAllianceFound;
  public UIButton m_CreateButton;
  public UIButton m_CreateGoldButton;
  public UILabel m_GoldTip;
  public GameObject m_Badge;
  public UILabel m_BadgeCount;
  public GameObject m_Loading;
  public UILabel m_price;
  private AllianceJoinDlg.SearchMode m_SearchMode;
  private int m_Offset;
  private bool m_ShouldResetPosition;
  private ArrayList m_ServerData;
  private TweenPosition m_Tween;

  private void StopTween()
  {
    if (!(bool) ((UnityEngine.Object) this.m_Tween))
      return;
    this.m_Tween.enabled = false;
    this.m_Tween = (TweenPosition) null;
  }

  private void StartTween()
  {
    this.StopTween();
    this.m_Tween = TweenPosition.Begin(this.m_Grid.gameObject, 0.5f, this.m_Grid.transform.localPosition + new Vector3(0.0f, 380f, 0.0f));
  }

  private void SendRequestToServer(AllianceJoinDlg.SearchMode searchMode, bool forceClear)
  {
    if (this.m_SearchMode != searchMode || forceClear)
      this.ClearData();
    this.m_SearchMode = searchMode;
    switch (this.m_SearchMode)
    {
      case AllianceJoinDlg.SearchMode.Recommend:
      case AllianceJoinDlg.SearchMode.Randomize:
        this.RecommendAlliance();
        break;
      case AllianceJoinDlg.SearchMode.Search:
        this.SearchAlliance();
        break;
    }
  }

  private void RecommendAlliance()
  {
    if (!this.m_Paging)
      return;
    Hashtable postData = new Hashtable();
    postData[(object) "lang"] = (object) Language.Instance.GetAllianceLanguage();
    postData[(object) "offset"] = (object) this.m_Offset;
    postData[(object) "lang_type"] = (object) this.m_LanguageType;
    postData[(object) "page"] = (object) this.m_CurrentPage;
    MessageHub.inst.GetPortByAction("Alliance:recommendAllianceList").SendRequest(postData, (System.Action<bool, object>) ((ret, orgData) =>
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return;
      ArrayList arrayList = inData[(object) "list"] as ArrayList;
      if (arrayList != null && arrayList.Count > 0)
      {
        ++this.m_CurrentPage;
        this.m_ServerData = arrayList;
      }
      DatabaseTools.UpdateData(inData, "paging", ref this.m_Paging);
      DatabaseTools.UpdateData(inData, "offset", ref this.m_Offset);
      DatabaseTools.UpdateData(inData, "lang_type", ref this.m_LanguageType);
    }), true);
  }

  private void SearchAlliance()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "context"] = (object) this.m_SearchInput.value;
    postData[(object) "page"] = (object) this.m_CurrentPage;
    MessageHub.inst.GetPortByAction("Alliance:searchAllianceByUser").SendRequest(postData, (System.Action<bool, object>) ((ret, orgData) =>
    {
      ArrayList arrayList = orgData as ArrayList;
      if (arrayList == null || arrayList.Count <= 0)
        return;
      ++this.m_CurrentPage;
      this.m_ServerData = arrayList;
    }), true);
  }

  private void EndSendRequest()
  {
    bool flag1 = this.m_ItemDict.Count == 0;
    this.m_NoAllianceFound.SetActive(flag1);
    this.m_Viewport.gameObject.SetActive(!flag1);
    bool flag2 = this.m_ItemDict.Count >= 5;
    foreach (Behaviour componentsInChild in this.m_Grid.GetComponentsInChildren<UIDragCamera>())
      componentsInChild.enabled = flag2;
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Loading.SetActive(false);
    this.m_Viewport.sourceCamera = UIManager.inst.ui2DCamera;
    this.m_Viewport.gameObject.SetActive(false);
    this.m_SearchInput.defaultText = ScriptLocalization.Get("alliance_search_placeholder", true);
    this.m_SearchInput.value = string.Empty;
    this.m_ShouldResetPosition = true;
    this.SendRequestToServer(AllianceJoinDlg.SearchMode.Recommend, true);
    StatsData statsData = DBManager.inst.DB_Stats.Get("alliance_join");
    if (TutorialManager.Instance.EarlyGoalType == TutorialManager.ABTestType.Second)
    {
      if (statsData == null)
        UIManager.inst.OpenPopup("Alliance/AllianceJoinBPopup", (Popup.PopupParameter) null);
    }
    else if (statsData == null && !AllianceJoinPopup.Displayed)
    {
      AllianceJoinPopup.Displayed = true;
      UIManager.inst.OpenPopup("Alliance/AllianceJoinPopup", (Popup.PopupParameter) null);
    }
    this.m_ScrollView.onDragStarted += new UIScrollView.OnDragNotification(this.OnStartDragging);
    this.m_ScrollView.onDragFinished += new UIScrollView.OnDragNotification(this.OnStopDragging);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.SendRequestToServer(AllianceJoinDlg.SearchMode.Unknown, true);
    this.m_ScrollView.onDragStarted -= new UIScrollView.OnDragNotification(this.OnStartDragging);
    this.m_ScrollView.onDragFinished -= new UIScrollView.OnDragNotification(this.OnStopDragging);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    using (Dictionary<long, AllianceSearchItemRenderer>.ValueCollection.Enumerator enumerator = this.m_ItemDict.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UpdateUI();
    }
    this.Reposition();
    this.UpdateCreateButton();
    DBManager.inst.DB_City.onDataChanged += new System.Action<long>(this.OnCityDataChanged);
    DBManager.inst.DB_AllianceInviteApply.onDataChanged += new System.Action<long, long>(this.OnInviteApplyChanged);
    AllianceManager.Instance.onPushAcceptApply += new System.Action<object>(this.OnAcceptApply);
    this.UpdateBadgeCount(PlayerData.inst.uid);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    DBManager.inst.DB_City.onDataChanged -= new System.Action<long>(this.OnCityDataChanged);
    DBManager.inst.DB_AllianceInviteApply.onDataChanged -= new System.Action<long, long>(this.OnInviteApplyChanged);
    AllianceManager.Instance.onPushAcceptApply -= new System.Action<object>(this.OnAcceptApply);
  }

  private void OnAcceptApply(object data)
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnSearchBtnPressed()
  {
    this.m_ShouldResetPosition = true;
    if (this.m_SearchInput.value.Length == 0)
      this.SendRequestToServer(AllianceJoinDlg.SearchMode.Randomize, true);
    else
      this.SendRequestToServer(AllianceJoinDlg.SearchMode.Search, true);
  }

  private void OnInviteApplyChanged(long uid, long allianceId)
  {
    if (PlayerData.inst.uid != uid)
      return;
    this.UpdateBadgeCount(uid);
  }

  private void OnCityDataChanged(long cityId)
  {
    if (cityId != (long) PlayerData.inst.cityId)
      return;
    this.UpdateCreateButton();
  }

  private void UpdateBadgeCount(long uid)
  {
    List<AllianceInvitedApplyData> dataByUid = DBManager.inst.DB_AllianceInviteApply.GetDataByUid(uid);
    this.m_Badge.SetActive(dataByUid.Count > 0);
    this.m_BadgeCount.text = dataByUid.Count.ToString();
  }

  private void UpdateCreateButton()
  {
    bool flag = PlayerData.inst.playerCityData.level >= 6;
    this.m_CreateButton.gameObject.SetActive(flag);
    this.m_CreateGoldButton.gameObject.SetActive(!flag);
    this.m_GoldTip.gameObject.SetActive(!flag);
    Utils.SetPriceToLabel(this.m_price, 200);
  }

  private void ClearData()
  {
    this.m_CurrentPage = 1;
    this.m_Paging = true;
    this.m_Offset = 0;
    this.m_LanguageType = 1;
    using (Dictionary<long, AllianceSearchItemRenderer>.ValueCollection.Enumerator enumerator = this.m_ItemDict.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceSearchItemRenderer current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ItemDict.Clear();
  }

  private void AddItem(AllianceSearchItemData itemData)
  {
    if (this.m_ItemDict.ContainsKey(itemData.allianceID))
      return;
    GameObject gameObject = Utils.DuplicateGOB(this.m_AllianceItemPrefab);
    gameObject.SetActive(true);
    AllianceSearchItemRenderer component = gameObject.GetComponent<AllianceSearchItemRenderer>();
    component.SetData(itemData);
    this.m_ItemDict.Add(itemData.allianceID, component);
  }

  private void Reposition()
  {
    this.m_Grid.Reposition();
    this.m_Grid.transform.localPosition = Vector3.zero;
  }

  private void ResetPosition()
  {
    if (!this.m_ShouldResetPosition)
      return;
    this.m_ScrollView.ResetPosition();
  }

  private void UpdateItemList(ArrayList data)
  {
    if (data == null)
      return;
    for (int index = 0; index < data.Count; ++index)
    {
      AllianceSearchItemData itemData = new AllianceSearchItemData();
      itemData.Decode(data[index]);
      this.AddItem(itemData);
    }
    this.Reposition();
    this.ResetPosition();
    if (this.m_CurrentPage <= 2)
      return;
    this.StartTween();
  }

  private void OnStopDragging()
  {
    Bounds bounds = this.m_ScrollView.bounds;
    if ((double) this.m_ScrollView.panel.CalculateConstrainOffset((Vector2) bounds.min, (Vector2) bounds.max).y > -200.0)
      return;
    this.m_ShouldResetPosition = false;
    switch (this.m_SearchMode)
    {
      case AllianceJoinDlg.SearchMode.Recommend:
      case AllianceJoinDlg.SearchMode.Randomize:
      case AllianceJoinDlg.SearchMode.Search:
        this.SendRequestToServer(this.m_SearchMode, false);
        break;
    }
  }

  private void OnStartDragging()
  {
    this.StopTween();
  }

  private void Update()
  {
    Bounds bounds = this.m_ScrollView.bounds;
    Vector3 constrainOffset = this.m_ScrollView.panel.CalculateConstrainOffset((Vector2) bounds.min, (Vector2) bounds.max);
    if (!this.m_ScrollView.canMoveHorizontally)
      constrainOffset.x = 0.0f;
    if (!this.m_ScrollView.canMoveVertically)
      constrainOffset.y = 0.0f;
    this.m_Loading.SetActive(this.m_Paging && (double) constrainOffset.y <= -200.0);
    if ((double) Mathf.Abs(constrainOffset.y) >= 5.0 || this.m_ServerData == null)
      return;
    this.UpdateItemList(this.m_ServerData);
    this.EndSendRequest();
    this.m_ServerData = (ArrayList) null;
  }

  public void OnCreateAlliance()
  {
    if (this.m_CreateGoldButton.gameObject.activeSelf)
    {
      if (PlayerData.inst.userData.currency.gold >= 200L)
        UIManager.inst.OpenPopup("Alliance/AllianceCreatePopup", (Popup.PopupParameter) null);
      else
        Utils.ShowNotEnoughGoldTip();
    }
    else
      UIManager.inst.OpenPopup("Alliance/AllianceCreatePopup", (Popup.PopupParameter) null);
  }

  public void OnInviteApply()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceMoreInvitesApplicantsDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private enum SearchMode
  {
    Unknown,
    Recommend,
    Randomize,
    Search,
  }
}
