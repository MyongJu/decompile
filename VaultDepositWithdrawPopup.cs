﻿// Decompiled with JetBrains decompiler
// Type: VaultDepositWithdrawPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class VaultDepositWithdrawPopup : Popup
{
  private ProgressBarTweenProxy proxy = new ProgressBarTweenProxy();
  private bool autoFix = true;
  public GameObject progressRootNode;
  public GameObject depositRootNode;
  public GameObject cantDepositRootNode;
  public GameObject withdrawRootNode;
  public GameObject oneButtonRootNode;
  public GameObject twoButtonRootNode;
  public UILabel titleText;
  public UILabel durationText;
  public UILabel interestText;
  public UILabel depositMinDesc;
  public UILabel depositMaxDesc;
  public UILabel depositMinAmount;
  public UILabel depositMaxAmount;
  public UIProgressBar progressSlider;
  public UILabel progressValue;
  public UIProgressBar depositProgressSlider;
  public UILabel depositValueText;
  public UILabel withdrawButtonText;
  public UIButton depositButton;
  public UIInput depositGoldAmount;
  public UILabel depositGoldAmountText;
  private int slotId;
  private int maxGoldAmount;
  private int minGoldAmount;
  private int pickedGoldAmount;
  private TreasuryInfo treasuryInfo;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    VaultDepositWithdrawPopup.Parameter parameter = orgParam as VaultDepositWithdrawPopup.Parameter;
    if (parameter != null)
      this.slotId = parameter.slotId;
    this.treasuryInfo = ConfigManager.inst.DB_Treasury.Get(this.slotId);
    if (this.treasuryInfo == null)
      return;
    this.maxGoldAmount = (int) Mathf.Min((float) this.treasuryInfo.maxGoldCount, (float) PlayerData.inst.userData.currency.gold);
    this.minGoldAmount = (int) this.treasuryInfo.minGoldCount;
    this.proxy.MaxSize = this.maxGoldAmount - this.minGoldAmount;
    this.depositProgressSlider.value = 0.0f;
    this.pickedGoldAmount = (int) Mathf.Min((float) this.treasuryInfo.minGoldCount, (float) PlayerData.inst.userData.currency.gold);
    this.AddEventHandler();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.StopCoroutine(VaultFunHelper.Instance.BreathingEffect(this.progressSlider));
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnWithdrawBtnPressed()
  {
    TreasuryData treasuryData = DBManager.inst.DB_Treasury.GetTreasuryData();
    if (treasuryData == null || treasuryData.SlotId == 0 || treasuryData.SlotId != this.slotId)
      return;
    if (treasuryData.EndTime - NetServerTime.inst.ServerTimestamp > 0)
      UIManager.inst.OpenPopup("Vault/VaultConfirmationPopup", (Popup.PopupParameter) new VaultConfirmationPopup.Parameter()
      {
        confirmType = VaultConfirmationPopup.ConfirmType.TO_WITHDRAW_WITHOUT_INTEREST
      });
    else
      VaultFunHelper.Instance.DrawGold(true, (System.Action) null);
  }

  public void OnEarnBtnPressed()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
    this.OnCloseBtnPressed();
  }

  public void OnDepositBtnPressed()
  {
    if (this.treasuryInfo == null)
      return;
    if (!this.isCurrencyEnough())
      UIManager.inst.OpenPopup("Vault/VaultConfirmationPopup", (Popup.PopupParameter) new VaultConfirmationPopup.Parameter()
      {
        confirmType = VaultConfirmationPopup.ConfirmType.TO_EARN
      });
    else
      VaultFunHelper.Instance.SaveGold(this.slotId, this.pickedGoldAmount);
  }

  private void ShowDefaultContent()
  {
    if (this.treasuryInfo == null)
      return;
    this.maxGoldAmount = (int) Mathf.Min((float) this.treasuryInfo.maxGoldCount, (float) PlayerData.inst.userData.currency.gold);
    this.minGoldAmount = (int) this.treasuryInfo.minGoldCount;
    this.proxy.MaxSize = this.maxGoldAmount - this.minGoldAmount;
    this.pickedGoldAmount = (int) Mathf.Min((float) this.minGoldAmount, (float) PlayerData.inst.userData.currency.gold);
    this.UpdateUI();
  }

  private void ClearStatus()
  {
    NGUITools.SetActive(this.progressRootNode, false);
    NGUITools.SetActive(this.depositRootNode, false);
    NGUITools.SetActive(this.cantDepositRootNode, false);
    NGUITools.SetActive(this.withdrawRootNode, false);
    NGUITools.SetActive(this.oneButtonRootNode, false);
    NGUITools.SetActive(this.twoButtonRootNode, false);
  }

  private void UpdateUI()
  {
    this.ClearStatus();
    TreasuryData treasuryData = DBManager.inst.DB_Treasury.GetTreasuryData();
    if (treasuryData == null || this.treasuryInfo == null)
      return;
    if (treasuryData.SlotId != 0)
    {
      if (treasuryData.SlotId == this.slotId)
      {
        int num1 = treasuryData.EndTime - treasuryData.StartTime;
        int time = treasuryData.EndTime - NetServerTime.inst.ServerTimestamp;
        int num2 = NetServerTime.inst.ServerTimestamp - treasuryData.StartTime;
        if (num1 > 0 && time > 0)
        {
          this.progressSlider.value = (float) num2 / (float) num1;
          this.progressValue.text = Utils.FormatTime(time, true, false, true);
          this.withdrawButtonText.text = Utils.XLAT("vault_withdraw_early_button");
          NGUITools.SetActive(this.progressRootNode, true);
        }
        else
        {
          this.withdrawButtonText.text = Utils.XLAT("vault_withdraw_button");
          NGUITools.SetActive(this.withdrawRootNode, true);
        }
        this.depositMinDesc.text = Utils.XLAT("vault_current_investment_amount");
        this.depositMaxDesc.text = Utils.XLAT("vault_expected_withdrawal_amount");
        this.SetNumericText(this.depositMinAmount, "id_num_gold", treasuryData.GoldCount.ToString());
        this.SetNumericText(this.depositMaxAmount, "id_num_gold", Mathf.RoundToInt((float) treasuryData.GoldCount * (1f + this.treasuryInfo.interests)).ToString());
        NGUITools.SetActive(this.oneButtonRootNode, true);
      }
      else
      {
        this.depositMinDesc.text = Utils.XLAT("vault_deposit_min");
        this.depositMaxDesc.text = Utils.XLAT("vault_deposit_max");
        this.SetNumericText(this.depositMinAmount, "id_num_gold", this.treasuryInfo.minGoldCount.ToString());
        this.SetNumericText(this.depositMaxAmount, "id_num_gold", this.treasuryInfo.maxGoldCount.ToString());
        this.depositButton.isEnabled = false;
        NGUITools.SetActive(this.cantDepositRootNode, true);
        NGUITools.SetActive(this.twoButtonRootNode, true);
      }
    }
    else
    {
      this.depositMinDesc.text = Utils.XLAT("vault_deposit_min");
      this.depositMaxDesc.text = Utils.XLAT("vault_deposit_max");
      this.SetNumericText(this.depositMinAmount, "id_num_gold", this.treasuryInfo.minGoldCount.ToString());
      this.SetNumericText(this.depositMaxAmount, "id_num_gold", this.treasuryInfo.maxGoldCount.ToString());
      this.depositButton.isEnabled = this.isCurrencyEnough() || PlayerData.inst.userData.currency.gold != 0L;
      NGUITools.SetActive(this.depositRootNode, true);
      NGUITools.SetActive(this.twoButtonRootNode, true);
    }
    this.UpdatePanelContent();
    this.UpdateSliderContent();
  }

  private void SetNumericText(UILabel label, string key, string value)
  {
    label.text = ScriptLocalization.GetWithPara(key, new Dictionary<string, string>()
    {
      {
        "1",
        Utils.FormatThousands(value)
      }
    }, true);
  }

  private void UpdatePanelContent()
  {
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("1", this.treasuryInfo.duration.ToString());
    if ((double) this.treasuryInfo.duration > 1.0)
    {
      this.titleText.text = ScriptLocalization.GetWithPara("vault_deposit_duration_plural", para, true);
      this.durationText.text = ScriptLocalization.GetWithPara("id_num_days", para, true);
    }
    else
    {
      this.titleText.text = ScriptLocalization.GetWithPara("vault_deposit_duration", para, true);
      this.durationText.text = ScriptLocalization.GetWithPara("id_num_day", para, true);
    }
    this.interestText.text = ((double) this.treasuryInfo.interests * 100.0).ToString() + "%";
  }

  private void OnSecondEventHandler(int time)
  {
    this.UpdateUI();
  }

  private void OnTreasuryDataUpdated(TreasuryData treasuryData)
  {
    this.UpdateUI();
  }

  private void OnUserDataUpdate(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.ShowDefaultContent();
  }

  private void OnSliderDragFinish()
  {
    if (!this.isCurrencyEnough())
    {
      this.OnEarnBtnPressed();
    }
    else
    {
      int num = Mathf.RoundToInt((float) (this.maxGoldAmount - this.minGoldAmount) * this.depositProgressSlider.value + (float) this.minGoldAmount);
      if (num == this.pickedGoldAmount)
        return;
      this.pickedGoldAmount = num;
      this.pickedGoldAmount = Mathf.Clamp(this.pickedGoldAmount, this.minGoldAmount, this.maxGoldAmount);
      this.UpdateSliderContent();
    }
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEventHandler);
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdate);
    this.depositProgressSlider.onDragFinished += new UIProgressBar.OnDragFinished(this.OnSliderDragFinish);
    DBManager.inst.DB_Treasury.onDataUpdate += new System.Action<TreasuryData>(this.OnTreasuryDataUpdated);
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEventHandler);
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdate);
    this.depositProgressSlider.onDragFinished -= new UIProgressBar.OnDragFinished(this.OnSliderDragFinish);
    DBManager.inst.DB_Treasury.onDataUpdate -= new System.Action<TreasuryData>(this.OnTreasuryDataUpdated);
  }

  private void UpdateSliderContent()
  {
    this.depositGoldAmount.value = this.pickedGoldAmount.ToString();
    this.depositGoldAmountText.text = this.pickedGoldAmount.ToString();
    if (PlayerData.inst.userData.currency.gold <= this.treasuryInfo.minGoldCount)
    {
      this.depositProgressSlider.value = 0.0f;
      this.depositProgressSlider.enabled = false;
    }
    else
      this.depositProgressSlider.enabled = true;
  }

  private bool isCurrencyEnough()
  {
    return PlayerData.inst.userData.currency.gold >= this.treasuryInfo.minGoldCount;
  }

  public void OnSliderValueChanged()
  {
    int num = Mathf.RoundToInt((float) (this.maxGoldAmount - this.minGoldAmount) * this.depositProgressSlider.value + (float) this.minGoldAmount);
    if (num == this.pickedGoldAmount)
      return;
    this.pickedGoldAmount = Mathf.Clamp(num, this.minGoldAmount, this.maxGoldAmount);
    this.depositProgressSlider.value = (float) (this.pickedGoldAmount - this.minGoldAmount) / (float) (this.maxGoldAmount - this.minGoldAmount);
    this.UpdateSliderContent();
  }

  public void OnSliderSubtractBtnDown()
  {
    if (!this.isCurrencyEnough())
    {
      this.OnEarnBtnPressed();
    }
    else
    {
      if (this.treasuryInfo != null && (long) this.pickedGoldAmount == this.treasuryInfo.minGoldCount)
        return;
      this.autoFix = false;
      this.proxy.Slider = this.depositProgressSlider;
      this.proxy.Play(-1, -1f);
    }
  }

  public void OnSliderSubtractBtnUp()
  {
    if (!this.isCurrencyEnough() || this.treasuryInfo != null && (long) this.pickedGoldAmount == this.treasuryInfo.minGoldCount)
      return;
    this.autoFix = true;
    this.proxy.Stop();
    this.UpdateSliderContent();
  }

  public void OnSliderPlusBtnDown()
  {
    if (PlayerData.inst.userData.currency.gold <= this.treasuryInfo.minGoldCount)
    {
      if (this.isCurrencyEnough())
        return;
      this.OnEarnBtnPressed();
    }
    else
    {
      this.autoFix = false;
      this.proxy.Slider = this.depositProgressSlider;
      this.proxy.Play(1, -1f);
    }
  }

  public void OnSliderPlusBtnUp()
  {
    if (PlayerData.inst.userData.currency.gold <= this.treasuryInfo.minGoldCount)
      return;
    this.autoFix = true;
    this.proxy.Stop();
    this.UpdateSliderContent();
  }

  public void OnProgressSelectBtnPressed()
  {
    if (this.isCurrencyEnough())
      return;
    this.OnEarnBtnPressed();
  }

  public void OnDepositGoldAmountInputChange()
  {
    if (this.depositGoldAmount.isSelected && !this.isCurrencyEnough())
    {
      this.OnEarnBtnPressed();
      this.depositGoldAmount.isSelected = false;
    }
    else
    {
      int num = 0;
      try
      {
        num = Convert.ToInt32(this.depositGoldAmount.value);
      }
      catch (FormatException ex)
      {
        num = 0;
      }
      catch (OverflowException ex)
      {
        num = int.MaxValue;
      }
      finally
      {
        this.pickedGoldAmount = Mathf.Clamp(num, 0, this.maxGoldAmount);
        if (this.autoFix)
          this.depositProgressSlider.value = (float) (this.pickedGoldAmount - this.minGoldAmount) / (float) (this.maxGoldAmount - this.minGoldAmount);
        this.UpdateSliderContent();
      }
    }
  }

  public class Parameter : Popup.PopupParameter
  {
    public int slotId;
  }
}
