﻿// Decompiled with JetBrains decompiler
// Type: MarchSpeedUpPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;

public class MarchSpeedUpPopup : Popup
{
  public UIProgressBar m_Progress;
  public UILabel m_Status;
  public UILabel m_Timer;
  public UIGrid m_Grid;
  public UIGrid m_PitGrid;
  public MarchSpeedUpItemRenderer[] m_Items;
  public PitMarchSpeedUpItemRenderer[] m_PitItems;
  private MarchSpeedUpPopup.Parameter m_Parameter;
  private March.March m_MarchControler;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as MarchSpeedUpPopup.Parameter;
    if (this.m_Parameter == null)
      return;
    this.m_MarchControler = GameEngine.Instance.marchSystem.marches[this.m_Parameter.marchData.marchId];
    NGUITools.SetActive(this.m_Grid.gameObject, !PlayerData.inst.IsInPit);
    NGUITools.SetActive(this.m_PitGrid.gameObject, PlayerData.inst.IsInPit);
    if (PlayerData.inst.IsInPit)
      this.UpdatePitItems();
    else
      this.UpdateItems();
    this.Update();
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemDataChanged);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnItemDataRemoved);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataChanged);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnItemDataRemoved);
  }

  private void UpdateItems()
  {
    for (int index = 0; index < this.m_Items.Length; ++index)
      this.m_Items[index].Initialize(new System.Action<ShopStaticInfo, bool>(this.OnUseOrPurchase));
  }

  private void UpdatePitItems()
  {
    for (int index = 0; index < this.m_PitItems.Length; ++index)
      this.m_PitItems[index].Initialize(new System.Action(this.OnUsePitSpeedUp));
    this.m_PitGrid.Reposition();
  }

  private void OnItemDataChanged(int itemId)
  {
    this.UpdateItems();
  }

  private void OnItemDataRemoved(int itemId)
  {
    this.UpdateItems();
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnUsePitSpeedUp()
  {
    MessageHub.inst.GetPortByAction("Abyss:marchSpeedUp").SendRequest(new Hashtable()
    {
      {
        (object) "march_id",
        (object) this.m_Parameter.marchData.marchId
      },
      {
        (object) "march_owner_uid",
        (object) DBManager.inst.DB_March.Get(this.m_Parameter.marchData.marchId).ownerUid
      }
    }, (System.Action<bool, object>) ((ret, args) =>
    {
      if (!ret)
        return;
      Hashtable hashtable = args as Hashtable;
      if (hashtable != null && hashtable.ContainsKey((object) "errno"))
      {
        int result = 0;
        int.TryParse(hashtable[(object) "errno"].ToString(), out result);
        if (result != 1400040)
          ;
      }
      else
      {
        if (hashtable != null && hashtable.ContainsKey((object) "march_speedup"))
        {
          int result = 0;
          int.TryParse(hashtable[(object) "march_speedup"] as string, out result);
          PitExplorePayload.Instance.PitSpeedUpTimes = result;
        }
        if (!((UnityEngine.Object) this != (UnityEngine.Object) null))
          return;
        this.UpdatePitItems();
        AudioManager.Instance.PlaySound("sfx_march_speed_up", false);
        this.Update();
      }
    }), true);
  }

  private void OnUseOrPurchase(ShopStaticInfo shopInfo, bool purchase)
  {
    if (purchase)
    {
      MarchData marchData = this.m_Parameter.marchData;
      if (marchData == null || (marchData.state & (MarchData.MarchState.marching | MarchData.MarchState.returning)) == MarchData.MarchState.invalid)
        this.OnClosePressed();
      else if (ItemBag.Instance.GetShopItemPrice(shopInfo.ID) > PlayerData.inst.hostPlayer.Currency)
      {
        Utils.ShowNotEnoughGoldTip();
        UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      }
      else
        ItemBag.Instance.BuyAndUseShopItem(shopInfo.ID, new Hashtable()
        {
          {
            (object) "march_id",
            (object) marchData.marchId
          },
          {
            (object) "march_owner_uid",
            (object) DBManager.inst.DB_March.Get(marchData.marchId).ownerUid
          },
          {
            (object) "march_type",
            (object) "pvp"
          }
        }, new System.Action<bool, object>(this.OnUseCallback), 1);
    }
    else
    {
      MarchData marchData = this.m_Parameter.marchData;
      Hashtable extra = new Hashtable();
      extra.Add((object) "march_id", (object) marchData.marchId);
      extra.Add((object) "march_owner_uid", (object) DBManager.inst.DB_March.Get(marchData.marchId).ownerUid);
      extra.Add((object) "march_type", (object) "pvp");
      if (marchData == null || (marchData.state & (MarchData.MarchState.marching | MarchData.MarchState.returning)) == MarchData.MarchState.invalid)
        this.OnClosePressed();
      else
        ItemBag.Instance.UseItem(shopInfo.Item_InternalId, 1, extra, new System.Action<bool, object>(this.OnUseCallback));
    }
  }

  private void OnUseCallback(bool ret, object data)
  {
    if (!ret)
      return;
    try
    {
      AudioManager.Instance.PlaySound("sfx_march_speed_up", false);
      this.Update();
    }
    catch
    {
      D.warn((object) "[MarchSpeedUpPopup]OnUseCallback: popup has been closed.");
    }
  }

  private void Update()
  {
    if (this.m_Parameter.marchData == null || this.m_Parameter.marchData.state == MarchData.MarchState.done || ((this.m_Parameter.marchData.state & (MarchData.MarchState.marching | MarchData.MarchState.returning)) == MarchData.MarchState.invalid || this.m_Parameter.marchData.endTime - NetServerTime.inst.ServerTimestamp <= 0))
    {
      this.OnClosePressed();
    }
    else
    {
      if (this.m_Parameter.marchData != null)
      {
        this.m_Status.text = this.m_Parameter.marchData.loc_marchState;
        int time = this.m_Parameter.marchData.endTime - NetServerTime.inst.ServerTimestamp;
        int num = this.m_Parameter.marchData.endTime - this.m_Parameter.marchData.startTime;
        this.m_Progress.value = (float) (1.0 - (double) time / (double) num);
        this.m_Timer.text = Utils.FormatTime(time, false, false, true);
      }
      if (this.m_MarchControler == null || !((UnityEngine.Object) this.m_MarchControler.troopsView != (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_MarchControler.troopsView._circleBtnParent != (UnityEngine.Object) null))
        return;
      UIManager.inst.tileCamera.SetTargetLookPosition(this.m_MarchControler.troopsView._circleBtnParent.position, false);
      PVPSystem.Instance.Map.CurrentKXY = this.m_Parameter.marchData.MapData.ConvertWorldPositionToTileKXY(UIManager.inst.tileCamera.transform.position);
    }
  }

  public class Parameter : Popup.PopupParameter
  {
    public MarchData marchData;
  }
}
