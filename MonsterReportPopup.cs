﻿// Decompiled with JetBrains decompiler
// Type: MonsterReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MonsterReportPopup : BaseReportPopup
{
  private List<GameObject> pool = new List<GameObject>();
  public float gapX = 400f;
  public float gapY = -200f;
  private WarReportContent wrc;
  public UIScrollView sv;
  public UILabel winLbl;
  public UILabel lossLbl;
  public MonsterOverview monsteroverview;
  public GameObject resultGroup;
  public GameObject rewardGroup;
  public Icon reward;
  public UILabel losstip;
  public UISprite bg;
  public GameObject itemgroup;

  private void Init()
  {
    bool flag = this.wrc.IsWinner();
    if (flag)
      BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, true, false, string.Empty);
    else
      BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_loser", (System.Action<bool>) null, true, false, string.Empty);
    this.time.text = Utils.FormatTimeForMail(this.mailtime);
    WorldEncounterData data = ConfigManager.inst.DB_WorldEncount.GetData(long.Parse(this.wrc.defender.uid));
    string str = ScriptLocalization.Get("id_lv", true) + data.level + " " + data.Name;
    this.monsteroverview.SeedData(new MonsterOverview.Data()
    {
      name = str,
      icon = data.MonsterImage,
      k = this.wrc.defender.k,
      x = this.wrc.defender.x,
      y = this.wrc.defender.y,
      powerlost = this.wrc.CalculateAllPowerLost(this.wrc.attacker_troop_detail),
      totalTroop = this.wrc.CalculateAllTroop(this.wrc.attacker_troop_detail),
      wounded = this.wrc.CalculateAllTroopWound(this.wrc.attacker_troop_detail),
      survived = this.wrc.CalculateAllTroopSurvive(this.wrc.attacker_troop_detail),
      dsht = this.wrc.attacker.dragon
    });
    if (flag)
    {
      this.rewardGroup.SetActive(true);
      this.losstip.gameObject.SetActive(false);
      this.SummarizeRewards();
    }
    else
    {
      this.rewardGroup.SetActive(false);
      this.losstip.gameObject.SetActive(true);
    }
    this.HideTemplate(true);
  }

  private void SummarizeRewards()
  {
    List<IconData> iconDataList = new List<IconData>();
    if (this.wrc.dragon_attr != null)
    {
      if ("0" != this.wrc.dragon_attr.lord_exp)
      {
        IconData iconData = new IconData("Texture/ItemIcons/player_profile_xp", new string[2]
        {
          ScriptLocalization.Get("id_lord_xp", true),
          "+" + this.wrc.dragon_attr.lord_exp
        });
        iconDataList.Add(iconData);
      }
      if ("0" != this.wrc.dragon_attr.dragon_exp)
      {
        IconData iconData = new IconData("Texture/ItemIcons/item_dragon_exp", new string[2]
        {
          ScriptLocalization.Get("id_dragon_xp", true),
          "+" + this.wrc.dragon_attr.dragon_exp
        });
        iconDataList.Add(iconData);
      }
      if ("0" != this.wrc.dragon_attr.dark)
      {
        IconData iconData = new IconData("Texture/Dragon/icon_dragon_power_dark", new string[2]
        {
          ScriptLocalization.Get("id_assault_power", true),
          "+" + this.wrc.dragon_attr.dark
        });
        iconDataList.Add(iconData);
      }
    }
    if (this.wrc.attacker.rewards != null)
    {
      using (Dictionary<string, string>.Enumerator enumerator = this.wrc.attacker.rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, string> current = enumerator.Current;
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(int.Parse(current.Key));
          string imagePath = itemStaticInfo.ImagePath;
          ScriptLocalization.Get(itemStaticInfo.Loc_Name_Id, true);
          iconDataList.Add(new IconData(imagePath, new string[2]
          {
            itemStaticInfo.LocName,
            "X" + current.Value
          })
          {
            Data = (object) itemStaticInfo.internalId
          });
        }
      }
    }
    if (this.wrc.attacker.kingdom_coin > 0)
    {
      IconData iconData = new IconData("Texture/Alliance/kingdom_coin", new string[2]
      {
        ScriptLocalization.Get("throne_treasury_currency_name", true),
        "X" + this.wrc.attacker.kingdom_coin.ToString()
      });
      iconDataList.Add(iconData);
    }
    if (iconDataList.Count == 0)
      return;
    List<Vector3> tarpos = new List<Vector3>(iconDataList.Count);
    Utils.ArrangePositionMode1(this.reward.transform.localPosition, iconDataList.Count, this.gapX, this.gapY, ref tarpos, 4);
    for (int index = 0; index < tarpos.Count; ++index)
    {
      GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.reward.gameObject, this.reward.transform.parent);
      this.pool.Add(gameObject);
      gameObject.transform.localPosition = tarpos[index];
      Icon component = gameObject.GetComponent<Icon>();
      component.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
      component.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
      component.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
      component.FeedData((IComponentData) iconDataList[index]);
      if (iconDataList[index].Data == null)
        component.LoadDefaultBackgroud();
    }
    this.bg.height = (int) NGUIMath.CalculateRelativeWidgetBounds(this.itemgroup.transform).size.y + 30;
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  private void Reset()
  {
    this.sv.ResetPosition();
    Rigidbody component = this.sv.transform.GetComponent<Rigidbody>();
    if ((UnityEngine.Object) null != (UnityEngine.Object) component)
    {
      component.isKinematic = true;
      component.useGravity = false;
    }
    this.HideTemplate(false);
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        PrefabManagerEx.Instance.Destroy(current);
      }
    }
    this.pool.Clear();
  }

  private void HideTemplate(bool hide)
  {
    this.reward.gameObject.SetActive(!hide);
  }

  public void GotoMonster()
  {
    this.GoToTargetPlace(int.Parse(this.wrc.defender.k), int.Parse(this.wrc.defender.x), int.Parse(this.wrc.defender.y));
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.wrc = JsonReader.Deserialize<WarReport>(Utils.Object2Json((object) this.param.hashtable)).data;
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }
}
