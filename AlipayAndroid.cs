﻿// Decompiled with JetBrains decompiler
// Type: AlipayAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using Funplus.Abstract;
using HSMiniJSON;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UI;
using UnityEngine;

public class AlipayAndroid : BasePaymentWrapper
{
  private static readonly object locker = new object();
  private List<Dictionary<string, object>> _allProductData = new List<Dictionary<string, object>>();
  private const string FUNC_PAYMENT_INITIALIZE_SUCCESS = "OnPaymentInitializeSuccess";
  private const string FUNC_PAYMENT_INITIALIZE_ERROR = "OnPaymentInitializeError";
  private const string FUNC_NAME_PURCHASE_ERROR = "OnPaymentPurchaseError";
  private const string FUNC_NAME_PAYMENT_PURCHASE_SUCCESS = "OnPaymentPurchaseSuccess";
  private const string ERROR_MESSAGE = "{errorCode:9999, errorMsg:\"\", errorLocalizedMsg:\"\"}";
  private static AlipayAndroid _instance;
  private string _targetGameObjectName;
  private GameObject _targetGameObject;
  private AlipayAndroidMonoHelper _monoHelper;

  private void Log(string log)
  {
    Debug.Log((object) log);
  }

  private Dictionary<string, object> GetProductData(string productId)
  {
    using (List<Dictionary<string, object>>.Enumerator enumerator = this._allProductData.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Dictionary<string, object> current = enumerator.Current;
        if (current.ContainsKey(nameof (productId)) && current[nameof (productId)].ToString() == productId)
          return current;
      }
    }
    return (Dictionary<string, object>) null;
  }

  public static AlipayAndroid Instance
  {
    get
    {
      if (AlipayAndroid._instance == null)
      {
        lock (AlipayAndroid.locker)
          AlipayAndroid._instance = new AlipayAndroid();
      }
      return AlipayAndroid._instance;
    }
  }

  private string ReceiptRequestServerUrl
  {
    get
    {
      if (!string.IsNullOrEmpty(PaymentManager.Instance.PaymentUrl))
        return PaymentManager.Instance.PaymentUrl + "/payment/alipay_sign_data/";
      return FunplusSdk.Instance.Environment == "sandbox" ? "http://payment-sandbox-cn.funplusgame.com/payment/alipay_sign_data/" : "https://payment-cn.campfiregames.cn/payment/alipay_sign_data/";
    }
  }

  private string AppId
  {
    get
    {
      if (!string.IsNullOrEmpty(PaymentManager.Instance.PaymentAppId))
        return PaymentManager.Instance.PaymentAppId;
      return FunplusSdk.Instance.Environment == "sandbox" ? "156" : "10";
    }
  }

  private GameObject TargetGameObject
  {
    get
    {
      if (!(bool) ((UnityEngine.Object) this._targetGameObject))
        this._targetGameObject = GameObject.Find(this._targetGameObjectName);
      return this._targetGameObject;
    }
  }

  public override void SetGameObject(string gameObjectName)
  {
    this._targetGameObjectName = gameObjectName;
  }

  public override void SetCurrencyWhitelist(string whitelist)
  {
  }

  public override bool CanMakePurchases()
  {
    return true;
  }

  public override void StartHelper()
  {
    this.Log(string.Format(nameof (StartHelper)));
    if (!(bool) ((UnityEngine.Object) this._monoHelper))
    {
      GameObject gameObject = new GameObject("Instance_AlipayAndroidMonoHelper");
      UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
      this._monoHelper = gameObject.AddComponent<AlipayAndroidMonoHelper>();
    }
    if (!(FunplusSdk.Instance.Environment == "sandbox"))
      ;
    if (!this.BuildProductListFromConfigFiles())
      this.BuildProductListFromCode();
    string str = Json.Serialize((object) this._allProductData);
    this.TargetGameObject.BroadcastMessage("OnPaymentInitializeSuccess", (object) str);
    this.Log("alipay product list: " + str);
  }

  private bool BuildProductListFromConfigFiles()
  {
    this.Log(nameof (BuildProductListFromConfigFiles));
    if (!File.Exists(NetApi.inst.BuildLocalPath("iap_platform.json")) || !File.Exists(NetApi.inst.BuildLocalPath("iap_price_default.json")))
      return false;
    this._allProductData = new List<Dictionary<string, object>>();
    using (List<IAPPlatformInfo>.Enumerator enumerator = ConfigManager.inst.DB_IAPPlatform.GetAll("alipay").GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IAPPlatformInfo current = enumerator.Current;
        ConfigIAPFakePriceInfo iapFakePriceInfo = ConfigManager.inst.DB_IAPPrice.Get(current.product_id);
        if (iapFakePriceInfo == null)
        {
          D.error((object) string.Format("cannot find product default price : {0}", (object) current.product_id));
        }
        else
        {
          double cny = iapFakePriceInfo.cny;
          long num = (long) (cny * 1000000.0);
          this._allProductData.Add(new Dictionary<string, object>()
          {
            {
              "productId",
              (object) current.product_id
            },
            {
              "title",
              (object) string.Empty
            },
            {
              "description",
              (object) string.Empty
            },
            {
              "price_currency_code",
              (object) "CNY"
            },
            {
              "price",
              (object) string.Format("CNY {0}", (object) cny)
            },
            {
              "price_amount",
              (object) cny.ToString()
            },
            {
              "price_amount_micros",
              (object) num
            }
          });
        }
      }
    }
    return this._allProductData.Count > 0;
  }

  private void BuildProductListFromCode()
  {
    this.Log(nameof (BuildProductListFromCode));
    this._allProductData = new List<Dictionary<string, object>>();
    using (Dictionary<string, double>.Enumerator enumerator = new Dictionary<string, double>()
    {
      {
        "com.funplus.koa.gold0",
        6.0
      },
      {
        "com.funplus.koa.gold1",
        30.0
      },
      {
        "com.funplus.koa.gold2",
        68.0
      },
      {
        "com.funplus.koa.gold3",
        128.0
      },
      {
        "com.funplus.koa.gold4",
        328.0
      },
      {
        "com.funplus.koa.gold5",
        648.0
      },
      {
        "com.funplus.koa.month0",
        30.0
      },
      {
        "com.funplus.koa.month1",
        68.0
      },
      {
        "com.funplus.koa.month2",
        128.0
      },
      {
        "com.funplus.koa.dailyrecharge0",
        30.0
      },
      {
        "com.funplus.koa.dailyrecharge1",
        68.0
      },
      {
        "com.funplus.koa.dailyrecharge2",
        128.0
      }
    }.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, double> current = enumerator.Current;
        this._allProductData.Add(new Dictionary<string, object>()
        {
          {
            "productId",
            (object) current.Key
          },
          {
            "title",
            (object) string.Empty
          },
          {
            "description",
            (object) string.Empty
          },
          {
            "price_currency_code",
            (object) "CNY"
          },
          {
            "price",
            (object) string.Format("CNY {0}", (object) current.Value)
          },
          {
            "price_amount",
            (object) current.Value.ToString()
          },
          {
            "price_amount_micros",
            (object) (long) (1000000.0 * current.Value)
          }
        });
      }
    }
  }

  public override void Buy(string productId, string throughCargo)
  {
    D.error((object) "have not implement");
  }

  public override void Buy(string productId, string serverId, string throughCargo)
  {
    this.Log(string.Format("Buy {0} {1} {2}", (object) productId, (object) serverId, (object) throughCargo));
    string[] strArray = throughCargo.Split('|');
    if (strArray.Length >= 4)
      this.RequestReceiptDetail(AccountManager.Instance.FunplusID, strArray[3], productId, throughCargo);
    else
      D.error((object) string.Format("invalid throughcargo: {0}", (object) throughCargo));
  }

  private void RequestReceiptDetail(string uid, string appServerId, string productId, string throughCargo)
  {
    if (GameEngine.IsReady())
      UIManager.inst.ShowSystemBlocker("FullScreenWait", (SystemBlocker.SystemBlockerParameter) null);
    this.Log(string.Format("RequestReceiptDetail({0},{1},{2},{3})", (object) uid, (object) appServerId, (object) productId, (object) throughCargo));
    GameObject gameObject = new GameObject();
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
    HttpPostProcessor httpPostProcessor = gameObject.AddComponent<HttpPostProcessor>();
    Dictionary<string, object> productData = this.GetProductData(productId);
    Hashtable hashtable = new Hashtable()
    {
      {
        (object) nameof (uid),
        (object) uid
      },
      {
        (object) "appid",
        (object) this.AppId
      },
      {
        (object) "product_id",
        (object) productId
      },
      {
        (object) "through_cargo",
        (object) throughCargo
      },
      {
        (object) "amount",
        (object) productData["price_amount"].ToString()
      },
      {
        (object) "currency_code",
        (object) "CNY"
      },
      {
        (object) "country_code",
        (object) "CN"
      },
      {
        (object) "appservid",
        (object) appServerId
      }
    };
    httpPostProcessor.DoPost(this.ReceiptRequestServerUrl, hashtable, hashtable, new HttpPostProcessor.PostResultCallback(this.OnRequestReceiptDetailCallback));
  }

  private void OnRequestReceiptDetailCallback(bool result, Hashtable data, Hashtable userData)
  {
    if (GameEngine.IsReady())
      UIManager.inst.HideSystemBlocker("FullScreenWait", (SystemBlocker.SystemBlockerParameter) null);
    this.Log(string.Format("OnRequestReceiptDetailCallback({0},{1},{2})", (object) result, (object) Utils.Object2Json((object) data), (object) Utils.Object2Json((object) userData)));
    if (!result || data == null || (!data.ContainsKey((object) "status") || !(data[(object) "status"].ToString() == "ok")))
      return;
    this.CallAlipayBuy(data[(object) nameof (data)].ToString(), Utils.Object2Json((object) userData));
  }

  private void RequestProcessReceipt(string aliPayResult, Hashtable userData)
  {
    this.Log(string.Format("RequestProcessReceipt : {0}", (object) aliPayResult));
    this.OnRequestProcessReceiptCallback(true, (Hashtable) null, userData);
  }

  private void OnRequestProcessReceiptCallback(bool result, Hashtable data, Hashtable userData)
  {
    if (!result)
      return;
    string str = Json.Serialize((object) new Dictionary<string, object>()
    {
      {
        "product_id",
        (object) userData[(object) "product_id"].ToString()
      },
      {
        "through_cargo",
        (object) userData[(object) "through_cargo"].ToString()
      }
    });
    this.TargetGameObject.BroadcastMessage("OnPaymentPurchaseSuccess", (object) str);
    this.Log(string.Format("BroadcastMessage Success: {0}", (object) str));
  }

  private void CallAlipaySetSandBox()
  {
    this.Log(nameof (CallAlipaySetSandBox));
    new AndroidJavaClass("com.funplus.kingofavalon.AlipayFacade").CallStatic("SetSandBoxMode");
  }

  private void CallAlipayBuy(string detailSigned, string jsonUserData)
  {
    this.Log(string.Format("CallAlipayBuy({0},{1})", (object) detailSigned, (object) jsonUserData));
    AndroidJavaObject unityActivity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
    string gameObjectName = this._monoHelper.gameObject.name;
    unityActivity.Call("runOnUiThread", new object[1]
    {
      (object) (AndroidJavaRunnable) (() => new AndroidJavaClass("com.funplus.kingofavalon.AlipayFacade").CallStatic("Pay", (object) unityActivity, (object) detailSigned, (object) jsonUserData, (object) gameObjectName, (object) "OnCallAlipayBuyCallback"))
    });
  }

  public override bool GetSubsRenewing(string productId)
  {
    return false;
  }

  public override bool CanCheckSubs()
  {
    return false;
  }

  public void OnCallAlipayBuyCallback(string result)
  {
    this.Log(string.Format("OnCallAlipayBuyCallback({0})", (object) result));
    this._monoHelper.StartCoroutine(this.DelayCall(2f, (System.Action) (() =>
    {
      Hashtable hashtable = Utils.Json2Object(result, true) as Hashtable;
      if (hashtable != null && hashtable.ContainsKey((object) "status") && hashtable[(object) "status"].ToString() == "1")
      {
        Hashtable userData = Utils.Json2Object(hashtable[(object) "user_data"].ToString(), true) as Hashtable;
        this.RequestProcessReceipt(Utils.Object2Json(hashtable[(object) "alipay_result"]), userData);
      }
      else
      {
        this.TargetGameObject.BroadcastMessage("OnPaymentPurchaseError", (object) "{errorCode:9999, errorMsg:\"\", errorLocalizedMsg:\"\"}");
        this.Log("aplipay not success");
      }
    })));
  }

  [DebuggerHidden]
  private IEnumerator DelayCall(float delayTime, System.Action callback)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AlipayAndroid.\u003CDelayCall\u003Ec__IteratorA()
    {
      delayTime = delayTime,
      callback = callback,
      \u003C\u0024\u003EdelayTime = delayTime,
      \u003C\u0024\u003Ecallback = callback
    };
  }
}
