﻿// Decompiled with JetBrains decompiler
// Type: Puppet2D_ParentControl
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Puppet2D_ParentControl : MonoBehaviour
{
  public Vector3 OffsetScale = new Vector3(1f, 1f, 1f);
  public GameObject bone;
  public bool IsEnabled;
  public bool Point;
  public bool Orient;
  public bool Scale;
  public bool ConstrianedPosition;
  public bool ConstrianedOrient;
  public bool MaintainOffset;
  public Vector3 OffsetPos;
  public Quaternion OffsetOrient;

  public void ParentControlRun()
  {
    if (this.Orient)
      this.bone.transform.rotation = !this.MaintainOffset ? this.transform.rotation : this.transform.rotation * this.OffsetOrient;
    if (this.Point)
      this.bone.transform.position = !this.MaintainOffset ? this.transform.position : this.transform.TransformPoint(this.OffsetPos);
    if (this.Scale)
      this.bone.transform.localScale = new Vector3(this.transform.localScale.x * this.OffsetScale.x, this.transform.localScale.y * this.OffsetScale.y, this.transform.localScale.z * this.OffsetScale.z);
    if (!this.ConstrianedPosition || this.Point)
      return;
    this.transform.position = this.bone.transform.position;
  }
}
