﻿// Decompiled with JetBrains decompiler
// Type: AllianceInviteConfirmPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;

public class AllianceInviteConfirmPopup : Popup
{
  private AllianceInviteConfirmPopup.Parameter m_Parameter;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as AllianceInviteConfirmPopup.Parameter;
  }

  public void OnYes()
  {
    this.OnNo();
    AllianceManager.Instance.SendInvite(this.m_Parameter.uid, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Dictionary<string, string> para = new Dictionary<string, string>();
      ToastData data1 = ConfigManager.inst.DB_Toast.GetData("toast_alliance_invite_success");
      if (data1 == null)
        return;
      ToastManager.Instance.AddToast("BASIC_TOAST", (object) new ToastArgs(data1, para));
    }));
  }

  public void OnNo()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public long uid;
  }
}
