﻿// Decompiled with JetBrains decompiler
// Type: TroopSlider
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class TroopSlider : MonoBehaviour
{
  private ProgressBarTweenProxy proxy = new ProgressBarTweenProxy();
  private bool autoFix = true;
  private const string TEXTURE_PATH_PRE = "Texture/Unit/portrait_unit_";
  public UISlider SLD_troopSlider;
  public UILabel LB_troopTier;
  public UILabel LB_troopName;
  public UILabel LB_troopCurrentCount;
  public UIButton BT_increase;
  public UIButton BT_decrease;
  public UITexture TT_troopIcon;
  private EventDelegate onChangeEvent;
  private Unit_StatisticsInfo _troopInfo;
  private int _troopCount;
  private int _troopMaxCount;
  private int _targetCount;
  private bool _isEnable;

  public event System.Action<string, int> onValueChange;

  public void Awake()
  {
    this.onChangeEvent = new EventDelegate((MonoBehaviour) this, "OnSliderValueChanged");
  }

  private void OnButtonPhaseChange()
  {
    if (this.onValueChange == null)
      return;
    this.onValueChange(this.troopInfo.ID, this.troopCount);
  }

  public Unit_StatisticsInfo troopInfo
  {
    get
    {
      return this._troopInfo;
    }
    set
    {
      this._troopInfo = value;
      if (this._troopInfo != null)
      {
        BuilderFactory.Instance.HandyBuild((UIWidget) this.TT_troopIcon, this.troopInfo.Troop_ICON, (System.Action<bool>) null, true, false, string.Empty);
        this.LB_troopTier.text = Utils.GetRomaNumeralsBelowTen(this._troopInfo.Troop_Tier);
        this.LB_troopName.text = string.Format("{0} / {1}", (object) ScriptLocalization.Get(this._troopInfo.Troop_Name_LOC_ID, true), (object) ScriptLocalization.Get(string.Format("{0}_name", (object) this.troopInfo.Type), true));
      }
      else
        this.TT_troopIcon.mainTexture = (Texture) null;
    }
  }

  public int troopCount
  {
    get
    {
      return this._troopCount;
    }
    set
    {
      this._troopCount = value;
      if (this._troopCount < 0)
        this._troopCount = 0;
      this.LB_troopCurrentCount.text = this._troopCount.ToString();
      if (!this.autoFix)
        return;
      this.SLD_troopSlider.onChange.Clear();
      this.SLD_troopSlider.value = (float) this._troopCount / (float) this._troopMaxCount;
      this.SLD_troopSlider.onChange.Add(this.onChangeEvent);
    }
  }

  public int troopMaxCount
  {
    get
    {
      return this._troopMaxCount;
    }
    set
    {
      this._troopMaxCount = value;
    }
  }

  public int targetCount
  {
    get
    {
      return this._targetCount;
    }
    set
    {
      this._targetCount = value <= this._troopMaxCount ? value : this._troopMaxCount;
    }
  }

  public bool isEnable
  {
    get
    {
      return this._isEnable;
    }
    set
    {
      this.SLD_troopSlider.enabled = value;
      this._isEnable = value;
    }
  }

  public bool isFull
  {
    get
    {
      return this._troopCount == this._troopMaxCount;
    }
  }

  public void OnIncreaseButtonDown()
  {
    this.autoFix = false;
    this.proxy.MaxSize = this.troopMaxCount;
    this.proxy.Slider = (UIProgressBar) this.SLD_troopSlider;
    this.proxy.Play(1, (float) this.targetCount / (float) this.troopMaxCount);
  }

  public void OnIncreaseButtonUp()
  {
    this.proxy.Stop();
    this.autoFix = true;
  }

  public void OnDecreaseButtonDown()
  {
    this.autoFix = false;
    this.proxy.MaxSize = this.troopMaxCount;
    this.proxy.Slider = (UIProgressBar) this.SLD_troopSlider;
    this.proxy.Play(-1, 0.0f);
  }

  public void OnDecreaseButtonUp()
  {
    this.autoFix = true;
    this.proxy.Stop();
  }

  public void OnSliderValueChanged(float value)
  {
    this.troopCount = Mathf.RoundToInt((float) this.troopMaxCount * this.SLD_troopSlider.value);
    if (this.onValueChange == null)
      return;
    this.onValueChange(this.troopInfo.ID, this.troopCount);
  }

  public void Update()
  {
    this.BT_increase.isEnabled = this.troopCount < this._troopMaxCount;
    this.BT_decrease.isEnabled = this.troopCount > 0;
  }

  public void Zero()
  {
    this.SLD_troopSlider.value = 0.0f;
    this.troopCount = 0;
  }

  public void Max()
  {
    this.SLD_troopSlider.value = 1f;
    this.troopCount = this.troopMaxCount;
  }
}
