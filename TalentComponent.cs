﻿// Decompiled with JetBrains decompiler
// Type: TalentComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalentComponent : ComponentRenderBase
{
  public GameObject parent;
  public GameObject element;
  public GameObject container;

  public override void Init()
  {
    base.Init();
    Hashtable talentTable = (this.data as TalentComponentData).talentTable;
    List<TalentComponent.TalentElement> talentElementList = new List<TalentComponent.TalentElement>();
    foreach (DictionaryEntry dictionaryEntry in talentTable)
      talentElementList.Add(new TalentComponent.TalentElement(dictionaryEntry.Key.ToString(), float.Parse(dictionaryEntry.Value.ToString())));
    talentElementList.Sort((Comparison<TalentComponent.TalentElement>) ((x, y) =>
    {
      string longId1 = DBManager.inst.DB_Local_Benefit.GetLongID(x.key);
      string longId2 = DBManager.inst.DB_Local_Benefit.GetLongID(y.key);
      PropertyDefinition dbProperty1 = ConfigManager.inst.DB_Properties[longId1];
      if (dbProperty1 != null)
        ;
      PropertyDefinition dbProperty2 = ConfigManager.inst.DB_Properties[longId2];
      if (dbProperty2 != null)
        ;
      return dbProperty1.Priority.CompareTo(dbProperty2.Priority);
    }));
    GameObject parent = NGUITools.AddChild(this.parent, this.container);
    int index = 0;
    while (index < talentElementList.Count)
    {
      GameObject gameObject = NGUITools.AddChild(parent, this.element);
      PropertyDefinition dbProperty1 = ConfigManager.inst.DB_Properties[DBManager.inst.DB_Local_Benefit.GetLongID(talentElementList[index].key)];
      gameObject.transform.Find("key0").GetComponent<UILabel>().text = dbProperty1.Name;
      gameObject.transform.Find("value0").GetComponent<UILabel>().text = dbProperty1.ConvertToDisplayString((double) talentElementList[index].value, true, true);
      if (index + 1 < talentElementList.Count)
      {
        PropertyDefinition dbProperty2 = ConfigManager.inst.DB_Properties[DBManager.inst.DB_Local_Benefit.GetLongID(talentElementList[index + 1].key)];
        gameObject.transform.Find("key1").GetComponent<UILabel>().text = dbProperty2.Name;
        gameObject.transform.Find("value1").GetComponent<UILabel>().text = dbProperty2.ConvertToDisplayString((double) talentElementList[index + 1].value, true, true);
      }
      else
      {
        gameObject.transform.Find("key1").gameObject.SetActive(false);
        gameObject.transform.Find("value1").gameObject.SetActive(false);
      }
      index += 2;
    }
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private class TalentElement
  {
    public string key;
    public float value;

    public TalentElement(string key, float value)
    {
      this.key = key;
      this.value = value;
    }
  }
}
