﻿// Decompiled with JetBrains decompiler
// Type: DiyLotteryMainInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DiyLotteryMainInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "group")]
  public int group;
  [Config(Name = "rank")]
  public int rank;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "Rewards")]
  public Reward rewards;
  [Config(Name = "weight")]
  public int weight;
}
