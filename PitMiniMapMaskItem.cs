﻿// Decompiled with JetBrains decompiler
// Type: PitMiniMapMaskItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class PitMiniMapMaskItem : MonoBehaviour
{
  private Coordinate _coordinate;

  public void SetData(Coordinate coordinate)
  {
    this._coordinate = coordinate;
  }

  public void OnClicked()
  {
    PVPSystem.Instance.Map.GotoLocation(this._coordinate, false);
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }
}
