﻿// Decompiled with JetBrains decompiler
// Type: GuardEventDataParser
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GuardEventDataParser
{
  public static Dictionary<string, GuardAction> ParseActions(GameObject go, TextAsset script)
  {
    return GuardEventDataParser.ParseActions(go, Utils.Json2Object(script.text, true));
  }

  public static Dictionary<string, GuardAction> ParseActions(GameObject go, object orgData)
  {
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable == null)
      return (Dictionary<string, GuardAction>) null;
    ArrayList arrayList = hashtable[(object) "Actions"] as ArrayList;
    if (arrayList == null)
      return (Dictionary<string, GuardAction>) null;
    Dictionary<string, GuardAction> dictionary = new Dictionary<string, GuardAction>();
    for (int index = 0; index < arrayList.Count; ++index)
    {
      GuardAction action = GuardEventDataParser.ParseAction(go, arrayList[index]);
      dictionary[action.Action] = action;
    }
    return dictionary;
  }

  private static GuardAction ParseAction(GameObject go, object data)
  {
    GuardAction guardAction = (GuardAction) null;
    Hashtable hashtable = data as Hashtable;
    string key = hashtable[(object) "Action"] as string;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (GuardEventDataParser.\u003C\u003Ef__switch\u0024map53 == null)
      {
        // ISSUE: reference to a compiler-generated field
        GuardEventDataParser.\u003C\u003Ef__switch\u0024map53 = new Dictionary<string, int>(5)
        {
          {
            "AttackHit",
            0
          },
          {
            "SpellHit",
            1
          },
          {
            "SpellEnd",
            2
          },
          {
            "VFX",
            3
          },
          {
            "SFX",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (GuardEventDataParser.\u003C\u003Ef__switch\u0024map53.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            guardAction = (GuardAction) new GuardActionAttackHit();
            break;
          case 1:
            guardAction = (GuardAction) new GuardActionSpellHit();
            break;
          case 2:
            guardAction = (GuardAction) new GuardActionSpellEnd();
            break;
          case 3:
            guardAction = (GuardAction) new GuardActionVFX();
            break;
          case 4:
            guardAction = (GuardAction) new GuardActionSFX();
            break;
        }
      }
    }
    guardAction.Decode(go, (object) hashtable);
    return guardAction;
  }
}
