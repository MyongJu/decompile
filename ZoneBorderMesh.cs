﻿// Decompiled with JetBrains decompiler
// Type: ZoneBorderMesh
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class ZoneBorderMesh : MonoBehaviour
{
  private static readonly Vector3[] BORDER_OFFSET = new Vector3[4]
  {
    (0.5f * Vector3.up + Vector3.right) * 0.5f,
    (0.5f * Vector3.down + Vector3.right) * 0.5f,
    (0.5f * Vector3.down + Vector3.left) * 0.5f,
    (0.5f * Vector3.up + Vector3.left) * 0.5f
  };
  private static readonly Vector3[] BORDER_HALF = new Vector3[4]
  {
    (Vector3.up * 75f + Vector3.right * 150f) * 0.5f,
    (Vector3.down * 75f + Vector3.left * 150f) * 0.5f,
    (Vector3.up * 75f + Vector3.left * 150f) * 0.5f,
    (Vector3.down * 75f + Vector3.right * 150f) * 0.5f
  };
  public float BORDER_WIDTH = 40f;
  public float CORNER_WIDTH = 40f;
  private BetterList<Vector3> m_BorderVertices = new BetterList<Vector3>();
  private BetterList<Vector2> m_BorderUVs = new BetterList<Vector2>();
  private BetterList<int> m_BorderTriangles = new BetterList<int>();
  public MeshFilter m_MeshFilter;
  public MeshRenderer m_MeshRenderer;
  public UnityEngine.Animation m_Animation;
  private ZoneBorderData m_ZoneBorderData;
  private Mesh m_Mesh;
  private bool m_Start;

  private void Start()
  {
    this.m_Start = true;
    if (!(bool) ((Object) this.m_Animation))
      return;
    this.m_Animation.Play();
  }

  private void OnEnable()
  {
    if (!this.m_Start || !(bool) ((Object) this.m_Animation))
      return;
    this.m_Animation.Play();
  }

  public void SetColor(Color color)
  {
    this.m_MeshRenderer.material.color = color;
  }

  public void BuildMesh(ZoneBorderData borderData)
  {
    this.InitOnce();
    this.m_BorderVertices.Clear();
    this.m_BorderUVs.Clear();
    this.m_BorderTriangles.Clear();
    this.m_ZoneBorderData = borderData;
    this.transform.localPosition = this.m_ZoneBorderData.Position;
    List<ZoneBorderTileData> borders = borderData.Borders;
    int count = borders.Count;
    for (int index = 0; index < count; ++index)
    {
      ZoneBorderTileData zoneBorderTileData = borders[index];
      if (zoneBorderTileData.up)
        ZoneBorderMesh.GenerateMesh(zoneBorderTileData.localPosition, ZoneBorderMesh.BORDER_HALF[0], this.BORDER_WIDTH, this.m_BorderVertices, this.m_BorderUVs, this.m_BorderTriangles);
      if (zoneBorderTileData.down)
        ZoneBorderMesh.GenerateMesh(zoneBorderTileData.localPosition, ZoneBorderMesh.BORDER_HALF[1], this.BORDER_WIDTH, this.m_BorderVertices, this.m_BorderUVs, this.m_BorderTriangles);
      if (zoneBorderTileData.left)
        ZoneBorderMesh.GenerateMesh(zoneBorderTileData.localPosition, ZoneBorderMesh.BORDER_HALF[2], this.BORDER_WIDTH, this.m_BorderVertices, this.m_BorderUVs, this.m_BorderTriangles);
      if (zoneBorderTileData.right)
        ZoneBorderMesh.GenerateMesh(zoneBorderTileData.localPosition, ZoneBorderMesh.BORDER_HALF[3], this.BORDER_WIDTH, this.m_BorderVertices, this.m_BorderUVs, this.m_BorderTriangles);
    }
    this.m_Mesh.Clear();
    this.m_Mesh.vertices = this.m_BorderVertices.ToArray();
    this.m_Mesh.uv = this.m_BorderUVs.ToArray();
    this.m_Mesh.triangles = this.m_BorderTriangles.ToArray();
    this.m_MeshFilter.mesh = this.m_Mesh;
  }

  private void InitOnce()
  {
    if ((bool) ((Object) this.m_Mesh))
      return;
    this.m_Mesh = new Mesh();
  }

  private static void GenerateMesh(Vector3 center, Vector3 half, float width, BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<int> triangles)
  {
    Vector3 vector3_1 = 0.5f * PVPMapData.MapData.PixelsPerTile.x * ZoneBorderMesh.GetOffset(half);
    Vector3 vector3_2 = -vector3_1.normalized * width;
    Vector3 vector3_3 = center - half + vector3_1;
    Vector3 vector3_4 = center + half + vector3_1;
    Vector3 vector3_5 = vector3_4 + vector3_2;
    Vector3 vector3_6 = vector3_3 + vector3_2;
    int size = verts.size;
    int num1 = size;
    int num2 = size + 1;
    int num3 = size + 2;
    int num4 = size + 3;
    triangles.Add(num1);
    triangles.Add(num2);
    triangles.Add(num3);
    triangles.Add(num1);
    triangles.Add(num3);
    triangles.Add(num4);
    verts.Add(vector3_3);
    verts.Add(vector3_4);
    verts.Add(vector3_5);
    verts.Add(vector3_6);
    uvs.Add(new Vector2(1f, 0.0f));
    uvs.Add(new Vector2(0.0f, 0.0f));
    uvs.Add(new Vector2(0.0f, 1f));
    uvs.Add(new Vector2(1f, 1f));
  }

  private static Vector3 GetOffset(Vector3 half)
  {
    if ((double) half.x > 0.0 && (double) half.y < 0.0)
      return ZoneBorderMesh.BORDER_OFFSET[0];
    if ((double) half.x < 0.0 && (double) half.y < 0.0)
      return ZoneBorderMesh.BORDER_OFFSET[1];
    if ((double) half.x < 0.0 && (double) half.y > 0.0)
      return ZoneBorderMesh.BORDER_OFFSET[2];
    if ((double) half.x > 0.0 && (double) half.y > 0.0)
      return ZoneBorderMesh.BORDER_OFFSET[3];
    return Vector3.zero;
  }
}
