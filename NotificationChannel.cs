﻿// Decompiled with JetBrains decompiler
// Type: NotificationChannel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class NotificationChannel
{
  public const string HELP_SHIFT = "helpshift";
  public const string GCM = "gcm_message";
  public const string APNS = "apns_message";
  public const string ADJUST_TRACKER = "adjust_tracker";
  public const string ADJUST = "adjust";

  public virtual void Execute(Hashtable content)
  {
  }

  public static NotificationChannel Create(string channel)
  {
    NotificationChannel notificationChannel = (NotificationChannel) null;
    string key = channel;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (NotificationChannel.\u003C\u003Ef__switch\u0024map58 == null)
      {
        // ISSUE: reference to a compiler-generated field
        NotificationChannel.\u003C\u003Ef__switch\u0024map58 = new Dictionary<string, int>(5)
        {
          {
            "helpshift",
            0
          },
          {
            "gcm_message",
            1
          },
          {
            "apns_message",
            2
          },
          {
            "adjust_tracker",
            3
          },
          {
            "adjust",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (NotificationChannel.\u003C\u003Ef__switch\u0024map58.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            notificationChannel = (NotificationChannel) new HelpShiftChannel();
            break;
          case 1:
            notificationChannel = (NotificationChannel) new GCMChannel();
            break;
          case 2:
            notificationChannel = (NotificationChannel) new APNSChannel();
            break;
          case 3:
            notificationChannel = (NotificationChannel) new AdjustTrackerChannel();
            break;
          case 4:
            notificationChannel = (NotificationChannel) new AdjustChannel();
            break;
        }
      }
    }
    return notificationChannel;
  }
}
