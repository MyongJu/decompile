﻿// Decompiled with JetBrains decompiler
// Type: MiniWonderData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class MiniWonderData
{
  public int[] BenefitId = new int[3];
  public double[] BenefitValue = new double[3];
  public int[] AllianceBenefitId = new int[3];
  public double[] AllianceBenefitValue = new double[3];
  public int InternalId;
  public string ID;
  public int Level;
  public string Prefab;
  public int MinCityLevel;
  public int MaxCityLevel;
}
