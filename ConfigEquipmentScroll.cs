﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentScroll
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigEquipmentScroll
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, ConfigEquipmentScrollInfo> datas;
  private Dictionary<int, ConfigEquipmentScrollInfo> dicByUniqueId;
  private Dictionary<int, List<ConfigEquipmentScrollInfo>> dicByEquipmentType;
  private Dictionary<int, ConfigEquipmentScrollInfo> dicByEquipmentId;
  private Dictionary<int, ConfigEquipmentScrollInfo> dicByItemId;

  public void BuildDB(object res)
  {
    this.parse.Parse<ConfigEquipmentScrollInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.dicByItemId = new Dictionary<int, ConfigEquipmentScrollInfo>();
    this.dicByEquipmentId = new Dictionary<int, ConfigEquipmentScrollInfo>();
    Dictionary<int, ConfigEquipmentScrollInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator();
    while (enumerator.MoveNext())
    {
      this.dicByItemId.Add(enumerator.Current.Value.itemID, enumerator.Current.Value);
      this.dicByEquipmentId.Add(enumerator.Current.Value.equipmenID, enumerator.Current.Value);
    }
  }

  public ConfigEquipmentScrollInfo GetData(int internalID)
  {
    ConfigEquipmentScrollInfo equipmentScrollInfo;
    this.dicByUniqueId.TryGetValue(internalID, out equipmentScrollInfo);
    return equipmentScrollInfo;
  }

  public ConfigEquipmentScrollInfo GetDataByItemID(int itemID)
  {
    ConfigEquipmentScrollInfo equipmentScrollInfo;
    this.dicByItemId.TryGetValue(itemID, out equipmentScrollInfo);
    return equipmentScrollInfo;
  }

  public ConfigEquipmentScrollInfo GetDataByEquipmentID(int equipmentID)
  {
    ConfigEquipmentScrollInfo equipmentScrollInfo;
    this.dicByEquipmentId.TryGetValue(equipmentID, out equipmentScrollInfo);
    return equipmentScrollInfo;
  }

  public List<ConfigEquipmentScrollInfo> GetScrollListByEquipmentType(int type, BagType bagType)
  {
    if (this.dicByEquipmentType == null)
    {
      this.dicByEquipmentType = new Dictionary<int, List<ConfigEquipmentScrollInfo>>();
      Dictionary<int, ConfigEquipmentScrollInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator();
      while (enumerator.MoveNext())
      {
        ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(enumerator.Current.Value.equipmenID);
        if (!this.dicByEquipmentType.ContainsKey(data.type))
          this.dicByEquipmentType.Add(data.type, new List<ConfigEquipmentScrollInfo>());
        this.dicByEquipmentType[data.type].Add(enumerator.Current.Value);
      }
    }
    if (this.dicByEquipmentType.ContainsKey(type))
      return this.dicByEquipmentType[type];
    return new List<ConfigEquipmentScrollInfo>();
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, ConfigEquipmentScrollInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, ConfigEquipmentScrollInfo>) null;
  }
}
