﻿// Decompiled with JetBrains decompiler
// Type: CityTimerbar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class CityTimerbar : SceneUI, IRecycle
{
  private static readonly Color32 BLUE_COLOR = new Color32((byte) 41, (byte) 202, byte.MaxValue, byte.MaxValue);
  private static readonly Color32 PURPLE_COLOR = new Color32((byte) 116, (byte) 94, byte.MaxValue, byte.MaxValue);
  public float totalProgressWidth = 370f;
  private TimerType m_TimerType = TimerType.TIMER_CONSTRUCT;
  public UILabel mTimeLabel;
  public UILabel mDescription;
  public UILabel mDetail;
  public UISprite mIcon;
  public UISprite mTimerBar;
  public GameObject mTopBtn;
  public GameObject mHelpBtn;
  public GameObject mFreeBtn;
  public GameObject mHealingIcon;
  private JobHandle m_JobHandle;

  public TimerType timerType
  {
    get
    {
      return this.m_TimerType;
    }
  }

  public bool sync
  {
    get
    {
      return true;
    }
  }

  public bool IsDragonHatching
  {
    get
    {
      return Utils.IsDragonHatchingJob(this.m_JobHandle);
    }
  }

  public void SetDetails(long jobId, string targetName, TimerType timerType, string detail = null)
  {
    this.m_JobHandle = JobManager.Instance.GetJob(jobId);
    if (this.m_JobHandle == null)
      return;
    this.m_TimerType = timerType;
    this.mDescription.text = string.Empty;
    this.mDetail.text = string.Empty;
    TimerType timerType1 = timerType;
    switch (timerType1)
    {
      case TimerType.TIMER_CONSTRUCT:
        this.mIcon.spriteName = "icon_city_building";
        break;
      case TimerType.TIMER_UPGRADE:
        this.mIcon.spriteName = "icon_city_upgrade";
        break;
      case TimerType.TIMER_DESTRUCT:
        this.mIcon.spriteName = "icon_city_building";
        break;
      case TimerType.TIMER_TRAIN:
      case TimerType.TIMER_TRAP_BUILD:
        this.mIcon.spriteName = "icon_city_training";
        string uniqueId = (string) null;
        string str = (string) null;
        Hashtable data1 = this.m_JobHandle.Data as Hashtable;
        if (data1.ContainsKey((object) "troop_class"))
          uniqueId = data1[(object) "troop_class"].ToString();
        if (data1.ContainsKey((object) "troop_count"))
          str = data1[(object) "troop_count"].ToString();
        Unit_StatisticsInfo data2 = ConfigManager.inst.DB_Unit_Statistics.GetData(uniqueId);
        JobEvent jobEvent = this.m_JobHandle.GetJobEvent();
        if (data2 != null)
        {
          if (jobEvent == JobEvent.JOB_UPGRADE_TROOPS)
          {
            this.mDetail.text = ScriptLocalization.GetWithPara("barracks_on_upgrade_amount", new Dictionary<string, string>()
            {
              {
                "0",
                Utils.XLAT(data2.Troop_Name_LOC_ID)
              },
              {
                "1",
                str.ToString()
              }
            }, true);
            break;
          }
          this.mDetail.text = Utils.XLAT(data2.Troop_Name_LOC_ID) + " " + str;
          break;
        }
        this.mIcon.spriteName = "icon_dragon_hatching";
        break;
      default:
        switch (timerType1 - 26)
        {
          case TimerType.TIMER_NONE:
            this.mIcon.spriteName = "icon_research_progress";
            this.mDetail.text = ScriptLocalization.GetWithPara("timer_healing_quantity", new Dictionary<string, string>()
            {
              {
                "0",
                PlayerData.inst.playerCityData.hospitalTroops.healingTroopsCount.ToString()
              }
            }, true);
            break;
          case TimerType.TIMER_CONSTRUCT:
            ConfigEquipmentMainInfo data3 = ConfigManager.inst.GetEquipmentMain(BagType.Hero).GetData(int.Parse((this.m_JobHandle.Data as Hashtable)[(object) "item_property_id"].ToString()));
            this.mDetail.text = Utils.GetColoredString(data3.LocName, (Color32) EquipmentConst.QULITY_COLOR_MAP[data3.quality]);
            break;
          case TimerType.TIMER_UPGRADE:
            ConfigEquipmentMainInfo data4 = ConfigManager.inst.GetEquipmentMain(BagType.DragonKnight).GetData(int.Parse((this.m_JobHandle.Data as Hashtable)[(object) "item_property_id"].ToString()));
            this.mDetail.text = Utils.GetColoredString(data4.LocName, (Color32) EquipmentConst.QULITY_COLOR_MAP[data4.quality]);
            break;
          default:
            if (timerType1 == TimerType.TIMER_RESEARCH)
            {
              this.mIcon.spriteName = "icon_research_progress";
              if (ResearchManager.inst.CurrentResearch != null)
              {
                this.mDetail.text = ResearchManager.inst.CurrentResearch.Name;
                break;
              }
              break;
            }
            break;
        }
    }
    this.mTimerBar.width = 0;
    this.mTimeLabel.text = string.Empty;
    this.OnUpdate((double) NetServerTime.inst.ServerTimestamp);
  }

  public void OnInitialize()
  {
  }

  private void ChangeFreeState(bool b)
  {
    this.mFreeBtn.SetActive(b);
    this.mTimerBar.color = (Color) (!b ? CityTimerbar.BLUE_COLOR : CityTimerbar.PURPLE_COLOR);
  }

  public void OnFinalize()
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) this)
      return;
    this.ShiftBoxCollider(false);
    this.ChangeFreeState(false);
    this.mHelpBtn.SetActive(false);
    this.mDetail.text = string.Empty;
    this.m_JobHandle = (JobHandle) null;
  }

  public void OnUpdate(double dt)
  {
    if (this.m_JobHandle == null || this.m_JobHandle.IsNull())
      return;
    int num = (int) ((double) this.totalProgressWidth * ((double) this.m_JobHandle.Duration() - this.m_JobHandle.LeftTimeInMillisecond()) / (double) this.m_JobHandle.Duration());
    if ((double) num > (double) this.totalProgressWidth)
      num = (int) this.totalProgressWidth;
    this.mTimerBar.width = num;
    this.mTimeLabel.text = Utils.ConvertSecsToString((double) this.m_JobHandle.LeftTime());
    if (this.m_JobHandle.LeftTime() <= 0)
    {
      if (!TutorialManager.Instance.IsRunning)
        return;
      BuildingController componentInParent = this.transform.GetComponentInParent<BuildingController>();
      if (!((UnityEngine.Object) null != (UnityEngine.Object) componentInParent))
        return;
      JobManager.Instance.RemoveJob(this.m_JobHandle.GetJobID());
      this.UpdateFakeBuildingDB(componentInParent);
      componentInParent.UpgradeFinished(true);
    }
    else if (this.CanFree())
    {
      this.ChangeFreeState(true);
      this.mHelpBtn.SetActive(false);
      this.ShiftBoxCollider(true);
    }
    else
    {
      this.ChangeFreeState(false);
      if (this.CanHelp())
      {
        this.mHelpBtn.SetActive(true);
        this.ShiftBoxCollider(true);
      }
      else
      {
        this.mHelpBtn.SetActive(false);
        this.ShiftBoxCollider(false);
      }
    }
  }

  private void UpdateFakeBuildingDB(BuildingController bc)
  {
    long updateTime = (long) (NetServerTime.inst.UpdateTime * 1000.0);
    int num1 = 0;
    int num2 = 0;
    string mType = bc.mBuildingItem.mType;
    if (mType != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CityTimerbar.\u003C\u003Ef__switch\u0024map3D == null)
      {
        // ISSUE: reference to a compiler-generated field
        CityTimerbar.\u003C\u003Ef__switch\u0024map3D = new Dictionary<string, int>(3)
        {
          {
            "lumber_mill",
            0
          },
          {
            "barracks",
            1
          },
          {
            "farm",
            2
          }
        };
      }
      int num3;
      // ISSUE: reference to a compiler-generated field
      if (CityTimerbar.\u003C\u003Ef__switch\u0024map3D.TryGetValue(mType, out num3))
      {
        switch (num3)
        {
          case 0:
            num1 = 7;
            num2 = 10;
            break;
          case 1:
            num1 = 99;
            num2 = 144;
            Hashtable hashtable1 = new Hashtable();
            hashtable1[(object) "building"] = (object) 20;
            Hashtable hashtable2 = new Hashtable();
            hashtable2[(object) "trn8"] = (object) hashtable1;
            hashtable2[(object) "uid"] = (object) PlayerData.inst.uid;
            DBManager.inst.DB_Local_Benefit.Update((object) hashtable2, updateTime, false);
            break;
          case 2:
            num1 = 7;
            num2 = 10;
            break;
        }
      }
    }
    Hashtable hashtable = new Hashtable();
    hashtable[(object) "uid"] = (object) PlayerData.inst.uid;
    hashtable[(object) "city_id"] = (object) CityManager.inst.GetCityID();
    hashtable[(object) "type_id"] = (object) bc.mBuildingItem.mType;
    hashtable[(object) "level"] = (object) 1;
    hashtable[(object) "building_id"] = (object) bc.mBuildingItem.mID;
    hashtable[(object) "job_id"] = (object) 0;
    DBManager.inst.DB_CityMap.Update((object) hashtable, updateTime);
  }

  private void ShiftBoxCollider(bool b)
  {
    BoxCollider component = this.transform.parent.GetComponent<BoxCollider>();
    if (!((UnityEngine.Object) null != (UnityEngine.Object) component) || component.enabled != b)
      return;
    component.enabled = !b;
  }

  private bool CanFree()
  {
    if (this.m_JobHandle == null || !this.AllowFree() || !this.sync)
      return false;
    return (double) this.m_JobHandle.LeftTime() < this.EstablishFreeTimeSecs();
  }

  private bool AllowFree()
  {
    switch (this.m_JobHandle.GetJobEvent())
    {
      case JobEvent.JOB_TRAIN_TROOP:
      case JobEvent.JOB_RESEARCH:
      case JobEvent.JOB_HEALING_TROOPS:
      case JobEvent.JOB_FORGE:
      case JobEvent.JOB_DK_FORGE:
      case JobEvent.JOB_UPGRADE_TROOPS:
        return false;
      default:
        return true;
    }
  }

  private bool CanHelp()
  {
    if (this.m_JobHandle == null || this.m_JobHandle.IsHelped() || (!this.AllowHelp() || !this.sync) || this.m_JobHandle.IsHelped())
      return false;
    AllianceData allianceData = PlayerData.inst.allianceData;
    if (allianceData == null || !allianceData.helps.accessible)
      return false;
    long allianceId = allianceData.allianceId;
    long uid = PlayerData.inst.uid;
    long job_id = !this.m_JobHandle.IsRealJob ? this.m_JobHandle.GetJobID() : this.m_JobHandle.ServerJobID;
    return allianceData.helps.Get(allianceId, uid, job_id) == null;
  }

  private bool AllowHelp()
  {
    JobEvent jobEvent = this.m_JobHandle.GetJobEvent();
    switch (jobEvent)
    {
      case JobEvent.JOB_BUILDING_CONSTRUCTION:
      case JobEvent.JOB_BUILDING_DECONSTRUCTION:
      case JobEvent.JOB_RESEARCH:
        return true;
      default:
        if (jobEvent != JobEvent.JOB_HEALING_TROOPS && jobEvent != JobEvent.JOB_FORGE && jobEvent != JobEvent.JOB_DK_FORGE)
          return false;
        goto case JobEvent.JOB_BUILDING_CONSTRUCTION;
    }
  }

  public void OnSpeedUpBtnPressed()
  {
    UIManager.inst.OpenPopup("SpeedUpPopup", (Popup.PopupParameter) new SpeedUpPopUp.Parameter()
    {
      jobId = this.m_JobHandle.GetJobID(),
      speedupType = ItemBag.ItemType.speedup,
      timerType = this.m_TimerType,
      tarBC = this.GetComponentInParent<BuildingController>()
    });
  }

  public void OnFreeBtnPressed()
  {
    long job_id = this.FreeEndBuild();
    if (job_id == 0L)
      return;
    ItemBag.Instance.UseFreeSpeedup(job_id, (System.Action<bool, object>) ((ret, obj) =>
    {
      if (!ret)
        return;
      this.OnFinalize();
    }));
  }

  public long FreeEndBuild()
  {
    if (this.m_JobHandle == null)
      return 0;
    long jobId = this.m_JobHandle.GetJobID();
    if (this.m_TimerType != TimerType.TIMER_CONSTRUCT && this.m_TimerType != TimerType.TIMER_UPGRADE)
      return jobId;
    JobHandle job = JobManager.Instance.GetJob(jobId);
    long num = job != null ? job.ServerJobID : 0L;
    if (num == 0L)
      num = jobId;
    return num;
  }

  public void OnHelpBtnPressed()
  {
    if (this.m_JobHandle == null || !this.sync || (!this.AllowHelp() || this.m_JobHandle.IsFinished()))
      return;
    AllianceManager.Instance.AskHelp(!this.m_JobHandle.IsRealJob ? this.m_JobHandle.GetJobID() : this.m_JobHandle.ServerJobID, new System.Action<bool, object>(this.HelpRequestCallback));
  }

  private void HelpRequestCallback(bool ret, object data)
  {
    if (ret)
    {
      this.mHelpBtn.SetActive(false);
      this.ShiftBoxCollider(false);
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_action_help_request_sent", true), (System.Action) null, 4f, false);
      AudioManager.Instance.StopAndPlaySound("sfx_alliance_help_request");
    }
    else
      UIManager.inst.toast.Show("Help request failed.", (System.Action) null, 4f, false);
  }

  private double EstablishFreeTimeSecs()
  {
    return (double) ConfigManager.inst.DB_BenefitCalc.GetFinalData(ConfigManager.inst.DB_GameConfig.GetData("default_free_speedup_time_value").ValueInt, "calc_speedup_time");
  }

  private void Update()
  {
    this.ScaleInCity();
  }

  public override void Open(Transform parent)
  {
    base.Open(parent);
    Vector3 localPosition = this.transform.localPosition;
    localPosition.z += -1f;
    this.transform.localPosition = localPosition;
  }
}
