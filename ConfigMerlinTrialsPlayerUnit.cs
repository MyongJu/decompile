﻿// Decompiled with JetBrains decompiler
// Type: ConfigMerlinTrialsPlayerUnit
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigMerlinTrialsPlayerUnit
{
  private readonly List<MerlinTrialsPlayerUnitInfo> _dataList = new List<MerlinTrialsPlayerUnitInfo>();
  private Dictionary<string, MerlinTrialsPlayerUnitInfo> _dicByInternalId;
  private Dictionary<int, MerlinTrialsPlayerUnitInfo> _dicById;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<MerlinTrialsPlayerUnitInfo, string>(res as Hashtable, "id", out this._dicByInternalId, out this._dicById);
    Dictionary<string, MerlinTrialsPlayerUnitInfo>.ValueCollection.Enumerator enumerator = this._dicByInternalId.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._dataList.Add(enumerator.Current);
    }
  }

  public void Clear()
  {
    if (this._dicByInternalId != null)
      this._dicByInternalId.Clear();
    if (this._dicById != null)
      this._dicById.Clear();
    if (this._dataList == null)
      return;
    this._dataList.Clear();
  }

  public List<MerlinTrialsPlayerUnitInfo> GetInfoList()
  {
    return this._dataList;
  }

  public MerlinTrialsPlayerUnitInfo Get(int interalId)
  {
    if (this._dicById.ContainsKey(interalId))
      return this._dicById[interalId];
    return (MerlinTrialsPlayerUnitInfo) null;
  }

  public MerlinTrialsPlayerUnitInfo Get(string id)
  {
    if (this._dicByInternalId.ContainsKey(id))
      return this._dicByInternalId[id];
    return (MerlinTrialsPlayerUnitInfo) null;
  }
}
