﻿// Decompiled with JetBrains decompiler
// Type: BlockHeader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.IO;

public struct BlockHeader
{
  public int terrainOffset;
  public int terrainCount;
  public int referenceOffset;
  public int referenceCount;

  public void Read(BinaryReader reader)
  {
    this.terrainOffset = reader.ReadInt32();
    this.terrainCount = reader.ReadInt32();
    this.referenceOffset = reader.ReadInt32();
    this.referenceCount = reader.ReadInt32();
  }

  public void Write(BinaryWriter writer)
  {
    writer.Write(this.terrainOffset);
    writer.Write(this.terrainCount);
    writer.Write(this.referenceOffset);
    writer.Write(this.referenceCount);
  }
}
