﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightSkillInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;

public class DragonKnightSkillInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "stage")]
  public int stage;
  [Config(Name = "unlock_level")]
  public int unlockLevel;
  [Config(Name = "sort")]
  public int sort;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "description")]
  public string description;
  [Config(Name = "image")]
  public string image;
  [Config(CustomParse = true, CustomType = typeof (Benefits), Name = "Benefits")]
  public Benefits benefits;

  public List<Benefits.BenefitValuePair> GetSkillBenefits()
  {
    if (this.benefits != null)
      return this.benefits.GetBenefitsPairs();
    return (List<Benefits.BenefitValuePair>) null;
  }

  public string Name
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }

  public string Description
  {
    get
    {
      return Utils.XLAT(this.description);
    }
  }

  public string ImagePath
  {
    get
    {
      return "Texture/DragonKnight/SkillIcons/" + this.image;
    }
  }

  public int CurrentStage
  {
    get
    {
      int result = -1;
      DragonKnightSkillStageInfo knightSkillStageInfo = ConfigManager.inst.DB_DragonKnightSkillStage.Get(this.stage);
      if (knightSkillStageInfo != null)
        int.TryParse(knightSkillStageInfo.id.Replace("dragon_knight_skill_stage_", string.Empty), out result);
      return result;
    }
  }

  public int CurrentStageMaxLevel
  {
    get
    {
      DragonKnightSkillStageInfo knightSkillStageInfo = ConfigManager.inst.DB_DragonKnightSkillStage.Get(this.stage);
      if (knightSkillStageInfo != null)
        return knightSkillStageInfo.maxLevel;
      return 0;
    }
  }

  public DragonKnightSkillInfo.SkillState CurrentState
  {
    get
    {
      DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
      if (dragonKnightData == null)
        return DragonKnightSkillInfo.SkillState.LOCKED;
      if (dragonKnightData.Level >= this.unlockLevel && dragonKnightData.Level >= this.CurrentStageMaxLevel)
        return DragonKnightSkillInfo.SkillState.MASTERED;
      return dragonKnightData.Level < this.unlockLevel ? DragonKnightSkillInfo.SkillState.LOCKED : DragonKnightSkillInfo.SkillState.UNLOCKED;
    }
  }

  public string CurrentStateDesc
  {
    get
    {
      DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
      switch (this.CurrentState)
      {
        case DragonKnightSkillInfo.SkillState.MASTERED:
          return "[3FC526]" + Utils.XLAT("id_mastered") + "[-]";
        case DragonKnightSkillInfo.SkillState.UNLOCKED:
          return ScriptLocalization.GetWithPara("id_lv_num", new Dictionary<string, string>()
          {
            {
              "0",
              dragonKnightData == null ? this.unlockLevel.ToString() : dragonKnightData.Level.ToString()
            }
          }, true);
        case DragonKnightSkillInfo.SkillState.LOCKED:
          return "[FF0000]" + Utils.XLAT("id_locked") + "[-]";
        default:
          return string.Empty;
      }
    }
  }

  public enum SkillState
  {
    MASTERED,
    UNLOCKED,
    LOCKED,
  }
}
