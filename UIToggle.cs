﻿// Decompiled with JetBrains decompiler
// Type: UIToggle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using AnimationOrTween;
using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("NGUI/Interaction/Toggle")]
public class UIToggle : UIWidgetContainer
{
  public static BetterList<UIToggle> list = new BetterList<UIToggle>();
  public List<EventDelegate> onChange = new List<EventDelegate>();
  [SerializeField]
  [HideInInspector]
  private string functionName = "OnActivate";
  private bool mIsActive = true;
  public static UIToggle current;
  public int group;
  public UIWidget activeSprite;
  public UnityEngine.Animation activeAnimation;
  public bool startsActive;
  public bool instantTween;
  public bool optionCanBeNone;
  public UIToggle.Validate validator;
  [SerializeField]
  [HideInInspector]
  private UISprite checkSprite;
  [HideInInspector]
  [SerializeField]
  private UnityEngine.Animation checkAnimation;
  [SerializeField]
  [HideInInspector]
  private GameObject eventReceiver;
  [HideInInspector]
  [SerializeField]
  private bool startsChecked;
  private bool mStarted;

  public bool value
  {
    get
    {
      if (this.mStarted)
        return this.mIsActive;
      return this.startsActive;
    }
    set
    {
      if (!this.mStarted)
      {
        this.startsActive = value;
      }
      else
      {
        if (this.group != 0 && !value && (!this.optionCanBeNone && this.mStarted))
          return;
        this.Set(value);
      }
    }
  }

  [Obsolete("Use 'value' instead")]
  public bool isChecked
  {
    get
    {
      return this.value;
    }
    set
    {
      this.value = value;
    }
  }

  public static UIToggle GetActiveToggle(int group)
  {
    for (int index = 0; index < UIToggle.list.size; ++index)
    {
      UIToggle uiToggle = UIToggle.list[index];
      if ((UnityEngine.Object) uiToggle != (UnityEngine.Object) null && uiToggle.group == group && uiToggle.mIsActive)
        return uiToggle;
    }
    return (UIToggle) null;
  }

  private void OnEnable()
  {
    UIToggle.list.Add(this);
  }

  private void OnDisable()
  {
    UIToggle.list.Remove(this);
  }

  private void Start()
  {
    if (this.startsChecked)
    {
      this.startsChecked = false;
      this.startsActive = true;
    }
    if (!Application.isPlaying)
    {
      if ((UnityEngine.Object) this.checkSprite != (UnityEngine.Object) null && (UnityEngine.Object) this.activeSprite == (UnityEngine.Object) null)
      {
        this.activeSprite = (UIWidget) this.checkSprite;
        this.checkSprite = (UISprite) null;
      }
      if ((UnityEngine.Object) this.checkAnimation != (UnityEngine.Object) null && (UnityEngine.Object) this.activeAnimation == (UnityEngine.Object) null)
      {
        this.activeAnimation = this.checkAnimation;
        this.checkAnimation = (UnityEngine.Animation) null;
      }
      if (Application.isPlaying && (UnityEngine.Object) this.activeSprite != (UnityEngine.Object) null)
        this.activeSprite.alpha = !this.startsActive ? 0.0f : 1f;
      if (!EventDelegate.IsValid(this.onChange))
        return;
      this.eventReceiver = (GameObject) null;
      this.functionName = (string) null;
    }
    else
    {
      this.mIsActive = !this.startsActive;
      this.mStarted = true;
      bool instantTween = this.instantTween;
      this.instantTween = true;
      this.Set(this.startsActive);
      this.instantTween = instantTween;
    }
  }

  private void OnClick()
  {
    if (!this.enabled)
      return;
    this.value = !this.value;
  }

  public void Set(bool state)
  {
    if (this.validator != null && !this.validator(state))
      return;
    if (!this.mStarted)
    {
      this.mIsActive = state;
      this.startsActive = state;
      if (!((UnityEngine.Object) this.activeSprite != (UnityEngine.Object) null))
        return;
      this.activeSprite.alpha = !state ? 0.0f : 1f;
    }
    else
    {
      if (this.mIsActive == state)
        return;
      if (this.group != 0 && state)
      {
        int index = 0;
        int size = UIToggle.list.size;
        while (index < size)
        {
          UIToggle uiToggle = UIToggle.list[index];
          if ((UnityEngine.Object) uiToggle != (UnityEngine.Object) this && uiToggle.group == this.group)
            uiToggle.Set(false);
          if (UIToggle.list.size != size)
          {
            size = UIToggle.list.size;
            index = 0;
          }
          else
            ++index;
        }
      }
      this.mIsActive = state;
      if ((UnityEngine.Object) this.activeSprite != (UnityEngine.Object) null)
      {
        if (this.instantTween || !NGUITools.GetActive((Behaviour) this))
          this.activeSprite.alpha = !this.mIsActive ? 0.0f : 1f;
        else
          TweenAlpha.Begin(this.activeSprite.gameObject, 0.15f, !this.mIsActive ? 0.0f : 1f, 0.0f);
      }
      if ((UnityEngine.Object) UIToggle.current == (UnityEngine.Object) null)
      {
        UIToggle current = UIToggle.current;
        UIToggle.current = this;
        if (EventDelegate.IsValid(this.onChange))
          EventDelegate.Execute(this.onChange);
        else if ((UnityEngine.Object) this.eventReceiver != (UnityEngine.Object) null && !string.IsNullOrEmpty(this.functionName))
          this.eventReceiver.SendMessage(this.functionName, (object) this.mIsActive, SendMessageOptions.DontRequireReceiver);
        UIToggle.current = current;
      }
      if (!((UnityEngine.Object) this.activeAnimation != (UnityEngine.Object) null))
        return;
      ActiveAnimation activeAnimation = ActiveAnimation.Play(this.activeAnimation, (string) null, !state ? AnimationOrTween.Direction.Reverse : AnimationOrTween.Direction.Forward, EnableCondition.IgnoreDisabledState, DisableCondition.DoNotDisable);
      if (!((UnityEngine.Object) activeAnimation != (UnityEngine.Object) null) || !this.instantTween && NGUITools.GetActive((Behaviour) this))
        return;
      activeAnimation.Finish();
    }
  }

  public delegate bool Validate(bool choice);
}
