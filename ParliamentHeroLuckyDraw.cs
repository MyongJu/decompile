﻿// Decompiled with JetBrains decompiler
// Type: ParliamentHeroLuckyDraw
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ParliamentHeroLuckyDraw : MonoBehaviour
{
  public UITexture m_ItemIcon;
  public UILabel m_ItemName;
  public UILabel m_ItemCount;

  public void SetData(ItemStaticInfo itemStaticInfo, int count)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.m_ItemName.text = itemStaticInfo.LocName;
    EquipmentManager.Instance.ConfigQualityLabelWithColor(this.m_ItemName, itemStaticInfo.Quality);
    this.m_ItemCount.text = count.ToString();
  }
}
