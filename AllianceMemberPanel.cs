﻿// Decompiled with JetBrains decompiler
// Type: AllianceMemberPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class AllianceMemberPanel : MonoBehaviour
{
  public UILabel mPanelMemberName;
  public UILabel mPanelLevel;
  public UILabel mPanelPower;
  public UISprite mOnlineStateOn;
  public UISprite mOnlineStateOff;
  public UIButton mSendBtn;
  private AllianceMemberData m_MemberData;
  private UserData m_UserData;
  private CityData m_CityData;
  private System.Action<AllianceMemberData> m_Callback;

  public void SetDetails(AllianceMemberData member, System.Action<AllianceMemberData> callback)
  {
    this.m_Callback = callback;
    this.m_MemberData = member;
    this.m_UserData = DBManager.inst.DB_User.Get(member.uid);
    this.m_CityData = DBManager.inst.DB_City.GetByUid(member.uid);
    this.mPanelMemberName.text = this.m_UserData.userName;
    this.mPanelPower.text = Utils.FormatThousands(this.m_UserData.power.ToString());
    this.mPanelLevel.text = this.m_CityData.level.ToString();
    this.mOnlineStateOff.gameObject.SetActive(!this.m_UserData.online);
    this.mOnlineStateOn.gameObject.SetActive(this.m_UserData.online);
    if (!((UnityEngine.Object) this.mSendBtn != (UnityEngine.Object) null) || member.uid != PlayerData.inst.uid)
      return;
    this.mSendBtn.isEnabled = false;
  }

  public void OnMemberBtnPressed()
  {
    if (this.m_Callback == null)
      return;
    this.m_Callback(this.m_MemberData);
  }

  public AllianceMemberData member
  {
    get
    {
      return this.m_MemberData;
    }
  }

  public CityData city
  {
    get
    {
      return this.m_CityData;
    }
  }

  public UserData user
  {
    get
    {
      return this.m_UserData;
    }
  }

  public bool UpdateOnlineStatus(List<long> onlineStatus)
  {
    this.m_UserData.online = false;
    for (int index = 0; index < onlineStatus.Count; ++index)
    {
      if (onlineStatus[index] == this.m_UserData.channelId)
      {
        this.m_UserData.online = true;
        break;
      }
    }
    this.mOnlineStateOff.gameObject.SetActive(!this.m_UserData.online);
    this.mOnlineStateOn.gameObject.SetActive(this.m_UserData.online);
    return this.m_UserData.online;
  }
}
