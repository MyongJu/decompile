﻿// Decompiled with JetBrains decompiler
// Type: OperationTrace
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class OperationTrace
{
  public static List<string> allPageNeedTrace = new List<string>()
  {
    "VipBaseDlg(Clone)",
    "GetMoreResDlg(Clone)",
    "IAPStoreDialog(Clone)",
    "IAPStoreMonthlyDisountDlg(Clone)",
    "IAPRecommendedPackageDialog(Clone)",
    "ActivityMainDlg(Clone)",
    "MonthCardPopupB(Clone)",
    "SubscibePopup(Clone)",
    "SignPopup(Clone)",
    "IAPDailyRechargePopup(Clone)"
  };

  public static void TraceClickCircleMenu(string key)
  {
    OperationTrace.TraceClick(string.Format("{0}{1}", (object) ClickArea.ClickCircleMenuPrefix, (object) key));
  }

  public static void TraceClickCityBuilding(string buildingType)
  {
    OperationTrace.TraceClick(string.Format("{0}{1}", (object) ClickArea.ClickBuildingPrefix, (object) buildingType));
  }

  public static void TraceClick(string areaId)
  {
    Logger.Log((Color32) Color.green, "OperationTrace::TraceClick({0})", (object) areaId);
    Hashtable hashtable = new Hashtable();
    hashtable[(object) "area_id"] = (object) areaId;
    FunplusBi.Instance.TraceAction("click_area", Utils.Object2Json((object) hashtable));
  }

  public static void TraceOpenPage(string pageId, bool autoOpen)
  {
    if (!OperationTrace.allPageNeedTrace.Contains(pageId))
      return;
    Logger.Log((Color32) Color.green, "OperationTrace::TraceOpenPage({0}, {1})", (object) pageId, (object) autoOpen);
    Hashtable hashtable = new Hashtable();
    hashtable[(object) "page_id"] = (object) pageId;
    hashtable[(object) "auto_open"] = (object) (!autoOpen ? 0 : 1);
    FunplusBi.Instance.TraceAction("open_page", Utils.Object2Json((object) hashtable));
  }

  public static void TraceClosePage(string pageId, bool autoOpen)
  {
    if (!OperationTrace.allPageNeedTrace.Contains(pageId))
      return;
    Logger.Log((Color32) Color.green, "OperationTrace::TraceClosePage({0}, {1})", (object) pageId, (object) autoOpen);
    Hashtable hashtable = new Hashtable();
    hashtable[(object) "page_id"] = (object) pageId;
    hashtable[(object) "auto_open"] = (object) (!autoOpen ? 0 : 1);
    FunplusBi.Instance.TraceAction("close_page", Utils.Object2Json((object) hashtable));
  }

  public static void TraceViewShopItemDetail(string page, string packageId, int packageOrder, int totalPackageCount, double price, bool isSubscribe, double discount, long remainTime)
  {
    Logger.Log((Color32) Color.green, "OperationTrace::TraceViewShopItemDetail({0},{1},{2},{3},{4},{5},{6},{7})", (object) page, (object) packageId, (object) packageOrder, (object) totalPackageCount, (object) price, (object) isSubscribe, (object) discount, (object) remainTime);
    Hashtable hashtable = new Hashtable();
    hashtable[(object) "page_id"] = (object) page;
    hashtable[(object) "package_id"] = (object) packageId;
    hashtable[(object) "package_order"] = (object) packageOrder;
    hashtable[(object) "total_package_count"] = (object) totalPackageCount;
    hashtable[(object) nameof (price)] = (object) price;
    hashtable[(object) "is_subscriber"] = (object) (!isSubscribe ? 0 : 1);
    hashtable[(object) nameof (discount)] = (object) discount;
    hashtable[(object) "remain_time"] = (object) remainTime;
    FunplusBi.Instance.TraceAction("view_shop_item_detail", Utils.Object2Json((object) hashtable));
  }
}
