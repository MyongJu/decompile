﻿// Decompiled with JetBrains decompiler
// Type: QuestEmpireDetailsDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class QuestEmpireDetailsDlg : UI.Dialog
{
  private List<QuestRewardSlotEmpire> _items = new List<QuestRewardSlotEmpire>();
  public UILabel dialogTitle;
  public UILabel questInstructionsTitle;
  public UILabel questInstructions;
  public UITable rewardsTable;
  public UIButton claimRewardButton;
  public UILabel timerBarDescription;
  public UIProgressBar progress;
  public UIButton goToButton;
  public GameObject normalRewardPrefab;
  public GameObject itemRewardPrefab;
  public QuestRewardSlotEmpire rewardPrefab;
  public UITable itemRewardContainer;
  public UITable normalRewardContainer;
  public QuestIconRenderer iconRenderer;
  private QuestData2 questData;
  private bool addedHandler;
  private bool needToReset;

  public string GetFormattedTime(int inSeconds)
  {
    int num1 = inSeconds / 3600;
    int num2 = inSeconds % 3600 / 60;
    int num3 = inSeconds % 60;
    return string.Format("{0}:{1}:{2}", num1 >= 10 ? (object) num1.ToString() : (object) ("0" + (object) num1), num2 >= 10 ? (object) num2.ToString() : (object) ("0" + (object) num2), num3 >= 10 ? (object) num3.ToString() : (object) ("0" + (object) num3));
  }

  private void OnSecondEvent(int serverTime)
  {
    this.RefreshTimeText();
    this.RefreshProgressBar();
  }

  public void OnClaimClick()
  {
    PlayerData.inst.QuestManager.ClaimQuest(this.questData.QuestID);
    this.questData.Claim();
    this.BackToQuest(true);
  }

  private void DestroyRewards()
  {
    if (this._items.Count <= 0)
      return;
    this._items.Clear();
  }

  public void OnGotoBtnClickHandler()
  {
    QuestLinkHUDInfo questLinkHudInfo = ConfigManager.inst.DB_QuestLinkHud.GetQuestLinkHudInfo(this.questData.Info.LinkHudId);
    if (questLinkHudInfo == null)
      return;
    LinkerHub.Instance.Distribute(questLinkHudInfo.Param);
  }

  private void RefreshUIState()
  {
  }

  private void Setup()
  {
    this.dialogTitle.text = ScriptLocalization.Get("id_uppercase_quest_detail", true);
    this.questInstructionsTitle.text = ScriptLocalization.Get(this.questData.Info.Loc_Name, true);
    this.questInstructions.text = ScriptLocalization.Get(this.questData.Info.Loc_Desc, true);
    this.goToButton.gameObject.SetActive(false);
    this.iconRenderer.SetData(this.questData.QuestID);
    if (this.questData.Status == 1)
    {
      this.claimRewardButton.gameObject.SetActive(true);
      this.goToButton.gameObject.SetActive(false);
    }
    else
    {
      this.claimRewardButton.gameObject.SetActive(false);
      if (this.questData.Info.LinkHudId > 0)
        this.goToButton.gameObject.SetActive(true);
    }
    this.DestroyRewards();
    for (int index = 0; index < this.questData.Rewards.Count; ++index)
    {
      QuestReward reward = this.questData.Rewards[index];
      GameObject gameObject1 = this.normalRewardContainer.gameObject;
      GameObject prefab = this.normalRewardPrefab;
      if (reward is ConsumableReward || reward is ExperienceReward)
      {
        gameObject1 = this.itemRewardContainer.gameObject;
        prefab = this.itemRewardPrefab;
      }
      GameObject gameObject2 = NGUITools.AddChild(gameObject1, prefab);
      QuestRewardSlotEmpire componentInChildren = gameObject2.GetComponentInChildren<QuestRewardSlotEmpire>();
      gameObject2.SetActive(true);
      componentInChildren.Setup(this.questData.Rewards[index]);
      this._items.Add(componentInChildren);
    }
    this.normalRewardContainer.repositionNow = true;
    this.itemRewardContainer.repositionNow = true;
    this.RefreshProgressBar();
    this.RemoveEventHandlr();
    this.AddEventHandler();
  }

  private void RefreshTimeText()
  {
  }

  public void OnSpeedUpBtnPress()
  {
  }

  private void AddEventHandler()
  {
    if (this.addedHandler)
      return;
    this.addedHandler = true;
    PlayerData.inst.QuestManager.OnQuestCompleted += new System.Action<long>(this.Refresh);
  }

  private void Refresh(long questId)
  {
    if (questId == this.questData.QuestID)
      this.Setup();
    this.needToReset = true;
  }

  private void RemoveEventHandlr()
  {
    if (!this.addedHandler)
      return;
    this.addedHandler = false;
    PlayerData.inst.QuestManager.OnQuestCompleted -= new System.Action<long>(this.Refresh);
  }

  private void Reset()
  {
  }

  private void RefreshProgressBar()
  {
    if (this.questData == null)
      return;
    this.Reset();
    BasePresent present = this.questData.Present;
    if (present == null)
      return;
    present.Refresh();
    this.progress.gameObject.SetActive(true);
    this.progress.value = present.GetProgressBarValue();
    this.timerBarDescription.text = present.GetProgressContent();
  }

  public void CloseDlg()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  protected override void OnBackHandler()
  {
    this.RemoveEventHandlr();
    this.DestroyRewards();
  }

  private void BackToQuest(bool resetScrollView = false)
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    QuestEmpireDetailsDlg.Parameter parameter = orgParam as QuestEmpireDetailsDlg.Parameter;
    if (parameter == null)
      return;
    this.questData = DBManager.inst.DB_Quest.Get(parameter.questId);
    this.gameObject.SetActive(true);
    this.needToReset = false;
    this.Setup();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.RemoveEventHandlr();
    this.DestroyRewards();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandlr();
    this.DestroyRewards();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public long questId;
  }
}
