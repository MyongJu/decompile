﻿// Decompiled with JetBrains decompiler
// Type: BuildingInfoWishWellMoreInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingInfoWishWellMoreInfo : Popup
{
  private List<BuildingInfoWishWellMoreInfoItemRenderer> m_ItemList = new List<BuildingInfoWishWellMoreInfoItemRenderer>();
  public UIGrid m_Grid;
  public UIScrollView m_ScrollView;
  public GameObject m_ItemPrefab;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnCloseButtonPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void ClearData()
  {
    int count = this.m_ItemList.Count;
    for (int index = 0; index < count; ++index)
    {
      BuildingInfoWishWellMoreInfoItemRenderer infoItemRenderer = this.m_ItemList[index];
      infoItemRenderer.transform.parent = (Transform) null;
      Object.Destroy((Object) infoItemRenderer.gameObject);
    }
    this.m_ItemList.Clear();
  }

  private void UpdateUI()
  {
    this.ClearData();
    int levelCount = ConfigManager.inst.DB_WishWell.GetLevelCount();
    for (int index = 0; index < levelCount; ++index)
    {
      int level = index + 1;
      WishWellData wishWellDataByLevel = ConfigManager.inst.DB_WishWell.GetWishWellDataByLevel(level);
      GameObject gameObject = Object.Instantiate<GameObject>(this.m_ItemPrefab);
      gameObject.transform.parent = this.m_Grid.transform;
      gameObject.transform.localScale = Vector3.one;
      gameObject.SetActive(true);
      BuildingInfoWishWellMoreInfoItemRenderer component = gameObject.GetComponent<BuildingInfoWishWellMoreInfoItemRenderer>();
      component.SetData(level, wishWellDataByLevel.Food, wishWellDataByLevel.Wood, wishWellDataByLevel.Ore, wishWellDataByLevel.Silver, wishWellDataByLevel.FreeGem);
      this.m_ItemList.Add(component);
    }
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
  }
}
