﻿// Decompiled with JetBrains decompiler
// Type: SortingOrderModifier
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class SortingOrderModifier : MonoBehaviour
{
  [HideInInspector]
  [SerializeField]
  private string m_SortingLayer = "Default";
  private bool m_Changed = true;
  [SerializeField]
  [HideInInspector]
  private Renderer m_Target;
  [HideInInspector]
  [SerializeField]
  private int m_SortingOrder;

  public Renderer target
  {
    get
    {
      return this.m_Target;
    }
    set
    {
      if (!((Object) this.m_Target != (Object) value))
        return;
      this.m_Target = value;
      this.m_Changed = true;
    }
  }

  public string sortingLayer
  {
    get
    {
      return this.m_SortingLayer;
    }
    set
    {
      if (!(this.m_SortingLayer != value))
        return;
      this.m_SortingLayer = value;
      this.m_Changed = true;
    }
  }

  public int sortingOrder
  {
    get
    {
      return this.m_SortingOrder;
    }
    set
    {
      if (this.m_SortingOrder == value)
        return;
      this.m_SortingOrder = value;
      this.m_Changed = true;
    }
  }

  private void Update()
  {
    if (!((Object) this.m_Target != (Object) null) || !this.m_Changed)
      return;
    this.m_Target.sortingLayerName = this.m_SortingLayer;
    this.m_Target.sortingOrder = this.m_SortingOrder;
    this.m_Changed = false;
  }
}
