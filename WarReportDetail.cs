﻿// Decompiled with JetBrains decompiler
// Type: WarReportDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class WarReportDetail : Popup
{
  private List<GameObject> pool = new List<GameObject>();
  private Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>> attackertroopdetail = new Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>>();
  private Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>> defendertroopdetail = new Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>>();
  private Dictionary<string, Hashtable> attackerdragon = new Dictionary<string, Hashtable>();
  private Dictionary<string, Hashtable> defenderdragon = new Dictionary<string, Hashtable>();
  private Dictionary<string, List<Hashtable>> attackerlegend = new Dictionary<string, List<Hashtable>>();
  private Dictionary<string, List<Hashtable>> defenderlegend = new Dictionary<string, List<Hashtable>>();
  private AbstractMailEntry mail;
  public TroopDetailItem troopDetailItem;
  public GameObject range;
  public GameObject attackerheader;
  public GameObject defenderheader;
  public UILabel notroop;
  private bool needdragon;
  private bool showLegend;

  private void Init()
  {
    float y1 = this.troopDetailItem.transform.localPosition.y;
    using (Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>>.Enumerator enumerator = this.attackertroopdetail.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, Dictionary<string, List<SoldierInfo.Data>>> current = enumerator.Current;
        GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.troopDetailItem.gameObject, this.troopDetailItem.transform.parent);
        this.pool.Add(gameObject);
        Vector3 localPosition = this.troopDetailItem.transform.localPosition;
        localPosition.y = y1;
        gameObject.transform.localPosition = localPosition;
        TroopDetailItem.Data d = new TroopDetailItem.Data();
        d.ownername = current.Key;
        d.soldiers = current.Value;
        if (this.attackerdragon.ContainsKey(current.Key))
          d.dragondata = this.attackerdragon[current.Key];
        if (this.attackerlegend.ContainsKey(current.Key))
          d.legends = this.attackerlegend[current.Key];
        d.needdragon = this.needdragon;
        d.showLegend = this.showLegend;
        gameObject.GetComponent<TroopDetailItem>().SeedData(d);
        float y2 = NGUIMath.CalculateRelativeWidgetBounds(gameObject.transform).size.y;
        y1 -= y2 + 10f;
      }
    }
    if (this.attackertroopdetail.Count == 0)
      this.attackerheader.SetActive(false);
    float num1 = y1 - 20f;
    Vector3 localPosition1 = this.defenderheader.transform.localPosition;
    localPosition1.y = num1;
    this.defenderheader.transform.localPosition = localPosition1;
    float num2 = num1 - NGUIMath.CalculateRelativeWidgetBounds(this.defenderheader.transform).size.y;
    bool flag = true;
    if (this.defendertroopdetail.Count == 1)
    {
      using (Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>>.Enumerator enumerator = this.defendertroopdetail.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.Value.Count == 0)
            flag = false;
        }
      }
    }
    if (flag)
    {
      using (Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>>.Enumerator enumerator = this.defendertroopdetail.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, Dictionary<string, List<SoldierInfo.Data>>> current = enumerator.Current;
          GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.troopDetailItem.gameObject, this.troopDetailItem.transform.parent);
          this.pool.Add(gameObject);
          Vector3 localPosition2 = this.troopDetailItem.transform.localPosition;
          localPosition2.y = num2;
          gameObject.transform.localPosition = localPosition2;
          TroopDetailItem.Data d = new TroopDetailItem.Data();
          d.ownername = Utils.XLAT(current.Key);
          d.soldiers = current.Value;
          if (this.defenderdragon.ContainsKey(current.Key))
            d.dragondata = this.defenderdragon[current.Key];
          if (this.defenderlegend.ContainsKey(current.Key))
            d.legends = this.defenderlegend[current.Key];
          d.needdragon = this.needdragon;
          d.showLegend = this.showLegend;
          d.mail = this.mail;
          gameObject.GetComponent<TroopDetailItem>().SeedData(d);
          float y2 = NGUIMath.CalculateRelativeWidgetBounds(gameObject.transform).size.y;
          num2 -= y2 + 10f;
        }
      }
      this.notroop.gameObject.SetActive(false);
    }
    else
    {
      this.notroop.gameObject.SetActive(true);
      Vector3 localPosition2 = this.notroop.transform.localPosition;
      localPosition2.y = num2 - 50f;
      this.notroop.transform.localPosition = localPosition2;
    }
    BoxCollider component = this.range.GetComponent<BoxCollider>();
    Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.range.transform);
    Vector3 size = component.size;
    size.y = relativeWidgetBounds.size.y;
    component.size = size;
    component.center = relativeWidgetBounds.center;
    this.HideTemplate(true);
  }

  public void OnBackBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void Reset()
  {
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        current.GetComponent<TroopDetailItem>().Reset();
        PrefabManagerEx.Instance.Destroy(current);
      }
    }
    this.pool.Clear();
    this.HideTemplate(false);
  }

  private void HideTemplate(bool hide)
  {
    this.troopDetailItem.gameObject.SetActive(!hide);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    WarReportDetail.Parameter parameter = orgParam as WarReportDetail.Parameter;
    if (parameter.pattackertroopdetail != null)
      this.attackertroopdetail = parameter.pattackertroopdetail;
    if (parameter.pdefendertroopdetail != null)
      this.defendertroopdetail = parameter.pdefendertroopdetail;
    if (parameter.pattackerdragon != null)
      this.attackerdragon = parameter.pattackerdragon;
    if (parameter.pdefenderdragon != null)
      this.defenderdragon = parameter.pdefenderdragon;
    if (parameter.pattackerlegend != null)
      this.attackerlegend = parameter.pattackerlegend;
    if (parameter.pdefenderlegend != null)
      this.defenderlegend = parameter.pdefenderlegend;
    if (parameter.mail != null)
      this.mail = parameter.mail;
    this.needdragon = parameter.needDragon;
    this.showLegend = parameter.showLegend;
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }

  public class Parameter : Popup.PopupParameter
  {
    public bool needDragon = true;
    public bool showLegend = true;
    public Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>> pattackertroopdetail;
    public Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>> pdefendertroopdetail;
    public Dictionary<string, Hashtable> pattackerdragon;
    public Dictionary<string, Hashtable> pdefenderdragon;
    public Dictionary<string, List<Hashtable>> pattackerlegend;
    public Dictionary<string, List<Hashtable>> pdefenderlegend;
    public AbstractMailEntry mail;
  }
}
