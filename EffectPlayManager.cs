﻿// Decompiled with JetBrains decompiler
// Type: EffectPlayManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class EffectPlayManager
{
  private const string RESOURCE_PATH = "Prefab/VFX/SkillEffect";
  private static EffectPlayManager _instance;

  public static EffectPlayManager inst
  {
    get
    {
      if (EffectPlayManager._instance == null)
        EffectPlayManager._instance = new EffectPlayManager();
      return EffectPlayManager._instance;
    }
  }

  public void Init()
  {
  }

  public void Dispose()
  {
  }

  public void Play(EffectData data)
  {
    string effectResourceName = data.GetEffectResourceName();
    if (string.IsNullOrEmpty(effectResourceName))
      return;
    SimpleVfx simpleVfx = VfxManager.Instance.Create(string.Format("{0}/{1}", (object) "Prefab/VFX/SkillEffect", (object) effectResourceName)) as SimpleVfx;
    simpleVfx.offset = PVPSystem.Instance.Map.transform.InverseTransformPoint(data.GetPosition());
    simpleVfx.duration = data.GetDuration();
    simpleVfx.Play(PVPSystem.Instance.Map.transform);
  }
}
