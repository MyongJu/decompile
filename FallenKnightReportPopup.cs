﻿// Decompiled with JetBrains decompiler
// Type: FallenKnightReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class FallenKnightReportPopup : BaseReportPopup
{
  private List<GameObject> pool = new List<GameObject>();
  public float gapX = 100f;
  private FallenKnightReportContent wrc;
  public UIScrollView sv;
  public Icon defenderInfo;
  public Icon knightInfo;
  public Icon defenderDetail;
  public UILabel content;

  private void Init()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, true, false, string.Empty);
    this.time.text = Utils.FormatTimeForMail(this.mailtime);
    this.content.text = this.maildata.GetBodyString();
    string path1 = "Texture/Hero/Portrait_Icon/player_portrait_icon_" + this.wrc.defender.portrait;
    string str1 = this.wrc.defender.acronym == null ? (string) null : "(" + this.wrc.defender.acronym + ")";
    string str2 = str1 + this.wrc.defender.name;
    string str3 = " X: " + this.wrc.defender.x + " Y: " + this.wrc.defender.y;
    this.defenderInfo.SetData(path1, this.wrc.defender.icon, this.wrc.defender.lord_title, new string[2]
    {
      str2,
      str3
    });
    UserData userData = DBManager.inst.DB_User.Get(this.wrc.attacker.uid);
    if (userData != null)
    {
      string portraitIconPath = userData.PortraitIconPath;
      string str4 = ScriptLocalization.Get(userData.userName, true);
      string str5 = " X: " + this.wrc.attacker.x + " Y: " + this.wrc.attacker.y;
      long num = (long) (this.wrc.attacker.start_total_troops - this.wrc.attacker.end_total_troops);
      long startTotalTroops = (long) this.wrc.attacker.start_total_troops;
      string str6 = ((float) ((double) num / (double) startTotalTroops * 100.0)).ToString("f1") + "%";
      this.knightInfo.SetData(portraitIconPath, userData.Icon, userData.LordTitle, new string[5]
      {
        str4,
        str5,
        startTotalTroops.ToString(),
        num.ToString(),
        str6
      });
    }
    float y = this.defenderDetail.transform.localPosition.y;
    float num1 = NGUIMath.CalculateRelativeWidgetBounds(this.defenderDetail.transform).size.y + 2f;
    this.wrc.defender_troop_detail.Sort((Comparison<TroopDetail>) ((d1, d2) => d1.uid == PlayerData.inst.uid.ToString() ? -1 : 1));
    using (List<TroopDetail>.Enumerator enumerator = this.wrc.defender_troop_detail.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        string path2 = "Texture/Hero/Portrait_Icon/player_portrait_icon_" + current.portrait;
        string str4 = current.acronym == null ? (string) null : "(" + current.acronym + ")";
        string str5 = str1 + current.name;
        long troopLost = current.CalculateTroopLost();
        current.CalculateTroop();
        string empty = string.Empty;
        this.wrc.kill_rate.TryGetValue(current.uid, out empty);
        long num2 = 0;
        this.wrc.npc_score.TryGetValue(current.uid, out num2);
        GameObject gameObject = Utils.DuplicateGOB(this.defenderDetail.gameObject, this.defenderDetail.transform.parent);
        gameObject.GetComponent<Icon>().SetData(path2, current.icon, current.lord_title, new string[6]
        {
          str5,
          num2.ToString(),
          current.CalculatePowerLost().ToString(),
          troopLost.ToString(),
          current.CalculateTroopWounded().ToString(),
          empty + "%"
        });
        Vector3 localPosition = gameObject.transform.localPosition;
        localPosition.y = y;
        gameObject.transform.localPosition = localPosition;
        y -= num1;
      }
    }
    this.ArrangeLayout();
    this.HideTemplate(true);
  }

  private void ArrangeLayout()
  {
    this.sv.UpdatePosition();
  }

  private void Reset()
  {
    this.sv.ResetPosition();
    Rigidbody component = this.sv.transform.GetComponent<Rigidbody>();
    if ((UnityEngine.Object) null != (UnityEngine.Object) component)
    {
      component.isKinematic = true;
      component.useGravity = false;
    }
    this.HideTemplate(false);
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        PrefabManagerEx.Instance.Destroy(current);
      }
    }
    this.pool.Clear();
  }

  private void HideTemplate(bool hide)
  {
    this.defenderDetail.gameObject.SetActive(!hide);
  }

  public void GotoMycity()
  {
    this.GoToTargetPlace(int.Parse(this.wrc.defender.k), int.Parse(this.wrc.defender.x), int.Parse(this.wrc.defender.y));
  }

  public void GotoEnemycity()
  {
    this.GoToTargetPlace(int.Parse(this.wrc.attacker.k), int.Parse(this.wrc.attacker.x), int.Parse(this.wrc.attacker.y));
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.wrc = JsonReader.Deserialize<FallenKnightReport>(Utils.Object2Json((object) this.param.hashtable)).data;
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }
}
