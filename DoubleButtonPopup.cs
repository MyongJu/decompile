﻿// Decompiled with JetBrains decompiler
// Type: DoubleButtonPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;
using UnityEngine;

public class DoubleButtonPopup : Popup
{
  private System.Action onLeftButtonClick;
  private System.Action onRightButtonClick;
  private System.Action onCloseButtonClick;
  [SerializeField]
  private DoubleButtonPopup.Panel panel;

  public void OnCloseButtonClick()
  {
    if (this.onCloseButtonClick != null)
      this.onCloseButtonClick();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnButtonClick()
  {
    if (this.onLeftButtonClick != null)
      this.onLeftButtonClick();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnLeftButtonClick()
  {
    if (this.onLeftButtonClick != null)
      this.onLeftButtonClick();
    AudioManager.Instance.PlaySound("sfx_ui_comfirm", false);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnRightButtonClick()
  {
    if (this.onRightButtonClick != null)
      this.onRightButtonClick();
    AudioManager.Instance.PlaySound("sfx_ui_cancel", false);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DoubleButtonPopup.Parameter parameter = orgParam as DoubleButtonPopup.Parameter;
    if ((parameter.setType & DoubleButtonPopup.SetType.title) > (DoubleButtonPopup.SetType) 0)
      this.panel.LB_title.text = parameter.title;
    if ((parameter.setType & DoubleButtonPopup.SetType.description) > (DoubleButtonPopup.SetType) 0)
      this.panel.LB_description.text = parameter.description;
    if ((parameter.setType & DoubleButtonPopup.SetType.leftButtonEvent) > (DoubleButtonPopup.SetType) 0)
      this.onLeftButtonClick = parameter.leftButtonClickEvent;
    if ((parameter.setType & DoubleButtonPopup.SetType.leftButtonText) > (DoubleButtonPopup.SetType) 0)
      this.panel.LB_leftButtonText.text = parameter.leftButtonText;
    if ((parameter.setType & DoubleButtonPopup.SetType.rightButtonEvent) > (DoubleButtonPopup.SetType) 0)
      this.onRightButtonClick = parameter.rightButtonClickEvent;
    if ((parameter.setType & DoubleButtonPopup.SetType.rightButtonText) > (DoubleButtonPopup.SetType) 0)
      this.panel.LB_rightButtonText.text = parameter.rightButtonText;
    this.onCloseButtonClick = parameter.closeButtonCallbackEvent;
  }

  [Serializable]
  protected class Panel
  {
    public UILabel LB_title;
    public UILabel LB_description;
    public UILabel LB_leftButtonText;
    public UILabel LB_rightButtonText;
    public UIButton BTN_leftButton;
    public UIButton BTN_rightButton;
    public UIButton BTN_closeButton;
  }

  public enum SetType
  {
    title = 1,
    description = 2,
    leftButtonEvent = 4,
    leftButtonText = 8,
    rightButtonEvent = 16, // 0x00000010
    rightButtonText = 32, // 0x00000020
  }

  public class Parameter : Popup.PopupParameter
  {
    public DoubleButtonPopup.SetType setType = DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.rightButtonEvent;
    public const DoubleButtonPopup.SetType BASIC_TYPE = DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.rightButtonEvent;
    public const DoubleButtonPopup.SetType SET_BUTTON_TEXT = DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText;
    public System.Action leftButtonClickEvent;
    public System.Action rightButtonClickEvent;
    public string leftButtonText;
    public string rightButtonText;
    public string description;
    public string title;
    public System.Action closeButtonCallbackEvent;
  }
}
