﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentSuitGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigEquipmentSuitGroup
{
  private Dictionary<string, ConfigEquipmentSuitGroupInfo> datas;
  private Dictionary<int, ConfigEquipmentSuitGroupInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ConfigEquipmentSuitGroupInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public ConfigEquipmentSuitGroupInfo GetData(int internalID)
  {
    return this.dicByUniqueId[internalID];
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, ConfigEquipmentSuitGroupInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, ConfigEquipmentSuitGroupInfo>) null;
  }
}
