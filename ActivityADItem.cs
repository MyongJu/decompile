﻿// Decompiled with JetBrains decompiler
// Type: ActivityADItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class ActivityADItem : MonoBehaviour
{
  private string timePre = string.Empty;
  public UILabel timeLabel;
  public Icon icon;
  private string pre;
  private ActivityBaseUIData _basedata;
  public GameObject timeContent;

  public void SetData(ActivityBaseUIData basedata)
  {
    if (basedata == this._basedata)
    {
      this._basedata = basedata;
      this.RefreshTime();
    }
    else
    {
      this._basedata = basedata;
      this.timePre = this._basedata.TimePre;
      this.icon.FeedData((IComponentData) this._basedata.GetADIconData());
      this.RemoveEventHandler();
      this.AddEventHandler();
      this.RefreshTime();
    }
  }

  private int GetRemainTime()
  {
    return this._basedata.GetRemainTime();
  }

  private void RefreshTime()
  {
    if (!this._basedata.ShowADTime)
    {
      NGUITools.SetActive(this.timeContent, false);
      this.timeLabel.text = string.Empty;
    }
    else
    {
      Color color = Color.green;
      if (!this._basedata.IsStart())
        color = new Color(0.9921569f, 0.5372549f, 0.0f);
      NGUITools.SetActive(this.timeContent, true);
      int time = this.GetRemainTime();
      if (this._basedata.Type == ActivityBaseUIData.ActivityType.AllianceBoss)
        this.timeLabel.text = ActivityManager.Intance.GetPortalTimeStr();
      else if (this._basedata.Type == ActivityBaseUIData.ActivityType.AlianceWar)
      {
        AllianceWarActivityUIData basedata = this._basedata as AllianceWarActivityUIData;
        if (basedata == null)
          return;
        string locDescription = basedata.GetLocDescription();
        int remainTime = basedata.GetRemainTime();
        if (remainTime < 0)
        {
          this.timeLabel.text = Utils.XLAT(locDescription);
        }
        else
        {
          this.timeLabel.text = ScriptLocalization.GetWithPara(locDescription, new Dictionary<string, string>()
          {
            {
              "0",
              Utils.FormatTime(remainTime, true, true, true)
            }
          }, true);
          if (basedata.AllianceWarData.GetAllianceWarState() == AllianceWarActivityData.AllianceWarState.RegisterFail)
            color = new Color(0.9921569f, 0.5372549f, 0.0f);
        }
      }
      else if (time > 0)
      {
        if (this._basedata is RoundActivityUIData)
          time = (this._basedata as RoundActivityUIData).GetTotalRemainTime();
        this.timeLabel.text = ScriptLocalization.GetWithPara(this.timePre, new Dictionary<string, string>()
        {
          {
            "1",
            Utils.FormatTime(time, true, true, true)
          }
        }, true);
      }
      else
        this.RemoveEventHandler();
      this.timeLabel.color = color;
    }
  }

  public void OnClickHandler()
  {
    this._basedata.DisplayActivityDetail();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  private void Process(int time)
  {
    this.RefreshTime();
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
  }

  public void Clear()
  {
    this.icon.Dispose();
    this.RemoveEventHandler();
  }
}
