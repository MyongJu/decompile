﻿// Decompiled with JetBrains decompiler
// Type: ConfigGveBoss
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigGveBoss
{
  private Dictionary<string, ConfigGveBossData> m_GveBossByUniqueId;
  private Dictionary<int, ConfigGveBossData> m_GveBossByInternalId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ConfigGveBossData, string>(res as Hashtable, "id", out this.m_GveBossByUniqueId, out this.m_GveBossByInternalId);
  }

  public ConfigGveBossData GetData(int internalId)
  {
    ConfigGveBossData configGveBossData = (ConfigGveBossData) null;
    this.m_GveBossByInternalId.TryGetValue(internalId, out configGveBossData);
    return configGveBossData;
  }

  public ConfigGveBossData GetData(string id)
  {
    ConfigGveBossData configGveBossData = (ConfigGveBossData) null;
    this.m_GveBossByUniqueId.TryGetValue(id, out configGveBossData);
    return configGveBossData;
  }

  public void Clear()
  {
    if (this.m_GveBossByUniqueId != null)
    {
      this.m_GveBossByUniqueId.Clear();
      this.m_GveBossByUniqueId = (Dictionary<string, ConfigGveBossData>) null;
    }
    if (this.m_GveBossByInternalId == null)
      return;
    this.m_GveBossByInternalId.Clear();
    this.m_GveBossByInternalId = (Dictionary<int, ConfigGveBossData>) null;
  }
}
