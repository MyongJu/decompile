﻿// Decompiled with JetBrains decompiler
// Type: ChatMiniDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Chat;
using DB;
using System;
using System.Collections;
using UI;
using UnityEngine;

public class ChatMiniDlg : Hud
{
  private bool _isKingdomChannel = true;
  private ChatManager _chatManager;
  private ChatChannel _curChannel;
  [SerializeField]
  private ChatMiniDlg.Panel panel;

  public bool isKingdomChannel
  {
    get
    {
      return this._isKingdomChannel;
    }
    set
    {
      if (this._isKingdomChannel == value)
        return;
      this._isKingdomChannel = value;
      this.panel.kingdomChannelOn.gameObject.SetActive(this._isKingdomChannel);
      this.panel.kingdomChannelOff.gameObject.SetActive(!this._isKingdomChannel);
      this.panel.kingdomIcon.gameObject.SetActive(this._isKingdomChannel);
      this.panel.allianceChannelOn.gameObject.SetActive(!this._isKingdomChannel);
      this.panel.allianceChannleOff.gameObject.SetActive(this._isKingdomChannel);
      this.panel.allianceIcon.gameObject.SetActive(!this._isKingdomChannel);
      if (this._curChannel != null)
        this._curChannel.onNewMessage -= new System.Action<ChatMessage>(this.SetMessage);
      if (this._chatManager != null)
      {
        if (this._isKingdomChannel && this._chatManager.kingdomChannel != null)
          this._curChannel = this._chatManager.kingdomChannel;
        else if (this._chatManager.allianceChannel != null)
          this._curChannel = this._chatManager.allianceChannel;
      }
      this.SetMessage1(this._curChannel.GetLastMessage(0));
      this.SetMessage2(this._curChannel.GetLastMessage(1));
      this._curChannel.onNewMessage = new System.Action<ChatMessage>(this.SetMessage);
    }
  }

  public void ClearMsg()
  {
    UILabel name1 = this.panel.name1;
    string empty = string.Empty;
    this.panel.msg2.text = empty;
    string str1 = empty;
    this.panel.msg1.text = str1;
    string str2 = str1;
    this.panel.name2.text = str2;
    string str3 = str2;
    name1.text = str3;
    this.panel.onlineFriendsCount.text = "0";
    this.panel.unreadMsgCount.text = "0";
  }

  public void SetMessage(ChatMessage message)
  {
    this.SetMessage1(this._curChannel.GetLastMessage(0));
    this.SetMessage2(this._curChannel.GetLastMessage(1));
  }

  public void SetMessage1(ChatMessage message)
  {
    if (message != null)
    {
      this.panel.name1.text = !message.isMod || message.traceData.type != ChatTraceData.TraceType.Empty ? string.Format("{0}:", (object) message.fullName) : string.Format("{0}:", (object) message.senderName);
      this.panel.msg1.text = message.text;
      message.onTranslateEnd -= new System.Action(this.OnTransEnd);
      message.onTranslateEnd += new System.Action(this.OnTransEnd);
    }
    else
    {
      UILabel name1 = this.panel.name1;
      string empty = string.Empty;
      this.panel.msg1.text = empty;
      string str = empty;
      name1.text = str;
    }
    EmojiManager.Instance.ProcessEmojiLabel(this.panel.msg1);
  }

  public void SetMessage2(ChatMessage message)
  {
    if (message != null)
    {
      this.panel.name2.text = !message.isMod || message.traceData.type != ChatTraceData.TraceType.Empty ? string.Format("{0}:", (object) message.fullName) : string.Format("{0}:", (object) message.senderName);
      this.panel.msg2.text = message.text;
      message.onTranslateEnd -= new System.Action(this.OnTransEnd);
      message.onTranslateEnd += new System.Action(this.OnTransEnd);
    }
    else
    {
      UILabel name2 = this.panel.name2;
      string empty = string.Empty;
      this.panel.msg2.text = empty;
      string str = empty;
      name2.text = str;
    }
    EmojiManager.Instance.ProcessEmojiLabel(this.panel.msg2);
  }

  private void OnTransEnd()
  {
    this.SetMessage((ChatMessage) null);
  }

  public void SwitchChannel()
  {
    if (this._chatManager == null)
      return;
    if (this.isKingdomChannel)
    {
      if (this._chatManager.allianceChannel == null)
        return;
      this.isKingdomChannel = false;
    }
    else
      this.isKingdomChannel = true;
  }

  public void OnNewFriendClick()
  {
    ChatDlg.Parameter parameter = new ChatDlg.Parameter();
    parameter.channelType = !this.isKingdomChannel ? ChatDlg.Parameter.ChannelType.alliance : ChatDlg.Parameter.ChannelType.kingdom;
    if (DBManager.inst.DB_Contacts.beInvited.Count > 0)
      parameter.isOpenFriend = true;
    UIManager.inst.OpenDlg("ChatDlg", (UI.Dialog.DialogParameter) parameter, true, true, true);
  }

  public void OnAreaClick()
  {
    UIManager.inst.OpenDlg("ChatDlg", (UI.Dialog.DialogParameter) new ChatDlg.Parameter()
    {
      channelType = (!this.isKingdomChannel ? ChatDlg.Parameter.ChannelType.alliance : ChatDlg.Parameter.ChannelType.kingdom)
    }, true, true, true);
  }

  public void Init()
  {
    this._chatManager = GameEngine.Instance.ChatManager;
    if (this._chatManager != null)
    {
      this.ClearMsg();
      if (this._chatManager.allianceChannel != null)
      {
        this.isKingdomChannel = false;
        this._curChannel = this._chatManager.allianceChannel;
        this._curChannel.LoadGroupMessage();
      }
      else if (this._chatManager.kingdomChannel != null)
      {
        this.isKingdomChannel = true;
        this._curChannel = this._chatManager.kingdomChannel;
      }
      this._curChannel.onNewMessage = new System.Action<ChatMessage>(this.SetMessage);
      this.SetMessage(this._curChannel.GetLastMessage(0));
      PlayerData.inst.onlineFriendsManager.onOnlineFriendStatuesChanged += new System.Action(this.OnOnlineFriendStatuesChanged);
      DBManager.inst.DB_Contacts.onDataChanged += new System.Action<long>(this.OnContactsDataChanged);
      DBManager.inst.DB_Contacts.onDataRemoved += new System.Action(this.OnContactsDataChanged);
      DBManager.inst.DB_User.onDataChanged += new System.Action<long>(this.OnUserAllianceDataChanged);
      this._chatManager.onRoomChanged = new System.Action(this.CheckNotice);
      DBManager.inst.DB_room.onDataChanged += new System.Action<long>(this.OnRoomChanged);
      DBManager.inst.DB_room.onMemberChanged += new System.Action<long, long>(this.OnRoomMemberChanged);
      DBManager.inst.DB_room.onMemberRemoved += new System.Action<long>(this.OnRoomChanged);
      this.panel.onlineFriendsCount.text = PlayerData.inst.onlineFriendsManager.onlineFriendCount.ToString();
      MessageHub.inst.GetPortByAction("chat_message").AddEvent(new System.Action<object>(this.OnNewMessageReceive));
      MessageHub.inst.GetPortByAction("Player:mod").AddEvent(new System.Action<object>(this.OnMod));
      this._chatManager.kingdomChannel.LoadGroupMessage();
    }
    GameEngine.Instance.OnGameModeStarted += new System.Action<bool>(this.HandleOnGameModeStarted);
    AllianceManager.Instance.onLeave += new System.Action(this.HandleOnLeave);
    this.panel.newFriendContainer.gameObject.SetActive(false);
    MessageHub.inst.GetPortByAction("Chat:getInitChatRooms").SendLoader((Hashtable) null, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      this.CheckNotice();
    }), false, false);
  }

  public void OnNewMessageReceive(object orgData)
  {
    this.CheckNotice();
  }

  private void OnMod(object orgSrc)
  {
    this._curChannel.FilterMessage();
    this.SetMessage((ChatMessage) null);
  }

  private void CheckNotice()
  {
    this.panel.unreadMsgCount.gameObject.SetActive(false);
    this.panel.newFriendContainer.gameObject.SetActive(false);
    if (!this.ShowNewFriendSymbol())
      return;
    this.panel.newFriendContainer.gameObject.SetActive(true);
  }

  private void OnRoomChanged(long roomId)
  {
    this.CheckNotice();
  }

  private void OnRoomMemberChanged(long roomId, long uid)
  {
    this.CheckNotice();
  }

  private void HandleOnLeave()
  {
    this.isKingdomChannel = true;
  }

  private void HandleOnGameModeStarted(bool obj)
  {
    if (obj)
      return;
    this.SwitchChannel();
  }

  public void OnOnlineFriendStatuesChanged()
  {
    this.panel.onlineFriendsCount.text = PlayerData.inst.onlineFriendsManager.onlineFriendCount.ToString();
    this.panel.newFriendContainer.gameObject.SetActive(this.ShowNewFriendSymbol());
  }

  public void OnContactsDataChanged(long uid)
  {
    this.panel.onlineFriendsCount.text = PlayerData.inst.onlineFriendsManager.onlineFriendCount.ToString();
    this.panel.newFriendContainer.gameObject.SetActive(this.ShowNewFriendSymbol());
  }

  public void OnContactsDataChanged()
  {
    this.panel.onlineFriendsCount.text = PlayerData.inst.onlineFriendsManager.onlineFriendCount.ToString();
    this.panel.newFriendContainer.gameObject.SetActive(this.ShowNewFriendSymbol());
  }

  public void OnUserAllianceDataChanged(long uid)
  {
    if (uid != PlayerData.inst.uid || PlayerData.inst.allianceId != 0L)
      return;
    this.isKingdomChannel = true;
  }

  private void OnChatRoomMemberChanged(long roomId, long uid)
  {
    this.CheckNotice();
  }

  public bool ShowNewFriendSymbol()
  {
    if (!ChatRoomManager.HasInviting() && !ChatRoomManager.HasApplyingMyRoom())
      return DBManager.inst.DB_Contacts.beInvited.Count > 0;
    return true;
  }

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    Debug.Log((object) "=========");
    this.OnInit(orgParam);
    this.Init();
  }

  [Serializable]
  protected class Panel
  {
    public UILabel onlineFriendsCount;
    public UILabel unreadMsgCount;
    public Transform newFriendContainer;
    public Transform kingdomChannelOn;
    public Transform kingdomChannelOff;
    public Transform kingdomIcon;
    public Transform allianceChannelOn;
    public Transform allianceChannleOff;
    public Transform allianceIcon;
    public UILabel name1;
    public UILabel name2;
    public UILabel msg1;
    public UILabel msg2;
  }
}
