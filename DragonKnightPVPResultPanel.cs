﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightPVPResultPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class DragonKnightPVPResultPanel : MonoBehaviour
{
  public UITexture m_UserIcon;
  public UILabel m_UserName;
  public UILabel m_Result;
  public UITexture m_DKIcon;
  public UITexture m_DKTitle;
  public UILabel m_DKName;
  public UILabel m_Level;
  public UILabel m_Damage;
  public UILabel m_Heal;

  public void SetData(UserData userData, AllianceData allianceData, DragonKnightData dkData, int damage, int heal, long score, int win)
  {
    CustomIconLoader.Instance.requestCustomIcon(this.m_UserIcon, userData.PortraitIconPath, userData.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.m_UserIcon, userData.LordTitle, 2);
    this.m_UserName.text = allianceData == null ? userData.userName : string.Format("[{0}]{1}", (object) allianceData.allianceAcronym, (object) userData.userName);
    switch (win)
    {
      case 0:
        this.m_Result.text = Utils.XLAT("dragon_knight_pvp_fail");
        this.m_Result.color = (Color) new Color32(byte.MaxValue, (byte) 38, (byte) 16, byte.MaxValue);
        break;
      case 1:
        this.m_Result.text = Utils.XLAT("dragon_knight_pvp_success");
        this.m_Result.color = (Color) new Color32((byte) 67, (byte) 224, (byte) 36, byte.MaxValue);
        break;
      case 2:
        this.m_Result.text = Utils.XLAT("dragon_knight_pvp_draw");
        this.m_Result.color = Color.grey;
        break;
    }
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_DKIcon, "Texture/DragonKnight/Portrait/" + (dkData.Gender != DragonKnightGender.Male ? "dk_portrait_female" : "dk_portrait_male"), (System.Action<bool>) null, true, true, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_DKTitle, ConfigManager.inst.DB_DKArenaTitleReward.GetTitleImage(score), (System.Action<bool>) null, true, true, string.Empty);
    this.m_DKName.text = Utils.XLAT(dkData.Name);
    this.m_Level.text = string.Format(Utils.XLAT("dragon_knight_level_num"), (object) dkData.Level.ToString());
    this.m_Damage.text = string.Format(Utils.XLAT("dragon_knight_pvp_damage_num"), (object) damage);
    this.m_Heal.text = string.Format(Utils.XLAT("dragon_knight_pvp_health_num"), (object) heal);
  }
}
