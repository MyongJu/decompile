﻿// Decompiled with JetBrains decompiler
// Type: LoadingAsset
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LoadingAsset
{
  public string name;
  public System.Action<UnityEngine.Object, bool> callback;
  public string bundlename;
  public System.Type type;
  private int refCount;
  public AssetBundleRequest abr;
  internal bool isLoading;

  public LoadingAsset(string name, System.Type type, string bundlename, System.Action<UnityEngine.Object, bool> callback)
  {
    this.name = name;
    this.type = type;
    this.bundlename = bundlename;
    this.callback = callback;
  }

  public int RefCount
  {
    get
    {
      return this.refCount;
    }
    set
    {
      this.refCount = value;
      if (this.refCount != 0)
        return;
      this.name = (string) null;
    }
  }
}
