﻿// Decompiled with JetBrains decompiler
// Type: GameVersionState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.IO;
using UI;
using UnityEngine;

public class GameVersionState : LoadBaseState
{
  private Dictionary<string, string> androidUpdateUrls = new Dictionary<string, string>();
  private Dictionary<string, string> iosUpdateUrls = new Dictionary<string, string>();
  private string lastVersion = string.Empty;

  public GameVersionState(int step)
    : base(step)
  {
  }

  public override string Key
  {
    get
    {
      return nameof (GameVersionState);
    }
  }

  protected override void Prepare()
  {
    this.Retry();
    SettingReader.Instance.Start();
  }

  protected override void Retry()
  {
    string url = "file://" + Path.Combine(Application.streamingAssetsPath, "version.json");
    if (Application.platform == RuntimePlatform.Android)
      url = Path.Combine(Application.streamingAssetsPath, "version.json");
    this.batch.Add("version", url, (Hashtable) null, false, 4, false);
    LoaderManager.inst.Add(this.batch, false, false);
  }

  protected override void OnAllFileFinish(LoaderBatch batch)
  {
    LoaderInfo resultInfo = batch.GetResultInfo("version");
    if (resultInfo != null)
      this.ParseVersion(resultInfo.ResData);
    base.OnAllFileFinish(batch);
  }

  private void ParseVersion(byte[] sources)
  {
    string json = Utils.ByteArray2String(sources);
    object obj = Utils.Json2Object(json, true);
    NativeManager.inst.VersionContent = json;
    Hashtable hashtable1 = obj as Hashtable;
    NativeManager.inst.BuildNumber = !hashtable1.ContainsKey((object) "buildnum") ? "0" : hashtable1[(object) "buildnum"].ToString();
    if (!hashtable1.ContainsKey((object) "code"))
      return;
    Hashtable hashtable2 = hashtable1[(object) "code"] as Hashtable;
    string str1 = hashtable2[(object) "major"].ToString().Trim();
    string str2 = hashtable2[(object) "minor"].ToString().Trim();
    NativeManager.inst.AppMajorVersion = str1;
    NativeManager.inst.AppMinorVersion = str2;
    UIManager.inst.splashScreen.SetVersion();
    this.Finish();
  }

  protected override void Finish()
  {
    if (this.IsDestroy)
      return;
    base.Finish();
    this.RefreshProgress();
    this.Preloader.OnGameVersionFinish();
  }

  protected override SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.ClientVersion;
    }
  }
}
