﻿// Decompiled with JetBrains decompiler
// Type: ConfigMerlinTrialsLayer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigMerlinTrialsLayer
{
  private List<MerlinTrialsLayerInfo> _merlinTrialsLayerInfoList = new List<MerlinTrialsLayerInfo>();
  private Dictionary<string, MerlinTrialsLayerInfo> _datas;
  private Dictionary<int, MerlinTrialsLayerInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<MerlinTrialsLayerInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, MerlinTrialsLayerInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._merlinTrialsLayerInfoList.Add(enumerator.Current);
    }
  }

  public void Clear()
  {
    if (this._datas != null)
      this._datas.Clear();
    if (this._dicByUniqueId != null)
      this._dicByUniqueId.Clear();
    if (this._merlinTrialsLayerInfoList == null)
      return;
    this._merlinTrialsLayerInfoList.Clear();
  }

  public List<MerlinTrialsLayerInfo> GetInfoList()
  {
    return this._merlinTrialsLayerInfoList;
  }

  public MerlinTrialsLayerInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (MerlinTrialsLayerInfo) null;
  }

  public MerlinTrialsLayerInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (MerlinTrialsLayerInfo) null;
  }

  public int GetMinLevel(int groupId)
  {
    int num = 9999;
    int index = 0;
    for (int count = this._merlinTrialsLayerInfoList.Count; index < count; ++index)
    {
      MerlinTrialsLayerInfo merlinTrialsLayerInfo = this._merlinTrialsLayerInfoList[index];
      if (merlinTrialsLayerInfo.trialId == groupId && merlinTrialsLayerInfo.level < num)
        num = merlinTrialsLayerInfo.level;
    }
    return num;
  }

  public int GetMaxLevel(int groupId)
  {
    int num = 0;
    int index = 0;
    for (int count = this._merlinTrialsLayerInfoList.Count; index < count; ++index)
    {
      MerlinTrialsLayerInfo merlinTrialsLayerInfo = this._merlinTrialsLayerInfoList[index];
      if (merlinTrialsLayerInfo.trialId == groupId && merlinTrialsLayerInfo.level > num)
        num = merlinTrialsLayerInfo.level;
    }
    return num;
  }

  public int GetLayerId(int groupId, int level)
  {
    int index = 0;
    for (int count = this._merlinTrialsLayerInfoList.Count; index < count; ++index)
    {
      MerlinTrialsLayerInfo merlinTrialsLayerInfo = this._merlinTrialsLayerInfoList[index];
      if (merlinTrialsLayerInfo.trialId == groupId && merlinTrialsLayerInfo.level == level)
        return merlinTrialsLayerInfo.internalId;
    }
    return 0;
  }
}
