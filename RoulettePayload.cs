﻿// Decompiled with JetBrains decompiler
// Type: RoulettePayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoulettePayload
{
  private Dictionary<int, Color> colorMap = new Dictionary<int, Color>();
  private List<int> middleRewards = new List<int>();
  private List<int> highRewards = new List<int>();
  private static RoulettePayload instance;
  private bool isOpen;
  private int groupId;
  private int endTime;
  private int paymentGotGold;
  private int claimedGold;
  private int luckyGold;
  private int drawReturnedLucky;
  private List<int> rewardIds;
  private bool dailyClaimable;
  private Dictionary<string, int> rewardsMap;
  private bool isRouletteClose;
  private bool isRoulettePaused;
  private int grade;

  public static RoulettePayload Instance
  {
    get
    {
      if (RoulettePayload.instance == null)
        RoulettePayload.instance = new RoulettePayload();
      return RoulettePayload.instance;
    }
  }

  public bool IsOpen
  {
    get
    {
      return this.isOpen;
    }
    set
    {
      this.isOpen = value;
    }
  }

  public int GroupId
  {
    get
    {
      return this.groupId;
    }
    set
    {
      this.groupId = value;
    }
  }

  public int ItemId
  {
    get
    {
      int num = 0;
      TavernWheelGroupInfo tavernWheelGroupInfo = ConfigManager.inst.DB_TavernWheelGroup.Get(this.groupId);
      if (tavernWheelGroupInfo != null)
        num = tavernWheelGroupInfo.itemId;
      return num;
    }
  }

  public int GoldSpent
  {
    get
    {
      int num = 0;
      TavernWheelGroupInfo tavernWheelGroupInfo = ConfigManager.inst.DB_TavernWheelGroup.Get(this.groupId);
      if (tavernWheelGroupInfo != null)
        num = tavernWheelGroupInfo.spend;
      return num;
    }
  }

  public int ConsumableGoldCount
  {
    get
    {
      ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(this.ItemId);
      if (consumableItemData != null)
        return consumableItemData.quantity;
      return 0;
    }
  }

  public TavernWheelGradeInfo GradeInfo
  {
    get
    {
      List<TavernWheelGradeInfo> gradesWithGroupId = ConfigManager.inst.DB_TavernWheelGrade.GetGradesWithGroupId(this.groupId);
      if (gradesWithGroupId != null && gradesWithGroupId.Count > 0)
        return gradesWithGroupId[0];
      return (TavernWheelGradeInfo) null;
    }
  }

  public int EndTime
  {
    get
    {
      return this.endTime;
    }
    set
    {
      this.endTime = value;
    }
  }

  public int PaymentGotGold
  {
    get
    {
      return this.paymentGotGold;
    }
    set
    {
      this.paymentGotGold = value;
    }
  }

  public int ClaimedGold
  {
    get
    {
      return this.claimedGold;
    }
    set
    {
      this.claimedGold = value;
    }
  }

  public int LuckyGold
  {
    get
    {
      return this.luckyGold;
    }
    set
    {
      this.luckyGold = value;
    }
  }

  public int DrawReturnedLuck
  {
    get
    {
      return this.drawReturnedLucky;
    }
  }

  public List<int> RewardIds
  {
    get
    {
      return this.rewardIds;
    }
    set
    {
      this.rewardIds = value;
    }
  }

  public bool DailyClaimable
  {
    get
    {
      return this.dailyClaimable;
    }
    set
    {
      this.dailyClaimable = value;
    }
  }

  public Dictionary<string, int> RewardsMap
  {
    get
    {
      return this.rewardsMap;
    }
  }

  public bool IsRouletteClose
  {
    get
    {
      return this.isRouletteClose;
    }
  }

  public bool IsRoulettePaused
  {
    get
    {
      return this.isRoulettePaused;
    }
  }

  public Dictionary<int, Color> ColorMap
  {
    get
    {
      if (this.colorMap.Count == 0)
        this.InitColorMap();
      return this.colorMap;
    }
  }

  public List<int> MiddleRewards
  {
    get
    {
      return this.middleRewards;
    }
  }

  public List<int> HighRewards
  {
    get
    {
      return this.highRewards;
    }
  }

  public int LastSelectedReward
  {
    get
    {
      return this.loc_rouletteLastStopPos;
    }
  }

  public int loc_rouletteLastStopPos
  {
    get
    {
      return PlayerPrefsEx.GetIntByUid("roulette_last_stop_position");
    }
  }

  public void Initialize()
  {
    MessageHub.inst.GetPortByAction("open_tavern_wheel").AddEvent(new System.Action<object>(this.OnTavernWheelOpen));
    MessageHub.inst.GetPortByAction("resume_tavern_wheel").AddEvent(new System.Action<object>(this.OnTavernWheelResume));
    MessageHub.inst.GetPortByAction("paused_tavern_wheel").AddEvent(new System.Action<object>(this.OnTavernWheelPause));
    MessageHub.inst.GetPortByAction("close_tavern_wheel").AddEvent(new System.Action<object>(this.OnTavernWheelClose));
  }

  public void Dispose()
  {
    MessageHub.inst.GetPortByAction("open_tavern_wheel").RemoveEvent(new System.Action<object>(this.OnTavernWheelOpen));
    MessageHub.inst.GetPortByAction("resume_tavern_wheel").RemoveEvent(new System.Action<object>(this.OnTavernWheelResume));
    MessageHub.inst.GetPortByAction("paused_tavern_wheel").RemoveEvent(new System.Action<object>(this.OnTavernWheelPause));
    MessageHub.inst.GetPortByAction("close_tavern_wheel").RemoveEvent(new System.Action<object>(this.OnTavernWheelClose));
    this.ClearCachedDatas();
  }

  private void InitColorMap()
  {
    this.colorMap.Add(0, Utils.GetColorFromHex(16777215U));
    this.colorMap.Add(1, Utils.GetColorFromHex(6550272U));
    this.colorMap.Add(2, Utils.GetColorFromHex(43775U));
    this.colorMap.Add(3, Utils.GetColorFromHex(12976383U));
    this.colorMap.Add(4, Utils.GetColorFromHex(16745216U));
    this.colorMap.Add(5, Utils.GetColorFromHex(16762121U));
  }

  private void OnTavernWheelOpen(object data)
  {
    this.isRouletteClose = false;
    this.isOpen = true;
    this.DecodeSwitchData(data);
  }

  private void OnTavernWheelClose(object data)
  {
    this.isRouletteClose = true;
    this.isOpen = false;
    this.ClearCachedDatas();
  }

  private void OnTavernWheelPause(object data)
  {
    this.isRoulettePaused = true;
    this.isOpen = false;
  }

  private void OnTavernWheelResume(object data)
  {
    this.isRoulettePaused = false;
    this.isOpen = true;
    this.DecodeSwitchData(data);
  }

  private void DecodeSwitchData(object data)
  {
    Hashtable hashtable = data as Hashtable;
    if (hashtable == null)
      return;
    if (hashtable.ContainsKey((object) "groupId") && hashtable[(object) "groupId"] != null)
    {
      int result = 0;
      int.TryParse(hashtable[(object) "groupId"].ToString(), out result);
      RoulettePayload.Instance.GroupId = result;
    }
    if (!hashtable.ContainsKey((object) "end") || hashtable[(object) "end"] == null)
      return;
    int result1 = 0;
    int.TryParse(hashtable[(object) "end"].ToString(), out result1);
    RoulettePayload.Instance.EndTime = result1;
  }

  public bool IsMiddleReward()
  {
    return false;
  }

  public bool IsHighReward()
  {
    return false;
  }

  private void ClearCachedDatas()
  {
    this.paymentGotGold = 0;
    this.claimedGold = 0;
    this.luckyGold = 0;
    this.drawReturnedLucky = 0;
    this.rewardIds = (List<int>) null;
    this.rewardsMap = (Dictionary<string, int>) null;
    this.dailyClaimable = false;
  }

  public void SpecifyMiddleAndHignRewards()
  {
    if (this.rewardsMap.Count <= 1)
      return;
    this.middleRewards.Clear();
    this.highRewards.Clear();
    Dictionary<string, int>.Enumerator enumerator = this.rewardsMap.GetEnumerator();
    while (enumerator.MoveNext())
    {
      TavernWheelMainInfo tavernWheelMainInfo = ConfigManager.inst.DB_TavernWheelMain.Get(int.Parse(enumerator.Current.Key));
      TavernWheelGradeInfo tavernWheelGradeInfo = ConfigManager.inst.DB_TavernWheelGrade.Get(tavernWheelMainInfo.gradeId);
      List<Reward.RewardsValuePair> rewards = tavernWheelMainInfo.rewards.GetRewards();
      if (tavernWheelGradeInfo.grade != 1)
      {
        if (tavernWheelGradeInfo.grade == 2)
          this.middleRewards.Add(rewards[0].internalID);
        else
          this.highRewards.Add(rewards[0].internalID);
      }
    }
  }

  private void DecodeUserData(object data)
  {
    this.ClearCachedDatas();
    if (data == null)
      return;
    Hashtable hashtable = data as Hashtable;
    if (hashtable.ContainsKey((object) "luck") && hashtable[(object) "luck"] != null)
      this.luckyGold = int.Parse(hashtable[(object) "luck"].ToString());
    if (hashtable.ContainsKey((object) "claimed_tokens") && hashtable[(object) "claimed_tokens"] != null)
      this.claimedGold = int.Parse(hashtable[(object) "claimed_tokens"].ToString());
    if (hashtable.ContainsKey((object) "payment") && hashtable[(object) "payment"] != null)
      this.paymentGotGold = int.Parse(hashtable[(object) "payment"].ToString());
    if (hashtable.ContainsKey((object) "rewards") && hashtable[(object) "rewards"] != null)
      this.rewardIds = JsonReader.Deserialize<List<int>>(Utils.Object2Json(hashtable[(object) "rewards"]));
    if (!hashtable.ContainsKey((object) "daily_claimable") || hashtable[(object) "daily_claimable"] == null)
      return;
    if (hashtable[(object) "daily_claimable"].ToString().Trim().ToLower() == "true")
      this.dailyClaimable = true;
    else
      this.dailyClaimable = false;
  }

  private void DecodeTokenData(object data)
  {
    if (data == null)
      return;
    Hashtable hashtable = data as Hashtable;
    if (!hashtable.ContainsKey((object) "claimed_tokens") || hashtable[(object) "claimed_tokens"] == null)
      return;
    this.claimedGold = int.Parse(hashtable[(object) "claimed_tokens"].ToString());
  }

  private void DecodeDrawResult(object data)
  {
    if (data == null)
      return;
    Hashtable hashtable1 = data as Hashtable;
    if (hashtable1.ContainsKey((object) "rewards") && hashtable1[(object) "rewards"] != null)
      this.rewardsMap = JsonReader.Deserialize<Dictionary<string, int>>(Utils.Object2Json(hashtable1[(object) "rewards"]));
    if (!hashtable1.ContainsKey((object) "changes") || hashtable1[(object) "changes"] == null)
      return;
    Hashtable hashtable2 = hashtable1[(object) "changes"] as Hashtable;
    if (!hashtable2.ContainsKey((object) "luck") || hashtable2[(object) "luck"] == null)
      return;
    this.drawReturnedLucky = int.Parse(hashtable2[(object) "luck"].ToString());
  }

  public void ShowCollectEffect(int itemId, int count)
  {
    RewardsCollectionAnimator.Instance.Clear();
    RewardsCollectionAnimator.Instance.items2.Add(new Item2RewardInfo.Data()
    {
      count = (float) count,
      itemid = itemId
    });
    RewardsCollectionAnimator.Instance.CollectItems2(false);
    AudioManager.Instance.StopAndPlaySound("sfx_rural_resource_boost");
  }

  public void ShowCollectEffect(int itemId, int count, int grade)
  {
    this.grade = grade;
    this.ShowCollectEffect(itemId, count);
    Utils.ExecuteInSecs(0.5f, new System.Action(this.AttachCollectEffect));
  }

  private void AttachCollectEffect()
  {
    string empty = string.Empty;
    if (this.grade == 1)
      return;
    string fullname = this.grade != 2 ? "Prefab/VFX/fx_choujiang_01" : "Prefab/VFX/fx_choujiang_02";
    Transform transform = RewardsCollectionAnimator.Instance.GetComponentInChildren<Item2RewardInfo>().transform;
    GameObject go = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad(fullname, (System.Type) null) as GameObject);
    go.transform.parent = transform;
    go.transform.localPosition = Vector3.zero;
    go.transform.localScale = Vector3.one;
    Utils.SetLayer(go, transform.gameObject.layer);
  }

  public void SendGetUserDataRequest(System.Action<bool, object> callback, bool blockScreen = true)
  {
    MessageHub.inst.GetPortByAction("tavernWheel:getUserData").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.DecodeUserData(data);
      if (!ret || callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }

  public void SendGetTokenRequest(System.Action<bool, object> callback, int type, bool blockScreen = true)
  {
    RequestManager.inst.SendRequest("tavernWheel:claimTokenItem", new Hashtable()
    {
      {
        (object) nameof (type),
        (object) type
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret && type == 1)
        this.DecodeTokenData(data);
      if (!ret || callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }

  public void SendDrawRewardsRequest(System.Action<bool, object, int> callback, int times, bool blockScreen = true)
  {
    Hashtable postData = new Hashtable();
    postData[(object) nameof (times)] = (object) times;
    MessageHub.inst.GetPortByAction("tavernWheel:draw").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.DecodeDrawResult(data);
      if (!ret || callback == null)
        return;
      callback(ret, data, times);
    }), blockScreen);
  }
}
