﻿// Decompiled with JetBrains decompiler
// Type: ConfigLegendSkill
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigLegendSkill
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, LegendSkillInfo> datas;
  private Dictionary<int, LegendSkillInfo> dicByUniqueId;

  public object ParseDict(object rs)
  {
    Hashtable hashtable = rs as Hashtable;
    Dictionary<int, float> dictionary = new Dictionary<int, float>();
    if (hashtable == null)
      return (object) dictionary;
    IEnumerator enumerator = hashtable.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      string s = enumerator.Current.ToString();
      int result1 = -1;
      float result2;
      if (int.TryParse(s, out result1) && float.TryParse(hashtable[enumerator.Current].ToString(), out result2))
        dictionary.Add(result1, result2);
    }
    return (object) dictionary;
  }

  public void BuildDB(object res)
  {
    this.parse.AddParseFunc("ParseBenfits", new ParseFunc(this.ParseDict));
    this.parse.Parse<LegendSkillInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public LegendSkillInfo GetNextLevel(string id, string type)
  {
    LegendSkillInfo data = this.datas[id];
    int num = id.LastIndexOf('_');
    string key = id.Substring(0, num + 1) + (object) (data.Level + 1);
    if (this.datas.ContainsKey(key))
      return this.datas[key];
    return (LegendSkillInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, LegendSkillInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, LegendSkillInfo>) null;
  }

  public LegendSkillInfo GetSkillInfo(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (LegendSkillInfo) null;
  }

  public LegendSkillInfo GetSkillInfo(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (LegendSkillInfo) null;
  }
}
