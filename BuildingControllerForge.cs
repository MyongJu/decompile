﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerForge
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class BuildingControllerForge : BuildingControllerNew
{
  private const string WISH_WELL_PATH = "Prefab/City/CollectionForgeUI";
  private GameObject m_ForgeButton;
  private GameObject m_Template;

  public override void AddActionButton(ref List<CityCircleBtnPara> ftl)
  {
    base.AddActionButton(ref ftl);
    ftl.Add(this.CCBPair["forge"]);
    ftl.Add(this.CCBPair["enhance"]);
    ftl.Add(this.CCBPair["armory"]);
    if (EquipmentManager.Instance.GetSynAbleScrollChip(BagType.Hero) == null)
      return;
    this.CCBPair["armory"].needEffect = true;
  }

  public override void InitBuilding()
  {
    base.InitBuilding();
    this.m_Template = AssetManager.Instance.HandyLoad("Prefab/City/CollectionForgeUI", (System.Type) null) as GameObject;
    this.OnItemDataChanged(0);
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemDataChanged);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnItemDataChanged);
  }

  public override void Dispose()
  {
    base.Dispose();
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataChanged);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnItemDataChanged);
  }

  private void OnItemDataChanged(int itemID)
  {
    if (EquipmentManager.Instance.GetSynAbleScrollChip(BagType.Hero) != null && !EquipmentManager.Instance.HasUnCollectedEquipment(BagType.Hero) && (UnityEngine.Object) this.m_Template != (UnityEngine.Object) null)
    {
      if ((bool) ((UnityEngine.Object) this.m_ForgeButton))
        return;
      this.m_ForgeButton = UnityEngine.Object.Instantiate<GameObject>(this.m_Template);
      this.m_ForgeButton.SetActive(true);
      this.m_ForgeButton.transform.parent = this.transform;
      this.m_ForgeButton.transform.localPosition = new Vector3(0.0f, 50f, 0.0f);
      this.m_ForgeButton.transform.localScale = new Vector3(1.6f, 1.6f, 1.6f);
    }
    else
    {
      if (!(bool) ((UnityEngine.Object) this.m_ForgeButton))
        return;
      this.m_ForgeButton.SetActive(false);
      this.m_ForgeButton.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_ForgeButton);
      this.m_ForgeButton = (GameObject) null;
    }
  }
}
