﻿// Decompiled with JetBrains decompiler
// Type: AllianceChangeRankItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public class AllianceChangeRankItemRenderer : MonoBehaviour
{
  public UILabel m_Rank;
  public UILabel m_OldName;
  public UIInput m_NewName;
  public UISprite m_NameStatus;
  private bool m_IsOkay;
  private AllianceData m_AllianceData;
  private int m_Title;

  public void SetData(AllianceData allianceData, int title)
  {
    this.m_AllianceData = allianceData;
    this.m_IsOkay = true;
    this.m_Title = title;
    this.UpdateUI();
  }

  public void OnNameInputChanged()
  {
    this.m_NameStatus.spriteName = string.Empty;
    this.m_IsOkay = false;
    string str = this.m_NewName.value.Trim();
    byte[] bytes = Encoding.UTF8.GetBytes(str);
    if (bytes.Length == 0)
      this.m_IsOkay = true;
    else if (bytes.Length < 3 || bytes.Length > 10)
    {
      this.m_NameStatus.spriteName = "red_cross";
    }
    else
    {
      this.m_IsOkay = Regex.IsMatch(str, "^[\\u4E00-\\u9FFFA-Za-z0-9 ]+$");
      if (this.m_IsOkay)
        this.m_NameStatus.spriteName = "green_tick";
      else
        this.m_NameStatus.spriteName = "red_cross";
    }
  }

  public bool isOkay
  {
    get
    {
      return this.m_IsOkay;
    }
  }

  public string Name
  {
    get
    {
      if (string.IsNullOrEmpty(this.m_NewName.value))
        return string.Empty;
      return this.m_NewName.value;
    }
  }

  private void UpdateUI()
  {
    this.m_Rank.text = AllianceTitle.TitleToRank(this.m_Title);
    this.m_OldName.text = this.m_AllianceData.GetTitleName(this.m_Title);
    this.m_NewName.value = string.Empty;
    this.m_NameStatus.spriteName = string.Empty;
  }
}
