﻿// Decompiled with JetBrains decompiler
// Type: IMarchBaseState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public interface IMarchBaseState : IState
{
  MarchViewControler Controler { get; set; }

  MarchViewControler.Data StateData { get; set; }

  bool IsInitState { get; set; }

  bool IsReady { get; set; }
}
