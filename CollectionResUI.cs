﻿// Decompiled with JetBrains decompiler
// Type: CollectionResUI
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class CollectionResUI : CollectionUI
{
  public override void Open(Transform parent)
  {
    base.Open(parent);
  }

  public override void Close()
  {
    UIManager.inst.cityCamera.LockInput = true;
    UIManager.inst.GetComponent<FingerHoverDetector>().OnFingerHover -= new FingerEventDetector<FingerHoverEvent>.FingerEventHandler(this.OnHoverd);
    base.Close();
  }

  public override void InitCollectionUI(CityCircleBtnPara para, CollectionUI.CollectionType ct)
  {
    base.InitCollectionUI(para, ct);
    this.et.onDragStart.Clear();
    this.et.onDragStart.Add(para.ed);
    this.et.onDragStart.Add(new EventDelegate(new EventDelegate.Callback(((CollectionUI) this).Close)));
    UIManager.inst.GetComponent<FingerHoverDetector>().OnFingerHover += new FingerEventDetector<FingerHoverEvent>.FingerEventHandler(this.OnHoverd);
  }

  private void OnHoverd(FingerHoverEvent eventData)
  {
    if (eventData.Phase != FingerHoverPhase.Enter || !((Object) eventData.Selection == (Object) this.gameObject))
      return;
    this.mPara.ed.Execute();
    this.Close();
  }

  protected override void Awake()
  {
    base.Awake();
  }

  private void OnDestroy()
  {
    if (!GameEngine.IsAvailable || GameEngine.IsShuttingDown)
      return;
    this.Close();
  }
}
