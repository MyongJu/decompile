﻿// Decompiled with JetBrains decompiler
// Type: LanguageSDK
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using System;
using System.Collections.Generic;
using UnityEngine;

public class LanguageSDK
{
  private Dictionary<string, FunplusLanguage> _languageMap = new Dictionary<string, FunplusLanguage>();
  private Dictionary<string, FunplusLanguage> _languageMap2 = new Dictionary<string, FunplusLanguage>();
  private static LanguageSDK _instance;

  public LanguageSDK()
  {
    this.Init();
  }

  public static LanguageSDK Instance
  {
    get
    {
      if (LanguageSDK._instance == null)
        LanguageSDK._instance = new LanguageSDK();
      return LanguageSDK._instance;
    }
  }

  private FunplusLanguage GetLanguage(string local)
  {
    if (string.IsNullOrEmpty(local))
      return FunplusLanguage.English;
    if (this._languageMap.ContainsKey(local))
      return this._languageMap[local];
    if (this._languageMap2.ContainsKey(local))
      return this._languageMap2[local];
    return FunplusLanguage.English;
  }

  public void SetBiLanguage(bool isGameLanguage = false)
  {
    if (Application.isEditor)
      return;
    string language = ((Enum) Application.systemLanguage).ToString();
    if (isGameLanguage)
      language = PlayerData.inst.userData.language;
    FunplusSdk.Instance.setGameLanguage(this.GetLanguage(language));
  }

  private void Init()
  {
    this._languageMap.Add(((Enum) SystemLanguage.Chinese).ToString(), FunplusLanguage.SimplifiedChinese);
    this._languageMap.Add(((Enum) SystemLanguage.ChineseSimplified).ToString(), FunplusLanguage.SimplifiedChinese);
    this._languageMap.Add(((Enum) SystemLanguage.ChineseTraditional).ToString(), FunplusLanguage.TraditionalChinese);
    this._languageMap.Add(((Enum) SystemLanguage.Arabic).ToString(), FunplusLanguage.Arabic);
    this._languageMap.Add(((Enum) SystemLanguage.Dutch).ToString(), FunplusLanguage.Dutch);
    this._languageMap.Add(((Enum) SystemLanguage.English).ToString(), FunplusLanguage.English);
    this._languageMap.Add(((Enum) SystemLanguage.French).ToString(), FunplusLanguage.French);
    this._languageMap.Add(((Enum) SystemLanguage.German).ToString(), FunplusLanguage.German);
    this._languageMap.Add(((Enum) SystemLanguage.Indonesian).ToString(), FunplusLanguage.Indonesian);
    this._languageMap.Add(((Enum) SystemLanguage.Italian).ToString(), FunplusLanguage.Italian);
    this._languageMap.Add(((Enum) SystemLanguage.Japanese).ToString(), FunplusLanguage.Japanese);
    this._languageMap.Add(((Enum) SystemLanguage.Korean).ToString(), FunplusLanguage.Korean);
    this._languageMap.Add(((Enum) SystemLanguage.Norwegian).ToString(), FunplusLanguage.Norwegian);
    this._languageMap.Add(((Enum) SystemLanguage.Polish).ToString(), FunplusLanguage.Polish);
    this._languageMap.Add(((Enum) SystemLanguage.Portuguese).ToString(), FunplusLanguage.Portuguese);
    this._languageMap.Add(((Enum) SystemLanguage.Russian).ToString(), FunplusLanguage.Russian);
    this._languageMap.Add(((Enum) SystemLanguage.Spanish).ToString(), FunplusLanguage.Spanish);
    this._languageMap.Add(((Enum) SystemLanguage.Swedish).ToString(), FunplusLanguage.Swedish);
    this._languageMap.Add(((Enum) SystemLanguage.Thai).ToString(), FunplusLanguage.Thai);
    this._languageMap.Add(((Enum) SystemLanguage.Turkish).ToString(), FunplusLanguage.Turkish);
    this._languageMap2.Add("zh-CN", FunplusLanguage.SimplifiedChinese);
    this._languageMap2.Add("zh-TW", FunplusLanguage.TraditionalChinese);
    this._languageMap2.Add("nl", FunplusLanguage.Dutch);
    this._languageMap2.Add("en", FunplusLanguage.English);
    this._languageMap2.Add("fr", FunplusLanguage.French);
    this._languageMap2.Add("de", FunplusLanguage.German);
    this._languageMap2.Add("id", FunplusLanguage.Indonesian);
    this._languageMap2.Add("pt", FunplusLanguage.Portuguese);
    this._languageMap2.Add("it", FunplusLanguage.Italian);
    this._languageMap2.Add("ja", FunplusLanguage.Japanese);
    this._languageMap2.Add("ko", FunplusLanguage.Korean);
    this._languageMap2.Add("no", FunplusLanguage.Norwegian);
    this._languageMap2.Add("pl", FunplusLanguage.Polish);
    this._languageMap2.Add("ru", FunplusLanguage.Russian);
    this._languageMap2.Add("es", FunplusLanguage.Spanish);
    this._languageMap2.Add("sv", FunplusLanguage.Swedish);
    this._languageMap2.Add("th", FunplusLanguage.Thai);
    this._languageMap2.Add("tr", FunplusLanguage.Turkish);
    this._languageMap2.Add("ar", FunplusLanguage.Arabic);
  }
}
