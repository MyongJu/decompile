﻿// Decompiled with JetBrains decompiler
// Type: ConfigAudio
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAudio
{
  private Dictionary<string, AudioData> _AudioPair = new Dictionary<string, AudioData>();
  public bool Builded;

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exsit", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "path");
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          AudioData data = new AudioData();
          ArrayList arr = hashtable[key] as ArrayList;
          if (arr != null)
          {
            data.id = ConfigManager.GetString(arr, index1);
            data.fullname = ConfigManager.GetString(arr, index2);
            this.PushData(data.id, data);
          }
        }
        this.Builded = true;
      }
    }
  }

  public void PushData(string id, AudioData data)
  {
    if (this._AudioPair.ContainsKey(id))
      return;
    this._AudioPair.Add(id, data);
  }

  public void Clear()
  {
    if (this._AudioPair != null)
      this._AudioPair.Clear();
    this.Builded = false;
  }

  public AudioData GetData(string id)
  {
    AudioData audioData = (AudioData) null;
    this._AudioPair.TryGetValue(id, out audioData);
    return audioData;
  }
}
