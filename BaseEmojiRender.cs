﻿// Decompiled with JetBrains decompiler
// Type: BaseEmojiRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BaseEmojiRender : EmojiRender
{
  public UITexture emoji;

  public override void Show(Emoji element, Vector3 position, Vector2 size)
  {
    this.gameObject.SetActive(true);
    if (position != new Vector3())
      this.transform.localPosition = position;
    if (size != new Vector2())
    {
      this.emoji.width = (int) size.x;
      this.emoji.height = (int) size.y;
    }
    BuilderFactory.Instance.HandyBuild((UIWidget) this.emoji, element.param, (System.Action<bool>) null, true, false, string.Empty);
    this.emoji.depth = this.transform.parent.GetComponent<UIWidget>().depth + 1;
  }

  public override void Hide()
  {
    this.gameObject.SetActive(false);
  }
}
