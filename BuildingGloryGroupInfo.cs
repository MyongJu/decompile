﻿// Decompiled with JetBrains decompiler
// Type: BuildingGloryGroupInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class BuildingGloryGroupInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(CustomParse = true, CustomType = typeof (Requirements), Name = "Requirements")]
  public Requirements require;

  public int ReqBuildingId
  {
    get
    {
      int result = 0;
      if (this.require != null && this.require.requires != null)
      {
        using (Dictionary<string, int>.KeyCollection.Enumerator enumerator = this.require.requires.Keys.GetEnumerator())
        {
          if (enumerator.MoveNext())
            int.TryParse(enumerator.Current, out result);
        }
      }
      return result;
    }
  }

  public int ReqBuildingMinLevel
  {
    get
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(this.ReqBuildingId);
      if (data != null)
        return data.Building_Lvl;
      return 1;
    }
  }

  public string BuildingType
  {
    get
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(this.ReqBuildingId);
      if (data != null)
        return data.Type;
      return string.Empty;
    }
  }

  public int BuildingGloryMaxLevel
  {
    get
    {
      int num = 0;
      List<BuildingGloryMainInfo> gloryMainInfoList = ConfigManager.inst.DB_BuildingGloryMain.GetBuildingGloryMainInfoList();
      if (gloryMainInfoList != null)
      {
        List<BuildingGloryMainInfo> all = gloryMainInfoList.FindAll((Predicate<BuildingGloryMainInfo>) (x => x.groupId == this.internalId));
        if (all != null)
        {
          for (int index = 0; index < all.Count; ++index)
          {
            if (num < all[index].level)
              num = all[index].level;
          }
        }
      }
      return num;
    }
  }
}
