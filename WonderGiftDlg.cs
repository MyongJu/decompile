﻿// Decompiled with JetBrains decompiler
// Type: WonderGiftDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class WonderGiftDlg : UI.Dialog
{
  protected List<WonderGiftItem> m_allWonderGiftItem = new List<WonderGiftItem>();
  [SerializeField]
  protected WonderGiftItem m_template;
  [SerializeField]
  protected UIGrid m_container;
  [SerializeField]
  protected UIScrollView m_scrollView;
  protected Hashtable m_sendCounter;
  protected long m_receiverId;

  protected int getSendGiftCount(int giftInternalId)
  {
    if (this.m_sendCounter == null)
      return 0;
    int outData = 0;
    DatabaseTools.UpdateData(this.m_sendCounter, giftInternalId.ToString(), ref outData);
    return outData;
  }

  protected int GetPackageMultiFactor()
  {
    if (this.m_sendCounter == null)
      return 1;
    int outData = 1;
    DatabaseTools.UpdateData(this.m_sendCounter, "multiplier", ref outData);
    return outData;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    WonderGiftDlg.Parameter parameter = orgParam as WonderGiftDlg.Parameter;
    this.m_receiverId = parameter != null ? parameter.receiverId : 0L;
    this.m_template.gameObject.SetActive(false);
    Hashtable postData = new Hashtable()
    {
      {
        (object) "world_id",
        (object) PlayerData.inst.userData.world_id
      }
    };
    RequestManager.inst.SendRequest("wonder:getKingGiftSentAmount", postData, new System.Action<bool, object>(this.onRequestCallBack), true);
  }

  protected void onRequestCallBack(bool result, object data)
  {
    this.m_sendCounter = data as Hashtable;
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  protected void UpdateUI()
  {
    this.UpdateGiftItemList();
  }

  protected void UpdateGiftItemList()
  {
    this.DestroyAllHistoryItem();
    using (List<WonderPackageInfo>.Enumerator enumerator = ConfigManager.inst.DB_WonderPackage.GetAllWonderPackageInfo().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        WonderPackageInfo current = enumerator.Current;
        this.AddGiftItem().setData(current, this.getSendGiftCount(current.internalId), this.m_receiverId, this.GetPackageMultiFactor());
      }
    }
    this.m_container.repositionNow = true;
    this.m_container.Reposition();
    this.m_scrollView.movement = UIScrollView.Movement.Unrestricted;
    this.m_scrollView.ResetPosition();
    this.m_scrollView.currentMomentum = Vector3.zero;
    this.m_scrollView.movement = UIScrollView.Movement.Horizontal;
  }

  protected void DestroyAllHistoryItem()
  {
    using (List<WonderGiftItem>.Enumerator enumerator = this.m_allWonderGiftItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        WonderGiftItem current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_allWonderGiftItem.Clear();
  }

  protected WonderGiftItem AddGiftItem()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_template.gameObject);
    gameObject.transform.SetParent(this.m_container.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    WonderGiftItem component = gameObject.GetComponent<WonderGiftItem>();
    this.m_allWonderGiftItem.Add(component);
    return component;
  }

  public void OnButtonGiftLogClicked()
  {
    UIManager.inst.OpenPopup("Wonder/WonderGiftHistoryPopup", (Popup.PopupParameter) null);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public long receiverId;
  }
}
