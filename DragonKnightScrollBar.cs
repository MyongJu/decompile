﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightScrollBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DragonKnightScrollBar : MonoBehaviour
{
  [SerializeField]
  private int _maxWidth = 100;
  [SerializeField]
  private float _duration = 0.1f;
  [SerializeField]
  private float _delay = 0.5f;
  private float _progress = 1f;
  public System.Action OnTweenFinished;
  [SerializeField]
  private UISpriteMesh _spriteMesh;
  [SerializeField]
  private int _minWidth;
  [SerializeField]
  private int _threshold;
  private bool _dirty;
  private bool _tween;
  private float _start;
  private int _sourceWidth;
  private int _targetWidth;

  private void Start()
  {
    this._dirty = true;
  }

  private void Update()
  {
    if (!(bool) ((UnityEngine.Object) this._spriteMesh))
      return;
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    if (this._dirty)
    {
      this._dirty = false;
      this._sourceWidth = this._spriteMesh.width;
      this._targetWidth = Mathf.Max((int) ((double) this._progress * (double) this._maxWidth), this._minWidth);
      this._tween = true;
      this._start = realtimeSinceStartup;
    }
    if (!this._tween)
      return;
    float num1 = (realtimeSinceStartup - this._start) * Time.timeScale;
    if ((double) num1 <= (double) this._delay)
      return;
    float num2 = (double) this._duration > 0.0 ? (num1 - this._delay) / this._duration : 1f;
    if ((double) num2 >= 1.0)
    {
      this._tween = false;
      this.SetWidth(this._targetWidth);
      if (this.OnTweenFinished == null)
        return;
      this.OnTweenFinished();
    }
    else
      this.SetWidth((int) ((float) this._sourceWidth + num2 * (float) (this._targetWidth - this._sourceWidth)));
  }

  private void SetWidth(int width)
  {
    if (!(bool) ((UnityEngine.Object) this._spriteMesh))
      return;
    this._spriteMesh.width = width;
    if (this._spriteMesh.width <= this._threshold)
      this._spriteMesh.fillType = UISpriteMesh.FillType.Simple;
    else
      this._spriteMesh.fillType = UISpriteMesh.FillType.Sliced;
  }

  public void SetProgress(float progress)
  {
    progress = Mathf.Clamp(progress, 0.0f, 1f);
    if ((double) this._progress == (double) progress)
      return;
    this._progress = progress;
    this._dirty = true;
  }

  public float GetProgress()
  {
    return this._progress;
  }

  public void Reset()
  {
    this.SetWidth(this._maxWidth);
  }
}
