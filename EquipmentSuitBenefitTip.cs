﻿// Decompiled with JetBrains decompiler
// Type: EquipmentSuitBenefitTip
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class EquipmentSuitBenefitTip : MonoBehaviour
{
  [SerializeField]
  private float _fadeInTime = 0.5f;
  [SerializeField]
  private float _fadeOutTime = 0.5f;
  [SerializeField]
  private float _showTime = 1f;
  private List<EquipmentSuitBenefitTipSubItem> _allSubItem = new List<EquipmentSuitBenefitTipSubItem>();
  [SerializeField]
  private UIPanel _panelRoot;
  [SerializeField]
  private EquipmentSuitBenefitTipSubItem _subItemTemplate;
  [SerializeField]
  private UITable _subItemContainer;

  private void Start()
  {
    this._subItemTemplate.gameObject.SetActive(false);
  }

  public void Play(List<Benefits.BenefitValuePair> allDelProperty, List<Benefits.BenefitValuePair> allAddProperty, ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance, ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance)
  {
    this.gameObject.SetActive(true);
    this.DestroyAllSubItem();
    if (allAddProperty != null)
    {
      using (List<Benefits.BenefitValuePair>.Enumerator enumerator = allAddProperty.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Benefits.BenefitValuePair current = enumerator.Current;
          string displayString = current.propertyDefinition.ConvertToDisplayString((double) current.value, false, true);
          this.CreateSubItem().SetData(string.Format("[00ff00]{0} {1}[-]", (object) ScriptLocalization.GetWithPara("toast_suit_group_benefit_added", new Dictionary<string, string>()
          {
            {
              "0",
              current.propertyDefinition.Name
            }
          }, true), (object) displayString));
        }
      }
    }
    if (addEquipmentSuitEnhance != null)
      this.CreateSubItem().SetData(string.Format("[00ff00]{0} +{1}%[-]", (object) ScriptLocalization.GetWithPara("toast_suit_group_benefit_added", new Dictionary<string, string>()
      {
        {
          "0",
          ScriptLocalization.Get("forge_enhance_resonance_benefit", true)
        }
      }, true), (object) (addEquipmentSuitEnhance.enhanceValue * 100f).ToString()));
    if (allDelProperty != null)
    {
      using (List<Benefits.BenefitValuePair>.Enumerator enumerator = allDelProperty.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Benefits.BenefitValuePair current = enumerator.Current;
          string displayString = current.propertyDefinition.ConvertToDisplayString((double) current.value, false, true);
          this.CreateSubItem().SetData(string.Format("[ff0000]{0} {1}[-]", (object) ScriptLocalization.GetWithPara("toast_suit_group_benefit_removed", new Dictionary<string, string>()
          {
            {
              "0",
              current.propertyDefinition.Name
            }
          }, true), (object) displayString));
        }
      }
    }
    if (delEquipmentSuitEnhance != null)
      this.CreateSubItem().SetData(string.Format("[ff0000]{0} +{1}%[-]", (object) ScriptLocalization.GetWithPara("toast_suit_group_benefit_removed", new Dictionary<string, string>()
      {
        {
          "0",
          ScriptLocalization.Get("forge_enhance_resonance_benefit", true)
        }
      }, true), (object) (delEquipmentSuitEnhance.enhanceValue * 100f).ToString()));
    this._subItemContainer.repositionNow = true;
    this._subItemContainer.Reposition();
    this.StopAllCoroutines();
    this.StartCoroutine(this.AnimationCoroutine());
  }

  [DebuggerHidden]
  private IEnumerator AnimationCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EquipmentSuitBenefitTip.\u003CAnimationCoroutine\u003Ec__Iterator6E()
    {
      \u003C\u003Ef__this = this
    };
  }

  public EquipmentSuitBenefitTipSubItem CreateSubItem()
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this._subItemTemplate.gameObject);
    gameObject.transform.SetParent(this._subItemContainer.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    EquipmentSuitBenefitTipSubItem component = gameObject.GetComponent<EquipmentSuitBenefitTipSubItem>();
    this._allSubItem.Add(component);
    return component;
  }

  public void DestroyAllSubItem()
  {
    using (List<EquipmentSuitBenefitTipSubItem>.Enumerator enumerator = this._allSubItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EquipmentSuitBenefitTipSubItem current = enumerator.Current;
        if ((bool) ((Object) current))
        {
          current.transform.SetParent((Transform) null);
          Object.DestroyObject((Object) current.gameObject);
        }
      }
    }
    this._allSubItem.Clear();
  }
}
