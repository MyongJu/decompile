﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragonSkillEnhance
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDragonSkillEnhance
{
  private Dictionary<string, DragonSkillEnhanceInfo> _dataByKey;
  private Dictionary<int, DragonSkillEnhanceInfo> _dataByInternalid;

  public void BuildDB(object res)
  {
    ConfigParse configParse = new ConfigParse();
    Hashtable sources = res as Hashtable;
    if (sources == null)
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    else
      configParse.Parse<DragonSkillEnhanceInfo, string>(sources, "ID", out this._dataByKey, out this._dataByInternalid);
  }

  public DragonSkillEnhanceInfo GetByStarLevel(int starLevel)
  {
    using (Dictionary<string, DragonSkillEnhanceInfo>.Enumerator enumerator = this._dataByKey.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, DragonSkillEnhanceInfo> current = enumerator.Current;
        if (current.Value.TargetEnhanceLevel == starLevel)
          return current.Value;
      }
    }
    return (DragonSkillEnhanceInfo) null;
  }

  public void Clear()
  {
    if (this._dataByKey != null)
      this._dataByKey.Clear();
    if (this._dataByInternalid == null)
      return;
    this._dataByInternalid.Clear();
  }
}
