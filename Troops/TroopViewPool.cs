﻿// Decompiled with JetBrains decompiler
// Type: Troops.TroopViewPool
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Troops
{
  public class TroopViewPool
  {
    private List<MarchViewControler> _troopViewList = new List<MarchViewControler>(10);
    private Queue<MarchViewControler> _troopViewQueue = new Queue<MarchViewControler>(10);
    private GameObjectPool _pool = new GameObjectPool();
    public const int MAX_TROOP_VIEW_COUNT = 10;
    public GameObject TroopsRoot;
    private GameObject _troopViewPrefab;
    private GameObject _troopsPoolRoot;

    public void CreateTroopViews(Transform root)
    {
      if (!((UnityEngine.Object) this.TroopsRoot == (UnityEngine.Object) null))
        return;
      this.TroopsRoot = new GameObject("TroopsRoot");
      this.TroopsRoot.transform.parent = root;
      this._troopsPoolRoot = new GameObject("TroopsPoolRoot");
      this._troopsPoolRoot.transform.parent = this.TroopsRoot.transform;
      this._troopViewPrefab = AssetManager.Instance.HandyLoad("Prefab/TroopModels/TroopView", (System.Type) null) as GameObject;
      this._pool.Initialize(this._troopViewPrefab, this._troopsPoolRoot);
    }

    private void CreateViews()
    {
      for (int index = 0; index < 10; ++index)
      {
        GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/TroopModels/TroopView", (System.Type) null)) as GameObject;
        gameObject.name = string.Format("TroopView{0}", (object) this._troopViewList.Count);
        gameObject.transform.parent = this.TroopsRoot.transform;
        MarchViewControler component = gameObject.GetComponent<MarchViewControler>();
        component.Init();
        this._troopViewList.Add(component);
        this._troopViewQueue.Enqueue(component);
        gameObject.gameObject.SetActive(false);
      }
    }

    public MarchViewControler GetTroopView(long uid = 0)
    {
      MarchViewControler component = this._pool.AddChild(this.TroopsRoot).GetComponent<MarchViewControler>();
      component.Init();
      component.gameObject.SetActive(true);
      return component;
    }

    public void Clear()
    {
      this._pool.Clear();
    }

    public void ReleaseTroopView(MarchViewControler discardView)
    {
      this._pool.Release(discardView.gameObject);
    }

    public void Dispose()
    {
      this._troopViewQueue.Clear();
      this._troopViewList.Clear();
    }
  }
}
