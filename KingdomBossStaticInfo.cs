﻿// Decompiled with JetBrains decompiler
// Type: KingdomBossStaticInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class KingdomBossStaticInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(CustomParse = true, CustomType = typeof (UnitGroup), Name = "Units")]
  public UnitGroup Units;
  [Config(Name = "boss_level")]
  public int BossLevel;
  [Config(Name = "player_power")]
  public long PlayerPower;
  [Config(Name = "hp")]
  public long HP;
  [Config(Name = "name")]
  public string Name;
  [Config(Name = "type")]
  public string Type;
  [Config(Name = "boss_talk_1")]
  public string BossTalk1;
  [Config(Name = "boss_talk_2")]
  public string BossTalk2;
  [Config(Name = "boss_talk_3")]
  public string BossTalk3;
  [Config(Name = "boss_talk_4")]
  public string BossTalk4;
  [Config(Name = "boss_talk_5")]
  public string BossTalk5;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "BaseRewards")]
  public Reward BaseRewards;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "KillRewards")]
  public Reward KillRewards;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "AdvanceRewards")]
  public Reward AdvanceRewards;

  public string LocalName
  {
    get
    {
      return ScriptLocalization.GetWithPara(this.Name, new Dictionary<string, string>()
      {
        {
          "0",
          this.BossLevel.ToString()
        }
      }, true);
    }
  }

  public string GetTalk()
  {
    int num = Random.Range(1, 5);
    string Term = string.Empty;
    switch (num)
    {
      case 1:
        Term = this.BossTalk1;
        break;
      case 2:
        Term = this.BossTalk2;
        break;
      case 3:
        Term = this.BossTalk3;
        break;
      case 4:
        Term = this.BossTalk4;
        break;
      case 5:
        Term = this.BossTalk5;
        break;
    }
    return ScriptLocalization.Get(Term, true);
  }
}
