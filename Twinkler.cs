﻿// Decompiled with JetBrains decompiler
// Type: Twinkler
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class Twinkler : MonoBehaviour
{
  public float fadetime = 0.5f;
  public float fadefactor = 0.98f;
  private float curFF;
  private SpriteRenderer[] srs;
  private bool canLoop;
  private bool needReGet;
  private Twinkler.Mode _mode;

  public Twinkler.Mode CurrentMode
  {
    get
    {
      return this._mode;
    }
  }

  public void Twinkle(Twinkler.Mode mode = Twinkler.Mode.ONCE)
  {
    this._mode = mode;
    this.srs = this.GetComponentsInChildren<SpriteRenderer>();
    switch (mode)
    {
      case Twinkler.Mode.LOOP:
        this.StartCoroutine(this.TwinkleLoop());
        break;
      case Twinkler.Mode.ONCE:
        this.StartCoroutine(this.TwinkleOnce());
        break;
      case Twinkler.Mode.DARK:
        this.TwinkleDark();
        break;
    }
  }

  [DebuggerHidden]
  private IEnumerator TwinkleLoop()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Twinkler.\u003CTwinkleLoop\u003Ec__IteratorB6()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void Update()
  {
    if (!this.canLoop || this.srs == null)
      return;
    foreach (SpriteRenderer sr in this.srs)
    {
      if ((Object) null == (Object) sr)
      {
        this.needReGet = true;
        break;
      }
      Color color = sr.color;
      color.r *= this.curFF;
      color.g *= this.curFF;
      color.b *= this.curFF;
      sr.color = color;
    }
    if (!this.needReGet)
      return;
    this.needReGet = false;
    this.srs = this.GetComponentsInChildren<SpriteRenderer>();
    this.Reset();
  }

  private void Reset()
  {
    if (this.srs == null)
      return;
    foreach (SpriteRenderer sr in this.srs)
    {
      if (!((Object) null == (Object) sr))
      {
        Color color = sr.color;
        color.r = 1f;
        color.g = 1f;
        color.b = 1f;
        sr.color = color;
      }
    }
  }

  private void OnDestroy()
  {
    this.Reset();
  }

  [DebuggerHidden]
  private IEnumerator TwinkleOnce()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Twinkler.\u003CTwinkleOnce\u003Ec__IteratorB7()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void TwinkleDark()
  {
    foreach (SpriteRenderer sr in this.srs)
    {
      Color color = sr.color;
      color.r *= 0.2f;
      color.g *= 0.2f;
      color.b *= 0.2f;
      sr.color = color;
    }
  }

  public enum Mode
  {
    LOOP,
    ONCE,
    DARK,
  }
}
