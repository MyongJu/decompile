﻿// Decompiled with JetBrains decompiler
// Type: ArtifactSpecialSaleData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class ArtifactSpecialSaleData
{
  private int _worldId;
  private int _configId;
  private int _artifactId;
  private int _slot;
  private int _endTime;
  private int _leftAmount;
  private long _price;

  public int WorldId
  {
    get
    {
      return this._worldId;
    }
  }

  public int ConfigId
  {
    get
    {
      return this._configId;
    }
  }

  public int ArtifactId
  {
    get
    {
      return this._artifactId;
    }
  }

  public int Slot
  {
    get
    {
      return this._slot;
    }
  }

  public int EndTime
  {
    get
    {
      return this._endTime;
    }
  }

  public int LeftAmount
  {
    get
    {
      return this._leftAmount;
    }
  }

  public long Price
  {
    get
    {
      return this._price;
    }
  }

  public void Decode(Hashtable data)
  {
    if (data == null)
      return;
    DatabaseTools.UpdateData(data, "world_id", ref this._worldId);
    DatabaseTools.UpdateData(data, "config_id", ref this._configId);
    DatabaseTools.UpdateData(data, "artifact_id", ref this._artifactId);
    DatabaseTools.UpdateData(data, "slot", ref this._slot);
    DatabaseTools.UpdateData(data, "end_time", ref this._endTime);
    DatabaseTools.UpdateData(data, "amount", ref this._leftAmount);
    DatabaseTools.UpdateData(data, "price", ref this._price);
  }

  public struct Params
  {
    public const string WORLD_ID = "world_id";
    public const string CONFIG_ID = "config_id";
    public const string ARTIFACT_ID = "artifact_id";
    public const string SLOT = "slot";
    public const string END_TIME = "end_time";
    public const string LEFT_AMOUNT = "amount";
    public const string PRICE = "price";
  }
}
