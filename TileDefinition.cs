﻿// Decompiled with JetBrains decompiler
// Type: TileDefinition
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class TileDefinition
{
  public const string PVP_PREFAB_PATH = "Prefab/Tiles/";
  public const string PIT_PREFAB_PATH = "Prefab/Pit/";
  public const string DEFAULT_PREFAB_PATH = "Prefab/Default/";
  private long _id;
  private string _prefabName;
  private TileType _tileType;

  public long ID
  {
    get
    {
      return this._id;
    }
    set
    {
      this._id = value;
    }
  }

  public void UpdatePrefab(string path)
  {
    PrefabManagerEx.Instance.AddRouteMapping(this._prefabName, path + this._prefabName);
  }

  public string PrefabName
  {
    get
    {
      return this._prefabName;
    }
    set
    {
      this._prefabName = value;
    }
  }

  public TileType TileType
  {
    get
    {
      return this._tileType;
    }
    set
    {
      this._tileType = value;
    }
  }
}
