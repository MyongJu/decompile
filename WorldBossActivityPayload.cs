﻿// Decompiled with JetBrains decompiler
// Type: WorldBossActivityPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class WorldBossActivityPayload
{
  private static WorldBossActivityPayload instance;
  private string damage;
  private string rank;
  private bool isPayloadNull;
  private int bossInfoReady;
  private KingdomBossData bossDBInfo;
  private bool needToRefreshMainDlg;

  public event System.Action OnRequestDataReceived;

  public static WorldBossActivityPayload Instance
  {
    get
    {
      if (WorldBossActivityPayload.instance == null)
        WorldBossActivityPayload.instance = new WorldBossActivityPayload();
      return WorldBossActivityPayload.instance;
    }
  }

  public string Damage
  {
    get
    {
      return this.damage;
    }
  }

  public string Rank
  {
    get
    {
      return this.rank;
    }
  }

  public bool IsPayloadNull
  {
    get
    {
      return this.isPayloadNull;
    }
  }

  public int BossInfoReady
  {
    get
    {
      return this.bossInfoReady;
    }
  }

  public KingdomBossData BossDBInfo
  {
    get
    {
      return this.bossDBInfo;
    }
  }

  public bool NeedToRefreshActivityMainDlg
  {
    get
    {
      return this.needToRefreshMainDlg;
    }
    set
    {
      this.needToRefreshMainDlg = value;
    }
  }

  private void Decode(object orgData)
  {
    this.ClearCachedData();
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable == null)
      return;
    if (hashtable.Contains((object) "damage") && hashtable[(object) "damage"] != null)
      this.damage = hashtable[(object) "damage"].ToString();
    else
      this.isPayloadNull = true;
    if (hashtable.Contains((object) "rank") && hashtable[(object) "rank"] != null)
      this.rank = hashtable[(object) "rank"].ToString();
    if (hashtable.Contains((object) "ready") && hashtable[(object) "ready"] != null)
      this.bossInfoReady = int.Parse(hashtable[(object) "ready"].ToString());
    if (this.OnRequestDataReceived == null)
      return;
    this.OnRequestDataReceived();
  }

  private void DecodeActivityTime(object data)
  {
    Hashtable source = data as Hashtable;
    if (source == null)
      return;
    ActivityManager.Intance.worldBoss.Decode(source);
  }

  public void ClearCachedData()
  {
    this.damage = (string) null;
    this.rank = (string) null;
    this.isPayloadNull = false;
    this.bossInfoReady = 0;
  }

  private void OnDBUpdated(long bossID)
  {
    DBManager.inst.DB_KingdomBossDB.onDataChanged -= new System.Action<long>(this.OnDBUpdated);
    this.bossDBInfo = DBManager.inst.DB_KingdomBossDB.Get((int) bossID);
  }

  public void RequestData(System.Action<bool, object> callback)
  {
    DBManager.inst.DB_KingdomBossDB.onDataChanged += new System.Action<long>(this.OnDBUpdated);
    this.RequestWorldBossData(callback, new Hashtable()
    {
      {
        (object) "kingdom_id",
        (object) PlayerData.inst.playerCityData.cityLocation.K
      }
    });
  }

  public void RequestWorldBossData(System.Action<bool, object> callback, Hashtable param)
  {
    MessageHub.inst.GetPortByAction("worldBoss:loadBossInfo").SendRequest(param, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void RequestNextActivityTime(System.Action<bool, object> callback, Hashtable param)
  {
    MessageHub.inst.GetPortByAction("WorldBoss:loadActivity").SendRequest(param, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.DecodeActivityTime(data);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }
}
