﻿// Decompiled with JetBrains decompiler
// Type: SystemMessage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;

public class SystemMessage : AbstractMailEntry
{
  public string link = string.Empty;
  private const string LINK_MAIL_PREFS_KEY = "link_mail_prefs_key";
  public SystemMessage.Reward reward;
  public double targetVersion;

  public int linkClicked
  {
    get
    {
      return PlayerPrefsEx.GetIntByUid("link_mail_prefs_key" + (object) this.mailID, 0);
    }
    set
    {
      PlayerPrefsEx.SetIntByUid("link_mail_prefs_key" + (object) this.mailID, value);
    }
  }

  public override void ApplyData(Hashtable ht)
  {
    base.ApplyData(ht);
    this.ConfigReward();
    if (this.type == MailType.MAIL_TYPE_SYSTEM_VERSION)
      this.ConfigVersion();
    else
      this.ConfigLink();
  }

  private void ConfigLink()
  {
    ArrayList arrayList = this.HtBody[(object) "params"] as ArrayList;
    if (arrayList == null || arrayList.Count < 1)
      return;
    Hashtable hashtable1 = arrayList[0] as Hashtable;
    if (hashtable1 == null)
      return;
    if (hashtable1.ContainsKey((object) "link_list"))
    {
      Hashtable hashtable2 = hashtable1[(object) "link_list"] as Hashtable;
      string currentLanguageCode = LocalizationManager.CurrentLanguageCode;
      if (hashtable2 == null)
        return;
      if (hashtable2.ContainsKey((object) currentLanguageCode))
      {
        this.link = hashtable2[(object) currentLanguageCode].ToString();
        this.link = string.Format(this.link, (object) PlayerData.inst.userData.userName, (object) AccountManager.Instance.FunplusID);
      }
      else
      {
        if (!hashtable1.ContainsKey((object) "link"))
          return;
        this.link = hashtable1[(object) "link"].ToString();
        this.link = string.Format(this.link, (object) PlayerData.inst.userData.userName, (object) AccountManager.Instance.FunplusID);
      }
    }
    else
    {
      if (!hashtable1.ContainsKey((object) "link"))
        return;
      this.link = hashtable1[(object) "link"].ToString();
      this.link = string.Format(this.link, (object) PlayerData.inst.userData.userName, (object) AccountManager.Instance.FunplusID);
    }
  }

  private void ConfigVersion()
  {
    ArrayList arrayList = this.HtBody[(object) "params"] as ArrayList;
    if (arrayList == null || arrayList.Count < 1)
      return;
    Hashtable hashtable = arrayList[0] as Hashtable;
    if (hashtable == null)
      return;
    this.targetVersion = 0.0;
    string[] strArray = hashtable[(object) "version"].ToString().Split(new char[1]
    {
      '.'
    }, int.MaxValue);
    for (int index = strArray.Length - 1; index >= 0; --index)
      this.targetVersion += Math.Pow(10.0, (double) ((strArray.Length - index - 1) * 5)) * (double) int.Parse(strArray[index]);
  }

  private void ConfigReward()
  {
    Hashtable hashtable = this.data[(object) "attachment"] as Hashtable;
    if (hashtable == null)
      return;
    this.reward = new SystemMessage.Reward();
    if (hashtable.ContainsKey((object) "resource"))
    {
      this.reward.resources = new List<SystemMessage.ResourceReward>();
      foreach (DictionaryEntry dictionaryEntry in hashtable[(object) "resource"] as Hashtable)
      {
        try
        {
          this.reward.resources.Add(new SystemMessage.ResourceReward()
          {
            resourceName = dictionaryEntry.Key.ToString(),
            amount = int.Parse(dictionaryEntry.Value.ToString())
          });
        }
        catch
        {
        }
      }
    }
    if (hashtable.ContainsKey((object) "item"))
    {
      this.reward.items = new List<SystemMessage.ItemReward>();
      foreach (DictionaryEntry dictionaryEntry in hashtable[(object) "item"] as Hashtable)
      {
        try
        {
          this.reward.items.Add(new SystemMessage.ItemReward()
          {
            internalID = int.Parse(dictionaryEntry.Key.ToString()),
            amount = int.Parse(dictionaryEntry.Value.ToString())
          });
        }
        catch
        {
        }
      }
    }
    if (!hashtable.ContainsKey((object) "gold"))
      return;
    int num = 0;
    try
    {
      num = int.Parse(hashtable[(object) "gold"].ToString());
    }
    catch
    {
      D.error((object) "Mail gold is not int num");
    }
    this.reward.goldReward = new SystemMessage.GoldReward()
    {
      amount = num
    };
  }

  public override void Display()
  {
    Hashtable hashtable = new Hashtable();
    hashtable.Add((object) "data", this.data[(object) "data"]);
    if (this.type == MailType.MAIL_TYPE_SYSTEM_INTEGRAL_REWARD)
      UIManager.inst.OpenPopup("Activity/ActivityRewardsPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
      {
        hashtable = hashtable,
        mail = (AbstractMailEntry) this
      });
    else if (this.type == MailType.MAIL_TYPE_SYSTEM_VERSION)
      UIManager.inst.OpenPopup("Mail/UpdateMailPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
      {
        mail = (AbstractMailEntry) this
      });
    else if (this.type == MailType.MAIL_TYPE_SYSTEM_RECOMMEND_ALLIANCE)
      UIManager.inst.OpenPopup("Mail/AllianceRecommendMailPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
      {
        hashtable = hashtable,
        mail = (AbstractMailEntry) this
      });
    else if (this.type == MailType.MAIL_TYPE_SYSTEM_ABYSS_RESULT_NOTICE)
      UIManager.inst.OpenPopup("Mail/AbyssMailPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
      {
        mail = (AbstractMailEntry) this
      });
    else if (this.type == MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_KINGS_START || this.type == MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_START || (this.type == MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_END || this.type == MailType.MAIL_TYPE_WORLD_BOSS_START))
      UIManager.inst.OpenPopup("Mail/AnnKingArenaOpenClosePopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
      {
        hashtable = hashtable,
        mail = (AbstractMailEntry) this
      });
    else
      UIManager.inst.OpenPopup("Mail/SystemMailPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
      {
        mail = (AbstractMailEntry) this
      });
    base.Display();
  }

  public override bool CanReceiveAttachment()
  {
    if ((!string.IsNullOrEmpty(this.link) ? (this.linkClicked == 1 ? 1 : 0) : 1) != 0 && (this.type != MailType.MAIL_TYPE_SYSTEM_VERSION ? 1 : (this.GetCurrentVersion() >= this.targetVersion ? 1 : 0)) != 0)
      return base.CanReceiveAttachment();
    return false;
  }

  public double GetCurrentVersion()
  {
    double num = 0.0;
    string[] strArray = NativeManager.inst.AppMajorVersion.Split(new char[1]
    {
      '.'
    }, int.MaxValue);
    for (int index = strArray.Length - 1; index >= 0; --index)
      num += Math.Pow(10.0, (double) ((strArray.Length - index - 1) * 5)) * (double) int.Parse(strArray[index]);
    return num;
  }

  public class ItemReward
  {
    public int internalID;
    public int amount;
  }

  public class ResourceReward
  {
    public string resourceName;
    public int amount;
  }

  public class GoldReward
  {
    public int amount;
  }

  public class Reward
  {
    public List<SystemMessage.ItemReward> items;
    public List<SystemMessage.ResourceReward> resources;
    public SystemMessage.GoldReward goldReward;
  }
}
