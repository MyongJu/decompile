﻿// Decompiled with JetBrains decompiler
// Type: ComplexTypeIcon
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class ComplexTypeIcon : MonoBehaviour
{
  public int MAX_SIZE = 310;
  public UISprite normalSprite;
  public UITexture textureIcon;
  public GameObject gameObjectIcon;
  [NonSerialized]
  public ComplexTypeIcon.ImageType type;
  public ComplexTypeIcon.SubType subType;

  public void SetData(string name, ComplexTypeIcon.ImageType type, ComplexTypeIcon.SubType subType)
  {
    this.HideAll();
    this.type = type;
    this.subType = subType;
    string str = this.GetPath() + name;
    switch (type)
    {
      case ComplexTypeIcon.ImageType.UISprite:
        this.SetUISprite(str);
        break;
      case ComplexTypeIcon.ImageType.UITexture:
        this.SetUITexture(str);
        break;
      case ComplexTypeIcon.ImageType.GO:
        this.SetGo(str);
        break;
    }
  }

  private void SetUISprite(string name)
  {
    this.normalSprite.spriteName = name;
    this.SetChild((UIWidget) this.normalSprite, true);
  }

  private void SetUITexture(string path)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.textureIcon, path, (System.Action<bool>) null, true, false, string.Empty);
    this.SetChild((UIWidget) this.textureIcon, true);
  }

  private void SetGo(string path)
  {
    GameObject prefab = AssetManager.Instance.HandyLoad(path, (System.Type) null) as GameObject;
    NGUITools.SetActive(this.gameObjectIcon, true);
    if (!((UnityEngine.Object) prefab != (UnityEngine.Object) null))
      return;
    this.Clear();
    GameObject gameObject = NGUITools.AddChild(this.gameObjectIcon, prefab);
    gameObject.GetComponent<UI2DSprite>().depth = 3;
    UI2DSprite component = gameObject.GetComponent<UI2DSprite>();
    if (component.width > this.MAX_SIZE)
    {
      float num = (float) this.MAX_SIZE / (float) component.width;
      component.width = this.MAX_SIZE;
      component.height = (int) ((double) component.height * (double) num);
    }
    if (component.height <= this.MAX_SIZE)
      return;
    float num1 = (float) this.MAX_SIZE / (float) component.height;
    component.height = this.MAX_SIZE;
    component.width = (int) ((double) component.width * (double) num1);
  }

  public void Clear()
  {
    if (!((UnityEngine.Object) this.textureIcon != (UnityEngine.Object) null))
      return;
    BuilderFactory.Instance.Release((UIWidget) this.textureIcon);
  }

  private string GetPath()
  {
    if (this.type == ComplexTypeIcon.ImageType.GO)
    {
      if (this.subType == ComplexTypeIcon.SubType.Building)
        return "Prefab/UI/BuildingPrefab/ui_";
    }
    else if (this.type == ComplexTypeIcon.ImageType.UITexture)
    {
      if (this.subType == ComplexTypeIcon.SubType.Items)
        return Utils.GetUITextPath();
      if (this.subType == ComplexTypeIcon.SubType.Daily)
        return "Texture/Daily_reward/";
    }
    return string.Empty;
  }

  private void HideAll()
  {
    if ((UnityEngine.Object) this.gameObjectIcon != (UnityEngine.Object) null)
      NGUITools.SetActive(this.gameObjectIcon, false);
    this.SetChild((UIWidget) this.normalSprite, false);
    this.SetChild((UIWidget) this.textureIcon, false);
  }

  private void SetChild(UIWidget ui, bool visable = false)
  {
    if (!((UnityEngine.Object) ui != (UnityEngine.Object) null))
      return;
    NGUITools.SetActive(ui.gameObject, visable);
  }

  public enum ImageType
  {
    None,
    UISprite,
    UITexture,
    GO,
  }

  public enum SubType
  {
    None,
    Items,
    Daily,
    Building,
  }
}
