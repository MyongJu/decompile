﻿// Decompiled with JetBrains decompiler
// Type: AllianceMagicHistoryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceMagicHistoryPopup : Popup
{
  private List<IHistoryData> datas = new List<IHistoryData>();
  private List<AllianceMagicHistoryItem> items = new List<AllianceMagicHistoryItem>();
  private string _action = "AllianceTemple:loadHistory";
  public UILabel emptyLabel;
  public UIScrollView scroll;
  public UITable container;
  public UILabel title;
  public UILabel tip;
  public AllianceMagicHistoryItem itemPrefab;
  private HistoryDataProxy _proxy;
  public UIButton _viewMailButton;
  private bool isDestroy;
  private int initCount;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    AllianceMagicHistoryPopup.Parameter parameter = orgParam as AllianceMagicHistoryPopup.Parameter;
    if (parameter != null && !string.IsNullOrEmpty(parameter.action))
      this._action = parameter.action;
    this._proxy = new HistoryDataProxy(this._action);
    this.title.text = this._proxy.GetTitle();
    this.emptyLabel.text = this._proxy.GetEmptyLabel();
    NGUITools.SetActive(this.tip.gameObject, false);
    if ((UnityEngine.Object) this._viewMailButton != (UnityEngine.Object) null && parameter != null)
      NGUITools.SetActive(this._viewMailButton.gameObject, parameter.viewMail);
    this.LoadData();
  }

  public void OnViewMail()
  {
    this.OnCloseHandler();
    UIManager.inst.OpenDlg("Mail/MailMainDlg", (UI.Dialog.DialogParameter) new MailMainDlg.Parameter()
    {
      category = MailCategory.BattleReport
    }, true, true, true);
  }

  private void OnDestroy()
  {
    this.isDestroy = true;
  }

  private void UpdateUI()
  {
    if (this.isDestroy)
      return;
    this.datas = this._proxy.GetData();
    for (int index = 0; index < this.datas.Count; ++index)
    {
      AllianceMagicHistoryItem magicHistoryItem = this.GetItem();
      magicHistoryItem.SetData(this.datas[index]);
      magicHistoryItem.InitCallBack = new System.Action<AllianceMagicHistoryItem>(this.InitCallBack);
      this.items.Add(magicHistoryItem);
    }
    if (this.datas.Count != 0)
      return;
    NGUITools.SetActive(this.emptyLabel.gameObject, true);
  }

  private void InitCallBack(AllianceMagicHistoryItem item)
  {
    ++this.initCount;
    item.transform.parent = this.container.transform;
    if (this.initCount != this.datas.Count)
      return;
    this.container.Reposition();
    this.scroll.ResetPosition();
    NGUITools.SetActive(this.tip.gameObject, this.scroll.shouldMoveVertically);
  }

  private AllianceMagicHistoryItem GetItem()
  {
    GameObject go = NGUITools.AddChild(this.scroll.gameObject, this.itemPrefab.gameObject);
    AllianceMagicHistoryItem component = go.GetComponent<AllianceMagicHistoryItem>();
    NGUITools.SetActive(go, true);
    return component;
  }

  private void LoadData()
  {
    this.datas.Clear();
    NGUITools.SetActive(this.emptyLabel.gameObject, false);
    this._proxy.OnFetchDataFinish -= new System.Action(this.UpdateUI);
    this._proxy.OnFetchDataFinish += new System.Action(this.UpdateUI);
    this._proxy.FetchData();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Clear();
  }

  private void Clear()
  {
    this._proxy.OnFetchDataFinish -= new System.Action(this.UpdateUI);
    this._proxy.Destroy();
    this._proxy = (HistoryDataProxy) null;
    for (int index = 0; index < this.items.Count; ++index)
      this.items[index].InitCallBack = (System.Action<AllianceMagicHistoryItem>) null;
    this.items.Clear();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string action = string.Empty;
    public bool viewMail;
  }
}
