﻿// Decompiled with JetBrains decompiler
// Type: NetWorkDetector
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Funplus;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class NetWorkDetector
{
  private Dictionary<int, System.Action> _tipHandlers = new Dictionary<int, System.Action>();
  private List<System.Action> tipsHandler = new List<System.Action>();
  private float TIME = 4f;
  private int _dispatchRetryCount = -1;
  private int _connectRetryCount = -1;
  private float _networkLastCheckTime = -1f;
  private float _connectionLastCheckTime = -1f;
  private float _reconnectTime = -1f;
  private int _focusTime = -1;
  private int restartTime = 120;
  private int loseConnectionTime = -1;
  private bool _canReconnect = true;
  private float startConnectTime = 2f;
  private int retryMaxCount = 3;
  private float _popupTime = -1f;
  private float _popupLastTime = -1f;
  private Queue<NetWorkDetector.RUMInfo> caches = new Queue<NetWorkDetector.RUMInfo>();
  private Queue<NetWorkDetector.BIInfo> bicaches = new Queue<NetWorkDetector.BIInfo>();
  public const float NetworkCheckInterval = 3f;
  public const float ConnectionCheckInterval = 1f;
  private static NetWorkDetector m_Instance;
  private bool displayTip;
  private NetWorkDetector.NetWorkState _currentNetWork;
  private int tipIndex;
  private int currentIndex;
  private bool isKick;
  private bool _isRetry;
  private bool _isLoseRetry;
  private bool _isNetworkAvailable;
  private bool _isConnectionAvailable;
  private int _reconnectCount;
  private bool _connectionInited;
  private float loseConnectTime;
  private bool _showReConnectRTMPop;
  private bool _showRestartPop;

  public event System.Action OnLoseNetWork;

  public event System.Action OnLoseNetWorkHandler;

  public event System.Action OnRecacheNetWork;

  public event System.Action OnNetWorkStateChanged;

  public static NetWorkDetector Instance
  {
    get
    {
      if (NetWorkDetector.m_Instance == null)
        NetWorkDetector.m_Instance = new NetWorkDetector();
      return NetWorkDetector.m_Instance;
    }
  }

  public NetWorkDetector.NetWorkState CurrentNetWork
  {
    get
    {
      return this._currentNetWork;
    }
    protected set
    {
      this._currentNetWork = value;
      if (this._currentNetWork == NetWorkDetector.NetWorkState.NotReachable || this._currentNetWork == NetWorkDetector.NetWorkState.None)
        this.IsNetworkAvailable = false;
      else
        this.IsNetworkAvailable = true;
    }
  }

  public void Init()
  {
    this.AddEventHandler();
    this.CurrentNetWork = NetWorkDetector.NetWorkState.NotReachable;
    this._connectionInited = false;
    this.displayTip = false;
    this._showRestartPop = false;
    this._showReConnectRTMPop = false;
    this.StopCheck = false;
    this.loseConnectionTime = 0;
    this.isKick = false;
  }

  public void ResetConnection()
  {
    this._connectionInited = false;
    this._isRetry = true;
  }

  public void Dispose()
  {
    this._isRetry = false;
    this._isLoseRetry = false;
    this._showRestartPop = false;
    this._showReConnectRTMPop = false;
    this._connectionInited = false;
    this.displayTip = false;
    this.RemoveEventHandler();
    this._tipHandlers.Clear();
    this.tipIndex = 0;
    this.currentIndex = 0;
    this.loseConnectionTime = 0;
    this._dispatchRetryCount = -1;
    this._connectRetryCount = -1;
  }

  private void AddEventHandler()
  {
    PushManager.inst.ConnectCallback += new System.Action<bool>(this.ConnectionCallbackHandler);
    Oscillator.Instance.updateEvent += new System.Action<double>(this.Check);
    PushManager.inst.ConsistentErrorCallback += new System.Action(this.OnConsistentError);
    PushManager.inst.OnKickedOutCallback += new System.Action(this.OnKickOutHandler);
  }

  private void OnKickOutHandler()
  {
    this.isKick = true;
    this.QuitGame(ScriptLocalization.Get("network_error_title", true), ScriptLocalization.Get("session_terminated_another_device_log_in_description", true));
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.Check);
    PushManager.inst.ConsistentErrorCallback -= new System.Action(this.OnConsistentError);
    PushManager.inst.ConnectCallback -= new System.Action<bool>(this.ConnectionCallbackHandler);
    PushManager.inst.OnKickedOutCallback -= new System.Action(this.OnKickOutHandler);
  }

  public int RTM_DISPATCH_COUNT
  {
    get
    {
      return this._dispatchRetryCount;
    }
    set
    {
      this._dispatchRetryCount = value;
    }
  }

  public int RTM_CONNECT_COUNT
  {
    get
    {
      return this._connectRetryCount;
    }
    set
    {
      this._connectRetryCount = value;
    }
  }

  public int RTM_KINGDOM_HISTORY_TIME { get; set; }

  public int RTM_ALLIANCE_HISTORY_TIME { get; set; }

  public int RTM_PERSONAL_HISTORY_TIME { get; set; }

  public int RTM_TIME { get; set; }

  private void SendRTMHistoryTime2RUM()
  {
    if (this.RTM_KINGDOM_HISTORY_TIME > 0)
    {
      this.SaveRUMData("RTM_Load_Kingdom_History" + this.Retry, NetApi.inst.RTM_URL, "200", this.RTM_KINGDOM_HISTORY_TIME, 0, 0, string.Empty, 0L, 0L);
      this.RTM_KINGDOM_HISTORY_TIME = 0;
    }
    if (this.RTM_ALLIANCE_HISTORY_TIME > 0)
    {
      this.SaveRUMData("RTM_Load_Alliance_History" + this.Retry, NetApi.inst.RTM_URL, "200", this.RTM_ALLIANCE_HISTORY_TIME, 0, 0, string.Empty, 0L, 0L);
      this.RTM_ALLIANCE_HISTORY_TIME = 0;
    }
    if (this.RTM_PERSONAL_HISTORY_TIME <= 0)
      return;
    this.SaveRUMData("RTM_Load_Personal_History" + this.Retry, NetApi.inst.RTM_URL, "200", this.RTM_PERSONAL_HISTORY_TIME, 0, 0, string.Empty, 0L, 0L);
    this.RTM_PERSONAL_HISTORY_TIME = 0;
  }

  public void SendRTMRetry2RUM(bool auto = true)
  {
    string action = "RTM_RETRY_CONNECT_AUTO";
    if (!auto)
      action = "RTM_RETRY_CONNECT_MANUAL";
    this.SaveRUMData(action, NetApi.inst.RTM_URL, "200", 0, 0, 0, string.Empty, 0L, 0L);
  }

  private void SendData2RUM()
  {
    if (Application.isEditor)
      return;
    this.SendRTMHistoryTime2RUM();
    if (this.RTM_TIME == 0)
      return;
    string httpStatus = "success";
    if (this.RTM_TIME < 0)
      httpStatus = "fail";
    int httpLatency = this.RTM_TIME <= 0 ? 0 : this.RTM_TIME;
    string rtmUrl = NetApi.inst.RTM_URL;
    switch (NetApi.inst.RTM_Status)
    {
      case NetApi.RTMSTATE.DispatchError:
        string httpUrl1 = rtmUrl + "error=" + NetApi.inst.RTMError;
        if (this.RTM_DISPATCH_COUNT > 0 && this.Retry == "_retry")
          httpUrl1 = httpUrl1 + ";count=" + (object) this.RTM_DISPATCH_COUNT;
        this.SaveRUMData("RTM_Dispatch_Fail" + this.Retry, httpUrl1, httpStatus, -this.RTM_TIME, 0, 0, string.Empty, 0L, 0L);
        break;
      case NetApi.RTMSTATE.ConnectionFail:
        string httpUrl2 = rtmUrl + ";error=" + NetApi.inst.RTMError;
        if (this.RTM_CONNECT_COUNT > 0 && this.Retry == "_retry")
          httpUrl2 = httpUrl2 + ";count=" + (object) this.RTM_CONNECT_COUNT;
        this.SaveRUMData("RTM_Connect_Fail" + this.Retry, httpUrl2, httpStatus, -this.RTM_TIME, 0, 0, string.Empty, 0L, 0L);
        break;
      case NetApi.RTMSTATE.AuthFail:
        this.SaveRUMData("RTM_Auth_Fail" + this.Retry, rtmUrl, httpStatus, httpLatency, 0, 0, string.Empty, 0L, 0L);
        break;
      case NetApi.RTMSTATE.Success:
        this.SaveRUMData("RTM_All_Success" + this.Retry, rtmUrl, httpStatus, httpLatency, 0, 0, string.Empty, 0L, 0L);
        break;
    }
    this.RTM_TIME = 0;
  }

  private string Retry
  {
    get
    {
      if (this._isLoseRetry)
        return "_in_game_retry";
      if (this._isRetry)
        return "_retry";
      return string.Empty;
    }
  }

  private void OnConsistentError()
  {
    this.SaveRUMData("RTM_Consistent_Error", "EmptyURL", "fail", 0, 0, 0);
    string str = !JobManager.Instance.HasServerFinishedJob ? string.Empty : "JobFinish";
    string errorMarchData = DBManager.inst.DB_March.GetErrorMarchData();
    if (!string.IsNullOrEmpty(str) || !string.IsNullOrEmpty(errorMarchData))
    {
      string errorDetail = str + "|" + errorMarchData;
      if (!this.IsReadyRestart())
        this.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.RTM, "Close", errorDetail);
      this.RestartGame(false);
    }
    else
    {
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode && GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PitMode)
        return;
      PVPMapData.MapData.Dispose(true);
      PVPSystem.Instance.Map.ForceUpdate = true;
    }
  }

  private bool StopCheck { get; set; }

  public bool WaittingRestart
  {
    get
    {
      return this._showRestartPop;
    }
  }

  public void StopMonitor()
  {
    this.StopCheck = true;
  }

  public void StartMonitor()
  {
    this.StopCheck = false;
  }

  private void Check(double time)
  {
    this.SendData2RUM();
    if (this.StopCheck)
      return;
    if ((double) this._networkLastCheckTime < 0.0 || (double) Time.time - (double) this._networkLastCheckTime > 3.0)
    {
      this._networkLastCheckTime = Time.time;
      NetWorkDetector.NetWorkState netWorkState = this.GetNetWorkState();
      if (this.CurrentNetWork != netWorkState)
      {
        if (this.CurrentNetWork == NetWorkDetector.NetWorkState.NotReachable && (netWorkState == NetWorkDetector.NetWorkState.CarrierData || netWorkState == NetWorkDetector.NetWorkState.WIFI))
        {
          if (this.OnRecacheNetWork != null)
            this.OnRecacheNetWork();
          this.CurrentNetWork = netWorkState;
          if (this.OnNetWorkStateChanged != null)
            this.OnNetWorkStateChanged();
        }
        else if ((this.CurrentNetWork == NetWorkDetector.NetWorkState.CarrierData || this.CurrentNetWork == NetWorkDetector.NetWorkState.WIFI) && netWorkState == NetWorkDetector.NetWorkState.NotReachable)
        {
          this.CurrentNetWork = netWorkState;
          if (this.OnLoseNetWork != null)
            this.OnLoseNetWork();
          if (this.OnNetWorkStateChanged != null)
            this.OnNetWorkStateChanged();
        }
      }
    }
    if ((double) this._connectionLastCheckTime >= 0.0 && (double) Time.time - (double) this._connectionLastCheckTime <= 1.0)
      return;
    this._connectionLastCheckTime = Time.time;
    this.CheckNetworkState();
  }

  private NetWorkDetector.NetWorkState GetNetWorkState()
  {
    NetWorkDetector.NetWorkState netWorkState = NetWorkDetector.NetWorkState.None;
    switch (Application.internetReachability)
    {
      case NetworkReachability.NotReachable:
        netWorkState = NetWorkDetector.NetWorkState.NotReachable;
        break;
      case NetworkReachability.ReachableViaCarrierDataNetwork:
        netWorkState = NetWorkDetector.NetWorkState.CarrierData;
        break;
      case NetworkReachability.ReachableViaLocalAreaNetwork:
        netWorkState = NetWorkDetector.NetWorkState.WIFI;
        break;
    }
    return netWorkState;
  }

  public void RetryTip(System.Action handler, string title = null, string content = null)
  {
    if (this._showRestartPop)
      return;
    if (!this.tipsHandler.Contains(handler))
      this.tipsHandler.Add(handler);
    if (this.displayTip)
      return;
    this.displayTip = true;
    if (string.IsNullOrEmpty(content))
      content = ScriptLocalization.Get("network_error_description", true);
    if (string.IsNullOrEmpty(title))
      title = ScriptLocalization.Get("network_error_title", true);
    string left = ScriptLocalization.Get("network_error_try_again_button", true);
    ScriptLocalization.Get("network_error_send_feedback_button", true);
    string right = ScriptLocalization.Get("network_error_exit_game", true);
    TutorialManager.Instance.Pause();
    UIManager.inst.ShowNetErrorBox(title, content, left, right, NetErrorBlocker.ButtonState.OK_CENTER, new System.Action(this.RetryAgain), new System.Action(this.RetryAgain), new System.Action(this.RetryAgain));
  }

  public void RestartGame(string tip, bool deepLoad = true)
  {
    if (this._showRestartPop)
      return;
    Time.timeScale = 0.0f;
    this._showRestartPop = true;
    this.StopCheck = true;
    string content = tip;
    string title = ScriptLocalization.Get("network_error_title", true);
    string left = ScriptLocalization.Get("network_error_reestablish_connection_button", true);
    string right = ScriptLocalization.Get("network_error_send_feedback_button", true);
    TutorialManager.Instance.Pause();
    System.Action action = new System.Action(this.MarkRestartGame);
    if (!deepLoad)
      action = new System.Action(this.MarkSoftRestartGame);
    UIManager.inst.ShowNetErrorBox(title, content, left, right, NetErrorBlocker.ButtonState.OK_CENTER, action, action, action);
  }

  public void RestartGame(bool deepLoad = true)
  {
    this.RestartGame(ScriptLocalization.Get("network_error_description", true), deepLoad);
  }

  public void QuitGame(string title, string tip)
  {
    if (this._showRestartPop)
      return;
    this._showRestartPop = true;
    this.StopCheck = true;
    string content = tip;
    string left = ScriptLocalization.Get("network_error_exit_game", true);
    TutorialManager.Instance.Pause();
    UIManager.inst.ShowNetErrorBox(title, content, left, string.Empty, NetErrorBlocker.ButtonState.OK_CENTER, new System.Action(this.QuitGame), new System.Action(this.QuitGame), new System.Action(this.QuitGame));
  }

  public void RestartOrQuitGame(string tip, string title = null, string leftBt = null)
  {
    if (this._showRestartPop)
      return;
    this._showRestartPop = true;
    this.StopCheck = true;
    string content = tip;
    if (string.IsNullOrEmpty(title))
      title = ScriptLocalization.Get("network_error_title", true);
    string left = ScriptLocalization.Get("network_error_reestablish_connection_button", true);
    if (leftBt != null)
      left = leftBt;
    string right = ScriptLocalization.Get("network_error_exit_game", true);
    TutorialManager.Instance.Pause();
    UIManager.inst.ShowNetErrorBox(title, content, left, right, NetErrorBlocker.ButtonState.OK_CENTER, new System.Action(this.MarkRestartGame), new System.Action(this.MarkRestartGame), new System.Action(this.MarkRestartGame));
  }

  private void MarkRestartGame()
  {
    GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Deep);
  }

  private void MarkSoftRestartGame()
  {
    GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Lite);
  }

  private void RetryAgain()
  {
    TutorialManager.Instance.Resume();
    if (this.OnLoseNetWorkHandler != null)
      this.OnLoseNetWorkHandler();
    this.displayTip = false;
    if (this.tipsHandler.Count <= 0)
      return;
    for (int index = 0; index < this.tipsHandler.Count; ++index)
      this.tipsHandler[index]();
    this.tipsHandler.Clear();
  }

  private void QuitGame()
  {
    Application.Quit();
  }

  public bool ConnectionInited
  {
    get
    {
      return this._connectionInited;
    }
  }

  public bool IsNetworkAvailable
  {
    get
    {
      return this._isNetworkAvailable;
    }
    set
    {
      if (this._isNetworkAvailable != value)
        this._isNetworkAvailable = value;
      if (!value)
        ;
    }
  }

  public bool IsConnectionAvailable
  {
    get
    {
      return this._isConnectionAvailable;
    }
    set
    {
      if (this._isConnectionAvailable == value)
        return;
      this._isConnectionAvailable = value;
      if (!value)
        return;
      this.ClearReconnectInfo();
    }
  }

  public void OnApplicationFocus(bool state)
  {
    if (state)
    {
      this._focusTime = this.Now;
      if (PushManager.inst != null)
        PushManager.inst.SyncConnectionState();
    }
    else
      this.ClearReconnectInfo();
    this.StopCheck = !state;
  }

  public void ConnectionCallbackHandler(bool state)
  {
    if (!this._connectionInited)
      this.CanConnect = !state;
    this._connectionInited = true;
    this.IsConnectionAvailable = state;
  }

  private bool CanConnect
  {
    get
    {
      return this._canReconnect;
    }
    set
    {
      this._canReconnect = value;
    }
  }

  public void CheckNetworkState()
  {
    if (!this._connectionInited || this.IsConnectionAvailable)
      return;
    if (this._focusTime > -1 && this.Now - this._focusTime < 3 && !this.isKick)
    {
      this.RemoveEventHandler();
      GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Lite);
    }
    else if (!this.CanConnect)
    {
      if ((double) this.loseConnectTime <= 0.0)
        this.loseConnectTime = Time.time;
      if (this.isKick)
      {
        this.StopCheck = true;
      }
      else
      {
        if ((double) Time.time - (double) this.loseConnectTime < (double) this.startConnectTime || (double) this._reconnectTime > 0.0 && (double) this.Now - (double) this._reconnectTime <= 50.0)
          return;
        this.TryReconnect();
      }
    }
    else
    {
      if ((double) this._reconnectTime > 0.0 && (double) this.Now - (double) this._reconnectTime <= 50.0)
        return;
      this.TryReconnect();
    }
  }

  public void TryReconnect()
  {
    if (this._reconnectCount < this.retryMaxCount)
    {
      NetWorkDetector.Instance.SendRTMRetry2RUM(true);
      ++this._reconnectCount;
      this._reconnectTime = Time.realtimeSinceStartup;
      this._isLoseRetry = true;
      GameEngine.Instance.ChatManager.ReConnect();
    }
    else
    {
      this.StopCheck = true;
      if (!this.IsReadyRestart())
        this.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.RTM, "Retry", string.Empty);
      this.RestartGame(true);
    }
  }

  public void ClearReconnectInfo()
  {
    this._reconnectCount = 0;
    this._reconnectTime = -1f;
  }

  public bool IsReadyRestart()
  {
    return this._showRestartPop;
  }

  private void ShowRetryConnectRTMPopup()
  {
    if (this._showReConnectRTMPop)
      return;
    this.loseConnectionTime = this.Now;
    this._showReConnectRTMPop = true;
    this.StopCheck = true;
    string content = ScriptLocalization.Get("network_error_description", true);
    string title = ScriptLocalization.Get("network_error_title", true);
    string left = ScriptLocalization.Get("network_error_try_again_button", true);
    string right = ScriptLocalization.Get("network_error_send_feedback_button", true);
    TutorialManager.Instance.Pause();
    UIManager.inst.ShowNetErrorBox(title, content, left, right, NetErrorBlocker.ButtonState.YES_LEFT | NetErrorBlocker.ButtonState.NO_RIGHT, new System.Action(this.OnTryReconnectConfirm), new System.Action(this.MarkRestartGame), new System.Action(this.OnTryReconnectConfirm));
  }

  private int Now
  {
    get
    {
      return (int) Time.realtimeSinceStartup;
    }
  }

  private void OnTryReconnectConfirm()
  {
    if (this.Now - this.loseConnectionTime > this.restartTime)
    {
      this.StopCheck = true;
      this.MarkRestartGame();
    }
    else
    {
      this._reconnectCount = 0;
      this._showReConnectRTMPop = false;
      this.StopCheck = false;
      this.TryReconnect();
    }
  }

  public void SaveRUMData(string action, string httpUrl, string httpStatus, int httpLatency, int requestSize, int responseSize, string requestId, long sendTime, long receviceTime)
  {
    if (httpStatus == null)
      return;
    if (NetApi.inst.PZTest == "1")
      action += "_pztest";
    if (FunplusSdk.Instance.IsSdkInstalled() && PlayerData.inst.playerCityData != null)
    {
      string gameserverId = PlayerData.inst.playerCityData.cityLocation.K.ToString();
      string funplusId = AccountManager.Instance.FunplusID;
      while (this.caches.Count > 0)
      {
        NetWorkDetector.RUMInfo rumInfo = this.caches.Dequeue();
        if (rumInfo != null)
          FunplusRum.GetIntance().TraceServiceMonitoring(rumInfo.action, rumInfo.httpUrl, rumInfo.httpStatus, rumInfo.httpLatency, rumInfo.requestSize, rumInfo.responseSize, funplusId, rumInfo.requestId, gameserverId, rumInfo.sendTime, rumInfo.receiveTime);
      }
      httpUrl = httpUrl + "\\CurrentNetWork=" + this.CurrentNetWork.ToString();
      FunplusRum.GetIntance().TraceServiceMonitoring(action, httpUrl, httpStatus, httpLatency, requestSize, responseSize, funplusId, requestId, gameserverId, sendTime, receviceTime);
    }
    else
    {
      NetWorkDetector.RUMInfo rumInfo1 = new NetWorkDetector.RUMInfo();
      rumInfo1.action = action;
      rumInfo1.httpUrl = httpUrl;
      NetWorkDetector.RUMInfo rumInfo2 = rumInfo1;
      rumInfo2.httpUrl = rumInfo2.httpUrl + "\\CurrentNetWork=" + this.CurrentNetWork.ToString();
      rumInfo1.httpStatus = httpStatus;
      rumInfo1.httpLatency = httpLatency;
      rumInfo1.requestSize = requestSize;
      rumInfo1.responseSize = responseSize;
      rumInfo1.receiveTime = receviceTime;
      rumInfo1.requestId = requestId;
      rumInfo1.sendTime = sendTime;
      this.caches.Enqueue(rumInfo1);
    }
  }

  public void Send37LoginError2RUM(string reason)
  {
    this.SaveRUMData("37Login", reason, "fail", 0, 0, 0);
  }

  public void SendRestartGameError2RUM(NetWorkDetector.RestartErrorType type, string subType = "", string errorDetail = "")
  {
    string action = "RestartError";
    string str = errorDetail;
    if (string.IsNullOrEmpty(str))
      str = "EmptyURL";
    string httpUrl = str + "_" + (object) type;
    if (!string.IsNullOrEmpty(subType))
      httpUrl = httpUrl + "_" + subType;
    string httpStatus = "fail";
    this.SaveRUMData(action, httpUrl, httpStatus, 0, 0, 0);
  }

  public void SaveRUMData(string action, string httpUrl, string httpStatus, int httpLatency, int requestSize, int responseSize)
  {
    D.warn((object) ("SaveRUMData action=" + action));
    if (httpStatus == null)
      return;
    httpUrl = httpUrl + "\\CurrentNetWork=" + this.CurrentNetWork.ToString();
    if (FunplusSdk.Instance.IsSdkInstalled())
    {
      while (this.caches.Count > 0)
      {
        NetWorkDetector.RUMInfo rumInfo = this.caches.Dequeue();
        if (rumInfo != null)
          FunplusRum.GetIntance().TraceServiceMonitoring(rumInfo.action, rumInfo.httpUrl, rumInfo.httpStatus, rumInfo.httpLatency, rumInfo.requestSize, rumInfo.responseSize);
      }
      FunplusRum.GetIntance().TraceServiceMonitoring(action, httpUrl, httpStatus, httpLatency, requestSize, responseSize);
    }
    else
      this.caches.Enqueue(new NetWorkDetector.RUMInfo()
      {
        action = action,
        httpUrl = httpUrl,
        httpStatus = httpStatus,
        httpLatency = httpLatency,
        requestSize = requestSize,
        responseSize = responseSize
      });
  }

  public void PushBI(string eventName, string json)
  {
    if (FunplusSdk.Instance.IsSdkInstalled())
    {
      while (this.bicaches.Count > 0)
      {
        NetWorkDetector.BIInfo biInfo = this.bicaches.Dequeue();
        if (biInfo != null)
          FunplusBi.Instance.TraceEvent(biInfo.eventName, biInfo.content);
      }
      FunplusBi.Instance.TraceEvent(eventName, json);
    }
    else
      this.bicaches.Enqueue(new NetWorkDetector.BIInfo()
      {
        eventName = eventName,
        content = json
      });
  }

  public void PushClientError(string type, string stack)
  {
    Hashtable hashtable = new Hashtable();
    hashtable.Add((object) "trace", (object) stack);
    hashtable.Add((object) "error_msg", (object) type);
    hashtable.Add((object) "utc", (object) NetServerTime.inst.ServerTimestamp);
    hashtable.Add((object) "uid", (object) PlayerPrefsEx.GetString("saved_uid", string.Empty));
    D.warn((object) (nameof (PushClientError) + type));
    FunplusBi.Instance.TraceLog(Utils.Object2Json((object) hashtable));
    D.warn((object) (nameof (PushClientError) + stack));
  }

  public enum NetWorkState
  {
    None,
    NotReachable,
    WIFI,
    CarrierData,
  }

  private class RUMInfo
  {
    public string action = string.Empty;
    public string httpUrl = string.Empty;
    public string httpStatus = string.Empty;
    public string targetUserId = string.Empty;
    public string requestId = string.Empty;
    public string gameserverId = string.Empty;
    public int httpLatency;
    public int requestSize;
    public int responseSize;
    public long sendTime;
    public long receiveTime;
  }

  public enum RestartErrorType
  {
    SDK,
    Bundle,
    RTM,
    RTM_Consistent_Error,
    Request,
    ServerError,
    Loading,
    Maintenance,
    Other,
    Job,
    LoadAsset,
    ConfigParse,
    CN37_login,
    CheckSessionError,
    Logout_Start,
    Logout_Fail,
    Logout_Finish,
  }

  private class BIInfo
  {
    public string eventName;
    public string content;
  }
}
