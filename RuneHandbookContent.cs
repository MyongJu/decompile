﻿// Decompiled with JetBrains decompiler
// Type: RuneHandbookContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using GemConstant;
using System;
using System.Collections.Generic;
using UnityEngine;

public class RuneHandbookContent : MonoBehaviour
{
  private List<ConfigEquipmentGemInfo> fullLevelDataList = new List<ConfigEquipmentGemInfo>();
  private List<GemHandbookComponent> componentList = new List<GemHandbookComponent>();
  public UIScrollView scrollView;
  public UIGrid grid;
  public GameObject gemTemplete;
  public GemDetailedInfoRender gemDetailedInfoRender;
  private SlotType slotType;
  private List<ConfigEquipmentGemInfo> initDataList;
  private GemHandbookComponent current;

  public void Show(SlotType slotType)
  {
    this.gameObject.SetActive(true);
    this.slotType = slotType;
    this.InitData();
    this.UpdateUI();
  }

  private void InitData()
  {
    this.ClearData();
    this.initDataList = ConfigManager.inst.DB_EquipmentGem.GetDatasBySlotType(this.slotType);
    for (int index = 0; index < this.initDataList.Count; ++index)
    {
      if (GemManager.Instance.IsGemMaxLevel(this.initDataList[index].internalId) && this.initDataList[index].gemType != 0 && this.initDataList[index].handbookId != 0)
        this.fullLevelDataList.Add(this.initDataList[index]);
    }
    this.fullLevelDataList.Sort(new Comparison<ConfigEquipmentGemInfo>(this.Compare));
  }

  private void UpdateUI()
  {
    for (int index = 0; index < this.fullLevelDataList.Count; ++index)
    {
      GameObject go = NGUITools.AddChild(this.grid.gameObject, this.gemTemplete.gameObject);
      NGUITools.SetActive(go, true);
      GemHandbookComponent component = go.GetComponent<GemHandbookComponent>();
      component.OnSelectedHandler += new System.Action<GemHandbookComponent>(this.OnSelectedHandler);
      component.FeedData(this.fullLevelDataList[index]);
      this.componentList.Add(component);
    }
    this.current = this.componentList[0];
    this.OnSelectedHandler(this.current);
  }

  private void OnSelectedHandler(GemHandbookComponent selected)
  {
    if (this.current.gemConfigInfo.ID != selected.gemConfigInfo.ID)
    {
      this.current.Select = false;
      this.current = selected;
      this.current.Select = true;
    }
    else
      this.current.Select = true;
    this.gemDetailedInfoRender.gameObject.SetActive(true);
    this.gemDetailedInfoRender.Show(this.current);
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
    this.ClearData();
  }

  private int Compare(ConfigEquipmentGemInfo a, ConfigEquipmentGemInfo b)
  {
    EquipmentGemHandbookInfo equipmentGemHandbookInfo1 = ConfigManager.inst.DB_GemHandbook.Get(a.handbookId);
    if (equipmentGemHandbookInfo1 == null)
      D.error((object) string.Format("Can't get EquipmentGemHandbookInfo with id: {0}", (object) a.handbookId));
    EquipmentGemHandbookInfo equipmentGemHandbookInfo2 = ConfigManager.inst.DB_GemHandbook.Get(b.handbookId);
    if (equipmentGemHandbookInfo2 == null)
      D.error((object) string.Format("Can't get EquipmentGemHandbookInfo with id: {0}", (object) b.handbookId));
    return Math.Sign(equipmentGemHandbookInfo1.priority - equipmentGemHandbookInfo2.priority);
  }

  private void ClearData()
  {
    for (int index = 0; index < this.componentList.Count; ++index)
    {
      this.componentList[index].OnSelectedHandler -= new System.Action<GemHandbookComponent>(this.OnSelectedHandler);
      this.componentList[index].gameObject.SetActive(false);
      this.componentList[index].gameObject.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.componentList[index].gameObject);
    }
    this.componentList.Clear();
    for (int index = 0; index < this.fullLevelDataList.Count; ++index)
      this.fullLevelDataList.Clear();
    if (this.initDataList == null)
      return;
    this.initDataList.Clear();
    this.initDataList = (List<ConfigEquipmentGemInfo>) null;
  }
}
