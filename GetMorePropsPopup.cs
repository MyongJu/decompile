﻿// Decompiled with JetBrains decompiler
// Type: GetMorePropsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class GetMorePropsPopup : Popup
{
  [SerializeField]
  private UILabel panelTitle;
  [SerializeField]
  private UILabel panelContent;
  [SerializeField]
  private ItemIconRenderer itemIconRenderer;
  private int _itemId;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    GetMorePropsPopup.Parameter parameter = orgParam as GetMorePropsPopup.Parameter;
    if (parameter != null)
      this._itemId = parameter.itemId;
    this.UpdateUI();
  }

  public void OnCloseButtonClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnGetMoreButtonClicked()
  {
    this.OnCloseButtonClicked();
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  private void UpdateUI()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._itemId);
    if (itemStaticInfo != null)
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      para.Add("0", itemStaticInfo.LocName);
      this.panelTitle.text = ScriptLocalization.GetWithPara("tavern_lucky_archer_not_enough_coins_title", para, true);
      this.panelContent.text = ScriptLocalization.GetWithPara("tavern_lucky_archer_not_enough_coins_description", para, true);
    }
    this.itemIconRenderer.SetData(this._itemId, string.Empty, false);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int itemId;
  }
}
