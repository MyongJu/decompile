﻿// Decompiled with JetBrains decompiler
// Type: AutoResizeGridWidget
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AutoResizeGridWidget : UIWidget
{
  private UIGrid boundGrid;

  private new void OnEnable()
  {
    this.boundGrid = this.GetComponentInChildren<UIGrid>();
    if (!((Object) this.boundGrid != (Object) null))
      return;
    this.boundGrid.onReposition += new UIGrid.OnReposition(this.CalcSize);
    this.CalcSize();
  }

  private new void OnDisable()
  {
    if (!((Object) this.boundGrid != (Object) null))
      return;
    this.boundGrid.onReposition -= new UIGrid.OnReposition(this.CalcSize);
  }

  private void CalcSize()
  {
    float x = this.boundGrid.transform.localScale.x;
    float y = this.boundGrid.transform.localScale.y;
    this.width = (int) ((double) x * (double) this.boundGrid.cellWidth * (double) this.boundGrid.maxPerLine);
    int num = Mathf.CeilToInt((float) this.boundGrid.GetChildList().Count / (float) this.boundGrid.maxPerLine);
    this.height = (int) ((double) y * (double) this.boundGrid.cellHeight * (double) num);
  }
}
