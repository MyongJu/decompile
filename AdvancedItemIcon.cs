﻿// Decompiled with JetBrains decompiler
// Type: AdvancedItemIcon
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AdvancedItemIcon : MonoBehaviour
{
  public UITexture layer1;
  public UITexture layer2;
  public UITexture layer3;

  public void SetItemData(string itemid, System.Action<bool> callBack = null, bool needtempicon = true, bool useWidgetSetting = false)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemid);
    BuilderFactory.Instance.Build((UIWidget) this.layer1, "Texture/ItemIcons/" + itemStaticInfo.Image, callBack, needtempicon, useWidgetSetting, false, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.layer2, "Texture/ItemIcons/" + itemStaticInfo.image_central, (System.Action<bool>) null, false, useWidgetSetting, false, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.layer3, "Texture/ItemIcons/" + itemStaticInfo.image_chip, (System.Action<bool>) null, false, useWidgetSetting, false, string.Empty);
    this.Adjust(itemid);
  }

  private void Adjust(string itemid)
  {
    UITexture component = this.transform.parent.GetComponent<UITexture>();
    this.layer1.width = component.width;
    this.layer2.width = component.width;
    this.layer3.width = component.width;
    this.layer1.height = component.height;
    this.layer2.height = component.height;
    this.layer3.height = component.height;
    this.layer1.depth = component.depth + 1;
    this.layer2.depth = component.depth + 2;
    this.layer3.depth = component.depth + 3;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemid);
    this.layer1.width = (int) ((double) this.layer1.width * (double) itemStaticInfo.base_image_scaling);
    this.layer1.height = (int) ((double) this.layer1.height * (double) itemStaticInfo.base_image_scaling);
    this.layer2.width = (int) ((double) this.layer2.width * (double) itemStaticInfo.image_central_scaling);
    this.layer2.height = (int) ((double) this.layer2.height * (double) itemStaticInfo.image_central_scaling);
  }

  public void Release()
  {
    BuilderFactory.Instance.Release((UIWidget) this.layer1);
    BuilderFactory.Instance.Release((UIWidget) this.layer2);
    BuilderFactory.Instance.Release((UIWidget) this.layer3);
  }
}
