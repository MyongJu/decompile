﻿// Decompiled with JetBrains decompiler
// Type: ConfigGift_Type
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigGift_Type
{
  public Dictionary<int, Gift_TypeInfo> datas;
  private Dictionary<string, int> dicByUniqueId;

  public ConfigGift_Type()
  {
    this.datas = new Dictionary<int, Gift_TypeInfo>();
    this.dicByUniqueId = new Dictionary<string, int>();
  }

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exsit", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "iap");
        int index3 = ConfigManager.GetIndex(inHeader, "gift_points");
        int index4 = ConfigManager.GetIndex(inHeader, "image");
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          Gift_TypeInfo data = new Gift_TypeInfo();
          if (int.TryParse(key.ToString(), out data.internalId))
          {
            ArrayList arr = hashtable[key] as ArrayList;
            if (arr != null)
            {
              data.ID = ConfigManager.GetString(arr, index1);
              data.IAP_ID = ConfigManager.GetString(arr, index2);
              data.Gift_Points = ConfigManager.GetInt(arr, index3);
              data.Icon = ConfigManager.GetString(arr, index4);
              this.PushData(data.internalId, data.ID, data);
            }
          }
        }
      }
    }
  }

  public Dictionary<int, Gift_TypeInfo>.ValueCollection Values
  {
    get
    {
      return this.datas.Values;
    }
  }

  private void PushData(int internalId, string uniqueId, Gift_TypeInfo data)
  {
    if (this.datas.ContainsKey(internalId))
      return;
    this.datas.Add(internalId, data);
    if (!this.dicByUniqueId.ContainsKey(uniqueId))
      this.dicByUniqueId.Add(uniqueId, internalId);
    ConfigManager.inst.AddData(internalId, (object) data);
  }

  public void Clear()
  {
    if (this.datas != null)
      this.datas.Clear();
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
  }

  public Gift_TypeInfo GetData(int internalId)
  {
    if (this.datas.ContainsKey(internalId))
      return this.datas[internalId];
    return (Gift_TypeInfo) null;
  }

  public Gift_TypeInfo GetData(string uniqueId)
  {
    if (!this.dicByUniqueId.ContainsKey(uniqueId))
      return (Gift_TypeInfo) null;
    return this.datas[this.dicByUniqueId[uniqueId]];
  }
}
