﻿// Decompiled with JetBrains decompiler
// Type: IconGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class IconGroup : MonoBehaviour
{
  private List<Icon> list = new List<Icon>();
  private GameObjectPool pools = new GameObjectPool();
  public UITable table;
  public Icon iconPrefab;
  public Icon[] icons;
  public System.Action OnRepositionComplete;
  private bool init;
  private bool isDestroy;

  public List<Icon> TotalIcons
  {
    get
    {
      return this.list;
    }
  }

  public void Clear()
  {
    for (int index = 0; index < this.list.Count; ++index)
    {
      this.list[index].Dispose();
      this.pools.Release(this.list[index].gameObject);
    }
    if ((UnityEngine.Object) this.table != (UnityEngine.Object) null)
    {
      this.table.repositionNow = true;
      this.table.Reposition();
      this.table.onReposition -= new UITable.OnReposition(this.OnReposition);
    }
    this.list.Clear();
  }

  private void OnReposition()
  {
    if (this.OnRepositionComplete == null)
      return;
    this.OnRepositionComplete();
  }

  public void Dispose()
  {
    this.Clear();
    this.pools.Clear();
    this.OnRepositionComplete = (System.Action) null;
  }

  public void CreateIcon(List<IconData> datas)
  {
    this.Clear();
    for (int index = 0; index < datas.Count; ++index)
    {
      IconData data = datas[index];
      Icon icon = this.GetIcon(index);
      icon.FeedData((IComponentData) data);
      NGUITools.SetActive(icon.gameObject, true);
      this.list.Add(icon);
    }
    if (this.icons.Length > datas.Count)
    {
      for (int count = datas.Count; count < this.icons.Length; ++count)
        NGUITools.SetActive(this.icons[count].gameObject, false);
    }
    if ((UnityEngine.Object) this.table != (UnityEngine.Object) null)
    {
      this.table.onReposition += new UITable.OnReposition(this.OnReposition);
      this.table.repositionNow = true;
    }
    Utils.ExecuteInSecs(1f / 1000f, (System.Action) (() =>
    {
      if (this.isDestroy || !((UnityEngine.Object) this.table != (UnityEngine.Object) null) || !((UnityEngine.Object) this.table.gameObject != (UnityEngine.Object) null))
        return;
      this.table.Reposition();
    }));
  }

  private void OnDestroy()
  {
    this.isDestroy = true;
  }

  private Icon GetIcon(int index)
  {
    if (this.icons.Length > index)
      return this.icons[index];
    if (!this.init)
    {
      this.init = true;
      this.pools.Initialize(this.iconPrefab.gameObject, this.gameObject);
    }
    GameObject go = this.pools.AddChild(this.table.gameObject);
    NGUITools.SetActive(go, true);
    return go.GetComponent<Icon>();
  }
}
