﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightSelectionAction
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DragonKnightSelectionAction : StateMachineBehaviour
{
  public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    if (!(bool) ((Object) animator))
      return;
    if (DragonKnightDraggable.Dragging)
    {
      animator.CrossFadeInFixedTime("Idle", 0.01f);
    }
    else
    {
      float num = Random.Range(0.0f, 100f);
      if ((double) num < 5.0)
        animator.CrossFadeInFixedTime("Attack", 0.01f);
      else if ((double) num < 10.0)
        animator.CrossFadeInFixedTime("Provoke", 0.01f);
      else if ((double) num < 15.0)
        animator.CrossFadeInFixedTime("LookAround", 0.01f);
      else
        animator.CrossFadeInFixedTime("Idle", 0.01f);
    }
  }
}
