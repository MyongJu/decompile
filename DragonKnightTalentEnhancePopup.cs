﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightTalentEnhancePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonKnightTalentEnhancePopup : Popup
{
  private List<GameObject> pool = new List<GameObject>();
  public float gapX = 100f;
  private const int processMaxLength = 1265;
  public GameObject lockpart;
  public GameObject lvluppart;
  public GameObject fulllvlpart;
  public UILabel title;
  public UILabel des;
  public DragonKnightTalentSlot talentSlot;
  public GameObject tspos;
  public UILabel curLevel;
  public UILabel nextLevel;
  public UILabel curBenefit;
  public UILabel incBenefit;
  public UISprite curBene;
  public UISprite incBene;
  public GameObject addPointBtn;
  public GameObject addMaxPointBtn;
  public UILabel fullLevel;
  public UILabel fullBenefit;
  public GameObject tspivot;
  private DragonKnightTalentEnhancePopup.DLGType curType;
  private int curTalentInternalId;
  private int curtalentTreeId;
  private string nextLevelInternalId;
  public System.Action<int, int> onTalentLvlUpFull;

  public void OnCloseBtnPressed()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  private void Init()
  {
    try
    {
      ConfigDragonKnightTalent dragonKnightTalent = ConfigManager.inst.DB_DragonKnightTalent;
      bool flag = true;
      Requirements requirementsByTalentTreeId = dragonKnightTalent.GetRequirementsByTalentTreeId(this.curtalentTreeId);
      if (requirementsByTalentTreeId == null)
        return;
      if (requirementsByTalentTreeId.requires.Count == 0)
        flag = false;
      using (Dictionary<string, int>.Enumerator enumerator = requirementsByTalentTreeId.requires.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          if (DBManager.inst.DB_DragonKnightTalent.IsTalentOwned(dragonKnightTalent.GetDragonKnightTalentInfo(int.Parse(current.Key)).internalId))
            flag = false;
        }
      }
      DragonKnightTalentInfo talentByTalentTreeId1 = dragonKnightTalent.GetCurrentLevelTalentByTalentTreeId(this.curtalentTreeId);
      DragonKnightTalentInfo talentByTalentTreeId2 = dragonKnightTalent.GetMaxLevelTalentByTalentTreeId(this.curtalentTreeId);
      DragonKnightTalentInfo knightTalentInfo1 = talentByTalentTreeId1;
      if (talentByTalentTreeId1 == null)
        knightTalentInfo1 = talentByTalentTreeId2;
      this.curTalentInternalId = knightTalentInfo1.internalId;
      this.curType = !flag ? (talentByTalentTreeId1 == null || talentByTalentTreeId1.level != talentByTalentTreeId2.level ? DragonKnightTalentEnhancePopup.DLGType.LVLUP : DragonKnightTalentEnhancePopup.DLGType.FULLLVL) : DragonKnightTalentEnhancePopup.DLGType.LOCK;
      this.title.text = talentByTalentTreeId2.TitleName;
      Dictionary<string, string> para = new Dictionary<string, string>();
      para.Add("1", (talentByTalentTreeId2.activeValue1 * 100f).ToString());
      para.Add("2", talentByTalentTreeId2.activeValue2.ToString());
      para.Add("3", (talentByTalentTreeId2.activeValue3 * 100f).ToString());
      para.Add("4", talentByTalentTreeId2.activeBuffRound.ToString());
      if (talentByTalentTreeId2.benefit != null && talentByTalentTreeId2.benefit.benefits != null && talentByTalentTreeId2.benefit.benefits.Count > 0)
      {
        Dictionary<string, float>.ValueCollection.Enumerator enumerator = talentByTalentTreeId2.benefit.benefits.Values.GetEnumerator();
        if (enumerator.MoveNext())
          para.Add("benefit_value_1", string.Format("{0}%", (object) (float) ((double) (int) ((double) Mathf.Abs(enumerator.Current) * 1000.0) / 10.0)));
      }
      this.des.text = ScriptLocalization.GetWithPara(talentByTalentTreeId2.Description, para, true);
      GameObject gameObject1 = Utils.DuplicateGOB(this.talentSlot.gameObject, this.transform);
      DragonKnightTalentSlot component1 = gameObject1.GetComponent<DragonKnightTalentSlot>();
      gameObject1.transform.localPosition = this.tspos.transform.localPosition;
      component1.talentTreeId = this.curtalentTreeId;
      component1.ToggleBoxcollider(false);
      component1.hideTitle = true;
      this.pool.Add(gameObject1);
      this.lockpart.SetActive(false);
      this.lvluppart.SetActive(false);
      this.fulllvlpart.SetActive(false);
      if (this.curType == DragonKnightTalentEnhancePopup.DLGType.LOCK)
      {
        this.lockpart.SetActive(true);
        Dictionary<string, int> requires = requirementsByTalentTreeId.requires;
        List<Vector3> tarpos = new List<Vector3>();
        Utils.ArrangePosition(this.tspivot.transform.localPosition, requires.Count, this.gapX, 0.0f, ref tarpos);
        int index = 0;
        using (Dictionary<string, int>.Enumerator enumerator = requires.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, int> current = enumerator.Current;
            GameObject gameObject2 = Utils.DuplicateGOB(gameObject1.gameObject, this.tspivot.transform.parent);
            gameObject2.transform.localPosition = tarpos[index];
            DragonKnightTalentInfo knightTalentInfo2 = dragonKnightTalent.GetDragonKnightTalentInfo(int.Parse(current.Key));
            DragonKnightTalentSlot component2 = gameObject2.GetComponent<DragonKnightTalentSlot>();
            component2.talentTreeId = knightTalentInfo2.talentTreeInternalId;
            component2.ToggleBoxcollider(false);
            component2.hideTitle = true;
            component2.panel.talentLevel.text = ScriptLocalization.Get("id_lv", true) + (object) knightTalentInfo2.level;
            this.pool.Add(gameObject2);
            ++index;
          }
        }
      }
      else if (this.curType == DragonKnightTalentEnhancePopup.DLGType.LVLUP)
      {
        this.lvluppart.SetActive(true);
        string id = knightTalentInfo1.ID;
        int num = 0;
        if (talentByTalentTreeId1 != null)
          num = dragonKnightTalent.GetDragonKnightTalentInfo(id).level;
        this.curLevel.text = ScriptLocalization.Get("id_lv", true) + (object) num;
        this.nextLevel.text = ScriptLocalization.Get("id_lv", true) + (num + 1).ToString();
        string s = "0";
        this.nextLevelInternalId = ConfigManager.inst.DB_DragonKnightTalentTree.GetTalentTreeInfo(knightTalentInfo1.talentTreeInternalId).ID + "_" + (num + 1).ToString();
        PropertyDefinition.FormatType benefitTypeByStringId;
        if (talentByTalentTreeId1 != null)
        {
          s = dragonKnightTalent.GetBenefitByStringId(talentByTalentTreeId1.ID);
          benefitTypeByStringId = dragonKnightTalent.GetBenefitTypeByStringId(talentByTalentTreeId1.ID);
        }
        else
          benefitTypeByStringId = dragonKnightTalent.GetBenefitTypeByStringId(this.nextLevelInternalId);
        switch (benefitTypeByStringId)
        {
          case PropertyDefinition.FormatType.Integer:
            this.curBenefit.text = s;
            break;
          case PropertyDefinition.FormatType.Percent:
            this.curBenefit.text = ((double) float.Parse(s) * 100.0).ToString() + "%";
            break;
        }
        string benefitByStringId1 = dragonKnightTalent.GetIncreaseBenefitByStringId(this.nextLevelInternalId);
        if (benefitTypeByStringId == PropertyDefinition.FormatType.Percent)
          this.incBenefit.text = "+" + (object) (float) ((double) float.Parse(benefitByStringId1) * 100.0) + "%";
        else if (benefitTypeByStringId == PropertyDefinition.FormatType.Integer)
          this.incBenefit.text = "+" + benefitByStringId1;
        string benefitByStringId2 = dragonKnightTalent.GetBenefitByStringId(talentByTalentTreeId2.ID);
        this.curBene.width = (int) ((double) float.Parse(s) / (double) float.Parse(benefitByStringId2) * 1265.0);
        this.incBene.width = (int) (((double) float.Parse(benefitByStringId1) + (double) float.Parse(s)) / (double) float.Parse(benefitByStringId2) * 1265.0);
        if (DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L).TalentPoint < 1L)
        {
          this.addPointBtn.GetComponent<UIButton>().isEnabled = false;
          this.addMaxPointBtn.GetComponent<UIButton>().isEnabled = false;
        }
        else
        {
          this.addPointBtn.GetComponent<UIButton>().isEnabled = true;
          this.addMaxPointBtn.GetComponent<UIButton>().isEnabled = true;
        }
        if (talentByTalentTreeId2.type == 0)
        {
          this.curBenefit.gameObject.SetActive(true);
          this.incBenefit.gameObject.SetActive(true);
        }
        else
        {
          this.curBenefit.gameObject.SetActive(false);
          this.incBenefit.gameObject.SetActive(false);
        }
      }
      else
      {
        if (this.curType != DragonKnightTalentEnhancePopup.DLGType.FULLLVL)
          return;
        this.fulllvlpart.SetActive(true);
        this.fullLevel.text = ScriptLocalization.Get("id_lv", true) + (object) talentByTalentTreeId2.level;
        string benefitByStringId = dragonKnightTalent.GetBenefitByStringId(talentByTalentTreeId2.ID);
        switch (dragonKnightTalent.GetBenefitTypeByStringId(talentByTalentTreeId1.ID))
        {
          case PropertyDefinition.FormatType.Integer:
            this.fullBenefit.text = benefitByStringId;
            break;
          case PropertyDefinition.FormatType.Percent:
            this.fullBenefit.text = ((double) float.Parse(benefitByStringId) * 100.0).ToString() + "%";
            break;
        }
        if (talentByTalentTreeId2.type == 0)
          this.fullBenefit.gameObject.SetActive(true);
        else
          this.fullBenefit.gameObject.SetActive(false);
      }
    }
    catch
    {
    }
  }

  public void OnSpendPoint()
  {
    List<int> intList = new List<int>();
    DragonKnightTalentInfo knightTalentInfo = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(this.nextLevelInternalId);
    intList.Add(knightTalentInfo.internalId);
    MessageHub.inst.GetPortByAction("DragonKnight:activeTalent").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "talent_list",
        (object) intList.ToArray()
      }
    }, new System.Action<bool, object>(this.ActiveSkillCallBack), true);
    this.addPointBtn.GetComponent<UIButton>().isEnabled = false;
    this.addMaxPointBtn.GetComponent<UIButton>().isEnabled = false;
    DragonKnightTalentInfo talentByTalentTreeId = ConfigManager.inst.DB_DragonKnightTalent.GetMaxLevelTalentByTalentTreeId(this.curtalentTreeId);
    if (knightTalentInfo == null || knightTalentInfo.level != talentByTalentTreeId.level || this.onTalentLvlUpFull == null)
      return;
    DragonKnightTalentTreeInfo talentTreeInfo = ConfigManager.inst.DB_DragonKnightTalentTree.GetTalentTreeInfo(talentByTalentTreeId.talentTreeInternalId);
    this.onTalentLvlUpFull(talentByTalentTreeId.internalId, talentTreeInfo.tree);
  }

  public void OnMaxSpendPoint()
  {
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (dragonKnightData == null)
      return;
    long talentPoint = dragonKnightData.TalentPoint;
    ConfigDragonKnightTalent dragonKnightTalent = ConfigManager.inst.DB_DragonKnightTalent;
    DragonKnightTalentInfo talentByTalentTreeId1 = dragonKnightTalent.GetCurrentLevelTalentByTalentTreeId(this.curtalentTreeId);
    DragonKnightTalentInfo talentByTalentTreeId2 = dragonKnightTalent.GetMaxLevelTalentByTalentTreeId(this.curtalentTreeId);
    int num = 0;
    if (talentByTalentTreeId1 != null)
      num = dragonKnightTalent.GetDragonKnightTalentInfo(talentByTalentTreeId1.ID).level;
    int level = talentByTalentTreeId2.level;
    List<int> intList = new List<int>();
    DragonKnightTalentInfo knightTalentInfo = talentByTalentTreeId1;
    for (; talentPoint > 0L && num != level; ++num)
    {
      this.nextLevelInternalId = ConfigManager.inst.DB_DragonKnightTalentTree.GetTalentTreeInfo(talentByTalentTreeId2.talentTreeInternalId).ID + "_" + (num + 1).ToString();
      knightTalentInfo = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(this.nextLevelInternalId);
      if (knightTalentInfo == null)
        return;
      intList.Add(knightTalentInfo.internalId);
      --talentPoint;
    }
    MessageHub.inst.GetPortByAction("DragonKnight:activeTalent").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "talent_list",
        (object) intList.ToArray()
      }
    }, new System.Action<bool, object>(this.ActiveSkillCallBack), true);
    this.addPointBtn.GetComponent<UIButton>().isEnabled = false;
    this.addMaxPointBtn.GetComponent<UIButton>().isEnabled = false;
    if (knightTalentInfo == null || knightTalentInfo.level != talentByTalentTreeId2.level || this.onTalentLvlUpFull == null)
      return;
    DragonKnightTalentTreeInfo talentTreeInfo = ConfigManager.inst.DB_DragonKnightTalentTree.GetTalentTreeInfo(talentByTalentTreeId2.talentTreeInternalId);
    this.onTalentLvlUpFull(talentByTalentTreeId2.internalId, talentTreeInfo.tree);
  }

  private void ActiveSkillCallBack(bool arg1, object arg2)
  {
    if (!arg1 || (UnityEngine.Object) null == (UnityEngine.Object) this)
      return;
    ConfigDragonKnightTalent dragonKnightTalent = ConfigManager.inst.DB_DragonKnightTalent;
    DragonKnightTalentInfo talentByTalentTreeId1 = dragonKnightTalent.GetCurrentLevelTalentByTalentTreeId(this.curtalentTreeId);
    DragonKnightTalentInfo talentByTalentTreeId2 = dragonKnightTalent.GetMaxLevelTalentByTalentTreeId(this.curtalentTreeId);
    if (talentByTalentTreeId1 != null && talentByTalentTreeId1.level == talentByTalentTreeId2.level || talentByTalentTreeId1 == null)
    {
      UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
    }
    else
    {
      this.Reset();
      this.Init();
    }
  }

  private void Reset()
  {
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.pool.Clear();
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.curtalentTreeId = (orgParam as DragonKnightTalentEnhancePopup.Parameter).talentTreeId;
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  public enum DLGType
  {
    LOCK,
    LVLUP,
    FULLLVL,
  }

  public class Parameter : Popup.PopupParameter
  {
    public int talentTreeId;
  }
}
