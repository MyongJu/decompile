﻿// Decompiled with JetBrains decompiler
// Type: DiceRewardSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class DiceRewardSlot : MonoBehaviour
{
  [SerializeField]
  private ItemIconRenderer _itemIconRenderer;
  [SerializeField]
  private UILabel _labelRewardCount;
  [SerializeField]
  private UILabel _labelRewardScore;
  private DiceData.RewardData _diceRewardData;

  public void SetData(DiceData.RewardData diceRewardData)
  {
    this._diceRewardData = diceRewardData;
    this.UpdateUI();
  }

  public void OnItemPress()
  {
    if (this._diceRewardData == null)
      return;
    if (this._diceRewardData.IsRewardScore)
      Utils.DelayShowCustomTip(new ItemTipCustomInfo.ItemTipData(this._diceRewardData.Amount.ToString(), this.GetScore(), string.Empty, string.Empty, "Texture/ItemIcons/dice_score_frame"), this.transform);
    else
      Utils.DelayShowTip(this._diceRewardData.ItemId, this.transform, 0L, 0L, 0);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  public void OnItemClick()
  {
    if (this._diceRewardData == null)
      return;
    if (this._diceRewardData.IsRewardScore)
      Utils.ShowCustomItemTip(new ItemTipCustomInfo.ItemTipData(this._diceRewardData.Amount.ToString(), this.GetScore(), string.Empty, string.Empty, "Texture/ItemIcons/dice_score_frame"), this.transform);
    else
      Utils.ShowItemTip(this._diceRewardData.ItemId, this.transform, 0L, 0L, 0);
  }

  private string GetScore()
  {
    if (this._diceRewardData == null || !this._diceRewardData.IsRewardScore)
      return string.Empty;
    return ScriptLocalization.GetWithPara("id_points_num", new Dictionary<string, string>()
    {
      {
        "0",
        this._diceRewardData.Amount.ToString()
      }
    }, true);
  }

  private void UpdateUI()
  {
    if (this._diceRewardData == null)
      return;
    if (this._diceRewardData.IsRewardScore)
    {
      this._labelRewardScore.text = this._diceRewardData.Amount.ToString();
    }
    else
    {
      this._labelRewardScore.text = string.Empty;
      this._labelRewardCount.text = this._diceRewardData.Amount.ToString();
    }
    NGUITools.SetActive(this._labelRewardCount.transform.parent.gameObject, !this._diceRewardData.IsRewardScore);
    if (this._diceRewardData.ItemId <= 0)
      return;
    this._itemIconRenderer.SetData(this._diceRewardData.ItemId, string.Empty, true);
  }
}
