﻿// Decompiled with JetBrains decompiler
// Type: KingdomBuffHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UI;
using UnityEngine;

public class KingdomBuffHUD : MonoBehaviour
{
  private string _vfxName = string.Empty;
  private const string VFX_PATH = "Prefab/VFX/";
  private const string VFX_USED_NAME = "fx_kingdom_buff";
  public GameObject rootNode;
  public UITexture buffTexture;
  public UILabel buffFinishedTime;
  private GameObject _vfxObj;
  private int _curTextureIndex;
  private bool _buffTextureSet;
  private KingdomBuffPayload.KingdomBuffData _buffData;

  private void OnEnable()
  {
    Oscillator.Instance.threeSecondEvent -= new System.Action(this.OnThreeSecondEvent);
    Oscillator.Instance.threeSecondEvent += new System.Action(this.OnThreeSecondEvent);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
    this.UpdateUI();
  }

  private void OnDestroy()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.threeSecondEvent -= new System.Action(this.OnThreeSecondEvent);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  private void OnThreeSecondEvent()
  {
    ++this._curTextureIndex;
    this.UpdateUI();
  }

  private void OnSecondEvent(int time)
  {
    NGUITools.SetActive(this.rootNode, KingdomBuffPayload.Instance.HasBuff && UIManager.inst.dialogStackCount <= 0);
    if (!KingdomBuffPayload.Instance.HasBuff)
      this.DeleteVFX();
    if (this._buffData == null)
      return;
    this.buffFinishedTime.text = Utils.FormatTime(this._buffData.finishedTime - NetServerTime.inst.ServerTimestamp, true, false, true);
  }

  private void UpdateUI()
  {
    if (this._curTextureIndex >= KingdomBuffPayload.Instance.KingdomBuffDataList.Count)
      this._curTextureIndex = 0;
    if (this._curTextureIndex >= KingdomBuffPayload.Instance.KingdomBuffDataList.Count)
      return;
    lock (KingdomBuffPayload.Instance.KingdomBuffDataList)
    {
      if (!this.rootNode.activeInHierarchy)
        return;
      this.StartCoroutine(this.FadeBuffTexture(KingdomBuffPayload.Instance.KingdomBuffDataList[this._curTextureIndex]));
    }
  }

  [DebuggerHidden]
  private IEnumerator FadeBuffTexture(KingdomBuffPayload.KingdomBuffData buffData)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KingdomBuffHUD.\u003CFadeBuffTexture\u003Ec__Iterator78()
    {
      buffData = buffData,
      \u003C\u0024\u003EbuffData = buffData,
      \u003C\u003Ef__this = this
    };
  }

  private void SetVFX()
  {
    if (!(this._vfxName != "fx_kingdom_buff"))
      return;
    this.DeleteVFX();
    this._vfxName = "fx_kingdom_buff";
    this._vfxObj = AssetManager.Instance.HandyLoad("Prefab/VFX/" + this._vfxName, typeof (GameObject)) as GameObject;
    if (!((UnityEngine.Object) this._vfxObj != (UnityEngine.Object) null))
      return;
    this._vfxObj = UnityEngine.Object.Instantiate<GameObject>(this._vfxObj);
    this._vfxObj.transform.parent = this.rootNode.transform;
    this._vfxObj.transform.localPosition = Vector3.zero;
    this._vfxObj.transform.localScale = Vector3.one;
  }

  private void DeleteVFX()
  {
    this._vfxName = string.Empty;
    if (!((UnityEngine.Object) this._vfxObj != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this._vfxObj);
    this._vfxObj = (GameObject) null;
  }

  public void OnBuffTextureClicked()
  {
    KingdomBuffPayload.Instance.RequestServerData((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenPopup("KingdomBuff/KingdomBuffPopup", (Popup.PopupParameter) null);
    }));
    OperationTrace.TraceClick(ClickArea.KingdomBuff);
  }
}
