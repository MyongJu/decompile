﻿// Decompiled with JetBrains decompiler
// Type: ConfigAllianceShop
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAllianceShop
{
  private ConfigParse m_Parser = new ConfigParse();
  private Dictionary<int, AllianceShopInfo> m_AllianceShopInfoDictByInternalId;
  private Dictionary<string, AllianceShopInfo> m_AllianceShopInfoDictByUniqueId;
  private List<AllianceShopInfo> m_AllianceShopInfoList;

  public void BuildDB(object result)
  {
    this.m_Parser.Parse<AllianceShopInfo, string>(result as Hashtable, "ID", out this.m_AllianceShopInfoDictByUniqueId, out this.m_AllianceShopInfoDictByInternalId);
    this.m_AllianceShopInfoList = new List<AllianceShopInfo>();
    Dictionary<int, AllianceShopInfo>.Enumerator enumerator = this.m_AllianceShopInfoDictByInternalId.GetEnumerator();
    while (enumerator.MoveNext())
      this.m_AllianceShopInfoList.Add(enumerator.Current.Value);
  }

  public AllianceShopInfo GetByInternalId(int internalId)
  {
    AllianceShopInfo allianceShopInfo;
    this.m_AllianceShopInfoDictByInternalId.TryGetValue(internalId, out allianceShopInfo);
    return allianceShopInfo;
  }

  public AllianceShopInfo GetByUniqueId(string uniqueId)
  {
    AllianceShopInfo allianceShopInfo;
    this.m_AllianceShopInfoDictByUniqueId.TryGetValue(uniqueId, out allianceShopInfo);
    return allianceShopInfo;
  }

  public int GetAllianceShopInfoCount()
  {
    return this.m_AllianceShopInfoList.Count;
  }

  public AllianceShopInfo GetAllianceShopInfoByIndex(int index)
  {
    return this.m_AllianceShopInfoList[index];
  }
}
