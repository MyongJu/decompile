﻿// Decompiled with JetBrains decompiler
// Type: Edge
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

internal class Edge
{
  public int p1;
  public int p2;

  public Edge(int point1, int point2)
  {
    this.p1 = point1;
    this.p2 = point2;
  }

  public Edge()
    : this(0, 0)
  {
  }

  public bool Equals(Edge other)
  {
    if (this.p1 == other.p2 && this.p2 == other.p1)
      return true;
    if (this.p1 == other.p1)
      return this.p2 == other.p2;
    return false;
  }
}
