﻿// Decompiled with JetBrains decompiler
// Type: WarReportRoundEntry
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarReportRoundEntry : MonoBehaviour
{
  private List<TroopCasualtyLabel> _casualtyList = new List<TroopCasualtyLabel>();
  public UITable casualtyTable;
  public UILabel roundTitle;
  public TroopCasualtyLabel casualtyTemplate;
  public EnvelopContent envelopContentContainer;
  public System.Action<WarReportRoundEntry> OnPosition;
  private char _currentChar;

  public bool positioned { get; private set; }

  public void Start()
  {
  }

  public void SetData(Hashtable data, long playerUid, int round)
  {
    this.Clear();
    this.roundTitle.text = ScriptLocalization.Get("Round", true) + " " + round.ToString();
    IEnumerator enumerator1 = data.Keys.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      long result;
      bool yourAttack = long.TryParse(enumerator1.Current.ToString(), out result) && result == playerUid;
      Hashtable hashtable1 = data[enumerator1.Current] as Hashtable;
      if (hashtable1 != null)
      {
        IEnumerator enumerator2 = hashtable1.Keys.GetEnumerator();
        while (enumerator2.MoveNext())
        {
          TroopInfo troopInfo1 = TroopInfoManager.Instance.GetTroopInfo(enumerator2.Current.ToString());
          Hashtable hashtable2 = hashtable1[enumerator2.Current] as Hashtable;
          if (hashtable2 != null)
          {
            TroopCasualtyLabel component = NGUITools.AddChild(this.casualtyTable.gameObject, this.casualtyTemplate.gameObject).GetComponent<TroopCasualtyLabel>();
            int casualties = !hashtable2.ContainsKey((object) "kill") ? 0 : int.Parse(hashtable2[(object) "kill"].ToString());
            TroopInfo troopInfo2 = TroopInfoManager.Instance.GetTroopInfo(hashtable2[(object) "attackee"].ToString());
            component.name = this._currentChar.ToString();
            ++this._currentChar;
            component.gameObject.SetActive(true);
            component.SetData(0, troopInfo1, 0, troopInfo2, casualties, yourAttack);
            this._casualtyList.Add(component);
          }
        }
      }
    }
    this.casualtyTable.onReposition += new UITable.OnReposition(this.OnReposition);
    this.casualtyTable.repositionNow = true;
  }

  private void Clear()
  {
    this._currentChar = 'a';
    this.positioned = false;
    List<TroopCasualtyLabel>.Enumerator enumerator = this._casualtyList.GetEnumerator();
    while (enumerator.MoveNext())
      UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    this._casualtyList.Clear();
  }

  private void OnReposition()
  {
    this.casualtyTable.onReposition -= new UITable.OnReposition(this.OnReposition);
    this.casualtyTable.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
    this.envelopContentContainer.Execute();
    this.positioned = true;
    if (this.OnPosition == null)
      return;
    this.OnPosition(this);
  }
}
