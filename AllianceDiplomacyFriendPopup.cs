﻿// Decompiled with JetBrains decompiler
// Type: AllianceDiplomacyFriendPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceDiplomacyFriendPopup : MonoBehaviour
{
  public UISprite mAllianceIcon;
  public UILabel mAllianceName;
  public UILabel mContent;
  public System.Action onYes;
  public System.Action onNo;

  public void SetDetails(string icon, string name, System.Action yes, System.Action no)
  {
    this.mAllianceIcon.spriteName = icon;
    this.mAllianceName.text = name;
    this.mContent.text = this.mContent.text.Replace("...", name);
    this.onYes = yes;
    this.onNo = no;
  }

  public void OnYesBtnPressed()
  {
    if (this.onYes == null)
      return;
    this.onYes();
  }

  public void OnNoBtnPressed()
  {
    if (this.onNo == null)
      return;
    this.onNo();
  }
}
