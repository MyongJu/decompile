﻿// Decompiled with JetBrains decompiler
// Type: MerlinExchangeStoreDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinExchangeStoreDlg : UI.Dialog
{
  private Dictionary<int, MerlinExchangeSlot> _itemDict = new Dictionary<int, MerlinExchangeSlot>();
  private Dictionary<int, MerlinTrialsExchangeCatalog> _categoryDict = new Dictionary<int, MerlinTrialsExchangeCatalog>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UILabel _labelMerlinCoinCount;
  [SerializeField]
  private UITexture _textureMerlinCoin;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UITable _table;
  [SerializeField]
  private GameObject _itemPrefab;
  [SerializeField]
  private UIScrollView _categoryScrollView;
  [SerializeField]
  private UITable _categoryTable;
  [SerializeField]
  private GameObject _categoryPrefab;
  private List<string> _merlinShopItemCategories;

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
    this._merlinShopItemCategories = ConfigManager.inst.DB_MerlinTrialsShop.GetCategoryList();
    this.AddEventHandler();
    this.ShowMainContent();
    this.UpdateCategory();
    this.UpdateUI("all");
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.ClearData();
    this.RemoveEventHandler();
  }

  private void ShowMainContent()
  {
    this._labelMerlinCoinCount.text = Utils.FormatThousands(PlayerData.inst.userData.MerlinCoin.ToString());
    BuilderFactory.Instance.HandyBuild((UIWidget) this._textureMerlinCoin, MerlinTrialsPayload.Instance.MerlinCoinImagePath, (System.Action<bool>) null, true, true, string.Empty);
  }

  private void UpdateCategory()
  {
    this.ClearCategory();
    for (int key = 0; key < this._merlinShopItemCategories.Count; ++key)
    {
      MerlinTrialsExchangeCatalog category = this.GenerateCategory(this._merlinShopItemCategories[key]);
      this._categoryDict.Add(key, category);
    }
    this.RepositionCategory();
  }

  private void UpdateUI(string category = "all")
  {
    this.ClearData();
    List<MerlinTrialsShopInfo> infoList = ConfigManager.inst.DB_MerlinTrialsShop.GetInfoList();
    List<MerlinTrialsShopInfo> merlinTrialsShopInfoList = new List<MerlinTrialsShopInfo>();
    string key1 = category;
    if (key1 != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (MerlinExchangeStoreDlg.\u003C\u003Ef__switch\u0024map8C == null)
      {
        // ISSUE: reference to a compiler-generated field
        MerlinExchangeStoreDlg.\u003C\u003Ef__switch\u0024map8C = new Dictionary<string, int>(1)
        {
          {
            "all",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (MerlinExchangeStoreDlg.\u003C\u003Ef__switch\u0024map8C.TryGetValue(key1, out num) && num == 0)
      {
        merlinTrialsShopInfoList = infoList;
        goto label_10;
      }
    }
    for (int index = 0; index < infoList.Count; ++index)
    {
      if (infoList[index].category == category)
        merlinTrialsShopInfoList.Add(infoList[index]);
    }
label_10:
    merlinTrialsShopInfoList.Sort((Comparison<MerlinTrialsShopInfo>) ((a, b) => a.priority.CompareTo(b.priority)));
    for (int key2 = 0; key2 < merlinTrialsShopInfoList.Count; ++key2)
    {
      MerlinExchangeSlot slot = this.GenerateSlot(merlinTrialsShopInfoList[key2]);
      this._itemDict.Add(key2, slot);
    }
    this.Reposition();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdate);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdate);
  }

  private void OnUserDataUpdate(long userId)
  {
    if (userId == PlayerData.inst.uid)
      this.ShowMainContent();
    using (Dictionary<int, MerlinExchangeSlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.RefreshUI();
    }
  }

  private void ClearData()
  {
    using (Dictionary<int, MerlinExchangeSlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this._table.repositionNow = true;
    this._table.Reposition();
    this._scrollView.ResetPosition();
  }

  private MerlinExchangeSlot GenerateSlot(MerlinTrialsShopInfo shopInfo)
  {
    this._itemPool.Initialize(this._itemPrefab, this._table.gameObject);
    GameObject gameObject = this._itemPool.AddChild(this._table.gameObject);
    gameObject.SetActive(true);
    MerlinExchangeSlot component = gameObject.GetComponent<MerlinExchangeSlot>();
    component.SetData(shopInfo);
    return component;
  }

  private void ClearCategory()
  {
    using (Dictionary<int, MerlinTrialsExchangeCatalog>.Enumerator enumerator = this._categoryDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, MerlinTrialsExchangeCatalog> current = enumerator.Current;
        current.Value.OnCategoryClicked -= new System.Action<string>(this.OnCategoryClicked);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._categoryDict.Clear();
    this._itemPool.Clear();
  }

  private void RepositionCategory()
  {
    this._categoryTable.repositionNow = true;
    this._categoryTable.Reposition();
    this._categoryScrollView.ResetPosition();
  }

  private MerlinTrialsExchangeCatalog GenerateCategory(string category)
  {
    this._itemPool.Initialize(this._categoryPrefab, this._categoryTable.gameObject);
    GameObject gameObject = this._itemPool.AddChild(this._categoryTable.gameObject);
    gameObject.SetActive(true);
    MerlinTrialsExchangeCatalog component = gameObject.GetComponent<MerlinTrialsExchangeCatalog>();
    component.SetData(category);
    component.OnCategoryClicked += new System.Action<string>(this.OnCategoryClicked);
    return component;
  }

  private void OnCategoryClicked(string category)
  {
    this.UpdateUI(category);
  }
}
