﻿// Decompiled with JetBrains decompiler
// Type: AllianceRecommendationGoldEntryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;

public class AllianceRecommendationGoldEntryPopup : Popup
{
  private AllianceSearchItemData allianceData;
  private string entryGold;
  public UILabel m_TagAndName;
  public UILabel m_Language;
  public AllianceSymbol m_AllianceSymbol;
  private UILabel[] goldCountGroup;
  public UILabel m_GoldCount1;
  public UILabel m_GoldCount2;
  public UILabel m_Ad;

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("join_alliance_reward_gold");
    if (data != null)
      this.entryGold = data.ValueString;
    this.goldCountGroup = new UILabel[2];
    this.goldCountGroup[0] = this.m_GoldCount1;
    this.goldCountGroup[1] = this.m_GoldCount2;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    AllianceRecommendationGoldEntryPopup.Parameter parameter = orgParam as AllianceRecommendationGoldEntryPopup.Parameter;
    if (parameter == null)
    {
      this.gameObject.SetActive(false);
    }
    else
    {
      this.gameObject.SetActive(true);
      this.allianceData = parameter.allianceSearchItemData;
      Dictionary<string, string> para = new Dictionary<string, string>();
      para.Add("0", Language.Instance.GetLanguageName(this.allianceData.language));
      this.m_Language.text = ScriptLocalization.GetWithPara("id_language_lang", para, true);
      this.m_TagAndName.text = string.Format("[{0}]{1}", (object) this.allianceData.tag, (object) this.allianceData.name);
      this.m_AllianceSymbol.SetSymbols(this.allianceData.symbolCode);
      foreach (UILabel uiLabel in this.goldCountGroup)
        uiLabel.text = string.Format("+{0}", (object) this.entryGold);
      para.Clear();
      para.Add("0", this.entryGold);
      this.m_Ad.text = ScriptLocalization.GetWithPara("alliance_system_invite_popup_one_liner", para, true);
    }
  }

  public void OnJoinPressed()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) this.allianceData.allianceID;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      this.SendJoinRequest();
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    }), true);
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void SendJoinRequest()
  {
    AllianceManager.Instance.JoinAlliance(this.allianceData.allianceID, (System.Action<bool, object>) ((result, data) =>
    {
      if (!result)
        return;
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
      Dictionary<string, string> para = new Dictionary<string, string>();
      para["Alliance_Name"] = allianceData.allianceName;
      ToastManager.Instance.AddToast("BASIC_TOAST", (object) new ToastArgs(ConfigManager.inst.DB_Toast.GetData("toast_alliance_join"), para));
    }));
  }

  public class Parameter : Popup.PopupParameter
  {
    public AllianceSearchItemData allianceSearchItemData;
  }
}
