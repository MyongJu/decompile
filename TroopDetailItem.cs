﻿// Decompiled with JetBrains decompiler
// Type: TroopDetailItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TroopDetailItem : MonoBehaviour, IRecycle
{
  private List<GameObject> pool = new List<GameObject>();
  private const float parliaGapX = 124f;
  public UILabel owner;
  public GameObject soldierType;
  public SoldierInfo soldierInfo;
  public DragonSkillForWarReportComponent dragonUI;
  public GameObject legendContainer;
  public GameObject parliaitem;
  public GameObject noDragon;
  public GameObject noLegend;
  private float soldierTypeHeight;
  private float soldierInfoHeight;
  private float parliaItemWidth;
  private float myheight;
  private float parliaX;

  public void SeedData(TroopDetailItem.Data d)
  {
    this.owner.text = d.ownername;
    this.soldierTypeHeight = (float) this.soldierType.GetComponent<UIWidget>().height;
    this.soldierInfoHeight = (float) this.soldierInfo.GetComponent<UIWidget>().height;
    this.parliaItemWidth = (float) this.parliaitem.GetComponent<UIWidget>().width + 124f;
    this.myheight = d.mail == null || d.mail.type != MailType.MAIL_TYPE_DRAGON_ALTER_RESULT ? this.soldierType.transform.localPosition.y + this.soldierTypeHeight / 2f : this.owner.transform.localPosition.y;
    this.parliaX = this.parliaitem.transform.localPosition.x;
    if (d.legends != null && d.legends.Count != 0)
    {
      this.legendContainer.SetActive(d.showLegend);
      this.LayoutLegend(d.legends, d.ownername);
      this.noLegend.SetActive(false);
    }
    else
    {
      this.legendContainer.SetActive(false);
      this.noLegend.SetActive(d.showLegend);
    }
    if (!d.showLegend)
      this.myheight = this.myheight + this.soldierTypeHeight + this.soldierInfoHeight;
    this.parliaitem.gameObject.SetActive(false);
    List<SoldierInfo.Data> ss = (List<SoldierInfo.Data>) null;
    d.soldiers.TryGetValue("infantry", out ss);
    if (ss != null)
    {
      this.Layout("infantry", ss);
      ss = (List<SoldierInfo.Data>) null;
    }
    d.soldiers.TryGetValue("cavalry", out ss);
    if (ss != null)
    {
      this.Layout("cavalry", ss);
      ss = (List<SoldierInfo.Data>) null;
    }
    d.soldiers.TryGetValue("ranged", out ss);
    if (ss != null)
    {
      this.Layout("ranged", ss);
      ss = (List<SoldierInfo.Data>) null;
    }
    d.soldiers.TryGetValue("siege", out ss);
    if (ss != null)
    {
      this.Layout("siege", ss);
      ss = (List<SoldierInfo.Data>) null;
    }
    d.soldiers.TryGetValue("trap", out ss);
    if (ss != null)
      this.Layout("trap", ss);
    this.soldierType.SetActive(false);
    this.soldierInfo.gameObject.SetActive(false);
    Debug.Log((object) d.dragondata);
    if (d.dragondata != null)
      Debug.Log((object) d.dragondata.Count);
    Debug.Log((object) d.needdragon);
    if (d.needdragon)
    {
      if (d.dragondata != null && d.dragondata.Count > 0)
        NGUITools.SetActive(this.noDragon, false);
      else
        NGUITools.SetActive(this.noDragon, true);
    }
    else
      NGUITools.SetActive(this.noDragon, true);
    this.dragonUI.FeedData<DragonSkillForWarReportComponentData>(d.dragondata);
    this.AdjustWarReportDisplay(d);
  }

  private void LayoutLegend(List<Hashtable> legend, string name)
  {
    using (List<Hashtable>.Enumerator enumerator = legend.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Hashtable current = enumerator.Current;
        GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.parliaitem.gameObject, this.legendContainer.transform);
        gameObject.gameObject.SetActive(true);
        this.pool.Add(gameObject);
        Vector3 localPosition = this.parliaitem.transform.localPosition;
        localPosition.x = this.parliaX;
        gameObject.transform.localScale = this.parliaitem.transform.localScale;
        gameObject.transform.localPosition = localPosition;
        this.parliaX += this.parliaItemWidth;
        gameObject.GetComponent<ParliaInfo>().SeedData(current, name);
      }
    }
  }

  private void Layout(string type, List<SoldierInfo.Data> ss)
  {
    this.myheight -= this.soldierTypeHeight / 2f;
    GameObject gameObject1 = PrefabManagerEx.Instance.Spawn(this.soldierType.gameObject, this.soldierType.transform.parent);
    gameObject1.SetActive(true);
    this.pool.Add(gameObject1);
    Vector3 localPosition1 = this.soldierType.transform.localPosition;
    localPosition1.y = this.myheight;
    gameObject1.transform.localPosition = localPosition1;
    UILabel componentInChildren = gameObject1.GetComponentInChildren<UILabel>();
    if ((Object) null != (Object) componentInChildren)
      componentInChildren.text = ScriptLocalization.Get(type + "_name", true);
    this.myheight -= this.soldierTypeHeight / 2f;
    using (List<SoldierInfo.Data>.Enumerator enumerator = ss.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SoldierInfo.Data current = enumerator.Current;
        this.myheight -= this.soldierInfoHeight / 2f;
        GameObject gameObject2 = PrefabManagerEx.Instance.Spawn(this.soldierInfo.gameObject, this.soldierInfo.transform.parent);
        gameObject2.gameObject.SetActive(true);
        this.pool.Add(gameObject2);
        Vector3 localPosition2 = this.soldierInfo.transform.localPosition;
        localPosition2.y = this.myheight;
        gameObject2.transform.localPosition = localPosition2;
        this.myheight -= this.soldierInfoHeight / 2f;
        gameObject2.GetComponent<SoldierInfo>().SeedData(current);
      }
    }
  }

  private void AdjustWarReportDisplay(TroopDetailItem.Data d)
  {
    if (d.mail != null && d.mail.type == MailType.MAIL_TYPE_DRAGON_ALTER_RESULT)
    {
      this.ApplyForAltarReport(false);
      this.noLegend.SetActive(false);
      this.legendContainer.SetActive(false);
      NGUITools.SetActive(this.noDragon, false);
    }
    else
    {
      this.ApplyForAltarReport(true);
      if (d.legends != null && d.legends.Count != 0)
      {
        this.legendContainer.SetActive(d.showLegend);
        this.noLegend.SetActive(false);
      }
      else
      {
        this.legendContainer.SetActive(false);
        this.noLegend.SetActive(d.showLegend);
      }
      if (d.needdragon)
      {
        if (d.dragondata != null && d.dragondata.Count > 0)
          NGUITools.SetActive(this.noDragon, false);
        else
          NGUITools.SetActive(this.noDragon, true);
      }
      else
        NGUITools.SetActive(this.noDragon, true);
    }
    if (d.showLegend)
      return;
    Transform child = this.transform.FindChild("LegendTitle");
    if (!(bool) ((Object) child))
      return;
    child.gameObject.SetActive(false);
  }

  private void ApplyForAltarReport(bool enable)
  {
    this.transform.FindChild("NoLegend").gameObject.SetActive(enable);
    this.transform.FindChild("your").gameObject.SetActive(enable);
    this.transform.FindChild("PlayerNameBg").gameObject.SetActive(enable);
    this.transform.FindChild("LegendTitle").gameObject.SetActive(enable);
    this.transform.FindChild("DragonTitle").gameObject.SetActive(enable);
    this.dragonUI.gameObject.SetActive(enable);
  }

  public void OnInitialize()
  {
  }

  public void OnFinalize()
  {
    this.Reset();
  }

  public void Reset()
  {
    this.soldierType.SetActive(true);
    this.soldierInfo.gameObject.SetActive(true);
    foreach (ParliaInfo componentsInChild in this.GetComponentsInChildren<ParliaInfo>(true))
      componentsInChild.Reset();
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        PrefabManagerEx.Instance.Destroy(current);
      }
    }
    this.pool.Clear();
  }

  public class Data
  {
    public Hashtable dragondata = new Hashtable();
    public List<Hashtable> legends = new List<Hashtable>();
    public bool showLegend = true;
    public string ownername;
    public Dictionary<string, List<SoldierInfo.Data>> soldiers;
    public bool needdragon;
    public AbstractMailEntry mail;
  }
}
