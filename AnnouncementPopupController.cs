﻿// Decompiled with JetBrains decompiler
// Type: AnnouncementPopupController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;

public class AnnouncementPopupController
{
  private static AnnouncementPopupController _instance;
  public bool isFirstAnnPopupClosed;

  public static AnnouncementPopupController Instance
  {
    get
    {
      if (AnnouncementPopupController._instance == null)
        AnnouncementPopupController._instance = new AnnouncementPopupController();
      return AnnouncementPopupController._instance;
    }
  }

  private void OnRequestResponse(bool ret, object data)
  {
    if (!ret)
      return;
    List<AnnouncementData> announcementDataList = GameLoggedonAnnouncementPayload.Instance.AnnouncementDataList;
    if (announcementDataList == null || announcementDataList.Count <= 0)
      return;
    UIManager.inst.OpenPopup("LoginAnnouncement/LoginAnnouncementPopup", (Popup.PopupParameter) new LoginAnnouncementPopup.Parameter()
    {
      dataList = announcementDataList
    });
  }

  private void RequestData()
  {
    GameLoggedonAnnouncementPayload.Instance.RequestAnnouncement(new System.Action<bool, object>(this.OnRequestResponse), new Hashtable()
    {
      {
        (object) "client_platform",
        (object) NativeManager.inst.GetOS()
      },
      {
        (object) "lang",
        (object) LocalizationManager.CurrentLanguageCode
      },
      {
        (object) "client_version",
        (object) NativeManager.inst.AppMajorVersion
      }
    });
  }

  public void ShowAnnouncementPopup()
  {
    if (UIManager.inst.IsDialogClear && UIManager.inst.IsPopupClear)
      this.OnWindowClear();
    else
      UIManager.inst.OnWindowClear += new System.Action(this.OnWindowClear);
  }

  private bool isAnniversaryInOpenState()
  {
    return (long) NetServerTime.inst.ServerTimestamp >= AnniversaryIapPayload.Instance.TournamentShowTime && (long) NetServerTime.inst.ServerTimestamp < AnniversaryIapPayload.Instance.EndTime;
  }

  private void OnWindowClear()
  {
    if (!UIManager.inst.IsDialogClear || !UIManager.inst.IsPopupClear || this.isAnniversaryInOpenState() && !this.isFirstAnnPopupClosed)
      return;
    this.RequestData();
    UIManager.inst.OnWindowClear -= new System.Action(this.OnWindowClear);
  }
}
