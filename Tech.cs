﻿// Decompiled with JetBrains decompiler
// Type: Tech
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class Tech : IGameData
{
  private static readonly string NameKey = nameof (_name);
  private static readonly string DescKey = "_discription";
  private int _internalID;
  private string _id;
  private string _name;
  private string _desc;
  private string _image;
  private int _tier;
  private int _row;
  private int _tree;

  public Tech(int internalID, string ID, string image, int tree, int row, int tier)
  {
    this._internalID = internalID;
    this._id = ID;
    this._image = image;
    this._tree = tree;
    this._row = row;
    this._tier = tier;
    this._name = ScriptLocalization.Get(ID + Tech.NameKey, true);
    this._desc = ScriptLocalization.Get(ID + Tech.DescKey, true);
  }

  public int InternalID
  {
    get
    {
      return this._internalID;
    }
  }

  public string ID
  {
    get
    {
      return this._id;
    }
  }

  public string Name
  {
    get
    {
      return this._name;
    }
  }

  public string Desc
  {
    get
    {
      return this._desc;
    }
  }

  public string Image
  {
    get
    {
      return this._image;
    }
  }

  public string IconPath
  {
    get
    {
      return "Texture/ResearchIcons/" + this.Image;
    }
  }

  public int tree
  {
    get
    {
      return this._tree;
    }
  }

  public int Row
  {
    get
    {
      return this._row;
    }
  }

  public int Tier
  {
    get
    {
      return this._tier;
    }
  }
}
