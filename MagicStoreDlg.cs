﻿// Decompiled with JetBrains decompiler
// Type: MagicStoreDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class MagicStoreDlg : UI.Dialog
{
  private float gap = 0.5f;
  private int NextRefreshTime;
  private bool mBlockFlag;
  [SerializeField]
  private MagicStoreDlg.Panel panel;
  private bool initialized;

  private void Start()
  {
    UIViewport componentInChildren = this.GetComponentInChildren<UIViewport>();
    if (!((UnityEngine.Object) null != (UnityEngine.Object) componentInChildren))
      return;
    componentInChildren.sourceCamera = UIManager.inst.ui2DCamera;
  }

  public void OpenMagicSelter()
  {
    UIManager.inst.OpenDlg("MagicSelterDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    this.OnShow(orgParam);
    this.NextRefreshTime = PlayerData.inst.magicStoreUpdateTime / 86400 * 86400 + 86400;
    if (this.NextRefreshTime - NetServerTime.inst.ServerTimestamp > 0)
    {
      this.UpdateData();
      this.InitItems();
    }
    BuilderFactory.Instance.Build((UIWidget) this.panel.TT_PlayceHolder, "Texture/GUI_Textures/npc_store_character", (System.Action<bool>) null, true, false, true, string.Empty);
    this.AddEventHandler();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemDataChange);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnItemDataChange);
  }

  private void OnItemDataChange(int itemid)
  {
    if (!this.panel.costItems.ContainsKey(itemid))
      return;
    this.panel.costItems[itemid].Refresh();
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataChange);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnItemDataChange);
  }

  private void InitItems()
  {
    List<int> goods = DBManager.inst.DB_Local_MagicStore.GetGoods();
    if (goods == null)
      return;
    for (int index = 0; index < goods.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.panel.itemTabel.gameObject, this.panel.itemSlotOrg.gameObject);
      gameObject.name = string.Format("item_slot_{0}", (object) (100 + index));
      gameObject.SetActive(true);
      MagicStoreItemSlot.Data data = new MagicStoreItemSlot.Data();
      data.itemInteranlId = goods[index];
      data.inIndex = index;
      data.onBuyButtonClickEvent = new System.Action<int>(this.OnBuyButtonClick);
      MagicStoreItemSlot component = gameObject.GetComponent<MagicStoreItemSlot>();
      component.Init((object) data);
      this.panel.items.Add(component);
    }
    this.panel.itemTabel.Reposition();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.OnHide(orgParam);
    BuilderFactory.Instance.Release((UIWidget) this.panel.TT_PlayceHolder);
    this.RemoveEventHandler();
    Dictionary<int, MagicStoreCostItem>.ValueCollection.Enumerator enumerator = this.panel.costItems.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if ((UnityEngine.Object) enumerator.Current != (UnityEngine.Object) null)
        enumerator.Current.Dispose();
    }
    this.panel.costItems.Clear();
  }

  private void OnSecond(int serverTime)
  {
    int time = this.NextRefreshTime - serverTime;
    if (time <= 0)
    {
      time = 0;
      this.Reload();
    }
    this.panel.refreshLeftTime.text = Utils.FormatTime(time, false, false, true);
    this.panel.curGold.text = Utils.FormatThousands(PlayerData.inst.userData.currency.gold.ToString());
    if (this.mBlockFlag)
      return;
    for (int index = 0; index < this.panel.items.Count; ++index)
      this.panel.items[index].RefreshCost();
  }

  private void UpdateData()
  {
    this.NextRefreshTime = PlayerData.inst.magicStoreUpdateTime / 86400 * 86400 + 86400;
    int time = this.NextRefreshTime - NetServerTime.inst.ServerTimestamp;
    if (time <= 0)
      time = 0;
    this.panel.refreshLeftTime.text = Utils.FormatTime(time, false, false, true);
    this.panel.refreshLeftTime.text = Utils.FormatTime(time, false, false, true);
    this.panel.refreshLeftTime.text = Utils.FormatTime(this.NextRefreshTime - NetServerTime.inst.ServerTimestamp, false, false, true);
    this.RefreshCostItems();
    this.ResfreshCost();
  }

  private void RefreshDlg(int index = -1)
  {
    this.UpdateData();
    if (index == -1)
    {
      this.StartCoroutine(this.ReplaceAll());
      this.panel.BT_refresh.isEnabled = false;
      this.SetItemsButtonEnable(false);
    }
    else
      this.ReplaceItem(index);
  }

  private void SetItemsButtonEnable(bool enable)
  {
    int index = 0;
    for (int count = this.panel.items.Count; index < count; ++index)
      this.panel.items[index].ButtonEnable = enable;
    this.mBlockFlag = !enable;
  }

  [DebuggerHidden]
  private IEnumerator ReplaceAll()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MagicStoreDlg.\u003CReplaceAll\u003Ec__Iterator82()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void ReplaceItem(int index)
  {
    MagicStoreItemSlot.Data data = new MagicStoreItemSlot.Data();
    List<int> goods = DBManager.inst.DB_Local_MagicStore.GetGoods();
    data.itemInteranlId = goods[index];
    data.inIndex = index;
    data.onBuyButtonClickEvent = new System.Action<int>(this.OnBuyButtonClick);
    new DynamicReplaceAnimator().PlayReplaceAnimation((DynamicReplaceItem) this.panel.items[index], (object) data);
  }

  public void OnBuyButtonClick(int index)
  {
    this.BuyItem(index);
  }

  public void OnShowIAP()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  private void RefreshCostItems()
  {
    List<int> allCostItems = ConfigManager.inst.DB_MagicStore.GetAllCostItems();
    for (int index = 0; index < allCostItems.Count; ++index)
      this.GetCostItem(allCostItems[index]).SetData(allCostItems[index]);
    this.panel.costItemsGrid.Reposition();
  }

  private MagicStoreCostItem GetCostItem(int itemId)
  {
    if (this.panel.costItems.ContainsKey(itemId))
      return this.panel.costItems[itemId];
    GameObject go = NGUITools.AddChild(this.panel.costItemsGrid.gameObject, this.panel.costItemPrefab.gameObject);
    NGUITools.SetActive(go, true);
    MagicStoreCostItem component = go.GetComponent<MagicStoreCostItem>();
    this.panel.costItems.Add(itemId, component);
    return component;
  }

  private void ResfreshCost()
  {
    int price = ConfigManager.inst.DB_MagicStoreRefreshPrice.GetPrice(DBManager.inst.DB_Local_MagicStore.buyTimes);
    if (price > 0)
    {
      this.panel.refreshCostContainer.gameObject.SetActive(true);
      this.panel.refresh_free.gameObject.SetActive(false);
      this.panel.refreshCost.text = price.ToString();
      if ((long) price > PlayerData.inst.userData.currency.gold)
        this.panel.refreshCost.color = Color.red;
      else
        this.panel.refreshCost.color = Color.white;
    }
    else
    {
      this.panel.refreshCostContainer.gameObject.SetActive(false);
      this.panel.refresh_free.gameObject.SetActive(true);
    }
  }

  public void Reload()
  {
    if (PlayerData.inst.magicStoreUpdateTime / 7200 >= NetServerTime.inst.ServerTimestamp / 7200)
      return;
    HubPort portByAction = MessageHub.inst.GetPortByAction("magicStore:loadGoods");
    PlayerData.inst.magicStoreUpdateTime = NetServerTime.inst.ServerTimestamp;
    portByAction.SendRequest((Hashtable) null, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      DBManager.inst.DB_Local_MagicStore.Update(arg2, (long) NetServerTime.inst.ServerTimestamp);
      if (!this.initialized)
      {
        this.UpdateData();
        this.InitItems();
        this.initialized = true;
      }
      else
        this.RefreshDlg(-1);
    }), true);
  }

  public void BuyItem(int slotIndex)
  {
    MagicStoreInfo info = ConfigManager.inst.DB_MagicStore.GetData(this.panel.items[slotIndex].npcStoreItemInteranlId);
    ItemStaticInfo itemInfo = ConfigManager.inst.DB_Items.GetItem(info.RewardItemId);
    MessageHub.inst.GetPortByAction("magicStore:buyGoods").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "slot_id", (object) (slotIndex + 1)), (System.Action<bool, object>) ((arg1, arg2) =>
    {
      DBManager.inst.DB_Local_MagicStore.Update(arg2, (long) NetServerTime.inst.ServerTimestamp);
      this.RefreshDlg(slotIndex);
      if (!arg1)
        return;
      RewardsCollectionAnimator.Instance.Clear();
      RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
      {
        count = (float) info.RewardValue,
        icon = itemInfo.ImagePath
      });
      RewardsCollectionAnimator.Instance.CollectItems(true);
      AudioManager.Instance.PlaySound("sfx_city_trading_post_buy", false);
    }), true);
  }

  public void OnRunesClicked()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  public void Refresh()
  {
    int price = ConfigManager.inst.DB_MagicStoreRefreshPrice.GetPrice(DBManager.inst.DB_Local_MagicStore.buyTimes);
    if (price > 0)
      UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
      {
        confirmButtonClickEvent = (System.Action) (() => MessageHub.inst.GetPortByAction("magicStore:refreshGoods").SendRequest((Hashtable) null, (System.Action<bool, object>) ((arg1, arg2) =>
        {
          DBManager.inst.DB_Local_MagicStore.Update(arg2, (long) NetServerTime.inst.ServerTimestamp);
          this.RefreshDlg(-1);
          AudioManager.Instance.PlaySound("sfx_city_trading_post_refresh", false);
        }), true)),
        description = ScriptLocalization.GetWithPara("confirm_gold_spend_trading_post_refresh_desc", new Dictionary<string, string>()
        {
          {
            "num",
            price.ToString()
          }
        }, true),
        goldNum = price
      });
    else
      MessageHub.inst.GetPortByAction("magicStore:refreshGoods").SendRequest((Hashtable) null, (System.Action<bool, object>) ((arg1, arg2) =>
      {
        DBManager.inst.DB_Local_MagicStore.Update(arg2, (long) NetServerTime.inst.ServerTimestamp);
        this.RefreshDlg(-1);
        AudioManager.Instance.PlaySound("sfx_city_trading_post_refresh", false);
      }), true);
  }

  [Serializable]
  protected class Panel
  {
    public Dictionary<int, MagicStoreCostItem> costItems = new Dictionary<int, MagicStoreCostItem>();
    public UILabel leftDiscription;
    public UILabel resFood;
    public UILabel resWood;
    public UILabel resOre;
    public UILabel resSilver;
    public UILabel refreshLeftTime;
    public Transform refreshCostContainer;
    public UILabel refreshCost;
    public UILabel refresh_free;
    public UIButton BT_refresh;
    public UITexture TT_PlayceHolder;
    public MagicStoreItemSlot itemSlotOrg;
    public UIScrollView scrollView;
    public UITable itemTabel;
    public List<MagicStoreItemSlot> items;
    public UITable costItemsGrid;
    public MagicStoreCostItem costItemPrefab;
    public UILabel curGold;
  }
}
