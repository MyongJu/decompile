﻿// Decompiled with JetBrains decompiler
// Type: Quad
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class Quad : MonoBehaviour
{
  public Vertex v0 = new Vertex(new Vector3(-1f, -1f, 0.0f), Color.white, new Vector2(0.0f, 0.0f));
  public Vertex v1 = new Vertex(new Vector3(-1f, 1f, 0.0f), Color.white, new Vector2(0.0f, 1f));
  public Vertex v2 = new Vertex(new Vector3(1f, 1f, 0.0f), Color.white, new Vector2(1f, 1f));
  public Vertex v3 = new Vertex(new Vector3(1f, -1f, 0.0f), Color.white, new Vector2(1f, 0.0f));
  public Vector2 UVOffset = Vector2.zero;
  private Vector3[] m_Points = new Vector3[4];
  private Color[] m_Colors = new Color[4];
  private Vector2[] m_UVs = new Vector2[4];
  private int[] m_Indices = new int[6];
  private Mesh m_Mesh;
  private MeshFilter m_MeshFilter;
  private MeshRenderer m_MeshRenderer;

  private void Start()
  {
    this.m_Indices[0] = 0;
    this.m_Indices[1] = 1;
    this.m_Indices[2] = 3;
    this.m_Indices[3] = 3;
    this.m_Indices[4] = 1;
    this.m_Indices[5] = 2;
  }

  private void Update()
  {
    this.InitOnce();
    this.m_Points[0] = this.v0.position;
    this.m_Colors[0] = this.v0.color;
    this.m_UVs[0] = this.v0.uv + this.UVOffset;
    this.m_Points[1] = this.v1.position;
    this.m_Colors[1] = this.v1.color;
    this.m_UVs[1] = this.v1.uv + this.UVOffset;
    this.m_Points[2] = this.v2.position;
    this.m_Colors[2] = this.v2.color;
    this.m_UVs[2] = this.v2.uv + this.UVOffset;
    this.m_Points[3] = this.v3.position;
    this.m_Colors[3] = this.v3.color;
    this.m_UVs[3] = this.v3.uv + this.UVOffset;
    this.m_Mesh.Clear();
    this.m_Mesh.vertices = this.m_Points;
    this.m_Mesh.colors = this.m_Colors;
    this.m_Mesh.uv = this.m_UVs;
    this.m_Mesh.triangles = this.m_Indices;
    this.m_MeshFilter.mesh = this.m_Mesh;
  }

  private void InitOnce()
  {
    if ((Object) this.m_Mesh == (Object) null)
      this.m_Mesh = new Mesh();
    this.m_MeshFilter = this.GetComponent<MeshFilter>();
    if ((Object) this.m_MeshFilter == (Object) null)
      this.m_MeshFilter = this.gameObject.AddComponent<MeshFilter>();
    this.m_MeshRenderer = this.GetComponent<MeshRenderer>();
    if (!((Object) this.m_MeshRenderer == (Object) null))
      return;
    this.m_MeshRenderer = this.gameObject.AddComponent<MeshRenderer>();
  }
}
