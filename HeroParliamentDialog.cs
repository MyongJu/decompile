﻿// Decompiled with JetBrains decompiler
// Type: HeroParliamentDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HeroParliamentDialog : UI.Dialog
{
  [SerializeField]
  private UITable _BenefitsTable;
  [SerializeField]
  private GameObject _BenefitTitleTemplate;
  [SerializeField]
  private GameObject _BenefitItemTemplate;
  [SerializeField]
  private UITable _TitleItemTable;
  [SerializeField]
  private HeroParliamentTitleItem _TitleItemTemplate;
  protected bool isDirty;
  protected List<ParliamentInfo> parliamentInfoList;
  protected List<HeroParliamentTitleItem> parliamentItemList;

  private void Init()
  {
    this.SetupUI();
  }

  private void RegisterEvents()
  {
    DBManager.inst.DB_LegendCard.onDataChanged += new System.Action<LegendCardData>(this.OnLegendDataChanged);
  }

  private void UnregisterEvents()
  {
    DBManager.inst.DB_LegendCard.onDataChanged -= new System.Action<LegendCardData>(this.OnLegendDataChanged);
  }

  public void OnLegendDataChanged(LegendCardData lengendData)
  {
    this.isDirty = true;
  }

  private void SetupUI()
  {
    this.InitParliamentInfoList();
    this.parliamentItemList = new List<HeroParliamentTitleItem>();
    using (List<ParliamentInfo>.Enumerator enumerator = this.parliamentInfoList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ParliamentInfo current = enumerator.Current;
        GameObject go = NGUITools.AddChild(this._TitleItemTable.gameObject, this._TitleItemTemplate.gameObject);
        NGUITools.SetActive(go, true);
        HeroParliamentTitleItem component = go.GetComponent<HeroParliamentTitleItem>();
        component.FeedData((IComponentData) new HeroParliamentTitleItemData(current));
        this.parliamentItemList.Add(component);
      }
    }
    this._TitleItemTable.Reposition();
  }

  private void UpdateUI()
  {
    List<LegendCardData> cardDataListByUid = DBManager.inst.DB_LegendCard.GetLegendCardDataListByUid(PlayerData.inst.uid);
    Dictionary<int, LegendCardData> heroDic = new Dictionary<int, LegendCardData>();
    using (List<LegendCardData>.Enumerator enumerator = cardDataListByUid.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        LegendCardData current = enumerator.Current;
        if (current.PositionAssigned)
        {
          if (heroDic.ContainsKey(current.Position))
            D.error((object) "Exists the same position_{0} hero card", (object) current.Position);
          else
            heroDic.Add(current.Position, current);
        }
      }
    }
    UIUtils.ClearTable(this._BenefitsTable);
    this.UpdateTitleItems(heroDic);
    this.UpdateHeroBenefits(heroDic);
    this.UpdateSuitBenefits();
    this._BenefitsTable.Reposition();
  }

  private void UpdateTitleItems(Dictionary<int, LegendCardData> heroDic)
  {
    using (List<HeroParliamentTitleItem>.Enumerator enumerator = this.parliamentItemList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HeroParliamentTitleItem current = enumerator.Current;
        LegendCardData cardData = (LegendCardData) null;
        if (heroDic.ContainsKey(current.GetTitlePosition()))
          cardData = heroDic[current.GetTitlePosition()];
        current.UpdateAssignedData(cardData);
      }
    }
  }

  private void UpdateSuitBenefits()
  {
    List<ParliamentSuitGroupInfo> suitGroupInfoList = ConfigManager.inst.DB_ParliamentSuitGroup.GetParliamentSuitGroupInfoList();
    if (suitGroupInfoList.Count <= 0)
      return;
    GameObject go = NGUITools.AddChild(this._BenefitsTable.gameObject, this._BenefitTitleTemplate);
    go.transform.Find("Label").GetComponent<UILabel>().text = Utils.XLAT("hero_council_group_benefits_subtitle");
    int num1 = 0;
    using (List<ParliamentSuitGroupInfo>.Enumerator enumerator = suitGroupInfoList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ParliamentSuitGroupInfo current = enumerator.Current;
        int suitActiveCount = HeroCardUtils.GetSuitActiveCount(current.internalId);
        int index1 = 0;
        for (int length = current.AllSuitRequirement.Length; index1 < length; ++index1)
        {
          int num2 = current.AllSuitRequirement[index1];
          if (suitActiveCount >= num2 && num2 > 0)
          {
            int index2 = current.AllSuitBenefit[index1];
            float num3 = current.AllSuitBenefitValue[index1];
            PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[index2];
            if (dbProperty != null)
            {
              go = NGUITools.AddChild(this._BenefitsTable.gameObject, this._BenefitItemTemplate);
              NGUITools.SetActive(go, true);
              go.transform.Find("Label").GetComponent<UILabel>().text = string.Format("{0}:{1} {2}", (object) current.Name, (object) dbProperty.Name, (object) dbProperty.ConvertToDisplayStringValue((double) num3, true));
              ++num1;
            }
          }
        }
      }
    }
    NGUITools.SetActive(go, num1 > 0);
  }

  private void UpdateHeroBenefits(Dictionary<int, LegendCardData> heroDic)
  {
    List<KeyValuePair<int, float>> heroBenefits = this.GetHeroBenefits(heroDic);
    if (heroBenefits.Count <= 0)
      return;
    GameObject go1 = NGUITools.AddChild(this._BenefitsTable.gameObject, this._BenefitTitleTemplate);
    NGUITools.SetActive(go1, true);
    go1.transform.Find("Label").GetComponent<UILabel>().text = Utils.XLAT("id_benefits");
    using (List<KeyValuePair<int, float>>.Enumerator enumerator = heroBenefits.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, float> current = enumerator.Current;
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[current.Key];
        if (dbProperty != null)
        {
          GameObject go2 = NGUITools.AddChild(this._BenefitsTable.gameObject, this._BenefitItemTemplate);
          NGUITools.SetActive(go2, true);
          go2.transform.Find("Label").GetComponent<UILabel>().text = string.Format("{0} {1}", (object) dbProperty.Name, (object) dbProperty.ConvertToDisplayStringValue((double) current.Value, true));
        }
      }
    }
  }

  private List<KeyValuePair<int, float>> GetHeroBenefits(Dictionary<int, LegendCardData> heroDic)
  {
    Dictionary<int, float> dictionary1 = new Dictionary<int, float>();
    List<KeyValuePair<int, float>> keyValuePairList = new List<KeyValuePair<int, float>>();
    using (Dictionary<int, LegendCardData>.Enumerator enumerator = heroDic.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        LegendCardData legendCardData = enumerator.Current.Value;
        ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(legendCardData.LegendId);
        if (parliamentHeroInfo != null)
        {
          float levelBenefitByLevel = parliamentHeroInfo.GetLevelBenefitByLevel(legendCardData.Level);
          if (dictionary1.ContainsKey(parliamentHeroInfo.levelBenefit))
          {
            Dictionary<int, float> dictionary2;
            int levelBenefit;
            (dictionary2 = dictionary1)[levelBenefit = parliamentHeroInfo.levelBenefit] = dictionary2[levelBenefit] + levelBenefitByLevel;
          }
          else
            dictionary1.Add(parliamentHeroInfo.levelBenefit, levelBenefitByLevel);
          int index1 = 0;
          for (int length = parliamentHeroInfo.AllStarRequirement.Length; index1 < length; ++index1)
          {
            int num1 = parliamentHeroInfo.AllStarRequirement[index1];
            if (legendCardData.Star >= num1 && num1 > 0)
            {
              int key = parliamentHeroInfo.AllStarBenefit[index1];
              float num2 = parliamentHeroInfo.AllStarBenefitValue[index1];
              if (dictionary1.ContainsKey(key))
              {
                Dictionary<int, float> dictionary2;
                int index2;
                (dictionary2 = dictionary1)[index2 = key] = dictionary2[index2] + num2;
              }
              else
                dictionary1.Add(key, num2);
            }
          }
        }
      }
    }
    using (Dictionary<int, float>.Enumerator enumerator = dictionary1.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, float> current = enumerator.Current;
        keyValuePairList.Add(new KeyValuePair<int, float>(current.Key, current.Value));
      }
    }
    keyValuePairList.Sort((Comparison<KeyValuePair<int, float>>) ((x, y) => x.Key.CompareTo(y.Key)));
    return keyValuePairList;
  }

  private void InitParliamentInfoList()
  {
    this.parliamentInfoList = ConfigManager.inst.DB_Parliament.GetParliamentInfoList();
    this.parliamentInfoList.Sort((Comparison<ParliamentInfo>) ((x, y) => x.priority.CompareTo(y.priority)));
  }

  public void Update()
  {
    if (!this.isDirty)
      return;
    this.isDirty = false;
    this.UpdateUI();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.RegisterEvents();
    this.Init();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.UnregisterEvents();
    base.OnClose(orgParam);
  }
}
