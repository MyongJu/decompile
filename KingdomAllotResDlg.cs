﻿// Decompiled with JetBrains decompiler
// Type: KingdomAllotResDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Diagnostics;
using UI;
using UnityEngine;

public class KingdomAllotResDlg : UI.Dialog
{
  public float m_scaleForCityIcon = 1.4f;
  public Vector3 m_offsetForCityIcon = new Vector3(0.0f, 70f, 0.0f);
  public float m_scaleForWarehouse = 1.4f;
  public Vector3 m_offsetForWarehouse = new Vector3(0.0f, 70f, 0.0f);
  private const int TagFood = 1;
  private const int TagWood = 2;
  private const int TagOre = 3;
  private const int TagSliver = 4;
  public UIInput m_foodCount;
  public UIInput m_woodCount;
  public UIInput m_oreCount;
  public UIInput m_sliverCount;
  public UISlider m_foodSlider;
  public UISlider m_woodSlider;
  public UISlider m_oreSlider;
  public UISlider m_sliverSlider;
  public UILabel m_title;
  public UILabel limit;
  public UIButton button;
  public UILabel kingdomName;
  public Color m_normalTextColor;
  public Color m_warningTextColor;
  public UITexture m_myCityIcon_store;
  public UILabel m_myFoodCount_store;
  public UILabel m_myWoodCount_store;
  public UILabel m_myOreCount_store;
  public UILabel m_mySliverCount_store;
  public UILabel playerName;
  public UITexture playerIcon;
  protected long m_food;
  protected long m_wood;
  protected long m_ore;
  protected long m_sliver;
  protected long _remainCount;
  private long _currentFood;
  private long _currentWood;
  private long _currentOre;
  private long _currentSliver;
  protected long _maxCount;
  private long _target_uid;
  protected bool m_ignoreValueChangesFromUI;
  protected KingdomAllotResDlg.Parameter m_Parameter;
  protected bool m_showWarning;

  public long GetRemainCount(int tagFood)
  {
    long num = 0;
    switch (tagFood)
    {
      case 1:
        num = this._maxCount - this.m_wood * this.WeightWood - this.m_ore * this.WeightOre - this.m_sliver * this.WeightSliver;
        break;
      case 2:
        num = this._maxCount - this.m_food * this.WeightFood - this.m_ore * this.WeightOre - this.m_sliver * this.WeightSliver;
        break;
      case 3:
        num = this._maxCount - this.m_food * this.WeightFood - this.m_wood * this.WeightWood - this.m_sliver * this.WeightSliver;
        break;
      case 4:
        num = this._maxCount - this.m_food * this.WeightFood - this.m_wood * this.WeightWood - this.m_ore * this.WeightOre;
        break;
    }
    return num;
  }

  protected long WeightFood
  {
    get
    {
      return (long) ConfigManager.inst.DB_GameConfig.GetData("food_load").ValueInt;
    }
  }

  protected long WeightWood
  {
    get
    {
      return (long) ConfigManager.inst.DB_GameConfig.GetData("wood_load").ValueInt;
    }
  }

  protected long WeightOre
  {
    get
    {
      return (long) ConfigManager.inst.DB_GameConfig.GetData("ore_load").ValueInt;
    }
  }

  protected long WeightSliver
  {
    get
    {
      return (long) ConfigManager.inst.DB_GameConfig.GetData("silver_load").ValueInt;
    }
  }

  protected long CurrentFood
  {
    get
    {
      return this._currentFood;
    }
  }

  protected long CurrentWood
  {
    get
    {
      return this._currentWood;
    }
  }

  protected long CurrentOre
  {
    get
    {
      return this._currentOre;
    }
  }

  protected long CurrentSliver
  {
    get
    {
      return this._currentSliver;
    }
  }

  protected long FoodInCity
  {
    get
    {
      return this.CurrentFood - this.m_food;
    }
  }

  protected long WoodInCity
  {
    get
    {
      return this.CurrentWood - this.m_wood;
    }
  }

  protected long OreInCity
  {
    get
    {
      return this.CurrentOre - this.m_ore;
    }
  }

  protected long SliverInCity
  {
    get
    {
      return this.CurrentSliver - this.m_sliver;
    }
  }

  protected long FoodMax
  {
    get
    {
      long num = this.GetRemainCount(1) / this.WeightFood;
      return num <= this._currentFood ? num : this._currentFood;
    }
  }

  protected long WoodMax
  {
    get
    {
      long num = this.GetRemainCount(2) / this.WeightWood;
      return num <= this._currentWood ? num : this._currentWood;
    }
  }

  protected long OreMax
  {
    get
    {
      long num = this.GetRemainCount(3) / this.WeightOre;
      return num <= this._currentOre ? num : this._currentOre;
    }
  }

  protected long SliverMax
  {
    get
    {
      long num = this.GetRemainCount(4) / this.WeightSliver;
      return num <= this._currentSliver ? num : this._currentSliver;
    }
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as KingdomAllotResDlg.Parameter;
    this._target_uid = this.m_Parameter.playerId;
    this.m_foodSlider.value = 0.0f;
    this.m_woodSlider.value = 0.0f;
    this.m_oreSlider.value = 0.0f;
    this.m_sliverSlider.value = 0.0f;
    this.InitUI();
    this.UpdateUI();
    this.RequestUserLimitFromServer();
  }

  protected void RequestUserLimitFromServer()
  {
    Hashtable postData = Utils.Hash((object) "target_uid", (object) this._target_uid);
    RequestManager.inst.SendRequest("king:getUserTreasuryResourceAssignInfo", postData, new System.Action<bool, object>(this.OnGetUserResLimitData), true);
  }

  protected void OnGetUserResLimitData(bool result, object data)
  {
    if (!result)
      return;
    Hashtable inData1 = data as Hashtable;
    Hashtable inData2 = inData1[(object) "treasury_resource"] as Hashtable;
    if (inData2 != null)
    {
      DatabaseTools.UpdateData(inData2, "food", ref this._currentFood);
      DatabaseTools.UpdateData(inData2, "wood", ref this._currentWood);
      DatabaseTools.UpdateData(inData2, "ore", ref this._currentOre);
      DatabaseTools.UpdateData(inData2, "silver", ref this._currentSliver);
    }
    string empty = string.Empty;
    DatabaseTools.UpdateData(inData1, "target_assign_info", ref empty);
    long num = 0;
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("king_send_resource_max");
    if (data1 != null)
      num = (long) data1.ValueInt;
    this._maxCount = num;
    if (!string.IsNullOrEmpty(empty))
    {
      string[] strArray = empty.Split('_');
      if (strArray.Length > 1)
      {
        int result1 = 0;
        int.TryParse(strArray[0], out result1);
        long result2 = 0;
        long.TryParse(strArray[1], out result2);
        if (result1 > 0 && NetServerTime.inst.ServerTimestamp - result1 < 86400)
          this._maxCount = num - result2;
      }
    }
    if (this._maxCount < 0L)
      this._maxCount = 0L;
    this.UpdateUI();
  }

  private GameObject CreateWorldObjectUnderUI(string prefabPath, UITexture parentTexture)
  {
    GameObject original = AssetManager.Instance.HandyLoad(prefabPath, (System.Type) null) as GameObject;
    GameObject go = (GameObject) null;
    if ((bool) ((UnityEngine.Object) original))
    {
      go = UnityEngine.Object.Instantiate<GameObject>(original);
      SpriteRenderer[] componentsInChildren = go.GetComponentsInChildren<SpriteRenderer>();
      Vector3 vector3_1 = float.MaxValue * Vector3.one;
      Vector3 vector3_2 = float.MinValue * Vector3.one;
      foreach (SpriteRenderer spriteRenderer in componentsInChildren)
      {
        spriteRenderer.sortingOrder = 1;
        vector3_1 = Vector3.Min(spriteRenderer.bounds.min, vector3_1);
        vector3_2 = Vector3.Max(spriteRenderer.bounds.max, vector3_2);
      }
      Bounds bounds = new Bounds();
      bounds.SetMinMax(vector3_1, vector3_2);
      NGUITools.SetLayer(go, LayerMask.NameToLayer("NGUI"));
      go.transform.SetParent(parentTexture.transform);
      SpriteRenderer spriteRenderer1 = componentsInChildren[0];
      Vector3 center = bounds.center;
      float x = bounds.size.x;
      float y = bounds.size.y;
      float num = (float) parentTexture.width / x;
      go.transform.localScale = num * Vector3.one;
      go.transform.localPosition = -1f * center;
      this.m_myCityIcon_store.enabled = false;
      WonderVFX component = go.GetComponent<WonderVFX>();
      if ((UnityEngine.Object) null != (UnityEngine.Object) component)
        component.Reset();
    }
    return go;
  }

  private void InitUI()
  {
    int level = PlayerData.inst.playerCityData.level;
    DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) PlayerData.inst.userData.world_id);
    Mathf.Min(Mathf.Max(0, level - 1), MapUtils.CityPrefabNames.Length - 1);
    this.CreateWorldObjectUnderUI("Prefab/Tiles/wonder_northern02", this.m_myCityIcon_store);
    this.m_myCityIcon_store.enabled = false;
    this.kingdomName.text = wonderData.LabelName;
    UserData userData = DBManager.inst.DB_User.Get(this._target_uid);
    this.playerName.text = userData.userName_Alliance_Name;
    CustomIconLoader.Instance.requestCustomIcon(this.playerIcon, UserData.GetPortraitIconPath(userData.portrait), userData.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.playerIcon, userData.LordTitle, 1);
  }

  private void UpdateUI()
  {
    this.RefreshCommonUI();
    this.RefreshStoreUI();
  }

  private void RefreshCommonUI()
  {
    this.m_foodCount.value = Utils.FormatThousands(this.m_food.ToString());
    this.m_woodCount.value = Utils.FormatThousands(this.m_wood.ToString());
    this.m_oreCount.value = Utils.FormatThousands(this.m_ore.ToString());
    this.m_sliverCount.value = Utils.FormatThousands(this.m_sliver.ToString());
    this.button.isEnabled = !this.IsEmpty;
  }

  private string MyWarehouseName
  {
    get
    {
      return string.Empty;
    }
  }

  private void RefreshStoreUI()
  {
    this.m_myFoodCount_store.text = Utils.FormatShortThousandsLong(this.FoodInCity);
    this.m_myWoodCount_store.text = Utils.FormatShortThousandsLong(this.WoodInCity);
    this.m_myOreCount_store.text = Utils.FormatShortThousandsLong(this.OreInCity);
    this.m_mySliverCount_store.text = Utils.FormatShortThousandsLong(this.SliverInCity);
    this.limit.text = Utils.FormatShortThousandsLong(this.TotalResoucesWeight) + "/" + Utils.FormatShortThousandsLong(this._maxCount);
  }

  private long TotalResouces
  {
    get
    {
      return this.m_food + this.m_wood + this.m_ore + this.m_sliver;
    }
  }

  private long TotalResoucesWeight
  {
    get
    {
      return this.m_food * this.WeightFood + this.m_wood * this.WeightWood + this.m_ore * this.WeightOre + this.m_sliver * this.WeightSliver;
    }
  }

  protected void GetResourceValuesFromSliderControl(int sliderTag)
  {
    long foodMax = this.FoodMax;
    long woodMax = this.WoodMax;
    long oreMax = this.OreMax;
    long sliverMax = this.SliverMax;
    switch (sliderTag)
    {
      case 1:
        this.m_food = (long) ((double) this.m_foodSlider.value * (double) foodMax);
        break;
      case 2:
        this.m_wood = (long) ((double) this.m_woodSlider.value * (double) woodMax);
        break;
      case 3:
        this.m_ore = (long) ((double) this.m_oreSlider.value * (double) oreMax);
        break;
      case 4:
        this.m_sliver = (long) ((double) this.m_sliverSlider.value * (double) sliverMax);
        break;
    }
  }

  protected long GetLongValueFromEdit(UIInput input)
  {
    long result = 0;
    long.TryParse(input.value, out result);
    if (result < 0L)
      result = 0L;
    return result;
  }

  protected void GetResourceValuesFromEditControl(int sliderTag)
  {
    long num1 = 0;
    long num2 = 0;
    long num3 = 0;
    long num4 = 0;
    num1 = this.FoodMax;
    num2 = this.WoodMax;
    num3 = this.OreMax;
    num4 = this.SliverMax;
    switch (sliderTag)
    {
      case 1:
        this.m_food = this.GetLongValueFromEdit(this.m_foodCount);
        break;
      case 2:
        this.m_wood = this.GetLongValueFromEdit(this.m_woodCount);
        break;
      case 3:
        this.m_ore = this.GetLongValueFromEdit(this.m_oreCount);
        break;
      case 4:
        this.m_sliver = this.GetLongValueFromEdit(this.m_sliverCount);
        break;
    }
  }

  protected void SetResourceValuesToControl(int sliderTag)
  {
    switch (sliderTag)
    {
      case 1:
        this.m_foodSlider.value = this.m_food > 0L ? (float) this.m_food / (float) this.FoodMax : 0.0f;
        break;
      case 2:
        this.m_woodSlider.value = this.m_wood > 0L ? (float) this.m_wood / (float) this.WoodMax : 0.0f;
        break;
      case 3:
        this.m_oreSlider.value = this.m_ore > 0L ? (float) this.m_ore / (float) this.OreMax : 0.0f;
        break;
      case 4:
        this.m_sliverSlider.value = this.m_sliver > 0L ? (float) this.m_sliver / (float) this.SliverMax : 0.0f;
        break;
    }
  }

  protected long MaxValue(long a, long b)
  {
    if (a > b)
      return a;
    return b;
  }

  protected long MinValue(long a, long b)
  {
    if (a < b)
      return a;
    return b;
  }

  protected void AjustMaxValueForTakeback(int sliderTag)
  {
    this.m_food = this.MinValue(this.m_food, this.FoodMax);
    this.m_wood = this.MinValue(this.m_wood, this.WoodMax);
    this.m_ore = this.MinValue(this.m_ore, this.OreMax);
    this.m_sliver = this.MinValue(this.m_sliver, this.SliverMax);
  }

  public void OnSliderValueChanged(int sliderTag)
  {
    if (this.m_ignoreValueChangesFromUI)
      return;
    this.m_ignoreValueChangesFromUI = true;
    this.GetResourceValuesFromSliderControl(sliderTag);
    this.AjustMaxValueForTakeback(sliderTag);
    this.SetResourceValuesToControl(sliderTag);
    this.UpdateUI();
    this.CheckToShowWarning();
    this.m_ignoreValueChangesFromUI = false;
  }

  public void OnFoodSliderValueChanged()
  {
    this.OnSliderValueChanged(1);
  }

  public void OnWoodSliderValueChanged()
  {
    this.OnSliderValueChanged(2);
  }

  public void OnOreSliderValueChanged()
  {
    this.OnSliderValueChanged(3);
  }

  public void OnSliverSliderValueChanged()
  {
    this.OnSliderValueChanged(4);
  }

  protected void OnEditValueChanged(int editTag)
  {
    if (this.m_ignoreValueChangesFromUI)
      return;
    this.m_ignoreValueChangesFromUI = true;
    this.GetResourceValuesFromEditControl(editTag);
    this.AjustMaxValueForTakeback(editTag);
    this.SetResourceValuesToControl(editTag);
    this.UpdateUI();
    this.CheckToShowWarning();
    this.m_ignoreValueChangesFromUI = false;
  }

  public void OnFoodEditValueChanged()
  {
    this.OnEditValueChanged(1);
  }

  public void OnWoodEditValueChanged()
  {
    this.OnEditValueChanged(2);
  }

  public void OnOreEditValueChanged()
  {
    this.OnEditValueChanged(3);
  }

  public void OnSliverEditValueChanged()
  {
    this.OnEditValueChanged(4);
  }

  protected void OnTakebackResult(bool result, object data)
  {
    this.OnCloseButtonPress();
  }

  private bool IsEmpty
  {
    get
    {
      if (this.m_food == 0L && this.m_wood == 0L && this.m_ore == 0L)
        return this.m_sliver == 0L;
      return false;
    }
  }

  public void OnStoreButtonClicked()
  {
    if (this.IsEmpty)
      return;
    Hashtable hashtable = new Hashtable();
    hashtable[(object) "food"] = (object) this.m_food;
    hashtable[(object) "wood"] = (object) this.m_wood;
    hashtable[(object) "ore"] = (object) this.m_ore;
    hashtable[(object) "silver"] = (object) this.m_sliver;
    RequestManager.inst.SendRequest("king:assignTreasuryResource", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "target_uid",
        (object) this._target_uid
      },
      {
        (object) "resource",
        (object) hashtable
      }
    }, new System.Action<bool, object>(this.OnAssignCallBack), true);
  }

  private void OnAssignCallBack(bool success, object result)
  {
    if (!success)
      return;
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_throne_resource_allocated_success", true), (System.Action) null, 4f, false);
  }

  protected void OnStoreResult(bool result, object data)
  {
    this.OnCloseButtonPress();
  }

  protected void CheckToShowWarning()
  {
    if (this.m_showWarning || this.TotalResoucesWeight <= 0L || this.TotalResoucesWeight < this._maxCount)
      return;
    TweenScale component = this.limit.GetComponent<TweenScale>();
    if (!(bool) ((UnityEngine.Object) component))
      return;
    this.m_showWarning = true;
    component.enabled = true;
    component.ResetToBeginning();
    component.Play();
    this.limit.color = this.m_warningTextColor;
    this.StartCoroutine(this.StopTweenScale(component.duration * 1f, component, (System.Action) (() =>
    {
      this.limit.color = this.m_normalTextColor;
      this.m_showWarning = false;
    })));
  }

  [DebuggerHidden]
  protected IEnumerator StopTweenScale(float duation, TweenScale tweenScale, System.Action callback)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new KingdomAllotResDlg.\u003CStopTweenScale\u003Ec__Iterator77()
    {
      duation = duation,
      tweenScale = tweenScale,
      callback = callback,
      \u003C\u0024\u003Eduation = duation,
      \u003C\u0024\u003EtweenScale = tweenScale,
      \u003C\u0024\u003Ecallback = callback
    };
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public long playerId;
  }
}
