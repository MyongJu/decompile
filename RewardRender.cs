﻿// Decompiled with JetBrains decompiler
// Type: RewardRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RewardRender : MonoBehaviour
{
  public UITexture texture;
  public UILabel description;
  public UILabel amount;
  public UISprite spriteIcon;
  public Transform border;
  public UITexture background;
  private int _itemId;

  public void Feed(SystemMessage.ResourceReward reward)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.texture, "Texture/BuildingConstruction/" + reward.resourceName + "_icon", (System.Action<bool>) null, true, false, string.Empty);
    this.description.text = reward.amount.ToString() + " " + Utils.XLAT(reward.resourceName);
    this.amount.text = Utils.FormatShortThousands(reward.amount);
    Utils.SetItemNormalName(this.description, 0);
    Utils.SetItemNormalBackground(this.background, 0);
  }

  public void OnClickHandler()
  {
    Utils.ShowItemTip(this._itemId, this.border, 0L, 0L, 0);
  }

  public void OnPressHandler()
  {
    Utils.DelayShowTip(this._itemId, this.border, 0L, 0L, 0);
  }

  public void OnReleaseHandler()
  {
    Utils.StopShowItemTip();
  }

  public void Feed(SystemMessage.ItemReward reward)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(reward.internalID);
    if (itemStaticInfo != null)
    {
      this._itemId = reward.internalID;
      BuilderFactory.Instance.HandyBuild((UIWidget) this.texture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    }
    this.description.text = Utils.XLAT(itemStaticInfo.Loc_Name_Id);
    Utils.SetItemNormalName(this.description, itemStaticInfo.Quality);
    Utils.SetItemNormalBackground(this.background, itemStaticInfo.Quality);
    this.amount.text = Utils.FormatShortThousands(reward.amount);
  }

  public void Feed(SystemMessage.GoldReward reward)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.texture, "Texture/BuildingConstruction/gold_coin", (System.Action<bool>) null, true, false, string.Empty);
    this.description.text = "Gold";
    this.amount.text = Utils.FormatShortThousands(reward.amount);
    Utils.SetItemNormalName(this.description, 0);
    Utils.SetItemNormalBackground(this.background, 0);
  }
}
