﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsHistorySlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTrialsHistorySlot : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelWin;
  [SerializeField]
  private UILabel _labelLost;
  private MerlinTrialsPayload.HistoryData _historyData;

  public void SetData(MerlinTrialsPayload.HistoryData historyData)
  {
    this._historyData = historyData;
    this.UpdateUI();
  }

  public void OnDetailPress()
  {
    if (this._historyData == null)
      return;
    MerlinTrialsPayload.Instance.GetWarLog(this._historyData.logId, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable hashtable = data as Hashtable;
      if (hashtable == null || !hashtable.ContainsKey((object) "detail"))
        return;
      UIManager.inst.OpenPopup("MerlinTrials/MerlinTrialsWarReportPopup", (Popup.PopupParameter) new MerlinTrialsWarReportPopup.Parameter()
      {
        winWar = this._historyData.WinOrLost,
        mailData = (hashtable[(object) "detail"] as Hashtable)
      });
    }));
  }

  private void UpdateUI()
  {
    if (this._historyData == null)
      return;
    NGUITools.SetActive(this._labelWin.gameObject, this._historyData.WinOrLost);
    NGUITools.SetActive(this._labelLost.gameObject, !this._historyData.WinOrLost);
    MerlinTrialsMainInfo merlinTrialsMainInfo = ConfigManager.inst.DB_MerlinTrialsMain.Get(this._historyData.mainId);
    if (merlinTrialsMainInfo == null)
      return;
    Dictionary<string, string> para = new Dictionary<string, string>();
    MerlinTrialsGroupInfo merlinTrialsGroupInfo = ConfigManager.inst.DB_MerlinTrialsGroup.Get(merlinTrialsMainInfo.trialId);
    if (merlinTrialsGroupInfo != null)
      para.Add("0", merlinTrialsGroupInfo.LocName);
    MerlinTrialsLayerInfo merlinTrialsLayerInfo = ConfigManager.inst.DB_MerlinTrialsLayer.Get(merlinTrialsMainInfo.layerId);
    if (merlinTrialsLayerInfo != null)
      para.Add("1", merlinTrialsLayerInfo.level.ToString());
    para.Add("2", merlinTrialsMainInfo.TrialIndex.ToString());
    string str = Utils.FormatTimeMMDDHHMMSS((long) this._historyData.time, "-");
    this._labelWin.text = string.Format("{0} {1}", (object) str, (object) ScriptLocalization.GetWithPara("trial_log_success_description", para, true));
    this._labelLost.text = string.Format("{0} {1}", (object) str, (object) ScriptLocalization.GetWithPara("trial_log_fail_description", para, true));
  }
}
