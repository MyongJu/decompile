﻿// Decompiled with JetBrains decompiler
// Type: ConfigTempleSkill
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;

public class ConfigTempleSkill
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<int, TempleSkillInfo> m_allTemleSkillInfo;

  public void BuildDB(object res)
  {
    this.parse.Parse<TempleSkillInfo, int>(res as Hashtable, "internalId", out this.m_allTemleSkillInfo);
    using (Dictionary<int, TempleSkillInfo>.ValueCollection.Enumerator enumerator = this.m_allTemleSkillInfo.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TempleSkillInfo current = enumerator.Current;
        current.CdTime = (int) ((double) current._CdTimeInMinute * 3600.0);
        current.DonationTime = (int) ((double) current._DonationTimeInMinute * 60.0);
        current.EffectTimeMin = (int) ((double) current._EffectTimeMinInMinute * 60.0);
        current.EffectTimeMax = (int) ((double) current._EffectTimeMaxInMinute * 60.0);
      }
    }
  }

  public void Clear()
  {
    if (this.m_allTemleSkillInfo == null)
      return;
    this.m_allTemleSkillInfo.Clear();
  }

  public TempleSkillInfo GetBySkillType(string type)
  {
    using (Dictionary<int, TempleSkillInfo>.Enumerator enumerator = this.m_allTemleSkillInfo.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, TempleSkillInfo> current = enumerator.Current;
        if (current.Value.Type == type)
          return current.Value;
      }
    }
    return (TempleSkillInfo) null;
  }

  public TempleSkillInfo GetById(int internalId)
  {
    if (this.m_allTemleSkillInfo.ContainsKey(internalId))
      return this.m_allTemleSkillInfo[internalId];
    return (TempleSkillInfo) null;
  }

  public bool IsHaveSkillUnlockAtLevel(int level)
  {
    using (Dictionary<int, TempleSkillInfo>.Enumerator enumerator = this.m_allTemleSkillInfo.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.UnlockLevel == level)
          return true;
      }
    }
    return false;
  }

  public List<TempleSkillInfo> GetByLevelAndGroup(int unlockLevel, string group)
  {
    List<TempleSkillInfo> templeSkillInfoList = new List<TempleSkillInfo>();
    using (Dictionary<int, TempleSkillInfo>.Enumerator enumerator = this.m_allTemleSkillInfo.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, TempleSkillInfo> current = enumerator.Current;
        if (current.Value.Group == group && current.Value.UnlockLevel == unlockLevel)
          templeSkillInfoList.Add(current.Value);
      }
    }
    return templeSkillInfoList;
  }

  public string GetSkillDescription(int internalId)
  {
    TempleSkillInfo byId = this.GetById(internalId);
    if (byId == null)
      return string.Empty;
    Dictionary<string, string> para = new Dictionary<string, string>();
    float num1 = (float) byId.EffectTimeMin / 60f;
    float num2 = (float) byId.EffectTimeMax / 60f;
    string type = byId.Type;
    if (type != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (ConfigTempleSkill.\u003C\u003Ef__switch\u0024map4B == null)
      {
        // ISSUE: reference to a compiler-generated field
        ConfigTempleSkill.\u003C\u003Ef__switch\u0024map4B = new Dictionary<string, int>(12)
        {
          {
            "heal_speed",
            0
          },
          {
            "interrupt",
            1
          },
          {
            "chains",
            2
          },
          {
            "research_speed",
            3
          },
          {
            "shield",
            4
          },
          {
            "flame",
            5
          },
          {
            "building_speed",
            6
          },
          {
            "force_teleport",
            7
          },
          {
            "wounded_killer",
            8
          },
          {
            "gather_speed",
            9
          },
          {
            "tenacious",
            10
          },
          {
            "march_speed",
            11
          }
        };
      }
      int num3;
      // ISSUE: reference to a compiler-generated field
      if (ConfigTempleSkill.\u003C\u003Ef__switch\u0024map4B.TryGetValue(type, out num3))
      {
        switch (num3)
        {
          case 0:
            para.Add("0", ((double) byId.Param1Min * 100.0).ToString() + "%");
            para.Add("1", num1.ToString());
            return ScriptLocalization.GetWithPara(byId.DescriptionKey, para, true);
          case 1:
            return Utils.XLAT(byId.DescriptionKey);
          case 2:
            para.Add("0", num1.ToString());
            para.Add("1", num2.ToString());
            return ScriptLocalization.GetWithPara(byId.DescriptionKey, para, true);
          case 3:
            para.Add("0", ((double) byId.Param1Min * 100.0).ToString() + "%");
            para.Add("1", num1.ToString());
            return ScriptLocalization.GetWithPara(byId.DescriptionKey, para, true);
          case 4:
            para.Add("0", (num1 / 60f).ToString());
            return ScriptLocalization.GetWithPara(byId.DescriptionKey, para, true);
          case 5:
            para.Add("0", byId.TargetArea.ToString());
            para.Add("1", ((double) byId.Param1Min * 100.0).ToString() + "%");
            para.Add("2", ((double) byId.Param1Max * 100.0).ToString() + "%");
            para.Add("3", byId.Param2Min.ToString());
            para.Add("4", byId.Param2Max.ToString());
            return ScriptLocalization.GetWithPara(byId.DescriptionKey, para, true);
          case 6:
            para.Add("0", ((double) byId.Param1Min * 100.0).ToString() + "%");
            para.Add("1", num1.ToString());
            return ScriptLocalization.GetWithPara(byId.DescriptionKey, para, true);
          case 7:
            return Utils.XLAT(byId.DescriptionKey);
          case 8:
            para.Add("0", ((double) byId.Param1Min * 100.0).ToString() + "%");
            para.Add("1", ((double) byId.Param1Max * 100.0).ToString() + "%");
            return ScriptLocalization.GetWithPara(byId.DescriptionKey, para, true);
          case 9:
            para.Add("0", ((double) byId.Param1Min * 100.0).ToString() + "%");
            para.Add("1", num1.ToString());
            return ScriptLocalization.GetWithPara(byId.DescriptionKey, para, true);
          case 10:
            para.Add("0", ((double) byId.Param1Min * 100.0).ToString() + "%");
            para.Add("1", num1.ToString());
            return ScriptLocalization.GetWithPara(byId.DescriptionKey, para, true);
          case 11:
            para.Add("0", num1.ToString());
            para.Add("1", num2.ToString());
            para.Add("2", ((double) byId.Param1Min * 100.0).ToString() + "%");
            para.Add("3", ((double) byId.Param1Max * 100.0).ToString() + "%");
            return ScriptLocalization.GetWithPara(byId.DescriptionKey, para, true);
        }
      }
    }
    return string.Empty;
  }
}
