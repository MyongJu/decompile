﻿// Decompiled with JetBrains decompiler
// Type: CityDecorationVFX
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class CityDecorationVFX : MonoBehaviour, IConstructionController
{
  private const string ASSET_PATH = "Prefab/Decoration/";
  private GameObject _decorationObject;
  private string _currentDecoration;
  private Renderer[] _renderers;
  private PrefabSpawnRequestEx _request;
  private int _sortingLayerID;

  public void Reset()
  {
  }

  public void UpdateUI(string decoration, int level)
  {
    if (!string.IsNullOrEmpty(decoration))
    {
      decoration = string.Format("{0}_{1}", (object) decoration, (object) level);
      if (!(this._currentDecoration != decoration))
        return;
      try
      {
        this._currentDecoration = decoration;
        this.DestroyDecorationObject();
        PrefabManagerEx.Instance.AddRouteMapping(decoration, "Prefab/Decoration/" + decoration);
        this._request = PrefabManagerEx.Instance.SpawnAsync(decoration, this.transform, new PrefabSpawnRequestCallback(this.OnAssetLoaded), (object) null);
      }
      catch
      {
        this.OnFinalize();
      }
    }
    else
      this.OnFinalize();
  }

  private void OnAssetLoaded(GameObject go, object userData)
  {
    this._request = (PrefabSpawnRequestEx) null;
    if (!(bool) ((Object) go))
      return;
    this._decorationObject = go;
    NGUITools.SetLayer(this._decorationObject, this.transform.gameObject.layer);
    this._decorationObject.transform.parent = this.transform;
    this._decorationObject.transform.localPosition = Vector3.zero;
    this._decorationObject.transform.localRotation = Quaternion.identity;
    this._decorationObject.transform.localScale = Vector3.one;
    this._renderers = this._decorationObject.GetComponentsInChildren<Renderer>(true);
    this.UpdateSortingLayerID();
  }

  public void Clear()
  {
    this.OnFinalize();
  }

  public void UpdateUI(TileData tile)
  {
    CityData cityData = tile.CityData;
    if (cityData != null)
      this.UpdateUI(cityData.decoration, cityData.level);
    else
      this.OnFinalize();
  }

  public void SetSortingLayerID(int sortingLayerID)
  {
    this._sortingLayerID = sortingLayerID;
    this.UpdateSortingLayerID();
  }

  private void UpdateSortingLayerID()
  {
    if (this._renderers == null)
      return;
    for (int index = 0; index < this._renderers.Length; ++index)
      this._renderers[index].sortingLayerID = this._sortingLayerID;
  }

  public void Update()
  {
  }

  private void OnFinalize()
  {
    this.DestroyDecorationObject();
    this._currentDecoration = (string) null;
    this._renderers = (Renderer[]) null;
  }

  private void DestroyDecorationObject()
  {
    if ((bool) ((Object) this._decorationObject))
      PrefabManagerEx.Instance.Destroy(this._decorationObject);
    this._decorationObject = (GameObject) null;
    if (this._request == null)
      return;
    this._request.cancel = true;
    this._request = (PrefabSpawnRequestEx) null;
  }
}
