﻿// Decompiled with JetBrains decompiler
// Type: GetMoreLegendDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;

public class GetMoreLegendDlg : Popup
{
  public UILabel m_ItemName;
  public UILabel m_Description;
  public UILabel m_Price;
  public UIButton m_GetButton;
  public GetMoreLegendDlg.UpdateHandler RefreshHandler;
  private int maxSlots;
  private int buyprice;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    int num = DBManager.inst.DB_Legend.GetSlotNum() + 1;
    long gold = DBManager.inst.DB_User.Get(PlayerData.inst.uid).currency.gold;
    this.maxSlots = ConfigManager.inst.DB_LegendSlots.GetTotalNum();
    if (num > this.maxSlots)
    {
      this.m_Description.text = string.Format("We can't build more slots!");
      this.m_Description.fontSize = int.Parse("72");
      this.m_GetButton.gameObject.SetActive(false);
    }
    else if (gold < (long) this.buyprice)
    {
      this.m_Description.text = string.Format("We need more Gold!");
      this.m_Description.fontSize = int.Parse("72");
      this.m_GetButton.gameObject.SetActive(false);
    }
    else
    {
      this.buyprice = ConfigManager.inst.DB_LegendSlots.GetLegendSlotInfo(string.Format("legend_slot_{0}", (object) num.ToString())).Gold;
      this.RefreshHandler = (orgParam as GetMoreLegendDlg.GetMoreSlotDialogParamer).RefreshHandler;
      this.m_Price.text = this.buyprice.ToString();
    }
  }

  private int GetTotalNum()
  {
    List<CityMapData> byUidCityId = DBManager.inst.DB_CityMap.GetByUid_CityId(PlayerData.inst.uid, (long) PlayerData.inst.cityId);
    int num = 0;
    using (List<CityMapData>.Enumerator enumerator = byUidCityId.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        List<InBuildingData> inBuildingData = enumerator.Current.inBuildingData;
        for (int index = 0; index < inBuildingData.Count; ++index)
        {
          if (inBuildingData[index].type == "slot_num")
          {
            num = int.Parse(inBuildingData[index].value);
            break;
          }
        }
      }
    }
    return num;
  }

  private void UnlockLegendSlot()
  {
    MessageHub.inst.GetPortByAction("Legend:unlockTavernSlot").SendRequest(new Hashtable()
    {
      {
        (object) "city_id",
        (object) PlayerData.inst.cityId
      },
      {
        (object) "building_id",
        (object) CitadelSystem.inst.GetBuildingByType("dragon_lair").mBuildingItem.mID
      },
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, new System.Action<bool, object>(this.UNLOCKCALLBACK), true);
  }

  public void OnGetButtonPressed()
  {
    if (DBManager.inst.DB_User.Get(PlayerData.inst.uid).currency.gold < (long) this.buyprice)
      UIManager.inst.toast.Show(ScriptLocalization.Get("We need more gold!", true), (System.Action) null, 4f, false);
    else
      this.UnlockLegendSlot();
  }

  private void UNLOCKCALLBACK(bool success, object result)
  {
    if (!success)
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_unlocktravernslot_success", true), (System.Action) null, 4f, false);
    this.OnCloseBtnPress();
    if (this.RefreshHandler == null)
      return;
    this.RefreshHandler();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  public void OnCloseBtnPress()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  public class GetMoreSlotDialogParamer : Popup.PopupParameter
  {
    public GetMoreLegendDlg.UpdateHandler RefreshHandler;
  }

  public delegate void UpdateHandler();
}
