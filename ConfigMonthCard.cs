﻿// Decompiled with JetBrains decompiler
// Type: ConfigMonthCard
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigMonthCard
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, MonthCardInfo> datas;
  private Dictionary<long, MonthCardInfo> dicByUniqueId;

  public object ParseDict(object rs)
  {
    Hashtable hashtable = rs as Hashtable;
    Dictionary<long, int> dictionary = new Dictionary<long, int>();
    if (hashtable == null)
      return (object) dictionary;
    IEnumerator enumerator = hashtable.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      long result1;
      int result2;
      if (long.TryParse(enumerator.Current.ToString(), out result1) && int.TryParse(hashtable[enumerator.Current].ToString(), out result2))
        dictionary.Add(result1, result2);
    }
    return (object) dictionary;
  }

  public void BuildDB(object res)
  {
    this.parse.AddParseFunc("ParseComposeItems", new ParseFunc(this.ParseDict));
    this.parse.Parse<MonthCardInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, MonthCardInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<long, MonthCardInfo>) null;
  }

  public MonthCardInfo GetMonthCardInfo(long internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (MonthCardInfo) null;
  }

  public MonthCardInfo GetMonthCardInfo(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (MonthCardInfo) null;
  }

  public Dictionary<long, MonthCardInfo> GetAllMonthCard()
  {
    return this.dicByUniqueId;
  }
}
