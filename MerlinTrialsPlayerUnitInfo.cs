﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsPlayerUnitInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class MerlinTrialsPlayerUnitInfo
{
  private int totalUnit = -1;
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(CustomParse = true, CustomType = typeof (UnitGroup), Name = "Units")]
  public UnitGroup units;

  public int TotalUnit
  {
    get
    {
      if (this.totalUnit >= 0)
        return this.totalUnit;
      this.totalUnit = 0;
      List<UnitGroup.UnitRecord> unitList = this.units.GetUnitList();
      int index = 0;
      for (int count = unitList.Count; index < count; ++index)
        this.totalUnit += unitList[index].value;
      return this.totalUnit;
    }
  }
}
