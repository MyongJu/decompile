﻿// Decompiled with JetBrains decompiler
// Type: DKArenaTierUpVFX
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DKArenaTierUpVFX : MonoBehaviour
{
  public ParticleSystem Player;
  public GameObject OldTier;
  public GameObject NewTier;

  public void SetData(string oldTier, string newTier)
  {
    Renderer componentsInChild1 = this.OldTier.GetComponentsInChildren<Renderer>(true)[0];
    Renderer componentsInChild2 = this.NewTier.GetComponentsInChildren<Renderer>(true)[0];
    componentsInChild1.material.mainTexture = (Texture) (AssetManager.Instance.HandyLoad(oldTier, (System.Type) null) as Texture2D);
    componentsInChild2.material.mainTexture = (Texture) (AssetManager.Instance.HandyLoad(newTier, (System.Type) null) as Texture2D);
  }

  public void Play()
  {
    this.Stop();
    this.Player.Play(true);
  }

  public void Stop()
  {
    this.Player.Clear();
    this.Player.Stop(true);
  }
}
