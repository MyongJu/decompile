﻿// Decompiled with JetBrains decompiler
// Type: IAPGoldInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class IAPGoldInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "gold")]
  public long gold;
  [Config(Name = "icon")]
  public string icon;
  [Config(Name = "type")]
  public int type;
  [Config(Name = "ex_gold")]
  public long ex_gold;
  [Config(Name = "tag")]
  public string tag;

  public string IconPath
  {
    get
    {
      return string.Format("Texture/ItemIcons/{0}", (object) this.icon);
    }
  }

  public bool IsOverbalance
  {
    get
    {
      long count;
      long date;
      IAPStoreUtils.GetPurchaseStats(this.id, out count, out date);
      switch (this.type)
      {
        case 1:
          if (date > 0L)
            return false;
          break;
        case 2:
          long num1 = (long) NetServerTime.inst.ServerTimestamp / 86400L * 86400L;
          long num2 = num1 + 86400L;
          if (num1 <= date && date <= num2)
            return false;
          break;
      }
      return this.ex_gold > 0L;
    }
  }

  public string productId
  {
    get
    {
      IAPPlatformInfo iapPlatformInfo = ConfigManager.inst.DB_IAPPlatform.Get(Application.platform, this.id);
      if (iapPlatformInfo != null)
        return iapPlatformInfo.product_id;
      return string.Empty;
    }
  }
}
