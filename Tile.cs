﻿// Decompiled with JetBrains decompiler
// Type: Tile
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Tile : MonoBehaviour, IRecycle
{
  public UISprite Sprite;
  public UISprite SpriteBanner;
  public UILabel LabelLevel;
  public UILabel LabelTitle;
  [HideInInspector]
  public TileType TileType;

  public void Init(int tileID, TileType type)
  {
    this.TileType = type;
  }

  public void OnInitialize()
  {
  }

  public void OnFinalize()
  {
  }
}
