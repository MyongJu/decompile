﻿// Decompiled with JetBrains decompiler
// Type: GveBossItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class GveBossItemRenderer : MonoBehaviour
{
  public UITexture m_BossIcon;
  public UILabel m_LevelAndName;
  public UILabel m_TroopCount;
  public UIButton m_RallyButton;
  public GameObject m_CollapsedIcon;
  public GameObject m_CollapsedFrame;
  public GameObject m_ExpandIcon;
  public GameObject m_ExpandFrame;
  public GameObject m_ExpandRoot;
  public UITable m_Table;
  public UIGrid m_RewardGrid;
  public GameObject m_RewardPrefab;
  public UIGrid m_TroopGrid;
  public GameObject m_TroopPrefab;
  public UILabel m_RefreshTime;
  private ConfigGveBossData m_BossData;
  private int m_Index;
  private System.Action m_OnChanged;
  private System.Action<int> m_OnRally;
  private bool m_Initialized;
  private bool m_Expand;

  public void SetData(ConfigGveBossData bossData, int index, System.Action onchanged, System.Action<int> onrally)
  {
    this.m_Table.enabled = false;
    this.m_Index = index;
    this.m_BossData = bossData;
    this.m_OnChanged = onchanged;
    this.m_OnRally = onrally;
    this.UpdateInfo();
    this.UpdateDetails();
  }

  public void OnExpandOrCollapse()
  {
    this.m_Expand = !this.m_Expand;
    this.UpdateDetails();
    if (this.m_OnChanged == null)
      return;
    this.m_OnChanged();
  }

  private void UpdateInfo()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_BossIcon, this.m_BossData.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.m_LevelAndName.text = string.Format("Lv.{0} {1}", (object) this.m_BossData.boss_level, (object) ScriptLocalization.Get(this.m_BossData.name, true));
    this.m_TroopCount.text = Utils.FormatThousands(this.m_BossData.TroopCount.ToString());
    this.m_RefreshTime.text = Utils.FormatTime(this.m_BossData.refresh_time, false, false, true);
  }

  private void UpdateDetails()
  {
    this.m_CollapsedIcon.SetActive(!this.m_Expand);
    this.m_CollapsedFrame.SetActive(!this.m_Expand);
    this.m_ExpandIcon.SetActive(this.m_Expand);
    this.m_ExpandFrame.SetActive(this.m_Expand);
    this.m_ExpandRoot.SetActive(this.m_Expand);
    if (!this.m_Expand)
      return;
    this.InitOnce();
  }

  public void OnRally()
  {
    if (this.m_OnRally == null)
      return;
    this.m_OnRally(this.m_Index);
  }

  private void InitOnce()
  {
    if (this.m_Initialized)
      return;
    this.m_Initialized = true;
    this.UpdateRewards(this.m_BossData.BaseRewards);
    this.UpdateRewards(this.m_BossData.KillRewards);
    this.UpdateRewards(this.m_BossData.AdvanceRewards);
    this.UpdateTroops();
    this.m_Table.repositionNow = true;
  }

  private void UpdateTroops()
  {
    List<UnitGroup.UnitRecord> unitList = this.m_BossData.Units.GetUnitList();
    for (int index = 0; index < unitList.Count; ++index)
    {
      GameObject gameObject = Utils.DuplicateGOB(this.m_TroopPrefab, this.m_TroopGrid.transform);
      gameObject.SetActive(true);
      UnitGroup.UnitRecord record = unitList[index];
      gameObject.GetComponent<GveBossTroopRenderer>().SetData(record);
    }
    this.m_TroopGrid.Reposition();
  }

  private void UpdateRewards(Reward rewards)
  {
    List<Reward.RewardsValuePair> rewards1 = rewards.GetRewards();
    for (int index = 0; index < rewards1.Count; ++index)
    {
      GameObject gameObject = Utils.DuplicateGOB(this.m_RewardPrefab, this.m_RewardGrid.transform);
      gameObject.SetActive(true);
      Reward.RewardsValuePair reward = rewards1[index];
      gameObject.GetComponent<GveBossRewardRenderer>().SetData(reward);
    }
    this.m_RewardGrid.Reposition();
  }
}
