﻿// Decompiled with JetBrains decompiler
// Type: TradingHallExpiredMailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;

public class TradingHallExpiredMailPopup : BaseReportPopup
{
  public ItemIconRenderer itemIconRender;
  public UIScrollView reportScrollView;
  public UILabel expiredItemName;
  public UILabel expiredItemCount;
  private UIButton _btnSkip;
  private int _enhanced;

  private void Init()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, true, false, string.Empty);
    this._btnSkip = this.btnCollectionBase.GetComponentInChildren<UIButton>();
    if ((bool) ((UnityEngine.Object) this._btnSkip))
      this._btnSkip.isEnabled = PlayerData.inst.ExchangeSwitch;
    Hashtable hashtable = this.param.mail.data[(object) "data"] as Hashtable;
    if (hashtable != null && hashtable.ContainsKey((object) "exchange_id") && hashtable.ContainsKey((object) "quantity"))
    {
      int result1 = 0;
      int result2 = 0;
      int.TryParse(hashtable[(object) "exchange_id"].ToString(), out result1);
      int.TryParse(hashtable[(object) "quantity"].ToString(), out result2);
      ExchangeHallInfo exchangeHallInfo = ConfigManager.inst.DB_ExchangeHall.Get(result1);
      if (exchangeHallInfo != null)
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(exchangeHallInfo.itemId);
        if (itemStaticInfo != null)
        {
          this.expiredItemName.text = itemStaticInfo.LocName;
          if (exchangeHallInfo.Enhanced > 0)
            this.expiredItemName.text += string.Format("+{0}", (object) exchangeHallInfo.Enhanced);
          this.expiredItemCount.text = "X" + result2.ToString();
          this._enhanced = exchangeHallInfo.Enhanced;
          EquipmentManager.Instance.ConfigQualityLabelWithColor(this.expiredItemName, itemStaticInfo.Quality);
        }
        this.itemIconRender.SetData(exchangeHallInfo.itemId, string.Empty, false);
        NGUITools.SetActive(this.btnCollectionBase, true);
      }
    }
    this.reportScrollView.ResetPosition();
    this.reportScrollView.restrictWithinPanel = true;
    this.reportScrollView.InvalidateBounds();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
  }

  public void OnSkipBtnPressed()
  {
    this.OnCloseBtnPressed();
    TradingHallPayload.Instance.ShowFirstScene(1);
  }

  public new void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnItemPress()
  {
    if ((UnityEngine.Object) this.itemIconRender == (UnityEngine.Object) null)
      return;
    Utils.DelayShowTip(this.itemIconRender.ItemId, this.itemIconRender.transform, 0L, 0L, this._enhanced);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  public void OnItemClick()
  {
    if ((UnityEngine.Object) this.itemIconRender == (UnityEngine.Object) null)
      return;
    Utils.ShowItemTip(this.itemIconRender.ItemId, this.itemIconRender.transform, 0L, 0L, this._enhanced);
  }
}
