﻿// Decompiled with JetBrains decompiler
// Type: ConfigRuralBlock
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigRuralBlock
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, RuralBlockInfo> datas;
  private Dictionary<int, RuralBlockInfo> dicByUniqueId;
  private Dictionary<int, RuralBlockInfo> lev2Data;

  public void BuildDB(object res)
  {
    this.parse.Parse<RuralBlockInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.ParseSlots();
    this.lev2Data = new Dictionary<int, RuralBlockInfo>();
    Dictionary<int, RuralBlockInfo>.KeyCollection.Enumerator enumerator = this.dicByUniqueId.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      RuralBlockInfo ruralBlockInfo = this.dicByUniqueId[enumerator.Current];
      if (!this.lev2Data.ContainsKey(ruralBlockInfo.reqLevel))
        this.lev2Data.Add(ruralBlockInfo.reqLevel, ruralBlockInfo);
    }
  }

  public RuralBlockInfo GetDataByStrongholdLev(int level)
  {
    if (this.lev2Data.ContainsKey(level))
      return this.lev2Data[level];
    return (RuralBlockInfo) null;
  }

  public RuralBlockInfo GetData(int internalId)
  {
    if (this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (RuralBlockInfo) null;
  }

  public RuralBlockInfo GetData(string ID)
  {
    if (this.datas.ContainsKey(ID))
      return this.datas[ID];
    return (RuralBlockInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, RuralBlockInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, RuralBlockInfo>) null;
  }

  private void ParseSlots()
  {
    Dictionary<string, RuralBlockInfo>.Enumerator enumerator = this.datas.GetEnumerator();
    while (enumerator.MoveNext())
    {
      List<int> slots = enumerator.Current.Value.slots;
      slots.Clear();
      string bSlot = enumerator.Current.Value.bSlot;
      char[] chArray = new char[1]{ ',' };
      foreach (string s in bSlot.Split(chArray))
        slots.Add(int.Parse(s));
    }
  }
}
