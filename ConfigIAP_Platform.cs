﻿// Decompiled with JetBrains decompiler
// Type: ConfigIAP_Platform
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigIAP_Platform
{
  private Dictionary<string, IAPPlatformInfo> m_DataByID;
  private Dictionary<int, IAPPlatformInfo> m_DataByInternalID;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<IAPPlatformInfo, string>(result as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
  }

  public IAPPlatformInfo Get(string id)
  {
    IAPPlatformInfo iapPlatformInfo;
    this.m_DataByID.TryGetValue(id, out iapPlatformInfo);
    return iapPlatformInfo;
  }

  public IAPPlatformInfo Get(string platform, string pay_id)
  {
    Dictionary<int, IAPPlatformInfo>.Enumerator enumerator = this.m_DataByInternalID.GetEnumerator();
    while (enumerator.MoveNext())
    {
      IAPPlatformInfo iapPlatformInfo = enumerator.Current.Value;
      if (iapPlatformInfo.platform == platform && iapPlatformInfo.pay_id == pay_id)
        return iapPlatformInfo;
    }
    return (IAPPlatformInfo) null;
  }

  public IAPPlatformInfo Get(RuntimePlatform runtimePlatform, string pay_id)
  {
    string platform = string.Empty;
    switch (runtimePlatform)
    {
      case RuntimePlatform.IPhonePlayer:
        platform = CustomDefine.GetIAPChannel();
        break;
      case RuntimePlatform.Android:
        platform = "googleplayiap";
        break;
    }
    return this.Get(platform, pay_id);
  }

  public List<IAPPlatformInfo> GetAll(string platformName)
  {
    List<IAPPlatformInfo> iapPlatformInfoList = new List<IAPPlatformInfo>();
    using (Dictionary<string, IAPPlatformInfo>.ValueCollection.Enumerator enumerator = this.m_DataByID.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IAPPlatformInfo current = enumerator.Current;
        if (current.platform == platformName)
          iapPlatformInfoList.Add(current);
      }
    }
    return iapPlatformInfoList;
  }

  public IAPPlatformInfo Get(int internalID)
  {
    IAPPlatformInfo iapPlatformInfo;
    this.m_DataByInternalID.TryGetValue(internalID, out iapPlatformInfo);
    return iapPlatformInfo;
  }

  public void Clear()
  {
    this.m_DataByID = (Dictionary<string, IAPPlatformInfo>) null;
    this.m_DataByInternalID = (Dictionary<int, IAPPlatformInfo>) null;
  }
}
