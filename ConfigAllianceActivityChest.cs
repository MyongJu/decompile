﻿// Decompiled with JetBrains decompiler
// Type: ConfigAllianceActivityChest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAllianceActivityChest
{
  private Dictionary<string, AllianceActivityChestInfo> m_DataByUniqueID;
  private Dictionary<int, AllianceActivityChestInfo> m_DataByInternalID;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<AllianceActivityChestInfo, string>(res as Hashtable, "ID", out this.m_DataByUniqueID, out this.m_DataByInternalID);
  }

  public void Clear()
  {
    if (this.m_DataByUniqueID != null)
      this.m_DataByUniqueID.Clear();
    if (this.m_DataByInternalID == null)
      return;
    this.m_DataByInternalID.Clear();
  }

  public AllianceActivityChestInfo GetData(int internalId)
  {
    if (this.m_DataByInternalID.ContainsKey(internalId))
      return this.m_DataByInternalID[internalId];
    return (AllianceActivityChestInfo) null;
  }

  public AllianceActivityChestInfo GetDataByAllianceLevel(int allianceLevel)
  {
    using (Dictionary<string, AllianceActivityChestInfo>.ValueCollection.Enumerator enumerator = this.m_DataByUniqueID.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceActivityChestInfo current = enumerator.Current;
        if (current.AllianceLevel == allianceLevel)
          return current;
      }
    }
    return (AllianceActivityChestInfo) null;
  }
}
