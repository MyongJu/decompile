﻿// Decompiled with JetBrains decompiler
// Type: IAPDailyRechargeHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class IAPDailyRechargeHUD : MonoBehaviour
{
  public GameObject m_RootNode;
  public UILabel m_Timer;

  private void Start()
  {
    this.UpdateUI();
  }

  private void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  private void OnSecondEvent(int serverTimestamp)
  {
    if (NetServerTime.inst.TodayLeftTimeUTC() == 86400 && IAPDailyRechargePackagePayload.Instance.PurchasedPackageState != null)
    {
      IAPDailyRechargePackagePayload.Instance.PurchasedPackageState.all = false;
      IAPDailyRechargePackagePayload.Instance.PurchasedPackageState.bought.Clear();
    }
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this.IsDailyPackageAvailable())
    {
      this.m_RootNode.SetActive(true);
      this.m_Timer.text = Utils.FormatTime(NetServerTime.inst.TodayLeftTimeUTC(), true, false, true);
    }
    else
      this.m_RootNode.SetActive(false);
  }

  private bool IsDailyPackageAvailable()
  {
    return !IAPDailyRechargePackagePayload.Instance.IsSwitchOff && !IAPDailyRechargePackagePayload.Instance.AreAllPackagesPurchased();
  }

  public void OnIAPDailyRechargeBtnPressed()
  {
    UIManager.inst.OpenPopup("IAP/IAPDailyRechargePopup", (Popup.PopupParameter) null);
    OperationTrace.TraceClick(ClickArea.IAPDailyRecharge);
  }
}
