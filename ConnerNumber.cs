﻿// Decompiled with JetBrains decompiler
// Type: ConnerNumber
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ConnerNumber : MonoBehaviour
{
  private bool _isShowed = true;
  public GameObject container;
  public UILabel number;
  private int _value;

  public bool isShowed
  {
    get
    {
      return this._isShowed;
    }
    set
    {
      if (value == this._isShowed)
        return;
      this.container.SetActive(this._isShowed = value);
    }
  }

  public int value
  {
    get
    {
      return this.value;
    }
    set
    {
      this._SetValue(value);
    }
  }

  private void _SetValue(int newValue)
  {
    this._value = newValue;
    this.number.text = this._value.ToString();
  }
}
