﻿// Decompiled with JetBrains decompiler
// Type: AllianceActivityCurrentStepDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceActivityCurrentStepDetail : MonoBehaviour
{
  public UILabel title;
  public UISlider progressSlider;
  public UILabel[] scores;
  public UILabel currentScore;
  private bool init;
  public AllianceActivityRewardContainer containter;
  private ActivityBaseData data;

  public void Clear()
  {
    this.containter.Clear();
  }

  public void UpdateUI(ActivityBaseData data)
  {
    if (this.init)
      return;
    RoundActivityData roundActivityData = data as RoundActivityData;
    this.progressSlider.value = roundActivityData.ProgressValue;
    this.currentScore.text = roundActivityData.CurrentRoundScore.ToString();
    for (int index = 0; index < this.scores.Length; ++index)
      this.scores[index].text = roundActivityData.CurrentRoundTargetScores[index].ToString();
    this.containter.Refresh(data);
    this.init = true;
  }

  public void OnViewPlayerScore()
  {
  }

  public void OnHowToGetPoints()
  {
  }
}
