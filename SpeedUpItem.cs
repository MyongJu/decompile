﻿// Decompiled with JetBrains decompiler
// Type: SpeedUpItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class SpeedUpItem : Icon
{
  public UISprite selectedImage;
  public System.Action<int> OnSelectedDelegate;
  private bool selected;

  public int Item_Id { get; set; }

  public new void OnClickHandler()
  {
    this.Selected = true;
    if (this.OnSelectedDelegate == null)
      return;
    this.OnSelectedDelegate(this.Item_Id);
  }

  public void OnIconPress()
  {
    Utils.DelayShowTip(this.Item_Id, this.mBorder, 0L, 0L, 0);
  }

  public void OnIconRelease()
  {
    Utils.StopShowItemTip();
  }

  public bool Selected
  {
    get
    {
      return this.selected;
    }
    set
    {
      if (this.selected == value)
        return;
      this.selected = value;
      NGUITools.SetActive(this.selectedImage.gameObject, this.selected);
    }
  }
}
