﻿// Decompiled with JetBrains decompiler
// Type: AllianceHighLordPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class AllianceHighLordPanel : MonoBehaviour
{
  public UILabel m_LordTitle;
  public UILabel m_LordName;
  public UILabel m_LordPower;
  public UILabel m_LordLevel;
  public UIButton m_InfoButton;
  public UILabel m_CountDown;
  public UITexture m_Icon;
  private AllianceMemberData m_Member;
  private JobHandle m_Job;
  private System.Action<AllianceMemberData> m_Callback;

  public bool SetDetails(AllianceMemberData member, System.Action<AllianceMemberData> callback)
  {
    this.m_Member = member;
    this.m_Callback = callback;
    bool flag = member != null;
    if (flag)
    {
      this.Show();
      UserData userData = DBManager.inst.DB_User.Get(member.uid);
      CityData byUid = DBManager.inst.DB_City.GetByUid(member.uid);
      this.m_LordName.text = userData.userName;
      this.m_LordPower.text = Utils.FormatThousands(userData.power.ToString());
      this.m_LordLevel.text = byUid.level.ToString();
      CustomIconLoader.Instance.requestCustomIcon(this.m_Icon, userData.PortraitIconPath, userData.Icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.m_Icon, userData.LordTitle, 1);
      this.m_InfoButton.isEnabled = member.uid != GameEngine.Instance.PlayerData.uid;
      this.m_LordTitle.text = PlayerData.inst.allianceData.GetTitleName(member.title);
      this.m_Job = JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_TEMP_HIGHLORD);
    }
    else
      this.Hide();
    return flag;
  }

  private void Show()
  {
    this.gameObject.SetActive(true);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  private void Hide()
  {
    this.gameObject.SetActive(false);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  private void OnSecond(int timestamp)
  {
    if ((UnityEngine.Object) this.m_CountDown == (UnityEngine.Object) null || this.m_Job == null)
      return;
    if (this.m_Member != null && this.m_Member.title == 5)
    {
      int num = this.m_Job.EndTime() - timestamp;
      this.m_CountDown.text = Utils.FormatTime(num <= 0 ? 0 : num, false, false, true);
    }
    else
      this.m_CountDown.text = string.Empty;
  }

  public void OnInfoButtonClicked()
  {
    if (this.m_Callback == null)
      return;
    this.m_Callback(this.m_Member);
  }
}
