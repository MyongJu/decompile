﻿// Decompiled with JetBrains decompiler
// Type: EquipmentTreasuryDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using GemConstant;
using UI;

public class EquipmentTreasuryDlg : UI.Dialog
{
  private int selcetedChipInfoID = -1;
  private int selectedEquipmentID = -1;
  private int selectedGemId = -1;
  public EquipmentTreasuryTabBar tabBar;
  public EquipmentContent equipmentContent;
  public ScrollContent scrollContent;
  public ScrollChipContent scrollChipContent;
  public MaterialContent materialContent;
  public GemContent gemContent;
  public UIButton gemHandbookBtn;
  public UILabel gemHandbookLabel;
  private int currentSelectedIndex;
  private BagType bagType;

  public void Init()
  {
    if (this.selectedEquipmentID != -1)
      this.currentSelectedIndex = 2;
    else if (this.selectedGemId != -1)
      this.currentSelectedIndex = 3;
    this.tabBar.SelectedIndex = this.currentSelectedIndex;
    this.UpdateHandbookBtnLabel();
    this.HandleTabSelected(this.currentSelectedIndex);
    this.AddEventHandler();
  }

  public void Dispose()
  {
    this.RemoveEventHandler();
    this.equipmentContent.Dispose();
    this.scrollContent.Dispose();
    this.scrollChipContent.Dispose();
    this.materialContent.Dispose();
  }

  private void HandleTabSelected(int index)
  {
    this.currentSelectedIndex = index;
    this.equipmentContent.gameObject.SetActive(false);
    this.scrollContent.gameObject.SetActive(false);
    this.scrollChipContent.gameObject.SetActive(false);
    this.materialContent.gameObject.SetActive(false);
    this.gemContent.gameObject.SetActive(false);
    this.gemHandbookBtn.gameObject.SetActive(false);
    this.equipmentContent.Dispose();
    this.scrollContent.Dispose();
    this.scrollChipContent.Dispose();
    this.materialContent.Dispose();
    this.gemContent.Dispose();
    switch (index)
    {
      case 0:
        this.scrollChipContent.gameObject.SetActive(true);
        this.scrollChipContent.Init(this.bagType, this.selcetedChipInfoID);
        break;
      case 1:
        this.scrollContent.gameObject.SetActive(true);
        this.scrollContent.Init(this.bagType);
        break;
      case 2:
        this.equipmentContent.gameObject.SetActive(true);
        this.equipmentContent.Init(this.bagType, this.selectedEquipmentID);
        break;
      case 3:
        this.gemContent.gameObject.SetActive(true);
        this.gemContent.Init(this.bagType, (long) this.selectedGemId);
        this.gemHandbookBtn.gameObject.SetActive(true);
        break;
      case 4:
        this.materialContent.gameObject.SetActive(true);
        this.materialContent.Init(this.bagType);
        break;
    }
  }

  private void AddEventHandler()
  {
    this.tabBar.OnSelectedHandler = new System.Action<int>(this.HandleTabSelected);
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnConsumableCountChanged);
  }

  private void RemoveEventHandler()
  {
    this.tabBar.OnSelectedHandler = (System.Action<int>) null;
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnConsumableCountChanged);
  }

  private void OnConsumableCountChanged(int itemId)
  {
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (orgParam != null)
    {
      this.selcetedChipInfoID = (orgParam as EquipmentTreasuryDlg.Parameter).selcetedChipInfoID;
      this.selectedEquipmentID = (orgParam as EquipmentTreasuryDlg.Parameter).selectedEquipmentID;
      this.selectedGemId = (orgParam as EquipmentTreasuryDlg.Parameter).selectedGemId;
      this.bagType = (orgParam as EquipmentTreasuryDlg.Parameter).bagType;
    }
    this.Init();
  }

  private void UpdateHandbookBtnLabel()
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.gemHandbookLabel))
      return;
    switch (this.bagType)
    {
      case BagType.Hero:
        this.gemHandbookLabel.text = Utils.XLAT("forge_armory_gemstone_display_title");
        break;
      case BagType.DragonKnight:
        this.gemHandbookLabel.text = Utils.XLAT("forge_armory_dk_runestone_display_title");
        break;
    }
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.Dispose();
    base.OnHide(orgParam);
  }

  public void ViewScroll(int targetSelectInfoID)
  {
    this.tabBar.SelectedIndex = 1;
    Utils.ExecuteInSecs(0.1f, (System.Action) (() => this.scrollContent.ViewScroll(targetSelectInfoID)));
  }

  public void OnGemHandbookBtnClicked()
  {
    EquipmentGemHandbookPopup.Parameter parameter = new EquipmentGemHandbookPopup.Parameter();
    switch (this.bagType)
    {
      case BagType.Hero:
        parameter.slotType = SlotType.lord;
        break;
      case BagType.DragonKnight:
        parameter.slotType = SlotType.dragon_knight;
        break;
    }
    UIManager.inst.OpenPopup("Gem/EquipGemHandbookPopup", (Popup.PopupParameter) parameter);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int selcetedChipInfoID = -1;
    public int selectedEquipmentID = -1;
    public int selectedGemId = -1;
    public BagType bagType;
  }
}
