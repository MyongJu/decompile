﻿// Decompiled with JetBrains decompiler
// Type: IRound
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public interface IRound
{
  RoundPlayer Trigger { get; set; }

  List<RoundPlayer> Targets { get; set; }

  List<RoundResult> Results { get; set; }

  bool IsFinish { get; set; }

  bool IsRunning { get; set; }

  long SkillId { get; set; }

  void Process();

  void Play();

  void Destroy();

  int GUID { get; set; }

  void NormalAttack();

  void Spell();

  void UnderAttack();

  void Finish();

  void MarkFinish();

  Hashtable GetValidateData();

  int RoundID { get; set; }

  void SetData(string key, object data);

  T GetData<T>(string key);
}
