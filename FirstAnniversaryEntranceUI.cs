﻿// Decompiled with JetBrains decompiler
// Type: FirstAnniversaryEntranceUI
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class FirstAnniversaryEntranceUI : MonoBehaviour
{
  public void OnEntranceClicked()
  {
    if (SuperLoginPayload.Instance.CanActivityShow("super_log_in_1_year_celebration"))
      UIManager.inst.OpenPopup("SuperLogin/FirstAnniversaryPopup", (Popup.PopupParameter) null);
    OperationTrace.TraceClick(ClickArea.FirstAnniversary);
  }
}
