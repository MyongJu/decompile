﻿// Decompiled with JetBrains decompiler
// Type: QuestDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class QuestDetail : MonoBehaviour
{
  private List<QuestRewardSlotEmpire> _items = new List<QuestRewardSlotEmpire>();
  public UILabel questInstructionsTitle;
  public UILabel questInstructions;
  public QuestRewardSlotEmpire rewardPrefab;
  public UIGrid rewardContainer;
  public QuestIconRenderer iconRenderer;
  public UIScrollView scrollView;
  private QuestData2 questData;
  private bool addedHandler;
  private bool needToReset;

  private void OnSecondEvent(int serverTime)
  {
  }

  private void DestroyRewards()
  {
    if (this._items.Count <= 0)
      return;
    for (int index = 0; index < this._items.Count; ++index)
    {
      QuestRewardSlotEmpire rewardSlotEmpire = this._items[index];
      rewardSlotEmpire.Clear();
      NGUITools.SetActive(rewardSlotEmpire.gameObject, false);
      rewardSlotEmpire.transform.parent = this.transform;
      UnityEngine.Object.Destroy((UnityEngine.Object) rewardSlotEmpire.gameObject);
    }
    this._items.Clear();
  }

  private void Setup()
  {
    this.questInstructionsTitle.text = ScriptLocalization.Get(this.questData.Info.Loc_Name, true);
    this.questInstructions.text = ScriptLocalization.Get(this.questData.Info.Loc_Desc, true);
    this.iconRenderer.SetData(this.questData.QuestID);
    this.DestroyRewards();
    for (int index = 0; index < this.questData.Rewards.Count; ++index)
    {
      QuestReward reward = this.questData.Rewards[index];
      GameObject gameObject = NGUITools.AddChild(this.rewardContainer.gameObject, this.rewardPrefab.gameObject);
      QuestRewardSlotEmpire component = gameObject.GetComponent<QuestRewardSlotEmpire>();
      gameObject.SetActive(true);
      component.Setup(this.questData.Rewards[index]);
      this._items.Add(component);
    }
    this.rewardContainer.repositionNow = true;
    this.rewardContainer.Reposition();
    this.scrollView.ResetPosition();
    this.RemoveEventHandlr();
    this.AddEventHandler();
  }

  private void AddEventHandler()
  {
    if (this.addedHandler)
      return;
    this.addedHandler = true;
    PlayerData.inst.QuestManager.OnQuestCompleted += new System.Action<long>(this.Refresh);
  }

  private void Refresh(long questId)
  {
    if (questId == this.questData.QuestID)
      this.Setup();
    this.needToReset = true;
  }

  public void SetData(long questId)
  {
    this.questData = DBManager.inst.DB_Quest.Get(questId);
    if (this.questData == null)
      return;
    this.Setup();
  }

  private void RemoveEventHandlr()
  {
    if (!this.addedHandler)
      return;
    this.addedHandler = false;
    PlayerData.inst.QuestManager.OnQuestCompleted -= new System.Action<long>(this.Refresh);
  }

  public void Clear()
  {
    this.DestroyRewards();
    this.RemoveEventHandlr();
  }
}
