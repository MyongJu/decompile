﻿// Decompiled with JetBrains decompiler
// Type: EquipmentForgeContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentForgeContent : MonoBehaviour
{
  public GameObject template;
  public UIGrid grid;
  public UIScrollView scrollView;
  public EquipmentForgeInfo info;
  public UILabel gotoBtnLabel;
  public int currentSelectInfoID;
  public EquipmentScrollComponent current;
  private List<EquipmentScrollComponent> contentList;
  private List<ConfigEquipmentScrollInfo> infos;
  private BagType _bagType;

  public void Init(BagType bagType, List<ConfigEquipmentScrollInfo> infos, bool clear = false, int targetSelectInfoID = 0)
  {
    this._bagType = bagType;
    this.infos = infos;
    if (targetSelectInfoID != 0)
      this.currentSelectInfoID = targetSelectInfoID;
    this.current = (EquipmentScrollComponent) null;
    if (clear)
      this.currentSelectInfoID = 0;
    this.ClearGrid();
    this.contentList = new List<EquipmentScrollComponent>();
    for (int index = 0; index < infos.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.template);
      gameObject.SetActive(true);
      EquipmentScrollComponent component = gameObject.GetComponent<EquipmentScrollComponent>();
      component.OnSelectedHandler = new System.Action<EquipmentScrollComponent>(this.OnSelectedHandler);
      this.contentList.Add(component);
      component.FeedData((IComponentData) new EquipmentScrollComponetData(infos[index], this._bagType));
      if (component.scrollInfo.internalId == this.currentSelectInfoID)
      {
        this.current = component;
        this.current.Select = true;
      }
    }
    if (this.contentList.Count > 0)
    {
      if ((UnityEngine.Object) this.current == (UnityEngine.Object) null)
      {
        this.current = this.contentList[0];
        this.current.Select = true;
        this.currentSelectInfoID = this.current.scrollInfo.internalId;
      }
      this.info.Init(this._bagType, this.current.scrollInfo.internalId);
    }
    else
      this.info.Init(this._bagType, 0);
    this.grid.Reposition();
    this.scrollView.ResetPosition();
    if ((UnityEngine.Object) this.current != (UnityEngine.Object) null)
      this.CenterOn(this.current.transform);
    this.ConfigGotBtn();
    DBManager.inst.GetInventory(this._bagType).onDataRemoved -= new System.Action<long, long>(this.OnInventoryChanged);
    DBManager.inst.GetInventory(this._bagType).onDataRemoved += new System.Action<long, long>(this.OnInventoryChanged);
    DBManager.inst.GetInventory(this._bagType).onDataChanged -= new System.Action<long, long>(this.OnInventoryChanged);
    DBManager.inst.GetInventory(this._bagType).onDataChanged += new System.Action<long, long>(this.OnInventoryChanged);
  }

  private void ConfigGotBtn()
  {
    if ((UnityEngine.Object) this.gotoBtnLabel == (UnityEngine.Object) null)
      return;
    if (DBManager.inst.GetInventory(this._bagType).HasMyEquipment(this.current.scrollInfo.equipmenID))
      this.gotoBtnLabel.text = Utils.XLAT("forge_uppercase_go_to_enhance_button");
    else
      this.gotoBtnLabel.text = Utils.XLAT("forge_uppercase_go_to_forge_button");
  }

  private void OnInventoryChanged(long uid, long itemId)
  {
    this.Init(this._bagType, this.infos, false, this.currentSelectInfoID);
  }

  public void Dispose()
  {
    DBManager.inst.GetInventory(this._bagType).onDataRemoved -= new System.Action<long, long>(this.OnInventoryChanged);
    DBManager.inst.GetInventory(this._bagType).onDataChanged -= new System.Action<long, long>(this.OnInventoryChanged);
    this.info.Dispose();
  }

  private void OnSelectedHandler(EquipmentScrollComponent selected)
  {
    if (!((UnityEngine.Object) this.current != (UnityEngine.Object) null) || this.current.scrollInfo.internalId == selected.scrollInfo.internalId)
      return;
    this.currentSelectInfoID = selected.scrollInfo.internalId;
    this.current.Select = false;
    this.current = selected;
    this.current.Select = true;
    this.info.Init(this._bagType, this.current.scrollInfo.internalId);
    this.ConfigGotBtn();
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.grid);
  }

  public void CenterOn(Transform target)
  {
    if (!((UnityEngine.Object) this.scrollView != (UnityEngine.Object) null) || !((UnityEngine.Object) this.scrollView.panel != (UnityEngine.Object) null))
      return;
    Vector3[] worldCorners = this.scrollView.panel.worldCorners;
    Vector3 panelBottom = (worldCorners[1] + worldCorners[2]) * 0.5f;
    this.CenterOn(target, panelBottom);
  }

  private void CenterOn(Transform target, Vector3 panelBottom)
  {
    if (!((UnityEngine.Object) target != (UnityEngine.Object) null) || !((UnityEngine.Object) this.scrollView != (UnityEngine.Object) null) || (!((UnityEngine.Object) this.scrollView.panel != (UnityEngine.Object) null) || !(this.scrollView.currentMomentum == Vector3.zero)))
      return;
    Transform cachedTransform = this.scrollView.panel.cachedTransform;
    Vector3 vector3 = cachedTransform.InverseTransformPoint(target.position) - cachedTransform.InverseTransformPoint(panelBottom);
    if (!this.scrollView.canMoveHorizontally)
      vector3.x = 0.0f;
    if (!this.scrollView.canMoveVertically)
      vector3.y = 0.0f;
    vector3.z = 0.0f;
    vector3.y += this.grid.cellHeight / 2f;
    cachedTransform.localPosition -= vector3;
    Vector4 clipOffset = (Vector4) this.scrollView.panel.clipOffset;
    clipOffset.x += vector3.x;
    clipOffset.y += vector3.y;
    this.scrollView.panel.clipOffset = (Vector2) clipOffset;
    this.scrollView.RestrictWithinBounds(true);
  }
}
