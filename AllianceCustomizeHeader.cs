﻿// Decompiled with JetBrains decompiler
// Type: AllianceCustomizeHeader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class AllianceCustomizeHeader : MonoBehaviour
{
  public GameObject m_Collapsed;
  public GameObject m_Expanded;
  private bool m_IsExpanded;
  private System.Action m_OnChanged;
  private AllianceData m_AllianceData;

  public bool IsExpanded
  {
    get
    {
      return this.m_IsExpanded;
    }
  }

  public void SetData(AllianceData allianceData, bool expand, System.Action onChanged)
  {
    this.m_AllianceData = allianceData;
    this.m_IsExpanded = expand;
    this.m_OnChanged = onChanged;
    this.UpdatePanel();
    this.UpdateUI();
  }

  protected void FireChangedEvent()
  {
    if (this.m_OnChanged == null)
      return;
    this.m_OnChanged();
  }

  public void OnHeaderClick()
  {
    this.m_IsExpanded = !this.m_IsExpanded;
    this.UpdatePanel();
    this.FireChangedEvent();
  }

  public AllianceData allianceData
  {
    get
    {
      return this.m_AllianceData;
    }
  }

  protected virtual void UpdatePanel()
  {
    this.m_Collapsed.SetActive(!this.m_IsExpanded);
    this.m_Expanded.SetActive(this.m_IsExpanded);
  }

  protected virtual void UpdateUI()
  {
  }
}
