﻿// Decompiled with JetBrains decompiler
// Type: ReinforcePlayerWithTierComponentData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ReinforcePlayerWithTierComponentData : IComponentData
{
  public string name;
  public string portrait;
  public string icon;
  public int lordTitleId;
  public string acronym;
  public string level;
  public string amount;
  public Hashtable dragon;
  public List<TierComponentData> tiers;

  public ReinforcePlayerWithTierComponentData(List<TierComponentData> tiers, Hashtable data)
  {
    this.tiers = tiers;
    this.name = !data.ContainsKey((object) nameof (name)) ? string.Empty : data[(object) nameof (name)].ToString();
    this.portrait = !data.ContainsKey((object) nameof (portrait)) ? "0" : data[(object) nameof (portrait)].ToString();
    this.icon = !data.ContainsKey((object) nameof (icon)) ? "0" : data[(object) nameof (icon)].ToString();
    this.acronym = !data.ContainsKey((object) nameof (acronym)) ? string.Empty : data[(object) nameof (acronym)].ToString();
    this.level = !data.ContainsKey((object) "lord_level") ? "0" : data[(object) "lord_level"].ToString();
    this.amount = !data.ContainsKey((object) nameof (amount)) ? string.Empty : data[(object) nameof (amount)].ToString();
    this.dragon = !data.ContainsKey((object) nameof (dragon)) ? (Hashtable) null : data[(object) nameof (dragon)] as Hashtable;
    if (!data.ContainsKey((object) "lord_title"))
      return;
    int.TryParse(data[(object) "lord_title"].ToString(), out this.lordTitleId);
  }

  public ReinforcePlayerWithTierComponentData()
  {
  }
}
