﻿// Decompiled with JetBrains decompiler
// Type: AchievementItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class AchievementItemRenderer : MonoBehaviour
{
  public System.Action OnResortHandler;
  public QuestIconRenderer icon;
  public UILabel achievementName;
  public UILabel desc;
  public UIButton claim;
  public UIProgressBar progressValue;
  public UILabel progressContent;
  public UILabel rewardCount;
  public GameObject finishGo;
  public GameObject rewards;
  public StarGroup stars;
  public Icon rewardItemIcon;
  public UILabel lordTitleTip;
  private int achievementid;

  public int AchievementId { get; private set; }

  public int GroupId { get; private set; }

  public void SetData(int id)
  {
    this.achievementid = id;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    AchievementInfo data1 = ConfigManager.inst.DB_Achievement.GetData(this.achievementid);
    AchievementMainInfo data2 = ConfigManager.inst.DB_AchievementMain.GetData(data1.GroupID);
    this.AchievementId = this.achievementid;
    this.GroupId = data1.GroupID;
    BasePresent basePresentById = Utils.GetBasePresentByID((long) this.achievementid);
    basePresentById.Refresh();
    string[] strArray = basePresentById.GetProgressContent().Split('/');
    string empty = string.Empty;
    if (strArray.Length > 1)
      empty = strArray[1];
    if (empty == "1" && data1.Value > 1)
      empty = data1.Value.ToString();
    this.icon.SetData((long) this.achievementid);
    this.achievementName.text = ScriptLocalization.Get(data2.LocName, true);
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("1", empty);
    if (data2.Category == "equip")
    {
      int num = data1.Formula.IndexOf('=');
      if (num > -1)
      {
        string str = ScriptLocalization.Get("equip_color_" + data1.Formula.Substring(num + 1, data1.Formula.Length - (num + 1)), true);
        para.Add("2", str);
      }
    }
    this.desc.text = ScriptLocalization.GetWithPara(data2.LocDesc, para, true);
    this.rewardCount.text = data1.ItemCount.ToString();
    AchievementInfo itemAchievementInfo = ConfigManager.inst.DB_Achievement.GetLordItemAchievementInfo(data1);
    if (itemAchievementInfo != null && itemAchievementInfo.Lord_Item > 0 && itemAchievementInfo.Star >= data1.Star)
    {
      IconData iconData = new IconData(ConfigManager.inst.DB_Items.GetItem(itemAchievementInfo.Lord_Item).ImagePath, new string[1]
      {
        itemAchievementInfo.LordItemCount.ToString()
      });
      iconData.Data = (object) itemAchievementInfo.Lord_Item;
      this.rewardItemIcon.StopAutoFillName();
      this.rewardItemIcon.StartAutoTip();
      this.rewardItemIcon.FeedData((IComponentData) iconData);
      this.lordTitleTip.text = ScriptLocalization.GetWithPara("achievement_star_reward_subtitle", new Dictionary<string, string>()
      {
        {
          "0",
          itemAchievementInfo.Star.ToString()
        }
      }, true);
    }
    else
      this.rewardItemIcon.gameObject.SetActive(false);
    if (!DBManager.inst.DB_Achievements.Get(this.achievementid).IsClaim)
    {
      NGUITools.SetActive(this.rewards, true);
      NGUITools.SetActive(this.finishGo, false);
      this.RefreshProgress();
      this.RemoveEventHandler();
      this.AddEventHandler();
    }
    else
    {
      this.RemoveEventHandler();
      this.FinishHandler();
    }
  }

  public void OnClaimHandler()
  {
    this.RemoveEventHandler();
    NGUITools.SetActive(this.rewards, false);
    RewardsCollectionAnimator.Instance.Clear();
    AchievementInfo data = ConfigManager.inst.DB_Achievement.GetData(this.achievementid);
    if (data.Item_ID > 0)
    {
      int itemId = data.Item_ID;
      int itemCount = data.ItemCount;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
      RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
      {
        count = (float) itemCount,
        icon = itemStaticInfo.ImagePath
      });
    }
    if (data.Lord_Item > 0)
    {
      int lordItem = data.Lord_Item;
      int lordItemCount = data.LordItemCount;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(lordItem);
      RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
      {
        count = (float) lordItemCount,
        icon = itemStaticInfo.ImagePath
      });
    }
    RequestManager.inst.SendRequest("Achievement:collectReward", Utils.Hash((object) "city_id", (object) PlayerData.inst.cityId, (object) "achievement_id", (object) this.achievementid), (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      AudioManager.Instance.PlaySound("sfx_claim", false);
      RewardsCollectionAnimator.Instance.CollectItems(false);
      if (this.OnResortHandler == null)
        return;
      this.OnResortHandler();
    }), true);
  }

  private void OnDisable()
  {
    this.RemoveEventHandler();
  }

  public void Clear()
  {
    this.RemoveEventHandler();
  }

  private void AddEventHandler()
  {
    if (!GameEngine.IsReady())
      return;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  private void Process(int tiem)
  {
    this.RefreshProgress();
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
  }

  private void FinishHandler()
  {
    AchievementData achievementData = DBManager.inst.DB_Achievements.Get(this.achievementid);
    this.stars.SetCount(achievementData.MaxStarCount);
    NGUITools.SetActive(this.claim.gameObject, false);
    NGUITools.SetActive(this.rewards, false);
    NGUITools.SetActive(this.finishGo, true);
    BasePresent present = achievementData.Present;
    if (present == null)
      return;
    this.progressValue.value = present.GetProgressBarValue();
    this.progressContent.text = present.GetProgressContent();
  }

  private void RefreshProgress()
  {
    AchievementData achievementData = DBManager.inst.DB_Achievements.Get(this.achievementid);
    if (achievementData.IsClaim)
    {
      this.RemoveEventHandler();
      this.FinishHandler();
    }
    else
    {
      AchievementInfo data = ConfigManager.inst.DB_Achievement.GetData(this.achievementid);
      this.stars.SetCount(achievementData.CurrentStarCount);
      if (achievementData == null)
        return;
      this.rewardCount.text = data.ItemCount.ToString();
      BasePresent present = achievementData.Present;
      if (present == null)
        return;
      this.progressValue.value = present.GetProgressBarValue();
      this.progressContent.text = present.GetProgressContent();
      NGUITools.SetActive(this.claim.gameObject, present.IsFinish);
      if (!present.IsFinish)
        return;
      present.Refresh();
      this.transform.parent.gameObject.GetComponent<UIGrid>().Reposition();
    }
  }
}
