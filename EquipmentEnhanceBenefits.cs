﻿// Decompiled with JetBrains decompiler
// Type: EquipmentEnhanceBenefits
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class EquipmentEnhanceBenefits : MonoBehaviour
{
  public UIGrid grid;
  public EquipmentEnhanceBenefitComponent template;
  private List<EquipmentEnhanceBenefitComponent> benefitsComponentList;

  public void Init(Benefits currentLevel, Benefits nextLevel)
  {
    this.ClearGrid();
    List<Benefits.BenefitValuePair> benefitsPairs = currentLevel.GetBenefitsPairs();
    this.benefitsComponentList = new List<EquipmentEnhanceBenefitComponent>();
    for (int index = 0; index < benefitsPairs.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.template.gameObject);
      gameObject.SetActive(true);
      EquipmentEnhanceBenefitComponent component = gameObject.GetComponent<EquipmentEnhanceBenefitComponent>();
      component.FeedData((IComponentData) new EquipmentEnhanceBenefitComponentData(benefitsPairs[index], nextLevel == null ? (Benefits.BenefitValuePair) null : nextLevel.GetBenefitsPairs()[index]));
      this.benefitsComponentList.Add(component);
    }
    this.grid.repositionNow = true;
  }

  public void Dispose()
  {
    if (this.benefitsComponentList == null)
      return;
    this.benefitsComponentList.ForEach((System.Action<EquipmentEnhanceBenefitComponent>) (obj => obj.Dispose()));
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.grid);
  }
}
