﻿// Decompiled with JetBrains decompiler
// Type: UseItemCannotGoldPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;
using UnityEngine;

public class UseItemCannotGoldPopup : Popup
{
  public UILabel titleLabel;
  public UILabel itemNameLabel;
  public UILabel itemDescLabel;
  public UITexture itemIconTexture;
  public UITexture backgroud;
  public Transform border;
  public UIButton actionBtn;
  public UILabel ownedValueLabel;
  public UILabel actionBtnLabel;
  private UseItemCannotGoldPopup.Parameter m_Parameter;
  private ItemStaticInfo itemStaticInfo;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as UseItemCannotGoldPopup.Parameter;
    this.itemStaticInfo = this.m_Parameter.itemStaticInfo;
    this.UpdateUI();
  }

  public void OnItemPress()
  {
    Utils.DelayShowTip(this.itemStaticInfo.internalId, this.border, 0L, 0L, 0);
  }

  public void OnItemRelese()
  {
    Utils.StopShowItemTip();
  }

  public void OnItemClick()
  {
    Utils.ShowItemTip(this.itemStaticInfo.internalId, this.border, 0L, 0L, 0);
  }

  private void UpdateUI()
  {
    this.titleLabel.text = this.m_Parameter.titleText;
    this.actionBtnLabel.text = this.m_Parameter.btnText;
    this.itemNameLabel.text = this.itemStaticInfo.LocName;
    Utils.SetItemBackground(this.backgroud, this.itemStaticInfo.internalId);
    Utils.SetItemName(this.itemNameLabel, this.itemStaticInfo.internalId);
    this.itemDescLabel.text = this.m_Parameter.contentText;
    int itemCount = ItemBag.Instance.GetItemCount(this.itemStaticInfo.internalId);
    this.ownedValueLabel.text = (itemCount >= this.m_Parameter.needvalue ? "[00FF00FF]" : "[FF0000FF]") + (object) itemCount + "[-]/" + (object) this.m_Parameter.needvalue;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.itemIconTexture, this.m_Parameter.itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    if (itemCount < this.m_Parameter.needvalue)
    {
      this.actionBtn.onClick.Clear();
      this.actionBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnIAPBtnPressed)));
    }
    else
    {
      this.actionBtn.onClick.Clear();
      this.actionBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnUseBtnPressed)));
    }
  }

  public void OnIAPBtnPressed()
  {
    this.OpenIAPStore();
  }

  public void OpenIAPStore()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    UIManager.inst.publicHUD.OnRunesClicked();
  }

  public void OnUseBtnPressed()
  {
    ItemBag.Instance.UseItem(this.itemStaticInfo.ID, this.m_Parameter.param, (System.Action<bool, object>) ((arg1, arg2) => this.OnUse(arg1, arg2)), this.m_Parameter.needvalue);
  }

  public void OnUse(bool arg1, object arg2)
  {
    if (this.m_Parameter.callback != null)
      this.m_Parameter.callback(arg1, arg2);
    this.OnCloseButtonClick();
  }

  public void OnCloseButtonClick()
  {
    BuilderFactory.Instance.Release((UIWidget) this.itemIconTexture);
    this.actionBtn.onClick.Clear();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string titleText;
    public string contentText;
    public string btnText;
    public int needvalue;
    public Hashtable param;
    public ItemStaticInfo itemStaticInfo;
    public System.Action<bool, object> callback;
  }
}
