﻿// Decompiled with JetBrains decompiler
// Type: AllianceInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class AllianceInfo : ComponentRenderBase
{
  public Icon lordinfo;
  public AllianceSymbol alliance;
  public UILabel alliancename;
  public UILabel power;
  public UILabel member;
  public UILabel language;

  public override void Init()
  {
    base.Init();
    AllianceInfoData data = this.data as AllianceInfoData;
    this.alliance.SetSymbols(int.Parse(data.symbol_code));
    this.alliancename.text = string.Format("({0}) {1}", (object) data.acronym, (object) data.name);
    this.power.text = Utils.FormatThousands(data.power);
    this.member.text = string.Format("{0}/{1}", (object) data.member_count, (object) data.member_max);
    this.lordinfo.FeedData((IComponentData) new IconData(UserData.GetPortraitIconPath(int.Parse(data.lord_portrait)), new string[1]
    {
      data.lord_name
    }));
    this.language.text = Language.Instance.GetLanguageName(data.language);
  }

  public void ShowAllianceSymbol()
  {
    if (!(bool) ((Object) this.alliance))
      return;
    this.alliance.gameObject.SetActive(true);
  }

  public void HideAllianceSymbol()
  {
    if (!(bool) ((Object) this.alliance))
      return;
    this.alliance.gameObject.SetActive(false);
  }
}
