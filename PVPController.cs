﻿// Decompiled with JetBrains decompiler
// Type: PVPController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using March;
using System.Collections;
using System.Collections.Generic;
using Troops;
using UI;
using UnityEngine;

public class PVPController : MonoBehaviour
{
  private TroopViewPool m_TroopViewPool = new TroopViewPool();
  public const string SHOW_KINGDOM_INFO = "KingdomInfo";
  public PVPMap Map;
  private DynamicMapData m_MapData;
  private bool m_Ready;

  public TroopViewPool troopViewManager
  {
    get
    {
      return this.m_TroopViewPool;
    }
  }

  public bool IsReady
  {
    get
    {
      return this.m_Ready;
    }
  }

  public void Setup(DynamicMapData mapData)
  {
    this.RegisterListeners();
    this.m_TroopViewPool.CreateTroopViews(this.transform);
    this.Map.Setup(mapData);
    this.m_MapData = mapData;
  }

  public void Shutdown()
  {
    this.UnregisterListeners();
    this.Map.Shutdown();
    this.m_MapData = (DynamicMapData) null;
    this.m_Ready = false;
  }

  public void Show(bool focusOnMyCity)
  {
    UIManager.inst.publicHUD.SwitchToPart(GAME_LOCATION.KINGDOM);
    UIManager.inst.tileCamera.ActiveKingdomOnly = true;
    AudioManager.Instance.PlayBGM("bgm_kingdom2");
    UIManager.inst.tileCamera.gameObject.SetActive(true);
    this.m_MapData.OnRequestKingdomBlock += new System.Action(this.OnRequestKingdomBlock);
    this.m_MapData.OnEnterKingdomBlock += new System.Action(this.OnEnterKingdomBlock);
    this.Map.CurrentKXY = new Coordinate();
    this.Map.ForceUpdate = true;
    if (!focusOnMyCity || MapUtils.IsPitWorld(PlayerData.inst.CityData.Location.K))
      return;
    this.Map.GotoLocation(PlayerData.inst.CityData.Location, false);
  }

  public void Hide()
  {
    UIManager.inst.tileCamera.gameObject.SetActive(false);
    this.m_MapData.OnRequestKingdomBlock -= new System.Action(this.OnRequestKingdomBlock);
    this.m_MapData.OnEnterKingdomBlock -= new System.Action(this.OnEnterKingdomBlock);
    this.m_Ready = false;
    UIManager.inst.CloseKingdomTouchCircle();
  }

  private void RegisterListeners()
  {
    DBManager.inst.DB_City.onDataChanged += new System.Action<long>(this.OnCityDataChanged);
    DBManager.inst.DB_ZoneBorder.onCreated += new System.Action<ZoneBorderData>(this.OnFortressBorderUpdated);
    DBManager.inst.DB_ZoneBorder.onUpdated += new System.Action<ZoneBorderData>(this.OnFortressBorderUpdated);
    DBManager.inst.DB_AllianceFortress.onDataRemoved += new System.Action<AllianceFortressData>(this.OnFortressDataRemoved);
    DBManager.inst.DB_AllianceFortress.onDataUpdate += new System.Action<AllianceFortressData>(this.OnFortressUpdated);
    DBManager.inst.DB_AllianceHospital.onDataChanged += new System.Action<AllianceHospitalData>(this.OnHospitalDataChanged);
    DBManager.inst.DB_AllianceBoss.onDataChanged += new System.Action<AllianceBossData>(this.OnBossDataChanged);
    DBManager.inst.DB_AllianceTemple.onDataChanged += new System.Action<AllianceTempleData>(this.OnTempleDataChanged);
  }

  private void UnregisterListeners()
  {
    DBManager.inst.DB_City.onDataChanged -= new System.Action<long>(this.OnCityDataChanged);
    DBManager.inst.DB_ZoneBorder.onCreated -= new System.Action<ZoneBorderData>(this.OnFortressBorderUpdated);
    DBManager.inst.DB_ZoneBorder.onUpdated -= new System.Action<ZoneBorderData>(this.OnFortressBorderUpdated);
    DBManager.inst.DB_AllianceFortress.onDataRemoved -= new System.Action<AllianceFortressData>(this.OnFortressDataRemoved);
    DBManager.inst.DB_AllianceFortress.onDataUpdate -= new System.Action<AllianceFortressData>(this.OnFortressUpdated);
    DBManager.inst.DB_AllianceHospital.onDataChanged -= new System.Action<AllianceHospitalData>(this.OnHospitalDataChanged);
    DBManager.inst.DB_AllianceBoss.onDataChanged -= new System.Action<AllianceBossData>(this.OnBossDataChanged);
    DBManager.inst.DB_AllianceTemple.onDataChanged -= new System.Action<AllianceTempleData>(this.OnTempleDataChanged);
  }

  private void OnTempleDataChanged(AllianceTempleData templeData)
  {
    if (templeData == null)
      return;
    TileData tileAt = this.m_MapData.GetTileAt(templeData.Location);
    if (tileAt == null)
      return;
    tileAt.UpdateUI();
  }

  private void OnCityDataChanged(long cityid)
  {
    PVPTile pvpTile = this.m_MapData.GetPVPTile(DBManager.inst.DB_City.Get(cityid).cityLocation);
    if (pvpTile == null)
      return;
    pvpTile.UpdateVFX();
    pvpTile.UpdateUI();
  }

  private void OnFortressBorderUpdated(ZoneBorderData borderData)
  {
    TileData tileAt = this.m_MapData.GetTileAt(borderData.Location);
    if (tileAt == null)
      return;
    tileAt.BuildZoneBorderMesh(borderData);
  }

  private void OnFortressUpdated(AllianceFortressData fortressData)
  {
    TileData tileAt = this.m_MapData.GetTileAt(fortressData.Location);
    if (tileAt == null)
      return;
    tileAt.UpdateUI();
  }

  private void OnHospitalDataChanged(AllianceHospitalData hospitalData)
  {
    TileData tileAt = this.m_MapData.GetTileAt(hospitalData.Location);
    if (tileAt == null)
      return;
    tileAt.UpdateUI();
  }

  private void OnBossDataChanged(AllianceBossData bossData)
  {
    AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
    if (alliancePortalData == null)
      return;
    TileData tileAt = this.m_MapData.GetTileAt(alliancePortalData.Location);
    if (tileAt == null)
      return;
    tileAt.UpdateUI();
  }

  private void OnFortressDataRemoved(AllianceFortressData fortressData)
  {
    TileData tileAt = this.m_MapData.GetTileAt(fortressData.Location);
    if (tileAt == null)
      return;
    tileAt.OnDestroyFortress();
  }

  public void AddAttacker(long marchId, Coordinate location)
  {
    TileData referenceAt = this.m_MapData.GetReferenceAt(location);
    if (referenceAt == null)
      return;
    referenceAt.AddAttacker(marchId);
  }

  public void RemoveAttacker(long marchId, Coordinate location)
  {
    TileData referenceAt = this.m_MapData.GetReferenceAt(location);
    if (referenceAt == null)
      return;
    referenceAt.RemoveAttacker(marchId);
  }

  private void OnRequestKingdomBlock()
  {
    UIManager.inst.publicHUD.KingdomSpin.SetActive(true);
  }

  private void OnEnterKingdomBlock()
  {
    GameEngine.Instance.marchSystem.LoadScene();
    if (UIManager.inst.publicHUD.KingdomSpin.activeSelf)
      Utils.ExecuteInSecs(0.5f, (System.Action) (() =>
      {
        try
        {
          UIManager.inst.publicHUD.KingdomSpin.SetActive(false);
        }
        catch
        {
        }
      }));
    this.m_Ready = true;
    if (TutorialManager.Instance.IsRunning || PlayerPrefs.HasKey("KingdomInfo"))
      return;
    UIManager.inst.OpenPopup("InfoKingdom", (Popup.PopupParameter) new InfoKingdom.Parameter());
    PlayerPrefs.SetInt("KingdomInfo", 1);
  }

  public void Attack(Coordinate target, MarchAllocDlg.MarchType inMarchType)
  {
    AudioManager.Instance.PlaySound("sfx_kingdom_click_player_attack", false);
    UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
    {
      location = target,
      marchType = inMarchType
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void AttackCurTile()
  {
    if (DBManager.inst.DB_March.marchOwner.Count >= PlayerData.inst.playerCityData.marchSlot)
    {
      GameEngine.Instance.marchSystem.ShowMaxMarchSlotMessage();
    }
    else
    {
      switch (this.Map.SelectedTile.TileType)
      {
        case TileType.City:
          this.Attack(this.Map.SelectedTile.Location, MarchAllocDlg.MarchType.cityAttack);
          break;
        case TileType.Camp:
          this.Attack(this.Map.SelectedTile.Location, MarchAllocDlg.MarchType.encampAttack);
          break;
        case TileType.Resource1:
        case TileType.Resource2:
        case TileType.Resource3:
        case TileType.Resource4:
        case TileType.PitMine:
          this.Attack(this.Map.SelectedTile.Location, MarchAllocDlg.MarchType.gatherAttack);
          break;
        case TileType.Wonder:
        case TileType.WonderTower:
          if (GameEngine.Instance.marchSystem.HasMarchToWonder())
          {
            UIManager.inst.toast.Show(ScriptLocalization.Get("throne_invasion_existing_garrison_march", true), (System.Action) null, 4f, false);
            break;
          }
          this.Attack(this.Map.SelectedTile.Location, MarchAllocDlg.MarchType.wonderAttack);
          break;
        case TileType.AllianceFortress:
          this.Attack(this.Map.SelectedTile.Location, MarchAllocDlg.MarchType.fortressAttack);
          break;
        case TileType.AllianceTemple:
          this.Attack(this.Map.SelectedTile.Location, MarchAllocDlg.MarchType.templeAttack);
          break;
        case TileType.Digsite:
          this.Attack(this.Map.SelectedTile.Location, MarchAllocDlg.MarchType.digAttack);
          break;
      }
    }
  }

  public void Gather(Coordinate target)
  {
    UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
    {
      location = target,
      marchType = MarchAllocDlg.MarchType.gather
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void GatherCurTile()
  {
    AllianceSuperMineData dataByCoordinate = DBManager.inst.DB_AllianceSuperMine.GetDataByCoordinate(this.Map.SelectedTile.Location);
    if (dataByCoordinate != null)
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_" + ConfigManager.inst.DB_AllianceBuildings.Get(dataByCoordinate.ConfigId).ResourceType);
      if (data != null && data.ValueInt > PlayerData.inst.CityData.mStronghold.mLevel)
      {
        Dictionary<string, string> para = new Dictionary<string, string>();
        para["1"] = data.ValueString;
        UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_resource_not_available_gather_description", para, true), (System.Action) null, 4f, true);
        return;
      }
    }
    if (DBManager.inst.DB_March.marchOwner.Count >= PlayerData.inst.playerCityData.marchSlot)
      GameEngine.Instance.marchSystem.ShowMaxMarchSlotMessage();
    else
      this.Gather(this.Map.SelectedTile.Location);
  }

  public void DigCurTile()
  {
    this.Dig(this.Map.SelectedTile.Location);
  }

  public void Dig(Coordinate location)
  {
    UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
    {
      location = location,
      marchType = MarchAllocDlg.MarchType.dig
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void Encamp(Coordinate target)
  {
    UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
    {
      location = target,
      marchType = MarchAllocDlg.MarchType.encamp
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void EncampCurTile()
  {
    this.Encamp(this.Map.SelectedTile.Location);
  }

  public void OnInviteTeleport()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceDlg", (UI.Dialog.DialogParameter) new AllianceDlg.Parameter()
    {
      selectedIndex = 1
    }, true, true, true);
  }

  public void AttackMonster(Coordinate target)
  {
    UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
    {
      location = target,
      marchType = MarchAllocDlg.MarchType.monsterAttack
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void AttackMiniWonder(Coordinate target)
  {
    UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
    {
      location = target,
      marchType = MarchAllocDlg.MarchType.miniWonderAttack
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void Teleport(Coordinate target)
  {
    if (PlayerData.inst.IsInPit)
      this.Map.EnterTeleportMode(target, TeleportMode.ADVANCE_PIT_TELEPORT, 0, false);
    else
      this.Map.EnterTeleportMode(target, TeleportMode.ADVANCE_TELEPORT, 0, false);
  }

  public void Teleport2CurTile()
  {
    this.Teleport(this.Map.SelectedTile.Location);
  }

  public void KingdomTeleport()
  {
    Coordinate location = this.Map.SelectedTile.Location;
    Hashtable hashtable = new Hashtable();
    hashtable[(object) "kingdom_id"] = (object) location.K;
    hashtable[(object) "map_x"] = (object) location.X;
    hashtable[(object) "map_y"] = (object) location.Y;
    UIManager.inst.OpenPopup("Teleport/ForceTeleportPopup", (Popup.PopupParameter) new ForceTeleportPopup.Parameter()
    {
      request = hashtable,
      onConfirm = new System.Action<bool, object>(this.UseKingdomTeleportCallBack)
    });
  }

  private void UseKingdomTeleportCallBack(bool ret, object orgData)
  {
    if (!ret)
      return;
    GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Deep);
  }

  public void KingdomTeleportBack()
  {
    this.Map.EnterTeleportMode(this.Map.SelectedTile.Location, TeleportMode.KINGDOM_TELEPORT_BACK, 0, false);
  }

  public void AllianceWarTeleport(Coordinate targetLocation)
  {
    PVPSystem.Instance.Map.GotoLocation(targetLocation, false);
    Utils.ExecuteInSecs(0.5f, (System.Action) (() =>
    {
      if (!((UnityEngine.Object) this != (UnityEngine.Object) null))
        return;
      TileData emptyTileForCity = PVPMapData.MapData.GetNearEmptyTileForCity(targetLocation, false);
      if (emptyTileForCity == null)
        return;
      this.Map.EnterTeleportMode(emptyTileForCity.Location, TeleportMode.ALLIANCE_WAR_ENTER_TELEPORT, 0, false);
      Coordinate location = emptyTileForCity.Location;
      if (emptyTileForCity.Location.K != PlayerData.inst.playerCityData.cityLocation.K)
        ++location.Y;
      PVPSystem.Instance.Map.GotoLocation(location, false);
    }));
  }

  public void AllianceWarBackTeleport()
  {
    AllianceWarPayload.Instance.Back((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Lite);
    }));
  }

  private bool CanTeleport(Coordinate location, bool ignoreWonderArea = false)
  {
    TileData referenceAt = this.m_MapData.GetReferenceAt(location);
    if (referenceAt == null || location.X == 0 || location.Y == 0)
      return false;
    if (referenceAt.TileType != TileType.Terrain && (ignoreWonderArea || referenceAt.TileType != TileType.WonderTree))
      return referenceAt.TileType == TileType.None;
    return true;
  }

  private bool CanTeleport2Location(Coordinate targetLocation)
  {
    if (this.m_MapData == null)
      return false;
    WorldCoordinate worldCoordinate = this.m_MapData.ConvertTileKXYToWorldCoordinate(targetLocation);
    WorldCoordinate worldCoords1 = new WorldCoordinate(worldCoordinate.X - 1, worldCoordinate.Y + 1);
    WorldCoordinate worldCoords2 = new WorldCoordinate(worldCoordinate.X + 1, worldCoordinate.Y + 1);
    WorldCoordinate worldCoords3 = new WorldCoordinate(worldCoordinate.X, worldCoordinate.Y + 2);
    Coordinate kxy1 = this.m_MapData.ConvertWorldCoordinateToKXY(worldCoordinate);
    Coordinate kxy2 = this.m_MapData.ConvertWorldCoordinateToKXY(worldCoords1);
    Coordinate kxy3 = this.m_MapData.ConvertWorldCoordinateToKXY(worldCoords2);
    Coordinate kxy4 = this.m_MapData.ConvertWorldCoordinateToKXY(worldCoords3);
    if (this.CanTeleport(kxy1, false) && this.CanTeleport(kxy2, false) && this.CanTeleport(kxy3, false))
      return this.CanTeleport(kxy4, false);
    return false;
  }

  public void BeginnerTeleport()
  {
    Coordinate location = this.Map.SelectedTile.Location;
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = ScriptLocalization.Get("id_uppercase_warning", true),
      content = ScriptLocalization.Get("teleport_beginner_change_kingdom_description", true),
      yes = ScriptLocalization.Get("id_uppercase_yes", true),
      no = ScriptLocalization.Get("id_uppercase_no", true),
      yesCallback = new System.Action(this.OnYesCallBack)
    });
  }

  private void OnYesCallBack()
  {
    Coordinate location = this.Map.SelectedTile.Location;
    Hashtable extra = new Hashtable();
    extra[(object) "kingdom_id"] = (object) location.K;
    extra[(object) "map_x"] = (object) location.X;
    extra[(object) "map_y"] = (object) location.Y;
    ItemBag.Instance.UseItem("item_beginnertele", extra, (System.Action<bool, object>) ((ret, orgData) =>
    {
      if (ret)
      {
        GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Lite);
      }
      else
      {
        Hashtable hashtable = orgData as Hashtable;
        if (hashtable == null || !hashtable.ContainsKey((object) "errno"))
          return;
        int result = 0;
        int.TryParse(hashtable[(object) "errno"].ToString(), out result);
        if (result != 1400081)
          return;
        UIManager.inst.ShowSystemBlocker("CommonBlocker", (SystemBlocker.SystemBlockerParameter) new CommonBlocker.Parameter()
        {
          type = CommonBlocker.CommonBlockerType.confirmation,
          displayType = 11,
          title = Utils.XLAT("exception_title"),
          buttonEventText = Utils.XLAT("exception_button"),
          descriptionText = string.Format(Utils.XLAT("exception_1400080"), (object) 5),
          buttonEvent = (System.Action) null
        });
      }
    }), 1);
  }

  public void Rally(Coordinate target)
  {
    UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
    {
      location = target,
      marchType = MarchAllocDlg.MarchType.startRally
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void RallyCurTile()
  {
    if (GameEngine.Instance.marchSystem.HasMarchToWonder() && (WonderUtils.IsWonder(this.Map.SelectedTile.Location) || WonderUtils.IsWonderTower(this.Map.SelectedTile.Location)))
      UIManager.inst.toast.Show(ScriptLocalization.Get("throne_invasion_existing_garrison_march", true), (System.Action) null, 4f, false);
    else if (CityManager.inst.GetHighestBuildingLevelFor("war_rally") > 0 && PlayerData.inst.allianceId != 0L && DBManager.inst.DB_March.marchOwner.Count < PlayerData.inst.playerCityData.marchSlot)
    {
      this.Rally(this.Map.SelectedTile.Location);
    }
    else
    {
      MarchSystem.LocalMarchCheckType type = GameEngine.Instance.marchSystem.CanRally(this.Map.SelectedTile.Location);
      UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
      {
        title = Utils.XLAT("id_uppercase_warning"),
        description = GameEngine.Instance.marchSystem.GetCheckTypeMsg(type)
      });
    }
  }

  public static void RallyAllianceBoss()
  {
    AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
    if (alliancePortalData == null)
      return;
    if (CityManager.inst.GetHighestBuildingLevelFor("war_rally") > 0 && PlayerData.inst.allianceId != 0L)
    {
      UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
      {
        location = alliancePortalData.Location,
        marchType = MarchAllocDlg.MarchType.startRabRally,
        energyCost = 20
      }, 1 != 0, 1 != 0, 1 != 0);
    }
    else
    {
      MarchSystem.LocalMarchCheckType type = GameEngine.Instance.marchSystem.CanRally(alliancePortalData.Location);
      UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
      {
        title = Utils.XLAT("id_uppercase_warning"),
        description = GameEngine.Instance.marchSystem.GetCheckTypeMsg(type)
      });
    }
  }

  public void RallyToAllianceBoss()
  {
    PVPController.RallyAllianceBoss();
  }

  public void Reinforce(Coordinate target)
  {
    if (DBManager.inst.DB_March.marchOwner.Count >= PlayerData.inst.playerCityData.marchSlot)
    {
      GameEngine.Instance.marchSystem.ShowMaxMarchSlotMessage();
    }
    else
    {
      TileType tileType = this.Map.SelectedTile.TileType;
      switch (tileType)
      {
        case TileType.Wonder:
          if (GameEngine.Instance.marchSystem.HasMarchToWonder())
          {
            UIManager.inst.toast.Show(ScriptLocalization.Get("throne_invasion_existing_garrison_march", true), (System.Action) null, 4f, false);
            break;
          }
          TileData tileData1 = this.m_MapData.GetReferenceAt(target);
          RequestManager.inst.SendRequest("wonder:getWonderCapacity", new Hashtable()
          {
            {
              (object) "world_id",
              (object) tileData1.WonderData.WORLD_ID
            }
          }, (System.Action<bool, object>) ((arg1, arg2) =>
          {
            if (arg1)
            {
              Hashtable hashtable = arg2 as Hashtable;
              string empty = string.Empty;
              if (hashtable != null && hashtable.ContainsKey((object) "capacity"))
                empty = hashtable[(object) "capacity"].ToString();
              if (tileData1.WonderData != null && !string.IsNullOrEmpty(empty))
              {
                int result;
                if (int.TryParse(empty, out result))
                  tileData1.WonderData.MaxTroopsCount = (long) result;
                if (tileData1.WonderData.MaxTroopsCount < 0L)
                  tileData1.WonderData.MaxTroopsCount = 0L;
              }
            }
            UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
            {
              location = target,
              desTroopCount = (tileData1.WonderData.MaxTroopsCount != 0L ? (int) (tileData1.WonderData.MaxTroopsCount - tileData1.WonderData.CurrentTroopsCount) : -1),
              marchType = MarchAllocDlg.MarchType.wonderReinforce
            }, 1 != 0, 1 != 0, 1 != 0);
          }), true);
          break;
        case TileType.WonderTower:
          if (GameEngine.Instance.marchSystem.HasMarchToWonder())
          {
            UIManager.inst.toast.Show(ScriptLocalization.Get("throne_invasion_existing_garrison_march", true), (System.Action) null, 4f, false);
            break;
          }
          TileData tileData2 = this.m_MapData.GetReferenceAt(target);
          RequestManager.inst.SendRequest("wonder:getWonderCapacity", new Hashtable()
          {
            {
              (object) "world_id",
              (object) tileData2.WonderTowerData.WORLD_ID
            },
            {
              (object) "tower_id",
              (object) tileData2.WonderTowerData.TOWER_ID
            }
          }, (System.Action<bool, object>) ((arg1, arg2) =>
          {
            if (arg1)
            {
              Hashtable hashtable = arg2 as Hashtable;
              string empty = string.Empty;
              if (hashtable != null && hashtable.ContainsKey((object) "capacity"))
                empty = hashtable[(object) "capacity"].ToString();
              if (tileData2.WonderTowerData != null && !string.IsNullOrEmpty(empty))
              {
                int result;
                if (int.TryParse(empty, out result))
                  tileData2.WonderTowerData.MaxTroopsCount = (long) result;
                if (tileData2.WonderTowerData.MaxTroopsCount < 0L)
                  tileData2.WonderTowerData.MaxTroopsCount = 0L;
              }
            }
            UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
            {
              location = target,
              desTroopCount = (tileData2.WonderTowerData.MaxTroopsCount != 0L ? (int) (tileData2.WonderTowerData.MaxTroopsCount - tileData2.WonderTowerData.CurrentTroopsCount) : -1),
              marchType = MarchAllocDlg.MarchType.wonderReinforce
            }, 1 != 0, 1 != 0, 1 != 0);
          }), true);
          break;
        case TileType.AllianceFortress:
          AllianceFortressData afd = DBManager.inst.DB_AllianceFortress.GetDataByCoordinate(target);
          AllianceBuildUtils.LoadMaxTroopCount(afd, (System.Action) (() => UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
          {
            location = target,
            desTroopCount = (afd.MaxTroopsCount != 0L ? (int) (afd.MaxTroopsCount - AllianceBuildUtils.GetCurrentTroopCount(afd)) : -1),
            marchType = MarchAllocDlg.MarchType.fortressReinforce
          }, 1 != 0, 1 != 0, 1 != 0)));
          break;
        case TileType.AllianceWarehouse:
          AllianceWareHouseData warehouesData = DBManager.inst.DB_AllianceWarehouse.GetWarehouseDataByCoordinate(target);
          if (warehouesData == null)
            break;
          AllianceBuildUtils.LoadAllianceBuildingMaxTroopCount(target, TileType.AllianceWarehouse, (System.Action) (() => UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
          {
            location = target,
            desTroopCount = (warehouesData.MaxTroopsCount != 0L ? (int) (warehouesData.MaxTroopsCount - warehouesData.CurrentTroopsCount) : -1),
            marchType = MarchAllocDlg.MarchType.allianceCityBuild
          }, 1 != 0, 1 != 0, 1 != 0)));
          break;
        case TileType.AllianceSuperMine:
          AllianceSuperMineData superMineData = DBManager.inst.DB_AllianceSuperMine.GetDataByCoordinate(target);
          if (superMineData == null)
            break;
          AllianceBuildUtils.LoadAllianceBuildingMaxTroopCount(target, TileType.AllianceSuperMine, (System.Action) (() => UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
          {
            location = target,
            desTroopCount = (superMineData.MaxTroopsCount != 0L ? (int) (superMineData.MaxTroopsCount - superMineData.CurrentTroopsCount) : -1),
            marchType = (superMineData.CurrentState != AllianceSuperMineData.State.COMPLETE ? MarchAllocDlg.MarchType.allianceCityBuild : MarchAllocDlg.MarchType.gather)
          }, 1 != 0, 1 != 0, 1 != 0)));
          break;
        case TileType.AlliancePortal:
          AlliancePortalData portalData = DBManager.inst.DB_AlliancePortal.GetDataByCoordinate(target);
          if (portalData == null)
            break;
          AllianceBuildUtils.LoadAllianceBuildingMaxTroopCount(target, TileType.AlliancePortal, (System.Action) (() => UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
          {
            location = target,
            desTroopCount = (portalData.MaxTroopsCount != 0L ? (int) (portalData.MaxTroopsCount - portalData.CurrentTroopsCount) : -1),
            marchType = (portalData.CurrentState != AlliancePortalData.State.COMPLETE ? MarchAllocDlg.MarchType.allianceCityBuild : MarchAllocDlg.MarchType.gather)
          }, 1 != 0, 1 != 0, 1 != 0)));
          break;
        case TileType.AllianceHospital:
          AllianceHospitalData hospitalData = DBManager.inst.DB_AllianceHospital.GetDataByCoordinate(target);
          if (hospitalData == null)
            break;
          AllianceBuildUtils.LoadAllianceBuildingMaxTroopCount(target, TileType.AllianceHospital, (System.Action) (() => UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
          {
            location = target,
            desTroopCount = (hospitalData.MaxTroopsCount != 0L ? (int) (hospitalData.MaxTroopsCount - hospitalData.CurrentTroopsCount) : -1),
            marchType = (hospitalData.CurrentState != AllianceHospitalData.State.COMPLETE ? MarchAllocDlg.MarchType.allianceCityBuild : MarchAllocDlg.MarchType.gather)
          }, 1 != 0, 1 != 0, 1 != 0)));
          break;
        case TileType.AllianceTemple:
          AllianceTempleData templeData = DBManager.inst.DB_AllianceTemple.GetDataByCoordinate(target);
          if (templeData == null)
            break;
          AllianceBuildUtils.LoadAllianceBuildingMaxTroopCount(target, TileType.AllianceTemple, (System.Action) (() => UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
          {
            location = target,
            desTroopCount = (templeData.MaxTroopsCount != 0L ? (int) (templeData.MaxTroopsCount - templeData.CurrentTroopsCount) : -1),
            marchType = MarchAllocDlg.MarchType.templeReinforce
          }, 1 != 0, 1 != 0, 1 != 0)));
          break;
        default:
          if (tileType != TileType.City)
            break;
          UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
          {
            location = target,
            marchType = MarchAllocDlg.MarchType.reinforce
          }, 1 != 0, 1 != 0, 1 != 0);
          break;
      }
    }
  }

  public void ReinforceCurTile()
  {
    if (DBManager.inst.DB_March.marchOwner.Count >= PlayerData.inst.playerCityData.marchSlot)
      GameEngine.Instance.marchSystem.ShowMaxMarchSlotMessage();
    else
      this.Reinforce(this.Map.SelectedTile.Location);
  }

  public void OccupyWonder()
  {
    if (DBManager.inst.DB_March.marchOwner.Count >= PlayerData.inst.playerCityData.marchSlot)
      GameEngine.Instance.marchSystem.ShowMaxMarchSlotMessage();
    else if (GameEngine.Instance.marchSystem.HasMarchToWonder())
      UIManager.inst.toast.Show(ScriptLocalization.Get("throne_invasion_existing_garrison_march", true), (System.Action) null, 4f, false);
    else
      UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
      {
        location = this.Map.SelectedTile.Location,
        marchType = MarchAllocDlg.MarchType.wonderOccupy
      }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void RecallFromCurTile()
  {
    GameEngine.Instance.marchSystem.Recall(this.Map.SelectedTile.MarchID);
  }

  public void RecallFromCurFortress()
  {
    switch (this.Map.SelectedTile.TileType)
    {
      case TileType.Wonder:
        DB.WonderData wonderData = this.Map.SelectedTile.WonderData;
        if (wonderData == null)
          break;
        GameEngine.Instance.marchSystem.Recall(wonderData.MyTroopId);
        break;
      case TileType.WonderTower:
        WonderTowerData wonderTowerData = this.Map.SelectedTile.WonderTowerData;
        if (wonderTowerData == null)
          break;
        GameEngine.Instance.marchSystem.Recall(wonderTowerData.MyTroopId);
        break;
      case TileType.AllianceFortress:
        AllianceFortressData dataByCoordinate1 = DBManager.inst.DB_AllianceFortress.GetDataByCoordinate(this.Map.SelectedTile.Location);
        if (dataByCoordinate1 == null)
          break;
        GameEngine.Instance.marchSystem.Recall(AllianceBuildUtils.GetMyTroopId(dataByCoordinate1));
        break;
      case TileType.AllianceWarehouse:
        AllianceWareHouseData dataByCoordinate2 = DBManager.inst.DB_AllianceWarehouse.GetWarehouseDataByCoordinate(this.Map.SelectedTile.Location);
        if (dataByCoordinate2 == null)
          break;
        GameEngine.Instance.marchSystem.Recall(dataByCoordinate2.MyTroopId);
        break;
      case TileType.AllianceSuperMine:
        AllianceSuperMineData dataByCoordinate3 = DBManager.inst.DB_AllianceSuperMine.GetDataByCoordinate(this.Map.SelectedTile.Location);
        if (dataByCoordinate3 == null)
          break;
        GameEngine.Instance.marchSystem.Recall(dataByCoordinate3.CurrentTroopId);
        break;
      case TileType.AllianceHospital:
        AllianceHospitalData dataByCoordinate4 = DBManager.inst.DB_AllianceHospital.GetDataByCoordinate(this.Map.SelectedTile.Location);
        if (dataByCoordinate4 == null)
          break;
        GameEngine.Instance.marchSystem.Recall(dataByCoordinate4.MyTroopId);
        break;
      case TileType.AllianceTemple:
        AllianceTempleData allianceTempleData = DBManager.inst.DB_AllianceTemple.Get(this.Map.SelectedTile.Location.X, this.Map.SelectedTile.Location.Y);
        if (allianceTempleData == null)
          break;
        GameEngine.Instance.marchSystem.Recall(allianceTempleData.MyTroopId);
        break;
    }
  }

  public void Scout(Coordinate target)
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    GameEngine.Instance.marchSystem.StartScout(target);
  }

  public void OpenScoutDlg(Coordinate target)
  {
    UIManager.inst.OpenDlg("scoutConfirmDlg", (UI.Dialog.DialogParameter) new ScoutConfirmDlg.Parameter()
    {
      targetLocation = target,
      mapData = this.m_MapData
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void ScoutCurTile()
  {
    this.OpenScoutDlg(this.Map.SelectedTile.Location);
  }

  public void ViewMyEncamp()
  {
    UIManager.inst.OpenPopup("MarchDetailPopUp", (Popup.PopupParameter) new MarchDetailPopUp.Parameter()
    {
      marchId = this.Map.SelectedTile.MarchID
    });
  }

  public void RecallMyEncamp()
  {
    GameEngine.Instance.marchSystem.Recall(this.Map.SelectedTile.MarchID);
  }

  public void OnShowTileInfo()
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.m_MapData.GetTileAt(KingdomTouchCircle.Instance.Para.tData.Location).MarchID);
    if (marchData == null || marchData.ownerUid != PlayerData.inst.uid || marchData != null && marchData.state != MarchData.MarchState.gathering)
      UIManager.inst.OpenPopup("InfoKingdom", (Popup.PopupParameter) new InfoKingdom.Parameter());
    else
      UIManager.inst.OpenDlg("ResourceInfoDlg", (UI.Dialog.DialogParameter) new ResourceTileDlg.Parameter()
      {
        location = KingdomTouchCircle.Instance.Para.tData.Location
      }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnDigsiteInfo()
  {
    UIManager.inst.OpenPopup("HelpPopup", (Popup.PopupParameter) new HelpPopup.Parameter()
    {
      id = "help_treasure_map"
    });
  }

  private void ShowCity()
  {
    UIManager.inst.Cloud.CoverCloud((System.Action) (() => GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.CityMode));
  }
}
