﻿// Decompiled with JetBrains decompiler
// Type: TimeLimitActivityData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class TimeLimitActivityData : RoundActivityData
{
  public override List<int> CurrentRoundTargetScores
  {
    get
    {
      List<int> souces = new List<int>();
      ActivityRewardInfo currentStepReward = ConfigManager.inst.DB_ActivityReward.GetCurrentStepReward(this.CurrentRound.ActivitySubConfigID, this.StrongholdLevel);
      this.AddList(souces, currentStepReward.RequireScore_1);
      this.AddList(souces, currentStepReward.RequireScore_2);
      this.AddList(souces, currentStepReward.RequireScore_3);
      return souces;
    }
  }

  public override void Decode(Hashtable source)
  {
    base.Decode(source);
    PlayerPrefsEx.SetIntByUid("activity_gold_start_time", this.StartTime);
    PlayerPrefsEx.SetIntByUid("activity_gold_end_time", this.EndTime);
    PlayerPrefsEx.Save();
  }

  public override List<int> CurrentItems
  {
    get
    {
      List<int> souces = new List<int>();
      ActivityRewardInfo currentStepReward = ConfigManager.inst.DB_ActivityReward.GetCurrentStepReward(this.CurrentRound.ActivitySubConfigID, this.StrongholdLevel);
      this.AddList(souces, currentStepReward.ItemRewardID_1);
      this.AddList(souces, currentStepReward.ItemRewardID_2);
      this.AddList(souces, currentStepReward.ItemRewardID_3);
      return souces;
    }
  }

  public override Dictionary<int, int> CurrentRoundRewardItems
  {
    get
    {
      Dictionary<int, int> souces = new Dictionary<int, int>();
      ActivityRewardInfo currentStepReward = ConfigManager.inst.DB_ActivityReward.GetCurrentStepReward(this.CurrentRound.ActivitySubConfigID, this.StrongholdLevel);
      this.AddDict(souces, currentStepReward.ItemRewardID_1, currentStepReward.ItemRewardValue_1);
      this.AddDict(souces, currentStepReward.ItemRewardID_2, currentStepReward.ItemRewardValue_2);
      this.AddDict(souces, currentStepReward.ItemRewardID_3, currentStepReward.ItemRewardValue_3);
      return souces;
    }
  }

  public override List<int> TotalRankTopOneItems
  {
    get
    {
      return this.GetRankItems(0, 1);
    }
  }

  public override Dictionary<int, int> TotalRankTopOneRewardItems
  {
    get
    {
      return this.GetRankRewardItems(0, 1);
    }
  }

  public override List<int> GetRankItems(int subActivityId, int rank)
  {
    List<int> souces = new List<int>();
    ActivityRankRewardInfo rewardInfo = ConfigManager.inst.DB_ActivityRankReward.GetRewardInfo(subActivityId, rank);
    this.AddList(souces, rewardInfo.ItemRewardID_1);
    this.AddList(souces, rewardInfo.ItemRewardID_2);
    this.AddList(souces, rewardInfo.ItemRewardID_3);
    this.AddList(souces, rewardInfo.ItemRewardID_4);
    this.AddList(souces, rewardInfo.ItemRewardID_5);
    this.AddList(souces, rewardInfo.ItemRewardID_6);
    return souces;
  }

  public override Dictionary<int, int> GetRankRewardItems(int subActivityId, int rank)
  {
    Dictionary<int, int> souces = new Dictionary<int, int>();
    ActivityRankRewardInfo rewardInfo = ConfigManager.inst.DB_ActivityRankReward.GetRewardInfo(subActivityId, rank);
    this.AddDict(souces, rewardInfo.ItemRewardID_1, rewardInfo.ItemRewardValue_1);
    this.AddDict(souces, rewardInfo.ItemRewardID_2, rewardInfo.ItemRewardValue_2);
    this.AddDict(souces, rewardInfo.ItemRewardID_3, rewardInfo.ItemRewardValue_3);
    this.AddDict(souces, rewardInfo.ItemRewardID_4, rewardInfo.ItemRewardValue_4);
    this.AddDict(souces, rewardInfo.ItemRewardID_5, rewardInfo.ItemRewardValue_5);
    this.AddDict(souces, rewardInfo.ItemRewardID_6, rewardInfo.ItemRewardValue_6);
    return souces;
  }

  public override ActivityBaseData.ActivityType Type
  {
    get
    {
      return ActivityBaseData.ActivityType.TimeLimit;
    }
  }
}
