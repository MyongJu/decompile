﻿// Decompiled with JetBrains decompiler
// Type: ChestLibraryPage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class ChestLibraryPage : MonoBehaviour
{
  private List<AllianceTreasuryVaultData> _allSendedChestData = new List<AllianceTreasuryVaultData>();
  private List<SendedChestItem> _allSendedChestItem = new List<SendedChestItem>();
  [SerializeField]
  private UIScrollView _ScrollView;
  [SerializeField]
  private UIWrapContent _WrapContent;
  [SerializeField]
  private SendedChestItem _SendedChestItemTemplate;
  [SerializeField]
  private GameObject _rootHaveNoRecord;
  private bool _needUpdate;

  private void Awake()
  {
    this._WrapContent.onInitializeItem += new UIWrapContent.OnInitializeItem(this.OnInitializeItem);
  }

  private void OnInitializeItem(GameObject go, int warpIndex, int realIndex)
  {
    SendedChestItem component = go.GetComponent<SendedChestItem>();
    if (this._allSendedChestItem.Contains(component))
      this._allSendedChestItem.Remove(component);
    if (!(bool) ((UnityEngine.Object) component))
      return;
    if (realIndex >= 0 && realIndex < this._allSendedChestData.Count)
    {
      component.gameObject.SetActive(true);
      component.SetData(this._allSendedChestData[realIndex]);
      this._allSendedChestItem.Add(component);
    }
    else
      component.gameObject.SetActive(false);
  }

  public void OnSecondTick(int delta)
  {
    using (List<SendedChestItem>.Enumerator enumerator = this._allSendedChestItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.OnSecondTick(delta);
    }
  }

  private void Reset()
  {
    this._rootHaveNoRecord.SetActive(false);
    this._WrapContent.cullContent = false;
    this._WrapContent.minIndex = 0;
    this._WrapContent.maxIndex = this._WrapContent.transform.childCount - 1;
    using (List<SendedChestItem>.Enumerator enumerator = this._allSendedChestItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.gameObject.SetActive(false);
    }
    this._WrapContent.WrapContent();
  }

  public void NotifyUpdate()
  {
    this._needUpdate = true;
  }

  protected void Update()
  {
    if (this._needUpdate)
    {
      this._needUpdate = false;
      this.UpdateUI();
    }
    if (!AllianceTreasuryVaultPayload.Instance.TryRemoveExpired())
      return;
    this.UpdateSendedChestItemList();
  }

  public void UpdateUI()
  {
    this.Reset();
    this._SendedChestItemTemplate.gameObject.SetActive(false);
    AllianceTreasuryVaultPayload.Instance.LoadTreasuryList((System.Action) (() => this.UpdateSendedChestItemList()));
  }

  private void UpdateSendedChestItemList()
  {
    this._allSendedChestData = AllianceTreasuryVaultPayload.Instance.AllSendedChestData;
    this._rootHaveNoRecord.SetActive(this._allSendedChestData.Count <= 0);
    if (this._allSendedChestData.Count <= 0)
      this._WrapContent.minIndex = 0;
    this._WrapContent.maxIndex = Mathf.Max(this._allSendedChestData.Count - 1, this._WrapContent.transform.childCount - 1);
    this._WrapContent.SortBasedOnScrollMovement();
    this._WrapContent.WrapContent();
    this._ScrollView.ResetPosition();
  }

  public void OnButtonViewHistoryClicked()
  {
    UIManager.inst.OpenPopup("AllianceChest/AllianceChestHistoryPopup", (Popup.PopupParameter) null);
  }
}
