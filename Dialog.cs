﻿// Decompiled with JetBrains decompiler
// Type: Dialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public abstract class Dialog : UI.Dialog
{
  [HideInInspector]
  public bool UseTransition = true;
  private float moveAniamtionDuringTime = 0.01f;

  public virtual void OpenDialog(Hashtable param = null)
  {
  }

  public virtual void ShowDialog()
  {
    this.gameObject.SetActive(true);
  }

  public virtual void CloseDialog()
  {
    Object.Destroy((Object) this.gameObject);
  }

  public virtual void HideDialog()
  {
    this.gameObject.SetActive(false);
  }

  public virtual void OpenDialogAnim()
  {
    if (this.UseTransition)
    {
      this.GetComponent<UIPanel>().alpha = 0.0f;
      UITweener.Begin<TweenPosition>(this.gameObject, this.moveAniamtionDuringTime);
      TweenAlpha tweenAlpha = TweenAlpha.Begin(this.gameObject, this.moveAniamtionDuringTime, 1f, 0.0f);
      tweenAlpha.onFinished.Clear();
      tweenAlpha.onFinished.Add(new EventDelegate((MonoBehaviour) this, "OpenDialogAnimEndCallback"));
    }
    else
      this.OpenDialogAnimEndCallback();
  }

  public virtual void OpenDialogAnimEndCallback()
  {
  }

  public virtual void ShowDialogAnim()
  {
    if (this.UseTransition)
    {
      this.GetComponent<UIPanel>().alpha = 0.0f;
      UITweener.Begin<TweenPosition>(this.gameObject, this.moveAniamtionDuringTime);
      TweenAlpha tweenAlpha = TweenAlpha.Begin(this.gameObject, this.moveAniamtionDuringTime, 1f, 0.0f);
      tweenAlpha.onFinished.Clear();
      tweenAlpha.onFinished.Add(new EventDelegate((MonoBehaviour) this, "ShowDialogAnimEndCallback"));
    }
    else
      this.ShowDialogAnimEndCallback();
  }

  public virtual void ShowDialogAnimEndCallback()
  {
  }

  public virtual void HideDialogAnim()
  {
    if (this.UseTransition)
    {
      UITweener.Begin<TweenPosition>(this.gameObject, this.moveAniamtionDuringTime);
      TweenAlpha tweenAlpha = TweenAlpha.Begin(this.gameObject, this.moveAniamtionDuringTime, 0.0f, 0.0f);
      tweenAlpha.onFinished.Clear();
      tweenAlpha.onFinished.Add(new EventDelegate((MonoBehaviour) this, "HideDialogAnimEndCallback"));
    }
    else
      this.HideDialogAnimEndCallback();
  }

  public virtual void HideDialogAnimEndCallback()
  {
    this.HideDialog();
  }

  public virtual void CloseDialogAnim()
  {
    if (this.UseTransition)
    {
      UITweener.Begin<TweenPosition>(this.gameObject, this.moveAniamtionDuringTime);
      TweenAlpha tweenAlpha = TweenAlpha.Begin(this.gameObject, this.moveAniamtionDuringTime, 0.0f, 0.0f);
      tweenAlpha.onFinished.Clear();
      tweenAlpha.onFinished.Add(new EventDelegate((MonoBehaviour) this, "CloseDialogAnimEndCallback"));
    }
    else
      this.CloseDialogAnimEndCallback();
  }

  public virtual void CloseDialogAnimEndCallback()
  {
    this.CloseDialog();
  }
}
