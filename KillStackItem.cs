﻿// Decompiled with JetBrains decompiler
// Type: KillStackItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class KillStackItem : MonoBehaviour
{
  public UITexture m_Icon;
  public UILabel m_Desc;
  public System.Action OnDestroy;
  private DragonKnightTalentInfo m_TalentInfo;

  public void SetData(int talentSkillId)
  {
    this.m_TalentInfo = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(talentSkillId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, this.m_TalentInfo.imagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.m_Desc.text = this.m_TalentInfo.LocName;
    TweenAlpha tweenAlpha = TweenAlpha.Begin(this.gameObject, 0.2f, 0.0f, 1f);
    tweenAlpha.ignoreTimeScale = false;
    tweenAlpha.SetOnFinished(new EventDelegate.Callback(this.OnAnimationEnd));
  }

  private void OnAnimationEnd()
  {
    this.gameObject.SetActive(false);
    this.transform.parent = (Transform) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    if (this.OnDestroy == null)
      return;
    this.OnDestroy();
  }
}
