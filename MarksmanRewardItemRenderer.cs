﻿// Decompiled with JetBrains decompiler
// Type: MarksmanRewardItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UnityEngine;

public class MarksmanRewardItemRenderer : MonoBehaviour
{
  private const string FRAME_PREFIX = "round_";
  [SerializeField]
  private MarksmanRewardItemRenderer.PickPanel pickPanel;
  [SerializeField]
  private MarksmanRewardItemRenderer.PlayPanel playPanel;
  [SerializeField]
  private MarksmanRewardItemRenderer.RecordPanel recordPanel;
  public System.Action<int> onMarkFrameClicked;
  private bool _isShotting;
  private MarksmanData.RewardData _rewardData;
  private MarksmanData.RewardType _rewardType;

  public int RewardStayIndex
  {
    get
    {
      if (this._rewardData != null)
        return this._rewardData.RewardStayIndex;
      return 0;
    }
  }

  public void SetData(MarksmanData.RewardData rewardData, MarksmanData.RewardType rewardType)
  {
    this._rewardData = rewardData;
    this._rewardType = rewardType;
    this.UpdateUI(false);
  }

  public void RefreshUI()
  {
    this.UpdateUI(true);
  }

  public void OnMarkFrameClicked()
  {
    if (this._rewardData == null)
      return;
    if (!MarksmanPayload.Instance.IsPropsEnough)
    {
      MarksmanPayload.Instance.ShowPropsNotEnoughConfirm();
    }
    else
    {
      if (this._isShotting)
        return;
      this._isShotting = true;
      MarksmanPayload.Instance.OpenMarksmanByIndex(this._rewardData.RewardStayIndex, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this._rewardData = MarksmanPayload.Instance.TempMarksmanData.RewardDataList.Find((Predicate<MarksmanData.RewardData>) (x => x.RewardStayIndex == this._rewardData.RewardStayIndex));
        this.ShowShotEffect();
      }));
    }
  }

  private void UpdateUI(bool refresh = false)
  {
    if (this._rewardData == null)
      return;
    switch (this._rewardType)
    {
      case MarksmanData.RewardType.PICK:
        this.pickPanel.itemIconRenderer.SetData(this._rewardData.RewardId, string.Empty, true);
        this.pickPanel.itemCountText.text = "x" + this._rewardData.RewardCount.ToString();
        break;
      case MarksmanData.RewardType.PLAY:
        this.playPanel.itemIconRenderer.SetData(this._rewardData.RewardId, "round_", true);
        this.playPanel.itemCountText.text = "x" + this._rewardData.RewardCount.ToString();
        this.playPanel.propsNeedCountText.text = MarksmanPayload.Instance.NeedPropsCount2Shot > 0 ? MarksmanPayload.Instance.NeedPropsCount2Shot.ToString() : Utils.XLAT("id_uppercase_free");
        this.playPanel.propsNeedCountText.color = !MarksmanPayload.Instance.IsPropsEnough ? Color.red : Color.white;
        if (!refresh)
        {
          NGUITools.SetActive(this.playPanel.rewardRootNode, this._rewardData.RewardOpenedStep > 0);
          NGUITools.SetActive(this.playPanel.rewardFrameRootNode, this._rewardData.RewardOpenedStep <= 0);
        }
        MarksmanPayload.Instance.FeedMarksmanTexture(this.playPanel.propsTexture, MarksmanPayload.Instance.TempMarksmanData.PropsImagePath);
        break;
      case MarksmanData.RewardType.RECORD:
        this.recordPanel.itemIconRenderer.SetData(this._rewardData.RewardId, "round_", true);
        this.recordPanel.itemCountText.text = "x" + this._rewardData.RewardCount.ToString();
        this.recordPanel.itemCollectedStepText.text = ScriptLocalization.GetWithPara("tavern_rewards_step_num", new Dictionary<string, string>()
        {
          {
            "0",
            this._rewardData.RewardOpenedStep.ToString()
          }
        }, true);
        NGUITools.SetActive(this.recordPanel.itemCollectedNode, this._rewardData.RewardOpenedStep > 0);
        NGUITools.SetActive(this.recordPanel.itemCollectedStepText.transform.parent.gameObject, this._rewardData.RewardOpenedStep > 0);
        break;
    }
  }

  private void CheckScrollChip()
  {
    if (this._rewardData == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._rewardData.RewardId);
    if (itemStaticInfo == null)
      return;
    ConfigEquipmentScrollCombine equipmentScrollCombine = (ConfigEquipmentScrollCombine) null;
    ConfigEquipmentScrollChipInfo equipmentScrollChipInfo = (ConfigEquipmentScrollChipInfo) null;
    bool flag = false;
    bool state = false;
    if (itemStaticInfo.Type == ItemBag.ItemType.equipment_scroll_chip)
    {
      equipmentScrollCombine = ConfigManager.inst.GetEquipmentScrollChipCombine(BagType.Hero);
      equipmentScrollChipInfo = ConfigManager.inst.GetEquipmentScrollChip(BagType.Hero).GetDataByItemID(this._rewardData.RewardId);
      state = true;
    }
    else if (itemStaticInfo.Type == ItemBag.ItemType.dk_equipment_scroll_chip)
    {
      equipmentScrollCombine = ConfigManager.inst.GetEquipmentScrollChipCombine(BagType.DragonKnight);
      equipmentScrollChipInfo = ConfigManager.inst.GetEquipmentScrollChip(BagType.DragonKnight).GetDataByItemID(this._rewardData.RewardId);
      state = true;
    }
    if (equipmentScrollChipInfo != null && equipmentScrollCombine != null && equipmentScrollCombine.GetCombineTarget(equipmentScrollChipInfo.internalId) != null)
      flag = equipmentScrollCombine.GetCombineTarget(equipmentScrollChipInfo.internalId).Count == 1;
    string str = !flag ? "icon_corner_mark_question" : "icon_corner_mark_fragment";
    switch (this._rewardType)
    {
      case MarksmanData.RewardType.PICK:
        this.pickPanel.itemScrollChipTip.spriteName = str;
        NGUITools.SetActive(this.pickPanel.itemScrollChipTip.gameObject, state);
        break;
      case MarksmanData.RewardType.PLAY:
        this.playPanel.itemScrollChipTip.spriteName = str;
        NGUITools.SetActive(this.playPanel.itemScrollChipTip.gameObject, state);
        break;
      case MarksmanData.RewardType.RECORD:
        this.recordPanel.itemScrollChipTip.spriteName = str;
        NGUITools.SetActive(this.recordPanel.itemScrollChipTip.gameObject, state);
        break;
    }
  }

  private void ShowShotEffect()
  {
    AudioManager.Instance.StopAndPlaySound("sfx_city_lucky_archer_shot");
    this.ShowShotArrowAnim(false);
    Utils.ExecuteInSecs(1f, (System.Action) (() =>
    {
      if ((UnityEngine.Object) null == (UnityEngine.Object) this)
        return;
      AudioManager.Instance.StopAndPlaySound("sfx_city_daily_activity_reward");
      this.ShowShotExplosion();
      if (this.onMarkFrameClicked == null)
        return;
      this.onMarkFrameClicked(this._rewardData.RewardStayIndex);
    }));
  }

  public void ShowShotArrowAnim(bool resetPropsCount = false)
  {
    NGUITools.SetActive(this.playPanel.arrowSprite.gameObject, true);
    Animator component = this.playPanel.arrowSprite.transform.GetComponent<Animator>();
    if ((bool) ((UnityEngine.Object) component))
      component.Play("Fx_Arrow");
    if (!resetPropsCount || this._rewardType != MarksmanData.RewardType.PLAY)
      return;
    this.playPanel.propsNeedCountText.text = string.Empty;
  }

  public void ShowShotExplosion()
  {
    NGUITools.SetActive(this.playPanel.shotSpecialEffect, true);
    NGUITools.SetActive(this.playPanel.arrowSprite.gameObject, false);
    NGUITools.SetActive(this.playPanel.rewardFrameRootNode, false);
    NGUITools.SetActive(this.playPanel.rewardRootNode, true);
    this._isShotting = false;
  }

  [Serializable]
  public class PickPanel
  {
    public ItemIconRenderer itemIconRenderer;
    public UILabel itemCountText;
    public UISprite itemScrollChipTip;
  }

  [Serializable]
  public class PlayPanel
  {
    public ItemIconRenderer itemIconRenderer;
    public UILabel itemCountText;
    public UISprite itemScrollChipTip;
    public UITexture propsTexture;
    public UILabel propsNeedCountText;
    public UISprite arrowSprite;
    public GameObject rewardRootNode;
    public GameObject rewardFrameRootNode;
    public GameObject shotSpecialEffect;
  }

  [Serializable]
  public class RecordPanel
  {
    public ItemIconRenderer itemIconRenderer;
    public UILabel itemCountText;
    public UISprite itemScrollChipTip;
    public GameObject itemCollectedNode;
    public UILabel itemCollectedStepText;
  }
}
