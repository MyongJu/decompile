﻿// Decompiled with JetBrains decompiler
// Type: GAME_LOCATION
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public enum GAME_LOCATION
{
  LOADING = -1,
  CITY = 0,
  KINGDOM = 1,
  WORLD = 2,
  MINI_MAP = 3,
  DRAGON_KNIGHT = 4,
  MERLIN_TOWER_FLOOR = 5,
  MERLIN_TOWER_KINGDOM = 6,
}
