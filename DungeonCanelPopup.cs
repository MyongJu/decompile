﻿// Decompiled with JetBrains decompiler
// Type: DungeonCanelPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;

public class DungeonCanelPopup : Popup
{
  public UILabel _content1;
  public UILabel _content2;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
    JobManager.Instance.OnJobRemove += new System.Action<long>(this.OnJobRemove);
    this.OnSecondEvent(0);
    this._content2.text = ScriptLocalization.GetWithPara("dragon_knight_raid_vip_instant_description", new Dictionary<string, string>()
    {
      {
        "0",
        ConfigManager.inst.DB_VIP_Level.GetMinLevelLimitForPrivilege("dk_dungeon_layer_complete_2").Level.ToString()
      }
    }, 1 != 0);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
    JobManager.Instance.OnJobRemove -= new System.Action<long>(this.OnJobRemove);
  }

  private void OnJobRemove(long jobId)
  {
    JobHandle job = JobManager.Instance.GetJob(jobId);
    if (job == null || job.GetJobEvent() != JobEvent.JOB_DRAGON_KNIGHT_DUNGEON_RAID)
      return;
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    DragonKnightDungeonData dungeonData = DragonKnightUtils.GetDungeonData();
    DragonKnightSystem.Instance.Controller.BattleHud.HideSpeedAndQuickBattle();
    DragonKnightSystem.Instance.Controller.RoomHud.HideAll();
    DragonKnightSystem.Instance.RoomManager.SetMazeData(dungeonData);
    DragonKnightSystem.Instance.RoomManager.StartSearch();
    DragonKnightSystem.Instance.Controller.RoomHud.SetTagPosition(DragonKnightSystem.Instance.RoomManager.CurrentRoom);
    if (dungeonData.RaidInfoList.Count <= 0)
      return;
    UIManager.inst.OpenPopup("DragonKnight/DungeonMopupCompletePopup", (Popup.PopupParameter) null);
  }

  public void OnCloseClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    UIManager.inst.Cloud.CoverCloud((System.Action) (() => GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.CityMode));
  }

  public void OnLeaveClick()
  {
    this.OnCloseClick();
  }

  public void OnCancelClick()
  {
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = Utils.XLAT("dragon_knight_raid_stop_button"),
      content = Utils.XLAT("dragon_knight_raid_stop_description"),
      no = Utils.XLAT("id_uppercase_cancel"),
      yes = Utils.XLAT("id_uppercase_confirm"),
      yesCallback = (System.Action) (() => MessageHub.inst.GetPortByAction("DragonKnight:cancelRaid").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
        DragonKnightDungeonData dungeonData = DragonKnightUtils.GetDungeonData();
        DragonKnightSystem.Instance.Controller.BattleHud.HideSpeedAndQuickBattle();
        DragonKnightSystem.Instance.Controller.RoomHud.HideAll();
        DragonKnightSystem.Instance.RoomManager.SetMazeData(dungeonData);
        DragonKnightSystem.Instance.RoomManager.StartSearch();
        DragonKnightSystem.Instance.Controller.RoomHud.SetTagPosition(DragonKnightSystem.Instance.RoomManager.CurrentRoom);
      }), true))
    });
  }

  private void OnSecondEvent(int timestamp = 0)
  {
    this._content1.text = ScriptLocalization.GetWithPara("dragon_knight_raid_finish_after_subtitle", new Dictionary<string, string>()
    {
      {
        "0",
        Utils.FormatTime((int) DragonKnightUtils.DungeonRaidJobLeftTime, false, false, true)
      }
    }, 1 != 0);
  }
}
