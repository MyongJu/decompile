﻿// Decompiled with JetBrains decompiler
// Type: Bridge
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Facebook.Unity;
using UnityEngine;

public class Bridge : MonoBehaviour
{
  public void ReceiveLocalNotificaitonData(string jsonData)
  {
    if (string.IsNullOrEmpty(jsonData) || jsonData.IndexOf("data=") == -1)
      return;
    string data = jsonData;
    if (jsonData.Contains("="))
      data = jsonData.Split('=')[1];
    NotificationManager.Instance.OnReviceNotificationData(data);
  }

  public void ReceiveGCMNotificaitonData(string jsonData)
  {
    if (string.IsNullOrEmpty(jsonData))
      return;
    NotificationManager.Instance.ReviceGCM(jsonData);
  }

  private void Awake()
  {
    Object.DontDestroyOnLoad((Object) this.gameObject);
    if (FB.IsInitialized)
      FB.ActivateApp();
    else
      FB.Init((InitDelegate) (() => FB.ActivateApp()), (HideUnityDelegate) null, (string) null);
  }
}
