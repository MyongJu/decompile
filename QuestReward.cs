﻿// Decompiled with JetBrains decompiler
// Type: QuestReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class QuestReward : IQuestReward
{
  protected bool _isCollected;

  public bool isCollected
  {
    get
    {
      return this._isCollected;
    }
  }

  public virtual bool IsCollected()
  {
    return this.isCollected;
  }

  public virtual void Claim()
  {
    this._isCollected = true;
  }

  public static List<QuestReward> CreateReward(int questId)
  {
    QuestInfo data = ConfigManager.inst.DB_Quest.GetData(questId);
    List<QuestReward> questRewardList = new List<QuestReward>();
    if (data.Hero_XP != 0)
      questRewardList.Add((QuestReward) new ExperienceReward(data.Hero_XP));
    ItemStaticInfo itemStaticInfo1 = ConfigManager.inst.DB_Items.GetItem(data.Reward_ID_1);
    QuestReward questReward1 = (QuestReward) null;
    ItemStaticInfo itemStaticInfo2 = ConfigManager.inst.DB_Items.GetItem(data.Reward_ID_2);
    QuestReward questReward2 = (QuestReward) null;
    if (itemStaticInfo1 != null)
    {
      questReward1 = (QuestReward) new ConsumableReward(data.Reward_ID_1, data.Reward_Value_1);
      if (itemStaticInfo1.Type == ItemBag.ItemType.speedup)
        questRewardList.Add(questReward1);
    }
    if (itemStaticInfo2 != null)
    {
      questReward2 = (QuestReward) new ConsumableReward(data.Reward_ID_2, data.Reward_Value_2);
      if (itemStaticInfo2.Type == ItemBag.ItemType.speedup)
        questRewardList.Add(questReward2);
    }
    if (questReward1 != null && questRewardList.IndexOf(questReward1) == -1)
      questRewardList.Add(questReward1);
    if (questReward2 != null && questRewardList.IndexOf(questReward2) == -1)
      questRewardList.Add(questReward2);
    if (data.Wood_Payout > 0)
      questRewardList.Add((QuestReward) new ResourceReward(CityManager.ResourceTypes.WOOD, data.Wood_Payout));
    if (data.Food_Payout > 0)
      questRewardList.Add((QuestReward) new ResourceReward(CityManager.ResourceTypes.FOOD, data.Food_Payout));
    if (data.Ore_Payout > 0)
      questRewardList.Add((QuestReward) new ResourceReward(CityManager.ResourceTypes.ORE, data.Ore_Payout));
    if (data.Silver_Payout > 0)
      questRewardList.Add((QuestReward) new SilverReward((long) data.Silver_Payout));
    if (data.Gold_PayOut > 0)
      questRewardList.Add((QuestReward) new GoldReward((long) data.Gold_PayOut));
    if (data.Alliance_XP > 0)
      questRewardList.Add((QuestReward) new AllianceXPReward(data.Alliance_XP));
    if (data.Alliance_Fund > 0)
      questRewardList.Add((QuestReward) new AllianceFundReward(data.Alliance_Fund));
    if (data.Honor > 0)
      questRewardList.Add((QuestReward) new AllianceHonourReward(data.Honor));
    if (data.power > 0)
      questRewardList.Add((QuestReward) new PowerReward(data.power));
    return questRewardList;
  }

  public virtual string GetRewardIconName()
  {
    return string.Empty;
  }

  public virtual string GetRewardTypeName()
  {
    return string.Empty;
  }

  public virtual string GetValueText()
  {
    return string.Empty;
  }

  public virtual int GetValue()
  {
    return 0;
  }
}
