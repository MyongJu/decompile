﻿// Decompiled with JetBrains decompiler
// Type: AllianceChangeFortressItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Text;
using UnityEngine;

public class AllianceChangeFortressItemRenderer : MonoBehaviour
{
  public UILabel m_NameLabel;
  public UIInput m_NameInput;
  public UISprite m_NameStatus;
  public UISprite m_NameGrey;
  private bool m_IsOkay;
  protected string m_configName;
  protected string m_previousName;
  protected bool m_editable;
  private System.Action m_modifyAction;
  private bool m_changed;

  public void SetData(string configName, string name, bool editable, System.Action action)
  {
    this.m_NameInput.onValidate = new UIInput.OnValidate(AllianceChangeFortressItemRenderer.OnInputValidator);
    this.m_configName = configName;
    this.m_previousName = name != null ? name : string.Empty;
    this.m_editable = editable;
    this.m_modifyAction = action;
    this.UpdateUI();
  }

  public static char OnInputValidator(string text, int charIndex, char addedChar)
  {
    text = text.Trim();
    if (Encoding.UTF8.GetBytes(text).Length >= 10)
      return char.MinValue;
    return addedChar;
  }

  public void OnNameInputChanged()
  {
    if (!this.m_changed)
      this.m_changed = this.Name != this.m_previousName;
    this.UpdateState();
  }

  private void UpdateState()
  {
    this.m_NameStatus.spriteName = string.Empty;
    this.m_IsOkay = false;
    byte[] bytes = Encoding.UTF8.GetBytes(this.m_NameInput.value.Trim().Replace("[", "【").Replace("]", "】").Replace("\n", string.Empty));
    if (!this.IsChanged)
    {
      this.m_IsOkay = true;
      this.m_NameStatus.spriteName = string.Empty;
    }
    else if (bytes.Length < 1 || bytes.Length > 10)
    {
      this.m_NameStatus.spriteName = "red_cross";
    }
    else
    {
      this.m_IsOkay = true;
      this.m_NameStatus.spriteName = "green_tick";
    }
  }

  public bool isOkay
  {
    get
    {
      return this.m_IsOkay;
    }
  }

  public bool IsChanged
  {
    get
    {
      return this.m_changed;
    }
  }

  public string Name
  {
    get
    {
      return this.m_NameInput.text;
    }
  }

  public bool Editable
  {
    get
    {
      return this.m_editable;
    }
  }

  public void DoModify()
  {
    if (!this.isOkay || !this.IsChanged)
      return;
    this.m_modifyAction();
  }

  private void UpdateUI()
  {
    if (this.m_editable)
    {
      this.m_NameLabel.text = this.m_configName;
      this.m_NameInput.gameObject.SetActive(true);
      this.m_NameGrey.gameObject.SetActive(false);
      this.m_NameInput.value = this.m_previousName;
      this.m_NameStatus.spriteName = string.Empty;
    }
    else
    {
      this.m_NameLabel.text = this.m_configName;
      this.m_NameInput.gameObject.SetActive(false);
      this.m_NameGrey.gameObject.SetActive(true);
      this.m_NameInput.value = string.Empty;
      this.m_NameStatus.spriteName = string.Empty;
    }
    this.UpdateState();
  }
}
