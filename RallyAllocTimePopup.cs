﻿// Decompiled with JetBrains decompiler
// Type: RallyAllocTimePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;
using UnityEngine;

public class RallyAllocTimePopup : MonoBehaviour
{
  private RallyAllocTimePopup.Data data = new RallyAllocTimePopup.Data();
  public System.Action<int> onSetTime;
  [SerializeField]
  private RallyAllocTimePopup.Panel panel;

  public void ShowPopup(int index = 0, int setTimerCount = 4)
  {
    this.gameObject.SetActive(true);
    for (int index1 = 0; index1 < setTimerCount && index1 < 6; ++index1)
      this.panel.checkersContainers[index1].gameObject.SetActive(true);
    for (int index1 = setTimerCount; index1 < 6; ++index1)
      this.panel.checkersContainers[index1].gameObject.SetActive(false);
    this.data.timeIndex = index;
    this.ClearToggle();
    this.panel.checkers[this.data.timeIndex].gameObject.SetActive(true);
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  public void ClickToggle(int i)
  {
    this.panel.checkers[this.data.timeIndex].gameObject.SetActive(false);
    this.panel.checkers[this.data.timeIndex = i].gameObject.SetActive(true);
  }

  private void ClearToggle()
  {
    for (int index = 0; index < 6; ++index)
      this.panel.checkers[index].gameObject.SetActive(false);
  }

  public void OnTime0Click()
  {
    this.ClickToggle(0);
  }

  public void OnTime1Click()
  {
    this.ClickToggle(1);
  }

  public void OnTime2Click()
  {
    this.ClickToggle(2);
  }

  public void OnTime3Click()
  {
    this.ClickToggle(3);
  }

  public void OnTime4Click()
  {
    this.ClickToggle(4);
  }

  public void OnTime5Click()
  {
    this.ClickToggle(5);
  }

  public void OnCancelButtonClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnSetButtonClick()
  {
    if (this.onSetTime != null)
      this.onSetTime(this.data.timeIndex);
    this.gameObject.SetActive(false);
  }

  protected struct Data
  {
    public int timeIndex;
  }

  [Serializable]
  protected class Panel
  {
    public Transform[] checkersContainers = new Transform[6];
    public UISprite[] checkers = new UISprite[6];
    public const int MAX_TOGGLE_COUNT = 6;
    public UIButton BT_cancel;
    public UIButton BT_set;
  }
}
