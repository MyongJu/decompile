﻿// Decompiled with JetBrains decompiler
// Type: DragonController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;

public class DragonController
{
  public const string DARK_ICON_PATH = "Texture/Dragon/icon_dragon_power_dark";
  public const string LIGHT_ICON_PATH = "Texture/Dragon/icon_dragon_power_light";

  public DragonController(DragonAPI api)
  {
    this.API = api;
    api.parent = this;
  }

  public DragonAPI API { get; private set; }

  public void Init()
  {
    this.API.Initialize();
  }

  public void Dispose()
  {
  }

  public void CollectDragon(long building_id, System.Action<object> callBack = null)
  {
    this.API.CollectDragon(PlayerData.inst.uid, (long) CityManager.inst.GetCityID(), building_id, callBack);
  }

  public void RenameDragon(string name, System.Action<object> callBack = null)
  {
    this.API.RenameDragon(PlayerData.inst.uid, name, callBack);
  }

  public void ClaimDragonLevelup(int level)
  {
    this.API.ClaimLevelUp(PlayerData.inst.uid, level);
  }

  public void UpgradeSkill(int skillID, System.Action<object> callBack = null)
  {
    this.API.UpgradeSkill(PlayerData.inst.uid, (long) skillID, callBack);
  }

  public void RetSkill(int skillID, System.Action<object> callBack = null)
  {
    this.API.RetSkill((long) skillID, callBack);
  }

  public int GetTopLevelofGroup(int groupID, bool all = false)
  {
    int num = 0;
    List<long> skillDataByGroupId = DBManager.inst.DB_DragonSkill.GetSkillDataByGroupId(groupID);
    if (skillDataByGroupId == null)
      return 0;
    for (int index = 0; index < skillDataByGroupId.Count; ++index)
    {
      ConfigDragonSkillMainInfo skillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo((int) skillDataByGroupId[index]);
      if ((all || skillMainInfo.CanLearn) && skillMainInfo.level > num)
        num = skillMainInfo.level;
    }
    return num;
  }

  public ConfigDragonSkillMainInfo GetLearnedTopLevelSkillOfGroup(int groupID)
  {
    int num = 0;
    ConfigDragonSkillMainInfo dragonSkillMainInfo = (ConfigDragonSkillMainInfo) null;
    List<long> skillDataByGroupId = DBManager.inst.DB_DragonSkill.GetSkillDataByGroupId(groupID);
    if (skillDataByGroupId != null)
    {
      for (int index = 0; index < skillDataByGroupId.Count; ++index)
      {
        ConfigDragonSkillMainInfo skillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo((int) skillDataByGroupId[index]);
        if (skillMainInfo.level > num)
        {
          num = skillMainInfo.level;
          dragonSkillMainInfo = skillMainInfo;
        }
      }
    }
    return dragonSkillMainInfo;
  }

  public int GetTotalLevelofGroup(int groupID)
  {
    return ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfosofGroup(groupID).Count;
  }

  public bool IsFull(string usability)
  {
    int skillSlot = ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(PlayerData.inst.dragonData.Level).skill_slot;
    List<int> skillBar = PlayerData.inst.dragonData.Skills.GetSkillBar(usability);
    if (skillBar == null)
      return false;
    for (int index = 0; index < skillSlot; ++index)
    {
      if (index >= skillBar.Count || skillBar[index] == 0)
        return false;
    }
    return true;
  }
}
