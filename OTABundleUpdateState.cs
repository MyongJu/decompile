﻿// Decompiled with JetBrains decompiler
// Type: OTABundleUpdateState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class OTABundleUpdateState : LoadBaseState
{
  private Stack<string> otaversionStack = new Stack<string>();
  private HashSet<string> cachingOtaBundle = new HashSet<string>();
  private const int maxthread = 1;
  private int startTime;

  public OTABundleUpdateState(int step)
    : base(step)
  {
  }

  public override string Key
  {
    get
    {
      return nameof (OTABundleUpdateState);
    }
  }

  protected override SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.OTAUpdate;
    }
  }

  protected override void Prepare()
  {
    base.Prepare();
    BundleManager.Instance.onBundleLoaded += new System.Action<string, bool>(this.OnBundleLoadFinished);
    if (!AssetManager.IsLoadAssetFromBundle)
    {
      this.Finish();
    }
    else
    {
      this.InitOTAList();
      this.BeginUpdateOTABundle();
    }
  }

  private void OnBundleLoadFinished(string name, bool success)
  {
    if (!success)
      return;
    string str = name;
    int length = name.IndexOf(".assetbundle");
    if (length != -1)
      str = name.Substring(0, length);
    if (!this.cachingOtaBundle.Contains(str))
      return;
    this.cachingOtaBundle.Remove(str);
    if (this.otaversionStack.Count > 0)
    {
      string bundlename = this.otaversionStack.Pop();
      this.cachingOtaBundle.Add(bundlename);
      BundleManager.Instance.CacheBundle(bundlename);
    }
    else
    {
      if (this.cachingOtaBundle.Count != 0)
        return;
      this.Finish();
    }
  }

  private void InitOTAList()
  {
    List<string> list = new List<string>();
    using (Dictionary<string, string>.Enumerator enumerator = VersionManager.Instance.Newversionlist.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, string> current = enumerator.Current;
        if (current.Key.Contains("ota+"))
        {
          string str = current.Key.Substring(0, current.Key.IndexOf(".assetbundle"));
          list.Add(str);
        }
      }
    }
    OTABundleUpdateHelper.GetOrderedOtaversionStack(list, ref this.otaversionStack);
  }

  private void BeginUpdateOTABundle()
  {
    for (int index = 0; index < 1 && this.otaversionStack.Count > 0; ++index)
    {
      string bundlename = this.otaversionStack.Pop();
      this.cachingOtaBundle.Add(bundlename);
      BundleManager.Instance.CacheBundle(bundlename);
    }
  }

  protected override void Finish()
  {
    if (this.IsDestroy)
      return;
    base.Finish();
    BundleManager.Instance.onBundleLoaded -= new System.Action<string, bool>(this.OnBundleLoadFinished);
  }

  private void UpdateEventHandler(double t)
  {
    if (this.startTime != 0)
      return;
    this.startTime = -1;
  }
}
