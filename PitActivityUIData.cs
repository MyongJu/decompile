﻿// Decompiled with JetBrains decompiler
// Type: PitActivityUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class PitActivityUIData : ActivityBaseUIData
{
  public override ActivityBaseUIData.ActivityType Type
  {
    get
    {
      return ActivityBaseUIData.ActivityType.Pit;
    }
  }

  private PitActivityData PitActivityData
  {
    get
    {
      return this.Data as PitActivityData;
    }
  }

  public override string EventKey
  {
    get
    {
      return "abyss";
    }
  }

  public override string TimePre
  {
    get
    {
      return "event_fire_kingdom_opens_in";
    }
  }

  public override IconData GetNormalIconData()
  {
    IconData iconData = new IconData();
    iconData.image = "Texture/Events/icon_pit_explore";
    iconData.contents = new string[2];
    iconData.contents[0] = Utils.XLAT("event_fire_kingdom_title");
    iconData.contents[1] = Utils.XLAT("event_fire_kingdom_one_line_description");
    iconData.Data = (object) this;
    return iconData;
  }

  public override int StartTime
  {
    get
    {
      return this.PitActivityData.StartTime;
    }
  }

  public override int EndTime
  {
    get
    {
      return this.PitActivityData.EndTime;
    }
  }

  public override bool ShowADTime
  {
    get
    {
      return false;
    }
  }

  public override IconData GetADIconData()
  {
    IconData iconData = new IconData();
    iconData.image = "Texture/Events/pit_explore_image_big";
    iconData.contents = new string[2];
    iconData.contents[0] = Utils.XLAT("event_fire_kingdom_title");
    iconData.contents[1] = Utils.XLAT("event_fire_kingdom_one_line_description");
    return iconData;
  }

  public override void DisplayActivityDetail()
  {
    PitExplorePayload.Instance.ShowPitActivityScene();
  }
}
