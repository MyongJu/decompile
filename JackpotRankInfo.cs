﻿// Decompiled with JetBrains decompiler
// Type: JackpotRankInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class JackpotRankInfo
{
  public string winner;
  public string winnerAllianceAcronym;
  public string winnerPortrait;
  public string winnerImage;
  public int winnerLordTitleId;
  public int winnerUid;
  public int winTime;
  public int winnerKingdom;
  public long winGoldAmount;
}
