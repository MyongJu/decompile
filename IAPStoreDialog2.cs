﻿// Decompiled with JetBrains decompiler
// Type: IAPStoreDialog2
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Funplus;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class IAPStoreDialog2 : UI.Dialog
{
  private List<IAPStoreRenderer> m_ItemList = new List<IAPStoreRenderer>();
  private string m_EnterSource = string.Empty;
  public UILabel m_GoldCount;
  public GameObject m_SuitPrefab;
  public GameObject m_GoldPrefab;
  public UITable m_Table;
  public UIScrollView m_ScrollView;
  public UITexture m_BgTextrue;
  public UIButton myCard;
  private bool m_NeedToTip;
  private bool m_Reposition;
  private int m_GroupId;
  private int m_GoldId;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    IAPStoreDialog.Parameter parameter = orgParam as IAPStoreDialog.Parameter;
    if (parameter != null)
    {
      int.TryParse(parameter.groupId, out this.m_GroupId);
      this.m_EnterSource = parameter.enterSource == null ? string.Empty : parameter.enterSource;
    }
    AccountManager.Instance.DelayShowBind = true;
    DBManager.inst.DB_User.onDataChanged += new System.Action<long>(this.OnUserDataChanged);
    IAPStorePackagePayload.Instance.OnPurchaseSucceed += new System.Action<object>(this.OnPaymentSuccess);
    IAPStorePackagePayload.Instance.OnPackageDataChanged += new System.Action(this.OnPackageDataChanged);
    if ((UnityEngine.Object) this.myCard != (UnityEngine.Object) null)
      NGUITools.SetActive(this.myCard.gameObject, CustomDefine.IsMyCardDefine());
    this.UpdateListUI();
  }

  public void ShowMyCard()
  {
    PaymentManager.Instance.ShowMyCard();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.ClearItemList();
    DBManager.inst.DB_User.onDataChanged -= new System.Action<long>(this.OnUserDataChanged);
    IAPStorePackagePayload.Instance.OnPurchaseSucceed -= new System.Action<object>(this.OnPaymentSuccess);
    IAPStorePackagePayload.Instance.OnPackageDataChanged -= new System.Action(this.OnPackageDataChanged);
    AccountManager.Instance.DelayShowBind = false;
    if (!this.m_NeedToTip)
      return;
    this.m_NeedToTip = false;
    AccountManager.Instance.ShowBindPop();
  }

  private void LateUpdate()
  {
    if (!this.m_Reposition)
      return;
    this.m_Reposition = false;
    this.Reposition();
    this.FocusPackage(this.m_GroupId);
    this.FocusGold(this.m_GoldId);
  }

  private void OnUserDataChanged(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.m_GoldCount.text = Utils.FormatThousands(PlayerData.inst.userData.currency.gold.ToString());
  }

  private void UpdateListUI()
  {
    this.OnUserDataChanged(PlayerData.inst.uid);
    this.ClearItemList();
    this.UpdatePackage();
    this.UpdateGold();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_BgTextrue, "Texture/IAP/bg_iap_package_1", (System.Action<bool>) null, false, true, string.Empty);
    this.m_Reposition = true;
  }

  private void Reposition()
  {
    this.m_Table.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  private void ClearItemList()
  {
    using (List<IAPStoreRenderer>.Enumerator enumerator = this.m_ItemList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IAPStoreRenderer current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ItemList.Clear();
  }

  private void UpdatePackage()
  {
    List<IAPStorePackage> availablePackages = IAPStorePackagePayload.Instance.GetAvailablePackages();
    if (availablePackages.Count <= 0)
      return;
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_SuitPrefab);
    gameObject.SetActive(true);
    gameObject.transform.parent = this.m_Table.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    IAPStorePackageSuit component = gameObject.GetComponent<IAPStorePackageSuit>();
    component.SetData(availablePackages);
    component.OnPurchaseBegin = new System.Action(this.OnPurchaseBegin);
    component.OnPurchaseEnd = new System.Action<int>(this.OnPurchasePackageEnd);
    this.m_ItemList.Add((IAPStoreRenderer) component);
  }

  private void UpdateGold()
  {
    foreach (IAPGoldInfo data in ConfigManager.inst.DB_IAPGold.GetDataList())
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_GoldPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_Table.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localScale = Vector3.one;
      IAPStoreGoldRenderer component = gameObject.GetComponent<IAPStoreGoldRenderer>();
      component.SetData(data, new System.Action(this.OnPurchaseBegin), new System.Action<int>(this.OnPurchaseGoldEnd));
      this.m_ItemList.Add((IAPStoreRenderer) component);
    }
  }

  private void PushBIOnPaymentSuccess(object result)
  {
    Hashtable hashtable = result as Hashtable;
    IAPPackagePurchaseBIItem packagePurchaseBiItem = new IAPPackagePurchaseBIItem();
    string properties = string.Empty;
    if (hashtable != null && hashtable.ContainsKey((object) "package_id"))
    {
      packagePurchaseBiItem.d_c1 = new IAPPackagePurchaseBICell();
      packagePurchaseBiItem.d_c1.key = "package_id";
      packagePurchaseBiItem.d_c1.value = hashtable[(object) "package_id"].ToString();
      packagePurchaseBiItem.d_c2 = new IAPPackagePurchaseBICell();
      packagePurchaseBiItem.d_c2.key = "enterSource";
      packagePurchaseBiItem.d_c2.value = this.m_EnterSource;
      packagePurchaseBiItem.d_c3 = new IAPPackagePurchaseBICell();
      packagePurchaseBiItem.d_c3.key = "is_active";
      packagePurchaseBiItem.d_c3.value = !UIManager.inst.ExistAutoOpened ? "0" : "1";
      properties = Utils.Object2Json((object) packagePurchaseBiItem);
      if (Application.isEditor)
        return;
    }
    if (!FunplusSdk.Instance.IsSdkInstalled())
      return;
    FunplusBi.Instance.TraceEvent("package_payment_success", properties);
  }

  private void OnPaymentSuccess(object result)
  {
    using (List<IAPStoreRenderer>.Enumerator enumerator = this.m_ItemList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UpdateUI();
    }
    this.m_NeedToTip = true;
    this.PushBIOnPaymentSuccess(result);
  }

  private void OnPackageDataChanged()
  {
    this.UpdateListUI();
  }

  private void FocusPackage(int groupId)
  {
    IAPStorePackageSuit packageSuit = this.FindPackageSuit();
    if (!((UnityEngine.Object) packageSuit != (UnityEngine.Object) null))
      return;
    packageSuit.FocusPackage(groupId);
    this.FocusChild(packageSuit.transform);
  }

  private void FocusGold(int goldId)
  {
    IAPStoreGoldRenderer goldItem = this.FindGoldItem(goldId);
    if (!((UnityEngine.Object) goldItem != (UnityEngine.Object) null))
      return;
    this.FocusChild(goldItem.transform);
  }

  private void FocusChild(Transform t)
  {
    Vector3[] worldCorners = this.m_ScrollView.panel.worldCorners;
    Vector3 position = (worldCorners[2] + worldCorners[0]) * 0.5f;
    Vector3 vector3 = this.m_ScrollView.panel.transform.InverseTransformPoint(t.position) - this.m_ScrollView.panel.transform.InverseTransformPoint(position);
    this.m_ScrollView.panel.transform.localPosition = this.m_ScrollView.panel.transform.localPosition - vector3;
    Vector4 clipOffset = (Vector4) this.m_ScrollView.panel.clipOffset;
    clipOffset.x += vector3.x;
    clipOffset.y += vector3.y;
    this.m_ScrollView.panel.clipOffset = (Vector2) clipOffset;
    Bounds bounds = this.m_ScrollView.bounds;
    this.m_ScrollView.MoveRelative(this.m_ScrollView.panel.CalculateConstrainOffset((Vector2) bounds.min, (Vector2) bounds.max));
  }

  private IAPStorePackageSuit FindPackageSuit()
  {
    for (int index = 0; index < this.m_ItemList.Count; ++index)
    {
      IAPStorePackageSuit storePackageSuit = this.m_ItemList[index] as IAPStorePackageSuit;
      if ((UnityEngine.Object) storePackageSuit != (UnityEngine.Object) null)
        return storePackageSuit;
    }
    return (IAPStorePackageSuit) null;
  }

  private IAPStoreGoldRenderer FindGoldItem(int goldId)
  {
    for (int index = 0; index < this.m_ItemList.Count; ++index)
    {
      IAPStoreGoldRenderer storeGoldRenderer = this.m_ItemList[index] as IAPStoreGoldRenderer;
      if ((UnityEngine.Object) storeGoldRenderer != (UnityEngine.Object) null)
      {
        IAPGoldInfo goldInfo = storeGoldRenderer.GoldInfo;
        if (goldId == goldInfo.internalId)
          return storeGoldRenderer;
      }
    }
    return (IAPStoreGoldRenderer) null;
  }

  private void OnPurchaseBegin()
  {
    this.m_GroupId = 0;
    this.m_GoldId = 0;
  }

  private void OnPurchasePackageEnd(int groupId)
  {
    this.m_GroupId = groupId;
  }

  private void OnPurchaseGoldEnd(int goldId)
  {
    this.m_GoldId = goldId;
  }
}
