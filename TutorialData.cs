﻿// Decompiled with JetBrains decompiler
// Type: TutorialData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TutorialData
{
  public float smoothTime = 0.5f;
  public bool ignoreCamera = true;
  public Vector2 ctSize = new Vector2(200f, 200f);
  public Vector3 arrowScale = new Vector3(2.4f, 2.4f, 1f);
  public Vector2 maskSize = new Vector2(200f, 200f);
  public float cityCameraZoom = 800f;
  public string name;
  public string parent;
  public int mode;
  public int npcmode;
  public string npcText;
  public int npcindex;
  public string npcImage;
  public bool isMiniMapCamera;
  public Vector3 cameraPos;
  public Vector3 ctPos;
  public Vector3 arrowPos;
  public Quaternion arrowRot;
}
