﻿// Decompiled with JetBrains decompiler
// Type: LotteryDiyItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class LotteryDiyItemRenderer : MonoBehaviour
{
  private const float TIPS_DISPLAY_TRIGGER_TIME = 0.8f;
  public UIEventTrigger eventTrigger;
  public UITexture itemTexture;
  public UILabel itemCount;
  public GameObject itemChecked;
  public UISprite itemScrollChipTip;
  public System.Action<LotteryDiyBaseData.RewardData> onItemSinglePressed;
  public System.Action<LotteryDiyBaseData.RewardData, LotteryDiyPayload.LotteryArea, Vector3> onItemTipsDisplay;
  private Coroutine _doubleClickRoutine;
  private float _clickTimer;
  private LotteryDiyPayload.LotteryArea _lotteryArea;
  private LotteryDiyBaseData.RewardData _rewardData;

  public LotteryDiyPayload.LotteryArea CurrentLotteryArea
  {
    get
    {
      return this._lotteryArea;
    }
  }

  public LotteryDiyBaseData.RewardData RewardData
  {
    get
    {
      return this._rewardData;
    }
  }

  public virtual void SetData(LotteryDiyBaseData.RewardData rewardData, LotteryDiyPayload.LotteryArea lotteryArea, int index = -1, bool isHistoryMode = false)
  {
    this._rewardData = rewardData;
    this._lotteryArea = lotteryArea;
    this.UpdateUI();
    if (!((UnityEngine.Object) this.eventTrigger != (UnityEngine.Object) null))
      return;
    this.eventTrigger.onPress.Clear();
    this.eventTrigger.onPress.Add(new EventDelegate((EventDelegate.Callback) (() => this.StartCoroutine("OnItemPressHandler"))));
    this.eventTrigger.onRelease.Clear();
    this.eventTrigger.onRelease.Add(new EventDelegate((EventDelegate.Callback) (() => this.StartCoroutine("OnItemReleaseHandler"))));
  }

  public void UpdateLotteryItemStatus(bool show)
  {
    if (!((UnityEngine.Object) this.itemChecked != (UnityEngine.Object) null))
      return;
    NGUITools.SetActive(this.itemChecked, show);
  }

  public void OnItemSingleClicked()
  {
    if (this._doubleClickRoutine != null)
    {
      this.StopCoroutine(this._doubleClickRoutine);
      this._doubleClickRoutine = (Coroutine) null;
    }
    this._doubleClickRoutine = this.StartCoroutine(this.SingleClickTimeout());
  }

  public void OnItemDoubleClicked()
  {
    if (this._doubleClickRoutine != null)
    {
      this.StopCoroutine(this._doubleClickRoutine);
      this._doubleClickRoutine = (Coroutine) null;
    }
    if (this._rewardData == null)
      return;
    this._clickTimer = Time.time;
    Utils.ShowItemTip(this._rewardData.itemId, this.transform, 0L, 0L, 0);
  }

  [DebuggerHidden]
  private IEnumerator SingleClickTimeout()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LotteryDiyItemRenderer.\u003CSingleClickTimeout\u003Ec__Iterator7A()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator OnItemPressHandler()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LotteryDiyItemRenderer.\u003COnItemPressHandler\u003Ec__Iterator7B()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator OnItemReleaseHandler()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LotteryDiyItemRenderer.\u003COnItemReleaseHandler\u003Ec__Iterator7C()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void UpdateUI()
  {
    if (this._rewardData == null)
    {
      this.itemTexture.mainTexture = (Texture) null;
      NGUITools.SetActive(this.itemTexture.gameObject, false);
      NGUITools.SetActive(this.itemCount.transform.parent.gameObject, false);
    }
    else
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._rewardData.itemId);
      if (itemStaticInfo != null && ((UnityEngine.Object) this.itemTexture.mainTexture == (UnityEngine.Object) null || this.itemTexture.mainTexture.name != itemStaticInfo.Image))
      {
        NGUITools.SetActive(this.itemTexture.gameObject, true);
        BuilderFactory.Instance.HandyBuild((UIWidget) this.itemTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      }
      this.itemCount.text = this._rewardData.number.ToString();
      NGUITools.SetActive(this.itemCount.transform.parent.gameObject, true);
    }
  }

  private void CheckScrollChip()
  {
    if (this._rewardData == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._rewardData.itemId);
    if (itemStaticInfo == null)
      return;
    ConfigEquipmentScrollCombine equipmentScrollCombine = (ConfigEquipmentScrollCombine) null;
    ConfigEquipmentScrollChipInfo equipmentScrollChipInfo = (ConfigEquipmentScrollChipInfo) null;
    bool flag = false;
    bool state = false;
    if (itemStaticInfo.Type == ItemBag.ItemType.equipment_scroll_chip)
    {
      equipmentScrollCombine = ConfigManager.inst.GetEquipmentScrollChipCombine(BagType.Hero);
      equipmentScrollChipInfo = ConfigManager.inst.GetEquipmentScrollChip(BagType.Hero).GetDataByItemID(this._rewardData.itemId);
      state = true;
    }
    else if (itemStaticInfo.Type == ItemBag.ItemType.dk_equipment_scroll_chip)
    {
      equipmentScrollCombine = ConfigManager.inst.GetEquipmentScrollChipCombine(BagType.DragonKnight);
      equipmentScrollChipInfo = ConfigManager.inst.GetEquipmentScrollChip(BagType.DragonKnight).GetDataByItemID(this._rewardData.itemId);
      state = true;
    }
    if (equipmentScrollChipInfo != null && equipmentScrollCombine != null && equipmentScrollCombine.GetCombineTarget(equipmentScrollChipInfo.internalId) != null)
      flag = equipmentScrollCombine.GetCombineTarget(equipmentScrollChipInfo.internalId).Count == 1;
    this.itemScrollChipTip.spriteName = !flag ? "icon_corner_mark_question" : "icon_corner_mark_fragment";
    NGUITools.SetActive(this.itemScrollChipTip.gameObject, state);
  }
}
