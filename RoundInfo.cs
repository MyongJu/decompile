﻿// Decompiled with JetBrains decompiler
// Type: RoundInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class RoundInfo : MonoBehaviour
{
  private List<GameObject> pool = new List<GameObject>();
  private int id;
  public GameObject roundHeader;
  public GameObject roundContent;
  public UILabel roundLbl;
  public UISprite togglemark;

  public event System.Action toggleRound;

  public void SeedData(int roundid, RoundInfo.Data d)
  {
    this.id = roundid;
    float num = (float) this.roundLbl.height * 1.3f;
    float y = this.roundLbl.transform.localPosition.y;
    this.roundHeader.GetComponentInChildren<UILabel>().text = ScriptLocalization.Get("battle_report_damagedetails_uppercase_round", true) + " " + (roundid + 1).ToString();
    using (List<string>.Enumerator enumerator = d.content.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.roundLbl.gameObject, this.roundLbl.transform.parent);
        this.pool.Add(gameObject);
        Vector3 localPosition = this.roundLbl.transform.localPosition;
        localPosition.y = y;
        gameObject.transform.localPosition = localPosition;
        y -= num;
        gameObject.SetActive(true);
        gameObject.GetComponent<UILabel>().text = current;
      }
    }
    this.roundLbl.gameObject.SetActive(false);
    this.ToggleRoundInfo(roundid == 0);
  }

  public void ToggleRoundInfo(bool show)
  {
    this.roundContent.SetActive(show);
    this.togglemark.transform.localEulerAngles = this.roundContent.activeSelf ? new Vector3(0.0f, 0.0f, 180f) : Vector3.zero;
  }

  public void OnTogglePressed()
  {
    this.ToggleRoundInfo(!this.roundContent.activeSelf);
    if (this.toggleRound == null)
      return;
    this.toggleRound();
  }

  public void Reset()
  {
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        PrefabManagerEx.Instance.Destroy(current);
      }
    }
    this.pool.Clear();
    this.roundContent.SetActive(true);
    this.roundLbl.gameObject.SetActive(true);
    this.togglemark.transform.localEulerAngles = Vector3.zero;
  }

  public class Data
  {
    public List<string> content;
  }
}
