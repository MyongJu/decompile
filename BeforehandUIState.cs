﻿// Decompiled with JetBrains decompiler
// Type: BeforehandUIState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class BeforehandUIState : LoadBaseState
{
  private List<string> preloadAssets = new List<string>()
  {
    "Prefab/UI/Dialogs/BuildingConstruction",
    "Prefab/UI/Dialogs/Construction/ConstructionSelectDlg",
    "Prefab/UI/Dialogs/Barracks/TrainTroopDlg",
    "Prefab/UI/Dialogs/QuestDlg"
  };
  private int preloadCount;

  public BeforehandUIState(int step)
    : base(step)
  {
  }

  public override string Key
  {
    get
    {
      return nameof (BeforehandUIState);
    }
  }

  protected override SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.LoadPreUI;
    }
  }

  protected override void Prepare()
  {
    base.Prepare();
    CustomIconCache.TryClearCache();
    Oscillator.Instance.updateEvent += new System.Action<double>(this.UpdateEventHandler);
    BundleManager.Instance.LoadBasicBundle("static+basic_common");
    BundleManager.Instance.LoadBasicBundle("static+basic_gui2");
    BundleManager.Instance.LoadBasicBundle("static+scene_ui");
    if (this.preloadAssets.Count == 0)
    {
      this.LoadCity();
    }
    else
    {
      AssetPreloadManager.Instance.onAssetPreloadFinished += new System.Action<string, object>(this.OnAssetPreloadFinish);
      using (List<string>.Enumerator enumerator = this.preloadAssets.GetEnumerator())
      {
        while (enumerator.MoveNext())
          AssetPreloadManager.Instance.PreloadAsset(enumerator.Current);
      }
    }
  }

  private void OnAssetPreloadFinish(string name, object obj)
  {
    ++this.preloadCount;
    if (this.preloadAssets.Count != this.preloadCount)
      return;
    AssetPreloadManager.Instance.onAssetPreloadFinished -= new System.Action<string, object>(this.OnAssetPreloadFinish);
    this.LoadCity();
  }

  private void LoadCity()
  {
    this.Finish();
  }

  private void UpdateEventHandler(double t)
  {
    GameEngine.Instance.iTweenValue = Mathf.Lerp(GameEngine.Instance.iTweenValue, SplashDataConfig.GetValue(this.CurrentPhase), 0.1f);
    this.RefreshProgress(GameEngine.Instance.iTweenValue);
  }

  protected override void OnDispose()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.updateEvent -= new System.Action<double>(this.UpdateEventHandler);
  }

  protected override void Finish()
  {
    if (this.IsDestroy)
      return;
    base.Finish();
    this.Preloader.OnLoadUIFinish();
    if (Oscillator.IsAvailable)
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.UpdateEventHandler);
    this.RefreshProgress();
  }
}
