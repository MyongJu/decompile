﻿// Decompiled with JetBrains decompiler
// Type: CacheManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;

public class CacheManager
{
  private ConcurrentDictionary<string, string> cacheMap = new ConcurrentDictionary<string, string>();
  private ReaderWriterLockSlim cacheLock = new ReaderWriterLockSlim();
  private static CacheManager _instance;

  public static CacheManager Instance
  {
    get
    {
      if (CacheManager._instance == null)
      {
        CacheManager._instance = new CacheManager();
        if (CacheManager._instance == null)
          throw new ArgumentException("lost of cache manager");
      }
      return CacheManager._instance;
    }
  }

  public void Init()
  {
    this.cacheMap.Clear();
    if (Directory.Exists(AssetConfig.BundleCacheDir))
    {
      foreach (FileInfo file in new DirectoryInfo(AssetConfig.BundleCacheDir).GetFiles())
      {
        string[] strArray = file.Name.Split('=');
        if (strArray.Length == 2)
        {
          if (file.Length < 10L)
          {
            file.Delete();
          }
          else
          {
            string str = strArray[1];
            string strB = strArray[0];
            string version = VersionManager.Instance.GetVersion(str, VersionType.Bundle);
            if (version != null)
            {
              if (version.CompareTo(strB) != 0)
                file.Delete();
              else
                this.cacheMap.TryAdd(str, strB);
            }
          }
        }
      }
    }
    else
      Directory.CreateDirectory(AssetConfig.BundleCacheDir);
  }

  public void WriteToCache(string bundlename, byte[] bytes, VersionType vt = VersionType.Bundle)
  {
    string name = (string) null;
    string path = (string) null;
    string str = (string) null;
    switch (vt)
    {
      case VersionType.Bundle:
        name = bundlename + ".assetbundle";
        path = NetApi.inst.BuildLocalBundlePath(BundleManager.Instance.GetFullBundleName(bundlename));
        str = VersionManager.Instance.GetVersion(name, VersionType.Bundle);
        break;
      case VersionType.Config:
        name = bundlename;
        str = VersionManager.Instance.GetVersion(name, VersionType.Bundle);
        path = NetApi.inst.BuildLocalBundlePath(str + (object) '=' + name);
        break;
    }
    AsyncCallback userCallback = new AsyncCallback(this.WriteCallback);
    FileStream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None, bytes.Length, true);
    fileStream.BeginWrite(bytes, 0, bytes.Length, userCallback, (object) new CacheManager.States()
    {
      fs = fileStream,
      content = bytes,
      md5 = str,
      path = path,
      filename = name
    });
  }

  public void WriteCallback(IAsyncResult asyncResult)
  {
    CacheManager.States asyncState = (CacheManager.States) asyncResult.AsyncState;
    FileStream fs = asyncState.fs;
    fs.EndWrite(asyncResult);
    fs.Close();
    this.Add2CacheMap(asyncState.filename, asyncState.md5);
  }

  public void Add2CacheMap(string filename, string md5)
  {
    if (this.cacheMap.ContainsKey(filename))
      this.cacheMap[filename] = md5;
    else
      this.cacheMap.TryAdd(filename, md5);
  }

  public bool IsCached(string bundlename, string version)
  {
    return this.cacheMap.ContainsKey(bundlename) && this.cacheMap[bundlename] == version;
  }

  public void Dispose()
  {
    this.cacheMap.Clear();
    CacheManager._instance = (CacheManager) null;
  }

  private class States
  {
    public FileStream fs;
    public byte[] content;
    public string md5;
    public string path;
    public string filename;
  }
}
