﻿// Decompiled with JetBrains decompiler
// Type: BundleManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using SevenZip;
using SevenZip.Compression.LZMA;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using UnityEngine;

public class BundleManager : MonoBehaviour
{
  public Dictionary<string, BundleManager.LoadType> updatingBundle = new Dictionary<string, BundleManager.LoadType>();
  private ReaderWriterLockSlim cacheLock = new ReaderWriterLockSlim();
  private ConcurrentDictionary<string, BundleManager.UnzipRecord> upzipMark = new ConcurrentDictionary<string, BundleManager.UnzipRecord>();
  private Dictionary<string, AssetBundle> ablist = new Dictionary<string, AssetBundle>();
  private bool hasrestart = true;
  private const int cachesize = 50;
  private static BundleManager _instance;
  public System.Action<LoadingBundle> onBundleLoadFinished;
  public System.Action<string, bool> onBundleLoaded;
  public System.Action<string, LoaderBatch> onBundleBeginToDownload;
  private FrequencyCache<AssetBundle> abCache;
  private bool needrestart;

  public static BundleManager Instance
  {
    get
    {
      if ((UnityEngine.Object) BundleManager._instance == (UnityEngine.Object) null)
      {
        GameObject gameObject = new GameObject();
        BundleManager._instance = gameObject.AddComponent<BundleManager>();
        gameObject.AddComponent<DontDestroy>();
        gameObject.name = nameof (BundleManager);
        if ((UnityEngine.Object) null == (UnityEngine.Object) BundleManager._instance)
          throw new ArgumentException("BundleManager hasn't been created yet.");
      }
      return BundleManager._instance;
    }
  }

  public FrequencyCache<AssetBundle> AbCache
  {
    get
    {
      if (this.abCache == null)
        this.abCache = new FrequencyCache<AssetBundle>(50, (System.Action<AssetBundle>) (ab => ab.Unload(false)));
      return this.abCache;
    }
  }

  private void GetBundleFromCache(string bundlename, out AssetBundle ab)
  {
    BundleManager.Instance.Ablist.TryGetValue(bundlename, out ab);
    if ((UnityEngine.Object) null != (UnityEngine.Object) ab)
      return;
    this.abCache.TryGetObject(bundlename, out ab);
  }

  public void LoadBundle(string bundlename)
  {
    AssetBundle ab1 = (AssetBundle) null;
    this.GetBundleFromCache(bundlename, out ab1);
    if ((UnityEngine.Object) null != (UnityEngine.Object) ab1)
      this.LoadBundleFinish(bundlename, ab1);
    else if (this.CheckVersionList(bundlename))
    {
      AssetBundle ab2 = this.LoadBundleFromLocal(bundlename);
      if ((UnityEngine.Object) null == (UnityEngine.Object) ab2)
        return;
      this.LoadBundleFinish(bundlename, ab2);
    }
    else
    {
      string filename = bundlename + ".assetbundle";
      if (this.CheckIfUpdating(filename, BundleManager.LoadType.Load))
        return;
      LoaderBatch batch = LoaderManager.inst.PopBatch();
      batch.AddAsset(filename, NetApi.inst.BuildBundleUrl(filename), (Hashtable) null, false);
      batch.finishedEvent += (System.Action<LoaderBatch>) (b =>
      {
        LoaderInfo resultInfo = b.GetResultInfo(filename);
        if (resultInfo != null && resultInfo.ResData != null)
        {
          ThreadPool.QueueUserWorkItem(new WaitCallback(this.PostprocessBundleUseMultiThreads), (object) new BundleManager.States()
          {
            bundlename = bundlename,
            content = resultInfo.ResData,
            type = BundleManager.LoadType.Load
          });
        }
        else
        {
          if (this.updatingBundle.ContainsKey(bundlename))
            this.updatingBundle.Remove(bundlename);
          D.error((object) ("load remote bundle failed " + filename));
          if (this.onBundleLoaded != null)
            this.onBundleLoaded(bundlename, false);
          else
            NetWorkDetector.Instance.RetryTip((System.Action) (() => this.LoadBundle(bundlename)), ScriptLocalization.Get("network_error_try_again_button", true), ScriptLocalization.Get("network_error_description", true));
        }
      });
      LoaderManager.inst.Add(batch, false, false);
    }
  }

  public void CacheBundle(string bundlename)
  {
    if (!this.CheckVersionList(bundlename))
    {
      string filename = bundlename + ".assetbundle";
      string fullBundleName = this.GetFullBundleName(bundlename);
      if (this.CheckIfUpdating(fullBundleName, BundleManager.LoadType.Cache))
        return;
      NetApi.inst.BuildLocalBundlePath(fullBundleName);
      LoaderBatch batch1 = LoaderManager.inst.PopBatch();
      batch1.AddAsset(filename, NetApi.inst.BuildStreamingAssetsCacheUrl(fullBundleName), (Hashtable) null, false);
      batch1.finishedEvent += (System.Action<LoaderBatch>) (b =>
      {
        LoaderInfo resultInfo1 = b.GetResultInfo(filename);
        if (resultInfo1 != null)
        {
          if (resultInfo1.ResData != null)
          {
            ThreadPool.QueueUserWorkItem(new WaitCallback(this.PostprocessBundleUseMultiThreads), (object) new BundleManager.States()
            {
              bundlename = bundlename,
              content = resultInfo1.ResData,
              type = BundleManager.LoadType.Cache
            });
          }
          else
          {
            LoaderBatch batch = LoaderManager.inst.PopBatch();
            batch.AddAsset(filename, NetApi.inst.BuildBundleUrl(filename), (Hashtable) null, false);
            batch.finishedEvent += (System.Action<LoaderBatch>) (b2 =>
            {
              LoaderInfo resultInfo2 = b2.GetResultInfo(filename);
              if (resultInfo2 != null && resultInfo2.ResData != null)
              {
                ThreadPool.QueueUserWorkItem(new WaitCallback(this.PostprocessBundleUseMultiThreads), (object) new BundleManager.States()
                {
                  bundlename = bundlename,
                  content = resultInfo2.ResData,
                  type = BundleManager.LoadType.Cache
                });
              }
              else
              {
                if (this.updatingBundle.ContainsKey(bundlename))
                  this.updatingBundle.Remove(bundlename);
                D.error((object) ("cache remote bundle failed " + filename));
                if (this.onBundleLoaded != null)
                  this.onBundleLoaded(bundlename, false);
                else
                  NetWorkDetector.Instance.RetryTip((System.Action) (() => this.CacheBundle(bundlename)), ScriptLocalization.Get("network_error_try_again_button", true), ScriptLocalization.Get("network_error_description", true));
              }
            });
            LoaderManager.inst.Add(batch, false, false);
            if (this.onBundleBeginToDownload == null)
              return;
            this.onBundleBeginToDownload(bundlename, batch);
          }
        }
        else
        {
          if (this.updatingBundle.ContainsKey(bundlename))
            this.updatingBundle.Remove(bundlename);
          D.error((object) ("cache local bundle failed " + filename));
          if (this.onBundleLoaded == null)
            return;
          this.onBundleLoaded(bundlename, false);
        }
      });
      LoaderManager.inst.Add(batch1, false, false);
      if (this.onBundleBeginToDownload == null)
        return;
      this.onBundleBeginToDownload(bundlename, batch1);
    }
    else
    {
      if (this.onBundleLoaded == null)
        return;
      this.onBundleLoaded(bundlename, true);
    }
  }

  private bool CheckIfUpdating(string filename, BundleManager.LoadType type)
  {
    string str = filename;
    if (filename.Contains('='.ToString()))
      str = filename.Split('=')[1];
    string key = str.Replace(".assetbundle", string.Empty);
    if (this.updatingBundle.ContainsKey(key))
    {
      if (type == BundleManager.LoadType.Load)
        this.updatingBundle[key] = BundleManager.LoadType.Load;
      return true;
    }
    this.updatingBundle.Add(key, type);
    return false;
  }

  [DebuggerHidden]
  private IEnumerator PostprocessBundleUseCoroutine(string bundlename, byte[] binary, BundleManager.LoadType type)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BundleManager.\u003CPostprocessBundleUseCoroutine\u003Ec__Iterator5F()
    {
      bundlename = bundlename,
      binary = binary,
      type = type,
      \u003C\u0024\u003Ebundlename = bundlename,
      \u003C\u0024\u003Ebinary = binary,
      \u003C\u0024\u003Etype = type,
      \u003C\u003Ef__this = this
    };
  }

  private void PostprocessBundleUseMultiThreads(object sts)
  {
    BundleManager.States states = (BundleManager.States) sts;
    this.PostprocessBundle(states.bundlename, states.content, states.type);
  }

  private void PostprocessBundle(string bundlename, byte[] binary, BundleManager.LoadType type)
  {
    byte[] numArray1 = binary;
    string str = bundlename + ".assetbundle";
    string version = VersionManager.Instance.GetVersion(str, VersionType.Bundle);
    string path = NetApi.inst.BuildLocalBundlePath(this.GetFullBundleName(bundlename));
    if (AssetConfig.IsUseCompress(bundlename))
    {
      MemoryStream memoryStream = new MemoryStream(numArray1);
      FileStream fileStream = new FileStream(path, FileMode.Create);
      Decoder decoder = new Decoder();
      byte[] numArray2 = new byte[5];
      memoryStream.Read(numArray2, 0, 5);
      byte[] buffer = new byte[8];
      memoryStream.Read(buffer, 0, 8);
      long int64 = BitConverter.ToInt64(buffer, 0);
      decoder.SetDecoderProperties(numArray2);
      try
      {
        decoder.Code((Stream) memoryStream, (Stream) fileStream, memoryStream.Length, int64, (ICodeProgress) null);
        fileStream.Flush();
      }
      catch (IOException ex)
      {
        this.needrestart = true;
      }
      fileStream.Close();
      memoryStream.Close();
    }
    else
      Utils.WriteFile(path, numArray1);
    CacheManager.Instance.Add2CacheMap(str, version);
    BundleManager.UnzipRecord unzipRecord = new BundleManager.UnzipRecord();
    unzipRecord.lt = type;
    unzipRecord.sts = false;
    if (this.upzipMark.TryAdd(bundlename, unzipRecord) || this.upzipMark.TryAdd(bundlename, unzipRecord))
      ;
  }

  public Dictionary<string, AssetBundle> Ablist
  {
    get
    {
      return this.ablist;
    }
  }

  public void LoadBasicBundle(string bundlename)
  {
    if (!this.CheckVersionList(bundlename) || this.ablist.ContainsKey(bundlename))
      return;
    AssetBundle assetBundle = this.LoadBundleFromLocal(bundlename);
    if ((UnityEngine.Object) null == (UnityEngine.Object) assetBundle)
      return;
    this.ablist.Add(assetBundle.name, assetBundle);
  }

  public AssetBundle LoadBundleFromLocal(string bundlename)
  {
    AssetBundle ab;
    this.GetBundleFromCache(bundlename, out ab);
    if ((UnityEngine.Object) null != (UnityEngine.Object) ab)
      return ab;
    string str = NetApi.inst.BuildLocalBundlePath(this.GetFullBundleName(bundlename));
    AssetBundle fromFile = AssetBundle.CreateFromFile(str);
    if ((UnityEngine.Object) null == (UnityEngine.Object) fromFile)
    {
      FileInfo fileInfo = new FileInfo(str);
      if (fileInfo != null && fileInfo.Exists)
        fileInfo.Delete();
      if (bundlename.Contains("static+"))
      {
        D.error((object) ("[Bundle] Create from File fail " + bundlename));
        string title = ScriptLocalization.Get("data_error_title", true);
        string tip = ScriptLocalization.Get("data_error_create_description", true) + bundlename;
        if (!NetWorkDetector.Instance.IsReadyRestart())
          NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.Bundle, "CreateFail", str);
        NetWorkDetector.Instance.RestartOrQuitGame(tip, title, (string) null);
      }
      return (AssetBundle) null;
    }
    fromFile.name = bundlename;
    return fromFile;
  }

  private void LoadBundleFinish(string bundlename, AssetBundle ab)
  {
    LoadingBundle loadingBundle = new LoadingBundle();
    loadingBundle.name = bundlename;
    loadingBundle.ab = ab;
    if (this.onBundleLoadFinished != null)
      this.onBundleLoadFinished(loadingBundle);
    if (this.onBundleLoaded != null)
      this.onBundleLoaded(bundlename, true);
    this.RecordBundle(ab);
  }

  public void RecordBundle(AssetBundle ab)
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) ab)
      return;
    if (AssetConfig.basicBundleList.Contains(ab.name))
    {
      if (this.ablist.ContainsKey(ab.name))
        return;
      this.ablist.Add(ab.name, ab);
    }
    else
      this.abCache.AddObject(ab.name, ab);
  }

  private bool CheckVersionList(string bundlename)
  {
    string str = bundlename + ".assetbundle";
    string version = VersionManager.Instance.GetVersion(str, VersionType.Bundle);
    return CacheManager.Instance.IsCached(str, version);
  }

  public bool IsBundleCached(string bundleName)
  {
    return this.CheckVersionList(bundleName);
  }

  public bool IsFileCached(string fullName)
  {
    string bundleNameFromPath = AssetManager.Instance.GetBundleNameFromPath(fullName);
    if (!string.IsNullOrEmpty(bundleNameFromPath))
      return this.IsBundleCached(bundleNameFromPath);
    return true;
  }

  public string GetFullBundleName(string path)
  {
    string name = path + ".assetbundle";
    return VersionManager.Instance.GetVersion(name, VersionType.Bundle) + (object) '=' + name;
  }

  private void Start()
  {
    BundleManager._instance = this;
    this.abCache = new FrequencyCache<AssetBundle>(50, (System.Action<AssetBundle>) (ab =>
    {
      if (!((UnityEngine.Object) null != (UnityEngine.Object) ab))
        throw new ArgumentException("something wrong happend!!!");
      ab.Unload(false);
    }));
    Oscillator.Instance.secondEvent += new System.Action<int>(this.SecondUpdate);
  }

  private void SecondUpdate(int obj)
  {
    if (this.needrestart && this.hasrestart)
    {
      this.hasrestart = false;
      NetWorkDetector.Instance.QuitGame(ScriptLocalization.Get("insufficient_space_title", true), ScriptLocalization.Get("insufficient_space_description", true));
    }
    if (this.upzipMark.Count == 0)
      return;
    using (IEnumerator<KeyValuePair<string, BundleManager.UnzipRecord>> enumerator = this.upzipMark.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, BundleManager.UnzipRecord> current = enumerator.Current;
        if (!current.Value.sts)
        {
          this.AfterUpzipFinished(current.Key, current.Value.lt);
          current.Value.sts = true;
        }
      }
    }
  }

  private void Update()
  {
  }

  public void ClearUnzipMarkCollection()
  {
    this.upzipMark.Clear();
  }

  private void AfterUpzipFinished(string bundlename, BundleManager.LoadType type)
  {
    if (this.updatingBundle.ContainsKey(bundlename))
    {
      type = this.updatingBundle[bundlename];
      this.updatingBundle.Remove(bundlename);
    }
    switch (type)
    {
      case BundleManager.LoadType.Load:
        AssetBundle ab = this.LoadBundleFromLocal(bundlename);
        if ((UnityEngine.Object) null == (UnityEngine.Object) ab)
          break;
        this.LoadBundleFinish(bundlename, ab);
        break;
      case BundleManager.LoadType.Cache:
        if (this.onBundleLoaded == null)
          break;
        this.onBundleLoaded(bundlename, true);
        break;
    }
  }

  public void Init()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.SecondUpdate);
  }

  public void Dispose()
  {
    using (Dictionary<string, AssetBundle>.Enumerator enumerator = this.ablist.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.Unload(true);
    }
    this.ablist.Clear();
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.SecondUpdate);
    BundleManager._instance = (BundleManager) null;
  }

  public void ClearTempData()
  {
    this.onBundleLoaded = (System.Action<string, bool>) null;
    this.onBundleBeginToDownload = (System.Action<string, LoaderBatch>) null;
    this.updatingBundle.Clear();
  }

  public enum LoadType
  {
    Load,
    Cache,
  }

  private class States
  {
    public string bundlename;
    public byte[] content;
    public BundleManager.LoadType type;
  }

  private class UnzipRecord
  {
    public BundleManager.LoadType lt;
    public bool sts;
  }
}
