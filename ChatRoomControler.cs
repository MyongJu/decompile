﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomControler
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;
using UnityEngine;

public class ChatRoomControler : MonoBehaviour
{
  private ChatRoomControler.Data data = new ChatRoomControler.Data();
  [SerializeField]
  private ChatRoomControler.Panel panel;

  public void Setup(long roomId)
  {
    this.Reset();
    this.data.roomId = roomId;
    this.SelectMemeber();
    this.panel.settingContainer.gameObject.SetActive(false);
  }

  public void SelectMemeber()
  {
    this.panel.TAG_MemberSelected.SetActive(true);
    this.panel.TAG_MemberUnSelected.SetActive(false);
    this.panel.TAG_ManageSelected.SetActive(false);
    this.panel.TAG_ManageUnSelected.SetActive(true);
    this.panel.memberList.gameObject.SetActive(true);
    this.panel.memberList.Setup(this.data.roomId);
    this.panel.manageList.gameObject.SetActive(false);
  }

  public void SelectManage()
  {
    this.panel.TAG_MemberSelected.SetActive(false);
    this.panel.TAG_MemberUnSelected.SetActive(true);
    this.panel.TAG_ManageSelected.SetActive(true);
    this.panel.TAG_ManageUnSelected.SetActive(false);
    this.panel.memberList.gameObject.SetActive(false);
    this.panel.manageList.gameObject.SetActive(true);
    this.panel.manageList.Setup(this.data.roomId);
  }

  public void OnMemberTagClick()
  {
    this.SelectMemeber();
  }

  public void OnManagerTagClick()
  {
    this.SelectManage();
  }

  public void OnCloseClick()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
  }

  public void Reset()
  {
  }

  public class Data
  {
    public long roomId;
  }

  [Serializable]
  public class Panel
  {
    public GameObject TAG_MemberSelected;
    public GameObject TAG_MemberUnSelected;
    public GameObject TAG_ManageSelected;
    public GameObject TAG_ManageUnSelected;
    public ChatRoomMemberList memberList;
    public ChatRoomManageList manageList;
    public Transform settingContainer;
  }
}
