﻿// Decompiled with JetBrains decompiler
// Type: LeaderBoardPlayerInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;

public class LeaderBoardPlayerInfo : Popup
{
  private const string CROWN_PATH = null;
  private const string PLAYER_ICON_PATH = null;
  private const string ALLIANCE_ICON_PATH = null;
  public UILabel title;
  public UITexture crown;
  public UITexture playerIcon;
  public UITexture allianceIcon;
  public UILabel allianceName;
  public UILabel playerName;
  public UILabel playerPower;
  public UIButton blockBtn;
  public UIButton lookupBtn;
  public UIButton emailBtn;
  public UILabel blockLabel;
  public AllianceSymbol symbol;
  private long id;
  private bool _showKingdom;
  private RankData _data;

  private void Clear()
  {
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    this.Clear();
    base.OnOpen(orgParam);
    LeaderBoardPlayerInfo.LeaderBoardPlayerInfoParameter playerInfoParameter = orgParam as LeaderBoardPlayerInfo.LeaderBoardPlayerInfoParameter;
    this.id = playerInfoParameter.playerID;
    this._data = playerInfoParameter.data;
    this._showKingdom = playerInfoParameter.showKingdom;
    this.allianceName.text = this._data.AllianceName == null ? string.Empty : this._data.AllianceName;
    this.playerName.text = this._showKingdom ? this._data.FullName : this._data.Name;
    long num1 = this._data.Score;
    long num2 = long.Parse(this._data.UID);
    string remoteIcon = string.Empty;
    int num3 = 0;
    if (this._data is PlayerRankData)
    {
      PlayerRankData data = this._data as PlayerRankData;
      remoteIcon = data.CustomIconUrl;
      num1 = data.Power;
      num3 = data.LordTitleId;
    }
    this.playerPower.text = num1.ToString();
    CustomIconLoader.Instance.requestCustomIcon(this.playerIcon, this._data.Image, remoteIcon, false);
    this.blockLabel.text = DBManager.inst.DB_Contacts.blocking.Contains(num2) ? ScriptLocalization.Get("lord_details_uppercase_unblock_button", true) : ScriptLocalization.Get("lord_details_uppercase_block_button", true);
    NGUITools.SetActive(this.blockBtn.gameObject, false);
    NGUITools.SetActive(this.symbol.gameObject, false);
    if (string.IsNullOrEmpty(this._data.Tag))
      return;
    NGUITools.SetActive(this.symbol.gameObject, true);
    this.symbol.SetSymbols(this._data.Symbol);
  }

  public void OnBlockBtnClicked()
  {
    long uid = long.Parse(this._data.UID);
    if (!DBManager.inst.DB_Contacts.blocking.Contains(uid))
      ContactManager.BlockUser(uid, new System.Action<bool, object>(this.BlockCallBack));
    else
      ContactManager.UnblockUser(uid, new System.Action<bool, object>(this.UnBlockCallBack));
  }

  private void BlockCallBack(bool success, object result)
  {
    if (!success)
      return;
    this.blockLabel.text = ScriptLocalization.Get("lord_details_uppercase_unblock_button", true);
  }

  private void UnBlockCallBack(bool success, object result)
  {
    if (!success)
      return;
    this.blockLabel.text = ScriptLocalization.Get("lord_details_uppercase_block_button", true);
  }

  public void OnLookUpBtnClicked()
  {
    long num = long.Parse(this._data.UID);
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
    {
      uid = num
    }, 1 != 0, 1 != 0, 1 != 0);
    this.OnCloseBtnClicked();
  }

  public void OnEmailBtnClicked()
  {
    UIManager.inst.OpenDlg("Mail/MailComposeDlg", (UI.Dialog.DialogParameter) new MailComposeDlg.Parameter()
    {
      recipents = new List<string>()
      {
        this._data.OrginName
      }
    }, 1 != 0, 1 != 0, 1 != 0);
    this.OnCloseBtnClicked();
  }

  public void OnCloseBtnClicked()
  {
    this.Clear();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class LeaderBoardPlayerInfoParameter : Popup.PopupParameter
  {
    public long playerID;
    public RankData data;
    public bool showKingdom;
  }
}
