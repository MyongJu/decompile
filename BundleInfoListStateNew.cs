﻿// Decompiled with JetBrains decompiler
// Type: BundleInfoListStateNew
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BundleInfoListStateNew : LoadBaseState
{
  public BundleInfoListStateNew(int step)
    : base(step)
  {
  }

  public override string Key
  {
    get
    {
      return "BundleInfoListState";
    }
  }

  protected override SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.BunldeInfoVersion;
    }
  }

  protected override void Prepare()
  {
    base.Prepare();
    if (!AssetManager.IsLoadAssetFromBundle)
    {
      this.Finish();
    }
    else
    {
      this.Load();
      Oscillator.Instance.updateEvent += new System.Action<double>(this.UpdateEventHandler);
    }
  }

  private void UpdateEventHandler(double t)
  {
    GameEngine.Instance.iTweenValue = Mathf.Lerp(GameEngine.Instance.iTweenValue, SplashDataConfig.GetValue(this.CurrentPhase), 0.02f);
    this.RefreshProgress(GameEngine.Instance.iTweenValue);
  }

  private void Load()
  {
    VersionManager.Instance.onVersionLoadFinished += new System.Action<bool>(this.OnVersionLoadFinished);
    VersionManager.Instance.Init();
  }

  private void OnVersionLoadFinished(bool success)
  {
    VersionManager.Instance.onVersionLoadFinished -= new System.Action<bool>(this.OnVersionLoadFinished);
    if (!success || this.IsFail)
      return;
    this.Finish();
  }

  protected override void Retry()
  {
    this.ResetBatch();
    this.Load();
  }

  protected override void OnDispose()
  {
    VersionManager.Instance.onVersionLoadFinished -= new System.Action<bool>(this.OnVersionLoadFinished);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.updateEvent -= new System.Action<double>(this.UpdateEventHandler);
  }

  protected override void Finish()
  {
    if (this.IsDestroy)
      return;
    base.Finish();
    if (Oscillator.IsAvailable)
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.UpdateEventHandler);
    this.Preloader.OnBundleVerListFinish();
  }
}
