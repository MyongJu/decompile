﻿// Decompiled with JetBrains decompiler
// Type: RoundResult
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class RoundResult
{
  private long _dataId = -1;
  private int _duration = -1;
  private RoundResult.ResultType _type;
  private RoundPlayer _target;
  private int _value;
  private List<BattleBuff> _buffs;
  private bool _isMyself;

  public bool IsMyself
  {
    get
    {
      return this._isMyself;
    }
    set
    {
      this._isMyself = value;
    }
  }

  public void AddBuff(BattleBuff buff)
  {
    this.Buffs.Add(buff);
  }

  public List<BattleBuff> Buffs
  {
    get
    {
      if (this._buffs == null)
        this._buffs = new List<BattleBuff>();
      return this._buffs;
    }
  }

  public List<int> GetBuffIds()
  {
    List<int> intList = new List<int>();
    if (this._buffs != null)
    {
      for (int index = 0; index < this._buffs.Count; ++index)
        intList.Add(this._buffs[index].BuffId);
    }
    return intList;
  }

  public int Duration
  {
    get
    {
      return this._duration;
    }
    set
    {
      this._duration = value;
    }
  }

  public int Value
  {
    get
    {
      return this._value;
    }
    set
    {
      this._value = value;
    }
  }

  public RoundPlayer Target
  {
    get
    {
      return this._target;
    }
    set
    {
      this._target = value;
    }
  }

  public long DataId
  {
    get
    {
      return this._dataId;
    }
    set
    {
      this._dataId = value;
    }
  }

  public RoundResult.ResultType Type
  {
    get
    {
      return this._type;
    }
    set
    {
      this._type = value;
    }
  }

  public bool Decode(object result)
  {
    return false;
  }

  public override string ToString()
  {
    return string.Format("[RoundResult]");
  }

  public enum ResultType
  {
    None,
    Miss,
    NormalDamage,
    SkillDamage,
    Rebound,
    Heal,
  }
}
