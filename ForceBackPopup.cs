﻿// Decompiled with JetBrains decompiler
// Type: ForceBackPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;

public class ForceBackPopup : Popup
{
  public static bool IsRunning;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    ForceBackPopup.IsRunning = true;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    ForceBackPopup.IsRunning = false;
  }

  public void OnOkayPressed()
  {
    MessageHub.inst.GetPortByAction("City:replace").SendRequest((Hashtable) null, (System.Action<bool, object>) ((_param1, _param2) =>
    {
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
      GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Lite);
    }), true);
  }
}
