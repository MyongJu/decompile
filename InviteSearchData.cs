﻿// Decompiled with JetBrains decompiler
// Type: InviteSearchData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class InviteSearchData
{
  public long uid;
  public string name;
  public string language;
  public int power;
  public int portrait;

  public void Decode(object data)
  {
    Hashtable inData = data as Hashtable;
    DatabaseTools.UpdateData(inData, "uid", ref this.uid);
    DatabaseTools.UpdateData(inData, "name", ref this.name);
    DatabaseTools.UpdateData(inData, "power", ref this.power);
    DatabaseTools.UpdateData(inData, "portrait", ref this.portrait);
    DatabaseTools.UpdateData(inData, "language", ref this.language);
  }
}
