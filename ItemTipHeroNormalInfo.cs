﻿// Decompiled with JetBrains decompiler
// Type: ItemTipHeroNormalInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ItemTipHeroNormalInfo : MonoBehaviour
{
  [SerializeField]
  private UITexture icon;
  [SerializeField]
  private UITexture background;
  [SerializeField]
  private GameObject _redPointNode;
  [SerializeField]
  private UIButton _summonButton;
  [SerializeField]
  private UILabel _heroNameText;
  [SerializeField]
  private UIProgressBar _progressBar;
  [SerializeField]
  private UILabel _progressText;
  [SerializeField]
  private UILabel _heroLevelText;
  [SerializeField]
  private UILabel _heroTitleText;
  [SerializeField]
  private GameObject _heroTitleNode;
  [SerializeField]
  private UISprite _heroFrameSprite;
  [SerializeField]
  private UISprite _heroCanSummonSprite;
  [SerializeField]
  private ParliamentHeroStar _parliamentHeroStar;
  private ParliamentHeroInfo _parliamentHeroInfo;
  private int itemId;

  protected bool RedPointShow
  {
    get
    {
      return this.GetCurrentFragments() >= this.GetSummonFragmentsCount();
    }
  }

  public bool HeroSummonable
  {
    get
    {
      return this.GetCurrentFragments() >= this.GetSummonFragmentsCount();
    }
  }

  public void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
    BuilderFactory.Instance.Release((UIWidget) this.background);
  }

  public void SetData(ParliamentHeroInfo parliamentHeroInfo, int itemId)
  {
    this._parliamentHeroInfo = parliamentHeroInfo;
    this.itemId = itemId;
    this.UpdateUI();
  }

  public void OnSummonButtonClicked()
  {
    if (this._parliamentHeroInfo == null)
      ;
  }

  public void OnHeroCardClicked()
  {
    if (this._parliamentHeroInfo == null)
      return;
    UIManager.inst.OpenPopup("ParliamentHero/HeroFragmentDetailPopup", (Popup.PopupParameter) new HeroFragmentDetailPopup.Parameter()
    {
      UserId = PlayerData.inst.uid,
      HeroTemplateId = this._parliamentHeroInfo.internalId
    });
  }

  private void UpdateUI()
  {
    this.ShowHeroDetail();
    this._heroNameText.text = this.GetName();
    if (this._parliamentHeroInfo != null)
      this._heroNameText.color = this._parliamentHeroInfo.QualityColor;
    int currentFragments = this.GetCurrentFragments();
    int summonFragmentsCount = this.GetSummonFragmentsCount();
    NGUITools.SetActive(this._redPointNode, false);
    NGUITools.SetActive(this._summonButton.gameObject, false);
    this.CheckStarsStatus();
    this.CheckSummonStatus(currentFragments >= summonFragmentsCount);
  }

  private void CheckSummonStatus(bool showHighlight)
  {
    NGUITools.SetActive(this._heroCanSummonSprite.gameObject, false);
  }

  private void ShowHeroDetail()
  {
    Utils.SetItemBackground(this.background, this.itemId);
    BuilderFactory.Instance.Build((UIWidget) this.icon, ConfigManager.inst.DB_Items.GetItem(this.itemId).ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    this._heroLevelText.text = this.GetHeroLevelDescription();
    this._heroTitleText.text = this.GetHeroTitle();
    NGUITools.SetActive(this._progressBar.gameObject, false);
    NGUITools.SetActive(this._heroTitleNode, true);
  }

  private void CheckStarsStatus()
  {
    if (this._parliamentHeroInfo == null)
      return;
    this._parliamentHeroStar.SetData(this._parliamentHeroInfo, (LegendCardData) null, false);
    this._heroFrameSprite.color = this._parliamentHeroInfo.QualityColor;
  }

  private void ShowHeroSummonEffect()
  {
    if (this._parliamentHeroInfo == null)
      return;
    HeroRecruitLuckyDrawDialog.Parameter parameter = new HeroRecruitLuckyDrawDialog.Parameter();
    HeroRecruitPayloadData recruitPayloadData = new HeroRecruitPayloadData();
    recruitPayloadData.Decode((object) Utils.Hash((object) "type", (object) "hero", (object) "hero_id", (object) this._parliamentHeroInfo.internalId, (object) "num", (object) 1));
    parameter.heroPayload = recruitPayloadData;
    parameter.showButton = false;
    parameter.heroRecruitData = (HeroRecruitData) new HeroRecruitDataGold();
    UIManager.inst.OpenDlg("HeroCard/HeroRecruitLuckyDrawDialog", (UI.Dialog.DialogParameter) parameter, true, true, true);
  }

  private int GetMaxStars()
  {
    return HeroCardUtils.GetHeroMaxStars(this._parliamentHeroInfo.internalId);
  }

  private int GetCurrentStars()
  {
    if (this._parliamentHeroInfo != null)
    {
      ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(this._parliamentHeroInfo.quality.ToString());
      if (parliamentHeroQualityInfo != null)
        return parliamentHeroQualityInfo.initialStar;
    }
    return 1;
  }

  private int GetCurrentFragments()
  {
    if (this._parliamentHeroInfo != null)
      return this._parliamentHeroInfo.ChipCount;
    return 0;
  }

  private int GetSummonFragmentsCount()
  {
    if (this._parliamentHeroInfo != null)
    {
      ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(this._parliamentHeroInfo.quality.ToString());
      if (parliamentHeroQualityInfo != null)
        return parliamentHeroQualityInfo.summonChipsReq;
    }
    return 0;
  }

  private int GetNextStarFragments()
  {
    ParliamentHeroStarInfo parliamentHeroStarInfo = ConfigManager.inst.DB_ParliamentHeroStar.Get(this.GetCurrentStars().ToString());
    if (parliamentHeroStarInfo != null)
      return parliamentHeroStarInfo.nextStarChipReq;
    return 0;
  }

  private string GetName()
  {
    if (this._parliamentHeroInfo != null)
      return this._parliamentHeroInfo.Name;
    return string.Empty;
  }

  private int GetHeroLevel()
  {
    return 1;
  }

  private string GetHeroLevelDescription()
  {
    return ScriptLocalization.GetWithPara("id_lv_num", new Dictionary<string, string>()
    {
      {
        "0",
        this.GetHeroLevel().ToString()
      }
    }, true);
  }

  private string GetHeroTitle()
  {
    return string.Empty;
  }
}
