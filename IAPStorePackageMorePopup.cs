﻿// Decompiled with JetBrains decompiler
// Type: IAPStorePackageMorePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class IAPStorePackageMorePopup : Popup
{
  private List<IAPStoreContentRenderer> m_ItemList = new List<IAPStoreContentRenderer>();
  public UILabel m_PackageName;
  public UILabel m_Price1;
  public UILabel m_Price2;
  public UILabel m_Duration;
  public GameObject m_ItemPrefab;
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  private IAPStorePackageMorePopup.Parameter m_Parameter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as IAPStorePackageMorePopup.Parameter;
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.ClearData();
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnBuy()
  {
    if (this.m_Parameter.OnPurchaseBegin != null)
      this.m_Parameter.OnPurchaseBegin();
    IAPStorePackagePayload.Instance.BuyProduct(this.m_Parameter.package.package.productId, this.m_Parameter.package.id, this.m_Parameter.package.group_id, (System.Action) (() =>
    {
      if (this.m_Parameter.OnPurchaseEnd == null)
        return;
      this.m_Parameter.OnPurchaseEnd(this.m_Parameter.package.group.internalId);
    }));
    this.OnClosePressed();
  }

  private void UpdateUI()
  {
    IAPStorePackage package = this.m_Parameter.package;
    this.m_PackageName.text = ScriptLocalization.Get(package.group.name, true);
    double num = PaymentManager.Instance.GetRealPrice(package.package.productId, package.package.pay_id) * package.package.discount / 100.0;
    this.m_Price1.text = PaymentManager.Instance.GetCurrencyCode(package.package.productId, package.package.pay_id) + Utils.FormatThousands(num.ToString("f2"));
    this.m_Price2.text = PaymentManager.Instance.GetFormattedPrice(package.package.productId, package.package.pay_id);
    this.UpdateData();
  }

  private void ClearData()
  {
    using (List<IAPStoreContentRenderer>.Enumerator enumerator = this.m_ItemList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IAPStoreContentRenderer current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ItemList.Clear();
  }

  private void UpdateData()
  {
    this.ClearData();
    IAPPackageInfo package = this.m_Parameter.package.package;
    IAPRewardInfo rewardInfo1 = ConfigManager.inst.DB_IAPRewards.Get(package.reward_group_id);
    IAPRewardInfo rewardInfo2 = ConfigManager.inst.DB_IAPRewards.Get(package.alliance_reward_group);
    this.UpdateData(rewardInfo1);
    this.UpdateData(rewardInfo2);
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  private void UpdateData(IAPRewardInfo rewardInfo)
  {
    if (rewardInfo == null || rewardInfo.Rewards == null)
      return;
    List<Reward.RewardsValuePair> rewards = rewardInfo.GetRewards();
    rewards.Sort(new Comparison<Reward.RewardsValuePair>(IAPStorePackageMorePopup.Compare));
    for (int index = 0; index < rewards.Count; ++index)
    {
      Reward.RewardsValuePair rewardsValuePair = rewards[index];
      int internalId = rewardsValuePair.internalID;
      int itemCount = rewardsValuePair.value;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_Grid.transform;
      gameObject.transform.localScale = Vector3.one;
      IAPStoreContentRenderer component = gameObject.GetComponent<IAPStoreContentRenderer>();
      component.SetData(itemStaticInfo, itemCount);
      this.m_ItemList.Add(component);
    }
  }

  private void Update()
  {
    if (this.m_Parameter == null || this.m_Parameter.package == null)
      return;
    int time = (int) this.m_Parameter.package.endTime - NetServerTime.inst.ServerTimestamp;
    if (time > 0)
      this.m_Duration.text = Utils.FormatTime(time, true, false, true);
    else
      this.m_Duration.text = Utils.FormatTime(0, true, false, true);
  }

  private static int Compare(Reward.RewardsValuePair x, Reward.RewardsValuePair y)
  {
    return ConfigManager.inst.DB_Items.GetItem(x.internalID).Priority - ConfigManager.inst.DB_Items.GetItem(y.internalID).Priority;
  }

  public class Parameter : Popup.PopupParameter
  {
    public IAPStorePackage package;
    public System.Action OnPurchaseBegin;
    public System.Action<int> OnPurchaseEnd;
  }
}
