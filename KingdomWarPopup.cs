﻿// Decompiled with JetBrains decompiler
// Type: KingdomWarPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class KingdomWarPopup : Popup
{
  private List<KingdomItemRenderer> _items = new List<KingdomItemRenderer>();
  public UILabel title;
  public UIGrid grid;
  public UIScrollView scroll;
  public UILabel remainTime;
  public KingdomItemRenderer itemPrefab;
  public UILabel timeLabel;
  public UITexture bg;
  private KingdomWarActivityData _data;

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._data = (orgParam as KingdomWarPopup.Parameter).data;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.bg, "Texture/GUI_Textures/avalon_state_top_banner", (System.Action<bool>) null, true, false, string.Empty);
    this.title.text = ScriptLocalization.Get("throne_invasion_title", true);
    this.UpdateUI();
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
    this.RefreshTime();
  }

  private void UpdateUI()
  {
    string Term = "throne_invasion_kingdoms_change_time";
    if (!this._data.IsInWar)
      Term = "throne_invasion_starts_in_time";
    this.timeLabel.text = ScriptLocalization.Get(Term, true);
    List<KingdomWarActivityData.KingdomInfo> datas = this._data.Datas;
    for (int index = 0; index < datas.Count; ++index)
    {
      KingdomWarActivityData.KingdomInfo info = datas[index];
      KingdomItemRenderer kingdomItemRenderer = this.GetItem();
      kingdomItemRenderer.SetData(info);
      this._items.Add(kingdomItemRenderer);
    }
    this.grid.Reposition();
    this.scroll.ResetPosition();
  }

  private KingdomItemRenderer GetItem()
  {
    GameObject go = NGUITools.AddChild(this.grid.gameObject, this.itemPrefab.gameObject);
    KingdomItemRenderer component = go.GetComponent<KingdomItemRenderer>();
    NGUITools.SetActive(go, true);
    return component;
  }

  private void RefreshTime()
  {
    int remainTime = this._data.RemainTime;
    this.remainTime.text = Utils.FormatTime(remainTime, true, false, true);
    if (remainTime > 0 || !Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
  }

  private void Process(int time)
  {
    this.RefreshTime();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
    for (int index = 0; index < this._items.Count; ++index)
      this._items[index].Clear();
    BuilderFactory.Instance.Release((UIWidget) this.bg);
    this._items.Clear();
  }

  public class Parameter : Popup.PopupParameter
  {
    public KingdomWarActivityData data;
  }
}
