﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarScoreRewardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceWarScoreRewardPopup : Popup
{
  private readonly List<AllianceWarScoreRewardItem> _allPitRewardItem = new List<AllianceWarScoreRewardItem>();
  [SerializeField]
  private UILabel _labelCurrentScore;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UIGrid _tableContainer;
  [SerializeField]
  private AllianceWarScoreRewardItem _rewardItemTemplate;

  private AllianceWarScoreRewardItem CreateRewardItem()
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this._rewardItemTemplate.gameObject);
    gameObject.transform.SetParent(this._tableContainer.transform, false);
    gameObject.SetActive(true);
    AllianceWarScoreRewardItem component = gameObject.GetComponent<AllianceWarScoreRewardItem>();
    this._allPitRewardItem.Add(component);
    return component;
  }

  private void DestroyAllRewardItem()
  {
    using (List<AllianceWarScoreRewardItem>.Enumerator enumerator = this._allPitRewardItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceWarScoreRewardItem current = enumerator.Current;
        if ((bool) ((Object) current))
          Object.Destroy((Object) current.gameObject);
      }
    }
    this._allPitRewardItem.Clear();
  }

  public override void OnShow(UIControler.UIParameter parameter)
  {
    base.OnShow(parameter);
    this._rewardItemTemplate.gameObject.SetActive(false);
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this._labelCurrentScore.text = Utils.XLAT("event_royal_battle_personal_score", (object) "0", (object) AllianceWarPayload.Instance.UserScore);
    this.DestroyAllRewardItem();
    using (List<AllianceWarScoreRewardInfo>.Enumerator enumerator = ConfigManager.inst.DB_AllianceWarScoreReward.RewardInfoList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.CreateRewardItem().SetData(enumerator.Current);
    }
    this._tableContainer.Reposition();
    this._scrollView.ResetPosition();
  }

  public void OnButtonCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }
}
