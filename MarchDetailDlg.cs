﻿// Decompiled with JetBrains decompiler
// Type: MarchDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MarchDetailDlg : UI.Dialog
{
  private GameObjectPool m_LegendPool = new GameObjectPool();
  private GameObjectPool m_TroopPool = new GameObjectPool();
  private int legendcount = 3;
  private List<LegendItem> lstMarchLegend = new List<LegendItem>();
  private List<MarchTroopItemUI> lstMarchTroop = new List<MarchTroopItemUI>();
  public UIGrid legendGrid;
  public UIGrid troopGrid;
  public UIScrollView scrollView;
  public GameObject legendItemTemplate;
  public GameObject troopItemTemplate;
  public UILabel troopTotalAmount;
  public UILabel legendTotalAmount;
  private bool m_Initialized;
  private int trooptotalamount;
  private int trooptype;

  private void Clear()
  {
    for (int index = 0; index < this.lstMarchLegend.Count; ++index)
    {
      this.lstMarchLegend[index].Clear();
      this.m_LegendPool.Release(this.lstMarchLegend[index].gameObject);
    }
    for (int index = 0; index < this.lstMarchTroop.Count; ++index)
    {
      this.lstMarchTroop[index].Clear();
      this.m_TroopPool.Release(this.lstMarchTroop[index].gameObject);
    }
    this.lstMarchLegend.Clear();
    this.lstMarchTroop.Clear();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    MarchDetailDlg.Parameter parameter = orgParam as MarchDetailDlg.Parameter;
    if (!this.m_Initialized)
    {
      this.m_LegendPool.Initialize(this.legendItemTemplate, this.legendGrid.gameObject);
      this.m_TroopPool.Initialize(this.troopItemTemplate, this.troopGrid.gameObject);
      this.m_Initialized = true;
    }
    this.Clear();
    List<LegendData> legendDataList = new List<LegendData>();
    legendDataList.Clear();
    this.legendcount = 0;
    MarchData marchData = DBManager.inst.DB_March.Get(parameter.marchId);
    this.trooptotalamount = marchData.troopsInfo.totalCount;
    if (marchData.legends != null)
      this.legendcount = marchData.legends.Count;
    this.trooptype = marchData.troopsInfo.troops.Count;
    this.troopTotalAmount.text = this.trooptotalamount.ToString();
    this.legendTotalAmount.text = this.legendcount.ToString();
    if (this.legendcount > 0)
    {
      this.legendGrid.maxPerLine = this.legendcount;
      this.legendGrid.cellWidth = (float) (this.legendGrid.gameObject.GetComponentInParent<UIWidget>().width / this.legendcount);
    }
    for (int index = 0; index < this.legendcount; ++index)
    {
      GameObject gameObject = this.m_LegendPool.AddChild(this.legendGrid.gameObject);
      gameObject.SetActive(true);
      LegendItem component = gameObject.GetComponent<LegendItem>();
      component.Clear();
      component.HideInfo();
      component.HideContent();
      this.lstMarchLegend.Add(component);
    }
    legendDataList.Sort((IComparer<LegendData>) new MarchDetailDlg.LengendSortRule());
    int num1 = 0;
    if (marchData.legends != null)
    {
      foreach (object legend in marchData.legends)
      {
        long legendid = (long) int.Parse(legend as string);
        LegendItem legendItem = this.lstMarchLegend[num1++];
        legendItem.transform.localScale = new Vector3(0.75f, 0.75f, 1f);
        legendItem.SetData(legendid);
      }
    }
    this.legendGrid.Reposition();
    this.scrollView.ResetPosition();
    for (int index = 0; index < this.trooptype; ++index)
    {
      GameObject gameObject = this.m_TroopPool.AddChild(this.troopGrid.gameObject);
      gameObject.SetActive(true);
      this.lstMarchTroop.Add(gameObject.GetComponent<MarchTroopItemUI>());
    }
    int num2 = 0;
    if (marchData != null && marchData.troopsInfo.troops != null)
    {
      Dictionary<string, Unit>.KeyCollection.Enumerator enumerator1 = marchData.troopsInfo.troops.Keys.GetEnumerator();
      while (enumerator1.MoveNext())
      {
        ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator1.Current);
        MarchTroopItemUI marchTroopItemUi = this.lstMarchTroop[num2++];
        IEnumerator enumerator2 = marchTroopItemUi.transform.GetEnumerator();
        try
        {
          while (enumerator2.MoveNext())
            ((Component) enumerator2.Current).gameObject.SetActive(true);
        }
        finally
        {
          IDisposable disposable = enumerator2 as IDisposable;
          if (disposable != null)
            disposable.Dispose();
        }
        int count = marchData.troopsInfo.troops[enumerator1.Current].Count;
        Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator1.Current);
        marchTroopItemUi.SetTroopInfo(data, count);
      }
    }
    this.troopGrid.Reposition();
    this.scrollView.ResetPosition();
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    this.Clear();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public long marchId;
  }

  public class LengendSortRule : IComparer<LegendData>
  {
    public int Compare(LegendData x, LegendData y)
    {
      return x.Level > y.Level || x.Level >= y.Level && (x.Power > y.Power || x.Power >= y.Power && x.LegendID > y.LegendID) ? -1 : 1;
    }
  }
}
