﻿// Decompiled with JetBrains decompiler
// Type: ConfigLookup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigLookup
{
  public const string SheetName = "lookup";
  public Dictionary<int, LookupInfo> datas;
  private Dictionary<string, LookupInfo> dicByUniqueId;

  public ConfigLookup()
  {
    this.datas = new Dictionary<int, LookupInfo>();
    this.dicByUniqueId = new Dictionary<string, LookupInfo>();
  }

  public void BuildDB(object res)
  {
    if (res == null)
    {
      Debug.LogError((object) string.Format("Can't find json object {0}", (object) "lookup.json"));
    }
    else
    {
      Hashtable hashtable = res as Hashtable;
      if (hashtable == null)
        Debug.LogError((object) string.Format("JSON object {0} need to be a HashTable", (object) "lookup.json"));
      else if (!hashtable.ContainsKey((object) "headers"))
      {
        Debug.LogError((object) string.Format("JSON object {0} need to have headers", (object) "lookup.json"));
      }
      else
      {
        ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "type");
        int index3 = ConfigManager.GetIndex(inHeader, "loc_id");
        int index4 = ConfigManager.GetIndex(inHeader, "image");
        hashtable.Remove((object) "headers");
        foreach (DictionaryEntry dictionaryEntry in hashtable)
        {
          ArrayList arr = dictionaryEntry.Value as ArrayList;
          if (arr != null)
          {
            LookupInfo data = new LookupInfo(Convert.ToInt32(dictionaryEntry.Key), arr[index1] as string, LookupInfo.TryParseLookupType(ConfigManager.GetString(arr, index2)), arr[index3] as string, arr[index4] as string);
            this.PushData(data.InternalID, data.ID, data);
          }
        }
      }
    }
  }

  private void PushData(int internalID, string ID, LookupInfo data)
  {
    if (this.datas.ContainsKey(internalID))
      return;
    this.datas.Add(internalID, data);
    if (!this.dicByUniqueId.ContainsKey(ID))
      this.dicByUniqueId.Add(ID, data);
    ConfigManager.inst.AddData(internalID, (object) data);
  }

  public void Clear()
  {
    if (this.datas != null)
      this.datas.Clear();
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
  }

  public LookupInfo this[int internalID]
  {
    get
    {
      return this.GetData(internalID);
    }
  }

  public LookupInfo this[string ID]
  {
    get
    {
      return this.GetData(ID);
    }
  }

  public bool Contains(string ID)
  {
    return this.dicByUniqueId.ContainsKey(ID);
  }

  public bool Contains(int internalID)
  {
    return this.datas.ContainsKey(internalID);
  }

  public LookupInfo GetData(int internalId)
  {
    if (this.datas.ContainsKey(internalId))
      return this.datas[internalId];
    return (LookupInfo) null;
  }

  public LookupInfo GetData(string ID)
  {
    if (this.dicByUniqueId.ContainsKey(ID))
      return this.dicByUniqueId[ID];
    return (LookupInfo) null;
  }

  public string GetImage(string uniqueId)
  {
    LookupInfo data = this.GetData(uniqueId);
    if (data != null)
      return data.Image;
    return "icon_missing";
  }
}
