﻿// Decompiled with JetBrains decompiler
// Type: SuggestionRecipientEntry
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SuggestionRecipientEntry : MonoBehaviour
{
  public UILabel recipientLabel;
  public UIButton btn;

  public event SuggestionRecipientEntry.SuggestionRecipientClickHandler OnSuggestionRecipientClicked;

  private void Awake()
  {
    this.btn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnClick)));
  }

  private void OnDestroy()
  {
    this.btn.onClick.Clear();
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
  }

  private void OnClick()
  {
    if (this.OnSuggestionRecipientClicked == null)
      return;
    this.OnSuggestionRecipientClicked(this);
  }

  public delegate void SuggestionRecipientClickHandler(SuggestionRecipientEntry sender);
}
