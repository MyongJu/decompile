﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_Rally_Slot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UI;
using UnityEngine;

public class AllianceWar_MainDlg_Rally_Slot : AllianceWar_MainDlg_Slot
{
  protected AllianceWar_MainDlg_Rally_Slot.Data data = new AllianceWar_MainDlg_Rally_Slot.Data();
  [SerializeField]
  protected AllianceWar_MainDlg_Rally_Slot.Panel panel;

  public override void Setup(long rallyId)
  {
    if (this.IsDestroy)
      return;
    RallyData rallyData = DBManager.inst.DB_Rally.Get(rallyId);
    AllianceWarManager.WarData data = PlayerData.inst.allianceWarManager.GetData(rallyId);
    if (rallyData == null || data == null)
      return;
    this.data.rallyId = rallyId;
    this.SetupAboveList(rallyData, rallyData.ownerUid, rallyData.ownerAllianceId, data.rallyCount);
    this.SetupBelowList(rallyData, rallyData.targetUid, rallyData.targetAllianceId, data.defenseCount);
    this.SetupTargetSlot(rallyData);
    this.RefreshTime();
  }

  protected virtual void SetupAboveList(RallyData rallyData, long uid, long allianceId, int memberCount)
  {
    UserData userData = DBManager.inst.DB_User.Get(uid);
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(allianceId);
    AllianceWar_MainDlg_RallyList.param.ownerPortrait = 0;
    if (userData != null)
    {
      AllianceWar_MainDlg_RallyList.param.ownerName = allianceData == null ? userData.userName : string.Format("[{0}]{1}", (object) allianceData.allianceAcronym, (object) userData.userName);
      AllianceWar_MainDlg_RallyList.param.ownerPortrait = userData.portrait;
      AllianceWar_MainDlg_RallyList.param.ownerCustomIconUrl = userData.Icon;
      AllianceWar_MainDlg_RallyList.param.ownerLordTitleId = userData.LordTitle;
    }
    AllianceWar_MainDlg_RallyList.param.memberCount = memberCount;
    AllianceWar_MainDlg_RallyList.param.canJoin = true;
    AllianceWar_MainDlg_RallyList.param.isAttack = true;
    AllianceWar_MainDlg_RallyList.param.rallyType = AllianceWar_MainDlg_RallyList.Params.RallyListType.MEMBER;
    this.panel.aboveList.Setup(AllianceWar_MainDlg_RallyList.param);
  }

  protected virtual void SetupBelowList(RallyData rallyData, long uid, long allianceId, int memberCount)
  {
    UserData userData = DBManager.inst.DB_User.Get(uid);
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(allianceId);
    AllianceWar_MainDlg_RallyList.param.ownerPortrait = 0;
    if (userData != null)
    {
      AllianceWar_MainDlg_RallyList.param.ownerName = allianceData == null ? (WonderUtils.IsWonderTower(rallyData.location) || WonderUtils.IsWonder(rallyData.location) ? string.Format("[{0}]{1}", (object) PlayerData.inst.allianceData.allianceAcronym, (object) userData.userName) : userData.userName) : string.Format("[{0}]{1}", (object) allianceData.allianceAcronym, (object) userData.userName);
      AllianceWar_MainDlg_RallyList.param.ownerPortrait = userData.portrait;
      AllianceWar_MainDlg_RallyList.param.ownerCustomIconUrl = userData.Icon;
      AllianceWar_MainDlg_RallyList.param.ownerLordTitleId = userData.LordTitle;
    }
    if (uid <= 0L)
    {
      if (WonderUtils.IsWonderTower(rallyData.location))
        AllianceWar_MainDlg_RallyList.param.ownerName = WonderUtils.GetWonderTowerName((int) WonderUtils.GetWonderTowerID(rallyData.location));
      else if (WonderUtils.IsWonder(rallyData.location))
        AllianceWar_MainDlg_RallyList.param.ownerName = Utils.XLAT("throne_wonder_name");
    }
    AllianceWar_MainDlg_RallyList.param.memberCount = memberCount;
    AllianceWar_MainDlg_RallyList.param.canJoin = false;
    AllianceWar_MainDlg_RallyList.param.isAttack = false;
    AllianceWar_MainDlg_RallyList.param.rallyType = AllianceWar_MainDlg_RallyList.Params.RallyListType.MEMBER;
    this.panel.belowList.Setup(AllianceWar_MainDlg_RallyList.param);
  }

  protected virtual void SetupTargetSlot(RallyData rallyData)
  {
    this.panel.targetSlot.Setup(rallyData.rallyId, 80f, true);
  }

  protected virtual void RefreshTime()
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this.data.rallyId);
    if (rallyData != null)
    {
      if (rallyData.state == RallyData.RallyState.waitRallying)
      {
        this.panel.rallyProgress.gameObject.SetActive(true);
        this.panel.rallyProgress.value = rallyData.waitTimeDuration.oneOverTimeDuration * (float) (NetServerTime.inst.ServerTimestamp - rallyData.waitTimeDuration.startTime);
        this.panel.rallyProgressText.text = string.Format("{0} {1}", (object) Utils.XLAT("war_rally_rallying"), (object) Utils.FormatTime1(rallyData.waitTimeDuration.endTime - NetServerTime.inst.ServerTimestamp));
      }
      else if (rallyData.state == RallyData.RallyState.attacking)
      {
        this.panel.rallyProgress.gameObject.SetActive(true);
        MarchData marchData = DBManager.inst.DB_March.Get(rallyData.rallyMarchId);
        if (marchData == null)
          return;
        this.panel.rallyProgress.value = marchData.timeDuration.oneOverTimeDuration * (float) (NetServerTime.inst.ServerTimestamp - marchData.startTime);
        this.panel.rallyProgressText.text = string.Format("{0} {1}", (object) Utils.XLAT("war_rally_marching"), (object) Utils.FormatTime1(marchData.endTime - NetServerTime.inst.ServerTimestamp));
      }
      else
      {
        if (rallyData.state != RallyData.RallyState.waitAttacking)
          return;
        this.panel.rallyProgress.gameObject.SetActive(false);
        PlayerData.inst.allianceWarManager.LoadDatas();
      }
    }
    else
      PlayerData.inst.allianceWarManager.LoadDatas();
  }

  public virtual void OnDetailClick()
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this.data.rallyId);
    if (rallyData == null)
      return;
    if (rallyData.location.K != PlayerData.inst.userData.current_world_id)
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_rally_in_other_kingdom"), (System.Action) null, 4f, false);
    else
      UIManager.inst.OpenDlg("Alliance/AllianceRallyDetialDlg", (UI.Dialog.DialogParameter) new AllianceRallyDetailPopUp.Parameter()
      {
        data_id = this.data.rallyId
      }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnSecond(int timeStamp)
  {
    this.RefreshTime();
  }

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  protected bool IsDestroy { get; set; }

  private void OnDestroy()
  {
    this.IsDestroy = true;
  }

  public void Reset()
  {
    this.panel.targetSlot = this.transform.Find("TargetSlot").gameObject.GetComponent<AllianceWar_TargetSlot>();
    this.panel.targetSlot.Reset();
    this.panel.aboveList = this.transform.Find("AboveContainer").gameObject.GetComponent<AllianceWar_MainDlg_RallyList>();
    this.panel.aboveList.Reset();
    this.panel.belowList = this.transform.Find("BelowContainer").gameObject.GetComponent<AllianceWar_MainDlg_RallyList>();
    this.panel.belowList.Reset();
    this.panel.rallyProgress = this.transform.Find("Background/MarchTimer").gameObject.GetComponent<UISlider>();
    this.panel.rallyProgressText = this.transform.Find("Background/MarchTimer/Exp").gameObject.GetComponent<UILabel>();
    UIEventTrigger component = this.transform.Find("Background/ClickIcon").gameObject.GetComponent<UIEventTrigger>();
    component.onClick.Clear();
    component.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnDetailClick)));
  }

  protected class Data
  {
    public long rallyId;
    public long marchId;
  }

  [Serializable]
  protected class Panel
  {
    public AllianceWar_TargetSlot targetSlot;
    public AllianceWar_MainDlg_RallyList aboveList;
    public AllianceWar_MainDlg_RallyList belowList;
    public UISlider rallyProgress;
    public UILabel rallyProgressText;
  }
}
