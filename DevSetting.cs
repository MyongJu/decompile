﻿// Decompiled with JetBrains decompiler
// Type: DevSetting
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

[AttributeUsage(AttributeTargets.Field)]
public class DevSetting : Attribute
{
  public string description;
  public string category;
  public string onChangedFuncName;

  public DevSetting(string desc = null, string cat = null, string changedFn = null)
  {
    this.description = desc;
    this.category = cat;
    this.onChangedFuncName = changedFn;
  }
}
