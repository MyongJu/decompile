﻿// Decompiled with JetBrains decompiler
// Type: GoldComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class GoldComponent : MonoBehaviour
{
  public UILabel current;

  private void OnEnable()
  {
    this.current.text = Utils.FormatThousands(PlayerData.inst.hostPlayer.Currency.ToString());
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnPlayerStatChanged);
  }

  private void OnPlayerStatChanged(long uid)
  {
    try
    {
      if (uid != PlayerData.inst.uid)
        return;
      this.current.text = Utils.FormatThousands(DBManager.inst.DB_User.Get(PlayerData.inst.uid).currency.gold.ToString());
    }
    catch
    {
    }
  }

  public void OnClick()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }
}
