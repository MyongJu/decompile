﻿// Decompiled with JetBrains decompiler
// Type: DiceAllRewardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DiceAllRewardPopup : Popup
{
  private Dictionary<int, DiceRewardSlot> _itemDict = new Dictionary<int, DiceRewardSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UIGrid _grid;
  [SerializeField]
  private GameObject _itemPrefab;
  [SerializeField]
  private GameObject _itemRoot;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._itemPool.Initialize(this._itemPrefab, this._itemRoot);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.ClearData();
    List<DiceMainInfo> diceMainInfoList = ConfigManager.inst.DB_DiceMain.GetDiceMainList();
    if (diceMainInfoList != null)
      diceMainInfoList = diceMainInfoList.FindAll((Predicate<DiceMainInfo>) (x => x.groupId == DicePayload.Instance.TempDiceData.GroupId));
    if (diceMainInfoList != null && diceMainInfoList.Count > 0)
    {
      diceMainInfoList.Sort((Comparison<DiceMainInfo>) ((a, b) =>
      {
        if (a.IsScore != b.IsScore)
          return b.IsScore.CompareTo(a.IsScore);
        if (a.IsScore && b.IsScore)
          return a.number.CompareTo(b.number);
        if (a.rank == b.rank)
          return a.internalId.CompareTo(b.internalId);
        return b.rank.CompareTo(a.rank);
      }));
      for (int key = 0; key < diceMainInfoList.Count; ++key)
      {
        DiceRewardSlot slot = this.GenerateSlot(new DiceData.RewardData(Utils.Hash((object) "type", (object) diceMainInfoList[key].isScore, (object) "item_id", (object) diceMainInfoList[key].rewardId, (object) "amount", (object) diceMainInfoList[key].number)));
        this._itemDict.Add(key, slot);
      }
    }
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, DiceRewardSlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this._grid.repositionNow = true;
    this._grid.Reposition();
    this._scrollView.ResetPosition();
    this._scrollView.gameObject.SetActive(false);
    this._scrollView.gameObject.SetActive(true);
  }

  private DiceRewardSlot GenerateSlot(DiceData.RewardData rewardData)
  {
    GameObject gameObject = this._itemPool.AddChild(this._grid.gameObject);
    gameObject.SetActive(true);
    DiceRewardSlot component = gameObject.GetComponent<DiceRewardSlot>();
    component.SetData(rewardData);
    return component;
  }
}
