﻿// Decompiled with JetBrains decompiler
// Type: UpLoadCustomIconPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Diagnostics;
using System.IO;
using UI;
using UnityEngine;

public class UpLoadCustomIconPopup : Popup
{
  protected const int TEXTURE_WIDTH = 128;
  protected const int TEXTURE_HEIGHT = 128;
  protected const float TIME_OUT_TIME = 30f;
  protected bool m_uploading;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    ImageImporter.Instance.ImportCallback += new ImageImporter.OnImageImported(this.OnImageImported);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    ImageImporter.Instance.ImportCallback -= new ImageImporter.OnImageImported(this.OnImageImported);
  }

  private Texture2D ScaleTexture(Texture2D source, int targetWidth, int targetHeight)
  {
    Texture2D texture2D = new Texture2D(targetWidth, targetHeight, source.format, false);
    float num1 = 1f / (float) targetWidth;
    float num2 = 1f / (float) targetHeight;
    for (int y = 0; y < texture2D.height; ++y)
    {
      for (int x = 0; x < texture2D.width; ++x)
      {
        Color pixelBilinear = source.GetPixelBilinear((float) x / (float) texture2D.width, (float) y / (float) texture2D.height);
        texture2D.SetPixel(x, y, pixelBilinear);
      }
    }
    texture2D.Apply();
    return texture2D;
  }

  protected byte[] ensureTextureSize(byte[] bytes)
  {
    byte[] numArray = bytes;
    Texture2D source = new Texture2D(1, 1);
    source.LoadImage(bytes);
    if (source.width != 128 || source.height != 128)
    {
      Texture2D texture2D = this.ScaleTexture(source, 128, 128);
      numArray = texture2D.EncodeToJPG();
      UnityEngine.Object.Destroy((UnityEngine.Object) texture2D);
    }
    UnityEngine.Object.Destroy((UnityEngine.Object) source);
    return numArray;
  }

  [DebuggerHidden]
  protected IEnumerator uploadCoroutine(string imagePath)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new UpLoadCustomIconPopup.\u003CuploadCoroutine\u003Ec__Iterator92()
    {
      imagePath = imagePath,
      \u003C\u0024\u003EimagePath = imagePath,
      \u003C\u003Ef__this = this
    };
  }

  protected void OnImageImported(bool result, string imagePath)
  {
    if (result && File.Exists(imagePath))
      this.StartCoroutine(this.uploadCoroutine(imagePath));
    else
      this.m_uploading = false;
  }

  protected void OnUpLoadResult(bool result, string icon, long icon_cd)
  {
    if (result)
    {
      PlayerData.inst.userData.Icon = icon;
      PlayerData.inst.userData.IconCd = icon_cd;
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
      if ((bool) ((UnityEngine.Object) UIManager.inst.publicHUD) && (bool) ((UnityEngine.Object) UIManager.inst.publicHUD.heroMiniDlg))
        UIManager.inst.publicHUD.heroMiniDlg.ForceRefresh();
      UIManager.inst.OpenPopup("Marksman/CommonConfirmPopup", (Popup.PopupParameter) new CommonConfirmPopup.Parameter()
      {
        title = Utils.XLAT("avatar_upload_check_title"),
        content = Utils.XLAT("avatar_upload_check_description"),
        confirmButtonText = Utils.XLAT("id_uppercase_okay")
      });
    }
    else
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_avatar_upload_fail", true), (System.Action) null, 4f, true);
  }

  public void OnButtonCloseClicked()
  {
    if (this.m_uploading)
      return;
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnPickImageClicked()
  {
    if (this.m_uploading)
      return;
    this.m_uploading = true;
    ImageImporter.Instance.requestPickImage(128, 128, NetApi.inst.GetRootUrl(), NetApi.inst.Token);
  }

  public void OnCaptureImageClicked()
  {
    if (this.m_uploading)
      return;
    this.m_uploading = true;
    ImageImporter.Instance.requestCaptureImage(128, 128, NetApi.inst.GetRootUrl(), NetApi.inst.Token);
  }
}
