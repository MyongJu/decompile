﻿// Decompiled with JetBrains decompiler
// Type: ConfigDKArenaRankReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigDKArenaRankReward
{
  private ConfigParse parse = new ConfigParse();
  private List<DKArenaRankReward> _list = new List<DKArenaRankReward>();
  public Dictionary<string, DKArenaRankReward> datas;
  private Dictionary<int, DKArenaRankReward> dicByUniqueId;

  public List<DKArenaRankReward> GetTotals()
  {
    if (this._list.Count > 0)
      return this._list;
    Dictionary<int, DKArenaRankReward>.ValueCollection.Enumerator enumerator = this.dicByUniqueId.Values.GetEnumerator();
    while (enumerator.MoveNext())
      this._list.Add(enumerator.Current);
    this._list.Sort(new Comparison<DKArenaRankReward>(this.Compare));
    return this._list;
  }

  private int Compare(DKArenaRankReward a, DKArenaRankReward b)
  {
    return a.internalId.CompareTo(b.internalId);
  }

  public void BuildDB(object res)
  {
    this.parse.Parse<DKArenaRankReward, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, DKArenaRankReward>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, DKArenaRankReward>) null;
  }

  public bool Contains(int internalId)
  {
    return this.dicByUniqueId.ContainsKey(internalId);
  }

  public bool Contains(string id)
  {
    return this.datas.ContainsKey(id);
  }

  public DKArenaRankReward GetItem(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (DKArenaRankReward) null;
  }

  public DKArenaRankReward GetItem(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (DKArenaRankReward) null;
  }
}
