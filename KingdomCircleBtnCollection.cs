﻿// Decompiled with JetBrains decompiler
// Type: KingdomCircleBtnCollection
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class KingdomCircleBtnCollection
{
  private Dictionary<string, CityCircleBtnPara> circleBtnPair = new Dictionary<string, CityCircleBtnPara>();
  private static KingdomCircleBtnCollection _instance;

  public static KingdomCircleBtnCollection Instance
  {
    get
    {
      if (KingdomCircleBtnCollection._instance == null)
      {
        KingdomCircleBtnCollection._instance = new KingdomCircleBtnCollection();
        if (KingdomCircleBtnCollection._instance == null)
          throw new ArgumentException("lost of CircleBtnCollection");
      }
      return KingdomCircleBtnCollection._instance;
    }
  }

  public Dictionary<string, CityCircleBtnPara> CircleBtnPair
  {
    get
    {
      return this.circleBtnPair;
    }
  }

  private void Init()
  {
    this.circleBtnPair.Add("dig", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_occupy", "kingdom_tile_occupy", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "DigCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("troopdetail", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_profile", "march_troops", (EventDelegate) null, true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("troopcallback", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_call_back", "march_callback", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "RecallFromCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("troopspeedup", new CityCircleBtnPara(AtlasType.Gui_2, "icon_city_speedup", "march_speedup", (EventDelegate) null, true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("teleport", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_teleport", "kingdom_tile_teleport", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "Teleport2CurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("crossteleport", new CityCircleBtnPara(AtlasType.Gui_2, "icon_invade_other_kingdoms", "id_invade", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "KingdomTeleport"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("crossbackteleport", new CityCircleBtnPara(AtlasType.Gui_2, "icon_return_self_kingdom", "id_return_home", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "KingdomTeleportBack"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("joinKingdom", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom", "id_join_kingdom", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "BeginnerTeleport"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("occupyTile", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_occupy", "kingdom_tile_occupy", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "EncampCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("inviteTeleport", new CityCircleBtnPara(AtlasType.Gui_2, "city_info", "id_teleport_invitation", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "OnInviteTeleport"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("gatherresource", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_occupy", "kingdom_tile_occupy", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "GatherCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("gatherSuperMine", new CityCircleBtnPara(AtlasType.Gui_2, "icon_alliance_mining", "id_gather", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "GatherCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("teleportOtherKingdom", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom", "id_join_kingdom", (EventDelegate) null, false, string.Format(ScriptLocalization.Get("toast_teleport_change_kingdom_conditions", true), (object) 5, (object) 6), (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("noBeginnerTeleport", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom", "id_join_kingdom", (EventDelegate) null, false, ScriptLocalization.Get("toast_kingdom_no_beginner_teleport", true), (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("occupyOtherKingdom", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_occupy_gray", "kingdom_tile_occupy", (EventDelegate) null, false, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("teleport2Mountain", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_teleport_gray", "kingdom_tile_teleport", (EventDelegate) null, false, "teleport_no_mountain", (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("occupyMountain", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_occupy_gray", "kingdom_tile_occupy", (EventDelegate) null, false, "occupy_no_mountain", (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("teleport2Water", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_teleport_gray", "kingdom_tile_teleport", (EventDelegate) null, false, "teleport_no_water", (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("occupyWater", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_occupy_gray", "kingdom_tile_occupy", (EventDelegate) null, false, "occupy_no_water", (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("occupyWonder", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_occupy_gray", "kingdom_tile_occupy", (EventDelegate) null, false, "occupy_no_wonder", (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("ownerProfile", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_profile", "kingdom_city_mycity_profile", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlg", (UI.Dialog.DialogParameter) null, true, true, true))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("ownerBoost", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_city_boost", "id_city_buff", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("Boost/BoostMenu", (UI.Dialog.DialogParameter) null, true, true, true))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("enter_city", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_enter_city", "kingdom_city_mycity_enter", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "ShowCity"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("allyProfile", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_profile", "kingdom_city_player_profile", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "ReinforceCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("allyReinforce", new CityCircleBtnPara(AtlasType.Gui_2, "icon_reinforce", "kingdom_city_player_reinforcement", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "ReinforceCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("zhanlingWonder", new CityCircleBtnPara(AtlasType.Gui_2, "icon_reinforce", "kingdom_tile_occupy", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "OccupyWonder"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("allyTradeKingdom", new CityCircleBtnPara(AtlasType.Gui_2, "icon_trading", "kingdom_city_player_trade", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Map, "OnTrade"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("enemyProfile", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_profile", "kingdom_city_player_profile", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "ScoutCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("enemyScout", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_scout", "kingdom_city_player_scout", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "ScoutCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("enemyRally", new CityCircleBtnPara(AtlasType.Gui_2, "icon_rally", "kingdom_city_player_rally", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "RallyCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("enemyAllianceBossRally", new CityCircleBtnPara(AtlasType.Gui_2, "icon_rally", "kingdom_city_player_rally", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "RallyToAllianceBoss"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("enemyAttack", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_attack", "kingdom_city_player_attack", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "AttackCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("enemyFortressAttack", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_attack", "kingdom_city_player_attack", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "AttackCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("encampInfo", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_view_army", "kingdom_encamp_troops", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "ViewMyEncamp"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("encampRecall", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_back_city", "kingdom_encamp_return", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "RecallMyEncamp"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("enemyEncampInfo", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_profile", "kingdom_city_player_profile", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "ScoutCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("enemyEncampScout", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_scout", "kingdom_city_player_scout", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "ScoutCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("enemyEncampAttack", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_attack", "kingdom_city_player_attack", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "AttackCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("resTileInfo", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_description", "kingdom_resource_info", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "OnShowTileInfo"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("digsiteInfo", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_description", "kingdom_resource_info", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "OnDigsiteInfo"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("gveCampInfo", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_description", "kingdom_resource_info", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Map, "OnShowGveInfo"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("gveFallenKnight", new CityCircleBtnPara(AtlasType.Gui_2, "icon_fallen_knight", "event_fallen_knight_name", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Map, "OnShowFallenKnight"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("gveCampAttack", new CityCircleBtnPara(AtlasType.Gui_2, "icon_dragon_stats", "kingdom_city_player_attack", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Map, "OnGveAttack"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("takeBackFortress", new CityCircleBtnPara(AtlasType.Gui_2, "icon_dragon_stats", "id_uppercase_deconstruct", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Map, "OnTakeBackFortress"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("buildfortress", new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_construct", "id_construct", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "ReinforceCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("guardfortress", new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_guard", "id_garrison", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "ReinforceCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("repairfortress", new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_repair", "id_repair", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "ReinforceCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("demolishfortress", new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_demolish", "id_demolish", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "AttackCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("fortresstroopcallback", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_call_back", "march_callback", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "RecallFromCurFortress"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
  }

  public void Create()
  {
    this.CircleBtnPair.Clear();
    this.Init();
  }
}
