﻿// Decompiled with JetBrains decompiler
// Type: TradingHallBuyPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TradingHallBuyPopup : Popup
{
  private ProgressBarTweenProxy _proxy = new ProgressBarTweenProxy();
  private Dictionary<string, string> _exchangeFreezeParam = new Dictionary<string, string>();
  [SerializeField]
  private UILabel _labelItemName;
  [SerializeField]
  private UILabel _labelEquipmentEnhanced;
  [SerializeField]
  private UILabel _labelItemUnitPrice;
  [SerializeField]
  private UILabel _labelItemFreezeTime;
  [SerializeField]
  private UILabel _labelTotalPrice;
  [SerializeField]
  private ItemIconRenderer _itemIconRenderer;
  [SerializeField]
  private UIButton _buyButton;
  [SerializeField]
  private UIProgressBar _buyProgressBar;
  [SerializeField]
  private UIInput _buyInput;
  private TradingHallData.BuyGoodData _buyGoodData;
  private int _itemUnitPrice;
  private long _itemCount;
  private long _maxBuyCount;
  private long _curBuyCount;
  private System.Action _onBuyGoodSuccess;
  private TradingHallCatalog _catalog;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    TradingHallBuyPopup.Parameter parameter = orgParam as TradingHallBuyPopup.Parameter;
    if (parameter != null)
    {
      this._buyGoodData = parameter.buyGoodData;
      this._itemUnitPrice = parameter.itemUnitPrice;
      this._itemCount = parameter.itemCount;
      this._catalog = parameter.catalog;
      this._onBuyGoodSuccess = parameter.onBuyGoodSuccess;
    }
    this._curBuyCount = this._maxBuyCount = (long) Mathf.Min((float) this._itemCount, this._itemUnitPrice <= 0 ? (float) this._itemCount : Mathf.Ceil((float) PlayerData.inst.userData.currency.gold / (float) this._itemUnitPrice));
    if (this._buyGoodData != null && TradingHallPayload.Instance.GetBagType(this._buyGoodData.ExchangeId) != BagType.None)
      this._curBuyCount = this._maxBuyCount = (long) Mathf.Min((float) this._maxBuyCount, 1f);
    this._proxy.MaxSize = (int) this._maxBuyCount;
    this._buyProgressBar.value = this._maxBuyCount <= 0L ? 0.0f : Mathf.Clamp01((float) this._curBuyCount / (float) this._maxBuyCount);
    this._buyProgressBar.numberOfSteps = (int) this._maxBuyCount + 1;
    this.UpdateUI();
    this.UpdateBuySlider();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnItemInfoClick()
  {
    if (this._buyGoodData == null)
      return;
    Utils.ShowItemTip(this._buyGoodData.ItemId, this._itemIconRenderer.transform, (long) this._buyGoodData.EquipmentId, 0L, this._buyGoodData.EquipmentEnhanced);
  }

  public void OnBuyBtnPressed()
  {
    if (this._buyGoodData == null)
      return;
    TradingHallPayload.Instance.BuyGood(this._buyGoodData.ExchangeId, this._itemUnitPrice, this._curBuyCount, this._catalog, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
      {
        if (this._onBuyGoodSuccess != null)
          this._onBuyGoodSuccess();
        this.OnCloseBtnPressed();
      }
      else
      {
        TradingHallPayload.Instance.CheckErrorMessage(data as Hashtable);
        int catalog1Id = -1;
        int catalog2Id = -1;
        int catalog3Id = -1;
        TradingHallPayload.Instance.GetCatalogId(this._catalog, ref catalog1Id, ref catalog2Id, ref catalog3Id);
        TradingHallPayload.Instance.GetBuyGoods(catalog1Id, catalog2Id, catalog3Id, true, (System.Action<bool, object>) null);
      }
    }));
  }

  public void OnSliderValueChanged()
  {
    int num = Mathf.RoundToInt((float) this._maxBuyCount * this._buyProgressBar.value);
    if (this._curBuyCount == (long) num)
      return;
    this._curBuyCount = (long) num;
    this.UpdateBuySlider();
  }

  public void OnSliderSubtractBtnDown()
  {
    this._proxy.Slider = this._buyProgressBar;
    this._proxy.Play(-1, -1f);
  }

  public void OnSliderSubtractBtnUp()
  {
    this._proxy.Stop();
    this.UpdateBuySlider();
  }

  public void OnSliderPlusBtnDown()
  {
    this._proxy.Slider = this._buyProgressBar;
    this._proxy.Play(1, -1f);
  }

  public void OnSliderPlusBtnUp()
  {
    this._proxy.Stop();
    this.UpdateBuySlider();
  }

  private void UpdateUI()
  {
    if (this._buyGoodData == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._buyGoodData.ItemId);
    if (itemStaticInfo != null)
    {
      this._labelItemName.text = itemStaticInfo.LocName;
      this._labelEquipmentEnhanced.text = this._buyGoodData.EquipmentEnhanced <= 0 ? string.Empty : "+" + this._buyGoodData.EquipmentEnhanced.ToString();
      this._labelItemName.text += this._labelEquipmentEnhanced.text;
      EquipmentManager.Instance.ConfigQualityLabelWithColor(this._labelItemName, itemStaticInfo.Quality);
      this._labelItemUnitPrice.text = Utils.FormatThousands(this._itemUnitPrice.ToString());
      ExchangeHallInfo exchangeHallInfo = ConfigManager.inst.DB_ExchangeHall.Get(this._buyGoodData.ExchangeId);
      if (exchangeHallInfo != null)
      {
        this._exchangeFreezeParam.Clear();
        this._exchangeFreezeParam.Add("0", Mathf.CeilToInt((float) exchangeHallInfo.freezeTime / 86400f).ToString());
        this._labelItemFreezeTime.text = ScriptLocalization.GetWithPara("exchange_hall_item_frozen_time_description", this._exchangeFreezeParam, true);
      }
      this._itemIconRenderer.SetData(this._buyGoodData.ItemId, string.Empty, true);
    }
    this.UpdateBuySlider();
  }

  private void UpdateBuySlider()
  {
    this._buyInput.label.text = Utils.FormatThousands(this._curBuyCount.ToString());
    this._labelTotalPrice.text = Utils.FormatThousands((this._curBuyCount * (long) this._itemUnitPrice).ToString());
    this._buyButton.isEnabled = this._curBuyCount > 0L;
  }

  public class Parameter : Popup.PopupParameter
  {
    public TradingHallData.BuyGoodData buyGoodData;
    public int itemUnitPrice;
    public long itemCount;
    public TradingHallCatalog catalog;
    public System.Action onBuyGoodSuccess;
  }
}
