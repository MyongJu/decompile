﻿// Decompiled with JetBrains decompiler
// Type: WebviewPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class WebviewPopup : Popup
{
  private WebViewObject webviewInst;
  public UIButton close;
  public UIButton back;
  public UIButton forward;
  public UIButton refresh;
  public UILabel loadingtips;
  private bool visiable;
  private bool loading;
  public UIWidget webArea;

  private void Start()
  {
    EventDelegate.Add(this.close.onClick, (EventDelegate.Callback) (() => UIManager.inst.CloseTopPopup((Popup.PopupParameter) null)));
    EventDelegate.Add(this.back.onClick, (EventDelegate.Callback) (() =>
    {
      if (!(bool) ((UnityEngine.Object) this.webviewInst) || !this.webviewInst.CanGoBack())
        return;
      this.webviewInst.GoBack();
    }));
    EventDelegate.Add(this.forward.onClick, (EventDelegate.Callback) (() =>
    {
      if (!(bool) ((UnityEngine.Object) this.webviewInst) || !this.webviewInst.CanGoForward())
        return;
      this.webviewInst.GoForward();
    }));
    EventDelegate.Add(this.refresh.onClick, (EventDelegate.Callback) (() =>
    {
      if (!(bool) ((UnityEngine.Object) this.webviewInst) || !this.webviewInst.CanGoBack())
        return;
      this.webviewInst.GoBack();
    }));
  }

  private void Update()
  {
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.webviewInst = this.gameObject.AddComponent<WebViewObject>();
    WebViewObject webviewInst = this.webviewInst;
    System.Action<string> action1 = new System.Action<string>(this.OnError);
    System.Action<string> action2 = new System.Action<string>(this.OnLoaded);
    System.Action<string> cb = new System.Action<string>(this.OnCallFromJS);
    int num1 = 0;
    string ua = "Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53";
    System.Action<string> err = action1;
    System.Action<string> ld = action2;
    int num2 = 1;
    webviewInst.Init(cb, num1 != 0, ua, err, ld, num2 != 0);
    Vector3[] worldCorners = this.webArea.worldCorners;
    Vector3 screenPoint1 = UICamera.current.cachedCamera.WorldToScreenPoint(worldCorners[0]);
    Vector3 screenPoint2 = UICamera.current.cachedCamera.WorldToScreenPoint(worldCorners[2]);
    Vector2 localSize = this.webArea.localSize;
    Vector3 lossyScale = this.webArea.transform.lossyScale;
    this.webviewInst.SetMargins((int) screenPoint1.x, (int) ((double) Screen.height - (double) screenPoint2.y), (int) ((double) Screen.width - (double) screenPoint2.x), (int) screenPoint1.y);
    this.webviewInst.SetVisibility(this.visiable);
    this.LoadURL((orgParam as WebviewPopupParam).URL);
  }

  private void DestroyWebview()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.webviewInst);
    this.webviewInst = (WebViewObject) null;
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.DestroyWebview();
  }

  private void OnCallFromJS(string command)
  {
    Debug.Log((object) string.Format("CallFromJS[{0}]", (object) command));
  }

  private void OnError(string errorString)
  {
    Debug.Log((object) string.Format("CallOnError[{0}]", (object) errorString));
    this.OnLoadResult();
  }

  private void OnLoaded(string loadInfo)
  {
    if (!this.visiable)
    {
      this.visiable = true;
      this.webviewInst.SetVisibility(this.visiable);
      this.OnLoadResult();
    }
    Debug.Log((object) string.Format("CallOnLoaded[{0}]", (object) loadInfo));
  }

  private void LoadURL(string url)
  {
    this.loading = true;
    this.loadingtips.gameObject.SetActive(true);
    this.webviewInst.LoadURL(url);
  }

  private void OnLoadResult()
  {
    this.loading = false;
    this.loadingtips.gameObject.SetActive(false);
  }
}
