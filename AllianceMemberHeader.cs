﻿// Decompiled with JetBrains decompiler
// Type: AllianceMemberHeader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class AllianceMemberHeader : MonoBehaviour, IOnlineUpdateable
{
  private int m_Title = 1;
  public UISprite mHeaderIcon;
  public UILabel mHeaderLabel;
  public UILabel mOnlineLabel;
  private AllianceData m_AllianceData;

  public void SetDetails(AllianceData allianceData, int title, int online, int total)
  {
    this.m_AllianceData = allianceData;
    this.m_Title = title;
    this.mHeaderIcon.spriteName = AllianceUtilities.GetTitleIconPath(title);
    this.mHeaderLabel.text = this.m_AllianceData.GetTitleName(title);
    this.mOnlineLabel.text = string.Format("({0}/{1})", (object) online, (object) total);
  }

  public int title
  {
    get
    {
      return this.m_Title;
    }
  }

  public void UpdateOnlineStatus(List<long> onlineStatus)
  {
    int num1 = 0;
    int num2 = 0;
    using (Dictionary<long, AllianceMemberData>.ValueCollection.Enumerator enumerator = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId).members.datas.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceMemberData current = enumerator.Current;
        if (current.title == this.title && current.status == 0)
        {
          UserData userData = DBManager.inst.DB_User.Get(current.uid);
          userData.online = false;
          ++num2;
          for (int index = 0; index < onlineStatus.Count; ++index)
          {
            if (onlineStatus[index] == userData.channelId)
            {
              userData.online = true;
              ++num1;
              break;
            }
          }
        }
      }
    }
    this.mOnlineLabel.text = string.Format("({0}/{1})", (object) num1, (object) num2);
  }

  GameObject IOnlineUpdateable.get_gameObject()
  {
    return this.gameObject;
  }
}
