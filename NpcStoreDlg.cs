﻿// Decompiled with JetBrains decompiler
// Type: NpcStoreDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class NpcStoreDlg : Dialog
{
  private List<NpcStoreItemSlot> _itemSlotList = new List<NpcStoreItemSlot>();
  private float gap = 0.5f;
  private int NextRefreshTime;
  private bool mBlockFlag;
  [SerializeField]
  private NpcStoreDlg.Panel panel;
  private bool initialized;

  private void Start()
  {
    UIViewport componentInChildren = this.GetComponentInChildren<UIViewport>();
    if (!((UnityEngine.Object) null != (UnityEngine.Object) componentInChildren))
      return;
    componentInChildren.sourceCamera = UIManager.inst.ui2DCamera;
  }

  public override void OnOpen(UIControler.UIParameter orgParam = null)
  {
    base.OnOpen(orgParam);
  }

  public override void OnClose(UIControler.UIParameter orgParam = null)
  {
    base.OnClose(orgParam);
    this.ClearData();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.NextRefreshTime = PlayerData.inst.npcStoreUpdateTime / 86400 * 86400 + 86400;
    if (this.NextRefreshTime - NetServerTime.inst.ServerTimestamp > 0)
    {
      this.UpdateData();
      this.InitItems();
    }
    Utils.SetPortrait(this.panel.TT_PlayceHolder, "Texture/GUI_Textures/npc_store_character");
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  private void InitItems()
  {
    List<int> goods = DBManager.inst.DB_Local_NpcStore.GetGoods();
    if (goods == null)
      return;
    this._itemSlotList.Clear();
    for (int index = 0; index < goods.Count; ++index)
    {
      NpcStoreItemSlot component;
      if (index < this.panel.items.Count)
      {
        component = this.panel.items[index];
        component.gameObject.SetActive(true);
      }
      else
      {
        GameObject gameObject = NGUITools.AddChild(this.panel.itemTabel.gameObject, this.panel.itemSlotOrg.gameObject);
        gameObject.name = string.Format("item_slot_{0}", (object) (100 + index));
        gameObject.SetActive(true);
        component = gameObject.GetComponent<NpcStoreItemSlot>();
        this.panel.items.Add(component);
      }
      this._itemSlotList.Add(component);
      component.Init((object) new NpcStoreItemSlot.Data()
      {
        itemInteranlId = goods[index],
        inIndex = index,
        onBuyButtonClickEvent = new System.Action<int>(this.OnBuyButtonClick)
      });
    }
    this.panel.itemTabel.Reposition();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  private void ClearData()
  {
    for (int index = 0; index < this._itemSlotList.Count; ++index)
    {
      NGUITools.SetActive(this._itemSlotList[index].gameObject, false);
      UnityEngine.Object.Destroy((UnityEngine.Object) this._itemSlotList[index].gameObject);
    }
    this._itemSlotList.Clear();
  }

  private void OnSecond(int serverTime)
  {
    int time = this.NextRefreshTime - serverTime;
    if (time <= 0)
    {
      time = 0;
      this.Reload();
    }
    this.panel.refreshLeftTime.text = Utils.FormatTime(time, false, false, true);
    this.panel.curGold.text = Utils.FormatThousands(PlayerData.inst.userData.currency.gold.ToString());
    this.panel.resFood.text = Utils.FormatShortThousandsLong((long) PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD));
    this.panel.resWood.text = Utils.FormatShortThousandsLong((long) PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD));
    this.panel.resOre.text = Utils.FormatShortThousandsLong((long) PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE));
    this.panel.resSilver.text = Utils.FormatShortThousandsLong((long) PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER));
    if (this.mBlockFlag)
      return;
    for (int index = 0; index < this.panel.items.Count; ++index)
      this.panel.items[index].RefreshCost();
  }

  private void UpdateData()
  {
    this.NextRefreshTime = PlayerData.inst.npcStoreUpdateTime / 86400 * 86400 + 86400;
    int time = this.NextRefreshTime - NetServerTime.inst.ServerTimestamp;
    if (time <= 0)
      time = 0;
    this.panel.refreshLeftTime.text = Utils.FormatTime(time, false, false, true);
    this.panel.refreshLeftTime.text = Utils.FormatTime(time, false, false, true);
    this.panel.curGold.text = Utils.FormatThousands(PlayerData.inst.userData.currency.gold.ToString());
    this.panel.resFood.text = Utils.FormatShortThousandsLong((long) PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD));
    this.panel.resWood.text = Utils.FormatShortThousandsLong((long) PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD));
    this.panel.resOre.text = Utils.FormatShortThousandsLong((long) PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE));
    this.panel.resSilver.text = Utils.FormatShortThousandsLong((long) PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER));
    this.panel.refreshLeftTime.text = Utils.FormatTime(this.NextRefreshTime - NetServerTime.inst.ServerTimestamp, false, false, true);
    this.ResfreshCost();
  }

  private void RefreshDlg(int index = -1)
  {
    this.UpdateData();
    if (index == -1)
    {
      this.StartCoroutine(this.ReplaceAll());
      this.panel.BT_refresh.isEnabled = false;
      this.SetItemsButtonEnable(false);
    }
    else
      this.ReplaceItem(index);
  }

  private void SetItemsButtonEnable(bool enable)
  {
    int index = 0;
    for (int count = this.panel.items.Count; index < count; ++index)
      this.panel.items[index].ButtonEnable = enable;
    this.mBlockFlag = !enable;
  }

  [DebuggerHidden]
  private IEnumerator ReplaceAll()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new NpcStoreDlg.\u003CReplaceAll\u003Ec__Iterator88()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void ReplaceItem(int index)
  {
    NpcStoreItemSlot.Data data = new NpcStoreItemSlot.Data();
    List<int> goods = DBManager.inst.DB_Local_NpcStore.GetGoods();
    data.itemInteranlId = goods[index];
    data.inIndex = index;
    data.onBuyButtonClickEvent = new System.Action<int>(this.OnBuyButtonClick);
    new DynamicReplaceAnimator().PlayReplaceAnimation((DynamicReplaceItem) this.panel.items[index], (object) data);
  }

  public void OnBuyButtonClick(int index)
  {
    this.BuyItem(index);
  }

  private void ResfreshCost()
  {
    int price = ConfigManager.inst.DB_NpcStoreRefreshPrice.GetPrice(DBManager.inst.DB_Local_NpcStore.buyTimes);
    if (price > 0)
    {
      this.panel.refreshCostContainer.gameObject.SetActive(true);
      this.panel.refresh_free.gameObject.SetActive(false);
      this.panel.refreshCost.text = price.ToString();
      if ((long) price > PlayerData.inst.userData.currency.gold)
        this.panel.refreshCost.color = Color.red;
      else
        this.panel.refreshCost.color = Color.white;
    }
    else
    {
      this.panel.refreshCostContainer.gameObject.SetActive(false);
      this.panel.refresh_free.gameObject.SetActive(true);
    }
  }

  public void Reload()
  {
    if (PlayerData.inst.npcStoreUpdateTime / 7200 >= NetServerTime.inst.ServerTimestamp / 7200)
      return;
    HubPort portByAction = MessageHub.inst.GetPortByAction("npcstore:loadGoods");
    PlayerData.inst.npcStoreUpdateTime = NetServerTime.inst.ServerTimestamp;
    portByAction.SendRequest((Hashtable) null, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      DBManager.inst.DB_Local_NpcStore.Update(arg2, (long) NetServerTime.inst.ServerTimestamp);
      if (!this.initialized)
      {
        this.UpdateData();
        this.InitItems();
        this.initialized = true;
      }
      else
        this.RefreshDlg(-1);
    }), true);
  }

  public void BuyItem(int slotIndex)
  {
    if (this.panel.items[slotIndex].costType == "gold")
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      para.Add("num", this.panel.items[slotIndex].costValue.ToString());
      para.Add("item_name", this.panel.items[slotIndex].itemName);
      NpcStoreInfo itemInfo = ConfigManager.inst.DB_NpcStore.GetData(this.panel.items[slotIndex].npcStoreItemInteranlId);
      string imagePath = itemInfo.imagePath;
      float itemValue = (float) itemInfo.itemValue;
      UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
      {
        description = ScriptLocalization.GetWithPara("confirm_gold_spend_item_desc", para, true),
        goldNum = this.panel.items[slotIndex].costValue,
        confirmButtonClickEvent = (System.Action) (() => MessageHub.inst.GetPortByAction("npcstore:buyGoods").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "slot_id", (object) (slotIndex + 1)), (System.Action<bool, object>) ((arg1, arg2) =>
        {
          DBManager.inst.DB_Local_NpcStore.Update(arg2, (long) NetServerTime.inst.ServerTimestamp);
          this.RefreshDlg(slotIndex);
          if (!arg1)
            return;
          RewardsCollectionAnimator.Instance.Clear();
          if (itemInfo.itemType != "item")
          {
            ResRewardsInfo.Data data = new ResRewardsInfo.Data();
            data.count = itemInfo.itemValue;
            string itemType = itemInfo.itemType;
            if (itemType != null)
            {
              // ISSUE: reference to a compiler-generated field
              if (NpcStoreDlg.\u003C\u003Ef__switch\u0024map8E == null)
              {
                // ISSUE: reference to a compiler-generated field
                NpcStoreDlg.\u003C\u003Ef__switch\u0024map8E = new Dictionary<string, int>(6)
                {
                  {
                    "food",
                    0
                  },
                  {
                    "wood",
                    1
                  },
                  {
                    "ore",
                    2
                  },
                  {
                    "silver",
                    3
                  },
                  {
                    "steel",
                    4
                  },
                  {
                    "coin",
                    5
                  }
                };
              }
              int num;
              // ISSUE: reference to a compiler-generated field
              if (NpcStoreDlg.\u003C\u003Ef__switch\u0024map8E.TryGetValue(itemType, out num))
              {
                switch (num)
                {
                  case 0:
                    data.rt = ResRewardsInfo.ResType.Food;
                    goto label_15;
                  case 1:
                    data.rt = ResRewardsInfo.ResType.Wood;
                    goto label_15;
                  case 2:
                    data.rt = ResRewardsInfo.ResType.Ore;
                    goto label_15;
                  case 3:
                    data.rt = ResRewardsInfo.ResType.Silver;
                    goto label_15;
                  case 4:
                    data.rt = ResRewardsInfo.ResType.Steel;
                    goto label_15;
                  case 5:
                    data.rt = ResRewardsInfo.ResType.Coin;
                    goto label_15;
                }
              }
            }
            data.rt = ResRewardsInfo.ResType.Food;
label_15:
            RewardsCollectionAnimator.Instance.Ress.Add(data);
            RewardsCollectionAnimator.Instance.CollectResource(true);
          }
          else
          {
            RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
            {
              count = (float) itemInfo.itemValue,
              icon = itemInfo.imagePath
            });
            RewardsCollectionAnimator.Instance.CollectItems(true);
          }
          AudioManager.Instance.PlaySound("sfx_city_trading_post_buy", false);
        }), true))
      });
    }
    else
    {
      NpcStoreInfo itemInfo = ConfigManager.inst.DB_NpcStore.GetData(this.panel.items[slotIndex].npcStoreItemInteranlId);
      string imagePath = itemInfo.imagePath;
      float itemValue = (float) itemInfo.itemValue;
      MessageHub.inst.GetPortByAction("npcstore:buyGoods").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "slot_id", (object) (slotIndex + 1)), (System.Action<bool, object>) ((arg1, arg2) =>
      {
        DBManager.inst.DB_Local_NpcStore.Update(arg2, (long) NetServerTime.inst.ServerTimestamp);
        this.RefreshDlg(slotIndex);
        if (!arg1)
          return;
        RewardsCollectionAnimator.Instance.Clear();
        if (itemInfo.itemType != "item")
        {
          ResRewardsInfo.Data data = new ResRewardsInfo.Data();
          data.count = itemInfo.itemValue;
          string itemType = itemInfo.itemType;
          if (itemType != null)
          {
            // ISSUE: reference to a compiler-generated field
            if (NpcStoreDlg.\u003C\u003Ef__switch\u0024map90 == null)
            {
              // ISSUE: reference to a compiler-generated field
              NpcStoreDlg.\u003C\u003Ef__switch\u0024map90 = new Dictionary<string, int>(6)
              {
                {
                  "food",
                  0
                },
                {
                  "wood",
                  1
                },
                {
                  "ore",
                  2
                },
                {
                  "silver",
                  3
                },
                {
                  "steel",
                  4
                },
                {
                  "coin",
                  5
                }
              };
            }
            int num;
            // ISSUE: reference to a compiler-generated field
            if (NpcStoreDlg.\u003C\u003Ef__switch\u0024map90.TryGetValue(itemType, out num))
            {
              switch (num)
              {
                case 0:
                  data.rt = ResRewardsInfo.ResType.Food;
                  goto label_15;
                case 1:
                  data.rt = ResRewardsInfo.ResType.Wood;
                  goto label_15;
                case 2:
                  data.rt = ResRewardsInfo.ResType.Ore;
                  goto label_15;
                case 3:
                  data.rt = ResRewardsInfo.ResType.Silver;
                  goto label_15;
                case 4:
                  data.rt = ResRewardsInfo.ResType.Steel;
                  goto label_15;
                case 5:
                  data.rt = ResRewardsInfo.ResType.Coin;
                  goto label_15;
              }
            }
          }
          data.rt = ResRewardsInfo.ResType.Food;
label_15:
          RewardsCollectionAnimator.Instance.Ress.Add(data);
          RewardsCollectionAnimator.Instance.CollectResource(true);
        }
        else
        {
          RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
          {
            count = (float) itemInfo.itemValue,
            icon = itemInfo.imagePath
          });
          RewardsCollectionAnimator.Instance.CollectItems(true);
        }
        AudioManager.Instance.PlaySound("sfx_city_trading_post_buy", false);
      }), true);
    }
  }

  public void OnRunesClicked()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  public void Refresh()
  {
    int price = ConfigManager.inst.DB_NpcStoreRefreshPrice.GetPrice(DBManager.inst.DB_Local_NpcStore.buyTimes);
    if (price > 0)
      UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
      {
        confirmButtonClickEvent = (System.Action) (() => MessageHub.inst.GetPortByAction("npcstore:refreshGoods").SendRequest((Hashtable) null, (System.Action<bool, object>) ((arg1, arg2) =>
        {
          DBManager.inst.DB_Local_NpcStore.Update(arg2, (long) NetServerTime.inst.ServerTimestamp);
          this.RefreshDlg(-1);
          AudioManager.Instance.PlaySound("sfx_city_trading_post_refresh", false);
        }), true)),
        description = ScriptLocalization.GetWithPara("confirm_gold_spend_trading_post_refresh_desc", new Dictionary<string, string>()
        {
          {
            "num",
            price.ToString()
          }
        }, true),
        goldNum = price
      });
    else
      MessageHub.inst.GetPortByAction("npcstore:refreshGoods").SendRequest((Hashtable) null, (System.Action<bool, object>) ((arg1, arg2) =>
      {
        DBManager.inst.DB_Local_NpcStore.Update(arg2, (long) NetServerTime.inst.ServerTimestamp);
        this.RefreshDlg(-1);
        AudioManager.Instance.PlaySound("sfx_city_trading_post_refresh", false);
      }), true);
  }

  [Serializable]
  protected class Panel
  {
    public UILabel leftDiscription;
    public UILabel resFood;
    public UILabel resWood;
    public UILabel resOre;
    public UILabel resSilver;
    public UILabel refreshLeftTime;
    public Transform refreshCostContainer;
    public UILabel refreshCost;
    public UILabel refresh_free;
    public UIButton BT_refresh;
    public UITexture TT_PlayceHolder;
    public NpcStoreItemSlot itemSlotOrg;
    public UIScrollView scrollView;
    public UITable itemTabel;
    public List<NpcStoreItemSlot> items;
    public UILabel curGold;
  }
}
