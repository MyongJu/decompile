﻿// Decompiled with JetBrains decompiler
// Type: KingdomView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class KingdomView
{
  private GameObject m_Quad;
  private KingdomData m_Kingdom;

  public KingdomView(KingdomData kingdom)
  {
    this.m_Kingdom = kingdom;
  }

  public bool Showing
  {
    get
    {
      return (bool) ((Object) this.m_Quad);
    }
  }

  public void CullAndShow(Rect viewport, Transform transform)
  {
    if (viewport.Overlaps(this.m_Kingdom.Dimension, true))
      this.ShowBackground(transform);
    else
      this.HideBackground();
  }

  public void Dispose()
  {
    this.HideBackground();
  }

  private void ShowBackground(Transform transform)
  {
    if ((bool) ((Object) this.m_Quad) || !(bool) ((Object) transform))
      return;
    this.m_Quad = PrefabManagerEx.Instance.Spawn(this.m_Kingdom.PrefabName, transform);
    this.m_Quad.transform.localPosition = new Vector3(this.m_Kingdom.Dimension.x + 0.5f * this.m_Kingdom.MapData.PixelsPerKingdom.x, this.m_Kingdom.Dimension.y - 0.5f * this.m_Kingdom.MapData.PixelsPerKingdom.y, 0.0f);
    this.m_Quad.transform.localScale = new Vector3(this.m_Kingdom.MapData.PixelsPerKingdom.x, this.m_Kingdom.MapData.PixelsPerKingdom.y);
  }

  private void HideBackground()
  {
    if (!((Object) this.m_Quad != (Object) null))
      return;
    PrefabManagerEx.Instance.Destroy(this.m_Quad);
    this.m_Quad = (GameObject) null;
  }
}
