﻿// Decompiled with JetBrains decompiler
// Type: GameLoadingAnnouncementPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class GameLoadingAnnouncementPayload
{
  private static GameLoadingAnnouncementPayload _instance;
  private string _announcementContent;

  public event System.Action AnnoucementContentReady;

  public static GameLoadingAnnouncementPayload Instance
  {
    get
    {
      if (GameLoadingAnnouncementPayload._instance == null)
        GameLoadingAnnouncementPayload._instance = new GameLoadingAnnouncementPayload();
      return GameLoadingAnnouncementPayload._instance;
    }
  }

  public string AnnouncementContent
  {
    get
    {
      return this._announcementContent;
    }
    set
    {
      this._announcementContent = value;
      if (this.AnnoucementContentReady == null)
        return;
      this.AnnoucementContentReady();
    }
  }
}
