﻿// Decompiled with JetBrains decompiler
// Type: PVPMap
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PVPMap : MonoBehaviour
{
  private static Dictionary<int, string> AllAllianceBuildingPrefabNames = new Dictionary<int, string>()
  {
    {
      0,
      "tiles_alliance_fortress"
    },
    {
      1,
      "tiles_alliance_turret"
    },
    {
      2,
      "tiles_alliance_storehouse"
    },
    {
      3,
      "tiles_alliance_farm"
    },
    {
      4,
      "tiles_alliance_sawmil"
    },
    {
      5,
      "tiles_alliance_mine"
    },
    {
      6,
      "tiles_alliance_house"
    },
    {
      7,
      "tiles_alliance_portal"
    },
    {
      8,
      "tiles_alliance_hospital"
    },
    {
      9,
      "tiles_alliance_dragon_altar"
    }
  };
  public static Coordinate PendingGotoRequest = new Coordinate();
  private Plane m_XYPlane = new Plane(Vector3.back, Vector3.zero);
  private Vector3 m_LastTarget = Vector3.zero;
  private PVPMap.CameraPara m_CameraPara = new PVPMap.CameraPara();
  private const int HOW_MUCH_TO_EXPAND_TO_ONE_SIDE = 2;
  [Tooltip("All possible tile prefabs goes here for reference")]
  public Transform Resources;
  public Transform Cities;
  public Transform Monsters;
  public Transform Wonders;
  public Transform Terrains;
  public Transform Water;
  public Transform Fortress;
  public Transform Bannar;
  public bool ForceUpdate;
  public Rect Viewport;
  private TileCamera m_TileCamera;
  private GameObject m_Highlight;
  private GameObject m_CircleNode;
  private TileData m_SelectedTile;
  private Coordinate m_CurrentKXY;
  private DynamicMapData m_MapData;
  public static bool ShowCircle;
  public static Coordinate CircleLocation;
  private PVPMapMode m_PVPMapMode;
  private TeleportHighlight m_TeleportHighlight;
  private AllianceTurretHighlight m_AllianceTurrentHighlight;
  private SingleCastSpellDialog m_SingleCastSpellDialog;
  private SingleCastSpellSelection m_SingleCastSpellSelection;
  private SpellRangeHighlight m_SpellRangeHighlight;
  protected TileData m_PreviousSpellTileData;

  public static string GetAllianceBuildingPrefabByBuildingId(int allianceBuildingId)
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(allianceBuildingId);
    if (buildingStaticInfo != null)
      return PVPMap.GetAllianceBuildingPrefab(buildingStaticInfo.type);
    throw new Exception(string.Format("PVPMap::GetAllianceBuildingPrefabByBuildingId({0}) -> invalid allianceBuildingId", (object) allianceBuildingId));
  }

  public static string GetAllianceBuildingPrefab(int allianceBuildingType)
  {
    string str = "tiles_alliance_fortress";
    PVPMap.AllAllianceBuildingPrefabNames.TryGetValue(allianceBuildingType, out str);
    return str;
  }

  public TileData SelectedTile
  {
    get
    {
      return this.m_SelectedTile;
    }
  }

  public Coordinate CurrentKXY
  {
    get
    {
      return this.m_CurrentKXY;
    }
    set
    {
      this.m_CurrentKXY = value;
    }
  }

  public TeleportMode GetCurrentTeleportMode()
  {
    if ((UnityEngine.Object) this.m_TeleportHighlight != (UnityEngine.Object) null)
      return this.m_TeleportHighlight.GetTeleportMode();
    return TeleportMode.ADVANCE_TELEPORT;
  }

  public void EnterMultipleSpellMode(TempleSkillInfo skillInfo)
  {
    this.SetPVPMapMode(PVPMapMode.MultipleSpell);
    this.m_SingleCastSpellDialog.SetData(skillInfo);
    this.m_SpellRangeHighlight.Setup(this.m_MapData.ConvertTileKXYToWorldCoordinate(this.m_CurrentKXY), skillInfo);
  }

  public void EnterMultipleSpellMode(TempleSkillInfo skillInfo, Coordinate location)
  {
    this.SetPVPMapMode(PVPMapMode.MultipleSpell);
    this.m_SingleCastSpellDialog.SetData(skillInfo);
    this.m_SpellRangeHighlight.Setup(this.m_MapData.ConvertTileKXYToWorldCoordinate(location), skillInfo);
  }

  public void EnterSingleSpellMode(TempleSkillInfo skillInfo)
  {
    this.SetPVPMapMode(PVPMapMode.SingleSpell);
    this.m_SingleCastSpellDialog.SetData(skillInfo);
  }

  public void EnterSingleSpellMode(KingSkillInfo skillInfo)
  {
    this.SetPVPMapMode(PVPMapMode.SingleSpell);
    this.m_SingleCastSpellDialog.SetData(skillInfo);
  }

  public void ResetTeleport()
  {
    this.m_PVPMapMode = PVPMapMode.Teleport;
    this.SetPVPMapMode(PVPMapMode.Normal);
  }

  public void EnterTeleportMode(Coordinate target, TeleportMode teleportMode, int allianceBuildingID, bool keepTargetInCenter = false)
  {
    this.SetPVPMapMode(PVPMapMode.Teleport);
    object data = teleportMode == TeleportMode.PLACE_FORTRESS ? (object) PlayerData.inst.allianceData : (object) PlayerData.inst.playerCityData;
    this.CurrentKXY = target;
    this.GotoLocation(target, false);
    WorldCoordinate worldCoordinate = this.m_MapData.ConvertTileKXYToWorldCoordinate(target);
    this.m_TeleportHighlight.Setup(teleportMode, this.m_MapData, PVPSystem.Instance.Map.Cities, worldCoordinate, allianceBuildingID, data, keepTargetInCenter);
  }

  public void EnterNormalMode()
  {
    this.SetPVPMapMode(PVPMapMode.Normal);
  }

  public void EnterPlaceTurretMode(Coordinate target, int internalID, bool keepTargetInCenter)
  {
    this.SetPVPMapMode(PVPMapMode.Turret);
    this.CurrentKXY = target;
    this.GotoLocation(target, false);
    this.m_AllianceTurrentHighlight.Setup(this.m_MapData.ConvertTileKXYToWorldCoordinate(target), internalID, keepTargetInCenter);
  }

  private void SetPVPMapMode(PVPMapMode pvpMapMode)
  {
    switch (this.m_PVPMapMode)
    {
      case PVPMapMode.Teleport:
        if ((UnityEngine.Object) this.m_TeleportHighlight != (UnityEngine.Object) null && (bool) ((UnityEngine.Object) this.m_TeleportHighlight.gameObject) && this.m_TeleportHighlight.CanDestroy)
          UnityEngine.Object.Destroy((UnityEngine.Object) this.m_TeleportHighlight.gameObject);
        this.m_TileCamera.EnableDragging = true;
        TeleportMode teleportMode = !((UnityEngine.Object) this.m_TeleportHighlight != (UnityEngine.Object) null) ? TeleportMode.ADVANCE_TELEPORT : this.m_TeleportHighlight.GetTeleportMode();
        switch (teleportMode)
        {
          case TeleportMode.ALLIANCE_WAR_ENTER_TELEPORT:
          case TeleportMode.ALLIANCE_WAR_TELEPORT:
          case TeleportMode.ALLIANCE_WAR_BACK_TELEPORT:
            if ((UnityEngine.Object) this.m_TeleportHighlight != (UnityEngine.Object) null && !this.m_TeleportHighlight.CanDestroy)
            {
              UIManager.inst.DisableUI2DTouchAndMouse();
              break;
            }
            if (teleportMode == TeleportMode.ALLIANCE_WAR_ENTER_TELEPORT)
              UIHideHelperManager.Instance.BackToOriginal();
            UIManager.inst.EnableUI2DTouchAndMouse();
            break;
          default:
            UIManager.inst.EnableUI2DTouchAndMouse();
            break;
        }
      case PVPMapMode.Turret:
        if ((bool) ((UnityEngine.Object) this.m_AllianceTurrentHighlight.gameObject))
          UnityEngine.Object.Destroy((UnityEngine.Object) this.m_AllianceTurrentHighlight.gameObject);
        this.m_TileCamera.EnableDragging = true;
        UIManager.inst.EnableUI2DTouchAndMouse();
        break;
      case PVPMapMode.MultipleSpell:
        if ((bool) ((UnityEngine.Object) this.m_SingleCastSpellDialog))
        {
          UnityEngine.Object.Destroy((UnityEngine.Object) this.m_SingleCastSpellDialog.gameObject);
          this.m_SingleCastSpellDialog = (SingleCastSpellDialog) null;
        }
        if ((bool) ((UnityEngine.Object) this.m_SpellRangeHighlight))
        {
          this.m_SpellRangeHighlight.ClearSelectedTiles();
          UnityEngine.Object.Destroy((UnityEngine.Object) this.m_SpellRangeHighlight.gameObject);
          this.m_SpellRangeHighlight = (SpellRangeHighlight) null;
        }
        this.m_TileCamera.EnableDragging = true;
        if ((bool) ((UnityEngine.Object) UIManager.inst.publicHUD))
          UIManager.inst.publicHUD.ExitSelectTargetMode();
        UIManager.inst.ShowTimerHud();
        break;
      case PVPMapMode.SingleSpell:
        if ((bool) ((UnityEngine.Object) this.m_SingleCastSpellDialog))
        {
          UnityEngine.Object.Destroy((UnityEngine.Object) this.m_SingleCastSpellDialog.gameObject);
          this.m_SingleCastSpellDialog = (SingleCastSpellDialog) null;
        }
        if ((bool) ((UnityEngine.Object) this.m_SingleCastSpellSelection))
        {
          UnityEngine.Object.Destroy((UnityEngine.Object) this.m_SingleCastSpellSelection.gameObject);
          this.m_SingleCastSpellSelection = (SingleCastSpellSelection) null;
        }
        if (this.m_PreviousSpellTileData != null)
        {
          this.m_PreviousSpellTileData.CurrentSpellAbleState = TileData.SpellAbleState.SpellAbleNone;
          this.m_PreviousSpellTileData = (TileData) null;
        }
        if ((bool) ((UnityEngine.Object) UIManager.inst.publicHUD))
          UIManager.inst.publicHUD.ExitSelectTargetMode();
        UIManager.inst.ShowTimerHud();
        break;
    }
    this.m_PVPMapMode = pvpMapMode;
    switch (this.m_PVPMapMode)
    {
      case PVPMapMode.Teleport:
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad("Prefab/Kingdom/TELEPORT_HIGHLIGHT", typeof (GameObject)) as GameObject);
        if ((UnityEngine.Object) this.m_TeleportHighlight != (UnityEngine.Object) null)
          UnityEngine.Object.Destroy((UnityEngine.Object) this.m_TeleportHighlight.gameObject);
        this.m_TeleportHighlight = gameObject.GetComponent<TeleportHighlight>();
        this.m_TeleportHighlight.InitCamera(this.m_TileCamera);
        this.m_TeleportHighlight.transform.parent = this.gameObject.transform;
        this.m_TileCamera.EnableDragging = false;
        this.m_TeleportHighlight.StopDragging();
        this.m_TeleportHighlight.OnCancel = new TeleportHighlight.OnCancelCallback(this.OnCancelTeleport);
        this.m_TeleportHighlight.OnConfirm = new TeleportHighlight.OnConfirmCallback(this.OnConfirmTeleport);
        UIManager.inst.DisableUI2DTouchAndMouse();
        this.HideHighlight();
        break;
      case PVPMapMode.Turret:
        this.m_AllianceTurrentHighlight = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad("Prefab/Kingdom/ALLIANCE_TURRET", typeof (GameObject)) as GameObject).GetComponent<AllianceTurretHighlight>();
        this.m_AllianceTurrentHighlight.InitCamera(this.m_TileCamera);
        this.m_AllianceTurrentHighlight.transform.parent = this.gameObject.transform;
        this.m_AllianceTurrentHighlight.transform.localScale = Vector3.one;
        this.m_TileCamera.EnableDragging = false;
        this.m_AllianceTurrentHighlight.StopDragging();
        this.m_AllianceTurrentHighlight.OnCancel = new AllianceTurretHighlight.OnCancelCallback(this.OnCancelTeleport);
        this.m_AllianceTurrentHighlight.OnConfirm = new AllianceTurretHighlight.OnConfirmCallback(this.OnConfirmTeleport);
        UIManager.inst.DisableUI2DTouchAndMouse();
        this.HideHighlight();
        break;
      case PVPMapMode.MultipleSpell:
        this.m_SingleCastSpellDialog = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad("Prefab/Kingdom/SINGLE_CAST_SPELL", typeof (GameObject)) as GameObject).GetComponent<SingleCastSpellDialog>();
        this.m_SingleCastSpellDialog.transform.parent = UIManager.inst.ui2DCamera.transform;
        this.m_SingleCastSpellDialog.transform.localPosition = Vector3.zero;
        this.m_SingleCastSpellDialog.transform.localScale = Vector3.one;
        this.m_SpellRangeHighlight = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad("Prefab/Kingdom/SPELL_RANGE", typeof (GameObject)) as GameObject).GetComponent<SpellRangeHighlight>();
        this.m_SpellRangeHighlight.InitCamera(this.m_TileCamera);
        this.m_SpellRangeHighlight.transform.parent = this.gameObject.transform;
        this.m_SpellRangeHighlight.transform.localScale = Vector3.one;
        this.m_TileCamera.EnableDragging = false;
        this.m_SpellRangeHighlight.StopDragging();
        this.m_SpellRangeHighlight.OnCancel = new SpellRangeHighlight.OnCancelCallback(this.OnCancelTeleport);
        this.m_SpellRangeHighlight.OnConfirm = new SpellRangeHighlight.OnConfirmCallback(this.OnConfirmTeleport);
        if ((bool) ((UnityEngine.Object) UIManager.inst.publicHUD))
          UIManager.inst.publicHUD.EnterSelectTargetMode();
        UIManager.inst.HideTimerHud();
        this.HideHighlight();
        break;
      case PVPMapMode.SingleSpell:
        this.m_SingleCastSpellDialog = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad("Prefab/Kingdom/SINGLE_CAST_SPELL", typeof (GameObject)) as GameObject).GetComponent<SingleCastSpellDialog>();
        this.m_SingleCastSpellDialog.transform.parent = UIManager.inst.ui2DCamera.transform;
        this.m_SingleCastSpellDialog.transform.localPosition = Vector3.zero;
        this.m_SingleCastSpellDialog.transform.localScale = Vector3.one;
        if ((bool) ((UnityEngine.Object) UIManager.inst.publicHUD))
          UIManager.inst.publicHUD.EnterSelectTargetMode();
        UIManager.inst.HideTimerHud();
        this.HideHighlight();
        break;
    }
  }

  private void OnCancelTeleport()
  {
    this.EnterNormalMode();
  }

  private void OnConfirmTeleport(Coordinate target)
  {
    this.EnterNormalMode();
  }

  private void RegisterListeners()
  {
    DBManager.inst.DB_Alliance.onDataChanged += new System.Action<long>(this.OnAllianceDataChanged);
    DBManager.inst.DB_User.onDataChanged += new System.Action<long>(this.OnAllianceDataChanged);
    DBManager.inst.DB_City.onDataChanged += new System.Action<long>(this.OnAllianceDataChanged);
    this.m_MapData.OnEnterKingdomBlock += new System.Action(this.OnEnterKingdomBlock);
  }

  private void UnregisterListeners()
  {
    DBManager.inst.DB_Alliance.onDataChanged -= new System.Action<long>(this.OnAllianceDataChanged);
    DBManager.inst.DB_User.onDataChanged -= new System.Action<long>(this.OnAllianceDataChanged);
    DBManager.inst.DB_City.onDataChanged -= new System.Action<long>(this.OnAllianceDataChanged);
    this.m_MapData.OnEnterKingdomBlock -= new System.Action(this.OnEnterKingdomBlock);
  }

  private void OnEnterKingdomBlock()
  {
    Coordinate blockOrigin = BlockData.CalculateBlockOrigin(this.m_MapData.ConvertPixelPositionToTileKXY(this.m_LastTarget), this.m_MapData);
    if (DBManager.inst == null)
      return;
    DBManager.inst.DB_March.ClearThirdPartMarch(blockOrigin.X, blockOrigin.Y);
  }

  private void OnAllianceDataChanged(long allianceId)
  {
    this.m_MapData.UpdateUI();
  }

  public void Setup(DynamicMapData mapData)
  {
    this.m_MapData = mapData;
    this.SetupCamera();
    this.RegisterListeners();
  }

  public void Shutdown()
  {
    this.ShutdownCamera();
    this.UnregisterListeners();
  }

  private void SetupCamera()
  {
    if (!(bool) ((UnityEngine.Object) UIManager.inst))
      return;
    this.m_TileCamera = UIManager.inst.tileCamera;
    if (!(bool) ((UnityEngine.Object) this.m_TileCamera))
      return;
    float tileMapScale = GameEngine.Instance.tileMapScale;
    this.transform.localScale = new Vector3(tileMapScale, tileMapScale, tileMapScale);
    Rect worldDimension = this.m_MapData.WorldDimension;
    worldDimension.xMin = (float) ((double) worldDimension.xMin * (double) this.m_MapData.PixelsPerKingdom.x - 2.0 * (double) this.m_MapData.PixelsPerTile.x);
    worldDimension.xMax = (float) ((double) worldDimension.xMax * (double) this.m_MapData.PixelsPerKingdom.x + 2.0 * (double) this.m_MapData.PixelsPerTile.x);
    worldDimension.yMin = (float) ((double) worldDimension.yMin * (double) this.m_MapData.PixelsPerKingdom.y - 2.0 * (double) this.m_MapData.PixelsPerTile.y);
    worldDimension.yMax = (float) ((double) worldDimension.yMax * (double) this.m_MapData.PixelsPerKingdom.y + 2.0 * (double) this.m_MapData.PixelsPerTile.y);
    worldDimension.xMin *= tileMapScale;
    worldDimension.xMax *= tileMapScale;
    worldDimension.yMin *= tileMapScale;
    worldDimension.yMax *= tileMapScale;
    this.m_TileCamera.Boundary = worldDimension;
    this.m_TileCamera.MapData = this.m_MapData;
    this.m_TileCamera.SetXYPlane(this.m_XYPlane);
    this.m_TileCamera.OnZoomCallback += new System.Action(this.OnZoom);
    this.m_TileCamera.OnTapCallback += new System.Action<TapGesture>(this.OnTap);
    this.m_TileCamera.OnDragCallback += new System.Action<DragGesture>(this.OnDrag);
    this.m_TileCamera.SetAutoZoom();
  }

  private void ShutdownCamera()
  {
    if (!(bool) ((UnityEngine.Object) this.m_TileCamera))
      return;
    this.m_TileCamera.OnZoomCallback -= new System.Action(this.OnZoom);
    this.m_TileCamera.OnTapCallback -= new System.Action<TapGesture>(this.OnTap);
    this.m_TileCamera.OnDragCallback -= new System.Action<DragGesture>(this.OnDrag);
    this.m_TileCamera.StopAutoZoom();
  }

  private void OnZoom()
  {
    this.Viewport = this.GetClipViewport();
    this.m_MapData.UpdateViewport(this.Viewport);
  }

  private Rect ConvertCameraViewport2MapArea(Rect viewport)
  {
    Rect rect = new Rect();
    float num = 1f / this.transform.localScale.x;
    rect.xMin = viewport.xMin * num;
    rect.xMax = viewport.xMax * num;
    rect.yMin = viewport.yMax * num;
    rect.yMax = viewport.yMin * num;
    return rect;
  }

  private void Update()
  {
    if ((UnityEngine.Object) this.m_TileCamera == (UnityEngine.Object) null || !GameEngine.IsAvailable || GameEngine.IsShuttingDown)
      return;
    if (this.m_MapData.IsKingdomActive(PVPMap.PendingGotoRequest.K))
    {
      this.GotoLocation(PVPMap.PendingGotoRequest, false);
      PVPMap.PendingGotoRequest.K = 0;
      PVPMap.PendingGotoRequest.Y = 0;
      PVPMap.PendingGotoRequest.X = 0;
    }
    Vector3 pixelPosition = this.ConvertScreenPositionToPixelPosition(new Vector2(0.5f * (float) Screen.width, 0.5f * (float) Screen.height));
    UIManager.inst.publicHUD.CityLocationMarker.SetPosition(this.m_MapData, pixelPosition);
    float upateTolerance = this.GetUpateTolerance();
    if ((double) Vector3.Distance(pixelPosition, this.m_LastTarget) > (double) upateTolerance || this.ForceUpdate)
    {
      this.ForceUpdate = false;
      WorldCoordinate worldCoordinate = this.m_MapData.ConvertPixelPositionToWorldCoordinate((Vector2) pixelPosition);
      this.m_MapData.UpdateCenter(worldCoordinate);
      this.Viewport = this.GetClipViewport();
      this.m_MapData.UpdateViewport(this.Viewport);
      Coordinate currentKxy = this.m_CurrentKXY;
      this.m_CurrentKXY = this.m_MapData.ConvertWorldCoordinateToKXY(worldCoordinate);
      UIManager.inst.publicHUD.UpdateKingdomCoordinate(this.m_CurrentKXY, currentKxy);
      this.m_LastTarget = pixelPosition;
    }
    if (PVPSystem.Instance.IsAvailable && this.m_MapData.IsLoaded && PVPMap.ShowCircle)
    {
      PVPMap.ShowCircle = false;
      this.ShowCircleAt(PVPMap.CircleLocation);
    }
    this.m_MapData.CullAndShow(this.transform);
  }

  private float GetUpateTolerance()
  {
    return 0.5f * this.m_MapData.PixelsPerTile.y;
  }

  public Rect GetClipViewport()
  {
    return this.ExpandViewport(this.ConvertCameraViewport2MapArea(this.m_TileCamera.GetViewport()));
  }

  private Rect ExpandViewport(Rect viewport)
  {
    float x = viewport.x;
    float y = viewport.y;
    float width = viewport.width;
    float height = viewport.height;
    float num1 = x - 2f * this.m_MapData.PixelsPerTile.x;
    float num2 = y + 2f * this.m_MapData.PixelsPerTile.y;
    float num3 = width + 4f * this.m_MapData.PixelsPerTile.x;
    float num4 = height - 4f * this.m_MapData.PixelsPerTile.y;
    viewport.x = num1;
    viewport.y = num2;
    viewport.width = num3;
    viewport.height = num4;
    return viewport;
  }

  public void GotoLocation(Coordinate location, bool smoothTranslation)
  {
    if (!MapUtils.CanGotoTarget(location.K))
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    else if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PitMode && !MapUtils.IsPitWorld(location.K))
      UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
      {
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
        PVPMap.PendingGotoRequest = location;
        if (!((UnityEngine.Object) this.m_TileCamera != (UnityEngine.Object) null))
          return;
        this.HideHighlight();
        UIManager.inst.CloseKingdomTouchCircle();
        this.m_TileCamera.SetTargetLookLocation(location, smoothTranslation);
        if (!(bool) ((UnityEngine.Object) this.m_SpellRangeHighlight))
          return;
        this.m_SpellRangeHighlight.SetLocation(this.m_MapData.ConvertTileKXYToWorldCoordinate(location));
      }));
    else if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PVPMode && MapUtils.IsPitWorld(location.K))
    {
      UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
      {
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PitMode;
        PVPMap.PendingGotoRequest = location;
        if (!((UnityEngine.Object) this.m_TileCamera != (UnityEngine.Object) null))
          return;
        this.HideHighlight();
        UIManager.inst.CloseKingdomTouchCircle();
        this.m_TileCamera.SetTargetLookLocation(location, smoothTranslation);
        if (!(bool) ((UnityEngine.Object) this.m_SpellRangeHighlight))
          return;
        this.m_SpellRangeHighlight.SetLocation(this.m_MapData.ConvertTileKXYToWorldCoordinate(location));
      }));
    }
    else
    {
      if (!((UnityEngine.Object) this.m_TileCamera != (UnityEngine.Object) null))
        return;
      this.HideHighlight();
      UIManager.inst.CloseKingdomTouchCircle();
      this.m_TileCamera.SetTargetLookLocation(location, smoothTranslation);
      NewTutorial.Instance.TriggerEnterWonderEarlyTutorial();
      if (!(bool) ((UnityEngine.Object) this.m_SpellRangeHighlight))
        return;
      this.m_SpellRangeHighlight.SetLocation(this.m_MapData.ConvertTileKXYToWorldCoordinate(location));
    }
  }

  private Coordinate ConvertScreenPointToKXY(Vector2 screenPosition)
  {
    return this.m_MapData.ConvertWorldPositionToTileKXY(this.ConvertScreenPositionToWorldPosition(screenPosition));
  }

  private Vector3 ConvertScreenPositionToWorldPosition(Vector2 point)
  {
    Ray ray = this.m_TileCamera.GetComponent<Camera>().ScreenPointToRay((Vector3) point);
    float enter = 0.0f;
    if (this.m_XYPlane.Raycast(ray, out enter))
      return ray.GetPoint(enter);
    return Vector3.zero;
  }

  private Vector3 ConvertViewportPositionToWorldPosition(Vector3 vp)
  {
    Ray ray = this.m_TileCamera.GetComponent<Camera>().ViewportPointToRay(vp);
    float enter = 0.0f;
    if (this.m_XYPlane.Raycast(ray, out enter))
      return ray.GetPoint(enter);
    return Vector3.zero;
  }

  private Vector3 ConvertScreenPositionToPixelPosition(Vector2 screenPosition)
  {
    return 1f / GameEngine.Instance.tileMapScale * this.ConvertScreenPositionToWorldPosition(screenPosition);
  }

  public void ShowHighlight(Vector3 position, Vector3 scale)
  {
    if ((UnityEngine.Object) this.m_Highlight == (UnityEngine.Object) null)
    {
      this.m_Highlight = MapUtils.Instantiate("Prefab/Kingdom/4_TILE_HIGHLIGHT");
      this.m_Highlight.transform.parent = this.gameObject.transform;
      this.m_Highlight.transform.rotation = Quaternion.identity;
      this.m_Highlight.transform.localScale = Vector3.one;
    }
    this.m_Highlight.transform.parent = this.gameObject.transform;
    this.m_Highlight.transform.localPosition = position;
    this.m_Highlight.transform.rotation = Quaternion.identity;
    this.m_Highlight.transform.localScale = scale;
    this.m_Highlight.SetActive(true);
  }

  private void ShowHighlight(TileData tile)
  {
    int outSizeX = 1;
    int outSizeY = 1;
    this.ShowHighlight(!tile.GetMultiTileSize(out outSizeX, out outSizeY) || tile.IsCenter ? new Vector3(tile.Position.x, tile.Position.y, tile.Position.z - 150f) : new Vector3(tile.Position.x, tile.Position.y - (float) ((double) (outSizeY - 1) * (double) this.m_MapData.PixelsPerTile.y * 0.5), tile.Position.z - 150f), new Vector3((float) outSizeX, (float) outSizeY, 1f));
  }

  public void HideHighlight()
  {
    if (!(bool) ((UnityEngine.Object) this.m_Highlight))
      return;
    this.m_Highlight.SetActive(false);
  }

  private void OnDrag(DragGesture gesture)
  {
    if (this.m_PVPMapMode == PVPMapMode.Teleport)
    {
      if (gesture.Phase == ContinuousGesturePhase.Started)
        this.m_TeleportHighlight.StartDragging(gesture);
      else if (gesture.Phase == ContinuousGesturePhase.Updated)
        this.m_TeleportHighlight.UpdateDragging(gesture);
      else
        this.m_TeleportHighlight.StopDragging();
    }
    else if (this.m_PVPMapMode == PVPMapMode.Turret)
    {
      if (gesture.Phase == ContinuousGesturePhase.Started)
        this.m_AllianceTurrentHighlight.StartDragging(gesture);
      else if (gesture.Phase == ContinuousGesturePhase.Updated)
        this.m_AllianceTurrentHighlight.UpdateDragging(gesture);
      else
        this.m_AllianceTurrentHighlight.StopDragging();
    }
    else
    {
      if (this.m_PVPMapMode != PVPMapMode.MultipleSpell)
        return;
      if (gesture.Phase == ContinuousGesturePhase.Started)
        this.m_SpellRangeHighlight.StartDragging(gesture);
      else if (gesture.Phase == ContinuousGesturePhase.Updated)
        this.m_SpellRangeHighlight.UpdateDragging(gesture);
      else
        this.m_SpellRangeHighlight.StopDragging();
    }
  }

  public void OnTap(TapGesture inGesture)
  {
    if (this.m_PVPMapMode == PVPMapMode.Normal)
      this.TapInNormalMode(inGesture);
    else if (this.m_PVPMapMode == PVPMapMode.Teleport)
      this.TapInTeleportMode(inGesture);
    else if (this.m_PVPMapMode == PVPMapMode.Turret)
      this.TapInPlaceTurretMode(inGesture);
    else if (this.m_PVPMapMode == PVPMapMode.SingleSpell)
    {
      this.TapInSingleSpellMode(inGesture);
    }
    else
    {
      if (this.m_PVPMapMode != PVPMapMode.MultipleSpell)
        return;
      this.TapInMultipleSpellMode(inGesture);
    }
  }

  private void TapInTeleportMode(TapGesture inGesture)
  {
    if (this.m_TeleportHighlight.IsCollide(this.m_MapData.ConvertWorldPositionToWorldCoordinate(this.m_TileCamera.GetWorldHitPoint((Vector3) inGesture.Position))) || this.m_TeleportHighlight.GetTeleportMode() == TeleportMode.WORLD_TELEPORT)
      return;
    this.EnterNormalMode();
  }

  private void TapInPlaceTurretMode(TapGesture inGesture)
  {
    if (this.m_AllianceTurrentHighlight.IsCollide(this.m_MapData.ConvertWorldPositionToWorldCoordinate(this.m_TileCamera.GetWorldHitPoint((Vector3) inGesture.Position))))
      return;
    this.EnterNormalMode();
  }

  private void TapInSingleSpellMode(TapGesture inGesture)
  {
    TempleSkillInfo templeSkill = this.m_SingleCastSpellDialog.GetTempleSkill();
    this.TapInTempleSkill(inGesture, templeSkill);
    KingSkillInfo kingSkill = this.m_SingleCastSpellDialog.GetKingSkill();
    this.TapInKingSkill(inGesture, kingSkill);
  }

  private void TapInTempleSkill(TapGesture inGesture, TempleSkillInfo skillInfo)
  {
    if (skillInfo == null)
      return;
    TileData referenceAt = this.m_MapData.GetReferenceAt(this.ConvertScreenPointToKXY(inGesture.Position));
    if (referenceAt == null || !referenceAt.IsValid() || skillInfo == null)
      return;
    if (this.m_PreviousSpellTileData != null)
    {
      this.m_PreviousSpellTileData.CurrentSpellAbleState = TileData.SpellAbleState.SpellAbleNone;
      this.m_PreviousSpellTileData = (TileData) null;
    }
    if ((bool) ((UnityEngine.Object) this.m_SingleCastSpellSelection))
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_SingleCastSpellSelection.gameObject);
      this.m_SingleCastSpellSelection = (SingleCastSpellSelection) null;
    }
    if (!skillInfo.MeetCondition(referenceAt))
      return;
    this.m_SingleCastSpellSelection = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad("Prefab/Kingdom/SINGLE_CAST_SPELL_SELECTION", typeof (GameObject)) as GameObject).GetComponent<SingleCastSpellSelection>();
    this.m_SingleCastSpellSelection.transform.parent = PVPSystem.Instance.Map.transform;
    this.m_SingleCastSpellSelection.transform.localPosition = referenceAt.Position;
    this.m_SingleCastSpellSelection.transform.localScale = Vector3.one;
    this.m_SingleCastSpellSelection.SetData(skillInfo, referenceAt);
    string empty = string.Empty;
    bool targetTile = Utils.CanSpellToTargetTile(skillInfo, referenceAt, ref empty);
    referenceAt.CurrentSpellAbleState = !targetTile ? TileData.SpellAbleState.SpellAbleFalse : TileData.SpellAbleState.SpellAbleTrue;
    this.m_PreviousSpellTileData = referenceAt;
  }

  private void TapInKingSkill(TapGesture inGesture, KingSkillInfo skillInfo)
  {
    if (skillInfo == null)
      return;
    TileData referenceAt = this.m_MapData.GetReferenceAt(this.ConvertScreenPointToKXY(inGesture.Position));
    if (referenceAt == null || !referenceAt.IsValid() || skillInfo == null)
      return;
    if (this.m_PreviousSpellTileData != null)
    {
      this.m_PreviousSpellTileData.CurrentSpellAbleState = TileData.SpellAbleState.SpellAbleNone;
      this.m_PreviousSpellTileData = (TileData) null;
    }
    if ((bool) ((UnityEngine.Object) this.m_SingleCastSpellSelection))
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_SingleCastSpellSelection.gameObject);
      this.m_SingleCastSpellSelection = (SingleCastSpellSelection) null;
    }
    if (!skillInfo.MeetCondition(referenceAt))
      return;
    this.m_SingleCastSpellSelection = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad("Prefab/Kingdom/SINGLE_CAST_SPELL_SELECTION", typeof (GameObject)) as GameObject).GetComponent<SingleCastSpellSelection>();
    this.m_SingleCastSpellSelection.transform.parent = PVPSystem.Instance.Map.transform;
    this.m_SingleCastSpellSelection.transform.localPosition = referenceAt.Position;
    this.m_SingleCastSpellSelection.transform.localScale = Vector3.one;
    this.m_SingleCastSpellSelection.SetData(skillInfo, referenceAt);
    string empty = string.Empty;
    bool targetTile = Utils.CanSpellToTargetTile(skillInfo, referenceAt, ref empty);
    referenceAt.CurrentSpellAbleState = !targetTile ? TileData.SpellAbleState.SpellAbleFalse : TileData.SpellAbleState.SpellAbleTrue;
    this.m_PreviousSpellTileData = referenceAt;
  }

  private void TapInMultipleSpellMode(TapGesture inGesture)
  {
  }

  private void TapInNormalMode(TapGesture inGesture)
  {
    if ((UnityEngine.Object) this.m_TileCamera == (UnityEngine.Object) null || (UnityEngine.Object) this.m_TileCamera.GetComponent<Camera>() == (UnityEngine.Object) null || (UnityEngine.Object) this.m_TeleportHighlight != (UnityEngine.Object) null && !this.m_TeleportHighlight.CanDestroy && (this.m_TeleportHighlight.GetTeleportMode() == TeleportMode.ALLIANCE_WAR_TELEPORT || this.m_TeleportHighlight.GetTeleportMode() == TeleportMode.ALLIANCE_WAR_BACK_TELEPORT))
      return;
    Ray ray = this.m_TileCamera.GetComponent<Camera>().ScreenPointToRay((Vector3) inGesture.Position);
    RaycastHit hitInfo;
    if (Physics.Raycast(ray, out hitInfo, float.PositiveInfinity, 1 << GameEngine.Instance.PVPUILayer))
    {
      UIEventTrigger component = hitInfo.collider.gameObject.GetComponent<UIEventTrigger>();
      if (!((UnityEngine.Object) null != (UnityEngine.Object) component) || component.onClick == null || !((UnityEngine.Object) UIEventTrigger.current == (UnityEngine.Object) null))
        return;
      UIEventTrigger.current = component;
      EventDelegate.Execute(component.onClick);
      UIEventTrigger.current = (UIEventTrigger) null;
    }
    else if (!SettingManager.Instance.IsPrioritySelectionOff && this.IsSpecialBuilding((Vector3) inGesture.Position))
      this.ShowCircleAt((Vector3) inGesture.Position);
    else if (Physics.Raycast(ray, out hitInfo, float.PositiveInfinity, 1 << GameEngine.Instance.ActorLayer))
    {
      UIEventTrigger component = hitInfo.collider.gameObject.GetComponent<UIEventTrigger>();
      if ((UnityEngine.Object) null != (UnityEngine.Object) component && component.onClick != null && (UnityEngine.Object) UIEventTrigger.current == (UnityEngine.Object) null)
      {
        UIEventTrigger.current = component;
        EventDelegate.Execute(component.onClick);
        UIEventTrigger.current = (UIEventTrigger) null;
      }
      PVEMonsterTile componentInParent = hitInfo.collider.gameObject.GetComponentInParent<PVEMonsterTile>();
      if (!((UnityEngine.Object) componentInParent != (UnityEngine.Object) null))
        return;
      this.ShowCircleAt(this.m_MapData.ConvertPixelPositionToTileKXY(componentInParent.transform.localPosition));
    }
    else
      this.ShowCircleAt((Vector3) inGesture.Position);
  }

  private bool IsSpecialBuilding(Vector3 screenPosition)
  {
    TileData referenceAt = this.m_MapData.GetReferenceAt(this.ConvertScreenPointToKXY((Vector2) screenPosition));
    return referenceAt != null && (referenceAt.TileType == TileType.City || referenceAt.TileType == TileType.Wonder || referenceAt.TileType == TileType.WonderTower);
  }

  public void ShowCircleAt(Vector3 screenPosition)
  {
    this.ShowCircleAt(this.ConvertScreenPointToKXY((Vector2) screenPosition));
  }

  private static bool CanYouInviteTeleport()
  {
    int num = 0;
    AllianceData allianceData = PlayerData.inst.allianceData;
    if (allianceData != null)
    {
      AllianceMemberData allianceMemberData = allianceData.members.Get(PlayerData.inst.uid);
      if (allianceMemberData != null)
        num = allianceMemberData.title;
    }
    if (num != 6)
      return num == 3;
    return true;
  }

  private static bool CanBeginnerTeleport
  {
    get
    {
      return PlayerData.inst.playerCityData.level <= 5;
    }
  }

  private static bool HasBeginnnerTeleport
  {
    get
    {
      return ItemBag.Instance.GetItemCount("item_beginnertele") > 0;
    }
  }

  public static bool InHeitudi(Coordinate location)
  {
    return Mathf.Abs(location.X - 640) + Mathf.Abs(location.Y - 1278) <= 64;
  }

  public bool IsForeigner()
  {
    return PlayerData.inst.userData.world_id != PlayerData.inst.userData.current_world_id;
  }

  public void ShowCircleAt(Coordinate location)
  {
    TileData tile = this.m_MapData.GetReferenceAt(location);
    if (tile == null || tile.Teleporting)
      return;
    if (KingdomTouchCircle.Instance.gameObject.activeInHierarchy)
    {
      this.HideHighlight();
      UIManager.inst.CloseKingdomTouchCircle();
    }
    else
    {
      if (!tile.IsValid())
        return;
      this.m_SelectedTile = tile;
      this.ShowHighlight(tile);
      if ((UnityEngine.Object) null == (UnityEngine.Object) this.m_CircleNode)
      {
        this.m_CircleNode = new GameObject("KingdomCircleMenu");
        this.m_CircleNode.transform.parent = this.gameObject.transform;
      }
      this.m_CircleNode.transform.localPosition = this.m_Highlight.transform.localPosition;
      this.m_CircleNode.transform.localScale = Vector3.one;
      KingdomCirclePara para1 = KingdomTouchCircle.Instance.Para;
      para1.mode = CircleMode.Tile;
      para1.tData = tile;
      Dictionary<string, CityCircleBtnPara> circleBtnPair = KingdomCircleBtnCollection.Instance.CircleBtnPair;
      this.PlaySound(tile.TileType);
      if (tile.TileType >= TileType.Count)
      {
        UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
        {
          Title = ScriptLocalization.Get("id_uppercase_confirm", true),
          Content = ScriptLocalization.Get("old_version_cannot_do_this_description", true),
          Okay = ScriptLocalization.Get("id_uppercase_okay", true),
          OkayCallback = (System.Action) null
        });
      }
      else
      {
        switch (tile.TileType)
        {
          case TileType.Terrain:
            AudioManager.Instance.StopAndPlaySound("sfx_kingdom_map_click_space");
            if (!MapUtils.IsPitWorld(PlayerData.inst.playerCityData.cityLocation.K) || PlayerData.inst.playerCityData.cityLocation.K == location.K)
            {
              if (location.K == PlayerData.inst.userData.world_id)
              {
                AllianceData allianceData = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
                if (allianceData != null && (allianceData.creatorId == PlayerData.inst.uid || Utils.IsR4Member()))
                {
                  CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_alliance_fort", "id_place", new EventDelegate((EventDelegate.Callback) (() =>
                  {
                    Hashtable postData = new Hashtable();
                    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
                    MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
                    {
                      if (!ret)
                        return;
                      UIManager.inst.OpenDlg("Alliance/AllianceCityDlg", (UI.Dialog.DialogParameter) new AllianceCityDlg.Parameter()
                      {
                        targetLocation = tile.Location
                      }, true, true, true);
                    }), true);
                  })), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
                  para1.tList.Add(cityCircleBtnPara);
                }
                if (location.K == PlayerData.inst.playerCityData.cityLocation.K)
                  para1.tList.Add(circleBtnPair["teleport"]);
                else
                  para1.tList.Add(circleBtnPair["crossbackteleport"]);
                if (PVPMap.CanYouInviteTeleport())
                  para1.tList.Add(circleBtnPair["inviteTeleport"]);
                if (location.K == PlayerData.inst.playerCityData.cityLocation.K)
                {
                  para1.tList.Add(circleBtnPair["occupyTile"]);
                  break;
                }
                break;
              }
              if (location.K != PlayerData.inst.playerCityData.cityLocation.K)
              {
                if (ActivityManager.Intance.CanInvade(location.K))
                  para1.tList.Add(circleBtnPair["crossteleport"]);
                if (!PVPMap.CanBeginnerTeleport)
                {
                  para1.tList.Add(circleBtnPair["teleportOtherKingdom"]);
                  break;
                }
                if (!PVPMap.HasBeginnnerTeleport)
                {
                  para1.tList.Add(circleBtnPair["noBeginnerTeleport"]);
                  break;
                }
                para1.tList.Add(circleBtnPair["joinKingdom"]);
                break;
              }
              para1.tList.Add(circleBtnPair["teleport"]);
              break;
            }
            break;
          case TileType.City:
            AudioManager.Instance.StopAndPlaySound("sfx_kingdom_map_click_city");
            if (tile.OwnerID == PlayerData.inst.uid)
            {
              para1.tList.Add(circleBtnPair["ownerProfile"]);
              para1.tList.Add(circleBtnPair["ownerBoost"]);
              para1.tList.Add(circleBtnPair["enter_city"]);
            }
            else
            {
              CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_profile", "kingdom_city_player_profile", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
              {
                uid = tile.OwnerID
              }, 1 != 0, 1 != 0, 1 != 0))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
              para1.tList.Add(cityCircleBtnPara1);
              if (tile.Location.K == PlayerData.inst.playerCityData.cityLocation.K)
              {
                if (tile.AllianceID > 0L && tile.AllianceID == PlayerData.inst.allianceId)
                {
                  if (AllianceWarPayload.Instance.IsPlayerInSameKingdom(tile.OwnerID, PlayerData.inst.uid))
                  {
                    if (!Utils.IsPlayerReinforcedBySelf(tile.OwnerID))
                    {
                      para1.tList.Add(circleBtnPair["allyReinforce"]);
                    }
                    else
                    {
                      CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_troop_details", "id_reinforcement_details", new EventDelegate((EventDelegate.Callback) (() => MessageHub.inst.GetPortByAction("PVP:getReinforceList").SendRequest(new Hashtable()
                      {
                        {
                          (object) "uid",
                          (object) PlayerData.inst.uid
                        },
                        {
                          (object) "opp_uid",
                          (object) tile.OwnerID
                        }
                      }, (System.Action<bool, object>) ((ret, orgData) =>
                      {
                        if (!ret)
                          return;
                        Hashtable hashtable1 = orgData as Hashtable;
                        long result1 = 0;
                        long result2 = 0;
                        if (hashtable1.ContainsKey((object) "capacity_info"))
                        {
                          Hashtable hashtable2 = hashtable1[(object) "capacity_info"] as Hashtable;
                          if (hashtable2 != null)
                          {
                            if (hashtable2.ContainsKey((object) "troops_amount"))
                              long.TryParse(hashtable2[(object) "troops_amount"].ToString(), out result1);
                            if (hashtable2.ContainsKey((object) "capacity"))
                              long.TryParse(hashtable2[(object) "capacity"].ToString(), out result2);
                          }
                        }
                        UIManager.inst.OpenPopup("Wonder/KingdomWarMarchPopup", (Popup.PopupParameter) new KingdomWarMarchPopup.Parameter()
                        {
                          titleKey = "id_uppercase_reinforcement_details",
                          tileData = tile,
                          troopCapacity = result2,
                          troopCount = result1
                        });
                      }), true))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
                      para1.tList.Add(cityCircleBtnPara2);
                    }
                  }
                  para1.tList.Add(circleBtnPair["allyTradeKingdom"]);
                }
                else
                {
                  if (tile.Location.K == PlayerData.inst.userData.world_id || PVPMap.InHeitudi(tile.Location) || (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PitMode || AllianceWarPayload.Instance.CanAttackTargetTile(tile)))
                  {
                    para1.tList.Add(circleBtnPair["enemyScout"]);
                    int num1 = 0;
                    CityData cityData = DBManager.inst.DB_City.Get(tile.CityID);
                    if (cityData != null)
                      num1 = cityData.buildings.level_stronghold;
                    int num2 = (int) ConfigManager.inst.DB_GameConfig.GetData("no_alliance_be_rally_target_min_level").Value;
                    if (tile.AllianceID != 0L || num2 <= num1)
                      para1.tList.Add(circleBtnPair["enemyRally"]);
                    para1.tList.Add(circleBtnPair["enemyAttack"]);
                  }
                  if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PVPMode && tile.UserData != null && (tile.UserData.IsForeigner && tile.Location.K == PlayerData.inst.userData.world_id) && (tile.UserData.AcGroupId > 0 && tile.UserData.AcGroupId != PlayerData.inst.userData.AcGroupId))
                  {
                    para1.tList.Remove(circleBtnPair["enemyScout"]);
                    para1.tList.Remove(circleBtnPair["enemyRally"]);
                    para1.tList.Remove(circleBtnPair["enemyAttack"]);
                  }
                }
              }
            }
            if (this.IsLocalKing(tile))
            {
              CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_officical_position", "id_titles", new EventDelegate((EventDelegate.Callback) (() => Utils.ShowKingdomTitleDetail(tile.Location.K, tile.OwnerID))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
              para1.tList.Add(cityCircleBtnPara1);
              CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_allot_res", "id_allocate", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("Wonder/KingAllotResDlg", (UI.Dialog.DialogParameter) new KingdomAllotResDlg.Parameter()
              {
                playerId = tile.OwnerID
              }, 1 != 0, 1 != 0, 1 != 0))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
              para1.tList.Add(cityCircleBtnPara2);
              break;
            }
            break;
          case TileType.Camp:
            if (tile.OwnerID == PlayerData.inst.uid)
            {
              para1.tList.Add(circleBtnPair["encampInfo"]);
              para1.tList.Add(circleBtnPair["encampRecall"]);
              break;
            }
            CityCircleBtnPara cityCircleBtnPara3 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_profile", "kingdom_city_player_profile", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
            {
              uid = tile.OwnerID
            }, 1 != 0, 1 != 0, 1 != 0))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
            para1.tList.Add(cityCircleBtnPara3);
            if ((tile.AllianceID <= 0L || tile.AllianceID != PlayerData.inst.allianceId) && (tile.Location.K == PlayerData.inst.userData.world_id || PVPMap.InHeitudi(tile.Location) || AllianceWarPayload.Instance.CanAttackTargetTile(tile)))
            {
              para1.tList.Add(circleBtnPair["enemyEncampScout"]);
              para1.tList.Add(circleBtnPair["enemyEncampAttack"]);
              break;
            }
            break;
          case TileType.Resource1:
          case TileType.Resource2:
          case TileType.Resource3:
          case TileType.Resource4:
          case TileType.PitMine:
            if (tile.TileType != TileType.PitMine && this.IsForeigner() && !PVPMap.InHeitudi(location))
            {
              para1.tList.Add(circleBtnPair["resTileInfo"]);
              if (AllianceWarPayload.Instance.CanAttackTargetTile(tile))
              {
                para1.tList.Add(circleBtnPair["enemyScout"]);
                para1.tList.Add(circleBtnPair["enemyAttack"]);
                break;
              }
              break;
            }
            int mLevel = CityManager.inst.mStronghold.mLevel;
            int num3 = 0;
            if (tile.TileType == TileType.Resource1)
              num3 = (int) ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_food").Value;
            else if (tile.TileType == TileType.Resource2)
              num3 = (int) ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_wood").Value;
            else if (tile.TileType == TileType.Resource3)
              num3 = (int) ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_ore").Value;
            else if (tile.TileType == TileType.Resource4)
              num3 = (int) ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_silver").Value;
            if (mLevel < num3)
            {
              Dictionary<string, string> para2 = new Dictionary<string, string>();
              para2["1"] = num3.ToString();
              UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_resource_not_available_gather_description", para2, true), (System.Action) null, 4f, true);
              break;
            }
            if (tile.Location.K != PlayerData.inst.playerCityData.cityLocation.K)
            {
              if (tile.TileType != TileType.PitMine)
              {
                para1.tList.Add(circleBtnPair["resTileInfo"]);
                break;
              }
              break;
            }
            if (tile.OwnerID == PlayerData.inst.uid)
            {
              para1.tList.Add(circleBtnPair["resTileInfo"]);
              para1.tList.Add(circleBtnPair["troopcallback"]);
              break;
            }
            if (tile.OwnerID == 0L)
            {
              if (tile.TileType != TileType.PitMine)
                para1.tList.Add(circleBtnPair["resTileInfo"]);
              para1.tList.Add(circleBtnPair["gatherresource"]);
              break;
            }
            if (tile.AllianceID > 0L && tile.AllianceID == PlayerData.inst.allianceId)
            {
              CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_profile", "kingdom_city_player_profile", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
              {
                uid = tile.OwnerID
              }, 1 != 0, 1 != 0, 1 != 0))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
              para1.tList.Add(cityCircleBtnPara1);
              if (tile.TileType != TileType.PitMine)
              {
                para1.tList.Add(circleBtnPair["resTileInfo"]);
                break;
              }
              break;
            }
            CityCircleBtnPara cityCircleBtnPara4 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_profile", "kingdom_city_player_profile", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
            {
              uid = tile.OwnerID
            }, 1 != 0, 1 != 0, 1 != 0))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
            para1.tList.Add(cityCircleBtnPara4);
            para1.tList.Add(circleBtnPair["enemyScout"]);
            if (tile.TileType != TileType.PitMine)
              para1.tList.Add(circleBtnPair["resTileInfo"]);
            para1.tList.Add(circleBtnPair["enemyAttack"]);
            break;
          case TileType.Encounter:
            if (tile.ResourceId > 0)
            {
              WorldEncounterData data = ConfigManager.inst.DB_WorldEncount.GetData((long) tile.ResourceId);
              if (data != null)
              {
                string type = data.type;
                if (type != null)
                {
                  // ISSUE: reference to a compiler-generated field
                  if (PVPMap.\u003C\u003Ef__switch\u0024map9C == null)
                  {
                    // ISSUE: reference to a compiler-generated field
                    PVPMap.\u003C\u003Ef__switch\u0024map9C = new Dictionary<string, int>(6)
                    {
                      {
                        "cyclops",
                        0
                      },
                      {
                        "dragon",
                        1
                      },
                      {
                        "goblin",
                        2
                      },
                      {
                        "lizard",
                        3
                      },
                      {
                        "dog",
                        4
                      },
                      {
                        "wolf",
                        5
                      }
                    };
                  }
                  int num1;
                  // ISSUE: reference to a compiler-generated field
                  if (PVPMap.\u003C\u003Ef__switch\u0024map9C.TryGetValue(type, out num1))
                  {
                    switch (num1)
                    {
                      case 0:
                        AudioManager.Instance.PlaySound("sfx_monster_click_cyclops", false);
                        break;
                      case 1:
                        AudioManager.Instance.PlaySound("sfx_monster_click_dragon", false);
                        break;
                      case 2:
                        AudioManager.Instance.PlaySound("sfx_monster_click_goblin", false);
                        break;
                      case 3:
                        AudioManager.Instance.PlaySound("sfx_monster_click_lizard", false);
                        break;
                      case 4:
                        AudioManager.Instance.PlaySound("sfx_monster_click_dog", false);
                        break;
                      case 5:
                        AudioManager.Instance.PlaySound("sfx_monster_click_wolf", false);
                        break;
                    }
                  }
                }
              }
              UIManager.inst.OpenDlg("PVEMonsterDetailDialog", (UI.Dialog.DialogParameter) new PVEMonsterDetailDialog.Parameter()
              {
                coordinate = tile.Location
              }, 1 != 0, 1 != 0, 1 != 0);
              break;
            }
            break;
          case TileType.Wonder:
            this.GenerateWonderCircleButtons(tile, para1, circleBtnPair);
            break;
          case TileType.WonderTower:
            this.GenerateWonderTowerCircleButtons(tile, para1, circleBtnPair);
            break;
          case TileType.Water:
            if (!MapUtils.IsPitWorld(tile.Location.K))
            {
              AudioManager.Instance.StopAndPlaySound("sfx_kingdom_map_click_water");
              para1.tList.Add(circleBtnPair["teleport2Water"]);
              para1.tList.Add(circleBtnPair["occupyWater"]);
              break;
            }
            break;
          case TileType.Mountain:
            AudioManager.Instance.StopAndPlaySound("sfx_kingdom_map_click_rock");
            para1.tList.Add(circleBtnPair["teleport2Mountain"]);
            para1.tList.Add(circleBtnPair["occupyMountain"]);
            break;
          case TileType.WonderTree:
            if (!MapUtils.IsPitWorld(PlayerData.inst.CityData.Location.K))
            {
              if (location.K == PlayerData.inst.userData.world_id)
              {
                if (location.K == PlayerData.inst.playerCityData.cityLocation.K)
                {
                  para1.tList.Add(circleBtnPair["teleport"]);
                  para1.tList.Add(circleBtnPair["occupyWonder"]);
                }
                else
                  para1.tList.Add(circleBtnPair["crossbackteleport"]);
                if (PVPMap.CanYouInviteTeleport())
                {
                  para1.tList.Add(circleBtnPair["inviteTeleport"]);
                  break;
                }
                break;
              }
              if (location.K != PlayerData.inst.playerCityData.cityLocation.K)
              {
                if (ActivityManager.Intance.CanInvade(location.K))
                  para1.tList.Add(circleBtnPair["crossteleport"]);
                if (!PVPMap.CanBeginnerTeleport)
                {
                  para1.tList.Add(circleBtnPair["teleportOtherKingdom"]);
                  break;
                }
                if (!PVPMap.HasBeginnnerTeleport)
                {
                  para1.tList.Add(circleBtnPair["noBeginnerTeleport"]);
                  break;
                }
                para1.tList.Add(circleBtnPair["joinKingdom"]);
                break;
              }
              para1.tList.Add(circleBtnPair["teleport"]);
              break;
            }
            break;
          case TileType.GveCamp:
            AudioManager.Instance.StopAndPlaySound("sfx_gve_camp_click");
            if (this.IsForeigner() && !PVPMap.InHeitudi(location))
            {
              para1.tList.Add(circleBtnPair["gveCampInfo"]);
              break;
            }
            if (tile.Location.K != PlayerData.inst.playerCityData.cityLocation.K)
            {
              para1.tList.Add(circleBtnPair["gveCampInfo"]);
              break;
            }
            para1.tList.Add(circleBtnPair["gveCampInfo"]);
            para1.tList.Add(circleBtnPair["gveFallenKnight"]);
            para1.tList.Add(circleBtnPair["gveCampAttack"]);
            break;
          case TileType.AllianceFortress:
            AudioManager.Instance.StopAndPlaySound("sfx_kingdom_map_click_alliance_fort");
            this.GenerateAllianceFortressCircleButtons(tile, para1, circleBtnPair);
            break;
          case TileType.EventVisitNPC:
            UIManager.inst.OpenDlg("FestivalVisitDlg", (UI.Dialog.DialogParameter) new FestivalVisitDlg.Parameter()
            {
              coordinate = location
            }, 1 != 0, 1 != 0, 1 != 0);
            break;
          case TileType.EventAllianceBoss:
            return;
          case TileType.AllianceWarehouse:
            AudioManager.Instance.StopAndPlaySound("sfx_city_click_storehouse");
            this.GenerateAllianceWarehouseCircleButtons(tile, para1, circleBtnPair);
            break;
          case TileType.AllianceSuperMine:
            this.GenerateAllianceSuperMineCircleButtons(tile, para1, circleBtnPair);
            break;
          case TileType.AlliancePortal:
            AudioManager.Instance.StopAndPlaySound("sfx_kingdom_map_click_alliance_portal");
            this.GenerateAlliancePortalCircleButtons(tile, para1, circleBtnPair);
            break;
          case TileType.AllianceHospital:
            AudioManager.Instance.StopAndPlaySound("sfx_city_click_hospital");
            this.GenerateHospitalCircleButtons(tile, para1, circleBtnPair);
            break;
          case TileType.AllianceTemple:
            AudioManager.Instance.StopAndPlaySound("sfx_kingdom_map_click_alliance_altar");
            this.GenerateAllianceTempleCircleButtons(tile, para1, circleBtnPair);
            break;
          case TileType.WorldBoss:
            if (AllianceWarPayload.Instance.IsPlayerCurrentJoined(0L) && PlayerData.inst.userData.IsForeigner)
            {
              UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_assigned_opponent"), (System.Action) null, 4f, true);
              break;
            }
            WorldBossData worldBossData = tile.WorldBossData;
            if (worldBossData != null)
            {
              WorldBossStaticInfo data = ConfigManager.inst.DB_WorldBoss.GetData(worldBossData.configId);
              if (data != null)
              {
                string type = data.Type;
                if (type != null)
                {
                  // ISSUE: reference to a compiler-generated field
                  if (PVPMap.\u003C\u003Ef__switch\u0024map9E == null)
                  {
                    // ISSUE: reference to a compiler-generated field
                    PVPMap.\u003C\u003Ef__switch\u0024map9E = new Dictionary<string, int>(2)
                    {
                      {
                        "vampire",
                        0
                      },
                      {
                        "nian",
                        1
                      }
                    };
                  }
                  int num1;
                  // ISSUE: reference to a compiler-generated field
                  if (PVPMap.\u003C\u003Ef__switch\u0024map9E.TryGetValue(type, out num1))
                  {
                    switch (num1)
                    {
                      case 0:
                        AudioManager.Instance.PlaySound("sfx_monster_click_event_vampire", false);
                        break;
                      case 1:
                        AudioManager.Instance.PlaySound("sfx_monster_click_event_cny", false);
                        break;
                    }
                  }
                }
              }
            }
            UIManager.inst.OpenDlg("WorldBossDetaillDlg", (UI.Dialog.DialogParameter) new WorldBossDetaillDlg.Parameter()
            {
              coordinate = tile.Location
            }, true, true, true);
            break;
          case TileType.Digsite:
            if (this.IsForeigner() && !PVPMap.InHeitudi(location))
            {
              para1.tList.Add(circleBtnPair["digsiteInfo"]);
              break;
            }
            if (tile.Location.K != PlayerData.inst.playerCityData.cityLocation.K)
            {
              para1.tList.Add(circleBtnPair["digsiteInfo"]);
              break;
            }
            if (tile.OwnerID == PlayerData.inst.uid)
            {
              para1.tList.Add(circleBtnPair["digsiteInfo"]);
              para1.tList.Add(circleBtnPair["troopcallback"]);
              break;
            }
            if (tile.OwnerID == 0L)
            {
              para1.tList.Add(circleBtnPair["digsiteInfo"]);
              para1.tList.Add(circleBtnPair["dig"]);
              break;
            }
            if (tile.AllianceID > 0L && tile.AllianceID == PlayerData.inst.allianceId)
            {
              CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_profile", "kingdom_city_player_profile", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
              {
                uid = tile.OwnerID
              }, 1 != 0, 1 != 0, 1 != 0))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
              para1.tList.Add(cityCircleBtnPara1);
              para1.tList.Add(circleBtnPair["digsiteInfo"]);
              break;
            }
            CityCircleBtnPara cityCircleBtnPara5 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_profile", "kingdom_city_player_profile", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
            {
              uid = tile.OwnerID
            }, 1 != 0, 1 != 0, 1 != 0))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
            para1.tList.Add(cityCircleBtnPara5);
            para1.tList.Add(circleBtnPair["enemyScout"]);
            para1.tList.Add(circleBtnPair["digsiteInfo"]);
            para1.tList.Add(circleBtnPair["enemyAttack"]);
            break;
          case TileType.KingdomBoss:
            KingdomBossData kingdomBossData = tile.KingdomBossData;
            if (kingdomBossData == null)
              return;
            if (kingdomBossData != null)
            {
              KingdomBossStaticInfo kingdomBossStaticInfo = ConfigManager.inst.DB_KingdomBoss.GetItem(kingdomBossData.configId);
              if (kingdomBossStaticInfo != null)
              {
                string type = kingdomBossStaticInfo.Type;
                if (type != null)
                {
                  // ISSUE: reference to a compiler-generated field
                  if (PVPMap.\u003C\u003Ef__switch\u0024map9D == null)
                  {
                    // ISSUE: reference to a compiler-generated field
                    PVPMap.\u003C\u003Ef__switch\u0024map9D = new Dictionary<string, int>(2)
                    {
                      {
                        "vampire",
                        0
                      },
                      {
                        "nian",
                        1
                      }
                    };
                  }
                  int num1;
                  // ISSUE: reference to a compiler-generated field
                  if (PVPMap.\u003C\u003Ef__switch\u0024map9D.TryGetValue(type, out num1))
                  {
                    switch (num1)
                    {
                      case 0:
                        AudioManager.Instance.PlaySound("sfx_monster_click_event_vampire", false);
                        goto label_149;
                      case 1:
                        AudioManager.Instance.PlaySound("sfx_monster_click_event_cny", false);
                        goto label_149;
                    }
                  }
                }
                AudioManager.Instance.PlaySound("sfx_monster_click_boss_golem", false);
              }
            }
label_149:
            UIManager.inst.OpenDlg("KingdomBoss/KingdomBossDetailDialog", (UI.Dialog.DialogParameter) new KingdomBossDetailDlg.Parameter()
            {
              coordinate = tile.Location
            }, true, true, true);
            break;
          default:
            return;
        }
        UIManager.inst.OpenKingdomTouchCircle(this.m_CircleNode.transform);
      }
    }
  }

  private void GenerateAllianceFortressCircleButtons(TileData tile, KingdomCirclePara para, Dictionary<string, CityCircleBtnPara> pair)
  {
    AllianceFortressData afd = DBManager.inst.DB_AllianceFortress.GetDataByCoordinate(tile.Location);
    if (afd == null)
      return;
    CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_view", "id_view_details", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("Alliance/AllianceFortressDetailDlg", (UI.Dialog.DialogParameter) new AllianceFortressDetailDlg.Parameter()
    {
      cor = tile.Location,
      allianceFrotressData = afd
    }, 1 != 0, 1 != 0, 1 != 0))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    para.tList.Add(cityCircleBtnPara1);
    if (!AllianceWarPayload.Instance.CanAttackTargetTile(tile) && (this.IsForeigner() || tile.Location.K != PlayerData.inst.userData.current_world_id) || tile.Location.K != PlayerData.inst.playerCityData.cityLocation.K)
      return;
    if (afd.allianceId == PlayerData.inst.allianceId)
    {
      AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(afd.ConfigId);
      if (buildingStaticInfo != null && buildingStaticInfo.type == 0)
      {
        CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_info", "id_view_features", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenPopup("Alliance/AllianceFortViewFunction", (Popup.PopupParameter) new AllianceFortressViewPopup.Parameter()
        {
          allianceId = tile.AllianceID
        }))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
        para.tList.Add(cityCircleBtnPara2);
      }
      if (afd.OwnerAllianceID == 0L)
      {
        if (AllianceBuildUtils.IsFullDurability((object) afd))
          para.tList.Add(pair["guardfortress"]);
        else if ("building" == afd.State || "unComplete" == afd.State)
          para.tList.Add(pair["buildfortress"]);
        else if ("repairing" == afd.State || "broken" == afd.State)
        {
          para.tList.Add(pair["repairfortress"]);
        }
        else
        {
          if (!("demolishing" == afd.State))
            return;
          para.tList.Add(pair["enemyRally"]);
          para.tList.Add(pair["enemyAttack"]);
        }
      }
      else if (afd.allianceId == afd.OwnerAllianceID)
      {
        if ("unComplete" == afd.State)
          para.tList.Add(pair["buildfortress"]);
        else if (AllianceBuildUtils.IsFullDurability((object) afd))
        {
          CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_guard", "id_garrison", new EventDelegate((EventDelegate.Callback) (() => PVPSystem.Instance.Controller.ReinforceCurTile())), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara2);
        }
        else if (afd.IsComplete)
          para.tList.Add(pair["repairfortress"]);
        else
          para.tList.Add(pair["buildfortress"]);
        if (!AllianceBuildUtils.HasMyTroop(afd))
          return;
        para.tList.Add(pair["fortresstroopcallback"]);
      }
      else
      {
        para.tList.Add(pair["enemyRally"]);
        para.tList.Add(pair["enemyAttack"]);
      }
    }
    else
    {
      if (!AllianceBuildUtils.HasMyTroop(afd))
      {
        para.tList.Add(pair["enemyScout"]);
        para.tList.Add(pair["enemyRally"]);
      }
      if (PlayerData.inst.allianceId == afd.OwnerAllianceID)
      {
        if (AllianceBuildUtils.HasMyTroop(afd))
          para.tList.Add(pair["fortresstroopcallback"]);
        else
          para.tList.Add(pair["demolishfortress"]);
      }
      else if (AllianceBuildUtils.HasTroop(afd))
        para.tList.Add(pair["enemyAttack"]);
      else
        para.tList.Add(pair["demolishfortress"]);
    }
  }

  private void GenerateAllianceWarehouseCircleButtons(TileData tile, KingdomCirclePara para, Dictionary<string, CityCircleBtnPara> pair)
  {
    AllianceWareHouseData awhd = tile.WareHouseData;
    if (awhd == null)
      return;
    if (awhd.AllianceId == PlayerData.inst.allianceId)
    {
      if (awhd.CurrentState == AllianceWareHouseData.State.Complete)
      {
        bool canfetch = awhd.getResourceDataByUserId(PlayerData.inst.uid).TotalWeight > 0L;
        CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_retrive", "id_retrieve", new EventDelegate((EventDelegate.Callback) (() =>
        {
          if (canfetch)
            UIManager.inst.OpenDlg("Alliance/AllianceStorehouse", (UI.Dialog.DialogParameter) new AllianceWarehouseDlg.Parameter()
            {
              mode = AllianceWarehouseDlg.Mode.Takeback,
              buildingConfigId = awhd.ConfigId
            }, true, true, true);
          else
            UIManager.inst.toast.Show(ScriptLocalization.Get("toast_alliance_storehouse_no_resources", true), (System.Action) null, 4f, true);
        })), canfetch, "toast_alliance_storehouse_no_resources", (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
        para.tList.Add(cityCircleBtnPara1);
        CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_view", "id_view_details", new EventDelegate((EventDelegate.Callback) (() =>
        {
          Hashtable postData = new Hashtable();
          postData[(object) "alliance_id"] = (object) awhd.AllianceId;
          MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
          {
            if (!ret)
              return;
            UIManager.inst.OpenDlg("Alliance/AllianceWarehouseDetailDlg", (UI.Dialog.DialogParameter) new AllianceWarehouseDetailDlg.Parameter()
            {
              warehouesConfigId = awhd.ConfigId
            }, true, true, true);
          }), true);
        })), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
        para.tList.Add(cityCircleBtnPara2);
        if (awhd.Resource.Count > 0)
        {
          CityCircleBtnPara cityCircleBtnPara3 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_building_retract", "id_building_close", new EventDelegate((EventDelegate.Callback) (() =>
          {
            ZoneBorderData hospitalZoneBorder = this.GetWareHouseAndHospitalZoneBorder(awhd.Location);
            if (hospitalZoneBorder != null && !Utils.IsSomeoneAttackingMyFortress(hospitalZoneBorder.FortressID))
            {
              GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_building_recycle_fund");
              UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
              {
                title = Utils.XLAT("alliance_building_relocate_title"),
                content = string.Format(Utils.XLAT("alliance_building_close_storehouse_confirm_description"), (object) data.ValueInt),
                yes = Utils.XLAT("id_uppercase_confirm"),
                no = Utils.XLAT("id_uppercase_cancel"),
                yesCallback = (System.Action) (() =>
                {
                  Hashtable postData = new Hashtable();
                  postData[(object) "building_id"] = (object) tile.WareHouseData.WarehouseId;
                  postData[(object) "tile_type"] = (object) tile.TileType;
                  MessageHub.inst.GetPortByAction("map:removeAllianceBuilding").SendRequest(postData, (System.Action<bool, object>) null, true);
                })
              });
            }
            else
              UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
              {
                Title = Utils.XLAT("alliance_building_cannot_close_title"),
                Content = Utils.XLAT("alliance_building_cannot_close_description"),
                Okay = Utils.XLAT("id_uppercase_confirm")
              });
          })), Utils.IsR5Member(), "alliance_notice_leader_only_description", (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara3);
        }
        bool canstore = true;
        using (List<MarchData>.Enumerator enumerator = DBManager.inst.DB_March.GetMarchListByUserId(PlayerData.inst.uid).GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            if (enumerator.Current.type == MarchData.MarchType.trade_warehouse)
            {
              canstore = false;
              break;
            }
          }
        }
        CityCircleBtnPara cityCircleBtnPara4 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_store_resources", "id_store", new EventDelegate((EventDelegate.Callback) (() =>
        {
          if (!canstore)
            return;
          UIManager.inst.OpenDlg("Alliance/AllianceStorehouse", (UI.Dialog.DialogParameter) new AllianceWarehouseDlg.Parameter()
          {
            mode = AllianceWarehouseDlg.Mode.Store,
            buildingConfigId = awhd.ConfigId
          }, true, true, true);
        })), canstore, "toast_alliance_storehouse_already_marching", (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
        para.tList.Add(cityCircleBtnPara4);
      }
      else if (awhd.CurrentState == AllianceWareHouseData.State.Freeze)
      {
        UIManager.inst.toast.Show(ScriptLocalization.Get("toast_alliance_storehouse_defense_too_low", true), (System.Action) null, 4f, true);
      }
      else
      {
        CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_view", "id_view_details", new EventDelegate((EventDelegate.Callback) (() => AllianceBuildUtils.LoadAllianceBuildingMaxTroopCount(tile.Location, TileType.AllianceWarehouse, (System.Action) (() => UIManager.inst.OpenDlg("Alliance/AllianceBuildingConstructDetailDlg", (UI.Dialog.DialogParameter) new AllianceBuildingConstructDetailDlg.Parameter()
        {
          type = TileType.AllianceWarehouse,
          cor = tile.Location
        }, true, true, true))))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
        para.tList.Add(cityCircleBtnPara);
        para.tList.Add(pair["buildfortress"]);
        if (!awhd.Troops.ContainsKey(PlayerData.inst.uid))
          return;
        para.tList.Add(pair["fortresstroopcallback"]);
      }
    }
    else
    {
      CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_view", "id_view_details", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.ShowConfirmationBox(ScriptLocalization.Get("alliance_storehouse_title", true), ScriptLocalization.Get("alliance_storehouse_function_description", true), ScriptLocalization.Get("id_uppercase_alliance_info", true), string.Empty, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) (() =>
      {
        Hashtable postData = new Hashtable();
        postData[(object) "alliance_id"] = (object) awhd.AllianceId;
        MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          UIManager.inst.OpenDlg("Alliance/AllianceJoinAndApplyDialog", (UI.Dialog.DialogParameter) new AllianceJoinAndApplyDialog.Parameter()
          {
            allianceData = DBManager.inst.DB_Alliance.Get(awhd.AllianceId),
            justView = (PlayerData.inst.allianceId > 0L)
          }, true, true, true);
        }), true);
      }), (System.Action) null, (System.Action) null))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
      para.tList.Add(cityCircleBtnPara);
    }
  }

  private void GenerateAllianceSuperMineCircleButtons(TileData tile, KingdomCirclePara para, Dictionary<string, CityCircleBtnPara> pair)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    PVPMap.\u003CGenerateAllianceSuperMineCircleButtons\u003Ec__AnonStorey2B8 buttonsCAnonStorey2B8_1 = new PVPMap.\u003CGenerateAllianceSuperMineCircleButtons\u003Ec__AnonStorey2B8();
    // ISSUE: reference to a compiler-generated field
    buttonsCAnonStorey2B8_1.tile = tile;
    // ISSUE: reference to a compiler-generated field
    buttonsCAnonStorey2B8_1.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    buttonsCAnonStorey2B8_1.asmd = buttonsCAnonStorey2B8_1.tile.SuperMineData;
    // ISSUE: reference to a compiler-generated field
    if (buttonsCAnonStorey2B8_1.asmd == null)
      return;
    string title = string.Empty;
    // ISSUE: reference to a compiler-generated field
    switch (buttonsCAnonStorey2B8_1.asmd.BuildingType)
    {
      case 3:
        title = Utils.XLAT("alliance_resource_building_food_title");
        AudioManager.Instance.StopAndPlaySound("sfx_rural_click_farm");
        break;
      case 4:
        title = Utils.XLAT("alliance_resource_building_wood_title");
        AudioManager.Instance.StopAndPlaySound("sfx_rural_click_sawmill");
        break;
      case 5:
        title = Utils.XLAT("alliance_resource_building_ore_title");
        AudioManager.Instance.StopAndPlaySound("sfx_rural_click_mine");
        break;
      case 6:
        title = Utils.XLAT("alliance_resource_building_silver_title");
        AudioManager.Instance.StopAndPlaySound("sfx_rural_click_house");
        break;
    }
    // ISSUE: reference to a compiler-generated field
    if (buttonsCAnonStorey2B8_1.asmd.AllianceId == PlayerData.inst.allianceId)
    {
      // ISSUE: reference to a compiler-generated field
      if (buttonsCAnonStorey2B8_1.asmd.CurrentState == AllianceSuperMineData.State.COMPLETE)
      {
        CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_view", "id_view_details", new EventDelegate((EventDelegate.Callback) (() =>
        {
          // ISSUE: variable of a compiler-generated type
          PVPMap.\u003CGenerateAllianceSuperMineCircleButtons\u003Ec__AnonStorey2B8 buttonsCAnonStorey2B8 = buttonsCAnonStorey2B8_1;
          AllianceBuildUtils.LoadAllianceBuildingInfo(tile.Location, TileType.AllianceSuperMine, (System.Action) (() => UIManager.inst.OpenDlg("Alliance/AllianceBuildingGatherDetailDlg", (UI.Dialog.DialogParameter) new AllianceBuildingGatherDetailDlg.Parameter()
          {
            buildingConfigId = asmd.ConfigId,
            cor = tile.Location
          }, true, true, true)));
        })), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
        para.tList.Add(cityCircleBtnPara);
        // ISSUE: reference to a compiler-generated field
        bool flag = buttonsCAnonStorey2B8_1.asmd.Troops.ContainsKey(PlayerData.inst.uid);
        List<MarchData> marchListByUserId = DBManager.inst.DB_March.GetMarchListByUserId(PlayerData.inst.uid);
        if (flag)
        {
          para.tList.Add(pair["fortresstroopcallback"]);
        }
        else
        {
          if (marchListByUserId == null || marchListByUserId.Find((Predicate<MarchData>) (x =>
          {
            if (this.m_MapData.GetReferenceAt(x.targetLocation) != null)
              return this.m_MapData.GetReferenceAt(x.targetLocation).TileType == TileType.AllianceSuperMine;
            return false;
          })) != null)
            return;
          para.tList.Add(pair["gatherSuperMine"]);
        }
      }
      else
      {
        // ISSUE: reference to a compiler-generated field
        if (buttonsCAnonStorey2B8_1.asmd.CurrentState == AllianceSuperMineData.State.FREEZE)
        {
          UIManager.inst.toast.Show(ScriptLocalization.Get("toast_alliance_storehouse_defense_too_low", true), (System.Action) null, 4f, true);
        }
        else
        {
          CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_view", "id_view_details", new EventDelegate((EventDelegate.Callback) (() => AllianceBuildUtils.LoadAllianceBuildingMaxTroopCount(tile.Location, TileType.AllianceSuperMine, (System.Action) (() => UIManager.inst.OpenDlg("Alliance/AllianceBuildingConstructDetailDlg", (UI.Dialog.DialogParameter) new AllianceBuildingConstructDetailDlg.Parameter()
          {
            type = TileType.AllianceSuperMine,
            cor = tile.Location
          }, true, true, true))))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara);
          para.tList.Add(pair["buildfortress"]);
          // ISSUE: reference to a compiler-generated field
          if (!buttonsCAnonStorey2B8_1.asmd.Troops.ContainsKey(PlayerData.inst.uid))
            return;
          para.tList.Add(pair["fortresstroopcallback"]);
        }
      }
    }
    else
    {
      CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_view", "id_view_details", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.ShowConfirmationBox(title, Utils.XLAT("alliance_resource_building_tip"), Utils.XLAT("id_uppercase_alliance_info"), string.Empty, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) (() =>
      {
        Hashtable postData = new Hashtable();
        postData[(object) "alliance_id"] = (object) asmd.AllianceId;
        MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          UIManager.inst.OpenDlg("Alliance/AllianceJoinAndApplyDialog", (UI.Dialog.DialogParameter) new AllianceJoinAndApplyDialog.Parameter()
          {
            allianceData = DBManager.inst.DB_Alliance.Get(asmd.AllianceId),
            justView = (PlayerData.inst.allianceId > 0L)
          }, true, true, true);
        }), true);
      }), (System.Action) null, (System.Action) null))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
      para.tList.Add(cityCircleBtnPara);
    }
  }

  private void GenerateAlliancePortalCircleButtons(TileData tile, KingdomCirclePara para, Dictionary<string, CityCircleBtnPara> pair)
  {
    AlliancePortalData apd = tile.PortalData;
    if (apd == null)
      return;
    if (apd.AllianceId == PlayerData.inst.allianceId)
    {
      this.UpdateAllianceBossData();
      CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_portal_history", "id_portal_history", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenPopup("Alliance/AllianceBossHistoryPopup", (Popup.PopupParameter) null))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
      para.tList.Add(cityCircleBtnPara1);
      CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_description", "kingdom_resource_info", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenPopup("Alliance/AlliancePortalInfoPopup", (Popup.PopupParameter) null))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
      para.tList.Add(cityCircleBtnPara2);
      if (apd.CurrentState == AlliancePortalData.State.COMPLETE)
      {
        AlliancePortalData.BossSummonState bossSummonState = AllianceBuildUtils.GetAlliancePortalBossSummonState();
        if (bossSummonState == AlliancePortalData.BossSummonState.ACTIVATED_TO_RALLY)
        {
          para.tList.Add(pair["enemyAllianceBossRally"]);
        }
        else
        {
          CityCircleBtnPara cityCircleBtnPara3 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_alliance_boss_trial", "id_open", new EventDelegate((EventDelegate.Callback) (() =>
          {
            if (bossSummonState == AlliancePortalData.BossSummonState.CAN_ACTIVATE)
              UIManager.inst.OpenDlg("Alliance/AllianceBossTrialListDlg", (UI.Dialog.DialogParameter) null, true, true, true);
            else if (apd.CurrentEnergy >= (long) ConfigManager.inst.DB_GameConfig.GetData("alliance_boss_donate_energy_max").ValueInt)
              UIManager.inst.OpenDlg("Alliance/AllianceBossTrialListDlg", (UI.Dialog.DialogParameter) null, true, true, true);
            else
              UIManager.inst.OpenPopup("Alliance/AllianceBossDonatePopup", (Popup.PopupParameter) null);
          })), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara3);
        }
      }
      else
      {
        CityCircleBtnPara cityCircleBtnPara3 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_view", "id_view_details", new EventDelegate((EventDelegate.Callback) (() => AllianceBuildUtils.LoadAllianceBuildingMaxTroopCount(tile.Location, TileType.AlliancePortal, (System.Action) (() => UIManager.inst.OpenDlg("Alliance/AllianceBuildingConstructDetailDlg", (UI.Dialog.DialogParameter) new AllianceBuildingConstructDetailDlg.Parameter()
        {
          type = TileType.AlliancePortal,
          cor = tile.Location
        }, true, true, true))))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
        para.tList.Add(cityCircleBtnPara3);
        para.tList.Add(pair["buildfortress"]);
        if (!apd.Troops.ContainsKey(PlayerData.inst.uid))
          return;
        para.tList.Add(pair["fortresstroopcallback"]);
      }
    }
    else
    {
      CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_view", "id_view_features", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.ShowConfirmationBox(Utils.XLAT("alliance_portal_uppercase_name"), Utils.XLAT("alliance_portal_description"), Utils.XLAT("id_uppercase_alliance_info"), string.Empty, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) (() =>
      {
        Hashtable postData = new Hashtable();
        postData[(object) "alliance_id"] = (object) apd.AllianceId;
        MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          UIManager.inst.OpenDlg("Alliance/AllianceJoinAndApplyDialog", (UI.Dialog.DialogParameter) new AllianceJoinAndApplyDialog.Parameter()
          {
            allianceData = DBManager.inst.DB_Alliance.Get(apd.AllianceId),
            justView = (PlayerData.inst.allianceId > 0L)
          }, true, true, true);
        }), true);
      }), (System.Action) null, (System.Action) null))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
      para.tList.Add(cityCircleBtnPara);
    }
  }

  private ZoneBorderData GetWareHouseAndHospitalZoneBorder(Coordinate mainPoint)
  {
    return this.Contains(mainPoint) ?? this.Contains(new Coordinate(mainPoint.K, mainPoint.X - 1, mainPoint.Y + 1)) ?? this.Contains(new Coordinate(mainPoint.K, mainPoint.X + 1, mainPoint.Y + 1)) ?? this.Contains(new Coordinate(mainPoint.K, mainPoint.X, mainPoint.Y + 2)) ?? (ZoneBorderData) null;
  }

  private ZoneBorderData Contains(Coordinate p2)
  {
    List<ZoneBorderData> bordersByAllianceId = DBManager.inst.DB_ZoneBorder.GetZoneBordersByAllianceID(PlayerData.inst.allianceId);
    for (int index = 0; index < bordersByAllianceId.Count; ++index)
    {
      ZoneBorderData zoneBorderData = bordersByAllianceId[index];
      if (zoneBorderData.Contains(p2))
        return zoneBorderData;
    }
    return (ZoneBorderData) null;
  }

  private void GenerateHospitalCircleButtons(TileData tile, KingdomCirclePara para, Dictionary<string, CityCircleBtnPara> pair)
  {
    AllianceHospitalData hospitalData = tile.HospitalData;
    if (hospitalData == null)
      return;
    if (hospitalData.AllianceID == PlayerData.inst.allianceId)
    {
      if (hospitalData.CurrentState == AllianceHospitalData.State.COMPLETE)
      {
        CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_view", "id_view_details", new EventDelegate((EventDelegate.Callback) (() =>
        {
          Hashtable postData = new Hashtable();
          postData[(object) "alliance_id"] = (object) hospitalData.AllianceID;
          MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
          {
            if (!ret)
              return;
            UIManager.inst.OpenDlg("Alliance/AllianceHospitalDetailDialog", (UI.Dialog.DialogParameter) new AllianceHospitalDetailDialog.Parameter()
            {
              tileData = tile,
              hospitalData = hospitalData
            }, true, true, true);
          }), true);
        })), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
        para.tList.Add(cityCircleBtnPara1);
        if (hospitalData.InjuredTroops.Count > 0)
        {
          CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_building_retract", "id_building_close", new EventDelegate((EventDelegate.Callback) (() =>
          {
            ZoneBorderData hospitalZoneBorder = this.GetWareHouseAndHospitalZoneBorder(hospitalData.Location);
            if (hospitalZoneBorder != null && !Utils.IsSomeoneAttackingMyFortress(hospitalZoneBorder.FortressID))
            {
              GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_building_recycle_fund");
              UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
              {
                title = Utils.XLAT("alliance_building_relocate_title"),
                content = string.Format(Utils.XLAT("alliance_building_close_hospital_confirm_description"), (object) data.ValueInt),
                yes = Utils.XLAT("id_uppercase_confirm"),
                no = Utils.XLAT("id_uppercase_cancel"),
                yesCallback = (System.Action) (() =>
                {
                  Hashtable postData = new Hashtable();
                  postData[(object) "building_id"] = (object) tile.HospitalData.BuildingID;
                  postData[(object) "tile_type"] = (object) tile.TileType;
                  MessageHub.inst.GetPortByAction("map:removeAllianceBuilding").SendRequest(postData, (System.Action<bool, object>) null, true);
                })
              });
            }
            else
              UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
              {
                Title = Utils.XLAT("alliance_building_cannot_close_title"),
                Content = Utils.XLAT("alliance_building_cannot_close_description"),
                Okay = Utils.XLAT("id_uppercase_confirm")
              });
          })), Utils.IsR5Member(), "alliance_notice_leader_only_description", (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara2);
        }
        CityCircleBtnPara cityCircleBtnPara3 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_alliance_heal", "id_alliance_heal", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("Alliance/AllianceHospitalHealDlg", (UI.Dialog.DialogParameter) null, true, true, true))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
        para.tList.Add(cityCircleBtnPara3);
      }
      else
      {
        CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_view", "id_view_details", new EventDelegate((EventDelegate.Callback) (() => AllianceBuildUtils.LoadAllianceBuildingMaxTroopCount(tile.Location, TileType.AllianceHospital, (System.Action) (() => UIManager.inst.OpenDlg("Alliance/AllianceBuildingConstructDetailDlg", (UI.Dialog.DialogParameter) new AllianceBuildingConstructDetailDlg.Parameter()
        {
          type = TileType.AllianceHospital,
          cor = tile.Location
        }, true, true, true))))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
        para.tList.Add(cityCircleBtnPara);
        para.tList.Add(pair["buildfortress"]);
        if (!hospitalData.Troops.ContainsKey(PlayerData.inst.uid))
          return;
        para.tList.Add(pair["fortresstroopcallback"]);
      }
    }
    else
    {
      CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_view", "id_view_features", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.ShowConfirmationBox(Utils.XLAT("alliance_hospital_uppercase_name"), Utils.XLAT("alliance_hospital_features_description"), Utils.XLAT("id_uppercase_alliance_info"), string.Empty, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) (() =>
      {
        Hashtable postData = new Hashtable();
        postData[(object) "alliance_id"] = (object) hospitalData.AllianceID;
        MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          UIManager.inst.OpenDlg("Alliance/AllianceJoinAndApplyDialog", (UI.Dialog.DialogParameter) new AllianceJoinAndApplyDialog.Parameter()
          {
            allianceData = DBManager.inst.DB_Alliance.Get(hospitalData.AllianceID),
            justView = (PlayerData.inst.allianceId > 0L)
          }, true, true, true);
        }), true);
      }), (System.Action) null, (System.Action) null))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
      para.tList.Add(cityCircleBtnPara);
    }
  }

  private void OpenHospitalBuildDlg()
  {
  }

  private void GenerateAllianceTempleCircleButtons(TileData tile, KingdomCirclePara para, Dictionary<string, CityCircleBtnPara> pair)
  {
    AllianceTempleData templeData = tile.TempleData;
    if (templeData == null)
      return;
    if (templeData.allianceId == PlayerData.inst.allianceId)
    {
      if ("unComplete" == templeData.State || "building" == templeData.State)
      {
        CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_view", "id_view_details", new EventDelegate((EventDelegate.Callback) (() => AllianceBuildUtils.LoadAllianceBuildingMaxTroopCount(tile.Location, TileType.AllianceTemple, (System.Action) (() => UIManager.inst.OpenDlg("Alliance/AllianceBuildingConstructDetailDlg", (UI.Dialog.DialogParameter) new AllianceBuildingConstructDetailDlg.Parameter()
        {
          type = TileType.AllianceTemple,
          cor = tile.Location
        }, true, true, true))))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
        para.tList.Add(cityCircleBtnPara);
        para.tList.Add(pair["buildfortress"]);
      }
      else
      {
        CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_view", "id_view_details", new EventDelegate((EventDelegate.Callback) (() => AllianceBuildUtils.LoadAllianceBuildingMaxTroopCount(tile.Location, TileType.AllianceTemple, (System.Action) (() => UIManager.inst.OpenDlg("Alliance/AllianceBuildingDefendDetailDlg", (UI.Dialog.DialogParameter) new AllianceBuildingDefendDetailDlg.Parameter()
        {
          type = TileType.AllianceTemple,
          cor = tile.Location
        }, true, true, true))))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
        para.tList.Add(cityCircleBtnPara1);
        if (this.IsForeigner() || tile.Location.K != PlayerData.inst.userData.current_world_id)
          return;
        if ("demolishing" != templeData.State)
        {
          CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_spell", "id_spells", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("Alliance/AllianceTempleSkillDlg", (UI.Dialog.DialogParameter) null, true, true, true))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara2);
        }
        if (AllianceBuildUtils.IsFullDurability((object) templeData))
        {
          CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_guard", "id_garrison", new EventDelegate((EventDelegate.Callback) (() => PVPSystem.Instance.Controller.ReinforceCurTile())), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara2);
          if (DBManager.inst.DB_AllianceMagic.GetCurrentPowerUpSkill() != null)
          {
            CityCircleBtnPara cityCircleBtnPara3 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_spell_charge", "id_spell_charge", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenPopup("Alliance/DragonAltarPowerUpPopup", (Popup.PopupParameter) null))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
            para.tList.Add(cityCircleBtnPara3);
          }
        }
        else if ("building" == templeData.State || "unComplete" == templeData.State)
          para.tList.Add(pair["buildfortress"]);
        else if ("repairing" == templeData.State || "broken" == templeData.State)
          para.tList.Add(pair["repairfortress"]);
        else if ("demolishing" == templeData.State)
        {
          para.tList.Add(pair["enemyRally"]);
          para.tList.Add(pair["enemyAttack"]);
        }
      }
      if (!templeData.Troops.ContainsKey(PlayerData.inst.uid))
        return;
      para.tList.Add(pair["fortresstroopcallback"]);
    }
    else
    {
      CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_fort_view", "id_view_details", new EventDelegate((EventDelegate.Callback) (() => AllianceBuildUtils.LoadAllianceBuildingMaxTroopCount(tile.Location, TileType.AllianceTemple, (System.Action) (() => UIManager.inst.OpenDlg("Alliance/AllianceBuildingConstructDetailDlg", (UI.Dialog.DialogParameter) new AllianceBuildingConstructDetailDlg.Parameter()
      {
        type = TileType.AllianceTemple,
        cor = tile.Location
      }, true, true, true))))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
      para.tList.Add(cityCircleBtnPara);
      if (!AllianceWarPayload.Instance.CanAttackTargetTile(tile) && (this.IsForeigner() || tile.Location.K != PlayerData.inst.userData.current_world_id))
        return;
      if (!templeData.Troops.ContainsKey(PlayerData.inst.uid))
      {
        para.tList.Add(pair["enemyScout"]);
        para.tList.Add(pair["enemyRally"]);
      }
      if (PlayerData.inst.allianceId == templeData.OwnerAllianceID)
      {
        if (templeData.Troops.ContainsKey(PlayerData.inst.uid))
          para.tList.Add(pair["fortresstroopcallback"]);
        else
          para.tList.Add(pair["demolishfortress"]);
      }
      else if (templeData.Troops.ContainsKey(PlayerData.inst.uid))
        para.tList.Add(pair["enemyAttack"]);
      else
        para.tList.Add(pair["demolishfortress"]);
    }
  }

  private void GenerateWonderCircleButtons(TileData tile, KingdomCirclePara para, Dictionary<string, CityCircleBtnPara> pair)
  {
    AudioManager.Instance.PlaySound("sfx_kingdom_map_click_wonder", false);
    DB.WonderData wd = DBManager.inst.DB_Wonder.Get((long) tile.Location.K);
    if (wd == null || (long) NetServerTime.inst.ServerTimestamp > wd.StateChangeTime && wd.State == "unOpen")
      return;
    CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_avalon_state", "id_throne_status", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenPopup("Wonder/KingdomStatePopup", (Popup.PopupParameter) new WonderStatePopup.Parameter()
    {
      worldId = wd.WORLD_ID
    }))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    para.tList.Add(cityCircleBtnPara1);
    if ("fighting" == wd.State)
    {
      if (DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid).cityLocation.K != tile.Location.K)
        return;
      if (PlayerData.inst.allianceId == 0L)
      {
        if (wd.OWNER_ALLIANCE_ID != 0L)
        {
          CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_attack", "kingdom_city_player_attack", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.toast.Show(ScriptLocalization.Get("toast_throne_attack_need_alliance", true), (System.Action) null, 4f, false))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara2);
        }
        else
        {
          CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_attack", "kingdom_tile_occupy", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.toast.Show(ScriptLocalization.Get("toast_throne_miniwonder_need_alliance", true), (System.Action) null, 4f, false))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara2);
        }
      }
      else if (wd.OWNER_ALLIANCE_ID == PlayerData.inst.allianceId || wd.OWNER_ALLIANCE_ID == 0L)
      {
        if (wd.OWNER_ALLIANCE_ID != 0L)
        {
          CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_troop_details", "id_troop_details", new EventDelegate((EventDelegate.Callback) (() => WonderUtils.LoadWonderMaxTroopCount(wd.WORLD_ID, (System.Action<long>) (capacity => UIManager.inst.OpenPopup("Wonder/KingdomWarMarchPopup", (Popup.PopupParameter) new KingdomWarMarchPopup.Parameter()
          {
            tileData = tile,
            troopCapacity = capacity
          }))))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara2);
          CityCircleBtnPara cityCircleBtnPara3 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_rally", "id_appointed_lords", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "RallyCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara3);
          para.tList.Add(pair["allyReinforce"]);
        }
        else
        {
          para.tList.Add(pair["enemyRally"]);
          para.tList.Add(pair["zhanlingWonder"]);
        }
        if (wd.Troops != null && wd.Troops.ContainsKey(PlayerData.inst.uid))
          para.tList.Add(pair["fortresstroopcallback"]);
      }
      else
      {
        para.tList.Add(pair["enemyScout"]);
        para.tList.Add(pair["enemyRally"]);
        para.tList.Add(pair["enemyAttack"]);
      }
    }
    else if ("protected" == wd.State)
    {
      CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_officical_position", "id_titles", new EventDelegate((EventDelegate.Callback) (() => Utils.ShowKingdomTitleDetail(tile.Location.K, 0L))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
      para.tList.Add(cityCircleBtnPara2);
      CityCircleBtnPara cityCircleBtnPara3 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_national_treasury", "id_treasury", new EventDelegate((EventDelegate.Callback) (() => WonderUtils.ShowKingdomTreasury(tile.Location.K))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
      para.tList.Add(cityCircleBtnPara3);
      if (this.IsLocalKing(tile) || this.IsLocalTempKing(tile))
      {
        CityCircleBtnPara cityCircleBtnPara4 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_settings", "id_king_settings", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("Wonder/KingdomSettingDlg", (UI.Dialog.DialogParameter) new WonderSettingDlg.Parameter()
        {
          kingdomId = (long) tile.Location.K
        }, 1 != 0, 1 != 0, 1 != 0))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
        para.tList.Add(cityCircleBtnPara4);
      }
    }
    if (tile.Location.K != PlayerData.inst.userData.world_id)
      return;
    CityCircleBtnPara cityCircleBtnPara5 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_pit_explore_entry", "id_fire_kingdom_event", new EventDelegate((EventDelegate.Callback) (() => PitExplorePayload.Instance.ShowPitActivityScene())), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    para.tList.Add(cityCircleBtnPara5);
  }

  private void GenerateWonderTowerCircleButtons(TileData tile, KingdomCirclePara para, Dictionary<string, CityCircleBtnPara> pair)
  {
    AudioManager.Instance.PlaySound("sfx_kingdom_map_click_miniwonder", false);
    WonderTowerData wtd = tile.WonderTowerData;
    if (wtd == null || (long) NetServerTime.inst.ServerTimestamp > wtd.StateChangeTime && wtd.State == "unOpen")
      return;
    CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_avalon_state", "id_throne_status", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenPopup("Wonder/KingdomStatePopup", (Popup.PopupParameter) new WonderStatePopup.Parameter()
    {
      worldId = wtd.WORLD_ID,
      townerId = wtd.TOWER_ID
    }))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    para.tList.Add(cityCircleBtnPara1);
    if ("fighting" == wtd.State)
    {
      if (DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid).cityLocation.K != tile.Location.K)
        return;
      if (PlayerData.inst.allianceId == 0L)
      {
        if (wtd.OWNER_ALLIANCE_ID != 0L)
        {
          CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_attack", "kingdom_city_player_attack", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.toast.Show(ScriptLocalization.Get("toast_throne_attack_need_alliance", true), (System.Action) null, 4f, false))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara2);
        }
        else
        {
          CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_attack", "kingdom_tile_occupy", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.toast.Show(ScriptLocalization.Get("toast_throne_miniwonder_need_alliance", true), (System.Action) null, 4f, false))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara2);
        }
      }
      else if (wtd.OWNER_ALLIANCE_ID == PlayerData.inst.allianceId || wtd.OWNER_ALLIANCE_ID == 0L)
      {
        if (wtd.OWNER_ALLIANCE_ID != 0L)
        {
          CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_troop_details", "march_troops", new EventDelegate((EventDelegate.Callback) (() => WonderUtils.LoadWonderTowerMaxTroopCount(wtd.WORLD_ID, wtd.TOWER_ID, (System.Action<long>) (capacity => UIManager.inst.OpenPopup("Wonder/KingdomWarMarchPopup", (Popup.PopupParameter) new KingdomWarMarchPopup.Parameter()
          {
            tileData = tile,
            troopCapacity = capacity
          }))))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara2);
          CityCircleBtnPara cityCircleBtnPara3 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_rally", "id_appointed_lords", new EventDelegate((MonoBehaviour) PVPSystem.Instance.Controller, "RallyCurTile"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara3);
          para.tList.Add(pair["allyReinforce"]);
        }
        else
        {
          para.tList.Add(pair["enemyRally"]);
          para.tList.Add(pair["zhanlingWonder"]);
        }
        if (wtd.Troops == null || !wtd.Troops.ContainsKey(PlayerData.inst.uid))
          return;
        para.tList.Add(pair["fortresstroopcallback"]);
      }
      else
      {
        para.tList.Add(pair["enemyScout"]);
        para.tList.Add(pair["enemyRally"]);
        para.tList.Add(pair["enemyAttack"]);
      }
    }
    else
    {
      if (!("protected" == wtd.State))
        return;
      CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_officical_position", "id_titles", new EventDelegate((EventDelegate.Callback) (() => Utils.ShowKingdomTitleDetail(tile.Location.K, 0L))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
      para.tList.Add(cityCircleBtnPara2);
      CityCircleBtnPara cityCircleBtnPara3 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_national_treasury", "id_treasury", new EventDelegate((EventDelegate.Callback) (() => WonderUtils.ShowKingdomTreasury(tile.Location.K))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
      para.tList.Add(cityCircleBtnPara3);
      if (!this.IsLocalKing(tile) && !this.IsLocalTempKing(tile))
        return;
      CityCircleBtnPara cityCircleBtnPara4 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_settings", "id_king_settings", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("Wonder/KingdomSettingDlg", (UI.Dialog.DialogParameter) new WonderSettingDlg.Parameter()
      {
        kingdomId = (long) tile.Location.K
      }, 1 != 0, 1 != 0, 1 != 0))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
      para.tList.Add(cityCircleBtnPara4);
    }
  }

  private bool IsLocalKing(TileData tile)
  {
    if (PlayerData.inst.userData.world_id != tile.Location.K)
      return false;
    return PlayerData.inst.userData.IsKing;
  }

  private bool IsLocalTempKing(TileData tile)
  {
    if (PlayerData.inst.userData.world_id != tile.Location.K)
      return false;
    DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) tile.Location.K);
    if (wonderData.KING_ALLIANCE_ID != 0L && wonderData.KING_UID == 0L && wonderData.KING_ALLIANCE_ID == PlayerData.inst.allianceId)
    {
      long num1 = 0;
      AllianceMemberData highLord = PlayerData.inst.allianceData.GetHighLord();
      if (highLord != null)
        num1 = highLord.uid;
      long num2 = 0;
      AllianceMemberData tempHighLord = PlayerData.inst.allianceData.GetTempHighLord();
      if (tempHighLord != null)
        num2 = tempHighLord.uid;
      if (num1 == PlayerData.inst.uid || num2 == PlayerData.inst.uid)
        return true;
    }
    return false;
  }

  public void OnTakeBackFortress()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "k"] = (object) this.SelectedTile.Location.K;
    postData[(object) "x"] = (object) this.SelectedTile.Location.X;
    postData[(object) "y"] = (object) this.SelectedTile.Location.Y;
    MessageHub.inst.GetPortByAction("Map:takeBackAllianceFortress").SendRequest(postData, (System.Action<bool, object>) null, true);
  }

  private void PlaySound(TileType tileType)
  {
    switch (tileType)
    {
      case TileType.Resource1:
        AudioManager.Instance.PlaySound("sfx_rural_click_farm", false);
        break;
      case TileType.Resource2:
        AudioManager.Instance.PlaySound("sfx_rural_click_sawmill", false);
        break;
      case TileType.Resource3:
        AudioManager.Instance.PlaySound("sfx_rural_click_mine", false);
        break;
      case TileType.Resource4:
        AudioManager.Instance.PlaySound("sfx_rural_click_house", false);
        break;
      case TileType.PitMine:
        AudioManager.Instance.PlaySound("sfx_city_hero_altar", false);
        break;
    }
  }

  public void Goto(Vector3 tilePosition, Vector3 offset)
  {
    UIManager.inst.tileCamera.SetTargetLookPosition(UIManager.inst.tileCamera.GetLookPosition() + tilePosition - offset, true);
  }

  public void FocusTile(Vector3 tilePosition, UIWidget tar, float zoom = 0.0f)
  {
    if (this.m_CameraPara.isreord)
      return;
    Camera component = UIManager.inst.tileCamera.GetComponent<Camera>();
    Vector3 position = component.transform.position;
    Vector3 worldPosition = this.ConvertViewportPositionToWorldPosition(UIManager.inst.ui2DCamera.WorldToViewportPoint(tar.transform.position));
    component.transform.position = position;
    this.m_CameraPara.cwpos = position;
    this.m_CameraPara.isreord = true;
    this.Goto(tilePosition, worldPosition);
  }

  public void BackToOriStats()
  {
    if (UIManager.inst.dialogStackCount != 0 || !this.m_CameraPara.isreord)
      return;
    this.m_CameraPara.isreord = false;
    UIManager.inst.tileCamera.SetTargetLookPosition(this.m_CameraPara.cwpos, true);
  }

  public void OnTrade()
  {
    if (DBManager.inst.DB_March.marchOwner.Count >= PlayerData.inst.playerCityData.marchSlot)
    {
      GameEngine.Instance.marchSystem.ShowMaxMarchSlotMessage();
    }
    else
    {
      int buildingLevelFor = CityManager.inst.GetHighestBuildingLevelFor("marketplace");
      if (buildingLevelFor > 0)
      {
        BuildingInfo data = ConfigManager.inst.DB_Building.GetData("marketplace", buildingLevelFor);
        CityData cityData = DBManager.inst.DB_City.Get(this.m_SelectedTile.CityID);
        AllianceSendResources.Parameter parameter = new AllianceSendResources.Parameter();
        parameter.Uid = this.m_SelectedTile.OwnerID;
        float finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(0.0f, "calc_tradetax");
        parameter.TaxPercent = Math.Round((double) finalData, 2);
        parameter.MaxResourceLoad = (long) ConfigManager.inst.DB_BenefitCalc.GetFinalData((int) data.Benefit_Value_1, "calc_trade_capacity");
        parameter.TargetLocation = cityData.cityLocation;
        UIManager.inst.OpenPopup("AllianceSendTradePopup", (Popup.PopupParameter) parameter);
      }
      else
        UIManager.inst.OpenDlg("NoMarketplaceDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    }
  }

  public void OnShowGveInfo()
  {
    this.HideHighlight();
    UIManager.inst.OpenPopup("InfoKingdom", (Popup.PopupParameter) new InfoKingdom.Parameter()
    {
      initIndex = 5
    });
  }

  public void OnShowFallenKnight()
  {
    this.HideHighlight();
    RequestManager.inst.SendRequest("activity:getIntegralActivityState", (Hashtable) null, (System.Action<bool, object>) ((result, orgSrc) =>
    {
      if (!result)
        return;
      UIManager.inst.OpenDlg("Activity/FallenKnightEntryDlg", (UI.Dialog.DialogParameter) new ActivityFallenKnightDlg.Params()
      {
        internalId = ActivityManager.Intance.knightData.activityId
      }, true, true, true);
    }), true);
  }

  public void OnGveAttack()
  {
    this.HideHighlight();
    Hashtable postData = new Hashtable();
    postData[(object) "k"] = (object) this.m_SelectedTile.Location.K;
    postData[(object) "x"] = (object) this.m_SelectedTile.Location.X;
    postData[(object) "y"] = (object) this.m_SelectedTile.Location.Y;
    MessageHub.inst.GetPortByAction("Map:loadGveCampInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, orgData) =>
    {
      if (ret)
      {
        GveCampData gveCampData = new GveCampData();
        gveCampData.Decode(orgData);
        UIManager.inst.OpenDlg("GvE/GveBossDetailDialog", (UI.Dialog.DialogParameter) new GveBossDetailDialog.Parameter()
        {
          tileData = this.m_SelectedTile,
          gveCampData = gveCampData
        }, true, true, true);
      }
      else
      {
        Hashtable hashtable = orgData as Hashtable;
        if (hashtable == null || !hashtable.ContainsKey((object) "errno"))
          return;
        int result = 0;
        int.TryParse(hashtable[(object) "errno"].ToString(), out result);
        if (result != 1200110)
          return;
        Utils.RefreshBlock(this.m_SelectedTile.Location, (System.Action<bool, object>) null);
      }
    }), true);
  }

  private void UpdateAllianceBossData()
  {
    MessageHub.inst.GetPortByAction("alliance:loadBoss").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || data == null)
        return;
      DBManager.inst.DB_AllianceBoss.UpdateDatas(data as Hashtable, (long) NetServerTime.inst.ServerTimestamp);
    }), true);
  }

  private class CameraPara
  {
    public Vector3 cwpos;
    public float zoom;
    public bool isreord;
  }
}
