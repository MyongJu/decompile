﻿// Decompiled with JetBrains decompiler
// Type: SevenDaysRewardDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class SevenDaysRewardDetail : MonoBehaviour
{
  private GameObjectPool pool = new GameObjectPool();
  private List<SevenDaysRewardItemRenderer> _items = new List<SevenDaysRewardItemRenderer>();
  public UIButton rewardbt;
  public UITable tabel;
  public GameObject statusGo;
  public UILabel label;
  public SevenDaysRewardItemRenderer itemPrefab;
  public GameObject itemRoot;
  public GameObject grayObject;
  private int _day;
  private bool _init;

  public void SetData(int day)
  {
    if (this._day == day)
      return;
    this._day = day;
    this.Clear();
    this.RefreshItems();
    this.UpdateSrtatus();
    this.AddEventHandler();
  }

  private SevenDaysRewardItemRenderer GetItem()
  {
    if (!this._init)
    {
      this._init = true;
      this.pool.Initialize(this.itemPrefab.gameObject, this.itemRoot);
    }
    GameObject go = this.pool.AddChild(this.tabel.gameObject);
    SevenDaysRewardItemRenderer component = go.GetComponent<SevenDaysRewardItemRenderer>();
    NGUITools.SetActive(go, true);
    return component;
  }

  private void RefreshItems()
  {
    SevenDayRewardInfo rewardInfo = ConfigManager.inst.DB_SevenDayRewards.GetRewardInfo(this._day);
    if (rewardInfo.Rewards == null)
      return;
    List<Reward.RewardsValuePair> rewards = rewardInfo.Rewards.GetRewards();
    for (int index = 0; index < rewards.Count; ++index)
    {
      Reward.RewardsValuePair rewardsValuePair = rewards[index];
      if (rewardsValuePair.internalID > 0 && rewardsValuePair.value > 0)
      {
        ConfigManager.inst.DB_Items.GetItem(rewardsValuePair.internalID);
        SevenDaysRewardItemRenderer rewardItemRenderer = this.GetItem();
        rewardItemRenderer.SetData(rewardsValuePair.internalID, rewardsValuePair.value);
        this._items.Add(rewardItemRenderer);
      }
    }
    this.tabel.Reposition();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_SevenDays.onDataChanged += new System.Action<long>(this.OnDataChange);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_SevenDays.onDataChanged -= new System.Action<long>(this.OnDataChange);
  }

  private void OnDataChange(long configId)
  {
    this.UpdateSrtatus();
  }

  private void UpdateSrtatus()
  {
    SevenDaysData sevenDaysData = DBManager.inst.DB_SevenDays.GetSevenDaysData(this._day);
    bool state = false;
    string Term = "event_7_days_sale_not_started";
    Color red = Color.red;
    if (sevenDaysData != null)
    {
      if (sevenDaysData.IsExpired)
      {
        for (int index = 0; index < this._items.Count; ++index)
        {
          if (!sevenDaysData.IsClaim)
            this._items[index].Expired();
          else
            this._items[index].Normal();
        }
        Term = "id_expired";
        state = false;
      }
      else
      {
        for (int index = 0; index < this._items.Count; ++index)
          this._items[index].Normal();
        state = sevenDaysData.IsFinish;
        if (sevenDaysData.IsClaim)
          Term = "id_claimed";
      }
    }
    NGUITools.SetActive(this.rewardbt.gameObject, state);
    NGUITools.SetActive(this.statusGo, !state);
    this.label.color = red;
    this.label.text = ScriptLocalization.Get(Term, true);
  }

  public void OnGetReward()
  {
    RequestManager.inst.SendRequest("sevenDays:welfare", Utils.Hash((object) "uid", (object) PlayerData.inst.uid), (System.Action<bool, object>) null, true);
    List<Reward.RewardsValuePair> rewards = ConfigManager.inst.DB_SevenDayRewards.GetRewardInfo(this._day).Rewards.GetRewards();
    RewardsCollectionAnimator.Instance.Clear();
    for (int index = 0; index < rewards.Count; ++index)
    {
      Reward.RewardsValuePair rewardsValuePair = rewards[index];
      if (rewardsValuePair.internalID > 0 && rewardsValuePair.value > 0)
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardsValuePair.internalID);
        RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
        {
          count = (float) rewardsValuePair.value,
          icon = itemStaticInfo.ImagePath
        });
      }
    }
    RewardsCollectionAnimator.Instance.CollectItems(true);
  }

  private void Clear()
  {
    this.RemoveEventHandler();
    for (int index = 0; index < this._items.Count; ++index)
      this.pool.Release(this._items[index].gameObject);
    this._items.Clear();
  }

  public void Dispose()
  {
    this.Clear();
    this._init = false;
  }
}
