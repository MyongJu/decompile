﻿// Decompiled with JetBrains decompiler
// Type: ConfigBuildingIdle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigBuildingIdle
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, BuildingIdleInfo> databybuildingtype = new Dictionary<string, BuildingIdleInfo>();
  private Dictionary<string, BuildingIdleInfo> datas;
  private Dictionary<long, BuildingIdleInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<BuildingIdleInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    this.databybuildingtype.Clear();
    using (Dictionary<string, BuildingIdleInfo>.Enumerator enumerator = this.datas.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, BuildingIdleInfo> current = enumerator.Current;
        this.databybuildingtype.Add(current.Value.buildingtype, current.Value);
      }
    }
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, BuildingIdleInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<long, BuildingIdleInfo>) null;
  }

  public BuildingIdleInfo GetBuildingIdleInfo(long internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (BuildingIdleInfo) null;
  }

  public BuildingIdleInfo GetBuildingIdleInfo(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (BuildingIdleInfo) null;
  }

  public BuildingIdleInfo GetBuildingIdleInfoByBuildingType(string type)
  {
    if (this.databybuildingtype.ContainsKey(type))
      return this.databybuildingtype[type];
    return (BuildingIdleInfo) null;
  }
}
