﻿// Decompiled with JetBrains decompiler
// Type: CreateRoomStepDlg1
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Text;
using UI;
using UnityEngine;

public class CreateRoomStepDlg1 : MonoBehaviour
{
  private const float MIN_CHECK_TIME = 3f;
  private bool _isCheckCorrect;
  private bool _isPrivate;
  private bool needCheck;
  private float lastCheckTime;
  [SerializeField]
  public CreateRoomStepDlg1.Panel panel;

  public bool isCheckCorrect
  {
    get
    {
      return this._isCheckCorrect;
    }
    set
    {
      this._isCheckCorrect = value;
      if (this._isCheckCorrect)
      {
        this.panel.roomNameCheckCorrect.gameObject.SetActive(true);
        this.panel.roomNameCheckIncorrect.gameObject.SetActive(false);
      }
      else
      {
        this.panel.roomNameCheckCorrect.gameObject.SetActive(false);
        this.panel.roomNameCheckIncorrect.gameObject.SetActive(true);
      }
    }
  }

  public bool isPrivate
  {
    get
    {
      return this._isPrivate;
    }
    set
    {
      this._isPrivate = value;
      if (this._isPrivate)
      {
        this.panel.roomPrivate.gameObject.SetActive(true);
        this.panel.roomPublic.gameObject.SetActive(false);
      }
      else
      {
        this.panel.roomPrivate.gameObject.SetActive(false);
        this.panel.roomPublic.gameObject.SetActive(true);
      }
    }
  }

  public void OnRoomPrivateClick()
  {
    this.isPrivate = true;
  }

  public void OnRoomPublicClick()
  {
    this.isPrivate = false;
  }

  public void OnNextButtonClick()
  {
    this.panel.createDlg2.Show(this.panel.input.value, this.isPrivate);
    this.Hide();
  }

  public void OnCloseButtonClick()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
  }

  public void OnRoomValueChanged()
  {
    this.needCheck = true;
    this.panel.roomNameCheckIncorrect.gameObject.SetActive(false);
    this.panel.roomNameCheckCorrect.gameObject.SetActive(false);
    this.panel.roonNameChecking.gameObject.SetActive(true);
    this.panel.BT_Next.isEnabled = false;
  }

  private void CheckName()
  {
    this.lastCheckTime = Time.time;
    this.needCheck = false;
    this.panel.BT_Next.isEnabled = false;
    this.panel.roomNameCheckIncorrect.gameObject.SetActive(false);
    this.panel.roomNameCheckCorrect.gameObject.SetActive(false);
    this.panel.roonNameChecking.gameObject.SetActive(true);
    byte[] bytes = Encoding.UTF8.GetBytes(this.panel.input.value);
    this.panel.nameLabelCount.text = string.Format("{0}/16", (object) bytes.Length);
    if (bytes.Length < 3 || bytes.Length > 16)
    {
      this.panel.roomNameCheckIncorrect.gameObject.SetActive(true);
      this.panel.roomNameCheckCorrect.gameObject.SetActive(false);
      this.panel.roonNameChecking.gameObject.SetActive(false);
      this.panel.BT_Next.isEnabled = false;
    }
    else
      MessageHub.inst.GetPortByAction("Chat:checkRoomName").SendLoader(Utils.Hash((object) "name", (object) this.panel.input.value), (System.Action<bool, object>) ((result, orgSrc) =>
      {
        if (result && (bool) (orgSrc as Hashtable)[(object) "ret"])
        {
          if (this.needCheck)
          {
            this.CheckName();
          }
          else
          {
            this.panel.roomNameCheckIncorrect.gameObject.SetActive(false);
            this.panel.roomNameCheckCorrect.gameObject.SetActive(true);
            this.panel.roonNameChecking.gameObject.SetActive(false);
            this.panel.BT_Next.isEnabled = true;
          }
        }
        else
        {
          this.panel.roomNameCheckIncorrect.gameObject.SetActive(true);
          this.panel.roomNameCheckCorrect.gameObject.SetActive(false);
          this.panel.roonNameChecking.gameObject.SetActive(false);
          this.panel.BT_Next.isEnabled = false;
        }
      }), true, false);
  }

  public void Update()
  {
    if (!this.needCheck || (double) Time.time - (double) this.lastCheckTime <= 3.0)
      return;
    this.CheckName();
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
    this.panel.input.value = string.Empty;
    this.isPrivate = false;
    this.isCheckCorrect = false;
    this.CheckName();
    this.panel.BT_Next.isEnabled = false;
    this.panel.nameLabelCount.text = string.Format("{0}/16", (object) Encoding.UTF8.GetBytes(this.panel.input.value).Length);
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  [Serializable]
  public class Panel
  {
    public UIInput input;
    public Transform roomNameCheckCorrect;
    public Transform roomNameCheckIncorrect;
    public Transform roonNameChecking;
    public Transform roomPrivate;
    public Transform roomPublic;
    public UIButton BT_Next;
    public UILabel nameLabelCount;
    public CreateRoomStepDlg2 createDlg2;
  }
}
