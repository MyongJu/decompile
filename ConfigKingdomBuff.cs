﻿// Decompiled with JetBrains decompiler
// Type: ConfigKingdomBuff
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigKingdomBuff
{
  private List<KingdomBuffInfo> _kingdomBuffInfoList = new List<KingdomBuffInfo>();
  private Dictionary<string, KingdomBuffInfo> _datas;
  private Dictionary<int, KingdomBuffInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<KingdomBuffInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, KingdomBuffInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._kingdomBuffInfoList.Add(enumerator.Current);
    }
  }

  public KingdomBuffInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (KingdomBuffInfo) null;
  }

  public KingdomBuffInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (KingdomBuffInfo) null;
  }
}
