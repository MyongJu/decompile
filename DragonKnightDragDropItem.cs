﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightDragDropItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonKnightDragDropItem : UIDragDropItemEx
{
  private int _originSlot;
  private int _originSlotTalentId;
  private DragonKnightSkillSettingItemRenderer _settingItem;

  protected override string DragImagePath
  {
    get
    {
      return "Texture/Talent/Icon/";
    }
  }

  protected override bool GenerateNewSurface
  {
    get
    {
      return false;
    }
  }

  protected override void OnDragDropStart()
  {
    this._originSlot = this.GetSkillSlotIndex(this.transform.parent.name);
    this._originSlotTalentId = this.GetSlotSkillId(this._originSlot);
    this._settingItem = this.transform.parent.GetComponent<DragonKnightSkillSettingItemRenderer>();
    if (this._originSlotTalentId <= 0 && (UnityEngine.Object) this._settingItem != (UnityEngine.Object) null && this._settingItem.TalentInfo != null)
      this._originSlotTalentId = this._settingItem.TalentInfo.internalId;
    base.OnDragDropStart();
  }

  protected override void OnDragDropRelease(GameObject surface)
  {
    this.CheckCloneObjects();
    int skillSlotIndex = this.GetSkillSlotIndex(surface.transform.parent.name);
    int slotSkillId = this.GetSlotSkillId(skillSlotIndex);
    if (this.IsAvailabeSlot(skillSlotIndex))
    {
      base.OnDragDropRelease(surface);
      Hashtable postData = new Hashtable();
      Hashtable hashtable = this.AssignedSkills(this._originSlot, this._originSlotTalentId, skillSlotIndex, slotSkillId);
      postData.Add((object) "uid", (object) PlayerData.inst.uid);
      postData.Add((object) "skills", (object) hashtable);
      if (hashtable != null && hashtable.Count > 0)
        MessageHub.inst.GetPortByAction("dragonKnight:setSkill").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            this.ResetDragStatus();
          this.BringBackward();
          AudioManager.Instance.StopAndPlaySound("sfx_city_dragon_skill_assign");
        }), true);
    }
    else
    {
      this.ResetDragStatus();
      this.BringBackward();
    }
    this.EnableGeneratedObject(this.gameObject);
  }

  private void CheckCloneObjects()
  {
    if (!this.cloneOnDrag || !((UnityEngine.Object) this.mTouch.dragged != (UnityEngine.Object) null))
      return;
    NGUITools.SetActive(this.mTouch.dragged, false);
  }

  private void ResetDragStatus()
  {
    if (!this.cloneOnDrag)
      this.ResetDropItem();
    else
      this.ClearCurrentTexture(true);
  }

  private int GetSkillSlotIndex(string surfaceParentName)
  {
    string s = surfaceParentName.Replace("AssignedSkill_", string.Empty);
    int result = 0;
    int.TryParse(s, out result);
    return result;
  }

  private int GetSlotSkillId(int slotIndex)
  {
    int num = 0;
    DragonKnightDungeonData knightDungeonData = DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L);
    if (knightDungeonData != null && knightDungeonData.AssignedSkill != null)
      knightDungeonData.AssignedSkill.talentSkills.TryGetValue(slotIndex, out num);
    return num;
  }

  private Hashtable AssignedSkills(int originSlot, int originSlotTalentId, int finalSlot, int finalSlotTalentId)
  {
    if (originSlotTalentId <= 0 || !this.IsAvailabeSlot(finalSlot))
      return (Hashtable) null;
    DragonKnightDungeonData knightDungeonData = DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L);
    Dictionary<int, int> dictionary = new Dictionary<int, int>();
    Dictionary<int, int> assignedSkills = knightDungeonData == null ? dictionary : knightDungeonData.AssignedSkill.talentSkills;
    this.RemoveExistSkill(originSlotTalentId, ref assignedSkills);
    this.AssignSkill(finalSlot, originSlotTalentId, ref assignedSkills);
    if (this.IsAvailabeSlot(originSlot))
    {
      this.RemoveExistSkill(finalSlotTalentId, ref assignedSkills);
      this.AssignSkill(originSlot, finalSlotTalentId, ref assignedSkills);
    }
    return this.ConvertDict2Hash(assignedSkills);
  }

  private Hashtable ConvertDict2Hash(Dictionary<int, int> dict)
  {
    Hashtable hashtable = new Hashtable();
    if (dict != null)
    {
      using (Dictionary<int, int>.Enumerator enumerator = dict.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, int> current = enumerator.Current;
          hashtable.Add((object) current.Key.ToString(), (object) current.Value);
        }
      }
    }
    return hashtable;
  }

  private bool IsAvailabeSlot(int slot)
  {
    return slot > 0 && slot <= 3;
  }

  private void RemoveExistSkill(int removeSkillId, ref Dictionary<int, int> assignedSkills)
  {
    int key = 0;
    if (assignedSkills == null)
      return;
    using (Dictionary<int, int>.Enumerator enumerator = assignedSkills.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, int> current = enumerator.Current;
        if (current.Value == removeSkillId)
        {
          key = current.Key;
          break;
        }
      }
    }
    assignedSkills.Remove(key);
  }

  private void AssignSkill(int slotIndex, int assignSkillId, ref Dictionary<int, int> assignedSkills)
  {
    if (!this.IsAvailabeSlot(slotIndex) || assignSkillId <= 0 || assignedSkills == null)
      return;
    if (assignedSkills.ContainsKey(slotIndex))
      assignedSkills[slotIndex] = assignSkillId;
    else
      assignedSkills.Add(slotIndex, assignSkillId);
  }
}
