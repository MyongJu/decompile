﻿// Decompiled with JetBrains decompiler
// Type: MonsterOverview
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class MonsterOverview : MonoBehaviour
{
  public UILabel playername;
  public UITexture icon;
  public UILabel position;
  public UILabel powerlost;
  public UILabel totalTroop;
  public UILabel wounded;
  public UILabel survived;
  public DragonSkillForWarReportComponent dsc;

  public void SeedData(MonsterOverview.Data d)
  {
    this.playername.text = d.name;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, d.icon, (System.Action<bool>) null, true, false, string.Empty);
    this.position.text = "K: " + d.k + " X: " + d.x + " Y: " + d.y;
    this.powerlost.text = d.powerlost != 0L ? "-" + d.powerlost.ToString() : d.powerlost.ToString();
    this.totalTroop.text = d.totalTroop.ToString();
    this.wounded.text = d.wounded.ToString();
    this.survived.text = d.survived.ToString();
  }

  public class Data
  {
    public string name;
    public string icon;
    public string k;
    public string x;
    public string y;
    public long powerlost;
    public long totalTroop;
    public long wounded;
    public long survived;
    public Hashtable dsht;
  }
}
