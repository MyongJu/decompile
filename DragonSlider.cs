﻿// Decompiled with JetBrains decompiler
// Type: DragonSlider
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using UnityEngine;

public class DragonSlider : MonoBehaviour
{
  public System.Action<bool> onDragonSelectedChanged;
  private bool _isSelected;
  private bool _lock;
  [SerializeField]
  private DragonSlider.Panel panel;

  public DragonSlider.DragonState state { get; private set; }

  public bool isSelected
  {
    get
    {
      return this._isSelected;
    }
    set
    {
      this._isSelected = value;
      this.panel.dragonChecker.gameObject.SetActive(value);
    }
  }

  public void JustShow(string dragonInfoType = null)
  {
    DragonData dragonData = PlayerData.inst.dragonData;
    this.panel.noDragonContainer.gameObject.SetActive(false);
    this.panel.inCityContainer.gameObject.SetActive(false);
    this.panel.marchingContainer.gameObject.SetActive(false);
    if (dragonData == null)
    {
      this.state = DragonSlider.DragonState.notBirth;
      this.panel.noDragonContainer.gameObject.SetActive(true);
    }
    else
    {
      this.state = DragonSlider.DragonState.inCity;
      this.panel.inCityContainer.gameObject.SetActive(true);
      this.panel.dragonPower.text = string.Format("{0} : {1}", (object) ScriptLocalization.Get("march_dragon_power_name", true), (object) Utils.FormatThousands(PlayerData.inst.userData.dragon_power.ToString()));
      this.panel.BT_Checker.isEnabled = true;
      if (!string.IsNullOrEmpty(dragonInfoType))
        this.panel.dragonInfo.SetDragon(dragonData.Uid, dragonInfoType, (int) dragonData.tendency, dragonData.Level);
      else
        this.panel.dragonInfo.JustShow(dragonData.Uid);
    }
  }

  public void Refresh(string dragonInfoType, bool forcedSelected = false)
  {
    DragonData dragonData = PlayerData.inst.dragonData;
    this.panel.noDragonContainer.gameObject.SetActive(false);
    this.panel.inCityContainer.gameObject.SetActive(false);
    this.panel.marchingContainer.gameObject.SetActive(false);
    if (dragonData == null)
    {
      this.state = DragonSlider.DragonState.notBirth;
      this.panel.noDragonContainer.gameObject.SetActive(true);
    }
    else if (dragonData.MarchId != 0L && !forcedSelected)
    {
      this.state = DragonSlider.DragonState.marching;
      this.panel.marchingContainer.gameObject.SetActive(true);
      this.panel.marchingDragonInfo.SetDragon(dragonData.Uid, dragonInfoType, (int) dragonData.tendency, dragonData.Level);
    }
    else
    {
      this.state = DragonSlider.DragonState.inCity;
      this.panel.inCityContainer.gameObject.SetActive(true);
      this.panel.dragonPower.text = string.Format("{0} : {1}", (object) ScriptLocalization.Get("march_dragon_power_name", true), (object) Utils.FormatThousands(PlayerData.inst.userData.dragon_power.ToString()));
      this.panel.dragonInfo.SetDragon(dragonData.Uid, dragonInfoType, (int) dragonData.tendency, dragonData.Level);
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.DragonKnight)
        this.panel.BT_Checker.isEnabled = true;
      else
        this.panel.BT_Checker.isEnabled = false;
    }
  }

  public void LockSelected()
  {
    this._lock = true;
  }

  public void OnDragonSelectedClick()
  {
    if (this._lock)
      return;
    this.isSelected = !this.isSelected;
    if (this.onDragonSelectedChanged == null)
      return;
    this.onDragonSelectedChanged(this.isSelected);
  }

  public void Reset()
  {
    this.panel.noDragonContainer = this.transform.Find("NoDragonContainer").gameObject.transform;
    this.panel.inCityContainer = this.transform.Find("InCtiyDragonContainer").gameObject.transform;
    this.panel.marchingContainer = this.transform.Find("MarchingDragonContainer").gameObject.transform;
    this.panel.dragonInfo = this.transform.Find("InCtiyDragonContainer/Dragon").gameObject.GetComponent<DragonUIInfo>();
    this.panel.marchingDragonInfo = this.transform.Find("MarchingDragonContainer/Dragon").gameObject.GetComponent<DragonUIInfo>();
    this.panel.dragonPower = this.transform.Find("InCtiyDragonContainer/DragonPower").gameObject.GetComponent<UILabel>();
    this.panel.BT_Checker = this.transform.Find("InCtiyDragonContainer/DragonCheckBox").gameObject.GetComponent<UIButton>();
    this.panel.BT_Checker.onClick.Clear();
    this.panel.BT_Checker.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnDragonSelectedClick)));
    this.panel.dragonChecker = this.transform.Find("InCtiyDragonContainer/DragonCheckBox/DragonChecker").gameObject.transform;
    this.panel.dragonChecker.gameObject.SetActive(false);
  }

  [Serializable]
  public class Panel
  {
    public Transform noDragonContainer;
    public Transform inCityContainer;
    public Transform marchingContainer;
    public DragonUIInfo dragonInfo;
    public DragonUIInfo marchingDragonInfo;
    public UILabel dragonPower;
    public Transform dragonChecker;
    public UIButton BT_Checker;
  }

  public enum DragonState
  {
    notBirth,
    inCity,
    marching,
  }
}
