﻿// Decompiled with JetBrains decompiler
// Type: ExchangeHallSort1Info
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ExchangeHallSort1Info
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "priority")]
  public int priority;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "icon")]
  public string icon;

  public string Name
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }

  public string SpriteName
  {
    get
    {
      return this.icon;
    }
  }
}
