﻿// Decompiled with JetBrains decompiler
// Type: AllianceFortressDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UI;

public class AllianceFortressDetailDlg : UI.Dialog
{
  public AllianceFortressInfo info;
  public AllianceFortressSlotContainer container;
  public UILabel title;
  public UILabel hint;
  public UIWidget m_FocusTarget;
  public UIButton demolishBtn;
  private Coordinate fortressCor;
  private AllianceFortressData allianceFrotressData;
  private bool isDestroy;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    this.OnShow(orgParam);
    AllianceFortressDetailDlg.Parameter parameter = orgParam as AllianceFortressDetailDlg.Parameter;
    if (parameter != null)
    {
      this.allianceFrotressData = parameter.allianceFrotressData;
      this.fortressCor = parameter.cor;
    }
    PVPSystem.Instance.Map.FocusTile(PVPMapData.MapData.ConvertTileKXYToWorldPosition(this.fortressCor), this.m_FocusTarget, 0.0f);
    UIManager.inst.CloseKingdomTouchCircle();
    if (PlayerData.inst.allianceId == this.allianceFrotressData.allianceId)
    {
      Hashtable postData = new Hashtable();
      postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
      MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) null, true);
      AllianceBuildUtils.LoadMaxTroopCount(this.allianceFrotressData, (System.Action) (() => this.UpdateUI()));
    }
    else
      AllianceBuildUtils.LoadMaxTroopCount(this.allianceFrotressData, (System.Action) (() => this.UpdateUI()));
    this.AddEventHandler();
  }

  private void OnDestroy()
  {
    this.isDestroy = true;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.OnHide(orgParam);
    this.Clear();
    this.RemoveEventHandler();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void Clear()
  {
    this.info.Clear();
    this.container.Clear();
  }

  private void UpdateUI()
  {
    if (this.isDestroy)
      return;
    if (DBManager.inst.DB_AllianceFortress.GetDataByCoordinate(this.fortressCor) == null)
    {
      this.OnCloseHandler();
    }
    else
    {
      this.info.SeedData(this.fortressCor, this.allianceFrotressData);
      this.container.SetData(this.allianceFrotressData);
      this.title.text = this.GetTitle();
      this.hint.text = this.GetHintByState(this.allianceFrotressData.State);
      this.UpdateDemolishBtnState();
    }
  }

  private string GetTitle()
  {
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(this.allianceFrotressData.allianceId);
    StringBuilder stringBuilder = new StringBuilder();
    if (allianceData != null)
      stringBuilder.Append("(" + allianceData.allianceAcronym + ")");
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(this.allianceFrotressData.ConfigId);
    if (buildingStaticInfo != null)
      stringBuilder.Append(Utils.XLAT(buildingStaticInfo.BuildName));
    return stringBuilder.ToString();
  }

  private void UpdateDemolishBtnState()
  {
    this.demolishBtn.isEnabled = PlayerData.inst.allianceId == this.allianceFrotressData.allianceId && AllianceBuildUtils.HasRightToDemolish();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_AllianceFortress.onDataUpdate += new System.Action<AllianceFortressData>(this.OnFortressUpdated);
    DBManager.inst.DB_AllianceFortress.onDataRemoved += new System.Action<AllianceFortressData>(this.OnFortressRemoved);
  }

  private void OnFortressRemoved(AllianceFortressData obj)
  {
    if (this.allianceFrotressData.fortressId != obj.fortressId)
      return;
    this.OnCloseHandler();
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_AllianceFortress.onDataUpdate -= new System.Action<AllianceFortressData>(this.OnFortressUpdated);
    DBManager.inst.DB_AllianceFortress.onDataRemoved -= new System.Action<AllianceFortressData>(this.OnFortressRemoved);
  }

  private void OnFortressUpdated(AllianceFortressData obj)
  {
    if (this.allianceFrotressData.fortressId != obj.fortressId)
      return;
    this.UpdateUI();
  }

  private string GetHintByState(string sts)
  {
    string empty1 = string.Empty;
    string empty2 = string.Empty;
    if (this.allianceFrotressData.allianceId != PlayerData.inst.allianceId)
      return ScriptLocalization.Get("alliance_fort_troop_demolish_tip", true);
    string state = this.allianceFrotressData.State;
    string Term;
    if (state != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceFortressDetailDlg.\u003C\u003Ef__switch\u0024map1B == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceFortressDetailDlg.\u003C\u003Ef__switch\u0024map1B = new Dictionary<string, int>(7)
        {
          {
            "building",
            0
          },
          {
            "repairing",
            0
          },
          {
            "unDefend",
            1
          },
          {
            "defending",
            2
          },
          {
            "unComplete",
            3
          },
          {
            "broken",
            3
          },
          {
            "demolishing",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceFortressDetailDlg.\u003C\u003Ef__switch\u0024map1B.TryGetValue(state, out num))
      {
        switch (num)
        {
          case 0:
            Term = "alliance_fort_troop_construction_speed_tip";
            goto label_13;
          case 1:
            Term = "alliance_fort_troop_garrison_tip";
            goto label_13;
          case 2:
            Term = "alliance_fort_troop_hospital_tip";
            goto label_13;
          case 3:
            Term = "alliance_fort_troop_repair_tip";
            goto label_13;
          case 4:
            Term = "alliance_fort_troop_recapture_tip";
            goto label_13;
        }
      }
    }
    Term = string.Empty;
label_13:
    return ScriptLocalization.Get(Term, true);
  }

  public void OnDemolishBtnClicked()
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(this.allianceFrotressData.ConfigId);
    if (buildingStaticInfo == null)
      return;
    string content = string.Format(ScriptLocalization.Get("alliance_buildings_confirm_demolish_description", true), (object) ScriptLocalization.Get(buildingStaticInfo.BuildName, true));
    string title = ScriptLocalization.Get("alliance_buildings_confirm_placement_button", true);
    string left = ScriptLocalization.Get("id_uppercase_confirm", true);
    string right = ScriptLocalization.Get("id_uppercase_cancel", true);
    UIManager.inst.ShowConfirmationBox(title, content, left, right, ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.ConfirmDemolish), (System.Action) null, (System.Action) null);
  }

  private void ConfirmDemolish()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "k"] = (object) this.allianceFrotressData.Location.K;
    postData[(object) "x"] = (object) this.allianceFrotressData.Location.X;
    postData[(object) "y"] = (object) this.allianceFrotressData.Location.Y;
    MessageHub.inst.GetPortByAction("Map:takeBackAllianceFortress").SendRequest(postData, (System.Action<bool, object>) null, true);
    this.OnCloseButtonPress();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public AllianceFortressData allianceFrotressData;
    public Coordinate cor;
  }
}
