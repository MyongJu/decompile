﻿// Decompiled with JetBrains decompiler
// Type: ChatMutePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;

public class ChatMutePopup : Popup
{
  public const string POP_TYPE = "Chat/ChatMutePopup";
  public UITexture Sender_userIcon;
  public UILabel nameLabel;
  public UILabel desLabel;
  private long _personalChannelId;
  private long _personalUid;
  private string _personalName;
  private string _senderCustomIcon;
  private int _personMuteAdmin;
  private int _personIcon;
  private int _personLordTitleId;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    ChatMutePopup.Paramer paramer = orgParam as ChatMutePopup.Paramer;
    if (paramer != null)
    {
      this._personalChannelId = paramer.personalChannelId;
      this._personalUid = paramer.personalUid;
      this._personalName = paramer.personalName;
      this._senderCustomIcon = paramer.senderCustomIcon;
      this._personMuteAdmin = paramer.personMuteAdmin;
      this._personIcon = paramer.personIcon;
      this._personLordTitleId = paramer.personLordTitleId;
    }
    UserData userData = DBManager.inst.DB_User.Get(this._personalUid);
    if (string.IsNullOrEmpty(this._personalName))
    {
      if (userData != null)
        this.nameLabel.text = userData.userName;
    }
    else
      this.nameLabel.text = this._personalName;
    CustomIconLoader.Instance.requestCustomIcon(this.Sender_userIcon, UserData.GetPortraitIconPath(this._personIcon), this._senderCustomIcon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.Sender_userIcon, this._personLordTitleId, 1);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.HandlesecondEvent);
    this.HandlesecondEvent(0);
  }

  private void HandlesecondEvent(int obj)
  {
    UserData userData = DBManager.inst.DB_User.Get(this._personalUid);
    if (userData == null)
      return;
    if (userData.IsShutUp)
      this.desLabel.text = Utils.FormatTime(userData.ShutUpLeftTime, false, false, true);
    else
      this.desLabel.text = "00:00";
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.HandlesecondEvent);
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnUnBanBtnClicked()
  {
    MuteAPI.UnMuteUser(this._personalUid, (System.Action) (() => this.OnCloseBtnClicked()));
  }

  public void OnBanBtnClicked(int time)
  {
    MuteAPI.MuteUser(this._personalUid, (long) (time * 60), (System.Action) (() => this.OnCloseBtnClicked()));
  }

  public class Paramer : Popup.PopupParameter
  {
    public long personalChannelId;
    public long personalUid;
    public string personalName;
    public string senderCustomIcon;
    public int personMuteAdmin;
    public int personIcon;
    public int personLordTitleId;
  }
}
