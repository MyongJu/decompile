﻿// Decompiled with JetBrains decompiler
// Type: AllianceActivitySubInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class AllianceActivitySubInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "alliance_activity_id")]
  public int ActivityMainID;
  [Config(Name = "name")]
  public string name;

  public string LocName
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }
}
