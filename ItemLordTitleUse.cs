﻿// Decompiled with JetBrains decompiler
// Type: ItemLordTitleUse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class ItemLordTitleUse : ItemBaseUse
{
  public ItemLordTitleUse(int id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  public ItemLordTitleUse(string id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  protected override void CustomUse(System.Action<bool, object> resultHandler)
  {
    if (this.Info == null)
      return;
    LordTitleMainInfo mainInfo = ConfigManager.inst.DB_LordTitleMain.Get(this.Info.Param1);
    if (mainInfo == null)
      return;
    LordTitlePayload.Instance.UnlockAvatorTitle(mainInfo.internalId, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      LordTitlePayload.Instance.SetUnlockedStatus(mainInfo.internalId, 1);
      UIManager.inst.OpenPopup("LordTitle/LordTitleDetailPopup", (Popup.PopupParameter) new LordTitleDetailPopup.Parameter()
      {
        lordTitleId = mainInfo.internalId
      });
    }));
  }
}
