﻿// Decompiled with JetBrains decompiler
// Type: LegendDevourSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class LegendDevourSlot : MonoBehaviour
{
  public UITexture m_Portrait;
  public UILabel m_Level;
  public UILabel m_Exp;
  private LegendData m_LegendData;
  private System.Action<LegendData> m_Callback;

  public void SetData(LegendData legendData, System.Action<LegendData> callback)
  {
    this.m_LegendData = legendData;
    this.m_Callback = callback;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this.m_LegendData == null)
    {
      this.m_Portrait.mainTexture = (Texture) null;
      this.m_Exp.text = string.Empty;
      this.m_Level.text = string.Empty;
    }
    else
    {
      LegendInfo legendInfo = ConfigManager.inst.DB_Legend.GetLegendInfo(this.m_LegendData.LegendID);
      if (legendInfo != null)
        BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Portrait, legendInfo.Icon, (System.Action<bool>) null, true, false, string.Empty);
      this.m_Exp.text = string.Format("+ {0} EXP", (object) Utils.FormatThousands(this.m_LegendData.Xp.ToString()));
      int nextXp = 0;
      this.m_Level.text = "Lv. " + Utils.FormatThousands(ConfigManager.inst.DB_LegendPoint.GetLegendLevelByXP(this.m_LegendData.Xp, out nextXp).ToString());
    }
  }

  public LegendData GetLegendData()
  {
    return this.m_LegendData;
  }

  public void OnClicked()
  {
    if (this.m_Callback == null)
      return;
    this.m_Callback(this.m_LegendData);
  }
}
