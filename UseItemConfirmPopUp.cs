﻿// Decompiled with JetBrains decompiler
// Type: UseItemConfirmPopUp
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;

public class UseItemConfirmPopUp : Popup
{
  private string itemID = string.Empty;
  private const string HORN_ITEM_ID = "";
  public Icon itemIcon;
  public UILabel buttonLabel;
  public UILabel priceLabel;
  public UISprite goldTypeIcon;
  private System.Action onUseCallBack;
  public UIButton useButton;
  public UIButton buyButton;
  private Hashtable useItemParam;
  public UILabel title;

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    UseItemConfirmPopUp.Parameter parameter = orgParam as UseItemConfirmPopUp.Parameter;
    this.itemID = parameter.itemID;
    this.onUseCallBack = parameter.onUseCallBack;
    if (!string.IsNullOrEmpty(parameter.buttonLabel))
      this.buttonLabel.text = parameter.buttonLabel;
    this.useItemParam = parameter.param;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.itemID);
    int itemCount = ItemBag.Instance.GetItemCount(this.itemID);
    ShopStaticInfo byItemInternalId = ConfigManager.inst.DB_Shop.GetShopInfoByItemInternalId(itemStaticInfo.internalId);
    this.title.text = itemStaticInfo.LocName;
    this.itemIcon.FeedData((IComponentData) new IconData(itemStaticInfo.ImagePath, new string[2]
    {
      itemStaticInfo.LocDescription,
      itemCount.ToString()
    }));
    if (byItemInternalId != null)
      Utils.SetPriceToLabel(this.priceLabel, ItemBag.Instance.GetShopItemPrice(byItemInternalId.internalId));
    NGUITools.SetActive(this.useButton.gameObject, itemCount > 0);
    NGUITools.SetActive(this.buyButton.gameObject, itemCount <= 0);
  }

  public void OnUseItemHandler()
  {
    ItemBag.Instance.UseItem(this.itemID, this.useItemParam, (System.Action<bool, object>) null, 1);
    if (this.onUseCallBack != null)
    {
      this.onUseCallBack();
      this.onUseCallBack = (System.Action) null;
    }
    this.OnCloseHandler();
  }

  public void OnBuyItemHandler()
  {
    ItemBag.Instance.BuyAndUseShopItem(ConfigManager.inst.DB_Shop.GetShopInfoByItemInternalId(ConfigManager.inst.DB_Items.GetItem(this.itemID).internalId).internalId, 1, this.useItemParam, (System.Action<bool, object>) null);
    if (this.onUseCallBack != null)
    {
      this.onUseCallBack();
      this.onUseCallBack = (System.Action) null;
    }
    this.OnCloseHandler();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string itemID;
    public System.Action onUseCallBack;
    public string buttonLabel;
    public Hashtable param;
  }
}
