﻿// Decompiled with JetBrains decompiler
// Type: HeroStatDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class HeroStatDlg : MonoBehaviour
{
  [SerializeField]
  private HeroStatDlg.Panel panel;
  private bool _refreshed;
  private UIPanel _uipanel;

  public bool Refreshed
  {
    set
    {
      this._refreshed = value;
      if (this._refreshed)
        return;
      this.Refresh();
    }
  }

  public void Refresh()
  {
    if (PlayerData.inst.CurrentHero == null || this._refreshed || !this.gameObject.activeSelf)
      return;
    if (!(bool) ((UnityEngine.Object) this._uipanel))
      this._uipanel = this.panel.propertiesCityContainer.transform.parent.parent.GetComponent<UIPanel>();
    foreach (Component componentsInChild in this.panel.propertiesOverallTable.GetComponentsInChildren<ItemSkillBenefitSlot>())
      UnityEngine.Object.Destroy((UnityEngine.Object) componentsInChild.gameObject);
    foreach (Component componentsInChild in this.panel.propertiesCityTable.GetComponentsInChildren<ItemSkillBenefitSlot>())
      UnityEngine.Object.Destroy((UnityEngine.Object) componentsInChild.gameObject);
    foreach (Component componentsInChild in this.panel.propertiesTroopsTable.GetComponentsInChildren<ItemSkillBenefitSlot>())
      UnityEngine.Object.Destroy((UnityEngine.Object) componentsInChild.gameObject);
    int num1 = 0;
    int num2 = 0;
    int num3 = 0;
    if (this.AddPercentPropertyToPanel(Property.HERO_HEALTH, this.panel.propertiesOverallTable))
      ++num1;
    if (this.AddPercentPropertyToPanel(Property.HERO_ATTACK, this.panel.propertiesOverallTable))
      ++num1;
    if (this.AddPercentPropertyToPanel(Property.HERO_DEFENSE, this.panel.propertiesOverallTable))
      ++num1;
    if (this.AddPercentPropertyToPanel(Property.FOOD_GENERATION, this.panel.propertiesCityTable))
      ++num2;
    if (this.AddPercentPropertyToPanel(Property.WOOD_GENERATION, this.panel.propertiesCityTable))
      ++num2;
    if (this.AddPercentPropertyToPanel(Property.ORE_GENERATION, this.panel.propertiesCityTable))
      ++num2;
    if (this.AddPercentPropertyToPanel(Property.SILVER_GENERATION, this.panel.propertiesCityTable))
      ++num2;
    if (this.AddPercentPropertyToPanel(Property.CONSTRUCTION_TIME, this.panel.propertiesCityTable))
      ++num2;
    if (this.AddPercentPropertyToPanel(Property.RESEARCH_TIME, this.panel.propertiesCityTable))
      ++num2;
    if (this.AddPercentPropertyToPanel(Property.INFANTRY_HEALTH, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.INFANTRY_ATTACK, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.INFANTRY_DEFENSE, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.INFANTRY_UPKEEP, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.CAVALRY_HEALTH, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.CAVALRY_ATTACK, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.CAVALRY_DEFENSE, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.CAVALRY_UPKEEP, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.RANGED_HEALTH, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.RANGED_ATTACK, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.RANGED_DEFENSE, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.RANGED_UPKEEP, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.ARTYCLOSE_HEALTH, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.ARTYCLOSE_ATTACK, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.ARTYCLOSE_DEFENSE, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.ARTYCLOSE_UPKEEP, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.ARTYFAR_HEALTH, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.ARTYFAR_ATTACK, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.ARTYFAR_DEFENSE, this.panel.propertiesTroopsTable))
      ++num3;
    if (this.AddPercentPropertyToPanel(Property.ARTYFAR_UPKEEP, this.panel.propertiesTroopsTable))
      ++num3;
    this.panel.propertiesOverallContainer.SetActive(num1 > 0);
    this.panel.propertiesCityContainer.SetActive(num2 > 0);
    this.panel.propertiesTroopsContainer.SetActive(num3 > 0);
    UITable propertiesOverallTable = this.panel.propertiesOverallTable;
    bool flag1 = true;
    this.panel.propertiesTroopsTable.repositionNow = flag1;
    bool flag2 = flag1;
    this.panel.propertiesCityTable.repositionNow = flag2;
    int num4 = flag2 ? 1 : 0;
    propertiesOverallTable.repositionNow = num4 != 0;
    if ((bool) ((UnityEngine.Object) this._uipanel))
    {
      this._uipanel.alpha = 0.0f;
      this.StartCoroutine(this.UpdateVisibilty());
    }
    this._refreshed = true;
    this.StartCoroutine(this._DelayReposition(this.panel.propertiesTable));
  }

  private bool AddValuePropertyToPanel(string propertyPrefix, UITable table)
  {
    if (!ConfigManager.inst.DB_Properties.Contains(propertyPrefix + Property.VALUE))
      return false;
    ItemSkillBenefitSlot component = NGUITools.AddChild(table.gameObject, this.panel.benefitSlotOrg.gameObject).GetComponent<ItemSkillBenefitSlot>();
    component.SetScrollView(this.panel.scrollView);
    component.Setup(ConfigManager.inst.DB_Properties[propertyPrefix + Property.VALUE].InternalID, string.Format("{0}", (object) Property.EffectiveBonus(propertyPrefix + Property.VALUE, PlayerData.inst.CurrentHero.ID, Effect.ConditionType.all)));
    return true;
  }

  private bool AddPercentPropertyToPanel(string propertyPrefix, UITable table)
  {
    if (!ConfigManager.inst.DB_Properties.Contains(propertyPrefix + Property.PERCENT))
      return false;
    ItemSkillBenefitSlot component = NGUITools.AddChild(table.gameObject, this.panel.benefitSlotOrg.gameObject).GetComponent<ItemSkillBenefitSlot>();
    component.SetScrollView(this.panel.scrollView);
    component.Setup(ConfigManager.inst.DB_Properties[propertyPrefix + Property.PERCENT].InternalID, string.Format("{0:P}", (object) Property.EffectiveBonus(propertyPrefix + Property.PERCENT, PlayerData.inst.CurrentHero.ID, Effect.ConditionType.all)));
    return true;
  }

  [DebuggerHidden]
  private IEnumerator _DelayReposition(UITable table)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroStatDlg.\u003C_DelayReposition\u003Ec__Iterator71()
    {
      table = table,
      \u003C\u0024\u003Etable = table
    };
  }

  [DebuggerHidden]
  private IEnumerator UpdateVisibilty()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HeroStatDlg.\u003CUpdateVisibilty\u003Ec__Iterator72()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void OnEnable()
  {
    this.Refresh();
  }

  [Serializable]
  protected class Panel
  {
    public GameObject properitesConatiner;
    public UITable propertiesTable;
    public GameObject propertiesOverallContainer;
    public UITable propertiesOverallTable;
    public GameObject propertiesCityContainer;
    public UITable propertiesCityTable;
    public GameObject propertiesTroopsContainer;
    public UITable propertiesTroopsTable;
    public ItemSkillBenefitSlot benefitSlotOrg;
    public UIScrollView scrollView;
  }
}
