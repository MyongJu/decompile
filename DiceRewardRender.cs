﻿// Decompiled with JetBrains decompiler
// Type: DiceRewardRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DiceRewardRender : MonoBehaviour
{
  private int _index = -1;
  public ItemIconRenderer icon;
  public UILabel desc;
  public UISprite state_gray;
  public UISprite state_select;
  public UISprite state_gotten;

  public event DiceRewardRender.ClickEventHandler OnRewardClickHandler;

  public int Index
  {
    set
    {
      this._index = value;
    }
  }

  public void ClearState()
  {
    this.state_gray.gameObject.SetActive(false);
    this.state_select.gameObject.SetActive(false);
    this.state_gotten.gameObject.SetActive(false);
  }

  public void SetState(DiceRewardRender.State state)
  {
    switch (state)
    {
      case DiceRewardRender.State.gray:
        this.state_gray.gameObject.SetActive(true);
        break;
      case DiceRewardRender.State.selected:
        this.state_select.gameObject.SetActive(true);
        break;
      case DiceRewardRender.State.gotten:
        this.state_gotten.gameObject.SetActive(true);
        break;
    }
  }

  public void OnRewardClick()
  {
    if (this.OnRewardClickHandler == null)
      return;
    this.OnRewardClickHandler(this._index);
  }

  public enum State
  {
    gray,
    selected,
    gotten,
  }

  public delegate void ClickEventHandler(int index);
}
