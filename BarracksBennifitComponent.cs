﻿// Decompiled with JetBrains decompiler
// Type: BarracksBennifitComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class BarracksBennifitComponent : MonoBehaviour
{
  public static Dictionary<BarracksBennifitSource, string> titleKeyDic = new Dictionary<BarracksBennifitSource, string>()
  {
    {
      BarracksBennifitSource.HERO_SKILL,
      "id_barracks_bonuses_heroskills"
    },
    {
      BarracksBennifitSource.RESEARCH,
      "id_barracks_bonuses_research"
    },
    {
      BarracksBennifitSource.BUILDING,
      "id_barracks_bonuses_building"
    },
    {
      BarracksBennifitSource.OTHERS,
      "id_barracks_bonuses_other"
    }
  };
  public static string[] BENNIFITKEYS = new string[8]
  {
    "prop_{0}_health",
    "prop_{0}_attack",
    "prop_{0}_defense",
    "prop_{0}_speed",
    "prop_{0}_load",
    "prop_{0}_upkeep",
    "prop_{0}_training_time",
    "prop_{0}_healing_time"
  };
  public BarracksBennifitSource bennifitSource;
  public UILabel title;
  public UILabel noBenifitLabel;
  public UILabel[] nameLabels;
  public UILabel[] valueLabels;

  public void UpdateUI(string type)
  {
    string str = type.Substring(type.IndexOf("_") + 1);
    this.title.text = ScriptLocalization.Get(BarracksBennifitComponent.titleKeyDic[this.bennifitSource], true);
    bool flag = false;
    for (int index = 0; index < this.nameLabels.Length; ++index)
    {
      BenefitInfo benefitInfo = DBManager.inst.DB_Local_Benefit.Get(string.Format(BarracksBennifitComponent.BENNIFITKEYS[index], (object) str));
      switch (this.bennifitSource)
      {
        case BarracksBennifitSource.HERO_SKILL:
          if (ConfigManager.inst.DB_Properties[string.Format(BarracksBennifitComponent.BENNIFITKEYS[index], (object) str)] != null)
          {
            this.nameLabels[index].text = ConfigManager.inst.DB_Properties[string.Format(BarracksBennifitComponent.BENNIFITKEYS[index], (object) str)].Name;
            this.valueLabels[index].text = benefitInfo.hero.value.ToString();
            flag = true;
            break;
          }
          this.nameLabels[index].text = string.Empty;
          this.valueLabels[index].text = string.Empty;
          break;
        case BarracksBennifitSource.RESEARCH:
          if (ConfigManager.inst.DB_Properties[string.Format(BarracksBennifitComponent.BENNIFITKEYS[index], (object) str)] != null)
          {
            this.nameLabels[index].text = ConfigManager.inst.DB_Properties[string.Format(BarracksBennifitComponent.BENNIFITKEYS[index], (object) str)].Name;
            this.valueLabels[index].text = benefitInfo.research.value.ToString();
            flag = true;
            break;
          }
          this.nameLabels[index].text = string.Empty;
          this.valueLabels[index].text = string.Empty;
          break;
        case BarracksBennifitSource.BUILDING:
          if (ConfigManager.inst.DB_Properties[string.Format(BarracksBennifitComponent.BENNIFITKEYS[index], (object) str)] != null)
          {
            this.nameLabels[index].text = ConfigManager.inst.DB_Properties[string.Format(BarracksBennifitComponent.BENNIFITKEYS[index], (object) str)].Name;
            this.valueLabels[index].text = benefitInfo.building.value.ToString();
            flag = true;
            break;
          }
          this.nameLabels[index].text = string.Empty;
          this.valueLabels[index].text = string.Empty;
          break;
        case BarracksBennifitSource.OTHERS:
          if (ConfigManager.inst.DB_Properties[string.Format(BarracksBennifitComponent.BENNIFITKEYS[index], (object) str)] != null)
          {
            this.nameLabels[index].text = ConfigManager.inst.DB_Properties[string.Format(BarracksBennifitComponent.BENNIFITKEYS[index], (object) str)].Name;
            this.valueLabels[index].text = string.Empty;
            flag = true;
            break;
          }
          this.nameLabels[index].text = string.Empty;
          this.valueLabels[index].text = string.Empty;
          break;
      }
    }
    this.noBenifitLabel.enabled = !flag;
  }
}
