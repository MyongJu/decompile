﻿// Decompiled with JetBrains decompiler
// Type: PrefabSpawnRequestEx
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PrefabSpawnRequestEx : LinkedList.Node
{
  public CacheInfoEx cacheInfo;
  public PrefabSpawnRequestCallback callback;
  public bool cancel;
  public object userData;
  public Transform parent;

  public void Reset()
  {
    this.cacheInfo = (CacheInfoEx) null;
    this.callback = (PrefabSpawnRequestCallback) null;
    this.cancel = false;
    this.userData = (object) null;
  }
}
