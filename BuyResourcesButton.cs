﻿// Decompiled with JetBrains decompiler
// Type: BuyResourcesButton
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BuyResourcesButton : MonoBehaviour
{
  public CityManager.ResourceTypes type;

  public void OnResourcesBtnClicked()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(this.type);
  }
}
