﻿// Decompiled with JetBrains decompiler
// Type: PitExploreViewRankPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class PitExploreViewRankPopup : Popup
{
  private Dictionary<int, PitExploreRankContainer> _itemDict = new Dictionary<int, PitExploreRankContainer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UIGrid _grid;
  [SerializeField]
  private GameObject _itemPrefab;
  [SerializeField]
  private GameObject _itemRoot;
  [SerializeField]
  private UILabel _noHistoryHint;
  private List<KeyValuePair<int, List<PitExplorePayload.PitRankDataWithTime>>> _rankDataList;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    PitExploreViewRankPopup.Parameter parameter = orgParam as PitExploreViewRankPopup.Parameter;
    if (parameter != null)
      this._rankDataList = parameter.rankDataList;
    this._itemPool.Initialize(this._itemPrefab, this._itemRoot);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.ClearData();
    if (this._rankDataList != null && this._rankDataList.Count > 0)
    {
      for (int key = 0; key < this._rankDataList.Count; ++key)
      {
        PitExploreRankContainer itemRenderer = this.CreateItemRenderer(this._rankDataList[key].Value);
        this._itemDict.Add(key, itemRenderer);
      }
      NGUITools.SetActive(this._noHistoryHint.gameObject, false);
    }
    else
      NGUITools.SetActive(this._noHistoryHint.gameObject, true);
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, PitExploreRankContainer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, PitExploreRankContainer> current = enumerator.Current;
        current.Value.ClearData();
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this._grid.repositionNow = true;
    this._grid.Reposition();
    this._scrollView.ResetPosition();
    this._scrollView.gameObject.SetActive(false);
    this._scrollView.gameObject.SetActive(true);
  }

  private PitExploreRankContainer CreateItemRenderer(List<PitExplorePayload.PitRankDataWithTime> rankInfoList)
  {
    GameObject gameObject = this._itemPool.AddChild(this._grid.gameObject);
    gameObject.SetActive(true);
    PitExploreRankContainer component = gameObject.GetComponent<PitExploreRankContainer>();
    component.SetData(rankInfoList);
    return component;
  }

  public class Parameter : Popup.PopupParameter
  {
    public List<KeyValuePair<int, List<PitExplorePayload.PitRankDataWithTime>>> rankDataList;
  }
}
