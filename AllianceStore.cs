﻿// Decompiled with JetBrains decompiler
// Type: AllianceStore
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class AllianceStore : UI.Dialog
{
  private int m_SelectedIndex = -1;
  public UILabel m_Honor;
  public UIToggle[] m_Toggles;
  public AllianceStoreHonorPanel m_HonorPanel;
  public AllianceStoreFundPanel m_FundPanel;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    ItemBag.Instance.StartSyncTime();
    this.m_SelectedIndex = -1;
    this.m_Toggles[0].Set(true);
    AllianceManager.Instance.onLeave += new System.Action(this.OnLeave);
  }

  private void OnLeave()
  {
    this.m_HonorPanel.Hide();
    this.m_FundPanel.Hide();
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    UIManager.inst.toast.Show("You left the alliance.", (System.Action) null, 4f, false);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
    ItemBag.Instance.StopSyncTime();
    AllianceManager.Instance.onLeave -= new System.Action(this.OnLeave);
  }

  public void OnToggleValueChanged()
  {
    int selectedIndex = this.GetSelectedIndex();
    if (selectedIndex == this.m_SelectedIndex)
      return;
    this.m_SelectedIndex = selectedIndex;
    if (this.m_SelectedIndex == 0)
    {
      this.m_FundPanel.Hide();
      this.m_HonorPanel.Show();
    }
    else
    {
      if (this.m_SelectedIndex != 1)
        return;
      this.m_HonorPanel.Hide();
      this.m_FundPanel.Show();
    }
  }

  private int GetSelectedIndex()
  {
    for (int index = 0; index < this.m_Toggles.Length; ++index)
    {
      if (this.m_Toggles[index].value)
        return index;
    }
    return -1;
  }

  private void ClearData()
  {
    this.m_HonorPanel.Hide();
    this.m_FundPanel.Hide();
  }
}
