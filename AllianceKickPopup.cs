﻿// Decompiled with JetBrains decompiler
// Type: AllianceKickPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class AllianceKickPopup : Popup
{
  private AllianceKickPopup.Parameter m_Parameter;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as AllianceKickPopup.Parameter;
  }

  public void OnNo()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnYes()
  {
    this.OnNo();
    AllianceManager.Instance.Kick(this.m_Parameter.uid, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      ChatMessageManager.SendAllianceMessage(ChatMessageManager.AllianceMessageType.kick, this.m_Parameter.name, string.Empty, string.Empty);
    }));
  }

  public class Parameter : Popup.PopupParameter
  {
    public long uid;
    public string name;
  }
}
