﻿// Decompiled with JetBrains decompiler
// Type: Hero_EquipmentData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class Hero_EquipmentData : IGameData
{
  public static readonly int MAX_MATERIAL_REQUIREMENT = 3;
  public List<Hero_EquipmentData.Benefit> benefits = new List<Hero_EquipmentData.Benefit>();
  public static readonly int InvalidID;
  private int _internalID;
  private string _id;
  public int Essence;
  public int SetInternalID;
  public string LOC_Name_ID;
  public string LOC_Description_ID;
  public string Asset_Filename;
  public string Asset_AlterName;
  public string Prefab1;
  public string Prefab2;

  public int InternalID
  {
    get
    {
      return this._internalID;
    }
    set
    {
      this._internalID = value;
    }
  }

  public string ID
  {
    get
    {
      return this._id;
    }
    set
    {
      this._id = value;
    }
  }

  public string uniqueId
  {
    get
    {
      return this.ID;
    }
    set
    {
      this.ID = value;
    }
  }

  public string Name
  {
    get
    {
      return ScriptLocalization.Get(this.LOC_Name_ID, true);
    }
  }

  public float GetBenefitValueByLevel(int benefitIndex, int level)
  {
    return 0.0f;
  }

  public class Benefit
  {
    public const int MAX_BENEFIT_COUNT = 4;
    public const string BENEFIT_ID_NAME_PRE = "benefit_id_";
    public const string BENEFIT_MIN_NAME_PRE = "benefit_min_";
    public const string BENEFIT_MAX_NAME_PRE = "benefit_max_";
    public int benefitId;
    public float maxValue;
    public float minValue;
  }
}
