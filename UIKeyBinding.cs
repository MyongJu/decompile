﻿// Decompiled with JetBrains decompiler
// Type: UIKeyBinding
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Interaction/Key Binding")]
public class UIKeyBinding : MonoBehaviour
{
  public KeyCode keyCode;
  public UIKeyBinding.Modifier modifier;
  public UIKeyBinding.Action action;
  private bool mIgnoreUp;
  private bool mIsInput;
  private bool mPress;

  protected virtual void Start()
  {
    UIInput component = this.GetComponent<UIInput>();
    this.mIsInput = (Object) component != (Object) null;
    if (!((Object) component != (Object) null))
      return;
    EventDelegate.Add(component.onSubmit, new EventDelegate.Callback(this.OnSubmit));
  }

  protected virtual void OnSubmit()
  {
    if (UICamera.currentKey != this.keyCode || !this.IsModifierActive())
      return;
    this.mIgnoreUp = true;
  }

  protected virtual bool IsModifierActive()
  {
    if (this.modifier == UIKeyBinding.Modifier.None)
      return true;
    if (this.modifier == UIKeyBinding.Modifier.Alt)
    {
      if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
        return true;
    }
    else if (this.modifier == UIKeyBinding.Modifier.Control)
    {
      if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        return true;
    }
    else if (this.modifier == UIKeyBinding.Modifier.Shift && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
      return true;
    return false;
  }

  protected virtual void Update()
  {
    if (UICamera.inputHasFocus || this.keyCode == KeyCode.None || !this.IsModifierActive())
      return;
    bool keyDown = Input.GetKeyDown(this.keyCode);
    bool keyUp = Input.GetKeyUp(this.keyCode);
    if (keyDown)
      this.mPress = true;
    if (this.action == UIKeyBinding.Action.PressAndClick || this.action == UIKeyBinding.Action.All)
    {
      UICamera.currentTouch = UICamera.controller;
      UICamera.currentScheme = UICamera.ControlScheme.Mouse;
      UICamera.currentTouch.current = this.gameObject;
      if (keyDown)
        this.OnBindingPress(true);
      if (this.mPress && keyUp)
      {
        this.OnBindingPress(false);
        this.OnBindingClick();
      }
      UICamera.currentTouch.current = (GameObject) null;
    }
    if ((this.action == UIKeyBinding.Action.Select || this.action == UIKeyBinding.Action.All) && keyUp)
    {
      if (this.mIsInput)
      {
        if (!this.mIgnoreUp && !UICamera.inputHasFocus && this.mPress)
          UICamera.selectedObject = this.gameObject;
        this.mIgnoreUp = false;
      }
      else if (this.mPress)
        UICamera.selectedObject = this.gameObject;
    }
    if (!keyUp)
      return;
    this.mPress = false;
  }

  protected virtual void OnBindingPress(bool pressed)
  {
    UICamera.Notify(this.gameObject, "OnPress", (object) pressed);
  }

  protected virtual void OnBindingClick()
  {
    UICamera.Notify(this.gameObject, "OnClick", (object) null);
  }

  public enum Action
  {
    PressAndClick,
    Select,
    All,
  }

  public enum Modifier
  {
    None,
    Shift,
    Control,
    Alt,
  }
}
