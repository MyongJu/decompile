﻿// Decompiled with JetBrains decompiler
// Type: TroopsLevelUpDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class TroopsLevelUpDialog : UI.Dialog
{
  private float _fadeInTime = 0.5f;
  private float _fadeOutTime = 0.5f;
  private int MAX_TRAIN_NUM = int.MaxValue;
  private Hashtable _itemHT = new Hashtable();
  private Dictionary<string, int> _lackitem = new Dictionary<string, int>(1);
  private Dictionary<string, long> _lackRes = new Dictionary<string, long>(4);
  private ProgressBarTweenProxy proxy = new ProgressBarTweenProxy();
  private Color _normalColor = new Color(229f, 183f, 8f);
  private List<string> _unitTextureList = new List<string>();
  private bool autoFix = true;
  private const float UNIT_IMAGE_SHOWN_TIME = 2f;
  [SerializeField]
  private UILabel _labelCurrentLevelCount;
  [SerializeField]
  private UILabel _labelNextLevelCount;
  [SerializeField]
  private UILabel _titleLabel;
  [SerializeField]
  private UILabel _subTitleLabel;
  [SerializeField]
  private UILabel _unitDescription;
  [SerializeField]
  private UnitRender _currentUnit;
  [SerializeField]
  private UnitRender _targetUnit;
  [SerializeField]
  private BarracksResourcesComponent _resourceCostComponent;
  [SerializeField]
  private UILabel _labelInstantCost;
  [SerializeField]
  private UILabel _labelTimeCost;
  [SerializeField]
  private UILabel _labelTop;
  [SerializeField]
  private UILabel _inputLabel;
  [SerializeField]
  private UILabel _trianLabel;
  [SerializeField]
  private UILabel _instanceTrianLabel;
  [SerializeField]
  private UIButton _buttonInstant;
  [SerializeField]
  private UIButton _buttonTrain;
  [SerializeField]
  private UIInput _inputSelectCount;
  [SerializeField]
  private UISlider trainSlider;
  [SerializeField]
  private UITexture _unitTexture;
  private TroopsLevelUpDialog.Parameter _parameter;
  private Unit_StatisticsInfo _targetUnitStaticsInfo;
  private Unit_StatisticsInfo _currentUnitStaticsInfo;
  private int _currentLevelCount;
  private int _nextLevelCount;
  private int _troopCount;
  private int _resourceGold;
  private int _instTrainCost;
  private float _fullTime;
  private bool _instantTrain;
  private long _buildingID;
  private BuildingInfo _buildingInfo;
  private Coroutine _unitSwitchCoroutine;

  public override void OnOpen(UIControler.UIParameter param)
  {
    base.OnOpen(param);
    this._parameter = param as TroopsLevelUpDialog.Parameter;
    this._currentUnitStaticsInfo = this._parameter.UnitInfo;
    this._buildingInfo = this._parameter.buildingInfo;
    this._buildingID = this._parameter.BuildingId;
    this._targetUnitStaticsInfo = ConfigManager.inst.DB_Unit_Statistics.GetData(this._currentUnitStaticsInfo.Type, this._currentUnitStaticsInfo.Troop_Tier + 1);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.AddEventListener();
    this.InitUnitTextureList();
    this.UpdateUnitDetails();
    this.UpdateMaxTrainNum();
    this.UpdateUI();
  }

  private void AddEventListener()
  {
    UISlider trainSlider = this.trainSlider;
    trainSlider.onDragFinished = trainSlider.onDragFinished + new UIProgressBar.OnDragFinished(this.OnSliderDragFinish);
    DBManager.inst.DB_City.onDataChanged += new System.Action<long>(this.RefreshTroop);
    DBManager.inst.DB_City.onDataRemoved += new System.Action(this.RefreshTroop);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventListener();
    this.ClearData();
    this.StopTextureSwitchCoroutine();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  private void ClearData()
  {
    this._unitTextureList.Clear();
    this._itemHT.Clear();
    this._lackitem.Clear();
    this._lackRes.Clear();
  }

  private void RemoveEventListener()
  {
    UISlider trainSlider = this.trainSlider;
    trainSlider.onDragFinished = trainSlider.onDragFinished - new UIProgressBar.OnDragFinished(this.OnSliderDragFinish);
    DBManager.inst.DB_City.onDataChanged -= new System.Action<long>(this.RefreshTroop);
    DBManager.inst.DB_City.onDataRemoved -= new System.Action(this.RefreshTroop);
  }

  private int MaxSelectCount
  {
    get
    {
      return Mathf.Min(this._currentLevelCount, this._currentUnitStaticsInfo.GetTrainLimit());
    }
  }

  private void RefreshTroop(long cityID)
  {
    this.UpdateUnitDetails();
    this.UpdateMaxCount();
  }

  private void RefreshTroop()
  {
    this.UpdateUnitDetails();
    this.UpdateMaxCount();
  }

  protected void UpdateUI()
  {
    this.UpdateUnitDetails();
    this.UpdateUnitTexture();
    this.UpdateTitleTexts();
    this.UpdateMaxCount();
    this.UpdateCostUI();
  }

  private void UpdateUnitDetails()
  {
    this._currentLevelCount = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).cityTroops.GetTroopsCount(this._currentUnitStaticsInfo.ID);
    this._labelCurrentLevelCount.text = Utils.XLAT("id_stronghold_requirements") + Utils.FormatThousands(this._currentLevelCount.ToString());
    this._currentUnit.UpdateUI(this._currentUnitStaticsInfo, this._parameter.BuildingId, (System.Action<UnitRender>) null, (UIScrollView) null);
    if (this._targetUnitStaticsInfo == null)
      return;
    this._nextLevelCount = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).totalTroops.GetTroopsCount(this._targetUnitStaticsInfo.ID);
    this._labelNextLevelCount.text = Utils.XLAT("barracks_own") + ":  " + Utils.FormatThousands(this._nextLevelCount.ToString());
    this._targetUnit.UpdateUI(this._targetUnitStaticsInfo, this._parameter.BuildingId, (System.Action<UnitRender>) null, (UIScrollView) null);
  }

  private void InitUnitTextureList()
  {
    this._unitTextureList.Add(this._currentUnitStaticsInfo.Troop_IMAGE);
    this._unitTextureList.Add(this._targetUnitStaticsInfo.Troop_IMAGE);
  }

  protected void OnSelectChanged()
  {
    this.UpdateCostUI();
  }

  private void UpdateUnitTexture()
  {
    if (this._unitSwitchCoroutine != null)
      return;
    this._unitSwitchCoroutine = this.StartCoroutine(this.TextureSwitchCoroutine());
  }

  [DebuggerHidden]
  private IEnumerator TextureSwitchCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TroopsLevelUpDialog.\u003CTextureSwitchCoroutine\u003Ec__Iterator2D()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void StopTextureSwitchCoroutine()
  {
    if (this._unitSwitchCoroutine == null)
      return;
    Utils.StopCoroutine(this._unitSwitchCoroutine);
    this._unitSwitchCoroutine = (Coroutine) null;
  }

  private void UpdateMaxTrainNum()
  {
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    this.MAX_TRAIN_NUM = int.MaxValue;
    long currentResource1 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD);
    long currentResource2 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
    long currentResource3 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD);
    long currentResource4 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE);
    float num1 = this.UpgradeCostFood(this._targetUnitStaticsInfo, 1) - this.UpgradeCostFood(this._currentUnitStaticsInfo, 1);
    float num2 = this.UpgradeCostSilver(this._targetUnitStaticsInfo, 1) - this.UpgradeCostSilver(this._currentUnitStaticsInfo, 1);
    float num3 = this.UpgradeCostWood(this._targetUnitStaticsInfo, 1) - this.UpgradeCostWood(this._currentUnitStaticsInfo, 1);
    float num4 = this.UpgradeCostOre(this._targetUnitStaticsInfo, 1) - this.UpgradeCostOre(this._currentUnitStaticsInfo, 1);
    float f1 = (double) num1 <= 0.0 ? 0.0f : (float) currentResource3 / num1;
    float f2 = (double) num2 <= 0.0 ? 0.0f : (float) currentResource2 / num2;
    float f3 = (double) num3 <= 0.0 ? 0.0f : (float) currentResource1 / num3;
    float f4 = (double) num4 <= 0.0 ? 0.0f : (float) currentResource4 / num4;
    if ((double) num1 > 0.0 && (double) f1 < (double) this.MAX_TRAIN_NUM)
      this.MAX_TRAIN_NUM = Mathf.FloorToInt(f1);
    if ((double) num2 > 0.0 && (double) f2 < (double) this.MAX_TRAIN_NUM)
      this.MAX_TRAIN_NUM = Mathf.FloorToInt(f2);
    if ((double) num3 > 0.0 && (double) f3 < (double) this.MAX_TRAIN_NUM)
      this.MAX_TRAIN_NUM = Mathf.FloorToInt(f3);
    if ((double) num4 > 0.0 && (double) f4 < (double) this.MAX_TRAIN_NUM)
      this.MAX_TRAIN_NUM = Mathf.FloorToInt(f4);
    if (this.MAX_TRAIN_NUM < 1)
      this.MAX_TRAIN_NUM = 0;
    this.MAX_TRAIN_NUM = Mathf.Max(this.MAX_TRAIN_NUM, 1);
    if (this.MAX_TRAIN_NUM > this.MaxSelectCount)
      this._troopCount = this.MaxSelectCount;
    else
      this._troopCount = this.MAX_TRAIN_NUM;
  }

  protected void UpdateTitleTexts()
  {
    this._titleLabel.text = Utils.XLAT(this._buildingInfo.Building_LOC_ID);
    this._subTitleLabel.text = Utils.XLAT("barracks_upgrade_troop_level");
    this._unitDescription.text = Utils.XLAT("barracks_upgrade_troop_description");
  }

  protected void UpdateCostUI()
  {
    this.UpdateTroopsCount();
    this.UpdataSliderTexts();
    this.UpdateResourceCostUI();
    this.UpdateTimeCostUI();
    this.UpdateGoldCostUI();
    this.UpdateButtonLabels();
    this.UpdateButtonState();
  }

  protected void UpdateResourceCostUI()
  {
    float num1 = this.UpgradeCostWood(this._currentUnitStaticsInfo, this._troopCount);
    float num2 = this.UpgradeCostSilver(this._currentUnitStaticsInfo, this._troopCount);
    float num3 = this.UpgradeCostFood(this._currentUnitStaticsInfo, this._troopCount);
    float num4 = this.UpgradeCostOre(this._currentUnitStaticsInfo, this._troopCount);
    float num5 = this.UpgradeCostWood(this._targetUnitStaticsInfo, this._troopCount);
    float num6 = this.UpgradeCostSilver(this._targetUnitStaticsInfo, this._troopCount);
    float num7 = this.UpgradeCostFood(this._targetUnitStaticsInfo, this._troopCount);
    float num8 = this.UpgradeCostOre(this._targetUnitStaticsInfo, this._troopCount);
    float wood = num5 - num1;
    float silver = num6 - num2;
    float food = num7 - num3;
    float ore = num8 - num4;
    if ((double) wood < 0.0)
      wood = 0.0f;
    if ((double) silver < 0.0)
      silver = 0.0f;
    if ((double) food < 0.0)
      food = 0.0f;
    if ((double) ore < 0.0)
      ore = 0.0f;
    this._resourceCostComponent.UpdateCostResources(wood, silver, food, ore, true);
  }

  private float UpgradeCostWood(Unit_StatisticsInfo Unit, int count)
  {
    return Unit.BenefitCostWood(count);
  }

  private float UpgradeCostSilver(Unit_StatisticsInfo Unit, int count)
  {
    return Unit.BenefitCostSilver(count);
  }

  private float UpgradeCostFood(Unit_StatisticsInfo Unit, int count)
  {
    return Unit.BenefitCostFood(count);
  }

  private float UpgradeCostOre(Unit_StatisticsInfo Unit, int count)
  {
    return Unit.BenefitCostOre(count);
  }

  protected void UpdateTimeCostUI()
  {
    float baseValue = (float) (this._troopCount * (this._targetUnitStaticsInfo.Training_Time - this._currentUnitStaticsInfo.Training_Time));
    if ((double) baseValue < 0.0)
      baseValue = 0.0f;
    float finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(baseValue, this._currentUnitStaticsInfo.TRAIN_TIME_CAL_KEY);
    this._fullTime = finalData;
    this._labelTimeCost.text = Utils.ConvertSecsToString((double) (int) finalData);
  }

  protected void UpdateGoldCostUI()
  {
    this._itemHT.Clear();
    this._itemHT.Add((object) "time", (object) (int) this._fullTime);
    Hashtable hashtable = new Hashtable();
    long num1 = (long) this._currentUnitStaticsInfo.BenefitCostFood(this._troopCount);
    long num2 = (long) this._currentUnitStaticsInfo.BenefitCostWood(this._troopCount);
    long num3 = (long) this._currentUnitStaticsInfo.BenefitCostSilver(this._troopCount);
    long num4 = (long) this._currentUnitStaticsInfo.BenefitCostOre(this._troopCount);
    long num5 = (long) this._targetUnitStaticsInfo.BenefitCostFood(this._troopCount);
    long num6 = (long) this._targetUnitStaticsInfo.BenefitCostWood(this._troopCount);
    long num7 = (long) this._targetUnitStaticsInfo.BenefitCostSilver(this._troopCount);
    long num8 = (long) this._targetUnitStaticsInfo.BenefitCostOre(this._troopCount);
    hashtable.Add((object) ItemBag.ItemType.food, (object) (num5 - num1));
    hashtable.Add((object) ItemBag.ItemType.wood, (object) (num6 - num2));
    hashtable.Add((object) ItemBag.ItemType.silver, (object) (num7 - num3));
    hashtable.Add((object) ItemBag.ItemType.ore, (object) (num8 - num4));
    this._itemHT.Add((object) "resources", (object) hashtable);
    this._lackRes = ItemBag.CalculateLackResources(this._itemHT);
    this._lackitem = ItemBag.CalculateLackItems(this._itemHT);
    this._instTrainCost = ItemBag.CalculateCost(this._itemHT);
    this._itemHT.Remove((object) "time");
    this._resourceGold = ItemBag.CalculateCost(this._itemHT);
    this._labelInstantCost.text = Utils.FormatThousands(this._instTrainCost.ToString());
    if (GameEngine.Instance.PlayerData.hostPlayer.Currency >= this._instTrainCost)
      this._labelInstantCost.color = this._normalColor;
    else
      this._labelInstantCost.color = Color.red;
  }

  private void UpdateButtonLabels()
  {
    this._trianLabel.text = Utils.XLAT("id_uppercase_level_up");
    this._instanceTrianLabel.text = Utils.XLAT("id_uppercase_instant_train");
  }

  protected void UpdateButtonState()
  {
    if (this._troopCount == 0)
    {
      this._buttonInstant.isEnabled = false;
      this._buttonTrain.isEnabled = false;
    }
    else
    {
      this._buttonInstant.isEnabled = true;
      this._buttonTrain.isEnabled = true;
    }
  }

  private void UpdateTroopsCount()
  {
    if (!this._instantTrain)
      return;
    if (this._troopCount > this._currentLevelCount)
    {
      this._troopCount = this._currentLevelCount;
      this.autoFix = true;
    }
    else
    {
      if (this.MaxSelectCount == 0)
        this.trainSlider.value = 0.0f;
      else
        this.trainSlider.value = (float) this._troopCount / (float) this.MaxSelectCount;
      this.autoFix = false;
    }
    this._instantTrain = false;
  }

  protected void UpdataSliderTexts()
  {
    this._inputLabel.text = this._troopCount.ToString();
    this._inputSelectCount.value = this._troopCount.ToString();
    if (this._currentLevelCount != 0)
      return;
    this.DisableSlider();
  }

  protected void UpdateMaxCount()
  {
    this._labelTop.text = string.Format("/{0}", (object) this.MaxSelectCount.ToString());
  }

  public void OnButtonInstantClicked()
  {
    this._instantTrain = true;
    if (PlayerData.inst.hostPlayer.Currency < this._instTrainCost)
    {
      Utils.ShowNotEnoughGoldTip();
    }
    else
    {
      AudioManager.Instance.PlaySound("sfx_troop_training_start", false);
      this._buttonInstant.isEnabled = false;
      MessageHub.inst.GetPortByAction("City:upgradeTroop").SendRequest(Utils.Hash((object) "building_id", (object) this._buildingID, (object) "class", (object) this._currentUnitStaticsInfo.ID, (object) "count", (object) this._troopCount, (object) "instant", (object) this._instantTrain, (object) "gold", (object) this._instTrainCost), (System.Action<bool, object>) ((bSuccess, data) =>
      {
        if (!bSuccess)
          return;
        BarracksManager.Instance.CollectBuilding(this._buildingID, (System.Action) (() => this.UpdateUI()));
      }), true);
    }
  }

  public void OnButtonTrainClicked()
  {
    this._instantTrain = false;
    AudioManager.Instance.PlaySound("sfx_troop_training_start", false);
    if (this._lackRes.Count > 0 || this._lackitem.Count > 0)
      UIManager.inst.OpenPopup("BuildingResPopUp", (Popup.PopupParameter) new BuildingResPopUp.Parameter()
      {
        lackItem = this._lackitem,
        lackRes = this._lackRes,
        requireRes = this._itemHT,
        action = ScriptLocalization.Get("id_uppercase_level_up", true),
        gold = this._resourceGold,
        buyResourCallBack = new System.Action<int>(this.SendTrainRequest),
        title = ScriptLocalization.Get("train_notenoughresource_title", true),
        content = ScriptLocalization.Get("upgrade_notenoughresource_description", true)
      });
    else
      this.SendTrainRequest(0);
  }

  private void SendTrainRequest(int gold = 0)
  {
    if (PlayerData.inst.hostPlayer.Currency < gold)
      Utils.ShowNotEnoughGoldTip();
    else
      MessageHub.inst.GetPortByAction("City:upgradeTroop").SendRequest(Utils.Hash((object) "building_id", (object) this._buildingID, (object) "class", (object) this._currentUnitStaticsInfo.ID, (object) "count", (object) this._troopCount, (object) "instant", (object) this._instantTrain, (object) nameof (gold), (object) gold), (System.Action<bool, object>) ((bSuccess, data) =>
      {
        if (!bSuccess)
          return;
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      }), true);
  }

  public void OnButtonCurrentUnitDetailClicked()
  {
    if (this._currentUnitStaticsInfo == null)
      return;
    UIManager.inst.OpenPopup("BarracksMoreInfoPopup", (Popup.PopupParameter) new BarracksMoreInfoPopup.Parameter()
    {
      TSC = this._currentUnitStaticsInfo,
      buildingID = this._parameter.BuildingId,
      onClose = new System.Action(this.OnBarracksMoreInfoPopuoClose)
    });
  }

  public void OnButtonTargetUnitDetailClicked()
  {
    if (this._targetUnitStaticsInfo == null)
      return;
    UIManager.inst.OpenPopup("BarracksMoreInfoPopup", (Popup.PopupParameter) new BarracksMoreInfoPopup.Parameter()
    {
      TSC = this._targetUnitStaticsInfo,
      buildingID = this._parameter.BuildingId,
      onClose = new System.Action(this.OnBarracksMoreInfoPopuoClose)
    });
  }

  protected void OnBarracksMoreInfoPopuoClose()
  {
    this.UpdateUI();
  }

  public void OnTroopCountInputChange()
  {
    int num = 0;
    try
    {
      num = Convert.ToInt32(this._inputSelectCount.value);
    }
    catch (FormatException ex)
    {
      num = 0;
    }
    catch (OverflowException ex)
    {
      num = this.MaxSelectCount;
    }
    finally
    {
      this._troopCount = Mathf.Clamp(num, 0, this.MaxSelectCount);
      this._inputSelectCount.value = this._troopCount.ToString();
      if (this.autoFix)
      {
        if (this.MaxSelectCount == 0)
          this.trainSlider.value = 0.0f;
        else
          this.trainSlider.value = (float) this._troopCount / (float) this.MaxSelectCount;
      }
      this.UpdateCostUI();
    }
  }

  private void OnSliderDragFinish()
  {
    this.autoFix = true;
  }

  public void OnSliderValueChanged()
  {
    if (Mathf.RoundToInt((float) this.MaxSelectCount * this.trainSlider.value) == this._troopCount)
      return;
    this._troopCount = Mathf.Clamp(Mathf.RoundToInt((float) this.MaxSelectCount * this.trainSlider.value), 0, this.MaxSelectCount);
    if (this.autoFix)
      this.autoFix = !this.autoFix;
    this.UpdateCostUI();
  }

  public void OnSliderSubtractBtnDown()
  {
    this.autoFix = false;
    this.proxy.MaxSize = this.MaxSelectCount;
    this.proxy.Slider = (UIProgressBar) this.trainSlider;
    this.proxy.Play(-1, -1f);
  }

  public void OnSliderSubtractBtnUp()
  {
    this.autoFix = true;
    this.proxy.Stop();
  }

  public void OnSliderPlusBtnDown()
  {
    if (this._currentLevelCount == 0)
      return;
    this.autoFix = false;
    this.proxy.MaxSize = this.MaxSelectCount;
    this.proxy.Slider = (UIProgressBar) this.trainSlider;
    this.proxy.Play(1, -1f);
  }

  public void OnSliderPlusBtnUp()
  {
    if (this._currentLevelCount == 0)
      return;
    this.autoFix = true;
    this.proxy.Stop();
  }

  private void DisableSlider()
  {
    foreach (Collider componentsInChild in this.trainSlider.GetComponentsInChildren<Collider>())
      componentsInChild.enabled = false;
    this.trainSlider.GetComponent<UIButton>().enabled = false;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public long BuildingId;
    public BuildingInfo buildingInfo;
    public Unit_StatisticsInfo UnitInfo;
  }
}
