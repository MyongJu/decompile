﻿// Decompiled with JetBrains decompiler
// Type: ConfigMerlinTrialsRewards
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigMerlinTrialsRewards
{
  private List<MerlinTrialsRewardsInfo> _merlinTrialsRewardsInfoList = new List<MerlinTrialsRewardsInfo>();
  private Dictionary<string, MerlinTrialsRewardsInfo> _datas;
  private Dictionary<int, MerlinTrialsRewardsInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<MerlinTrialsRewardsInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, MerlinTrialsRewardsInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._merlinTrialsRewardsInfoList.Add(enumerator.Current);
    }
  }

  public void Clear()
  {
    if (this._datas != null)
      this._datas.Clear();
    if (this._dicByUniqueId != null)
      this._dicByUniqueId.Clear();
    if (this._merlinTrialsRewardsInfoList == null)
      return;
    this._merlinTrialsRewardsInfoList.Clear();
  }

  public List<MerlinTrialsRewardsInfo> GetInfoList()
  {
    return this._merlinTrialsRewardsInfoList;
  }

  public MerlinTrialsRewardsInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (MerlinTrialsRewardsInfo) null;
  }

  public MerlinTrialsRewardsInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (MerlinTrialsRewardsInfo) null;
  }
}
