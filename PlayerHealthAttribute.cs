﻿// Decompiled with JetBrains decompiler
// Type: PlayerHealthAttribute
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class PlayerHealthAttribute : RoundPlayerAttribute
{
  public PlayerHealthAttribute(RoundPlayer player)
    : base(player)
  {
  }

  protected override DragonKnightAttibute.Type Type
  {
    get
    {
      return DragonKnightAttibute.Type.Health;
    }
  }

  public override void Reset()
  {
    int result = Mathf.CeilToInt((float) this.InBattleBaseValue + this.GetInBattleAttibutePlus((float) this.Owner.Constitution, DragonKnightAttibute.Type.Constitution));
    this.CurrentValue = this.BuffFactor(result);
    if (this.MaxValue > 0)
      return;
    this.MaxValue = result;
  }

  protected override bool AutoFix
  {
    get
    {
      return true;
    }
  }

  public override int OutBattleBaseValue
  {
    get
    {
      int constitution = this.Owner.Constitution;
      bool flag = this.Owner.PlayerCamp == RoundPlayer.Camp.Evil;
      this.Owner.GetAllBuffs();
      return this.AddPercent((float) this.BaseValue + this.GetOutBattleAttibutePlus((float) constitution, DragonKnightAttibute.Type.Constitution), DragonKnightAttibute.Type.Health);
    }
  }
}
