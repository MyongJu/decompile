﻿// Decompiled with JetBrains decompiler
// Type: DiceScoreBoxInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class DiceScoreBoxInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "level")]
  public int level;
  [Config(Name = "score")]
  public int score;
  [Config(Name = "rewards_1")]
  public int rewardId1;
  [Config(Name = "rewards_value_1")]
  public int rewardValue1;
  [Config(Name = "rewards_2")]
  public int rewardId2;
  [Config(Name = "rewards_value_2")]
  public int rewardValue2;
  [Config(Name = "rewards_3")]
  public int rewardId3;
  [Config(Name = "rewards_value_3")]
  public int rewardValue3;
  [Config(Name = "rewards_4")]
  public int rewardId4;
  [Config(Name = "rewards_value_4")]
  public int rewardValue4;
  [Config(Name = "rewards_5")]
  public int rewardId5;
  [Config(Name = "rewards_value_5")]
  public int rewardValue5;
  private List<Reward.RewardsValuePair> _rewards;

  public List<Reward.RewardsValuePair> Rewards
  {
    get
    {
      if (this._rewards == null)
      {
        this._rewards = new List<Reward.RewardsValuePair>();
        int[] numArray1 = new int[5]
        {
          this.rewardId1,
          this.rewardId2,
          this.rewardId3,
          this.rewardId4,
          this.rewardId5
        };
        int[] numArray2 = new int[5]
        {
          this.rewardValue1,
          this.rewardValue2,
          this.rewardValue3,
          this.rewardValue4,
          this.rewardValue5
        };
        for (int index = 0; index < numArray1.Length; ++index)
          this._rewards.Add(new Reward.RewardsValuePair(numArray1[index], numArray2[index]));
      }
      return this._rewards;
    }
  }
}
