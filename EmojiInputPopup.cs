﻿// Decompiled with JetBrains decompiler
// Type: EmojiInputPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class EmojiInputPopup : Popup
{
  private List<EmojiPage> pages = new List<EmojiPage>();
  public const string POPUP_TYPE = "Emoji/EmojiInputPopup";
  public UIScrollView scrollView;
  public UIGrid grid;
  public EmojiPage pageTemplate;
  public EmojiIndex emojiIndex;
  public UICenterOnChild centerOnChild;
  public PageTabsMonitor pageTabsMonitor;
  private bool showNormal;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.Init();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  public void OnNormalClick()
  {
    this.pageTabsMonitor.SetCurrentTab(0, true);
  }

  public void OnFavClick()
  {
    this.pageTabsMonitor.SetCurrentTab(1, true);
  }

  private void Init()
  {
    this.grid.onReposition = (UIGrid.OnReposition) (() => this.scrollView.ResetPosition());
    this.pageTabsMonitor.onTabSelected += (System.Action<int>) (obj =>
    {
      if (obj == 0)
      {
        this.showNormal = true;
        this.DrawNormal();
      }
      else
      {
        this.showNormal = false;
        this.DrawFavor();
      }
      this.emojiIndex.gameObject.SetActive(this.showNormal);
    });
    this.pageTabsMonitor.SetCurrentTab(0, true);
  }

  private void DrawNormal()
  {
    List<Emoji> emojiListByGroup = EmojiManager.Instance.GetEmojiListByGroup(0);
    this.pages.ForEach((System.Action<EmojiPage>) (obj => obj.gameObject.SetActive(false)));
    int maxCount = this.pageTemplate.MAXCount;
    int count = Mathf.CeilToInt((float) emojiListByGroup.Count / (float) maxCount);
    for (int index = 0; index < count; ++index)
    {
      EmojiPage emojiPage;
      if (index < this.pages.Count)
      {
        emojiPage = this.pages[index];
      }
      else
      {
        emojiPage = NGUITools.AddChild(this.grid.gameObject, this.pageTemplate.gameObject).GetComponent<EmojiPage>();
        this.pages.Add(emojiPage);
      }
      emojiPage.gameObject.name = index.ToString();
      emojiPage.gameObject.SetActive(true);
      emojiPage.FeedContent(emojiListByGroup.GetRange(index * this.pageTemplate.MAXCount, Mathf.Min(emojiListByGroup.Count - index * maxCount, maxCount)));
    }
    this.emojiIndex.Init(count);
    this.centerOnChild.onCenter = (UICenterOnChild.OnCenterCallback) (centeredObject => this.emojiIndex.Index = int.Parse(centeredObject.name));
    this.centerOnChild.CenterOn(this.pages[0].transform);
    this.grid.repositionNow = true;
    this.emojiIndex.Index = 0;
  }

  private void DrawFavor()
  {
    List<Emoji> favorEmojiList = EmojiManager.Instance.GetFavorEmojiList();
    int maxCount = this.pageTemplate.MAXCount;
    this.pages.ForEach((System.Action<EmojiPage>) (obj => obj.gameObject.SetActive(false)));
    for (int index = 0; index < 1; ++index)
    {
      EmojiPage emojiPage;
      if (index < this.pages.Count)
      {
        emojiPage = this.pages[index];
        emojiPage.name = index.ToString();
      }
      else
      {
        emojiPage = NGUITools.AddChild(this.grid.gameObject, this.pageTemplate.gameObject).GetComponent<EmojiPage>();
        this.pages.Add(emojiPage);
      }
      emojiPage.gameObject.SetActive(true);
      emojiPage.FeedContent(favorEmojiList.GetRange(index * this.pageTemplate.MAXCount, Mathf.Min(favorEmojiList.Count - index * maxCount, maxCount)));
    }
    this.grid.repositionNow = true;
  }

  public class Parameter : Popup.PopupParameter
  {
  }
}
