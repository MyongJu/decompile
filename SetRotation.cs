﻿// Decompiled with JetBrains decompiler
// Type: SetRotation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SetRotation : MonoBehaviour
{
  public bool SetX;
  public float X;
  public bool SetY;
  public float Y;
  public bool SetZ;
  public float Z;

  private void LateUpdate()
  {
    Vector3 eulerAngles = this.transform.eulerAngles;
    if (this.SetX)
      eulerAngles.x = this.X;
    if (this.SetY)
      eulerAngles.y = this.Y;
    if (this.SetZ)
      eulerAngles.z = this.Z;
    this.transform.eulerAngles = eulerAngles;
  }
}
