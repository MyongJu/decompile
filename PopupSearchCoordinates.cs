﻿// Decompiled with JetBrains decompiler
// Type: PopupSearchCoordinates
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;

public class PopupSearchCoordinates : UI.Dialog
{
  public UIInput KInput;
  public UIInput XInput;
  public UIInput YInput;
  public UIButton GotoButton;
  private static Coordinate m_Location;

  public static void Init()
  {
    PopupSearchCoordinates.m_Location = PlayerData.inst.playerCityData.cityLocation;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.XInput.value = PopupSearchCoordinates.m_Location.X.ToString();
    this.YInput.value = PopupSearchCoordinates.m_Location.Y.ToString();
    this.KInput.value = PopupSearchCoordinates.m_Location.K.ToString();
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PitMode)
      return;
    this.KInput.value = string.Empty;
    PopupSearchCoordinates.m_Location.K = this.CurrentK;
  }

  public void OnCloseButtonPressed()
  {
    this.Close();
  }

  public void OnBackButtonPressed()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnKInputChanged()
  {
    PopupSearchCoordinates.m_Location.K = 0;
    int.TryParse(this.KInput.value, out PopupSearchCoordinates.m_Location.K);
    PopupSearchCoordinates.m_Location.K = Math.Max(Math.Min(PopupSearchCoordinates.m_Location.K, PVPMapData.KingdomCount), 0);
    this.KInput.value = PopupSearchCoordinates.m_Location.K != 0 ? PopupSearchCoordinates.m_Location.K.ToString() : string.Empty;
    if (PopupSearchCoordinates.m_Location.K == 0)
      PopupSearchCoordinates.m_Location.K = this.CurrentK;
    this.OnXInputChanged();
    this.OnYInputChanged();
  }

  public void OnXInputChanged()
  {
    int.TryParse(this.XInput.value, out PopupSearchCoordinates.m_Location.X);
    PopupSearchCoordinates.m_Location.X = Math.Max(Math.Min(PopupSearchCoordinates.m_Location.X, this.GetTilesPerKingdomX(PopupSearchCoordinates.m_Location.K) - 1), 0);
    this.XInput.value = PopupSearchCoordinates.m_Location.X.ToString();
  }

  public void OnYInputChanged()
  {
    int.TryParse(this.YInput.value, out PopupSearchCoordinates.m_Location.Y);
    PopupSearchCoordinates.m_Location.Y = Math.Max(Math.Min(PopupSearchCoordinates.m_Location.Y, this.GetTilesPerKingdomY(PopupSearchCoordinates.m_Location.K) - 1), 0);
    this.YInput.value = PopupSearchCoordinates.m_Location.Y.ToString();
  }

  public void OnGotoButtonPressed()
  {
    this.Close();
    if (string.IsNullOrEmpty(this.KInput.value))
      PopupSearchCoordinates.m_Location.K = this.CurrentK;
    PopupSearchCoordinates.m_Location.K = Math.Max(PopupSearchCoordinates.m_Location.K, 1);
    PVPSystem.Instance.Map.GotoLocation(PopupSearchCoordinates.m_Location, false);
  }

  private void Close()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public int CurrentK
  {
    get
    {
      return PVPSystem.Instance.Map.CurrentKXY.K;
    }
  }

  public int GetTilesPerKingdomX(int k)
  {
    if (MapUtils.IsPitWorld(k))
      return (int) PitMapData.MapData.TilesPerKingdom.x;
    return (int) PVPMapData.MapData.TilesPerKingdom.x;
  }

  public int GetTilesPerKingdomY(int k)
  {
    if (MapUtils.IsPitWorld(k))
      return (int) PitMapData.MapData.TilesPerKingdom.y;
    return (int) PVPMapData.MapData.TilesPerKingdom.y;
  }
}
