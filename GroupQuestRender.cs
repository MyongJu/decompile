﻿// Decompiled with JetBrains decompiler
// Type: GroupQuestRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class GroupQuestRender : MonoBehaviour
{
  public UIToggle toggle;
  public UILabel contentLabel;
  public UIEventTrigger trigger;
  private QuestData2 quest;
  private BasePresent present;

  public void UpdateUI(QuestData2 quest)
  {
    this.quest = quest;
    quest.Present.Refresh();
    this.contentLabel.text = Utils.XLAT(quest.Info.Loc_Name);
    this.toggle.Set(quest.Status == 1);
    if (quest.Status == 1)
    {
      this.trigger.enabled = false;
    }
    else
    {
      this.trigger.enabled = true;
      this.trigger.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnClick)));
    }
  }

  public void OnClick()
  {
    QuestLinkHUDInfo questLinkHudInfo = ConfigManager.inst.DB_QuestLinkHud.GetQuestLinkHudInfo(this.quest.Info.LinkHudId);
    if (questLinkHudInfo != null)
      LinkerHub.Instance.Distribute(questLinkHudInfo.Param);
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    UIManager.inst.publicHUD.mGroupQuestBtn.ShowTip(this.quest);
  }
}
