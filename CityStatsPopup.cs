﻿// Decompiled with JetBrains decompiler
// Type: CityStatsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class CityStatsPopup : Popup
{
  private GameObjectPool pools = new GameObjectPool();
  private List<CityResouceBuildingItem> items = new List<CityResouceBuildingItem>();
  private Dictionary<CityManager.BuildingItem, ResouceGenSpeedData> data2Building = new Dictionary<CityManager.BuildingItem, ResouceGenSpeedData>();
  public UILabel title;
  public UIScrollView scrollView;
  public UIGrid grid;
  public CityResouceBuildingItem itemPrefab;
  public UISprite bacgkround;
  public UIButtonBar buttonBar;
  public GameObject itemRoot;
  private bool init;
  public UILabel baseValueLabel;
  public UILabel benefitValueLabel;
  public UILabel upkeepValueLabel;
  public UILabel vipValueLabel;
  public UILabel researchValueLabel;
  public UILabel telentValueLabel;
  public UILabel itemValueLabel;
  public UILabel equipValueLabel;
  public UILabel fortressValueLabel;
  public UILabel heroCardsValueLabel;
  private int minSize;
  public GameObject upkeepGo;
  public UITable tabel;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.Init();
    this.minSize = this.bacgkround.height;
    this.buttonBar.SelectedIndex = 0;
  }

  private void Init()
  {
    if (this.init)
      return;
    this.init = true;
    this.pools.Initialize(this.itemPrefab.gameObject, this.itemRoot);
    this.buttonBar.OnSelectedHandler = new System.Action<int>(this.OnButtonBarChangeHandler);
  }

  private void OnButtonBarChangeHandler(int index)
  {
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Clear();
    this.init = false;
    this.pools.Clear();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.title.text = ScriptLocalization.Get("city_stats_uppercase_title", true);
    this.Clear();
    this.bacgkround.gameObject.SetActive(false);
    List<CityManager.BuildingItem> data = this.GetData();
    this.scrollView.gameObject.SetActive(false);
    if (data != null)
    {
      for (int index = 0; index < data.Count; ++index)
      {
        CityResouceBuildingItem resouceBuildingItem = this.GetItem();
        resouceBuildingItem.SetData(data[index], this.data2Building[data[index]]);
        this.items.Add(resouceBuildingItem);
      }
    }
    this.scrollView.gameObject.SetActive(true);
    this.grid.repositionNow = true;
    this.grid.Reposition();
    double num1 = 0.0;
    double num2 = 0.0;
    double num3 = 0.0;
    double num4 = 0.0;
    double num5 = 0.0;
    double num6 = 0.0;
    double num7 = 0.0;
    double num8 = 0.0;
    double num9 = 0.0;
    Dictionary<CityManager.BuildingItem, ResouceGenSpeedData>.ValueCollection.Enumerator enumerator = this.data2Building.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null)
      {
        num1 += enumerator.Current.BaseSpeedInHour;
        num2 += enumerator.Current.TotalBenefitSpeedInHour;
        num3 += enumerator.Current.VipGenerateSpeedInHour;
        num4 += enumerator.Current.ResearchGenerateSpeedInHour;
        num5 += enumerator.Current.TelentGenerateSpeedInHour;
        num6 += enumerator.Current.ItemGenerateSpeedInHour;
        num7 += enumerator.Current.EquipmentBenefitInHour;
        num8 += enumerator.Current.FortressBenefittInHour;
        num9 += enumerator.Current.LegendBenefitInHour;
      }
    }
    this.baseValueLabel.text = num1.ToString() + "/h";
    this.benefitValueLabel.text = num2.ToString() + "/h";
    this.vipValueLabel.text = num3.ToString() + "/h";
    this.researchValueLabel.text = num4.ToString() + "/h";
    this.telentValueLabel.text = num5.ToString() + "/h";
    this.itemValueLabel.text = num6.ToString() + "/h";
    this.upkeepValueLabel.text = "[ff0000]" + CityManager.inst.CityUpKeep.ToString() + "[-]/h";
    this.equipValueLabel.text = num7.ToString() + "/h";
    this.fortressValueLabel.text = num8.ToString() + "/h";
    this.heroCardsValueLabel.text = num9.ToString() + "/h";
    if (data.Count > 0)
    {
      this.bacgkround.height = Math.Max((int) NGUIMath.CalculateRelativeWidgetBounds(this.grid.transform, true).size.y + 20, this.minSize);
      this.bacgkround.gameObject.SetActive(true);
    }
    this.tabel.repositionNow = true;
    this.tabel.Reposition();
    this.scrollView.ResetPosition();
  }

  private CityResouceBuildingItem GetItem()
  {
    GameObject gameObject = this.pools.AddChild(this.grid.gameObject);
    gameObject.SetActive(true);
    return gameObject.GetComponent<CityResouceBuildingItem>();
  }

  private void HideUpKeep()
  {
    NGUITools.SetActive(this.upkeepGo, false);
  }

  private void ShowUpKeep()
  {
    NGUITools.SetActive(this.upkeepGo, true);
  }

  private List<CityManager.BuildingItem> GetData()
  {
    this.HideUpKeep();
    List<CityManager.BuildingItem> buildingItemList = (List<CityManager.BuildingItem>) null;
    switch (this.buttonBar.SelectedIndex)
    {
      case 0:
        buildingItemList = CityManager.inst.GetBuildingsByType("farm");
        this.ShowUpKeep();
        break;
      case 1:
        buildingItemList = CityManager.inst.GetBuildingsByType("lumber_mill");
        break;
      case 2:
        buildingItemList = CityManager.inst.GetBuildingsByType("mine");
        break;
      case 3:
        buildingItemList = CityManager.inst.GetBuildingsByType("house");
        break;
    }
    if (buildingItemList != null)
    {
      buildingItemList.Sort(new Comparison<CityManager.BuildingItem>(this.Comparte));
      buildingItemList.Reverse();
      this.data2Building.Clear();
      for (int index = 0; index < buildingItemList.Count; ++index)
      {
        ResouceGenSpeedData resouceGenSpeedData = new ResouceGenSpeedData();
        resouceGenSpeedData.SetData(buildingItemList[index]);
        this.data2Building.Add(buildingItemList[index], resouceGenSpeedData);
      }
    }
    return buildingItemList;
  }

  private int Comparte(CityManager.BuildingItem a, CityManager.BuildingItem b)
  {
    return a.mLevel.CompareTo(b.mLevel);
  }

  private void Clear()
  {
    for (int index = 0; index < this.items.Count; ++index)
    {
      this.items[index].Clear();
      this.pools.Release(this.items[index].gameObject);
    }
    this.items.Clear();
  }
}
