﻿// Decompiled with JetBrains decompiler
// Type: GameObjectPool
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GameObjectPool
{
  private BetterList<GameObject> m_Pool = new BetterList<GameObject>();
  private GameObject m_Prototype;
  private GameObject m_Root;

  public void Initialize(GameObject prototype, GameObject root)
  {
    this.m_Prototype = prototype;
    this.m_Root = root;
  }

  public GameObject Allocate()
  {
    if (this.m_Pool.size > 0)
    {
      GameObject gameObject = this.m_Pool.Pop();
      gameObject.SetActive(true);
      gameObject.transform.parent = (Transform) null;
      return gameObject;
    }
    if ((Object) this.m_Prototype != (Object) null)
      return Object.Instantiate<GameObject>(this.m_Prototype);
    D.error((object) "Prototype is null.");
    return (GameObject) null;
  }

  public void Release(GameObject go)
  {
    if (!((Object) go != (Object) null))
      return;
    go.transform.parent = this.m_Root.transform;
    go.SetActive(false);
    this.m_Pool.Add(go);
  }

  public void Clear()
  {
    for (int index = 0; index < this.m_Pool.size; ++index)
    {
      GameObject gameObject = this.m_Pool[index];
      gameObject.transform.parent = (Transform) null;
      Object.Destroy((Object) gameObject);
      this.m_Pool[index] = (GameObject) null;
    }
    this.m_Pool.Clear();
  }

  public GameObject AddChild(GameObject parent)
  {
    GameObject gameObject = this.Allocate();
    if ((Object) gameObject != (Object) null && (Object) parent != (Object) null)
    {
      Transform transform = gameObject.transform;
      transform.parent = parent.transform;
      transform.localPosition = Vector3.zero;
      transform.localRotation = Quaternion.identity;
      transform.localScale = Vector3.one;
      gameObject.layer = parent.layer;
    }
    return gameObject;
  }
}
