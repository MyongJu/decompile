﻿// Decompiled with JetBrains decompiler
// Type: IconManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.IO;

public class IconManager
{
  private Dictionary<string, string> m_IconDict = new Dictionary<string, string>();
  private static IconManager m_Instance;

  public static IconManager Instance
  {
    get
    {
      if (IconManager.m_Instance == null)
        IconManager.m_Instance = new IconManager();
      return IconManager.m_Instance;
    }
  }

  public void Initialize(string text)
  {
    this.m_IconDict.Clear();
    if (string.IsNullOrEmpty(text))
      return;
    string empty = string.Empty;
    using (StringReader stringReader = new StringReader(text))
    {
      string str1;
      while ((str1 = stringReader.ReadLine()) != null)
      {
        string[] strArray = str1.Split(',');
        if (strArray.Length == 2)
        {
          string key = strArray[0];
          string str2 = strArray[1];
          if (!this.m_IconDict.ContainsKey(key))
            this.m_IconDict.Add(key, str2);
        }
      }
    }
  }

  public string GetIconPath(string key)
  {
    string empty = string.Empty;
    this.m_IconDict.TryGetValue(key, out empty);
    return empty;
  }
}
