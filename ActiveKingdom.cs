﻿// Decompiled with JetBrains decompiler
// Type: ActiveKingdom
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UnityEngine;

public class ActiveKingdom
{
  public const int INVALID_ID = 0;
  public int WorldID;
  public int Hot;

  public ActiveKingdom()
  {
  }

  public ActiveKingdom(int worldId, int hot)
  {
    this.WorldID = worldId;
    this.Hot = hot;
  }

  public bool Decode(object orgData)
  {
    Hashtable inData = orgData as Hashtable;
    if (inData == null)
      return false;
    DatabaseTools.UpdateData(inData, "world_id", ref this.WorldID);
    DatabaseTools.UpdateData(inData, "hot", ref this.Hot);
    return this.WorldID != 0;
  }

  public int KingdomX
  {
    get
    {
      return HelixArrayCalaculater.GetCoordinate(this.WorldID).X;
    }
  }

  public int KingdomY
  {
    get
    {
      return HelixArrayCalaculater.GetCoordinate(this.WorldID).Y;
    }
  }

  public Vector2 pixelOrigin
  {
    get
    {
      WorldCoordinate coordinate = HelixArrayCalaculater.GetCoordinate(this.WorldID);
      return PVPMapData.MapData.CalculateKingdomPixelOrigin(coordinate.X, coordinate.Y);
    }
  }
}
