﻿// Decompiled with JetBrains decompiler
// Type: UIHideHelperEX
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIHideHelperEX : UIHideHelper
{
  public bool needPos = true;
  public float deltaY = 200f;
  private Vector3 originalPos = Vector3.one;
  private float originalAlpha = 1f;
  public new const float TweenDelta = 0.2f;
  public bool needAlpha;
  public bool hasSpriteRender;
  private bool hideOnce;
  private bool isHidden;
  private bool originalPosInited;
  private bool originalAlphaInited;
  private SpriteRenderColorTweener tweener;

  private void Awake()
  {
    UIHideHelperManager.Instance.AddHelper((UIHideHelper) this);
  }

  private void OnDestroy()
  {
    UIHideHelperManager.Instance.RemoveHelper((UIHideHelper) this);
  }

  private void Start()
  {
    this.originalPos = this.gameObject.transform.localPosition;
    if ((double) this.originalPos.x > 80000.0)
      this.originalPos.x -= 100000f;
    this.originalPosInited = true;
    if (this.hasSpriteRender)
      this.tweener = this.gameObject.AddComponent<SpriteRenderColorTweener>();
    if (!this.isHidden)
      return;
    if (this.needPos)
      this.SetGameObjectPos(this.originalPos + new Vector3(0.0f, this.deltaY, 0.0f));
    if (!this.needAlpha)
      return;
    this.ShowParticleEffect(false);
    this.EnableButtons(false);
    this.EnableAnimator(false);
    this.EnableMesh(false);
    this.tweener.QuickHide();
  }

  public override void Hide()
  {
    this.hideOnce = true;
    this.isHidden = true;
    if (this.needPos && this.originalPosInited)
      this.TweenGameObjectPos(this.originalPos + new Vector3(0.0f, this.deltaY, 0.0f));
    if (!this.needAlpha)
      return;
    if (this.originalAlphaInited)
    {
      double num = (double) this.TweenAlphaGameObject(0.0f);
    }
    else
    {
      this.originalAlphaInited = true;
      this.originalAlpha = this.TweenAlphaGameObject(0.0f);
    }
    this.EnableButtons(false);
    this.EnableAnimator(false);
    this.EnableMesh(false);
    this.ShowParticleEffect(false);
    if (!this.hasSpriteRender || !(bool) ((Object) this.tweener))
      return;
    this.tweener.Hide();
  }

  public override void Show()
  {
    this.isHidden = false;
    if (!this.hideOnce)
      return;
    if (this.needPos && this.originalPosInited)
      this.TweenGameObjectPos(this.originalPos);
    if (!this.needAlpha)
      return;
    if (this.originalAlphaInited)
    {
      double num = (double) this.TweenAlphaGameObject(this.originalAlpha);
    }
    this.EnableButtons(true);
    this.EnableMesh(true);
    this.EnableAnimator(true);
    this.ShowParticleEffect(true);
    if (!this.hasSpriteRender || !(bool) ((Object) this.tweener))
      return;
    this.tweener.Show();
  }

  public override void QuickShow()
  {
    this.isHidden = false;
    if (!this.hideOnce)
      return;
    if (this.needPos && this.originalPosInited)
      this.SetGameObjectPos(this.originalPos);
    if (!this.needAlpha)
      return;
    if (this.originalAlphaInited)
      this.SetGameObjectAlpha(this.originalAlpha);
    this.EnableButtons(true);
    this.EnableMesh(true);
    this.EnableAnimator(true);
    this.ShowParticleEffect(true);
    if (!this.hasSpriteRender || !(bool) ((Object) this.tweener))
      return;
    this.tweener.Back();
  }

  public override void QuickHide()
  {
    this.isHidden = true;
    if (!this.hideOnce)
      return;
    if (this.needPos && this.originalPosInited)
      this.SetGameObjectPos(this.originalPos + new Vector3(0.0f, this.deltaY, 0.0f));
    if (!this.needAlpha)
      return;
    if (this.originalAlphaInited)
      this.SetGameObjectAlpha(0.0f);
    this.EnableButtons(false);
    this.EnableMesh(false);
    this.EnableAnimator(false);
    this.ShowParticleEffect(false);
    if (!this.hasSpriteRender || !(bool) ((Object) this.tweener))
      return;
    this.tweener.QuickHide();
  }

  private void TweenGameObjectPos(Vector3 targetPos)
  {
    if (this.gameObject.activeInHierarchy)
    {
      TweenPosition.Begin(this.gameObject, 0.2f, targetPos, false);
    }
    else
    {
      float num = targetPos.y - this.transform.localPosition.y;
      this.SetGameObjectPos(targetPos);
      UIWidget component = this.gameObject.GetComponent<UIWidget>();
      if (!(bool) ((Object) component))
        return;
      component.bottomAnchor.absolute += (int) num;
      component.topAnchor.absolute += (int) num;
      component.UpdateAnchors();
    }
  }

  private void SetGameObjectPos(Vector3 targetPos)
  {
    this.transform.localPosition = targetPos;
  }

  private void SetGameObjectAlpha(float alpha)
  {
    TweenAlpha.Begin(this.gameObject, 0.0f, alpha, 0.0f);
    int childCount = this.transform.childCount;
    for (int index = 0; index < childCount; ++index)
      TweenAlpha.Begin(this.transform.GetChild(index).gameObject, 0.0f, alpha, 0.0f);
  }

  private float TweenAlphaGameObject(float alpha)
  {
    float num = TweenAlpha.Begin(this.gameObject, 0.2f, alpha, 0.0f).value;
    int childCount = this.transform.childCount;
    for (int index = 0; index < childCount; ++index)
      TweenAlpha.Begin(this.transform.GetChild(index).gameObject, 0.2f, alpha, 0.0f);
    return num;
  }

  private void ShowParticleEffect(bool show)
  {
    ParticleSystem[] componentsInChildren = this.GetComponentsInChildren<ParticleSystem>(true);
    for (int index = 0; index < componentsInChildren.Length; ++index)
    {
      if (show)
      {
        componentsInChildren[index].gameObject.SetActive(true);
        componentsInChildren[index].Play();
      }
      else
      {
        componentsInChildren[index].Stop();
        componentsInChildren[index].Clear();
        componentsInChildren[index].gameObject.SetActive(false);
      }
    }
  }

  private void EnableButtons(bool enable)
  {
    foreach (Behaviour componentsInChild in this.GetComponentsInChildren<UIButton>(true))
      componentsInChild.enabled = enable;
  }

  protected void EnableAnimator(bool enable)
  {
    foreach (Behaviour componentsInChild in this.GetComponentsInChildren<Animator>(true))
      componentsInChild.enabled = enable;
  }

  protected void EnableMesh(bool enable)
  {
    foreach (Renderer componentsInChild in this.GetComponentsInChildren<MeshRenderer>(true))
      componentsInChild.enabled = enable;
    foreach (Renderer componentsInChild in this.GetComponentsInChildren<SkinnedMeshRenderer>(true))
      componentsInChild.enabled = enable;
  }
}
