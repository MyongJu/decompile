﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightTalentInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class DragonKnightTalentInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "tree_id")]
  public int talentTreeInternalId;
  [Config(Name = "level")]
  public int level;
  [Config(Name = "active_buff_round")]
  public int activeBuffRound;
  public int maxLevel;
  [Config(Name = "power")]
  public int power;
  [Config(Name = "image")]
  public string image;
  [Config(Name = "skill_priority")]
  public int _priority;
  [Config(Name = "tier")]
  public int tier;
  [Config(CustomParse = true, CustomType = typeof (Requirements), Name = "Requirements")]
  public Requirements require;
  [Config(CustomParse = true, CustomType = typeof (Benefits), Name = "Benefits")]
  public Benefits benefit;
  [Config(Name = "active_time")]
  public int activeTime;
  [Config(Name = "mana_cost")]
  public int manaCost;
  [Config(Name = "active_value_1")]
  public float activeValue1;
  [Config(Name = "active_value_2")]
  public float activeValue2;
  [Config(Name = "active_value_3")]
  public float activeValue3;
  [Config(Name = "active_type")]
  public string activeType;
  [Config(Name = "magic_type")]
  public string magicType;
  [Config(Name = "cd_time")]
  public int cdTime;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "description")]
  public string description;
  [Config(Name = "type")]
  public int type;
  [Config(Name = "active_buff_id_1")]
  public int buffId;

  public int Priority
  {
    get
    {
      return this._priority;
    }
  }

  public string imagePath
  {
    get
    {
      return string.Format("Texture/DragonKnight/TalentIcons/{0}", (object) this.image);
    }
  }

  public int ActiveTimeInMinutes
  {
    get
    {
      return (int) ((double) this.activeTime / 60.0);
    }
  }

  public int CdTimeInHour
  {
    get
    {
      return (int) ((double) this.cdTime / 3600.0);
    }
  }

  public string LocName
  {
    get
    {
      if (this.name == null)
        return string.Empty;
      return Utils.XLAT(this.name);
    }
  }

  public string Description
  {
    get
    {
      if (this.description == null)
        return string.Empty;
      return this.description;
    }
  }

  public string LocDescription
  {
    get
    {
      return ScriptLocalization.GetWithPara(this.Description, new Dictionary<string, string>()
      {
        {
          "1",
          (this.activeValue1 * 100f).ToString()
        },
        {
          "2",
          this.activeValue2.ToString()
        },
        {
          "3",
          (this.activeValue3 * 100f).ToString()
        },
        {
          "4",
          this.activeBuffRound.ToString()
        }
      }, true);
    }
  }

  public string TitleName
  {
    get
    {
      return this.LocName;
    }
  }

  public bool IsHeal()
  {
    return this.activeType == "knight_heal";
  }

  public bool AutoTriggerCheck()
  {
    if (!(this.ID == "knight_talent_knight_active_skill_1_1") && !(this.ID == "knight_talent_knight_active_skill_2_1") && (!(this.ID == "knight_talent_knight_active_skill_3_1") && !(this.ID == "knight_talent_warrior_active_skill_1_1")))
      return this.ID == "knight_talent_warrior_active_skill_3_1";
    return true;
  }

  public bool IsMyself()
  {
    bool flag = false;
    string activeType = this.activeType;
    if (activeType != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DragonKnightTalentInfo.\u003C\u003Ef__switch\u0024map41 == null)
      {
        // ISSUE: reference to a compiler-generated field
        DragonKnightTalentInfo.\u003C\u003Ef__switch\u0024map41 = new Dictionary<string, int>(3)
        {
          {
            "dragon_knight_talent_knight_2",
            0
          },
          {
            "knight_heal",
            0
          },
          {
            "knight_defend",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DragonKnightTalentInfo.\u003C\u003Ef__switch\u0024map41.TryGetValue(activeType, out num) && num == 0)
        flag = true;
    }
    return flag;
  }

  public bool HasDamge()
  {
    return !this.IsMyself();
  }

  public bool IsSplash()
  {
    return this.activeType == "warrior_splash";
  }

  public bool HasRebound()
  {
    return (double) this.activeValue3 > 0.0;
  }

  public bool IsSingleAttack()
  {
    bool flag = true;
    string activeType = this.activeType;
    if (activeType != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DragonKnightTalentInfo.\u003C\u003Ef__switch\u0024map42 == null)
      {
        // ISSUE: reference to a compiler-generated field
        DragonKnightTalentInfo.\u003C\u003Ef__switch\u0024map42 = new Dictionary<string, int>(2)
        {
          {
            "mage_area",
            0
          },
          {
            "warrior_splash",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DragonKnightTalentInfo.\u003C\u003Ef__switch\u0024map42.TryGetValue(activeType, out num) && num == 0)
        flag = false;
    }
    return flag;
  }

  public struct MagicType
  {
    public const string PHYSICS = "physics";
    public const string NATURE = "nature";
    public const string FIRE = "fire";
    public const string WATER = "water";
  }

  public struct ActiveType
  {
    public const string DRAGON_KNIGHT_TALENT_WARRIOR_1 = "dragon_knight_talent_warrior_1";
    public const string DRAGON_KNIGHT_TALENT_WARRIOR_2 = "dragon_knight_talent_warrior_2";
    public const string DRAGON_KNIGHT_TALENT_WARRIOR_3 = "dragon_knight_talent_warrior_3";
    public const string DRAGON_KNIGHT_TALENT_KNIGHT_1 = "dragon_knight_talent_knight_1";
    public const string DRAGON_KNIGHT_TALENT_KNIGHT_2 = "dragon_knight_talent_knight_2";
    public const string DRAGON_KNIGHT_TALENT_KNIGHT_3 = "dragon_knight_talent_knight_3";
    public const string DRAGON_KNIGHT_TALENT_MAGE_1 = "dragon_knight_talent_mage_1";
    public const string DRAGON_KNIGHT_TALENT_MAGE_2 = "dragon_knight_talent_mage_2";
    public const string DRAGON_KNIGHT_TALENT_MAGE_3 = "dragon_knight_talent_mage_3";
    public const string WARRIOR_ATTACK = "warrior_attack";
    public const string WARRIOR_SPLASH = "warrior_splash";
    public const string KNIGHT_HEAL = "knight_heal";
    public const string KNIGHT_DEFEND = "knight_defend";
    public const string MAGE_ATTACK = "mage_attack";
    public const string MAGE_AREA = "mage_area";
  }
}
