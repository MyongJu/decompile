﻿// Decompiled with JetBrains decompiler
// Type: Reward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class Reward : ICustomParse
{
  public Dictionary<string, int> rewards = new Dictionary<string, int>();

  public void Parse(object content)
  {
    Hashtable hashtable = content as Hashtable;
    if (hashtable == null)
      return;
    foreach (DictionaryEntry dictionaryEntry in hashtable)
      this.rewards.Add(dictionaryEntry.Key.ToString(), int.Parse(dictionaryEntry.Value.ToString()));
  }

  public List<Reward.RewardsValuePair> GetRewards()
  {
    List<Reward.RewardsValuePair> rewardsValuePairList = new List<Reward.RewardsValuePair>();
    using (Dictionary<string, int>.Enumerator enumerator = this.rewards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, int> current = enumerator.Current;
        if (ConfigManager.inst.DB_ConfigAddress.IsItem(int.Parse(current.Key)))
          rewardsValuePairList.Add(new Reward.RewardsValuePair(int.Parse(current.Key), current.Value));
      }
    }
    return rewardsValuePairList;
  }

  public class RewardsValuePair
  {
    public int internalID;
    public int value;

    public RewardsValuePair(int internalID, int value)
    {
      this.internalID = internalID;
      this.value = value;
    }
  }
}
