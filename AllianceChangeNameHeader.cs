﻿// Decompiled with JetBrains decompiler
// Type: AllianceChangeNameHeader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Diagnostics;
using System.Text;
using UI;
using UnityEngine;

public class AllianceChangeNameHeader : AllianceCustomizeHeader
{
  [SerializeField]
  private UIInput m_NameInput;
  [SerializeField]
  private UISprite m_NameStatus;
  [SerializeField]
  private SpriteAnimation m_NameBusyIcon;
  [SerializeField]
  private UILabel m_NameTip;
  [SerializeField]
  private UIButton m_NameChangeFree;
  [SerializeField]
  private UIButton m_NameChangeGold;
  [SerializeField]
  private UILabel m_NameChangeGoldPrice;
  private IEnumerator m_NameCoroutine;
  private bool m_NameOkay;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    this.m_NameInput.onValidate = new UIInput.OnValidate(Utils.AllianceNameValidator);
    this.m_NameInput.value = this.allianceData.allianceName;
    this.ResetNameTip();
    this.m_NameOkay = false;
    this.UpdateButtons();
  }

  public void OnChangeName()
  {
    if (IllegalWordsUtils.WarningIllegalWords(this.m_NameInput.value))
      return;
    AllianceManager.Instance.ChangeAllianceName(this.m_NameInput.value, new System.Action<bool, object>(this.SetupAllianceCallback));
  }

  private void SetupAllianceCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.m_NameOkay = false;
    UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_name_changed"), (System.Action) null, 4f, false);
  }

  public void OnNameInputChanged()
  {
    this.m_NameBusyIcon.gameObject.SetActive(false);
    this.m_NameStatus.spriteName = string.Empty;
    this.m_NameOkay = false;
    if (this.m_NameCoroutine != null)
    {
      this.StopCoroutine(this.m_NameCoroutine);
      this.m_NameCoroutine = (IEnumerator) null;
    }
    string str = this.m_NameInput.value.Trim().Replace("[", "【").Replace("]", "】").Replace("\n", string.Empty);
    byte[] bytes = Encoding.UTF8.GetBytes(str);
    if (bytes.Length < 3 || bytes.Length > 20)
    {
      this.m_NameStatus.spriteName = "red_cross";
      this.SetNameError(Utils.XLAT("alliance_settings_change_name_conditions"));
    }
    else if (str == this.allianceData.allianceName)
      this.SetNameError(Utils.XLAT("alliance_settings_change_name_must_be_different_condition"));
    else
      this.StartCoroutine(this.m_NameCoroutine = this.WaitAndCheckName(str));
  }

  [DebuggerHidden]
  private IEnumerator WaitAndCheckName(string newName)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AllianceChangeNameHeader.\u003CWaitAndCheckName\u003Ec__Iterator26()
    {
      newName = newName,
      \u003C\u0024\u003EnewName = newName,
      \u003C\u003Ef__this = this
    };
  }

  private void CheckNameCallback(bool ret, object data)
  {
    try
    {
      this.m_NameBusyIcon.gameObject.SetActive(false);
      if (ret)
      {
        this.m_NameOkay = true;
        this.m_NameStatus.spriteName = "green_tick";
        this.ResetNameTip();
      }
      else
      {
        this.m_NameOkay = false;
        this.m_NameStatus.spriteName = "red_cross";
        Hashtable inData = data as Hashtable;
        int outData = 0;
        DatabaseTools.UpdateData(inData, "errno", ref outData);
        if (outData == 1004021)
          this.SetNameError(Utils.XLAT("exception_" + (object) 1004021));
        else
          this.SetNameError(Utils.XLAT("alliance_create_name_reminder"));
      }
    }
    catch
    {
    }
  }

  private void ResetNameTip()
  {
    this.m_NameTip.text = Utils.XLAT("alliance_name_conditions");
    this.m_NameTip.color = new Color(0.6941177f, 0.6941177f, 0.6941177f, 1f);
  }

  private void SetNameError(string errmsg)
  {
    this.m_NameTip.text = errmsg;
    this.m_NameTip.color = Color.red;
  }

  private void UpdateButtons()
  {
    AllianceInfoStatsData allianceInfoStatsData = DBManager.inst.DB_AllianceStats.Get(PlayerData.inst.allianceId, "edit_alliance_name");
    int num = allianceInfoStatsData != null ? (int) allianceInfoStatsData.value : 0;
    bool flag = false;
    this.m_NameChangeFree.gameObject.SetActive(flag);
    this.m_NameChangeGold.gameObject.SetActive(!flag);
    this.m_NameChangeFree.isEnabled = this.m_NameOkay;
    this.m_NameChangeGold.isEnabled = this.m_NameOkay;
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("alliance_rename_price_base");
    GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("alliance_rename_price_addition");
    GameConfigInfo data3 = ConfigManager.inst.DB_GameConfig.GetData("alliance_rename_price_max");
    int valueInt1 = data1.ValueInt;
    int valueInt2 = data2.ValueInt;
    int valueInt3 = data3.ValueInt;
    this.m_NameChangeGoldPrice.text = Utils.FormatThousands(Math.Min(valueInt1 + num * valueInt2, valueInt3).ToString());
  }

  private void Update()
  {
    this.UpdateButtons();
  }
}
