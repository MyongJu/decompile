﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightMazeHud
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class DragonKnightMazeHud : MonoBehaviour
{
  [SerializeField]
  protected UIButton _ButtonLeft;
  [SerializeField]
  protected UIButton _ButtonRight;
  [SerializeField]
  protected UIButton _ButtonForward;
  [SerializeField]
  protected UIButton _ButtonBackward;
  [SerializeField]
  protected UIButton _ButtonDown;
  [SerializeField]
  protected MazeMinimapItem _MipmapItem;
  [SerializeField]
  protected MazeRoomAnimation _RoomAnimation;
  [SerializeField]
  protected MazeRoomBuffItem _RoomBuffItem;
  [SerializeField]
  protected MazeRoomChestItem _RoomChestItem;
  [SerializeField]
  protected MazeRoomRaidChestItem _RoomRaidChestItem;
  [SerializeField]
  protected BuffTipItem _BuffTipItem;
  [SerializeField]
  protected UILabel _labelFloor;
  [SerializeField]
  protected UILabel _labelLeftTime;
  [SerializeField]
  protected GameObject _rootFloor;
  [SerializeField]
  protected GameObject _rootTime;
  public Camera RoomCamera;
  private Room _previousRoom;
  protected bool _Moving;

  public Transform RoomGroundTransform
  {
    get
    {
      return this._RoomAnimation.GetRoomGroundTransform();
    }
  }

  public void ShakeRoom()
  {
    if (!(bool) ((UnityEngine.Object) this._RoomAnimation))
      return;
    this._RoomAnimation.ShakeRoom();
  }

  public void SamllShakeRoom()
  {
    if (!(bool) ((UnityEngine.Object) this._RoomAnimation))
      return;
    this._RoomAnimation.SmallShakeRoom();
  }

  private void OnEnable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondTick);
  }

  private void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondTick);
  }

  protected void OnSecondTick(int delta)
  {
    DragonKnightDungeonData knightDungeonData = DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L);
    if (knightDungeonData == null)
      return;
    JobHandle job = JobManager.Instance.GetJob(knightDungeonData.ForceQuitJobId);
    if (job == null)
      return;
    this._labelLeftTime.text = Utils.FormatTime1(Mathf.Max(0, job.LeftTime()));
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
    this._labelLeftTime.text = string.Empty;
    this._MipmapItem.SetEnabled(true);
    this._RoomBuffItem.SetEnabled(false);
    this._RoomChestItem.SetEnabled(false);
    this._RoomRaidChestItem.SetEnabled(false);
    DragonKnightSystem.Instance.RoomManager.OnMoveToNextFloorEnabled += new System.Action<bool>(this.OnMoveToNextFloorEnabled);
    DragonKnightSystem.Instance.RoomManager.OnMoveToOtherRoomEnabled += new System.Action<bool, bool, bool, bool>(this.OnMoveToOtherRoomEnabled);
    DragonKnightSystem.Instance.RoomManager.OnRoomSearched += new System.Action<Room>(this.OnRoomSearched);
    DragonKnightSystem.Instance.RoomManager.OnBacktoPreviousRoom += new System.Action<Room, Room>(this.OnBacktoPreviousRoom);
    DragonKnightSystem.Instance.RoomManager.OnMinimapEnabled += new System.Action<bool>(this.OnMinimapEnabled);
    DragonKnightSystem.Instance.RoomManager.OnFloorEnabled += new System.Action<bool>(this.OnFloorEnabled);
    DragonKnightSystem.Instance.RoomManager.OnTimeEnabled += new System.Action<bool>(this.OnTimeEnabled);
    DragonKnightSystem.Instance.RoomManager.OnRoomEventComplete += new System.Action<Room>(this.OnRoomEventComplete);
    DragonKnightSystem.Instance.RoomManager.SetRoomEventProcessor(RoomEventType.EmptyEvent, (IRoomEventProcessor) new RoomProcessorEmpty());
    DragonKnightSystem.Instance.RoomManager.SetRoomEventProcessor(RoomEventType.BuffEvent, (IRoomEventProcessor) this._RoomBuffItem);
    DragonKnightSystem.Instance.RoomManager.SetRoomEventProcessor(RoomEventType.OpenChestEvent, (IRoomEventProcessor) this._RoomChestItem);
    DragonKnightSystem.Instance.RoomManager.SetRoomEventProcessor(RoomEventType.RaidChestEvent, (IRoomEventProcessor) this._RoomRaidChestItem);
    this.UpdateUI();
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
    DragonKnightSystem.Instance.RoomManager.OnMoveToNextFloorEnabled -= new System.Action<bool>(this.OnMoveToNextFloorEnabled);
    DragonKnightSystem.Instance.RoomManager.OnMoveToOtherRoomEnabled -= new System.Action<bool, bool, bool, bool>(this.OnMoveToOtherRoomEnabled);
    DragonKnightSystem.Instance.RoomManager.OnRoomSearched -= new System.Action<Room>(this.OnRoomSearched);
    DragonKnightSystem.Instance.RoomManager.OnBacktoPreviousRoom -= new System.Action<Room, Room>(this.OnBacktoPreviousRoom);
    DragonKnightSystem.Instance.RoomManager.OnMinimapEnabled -= new System.Action<bool>(this.OnMinimapEnabled);
    DragonKnightSystem.Instance.RoomManager.OnFloorEnabled -= new System.Action<bool>(this.OnFloorEnabled);
    DragonKnightSystem.Instance.RoomManager.OnTimeEnabled -= new System.Action<bool>(this.OnTimeEnabled);
    DragonKnightSystem.Instance.RoomManager.OnRoomEventComplete -= new System.Action<Room>(this.OnRoomEventComplete);
    DragonKnightSystem.Instance.RoomManager.SetRoomEventProcessor(RoomEventType.EmptyEvent, (IRoomEventProcessor) null);
    DragonKnightSystem.Instance.RoomManager.SetRoomEventProcessor(RoomEventType.BuffEvent, (IRoomEventProcessor) null);
    DragonKnightSystem.Instance.RoomManager.SetRoomEventProcessor(RoomEventType.OpenChestEvent, (IRoomEventProcessor) null);
  }

  protected void UpdateUI()
  {
    this.SetAllMoveOperationEnabled(false);
  }

  protected void SetAllMoveOperationEnabled(bool enabled)
  {
    this.OnMoveToNextFloorEnabled(enabled);
    this.OnMoveToOtherRoomEnabled(enabled, enabled, enabled, enabled);
  }

  protected void OnMinimapEnabled(bool enabled)
  {
    this._MipmapItem.SetEnabled(enabled);
  }

  protected void OnFloorEnabled(bool enabled)
  {
    this._rootFloor.SetActive(enabled);
  }

  protected void OnTimeEnabled(bool enabled)
  {
    this._rootTime.SetActive(enabled);
  }

  protected void OnMoveToNextFloorEnabled(bool enabled)
  {
    this._ButtonDown.gameObject.SetActive(enabled);
  }

  protected void OnMoveToOtherRoomEnabled(bool left, bool right, bool forward, bool backward)
  {
    this._ButtonLeft.gameObject.SetActive(left);
    this._ButtonRight.gameObject.SetActive(right);
    this._ButtonForward.gameObject.SetActive(forward);
    this._ButtonBackward.gameObject.SetActive(backward);
  }

  protected void OnRoomEventComplete(Room room)
  {
    this._RoomChestItem.SetEnabled(false);
    this._RoomRaidChestItem.SetEnabled(false);
  }

  public void HideAll()
  {
    this._RoomChestItem.SetEnabled(false);
    this._RoomRaidChestItem.SetEnabled(false);
    this._RoomBuffItem.SetEnabled(false);
  }

  protected void OnRoomSearched(Room room)
  {
    bool flag = false;
    if (this._previousRoom == null || this._previousRoom.Floor != room.Floor)
      flag = true;
    ConfigManager.inst.DB_DungeonMain.Get(DragonKnightSystem.Instance.RoomManager.CurrentDragonKnightDungeonData.LevelId);
    if (flag)
    {
      this._labelFloor.text = ScriptLocalization.GetWithPara("dragon_knight_dungeon_floor_num", new Dictionary<string, string>()
      {
        {
          "0",
          room.Floor.ToString()
        }
      }, true);
      this._MipmapItem.UpdateCurrentFloor();
    }
    this._MipmapItem.UpdateRoom(room);
    this._RoomAnimation.SetCurrentRoomData(room);
    this._RoomAnimation.SetSourceRoomEnabled(false);
  }

  private void Update()
  {
    this.AjustButtonPosition();
  }

  protected void AjustButtonPosition()
  {
    MazeRoomItem targetRoomItem = this._RoomAnimation.GetTargetRoomItem();
    if (!(bool) ((UnityEngine.Object) targetRoomItem))
      return;
    this._ButtonLeft.transform.position = targetRoomItem.LeftDoorPosition;
    this._ButtonRight.transform.position = targetRoomItem.RightDoorPosition;
  }

  protected void OnBacktoPreviousRoom(Room fromRoom, Room toRoom)
  {
    this.StartCoroutine(this.BackToPreviousRoomCoroutine(fromRoom, toRoom));
  }

  [DebuggerHidden]
  protected IEnumerator BackToPreviousRoomCoroutine(Room fromRoom, Room toRoom)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DragonKnightMazeHud.\u003CBackToPreviousRoomCoroutine\u003Ec__Iterator56()
    {
      toRoom = toRoom,
      fromRoom = fromRoom,
      \u003C\u0024\u003EtoRoom = toRoom,
      \u003C\u0024\u003EfromRoom = fromRoom,
      \u003C\u003Ef__this = this
    };
  }

  protected bool CheckAndDeleteSpirit(Room room)
  {
    if (room == null || room.IsSearched)
      return true;
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("dungeon_stamina_cost");
    if (data != null && DBManager.inst.DB_DragonKnight.CostSpirit(PlayerData.inst.uid, data.ValueInt))
      return true;
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_dragon_knight_stamina_not_enough", true), (System.Action) null, 4f, true);
    DragonKnightSystem.Instance.Controller.BattleHud.PlayerNoSpiritVFX();
    return false;
  }

  public void OnButtonLeftClicked()
  {
    if (this._Moving)
      return;
    Room nextRoom = DragonKnightSystem.Instance.RoomManager.GetNextRoom(0, -1, 0);
    if (!this.CheckAndDeleteSpirit(nextRoom))
      return;
    this._Moving = true;
    this._RoomBuffItem.SetEnabled(false);
    this._RoomChestItem.SetEnabled(false);
    this._RoomRaidChestItem.SetEnabled(false);
    this.SetAllMoveOperationEnabled(false);
    this._RoomAnimation.PlayAnimation(DragonKnightSystem.Instance.RoomManager.CurrentRoom, nextRoom, (System.Action) null, MazeRoomAnimation.AnimationMode.Scroll);
    this._MipmapItem.MoveIntoRoom(DragonKnightSystem.Instance.RoomManager.GetNextRoom(0, -1, 0), (System.Action<Room>) (_param1 =>
    {
      DragonKnightSystem.Instance.RoomManager.MoveToNextRoom(-1, 0);
      this._Moving = false;
    }));
  }

  public void OnButtonRightClicked()
  {
    if (this._Moving)
      return;
    Room nextRoom = DragonKnightSystem.Instance.RoomManager.GetNextRoom(0, 1, 0);
    if (!this.CheckAndDeleteSpirit(nextRoom))
      return;
    this._Moving = true;
    this._RoomBuffItem.SetEnabled(false);
    this._RoomChestItem.SetEnabled(false);
    this._RoomRaidChestItem.SetEnabled(false);
    this.SetAllMoveOperationEnabled(false);
    this._RoomAnimation.PlayAnimation(DragonKnightSystem.Instance.RoomManager.CurrentRoom, nextRoom, (System.Action) null, MazeRoomAnimation.AnimationMode.Scroll);
    this._MipmapItem.MoveIntoRoom(DragonKnightSystem.Instance.RoomManager.GetNextRoom(0, 1, 0), (System.Action<Room>) (_param1 =>
    {
      DragonKnightSystem.Instance.RoomManager.MoveToNextRoom(1, 0);
      this._Moving = false;
    }));
  }

  public void OnButtonForwardClicked()
  {
    if (this._Moving)
      return;
    Room nextRoom = DragonKnightSystem.Instance.RoomManager.GetNextRoom(0, 0, 1);
    if (!this.CheckAndDeleteSpirit(nextRoom))
      return;
    this._Moving = true;
    this._RoomBuffItem.SetEnabled(false);
    this._RoomChestItem.SetEnabled(false);
    this._RoomRaidChestItem.SetEnabled(false);
    this.SetAllMoveOperationEnabled(false);
    this._RoomAnimation.PlayAnimation(DragonKnightSystem.Instance.RoomManager.CurrentRoom, nextRoom, (System.Action) null, MazeRoomAnimation.AnimationMode.Scroll);
    this._MipmapItem.MoveIntoRoom(DragonKnightSystem.Instance.RoomManager.GetNextRoom(0, 0, 1), (System.Action<Room>) (_param1 =>
    {
      DragonKnightSystem.Instance.RoomManager.MoveToNextRoom(0, 1);
      this._Moving = false;
    }));
  }

  public void OnButtonBackwardClicked()
  {
    if (this._Moving)
      return;
    Room nextRoom = DragonKnightSystem.Instance.RoomManager.GetNextRoom(0, 0, -1);
    if (!this.CheckAndDeleteSpirit(nextRoom))
      return;
    this._Moving = true;
    this._RoomBuffItem.SetEnabled(false);
    this._RoomChestItem.SetEnabled(false);
    this._RoomRaidChestItem.SetEnabled(false);
    this.SetAllMoveOperationEnabled(false);
    this._RoomAnimation.PlayAnimation(DragonKnightSystem.Instance.RoomManager.CurrentRoom, nextRoom, (System.Action) null, MazeRoomAnimation.AnimationMode.Scroll);
    this._MipmapItem.MoveIntoRoom(DragonKnightSystem.Instance.RoomManager.GetNextRoom(0, 0, -1), (System.Action<Room>) (_param1 =>
    {
      DragonKnightSystem.Instance.RoomManager.MoveToNextRoom(0, -1);
      this._Moving = false;
    }));
  }

  public void RequestMoveToCertainRoom(Room room)
  {
    if (this._Moving)
      return;
    Room currentRoom = DragonKnightSystem.Instance.RoomManager.CurrentRoom;
    if (currentRoom.RoomX == room.RoomX && currentRoom.RoomY == room.RoomY)
      return;
    this._Moving = true;
    DragonKnightSystem.Instance.RoomManager.SyncSearchPathToServer((System.Action) (() =>
    {
      this._RoomBuffItem.SetEnabled(false);
      this._RoomChestItem.SetEnabled(false);
      this._RoomRaidChestItem.SetEnabled(false);
      this.SetAllMoveOperationEnabled(false);
      this._RoomAnimation.PlayAnimation(DragonKnightSystem.Instance.RoomManager.CurrentRoom, room, (System.Action) null, MazeRoomAnimation.AnimationMode.FadeOutIn);
      this._MipmapItem.FastMoveTo(room, (System.Action<Room>) (_param1 =>
      {
        DragonKnightSystem.Instance.RoomManager.SearchRoom(room, true);
        this._Moving = false;
      }));
    }), room);
  }

  public void SetTagPosition(Room room)
  {
    this._MipmapItem.SetTagPosition(room);
  }

  public void OnButtonDownClicked()
  {
    UIManager.inst.OpenPopup("DragonKnight/EnterNextFloorConfirmPopup", (Popup.PopupParameter) new EnterNextFloorConfirmPopup.Parameter()
    {
      ConfirmCallback = new System.Action(this.OnConfirmEnterNextFloor)
    });
  }

  protected void OnConfirmEnterNextFloor()
  {
    this._RoomBuffItem.SetEnabled(false);
    this._RoomChestItem.SetEnabled(false);
    this._RoomRaidChestItem.SetEnabled(false);
    this.SetAllMoveOperationEnabled(false);
    DragonKnightSystem.Instance.RoomManager.RequestMoveToNextFloor();
  }

  public void PlayBuffTip(string buffIcon, string buffName)
  {
    this._BuffTipItem.Play(buffIcon, buffName);
  }

  public void PlayRoomAnimation(Room from, Room target, System.Action callback)
  {
    this._RoomAnimation.PlayAnimation(from, target, callback, MazeRoomAnimation.AnimationMode.Scroll);
  }
}
