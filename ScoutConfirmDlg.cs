﻿// Decompiled with JetBrains decompiler
// Type: ScoutConfirmDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ScoutConfirmDlg : UI.Dialog
{
  private Dictionary<string, string> _lastScoutTimeParam = new Dictionary<string, string>();
  private TileData _tileData;
  private ScoutConfirmDlg.Parameter _param;
  [SerializeField]
  private ScoutConfirmDlg.Panel panel;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._param = orgParam as ScoutConfirmDlg.Parameter;
    if (this._param == null)
      return;
    TileData referenceAt = this._param.mapData.GetReferenceAt(this._param.targetLocation);
    if (referenceAt == null)
      return;
    this._tileData = referenceAt;
    this.panel.title.text = Utils.XLAT("march_uppercase_scout_title");
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(referenceAt.AllianceID);
    AllianceFortressData fortressData = referenceAt.FortressData;
    if (fortressData != null)
    {
      AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(fortressData.ConfigId);
      this.panel.targetName.text = fortressData == null || string.IsNullOrEmpty(fortressData.Name) ? buildingStaticInfo.LocalName : fortressData.Name;
    }
    else
      this.panel.targetName.text = referenceAt.TileType != TileType.AllianceTemple ? (referenceAt.UserData == null ? string.Empty : (allianceData == null ? referenceAt.UserData.userName : string.Format("[{0}]{1}", (object) allianceData.allianceAcronym, (object) referenceAt.UserData.userName))) : Utils.XLAT("alliance_altar_name");
    this.panel.targetCoordinateK.text = !MapUtils.IsPitWorld(this._param.targetLocation.K) ? this._param.targetLocation.K.ToString() : Utils.XLAT("id_fire_kingdom");
    this.panel.targetCoordinateX.text = this._param.targetLocation.X.ToString();
    this.panel.targetCoordinateY.text = this._param.targetLocation.Y.ToString();
    this.panel.cost.text = this.CalcScoutSilver().ToString();
    int num1 = this._param.targetLocation.X - PlayerData.inst.playerCityData.cityLocation.X;
    int num2 = this._param.targetLocation.Y - PlayerData.inst.playerCityData.cityLocation.Y;
    this.panel.time.text = Utils.FormatTime(MarchData.CalcScoutTime(PlayerData.inst.CityData.Location, this._param.targetLocation), false, false, true);
    this.AddEventHandler();
    this.OnSecondEvent(0);
    this.UpdateScoutInfo();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
  }

  private void UpdateScoutInfo()
  {
    if (this._tileData == null)
      return;
    Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.transform, false);
    if (this._tileData.TileType == TileType.City)
    {
      if (this._tileData.ScoutData == null || !this._tileData.ScoutData.HasAvailableScoutReport)
        NGUITools.SetActive(this.panel.scoutInfoNode, false);
      else
        NGUITools.SetActive(this.panel.scoutInfoNode, true);
    }
    else
      NGUITools.SetActive(this.panel.scoutInfoNode, false);
    int num = !this.panel.scoutInfoNode.activeInHierarchy ? 30 : 300;
    this.panel.panelFrame.height = (int) relativeWidgetBounds.size.y / 2 + num;
    this.panel.scoutContentNode.localPosition = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y + (float) (num / 2), this.transform.localPosition.z);
  }

  private void UpdateLastScoutTime()
  {
    if (this._tileData == null)
      return;
    if (this._tileData.ScoutData != null && this._tileData.ScoutData.HasAvailableScoutReport)
    {
      this._lastScoutTimeParam.Clear();
      this._lastScoutTimeParam.Add("0", Utils.FormatTime(NetServerTime.inst.ServerTimestamp - this._tileData.ScoutData.ScoutTime, false, false, true));
      this.panel.latestScoutTime.text = ScriptLocalization.GetWithPara("scout_most_recent_report_time", this._lastScoutTimeParam, true);
    }
    else
      this.UpdateScoutInfo();
  }

  private void ShowScoutReport()
  {
    if (this._tileData == null)
      return;
    AbstractMailEntry abstractMailEntry = new AbstractMailEntry();
    abstractMailEntry.ApplyData(this._tileData.ScoutData.MailData);
    abstractMailEntry.isShared = true;
    if (!abstractMailEntry.isRead)
      abstractMailEntry.isRead = this._tileData.ScoutData.IsMailRead;
    UIManager inst = UIManager.inst;
    string popupType = "ScoutReportPopup";
    ScoutReportPopup.Parameter parameter1 = new ScoutReportPopup.Parameter();
    parameter1.hashtable = this._tileData.ScoutData.MailData[(object) "data"] as Hashtable;
    parameter1.mail = abstractMailEntry;
    ScoutReportPopup.Parameter parameter2 = parameter1;
    inst.OpenPopup(popupType, (Popup.PopupParameter) parameter2);
    this._tileData.ScoutData.IsMailRead = true;
    PlayerData.inst.mail.MarkMailAsRead((int) abstractMailEntry.category, new List<AbstractMailEntry>()
    {
      abstractMailEntry
    }, 1 != 0, 0 != 0);
  }

  private void OnSecondEvent(int time)
  {
    this.UpdateLastScoutTime();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void RemoveEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  public void OnCloseButtonClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnScoutButtonClick()
  {
    if (PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD) < (double) this.CalcScoutSilver())
      UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
      {
        title = Utils.XLAT("exception_title"),
        description = Utils.XLAT("scout_not_enough_food_description"),
        buttonClickEvent = (System.Action) null
      });
    else
      GameEngine.Instance.marchSystem.StartScout(this._param.targetLocation);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnScoutReportButtonClick()
  {
    if (this._tileData == null || this._tileData.ScoutData == null || !this._tileData.ScoutData.HasAvailableScoutReport)
      return;
    if (this._tileData.ScoutData.MailData != null && this._tileData.ScoutData.MailData.Count > 0)
      this.ShowScoutReport();
    else
      MessageHub.inst.GetPortByAction("mail:getMail").SendRequest(new Hashtable()
      {
        {
          (object) "mail_id",
          (object) this._tileData.ScoutData.MailId
        }
      }, (System.Action<bool, object>) ((ret, orgData) =>
      {
        if (!ret || orgData == null)
          return;
        Hashtable data = orgData as Hashtable;
        if (data == null || !data.ContainsKey((object) "mail"))
          return;
        Hashtable hashtable = data[(object) "mail"] as Hashtable;
        if (hashtable != null && hashtable.ContainsKey((object) "deleted"))
        {
          int result = 0;
          int.TryParse(hashtable[(object) "deleted"].ToString(), out result);
          if (result != 1)
            return;
          UIManager.inst.toast.Show(Utils.XLAT("toast_battle_report_share_deleted"), (System.Action) null, 4f, true);
        }
        else
        {
          this._tileData.ScoutData.SetMailData(data);
          this.ShowScoutReport();
        }
      }), true);
  }

  public int CalcScoutSilver()
  {
    return (int) ((double) Mathf.Pow((float) PlayerData.inst.playerCityData.buildings.level_stronghold, (float) ConfigManager.inst.DB_GameConfig.GetData("scout_cost_food_c2").Value) * ConfigManager.inst.DB_GameConfig.GetData("scout_cost_food_c1").Value + ConfigManager.inst.DB_GameConfig.GetData("scout_cost_food_c3").Value);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public Coordinate targetLocation;
    public DynamicMapData mapData;
  }

  [Serializable]
  protected class Panel
  {
    public UILabel title;
    public UILabel targetName;
    public UILabel targetCoordinateK;
    public UILabel targetCoordinateX;
    public UILabel targetCoordinateY;
    public UILabel cost;
    public UILabel time;
    public GameObject scoutInfoNode;
    public UISprite panelFrame;
    public UILabel latestScoutTime;
    public Transform scoutContentNode;
  }
}
