﻿// Decompiled with JetBrains decompiler
// Type: WatchtowerMarchListPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WatchtowerMarchListPanel : MonoBehaviour
{
  private Dictionary<WatchEntity, GameObject> m_RendererDict = new Dictionary<WatchEntity, GameObject>();
  private List<GameObject> m_RenderList = new List<GameObject>();
  public System.Action<Hashtable> onMarchDetail;
  public GameObject m_ListPanel;
  public UILabel m_NoMarchLabel;
  public GameObject m_Renderer;
  public GameObject m_FallenKnightNotice;
  public GameObject ignoreAllArea;

  public void UpdateData()
  {
    this.UpdateMarchListData();
    if (!WatchtowerUtilities.IsAlive())
    {
      this.m_NoMarchLabel.gameObject.SetActive(true);
      this.ignoreAllArea.SetActive(false);
    }
    else
    {
      this.m_NoMarchLabel.gameObject.SetActive(false);
      this.ignoreAllArea.SetActive(true);
    }
  }

  public void Dispose()
  {
    this.ClearItems();
  }

  private void OnEnable()
  {
    WatchtowerUtilities.onAdded += new System.Action<WatchEntity>(this.OnMarchAdded);
    WatchtowerUtilities.onRemoved += new System.Action<WatchEntity>(this.OnMarchRemoved);
  }

  private void OnDisable()
  {
    WatchtowerUtilities.onAdded -= new System.Action<WatchEntity>(this.OnMarchAdded);
    WatchtowerUtilities.onRemoved -= new System.Action<WatchEntity>(this.OnMarchRemoved);
  }

  private void OnMarchAdded(WatchEntity entity)
  {
    if (entity.IsEmpty() || this.m_RendererDict.ContainsKey(entity))
      return;
    this.AddItem(entity);
    this.Reposition();
    this.m_NoMarchLabel.gameObject.SetActive(false);
  }

  private void OnMarchRemoved(WatchEntity entity)
  {
    this.RemoveItem(entity);
    this.Reposition();
    if (WatchtowerUtilities.IsAlive())
      return;
    this.m_NoMarchLabel.gameObject.SetActive(true);
    this.ignoreAllArea.SetActive(false);
  }

  private void AddItem(WatchEntity entity)
  {
    if (this.m_RendererDict.ContainsKey(entity))
      return;
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_Renderer);
    gameObject.SetActive(true);
    gameObject.transform.parent = this.m_ListPanel.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
    this.m_RendererDict.Add(entity, gameObject);
    UIStretch[] componentsInChildren = gameObject.GetComponentsInChildren<UIStretch>(true);
    int length = componentsInChildren.Length;
    for (int index = 0; index < length; ++index)
      componentsInChildren[index].container = this.m_ListPanel;
    gameObject.GetComponent<WatchtowerMarchesItemRenderer>().SetData(entity, new System.Action<Hashtable>(this.OnMarchDetail));
  }

  private void AddWatchtowerFallenKnightNotice()
  {
    if (!ActivityManager.Intance.knightData.isActivity)
      return;
    GameObject gameObject = NGUITools.AddChild(this.m_ListPanel, this.m_FallenKnightNotice);
    gameObject.SetActive(true);
    gameObject.GetComponent<WatchtowerFallenKnightNotice>().UpdateUI();
    this.m_RenderList.Add(gameObject);
  }

  private void RemoveItem(WatchEntity entity)
  {
    GameObject gameObject = (GameObject) null;
    this.m_RendererDict.TryGetValue(entity, out gameObject);
    if (!((UnityEngine.Object) gameObject != (UnityEngine.Object) null))
      return;
    gameObject.transform.parent = (Transform) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) gameObject);
  }

  private void OnMarchDetail(Hashtable detail)
  {
    if (this.onMarchDetail == null)
      return;
    this.onMarchDetail(detail);
  }

  private void UpdateMarchListData()
  {
    this.ClearItems();
    this.AddWatchtowerFallenKnightNotice();
    Hashtable postData = new Hashtable();
    List<long> marchOwerIdList = this.GetMarchOwerIDList();
    if (marchOwerIdList.Count > 0)
    {
      postData[(object) "uids"] = (object) marchOwerIdList;
      MessageHub.inst.GetPortByAction("PVP:getIncomingMarchOwnerDefaultInfo").SendRequest(postData, (System.Action<bool, object>) ((arg1, arg2) =>
      {
        if (!arg1)
          return;
        Utils.ExecuteInSecs(0.01f, new System.Action(this.AddItems));
      }), true);
    }
    else
      this.AddItems();
  }

  private void AddItems()
  {
    List<WatchEntity> entities = WatchtowerUtilities.Entities;
    int count = entities.Count;
    for (int index = 0; index < count; ++index)
    {
      WatchEntity entity = entities[index];
      MarchData marchData = (MarchData) null;
      RallyData rallyData = (RallyData) null;
      if (entity.m_Type == WatchType.March)
        marchData = DBManager.inst.DB_March.Get(entity.m_Id);
      else
        rallyData = DBManager.inst.DB_Rally.Get(entity.m_Id);
      if (rallyData != null || marchData != null)
        this.AddItem(entity);
    }
    this.Reposition();
  }

  private List<long> GetMarchOwerIDList()
  {
    List<long> longList = new List<long>();
    using (List<WatchEntity>.Enumerator enumerator = WatchtowerUtilities.Entities.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        WatchEntity current = enumerator.Current;
        if (current.m_Type == WatchType.March)
        {
          MarchData marchData = DBManager.inst.DB_March.Get(current.m_Id);
          if (marchData != null && !longList.Contains(marchData.ownerUid) && ConfigManager.inst.DB_NPC.GetDataByUid(marchData.ownerUid) == null)
            longList.Add(marchData.ownerUid);
        }
        else
        {
          RallyData rallyData = DBManager.inst.DB_Rally.Get(current.m_Id);
          if (!longList.Contains(rallyData.ownerUid))
            longList.Add(rallyData.ownerUid);
        }
      }
    }
    return longList;
  }

  private void Reposition()
  {
    UITable component = this.m_ListPanel.GetComponent<UITable>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
    {
      component.Reposition();
      component.sorting = UITable.Sorting.Custom;
      component.onCustomSort = new Comparison<Transform>(WatchtowerMarchListPanel.Compare);
    }
    UIScrollView scrollView = this.m_ListPanel.GetComponent<UIScrollView>();
    if (!((UnityEngine.Object) scrollView != (UnityEngine.Object) null))
      return;
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => scrollView.ResetPosition()));
  }

  private static int Compare(Transform a, Transform b)
  {
    WatchtowerMarchesItemRenderer component1 = a.GetComponent<WatchtowerMarchesItemRenderer>();
    WatchtowerMarchesItemRenderer component2 = b.GetComponent<WatchtowerMarchesItemRenderer>();
    if ((UnityEngine.Object) component1 == (UnityEngine.Object) null)
      return -1;
    if ((UnityEngine.Object) component2 == (UnityEngine.Object) null)
      return 1;
    return component1.entity.CompareTo(component2.entity);
  }

  private void ClearItems()
  {
    Dictionary<WatchEntity, GameObject>.Enumerator enumerator1 = this.m_RendererDict.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      GameObject gameObject = enumerator1.Current.Value;
      if ((UnityEngine.Object) gameObject != (UnityEngine.Object) null)
      {
        gameObject.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) gameObject);
      }
    }
    this.m_RendererDict.Clear();
    using (List<GameObject>.Enumerator enumerator2 = this.m_RenderList.GetEnumerator())
    {
      while (enumerator2.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator2.Current);
    }
    this.m_RenderList.Clear();
  }

  public void OnIgnoreAllBtnClick()
  {
    List<WatchEntity> entities = WatchtowerUtilities.Entities;
    for (int index = 0; index < entities.Count; ++index)
      WatchtowerUtilities.IgnorMap[entities[index]] = true;
    this.UpdateData();
  }
}
