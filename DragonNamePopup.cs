﻿// Decompiled with JetBrains decompiler
// Type: DragonNamePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Text;
using UI;
using UnityEngine;

public class DragonNamePopup : Popup
{
  public UILabel mCharsRemainingValue;
  public UIInput mCityNameInput;
  public UILabel mCityNameInputLabel;
  public UIButton mOkBtn;
  public UISprite mStatusSprite;
  public Transform dragonroot;
  private DragonView _dragonView;

  public void SetDetails()
  {
    this.mCityNameInput.value = Utils.XLAT("dragon_init_name_placeholder");
    this.mCharsRemainingValue.text = string.Format("{0}/15", (object) Encoding.UTF8.GetBytes(this.mCityNameInput.text).Length);
    this.mStatusSprite.spriteName = "green_tick";
    this.mOkBtn.isEnabled = true;
    this.CreateDragon();
  }

  public void OnCloseBtn()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  public void OnInputChanged()
  {
    string text = this.mCityNameInputLabel.text;
    int num = 0;
    if (text.Length != 0)
      num = Encoding.UTF8.GetBytes(text).Length;
    this.mCharsRemainingValue.text = string.Format("{0}/15", (object) num);
    if (this.CheckName())
    {
      this.mStatusSprite.spriteName = "green_tick";
      this.mOkBtn.isEnabled = true;
    }
    else
    {
      this.mStatusSprite.spriteName = "red_cross";
      this.mOkBtn.isEnabled = false;
    }
  }

  private bool CheckName()
  {
    string text = this.mCityNameInputLabel.text;
    if (text.Length == 0)
      return false;
    byte[] bytes = Encoding.UTF8.GetBytes(text);
    return bytes.Length >= 3 && bytes.Length <= 15;
  }

  public void OnOKBtnPressed()
  {
    this.OnCloseBtn();
  }

  private void CreateDragon()
  {
    this.ReleaseDragon();
    DragonViewData dragonViewData = DragonUtils.GetDragonViewData(PlayerData.inst.dragonData, 0);
    this._dragonView = new DragonView();
    this._dragonView.Mount(this.dragonroot, Vector3.zero, Quaternion.Euler(Vector3.one), Vector3.one);
    this._dragonView.LoadAsync((SpriteViewData) dragonViewData, true, (System.Action<bool, UnityEngine.Object>) ((ret, asset) =>
    {
      if (!ret)
        return;
      this._dragonView.SetRendererLayer(this.dragonroot.gameObject.layer);
    }));
    this._dragonView.OnStateChange += new System.Action<SpriteView.State, SpriteView.State>(this.OnDragonViewStateChange);
  }

  private void OnDragonViewStateChange(SpriteView.State arg1, SpriteView.State arg2)
  {
    if (arg1 != SpriteView.State.Initialized || arg2 != SpriteView.State.Loaded)
      ;
  }

  private void ReleaseDragon()
  {
    if (this._dragonView == null)
      return;
    this._dragonView.Dispose();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.SetDetails();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    string name = this.mCityNameInput.value;
    if (name.Length == 0)
      name = Utils.XLAT("dragon_init_name_placeholder");
    PlayerData.inst.dragonController.RenameDragon(name, (System.Action<object>) null);
    base.OnClose(orgParam);
    this.ReleaseDragon();
    if (NewTutorial.skipTutorial)
      return;
    string recordPoint = NewTutorial.Instance.GetRecordPoint("Tutorial_dragon_skill");
    if (!("finished" != recordPoint))
      return;
    NewTutorial.Instance.InitTutorial("Tutorial_dragon_skill");
    NewTutorial.Instance.LoadTutorialData(recordPoint);
  }
}
