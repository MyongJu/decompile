﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerViewBenefitsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTowerViewBenefitsPopup : Popup
{
  private List<int> marchCapacityList = new List<int>();
  private List<int> noneMarchCapacityList = new List<int>();
  private List<MerlinTowerBenefitViewRender> renderList = new List<MerlinTowerBenefitViewRender>();
  private const string TITLE = "tower_benefits_claimed_title";
  private const string TIPS = "tower_benefits_not_bought_yet_description";
  private const string CURRENT_CAPACITY = "id_march_capacity_counter";
  public UILabel title;
  public GameObject tips;
  public UILabel tipsLable;
  public UITable table;
  public GameObject benefitSlot;
  public UILabel currentMarchCapName;
  private List<int> shopItems;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    this.shopItems = MerlinTowerPayload.Instance.UserData.shopItems;
    this.InitData();
    this.UpdateUI();
  }

  private void InitData()
  {
    this.title.text = ScriptLocalization.Get("tower_benefits_claimed_title", true);
    if (this.shopItems == null)
      return;
    for (int index = 0; index < this.shopItems.Count; ++index)
    {
      if (ConfigManager.inst.DB_MagicTowerShop.GetData(this.shopItems[index]).type == 2)
        this.marchCapacityList.Add(this.shopItems[index]);
      else
        this.noneMarchCapacityList.Add(this.shopItems[index]);
    }
  }

  private void UpdateUI()
  {
    this.currentMarchCapName.text = ScriptLocalization.GetWithPara("id_march_capacity_counter", new Dictionary<string, string>()
    {
      {
        "0",
        MerlinTowerPayload.Instance.UserData.capacity.ToString()
      }
    }, true);
    this.currentMarchCapName.text += string.Format("/{0}", (object) MerlinTowerPayload.Instance.UserData.maxCapacity.ToString());
    if (this.noneMarchCapacityList.Count == 0)
    {
      this.tips.SetActive(true);
      this.tipsLable.text = ScriptLocalization.Get("tower_benefits_not_bought_yet_description", true);
    }
    else
    {
      this.tips.SetActive(false);
      for (int index = 0; index < this.noneMarchCapacityList.Count; ++index)
      {
        GameObject gameObject = Object.Instantiate<GameObject>(this.benefitSlot);
        gameObject.transform.parent = this.table.transform;
        gameObject.transform.localPosition = Vector3.zero;
        gameObject.transform.localScale = Vector3.one;
        gameObject.SetActive(true);
        MerlinTowerBenefitViewRender component = gameObject.GetComponent<MerlinTowerBenefitViewRender>();
        MagicTowerShopInfo data = ConfigManager.inst.DB_MagicTowerShop.GetData(this.noneMarchCapacityList[index]);
        List<Benefits.BenefitValuePair> benefitsPairs = data.Benefits.GetBenefitsPairs();
        PropertyDefinition propertyDefinition = benefitsPairs[0].propertyDefinition;
        component.SetData(new BenefitViewData()
        {
          benefitName = propertyDefinition.Name,
          benefitValue = benefitsPairs[0].value,
          type = data.type,
          format = PropertyDefinition.FormatType.Percent
        });
        this.renderList.Add(component);
      }
    }
  }

  private void ClearData()
  {
    for (int index = 0; index < this.renderList.Count; ++index)
    {
      this.renderList[index].transform.parent = (Transform) null;
      this.renderList[index].gameObject.SetActive(false);
      Object.Destroy((Object) this.renderList[index].gameObject);
    }
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.ClearData();
  }
}
