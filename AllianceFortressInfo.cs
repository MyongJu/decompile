﻿// Decompiled with JetBrains decompiler
// Type: AllianceFortressInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class AllianceFortressInfo : MonoBehaviour
{
  public Icon defaultGroup;
  public GameObject timeGroup;
  public UILabel remainTime;
  private Coordinate cor;
  private bool hasInited;
  private AllianceFortressData afd;

  public void SeedData(Coordinate location, AllianceFortressData afd)
  {
    this.cor = location;
    this.afd = afd;
    this.UpdateData();
    if (this.hasInited)
      return;
    this.AddEventHandler();
  }

  private void UpdateData()
  {
    if (this.afd == null)
    {
      Debug.LogWarning((object) "lack AllianceFortressData!!");
    }
    else
    {
      string stateString = PVPFortressBanner.GetStateString(this.afd.State);
      string str1 = AllianceBuildUtils.CalcCurrentFortressDurabilityByFortressId(this.afd.fortressId).ToString() + "/" + AllianceBuildUtils.GetFullDurability(this.afd.ConfigId).ToString();
      string troopType = this.GetTroopType(this.afd.State);
      string str2 = AllianceBuildUtils.GetCurrentTroopCount(this.afd).ToString() + "/" + AllianceBuildUtils.GetMaxTroopCount(this.afd).ToString();
      if (this.afd.Troops.Count == 0)
        str2 = "0/0";
      string state = this.afd.State;
      if (state != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (AllianceFortressInfo.\u003C\u003Ef__switch\u0024map1C == null)
        {
          // ISSUE: reference to a compiler-generated field
          AllianceFortressInfo.\u003C\u003Ef__switch\u0024map1C = new Dictionary<string, int>(3)
          {
            {
              "building",
              0
            },
            {
              "repairing",
              0
            },
            {
              "demolishing",
              0
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (AllianceFortressInfo.\u003C\u003Ef__switch\u0024map1C.TryGetValue(state, out num) && num == 0)
        {
          this.timeGroup.SetActive(true);
          this.remainTime.text = Utils.FormatTime(AllianceBuildUtils.GetBuildingJobLeftTime(this.afd), true, false, true);
          goto label_10;
        }
      }
      this.timeGroup.SetActive(false);
label_10:
      this.defaultGroup.SetData((string) null, stateString, str1, troopType, str2);
    }
  }

  public void Clear()
  {
    this.RemoveEventHandler();
  }

  private void AddEventHandler()
  {
    this.hasInited = true;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  private void Process(int time)
  {
    this.UpdateData();
  }

  private void RemoveEventHandler()
  {
    this.hasInited = false;
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
  }

  public string GetTroopType(string state)
  {
    string key = state;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceFortressInfo.\u003C\u003Ef__switch\u0024map1D == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceFortressInfo.\u003C\u003Ef__switch\u0024map1D = new Dictionary<string, int>(4)
        {
          {
            "building",
            0
          },
          {
            "defending",
            1
          },
          {
            "repairing",
            2
          },
          {
            "demolishing",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceFortressInfo.\u003C\u003Ef__switch\u0024map1D.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return Utils.XLAT("alliance_fort_constructing_troops");
          case 1:
            return Utils.XLAT("alliance_fort_garrisoned_troops");
          case 2:
            return Utils.XLAT("alliance_fort_repairing_troops");
          case 3:
            return Utils.XLAT("alliance_fort_demolishing_troops");
        }
      }
    }
    return Utils.XLAT("alliance_fort_troops");
  }
}
