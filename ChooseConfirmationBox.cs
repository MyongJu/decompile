﻿// Decompiled with JetBrains decompiler
// Type: ChooseConfirmationBox
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class ChooseConfirmationBox : Popup
{
  public ChooseConfirmationBox.ButtonState buttonState = ChooseConfirmationBox.ButtonState.OK_CENTER;
  public UILabel title;
  public UILabel message;
  public UILabel leftlabel;
  public UILabel rightlabel;
  public UILabel centerlabel;
  public UIButton yesButton;
  public UIButton noButton;
  public UIButton okButton;
  private ChooseConfirmationBox.Parameter m_Parameter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as ChooseConfirmationBox.Parameter;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.title.text = this.m_Parameter.title;
    this.message.text = this.m_Parameter.message;
    this.centerlabel.text = this.m_Parameter.yesOrOklabel_left;
    this.leftlabel.text = this.m_Parameter.yesOrOklabel_left;
    this.rightlabel.text = this.m_Parameter.nolabel_right;
    NGUITools.SetActive(this.yesButton.gameObject, (this.m_Parameter.buttonState & ChooseConfirmationBox.ButtonState.YES_LEFT) > (ChooseConfirmationBox.ButtonState) 0);
    NGUITools.SetActive(this.noButton.gameObject, (this.m_Parameter.buttonState & ChooseConfirmationBox.ButtonState.NO_RIGHT) > (ChooseConfirmationBox.ButtonState) 0);
    NGUITools.SetActive(this.okButton.gameObject, (this.m_Parameter.buttonState & ChooseConfirmationBox.ButtonState.OK_CENTER) > (ChooseConfirmationBox.ButtonState) 0);
  }

  private void Close()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnCloseHandler()
  {
    this.Close();
    if (this.m_Parameter.closeHandler != null)
      this.m_Parameter.closeHandler();
    this.m_Parameter = (ChooseConfirmationBox.Parameter) null;
  }

  public void OnYesHandler()
  {
    this.Close();
    if (this.m_Parameter.noHandler_left != null)
      this.m_Parameter.noHandler_left();
    this.m_Parameter = (ChooseConfirmationBox.Parameter) null;
  }

  public void OnNoHandler()
  {
    this.Close();
    if (this.m_Parameter.yesOrOkHandler_right != null)
      this.m_Parameter.yesOrOkHandler_right();
    this.m_Parameter = (ChooseConfirmationBox.Parameter) null;
  }

  public void OnCenterHandler()
  {
    this.Close();
    if (this.m_Parameter.yesOrOkHandler_right != null)
      this.m_Parameter.yesOrOkHandler_right();
    this.m_Parameter = (ChooseConfirmationBox.Parameter) null;
  }

  public class Parameter : Popup.PopupParameter
  {
    public string yesOrOklabel_left = "YES";
    public string nolabel_right = "NO";
    public ChooseConfirmationBox.ButtonState buttonState = ChooseConfirmationBox.ButtonState.OK_CENTER;
    public string title;
    public string message;
    public System.Action yesOrOkHandler_right;
    public System.Action noHandler_left;
    public System.Action closeHandler;
  }

  public enum ButtonState
  {
    YES_LEFT = 2,
    NO_RIGHT = 4,
    OK_CENTER = 8,
    NO_BUTTON = 16, // 0x00000010
  }
}
