﻿// Decompiled with JetBrains decompiler
// Type: HeroRecruitDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;

public class HeroRecruitDialog : UI.Dialog
{
  public UILabel m_GoldCount;
  public HeroRecruitSlot[] m_RecruitSlots;

  public static void ShowHeroRecruitDialog()
  {
    MessageHub.inst.GetPortByAction("Legend:getTempleInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) ((_param0, _param1) => UIManager.inst.OpenDlg("HeroCard/HeroRecruitDlg", (UI.Dialog.DialogParameter) null, true, true, true)), true);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.UpdateUI();
    DBManager.inst.DB_LegendTemple.onDataChanged += new System.Action<LegendTempleData>(this.OnLegendTempleChanged);
    DBManager.inst.DB_User.onDataChanged += new System.Action<long>(this.OnUserChanged);
    if (NewTutorial.skipTutorial)
      return;
    string recordPoint = NewTutorial.Instance.GetRecordPoint("Tutorial_hero_summon");
    if (!("finished" != recordPoint))
      return;
    NewTutorial.Instance.InitTutorial("Tutorial_hero_summon");
    NewTutorial.Instance.LoadTutorialData(recordPoint);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    DBManager.inst.DB_LegendTemple.onDataChanged -= new System.Action<LegendTempleData>(this.OnLegendTempleChanged);
    DBManager.inst.DB_User.onDataChanged -= new System.Action<long>(this.OnUserChanged);
  }

  private void OnLegendTempleChanged(LegendTempleData data)
  {
    this.UpdateUI();
  }

  private void OnUserChanged(long userId)
  {
    this.UpdateUI();
  }

  public void OnGold()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  public void OnHandbook()
  {
    UIManager.inst.OpenPopup("ParliamentHero/HeroBookPopup", (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.m_GoldCount.text = Utils.FormatThousands(PlayerData.inst.userData.currency.gold.ToString());
    this.m_RecruitSlots[0].UpdateUI((HeroRecruitData) new HeroRecruitDataBronze());
    this.m_RecruitSlots[1].UpdateUI((HeroRecruitData) new HeroRecruitDataSilver());
    this.m_RecruitSlots[2].UpdateUI((HeroRecruitData) new HeroRecruitDataGold());
  }
}
