﻿// Decompiled with JetBrains decompiler
// Type: TreasureMapPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TreasureMapPopup : Popup
{
  private List<TreasureItemRenderer> _itemList = new List<TreasureItemRenderer>();
  public GameObject m_ItemPrefab;
  public UIButton m_ArrowLeft;
  public UIButton m_ArrowRight;
  public UILabel m_Location;
  public UILabel m_Title;
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  public UITexture m_Banner;
  private int _currentMapIndex;
  private ItemStaticInfo _itemInfo;
  private ConsumableItemData _itemData;
  private TreasureMapData _mapData;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._itemInfo = (orgParam as TreasureMapPopup.Paramenter).itemInfo;
    this._itemData = DBManager.inst.DB_Item.Get(this._itemInfo.internalId);
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnDiscardPressed()
  {
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = ScriptLocalization.Get("id_uppercase_warning", true),
      content = ScriptLocalization.Get("kingdom_treasure_map_discard_confirm_description", true),
      yes = ScriptLocalization.Get("id_uppercase_yes", true),
      no = ScriptLocalization.Get("id_uppercase_no", true),
      yesCallback = (System.Action) (() =>
      {
        Hashtable postData = new Hashtable();
        postData[(object) "item_id"] = (object) this._itemInfo.internalId;
        postData[(object) "map_id"] = (object) this._mapData.MapId;
        MessageHub.inst.GetPortByAction("TreasureMap:discardTreasureMap").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          this._currentMapIndex = this._currentMapIndex >= 0 ? this._currentMapIndex : 0;
          this._currentMapIndex = this._currentMapIndex < this._itemData.quantity ? this._currentMapIndex : this._itemData.quantity - 1;
          this.UpdateUI();
        }), true);
      }),
      noCallback = (System.Action) (() => {})
    });
  }

  public void OnGotoPressed()
  {
    LinkerHub.Instance.LHGotoTile(new LPTile()
    {
      k = PlayerData.inst.playerCityData.cityLocation.K,
      x = this._mapData.MapX,
      y = this._mapData.MapY,
      circle = true
    });
    this.OnClosePressed();
  }

  public void OnNextPressed()
  {
    ++this._currentMapIndex;
    this._currentMapIndex = this._currentMapIndex < this._itemData.quantity ? this._currentMapIndex : this._itemData.quantity - 1;
    this.UpdateUI();
  }

  public void OnPrevPressed()
  {
    --this._currentMapIndex;
    this._currentMapIndex = this._currentMapIndex >= 0 ? this._currentMapIndex : 0;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Banner, "Texture/GUI_Textures/treasure_map_image", (System.Action<bool>) null, true, true, string.Empty);
    this.RequestCurrentMap((System.Action) (() =>
    {
      if (ItemBag.Instance.GetItemCount(this._itemData.internalId) == 0)
      {
        this.OnClosePressed();
      }
      else
      {
        this._mapData = DBManager.inst.DB_Treasure.Get(this._itemData.properties[this._currentMapIndex]);
        this.UpdateMapUI();
      }
    }));
  }

  private void UpdateMapUI()
  {
    this.m_Title.text = string.Format("{0} ({1}/{2})", (object) Utils.XLAT("kingdom_uppercase_treasure_map"), (object) (this._currentMapIndex + 1), (object) this._itemData.quantity);
    this.m_Location.text = string.Format("X:{0} Y:{1}", (object) this._mapData.MapX, (object) this._mapData.MapY);
    this.m_ArrowLeft.gameObject.SetActive(this._currentMapIndex > 0);
    this.m_ArrowRight.gameObject.SetActive(this._currentMapIndex < this._itemData.quantity - 1);
    this.UpdateList();
  }

  private void ClearList()
  {
    using (List<TreasureItemRenderer>.Enumerator enumerator = this._itemList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TreasureItemRenderer current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this._itemList.Clear();
  }

  private void UpdateList()
  {
    this.ClearList();
    List<DropMainData> listByDropGroupId = ConfigManager.inst.DB_DropMain.GetDropMainDataListByDropGroupId(ConfigManager.inst.DB_TreasureMap.Get(this._itemInfo.Param1).drop_id);
    for (int index = 0; index < listByDropGroupId.Count; ++index)
    {
      DropMainData dropMainData = listByDropGroupId[index];
      ConfigManager.inst.DB_Items.GetItem(dropMainData.ItemId);
      GameObject gameObject = Utils.DuplicateGOB(this.m_ItemPrefab, this.m_Grid.transform);
      gameObject.SetActive(true);
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localScale = Vector3.one;
      TreasureItemRenderer component = gameObject.GetComponent<TreasureItemRenderer>();
      component.SetData(dropMainData);
      this._itemList.Add(component);
    }
    this.m_Grid.sorting = UIGrid.Sorting.Custom;
    this.m_Grid.onCustomSort = new Comparison<Transform>(this.OnCustomSort);
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  private int OnCustomSort(Transform x, Transform y)
  {
    return x.GetComponent<TreasureItemRenderer>().ItemInfo.Priority - y.GetComponent<TreasureItemRenderer>().ItemInfo.Priority;
  }

  private void RequestCurrentMap(System.Action callback)
  {
    if (this._currentMapIndex >= this._itemData.properties.Count)
    {
      Hashtable postData = new Hashtable();
      postData[(object) "item_id"] = (object) this._itemInfo.internalId;
      MessageHub.inst.GetPortByAction("TreasureMap:openTreasureMap").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret || callback == null)
          return;
        callback();
      }), true);
    }
    else
    {
      if (callback == null)
        return;
      callback();
    }
  }

  public class Paramenter : Popup.PopupParameter
  {
    public ItemStaticInfo itemInfo;
  }
}
