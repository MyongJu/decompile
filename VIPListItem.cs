﻿// Decompiled with JetBrains decompiler
// Type: VIPListItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UI;
using UnityEngine;

public class VIPListItem : MonoBehaviour
{
  public UILabel itemName;
  public UITexture itemIcon;
  public UILabel itemDesc;
  public UILabel ownNum;
  public UIButton buyBtn;
  public UIButton useBtn;
  public UILabel itemPrice;
  public Texture missingIcon;
  private ShopStaticInfo shopInfo;
  private ItemStaticInfo item;
  private bool isAddedEventHandler;
  private bool isInit;
  private System.Action callback;

  public void SetItem(ItemStaticInfo info, System.Action callback)
  {
    this.item = info;
    this.callback = callback;
    this.UpdateUI();
    if (this.isInit)
      return;
    this.isInit = true;
    this.AddEventHandler();
  }

  public void SetShopItem(ShopStaticInfo info)
  {
    this.shopInfo = info;
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemChangeHandler);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnItemChangeHandler);
    EventDelegate.Add(this.buyBtn.onClick, new EventDelegate.Callback(this.OnBuyHandler));
    EventDelegate.Add(this.useBtn.onClick, new EventDelegate.Callback(this.OnUseItemHandler));
  }

  private void OnBuyHandler()
  {
    if (ItemBag.Instance.GetShopItemPrice(this.shopInfo.ID) > PlayerData.inst.hostPlayer.Currency)
    {
      Utils.ShowNotEnoughGoldTip();
      this.CloseWindow();
    }
    else
      UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) new ItemUseOrBuyPopup.Parameter()
      {
        shop_id = this.shopInfo.internalId
      });
  }

  private void BuyAndUserConfirmHandler()
  {
    if (PlayerData.inst.hostPlayer.Currency < ItemBag.Instance.GetShopItemPrice(this.shopInfo.Item_InternalId))
      Utils.ShowNotEnoughGoldTip();
    else
      UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) new ItemUseOrBuyPopup.Parameter()
      {
        shop_id = this.shopInfo.internalId
      });
  }

  private void OnUseItemHandler()
  {
    UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) new ItemUseOrBuyPopup.Parameter()
    {
      shop_id = this.shopInfo.internalId,
      type = ItemUseOrBuyPopup.Parameter.Type.UseItem
    });
  }

  private void BackConfirm()
  {
    ItemBag.Instance.UseItem(this.item.ID, (Hashtable) null, new System.Action<bool, object>(this.OnUseOrBuyCallback), 1);
    this.CloseWindow();
  }

  private void OnUseOrBuyCallback(bool ret, object data)
  {
    if (!ret || this.callback == null)
      return;
    this.callback();
  }

  private void CloseWindow()
  {
  }

  private void OnItemChangeHandler(int internalId)
  {
    if (internalId != this.item.internalId)
      return;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this.item == null)
      this.item = ConfigManager.inst.DB_Items.GetItem(this.shopInfo.Item_InternalId);
    DBManager.inst.DB_Item.Get(this.item.internalId);
    int itemCount = ItemBag.Instance.GetItemCount(this.item.internalId);
    this.ownNum.text = string.Format("x{0}", (object) itemCount);
    string str1 = ScriptLocalization.Get(this.item.Loc_Name_Id, true);
    string str2 = ScriptLocalization.Get(this.item.Loc_Description_Id, true);
    this.itemName.text = str1;
    this.itemDesc.text = str2;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.itemIcon, string.Format("Texture/ItemIcons/{0}", (object) this.item.ID), (System.Action<bool>) null, true, false, string.Empty);
    if (itemCount <= 0)
    {
      if (this.shopInfo.Price <= 0)
        this.itemPrice.text = ScriptLocalization.Get("shop_buy_free", true);
      else
        Utils.SetPriceToLabel(this.itemPrice, ItemBag.Instance.GetShopItemPrice(this.shopInfo.ID));
    }
    if (itemCount > 0)
    {
      this.ownNum.color = ItemStore.NonZeroColor;
      this.buyBtn.gameObject.SetActive(false);
      this.useBtn.gameObject.SetActive(true);
    }
    else
    {
      this.ownNum.color = ItemStore.ZeroColor;
      this.buyBtn.gameObject.SetActive(true);
      this.useBtn.gameObject.SetActive(false);
    }
    if (this.shopInfo == null || (double) this.shopInfo.Discount_In_Limit_Time == 0.0 || (this.shopInfo.Limit_Discount_End_Time <= (long) NetServerTime.inst.ServerTimestamp || this.isAddedEventHandler))
      return;
    this.isAddedEventHandler = true;
    Oscillator.Instance.updateEvent += new System.Action<double>(this.Process);
  }

  private void Process(double time)
  {
    if (this.shopInfo.Price <= 0)
      this.itemPrice.text = ScriptLocalization.Get("shop_buy_free", true);
    else
      this.itemPrice.text = ItemBag.Instance.GetShopItemPrice(this.shopInfo.ID).ToString();
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemChangeHandler);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnItemChangeHandler);
    EventDelegate.Remove(this.buyBtn.onClick, new EventDelegate.Callback(this.OnBuyHandler));
    EventDelegate.Remove(this.useBtn.onClick, new EventDelegate.Callback(this.OnUseItemHandler));
    this.isInit = false;
    if (!this.isAddedEventHandler)
      return;
    this.isAddedEventHandler = false;
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.updateEvent -= new System.Action<double>(this.Process);
  }

  public void Clear()
  {
    this.RemoveEventHandler();
    BuilderFactory.Instance.Release((UIWidget) this.itemIcon);
  }
}
