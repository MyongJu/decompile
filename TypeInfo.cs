﻿// Decompiled with JetBrains decompiler
// Type: TypeInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class TypeInfo : MonoBehaviour
{
  public UILabel name;
  public UILabel count;

  public void SeedData(TypeInfo.Data d)
  {
    this.name.text = ScriptLocalization.Get(d.name + "_name", true);
    if (d.count == 0L)
    {
      this.count.gameObject.SetActive(false);
    }
    else
    {
      this.count.gameObject.SetActive(true);
      this.count.text = d.count.ToString();
    }
  }

  public class Data
  {
    public string name;
    public long count;
  }
}
