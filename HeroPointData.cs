﻿// Decompiled with JetBrains decompiler
// Type: HeroPointData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class HeroPointData
{
  public int _internalID;
  [Config(Name = "hero_level")]
  public string _level;
  [Config(Name = "skill_points")]
  public int _skillPoints;
  [Config(Name = "total_experience")]
  public int _experienceRequired;
  [Config(Name = "power")]
  public int _power;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "Rewards")]
  public Reward Rewards;

  public string Level
  {
    get
    {
      return this._level;
    }
  }

  public int SkillPoints
  {
    get
    {
      return this._skillPoints;
    }
  }

  public int ExperienceRequired
  {
    get
    {
      return this._experienceRequired;
    }
  }

  public int Power
  {
    get
    {
      return this._power;
    }
  }
}
