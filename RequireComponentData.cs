﻿// Decompiled with JetBrains decompiler
// Type: RequireComponentData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class RequireComponentData : IComponentData
{
  private List<RequireComponentData.RequireElement> requires;
  private BagType bagType;

  public RequireComponentData(BagType bagType, List<RequireComponentData.RequireElement> requires)
  {
    this.requires = requires;
    this.bagType = bagType;
  }

  public List<RequireComponentData.RequireElement> GetRequires()
  {
    return this.requires;
  }

  public BagType GetBagType()
  {
    return this.bagType;
  }

  public enum Requirement
  {
    NONE,
    FOOD,
    WOOD,
    SILVER,
    ORE,
    BUILDING,
    ITEM,
    QUEUE,
    LIMIT,
    STEEL,
    SCROLL,
  }

  public class RequireElement
  {
    public RequireComponentData.Requirement type;
    public long need;
    public int internalID;
    public bool showOwnMessage;

    public RequireElement(RequireComponentData.Requirement type, long need, int internalID, bool showOwnMessage = true)
    {
      this.type = type;
      this.need = need;
      this.internalID = internalID;
      this.showOwnMessage = showOwnMessage;
    }
  }
}
