﻿// Decompiled with JetBrains decompiler
// Type: FunplusPaymentAgent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using System.Collections.Generic;

public class FunplusPaymentAgent : FunplusPayment.IDelegate, FunplusThirdPartyPayment.IDelegate
{
  private List<FunplusProduct> products;

  public event System.Action<bool> OnBuyCallBack;

  public event System.Action OnFetchProductListFinish;

  public List<FunplusProduct> Products
  {
    get
    {
      return this.products;
    }
  }

  public void OnInitializeSuccess(List<FunplusProduct> products)
  {
    this.products = products;
    if (this.OnFetchProductListFinish == null)
      return;
    this.OnFetchProductListFinish();
  }

  public void OnInitializeError(FunplusError error)
  {
  }

  public void OnPurchaseSuccess(string productId, string throughCargo)
  {
    if (this.OnBuyCallBack == null)
      return;
    this.OnBuyCallBack(true);
  }

  public void OnPurchaseError(FunplusError error)
  {
    if (this.OnBuyCallBack == null)
      return;
    this.OnBuyCallBack(false);
  }

  public void onPurchaseFinished(string throughCargo)
  {
  }

  public void onPendingPurchaseFound(string throughCargo)
  {
  }
}
