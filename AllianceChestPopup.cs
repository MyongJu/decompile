﻿// Decompiled with JetBrains decompiler
// Type: AllianceChestPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceChestPopup : Popup
{
  private List<ChestReceiveDetailItem> _AllChestReceiveDetailItem = new List<ChestReceiveDetailItem>();
  [SerializeField]
  private AllianceSymbol _allianceSymbol;
  [SerializeField]
  private UILabel _LabelAllianceName;
  [SerializeField]
  private UILabel _LabelGetCount;
  [SerializeField]
  private UILabel _labelChestName;
  [SerializeField]
  private UIScrollView _ScrollView;
  [SerializeField]
  private UITable _TableContainer;
  [SerializeField]
  private ChestReceiveDetailItem _ChestReceiveDetailItemTemplate;
  [SerializeField]
  private GameObject _rootOpenEffect;
  private AllianceTreasuryVaultData _sendedChestData;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    AllianceChestPopup.Parameter parameter = orgParam as AllianceChestPopup.Parameter;
    this._sendedChestData = parameter.sendedChestData;
    this._ChestReceiveDetailItemTemplate.gameObject.SetActive(false);
    this.UpdateUI();
    this._rootOpenEffect.SetActive(parameter.firstTime);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.OnClose(orgParam);
  }

  private void UpdateUI()
  {
    AllianceTreasuryVaultMainInfo data = ConfigManager.inst.DB_AllianceTreasuryVaultMain.GetData(this._sendedChestData.configId);
    if (data != null)
      this._labelChestName.text = data.LocalChestName;
    AllianceData allianceData = PlayerData.inst.allianceData;
    if (allianceData != null)
    {
      this._LabelAllianceName.text = allianceData.allianceFullName;
      this._allianceSymbol.SetSymbols(allianceData.allianceSymbolCode);
    }
    this.DestoryAllChestReceiveDetailItem();
    using (List<AllianceTreasuryVaultData.ClaimedInfo>.Enumerator enumerator = this._sendedChestData.allClaimedInfo.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceTreasuryVaultData.ClaimedInfo current = enumerator.Current;
        if (current.uid == PlayerData.inst.uid)
          this._LabelGetCount.text = current.gold.ToString();
        this.CreateChestReceiveDetailItem().SetData(current.uid, current.userName, current.gold, current.isLuckiest);
      }
    }
    this._TableContainer.Reposition();
    this._ScrollView.ResetPosition();
  }

  private ChestReceiveDetailItem CreateChestReceiveDetailItem()
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this._ChestReceiveDetailItemTemplate.gameObject);
    gameObject.transform.SetParent(this._TableContainer.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    ChestReceiveDetailItem component = gameObject.GetComponent<ChestReceiveDetailItem>();
    this._AllChestReceiveDetailItem.Add(component);
    return component;
  }

  private void DestoryAllChestReceiveDetailItem()
  {
    using (List<ChestReceiveDetailItem>.Enumerator enumerator = this._AllChestReceiveDetailItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Object.Destroy((Object) enumerator.Current.gameObject);
    }
    this._AllChestReceiveDetailItem.Clear();
  }

  public void OnButtonCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public AllianceTreasuryVaultData sendedChestData;
    public bool firstTime;
  }
}
