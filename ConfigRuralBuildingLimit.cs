﻿// Decompiled with JetBrains decompiler
// Type: ConfigRuralBuildingLimit
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigRuralBuildingLimit
{
  private Dictionary<int, ConfigRuralBuildingLimitInfo> levelLimitMap = new Dictionary<int, ConfigRuralBuildingLimitInfo>();
  private ConfigParse parse = new ConfigParse();

  public void BuildDB(object res)
  {
    this.parse.Parse<ConfigRuralBuildingLimitInfo, int>(res as Hashtable, "strongHoldLevel", out this.levelLimitMap);
    using (Dictionary<int, ConfigRuralBuildingLimitInfo>.Enumerator enumerator = this.levelLimitMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.BuildLimitDic();
    }
  }

  public int GetBuildingLimit(string buildingType)
  {
    ConfigRuralBuildingLimitInfo levelLimit = this.levelLimitMap[CityManager.inst.GetHighestBuildingLevelFor("stronghold")];
    if (levelLimit.limitDic.ContainsKey(buildingType))
      return levelLimit.limitDic[buildingType];
    return 1;
  }

  public void Clear()
  {
    if (this.levelLimitMap == null)
      return;
    this.levelLimitMap.Clear();
  }
}
