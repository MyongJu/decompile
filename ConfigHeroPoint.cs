﻿// Decompiled with JetBrains decompiler
// Type: ConfigHeroPoint
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigHeroPoint
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<int, HeroPointData> dicByHeroLevel;
  private Dictionary<string, HeroPointData> datas;

  public int MaxLevel
  {
    get
    {
      return this.datas.Count;
    }
  }

  public void BuildDB(object res)
  {
    this.parse.Parse<HeroPointData, string>(res as Hashtable, "_level", out this.datas, out this.dicByHeroLevel);
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, HeroPointData>) null;
    }
    if (this.dicByHeroLevel == null)
      return;
    this.dicByHeroLevel.Clear();
    this.dicByHeroLevel = (Dictionary<int, HeroPointData>) null;
  }

  public HeroPointData GetHeroPointData(int internalId)
  {
    if (this.dicByHeroLevel != null && this.dicByHeroLevel.ContainsKey(internalId))
      return this.dicByHeroLevel[internalId];
    return (HeroPointData) null;
  }

  public HeroPointData GetHeroPointData(string id)
  {
    if (this.datas == null)
      return (HeroPointData) null;
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (HeroPointData) null;
  }

  public HeroPointData this[int level]
  {
    get
    {
      if (this.datas.ContainsKey(level.ToString()))
        return this.datas[level.ToString()];
      return (HeroPointData) null;
    }
  }

  public int LevelForXP(int xp)
  {
    int num = 1;
    while (num < this.MaxLevel && xp >= this.datas[(num + 1).ToString()].ExperienceRequired)
      ++num;
    return num;
  }
}
