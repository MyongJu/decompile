﻿// Decompiled with JetBrains decompiler
// Type: KingdomArtifactPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class KingdomArtifactPopup : Popup
{
  public UILabel artifactName;
  public UILabel artifactDescription;
  public UITexture artifactTexture;
  public EquipmentBenefits benefitContent;
  private int artifactId;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    KingdomArtifactPopup.Parameter parameter = orgParam as KingdomArtifactPopup.Parameter;
    if (parameter != null)
      this.artifactId = parameter.artifactId;
    this.Initialize();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.Dispose();
    base.OnClose(orgParam);
  }

  private void Initialize()
  {
    ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this.artifactId);
    if (artifactInfo == null)
      return;
    if (artifactInfo.benefits != null)
      this.benefitContent.Init(artifactInfo.benefits);
    this.artifactName.text = artifactInfo.Name;
    this.artifactDescription.text = artifactInfo.Description;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactTexture, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnOkayBtnPressed()
  {
    this.OnCloseBtnPressed();
  }

  private void Dispose()
  {
    this.benefitContent.Dispose();
  }

  public class Parameter : Popup.PopupParameter
  {
    public int artifactId;
  }
}
