﻿// Decompiled with JetBrains decompiler
// Type: ConfigLegendSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigLegendSlot
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, LegendSlotInfo> datas;
  private Dictionary<int, LegendSlotInfo> dicByUniqueId;

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, LegendSlotInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, LegendSlotInfo>) null;
  }

  public void BuildDB(object res)
  {
    this.parse.Parse<LegendSlotInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public LegendSlotInfo GetLegendSlotInfo(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (LegendSlotInfo) null;
  }

  public LegendSlotInfo GetLegendSlotInfo(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (LegendSlotInfo) null;
  }

  public int GetTotalNum()
  {
    if (this.dicByUniqueId != null)
      return this.dicByUniqueId.Count;
    return 0;
  }
}
