﻿// Decompiled with JetBrains decompiler
// Type: MailTabEntry
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MailTabEntry : MonoBehaviour
{
  public MailCategory type;
  public UILabel countLabel;

  public void UpdateUI(int count)
  {
    if (count < 1)
    {
      this.gameObject.SetActive(false);
    }
    else
    {
      this.gameObject.SetActive(true);
      this.countLabel.text = count <= 99 ? count.ToString() : "99+";
    }
  }
}
