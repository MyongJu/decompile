﻿// Decompiled with JetBrains decompiler
// Type: AllianceRecommendTip
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class AllianceRecommendTip : MonoBehaviour
{
  private const float FIVE_MINUTES = 300f;
  public UITexture m_Portrait;
  public UILabel m_AllianceName;
  public UILabel m_Language;
  private AllianceSearchItemData m_ItemData;
  private Coroutine m_AutoCloseCoroutine;
  private System.Action m_OnHide;

  public void Show(AllianceSearchItemData itemData, System.Action onHide)
  {
    this.m_ItemData = itemData;
    this.m_OnHide = onHide;
    this.gameObject.SetActive(true);
    this.m_AutoCloseCoroutine = this.StartCoroutine(this.AutoCloseJob());
    this.m_AllianceName.text = string.Format("[{0}]{1}", (object) this.m_ItemData.tag, (object) this.m_ItemData.name);
    this.m_Language.text = ScriptLocalization.GetWithPara("id_language_lang", new Dictionary<string, string>()
    {
      {
        "0",
        Language.Instance.GetLanguageName(this.m_ItemData.language)
      }
    }, true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Portrait, "Texture/Hero/Portrait_Icon/player_portrait_icon_" + (object) this.m_ItemData.creatorPortrait, (System.Action<bool>) null, true, false, string.Empty);
    LordTitlePayload.Instance.ApplyUserAvator(this.m_Portrait, this.m_ItemData.creatorLordTitleId, 1);
  }

  public void OnClosePressed()
  {
    this.Hide();
  }

  public void OnJoinPressed()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) this.m_ItemData.allianceID;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) => this.SendJoinRequest()), true);
  }

  private void SendJoinRequest()
  {
    AllianceManager.Instance.JoinAlliance(this.m_ItemData.allianceID, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.Hide();
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
      Dictionary<string, string> para = new Dictionary<string, string>();
      para["Alliance_Name"] = allianceData.allianceName;
      ToastManager.Instance.AddToast("BASIC_TOAST", (object) new ToastArgs(ConfigManager.inst.DB_Toast.GetData("toast_alliance_join"), para));
    }));
  }

  [DebuggerHidden]
  private IEnumerator AutoCloseJob()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AllianceRecommendTip.\u003CAutoCloseJob\u003Ec__Iterator24()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
    if (this.m_AutoCloseCoroutine != null)
    {
      this.StopCoroutine(this.m_AutoCloseCoroutine);
      this.m_AutoCloseCoroutine = (Coroutine) null;
    }
    if (this.m_OnHide == null)
      return;
    this.m_OnHide();
  }
}
