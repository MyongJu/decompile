﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightBattleSplashPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class DragonKnightBattleSplashPopup : Popup
{
  public UITexture m_Post;
  public UILabel m_Front;
  public UILabel m_Back;
  public UIButton SkipButton;
  public DragonKnightSpeedUpButton SpeedButton;
  private bool m_Skip;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    DragonKnightBattleSplashPopup.Parameter parameter = orgParam as DragonKnightBattleSplashPopup.Parameter;
    string str1 = !parameter.isBoss ? Utils.XLAT("dragon_knight_start_fight_title") : Utils.XLAT("dragon_knight_start_boss_fight_title");
    if (parameter.isBoss)
      AudioManager.Instance.StopAndPlaySound("sfx_dragon_knight_battle_boss");
    else
      AudioManager.Instance.StopAndPlaySound("sfx_dragon_knight_battle_room");
    UILabel front = this.m_Front;
    string str2 = str1;
    this.m_Back.text = str2;
    string str3 = str2;
    front.text = str3;
    BattleManager.Instance.Setup(parameter.groupId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Post, "Texture/DragonKnight/DungeonTexture/dungeon_battle_start_texture", (System.Action<bool>) null, true, true, string.Empty);
    VIP_Level_Info vipLevelInfo = PlayerData.inst.hostPlayer.VIPLevelInfo;
    bool vipActive = PlayerData.inst.hostPlayer.VIPActive;
    DragonKnightSystem.Instance.Controller.BattleHud.HideButtons();
    this.SpeedButton.gameObject.SetActive(vipActive && (vipLevelInfo.ContainsPrivilege("dk_dungeon_speedup_2") || vipLevelInfo.ContainsPrivilege("dk_dungeon_speedup_4")));
    this.SkipButton.gameObject.SetActive(vipActive && vipLevelInfo.ContainsPrivilege("dk_dungeon_skip"));
    this.SpeedButton.Restore = false;
    this.SpeedButton.callback = (System.Action) (() => DragonKnightSystem.Instance.Controller.BattleHud.SpeedButton.Reset());
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    DragonKnightSystem.Instance.Controller.BattleHud.ShowButtons();
    if (this.m_Skip)
      return;
    DragonKnightAssignedSkill assignedSkill = DBManager.inst.DB_DragonKnightDungeon.Get(PlayerData.inst.uid).AssignedSkill;
    bool flag = DragonKnightSystem.Instance.RoomManager.CurrentRoom.RoomType == "arena";
    if (!NewTutorial.skipTutorial && assignedSkill.GetTalentSkill(1) != 0 && !flag)
    {
      string recordPoint = NewTutorial.Instance.GetRecordPoint("Tutorial_skill_dragon_rider");
      if ("finished" != recordPoint)
      {
        if (TutorialManager.Instance.IsRunning)
          return;
        NewTutorial.Instance.InitTutorial("Tutorial_skill_dragon_rider");
        NewTutorial.Instance.LoadTutorialData(recordPoint);
        TutorialManager.Instance.onTutotialFinished += new System.Action(this.OnTutorialFinished);
      }
      else
        BattleManager.Instance.Start();
    }
    else
      BattleManager.Instance.Start();
  }

  public void OnSkip()
  {
    Time.timeScale = 1f;
    this.m_Skip = true;
    BattleManager.Instance.AutoAttack();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnStartGame()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnTutorialFinished()
  {
    BattleManager.Instance.Start();
    TutorialManager.Instance.onTutotialFinished -= new System.Action(this.OnTutorialFinished);
  }

  public void OnInventoryBtnClick()
  {
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightDungeonInventoryPopup", (Popup.PopupParameter) null);
  }

  public void OnTemporaryItemClicked()
  {
    UIManager.inst.OpenPopup("DungeonTemporaryItemsPopup", (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public bool isBoss;
    public int groupId;
  }
}
