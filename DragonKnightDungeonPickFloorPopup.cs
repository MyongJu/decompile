﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightDungeonPickFloorPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonKnightDungeonPickFloorPopup : Popup
{
  private Dictionary<int, DungeonFloorPickItemRenderer> _itemDict = new Dictionary<int, DungeonFloorPickItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public UIButton dungeonEnterButton;
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  private int _pickedDungeonFloor;
  private int _currentDungeonLevel;
  private int _ablePickDungeonCount;
  private System.Action<int> _onPickButtonPressed;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DragonKnightDungeonPickFloorPopup.Parameter parameter = orgParam as DragonKnightDungeonPickFloorPopup.Parameter;
    if (parameter != null)
    {
      this._currentDungeonLevel = parameter.currentDungeonLevel;
      this._onPickButtonPressed = parameter.onPickButtonPressed;
    }
    this._ablePickDungeonCount = (this._currentDungeonLevel - 1) / 10 + 1;
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.UpdateUI();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnEnterBtnPressed()
  {
    this.OnCloseBtnPressed();
    if (this._onPickButtonPressed == null)
      return;
    this._onPickButtonPressed(this._pickedDungeonFloor);
  }

  private void UpdateUI()
  {
    this.ClearData();
    for (int key = 1; key <= this._ablePickDungeonCount; ++key)
    {
      DungeonFloorPickItemRenderer itemRenderer = this.CreateItemRenderer((key - 1) * 9 + key);
      this._itemDict.Add(key, itemRenderer);
    }
    this.Reposition();
    this.UpdateEnterButtonStatus();
  }

  private void UpdateEnterButtonStatus()
  {
    this.dungeonEnterButton.isEnabled = this._pickedDungeonFloor > 0;
  }

  private void CheckFloorPickedStatus()
  {
    using (Dictionary<int, DungeonFloorPickItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.UpdateFloorPickStatus(false);
    }
  }

  private void OnDungeonFloorPicked(int dungeonFloor, bool picked)
  {
    this.CheckFloorPickedStatus();
    if (picked)
      this._pickedDungeonFloor = dungeonFloor;
    this.UpdateEnterButtonStatus();
  }

  private DungeonFloorPickItemRenderer CreateItemRenderer(int dungeonFloor)
  {
    GameObject gameObject = this._itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    DungeonFloorPickItemRenderer component = gameObject.GetComponent<DungeonFloorPickItemRenderer>();
    component.SetData(dungeonFloor);
    component.onDungeonFloorPicked += new System.Action<int, bool>(this.OnDungeonFloorPicked);
    return component;
  }

  private void ClearData()
  {
    using (Dictionary<int, DungeonFloorPickItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, DungeonFloorPickItemRenderer> current = enumerator.Current;
        current.Value.onDungeonFloorPicked -= new System.Action<int, bool>(this.OnDungeonFloorPicked);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int currentDungeonLevel;
    public System.Action<int> onPickButtonPressed;
  }
}
