﻿// Decompiled with JetBrains decompiler
// Type: AllianceDonateRankItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceDonateRankItem : MonoBehaviour
{
  public UITexture playerIcon;
  public UISprite normalBg_1;
  public UISprite normalBg_2;
  public UISprite firstBg;
  public UISprite secondBg;
  public UISprite thirthBg;
  public UILabel rank;
  public UILabel playerName;
  public UILabel donation;
  public UILabel contruction;
  public UILabel honor;

  public void SetData(AllianceDonateRankItemData data, int index)
  {
    CustomIconLoader.Instance.requestCustomIcon(this.playerIcon, "Texture/Hero/Portrait_Icon/player_portrait_icon_" + (object) data.Portrait, data.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.playerIcon, data.LordTitle, 1);
    this.playerName.text = data.Name;
    this.donation.text = Utils.FormatThousands(data.Donation.ToString());
    this.honor.text = Utils.FormatThousands(data.Honor.ToString());
    this.contruction.text = Utils.FormatThousands(data.Alliance_EXP.ToString());
    this.rank.text = data.Rank.ToString();
    this.HideBg();
    if (data.Rank > 3)
    {
      NGUITools.SetActive(this.normalBg_1.gameObject, index % 2 == 0);
      NGUITools.SetActive(this.normalBg_2.gameObject, index % 2 > 0);
    }
    else if (data.Rank == 1)
      NGUITools.SetActive(this.firstBg.gameObject, true);
    else if (data.Rank == 2)
    {
      NGUITools.SetActive(this.secondBg.gameObject, true);
    }
    else
    {
      if (data.Rank != 3)
        return;
      NGUITools.SetActive(this.thirthBg.gameObject, true);
    }
  }

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.playerIcon);
  }

  private void HideBg()
  {
    NGUITools.SetActive(this.normalBg_1.gameObject, false);
    NGUITools.SetActive(this.normalBg_2.gameObject, false);
    NGUITools.SetActive(this.firstBg.gameObject, false);
    NGUITools.SetActive(this.secondBg.gameObject, false);
    NGUITools.SetActive(this.thirthBg.gameObject, false);
  }
}
