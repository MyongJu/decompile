﻿// Decompiled with JetBrains decompiler
// Type: Toast
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class Toast : MonoBehaviour
{
  public int _backgroundWidth = 2730;
  private bool _enabled = true;
  public UISprite background;
  public UILabel label;
  public UILabel buttonLabel;
  private System.Action _onClose;
  private System.Action _onButtonPressed;
  private IEnumerator _CR;

  public bool Enabled
  {
    get
    {
      return this._enabled;
    }
    set
    {
      this._enabled = value;
      if (this._enabled)
        return;
      this.Hide();
    }
  }

  public void Show(string toastText, System.Action onClose = null, float secondsToShow = 4f, bool onlyOne = true)
  {
    this.Show(toastText, onClose, string.Empty, (System.Action) null, secondsToShow, onlyOne);
  }

  public void Show(string toastText, System.Action onClose, string buttonText, System.Action onButtonPressed, float secondsToShow = 4f, bool onlyOne = true)
  {
    if (!this._enabled || onlyOne && this.background.gameObject.activeSelf)
      return;
    this._onClose = onClose;
    this._onButtonPressed = onButtonPressed;
    this.label.text = toastText;
    this.buttonLabel.text = buttonText;
    this.label.alpha = 0.0f;
    this.gameObject.SetActive(true);
    if (this._CR != null)
      this.StopCoroutine(this._CR);
    this._CR = (IEnumerator) null;
    this.StartCoroutine(this._CR = this.AnimateIn(secondsToShow));
  }

  public void Hide()
  {
    this.background.gameObject.SetActive(false);
    this.gameObject.SetActive(false);
    if (this._CR != null)
      this.StopCoroutine(this._CR);
    this._CR = (IEnumerator) null;
    if (this._onClose == null)
      return;
    this._onClose();
  }

  public void OnButtonPressed()
  {
    if (this._onButtonPressed != null)
      this._onButtonPressed();
    this.Hide();
  }

  [DebuggerHidden]
  private IEnumerator AnimateIn(float secondsToShow)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Toast.\u003CAnimateIn\u003Ec__Iterator9E()
    {
      secondsToShow = secondsToShow,
      \u003C\u0024\u003EsecondsToShow = secondsToShow,
      \u003C\u003Ef__this = this
    };
  }
}
