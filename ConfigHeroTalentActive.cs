﻿// Decompiled with JetBrains decompiler
// Type: ConfigHeroTalentActive
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigHeroTalentActive
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, HeroTalentActiveInfo> datas;
  private Dictionary<int, HeroTalentActiveInfo> dicByUniqueId;
  private List<HeroTalentActiveInfo> listBySlotId;

  public int slotCount { get; private set; }

  public void BuildDB(object res)
  {
    this.parse.Parse<HeroTalentActiveInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.listBySlotId = new List<HeroTalentActiveInfo>();
    Dictionary<string, HeroTalentActiveInfo>.ValueCollection.Enumerator enumerator1 = this.datas.Values.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      if (enumerator1.Current.activeSlotId > this.slotCount)
        this.slotCount = enumerator1.Current.activeSlotId;
    }
    this.listBySlotId = new List<HeroTalentActiveInfo>(this.slotCount + 1);
    for (int index = 0; index <= this.slotCount; ++index)
      this.listBySlotId.Add(new HeroTalentActiveInfo());
    Dictionary<string, HeroTalentActiveInfo>.ValueCollection.Enumerator enumerator2 = this.datas.Values.GetEnumerator();
    while (enumerator2.MoveNext())
      this.listBySlotId[enumerator2.Current.activeSlotId] = enumerator2.Current;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, HeroTalentActiveInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, HeroTalentActiveInfo>) null;
  }

  public bool Contains(int internalId)
  {
    return this.dicByUniqueId.ContainsKey(internalId);
  }

  public bool Contains(string id)
  {
    return this.datas.ContainsKey(id);
  }

  public HeroTalentActiveInfo GetItem(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (HeroTalentActiveInfo) null;
  }

  public HeroTalentActiveInfo GetItem(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (HeroTalentActiveInfo) null;
  }

  public HeroTalentActiveInfo GetSkillBySlotIndex(int index)
  {
    if (index <= this.slotCount)
      return this.listBySlotId[index];
    return (HeroTalentActiveInfo) null;
  }
}
