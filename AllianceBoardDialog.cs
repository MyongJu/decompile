﻿// Decompiled with JetBrains decompiler
// Type: AllianceBoardDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UI;
using UnityEngine;

public class AllianceBoardDialog : UI.Dialog
{
  private List<AllianceBoardItemRenderer> m_ItemList = new List<AllianceBoardItemRenderer>();
  public UIInput m_Input;
  public UITable m_Table;
  public UIScrollView m_ScrollView;
  public GameObject m_OtherPrefab;
  public GameObject m_SelfPrefab;
  private AllianceBoardDialog.Parameter m_Parameter;
  private int m_NeedReposition;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Input.onValidate = new UIInput.OnValidate(this.InputValidator);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (this.m_Parameter != null)
      return;
    this.m_Parameter = orgParam as AllianceBoardDialog.Parameter;
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) this.m_Parameter.allianceData.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:loadComments").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      ArrayList arrayList = data as ArrayList;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        AllianceBoardPayload payload = new AllianceBoardPayload();
        payload.Decode(arrayList[index]);
        this.AddItem(payload);
      }
      this.Reposition();
    }), true);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  private char InputValidator(string text, int charIndex, char addedChar)
  {
    if (this.isValidateCharacter(addedChar))
      return addedChar;
    return char.MinValue;
  }

  private bool isValidateCharacter(char codePoint)
  {
    return char.GetUnicodeCategory(codePoint) != UnicodeCategory.Surrogate;
  }

  public void OnSendMessage()
  {
    if (string.IsNullOrEmpty(this.m_Input.value))
      return;
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) this.m_Parameter.allianceData.allianceId;
    postData[(object) "content"] = (object) this.m_Input.value;
    MessageHub.inst.GetPortByAction("Alliance:addComment").SendLoader(postData, (System.Action<bool, object>) null, true, false);
    this.AddItem(new AllianceBoardPayload()
    {
      uid = PlayerData.inst.uid,
      portrait = PlayerData.inst.userData.portrait,
      name = PlayerData.inst.userData.userName,
      content = IllegalWordsUtils.Filter(this.m_Input.value),
      ctime = (long) NetServerTime.inst.ServerTimestamp,
      icon = PlayerData.inst.userData.Icon
    });
    this.m_Input.value = string.Empty;
    this.Reposition();
  }

  private void Reposition()
  {
    this.m_NeedReposition = 2;
  }

  private void Update()
  {
    if (this.m_NeedReposition <= 0)
      return;
    --this.m_NeedReposition;
    if (this.m_NeedReposition != 0)
      return;
    this.m_Table.sorting = UITable.Sorting.Custom;
    this.m_Table.onCustomSort = new Comparison<Transform>(AllianceBoardDialog.Compare);
    this.m_Table.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  private static int Compare(Transform x, Transform y)
  {
    AllianceBoardItemRenderer component = x.GetComponent<AllianceBoardItemRenderer>();
    return (int) (y.GetComponent<AllianceBoardItemRenderer>().Payload.ctime - component.Payload.ctime);
  }

  private void AddItem(AllianceBoardPayload payload)
  {
    GameObject gameObject = payload.uid != PlayerData.inst.uid ? Utils.DuplicateGOB(this.m_OtherPrefab, this.m_Table.transform) : Utils.DuplicateGOB(this.m_SelfPrefab, this.m_Table.transform);
    gameObject.SetActive(true);
    AllianceBoardItemRenderer component = gameObject.GetComponent<AllianceBoardItemRenderer>();
    component.SetData(payload);
    this.m_ItemList.Add(component);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public AllianceData allianceData;
  }
}
