﻿// Decompiled with JetBrains decompiler
// Type: ConfigParse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class ConfigParse
{
  private Dictionary<string, ParseFunc> parseFuncs = new Dictionary<string, ParseFunc>();
  private const string INTERNAL_ID = "internalId";
  private Dictionary<string, int> indexs;
  private System.Type typeInfo;
  private ArrayList headers;
  private FieldInfo[] fieldInfos;

  public void SetHeader(ArrayList header)
  {
    this.headers = header;
  }

  public void Parse<T>(Hashtable sources, out List<T> container)
  {
    if (this.headers == null)
      this.Init<T>(sources);
    container = new List<T>();
    IEnumerator enumerator = sources.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      ArrayList source = sources[(object) enumerator.Current.ToString()] as ArrayList;
      T instance;
      int internalId;
      if (this.ParseOne<T>(source, (object) enumerator.Current.ToString(), out instance, out internalId))
      {
        this.Set<T>(instance, source, internalId);
        container.Add(instance);
      }
    }
  }

  public void AddParseFunc(string key, ParseFunc handler)
  {
    if (this.parseFuncs.ContainsKey(key))
      return;
    this.parseFuncs.Add(key, handler);
  }

  public bool ParseOne<T>(ArrayList datas, object htKey, out T instance, out int internalId)
  {
    instance = Activator.CreateInstance<T>();
    if (!int.TryParse(htKey.ToString(), out internalId) || datas == null)
      return false;
    if (this.headers == null)
    {
      D.error((object) "config parse is not init!");
      return false;
    }
    this.Set<T>(instance, datas, internalId);
    return true;
  }

  protected bool ParseOne<T>(ArrayList datas, object htKey, out T instance, out long internalId)
  {
    instance = Activator.CreateInstance<T>();
    if (!long.TryParse(htKey.ToString(), out internalId) || datas == null)
      return false;
    if (this.headers == null)
    {
      D.error((object) "config parse is not init!");
      return false;
    }
    this.Set<T>(instance, datas, internalId);
    return true;
  }

  public void Parse<T>(Hashtable sources, out Dictionary<int, T> container)
  {
    if (this.headers == null)
      this.Init<T>(sources);
    container = new Dictionary<int, T>();
    IEnumerator enumerator = sources.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      T instance;
      int internalId;
      if (this.ParseOne<T>(sources[(object) enumerator.Current.ToString()] as ArrayList, (object) enumerator.Current.ToString(), out instance, out internalId))
        container.Add(internalId, instance);
    }
  }

  public void Parse<T, T1>(Hashtable sources, string keyName, out Dictionary<T1, T> container, out Dictionary<int, T> container1)
  {
    if (this.headers == null)
      this.Init<T>(sources);
    container = new Dictionary<T1, T>();
    container1 = new Dictionary<int, T>();
    IEnumerator enumerator = sources.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      T instance;
      int internalId;
      if (this.ParseOne<T>(sources[(object) enumerator.Current.ToString()] as ArrayList, (object) enumerator.Current.ToString(), out instance, out internalId))
      {
        T1 key = (T1) Convert.ChangeType(this.typeInfo.GetField(keyName).GetValue((object) instance), typeof (T1));
        container.Add(key, instance);
        container1.Add(internalId, instance);
      }
    }
  }

  public void Parse<T, T1>(Hashtable sources, string keyName, out Dictionary<T1, T> container, out Dictionary<long, T> container1)
  {
    if (this.headers == null)
      this.Init<T>(sources);
    container = new Dictionary<T1, T>();
    container1 = new Dictionary<long, T>();
    IEnumerator enumerator = sources.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      T instance;
      long internalId;
      if (this.ParseOne<T>(sources[(object) enumerator.Current.ToString()] as ArrayList, (object) enumerator.Current.ToString(), out instance, out internalId))
      {
        T1 key = (T1) Convert.ChangeType(this.typeInfo.GetField(keyName).GetValue((object) instance), typeof (T1));
        container.Add(key, instance);
        container1.Add(internalId, instance);
      }
    }
  }

  public void Parse<T, T1>(Hashtable sources, string keyName, out Dictionary<T1, T> container)
  {
    if (this.headers == null)
      this.Init<T>(sources);
    container = new Dictionary<T1, T>();
    IEnumerator enumerator = sources.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      ArrayList source = sources[(object) enumerator.Current.ToString()] as ArrayList;
      T instance;
      int internalId;
      if (this.ParseOne<T>(source, (object) enumerator.Current.ToString(), out instance, out internalId))
      {
        this.Set<T>(instance, source, internalId);
        T1 key = (T1) Convert.ChangeType(this.typeInfo.GetField(keyName).GetValue((object) instance), typeof (T1));
        container.Add(key, instance);
      }
    }
  }

  public void Clear()
  {
    if (this.indexs != null)
    {
      this.indexs.Clear();
      this.indexs = (Dictionary<string, int>) null;
    }
    this.headers = (ArrayList) null;
    this.typeInfo = (System.Type) null;
    this.fieldInfos = (FieldInfo[]) null;
    this.parseFuncs.Clear();
  }

  public void SetFiledIndexs(Dictionary<string, int> indexs)
  {
    this.indexs = indexs;
  }

  private void Init<T>(Hashtable sources)
  {
    if (this.indexs != null)
      this.indexs.Clear();
    else
      this.indexs = new Dictionary<string, int>();
    this.typeInfo = typeof (T);
    this.headers = sources[(object) "headers"] as ArrayList;
    if (this.headers == null)
    {
      D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exsit", (object) this.typeInfo.FullName);
    }
    else
    {
      this.fieldInfos = this.typeInfo.GetFields();
      for (int index1 = 0; index1 < this.fieldInfos.Length; ++index1)
      {
        object[] customAttributes = this.fieldInfos[index1].GetCustomAttributes(typeof (ConfigAttribute), false);
        if (customAttributes != null && customAttributes.Length != 0)
        {
          ConfigAttribute configAttribute = (ConfigAttribute) customAttributes[0];
          string empty = string.Empty;
          if (configAttribute.IsArray)
          {
            for (int index2 = 0; index2 < configAttribute.ArraySize; ++index2)
            {
              string str = string.Format("{0}{1}", (object) configAttribute.Name, (object) (index2 + 1));
              int index3 = ConfigManager.GetIndex(this.headers, str);
              this.indexs.Add(str, index3);
            }
          }
          else
            this.indexs.Add(configAttribute.Name, ConfigManager.GetIndex(this.headers, configAttribute.Name));
        }
      }
      if (this.indexs.Count != 0)
        return;
      D.error((object) " can not parse empty configAttribute array !");
    }
  }

  public void Set<T>(T instance, ArrayList datas, int internalId)
  {
    if (this.fieldInfos.Length <= 0)
      return;
    for (int index1 = 0; index1 < this.fieldInfos.Length; ++index1)
    {
      FieldInfo fieldInfo = this.fieldInfos[index1];
      if (fieldInfo.Name == nameof (internalId))
      {
        fieldInfo.SetValue((object) instance, (object) internalId);
      }
      else
      {
        object[] customAttributes = fieldInfo.GetCustomAttributes(typeof (ConfigAttribute), false);
        if (customAttributes.Length == 0 && this.indexs.ContainsKey(fieldInfo.Name))
        {
          int index2 = this.indexs[fieldInfo.Name];
          object rawData = ConfigManager.GetRawData(datas, index2);
          if (!string.IsNullOrEmpty(rawData.ToString()))
          {
            object obj = this.CovertValue(fieldInfo.FieldType, rawData, (ConfigAttribute) null);
            fieldInfo.SetValue((object) instance, obj);
          }
          else
            continue;
        }
        if (customAttributes.Length > 0)
        {
          for (int index2 = 0; index2 < customAttributes.Length; ++index2)
          {
            ConfigAttribute att = customAttributes[index2] as ConfigAttribute;
            if (!att.IsArray)
            {
              int index3 = this.indexs[att.Name];
              object rawData = ConfigManager.GetRawData(datas, index3);
              if (!string.IsNullOrEmpty(rawData.ToString()))
              {
                object obj = this.CovertValue(fieldInfo.FieldType, rawData, att);
                fieldInfo.SetValue((object) instance, obj);
              }
            }
            else
            {
              ArrayList arrayList = new ArrayList();
              for (int index3 = 0; index3 < att.ArraySize; ++index3)
              {
                int index4 = this.indexs[att.Name + (object) (index3 + 1)];
                object rawData = ConfigManager.GetRawData(datas, index4);
                if (!string.IsNullOrEmpty(rawData.ToString()))
                {
                  string str = fieldInfo.FieldType.ToString();
                  System.Type type = System.Type.GetType(str.Substring(0, str.Length - 2));
                  arrayList.Add(this.CovertValue(type, rawData, att));
                }
              }
              fieldInfo.SetValue((object) instance, (object) arrayList.ToArray());
            }
          }
        }
      }
    }
  }

  public void SetFiledInfos(FieldInfo[] infos)
  {
    this.fieldInfos = infos;
  }

  public void Set<T>(T instance, ArrayList datas, long internalId)
  {
    if (this.fieldInfos.Length <= 0)
      return;
    for (int index1 = 0; index1 < this.fieldInfos.Length; ++index1)
    {
      FieldInfo fieldInfo = this.fieldInfos[index1];
      if (fieldInfo.Name == nameof (internalId))
      {
        fieldInfo.SetValue((object) instance, (object) internalId);
      }
      else
      {
        object[] customAttributes = fieldInfo.GetCustomAttributes(typeof (ConfigAttribute), false);
        if (customAttributes.Length == 0 && this.indexs.ContainsKey(fieldInfo.Name))
        {
          int index2 = this.indexs[fieldInfo.Name];
          object rawData = ConfigManager.GetRawData(datas, index2);
          if (!string.IsNullOrEmpty(rawData.ToString()))
          {
            object obj = this.CovertValue(fieldInfo.FieldType, rawData, (ConfigAttribute) null);
            fieldInfo.SetValue((object) instance, obj);
          }
          else
            continue;
        }
        if (customAttributes.Length > 0)
        {
          for (int index2 = 0; index2 < customAttributes.Length; ++index2)
          {
            ConfigAttribute att = customAttributes[index2] as ConfigAttribute;
            if (!att.IsArray)
            {
              int index3 = this.indexs[att.Name];
              object rawData = ConfigManager.GetRawData(datas, index3);
              if (!string.IsNullOrEmpty(rawData.ToString()))
              {
                object obj = this.CovertValue(fieldInfo.FieldType, rawData, att);
                fieldInfo.SetValue((object) instance, obj);
              }
            }
            else
            {
              ArrayList arrayList = new ArrayList();
              for (int index3 = 0; index3 < att.ArraySize; ++index3)
              {
                int index4 = this.indexs[att.Name + (object) (index3 + 1)];
                object rawData = ConfigManager.GetRawData(datas, index4);
                if (!string.IsNullOrEmpty(rawData.ToString()))
                {
                  string str = fieldInfo.FieldType.ToString();
                  System.Type type = System.Type.GetType(str.Substring(0, str.Length - 2));
                  arrayList.Add(this.CovertValue(type, rawData, att));
                }
              }
              fieldInfo.SetValue((object) instance, (object) arrayList.ToArray());
            }
          }
        }
      }
    }
  }

  private object CustomParse(System.Type typeinfo, object content)
  {
    object instance = Activator.CreateInstance(typeinfo);
    if (instance is ICustomParse)
      ((ICustomParse) instance).Parse(content);
    return instance;
  }

  private object CovertValue(System.Type targetType, object value, ConfigAttribute att = null)
  {
    object obj1 = (object) null;
    if (targetType.IsEnum)
    {
      if (Enum.IsDefined(targetType, value))
        obj1 = Enum.Parse(targetType, value.ToString());
    }
    else
    {
      if (att != null && att.IsTimeSpan)
        value = (object) (Convert.ToDateTime(value).ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds;
      string str1 = value.ToString().Trim();
      if (targetType.ToString() == typeof (bool).ToString() && (str1 == "0" || str1 == "1"))
        str1 = !(str1 == "0") ? "true" : "false";
      if (att != null)
      {
        if (att.IsArrayList)
        {
          ArrayList arrayList1 = value as ArrayList;
          ArrayList arrayList2 = new ArrayList();
          if (arrayList1 == null)
          {
            ArrayList arrayList3 = new ArrayList();
            Hashtable hashtable = value as Hashtable;
            List<string> stringList = (List<string>) null;
            Dictionary<string, string> dictionary = (Dictionary<string, string>) null;
            if (att.NeedValueSort)
            {
              dictionary = new Dictionary<string, string>();
              stringList = new List<string>();
            }
            if (hashtable != null)
            {
              IEnumerator enumerator = hashtable.Keys.GetEnumerator();
              while (enumerator.MoveNext())
              {
                string str2 = enumerator.Current.ToString();
                arrayList2.Add((object) str2);
                if (dictionary != null)
                {
                  try
                  {
                    string key = hashtable[enumerator.Current].ToString();
                    dictionary.Add(key, str2);
                    stringList.Add(key);
                  }
                  catch
                  {
                  }
                }
              }
            }
            if (att.NeedValueSort)
            {
              stringList.Sort(new Comparison<string>(this.Compare));
              arrayList2.Clear();
              for (int index1 = 0; index1 < stringList.Count; ++index1)
              {
                string index2 = stringList[index1];
                arrayList2.Add((object) dictionary[index2]);
              }
            }
            return (object) arrayList2;
          }
          for (int index = 0; index < arrayList1.Count; ++index)
          {
            if (att.CustomType != null && att.CustomParse)
            {
              object obj2 = this.CustomParse(att.CustomType, arrayList1[index]);
              arrayList2.Add(obj2);
            }
            else
              arrayList2.Add(arrayList1[index]);
          }
          return (object) arrayList2;
        }
        if (att.CustomParse)
        {
          if (!string.IsNullOrEmpty(att.ParseFuncKey) && this.parseFuncs.ContainsKey(att.ParseFuncKey))
            return this.parseFuncs[att.ParseFuncKey](value);
          return this.CustomParse(att.CustomType, value);
        }
      }
      obj1 = Convert.ChangeType((object) str1, targetType);
    }
    return obj1;
  }

  private int Compare(object a, object b)
  {
    int result1;
    int result2;
    if (int.TryParse(a.ToString(), out result1) && int.TryParse(b.ToString(), out result2))
      return result1.CompareTo(result2);
    return 0;
  }
}
