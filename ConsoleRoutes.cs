﻿// Decompiled with JetBrains decompiler
// Type: ConsoleRoutes
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Net;

internal static class ConsoleRoutes
{
  [ConsoleRoute("^/console/out$", "(GET|HEAD)")]
  public static bool Output(HttpListenerContext context)
  {
    context.Response.WriteString(Console.Output(), "text/plain");
    return true;
  }

  [ConsoleRoute("^/console/run$", "(GET|HEAD)")]
  public static bool Run(HttpListenerContext context)
  {
    string str = context.Request.QueryString.Get("command");
    if (!string.IsNullOrEmpty(str))
      Console.Run(str);
    context.Response.StatusCode = 200;
    context.Response.StatusDescription = "OK";
    return true;
  }

  [ConsoleRoute("^/console/commandHistory$", "(GET|HEAD)")]
  public static bool History(HttpListenerContext context)
  {
    string s = context.Request.QueryString.Get("index");
    string input = (string) null;
    if (!string.IsNullOrEmpty(s))
      input = Console.PreviousCommand(int.Parse(s));
    context.Response.WriteString(input, "text/plain");
    return true;
  }

  [ConsoleRoute("^/console/complete$", "(GET|HEAD)")]
  public static bool Complete(HttpListenerContext context)
  {
    string partialCommand = context.Request.QueryString.Get("command");
    string input = (string) null;
    if (partialCommand != null)
      input = Console.Complete(partialCommand);
    context.Response.WriteString(input, "text/plain");
    return true;
  }
}
