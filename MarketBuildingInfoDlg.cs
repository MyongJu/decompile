﻿// Decompiled with JetBrains decompiler
// Type: MarketBuildingInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;

public class MarketBuildingInfoDlg : BuildingInfoBaseDlg
{
  public UITexture texture1;
  public UILabel nameLabel1;
  public UILabel valueLabel1;
  public UITexture texture2;
  public UILabel nameLabel2;
  public UILabel valueLabel2;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    if (this.buildingInfo.Benefit_ID_1.Length > 0)
      this.DrawBenefit(this.buildingInfo.Benefit_Value_1, this.nameLabel1, this.valueLabel1, int.Parse(this.buildingInfo.Benefit_ID_1), this.buildingInfo.Benefit_Value_1, "calc_trade_capacity", this.texture1);
    if (this.buildingInfo.Benefit_ID_2.Length <= 0)
      return;
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[int.Parse(this.buildingInfo.Benefit_ID_2)];
    float num = (float) this.buildingInfo.Benefit_Value_2 - ConfigManager.inst.DB_BenefitCalc.GetFinalData(0.0f, "calc_tradetax");
    this.nameLabel2.text = dbProperty.Name;
    this.valueLabel2.text = Math.Round(this.buildingInfo.Benefit_Value_2 * 100.0, 2).ToString() + "%[00FF00]-" + (object) Math.Round((double) num * 100.0, 2) + "%[-]";
  }

  private void DrawBenefit(double benifitValue, UILabel nameLabel, UILabel valueLabel, int benefitInternalID, double baseValue, string caclID, UITexture texture = null)
  {
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitInternalID];
    nameLabel.text = dbProperty.Name;
    if ((UnityEngine.Object) texture != (UnityEngine.Object) null)
      BuilderFactory.Instance.HandyBuild((UIWidget) texture, dbProperty.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    float finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) baseValue, caclID);
    string str = (double) finalData <= 0.0 ? "-" : string.Empty;
    if (dbProperty.Format == PropertyDefinition.FormatType.Percent)
      valueLabel.text = dbProperty.ConvertToDisplayString(baseValue, false, true) + str + dbProperty.ConvertToDisplayString(Math.Abs((double) finalData - baseValue), true, true);
    else
      valueLabel.text = dbProperty.ConvertToDisplayString(baseValue, false, true) + str + dbProperty.ConvertToDisplayString(Math.Abs((double) finalData - baseValue), true, true);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    BuilderFactory.Instance.Release((UIWidget) this.texture1);
    BuilderFactory.Instance.Release((UIWidget) this.texture2);
    base.OnClose(orgParam);
  }
}
