﻿// Decompiled with JetBrains decompiler
// Type: CityTouchCircle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class CityTouchCircle : SceneUI
{
  public float gapX = 100f;
  public float gapY = 40f;
  public float openTime = 1f;
  public float gapTime = 0.5f;
  public float tarScale = 0.5f;
  public float dropDistance = -50f;
  private int maxBtn = 7;
  private List<CityCircleBtnPara> typeList = new List<CityCircleBtnPara>();
  private Vector3 recordBgPos = Vector3.zero;
  private static CityTouchCircle _instance;
  public UILabel lblTitle;
  public CityTouchCircleBtn btnTemplate;
  public GameObject titleGroup;
  public GameObject bg;
  public Transform btnGroup;
  private List<CityTouchCircleBtn> btnList;
  private List<Vector3> btnTarPos;
  private string title;

  public static CityTouchCircle Instance
  {
    get
    {
      if ((UnityEngine.Object) CityTouchCircle._instance == (UnityEngine.Object) null)
      {
        CityTouchCircle._instance = (UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/City/CityTouchCircle", (System.Type) null)) as GameObject).GetComponent<CityTouchCircle>();
        if ((UnityEngine.Object) null == (UnityEngine.Object) CityTouchCircle._instance)
          throw new ArgumentException("lost of CityTouchCircle");
        CityTouchCircle._instance.Init();
      }
      return CityTouchCircle._instance;
    }
  }

  private Vector3 Distance
  {
    get
    {
      if (this.HasAction())
        return new Vector3(0.0f, this.dropDistance, 0.0f);
      return Vector3.zero;
    }
  }

  public static bool IsAvailable
  {
    get
    {
      return (UnityEngine.Object) CityTouchCircle._instance != (UnityEngine.Object) null;
    }
  }

  public List<CityCircleBtnPara> TypeList
  {
    get
    {
      return this.typeList;
    }
    set
    {
      this.typeList = value;
    }
  }

  public string Title
  {
    get
    {
      return this.title;
    }
    set
    {
      this.title = value;
      this.lblTitle.text = this.title;
    }
  }

  public void Init()
  {
    this.btnList = new List<CityTouchCircleBtn>(this.maxBtn);
    for (int index = 0; index < this.maxBtn; ++index)
    {
      CityTouchCircleBtn cityTouchCircleBtn = UnityEngine.Object.Instantiate<CityTouchCircleBtn>(this.btnTemplate);
      cityTouchCircleBtn.transform.parent = this.btnTemplate.transform.parent;
      this.btnList.Add(cityTouchCircleBtn);
    }
    this.recordBgPos = this.bg.transform.localPosition;
    this.Reset();
    this.btnTemplate.gameObject.SetActive(false);
    CityCircleBtnCollection.Instance.Create();
  }

  private void UpdateData()
  {
    if (this.TypeList.Count == 0)
      return;
    this.btnTarPos = new List<Vector3>(this.TypeList.Count);
    for (int index = 0; index < this.TypeList.Count; ++index)
      this.btnList[index].GetComponent<CityTouchCircleBtn>().Init(this.TypeList[index]);
    Utils.ArrangePosition(this.btnTemplate.transform.localPosition, this.TypeList.Count, this.gapX, this.gapY, ref this.btnTarPos);
    BuildingInfo selectedBuildingInfo = CitadelSystem.inst.GetCurSelectedBuildingInfo();
    if (selectedBuildingInfo == null)
      return;
    this.Title = ScriptLocalization.Get(selectedBuildingInfo.Building_LOC_ID, true);
  }

  private void PlayBtnAnimation()
  {
    if (this.TypeList == null || this.TypeList.Count == 0)
      return;
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) 0, (object) "to", (object) 1, (object) "time", (object) this.openTime, (object) "easetype", (object) iTween.EaseType.easeOutBack, (object) "onupdate", (object) "CircleAnimation", (object) "oncomplete", (object) "OnAnimationComplete"));
  }

  private void CircleAnimation(float value)
  {
    for (int index = this.TypeList.Count - 1; index >= 0; --index)
    {
      GameObject gameObject = this.btnList[index].gameObject;
      gameObject.transform.localPosition = (this.btnTarPos[index] + this.Distance) * value;
      gameObject.transform.localScale = new Vector3(this.tarScale, this.tarScale, 1f) * value;
    }
    this.titleGroup.transform.localScale = new Vector3(value, 1f, 1f);
    this.bg.transform.localPosition = (this.recordBgPos + this.Distance) * value;
  }

  private void OnAnimationComplete()
  {
    this.CircleAnimation(1f);
    UIManager.inst.cityCamera.FocusTarget(this.btnGroup, CityCamera.FocusMode.Edge);
  }

  public void CloseSelf()
  {
    CitadelSystem.inst.OnCityMapClicked();
  }

  private void Reset()
  {
    for (int index = 0; index < this.btnList.Count; ++index)
    {
      this.btnList[index].transform.localPosition = Vector3.zero;
      this.btnList[index].transform.localScale = Vector3.zero;
      this.btnList[index].Reset();
    }
    this.titleGroup.transform.localScale = new Vector3(0.0f, 1f, 1f);
    this.bg.transform.localPosition = Vector3.zero;
    if (this.btnTarPos != null)
      this.btnTarPos.Clear();
    using (List<CityCircleBtnPara>.Enumerator enumerator = this.TypeList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Reset();
    }
    this.TypeList.Clear();
  }

  private bool HasAction()
  {
    return !((UnityEngine.Object) null == (UnityEngine.Object) this.transform.parent.GetComponentInChildren<CityTimerbar>());
  }

  private void OnSpeedUpBtnClicked()
  {
    CityTimerbar cityTimerbar = this.GetCityTimerbar();
    if (!((UnityEngine.Object) cityTimerbar != (UnityEngine.Object) null))
      return;
    cityTimerbar.OnSpeedUpBtnPressed();
  }

  private CityTimerbar GetCityTimerbar()
  {
    foreach (CityTimerbar componentsInChild in this.transform.parent.GetComponentsInChildren<CityTimerbar>())
    {
      if (!componentsInChild.IsDragonHatching)
        return componentsInChild;
    }
    return (CityTimerbar) null;
  }

  private void OnUseGoldSpeedUpBtnClicked()
  {
    CityTimerbar cityTimerbar = this.GetCityTimerbar();
    CitadelSystem.inst.GetCurSelectedBuildingInfo();
    JobHandle jobHandle = JobManager.Instance.GetUnfinishedJob(BuildingController.mSelectedBuildingItem.mBuildingJobID);
    if (jobHandle == null)
    {
      jobHandle = JobManager.Instance.GetUnfinishedJob(BuildingController.mSelectedBuildingItem.mTrainingJobID);
      if (BuildingController.mSelectedBuildingItem.mType == "hospital" && jobHandle == null)
        jobHandle = JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_HEALING_TROOPS);
      if (BuildingController.mSelectedBuildingItem.mType == "university" && jobHandle == null)
        jobHandle = JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_RESEARCH);
    }
    if (!((UnityEngine.Object) null != (UnityEngine.Object) cityTimerbar) || jobHandle == null)
      return;
    int num1 = CityManager.inst.GetFreeSpeedUpTime();
    if (cityTimerbar.timerType == TimerType.TIMER_HEAL || cityTimerbar.timerType == TimerType.TIMER_TRAIN || (cityTimerbar.timerType == TimerType.TIMER_RESEARCH || cityTimerbar.timerType == TimerType.TIMER_FORGE) || (cityTimerbar.timerType == TimerType.TIMER_DK_FORGE || cityTimerbar.timerType == TimerType.TIMER_TRAP_BUILD))
      num1 = 0;
    int num2 = jobHandle.LeftTime();
    int time = num2 - num1;
    GoldConsumePopup.Parameter parameter = new GoldConsumePopup.Parameter();
    int cost = ItemBag.CalculateCost(time, 0);
    parameter.confirmButtonClickEvent = new System.Action(this.ConFirmCallBack);
    parameter.lefttime = num2;
    parameter.goldNum = cost;
    parameter.freetime = num1;
    parameter.description = ScriptLocalization.GetWithPara("confirm_gold_spend_upgrade_instant_desc", new Dictionary<string, string>()
    {
      {
        "num",
        cost.ToString()
      }
    }, true);
    UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) parameter);
  }

  private void ConFirmCallBack()
  {
    CityTimerbar cityTimerbar = this.GetCityTimerbar();
    BuildingController bc = cityTimerbar.transform.parent.GetComponent<BuildingController>();
    JobHandle jobHandle = JobManager.Instance.GetUnfinishedJob(BuildingController.mSelectedBuildingItem.mBuildingJobID);
    if (jobHandle == null)
    {
      jobHandle = JobManager.Instance.GetUnfinishedJob(BuildingController.mSelectedBuildingItem.mTrainingJobID);
      if (BuildingController.mSelectedBuildingItem.mType == "hospital" && jobHandle == null)
        jobHandle = JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_HEALING_TROOPS);
      if (BuildingController.mSelectedBuildingItem.mType == "university" && jobHandle == null)
        jobHandle = JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_RESEARCH);
    }
    if (!((UnityEngine.Object) cityTimerbar != (UnityEngine.Object) null) || jobHandle == null)
      return;
    int num1 = jobHandle.LeftTime();
    long num2 = jobHandle.ServerJobID;
    if (num2 <= 0L)
      num2 = jobHandle.GetJobID();
    int num3 = CityManager.inst.GetFreeSpeedUpTime();
    if (cityTimerbar.timerType == TimerType.TIMER_HEAL || cityTimerbar.timerType == TimerType.TIMER_TRAIN || (cityTimerbar.timerType == TimerType.TIMER_RESEARCH || cityTimerbar.timerType == TimerType.TIMER_FORGE) || (cityTimerbar.timerType == TimerType.TIMER_DK_FORGE || cityTimerbar.timerType == TimerType.TIMER_TRAP_BUILD))
      num3 = 0;
    if (num1 - num3 > 0)
    {
      int cost = ItemBag.CalculateCost(num1 - num3, 0);
      RequestManager.inst.SendRequest("City:speedUpByGold", new Hashtable()
      {
        {
          (object) "job_id",
          (object) num2
        },
        {
          (object) "gold",
          (object) cost
        }
      }, (System.Action<bool, object>) ((arg1, arg2) =>
      {
        if (!arg1 || !((UnityEngine.Object) bc != (UnityEngine.Object) null))
          return;
        bc.UpgradeFinished(true);
      }), true);
    }
    else
      cityTimerbar.OnFreeBtnPressed();
  }

  public override void Open(Transform parent)
  {
    base.Open(parent);
    Vector3 localPosition = this.transform.localPosition;
    localPosition.z += -10f;
    this.transform.localPosition = localPosition;
    this.UpdateData();
    this.PlayBtnAnimation();
    this.ChangeParentBCSts(false);
  }

  public override void Close()
  {
    base.Close();
    if ((UnityEngine.Object) null != (UnityEngine.Object) CitadelSystem.inst.TwinklerInst)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) CitadelSystem.inst.TwinklerInst);
      CitadelSystem.inst.TwinklerInst = (Twinkler) null;
    }
    LinkerHub.Instance.NeedGuide = false;
    this.ChangeParentBCSts(true);
    this.Reset();
  }

  public void Dispose()
  {
    this.Close();
    CityTouchCircle._instance = (CityTouchCircle) null;
  }

  private void Start()
  {
  }

  private void Update()
  {
    this.ScaleInCity();
  }
}
