﻿// Decompiled with JetBrains decompiler
// Type: IAPDailyPackageInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class IAPDailyPackageInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "type")]
  public int type;
  [Config(Name = "pay_id")]
  public string pay_id;
  [Config(Name = "discount")]
  public double discount;
  [Config(Name = "start_time")]
  public int start_time;
  [Config(Name = "end_time")]
  public int end_time;
  [Config(Name = "reward_group_id")]
  public string reward_group_id;
  [Config(Name = "sub_reward_group_id")]
  public string sub_reward_group_id;
}
