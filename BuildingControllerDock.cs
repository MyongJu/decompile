﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerDock
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Funplus;
using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UnityEngine;

public class BuildingControllerDock : BuildingControllerNew
{
  public GameObject activehint;
  public GameObject collecthint;

  public override void InitBuilding()
  {
    base.InitBuilding();
    if (Utils.IsIAPStoreEnabled)
    {
      this.onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
      this.CheckState();
      DBManager.inst.DB_MonthCard.onDataUpdated += new System.Action<long>(this.OnDBChanged);
      SubscriptionEvents.updateEvent.AddListener(new System.Action(this.CheckState));
    }
    else
    {
      this.activehint.SetActive(false);
      this.collecthint.SetActive(false);
    }
  }

  public override void Dispose()
  {
    base.Dispose();
    this.onBuildingSelected -= new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    DBManager.inst.DB_MonthCard.onDataUpdated -= new System.Action<long>(this.OnDBChanged);
    SubscriptionEvents.updateEvent.RemoveListener(new System.Action(this.CheckState));
  }

  private void OnDBChanged(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.CheckState();
  }

  private void CheckState()
  {
    this.activehint.SetActive(false);
    this.collecthint.SetActive(false);
    DBManager.inst.DB_MonthCard.Get(PlayerData.inst.uid);
    bool flag = this.IsMonthCardActive();
    SubscriptionSystem.Instance.IsActive();
    if (this.IsMonthCardCanCollect() || SubscriptionSystem.Instance.CanCollect())
    {
      this.collecthint.SetActive(true);
    }
    else
    {
      if (flag)
        return;
      this.activehint.SetActive(true);
    }
  }

  private bool IsMonthCardActive()
  {
    return NetServerTime.inst.UpdateTime < DBManager.inst.DB_MonthCard.Get(PlayerData.inst.uid).expiration_date;
  }

  private bool IsMonthCardCanCollect()
  {
    MonthCardData monthCardData = DBManager.inst.DB_MonthCard.Get(PlayerData.inst.uid);
    bool flag1 = this.IsMonthCardActive();
    bool flag2 = NetServerTime.inst.IsToday(monthCardData.last_award_time);
    if (flag1)
      return !flag2;
    return false;
  }

  private void OnBuildingSelected()
  {
    CitadelSystem.inst.OpenTouchCircle();
  }

  public void OpenMonthCard()
  {
    if (!Application.isEditor)
      FunplusBi.Instance.TraceEvent("clickbuilding", JsonWriter.Serialize((object) new BuildingControllerNew.BIFormat()
      {
        d_c1 = new BuildingControllerNew.BIClick()
        {
          key = "monthcard",
          value = "click"
        }
      }));
    AudioManager.Instance.StopAndPlaySound("sfx_city_click_signin_rewards");
    if (SubscriptionSystem.Instance.OpenSubscribe)
    {
      bool flag1 = this.IsMonthCardActive();
      bool flag2 = SubscriptionSystem.Instance.IsActive();
      bool flag3 = this.IsMonthCardCanCollect();
      bool flag4 = SubscriptionSystem.Instance.CanCollect();
      if (flag3)
        this.ShowMonthCard();
      else if (flag4)
        SubscriptionSystem.Instance.Start();
      else if (!flag1)
        this.ShowMonthCard();
      else if (!flag2)
        SubscriptionSystem.Instance.Start();
    }
    else
      this.ShowMonthCard();
    OperationTrace.TraceClick(ClickArea.MonthCardOutSide);
  }

  private void ShowMonthCard()
  {
    Utils.ShowMonthCardPopup();
  }

  public override void AddActionButton(ref List<CityCircleBtnPara> ftl)
  {
    AudioManager.Instance.PlaySound("sfx_city_click_tavern", false);
    CityTouchCircle.Instance.Title = Utils.XLAT("daily_delivery_building_name");
    CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_month_card", "id_daily_delivery", new EventDelegate((EventDelegate.Callback) (() => this.ShowMonthCard())), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ftl.Add(cityCircleBtnPara1);
    if (!SubscriptionSystem.Instance.OpenSubscribe || CustomDefine.Channel != CustomDefine.ChannelEnum.GooglePlay)
      return;
    CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, SubscriptionSystem.Instance.CircleMenuIcon, SubscriptionSystem.Instance.CircleMenuTextKey, new EventDelegate((EventDelegate.Callback) (() => SubscriptionSystem.Instance.Start())), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ftl.Add(cityCircleBtnPara2);
  }
}
