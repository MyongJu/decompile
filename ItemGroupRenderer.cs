﻿// Decompiled with JetBrains decompiler
// Type: ItemGroupRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ItemGroupRenderer : MonoBehaviour
{
  private Dictionary<int, ItemRenderer> dict = new Dictionary<int, ItemRenderer>();
  private List<int> indexs = new List<int>();
  private List<int> refreshList = new List<int>();
  public UISprite[] bgs;
  public ItemRenderer[] itemRenderers;
  public UIButton useBt;
  public UIButton composeBt;
  public UILabel itemName;
  public UILabel itemDesc;
  public GameObject detailContent;
  public UILabel require;
  public UILabel normalTip;
  public UILabel useMethod;
  public System.Action<int> OnItemUseHandler;
  public System.Action<ItemGroupRenderer> OnOpenHandler;
  public System.Action OnRepositionComplete;
  private List<ConsumableItemData> _itemDataList;
  private int _selectedItemId;
  private int itemId;

  public bool Contains(int item_id)
  {
    return this.dict.ContainsKey(item_id);
  }

  public void SetData(List<ConsumableItemData> items)
  {
    int num = 0;
    this.dict.Clear();
    this.indexs.Clear();
    for (int index = 0; index < items.Count; ++index)
    {
      ItemRenderer itemRenderer = this.itemRenderers[index];
      ++num;
      NGUITools.SetActive(itemRenderer.gameObject, true);
      itemRenderer.StopAutoFillName();
      itemRenderer.FeedData((IComponentData) this.GetIconData(items[index].internalId));
      itemRenderer.OnSelectedItemDelegate = new System.Action<int>(this.OnItemClickHandler);
      this.dict.Add(itemRenderer.ItemID, itemRenderer);
      this.indexs.Add(itemRenderer.ItemID);
    }
    for (int index = num; index < this.itemRenderers.Length; ++index)
      NGUITools.SetActive(this.itemRenderers[index].gameObject, false);
    this._itemDataList = items;
  }

  public void Clear()
  {
    this.OnOpenHandler = (System.Action<ItemGroupRenderer>) null;
    this.OnRepositionComplete = (System.Action) null;
    this.Close();
    this.dict.Clear();
    this.indexs.Clear();
    this.RemoveEventHandler();
  }

  public void Dispose()
  {
    this.Clear();
  }

  private void OnItemClickHandler(int item_id)
  {
    Dictionary<int, ItemRenderer>.KeyCollection.Enumerator enumerator = this.dict.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != item_id)
        this.dict[enumerator.Current].Selected = false;
    }
    this.Open(item_id);
  }

  public int SelectedItemId
  {
    get
    {
      return this._selectedItemId;
    }
  }

  public void OnUseItem()
  {
    if (this._selectedItemId <= 0)
      return;
    if (this.OnItemUseHandler != null)
      this.OnItemUseHandler(this._selectedItemId);
    else
      ItemBaseUse.Create(this._selectedItemId, false).UseItem((System.Action<bool, object>) null);
  }

  public void OnComposeItem()
  {
    if (this._selectedItemId <= 0)
      return;
    long internalId = ConfigManager.inst.DB_ItemCompose.GetItemComposeInfo(ConfigManager.inst.DB_Items.GetItem(this._selectedItemId).Param1).internalId;
    UIManager.inst.OpenPopup("ItemComposePopup", (Popup.PopupParameter) new ItemComposePopup.Parameter()
    {
      itemcomposeid = internalId
    });
  }

  private void OnEnable()
  {
    if (!GameEngine.IsAvailable)
      return;
    this.RemoveEventHandler();
    this.AddEventHandler();
  }

  private void Update()
  {
    if (this.dict.Count <= 0 || this.refreshList.Count <= 0)
      return;
    for (int index = 0; index < this.refreshList.Count; ++index)
    {
      int refresh = this.refreshList[index];
      if (this.dict.ContainsKey(refresh))
        this.dict[refresh].FeedData((IComponentData) this.GetIconData(refresh));
    }
    this.refreshList.Clear();
  }

  private void OnDisable()
  {
  }

  private void Open(int item_id)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(item_id);
    this.itemId = item_id;
    Utils.SetItemName(this.itemName, itemStaticInfo.internalId);
    this.itemDesc.text = itemStaticInfo.LocDescription;
    NGUITools.SetActive(this.useBt.gameObject, ItemBag.CanUseInItemBag(item_id));
    this.useBt.isEnabled = ItemBag.GetItemLeftCdTime(item_id) <= 0;
    if (ItemBag.CanUseInItemBag(item_id))
    {
      bool state = ItemBag.IsDragonItemUse(item_id) && ItemBag.IsDragonKnightItemUse(item_id);
      NGUITools.SetActive(this.useBt.gameObject, state);
      if (state)
        NGUITools.SetActive(this.useBt.gameObject, ItemBag.Instance.CheckCanUse(item_id));
    }
    int index1 = this.indexs.IndexOf(item_id);
    ItemRenderer itemRenderer = this.dict[item_id];
    for (int index2 = 0; index2 < this.bgs.Length; ++index2)
      NGUITools.SetActive(this.bgs[index2].gameObject, false);
    NGUITools.SetActive(this.bgs[index1].gameObject, itemRenderer.Selected);
    if ((UnityEngine.Object) this.normalTip != (UnityEngine.Object) null)
      NGUITools.SetActive(this.normalTip.gameObject, false);
    if ((UnityEngine.Object) this.require != (UnityEngine.Object) null)
      NGUITools.SetActive(this.require.gameObject, false);
    if ((UnityEngine.Object) this.useMethod != (UnityEngine.Object) null)
      NGUITools.SetActive(this.useMethod.gameObject, false);
    if (!ItemBag.Instance.CheckCanUse(item_id))
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      para.Add("num", itemStaticInfo.RequireLev.ToString());
      if ((UnityEngine.Object) this.require != (UnityEngine.Object) null)
      {
        this.require.text = ScriptLocalization.GetWithPara("item_requirement_description", para, true);
        NGUITools.SetActive(this.require.gameObject, true);
      }
    }
    else
    {
      if (!string.IsNullOrEmpty(itemStaticInfo.CDGroup))
      {
        string Term = "item_use_cooldown_num_description";
        Dictionary<string, string> para = new Dictionary<string, string>();
        para.Add("0", ((double) itemStaticInfo.CDTime).ToString() + "H");
        if (itemStaticInfo.RemainTime > 0)
        {
          para.Clear();
          para.Add("1", Utils.FormatTime(itemStaticInfo.RemainTime, true, false, true));
          Term = "item_use_cooldown_num";
        }
        if ((UnityEngine.Object) this.require != (UnityEngine.Object) null)
        {
          NGUITools.SetActive(this.require.gameObject, true);
          this.require.text = ScriptLocalization.GetWithPara(Term, para, true);
        }
      }
      else if (!ItemBag.IsDragonItemUse(item_id) || !ItemBag.IsDragonKnightItemUse(item_id))
      {
        if ((UnityEngine.Object) this.require != (UnityEngine.Object) null)
        {
          NGUITools.SetActive(this.require.gameObject, true);
          this.require.text = ScriptLocalization.Get("item_cannot_use_yet_description", true);
        }
      }
      else if (!ItemBag.CanUseInItemBag(item_id) && !string.IsNullOrEmpty(itemStaticInfo.UseMethod) && (UnityEngine.Object) this.useMethod != (UnityEngine.Object) null)
      {
        NGUITools.SetActive(this.useMethod.gameObject, true);
        this.useMethod.text = ScriptLocalization.Get(itemStaticInfo.UseMethod, true);
      }
      if (this._itemDataList != null && index1 < this._itemDataList.Count && this._itemDataList[index1].IsFreeze)
        this.itemDesc.text = itemStaticInfo.LocDescription + "\n[FF0000]" + this._itemDataList[index1].FreezeHint + "[-]";
    }
    this._selectedItemId = !itemRenderer.Selected ? -1 : item_id;
    NGUITools.SetActive(this.detailContent, itemRenderer.Selected);
    bool canBuild = itemStaticInfo.CanBuild;
    if ((UnityEngine.Object) this.composeBt != (UnityEngine.Object) null)
      NGUITools.SetActive(this.composeBt.gameObject, canBuild);
    if (this.OnOpenHandler != null)
      this.OnOpenHandler(this);
    NGUITools.SetDirty((UnityEngine.Object) this.gameObject);
  }

  public void UpdateItemCd(int delta)
  {
    foreach (ItemRenderer itemRenderer in this.itemRenderers)
    {
      if (itemRenderer.gameObject.activeInHierarchy)
      {
        int num = itemRenderer.UpdateLeftCdTime();
        if (this._selectedItemId == itemRenderer.ItemID)
          this.useBt.isEnabled = num <= 0;
      }
    }
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.itemId);
    if (itemStaticInfo == null || string.IsNullOrEmpty(itemStaticInfo.CDGroup))
      return;
    string Term = "item_use_cooldown_num_description";
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("0", ((double) itemStaticInfo.CDTime).ToString() + "H");
    if (itemStaticInfo.RemainTime > 0)
    {
      para.Clear();
      para.Add("1", Utils.FormatTime(itemStaticInfo.RemainTime, true, false, true));
      Term = "item_use_cooldown_num";
    }
    if (!((UnityEngine.Object) this.require != (UnityEngine.Object) null))
      return;
    NGUITools.SetActive(this.require.gameObject, true);
    this.require.text = ScriptLocalization.GetWithPara(Term, para, true);
  }

  public void Close()
  {
    NGUITools.SetActive(this.detailContent, false);
    Dictionary<int, ItemRenderer>.ValueCollection.Enumerator enumerator = this.dict.Values.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Selected = false;
    this._selectedItemId = -1;
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Item.onDataUpdated += new System.Action<int>(this.OnItemDataChange);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Item.onDataUpdated -= new System.Action<int>(this.OnItemDataChange);
  }

  private void OnItemDataChange(int item_id)
  {
    if (!this.dict.ContainsKey(item_id))
      return;
    this.refreshList.Add(item_id);
  }

  private IconData GetIconData(int item_id)
  {
    int itemCount = ItemBag.Instance.GetItemCount(item_id);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(item_id);
    IconData iconData = new IconData();
    iconData.image = itemStaticInfo.ImagePath;
    iconData.Data = (object) item_id;
    iconData.contents = new string[2];
    iconData.contents[0] = itemCount.ToString();
    iconData.contents[1] = ScriptLocalization.GetWithPara("item_requirement_name", new Dictionary<string, string>()
    {
      {
        "num",
        itemStaticInfo.RequireLev.ToString()
      }
    }, true);
    return iconData;
  }
}
