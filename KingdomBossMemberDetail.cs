﻿// Decompiled with JetBrains decompiler
// Type: KingdomBossMemberDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class KingdomBossMemberDetail
{
  public string k;
  public string x;
  public string y;
  public string uid;
  public string name;
  public string acronym;
  public string portrait;
  public string icon;
  public Hashtable start_troops;
  public Hashtable kill_troops;
  public Hashtable end_troops;
  public Hashtable dragon;

  public string fullname
  {
    get
    {
      if (this.acronym != null)
        return "(" + this.acronym + ")" + this.name;
      return this.name;
    }
  }

  public string site
  {
    get
    {
      return "K: " + this.k + " X: " + this.x + " Y: " + this.y;
    }
  }
}
