﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentScrollInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ConfigEquipmentScrollInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "item_id")]
  public int itemID;
  [Config(Name = "equipment_id")]
  public int equipmenID;
  [Config(Name = "level")]
  public int level;
  [Config(Name = "quality")]
  public int quality;
  [Config(Name = "tendency")]
  public int tendency;
  [Config(Name = "sell_wood")]
  public int sellWood;
  [Config(Name = "forge_duration")]
  public int forgeDuration;
  [Config(CustomParse = true, CustomType = typeof (Materials), Name = "Materials")]
  public Materials Materials;

  public string EquipmentTypeImagePath(BagType type)
  {
    return "Texture/Equipment/icon_equipment_" + (object) ConfigManager.inst.GetEquipmentMain(type).GetData(this.equipmenID).type;
  }

  public string ImagePath(BagType type)
  {
    return ConfigManager.inst.GetEquipmentMain(type).GetData(this.equipmenID).ImagePath;
  }

  public string QualityImagePath
  {
    get
    {
      return "Texture/Equipment/frame_equipment_" + (object) this.quality;
    }
  }
}
