﻿// Decompiled with JetBrains decompiler
// Type: HubPort
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class HubPort
{
  private string _action;

  public HubPort(string action)
  {
    this._action = action;
  }

  private event System.Action<object> messageEvent;

  public void MergeMessage(object resData)
  {
    if (this.messageEvent == null)
      return;
    this.messageEvent(resData);
  }

  public void AddEvent(System.Action<object> func)
  {
    this.messageEvent += func;
  }

  public void RemoveEvent(System.Action<object> func)
  {
    this.messageEvent -= func;
  }

  public void SendRequest(Hashtable postData = null, System.Action<bool, object> callback = null, bool blockScreen = true)
  {
    RequestManager.inst.SendRequest(this.Action, postData, callback, blockScreen);
  }

  public void SendRequest(System.Action<bool, object> callback, params string[] args)
  {
    if (args != null && (args.Length & 1) == 1)
    {
      Debug.LogError((object) "Argument list must be even");
    }
    else
    {
      Hashtable postData = new Hashtable();
      if (args != null)
      {
        int index = 0;
        while (index < args.Length)
        {
          postData.Add((object) args[index], (object) args[index + 1]);
          index += 2;
        }
      }
      this.SendRequest(postData, callback, true);
    }
  }

  public void SendLoader(Hashtable postData = null, System.Action<bool, object> callback = null, bool autoRetry = true, bool useStack = false)
  {
    RequestManager.inst.SendLoader(this.Action, postData, callback, autoRetry, useStack);
  }

  public void Dispose()
  {
    this._action = (string) null;
    this.messageEvent = (System.Action<object>) null;
  }

  public string Action
  {
    get
    {
      return this._action;
    }
  }
}
