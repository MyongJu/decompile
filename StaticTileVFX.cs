﻿// Decompiled with JetBrains decompiler
// Type: StaticTileVFX
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StaticTileVFX : MonoBehaviour, IConstructionController
{
  private ParticleSortingOrderModifier m_Modifier;

  private void Start()
  {
    this.m_Modifier = this.GetComponent<ParticleSortingOrderModifier>();
  }

  public void UpdateUI(TileData tile)
  {
  }

  public void Reset()
  {
  }

  public void SetSortingLayerID(int sortingLayerID)
  {
    if (!((Object) this.m_Modifier != (Object) null))
      return;
    this.m_Modifier.SortingLayerName = SortingLayer.IDToName(sortingLayerID);
  }
}
