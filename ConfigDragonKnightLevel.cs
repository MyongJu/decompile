﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragonKnightLevel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDragonKnightLevel
{
  private List<DragonKnightLevelInfo> dragonKnightLevelInfoList = new List<DragonKnightLevelInfo>();
  private Dictionary<string, DragonKnightLevelInfo> datas;
  private Dictionary<int, DragonKnightLevelInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<DragonKnightLevelInfo, string>(res as Hashtable, "dragonKnightLevel", out this.datas, out this.dicByUniqueId);
    Dictionary<string, DragonKnightLevelInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.dragonKnightLevel.ToString()))
        this.dragonKnightLevelInfoList.Add(enumerator.Current);
    }
  }

  public List<DragonKnightLevelInfo> GetDragonKnightLevelInfoList()
  {
    return this.dragonKnightLevelInfoList;
  }

  public DragonKnightLevelInfo Get(int interalId)
  {
    if (this.dicByUniqueId.ContainsKey(interalId))
      return this.dicByUniqueId[interalId];
    return (DragonKnightLevelInfo) null;
  }

  public DragonKnightLevelInfo Get(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (DragonKnightLevelInfo) null;
  }

  public DragonKnightLevelInfo GetByLevel(int level)
  {
    return this.datas[level.ToString()];
  }

  public int DragonKnightMaxLevel
  {
    get
    {
      return this.dragonKnightLevelInfoList.Count;
    }
  }
}
