﻿// Decompiled with JetBrains decompiler
// Type: ConfigMerlinTrialsMonster
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigMerlinTrialsMonster
{
  private List<MerlinTrialsMonsterInfo> _merlinTrialsMonsterInfoList = new List<MerlinTrialsMonsterInfo>();
  private Dictionary<string, MerlinTrialsMonsterInfo> _datas;
  private Dictionary<int, MerlinTrialsMonsterInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<MerlinTrialsMonsterInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, MerlinTrialsMonsterInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._merlinTrialsMonsterInfoList.Add(enumerator.Current);
    }
  }

  public void Clear()
  {
    if (this._datas != null)
      this._datas.Clear();
    if (this._dicByUniqueId != null)
      this._dicByUniqueId.Clear();
    if (this._merlinTrialsMonsterInfoList == null)
      return;
    this._merlinTrialsMonsterInfoList.Clear();
  }

  public List<MerlinTrialsMonsterInfo> GetInfoList()
  {
    return this._merlinTrialsMonsterInfoList;
  }

  public MerlinTrialsMonsterInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (MerlinTrialsMonsterInfo) null;
  }

  public MerlinTrialsMonsterInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (MerlinTrialsMonsterInfo) null;
  }
}
