﻿// Decompiled with JetBrains decompiler
// Type: MonthCardPurchaseItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MonthCardPurchaseItem : MonoBehaviour
{
  private const string iconpath = "Texture/ItemIcons/item_month_card_";
  public Icon purchaseItem;
  public GameObject bonus;
  private long pid;
  public System.Action onPurchaseSuccessed;

  public void OnPurchaseButtonClicked()
  {
    PaymentManager.Instance.BlockBuyProduct(ConfigManager.inst.DB_IAPMonthCard.GetIAPMonthCardInfo(this.pid).productId, string.Empty, string.Empty, (System.Action<bool>) (ret =>
    {
      if (this.onPurchaseSuccessed == null || !ret)
        return;
      this.onPurchaseSuccessed();
      AccountManager.Instance.ShowBindPop();
    }), (System.Action) null);
  }

  public void Init(long id)
  {
    this.pid = id;
    IAPMonthCardInfo iapMonthCardInfo = ConfigManager.inst.DB_IAPMonthCard.GetIAPMonthCardInfo(this.pid);
    string formattedPrice = PaymentManager.Instance.GetFormattedPrice(iapMonthCardInfo.productId, iapMonthCardInfo.id);
    this.purchaseItem.SetData("Texture/ItemIcons/item_month_card_" + iapMonthCardInfo.days.ToString(), Utils.XLAT(iapMonthCardInfo.name), Utils.XLAT(iapMonthCardInfo.description), iapMonthCardInfo.prosperity.ToString(), formattedPrice);
    if (PlayerData.inst.MonthCardValue == PlayerData.MonthCardType.NORMAL)
      this.bonus.SetActive(true);
    else
      this.bonus.SetActive(false);
  }
}
