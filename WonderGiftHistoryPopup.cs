﻿// Decompiled with JetBrains decompiler
// Type: WonderGiftHistoryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class WonderGiftHistoryPopup : Popup
{
  protected List<WonderGiftHistoryItem> m_allHistoryItem = new List<WonderGiftHistoryItem>();
  [SerializeField]
  protected WonderGiftHistoryItem m_template;
  [SerializeField]
  protected UITable m_container;
  [SerializeField]
  protected UIScrollView m_scrollView;
  [SerializeField]
  protected GameObject m_rootNoLogTip;
  protected Hashtable m_historyData;
  protected bool m_layoutDirty;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_template.gameObject.SetActive(false);
    RequestManager.inst.SendRequest("wonder:loadKingGiftLog", (Hashtable) null, new System.Action<bool, object>(this.onRequestCallBack), true);
  }

  protected void onRequestCallBack(bool result, object data)
  {
    if (!result)
      return;
    this.m_historyData = data as Hashtable;
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  protected void UpdateUI()
  {
    this.UpdateHistoryList();
  }

  protected int HistoryDataSorFunc(WonderGiftHistoryPopup.HistoryData a, WonderGiftHistoryPopup.HistoryData b)
  {
    return b.sendTime.CompareTo(a.sendTime);
  }

  protected void UpdateHistoryList()
  {
    this.DestroyAllHistoryItem();
    if (this.m_historyData == null)
      return;
    List<WonderGiftHistoryPopup.HistoryData> historyDataList = new List<WonderGiftHistoryPopup.HistoryData>();
    foreach (DictionaryEntry dictionaryEntry in this.m_historyData)
    {
      WonderGiftHistoryPopup.HistoryData historyData = new WonderGiftHistoryPopup.HistoryData();
      long.TryParse(dictionaryEntry.Key.ToString(), out historyData.sendTime);
      Hashtable hashtable = dictionaryEntry.Value as Hashtable;
      historyData.senderName = hashtable[(object) "sender_name"].ToString();
      historyData.receiverName = hashtable[(object) "receiver_name"].ToString();
      int result = 0;
      int.TryParse(hashtable[(object) "package_id"].ToString(), out result);
      string empty = string.Empty;
      WonderPackageInfo wonderPackageInfo = ConfigManager.inst.DB_WonderPackage.Get(result);
      if (wonderPackageInfo != null)
        historyData.packageName = ScriptLocalization.Get(wonderPackageInfo.name, true);
      historyDataList.Add(historyData);
    }
    historyDataList.Sort(new Comparison<WonderGiftHistoryPopup.HistoryData>(this.HistoryDataSorFunc));
    bool flag = false;
    using (List<WonderGiftHistoryPopup.HistoryData>.Enumerator enumerator = historyDataList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        WonderGiftHistoryPopup.HistoryData current = enumerator.Current;
        this.AddHistoryItem().setData(Utils.FormatTimeForMail(current.sendTime), ScriptLocalization.GetWithPara("throne_royal_pakage_announcement", new Dictionary<string, string>()
        {
          {
            "0",
            current.senderName
          },
          {
            "1",
            current.receiverName
          },
          {
            "2",
            current.packageName
          }
        }, true));
        flag = true;
      }
    }
    this.m_rootNoLogTip.SetActive(!flag);
    this.m_layoutDirty = true;
  }

  protected void LateUpdate()
  {
    if (!this.m_layoutDirty)
      return;
    this.m_layoutDirty = false;
    this.m_container.repositionNow = true;
    this.m_container.Reposition();
    this.m_scrollView.movement = UIScrollView.Movement.Unrestricted;
    this.m_scrollView.ResetPosition();
    this.m_scrollView.currentMomentum = Vector3.zero;
    this.m_scrollView.movement = UIScrollView.Movement.Vertical;
  }

  protected void DestroyAllHistoryItem()
  {
    using (List<WonderGiftHistoryItem>.Enumerator enumerator = this.m_allHistoryItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        WonderGiftHistoryItem current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_allHistoryItem.Clear();
  }

  protected WonderGiftHistoryItem AddHistoryItem()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_template.gameObject);
    gameObject.transform.SetParent(this.m_container.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    WonderGiftHistoryItem component = gameObject.GetComponent<WonderGiftHistoryItem>();
    this.m_allHistoryItem.Add(component);
    return component;
  }

  public void OnButtonCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  protected class HistoryData
  {
    public long sendTime;
    public string senderName;
    public string receiverName;
    public string packageName;
  }
}
