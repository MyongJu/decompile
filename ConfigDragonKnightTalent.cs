﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragonKnightTalent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigDragonKnightTalent
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<int, List<DragonKnightTalentInfo>> dicByTalentTreeId = new Dictionary<int, List<DragonKnightTalentInfo>>();
  private Dictionary<string, DragonKnightTalentInfo> datas;
  private Dictionary<int, DragonKnightTalentInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<DragonKnightTalentInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    if (this.dicByUniqueId == null)
      return;
    Dictionary<int, DragonKnightTalentInfo>.ValueCollection.Enumerator enumerator1 = this.dicByUniqueId.Values.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      if (!this.dicByTalentTreeId.ContainsKey(enumerator1.Current.talentTreeInternalId))
        this.dicByTalentTreeId.Add(enumerator1.Current.talentTreeInternalId, new List<DragonKnightTalentInfo>());
      this.dicByTalentTreeId[enumerator1.Current.talentTreeInternalId].Add(enumerator1.Current);
    }
    Dictionary<int, List<DragonKnightTalentInfo>>.ValueCollection.Enumerator enumerator2 = this.dicByTalentTreeId.Values.GetEnumerator();
    while (enumerator2.MoveNext())
      enumerator2.Current.Sort((Comparison<DragonKnightTalentInfo>) ((x, y) => y.level.CompareTo(x.level)));
    Dictionary<int, List<DragonKnightTalentInfo>>.ValueCollection.Enumerator enumerator3 = this.dicByTalentTreeId.Values.GetEnumerator();
    while (enumerator3.MoveNext())
    {
      for (int index = 0; index < enumerator3.Current.Count; ++index)
        enumerator3.Current[index].maxLevel = enumerator3.Current.Count;
    }
  }

  public DragonKnightTalentInfo GetMaxLevelTalentByTalentTreeId(int talentTreeId)
  {
    if (this.dicByTalentTreeId.ContainsKey(talentTreeId))
    {
      List<DragonKnightTalentInfo> knightTalentInfoList = this.dicByTalentTreeId[talentTreeId];
      if (knightTalentInfoList.Count > 0)
        return knightTalentInfoList[0];
    }
    return (DragonKnightTalentInfo) null;
  }

  public DragonKnightTalentInfo GetCurrentLevelTalentByTalentTreeId(int talentTreeId)
  {
    if (this.dicByTalentTreeId.ContainsKey(talentTreeId))
    {
      List<DragonKnightTalentInfo> knightTalentInfoList = this.dicByTalentTreeId[talentTreeId];
      for (int index = 0; index < knightTalentInfoList.Count; ++index)
      {
        if (DBManager.inst.DB_DragonKnightTalent.IsTalentOwned(knightTalentInfoList[index].internalId))
          return knightTalentInfoList[index];
      }
    }
    return (DragonKnightTalentInfo) null;
  }

  public Requirements GetRequirementsByTalentTreeId(int talentTreeId)
  {
    if (!this.dicByTalentTreeId.ContainsKey(talentTreeId))
      return (Requirements) null;
    List<DragonKnightTalentInfo> knightTalentInfoList = this.dicByTalentTreeId[talentTreeId];
    return knightTalentInfoList[knightTalentInfoList.Count - 1].require;
  }

  public string GetBenefitByInternalId(int internalid)
  {
    DragonKnightTalentInfo knightTalentInfo = this.GetDragonKnightTalentInfo(internalid);
    if (knightTalentInfo == null)
      return "0";
    return this.GetBenefitByStringId(knightTalentInfo.ID);
  }

  public string GetBenefitByStringId(string id)
  {
    string str = (string) null;
    DragonKnightTalentInfo knightTalentInfo1 = this.GetDragonKnightTalentInfo(id);
    if (knightTalentInfo1 == null || knightTalentInfo1.benefit.GetBenefitsPairs().Count < 1)
      return "0";
    string id1 = ConfigManager.inst.DB_DragonKnightTalentTree.GetTalentTreeInfo(knightTalentInfo1.talentTreeInternalId).ID;
    switch (knightTalentInfo1.benefit.GetBenefitsPairs()[0].type)
    {
      case PropertyDefinition.FormatType.Integer:
        int num1 = 0;
        for (int index = 1; index <= knightTalentInfo1.level; ++index)
        {
          DragonKnightTalentInfo knightTalentInfo2 = this.GetDragonKnightTalentInfo(id1 + "_" + (object) index);
          num1 += (int) knightTalentInfo2.benefit.GetBenefitsPairs()[0].value;
        }
        str = num1.ToString();
        break;
      case PropertyDefinition.FormatType.Percent:
        float num2 = 0.0f;
        for (int index = 1; index <= knightTalentInfo1.level; ++index)
        {
          DragonKnightTalentInfo knightTalentInfo2 = this.GetDragonKnightTalentInfo(id1 + "_" + (object) index);
          num2 += knightTalentInfo2.benefit.GetBenefitsPairs()[0].value;
        }
        str = ((float) (int) ((double) num2 * 1000.0 + 0.5) / 1000f).ToString();
        break;
    }
    return str;
  }

  public PropertyDefinition.FormatType GetBenefitTypeByStringId(string id)
  {
    DragonKnightTalentInfo knightTalentInfo = this.GetDragonKnightTalentInfo(id);
    if (knightTalentInfo == null || knightTalentInfo.benefit.GetBenefitsPairs().Count < 1)
      return PropertyDefinition.FormatType.Invalid;
    return knightTalentInfo.benefit.GetBenefitsPairs()[0].type;
  }

  public string GetIncreaseBenefitByInternalId(int internalid)
  {
    return this.GetIncreaseBenefitByStringId(this.GetDragonKnightTalentInfo(internalid).ID);
  }

  public string GetIncreaseBenefitByStringId(string id)
  {
    string str = (string) null;
    DragonKnightTalentInfo knightTalentInfo = this.GetDragonKnightTalentInfo(id);
    if (knightTalentInfo == null || knightTalentInfo.benefit.GetBenefitsPairs().Count < 1)
      return "0";
    Benefits.BenefitValuePair benefitsPair = knightTalentInfo.benefit.GetBenefitsPairs()[0];
    switch (benefitsPair.type)
    {
      case PropertyDefinition.FormatType.Integer:
        str = ((int) benefitsPair.value).ToString();
        break;
      case PropertyDefinition.FormatType.Percent:
        str = benefitsPair.value.ToString();
        break;
    }
    return str;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, DragonKnightTalentInfo>) null;
    }
    if (this.dicByUniqueId != null)
    {
      this.dicByUniqueId.Clear();
      this.dicByUniqueId = (Dictionary<int, DragonKnightTalentInfo>) null;
    }
    if (this.dicByTalentTreeId == null)
      return;
    this.dicByTalentTreeId.Clear();
    this.dicByTalentTreeId = (Dictionary<int, List<DragonKnightTalentInfo>>) null;
  }

  public DragonKnightTalentInfo GetDragonKnightTalentInfo(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (DragonKnightTalentInfo) null;
  }

  public DragonKnightTalentInfo GetDragonKnightTalentInfo(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (DragonKnightTalentInfo) null;
  }

  public bool IsTalentTreeRequirementsComplete(int talentTreeId)
  {
    Requirements requirementsByTalentTreeId = this.GetRequirementsByTalentTreeId(talentTreeId);
    if (requirementsByTalentTreeId == null || requirementsByTalentTreeId.requires == null || requirementsByTalentTreeId.requires.Count == 0)
      return true;
    Dictionary<string, int>.KeyCollection.Enumerator enumerator = requirementsByTalentTreeId.requires.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (DBManager.inst.DB_DragonKnightTalent.IsTalentOwned(int.Parse(enumerator.Current)))
        return true;
    }
    return false;
  }

  public List<DragonKnightTalentInfo> GetDragonKnightActiveSkillsByType(int type)
  {
    List<DragonKnightTalentInfo> knightTalentInfoList = new List<DragonKnightTalentInfo>();
    using (Dictionary<int, DragonKnightTalentInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, DragonKnightTalentInfo> current = enumerator.Current;
        DragonKnightTalentData knightTalentData = DBManager.inst.DB_DragonKnightTalent.Get(current.Value.internalId);
        if (current.Value.type == type && knightTalentData != null)
          knightTalentInfoList.Add(current.Value);
      }
    }
    return knightTalentInfoList;
  }

  public int GetDragonKnightUnlockedSkillSlotsCount()
  {
    return ConfigManager.inst.DB_BenefitCalc.GetFinalData(1, "calc_dragon_knight_skill_equip");
  }
}
