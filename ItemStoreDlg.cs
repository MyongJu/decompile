﻿// Decompiled with JetBrains decompiler
// Type: ItemStoreDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ItemStoreDlg : MonoBehaviour
{
  private string[] SHOPITEM_CATEGORIES = new string[6]
  {
    "all",
    "general",
    "resources",
    "speed up",
    "combat",
    "chest"
  };
  private Stack<GameObject> pools = new Stack<GameObject>();
  private List<ItemGroupRenderer> list = new List<ItemGroupRenderer>();
  public UIButtonBar buttonBar;
  private bool init;
  public UIScrollView scrollView;
  public UITable table;
  public ItemGroupRenderer groupPrefab;
  private int completeCount;
  public GameObject dragGO;
  private bool flag;
  public UILabel empty;
  private bool needRefresh;
  private float moveDistance;

  private int SelectedIndex
  {
    get
    {
      return this.buttonBar.SelectedIndex;
    }
    set
    {
      this.buttonBar.SelectedIndex = value;
    }
  }

  private void Start()
  {
    this.Init();
  }

  private void Init()
  {
    if (this.init)
      return;
    this.init = true;
    this.buttonBar.OnSelectedHandler = new System.Action<int>(this.OnSelectedHandler);
    this.AddEventHandler();
  }

  private void OnEnable()
  {
    if (!this.needRefresh)
      return;
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnItemDataChangeHandler);
    DBManager.inst.DB_Item.onDataCreated += new System.Action<int>(this.OnItemDataChangeHandler);
    this.scrollView.onDragStarted += new UIScrollView.OnDragNotification(this.OnDragFinish);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.UpdateItemCd);
  }

  private void OnDragFinish()
  {
    this.moveDistance = 0.0f;
  }

  private void OnItemDataChangeHandler(int item_id)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(item_id);
    int num = -1;
    for (int index = 0; index < this.SHOPITEM_CATEGORIES.Length; ++index)
    {
      if (this.SHOPITEM_CATEGORIES[index] == itemStaticInfo.Category)
      {
        num = index;
        break;
      }
    }
    if (num <= -1 && this.buttonBar.SelectedIndex != 0)
      return;
    if (UIManager.inst.IsTheCurrentDialog("ItemStore/ItemStoreDlg") && this.gameObject.activeSelf)
      this.UpdateUI();
    else
      this.needRefresh = true;
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnItemDataChangeHandler);
    DBManager.inst.DB_Item.onDataCreated -= new System.Action<int>(this.OnItemDataChangeHandler);
    this.scrollView.onDragStarted -= new UIScrollView.OnDragNotification(this.OnDragFinish);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.UpdateItemCd);
  }

  protected void UpdateItemCd(int delta)
  {
    using (List<ItemGroupRenderer>.Enumerator enumerator = this.list.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ItemGroupRenderer current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
          current.UpdateItemCd(delta);
      }
    }
  }

  public void Dispose()
  {
    this.RemoveEventHandler();
    for (int index = 0; index < this.list.Count; ++index)
      this.list[index].Dispose();
    this.pools.Clear();
    this.list.Clear();
  }

  public void Refresh()
  {
    this.Init();
    if (this.SelectedIndex != -1)
      return;
    this.SelectedIndex = 0;
  }

  private void OnSelectedHandler(int index)
  {
    this.flag = false;
    this.UpdateUI();
  }

  public void Clear()
  {
    for (int index = 0; index < this.list.Count; ++index)
    {
      this.list[index].Clear();
      this.list[index].gameObject.SetActive(false);
      this.list[index].transform.parent = this.scrollView.transform;
      this.pools.Push(this.list[index].gameObject);
    }
    this.list.Clear();
  }

  private void UpdateUI()
  {
    this.Init();
    this.moveDistance = 0.0f;
    this.Clear();
    List<ConsumableItemData> consumableItemDataList = new List<ConsumableItemData>();
    string str = this.SHOPITEM_CATEGORIES[this.SelectedIndex];
    Dictionary<long, ConsumableItemData>.ValueCollection.Enumerator enumerator = DBManager.inst.DB_Item.Datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(enumerator.Current.internalId);
      if (itemStaticInfo != null && (itemStaticInfo.Category == str || this.SelectedIndex == 0) && itemStaticInfo.InStore)
      {
        ConsumableItemData consumableItemData = new ConsumableItemData(enumerator.Current.internalId);
        consumableItemData.quantity -= consumableItemData.FreezeQuantity;
        consumableItemDataList.Add(consumableItemData);
      }
    }
    consumableItemDataList.Sort(new Comparison<ConsumableItemData>(this.CompareShopItem));
    int num = 4;
    List<ConsumableItemData> datas = new List<ConsumableItemData>();
    this.completeCount = 0;
    NGUITools.SetActive(this.empty.gameObject, consumableItemDataList.Count == 0);
    for (int index = 0; index < consumableItemDataList.Count; ++index)
    {
      if (datas.Count < num)
      {
        datas.Add(consumableItemDataList[index]);
      }
      else
      {
        this.CreateItemGroup(datas);
        datas = new List<ConsumableItemData>();
        datas.Add(consumableItemDataList[index]);
      }
    }
    if (datas.Count != 0)
      this.CreateItemGroup(datas);
    this.table.Reposition();
    Utils.ExecuteInSecs(1f / 1000f, (System.Action) (() =>
    {
      try
      {
        if (!this.flag)
        {
          this.flag = true;
          this.scrollView.ResetPosition();
        }
        else
        {
          Bounds bounds = this.scrollView.bounds;
          this.scrollView.MoveRelative(this.scrollView.panel.CalculateConstrainOffset((Vector2) bounds.min, (Vector2) bounds.max));
        }
      }
      catch
      {
      }
    }));
    this.needRefresh = false;
  }

  private void CreateItemGroup(List<ConsumableItemData> datas)
  {
    GameObject go = (GameObject) null;
    if (this.pools.Count > 0)
    {
      go = this.pools.Pop();
      go.transform.parent = this.table.transform;
    }
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      go = NGUITools.AddChild(this.table.gameObject, this.groupPrefab.gameObject);
    ItemGroupRenderer component = go.GetComponent<ItemGroupRenderer>();
    component.OnOpenHandler = new System.Action<ItemGroupRenderer>(this.OnOpenHandler);
    this.list.Add(component);
    component.SetData(datas);
    NGUITools.SetActive(go, true);
  }

  private void OnOpenHandler(ItemGroupRenderer item)
  {
    for (int index = 0; index < this.list.Count; ++index)
    {
      if ((UnityEngine.Object) this.list[index] != (UnityEngine.Object) item)
        this.list[index].Close();
    }
    this.table.repositionNow = true;
    this.table.Reposition();
    if (item.SelectedItemId > 0)
    {
      Vector4 finalClipRegion = this.scrollView.panel.finalClipRegion;
      Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.dragGO.transform, item.transform);
      int num1 = Mathf.RoundToInt(finalClipRegion.w);
      if ((num1 & 1) != 0)
        --num1;
      float num2 = Mathf.Round((float) num1 * 0.5f);
      if (this.scrollView.panel.clipping == UIDrawCall.Clipping.SoftClip)
        num2 -= this.scrollView.panel.clipSoftness.y;
      float num3 = num2 * 2f;
      float num4 = relativeWidgetBounds.max.y - 537f;
      if ((double) num4 < -(double) num2 * 0.5)
      {
        float num5 = (float) (-(double) num2 * 0.5) - num4;
        this.scrollView.MoveRelative(new Vector3()
        {
          y = num5 - this.moveDistance
        });
        this.moveDistance = num5 - this.moveDistance;
      }
    }
    if (item.SelectedItemId >= 0)
      return;
    this.scrollView.MoveRelative(new Vector3()
    {
      y = -this.moveDistance
    });
    this.moveDistance = 0.0f;
  }

  private void OnReposistion()
  {
    this.table.onReposition -= new UITable.OnReposition(this.OnReposistion);
    this.scrollView.ResetPosition();
    this.scrollView.RestrictWithinBounds(true);
  }

  private int CompareShopItem(ConsumableItemData a, ConsumableItemData b)
  {
    return ConfigManager.inst.DB_Items.GetItem(a.internalId).Priority.CompareTo(ConfigManager.inst.DB_Items.GetItem(b.internalId).Priority);
  }
}
