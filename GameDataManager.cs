﻿// Decompiled with JetBrains decompiler
// Type: GameDataManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class GameDataManager : MonoBehaviour
{
  public static string VERSION = "1.0.67";
  public string mLocale = "EN";
  public bool bLoadFonts = true;
  public bool bLoadKingdomFonts = true;
  public ArrayList mScriptCommands = new ArrayList();
  public Vector2[] DEBUGWALL = new Vector2[6];
  public const float TILE_WIDTH = 36f;
  public const float TILE_HEIGHT = 26.64f;
  public const float TILE_HWIDTH = 18f;
  public const float TILE_HHEIGHT = 13.32f;
  private bool _lockScrolling;
  private bool _lockBuilding;
  public bool mButtonDown;
  public int mBuildingSerialOffset;
  private Hashtable _xlatTable;
  public float XX;
  public float YY;
  public float WW;
  public float HH;
  public int currentDebugIndex;
  public IEnumerator CRB;
  public bool mLock2;
  public bool mTutorialLock;
  private bool bStarted;
  private static GameDataManager _singleton;

  public static GameDataManager inst
  {
    get
    {
      if ((UnityEngine.Object) GameDataManager._singleton == (UnityEngine.Object) null)
      {
        GameObject gameObject = new GameObject();
        GameDataManager._singleton = gameObject.AddComponent<GameDataManager>();
        gameObject.name = nameof (GameDataManager);
        UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
      }
      return GameDataManager._singleton;
    }
  }

  private void Start()
  {
    if (this.bStarted)
      return;
    this.bStarted = true;
  }

  public void Activate()
  {
    this.Start();
  }

  public void AddCommand(string _key, object _data)
  {
    this.mScriptCommands.Add((object) new CmdAction(_key, _data));
  }

  public bool IsScrollLocked()
  {
    return this._lockScrolling;
  }

  public void LockScrolling()
  {
    this._lockScrolling = true;
  }

  public void UnlockScrolling()
  {
    this._lockScrolling = false;
  }

  public bool IsBuildingLocked()
  {
    return this._lockBuilding;
  }

  public void LockBuilding()
  {
    this._lockBuilding = true;
  }

  public void UnlockBuilding()
  {
    this._lockBuilding = false;
  }

  public string XLAT(string code)
  {
    if (code.Length == 0)
      return string.Empty;
    if (this._xlatTable == null)
      this._xlatTable = Utils.Json2Object((AssetManager.Instance.HandyLoad("Translations", (System.Type) null) as TextAsset).text, true) as Hashtable;
    if (!this._xlatTable.ContainsKey((object) code))
      return code;
    Hashtable hashtable = this._xlatTable[(object) code] as Hashtable;
    if (hashtable.ContainsKey((object) this.mLocale))
      return hashtable[(object) this.mLocale].ToString();
    return "!!" + code;
  }

  public static void LogEvent(string eventName)
  {
  }
}
