﻿// Decompiled with JetBrains decompiler
// Type: TroopLeader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TroopLeader
{
  public string k;
  public string x;
  public string y;
  public string uid;
  public string alliance_id;
  public string name;
  public string acronym;
  public string portrait;
  public string icon;
  public int lord_title;
  public Hashtable dragon;
  public List<Hashtable> legends;
  public Dictionary<string, string> rewards;
  public Dictionary<string, string> boosts_detail;
  public int kingdom_coin;
  public int world_id;
  public int dragon_skill_aoe_killed;

  public string fullname
  {
    get
    {
      if (this.acronym != null)
        return "(" + this.acronym + ")" + this.name;
      return this.name;
    }
  }

  public string site
  {
    get
    {
      return "K: " + this.k + " X: " + this.x + " Y: " + this.y;
    }
  }

  public void SetLegend(List<Hashtable> _legends)
  {
    if (_legends == null)
      return;
    if (this.legends != null)
      this.legends.Clear();
    else
      this.legends = new List<Hashtable>();
    using (List<Hashtable>.Enumerator enumerator = _legends.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Hashtable current = enumerator.Current;
        if (current[(object) "uid"].ToString() == this.uid.ToString())
          this.legends.Add(current);
      }
    }
  }

  public void SummarizeBoostDetail(ref List<BoostInfo.Data> boostdetail)
  {
    if (this.boosts_detail == null)
      return;
    boostdetail.Clear();
    using (Dictionary<string, string>.KeyCollection.Enumerator enumerator = this.boosts_detail.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        BoostInfo.Data data = new BoostInfo.Data();
        string longId = DBManager.inst.DB_Local_Benefit.GetLongID(current);
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[longId];
        if (dbProperty != null)
        {
          data.name = dbProperty.Name;
          data.enhance = float.Parse(this.boosts_detail[current]);
          data.pri = dbProperty.Priority;
          data.ispercent = dbProperty.Format == PropertyDefinition.FormatType.Percent;
          boostdetail.Add(data);
        }
        else
          Debug.LogWarning((object) ("wrong boost~~~ " + longId));
      }
    }
    boostdetail.Sort((Comparison<BoostInfo.Data>) ((b1, b2) => b1.pri - b2.pri));
  }
}
