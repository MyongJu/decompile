﻿// Decompiled with JetBrains decompiler
// Type: AllianceTechSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using UI;
using UnityEngine;

public class AllianceTechSlot : MonoBehaviour
{
  public System.Action<int> onSlotClick;
  public System.Action<int> onMarkClick;
  public System.Action<int> onResearchClick;
  public System.Action<int> onSpeedUpClick;
  private int _researchId;
  [SerializeField]
  private AllianceTechSlot.Panel panel;

  public void Setup(int researchId)
  {
    this._researchId = researchId;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.techIcon, ConfigManager.inst.DB_AllianceTech.GetItem(this._researchId).ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.RefreshPanel();
  }

  private void RefreshPanel()
  {
    if (PlayerData.inst.allianceData == null)
    {
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
    {
      long jobId = 0;
      int curValue = 0;
      AllianceTechData allianceTechData = DBManager.inst.DB_AllianceTech.Get(this._researchId);
      if (allianceTechData != null)
      {
        curValue = allianceTechData.donation;
        jobId = allianceTechData.jobId;
      }
      AllianceTechInfo allianceTechInfo1 = ConfigManager.inst.DB_AllianceTech.GetItem(this._researchId);
      AllianceTechInfo allianceTechInfo2 = ConfigManager.inst.DB_AllianceTech.GetNextAllianceTechInfo(this._researchId);
      this.panel.techName.text = allianceTechInfo1.LocName;
      this.panel.techLevel.text = allianceTechInfo1.Level.ToString();
      this.panel.techCurEffect.text = Utils.GetBenefitsDesc(allianceTechInfo1.Benefits);
      if (allianceTechInfo2 == null)
      {
        this.panel.techProgress.gameObject.SetActive(false);
        this.panel.techMaxLevelText.gameObject.SetActive(true);
      }
      else
      {
        this.panel.techProgress.gameObject.SetActive(true);
        this.panel.techMaxLevelText.gameObject.SetActive(false);
        this.panel.techProgress.maxStep = allianceTechInfo1.maxExpIndex;
        this.panel.techProgress.curProgress = ConfigManager.inst.DB_AllianceTech.GetProgressInTotal(this._researchId, curValue);
      }
      if (PlayerData.inst.allianceData.techMark == this._researchId)
        this.panel.techMarkSprite.gameObject.SetActive(true);
      else
        this.panel.techMarkSprite.gameObject.SetActive(false);
      if (!PlayerData.inst.isAllianceR4Member)
      {
        this.panel.BT_Mark.gameObject.SetActive(false);
        this.panel.BT_Research.gameObject.SetActive(false);
      }
      else if (curValue < allianceTechInfo1.TotalExp)
      {
        this.panel.BT_Research.gameObject.SetActive(false);
        if (PlayerData.inst.allianceData.techMark != this._researchId && allianceTechInfo2 != null)
          this.panel.BT_Mark.gameObject.SetActive(true);
        else
          this.panel.BT_Mark.gameObject.SetActive(false);
      }
      else
      {
        this.panel.BT_Mark.gameObject.SetActive(false);
        this.panel.techMarkSprite.gameObject.SetActive(false);
        this.panel.BT_Research.gameObject.SetActive(true);
      }
      if (jobId != 0L)
      {
        this.panel.BT_Mark.gameObject.SetActive(false);
        this.panel.BT_Research.gameObject.SetActive(false);
        this.panel.techMarkSprite.gameObject.SetActive(false);
        this.panel.techProgress.gameObject.SetActive(false);
        this.panel.techTimerBar.gameObject.SetActive(true);
        this.panel.BT_SpeedUp.gameObject.SetActive(PlayerData.inst.isAllianceR4Member);
        JobHandle job = JobManager.Instance.GetJob(jobId);
        if (job == null)
          return;
        this.panel.techTimerBarText.text = Utils.FormatTime(job.EndTime() - NetServerTime.inst.ServerTimestamp <= 0 ? 0 : job.EndTime() - NetServerTime.inst.ServerTimestamp, false, false, true);
        this.panel.techTimerBar.value = Mathf.Clamp01((float) (NetServerTime.inst.ServerTimestamp - job.StartTime()) / (float) job.Duration());
      }
      else
      {
        this.panel.techTimerBar.gameObject.SetActive(false);
        this.panel.BT_SpeedUp.gameObject.SetActive(false);
      }
    }
  }

  public void OnSecond(int serverTime)
  {
    this.RefreshPanel();
  }

  public void OnSlotButtonClick()
  {
    AllianceTechData allianceTechData = DBManager.inst.DB_AllianceTech.Get(this._researchId);
    if (allianceTechData != null && (allianceTechData.info.TotalExp <= allianceTechData.donation || allianceTechData.jobId != 0L) || this.onSlotClick == null)
      return;
    this.onSlotClick(this._researchId);
  }

  public void OnMarkButtonClick()
  {
    AllianceTechData allianceTechData = DBManager.inst.DB_AllianceTech.Get(this._researchId);
    if (allianceTechData != null && (allianceTechData.info.TotalExp <= allianceTechData.donation || allianceTechData.jobId != 0L) || this.onMarkClick == null)
      return;
    this.onMarkClick(this._researchId);
  }

  public void OnResearchButtonClick()
  {
    if (DBManager.inst.DB_AllianceTech.researchingId != 0)
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_alliance_tech_already_researching", true), (System.Action) null, 4f, false);
    }
    else
    {
      if (this.onResearchClick == null)
        return;
      this.onResearchClick(this._researchId);
    }
  }

  public void OnSpeedUpButtonClick()
  {
    if (this.onSpeedUpClick == null)
      return;
    this.onSpeedUpClick(this._researchId);
  }

  private void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  private void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  [Serializable]
  protected class Panel
  {
    public UITexture techIcon;
    public UILabel techName;
    public UILabel techLevel;
    public UILabel techCurEffect;
    public UILabel techMaxLevelText;
    public AllianceTechSlotProgressBar techProgress;
    public UISlider techTimerBar;
    public UILabel techTimerBarText;
    public UIButton BT_Slot;
    public UIButton BT_Mark;
    public UISprite techMarkSprite;
    public UIButton BT_Research;
    public UIButton BT_SpeedUp;
  }
}
