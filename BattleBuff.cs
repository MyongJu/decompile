﻿// Decompiled with JetBrains decompiler
// Type: BattleBuff
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class BattleBuff
{
  private Dictionary<string, float> _benefits = new Dictionary<string, float>();
  private bool _isCreate = true;
  private int _duration;
  private int _buffId;
  private float _ctime;
  private int _value;
  private DragonKnightAttibute.Type _type;

  public BattleBuff()
  {
    this._ctime = Time.time;
    this._isCreate = true;
  }

  public int Value
  {
    get
    {
      return this._value;
    }
  }

  public DragonKnightAttibute.Type Type
  {
    get
    {
      return this._type;
    }
    set
    {
      this._type = value;
    }
  }

  public float CTime
  {
    get
    {
      return this._ctime;
    }
  }

  public bool IsFinish()
  {
    return this._duration <= 0;
  }

  public int BuffId
  {
    get
    {
      return this._buffId;
    }
  }

  public bool Equal(BattleBuff other)
  {
    return other._buffId == this._buffId;
  }

  public void Reset(BattleBuff other)
  {
    this._duration = other._duration;
  }

  public void Attenuation()
  {
    --this._duration;
  }

  public Dictionary<string, float> Benefits
  {
    get
    {
      return this._benefits;
    }
    set
    {
      this._benefits = value;
    }
  }

  public float GetBenefit(string key)
  {
    if (this._benefits.ContainsKey(key))
      return this._benefits[key];
    return 0.0f;
  }

  private void AddBenefit(string key, float value)
  {
    if (!this._benefits.ContainsKey(key))
      this._benefits.Add(key, value);
    else
      this._benefits[key] = value;
  }

  public static BattleBuff Create(int buffId, int round)
  {
    List<global::Benefits.BenefitValuePair> benefitsPairs = ConfigManager.inst.DBDragonKnightTalentBuff.GetData(buffId).Benefit.GetBenefitsPairs();
    BattleBuff battleBuff = new BattleBuff();
    battleBuff._buffId = buffId;
    for (int index = 0; index < benefitsPairs.Count; ++index)
    {
      global::Benefits.BenefitValuePair benefitValuePair = benefitsPairs[index];
      if (benefitValuePair != null && benefitValuePair.propertyDefinition != null)
        battleBuff.AddBenefit(benefitValuePair.propertyDefinition.ID, benefitValuePair.value);
    }
    battleBuff._duration = round;
    return battleBuff;
  }
}
