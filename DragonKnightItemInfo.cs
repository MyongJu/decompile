﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightItemInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;

public class DragonKnightItemInfo
{
  private static readonly Dictionary<string, string> Type2Calc = new Dictionary<string, string>()
  {
    {
      "stamina",
      "calc_dragon_knight_addition_stamina_effect"
    },
    {
      "health",
      "calc_dragon_knight_addition_health_effect"
    },
    {
      "mana",
      "calc_dragon_knight_addition_mana_effect"
    }
  };
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "effect")]
  public string effect;
  [Config(Name = "count_base")]
  public int count_base;
  [Config(Name = "effect_base")]
  public int effect_base;
  [Config(Name = "icon")]
  public string icon;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "description")]
  public string description;

  public string ImagePath
  {
    get
    {
      return string.Format("Texture/ItemIcons/{0}", (object) this.icon);
    }
  }

  public string LocName
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }

  public string LocDesc
  {
    get
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      DragonKnightAttibute.Type type = DragonKnightAttibute.Type.StaminaEff;
      string effect = this.effect;
      if (effect != null)
      {
        if (DragonKnightItemInfo.\u003C\u003Ef__switch\u0024map40 == null)
          DragonKnightItemInfo.\u003C\u003Ef__switch\u0024map40 = new Dictionary<string, int>(3)
          {
            {
              "stamina",
              0
            },
            {
              "health",
              1
            },
            {
              "mana",
              2
            }
          };
        int num;
        if (DragonKnightItemInfo.\u003C\u003Ef__switch\u0024map40.TryGetValue(effect, out num))
        {
          switch (num)
          {
            case 0:
              type = DragonKnightAttibute.Type.StaminaEff;
              break;
            case 1:
              type = DragonKnightAttibute.Type.HPEff;
              break;
            case 2:
              type = DragonKnightAttibute.Type.MPEff;
              break;
          }
        }
      }
      int attibute = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L).GetAttibute(type);
      para["1"] = attibute.ToString();
      return ScriptLocalization.GetWithPara(this.description, para, true);
    }
  }
}
