﻿// Decompiled with JetBrains decompiler
// Type: DKArenaScoreRewardItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class DKArenaScoreRewardItem : MonoBehaviour
{
  private List<Icon> totals = new List<Icon>();
  private int _rewardId;
  public Icon icon;
  public UIGrid grid;
  public UIButton claim;
  public GameObject claimState;
  public UILabel stateLabel;
  public UILabel targetScore;
  public UISprite normalBg;
  public UISprite claimBg;
  public System.Action OnRefreshDelegate;

  public void Dispose()
  {
    for (int index = 0; index < this.totals.Count; ++index)
      this.totals[index].Dispose();
    this.totals.Clear();
  }

  private void OnClaimHandler(bool success, object result)
  {
    if (!success)
      return;
    this.stateLabel.text = ScriptLocalization.Get("id_uppercase_collected", true);
    NGUITools.SetActive(this.claimState, success);
    NGUITools.SetActive(this.claim.gameObject, !success);
    NGUITools.SetActive(this.normalBg.gameObject, !success);
    NGUITools.SetActive(this.claimBg.gameObject, success);
    if (this.OnRefreshDelegate != null)
      this.OnRefreshDelegate();
    RewardsCollectionAnimator.Instance.Clear();
    List<Reward.RewardsValuePair> rewards = ConfigManager.inst.DB_DKArenaTitleReward.GetItem(this._rewardId).Rewards.GetRewards();
    for (int index = 0; index < rewards.Count; ++index)
    {
      Reward.RewardsValuePair rewardsValuePair = rewards[index];
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardsValuePair.internalID);
      RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
      {
        icon = itemStaticInfo.ImagePath,
        count = (float) rewardsValuePair.value
      });
    }
    RewardsCollectionAnimator.Instance.CollectItems(false);
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  public void ClaimHandler()
  {
    DKArenaTitleReward arenaTitleReward = ConfigManager.inst.DB_DKArenaTitleReward.GetItem(this._rewardId);
    RequestManager.inst.SendRequest("dragonKnight:receiveTitleRewards", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "step_id", (object) arenaTitleReward.ID), new System.Action<bool, object>(this.OnClaimHandler), true);
  }

  public void SetData(int rewardId, long score)
  {
    this._rewardId = rewardId;
    DKArenaTitleReward arenaTitleReward = ConfigManager.inst.DB_DKArenaTitleReward.GetItem(this._rewardId);
    if (this.totals.Count == 0 && arenaTitleReward.Rewards != null)
    {
      List<Reward.RewardsValuePair> rewards = arenaTitleReward.Rewards.GetRewards();
      for (int index = 0; index < rewards.Count; ++index)
      {
        Reward.RewardsValuePair rewardsValuePair = rewards[index];
        IconData iconData = new IconData(ConfigManager.inst.DB_Items.GetItem(rewardsValuePair.internalID).ImagePath, new string[1]
        {
          rewardsValuePair.value.ToString()
        });
        iconData.Data = (object) rewardsValuePair.internalID;
        Icon icon = this.GetIcon();
        icon.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
        icon.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
        icon.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
        icon.StopAutoFillName();
        icon.FeedData((IComponentData) iconData);
      }
    }
    this.targetScore.text = arenaTitleReward.ScoreMin.ToString();
    this.stateLabel.text = ScriptLocalization.Get("id_uppercase_collected", true);
    NGUITools.SetActive(this.claimState, false);
    NGUITools.SetActive(this.claim.gameObject, false);
    bool state = DBManager.inst.DB_DKArenaDB.GetCurrentData().IsClaim(arenaTitleReward.ID);
    NGUITools.SetActive(this.normalBg.gameObject, !state);
    NGUITools.SetActive(this.claimBg.gameObject, state);
    if (score >= (long) arenaTitleReward.ScoreMin && score <= (long) arenaTitleReward.ScoreMax || score > (long) arenaTitleReward.ScoreMax)
    {
      NGUITools.SetActive(this.claimState, state);
      NGUITools.SetActive(this.claim.gameObject, !state);
    }
    else
    {
      this.stateLabel.text = ScriptLocalization.Get("id_target_not_reached", true);
      NGUITools.SetActive(this.claimState, true);
    }
  }

  private Icon GetIcon()
  {
    GameObject go = NGUITools.AddChild(this.grid.gameObject, this.icon.gameObject);
    Icon component = go.GetComponent<Icon>();
    NGUITools.SetActive(go, true);
    this.totals.Add(component);
    return component;
  }
}
