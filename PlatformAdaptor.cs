﻿// Decompiled with JetBrains decompiler
// Type: PlatformAdaptor
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PlatformAdaptor
{
  private static IPlatformAdaptor _adaptor;

  public static void NotifyApplicationStart()
  {
    PlatformAdaptor._adaptor = PlatformAdaptor.CreateAdaptor(Application.platform);
    if (PlatformAdaptor._adaptor == null)
      return;
    PlatformAdaptor._adaptor.OnApplicationStart();
  }

  public static void NotifyGameStart()
  {
    if (PlatformAdaptor._adaptor == null)
      return;
    PlatformAdaptor._adaptor.OnGameStart();
  }

  public static void NotifyGameRestart()
  {
    if (PlatformAdaptor._adaptor == null)
      return;
    PlatformAdaptor._adaptor.OnGameRestart();
  }

  public static void NotifyApplicationQuit()
  {
    if (PlatformAdaptor._adaptor != null)
      PlatformAdaptor._adaptor.OnApplicationQuit();
    PlatformAdaptor._adaptor = (IPlatformAdaptor) null;
  }

  private static IPlatformAdaptor CreateAdaptor(RuntimePlatform platform)
  {
    IPlatformAdaptor platformAdaptor;
    switch (platform)
    {
      case RuntimePlatform.IPhonePlayer:
        platformAdaptor = (IPlatformAdaptor) new iOSAdaptor();
        break;
      case RuntimePlatform.Android:
        platformAdaptor = (IPlatformAdaptor) new AndroidAdaptor();
        break;
      default:
        platformAdaptor = (IPlatformAdaptor) new DefaultAdaptor();
        break;
    }
    return platformAdaptor;
  }
}
