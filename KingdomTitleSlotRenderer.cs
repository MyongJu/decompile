﻿// Decompiled with JetBrains decompiler
// Type: KingdomTitleSlotRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;
using UnityEngine;

public class KingdomTitleSlotRenderer : MonoBehaviour
{
  public UILabel title;
  public UILabel titleGrantedUser;
  public UILabel titleCoolDownTime;
  public UITexture titleTexture;
  public UITexture titleBkgTexture;
  public UISprite plusSprite;
  private int kingdomId;
  private long nominateUserId;
  private WonderTitleInfo wonderTitleInfo;

  public void SetData(WonderTitleInfo wonderTitleInfo, int kingdomId, long nominateUserId)
  {
    this.kingdomId = kingdomId;
    this.nominateUserId = nominateUserId;
    this.wonderTitleInfo = wonderTitleInfo;
    this.Initialize();
    this.AddEventHandler();
    this.UpdateUI();
  }

  public void ClearData()
  {
    this.RemoveEventHandler();
  }

  public void OnTitlePressed()
  {
    if (this.nominateUserId <= 0L || this.kingdomId != PlayerData.inst.userData.world_id)
      this.ShowTitleDetail();
    else if (!this.wonderTitleInfo.IsNominated && this.wonderTitleInfo.TitleCoolDownLeftTime > 0)
      this.AppointKingdomTitle(this.wonderTitleInfo.id, this.nominateUserId);
    else if (!this.wonderTitleInfo.IsNominated)
      this.ShowTitleAppoint();
    else
      UIManager.inst.ShowConfirmationBox(Utils.XLAT(!this.wonderTitleInfo.IsNominated ? "throne_title_appoint_confirm_title" : "throne_title_cancel_confirm_title"), Utils.XLAT(!this.wonderTitleInfo.IsNominated ? "throne_title_appoint_confirm_description" : "throne_title_cancel_confirm_description"), Utils.XLAT("id_uppercase_confirm"), Utils.XLAT("id_uppercase_cancel"), ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, (System.Action) (() => this.AppointKingdomTitle(this.wonderTitleInfo.id, !this.wonderTitleInfo.IsNominated ? this.nominateUserId : 0L)), (System.Action) null, (System.Action) null);
  }

  private void ShowTitleAppoint()
  {
    UIManager.inst.OpenPopup("Wonder/KingdomTitleInfoPopup", (Popup.PopupParameter) new KingdomTitleInfoPopup.Parameter()
    {
      kingdomId = this.kingdomId,
      wonderTitleInfo = this.wonderTitleInfo,
      nominateUserId = this.nominateUserId,
      nominateButtonPressed = (System.Action) (() => this.AppointKingdomTitle(this.wonderTitleInfo.id, this.nominateUserId))
    });
  }

  private void UpdateUI()
  {
    if (this.wonderTitleInfo == null)
      return;
    this.title.text = this.wonderTitleInfo.Name;
    DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) this.kingdomId);
    if (this.wonderTitleInfo.IsNominated)
    {
      this.titleGrantedUser.text = this.wonderTitleInfo.NominatedUser.UserName;
      string portrait = this.wonderTitleInfo.NominatedUser.Portrait;
      string localPath = "Texture/Hero/Portrait_Icon/player_portrait_icon_" + portrait;
      if ((UnityEngine.Object) this.titleTexture.mainTexture == (UnityEngine.Object) null || (UnityEngine.Object) this.titleTexture.mainTexture != (UnityEngine.Object) null && this.titleTexture.mainTexture.name != "player_portrait_icon_" + portrait && this.titleTexture.mainTexture.name != this.wonderTitleInfo.NominatedUser.UserImage)
      {
        CustomIconLoader.Instance.requestCustomIcon(this.titleTexture, localPath, this.wonderTitleInfo.NominatedUser.UserImage, false);
        LordTitlePayload.Instance.ApplyUserAvator(this.titleTexture, this.wonderTitleInfo.NominatedUser.LordTitleId, 1);
      }
    }
    else
    {
      this.titleGrantedUser.text = Utils.XLAT("throne_title_vacant");
      if ((UnityEngine.Object) this.titleTexture.mainTexture == (UnityEngine.Object) null || (UnityEngine.Object) this.titleTexture.mainTexture != (UnityEngine.Object) null && this.titleTexture.mainTexture.name != this.wonderTitleInfo.icon)
        BuilderFactory.Instance.HandyBuild((UIWidget) this.titleTexture, "Texture/Kingdom/" + this.wonderTitleInfo.icon, (System.Action<bool>) null, true, false, string.Empty);
      LordTitlePayload.Instance.ApplyUserAvator(this.titleTexture, 0, 1);
    }
    if (wonderData != null && wonderData.KING_UID == PlayerData.inst.uid)
      NGUITools.SetActive(this.plusSprite.gameObject, this.nominateUserId > 0L && !this.wonderTitleInfo.IsNominated);
    else
      NGUITools.SetActive(this.plusSprite.gameObject, false);
    if (wonderData != null && wonderData.KING_UID == PlayerData.inst.uid && this.wonderTitleInfo.TitleCoolDownLeftTime >= 0)
    {
      this.titleCoolDownTime.text = Utils.FormatTime(this.wonderTitleInfo.TitleCoolDownLeftTime, true, false, true);
      NGUITools.SetActive(this.titleCoolDownTime.gameObject, true);
    }
    else
      NGUITools.SetActive(this.titleCoolDownTime.gameObject, false);
    if (this.wonderTitleInfo.titleType <= 0)
    {
      if (!((UnityEngine.Object) this.titleBkgTexture.mainTexture == (UnityEngine.Object) null) && (!((UnityEngine.Object) this.titleBkgTexture.mainTexture != (UnityEngine.Object) null) || !(this.titleBkgTexture.mainTexture.name != "kingdom_title_base_official")))
        return;
      BuilderFactory.Instance.HandyBuild((UIWidget) this.titleBkgTexture, "Texture/Kingdom/kingdom_title_base_official", (System.Action<bool>) null, true, false, string.Empty);
    }
    else
    {
      if (!((UnityEngine.Object) this.titleBkgTexture.mainTexture == (UnityEngine.Object) null) && (!((UnityEngine.Object) this.titleBkgTexture.mainTexture != (UnityEngine.Object) null) || !(this.titleBkgTexture.mainTexture.name != "kingdom_title_base_slave")))
        return;
      BuilderFactory.Instance.HandyBuild((UIWidget) this.titleBkgTexture, "Texture/Kingdom/kingdom_title_base_slave", (System.Action<bool>) null, true, false, string.Empty);
    }
  }

  private void Initialize()
  {
    NGUITools.SetActive(this.plusSprite.gameObject, false);
  }

  private void OnSecondEventHandler(int time)
  {
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEventHandler);
    ConfigManager.inst.DB_WonderTitle.onWonderTitleDataUpdated += new System.Action(this.OnWonderTitleDataUpdated);
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEventHandler);
    ConfigManager.inst.DB_WonderTitle.onWonderTitleDataUpdated -= new System.Action(this.OnWonderTitleDataUpdated);
  }

  private void ShowTitleDetail()
  {
    UIManager.inst.OpenPopup("Wonder/KingdomTitleInfoPopup", (Popup.PopupParameter) new KingdomTitleInfoPopup.Parameter()
    {
      kingdomId = this.kingdomId,
      wonderTitleInfo = this.wonderTitleInfo
    });
  }

  private void AppointKingdomTitle(string titleId, long targetUid)
  {
    MessageHub.inst.GetPortByAction("wonder:appointTitle").SendRequest(new Hashtable()
    {
      {
        (object) "title_id",
        (object) titleId
      },
      {
        (object) "target_uid",
        (object) targetUid
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || data == null)
        return;
      Hashtable data1 = data as Hashtable;
      if (data1 == null || data1.Count <= 0)
        return;
      ConfigManager.inst.DB_WonderTitle.UpdateWonderTitleData(data1, false);
    }), true);
  }

  private void OnWonderTitleDataUpdated()
  {
    if (this.wonderTitleInfo == null)
      return;
    this.wonderTitleInfo = ConfigManager.inst.DB_WonderTitle.Get(this.wonderTitleInfo.id);
    this.UpdateUI();
  }
}
