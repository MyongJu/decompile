﻿// Decompiled with JetBrains decompiler
// Type: CityTouchCircleBtn
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class CityTouchCircleBtn : SceneUI
{
  protected Dictionary<AtlasType, string> esPair = new Dictionary<AtlasType, string>(2);
  private const string BasicGui = "Basic_GUI";
  private const string Gui_2 = "GUI_2";
  protected const string attatchname = "attatchGO";
  public GameObject icon;
  public UILabel title;
  public UIAtlas gui_2;
  public UIAtlas basic_gui;
  public UISprite useUS;
  public GameObject effect;
  public UILabel badge;
  protected UIEventTrigger et;
  private CityCircleBtnPara mPara;
  private Coroutine refreshCoroutine;

  public virtual void Init(CityCircleBtnPara para)
  {
    this.mPara = para;
    this.AdjustAtlas(this.esPair[para.at]);
    this.AdjustSprite(para.spritename);
    this.useUS.spriteName = para.spritename;
    this.useUS.gameObject.name = para.spritename;
    this.title.text = ScriptLocalization.Get(para.titleid, true);
    if (para.isActive)
    {
      GreyUtility.Normal(this.useUS.gameObject);
      this.et.onClick.Clear();
      this.et.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnBtnClicked)));
      this.et.onClick.Add(para.ed);
      this.et.onClick.Add(new EventDelegate((MonoBehaviour) CityTouchCircle.Instance, "Close"));
    }
    else
    {
      GreyUtility.Grey(this.useUS.gameObject);
      this.et.onClick.Clear();
      if (para.warnTextId != null)
      {
        new WarningPara().titleId = para.warnTextId;
        this.et.onClick.Add(new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.toast.Show(ScriptLocalization.Get(para.warnTextId, true), (System.Action) null, 4f, true))));
      }
    }
    Transform child = this.transform.FindChild("attatchGO");
    if ((UnityEngine.Object) null != (UnityEngine.Object) child)
    {
      child.gameObject.SetActive(false);
      child.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) child.gameObject);
    }
    if ((UnityEngine.Object) null != (UnityEngine.Object) para.attachment)
    {
      GameObject gameObject = Utils.DuplicateGOB(para.attachment, this.transform);
      gameObject.name = "attatchGO";
      CityTouchCircleBtnAttachment component = gameObject.GetComponent<CityTouchCircleBtnAttachment>();
      if ((UnityEngine.Object) null != (UnityEngine.Object) component)
        component.SeedData(para.attachmentpara);
    }
    if ((double) para.refreshTime != 0.0 && para.refreshPara != null)
    {
      if (this.refreshCoroutine != null)
        this.StopCoroutine(this.refreshCoroutine);
      this.refreshCoroutine = this.StartCoroutine(this.RefreshStates(para.refreshTime, para.refreshPara));
    }
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.effect)
    {
      if (para.needEffect && !this.effect.activeSelf)
        this.effect.SetActive(true);
      else if (!para.needEffect && this.effect.activeSelf)
        this.effect.SetActive(false);
    }
    if (!(bool) ((UnityEngine.Object) this.badge))
      return;
    NGUITools.SetActive(this.badge.gameObject, para.showRedPoint);
  }

  internal void Reset()
  {
  }

  [DebuggerHidden]
  private IEnumerator RefreshStates(float p, CityCircleBtnPara cityCircleBtnPara)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CityTouchCircleBtn.\u003CRefreshStates\u003Ec__Iterator40()
    {
      p = p,
      cityCircleBtnPara = cityCircleBtnPara,
      \u003C\u0024\u003Ep = p,
      \u003C\u0024\u003EcityCircleBtnPara = cityCircleBtnPara,
      \u003C\u003Ef__this = this
    };
  }

  protected void AdjustSprite(string p)
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.useUS)
      return;
    UISpriteData sprite = this.useUS.atlas.GetSprite(p);
    if (sprite == null)
      return;
    this.useUS.height = (int) ((float) this.useUS.width / (float) sprite.width * (float) sprite.height);
    this.useUS.transform.localScale = new Vector3(1f, (float) this.useUS.width / (float) this.useUS.height, 1f);
  }

  protected void AdjustAtlas(string name)
  {
    if ("Basic_GUI" == name)
    {
      this.useUS.atlas = this.basic_gui;
    }
    else
    {
      if (!("GUI_2" == name))
        return;
      this.useUS.atlas = this.gui_2;
    }
  }

  protected virtual void Awake()
  {
    this.et = this.gameObject.GetComponent<UIEventTrigger>();
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.et)
      this.et = this.gameObject.AddComponent<UIEventTrigger>();
    this.esPair.Add(AtlasType.BasicGui, "Basic_GUI");
    this.esPair.Add(AtlasType.Gui_2, "GUI_2");
  }

  private void Update()
  {
  }

  public void OnBtnClicked()
  {
    if (this.mPara.needEffect)
      LinkerHub.Instance.NeedGuide = true;
    OperationTrace.TraceClickCircleMenu(this.mPara.titleid);
  }
}
