﻿// Decompiled with JetBrains decompiler
// Type: BuildLoader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using UnityEngine;

public class BuildLoader : MonoBehaviour
{
  private static BuildLoader _inst;
  private Hashtable config_ht;
  private Hashtable map_ht;
  private Hashtable bundle_ht;

  public static BuildLoader inst
  {
    get
    {
      if ((Object) BuildLoader._inst == (Object) null)
      {
        BuildLoader buildLoader = Object.FindObjectOfType<BuildLoader>();
        if ((Object) buildLoader == (Object) null)
          buildLoader = new GameObject().AddComponent<BuildLoader>();
        BuildLoader._inst = buildLoader;
      }
      return BuildLoader._inst;
    }
  }

  public void StartSycLoad(string server)
  {
    this.ClearBuildFolder();
    NetApi.inst.Dispose();
    WWW www = new WWW(server);
    Debug.Log((object) ("StartSycLoad -------- " + server));
    while (!www.isDone)
      Thread.Sleep(200);
    Debug.Log((object) ("StartSycLoad error message -------- " + www.error));
    object obj = Utils.Json2Object(Utils.ByteArray2String(www.bytes), true);
    if (string.IsNullOrEmpty(www.error) && obj != null)
    {
      NetApi.inst.Manifest = obj as Hashtable;
      this.LoadRemoteConfigVersionListSyc();
      Utils.WriteFile(this.GeneratePath("local_config_version"), Encoding.UTF8.GetBytes(NetApi.inst.ConfigFilesVersion));
    }
    else
    {
      Debug.Log((object) "StartSycLoad error .........");
      throw new FileLoadException();
    }
  }

  private void LoadRemoteConfigVersionListSyc()
  {
    WWW www = new WWW(NetApi.inst.BuildConfigUrl("VersionList.json"));
    while (!www.isDone)
      Thread.Sleep(200);
    if (string.IsNullOrEmpty(www.error))
    {
      object obj = Utils.Json2Object(Utils.ByteArray2String(www.bytes), true);
      Utils.WriteFile(string.Format("{0}/{1}", (object) BuildConfig.CONFIG_FOLDER, (object) "ConfigVersionList.json"), www.bytes);
      if (obj != null)
      {
        this.config_ht = obj as Hashtable;
        this.LoadConfigFilesSyc();
      }
      else
      {
        Debug.Log((object) "StartSycLoad error .........");
        throw new FileLoadException();
      }
    }
    else
    {
      Debug.Log((object) "StartSycLoad error .........");
      throw new FileLoadException();
    }
  }

  private void LoadRemoteMapVersionListSyc()
  {
  }

  private void LoadRemoteBundleVersionListSyc()
  {
    WWW www = new WWW(NetApi.inst.BuildAssetLocalServerUrl("VersionList.json"));
    while (!www.isDone)
      Thread.Sleep(200);
    object obj = Utils.Json2Object(Utils.ByteArray2String(www.bytes), true);
    Utils.WriteFile(this.GeneratePath("BundleVersionList.json"), www.bytes);
    if (obj == null)
      return;
    this.bundle_ht = obj as Hashtable;
    this.LoadBundleFilesSyc();
  }

  private void LoadConfigFilesSyc()
  {
    if (this.config_ht != null)
    {
      IDictionaryEnumerator enumerator = this.config_ht.GetEnumerator();
      while (enumerator.MoveNext())
      {
        bool flag = false;
        for (int index = 0; index < 20; ++index)
        {
          string fileName = enumerator.Key.ToString();
          string str = enumerator.Value.ToString();
          WWW www = new WWW(NetApi.inst.BuildConfigUrl(fileName));
          Debug.Log((object) (NetApi.inst.BuildConfigUrl(fileName) + "  is loading ...."));
          while (!www.isDone)
            Thread.Sleep(200);
          if (string.IsNullOrEmpty(www.error))
          {
            if (Utils.GetMD5HashString(Utils.ByteArray2String(www.bytes)) == str)
            {
              Utils.WriteFile(this.GeneratePath(fileName), www.bytes);
              flag = true;
              break;
            }
            Debug.Log((object) ("checking config file " + fileName + " fail...."));
          }
        }
        if (!flag)
          throw new FileLoadException();
      }
    }
    this.LoadRemoteMapVersionListSyc();
  }

  private void LoadMapFilesSyc()
  {
  }

  private void LoadBundleFilesSyc()
  {
    if (this.bundle_ht == null)
      return;
    IDictionaryEnumerator enumerator = this.bundle_ht.GetEnumerator();
    while (enumerator.MoveNext())
    {
      string str = enumerator.Key.ToString();
      if (str.Contains(".assetbundle") && str.Contains("static+"))
        this.StartCoroutine(this.LoadMainGameObject(str));
      else if (!str.Contains(".assetbundle") || !str.Contains("ota+"))
      {
        WWW www = new WWW(NetApi.inst.BuildAssetLocalServerUrl(str));
        while (!www.isDone)
          Thread.Sleep(200);
        Utils.WriteFile(this.GeneratePath(str), www.bytes);
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator LoadMainGameObject(string path)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BuildLoader.\u003CLoadMainGameObject\u003Ec__Iterator21()
    {
      path = path,
      \u003C\u0024\u003Epath = path,
      \u003C\u003Ef__this = this
    };
  }

  private string GeneratePath(string fileName)
  {
    return string.Format("{0}/{1}", (object) BuildConfig.CONFIG_FOLDER, (object) fileName);
  }

  private void ClearBuildFolder()
  {
    DirectoryInfo directoryInfo = new DirectoryInfo(BuildConfig.CONFIG_FOLDER);
    if (directoryInfo.Exists)
      directoryInfo.Delete(true);
    directoryInfo.Create();
  }
}
