﻿// Decompiled with JetBrains decompiler
// Type: ConstructionBuildingBtn
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ConstructionBuildingBtn : MonoBehaviour
{
  public string mBuildingIconStr = string.Empty;
  public string mBuildingCodeStr = string.Empty;
  public UITexture mBuildingIcon;
  public UILabel mBuildingName;
  public UILabel mRequirementsLabel;
  public UILabel mIntroduction;

  public void SetDetails(string buildingCode, string iconName, string buildingName, string briefName, bool bRequirementsOk)
  {
    this.mBuildingIconStr = iconName;
    this.mBuildingCodeStr = buildingCode;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.mBuildingIcon, iconName, (System.Action<bool>) null, true, false, string.Empty);
    Utils.SnapAndScale(this.mBuildingIcon, 1f);
    float num1 = (float) this.mBuildingIcon.width / 330f;
    float num2 = (float) this.mBuildingIcon.height / 218f;
    Utils.SnapAndScale(this.mBuildingIcon, 1.4f / ((double) num1 <= (double) num2 ? num2 : num1));
    this.mBuildingName.text = Utils.XLAT(buildingName + "_name");
    if (bRequirementsOk)
    {
      this.mIntroduction.gameObject.SetActive(true);
      this.mRequirementsLabel.gameObject.SetActive(false);
      this.mIntroduction.text = Utils.XLAT(briefName);
    }
    else
    {
      this.mIntroduction.gameObject.SetActive(false);
      this.mRequirementsLabel.gameObject.SetActive(true);
      this.mRequirementsLabel.text = Utils.XLAT("Not Available");
    }
  }
}
