﻿// Decompiled with JetBrains decompiler
// Type: IAPStorePackagePayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class IAPStorePackagePayload
{
  private List<IAPStorePackage> m_Packages = new List<IAPStorePackage>();
  private string _showBtn = "1";
  private string _topSeven = string.Empty;
  private static IAPStorePackagePayload m_Instance;
  private Coroutine m_Coroutine;
  private System.Action OnPurchaseFinished;
  private int _policy;
  private IAPStorePackage _recommendedPackage;

  public event System.Action<object> OnPurchaseSucceed;

  public event System.Action OnPackageDataChanged;

  public int Policy
  {
    get
    {
      return this._policy;
    }
  }

  public string TopSeven
  {
    get
    {
      return this._topSeven;
    }
  }

  public IAPStorePackage RecommendedPackage
  {
    get
    {
      return this._recommendedPackage;
    }
    set
    {
      this._recommendedPackage = value;
    }
  }

  public static IAPStorePackagePayload Instance
  {
    get
    {
      if (IAPStorePackagePayload.m_Instance == null)
        IAPStorePackagePayload.m_Instance = new IAPStorePackagePayload();
      return IAPStorePackagePayload.m_Instance;
    }
  }

  public void Initialize()
  {
    this.RequestServerData((System.Action<bool, object>) null, true);
    MessageHub.inst.GetPortByAction("payment_success").AddEvent(new System.Action<object>(this.OnPushPaymentSuccess));
    MessageHub.inst.GetPortByAction("anniversary_package").AddEvent(new System.Action<object>(this.OnPushPaymentSuccess));
    MessageHub.inst.GetPortByAction("merlin_pay_success").AddEvent(new System.Action<object>(this.OnPushPaymentSuccess));
  }

  public void Dispose()
  {
    MessageHub.inst.GetPortByAction("payment_success").RemoveEvent(new System.Action<object>(this.OnPushPaymentSuccess));
    MessageHub.inst.GetPortByAction("anniversary_package").RemoveEvent(new System.Action<object>(this.OnPushPaymentSuccess));
    MessageHub.inst.GetPortByAction("merlin_pay_success").RemoveEvent(new System.Action<object>(this.OnPushPaymentSuccess));
    this.OnPurchaseSucceed = (System.Action<object>) null;
    this.OnPackageDataChanged = (System.Action) null;
    this._recommendedPackage = (IAPStorePackage) null;
  }

  private void OnPushPaymentSuccess(object result)
  {
    if (this.OnPurchaseSucceed != null)
      this.OnPurchaseSucceed(result);
    this.RequestServerData((System.Action<bool, object>) ((_param1, _param2) =>
    {
      this.PublishPurchaseFinished();
      UIManager.inst.HideManualBlocker();
      this.StopCoroutine();
    }), false);
  }

  public void Decode(object orgData)
  {
    this.m_Packages.Clear();
    Hashtable inData = orgData as Hashtable;
    if (inData == null)
      return;
    this._showBtn = !inData.Contains((object) "show_btn") ? "1" : inData[(object) "show_btn"].ToString();
    DatabaseTools.UpdateData(inData, "iap_policy", ref this._policy);
    ArrayList arrayList = (ArrayList) null;
    if (inData.Contains((object) "content"))
      arrayList = inData[(object) "content"] as ArrayList;
    if (arrayList == null)
      return;
    for (int index = 0; index < arrayList.Count; ++index)
    {
      Hashtable data = arrayList[index] as Hashtable;
      IAPStorePackage iapStorePackage = new IAPStorePackage();
      iapStorePackage.Decode(data);
      this.m_Packages.Add(iapStorePackage);
    }
    this.m_Packages.Sort(new Comparison<IAPStorePackage>(IAPStorePackagePayload.Compare));
    if (inData.Contains((object) "lucky"))
      MarksmanPayload.Instance.Decode(inData[(object) "lucky"] as Hashtable);
    if (inData.Contains((object) "top7"))
      this._topSeven = inData[(object) "top7"].ToString();
    if (this.OnPackageDataChanged == null)
      return;
    this.OnPackageDataChanged();
  }

  public void PushPackageDataChanged()
  {
    if (this.OnPackageDataChanged == null)
      return;
    this.OnPackageDataChanged();
  }

  private static int Compare(IAPStorePackage x, IAPStorePackage y)
  {
    if (x.CalculatePriority(x.group.type) != y.CalculatePriority(y.group.type))
      return Math.Sign(y.CalculatePriority(y.group.type) - x.CalculatePriority(x.group.type));
    if (x.group.type == 5)
      return Math.Sign(x.priceInfo.price - y.priceInfo.price);
    return Math.Sign(y.paymentLevel - x.paymentLevel);
  }

  public List<IAPStorePackage> GetAvailablePackages()
  {
    List<IAPStorePackage> iapStorePackageList = new List<IAPStorePackage>();
    for (int index = 0; index < this.m_Packages.Count; ++index)
    {
      if (this.m_Packages[index].CanPurchase)
        iapStorePackageList.Add(this.m_Packages[index]);
    }
    return iapStorePackageList;
  }

  public IAPStorePackage GetFirstAvailablePackage()
  {
    for (int index = 0; index < this.m_Packages.Count; ++index)
    {
      if (this.m_Packages[index].CanPurchase)
        return this.m_Packages[index];
    }
    return (IAPStorePackage) null;
  }

  public string GetShowBtnState()
  {
    return this._showBtn;
  }

  public void BuyProduct(string productId, string packageId, string groupId, System.Action finish = null)
  {
    this.OnPurchaseFinished = finish;
    Hashtable postData = new Hashtable();
    postData[(object) "package_id"] = (object) packageId;
    postData[(object) "group_id"] = (object) groupId;
    MessageHub.inst.GetPortByAction("player:validateAndCreateOrder").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable inData = data as Hashtable;
      int outData = 0;
      DatabaseTools.UpdateData(inData, "can_buy", ref outData);
      if (outData == 1)
      {
        string empty = string.Empty;
        DatabaseTools.UpdateData(inData, "order_id", ref empty);
        if (Application.isEditor)
        {
          this.RequestServerData((System.Action<bool, object>) ((_param1, _param2) => this.PublishPurchaseFinished()), false);
        }
        else
        {
          UIManager.inst.ShowManualBlocker();
          PaymentManager.Instance.BuyProduct(productId, packageId, groupId, empty, new System.Action<bool>(this.OnPurchaseCallback));
          this.StopCoroutine();
          this.m_Coroutine = Utils.ExecuteInSecs(60f, (System.Action) (() => this.RequestServerData((System.Action<bool, object>) ((_param1, _param2) =>
          {
            this.PublishPurchaseFinished();
            UIManager.inst.HideManualBlocker();
            this.StopCoroutine();
          }), false)));
        }
      }
      else
      {
        UIManager.inst.toast.Show(Utils.XLAT("iap_package_1_year_celebration_sold_out_notice"), (System.Action) null, 4f, false);
        this.RequestServerData((System.Action<bool, object>) ((_param1, _param2) => this.PublishPurchaseFinished()), false);
      }
    }), true);
  }

  public void BuySpecialProduct(int iapId, string productId, string packageName, string groupName, System.Action finish = null)
  {
    this.OnPurchaseFinished = finish;
    if (Application.isEditor)
    {
      this.PublishPurchaseFinished();
    }
    else
    {
      UIManager.inst.ShowManualBlocker();
      PaymentManager.Instance.BuyIapProduct(productId, iapId.ToString(), packageName, groupName, new System.Action<bool>(this.OnPurchaseCallback));
      this.StopCoroutine();
      this.m_Coroutine = Utils.ExecuteInSecs(60f, (System.Action) (() =>
      {
        this.PublishPurchaseFinished();
        UIManager.inst.HideManualBlocker();
        this.StopCoroutine();
      }));
    }
  }

  private void PublishPurchaseFinished()
  {
    if (this.OnPurchaseFinished == null)
      return;
    this.OnPurchaseFinished();
    this.OnPurchaseFinished = (System.Action) null;
  }

  private void OnPurchaseCallback(bool success)
  {
    if (success)
      return;
    this.PublishPurchaseFinished();
    UIManager.inst.HideManualBlocker();
    this.StopCoroutine();
  }

  private void StopCoroutine()
  {
    if (this.m_Coroutine == null)
      return;
    Utils.StopCoroutine(this.m_Coroutine);
    this.m_Coroutine = (Coroutine) null;
  }

  private void RequestServerData(System.Action<bool, object> callback, bool loader = false)
  {
    if (loader)
      MessageHub.inst.GetPortByAction("Player:getIapPackage").SendLoader((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          this.Decode(data);
        if (callback == null)
          return;
        callback(ret, data);
      }), false, false);
    else
      MessageHub.inst.GetPortByAction("Player:getIapPackage").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          this.Decode(data);
        if (callback == null)
          return;
        callback(ret, data);
      }), true);
  }
}
