﻿// Decompiled with JetBrains decompiler
// Type: PitScoreRewardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PitScoreRewardPopup : Popup
{
  private List<PitScoreRewardItem> _allPitRewardItem = new List<PitScoreRewardItem>();
  [SerializeField]
  private UILabel _labelCurrentScore;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UIGrid _tableContainer;
  [SerializeField]
  private PitScoreRewardItem _pitScoreRewardItemTemplate;

  private PitScoreRewardItem CreatePitRewardItem()
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this._pitScoreRewardItemTemplate.gameObject);
    gameObject.transform.SetParent(this._tableContainer.transform, false);
    gameObject.SetActive(true);
    PitScoreRewardItem component = gameObject.GetComponent<PitScoreRewardItem>();
    this._allPitRewardItem.Add(component);
    return component;
  }

  private void DestroyAllPitRewardItem()
  {
    using (List<PitScoreRewardItem>.Enumerator enumerator = this._allPitRewardItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PitScoreRewardItem current = enumerator.Current;
        if ((bool) ((Object) current))
          Object.Destroy((Object) current.gameObject);
      }
    }
    this._allPitRewardItem.Clear();
  }

  public override void OnShow(UIControler.UIParameter parameter)
  {
    base.OnShow(parameter);
    this._pitScoreRewardItemTemplate.gameObject.SetActive(false);
    this.UpdateUI();
  }

  protected void UpdateUI()
  {
    this._labelCurrentScore.text = string.Format("{0}{1}", (object) ScriptLocalization.Get("event_fireland_dragon_fossil_collection_amount", true), (object) PitExplorePayload.Instance.ActivityData.CurrentScore);
    this.DestroyAllPitRewardItem();
    using (List<AbyssCollectRewardInfo>.Enumerator enumerator = ConfigManager.inst.DB_AbyssCollectReward.AllCollectRewardInfo.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.CreatePitRewardItem().SetData(enumerator.Current);
    }
    this._tableContainer.Reposition();
    this._scrollView.ResetPosition();
  }

  public void OnButtonCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }
}
