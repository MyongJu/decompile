﻿// Decompiled with JetBrains decompiler
// Type: SevenDaysQuestRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UnityEngine;

public class SevenDaysQuestRenderer : MonoBehaviour
{
  public ItemIconRenderer itemRender;
  public UILabel count;
  public UILabel desc;
  public GameObject expired;
  public UILabel status;
  public GameObject statusGo;
  public UIButton claimBt;
  private long _questId;
  public GameObject gayGame;
  public GameObject finishGo;
  private bool _destroy;

  private void OnDestroy()
  {
    this._destroy = true;
  }

  public void SetData(int configId)
  {
    if (this._questId == (long) configId)
    {
      this.CheckStatus();
    }
    else
    {
      this._questId = (long) configId;
      SevenDayQuestInfo data = ConfigManager.inst.SevenDayQuests.GetData(configId);
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(data.ItemId);
      if (data != null && itemStaticInfo != null)
      {
        this.itemRender.SetData(data.ItemId, string.Empty, false);
        this.count.text = data.ItemCount.ToString();
        this.desc.text = data.LocalDesc;
      }
      this.CheckStatus();
    }
  }

  private void OnEnable()
  {
    this.AddEventHandler();
  }

  private void OnDisable()
  {
    this.RemoveEventHandler();
  }

  private void AddEventHandler()
  {
    this.RemoveEventHandler();
    DBManager.inst.DB_SevenQuests.onDataChanged += new System.Action<long>(this.OnDataChange);
  }

  private void RemoveEventHandler()
  {
    if (!GameEngine.IsAvailable)
      return;
    DBManager.inst.DB_SevenQuests.onDataChanged -= new System.Action<long>(this.OnDataChange);
  }

  private void OnDataChange(long questId)
  {
    if (this._questId != questId)
      return;
    this.CheckStatus();
  }

  private void Clear()
  {
  }

  public void OnClaim()
  {
    RequestManager.inst.SendRequest("sevenDays:collectReward", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "quest_id", (object) this._questId), new System.Action<bool, object>(this.ClaimHandler), true);
  }

  private void ClaimHandler(bool success, object result)
  {
    if (!success || this._destroy)
      return;
    this.CheckStatus();
    SevenDayQuestInfo data = ConfigManager.inst.SevenDayQuests.GetData((int) this._questId);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(data.ItemId);
    RewardsCollectionAnimator.Instance.Clear();
    RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
    {
      count = (float) data.ItemCount,
      icon = itemStaticInfo.ImagePath
    });
    RewardsCollectionAnimator.Instance.CollectItems(true);
  }

  private SevenQuestData Data
  {
    get
    {
      return DBManager.inst.DB_SevenQuests.GetSevenQuestData((int) this._questId);
    }
  }

  private void CheckStatus()
  {
    SevenQuestData sevenQuestData = DBManager.inst.DB_SevenQuests.GetSevenQuestData((int) this._questId);
    string Term = "event_7_days_quest_not_started";
    Color color = Color.green;
    bool flag1 = false;
    bool flag2 = false;
    bool state = false;
    if (sevenQuestData == null)
    {
      color = Color.red;
      Term = "event_7_days_quest_not_started";
    }
    else
    {
      state = sevenQuestData.IsFinish;
      flag2 = sevenQuestData.IsClaim;
      if (!sevenQuestData.IsExpired)
      {
        switch (sevenQuestData.Status)
        {
          case 0:
            Term = "event_7_days_quest_incomplete";
            break;
          case 1:
            Term = "id_completed";
            state = true;
            break;
          case 2:
            Term = "event_7_days_quest_claimed";
            flag2 = true;
            break;
        }
      }
      else
      {
        Term = "id_expired";
        color = Color.red;
        flag1 = true;
        if (sevenQuestData.Status == 1)
          flag1 = false;
        if (sevenQuestData.Status == 2)
        {
          Term = "event_7_days_quest_claimed";
          flag1 = false;
        }
      }
    }
    this.status.color = color;
    this.status.text = ScriptLocalization.Get(Term, true);
    NGUITools.SetActive(this.claimBt.gameObject, state);
    NGUITools.SetActive(this.finishGo, state);
    NGUITools.SetActive(this.statusGo, !state);
    if (flag1)
      GreyUtility.Grey(this.gayGame);
    else
      GreyUtility.Normal(this.gayGame);
  }
}
