﻿// Decompiled with JetBrains decompiler
// Type: Costs
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Pathfinding.Serialization.JsonFx;
using System.Collections;

public class Costs : ICustomParse
{
  public int food;
  public int wood;
  public int ore;
  public int silver;

  public void Parse(object content)
  {
    Hashtable hashtable = content as Hashtable;
    if (hashtable.ContainsKey((object) "food"))
      this.food = int.Parse(hashtable[(object) "food"].ToString());
    if (hashtable.ContainsKey((object) "wood"))
      this.wood = int.Parse(hashtable[(object) "wood"].ToString());
    if (hashtable.ContainsKey((object) "ore"))
      this.ore = int.Parse(hashtable[(object) "ore"].ToString());
    if (!hashtable.ContainsKey((object) "silver"))
      return;
    this.silver = int.Parse(hashtable[(object) "silver"].ToString());
  }

  public bool IsFoodEnough()
  {
    return PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD) >= (double) this.food;
  }

  public bool IsWoodEnough()
  {
    return PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD) >= (double) this.wood;
  }

  public bool IsOreEnough()
  {
    return PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE) >= (double) this.ore;
  }

  public bool IsSilverEnough()
  {
    return PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER) >= (double) this.silver;
  }

  public static Costs Build(string content)
  {
    return JsonReader.Deserialize<Costs>(content);
  }
}
