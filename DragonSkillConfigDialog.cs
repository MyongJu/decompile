﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillConfigDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;

public class DragonSkillConfigDialog : UI.Dialog
{
  private int m_SelectedIndex = -1;
  public UIToggle[] m_Toggles;
  public DragonSkillConfigContainer m_LightPanel;
  public DragonSkillConfigContainer m_DarkPanel;
  public DragonSkillBar m_SkillBar;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_LightPanel.OnEquip = new OnDragonSkillEquipCallback(this.OnEquip);
    this.m_DarkPanel.OnEquip = new OnDragonSkillEquipCallback(this.OnEquip);
    DragonSkillConfigDialog.Parameter parameter = orgParam as DragonSkillConfigDialog.Parameter;
    this.m_SelectedIndex = -1;
    if (parameter != null)
      this.m_Toggles[parameter.tabIndex].Set(true);
    else
      this.m_Toggles[0].Set(true);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnToggleValueChanged()
  {
    int selectedIndex = this.GetSelectedIndex();
    if (this.m_SelectedIndex == selectedIndex || selectedIndex == -1)
      return;
    this.m_SelectedIndex = selectedIndex;
    string usability = this.GetUsability();
    List<ConfigDragonSkillGroupInfo> typeAndUsability1 = DBManager.inst.DB_DragonSkill.GetLearnedDragonSkillGroupsByTypeAndUsability(ConfigDragonSkillGroupInfo.LIGHT, usability);
    List<ConfigDragonSkillGroupInfo> typeAndUsability2 = DBManager.inst.DB_DragonSkill.GetLearnedDragonSkillGroupsByTypeAndUsability(ConfigDragonSkillGroupInfo.DARK, usability);
    this.m_LightPanel.UpdateUI(usability, typeAndUsability1);
    this.m_DarkPanel.UpdateUI(usability, typeAndUsability2);
    this.m_SkillBar.SetData(usability);
  }

  private void OnEquip(int skill_id)
  {
    this.m_SkillBar.Equip(skill_id);
  }

  private void OnUnEquip(int skill_id)
  {
    this.m_SkillBar.UnEquip(skill_id);
  }

  private string GetUsability()
  {
    switch (this.m_SelectedIndex)
    {
      case 0:
        return "attack";
      case 1:
        return "defend";
      case 2:
        return "gather";
      case 3:
        return "attack_monster";
      default:
        return string.Empty;
    }
  }

  private int GetSelectedIndex()
  {
    for (int index = 0; index < this.m_Toggles.Length; ++index)
    {
      if (this.m_Toggles[index].value)
        return index;
    }
    return -1;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int tabIndex;
  }
}
