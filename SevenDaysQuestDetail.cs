﻿// Decompiled with JetBrains decompiler
// Type: SevenDaysQuestDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class SevenDaysQuestDetail : MonoBehaviour
{
  private GameObjectPool _pool = new GameObjectPool();
  private List<SevenDaysQuestRenderer> _items = new List<SevenDaysQuestRenderer>();
  public UIScrollView scrollView;
  public UIGrid grid;
  public SevenDaysQuestRenderer itemPrefab;
  private int _day;
  private bool _init;

  public void SetData(int day)
  {
    if (this._day == day)
      return;
    this._day = day;
    List<SevenDayQuestInfo> dataByDay = ConfigManager.inst.SevenDayQuests.GetDataByDay(day);
    this.Clear();
    for (int index = 0; index < dataByDay.Count; ++index)
    {
      SevenDaysQuestRenderer daysQuestRenderer = this.GetItem();
      daysQuestRenderer.SetData(dataByDay[index].internalId);
      NGUITools.SetActive(daysQuestRenderer.gameObject, false);
      NGUITools.SetActive(daysQuestRenderer.gameObject, true);
    }
    this.grid.Reposition();
    this.scrollView.ResetPosition();
  }

  private SevenDaysQuestRenderer GetItem()
  {
    if (!this._init)
    {
      this._init = true;
      this._pool.Initialize(this.itemPrefab.gameObject, this.gameObject);
    }
    GameObject go = this._pool.AddChild(this.grid.gameObject);
    SevenDaysQuestRenderer component = go.GetComponent<SevenDaysQuestRenderer>();
    NGUITools.SetActive(go, true);
    this._items.Add(component);
    return component;
  }

  private void Clear()
  {
    for (int index = 0; index < this._items.Count; ++index)
      this._pool.Release(this._items[index].gameObject);
    this._items.Clear();
  }

  public void Dispose()
  {
    this.Clear();
    this._pool.Clear();
    this._init = false;
  }

  public void Hide()
  {
  }
}
