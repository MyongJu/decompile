﻿// Decompiled with JetBrains decompiler
// Type: ChatPopup_ChatBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Chat;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ChatPopup_ChatBar : MonoBehaviour
{
  public ChatDlg chatDlg;
  public UIButton buttonReportIcon;
  public UIButton muteBtn;
  public UIGrid buttonContainer;
  private long _personalChannelId;
  private long _personalUid;
  private string _personalName;

  public void Show(long personalChannelId, long personalUid, string personalName, string senderCustomIcon)
  {
    this.gameObject.SetActive(true);
    this._personalChannelId = personalChannelId;
    this._personalUid = personalUid;
    this._personalName = personalName;
    this.buttonReportIcon.gameObject.SetActive(!string.IsNullOrEmpty(senderCustomIcon));
    this.muteBtn.gameObject.SetActive(PlayerData.inst.userData.isMuteAdmin);
    this.buttonContainer.repositionNow = true;
    this.buttonContainer.Reposition();
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  public void OnUserProfileClick()
  {
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
    {
      uid = this._personalUid
    }, 1 != 0, 1 != 0, 1 != 0);
    this.Hide();
  }

  public void OnSendMailClick()
  {
    PlayerData.inst.mail.ComposeMail(new List<string>()
    {
      this._personalName
    }, (string) null, (string) null, 0 != 0);
    this.Hide();
  }

  public void OnAddContactClick()
  {
    ContactManager.AddContact(this._personalUid, (System.Action<bool, object>) null);
    this.Hide();
  }

  public void OnBlockClick()
  {
    ContactManager.BlockUser(this._personalUid, (System.Action<bool, object>) null);
    this.Hide();
  }

  public void OnCloseClick()
  {
    this.Hide();
  }

  public void OnReportBtnClicked()
  {
    string str1 = ScriptLocalization.Get("id_uppercase_report", true);
    string str2 = ScriptLocalization.Get("player_profile_avatar_report_confirm_description", true);
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = str1,
      Content = str2,
      Okay = ScriptLocalization.Get("id_uppercase_confirm", true),
      OkayCallback = new System.Action(this.OnConfirmReportIcon)
    });
    this.Hide();
  }

  protected void OnConfirmReportIcon()
  {
    Hashtable postData = new Hashtable()
    {
      {
        (object) "report_uid",
        (object) this._personalUid
      }
    };
    RequestManager.inst.SendRequest("Player:reportIcon", postData, new System.Action<bool, object>(this.OnReportCallback), true);
  }

  protected void OnReportCallback(bool result, object data)
  {
    if (!result)
      return;
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_avatar_report_success", true), (System.Action) null, 4f, true);
  }
}
