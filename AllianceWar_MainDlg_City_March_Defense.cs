﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_City_March_Defense
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class AllianceWar_MainDlg_City_March_Defense : AllianceWar_MainDlg_March_Slot
{
  public override void Setup(long marchId)
  {
    MarchData marchData = DBManager.inst.DB_March.Get(marchId);
    if (marchData == null)
      return;
    this.data.marchId = marchId;
    this.SetLeftItems(marchData, DBManager.inst.DB_User.Get(marchData.targetUid), marchData.targetLocation);
    this.SetRightItems(marchData, DBManager.inst.DB_User.Get(marchData.ownerUid), marchData.ownerLocation);
    this.OnSecond(NetServerTime.inst.ServerTimestamp);
  }
}
