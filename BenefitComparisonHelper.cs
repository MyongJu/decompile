﻿// Decompiled with JetBrains decompiler
// Type: BenefitComparisonHelper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class BenefitComparisonHelper
{
  public static Dictionary<string, BenefitComparisonElement> SetupResearchBenefits(string category = "")
  {
    Dictionary<string, BenefitComparisonElement> dictionary = new Dictionary<string, BenefitComparisonElement>();
    using (Dictionary<string, BenefitInfo>.Enumerator enumerator = DBManager.inst.DB_Local_Benefit.benefits.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, BenefitInfo> current = enumerator.Current;
        string longId = DBManager.inst.DB_Local_Benefit.GetLongID(current.Key);
        BenefitInfo benefitInfo = current.Value;
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[longId];
        if (dbProperty == null)
          D.warn((object) "this is a null point, key[{0}]", (object) current.Key);
        else if (category.Equals(string.Empty) || category.Equals(dbProperty.Category))
        {
          float researchBenefit = benefitInfo.GetResearchBenefit();
          if ((double) Mathf.Abs(researchBenefit) > 1.40129846432482E-45)
          {
            if (dictionary.ContainsKey(longId))
            {
              dictionary[longId].value1 = researchBenefit;
            }
            else
            {
              BenefitComparisonElement comparisonElement = new BenefitComparisonElement(longId, researchBenefit, 0.0f);
              dictionary.Add(longId, comparisonElement);
            }
          }
        }
      }
    }
    return dictionary;
  }

  public static Dictionary<string, BenefitComparisonElement> SetupEquipmentBenefits(string category = "")
  {
    Dictionary<string, BenefitComparisonElement> dictionary = new Dictionary<string, BenefitComparisonElement>();
    using (Dictionary<string, BenefitInfo>.Enumerator enumerator = DBManager.inst.DB_Local_Benefit.benefits.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, BenefitInfo> current = enumerator.Current;
        string longId = DBManager.inst.DB_Local_Benefit.GetLongID(current.Key);
        BenefitInfo benefitInfo = current.Value;
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[longId];
        if (dbProperty == null)
          D.warn((object) "this is a null point, key[{0}]", (object) current.Key);
        else if (category.Equals(string.Empty) || category.Equals(dbProperty.Category))
        {
          float equipmentBenefit = benefitInfo.GetEquipmentBenefit();
          if ((double) Mathf.Abs(equipmentBenefit) > 1.40129846432482E-45)
          {
            if (dictionary.ContainsKey(longId))
            {
              dictionary[longId].value1 = equipmentBenefit;
            }
            else
            {
              BenefitComparisonElement comparisonElement = new BenefitComparisonElement(longId, equipmentBenefit, 0.0f);
              dictionary.Add(longId, comparisonElement);
            }
          }
        }
      }
    }
    return dictionary;
  }

  public static Dictionary<string, BenefitComparisonElement> SetupTalentBenefits(string category = "")
  {
    Dictionary<string, BenefitComparisonElement> dictionary = new Dictionary<string, BenefitComparisonElement>();
    using (Dictionary<string, BenefitInfo>.Enumerator enumerator = DBManager.inst.DB_Local_Benefit.benefits.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, BenefitInfo> current = enumerator.Current;
        string longId = DBManager.inst.DB_Local_Benefit.GetLongID(current.Key);
        BenefitInfo benefitInfo = current.Value;
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[longId];
        if (dbProperty == null)
          D.warn((object) "this is a null point, key[{0}]", (object) current.Key);
        else if (category.Equals(string.Empty) || category.Equals(dbProperty.Category))
        {
          float heroBenefit = benefitInfo.GetHeroBenefit();
          if ((double) Mathf.Abs(heroBenefit) > 1.40129846432482E-45)
          {
            if (dictionary.ContainsKey(longId))
            {
              dictionary[longId].value1 = heroBenefit;
            }
            else
            {
              BenefitComparisonElement comparisonElement = new BenefitComparisonElement(longId, heroBenefit, 0.0f);
              dictionary.Add(longId, comparisonElement);
            }
          }
        }
      }
    }
    return dictionary;
  }

  public static Dictionary<string, BenefitComparisonElement> SetupBuildingBenefits(string category = "")
  {
    Dictionary<string, BenefitComparisonElement> dictionary = new Dictionary<string, BenefitComparisonElement>();
    using (Dictionary<string, BenefitInfo>.Enumerator enumerator = DBManager.inst.DB_Local_Benefit.benefits.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, BenefitInfo> current = enumerator.Current;
        string longId = DBManager.inst.DB_Local_Benefit.GetLongID(current.Key);
        BenefitInfo benefitInfo = current.Value;
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[longId];
        if (dbProperty == null)
          D.warn((object) "this is a null point, key[{0}]", (object) current.Key);
        else if (category.Equals(string.Empty) || category.Equals(dbProperty.Category))
        {
          float buildingBenefit = benefitInfo.GetBuildingBenefit();
          if ((double) Mathf.Abs(buildingBenefit) > 1.40129846432482E-45)
          {
            if (dictionary.ContainsKey(longId))
            {
              dictionary[longId].value1 = buildingBenefit;
            }
            else
            {
              BenefitComparisonElement comparisonElement = new BenefitComparisonElement(longId, buildingBenefit, 0.0f);
              dictionary.Add(longId, comparisonElement);
            }
          }
        }
      }
    }
    return dictionary;
  }

  public static bool IsSelfBenefitEmpty(List<BenefitComparisonElement> list)
  {
    if (list == null)
      return true;
    using (List<BenefitComparisonElement>.Enumerator enumerator = list.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if ((double) Mathf.Abs(enumerator.Current.value1) > 1.40129846432482E-45)
          return false;
      }
    }
    return true;
  }

  public static bool IsOtherBenefitEmpty(List<BenefitComparisonElement> list)
  {
    if (list == null)
      return true;
    using (List<BenefitComparisonElement>.Enumerator enumerator = list.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if ((double) Mathf.Abs(enumerator.Current.value2) > 1.40129846432482E-45)
          return false;
      }
    }
    return true;
  }
}
