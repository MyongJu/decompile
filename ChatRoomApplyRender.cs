﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomApplyRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class ChatRoomApplyRender : MonoBehaviour
{
  public Icon roomIcon;
  public UIButton cancelApplyBtn;
  private ChatRoomData _roomData;

  public void Feed(ChatRoomData data)
  {
    this._roomData = data;
    this.roomIcon.SetData(data.GetOwnerMember().PortraitIconPath, new string[3]
    {
      data.roomName,
      data.roomTitle,
      string.Format("{0}/{1}", (object) data.MemberCount, (object) data.TotalCount)
    });
    this.cancelApplyBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnCancelApplyBtnClick)));
  }

  private void OnCancelApplyBtnClick()
  {
    ChatRoomManager.CancelApply(this._roomData.roomId);
  }
}
