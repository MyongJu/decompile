﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsHelper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class MerlinTrialsHelper
{
  private static readonly MerlinTrialsHelper _instance = new MerlinTrialsHelper();
  private readonly List<MerlinDailyInfo> _dailyInfos = new List<MerlinDailyInfo>();
  private readonly Dictionary<int, List<int>> _cliamedChestDic = new Dictionary<int, List<int>>();
  private readonly Dictionary<int, bool> _groupCompleteDict = new Dictionary<int, bool>();
  public const int MAX_DAY = 7;
  private Hashtable _mysteryShopInfo;
  public System.Action onMerlinTrialsDataChanged;

  private MerlinTrialsHelper()
  {
  }

  public event System.Action<int> onGroupComplete;

  public int Day { private set; get; }

  public int Deadline { private set; get; }

  public static MerlinTrialsHelper inst
  {
    get
    {
      return MerlinTrialsHelper._instance;
    }
  }

  public void CheckIn()
  {
    PlayerPrefsEx.SetStringByUid("merlin_check_in", DateTime.Now.ToString("d"));
  }

  public bool IsTodayCheckedIn()
  {
    return PlayerPrefsEx.GetStringByUid("merlin_check_in").Equals(DateTime.Now.ToString("d"));
  }

  public int GetCompleteNumbers(int day)
  {
    if (this._dailyInfos == null || day >= this._dailyInfos.Count || day < 0)
      return 0;
    return this._dailyInfos[day].number;
  }

  public bool IsGroupComplete(int groupId)
  {
    if (this._groupCompleteDict.ContainsKey(groupId))
      return this._groupCompleteDict[groupId];
    return false;
  }

  public void SetGroupComplete(int groupId)
  {
    this._groupCompleteDict[groupId] = true;
    if (this.onGroupComplete == null)
      return;
    this.onGroupComplete(groupId);
  }

  public int GetTotalBattleCount(int groupId)
  {
    List<MerlinTrialsMainInfo> allByGroup = ConfigManager.inst.DB_MerlinTrialsMain.GetAllByGroup(groupId);
    if (allByGroup == null)
      return 0;
    return allByGroup.Count;
  }

  public bool IsLastBattleOfLevel(int battleId)
  {
    MerlinTrialsMainInfo merlinTrialsMainInfo = ConfigManager.inst.DB_MerlinTrialsMain.Get(battleId);
    List<MerlinTrialsMainInfo> allByLayerId = ConfigManager.inst.DB_MerlinTrialsMain.GetAllByLayerId(merlinTrialsMainInfo.trialId, merlinTrialsMainInfo.layerId);
    return allByLayerId[allByLayerId.Count - 1].internalId == battleId;
  }

  public bool IsLastBattleOfGroup(int battleId)
  {
    List<MerlinTrialsMainInfo> allByGroup = ConfigManager.inst.DB_MerlinTrialsMain.GetAllByGroup(ConfigManager.inst.DB_MerlinTrialsMain.Get(battleId).trialId);
    return allByGroup[allByGroup.Count - 1].internalId == battleId;
  }

  public MerlinTrialsMainInfo LatestAvaliableTrialConfig(int group)
  {
    int completeNumbers = this.GetCompleteNumbers(ConfigManager.inst.DB_MerlinTrialsGroup.Group2Day(group));
    List<MerlinTrialsMainInfo> allByGroup = ConfigManager.inst.DB_MerlinTrialsMain.GetAllByGroup(group);
    if (allByGroup == null || allByGroup.Count == 0)
    {
      D.error((object) "MerlinTrials cannot find MerlinTrialsMain by groupId {0}", (object) group);
      return (MerlinTrialsMainInfo) null;
    }
    if (completeNumbers <= 0)
      return allByGroup[0];
    if (completeNumbers >= allByGroup.Count)
      return allByGroup[allByGroup.Count - 1];
    return allByGroup[completeNumbers];
  }

  public MerlinTrialsMainInfo MaxAvaliableTrialConfig(int group, int level)
  {
    int layerId = ConfigManager.inst.DB_MerlinTrialsLayer.GetLayerId(group, level);
    List<MerlinTrialsMainInfo> allByLayerId = ConfigManager.inst.DB_MerlinTrialsMain.GetAllByLayerId(group, layerId);
    MerlinTrialsMainInfo merlinTrialsMainInfo = this.LatestAvaliableTrialConfig(group);
    if (allByLayerId[allByLayerId.Count - 1].number > merlinTrialsMainInfo.number)
      return merlinTrialsMainInfo;
    return allByLayerId[allByLayerId.Count - 1];
  }

  public MerlinTrialsLayerInfo LatestLevelConfig(int group)
  {
    return ConfigManager.inst.DB_MerlinTrialsLayer.Get(this.LatestAvaliableTrialConfig(group).layerId);
  }

  public void UpdateLastNumber(int battleId)
  {
    MerlinTrialsMainInfo merlinTrialsMainInfo = ConfigManager.inst.DB_MerlinTrialsMain.Get(battleId);
    if (merlinTrialsMainInfo == null)
      return;
    MerlinTrialsGroupInfo merlinTrialsGroupInfo = ConfigManager.inst.DB_MerlinTrialsGroup.Get(merlinTrialsMainInfo.trialId);
    if (merlinTrialsGroupInfo == null)
      return;
    int openDay = merlinTrialsGroupInfo.OpenDays[0];
    if (this._dailyInfos == null || openDay >= this._dailyInfos.Count || (openDay < 0 || merlinTrialsMainInfo.number <= this._dailyInfos[openDay].number))
      return;
    this._dailyInfos[openDay].number = merlinTrialsMainInfo.number;
  }

  public bool IsRewardable(int day)
  {
    if (this._dailyInfos == null || day >= this._dailyInfos.Count || day < 0)
      return false;
    return this._dailyInfos[day].rewardable != 0;
  }

  public bool IsOpenDay(int battleId)
  {
    MerlinTrialsMainInfo merlinTrialsMainInfo = ConfigManager.inst.DB_MerlinTrialsMain.Get(battleId);
    if (merlinTrialsMainInfo == null)
      return false;
    int num = ConfigManager.inst.DB_MerlinTrialsGroup.Day2Group(this.Day);
    return merlinTrialsMainInfo.trialId == num;
  }

  public string GetDayString(int day)
  {
    switch (day)
    {
      case 1:
        return Utils.XLAT("id_monday");
      case 2:
        return Utils.XLAT("id_tuesday");
      case 3:
        return Utils.XLAT("id_wednesday");
      case 4:
        return Utils.XLAT("id_thursday");
      case 5:
        return Utils.XLAT("id_friday");
      case 6:
        return Utils.XLAT("id_saturday");
      default:
        return Utils.XLAT("id_sunday");
    }
  }

  public MerlinMonsterStatus GetMonsterStatus(int battleId)
  {
    MerlinTrialsMainInfo merlinTrialsMainInfo1 = ConfigManager.inst.DB_MerlinTrialsMain.Get(battleId);
    MerlinTrialsMainInfo merlinTrialsMainInfo2 = this.LatestAvaliableTrialConfig(merlinTrialsMainInfo1.trialId);
    if (merlinTrialsMainInfo1.number < merlinTrialsMainInfo2.number)
      return MerlinMonsterStatus.defeated;
    if (merlinTrialsMainInfo1.number > merlinTrialsMainInfo2.number)
      return MerlinMonsterStatus.locked;
    return this.IsGroupComplete(merlinTrialsMainInfo1.trialId) ? MerlinMonsterStatus.defeated : MerlinMonsterStatus.can_attack;
  }

  public bool HasUnclaimedChest(int groupId, int level)
  {
    int layerId = ConfigManager.inst.DB_MerlinTrialsLayer.GetLayerId(groupId, level);
    List<MerlinTrialsMainInfo> allByLayerId = ConfigManager.inst.DB_MerlinTrialsMain.GetAllByLayerId(groupId, layerId);
    if (allByLayerId == null)
      return false;
    List<MerlinTrialsMainInfo>.Enumerator enumerator = allByLayerId.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (this.GetChestStatus(enumerator.Current.internalId) == MerlinChestStatus.can_claim)
        return true;
    }
    return false;
  }

  public MerlinChestStatus GetChestStatus(int battleId)
  {
    MerlinTrialsMainInfo merlinTrialsMainInfo = ConfigManager.inst.DB_MerlinTrialsMain.Get(battleId);
    int trialId = merlinTrialsMainInfo.trialId;
    int completeNumbers = this.GetCompleteNumbers(ConfigManager.inst.DB_MerlinTrialsGroup.Group2Day(trialId));
    if (merlinTrialsMainInfo.number > completeNumbers)
      return MerlinChestStatus.not_ready;
    if (!this._cliamedChestDic.ContainsKey(trialId))
      return MerlinChestStatus.can_claim;
    List<int> intList = this._cliamedChestDic[trialId];
    int index = 0;
    for (int count = intList.Count; index < count; ++index)
    {
      if (battleId == intList[index])
        return MerlinChestStatus.claimed;
    }
    return MerlinChestStatus.can_claim;
  }

  public void RequestMerlinStatus(System.Action callback)
  {
    if (callback == null)
      MessageHub.inst.GetPortByAction("Merlin:getStatus").SendLoader((Hashtable) null, (System.Action<bool, object>) ((result, response) =>
      {
        if (!result)
          return;
        this.Decode(response as Hashtable);
      }), true, false);
    else
      MessageHub.inst.GetPortByAction("Merlin:getStatus").SendRequest((Hashtable) null, (System.Action<bool, object>) ((result, response) =>
      {
        if (!result)
          return;
        this.Decode(response as Hashtable);
        callback();
      }), true);
  }

  public void RequestBenefits(int day, System.Action<object> callback)
  {
    MessageHub.inst.GetPortByAction("Merlin:getBenefit").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) nameof (day), (object) day), (System.Action<bool, object>) ((result, response) =>
    {
      if (!result)
        return;
      Hashtable hashtable = response as Hashtable;
      if (!hashtable.ContainsKey((object) "benefits") || callback == null)
        return;
      callback(hashtable[(object) "benefits"]);
    }), true);
  }

  public void RequestCapacityInfo(int battleId, System.Action<object> callback)
  {
    MessageHub.inst.GetPortByAction("Merlin:getCapacity").SendRequest(Utils.Hash((object) nameof (battleId), (object) battleId, (object) "uid", (object) PlayerData.inst.uid), (System.Action<bool, object>) ((ret, response) =>
    {
      if (!ret || callback == null)
        return;
      callback(response);
    }), true);
  }

  public void RequestCombat(int battleId, System.Action<object> callback)
  {
    MessageHub.inst.GetPortByAction("Merlin:combat").SendRequest(Utils.Hash((object) nameof (battleId), (object) battleId, (object) "uid", (object) PlayerData.inst.uid), (System.Action<bool, object>) ((ret, resposne) =>
    {
      if (!ret)
        return;
      this.Decode(resposne as Hashtable);
      if (callback == null)
        return;
      callback(resposne);
    }), true);
  }

  public void RequestBattleStatus(int groupId, System.Action<object> callback)
  {
    MessageHub.inst.GetPortByAction("Merlin:getBattleStatus").SendRequest(Utils.Hash((object) nameof (groupId), (object) groupId), (System.Action<bool, object>) ((ret, response) =>
    {
      if (!ret)
        return;
      this.UpdateChestData(groupId, response as Hashtable);
      if (callback == null)
        return;
      callback(response);
    }), true);
  }

  public void UpdateChestData(int groupId, Hashtable data)
  {
    if (data == null || !data.ContainsKey((object) "rewardedBattles"))
      return;
    if (!this._cliamedChestDic.ContainsKey(groupId))
      this._cliamedChestDic.Add(groupId, new List<int>());
    this._cliamedChestDic[groupId].Clear();
    foreach (object obj in data[(object) "rewardedBattles"] as ArrayList)
    {
      int result = 0;
      int.TryParse(obj.ToString(), out result);
      this._cliamedChestDic[groupId].Add(result);
    }
  }

  public void UpdateDailyData(int day, bool collected)
  {
    if (day >= 0 && day < this._dailyInfos.Count)
      this._dailyInfos[day].rewardable = !collected ? 1 : 0;
    if (this.onMerlinTrialsDataChanged == null)
      return;
    this.onMerlinTrialsDataChanged();
  }

  public int GetMysteryShopEndTime()
  {
    if (this._mysteryShopInfo == null)
      return 0;
    int result = 0;
    if (this._mysteryShopInfo.ContainsKey((object) "expires"))
      int.TryParse(this._mysteryShopInfo[(object) "expires"].ToString(), out result);
    return result;
  }

  public int GetMysteryShopBattleId()
  {
    if (this._mysteryShopInfo == null)
      return 0;
    int result = 0;
    if (this._mysteryShopInfo.ContainsKey((object) "battleId"))
      int.TryParse(this._mysteryShopInfo[(object) "battleId"].ToString(), out result);
    return result;
  }

  public bool ShouldShowMysteryShop()
  {
    bool flag = false;
    MerlinTrialsMainInfo merlinTrialsMainInfo = ConfigManager.inst.DB_MerlinTrialsMain.Get(this.GetMysteryShopBattleId());
    if (merlinTrialsMainInfo != null)
    {
      for (int index = 0; index < merlinTrialsMainInfo.IapIdArray.Length; ++index)
        flag |= merlinTrialsMainInfo.IapIdArray[index] > 0;
    }
    if (flag)
      return this.GetMysteryShopEndTime() > NetServerTime.inst.ServerTimestamp;
    return false;
  }

  public bool IsMysteryShopExist(int battleId)
  {
    return this.GetMysteryShopBattleId() == battleId;
  }

  public bool HasDailyReward
  {
    get
    {
      if (this._dailyInfos != null)
      {
        using (List<MerlinDailyInfo>.Enumerator enumerator = this._dailyInfos.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            if (enumerator.Current.rewardable == 1)
              return true;
          }
        }
      }
      return false;
    }
  }

  private void Decode(Hashtable orgData)
  {
    if (orgData == null)
      return;
    int outData1 = 0;
    if (DatabaseTools.UpdateData(orgData, "day", ref outData1))
      this.Day = outData1;
    int outData2 = 0;
    if (DatabaseTools.UpdateData(orgData, "deadline", ref outData2))
      this.Deadline = outData2;
    if (orgData.ContainsKey((object) "shop"))
      this._mysteryShopInfo = orgData[(object) "shop"] as Hashtable;
    if (orgData.ContainsKey((object) "daily"))
    {
      ArrayList arrayList = orgData[(object) "daily"] as ArrayList;
      this._dailyInfos.Clear();
      int day = 0;
      for (int count = arrayList.Count; day < count; ++day)
      {
        Hashtable inData = arrayList[day] as Hashtable;
        MerlinDailyInfo merlinDailyInfo = new MerlinDailyInfo();
        DatabaseTools.UpdateData(inData, "number", ref merlinDailyInfo.number);
        DatabaseTools.UpdateData(inData, "rewardable", ref merlinDailyInfo.rewardable);
        int groupId = ConfigManager.inst.DB_MerlinTrialsGroup.Day2Group(day);
        this._groupCompleteDict[groupId] = merlinDailyInfo.number == this.GetTotalBattleCount(groupId);
        this._dailyInfos.Add(merlinDailyInfo);
      }
    }
    if (this.onMerlinTrialsDataChanged == null)
      return;
    this.onMerlinTrialsDataChanged();
  }
}
