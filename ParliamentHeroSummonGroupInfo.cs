﻿// Decompiled with JetBrains decompiler
// Type: ParliamentHeroSummonGroupInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ParliamentHeroSummonGroupInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "gold_req")]
  public int reqGoldAmount;
  [Config(Name = "item_req")]
  public int reqItemId;
  [Config(Name = "item_req_value")]
  public int reqItemCount;
  [Config(Name = "free_summon_cd")]
  public float freeSummonCd;
  [Config(Name = "activity_hero")]
  public int activityHero;
  [Config(Name = "activity_end_time")]
  public string activityEndTime;
  [Config(Name = "ten_price_rate")]
  public int tenPriceRate;
  [Config(Name = "base_reward_id")]
  public int baseRewardId;
  [Config(Name = "base_reward_value")]
  public int baseRewardCount;
}
