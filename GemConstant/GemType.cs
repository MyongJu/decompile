﻿// Decompiled with JetBrains decompiler
// Type: GemConstant.GemType
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace GemConstant
{
  public enum GemType
  {
    EXP = 0,
    POLYGON_3 = 1,
    POLYGON_4 = 2,
    POLYGON_5 = 3,
    POLYGON_6 = 4,
    MULTICOLORED_11 = 11, // 0x0000000B
    MULTICOLORED_12 = 12, // 0x0000000C
    MULTICOLORED_13 = 13, // 0x0000000D
    MULTICOLORED_14 = 14, // 0x0000000E
    MULTICOLORED_15 = 15, // 0x0000000F
    MULTICOLORED_16 = 16, // 0x00000010
  }
}
