﻿// Decompiled with JetBrains decompiler
// Type: ConfigAnniversaryIapMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigAnniversaryIapMain
{
  private List<AnniversaryIapMainInfo> _anniversaryIapMainInfoList = new List<AnniversaryIapMainInfo>();
  private Dictionary<string, AnniversaryIapMainInfo> _datas;
  private Dictionary<int, AnniversaryIapMainInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<AnniversaryIapMainInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, AnniversaryIapMainInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._anniversaryIapMainInfoList.Add(enumerator.Current);
    }
  }

  public AnniversaryIapMainInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (AnniversaryIapMainInfo) null;
  }

  public AnniversaryIapMainInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (AnniversaryIapMainInfo) null;
  }

  public List<AnniversaryIapMainInfo> GetAnniversaryIapMainInfoList()
  {
    if (this._anniversaryIapMainInfoList != null)
      this._anniversaryIapMainInfoList.Sort((Comparison<AnniversaryIapMainInfo>) ((a, b) => a.type.CompareTo(b.type)));
    return this._anniversaryIapMainInfoList;
  }

  public void Clear()
  {
    if (this._datas != null)
      this._datas.Clear();
    if (this._dicByUniqueId != null)
      this._dicByUniqueId.Clear();
    if (this._anniversaryIapMainInfoList == null)
      return;
    this._anniversaryIapMainInfoList.Clear();
  }
}
