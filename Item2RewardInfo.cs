﻿// Decompiled with JetBrains decompiler
// Type: Item2RewardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Item2RewardInfo : MonoBehaviour
{
  public ItemIconRenderer icon;
  public UILabel enhance;

  public void SeedData(Item2RewardInfo.Data d)
  {
    if ((Object) null != (Object) this.enhance)
      this.enhance.text = "x" + (object) d.count;
    if ((double) d.count == 0.0)
      this.enhance.gameObject.SetActive(false);
    if (!((Object) null != (Object) this.icon))
      return;
    if (d.itemid > 0)
      this.icon.SetData(d.itemid, string.Empty, false);
    else
      this.icon.SetData(d.image);
  }

  public class Data
  {
    public int itemid;
    public float count;
    public string image;
  }
}
