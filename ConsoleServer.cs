﻿// Decompiled with JetBrains decompiler
// Type: ConsoleServer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;

public class ConsoleServer : MonoBehaviour
{
  private static HttpListener listener = new HttpListener();
  [SerializeField]
  public int Port = 55055;
  private static string filePath;
  private static List<ConsoleRouteAttribute> registeredRoutes;

  public virtual void Awake()
  {
    switch (Application.platform)
    {
      case RuntimePlatform.OSXEditor:
      case RuntimePlatform.WindowsPlayer:
      case RuntimePlatform.WindowsEditor:
        ConsoleServer.filePath = Application.dataPath + "/StreamingAssets/CUDLR/";
        break;
      case RuntimePlatform.OSXPlayer:
        ConsoleServer.filePath = Application.dataPath + "/Data/StreamingAssets/CUDLR/";
        break;
      case RuntimePlatform.IPhonePlayer:
        ConsoleServer.filePath = Application.dataPath + "/Raw/CUDLR/";
        break;
      case RuntimePlatform.Android:
        ConsoleServer.filePath = "jar:file://" + Application.dataPath + "!/assets/CUDLR/";
        break;
      default:
        Debug.Log((object) "Error starting CUDLR: Unsupported platform.");
        return;
    }
    this.RegisterRoutes();
    ConsoleServer.RegisterFileHandlers();
    Debug.Log((object) ("Starting Debug Console on port : " + (object) this.Port));
    ConsoleServer.listener.Prefixes.Add("http://*:" + (object) this.Port + "/");
    ConsoleServer.listener.Start();
    ConsoleServer.listener.BeginGetContext(new AsyncCallback(this.ListenerCallback), (object) null);
  }

  private void RegisterRoutes()
  {
    ConsoleServer.registeredRoutes = new List<ConsoleRouteAttribute>();
    foreach (System.Type type in Assembly.GetExecutingAssembly().GetTypes())
    {
      foreach (MethodInfo method in type.GetMethods(BindingFlags.Static | BindingFlags.Public))
      {
        ConsoleRouteAttribute[] customAttributes = method.GetCustomAttributes(typeof (ConsoleRouteAttribute), true) as ConsoleRouteAttribute[];
        if (customAttributes.Length != 0)
        {
          ConsoleRouteAttribute.Callback callback = (ConsoleRouteAttribute.Callback) Delegate.CreateDelegate(typeof (ConsoleRouteAttribute.Callback), method, false);
          if (callback == null)
          {
            ConsoleRouteAttribute.CallbackSimple cb = (ConsoleRouteAttribute.CallbackSimple) Delegate.CreateDelegate(typeof (ConsoleRouteAttribute.CallbackSimple), method, false);
            if (cb != null)
              callback = (ConsoleRouteAttribute.Callback) ((context, match) => cb(context));
          }
          if (callback == null)
          {
            Debug.LogError((object) string.Format("Method {0}.{1} takes the wrong arguments for a console route.", (object) type, (object) method.Name));
          }
          else
          {
            foreach (ConsoleRouteAttribute consoleRouteAttribute in customAttributes)
            {
              if (consoleRouteAttribute.m_route == null)
              {
                Debug.LogError((object) string.Format("Method {0}.{1} needs a valid route regexp.", (object) type, (object) method.Name));
              }
              else
              {
                consoleRouteAttribute.m_callback = callback;
                ConsoleServer.registeredRoutes.Add(consoleRouteAttribute);
              }
            }
          }
        }
      }
    }
  }

  private static bool FileHandler(HttpListenerContext context, Match match, bool download)
  {
    string path = ConsoleServer.filePath + match.Groups[1].Value;
    if (!File.Exists(path))
      return false;
    string type = "application/octet-stream";
    string lower = Path.GetExtension(path).ToLower();
    if (lower != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (ConsoleServer.\u003C\u003Ef__switch\u0024map6 == null)
      {
        // ISSUE: reference to a compiler-generated field
        ConsoleServer.\u003C\u003Ef__switch\u0024map6 = new Dictionary<string, int>(10)
        {
          {
            ".js",
            0
          },
          {
            ".json",
            1
          },
          {
            ".jpg",
            2
          },
          {
            ".jpeg",
            2
          },
          {
            ".gif",
            3
          },
          {
            ".png",
            4
          },
          {
            ".css",
            5
          },
          {
            ".htm",
            6
          },
          {
            ".html",
            6
          },
          {
            ".ico",
            7
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (ConsoleServer.\u003C\u003Ef__switch\u0024map6.TryGetValue(lower, out num))
      {
        switch (num)
        {
          case 0:
            type = "application/javascript";
            break;
          case 1:
            type = "application/json";
            break;
          case 2:
            type = "image/jpeg";
            break;
          case 3:
            type = "image/gif";
            break;
          case 4:
            type = "image/png";
            break;
          case 5:
            type = "text/css";
            break;
          case 6:
            type = "text/html";
            break;
          case 7:
            type = "image/x-icon";
            break;
        }
      }
    }
    context.Response.WriteFile(path, type, download);
    return true;
  }

  private static void RegisterFileHandlers()
  {
    ConsoleRouteAttribute consoleRouteAttribute1 = new ConsoleRouteAttribute("^/download/(.*\\.(js|json|jpg|jpeg|gif|png|css|htm|html|ico))$", "(GET|HEAD)");
    ConsoleRouteAttribute consoleRouteAttribute2 = new ConsoleRouteAttribute("^/(.*\\.(js|json|jpg|jpeg|gif|png|css|htm|html|ico))$", "(GET|HEAD)");
    consoleRouteAttribute1.m_callback = (ConsoleRouteAttribute.Callback) ((context, match) => ConsoleServer.FileHandler(context, match, true));
    consoleRouteAttribute2.m_callback = (ConsoleRouteAttribute.Callback) ((context, match) => ConsoleServer.FileHandler(context, match, false));
    ConsoleServer.registeredRoutes.Add(consoleRouteAttribute1);
    ConsoleServer.registeredRoutes.Add(consoleRouteAttribute2);
  }

  private void OnEnable()
  {
    Application.RegisterLogCallback(new Application.LogCallback(this.HandleLog));
  }

  private void OnDisable()
  {
    Application.RegisterLogCallback((Application.LogCallback) null);
  }

  private void Update()
  {
    Console.Update();
  }

  private void ListenerCallback(IAsyncResult result)
  {
    HttpListenerContext context = ConsoleServer.listener.EndGetContext(result);
    string input = context.Request.Url.AbsolutePath;
    if (input == "/")
      input = "/index.html";
    try
    {
      bool flag = false;
      using (List<ConsoleRouteAttribute>.Enumerator enumerator = ConsoleServer.registeredRoutes.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ConsoleRouteAttribute current = enumerator.Current;
          Match match = current.m_route.Match(input);
          if (match.Success && (current.m_methods == null || current.m_methods.IsMatch(context.Request.HttpMethod)) && current.m_callback(context, match))
          {
            flag = true;
            break;
          }
        }
      }
      if (!flag)
      {
        context.Response.StatusCode = 404;
        context.Response.StatusDescription = "Not Found";
      }
    }
    catch (Exception ex)
    {
      context.Response.StatusCode = 500;
      context.Response.StatusDescription = string.Format("Fatal error:\n{0}", (object) ex);
      Debug.LogException(ex);
    }
    context.Response.OutputStream.Close();
    ConsoleServer.listener.BeginGetContext(new AsyncCallback(this.ListenerCallback), (object) null);
  }

  private void HandleLog(string logString, string stackTrace, LogType type)
  {
    Console.Log(logString);
    if (type == LogType.Log)
      return;
    Console.Log(stackTrace);
  }
}
