﻿// Decompiled with JetBrains decompiler
// Type: FestivalVisitDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UI;
using UnityEngine;

public class FestivalVisitDlg : UI.Dialog
{
  public UILabel title;
  public UILabel desc;
  public UILabel labelX;
  public UILabel labelY;
  public UILabel leftCount;
  public UITexture monsterIcon;
  public RewardNormalItem[] rewardItems;
  public UITexture itemIcon;
  public UILabel itemCost;
  public UIButton attackBt;
  public UILabel normalTip;
  public PVEMonsterPreview previewPrefab;
  private Coordinate _targetLocation;
  private TileData _tileData;
  private FestivalVisitInfo visitInfo;
  private MonsterDisplayInfo monsterInfo;
  private GameObject monster;
  private PVEMonsterPreview preview;
  private bool _open;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    FestivalVisitDlg.Parameter parameter = orgParam as FestivalVisitDlg.Parameter;
    if (orgParam != null)
    {
      this._targetLocation = parameter.coordinate;
      this._tileData = PVPMapData.MapData.GetReferenceAt(this._targetLocation);
      this.visitInfo = ConfigManager.inst.DB_FestivalVisit.GetData(this._tileData.ResourceId);
      this.monsterInfo = ConfigManager.inst.DB_MonsterDisplay.GetDataByType(this.visitInfo.type);
      AudioManager.Instance.StopAndPlaySound("sfx_monster_click_event_white_knight");
      GameObject original = AssetManager.Instance.HandyLoad(ConfigWorldEncounter.GetMonsterPrefabPath(this.visitInfo.type), (System.Type) null) as GameObject;
      if ((UnityEngine.Object) original != (UnityEngine.Object) null)
      {
        GameObject go = UnityEngine.Object.Instantiate<GameObject>(original);
        go.transform.parent = this.transform;
        NGUITools.SetLayer(go, LayerMask.NameToLayer("UI3D"));
        if (this.monsterInfo != null)
        {
          go.transform.localPosition = this.monsterInfo.offsetValue;
          go.transform.localScale = this.monsterInfo.scalingValue;
          go.transform.localRotation = this.monsterInfo.rotationValue;
        }
        else
        {
          go.transform.localPosition = Vector3.zero;
          go.transform.localScale = Vector3.one;
          go.transform.localRotation = Quaternion.identity;
        }
      }
      this.itemCost.text = "1";
      this.attackBt.isEnabled = DBManager.inst.DB_Item.GetQuantity(this.visitInfo.itemCostId) >= 1;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.visitInfo.itemCostId);
      if (itemStaticInfo != null)
        BuilderFactory.Instance.Build((UIWidget) this.itemIcon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    }
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  private void OnSecond(int serverTime)
  {
    if (this._tileData == null || this._tileData.OwnerID <= 0L || this._tileData.Value > 0)
      return;
    this.OnCloseHandler();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void Clear()
  {
    for (int index = 0; index < this.rewardItems.Length; ++index)
      this.rewardItems[index].Clear();
    BuilderFactory.Instance.Release((UIWidget) this.itemIcon);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.monster);
    this.preview = (PVEMonsterPreview) null;
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  private void UpdateUI()
  {
    if (this._tileData == null)
      return;
    this.labelX.text = this._targetLocation.X.ToString();
    this.labelY.text = this._targetLocation.Y.ToString();
    if (this.visitInfo == null)
      return;
    this._open = this._tileData.Value > 0;
    this.title.text = this.visitInfo.LOC_Name;
    this.desc.text = this.visitInfo.LOC_Description;
    this.leftCount.text = this._tileData.Value.ToString();
    this.rewardItems[0].SetData(RewardNormalItem.RewardType.item, ScriptLocalization.Get(this.visitInfo.reward_display_name_1, true), Utils.GetUITextPath() + this.visitInfo.reward_display_image_1);
    this.rewardItems[1].SetData(RewardNormalItem.RewardType.item, ScriptLocalization.Get(this.visitInfo.reward_display_name_2, true), Utils.GetUITextPath() + this.visitInfo.reward_display_image_2);
    this.rewardItems[2].SetData(RewardNormalItem.RewardType.item, ScriptLocalization.Get(this.visitInfo.reward_display_name_3, true), Utils.GetUITextPath() + this.visitInfo.reward_display_image_3);
    this.rewardItems[3].SetData(RewardNormalItem.RewardType.item, ScriptLocalization.Get(this.visitInfo.reward_display_name_4, true), Utils.GetUITextPath() + this.visitInfo.reward_display_image_4);
    this.rewardItems[4].SetData(RewardNormalItem.RewardType.item, ScriptLocalization.Get(this.visitInfo.reward_display_name_5, true), Utils.GetUITextPath() + this.visitInfo.reward_display_image_5);
    DBManager.inst.DB_hero.Get((long) PlayerData.inst.cityId);
    if (DBManager.inst.DB_Item.GetQuantity(this.visitInfo.itemCostId) <= 0)
      this.itemCost.color = Color.red;
    else
      this.itemCost.color = Color.white;
  }

  public void AttackHandler()
  {
    if (DBManager.inst.DB_March.marchOwner.Count >= PlayerData.inst.playerCityData.marchSlot)
    {
      GameEngine.Instance.marchSystem.ShowMaxMarchSlotMessage();
    }
    else
    {
      GameEngine.Instance.marchSystem.StartVisit(this._targetLocation, (System.Action<bool, object>) ((ret, orgData) =>
      {
        if (ret)
          return;
        Hashtable hashtable = orgData as Hashtable;
        if (hashtable == null || !hashtable.ContainsKey((object) "errno"))
          return;
        int result = 0;
        int.TryParse(hashtable[(object) "errno"].ToString(), out result);
        if (result != 1200080)
          return;
        Utils.RefreshBlock(this._targetLocation, (System.Action<bool, object>) null);
      }));
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.Clear();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public Coordinate coordinate;
  }
}
