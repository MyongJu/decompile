﻿// Decompiled with JetBrains decompiler
// Type: OpeningSkip
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class OpeningSkip : MonoBehaviour
{
  public void OnClick()
  {
    PlayerPrefsEx.SetInt("openingcinematic", 1);
    if (UIManager.inst.ui2DCamera.transform.childCount > 0)
    {
      Transform child = UIManager.inst.ui2DCamera.transform.GetChild(0);
      child.parent = (Transform) null;
      Object.Destroy((Object) child.gameObject);
    }
    OpeningCinematicManager.Instance.Terminate();
    Object.Destroy((Object) this.gameObject);
  }
}
