﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerGatherReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;

public class MerlinTowerGatherReportPopup : BaseReportPopup
{
  private WarMessage message;
  public UILabel tileNameLabel;
  public UILabel itemName;
  public UILabel itemCount;
  public UITexture itemTexture;
  public TableContainer tableContainer;

  private void Init()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/gather_report_header", (System.Action<bool>) null, true, false, string.Empty);
    this.ConfigInfo();
    this.ConfigReward();
    this.tableContainer.ResetPosition();
  }

  private void ConfigInfo()
  {
    if (this.param.hashtable.ContainsKey((object) "mine_level"))
    {
      int result = 0;
      int.TryParse(this.param.hashtable[(object) "mine_level"].ToString(), out result);
      this.tileNameLabel.text = ScriptLocalization.GetWithPara("tower_mine_level_name", new Dictionary<string, string>()
      {
        {
          "0",
          result.ToString()
        }
      }, true);
    }
    else
      this.tileNameLabel.text = string.Empty;
  }

  private void ConfigReward()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_magic_tower_coin_1");
    if (itemStaticInfo != null)
    {
      this.itemName.text = itemStaticInfo.LocName;
      BuilderFactory.Instance.HandyBuild((UIWidget) this.itemTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, true, string.Empty);
    }
    if (!this.param.hashtable.ContainsKey((object) "crystal_number"))
      return;
    int result = 0;
    int.TryParse(this.param.hashtable[(object) "crystal_number"].ToString(), out result);
    this.itemCount.text = string.Format("+{0}", (object) result);
  }

  public void OnLocationClicked()
  {
    WarMessage mail = this.param.mail as WarMessage;
    this.GoToTargetPlace(mail.k, mail.x, mail.y);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }
}
