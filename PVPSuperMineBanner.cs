﻿// Decompiled with JetBrains decompiler
// Type: PVPSuperMineBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class PVPSuperMineBanner : SpriteBanner
{
  public TextMesh m_Status;

  public override void UpdateUI(TileData tile)
  {
    base.UpdateUI(tile);
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(tile.AllianceID);
    AllianceSuperMineData superMineData = tile.SuperMineData;
    string empty = string.Empty;
    string str1 = string.Empty;
    if (allianceData != null)
      empty += string.Format("({0})", (object) allianceData.allianceAcronym);
    if (superMineData != null)
    {
      AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(superMineData.ConfigId);
      string str2 = string.Empty;
      switch (buildingStaticInfo.type)
      {
        case 3:
          str2 = Utils.XLAT("alliance_resource_building_food_title");
          break;
        case 4:
          str2 = Utils.XLAT("alliance_resource_building_wood_title");
          break;
        case 5:
          str2 = Utils.XLAT("alliance_resource_building_ore_title");
          break;
        case 6:
          str2 = Utils.XLAT("alliance_resource_building_silver_title");
          break;
      }
      empty += string.IsNullOrEmpty(superMineData.Name) ? str2 : superMineData.Name;
      str1 = string.Format("({0})", (object) PVPSuperMineBanner.GetStateString(superMineData.CurrentState));
    }
    this.Name = empty;
    this.m_Status.text = str1;
  }

  public static string GetStateString(AllianceSuperMineData.State state)
  {
    switch (state)
    {
      case AllianceSuperMineData.State.UNCOMPLETE:
        return Utils.XLAT("alliance_fort_status_unfinished");
      case AllianceSuperMineData.State.BUILDING:
        return Utils.XLAT("alliance_fort_status_under_construction");
      case AllianceSuperMineData.State.COMPLETE:
        return Utils.XLAT("alliance_resource_building_status_gathering");
      case AllianceSuperMineData.State.FREEZE:
        return Utils.XLAT("alliance_storehouse_status_frozen");
      default:
        return string.Empty;
    }
  }
}
