﻿// Decompiled with JetBrains decompiler
// Type: ProgressBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class ProgressBar : MonoBehaviour
{
  [SerializeField]
  private ProgressBar.Panel panel;
  private float _progress;

  public float progress
  {
    get
    {
      return this._progress;
    }
    set
    {
      this._progress = value;
      this.panel.progressBar.transform.localScale = new Vector3(Mathf.Clamp01(this._progress), 1f, 1f);
    }
  }

  [Serializable]
  protected class Panel
  {
    public GameObject progressBar;
  }
}
