﻿// Decompiled with JetBrains decompiler
// Type: TalentEnhanceDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TalentEnhanceDlg : Popup
{
  public float gapX = 100f;
  private List<GameObject> pool = new List<GameObject>();
  private const int processMaxLength = 1265;
  public GameObject lockpart;
  public GameObject lvluppart;
  public GameObject fulllvlpart;
  public UILabel title;
  public UILabel des;
  public TalentSlot ts;
  public GameObject tspos;
  public UILabel curLevel;
  public UILabel nextLevel;
  public UILabel curBenefit;
  public UILabel incBenefit;
  public UISprite curBene;
  public UISprite incBene;
  public GameObject addPointBtn;
  public GameObject addMaxPointBtn;
  public UILabel fullLevel;
  public UILabel fullBenefit;
  public GameObject tspivot;
  private TalentEnhanceDlg.DLGType curType;
  private int curTalentInternalId;
  private string nextlevelinternalid;
  private int curtalentTreeId;
  public System.Action<int, int> onTalentLvlUpFull;

  public void OnCloseBtnPressed()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  private void Init()
  {
    ConfigPlayerTalent dbPlayerTalent = ConfigManager.inst.DB_PlayerTalent;
    bool flag = true;
    Requirements requirementsByTalentTreeId = dbPlayerTalent.GetRequirementsByTalentTreeId(this.curtalentTreeId);
    if (requirementsByTalentTreeId.requires.Count == 0)
      flag = false;
    using (Dictionary<string, int>.Enumerator enumerator = requirementsByTalentTreeId.requires.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, int> current = enumerator.Current;
        if (DBManager.inst.DB_Talent.IsTalentOwned(dbPlayerTalent.GetPlayerTalentInfo(int.Parse(current.Key)).internalId))
          flag = false;
      }
    }
    PlayerTalentInfo talentByTalentTreeId1 = dbPlayerTalent.GetCurrentLevelTalentByTalentTreeId(this.curtalentTreeId);
    PlayerTalentInfo talentByTalentTreeId2 = dbPlayerTalent.GetMaxLevelTalentByTalentTreeId(this.curtalentTreeId);
    PlayerTalentInfo playerTalentInfo1 = talentByTalentTreeId1;
    if (talentByTalentTreeId1 == null)
      playerTalentInfo1 = talentByTalentTreeId2;
    this.curTalentInternalId = playerTalentInfo1.internalId;
    this.curType = !flag ? (talentByTalentTreeId1 == null || talentByTalentTreeId1.level != talentByTalentTreeId2.level ? TalentEnhanceDlg.DLGType.LVLUP : TalentEnhanceDlg.DLGType.FULLLVL) : TalentEnhanceDlg.DLGType.LOCK;
    this.title.text = talentByTalentTreeId2.Title_name;
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("active_time", talentByTalentTreeId2.ActiveTimeInMinutes.ToString());
    para.Add("active_value", talentByTalentTreeId2.activeValue);
    para.Add("cd_time", talentByTalentTreeId2.CdTimeInHour.ToString());
    if (talentByTalentTreeId2.benefit != null && talentByTalentTreeId2.benefit.benefits != null && talentByTalentTreeId2.benefit.benefits.Count > 0)
    {
      Dictionary<string, float>.ValueCollection.Enumerator enumerator = talentByTalentTreeId2.benefit.benefits.Values.GetEnumerator();
      if (enumerator.MoveNext())
        para.Add("benefit_value_1", string.Format("{0}%", (object) (float) ((double) (int) ((double) Mathf.Abs(enumerator.Current) * 1000.0) / 10.0)));
    }
    this.des.text = ScriptLocalization.GetWithPara(talentByTalentTreeId2.description, para, true);
    GameObject gameObject1 = Utils.DuplicateGOB(this.ts.gameObject, this.transform);
    TalentSlot component1 = gameObject1.GetComponent<TalentSlot>();
    gameObject1.transform.localPosition = this.tspos.transform.localPosition;
    component1.talentTreeId = this.curtalentTreeId;
    component1.ToggleBoxcollider(false);
    component1.hideTitle = true;
    this.pool.Add(gameObject1);
    this.lockpart.SetActive(false);
    this.lvluppart.SetActive(false);
    this.fulllvlpart.SetActive(false);
    if (this.curType == TalentEnhanceDlg.DLGType.LOCK)
    {
      this.lockpart.SetActive(true);
      Dictionary<string, int> requires = requirementsByTalentTreeId.requires;
      List<Vector3> tarpos = new List<Vector3>();
      Utils.ArrangePosition(this.tspivot.transform.localPosition, requires.Count, this.gapX, 0.0f, ref tarpos);
      int index = 0;
      using (Dictionary<string, int>.Enumerator enumerator = requires.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          GameObject gameObject2 = Utils.DuplicateGOB(this.ts.gameObject, this.tspivot.transform.parent);
          gameObject2.transform.localPosition = tarpos[index];
          PlayerTalentInfo playerTalentInfo2 = dbPlayerTalent.GetPlayerTalentInfo(int.Parse(current.Key));
          TalentSlot component2 = gameObject2.GetComponent<TalentSlot>();
          component2.talentTreeId = playerTalentInfo2.talentTreeInternalId;
          component2.ToggleBoxcollider(false);
          component2.hideTitle = true;
          component2.panel.talentLevel.text = ScriptLocalization.Get("id_lv", true) + (object) playerTalentInfo2.level;
          this.pool.Add(gameObject2);
          ++index;
        }
      }
    }
    else if (this.curType == TalentEnhanceDlg.DLGType.LVLUP)
    {
      this.lvluppart.SetActive(true);
      string id1 = playerTalentInfo1.ID;
      int num = 0;
      if (talentByTalentTreeId1 != null)
        num = dbPlayerTalent.GetPlayerTalentInfo(id1).level;
      this.curLevel.text = ScriptLocalization.Get("id_lv", true) + (object) num;
      this.nextLevel.text = ScriptLocalization.Get("id_lv", true) + (num + 1).ToString();
      string s = "0";
      this.nextlevelinternalid = ConfigManager.inst.DB_TalentTree.GetTalentTreeInfo(playerTalentInfo1.talentTreeInternalId).ID + "_" + (num + 1).ToString();
      PropertyDefinition.FormatType benefitTypeByStringId;
      if (talentByTalentTreeId1 != null)
      {
        string id2 = ConfigManager.inst.DB_TalentTree.GetTalentTreeInfo(talentByTalentTreeId1.talentTreeInternalId).ID;
        s = dbPlayerTalent.GetBenefitByStringId(talentByTalentTreeId1.ID);
        benefitTypeByStringId = dbPlayerTalent.GetBenefitTypeByStringId(talentByTalentTreeId1.ID);
      }
      else
        benefitTypeByStringId = dbPlayerTalent.GetBenefitTypeByStringId(this.nextlevelinternalid);
      switch (benefitTypeByStringId)
      {
        case PropertyDefinition.FormatType.Integer:
          this.curBenefit.text = s;
          break;
        case PropertyDefinition.FormatType.Percent:
          this.curBenefit.text = ((double) float.Parse(s) * 100.0).ToString() + "%";
          break;
      }
      string benefitByStringId1 = dbPlayerTalent.GetIncreaseBenefitByStringId(this.nextlevelinternalid);
      if (benefitTypeByStringId == PropertyDefinition.FormatType.Percent)
        this.incBenefit.text = "+" + (object) (float) ((double) float.Parse(benefitByStringId1) * 100.0) + "%";
      else if (benefitTypeByStringId == PropertyDefinition.FormatType.Integer)
        this.incBenefit.text = "+" + benefitByStringId1;
      string benefitByStringId2 = dbPlayerTalent.GetBenefitByStringId(talentByTalentTreeId2.ID);
      this.curBene.width = (int) ((double) float.Parse(s) / (double) float.Parse(benefitByStringId2) * 1265.0);
      this.incBene.width = (int) (((double) float.Parse(benefitByStringId1) + (double) float.Parse(s)) / (double) float.Parse(benefitByStringId2) * 1265.0);
      if (DBManager.inst.DB_hero.Get(PlayerData.inst.uid).skill_points < 1)
      {
        this.addPointBtn.GetComponent<UIButton>().isEnabled = false;
        this.addMaxPointBtn.GetComponent<UIButton>().isEnabled = false;
      }
      else
      {
        this.addPointBtn.GetComponent<UIButton>().isEnabled = true;
        this.addMaxPointBtn.GetComponent<UIButton>().isEnabled = true;
      }
      if (talentByTalentTreeId2.type == 0)
      {
        this.curBenefit.gameObject.SetActive(true);
        this.incBenefit.gameObject.SetActive(true);
      }
      else
      {
        this.curBenefit.gameObject.SetActive(false);
        this.incBenefit.gameObject.SetActive(false);
      }
    }
    else
    {
      if (this.curType != TalentEnhanceDlg.DLGType.FULLLVL)
        return;
      this.fulllvlpart.SetActive(true);
      this.fullLevel.text = ScriptLocalization.Get("id_lv", true) + (object) talentByTalentTreeId2.level;
      string benefitByStringId = dbPlayerTalent.GetBenefitByStringId(talentByTalentTreeId2.ID);
      switch (dbPlayerTalent.GetBenefitTypeByStringId(talentByTalentTreeId1.ID))
      {
        case PropertyDefinition.FormatType.Integer:
          this.fullBenefit.text = benefitByStringId;
          break;
        case PropertyDefinition.FormatType.Percent:
          this.fullBenefit.text = ((double) float.Parse(benefitByStringId) * 100.0).ToString() + "%";
          break;
      }
      if (talentByTalentTreeId2.type == 0)
        this.fullBenefit.gameObject.SetActive(true);
      else
        this.fullBenefit.gameObject.SetActive(false);
    }
  }

  public void OnSpendPoint()
  {
    List<int> intList = new List<int>();
    PlayerTalentInfo playerTalentInfo = ConfigManager.inst.DB_PlayerTalent.GetPlayerTalentInfo(this.nextlevelinternalid);
    intList.Add(playerTalentInfo.internalId);
    MessageHub.inst.GetPortByAction("Hero:activeSkill").SendRequest(new Hashtable()
    {
      {
        (object) "skill_list",
        (object) intList.ToArray()
      }
    }, new System.Action<bool, object>(this.ActiveSkillCallBack), true);
    this.addPointBtn.GetComponent<UIButton>().isEnabled = false;
    this.addMaxPointBtn.GetComponent<UIButton>().isEnabled = false;
    PlayerTalentInfo talentByTalentTreeId = ConfigManager.inst.DB_PlayerTalent.GetMaxLevelTalentByTalentTreeId(this.curtalentTreeId);
    if (playerTalentInfo == null || playerTalentInfo.level != talentByTalentTreeId.level || this.onTalentLvlUpFull == null)
      return;
    TalentTreeInfo talentTreeInfo = ConfigManager.inst.DB_TalentTree.GetTalentTreeInfo(talentByTalentTreeId.talentTreeInternalId);
    this.onTalentLvlUpFull(talentByTalentTreeId.internalId, talentTreeInfo.tree);
  }

  public void OnMaxSpendPoint()
  {
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(PlayerData.inst.uid);
    if (heroData == null)
      return;
    int skillPoints = heroData.skill_points;
    ConfigPlayerTalent dbPlayerTalent = ConfigManager.inst.DB_PlayerTalent;
    PlayerTalentInfo talentByTalentTreeId1 = dbPlayerTalent.GetCurrentLevelTalentByTalentTreeId(this.curtalentTreeId);
    PlayerTalentInfo talentByTalentTreeId2 = dbPlayerTalent.GetMaxLevelTalentByTalentTreeId(this.curtalentTreeId);
    int num = 0;
    if (talentByTalentTreeId1 != null)
      num = dbPlayerTalent.GetPlayerTalentInfo(talentByTalentTreeId1.ID).level;
    int level = talentByTalentTreeId2.level;
    List<int> intList = new List<int>();
    PlayerTalentInfo playerTalentInfo = talentByTalentTreeId1;
    for (; skillPoints > 0 && num != level; ++num)
    {
      this.nextlevelinternalid = ConfigManager.inst.DB_TalentTree.GetTalentTreeInfo(talentByTalentTreeId2.talentTreeInternalId).ID + "_" + (num + 1).ToString();
      playerTalentInfo = ConfigManager.inst.DB_PlayerTalent.GetPlayerTalentInfo(this.nextlevelinternalid);
      intList.Add(playerTalentInfo.internalId);
      --skillPoints;
    }
    MessageHub.inst.GetPortByAction("Hero:activeSkill").SendRequest(new Hashtable()
    {
      {
        (object) "skill_list",
        (object) intList.ToArray()
      }
    }, new System.Action<bool, object>(this.ActiveSkillCallBack), true);
    this.addPointBtn.GetComponent<UIButton>().isEnabled = false;
    this.addMaxPointBtn.GetComponent<UIButton>().isEnabled = false;
    if (playerTalentInfo == null || playerTalentInfo.level != talentByTalentTreeId2.level || this.onTalentLvlUpFull == null)
      return;
    TalentTreeInfo talentTreeInfo = ConfigManager.inst.DB_TalentTree.GetTalentTreeInfo(talentByTalentTreeId2.talentTreeInternalId);
    this.onTalentLvlUpFull(talentByTalentTreeId2.internalId, talentTreeInfo.tree);
  }

  private void ActiveSkillCallBack(bool arg1, object arg2)
  {
    if (!arg1)
      return;
    ConfigPlayerTalent dbPlayerTalent = ConfigManager.inst.DB_PlayerTalent;
    PlayerTalentInfo talentByTalentTreeId1 = dbPlayerTalent.GetCurrentLevelTalentByTalentTreeId(this.curtalentTreeId);
    PlayerTalentInfo talentByTalentTreeId2 = dbPlayerTalent.GetMaxLevelTalentByTalentTreeId(this.curtalentTreeId);
    if (talentByTalentTreeId1 != null && talentByTalentTreeId1.level == talentByTalentTreeId2.level)
    {
      UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
    }
    else
    {
      this.Reset();
      this.Init();
    }
  }

  private void Reset()
  {
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.pool.Clear();
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.curtalentTreeId = (orgParam as TalentEnhanceDlg.Parameter).talentTreeId;
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  public enum DLGType
  {
    LOCK,
    LVLUP,
    FULLLVL,
  }

  public class Parameter : Popup.PopupParameter
  {
    public int talentTreeId;
  }
}
