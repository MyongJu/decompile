﻿// Decompiled with JetBrains decompiler
// Type: Puppet2D_SplineControl
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Puppet2D_SplineControl : MonoBehaviour
{
  public List<Transform> _splineCTRLS = new List<Transform>();
  public int numberBones = 4;
  private List<Vector3> outCoordinates = new List<Vector3>();
  public List<GameObject> bones = new List<GameObject>();

  private string GetUniqueName(string name)
  {
    string str = name;
    int startIndex = str.Length + 1;
    int num1 = 0;
    foreach (GameObject gameObject in Object.FindObjectsOfType(typeof (GameObject)))
    {
      if (gameObject.name.StartsWith(str))
      {
        string s = gameObject.name.Substring(startIndex, gameObject.name.Length - startIndex);
        int result = 0;
        if (int.TryParse(s, out result) && int.Parse(s) > num1)
          num1 = int.Parse(s);
      }
    }
    int num2 = num1 + 1;
    return name + "_" + (object) num2;
  }

  public void Run()
  {
    Quaternion quaternion = Quaternion.Euler(new Vector3(0.0f, this.transform.eulerAngles.y, 0.0f));
    Puppet2D_SplineControl.CatmullRom(this._splineCTRLS, out this.outCoordinates, this.numberBones);
    for (int index = 0; index < this.outCoordinates.Count; ++index)
    {
      Vector3 outCoordinate = this.outCoordinates[index];
      this.bones[index].transform.position = outCoordinate;
      this.bones[index].transform.rotation = index >= this.outCoordinates.Count - 1 ? this._splineCTRLS[this._splineCTRLS.Count - 2].transform.rotation : (index != 0 || this.outCoordinates.Count <= 2 ? Quaternion.LookRotation(this.outCoordinates[index] - this.outCoordinates[index + 1], Vector3.forward) * Quaternion.AngleAxis(90f, Vector3.left) * quaternion : this._splineCTRLS[1].transform.rotation);
    }
  }

  public static bool CatmullRom(List<Transform> inCoordinates, out List<Vector3> outCoordinates, int samples)
  {
    if (inCoordinates.Count < 4)
    {
      outCoordinates = (List<Vector3>) null;
      return false;
    }
    List<Vector3> vector3List = new List<Vector3>();
    for (int index1 = 1; index1 < inCoordinates.Count - 2; ++index1)
    {
      for (int index2 = 0; index2 < samples; ++index2)
        vector3List.Add(Puppet2D_SplineControl.PointOnCurve(inCoordinates[index1 - 1].position, inCoordinates[index1].position, inCoordinates[index1 + 1].position, inCoordinates[index1 + 2].position, 1f / (float) samples * (float) index2));
    }
    vector3List.Add(inCoordinates[inCoordinates.Count - 2].position);
    outCoordinates = vector3List;
    return true;
  }

  public static Vector3 PointOnCurve(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
  {
    Vector3 vector3 = new Vector3();
    float num1 = (float) (((-(double) t + 2.0) * (double) t - 1.0) * (double) t * 0.5);
    float num2 = (float) (((3.0 * (double) t - 5.0) * (double) t * (double) t + 2.0) * 0.5);
    float num3 = (float) (((-3.0 * (double) t + 4.0) * (double) t + 1.0) * (double) t * 0.5);
    float num4 = (float) (((double) t - 1.0) * (double) t * (double) t * 0.5);
    vector3.x = (float) ((double) p0.x * (double) num1 + (double) p1.x * (double) num2 + (double) p2.x * (double) num3 + (double) p3.x * (double) num4);
    vector3.y = (float) ((double) p0.y * (double) num1 + (double) p1.y * (double) num2 + (double) p2.y * (double) num3 + (double) p3.y * (double) num4);
    vector3.z = (float) ((double) p0.z * (double) num1 + (double) p1.z * (double) num2 + (double) p2.z * (double) num3 + (double) p3.z * (double) num4);
    return vector3;
  }
}
