﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillEnhanceInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DragonSkillEnhanceInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "target_enhance_level")]
  public int TargetEnhanceLevel;
  [Config(Name = "item_id")]
  public int ItemId;
  [Config(Name = "item_value")]
  public int ItemCount;
  [Config(Name = "enhance_effect")]
  public float EnhanceEffect;
}
