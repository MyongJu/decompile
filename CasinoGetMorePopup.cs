﻿// Decompiled with JetBrains decompiler
// Type: CasinoGetMorePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class CasinoGetMorePopup : Popup
{
  public UILabel nextFreeTime;
  public UILabel panelTitle;
  public UILabel panelDetailes;
  public UITexture iconTexture;
  private string type;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    CasinoGetMorePopup.Parameter parameter = orgParam as CasinoGetMorePopup.Parameter;
    if (parameter != null)
      this.type = parameter.type;
    if (this.type.Contains("casino1"))
    {
      this.nextFreeTime.gameObject.SetActive(true);
      this.AddEventHandler();
    }
    this.ExecuteInSecond(0);
    this.ShowPanelDetails();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    if (!this.type.Contains("casino1"))
      return;
    this.RemoveEventHandler();
  }

  public void ShowPanelDetails()
  {
    string str = "Texture/ItemIcons/";
    string path;
    if (this.type.Contains("casino1"))
    {
      this.panelTitle.text = Utils.XLAT("tavern_wheel_not_enough_title");
      this.panelDetailes.text = Utils.XLAT("tavern_wheel_not_enough_description");
      path = str + "item_casino_knucklebone_tiny";
    }
    else
    {
      this.panelTitle.text = Utils.XLAT("tavern_cards_not_enough_title");
      this.panelDetailes.text = Utils.XLAT("tavern_cards_not_enough_description");
      path = str + "item_casino_rune_stone_tiny";
    }
    BuilderFactory.Instance.HandyBuild((UIWidget) this.iconTexture, path, (System.Action<bool>) null, true, false, string.Empty);
  }

  public void OnGetMoreBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    UIManager.inst.OpenDlg("Casino/CasinoStoreDlg", (UI.Dialog.DialogParameter) new CasinoStoreDlg.Parameter()
    {
      displayType = (!this.type.Contains("casino1") ? 1 : 0)
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.ExecuteInSecond);
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.ExecuteInSecond);
  }

  private void ExecuteInSecond(int time = 0)
  {
    this.nextFreeTime.text = string.Format(Utils.XLAT("tavern_wheel_next_free_spin"), (object) CasinoFunHelper.Instance.FreeSpinLeftTime);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string type;
  }
}
