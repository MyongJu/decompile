﻿// Decompiled with JetBrains decompiler
// Type: ConfigLegend
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigLegend
{
  private ConfigParse parse = new ConfigParse();
  private List<LegendInfo> legends = new List<LegendInfo>();
  private Dictionary<string, LegendInfo> datas;
  private Dictionary<int, LegendInfo> dicByUniqueId;
  private bool isinited;

  public List<LegendInfo> Legends
  {
    get
    {
      if (!this.isinited)
      {
        this.isinited = true;
        this.Init();
      }
      return this.legends;
    }
  }

  private void Init()
  {
    this.legends.Sort((Comparison<LegendInfo>) ((l1, l2) =>
    {
      int num1 = l1.require.RequireBuildings()[0].value;
      int num2 = l2.require.RequireBuildings()[0].value;
      if (num1 == num2)
      {
        if (l1.priority > l2.priority)
          return 1;
        return l1.priority < l2.priority ? -1 : 0;
      }
      return num1 > num2 ? 1 : -1;
    }));
  }

  public void BuildDB(object res)
  {
    this.parse.Parse<LegendInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    using (Dictionary<string, LegendInfo>.Enumerator enumerator = this.datas.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.legends.Add(enumerator.Current.Value);
    }
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, LegendInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, LegendInfo>) null;
  }

  public LegendInfo GetLegendInfo(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (LegendInfo) null;
  }

  public LegendInfo GetLegendInfo(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (LegendInfo) null;
  }
}
