﻿// Decompiled with JetBrains decompiler
// Type: BaseRequirement
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class BaseRequirement
{
  private bool _isMatch;
  public long Target;
  public QuestInfo.RequirementType RequirementType;

  public bool IsMatch
  {
    get
    {
      return this._isMatch;
    }
  }

  public string RequirementID { get; set; }

  public string StatKey { get; set; }

  public void MatchRequirement()
  {
    this._isMatch = true;
  }
}
