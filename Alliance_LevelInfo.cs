﻿// Decompiled with JetBrains decompiler
// Type: Alliance_LevelInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class Alliance_LevelInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "level")]
  public int Alliance_Level;
  [Config(Name = "experience")]
  public int Experience;
  [Config(Name = "silver")]
  public int SilverRequired;
  [Config(Name = "min_donate")]
  public int MinDonate;
  [Config(CustomParse = true, CustomType = typeof (Benefits), Name = "Benefits")]
  public Benefits benefit;
  [Config(Name = "benefit_name_1")]
  public string benefit_name_1;
  [Config(Name = "benefit_name_2")]
  public string benefit_name_2;
  [Config(Name = "benefit_name_3")]
  public string benefit_name_3;
  [Config(Name = "benefit_name_4")]
  public string benefit_name_4;
  [Config(Name = "benefit_name_5")]
  public string benefit_name_5;
  [Config(Name = "benefit_des_1")]
  public string benefit_des_1;
  [Config(Name = "benefit_des_2")]
  public string benefit_des_2;
  [Config(Name = "benefit_des_3")]
  public string benefit_des_3;
  [Config(Name = "benefit_des_4")]
  public string benefit_des_4;
  [Config(Name = "benefit_des_5")]
  public string benefit_des_5;
  [Config(Name = "benefit_1_value_des")]
  public string benefit_value_des_1;
  [Config(Name = "benefit_2_value_des")]
  public string benefit_value_des_2;
  [Config(Name = "benefit_3_value_des")]
  public string benefit_value_des_3;
  [Config(Name = "benefit_4_value_des")]
  public string benefit_value_des_4;
  [Config(Name = "benefit_5_value_des")]
  public string benefit_value_des_5;
}
