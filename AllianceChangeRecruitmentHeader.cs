﻿// Decompiled with JetBrains decompiler
// Type: AllianceChangeRecruitmentHeader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class AllianceChangeRecruitmentHeader : AllianceCustomizeHeader
{
  [SerializeField]
  private GameObject m_PublicButton;
  [SerializeField]
  private GameObject m_PrivateButton;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    this.m_PublicButton.SetActive(!this.allianceData.isPirvate);
    this.m_PrivateButton.SetActive(this.allianceData.isPirvate);
  }

  public void OnPublicPressed()
  {
    this.m_PublicButton.SetActive(false);
    this.m_PrivateButton.SetActive(true);
    this.SendRequest();
  }

  public void OnPrivatePressed()
  {
    this.m_PublicButton.SetActive(true);
    this.m_PrivateButton.SetActive(false);
    this.SendRequest();
  }

  private bool IsPrivate
  {
    get
    {
      return this.m_PrivateButton.activeSelf;
    }
  }

  private void SendRequest()
  {
    AllianceManager.Instance.SetupAllianceInfo(this.allianceData.language, this.allianceData.publicMessage, this.IsPrivate, new System.Action<bool, object>(this.SetupAllianceCallback));
  }

  private void SetupAllianceCallback(bool ret, object data)
  {
    if (!ret)
      return;
    UIManager.inst.toast.Show(string.Format(Utils.XLAT("toast_alliance_open_recruitment_changed"), !this.IsPrivate ? (object) Utils.XLAT("alliance_settings_uppercase_open_button") : (object) Utils.XLAT("alliance_settings_uppercase_closed_button")), (System.Action) null, 4f, false);
  }
}
