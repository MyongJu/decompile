﻿// Decompiled with JetBrains decompiler
// Type: RoundPlayerMoral
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class RoundPlayerMoral : RoundPlayer
{
  public const string SCREEN_EFFECT_UNDER_ATTACK = "Prefab/DragonKnight/Objects/VFX/fx_moral_under_attack";
  public const string SCREEN_EFFECT_ENHANCE = "Prefab/DragonKnight/Objects/VFX/fx_moral_enhance";
  private IVfx _currentScreenVfx;

  public RoundPlayerMoral()
  {
    this.PlayerId = 1L;
  }

  public override void InitializeStateMachine()
  {
    this.StateMacine.AddState((IState) new GuardStateIdleMoral((RoundPlayer) this));
    this.StateMacine.AddState((IState) new GuardStateDeathMoral((RoundPlayer) this));
    this.StateMacine.AddState((IState) new GuardStateAttackMoral((RoundPlayer) this));
    this.StateMacine.AddState((IState) new GuardStateUnderAttackMoral((RoundPlayer) this));
    this.StateMacine.AddState((IState) new GuardStateSpellMoral((RoundPlayer) this));
    this.StateMacine.AddState((IState) new GuardStateEnhanceMoral((RoundPlayer) this));
  }

  public override RoundPlayer.Camp PlayerCamp
  {
    get
    {
      return RoundPlayer.Camp.Moral;
    }
  }

  public override void Shutdown()
  {
    this.StopVFX();
    this.StateMacine.Dispose();
  }

  public void PlayerVFX(string vfxPath)
  {
    this.StopVFX();
    this._currentScreenVfx = VfxManager.Instance.Create(vfxPath);
    this._currentScreenVfx.Play(DragonKnightSystem.Instance.Controller.BattleHud.transform);
  }

  public void StopVFX()
  {
    if (this._currentScreenVfx == null)
      return;
    this._currentScreenVfx.Stop();
    VfxManager.Instance.Delete(this._currentScreenVfx);
    this._currentScreenVfx = (IVfx) null;
  }
}
