﻿// Decompiled with JetBrains decompiler
// Type: ItemComposePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Text;
using UI;
using UnityEngine;

public class ItemComposePopup : Popup
{
  private long composeid;
  private ItemComposeInfo composeInfo;
  private HubPort mItemCompose;
  private bool gotoBuy;
  private int selectedNum;
  private int maxcancompose;
  public Icon tarItem;
  public UILabel tarNum;
  public StandardProgressBar spb;
  public GameObject itemsmore;
  public GameObject itemsless;
  public UIScrollView scrollview;
  public UIGrid scrollroot;
  public UIGrid notscrollroot;
  public Icon itemtemplate;
  public UIButton useBtn;
  public UILabel notEnoughTips;
  public UILabel buttonLabel;

  private void Init()
  {
    this.composeInfo = ConfigManager.inst.DB_ItemCompose.GetItemComposeInfo(this.composeid);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem((int) this.composeInfo.compose_result_item_id);
    this.gotoBuy = itemStaticInfo.gotoBuy > 0;
    this.tarItem.SetData(itemStaticInfo.ImagePath, itemStaticInfo.LocName, itemStaticInfo.LocDescription);
    this.maxcancompose = this.GetMaxCanCompose();
    this.tarNum.text = (this.maxcancompose * this.composeInfo.compose_result_item_value).ToString();
    this.ArrangeItems();
    this.spb.Init(0, this.maxcancompose);
    if (this.maxcancompose == 0 && this.gotoBuy)
    {
      this.useBtn.isEnabled = true;
      this.buttonLabel.text = Utils.XLAT("id_uppercase_get_more");
    }
    else
      this.buttonLabel.text = Utils.XLAT("event_synthesize_button");
    this.notEnoughTips.gameObject.SetActive(this.maxcancompose == 0);
  }

  private void ArrangeItems()
  {
    if (this.composeInfo.ComposeItems.Count > 3)
    {
      this.itemsmore.SetActive(true);
      this.itemsless.SetActive(false);
      this.GenerateItems(this.scrollroot.transform);
      this.scrollroot.Reposition();
      this.scrollview.ResetPosition();
    }
    else
    {
      this.itemsmore.SetActive(false);
      this.itemsless.SetActive(true);
      this.GenerateItems(this.notscrollroot.transform);
    }
    this.itemtemplate.gameObject.SetActive(false);
  }

  private void GenerateItems(Transform parent)
  {
    using (Dictionary<long, int>.Enumerator enumerator = this.composeInfo.ComposeItems.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<long, int> current = enumerator.Current;
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem((int) current.Key);
        int itemCount = ItemBag.Instance.GetItemCount((int) current.Key);
        StringBuilder stringBuilder = new StringBuilder(itemCount.ToString() + "/" + (object) current.Value);
        if (itemCount < current.Value)
        {
          stringBuilder.Insert(0, "[ff0000]");
          stringBuilder.Append("[-]");
        }
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.itemtemplate.gameObject);
        gameObject.transform.parent = parent;
        gameObject.transform.localScale = Vector3.one;
        gameObject.GetComponent<Icon>().SetData(itemStaticInfo.ImagePath, new string[1]
        {
          stringBuilder.ToString()
        });
      }
    }
  }

  private int GetMaxCanCompose()
  {
    int num1 = int.MaxValue;
    using (Dictionary<long, int>.Enumerator enumerator = this.composeInfo.ComposeItems.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<long, int> current = enumerator.Current;
        int num2 = ItemBag.Instance.GetItemCount((int) current.Key) / current.Value;
        if (num2 < num1)
          num1 = num2;
      }
    }
    return num1;
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  public void OnComposeBtnPressed()
  {
    if (this.maxcancompose == 0 && this.gotoBuy)
    {
      Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    }
    else
    {
      RewardsCollectionAnimator.Instance.Clear();
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem((int) this.composeInfo.compose_result_item_id);
      RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
      {
        count = (float) this.spb.CurrentCount,
        icon = itemStaticInfo.ImagePath
      });
      this.mItemCompose.SendRequest(new Hashtable()
      {
        {
          (object) "compose_id",
          (object) this.composeInfo.internalId
        },
        {
          (object) "amount",
          (object) this.spb.CurrentCount
        }
      }, (System.Action<bool, object>) ((ret, obj) =>
      {
        RewardsCollectionAnimator.Instance.CollectItems(false);
        UIManager.inst.ShowBasicToast("toast_event_synthesis_success");
        UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
      }), true);
    }
  }

  private void OnProcessBarChange(int value)
  {
    this.tarNum.text = (value * this.composeInfo.compose_result_item_value).ToString();
    if (value == 0)
    {
      if (this.maxcancompose == 0 && this.gotoBuy)
        this.useBtn.isEnabled = true;
      else
        this.useBtn.isEnabled = false;
    }
    else
      this.useBtn.isEnabled = true;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.composeid = (orgParam as ItemComposePopup.Parameter).itemcomposeid;
    this.mItemCompose = MessageHub.inst.GetPortByAction("Item:compose");
    this.spb.onValueChanged += new System.Action<int>(this.OnProcessBarChange);
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.spb.onValueChanged -= new System.Action<int>(this.OnProcessBarChange);
    base.OnHide(orgParam);
  }

  public class Parameter : Popup.PopupParameter
  {
    public long itemcomposeid;
  }
}
