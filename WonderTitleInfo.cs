﻿// Decompiled with JetBrains decompiler
// Type: WonderTitleInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class WonderTitleInfo
{
  public const int KING_ID = 1;
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "type")]
  public int titleType;
  [Config(Name = "sort")]
  public int sort;
  [Config(Name = "cd")]
  public int cdTime;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "icon")]
  public string icon;
  [Config(CustomParse = true, CustomType = typeof (Benefits), Name = "Benefits")]
  public Benefits benefits;
  private bool isNominated;
  private WonderTitleInfo.NominatedUserInfo nominatedUser;

  public List<Benefits.BenefitValuePair> GetTitleBenefits()
  {
    if (this.benefits != null)
      return this.benefits.GetBenefitsPairs();
    return (List<Benefits.BenefitValuePair>) null;
  }

  public string Name
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }

  public bool IsNominated
  {
    get
    {
      return this.isNominated;
    }
    set
    {
      this.isNominated = value;
    }
  }

  public WonderTitleInfo.NominatedUserInfo NominatedUser
  {
    get
    {
      return this.nominatedUser;
    }
    set
    {
      this.nominatedUser = value;
    }
  }

  public int TitleCoolDownLeftTime
  {
    get
    {
      if (this.nominatedUser != null)
        return this.nominatedUser.TitleCdTime - NetServerTime.inst.ServerTimestamp;
      return -1;
    }
  }

  public class NominatedUserInfo
  {
    private int titleCdTime;
    private long userId;
    private string userName;
    private string portrait;
    private string userImage;
    private int lordTitleId;
    private string allianceName;

    public int TitleCdTime
    {
      get
      {
        return this.titleCdTime;
      }
      set
      {
        this.titleCdTime = value;
      }
    }

    public long UserId
    {
      get
      {
        return this.userId;
      }
      set
      {
        this.userId = value;
      }
    }

    public string UserName
    {
      get
      {
        return this.userName;
      }
      set
      {
        this.userName = value;
      }
    }

    public string Portrait
    {
      get
      {
        return this.portrait;
      }
      set
      {
        this.portrait = value;
      }
    }

    public string UserImage
    {
      get
      {
        return this.userImage;
      }
      set
      {
        this.userImage = value;
      }
    }

    public int LordTitleId
    {
      get
      {
        return this.lordTitleId;
      }
      set
      {
        this.lordTitleId = value;
      }
    }

    public string AllianceName
    {
      get
      {
        return this.allianceName;
      }
      set
      {
        this.allianceName = value;
      }
    }
  }
}
