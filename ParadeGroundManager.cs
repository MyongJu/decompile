﻿// Decompiled with JetBrains decompiler
// Type: ParadeGroundManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class ParadeGroundManager : MonoBehaviour
{
  public static ParadeGroundManager.ParadeGroundTroopsInfo troopsParam = new ParadeGroundManager.ParadeGroundTroopsInfo();
  private const int MIN_SLOT_TROOP_NUM = 112;
  public const int PARADE_GROUND_SLOT_FULL_BASE_COUNT = 26;
  public const int PARADE_GROUND_SLOT_COUNT = 31;
  private const int PARADE_GROUND_SLOT_COL_COUNT = 8;
  private const int PARADE_GROUND_SLOT_HEIGHT = 100;
  private const int PARADE_GROUND_SLOT_WIDTH = 100;
  public ParadeGroundSlot[] slots;

  public void Awake()
  {
    this.Init();
  }

  public void OnEnable()
  {
    if (!GameEngine.IsAvailable || !GameEngine.IsReady() || GameEngine.IsShuttingDown)
      return;
    DBManager.inst.DB_City.onCityTroopChanged += new System.Action<long>(this.OnCityDataUpdate);
    DBManager.inst.DB_City.onDataRemoved += new System.Action(this.FreshCityTroops);
  }

  public void OnDisable()
  {
    if (!GameEngine.IsAvailable || !GameEngine.IsReady() || GameEngine.IsShuttingDown)
      return;
    DBManager.inst.DB_City.onCityTroopChanged -= new System.Action<long>(this.OnCityDataUpdate);
    DBManager.inst.DB_City.onDataRemoved -= new System.Action(this.FreshCityTroops);
  }

  public void Init()
  {
  }

  public void FreshCityTroops()
  {
    CityTroopsInfo cityTroops1 = PlayerData.inst.playerCityData.cityTroops;
    Dictionary<TroopType, int> cityTroops2 = CityTroopsAnimManager.inst.cityTroops;
    ParadeGroundManager.troopsParam.Clear();
    this.ClearParadeGround();
    using (Dictionary<string, int>.KeyCollection.Enumerator enumerator = cityTroops1.troops.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(current);
        if (data != null)
          this.AddMoreTroops(data.GetUnitType(), cityTroops1.troops[current]);
      }
    }
    Dictionary<TroopType, int>.KeyCollection.Enumerator enumerator1 = cityTroops2.Keys.GetEnumerator();
    while (enumerator1.MoveNext())
      this.AddMoreTroops(enumerator1.Current, -cityTroops2[enumerator1.Current]);
  }

  public void AddMoreTroops(TroopType troopType, int troopCount)
  {
    ParadeGroundManager.troopsParam[troopType] += troopCount;
    this.SetParadeGround(ParadeGroundManager.troopsParam);
  }

  public void ClearParadeGround()
  {
    for (int index = 0; index < 31; ++index)
      this.slots[index].Clear();
  }

  public void SetParadeGround(ParadeGroundManager.ParadeGroundTroopsInfo info)
  {
    ParadeGroundManager.troopsParam = info;
    TroopType[] troopTypeArray = new TroopType[31];
    int[] numArray = new int[31];
    int index1 = 0;
    bool flag = this.IsSlotFull(info);
    for (int index2 = 0; index2 < 5; ++index2)
    {
      int num1;
      int num2;
      if (flag)
      {
        num1 = Mathf.CeilToInt((float) (26.0 * ((double) info[index2] / (double) info.totalTroopCount)));
        num2 = info.totalTroopCount / 26;
      }
      else
      {
        num1 = Mathf.CeilToInt((float) info[index2] / 112f);
        num2 = 112;
      }
      if (num1 >= 1)
      {
        for (int index3 = 0; index1 < 31 && index3 < num1 - 1; ++index1)
        {
          troopTypeArray[index1] = (TroopType) index2;
          numArray[index1] = 112;
          this.slots[index1].SetSlot((TroopType) index2, num2, num2);
          ++index3;
        }
        if (num1 > 0 && index1 < 31)
        {
          troopTypeArray[index1] = (TroopType) index2;
          numArray[index1] = info[index2] % 112;
          this.slots[index1].SetSlot((TroopType) index2, info[index2] % num2 != 0 ? info[index2] % num2 : num2, num2);
          ++index1;
        }
      }
    }
    for (int index2 = index1; index2 < 31; ++index2)
      this.slots[index2].Clear();
  }

  private void OnCityDataUpdate(long cityId)
  {
    if (cityId != (long) PlayerData.inst.cityId)
      return;
    this.FreshCityTroops();
  }

  private bool IsSlotFull(ParadeGroundManager.ParadeGroundTroopsInfo info)
  {
    int num = 0;
    for (int index = 0; index < info.troopTypeCount; ++index)
      num += Mathf.CeilToInt((float) info[index] / 112f);
    return num > 31;
  }

  public class ParadeGroundTroopsInfo
  {
    private int[] _troopCount = new int[5];
    private int _totalTroopCount;

    public int totalTroopCount
    {
      get
      {
        return this._totalTroopCount;
      }
      private set
      {
        this._totalTroopCount = value;
      }
    }

    public int troopTypeCount
    {
      get
      {
        return this._troopCount.Length;
      }
    }

    public int this[TroopType type]
    {
      get
      {
        if (type >= (TroopType) this.troopTypeCount)
          return 0;
        return this._troopCount[(int) type];
      }
      set
      {
        if (type >= (TroopType) this.troopTypeCount || this._troopCount[(int) type] == value)
          return;
        this._troopCount[(int) type] = value;
        int num = 0;
        for (int index = 0; index < this.troopTypeCount; ++index)
          num += this._troopCount[index];
        this.totalTroopCount = num;
      }
    }

    public int this[int inIndex]
    {
      get
      {
        if (inIndex >= this.troopTypeCount)
          return 0;
        return this._troopCount[inIndex];
      }
      set
      {
        if (inIndex >= this.troopTypeCount)
          return;
        this._troopCount[inIndex] = value;
      }
    }

    public void Clear()
    {
      for (int index = 0; index < this._troopCount.Length; ++index)
        this._troopCount[index] = 0;
    }
  }
}
