﻿// Decompiled with JetBrains decompiler
// Type: WorldBossActivityUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class WorldBossActivityUIData : ActivityBaseUIData
{
  public override ActivityBaseUIData.ActivityType Type
  {
    get
    {
      return ActivityBaseUIData.ActivityType.WorldBoss;
    }
  }

  public override string EventKey
  {
    get
    {
      return "world_boss";
    }
  }

  private WorldBossActivityData WorldBossData
  {
    get
    {
      return this.Data as WorldBossActivityData;
    }
  }

  public override int StartTime
  {
    get
    {
      return this.WorldBossData.StartTime;
    }
  }

  public override int EndTime
  {
    get
    {
      return this.WorldBossData.EndTime;
    }
  }

  public override IconData GetNormalIconData()
  {
    IconData iconData = new IconData();
    iconData.image = "Texture/Events/icon_world_boss";
    iconData.contents = new string[2];
    iconData.contents[0] = Utils.XLAT("event_kingdom_giant_title");
    iconData.contents[1] = Utils.XLAT("event_kingdom_giant_one_line_description");
    iconData.Data = (object) this;
    return iconData;
  }

  public override IconData GetADIconData()
  {
    IconData iconData = new IconData();
    iconData.image = "Texture/Events/world_boss_image_big";
    iconData.contents = new string[2];
    iconData.contents[0] = Utils.XLAT("event_kingdom_giant_title");
    iconData.contents[1] = Utils.XLAT("event_kingdom_giant_one_line_description");
    return iconData;
  }

  public override void DisplayActivityDetail()
  {
    if (!ActivityManager.Intance.worldBoss.IsStart())
    {
      UIManager.inst.OpenDlg("KingdomBoss/WorldBossActivityDetailsDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    }
    else
    {
      WorldBossActivityPayload.Instance.OnRequestDataReceived += new System.Action(this.OnRequestedDataReceived);
      WorldBossActivityPayload.Instance.RequestData((System.Action<bool, object>) null);
    }
  }

  private void OnRequestedDataReceived()
  {
    if (WorldBossActivityPayload.Instance.BossInfoReady == 0)
      return;
    WorldBossActivityPayload.Instance.OnRequestDataReceived -= new System.Action(this.OnRequestedDataReceived);
    UIManager.inst.OpenDlg("KingdomBoss/WorldBossActivityDetailsDlg", (UI.Dialog.DialogParameter) new WorldBossActivityDetailsDlg.Parameter()
    {
      bossDBInfo = WorldBossActivityPayload.Instance.BossDBInfo
    }, true, true, true);
  }
}
