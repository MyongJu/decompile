﻿// Decompiled with JetBrains decompiler
// Type: VaultSlotItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class VaultSlotItemRenderer : MonoBehaviour
{
  public UILabel dayDepositTime;
  public UILabel interestCount;
  public UITexture dayDepositTexture;
  public UIProgressBar depositingProgressSlider;
  public UILabel depositingProgressValue;
  public UILabel dayDepositValue;
  public GameObject depositingEffect;
  public GameObject tapToUnlockRootNode;
  public GameObject tapToDepositRootNode;
  public GameObject depositingRootNode;
  public GameObject cantDepositRootNode;
  public GameObject withdrawRootNode;
  private TreasuryInfo treasuryInfo;
  private bool initialized;

  public void SetData(TreasuryInfo treasuryInfo)
  {
    this.treasuryInfo = treasuryInfo;
    this.AddEventHandler();
    this.UpdateUI();
    this.initialized = true;
  }

  private void UpdateUI()
  {
    if (this.treasuryInfo == null)
      return;
    NGUITools.SetActive(this.depositingEffect, false);
    NGUITools.SetActive(this.tapToUnlockRootNode, false);
    NGUITools.SetActive(this.tapToDepositRootNode, false);
    NGUITools.SetActive(this.depositingRootNode, false);
    NGUITools.SetActive(this.cantDepositRootNode, false);
    NGUITools.SetActive(this.withdrawRootNode, false);
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("1", this.treasuryInfo.duration.ToString());
    TreasuryData treasuryData = DBManager.inst.DB_Treasury.GetTreasuryData();
    this.dayDepositTime.text = (double) this.treasuryInfo.duration <= 1.0 ? ScriptLocalization.GetWithPara("vault_deposit_duration", para, true) : ScriptLocalization.GetWithPara("vault_deposit_duration_plural", para, true);
    this.interestCount.text = ((double) this.treasuryInfo.interests * 100.0).ToString() + "%";
    this.dayDepositValue.text = this.treasuryInfo.duration.ToString();
    if (!this.initialized)
      BuilderFactory.Instance.HandyBuild((UIWidget) this.dayDepositTexture, "Texture/Vault/icon_vault_duration", (System.Action<bool>) null, true, false, string.Empty);
    if (treasuryData == null)
      return;
    if (treasuryData.SlotId != 0)
    {
      if (treasuryData.SlotId == this.treasuryInfo.internalId)
      {
        int num1 = treasuryData.EndTime - treasuryData.StartTime;
        int time = treasuryData.EndTime - NetServerTime.inst.ServerTimestamp;
        int num2 = NetServerTime.inst.ServerTimestamp - treasuryData.StartTime;
        if (num1 > 0 && time > 0)
        {
          this.depositingProgressSlider.value = (float) num2 / (float) num1;
          this.depositingProgressValue.text = Utils.FormatTime(time, true, false, true);
          NGUITools.SetActive(this.depositingEffect, true);
          NGUITools.SetActive(this.depositingRootNode, true);
        }
        else
          NGUITools.SetActive(this.withdrawRootNode, true);
      }
      else if (!treasuryData.VaultLockInfo[this.treasuryInfo.internalId])
      {
        NGUITools.SetActive(this.cantDepositRootNode, true);
      }
      else
      {
        if (this.treasuryInfo.unlockType != 1)
          return;
        NGUITools.SetActive(this.tapToUnlockRootNode, true);
      }
    }
    else if (!treasuryData.VaultLockInfo[this.treasuryInfo.internalId])
      NGUITools.SetActive(this.tapToDepositRootNode, true);
    else
      NGUITools.SetActive(this.tapToUnlockRootNode, true);
  }

  public void OnVaultSlotPressed()
  {
    TreasuryData treasuryData = DBManager.inst.DB_Treasury.GetTreasuryData();
    if (treasuryData == null || this.treasuryInfo == null)
      return;
    if (treasuryData.VaultLockInfo[this.treasuryInfo.internalId])
      UIManager.inst.OpenPopup("Vault/VaultConfirmationPopup", (Popup.PopupParameter) new VaultConfirmationPopup.Parameter()
      {
        slotId = this.treasuryInfo.internalId,
        confirmType = VaultConfirmationPopup.ConfirmType.TO_UNLOCK
      });
    else
      UIManager.inst.OpenPopup("Vault/VaultDepositWithdrawPopup", (Popup.PopupParameter) new VaultDepositWithdrawPopup.Parameter()
      {
        slotId = this.treasuryInfo.internalId
      });
  }

  public void ClearData()
  {
    this.RemoveEventHandler();
    this.StopCoroutine(VaultFunHelper.Instance.BreathingEffect(this.depositingProgressSlider));
    this.initialized = false;
  }

  private void OnSecondEventHandler(int time)
  {
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEventHandler);
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEventHandler);
  }
}
