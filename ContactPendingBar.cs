﻿// Decompiled with JetBrains decompiler
// Type: ContactPendingBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UI;
using UnityEngine;

public class ContactPendingBar : MonoBehaviour
{
  private long _uid;
  [SerializeField]
  private ContactPendingBar.Panel panel;

  public void Setup(long uid)
  {
    this._uid = uid;
    UserData userData = DBManager.inst.DB_User.Get(uid);
    if (userData == null)
      return;
    CustomIconLoader.Instance.requestCustomIcon(this.panel.userIcon, UserData.GetPortraitIconPath(userData.portrait), userData.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.panel.userIcon, userData.LordTitle, 1);
    this.panel.userName.text = userData.userName_Kingdom_Alliance_Name;
  }

  public void OnIgnoreButtonClick()
  {
    ContactManager.DenyInvited(this._uid, (System.Action<bool, object>) null);
  }

  public void OnAcceptButtonClick()
  {
    ContactManager.AcceptInvited(this._uid, (System.Action<bool, object>) null);
  }

  public void OnPlayerInfoButtonClick()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
    {
      uid = this._uid
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  [Serializable]
  protected class Panel
  {
    public UITexture userIcon;
    public UILabel userName;
    public UIButton BT_ignore;
    public UIButton BT_accept;
  }
}
