﻿// Decompiled with JetBrains decompiler
// Type: SystemAnnouncementItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SystemAnnouncementItem : MonoBehaviour
{
  [HideInInspector]
  private float speed = 300f;
  private Vector3 locPos = Vector3.zero;
  public const float DEFAULT_SPEED = 300f;
  private long id;
  public UILabel LB_text;
  public System.Action<long> onMoveOut;
  public System.Action<long> onMoveIn;
  private int textLength;
  private bool isAllMoveIn;
  private bool isAllMoveOut;
  private int displayAllPos;
  private int moveOutPos;

  public string text
  {
    set
    {
      this.LB_text.text = value;
    }
  }

  public void AllMoveOut()
  {
    this.isAllMoveOut = true;
    if (this.onMoveOut == null)
      return;
    this.onMoveOut(this.id);
  }

  public void AllMoveIn()
  {
    this.isAllMoveIn = true;
    if (this.onMoveIn == null)
      return;
    this.onMoveIn(this.id);
  }

  public void Display(long inId, string message, int backgroundLength, float inSpeed = 300f)
  {
    this.id = inId;
    this.text = message;
    this.speed = inSpeed;
    this.textLength = this.LB_text.width;
    this.locPos = Vector3.zero;
    this.locPos.x = 300f;
    this.displayAllPos = -this.textLength;
    this.moveOutPos = -this.textLength - backgroundLength;
  }

  private void UpdatePos()
  {
    this.locPos.x -= this.speed * Time.deltaTime;
    if (!this.isAllMoveIn && (double) this.locPos.x < (double) this.displayAllPos)
      this.AllMoveIn();
    if (!this.isAllMoveOut && (double) this.locPos.x < (double) this.moveOutPos)
      this.AllMoveOut();
    this.transform.localPosition = this.locPos;
  }

  public void Release()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  public void Update()
  {
    this.UpdatePos();
  }
}
