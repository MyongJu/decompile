﻿// Decompiled with JetBrains decompiler
// Type: ResearchFullLevelPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ResearchFullLevelPopup : ResearchingStatePopup
{
  protected override void Init()
  {
  }

  protected override void Clean()
  {
  }

  protected override void UpdateBonus()
  {
    if ((UnityEngine.Object) this.totalLabel != (UnityEngine.Object) null)
      this.totalLabel.text = this.FormattedModifier(this.techLevel.Effects[0].Property.Format, ResearchManager.inst.NextRank(this.techLevel));
    this.levelLabel.text = this.techLevel.Level.ToString() + "/" + (object) ResearchManager.inst.GetTechSteps(this.techLevel.Tech);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.researchIcon, this.techLevel.Tech.IconPath, (System.Action<bool>) null, true, false, string.Empty);
  }
}
