﻿// Decompiled with JetBrains decompiler
// Type: MarchCallbackDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UI;
using UnityEngine;

public class MarchCallbackDlg : UI.Dialog
{
  private MarchCallbackDlg.Data data = new MarchCallbackDlg.Data();
  private const string RECALL_SHOPITEM_ID = "shopitem_marchrecall";
  private System.Action<long> Handler;
  [SerializeField]
  private MarchCallbackDlg.Panel panel;

  private int _itemCount
  {
    set
    {
      this.panel.itemCount.text = value.ToString();
      this.panel.itemCount.color = value != 0 ? this.panel.normalCountColor : this.panel.zeroCountColor;
      this.panel.BT_buy.gameObject.SetActive(value == 0);
      this.panel.BT_use.gameObject.SetActive(value != 0);
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    MarchCallbackDlg.Parameter parameter = orgParam as MarchCallbackDlg.Parameter;
    if (parameter == null)
      return;
    this.data.marchId = parameter.marchId;
    this.Handler = parameter.recallHandler;
    this._itemCount = ItemBag.Instance.GetItemCountByShopID("shopitem_marchrecall");
    Utils.SetPriceToLabel(this.panel.itemPrice, ItemBag.Instance.GetShopItemPrice("shopitem_marchrecall"));
    if (ItemBag.Instance.GetItemCountByShopID("shopitem_marchrecall") > 0)
      this.panel.onMarchRecallTip.gameObject.SetActive(false);
    else
      this.panel.onMarchRecallTip.gameObject.SetActive(true);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnUserButtonClick()
  {
    ItemBag.Instance.UseItemByShopItemID("shopitem_marchrecall", new Hashtable()
    {
      {
        (object) "march_id",
        (object) this.data.marchId
      }
    }, new System.Action<bool, object>(this.UseCallBack), 1);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void UseCallBack(bool success, object result)
  {
    if (!success || this.Handler == null)
      return;
    this.Handler(this.data.marchId);
    this.Handler = (System.Action<long>) null;
  }

  public void OnBuyButtonClick()
  {
    if (ItemBag.Instance.GetShopItemPrice("shopitem_marchrecall") > PlayerData.inst.hostPlayer.Currency)
    {
      Utils.ShowNotEnoughGoldTip();
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
    {
      ItemBag.Instance.BuyAndUseShopItem("shopitem_marchrecall", new Hashtable()
      {
        {
          (object) "march_id",
          (object) this.data.marchId
        }
      }, new System.Action<bool, object>(this.UseCallBack), 1);
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }

  public void OnCloseButtonClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public long marchId;
    public System.Action<long> recallHandler;
  }

  protected struct Data
  {
    public long marchId;
  }

  [Serializable]
  public class Panel
  {
    public UILabel itemCount;
    public Color zeroCountColor;
    public Color normalCountColor;
    public UILabel itemPrice;
    public UIButton BT_use;
    public UIButton BT_buy;
    public UIButton BT_close;
    public UILabel onMarchRecallTip;
  }
}
