﻿// Decompiled with JetBrains decompiler
// Type: InfoBoard
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class InfoBoard : MonoBehaviour
{
  public UILabel banner;
  public UISprite board;
  public int showtime;
  public int hidetime;
  private int boardwidth;
  public bool stop;
  private int tick;

  public void Init(string content)
  {
    this.banner.text = ScriptLocalization.Get(content, true);
    this.boardwidth = (int) this.banner.printedSize.x;
    this.board.width = this.boardwidth + 85;
    this.OnShiftComplete();
  }

  private void Ticks(int obj)
  {
    if (this.stop)
      return;
    ++this.tick;
    if (this.tick % (this.hidetime + this.showtime) != 0)
      return;
    this.banner.gameObject.SetActive(true);
    this.banner.width = 0;
    this.board.gameObject.SetActive(true);
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) 0, (object) "to", (object) 1, (object) "time", (object) (this.showtime - 3), (object) "onupdate", (object) "Shift", (object) "oncomplete", (object) "OnShiftComplete", (object) "easetype", (object) "Linear"));
  }

  public void Stop()
  {
    this.stop = true;
    iTween.Stop(this.gameObject);
    this.banner.gameObject.SetActive(false);
    this.board.gameObject.SetActive(false);
  }

  public void Resume()
  {
    if (!this.stop)
      return;
    this.stop = false;
    this.OnShiftComplete();
  }

  private void Shift(float value)
  {
    this.banner.width = (int) ((double) this.boardwidth * (double) value);
  }

  private void OnShiftComplete()
  {
    this.StartCoroutine(this.HideHint());
  }

  [DebuggerHidden]
  private IEnumerator HideHint()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new InfoBoard.\u003CHideHint\u003Ec__Iterator41()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Ticks);
  }

  private void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Ticks);
  }
}
