﻿// Decompiled with JetBrains decompiler
// Type: WebviewSystem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class WebviewSystem
{
  private static WebviewSystem m_Inst;

  public static WebviewSystem Instance
  {
    get
    {
      if (WebviewSystem.m_Inst == null)
        WebviewSystem.m_Inst = new WebviewSystem();
      return WebviewSystem.m_Inst;
    }
  }

  private void Start()
  {
  }

  private void Update()
  {
  }

  public void Start(string url)
  {
    UIManager.inst.OpenPopup("WebviewPopup", (Popup.PopupParameter) new WebviewPopupParam(url));
  }
}
