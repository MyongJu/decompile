﻿// Decompiled with JetBrains decompiler
// Type: MessageBox
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class MessageBox : Popup
{
  public System.Action onColose;
  [SerializeField]
  private UILabel m_TitleLabel;
  [SerializeField]
  private UILabel m_ContentLabel;
  [SerializeField]
  private UIButton m_CloseButton;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    MessageBox.Parameter parameter = orgParam as MessageBox.Parameter;
    this.m_TitleLabel.text = parameter.Title;
    this.m_ContentLabel.text = parameter.Content;
    this.onColose = parameter.CloseCallback;
  }

  public void OnClosePressed()
  {
    if (this.onColose != null)
      this.onColose();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string Title;
    public string Content;
    public System.Action CloseCallback;
  }
}
