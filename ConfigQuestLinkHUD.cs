﻿// Decompiled with JetBrains decompiler
// Type: ConfigQuestLinkHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigQuestLinkHUD
{
  private ConfigParse parse = new ConfigParse();
  public Dictionary<int, QuestLinkHUDInfo> internalId2Info;

  public void BuildDB(object res)
  {
    this.parse.Clear();
    this.parse.Parse<QuestLinkHUDInfo, int>(res as Hashtable, "internalId", out this.internalId2Info);
  }

  public QuestLinkHUDInfo GetQuestLinkHudInfo(int internalId)
  {
    if (this.internalId2Info.ContainsKey(internalId))
      return this.internalId2Info[internalId];
    return (QuestLinkHUDInfo) null;
  }
}
