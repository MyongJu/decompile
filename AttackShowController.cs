﻿// Decompiled with JetBrains decompiler
// Type: AttackShowController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AttackShowController : MonoBehaviour
{
  public float battletime = 10f;
  public Camera cinematicCamera;

  public void PlayAction(int action)
  {
    this.BeginAttack();
  }

  private void BeginAttack()
  {
    foreach (AttackShowTroopController componentsInChild in this.GetComponentsInChildren<AttackShowTroopController>())
      componentsInChild.StartCoroutine(componentsInChild.BeginMove());
  }

  public void Dispose()
  {
    foreach (AttackShowTroopController componentsInChild in this.GetComponentsInChildren<AttackShowTroopController>())
    {
      componentsInChild.ReleaseTroops();
      componentsInChild.enabled = false;
    }
    this.StopAllCoroutines();
  }
}
