﻿// Decompiled with JetBrains decompiler
// Type: UIToggleChecker
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIToggleChecker : MonoBehaviour
{
  public UIToggle m_Toggle;
  public UISprite m_CheckSprite;
  public UISprite m_UncheckSprite;

  private void Start()
  {
    EventDelegate.Add(this.m_Toggle.onChange, new EventDelegate.Callback(this.OnToggleChanged));
    this.OnToggleChanged();
  }

  private void OnToggleChanged()
  {
    this.m_CheckSprite.gameObject.SetActive(this.m_Toggle.value);
    this.m_UncheckSprite.gameObject.SetActive(!this.m_Toggle.value);
  }
}
