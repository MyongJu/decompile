﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialCityItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class MerlinTrialCityItem : ComponentRenderBase
{
  private const string VFX_PATH = "Prefab/VFX/MerlinTrials/fx_merlin_trial_challenged";
  public GameObject _circleMenuGO;
  public GameObject _groupBg;
  public UIButton _attackBtn;
  public UIButton _checkBtn;
  public GameObject _monsterGo;
  public UITexture _monsterIcon;
  public UISprite _monsterTypeIcon;
  public GameObject _monsterKilled;
  public GameObject _lockGo;
  public GameObject _cityRoot;
  public UILabel _attackLabel;
  public UILabel _viewLabel;
  private bool _showCircleMenu;
  private System.Action<int> _onSelectHandler;
  private System.Action<int> _onFocusHandler;
  private System.Action<int> _onBattleFinishHandler;
  private GameObject _city;
  private IVfx _vfx;

  public int BattleId { get; private set; }

  public override void Init()
  {
    base.Init();
    MerlinTrialCityItemData data = this.data as MerlinTrialCityItemData;
    this._showCircleMenu = false;
    this._circleMenuGO.SetActive(false);
    this.BattleId = data.battleId;
    this._onSelectHandler = data.onSelectAction;
    this._onFocusHandler = data.onFocusAction;
    this._onBattleFinishHandler = data.onBattleFinishAction;
    MerlinTrialsMainInfo merlinTrialsMainInfo = ConfigManager.inst.DB_MerlinTrialsMain.Get(this.BattleId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this._monsterIcon, merlinTrialsMainInfo.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    this._monsterTypeIcon.spriteName = ConfigManager.inst.DB_MerlinTrialsGroup.Get(merlinTrialsMainInfo.trialId).TypeIconPath;
    this.UpdateCity(AssetManager.Instance.Load(merlinTrialsMainInfo.CityImagePath, (System.Type) null) as GameObject);
    if (this._vfx == null)
    {
      this._vfx = VfxManager.Instance.Create("Prefab/VFX/MerlinTrials/fx_merlin_trial_challenged");
      NGUITools.SetLayer(this._vfx.gameObject, LayerMask.NameToLayer("NGUI"));
    }
    switch (MerlinTrialsHelper.inst.GetMonsterStatus(this.BattleId))
    {
      case MerlinMonsterStatus.can_attack:
        this._lockGo.SetActive(false);
        this._monsterKilled.SetActive(false);
        this._vfx.Stop();
        break;
      case MerlinMonsterStatus.defeated:
        this._lockGo.SetActive(false);
        this._monsterKilled.SetActive(true);
        this._vfx.Play(this.transform);
        break;
      case MerlinMonsterStatus.locked:
        this._lockGo.SetActive(true);
        this._monsterKilled.SetActive(false);
        this._vfx.Stop();
        break;
    }
  }

  public override void Dispose()
  {
    base.Dispose();
    if (this._vfx == null)
      return;
    VfxManager.Instance.Delete(this._vfx);
    this._vfx = (IVfx) null;
  }

  private void UpdateCity(GameObject cityPrefab)
  {
    if ((UnityEngine.Object) this._city != (UnityEngine.Object) null)
    {
      this._city.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this._city);
    }
    this._city = UnityEngine.Object.Instantiate<GameObject>(cityPrefab);
    NGUITools.SetLayer(this._city, LayerMask.NameToLayer("NGUI"));
    this._city.transform.parent = this._cityRoot.transform;
    this._city.transform.localPosition = new Vector3(0.0f, 30f, 0.0f);
    this._city.transform.localScale = Vector3.one * 1.2f;
    SpriteRenderer[] componentsInChildren = this._cityRoot.GetComponentsInChildren<SpriteRenderer>();
    int index = 0;
    for (int length = componentsInChildren.Length; index < length; ++index)
      componentsInChildren[index].material = new Material(Shader.Find("Unlit/Transparent Colored"));
  }

  public void OnCityClick()
  {
    if (this._showCircleMenu)
      this.OnSelect(-1);
    else
      this.OnSelect(this.BattleId);
  }

  public void ShowCircleMenu(bool show)
  {
    this._showCircleMenu = show;
    this._circleMenuGO.SetActive(this._showCircleMenu);
    if (!this._showCircleMenu)
      return;
    this._viewLabel.text = Utils.XLAT("id_details");
    this._attackLabel.text = Utils.XLAT("id_challenge");
    this.PlayButtonAnimation();
    AudioManager.Instance.PlaySound("sfx_kingdom_map_click_city", false);
  }

  private void PlayButtonAnimation()
  {
    this.CircleAnimation(0.0f);
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) 0.0f, (object) "to", (object) 1f, (object) "time", (object) 0.1f, (object) "easetype", (object) iTween.EaseType.easeInOutBack, (object) "onupdate", (object) "CircleAnimation", (object) "oncomplete", (object) "OnComplete"));
  }

  private void CircleAnimation(float value)
  {
    this._circleMenuGO.transform.localScale = new Vector3(1f, 1f, 1f) * value;
  }

  private void OnComplete()
  {
    this.CircleAnimation(1f);
  }

  public void OnAttackClick()
  {
    if (!MerlinTrialsHelper.inst.IsOpenDay(this.BattleId))
      UIManager.inst.toast.Show(Utils.XLAT("toast_trial_not_open"), (System.Action) null, 4f, false);
    else if (MerlinTrialsHelper.inst.GetMonsterStatus(this.BattleId) == MerlinMonsterStatus.locked)
      UIManager.inst.toast.Show(Utils.XLAT("toast_trial_complete_previous_challenge"), (System.Action) null, 4f, false);
    else
      UIManager.inst.OpenDlg("MerlinTrials/MerlinTrialsMarchAllocDlg", (UI.Dialog.DialogParameter) new MerlinTrialsMarchAllocDialog.Parameter()
      {
        battleId = this.BattleId,
        onBattleFinishCallback = new System.Action<int>(this.OnBattleFinished)
      }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnViewClick()
  {
    UIManager.inst.OpenPopup("MerlinTrials/MonsterDetailPopup", (Popup.PopupParameter) new MerlinTrialsMonsterDetailPopup.Parameter()
    {
      battleId = this.BattleId,
      onFocus = new System.Action<int>(this.OnFocus),
      onBattleFinished = new System.Action<int>(this.OnBattleFinished)
    });
  }

  private void OnFocus(int battleId)
  {
    if (this._onFocusHandler == null)
      return;
    this._onFocusHandler(battleId);
  }

  private void OnSelect(int battleId)
  {
    if (this._onSelectHandler == null)
      return;
    this._onSelectHandler(battleId);
  }

  private void OnBattleFinished(int battleId)
  {
    if (this._onBattleFinishHandler == null)
      return;
    this._onBattleFinishHandler(battleId);
  }
}
