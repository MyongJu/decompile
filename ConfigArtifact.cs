﻿// Decompiled with JetBrains decompiler
// Type: ConfigArtifact
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigArtifact
{
  private List<ArtifactInfo> artifactInfoList = new List<ArtifactInfo>();
  private Dictionary<string, ArtifactInfo> datas;
  private Dictionary<int, ArtifactInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ArtifactInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    Dictionary<string, ArtifactInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this.artifactInfoList.Add(enumerator.Current);
    }
    this.artifactInfoList.Sort((Comparison<ArtifactInfo>) ((a, b) => int.Parse(a.id).CompareTo(int.Parse(b.id))));
  }

  public List<ArtifactInfo> GetArtifactInfoList()
  {
    return this.artifactInfoList;
  }

  public ArtifactInfo Get(int interalId)
  {
    if (this.dicByUniqueId.ContainsKey(interalId))
      return this.dicByUniqueId[interalId];
    return (ArtifactInfo) null;
  }

  public ArtifactInfo Get(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (ArtifactInfo) null;
  }

  public ArtifactInfo GetArtifactByActiveEffect(string activeEffect)
  {
    return this.artifactInfoList.Find((Predicate<ArtifactInfo>) (x => x.activeEffect == activeEffect));
  }
}
