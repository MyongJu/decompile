﻿// Decompiled with JetBrains decompiler
// Type: ResourceReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ResourceReward : QuestReward
{
  public readonly CityManager.ResourceTypes type;
  public readonly int value;
  private LookupInfo _resource;

  public ResourceReward(CityManager.ResourceTypes type, int value)
  {
    this.type = type;
    this.value = value;
    if (!ConfigManager.inst.DB_Lookup.Contains(type.ToString().ToLower()))
      return;
    this._resource = ConfigManager.inst.DB_Lookup[type.ToString().ToLower()];
  }

  public override void Claim()
  {
    base.Claim();
  }

  public override string GetRewardIconName()
  {
    return this._resource.Image;
  }

  public override string GetRewardTypeName()
  {
    return this._resource.Name;
  }

  public override int GetValue()
  {
    return this.value;
  }

  public override string GetValueText()
  {
    return Utils.FormatShortThousands(this.value);
  }
}
