﻿// Decompiled with JetBrains decompiler
// Type: PitGoHomePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PitGoHomePopup : Popup
{
  [SerializeField]
  private UIButton _btnGoHomeFree;
  [SerializeField]
  private UIButton _btnGoHomeUseGold;
  [SerializeField]
  private UILabel _labelCostGold;
  [SerializeField]
  private UILabel _labelFreeTimes;
  [SerializeField]
  private UILabel _description;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    int leavePitCost = PitExplorePayload.Instance.GetLeavePitCost();
    NGUITools.SetActive(this._btnGoHomeFree.gameObject, leavePitCost <= 0);
    NGUITools.SetActive(this._btnGoHomeUseGold.gameObject, leavePitCost > 0);
    NGUITools.SetActive(this._labelFreeTimes.gameObject, leavePitCost <= 0);
    this._description.text = ScriptLocalization.GetWithPara("event_fire_kingdom_return_home_description", new Dictionary<string, string>()
    {
      {
        "0",
        PitExplorePayload.Instance.MaxFreeBackCount.ToString()
      }
    }, true);
    if (leavePitCost <= 0)
    {
      this._labelFreeTimes.text = ScriptLocalization.GetWithPara("event_fire_kingdom_free_return_home_num", new Dictionary<string, string>()
      {
        {
          "0",
          PitExplorePayload.Instance.FreeBackCount.ToString()
        }
      }, true);
    }
    else
    {
      this._labelCostGold.text = Utils.FormatThousands(leavePitCost.ToString());
      if (PlayerData.inst.hostPlayer.Currency >= leavePitCost)
      {
        this._labelCostGold.color = Color.white;
        this._btnGoHomeUseGold.isEnabled = true;
      }
      else
      {
        this._labelCostGold.color = Color.red;
        this._btnGoHomeUseGold.isEnabled = false;
      }
    }
  }

  public void OnButtonGoHomeClicked()
  {
    MessageHub.inst.GetPortByAction("Abyss:leave").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Lite);
    }), true);
  }

  public void OnButtonCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }
}
