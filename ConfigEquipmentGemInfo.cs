﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentGemInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using GemConstant;

public class ConfigEquipmentGemInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "item_id")]
  public int itemID;
  [Config(Name = "slot_type")]
  public SlotType slotType;
  [Config(Name = "level")]
  public int level;
  [Config(Name = "gem_type")]
  public int gemType;
  [Config(CustomParse = true, CustomType = typeof (Benefits), Name = "Benefits")]
  public Benefits benefits;
  [Config(Name = "exp_req_to_next")]
  public long expReqToNext;
  [Config(Name = "next_level_id")]
  public int nextLevelId;
  [Config(Name = "exp_contain")]
  public long expContain;
  [Config(Name = "handbook_id")]
  public int handbookId;

  public string IconPath
  {
    get
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.itemID);
      if (itemStaticInfo != null)
        return itemStaticInfo.ImagePath;
      return string.Empty;
    }
  }
}
