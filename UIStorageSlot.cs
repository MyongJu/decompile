﻿// Decompiled with JetBrains decompiler
// Type: UIStorageSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Examples/UI Storage Slot")]
public class UIStorageSlot : UIItemSlot
{
  public UIItemStorage storage;
  public int slot;

  protected override InvGameItem observedItem
  {
    get
    {
      if ((Object) this.storage != (Object) null)
        return this.storage.GetItem(this.slot);
      return (InvGameItem) null;
    }
  }

  protected override InvGameItem Replace(InvGameItem item)
  {
    if ((Object) this.storage != (Object) null)
      return this.storage.Replace(this.slot, item);
    return item;
  }
}
