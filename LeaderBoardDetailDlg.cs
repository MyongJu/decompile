﻿// Decompiled with JetBrains decompiler
// Type: LeaderBoardDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class LeaderBoardDetailDlg : UI.Dialog
{
  private static Vector3 SCROLL_RESET_POSITION = new Vector3(-0.004882813f, -24f, 0.0f);
  private GameObjectPool m_ItemPool = new GameObjectPool();
  private List<LeaderDetailItem> leaderItem = new List<LeaderDetailItem>();
  private int totalNum = int.Parse("10");
  private string method = string.Empty;
  private const string UP_ARROW_PATH = null;
  private const string DOWN_ARROW_PATH = null;
  public GameObject itemTemplate;
  public GameObject itemRoot;
  public UILabel titleLabel;
  public UITable tabel;
  public UILabel updownNum;
  public UITexture updownArrow;
  public UITexture IconTexture;
  public UILabel Name;
  public UILabel Fighting;
  public GameObject noJoinAllian;
  private RankData currentData;
  public UIDraggableCamera m_DraggableCamera;
  public UIDragCamera m_DragCamera;
  public UIViewport m_Viewport;
  public UIWidget bar;
  public UIWidget backBt;
  public UIWidget closeBt;
  public UILabel tip;
  public UILabel myRank;
  public GameObject rankTip;
  public GameObject normalTitle;
  public GameObject dragonTitle;
  public GameObject levelTitle;
  public GameObject kingdomTitle;
  public UILabel myTip;
  public UILabel headerTitle;
  public UILabel emptyLabel;
  private bool m_Initialized;
  private int lastRank;
  private int currentRank;
  private List<RankData> _datas;
  private string title;
  private bool sendRequest;
  private bool init;
  private LeaderBoardDetailDlg.LeaderBoardDetailDlgParamer _param;
  private Hashtable reqParam;
  private long _myScore;

  private void Start()
  {
    this.m_DraggableCamera.gameObject.SetActive(false);
    this.init = true;
    if (this.sendRequest)
      return;
    this.SendRequest(this.method, this.reqParam);
  }

  private void InitItemHandler(GameObject go, int wrapIndex, int realIndex)
  {
    go.GetComponent<LeaderDetailItem>().SetData(this._datas[Mathf.Abs(realIndex)], this._param.isShowKingdom, this._param.hideKingdomTitle);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.m_DraggableCamera.gameObject.SetActive(false);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    LeaderBoardDetailDlg.LeaderBoardDetailDlgParamer detailDlgParamer = orgParam as LeaderBoardDetailDlg.LeaderBoardDetailDlgParamer;
    if (detailDlgParamer == null)
    {
      this.m_DraggableCamera.gameObject.SetActive(this._datas.Count > 0);
    }
    else
    {
      this._param = detailDlgParamer;
      if (!this.m_Initialized)
      {
        this.m_ItemPool.Initialize(this.itemTemplate, this.tabel.gameObject);
        this.m_Initialized = true;
      }
      if ((UnityEngine.Object) this.myTip != (UnityEngine.Object) null)
        this.myTip.text = string.Empty;
      this.m_Viewport.sourceCamera = UIManager.inst.ui2DCamera;
      this.m_DraggableCamera.gameObject.SetActive(false);
      this.title = detailDlgParamer.title;
      this.sendRequest = false;
      this.reqParam = detailDlgParamer.param;
      this.method = detailDlgParamer.method;
      this.HideAllTitle();
      switch (detailDlgParamer.optItemID)
      {
        case 3:
        case 4:
          NGUITools.SetActive(this.dragonTitle, true);
          break;
        case 6:
          NGUITools.SetActive(this.levelTitle, true);
          break;
        default:
          NGUITools.SetActive(this.normalTitle, true);
          if ((UnityEngine.Object) this.headerTitle != (UnityEngine.Object) null)
            this.headerTitle.text = ScriptLocalization.Get(detailDlgParamer.headerTitle, true);
          if ((UnityEngine.Object) this.myTip != (UnityEngine.Object) null && !string.IsNullOrEmpty(detailDlgParamer.tip))
          {
            this.myTip.text = ScriptLocalization.Get(detailDlgParamer.tip, true) + " " + this._myScore.ToString();
            break;
          }
          break;
      }
      if ((UnityEngine.Object) this.kingdomTitle != (UnityEngine.Object) null)
        NGUITools.SetActive(this.kingdomTitle.gameObject, !detailDlgParamer.hideKingdomTitle);
      NGUITools.SetActive(this.noJoinAllian, false);
      if (!this.init)
        return;
      this.SendRequest(this.method, this.reqParam);
    }
  }

  private void HideAllTitle()
  {
    NGUITools.SetActive(this.normalTitle, false);
    NGUITools.SetActive(this.dragonTitle, false);
    NGUITools.SetActive(this.levelTitle, false);
  }

  private void SendRequest(string method, Hashtable param)
  {
    this.sendRequest = true;
    MessageHub.inst.GetPortByAction(method).SendRequest(param, new System.Action<bool, object>(this.ResultHandler), true);
  }

  private void ResultHandler(bool success, object result)
  {
    if (!success)
      return;
    this._datas = this.ParseData(result as Hashtable);
    this.currentData = this.GetCurrentRankData(result as Hashtable);
    this._datas.Sort(new Comparison<RankData>(this.CompareData));
    this.UpdateUI();
  }

  private int CompareData(RankData a, RankData b)
  {
    return a.Rank.CompareTo(b.Rank);
  }

  private RankData GetCurrentRankData(Hashtable sources)
  {
    if (!sources.ContainsKey((object) "mine"))
      return (RankData) null;
    RankData rankData = RankData.Parse(sources[(object) "mine"] as Hashtable);
    if (rankData != null)
    {
      this._datas.Add(rankData);
      this.myRank.text = rankData.Rank.ToString();
    }
    return rankData;
  }

  private List<RankData> ParseData(Hashtable sources)
  {
    List<RankData> rankDataList = new List<RankData>();
    if (sources != null && sources.ContainsKey((object) "my_score") && sources[(object) "my_score"] != null)
      long.TryParse(sources[(object) "my_score"].ToString(), out this._myScore);
    if (sources != null && sources.ContainsKey((object) "ranks"))
    {
      ArrayList source = sources[(object) "ranks"] as ArrayList;
      for (int index = 0; index < source.Count; ++index)
      {
        RankData rankData = RankData.Parse(source[index] as Hashtable);
        rankDataList.Add(rankData);
        if (rankData is AllianceRankData)
        {
          if ((long) (rankData as AllianceRankData).AllianceId == PlayerData.inst.allianceId)
            this.myRank.text = rankData.Rank.ToString();
        }
        else if (rankData.UID == PlayerData.inst.hostPlayer.uid.ToString())
          this.myRank.text = rankData.Rank.ToString();
      }
    }
    return rankDataList;
  }

  private RankData CreateRankData(Hashtable source)
  {
    RankData rankData;
    if (source.ContainsKey((object) "alliance_id"))
    {
      AllianceRankData allianceRankData = new AllianceRankData();
      allianceRankData.AllianceId = int.Parse(source[(object) "alliance_id"].ToString());
      allianceRankData.Lang = source[(object) "lang"].ToString();
      allianceRankData.Member = int.Parse(source[(object) "member"].ToString());
      allianceRankData.Rank = int.Parse(source[(object) "rank"].ToString());
      rankData = (RankData) allianceRankData;
      if ((long) allianceRankData.AllianceId == PlayerData.inst.allianceId)
        this.myRank.text = rankData.Rank.ToString();
    }
    else
    {
      PlayerRankData playerRankData = new PlayerRankData();
      if (source.ContainsKey((object) "portrait") && source[(object) "portrait"] != null)
        playerRankData.Portrait = int.Parse(source[(object) "portrait"].ToString());
      rankData = (RankData) playerRankData;
    }
    if (source.ContainsKey((object) "name") && source[(object) "name"] != null)
      rankData.Name = source[(object) "name"].ToString();
    rankData.Score = long.Parse(source[(object) "score"].ToString());
    rankData.Rank = int.Parse(source[(object) "rank"].ToString());
    if (source.ContainsKey((object) "uid"))
      rankData.UID = source[(object) "uid"].ToString();
    if (rankData.UID == PlayerData.inst.uid.ToString())
      this.myRank.text = rankData.Rank.ToString();
    if (source.ContainsKey((object) "tag"))
    {
      rankData.Tag = source[(object) "tag"].ToString();
      rankData.Symbol = int.Parse(source[(object) "symbol_code"].ToString());
    }
    return rankData;
  }

  private void Clear()
  {
    for (int index = 0; index < this.leaderItem.Count; ++index)
    {
      this.leaderItem[index].Clear();
      this.m_ItemPool.Release(this.leaderItem[index].gameObject);
    }
    this.leaderItem.Clear();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Clear();
    this.m_DraggableCamera.gameObject.SetActive(false);
  }

  private void UpdateUI()
  {
    this.titleLabel.text = this.title;
    if (this.currentData == null)
    {
      NGUITools.SetActive(this.noJoinAllian, true);
      this.tip.text = ScriptLocalization.Get("leaderboards_no_rank_notice", true);
      NGUITools.SetActive(this.rankTip.gameObject, false);
    }
    else
    {
      NGUITools.SetActive(this.noJoinAllian, false);
      NGUITools.SetActive(this.rankTip.gameObject, true);
    }
    if (this._param != null && (UnityEngine.Object) this.myTip != (UnityEngine.Object) null && !string.IsNullOrEmpty(this._param.tip))
      this.myTip.text = ScriptLocalization.Get(this._param.tip, true) + " " + this._myScore.ToString();
    if ((UnityEngine.Object) this.emptyLabel != (UnityEngine.Object) null)
      NGUITools.SetActive(this.emptyLabel.gameObject, this._datas.Count == 0);
    this.CreateLegendItems();
    this.m_DraggableCamera.transform.localPosition = LeaderBoardDetailDlg.SCROLL_RESET_POSITION;
    this.tabel.Reposition();
    this.m_DraggableCamera.gameObject.SetActive(this._datas.Count > 0);
    this.m_DragCamera.enabled = this._datas.Count >= 4;
  }

  private void CreateLegendItems()
  {
    this.totalNum = this._datas.Count;
    for (int realIndex = 0; realIndex < this.totalNum; ++realIndex)
    {
      GameObject go = this.m_ItemPool.AddChild(this.tabel.gameObject);
      go.name = "index=" + (object) realIndex;
      go.SetActive(true);
      LeaderDetailItem component = go.GetComponent<LeaderDetailItem>();
      Vector3 localPosition = component.transform.localPosition;
      localPosition.y = (float) (-269 * realIndex);
      go.transform.localPosition = localPosition;
      this.InitItemHandler(go, 0, realIndex);
      this.leaderItem.Add(component);
    }
  }

  public void OnCloseBtnClicked()
  {
    this.Clear();
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnBckBtnClicked()
  {
    this.Clear();
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public class LeaderBoardDetailDlgParamer : UI.Dialog.DialogParameter
  {
    public string headerTitle = "leaderboards_uppercase_score";
    public string tip = string.Empty;
    public int optItemID;
    public List<RankData> datas;
    public RankData current;
    public string title;
    public string method;
    public Hashtable param;
    public bool isShowKingdom;
    public bool hideKingdomTitle;
  }
}
