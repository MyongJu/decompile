﻿// Decompiled with JetBrains decompiler
// Type: Effect
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

public class Effect : IComparable<Effect>
{
  private readonly int _propertyInternalID;
  private readonly float _modifier;
  private readonly Effect.SourceType _source;
  private readonly long _sourceID;
  private int _owner;
  private readonly Effect.ConditionType _condition;
  private readonly int _privilegeInternalID;

  public Effect(int propertyInternalID, int privilegeInternalID, float modifier, Effect.SourceType source, long sourceID, int owner, Effect.ConditionType condition)
  {
    this._propertyInternalID = propertyInternalID;
    this._modifier = modifier;
    this._sourceID = sourceID;
    this._source = source;
    this._owner = owner;
    this._condition = condition;
    this._privilegeInternalID = privilegeInternalID;
  }

  public Effect(int propertyInternalID, int privilegeInternalID, float modifier, Effect.SourceType source, long sourceID, int owner)
    : this(propertyInternalID, privilegeInternalID, modifier, source, sourceID, owner, Effect.ConditionType.all)
  {
  }

  public Effect(int propertyInternalID, int privilegeInternalID, float modifier, Effect.SourceType source, long sourceID)
    : this(propertyInternalID, privilegeInternalID, modifier, source, sourceID, 0, Effect.ConditionType.all)
  {
  }

  public string Name
  {
    get
    {
      if (this.Property == null)
        return "n/a";
      return this.Property.Name;
    }
  }

  public int PropertyInternalID
  {
    get
    {
      return this._propertyInternalID;
    }
  }

  public int PrivilegeInternalID
  {
    get
    {
      return this._privilegeInternalID;
    }
  }

  public PropertyDefinition Property
  {
    get
    {
      return ConfigManager.inst.DB_Properties[this._propertyInternalID];
    }
  }

  public PrivilegeInfo Privilege
  {
    get
    {
      return ConfigManager.inst.DB_Privilege.GetData(this._privilegeInternalID);
    }
  }

  public float Modifier
  {
    get
    {
      return this._modifier;
    }
  }

  public string FormattedModifier
  {
    get
    {
      if (this.Property == null)
        return "n/a";
      if (this.Property.Format == PropertyDefinition.FormatType.Percent)
      {
        if ((double) this.Modifier > 0.0)
          return string.Format("+{0:P1}", (object) this.Modifier);
        return string.Format("{0:P1}", (object) this.Modifier);
      }
      if (this.Property.Format == PropertyDefinition.FormatType.Seconds)
      {
        if ((double) this.Modifier > 0.0)
          return string.Format("+{0}s", (object) this.Modifier);
        return string.Format("{0}s", (object) this.Modifier);
      }
      if (this.Property.Format == PropertyDefinition.FormatType.Boolean)
        return string.Format("{0}", (object) ((double) this.Modifier > 0.0));
      return string.Format("+{0}", (object) this.Modifier);
    }
  }

  public long SourceID
  {
    get
    {
      return this._sourceID;
    }
  }

  public Effect.SourceType Source
  {
    get
    {
      return this._source;
    }
  }

  public Effect.ConditionType Condition
  {
    get
    {
      return this._condition;
    }
  }

  public int Owner
  {
    get
    {
      return this._owner;
    }
    set
    {
      this._owner = value;
    }
  }

  public int CompareTo(Effect other)
  {
    if (other == null)
      return 1;
    return this.Source.CompareTo((object) other.Source);
  }

  [Flags]
  public enum ConditionType
  {
    marching = 1,
    rallying = 2,
    occupying = 4,
    gathering = 8,
    guarding = 16, // 0x00000010
    captive = 32, // 0x00000020
    attacking = rallying | marching, // 0x00000003
    defending = guarding | gathering | occupying, // 0x0000001C
    all = defending | attacking | captive, // 0x0000003F
  }

  [Flags]
  public enum SourceType
  {
    Item = 1,
    Gem = 2,
    Set = 4,
    Buff = 8,
    Skill = 16, // 0x00000010
    Research = 32, // 0x00000020
    Building = 64, // 0x00000040
    VIP = 128, // 0x00000080
    Level = 256, // 0x00000100
    Rank = 512, // 0x00000200
    Setting = 1024, // 0x00000400
    All = Setting | Rank | Level | VIP | Building | Research | Skill | Buff | Set | Gem | Item, // 0x000007FF
  }
}
