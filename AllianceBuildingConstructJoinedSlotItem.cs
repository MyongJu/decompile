﻿// Decompiled with JetBrains decompiler
// Type: AllianceBuildingConstructJoinedSlotItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UnityEngine;

public class AllianceBuildingConstructJoinedSlotItem : MonoBehaviour
{
  private bool firstTime = true;
  public UITexture playerIcon;
  public UILabel playerName;
  public UILabel troopsCount;
  public UIButton speedUpBt;
  public UILabel btnsts;
  public UILabel tip;
  public UISprite dragonIcon;
  public GameObject noDragon;
  public GameObject OpenContent;
  public UISprite normalBg;
  public UISprite arrow;
  public DragonUIInfo dragonInfo;
  public MarchTroopsUIInfo marchInfo;
  public UITable childContainer;
  private long march_id;
  private bool isOpen;
  public System.Action OnOpenCallBackDelegate;

  public void SetData(long march_id)
  {
    this.march_id = march_id;
    MarchData marchData = DBManager.inst.DB_March.Get(march_id);
    if (marchData == null)
      return;
    this.troopsCount.text = marchData.troopsInfo.totalCount.ToString();
    UserData userData = DBManager.inst.DB_User.Get(marchData.ownerUid);
    if (userData == null)
      return;
    this.playerName.text = userData.userName;
    CustomIconLoader.Instance.requestCustomIcon(this.playerIcon, UserData.GetPortraitIconPath(userData.portrait), userData.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.playerIcon, userData.LordTitle, 1);
    this.RefreshTroops();
    this.RefreshDragon();
    this.childContainer.repositionNow = true;
    this.childContainer.Reposition();
    this.UpdateCallBackBtnState();
    this.UpdateLabelState();
  }

  private void UpdateLabelState()
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.march_id);
    if (marchData == null)
      return;
    TileData selectedTile = PVPSystem.Instance.Map.SelectedTile;
    long num = 0;
    if (selectedTile.TileType == TileType.AllianceWarehouse)
      num = selectedTile.WareHouseData.AllianceId;
    else if (selectedTile.TileType == TileType.AllianceSuperMine)
      num = selectedTile.SuperMineData.AllianceId;
    else if (selectedTile.TileType == TileType.AllianceHospital)
      num = selectedTile.HospitalData.AllianceID;
    else if (selectedTile.TileType == TileType.AllianceTemple)
      num = selectedTile.TempleData.allianceId;
    if (marchData.ownerAllianceId == num)
      NGUITools.SetActive(this.tip.gameObject, true);
    else
      NGUITools.SetActive(this.tip.gameObject, false);
  }

  private void UpdateCallBackBtnState()
  {
    int num1 = 0;
    int num2 = 0;
    AllianceData allianceData = PlayerData.inst.allianceData;
    MarchData marchData = DBManager.inst.DB_March.Get(this.march_id);
    if (marchData == null)
      return;
    TileData selectedTile = PVPSystem.Instance.Map.SelectedTile;
    long num3 = 0;
    if (selectedTile.TileType == TileType.AllianceWarehouse)
      num3 = selectedTile.WareHouseData.AllianceId;
    else if (selectedTile.TileType == TileType.AllianceSuperMine)
      num3 = selectedTile.SuperMineData.AllianceId;
    else if (selectedTile.TileType == TileType.AllianceHospital)
      num3 = selectedTile.HospitalData.AllianceID;
    else if (selectedTile.TileType == TileType.AllianceTemple)
      num3 = selectedTile.TempleData.allianceId;
    if (allianceData == null || marchData.ownerAllianceId != allianceData.allianceId || num3 != allianceData.allianceId)
    {
      if (marchData.ownerUid == PlayerData.inst.uid)
        this.btnsts.text = ScriptLocalization.Get("march_callback", true);
      else
        NGUITools.SetActive(this.speedUpBt.gameObject, false);
    }
    else if (allianceData != null && num3 == allianceData.allianceId && marchData.ownerAllianceId != allianceData.allianceId)
    {
      this.btnsts.text = ScriptLocalization.Get("watchtower_march_send_back_reinforcements_button", true);
    }
    else
    {
      if (allianceData != null)
      {
        AllianceMemberData allianceMemberData1 = allianceData.members.Get(PlayerData.inst.uid);
        if (allianceMemberData1 != null)
          num1 = allianceMemberData1.title;
        AllianceMemberData allianceMemberData2 = allianceData.members.Get(marchData.ownerUid);
        if (allianceMemberData2 != null)
          num2 = allianceMemberData2.title;
      }
      this.speedUpBt.isEnabled = 5 <= num1 || (num1 != 4 ? marchData.ownerUid == PlayerData.inst.uid : num2 < num1);
      if (marchData.ownerUid == PlayerData.inst.uid)
        this.speedUpBt.isEnabled = true;
      AllianceSuperMineData superMineData = selectedTile.SuperMineData;
      if (this.speedUpBt.isEnabled)
      {
        this.btnsts.text = marchData.ownerUid != PlayerData.inst.uid ? ScriptLocalization.Get("watchtower_march_send_back_reinforcements_button", true) : ScriptLocalization.Get("march_callback", true);
        if (marchData.ownerUid == PlayerData.inst.uid || superMineData == null || superMineData.CurrentState != AllianceSuperMineData.State.COMPLETE)
          return;
        NGUITools.SetActive(this.speedUpBt.gameObject, false);
      }
      else
        NGUITools.SetActive(this.speedUpBt.gameObject, false);
    }
  }

  public bool IsOpen
  {
    get
    {
      return this.isOpen;
    }
    set
    {
      if (this.isOpen == value)
        return;
      this.isOpen = value;
      if (this.isOpen)
        this.arrow.transform.localEulerAngles = new Vector3(0.0f, 0.0f, -90f);
      else
        this.arrow.transform.localEulerAngles = Vector3.zero;
    }
  }

  public void OnClickHandler()
  {
    if (this.march_id <= 0L)
      return;
    this.IsOpen = !this.IsOpen;
    NGUITools.SetActive(this.OpenContent, this.IsOpen);
    NGUITools.SetActive(this.normalBg.gameObject, false);
    DBManager.inst.DB_March.Get(this.march_id);
    Utils.ExecuteInSecs(0.01f, (System.Action) (() =>
    {
      if (!this.IsOpen)
      {
        this.normalBg.height = (int) byte.MaxValue;
      }
      else
      {
        Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.transform, false);
        if (this.firstTime)
        {
          this.firstTime = false;
          this.normalBg.height = (int) relativeWidgetBounds.size.y;
        }
        else
          this.normalBg.height = (int) relativeWidgetBounds.size.y + 80;
      }
      NGUITools.SetActive(this.normalBg.gameObject, true);
      this.normalBg.ResizeCollider();
      NGUITools.AddWidgetCollider(this.transform.parent.gameObject);
      this.childContainer.Reposition();
      if (this.OnOpenCallBackDelegate == null)
        return;
      this.OnOpenCallBackDelegate();
    }));
  }

  private void RefreshTroops()
  {
    this.marchInfo.SetData(this.march_id);
  }

  private void RefreshDragon()
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.march_id);
    string skillType = "attack";
    NGUITools.SetActive(this.dragonIcon.gameObject, marchData.withDragon);
    NGUITools.SetActive(this.noDragon, !marchData.withDragon);
    bool state = false;
    NGUITools.SetActive(this.dragonInfo.gameObject, state);
    if (!state)
      return;
    this.dragonInfo.SetDragon(marchData.dragonId, skillType, (int) marchData.dragon.tendency, marchData.dragon.level);
  }

  public void RecallMarch()
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.march_id);
    if (marchData.ownerUid == PlayerData.inst.uid)
    {
      GameEngine.Instance.marchSystem.Recall(this.march_id);
    }
    else
    {
      TileData selectedTile = PVPSystem.Instance.Map.SelectedTile;
      long buildingid = 0;
      if (selectedTile.TileType == TileType.AllianceWarehouse)
        buildingid = selectedTile.WareHouseData.WarehouseId;
      else if (selectedTile.TileType == TileType.AllianceSuperMine)
        buildingid = selectedTile.SuperMineData.SuperMineId;
      else if (selectedTile.TileType == TileType.AllianceHospital)
        buildingid = selectedTile.HospitalData.BuildingID;
      else if (selectedTile.TileType == TileType.AllianceTemple)
        buildingid = selectedTile.TempleData.templeId;
      GameEngine.Instance.marchSystem.SendAllianceBuildingTroopHome(selectedTile.TileType, buildingid, marchData.ownerUid, (System.Action<bool, object>) null);
    }
  }

  public void Clear()
  {
    this.OnOpenCallBackDelegate = (System.Action) null;
  }
}
