﻿// Decompiled with JetBrains decompiler
// Type: LuckyArcherGroupInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class LuckyArcherGroupInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "group_weight")]
  public int groupWeight;
  [Config(Name = "group_name")]
  public string groupName;

  public string GroupName
  {
    get
    {
      return Utils.XLAT(this.groupName);
    }
  }
}
