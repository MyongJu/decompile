﻿// Decompiled with JetBrains decompiler
// Type: UISpriteBuilder
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UISpriteBuilder : BaseBuilder
{
  private UISprite uiSprite;
  private UIAtlas atlas;

  protected override void Prepare()
  {
    if ((UnityEngine.Object) this.Target == (UnityEngine.Object) null || !((UnityEngine.Object) this.uiSprite == (UnityEngine.Object) null))
      return;
    this.uiSprite = this.Target.GetComponent<UISprite>();
  }

  protected override void Clear()
  {
    if (!((UnityEngine.Object) this.uiSprite != (UnityEngine.Object) null))
      return;
    this.uiSprite.atlas = (UIAtlas) null;
    this.uiSprite = (UISprite) null;
  }

  protected override System.Type GetResourceType()
  {
    return typeof (GameObject);
  }

  protected override string GetPath()
  {
    string[] strArray = this.ImageName.Split('@');
    if (strArray.Length <= 1)
    {
      if ((UnityEngine.Object) this.uiSprite != (UnityEngine.Object) null)
      {
        if ((UnityEngine.Object) this.uiSprite.atlas == (UnityEngine.Object) null)
        {
          this.uiSprite.atlas = this.MissingAtlas;
          this.IsSuccess = false;
        }
        if (this.uiSprite.atlas.GetSprite(this.ImageName) == null)
        {
          this.uiSprite.atlas = this.MissingAtlas;
          this.uiSprite.spriteName = this.MissingSpriteName;
          this.IsSuccess = false;
        }
        else
        {
          this.uiSprite.spriteName = this.ImageName;
          this.IsSuccess = true;
        }
        if (this.OnCompleteHandler != null)
          this.OnCompleteHandler(this.IsSuccess);
      }
      return string.Empty;
    }
    string str1 = strArray[0];
    this.ImageName = strArray[1];
    string str2 = "Atlas";
    if (string.IsNullOrEmpty(str1))
      return this.ImageName;
    return str2 + "/" + str1 + string.Empty;
  }

  protected override void ResultHandler(UnityEngine.Object result)
  {
    Texture2D texture2D = result as Texture2D;
    GameObject gameObject = result as GameObject;
    if ((UnityEngine.Object) gameObject != (UnityEngine.Object) null)
      this.atlas = gameObject.GetComponent<UIAtlas>();
    if ((UnityEngine.Object) this.atlas == (UnityEngine.Object) null)
    {
      this.atlas = this.MissingAtlas;
      this.IsSuccess = false;
    }
    if ((UnityEngine.Object) this.atlas != (UnityEngine.Object) null && this.atlas.GetSprite(this.ImageName) == null)
    {
      this.uiSprite.atlas = this.MissingAtlas;
      this.uiSprite.spriteName = this.MissingSpriteName;
      this.IsSuccess = false;
    }
    else if ((UnityEngine.Object) this.atlas != (UnityEngine.Object) null && (UnityEngine.Object) this.uiSprite != (UnityEngine.Object) null && !string.IsNullOrEmpty(this.ImageName))
    {
      this.uiSprite.atlas = this.atlas;
      this.uiSprite.spriteName = this.ImageName;
      this.IsSuccess = true;
    }
    else
    {
      this.uiSprite.atlas = this.MissingAtlas;
      this.uiSprite.spriteName = this.MissingSpriteName;
      this.IsSuccess = false;
    }
  }
}
