﻿// Decompiled with JetBrains decompiler
// Type: ResearchManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class ResearchManager
{
  private Dictionary<int, TechLevel> _techLevels = new Dictionary<int, TechLevel>();
  private Dictionary<string, int> _techLevelIDs = new Dictionary<string, int>();
  private Dictionary<int, List<TechLevel>> _sortedTechLevelsOfTechID = new Dictionary<int, List<TechLevel>>();
  private static ResearchManager _singleton;
  private TechLevel _currentTech;
  private bool _initialized;
  private Dictionary<int, List<TechLevel>> buildingInternalIDMap;

  public TechLevel GetTechLevel(int internalId)
  {
    TechLevel techLevel;
    this._techLevels.TryGetValue(internalId, out techLevel);
    return techLevel;
  }

  public bool Contains(int internalId)
  {
    return this._techLevels.ContainsKey(internalId);
  }

  public int CompleteCount { get; set; }

  public static ResearchManager inst
  {
    get
    {
      if (ResearchManager._singleton == null)
        ResearchManager._singleton = new ResearchManager();
      return ResearchManager._singleton;
    }
  }

  public string ResearchInternalId2ID(int researchId)
  {
    using (Dictionary<string, int>.KeyCollection.Enumerator enumerator = ResearchManager.inst._techLevelIDs.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        if (ResearchManager.inst._techLevelIDs[current] == researchId)
          return current;
      }
    }
    return string.Empty;
  }

  private void OnDataRemove(long itemID)
  {
    this._techLevels[(int) itemID].SetState(TechLevel.ResearchState.Available);
    this._currentTech = (TechLevel) null;
  }

  private void OnDataChange(long itemID)
  {
    int key = (int) itemID;
    ResearchData researchData = DBManager.inst.DB_Research.Get(key);
    if (researchData == null)
      return;
    long jobId = researchData.jobId;
    if (jobId > 0L)
    {
      this._techLevels[key].SetState(TechLevel.ResearchState.InProgress);
      this._currentTech = this._techLevels[key];
    }
    else
    {
      if (jobId != 0L)
        return;
      this._techLevels[key].SetState(TechLevel.ResearchState.Completed);
      ++this.CompleteCount;
      if (this._currentTech != null && key == this._currentTech.InternalID)
        this._currentTech = (TechLevel) null;
      ToastManager.Instance.AddBasicToast("toast_research_timer_complete", "Research_Name", this._techLevels[key].Name, "Level", this._techLevels[key].Level.ToString());
      AudioManager.Instance.PlaySound("sfx_city_research", false);
    }
  }

  public TechLevel CurrentResearch
  {
    get
    {
      return this._currentTech;
    }
  }

  public int Rank(Tech tech)
  {
    TechLevel techLevel = (TechLevel) null;
    if (tech != null)
    {
      Dictionary<int, TechLevel>.Enumerator enumerator = this._techLevels.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value != null && enumerator.Current.Value.Tech != null && (enumerator.Current.Value.Tech.InternalID == tech.InternalID && enumerator.Current.Value.IsCompleted))
        {
          if (techLevel == null)
            techLevel = enumerator.Current.Value;
          else if (techLevel.Level < enumerator.Current.Value.Level)
            techLevel = enumerator.Current.Value;
        }
      }
    }
    if (techLevel == null)
      return 0;
    return techLevel.Level;
  }

  public bool IsResearched(string techLevelID)
  {
    if (!this._techLevelIDs.ContainsKey(techLevelID))
      return false;
    TechLevel techLevel = this._techLevels[this._techLevelIDs[techLevelID]];
    return this.Rank(techLevel.Tech) >= techLevel.Level;
  }

  public void Init()
  {
    if (this._initialized || !GameEngine.IsAvailable)
      return;
    this.CompleteCount = 0;
    this._techLevels.Clear();
    this._techLevelIDs.Clear();
    this._sortedTechLevelsOfTechID.Clear();
    Dictionary<int, TechLevelData>.ValueCollection.Enumerator enumerator1 = ConfigManager.inst.DB_TechLevels.Values.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      this._techLevels.Add(enumerator1.Current.InternalID, new TechLevel(enumerator1.Current));
      if (!this._techLevelIDs.ContainsKey(enumerator1.Current.ID))
        this._techLevelIDs.Add(enumerator1.Current.ID, enumerator1.Current.InternalID);
    }
    using (Dictionary<long, ResearchData>.Enumerator enumerator2 = DBManager.inst.DB_Research.Datas.GetEnumerator())
    {
      while (enumerator2.MoveNext())
      {
        KeyValuePair<long, ResearchData> current = enumerator2.Current;
        if (current.Value.jobId == 0L)
        {
          this._techLevels[(int) current.Key].SetState(TechLevel.ResearchState.Completed);
          ++this.CompleteCount;
        }
        else
        {
          this._techLevels[(int) current.Key].SetState(TechLevel.ResearchState.InProgress);
          this._currentTech = this._techLevels[(int) current.Key];
        }
      }
    }
    Dictionary<int, TechLevel>.Enumerator enumerator3 = ResearchManager.inst._techLevels.GetEnumerator();
    while (enumerator3.MoveNext())
    {
      if (!this._sortedTechLevelsOfTechID.ContainsKey(enumerator3.Current.Value.Tech.InternalID))
        this._sortedTechLevelsOfTechID[enumerator3.Current.Value.Tech.InternalID] = new List<TechLevel>();
      this._sortedTechLevelsOfTechID[enumerator3.Current.Value.Tech.InternalID].Add(enumerator3.Current.Value);
      this._sortedTechLevelsOfTechID[enumerator3.Current.Value.Tech.InternalID].Sort();
    }
    DBManager.inst.DB_Research.onDataChanged += new System.Action<long>(this.OnDataChange);
    DBManager.inst.DB_Research.onDataRemove += new System.Action<long>(this.OnDataRemove);
    this._initialized = true;
  }

  public void Dispose()
  {
    if (!this._initialized)
      return;
    this._techLevels.Clear();
    this._techLevelIDs.Clear();
    this._currentTech = (TechLevel) null;
    if (this.buildingInternalIDMap != null)
    {
      this.buildingInternalIDMap.Clear();
      this.buildingInternalIDMap = (Dictionary<int, List<TechLevel>>) null;
    }
    DBManager.inst.DB_Research.onDataChanged -= new System.Action<long>(this.OnDataChange);
    DBManager.inst.DB_Research.onDataRemove -= new System.Action<long>(this.OnDataRemove);
    this._initialized = false;
    ResearchManager._singleton = (ResearchManager) null;
  }

  public Dictionary<int, Tech> TechsOfTree(int tree)
  {
    Dictionary<int, Tech> techs = ConfigManager.inst.DB_Techs.Techs;
    Dictionary<int, Tech> dictionary = new Dictionary<int, Tech>();
    using (Dictionary<int, Tech>.Enumerator enumerator = techs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, Tech> current = enumerator.Current;
        if (current.Value.tree == tree)
          dictionary.Add(current.Key, current.Value);
      }
    }
    return dictionary;
  }

  public int GetTechSteps(Tech tech)
  {
    int num = 0;
    Dictionary<int, TechLevel>.Enumerator enumerator = this._techLevels.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.Value.Tech.ID == tech.ID)
        ++num;
    }
    return num;
  }

  public TechLevel FirstLevelOfSameTechLevel(TechLevel techLevel)
  {
    if (techLevel == null)
      Debug.LogError((object) "techlevel should not be null ==============");
    using (Dictionary<int, TechLevel>.Enumerator enumerator = this._techLevels.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, TechLevel> current = enumerator.Current;
        if (current.Value.Tech.ID == techLevel.Tech.ID && current.Value.Level == 1)
          return current.Value;
      }
    }
    return (TechLevel) null;
  }

  public long GetJobId(TechLevel techLevel)
  {
    return DBManager.inst.DB_Research.Get(techLevel.InternalID).jobId;
  }

  public TechLevel NextTechLevelOfSameTech(TechLevel techLevel)
  {
    List<TechLevel> techLevelList = this._sortedTechLevelsOfTechID[techLevel.Tech.InternalID];
    if (techLevelList.Count > techLevel.Level)
      return techLevelList[techLevel.Level];
    return (TechLevel) null;
  }

  public List<TechLevel> TechLevelList(int techInternalID)
  {
    return this._sortedTechLevelsOfTechID[techInternalID];
  }

  public float CurrentBonus(TechLevel techLevel)
  {
    List<TechLevel> techLevelList = this._sortedTechLevelsOfTechID[techLevel.Tech.InternalID];
    if (techLevel.Level <= 1)
      return 0.0f;
    return techLevelList[techLevel.Level - 2].Effects[0].Modifier;
  }

  public float NextRank(TechLevel techLevel)
  {
    List<TechLevel> techLevelList = this._sortedTechLevelsOfTechID[techLevel.Tech.InternalID];
    if (techLevel.Level <= techLevelList.Count)
      return techLevelList[techLevel.Level - 1].Effects[0].Modifier;
    return 0.0f;
  }

  public int GetPowerAdded(TechLevel currentLevel)
  {
    List<TechLevel> techLevelList = this._sortedTechLevelsOfTechID[currentLevel.Tech.InternalID];
    if (currentLevel.Level > 1)
      return currentLevel.Power - techLevelList[currentLevel.Level - 2].Power;
    return currentLevel.Power;
  }

  public bool IsTheSameTech(TechLevel a, TechLevel b)
  {
    return a.Tech.InternalID == b.Tech.InternalID;
  }

  public List<TechLevel> GetUnlockTechLevel(int buildingInternalID)
  {
    if (this.buildingInternalIDMap == null)
    {
      this.buildingInternalIDMap = new Dictionary<int, List<TechLevel>>();
      using (Dictionary<int, TechLevel>.Enumerator enumerator = this._techLevels.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, TechLevel> current = enumerator.Current;
          int requiredBuilding = current.Value.RequiredBuildings[0];
          if (!this.buildingInternalIDMap.ContainsKey(requiredBuilding))
            this.buildingInternalIDMap.Add(requiredBuilding, new List<TechLevel>());
          if (current.Value.Level == 1)
            this.buildingInternalIDMap[requiredBuilding].Add(current.Value);
        }
      }
    }
    if (this.buildingInternalIDMap.ContainsKey(buildingInternalID))
      return this.buildingInternalIDMap[buildingInternalID];
    return new List<TechLevel>();
  }
}
