﻿// Decompiled with JetBrains decompiler
// Type: ConfigCasinoJackpot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigCasinoJackpot
{
  private int itemEachSlotCount = NGUITools.RandomRange(20, 25);
  private List<KeyValuePair<int, List<int>>> jackpotData = new List<KeyValuePair<int, List<int>>>();
  private List<CasinoJackpotInfo> jackpotInfoList = new List<CasinoJackpotInfo>();
  private const int JACKPOT_SLOT_COUNT = 5;
  private const float JACKPOT_SYMBOL_PERCENTAGE = 0.05f;
  private Dictionary<string, CasinoJackpotInfo> datas;
  private Dictionary<int, CasinoJackpotInfo> dicByUniqueId;

  public int JackpotSlotCount
  {
    get
    {
      return 5;
    }
  }

  public int ItemEachSlotCount
  {
    get
    {
      return this.itemEachSlotCount;
    }
  }

  public int CostCoins2Play
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("jackpot_cost_gold");
      if (data != null)
        return data.ValueInt;
      return 0;
    }
  }

  public int CostProps2Play
  {
    get
    {
      return 1;
    }
  }

  public List<KeyValuePair<int, List<int>>> JackpotData
  {
    get
    {
      return this.jackpotData;
    }
  }

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<CasinoJackpotInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    Dictionary<string, CasinoJackpotInfo>.ValueCollection.Enumerator i = this.datas.Values.GetEnumerator();
    while (i.MoveNext())
    {
      if (i.Current != null && !string.IsNullOrEmpty(i.Current.id) && (i.Current.rewardId != 0 && this.jackpotInfoList.Find((Predicate<CasinoJackpotInfo>) (x => x.rewardId == i.Current.rewardId)) == null))
        this.jackpotInfoList.Add(i.Current);
    }
    int num = Mathf.CeilToInt(0.05263158f * (float) this.jackpotInfoList.Count);
    for (int index = 0; index < num; ++index)
      this.jackpotInfoList.Add(new CasinoJackpotInfo()
      {
        id = "0",
        rewardId = -1
      });
    this.jackpotInfoList.Sort(new Comparison<CasinoJackpotInfo>(this.SortByID));
  }

  public List<CasinoJackpotInfo> GetCasinoJackpotInfoList()
  {
    return this.jackpotInfoList;
  }

  public int SortByID(CasinoJackpotInfo a, CasinoJackpotInfo b)
  {
    return int.Parse(a.id).CompareTo(int.Parse(b.id));
  }

  public List<KeyValuePair<int, List<int>>> GetJackpotFakeData()
  {
    this.jackpotData.Clear();
    for (int key = 0; key < 5; ++key)
    {
      List<int> intList = new List<int>();
      for (int index = 0; index < this.itemEachSlotCount + key; ++index)
        intList.Add(this.jackpotInfoList[NGUITools.RandomRange(0, this.jackpotInfoList.Count - 1)].rewardId);
      this.jackpotData.Add(new KeyValuePair<int, List<int>>(key, intList));
    }
    return this.jackpotData;
  }

  public List<KeyValuePair<int, List<int>>> GetJackpotData(int type, int winRewardId = 0)
  {
    List<int> intList1 = new List<int>()
    {
      0,
      0,
      0,
      0,
      0
    };
    List<int> intList2 = new List<int>()
    {
      0,
      1,
      2,
      3,
      4
    };
    List<KeyValuePair<int, List<int>>> jackpotFakeData = this.GetJackpotFakeData();
    List<CasinoJackpotInfo> casinoJackpotInfoList = new List<CasinoJackpotInfo>((IEnumerable<CasinoJackpotInfo>) this.jackpotInfoList.ToArray());
    if (winRewardId != 0)
    {
      CasinoJackpotInfo casinoJackpotInfo = casinoJackpotInfoList.Find((Predicate<CasinoJackpotInfo>) (x => x.rewardId == winRewardId));
      if (casinoJackpotInfo != null)
        casinoJackpotInfoList.Remove(casinoJackpotInfo);
    }
    for (int index1 = 0; index1 < 5; ++index1)
    {
      int index2 = NGUITools.RandomRange(0, intList2.Count - 1);
      switch (type)
      {
        case 0:
          int index3 = NGUITools.RandomRange(0, casinoJackpotInfoList.Count - 1);
          int noWinRewardId = casinoJackpotInfoList[index3].rewardId;
          List<int> all = intList1.FindAll((Predicate<int>) (x => x == noWinRewardId));
          if (all != null && all.Count < 2 || all == null)
          {
            intList1[intList2[index2]] = noWinRewardId;
            intList2.RemoveAt(index2);
            break;
          }
          casinoJackpotInfoList.RemoveAt(index3);
          --index1;
          break;
        case 1:
          intList1[intList2[index2]] = index1 > 2 ? casinoJackpotInfoList[NGUITools.RandomRange(0, casinoJackpotInfoList.Count - 1)].rewardId : winRewardId;
          intList2.RemoveAt(index2);
          break;
        case 2:
          intList1[intList2[index2]] = winRewardId;
          intList2.RemoveAt(index2);
          break;
        case 3:
          winRewardId = winRewardId != 0 ? winRewardId : -1;
          intList1[intList2[index2]] = winRewardId;
          intList2.RemoveAt(index2);
          break;
      }
    }
    for (int i = 0; i < 5; ++i)
    {
      KeyValuePair<int, List<int>> keyValuePair = jackpotFakeData.Find((Predicate<KeyValuePair<int, List<int>>>) (x => x.Key == i));
      List<int> intList3 = keyValuePair.Value;
      intList3.Add(intList1[i]);
      jackpotFakeData.Remove(keyValuePair);
      jackpotFakeData.Add(new KeyValuePair<int, List<int>>(i, intList3));
    }
    jackpotFakeData.Sort((Comparison<KeyValuePair<int, List<int>>>) ((a, b) => a.Key.CompareTo(b.Key)));
    return jackpotFakeData;
  }
}
