﻿// Decompiled with JetBrains decompiler
// Type: ConfigVerificationReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigVerificationReward
{
  private ConfigParse parse = new ConfigParse();
  public Dictionary<string, VerificationRewardInfo> datas;
  private Dictionary<int, VerificationRewardInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<VerificationRewardInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public Dictionary<int, int> GetTotalRewards()
  {
    Dictionary<int, int> dictionary = new Dictionary<int, int>();
    Dictionary<string, VerificationRewardInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.Rewards != null)
      {
        List<Reward.RewardsValuePair> rewards = enumerator.Current.Rewards.GetRewards();
        for (int index = 0; index < rewards.Count; ++index)
          dictionary.Add(rewards[index].internalID, rewards[index].value);
      }
    }
    return dictionary;
  }

  public List<int> GetTotalItems()
  {
    List<int> intList = new List<int>();
    Dictionary<string, VerificationRewardInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.Rewards != null)
      {
        List<Reward.RewardsValuePair> rewards = enumerator.Current.Rewards.GetRewards();
        for (int index = 0; index < rewards.Count; ++index)
          intList.Add(rewards[index].internalID);
      }
    }
    intList.Sort();
    return intList;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, VerificationRewardInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, VerificationRewardInfo>) null;
  }

  public bool Contains(int internalId)
  {
    return this.dicByUniqueId.ContainsKey(internalId);
  }

  public bool Contains(string id)
  {
    return this.datas.ContainsKey(id);
  }

  public VerificationRewardInfo GetItem(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (VerificationRewardInfo) null;
  }

  public VerificationRewardInfo GetItem(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (VerificationRewardInfo) null;
  }
}
