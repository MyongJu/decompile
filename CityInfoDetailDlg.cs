﻿// Decompiled with JetBrains decompiler
// Type: CityInfoDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class CityInfoDetailDlg : Popup
{
  public CityInfoDetailDlg.CombatBonuses mCombatBonuses = new CityInfoDetailDlg.CombatBonuses();
  public CityInfoDetailDlg.MyHospital mMyHospital = new CityInfoDetailDlg.MyHospital();
  public CityInfoDetailDlg.ResourcesSubPage mResourcesSubPage = new CityInfoDetailDlg.ResourcesSubPage();
  public CityInfoDetailDlg.ResourcesSubPage mOtherResourcesSubPage = new CityInfoDetailDlg.ResourcesSubPage();
  public GameObject mCombatPanel;
  public GameObject mFoodPanel;
  public GameObject mWoodPanel;
  public GameObject mSilverPanel;
  public GameObject mOrePanel;
  public GameObject mHospitalPanel;
  public GameObject mResDetailPanel;
  public UIScrollView combatScrollView;
  public UIScrollView foodScrollView;
  public UIScrollView otherResScrollView;
  private CityInfoDetailDlg.DLGType dt;

  public void Show()
  {
    this.Reset();
    switch (this.dt)
    {
      case CityInfoDetailDlg.DLGType.combat:
        this.FillCombatData();
        break;
      case CityInfoDetailDlg.DLGType.food:
        this.FillFoodData();
        break;
      case CityInfoDetailDlg.DLGType.wood:
        this.FillWoodData();
        break;
      case CityInfoDetailDlg.DLGType.ore:
        this.FillOreData();
        break;
      case CityInfoDetailDlg.DLGType.silver:
        this.FillSilverData();
        break;
    }
  }

  public void FillCombatData()
  {
    this.mCombatPanel.SetActive(true);
    this.mFoodPanel.SetActive(false);
    this.mWoodPanel.SetActive(false);
    this.mHospitalPanel.SetActive(true);
    this.mResDetailPanel.SetActive(false);
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    BuildingInfo data1 = ConfigManager.inst.DB_Building.GetData("stronghold", CityManager.inst.GetHighestBuildingLevelFor("stronghold"));
    this.mCombatBonuses.mTotalTroops.text = Utils.FormatThousands(DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).totalTroops.totalTroopsCount.ToString());
    this.mCombatBonuses.mTroopMarches.text = data1.Benefit_Value_2.ToString();
    this.mCombatBonuses.mTroopsPerMarch.text = data1.Benefit_Value_1.ToString();
    this.mCombatBonuses.mMaxResourceLoad.text = cityData.totalTroops.totalLoad.ToString();
    BuildingInfo data2 = ConfigManager.inst.DB_Building.GetData("walls", CityManager.inst.GetHighestBuildingLevelFor("walls"));
    double num1 = (double) Property.EffectiveValue((float) data2.Benefit_Value_2, data2.Benefit_ID_2, 0, Effect.ConditionType.all);
    this.mCombatBonuses.mTraps.text = Utils.FormatThousands(DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).totalTroops.totalTrapsCount.ToString());
    this.mCombatBonuses.mWallTrapCapacity.text = Utils.FormatThousands(num1.ToString());
    this.mCombatBonuses.mUpKeep.text = Utils.FormatThousands(cityData.totalTroops.totalUpkeep.ToString());
    List<long> targetListByType = DBManager.inst.DB_March.GetTargetListByType(MarchData.MarchType.reinforce);
    int num2 = 0;
    using (List<long>.Enumerator enumerator = targetListByType.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MarchData marchData = DBManager.inst.DB_March.Get(enumerator.Current);
        num2 += marchData.troopsInfo.totalCount;
      }
    }
    this.mCombatBonuses.mReinforcement.text = Utils.ConvertNumberToNormalString(num2);
    float y = this.mMyHospital.mTroopItemPrefab.transform.localPosition.y;
    float height = (float) this.mMyHospital.mTroopItemPrefab.GetComponent<UIWidget>().height;
    GameObject mTroopItemPrefab = this.mMyHospital.mTroopItemPrefab;
    using (Dictionary<string, CityHealingTroopInfo>.Enumerator enumerator = cityData.hospitalTroops.healingTroops.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, CityHealingTroopInfo> current = enumerator.Current;
        long totalCount = cityData.hospitalTroops.GetTotalCount(current.Key);
        if (totalCount != 0L)
        {
          Unit_StatisticsInfo data3 = ConfigManager.inst.DB_Unit_Statistics.GetData(current.Key);
          GameObject gob = PrefabManagerEx.Instance.Spawn(mTroopItemPrefab, mTroopItemPrefab.transform.parent);
          gob.SetActive(true);
          gob.transform.localPosition = mTroopItemPrefab.transform.localPosition;
          Utils.SetLPY(gob, y);
          y -= height;
          gob.GetComponent<StrongholdInfoHospitalItem>().SetDetails(ScriptLocalization.Get(data3.Troop_Name_LOC_ID, true) + ":", totalCount);
          this.mMyHospital.mTroopInfo.Add(gob);
        }
      }
    }
    this.mCombatBonuses.mHospitalized.text = Utils.FormatThousands(cityData.hospitalTroops.totalTroopsCount.ToString());
    this.mMyHospital.mWounded.text = Utils.FormatThousands(cityData.hospitalTroops.totalTroopsCount.ToString());
  }

  public void FillFoodData()
  {
    this.mCombatPanel.SetActive(false);
    this.mFoodPanel.SetActive(true);
    this.mWoodPanel.SetActive(false);
    this.mHospitalPanel.SetActive(false);
    this.mResDetailPanel.SetActive(true);
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    this.mResourcesSubPage.mYouOwn.text = Utils.FormatThousands(((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD)).ToString());
    this.mResourcesSubPage.mUpKeep.text = string.Format("[D00000]{0}[-]", (object) Utils.FormatThousands(cityData.totalTroops.totalUpkeep.ToString()));
    this.mResourcesSubPage.mOverallIncome.text = Utils.FormatThousands(((long) CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.FOOD)).ToString());
    this.mResourcesSubPage.mCityCapacity.text = Utils.FormatThousands(CityManager.inst.GetTotalCapacityForResourceOfType(CityManager.ResourceTypes.FOOD).ToString());
    BenefitInfo benefitInfo = DBManager.inst.DB_Local_Benefit.Get("prop_food_generation_percent");
    this.mResourcesSubPage.mVIPResouceBonus.text = ((double) benefitInfo.vip.value * 100.0).ToString() + "%";
    this.mResourcesSubPage.mHeroResourceBonus.text = ((double) benefitInfo.GetHeroBenefit() * 100.0).ToString() + "%";
    this.mResourcesSubPage.mItemResourceBonus.text = ((double) benefitInfo.GetItemBenefit() * 100.0).ToString() + "%";
    this.mResourcesSubPage.mResearchBonus.text = ((double) benefitInfo.research.value * 100.0).ToString() + "%";
    this.mResourcesSubPage.mTotalBonus.text = ((double) benefitInfo.total * 100.0).ToString() + "%";
    this.PropergateBuildingList("farm");
  }

  public void FillWoodData()
  {
    this.mCombatPanel.SetActive(false);
    this.mFoodPanel.SetActive(false);
    this.mWoodPanel.SetActive(true);
    this.mHospitalPanel.SetActive(false);
    this.mResDetailPanel.SetActive(true);
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    this.mOtherResourcesSubPage.mTitle.text = Utils.XLAT("stronghold_cityinfo_uppercase_wood_bonuses_name");
    this.mOtherResourcesSubPage.mSubTitle.text = Utils.XLAT("stronghold_cityinfo_all_current_wood_bonuses");
    this.mOtherResourcesSubPage.mOwnedTitle.text = Utils.XLAT("stronghold_cityinfo_uppercase_sawmills_owned");
    this.mOtherResourcesSubPage.mResourceIcon.spriteName = ConfigManager.inst.DB_Lookup.GetImage("wood");
    this.mOtherResourcesSubPage.mYouOwn.text = Utils.FormatThousands(((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD)).ToString());
    this.mOtherResourcesSubPage.mOverallIncome.text = Utils.FormatThousands(((long) CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.WOOD)).ToString());
    this.mOtherResourcesSubPage.mCityCapacity.text = Utils.FormatThousands(CityManager.inst.GetTotalCapacityForResourceOfType(CityManager.ResourceTypes.WOOD).ToString());
    BenefitInfo benefitInfo = DBManager.inst.DB_Local_Benefit.Get("prop_wood_generation_percent");
    this.mOtherResourcesSubPage.mVIPResouceBonus.text = ((double) benefitInfo.vip.value * 100.0).ToString() + "%";
    this.mOtherResourcesSubPage.mHeroResourceBonus.text = ((double) benefitInfo.GetHeroBenefit() * 100.0).ToString() + "%";
    this.mOtherResourcesSubPage.mItemResourceBonus.text = ((double) benefitInfo.GetItemBenefit() * 100.0).ToString() + "%";
    this.mOtherResourcesSubPage.mResearchBonus.text = ((double) benefitInfo.research.value * 100.0).ToString() + "%";
    this.mOtherResourcesSubPage.mTotalBonus.text = ((double) benefitInfo.total * 100.0).ToString() + "%";
    this.PropergateBuildingList("lumber_mill");
  }

  public void FillSilverData()
  {
    this.mCombatPanel.SetActive(false);
    this.mFoodPanel.SetActive(false);
    this.mWoodPanel.SetActive(true);
    this.mHospitalPanel.SetActive(false);
    this.mResDetailPanel.SetActive(true);
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    this.mOtherResourcesSubPage.mTitle.text = Utils.XLAT("stronghold_cityinfo_uppercase_silver_bonuses_name");
    this.mOtherResourcesSubPage.mSubTitle.text = Utils.XLAT("stronghold_cityinfo_all_current_silver_bonuses");
    this.mOtherResourcesSubPage.mOwnedTitle.text = Utils.XLAT("stronghold_cityinfo_uppercase_houses_owned");
    this.mOtherResourcesSubPage.mResourceIcon.spriteName = ConfigManager.inst.DB_Lookup.GetImage("silver");
    this.mOtherResourcesSubPage.mYouOwn.text = Utils.FormatThousands(((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER)).ToString());
    this.mOtherResourcesSubPage.mOverallIncome.text = Utils.FormatThousands(((long) CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.SILVER)).ToString());
    this.mOtherResourcesSubPage.mCityCapacity.text = Utils.FormatThousands(CityManager.inst.GetTotalCapacityForResourceOfType(CityManager.ResourceTypes.SILVER).ToString());
    BenefitInfo benefitInfo = DBManager.inst.DB_Local_Benefit.Get("prop_silver_generation_percent");
    this.mOtherResourcesSubPage.mVIPResouceBonus.text = ((double) benefitInfo.vip.value * 100.0).ToString() + "%";
    this.mOtherResourcesSubPage.mHeroResourceBonus.text = ((double) benefitInfo.GetHeroBenefit() * 100.0).ToString() + "%";
    this.mOtherResourcesSubPage.mItemResourceBonus.text = ((double) benefitInfo.GetItemBenefit() * 100.0).ToString() + "%";
    this.mOtherResourcesSubPage.mResearchBonus.text = ((double) benefitInfo.research.value * 100.0).ToString() + "%";
    this.mOtherResourcesSubPage.mTotalBonus.text = ((double) benefitInfo.total * 100.0).ToString() + "%";
    this.PropergateBuildingList("house");
  }

  public void FillOreData()
  {
    this.mCombatPanel.SetActive(false);
    this.mFoodPanel.SetActive(false);
    this.mWoodPanel.SetActive(true);
    this.mHospitalPanel.SetActive(false);
    this.mResDetailPanel.SetActive(true);
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    this.mOtherResourcesSubPage.mTitle.text = Utils.XLAT("stronghold_cityinfo_uppercase_ore_bonuses_name");
    this.mOtherResourcesSubPage.mSubTitle.text = Utils.XLAT("stronghold_cityinfo_all_current_ore_bonuses");
    this.mOtherResourcesSubPage.mOwnedTitle.text = Utils.XLAT("stronghold_cityinfo_uppercase_mines_owned");
    this.mOtherResourcesSubPage.mResourceIcon.spriteName = ConfigManager.inst.DB_Lookup.GetImage("ore");
    this.mOtherResourcesSubPage.mYouOwn.text = Utils.FormatThousands(((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE)).ToString());
    this.mOtherResourcesSubPage.mOverallIncome.text = Utils.FormatThousands(((long) CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.ORE)).ToString());
    this.mOtherResourcesSubPage.mCityCapacity.text = Utils.FormatThousands(CityManager.inst.GetTotalCapacityForResourceOfType(CityManager.ResourceTypes.ORE).ToString());
    BenefitInfo benefitInfo = DBManager.inst.DB_Local_Benefit.Get("prop_ore_generation_percent");
    this.mOtherResourcesSubPage.mVIPResouceBonus.text = ((double) benefitInfo.vip.value * 100.0).ToString() + "%";
    this.mOtherResourcesSubPage.mHeroResourceBonus.text = ((double) benefitInfo.GetHeroBenefit() * 100.0).ToString() + "%";
    this.mOtherResourcesSubPage.mItemResourceBonus.text = ((double) benefitInfo.GetItemBenefit() * 100.0).ToString() + "%";
    this.mOtherResourcesSubPage.mResearchBonus.text = ((double) benefitInfo.research.value * 100.0).ToString() + "%";
    this.mOtherResourcesSubPage.mTotalBonus.text = ((double) benefitInfo.total * 100.0).ToString() + "%";
    this.PropergateBuildingList("mine");
  }

  public void PropergateBuildingList(string buildingType)
  {
    ArrayList buildings = new ArrayList();
    CityManager.inst.GetBuildingLevelDataOfType(buildingType, buildings);
    float y = this.mResourcesSubPage.mResourceBuildingPrefab.transform.localPosition.y;
    float height = (float) this.mResourcesSubPage.mResourceBuildingPrefab.GetComponent<UIWidget>().height;
    GameObject resourceBuildingPrefab = this.mResourcesSubPage.mResourceBuildingPrefab;
    foreach (StrongholdInfoDlg.BuildingLevelInfo buildingLevelInfo in buildings)
    {
      GameObject gob = PrefabManagerEx.Instance.Spawn(resourceBuildingPrefab, resourceBuildingPrefab.transform.parent);
      gob.SetActive(true);
      gob.transform.localPosition = resourceBuildingPrefab.transform.localPosition;
      Utils.SetLPY(gob, y);
      y -= height;
      gob.GetComponent<BuildInfoResourceStatItem>().SetDetails(Utils.XLAT(buildingLevelInfo.mName + "_name"), buildingLevelInfo.mProductionRate, buildingLevelInfo.mLevel);
      this.mResourcesSubPage.mBuildingItems.Add(gob);
    }
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  private void OnResourcesUpdated(long food, long wood, long silver, long ore, long gold)
  {
    this.Show();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.dt = (orgParam as CityInfoDetailDlg.Parameter).type;
    this.Init();
    this.Show();
    CityManager.inst.onResourcesUpdated += new CityManager.OnResourcesUpdated(this.OnResourcesUpdated);
  }

  private void Init()
  {
  }

  private void Reset()
  {
    for (int index = this.mResourcesSubPage.mBuildingItems.Count - 1; index >= 0; --index)
      PrefabManagerEx.Instance.Destroy(this.mResourcesSubPage.mBuildingItems[index]);
    this.mResourcesSubPage.mBuildingItems.Clear();
    for (int index = this.mMyHospital.mTroopInfo.Count - 1; index >= 0; --index)
      PrefabManagerEx.Instance.Destroy(this.mMyHospital.mTroopInfo[index]);
    this.mMyHospital.mTroopInfo.Clear();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
    CityManager.inst.onResourcesUpdated -= new CityManager.OnResourcesUpdated(this.OnResourcesUpdated);
  }

  [Serializable]
  public class CombatBonuses
  {
    public UILabel mTotalTroops;
    public UILabel mTroopMarches;
    public UILabel mTroopsPerMarch;
    public UILabel mMaxResourceLoad;
    public UILabel mUpKeep;
    public UILabel mTraps;
    public UILabel mWallTrapCapacity;
    public UILabel mReinforcement;
    public UILabel mHospitalized;
  }

  [Serializable]
  public class MyHospital
  {
    [HideInInspector]
    public List<GameObject> mTroopInfo = new List<GameObject>();
    public UILabel mWounded;
    public UITable mHospitalTable;
    public GameObject mTroopItemPrefab;
  }

  [Serializable]
  public class ResourcesSubPage
  {
    [HideInInspector]
    public List<GameObject> mBuildingItems = new List<GameObject>();
    public UISprite mResourceIcon;
    public UILabel mTitle;
    public UILabel mSubTitle;
    public UILabel mOwnedTitle;
    public UILabel mOverallIncome;
    public UILabel mYouOwn;
    public UILabel mCityCapacity;
    public UILabel mUpKeep;
    public UILabel mVIPResouceBonus;
    public UILabel mHeroResourceBonus;
    public UILabel mItemResourceBonus;
    public UILabel mResearchBonus;
    public UILabel mTotalBonus;
    public GameObject mResourceBuildingPrefab;
  }

  public enum DLGType
  {
    combat,
    food,
    wood,
    ore,
    silver,
  }

  public class Parameter : Popup.PopupParameter
  {
    public CityInfoDetailDlg.DLGType type;
  }
}
