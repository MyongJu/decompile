﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightSkillStageRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class DragonKnightSkillStageRenderer : MonoBehaviour
{
  private Dictionary<int, DragonKnightSkillItemRenderer> _itemDict = new Dictionary<int, DragonKnightSkillItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public GameObject masteredNode;
  public GameObject lockedNode;
  public UILabel skillStageTitle;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  public UIGrid grid;
  private List<DragonKnightSkillInfo> _stageInfo;

  public void SetData(List<DragonKnightSkillInfo> stageInfo)
  {
    this._stageInfo = stageInfo;
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.ShowStageContent();
    this.ShowSkillContent();
  }

  public void ClearData()
  {
    using (Dictionary<int, DragonKnightSkillItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.grid.repositionNow = true;
    this.grid.Reposition();
  }

  private DragonKnightSkillItemRenderer CreateItemRenderer(DragonKnightSkillInfo skillInfo)
  {
    GameObject gameObject = this._itemPool.AddChild(this.grid.gameObject);
    gameObject.SetActive(true);
    DragonKnightSkillItemRenderer component = gameObject.GetComponent<DragonKnightSkillItemRenderer>();
    component.SetData(skillInfo);
    return component;
  }

  private void ShowSkillContent()
  {
    this.ClearData();
    if (this._stageInfo != null)
    {
      for (int key = 0; key < this._stageInfo.Count; ++key)
      {
        DragonKnightSkillItemRenderer itemRenderer = this.CreateItemRenderer(this._stageInfo[key]);
        this._itemDict.Add(key, itemRenderer);
      }
    }
    this.Reposition();
  }

  private void ShowStageContent()
  {
    if (this._stageInfo == null || this._stageInfo.Count <= 0)
      return;
    this.skillStageTitle.text = ScriptLocalization.GetWithPara("dragon_knight_skill_slot_title", new Dictionary<string, string>()
    {
      {
        "1",
        this._stageInfo[0].CurrentStage.ToString()
      },
      {
        "2",
        this._stageInfo[0].CurrentStageMaxLevel.ToString()
      }
    }, true);
    NGUITools.SetActive(this.masteredNode, ConfigManager.inst.DB_DragonKnightSkill.IsStageSkillsMastered(this._stageInfo[0].CurrentStage));
    NGUITools.SetActive(this.lockedNode, ConfigManager.inst.DB_DragonKnightSkill.IsStageSkillsLocked(this._stageInfo[0].CurrentStage));
  }
}
