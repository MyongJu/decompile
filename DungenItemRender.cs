﻿// Decompiled with JetBrains decompiler
// Type: DungenItemRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DungenItemRender : ComponentRenderBase
{
  private bool _selected = true;
  public UISprite selectedImage;
  public UITexture itemImage;
  public UILabel amountLabel;
  public GameObject lockedObj;
  public GameObject unlockedObj;
  public System.Action<DungenItemRender> OnSelectedItemDelegate;
  public int index;
  public DungenItemRenderData renderData;

  public override void Dispose()
  {
    base.Dispose();
    this.OnSelectedItemDelegate = (System.Action<DungenItemRender>) null;
  }

  public override void Init()
  {
    this.renderData = this.data as DungenItemRenderData;
    if (this.renderData.locked)
    {
      this.lockedObj.SetActive(true);
      this.unlockedObj.SetActive(false);
    }
    else
    {
      this.lockedObj.SetActive(false);
      this.unlockedObj.SetActive(true);
      if (this.renderData.bagInfo != null)
      {
        BuilderFactory.Instance.HandyBuild((UIWidget) this.itemImage, ConfigManager.inst.DB_Items.GetItem(this.renderData.bagInfo.itemId).ImagePath, (System.Action<bool>) null, true, false, string.Empty);
        this.amountLabel.text = this.renderData.bagInfo.count.ToString();
      }
      else
        this.unlockedObj.SetActive(false);
    }
  }

  public bool Selected
  {
    get
    {
      return this._selected;
    }
    set
    {
      if (this._selected == value)
        return;
      this._selected = value;
      NGUITools.SetActive(this.selectedImage.gameObject, this._selected);
    }
  }

  public int ItemID
  {
    get
    {
      return (int) (this.data as IconData).Data;
    }
  }

  public void OnItemClickHandler()
  {
    if (this.renderData.bagInfo == null || this.OnSelectedItemDelegate == null)
      return;
    this.Selected = !this.Selected;
    this.OnSelectedItemDelegate(this);
  }
}
