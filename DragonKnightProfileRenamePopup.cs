﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightProfileRenamePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using UI;

public class DragonKnightProfileRenamePopup : Popup
{
  public const string POPUP_TYPE = "DragonKnight/DragonKnightProfileRenamePopup";
  private const string COST_SHOPITEM_NAME = "shopitem_dragon_knight_rename";
  private const int MAX_LETTER_NUM = 10;
  public UILabel mPropsOwnedValue;
  public UILabel mCharsRemainingValue;
  public UILabel mEnterNameLabel;
  public UIInput mPlayerNameInput;
  public UILabel mUseBtnValue;
  public UILabel mGetAndUseBtnValue;
  public UILabel mPlayerNameInputLabel;
  public UIButton mUseBtn;
  public UIButton mGetAndUseBtn;
  public UISprite mBusySprite;
  public UISprite mStatusSprite;
  public UILabel mErrorMsg;
  public UILabel leastLabel;
  public UILabel tipLabel;
  public UILabel title;
  private IEnumerator _CR;
  private int _itemsAvailable;
  private bool _ignoreInput;
  private System.Action closeCallBack;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    DragonKnightProfileRenamePopup.Parameter parameter = orgParam as DragonKnightProfileRenamePopup.Parameter;
    if (parameter != null)
      this.closeCallBack = parameter.closeCallBack;
    this.SetDetails();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    if (this.closeCallBack != null)
      this.closeCallBack();
    this.mUseBtn.isEnabled = true;
    this.mGetAndUseBtn.isEnabled = true;
    this.closeCallBack = (System.Action) null;
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void SetDetails()
  {
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    this._ignoreInput = true;
    this.mPlayerNameInput.value = string.Empty;
    this.mPlayerNameInput.defaultText = Utils.XLAT(dragonKnightData.Name);
    Utils.ExecuteInSecs(0.1f, (System.Action) (() => this._ignoreInput = false));
    this.leastLabel.text = Utils.XLAT("rename_leastchar");
    this.tipLabel.text = Utils.XLAT("dragon_knight_rename_tip");
    this.title.text = Utils.XLAT("dragon_knight_rename_title");
    this._itemsAvailable = ItemBag.Instance.GetItemCountByShopID("shopitem_dragon_knight_rename");
    int shopItemPrice = ItemBag.Instance.GetShopItemPrice("shopitem_dragon_knight_rename");
    ConfigManager.inst.DB_Items.GetItem(ConfigManager.inst.DB_Shop.GetShopData("shopitem_dragon_knight_rename").Item_InternalId);
    this.mPropsOwnedValue.text = this._itemsAvailable.ToString();
    if (this._itemsAvailable > 0)
    {
      this.mUseBtn.gameObject.SetActive(true);
      this.mGetAndUseBtn.gameObject.SetActive(false);
    }
    else
    {
      this.mUseBtn.gameObject.SetActive(false);
      this.mGetAndUseBtn.gameObject.SetActive(true);
    }
    this.mUseBtnValue.text = "1";
    Utils.SetPriceToLabel(this.mGetAndUseBtnValue, shopItemPrice);
    this.mCharsRemainingValue.text = "0/" + (object) 10;
    this.mStatusSprite.spriteName = "red_cross";
    this.mBusySprite.gameObject.SetActive(false);
    this.mUseBtn.isEnabled = false;
    this.mGetAndUseBtn.isEnabled = false;
    this.mCharsRemainingValue.gameObject.SetActive(false);
    this.mStatusSprite.gameObject.SetActive(false);
    this.mErrorMsg.gameObject.SetActive(false);
    this.mPlayerNameInput.onValidate = new UIInput.OnValidate(this.InputValidator);
  }

  private char InputValidator(string text, int charIndex, char addedChar)
  {
    if (this.isValidateCharacter(addedChar))
      return addedChar;
    return char.MinValue;
  }

  private bool isValidateCharacter(char codePoint)
  {
    return char.GetUnicodeCategory(codePoint) != UnicodeCategory.Surrogate;
  }

  public void OnCloseBtn()
  {
    this.gameObject.SetActive(false);
  }

  public void OnInputChanged()
  {
    if (this._ignoreInput)
      return;
    this.mCharsRemainingValue.gameObject.SetActive(true);
    this.mStatusSprite.gameObject.SetActive(true);
    this.mErrorMsg.gameObject.SetActive(false);
    this.mPlayerNameInput.value = this.mPlayerNameInput.value.Replace("[", "【");
    this.mPlayerNameInput.value = this.mPlayerNameInput.value.Replace("]", "】");
    this.mPlayerNameInput.value = this.mPlayerNameInput.value.Replace("\n", string.Empty);
    byte[] bytes = Encoding.UTF8.GetBytes(this.mPlayerNameInput.value);
    this.mStatusSprite.spriteName = "red_cross";
    this.mCharsRemainingValue.text = string.Format("{0}/" + (object) 10, (object) bytes.Length);
    this.mCharsRemainingValue.gameObject.SetActive(false);
    this.mUseBtn.isEnabled = false;
    this.mGetAndUseBtn.isEnabled = false;
    this.mErrorMsg.gameObject.SetActive(false);
    if (this._CR != null)
      this.StopCoroutine(this._CR);
    this.StartCoroutine(this._CR = this.CheckName());
  }

  [DebuggerHidden]
  private IEnumerator CheckName()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DragonKnightProfileRenamePopup.\u003CCheckName\u003Ec__Iterator5B()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void CheckCityCallback(bool ret, object data)
  {
    if (ret)
    {
      this.mStatusSprite.gameObject.SetActive(true);
      this.mBusySprite.gameObject.SetActive(false);
      this.mStatusSprite.spriteName = "green_tick";
      if (this._itemsAvailable > 0)
        this.mUseBtn.isEnabled = true;
      else
        this.mGetAndUseBtn.isEnabled = true;
    }
    else
    {
      this.mStatusSprite.gameObject.SetActive(true);
      this.mBusySprite.gameObject.SetActive(false);
      this.mStatusSprite.spriteName = "red_cross";
      this.mErrorMsg.gameObject.SetActive(true);
      Hashtable hashtable = data as Hashtable;
      this.mErrorMsg.text = Utils.XLAT("id_invalid_name");
    }
  }

  public void OnUseBtnPressed()
  {
    bool flag = false;
    int itemInternalId = ConfigManager.inst.DB_Shop.GetShopData("shopitem_dragon_knight_rename").Item_InternalId;
    this.mUseBtn.isEnabled = false;
    Hashtable extra = Utils.Hash((object) "buy", (object) flag, (object) "name", (object) this.mPlayerNameInput.value);
    ItemBag.Instance.UseItem(itemInternalId, 1, extra, (System.Action<bool, object>) ((_param1, _param2) =>
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_player_profile_name_change"), (System.Action) null, 4f, false);
      this.OnCloseBtnClick();
    }));
  }

  public void OnGetAndUseBtnPressed()
  {
    bool flag = true;
    this.mGetAndUseBtn.isEnabled = false;
    Hashtable extra = Utils.Hash((object) "buy", (object) flag, (object) "name", (object) this.mPlayerNameInput.value);
    if (ItemBag.Instance.GetShopItemPrice("shopitem_dragon_knight_rename") > PlayerData.inst.hostPlayer.Currency)
    {
      Utils.ShowNotEnoughGoldTip();
      this.OnCloseBtnClick();
    }
    else
      ItemBag.Instance.BuyAndUseShopItem("shopitem_dragon_knight_rename", extra, (System.Action<bool, object>) ((_param1, _param2) =>
      {
        this.OnCloseBtnClick();
        UIManager.inst.toast.Show(Utils.XLAT("toast_player_profile_name_change"), (System.Action) null, 4f, false);
      }), 1);
  }

  public class Parameter : Popup.PopupParameter
  {
    public System.Action closeCallBack;
  }
}
