﻿// Decompiled with JetBrains decompiler
// Type: FallenKnightAllianceRankPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class FallenKnightAllianceRankPopup : Popup
{
  private Dictionary<string, FallenKnightRankListItemRenderer> itemDict = new Dictionary<string, FallenKnightRankListItemRenderer>();
  private GameObjectPool itemPool = new GameObjectPool();
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  public UILabel noRankTipInfo;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.ShowRankInfoList();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void ShowRankInfoList()
  {
    this.ClearData();
    ArrayList data1 = new ArrayList();
    Hashtable history = ActivityManager.Intance.knightData.history;
    int num = 0;
    List<KeyValuePair<int, ArrayList>> keyValuePairList = new List<KeyValuePair<int, ArrayList>>();
    if (history != null && history.Count > 0)
      DatabaseTools.CheckAndParseOrgData(history[(object) "alliance_history"], out data1);
    if (data1 != null && data1.Count > 0)
    {
      foreach (object obj in data1)
      {
        Hashtable hashtable = obj as Hashtable;
        if (hashtable != null)
        {
          foreach (object key in (IEnumerable) hashtable.Keys)
          {
            string s = key.ToString();
            ArrayList data2 = new ArrayList();
            int result = 0;
            int.TryParse(s, out result);
            DatabaseTools.CheckAndParseOrgData(hashtable[(object) s], out data2);
            if (data2 != null)
              keyValuePairList.Add(new KeyValuePair<int, ArrayList>(result, data2));
          }
        }
      }
      keyValuePairList.Sort(new Comparison<KeyValuePair<int, ArrayList>>(this.SortByRankTime));
      for (int index = 0; index < keyValuePairList.Count; ++index)
      {
        FallenKnightRankListItemRenderer itemRenderer = this.CreateItemRenderer(0, keyValuePairList[index].Value, keyValuePairList[index].Key.ToString());
        this.itemDict.Add(keyValuePairList[index].Key.ToString() + (object) num++, itemRenderer);
      }
    }
    else
      this.noRankTipInfo.gameObject.SetActive(true);
    this.Reposition();
  }

  private FallenKnightRankListItemRenderer CreateItemRenderer(int type, ArrayList rankList, string rankTime)
  {
    GameObject gameObject = this.itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    FallenKnightRankListItemRenderer component = gameObject.GetComponent<FallenKnightRankListItemRenderer>();
    component.SetData(type, rankList, rankTime);
    return component;
  }

  private void ClearData()
  {
    using (Dictionary<string, FallenKnightRankListItemRenderer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this.itemPool.Release(gameObject);
      }
    }
    this.itemDict.Clear();
    this.itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private int SortByRankTime(KeyValuePair<int, ArrayList> a, KeyValuePair<int, ArrayList> b)
  {
    if (a.Key > b.Key)
      return -1;
    return a.Key == b.Key ? 0 : 1;
  }
}
