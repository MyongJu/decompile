﻿// Decompiled with JetBrains decompiler
// Type: RallyTempleData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;

public class RallyTempleData : RallyBuildingData
{
  private AllianceTempleData _templeData;

  public RallyTempleData(int x, int y)
    : base(x, y)
  {
  }

  public RallyTempleData(long id)
    : base(id)
  {
  }

  private AllianceTempleData TempleData
  {
    get
    {
      if (this._templeData == null)
        this._templeData = this.id <= 0L ? DBManager.inst.DB_AllianceTemple.Get(this.x, this.y) : DBManager.inst.DB_AllianceTemple.Get(this.id);
      return this._templeData;
    }
  }

  public override void Ready(System.Action callBack)
  {
    AllianceBuildUtils.LoadMaxTroopCount(this.TempleData, callBack);
  }

  public override string BuildName
  {
    get
    {
      return AllianceBuildUtils.GetAllianceTempleName((int) this.TempleData.templeId);
    }
  }

  public override Coordinate Location
  {
    get
    {
      return this.TempleData.Location;
    }
  }

  public override List<MarchData> MarchList
  {
    get
    {
      return AllianceBuildUtils.GetMarchsByAllianceTempID((int) this.TempleData.templeId);
    }
  }

  public override long OwnerID
  {
    get
    {
      return AllianceBuildUtils.GetDragonAltarOwner((int) this.TempleData.templeId);
    }
  }

  public override RallyBuildingData.DataType Type
  {
    get
    {
      return RallyBuildingData.DataType.Temple;
    }
  }

  public override long MaxCount
  {
    get
    {
      return this.TempleData.MaxTroopsCount;
    }
  }

  public override MarchAllocDlg.MarchType ReinforceMarchType
  {
    get
    {
      return MarchAllocDlg.MarchType.templeReinforce;
    }
  }
}
