﻿// Decompiled with JetBrains decompiler
// Type: ConfigAltarSummonRewardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ConfigAltarSummonRewardInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "group_id")]
  public string groupId;
  [Config(Name = "item_id")]
  public int itemId;
  [Config(Name = "value")]
  public int itemCount;
  [Config(Name = "weight")]
  public int weight;
}
