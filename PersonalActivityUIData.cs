﻿// Decompiled with JetBrains decompiler
// Type: PersonalActivityUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class PersonalActivityUIData : ActivityBaseUIData
{
  private PersonalActivityData _personalActivityData;

  protected PersonalActivityData PersonalData
  {
    get
    {
      if (this._personalActivityData == null)
        this._personalActivityData = this.Data as PersonalActivityData;
      return this._personalActivityData;
    }
  }

  public override string EventKey
  {
    get
    {
      return "single_activity";
    }
  }

  public override int StartTime
  {
    get
    {
      return this.PersonalData.StartTime;
    }
  }

  public override int EndTime
  {
    get
    {
      return this.PersonalData.EndTime;
    }
  }

  public override ActivityBaseUIData.ActivityType Type
  {
    get
    {
      return ActivityBaseUIData.ActivityType.Personal;
    }
  }

  public override IconData GetNormalIconData()
  {
    IconData iconData = new IconData();
    iconData.image = "Texture/Events/solo_activity_icon";
    iconData.contents = new string[2];
    iconData.contents[0] = Utils.XLAT("event_solo_event_name");
    iconData.contents[1] = Utils.XLAT("event_solo_event_one_line_description");
    iconData.Data = (object) this;
    return iconData;
  }

  public override IconData GetADIconData()
  {
    IconData iconData = new IconData();
    iconData.image = "Texture/Events/solo_activity_big";
    iconData.contents = new string[2];
    iconData.contents[0] = Utils.XLAT("event_solo_event_name");
    iconData.contents[1] = string.Empty;
    return iconData;
  }

  public override void DisplayActivityDetail()
  {
    UIManager.inst.OpenDlg("Activity/PersonalActivityDetailDlg", (UI.Dialog.DialogParameter) new PersonalActivityDetailDlg.Parameter()
    {
      data = (this.Data as ActivityBaseData)
    }, true, true, true);
  }

  public override int ActivityID
  {
    get
    {
      return this.PersonalData.ActivityId;
    }
  }
}
