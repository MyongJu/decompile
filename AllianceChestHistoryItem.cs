﻿// Decompiled with JetBrains decompiler
// Type: AllianceChestHistoryItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class AllianceChestHistoryItem : MonoBehaviour
{
  [SerializeField]
  private UILabel _LabelChestName;
  [SerializeField]
  private UILabel _LabelFromName;
  [SerializeField]
  private UILabel _LabelGetCount;

  public void SetData(AllianceChestHistoryData chestData)
  {
    AllianceTreasuryVaultMainInfo data = ConfigManager.inst.DB_AllianceTreasuryVaultMain.GetData(chestData.configId);
    if (data != null)
      this._LabelChestName.text = data.LocalChestName;
    string senderName = ScriptLocalization.Get("id_anonymous", true);
    if (!string.IsNullOrEmpty(chestData.senderName))
      senderName = chestData.senderName;
    this._LabelFromName.text = senderName;
    this._LabelGetCount.text = chestData.gold.ToString();
  }
}
