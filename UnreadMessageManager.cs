﻿// Decompiled with JetBrains decompiler
// Type: UnreadMessageManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Chat;

public class UnreadMessageManager
{
  public System.Action UnreadMessageChagned;
  public System.Action KingdomMessageChanged;
  private int _unreadKingdomMessage;
  public System.Action AllianceMessageChanged;
  private int _unreadAllianceMessage;
  public System.Action RoomMessageChanged;
  private int _unreadRoomMessage;
  public System.Action PrivateMessageChanged;
  private int _unreadPrivateMessage;

  public int unreadKingdomMessage
  {
    get
    {
      return this._unreadKingdomMessage;
    }
    set
    {
      this._unreadKingdomMessage = this.unreadRoomMessage;
    }
  }

  public int unreadAllianceMessage
  {
    get
    {
      return this._unreadAllianceMessage;
    }
    set
    {
      this._unreadAllianceMessage = value;
    }
  }

  public int unreadRoomMessage
  {
    get
    {
      return this._unreadRoomMessage;
    }
    set
    {
      this._unreadRoomMessage = value;
    }
  }

  public int unreadPrivateMessage
  {
    get
    {
      return this._unreadPrivateMessage;
    }
    set
    {
      this._unreadPrivateMessage = value;
    }
  }

  public void FreshUnreadMessage(ChatMessage message)
  {
    if (message.receiveChannelId == 0L)
      return;
    if (message.receiveChannelId == GameEngine.Instance.ChatManager.userChannelId)
      ++this.unreadPrivateMessage;
    else if (message.receiveChannelId == GameEngine.Instance.ChatManager.kingdomChannelId)
    {
      ++this.unreadKingdomMessage;
    }
    else
    {
      if (message.receiveChannelId != GameEngine.Instance.ChatManager.allianceChannelId)
        return;
      ++this.unreadAllianceMessage;
    }
  }
}
