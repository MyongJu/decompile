﻿// Decompiled with JetBrains decompiler
// Type: ImageImporter
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ImageImporter : MonoBehaviour
{
  protected static ImageImporter ms_instance;
  protected IPlatformImageImporter m_importer;
  public ImageImporter.OnImageImported ImportCallback;

  public static ImageImporter Instance
  {
    get
    {
      if ((Object) ImageImporter.ms_instance == (Object) null)
      {
        GameObject gameObject = new GameObject("ImageImporter_Instance");
        Object.DontDestroyOnLoad((Object) gameObject);
        ImageImporter.ms_instance = gameObject.AddComponent<ImageImporter>();
        ImageImporter.ms_instance.init();
      }
      return ImageImporter.ms_instance;
    }
  }

  protected void init()
  {
    this.m_importer = (IPlatformImageImporter) this.gameObject.AddComponent<AndroidImageImporter>();
    if (this.m_importer == null)
      return;
    this.m_importer.init(this);
  }

  public void nofityImageImportedResult(bool result, string imagePath)
  {
    if (this.ImportCallback == null)
      return;
    this.ImportCallback(result, imagePath);
  }

  protected bool checkPlatformSupport()
  {
    if (this.m_importer != null)
      return true;
    this.nofityImageImportedResult(false, string.Empty);
    return false;
  }

  public void requestPickImage(int requestWidth, int requestHeight, string url, string token)
  {
    if (!this.checkPlatformSupport())
      return;
    this.m_importer.requestPickImage(requestWidth, requestHeight, url, token);
  }

  public void requestCaptureImage(int requestWidth, int requestHeight, string url, string token)
  {
    if (!this.checkPlatformSupport())
      return;
    this.m_importer.requestCaptureImage(requestWidth, requestHeight, url, token);
  }

  public delegate void OnImageImported(bool result, string imagePath);
}
