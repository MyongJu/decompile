﻿// Decompiled with JetBrains decompiler
// Type: HeroModelDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HeroModelDlg : MonoBehaviour
{
  public GameObject heroOrg;
  public Transform heroSpin;
  private HeroEquipments _heroEquipments;
  private bool _refreshed;

  public bool Refreshed
  {
    set
    {
      this._refreshed = value;
      if (this._refreshed)
        return;
      this.Refresh();
    }
  }

  public void Init()
  {
    if (!((Object) this.heroOrg != (Object) null))
      return;
    GameObject gameObject = Object.Instantiate<GameObject>(this.heroOrg);
    if (!((Object) gameObject != (Object) null))
      return;
    gameObject.transform.parent = this.heroSpin;
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.layer = LayerMask.NameToLayer("UI3D");
    NGUITools.SetChildLayer(gameObject.transform, gameObject.layer);
    this._heroEquipments = gameObject.GetComponent<HeroEquipments>();
  }

  private void OnEnable()
  {
    this.Refresh();
  }

  private void Refresh()
  {
  }
}
