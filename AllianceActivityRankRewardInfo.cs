﻿// Decompiled with JetBrains decompiler
// Type: AllianceActivityRankRewardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class AllianceActivityRankRewardInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "alliance_activity_id")]
  public int ActivityMainSubInfo_ID;
  [Config(Name = "type")]
  public int Type;
  [Config(Name = "rank_min")]
  public int MinRank;
  [Config(Name = "rank_max")]
  public int MaxRank;
  [Config(Name = "rank_reward_1")]
  public int ItemRewardID_1;
  [Config(Name = "rank_reward_value_1")]
  public int ItemRewardValue_1;
  [Config(Name = "rank_reward_2")]
  public int ItemRewardID_2;
  [Config(Name = "rank_reward_value_2")]
  public int ItemRewardValue_2;
  [Config(Name = "rank_reward_3")]
  public int ItemRewardID_3;
  [Config(Name = "rank_reward_value_3")]
  public int ItemRewardValue_3;
  [Config(Name = "rank_reward_4")]
  public int ItemRewardID_4;
  [Config(Name = "rank_reward_value_4")]
  public int ItemRewardValue_4;
  [Config(Name = "rank_reward_5")]
  public int ItemRewardID_5;
  [Config(Name = "rank_reward_value_5")]
  public int ItemRewardValue_5;
  [Config(Name = "rank_reward_6")]
  public int ItemRewardID_6;
  [Config(Name = "rank_reward_value_6")]
  public int ItemRewardValue_6;
}
