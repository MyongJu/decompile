﻿// Decompiled with JetBrains decompiler
// Type: PVPWonderBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class PVPWonderBanner : SpriteBanner
{
  private static Color playerColor = new Color(1f, 1f, 0.6862745f, 1f);
  private static Color enemyColor = Color.red;
  private static Color allyColor = new Color(0.3686275f, 0.9764706f, 0.9764706f, 1f);
  private static Color neutralColor = Color.white;
  [SerializeField]
  protected GameObject m_root;
  [SerializeField]
  protected TextMesh m_stateName;
  [SerializeField]
  protected TextMesh m_remainTime;
  protected TileData m_tileData;
  public PVPAllianceSymbol m_Symbol;
  public GameObject m_RootNode;
  public GameObject m_Knife;

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.SecondTick);
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.SecondTick);
  }

  protected void SecondTick(int delta)
  {
    this.UpdateStateUI(this.m_tileData);
  }

  public override void UpdateUI(TileData tile)
  {
    this.m_tileData = tile;
    base.UpdateUI(tile);
    this.UpdateStateUI(tile);
    this.UpdateBannerUI(tile);
  }

  protected void UpdateStateUI(TileData tile)
  {
    if (tile == null || tile.WonderData == null)
    {
      this.m_root.SetActive(false);
    }
    else
    {
      this.m_root.SetActive(true);
      this.m_stateName.text = this.GetStateString(tile.WonderData);
      this.m_remainTime.text = this.GetStateLeftTime(tile.WonderData);
    }
  }

  protected string GetStateString(DB.WonderData wonderData)
  {
    string empty = string.Empty;
    string state = wonderData.State;
    if (state != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (PVPWonderBanner.\u003C\u003Ef__switch\u0024mapA0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        PVPWonderBanner.\u003C\u003Ef__switch\u0024mapA0 = new Dictionary<string, int>(3)
        {
          {
            "fighting",
            0
          },
          {
            "protected",
            1
          },
          {
            "unOpen",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (PVPWonderBanner.\u003C\u003Ef__switch\u0024mapA0.TryGetValue(state, out num))
      {
        switch (num)
        {
          case 0:
            empty = ScriptLocalization.Get("throne_challenge_status", true);
            break;
          case 1:
            empty = ScriptLocalization.Get("throne_protected_status", true);
            break;
        }
      }
    }
    return empty;
  }

  protected string GetStateLeftTime(DB.WonderData wonderData)
  {
    string empty = string.Empty;
    string str;
    if (wonderData.State == "fighting" && wonderData.OWNER_ALLIANCE_ID == 0L)
    {
      str = string.Empty;
    }
    else
    {
      int time = (int) wonderData.StateChangeTime - NetServerTime.inst.ServerTimestamp;
      str = time > 0 ? Utils.FormatTime(time, true, false, true) : string.Empty;
    }
    return str;
  }

  public void UpdateBannerUI(TileData tile)
  {
    UserData userData = tile.UserData;
    DB.WonderData wonderData = tile.WonderData;
    this.m_RootNode.SetActive(tile.OwnerID != 0L);
    if (wonderData != null && wonderData.State == "protected")
      this.m_RootNode.SetActive(true);
    string str1 = string.Empty;
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(tile.AllianceID);
    if (userData == null && wonderData != null && wonderData.State == "protected")
    {
      userData = DBManager.inst.DB_User.Get(wonderData.KING_UID);
      allianceData = DBManager.inst.DB_Alliance.Get(wonderData.KING_ALLIANCE_ID);
    }
    if (allianceData != null)
    {
      str1 = string.Format("({0})", (object) allianceData.allianceAcronym);
      this.m_Symbol.gameObject.SetActive(true);
      this.m_Symbol.SetSymbols(allianceData.allianceSymbolCode);
    }
    else
      this.m_Symbol.gameObject.SetActive(false);
    string str2 = string.Empty;
    if (userData != null)
    {
      str2 = userData.userName;
      if (userData.IsForeigner && tile.OwnerID != PlayerData.inst.uid || userData.IsPit)
        str2 = str2 + ".k" + (object) userData.world_id;
    }
    this.Name = str1 + str2;
    this.m_Knife.SetActive(userData != null && (userData.IsForeigner || userData.IsPit));
    this.UpdateNameColor(tile);
    if (!(bool) ((UnityEngine.Object) this.m_NameRenderer))
      return;
    Bounds bounds = this.m_NameRenderer.bounds;
    Vector3 vector3 = this.m_NameLabel.transform.InverseTransformPoint(new Vector3(bounds.min.x, bounds.center.y, bounds.center.z));
    vector3.x -= 50f;
    this.m_Symbol.transform.localPosition = vector3;
    Vector3 position = new Vector3(bounds.max.x, bounds.center.y, bounds.center.z);
    position = this.m_NameLabel.transform.InverseTransformPoint(position);
    position.x += 50f;
    this.m_Knife.transform.localPosition = position;
  }

  private void UpdateNameColor(TileData tile)
  {
    if (tile.OwnerID == PlayerData.inst.uid)
      this.m_NameLabel.color = PVPWonderBanner.playerColor;
    else if (PlayerData.inst.allianceData != null)
    {
      if (tile.AllianceID == PlayerData.inst.allianceId)
      {
        this.m_NameLabel.color = PVPWonderBanner.allyColor;
      }
      else
      {
        UserData userData = tile.UserData;
        if (userData != null && userData.IsForeigner)
          this.m_NameLabel.color = PVPWonderBanner.enemyColor;
        else
          this.m_NameLabel.color = PVPWonderBanner.neutralColor;
      }
    }
    else
    {
      UserData userData = tile.UserData;
      if (userData != null && userData.IsForeigner)
        this.m_NameLabel.color = PVPWonderBanner.enemyColor;
      else
        this.m_NameLabel.color = PVPWonderBanner.neutralColor;
    }
  }
}
