﻿// Decompiled with JetBrains decompiler
// Type: AllianceFundBuyDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using UI;

public class AllianceFundBuyDialog : Popup
{
  public UILabel m_ItemName;
  public UILabel m_ItemDesc;
  public UILabel m_FundPrice;
  public UITexture m_ItemIcon;
  public UITexture m_FundIcon;
  public UISlider m_Slider;
  public UIInput m_Input;
  public UIButton m_BuyButton;
  private AllianceFundBuyDialog.Parameter m_Parameter;
  private int m_PurchaseCount;
  private EventDelegate onSliderChanged;
  private EventDelegate onInputChanged;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as AllianceFundBuyDialog.Parameter;
    this.m_ItemName.text = this.m_Parameter.allianceShopInfo.Name;
    this.m_ItemDesc.text = this.m_Parameter.allianceShopInfo.Description;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, this.m_Parameter.allianceShopInfo.ImageIconPath, (System.Action<bool>) null, true, false, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_FundIcon, "Texture/Alliance/alliance_fund", (System.Action<bool>) null, true, false, string.Empty);
    this.SetPurchaseCount(0);
    this.onSliderChanged = new EventDelegate(new EventDelegate.Callback(this.OnSliderChanged));
    this.onInputChanged = new EventDelegate(new EventDelegate.Callback(this.OnInputChanged));
    this.m_Input.onChange.Clear();
    this.m_Slider.onChange.Clear();
    this.m_Input.onChange.Add(this.onInputChanged);
    this.m_Slider.onChange.Add(this.onSliderChanged);
    AllianceManager.Instance.onLeave += new System.Action(this.OnLeave);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    AllianceManager.Instance.onLeave += new System.Action(this.OnLeave);
  }

  private void OnLeave()
  {
    this.OnCloseButtonClick();
  }

  public void OnSliderChanged()
  {
    long num = PlayerData.inst.allianceData.allianceFund / (long) this.m_Parameter.allianceShopInfo.FundPrice;
    this.m_Slider.onChange.Clear();
    this.SetPurchaseCount((int) Math.Round((double) num * (double) this.m_Slider.value));
    this.m_Slider.onChange.Add(this.onSliderChanged);
  }

  public void OnInputChanged()
  {
    int result = 0;
    int.TryParse(this.m_Input.value, out result);
    this.m_Input.onChange.Clear();
    this.SetPurchaseCount(result);
    this.m_Input.onChange.Add(this.onInputChanged);
  }

  public void OnSubstract()
  {
    this.SetPurchaseCount(this.m_PurchaseCount - 1);
  }

  public void OnAdd()
  {
    this.SetPurchaseCount(this.m_PurchaseCount + 1);
  }

  public void OnBuy()
  {
    this.OnCloseButtonClick();
    if (this.SufficientPermission())
    {
      Hashtable postData = new Hashtable();
      postData[(object) "internalId"] = (object) this.m_Parameter.allianceShopInfo.internalId;
      postData[(object) "quantity"] = (object) this.m_PurchaseCount;
      MessageHub.inst.GetPortByAction("AllianceStore:addItemToStore").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          return;
        UIManager.inst.toast.Show("Failed to purchase!", (System.Action) null, 4f, false);
      }), true);
    }
    else
      UIManager.inst.toast.Show("Insufficient Purcase Permission!", (System.Action) null, 4f, false);
  }

  private bool SufficientPermission()
  {
    if (PlayerData.inst.allianceData == null)
      return false;
    AllianceMemberData allianceMemberData = PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid);
    if (allianceMemberData != null)
      return allianceMemberData.HasAllianceStoreAccess();
    return false;
  }

  public void OnCloseButtonClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void SetPurchaseCount(int count)
  {
    int fundPrice = this.m_Parameter.allianceShopInfo.FundPrice;
    long num = PlayerData.inst.allianceData.allianceFund / (long) fundPrice;
    this.m_PurchaseCount = Math.Max(1, Math.Min(count, (int) num));
    this.m_FundPrice.text = Utils.FormatThousands((this.m_PurchaseCount * fundPrice).ToString());
    this.m_BuyButton.isEnabled = this.m_PurchaseCount > 0;
    this.m_Slider.value = num <= 0L ? 0.0f : (float) this.m_PurchaseCount / (float) num;
    this.m_Input.value = this.m_PurchaseCount.ToString();
  }

  public class Parameter : Popup.PopupParameter
  {
    public AllianceShopInfo allianceShopInfo;
  }
}
