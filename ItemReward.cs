﻿// Decompiled with JetBrains decompiler
// Type: ItemReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ItemReward : QuestReward
{
  public readonly Hero_EquipmentData item;

  public ItemReward(int itemPHPID)
  {
    this.item = ConfigManager.inst.DB_Hero_Equipment.GetData(itemPHPID);
  }

  public override void Claim()
  {
    base.Claim();
  }

  public override string GetRewardIconName()
  {
    if (!string.IsNullOrEmpty(this.item.Asset_Filename))
      return this.item.Asset_Filename;
    return "Weapon";
  }

  public override string GetRewardTypeName()
  {
    return this.item.Name;
  }

  public override string GetValueText()
  {
    return string.Empty;
  }
}
