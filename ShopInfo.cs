﻿// Decompiled with JetBrains decompiler
// Type: ShopInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ShopInfo
{
  public int[] content_id = new int[4];
  public int[] contect_value = new int[4];
  public const int MAX_CONTENT_COUNT = 4;
  public int internalId;
  public string ID;
  public ItemBag.ItemType Type;
  public string LOC_ID;
  public string LOC_Description_ID;
  public int Priority;
  public int Price;
  public int Essence;
  public double Value;
  public string Category;
  public string Location;
  public string Icon;
  public bool Visible;
  public bool Purchasable;
  public bool Usable;
  public bool Live_Ready;
  public int Hide_Until_Level;
  public int Usable_Until_Level;
  public string Requirement_ID_1;
  public int Loyalty;
  public string Buff_ID;
  public string Toast;
}
