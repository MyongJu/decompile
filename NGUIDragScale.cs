﻿// Decompiled with JetBrains decompiler
// Type: NGUIDragScale
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class NGUIDragScale : MonoBehaviour
{
  private static Dictionary<int, List<Vector2>> dragPos = new Dictionary<int, List<Vector2>>();

  public void OnDrag(Vector2 dir)
  {
    NGUIDragScale.dragPos[UICamera.currentTouchID].Add(UICamera.lastTouchPosition);
    if (NGUIDragScale.dragPos.Count != 2)
      return;
    List<Vector2> vector2List1 = (List<Vector2>) null;
    List<Vector2> vector2List2 = (List<Vector2>) null;
    using (Dictionary<int, List<Vector2>>.ValueCollection.Enumerator enumerator = NGUIDragScale.dragPos.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        List<Vector2> current = enumerator.Current;
        if (vector2List1 == null)
          vector2List1 = current;
        else if (vector2List2 == null)
          vector2List2 = current;
      }
    }
    if (vector2List1 == null || vector2List1.Count < 2 || (vector2List2 == null || vector2List2.Count < 2))
      return;
    float num1 = Vector2.Distance(vector2List1[vector2List1.Count - 1], vector2List2[vector2List2.Count - 1]);
    float num2 = Vector2.Distance(vector2List1[vector2List1.Count - 2], vector2List2[vector2List2.Count - 2]);
    float orthographicSize = UICamera.currentCamera.GetComponent<Camera>().orthographicSize;
    if ((double) num1 > (double) num2)
      orthographicSize -= 0.01f;
    else if ((double) num1 < (double) num2)
      orthographicSize += 0.01f;
    float num3 = Mathf.Clamp(orthographicSize, 0.5f, 2f);
    UICamera.currentCamera.GetComponent<Camera>().orthographicSize = num3;
  }

  public void OnDragStart()
  {
    NGUIDragScale.dragPos.Remove(UICamera.currentTouchID);
    NGUIDragScale.dragPos.Add(UICamera.currentTouchID, new List<Vector2>());
    NGUIDragScale.dragPos[UICamera.currentTouchID].Add(UICamera.lastTouchPosition);
  }

  public void OnDragEnd()
  {
    NGUIDragScale.dragPos.Remove(UICamera.currentTouchID);
  }
}
