﻿// Decompiled with JetBrains decompiler
// Type: ClientMailStorage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

public class ClientMailStorage : IMailStorage
{
  private string mailTableName = "mail";
  private const string mailDb = "mail.db";
  private const string interactedUsersTableName = "users";
  private const string lastMailTimeTable = "lastMailTime";
  private const string idKey = "id";
  private const string timeKey = "time";
  private const string typeKey = "type";
  private const string jsonKey = "json";
  private const string readKey = "read";
  private const string favKey = "fav";
  private const string fromUIDKey = "fromUID";
  private const string toUIDKey = "toUID";
  private const string subjectKey = "subject";
  private const string mailStatusKey = "status";
  private const string MAIL_VERSION_CONFIG_KEY = "mail_version";
  private const string MAIL_INIT_SQL_CONFIG_KEY = "mail_init_sql";
  private SqliteDatabase db;
  private string initSQL;

  public AbstractMailEntry StoreMail(Hashtable mail)
  {
    AbstractMailEntry mailForType = this.GetMailForType(int.Parse(mail[(object) "type"] as string));
    mailForType.ApplyData(mail);
    this.ExecuteStoreMailQuery(mailForType, true);
    if (!string.IsNullOrEmpty(mailForType.sender) && mailForType.sender != PlayerData.inst.userData.userName)
      this.AddInteractedUsername(mailForType.sender);
    return mailForType;
  }

  public void StoreMail(AbstractMailEntry mailEntry)
  {
    this.ExecuteStoreMailQuery(mailEntry, true);
  }

  public void Initialize()
  {
    this.db = new SqliteDatabase("mail.db");
    this.ConfigVersion();
    this.InitializeDatabase();
  }

  public List<AbstractMailEntry> GetMailList(MailQueryArg arguments)
  {
    List<AbstractMailEntry> abstractMailEntryList = new List<AbstractMailEntry>();
    List<DataRow>.Enumerator enumerator = this.db.ExecuteQuery(this.GenerateQuery(arguments)).Rows.GetEnumerator();
    while (enumerator.MoveNext())
      abstractMailEntryList.Add(this.GetMailFromDataRow(enumerator.Current));
    return abstractMailEntryList;
  }

  public AbstractMailEntry RetrieveMail(long mailId)
  {
    DataRow row = this.db.ExecuteQuery("SELECT * FROM " + this.mailTableName + " WHERE id=" + (object) mailId).Rows[0];
    if (row == null)
      return (AbstractMailEntry) null;
    return this.GetMailFromDataRow(row);
  }

  public void DeleteMail(long mailId)
  {
    this.db.ExecuteNonQuery("DELETE FROM " + this.mailTableName + " WHERE id=" + (object) mailId);
  }

  public HashSet<string> InteractedUsernames()
  {
    HashSet<string> stringSet = new HashSet<string>();
    DataTable dataTable = this.db.ExecuteQuery("SELECT name FROM users");
    if (dataTable == null)
      return stringSet;
    List<DataRow>.Enumerator enumerator = dataTable.Rows.GetEnumerator();
    while (enumerator.MoveNext())
    {
      object empty = (object) string.Empty;
      if (enumerator.Current.TryGetValue("name", out empty))
        stringSet.Add(empty as string);
    }
    return stringSet;
  }

  public void AddInteractedUsername(string name)
  {
    StringBuilder builder = new StringBuilder();
    Utils.SerializeString(this.SanitizeString(name), builder);
    string str = this.SanitizeString(builder.ToString());
    if (this.InteractedUsernames().Contains(str))
      return;
    this.db.ExecuteNonQuery("INSERT INTO users (name) VALUES ('" + str + "')");
  }

  public int GetLastMailTime()
  {
    DataRow row = this.db.ExecuteQuery("SELECT time FROM lastMailTime LIMIT 1").Rows[0];
    object obj = (object) 0;
    if (row.TryGetValue("time", out obj))
      return (int) obj;
    return 0;
  }

  public int GetUnreadCount()
  {
    return this.db.ExecuteQuery("SELECT * FROM " + this.mailTableName + " WHERE read=0").Rows.Count;
  }

  public void UpdateLastMailTime(long p)
  {
    int lastMailTime = this.GetLastMailTime();
    this.db.ExecuteNonQuery("UPDATE lastMailTime SET time=" + (object) p + " WHERE time=" + (object) lastMailTime);
  }

  public void UpdateMailEntry(AbstractMailEntry mail)
  {
    this.ExecuteStoreMailQuery(mail, false);
  }

  private List<long> GetListForDataTable(DataTable queryResult)
  {
    List<long> longList = new List<long>();
    List<DataRow>.Enumerator enumerator = queryResult.Rows.GetEnumerator();
    while (enumerator.MoveNext())
    {
      object obj;
      if (enumerator.Current.TryGetValue("id", out obj))
        longList.Add((long) (int) obj);
    }
    return longList;
  }

  private string GenerateQuery(MailQueryArg arguments)
  {
    string str1 = "SELECT * FROM " + this.mailTableName;
    string str2 = " ORDER BY id DESC";
    bool flag1 = false;
    List<MailQueryArg.MailQueryArgs>.Enumerator enumerator1 = arguments.activeArguments.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      switch (enumerator1.Current)
      {
        case MailQueryArg.MailQueryArgs.TypeList:
          string str3 = str1 + (!flag1 ? " WHERE " : " AND ") + "(";
          bool flag2 = false;
          List<MailType>.Enumerator enumerator2 = arguments.validTypes.GetEnumerator();
          while (enumerator2.MoveNext())
          {
            if (flag2)
              str3 += " OR ";
            flag2 = true;
            str3 = str3 + "type=" + (object) enumerator2.Current;
          }
          str1 = str3 + ")";
          break;
        case MailQueryArg.MailQueryArgs.ReadStatus:
          str1 = str1 + (!flag1 ? " WHERE " : " AND ") + "read=" + (object) (!arguments.readStatus ? 0 : 1);
          break;
        case MailQueryArg.MailQueryArgs.FavStatus:
          str1 = str1 + (!flag1 ? " WHERE " : " AND ") + "fav=" + (object) (!arguments.favStatus ? 0 : 1);
          break;
        case MailQueryArg.MailQueryArgs.StartMailId:
          str1 = str1 + (!flag1 ? " WHERE " : " AND ") + "id < " + (object) arguments.startId;
          break;
        case MailQueryArg.MailQueryArgs.RetrievalLimit:
          str2 = str2 + " LIMIT " + (object) arguments.retrievalLimit;
          break;
        case MailQueryArg.MailQueryArgs.Subject:
          StringBuilder builder = new StringBuilder();
          Utils.SerializeString(this.SanitizeString(arguments.subject), builder);
          string str4 = builder.ToString();
          str1 = str1 + (!flag1 ? " WHERE " : " AND ") + "subject='" + str4 + "'";
          break;
        case MailQueryArg.MailQueryArgs.FromUID:
          str1 = str1 + (!flag1 ? " WHERE " : " AND ") + "fromUID='" + arguments.fromUID.ToString() + "'";
          break;
        case MailQueryArg.MailQueryArgs.ToUID:
          str1 = str1 + (!flag1 ? " WHERE " : " AND ") + "toUID='" + arguments.toUID.ToString() + "'";
          break;
        case MailQueryArg.MailQueryArgs.StartCtime:
          str1 = str1 + (!flag1 ? " WHERE " : " AND ") + "time > " + (object) arguments.startCtime;
          break;
        case MailQueryArg.MailQueryArgs.MailState:
          str1 = str1 + (!flag1 ? " WHERE " : " AND ") + "status=" + (object) arguments.mailState;
          break;
      }
      flag1 = true;
    }
    return str1 + str2;
  }

  private AbstractMailEntry GetMailFromDataRow(DataRow row)
  {
    object obj1 = (object) 0;
    object empty = (object) string.Empty;
    object obj2 = (object) 0;
    object obj3 = (object) 0;
    object obj4 = (object) 0;
    if (!row.TryGetValue("type", out obj1) || !row.TryGetValue("json", out empty) || (!row.TryGetValue("read", out obj2) || !row.TryGetValue("fav", out obj3)) || !row.TryGetValue("status", out obj4))
      return (AbstractMailEntry) null;
    AbstractMailEntry mailForType = this.GetMailForType((int) obj1);
    mailForType.ApplyData(Utils.Json2Object(empty as string, true) as Hashtable);
    mailForType.isRead = (int) obj2 == 1;
    mailForType.isFavorite = (int) obj3 == 1;
    return mailForType;
  }

  private void ExecuteStoreMailQuery(AbstractMailEntry mailEntry, bool insert)
  {
    string str1 = this.SanitizeString(Utils.Object2Json((object) mailEntry.data));
    int type = (int) mailEntry.type;
    long timeAsInt = mailEntry.timeAsInt;
    long mailId = mailEntry.mailID;
    int num1 = !mailEntry.isRead ? 0 : 1;
    int num2 = !mailEntry.isFavorite ? 0 : 1;
    string str2 = mailEntry.fromUID.ToString();
    string str3 = mailEntry.toUID.ToString();
    StringBuilder builder = new StringBuilder();
    Utils.SerializeString(this.SanitizeString(mailEntry.subject), builder);
    string str4 = builder.ToString();
    MailStatus status = mailEntry.status;
    string empty = string.Empty;
    string query;
    if (insert)
      query = "INSERT INTO " + this.mailTableName + " (id, time, status, type, read, fav, fromUID, toUID, subject, json) VALUES (" + (object) mailId + ", " + (object) timeAsInt + ", " + (object) status + ", " + (object) type + ", " + (object) num1 + ", " + (object) num2 + ",'" + str2 + "','" + str3 + "','" + str4 + "','" + str1 + "')";
    else
      query = "UPDATE " + this.mailTableName + " SET read=" + (object) num1 + ", fav=" + (object) num2 + ", status=" + (object) status + ", fromUID='" + str2 + "', toUID='" + str3 + "', subject='" + str4 + "', json='" + str1 + "' WHERE id=" + (object) mailId;
    this.db.ExecuteNonQuery(query);
  }

  public string SanitizeString(string input)
  {
    return Regex.Replace(input, "[']", "''");
  }

  private AbstractMailEntry GetMailForType(int type)
  {
    MailType mailType = (MailType) type;
    switch (mailType)
    {
      case MailType.MAIL_TYPE_NORMAL:
        return (AbstractMailEntry) new PlayerMessage();
      case MailType.MAIL_TYPE_PVP_BATTLE_REPORT:
      case MailType.MAIL_TYPE_PVP_SCOUT_REPORT:
      case MailType.MAIL_TYPE_PVE_BATTLE_REPORT:
        return (AbstractMailEntry) new WarMessage();
      case MailType.MAIL_TYPE_SYSTEM_NOTICE:
        return (AbstractMailEntry) new SystemMessage();
      default:
        if (mailType != MailType.MAIL_TYPE_SYSTEM_ABYSS_RESULT_NOTICE)
        {
          if (mailType != MailType.MAIL_TYPE_MERLIN_TOWER_GATHER_DEFEND_LOSE && mailType != MailType.MAIL_TYPE_ANNI_BATTLE_REPORT)
            return (AbstractMailEntry) new OtherReportMessage();
          goto case MailType.MAIL_TYPE_PVP_BATTLE_REPORT;
        }
        else
          goto case MailType.MAIL_TYPE_SYSTEM_NOTICE;
    }
  }

  private void InitializeDatabase()
  {
    this.CreateMailTable();
    this.CreateUserTable();
    this.CreateLastMailTimeTable();
  }

  private void ConfigVersion()
  {
    ClientMailStorage clientMailStorage = this;
    clientMailStorage.mailTableName = clientMailStorage.mailTableName + this.MailTableVersion() + (object) PlayerData.inst.uid;
    this.initSQL = this.MailInitSQL();
  }

  public void DebugTables()
  {
    using (List<DataRow>.Enumerator enumerator1 = this.db.ExecuteQuery("SELECT * FROM " + this.mailTableName).Rows.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        DataRow current1 = enumerator1.Current;
        string str = "MAIL INFO: ";
        using (Dictionary<string, object>.KeyCollection.Enumerator enumerator2 = current1.Keys.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            string current2 = enumerator2.Current;
            object obj;
            current1.TryGetValue(current2, out obj);
            str = str + "[Key: " + current2 + ", value:" + obj.ToString() + "], ";
          }
        }
      }
    }
  }

  private void DeleteTables()
  {
    this.db.ExecuteNonQuery("DROP TABLE IF EXISTS " + this.mailTableName);
    this.db.ExecuteNonQuery("DROP TABLE IF EXISTS users");
    this.db.ExecuteNonQuery("DROP TABLE IF EXISTS lastMailTime");
  }

  private void CreateMailTable()
  {
    this.db.ExecuteNonQuery(this.initSQL);
    this.db.ExecuteNonQuery("CREATE INDEX IF NOT EXISTS 'index' ON " + this.mailTableName + " (id, status, fav, type, fromUID, toUID, subject)");
  }

  private void CreateUserTable()
  {
    this.db.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS users (name STRING, uid INT)");
  }

  private void CreateLastMailTimeTable()
  {
    this.db.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS lastMailTime (time INT)");
    this.db.ExecuteNonQuery("INSERT INTO lastMailTime (time) VALUES (0)");
  }

  private string MailTableVersion()
  {
    if (ConfigManager.inst.DB_GameConfig.GetData("mail_version") != null)
      return ConfigManager.inst.DB_GameConfig.GetData("mail_version").ValueString;
    return "1";
  }

  private string MailInitSQL()
  {
    if (ConfigManager.inst.DB_GameConfig.GetData("mail_init_sql") != null)
      return string.Format(ConfigManager.inst.DB_GameConfig.GetData("mail_init_sql").ValueString, (object) this.mailTableName);
    return "CREATE TABLE IF NOT EXISTS " + this.mailTableName + " (id INT, time INT, status INT, read INT, fav INT, type INT, fromUID STRING, toUID STRING, subject STRING, json STRING)";
  }
}
