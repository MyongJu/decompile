﻿// Decompiled with JetBrains decompiler
// Type: RallyInfoData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;

public class RallyInfoData
{
  private int _desTroopCount = -1;
  private List<MarchData> marchList = new List<MarchData>();
  private MarchAllocDlg.MarchType _reinforceMarchType = MarchAllocDlg.MarchType.reinforce;
  public Coordinate _reinforceLocation = Coordinate.zero;
  public System.Action OnInitCompleteHandler;
  private RallyInfoData.RallyDataType _type;

  public event System.Action OnPrepareFail;

  private static RallyInfoData CreateByRallyId(long rallyId)
  {
    if (DBManager.inst.DB_Rally.Get(rallyId) == null)
      return (RallyInfoData) null;
    RallyInfoData rallyInfoData = (RallyInfoData) new NormalRallyInfoData();
    rallyInfoData.DataID = rallyId;
    return rallyInfoData;
  }

  protected void DispatchPrepareFail()
  {
    if (this.OnPrepareFail == null)
      return;
    this.OnPrepareFail();
  }

  public static RallyInfoData Create(long dataId)
  {
    if (DBManager.inst.DB_Rally.Get(dataId) != null)
      return RallyInfoData.CreateByRallyId(dataId);
    if (DBManager.inst.DB_March.Get(dataId) != null)
      return RallyInfoData.CreateByMarchId(dataId);
    return (RallyInfoData) null;
  }

  private static RallyInfoData CreateByMarchId(long marchId)
  {
    MarchData marchData = DBManager.inst.DB_March.Get(marchId);
    if (marchData == null || !marchData.IsNpcMarch)
      return (RallyInfoData) null;
    RallyInfoData rallyInfoData = (RallyInfoData) new KnightRallyInfoData();
    rallyInfoData.DataID = marchId;
    return rallyInfoData;
  }

  public long RemainCount { get; set; }

  protected bool IsInit { get; set; }

  public bool TargetIsAllianceBuild { get; set; }

  public bool CanJoin { get; set; }

  public float ProgressValue { get; set; }

  protected bool IsPrepare { get; set; }

  public int DesTroopCount
  {
    get
    {
      return this._desTroopCount;
    }
    set
    {
      this._desTroopCount = value;
    }
  }

  public int EmptySlot { get; set; }

  public RallyInfoData.RallyDataType Type
  {
    get
    {
      return this._type;
    }
    set
    {
      this._type = value;
    }
  }

  protected virtual void Prepare()
  {
    this.IsInit = true;
  }

  public void Reset()
  {
    this.IsInit = false;
    this.Refresh();
  }

  public void Refresh()
  {
    if (!this.IsInit)
      this.Prepare();
    if (!this.IsInit)
      return;
    this.Process();
    if (this.OnInitCompleteHandler == null)
      return;
    this.OnInitCompleteHandler();
    this.OnInitCompleteHandler = (System.Action) null;
  }

  public string ProgressContent { get; set; }

  protected virtual void Process()
  {
  }

  public bool IsCallBack { get; set; }

  public long DataID { get; set; }

  public bool IsDefense { get; set; }

  public long TargetID { get; set; }

  public long OwerID { get; set; }

  public bool IsMyRally
  {
    get
    {
      return this.OwerID == PlayerData.inst.uid;
    }
  }

  public List<MarchData> MarchList
  {
    get
    {
      return this.marchList;
    }
    protected set
    {
      this.marchList = value;
    }
  }

  public void AddMarch(MarchData march)
  {
    if (this.marchList.Contains(march))
      return;
    this.marchList.Add(march);
  }

  public bool IsJoined { get; set; }

  public string LeaderName { get; set; }

  public string JoinNum { get; set; }

  public string TroopsInfo { get; set; }

  public virtual MarchAllocDlg.MarchType ReinforceMarchType
  {
    get
    {
      return this._reinforceMarchType;
    }
    protected set
    {
      this._reinforceMarchType = value;
    }
  }

  public Coordinate ReinforceLocation
  {
    get
    {
      return this._reinforceLocation;
    }
    protected set
    {
      this._reinforceLocation = value;
    }
  }

  public long TargetCityID { get; set; }

  public enum RallyDataType
  {
    None,
    PVP,
    GVE,
    RAB,
    Building,
    Knight,
  }
}
