﻿// Decompiled with JetBrains decompiler
// Type: KingdomTitleDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class KingdomTitleDlg : UI.Dialog
{
  private Dictionary<int, KingdomTitleSlotRenderer> itemDict = new Dictionary<int, KingdomTitleSlotRenderer>();
  private GameObjectPool itemPool = new GameObjectPool();
  private int curPickedTab = -1;
  public GameObject kingRootNode;
  public UILabel kingKingdom;
  public UILabel kingAlliance;
  public UILabel kingName;
  public UILabel kingdomText;
  public UITexture kingTexture;
  public UITexture titleBkgTexture;
  public GameObject giftPackageNode;
  public UIToggle[] tabCategories;
  public UIScrollView scrollView;
  public UIGrid grid;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  private int kingdomId;
  private long needNominateUserId;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    KingdomTitleDlg.Parameter parameter = orgParam as KingdomTitleDlg.Parameter;
    if (parameter != null)
    {
      this.kingdomId = parameter.kingdomId;
      this.needNominateUserId = parameter.needNominateUserId;
    }
    this.tabCategories[0].value = true;
    this.itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.AddEventHandler();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
    this.RemoveEventHandler();
  }

  public void OnTabToggleUpdate()
  {
    int pickedTabIndex = this.GetPickedTabIndex();
    if (pickedTabIndex == this.curPickedTab || pickedTabIndex < 0)
      return;
    this.curPickedTab = pickedTabIndex;
    this.ShowWonderTitleContent(this.curPickedTab);
  }

  public void OnKingPortraitPressed()
  {
    WonderTitleInfo wonderTitleInfo = ConfigManager.inst.DB_WonderTitle.Get("1");
    if (wonderTitleInfo == null || !wonderTitleInfo.IsNominated)
      return;
    UserData userData = DBManager.inst.DB_User.Get(wonderTitleInfo.NominatedUser.UserId);
    UIManager.inst.OpenPopup("Wonder/KingdomTitleInfoPopup", (Popup.PopupParameter) new KingdomTitleInfoPopup.Parameter()
    {
      kingdomId = (userData == null ? 0 : userData.world_id),
      wonderTitleInfo = wonderTitleInfo,
      showKingBenefit = true
    });
  }

  public void OnKingsGiftBtnPressed()
  {
    UIManager.inst.OpenDlg("Wonder/WonderGiftDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private int GetPickedTabIndex()
  {
    for (int index = 0; index < this.tabCategories.Length; ++index)
    {
      if (this.tabCategories[index].value)
        return index;
    }
    return -1;
  }

  private void UpdateUI()
  {
    this.ShowKingContent();
    this.ShowWonderTitleContent(0);
    this.ShowGiftContent();
  }

  private void ShowGiftContent()
  {
    NGUITools.SetActive(this.giftPackageNode, this.kingdomId == PlayerData.inst.userData.world_id);
  }

  private void ShowKingContent()
  {
    WonderTitleInfo wonderTitleInfo = ConfigManager.inst.DB_WonderTitle.Get("1");
    if (wonderTitleInfo == null)
      return;
    if (wonderTitleInfo.IsNominated)
    {
      this.kingKingdom.text = this.kingdomId.ToString();
      this.kingName.text = wonderTitleInfo.NominatedUser.UserName;
      this.kingAlliance.text = wonderTitleInfo.NominatedUser.AllianceName;
      this.kingdomText.text = Utils.XLAT("id_kingdom") + ":";
      BuilderFactory.Instance.HandyBuild((UIWidget) this.kingTexture, "Texture/Hero/player_portrait_" + wonderTitleInfo.NominatedUser.Portrait, (System.Action<bool>) null, true, false, string.Empty);
      NGUITools.SetActive(this.kingRootNode, true);
    }
    else
      NGUITools.SetActive(this.kingRootNode, false);
  }

  private void ShowWonderTitleContent(int titleType = 0)
  {
    this.ClearData();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.titleBkgTexture, "Texture/Kingdom/" + (titleType != 0 ? "image_kingdom_title_negative" : "image_kingdom_title_positive"), (System.Action<bool>) null, false, false, string.Empty);
    List<WonderTitleInfo> titleInfoListByType = ConfigManager.inst.DB_WonderTitle.GetWonderTitleInfoListByType(titleType);
    if (titleInfoListByType != null)
    {
      for (int key = 0; key < titleInfoListByType.Count; ++key)
      {
        if (titleInfoListByType[key].id != "1")
        {
          KingdomTitleSlotRenderer itemRenderer = this.CreateItemRenderer(titleInfoListByType[key]);
          this.itemDict.Add(key, itemRenderer);
        }
      }
    }
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, KingdomTitleSlotRenderer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, KingdomTitleSlotRenderer> current = enumerator.Current;
        current.Value.ClearData();
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this.itemPool.Release(gameObject);
      }
    }
    this.itemDict.Clear();
    this.itemPool.Clear();
  }

  private void Reposition()
  {
    this.grid.repositionNow = true;
    this.grid.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private KingdomTitleSlotRenderer CreateItemRenderer(WonderTitleInfo wonderTitleInfo)
  {
    GameObject gameObject = this.itemPool.AddChild(this.grid.gameObject);
    gameObject.SetActive(true);
    KingdomTitleSlotRenderer component = gameObject.GetComponent<KingdomTitleSlotRenderer>();
    component.SetData(wonderTitleInfo, this.kingdomId, this.needNominateUserId);
    return component;
  }

  private void OnWonderDataUpdated(DB.WonderData wonderData)
  {
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Wonder.onDataUpdate += new System.Action<DB.WonderData>(this.OnWonderDataUpdated);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Wonder.onDataUpdate -= new System.Action<DB.WonderData>(this.OnWonderDataUpdated);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int kingdomId;
    public long needNominateUserId;
  }
}
