﻿// Decompiled with JetBrains decompiler
// Type: ItemTipLordTitleInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ItemTipLordTitleInfo : ItemTipBaseInfo
{
  public UISprite lordTitleIcon;

  public override void SetData(int itemId)
  {
    base.SetData(itemId);
    LordTitleMainInfo lordTitleMainInfo = ConfigManager.inst.DB_LordTitleMain.Get(ConfigManager.inst.DB_Items.GetItem(itemId).Param1);
    if (lordTitleMainInfo == null)
      return;
    this.lordTitleIcon.spriteName = lordTitleMainInfo.AvatarIcon;
  }

  public override void Dispose()
  {
    base.Dispose();
    BuilderFactory.Instance.Release((UIWidget) this.lordTitleIcon);
  }

  public Benefits GetBenefit()
  {
    return ConfigManager.inst.DB_LordTitleMain.Get(ConfigManager.inst.DB_Items.GetItem(this._itemId).Param1).benefits;
  }
}
