﻿// Decompiled with JetBrains decompiler
// Type: ItemStaticInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;

public class ItemStaticInfo
{
  private static int USE = 1;
  private static int BUILD = 2;
  private static int NOMRAL;
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "type")]
  public ItemBag.ItemType Type;
  [Config(Name = "priority")]
  public int Priority;
  [Config(Name = "value")]
  public float Value;
  [Config(Name = "image")]
  public string Image;
  [Config(Name = "usable")]
  public int Usable;
  [Config(Name = "buff")]
  public string Buff_Id;
  [Config(Name = "loc_toast_id")]
  public string Loc_Toast_Id;
  [Config(Name = "category")]
  public string Category;
  [Config(Name = "usable_level")]
  public int RequireLev;
  [Config(Name = "use_method")]
  public string UseMethod;
  [Config(Name = "para1")]
  public string Param1;
  [Config(Name = "para2")]
  public string Param2;
  [Config(Name = "para3")]
  public string Param3;
  [Config(Name = "max_use_number")]
  public int UseMaxCount;
  [Config(Name = "dungeon_weight")]
  public int DungeonWeight;
  [Config(Name = "is_post")]
  public int isPost;
  [Config(Name = "quality")]
  public int Quality;
  [Config(Name = "image_central")]
  public string image_central;
  [Config(Name = "image_central_scaling")]
  public float image_central_scaling;
  [Config(Name = "base_image_scaling")]
  public float base_image_scaling;
  [Config(Name = "image_chip")]
  public string image_chip;
  [Config(Name = "cd_group")]
  public string CDGroup;
  [Config(Name = "cd_time")]
  public float CDTime;
  [Config(Name = "goto_buy")]
  public int gotoBuy;

  public string Loc_Name_Id
  {
    get
    {
      return this.ID + "_name";
    }
  }

  public string Loc_Description_Id
  {
    get
    {
      return this.ID + "_description";
    }
  }

  public bool CanUse
  {
    get
    {
      return this.Usable == ItemStaticInfo.USE;
    }
  }

  public int RemainTime
  {
    get
    {
      if (this.Type == ItemBag.ItemType.peaceshield)
        return PlayerData.inst.playerCityData.LeftPeaceShieldCdTime;
      if (string.IsNullOrEmpty(this.CDGroup) || (double) this.CDTime <= 0.0)
        return 0;
      StatsData statsData = DBManager.inst.DB_Stats.Get("item_cd_group_" + this.CDGroup);
      if (statsData != null)
        return (int) (statsData.count - (long) NetServerTime.inst.ServerTimestamp);
      return 0;
    }
  }

  public bool CanBuild
  {
    get
    {
      return this.Usable == ItemStaticInfo.BUILD;
    }
  }

  public string ImagePath
  {
    get
    {
      return string.Format("Texture/ItemIcons/{0}", (object) this.ID);
    }
  }

  public string LocName
  {
    get
    {
      return ScriptLocalization.Get(this.Loc_Name_Id, true);
    }
  }

  public string LocDescription
  {
    get
    {
      if (this.Type == ItemBag.ItemType.equipment_gem)
      {
        ConfigEquipmentGemInfo dataByItemId = ConfigManager.inst.DB_EquipmentGem.GetDataByItemId(this.internalId);
        if (dataByItemId != null && dataByItemId.gemType != 0)
          return ScriptLocalization.GetWithPara(this.Loc_Description_Id, new Dictionary<string, string>()
          {
            {
              "1",
              string.Format("\n{0}", (object) GemManager.Instance.GetGemBenefitsDescription(dataByItemId.benefits))
            }
          }, true);
      }
      return ScriptLocalization.Get(this.Loc_Description_Id, true);
    }
  }

  public bool InStore
  {
    get
    {
      switch (this.Type)
      {
        case ItemBag.ItemType.equipment:
        case ItemBag.ItemType.equipment_scroll_chip:
        case ItemBag.ItemType.equipment_material:
        case ItemBag.ItemType.equipment_scroll:
        case ItemBag.ItemType.dk_equipment:
        case ItemBag.ItemType.dk_equipment_scroll_chip:
        case ItemBag.ItemType.dk_equipment_scroll:
        case ItemBag.ItemType.dk_equipment_material:
        case ItemBag.ItemType.parliament_hero_exp:
        case ItemBag.ItemType.parliament_hero_card_chip:
          return false;
        default:
          return true;
      }
    }
  }
}
