﻿// Decompiled with JetBrains decompiler
// Type: MsgHttpPostProcessor
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using System.Text;
using UnityEngine;

public class MsgHttpPostProcessor : MonoBehaviour
{
  private string _url;
  private MsgChecker _postData;
  private MsgHttpPostProcessor.MsgPostCallback _callback;
  private byte[] _jsonBytes;

  public void DoPost(string url, MsgChecker postData, MsgHttpPostProcessor.MsgPostCallback callback)
  {
    this._url = url;
    this._postData = postData;
    this._callback = callback;
    this._jsonBytes = this._postData.ToJsonBytes();
    this.StartCoroutine(this.PostCoroutine());
  }

  public void DoPost(string url, string jsonMsg)
  {
    this._url = url;
    this._postData = (MsgChecker) null;
    this._callback = (MsgHttpPostProcessor.MsgPostCallback) null;
    this._jsonBytes = Encoding.UTF8.GetBytes(jsonMsg);
    this.StartCoroutine(this.PostCoroutine());
  }

  [DebuggerHidden]
  public IEnumerator PostCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MsgHttpPostProcessor.\u003CPostCoroutine\u003Ec__Iterator2F()
    {
      \u003C\u003Ef__this = this
    };
  }

  public delegate void MsgPostCallback(bool result, MsgResponseStatus status);
}
