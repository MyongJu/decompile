﻿// Decompiled with JetBrains decompiler
// Type: MerlinMysteryStorePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinMysteryStorePopup : Popup
{
  private Dictionary<int, MerlinMysterySlot> _itemDict = new Dictionary<int, MerlinMysterySlot>();
  private Dictionary<int, MerlinTrialsRewardSlot> _rewardItemDict = new Dictionary<int, MerlinTrialsRewardSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UILabel _labelExpiredTime;
  [SerializeField]
  private ItemIconRenderer _itemIcon;
  [SerializeField]
  private UILabel _labelItemName;
  [SerializeField]
  private UILabel _labelItemDescription;
  [SerializeField]
  private UILabel _labelNoSelectedHint;
  [SerializeField]
  private UISprite _spriteSelectedFrame;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UITable _table;
  [SerializeField]
  private GameObject _itemPrefab;
  [SerializeField]
  private UIScrollView _rewardScrollView;
  [SerializeField]
  private UIGrid _rewardGrid;
  [SerializeField]
  private GameObject _rewardItemPrefab;
  private int _merlinTrialsMainId;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    MerlinMysteryStorePopup.Parameter parameter = orgParam as MerlinMysteryStorePopup.Parameter;
    if (parameter != null)
      this._merlinTrialsMainId = parameter.merlinTrialsMainId;
    this.AddEventHandler();
    this.OnSecond(0);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.HideLeftContent();
    this.ClearReward();
    this.ClearData();
    this.RemoveEventHandler();
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  private void RemoveEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  private void OnSecond(int time)
  {
    this._labelExpiredTime.text = Utils.FormatTime(MerlinTrialsHelper.inst.GetMysteryShopEndTime() - NetServerTime.inst.ServerTimestamp, true, false, true);
  }

  private void UpdateUI()
  {
    this._itemPool.Initialize(this._itemPrefab, this._table.gameObject);
    this.ClearData();
    List<MerlinTrialsShopSpecificInfo> shopSpecificInfoList = ConfigManager.inst.DB_MerlinTrialsShopSpecific.GetInfoList();
    if (this._merlinTrialsMainId > 0)
    {
      MerlinTrialsMainInfo merlinTrialsMainInfo = ConfigManager.inst.DB_MerlinTrialsMain.Get(this._merlinTrialsMainId);
      if (merlinTrialsMainInfo != null)
        shopSpecificInfoList = merlinTrialsMainInfo.MerlinTrialsIapInfoList;
    }
    if (shopSpecificInfoList != null)
    {
      for (int key = 0; key < shopSpecificInfoList.Count; ++key)
      {
        MerlinMysterySlot slot = this.GenerateSlot(shopSpecificInfoList[key]);
        this._itemDict.Add(key, slot);
      }
    }
    this.Reposition();
    this._labelNoSelectedHint.text = Utils.XLAT("trial_mystery_shop_no_good");
    this.HideLeftContent();
  }

  private void HideLeftContent()
  {
    NGUITools.SetActive(this._labelNoSelectedHint.gameObject, true);
    NGUITools.SetActive(this._spriteSelectedFrame.gameObject, false);
    NGUITools.SetActive(this._itemIcon.gameObject, false);
    NGUITools.SetActive(this._labelItemName.gameObject, false);
    NGUITools.SetActive(this._labelItemDescription.gameObject, false);
  }

  private void ShowLeftContent(MerlinTrialsShopSpecificInfo shopInfo)
  {
    if (shopInfo != null)
    {
      this._itemIcon.SetData(shopInfo.ImagePath);
      this._labelItemName.text = shopInfo.Name;
      this._labelItemDescription.text = shopInfo.Description;
    }
    NGUITools.SetActive(this._labelNoSelectedHint.gameObject, false);
    NGUITools.SetActive(this._spriteSelectedFrame.gameObject, true);
    NGUITools.SetActive(this._itemIcon.gameObject, true);
    NGUITools.SetActive(this._labelItemName.gameObject, true);
    NGUITools.SetActive(this._labelItemDescription.gameObject, true);
    this.ShowReward(shopInfo);
  }

  private void ShowReward(MerlinTrialsShopSpecificInfo shopInfo)
  {
    this._itemPool.Initialize(this._rewardItemPrefab, this._rewardGrid.gameObject);
    this.ClearReward();
    if (shopInfo != null && shopInfo.rewards != null && shopInfo.rewards.rewards != null)
    {
      using (Dictionary<string, int>.Enumerator enumerator = shopInfo.rewards.rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int result = 0;
          int count = current.Value;
          int.TryParse(current.Key, out result);
          MerlinTrialsRewardSlot rewardSlot = this.GenerateRewardSlot(result, count);
          if (!this._rewardItemDict.ContainsKey(result))
            this._rewardItemDict.Add(result, rewardSlot);
        }
      }
    }
    this.RepositionReward();
  }

  private void ClearReward()
  {
    using (Dictionary<int, MerlinTrialsRewardSlot>.Enumerator enumerator = this._rewardItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._rewardItemDict.Clear();
    this._itemPool.Clear();
  }

  private void RepositionReward()
  {
    this._rewardGrid.repositionNow = true;
    this._rewardGrid.Reposition();
    this._rewardScrollView.ResetPosition();
  }

  private MerlinTrialsRewardSlot GenerateRewardSlot(int itemId, int count)
  {
    GameObject gameObject = this._itemPool.AddChild(this._rewardGrid.gameObject);
    gameObject.SetActive(true);
    MerlinTrialsRewardSlot component = gameObject.GetComponent<MerlinTrialsRewardSlot>();
    component.SetData(itemId, count, false);
    return component;
  }

  private void OnItemPressed(MerlinTrialsShopSpecificInfo shopInfo)
  {
    this.ShowLeftContent(shopInfo);
  }

  private void ClearData()
  {
    using (Dictionary<int, MerlinMysterySlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, MerlinMysterySlot> current = enumerator.Current;
        current.Value.OnItemPressCallback -= new System.Action<MerlinTrialsShopSpecificInfo>(this.OnItemPressed);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this._table.repositionNow = true;
    this._table.Reposition();
    this._scrollView.ResetPosition();
  }

  private MerlinMysterySlot GenerateSlot(MerlinTrialsShopSpecificInfo shopInfo)
  {
    GameObject gameObject = this._itemPool.AddChild(this._table.gameObject);
    gameObject.SetActive(true);
    MerlinMysterySlot component = gameObject.GetComponent<MerlinMysterySlot>();
    component.SetData(shopInfo);
    component.OnItemPressCallback += new System.Action<MerlinTrialsShopSpecificInfo>(this.OnItemPressed);
    return component;
  }

  public class Parameter : Popup.PopupParameter
  {
    public int merlinTrialsMainId;
  }
}
