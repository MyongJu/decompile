﻿// Decompiled with JetBrains decompiler
// Type: UsedTips
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UsedTips : MonoBehaviour
{
  public UILabel labelMsg;

  private void Start()
  {
  }

  private void Update()
  {
    this.transform.Translate(Vector3.up * Time.deltaTime * 0.2f);
  }

  public void SetContent(string msg, Vector3 pos)
  {
    this.labelMsg.text = string.Format("You successfully used a {0}", (object) msg);
    this.transform.position = pos;
    this.gameObject.SetActive(true);
    Object.Destroy((Object) this.gameObject, 3f);
  }
}
