﻿// Decompiled with JetBrains decompiler
// Type: ConfigFilesVerListState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.IO;
using UnityEngine;

public class ConfigFilesVerListState : LoadBaseState
{
  private bool local = true;
  private string localConfigVersionPath;

  public ConfigFilesVerListState(int step)
    : base(step)
  {
  }

  protected override SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.ConfigFileVersion;
    }
  }

  public override string Key
  {
    get
    {
      return nameof (ConfigFilesVerListState);
    }
  }

  protected override void Prepare()
  {
    base.Prepare();
    this.LoadLocal();
  }

  protected override void OnAllFileFinish(LoaderBatch batch)
  {
    LoaderInfo resultInfo = batch.GetResultInfo("ConfigVersionList.json");
    if (resultInfo != null && resultInfo.ResData == null)
    {
      if (this.local)
      {
        this.IsFail = false;
        this.LoadRemote();
      }
      else
        base.OnAllFileFinish(batch);
    }
    else
    {
      base.OnAllFileFinish(batch);
      object obj = Utils.Json2Object(Utils.ByteArray2String(resultInfo.ResData), true);
      if (this.local)
      {
        if (obj != null)
          this.Preloader._localConfigVerHt = obj as Hashtable;
        else
          this.Preloader._localConfigVerHt = new Hashtable();
        this.LoadRemote();
      }
      else
      {
        if (this.Preloader._localConfigVerHt == null)
          this.Preloader._localConfigVerHt = new Hashtable();
        this.Preloader._remoteConfigVerHt = obj as Hashtable;
        this.Finish();
      }
    }
  }

  protected override void Retry()
  {
    base.Retry();
    if (this.local)
      return;
    this.LoadRemote();
  }

  private void LoadLocal()
  {
    this.CanRetry = false;
    try
    {
      this.ClearOldConfig();
      this.CheckFile(NativeManager.inst.AppMajorVersion + (object) '=' + "config.zip");
    }
    catch
    {
      return;
    }
    this.local = true;
    this.batch.Add("ConfigVersionList.json", NetApi.inst.BuildLocalUrl("ConfigVersionList.json"), (Hashtable) null, false, 3, false);
    LoaderManager.inst.Add(this.batch, false, false);
  }

  private void LoadRemote()
  {
    this.CanRetry = true;
    this.local = false;
    this.RemoveEventHandler();
    this.batch = LoaderManager.inst.PopBatch();
    this.AddEventHandler();
    bool flag = false;
    this.localConfigVersionPath = AssetConfig.BundleCacheDir + "/local_config_version";
    try
    {
      if (File.Exists(this.localConfigVersionPath))
        flag = File.ReadAllText(this.localConfigVersionPath) == NetApi.inst.ConfigFilesVersion;
    }
    catch
    {
    }
    if (!flag)
      this.batch.Add(this.GetLoaderInfo("ConfigVersionList.json", NetApi.inst.BuildConfigUrl("VersionList.json")));
    else
      this.batch.Add("ConfigVersionList.json", NetApi.inst.BuildLocalBundleUrl("ConfigVersionList.json"), (Hashtable) null, false, 3, false);
    LoaderManager.inst.Add(this.batch, false, false);
  }

  protected override void Finish()
  {
    if (this.IsDestroy)
      return;
    base.Finish();
    this.RefreshProgress();
    this.Preloader.OnConfigFilseVerListFinish();
  }

  private void CheckFile(string fileName)
  {
    string str1 = Path.Combine(Application.persistentDataPath, fileName);
    string str2 = Path.Combine(BuildConfig.CONFIG_CACHE_FOLDER, fileName);
    if (File.Exists(str1))
      return;
    if (str2.Contains("://"))
    {
      WWW www = new WWW(str2);
      do
        ;
      while (!www.isDone);
      if (string.IsNullOrEmpty(www.error))
        File.WriteAllBytes(str1, www.bytes);
    }
    else if (File.Exists(str2))
      File.Copy(str2, str1, true);
    string err;
    SharpZipUtil.UnZipFile(str1, Application.persistentDataPath, out err);
  }

  private void ClearOldConfig()
  {
    if (!Directory.Exists(AssetConfig.BundleCacheDir))
      return;
    foreach (FileInfo file in new DirectoryInfo(AssetConfig.BundleCacheDir).GetFiles())
    {
      string[] strArray = file.Name.Split('=');
      if (strArray.Length == 2 && (!(strArray[1] != "config.zip") && strArray[0] != NativeManager.inst.AppMajorVersion))
        file.Delete();
    }
  }
}
