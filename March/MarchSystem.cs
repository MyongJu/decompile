﻿// Decompiled with JetBrains decompiler
// Type: March.MarchSystem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

namespace March
{
  public class MarchSystem
  {
    public static string[] MissilePath = new string[2]
    {
      "Prefab/MissileSystem/Missile_Range_1",
      "Prefab/MissileSystem/Missile_arty_1"
    };
    protected Dictionary<long, March.March> _raylly2marchAttack = new Dictionary<long, March.March>();
    private Dictionary<long, long> _rally2March = new Dictionary<long, long>();
    private HubPort marchPort = MessageHub.inst.GetPortByAction("PVP:startMarch");
    private HubPort scoutPort = MessageHub.inst.GetPortByAction("PVP:startScout");
    private HubPort tradePort = MessageHub.inst.GetPortByAction("Alliance:offerTradeHelp");
    private HubPort visitPort = MessageHub.inst.GetPortByAction("PVP:startVisitNpc");
    private HubPort recallPort = MessageHub.inst.GetPortByAction("PVP:recallMarch");
    private HubPort joinRallyPort = MessageHub.inst.GetPortByAction("Rally:joinRally");
    private HubPort startRallyPort = MessageHub.inst.GetPortByAction("Rally:startRally");
    private HubPort reinforcePort = MessageHub.inst.GetPortByAction("PVP:startReinforce");
    private HubPort reinforceFortressPort = MessageHub.inst.GetPortByAction("PVP:startJoinFortress");
    private HubPort reinforceWonderPort = MessageHub.inst.GetPortByAction("PVP:startJoinWonder");
    private HubPort reinforceTemplePort = MessageHub.inst.GetPortByAction("PVP:startJoinTemple");
    private HubPort sendFortressBackPort = MessageHub.inst.GetPortByAction("Alliance:sendFortressTroopsBack");
    private HubPort sendAllianceBuildingBackPort = MessageHub.inst.GetPortByAction("Alliance:sendAllianceBuildingTroopsBack");
    private HubPort marchPushPort = MessageHub.inst.GetPortByAction("march");
    private HubPort marchRecall = MessageHub.inst.GetPortByAction("recall");
    private HubPort marchRemove = MessageHub.inst.GetPortByAction("marchDisappear");
    private HubPort cancelRall = MessageHub.inst.GetPortByAction("cancel_rally");
    private HubPort marchCollition = MessageHub.inst.GetPortByAction("marchCollision");
    private HubPort startGveRallyPort = MessageHub.inst.GetPortByAction("Rally:startGveRally");
    private HubPort startRabRallyPort = MessageHub.inst.GetPortByAction("Rally:startAllianceBossRally");
    private HubPort startTradeFetchWareHouse = MessageHub.inst.GetPortByAction("PVP:startTradeWarehouse");
    private HubPort sendWonderTroopsBackPort = MessageHub.inst.GetPortByAction("Wonder:sendWonderTroopsBack");
    private const int MIN_LEVEL_PLAYER_CAN_BE_RALLY_WITHOUT_ALLIANCE = 20;
    protected Dictionary<long, March.March> _marchs;
    private List<long> _disposeMarchIdList;
    private bool isReady;

    public MarchSystem()
    {
      this._marchs = new Dictionary<long, March.March>();
      this._disposeMarchIdList = new List<long>();
      DBManager.inst.DB_March.onDataCreate += new System.Action<long>(this.OnMarchDataCreate);
      DBManager.inst.DB_March.onDataChanged += new System.Action<long>(this.OnMarchDataChanged);
      DBManager.inst.DB_March.onDataRemove += new System.Action<long>(this.OnMarchDataRemoved);
      this.marchPushPort.AddEvent(new System.Action<object>(this.OnMarchCommandPushRecived));
      this.marchRecall.AddEvent(new System.Action<object>(this.RecallHandler));
      this.marchRemove.AddEvent(new System.Action<object>(this.RemoveMarchHandler));
      this.cancelRall.AddEvent(new System.Action<object>(this.OnRallCancelHandler));
      this.marchCollition.AddEvent(new System.Action<object>(this.OnMarchCollitionHandler));
      for (int index = 0; index < 2; ++index)
        MissileSystemManager.Instance.RegisterMissileSystem(((MarchSystem.MissileType) index).ToString(), MarchSystem.MissilePath[index]);
    }

    public Dictionary<long, March.March> marches
    {
      get
      {
        return this._marchs;
      }
    }

    public bool HasMarchToWonder()
    {
      bool flag = DBManager.inst.DB_March.HasWonderMarch();
      if (!flag)
        flag = DBManager.inst.DB_Rally.HasJoinWonderRally();
      return flag;
    }

    public int allianceWarCount
    {
      get
      {
        List<MarchData> inCurrentKingdom1 = DBManager.inst.DB_March.GetMarchesByAllianceIdInCurrentKingdom(PlayerData.inst.allianceId);
        List<RallyData> inCurrentKingdom2 = DBManager.inst.DB_Rally.GetRallyesByAllianceIdInCurrentKingdom(PlayerData.inst.allianceId);
        int num1 = inCurrentKingdom2 != null ? inCurrentKingdom2.Count : 0;
        int num2 = 0;
        int num3 = 0;
        for (int index = 0; index < inCurrentKingdom1.Count; ++index)
        {
          if ((inCurrentKingdom1[index].state & (MarchData.MarchState.marching | MarchData.MarchState.rally_attacking)) > MarchData.MarchState.invalid && (inCurrentKingdom1[index].ownerAllianceId == PlayerData.inst.allianceId && (inCurrentKingdom1[index].type == MarchData.MarchType.city_attack || inCurrentKingdom1[index].type == MarchData.MarchType.encamp_attack || (inCurrentKingdom1[index].type == MarchData.MarchType.gather_attack || inCurrentKingdom1[index].type == MarchData.MarchType.fortress_attack) || (inCurrentKingdom1[index].type == MarchData.MarchType.temple_attack || inCurrentKingdom1[index].type == MarchData.MarchType.wonder_attack || inCurrentKingdom1[index].type == MarchData.MarchType.dig_attack)) || inCurrentKingdom1[index].targetAllianceId == PlayerData.inst.allianceId && (inCurrentKingdom1[index].type == MarchData.MarchType.city_attack || inCurrentKingdom1[index].type == MarchData.MarchType.encamp_attack || (inCurrentKingdom1[index].type == MarchData.MarchType.gather_attack || inCurrentKingdom1[index].type == MarchData.MarchType.fortress_attack) || (inCurrentKingdom1[index].type == MarchData.MarchType.temple_attack || inCurrentKingdom1[index].type == MarchData.MarchType.wonder_attack || inCurrentKingdom1[index].type == MarchData.MarchType.dig_attack))))
            ++num2;
          if (inCurrentKingdom1[index].targetAllianceId == PlayerData.inst.allianceId && inCurrentKingdom1[index].state == MarchData.MarchState.demolishing_fortress)
            num3 = 1;
        }
        int count = DBManager.inst.DB_AllianceMagic.GetAllianceWarMagicList().Count;
        return num1 + num2 + num3 + count;
      }
    }

    private void OnMarchCollitionHandler(object orgSrc)
    {
      Hashtable data;
      if (!DatabaseTools.CheckAndParseOrgData(orgSrc, out data))
        return;
      string empty = string.Empty;
      int outData1 = 0;
      int outData2 = 0;
      DatabaseTools.UpdateData(data, "player", ref empty);
      DatabaseTools.UpdateData(data, "x", ref outData1);
      DatabaseTools.UpdateData(data, "y", ref outData2);
      UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
      {
        title = Utils.XLAT("id_uppercase_warning"),
        description = string.Format(Utils.XLAT("march_same_destination_warning"), (object) empty, (object) string.Format("X : {0} Y : {1}", (object) outData1, (object) outData2)),
        buttonClickEvent = (System.Action) null
      });
    }

    private void OnRallCancelHandler(object orgSrc)
    {
      if (!GameEngine.IsReady())
        D.error((object) "[MessageCallBack] OnRallCancelHandler, GameEngine is not ready!");
      else if (this._marchs == null)
      {
        D.error((object) "[MessageCallBack] OnRallCancelHandler, _marchs is not ready!");
      }
      else
      {
        if (orgSrc == null)
          return;
        Hashtable hashtable = orgSrc as Hashtable;
        if (hashtable == null || !hashtable.Contains((object) "rally_id") || !this.IsValidGameMode)
          return;
        long key1 = long.Parse(hashtable[(object) "rally_id"].ToString());
        long key2 = 0;
        March.March march = (March.March) null;
        if (this._rally2March.ContainsKey(key1))
          this._rally2March.Remove(key1);
        if (this._raylly2marchAttack.ContainsKey(key1))
        {
          march = this._raylly2marchAttack[key1];
          if (march != null && march.marchData != null)
          {
            key2 = march.marchData.marchId;
            if (this._marchs.ContainsKey(march.marchData.marchId))
              this._marchs.Remove(key2);
            this._raylly2marchAttack.Remove(key1);
          }
        }
        Dictionary<long, March.March>.Enumerator enumerator = this._marchs.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.Value != null && enumerator.Current.Value.marchData != null && enumerator.Current.Value.marchData.rallyId == key1)
          {
            enumerator.Current.Value.RallyAttackDone();
            if (enumerator.Current.Value.marchData.marchId != key2)
              enumerator.Current.Value.Recall();
          }
        }
        if (march == null)
          return;
        march.Dispose();
      }
    }

    public void MissileShoot(MarchSystem.MissileType type, Vector3 startPos, Vector3 endPos, int id = 0)
    {
      MissileSystem missileSystem = MissileSystemManager.Instance.GetMissileSystem(type.ToString());
      if (!((UnityEngine.Object) missileSystem != (UnityEngine.Object) null))
        return;
      missileSystem.Shoot(startPos, endPos, id);
    }

    public void StopMissileShoot(MarchSystem.MissileType type, int id = 0)
    {
      MissileSystem missileSystem = MissileSystemManager.Instance.GetMissileSystem(type.ToString());
      if (!((UnityEngine.Object) missileSystem != (UnityEngine.Object) null))
        return;
      missileSystem.Stop(id);
    }

    public void ClearAllMissileAsset(MarchSystem.MissileType type)
    {
      MissileSystem missileSystem = MissileSystemManager.Instance.GetMissileSystem(type.ToString());
      if (!((UnityEngine.Object) missileSystem != (UnityEngine.Object) null))
        return;
      missileSystem.ClearMissiles();
      missileSystem.ClearPools();
    }

    public void MarchDoneHandler(long rallyId, List<long> ids)
    {
      if (rallyId <= 0L)
        return;
      if (rallyId > 0L && this._rally2March.ContainsKey(rallyId))
        this._rally2March.Remove(rallyId);
      Dictionary<long, March.March>.KeyCollection.Enumerator enumerator = this.marches.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        long current = enumerator.Current;
        if (this.marches.ContainsKey(current) && this.marches[current] != null && this.marches[current].marchData != null && (this.marches[current].marchData.rallyId == rallyId || ids.Contains(current)))
          this.marches[current].RallyAttackDone();
      }
    }

    private void RemoveMarchHandler(object orgSrc)
    {
      if (orgSrc == null)
        return;
      Hashtable hashtable = orgSrc as Hashtable;
      if (hashtable == null || !hashtable.Contains((object) "march_id") || !this.IsValidGameMode)
        return;
      long key = long.Parse(hashtable[(object) "march_id"].ToString());
      if (!this._marchs.ContainsKey(key))
        return;
      this._marchs[key].Dispose();
      this._marchs.Remove(key);
    }

    private void RecallHandler(object orgSrc)
    {
      if (orgSrc == null)
        return;
      Hashtable hashtable = orgSrc as Hashtable;
      if (hashtable == null || !hashtable.Contains((object) "march_id") || !this.IsValidGameMode)
        return;
      long key = long.Parse(hashtable[(object) "march_id"].ToString());
      if (!this.marches.ContainsKey(key))
        return;
      this.marches[key].Recall();
    }

    public void StartMarch(MarchAllocDlg.MarchType marchType, Coordinate target, Hashtable troops, bool withDragon, System.Action<bool, object> OnStartMarchCallback = null)
    {
      switch (marchType)
      {
        case MarchAllocDlg.MarchType.cityAttack:
          this.SendMarchCall(target, troops, "city_attack", withDragon, OnStartMarchCallback);
          break;
        case MarchAllocDlg.MarchType.encampAttack:
          this.SendMarchCall(target, troops, "encamp_attack", withDragon, OnStartMarchCallback);
          break;
        case MarchAllocDlg.MarchType.gatherAttack:
          this.SendMarchCall(target, troops, "gather_attack", withDragon, OnStartMarchCallback);
          break;
        case MarchAllocDlg.MarchType.fortressReinforce:
          this.StartFortressReinforce(target, troops, withDragon, OnStartMarchCallback);
          break;
        case MarchAllocDlg.MarchType.fortressAttack:
          this.StartFortressAttack(target, troops, withDragon, OnStartMarchCallback);
          break;
        case MarchAllocDlg.MarchType.wonderAttack:
          this.SendMarchCall(target, troops, "wonder_attack", withDragon, OnStartMarchCallback);
          break;
        case MarchAllocDlg.MarchType.templeReinforce:
          this.SendMarchCall(target, troops, "temple_reinforce", withDragon, OnStartMarchCallback);
          break;
        case MarchAllocDlg.MarchType.templeAttack:
          this.SendMarchCall(target, troops, "temple_attack", withDragon, OnStartMarchCallback);
          break;
        case MarchAllocDlg.MarchType.allianceCityBuild:
          this.StartAllianceBuild(target, troops, withDragon, OnStartMarchCallback);
          break;
        case MarchAllocDlg.MarchType.gather:
          this.StartGather(target, troops, withDragon, OnStartMarchCallback);
          break;
        case MarchAllocDlg.MarchType.encamp:
          this.StartEncamp(target, troops, withDragon, OnStartMarchCallback);
          break;
        case MarchAllocDlg.MarchType.monsterAttack:
          this.StartAttackMonster(target, troops, withDragon, OnStartMarchCallback);
          break;
        case MarchAllocDlg.MarchType.worldBossAttack:
          this.SendMarchCall(target, troops, "world_boss_attack", withDragon, OnStartMarchCallback);
          break;
        case MarchAllocDlg.MarchType.miniWonderAttack:
          this.StartAttackMiniWonder(target, troops, withDragon, OnStartMarchCallback);
          break;
        case MarchAllocDlg.MarchType.dig:
          this.StartDig(target, troops, withDragon, OnStartMarchCallback);
          break;
        case MarchAllocDlg.MarchType.digAttack:
          this.SendMarchCall(target, troops, "dig_attack", withDragon, OnStartMarchCallback);
          break;
      }
    }

    public void StartAttack(Coordinate target, Hashtable troops, bool withDragon, System.Action<bool, object> callback = null)
    {
      this.SendMarchCall(target, troops, "attack", withDragon, callback);
    }

    public void StartScout(Coordinate target)
    {
      if (this.isMarchSlotFull)
        this.ShowMaxMarchSlotMessage();
      else if (target.K != PlayerData.inst.playerCityData.cityLocation.K)
        this.ShopWaringMessage(MarchSystem.LocalMarchCheckType.not_in_same_kingdom);
      else if (PlayerData.inst.playerCityData.peaceShieldJobId != 0L)
      {
        GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("break_shield_add_cd");
        Dictionary<string, string> para = new Dictionary<string, string>()
        {
          {
            "0",
            (data == null ? 0 : data.ValueInt / 60).ToString()
          }
        };
        UIManager.inst.OpenPopup("ConfirmWithPeaceShield", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
        {
          setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
          title = ScriptLocalization.Get("id_uppercase_confirm", true),
          description = ScriptLocalization.GetWithPara("peace_shield_break_cooldown_warning", para, true),
          leftButtonText = ScriptLocalization.Get("id_uppercase_no", true),
          rightButtonText = ScriptLocalization.Get("id_uppercase_yes", true),
          leftButtonClickEvent = (System.Action) null,
          rightButtonClickEvent = (System.Action) (() => this.scoutPort.SendRequest(new Hashtable()
          {
            {
              (object) "city_id",
              (object) GameEngine.Instance.PlayerData.cityId
            },
            {
              (object) "job_id",
              (object) ClientIdCreater.CreateLongId(ClientIdCreater.IdType.EVENT).ToString()
            },
            {
              (object) "type",
              (object) "scout"
            },
            {
              (object) "k",
              (object) target.K
            },
            {
              (object) "x",
              (object) target.X
            },
            {
              (object) "y",
              (object) target.Y
            }
          }, (System.Action<bool, object>) null, true))
        });
      }
      else
        this.scoutPort.SendRequest(new Hashtable()
        {
          {
            (object) "city_id",
            (object) GameEngine.Instance.PlayerData.cityId
          },
          {
            (object) "job_id",
            (object) ClientIdCreater.CreateLongId(ClientIdCreater.IdType.EVENT).ToString()
          },
          {
            (object) "type",
            (object) "scout"
          },
          {
            (object) "k",
            (object) target.K
          },
          {
            (object) "x",
            (object) target.X
          },
          {
            (object) "y",
            (object) target.Y
          }
        }, (System.Action<bool, object>) null, true);
    }

    private void ShopWaringMessage(string message)
    {
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("id_uppercase_warning"),
        Content = message,
        Okay = Utils.XLAT("id_uppercase_okay"),
        OkayCallback = (System.Action) null
      });
    }

    private void ShopWaringMessage(MarchSystem.LocalMarchCheckType type)
    {
      this.ShopWaringMessage(this.GetCheckTypeMsg(type));
    }

    public void ShowMaxMarchSlotMessage()
    {
      bool vipActive = PlayerData.inst.hostPlayer.VIPActive;
      bool flag = true;
      string researchID = string.Empty;
      for (int index = 1; index <= 3; ++index)
      {
        string techLevelID = string.Format("research_troop_march_limit_{0}_1", (object) index);
        flag &= ResearchManager.inst.IsResearched(techLevelID);
        if (!flag)
        {
          researchID = techLevelID;
          break;
        }
      }
      int marchSlot = PlayerData.inst.playerCityData.marchSlot;
      if (!vipActive && !flag)
        UIManager.inst.OpenPopup("MessageBoxWith2ButtonsPopup", (Popup.PopupParameter) new MessageBoxWith2ButtonsPopup.Parameter()
        {
          titleString = Utils.XLAT("vip_tip_title"),
          contentString = string.Format("{0} \n \n {1}", (object) string.Format("{0}" + Utils.XLAT("vip_march_slots_full_description"), (object) string.Empty, (object) marchSlot), (object) Utils.XLAT("vip_research_march_slots_description")),
          leftLabelString = Utils.XLAT("vip_go_to_university_button"),
          rightLabelString = Utils.XLAT("vip_activate_button"),
          onLeftBtnClick = (System.Action) (() =>
          {
            if (CityManager.inst.GetBuildingsByType("university").Count < 1)
            {
              UIManager.inst.toast.Show(Utils.XLAT("vip_tip_build_university_description"), (System.Action) null, 4f, false);
            }
            else
            {
              ResearchDetailDlg.Parameter param = new ResearchDetailDlg.Parameter();
              param.techLevelInternalID = ConfigManager.inst.DB_TechLevels[researchID].InternalID;
              Utils.ExecuteAtTheEndOfFrame((System.Action) (() =>
              {
                UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
                UIManager.inst.OpenDlg("Research/ResearchDetailDlg", (UI.Dialog.DialogParameter) param, true, true, true);
              }));
            }
          }),
          onRightBtnClick = (System.Action) (() =>
          {
            UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
            UIManager.inst.OpenDlg("VIP/VipBaseDlg", (UI.Dialog.DialogParameter) null, true, true, true);
          })
        });
      else if (!vipActive)
        UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
        {
          Title = Utils.XLAT("vip_tip_title"),
          Content = string.Format(Utils.XLAT("vip_march_slots_full_description"), (object) marchSlot),
          Okay = Utils.XLAT("vip_activate_button"),
          OkayCallback = (System.Action) (() =>
          {
            UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
            UIManager.inst.OpenDlg("VIP/VipBaseDlg", (UI.Dialog.DialogParameter) null, true, true, true);
          })
        });
      else if (!flag)
        UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
        {
          Title = Utils.XLAT("vip_tip_title"),
          Content = string.Format("{0}" + Utils.XLAT("vip_research_march_slots_description"), (object) string.Empty, (object) marchSlot),
          Okay = Utils.XLAT("vip_go_to_university_button"),
          OkayCallback = (System.Action) (() =>
          {
            if (CityManager.inst.GetBuildingsByType("university").Count < 1)
            {
              UIManager.inst.toast.Show(Utils.XLAT("vip_tip_build_university_description"), (System.Action) null, 4f, false);
            }
            else
            {
              ResearchDetailDlg.Parameter param = new ResearchDetailDlg.Parameter();
              param.techLevelInternalID = ConfigManager.inst.DB_TechLevels[researchID].InternalID;
              Utils.ExecuteAtTheEndOfFrame((System.Action) (() =>
              {
                UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
                UIManager.inst.OpenDlg("Research/ResearchDetailDlg", (UI.Dialog.DialogParameter) param, true, true, true);
              }));
            }
          })
        });
      else
        UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
        {
          Title = Utils.XLAT("vip_tip_title"),
          Content = Utils.XLAT("march_march_slots_exceeded_warning"),
          Okay = "OK"
        });
    }

    public void StartGather(Coordinate target, Hashtable troops, bool withDragon, System.Action<bool, object> callback = null)
    {
      this.SendMarchCall(target, troops, "gather", withDragon, callback);
    }

    public void StartEncamp(Coordinate target, Hashtable troops, bool withDragon, System.Action<bool, object> callback = null)
    {
      this.SendMarchCall(target, troops, "encamp", withDragon, callback);
    }

    public void StartDig(Coordinate target, Hashtable troops, bool withDragon, System.Action<bool, object> callback = null)
    {
      this.SendMarchCall(target, troops, "dig", withDragon, callback);
    }

    public void StartAttackMonster(Coordinate target, Hashtable troops, bool withDragon, System.Action<bool, object> callback = null)
    {
      this.SendMarchCall(target, troops, "monster_attack", withDragon, callback);
    }

    public void StartAttackMiniWonder(Coordinate target, Hashtable troops, bool withDragon, System.Action<bool, object> callback = null)
    {
      this.SendMarchCall(target, troops, "wonder_attack", withDragon, callback);
    }

    public void StartFortressReinforce(Coordinate target, Hashtable troops, bool withDragon, System.Action<bool, object> callback = null)
    {
      this.SendMarchCall(target, troops, "fortress_reinforce", withDragon, callback);
    }

    public void StartFortressAttack(Coordinate target, Hashtable troops, bool withDragon, System.Action<bool, object> callback = null)
    {
      this.SendMarchCall(target, troops, "fortress_attack", withDragon, callback);
    }

    public void StartAllianceBuild(Coordinate target, Hashtable troops, bool withDragon, System.Action<bool, object> callback = null)
    {
      this.SendMarchCall(target, troops, "alliance_city_build", withDragon, callback);
    }

    public void StartVisit(Coordinate location, System.Action<bool, object> callback = null)
    {
      if (this.isMarchSlotFull)
        this.ShowMaxMarchSlotMessage();
      else
        this.visitPort.SendRequest(Utils.Hash((object) "k", (object) location.K, (object) "x", (object) location.X, (object) "y", (object) location.Y), callback, true);
    }

    public void StartTrade(long targetUid, Hashtable resources, System.Action<bool, object> callback)
    {
      if (this.isMarchSlotFull)
        this.ShowMaxMarchSlotMessage();
      else
        this.tradePort.SendRequest(Utils.Hash((object) "help_uid", (object) targetUid, (object) "resource", (object) resources), (System.Action<bool, object>) ((result, orgData) =>
        {
          Hashtable data1;
          DatabaseTools.CheckAndParseOrgData(orgData, out data1);
          Debug.Log((object) Utils.Object2Json(orgData));
          if (data1.ContainsKey((object) "job_id"))
          {
            long result1 = 0;
            if (long.TryParse(data1[(object) "job_id"].ToString(), out result1))
            {
              JobHandle job = JobManager.Instance.GetJob(result1);
              Debug.Log((object) ("Job" + Utils.Object2Json(job.Data)));
              if (job != null)
              {
                Hashtable data2 = job.Data as Hashtable;
                if (data2 != null && data2.ContainsKey((object) "class"))
                {
                  string res = string.Empty;
                  string key = data2[(object) "class"].ToString();
                  if (key != null)
                  {
                    // ISSUE: reference to a compiler-generated field
                    if (MarchSystem.\u003C\u003Ef__switch\u0024map82 == null)
                    {
                      // ISSUE: reference to a compiler-generated field
                      MarchSystem.\u003C\u003Ef__switch\u0024map82 = new Dictionary<string, int>(4)
                      {
                        {
                          "food",
                          0
                        },
                        {
                          "wood",
                          1
                        },
                        {
                          "ore",
                          2
                        },
                        {
                          "silver",
                          3
                        }
                      };
                    }
                    int num;
                    // ISSUE: reference to a compiler-generated field
                    if (MarchSystem.\u003C\u003Ef__switch\u0024map82.TryGetValue(key, out num))
                    {
                      switch (num)
                      {
                        case 0:
                          res = "id_food";
                          break;
                        case 1:
                          res = "id_wood";
                          break;
                        case 2:
                          res = "id_ore";
                          break;
                        case 3:
                          res = "id_silver";
                          break;
                      }
                    }
                  }
                  ChatMessageManager.SendTradeMessage(res);
                }
              }
            }
          }
          if (callback == null)
            return;
          callback(result, orgData);
        }), true);
    }

    public void StartTradeWareHouse(Coordinate target, Hashtable resources, System.Action<bool, object> callback)
    {
      if (this.isMarchSlotFull)
        this.ShowMaxMarchSlotMessage();
      else
        this.startTradeFetchWareHouse.SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "resource", (object) resources, (object) "k", (object) target.K, (object) "x", (object) target.X, (object) "y", (object) target.Y, (object) "march_type", (object) "trade_warehouse"), callback, true);
    }

    public void StartFetchWareHouse(Coordinate target, Hashtable resources, System.Action<bool, object> callback)
    {
      if (this.isMarchSlotFull)
        this.ShowMaxMarchSlotMessage();
      else
        this.startTradeFetchWareHouse.SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "resource", (object) resources, (object) "k", (object) target.K, (object) "x", (object) target.X, (object) "y", (object) target.Y, (object) "march_type", (object) "fetch_warehouse"), callback, true);
    }

    public void StartReinforce(Coordinate target, Hashtable troops, bool withDragon, System.Action<bool, object> callback)
    {
      if (this.isMarchSlotFull)
      {
        this.ShowMaxMarchSlotMessage();
      }
      else
      {
        MarchSystem.LocalMarchCheckType type = this.CanReinforce(target);
        if (type == MarchSystem.LocalMarchCheckType.successed)
          this.reinforcePort.SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "city_id", (object) PlayerData.inst.cityId, (object) "k", (object) target.K, (object) "x", (object) target.X, (object) "y", (object) target.Y, (object) "with_dragon", (object) (!withDragon ? 0 : 1), (object) nameof (troops), (object) troops), callback, true);
        else
          this.ShopWaringMessage(type);
      }
    }

    public void StartJoinFortress(Coordinate target, Hashtable troops, bool withDragon, System.Action<bool, object> callback)
    {
      if (this.isMarchSlotFull)
      {
        this.ShowMaxMarchSlotMessage();
      }
      else
      {
        MarchSystem.LocalMarchCheckType type = this.CanReinforce(target);
        switch (type)
        {
          case MarchSystem.LocalMarchCheckType.successed:
          case MarchSystem.LocalMarchCheckType.already_in_reinforce:
            this.reinforceFortressPort.SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "city_id", (object) PlayerData.inst.cityId, (object) "k", (object) target.K, (object) "x", (object) target.X, (object) "y", (object) target.Y, (object) "with_dragon", (object) (!withDragon ? 0 : 1), (object) nameof (troops), (object) troops), callback, true);
            break;
          default:
            this.ShopWaringMessage(type);
            break;
        }
      }
    }

    public void StartJoinWonder(Coordinate target, Hashtable troops, bool withDragon, System.Action<bool, object> callback)
    {
      if (this.isMarchSlotFull)
      {
        this.ShowMaxMarchSlotMessage();
      }
      else
      {
        MarchSystem.LocalMarchCheckType type = this.CanReinforce(target);
        switch (type)
        {
          case MarchSystem.LocalMarchCheckType.successed:
          case MarchSystem.LocalMarchCheckType.already_in_reinforce:
            this.reinforceWonderPort.SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "city_id", (object) PlayerData.inst.cityId, (object) "k", (object) target.K, (object) "x", (object) target.X, (object) "y", (object) target.Y, (object) "with_dragon", (object) (!withDragon ? 0 : 1), (object) nameof (troops), (object) troops), callback, true);
            break;
          default:
            this.ShopWaringMessage(type);
            break;
        }
      }
    }

    public void StartJoinTemple(Coordinate target, Hashtable troops, bool withDragon, System.Action<bool, object> callback)
    {
      if (this.isMarchSlotFull)
      {
        this.ShowMaxMarchSlotMessage();
      }
      else
      {
        MarchSystem.LocalMarchCheckType type = this.CanReinforce(target);
        switch (type)
        {
          case MarchSystem.LocalMarchCheckType.successed:
          case MarchSystem.LocalMarchCheckType.already_in_reinforce:
            this.reinforceTemplePort.SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "city_id", (object) PlayerData.inst.cityId, (object) "k", (object) target.K, (object) "x", (object) target.X, (object) "y", (object) target.Y, (object) "with_dragon", (object) (!withDragon ? 0 : 1), (object) nameof (troops), (object) troops), callback, true);
            break;
          default:
            this.ShopWaringMessage(type);
            break;
        }
      }
    }

    public void StartRally(Coordinate target, Hashtable troops, int waitTime, bool withDragon, System.Action<bool, object> callback = null)
    {
      MarchSystem.LocalMarchCheckType type = this.CanRally(target);
      if (type == MarchSystem.LocalMarchCheckType.successed)
        this.startRallyPort.SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "alliance_id", (object) PlayerData.inst.allianceId, (object) "city_id", (object) PlayerData.inst.cityId, (object) "k", (object) target.K, (object) "x", (object) target.X, (object) "y", (object) target.Y, (object) "rally_time", (object) waitTime, (object) "with_dragon", (object) (!withDragon ? 0 : 1), (object) nameof (troops), (object) troops), (System.Action<bool, object>) ((result, orgData) =>
        {
          Hashtable data1;
          DatabaseTools.CheckAndParseOrgData(orgData, out data1);
          if (data1.ContainsKey((object) "rally"))
          {
            Hashtable data2;
            DatabaseTools.CheckAndParseOrgData(data1[(object) "rally"], out data2);
            if (data2 != null)
            {
              long result1 = -1;
              if (long.TryParse(data2[(object) "rally_id"].ToString(), out result1))
              {
                RallyData rallyData = DBManager.inst.DB_Rally.Get(result1);
                if (rallyData != null)
                  ChatMessageManager.SendStartRallyMessage(rallyData.rallyId, rallyData.location);
              }
            }
          }
          if (callback == null)
            return;
          callback(result, orgData);
        }), true);
      else
        this.ShopWaringMessage(type);
    }

    public void StartGvERally(Coordinate target, int bossIndex, Hashtable troops, int waitTime, bool withDragon, System.Action<bool, object> callback)
    {
      MarchSystem.LocalMarchCheckType type = this.CanRally(target);
      if (type == MarchSystem.LocalMarchCheckType.successed)
      {
        Hashtable postData = new Hashtable();
        postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
        postData[(object) "x"] = (object) target.X;
        postData[(object) "y"] = (object) target.Y;
        postData[(object) "boss_index"] = (object) bossIndex;
        postData[(object) nameof (troops)] = (object) troops;
        postData[(object) "with_dragon"] = (object) (!withDragon ? 0 : 1);
        postData[(object) "rally_time"] = (object) waitTime;
        this.startGveRallyPort.SendRequest(postData, (System.Action<bool, object>) ((result, orgData) =>
        {
          Hashtable data1;
          DatabaseTools.CheckAndParseOrgData(orgData, out data1);
          if (data1.ContainsKey((object) "rally"))
          {
            Hashtable data2;
            DatabaseTools.CheckAndParseOrgData(data1[(object) "rally"], out data2);
            if (data2 != null)
            {
              long result1 = -1;
              if (long.TryParse(data2[(object) "rally_id"].ToString(), out result1))
              {
                RallyData rallyData = DBManager.inst.DB_Rally.Get(result1);
                if (rallyData != null)
                {
                  ConfigGveBossData data3 = ConfigManager.inst.DB_GveBoss.GetData(rallyData.bossId);
                  if (data3 != null)
                    ChatMessageManager.SendStartGveRallyMessage(rallyData.rallyId, data3.boss_level.ToString(), rallyData.location);
                  WorldBossData worldBossData = DBManager.inst.DB_WorldBossDB.Get(rallyData.bossId);
                  if (worldBossData != null)
                  {
                    WorldBossStaticInfo data4 = ConfigManager.inst.DB_WorldBoss.GetData(worldBossData.configId);
                    ChatMessageManager.SendStartGveRallyMessage(rallyData.rallyId, data4.Name, rallyData.location);
                  }
                }
              }
            }
          }
          if (callback == null)
            return;
          callback(result, orgData);
        }), true);
      }
      else
        this.ShopWaringMessage(type);
    }

    public void StartRabRally(Coordinate target, Hashtable troops, int waitTime, bool withDragon, System.Action<bool, object> callback)
    {
      MarchSystem.LocalMarchCheckType type = this.CanRally(target);
      if (type == MarchSystem.LocalMarchCheckType.successed)
      {
        Hashtable postData = new Hashtable();
        postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
        postData[(object) "x"] = (object) target.X;
        postData[(object) "y"] = (object) target.Y;
        postData[(object) nameof (troops)] = (object) troops;
        postData[(object) "with_dragon"] = (object) (!withDragon ? 0 : 1);
        postData[(object) "rally_time"] = (object) waitTime;
        this.startRabRallyPort.SendRequest(postData, (System.Action<bool, object>) ((result, orgData) =>
        {
          Hashtable data1;
          DatabaseTools.CheckAndParseOrgData(orgData, out data1);
          if (data1.ContainsKey((object) "rally"))
          {
            Hashtable data2;
            DatabaseTools.CheckAndParseOrgData(data1[(object) "rally"], out data2);
            if (data2 != null)
            {
              long result1 = -1;
              if (long.TryParse(data2[(object) "rally_id"].ToString(), out result1))
              {
                RallyData rallyData = DBManager.inst.DB_Rally.Get(result1);
                if (rallyData != null)
                  ChatMessageManager.SendStartRallyMessage(rallyData.rallyId, rallyData.location);
              }
            }
          }
          if (callback == null)
            return;
          callback(result, orgData);
        }), true);
      }
      else
        this.ShopWaringMessage(type);
    }

    public void JoinRally(long rallyId, Hashtable troops, bool withDragon, System.Action<bool, object> callback = null)
    {
      this.joinRallyPort.SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "alliance_id", (object) PlayerData.inst.allianceId, (object) "city_id", (object) PlayerData.inst.cityId, (object) "rally_id", (object) rallyId, (object) "with_dragon", (object) (!withDragon ? 0 : 1), (object) nameof (troops), (object) troops), callback, true);
    }

    private void SendMarchCall(Coordinate target, Hashtable troops, string type, bool withDragon, System.Action<bool, object> callback = null)
    {
      if (this.isMarchSlotFull)
        this.ShowMaxMarchSlotMessage();
      else if (target.K != PlayerData.inst.playerCityData.cityLocation.K)
        this.ShopWaringMessage(MarchSystem.LocalMarchCheckType.not_in_same_kingdom);
      else
        this.marchPort.SendRequest(new Hashtable()
        {
          {
            (object) "city_id",
            (object) GameEngine.Instance.PlayerData.cityId
          },
          {
            (object) "job_id",
            (object) ClientIdCreater.CreateLongId(ClientIdCreater.IdType.EVENT).ToString()
          },
          {
            (object) nameof (type),
            (object) type
          },
          {
            (object) "k",
            (object) target.K
          },
          {
            (object) "x",
            (object) target.X
          },
          {
            (object) "y",
            (object) target.Y
          },
          {
            (object) "with_dragon",
            (object) (!withDragon ? 0 : 1)
          },
          {
            (object) nameof (troops),
            (object) troops
          }
        }, callback, true);
    }

    private void RallyAttackCompleteHandler(List<long> ids)
    {
      List<long>.Enumerator enumerator = ids.GetEnumerator();
      while (enumerator.MoveNext())
      {
        long current = enumerator.Current;
        if (this.marches.ContainsKey(current))
          this.marches[current].RallyAttackDone();
      }
    }

    public void DisposeMarch(long marchId)
    {
      if (this._disposeMarchIdList.Contains(marchId))
        return;
      this._disposeMarchIdList.Add(marchId);
      this._marchs.Remove(marchId);
    }

    public void OnMarchCommandPushRecived(object orgSrc)
    {
      if (orgSrc == null)
        return;
      Hashtable hashtable = orgSrc as Hashtable;
      if (hashtable == null || !hashtable.Contains((object) "skill") || (!hashtable.Contains((object) "march_id") || !this.IsValidGameMode))
        ;
    }

    private bool IsValidGameMode
    {
      get
      {
        if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
          return GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PitMode;
        return true;
      }
    }

    public void Dispose()
    {
      this._marchs.Clear();
      this._disposeMarchIdList.Clear();
      MissileSystemManager.Instance.UnloadAll();
    }

    public void OnMarchDataChanged(long marchId)
    {
      if (this._marchs.ContainsKey(marchId))
        this._marchs[marchId].ReloadControler();
      MarchData marchData = DBManager.inst.DB_March.Get(marchId);
      if (marchData == null || marchData.ownerUid != PlayerData.inst.uid)
        ;
    }

    protected int MaxWorldBossAttackCount
    {
      get
      {
        GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("world_boss_visible_march_max");
        if (data != null)
          return data.ValueInt;
        return 20;
      }
    }

    public bool NeedCreate(MarchData marchData)
    {
      if (marchData.ownerUid == PlayerData.inst.uid || marchData.type != MarchData.MarchType.world_boss_attack)
        return true;
      int num = 0;
      using (Dictionary<long, March.March>.Enumerator enumerator = this._marchs.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.Value.marchData.type == MarchData.MarchType.world_boss_attack)
            ++num;
        }
      }
      if (num < this.MaxWorldBossAttackCount)
        return true;
      Logger.Log("march count limit, ignore march view");
      return false;
    }

    public void OnMarchDataCreate(long marchId)
    {
      March.March march = new March.March(marchId);
      MarchData marchData = DBManager.inst.DB_March.Get(marchId);
      if (marchData == null || !this.NeedCreate(marchData))
        return;
      if (marchData.rallyId > 0L && (marchData.type == MarchData.MarchType.rally_attack || marchData.type == MarchData.MarchType.gve_rally_attack || (marchData.type == MarchData.MarchType.rab_rally_attack || marchData.type == MarchData.MarchType.fortress_attack)))
      {
        if (!this._raylly2marchAttack.ContainsKey(marchData.rallyId))
          this._raylly2marchAttack.Add(marchData.rallyId, march);
        if (!this._rally2March.ContainsKey(marchData.rallyId))
          this._rally2March.Add(marchData.rallyId, marchData.marchId);
      }
      if (this._marchs.ContainsKey(marchId))
        this._marchs[marchId] = march;
      else
        this._marchs.Add(marchId, march);
    }

    public void OnMarchDataRemoved(long marchId)
    {
      if (!this._marchs.ContainsKey(marchId) || !this.IsValidGameMode)
        return;
      this._marchs[marchId].Dispose();
      this._marchs.Remove(marchId);
    }

    protected void ConfirmMarchRecall(long marchId)
    {
      UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
      {
        title = Utils.XLAT("march_uppercase_call_back"),
        content = Utils.XLAT("march_encamp_call_back_description"),
        yes = Utils.XLAT("id_uppercase_confirm"),
        no = Utils.XLAT("id_uppercase_cancel"),
        yesCallback = (System.Action) (() => this.recallPort.SendRequest(Utils.Hash((object) "march_id", (object) marchId), (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          this.RecallCallBack(marchId);
        }), true))
      });
    }

    protected bool IsOwner(long startUserId, long endUserId)
    {
      if (startUserId != PlayerData.inst.uid)
        return endUserId == PlayerData.inst.uid;
      return true;
    }

    public void RecallRally(long rallyId, System.Action useCallBack)
    {
      string str = "item_rally_recall";
      UIManager.inst.OpenPopup("UseItemConfirmPopUp", (Popup.PopupParameter) new UseItemConfirmPopUp.Parameter()
      {
        itemID = str,
        param = Utils.Hash((object) "rally_id", (object) rallyId),
        onUseCallBack = useCallBack
      });
    }

    public void Recall(long marchId)
    {
      MarchData marchData = DBManager.inst.DB_March.Get(marchId);
      if (marchData == null)
        return;
      if (marchData.rallyId > 0L && (marchData.type == MarchData.MarchType.rally_attack || marchData.type == MarchData.MarchType.gve_rally_attack || marchData.type == MarchData.MarchType.rab_rally_attack))
        this.RecallRally(marchData.rallyId, (System.Action) null);
      else if ((marchData.state & (MarchData.MarchState.reinforcing | MarchData.MarchState.encamping | MarchData.MarchState.gathering | MarchData.MarchState.defending_fortress | MarchData.MarchState.demolishing_fortress | MarchData.MarchState.holding_wonder | MarchData.MarchState.building | MarchData.MarchState.dig)) > MarchData.MarchState.invalid || marchData.state == MarchData.MarchState.reinforcing)
        this.ConfirmMarchRecall(marchId);
      else
        UIManager.inst.OpenDlg("MarchRecallDlg", (UI.Dialog.DialogParameter) new MarchCallbackDlg.Parameter()
        {
          marchId = marchData.marchId,
          recallHandler = new System.Action<long>(this.RecallCallBack)
        }, 1 != 0, 1 != 0, 1 != 0);
    }

    public void RecallCallBack(long marchId)
    {
      if (!this._marchs.ContainsKey(marchId))
        return;
      this._marchs[marchId].Recall();
    }

    public void CancelRally(long rallyId)
    {
      MessageHub.inst.GetPortByAction("Rally:cancelRally").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "alliance_id", (object) PlayerData.inst.allianceId, (object) "rally_id", (object) rallyId), new System.Action<bool, object>(this.OnCancelRallyCallback), true);
    }

    public void OnCancelRallyCallback(bool result, object orgData)
    {
      if (result)
      {
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      }
      else
      {
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
        Hashtable data;
        if (!DatabaseTools.CheckAndParseOrgData(orgData, out data) || string.IsNullOrEmpty(data[(object) "errmsg"].ToString()))
          return;
        this.ShopWaringMessage(data[(object) "errmsg"].ToString());
      }
    }

    public void LoadScene()
    {
      Dictionary<long, March.March>.ValueCollection.Enumerator enumerator = this._marchs.Values.GetEnumerator();
      while (enumerator.MoveNext())
        enumerator.Current.ReloadControler();
      this.isReady = true;
    }

    public bool IsReady
    {
      get
      {
        return this.isReady;
      }
    }

    public void ExitScene()
    {
      Dictionary<long, March.March>.ValueCollection.Enumerator enumerator = this._marchs.Values.GetEnumerator();
      while (enumerator.MoveNext())
        enumerator.Current.DeleteView();
      this.isReady = false;
      MissileSystemManager.Instance.ClearAllMissiles();
      PVPSystem.Instance.troopViewManager.Clear();
      this._rally2March.Clear();
    }

    public bool GetRallyAttackMarch(long rallyId)
    {
      return this._rally2March.ContainsKey(rallyId);
    }

    public void SendTroopHome(long marchUid)
    {
      UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
      {
        title = "Warning",
        description = "Are you sure you want send this troop back?",
        leftButtonClickEvent = (System.Action) null,
        rightButtonClickEvent = (System.Action) (() => MessageHub.inst.GetPortByAction("City:sendEmbassyTroopsBack").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "city_id", (object) PlayerData.inst.cityId, (object) "back_uid", (object) marchUid), (System.Action<bool, object>) null, true))
      });
    }

    public void SendFortressTroopHome(long fortressId, long backUid, System.Action<bool, object> callback = null)
    {
      this.sendFortressBackPort.SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "alliance_id", (object) PlayerData.inst.allianceId, (object) "fortress_id", (object) fortressId, (object) "back_uid", (object) backUid), callback, true);
    }

    public void SendAllianceBuildingTroopHome(TileType tt, long buildingid, long backUid, System.Action<bool, object> callback = null)
    {
      this.sendAllianceBuildingBackPort.SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "building_type", (object) tt, (object) "building_id", (object) buildingid, (object) "back_uid", (object) backUid), callback, true);
    }

    public void SendWonderTroopsHome(long wonderId, long backUid, System.Action<bool, object> callback = null)
    {
      HubPort wonderTroopsBackPort = this.sendWonderTroopsBackPort;
      Hashtable postData;
      if (backUid > 0L)
        postData = Utils.Hash((object) "wonder_id", (object) wonderId, (object) "back_uid", (object) backUid);
      else
        postData = Utils.Hash((object) "wonder_id", (object) wonderId);
      System.Action<bool, object> callback1 = callback;
      int num = 1;
      wonderTroopsBackPort.SendRequest(postData, callback1, num != 0);
    }

    public string GetCheckTypeMsg(MarchSystem.LocalMarchCheckType type)
    {
      switch (type)
      {
        case MarchSystem.LocalMarchCheckType.not_in_alliance:
          return ScriptLocalization.Get("march_send_not_in_alliance_warning", true);
        case MarchSystem.LocalMarchCheckType.not_reach_level:
        case MarchSystem.LocalMarchCheckType.not_special_buliding_market:
        case MarchSystem.LocalMarchCheckType.count_find_the_city:
        case MarchSystem.LocalMarchCheckType.count_find_the_user:
          return type.ToString();
        case MarchSystem.LocalMarchCheckType.not_special_building_hall_of_war:
          return ScriptLocalization.Get("march_send_no_hall_of_war_warning", true);
        case MarchSystem.LocalMarchCheckType.not_special_building_embassy:
          return ScriptLocalization.Get("march_need_embassy_for_reinforcement_warning", true);
        case MarchSystem.LocalMarchCheckType.not_in_alliance_and_not_reach_the_level:
          return ScriptLocalization.Get("march_send_not_in_alliance_warning", true);
        case MarchSystem.LocalMarchCheckType.maxinum_march_slot:
          return ScriptLocalization.Get("march_march_slots_exceeded_warning", true);
        case MarchSystem.LocalMarchCheckType.not_in_same_kingdom:
          return ScriptLocalization.Get("march_different_kingdom_warning", true);
        case MarchSystem.LocalMarchCheckType.already_in_rally:
          return ScriptLocalization.Get("march_already_in_rally_warning", true);
        case MarchSystem.LocalMarchCheckType.already_in_reinforce:
          return ScriptLocalization.Get("toast_march_target_reinforced", true);
        default:
          return type.ToString();
      }
    }

    public MarchSystem.LocalMarchCheckType CanRally(Coordinate targetLocation)
    {
      if (PlayerData.inst.allianceId == 0L)
        return MarchSystem.LocalMarchCheckType.not_in_alliance;
      if (CityManager.inst.GetHighestBuildingLevelFor("war_rally") < 1)
        return MarchSystem.LocalMarchCheckType.not_special_building_hall_of_war;
      List<long> rallys = DBManager.inst.DB_Rally.rallys;
      for (int index = 0; index < rallys.Count; ++index)
      {
        RallyData rallyData = DBManager.inst.DB_Rally.Get(rallys[index]);
        if (rallyData != null && rallyData.ownerUid == PlayerData.inst.uid && rallyData.location.K == targetLocation.K && (rallyData.location.X == targetLocation.X && rallyData.location.Y == targetLocation.Y))
          return MarchSystem.LocalMarchCheckType.already_in_rally;
      }
      return this.isMarchSlotFull ? MarchSystem.LocalMarchCheckType.maxinum_march_slot : MarchSystem.LocalMarchCheckType.successed;
    }

    public MarchSystem.LocalMarchCheckType CanReinforce(Coordinate location)
    {
      if (PlayerData.inst.allianceId == 0L)
        return MarchSystem.LocalMarchCheckType.not_in_alliance;
      if (this.isMarchSlotFull)
        return MarchSystem.LocalMarchCheckType.maxinum_march_slot;
      TileData referenceAt = PVPMapData.MapData.GetReferenceAt(location);
      if (referenceAt != null)
      {
        List<long> targetListByType = DBManager.inst.DB_March.GetTargetListByType(MarchData.MarchType.reinforce);
        for (int index = 0; index < targetListByType.Count; ++index)
        {
          MarchData marchData = DBManager.inst.DB_March.Get(targetListByType[index]);
          if (marchData.ownerUid == PlayerData.inst.uid && marchData.targetUid == referenceAt.OwnerID)
            return MarchSystem.LocalMarchCheckType.already_in_reinforce;
        }
      }
      return MarchSystem.LocalMarchCheckType.successed;
    }

    public MarchSystem.LocalMarchCheckType CanTrade(long cityId)
    {
      if (PlayerData.inst.allianceId == 0L)
        return MarchSystem.LocalMarchCheckType.not_in_alliance;
      if (CityManager.inst.GetHighestBuildingLevelFor("marketplace") < 1)
        return MarchSystem.LocalMarchCheckType.not_special_buliding_market;
      return DBManager.inst.DB_March.marchOwner.Count > PlayerData.inst.playerCityData.marchSlot ? MarchSystem.LocalMarchCheckType.maxinum_march_slot : MarchSystem.LocalMarchCheckType.successed;
    }

    public bool isMarchSlotFull
    {
      get
      {
        return DBManager.inst.DB_March.marchOwner.Count >= PlayerData.inst.playerCityData.marchSlot;
      }
    }

    public enum MissileType
    {
      arrow_system,
      rock_system,
      count,
      none,
    }

    public enum LocalMarchCheckType
    {
      successed,
      not_in_alliance,
      not_reach_level,
      not_special_building_hall_of_war,
      not_special_building_embassy,
      not_special_buliding_market,
      not_in_alliance_and_not_reach_the_level,
      maxinum_march_slot,
      not_in_same_kingdom,
      already_in_rally,
      already_in_reinforce,
      count_find_the_city,
      count_find_the_user,
    }
  }
}
