﻿// Decompiled with JetBrains decompiler
// Type: March.March
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

namespace March
{
  public class March
  {
    public List<long> rallyJoinMarchId = new List<long>();
    private MarchData _marchData;
    public MarchViewControler troopsView;

    public March(long marchId)
    {
      this._marchData = DBManager.inst.DB_March.Get(marchId);
      if (this._marchData != null)
        this.InitRallyMarchId(this._marchData.rallyId);
      this.CreateView();
    }

    public MarchData marchData
    {
      get
      {
        return this._marchData;
      }
    }

    private void InitRallyMarchId(long rallyId)
    {
      if (rallyId <= 0L)
        return;
      RallyData rallyData = DBManager.inst.DB_Rally.Get(rallyId);
      if (rallyData == null)
        return;
      Dictionary<long, RallySlotInfo>.Enumerator enumerator = rallyData.slotsInfo_joined.GetEnumerator();
      while (enumerator.MoveNext())
      {
        long marchId = enumerator.Current.Value.marchId;
        if (!this.rallyJoinMarchId.Contains(marchId))
          this.rallyJoinMarchId.Add(marchId);
      }
    }

    public void CreateView()
    {
      if (!((Object) this.troopsView == (Object) null) || GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode && GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PitMode || (this._marchData.state == MarchData.MarchState.gathering || this._marchData.state == MarchData.MarchState.encamping || (this._marchData.state == MarchData.MarchState.reinforcing || this._marchData.state == MarchData.MarchState.holding_wonder)) || (this._marchData.state == MarchData.MarchState.building || this._marchData.state == MarchData.MarchState.defending_fortress || (this._marchData.state == MarchData.MarchState.demolishing_fortress || this._marchData.state == MarchData.MarchState.rallying)))
        return;
      this.troopsView = PVPSystem.Instance.troopViewManager.GetTroopView(this._marchData.marchId);
      this.troopsView.marchData = this.marchData;
      this.troopsView.owner = this;
    }

    public void DeleteView()
    {
      if (!((Object) this.troopsView != (Object) null))
        return;
      this.troopsView.ReleaseTroops();
      this.troopsView = (MarchViewControler) null;
    }

    public void UpdateMarchData(MarchData data)
    {
      if (data == null)
        return;
      this.CreateView();
      if (!((Object) this.troopsView != (Object) null))
        return;
      this.troopsView.UpdateData(data);
    }

    public void ReloadControler()
    {
      if (this.marchData == null)
        return;
      this.CreateView();
      if (!((Object) this.troopsView != (Object) null))
        return;
      this.troopsView.ReloadControler();
    }

    public void Recall()
    {
      if (this.marchData == null)
        return;
      this.CreateView();
      if (!((Object) this.troopsView != (Object) null))
        return;
      this.troopsView.ReloadControler();
    }

    public void ClearData()
    {
      if (this._marchData == null)
        return;
      long marchId = this._marchData.marchId;
      this._marchData = (MarchData) null;
      GameEngine.Instance.marchSystem.DisposeMarch(marchId);
    }

    public void RallyAttackDone()
    {
      if (this.marchData == null || !((Object) this.troopsView != (Object) null))
        return;
      this.troopsView.SetRallyAttackDone();
    }

    public void FortressAttackDone()
    {
      if (this.marchData == null || !((Object) this.troopsView != (Object) null))
        return;
      this.troopsView.SetFortressAttackDone();
    }

    public void DisposeView()
    {
      if (!((Object) this.troopsView != (Object) null) || !PVPSystem.Instance.IsAvailable || PVPSystem.Instance.troopViewManager == null)
        return;
      this.troopsView.ReleaseTroops();
      this.troopsView = (MarchViewControler) null;
    }

    public void Dispose()
    {
      if ((Object) this.troopsView != (Object) null && this.troopsView.CanDone)
      {
        this.ClearData();
        this.DisposeView();
      }
      else if ((Object) this.troopsView == (Object) null)
      {
        this.ClearData();
        this.DisposeView();
      }
      else
        this.troopsView.rallyJoinMarchId = this.rallyJoinMarchId;
    }
  }
}
