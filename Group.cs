﻿// Decompiled with JetBrains decompiler
// Type: Group
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class Group
{
  private List<GroupMember> m_GroupMembers = new List<GroupMember>();

  public void Broadcast(GroupMember sender, object data)
  {
    int count = this.m_GroupMembers.Count;
    for (int index = 0; index < count; ++index)
      this.m_GroupMembers[index].Receive(sender, data);
  }

  public bool ContainsMember(GroupMember member)
  {
    return this.m_GroupMembers.Contains(member);
  }

  public void AddMember(GroupMember member)
  {
    if (this.ContainsMember(member))
      return;
    this.m_GroupMembers.Add(member);
  }

  public void RemoveMember(GroupMember member)
  {
    this.m_GroupMembers.Remove(member);
  }

  public void ClearMembers()
  {
    this.m_GroupMembers.Clear();
  }
}
