﻿// Decompiled with JetBrains decompiler
// Type: LegendExpWindow
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class LegendExpWindow : MonoBehaviour
{
  public UITexture m_Portrait;
  public UILabel m_Level;
  public UISlider m_ExpSlider;
  public UILabel m_ExpValue;

  public void SetData(LegendData legendData)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Portrait, ConfigManager.inst.DB_Legend.GetLegendInfo(legendData.LegendID).Icon, (System.Action<bool>) null, true, false, string.Empty);
    int nextXp = 0;
    int legendLevelByXp = ConfigManager.inst.DB_LegendPoint.GetLegendLevelByXP(legendData.Xp, out nextXp);
    this.m_ExpSlider.value = (float) legendData.Xp / (float) nextXp;
    this.m_Level.text = Utils.FormatThousands(legendLevelByXp.ToString());
    this.SetExpText((int) legendData.Xp, nextXp, 0);
  }

  public void SetExpText(int xp, int nextXp, int gain)
  {
    this.m_ExpValue.text = string.Format("{0}[00FF00]+{1}[-]/{2}", (object) Utils.FormatThousands(xp.ToString()), (object) Utils.FormatThousands(gain.ToString()), (object) Utils.FormatThousands(nextXp.ToString()));
  }
}
