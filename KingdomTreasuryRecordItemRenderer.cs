﻿// Decompiled with JetBrains decompiler
// Type: KingdomTreasuryRecordItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KingdomTreasuryRecordItemRenderer : MonoBehaviour
{
  private Dictionary<int, KingdomTreasuryItemRenderer> _itemDict = new Dictionary<int, KingdomTreasuryItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public UILabel recordDesc;
  public UISprite recordFrame;
  public UIGrid grid;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  private string _recordType;
  private int _recordTime;
  private Hashtable _recordData;
  private Vector3 _recordFramePosition;
  private int _recordFrameHeight;

  public void SetData(string recordType, int recordTime, Hashtable recordData)
  {
    this._recordType = recordType;
    this._recordTime = recordTime;
    this._recordData = recordData;
    this._recordFramePosition = this.recordFrame.transform.localPosition;
    this._recordFrameHeight = this.recordFrame.height;
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.UpdateUI();
  }

  private void ShowSubRecordContent()
  {
    this.ClearData();
    Hashtable inData = this._recordData[(object) "resource"] as Hashtable;
    if (inData != null)
    {
      long outData1 = 0;
      long outData2 = 0;
      long outData3 = 0;
      long outData4 = 0;
      long outData5 = 0;
      DatabaseTools.UpdateData(inData, "food", ref outData1);
      DatabaseTools.UpdateData(inData, "wood", ref outData2);
      DatabaseTools.UpdateData(inData, "ore", ref outData3);
      DatabaseTools.UpdateData(inData, "silver", ref outData4);
      DatabaseTools.UpdateData(inData, "steel", ref outData5);
      Dictionary<string, long> dictionary = new Dictionary<string, long>();
      dictionary.Add("food", outData1);
      dictionary.Add("wood", outData2);
      dictionary.Add("ore", outData3);
      dictionary.Add("silver", outData4);
      int num = 0;
      Dictionary<string, long>.KeyCollection.Enumerator enumerator = dictionary.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (dictionary[enumerator.Current] > 0L)
        {
          KingdomTreasuryItemRenderer itemRenderer = this.CreateItemRenderer(enumerator.Current, dictionary[enumerator.Current]);
          this._itemDict.Add(num++, itemRenderer);
        }
      }
    }
    this.Reposition();
  }

  private void UpdateUI()
  {
    if (this._recordData == null)
      return;
    string empty1 = string.Empty;
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("0", Utils.FormatTimeYYYYMMDD((long) this._recordTime, "-"));
    string Term;
    if (this._recordType == "assign_log")
    {
      Term = "throne_treasury_distribution_history_description";
      string empty2 = string.Empty;
      string empty3 = string.Empty;
      DatabaseTools.UpdateData(this._recordData, "king", ref empty2);
      DatabaseTools.UpdateData(this._recordData, "target", ref empty3);
      para.Add("1", empty2);
      para.Add("2", empty3);
    }
    else
    {
      Term = "throne_treasury_tax_history_description";
      string empty2 = string.Empty;
      string empty3 = string.Empty;
      int outData1 = -1;
      int outData2 = -1;
      DatabaseTools.UpdateData(this._recordData, "kingdom_name", ref empty2);
      DatabaseTools.UpdateData(this._recordData, "target_kingdom_name", ref empty3);
      DatabaseTools.UpdateData(this._recordData, "kingdom_id", ref outData1);
      DatabaseTools.UpdateData(this._recordData, "target_kingdom_id", ref outData2);
      para.Add("1", !string.IsNullOrEmpty(empty2) ? empty2 : Utils.XLAT("id_kingdom"));
      para.Add("2", outData1.ToString());
      para.Add("3", !string.IsNullOrEmpty(empty3) ? empty3 : Utils.XLAT("id_kingdom"));
      para.Add("4", outData2.ToString());
    }
    this.ShowSubRecordContent();
    this.recordDesc.text = ScriptLocalization.GetWithPara(Term, para, true);
    float num1 = 0.0f;
    float num2 = 1f;
    if (this._itemDict.Count <= 0)
      num2 = 2f;
    else
      num1 = this._itemDict.Count > 2 ? 1.8f : 2.25f;
    this.recordFrame.height = (int) ((double) NGUIMath.CalculateRelativeWidgetBounds(this.transform).extents.y * (double) num2 + (double) NGUIMath.CalculateRelativeWidgetBounds(this.itemRoot.transform).extents.y * (double) num1);
    this.recordFrame.transform.localPosition = new Vector3(this._recordFramePosition.x, this._recordFramePosition.y - (float) ((this.recordFrame.height - this._recordFrameHeight) / 2), this._recordFramePosition.z);
  }

  private void ClearData()
  {
    using (Dictionary<int, KingdomTreasuryItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.grid.repositionNow = true;
    this.grid.Reposition();
  }

  private KingdomTreasuryItemRenderer CreateItemRenderer(string resType, long resAmount)
  {
    GameObject gameObject = this._itemPool.AddChild(this.grid.gameObject);
    gameObject.SetActive(true);
    KingdomTreasuryItemRenderer component = gameObject.GetComponent<KingdomTreasuryItemRenderer>();
    component.SetData(resType, resAmount);
    return component;
  }
}
