﻿// Decompiled with JetBrains decompiler
// Type: LinkerHub
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class LinkerHub : MonoBehaviour
{
  private Stopwatch sw = new Stopwatch();
  private const string targetPath = "UIManager/UI Root/UI2DCamera/PublicHUD(Clone)/PART_CITY/TipsBtn";
  public const string SLPDIALOG = "LPDialog";
  public const string SLPUPGRADE = "LPUpgrade";
  public const string SLPBUILD = "LPBuild";
  public const string SLPTROOP = "LPTroop";
  public const string SLPTILE = "LPTile";
  public const string SLPFOCUSPLOT = "LPFocusPlot";
  public const string SLPFOCUSBUILDING = "LPFocusBuilding";
  public const string SLPFOCUSCONSTANTBUILDING = "LPFocusConstantBuilding";
  public const string SLPFOCUSSPECIFICBUILDING = "LPFocusSpecificBuilding";
  public const string SLPFOCUSBUTTON = "LPFocusButton";
  public const string SLPFOCUSBLOCK = "LPFocusBlock";
  public const string SLPFOCUSGENERIC = "LPFocusGeneric";
  public const string BtnQuest = "Quest";
  public const string BtnAlliance = "Alliance";
  public const string BtnKingdom = "Kingdom";
  public const string BtnHero = "hero";
  private const string hintRes = "Prefab/City/Hint";
  private static LinkerHub _instance;
  private GameObject hint;
  private bool mounted;
  private bool needGuide;

  public static LinkerHub Instance
  {
    get
    {
      if ((UnityEngine.Object) LinkerHub._instance == (UnityEngine.Object) null)
      {
        LinkerHub._instance = UnityEngine.Object.FindObjectOfType<LinkerHub>();
        if ((UnityEngine.Object) null == (UnityEngine.Object) LinkerHub._instance)
          throw new ArgumentException("LinkerHub hasn't been created yet.");
      }
      return LinkerHub._instance;
    }
  }

  private void Start()
  {
    LinkerHub._instance = this;
  }

  public void Init()
  {
    UIManager.inst.cityCamera.onDragStart += new CityCamera.OnDragStart(this.cityCamera_onDragStart);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    this.sw.Reset();
    this.sw.Start();
  }

  public void StartTipCount()
  {
    this.sw.Reset();
    this.sw.Start();
  }

  public void Dispose()
  {
    UIManager.inst.cityCamera.onDragStart -= new CityCamera.OnDragStart(this.cityCamera_onDragStart);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    LinkerHub._instance = (LinkerHub) null;
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.hint)
      return;
    this.hint.transform.parent = (Transform) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.hint);
    this.hint = (GameObject) null;
  }

  private void cityCamera_onDragStart()
  {
    UIManager.inst.cityCamera.PopArg();
  }

  private void OnSecond(int obj)
  {
    if (this.sw.ElapsedMilliseconds <= 5000L || this.sw == null)
      return;
    this.sw.Reset();
    this.sw.Start();
    if (TutorialManager.Instance.IsRunning || CityManager.inst.mStronghold.mLevel > 5)
      return;
    GameObject gameObject = GameObject.Find("UIManager/UI Root/UI2DCamera/PublicHUD(Clone)/PART_CITY/TipsBtn");
    if (!((UnityEngine.Object) null != (UnityEngine.Object) gameObject))
      return;
    this.MountHint(gameObject.transform, 0, (object) null);
  }

  public void Distribute(string json)
  {
    if (json == null)
      return;
    LinkerPara linkerPara = JsonReader.Deserialize<LinkerPara>(json);
    if (linkerPara == null)
      return;
    string classname = linkerPara.classname;
    if (classname == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (LinkerHub.\u003C\u003Ef__switch\u0024map71 == null)
    {
      // ISSUE: reference to a compiler-generated field
      LinkerHub.\u003C\u003Ef__switch\u0024map71 = new Dictionary<string, int>(12)
      {
        {
          "LPDialog",
          0
        },
        {
          "LPBuild",
          1
        },
        {
          "LPTroop",
          2
        },
        {
          "LPTile",
          3
        },
        {
          "LPFocusPlot",
          4
        },
        {
          "LPFocusBuilding",
          5
        },
        {
          "LPFocusConstantBuilding",
          6
        },
        {
          "LPFocusSpecificBuilding",
          7
        },
        {
          "LPFocusButton",
          8
        },
        {
          "LPUpgrade",
          9
        },
        {
          "LPFocusBlock",
          10
        },
        {
          "LPFocusGeneric",
          11
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!LinkerHub.\u003C\u003Ef__switch\u0024map71.TryGetValue(classname, out num))
      return;
    switch (num)
    {
      case 0:
        this.LHGotoDialog(linkerPara.content);
        break;
      case 1:
        this.LHGotoBuild(linkerPara.content);
        break;
      case 2:
        this.LHGotoTroop(linkerPara.content);
        break;
      case 3:
        this.LHGotoTile(linkerPara.content);
        break;
      case 4:
        this.LHFocusPlot(linkerPara.content);
        break;
      case 5:
        this.LHFocusBuilding(linkerPara.content);
        break;
      case 6:
        this.LHFocusConstantBuilding(linkerPara.content);
        break;
      case 7:
        this.LHFocusSpecificBuilding(linkerPara.content);
        break;
      case 8:
        this.LHFocusButton(linkerPara.content);
        break;
      case 9:
        this.LHGotoUpgrade(linkerPara.content);
        break;
      case 10:
        this.LHGotoBlock(linkerPara.content);
        break;
      case 11:
        this.LHFocusGeneric(linkerPara.content);
        break;
    }
  }

  private void LHGotoBlock(string content)
  {
    LPFocusBlock lpuobj = JsonReader.Deserialize<LPFocusBlock>(content);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    this.StartCoroutine(this.LHGotoBlock(lpuobj));
  }

  [DebuggerHidden]
  public IEnumerator LHGotoBlock(LPFocusBlock lpuobj)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LinkerHub.\u003CLHGotoBlock\u003Ec__Iterator62()
    {
      lpuobj = lpuobj,
      \u003C\u0024\u003Elpuobj = lpuobj,
      \u003C\u003Ef__this = this
    };
  }

  public void LHGotoDialog(LPDialog lpdobj)
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    System.Type type = Utils.GetType(lpdobj.dParaClassName);
    if (type == null)
    {
      this.StartCoroutine(this.OpenTarDialog(lpdobj.dType, (UI.Dialog.DialogParameter) null));
    }
    else
    {
      object paraObj = JsonReader.Deserialize(lpdobj.dPara, type);
      if (!this.CheckBuildingExist(lpdobj.dType, paraObj))
        return;
      this.StartCoroutine(this.OpenTarDialog(lpdobj.dType, paraObj as UI.Dialog.DialogParameter));
    }
  }

  private bool CheckBuildingExist(string p, object paraObj)
  {
    bool flag = true;
    string key = p;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (LinkerHub.\u003C\u003Ef__switch\u0024map72 == null)
      {
        // ISSUE: reference to a compiler-generated field
        LinkerHub.\u003C\u003Ef__switch\u0024map72 = new Dictionary<string, int>(3)
        {
          {
            "Research/ResearchTreeDlg",
            0
          },
          {
            "Research/ResearchDetailDlg",
            0
          },
          {
            "Barracks/TrainTroopDlg",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (LinkerHub.\u003C\u003Ef__switch\u0024map72.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            if ((UnityEngine.Object) null != (UnityEngine.Object) CitadelSystem.inst.GetUniversity())
            {
              flag = true;
              break;
            }
            this.LHGotoBuild(new LPBuild()
            {
              zone = 2,
              buildingType = "university"
            });
            flag = false;
            break;
          case 1:
            string unitId = (paraObj as TrainTroopDlg.Parameter).UnitID;
            if (unitId == null)
              return true;
            if ((UnityEngine.Object) null != (UnityEngine.Object) CitadelSystem.inst.GetTrainBuildingbyUnitID(unitId))
            {
              flag = true;
              break;
            }
            string buildingTypebyUnitId = CitadelSystem.inst.GetBuildingTypebyUnitID(unitId);
            this.LHGotoBuild(new LPBuild()
            {
              zone = 2,
              buildingType = buildingTypebyUnitId
            });
            flag = false;
            break;
        }
      }
    }
    return flag;
  }

  public void LHGotoBuild(LPBuild lpbobj)
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    this.StartCoroutine(this.FocusPlot(new LPFocusPlot()
    {
      Zone = lpbobj.zone
    }));
  }

  public void LHGotoTroop(LPTroop lptobj)
  {
    if (MapUtils.IsPitWorld(DBManager.inst.DB_March.Get(lptobj.marchId).worldId))
    {
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PitMode)
        UIManager.inst.publicHUD.OnKingdomMapBtnPressed();
    }
    else if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
      UIManager.inst.publicHUD.OnKingdomMapBtnPressed();
    UIManager.inst.OpenKingdomArmyCircle(lptobj.marchId, (Transform) null);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void LHGotoTile(LPTile lptobj)
  {
    if (!MapUtils.CanGotoTarget(lptobj.k))
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    }
    else
    {
      if (MapUtils.IsPitWorld(lptobj.k))
      {
        if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PitMode)
          UIManager.inst.publicHUD.OnKingdomMapBtnPressed();
      }
      else if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
        UIManager.inst.publicHUD.OnKingdomMapBtnPressed();
      PVPMap.PendingGotoRequest = new Coordinate(lptobj.k, lptobj.x, lptobj.y);
      PVPMap.ShowCircle = lptobj.circle;
      PVPMap.CircleLocation = PVPMap.PendingGotoRequest;
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }

  private void LHGotoDialog(string lpdjson)
  {
    this.LHGotoDialog(JsonReader.Deserialize<LPDialog>(lpdjson));
  }

  private void LHGotoBuild(string lpbjson)
  {
    this.LHGotoBuild(JsonReader.Deserialize<LPBuild>(lpbjson));
  }

  private void LHGotoTroop(string lptjson)
  {
    this.LHGotoTroop(JsonReader.Deserialize<LPTroop>(lptjson));
  }

  private void LHGotoTile(string lptjson)
  {
    this.LHGotoTile(JsonReader.Deserialize<LPTile>(lptjson));
  }

  private void LHFocusBuilding(string p)
  {
    this.LHFocusBuilding(JsonReader.Deserialize<LPFocusBuilding>(p));
  }

  public void LHFocusBuilding(LPFocusBuilding lpfbobj)
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    this.StartCoroutine(this.FocusBuilding(lpfbobj));
  }

  private void LHFocusConstantBuilding(string p)
  {
    this.LHFocusConstantBuilding(JsonReader.Deserialize<LPFocusConstantBuilding>(p));
  }

  public void LHFocusConstantBuilding(LPFocusConstantBuilding lpfbobj)
  {
    this.StartCoroutine(this.FocusConstantBuilding(lpfbobj));
  }

  private void LHFocusSpecificBuilding(string p)
  {
    this.LHFocusSpecificBuilding(JsonReader.Deserialize<LPFocusSpecificBuilding>(p));
  }

  public void LHFocusSpecificBuilding(LPFocusSpecificBuilding lpfbobj)
  {
    this.StartCoroutine(this.FocusSpecificBuilding(lpfbobj));
  }

  private void LHFocusPlot(string p)
  {
    this.LHFocusPlot(JsonReader.Deserialize<LPFocusPlot>(p));
  }

  public void LHFocusPlot(LPFocusPlot lpfpobj)
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    this.StartCoroutine(this.FocusPlot(lpfpobj));
  }

  private void LHFocusButton(string p)
  {
    this.LHFocusButton(JsonReader.Deserialize<LPFocusButton>(p));
  }

  public void LHFocusButton(LPFocusButton lpfbobj)
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    string buttonName = lpfbobj.buttonName;
    if (buttonName == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (LinkerHub.\u003C\u003Ef__switch\u0024map73 == null)
    {
      // ISSUE: reference to a compiler-generated field
      LinkerHub.\u003C\u003Ef__switch\u0024map73 = new Dictionary<string, int>(4)
      {
        {
          "Quest",
          0
        },
        {
          "Alliance",
          1
        },
        {
          "Kingdom",
          2
        },
        {
          "hero",
          3
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!LinkerHub.\u003C\u003Ef__switch\u0024map73.TryGetValue(buttonName, out num))
      return;
    switch (num)
    {
      case 0:
        this.MountHint(UIManager.inst.publicHUD.mQuestBtn.transform, 0, (object) null);
        break;
      case 1:
        this.MountHint(UIManager.inst.publicHUD.mAllianceBtn.transform, 0, (object) null);
        break;
      case 2:
        this.MountHint(UIManager.inst.publicHUD.mKingdomBtn.transform, 0, (object) null);
        if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
          break;
        PVPSystem.Instance.Map.GotoLocation(PlayerData.inst.CityData.Location, false);
        break;
      case 3:
        this.MountHint(UIManager.inst.publicHUD.heroMiniDlg.gameObject.transform, 2, (object) null);
        break;
    }
  }

  private void LHGotoUpgrade(string lpujson)
  {
    this.LHGotoUpgrade(JsonReader.Deserialize<LPUpgrade>(lpujson));
  }

  public void LHGotoUpgrade(LPUpgrade lpuobj)
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    this.StartCoroutine(this.FocusTarLvlBuilding(lpuobj));
  }

  private void LHFocusGeneric(string p)
  {
    this.LHFocusGeneric(JsonReader.Deserialize<LPFocusGeneric>(p));
  }

  public void LHFocusGeneric(LPFocusGeneric lpfbobj)
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    this.MountHint(GameObject.Find(lpfbobj.targetName).transform, 0, (object) null);
  }

  [DebuggerHidden]
  private IEnumerator OpenUpgradeDialog(LPUpgrade lpuobj)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LinkerHub.\u003COpenUpgradeDialog\u003Ec__Iterator63()
    {
      lpuobj = lpuobj,
      \u003C\u0024\u003Elpuobj = lpuobj,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator OpenBuildDialog(LPBuild lpbobj)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LinkerHub.\u003COpenBuildDialog\u003Ec__Iterator64()
    {
      lpbobj = lpbobj,
      \u003C\u0024\u003Elpbobj = lpbobj,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator FocusPlot(LPFocusPlot lpfpobj)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LinkerHub.\u003CFocusPlot\u003Ec__Iterator65()
    {
      lpfpobj = lpfpobj,
      \u003C\u0024\u003Elpfpobj = lpfpobj,
      \u003C\u003Ef__this = this
    };
  }

  private void ChangeToCityScene()
  {
    if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.CityMode)
      return;
    UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
    {
      UIManager.inst.tileCamera.gameObject.SetActive(false);
      GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.CityMode;
    }));
  }

  [DebuggerHidden]
  private IEnumerator FocusBuilding(LPFocusBuilding lpfbobj)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LinkerHub.\u003CFocusBuilding\u003Ec__Iterator66()
    {
      lpfbobj = lpfbobj,
      \u003C\u0024\u003Elpfbobj = lpfbobj,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator FocusSpecificBuilding(LPFocusSpecificBuilding lpfbobj)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LinkerHub.\u003CFocusSpecificBuilding\u003Ec__Iterator67()
    {
      lpfbobj = lpfbobj,
      \u003C\u0024\u003Elpfbobj = lpfbobj,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator FocusConstantBuilding(LPFocusConstantBuilding lpfbobj)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LinkerHub.\u003CFocusConstantBuilding\u003Ec__Iterator68()
    {
      lpfbobj = lpfbobj,
      \u003C\u0024\u003Elpfbobj = lpfbobj,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator FocusTarLvlBuilding(LPUpgrade lpuobj)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LinkerHub.\u003CFocusTarLvlBuilding\u003Ec__Iterator69()
    {
      lpuobj = lpuobj,
      \u003C\u0024\u003Elpuobj = lpuobj,
      \u003C\u003Ef__this = this
    };
  }

  public bool Mounted
  {
    get
    {
      return this.mounted;
    }
    set
    {
      if (this.mounted == value)
        return;
      this.sw.Reset();
      if (this.mounted)
        this.sw.Start();
      else
        this.sw.Stop();
      this.mounted = value;
    }
  }

  public void MountHint(Transform parent, int mode = 0, object para = null)
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) parent)
      return;
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.hint)
      this.hint = UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/City/Hint", (System.Type) null)) as GameObject;
    this.hint.transform.parent = (Transform) null;
    this.hint.transform.localScale = Vector3.one;
    this.hint.transform.parent = parent;
    this.hint.SetActive(true);
    HintPrefab component = this.hint.GetComponent<HintPrefab>();
    component.Init(mode);
    component.Para = para;
    CityCamera cityCamera = UIManager.inst.cityCamera;
    cityCamera.PushArg(new CityCamera.CameraArgs()
    {
      zoomFactor = cityCamera.zoomSmoothFactor,
      dragFactor = component.focusTime
    });
    this.Mounted = true;
  }

  public void DisposeHint(Transform parent)
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) parent)
      return;
    HintPrefab[] componentsInChildren = parent.GetComponentsInChildren<HintPrefab>(true);
    if (componentsInChildren.Length == 0)
      return;
    this.Mounted = false;
    componentsInChildren[0].Dispose();
  }

  public void DisposeHintAnyway()
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.hint))
      return;
    this.hint.GetComponent<HintPrefab>().Dispose();
  }

  [DebuggerHidden]
  private IEnumerator OpenTarDialog(string dialogType, UI.Dialog.DialogParameter dp = null)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LinkerHub.\u003COpenTarDialog\u003Ec__Iterator6A()
    {
      dialogType = dialogType,
      dp = dp,
      \u003C\u0024\u003EdialogType = dialogType,
      \u003C\u0024\u003Edp = dp
    };
  }

  public bool NeedGuide
  {
    get
    {
      return this.needGuide;
    }
    set
    {
      this.needGuide = value;
    }
  }
}
