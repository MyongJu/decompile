﻿// Decompiled with JetBrains decompiler
// Type: DungeonInventoryDiscardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using UI;
using UnityEngine;

public class DungeonInventoryDiscardPopup : Popup
{
  private int _useCount = 1;
  private bool useItem = true;
  private bool autoFix = true;
  private ProgressBarTweenProxy proxy = new ProgressBarTweenProxy();
  private bool autoFixProgressBar = true;
  public const string POPUP_TYPE = "DragonKnight/DungeonInventoryDiscardPopup";
  public UILabel title;
  public Icon icon;
  public GameObject userCountContent;
  public UIButton leftButtton;
  public UIButton rightButton;
  public UIProgressBar progressBar;
  public UILabel countLabel;
  public UIInput inputCountLabel;
  public UILabel maxCountLabel;
  public UIButton useBt;
  public GameObject useContent;
  public UILabel weight;
  public System.Action<bool, object> onUseOrBuyCallBack;
  private int id;
  private bool useOne;
  private bool buyAndUse;
  private ItemStaticInfo ItemInfo;
  private int MaxCount;

  private int UseCount
  {
    get
    {
      return this._useCount;
    }
    set
    {
      if (this._useCount == value || value <= 0 || value > this.MaxCount)
        return;
      this._useCount = value;
      this.RefreshProgressBar(true);
    }
  }

  private UIProgressBar ProgressBar
  {
    get
    {
      return this.progressBar;
    }
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.InitParam(orgParam as DungeonInventoryDiscardPopup.Parameter);
    this.UpdateUI();
  }

  public void OnAddItemDownHandler()
  {
    this.proxy.Slider = this.progressBar;
    this.autoFixProgressBar = false;
    this.proxy.MaxSize = this.MaxCount;
    this.proxy.Play(1, -1f);
  }

  public void OnAddItemUpHandler()
  {
    this.autoFixProgressBar = true;
    this.proxy.Stop();
  }

  public void OnAddItemClick()
  {
  }

  public void OnRemoveItemClick()
  {
  }

  public void OnRemoveItemDownHandler()
  {
    this.proxy.Slider = this.progressBar;
    this.autoFixProgressBar = false;
    float targetValue = 1f / (float) this.MaxCount;
    this.proxy.MaxSize = this.MaxCount;
    this.proxy.Play(-1, targetValue);
  }

  public void OnRemoveItemUpHandler()
  {
    this.autoFixProgressBar = true;
    this.proxy.Stop();
  }

  private void UpdateUI()
  {
    ItemStaticInfo itemInfo = this.ItemInfo;
    NGUITools.SetActive(this.useContent, true);
    this.icon.SetData(itemInfo.ImagePath, new string[1]
    {
      itemInfo.LocName
    });
    this.maxCountLabel.text = "/" + this.MaxCount.ToString();
    this.title.text = ScriptLocalization.Get("item_uppercase_use_item_title", true);
    this.autoFix = false;
    this.RefreshProgressBar(true);
  }

  private int ShopItemPrice
  {
    get
    {
      return this.UseCount * ItemBag.Instance.GetShopItemPrice(ConfigManager.inst.DB_Shop.GetShopDataByInternalId(this.id).ID);
    }
  }

  private void RefreshProgressBar(bool needToChangeBar = true)
  {
    if (this.useItem)
    {
      if (needToChangeBar)
        this.progressBar.value = Mathf.Clamp01((float) this._useCount / (float) this.MaxCount);
      this.inputCountLabel.value = this._useCount.ToString();
      this.weight.text = Utils.FormatThousands((this._useCount * this.ItemInfo.DungeonWeight).ToString());
    }
    this.autoFix = true;
  }

  public void OnProgressBarChange()
  {
    int num = Mathf.RoundToInt(this.ProgressBar.value * (float) this.MaxCount);
    if (num <= 0)
    {
      num = 1;
      if (this.autoFixProgressBar)
        this.ProgressBar.value = (float) num / (float) this.MaxCount;
    }
    if (num > this.MaxCount)
      num = this.MaxCount;
    if (this.autoFix)
      this._useCount = num;
    this.RefreshProgressBar(false);
  }

  public UIInput TextInput
  {
    get
    {
      return this.inputCountLabel;
    }
  }

  public void OnTextInputCommit()
  {
    int result = 1;
    if (!int.TryParse(this.TextInput.value, out result))
      result = 1;
    if (result <= 0)
      result = 1;
    if (result > this.MaxCount)
      result = this.MaxCount;
    this._useCount = result;
    this.autoFix = false;
    this.RefreshProgressBar(this.autoFixProgressBar);
  }

  public void OnTextInputHandler()
  {
    int result = 1;
    if (!int.TryParse(this.TextInput.value, out result) || result == 0)
      return;
    if (result > this.MaxCount)
      result = this.MaxCount;
    this._useCount = result;
    this.autoFix = false;
    this.RefreshProgressBar(this.autoFixProgressBar);
  }

  public void OnUseItem()
  {
    ItemBag.Instance.UseItem(this.id, this.UseCount, (Hashtable) null, new System.Action<bool, object>(this.UseCallBack));
  }

  public void OnBuyAndUseItem()
  {
    if (this.ShopItemPrice > PlayerData.inst.hostPlayer.Currency)
    {
      this.OnCloseHandler();
      Utils.ShowNotEnoughGoldTip();
    }
    else
      ItemBag.Instance.BuyAndUseShopItem(this.id, this.UseCount, (Hashtable) null, new System.Action<bool, object>(this.UseCallBack));
  }

  private void UseCallBack(bool success, object result)
  {
    if (this.onUseOrBuyCallBack != null)
      this.onUseOrBuyCallBack(success, result);
    this.OnCloseHandler();
  }

  public void OnBuyItem()
  {
    RequestManager.inst.SendRequest("dragonKnight:discardBagItem", Utils.Hash((object) "item", (object) this.ItemInfo.internalId, (object) "count", (object) this._useCount), (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    }), true);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.onUseOrBuyCallBack = (System.Action<bool, object>) null;
  }

  private void InitParam(DungeonInventoryDiscardPopup.Parameter param)
  {
    this.ItemInfo = ConfigManager.inst.DB_Items.GetItem(param.item_id);
    this.MaxCount = param.amount;
    this.onUseOrBuyCallBack = param.onUseOrBuyCallBack;
  }

  public class Parameter : Popup.PopupParameter
  {
    public int item_id;
    public int amount;
    public System.Action<bool, object> onUseOrBuyCallBack;
  }
}
