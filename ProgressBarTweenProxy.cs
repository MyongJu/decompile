﻿// Decompiled with JetBrains decompiler
// Type: ProgressBarTweenProxy
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ProgressBarTweenProxy
{
  private float _targetValue = 1f;
  private List<int> speeds = new List<int>();
  private float factor = 6f;
  private int direction = 1;
  public int BaseValue = 1;
  private float elapseTime;
  private bool isInit;
  public List<EventDelegate> onChange;
  private int speedIndex;
  private bool isRuning;

  public ProgressBarTweenProxy()
  {
    this.speeds.Add(1);
    this.speeds.Add(3);
    this.speeds.Add(5);
    this.speeds.Add(9);
    this.speeds.Add(13);
    this.speeds.Add(19);
    this.speeds.Add(27);
    this.speeds.Add(37);
    this.speeds.Add(50);
    this.speeds.Add(70);
    this.speeds.Add(100);
    this.speeds.Add(140);
    this.speeds.Add(190);
  }

  public UIProgressBar Slider { get; set; }

  public int MinSize { get; set; }

  public int MaxSize { get; set; }

  public double CurrentValue { get; private set; }

  public float GetSpeed()
  {
    return (float) this.direction * ((float) this.speeds[this.SpeedIndex] * (float) this.BaseValue);
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.updateEvent += new System.Action<double>(this.OnUpdateHandler);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
  }

  private void OnSecondHandler(int time)
  {
    ++this.speedIndex;
  }

  private int SpeedIndex
  {
    get
    {
      if (this.speedIndex >= this.speeds.Count)
        this.speedIndex = this.speeds.Count - 1;
      return this.speedIndex;
    }
  }

  private void RemoveEventHandler()
  {
    this.speedIndex = 0;
    this.isInit = false;
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.updateEvent -= new System.Action<double>(this.OnUpdateHandler);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
  }

  private void OnUpdateHandler(double time)
  {
    if (!this.isRuning)
      return;
    this.CurrentValue += (double) this.GetSpeed() / (double) this.MaxSize * (double) Time.deltaTime;
    float currentValue = (float) this.CurrentValue;
    if (this.IsFinish(ref currentValue))
    {
      this.isRuning = false;
      this.RemoveEventHandler();
    }
    this.Slider.value = currentValue;
  }

  private bool IsFinish(ref float result)
  {
    bool flag = false;
    if (this.direction > 0)
    {
      if ((double) result > (double) this._targetValue)
      {
        result = this._targetValue;
        flag = true;
      }
    }
    else if ((double) result < (double) this._targetValue)
    {
      result = this._targetValue;
      flag = true;
    }
    return flag;
  }

  public void Stop()
  {
    this.RemoveEventHandler();
    iTween.Stop(this.Slider.foregroundWidget.gameObject);
  }

  public void Pause()
  {
    this.isRuning = false;
  }

  public void Play(int direction = 1, float targetValue = -1f)
  {
    if ((double) targetValue == (double) this.Slider.value)
      return;
    this.speedIndex = 0;
    this.direction = direction;
    this.isRuning = true;
    this._targetValue = direction <= 0 ? (float) this.MinSize / (float) this.MaxSize : 1f;
    if ((double) targetValue != -1.0)
      this._targetValue = targetValue;
    if (this.isInit)
      return;
    bool flag = false;
    if (this.MaxSize > 0)
    {
      float num = (float) this.BaseValue / (float) this.MaxSize;
      this.CurrentValue = (double) this.Slider.value + (double) direction * (double) num;
      float currentValue = (float) this.CurrentValue;
      flag = this.IsFinish(ref currentValue);
      this.Slider.value = currentValue;
    }
    if (flag)
      return;
    this.AddEventHandler();
    this.isInit = true;
  }
}
