﻿// Decompiled with JetBrains decompiler
// Type: ConfigAnniversaryCommunityeReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigAnniversaryCommunityeReward
{
  private Dictionary<string, AnniversaryCommunityRewardInfo> m_DataByID;
  private Dictionary<int, AnniversaryCommunityRewardInfo> m_DataByInternalID;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<AnniversaryCommunityRewardInfo, string>(result as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
  }

  public AnniversaryCommunityRewardInfo Get(string id)
  {
    AnniversaryCommunityRewardInfo communityRewardInfo;
    this.m_DataByID.TryGetValue(id, out communityRewardInfo);
    return communityRewardInfo;
  }

  public AnniversaryCommunityRewardInfo Get(int internalId)
  {
    AnniversaryCommunityRewardInfo communityRewardInfo;
    this.m_DataByInternalID.TryGetValue(internalId, out communityRewardInfo);
    return communityRewardInfo;
  }

  public List<AnniversaryCommunityRewardInfo> GetAnnCommunityRewardList()
  {
    List<AnniversaryCommunityRewardInfo> communityRewardInfoList = new List<AnniversaryCommunityRewardInfo>();
    if (this.m_DataByID != null && this.m_DataByID.Count > 0)
    {
      Dictionary<string, AnniversaryCommunityRewardInfo>.Enumerator enumerator = this.m_DataByID.GetEnumerator();
      while (enumerator.MoveNext())
        communityRewardInfoList.Add(enumerator.Current.Value);
    }
    communityRewardInfoList.Sort(new Comparison<AnniversaryCommunityRewardInfo>(this.Compare));
    return communityRewardInfoList;
  }

  private int Compare(AnniversaryCommunityRewardInfo x, AnniversaryCommunityRewardInfo y)
  {
    return Math.Sign(x.internalId - y.internalId);
  }

  public void Clear()
  {
    if (this.m_DataByID != null)
    {
      this.m_DataByID.Clear();
      this.m_DataByID = (Dictionary<string, AnniversaryCommunityRewardInfo>) null;
    }
    if (this.m_DataByInternalID == null)
      return;
    this.m_DataByInternalID.Clear();
    this.m_DataByInternalID = (Dictionary<int, AnniversaryCommunityRewardInfo>) null;
  }
}
