﻿// Decompiled with JetBrains decompiler
// Type: FunplusSDKSetting
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class FunplusSDKSetting
{
  public string gameId = "2031";
  public string gameKey = "8632781872155bc53b54729c4bdb368c";
  public string environment = "sandbox";
  public FunplusSDKSetting.AndroidSetting android;
  public FunplusSDKSetting.IOSSetting ios;
  public FunplusSDKSetting.FacebookSetting facebookSettings;

  public class AndroidSetting
  {
    public string launcherActivity = "UnityPlayerNativeActivity";
    public string targetSDKVersion = "23";
    public string buildToolsVersion = "23.0.1";
    public string minSDKVersion = "15";
    public string compileSDKVersion = "23";
  }

  public class IOSSetting
  {
    public string applicationController = "Classes/UnityAppController.mm";
  }

  public class FacebookSetting
  {
    public bool isEnable = true;
    public string appName = "King of Avalon";
    public string appId = "969208706496578";
  }
}
