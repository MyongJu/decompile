﻿// Decompiled with JetBrains decompiler
// Type: EquipmentGemRemovePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using GemConstant;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class EquipmentGemRemovePopup : Popup
{
  public const string SHOP_RECLAIM_GEM = "shopitem_gemstone_reclaim";
  public UITexture m_GemIcon;
  public UILabel m_GemLevel;
  public UILabel m_GemName;
  public UILabel m_GemBenefit;
  public UISlider m_ExpProgress;
  public UILabel m_ExpValue;
  public GameObject m_Reclaim;
  public GameObject m_BuyAndUse;
  public UIGrid grid;
  public UILabel m_Price;
  public UILabel m_Count;
  public UILabel title;
  public HelpBtnComponent bt;
  private EquipmentGemRemovePopup.Parameter m_Parameter;

  private UILabel GetBenefitItem()
  {
    GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.m_GemBenefit.gameObject);
    UILabel component = gameObject.GetComponent<UILabel>();
    gameObject.SetActive(true);
    return component;
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as EquipmentGemRemovePopup.Parameter;
    string source = "forge_armory_gemstone_remove_title";
    if (this.m_Parameter.gemData.SlotType == SlotType.dragon_knight)
    {
      source = "forge_armory_dk_runestone_remove_title";
      this.bt.ID = "help_forge_dk_embed_runestone";
    }
    this.title.text = Utils.XLAT(source);
    this.UpdateUI();
  }

  public override void OnShow(UIControler.UIParameter orgParam = null)
  {
    base.OnShow(orgParam);
    this.grid.Reposition();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnDestroyPressed()
  {
    GemManager.Instance.Destroy(this.m_Parameter.inventoryData, this.m_Parameter.slotIndex, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      if (this.m_Parameter.callback != null)
        this.m_Parameter.callback();
      this.OnClosePressed();
      AudioManager.Instance.StopAndPlaySound("sfx_city_gemstone_destroy");
    }));
  }

  public void OnReclaimPressed()
  {
    GemManager.Instance.Reclaim(this.m_Parameter.inventoryData, this.m_Parameter.slotIndex, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      if (this.m_Parameter.callback != null)
        this.m_Parameter.callback();
      this.OnClosePressed();
      AudioManager.Instance.StopAndPlaySound("sfx_city_gemstone_reclaim");
    }));
  }

  public void OnBuyAndUsePressed()
  {
    if (!this.CanBuy())
    {
      Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
      this.OnClosePressed();
    }
    else
      GemManager.Instance.BuyAndReclaim(this.m_Parameter.inventoryData, this.m_Parameter.slotIndex, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        if (this.m_Parameter.callback != null)
          this.m_Parameter.callback();
        this.OnClosePressed();
        AudioManager.Instance.StopAndPlaySound("sfx_city_gemstone_reclaim");
      }));
  }

  public void OnAddExpPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    UIManager.inst.OpenDlg("Gem/EquipmentGemRefineDlg", (UI.Dialog.DialogParameter) new EquipmentGemRefineDlg.Parameter()
    {
      gemId = this.m_Parameter.gemData.ID,
      slotType = this.m_Parameter.gemData.SlotType
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private void UpdateUI()
  {
    ConfigEquipmentGemInfo data = ConfigManager.inst.DB_EquipmentGem.GetData(this.m_Parameter.gemData.ConfigId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_GemIcon, data.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    this.m_GemLevel.text = Utils.XLAT("id_lv") + " " + data.level.ToString();
    this.m_GemName.text = ConfigManager.inst.DB_Items.GetItem(data.itemID).LocName;
    List<Benefits.BenefitValuePair> benefitsPairs = data.benefits.GetBenefitsPairs();
    for (int index = 0; index < benefitsPairs.Count; ++index)
    {
      Benefits.BenefitValuePair benefitValuePair = benefitsPairs[index];
      PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitValuePair.internalID];
      this.GetBenefitItem().text = dbProperty.Name + " " + dbProperty.ConvertToDisplayString((double) benefitValuePair.value, true, true);
    }
    long exp = this.m_Parameter.gemData.Exp;
    long expReqToNext = data.expReqToNext;
    this.m_ExpProgress.value = (float) exp / (float) expReqToNext;
    this.m_ExpValue.text = expReqToNext <= 0L ? Utils.XLAT("id_uppercase_maxed") : string.Format("{0}/{1}", (object) exp, (object) expReqToNext);
    int itemCountByShopId = ItemBag.Instance.GetItemCountByShopID("shopitem_gemstone_reclaim");
    this.m_Reclaim.SetActive(itemCountByShopId > 0);
    this.m_BuyAndUse.SetActive(itemCountByShopId == 0);
    this.m_Count.color = itemCountByShopId <= 0 ? Color.red : Color.white;
    this.m_Price.text = ItemBag.Instance.GetShopItemPrice("shopitem_gemstone_reclaim").ToString();
    this.m_Price.color = !this.CanBuy() ? Color.red : Color.white;
  }

  private bool CanBuy()
  {
    return ItemBag.Instance.CheckCanBuyShopItem("shopitem_gemstone_reclaim", false);
  }

  public class Parameter : Popup.PopupParameter
  {
    public GemData gemData;
    public InventoryItemData inventoryData;
    public int slotIndex;
    public System.Action callback;
  }
}
