﻿// Decompiled with JetBrains decompiler
// Type: BarracksMoreInfoPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using UI;
using UnityEngine;

public class BarracksMoreInfoPopup : Popup
{
  public UnitAttribute stateOneAttribute = new UnitAttribute();
  public UILabel titleLabel;
  public UILabel generalTitleLabel;
  public UILabel describeLabel;
  public UILabel quantityNameLabel;
  public UILabel quantityLabel;
  public UILabel typeNameLabel;
  public UILabel typeLabel;
  public UILabel totalUpNamekeepLabel;
  public UILabel totalUpkeepLabel;
  public UILabel powerNameLabel;
  public UILabel powerLabel;
  public UILabel loadNameLabel;
  public UILabel loadLabel;
  public UILabel healthLabel;
  public UILabel attackLabel;
  public UILabel defenseLabel;
  public UILabel speedLabel;
  public UILabel healthValueLabel;
  public UILabel attackValueLabel;
  public UILabel defenseValueLabel;
  public UILabel speedValueLabel;
  public UILabel costTitleLabel;
  public BarracksResourcesComponent barracksResourcesComponent;
  public UILabel timeNameLabel;
  public UILabel timeLabel;
  public UILabel upKeepNameLabel;
  public UILabel upKeepLabel;
  public UILabel bonusesTittleLabel;
  public BarracksBennifitComponent[] bennifitComonents;
  public GameObject upkeepIconTotal;
  public GameObject upkeepIcon;
  public UILabel dismissTittleLabel;
  public UILabel dismissBtnLabel;
  public UISlider dismissSlider;
  public UILabel dismissNumLabel;
  public UIInput dismissInputLabel;
  public UILabel hintLabel1;
  public UILabel hintLabel2;
  public UILabel hintLabel3;
  public UILabel hintLabel4;
  private int MAX_DISMISS_COUNT;
  private int dismissCount;
  private Unit_StatisticsInfo TSC;
  private System.Action onClose;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    BarracksMoreInfoPopup.Parameter parameter = orgParam as BarracksMoreInfoPopup.Parameter;
    base.OnShow(orgParam);
    this.Initialize(parameter.TSC, parameter.buildingID, parameter.onClose);
  }

  public void Initialize(Unit_StatisticsInfo TSC, long buildingID, System.Action onClose)
  {
    this.TSC = TSC;
    this.onClose = onClose;
    this.initTitle();
    this.initAttribut();
    this.initInfo();
    this.initCost();
    this.initBonuses();
    this.initDismiss();
  }

  public void OndismissInputChange()
  {
    int num = 0;
    try
    {
      num = Convert.ToInt32(this.dismissInputLabel.value);
    }
    catch (FormatException ex)
    {
      num = 0;
    }
    catch (OverflowException ex)
    {
      num = this.MAX_DISMISS_COUNT;
    }
    finally
    {
      this.dismissCount = Mathf.Clamp(num, 0, this.MAX_DISMISS_COUNT);
      this.dismissInputLabel.value = this.dismissCount.ToString();
      this.dismissSlider.value = (float) this.dismissCount / (float) this.MAX_DISMISS_COUNT;
    }
  }

  public void OnDismissPlusClick()
  {
    this.dismissCount = Mathf.Min(this.MAX_DISMISS_COUNT, this.dismissCount + 1);
    this.dismissSlider.value = (float) this.dismissCount / (float) this.MAX_DISMISS_COUNT;
    this.dismissNumLabel.text = this.dismissCount.ToString();
  }

  public void OndismissMinusClick()
  {
    this.dismissCount = Mathf.Max(0, this.dismissCount - 1);
    this.dismissSlider.value = (float) this.dismissCount / (float) this.MAX_DISMISS_COUNT;
    this.dismissNumLabel.text = this.dismissCount.ToString();
  }

  public void OnSliderValueChange()
  {
    if (this.dismissCount == Mathf.Min(Mathf.CeilToInt((float) this.MAX_DISMISS_COUNT * this.dismissSlider.value), this.MAX_DISMISS_COUNT))
      return;
    this.dismissCount = Mathf.Min(Mathf.CeilToInt((float) this.MAX_DISMISS_COUNT * this.dismissSlider.value), this.MAX_DISMISS_COUNT);
    this.dismissNumLabel.text = this.dismissCount.ToString();
  }

  public void OnClose()
  {
    this.Close((UIControler.UIParameter) null);
  }

  public void CloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    this.onClose();
  }

  private void initTitle()
  {
    this.titleLabel.text = !(this.TSC.Type == "trap") ? ScriptLocalization.Get("id_uppercase_troop_details", true) : ScriptLocalization.Get("id_uppercase_trap_details", true);
    this.generalTitleLabel.text = ScriptLocalization.Get("id_uppercase_general", true);
    this.costTitleLabel.text = ScriptLocalization.Get("id_uppercase_cost", true);
    this.hintLabel1.text = ScriptLocalization.Get("barracks_detail_no_bonuses", true);
    this.hintLabel2.text = ScriptLocalization.Get("barracks_detail_no_bonuses", true);
    this.hintLabel3.text = ScriptLocalization.Get("barracks_detail_no_bonuses", true);
    this.hintLabel4.text = ScriptLocalization.Get("barracks_detail_no_bonuses", true);
  }

  private void initInfo()
  {
    int troopsCount = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).cityTroops.GetTroopsCount(this.TSC.ID);
    this.quantityNameLabel.text = ScriptLocalization.Get("barracks_quantity", true);
    this.quantityLabel.text = Utils.FormatThousands(troopsCount.ToString());
    this.typeNameLabel.text = ScriptLocalization.Get("barracks_type", true);
    this.typeLabel.text = ScriptLocalization.Get("id_uppercase_" + this.TSC.Type, true);
    if ((double) this.TSC.Upkeep > 0.0)
    {
      this.totalUpkeepLabel.gameObject.SetActive(true);
      this.totalUpNamekeepLabel.gameObject.SetActive(true);
      this.upkeepIconTotal.SetActive(true);
      this.totalUpNamekeepLabel.text = ScriptLocalization.Get("barracks_total_upkeep", true);
      this.totalUpkeepLabel.text = "-" + Utils.FormatThousands(this.TSC.Upkeep.ToString());
    }
    else
    {
      this.totalUpkeepLabel.gameObject.SetActive(false);
      this.totalUpNamekeepLabel.gameObject.SetActive(false);
      this.upkeepIconTotal.SetActive(false);
    }
    this.powerNameLabel.text = ScriptLocalization.Get("barracks_power", true);
    this.powerLabel.text = Utils.FormatThousands(this.TSC.Power.ToString());
    this.loadNameLabel.text = ScriptLocalization.Get("barracks_load", true);
    this.loadLabel.text = Utils.FormatThousands(this.TSC.GatherCapacity.ToString());
    this.healthLabel.text = ScriptLocalization.Get("barracks_health", true);
    this.attackLabel.text = ScriptLocalization.Get("barracks_attack", true);
    this.defenseLabel.text = ScriptLocalization.Get("barracks_defense", true);
    this.speedLabel.text = ScriptLocalization.Get("barracks_speed", true);
    if ((UnityEngine.Object) this.healthValueLabel != (UnityEngine.Object) null)
      this.healthValueLabel.text = this.TSC.Health_Level.ToString();
    if ((UnityEngine.Object) this.attackValueLabel != (UnityEngine.Object) null)
      this.attackValueLabel.text = this.TSC.Attack_Level.ToString();
    if ((UnityEngine.Object) this.defenseValueLabel != (UnityEngine.Object) null)
      this.defenseValueLabel.text = this.TSC.Defense_Level.ToString();
    if (!((UnityEngine.Object) this.speedValueLabel != (UnityEngine.Object) null))
      return;
    this.speedValueLabel.text = this.TSC.Speed_Level.ToString();
  }

  private void initAttribut()
  {
    this.stateOneAttribute.health.SetValue(this.TSC.Health_Level);
    this.stateOneAttribute.attack.SetValue(this.TSC.Attack_Level);
    this.stateOneAttribute.speed.SetValue(this.TSC.Speed_Level);
    this.stateOneAttribute.defense.SetValue(this.TSC.Defense_Level);
    this.stateOneAttribute.attackTypeIcon.spriteName = "icon_attack_type_" + this.TSC.AttackType;
    this.stateOneAttribute.strongTypeIcon1.spriteName = "icon_defense_type_" + this.TSC.Strength_1;
    this.stateOneAttribute.strongTypeIcon2.spriteName = "icon_defense_type_" + this.TSC.Strength_2;
    this.stateOneAttribute.defenceTypeIcon.spriteName = "icon_defense_type_" + this.TSC.DefenseType;
    this.stateOneAttribute.weakTypeIcon1.spriteName = "icon_attack_type_" + this.TSC.Weakness_1;
    this.stateOneAttribute.weakTypeIcon2.spriteName = "icon_attack_type_" + this.TSC.Weakness_2;
    this.stateOneAttribute.strongVSLabel.text = ScriptLocalization.Get("barracks_strong", true);
    this.stateOneAttribute.weakVSLabel.text = ScriptLocalization.Get("barracks_weak", true);
  }

  private void initCost()
  {
    this.barracksResourcesComponent.UpdateCostResources(this.TSC.BenefitCostWood(1), this.TSC.BenefitCostSilver(1), this.TSC.BenefitCostFood(1), this.TSC.BenefitCostOre(1), false);
    if ((double) this.TSC.Upkeep > 0.0)
    {
      this.upKeepNameLabel.gameObject.SetActive(true);
      this.upKeepLabel.gameObject.SetActive(true);
      this.upkeepIcon.SetActive(true);
      this.upKeepNameLabel.text = ScriptLocalization.Get("barracks_upkeep", true);
      this.upKeepLabel.text = "-" + this.TSC.Upkeep.ToString();
    }
    else
    {
      this.upKeepNameLabel.gameObject.SetActive(false);
      this.upKeepLabel.gameObject.SetActive(false);
      this.upkeepIcon.SetActive(false);
    }
    this.timeNameLabel.text = ScriptLocalization.Get("barracks_time", true);
    this.timeLabel.text = Utils.ConvertSecsToString((double) this.TSC.Training_Time);
  }

  private void initBonuses()
  {
    this.bonusesTittleLabel.text = ScriptLocalization.Get("id_uppercase_bonuses", true);
    this.bennifitComonents[0].bennifitSource = BarracksBennifitSource.HERO_SKILL;
    this.bennifitComonents[0].UpdateUI(this.TSC.Troop_Class_ID);
    this.bennifitComonents[1].bennifitSource = BarracksBennifitSource.RESEARCH;
    this.bennifitComonents[1].UpdateUI(this.TSC.Troop_Class_ID);
    this.bennifitComonents[2].bennifitSource = BarracksBennifitSource.BUILDING;
    this.bennifitComonents[2].UpdateUI(this.TSC.Troop_Class_ID);
    this.bennifitComonents[3].bennifitSource = BarracksBennifitSource.OTHERS;
    this.bennifitComonents[3].UpdateUI(this.TSC.Troop_Class_ID);
  }

  private void initDismiss()
  {
    string Term = !(this.TSC.FromBuildingType == "fortress") ? "id_uppercase_dismiss" : "id_uppercase_destroy";
    this.dismissTittleLabel.text = ScriptLocalization.Get(Term, true);
    this.dismissBtnLabel.text = ScriptLocalization.Get(Term, true);
    this.MAX_DISMISS_COUNT = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).cityTroops.GetTroopsCount(this.TSC.ID);
    this.dismissSlider.value = 0.0f;
    this.dismissCount = 0;
    this.dismissNumLabel.text = this.dismissCount.ToString();
    if (this.MAX_DISMISS_COUNT > 0)
    {
      this.dismissSlider.enabled = true;
      UISlider dismissSlider = this.dismissSlider;
      dismissSlider.onDragFinished = dismissSlider.onDragFinished + new UIProgressBar.OnDragFinished(this.OnSliderValueChange);
    }
    else
      this.dismissSlider.enabled = false;
  }

  public void OnDismissBtnClick()
  {
    if (this.dismissCount <= 0)
      return;
    string str1 = !(this.TSC.FromBuildingType == "fortress") ? "barracks_uppercase_dismiss" : "barracks_uppercase_destroy";
    string str2 = !(this.TSC.FromBuildingType == "fortress") ? "dismiss_troops_question" : "destroy_traps_question";
    UIManager.inst.OpenPopup("DismissTroopConfirmPopup", (Popup.PopupParameter) new DismissTroopConfirmPopup.Parameter()
    {
      titleKey = str1,
      contentKey = str2,
      okKey = "id_uppercase_yes",
      noKey = "id_uppercase_no",
      okCallback = new System.Action(this.OnCancelYesPressed),
      noCallBack = new System.Action(this.OnCancelNoPressed),
      unit = this.TSC
    });
  }

  private void OnCancelYesPressed()
  {
    MessageHub.inst.GetPortByAction("City:dismissTroops").SendRequest(Utils.Hash((object) "city_id", (object) CityManager.inst.GetCityID(), (object) "troops", (object) Utils.Hash((object) this.TSC.ID, (object) this.dismissCount)), (System.Action<bool, object>) ((bSuccess, data) =>
    {
      if (!bSuccess)
        return;
      this.CloseHandler();
    }), true);
  }

  private void OnCancelNoPressed()
  {
  }

  public class Parameter : Popup.PopupParameter
  {
    public Unit_StatisticsInfo TSC;
    public long buildingID;
    public System.Action onClose;
  }
}
