﻿// Decompiled with JetBrains decompiler
// Type: UserMerlinTowerData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class UserMerlinTowerData
{
  public long magic;
  public long mineFieldJobId;
  public int mineId;
  public long mineJobId;
  public int towerId;
  public long maxCapacity;
  public long capacity;
  public int battleCdTime;
  public int canBattle;
  public int showRedPoint;
  public List<int> shopItems;
  public int cleared;
  public long version;
  public int lastFindTime;

  public MagicTowerMainInfo CurrentTowerMainInfo
  {
    get
    {
      return ConfigManager.inst.DB_MagicTowerMain.GetData(this.towerId);
    }
  }

  public int QuickSearchCd
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("magic_tower_fast_occupy_CD");
      if (data != null)
        return this.lastFindTime + data.ValueInt - NetServerTime.inst.ServerTimestamp;
      return 0;
    }
  }

  public bool IsInTower
  {
    get
    {
      return this.mineFieldJobId == 0L;
    }
  }

  public bool IsInKingdom
  {
    get
    {
      return this.mineFieldJobId != 0L;
    }
  }

  public bool HasMonster
  {
    get
    {
      MagicTowerMainInfo currentTowerMainInfo = this.CurrentTowerMainInfo;
      if (currentTowerMainInfo != null && currentTowerMainInfo.TowerMonsterId != 0)
        return this.cleared == 0;
      return false;
    }
  }

  public int CurrentPage
  {
    get
    {
      MagicTowerMainInfo currentTowerMainInfo = this.CurrentTowerMainInfo;
      if (currentTowerMainInfo != null && currentTowerMainInfo.NoviceProtection > 0)
        return 1;
      return (this.mineId - 1) / 9 + 1;
    }
  }

  public int BattleCd
  {
    get
    {
      return Mathf.Max(0, this.battleCdTime - NetServerTime.inst.ServerTimestamp);
    }
  }

  public int LeftCdTime
  {
    get
    {
      return Mathf.Max(0, this.canBattle - NetServerTime.inst.ServerTimestamp);
    }
  }

  public bool IsInCd
  {
    get
    {
      return this.canBattle > NetServerTime.inst.ServerTimestamp;
    }
  }
}
