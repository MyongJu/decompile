﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerPost
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BuildingControllerPost : BuildingControllerNew
{
  private bool _prepare = true;
  public TimerChestManager timerChestManager;
  public UnityEngine.Animation postChestArrived;
  public TextMesh postArriveTime;

  public void OnBuildingClick()
  {
    this.timerChestManager.OpenTimerChestPopup();
  }

  private bool prepare
  {
    get
    {
      return this._prepare;
    }
    set
    {
      if (value == this._prepare)
        return;
      this._prepare = value;
      this.postChestArrived.gameObject.SetActive(value);
    }
  }

  public void OnSeconed(int serverTime)
  {
    this.prepare = this.timerChestManager.isPrepare;
    this.postChestArrived.gameObject.SetActive(this.prepare);
    this.postArriveTime.text = Utils.FormatTime(this.timerChestManager.leftTime, false, false, true);
    this.postArriveTime.gameObject.SetActive(this.timerChestManager.leftTime != 0);
  }

  public void OnEnable()
  {
    this.InitBuilding();
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSeconed);
  }

  public override void InitBuilding()
  {
    base.InitBuilding();
    this.timerChestManager.Init();
    this.prepare = false;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSeconed);
  }

  public override void Dispose()
  {
    base.Dispose();
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSeconed);
  }
}
