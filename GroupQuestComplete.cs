﻿// Decompiled with JetBrains decompiler
// Type: GroupQuestComplete
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class GroupQuestComplete : MonoBehaviour
{
  public UILabel title;
  public UIGrid grid;
  public GroupQuestRewardRender template;
  public UIScrollView scrollView;
  public UILabel descriptionLabel;

  public void UpdateUI()
  {
    List<QuestData2> groupQuests = DBManager.inst.DB_Quest.GroupQuests;
    if (groupQuests.Count < 1)
      return;
    List<QuestReward> reward = QuestGroupReward.CreateReward((int) groupQuests[0].QuestID);
    this.ClearGrid();
    this.scrollView.ResetPosition();
    for (int index = 0; index < reward.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.template.gameObject);
      gameObject.SetActive(true);
      gameObject.GetComponent<GroupQuestRewardRender>().UpdateUI(reward[index]);
    }
    this.title.text = Utils.XLAT("advanced_tutorial_uppercase_title");
    this.descriptionLabel.text = Utils.XLAT("advanced_tutorial_complete_description");
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.grid.Reposition()));
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.grid);
  }
}
