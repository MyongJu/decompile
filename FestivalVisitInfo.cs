﻿// Decompiled with JetBrains decompiler
// Type: FestivalVisitInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class FestivalVisitInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "visit_group")]
  public int visitGroup;
  [Config(Name = "max_num")]
  public int maxNum;
  [Config(Name = "max_per_block")]
  public int maxPerBlock;
  [Config(Name = "max_visit")]
  public int maxVisit;
  [Config(Name = "visit_cd")]
  public int visitCD;
  [Config(Name = "item_cost_id")]
  public int itemCostId;
  [Config(Name = "drop_id")]
  public int dropGroup;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "reward_display_name_1")]
  public string reward_display_name_1;
  [Config(Name = "reward_display_image_1")]
  public string reward_display_image_1;
  [Config(Name = "reward_display_name_2")]
  public string reward_display_name_2;
  [Config(Name = "reward_display_image_2")]
  public string reward_display_image_2;
  [Config(Name = "reward_display_name_3")]
  public string reward_display_name_3;
  [Config(Name = "reward_display_image_3")]
  public string reward_display_image_3;
  [Config(Name = "reward_display_name_4")]
  public string reward_display_name_4;
  [Config(Name = "reward_display_image_4")]
  public string reward_display_image_4;
  [Config(Name = "reward_display_name_5")]
  public string reward_display_name_5;
  [Config(Name = "reward_display_image_5")]
  public string reward_display_image_5;
  [Config(Name = "description")]
  public string description;
  [Config(Name = "type")]
  public string type;

  public string LOC_Name
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }

  public string LOC_Description
  {
    get
    {
      return Utils.XLAT(this.description);
    }
  }
}
