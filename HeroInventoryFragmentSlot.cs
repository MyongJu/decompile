﻿// Decompiled with JetBrains decompiler
// Type: HeroInventoryFragmentSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HeroInventoryFragmentSlot : MonoBehaviour
{
  [SerializeField]
  private ParliamentHeroItem _parliamentHeroItem;
  [SerializeField]
  private GameObject _highlightNode;
  public System.Action<ParliamentHeroInfo> OnFragmentPressed;
  private ParliamentHeroInfo _parliamentHeroInfo;

  public void SetData(ParliamentHeroInfo parliamentHeroInfo)
  {
    this._parliamentHeroInfo = parliamentHeroInfo;
    this.UpdateUI();
    this.RefreshUI((ParliamentHeroInfo) null);
  }

  public void OnFragmentClicked()
  {
    if (this._parliamentHeroInfo == null || this.OnFragmentPressed == null)
      return;
    this.OnFragmentPressed(this._parliamentHeroInfo);
  }

  public void RefreshUI(ParliamentHeroInfo parliamentHeroInfo)
  {
    NGUITools.SetActive(this._highlightNode, this._parliamentHeroInfo == parliamentHeroInfo);
  }

  private void UpdateUI()
  {
    if (this._parliamentHeroInfo == null)
      return;
    this._parliamentHeroItem.SetFragmentData(this._parliamentHeroInfo, false);
  }
}
