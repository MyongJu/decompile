﻿// Decompiled with JetBrains decompiler
// Type: WatchTowerDetailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class WatchTowerDetailPopup : Popup
{
  private PlayerWithMarchInfoComponentData linker0 = new PlayerWithMarchInfoComponentData();
  private DragonWithSkillComponentData linker1 = new DragonWithSkillComponentData();
  private ResourceWithUnCollectComponentData linker2 = new ResourceWithUnCollectComponentData();
  protected WatchTowerDetailPopup.Parameter param;
  protected WatchEntity entity;
  public UITable contentTable;
  public GameObject container;
  public GameObject subTitle;
  public GameObject marchInfo;
  public GameObject dragon;
  public GameObject resource;
  public GameObject troopCount;
  public GameObject tier;
  public GameObject reinforceTroop;
  public GameObject buildings;
  public GameObject research;
  public GameObject equipment;
  public GameObject talent;
  public GameObject oneLabel;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.linker0.x = "1";
    this.linker1.level = "1";
    this.linker2.collect = new Dictionary<string, string>();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.param = orgParam as WatchTowerDetailPopup.Parameter;
    this.entity = this.param.entity;
    this.UpdateUI();
    WatchtowerUtilities.onRemoved += new System.Action<WatchEntity>(this.OnRemoveHandler);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    WatchtowerUtilities.onRemoved -= new System.Action<WatchEntity>(this.OnRemoveHandler);
  }

  private void OnRemoveHandler(WatchEntity eventEntity)
  {
    if (this.entity.m_Id != eventEntity.m_Id)
      return;
    this.OnCloseBtnClick();
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.DrawEnemyOverView();
    this.DrawDragon();
    this.DrawResource();
    this.DrawTroops();
    this.DrawResearch();
    this.DrawEquipment();
    this.DrawTalent();
    this.DrawFailure();
    this.DrawBuildings();
    this.container.GetComponent<UITable>().Reposition();
  }

  private void DrawEnemyOverView()
  {
    if (!this.param.hashtable.ContainsKey((object) "default"))
      return;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    this.AddSubtitle("id_uppercase_player_info", parent);
    PlayerWithMarchInfoComponent component = NGUITools.AddChild(parent, this.marchInfo).GetComponent<PlayerWithMarchInfoComponent>();
    component.purpose = this.param.hashtable[(object) "purpose"].ToString();
    if (this.param.hashtable.ContainsKey((object) "lord"))
      component.lv = "Lv." + (this.param.hashtable[(object) "lord"] as Hashtable)[(object) "level"].ToString();
    component.SetData(this.entity);
    component.FeedData<PlayerWithMarchInfoComponentData>(this.param.hashtable[(object) "default"] as Hashtable);
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawDragon()
  {
    if (!this.param.hashtable.ContainsKey((object) "dragon"))
      return;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    this.AddSubtitle("id_uppercase_dragon", parent);
    NGUITools.AddChild(parent, this.dragon).transform.Find("enmeyDragon").GetComponent<DragonWithSkillComponent>().FeedData<DragonWithSkillComponentData>(this.param.hashtable[(object) "dragon"] as Hashtable);
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawResource()
  {
    if (!this.param.hashtable.ContainsKey((object) "resource"))
      return;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    this.AddSubtitle(Utils.XLAT("id_uppercase_resources"), parent);
    NGUITools.AddChild(parent, this.resource).GetComponentInChildren<ResourceWithUnCollectComponent>().FeedData<ResourceWithUnCollectComponentData>(this.param.hashtable[(object) "resource"] as Hashtable);
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawTroops()
  {
    if (!this.param.hashtable.ContainsKey((object) "troops"))
      return;
    this.DrawMarchingTroop(this.param.hashtable[(object) "troops"] as Hashtable);
    this.DrawDefendingTroop(this.param.hashtable[(object) "troops"] as Hashtable);
    this.DrawReinforceTroop(this.param.hashtable[(object) "troops"] as Hashtable);
    this.DrawRallyTroop(this.param.hashtable[(object) "troops"] as Hashtable);
    this.DrawTrap(this.param.hashtable[(object) "troops"] as Hashtable);
  }

  private void DrawDefendingTroop(Hashtable data)
  {
    if (!data.ContainsKey((object) "defending_troops"))
      return;
    Hashtable hashtable1 = data[(object) "defending_troops"] as Hashtable;
    this.AddTroopCount(this.contentTable.gameObject, hashtable1[(object) "amount"] != null ? hashtable1[(object) "amount"].ToString() : "0");
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    this.AddSubtitle("TROOPS", parent);
    Hashtable hashtable2 = hashtable1[(object) "tier"] as Hashtable;
    if (hashtable2 != null)
    {
      ArrayList arrayList = new ArrayList(hashtable2.Keys);
      new List<string>((IEnumerable<string>) arrayList.ToArray(typeof (string))).Sort((Comparison<string>) ((x, y) => int.Parse(x.Substring(1)).CompareTo(int.Parse(y.Substring(1)))));
      for (int index = arrayList.Count - 1; index >= 0; --index)
        NGUITools.AddChild(parent, this.tier).GetComponent<TierListComponent>().FeedData((IComponentData) new TierComponentData(arrayList[index].ToString(), hashtable2[arrayList[index]] as Hashtable));
    }
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawMarchingTroop(Hashtable data)
  {
    if (!data.ContainsKey((object) "march"))
      return;
    Hashtable hashtable1 = data[(object) "march"] as Hashtable;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    this.AddSubtitle("id_uppercase_troops", parent);
    this.AddTroopCount(parent, hashtable1[(object) "amount"] != null ? hashtable1[(object) "amount"].ToString() : "0");
    Hashtable hashtable2 = hashtable1[(object) "tier"] as Hashtable;
    if (hashtable2 != null)
    {
      List<string> stringList = new List<string>((IEnumerable<string>) new ArrayList(hashtable2.Keys).ToArray(typeof (string)));
      stringList.Sort((Comparison<string>) ((x, y) => int.Parse(x.Substring(1)).CompareTo(int.Parse(y.Substring(1)))));
      for (int index = stringList.Count - 1; index >= 0; --index)
        NGUITools.AddChild(parent, this.tier).GetComponent<TierListComponent>().FeedData((IComponentData) new TierComponentData(stringList[index].ToString(), hashtable2[(object) stringList[index]] as Hashtable));
    }
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawTrap(Hashtable data)
  {
    if (!data.ContainsKey((object) "traps"))
      return;
    Hashtable hashtable1 = data[(object) "traps"] as Hashtable;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    this.AddSubtitle("TRAPS", parent);
    this.AddTroopCount(parent, hashtable1[(object) "amount"].ToString());
    Hashtable hashtable2 = hashtable1[(object) "tier"] as Hashtable;
    if (hashtable2 != null)
    {
      List<string> stringList = new List<string>((IEnumerable<string>) new ArrayList(hashtable2.Keys).ToArray(typeof (string)));
      stringList.Sort((Comparison<string>) ((x, y) => int.Parse(x.Substring(1)).CompareTo(int.Parse(y.Substring(1)))));
      for (int index = stringList.Count - 1; index >= 0; --index)
        NGUITools.AddChild(parent, this.tier).GetComponent<TierListComponent>().FeedData((IComponentData) new TierComponentData(stringList[index].ToString(), hashtable2[(object) stringList[index]] as Hashtable));
    }
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawReinforceTroop(Hashtable data)
  {
    if (!data.ContainsKey((object) "reinforcing_troops"))
      return;
    Hashtable hashtable1 = data[(object) "reinforcing_troops"] as Hashtable;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    this.AddSubtitle("REINFORCE TROOPS", parent);
    this.AddTroopCount(parent, hashtable1[(object) "amount"].ToString());
    foreach (DictionaryEntry dictionaryEntry in hashtable1)
    {
      if (dictionaryEntry.Key.ToString() != "amount")
      {
        Hashtable hashtable2 = ((dictionaryEntry.Value as Hashtable)[(object) "troops"] as Hashtable)[(object) "tier"] as Hashtable;
        List<TierComponentData> tiers = new List<TierComponentData>();
        if (hashtable2 != null)
        {
          List<string> stringList = new List<string>((IEnumerable<string>) new ArrayList(hashtable2.Keys).ToArray(typeof (string)));
          stringList.Sort((Comparison<string>) ((x, y) => int.Parse(x.Substring(1)).CompareTo(int.Parse(y.Substring(1)))));
          for (int index = stringList.Count - 1; index >= 0; --index)
          {
            TierComponentData tierComponentData = new TierComponentData(stringList[index].ToString(), hashtable2[(object) stringList[index]] as Hashtable);
            tiers.Add(tierComponentData);
          }
        }
        ReinforcePlayerWithTierComponent component = NGUITools.AddChild(this.contentTable.gameObject, this.reinforceTroop).GetComponent<ReinforcePlayerWithTierComponent>();
        component.parent = parent;
        component.FeedData((IComponentData) new ReinforcePlayerWithTierComponentData(tiers, dictionaryEntry.Value as Hashtable));
      }
    }
  }

  private void DrawRallyTroop(Hashtable data)
  {
    if (!data.ContainsKey((object) "rally_troops"))
      return;
    Hashtable hashtable1 = data[(object) "rally_troops"] as Hashtable;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    parent.name = "RALLY TROOPS";
    this.AddSubtitle(Utils.XLAT("watchtower_march_rally"), parent);
    this.AddTroopCount(parent, hashtable1[(object) "amount"].ToString());
    foreach (DictionaryEntry dictionaryEntry in hashtable1)
    {
      if (dictionaryEntry.Key.ToString() != "amount")
      {
        Hashtable hashtable2 = ((dictionaryEntry.Value as Hashtable)[(object) "troops"] as Hashtable)[(object) "tier"] as Hashtable;
        List<TierComponentData> tiers = new List<TierComponentData>();
        if (hashtable2 != null)
        {
          List<string> stringList = new List<string>((IEnumerable<string>) new ArrayList(hashtable2.Keys).ToArray(typeof (string)));
          stringList.Sort((Comparison<string>) ((x, y) => int.Parse(x.Substring(1)).CompareTo(int.Parse(y.Substring(1)))));
          for (int index = stringList.Count - 1; index >= 0; --index)
          {
            TierComponentData tierComponentData = new TierComponentData(stringList[index].ToString(), hashtable2[(object) stringList[index]] as Hashtable);
            tiers.Add(tierComponentData);
          }
        }
        ReinforcePlayerWithTierComponent component = NGUITools.AddChild(parent, this.reinforceTroop).GetComponent<ReinforcePlayerWithTierComponent>();
        component.parent = parent;
        component.FeedData((IComponentData) new ReinforcePlayerWithTierComponentData(tiers, dictionaryEntry.Value as Hashtable));
      }
    }
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawBuildings()
  {
    if (!this.param.hashtable.ContainsKey((object) "building_benefits"))
      return;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    this.AddSubtitle("mail_scout_report_uppercase_buildings_subtitle", parent);
    Hashtable table = this.param.hashtable[(object) "building_benefits"] as Hashtable;
    BuildingComponent component = NGUITools.AddChild(this.contentTable.gameObject, this.buildings).GetComponent<BuildingComponent>();
    component.parent = parent;
    component.FeedData((IComponentData) new BuildingComponentData(table));
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawResearch()
  {
    if (!this.param.hashtable.ContainsKey((object) "research_benefits"))
      return;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    this.AddSubtitle("id_uppercase_research", parent);
    Hashtable table = this.param.hashtable[(object) "research_benefits"] as Hashtable;
    ResearchComponent component = NGUITools.AddChild(this.contentTable.gameObject, this.research).GetComponent<ResearchComponent>();
    component.parent = parent;
    component.FeedData((IComponentData) new ResearchComponentData(table));
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawEquipment()
  {
    if (!this.param.hashtable.ContainsKey((object) "equipment_benefits"))
      return;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    this.AddSubtitle("id_uppercase_equipment", parent);
    Hashtable table = this.param.hashtable[(object) "equipment_benefits"] as Hashtable;
    ScoutMailEquipmentComponent component = NGUITools.AddChild(this.contentTable.gameObject, this.equipment).GetComponent<ScoutMailEquipmentComponent>();
    component.parent = parent;
    component.FeedData((IComponentData) new ScoutMailEquipmentComponentData(table));
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawTalent()
  {
    if (!this.param.hashtable.ContainsKey((object) "lord_benefits"))
      return;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    this.AddSubtitle("id_uppercase_lord_talents", parent);
    Hashtable table = this.param.hashtable[(object) "lord_benefits"] as Hashtable;
    TalentComponent component = NGUITools.AddChild(this.contentTable.gameObject, this.talent).GetComponent<TalentComponent>();
    component.parent = parent;
    component.FeedData((IComponentData) new TalentComponentData(table));
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawFailure()
  {
    if (!this.param.hashtable.ContainsKey((object) "fail"))
      return;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    NGUITools.AddChild(parent, this.oneLabel).GetComponentInChildren<UILabel>().text = Utils.XLAT("scout_fail_reason_" + this.param.hashtable[(object) "fail"].ToString());
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void AddSubtitle(string titleKey, GameObject parent)
  {
    NGUITools.AddChild(parent, this.subTitle).GetComponentInChildren<UILabel>().text = Utils.XLAT(titleKey);
  }

  private void AddTroopCount(GameObject parent, string count)
  {
    NGUITools.AddChild(parent, this.troopCount).transform.Find("TroopCountLabel").GetComponent<UILabel>().text = count;
  }

  public class Parameter : Popup.PopupParameter
  {
    public Hashtable hashtable;
    public WatchEntity entity;
  }
}
