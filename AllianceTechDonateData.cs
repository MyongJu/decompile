﻿// Decompiled with JetBrains decompiler
// Type: AllianceTechDonateData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class AllianceTechDonateData
{
  public const int MAX_DONATE_COL = 3;
  public int allianceTechId;
  public List<AllianceTechDonateData.AllianceTechDonateStruct> datas;

  public void Decode(int inAllianceTechId, object orgData)
  {
    this.allianceTechId = inAllianceTechId;
    ArrayList data;
    DatabaseTools.CheckAndParseOrgData(orgData, out data);
    if (this.datas == null)
    {
      this.datas = new List<AllianceTechDonateData.AllianceTechDonateStruct>(3);
      for (int index = 0; index < 3; ++index)
        this.datas.Add(new AllianceTechDonateData.AllianceTechDonateStruct());
    }
    for (int index = 0; index < 3; ++index)
    {
      if (index < data.Count && data[index] is Hashtable)
        this.datas[index].Decode(data[index]);
      else
        this.datas[index].Clear();
    }
  }

  public class AllianceTechDonateStruct
  {
    public string type;
    public long count;
    public long rewardDonation;
    public long rewardHonor;
    public long rewardFund;
    public long rewardXp;

    public void Clear()
    {
      this.type = string.Empty;
      this.count = 0L;
    }

    public void Decode(object orgData)
    {
      Hashtable data;
      DatabaseTools.CheckAndParseOrgData(orgData, out data);
      DatabaseTools.UpdateData(data, "type", ref this.type);
      DatabaseTools.UpdateData(data, "value", ref this.count);
      DatabaseTools.UpdateData(data, "reward_donation", ref this.rewardDonation);
      DatabaseTools.UpdateData(data, "reward_alliance_exp", ref this.rewardXp);
      DatabaseTools.UpdateData(data, "reward_honor", ref this.rewardHonor);
      DatabaseTools.UpdateData(data, "reward_fund", ref this.rewardFund);
    }

    public struct Param
    {
      public const string TYPE = "type";
      public const string VALUE = "value";
      public const string reward_donation = "reward_donation";
      public const string reward_alliance_exp = "reward_alliance_exp";
      public const string reward_honor = "reward_honor";
      public const string reward_fund = "reward_fund";
    }
  }
}
