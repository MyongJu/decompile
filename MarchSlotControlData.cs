﻿// Decompiled with JetBrains decompiler
// Type: MarchSlotControlData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class MarchSlotControlData
{
  public MarchSlotControlData.DataType Type { get; set; }

  public bool IsDefense { get; set; }

  public bool IsOwner { get; set; }

  public bool IsTarget { get; set; }

  public string DragonSkillType { get; set; }

  public bool CanSpeedUp { get; set; }

  public enum DataType
  {
    Rally,
    DarkKnight,
    None,
  }
}
