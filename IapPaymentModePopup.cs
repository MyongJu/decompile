﻿// Decompiled with JetBrains decompiler
// Type: IapPaymentModePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class IapPaymentModePopup : Popup
{
  public static bool AlipayEnabled;
  [SerializeField]
  private UITable _tableContainer;
  [SerializeField]
  private GameObject _rootAlipay;
  private IapPaymentModePopup.Parameter _parameter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._parameter = orgParam as IapPaymentModePopup.Parameter;
    this.UpdateUI();
  }

  protected void UpdateUI()
  {
    this._rootAlipay.SetActive(IapPaymentModePopup.AlipayEnabled);
    this._tableContainer.Reposition();
  }

  public void OnButtonAlipayClicked()
  {
    if (this._parameter != null && this._parameter.Callback != null)
      this._parameter.Callback(IapPaymentModePopup.PaymentMode.Alipay);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnButtonWechatClicked()
  {
    if (this._parameter != null && this._parameter.Callback != null)
      this._parameter.Callback(IapPaymentModePopup.PaymentMode.Wechat);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnButtonCloseClicked()
  {
    if (this._parameter != null && this._parameter.Callback != null)
      this._parameter.Callback(IapPaymentModePopup.PaymentMode.None);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public enum PaymentMode
  {
    None,
    Alipay,
    Wechat,
  }

  public class Parameter : Popup.PopupParameter
  {
    public System.Action<IapPaymentModePopup.PaymentMode> Callback;
  }
}
