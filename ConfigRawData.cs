﻿// Decompiled with JetBrains decompiler
// Type: ConfigRawData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigRawData
{
  public Dictionary<string, int> _s2i;
  public Dictionary<int, string> _i2s;
  public List<int> _ids;
  public Hashtable _raw;
  public ArrayList _header;

  public void Initialize(Hashtable raw)
  {
    this._s2i = new Dictionary<string, int>();
    this._i2s = new Dictionary<int, string>();
    this._ids = new List<int>();
    this._raw = raw;
    this._header = this._raw[(object) "headers"] as ArrayList;
    IDictionaryEnumerator enumerator = raw.GetEnumerator();
    while (enumerator.MoveNext())
    {
      int result;
      if (int.TryParse(enumerator.Key.ToString(), out result))
      {
        ArrayList arrayList = enumerator.Value as ArrayList;
        if (arrayList != null && arrayList.Count > 0)
        {
          string key = arrayList[0].ToString();
          this._s2i.Add(key, result);
          this._i2s.Add(result, key);
          this._ids.Add(result);
        }
        else
          D.error((object) "invalid row.");
      }
    }
  }

  public int GetFieldIndex(string fieldName)
  {
    return ConfigManager.GetIndex(this._header, fieldName);
  }

  public bool TryGetRecordField(System.Type type, int internalId, int index, out object target)
  {
    ArrayList arrayList = this._raw[(object) internalId.ToString()] as ArrayList;
    return ConfigRawData.TryConvertValue(type, arrayList[index], out target);
  }

  public static bool TryConvertValue(System.Type type, object source, out object target)
  {
    bool flag = false;
    target = (object) null;
    try
    {
      if (type.IsEnum)
      {
        if (Enum.IsDefined(type, source))
        {
          target = Enum.Parse(type, source.ToString());
          flag = true;
        }
      }
      else
      {
        target = Convert.ChangeType(source, type);
        flag = true;
      }
    }
    catch
    {
      flag = false;
    }
    return flag;
  }
}
