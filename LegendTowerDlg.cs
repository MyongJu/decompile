﻿// Decompiled with JetBrains decompiler
// Type: LegendTowerDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class LegendTowerDlg : UI.Dialog
{
  private HubPort defendAltarPort = MessageHub.inst.GetPortByAction("Legend:defendAltar");
  private LegendTowerDlg.Data data;
  [SerializeField]
  private LegendTowerDlg.Panel panel;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    List<LegendData> currentLegend1 = DBManager.inst.DB_Legend.GetCurrentLegend(2);
    List<LegendData> currentLegend2 = DBManager.inst.DB_Legend.GetCurrentLegend(0);
    this.ClearLegendSlots();
    this.data.selectedLegendID = this.data.currentLegendID = 0L;
    if (currentLegend1.Count > 0)
    {
      long legendId = (long) currentLegend1[0].LegendID;
      this.AddLegendSlot(legendId).isChecked = true;
      this.data.selectedLegendID = this.data.currentLegendID = legendId;
    }
    this.panel.defenseSlot.onCancelButtonClick = new System.Action(this.OnDefenseLegendCancelClick);
    this.panel.defenseSlot.legendId = this.data.currentLegendID;
    for (int index = 0; index < currentLegend2.Count; ++index)
      this.AddLegendSlot((long) currentLegend2[index].LegendID).isChecked = false;
    UIPanel component = this.panel.legendsScrollView.gameObject.GetComponent<UIPanel>();
    component.clipOffset = Vector2.zero;
    component.transform.localPosition = new Vector3(0.0f, component.transform.localPosition.y, 0.0f);
    this.panel.legendsTable.repositionNow = true;
    this.panel.BT_Confirm.isEnabled = this.data.isDataChanged;
  }

  private void ClearLegendSlots()
  {
    if (this.panel.legendSlots == null)
      return;
    Dictionary<long, LegendSlot>.ValueCollection.Enumerator enumerator = this.panel.legendSlots.Values.GetEnumerator();
    while (enumerator.MoveNext())
      UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    this.panel.legendSlots.Clear();
  }

  private LegendSlot AddLegendSlot(long legendId)
  {
    GameObject gameObject = NGUITools.AddChild(this.panel.legendsTable.gameObject, this.panel.legendSlotOrg.gameObject);
    gameObject.name = string.Format("Legend_{0}", (object) legendId);
    gameObject.SetActive(true);
    LegendSlot component = gameObject.GetComponent<LegendSlot>();
    component.legendId = legendId;
    component.isChecked = false;
    component.onSlotClick += new System.Action<long>(this.OnLegendClick);
    this.panel.legendSlots.Add(legendId, component);
    return component;
  }

  public void OnDefenseLegendCancelClick()
  {
    if (this.data.selectedLegendID != 0L)
      this.panel.legendSlots[this.data.selectedLegendID].isChecked = false;
    this.panel.defenseSlot.legendId = this.data.selectedLegendID = 0L;
    this.panel.BT_Confirm.isEnabled = this.data.isDataChanged;
  }

  public void OnLegendClick(long legendId)
  {
    if (this.data.selectedLegendID != 0L)
      this.panel.legendSlots[this.data.selectedLegendID].isChecked = false;
    this.panel.defenseSlot.legendId = this.data.selectedLegendID = legendId;
    if (this.data.selectedLegendID != 0L)
      this.panel.legendSlots[this.data.selectedLegendID].isChecked = true;
    this.panel.legendSlots[legendId].isChecked = true;
    this.panel.BT_Confirm.isEnabled = this.data.isDataChanged;
  }

  public void OnConfirmClick()
  {
    if (!this.data.isDataChanged)
      return;
    this.defendAltarPort.SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "legend", (object) this.data.selectedLegendID, (object) "off_legend", (object) this.data.currentLegendID), new System.Action<bool, object>(this.OnConfirmCallback), true);
  }

  private void OnConfirmCallback(bool result, object res)
  {
    List<LegendData> currentLegend = DBManager.inst.DB_Legend.GetCurrentLegend(2);
    this.data.currentLegendID = currentLegend.Count <= 0 ? 0L : (long) currentLegend[0].LegendID;
    this.panel.BT_Confirm.isEnabled = this.data.isDataChanged;
  }

  private struct Data
  {
    public long currentLegendID;
    public long selectedLegendID;

    public bool isDataChanged
    {
      get
      {
        return this.currentLegendID != this.selectedLegendID;
      }
    }
  }

  [Serializable]
  protected class Panel
  {
    [HideInInspector]
    public Dictionary<long, LegendSlot> legendSlots = new Dictionary<long, LegendSlot>();
    public LegendTowerDenfeseSlot defenseSlot;
    public LegendSlot legendSlotOrg;
    public UIScrollView legendsScrollView;
    public UITable legendsTable;
    public UIButton BT_Confirm;
  }
}
