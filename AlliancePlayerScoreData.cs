﻿// Decompiled with JetBrains decompiler
// Type: AlliancePlayerScoreData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class AlliancePlayerScoreData
{
  private string _name;
  private long _score;

  public string Name
  {
    get
    {
      return this._name;
    }
  }

  public long Score
  {
    get
    {
      return this._score;
    }
  }

  public int Rank { get; set; }

  public bool Decode(object source)
  {
    Hashtable hashtable = source as Hashtable;
    if (hashtable == null)
      return false;
    string empty = string.Empty;
    if (hashtable.ContainsKey((object) "name") && hashtable[(object) "name"] != null)
      this._name = hashtable[(object) "name"].ToString();
    if (hashtable.ContainsKey((object) "score") && hashtable[(object) "score"] != null)
      long.TryParse(hashtable[(object) "score"].ToString(), out this._score);
    return true;
  }
}
