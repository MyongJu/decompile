﻿// Decompiled with JetBrains decompiler
// Type: ConfigDiyLotteryGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDiyLotteryGroup
{
  private List<DiyLotteryGroupInfo> _diyLotteryGroupInfoList = new List<DiyLotteryGroupInfo>();
  private Dictionary<string, DiyLotteryGroupInfo> _datas;
  private Dictionary<int, DiyLotteryGroupInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<DiyLotteryGroupInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, DiyLotteryGroupInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._diyLotteryGroupInfoList.Add(enumerator.Current);
    }
  }

  public DiyLotteryGroupInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (DiyLotteryGroupInfo) null;
  }

  public DiyLotteryGroupInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (DiyLotteryGroupInfo) null;
  }
}
