﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_Bose_Rally_Attack
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;

public class AllianceWar_MainDlg_Bose_Rally_Attack : AllianceWar_MainDlg_Rally_Slot
{
  protected override void SetupBelowList(RallyData rallyData, long uid, long allianceId, int memberCount)
  {
    ConfigGveBossData data1 = ConfigManager.inst.DB_GveBoss.GetData(rallyData.bossId);
    AllianceWar_MainDlg_RallyList.param.rallyType = AllianceWar_MainDlg_RallyList.Params.RallyListType.BOSS;
    AllianceWar_MainDlg_RallyList.param.canJoin = false;
    AllianceWar_MainDlg_RallyList.param.memberCount = 1;
    if (data1 != null)
    {
      AllianceWar_MainDlg_RallyList.param.ownerName = string.Format("Lv{0} {1}", (object) data1.boss_level, (object) ScriptLocalization.Get(data1.name, true));
      AllianceWar_MainDlg_RallyList.param.bossIcon = data1.ImagePath;
    }
    else
    {
      WorldBossData worldBossData = DBManager.inst.DB_WorldBossDB.Get(rallyData.bossId);
      if (worldBossData != null)
      {
        WorldBossStaticInfo data2 = ConfigManager.inst.DB_WorldBoss.GetData(worldBossData.configId);
        if (data2 != null)
        {
          AllianceWar_MainDlg_RallyList.param.ownerName = string.Format("Lv{0} {1}", (object) data2.Level, (object) data2.LocalName);
          AllianceWar_MainDlg_RallyList.param.bossIcon = data2.ImagePath;
        }
      }
    }
    this.panel.belowList.Setup(AllianceWar_MainDlg_RallyList.param);
  }

  protected override void SetupTargetSlot(RallyData rallyData)
  {
    this.panel.targetSlot.Setup(rallyData.rallyId, 80f, true);
  }
}
