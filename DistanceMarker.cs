﻿// Decompiled with JetBrains decompiler
// Type: DistanceMarker
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DistanceMarker : MonoBehaviour
{
  public float m_Factor = 1f;
  public UIWidget m_Frame;
  public DistanceMarkerArrow[] m_Arrow;
  private Vector3 m_LocalPosition;

  public void SetPosition(DynamicMapData mapData, Vector3 position)
  {
    Coordinate cityLocation = PlayerData.inst.playerCityData.cityLocation;
    Vector3 pixelPosition = mapData.ConvertTileKXYToPixelPosition(cityLocation);
    position.z = 0.0f;
    pixelPosition.z = 0.0f;
    this.m_LocalPosition = this.m_Factor * (pixelPosition - position);
    Bounds bounds = this.m_Frame.CalculateBounds();
    bool flag = false;
    if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PVPMode && MapUtils.IsPitWorld(PlayerData.inst.CityData.Location.K))
      flag = true;
    if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PitMode && !MapUtils.IsPitWorld(PlayerData.inst.CityData.Location.K))
      flag = true;
    if (flag || bounds.Contains(this.m_LocalPosition))
    {
      this.m_Arrow[0].gameObject.SetActive(false);
      this.m_Arrow[1].gameObject.SetActive(false);
    }
    else
    {
      int index = DistanceMarker.SelectArrowIndex(this.m_LocalPosition);
      this.m_Arrow[index].gameObject.SetActive(true);
      this.m_Arrow[1 - index].gameObject.SetActive(false);
      Ray ray = new Ray(bounds.center, -this.m_LocalPosition);
      float distance = 0.0f;
      if (bounds.IntersectRay(ray, out distance))
      {
        Vector3 point = ray.GetPoint(distance);
        this.m_Arrow[index].transform.localPosition = point;
      }
      this.m_Arrow[index].transform.localRotation = Quaternion.LookRotation(this.m_LocalPosition, Vector3.back);
      this.m_Arrow[index].SetDistance(this.m_LocalPosition.magnitude);
    }
  }

  private static int SelectArrowIndex(Vector3 direction)
  {
    return (double) direction.x < 0.0 ? 1 : 0;
  }
}
