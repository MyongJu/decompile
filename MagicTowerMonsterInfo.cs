﻿// Decompiled with JetBrains decompiler
// Type: MagicTowerMonsterInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class MagicTowerMonsterInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "model")]
  public string Model;
  [Config(Name = "icon")]
  public string Icon;
  [Config(CustomParse = true, CustomType = typeof (UnitGroup), Name = "Units")]
  public UnitGroup Units;
  [Config(CustomParse = true, CustomType = typeof (Benefits), Name = "Benefits")]
  public Benefits Benefits;
}
