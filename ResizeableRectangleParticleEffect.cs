﻿// Decompiled with JetBrains decompiler
// Type: ResizeableRectangleParticleEffect
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ResizeableRectangleParticleEffect : MonoBehaviour
{
  public ParticleSystem left;
  public ParticleSystem right;
  public ParticleSystem top;
  public ParticleSystem bottom;

  public void Resize(int height, int width)
  {
    this.left.Stop();
    this.right.Stop();
    this.top.Stop();
    this.bottom.Stop();
    Utils.SetLPX(this.left.gameObject, (float) (-width / 2));
    Utils.SetLSX(this.left.gameObject, (float) (height / 2));
    Utils.SetLPX(this.right.gameObject, (float) (width / 2));
    Utils.SetLSX(this.right.gameObject, (float) (height / 2));
    Utils.SetLPY(this.top.gameObject, (float) (height / 2));
    Utils.SetLSX(this.top.gameObject, (float) (width / 2));
    Utils.SetLPY(this.bottom.gameObject, (float) (-height / 2));
    Utils.SetLSX(this.bottom.gameObject, (float) (width / 2));
    this.left.Play();
    this.right.Play();
    this.top.Play();
    this.bottom.Play();
  }
}
