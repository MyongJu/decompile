﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomManageBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using UI;
using UnityEngine;

public class ChatRoomManageBar : MonoBehaviour
{
  public static ChatRoomManageBar.Parameter param = new ChatRoomManageBar.Parameter();
  private ChatRoomManageBar.Data data = new ChatRoomManageBar.Data();
  [SerializeField]
  private ChatRoomManageBar.Panel panel;

  public void Setup(ChatRoomManageBar.Parameter param)
  {
    this.data.SetData(param);
    this.panel.playerName.text = this.data.member.userName;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.playerIcon, UserData.GetPortraitIconPath(this.data.member.portrait), (System.Action<bool>) null, true, false, string.Empty);
  }

  public void OnIgnoreButtonClick()
  {
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = ScriptLocalization.Get("id_uppercase_warning", true),
      content = ScriptLocalization.Get("chat_room_ignore_confirm_description", true),
      yes = ScriptLocalization.Get("id_uppercase_yes", true),
      no = ScriptLocalization.Get("id_uppercase_no", true),
      yesCallback = (System.Action) (() => MessageHub.inst.GetPortByAction("Chat:acceptOrDenyApply").SendRequest(Utils.Hash((object) "room_id", (object) this.data.member.roomId, (object) "be_uid", (object) this.data.member.uid, (object) "cmd", (object) "deny"), (System.Action<bool, object>) null, true)),
      noCallback = (System.Action) (() => {})
    });
  }

  public void OnAcceptButtonClick()
  {
    MessageHub.inst.GetPortByAction("Chat:acceptOrDenyApply").SendRequest(Utils.Hash((object) "room_id", (object) this.data.member.roomId, (object) "be_uid", (object) this.data.member.uid, (object) "cmd", (object) "accept"), (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      ChatRoomData chatRoomData = DBManager.inst.DB_room.Get(this.data.member.roomId);
      ChatMessageManager.SendChatMemberWelcomMessage(this.data.member.userName, chatRoomData.roomName, "chat_room_welcome_2_description", chatRoomData.channelId);
    }), true);
  }

  public void Reset()
  {
    this.panel.playerIcon = this.transform.Find("PlayerFrame/PlayerIcon").gameObject.GetComponent<UITexture>();
    this.panel.playerName = this.transform.Find("PlayerName").gameObject.GetComponent<UILabel>();
    this.panel.onlineState = this.transform.Find("OnlineStateIcon").gameObject.GetComponent<UISprite>();
    this.panel.BT_Ignore = this.transform.Find("Btn_Ignore").gameObject.GetComponent<UIButton>();
    this.panel.BT_Accept = this.transform.Find("Btn_Accept").gameObject.GetComponent<UIButton>();
  }

  public class Parameter
  {
    public ChatRoomMember member;
  }

  public class Data
  {
    public ChatRoomMember member;

    public void SetData(ChatRoomManageBar.Parameter param)
    {
      this.member = param.member;
    }
  }

  [Serializable]
  public class Panel
  {
    public UITexture playerIcon;
    public UILabel playerName;
    public UISprite onlineState;
    public UIButton BT_Ignore;
    public UIButton BT_Accept;
  }
}
