﻿// Decompiled with JetBrains decompiler
// Type: BenefitComparisonComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class BenefitComparisonComponent : ComponentRenderBase
{
  public UILabel title;
  public GameObject parent;
  public GameObject element;
  public GameObject container;
  public GameObject noneTips;
  protected List<BenefitComparisonElement> elementList;
  protected string titleId;

  public override void Init()
  {
    base.Init();
    BenefitComparisonComponentData data = this.data as BenefitComparisonComponentData;
    if (data == null)
      return;
    Dictionary<string, BenefitComparisonElement> comparisonDataDic = data.comparisonDataDic;
    if (comparisonDataDic == null || comparisonDataDic.Count == 0)
      return;
    this.titleId = data.title;
    this.elementList = new List<BenefitComparisonElement>();
    using (Dictionary<string, BenefitComparisonElement>.ValueCollection.Enumerator enumerator = comparisonDataDic.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.elementList.Add(enumerator.Current);
    }
    this.elementList.Sort((Comparison<BenefitComparisonElement>) ((x, y) => ConfigManager.inst.DB_Properties[x.ID].Priority.CompareTo(ConfigManager.inst.DB_Properties[y.ID].Priority)));
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.title.text = Utils.XLAT(this.titleId);
    if (BenefitComparisonHelper.IsSelfBenefitEmpty(this.elementList))
      this.UpdateUI_SelfEmpty();
    else if (BenefitComparisonHelper.IsOtherBenefitEmpty(this.elementList))
    {
      this.UpdateUI_OtherEmpty();
    }
    else
    {
      NGUITools.SetActive(this.noneTips, false);
      int index = 0;
      for (int count = this.elementList.Count; index < count; ++index)
      {
        BenefitComparisonElement element = this.elementList[index];
        GameObject go = NGUITools.AddChild(this.container, this.element);
        NGUITools.SetActive(go, true);
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[element.ID];
        GameObject gameObject1 = go.transform.Find("SelfItem").gameObject;
        NGUITools.SetActive(gameObject1, true);
        gameObject1.transform.Find("Benefit").GetComponent<UILabel>().text = dbProperty.Name;
        gameObject1.transform.Find("Value").GetComponent<UILabel>().text = dbProperty.ConvertToDisplayString((double) element.value1, (double) Mathf.Abs(element.value1) > 1.40129846432482E-45, true);
        GameObject gameObject2 = go.transform.Find("OtherItem").gameObject;
        NGUITools.SetActive(gameObject2, true);
        gameObject2.transform.Find("Benefit").GetComponent<UILabel>().text = dbProperty.Name;
        gameObject2.transform.Find("Value").GetComponent<UILabel>().text = dbProperty.ConvertToDisplayString((double) element.value2, (double) Mathf.Abs(element.value2) > 1.40129846432482E-45, true);
        if (index == count - 1)
        {
          NGUITools.SetActive(gameObject1.transform.Find("Line").gameObject, false);
          NGUITools.SetActive(gameObject2.transform.Find("Line").gameObject, false);
        }
      }
      this.container.GetComponent<TableContainer>().ResetPosition();
    }
  }

  private void UpdateUI_SelfEmpty()
  {
    NGUITools.SetActive(this.noneTips, true);
    NGUITools.SetActive(this.noneTips.transform.Find("self").gameObject, true);
    NGUITools.SetActive(this.noneTips.transform.Find("other").gameObject, false);
    int index = 0;
    for (int count = this.elementList.Count; index < count; ++index)
    {
      BenefitComparisonElement element = this.elementList[index];
      GameObject go = NGUITools.AddChild(this.container, this.element);
      NGUITools.SetActive(go, true);
      PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[element.ID];
      NGUITools.SetActive(go.transform.Find("SelfItem").gameObject, false);
      GameObject gameObject = go.transform.Find("OtherItem").gameObject;
      NGUITools.SetActive(gameObject, true);
      gameObject.transform.Find("Benefit").GetComponent<UILabel>().text = dbProperty.Name;
      gameObject.transform.Find("Value").GetComponent<UILabel>().text = dbProperty.ConvertToDisplayString((double) element.value2, (double) Mathf.Abs(element.value2) > 1.40129846432482E-45, true);
      if (index == count - 1)
        NGUITools.SetActive(gameObject.transform.Find("Line").gameObject, false);
    }
    this.container.GetComponent<TableContainer>().ResetPosition();
  }

  private void UpdateUI_OtherEmpty()
  {
    NGUITools.SetActive(this.noneTips, true);
    NGUITools.SetActive(this.noneTips.transform.Find("self").gameObject, false);
    NGUITools.SetActive(this.noneTips.transform.Find("other").gameObject, true);
    int index = 0;
    for (int count = this.elementList.Count; index < count; ++index)
    {
      BenefitComparisonElement element = this.elementList[index];
      GameObject go = NGUITools.AddChild(this.container, this.element);
      NGUITools.SetActive(go, true);
      PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[element.ID];
      GameObject gameObject = go.transform.Find("SelfItem").gameObject;
      NGUITools.SetActive(gameObject, true);
      gameObject.transform.Find("Benefit").GetComponent<UILabel>().text = dbProperty.Name;
      gameObject.transform.Find("Value").GetComponent<UILabel>().text = dbProperty.ConvertToDisplayString((double) element.value1, (double) Mathf.Abs(element.value1) > 1.40129846432482E-45, true);
      NGUITools.SetActive(go.transform.Find("OtherItem").gameObject, false);
      if (index == count - 1)
        NGUITools.SetActive(gameObject.transform.Find("Line").gameObject, false);
    }
    this.container.GetComponent<TableContainer>().ResetPosition();
  }
}
