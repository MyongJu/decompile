﻿// Decompiled with JetBrains decompiler
// Type: ChatMuteBtn
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class ChatMuteBtn : MonoBehaviour
{
  public int time;

  private void Awake()
  {
    UILabel componentInChildren = this.GetComponentInChildren<UILabel>();
    if (!((Object) componentInChildren != (Object) null))
      return;
    componentInChildren.text = ScriptLocalization.GetWithMultyContents("chat_uppercase_mute_time_button", true, this.time.ToString());
  }
}
