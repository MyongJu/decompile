﻿// Decompiled with JetBrains decompiler
// Type: EquipmentForgeDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class EquipmentForgeDlg : UI.Dialog
{
  private int index = -1;
  public EquipmentForgeTabBar tabBar;
  public EquipmentForgeContent content;
  public GameObject empty;
  private List<ConfigEquipmentScrollInfo> currentList;
  private int currentSelectInfoID;
  private BagType bagType;

  private void Init()
  {
    this.tabBar.SelectedIndex = this.index == -1 ? 0 : this.index;
    this.HandleTabSelected(this.tabBar.SelectedIndex);
    this.AddEventHandler();
  }

  public void Dispose()
  {
    this.RemoveEventHandler();
    this.content.Dispose();
  }

  private void HandleTabSelected(int index)
  {
    if (this.tabBar.SelectedIndex == 0)
    {
      List<ConsumableItemData> consumableScrollItems = ItemBag.Instance.GetConsumableScrollItems(this.bagType);
      this.currentList = new List<ConfigEquipmentScrollInfo>();
      for (int index1 = 0; index1 < consumableScrollItems.Count; ++index1)
        this.currentList.Add(ConfigManager.inst.GetEquipmentScroll(this.bagType).GetDataByItemID(consumableScrollItems[index1].internalId));
      this.Sort(this.currentList);
    }
    else
    {
      this.currentList = ConfigManager.inst.GetEquipmentScroll(this.bagType).GetScrollListByEquipmentType(HeroItemUtils.GetItemTypeByIndex(this.tabBar.SelectedIndex, this.bagType), this.bagType);
      this.Sort(this.currentList);
    }
    this.empty.SetActive(this.currentList.Count == 0);
    this.content.Init(this.bagType, this.currentList, this.currentSelectInfoID == 0, this.currentSelectInfoID);
    this.index = index;
  }

  private void AddEventHandler()
  {
    this.tabBar.OnSelectedHandler = new System.Action<int>(this.HandleTabSelected);
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnConsumableCountChanged);
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnConsumableCountChanged);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnConsumableCountChanged);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnConsumableCountChanged);
    DBManager.inst.DB_User.onDataChanged -= new System.Action<long>(this.OnUserDataChanged);
    DBManager.inst.DB_User.onDataChanged += new System.Action<long>(this.OnUserDataChanged);
    if (this.bagType == BagType.Hero)
    {
      DBManager.inst.DB_hero.onDataChanged -= new System.Action<long>(this.OnHeroDataChanged);
      DBManager.inst.DB_hero.onDataChanged += new System.Action<long>(this.OnHeroDataChanged);
    }
    else
    {
      DBManager.inst.DB_DragonKnight.onDataChanged -= new System.Action<DragonKnightData>(this.OnDragonKinightDataChanged);
      DBManager.inst.DB_DragonKnight.onDataChanged += new System.Action<DragonKnightData>(this.OnDragonKinightDataChanged);
    }
  }

  private void RemoveEventHandler()
  {
    this.tabBar.OnSelectedHandler = (System.Action<int>) null;
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnConsumableCountChanged);
    DBManager.inst.DB_User.onDataChanged -= new System.Action<long>(this.OnUserDataChanged);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnConsumableCountChanged);
    if (this.bagType == BagType.Hero)
      DBManager.inst.DB_hero.onDataChanged -= new System.Action<long>(this.OnHeroDataChanged);
    else
      DBManager.inst.DB_DragonKnight.onDataChanged -= new System.Action<DragonKnightData>(this.OnDragonKinightDataChanged);
  }

  private void OnHeroDataChanged(long uid)
  {
    this.OnUserDataChanged(uid);
  }

  private void OnDragonKinightDataChanged(DragonKnightData dragonKnightData)
  {
    this.OnUserDataChanged(dragonKnightData.UserId);
  }

  private void OnUserDataChanged(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    if (this.tabBar.SelectedIndex == 0)
    {
      List<ConsumableItemData> consumableScrollItems = ItemBag.Instance.GetConsumableScrollItems(this.bagType);
      this.currentList = new List<ConfigEquipmentScrollInfo>();
      for (int index = 0; index < consumableScrollItems.Count; ++index)
        this.currentList.Add(ConfigManager.inst.GetEquipmentScroll(this.bagType).GetDataByItemID(consumableScrollItems[index].internalId));
    }
    else
      this.currentList = ConfigManager.inst.GetEquipmentScroll(this.bagType).GetScrollListByEquipmentType(HeroItemUtils.GetItemTypeByIndex(this.tabBar.SelectedIndex, this.bagType), this.bagType);
    this.Sort(this.currentList);
    this.content.Init(this.bagType, this.currentList, false, 0);
  }

  private void OnConsumableCountChanged(int itemId)
  {
    if (this.tabBar.SelectedIndex == 0)
    {
      List<ConsumableItemData> consumableScrollItems = ItemBag.Instance.GetConsumableScrollItems(this.bagType);
      this.currentList = new List<ConfigEquipmentScrollInfo>();
      for (int index = 0; index < consumableScrollItems.Count; ++index)
        this.currentList.Add(ConfigManager.inst.GetEquipmentScroll(this.bagType).GetDataByItemID(consumableScrollItems[index].internalId));
      this.Sort(this.currentList);
    }
    else
      this.currentList = ConfigManager.inst.GetEquipmentScroll(this.bagType).GetScrollListByEquipmentType(HeroItemUtils.GetItemTypeByIndex(this.tabBar.SelectedIndex, this.bagType), this.bagType);
    this.content.Init(this.bagType, this.currentList, false, 0);
  }

  private void Sort(List<ConfigEquipmentScrollInfo> list)
  {
    list.Sort((Comparison<ConfigEquipmentScrollInfo>) ((x, y) =>
    {
      ConfigEquipmentMainInfo data1 = ConfigManager.inst.GetEquipmentMain(this.bagType).GetData(x.equipmenID);
      ConfigEquipmentMainInfo data2 = ConfigManager.inst.GetEquipmentMain(this.bagType).GetData(y.equipmenID);
      if (data1.type != data2.type)
        return data1.type.CompareTo(data2.type);
      if (data1.quality != data2.quality)
        return data2.quality.CompareTo(data1.quality);
      if (data1.heroLevelMin == data2.heroLevelMin)
        return data1.tendency.CompareTo(data2.tendency);
      return data2.heroLevelMin.CompareTo(data1.heroLevelMin);
    }));
  }

  public void OnGotoClick()
  {
    UIManager.inst.OpenDlg(this.bagType != BagType.Hero ? "Equipment/DragonKnightEquipmentTreasuryDialog" : "Equipment/EquipmentTreasuryDlg", (UI.Dialog.DialogParameter) new EquipmentTreasuryDlg.Parameter()
    {
      bagType = this.bagType
    }, true, true, true);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (orgParam != null)
    {
      this.currentSelectInfoID = (orgParam as EquipmentForgeDlg.Parameter).currentSelectInfoID;
      this.index = (orgParam as EquipmentForgeDlg.Parameter).index;
      this.bagType = (orgParam as EquipmentForgeDlg.Parameter).bagType;
    }
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    if ((UnityEngine.Object) this.content != (UnityEngine.Object) null)
      this.currentSelectInfoID = this.content.currentSelectInfoID;
    this.Dispose();
    base.OnHide(orgParam);
  }

  private enum SORT_TYPE
  {
    TYPE0,
    TYPE1,
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int index = -1;
    public int currentSelectInfoID;
    public BagType bagType;
  }
}
