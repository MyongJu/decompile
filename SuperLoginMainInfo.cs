﻿// Decompiled with JetBrains decompiler
// Type: SuperLoginMainInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class SuperLoginMainInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "group")]
  public int group;
  [Config(Name = "day")]
  public int day;
  [Config(Name = "reward_icon")]
  public string rewardIcon;
  [Config(Name = "description")]
  public string description;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "Rewards")]
  public Reward Rewards;

  public string Description
  {
    get
    {
      return Utils.XLAT(this.description);
    }
  }

  public string RewardIconPath
  {
    get
    {
      return "Texture/ItemIcons/" + this.rewardIcon;
    }
  }

  public Item2RewardInfo.Data FirstRewardData
  {
    get
    {
      Item2RewardInfo.Data data = new Item2RewardInfo.Data();
      if (this.Rewards != null)
      {
        Dictionary<string, int>.Enumerator enumerator = this.Rewards.rewards.GetEnumerator();
        while (enumerator.MoveNext())
        {
          int result = 0;
          int.TryParse(enumerator.Current.Key, out result);
          if (result > 0)
          {
            data.itemid = result;
            data.count = (float) enumerator.Current.Value;
            break;
          }
        }
      }
      return data;
    }
  }

  public string Group
  {
    get
    {
      SuperLoginGroupInfo superLoginGroupInfo = ConfigManager.inst.DB_SuperLoginGroup.Get(this.group);
      if (superLoginGroupInfo == null)
        return string.Empty;
      return superLoginGroupInfo.id;
    }
  }

  public SuperLoginPayload.DailyRewardState RewardState
  {
    get
    {
      if (SuperLoginPayload.Instance.SuperLoginDataDict.ContainsKey(this.Group))
      {
        SuperLoginBaseData superLoginBaseData = SuperLoginPayload.Instance.SuperLoginDataDict[this.Group];
        if (superLoginBaseData != null)
        {
          if (superLoginBaseData.RewardStateDict.ContainsKey(this.day))
            return (SuperLoginPayload.DailyRewardState) superLoginBaseData.RewardStateDict[this.day];
          if (NetServerTime.inst.ServerTimestamp >= superLoginBaseData.StartTime + this.day * 24 * 3600)
            return SuperLoginPayload.DailyRewardState.EXPIRED;
        }
      }
      return SuperLoginPayload.DailyRewardState.CANT_GET;
    }
  }
}
