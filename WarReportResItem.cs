﻿// Decompiled with JetBrains decompiler
// Type: WarReportResItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WarReportResItem : MonoBehaviour
{
  private const string defaultItem = "helmet";
  public UISprite spriteIcon;
  public UILabel labelCount;
  private Hero_EquipmentData _item;

  public void SetItem(string type, string count)
  {
    this.labelCount.text = Utils.FormatShortThousands(int.Parse(count));
    this._item = ConfigManager.inst.DB_Hero_Equipment.GetData(int.Parse(type));
    this.spriteIcon.spriteName = string.IsNullOrEmpty(this._item.Asset_Filename) ? "helmet" : this._item.Asset_Filename;
  }

  public void OnPress(bool isDown)
  {
    if (isDown)
      this.ShowTooltip();
    else
      this.HideTooltip();
  }

  public void OnHover(bool isOver)
  {
    if (isOver)
      this.ShowTooltip();
    else
      this.HideTooltip();
  }

  private void ShowTooltip()
  {
  }

  private void HideTooltip()
  {
  }
}
