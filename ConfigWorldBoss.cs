﻿// Decompiled with JetBrains decompiler
// Type: ConfigWorldBoss
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigWorldBoss
{
  private Dictionary<string, WorldBossStaticInfo> m_WorldBossByUniqueId;
  private Dictionary<int, WorldBossStaticInfo> m_WorldBossByInternalId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<WorldBossStaticInfo, string>(res as Hashtable, "ID", out this.m_WorldBossByUniqueId, out this.m_WorldBossByInternalId);
  }

  public WorldBossStaticInfo GetData(int internalId)
  {
    WorldBossStaticInfo worldBossStaticInfo = (WorldBossStaticInfo) null;
    this.m_WorldBossByInternalId.TryGetValue(internalId, out worldBossStaticInfo);
    return worldBossStaticInfo;
  }

  public WorldBossStaticInfo GetData(string id)
  {
    WorldBossStaticInfo worldBossStaticInfo = (WorldBossStaticInfo) null;
    this.m_WorldBossByUniqueId.TryGetValue(id, out worldBossStaticInfo);
    return worldBossStaticInfo;
  }

  public void Clear()
  {
    if (this.m_WorldBossByUniqueId != null)
    {
      this.m_WorldBossByUniqueId.Clear();
      this.m_WorldBossByUniqueId = (Dictionary<string, WorldBossStaticInfo>) null;
    }
    if (this.m_WorldBossByInternalId == null)
      return;
    this.m_WorldBossByInternalId.Clear();
    this.m_WorldBossByInternalId = (Dictionary<int, WorldBossStaticInfo>) null;
  }
}
