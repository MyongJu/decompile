﻿// Decompiled with JetBrains decompiler
// Type: ConfigParliamentHeroQuality
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigParliamentHeroQuality
{
  private List<ParliamentHeroQualityInfo> _parliamentHeroQualityInfoList = new List<ParliamentHeroQualityInfo>();
  private Dictionary<string, ParliamentHeroQualityInfo> _datas;
  private Dictionary<int, ParliamentHeroQualityInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ParliamentHeroQualityInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, ParliamentHeroQualityInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._parliamentHeroQualityInfoList.Add(enumerator.Current);
    }
    this._parliamentHeroQualityInfoList.Sort((Comparison<ParliamentHeroQualityInfo>) ((a, b) => int.Parse(a.id).CompareTo(int.Parse(b.id))));
  }

  public List<ParliamentHeroQualityInfo> GetParliamentHeroQualityInfoList()
  {
    return this._parliamentHeroQualityInfoList;
  }

  public ParliamentHeroQualityInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (ParliamentHeroQualityInfo) null;
  }

  public ParliamentHeroQualityInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (ParliamentHeroQualityInfo) null;
  }
}
