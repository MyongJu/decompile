﻿// Decompiled with JetBrains decompiler
// Type: CommandTree
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Text.RegularExpressions;

internal class CommandTree
{
  private static string[] emptyArgs = new string[0];
  private Dictionary<string, CommandTree> m_subcommands;
  private Console.CommandCallback m_command;
  private bool m_runOnMainThread;

  public CommandTree()
  {
    this.m_subcommands = new Dictionary<string, CommandTree>();
  }

  public void Add(string str, Console.CommandCallback cmd, bool runOnMainThread)
  {
    this._add(str.ToLower().Split(' '), 0, cmd, runOnMainThread);
  }

  private void _add(string[] commands, int command_index, Console.CommandCallback cmd, bool runOnMainThread)
  {
    if (commands.Length == command_index)
    {
      this.m_runOnMainThread = runOnMainThread;
      this.m_command = cmd;
    }
    else
    {
      string command = commands[command_index];
      if (!this.m_subcommands.ContainsKey(command))
        this.m_subcommands[command] = new CommandTree();
      this.m_subcommands[command]._add(commands, command_index + 1, cmd, runOnMainThread);
    }
  }

  public string Complete(string partialCommand)
  {
    return this._complete(partialCommand.Split(' '), 0, string.Empty);
  }

  public string _complete(string[] partialCommand, int index, string result)
  {
    if (partialCommand.Length == index && this.m_command != null)
      return result;
    if (partialCommand.Length == index)
    {
      Console.LogCommand(result);
      using (Dictionary<string, CommandTree>.KeyCollection.Enumerator enumerator = this.m_subcommands.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          Console.Log(result + " " + current);
        }
      }
      return result + " ";
    }
    if (partialCommand.Length == index + 1)
    {
      string key = partialCommand[index];
      if (this.m_subcommands.ContainsKey(key))
      {
        result += key;
        return this.m_subcommands[key]._complete(partialCommand, index + 1, result);
      }
      List<string> stringList = new List<string>();
      using (Dictionary<string, CommandTree>.KeyCollection.Enumerator enumerator = this.m_subcommands.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          if (current.StartsWith(key))
            stringList.Add(current);
        }
      }
      if (stringList.Count == 1)
        return result + stringList[0] + " ";
      if (stringList.Count > 1)
      {
        Console.LogCommand(result + key);
        using (List<string>.Enumerator enumerator = stringList.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            string current = enumerator.Current;
            Console.Log(result + current);
          }
        }
      }
      return result + key;
    }
    string key1 = partialCommand[index];
    if (!this.m_subcommands.ContainsKey(key1))
      return result;
    result = result + key1 + " ";
    return this.m_subcommands[key1]._complete(partialCommand, index + 1, result);
  }

  public void Run(string commandStr)
  {
    MatchCollection matchCollection = new Regex("\".*?\"|[^\\s]+").Matches(commandStr);
    string[] commands = new string[matchCollection.Count];
    for (int index = 0; index < commands.Length; ++index)
      commands[index] = matchCollection[index].Value.Replace("\"", string.Empty);
    this._run(commands, 0);
  }

  private void _run(string[] commands, int index)
  {
    if (commands.Length == index)
    {
      this.RunCommand(CommandTree.emptyArgs);
    }
    else
    {
      string lower = commands[index].ToLower();
      if (!this.m_subcommands.ContainsKey(lower))
      {
        int index1 = 0;
        string[] args = new string[commands.Length - 1];
        for (int index2 = 0; index2 < commands.Length; ++index2)
        {
          if (index2 != index)
          {
            args[index1] = commands[index2];
            ++index1;
          }
        }
        this.RunCommand(args);
      }
      else
        this.m_subcommands[lower]._run(commands, index + 1);
    }
  }

  private void RunCommand(string[] args)
  {
    if (this.m_command == null)
      Console.Log("command not found");
    else if (this.m_runOnMainThread)
      Console.Queue(this.m_command, args);
    else
      this.m_command(args);
  }
}
