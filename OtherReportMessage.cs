﻿// Decompiled with JetBrains decompiler
// Type: OtherReportMessage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class OtherReportMessage : AbstractMailEntry
{
  public string content
  {
    get
    {
      if (this.body == null)
        return "Loading...";
      return Utils.XLAT(this.body[(object) "text"] as string);
    }
    set
    {
      if (!this.body.ContainsKey((object) "text"))
        this.body.Add((object) "text", (object) string.Empty);
      this.body[(object) "text"] = (object) value;
      if (this.mailUpdated == null)
        return;
      this.mailUpdated();
    }
  }

  public override string ToString()
  {
    return base.ToString() + "content=" + this.content;
  }

  public override void Display()
  {
    base.Display();
  }
}
