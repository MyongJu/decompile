﻿// Decompiled with JetBrains decompiler
// Type: BoostManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;

public class BoostManager
{
  private static BoostManager _instance;

  public static BoostManager Instance
  {
    get
    {
      if (BoostManager._instance == null)
        BoostManager._instance = new BoostManager();
      return BoostManager._instance;
    }
  }

  public List<BoostStaticInfo> GetBoostStaticInfoByCategory(BoostCategory category)
  {
    List<BoostStaticInfo> boostStaticInfoList = new List<BoostStaticInfo>();
    using (Dictionary<int, BoostStaticInfo>.ValueCollection.Enumerator enumerator = ConfigManager.inst.DB_Boosts.Item2Boosts.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BoostStaticInfo current = enumerator.Current;
        if (current.Category == category && current.OnSale)
          boostStaticInfoList.Add(current);
      }
    }
    return boostStaticInfoList;
  }

  public bool HadPeaceShield()
  {
    return PlayerData.inst.playerCityData.peaceShieldJobId != 0L;
  }

  public void ShowPeaceShieldWarning(System.Action callBack)
  {
    if (PlayerData.inst.playerCityData.peaceShieldJobId != 0L)
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("break_shield_add_cd");
      Dictionary<string, string> para = new Dictionary<string, string>()
      {
        {
          "0",
          (data == null ? 0 : data.ValueInt / 60).ToString()
        }
      };
      UIManager.inst.OpenPopup("ConfirmWithPeaceShield", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
      {
        setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
        title = ScriptLocalization.Get("id_uppercase_confirm", true),
        description = ScriptLocalization.GetWithPara("peace_shield_break_cooldown_warning", para, true),
        leftButtonText = ScriptLocalization.Get("id_uppercase_no", true),
        rightButtonText = ScriptLocalization.Get("id_uppercase_yes", true),
        leftButtonClickEvent = (System.Action) null,
        rightButtonClickEvent = (System.Action) (() =>
        {
          if (callBack == null)
            return;
          callBack();
        })
      });
    }
    else
    {
      if (callBack == null)
        return;
      callBack();
    }
  }

  public bool CheckBoostIsStart(int item_internalId)
  {
    return false;
  }

  public float GetProgreeValue(int item_internalId)
  {
    return 0.0f;
  }

  public int GetBoostRemainTime(int item_internalId)
  {
    return 0;
  }
}
