﻿// Decompiled with JetBrains decompiler
// Type: ConfigAnniversaryChampionMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigAnniversaryChampionMain
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, AnniversaryChampionMainInfo> datas;
  private Dictionary<int, AnniversaryChampionMainInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<AnniversaryChampionMainInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public List<AnniversaryChampionMainInfo> GetAllData()
  {
    List<AnniversaryChampionMainInfo> championMainInfoList = new List<AnniversaryChampionMainInfo>();
    Dictionary<int, AnniversaryChampionMainInfo>.ValueCollection.Enumerator enumerator = this.dicByUniqueId.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null)
        championMainInfoList.Add(enumerator.Current);
    }
    championMainInfoList.Sort(new Comparison<AnniversaryChampionMainInfo>(this.Compare));
    return championMainInfoList;
  }

  private int Compare(AnniversaryChampionMainInfo x, AnniversaryChampionMainInfo y)
  {
    return x.internalId.CompareTo(y.internalId);
  }

  public AnniversaryChampionMainInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (AnniversaryChampionMainInfo) null;
  }

  public AnniversaryChampionMainInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AnniversaryChampionMainInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
      this.datas.Clear();
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
  }
}
