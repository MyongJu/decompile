﻿// Decompiled with JetBrains decompiler
// Type: SkillEffectManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class SkillEffectManager
{
  private Dictionary<long, SkillEffectPlayer> _playerDict = new Dictionary<long, SkillEffectPlayer>();
  private Queue<long> _deleteQueue = new Queue<long>();
  private static SkillEffectManager _instance;

  public static SkillEffectManager Instance
  {
    get
    {
      if (SkillEffectManager._instance == null)
        SkillEffectManager._instance = new SkillEffectManager();
      return SkillEffectManager._instance;
    }
  }

  public void Startup()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnUpdate);
  }

  public void Terminate()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnUpdate);
    this.RemoveAll();
  }

  private void OnUpdate(int delta)
  {
    Dictionary<long, SkillEffectPlayer>.ValueCollection.Enumerator enumerator = this._playerDict.Values.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Update();
    while (true)
      this._playerDict.Remove(this._deleteQueue.Dequeue());
  }

  public SkillEffectPlayer Get(long playerId)
  {
    SkillEffectPlayer skillEffectPlayer = (SkillEffectPlayer) null;
    if (this._playerDict.TryGetValue(playerId, out skillEffectPlayer))
      return skillEffectPlayer;
    return (SkillEffectPlayer) null;
  }

  public void Add(SkillEffectPlayer player)
  {
    if (player == null)
      return;
    this._playerDict.Add(player.Guid, player);
  }

  public void Remove(SkillEffectPlayer player)
  {
    if (player == null)
      return;
    this._deleteQueue.Enqueue(player.Guid);
  }

  public void StopAll(bool clearAllVfx = false)
  {
    Dictionary<long, SkillEffectPlayer>.ValueCollection.Enumerator enumerator = this._playerDict.Values.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Stop(clearAllVfx);
    this.RemoveAll();
  }

  public void RemoveAll()
  {
    this._deleteQueue.Clear();
    this._playerDict.Clear();
  }
}
