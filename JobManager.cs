﻿// Decompiled with JetBrains decompiler
// Type: JobManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class JobManager
{
  private Dictionary<JobEvent, Dictionary<long, JobHandle>> m_JobDictByClass = new Dictionary<JobEvent, Dictionary<long, JobHandle>>();
  private Dictionary<long, JobHandle> m_JobDictByID = new Dictionary<long, JobHandle>(32);
  private BetterList<JobHandle> m_JobLoop = new BetterList<JobHandle>();
  private ObjectPool<Job> m_JobPool = new ObjectPool<Job>();
  private Dictionary<long, long> m_JobHistoryDict = new Dictionary<long, long>();
  public const long InvalidID = 0;
  public const long FakeJobId = -1;
  private static JobManager m_Instance;

  private JobManager()
  {
  }

  public event System.Action<long> OnJobCreated;

  public event System.Action<long> OnJobRemove;

  public event System.Action<long, JobError> OnJobError;

  public static JobManager Instance
  {
    get
    {
      if (JobManager.m_Instance == null)
      {
        JobManager.m_Instance = new JobManager();
        Oscillator.Instance.secondEvent += new System.Action<int>(JobManager.m_Instance.OnSecond);
      }
      return JobManager.m_Instance;
    }
  }

  private void OnSecond(int timestamp)
  {
    this.m_JobLoop.Clear();
    Dictionary<long, JobHandle>.Enumerator enumerator = this.m_JobDictByID.GetEnumerator();
    while (enumerator.MoveNext())
      this.m_JobLoop.Add(enumerator.Current.Value);
    int size = this.m_JobLoop.size;
    for (int index = 0; index < size; ++index)
    {
      JobHandle jobHandle = this.m_JobLoop[index];
      if (!jobHandle.IsFinished())
        jobHandle.Update(timestamp);
      else if (jobHandle.OnFinished != null)
      {
        jobHandle.OnFinished();
        jobHandle.OnFinished = (System.Action) null;
      }
    }
  }

  public void UpdateServerJobData(object data, long updateTime)
  {
    ArrayList arrayList = data as ArrayList;
    if (arrayList == null)
      return;
    int count = arrayList.Count;
    for (int index = 0; index < count; ++index)
    {
      Hashtable hashtable = arrayList[index] as Hashtable;
      if (hashtable != null)
      {
        long result1 = 0;
        long result2 = 0;
        long jobId = 0;
        int result3 = 0;
        int result4 = 0;
        int result5 = 0;
        if (hashtable.ContainsKey((object) "job_id"))
          long.TryParse(hashtable[(object) "job_id"].ToString(), out result2);
        if (hashtable.ContainsKey((object) "uid"))
          long.TryParse(hashtable[(object) "uid"].ToString(), out result1);
        if (hashtable.ContainsKey((object) "c_job_id"))
          long.TryParse(hashtable[(object) "c_job_id"].ToString(), out jobId);
        if (hashtable.Contains((object) "time_start"))
          int.TryParse(hashtable[(object) "time_start"].ToString(), out result3);
        if (hashtable.Contains((object) "time_end"))
          int.TryParse(hashtable[(object) "time_end"].ToString(), out result4);
        if (hashtable.ContainsKey((object) "event_type"))
          int.TryParse(hashtable[(object) "event_type"].ToString(), out result5);
        object data1 = hashtable[(object) "trace"];
        if (jobId != 0L || this.GetClientJobIDByServerJobID(result2, out jobId))
        {
          JobHandle job = this.GetJob(jobId);
          if (job != null)
          {
            job.ServerJobID = result2;
            job.Reset(result3, result4);
            if (data1 != null)
              job.Data = data1;
          }
        }
        else
        {
          JobHandle job = this.GetJob(result2);
          if (job != null)
          {
            job.Reset(result3, result4);
            if (data1 != null)
              job.Data = data1;
          }
          else
            this.InternalCreateJob((JobEvent) result5, result1, result2, result3, result4, data1, (System.Action) null);
        }
      }
    }
  }

  public void RemoveServerJobData(object data)
  {
    ArrayList arrayList = data as ArrayList;
    if (arrayList == null)
      return;
    int count = arrayList.Count;
    for (int index = 0; index < count; ++index)
    {
      Hashtable hashtable = arrayList[index] as Hashtable;
      if (hashtable != null)
      {
        long result1 = 0;
        long clientJobId = 0;
        long result2 = 0;
        long.TryParse(hashtable[(object) "job_id"] as string, out result1);
        long.TryParse(hashtable[(object) "uid"] as string, out result2);
        if (result1 != 0L)
        {
          if (this.m_JobDictByID.ContainsKey(result1))
            this.RemoveJob(result1);
          else if (this.GetClientJobIDByServerJobID(result1, out clientJobId))
            this.RemoveJob(clientJobId);
          else if (!this.m_JobHistoryDict.ContainsKey(result1) && result2 == PlayerData.inst.uid)
            this.FireError(result1, JobError.RemoveNonExistJob);
        }
      }
    }
  }

  private void FireError(long serverJobId, JobError error)
  {
    if (this.OnJobError == null)
      return;
    this.OnJobError(serverJobId, error);
  }

  public long CreateClientJob(JobEvent evt, int startTime, int endTime, object data, System.Action finish)
  {
    long longId;
    do
    {
      longId = ClientIdCreater.CreateLongId(ClientIdCreater.IdType.EVENT);
    }
    while (this.m_JobDictByID.ContainsKey(longId));
    this.InternalCreateJob(evt, PlayerData.inst.uid, longId, startTime, endTime, data, finish).weakPtr.ByClient = true;
    return longId;
  }

  public long CreateClientJob(JobEvent evt, int duration, object data, System.Action finish)
  {
    int serverTimestamp = NetServerTime.inst.ServerTimestamp;
    return this.CreateClientJob(evt, serverTimestamp, serverTimestamp + duration, data, finish);
  }

  private JobHandle InternalCreateJob(JobEvent evt, long userId, long jobId, int startTime, int endTime, object data, System.Action finish)
  {
    Job job = this.m_JobPool.Allocate();
    job.uid = userId;
    job.Reset(evt, startTime, endTime, finish, jobId);
    JobHandle jobHandle = new JobHandle();
    jobHandle.weakPtr = job;
    jobHandle.Data = data;
    this.m_JobDictByID.Add(jobId, jobHandle);
    this.AddToClassDict(jobId, jobHandle);
    if (this.OnJobCreated != null)
      this.OnJobCreated(jobId);
    if (!this.m_JobHistoryDict.ContainsKey(jobId))
      this.m_JobHistoryDict.Add(jobId, jobId);
    return jobHandle;
  }

  private void AddToClassDict(long jobId, JobHandle jobHandle)
  {
    Dictionary<long, JobHandle> dictionary = (Dictionary<long, JobHandle>) null;
    this.m_JobDictByClass.TryGetValue(jobHandle.GetJobEvent(), out dictionary);
    if (dictionary == null)
    {
      dictionary = new Dictionary<long, JobHandle>();
      this.m_JobDictByClass.Add(jobHandle.GetJobEvent(), dictionary);
    }
    if (dictionary.ContainsKey(jobId))
      return;
    dictionary.Add(jobId, jobHandle);
  }

  private void RemoveFromClassDict(JobEvent jobEvent, long jobId)
  {
    Dictionary<long, JobHandle> dictionary = (Dictionary<long, JobHandle>) null;
    if (!this.m_JobDictByClass.TryGetValue(jobEvent, out dictionary))
      return;
    dictionary.Remove(jobId);
  }

  private bool GetClientJobIDByServerJobID(long serverJobId, out long clientJobId)
  {
    using (Dictionary<long, JobHandle>.Enumerator enumerator = this.m_JobDictByID.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<long, JobHandle> current = enumerator.Current;
        if (current.Value.ServerJobID == serverJobId)
        {
          clientJobId = current.Key;
          return true;
        }
      }
    }
    clientJobId = 0L;
    return false;
  }

  public void RemoveJob(long jobId)
  {
    if (!this.m_JobDictByID.ContainsKey(jobId))
      return;
    if (this.OnJobRemove != null)
      this.OnJobRemove(jobId);
    JobHandle jobHandle = this.m_JobDictByID[jobId];
    jobHandle.weakPtr.Dispose();
    this.m_JobDictByID.Remove(jobId);
    this.RemoveFromClassDict(jobHandle.GetJobEvent(), jobId);
    this.m_JobPool.Release(jobHandle.weakPtr);
    jobHandle.weakPtr = (Job) null;
  }

  public void Dispose()
  {
    this.OnJobCreated = (System.Action<long>) null;
    this.OnJobRemove = (System.Action<long>) null;
    this.m_JobDictByClass.Clear();
    this.m_JobDictByID.Clear();
    this.m_JobPool.Clear();
    this.m_JobLoop.Clear();
    this.m_JobHistoryDict.Clear();
  }

  public JobHandle GetJob(long jobId)
  {
    JobHandle jobHandle = (JobHandle) null;
    this.m_JobDictByID.TryGetValue(jobId, out jobHandle);
    return jobHandle;
  }

  public bool HasServerFinishedJob
  {
    get
    {
      Dictionary<long, JobHandle>.Enumerator enumerator = this.m_JobDictByID.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (!enumerator.Current.Value.IsClientJob && enumerator.Current.Value.IsFinished())
          return true;
      }
      return false;
    }
  }

  public JobHandle GetJob(JobEvent jobEvent, long jobId)
  {
    Dictionary<long, JobHandle> jobDictByClass = this.GetJobDictByClass(jobEvent);
    if (jobDictByClass == null)
      return (JobHandle) null;
    JobHandle jobHandle = (JobHandle) null;
    jobDictByClass.TryGetValue(jobId, out jobHandle);
    return jobHandle;
  }

  public JobHandle GetUnfinishedJob(long jobId)
  {
    JobHandle job = this.GetJob(jobId);
    if (job != null && !job.IsFinished())
      return job;
    return (JobHandle) null;
  }

  public Dictionary<long, JobHandle> GetJobDict()
  {
    return this.m_JobDictByID;
  }

  public Dictionary<long, JobHandle> GetJobDictByClass(JobEvent jobEvent)
  {
    Dictionary<long, JobHandle> dictionary = (Dictionary<long, JobHandle>) null;
    this.m_JobDictByClass.TryGetValue(jobEvent, out dictionary);
    return dictionary;
  }

  public JobHandle GetSingleJobByClass(JobEvent jobEvent)
  {
    Dictionary<long, JobHandle> jobDictByClass = this.GetJobDictByClass(jobEvent);
    if (jobDictByClass != null)
    {
      using (Dictionary<long, JobHandle>.ValueCollection.Enumerator enumerator = jobDictByClass.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          JobHandle current = enumerator.Current;
          if (!current.IsFinished() && PlayerData.inst.uid == current.weakPtr.uid)
            return current;
        }
      }
    }
    return (JobHandle) null;
  }

  public JobHandle GetAllUsersSingleJobByClass(JobEvent jobEvent)
  {
    Dictionary<long, JobHandle> jobDictByClass = this.GetJobDictByClass(jobEvent);
    if (jobDictByClass != null)
    {
      using (Dictionary<long, JobHandle>.ValueCollection.Enumerator enumerator = jobDictByClass.Values.GetEnumerator())
      {
        if (enumerator.MoveNext())
          return enumerator.Current;
      }
    }
    return (JobHandle) null;
  }
}
