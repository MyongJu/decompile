﻿// Decompiled with JetBrains decompiler
// Type: EquipmentGemSlotItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using GemConstant;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class EquipmentGemSlotItem : MonoBehaviour
{
  private List<EquipmentGemDetailItem> _allEquipmentGemDetail = new List<EquipmentGemDetailItem>();
  [SerializeField]
  private UITable _rootContainer;
  [SerializeField]
  private UITable _benefitContainer;
  [SerializeField]
  private EquipmentGemDetailItem _equipmentGemDetailTemplate;
  [SerializeField]
  private GameObject _rootHaveNoGem;
  [SerializeField]
  private GameObject _rootInteractableTag;
  [SerializeField]
  private GameObject _rootHaveGem;
  [SerializeField]
  private UISprite _emptyGemSlotIcon;
  [SerializeField]
  private UITexture _gemIcon;
  [SerializeField]
  private UILabel _gemLevel;
  [SerializeField]
  private Transform _effectRoot;
  [SerializeField]
  private GameObject _inlayEffect;
  [SerializeField]
  private GameObject _demolitionEffect;
  [SerializeField]
  private GameObject _explodeEffect;
  [SerializeField]
  private Transform _GemItemRoot;
  [SerializeField]
  private UILabel _labelGemName;
  [SerializeField]
  private UIDragScrollView _dragScrollView;
  private int _slotIndex;
  private GemType _gemType;
  private GemData _gemData;
  private bool _interactable;
  private InventoryItemData _inventoryItemData;
  protected Transform _EffectParent;
  protected Vector3 _WorldPosition;

  private string GetGemTypeSpriteByType(GemType gemType)
  {
    string empty = string.Empty;
    int num = (int) gemType;
    if (num == 0)
      return string.Empty;
    return "icon_gem_empty_slot_" + (object) (num + 2);
  }

  public void SetData(int slotIndex, GemType gemType, InventoryItemData inventoryData, bool interactable, UIScrollView container = null)
  {
    this._EffectParent = this._GemItemRoot.parent;
    this._equipmentGemDetailTemplate.gameObject.SetActive(false);
    this._slotIndex = slotIndex;
    this._gemType = gemType;
    this._gemData = inventoryData == null ? (GemData) null : inventoryData.inlaidGems.GetGemOnSlot(slotIndex);
    this._interactable = interactable;
    this._inventoryItemData = inventoryData;
    this.DestoryAllEquipmentGemDetailItem();
    this._rootHaveNoGem.SetActive(false);
    this._rootHaveGem.SetActive(false);
    this._labelGemName.text = string.Empty;
    if ((bool) ((UnityEngine.Object) this._dragScrollView))
      this._dragScrollView.scrollView = container;
    if (this._gemData == null)
    {
      this._rootHaveNoGem.SetActive(true);
      this._rootInteractableTag.SetActive(interactable);
      this.CreateEquipmentGemSlotItem().SetData(ScriptLocalization.Get("forge_armory_gemstone_socket_empty", true), string.Empty);
      this._emptyGemSlotIcon.spriteName = this.GetGemTypeSpriteByType(gemType);
    }
    else
    {
      this._rootHaveGem.SetActive(true);
      ConfigEquipmentGemInfo data = ConfigManager.inst.DB_EquipmentGem.GetData(this._gemData.ConfigId);
      if (data != null)
      {
        BuilderFactory.Instance.HandyBuild((UIWidget) this._gemIcon, data.IconPath, (System.Action<bool>) null, true, false, string.Empty);
        this._gemLevel.text = "+" + (object) this._gemData.Level;
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(data.itemID);
        if (itemStaticInfo != null)
          this._labelGemName.text = itemStaticInfo.LocName;
        using (List<Benefits.BenefitValuePair>.Enumerator enumerator = data.benefits.GetBenefitsPairs().GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Benefits.BenefitValuePair current = enumerator.Current;
            this.CreateEquipmentGemSlotItem().SetData(current.propertyDefinition.Name, current.propertyDefinition.ConvertToDisplayString((double) current.value, true, true));
          }
        }
      }
    }
    this._benefitContainer.Reposition();
    this._rootContainer.Reposition();
  }

  private void PlayEffect(GameObject effectTemplate)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(effectTemplate);
    gameObject.transform.SetParent(this._EffectParent);
    gameObject.transform.position = this._WorldPosition;
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    UnityEngine.Object.Destroy((UnityEngine.Object) gameObject, 3f);
    gameObject.AddComponent<EquipmentGemSlotItem.AutoDestoryAfterDisabled>();
  }

  public void Refresh()
  {
    GemData gemData1 = this._gemData;
    this.SetData(this._slotIndex, this._gemType, this._inventoryItemData, this._interactable, this._dragScrollView.scrollView);
    this._benefitContainer.Reposition();
    for (Transform transform = this.transform; (UnityEngine.Object) transform != (UnityEngine.Object) null; transform = transform.parent)
    {
      UITable component = transform.GetComponent<UITable>();
      if ((bool) ((UnityEngine.Object) component))
        component.Reposition();
    }
    GemData gemData2 = this._inventoryItemData == null ? (GemData) null : this._inventoryItemData.inlaidGems.GetGemOnSlot(this._slotIndex);
    if (gemData1 != null && gemData2 == null)
    {
      if (DBManager.inst.DB_Gems.Get(gemData1.ID) != null)
        this.PlayEffect(this._demolitionEffect);
      else
        this.PlayEffect(this._explodeEffect);
    }
    if (gemData1 != null || gemData2 == null)
      return;
    this.PlayEffect(this._inlayEffect);
  }

  public void OnClicked()
  {
    if (!this._interactable)
      return;
    if (this._gemData == null)
      UIManager.inst.OpenPopup("Gem/EquipmentGemInsetPopup", (Popup.PopupParameter) new EquipmentGemInsetPopup.Parameter()
      {
        slotType = (this._inventoryItemData.bagType != BagType.Hero ? SlotType.dragon_knight : SlotType.lord),
        gemType = this._gemType,
        slotIndex = this._slotIndex,
        inventoryData = this._inventoryItemData,
        callback = new System.Action(this.Refresh)
      });
    else
      UIManager.inst.OpenPopup("Gem/EquipmentGemRemovePopup", (Popup.PopupParameter) new EquipmentGemRemovePopup.Parameter()
      {
        gemData = this._gemData,
        inventoryData = this._inventoryItemData,
        slotIndex = this._slotIndex,
        callback = new System.Action(this.Refresh)
      });
  }

  protected void DestoryAllEquipmentGemDetailItem()
  {
    using (List<EquipmentGemDetailItem>.Enumerator enumerator = this._allEquipmentGemDetail.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EquipmentGemDetailItem current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this._allEquipmentGemDetail.Clear();
  }

  protected EquipmentGemDetailItem CreateEquipmentGemSlotItem()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._equipmentGemDetailTemplate.gameObject);
    gameObject.transform.SetParent(this._benefitContainer.transform);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    EquipmentGemDetailItem component = gameObject.GetComponent<EquipmentGemDetailItem>();
    this._allEquipmentGemDetail.Add(component);
    return component;
  }

  private void Update()
  {
    this._WorldPosition = this._effectRoot.position;
  }

  public class AutoDestoryAfterDisabled : MonoBehaviour
  {
    private void OnDisable()
    {
      if (!(bool) ((UnityEngine.Object) this.gameObject))
        return;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    }
  }
}
