﻿// Decompiled with JetBrains decompiler
// Type: AllianceTechTier
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class AllianceTechTier
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "tier")]
  public string Tier;
  [Config(Name = "req_alliance_level")]
  public int ReqAllianceLevel;
  [Config(Name = "donate_wood_value")]
  public int Donate_Wood_Value;
  [Config(Name = "donate_food_value")]
  public int Donate_Food_Value;
  [Config(Name = "donate_ore_value")]
  public int Donate_Ore_Value;
  [Config(Name = "donate_silver_value")]
  public int Donate_Silver_Value;
  [Config(Name = "donate_gold_value")]
  public int Donate_Gold_Value;
  [Config(Name = "reward_donation_value")]
  public int Reward_Donation_Value;
  [Config(Name = "reward_alliance_exp_value")]
  public int Reward_EXP_Value;
  [Config(Name = "reward_honor_value")]
  public int Reward_Honor_Value;
  [Config(Name = "reward_fund_value")]
  public int Reward_Fund_Value;
}
