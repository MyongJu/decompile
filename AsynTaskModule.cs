﻿// Decompiled with JetBrains decompiler
// Type: AsynTaskModule
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Threading;

public class AsynTaskModule
{
  private Queue<AsynTask> _pool = new Queue<AsynTask>();
  private Dictionary<int, bool> _works = new Dictionary<int, bool>();
  private int _taskId = 1;
  private bool _isDispose = true;
  private bool _isRunning;
  private AsynTask _currentTask;

  public void Execute(AsynTask task)
  {
    this._pool.Enqueue(task);
    this.Check();
  }

  private void Check()
  {
    if (this._pool.Count <= 0 || this._isRunning)
      return;
    this._currentTask = this._pool.Dequeue();
    this._currentTask.IsRunning = true;
    this._isRunning = true;
    this._works.Add(this._currentTask.TaskId, false);
    new Thread(new ThreadStart(((BaseTask) this._currentTask).Execute)).Start();
  }

  public void Remove(int taskId)
  {
    List<AsynTask> asynTaskList = new List<AsynTask>();
    using (Queue<AsynTask>.Enumerator enumerator = this._pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AsynTask current = enumerator.Current;
        if (current.TaskId != taskId)
          asynTaskList.Add(current);
      }
    }
    this._pool.Clear();
    for (int index = 0; index < asynTaskList.Count; ++index)
      this._pool.Enqueue(asynTaskList[index]);
  }

  public bool IsFinish(int taskId)
  {
    if (!this._works.ContainsKey(taskId))
      return false;
    return this._works[taskId];
  }

  public void Initialize()
  {
    this._isDispose = false;
  }

  public void Process()
  {
    if (!this._isRunning || this._currentTask == null || !this._currentTask.IsFinish)
      return;
    this._works[this._currentTask.TaskId] = true;
    this._isRunning = false;
    this.Check();
  }

  public void Dispose()
  {
    this._isDispose = true;
    this._pool.Clear();
    this._works.Clear();
  }
}
