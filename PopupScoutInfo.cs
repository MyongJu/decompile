﻿// Decompiled with JetBrains decompiler
// Type: PopupScoutInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UI;
using UnityEngine;

public class PopupScoutInfo : UI.Dialog
{
  [SerializeField]
  private PopupScoutInfo.Panel panel;
  private Coordinate _startCoordinate;
  private Coordinate _targetCoordinate;

  private int calcTime()
  {
    return MarchData.CalcScoutTime(this._startCoordinate, this._targetCoordinate);
  }

  private int calcCost()
  {
    return (int) ConfigManager.inst.DB_GameConfig.GetData("scout_cost_silver_value").Value;
  }

  public void OnBackPress()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnClosePress()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnScoutButtonPressed()
  {
    GameEngine.Instance.marchSystem.StartScout(this._targetCoordinate);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    PopupScoutInfo.Parameter parameter = orgParam as PopupScoutInfo.Parameter;
    if (parameter == null)
      return;
    this._startCoordinate = parameter.startLocation;
    this._targetCoordinate = parameter.endLocation;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.panel.coordinateK.text = this._targetCoordinate.K.ToString();
    this.panel.coordinateX.text = this._targetCoordinate.X.ToString();
    this.panel.coordinateY.text = this._targetCoordinate.Y.ToString();
    this.panel.time.text = Utils.FormatTime(this.calcTime(), false, false, true);
    this.panel.cost.text = this.calcCost().ToString();
  }

  [Serializable]
  protected class Panel
  {
    public UITexture PlayerIcon;
    public UISprite RankIcon;
    public UILabel RankLabel;
    public UILabel OwnerPower;
    public UILabel ownerName;
    public UILabel cost;
    public UILabel time;
    public UILabel coordinateK;
    public UILabel coordinateX;
    public UILabel coordinateY;
    public UIButton BT_scout;
    public UIButton BT_back;
    public UIButton BT_close;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public Coordinate startLocation;
    public Coordinate endLocation;
  }
}
