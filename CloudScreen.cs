﻿// Decompiled with JetBrains decompiler
// Type: CloudScreen
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UI;
using UnityEngine;

public class CloudScreen : MonoBehaviour
{
  public float duration = 1f;
  public float ccTarZoom = 2000f;
  public float tcTarZoom = 20f;
  private float _maxTime = 5f;
  public GameObject showCloud;
  public Animator animator;
  private float cacheCCZoom;
  private float cacheCCZoomSpeed;
  private float cacheCCMaxZoom;
  private float cacheTCZoom;
  private float cacheTCZoomSpeed;
  private float cacheTCMaxZoom;
  public CloudAnimationTrigger trigger;
  private bool _flag;
  private float _time;
  private System.Action OnPokeCallBack;

  public void CoverCloud(System.Action callBack)
  {
    AudioManager.Instance.PlaySound("sfx_switch_city_kingdom", false);
    this.HadCloud = true;
    NGUITools.SetActive(this.gameObject, true);
    NGUITools.SetActive(this.showCloud, true);
    this.SetCameraState(this.duration);
    this.trigger.OnEndHandler = (System.Action) null;
    this.trigger.OnStartHandler = (System.Action) null;
    if (GameEngine.IsAvailable)
      this.trigger.OnEndHandler = callBack;
    this._flag = true;
    this._time = Time.time;
    this.animator.Play(nameof (CoverCloud));
  }

  private void Update()
  {
    if (!this._flag || !this._flag || (double) Time.time - (double) this._time <= (double) this._maxTime)
      return;
    this._flag = false;
    if (this.trigger.OnEndHandler != null)
    {
      this.trigger.OnEndHandler();
      this.trigger.OnEndHandler = (System.Action) null;
    }
    this._time = 0.0f;
  }

  public bool HadCloud { get; private set; }

  private void OnAimationStart()
  {
  }

  private void SetCameraState(float time)
  {
    this.cacheCCZoom = UIManager.inst.cityCamera.TargetZoom;
    this.cacheCCZoomSpeed = UIManager.inst.cityCamera.zoomSmoothFactor;
    this.cacheCCMaxZoom = UIManager.inst.cityCamera.zoomMaxDis;
    UIManager.inst.cityCamera.zoomMaxDis = this.ccTarZoom;
    UIManager.inst.cityCamera.TargetZoom = this.ccTarZoom;
    UIManager.inst.cityCamera.zoomSmoothFactor = time;
  }

  public void PokeCloud(System.Action callBack = null)
  {
    if (!this.gameObject.activeSelf)
      return;
    this.animator.SetTrigger("Exit");
    this.StartCoroutine(this.ResetCameraState());
    if (!GameEngine.IsAvailable)
      return;
    this.trigger.OnEndHandler = (System.Action) null;
    this.trigger.OnStartHandler = (System.Action) null;
    if (!GameEngine.IsAvailable)
      return;
    this.trigger.OnStartHandler = new System.Action(this.OnCompleteHandler);
    this.OnPokeCallBack = callBack;
  }

  [DebuggerHidden]
  public IEnumerator ResetCameraState()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CloudScreen.\u003CResetCameraState\u003Ec__Iterator4B()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void OnCompleteHandler()
  {
    NGUITools.SetActive(this.gameObject, false);
    if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.CityMode)
      AccountManager.Instance.ShowRecommendReview();
    UIManager.inst.cityCamera.zoomSmoothFactor = this.cacheCCZoomSpeed;
    UIManager.inst.cityCamera.zoomMaxDis = this.cacheCCMaxZoom;
    this.HadCloud = false;
    if (this.OnPokeCallBack == null)
      return;
    this.OnPokeCallBack();
    this.OnPokeCallBack = (System.Action) null;
  }
}
