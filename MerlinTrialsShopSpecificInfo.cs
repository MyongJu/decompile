﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsShopSpecificInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MerlinTrialsShopSpecificInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "icon")]
  public string icon;
  [Config(Name = "pay_id")]
  public string payId;
  [Config(Name = "discount")]
  public int discount;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "Rewards")]
  public Reward rewards;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "description")]
  public string description;

  public string ImagePath
  {
    get
    {
      return "Texture/IAP/" + this.icon;
    }
  }

  public string Name
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }

  public string Description
  {
    get
    {
      return Utils.XLAT(this.description);
    }
  }

  public string ProductId
  {
    get
    {
      IAPPlatformInfo iapPlatformInfo = ConfigManager.inst.DB_IAPPlatform.Get(Application.platform, this.payId);
      if (iapPlatformInfo != null)
        return iapPlatformInfo.product_id;
      return string.Empty;
    }
  }

  public string IapGroupName
  {
    get
    {
      return "merlin_trials_package";
    }
  }
}
