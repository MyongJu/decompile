﻿// Decompiled with JetBrains decompiler
// Type: RebateByRechargeDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class RebateByRechargeDlg : UI.Dialog
{
  private List<RebateChestInfo> chests = new List<RebateChestInfo>();
  private Dictionary<int, RebateRewardsSlotItemRenderer> itemDict = new Dictionary<int, RebateRewardsSlotItemRenderer>();
  private GameObjectPool itemPool = new GameObjectPool();
  private const int ProgressSliderCount = 5;
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  public UILabel timeDur;
  public UILabel rechargedAmount;
  public UILabel progressStartValue;
  public UITexture rebateNpcTexture;
  public GameObject progressSliderContainer;
  public UIProgressBar progressSlider;
  public RebateChestInfo chestPrefab;

  private void Start()
  {
    UIViewport componentInChildren = this.GetComponentInChildren<UIViewport>();
    if (!((UnityEngine.Object) null != (UnityEngine.Object) componentInChildren))
      return;
    componentInChildren.sourceCamera = UIManager.inst.ui2DCamera;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.AddEventHandler();
    this.OnSecondEventHandler(0);
    this.ShowNPC();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.ClearData();
  }

  private void ShowNPC()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.rebateNpcTexture, "Texture/GUI_Textures/Recharge_NPC", (System.Action<bool>) null, true, false, string.Empty);
  }

  private void UpdateUI()
  {
    this.rechargedAmount.text = string.Format(Utils.XLAT("event_topup_rewards_already_topup_num"), (object) ("[FFD700]" + Utils.FormatThousands(RebateByRechargePayload.Instance.RechargedAmount.ToString()) + "[-]"));
    this.ClearData();
    this.ShowProgressSlider();
    this.ShowProgressNodeContent();
    this.ShowRebateRewardContent();
  }

  private int GetStartRebateTargetAmount()
  {
    List<RebateByRechargeRewards> rebateRewardsList = RebateByRechargePayload.Instance.RebateRewardsList;
    int rebateStartIndex = this.GetRebateStartIndex();
    return rebateStartIndex <= 0 ? 0 : rebateRewardsList[rebateStartIndex - 1].TargetRechargeAmount;
  }

  private int GetRebateStartIndex()
  {
    List<RebateByRechargeRewards> rebateRewardsList = RebateByRechargePayload.Instance.RebateRewardsList;
    int progressNodeCount = this.GetProgressNodeCount();
    if (rebateRewardsList.Count <= progressNodeCount)
      return 0;
    for (int index = 0; index < rebateRewardsList.Count; ++index)
    {
      if (!rebateRewardsList[index].Rebated)
      {
        int num = index / progressNodeCount;
        if (num == 0 || index / 5 < (rebateRewardsList.Count - 1) / 5)
          return num * 5;
        return rebateRewardsList.Count - 5;
      }
    }
    return rebateRewardsList.Count - 5;
  }

  private int GetProgressNodeCount()
  {
    return Mathf.Min(RebateByRechargePayload.Instance.RebateRewardsList.Count, 5);
  }

  private void ShowProgressNodeContent()
  {
    this.ClearChestData();
    this.progressStartValue.text = Utils.FormatThousands(this.GetStartRebateTargetAmount().ToString());
    List<RebateByRechargeRewards> rebateRewardsList = RebateByRechargePayload.Instance.RebateRewardsList;
    int width = this.progressSlider.foregroundWidget.width;
    Vector3 localPosition = this.progressSlider.foregroundWidget.transform.localPosition;
    int progressNodeCount = this.GetProgressNodeCount();
    int num = 0;
    int rebateStartIndex = this.GetRebateStartIndex();
    while (num < progressNodeCount)
    {
      GameObject go = NGUITools.AddChild(this.progressSliderContainer, this.chestPrefab.gameObject);
      NGUITools.SetActive(go, true);
      float x = localPosition.x + (float) (num * width) / 6f;
      go.transform.localPosition = new Vector3(x, this.chestPrefab.transform.localPosition.y);
      RebateChestInfo component = go.GetComponent<RebateChestInfo>();
      component.SetData(rebateRewardsList[rebateStartIndex]);
      this.chests.Add(component);
      ++num;
      ++rebateStartIndex;
    }
  }

  private void ShowProgressSlider()
  {
    List<RebateByRechargeRewards> rebateRewardsList = RebateByRechargePayload.Instance.RebateRewardsList;
    int progressNodeCount = this.GetProgressNodeCount();
    long rechargedAmount = RebateByRechargePayload.Instance.RechargedAmount;
    int rebateStartIndex = this.GetRebateStartIndex();
    for (int index = 0; index < progressNodeCount && rebateStartIndex >= 0; ++index)
    {
      if (rebateStartIndex < rebateRewardsList.Count)
      {
        RebateByRechargeRewards byRechargeRewards = rebateRewardsList[rebateStartIndex];
        if (rechargedAmount < (long) byRechargeRewards.TargetRechargeAmount)
        {
          int num1 = rebateStartIndex <= 0 ? byRechargeRewards.TargetRechargeAmount : byRechargeRewards.TargetRechargeAmount - rebateRewardsList[rebateStartIndex - 1].TargetRechargeAmount;
          int num2 = rebateStartIndex <= 0 ? (int) rechargedAmount : (int) (rechargedAmount - (long) rebateRewardsList[rebateStartIndex - 1].TargetRechargeAmount);
          if (num1 == 0)
            break;
          this.progressSlider.value = (float) (0.16666667163372 * ((double) ((float) num2 / (float) num1) + (double) index));
          break;
        }
        this.progressSlider.value = (float) (index + 1) / 6f;
      }
      ++rebateStartIndex;
    }
  }

  private void ShowRebateRewardContent()
  {
    List<RebateByRechargeRewards> rebateRewardsList = RebateByRechargePayload.Instance.RebateRewardsList;
    for (int key = 0; key < rebateRewardsList.Count; ++key)
    {
      RebateRewardsSlotItemRenderer itemRenderer = this.CreateItemRenderer(rebateRewardsList[key]);
      this.itemDict.Add(key, itemRenderer);
    }
    this.Reposition();
  }

  private void OnRebateRewardsDataChanged()
  {
    this.UpdateUI();
  }

  private RebateRewardsSlotItemRenderer CreateItemRenderer(RebateByRechargeRewards rebateRewards)
  {
    GameObject gameObject = this.itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    RebateRewardsSlotItemRenderer component = gameObject.GetComponent<RebateRewardsSlotItemRenderer>();
    component.SetData(rebateRewards);
    component.OnCollectHandler -= new System.Action(this.OnCollectHandler);
    component.OnCollectHandler += new System.Action(this.OnCollectHandler);
    component.OnItemClickHandler -= new System.Action(this.OnRewardItemClickHandler);
    component.OnItemClickHandler += new System.Action(this.OnRewardItemClickHandler);
    return component;
  }

  private void OnCollectHandler()
  {
    this.ShowProgressSlider();
    this.ShowProgressNodeContent();
  }

  private void OnRewardItemClickHandler()
  {
    using (Dictionary<int, RebateRewardsSlotItemRenderer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
        NGUITools.SetActive(enumerator.Current.Value.tipName.transform.parent.gameObject, false);
    }
  }

  private void ClearData()
  {
    using (Dictionary<int, RebateRewardsSlotItemRenderer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this.itemPool.Release(gameObject);
      }
    }
    this.itemDict.Clear();
    this.itemPool.Clear();
    this.ClearChestData();
  }

  private void ClearChestData()
  {
    for (int index = 0; index < this.chests.Count; ++index)
    {
      this.chests[index].gameObject.SetActive(false);
      this.chests[index].transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.chests[index].gameObject);
    }
    this.chests.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEventHandler);
    RebateByRechargePayload.Instance.OnRebateDataChanged -= new System.Action(this.OnRebateRewardsDataChanged);
    RebateByRechargePayload.Instance.OnRebateDataChanged += new System.Action(this.OnRebateRewardsDataChanged);
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEventHandler);
    RebateByRechargePayload.Instance.OnRebateDataChanged -= new System.Action(this.OnRebateRewardsDataChanged);
  }

  private void OnSecondEventHandler(int time)
  {
    int time1 = RebateByRechargePayload.Instance.EndTime - NetServerTime.inst.ServerTimestamp;
    this.timeDur.text = string.Format("{0}" + Utils.XLAT("event_finishes_num"), (object) string.Empty, (object) Utils.FormatTime(time1, true, false, true));
    if (time1 > 0)
      return;
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }
}
