﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightBundleConfiguration
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UnityEngine;

public class DragonKnightBundleConfiguration
{
  public const string ASSET_PATH = "Prefab/DragonKnight/Objects/DragonKnightBundles";
  private DragonKnightBundleConfiguration.BundleConfiguratue _configuration;
  private static DragonKnightBundleConfiguration _instance;

  public static DragonKnightBundleConfiguration Instance
  {
    get
    {
      if (DragonKnightBundleConfiguration._instance == null)
        DragonKnightBundleConfiguration._instance = new DragonKnightBundleConfiguration();
      return DragonKnightBundleConfiguration._instance;
    }
  }

  public string[] GetBundleNames()
  {
    if (this._configuration == null)
    {
      this._configuration = JsonReader.Deserialize<DragonKnightBundleConfiguration.BundleConfiguratue>((AssetManager.Instance.Load("Prefab/DragonKnight/Objects/DragonKnightBundles", (System.Type) null) as TextAsset).text);
      AssetManager.Instance.UnLoadAsset("Prefab/DragonKnight/Objects/DragonKnightBundles", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    }
    return this._configuration.Bundles;
  }

  public string[] GetNonCachedBundleNames()
  {
    string[] bundleNames = this.GetBundleNames();
    List<string> stringList = new List<string>();
    foreach (string bundleName in bundleNames)
    {
      if (!BundleManager.Instance.IsBundleCached(bundleName))
        stringList.Add(bundleName);
    }
    return stringList.ToArray();
  }

  private class BundleConfiguratue
  {
    public string[] Bundles;
  }
}
