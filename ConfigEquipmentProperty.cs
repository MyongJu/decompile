﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentProperty
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigEquipmentProperty
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, ConfigEquipmentPropertyInfo> datas;
  private Dictionary<int, ConfigEquipmentPropertyInfo> dicByUniqueId;
  private Dictionary<int, List<ConfigEquipmentPropertyInfo>> dicByEquipID;

  public void BuildDB(object res)
  {
    this.parse.Parse<ConfigEquipmentPropertyInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.dicByEquipID = new Dictionary<int, List<ConfigEquipmentPropertyInfo>>();
    Dictionary<int, ConfigEquipmentPropertyInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (!this.dicByEquipID.ContainsKey(enumerator.Current.Value.equipID))
        this.dicByEquipID.Add(enumerator.Current.Value.equipID, new List<ConfigEquipmentPropertyInfo>());
      this.dicByEquipID[enumerator.Current.Value.equipID].Add(enumerator.Current.Value);
    }
  }

  public ConfigEquipmentPropertyInfo GetData(int internalID)
  {
    ConfigEquipmentPropertyInfo equipmentPropertyInfo = (ConfigEquipmentPropertyInfo) null;
    this.dicByUniqueId.TryGetValue(internalID, out equipmentPropertyInfo);
    return equipmentPropertyInfo;
  }

  public ConfigEquipmentPropertyInfo GetData(string equipmentId)
  {
    Dictionary<string, ConfigEquipmentPropertyInfo>.KeyCollection.Enumerator enumerator = this.datas.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current == equipmentId)
        return this.datas[enumerator.Current];
    }
    return (ConfigEquipmentPropertyInfo) null;
  }

  public ConfigEquipmentPropertyInfo GetDataByEquipIDAndEnhanceLevel(int equipID, int enhanceLevel)
  {
    if (!this.dicByEquipID.ContainsKey(equipID))
      return (ConfigEquipmentPropertyInfo) null;
    for (int index = 0; index < this.dicByEquipID[equipID].Count; ++index)
    {
      if (this.dicByEquipID[equipID][index].enhanceLevel == enhanceLevel)
        return this.dicByEquipID[equipID][index];
    }
    return (ConfigEquipmentPropertyInfo) null;
  }

  public int GetMaxEnhanceLevel(int equipID)
  {
    return this.dicByEquipID[equipID].Count - 1;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, ConfigEquipmentPropertyInfo>) null;
    }
    if (this.dicByUniqueId != null)
    {
      this.dicByUniqueId.Clear();
      this.dicByUniqueId = (Dictionary<int, ConfigEquipmentPropertyInfo>) null;
    }
    if (this.dicByEquipID == null)
      return;
    this.dicByEquipID.Clear();
    this.dicByEquipID = (Dictionary<int, List<ConfigEquipmentPropertyInfo>>) null;
  }
}
