﻿// Decompiled with JetBrains decompiler
// Type: AuctionHallDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class AuctionHallDlg : UI.Dialog
{
  private Dictionary<int, AuctionItemRenderer> _itemDict = new Dictionary<int, AuctionItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private int _curPickedTab = -1;
  private List<EquipmentSuitItem> allEquipmentSuitItem = new List<EquipmentSuitItem>();
  private List<EquipmentGemItem> _allEquipmentGemItem = new List<EquipmentGemItem>();
  public UILabel panelTitle;
  public UILabel panelLeftHint;
  public UILabel curGoldAmount;
  public UILabel returnGoldAmount;
  public UITexture auctionNpcBkg;
  public UIButton returnGoldButton;
  public HelpBtnComponent helpBtnComponent;
  public GameObject noItemDetailObj;
  public GameObject auctionItemDetailObj;
  public GameObject auctionEquipmentDetailObj;
  public GameObject auctionEquipmentScrollDetailObj;
  public GameObject auctionArtifactDetailObj;
  public UITexture auctionItemTexture;
  public UILabel auctionItemName;
  public UILabel auctionItemDesc;
  public UITexture normalBackground;
  public Transform equipmentSuitItemContainerForEquipment;
  public Transform equipmentGemItemContainerForEquipment;
  public UITable equipmentInfoContainer;
  public EquipmentBenefits benefitContent;
  public EquipmentComponent equipComponent;
  public Transform equipmentSuitItemContainerForScroll;
  public Transform equipmentGemItemContainerForScroll;
  public UITable scrollInfoContainer;
  public EquipmentBenefits benefitScrollContent;
  public EquipmentComponent equipScrollComponent;
  public ItemIconRenderer equipScrollItemRenderer;
  public UILabel equipScrollName;
  public UITexture artifactTexture;
  public UILabel artifactName;
  public UILabel artifactProtectTime;
  public UILabel artifactDesc;
  public EquipmentBenefits artifactBenefitContent;
  public UIScrollView artifactDetailScrollView;
  public UITable artifactDetailTable;
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemAuctionPrefab;
  public GameObject itemDarkAuctionPrefab;
  public GameObject itemWorldAuctionPrefab;
  public GameObject itemRoot;
  public GameObject _auctionDragView;
  public GameObject _worldAuctionDragView;
  public UIScrollView equipmentScrollView;
  public UIScrollView equipmentScrollScrollView;
  [SerializeField]
  private UIToggle[] _tabCategories;
  [SerializeField]
  private GameObject _objPanelTitle;
  [SerializeField]
  private GameObject _objTabTitle;
  [SerializeField]
  private UILabel _labelWorldAuctionItemCount;
  [SerializeField]
  private GameObject _worldAuctionHasGold;
  [SerializeField]
  private GameObject _noWorldAuctionGoodsHint;
  [SerializeField]
  private UIScrollView _worldAuctionScrollView;
  [SerializeField]
  private UITable _worldAuctionTable;
  private AuctionHelper.AuctionType _auctionType;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    AuctionHallDlg.Parameter parameter = orgParam as AuctionHallDlg.Parameter;
    if (parameter != null)
      this._auctionType = parameter.auctionType;
    if (this._auctionType == AuctionHelper.AuctionType.PUBLIC_AUCTION || this._auctionType == AuctionHelper.AuctionType.WORLD_AUCTION)
    {
      this._curPickedTab = this._auctionType != AuctionHelper.AuctionType.PUBLIC_AUCTION ? 1 : 0;
      if (this._curPickedTab < 0)
        this._tabCategories[0].value = true;
      else if (this._curPickedTab >= 0 && this._curPickedTab < this._tabCategories.Length)
        this._tabCategories[this._curPickedTab].value = true;
    }
    switch (this._auctionType)
    {
      case AuctionHelper.AuctionType.PUBLIC_AUCTION:
        this._itemPool.Initialize(this.itemAuctionPrefab, this.itemRoot);
        break;
      case AuctionHelper.AuctionType.NON_PUBLIC_AUCTION:
        this._itemPool.Initialize(this.itemDarkAuctionPrefab, this.itemRoot);
        break;
      case AuctionHelper.AuctionType.WORLD_AUCTION:
        this._itemPool.Initialize(this.itemWorldAuctionPrefab, this._worldAuctionTable.gameObject);
        break;
    }
    this.AddEventHandler();
    this.UpdateUI(true);
    if (!AuctionHelper.Instance.IsWorldAuctionOpen || this._auctionType != AuctionHelper.AuctionType.PUBLIC_AUCTION)
      return;
    AuctionHelper.Instance.LoadAuctionData(AuctionHelper.AuctionType.WORLD_AUCTION, (System.Action<bool, object>) null, false);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.ClearData();
    this.RemoveEventHandler();
  }

  public void OnDrawReturnPressed()
  {
    MessageHub.inst.GetPortByAction("auction:returnGold").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "type",
        (object) this._auctionType
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      AuctionHelper.Instance.ResetReturnGold2Zero(this._auctionType);
      this.ShowAuctionInfo();
    }), true);
  }

  public void OnGoldBtnPressed()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  public void OnAuctionHistoryBtnPressed()
  {
    UIManager.inst.OpenPopup("Auction/AuctionHallHistoryPopup", (Popup.PopupParameter) new AuctionHallHistoryPopup.Parameter()
    {
      auctionType = this._auctionType
    });
  }

  public void OnAuctionDailyRecordPressed()
  {
    AuctionHelper.Instance.ShowAuctionRecord(this._auctionType, (System.Action<bool, List<AuctionRecordItemInfo>>) ((ret, itemInfoList) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenPopup("Auction/AuctionHallRecordPopup", (Popup.PopupParameter) new AuctionHallRecordPopup.Paramter()
      {
        auctionType = this._auctionType,
        auctionRecordItemInfoList = itemInfoList
      });
    }));
  }

  private void UpdateUI(bool forceRefreshScrollView = false)
  {
    this.ShowAuctionInfo();
    this.ShowAuctionContent(forceRefreshScrollView);
  }

  private void ShowAuctionInfo()
  {
    NGUITools.SetActive(this._objPanelTitle, false);
    NGUITools.SetActive(this._objTabTitle, false);
    switch (this._auctionType)
    {
      case AuctionHelper.AuctionType.PUBLIC_AUCTION:
        NGUITools.SetActive(this._objTabTitle, AuctionHelper.Instance.IsWorldAuctionOpen);
        NGUITools.SetActive(this._objPanelTitle, !AuctionHelper.Instance.IsWorldAuctionOpen);
        this.panelTitle.text = Utils.XLAT("exchange_auction_title");
        this.panelLeftHint.text = Utils.XLAT("exchange_auction_description");
        this.helpBtnComponent.ID = "help_auction_normal";
        this.SetNpc("npc_store_character");
        break;
      case AuctionHelper.AuctionType.NON_PUBLIC_AUCTION:
        NGUITools.SetActive(this._objPanelTitle, true);
        this.panelTitle.text = Utils.XLAT("exchange_black_market_title");
        this.panelLeftHint.text = Utils.XLAT("exchange_black_market_description");
        this.helpBtnComponent.ID = "help_auction_hidden";
        this.SetNpc("npc_store_character");
        break;
      case AuctionHelper.AuctionType.WORLD_AUCTION:
        NGUITools.SetActive(this._objTabTitle, true);
        this.panelLeftHint.text = Utils.XLAT("event_world_auction_description");
        this.helpBtnComponent.ID = "help_world_auction";
        this.SetNpc("npc_auction");
        break;
    }
    int num = AuctionHelper.Instance.GetReturnGoldAmount(AuctionHelper.AuctionType.WORLD_AUCTION) <= 0L ? 0 : 1;
    List<AuctionItemInfo> auctionItemInfoList = AuctionHelper.Instance.GetAuctionItemInfoList(AuctionHelper.AuctionType.WORLD_AUCTION);
    if (auctionItemInfoList != null)
      auctionItemInfoList = auctionItemInfoList.FindAll((Predicate<AuctionItemInfo>) (x => x.itemRemainedTime >= NetServerTime.inst.ServerTimestamp));
    if (auctionItemInfoList != null)
      num = auctionItemInfoList.Count + num;
    this._labelWorldAuctionItemCount.text = num.ToString();
    NGUITools.SetActive(this._labelWorldAuctionItemCount.gameObject, num > 0);
    NGUITools.SetActive(this._worldAuctionHasGold, AuctionHelper.Instance.GetReturnGoldAmount(this._auctionType) > 0L);
    this.curGoldAmount.text = Utils.FormatThousands(PlayerData.inst.userData.currency.gold.ToString());
    this.returnGoldAmount.text = Utils.FormatThousands(AuctionHelper.Instance.GetReturnGoldAmount(this._auctionType).ToString());
    this.returnGoldButton.isEnabled = AuctionHelper.Instance.GetReturnGoldAmount(this._auctionType) > 0L;
  }

  private void SetNpc(string image)
  {
    if (!((UnityEngine.Object) this.auctionNpcBkg.mainTexture == (UnityEngine.Object) null) && !(this.auctionNpcBkg.mainTexture.name != image))
      return;
    Utils.SetPortrait(this.auctionNpcBkg, "Texture/GUI_Textures/" + image);
  }

  private void ShowAuctionContent(bool forceRefreshScrollView = false)
  {
    this.ClearData();
    NGUITools.SetActive(this.scrollView.gameObject, false);
    NGUITools.SetActive(this._worldAuctionScrollView.gameObject, false);
    NGUITools.SetActive(this._auctionDragView, false);
    NGUITools.SetActive(this._worldAuctionDragView, false);
    switch (this._auctionType)
    {
      case AuctionHelper.AuctionType.PUBLIC_AUCTION:
      case AuctionHelper.AuctionType.NON_PUBLIC_AUCTION:
        NGUITools.SetActive(this.scrollView.gameObject, true);
        NGUITools.SetActive(this._auctionDragView, true);
        break;
      case AuctionHelper.AuctionType.WORLD_AUCTION:
        NGUITools.SetActive(this._worldAuctionScrollView.gameObject, true);
        NGUITools.SetActive(this._worldAuctionDragView, true);
        break;
    }
    List<AuctionItemInfo> auctionItemInfoList = AuctionHelper.Instance.GetAuctionItemInfoList(this._auctionType);
    if (auctionItemInfoList != null)
    {
      for (int key = 0; key < auctionItemInfoList.Count; ++key)
      {
        if (auctionItemInfoList[key].itemRemainedTime - NetServerTime.inst.ServerTimestamp >= 0)
        {
          AuctionItemRenderer itemRenderer = this.CreateItemRenderer(auctionItemInfoList[key]);
          this._itemDict.Add(key, itemRenderer);
        }
      }
    }
    if (auctionItemInfoList != null)
      auctionItemInfoList = auctionItemInfoList.FindAll((Predicate<AuctionItemInfo>) (x => x.itemRemainedTime >= NetServerTime.inst.ServerTimestamp));
    NGUITools.SetActive(this._noWorldAuctionGoodsHint, this._auctionType == AuctionHelper.AuctionType.WORLD_AUCTION && (auctionItemInfoList == null || auctionItemInfoList.Count <= 0));
    this.Reposition(forceRefreshScrollView);
  }

  private void ClearData()
  {
    using (Dictionary<int, AuctionItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, AuctionItemRenderer> current = enumerator.Current;
        current.Value.onAuctionBiddingSuccessed -= new System.Action(this.OnAuctionBiddingSuccessed);
        current.Value.onAuctionBiddingFinished -= new System.Action(this.OnAuctionBiddingFinished);
        current.Value.onAuctionItemPressed -= new System.Action<AuctionItemInfo>(this.OnAuctionItemPressed);
        current.Value.ClearData();
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition(bool forceRefreshScrollView = false)
  {
    if (this._auctionType == AuctionHelper.AuctionType.NON_PUBLIC_AUCTION)
      this.table.padding.y = -100f;
    this.table.repositionNow = true;
    this.table.Reposition();
    this._worldAuctionTable.repositionNow = true;
    this._worldAuctionTable.Reposition();
    if (!forceRefreshScrollView)
      return;
    this.scrollView.ResetPosition();
    this._worldAuctionScrollView.ResetPosition();
  }

  private AuctionItemRenderer CreateItemRenderer(AuctionItemInfo itemInfo)
  {
    GameObject gameObject = this._itemPool.AddChild(this._auctionType != AuctionHelper.AuctionType.WORLD_AUCTION ? this.table.gameObject : this._worldAuctionTable.gameObject);
    gameObject.SetActive(true);
    AuctionItemRenderer component = gameObject.GetComponent<AuctionItemRenderer>();
    component.SetData(this._auctionType, itemInfo);
    component.onAuctionBiddingSuccessed += new System.Action(this.OnAuctionBiddingSuccessed);
    component.onAuctionBiddingFinished += new System.Action(this.OnAuctionBiddingFinished);
    component.onAuctionItemPressed += new System.Action<AuctionItemInfo>(this.OnAuctionItemPressed);
    return component;
  }

  private void OnAuctionItemPressed(AuctionItemInfo itemInfo)
  {
    if (itemInfo == null)
      return;
    this.HideAuctionItemSelectedEffects();
    switch (AuctionHelper.Instance.GetAuctionItemType(itemInfo))
    {
      case AuctionHelper.AuctionItemType.PROPS:
        this.ShowAuctionPropsDetail(itemInfo);
        break;
      case AuctionHelper.AuctionItemType.EQUIPMENT:
      case AuctionHelper.AuctionItemType.DK_EQUIPMENT:
        this.ShowAuctionEquipmentDetail(itemInfo);
        break;
      case AuctionHelper.AuctionItemType.EQUIPMENT_SCROLL:
      case AuctionHelper.AuctionItemType.DK_EQUIPMENT_SCROLL:
        this.ShowAuctionEquipmentScrollDetail(itemInfo);
        break;
      case AuctionHelper.AuctionItemType.ARTIFACT:
        this.ShowAuctionArtifactDetail(itemInfo);
        break;
    }
  }

  private void HideAuctionItemSelectedEffects()
  {
    using (Dictionary<int, AuctionItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.HideItemSelectedEffects();
    }
  }

  private void HideAllAuctionDetail()
  {
    NGUITools.SetActive(this.noItemDetailObj, false);
    NGUITools.SetActive(this.auctionItemDetailObj, false);
    NGUITools.SetActive(this.auctionEquipmentDetailObj, false);
    NGUITools.SetActive(this.auctionEquipmentScrollDetailObj, false);
    NGUITools.SetActive(this.auctionArtifactDetailObj, false);
  }

  private void ShowAuctionPropsDetail(AuctionItemInfo itemInfo)
  {
    this.HideAllAuctionDetail();
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemInfo.itemId);
    if (itemStaticInfo != null)
    {
      Utils.SetItemName(this.auctionItemName, itemStaticInfo.internalId);
      this.auctionItemDesc.text = itemStaticInfo.LocDescription;
      Utils.SetItemBackground(this.normalBackground, itemStaticInfo.internalId);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.auctionItemTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    }
    NGUITools.SetActive(this.auctionItemDetailObj, true);
  }

  private void ShowAuctionEquipmentDetail(AuctionItemInfo itemInfo)
  {
    this.HideAllAuctionDetail();
    AuctionHelper.AuctionItemType auctionItemType = AuctionHelper.Instance.GetAuctionItemType(itemInfo);
    BagType bagType = BagType.Hero;
    switch (auctionItemType)
    {
      case AuctionHelper.AuctionItemType.EQUIPMENT:
        bagType = BagType.Hero;
        break;
      case AuctionHelper.AuctionItemType.DK_EQUIPMENT:
        bagType = BagType.DragonKnight;
        break;
    }
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemInfo.itemId);
    if (itemStaticInfo == null)
      return;
    this.ShowEquipmentDetails(itemStaticInfo.ID + "_0", bagType, false);
  }

  private void ShowAuctionEquipmentScrollDetail(AuctionItemInfo itemInfo)
  {
    this.HideAllAuctionDetail();
    AuctionHelper.AuctionItemType auctionItemType = AuctionHelper.Instance.GetAuctionItemType(itemInfo);
    BagType bagType = BagType.Hero;
    switch (auctionItemType)
    {
      case AuctionHelper.AuctionItemType.EQUIPMENT_SCROLL:
        bagType = BagType.Hero;
        break;
      case AuctionHelper.AuctionItemType.DK_EQUIPMENT_SCROLL:
        bagType = BagType.DragonKnight;
        break;
    }
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemInfo.itemId);
    if (itemStaticInfo == null)
      return;
    ConfigEquipmentScrollInfo dataByItemId = ConfigManager.inst.GetEquipmentScroll(bagType).GetDataByItemID(itemStaticInfo.internalId);
    if (dataByItemId == null)
      return;
    ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(dataByItemId.equipmenID);
    if (data != null)
      this.ShowEquipmentDetails(data.ID + "_0", bagType, true);
    this.equipScrollItemRenderer.ItemId = itemInfo.itemId;
    this.equipScrollName.text = itemStaticInfo.LocName;
    this.equipScrollName.color = this.equipScrollComponent.equipmentScrollName.color;
  }

  private void ShowEquipmentDetails(string equipmentId, BagType bagType, bool isEquipmentScroll = false)
  {
    ConfigEquipmentPropertyInfo equipmentPropertyInfo = ConfigManager.inst.GetEquipmentProperty(bagType).GetData(equipmentId);
    if (equipmentPropertyInfo == null)
      return;
    Transform parent;
    if (isEquipmentScroll)
    {
      NGUITools.SetActive(this.auctionEquipmentScrollDetailObj, true);
      parent = this.equipmentSuitItemContainerForScroll;
      this.equipScrollComponent.FeedData((IComponentData) new EquipmentComponentData(equipmentPropertyInfo.equipID, 0, bagType, 0L));
      this.UpdateEquipmentGem(bagType, equipmentPropertyInfo.equipID, this.equipmentGemItemContainerForScroll, this.equipmentScrollScrollView);
      this.UpdateEquipmentSuitBenefit(bagType, equipmentPropertyInfo.equipID, parent);
      Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.benefitScrollContent.Init(equipmentPropertyInfo.benefits)));
    }
    else
    {
      NGUITools.SetActive(this.auctionEquipmentDetailObj, true);
      this.equipComponent.FeedData((IComponentData) new EquipmentComponentData(equipmentPropertyInfo.equipID, 0, bagType, 0L));
      parent = this.equipmentSuitItemContainerForEquipment;
      this.benefitContent.Init(equipmentPropertyInfo.benefits);
      this.UpdateEquipmentGem(bagType, equipmentPropertyInfo.equipID, this.equipmentGemItemContainerForEquipment, this.equipmentScrollView);
      this.UpdateEquipmentSuitBenefit(bagType, equipmentPropertyInfo.equipID, parent);
    }
    this.StartCoroutine(this.Delay2ShowEquipmentDetail(0.05f, isEquipmentScroll, bagType, equipmentPropertyInfo.equipID, parent));
  }

  private void ShowAuctionArtifactDetail(AuctionItemInfo itemInfo)
  {
    this.HideAllAuctionDetail();
    ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(itemInfo.itemArtifactId);
    if (artifactInfo != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactTexture, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this.artifactName.text = artifactInfo.Name;
      this.artifactProtectTime.text = Utils.FormatTime(259200, false, false, true);
      this.artifactDesc.text = artifactInfo.Description;
      this.artifactBenefitContent.Init(artifactInfo.benefits);
      Utils.ExecuteInSecs(0.01f, (System.Action) (() =>
      {
        this.artifactDetailTable.repositionNow = true;
        this.artifactDetailTable.Reposition();
        this.artifactDetailScrollView.ResetPosition();
      }));
    }
    NGUITools.SetActive(this.auctionArtifactDetailObj, true);
  }

  [DebuggerHidden]
  private IEnumerator Delay2ShowEquipmentDetail(float delayTime, bool isEquipmentScroll, BagType bagType, int equipmentId, Transform parent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AuctionHallDlg.\u003CDelay2ShowEquipmentDetail\u003Ec__Iterator2A()
    {
      delayTime = delayTime,
      isEquipmentScroll = isEquipmentScroll,
      \u003C\u0024\u003EdelayTime = delayTime,
      \u003C\u0024\u003EisEquipmentScroll = isEquipmentScroll,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator HideAuctionItemDetail(float delay = 5f)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AuctionHallDlg.\u003CHideAuctionItemDetail\u003Ec__Iterator2B()
    {
      delay = delay,
      \u003C\u0024\u003Edelay = delay,
      \u003C\u003Ef__this = this
    };
  }

  private void OnAuctionBiddingSuccessed()
  {
  }

  private void OnAuctionBiddingFinished()
  {
    this.UpdateUI(false);
    this.StartCoroutine(this.HideAuctionItemDetail(0.01f));
  }

  private void OnAuctionDataUpdated(bool refresh)
  {
    if (!refresh)
      return;
    this.ShowAuctionInfo();
  }

  private void AddEventHandler()
  {
    AuctionHelper.Instance.onAuctionItemDataUpdated += new System.Action<bool>(this.OnAuctionDataUpdated);
    MessageHub.inst.GetPortByAction("auction_update").AddEvent(new System.Action<object>(this.OnPushAuctionUpdate));
    MessageHub.inst.GetPortByAction("world_auction_update").AddEvent(new System.Action<object>(this.OnPushWorldAuctionUpdate));
  }

  private void RemoveEventHandler()
  {
    AuctionHelper.Instance.onAuctionItemDataUpdated -= new System.Action<bool>(this.OnAuctionDataUpdated);
    MessageHub.inst.GetPortByAction("auction_update").RemoveEvent(new System.Action<object>(this.OnPushAuctionUpdate));
    MessageHub.inst.GetPortByAction("world_auction_update").RemoveEvent(new System.Action<object>(this.OnPushWorldAuctionUpdate));
  }

  private void OnPushAuctionUpdate(object orgData)
  {
    Hashtable data = orgData as Hashtable;
    if (data[(object) "type"] == null)
      return;
    int result = 0;
    int.TryParse(data[(object) "type"].ToString(), out result);
    if ((AuctionHelper.AuctionType) result != this._auctionType)
      return;
    AuctionHelper.Instance.UpdateData(this._auctionType, data, false, true);
    this.UpdateUI(false);
    this.StartCoroutine(this.HideAuctionItemDetail(0.01f));
  }

  private void OnPushWorldAuctionUpdate(object orgData)
  {
    if (this._auctionType != AuctionHelper.AuctionType.WORLD_AUCTION)
      return;
    AuctionHelper.Instance.LoadAuctionData(this._auctionType, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || !((UnityEngine.Object) this.gameObject != (UnityEngine.Object) null))
        return;
      this.UpdateUI(false);
      this.StartCoroutine(this.HideAuctionItemDetail(0.01f));
    }), false);
  }

  private void ClearEquipmentSuitItem()
  {
    using (List<EquipmentSuitItem>.Enumerator enumerator = this.allEquipmentSuitItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EquipmentSuitItem current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
        {
          current.transform.parent = (Transform) null;
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
        }
      }
    }
    this.allEquipmentSuitItem.Clear();
  }

  private void UpdateEquipmentSuitBenefit(BagType bagType, int equipmentId, Transform parent)
  {
    this.ClearEquipmentSuitItem();
    ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(equipmentId);
    if (data == null || data.suitGroup == 0)
      return;
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/UI/Common/EquipmentSuitItem", (System.Type) null)) as GameObject;
    gameObject.transform.SetParent(parent, true);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    EquipmentSuitItem component = gameObject.GetComponent<EquipmentSuitItem>();
    component.SetData(PlayerData.inst.uid, bagType, data.suitGroup, (InventoryItemData) null);
    this.allEquipmentSuitItem.Add(component);
  }

  private void ClearEquipmentGemItem()
  {
    using (List<EquipmentGemItem>.Enumerator enumerator = this._allEquipmentGemItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EquipmentGemItem current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this._allEquipmentGemItem.Clear();
  }

  private void UpdateEquipmentGem(BagType bagType, int equipmentInternalId, Transform parent, UIScrollView scrollView)
  {
    this.ClearEquipmentGemItem();
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/UI/Common/EquipmentGemItem", (System.Type) null)) as GameObject;
    gameObject.transform.SetParent(parent, true);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    EquipmentGemItem component = gameObject.GetComponent<EquipmentGemItem>();
    component.SetData(bagType, equipmentInternalId, scrollView);
    this._allEquipmentGemItem.Add(component);
  }

  public void OnTabToggleUpdate()
  {
    int pickedTabIndex = this.GetPickedTabIndex();
    if (pickedTabIndex == this._curPickedTab || pickedTabIndex < 0)
      return;
    this._curPickedTab = pickedTabIndex;
    if (this._curPickedTab <= 0)
      this.ShowTab0Content();
    else
      this.ShowTab1Content();
    this.StartCoroutine(this.HideAuctionItemDetail(0.01f));
  }

  private int GetPickedTabIndex()
  {
    for (int index = 0; index < this._tabCategories.Length; ++index)
    {
      if (this._tabCategories[index].value)
        return index;
    }
    return -1;
  }

  private void ShowTab0Content()
  {
    this._auctionType = AuctionHelper.AuctionType.PUBLIC_AUCTION;
    AuctionHelper.Instance.LoadAuctionData(this._auctionType, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || !((UnityEngine.Object) this.gameObject != (UnityEngine.Object) null))
        return;
      this.UpdateUI(false);
    }), true);
  }

  private void ShowTab1Content()
  {
    this._auctionType = AuctionHelper.AuctionType.WORLD_AUCTION;
    AuctionHelper.Instance.LoadAuctionData(this._auctionType, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || !((UnityEngine.Object) this.gameObject != (UnityEngine.Object) null))
        return;
      this.UpdateUI(false);
    }), true);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public AuctionHelper.AuctionType auctionType;
  }
}
