﻿// Decompiled with JetBrains decompiler
// Type: MailContactFunctionPopUp
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MailContactFunctionPopUp : PopupBase
{
  public UIButton btn1;
  public UIButton btn2;
  public UIButton btn3;
  public UIButton banSpeak;
  public UIButton closeBtn;
  public UIButton modMailBtn;
  public UIGrid grid;

  public override void OnShowPopup()
  {
    if (PlayerData.inst.moderatorInfo.IsModerator)
    {
      this.banSpeak.gameObject.SetActive(true);
      this.modMailBtn.gameObject.SetActive(true);
    }
    else
    {
      this.banSpeak.gameObject.SetActive(false);
      this.modMailBtn.gameObject.SetActive(false);
    }
    this.grid.repositionNow = true;
    this.grid.Reposition();
  }

  public void Initailize(EventDelegate callBack1, EventDelegate callBack2, EventDelegate callBack3)
  {
    this.btn1.onClick.Add(callBack1);
    this.btn1.onClick.Add(new EventDelegate((MonoBehaviour) this, "Close"));
    this.btn2.onClick.Add(callBack2);
    this.btn2.onClick.Add(new EventDelegate((MonoBehaviour) this, "Close"));
    this.btn3.onClick.Add(callBack3);
    this.btn3.onClick.Add(new EventDelegate((MonoBehaviour) this, "Close"));
    this.closeBtn.onClick.Add(new EventDelegate((MonoBehaviour) this, "Close"));
  }

  public void AddBanSpeakListener(EventDelegate banCallBack)
  {
    this.banSpeak.onClick.Add(banCallBack);
    this.banSpeak.onClick.Add(new EventDelegate((MonoBehaviour) this, "Close"));
  }

  public void AddModMailListener(EventDelegate modCallBack)
  {
    this.modMailBtn.onClick.Add(modCallBack);
    this.modMailBtn.onClick.Add(new EventDelegate((MonoBehaviour) this, "Close"));
  }

  public override void OnClosePopup()
  {
    base.OnClosePopup();
    this.btn1.onClick.Clear();
    this.btn2.onClick.Clear();
    this.btn3.onClick.Clear();
    this.closeBtn.onClick.Clear();
    this.banSpeak.onClick.Clear();
    this.modMailBtn.onClick.Clear();
  }
}
