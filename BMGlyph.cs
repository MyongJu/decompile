﻿// Decompiled with JetBrains decompiler
// Type: BMGlyph
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

[Serializable]
public class BMGlyph
{
  public int index;
  public int x;
  public int y;
  public int width;
  public int height;
  public int offsetX;
  public int offsetY;
  public int advance;
  public int channel;
  public List<int> kerning;

  public int GetKerning(int previousChar)
  {
    if (this.kerning != null && previousChar != 0)
    {
      int index = 0;
      int count = this.kerning.Count;
      while (index < count)
      {
        if (this.kerning[index] == previousChar)
          return this.kerning[index + 1];
        index += 2;
      }
    }
    return 0;
  }

  public void SetKerning(int previousChar, int amount)
  {
    if (this.kerning == null)
      this.kerning = new List<int>();
    int index = 0;
    while (index < this.kerning.Count)
    {
      if (this.kerning[index] == previousChar)
      {
        this.kerning[index + 1] = amount;
        return;
      }
      index += 2;
    }
    this.kerning.Add(previousChar);
    this.kerning.Add(amount);
  }

  public void Trim(int xMin, int yMin, int xMax, int yMax)
  {
    int num1 = this.x + this.width;
    int num2 = this.y + this.height;
    if (this.x < xMin)
    {
      int num3 = xMin - this.x;
      this.x += num3;
      this.width -= num3;
      this.offsetX += num3;
    }
    if (this.y < yMin)
    {
      int num3 = yMin - this.y;
      this.y += num3;
      this.height -= num3;
      this.offsetY += num3;
    }
    if (num1 > xMax)
      this.width -= num1 - xMax;
    if (num2 <= yMax)
      return;
    this.height -= num2 - yMax;
  }
}
