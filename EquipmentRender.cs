﻿// Decompiled with JetBrains decompiler
// Type: EquipmentRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class EquipmentRender : EquipmentComponent
{
  public System.Action<EquipmentRender> OnSelectedHandler;
  public GameObject equiped;
  public UILabel freezeTime;

  public override void Init()
  {
    this.componentData = this.data as EquipmentComponentData;
    BuilderFactory.Instance.Build((UIWidget) this.icon, this.componentData.itemInfo.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.background, this.componentData.equipmentInfo.QualityImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    InventoryItemData myItemByItemId = DBManager.inst.GetInventory(this.componentData.bagType).GetMyItemByItemID(this.componentData.itemId);
    if (myItemByItemId == null)
      return;
    NGUITools.SetActive(this.equiped, myItemByItemId.isEquipped);
    if (!((UnityEngine.Object) this.freezeTime != (UnityEngine.Object) null))
      return;
    this.freezeTime.text = myItemByItemId.FreezeTime;
    NGUITools.SetActive(this.freezeTime.gameObject, myItemByItemId.IsFreeze);
  }

  public override void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
    BuilderFactory.Instance.Release((UIWidget) this.background);
  }

  public void OnPressItem()
  {
    Utils.DelayShowTip(this.componentData.itemInfo.internalId, this.transform, this.componentData.itemId, 0L, 0);
  }

  public void OnReleaseItem()
  {
    Utils.StopShowItemTip();
  }

  public void OnClick()
  {
    if (this.OnSelectedHandler == null)
      return;
    this.OnSelectedHandler(this);
  }
}
