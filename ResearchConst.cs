﻿// Decompiled with JetBrains decompiler
// Type: ResearchConst
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ResearchConst
{
  public static string[] RESEARCH_TREE_KEY_PREFIXS = new string[6]
  {
    "research_tree_city_development_",
    "research_tree_economy_",
    "research_tree_troop_",
    "research_tree_trap_",
    "research_tree_dragon_knight_",
    "research_tree_battle_senior_"
  };
  public static string[] RESEARCH_CION_PREFIX = new string[7]
  {
    string.Empty,
    "Texture/ResearchIcons/City Development/ui_",
    "Texture/ResearchIcons/Economy/ui_",
    "Texture/ResearchIcons/Troop/ui_",
    "Texture/ResearchIcons/Trap/ui_",
    "Texture/ResearchIcons/DragonKnight/ui_",
    "Texture/ResearchIcons/BattleSenior/ui_"
  };
  public const string TITLE_SUBFIX = "name";
  public const string DESCRIPTION_SUBFIX = "description";
  private static List<Vector2> directions;

  public static List<Vector2> Direction
  {
    get
    {
      if (ResearchConst.directions == null)
      {
        ResearchConst.directions = new List<Vector2>();
        ResearchConst.directions.Add(new Vector2(0.0f, 0.5f));
        ResearchConst.directions.Add(new Vector2(0.5f, 0.0f));
        ResearchConst.directions.Add(new Vector2(0.0f, -0.5f));
        ResearchConst.directions.Add(new Vector2(-0.5f, 0.0f));
      }
      return ResearchConst.directions;
    }
  }

  public enum ResearchType
  {
    Troop = 1,
    War = 2,
    City = 3,
    Economic = 4,
  }
}
