﻿// Decompiled with JetBrains decompiler
// Type: MerlinFloorItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MerlinFloorItem : MonoBehaviour
{
  [SerializeField]
  protected UIWidget _rootFitHorizonal;
  [SerializeField]
  protected UIPanel _rootPanel;
  [SerializeField]
  protected UIWidget _rootWidget;
  [SerializeField]
  protected GameObject _rootDoor;
  [SerializeField]
  protected GameObject _rootTopMostFloor;
  protected float _roomXRange;

  public float RoomXRange
  {
    get
    {
      return this._roomXRange;
    }
  }

  public Vector3 DoorPosition
  {
    get
    {
      return this._rootDoor.transform.position;
    }
  }

  public void SetRoom(UserMerlinTowerData merlinTowerData)
  {
    bool flag = false;
    if (merlinTowerData != null)
    {
      MagicTowerMainInfo currentTowerMainInfo = merlinTowerData.CurrentTowerMainInfo;
      if (currentTowerMainInfo != null)
      {
        if (currentTowerMainInfo.Level > 1)
          flag = true;
        this._rootTopMostFloor.SetActive(ConfigManager.inst.DB_MagicTowerMain.GetDataWithLevel(currentTowerMainInfo.Level + 1) == null);
      }
    }
    this._rootDoor.SetActive(flag);
  }

  public void FitHorizonal()
  {
    Vector3[] sides = this._rootPanel.GetSides(this._rootFitHorizonal.transform);
    float num1 = sides[2].x - sides[0].x;
    float num2 = num1 / (float) this._rootFitHorizonal.width;
    this._rootFitHorizonal.transform.localScale = new Vector3(num2, num2, 1f);
    this._roomXRange = num1 / num2;
  }

  public UIPanel GetRootPanel()
  {
    return this._rootPanel;
  }

  public UIWidget GetRootWidget()
  {
    return this._rootWidget;
  }

  public void SetAlpha(float alpha)
  {
    this._rootPanel.alpha = alpha;
  }

  public void AlphaTo(float fromAlpha, float toAlpha, float duration)
  {
    this.SetAlpha(fromAlpha);
    TweenAlpha.Begin(this._rootPanel.gameObject, duration, toAlpha, 0.0f);
  }

  public void SetScale(float scale)
  {
    this._rootWidget.transform.localScale = new Vector3(scale, scale, 1f);
  }

  public void ScaleTo(float fromScale, float toScale, float duration)
  {
    this.SetScale(fromScale);
    Vector3 scale = new Vector3(toScale, toScale, 1f);
    TweenScale.Begin(this._rootWidget.gameObject, duration, scale);
  }

  public void SetPosition(float xPosition, float yPosition)
  {
    Vector3 localPosition = this._rootWidget.transform.localPosition;
    localPosition.x = xPosition;
    localPosition.y = yPosition;
    this._rootWidget.transform.localPosition = localPosition;
  }

  public void MoveTo(float xPosition, float yPosition, float tXPosition, float tYPosition, float duration)
  {
    this.SetPosition(xPosition, yPosition);
    Vector3 localPosition = this._rootWidget.transform.localPosition;
    localPosition.x = tXPosition;
    localPosition.y = tYPosition;
    TweenPosition.Begin(this._rootWidget.gameObject, duration, localPosition);
  }
}
