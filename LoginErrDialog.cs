﻿// Decompiled with JetBrains decompiler
// Type: LoginErrDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using I2.Loc;
using System;
using System.Collections;
using UI;

public class LoginErrDialog : Popup
{
  private const int BAN_REASON_NORMAL = 1;
  private const int BAN_REASON_ROBOT = 2;
  private const int BAN_REASON_REFUNDS = 3;
  private const int BAN_REASON_BUY_RESOURCES = 4;
  private const int BAN_REASON_OTHER_PAY = 5;
  private const int BAN_REASON_IVALID_WORDS = 6;
  private const int BAN_REASON_SELL_RESOURCES = 98;
  private const int BAN_REASON_AUTO_BAN = 99;
  public UILabel _description;
  private LoginErrDialog.Parameter param;
  private bool isCalledBindingInfo;

  private void RequestUserBindingInfo(System.Action action)
  {
    if (this.isCalledBindingInfo)
    {
      action();
    }
    else
    {
      this.isCalledBindingInfo = true;
      Hashtable postData = new Hashtable();
      postData.Add((object) "fpid", (object) AccountManager.Instance.AccountId);
      if (!string.IsNullOrEmpty(AccountManager.Instance.FunplusID))
        postData[(object) "fpid"] = (object) AccountManager.Instance.FunplusID;
      MessageHub.inst.GetPortByAction("call:account").SendLoader(postData, (System.Action<bool, object>) ((ret, orgData) =>
      {
        if (!ret)
          return;
        AccountManager.Instance.ClearAccounts();
        Hashtable hashtable = orgData as Hashtable;
        for (int index = 0; index < 5; ++index)
        {
          string str1 = "account_name_" + (object) (index + 1);
          string str2 = "account_type_" + (object) (index + 1);
          string str3 = "account_mtime_" + (object) (index + 1);
          if (hashtable.ContainsKey((object) str1) && hashtable[(object) str1] != null && (hashtable.ContainsKey((object) str2) && hashtable[(object) str2] != null))
          {
            string id = hashtable[(object) str1].ToString();
            string type = hashtable[(object) str2].ToString();
            int result = 0;
            if (hashtable[(object) str3] != null)
              int.TryParse(hashtable[(object) str3].ToString(), out result);
            AccountManager.Instance.AddAccount(id, type, result);
          }
        }
        action();
      }), true, false);
    }
  }

  public void OnStartNewGamePressed()
  {
    this.RequestUserBindingInfo((System.Action) (() =>
    {
      if (AccountManager.Instance.IsNotBindAccount)
        Utils.ShowWarning(ScriptLocalization.Get("account_warning_not_bound_switch_description", true), ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.AgainConfirm), (string) null);
      else
        Utils.ShowWarning(ScriptLocalization.Get("account_warning_bound_new_game_description", true), ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.ConfirmSignupGame), (string) null);
    }));
  }

  private void AgainConfirm()
  {
    Utils.ShowWarning(ScriptLocalization.Get("account_warning_not_bound_reset_description", true), ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.ConfirmSignupGame), (string) null);
  }

  private void ConfirmSignupGame()
  {
    AccountManager.Instance.Signup();
    this.CloseSelf();
  }

  public void OnSwitchAccountPressed()
  {
    this.RequestUserBindingInfo((System.Action) (() => UIManager.inst.OpenPopup("Account/AccountSwitchPopup", (Popup.PopupParameter) null)));
  }

  public void OnContactServiePressed()
  {
    FunplusHelpshift.Instance.ShowFAQs();
  }

  private void CloseSelf()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void Init()
  {
    LoginErrDialog.LoginErrLog(nameof (Init));
    this._description.text = this.GetDescription(this.param);
  }

  private string GetDescription(LoginErrDialog.Parameter p)
  {
    if (this.param == null)
      return string.Empty;
    DateTime dateTime = Utils.ServerTime2DateTime((long) p.timestamp);
    switch (p.reason)
    {
      case 1:
        return Utils.XLAT(p.timestamp >= 0 ? "account_banned_temporary_activity_description" : "account_banned_permanent_abnormal_description", (object) "0", (object) p.username, (object) "1", (object) dateTime.Year.ToString(), (object) "2", (object) dateTime.Month.ToString(), (object) "3", (object) dateTime.Day.ToString(), (object) "4", (object) string.Format("{0:00}:{1:00}", (object) dateTime.Hour, (object) dateTime.Minute));
      case 2:
        return Utils.XLAT(p.timestamp >= 0 ? "account_banned_temporary_software_description" : "account_banned_permanent_software_description", (object) "0", (object) p.username, (object) "1", (object) dateTime.Year.ToString(), (object) "2", (object) dateTime.Month.ToString(), (object) "3", (object) dateTime.Day.ToString(), (object) "4", (object) string.Format("{0:00}:{1:00}", (object) dateTime.Hour, (object) dateTime.Minute));
      case 3:
        return Utils.XLAT(p.timestamp >= 0 ? "account_banned_temporary_gains_description" : "account_banned_permanent_gains_description", (object) "0", (object) p.username, (object) "1", (object) dateTime.Year.ToString(), (object) "2", (object) dateTime.Month.ToString(), (object) "3", (object) dateTime.Day.ToString(), (object) "4", (object) string.Format("{0:00}:{1:00}", (object) dateTime.Hour, (object) dateTime.Minute));
      case 4:
        return Utils.XLAT(p.timestamp >= 0 ? "account_banned_temporary_resource_buying_description" : "account_banned_permanent_resource_buying_description", (object) "0", (object) p.username, (object) "1", (object) dateTime.Year.ToString(), (object) "2", (object) dateTime.Month.ToString(), (object) "3", (object) dateTime.Day.ToString(), (object) "4", (object) string.Format("{0:00}:{1:00}", (object) dateTime.Hour, (object) dateTime.Minute));
      case 5:
        return Utils.XLAT(p.timestamp >= 0 ? "account_banned_temporary_puppet_description" : "account_banned_permanent_puppet_description", (object) "0", (object) p.username, (object) "1", (object) dateTime.Year.ToString(), (object) "2", (object) dateTime.Month.ToString(), (object) "3", (object) dateTime.Day.ToString(), (object) "4", (object) string.Format("{0:00}:{1:00}", (object) dateTime.Hour, (object) dateTime.Minute));
      case 6:
        return Utils.XLAT(p.timestamp >= 0 ? "account_banned_permanent_speech_description" : "account_banned_permanent_speech_description", (object) "0", (object) p.username, (object) "1", (object) dateTime.Year.ToString(), (object) "2", (object) dateTime.Month.ToString(), (object) "3", (object) dateTime.Day.ToString(), (object) "4", (object) string.Format("{0:00}:{1:00}", (object) dateTime.Hour, (object) dateTime.Minute));
      case 98:
        return Utils.XLAT(p.timestamp >= 0 ? "account_banned_temporary_resource_selling_description" : "account_banned_permanent_resource_selling_description", (object) "0", (object) p.username, (object) "1", (object) dateTime.Year.ToString(), (object) "2", (object) dateTime.Month.ToString(), (object) "3", (object) dateTime.Day.ToString(), (object) "4", (object) string.Format("{0:00}:{1:00}", (object) dateTime.Hour, (object) dateTime.Minute));
      case 99:
        return Utils.XLAT(p.timestamp >= 0 ? "account_banned_temporary_activity_description" : "account_banned_permanent_abnormal_description", (object) "0", (object) p.username, (object) "1", (object) dateTime.Year.ToString(), (object) "2", (object) dateTime.Month.ToString(), (object) "3", (object) dateTime.Day.ToString(), (object) "4", (object) string.Format("{0:00}:{1:00}", (object) dateTime.Hour, (object) dateTime.Minute));
      default:
        return string.Empty;
    }
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.param = orgParam as LoginErrDialog.Parameter;
    this.Init();
  }

  public static void LoginErrLog(string msg)
  {
  }

  public class Parameter : Popup.PopupParameter
  {
    public string username;
    public int reason;
    public int timestamp;
  }
}
