﻿// Decompiled with JetBrains decompiler
// Type: ArmyUnitView_Dragon
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ArmyUnitView_Dragon : ArmyUnitView
{
  private DragonView _dragonView;
  private SkillEffectPlayer _effect;
  public GameObject dragonicon;

  public override void Setup(ArmyFormationData inFormationData, ArmyUnitView.Param param)
  {
    base.Setup(inFormationData, param);
    this.dragonicon.SetActive(false);
    if (this._dragonView != null)
      return;
    DragonViewData dragonViewData = DragonUtils.GetDragonViewData((int) param.dragonTendency, param.dragonLevel, 0);
    this._dragonView = new DragonView();
    this._dragonView.Mount(this.transform);
    this._dragonView.LoadAsync((SpriteViewData) dragonViewData, true, (System.Action<bool, UnityEngine.Object>) ((ret, asset) =>
    {
      if (!ret)
        return;
      this.SetRenderer(29999, "March");
      this._dragonView.SetPosition(Vector3.zero);
      this._dragonView.SetScale(new Vector3(0.3f, 0.3f, 0.3f));
      this._dragonView.SetAlpha(0.0f);
      this._dragonView.SetModelOffset(0.0f, 1f, 0.0f);
      if (this.isHide)
        return;
      this._FadeIn(0.0f);
    }));
  }

  public override void Rotate(Vector3 dirV3)
  {
    if (this._dragonView == null)
      return;
    this._data._dirV3 = dirV3;
    this._data._dirV2 = MarchViewControler.CalcVector3(this._data._dirV3);
    this._dragonView.RotateInKingdom(dirV3);
  }

  public override void SetRenderer(int order, string layer)
  {
    if (this._dragonView != null)
    {
      this._dragonView.SetRendererLayer("Actor");
      if ((UnityEngine.Object) this._dragonView.MainRenderer != (UnityEngine.Object) null)
      {
        this._dragonView.MainRenderer.sortingLayerName = layer;
        this._dragonView.MainRenderer.sortingOrder = order + 50;
      }
    }
    SpriteRenderer[] componentsInChildren = this.dragonicon.GetComponentsInChildren<SpriteRenderer>(true);
    for (int index = 0; index < componentsInChildren.Length; ++index)
    {
      componentsInChildren[index].sortingLayerName = layer;
      componentsInChildren[index].sortingOrder = order + 50 + index;
    }
  }

  public override void Attack(Vector3 attackLocalPosition, Vector3 attackCenter)
  {
    if (this._dragonView == null)
      return;
    this.SetRenderer(29999, "March");
    this._data._isAttackAfterMoving = true;
    this._data._isFadeOutAfterMoving = false;
    TweenScale.Begin(this.gameObject, 1f, Vector3.one * this._data.formationData.unitAttackScale);
    this.ResetAlpha();
    this.LocalMoveTo(attackLocalPosition, attackCenter, 0.0f);
  }

  protected override void AttackAnim()
  {
    if (this._dragonView == null || this._data == null)
      return;
    SkillEffectData data = new SkillEffectData();
    data.animation = "attack_common";
    data.sourceVfx = "Prefab/VFX/SkillEffect/fx_dragon_fire";
    data.sourceEventId = 1;
    data.targetVfx = "Prefab/Dragon/demo_dragon/TargetVfx";
    data.targetEventId = 2;
    data.faceTarget = true;
    data.time = -1f;
    data.faceTarget = true;
    if (this._effect == null)
      this._effect = new SkillEffectPlayer();
    this._effect.SetData(data);
    this._effect.Play((SpriteView) this._dragonView, this.transform.position + this._data.actionCenterLocalPosition);
  }

  protected override void MoveAnim()
  {
    if (this._dragonView != null)
      this._dragonView.PlayAnimation("fly", 1f);
    if (this._effect == null)
      return;
    this._effect.Stop(true);
  }

  public override void Dispose()
  {
    base.Dispose();
    if (this._dragonView == null)
      return;
    this._dragonView.Dispose();
    this._dragonView = (DragonView) null;
  }

  protected override void ResetScale(float scale)
  {
    this.transform.localScale = Vector3.one * scale;
  }

  private void OnDrawGizmos()
  {
  }

  protected override void _FadeIn(float delayTime = 0.0f)
  {
    if (this._dragonView == null || (UnityEngine.Object) this._dragonView.MainRenderer == (UnityEngine.Object) null)
      return;
    iTween.FadeTo(this._dragonView.MainRenderer.gameObject, iTween.Hash((object) "alpha", (object) 1, (object) "time", (object) 0.3, (object) "delay", (object) delayTime, (object) "includechildren", (object) false));
  }

  protected override void _FadeOut(float delayTime = 0.0f)
  {
    if (this._dragonView == null || (UnityEngine.Object) this._dragonView.MainRenderer == (UnityEngine.Object) null)
      return;
    iTween.FadeTo(this._dragonView.MainRenderer.gameObject, iTween.Hash((object) "alpha", (object) 0, (object) "time", (object) 0.3, (object) "delay", (object) delayTime, (object) "includechildren", (object) false));
  }

  protected override void ResetAlpha()
  {
    if (this._dragonView == null || (UnityEngine.Object) this._dragonView.MainRenderer == (UnityEngine.Object) null)
      return;
    iTween.Stop(this._dragonView.MainRenderer.gameObject, true);
    this._dragonView.SetAlpha(1f);
  }

  public override void SetLOD(ArmyView.LODLEVEL lod)
  {
    if (this.curLod >= lod)
      return;
    this.curLod = lod;
    switch (lod)
    {
      case ArmyView.LODLEVEL.LOD1:
        if (this._dragonView == null)
          break;
        this._dragonView.Dispose();
        this._dragonView = (DragonView) null;
        this.dragonicon.SetActive(true);
        break;
      case ArmyView.LODLEVEL.LOD2:
        if (this._dragonView != null)
        {
          this._dragonView.Dispose();
          this._dragonView = (DragonView) null;
        }
        this.dragonicon.SetActive(false);
        break;
    }
  }
}
