﻿// Decompiled with JetBrains decompiler
// Type: WonderFlagManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class WonderFlagManager
{
  private Dictionary<int, int> _selection = new Dictionary<int, int>();
  private Dictionary<int, int> _mapping = new Dictionary<int, int>();
  private static WonderFlagManager _instance;

  public static WonderFlagManager Instance
  {
    get
    {
      if (WonderFlagManager._instance == null)
        WonderFlagManager._instance = new WonderFlagManager();
      return WonderFlagManager._instance;
    }
  }

  public bool ContainsSelectFlag(int flag)
  {
    return this._selection.ContainsKey(flag);
  }

  public int GetMappedFlag(int flag)
  {
    int num;
    if (this._mapping.TryGetValue(flag, out num))
      return num;
    return flag;
  }

  public void Parse(object orgData)
  {
    this.Clear();
    Hashtable hashtable1 = orgData as Hashtable;
    if (hashtable1 == null)
      return;
    Hashtable hashtable2 = hashtable1[(object) "select_flag"] as Hashtable;
    if (hashtable2 != null)
    {
      ArrayList arrayList = hashtable2[(object) "flag"] as ArrayList;
      if (arrayList != null)
      {
        for (int index = 0; index < arrayList.Count; ++index)
        {
          int result = 0;
          if (int.TryParse(arrayList[index].ToString(), out result))
            this._selection[result] = result;
        }
      }
    }
    Hashtable hashtable3 = hashtable1[(object) "see_flag"] as Hashtable;
    if (hashtable3 == null)
      return;
    Hashtable hashtable4 = hashtable3[(object) "flag"] as Hashtable;
    if (hashtable4 == null)
      return;
    IDictionaryEnumerator enumerator = hashtable4.GetEnumerator();
    while (enumerator.MoveNext())
    {
      int result1 = 0;
      int result2 = 0;
      if (int.TryParse(enumerator.Key.ToString(), out result1) && int.TryParse(enumerator.Value.ToString(), out result2))
        this._mapping[result1] = result2;
    }
  }

  public void Clear()
  {
    this._selection.Clear();
    this._mapping.Clear();
  }
}
