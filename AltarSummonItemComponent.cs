﻿// Decompiled with JetBrains decompiler
// Type: AltarSummonItemComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AltarSummonItemComponent : ComponentRenderBase
{
  [SerializeField]
  private UITexture _icon;
  [SerializeField]
  private UILabel _itemCount;
  [SerializeField]
  private GameObject _content;
  [HideInInspector]
  public int id;
  public System.Action onItemClick;
  private int rewardId;

  public void OnClick()
  {
    if (this.onItemClick == null)
      return;
    this.onItemClick();
  }

  public override void Init()
  {
    base.Init();
    AltarSummonItemComponentData data1 = this.data as AltarSummonItemComponentData;
    if (data1 == null)
      return;
    this.rewardId = data1.rewardId;
    ConfigAltarSummonRewardInfo data2 = ConfigManager.inst.DB_ConfigAltarSummonRewardMain.GetData(this.rewardId);
    if (data2 == null)
      this.ShowEmpty();
    else
      this.ShowItem(data2);
  }

  private void ShowEmpty()
  {
    this._content.SetActive(false);
    BuilderFactory.Instance.Release((UIWidget) this._icon);
  }

  private void ShowItem(ConfigAltarSummonRewardInfo rewardConfig)
  {
    this._content.SetActive(true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this._icon, ConfigManager.inst.DB_Items.GetItem(rewardConfig.itemId).ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this._itemCount.text = rewardConfig.itemCount.ToString();
    this.AdjustLocalEulerAnges();
  }

  private void AdjustLocalEulerAnges()
  {
    Transform transform = this._icon.transform;
    for (int index = 0; index < transform.childCount; ++index)
      transform.GetChild(index).localEulerAngles = Vector3.zero;
  }

  public override void Dispose()
  {
    base.Dispose();
  }
}
