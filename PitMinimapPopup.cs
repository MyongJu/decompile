﻿// Decompiled with JetBrains decompiler
// Type: PitMinimapPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class PitMinimapPopup : Popup
{
  private static bool _ShowAllianceMember = true;
  [SerializeField]
  private float _MoveAnimationTime = 0.6f;
  [SerializeField]
  private float _MoveAnimationScale = 1.5f;
  private List<GameObject> _AllAllianceMemeberTag = new List<GameObject>();
  [SerializeField]
  private GameObject _RootAlliance;
  [SerializeField]
  private UIToggle _ToggleShowAlliance;
  [SerializeField]
  private GameObject _RootResourceLevel;
  [SerializeField]
  private UIToggle _ToggleShowresourceLevel;
  [SerializeField]
  private UITexture _TextureMap;
  [SerializeField]
  private GameObject _TagCurrentPosition;
  [SerializeField]
  private PitMiniMapMaskItem _TagPlayerCity;
  [SerializeField]
  private PitMiniMapMaskItem _TagAllianceLeaderPosition;
  [SerializeField]
  private PitMiniMapMaskItem _TagAllianceMemberPosition;
  [SerializeField]
  private UIScrollView _ScrollView;
  private static bool _ShowResourceLevel;
  private bool _MovingToTargetPosition;

  public Coordinate CurrentKXY
  {
    get
    {
      return PVPSystem.Instance.Map.CurrentKXY;
    }
  }

  public Coordinate TopLeftWorldCoordianate
  {
    get
    {
      return new Coordinate(this.CurrentKXY.K, 0, 0);
    }
  }

  public int KingdomWidth
  {
    get
    {
      return (int) PitMapData.MapData.TilesPerKingdom.x;
    }
  }

  public int KingdomHeight
  {
    get
    {
      return (int) PitMapData.MapData.TilesPerKingdom.y;
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
    if (PlayerData.inst.allianceId == 0L)
      return;
    Hashtable postData = new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) PlayerData.inst.allianceId
      }
    };
    RequestManager.inst.SendRequest("Alliance:getAllianceMemberInfo", postData, (System.Action<bool, object>) ((result, data) => this.UpdateUI()), true);
  }

  private void UpdateUI()
  {
    this._ToggleShowresourceLevel.value = PitMinimapPopup._ShowResourceLevel;
    this._ToggleShowAlliance.value = PitMinimapPopup._ShowAllianceMember;
    this.UpdateAllianceMemberTag(PitMinimapPopup._ShowAllianceMember);
    this.UpdatePlayerPositionTag();
    this.UpdateCurrentPositionTag();
    this.StartCoroutine(this.MoveCurrentPositionToTheCenter(0.0f, 1f, (System.Action) null));
  }

  private void UpdateAllianceMemberTag(bool show)
  {
    using (List<GameObject>.Enumerator enumerator = this._AllAllianceMemeberTag.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
          UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this._AllAllianceMemeberTag.Clear();
    this._TagAllianceMemberPosition.gameObject.SetActive(false);
    this._TagAllianceLeaderPosition.gameObject.SetActive(false);
    if (!show)
      return;
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
    if (allianceData == null)
      return;
    Dictionary<long, AllianceMemberData>.Enumerator enumerator1 = allianceData.members.datas.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      AllianceMemberData allianceMemberData = enumerator1.Current.Value;
      if (allianceMemberData.uid != PlayerData.inst.uid)
      {
        CityData byUid = DBManager.inst.DB_City.GetByUid(allianceMemberData.uid);
        if (byUid != null && byUid.cityLocation.K == this.CurrentKXY.K)
        {
          switch (allianceMemberData.title)
          {
            case 5:
            case 6:
              this._TagAllianceLeaderPosition.gameObject.SetActive(true);
              this._TagAllianceLeaderPosition.transform.localPosition = this.ConvertCoordinateToTagPosition(byUid.cityLocation);
              this._TagAllianceLeaderPosition.SetData(byUid.cityLocation);
              continue;
            default:
              GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._TagAllianceMemberPosition.gameObject);
              gameObject.transform.SetParent(this._TagAllianceMemberPosition.transform.parent);
              gameObject.SetActive(true);
              gameObject.transform.localScale = Vector3.one;
              gameObject.transform.localPosition = this.ConvertCoordinateToTagPosition(byUid.cityLocation);
              gameObject.GetComponent<PitMiniMapMaskItem>().SetData(byUid.cityLocation);
              this._AllAllianceMemeberTag.Add(gameObject);
              continue;
          }
        }
      }
    }
  }

  private void UpdatePlayerPositionTag()
  {
    this._TagPlayerCity.transform.localPosition = this.ConvertCoordinateToTagPosition(PlayerData.inst.CityData.Location);
    this._TagPlayerCity.SetData(PlayerData.inst.CityData.Location);
  }

  private void UpdateCurrentPositionTag()
  {
    this._TagCurrentPosition.transform.localPosition = this.ConvertCoordinateToTagPosition(PVPSystem.Instance.Map.CurrentKXY);
  }

  [DebuggerHidden]
  private IEnumerator MoveCurrentPositionToTheCenter(float animationTime = 0.0f, float targetScale = 1f, System.Action callback = null)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PitMinimapPopup.\u003CMoveCurrentPositionToTheCenter\u003Ec__Iterator90()
    {
      targetScale = targetScale,
      animationTime = animationTime,
      callback = callback,
      \u003C\u0024\u003EtargetScale = targetScale,
      \u003C\u0024\u003EanimationTime = animationTime,
      \u003C\u0024\u003Ecallback = callback,
      \u003C\u003Ef__this = this
    };
  }

  private Vector3 ConvertCoordinateToTagPosition(Coordinate coordinate)
  {
    Coordinate worldCoordianate = this.TopLeftWorldCoordianate;
    worldCoordianate.X = worldCoordianate.Y = 0;
    return new Vector3((float) (-this._TextureMap.width / 2) + (float) (coordinate.X - worldCoordianate.X) / (float) this.KingdomWidth * (float) this._TextureMap.width, (float) (-this._TextureMap.height / 2) + (float) (1.0 - (double) (coordinate.Y - worldCoordianate.Y) / (double) this.KingdomHeight) * (float) this._TextureMap.height);
  }

  public void OnToggleShowAllianceValueChanged()
  {
    PitMinimapPopup._ShowAllianceMember = this._ToggleShowAlliance.value;
    this._RootAlliance.SetActive(PitMinimapPopup._ShowAllianceMember);
    this.UpdateAllianceMemberTag(PitMinimapPopup._ShowAllianceMember);
  }

  public void OnToggleShowResourceLevelValueChanged()
  {
    PitMinimapPopup._ShowResourceLevel = this._ToggleShowresourceLevel.value;
    this._RootResourceLevel.SetActive(PitMinimapPopup._ShowResourceLevel);
  }

  public void OnButtonWorldClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnMapPressed()
  {
    if (this._MovingToTargetPosition)
      return;
    Vector3 vector3 = this._TextureMap.transform.worldToLocalMatrix.MultiplyPoint(UIManager.inst.ui2DCamera.ScreenToWorldPoint(new Vector3(UICamera.currentTouch.pos.x, UICamera.currentTouch.pos.y))) - new Vector3((float) -this._TextureMap.width / 2f, (float) -this._TextureMap.height / 2f, 0.0f);
    Vector2 vector2 = new Vector2(vector3.x / (float) this._TextureMap.width, vector3.y / (float) this._TextureMap.height);
    if ((double) vector2.x <= 0.0 || (double) vector2.y <= 0.0 || ((double) vector2.x >= 1.0 || (double) vector2.y >= 1.0))
      return;
    Coordinate worldCoordianate = this.TopLeftWorldCoordianate;
    Coordinate clickedCoordinate;
    clickedCoordinate.K = this.CurrentKXY.K;
    clickedCoordinate.X = worldCoordianate.X + (int) ((double) vector2.x * (double) this.KingdomWidth);
    clickedCoordinate.Y = worldCoordianate.Y + (int) ((1.0 - (double) vector2.y) * (double) this.KingdomHeight);
    this._TagCurrentPosition.transform.localPosition = this.ConvertCoordinateToTagPosition(clickedCoordinate);
    this.StartCoroutine(this.MoveCurrentPositionToTheCenter(this._MoveAnimationTime, this._MoveAnimationScale, (System.Action) (() =>
    {
      PVPSystem.Instance.Map.GotoLocation(clickedCoordinate, false);
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    })));
  }
}
