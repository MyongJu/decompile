﻿// Decompiled with JetBrains decompiler
// Type: ModEntrancePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class ModEntrancePopup : Popup
{
  public UIButton help;
  public UIButton mod;
  public UIButton close;
  [Header("Localize")]
  public UILabel titleTxt;
  public UILabel helpTxt;
  public UILabel modTxt;
  private bool inited;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    EventDelegate.Add(this.close.onClick, new EventDelegate.Callback(this.OnClosePopup));
    EventDelegate.Add(this.help.onClick, new EventDelegate.Callback(this.OnClickHelp));
    EventDelegate.Add(this.mod.onClick, new EventDelegate.Callback(this.OnEnterMod));
    if (this.inited)
      return;
    this._OnceInit();
    this.inited = true;
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    EventDelegate.Remove(this.close.onClick, new EventDelegate.Callback(this.OnClosePopup));
    EventDelegate.Remove(this.help.onClick, new EventDelegate.Callback(this.OnClickHelp));
    EventDelegate.Remove(this.mod.onClick, new EventDelegate.Callback(this.OnEnterMod));
  }

  private void OnEnterMod()
  {
    UIManager.inst.OpenPopup("Mod/ModHelpPopup", (Popup.PopupParameter) null);
  }

  private void OnClickHelp()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
    AccountManager.Instance.ShowFASQs();
  }

  private void OnClosePopup()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  private void _OnceInit()
  {
    this.titleTxt.text = Utils.XLAT("settings_uppercase_game_help");
    this.helpTxt.text = Utils.XLAT("settings_game_help_uppercase_contact_official");
    this.modTxt.text = Utils.XLAT("settings_game_help_uppercase_contact_mod");
  }
}
