﻿// Decompiled with JetBrains decompiler
// Type: FunplusFacebookAgent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using System.Collections.Generic;

public class FunplusFacebookAgent : FunplusFacebook.IDelegate
{
  public event System.Action OnFetchUserDataSuccess;

  public FunplusSocialUser UserData { get; private set; }

  public void OnAskFriendsPermission(bool result)
  {
  }

  public void OnAskPublishPermission(bool result)
  {
  }

  public void OnGetUserDataSuccess(FunplusSocialUser user)
  {
    this.UserData = user;
    if (this.OnFetchUserDataSuccess == null)
      return;
    this.OnFetchUserDataSuccess();
  }

  public void OnGetUserDataError(FunplusError error)
  {
  }

  public void OnGetGameFriendsSuccess(List<FunplusFBFriend> friends)
  {
  }

  public void OnGetGameFriendsError(FunplusError error)
  {
  }

  public void OnGetGameInvitableFriendsSuccess(List<FunplusFBFriend> friends)
  {
  }

  public void OnGetGameInvitableFriendsError(FunplusError error)
  {
  }

  public void OnSendGameRequestSuccess(string result)
  {
  }

  public void OnSendGameRequestError(FunplusError error)
  {
  }

  public void OnShareSuccess(string result)
  {
  }

  public void OnShareError(FunplusError error)
  {
  }

  public void OnOpenGraphStoryShareSuccess(string result)
  {
  }

  public void OnOpenGraphStoryShareError(FunplusError error)
  {
  }
}
