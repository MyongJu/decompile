﻿// Decompiled with JetBrains decompiler
// Type: MailNoItemDisplay
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class MailNoItemDisplay : MonoBehaviour
{
  public UILabel label;
  public UIButton modButton;
  public GameObject BG;
  public UILabel modButtonTxt;

  protected void Awake()
  {
    EventDelegate.Add(this.modButton.onClick, new EventDelegate.Callback(this.OnModButton));
  }

  public void Show(MailCategory catgory)
  {
    this.modButtonTxt.text = Utils.XLAT("settings_game_help_mod_contact");
    this.gameObject.SetActive(true);
    if (catgory == MailCategory.Mod)
    {
      this.modButton.gameObject.SetActive(true);
      this.label.gameObject.SetActive(false);
      this.BG.gameObject.SetActive(false);
    }
    else
    {
      this.modButton.gameObject.SetActive(false);
      this.label.gameObject.SetActive(true);
      this.BG.gameObject.SetActive(true);
    }
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  private void OnModButton()
  {
    UIManager.inst.OpenPopup("Mod/ModHelpPopup", (Popup.PopupParameter) null);
  }
}
