﻿// Decompiled with JetBrains decompiler
// Type: MailListRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Text.RegularExpressions;
using UnityEngine;

public class MailListRender : MonoBehaviour
{
  private const int messageDisplayCount = 45;
  private const string ICON_PATH = "Texture/Hero/Portrait_Icon_{0}";
  private const string OTEHR_ICON_PATH = "Texture/Mail/icon_mail_{0}";
  public UILabel mailTitle;
  public UILabel contentLable;
  public UILabel mailTime;
  public GameObject giftIcon;
  public GameObject favStar;
  public GameObject favClickArea;
  public GameObject unReadGameOjbect;
  public UISprite select;
  public UITexture iconTexture;
  public MailRenderData data;
  public MailController controller;
  public MailMainDlg parentView;
  public System.Action<MailRenderData> EditorClick;

  public AbstractMailEntry mail { get; protected set; }

  public bool visible
  {
    get
    {
      return this.gameObject.activeSelf;
    }
  }

  public bool Selected
  {
    get
    {
      return this.data.isSelected;
    }
    set
    {
      this.data.isSelected = value;
      this.select.gameObject.SetActive(value);
    }
  }

  public void Show()
  {
    NGUITools.SetActive(this.gameObject, true);
    this.Selected = this.data.isSelected;
  }

  public void Hide()
  {
    this.favStar.gameObject.SetActive(false);
    this.select.gameObject.SetActive(false);
    NGUITools.SetActive(this.gameObject, false);
    BuilderFactory.Instance.Release((UIWidget) this.iconTexture);
  }

  public void OnClickToRead()
  {
    MailType type = this.mail.type;
    switch (type)
    {
      case MailType.MAIL_TYPE_SYSTEM_RECOMMEND_ALLIANCE:
      case MailType.MAIL_TYPE_SYSTEM_USER_SENT_ATTACHMENT:
      case MailType.MAIL_TYPE_SYSTEM_DUNGEON_RANKING_REWARD:
      case MailType.MAIL_TYPE_SYSTEM_DK_ARENA_RANKING_REWARD:
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_END:
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_WORSHIPPED_REWARD:
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_KINGS_REWARD:
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_START:
      case MailType.MAIL_TYPE_SYSTEM_ABYSS_RESULT_NOTICE:
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_KINGS_START:
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_COUNTDOWN_REWARD:
      case MailType.MAIL_TYPE_WORLD_BOSS_START:
      case MailType.MAIL_TYPE_WORLD_BOSS_END:
      case MailType.MAIL_TYPE_WORLD_BOSS_KILLED:
      case MailType.MAIL_TYPE_WORLD_BOSS_KILL_REWARD:
      case MailType.MAIL_TYPE_SYSTEM_WONDER_DISMISS:
      case MailType.MAIL_TYPE_ALLIANCE_AC_REWARD:
label_5:
        (this.mail as SystemMessage).Display();
        break;
      default:
        switch (type - 49)
        {
          case MailType.MAIL_TYPE_NORMAL:
          case MailType.MAIL_TYPE_PVP_BATTLE_REPORT:
          case MailType.MAIL_TYPE_PVP_SCOUT_REPORT:
            goto label_5;
          default:
            if (type != MailType.MAIL_TYPE_NORMAL)
            {
              if (type != MailType.MAIL_TYPE_SYSTEM_NOTICE && type != MailType.MAIL_TYPE_SYSTEM_INTEGRAL_REWARD && (type != MailType.MAIL_TYPE_SYSTEM_VERSION && type != MailType.MAIL_TYPE_SYSTEM_NOTICE_MULTI_LANG))
              {
                (this.mail as WarMessage).Display();
                return;
              }
              goto label_5;
            }
            else
            {
              (this.mail as PlayerMessage).Display();
              return;
            }
        }
    }
  }

  public void OnSelectChange()
  {
    this.Selected = !this.Selected;
    if (this.EditorClick == null)
      return;
    this.EditorClick(this.data);
  }

  public void OnStarClick()
  {
    this.mail.isFavorite = !this.mail.isFavorite;
    this.favStar.SetActive(this.mail.isFavorite);
    this.controller.TagFavToMail(this.mail.mailID, this.mail.isFavorite, (int) this.mail.category);
  }

  public void Refresh()
  {
    this.OnMailUpdated();
  }

  protected void OnMailUpdated()
  {
    this.ConfigTitle();
    this.ConfitContent();
    this.ConfigReadIcon();
    this.ConfigFavIcon();
    this.ConfigTime();
    this.ConfigIcon();
    this.ConfigGift();
    this.ConfigBackGrondImage();
    if (this.data == null)
      return;
    this.Selected = this.data.isSelected;
  }

  private void ConfigTitle()
  {
    this.mailTitle.text = this.mail.GetTitleString();
  }

  private void ConfitContent()
  {
    string str = this.mail.GetBodyString();
    if (!Regex.IsMatch(str, "\\[\\b\\w{3}\\b]"))
      str = Utils.RemoveBBCode(str);
    this.contentLable.text = str;
    EmojiManager.Instance.ProcessEmojiLabel(this.contentLable);
  }

  private void ConfigReadIcon()
  {
    this.unReadGameOjbect.SetActive(!this.mail.isRead);
  }

  private void ConfigFavIcon()
  {
    this.favStar.SetActive(this.mail.isFavorite && this.mail.category != MailCategory.System);
    this.favClickArea.SetActive(this.mail.category != MailCategory.System);
  }

  private void ConfigIcon()
  {
    CustomIconLoader.Instance.requestCustomIcon(this.iconTexture, this.mail.GetPotraitPath(), this.mail.GetCoustomIcon(), false);
    LordTitlePayload.Instance.ApplyUserAvator(this.iconTexture, this.mail.GetLordTitle(), 1);
  }

  private void ConfigGift()
  {
    this.giftIcon.SetActive(this.mail.attachment_status == 1);
  }

  private void ConfigTime()
  {
    this.mailTime.text = Utils.FormatTimeForMail(this.mail.timeAsInt);
  }

  private void ConfigBackGrondImage()
  {
  }

  public void SetMailEntry(AbstractMailEntry inMail)
  {
    this.mail = this.controller.mailsById[inMail.mailID];
    this.OnMailUpdated();
  }

  public void OnEditorClick()
  {
    this.Selected = !this.Selected;
    if (this.EditorClick == null)
      return;
    this.EditorClick(this.data);
  }
}
