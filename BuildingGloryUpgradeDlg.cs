﻿// Decompiled with JetBrains decompiler
// Type: BuildingGloryUpgradeDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingGloryUpgradeDlg : UI.Dialog
{
  private Dictionary<int, BuildingGlorySlot> _itemDict = new Dictionary<int, BuildingGlorySlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UIWidget _pivotWidget;
  [SerializeField]
  private UILabel _labelPanelTitle;
  [SerializeField]
  private UILabel _labelCurrentLevel;
  [SerializeField]
  private UILabel _labelNextLevel;
  [SerializeField]
  private UISprite _spriteArrow;
  [SerializeField]
  private UILabel _labelLevelProgress;
  [SerializeField]
  private UILabel _labelLevelMax;
  [SerializeField]
  private UILabel _labelLevelLimit;
  [SerializeField]
  private UITexture _textureButtonProps;
  [SerializeField]
  private UILabel _labelButtonPropsCount;
  [SerializeField]
  private UIButton _buttonUpgradeGlory;
  [SerializeField]
  private UITexture _textureProps;
  [SerializeField]
  private UILabel _labelPropsCount;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UITable _table;
  [SerializeField]
  private GameObject _itemPrefab;
  [SerializeField]
  private GameObject _itemRoot;
  private CityManager.BuildingItem _buildingItem;
  private BuildingGloryMainInfo _buildingGloryMainInfo;
  private Color _buttonPropsCountOriginColor;

  protected bool IsPropsEnough
  {
    get
    {
      return this._buildingGloryMainInfo != null && this._buildingGloryMainInfo.NextBuildingGloryMainInfo != null && this.GetPropsCount(this._buildingGloryMainInfo.NextBuildingGloryMainInfo.reqItemId) >= (long) this._buildingGloryMainInfo.NextBuildingGloryMainInfo.ReqItemCount;
    }
  }

  protected bool IsUpgradeLimit
  {
    get
    {
      return this._buildingItem == null || this._buildingGloryMainInfo == null || !this._buildingGloryMainInfo.IsMaxGlory && this._buildingItem.mLevel < this._buildingGloryMainInfo.NextBuildingGloryMainInfo.ReqBuildingMinLevel;
    }
  }

  protected string PropsName
  {
    get
    {
      if (this._buildingGloryMainInfo != null)
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._buildingGloryMainInfo.reqItemId);
        if (itemStaticInfo != null)
          return itemStaticInfo.LocName;
      }
      return string.Empty;
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    BuildingGloryUpgradeDlg.Parameter parameter = orgParam as BuildingGloryUpgradeDlg.Parameter;
    if (parameter != null)
      this._buildingItem = parameter.buildingItem;
    this._itemPool.Initialize(this._itemPrefab, this._itemRoot);
    this._buttonPropsCountOriginColor = this._labelButtonPropsCount.color;
    CitadelSystem.inst.FocusConstructionBuilding(BuildingController.mSelectedBuildingController.gameObject, this._pivotWidget, 0.0f);
    this.UpdateUI();
    if (this._buildingItem == null)
      return;
    PlayerPrefsEx.SetIntByUid("building_glory_upgrade_" + (object) this._buildingItem.mBuildingId + "_" + (object) this._buildingItem.mLevel, 1);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnGloryHelpBtnPressed()
  {
    UIManager.inst.OpenPopup("HelpPopup", (Popup.PopupParameter) new HelpPopup.Parameter()
    {
      id = "help_construction_prestige",
      descParam = new Dictionary<string, string>()
      {
        {
          "0",
          this.PropsName
        }
      }
    });
  }

  public void GetMorePropsPressed()
  {
    List<IAPStorePackage> availablePackages = IAPStorePackagePayload.Instance.GetAvailablePackages();
    IAPPackageGroupInfo packageGroupInfo = (IAPPackageGroupInfo) null;
    if (availablePackages == null || availablePackages.Count <= 0 || this._buildingGloryMainInfo == null)
      return;
    for (int index = 0; index < availablePackages.Count; ++index)
    {
      if (availablePackages[index].group.internalId == this._buildingGloryMainInfo.iapPackageId)
      {
        packageGroupInfo = availablePackages[index].group;
        break;
      }
    }
    if (packageGroupInfo == null)
      Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
    else
      Utils.ShowIAPStore((UI.Dialog.DialogParameter) new IAPStoreDialog.Parameter()
      {
        groupId = packageGroupInfo.internalId.ToString()
      });
  }

  public void UpgradeBuildingGloryPressed()
  {
    if (this._buildingItem == null)
      return;
    if (this.IsPropsEnough)
    {
      Hashtable postData = new Hashtable();
      postData.Add((object) "uid", (object) PlayerData.inst.uid);
      postData.Add((object) "city_id", (object) this._buildingItem.mCityId);
      postData.Add((object) "building_id", (object) this._buildingItem.mBuildingId);
      int originGloryLevel = this._buildingItem.mGloryLevel;
      long addedPower = 0;
      if (this._buildingGloryMainInfo != null && this._buildingGloryMainInfo.NextBuildingGloryMainInfo != null)
        addedPower = this._buildingGloryMainInfo.NextBuildingGloryMainInfo.power;
      MessageHub.inst.GetPortByAction("City:upgradeGlory").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        AudioManager.Instance.StopAndPlaySound("sfx_city_dragon_skill_upgrade");
        UIManager.inst.OpenPopup("BuildingGlory/BuildingGloryLevelUpPopup", (Popup.PopupParameter) new BuildingGloryLevelUpPopup.Parameter()
        {
          originLevel = originGloryLevel,
          addedPower = addedPower
        });
        if (this._buildingItem != null)
          PlayerPrefsEx.SetIntByUid("building_glory_upgrade_" + (object) this._buildingItem.mBuildingId + "_" + (object) this._buildingItem.mLevel, 0);
        this.UpdateUI();
      }), true);
    }
    else
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      para.Add("0", this.PropsName);
      string str1 = Utils.XLAT("id_uppercase_insufficient_items");
      string withPara = ScriptLocalization.GetWithPara("toast_prestige_insufficient_item", para, true);
      string str2 = Utils.XLAT("event_lucky_draw_get_more_free_button");
      string str3 = Utils.XLAT("id_uppercase_okay");
      UIManager.inst.priorityPopupQueue.OpenPopup("BuildingGlory/CommonChoosePopup", (Popup.PopupParameter) new ChooseConfirmationBox.Parameter()
      {
        title = str1,
        message = withPara,
        yesOrOklabel_left = str2,
        nolabel_right = str3,
        buttonState = (ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT),
        yesOrOkHandler_right = new System.Action(this.GetMorePropsPressed)
      }, 100);
    }
  }

  private void UpdateUI()
  {
    this.ShowGloryContent();
    this.ShowMainContent();
  }

  private void ShowMainContent()
  {
    if (this._buildingItem == null)
      return;
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData(string.Format("{0}_{1}", (object) this._buildingItem.mType, (object) this._buildingItem.mLevel));
    if (data != null)
      this._labelPanelTitle.text = Utils.XLAT(data.Building_LOC_ID);
    if (this._buildingGloryMainInfo == null)
      return;
    NGUITools.SetActive(this._spriteArrow.gameObject, !this._buildingGloryMainInfo.IsMaxGlory);
    NGUITools.SetActive(this._labelCurrentLevel.gameObject, !this._buildingGloryMainInfo.IsMaxGlory);
    NGUITools.SetActive(this._labelNextLevel.gameObject, !this._buildingGloryMainInfo.IsMaxGlory);
    NGUITools.SetActive(this._labelLevelMax.gameObject, this._buildingGloryMainInfo.IsMaxGlory);
    NGUITools.SetActive(this._buttonUpgradeGlory.gameObject, !this._buildingGloryMainInfo.IsMaxGlory);
    UILabel labelCurrentLevel = this._labelCurrentLevel;
    string str1 = string.Format("{0}{1}", (object) Utils.XLAT("id_lv"), (object) this._buildingGloryMainInfo.level.ToString());
    this._labelLevelMax.text = str1;
    string str2 = str1;
    labelCurrentLevel.text = str2;
    if (this._buildingGloryMainInfo.IsMaxGlory)
      this._labelLevelMax.text = string.Format("{0}({1})", (object) this._labelLevelMax.text, (object) Utils.XLAT("id_uppercase_maxed"));
    if (this._buildingGloryMainInfo.NextBuildingGloryMainInfo != null)
    {
      this._labelNextLevel.text = string.Format("{0}{1}", (object) Utils.XLAT("id_lv"), (object) this._buildingGloryMainInfo.NextBuildingGloryMainInfo.level.ToString());
      this._labelLevelLimit.text = ScriptLocalization.GetWithPara("construction_prestige_upgrade_requirement", new Dictionary<string, string>()
      {
        {
          "0",
          this._buildingGloryMainInfo.NextBuildingGloryMainInfo.ReqBuildingMinLevel.ToString()
        }
      }, true);
    }
    NGUITools.SetActive(this._labelLevelLimit.gameObject, this.IsUpgradeLimit);
    this._buttonUpgradeGlory.isEnabled = !this.IsUpgradeLimit;
    this._labelLevelProgress.text = string.Format("{2}{0}/{1}", (object) this._buildingItem.mGloryLevel, (object) this._buildingGloryMainInfo.BuildingGloryMaxLevel, (object) Utils.XLAT("construction_prestige_level"));
    ItemStaticInfo itemStaticInfo = !this._buildingGloryMainInfo.IsMaxGlory ? ConfigManager.inst.DB_Items.GetItem(this._buildingGloryMainInfo.NextBuildingGloryMainInfo.reqItemId) : ConfigManager.inst.DB_Items.GetItem(this._buildingGloryMainInfo.reqItemId);
    if (itemStaticInfo != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureButtonProps, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, true, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureProps, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, true, string.Empty);
      this._labelPropsCount.text = Utils.FormatThousands(this.GetPropsCount(itemStaticInfo.internalId).ToString());
    }
    this._labelButtonPropsCount.text = this._buildingGloryMainInfo.NextBuildingGloryMainInfo == null || this._buildingGloryMainInfo.NextBuildingGloryMainInfo.ReqItemCount <= 0 ? Utils.XLAT("id_uppercase_free") : this._buildingGloryMainInfo.NextBuildingGloryMainInfo.ReqItemCount.ToString();
    this._labelButtonPropsCount.color = !this.IsPropsEnough ? Color.red : this._buttonPropsCountOriginColor;
  }

  private void ShowGloryContent()
  {
    if (this._buildingItem == null)
      return;
    this.ClearData();
    List<BuildingGloryMainInfo> gloryMainInfoList = ConfigManager.inst.DB_BuildingGloryMain.GetBuildingGloryMainInfoList();
    if (gloryMainInfoList != null)
    {
      List<BuildingGloryMainInfo> all = gloryMainInfoList.FindAll((Predicate<BuildingGloryMainInfo>) (x => x.BuildingType == this._buildingItem.mType));
      if (all != null)
      {
        BuildingGloryMainInfo buildingGloryMainInfo = this._buildingGloryMainInfo = all.Find((Predicate<BuildingGloryMainInfo>) (x => x.level == this._buildingItem.mGloryLevel));
        if (buildingGloryMainInfo != null && buildingGloryMainInfo.benefits != null)
        {
          BuildingGloryMainInfo buildingGloryInfo = ConfigManager.inst.DB_BuildingGloryMain.GetNextBuildingGloryInfo(buildingGloryMainInfo.internalId);
          List<Benefits.BenefitValuePair> benefitsPairs = buildingGloryMainInfo.benefits.GetBenefitsPairs();
          List<Benefits.BenefitValuePair> benefitValuePairList = buildingGloryInfo == null ? (List<Benefits.BenefitValuePair>) null : buildingGloryInfo.benefits.GetBenefitsPairs();
          for (int key = 0; key < benefitsPairs.Count; ++key)
          {
            BuildingGlorySlot buildingGlorySlot = benefitValuePairList == null ? this.GenerateSlot(benefitsPairs[key].internalID, benefitsPairs[key].value, 0.0f, true) : this.GenerateSlot(benefitsPairs[key].internalID, benefitsPairs[key].value, benefitValuePairList[key].value, buildingGloryMainInfo.IsMaxGlory);
            this._itemDict.Add(key, buildingGlorySlot);
          }
        }
      }
    }
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, BuildingGlorySlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this._table.repositionNow = true;
    this._table.Reposition();
    this._scrollView.ResetPosition();
  }

  private BuildingGlorySlot GenerateSlot(int benefitId, float curBenefitValue, float nextBenefitValue, bool isMaxGlory)
  {
    GameObject gameObject = this._itemPool.AddChild(this._itemRoot);
    gameObject.SetActive(true);
    BuildingGlorySlot component = gameObject.GetComponent<BuildingGlorySlot>();
    component.SetData(benefitId, curBenefitValue, nextBenefitValue, isMaxGlory);
    return component;
  }

  public long GetPropsCount(int propsId)
  {
    ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(propsId);
    if (consumableItemData != null)
      return (long) consumableItemData.quantity;
    return 0;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public CityManager.BuildingItem buildingItem;
  }
}
