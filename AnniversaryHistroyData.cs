﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryHistroyData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using System.Text;

public class AnniversaryHistroyData : IHistoryData
{
  private string _content = string.Empty;
  private int _time;

  public int StartTime
  {
    get
    {
      return this._time;
    }
  }

  public string Content
  {
    get
    {
      return this._content;
    }
    set
    {
      this._content = value;
    }
  }

  public bool IsAlly { get; set; }

  private string GetFormatTime()
  {
    int num1 = NetServerTime.inst.ServerTimestamp - this.StartTime;
    if (num1 < 60)
      return Utils.XLAT("chat_time_less_than_min");
    int num2 = num1 / 3600 / 24;
    int num3 = num1 / 3600 % 24;
    int num4 = num1 % 3600 / 60;
    return ScriptLocalization.GetWithPara("chat_time_display", new Dictionary<string, string>()
    {
      {
        "d",
        num2 <= 0 ? string.Empty : num2.ToString() + Utils.XLAT("chat_time_day")
      },
      {
        "h",
        num3 <= 0 ? string.Empty : num3.ToString() + Utils.XLAT("chat_time_hour")
      },
      {
        "m",
        num4 < 0 ? string.Empty : num4.ToString() + Utils.XLAT("chat_time_min")
      }
    }, true);
  }

  public bool Parse(string json)
  {
    if (string.IsNullOrEmpty(json))
      return false;
    string[] strArray = json.Split('|');
    if (strArray.Length < 4)
      return false;
    int.TryParse(strArray[0], out this._time);
    int result1 = 0;
    int.TryParse(strArray[1], out result1);
    string str = ScriptLocalization.Get(Encoding.Default.GetString(Convert.FromBase64String(Utils.XLAT(strArray[2]))), true);
    int result2 = 0;
    int.TryParse(strArray[3], out result2);
    string Term = string.Empty;
    AnniversaryChampionMainInfo data = ConfigManager.inst.DB_AnniversaryChampionMain.GetData(result2.ToString() + string.Empty);
    Dictionary<string, string> para = new Dictionary<string, string>();
    switch (result1)
    {
      case 1:
        Term = "event_1_year_tournament_tournament_victory_description";
        para.Add("1", str);
        para.Add("2", data.LocalName);
        this.IsAlly = true;
        break;
      case 2:
        Term = "event_1_year_tournament_tournament_defeat_description";
        para.Add("1", str);
        break;
      case 3:
        Term = "event_1_year_tournament_tournament_defeat_result_description";
        para.Add("0", str);
        para.Add("1", data.LocalName);
        break;
    }
    this.Content = this.GetFormatTime() + " " + ScriptLocalization.GetWithPara(Term, para, true);
    return true;
  }
}
