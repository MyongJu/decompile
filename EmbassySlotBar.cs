﻿// Decompiled with JetBrains decompiler
// Type: EmbassySlotBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class EmbassySlotBar : MonoBehaviour
{
  private bool _detailExpended = true;
  private List<EmbassySlotBarDetail> _details = new List<EmbassySlotBarDetail>();
  public System.Action<long> onSendTroopsHomeClick;
  public System.Action<bool> onExpandMarkClick;
  [SerializeField]
  private EmbassySlotBar.Panel panel;
  private EmbassySlotBar.Data data;

  public bool detailExpended
  {
    get
    {
      return this._detailExpended;
    }
    set
    {
      if (value == this._detailExpended)
        return;
      this._detailExpended = value;
      if (this._detailExpended)
      {
        this.panel.TweenScale_ExpendDetals.PlayForward();
        this.panel.TweenRotate_ExpendButton.PlayForward();
        this.panel.TweenScale_ExpendDragon.PlayForward();
        this.panel.TweenScale_ExpandNoDragon.PlayForward();
      }
      else
      {
        this.panel.TweenScale_ExpendDragon.PlayReverse();
        this.panel.TweenScale_ExpendDetals.PlayReverse();
        this.panel.TweenRotate_ExpendButton.PlayReverse();
        this.panel.TweenScale_ExpandNoDragon.PlayReverse();
      }
    }
  }

  public void Setup(int slotIndex, long marchId)
  {
    this.data.marchId = marchId;
    this.data.slotIndex = slotIndex;
    this.panel.slotIndex.text = slotIndex.ToString();
    this.data.marchData = DBManager.inst.DB_March.Get(marchId);
    this.data.uid = this.data.marchData.ownerUid;
    if (this.data.marchData != null)
    {
      if ((UnityEngine.Object) this.panel.dragonUIInfo != (UnityEngine.Object) null)
        NGUITools.SetActive(this.panel.dragonUIInfo.gameObject, this.data.marchData.withDragon);
      NGUITools.SetActive(this.panel.noDragonInfo, !this.data.marchData.withDragon);
      NGUITools.SetActive(this.panel.dragonIcon, this.data.marchData.withDragon);
      if (this.data.marchData.withDragon)
      {
        string empty = string.Empty;
        MarchData.MarchType type = this.data.marchData.type;
        string skillType;
        switch (type)
        {
          case MarchData.MarchType.encamp:
          case MarchData.MarchType.city_attack:
          case MarchData.MarchType.encamp_attack:
          case MarchData.MarchType.gather_attack:
          case MarchData.MarchType.wonder_attack:
          case MarchData.MarchType.rally_attack:
            skillType = "attack";
            break;
          case MarchData.MarchType.gather:
            skillType = "gather";
            break;
          case MarchData.MarchType.reinforce:
            skillType = "defend";
            break;
          case MarchData.MarchType.monster_attack:
            skillType = "attack_monster";
            break;
          default:
            if (type != MarchData.MarchType.rally)
            {
              if (type != MarchData.MarchType.world_boss_attack)
              {
                skillType = "attack";
                break;
              }
              goto case MarchData.MarchType.monster_attack;
            }
            else
              goto case MarchData.MarchType.encamp;
        }
        if ((UnityEngine.Object) this.panel.dragonUIInfo != (UnityEngine.Object) null)
        {
          this.panel.dragonUIInfo.SetDragonOwnerData(this.data.marchData.marchId);
          this.panel.dragonUIInfo.SetDragon(this.data.marchData.dragonId, skillType, (int) this.data.marchData.dragon.tendency, this.data.marchData.dragon.level);
        }
      }
      if (this.data.marchData.troopsInfo.troops != null)
      {
        this.panel.troopCount.text = Utils.ConvertNumberToNormalString(this.data.marchData.troopsInfo.totalCount);
        for (int index = this._details.Count + 1; index <= this.data.marchData.troopsInfo.troops.Count; ++index)
        {
          GameObject gameObject = NGUITools.AddChild(this.panel.DetailContainer.gameObject, this.panel.DetailBarOrg.gameObject);
          gameObject.transform.localPosition = new Vector3(0.0f, (float) (-index * this.panel.DetailBarOrgWidget.height), 0.0f);
          gameObject.name = string.Format("slot_{0}", (object) (1000 + index));
          this._details.Add(gameObject.GetComponent<EmbassySlotBarDetail>());
        }
        int index1 = 0;
        List<KeyValuePair<Unit_StatisticsInfo, int>> keyValuePairList = new List<KeyValuePair<Unit_StatisticsInfo, int>>();
        Dictionary<string, Unit> troops = this.data.marchData.troopsInfo.troops;
        Dictionary<string, Unit>.KeyCollection.Enumerator enumerator = troops.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator.Current);
          if (data != null)
            keyValuePairList.Add(new KeyValuePair<Unit_StatisticsInfo, int>(data, troops[enumerator.Current].Count));
          ++index1;
        }
        keyValuePairList.Sort((Comparison<KeyValuePair<Unit_StatisticsInfo, int>>) ((a, b) =>
        {
          if (a.Key.Troop_Tier == b.Key.Troop_Tier)
            return a.Key.Priority.CompareTo(b.Key.Priority);
          return b.Key.Troop_Tier.CompareTo(a.Key.Troop_Tier);
        }));
        for (int index2 = 0; index2 < keyValuePairList.Count; ++index2)
        {
          this._details[index2].Setup(keyValuePairList[index2].Key.ID, keyValuePairList[index2].Value);
          this._details[index2].gameObject.SetActive(true);
        }
        for (int index2 = index1; index2 < this._details.Count; ++index2)
          this._details[index1].gameObject.SetActive(false);
        this.panel.DetailContainer.height = this.panel.DetailsTitle.height + index1 * 200 + 60;
        this.panel.DetailContainer.transform.localScale = new Vector3(1f, 0.0f, 1f);
      }
      UserData userData = DBManager.inst.DB_User.Get(this.data.marchData.ownerUid);
      if (userData != null)
      {
        this.panel.playerName.text = userData.userName;
        CustomIconLoader.Instance.requestCustomIcon(this.panel.playerIcon, userData.PortraitIconPath, userData.Icon, false);
        LordTitlePayload.Instance.ApplyUserAvator(this.panel.playerIcon, userData.LordTitle, 1);
      }
      DB.HeroData heroData = DBManager.inst.DB_hero.Get(this.data.marchData.ownerUid);
      if (heroData != null)
        this.panel.playerLevel.text = heroData.level.ToString();
      NGUITools.SetActive(this.panel.playerLevel.gameObject, heroData != null);
    }
    this.detailExpended = false;
  }

  public void OnExpandedClick()
  {
    this.detailExpended = !this.detailExpended;
    if (this.onExpandMarkClick == null)
      return;
    this.onExpandMarkClick(this.detailExpended);
  }

  public void OnSendTroopsHomeClick()
  {
    if (this.onSendTroopsHomeClick == null)
      return;
    this.onSendTroopsHomeClick(this.data.uid);
  }

  private struct Data
  {
    public int slotIndex;
    public long marchId;
    public long uid;
    public MarchData marchData;
  }

  [Serializable]
  protected class Panel
  {
    public const string STATE_LOCKED_TEXT = "LOCKED WAR SLOT";
    public const string STATE_UNLOCKED_TEXT = "SEND TROOPS";
    public UILabel slotIndex;
    public UILabel playerName;
    public UILabel troopCount;
    public UILabel playerLevel;
    public UITexture playerIcon;
    public UIButton BT_SendTroopsHome;
    public UIButton BT_SlotExpend;
    public TweenRotation TweenRotate_ExpendButton;
    public TweenScale TweenScale_ExpendDetals;
    public TweenScale TweenScale_ExpendDragon;
    public TweenScale TweenScale_ExpandNoDragon;
    public GameObject dragonIcon;
    public UIWidget DetailContainer;
    public UIWidget DetailsTitle;
    public EmbassySlotBarDetail DetailBarOrg;
    public UIWidget DetailBarOrgWidget;
    public DragonUIInfo dragonUIInfo;
    public GameObject noDragonInfo;
  }
}
