﻿// Decompiled with JetBrains decompiler
// Type: LoadBaseState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using SevenZip;
using SevenZip.Compression.LZMA;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UI;
using UnityEngine;

public class LoadBaseState : IState
{
  private string key = string.Empty;
  private bool _canRetry = true;
  private bool needLoad = true;
  private Stopwatch watch = new Stopwatch();
  private const string ZIP_MARK = ".zip";
  public const int EXCEPTION_0_MAX_COUNT = 10;
  public const int EXCEPTION_0_DEFAULT = 0;
  public const int EXCEPTION_0_WITHOUT_POPUP = 1;
  protected LoaderBatch batch;
  private bool isDestroy;
  private long elapseTime;

  public LoadBaseState(int step)
  {
    this.CurrentLoadStep = step;
  }

  public virtual string Key
  {
    get
    {
      return this.key;
    }
  }

  public Hashtable Data { get; set; }

  protected Preloader Preloader
  {
    get
    {
      return Preloader.inst;
    }
  }

  private static int RetryTime
  {
    get
    {
      return 30;
    }
  }

  public int CurrentLoadStep { set; get; }

  protected LoaderInfo GetLoaderInfo(string action, string apiUrl)
  {
    LoaderInfo loaderInfo = LoaderManager.inst.PopInfo();
    loaderInfo.Action = action;
    loaderInfo.AutoRetry = true;
    loaderInfo.RetryMaxTime = LoadBaseState.RetryTime;
    loaderInfo.URL = apiUrl;
    loaderInfo.Type = 3;
    loaderInfo.TimeOut = 5;
    return loaderInfo;
  }

  public void OnEnter()
  {
    if (this.NeedLoad && this.batch == null)
    {
      this.batch = LoaderManager.inst.PopBatch();
      this.AddEventHandler();
    }
    this.watch.Reset();
    this.watch.Start();
    this.Prepare();
  }

  protected virtual void Prepare()
  {
    this.RefreshProgress(0.0f);
  }

  protected virtual bool NeedLoad
  {
    get
    {
      return this.needLoad;
    }
    set
    {
      this.needLoad = value;
    }
  }

  protected void AddEventHandler()
  {
    if (this.batch == null)
      return;
    this.batch.processEvent += new System.Action<LoaderInfo>(this.OnOneFileDone);
    this.batch.finishedEvent += new System.Action<LoaderBatch>(this.OnAllFileFinish);
  }

  protected void ResetBatch()
  {
    this.RemoveEventHandler();
    this.batch = LoaderManager.inst.PopBatch();
    this.AddEventHandler();
  }

  protected virtual void OnOneFileDone(LoaderInfo info)
  {
    if (!string.IsNullOrEmpty(info.ErrorMsg))
      D.warn((object) ("[Loading Error]:" + info.ErrorMsg + " Action =" + info.Action));
    this.IsFail = !this.CheckIsSuccess(info.ResData);
  }

  protected virtual bool CheckIsSuccess(byte[] ResData)
  {
    return ResData != null;
  }

  protected bool CheckResult(LoaderInfo info)
  {
    return this.CheckResult(Utils.Json2Object(Encryptor.LoaderDecode(info), true) as Hashtable, info);
  }

  private bool CheckResult(Hashtable ht, LoaderInfo info)
  {
    if (ht != null && ht.ContainsKey((object) "ok"))
    {
      LoginErrDialog.LoginErrLog(string.Format("Code {0}", (object) ht[(object) "ok"].ToString()));
      string key = ht[(object) "ok"].ToString();
      if (key != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (LoadBaseState.\u003C\u003Ef__switch\u0024map94 == null)
        {
          // ISSUE: reference to a compiler-generated field
          LoadBaseState.\u003C\u003Ef__switch\u0024map94 = new Dictionary<string, int>(8)
          {
            {
              "0",
              0
            },
            {
              "1",
              1
            },
            {
              "2",
              2
            },
            {
              "3",
              3
            },
            {
              "4",
              4
            },
            {
              "5",
              5
            },
            {
              "6",
              6
            },
            {
              "7",
              7
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (LoadBaseState.\u003C\u003Ef__switch\u0024map94.TryGetValue(key, out num))
        {
          switch (num)
          {
            case 0:
              return this.HandleException0(ht);
            case 1:
              return this.HandleException1(ht);
            case 2:
              return this.HandleException2(ht);
            case 3:
              return this.HandleException3(info.Action, ht);
            case 4:
              return this.HandleException4(info.Action, ht);
            case 5:
              return this.HandleBanAccount(ht);
            case 6:
              GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Deep);
              break;
            case 7:
              return this.HandleException7(ht);
          }
        }
      }
    }
    return false;
  }

  private bool HandleException7(Hashtable ht)
  {
    string errorDetail = "check session fail";
    if (ht.ContainsKey((object) "msg"))
      errorDetail = ht[(object) "msg"].ToString();
    string title = ScriptLocalization.Get("data_error_title", true);
    string content = ScriptLocalization.Get("data_error_sync_description", true);
    if (!NetWorkDetector.Instance.IsReadyRestart())
      NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.CheckSessionError, "call init", errorDetail);
    string left = ScriptLocalization.Get("network_error_reestablish_connection_button", true);
    string right = ScriptLocalization.Get("network_error_exit_game", true);
    UIManager.inst.ShowNetErrorBox(title, content, left, right, NetErrorBlocker.ButtonState.OK_CENTER, new System.Action(this.OnLogOutHandler), new System.Action(this.OnLogOutHandler), new System.Action(this.OnLogOutHandler));
    return false;
  }

  private void OnLogOutHandler()
  {
    AccountManager.Instance.LogoutNow();
  }

  private bool HandleException0(Hashtable ht)
  {
    return true;
  }

  private bool HandleException1(Hashtable ht)
  {
    return true;
  }

  private bool HandleException2(Hashtable ht)
  {
    Dictionary<string, string> para = new Dictionary<string, string>();
    long result = 0;
    if (ht.ContainsKey((object) "msg"))
      long.TryParse(ht[(object) "msg"].ToString(), out result);
    string title = (string) null;
    string tip = (string) null;
    string leftBt = (string) null;
    if (result > 0L)
    {
      int num1 = (int) ((double) result - Utils.ClientSideUnixUTCSeconds);
      if (num1 > 0)
      {
        int num2 = num1 / 60;
        if (num1 % 60 > 0)
          ++num2;
        para.Add("0", num2.ToString());
      }
      title = ScriptLocalization.Get("server_maintenance_time_title", true);
      tip = ScriptLocalization.GetWithPara("server_maintenance_time_description", para, true);
      if (num1 <= 0)
      {
        int num2 = Mathf.Abs(num1);
        int num3 = num2 / 60;
        if (num2 % 60 > 0)
          ++num3;
        para.Clear();
        para.Add("0", num3.ToString());
        tip = ScriptLocalization.GetWithPara("toast_server_not_open_yet", para, true);
        leftBt = ScriptLocalization.Get("event_server_ready_login", true);
      }
    }
    if (ht.ContainsKey((object) "payload") && ht[(object) "payload"] != null)
    {
      Hashtable hashtable = ht[(object) "payload"] as Hashtable;
      GameLoadingAnnouncementPayload.Instance.AnnouncementContent = !hashtable.ContainsKey((object) "loadingAnnounce") || hashtable[(object) "loadingAnnounce"] == null ? (string) null : hashtable[(object) "loadingAnnounce"].ToString();
    }
    if (!NetWorkDetector.Instance.IsReadyRestart())
      NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.Loading, "Maintenance", string.Empty);
    NetWorkDetector.Instance.RestartOrQuitGame(tip, title, leftBt);
    Utils.HideNetErrorWindowBlocker();
    return false;
  }

  private bool HandleException3(string action, Hashtable ht)
  {
    string errorDetail = "server msg = ";
    if (ht.ContainsKey((object) "msg"))
      errorDetail = ht[(object) "msg"].ToString();
    string title = ScriptLocalization.Get("data_error_title", true);
    string tip = ScriptLocalization.Get("data_error_sync_description", true);
    if (!NetWorkDetector.Instance.IsReadyRestart())
      NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.ServerError, action, errorDetail);
    NetWorkDetector.Instance.RestartOrQuitGame(tip, title, (string) null);
    Utils.HideNetErrorWindowBlocker();
    return false;
  }

  private bool HandleException4(string action, Hashtable ht)
  {
    string errorDetail = "server msg = ";
    if (ht.ContainsKey((object) "msg"))
      errorDetail = ht[(object) "msg"].ToString();
    string title = ScriptLocalization.Get("data_error_title", true);
    string tip = ScriptLocalization.Get("data_error_sync_description", true);
    if (!NetWorkDetector.Instance.IsReadyRestart())
      NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.ServerError, action, errorDetail);
    NetWorkDetector.Instance.RestartOrQuitGame(tip, title, (string) null);
    Utils.HideNetErrorWindowBlocker();
    return false;
  }

  private bool HandleBanAccount(Hashtable ht)
  {
    LoginErrDialog.LoginErrLog(nameof (HandleBanAccount));
    BundleManager.Instance.LoadBasicBundle("static+basic_common");
    BundleManager.Instance.LoadBasicBundle("static+basic_gui2");
    BundleManager.Instance.LoadBasicBundle("static+scene_ui");
    string str = ht[(object) "msg"] as string;
    int num1 = int.Parse(ht[(object) "reason"] as string);
    int num2 = int.Parse(ht[(object) "end_time"] as string);
    LoginErrDialog.LoginErrLog(string.Format("Ban Username {0}, reason {1}, unban timestamp {2}", (object) str, (object) num1, (object) num2));
    Popup popup = UIManager.inst.OpenPopup("Account/LoginErrPopup", (Popup.PopupParameter) new LoginErrDialog.Parameter()
    {
      username = str,
      reason = num1,
      timestamp = num2
    });
    if (GameLoadingAnnouncementPayload.Instance.AnnouncementContent != null)
    {
      Transform child = popup.transform.FindChild("z_Blocker");
      if ((UnityEngine.Object) child != (UnityEngine.Object) null)
        child.gameObject.SetActive(false);
    }
    return false;
  }

  protected virtual void OnAllFileFinish(LoaderBatch info)
  {
    this.ShowRetry();
  }

  protected void ShowRetry()
  {
    if (!this.IsFail || !this.CanRetry)
      return;
    NetWorkDetector.Instance.RetryTip(new System.Action(this.Retry), ScriptLocalization.Get("data_error_title", true), ScriptLocalization.Get("data_error_loading1_description", true));
    Utils.HideNetErrorWindowBlocker();
  }

  protected void RemoveEventHandler()
  {
    if (this.batch == null)
      return;
    this.batch.processEvent -= new System.Action<LoaderInfo>(this.OnOneFileDone);
    this.batch.finishedEvent -= new System.Action<LoaderBatch>(this.OnAllFileFinish);
  }

  protected virtual SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.None;
    }
  }

  protected void RefreshProgress()
  {
    GameEngine.Instance.iTweenValue = SplashDataConfig.GetValue(this.CurrentPhase);
    this.Preloader.SplashScreen(SplashDataConfig.GetTipFromPhaseKey(this.CurrentPhase), SplashDataConfig.GetValue(this.CurrentPhase));
  }

  protected virtual void RefreshProgress(float value)
  {
    this.Preloader.SplashScreen(SplashDataConfig.GetTipFromPhaseKey(this.CurrentPhase), SplashDataConfig.GetValue(this.CurrentPhase) * value);
  }

  protected bool IsFail { get; set; }

  protected bool CanRetry
  {
    get
    {
      return this._canRetry;
    }
    set
    {
      this._canRetry = value;
    }
  }

  protected virtual void Retry()
  {
  }

  protected virtual void Finish()
  {
    this.RemoveEventHandler();
    this.ElapseTime = this.watch.ElapsedMilliseconds;
    if (!Application.isEditor)
      this.PushBI();
    this.watch.Stop();
    this.batch = (LoaderBatch) null;
  }

  public void Dispose()
  {
    this.RemoveEventHandler();
    if (this.Data != null)
      this.Data = (Hashtable) null;
    this.OnDispose();
    this.isDestroy = true;
  }

  protected virtual void OnDispose()
  {
  }

  public bool IsDestroy
  {
    get
    {
      return this.isDestroy;
    }
  }

  protected void WriteFile(string path, byte[] bytes)
  {
    this.WriteFileSuccess = false;
    try
    {
      if (path.Contains(".zip"))
      {
        Utils.WriteFile(path, bytes);
        string path1 = path.Substring(0, path.IndexOf(".zip"));
        Decoder decoder = new Decoder();
        FileStream fileStream1 = new FileStream(path, FileMode.Open);
        FileStream fileStream2 = new FileStream(path1, FileMode.Create);
        byte[] numArray = new byte[5];
        fileStream1.Read(numArray, 0, 5);
        byte[] array = new byte[8];
        fileStream1.Read(array, 0, 8);
        long int64 = BitConverter.ToInt64(array, 0);
        decoder.SetDecoderProperties(numArray);
        decoder.Code((Stream) fileStream1, (Stream) fileStream2, fileStream1.Length, int64, (ICodeProgress) null);
        fileStream2.Flush();
        fileStream2.Close();
        fileStream1.Close();
        File.Delete(path);
        this.WriteFileSuccess = true;
      }
      else
      {
        Utils.WriteFile(path, bytes);
        this.WriteFileSuccess = true;
      }
    }
    catch (IOException ex)
    {
      this.WriteFileSuccess = false;
      this.WriteException();
    }
  }

  protected bool WriteFileSuccess { get; set; }

  protected void WriteException()
  {
    NetWorkDetector.Instance.QuitGame(ScriptLocalization.Get("insufficient_space_title", true), ScriptLocalization.Get("insufficient_space_description", true));
  }

  public void OnProcess()
  {
  }

  protected long ElapseTime
  {
    get
    {
      return this.elapseTime;
    }
    set
    {
      this.elapseTime = value;
    }
  }

  protected void PushBI()
  {
    NetWorkDetector.Instance.PushBI("loading", Utils.Object2Json((object) new LoadBaseState.BIDataFormat()
    {
      d_c1 = new LoadBaseState.BIStep()
      {
        value = this.CurrentLoadStep.ToString(),
        key = "loading_step"
      },
      d_c2 = new LoadBaseState.BIStep()
      {
        value = SplashDataConfig.GetTipFromPhaseKey(this.CurrentPhase),
        key = "loading_step"
      },
      d_c3 = new LoadBaseState.BIStep()
      {
        value = this.ElapseTime.ToString(),
        key = "elapse_time"
      }
    }));
  }

  public void OnExit()
  {
    this.RemoveEventHandler();
    this.OnDispose();
  }

  public class BIStep
  {
    public string key = "loading_step";
    public string value;
  }

  public class BIDataFormat
  {
    public LoadBaseState.BIStep d_c1;
    public LoadBaseState.BIStep d_c2;
    public LoadBaseState.BIStep d_c3;
  }
}
