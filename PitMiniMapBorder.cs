﻿// Decompiled with JetBrains decompiler
// Type: PitMiniMapBorder
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PitMiniMapBorder : MonoBehaviour
{
  [SerializeField]
  private float _worldToUv = 1f;
  [SerializeField]
  private bool _dirty = true;
  [SerializeField]
  private float _speed = 1f;
  private Vector2 _uvCache = Vector2.zero;
  [SerializeField]
  private Vector2 _rectSize;
  [SerializeField]
  private float _offset;
  [SerializeField]
  private Material _material;
  private Mesh _mesh;
  private Renderer _renderer;

  private void Start()
  {
    this._renderer = this.GetComponent<Renderer>();
    this._renderer.material = this._material;
    this.RegenerateMesh();
  }

  private void RegenerateMesh()
  {
    List<Vector2> vector2List = new List<Vector2>();
    vector2List.Add(new Vector2(0.0f, 1f));
    vector2List.Add(new Vector2(1f * this._worldToUv, 1f));
    vector2List.Add(new Vector2(0.0f, 1f));
    vector2List.Add(new Vector2(1f * this._worldToUv, 1f));
    vector2List.Add(new Vector2(0.0f, 0.0f));
    vector2List.Add(new Vector2(1f * this._worldToUv, 0.0f));
    vector2List.Add(new Vector2(0.0f, 0.0f));
    vector2List.Add(new Vector2(1f * this._worldToUv, 0.0f));
    List<int> intList = new List<int>();
    intList.Add(0);
    intList.Add(5);
    intList.Add(1);
    intList.Add(0);
    intList.Add(4);
    intList.Add(5);
    intList.Add(1);
    intList.Add(5);
    intList.Add(6);
    intList.Add(1);
    intList.Add(6);
    intList.Add(2);
    intList.Add(7);
    intList.Add(2);
    intList.Add(6);
    intList.Add(7);
    intList.Add(3);
    intList.Add(2);
    intList.Add(0);
    intList.Add(7);
    intList.Add(4);
    intList.Add(0);
    intList.Add(3);
    intList.Add(7);
    if ((bool) ((Object) this._mesh))
      Object.DestroyImmediate((Object) this._mesh);
    this._mesh = new Mesh();
    float x1 = -this._rectSize.x;
    float x2 = this._rectSize.x;
    float y1 = this._rectSize.y;
    float y2 = -this._rectSize.y;
    this._mesh.SetVertices(new List<Vector3>()
    {
      new Vector3(x1 - this._offset, y1 + this._offset, 0.0f),
      new Vector3(x2 + this._offset, y1 + this._offset, 0.0f),
      new Vector3(x2 + this._offset, y2 - this._offset, 0.0f),
      new Vector3(x1 - this._offset, y2 - this._offset, 0.0f),
      new Vector3(x1, y1, 0.0f),
      new Vector3(x2, y1, 0.0f),
      new Vector3(x2, y2, 0.0f),
      new Vector3(x1, y2, 0.0f)
    });
    this._mesh.SetUVs(0, vector2List);
    this._mesh.SetIndices(intList.ToArray(), MeshTopology.Triangles, 0);
    this.GetComponent<MeshFilter>().mesh = this._mesh;
  }

  private void Update()
  {
    if (this._dirty)
    {
      this._dirty = false;
      this.RegenerateMesh();
    }
    this._uvCache.y -= this._speed * Time.deltaTime;
    this._uvCache.y %= 1f;
    if (!(bool) ((Object) this._renderer))
      return;
    this._renderer.material.SetTextureOffset("_MainTex", this._uvCache);
  }
}
