﻿// Decompiled with JetBrains decompiler
// Type: GetMoreResourceDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class GetMoreResourceDialog : GetMoreDialog
{
  private ItemBag.ItemType[] m_Types = new ItemBag.ItemType[4]
  {
    ItemBag.ItemType.food,
    ItemBag.ItemType.wood,
    ItemBag.ItemType.ore,
    ItemBag.ItemType.silver
  };
  [SerializeField]
  private UILabel m_Food;
  [SerializeField]
  private UILabel m_Wood;
  [SerializeField]
  private UILabel m_Ore;
  [SerializeField]
  private UILabel m_Silver;
  [SerializeField]
  private UIToggle[] m_Categories;

  public void OnCategoryClicked()
  {
    this.SetSelectedIndex(this.GetCategoryIndex());
  }

  private void OnResourcesUpdated(long cityId)
  {
    if (cityId != (long) PlayerData.inst.cityId)
      return;
    CityData playerCityData = PlayerData.inst.playerCityData;
    long currentResource1 = (long) playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD);
    long currentResource2 = (long) playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD);
    long currentResource3 = (long) playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE);
    long currentResource4 = (long) playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
    this.m_Food.text = Utils.FormatShortThousandsLong(currentResource1);
    this.m_Wood.text = Utils.FormatShortThousandsLong(currentResource2);
    this.m_Ore.text = Utils.FormatShortThousandsLong(currentResource3);
    this.m_Silver.text = Utils.FormatShortThousandsLong(currentResource4);
  }

  private void SwitchCategory()
  {
    this.UpdateData(new GetMoreDialog.CompareMethod(GetMoreResourceDialog.Compare), (object) this.m_Types[this.GetCategoryIndex()]);
  }

  private static bool Compare(ShopStaticInfo shopInfo, object extra)
  {
    return ConfigManager.inst.DB_Items.GetItem(shopInfo.Item_InternalId).Type == (ItemBag.ItemType) extra;
  }

  private void SetSelectedIndex(int index)
  {
    foreach (UIToggle category in this.m_Categories)
      category.Set(false);
    this.m_Categories[index].Set(true);
    this.SwitchCategory();
  }

  private int GetCategoryIndex()
  {
    for (int index = 0; index < this.m_Categories.Length; ++index)
    {
      if (this.m_Categories[index].value)
        return index;
    }
    return 0;
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.SetSelectedIndex((orgParam as GetMoreResourceDialog.Parameter).index);
    DBManager.inst.DB_City.onDataChanged += new System.Action<long>(this.OnResourcesUpdated);
    this.OnResourcesUpdated((long) PlayerData.inst.cityId);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.SetSelectedIndex((orgParam as GetMoreResourceDialog.Parameter).index);
    this.OnResourcesUpdated((long) PlayerData.inst.cityId);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    DBManager.inst.DB_City.onDataChanged -= new System.Action<long>(this.OnResourcesUpdated);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int index;
  }
}
