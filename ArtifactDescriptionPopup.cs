﻿// Decompiled with JetBrains decompiler
// Type: ArtifactDescriptionPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class ArtifactDescriptionPopup : Popup
{
  public UILabel artifactName;
  public UILabel artifactSpecificDesc;
  private int _artifactId;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    ArtifactDescriptionPopup.Parameter parameter = orgParam as ArtifactDescriptionPopup.Parameter;
    if (parameter != null)
      this._artifactId = parameter.artifactId;
    this.UpdateUI();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this._artifactId);
    if (artifactInfo == null)
      return;
    this.artifactName.text = artifactInfo.Name;
    this.artifactSpecificDesc.text = artifactInfo.GainMethodDescription;
  }

  public class Parameter : Popup.PopupParameter
  {
    public int artifactId;
  }
}
