﻿// Decompiled with JetBrains decompiler
// Type: AllianceBuildingConstructSlotContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AllianceBuildingConstructSlotContainer : MonoBehaviour
{
  private List<AllianceBuildingConstructSlotItem> items = new List<AllianceBuildingConstructSlotItem>();
  public AllianceBuildingConstructSlotItem itemPrefab;
  public UIScrollView scroll;
  public UITable tabel;

  public void SetData(List<long> marchIds, bool showEmptyItem = true)
  {
    this.Clear();
    int index1 = 0;
    for (int index2 = 0; index2 < marchIds.Count; ++index2)
    {
      ++index1;
      AllianceBuildingConstructSlotItem constructSlotItem = this.GetItem();
      constructSlotItem.OnOpenCallBackDelegate = new System.Action(this.OnOpenHandler);
      constructSlotItem.SetData(index1, marchIds[index2]);
      this.items.Add(constructSlotItem);
    }
    if (showEmptyItem)
    {
      int index2 = index1 + 1;
      AllianceBuildingConstructSlotItem constructSlotItem = this.GetItem();
      constructSlotItem.Empty(index2, 0L);
      this.items.Add(constructSlotItem);
    }
    this.tabel.Reposition();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.scroll.ResetPosition()));
  }

  private void OnOpenHandler()
  {
    this.tabel.repositionNow = true;
    this.tabel.Reposition();
  }

  private AllianceBuildingConstructSlotItem GetItem()
  {
    GameObject go = Utils.DuplicateGOB(this.itemPrefab.gameObject, this.tabel.transform);
    AllianceBuildingConstructSlotItem component = go.GetComponent<AllianceBuildingConstructSlotItem>();
    NGUITools.SetActive(go, true);
    return component;
  }

  public void Clear()
  {
    for (int index = 0; index < this.items.Count; ++index)
    {
      this.items[index].Clear();
      NGUITools.SetActive(this.items[index].gameObject, false);
      this.items[index].transform.parent = this.transform;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.items[index]);
    }
    this.items.Clear();
  }
}
