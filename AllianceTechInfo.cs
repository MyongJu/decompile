﻿// Decompiled with JetBrains decompiler
// Type: AllianceTechInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class AllianceTechInfo
{
  public int[] exps = new int[6];
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "research_type")]
  public string ResearchType;
  [Config(Name = "tier")]
  public int TierId;
  [Config(Name = "tier_index")]
  public int TierIndex;
  [Config(Name = "row")]
  public int Index;
  [Config(Name = "loc_research_name_id")]
  public string LocNameId;
  [Config(Name = "loc_research_description_id")]
  public string LocDescriptionId;
  [Config(Name = "image")]
  public string Image;
  [Config(Name = "level")]
  public int Level;
  [Config(Name = "exp1")]
  public int Exp1;
  [Config(Name = "exp2")]
  public int Exp2;
  [Config(Name = "exp3")]
  public int Exp3;
  [Config(Name = "exp4")]
  public int Exp4;
  [Config(Name = "exp5")]
  public int Exp5;
  public int maxExpIndex;
  public int TotalExp;
  [Config(Name = "duration")]
  public float Duration;
  [Config(CustomParse = true, Name = "Requirements", ParseFuncKey = "ParseBenfits")]
  public Dictionary<int, float> Requirements;
  [Config(CustomParse = true, Name = "Benefits", ParseFuncKey = "ParseBenfits")]
  public Dictionary<int, float> Benefits;
  [Config(Name = "init_resource")]
  public string InitResources;
  [Config(Name = "change_chance")]
  public float ChangeChance;
  [Config(Name = "change_silver_chance")]
  public float ChangeSilverChance;
  [Config(Name = "change_wood_chance")]
  public float ChangeWoodChance;
  [Config(Name = "change_food_chance")]
  public float ChangeFoodChance;
  [Config(Name = "change_ore_chance")]
  public float ChangeOreChance;

  public int GetExp(int index)
  {
    if (index > this.maxExpIndex)
      return this.TotalExp;
    return this.exps[this.maxExpIndex];
  }

  public string ImagePath
  {
    get
    {
      return string.Format("Texture/ResearchIcons/{0}", (object) this.Image);
    }
  }

  public string LocName
  {
    get
    {
      return ScriptLocalization.Get(this.LocNameId, true);
    }
  }

  public string LocDescription
  {
    get
    {
      return ScriptLocalization.Get(this.LocDescriptionId, true);
    }
  }
}
