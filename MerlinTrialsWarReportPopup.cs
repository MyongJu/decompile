﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsWarReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;
using UnityEngine;

public class MerlinTrialsWarReportPopup : WarReportPopup
{
  private string _vfxName = string.Empty;
  private const string PVP_RESULT_WIN_VFX = "fx_AnniversaryKingWarPopup_win";
  private const string VFX_PATH = "Prefab/Anniversary/";
  [SerializeField]
  private UILabel _labelBattleResult;
  [SerializeField]
  private UILabel _labelBattleResultDesc;
  [SerializeField]
  private UITexture _textureBattleResult;
  [SerializeField]
  private UITexture _textureBattleFrame;
  private Hashtable _mailData;
  private bool _winWar;
  private bool _openAfterBattle;
  private bool _isMysteryShopExist;
  private int _battleId;
  private GameObject _vfxObj;

  public override string Type
  {
    get
    {
      return "MerlinTrials/MerlinTrialsWarReportPopup";
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    MerlinTrialsWarReportPopup.Parameter parameter = orgParam as MerlinTrialsWarReportPopup.Parameter;
    if (parameter != null)
    {
      this._mailData = parameter.mailData;
      this._winWar = parameter.winWar;
      this._battleId = parameter.battleId;
      this._openAfterBattle = parameter.openAfterBattle;
      this._isMysteryShopExist = parameter.isMysteryShopExist;
      if (this._mailData != null)
      {
        AbstractMailEntry abstractMailEntry = new AbstractMailEntry();
        abstractMailEntry.ApplyData(this._mailData);
        parameter.hashtable = this._mailData;
        parameter.mail = abstractMailEntry;
      }
    }
    this._showLegend = false;
    this._isMerlinTrials = true;
    base.OnShow((UIControler.UIParameter) parameter);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.DeleteVFX();
    this.CheckStatusAfterClose();
  }

  public void OnCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void CheckStatusAfterClose()
  {
    if (this._openAfterBattle && !this._winWar && (MerlinTrialsHelper.inst.ShouldShowMysteryShop() && !this._isMysteryShopExist))
      UIManager.inst.OpenPopup("MerlinTrials/MerlinMysteryStorePopup", (Popup.PopupParameter) new MerlinMysteryStorePopup.Parameter()
      {
        merlinTrialsMainId = MerlinTrialsHelper.inst.GetMysteryShopBattleId()
      });
    if (!this._openAfterBattle || !this._winWar || (!MerlinTrialsHelper.inst.IsLastBattleOfLevel(this._battleId) || !MerlinTrialsHelper.inst.IsLastBattleOfGroup(this._battleId)))
      return;
    MerlinTrialsMainInfo merlinTrialsMainInfo = ConfigManager.inst.DB_MerlinTrialsMain.Get(this._battleId);
    if (MerlinTrialsHelper.inst.IsGroupComplete(merlinTrialsMainInfo.trialId))
      return;
    UIManager.inst.OpenPopup("MerlinTrials/MerlinLevelCompletePopup", (Popup.PopupParameter) new MerlinTrialsLevelCompletePopup.Parameter()
    {
      groupId = merlinTrialsMainInfo.trialId
    });
    MerlinTrialsHelper.inst.SetGroupComplete(merlinTrialsMainInfo.trialId);
  }

  private void UpdateUI()
  {
    this.title.text = Utils.XLAT("trial_challenge_result_title");
    if (this._winWar)
    {
      this._labelBattleResult.text = Utils.XLAT("dragon_knight_pvp_success");
      this._labelBattleResultDesc.text = Utils.XLAT("trial_challenge_success_description");
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureBattleResult, "Texture/DragonKnight/DungeonTexture/dragon_knight_pvp_result_win", (System.Action<bool>) null, true, true, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureBattleFrame, "Texture/GUI_Textures/annv_winner", (System.Action<bool>) null, true, true, string.Empty);
      if (!this._openAfterBattle)
        return;
      this._vfxName = "fx_AnniversaryKingWarPopup_win";
      this.SetVFX(this._vfxName);
      AudioManager.Instance.PlaySound("sfx_dragon_knight_pvp_win", false);
    }
    else
    {
      this._labelBattleResult.text = Utils.XLAT("dragon_knight_pvp_fail");
      this._labelBattleResultDesc.text = Utils.XLAT("trial_challenge_fail_description");
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureBattleResult, "Texture/DragonKnight/DungeonTexture/dragon_knight_pvp_result_lose", (System.Action<bool>) null, true, true, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureBattleFrame, "Texture/GUI_Textures/annv_lost", (System.Action<bool>) null, true, true, string.Empty);
      if (!this._openAfterBattle)
        return;
      AudioManager.Instance.PlaySound("sfx_dragon_knight_pvp_lose", false);
    }
  }

  private void SetVFX(string vfxUsedName)
  {
    if (string.IsNullOrEmpty(vfxUsedName))
      return;
    this._vfxObj = AssetManager.Instance.HandyLoad("Prefab/Anniversary/" + vfxUsedName, typeof (GameObject)) as GameObject;
    if (!((UnityEngine.Object) this._vfxObj != (UnityEngine.Object) null))
      return;
    this._vfxObj = UnityEngine.Object.Instantiate<GameObject>(this._vfxObj);
    this._vfxObj.transform.parent = this._textureBattleResult.transform;
    this._vfxObj.transform.localPosition = Vector3.zero;
    this._vfxObj.transform.localScale = Vector3.one;
  }

  private void DeleteVFX()
  {
    this._vfxName = string.Empty;
    if (!((UnityEngine.Object) this._vfxObj != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this._vfxObj);
    this._vfxObj = (GameObject) null;
  }

  public class Parameter : BaseReportPopup.Parameter
  {
    public Hashtable mailData;
    public bool winWar;
    public bool openAfterBattle;
    public bool isMysteryShopExist;
    public int battleId;
  }
}
