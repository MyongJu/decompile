﻿// Decompiled with JetBrains decompiler
// Type: GemDetailedInfoRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class GemDetailedInfoRender : MonoBehaviour
{
  public UILabel gemName;
  public UILabel curLvTemplete;
  public UISprite line;
  public UILabel lblGemLevel;
  public UILabel benefitTitle;
  public UITable table;
  public UIScrollView scrollView;
  private GemHandbookComponent gemComponent;
  private ConfigEquipmentGemInfo gemConfig;

  public void Show(GemHandbookComponent component)
  {
    this.gemComponent = component;
    if ((Object) null == (Object) this.gemComponent)
      return;
    this.gemConfig = this.gemComponent.gemConfigInfo;
    if (this.gemConfig == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.gemConfig.itemID);
    if (itemStaticInfo == null)
      return;
    this.gemName.text = itemStaticInfo.LocName;
    this.benefitTitle.text = Utils.XLAT("id_uppercase_benefits");
    this.SetupBenefits();
  }

  private void SetupBenefits()
  {
    UIUtils.ClearTable(this.table);
    this.AddBenefitLabels(this.gemConfig.benefits.GetBenefitsPairs(), this.curLvTemplete, "id_level");
    this.table.Reposition();
    this.scrollView.ResetPosition();
  }

  private void AddBenefitLabels(List<Benefits.BenefitValuePair> benefits, UILabel template, string descId)
  {
    int count = benefits.Count;
    int num = 0;
    for (int index = count + 1; num < index; ++num)
    {
      UILabel component = NGUITools.AddChild(this.table.gameObject, template.gameObject).GetComponent<UILabel>();
      NGUITools.SetActive(component.gameObject, true);
      if (num == 0)
      {
        string str = "(" + Utils.XLAT("dragon_max_level") + ")";
        component.text = ScriptLocalization.Get(descId, true) + this.gemConfig.level.ToString() + " " + str;
      }
      else
      {
        Benefits.BenefitValuePair benefit = benefits[num - 1];
        component.text = benefit.ToString();
      }
      this.AddLine();
    }
  }

  private void AddLine()
  {
    NGUITools.SetActive(NGUITools.AddChild(this.table.gameObject, this.line.gameObject), true);
  }

  private void AddLevel()
  {
    GameObject go = NGUITools.AddChild(this.table.gameObject, this.lblGemLevel.gameObject);
    UILabel component = go.GetComponent<UILabel>();
    NGUITools.SetActive(go, true);
    component.text = this.gemConfig.level.ToString();
  }
}
