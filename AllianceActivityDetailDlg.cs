﻿// Decompiled with JetBrains decompiler
// Type: AllianceActivityDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceActivityDetailDlg : UI.Dialog
{
  public GameObject tab1;
  public GameObject tab2;
  public GameObject tab3;
  public AllianceActivityCurrentStepDetail stepReward;
  public ActivityStageRank stageRank;
  public ActivityTotalRank totalRank;
  public UILabel titalLabel;
  public UILabel subTitleLabel;
  public List<UILabel> tabLabels;
  private ActivityMainInfo mainInfo;
  private ActivityRewardInfo rewardInfo;
  private RoundActivityData data;
  public UIButtonBar bar;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.data = (orgParam as AllianceActivityDetailDlg.Parameter).data;
    this.AddEventHandler();
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    ActivityManager.Intance.OnTimeLimitActivityStartNewRound += new System.Action(this.StartNewRound);
    ActivityManager.Intance.OnTimeLimitActivityFinish += new System.Action(this.OnFinish);
    this.bar.OnSelectedHandler = new System.Action<int>(this.SwitchType);
  }

  private void OnFinish()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void StartNewRound()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void RemoveEventHandler()
  {
    ActivityManager.Intance.OnTimeLimitActivityStartNewRound -= new System.Action(this.StartNewRound);
    ActivityManager.Intance.OnTimeLimitActivityFinish -= new System.Action(this.OnFinish);
    this.bar.OnSelectedHandler -= new System.Action<int>(this.SwitchType);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
  }

  private void UpdateUI()
  {
    this.ConfigLables();
    this.bar.SelectedIndex = 0;
  }

  private void ConfigLables()
  {
    this.titalLabel.text = Utils.XLAT("event_alliance_name");
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("1", this.data.CurrentRound.index.ToString() + "/" + this.data.TotalRounds.ToString());
    AllianceActivitySubInfo data = ConfigManager.inst.DB_AllianceActivityMainSub.GetData(this.data.CurrentRound.ActivitySubConfigID);
    this.subTitleLabel.text = ScriptLocalization.GetWithPara("event_stage_num", para, true);
    this.subTitleLabel.text = this.subTitleLabel.text + " " + data.LocName;
    UILabel tabLabel1 = this.tabLabels[0];
    string str1 = Utils.XLAT("event_alliance_points") + " " + Utils.FormatShortThousandsLong(this.data.CurrentRoundScore);
    this.tabLabels[1].text = str1;
    string str2 = str1;
    tabLabel1.text = str2;
    UILabel tabLabel2 = this.tabLabels[2];
    string str3 = Utils.XLAT("event_stage_rankings_name") + " " + (this.data.CurrentRoundRank <= 0 ? "~" : this.data.CurrentRoundRank.ToString());
    this.tabLabels[3].text = str3;
    string str4 = str3;
    tabLabel2.text = str4;
    UILabel tabLabel3 = this.tabLabels[4];
    string str5 = Utils.XLAT("event_overall_ranking_name") + " " + (this.data.ActivityRank <= 0 ? "~" : this.data.ActivityRank.ToString());
    this.tabLabels[5].text = str5;
    string str6 = str5;
    tabLabel3.text = str6;
  }

  public void OnViewRankHistoryBtClick()
  {
    UIManager.inst.OpenPopup("Activity/ActivityRankPopup", (Popup.PopupParameter) new TimeLimitActivityRankPopup.Parameter()
    {
      isStepRank = true,
      data = (ActivityBaseData) this.data
    });
  }

  public void OnTotalRankHistoryBtClick()
  {
    UIManager.inst.OpenPopup("Activity/ActivityRankPopup", (Popup.PopupParameter) new TimeLimitActivityRankPopup.Parameter()
    {
      isStepRank = false,
      data = (ActivityBaseData) this.data
    });
  }

  public void OnViewPlayerScore()
  {
    UIManager.inst.OpenPopup("Activity/AllianceEventParticipationPopup", (Popup.PopupParameter) null);
  }

  public void OnViewRequirements()
  {
    UIManager.inst.OpenPopup("Activity/AllianceEventHowToPopup", (Popup.PopupParameter) new AllianceActivityRequirementPopup.Parameter()
    {
      data = (ActivityBaseData) this.data
    });
  }

  public void SwitchType(int index = 0)
  {
    this.tab1.SetActive(false);
    this.tab2.SetActive(false);
    this.tab3.SetActive(false);
    switch (index)
    {
      case 0:
        this.tab1.SetActive(true);
        this.stepReward.UpdateUI((ActivityBaseData) this.data);
        break;
      case 1:
        this.tab2.SetActive(true);
        this.stageRank.UpdateUI((ActivityBaseData) this.data);
        break;
      case 2:
        this.tab3.SetActive(true);
        this.totalRank.UpdateUI((ActivityBaseData) this.data);
        break;
    }
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public RoundActivityData data;
  }
}
