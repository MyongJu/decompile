﻿// Decompiled with JetBrains decompiler
// Type: ConfigLuckyArcher
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigLuckyArcher
{
  private List<LuckyArcherInfo> _luckyArcherInfoList = new List<LuckyArcherInfo>();
  private Dictionary<string, LuckyArcherInfo> _datas;
  private Dictionary<int, LuckyArcherInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<LuckyArcherInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, LuckyArcherInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._luckyArcherInfoList.Add(enumerator.Current);
    }
    this._luckyArcherInfoList.Sort((Comparison<LuckyArcherInfo>) ((a, b) => int.Parse(a.id).CompareTo(int.Parse(b.id))));
  }

  public List<LuckyArcherInfo> GetLuckyArcherInfoList()
  {
    return this._luckyArcherInfoList;
  }

  public LuckyArcherInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (LuckyArcherInfo) null;
  }

  public LuckyArcherInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (LuckyArcherInfo) null;
  }
}
