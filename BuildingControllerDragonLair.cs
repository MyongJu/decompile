﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerDragonLair
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class BuildingControllerDragonLair : BuildingControllerNew
{
  public const string dragoneggpath = "Prefab/City/dragonegg";
  public const string dragonmotionpath = "Prefab/City/CityDragonMotionPath";
  public GameObject eggroot;
  public GameObject dragonroot;
  public GameObject collectionroot;
  private GameObject dragonegg;
  private CollectionUI collectionUI;
  private DragonCityAnimationController dragonmotionroot;
  private DragonView _dragonView;

  public override void AddActionButton(ref List<CityCircleBtnPara> ftl)
  {
    base.AddActionButton(ref ftl);
    ftl.Add(this.CCBPair["dragonAttribute"]);
    ftl.Add(this.CCBPair["dragonSkillEnhance"]);
    ftl.Add(this.CCBPair["dragonSkill"]);
    if (PlayerData.inst.dragonData == null)
    {
      this.CCBPair["dragonAttribute"].isActive = false;
      this.CCBPair["dragonSkillEnhance"].isActive = false;
      this.CCBPair["dragonSkill"].isActive = false;
    }
    else
    {
      this.CCBPair["dragonAttribute"].isActive = true;
      this.CCBPair["dragonSkillEnhance"].isActive = true;
      this.CCBPair["dragonSkill"].isActive = true;
    }
  }

  private void OnEnable()
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.dragonmotionroot))
      return;
    this.dragonmotionroot.Refresh();
    this.dragonmotionroot.DragonView.SetAnimationStateTrigger("EnterCity");
  }

  public override void InitBuilding()
  {
    base.InitBuilding();
    DBManager.inst.DB_CityMap.onDataChanged += new System.Action<CityMapData>(this.OnCityDataChange);
    if (PlayerData.inst.dragonData == null)
      AssetManager.Instance.LoadAsync("Prefab/City/dragonegg", new System.Action<UnityEngine.Object, bool>(this.InitDragonEgg), (System.Type) null);
    else
      this.InitDragon();
  }

  public override void Dispose()
  {
    base.Dispose();
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.collectionUI)
    {
      this.collectionUI.Close();
      this.collectionUI = (CollectionUI) null;
    }
    DBManager.inst.DB_CityMap.onDataChanged -= new System.Action<CityMapData>(this.OnCityDataChange);
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.dragonmotionroot)
    {
      this.dragonmotionroot.Dispose();
      this.dragonmotionroot.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.dragonmotionroot.gameObject);
      this.dragonmotionroot = (DragonCityAnimationController) null;
    }
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.dragonegg))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.dragonegg);
    this.dragonegg = (GameObject) null;
  }

  private void OnCityDataChange(CityMapData data)
  {
    if (data.buildingId != this.mBuildingItem.mID)
      return;
    this.CollectionDragon();
  }

  public void CollectionDragon()
  {
    if (this.mBuildingItem.mInBuilding && (UnityEngine.Object) null == (UnityEngine.Object) this.collectionUI)
    {
      this.collectionUI = CollectionUIManager.Instance.OpenCollectionUI(this.collectionroot.transform, new CityCircleBtnPara(AtlasType.Gui_2, "btn_finish_dragon", "id_train", new EventDelegate((EventDelegate.Callback) (() => PlayerData.inst.dragonController.CollectDragon(this.mBuildingID, new System.Action<object>(this.OnCollectDragon)))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false), CollectionUI.CollectionType.TrainSoldier);
    }
    else
    {
      if (this.mBuildingItem.mInBuilding || !((UnityEngine.Object) null != (UnityEngine.Object) this.collectionUI))
        return;
      this.collectionUI.Close();
      this.collectionUI = (CollectionUI) null;
    }
  }

  private void OnCollectDragon(object para)
  {
    this.StartCoroutine(this.PlayCollectionEffect());
  }

  [DebuggerHidden]
  private IEnumerator PlayCollectionEffect()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BuildingControllerDragonLair.\u003CPlayCollectionEffect\u003Ec__Iterator34()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void NiceToMeetYou(Transform motionroot)
  {
    motionroot.GetComponent<Animator>().Play("Welcome");
    Utils.ExecuteInSecs(2f, new System.Action(this.ShowGetDragonUI));
  }

  private void ShowGetDragonUI()
  {
    UIManager.inst.OpenPopup("Dragon/DragonNamePopup", (Popup.PopupParameter) null);
  }

  public void InitDragonEgg(UnityEngine.Object egg, bool ret)
  {
    this.dragonegg = UnityEngine.Object.Instantiate(egg) as GameObject;
    AssetManager.Instance.UnLoadAsset("Prefab/City/dragonegg", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    this.dragonegg.transform.parent = this.eggroot.transform;
    this.dragonegg.transform.localScale = Vector3.one;
    DragonEgg component = this.dragonegg.GetComponent<DragonEgg>();
    JobHandle job = JobManager.Instance.GetJob(this.mBuildingItem.mTrainingJobID);
    if (job != null && !job.IsFinished())
    {
      int lefttime = job.LeftTime();
      int duration = job.Duration();
      component.UpdateStates(lefttime, duration);
    }
    else
      component.StartCoroutine(component.PlayState(0.0f, component.stepnum));
    this.CollectionDragon();
  }

  public void InitDragon()
  {
    this.dragonmotionroot = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad("Prefab/City/CityDragonMotionPath", (System.Type) null) as GameObject).GetComponent<DragonCityAnimationController>();
    this.dragonmotionroot.transform.parent = this.dragonroot.transform;
    this.dragonmotionroot.transform.localPosition = Vector3.zero;
    this.dragonmotionroot.transform.localScale = Vector3.one;
    this.dragonmotionroot.transform.localRotation = Quaternion.Euler(Vector3.one);
    this.dragonmotionroot.Init(this.dragonmotionroot.transform);
  }
}
