﻿// Decompiled with JetBrains decompiler
// Type: DevourLegendPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class DevourLegendPopup : Popup
{
  public System.Action onOK;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    this.onOK = (orgParam as DevourLegendPopup.Parameter).OkayCallback;
  }

  public void OnOkayPressed()
  {
    if (this.onOK != null)
      this.onOK();
    this.OnClosePressed();
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public System.Action OkayCallback;
  }
}
