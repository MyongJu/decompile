﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_Fortress_March_Defense
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class AllianceWar_MainDlg_Fortress_March_Defense : AllianceWar_MainDlg_March_Slot
{
  public override void Setup(long marchId)
  {
    MarchData marchData = DBManager.inst.DB_March.Get(marchId);
    if (marchData == null)
      return;
    this.data.marchId = marchId;
    this.SetLeftItems(marchData, DBManager.inst.DB_User.Get(marchData.targetUid), marchData.targetLocation);
    this.SetRightItems(marchData, DBManager.inst.DB_User.Get(marchData.ownerUid), marchData.ownerLocation);
    this.OnSecond(NetServerTime.inst.ServerTimestamp);
  }

  protected override void SetLeftItems(MarchData marchData, UserData userData, Coordinate location)
  {
    AllianceFortressData dataByCoordinate = DBManager.inst.DB_AllianceFortress.GetDataByCoordinate(location);
    if (dataByCoordinate != null)
    {
      GameObject gameObject = (GameObject) null;
      AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(dataByCoordinate.ConfigId);
      if (buildingStaticInfo.type == 0)
      {
        gameObject = this.RegenerateTargetGameObject("tiles_alliance_fortress", this.panel.leftIcon.transform);
        gameObject.transform.localPosition = new Vector3(0.0f, 60f, 0.0f);
        gameObject.transform.localScale = Vector3.one * 0.6f;
      }
      else if (buildingStaticInfo.type == 1)
      {
        gameObject = this.RegenerateTargetGameObject("tiles_alliance_turret", this.panel.leftIcon.transform);
        gameObject.transform.localPosition = Vector3.zero;
        gameObject.transform.localScale = Vector3.one;
      }
      if ((Object) gameObject != (Object) null)
      {
        AllianceFortressController component = gameObject.GetComponent<AllianceFortressController>();
        if ((bool) ((Object) component))
          component.UpdateForWarList(dataByCoordinate);
      }
      AllianceFortressData allianceFortressData = DBManager.inst.DB_AllianceFortress.Get((long) dataByCoordinate.ConfigId);
      if (allianceFortressData != null && !string.IsNullOrEmpty(allianceFortressData.Name))
        this.panel.leftName.text = allianceFortressData.Name;
      else
        this.panel.leftName.text = buildingStaticInfo.LocalName;
    }
    else
    {
      GameObject gameObject = this.RegenerateTargetGameObject("tiles_alliance_dragon_altar", this.panel.leftIcon.transform);
      if ((Object) gameObject != (Object) null)
      {
        gameObject.transform.localPosition = new Vector3(0.0f, 60f, 0.0f);
        gameObject.transform.localScale = Vector3.one * 0.6f;
        AllianceDragonAltarController component = gameObject.GetComponent<AllianceDragonAltarController>();
        if ((bool) ((Object) component))
          component.Reset();
      }
      this.panel.leftName.text = Utils.XLAT("alliance_altar_name");
    }
    this.panel.leftLocation.text = string.Format("X:{0}, Y:{1}", (object) location.X, (object) location.Y);
    this.data.leftLocation = location;
  }
}
