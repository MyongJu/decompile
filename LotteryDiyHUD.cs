﻿// Decompiled with JetBrains decompiler
// Type: LotteryDiyHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LotteryDiyHUD : MonoBehaviour
{
  private string vfxName = string.Empty;
  private const string vfxPath = "Prefab/LotteryDiy/";
  public GameObject rootNode;
  public UILabel remainTime;
  private GameObject vfxObj;

  private void OnEnable()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void OnSecondEvent(int time)
  {
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if ((UnityEngine.Object) this.remainTime != (UnityEngine.Object) null)
      this.remainTime.text = Utils.FormatTime(LotteryDiyPayload.Instance.LotteryData.EndTime - NetServerTime.inst.ServerTimestamp, true, false, true);
    if (LotteryDiyPayload.Instance.CanLotteryShow())
    {
      NGUITools.SetActive(this.rootNode, true);
      this.SetVFX(LotteryDiyPayload.Instance.LotteryData.ThemeEffect);
    }
    else
    {
      if (this.rootNode.activeInHierarchy)
        NGUITools.SetActive(this.rootNode, false);
      this.DeleteVFX();
    }
  }

  private void SetVFX(string vfxUsedName)
  {
    if (!(this.vfxName != vfxUsedName))
      return;
    this.DeleteVFX();
    this.vfxName = vfxUsedName;
    this.vfxObj = AssetManager.Instance.HandyLoad("Prefab/LotteryDiy/" + this.vfxName, typeof (GameObject)) as GameObject;
    if (!((UnityEngine.Object) this.vfxObj != (UnityEngine.Object) null))
      return;
    this.vfxObj = UnityEngine.Object.Instantiate<GameObject>(this.vfxObj);
    this.vfxObj.transform.parent = this.rootNode.transform;
    this.vfxObj.transform.localPosition = Vector3.zero;
    this.vfxObj.transform.localScale = Vector3.one;
  }

  private void DeleteVFX()
  {
    this.vfxName = string.Empty;
    if (!((UnityEngine.Object) this.vfxObj != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.vfxObj);
    this.vfxObj = (GameObject) null;
  }

  public void OnEntranceClicked()
  {
    LotteryDiyPayload.Instance.ShowFirstLotteryScene();
    OperationTrace.TraceClick(ClickArea.Lottery);
  }
}
