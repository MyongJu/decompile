﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentGem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using GemConstant;
using System.Collections;
using System.Collections.Generic;

public class ConfigEquipmentGem
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, ConfigEquipmentGemInfo> datas;
  private Dictionary<int, ConfigEquipmentGemInfo> dicByUniqueId;
  private Dictionary<int, ConfigEquipmentGemInfo> dicByItemId;
  private Dictionary<SlotType, List<ConfigEquipmentGemInfo>> dicBySlotType;

  public void BuildDB(object res)
  {
    this.parse.Parse<ConfigEquipmentGemInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.dicByItemId = new Dictionary<int, ConfigEquipmentGemInfo>();
    this.dicBySlotType = new Dictionary<SlotType, List<ConfigEquipmentGemInfo>>();
    Dictionary<int, ConfigEquipmentGemInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator();
    while (enumerator.MoveNext())
    {
      this.dicByItemId.Add(enumerator.Current.Value.itemID, enumerator.Current.Value);
      SlotType slotType = enumerator.Current.Value.slotType;
      if (!this.dicBySlotType.ContainsKey(slotType))
        this.dicBySlotType[slotType] = new List<ConfigEquipmentGemInfo>();
      this.dicBySlotType[slotType].Add(enumerator.Current.Value);
    }
  }

  public ConfigEquipmentGemInfo GetData(string id)
  {
    ConfigEquipmentGemInfo equipmentGemInfo = (ConfigEquipmentGemInfo) null;
    this.datas.TryGetValue(id, out equipmentGemInfo);
    return equipmentGemInfo;
  }

  public ConfigEquipmentGemInfo GetData(int internalId)
  {
    ConfigEquipmentGemInfo equipmentGemInfo = (ConfigEquipmentGemInfo) null;
    this.dicByUniqueId.TryGetValue(internalId, out equipmentGemInfo);
    return equipmentGemInfo;
  }

  public ConfigEquipmentGemInfo GetDataByItemId(int itemId)
  {
    ConfigEquipmentGemInfo equipmentGemInfo = (ConfigEquipmentGemInfo) null;
    this.dicByItemId.TryGetValue(itemId, out equipmentGemInfo);
    return equipmentGemInfo;
  }

  public List<ConfigEquipmentGemInfo> GetDatasBySlotType(SlotType slotType)
  {
    List<ConfigEquipmentGemInfo> equipmentGemInfoList1 = new List<ConfigEquipmentGemInfo>();
    Dictionary<SlotType, List<ConfigEquipmentGemInfo>>.Enumerator enumerator = this.dicBySlotType.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (slotType == enumerator.Current.Key)
      {
        List<ConfigEquipmentGemInfo> equipmentGemInfoList2 = this.dicBySlotType[enumerator.Current.Key];
        for (int index = 0; index < equipmentGemInfoList2.Count; ++index)
          equipmentGemInfoList1.Add(equipmentGemInfoList2[index]);
        break;
      }
    }
    return equipmentGemInfoList1;
  }

  public List<ConfigEquipmentGemInfo> GetDatas()
  {
    List<ConfigEquipmentGemInfo> equipmentGemInfoList = new List<ConfigEquipmentGemInfo>();
    Dictionary<string, ConfigEquipmentGemInfo>.Enumerator enumerator = this.datas.GetEnumerator();
    while (enumerator.MoveNext())
      equipmentGemInfoList.Add(enumerator.Current.Value);
    return equipmentGemInfoList;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, ConfigEquipmentGemInfo>) null;
    }
    if (this.dicByUniqueId != null)
    {
      this.dicByUniqueId.Clear();
      this.dicByUniqueId = (Dictionary<int, ConfigEquipmentGemInfo>) null;
    }
    if (this.dicByItemId != null)
    {
      this.dicByItemId.Clear();
      this.dicByItemId = (Dictionary<int, ConfigEquipmentGemInfo>) null;
    }
    if (this.dicBySlotType == null)
      return;
    this.dicBySlotType.Clear();
    this.dicBySlotType = (Dictionary<SlotType, List<ConfigEquipmentGemInfo>>) null;
  }
}
