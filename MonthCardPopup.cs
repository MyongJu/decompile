﻿// Decompiled with JetBrains decompiler
// Type: MonthCardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MonthCardPopup : Popup
{
  private List<int> separator = new List<int>();
  public List<MonthCardItem> mcitems = new List<MonthCardItem>(5);
  private const string gifticon = "Texture/ItemIcons/item_month_card_gift";
  private const string dockicon = "Texture/GUI_Textures/dock-1";
  private int curProsperity;
  private bool isactive;
  private int curLevel;
  public UIButton activeBtn;
  public UIButton collectBtn;
  public UILabel refreshTime;
  public UILabel remainingDay;
  public UILabel curPoint;
  public UISlider process;
  public Icon result;
  public UITexture dock;
  public GameObject activeGroup;
  public GameObject inactiveGroup;
  public GameObject hinteffect;

  public void OnActiveButtonClicked()
  {
    UIManager.inst.OpenPopup("MonthCardPurchasePopup", (Popup.PopupParameter) null);
  }

  public void OnCollectButtonClicked()
  {
    MessageHub.inst.GetPortByAction("Player:monthCardRewards").SendRequest((System.Action<bool, object>) ((ret, obj) =>
    {
      if (!ret)
        return;
      Dictionary<long, MonthCardInfo> allMonthCard = ConfigManager.inst.DB_MonthCard.GetAllMonthCard();
      Dictionary<long, int> dictionary = new Dictionary<long, int>();
      using (Dictionary<long, MonthCardInfo>.Enumerator enumerator = allMonthCard.GetEnumerator())
      {
        if (enumerator.MoveNext())
          dictionary = enumerator.Current.Value.rewards;
      }
      using (Dictionary<long, int>.Enumerator enumerator = dictionary.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, int> current = enumerator.Current;
          int num = current.Value * (this.curLevel + 1);
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem((int) current.Key);
          RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
          {
            icon = itemStaticInfo.ImagePath,
            count = (float) num
          });
        }
      }
      RewardsCollectionAnimator.Instance.CollectItems(false);
    }));
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  private float CalculateCurrentProcess()
  {
    int num1 = 0;
    float num2 = 0.0f;
    float num3 = (float) (1.0 / ((double) this.separator.Count - 1.0));
    for (int index = 0; index < this.separator.Count; ++index)
    {
      if (this.curProsperity < this.separator[index])
      {
        int num4 = this.separator[index];
        int num5 = this.separator[index - 1];
        num2 = (float) ((double) (num1 - 1) * (double) num3 + (double) (this.curProsperity - num5) / (double) (num4 - num5) * (double) num3);
        this.curLevel = index - 1;
        break;
      }
      ++num1;
      if (num1 == this.separator.Count)
      {
        num2 = 1f;
        this.curLevel = index;
      }
    }
    return num2;
  }

  private void Init()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.dock, "Texture/GUI_Textures/dock-1", (System.Action<bool>) null, true, false, string.Empty);
    this.separator.Clear();
    Dictionary<long, MonthCardInfo> allMonthCard = ConfigManager.inst.DB_MonthCard.GetAllMonthCard();
    using (Dictionary<long, MonthCardInfo>.Enumerator enumerator = allMonthCard.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.separator.Add(enumerator.Current.Value.level_req_min);
    }
    MonthCardData monthCardData = DBManager.inst.DB_MonthCard.Get(PlayerData.inst.uid);
    this.curProsperity = monthCardData.prosperityvalue;
    this.process.value = this.CalculateCurrentProcess();
    int index = 0;
    using (Dictionary<long, MonthCardInfo>.Enumerator enumerator = allMonthCard.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<long, MonthCardInfo> current = enumerator.Current;
        this.mcitems[index].GetComponent<Icon>().SetData("Texture/ItemIcons/item_month_card_gift", current.Value.level_req_min.ToString(), "X" + current.Value.reward_multi.ToString());
        this.mcitems[index].SetActiveState(index == this.curLevel);
        ++index;
      }
    }
    this.curPoint.text = this.curProsperity.ToString();
    this.result.SetData("Texture/ItemIcons/item_month_card_gift", new string[1]
    {
      "X" + (this.curLevel + 1).ToString()
    });
    this.isactive = NetServerTime.inst.UpdateTime < monthCardData.expiration_date;
    this.activeGroup.SetActive(this.isactive);
    this.inactiveGroup.SetActive(!this.isactive);
    this.UpdateTime(0);
    if (!this.isactive)
      return;
    int num = (int) (monthCardData.expiration_date - NetServerTime.inst.UpdateTime) / 86400 + 1;
    this.remainingDay.text = num.ToString();
    if (num >= 7)
      this.remainingDay.color = Color.green;
    else if (num < 7 && num >= 2)
      this.remainingDay.color = Color.yellow;
    else
      this.remainingDay.color = Color.red;
    if (num > 2)
      this.hinteffect.SetActive(false);
    else
      this.hinteffect.SetActive(true);
    this.collectBtn.isEnabled = !NetServerTime.inst.IsToday(monthCardData.last_award_time);
  }

  private void UpdateTime(int obj)
  {
    this.refreshTime.text = Utils.FormatTime(NetServerTime.inst.TodayLeftTimeUTC(), false, false, true);
  }

  private void OnDBChanged(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.Init();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
    Oscillator.Instance.secondEvent += new System.Action<int>(this.UpdateTime);
    DBManager.inst.DB_MonthCard.onDataUpdated += new System.Action<long>(this.OnDBChanged);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.UpdateTime);
    DBManager.inst.DB_MonthCard.onDataUpdated -= new System.Action<long>(this.OnDBChanged);
  }
}
