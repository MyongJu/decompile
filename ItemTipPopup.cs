﻿// Decompiled with JetBrains decompiler
// Type: ItemTipPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ItemTipPopup : Popup
{
  private BagType bagType = BagType.DragonKnight;
  private Queue<ItemTipPopup.DelayTask> _task = new Queue<ItemTipPopup.DelayTask>();
  public UIGrid UIRoot;
  private int _itemId;
  public ItemTipBaseInfo normalInfo;
  public ItemTipEquipInfo equipInfo;
  public ItemTipScrollInfo scrollInfo;
  public ItemTipChipInfo chipInfo;
  public EquipmentBenefits benefitContent;
  public ItemTipHeroChipInfo heroChipInfo;
  public ItemTipHeroInfo heroInfo;
  public ItemTipCustomInfo customInfo;
  public ItemTipLordTitleInfo titleInfo;
  public ItemTipContainer[] containers;
  private EquipmentSuitItem suitItem;
  private EquipmentGemItem gemItem;
  private int suitGroupId;
  private Benefits benefits;
  private ConfigEquipmentScrollInfo scrollStaticInfo;
  private ConfigEquipmentMainInfo equipmentInfo;
  private InventoryItemData inventoryItemData;
  private bool hadGemSlots;
  private int _index;
  private Transform focus;
  private bool _flag;
  public long equipmentId;
  private long userId;
  private int _enhanced;
  private bool _customTip;
  private ItemTipCustomInfo.ItemTipData _customItemTipData;
  private ItemTipPopup.Type _currentType;
  private bool _isDestroy;

  private void Update()
  {
    if (!this._flag || this._task.Count == 0)
      return;
    List<ItemTipPopup.DelayTask> delayTaskList = new List<ItemTipPopup.DelayTask>();
    while (this._task.Count > 0)
    {
      ItemTipPopup.DelayTask delayTask = this._task.Dequeue();
      if (!delayTask.Execute())
        delayTaskList.Add(delayTask);
    }
    this._task.Clear();
    if (delayTaskList.Count <= 0)
      return;
    for (int index = 0; index < delayTaskList.Count; ++index)
      this._task.Enqueue(delayTaskList[index]);
  }

  private void AddTask(int delay, System.Action handler)
  {
    this._task.Enqueue(new ItemTipPopup.DelayTask()
    {
      DelayFrame = delay,
      OnExecute = handler
    });
  }

  private ItemTipContainer Current
  {
    get
    {
      if (this.containers.Length > this._index)
        return this.containers[this._index];
      return this.containers[this.containers.Length - 1];
    }
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    ItemTipPopup.Parameter parameter = orgParam as ItemTipPopup.Parameter;
    this._itemId = parameter.itemId;
    this.focus = parameter.focus;
    this.equipmentId = parameter.equipmentId;
    this.userId = parameter.userId;
    this._enhanced = parameter.enhanced;
    this._customTip = parameter.customTip;
    this._customItemTipData = parameter.customItemTipData;
    this.inventoryItemData = parameter.inventoryItemData;
    this.ResetItems();
    this.AddEventHandler();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
  }

  private void Check()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._itemId);
    GameObject child = (GameObject) null;
    if (itemStaticInfo != null)
    {
      ItemBag.ItemType type = itemStaticInfo.Type;
      switch (type)
      {
        case ItemBag.ItemType.equipment:
label_4:
          this._currentType = ItemTipPopup.Type.Equip;
          NGUITools.SetActive(this.equipInfo.gameObject, true);
          this.equipInfo.SetData(this._itemId, this.equipmentId, this.userId, this._enhanced);
          this.benefits = this.equipInfo.GetBenefit();
          child = this.equipInfo.gameObject;
          this.suitGroupId = this.equipInfo.GetEquipSuitID();
          this.inventoryItemData = this.inventoryItemData == null ? this.equipInfo.GetInventoryItemData() : this.inventoryItemData;
          this.equipmentInfo = this.equipInfo.GetEquipmentInfo();
          this.hadGemSlots = this.equipInfo.HadGemSlots();
          break;
        case ItemBag.ItemType.equipment_scroll_chip:
label_6:
          this._currentType = ItemTipPopup.Type.Chip;
          NGUITools.SetActive(this.chipInfo.gameObject, true);
          this.chipInfo.SetData(this._itemId);
          child = this.chipInfo.gameObject;
          this.benefits = this.chipInfo.GetBenefit();
          this.suitGroupId = this.chipInfo.GetEquipSuitID();
          this.scrollStaticInfo = this.chipInfo.GetScrollInfo();
          this.hadGemSlots = this.chipInfo.HadGemSlots();
          break;
        case ItemBag.ItemType.equipment_scroll:
label_5:
          this._currentType = ItemTipPopup.Type.Scroll;
          NGUITools.SetActive(this.scrollInfo.gameObject, true);
          this.scrollInfo.SetData(this._itemId);
          child = this.scrollInfo.gameObject;
          this.benefits = this.scrollInfo.GetBenefit();
          this.suitGroupId = this.scrollInfo.GetEquipSuitID();
          this.scrollStaticInfo = this.scrollInfo.GetScrollInfo();
          this.hadGemSlots = this.scrollInfo.HadGemSlots();
          break;
        default:
          switch (type - 102)
          {
            case ItemBag.ItemType.Invalid:
              this._currentType = ItemTipPopup.Type.Hero;
              NGUITools.SetActive(this.heroInfo.gameObject, true);
              this.heroInfo.SetData(this._itemId);
              child = this.heroInfo.gameObject;
              break;
            case ItemBag.ItemType.refresh:
              this._currentType = ItemTipPopup.Type.HeroChip;
              NGUITools.SetActive(this.heroChipInfo.gameObject, true);
              this.heroChipInfo.SetData(this._itemId);
              child = this.heroChipInfo.gameObject;
              break;
            case ItemBag.ItemType.heroxp:
              this._currentType = ItemTipPopup.Type.LordTitle;
              NGUITools.SetActive(this.titleInfo.gameObject, true);
              this.titleInfo.SetData(this._itemId);
              child = this.titleInfo.gameObject;
              this.benefits = this.titleInfo.GetBenefit();
              break;
            default:
              switch (type - 84)
              {
                case ItemBag.ItemType.Invalid:
                  goto label_4;
                case ItemBag.ItemType.refresh:
                  goto label_6;
                case ItemBag.ItemType.speedup:
                  goto label_5;
                default:
                  this._currentType = ItemTipPopup.Type.Normal;
                  NGUITools.SetActive(this.normalInfo.gameObject, true);
                  this.normalInfo.SetData(this._itemId);
                  child = this.normalInfo.gameObject;
                  break;
              }
          }
      }
      if (itemStaticInfo.Type == ItemBag.ItemType.dk_equipment || itemStaticInfo.Type == ItemBag.ItemType.dk_equipment_scroll || itemStaticInfo.Type == ItemBag.ItemType.dk_equipment_scroll_chip)
        this.bagType = BagType.DragonKnight;
      else if (itemStaticInfo.Type == ItemBag.ItemType.equipment || itemStaticInfo.Type == ItemBag.ItemType.equipment_scroll || itemStaticInfo.Type == ItemBag.ItemType.equipment_scroll_chip)
        this.bagType = BagType.Hero;
    }
    else if (this._customTip)
    {
      this._currentType = ItemTipPopup.Type.Custom;
      NGUITools.SetActive(this.customInfo.gameObject, true);
      this.customInfo.SetData(this._customItemTipData);
      child = this.customInfo.gameObject;
    }
    if (!((UnityEngine.Object) child != (UnityEngine.Object) null))
      return;
    this.Current.AddChild(child);
  }

  private void ResetItems()
  {
    NGUITools.SetActive(this.normalInfo.gameObject, false);
    NGUITools.SetActive(this.equipInfo.gameObject, false);
    NGUITools.SetActive(this.scrollInfo.gameObject, false);
    NGUITools.SetActive(this.chipInfo.gameObject, false);
    NGUITools.SetActive(this.heroInfo.gameObject, false);
    NGUITools.SetActive(this.titleInfo.gameObject, false);
    this.Check();
  }

  private void OnDestroy()
  {
    this._isDestroy = true;
  }

  private void RefreshCurrentContainer()
  {
    this.Current.RefreshTable();
  }

  private void UpdateUI()
  {
    int num1 = 1;
    if (this._currentType == ItemTipPopup.Type.Hero || this._currentType == ItemTipPopup.Type.HeroChip)
      this.AddTask(num1++, new System.Action(this.RefreshCurrentContainer));
    this._flag = true;
    int delay1 = num1;
    int num2 = delay1 + 1;
    this.AddTask(delay1, new System.Action(this.AddBenefits));
    int delay2 = num2;
    int num3 = delay2 + 1;
    this.AddTask(delay2, new System.Action(this.AddGems));
    int delay3 = num3;
    int num4 = delay3 + 1;
    this.AddTask(delay3, new System.Action(this.AddSuit));
    int delay4 = num4;
    int num5 = delay4 + 1;
    this.AddTask(delay4, new System.Action(this.ResetPosition));
  }

  private void ResetPosition()
  {
    if (this._isDestroy)
      return;
    this.transform.InverseTransformPoint(this.focus.position);
    NGUIMath.CalculateRelativeWidgetBounds(this.focus);
    Bounds relativeWidgetBounds1 = NGUIMath.CalculateRelativeWidgetBounds(this.transform, this.focus);
    Bounds relativeWidgetBounds2 = NGUIMath.CalculateRelativeWidgetBounds(this.UIRoot.transform);
    int num1 = UIManager.inst.uiRoot.manualWidth / 2;
    int num2 = UIManager.inst.uiRoot.manualHeight / 2;
    Vector3 localPosition = this.UIRoot.transform.localPosition;
    int x1 = (int) relativeWidgetBounds1.max.x;
    int y1 = (int) relativeWidgetBounds1.max.y;
    int x2 = (int) relativeWidgetBounds1.min.x;
    int y2 = (int) relativeWidgetBounds1.min.y;
    int x3 = (int) relativeWidgetBounds2.size.x;
    int y3 = (int) relativeWidgetBounds2.size.y;
    int num3 = x1 + x3 >= num1 ? (x2 - x3 <= -num1 ? -num1 : x2 - x3) : x1;
    int num4 = y1 - y3 <= -num2 ? (y2 + y3 >= num2 ? num2 : y2 + y3) : y1;
    localPosition.x = (float) num3;
    localPosition.y = (float) num4;
    this.UIRoot.transform.localPosition = localPosition;
  }

  private void AddEventHandler()
  {
  }

  private void RemoveEventHandler()
  {
  }

  private void AddBenefits()
  {
    if (this._isDestroy || this.benefits == null)
      return;
    NGUITools.SetActive(this.benefitContent.gameObject, true);
    this.benefitContent.Init(this.benefits);
    this.Current.AddChild(this.benefitContent.gameObject);
  }

  private void AddGems()
  {
    if (this._isDestroy || !this.hadGemSlots)
      return;
    GameObject gameObject = Utils.DuplicateGOB(AssetManager.Instance.Load("Prefab/UI/Common/EquipmentGemItem", (System.Type) null) as GameObject, this.UIRoot.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    AssetManager.Instance.UnLoadAsset("Prefab/UI/Common/EquipmentGemItem", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    this.gemItem = gameObject.GetComponent<EquipmentGemItem>();
    if (this.inventoryItemData != null)
      this.gemItem.SetData(this.inventoryItemData, false, (UIScrollView) null);
    else if (this.scrollStaticInfo != null)
      this.gemItem.SetData(this.bagType, this.scrollStaticInfo, (UIScrollView) null);
    else if (this.equipmentInfo != null)
      this.gemItem.SetData(this.bagType, this.equipmentInfo.internalId, (UIScrollView) null);
    if (this.Current.IsFull(this.gemItem.gameObject, this.focus.localPosition))
      this.NextContainer();
    this.Current.AddChild(this.gemItem.gameObject);
    Vector3 localScale = gameObject.transform.localScale;
    localScale.x = localScale.y = 0.68f;
    localScale.z = 1f;
    gameObject.transform.localScale = localScale;
  }

  private void AddSuit()
  {
    if (this._isDestroy || this.suitGroupId <= 0)
      return;
    GameObject gameObject = Utils.DuplicateGOB(AssetManager.Instance.Load("Prefab/UI/Common/EquipmentSuitItem", (System.Type) null) as GameObject, this.UIRoot.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    AssetManager.Instance.UnLoadAsset("Prefab/UI/Common/EquipmentSuitItem", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    this.suitItem = gameObject.GetComponent<EquipmentSuitItem>();
    if (this.inventoryItemData != null)
      this.suitItem.SetData(this.inventoryItemData.uid, this.bagType, this.suitGroupId, this.inventoryItemData);
    else
      this.suitItem.SetData(PlayerData.inst.uid, this.bagType, this.suitGroupId, (InventoryItemData) null);
    if (this.Current.IsFull(this.suitItem.gameObject, this.focus.localPosition))
      this.NextContainer();
    this.Current.AddChild(this.suitItem.gameObject);
    Vector3 localScale = gameObject.transform.localScale;
    localScale.x = localScale.y = 0.68f;
    localScale.z = 1f;
    gameObject.transform.localScale = localScale;
  }

  private void NextContainer()
  {
    ++this._index;
    NGUITools.SetActive(this.Current.gameObject, true);
  }

  private void AddEnhand()
  {
  }

  private void SetPosition()
  {
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.RemoveEventHandler();
    if ((UnityEngine.Object) this.normalInfo != (UnityEngine.Object) null)
    {
      this.normalInfo.Dispose();
      this.normalInfo = (ItemTipBaseInfo) null;
    }
    if ((UnityEngine.Object) this.heroChipInfo != (UnityEngine.Object) null)
    {
      this.heroChipInfo.Dispose();
      this.heroChipInfo = (ItemTipHeroChipInfo) null;
    }
    if (!((UnityEngine.Object) this.heroInfo != (UnityEngine.Object) null))
      return;
    this.heroInfo.Dispose();
    this.heroInfo = (ItemTipHeroInfo) null;
  }

  private class DelayTask
  {
    public int DelayFrame = 1;
    public System.Action OnExecute;

    public bool Execute()
    {
      if (this.DelayFrame > 0)
      {
        --this.DelayFrame;
        return false;
      }
      if (this.OnExecute != null)
        this.OnExecute();
      return true;
    }
  }

  private enum Type
  {
    Normal,
    Equip,
    Scroll,
    Hero,
    HeroChip,
    Chip,
    LordTitle,
    Custom,
  }

  public class Parameter : Popup.PopupParameter
  {
    public int itemId;
    public Transform focus;
    public long equipmentId;
    public long userId;
    public int enhanced;
    public bool customTip;
    public ItemTipCustomInfo.ItemTipData customItemTipData;
    public InventoryItemData inventoryItemData;
  }
}
