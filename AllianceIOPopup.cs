﻿// Decompiled with JetBrains decompiler
// Type: AllianceIOPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;
using UnityEngine;

public class AllianceIOPopup : BaseReportPopup
{
  public AllianceInfo ai;
  public UILabel content;

  private void Init()
  {
    this.ai.FeedData<AllianceInfoData>(this.param.hashtable[(object) "data"] as Hashtable);
    this.content.text = this.param.mail.GetBodyString();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  private void Update()
  {
    if (!(bool) ((Object) this.ai))
      return;
    if (UIManager.inst.IsTopPopup(this.ID))
      this.ai.ShowAllianceSymbol();
    else
      this.ai.HideAllianceSymbol();
  }
}
