﻿// Decompiled with JetBrains decompiler
// Type: ConfigHelp
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigHelp
{
  private Dictionary<string, HelpInfo> datas;
  private Dictionary<int, HelpInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<HelpInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public void Clear()
  {
    if (this.datas != null)
      this.datas.Clear();
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
  }

  public HelpInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (HelpInfo) null;
  }

  public HelpInfo GetData(int internalId)
  {
    if (this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (HelpInfo) null;
  }
}
