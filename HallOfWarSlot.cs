﻿// Decompiled with JetBrains decompiler
// Type: HallOfWarSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UI;
using UnityEngine;

public class HallOfWarSlot : MonoBehaviour
{
  private long _rallyId;
  private int _rallyWaitStartTime;
  private int _rallyWaitEndTime;
  private RallyData.RallyState state;
  [SerializeField]
  private HallOfWarSlot.Panel panel;
  private bool secondFlag;

  public void Setup(long rallyId)
  {
    this._rallyId = rallyId;
    this.RefreshData();
  }

  private void RefreshData()
  {
    if (this._rallyId == -1L)
      return;
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this._rallyId);
    if (rallyData == null)
    {
      if (!UIManager.inst.IsTheCurrentDialog("RallyDlg"))
        return;
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
    {
      UserData userData1 = DBManager.inst.DB_User.Get(rallyData.ownerUid);
      if (userData1 != null)
      {
        this.panel.rallyOwnerName.text = userData1.userName;
        CityData cityData = DBManager.inst.DB_City.Get(rallyData.ownerCityId);
        if (cityData != null)
        {
          this.panel.rallyCityName.text = cityData.cityName;
          this.panel.rallyOwnerCoordinateK.text = cityData.cityLocation.K.ToString();
          this.panel.rallyOwnerCoordinateX.text = cityData.cityLocation.X.ToString();
          this.panel.rallyOwnerCoordinateY.text = cityData.cityLocation.Y.ToString();
        }
      }
      UserData userData2 = DBManager.inst.DB_User.Get(rallyData.targetUid);
      if (userData2 != null)
      {
        this.panel.rallyTargetName.text = userData2.userName;
        CityData cityData = DBManager.inst.DB_City.Get(rallyData.targetCityId);
        if (cityData != null)
        {
          this.panel.rallyTargetCityName.text = cityData.cityName;
          this.panel.rallyTargetCoordinateK.text = cityData.cityLocation.K.ToString();
          this.panel.rallyTargetCoordinateX.text = cityData.cityLocation.X.ToString();
          this.panel.rallyTargetCoordinateY.text = cityData.cityLocation.Y.ToString();
        }
      }
      this._rallyWaitStartTime = rallyData.waitTimeDuration.startTime;
      this._rallyWaitEndTime = rallyData.waitTimeDuration.endTime;
      if (rallyData.state == RallyData.RallyState.waitRallying && !this.secondFlag)
      {
        Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
        this.secondFlag = true;
      }
      this.panel.troopCount.text = rallyData.totalTroopsCount.ToString();
      this.state = rallyData.state;
      this.OnSecond(NetServerTime.inst.ServerTimestamp);
    }
  }

  public void ClickRallyDetailButton()
  {
    UIManager.inst.OpenDlg("RallyDlg", (UI.Dialog.DialogParameter) new PopupRallyDlg.Parameter()
    {
      rallyId = this._rallyId
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void ClickDefenseDetailButton()
  {
    UIManager.inst.OpenDlg("DefenseDlg", (UI.Dialog.DialogParameter) new PopupDefenseDlg.Parameter()
    {
      rallyId = this._rallyId
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnRallyStateChanged(long rallyId)
  {
    this.RefreshData();
  }

  public void OnSecond(int serverTime)
  {
    if (this.state == RallyData.RallyState.waitRallying)
    {
      if (this._rallyWaitEndTime <= serverTime)
        return;
      this.panel.marchLeftTimeText.text = Utils.FormatTime(this._rallyWaitEndTime - serverTime, false, false, true);
      if (this._rallyWaitEndTime != this._rallyWaitStartTime)
        this.panel.marchLeftTimeSlider.value = 1f - Mathf.Clamp01((float) (this._rallyWaitEndTime - serverTime) / (float) (this._rallyWaitEndTime - this._rallyWaitStartTime));
      if (this.secondFlag)
        return;
      Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
      this.secondFlag = true;
    }
    else
    {
      this.panel.marchLeftTimeText.text = "Time's up";
      this.panel.marchLeftTimeSlider.value = 1f;
      if (!this.secondFlag)
        return;
      if (Oscillator.IsAvailable)
        Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
      this.secondFlag = false;
    }
  }

  public void OnEnable()
  {
    DBManager.inst.DB_Rally.onRallyDataChanged += new System.Action<long>(this.OnRallyStateChanged);
    if (this.secondFlag)
      return;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    this.secondFlag = true;
  }

  public void OnDisable()
  {
    DBManager.inst.DB_Rally.onRallyDataChanged -= new System.Action<long>(this.OnRallyStateChanged);
    if (!this.secondFlag)
      return;
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    this.secondFlag = false;
  }

  [Serializable]
  protected class Panel
  {
    public UILabel rallyOwnerName;
    public UILabel rallyCityName;
    public UILabel rallyOwnerCoordinateK;
    public UILabel rallyOwnerCoordinateX;
    public UILabel rallyOwnerCoordinateY;
    public UILabel rallyTargetName;
    public UILabel rallyTargetCityName;
    public UILabel rallyTargetCoordinateK;
    public UILabel rallyTargetCoordinateX;
    public UILabel rallyTargetCoordinateY;
    public UILabel troopCount;
    public UISlider marchLeftTimeSlider;
    public UILabel marchLeftTimeText;
    public UIButton BT_detail;
  }
}
