﻿// Decompiled with JetBrains decompiler
// Type: PitExplorePayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PitExplorePayload
{
  private PitActivityData _activityData = new PitActivityData();
  private const int PRE_START_LIMIT_TIME = 60;
  private static PitExplorePayload _instance;
  private int _freeBackCount;
  private int _pitEndTime;
  private int _pitTeleportTimes;
  private int _pitSpeedUpTimes;

  public static PitExplorePayload Instance
  {
    get
    {
      if (PitExplorePayload._instance == null)
        PitExplorePayload._instance = new PitExplorePayload();
      return PitExplorePayload._instance;
    }
  }

  public int FreeBackCount
  {
    set
    {
      this._freeBackCount = value;
    }
    get
    {
      return this._freeBackCount;
    }
  }

  public int MaxFreeBackCount
  {
    get
    {
      int num = 0;
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("abyss_backhome_free_times");
      if (data != null)
        num = data.ValueInt;
      return num;
    }
  }

  public int GetLeavePitCost()
  {
    if (this.FreeBackCount > 0)
      return 0;
    int num = 0;
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("abyss_backhome_cost_gold");
    if (data != null)
      num = data.ValueInt;
    return num;
  }

  public int PitEndTime
  {
    set
    {
      this._pitEndTime = value;
    }
    get
    {
      return this._pitEndTime;
    }
  }

  public int PitTeleportTimes
  {
    set
    {
      this._pitTeleportTimes = value;
    }
    get
    {
      return this._pitTeleportTimes;
    }
  }

  public int PitTeleportMaxTimes
  {
    get
    {
      int num = 99999;
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("abyss_city_teleport_max_times");
      if (data != null)
        num = data.ValueInt;
      return num;
    }
  }

  public int PitTeleportCost
  {
    get
    {
      return ConfigManager.inst.DB_AbyssTeleport.GetCost(this.PitTeleportTimes + 1);
    }
  }

  public int PitSpeedUpCost
  {
    get
    {
      return ConfigManager.inst.DB_AbyssSpeedUp.GetCost(this.PitSpeedUpTimes + 1);
    }
  }

  public int PitSpeedUpTimes
  {
    set
    {
      this._pitSpeedUpTimes = value;
    }
    get
    {
      return this._pitSpeedUpTimes;
    }
  }

  public int PitSpeedUpMaxTimes
  {
    get
    {
      int num = 99999;
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("abyss_march_speedup_max_times");
      if (data != null)
        num = data.ValueInt;
      return num;
    }
  }

  public double PitSpeedUpPercent
  {
    get
    {
      double num = 0.0;
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("abyss_march_speedup_percent");
      if (data != null)
        num = data.Value;
      return num;
    }
  }

  public PitActivityData ActivityData
  {
    get
    {
      return this._activityData;
    }
  }

  public int CurrentActivityRound
  {
    get
    {
      if (this._activityData.Duration > 0 && this._activityData.StartTime > 0 && (NetServerTime.inst.ServerTimestamp >= this._activityData.StartTime && NetServerTime.inst.ServerTimestamp < this._activityData.EndTime))
        return (NetServerTime.inst.ServerTimestamp - this._activityData.StartTime) / this._activityData.Duration + 1;
      return 0;
    }
  }

  public bool CanSignup
  {
    get
    {
      if (this.IsLastRound || NetServerTime.inst.ServerTimestamp < this._activityData.RegStartTime)
        return false;
      return this.NextRoundStartTime - 60 > 0;
    }
  }

  public bool IsLastRound
  {
    get
    {
      if (this.CurrentActivityRound > 0)
        return this.CurrentActivityRound == this._activityData.Round;
      return false;
    }
  }

  public bool CurrentActivitySigned
  {
    get
    {
      if (this._activityData.CurSignupData.SignupTime >= this._activityData.RegStartTime)
        return this._activityData.CurSignupData.SignupTime < this._activityData.RegEndTime;
      return false;
    }
  }

  public int CurrentRoundEndTime
  {
    get
    {
      return this.CurrentActivityRound * this._activityData.Duration + this._activityData.StartTime - NetServerTime.inst.ServerTimestamp;
    }
  }

  public int NextRoundStartTime
  {
    get
    {
      if (this.IsLastRound)
        return this._activityData.StartTime + this._activityData.Interval - NetServerTime.inst.ServerTimestamp;
      return this.CurrentRoundEndTime;
    }
  }

  public int NeedGoldAmount2Signup
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("abyss_sign_up_cost");
      if (data != null)
        return data.ValueInt;
      return 0;
    }
  }

  public int SignupMaxMembers
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("abyss_players_num_max");
      if (data != null)
        return data.ValueInt;
      return 0;
    }
  }

  private void RequestServerData(System.Action<bool, object> callback = null)
  {
    if (callback != null)
      MessageHub.inst.GetPortByAction("Abyss:getActivityInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          this.Decode(data as Hashtable);
        if (callback == null)
          return;
        callback(ret, data);
      }), true);
    else
      MessageHub.inst.GetPortByAction("Abyss:getActivityInfo").SendLoader((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.Decode(data as Hashtable);
      }), false, false);
  }

  public void Decode(Hashtable data)
  {
    if (data == null)
      return;
    this._activityData.Decode(data);
  }

  public void ShowPitActivityScene()
  {
    this.RequestServerData((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("Pit/PitExploreDetailsDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    }));
  }

  public void ShowPitSignupScene(System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("Abyss:getRegInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable inData = data as Hashtable;
      if (inData == null)
        return;
      int outData = 0;
      ArrayList data1 = new ArrayList();
      List<long> longList = new List<long>();
      DatabaseTools.UpdateData(inData, "reg_num", ref outData);
      if (inData.ContainsKey((object) "alliance_users"))
        DatabaseTools.CheckAndParseOrgData(inData[(object) "alliance_users"], out data1);
      if (data1 != null && data1.Count > 0)
      {
        for (int index = 0; index < data1.Count; ++index)
        {
          long result = 0;
          long.TryParse(data1[index].ToString(), out result);
          if (!longList.Contains(result))
            longList.Add(result);
        }
      }
      UIManager.inst.OpenPopup("Pit/PitExploreSignUpPopup", (Popup.PopupParameter) new PitExploreSignUpPopup.Parameter()
      {
        signupCount = outData,
        signupAllianceMembers = longList,
        signedCallback = callback
      });
    }), true);
  }

  public void ShowPitRankHistory()
  {
    MessageHub.inst.GetPortByAction("Abyss:getRankHistory").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      ArrayList arrayList1 = data as ArrayList;
      List<KeyValuePair<int, List<PitExplorePayload.PitRankDataWithTime>>> keyValuePairList = new List<KeyValuePair<int, List<PitExplorePayload.PitRankDataWithTime>>>();
      if (arrayList1 != null)
      {
        for (int index1 = 0; index1 < arrayList1.Count; ++index1)
        {
          Hashtable hashtable = arrayList1[index1] as Hashtable;
          if (hashtable != null)
          {
            int result1 = 0;
            int result2 = 0;
            if (hashtable.ContainsKey((object) "start_time"))
              int.TryParse(hashtable[(object) "start_time"].ToString(), out result1);
            if (hashtable.ContainsKey((object) "end_time"))
              int.TryParse(hashtable[(object) "end_time"].ToString(), out result2);
            ArrayList arrayList2 = hashtable[(object) "list"] as ArrayList;
            if (arrayList2 != null && arrayList2.Count > 0)
            {
              List<PitExplorePayload.PitRankDataWithTime> rankDataWithTimeList = new List<PitExplorePayload.PitRankDataWithTime>();
              for (int index2 = 0; index2 < arrayList2.Count; ++index2)
              {
                Hashtable data1 = arrayList2[index2] as Hashtable;
                PitExplorePayload.PitRankData pitRankData = new PitExplorePayload.PitRankData();
                pitRankData.Decode(data1);
                rankDataWithTimeList.Add(new PitExplorePayload.PitRankDataWithTime(result1, result2, pitRankData));
              }
              keyValuePairList.Add(new KeyValuePair<int, List<PitExplorePayload.PitRankDataWithTime>>(result1, rankDataWithTimeList));
            }
          }
        }
      }
      UIManager.inst.OpenPopup("Pit/PitExploreViewRankPopup", (Popup.PopupParameter) new PitExploreViewRankPopup.Parameter()
      {
        rankDataList = keyValuePairList
      });
    }), true);
  }

  public void EnterPitExplore()
  {
    if (PlayerData.inst.IsInPit)
    {
      UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
      {
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PitMode;
        UIManager.inst.Cloud.PokeCloud((System.Action) (() => PVPSystem.Instance.Map.GotoLocation(PlayerData.inst.playerCityData.cityLocation, false)));
      }));
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else if (PlayerData.inst.userData.world_id != PlayerData.inst.CityData.Location.K)
      UIManager.inst.toast.Show(Utils.XLAT("exception_2300160"), (System.Action) null, 4f, false);
    else
      MessageHub.inst.GetPortByAction("Abyss:enter").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Lite);
      }), true);
  }

  public void LeavePitExplore()
  {
    if (!PlayerData.inst.IsInPit)
      return;
    UIManager.inst.OpenPopup("Pit/PitGoHomePopup", (Popup.PopupParameter) null);
  }

  public void SyncAbyssInfo()
  {
    if (!PlayerData.inst.IsInPit)
      return;
    MessageHub.inst.GetPortByAction("Abyss:getAbyssInfo").SendLoader((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable hashtable = data as Hashtable;
      if (hashtable == null)
        return;
      if (hashtable.ContainsKey((object) "free_leave"))
        PitExplorePayload.Instance.FreeBackCount = int.Parse(hashtable[(object) "free_leave"] as string);
      if (hashtable.ContainsKey((object) "march_speedup"))
        PitExplorePayload.Instance.PitSpeedUpTimes = int.Parse(hashtable[(object) "march_speedup"] as string);
      if (hashtable.ContainsKey((object) "teleport"))
        PitExplorePayload.Instance.PitTeleportTimes = int.Parse(hashtable[(object) "teleport"] as string);
      if (!hashtable.ContainsKey((object) "end_time"))
        return;
      PitExplorePayload.Instance.PitEndTime = int.Parse(hashtable[(object) "end_time"] as string);
    }), true, false);
  }

  public class PitRankDataWithTime
  {
    public int startTime;
    public int endTime;
    public PitExplorePayload.PitRankData pitRankData;

    public PitRankDataWithTime(int startTime, int endTime, PitExplorePayload.PitRankData pitRankData)
    {
      this.startTime = startTime;
      this.endTime = endTime;
      this.pitRankData = pitRankData;
    }
  }

  public class PitRankData
  {
    private int _worldId;
    private string _name;
    private string _acronym;
    private float _score;

    public int WorldId
    {
      get
      {
        return this._worldId;
      }
    }

    public string Name
    {
      get
      {
        return this._name;
      }
    }

    public string Acronym
    {
      get
      {
        return this._acronym;
      }
    }

    public float Score
    {
      get
      {
        return Mathf.Floor(this._score);
      }
    }

    public void Decode(Hashtable data)
    {
      if (data == null)
        return;
      DatabaseTools.UpdateData(data, "world_id", ref this._worldId);
      DatabaseTools.UpdateData(data, "name", ref this._name);
      DatabaseTools.UpdateData(data, "acronym", ref this._acronym);
      DatabaseTools.UpdateData(data, "score", ref this._score);
    }

    private class Params
    {
      public const string WORLD_ID = "world_id";
      public const string NAME = "name";
      public const string ACRONYM = "acronym";
      public const string SCORE = "score";
    }
  }
}
