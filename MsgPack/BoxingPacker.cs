﻿// Decompiled with JetBrains decompiler
// Type: MsgPack.BoxingPacker
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace MsgPack
{
  public class BoxingPacker
  {
    private static Type KeyValuePairDefinitionType = typeof (KeyValuePair<object, object>).GetGenericTypeDefinition();

    public void Pack(Stream strm, object o)
    {
      this.Pack(new MsgPackWriter(strm), o);
    }

    public byte[] Pack(object o)
    {
      using (MemoryStream memoryStream = new MemoryStream())
      {
        this.Pack((Stream) memoryStream, o);
        return memoryStream.ToArray();
      }
    }

    private void Pack(MsgPackWriter writer, object o)
    {
      if (o == null)
      {
        writer.WriteNil();
      }
      else
      {
        Type type = o.GetType();
        if (type.IsPrimitive)
        {
          if (type.Equals(typeof (int)))
            writer.Write((int) o);
          else if (type.Equals(typeof (uint)))
            writer.Write((uint) o);
          else if (type.Equals(typeof (float)))
            writer.Write((float) o);
          else if (type.Equals(typeof (double)))
            writer.Write((double) o);
          else if (type.Equals(typeof (long)))
            writer.Write((long) o);
          else if (type.Equals(typeof (ulong)))
            writer.Write((ulong) o);
          else if (type.Equals(typeof (bool)))
            writer.Write((bool) o);
          else if (type.Equals(typeof (byte)))
            writer.Write((byte) o);
          else if (type.Equals(typeof (sbyte)))
            writer.Write((sbyte) o);
          else if (type.Equals(typeof (short)))
          {
            writer.Write((short) o);
          }
          else
          {
            if (!type.Equals(typeof (ushort)))
              throw new NotSupportedException();
            writer.Write((ushort) o);
          }
        }
        else
        {
          IDictionary dictionary = o as IDictionary;
          if (dictionary != null)
          {
            writer.WriteMapHeader(dictionary.Count);
            foreach (DictionaryEntry dictionaryEntry in dictionary)
            {
              this.Pack(writer, dictionaryEntry.Key);
              this.Pack(writer, dictionaryEntry.Value);
            }
          }
          else
          {
            if (!type.IsArray)
              return;
            Array array = (Array) o;
            Type elementType = type.GetElementType();
            if (elementType.IsGenericType && elementType.GetGenericTypeDefinition().Equals(BoxingPacker.KeyValuePairDefinitionType))
            {
              PropertyInfo property1 = elementType.GetProperty("Key");
              PropertyInfo property2 = elementType.GetProperty("Value");
              writer.WriteMapHeader(array.Length);
              for (int index = 0; index < array.Length; ++index)
              {
                object obj = array.GetValue(index);
                this.Pack(writer, property1.GetValue(obj, (object[]) null));
                this.Pack(writer, property2.GetValue(obj, (object[]) null));
              }
            }
            else
            {
              writer.WriteArrayHeader(array.Length);
              for (int index = 0; index < array.Length; ++index)
                this.Pack(writer, array.GetValue(index));
            }
          }
        }
      }
    }

    public object Unpack(Stream strm)
    {
      return this.Unpack(new MsgPackReader(strm));
    }

    public object Unpack(byte[] buf, int offset, int size)
    {
      using (MemoryStream memoryStream = new MemoryStream(buf, offset, size))
        return this.Unpack((Stream) memoryStream);
    }

    public object Unpack(byte[] buf)
    {
      return this.Unpack(buf, 0, buf.Length);
    }

    private object Unpack(MsgPackReader reader)
    {
      if (!reader.Read())
        throw new FormatException();
      TypePrefixes type = reader.Type;
      switch (type)
      {
        case TypePrefixes.Nil:
          return (object) null;
        case TypePrefixes.False:
          return (object) false;
        case TypePrefixes.True:
          return (object) true;
        case TypePrefixes.Float:
          return (object) reader.ValueFloat;
        case TypePrefixes.Double:
          return (object) reader.ValueDouble;
        case TypePrefixes.UInt8:
        case TypePrefixes.UInt16:
        case TypePrefixes.UInt32:
          return (object) reader.ValueUnsigned;
        case TypePrefixes.UInt64:
          return (object) reader.ValueUnsigned64;
        case TypePrefixes.Int8:
        case TypePrefixes.Int16:
        case TypePrefixes.Int32:
        case TypePrefixes.NegativeFixNum:
label_4:
          return (object) reader.ValueSigned;
        case TypePrefixes.Int64:
          return (object) reader.ValueSigned64;
        case TypePrefixes.Raw16:
        case TypePrefixes.Raw32:
label_13:
          byte[] buf = new byte[(IntPtr) reader.Length];
          reader.ReadValueRaw(buf, 0, buf.Length);
          return (object) buf;
        case TypePrefixes.Array16:
        case TypePrefixes.Array32:
label_14:
          object[] objArray = new object[(IntPtr) reader.Length];
          for (int index = 0; index < objArray.Length; ++index)
            objArray[index] = this.Unpack(reader);
          return (object) objArray;
        case TypePrefixes.Map16:
        case TypePrefixes.Map32:
label_18:
          IDictionary<object, object> dictionary = (IDictionary<object, object>) new Dictionary<object, object>((int) reader.Length);
          int length = (int) reader.Length;
          for (int index = 0; index < length; ++index)
          {
            object key = this.Unpack(reader);
            object obj = this.Unpack(reader);
            dictionary.Add(key, obj);
          }
          return (object) dictionary;
        default:
          switch (type)
          {
            case TypePrefixes.PositiveFixNum:
              goto label_4;
            case TypePrefixes.FixMap:
              goto label_18;
            case TypePrefixes.FixArray:
              goto label_14;
            case TypePrefixes.FixRaw:
              goto label_13;
            default:
              throw new FormatException();
          }
      }
    }
  }
}
