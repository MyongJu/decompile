﻿// Decompiled with JetBrains decompiler
// Type: MsgPack.ReflectionCache
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace MsgPack
{
  public static class ReflectionCache
  {
    private static Dictionary<Type, ReflectionCacheEntry> _cache = new Dictionary<Type, ReflectionCacheEntry>();

    public static ReflectionCacheEntry Lookup(Type type)
    {
      lock (ReflectionCache._cache)
      {
        ReflectionCacheEntry reflectionCacheEntry;
        if (ReflectionCache._cache.TryGetValue(type, out reflectionCacheEntry))
          return reflectionCacheEntry;
      }
      ReflectionCacheEntry reflectionCacheEntry1 = new ReflectionCacheEntry(type);
      lock (ReflectionCache._cache)
        ReflectionCache._cache[type] = reflectionCacheEntry1;
      return reflectionCacheEntry1;
    }

    public static void RemoveCache(Type type)
    {
      lock (ReflectionCache._cache)
        ReflectionCache._cache.Remove(type);
    }

    public static void Clear()
    {
      lock (ReflectionCache._cache)
        ReflectionCache._cache.Clear();
    }
  }
}
