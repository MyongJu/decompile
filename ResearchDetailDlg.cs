﻿// Decompiled with JetBrains decompiler
// Type: ResearchDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ResearchDetailDlg : UI.Dialog
{
  private Hashtable itemHT = new Hashtable();
  private Dictionary<string, int> m_lackitem = new Dictionary<string, int>(1);
  private Dictionary<string, long> m_lackRes = new Dictionary<string, long>(4);
  private const int ALLOWED_QUEUE = 1;
  public UILabel mCurrentGold;
  public GameObject mBuildingInfoDlg;
  public UITexture researchIcon;
  public GameObject mCancelBuildPopup;
  public UILabel concelPanelContent;
  public UILabel currentBonus;
  public UILabel nextRank;
  public GameObject nextRankGameObject;
  public GameObject currentRankGameObject;
  public TimerHUDUIItem timerHUDUIItem;
  public GameObject timerGameObject;
  public UILabel speedUpGoldLabel;
  public UIScrollView requireScrollView;
  public UIScrollView rewardScrollView;
  private string _refName;
  private string _buildingName;
  private string _buildingIcon;
  private int _level;
  private TechLevel techLevel;
  private int instBuildAmount;
  private JobHandle currentJobHandle;
  public Vector3 nextRankGameObjectNormalPosition;
  public UILabel researchTimeTitle;
  public UILabel mGoldValue;
  private int gold;
  private Popup resPopUp;
  private bool instant;

  public override void OnOpen(UIControler.UIParameter orgParam = null)
  {
    this.techLevel = ResearchManager.inst.GetTechLevel((orgParam as ResearchDetailDlg.Parameter).techLevelInternalID);
    this.UpdateUI();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (CityManager.inst.GetBuildingsByType("university").Count < 1)
    {
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      UIManager.inst.toast.Show(Utils.XLAT("vip_tip_build_university_description"), (System.Action) null, 4f, false);
    }
    else
    {
      this.UpdateUI();
      DBManager.inst.DB_Research.onDataChanged += new System.Action<long>(this.OnDataChange);
      CityManager.inst.onResourcesUpdated += new CityManager.OnResourcesUpdated(this.OnResourcesUpdated);
      DBManager.inst.DB_Local_Benefit.onDataChanged += new System.Action<string>(this.OnBenefitChange);
    }
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.timerHUDUIItem.StopAllCoroutines();
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
    DBManager.inst.DB_Research.onDataChanged -= new System.Action<long>(this.OnDataChange);
    CityManager.inst.onResourcesUpdated -= new CityManager.OnResourcesUpdated(this.OnResourcesUpdated);
    DBManager.inst.DB_Local_Benefit.onDataChanged -= new System.Action<string>(this.OnBenefitChange);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.timerHUDUIItem.StopAllCoroutines();
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
    DBManager.inst.DB_Research.onDataChanged -= new System.Action<long>(this.OnDataChange);
    CityManager.inst.onResourcesUpdated -= new CityManager.OnResourcesUpdated(this.OnResourcesUpdated);
    DBManager.inst.DB_Local_Benefit.onDataChanged -= new System.Action<string>(this.OnBenefitChange);
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnBackBtnClicked()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnBuyGoldBtnPressed()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  private void OnResourcesUpdated(long food, long wood, long silver, long ore, long gold)
  {
    this.UpdateResourceAndBtnStat(false);
  }

  private void OnBenefitChange(string key)
  {
    this.UpdateResourceAndBtnStat(false);
    if (!((UnityEngine.Object) this.resPopUp != (UnityEngine.Object) null))
      return;
    this.resPopUp.Close((UIControler.UIParameter) null);
    int num = this.instBuildAmount;
    if (!this.instant)
      num = this.gold;
    this.resPopUp = UIManager.inst.OpenPopup("BuildingResPopUp", (Popup.PopupParameter) new BuildingResPopUp.Parameter()
    {
      lackItem = this.m_lackitem,
      lackRes = this.m_lackRes,
      requireRes = this.itemHT,
      action = ScriptLocalization.Get("research_notenoughresource_tip", true),
      gold = num,
      buyResourCallBack = new System.Action<int>(this.SendRequest),
      title = ScriptLocalization.Get("research_build_notenoughresource", true),
      content = ScriptLocalization.Get("research_build_notenoughresource_description", true)
    });
  }

  private void OnDataChange(long itemID)
  {
    if (this.techLevel != null && this.techLevel.InternalID == (int) itemID)
    {
      if (this.techLevel.IsCompleted)
        this.techLevel = ResearchManager.inst.NextTechLevelOfSameTech(this.techLevel);
      if (this.techLevel != null)
      {
        this.timerHUDUIItem.StopAllCoroutines();
        if (Oscillator.IsAvailable)
          Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
        this.Invoke("UpdateUI", 0.5f);
      }
      else
        UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
    }
    else
      UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public string FormattedModifier(PropertyDefinition.FormatType format, float Modifier)
  {
    switch (format)
    {
      case PropertyDefinition.FormatType.Percent:
        return string.Format("{0:P1}", (object) Modifier);
      case PropertyDefinition.FormatType.Seconds:
        return string.Format("{0}s", (object) Modifier);
      case PropertyDefinition.FormatType.Boolean:
        return string.Format("{0}", (object) ((double) Modifier > 0.0));
      default:
        return string.Format("{0:0}", (object) Modifier);
    }
  }

  public void UpdateUI()
  {
    if (!this.gameObject.activeSelf)
      return;
    if (this.techLevel.IsCompleted)
      this.techLevel = ResearchManager.inst.NextTechLevelOfSameTech(this.techLevel);
    if (this.techLevel == null)
    {
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
    {
      this.mGoldValue.text = Utils.FormatThousands(GameEngine.Instance.PlayerData.hostPlayer.Currency.ToString());
      this.currentRankGameObject.SetActive(true);
      this.nextRankGameObject.SetActive(true);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.researchIcon, this.techLevel.Tech.IconPath, (System.Action<bool>) null, true, false, string.Empty);
      if (this.techLevel.Effects.Count > 0)
      {
        this.currentBonus.text = this.FormattedModifier(this.techLevel.Effects[0].Property.Format, ResearchManager.inst.CurrentBonus(this.techLevel));
        this.nextRank.text = this.FormattedModifier(this.techLevel.Effects[0].Property.Format, ResearchManager.inst.NextRank(this.techLevel));
      }
      this.researchTimeTitle.text = Utils.XLAT("prop_research_time_percent_name");
      if (this.techLevel.Effects.Count > 0)
        this.currentRankGameObject.SetActive(true);
      else
        this.currentRankGameObject.SetActive(false);
      this.m_lackRes.Clear();
      this.m_lackitem.Clear();
      this.itemHT.Clear();
      CityManager.inst.GetBuildingsInFlux();
      Hashtable hashtable = new Hashtable();
      CityData byUid = DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid);
      for (int index = 0; index < this.techLevel.Costs.Count; ++index)
      {
        switch (this.techLevel.Costs[index].Key)
        {
          case CityResourceInfo.ResourceType.FOOD:
            hashtable[(object) ItemBag.ItemType.food] = (object) this.techLevel.Costs[index].Value;
            string key1 = "Texture/BuildingConstruction/food_icon";
            long currentResource1 = (long) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD);
            int num1 = this.techLevel.BenefitCost(this.techLevel.Costs[index].Key, (double) this.techLevel.Costs[index].Value);
            if (currentResource1 < (long) num1)
            {
              this.m_lackRes.Add(key1, (long) Convert.ToInt32((long) num1 - currentResource1));
              break;
            }
            break;
          case CityResourceInfo.ResourceType.WOOD:
            hashtable[(object) ItemBag.ItemType.wood] = (object) this.techLevel.Costs[index].Value;
            string key2 = "Texture/BuildingConstruction/wood_icon";
            long currentResource2 = (long) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD);
            int num2 = this.techLevel.BenefitCost(this.techLevel.Costs[index].Key, (double) this.techLevel.Costs[index].Value);
            if (currentResource2 < (long) num2)
            {
              this.m_lackRes.Add(key2, (long) Convert.ToInt32((long) num2 - currentResource2));
              break;
            }
            break;
          case CityResourceInfo.ResourceType.ORE:
            hashtable[(object) ItemBag.ItemType.ore] = (object) this.techLevel.Costs[index].Value;
            string key3 = "Texture/BuildingConstruction/ore_icon";
            long currentResource3 = (long) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE);
            int num3 = this.techLevel.BenefitCost(this.techLevel.Costs[index].Key, (double) this.techLevel.Costs[index].Value);
            if (currentResource3 < (long) num3)
            {
              this.m_lackRes.Add(key3, (long) Convert.ToInt32((long) num3 - currentResource3));
              break;
            }
            break;
          case CityResourceInfo.ResourceType.SILVER:
            hashtable[(object) ItemBag.ItemType.silver] = (object) this.techLevel.Costs[index].Value;
            string key4 = "Texture/BuildingConstruction/silver_icon";
            long currentResource4 = (long) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
            int num4 = this.techLevel.BenefitCost(this.techLevel.Costs[index].Key, (double) this.techLevel.Costs[index].Value);
            if (currentResource4 < (long) num4)
            {
              this.m_lackRes.Add(key4, (long) Convert.ToInt32((long) num4 - currentResource4));
              break;
            }
            break;
        }
      }
      new Hashtable()[(object) "resources"] = (object) hashtable;
      this.mCurrentGold.text = Utils.FormatThousands(PlayerData.inst.userData.currency.gold.ToString());
      this.UpdateResourceAndBtnStat(true);
      if (this.techLevel.State == TechLevel.ResearchState.InProgress)
      {
        long jobId = ResearchManager.inst.GetJobId(this.techLevel);
        this.currentJobHandle = JobManager.Instance.GetJob(jobId);
        if (this.currentJobHandle != null)
        {
          double secsRemaining = (double) this.currentJobHandle.LeftTime();
          double totalSeconds = (double) this.currentJobHandle.Duration();
          double num = 0.0;
          this.timerGameObject.SetActive(true);
          this.timerHUDUIItem.mJobID = jobId;
          this.timerHUDUIItem.mFreeTimeSecs = num;
          this.timerHUDUIItem.SetDetails("time_bar_researching", string.Empty, TimerType.TIMER_RESEARCH, secsRemaining, totalSeconds, 1, (TimerHUDUIItem.UpdateTimerHUDUIItem) null, (TimerHUDUIItem.UpdateTimerHUDUIItem) null, false);
          this.timerHUDUIItem.OnUpdate(0);
          Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
        }
      }
      else
        this.timerGameObject.SetActive(false);
      this.mCancelBuildPopup.SetActive(false);
    }
  }

  private void UpdateResourceAndBtnStat(bool resetPosition = true)
  {
    Hashtable hashtable = new Hashtable();
    if (this.techLevel == null)
      return;
    for (int index = 0; index < this.techLevel.Costs.Count; ++index)
    {
      switch (this.techLevel.Costs[index].Key)
      {
        case CityResourceInfo.ResourceType.FOOD:
          if (this.techLevel.Costs[index].Value > 0)
          {
            hashtable.Add((object) ItemBag.ItemType.food, (object) this.techLevel.BenefitCost(this.techLevel.Costs[index].Key, (double) this.techLevel.Costs[index].Value));
            break;
          }
          break;
        case CityResourceInfo.ResourceType.WOOD:
          if (this.techLevel.Costs[index].Value > 0)
          {
            hashtable.Add((object) ItemBag.ItemType.wood, (object) this.techLevel.BenefitCost(this.techLevel.Costs[index].Key, (double) this.techLevel.Costs[index].Value));
            break;
          }
          break;
        case CityResourceInfo.ResourceType.ORE:
          if (this.techLevel.Costs[index].Value > 0)
          {
            hashtable.Add((object) ItemBag.ItemType.ore, (object) this.techLevel.BenefitCost(this.techLevel.Costs[index].Key, (double) this.techLevel.Costs[index].Value));
            break;
          }
          break;
        case CityResourceInfo.ResourceType.SILVER:
          if (this.techLevel.Costs[index].Value > 0)
          {
            hashtable.Add((object) ItemBag.ItemType.silver, (object) this.techLevel.BenefitCost(this.techLevel.Costs[index].Key, (double) this.techLevel.Costs[index].Value));
            break;
          }
          break;
      }
    }
    int time = this.techLevel.Time;
    int finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(time, "calc_research_time");
    this.itemHT.Clear();
    this.itemHT.Add((object) "time", (object) finalData);
    this.itemHT.Add((object) "resources", (object) hashtable);
    this.instBuildAmount = ItemBag.CalculateCost(this.itemHT);
    this.itemHT.Remove((object) "time");
    this.gold = ItemBag.CalculateCost(this.itemHT);
    this.mBuildingInfoDlg.SetActive(true);
    ConstructionResearchDetailStats component = this.mBuildingInfoDlg.GetComponent<ConstructionResearchDetailStats>();
    component.SetDetails(this.techLevel.Tech.Image, (this.techLevel.Level - 1).ToString() + "/" + (object) ResearchManager.inst.GetTechSteps(this.techLevel.Tech), (double) time, (double) finalData, this.instBuildAmount, this.techLevel.Name, this.techLevel.Level, this.techLevel.ID, (System.Action) (() =>
    {
      this.mBuildingInfoDlg.SetActive(false);
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }));
    ArrayList buttons = new ArrayList();
    ArrayList arrayList = new ArrayList();
    bool flag1 = false;
    bool flag2 = true;
    int num = ResearchManager.inst.CurrentResearch != null ? 1 : 0;
    if (num >= 1)
    {
      GameObject gameObject = component.AddRequirement(ConstructionResearchDetailStats.Requirement.QUEUE, (string) null, (string) null, (double) num, 1.0, 0.0);
      buttons.Add((object) gameObject);
      flag1 = true;
      component.SetQueueFull();
    }
    for (int index = 0; index < this.techLevel.RequiredBuildings.Count; ++index)
    {
      if (!this.BuildItemRequirement(this.techLevel.RequiredBuildings[index], buttons, component))
        flag2 = false;
    }
    for (int index = 0; index < this.techLevel.RequiredTechs.Count; ++index)
    {
      if (!ResearchManager.inst.IsTheSameTech(ResearchManager.inst.GetTechLevel(this.techLevel.RequiredTechs[index]), this.techLevel) && !this.BuildResearchrequirement(this.techLevel.RequiredTechs[index], buttons, component))
        flag2 = false;
    }
    CityData byUid = DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid);
    for (int index = 0; index < this.techLevel.Costs.Count; ++index)
    {
      switch (this.techLevel.Costs[index].Key)
      {
        case CityResourceInfo.ResourceType.FOOD:
          if (this.techLevel.Costs[index].Value > 0)
          {
            long currentResource = (long) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD);
            GameObject gameObject = component.AddRequirement(ConstructionResearchDetailStats.Requirement.FOOD, (string) null, (string) null, (double) currentResource, (double) this.techLevel.BenefitCost(this.techLevel.Costs[index].Key, (double) this.techLevel.Costs[index].Value), (double) this.techLevel.Costs[index].Value);
            buttons.Add((object) gameObject);
            break;
          }
          break;
        case CityResourceInfo.ResourceType.WOOD:
          if (this.techLevel.Costs[index].Value > 0)
          {
            long currentResource = (long) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD);
            GameObject gameObject = component.AddRequirement(ConstructionResearchDetailStats.Requirement.WOOD, (string) null, (string) null, (double) currentResource, (double) this.techLevel.BenefitCost(this.techLevel.Costs[index].Key, (double) this.techLevel.Costs[index].Value), (double) this.techLevel.Costs[index].Value);
            buttons.Add((object) gameObject);
            break;
          }
          break;
        case CityResourceInfo.ResourceType.ORE:
          if (this.techLevel.Costs[index].Value > 0)
          {
            long currentResource = (long) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE);
            GameObject gameObject = component.AddRequirement(ConstructionResearchDetailStats.Requirement.ORE, (string) null, (string) null, (double) currentResource, (double) this.techLevel.BenefitCost(this.techLevel.Costs[index].Key, (double) this.techLevel.Costs[index].Value), (double) this.techLevel.Costs[index].Value);
            buttons.Add((object) gameObject);
            break;
          }
          break;
        case CityResourceInfo.ResourceType.SILVER:
          if (this.techLevel.Costs[index].Value > 0)
          {
            long currentResource = (long) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
            GameObject gameObject = component.AddRequirement(ConstructionResearchDetailStats.Requirement.SILVER, (string) null, (string) null, (double) currentResource, (double) this.techLevel.BenefitCost(this.techLevel.Costs[index].Key, (double) this.techLevel.Costs[index].Value), (double) this.techLevel.Costs[index].Value);
            buttons.Add((object) gameObject);
            break;
          }
          break;
      }
    }
    ResearchManager.inst.GetPowerAdded(this.techLevel);
    GameObject gameObject1 = component.AddRewards(ConstructionResearchDetailStats.Rewards.POWER, (string) null, (string) null, this.techLevel.Power.ToString());
    arrayList.Add((object) gameObject1);
    for (int index = 0; index < 1; ++index)
    {
      Effect effect = this.techLevel.Effects[index];
      string amount = !this.techLevel.Tech.ID.Equals("research_troop_scout") ? effect.FormattedModifier : Utils.XLAT(string.Format("research_troop_scout_{0}_description", (object) this.techLevel.Level.ToString()));
      GameObject gameObject2 = component.AddRewards(ConstructionResearchDetailStats.Rewards.DEFAULT, (string) null, effect.Property.InternalID.ToString(), amount);
      arrayList.Add((object) gameObject2);
    }
    component.mBuildBtn.isEnabled = this.techLevel.PlayerHasReqBuildingLevel && this.techLevel.PlayerHasReqTechLevels && !flag1;
    component.mInstBuidBtn.isEnabled = this.techLevel.State != TechLevel.ResearchState.InProgress && this.techLevel.PlayerHasReqBuildingLevel && this.techLevel.PlayerHasReqTechLevels;
    if (this.techLevel.State == TechLevel.ResearchState.InProgress)
      component.mBuildBtn.isEnabled = false;
    if (!resetPosition)
      return;
    this.requireScrollView.ResetPosition();
    this.rewardScrollView.ResetPosition();
  }

  private bool BuildItemRequirement(int internaoId, ArrayList buttons, ConstructionResearchDetailStats CBS)
  {
    bool flag = true;
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData(internaoId);
    int buildingLevelFor = CityManager.inst.GetHighestBuildingLevelFor(data.Type);
    if (buildingLevelFor < data.Building_Lvl)
      flag = false;
    string icon = "Prefab/UI/BuildingPrefab/ui_" + data.Building_ImagePath + "_l1";
    GameObject gameObject = CBS.AddRequirement(ConstructionResearchDetailStats.Requirement.BUILDING, icon, Utils.XLAT(data.Building_LOC_ID), (double) buildingLevelFor, (double) data.Building_Lvl, 0.0);
    buttons.Add((object) gameObject);
    return flag;
  }

  private bool BuildResearchrequirement(int internalID, ArrayList buttons, ConstructionResearchDetailStats CBS)
  {
    TechLevel techLevel = ResearchManager.inst.GetTechLevel(internalID);
    string iconPath = techLevel.Tech.IconPath;
    int num = ResearchManager.inst.Rank(techLevel.Tech);
    GameObject gameObject = CBS.AddRequirement(ConstructionResearchDetailStats.Requirement.TECH, iconPath, techLevel.Name, (double) num, (double) techLevel.Level, 0.0);
    buttons.Add((object) gameObject);
    return num >= techLevel.Level;
  }

  private void OnSecondHandler(int timeStamp)
  {
    this.timerHUDUIItem.OnUpdate(timeStamp);
    if (this.techLevel == null)
      return;
    long jobId = ResearchManager.inst.GetJobId(this.techLevel);
    Hashtable ht = new Hashtable();
    int num = JobManager.Instance.GetJob(jobId).LeftTime();
    ht.Add((object) "time", (object) num);
    this.speedUpGoldLabel.text = ItemBag.CalculateCost(ht).ToString();
  }

  public void OnTimerSpeedUpBtnPressed()
  {
    long jobId = ResearchManager.inst.GetJobId(this.techLevel);
    GoldConsumePopup.Parameter parameter = new GoldConsumePopup.Parameter();
    Hashtable ht = new Hashtable();
    int num1 = JobManager.Instance.GetJob(jobId).LeftTime();
    int num2 = num1;
    ht.Add((object) "time", (object) num2);
    int cost = ItemBag.CalculateCost(ht);
    parameter.confirmButtonClickEvent = new System.Action(this.ConFirmCallBack);
    parameter.lefttime = num1;
    parameter.goldNum = cost;
    parameter.freetime = 0;
    parameter.description = ScriptLocalization.GetWithPara("confirm_gold_spend_cooldown_instant_desc", new Dictionary<string, string>()
    {
      {
        "num",
        cost.ToString()
      }
    }, true);
    UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) parameter);
  }

  private void ConFirmCallBack()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
    long jobId = ResearchManager.inst.GetJobId(this.techLevel);
    JobHandle job = JobManager.Instance.GetJob(jobId);
    Hashtable ht = new Hashtable();
    int num = job.LeftTime() - 0;
    if (num <= 0)
      return;
    ht.Add((object) "time", (object) num);
    int cost = ItemBag.CalculateCost(ht);
    RequestManager.inst.SendRequest("City:speedUpByGold", new Hashtable()
    {
      {
        (object) "job_id",
        (object) jobId
      },
      {
        (object) "gold",
        (object) cost
      }
    }, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      this.UpdateUI();
    }), true);
  }

  public void OnCancelTimerBtnPressed()
  {
    this.mCancelBuildPopup.SetActive(true);
    this.concelPanelContent.text = string.Format(Utils.XLAT("research_cancel_research_description") + "\n [ff0000]" + Utils.XLAT("half_resource_return"), (object) this.techLevel.Name, (object) this.techLevel.Level);
  }

  public void OnCancelYesPressed()
  {
    this.mCancelBuildPopup.SetActive(false);
    this.CancelResearch();
  }

  public void OnCancelNoPressed()
  {
    this.mCancelBuildPopup.SetActive(false);
  }

  public void OnCancelClosePressed()
  {
    this.mCancelBuildPopup.SetActive(false);
  }

  public void OnBuildPressed()
  {
    this.StartResearch(false);
  }

  public void OnInstBuildPressed()
  {
    string withPara = ScriptLocalization.GetWithPara("confirm_gold_spend_research_instant_desc", new Dictionary<string, string>()
    {
      {
        "num",
        this.instBuildAmount.ToString()
      }
    }, true);
    UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
    {
      confirmButtonClickEvent = (System.Action) (() => this.StartResearch(true)),
      description = withPara,
      goldNum = this.instBuildAmount
    });
  }

  public void OnBackBtnPressed()
  {
    this.gameObject.SetActive(false);
    UIManager.inst.OpenDlg("Research/ResearchTreeDlg", (UI.Dialog.DialogParameter) new ResearchTreeDlg.Parameter()
    {
      tree = this.techLevel.Tech.tree,
      keepLastPositon = true
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnInfoBtnPressed()
  {
    UIManager.inst.OpenPopup("ResearchDetailInfoPopup", (Popup.PopupParameter) new ResearchDetailInfoPopup.UIParam()
    {
      techLevel = this.techLevel
    });
  }

  public void StartResearch(bool instant)
  {
    this.instant = instant;
    if (!this.techLevel.PlayerHasReqBuildingLevel)
      UIManager.inst.toast.Show(ScriptLocalization.Get("research_require_level_tip", true), (System.Action) null, 4f, false);
    else if (!this.techLevel.PlayerHasReqCosts)
    {
      if (instant)
      {
        this.SendRequest(this.instBuildAmount);
      }
      else
      {
        int num = this.instBuildAmount;
        if (!instant)
          num = this.gold;
        this.resPopUp = UIManager.inst.OpenPopup("BuildingResPopUp", (Popup.PopupParameter) new BuildingResPopUp.Parameter()
        {
          lackItem = this.m_lackitem,
          lackRes = this.m_lackRes,
          requireRes = this.itemHT,
          action = ScriptLocalization.Get("research_notenoughresource_tip", true),
          gold = num,
          buyResourCallBack = new System.Action<int>(this.SendRequest),
          title = ScriptLocalization.Get("research_build_notenoughresource", true),
          content = ScriptLocalization.Get("research_build_notenoughresource_description", true)
        });
      }
    }
    else
      this.SendRequest(0);
  }

  private void SendRequest(int gold = 0)
  {
    if (this.techLevel.Tech.tree == 5 && PlayerData.inst.dragonKnightData == null)
      Utils.ShowException(2041010);
    else if (PlayerData.inst.hostPlayer.Currency < gold)
      Utils.ShowNotEnoughGoldTip();
    else
      MessageHub.inst.GetPortByAction("Research:startResearch").SendRequest(this.GetResearchParam(gold), new System.Action<bool, object>(this.StartResearchCallback), true);
  }

  private Hashtable GetResearchParam(int gold = 0)
  {
    return new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "city_id",
        (object) DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid).cityId
      },
      {
        (object) "research_id",
        (object) this.techLevel.InternalID
      },
      {
        (object) "instant",
        (object) this.instant
      },
      {
        (object) nameof (gold),
        (object) gold
      }
    };
  }

  private void CancelResearch()
  {
    MessageHub.inst.GetPortByAction("Research:cancelResearch").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "city_id",
        (object) DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid).cityId
      },
      {
        (object) "research_id",
        (object) this.techLevel.InternalID
      }
    }, new System.Action<bool, object>(this.CancelResearchCallback), true);
  }

  private void StartResearchCallback(bool ret, object data)
  {
    if (!ret)
      return;
    if (this.instant)
      this.UpdateUI();
    else
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void CancelResearchCallback(bool ret, object data)
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int techLevelInternalID;
  }
}
