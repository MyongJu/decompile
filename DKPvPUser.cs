﻿// Decompiled with JetBrains decompiler
// Type: DKPvPUser
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class DKPvPUser
{
  public List<int> skills = new List<int>();
  public Dictionary<int, int> talents = new Dictionary<int, int>();
  public long uid;
  public int dungeonLevel;
  public DragonKnightPVPResultPopup.Parameter param;

  public void Decode(object data)
  {
    this.skills.Clear();
    Hashtable inData = data as Hashtable;
    if (inData == null)
      return;
    DatabaseTools.UpdateData(inData, "uid", ref this.uid);
    DatabaseTools.UpdateData(inData, "dungeon_level", ref this.dungeonLevel);
    ArrayList arrayList = inData[(object) "skill"] as ArrayList;
    if (arrayList != null)
    {
      for (int index = 0; index < arrayList.Count; ++index)
      {
        int result;
        if (int.TryParse(arrayList[index].ToString(), out result))
          this.skills.Add(result);
      }
    }
    Hashtable hashtable = inData[(object) "talent"] as Hashtable;
    if (hashtable == null)
      return;
    IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
    while (enumerator.MoveNext())
    {
      int result1;
      int result2;
      if (int.TryParse(enumerator.Key.ToString(), out result1) && int.TryParse(enumerator.Value.ToString(), out result2))
        this.talents.Add(result1, result2);
    }
  }

  public int GetTalent(int slot)
  {
    int num;
    this.talents.TryGetValue(slot, out num);
    return num;
  }

  public void MakeLog(Hashtable data)
  {
    this.param = new DragonKnightPVPResultPopup.Parameter();
    this.param.source = PlayerData.inst.userData;
    this.param.sourceAlliance = PlayerData.inst.allianceData;
    this.param.sourceDK = PlayerData.inst.dragonKnightData;
    this.param.sourceEquipments = DBManager.inst.GetInventory(BagType.DragonKnight).GetEquippedItemsByID(this.param.source.uid);
    this.param.sourceHeal = (int) data[(object) "sourceHeal"];
    this.param.sourceDamage = (int) data[(object) "sourceDamage"];
    this.param.sourceTalent1 = DBManager.inst.DB_DragonKnightTalent.GetTalentTreeTalentCount(1);
    this.param.sourceTalent2 = DBManager.inst.DB_DragonKnightTalent.GetTalentTreeTalentCount(2);
    this.param.sourceTalent3 = DBManager.inst.DB_DragonKnightTalent.GetTalentTreeTalentCount(3);
    this.param.target = DBManager.inst.DB_User.Get(this.uid);
    this.param.targetAlliance = DBManager.inst.DB_Alliance.Get(this.param.target.allianceId);
    this.param.targetDK = DBManager.inst.DB_DragonKnight.Get(this.uid);
    this.param.targetEquipments = DBManager.inst.GetInventory(BagType.DragonKnight).GetEquippedItemsByID(this.param.target.uid);
    this.param.targetHeal = (int) data[(object) "targetHeal"];
    this.param.targetDamage = (int) data[(object) "targetDamage"];
    this.param.targetTalent1 = this.GetTalent(1);
    this.param.targetTalent2 = this.GetTalent(2);
    this.param.targetTalent3 = this.GetTalent(3);
    this.param.win = (int) data[(object) "win"];
    this.param.totalRounds = (int) data[(object) "totalRound"];
    UserDKArenaData currentData = DBManager.inst.DB_DKArenaDB.GetCurrentData();
    UserDKArenaData dataById = DBManager.inst.DB_DKArenaDB.GetDataById(this.uid);
    this.param.oldSocre = currentData == null ? 0L : currentData.oldSocre;
    this.param.score = currentData == null ? 0L : currentData.score;
    this.param.oppScore = dataById == null ? 0L : dataById.score;
  }
}
