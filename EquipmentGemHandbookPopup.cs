﻿// Decompiled with JetBrains decompiler
// Type: EquipmentGemHandbookPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using GemConstant;
using UI;
using UnityEngine;

public class EquipmentGemHandbookPopup : Popup
{
  public UILabel title;
  public GemHandbookContent gemhandbookContent;
  public RuneHandbookContent runeHandbookContent;
  private SlotType slotType;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    EquipmentGemHandbookPopup.Parameter parameter = orgParam as EquipmentGemHandbookPopup.Parameter;
    if (parameter == null)
      D.error((object) "Parameter of EquipmentGemHandbookPopup can not be null !");
    else
      this.slotType = parameter.slotType;
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.HideAllContent();
  }

  private void UpdateUI()
  {
    this.HideAllContent();
    switch (this.slotType)
    {
      case SlotType.lord:
        this.title.text = Utils.XLAT("forge_armory_gemstone_display_title");
        this.gemhandbookContent.Show(this.slotType);
        break;
      case SlotType.dragon_knight:
        this.title.text = Utils.XLAT("forge_armory_dk_runestone_display_title");
        this.runeHandbookContent.Show(this.slotType);
        break;
    }
  }

  private void HideAllContent()
  {
    if ((Object) null != (Object) this.gemhandbookContent)
      this.gemhandbookContent.Hide();
    if (!((Object) null != (Object) this.runeHandbookContent))
      return;
    this.runeHandbookContent.Hide();
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public SlotType slotType;
  }
}
