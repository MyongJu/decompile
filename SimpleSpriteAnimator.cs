﻿// Decompiled with JetBrains decompiler
// Type: SimpleSpriteAnimator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class SimpleSpriteAnimator : MonoBehaviour
{
  public List<SimpleSpriteAnimator.Animation> animations = new List<SimpleSpriteAnimator.Animation>();
  public SpriteRenderer spriteRenderer;
  private float timer;
  private bool needUpdateDelay;
  private SimpleSpriteAnimator.Animation currentAnimation;
  private float delay;

  public int currentFrame { get; private set; }

  private void Start()
  {
    this.currentAnimation = this.animations[0];
    this.UpdateDelay(this.currentAnimation);
    if ((bool) ((UnityEngine.Object) this.spriteRenderer))
      return;
    this.spriteRenderer = this.GetComponent<SpriteRenderer>();
    this.spriteRenderer.sprite = this.currentAnimation.frames[0];
  }

  private void Update()
  {
    if (this.needUpdateDelay)
      this.UpdateDelay(this.currentAnimation);
    if ((double) this.timer < (double) this.delay)
    {
      this.timer += Time.deltaTime;
    }
    else
    {
      this.timer -= this.delay;
      this.NextFrame(this.currentAnimation);
      this.spriteRenderer.sprite = this.currentAnimation.frames[this.currentFrame];
    }
  }

  private void UpdateDelay(SimpleSpriteAnimator.Animation animation)
  {
    this.delay = 1f / (float) animation.fps;
    this.needUpdateDelay = false;
  }

  private void NextFrame(SimpleSpriteAnimator.Animation animation)
  {
    ++this.currentFrame;
    if (this.currentFrame < animation.frames.Count)
      return;
    if (animation.loop)
      this.currentFrame = 0;
    else
      this.currentFrame = animation.frames.Count - 1;
  }

  [Serializable]
  public class Animation
  {
    public List<UnityEngine.Sprite> frames = new List<UnityEngine.Sprite>();
    public string name;
    public int fps;
    public bool loop;
  }
}
