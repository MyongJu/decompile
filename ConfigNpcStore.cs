﻿// Decompiled with JetBrains decompiler
// Type: ConfigNpcStore
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigNpcStore
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, NpcStoreInfo> datas;
  private Dictionary<int, NpcStoreInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<NpcStoreInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public NpcStoreInfo GetData(int internalId)
  {
    if (this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (NpcStoreInfo) null;
  }

  public NpcStoreInfo GetData(string ID)
  {
    if (this.datas.ContainsKey(ID))
      return this.datas[ID];
    return (NpcStoreInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, NpcStoreInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, NpcStoreInfo>) null;
  }
}
