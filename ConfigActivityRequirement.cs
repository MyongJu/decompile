﻿// Decompiled with JetBrains decompiler
// Type: ConfigActivityRequirement
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigActivityRequirement
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, ActivityRequirementInfo> datas;
  private Dictionary<int, ActivityRequirementInfo> dicByUniqueId;
  private Dictionary<int, List<ActivityRequirementInfo>> dicByActivityMainID;

  public void BuildDB(object res)
  {
    this.parse.Parse<ActivityRequirementInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public ActivityRequirementInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (ActivityRequirementInfo) null;
  }

  public List<ActivityRequirementInfo> GetCurrentRequirementInfo(int id)
  {
    List<ActivityRequirementInfo> activityRequirementInfoList = new List<ActivityRequirementInfo>();
    ActivityMainSubInfo data = ConfigManager.inst.DB_ActivityMainSub.GetData(id);
    Dictionary<int, ActivityRequirementInfo>.ValueCollection.Enumerator enumerator = this.dicByUniqueId.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.ActivityMainSubInfo_ID == data.internalId)
        activityRequirementInfoList.Add(enumerator.Current);
    }
    activityRequirementInfoList.Sort(new Comparison<ActivityRequirementInfo>(this.CompareItem));
    return activityRequirementInfoList;
  }

  private int CompareItem(ActivityRequirementInfo a, ActivityRequirementInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }

  public ActivityRequirementInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (ActivityRequirementInfo) null;
  }

  public List<ActivityRequirementInfo> GetRequireByActivityMainID(int internalID)
  {
    if (this.dicByActivityMainID == null)
    {
      this.dicByActivityMainID = new Dictionary<int, List<ActivityRequirementInfo>>();
      using (Dictionary<int, ActivityRequirementInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, ActivityRequirementInfo> current = enumerator.Current;
          if (!this.dicByActivityMainID.ContainsKey(current.Key))
            this.dicByActivityMainID.Add(current.Key, new List<ActivityRequirementInfo>());
          this.dicByActivityMainID[current.Key].Add(current.Value);
          this.dicByActivityMainID[current.Key].Sort((Comparison<ActivityRequirementInfo>) ((x, y) => x.Priority.CompareTo(y.Priority)));
        }
      }
    }
    return this.dicByActivityMainID[internalID];
  }
}
