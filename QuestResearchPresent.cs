﻿// Decompiled with JetBrains decompiler
// Type: QuestResearchPresent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class QuestResearchPresent : BasePresent
{
  public override void Refresh()
  {
    if (this.Data.Type == PresentData.DataType.Quest)
    {
      base.Refresh();
    }
    else
    {
      int count = this.Count;
      int num = ResearchManager.inst.CompleteCount;
      if (num > count)
        num = count;
      this.ProgressValue = (float) num / (float) count;
      this.ProgressContent = num.ToString() + "/" + (object) count;
    }
  }

  public override int IconType
  {
    get
    {
      return 1;
    }
  }
}
