﻿// Decompiled with JetBrains decompiler
// Type: WorldCoordinate
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public struct WorldCoordinate
{
  public int X;
  public int Y;

  public WorldCoordinate(int x, int y)
  {
    this.X = x;
    this.Y = y;
  }

  public void Set(int x, int y)
  {
    this.X = x;
    this.Y = y;
  }

  public static int DistanceSquared(WorldCoordinate a, WorldCoordinate b)
  {
    int num1 = a.X - b.X;
    int num2 = a.Y - b.Y;
    return num1 * num1 + num2 * num2;
  }

  public override string ToString()
  {
    return string.Format("X:{0} Y:{1}", (object) this.X, (object) this.Y);
  }

  public override int GetHashCode()
  {
    return this.ToString().GetHashCode();
  }

  public bool Equals(WorldCoordinate other)
  {
    if (this.X == other.X)
      return this.Y == other.Y;
    return false;
  }
}
