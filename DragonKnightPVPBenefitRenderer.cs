﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightPVPBenefitRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DragonKnightPVPBenefitRenderer : MonoBehaviour
{
  public UILabel m_BenefitName;
  public UILabel m_BenefitValue;
  public GameObject Up;
  public GameObject Down;

  public void SetData(PropertyDefinition property, float source, float target)
  {
    bool flag1 = (double) source >= 0.0 ? (double) source > (double) target : (double) source < (double) target;
    bool flag2 = (double) source >= 0.0 ? (double) source < (double) target : (double) source > (double) target;
    this.Up.SetActive(flag1);
    this.Down.SetActive(flag2);
    this.m_BenefitName.text = property.Name;
    this.m_BenefitValue.text = Utils.GetBenefitValueString(property.Format, (double) source);
    if (flag1)
      this.m_BenefitValue.color = (Color) new Color32((byte) 67, (byte) 224, (byte) 36, byte.MaxValue);
    else if (flag2)
      this.m_BenefitValue.color = (Color) new Color32(byte.MaxValue, (byte) 38, (byte) 16, byte.MaxValue);
    else
      this.m_BenefitValue.color = (Color) new Color32((byte) 207, (byte) 207, (byte) 207, byte.MaxValue);
  }
}
