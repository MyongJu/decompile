﻿// Decompiled with JetBrains decompiler
// Type: LoopAD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopAD : MonoBehaviour
{
  public float inTime = 1f;
  public float stayTime = 2f;
  [NonSerialized]
  public int maxIndex = 2;
  public GameObject[] Children;
  [NonSerialized]
  public int minIndex;
  private int currentIndex;
  private int realIndex;
  public int itemSize;
  private bool isPlaying;
  public System.Action<GameObject, int> OnItemInHandler;
  public UIButtonBar buttonBar;
  public GameObject itemNormalPrefab;
  public GameObject itemSelectedPrefab;
  public UITable itemContainer;
  private Coroutine coroutine;
  private bool needStop;

  private void CreateItems()
  {
    int num = this.maxIndex - this.minIndex + 1;
    if (this.buttonBar.buttons != null && this.buttonBar.buttons.Length > 0)
    {
      for (int index = 0; index < this.buttonBar.buttons.Length; ++index)
      {
        this.buttonBar.buttons[index].transform.parent = this.transform;
        NGUITools.SetActive(this.buttonBar.buttons[index], false);
      }
    }
    this.buttonBar.buttons = new GameObject[num * 2];
    int index1 = 0;
    while (index1 < this.buttonBar.buttons.Length)
    {
      GameObject gameObject1 = NGUITools.AddChild(this.itemContainer.gameObject, this.itemNormalPrefab.gameObject);
      NGUITools.SetActive(gameObject1.gameObject, true);
      GameObject gameObject2 = NGUITools.AddChild(this.itemContainer.gameObject, this.itemSelectedPrefab.gameObject);
      this.buttonBar.buttons[index1] = gameObject1;
      this.buttonBar.buttons[index1 + 1] = gameObject2;
      index1 += 2;
    }
    this.itemContainer.Reposition();
  }

  private void ClearItween()
  {
    List<GameObject> data = this.GetData();
    for (int index = 0; index < data.Count; ++index)
      iTween.Stop(data[index]);
  }

  public void Resume()
  {
    this.needStop = false;
    this.Stay();
  }

  public void SetSelected(int realIndex)
  {
    int num = this.realIndex - realIndex;
    this.realIndex = realIndex;
    this.currentIndex -= num;
    this.currentIndex = this.currentIndex >= 0 ? this.currentIndex : 0;
    if (this.currentIndex > this.Children.Length - 1)
      this.currentIndex = 0;
    if (this.realIndex > this.maxIndex)
      this.realIndex = this.minIndex;
    this.buttonBar.SelectedIndex = this.realIndex;
    this.itemContainer.Reposition();
    this.ClearItween();
    this.InItem(false);
    this.needStop = true;
  }

  public void StartPlay()
  {
    this.isPlaying = true;
    this.CreateItems();
    this.realIndex = this.minIndex;
    this.currentIndex = 0;
    this.buttonBar.SelectedIndex = 0;
    this.itemContainer.Reposition();
    this.ClearItween();
    this.InItem(false);
    this.Stay();
  }

  private List<GameObject> GetData()
  {
    List<GameObject> gameObjectList = new List<GameObject>();
    for (int currentIndex = this.currentIndex; currentIndex < this.Children.Length; ++currentIndex)
      gameObjectList.Add(this.Children[currentIndex]);
    for (int index = 0; index < this.currentIndex; ++index)
      gameObjectList.Add(this.Children[index]);
    return gameObjectList;
  }

  private void InItem(bool needMove = true)
  {
    List<GameObject> data = this.GetData();
    for (int index = 0; index < data.Count; ++index)
    {
      GameObject target = data[index];
      if (this.OnItemInHandler != null)
      {
        int num = this.realIndex + index;
        if (num > this.maxIndex)
          num = this.minIndex;
        this.OnItemInHandler(target, num);
      }
      Vector3 zero = Vector3.zero;
      zero.x = (float) (index * this.itemSize);
      target.transform.localPosition = zero;
      if (needMove)
      {
        Hashtable args = new Hashtable();
        args.Add((object) "x", (object) (float) ((double) target.transform.localPosition.x - (double) this.itemSize));
        args.Add((object) "time", (object) this.inTime);
        args.Add((object) "easetype", (object) "Linear");
        args.Add((object) "isLocal", (object) true);
        args.Add((object) "delay", (object) this.stayTime);
        if (index == 0)
        {
          args.Add((object) "oncompleteparams", (object) target);
          args.Add((object) "oncompletetarget", (object) this.gameObject);
          args.Add((object) "oncomplete", (object) "ResetPosition");
        }
        if (index == data.Count - 1)
        {
          args.Add((object) "oncompletetarget", (object) this.gameObject);
          args.Add((object) "oncomplete", (object) "OnCompleteHandler");
        }
        iTween.MoveTo(target, args);
      }
    }
  }

  public void ResetPosition(GameObject go)
  {
    go.transform.localPosition = new Vector3((float) ((this.Children.Length - 1) * this.itemSize), 0.0f, 0.0f);
  }

  public void Stop()
  {
    this.isPlaying = false;
  }

  public void OnCompleteHandler()
  {
    this.Stay();
  }

  private void RestIndex()
  {
    ++this.realIndex;
    ++this.currentIndex;
    if (this.currentIndex > this.Children.Length - 1)
      this.currentIndex = 0;
    this.buttonBar.SelectedIndex = this.realIndex - 1;
    if (this.realIndex > this.maxIndex)
      this.realIndex = this.minIndex;
    this.itemContainer.Reposition();
  }

  private void Stay()
  {
    if (this.maxIndex - this.minIndex + 1 == 1 || !this.isPlaying)
      return;
    this.InItem(true);
    this.RestIndex();
  }

  private enum StatusEnum
  {
    Stop,
    Playing,
    Staying,
    Start,
    End,
  }
}
