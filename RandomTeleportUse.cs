﻿// Decompiled with JetBrains decompiler
// Type: RandomTeleportUse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;

public class RandomTeleportUse : ItemBaseUse
{
  public RandomTeleportUse(int id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  public RandomTeleportUse(string id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  protected override void CustomUse(System.Action<bool, object> resultHandler)
  {
    if (MapUtils.IsPitWorld(PlayerData.inst.CityData.Location.K))
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("exception_2300150", true), (System.Action) null, 4f, false);
    }
    else
    {
      string str1 = ScriptLocalization.Get("item_use_uppercase_yes", true);
      string str2 = ScriptLocalization.Get("item_use_uppercase_no", true);
      string str3 = ScriptLocalization.Get("item_uppercase_use_item_title", true);
      string str4 = ScriptLocalization.Get("item_use_random_teleport_warning", true);
      UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
      {
        setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
        title = str3,
        leftButtonText = str2,
        leftButtonClickEvent = (System.Action) null,
        rightButtonText = str1,
        rightButtonClickEvent = (System.Action) (() => this.DirectlyUse(resultHandler)),
        description = str4
      });
    }
  }
}
