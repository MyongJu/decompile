﻿// Decompiled with JetBrains decompiler
// Type: AllianceStoreFundItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceStoreFundItem : MonoBehaviour
{
  public UITexture m_ItemIcon;
  public UILabel m_ItemPrice;
  public UILabel m_ItemName;
  public UILabel m_LevelLimit;
  public GameObject m_PricePanel;
  public GameObject m_LockPanel;
  public Transform m_border;
  public UITexture m_backgroud;
  private int _itemId;
  private AllianceShopInfo m_ShopInfo;

  public void SetData(AllianceShopInfo shopInfo)
  {
    this.m_ShopInfo = shopInfo;
    this.UpdateUI();
  }

  public AllianceShopInfo ShopInfo
  {
    get
    {
      return this.m_ShopInfo;
    }
  }

  public void UpdateUI()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, this.m_ShopInfo.ImageIconPath, (System.Action<bool>) null, true, false, string.Empty);
    this.m_ItemIcon.color = !this.Locked() ? Color.white : Color.blue;
    this._itemId = this.m_ShopInfo.ItemId;
    Utils.SetItemName(this.m_ItemName, this.m_ShopInfo.ItemId);
    Utils.SetItemBackground(this.m_backgroud, this.m_ShopInfo.ItemId);
    this.m_ItemPrice.text = Utils.FormatThousands(this.m_ShopInfo.FundPrice.ToString());
    this.m_ItemPrice.color = !this.CanBuy() ? Color.red : new Color(0.9568627f, 0.7921569f, 0.003921569f);
    Dictionary<string, string> para = new Dictionary<string, string>();
    para["num"] = this.m_ShopInfo.AllianceLevel.ToString();
    this.m_LevelLimit.text = ScriptLocalization.GetWithPara("item_alliance_store_requirement_description", para, true);
    this.m_PricePanel.SetActive(!this.Locked());
    this.m_LockPanel.SetActive(this.Locked());
  }

  public void OnClickHandler()
  {
    Utils.ShowItemTip(this._itemId, this.m_border, 0L, 0L, 0);
  }

  public void OnReleaseHandler()
  {
    Utils.StopShowItemTip();
  }

  public void OnPressHandler()
  {
    Utils.DelayShowTip(this._itemId, this.m_border, 0L, 0L, 0);
  }

  public void OnItemClick()
  {
    if (this.Locked())
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_store_insufficient_level"), (System.Action) null, 4f, false);
    else if (!this.CanBuy())
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_store_insufficient_funds"), (System.Action) null, 4f, false);
    else if (!this.SufficientPermission())
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_store_insufficient_permission"), (System.Action) null, 4f, false);
    else
      UIManager.inst.OpenPopup("AllianceFundBuyDialog", (Popup.PopupParameter) new AllianceFundBuyDialog.Parameter()
      {
        allianceShopInfo = this.m_ShopInfo
      });
  }

  private bool CanBuy()
  {
    return (long) this.m_ShopInfo.FundPrice <= PlayerData.inst.allianceData.allianceFund;
  }

  private bool Locked()
  {
    return this.m_ShopInfo.AllianceLevel > PlayerData.inst.allianceData.allianceLevel;
  }

  private bool SufficientPermission()
  {
    if (PlayerData.inst.allianceData == null)
      return false;
    AllianceMemberData allianceMemberData = PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid);
    if (allianceMemberData != null)
      return allianceMemberData.HasAllianceStoreAccess();
    return false;
  }
}
