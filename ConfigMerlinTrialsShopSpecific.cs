﻿// Decompiled with JetBrains decompiler
// Type: ConfigMerlinTrialsShopSpecific
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigMerlinTrialsShopSpecific
{
  private List<MerlinTrialsShopSpecificInfo> _merlinTrialsShopSpecificInfoList = new List<MerlinTrialsShopSpecificInfo>();
  private Dictionary<string, MerlinTrialsShopSpecificInfo> _datas;
  private Dictionary<int, MerlinTrialsShopSpecificInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<MerlinTrialsShopSpecificInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, MerlinTrialsShopSpecificInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._merlinTrialsShopSpecificInfoList.Add(enumerator.Current);
    }
  }

  public void Clear()
  {
    if (this._datas != null)
      this._datas.Clear();
    if (this._dicByUniqueId != null)
      this._dicByUniqueId.Clear();
    if (this._merlinTrialsShopSpecificInfoList == null)
      return;
    this._merlinTrialsShopSpecificInfoList.Clear();
  }

  public List<MerlinTrialsShopSpecificInfo> GetInfoList()
  {
    return this._merlinTrialsShopSpecificInfoList;
  }

  public MerlinTrialsShopSpecificInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (MerlinTrialsShopSpecificInfo) null;
  }

  public MerlinTrialsShopSpecificInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (MerlinTrialsShopSpecificInfo) null;
  }
}
