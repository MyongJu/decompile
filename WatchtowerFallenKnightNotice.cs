﻿// Decompiled with JetBrains decompiler
// Type: WatchtowerFallenKnightNotice
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class WatchtowerFallenKnightNotice : MonoBehaviour
{
  private const string bgPath = "Texture/GUI_Textures/watchtower_fallen_knight";
  public UILabel label0;
  public UILabel label1;
  public UILabel label2;
  public UILabel label3;
  public UILabel btnLabel;
  public UITexture backGround;

  public void UpdateUI()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.backGround, "Texture/GUI_Textures/watchtower_fallen_knight", (System.Action<bool>) null, false, false, string.Empty);
    this.label0.text = Utils.XLAT("watchtower_fallen_knight_title");
    this.label1.text = string.Format(Utils.XLAT("watchtower_fallen_knight_current_wave"), (object) ActivityManager.Intance.knightData.GetFallenKnightCurrentWaves());
    this.label2.text = string.Format(Utils.XLAT("watchtower_fallen_knight_next_wave_time"), (object) ActivityManager.Intance.knightData.GetNextWaveAppearTime());
    this.label3.text = Utils.XLAT("watchtower_fallen_knight_description");
    this.btnLabel.text = Utils.XLAT("watchtower_fallen_knight_help_button");
    Oscillator.Instance.secondEvent += new System.Action<int>(this.UpdateTime);
  }

  private void UpdateTime(int time)
  {
    this.label1.text = string.Format(Utils.XLAT("watchtower_fallen_knight_current_wave"), (object) ActivityManager.Intance.knightData.GetFallenKnightCurrentWaves());
    this.label2.text = string.Format(Utils.XLAT("watchtower_fallen_knight_next_wave_time"), (object) ActivityManager.Intance.knightData.GetNextWaveAppearTime());
  }

  public void OnHelpAlliesBtnClick()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceWarMainDlg", (UI.Dialog.DialogParameter) new AllianceWar_MainDlg.Parameter()
    {
      selectType = AllianceWar_MainDlg.Parameter.SelectType.knight
    }, 1 != 0, 1 != 0, 1 != 0);
  }
}
