﻿// Decompiled with JetBrains decompiler
// Type: ConfigSevenDayQuests
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigSevenDayQuests
{
  private ConfigParse parse = new ConfigParse();
  public Dictionary<string, SevenDayQuestInfo> _datas;
  private Dictionary<int, SevenDayQuestInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<SevenDayQuestInfo, string>(res as Hashtable, "ID", out this._datas, out this.dicByUniqueId);
  }

  public void Clear()
  {
    if (this._datas != null)
    {
      this._datas.Clear();
      this._datas = (Dictionary<string, SevenDayQuestInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, SevenDayQuestInfo>) null;
  }

  public bool Contains(int internalId)
  {
    return this.dicByUniqueId.ContainsKey(internalId);
  }

  public bool Contains(string id)
  {
    return this._datas.ContainsKey(id);
  }

  public SevenDayQuestInfo GetData(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (SevenDayQuestInfo) null;
  }

  public List<SevenDayQuestInfo> GetDataByDay(int day)
  {
    List<SevenDayQuestInfo> sevenDayQuestInfoList = new List<SevenDayQuestInfo>();
    Dictionary<string, SevenDayQuestInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && enumerator.Current.Day == day)
        sevenDayQuestInfoList.Add(enumerator.Current);
    }
    sevenDayQuestInfoList.Sort(new Comparison<SevenDayQuestInfo>(this.Compare));
    return sevenDayQuestInfoList;
  }

  private int Compare(SevenDayQuestInfo a, SevenDayQuestInfo b)
  {
    return a.Sort.CompareTo(b.Sort);
  }

  public SevenDayQuestInfo GetData(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (SevenDayQuestInfo) null;
  }
}
