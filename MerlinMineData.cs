﻿// Decompiled with JetBrains decompiler
// Type: MerlinMineData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class MerlinMineData
{
  public long uid;
  public int world_id;
  public string name;
  public long alliance_id;
  public string alliance_name;
  public string tag;
  public string symbol_code;
  public int end_time;

  public bool IsGatherFinished
  {
    get
    {
      return this.end_time < NetServerTime.inst.ServerTimestamp;
    }
  }
}
