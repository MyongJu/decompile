﻿// Decompiled with JetBrains decompiler
// Type: UIKeyNavigation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Interaction/Key Navigation")]
public class UIKeyNavigation : MonoBehaviour
{
  public static BetterList<UIKeyNavigation> list = new BetterList<UIKeyNavigation>();
  public UIKeyNavigation.Constraint constraint;
  public GameObject onUp;
  public GameObject onDown;
  public GameObject onLeft;
  public GameObject onRight;
  public GameObject onClick;
  public bool startsSelected;

  protected virtual void OnEnable()
  {
    UIKeyNavigation.list.Add(this);
    if (!this.startsSelected || !((Object) UICamera.selectedObject == (Object) null) && NGUITools.GetActive(UICamera.selectedObject))
      return;
    UICamera.currentScheme = UICamera.ControlScheme.Controller;
    UICamera.selectedObject = this.gameObject;
  }

  protected virtual void OnDisable()
  {
    UIKeyNavigation.list.Remove(this);
  }

  protected GameObject GetLeft()
  {
    if (NGUITools.GetActive(this.onLeft))
      return this.onLeft;
    if (this.constraint == UIKeyNavigation.Constraint.Vertical || this.constraint == UIKeyNavigation.Constraint.Explicit)
      return (GameObject) null;
    return this.Get(Vector3.left, true);
  }

  private GameObject GetRight()
  {
    if (NGUITools.GetActive(this.onRight))
      return this.onRight;
    if (this.constraint == UIKeyNavigation.Constraint.Vertical || this.constraint == UIKeyNavigation.Constraint.Explicit)
      return (GameObject) null;
    return this.Get(Vector3.right, true);
  }

  protected GameObject GetUp()
  {
    if (NGUITools.GetActive(this.onUp))
      return this.onUp;
    if (this.constraint == UIKeyNavigation.Constraint.Horizontal || this.constraint == UIKeyNavigation.Constraint.Explicit)
      return (GameObject) null;
    return this.Get(Vector3.up, false);
  }

  protected GameObject GetDown()
  {
    if (NGUITools.GetActive(this.onDown))
      return this.onDown;
    if (this.constraint == UIKeyNavigation.Constraint.Horizontal || this.constraint == UIKeyNavigation.Constraint.Explicit)
      return (GameObject) null;
    return this.Get(Vector3.down, false);
  }

  protected GameObject Get(Vector3 myDir, bool horizontal)
  {
    Transform transform = this.transform;
    myDir = transform.TransformDirection(myDir);
    Vector3 center = UIKeyNavigation.GetCenter(this.gameObject);
    float num = float.MaxValue;
    GameObject gameObject = (GameObject) null;
    for (int index = 0; index < UIKeyNavigation.list.size; ++index)
    {
      UIKeyNavigation uiKeyNavigation = UIKeyNavigation.list[index];
      if (!((Object) uiKeyNavigation == (Object) this))
      {
        UIButton component = uiKeyNavigation.GetComponent<UIButton>();
        if (!((Object) component != (Object) null) || component.isEnabled)
        {
          Vector3 direction = UIKeyNavigation.GetCenter(uiKeyNavigation.gameObject) - center;
          if ((double) Vector3.Dot(myDir, direction.normalized) >= 0.707000017166138)
          {
            Vector3 vector3 = transform.InverseTransformDirection(direction);
            if (horizontal)
              vector3.y *= 2f;
            else
              vector3.x *= 2f;
            float sqrMagnitude = vector3.sqrMagnitude;
            if ((double) sqrMagnitude <= (double) num)
            {
              gameObject = uiKeyNavigation.gameObject;
              num = sqrMagnitude;
            }
          }
        }
      }
    }
    return gameObject;
  }

  protected static Vector3 GetCenter(GameObject go)
  {
    UIWidget component = go.GetComponent<UIWidget>();
    UICamera cameraForLayer = UICamera.FindCameraForLayer(go.layer);
    if ((Object) cameraForLayer != (Object) null)
    {
      Vector3 position = go.transform.position;
      if ((Object) component != (Object) null)
      {
        Vector3[] worldCorners = component.worldCorners;
        position = (worldCorners[0] + worldCorners[2]) * 0.5f;
      }
      Vector3 screenPoint = cameraForLayer.cachedCamera.WorldToScreenPoint(position);
      screenPoint.z = 0.0f;
      return screenPoint;
    }
    if (!((Object) component != (Object) null))
      return go.transform.position;
    Vector3[] worldCorners1 = component.worldCorners;
    return (worldCorners1[0] + worldCorners1[2]) * 0.5f;
  }

  protected virtual void OnKey(KeyCode key)
  {
    if (!NGUITools.GetActive((Behaviour) this))
      return;
    GameObject gameObject = (GameObject) null;
    switch (key)
    {
      case KeyCode.Tab:
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
          gameObject = this.GetLeft();
          if ((Object) gameObject == (Object) null)
            gameObject = this.GetUp();
          if ((Object) gameObject == (Object) null)
            gameObject = this.GetDown();
          if ((Object) gameObject == (Object) null)
          {
            gameObject = this.GetRight();
            break;
          }
          break;
        }
        gameObject = this.GetRight();
        if ((Object) gameObject == (Object) null)
          gameObject = this.GetDown();
        if ((Object) gameObject == (Object) null)
          gameObject = this.GetUp();
        if ((Object) gameObject == (Object) null)
        {
          gameObject = this.GetLeft();
          break;
        }
        break;
      case KeyCode.UpArrow:
        gameObject = this.GetUp();
        break;
      case KeyCode.DownArrow:
        gameObject = this.GetDown();
        break;
      case KeyCode.RightArrow:
        gameObject = this.GetRight();
        break;
      case KeyCode.LeftArrow:
        gameObject = this.GetLeft();
        break;
    }
    if (!((Object) gameObject != (Object) null))
      return;
    UICamera.selectedObject = gameObject;
  }

  protected virtual void OnClick()
  {
    if (!NGUITools.GetActive((Behaviour) this) || !NGUITools.GetActive(this.onClick))
      return;
    UICamera.selectedObject = this.onClick;
  }

  public enum Constraint
  {
    None,
    Vertical,
    Horizontal,
    Explicit,
  }
}
