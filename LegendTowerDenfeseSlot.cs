﻿// Decompiled with JetBrains decompiler
// Type: LegendTowerDenfeseSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UnityEngine;

public class LegendTowerDenfeseSlot : MonoBehaviour
{
  public System.Action onCancelButtonClick;
  private long _legendId;
  [SerializeField]
  private LegendTowerDenfeseSlot.Panel panel;

  public long legendId
  {
    get
    {
      return this._legendId;
    }
    set
    {
      this._legendId = value;
      if (value != 0L)
      {
        this.panel.level.gameObject.SetActive(true);
        this.panel.power.gameObject.SetActive(true);
        LegendData legend1 = DBManager.inst.DB_Legend.GetLegend(value);
        if (legend1 != null)
        {
          LegendData legend2 = DBManager.inst.DB_Legend.GetLegend(this.legendId);
          LegendInfo legendInfo = ConfigManager.inst.DB_Legend.GetLegendInfo(legend2.LegendID);
          if (legendInfo == null)
            return;
          BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.portrait, legendInfo.Icon, (System.Action<bool>) null, true, false, string.Empty);
          int nextXp = 0;
          int legendLevelByXp = ConfigManager.inst.DB_LegendPoint.GetLegendLevelByXP(legend2.Xp, out nextXp);
          this.panel.power.text = Utils.FormatThousands(legend2.Power.ToString());
          this.panel.level.text = "Lv. " + Utils.FormatThousands(legendLevelByXp.ToString());
          if (legend1.Skills == null)
          {
            for (int index = 0; index < 3; ++index)
              this.panel.skillSlots[index].skillId = 0;
          }
          else
          {
            int index;
            for (index = 0; index < legend1.Skills.Length; ++index)
              this.panel.skillSlots[index].skillId = legend1.Skills[index];
            for (; index < 3; ++index)
              this.panel.skillSlots[index].skillId = 0;
          }
          this.panel.BT_Cancel.gameObject.SetActive(true);
          return;
        }
      }
      else
      {
        this.panel.portrait.mainTexture = this.panel.TT_emptyPortrait;
        this.panel.level.gameObject.SetActive(false);
        this.panel.power.gameObject.SetActive(false);
      }
      for (int index = 0; index < 3; ++index)
        this.panel.skillSlots[index].skillId = 0;
      this.panel.BT_Cancel.gameObject.SetActive(false);
    }
  }

  public void OnCancelLegend()
  {
    if (this.onCancelButtonClick == null)
      return;
    this.onCancelButtonClick();
  }

  [Serializable]
  protected class Panel
  {
    public LegendTowerSkillSlot[] skillSlots = new LegendTowerSkillSlot[3];
    public UITexture portrait;
    public Texture TT_emptyPortrait;
    public UILabel level;
    public UILabel power;
    public UIButton BT_Cancel;
  }
}
