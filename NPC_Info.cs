﻿// Decompiled with JetBrains decompiler
// Type: NPC_Info
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class NPC_Info
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "uid")]
  public long uid;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "icon")]
  public string icon;
  [Config(Name = "para_1")]
  public int para_1;
  [Config(Name = "para_2")]
  public string para_2;
  [Config(Name = "type")]
  public string type;
  public int iconIndex;
  public string Description;

  public string LOC_Name
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }

  public string iconPath
  {
    get
    {
      return this.icon;
    }
  }
}
