﻿// Decompiled with JetBrains decompiler
// Type: PitRewardItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class PitRewardItem : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelRank;
  [SerializeField]
  private Icon[] _iconTemplate;
  [SerializeField]
  private UITable _tableContainer;

  public void SetData(AbyssRankRewardInfo rewardInfo, System.Action<Icon> clickCallback, System.Action<Icon> pressCallback, System.Action<Icon> releaseCallback)
  {
    foreach (Component component in this._iconTemplate)
      component.gameObject.SetActive(false);
    if (rewardInfo.rankMin == rewardInfo.rankMax)
      this._labelRank.text = ScriptLocalization.GetWithPara("event_rank_num", new Dictionary<string, string>()
      {
        {
          "0",
          rewardInfo.rankMin.ToString()
        }
      }, true);
    else
      this._labelRank.text = ScriptLocalization.GetWithPara("event_ranks_between_num", new Dictionary<string, string>()
      {
        {
          "1",
          rewardInfo.rankMin.ToString()
        },
        {
          "2",
          rewardInfo.rankMax.ToString()
        }
      }, true);
    for (int index = 0; index < rewardInfo.allRewardId.Length && index < rewardInfo.allRewardValue.Length; ++index)
    {
      int internalId = rewardInfo.allRewardId[index];
      int num = rewardInfo.allRewardValue[index];
      if (internalId != 0 && num != 0)
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
        if (itemStaticInfo != null)
        {
          Icon icon = this.CreateIcon(index % 2);
          icon.FeedData((IComponentData) new IconData(itemStaticInfo.ImagePath, new string[2]
          {
            itemStaticInfo.LocName,
            string.Format("X{0}", (object) num)
          })
          {
            Data = (object) internalId
          });
          icon.OnIconClickDelegate = clickCallback;
          icon.OnIconPressDelegate = pressCallback;
          icon.OnIconRelaseDelegate = releaseCallback;
        }
      }
    }
    this._tableContainer.Reposition();
  }

  private Icon CreateIcon(int templateIndex)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._iconTemplate[templateIndex].gameObject);
    gameObject.SetActive(true);
    gameObject.transform.SetParent(this._tableContainer.transform);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    return gameObject.GetComponent<Icon>();
  }
}
