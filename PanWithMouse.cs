﻿// Decompiled with JetBrains decompiler
// Type: PanWithMouse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Examples/Pan With Mouse")]
public class PanWithMouse : MonoBehaviour
{
  public Vector2 degrees = new Vector2(5f, 3f);
  public float range = 1f;
  private Vector2 mRot = Vector2.zero;
  private Transform mTrans;
  private Quaternion mStart;

  private void Start()
  {
    this.mTrans = this.transform;
    this.mStart = this.mTrans.localRotation;
  }

  private void Update()
  {
    float deltaTime = RealTime.deltaTime;
    Vector3 mousePosition = Input.mousePosition;
    float num1 = (float) Screen.width * 0.5f;
    float num2 = (float) Screen.height * 0.5f;
    if ((double) this.range < 0.100000001490116)
      this.range = 0.1f;
    this.mRot = Vector2.Lerp(this.mRot, new Vector2(Mathf.Clamp((mousePosition.x - num1) / num1 / this.range, -1f, 1f), Mathf.Clamp((mousePosition.y - num2) / num2 / this.range, -1f, 1f)), deltaTime * 5f);
    this.mTrans.localRotation = this.mStart * Quaternion.Euler(-this.mRot.y * this.degrees.y, this.mRot.x * this.degrees.x, 0.0f);
  }
}
