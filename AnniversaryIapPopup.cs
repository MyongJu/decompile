﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryIapPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AnniversaryIapPopup : Popup
{
  private List<IAPStoreContentRenderer> _itemList = new List<IAPStoreContentRenderer>();
  public UILabel m_PackageName;
  public UILabel m_GoldCount1;
  public UILabel m_GoldCount2;
  public UILabel m_GoldBonus;
  public UILabel m_Price1;
  public UILabel m_Price2;
  public GameObject m_ItemPrefab;
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  private AnniversaryIapPopup.Parameter _parameter;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._parameter = orgParam as AnniversaryIapPopup.Parameter;
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter OrgParam)
  {
    base.OnHide(OrgParam);
    this.ClearData();
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnBuy()
  {
    if (this._parameter.OnPurchaseBegin != null)
      this._parameter.OnPurchaseBegin();
    IAPStorePackagePayload.Instance.BuySpecialProduct(this._parameter.annvIapMainInfo.internalId, this._parameter.annvIapMainInfo.IapPackageInfo.productId, this._parameter.annvIapMainInfo.name, this._parameter.annvIapMainInfo.IapGroupName, (System.Action) (() =>
    {
      if (this._parameter.OnPurchaseEnd == null)
        return;
      this._parameter.OnPurchaseEnd(this._parameter.annvIapMainInfo.internalId);
    }));
    this.OnClosePressed();
  }

  private void UpdateUI()
  {
    IAPPackageInfo iapPackageInfo = this._parameter.annvIapMainInfo.IapPackageInfo;
    if (iapPackageInfo != null)
    {
      this.m_PackageName.text = this._parameter.annvIapMainInfo.Name;
      UILabel goldCount2 = this.m_GoldCount2;
      string str1 = ScriptLocalization.Get("id_gold", true) + " +" + Utils.FormatThousands(iapPackageInfo.gold.ToString());
      this.m_GoldCount1.text = str1;
      string str2 = str1;
      goldCount2.text = str2;
      this.m_GoldBonus.text = ScriptLocalization.GetWithPara("iap_gold_package_contents_value_description", new Dictionary<string, string>()
      {
        {
          "0",
          Utils.FormatThousands(iapPackageInfo.bonus_gold.ToString())
        }
      }, true);
      double num = PaymentManager.Instance.GetRealPrice(iapPackageInfo.productId, iapPackageInfo.pay_id) * iapPackageInfo.discount / 100.0;
      this.m_Price1.text = PaymentManager.Instance.GetCurrencyCode(iapPackageInfo.productId, iapPackageInfo.pay_id) + Utils.FormatThousands(num.ToString("f2"));
      this.m_Price2.text = PaymentManager.Instance.GetFormattedPrice(iapPackageInfo.productId, iapPackageInfo.pay_id);
    }
    this.UpdateData();
  }

  private void ClearData()
  {
    using (List<IAPStoreContentRenderer>.Enumerator enumerator = this._itemList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IAPStoreContentRenderer current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this._itemList.Clear();
  }

  private void UpdateData()
  {
    this.ClearData();
    if (this._parameter.annvIapMainInfo.IapPackageInfo != null)
      this.UpdateData(ConfigManager.inst.DB_IAPRewards.Get(this._parameter.annvIapMainInfo.IapPackageInfo.reward_group_id));
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  private void UpdateData(IAPRewardInfo rewardInfo)
  {
    if (rewardInfo == null || rewardInfo.Rewards == null)
      return;
    List<Reward.RewardsValuePair> rewards = rewardInfo.GetRewards();
    rewards.Sort(new Comparison<Reward.RewardsValuePair>(AnniversaryIapPopup.Compare));
    for (int index = 0; index < rewards.Count; ++index)
    {
      Reward.RewardsValuePair rewardsValuePair = rewards[index];
      int internalId = rewardsValuePair.internalID;
      int itemCount = rewardsValuePair.value;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_Grid.transform;
      gameObject.transform.localScale = Vector3.one;
      IAPStoreContentRenderer component = gameObject.GetComponent<IAPStoreContentRenderer>();
      component.SetData(itemStaticInfo, itemCount);
      this._itemList.Add(component);
    }
  }

  private static int Compare(Reward.RewardsValuePair x, Reward.RewardsValuePair y)
  {
    return ConfigManager.inst.DB_Items.GetItem(x.internalID).Priority - ConfigManager.inst.DB_Items.GetItem(y.internalID).Priority;
  }

  public class Parameter : Popup.PopupParameter
  {
    public AnniversaryIapMainInfo annvIapMainInfo;
    public System.Action OnPurchaseBegin;
    public System.Action<int> OnPurchaseEnd;
  }
}
