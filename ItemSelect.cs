﻿// Decompiled with JetBrains decompiler
// Type: ItemSelect
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class ItemSelect : MonoBehaviour
{
  private Dictionary<int, ShopItemUI> id2item = new Dictionary<int, ShopItemUI>();
  public GameObject itemClone;
  public UITable itemTable;
  public UIScrollView drawPanel;
  public UILabel goldVal;
  private ShopItemUI lastFocus;

  public void SetSelectTypes(params ItemBag.ItemType[] types)
  {
  }

  private void OnPlayerStatChanged(Events.PlayerStatChangedArgs ev)
  {
    if (ev.Stat != Events.PlayerStat.Currency)
      return;
    this.goldVal.text = ev.NewValue.ToString("N0", (IFormatProvider) CultureInfo.InvariantCulture);
  }

  private void Start()
  {
    this.OnPlayerStatChanged(new Events.PlayerStatChangedArgs(Events.PlayerStat.Currency, 0.0, (double) GameEngine.Instance.PlayerData.hostPlayer.Currency));
    GameEngine.Instance.events.onPlayerStatChanged += new System.Action<Events.PlayerStatChangedArgs>(this.OnPlayerStatChanged);
  }

  private void RefreshItem()
  {
  }

  private void Clear()
  {
    using (Dictionary<int, ShopItemUI>.ValueCollection.Enumerator enumerator = this.id2item.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.id2item.Clear();
  }

  public void CloseUI()
  {
    this.gameObject.SetActive(false);
  }

  public void OnEnable()
  {
    GameDataManager.inst.LockScrolling();
  }

  public void OnDisable()
  {
    GameDataManager.inst.UnlockScrolling();
  }

  public void OnItemClick(ShopItemUI si)
  {
  }
}
