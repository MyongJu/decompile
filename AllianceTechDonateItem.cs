﻿// Decompiled with JetBrains decompiler
// Type: AllianceTechDonateItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceTechDonateItem : MonoBehaviour
{
  private Dictionary<string, long> m_lackRes = new Dictionary<string, long>(4);
  private Dictionary<string, int> m_lackitem = new Dictionary<string, int>(1);
  private ItemBag.ItemType resourceType = ItemBag.ItemType.food;
  public UILabel title;
  public UILabel allianceDonation;
  public UILabel allianceXp;
  public UILabel honorValue;
  public RewardNormalItem[] rewardItems;
  public int index;
  public UISprite resourceIcon;
  public UILabel donateValue;
  public GameObject closeContent;
  private AllianceTechDonateItem.Data _data;
  public System.Action<int, int> OnDonateDelegater;
  private long currentValue;
  private bool displayClearCD;
  public System.Action OnRefreshHandler;

  public void OnDonateHandler()
  {
    if (!PlayerData.inst.allianceTechManager.canDonate)
    {
      if (this.displayClearCD)
        return;
      this.displayClearCD = true;
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_donate_cd_second_per_gold");
      int price = 1;
      if (data != null)
        price = data.ValueInt;
      int time = PlayerData.inst.allianceTechManager.codeDownTime - NetServerTime.inst.ServerTimestamp;
      int cost = ItemBag.CalculateCost(time, price);
      UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
      {
        confirmButtonClickEvent = new System.Action(this.ConFirmCallBack),
        closeButtonCallbackEvent = new System.Action(this.OnClosePopUpHandler),
        lefttime = time,
        goldNum = cost,
        unitprice = price,
        description = ScriptLocalization.GetWithPara("confirm_gold_spend_cooldown_instant_desc", new Dictionary<string, string>()
        {
          {
            "num",
            cost.ToString()
          }
        }, true)
      });
    }
    else if (this._data.count > this.currentValue)
    {
      if (this._data.type == "gold")
      {
        UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
        Utils.ShowNotEnoughGoldTip();
      }
      else
        this.ResNotEngough();
    }
    else
    {
      if (this.OnDonateDelegater == null)
        return;
      this.OnDonateDelegater(this.index, 0);
    }
  }

  private void OnClosePopUpHandler()
  {
    this.displayClearCD = false;
  }

  private void ConFirmCallBack()
  {
    this.displayClearCD = false;
    RequestManager.inst.SendRequest("AllianceTech:clearCdTime", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, (System.Action<bool, object>) ((arg1, result) =>
    {
      if (!arg1)
        return;
      PlayerData.inst.allianceTechManager.Update(result);
      if (this.OnRefreshHandler == null)
        return;
      this.OnRefreshHandler();
    }), true);
  }

  private void ResNotEngough()
  {
    Hashtable hashtable = new Hashtable();
    hashtable.Add((object) this.resourceType, (object) this._data.count);
    Hashtable ht = new Hashtable();
    ht.Add((object) "resources", (object) hashtable);
    int cost = ItemBag.CalculateCost(ht);
    UIManager.inst.OpenPopup("BuildingResPopUp", (Popup.PopupParameter) new BuildingResPopUp.Parameter()
    {
      lackItem = this.m_lackitem,
      lackRes = this.m_lackRes,
      requireRes = ht,
      action = ScriptLocalization.Get("id_uppercase_donate", true),
      gold = cost,
      buyResourCallBack = new System.Action<int>(this.OnInstBuyPressed),
      title = ScriptLocalization.Get("alliance_knowledge_donate_not_enough_resources", true),
      content = ScriptLocalization.Get("alliance_knowledge_donate_not_enough_resources_description", true)
    });
  }

  private void OnInstBuyPressed(int gold)
  {
    if (PlayerData.inst.hostPlayer.Currency < gold)
    {
      Utils.ShowNotEnoughGoldTip();
    }
    else
    {
      if (this.OnDonateDelegater == null)
        return;
      this.OnDonateDelegater(this.index, gold);
    }
  }

  public void SetData(AllianceTechDonateData.AllianceTechDonateStruct allianceTechDonateStruct)
  {
    AllianceTechDonateItem.Data data = new AllianceTechDonateItem.Data();
    if (allianceTechDonateStruct == null || string.IsNullOrEmpty(allianceTechDonateStruct.type))
    {
      data.isOpen = false;
      this.SetDetail(data);
    }
    else
    {
      data.type = allianceTechDonateStruct.type;
      data.count = allianceTechDonateStruct.count;
      data.honorValue = allianceTechDonateStruct.rewardHonor;
      data.allianceFund = allianceTechDonateStruct.rewardFund;
      data.allianceXp = allianceTechDonateStruct.rewardXp;
      data.allianceDonation = allianceTechDonateStruct.rewardDonation;
      data.isOpen = !string.IsNullOrEmpty(allianceTechDonateStruct.type);
      this.SetDetail(data);
    }
  }

  private void SetDetail(AllianceTechDonateItem.Data data)
  {
    this._data = data;
    if (data.isOpen)
    {
      if ((UnityEngine.Object) this.closeContent != (UnityEngine.Object) null)
        NGUITools.SetActive(this.closeContent, false);
      CityData byUid = DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid);
      string str = string.Empty;
      this.currentValue = 0L;
      this.m_lackRes.Clear();
      this.m_lackitem.Clear();
      string key = string.Empty;
      if (this.rewardItems != null && this.rewardItems.Length > 0)
      {
        this.rewardItems[0].SetData(data.allianceDonation, RewardNormalItem.RewardType.none);
        this.rewardItems[1].SetData(data.allianceXp, RewardNormalItem.RewardType.none);
        this.rewardItems[2].SetData(data.honorValue, RewardNormalItem.RewardType.none);
      }
      string type = data.type;
      if (type != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (AllianceTechDonateItem.\u003C\u003Ef__switch\u0024map18 == null)
        {
          // ISSUE: reference to a compiler-generated field
          AllianceTechDonateItem.\u003C\u003Ef__switch\u0024map18 = new Dictionary<string, int>(5)
          {
            {
              "food",
              0
            },
            {
              "wood",
              1
            },
            {
              "ore",
              2
            },
            {
              "silver",
              3
            },
            {
              "gold",
              4
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (AllianceTechDonateItem.\u003C\u003Ef__switch\u0024map18.TryGetValue(type, out num))
        {
          switch (num)
          {
            case 0:
              str = "food_icon";
              this.resourceType = ItemBag.ItemType.food;
              key = "Texture/BuildingConstruction/food_icon";
              this.currentValue = (long) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD);
              break;
            case 1:
              str = "wood_icon";
              this.resourceType = ItemBag.ItemType.wood;
              key = "Texture/BuildingConstruction/wood_icon";
              this.currentValue = (long) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD);
              break;
            case 2:
              str = "ore_icon";
              this.resourceType = ItemBag.ItemType.ore;
              key = "Texture/BuildingConstruction/ore_icon";
              this.currentValue = (long) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE);
              break;
            case 3:
              str = "silver_icon";
              this.resourceType = ItemBag.ItemType.silver;
              key = "Texture/BuildingConstruction/silver_icon";
              this.currentValue = (long) byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
              break;
            case 4:
              str = "icon_currency_premium";
              this.currentValue = PlayerData.inst.userData.currency.gold;
              break;
          }
        }
      }
      if (this.currentValue < data.count)
        this.m_lackRes.Add(key, data.count - this.currentValue);
      this.resourceIcon.spriteName = str;
      this.donateValue.text = data.count.ToString();
      this.donateValue.color = this.currentValue >= data.count ? Color.white : new Color(0.9058824f, 0.07058824f, 0.0f);
    }
    else
    {
      this.Clear();
      if (!((UnityEngine.Object) this.closeContent != (UnityEngine.Object) null))
        return;
      NGUITools.SetActive(this.closeContent, true);
    }
  }

  private void Clear()
  {
    this.donateValue.color = Color.white;
    this.honorValue.text = "?";
    this.allianceDonation.text = "?";
    this.allianceXp.text = "?";
    this.resourceIcon.spriteName = "?";
    this.donateValue.text = "?";
  }

  public struct Data
  {
    public long honorValue;
    public long allianceDonation;
    public long allianceFund;
    public long allianceXp;
    public string type;
    public long count;
    public bool isOpen;
  }
}
