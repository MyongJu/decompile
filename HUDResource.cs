﻿// Decompiled with JetBrains decompiler
// Type: HUDResource
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class HUDResource : MonoBehaviour
{
  private List<long> currentEffects = new List<long>();
  public const string ASSET_PATH = "Prefab/City/Resourcecollection";
  public UILabel mResourceValue;
  public UISprite mResouceIcon;
  public GameObject effectParent;
  public UISpriteAnimation animation;
  private bool playAnimation;

  private void Start()
  {
    if (!((Object) this.effectParent != (Object) null))
      return;
    NGUITools.SetActive(this.effectParent.gameObject, false);
  }

  public void SetLevels(long value, long maxValue)
  {
    this.mResourceValue.text = Utils.FormatShortThousandsLong(value);
  }

  public void PlayIncreaseAnimaition(Transform parent, Vector3 position, long resource)
  {
    this.StartCoroutine(this.PlayResource(parent, position, resource));
  }

  [DebuggerHidden]
  private IEnumerator PlayResource(Transform parent, Vector3 position, long resource)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HUDResource.\u003CPlayResource\u003Ec__Iterator6D()
    {
      parent = parent,
      position = position,
      resource = resource,
      \u003C\u0024\u003Eparent = parent,
      \u003C\u0024\u003Eposition = position,
      \u003C\u0024\u003Eresource = resource
    };
  }

  public void PlayAnimation()
  {
    if (this.currentEffects == null)
      this.currentEffects = new List<long>();
    if ((Object) this.effectParent != (Object) null)
    {
      NGUITools.SetActive(this.effectParent.gameObject, true);
      this.animation.ResetToBeginning();
      this.animation.Play();
      this.playAnimation = true;
    }
    iTween.ScaleFrom(this.mResouceIcon.gameObject, iTween.Hash((object) "scale", (object) new Vector3(2f, 2f, 0.0f), (object) "time", (object) 1.5f));
  }

  private void Update()
  {
    if (!this.playAnimation || this.animation.isPlaying)
      return;
    this.Clear();
    this.playAnimation = false;
  }

  private void OnDisable()
  {
    this.Clear();
  }

  private void Clear()
  {
    if (!((Object) this.effectParent != (Object) null))
      return;
    NGUITools.SetActive(this.effectParent.gameObject, false);
  }

  public void OnCompleteHandler(object param)
  {
    long guid = long.Parse(param.ToString());
    VfxManager.Instance.Delete(guid);
    this.currentEffects.Remove(guid);
  }
}
