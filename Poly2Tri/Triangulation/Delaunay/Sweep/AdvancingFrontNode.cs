﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.Delaunay.Sweep.AdvancingFrontNode
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Poly2Tri.Triangulation.Delaunay.Sweep
{
  public class AdvancingFrontNode
  {
    public AdvancingFrontNode Next;
    public TriangulationPoint Point;
    public AdvancingFrontNode Prev;
    public DelaunayTriangle Triangle;
    public double Value;

    public AdvancingFrontNode(TriangulationPoint point)
    {
      this.Point = point;
      this.Value = point.X;
    }

    public bool HasNext
    {
      get
      {
        return this.Next != null;
      }
    }

    public bool HasPrev
    {
      get
      {
        return this.Prev != null;
      }
    }
  }
}
