﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.Delaunay.Sweep.AdvancingFront
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;

namespace Poly2Tri.Triangulation.Delaunay.Sweep
{
  public class AdvancingFront
  {
    public AdvancingFrontNode Head;
    protected AdvancingFrontNode Search;
    public AdvancingFrontNode Tail;

    public AdvancingFront(AdvancingFrontNode head, AdvancingFrontNode tail)
    {
      this.Head = head;
      this.Tail = tail;
      this.Search = head;
      this.AddNode(head);
      this.AddNode(tail);
    }

    public void AddNode(AdvancingFrontNode node)
    {
    }

    public void RemoveNode(AdvancingFrontNode node)
    {
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder();
      for (AdvancingFrontNode advancingFrontNode = this.Head; advancingFrontNode != this.Tail; advancingFrontNode = advancingFrontNode.Next)
        stringBuilder.Append(advancingFrontNode.Point.X).Append("->");
      stringBuilder.Append(this.Tail.Point.X);
      return stringBuilder.ToString();
    }

    private AdvancingFrontNode FindSearchNode(double x)
    {
      return this.Search;
    }

    public AdvancingFrontNode LocateNode(TriangulationPoint point)
    {
      return this.LocateNode(point.X);
    }

    private AdvancingFrontNode LocateNode(double x)
    {
      AdvancingFrontNode advancingFrontNode = this.FindSearchNode(x);
      if (x < advancingFrontNode.Value)
      {
        while ((advancingFrontNode = advancingFrontNode.Prev) != null)
        {
          if (x >= advancingFrontNode.Value)
          {
            this.Search = advancingFrontNode;
            return advancingFrontNode;
          }
        }
      }
      else
      {
        while ((advancingFrontNode = advancingFrontNode.Next) != null)
        {
          if (x < advancingFrontNode.Value)
          {
            this.Search = advancingFrontNode.Prev;
            return advancingFrontNode.Prev;
          }
        }
      }
      return (AdvancingFrontNode) null;
    }

    public AdvancingFrontNode LocatePoint(TriangulationPoint point)
    {
      double x1 = point.X;
      AdvancingFrontNode advancingFrontNode = this.FindSearchNode(x1);
      double x2 = advancingFrontNode.Point.X;
      if (x1 == x2)
      {
        if (point != advancingFrontNode.Point)
        {
          if (point == advancingFrontNode.Prev.Point)
          {
            advancingFrontNode = advancingFrontNode.Prev;
          }
          else
          {
            if (point != advancingFrontNode.Next.Point)
              throw new Exception("Failed to find Node for given afront point");
            advancingFrontNode = advancingFrontNode.Next;
          }
        }
      }
      else if (x1 < x2)
      {
        while ((advancingFrontNode = advancingFrontNode.Prev) != null && point != advancingFrontNode.Point)
          ;
      }
      else
      {
        while ((advancingFrontNode = advancingFrontNode.Next) != null && point != advancingFrontNode.Point)
          ;
      }
      this.Search = advancingFrontNode;
      return advancingFrontNode;
    }
  }
}
