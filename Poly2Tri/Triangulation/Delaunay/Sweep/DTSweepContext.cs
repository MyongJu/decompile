﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.Delaunay.Sweep.DTSweepContext
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace Poly2Tri.Triangulation.Delaunay.Sweep
{
  public class DTSweepContext : TriangulationContext
  {
    public DTSweepContext.DTSweepBasin Basin = new DTSweepContext.DTSweepBasin();
    public DTSweepContext.DTSweepEdgeEvent EdgeEvent = new DTSweepContext.DTSweepEdgeEvent();
    private DTSweepPointComparator _comparator = new DTSweepPointComparator();
    private const float ALPHA = 0.3f;
    public AdvancingFront aFront;

    public DTSweepContext()
    {
      this.Clear();
    }

    public TriangulationPoint Head { get; set; }

    public TriangulationPoint Tail { get; set; }

    public void RemoveFromList(DelaunayTriangle triangle)
    {
      this.Triangles.Remove(triangle);
    }

    public void MeshClean(DelaunayTriangle triangle)
    {
      this.MeshCleanReq(triangle);
    }

    private void MeshCleanReq(DelaunayTriangle triangle)
    {
      if (triangle == null || triangle.IsInterior)
        return;
      triangle.IsInterior = true;
      this.Triangulatable.AddTriangle(triangle);
      for (int index = 0; index < 3; ++index)
      {
        if (!triangle.EdgeIsConstrained[index])
          this.MeshCleanReq(triangle.Neighbors[index]);
      }
    }

    public override void Clear()
    {
      base.Clear();
      this.Triangles.Clear();
    }

    public void AddNode(AdvancingFrontNode node)
    {
      this.aFront.AddNode(node);
    }

    public void RemoveNode(AdvancingFrontNode node)
    {
      this.aFront.RemoveNode(node);
    }

    public AdvancingFrontNode LocateNode(TriangulationPoint point)
    {
      return this.aFront.LocateNode(point);
    }

    public void CreateAdvancingFront()
    {
      DelaunayTriangle delaunayTriangle = new DelaunayTriangle(this.Points[0], this.Tail, this.Head);
      this.Triangles.Add(delaunayTriangle);
      AdvancingFrontNode head = new AdvancingFrontNode(delaunayTriangle.Points[1]);
      head.Triangle = delaunayTriangle;
      AdvancingFrontNode node = new AdvancingFrontNode(delaunayTriangle.Points[0]);
      node.Triangle = delaunayTriangle;
      AdvancingFrontNode tail = new AdvancingFrontNode(delaunayTriangle.Points[2]);
      this.aFront = new AdvancingFront(head, tail);
      this.aFront.AddNode(node);
      this.aFront.Head.Next = node;
      node.Next = this.aFront.Tail;
      node.Prev = this.aFront.Head;
      this.aFront.Tail.Prev = node;
    }

    public void MapTriangleToNodes(DelaunayTriangle t)
    {
      for (int index = 0; index < 3; ++index)
      {
        if (t.Neighbors[index] == null)
        {
          AdvancingFrontNode advancingFrontNode = this.aFront.LocatePoint(t.PointCW(t.Points[index]));
          if (advancingFrontNode != null)
            advancingFrontNode.Triangle = t;
        }
      }
    }

    public override void PrepareTriangulation(Triangulatable t)
    {
      base.PrepareTriangulation(t);
      double x;
      double num1 = x = this.Points[0].X;
      double y;
      double num2 = y = this.Points[0].Y;
      using (List<TriangulationPoint>.Enumerator enumerator = this.Points.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          TriangulationPoint current = enumerator.Current;
          if (current.X > num1)
            num1 = current.X;
          if (current.X < x)
            x = current.X;
          if (current.Y > num2)
            num2 = current.Y;
          if (current.Y < y)
            y = current.Y;
        }
      }
      double num3 = 0.300000011920929 * (num1 - x);
      double num4 = 0.300000011920929 * (num2 - y);
      TriangulationPoint triangulationPoint1 = new TriangulationPoint(num1 + num3, y - num4);
      TriangulationPoint triangulationPoint2 = new TriangulationPoint(x - num3, y - num4);
      this.Head = triangulationPoint1;
      this.Tail = triangulationPoint2;
      this.Points.Sort((IComparer<TriangulationPoint>) this._comparator);
    }

    public void FinalizeTriangulation()
    {
      this.Triangulatable.AddTriangles((IEnumerable<DelaunayTriangle>) this.Triangles);
      this.Triangles.Clear();
    }

    public override TriangulationConstraint NewConstraint(TriangulationPoint a, TriangulationPoint b)
    {
      return (TriangulationConstraint) new DTSweepConstraint(a, b);
    }

    public class DTSweepBasin
    {
      public AdvancingFrontNode bottomNode;
      public bool leftHighest;
      public AdvancingFrontNode leftNode;
      public AdvancingFrontNode rightNode;
      public double width;
    }

    public class DTSweepEdgeEvent
    {
      public DTSweepConstraint ConstrainedEdge;
      public bool Right;
    }
  }
}
