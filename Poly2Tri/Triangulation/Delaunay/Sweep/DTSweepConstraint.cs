﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.Delaunay.Sweep.DTSweepConstraint
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Poly2Tri.Triangulation.Delaunay.Sweep
{
  public class DTSweepConstraint : TriangulationConstraint
  {
    public DTSweepConstraint(TriangulationPoint p1, TriangulationPoint p2)
    {
      this.P = p1;
      this.Q = p2;
      if (p1.Y > p2.Y)
      {
        this.Q = p1;
        this.P = p2;
      }
      else if (p1.Y == p2.Y)
      {
        if (p1.X > p2.X)
        {
          this.Q = p1;
          this.P = p2;
        }
        else if (p1.X != p2.X)
          ;
      }
      this.Q.AddEdge(this);
    }
  }
}
