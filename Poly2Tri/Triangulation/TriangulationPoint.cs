﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.TriangulationPoint
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Poly2Tri.Triangulation.Delaunay.Sweep;
using System.Collections.Generic;

namespace Poly2Tri.Triangulation
{
  public class TriangulationPoint
  {
    public double X;
    public double Y;

    public TriangulationPoint(double x, double y)
    {
      this.X = x;
      this.Y = y;
    }

    public List<DTSweepConstraint> Edges { get; private set; }

    public float Xf
    {
      get
      {
        return (float) this.X;
      }
      set
      {
        this.X = (double) value;
      }
    }

    public float Yf
    {
      get
      {
        return (float) this.Y;
      }
      set
      {
        this.Y = (double) value;
      }
    }

    public bool HasEdges
    {
      get
      {
        return this.Edges != null;
      }
    }

    public override string ToString()
    {
      return "[" + (object) this.X + "," + (object) this.Y + "]";
    }

    public void AddEdge(DTSweepConstraint e)
    {
      if (this.Edges == null)
        this.Edges = new List<DTSweepConstraint>();
      this.Edges.Add(e);
    }
  }
}
