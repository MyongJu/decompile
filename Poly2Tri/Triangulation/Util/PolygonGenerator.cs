﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.Util.PolygonGenerator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Poly2Tri.Triangulation.Polygon;
using System;
using System.Collections.Generic;

namespace Poly2Tri.Triangulation.Util
{
  public class PolygonGenerator
  {
    private static readonly Random RNG = new Random();
    private static double PI_2 = 2.0 * Math.PI;

    public static Poly2Tri.Triangulation.Polygon.Polygon RandomCircleSweep(double scale, int vertexCount)
    {
      double num1 = scale / 4.0;
      PolygonPoint[] polygonPointArray = new PolygonPoint[vertexCount];
      for (int index = 0; index < vertexCount; ++index)
      {
        do
        {
          double num2 = index % 250 != 0 ? (index % 50 != 0 ? num1 + 25.0 * scale / (double) vertexCount * (0.5 - PolygonGenerator.RNG.NextDouble()) : num1 + scale / 5.0 * (0.5 - PolygonGenerator.RNG.NextDouble())) : num1 + scale / 2.0 * (0.5 - PolygonGenerator.RNG.NextDouble());
          double num3 = num2 <= scale / 2.0 ? num2 : scale / 2.0;
          num1 = num3 >= scale / 10.0 ? num3 : scale / 10.0;
        }
        while (num1 < scale / 10.0 || num1 > scale / 2.0);
        PolygonPoint polygonPoint = new PolygonPoint(num1 * Math.Cos(PolygonGenerator.PI_2 * (double) index / (double) vertexCount), num1 * Math.Sin(PolygonGenerator.PI_2 * (double) index / (double) vertexCount));
        polygonPointArray[index] = polygonPoint;
      }
      return new Poly2Tri.Triangulation.Polygon.Polygon((IList<PolygonPoint>) polygonPointArray);
    }

    public static Poly2Tri.Triangulation.Polygon.Polygon RandomCircleSweep2(double scale, int vertexCount)
    {
      double num1 = scale / 4.0;
      PolygonPoint[] polygonPointArray = new PolygonPoint[vertexCount];
      for (int index = 0; index < vertexCount; ++index)
      {
        do
        {
          double num2 = num1 + scale / 5.0 * (0.5 - PolygonGenerator.RNG.NextDouble());
          double num3 = num2 <= scale / 2.0 ? num2 : scale / 2.0;
          num1 = num3 >= scale / 10.0 ? num3 : scale / 10.0;
        }
        while (num1 < scale / 10.0 || num1 > scale / 2.0);
        PolygonPoint polygonPoint = new PolygonPoint(num1 * Math.Cos(PolygonGenerator.PI_2 * (double) index / (double) vertexCount), num1 * Math.Sin(PolygonGenerator.PI_2 * (double) index / (double) vertexCount));
        polygonPointArray[index] = polygonPoint;
      }
      return new Poly2Tri.Triangulation.Polygon.Polygon((IList<PolygonPoint>) polygonPointArray);
    }
  }
}
