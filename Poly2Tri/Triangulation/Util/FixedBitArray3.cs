﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.Util.FixedBitArray3
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Poly2Tri.Triangulation.Util
{
  public struct FixedBitArray3 : IEnumerable, IEnumerable<bool>
  {
    public bool _0;
    public bool _1;
    public bool _2;

    IEnumerator IEnumerable.GetEnumerator()
    {
      return (IEnumerator) this.GetEnumerator();
    }

    public bool this[int index]
    {
      get
      {
        switch (index)
        {
          case 0:
            return this._0;
          case 1:
            return this._1;
          case 2:
            return this._2;
          default:
            throw new IndexOutOfRangeException();
        }
      }
      set
      {
        switch (index)
        {
          case 0:
            this._0 = value;
            break;
          case 1:
            this._1 = value;
            break;
          case 2:
            this._2 = value;
            break;
          default:
            throw new IndexOutOfRangeException();
        }
      }
    }

    public IEnumerator<bool> GetEnumerator()
    {
      return this.Enumerate().GetEnumerator();
    }

    public bool Contains(bool value)
    {
      for (int index = 0; index < 3; ++index)
      {
        if (this[index] == value)
          return true;
      }
      return false;
    }

    public int IndexOf(bool value)
    {
      for (int index = 0; index < 3; ++index)
      {
        if (this[index] == value)
          return index;
      }
      return -1;
    }

    public void Clear()
    {
      this._0 = this._1 = this._2 = false;
    }

    public void Clear(bool value)
    {
      for (int index = 0; index < 3; ++index)
      {
        if (this[index] == value)
          this[index] = false;
      }
    }

    [DebuggerHidden]
    private IEnumerable<bool> Enumerate()
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      FixedBitArray3.\u003CEnumerate\u003Ec__Iterator12 enumerateCIterator12 = new FixedBitArray3.\u003CEnumerate\u003Ec__Iterator12()
      {
        \u003C\u003Ef__this = this
      };
      // ISSUE: reference to a compiler-generated field
      enumerateCIterator12.\u0024PC = -2;
      return (IEnumerable<bool>) enumerateCIterator12;
    }
  }
}
