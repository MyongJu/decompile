﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.Util.FixedArray3`1
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Poly2Tri.Triangulation.Util
{
  public struct FixedArray3<T> : IEnumerable, IEnumerable<T> where T : class
  {
    public T _0;
    public T _1;
    public T _2;

    IEnumerator IEnumerable.GetEnumerator()
    {
      return (IEnumerator) this.GetEnumerator();
    }

    public T this[int index]
    {
      get
      {
        switch (index)
        {
          case 0:
            return this._0;
          case 1:
            return this._1;
          case 2:
            return this._2;
          default:
            throw new IndexOutOfRangeException();
        }
      }
      set
      {
        switch (index)
        {
          case 0:
            this._0 = value;
            break;
          case 1:
            this._1 = value;
            break;
          case 2:
            this._2 = value;
            break;
          default:
            throw new IndexOutOfRangeException();
        }
      }
    }

    public IEnumerator<T> GetEnumerator()
    {
      return this.Enumerate().GetEnumerator();
    }

    public bool Contains(T value)
    {
      for (int index = 0; index < 3; ++index)
      {
        if ((object) this[index] == (object) value)
          return true;
      }
      return false;
    }

    public int IndexOf(T value)
    {
      for (int index = 0; index < 3; ++index)
      {
        if ((object) this[index] == (object) value)
          return index;
      }
      return -1;
    }

    public void Clear()
    {
      this._0 = this._1 = this._2 = (T) null;
    }

    public void Clear(T value)
    {
      for (int index = 0; index < 3; ++index)
      {
        if ((object) this[index] == (object) value)
          this[index] = (T) null;
      }
    }

    [DebuggerHidden]
    private IEnumerable<T> Enumerate()
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      FixedArray3<T>.\u003CEnumerate\u003Ec__Iterator11 enumerateCIterator11 = new FixedArray3<T>.\u003CEnumerate\u003Ec__Iterator11()
      {
        \u003C\u003Ef__this = this
      };
      // ISSUE: reference to a compiler-generated field
      enumerateCIterator11.\u0024PC = -2;
      return (IEnumerable<T>) enumerateCIterator11;
    }
  }
}
