﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.Util.PointGenerator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace Poly2Tri.Triangulation.Util
{
  public class PointGenerator
  {
    private static readonly Random RNG = new Random();

    public static List<TriangulationPoint> UniformDistribution(int n, double scale)
    {
      List<TriangulationPoint> triangulationPointList = new List<TriangulationPoint>();
      for (int index = 0; index < n; ++index)
        triangulationPointList.Add(new TriangulationPoint(scale * (0.5 - PointGenerator.RNG.NextDouble()), scale * (0.5 - PointGenerator.RNG.NextDouble())));
      return triangulationPointList;
    }

    public static List<TriangulationPoint> UniformGrid(int n, double scale)
    {
      double num1 = scale / (double) n;
      double num2 = 0.5 * scale;
      List<TriangulationPoint> triangulationPointList = new List<TriangulationPoint>();
      for (int index1 = 0; index1 < n + 1; ++index1)
      {
        double x = num2 - (double) index1 * num1;
        for (int index2 = 0; index2 < n + 1; ++index2)
          triangulationPointList.Add(new TriangulationPoint(x, num2 - (double) index2 * num1));
      }
      return triangulationPointList;
    }
  }
}
