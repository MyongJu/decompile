﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.Sets.PointSet
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Poly2Tri.Triangulation.Delaunay;
using System.Collections.Generic;

namespace Poly2Tri.Triangulation.Sets
{
  public class PointSet : Triangulatable
  {
    public PointSet(List<TriangulationPoint> points)
    {
      this.Points = (IList<TriangulationPoint>) new List<TriangulationPoint>((IEnumerable<TriangulationPoint>) points);
    }

    public IList<TriangulationPoint> Points { get; private set; }

    public IList<DelaunayTriangle> Triangles { get; private set; }

    public virtual TriangulationMode TriangulationMode
    {
      get
      {
        return TriangulationMode.Unconstrained;
      }
    }

    public void AddTriangle(DelaunayTriangle t)
    {
      this.Triangles.Add(t);
    }

    public void AddTriangles(IEnumerable<DelaunayTriangle> list)
    {
      foreach (DelaunayTriangle delaunayTriangle in list)
        this.Triangles.Add(delaunayTriangle);
    }

    public void ClearTriangles()
    {
      this.Triangles.Clear();
    }

    public virtual void PrepareTriangulation(TriangulationContext tcx)
    {
      if (this.Triangles == null)
        this.Triangles = (IList<DelaunayTriangle>) new List<DelaunayTriangle>(this.Points.Count);
      else
        this.Triangles.Clear();
      tcx.Points.AddRange((IEnumerable<TriangulationPoint>) this.Points);
    }
  }
}
