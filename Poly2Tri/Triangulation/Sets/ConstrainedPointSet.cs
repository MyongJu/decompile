﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.Sets.ConstrainedPointSet
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace Poly2Tri.Triangulation.Sets
{
  public class ConstrainedPointSet : PointSet
  {
    private List<TriangulationPoint> _constrainedPointList;

    public ConstrainedPointSet(List<TriangulationPoint> points, int[] index)
      : base(points)
    {
      this.EdgeIndex = index;
    }

    public ConstrainedPointSet(List<TriangulationPoint> points, IEnumerable<TriangulationPoint> constraints)
      : base(points)
    {
      this._constrainedPointList = new List<TriangulationPoint>();
      this._constrainedPointList.AddRange(constraints);
    }

    public int[] EdgeIndex { get; private set; }

    public override TriangulationMode TriangulationMode
    {
      get
      {
        return TriangulationMode.Constrained;
      }
    }

    public override void PrepareTriangulation(TriangulationContext tcx)
    {
      base.PrepareTriangulation(tcx);
      if (this._constrainedPointList != null)
      {
        List<TriangulationPoint>.Enumerator enumerator = this._constrainedPointList.GetEnumerator();
        while (enumerator.MoveNext())
        {
          TriangulationPoint current1 = enumerator.Current;
          enumerator.MoveNext();
          TriangulationPoint current2 = enumerator.Current;
          tcx.NewConstraint(current1, current2);
        }
      }
      else
      {
        int index = 0;
        while (index < this.EdgeIndex.Length)
        {
          tcx.NewConstraint(this.Points[this.EdgeIndex[index]], this.Points[this.EdgeIndex[index + 1]]);
          index += 2;
        }
      }
    }

    public bool isValid()
    {
      return true;
    }
  }
}
