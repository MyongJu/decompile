﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.TriangulationContext
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Poly2Tri.Triangulation.Delaunay;
using System.Collections.Generic;

namespace Poly2Tri.Triangulation
{
  public abstract class TriangulationContext
  {
    public readonly List<TriangulationPoint> Points = new List<TriangulationPoint>(200);
    public readonly List<DelaunayTriangle> Triangles = new List<DelaunayTriangle>();

    public TriangulationContext()
    {
      this.Terminated = false;
    }

    public TriangulationMode TriangulationMode { get; protected set; }

    public Triangulatable Triangulatable { get; private set; }

    public bool WaitUntilNotified { get; private set; }

    public bool Terminated { get; set; }

    public int StepCount { get; private set; }

    public virtual bool IsDebugEnabled { get; protected set; }

    public void Done()
    {
      ++this.StepCount;
    }

    public virtual void PrepareTriangulation(Triangulatable t)
    {
      this.Triangulatable = t;
      this.TriangulationMode = t.TriangulationMode;
      t.PrepareTriangulation(this);
    }

    public abstract TriangulationConstraint NewConstraint(TriangulationPoint a, TriangulationPoint b);

    public void Update(string message)
    {
    }

    public virtual void Clear()
    {
      this.Points.Clear();
      this.Terminated = false;
      this.StepCount = 0;
    }
  }
}
