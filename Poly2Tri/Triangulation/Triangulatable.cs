﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.Triangulatable
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Poly2Tri.Triangulation.Delaunay;
using System.Collections.Generic;

namespace Poly2Tri.Triangulation
{
  public interface Triangulatable
  {
    IList<TriangulationPoint> Points { get; }

    IList<DelaunayTriangle> Triangles { get; }

    TriangulationMode TriangulationMode { get; }

    void PrepareTriangulation(TriangulationContext tcx);

    void AddTriangle(DelaunayTriangle t);

    void AddTriangles(IEnumerable<DelaunayTriangle> list);

    void ClearTriangles();
  }
}
