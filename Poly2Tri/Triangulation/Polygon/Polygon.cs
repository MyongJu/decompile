﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.Polygon.Polygon
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Poly2Tri.Triangulation.Delaunay;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Poly2Tri.Triangulation.Polygon
{
  public class Polygon : Triangulatable
  {
    protected List<TriangulationPoint> _points = new List<TriangulationPoint>();
    protected List<Poly2Tri.Triangulation.Polygon.Polygon> _holes;
    protected PolygonPoint _last;
    protected List<TriangulationPoint> _steinerPoints;
    protected List<DelaunayTriangle> _triangles;

    public Polygon(IList<PolygonPoint> points)
    {
      if (points.Count < 3)
        throw new ArgumentException("List has fewer than 3 points", nameof (points));
      if (points[0].Equals((object) points[points.Count - 1]))
        points.RemoveAt(points.Count - 1);
      this._points.AddRange(points.Cast<TriangulationPoint>());
    }

    public Polygon(IEnumerable<PolygonPoint> points)
      : this((IList<PolygonPoint>) ((object) (points as IList<PolygonPoint>) ?? (object) points.ToArray<PolygonPoint>()))
    {
    }

    public Polygon()
    {
    }

    public IList<Poly2Tri.Triangulation.Polygon.Polygon> Holes
    {
      get
      {
        return (IList<Poly2Tri.Triangulation.Polygon.Polygon>) this._holes;
      }
    }

    public TriangulationMode TriangulationMode
    {
      get
      {
        return TriangulationMode.Polygon;
      }
    }

    public IList<TriangulationPoint> Points
    {
      get
      {
        return (IList<TriangulationPoint>) this._points;
      }
    }

    public IList<DelaunayTriangle> Triangles
    {
      get
      {
        return (IList<DelaunayTriangle>) this._triangles;
      }
    }

    public void AddTriangle(DelaunayTriangle t)
    {
      this._triangles.Add(t);
    }

    public void AddTriangles(IEnumerable<DelaunayTriangle> list)
    {
      this._triangles.AddRange(list);
    }

    public void ClearTriangles()
    {
      if (this._triangles == null)
        return;
      this._triangles.Clear();
    }

    public void PrepareTriangulation(TriangulationContext tcx)
    {
      if (this._triangles == null)
        this._triangles = new List<DelaunayTriangle>(this._points.Count);
      else
        this._triangles.Clear();
      for (int index = 0; index < this._points.Count - 1; ++index)
        tcx.NewConstraint(this._points[index], this._points[index + 1]);
      tcx.NewConstraint(this._points[0], this._points[this._points.Count - 1]);
      tcx.Points.AddRange((IEnumerable<TriangulationPoint>) this._points);
      if (this._holes != null)
      {
        using (List<Poly2Tri.Triangulation.Polygon.Polygon>.Enumerator enumerator = this._holes.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Poly2Tri.Triangulation.Polygon.Polygon current = enumerator.Current;
            for (int index = 0; index < current._points.Count - 1; ++index)
              tcx.NewConstraint(current._points[index], current._points[index + 1]);
            tcx.NewConstraint(current._points[0], current._points[current._points.Count - 1]);
            tcx.Points.AddRange((IEnumerable<TriangulationPoint>) current._points);
          }
        }
      }
      if (this._steinerPoints == null)
        return;
      tcx.Points.AddRange((IEnumerable<TriangulationPoint>) this._steinerPoints);
    }

    public void AddSteinerPoint(TriangulationPoint point)
    {
      if (this._steinerPoints == null)
        this._steinerPoints = new List<TriangulationPoint>();
      this._steinerPoints.Add(point);
    }

    public void AddSteinerPoints(List<TriangulationPoint> points)
    {
      if (this._steinerPoints == null)
        this._steinerPoints = new List<TriangulationPoint>();
      this._steinerPoints.AddRange((IEnumerable<TriangulationPoint>) points);
    }

    public void ClearSteinerPoints()
    {
      if (this._steinerPoints == null)
        return;
      this._steinerPoints.Clear();
    }

    public void AddHole(Poly2Tri.Triangulation.Polygon.Polygon poly)
    {
      if (this._holes == null)
        this._holes = new List<Poly2Tri.Triangulation.Polygon.Polygon>();
      this._holes.Add(poly);
    }

    public void InsertPointAfter(PolygonPoint point, PolygonPoint newPoint)
    {
      int num = this._points.IndexOf((TriangulationPoint) point);
      if (num == -1)
        throw new ArgumentException("Tried to insert a point into a Polygon after a point not belonging to the Polygon", nameof (point));
      newPoint.Next = point.Next;
      newPoint.Previous = point;
      point.Next.Previous = newPoint;
      point.Next = newPoint;
      this._points.Insert(num + 1, (TriangulationPoint) newPoint);
    }

    public void AddPoints(IEnumerable<PolygonPoint> list)
    {
      foreach (PolygonPoint polygonPoint in list)
      {
        polygonPoint.Previous = this._last;
        if (this._last != null)
        {
          polygonPoint.Next = this._last.Next;
          this._last.Next = polygonPoint;
        }
        this._last = polygonPoint;
        this._points.Add((TriangulationPoint) polygonPoint);
      }
      PolygonPoint point = (PolygonPoint) this._points[0];
      this._last.Next = point;
      point.Previous = this._last;
    }

    public void AddPoint(PolygonPoint p)
    {
      p.Previous = this._last;
      p.Next = this._last.Next;
      this._last.Next = p;
      this._points.Add((TriangulationPoint) p);
    }

    public void RemovePoint(PolygonPoint p)
    {
      PolygonPoint next = p.Next;
      PolygonPoint previous = p.Previous;
      previous.Next = next;
      next.Previous = previous;
      this._points.Remove((TriangulationPoint) p);
    }
  }
}
