﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.Polygon.PolygonSet
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace Poly2Tri.Triangulation.Polygon
{
  public class PolygonSet
  {
    protected List<Poly2Tri.Triangulation.Polygon.Polygon> _polygons = new List<Poly2Tri.Triangulation.Polygon.Polygon>();

    public PolygonSet()
    {
    }

    public PolygonSet(Poly2Tri.Triangulation.Polygon.Polygon poly)
    {
      this._polygons.Add(poly);
    }

    public IEnumerable<Poly2Tri.Triangulation.Polygon.Polygon> Polygons
    {
      get
      {
        return (IEnumerable<Poly2Tri.Triangulation.Polygon.Polygon>) this._polygons;
      }
    }

    public void Add(Poly2Tri.Triangulation.Polygon.Polygon p)
    {
      this._polygons.Add(p);
    }
  }
}
