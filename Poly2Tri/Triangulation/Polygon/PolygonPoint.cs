﻿// Decompiled with JetBrains decompiler
// Type: Poly2Tri.Triangulation.Polygon.PolygonPoint
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Poly2Tri.Triangulation.Polygon
{
  public class PolygonPoint : TriangulationPoint
  {
    public PolygonPoint(double x, double y)
      : base(x, y)
    {
    }

    public PolygonPoint Next { get; set; }

    public PolygonPoint Previous { get; set; }
  }
}
