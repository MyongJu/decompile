﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTowerPayload
{
  private UserMerlinTowerData userData = new UserMerlinTowerData();
  private MerlinKingdomData kingdomData = new MerlinKingdomData();
  public int RequestOpenMienIndex = -1;
  public const string MERLIN_TOWER_SHOP_ITEM = "item_magic_tower_coin_1";
  private static MerlinTowerPayload instance;
  private bool isActivityOn;
  private MerlinTowerState towerState;
  private BoughtBenefitData boughtBenefit;
  private Hashtable _battleResult;
  private Hashtable _pvpBattleResult;
  private int openLevel;
  private bool isTroopCapacityFull;
  private int _maxHistoryLevel;
  public bool Reset;

  public static MerlinTowerPayload Instance
  {
    get
    {
      if (MerlinTowerPayload.instance == null)
        MerlinTowerPayload.instance = new MerlinTowerPayload();
      return MerlinTowerPayload.instance;
    }
  }

  public bool IsActivityOn
  {
    get
    {
      return this.isActivityOn;
    }
  }

  public MerlinTowerState TowerState
  {
    get
    {
      this.towerState = PlayerData.inst.CityData.mStronghold.mLevel < this.OpenLevel ? MerlinTowerState.Unopened : MerlinTowerState.Opened;
      return this.towerState;
    }
  }

  public UserMerlinTowerData UserData
  {
    get
    {
      return this.userData;
    }
  }

  public BoughtBenefitData BoughtBenefit
  {
    get
    {
      return this.boughtBenefit;
    }
  }

  public MerlinKingdomData KingdomData
  {
    get
    {
      return this.kingdomData;
    }
  }

  public Hashtable BattleResult
  {
    get
    {
      return this._battleResult;
    }
  }

  public Hashtable PvpBattleResult
  {
    get
    {
      return this._pvpBattleResult;
    }
  }

  public int OpenLevel
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("magic_tower_open_level");
      if (data != null)
        this.openLevel = data.ValueInt;
      return this.openLevel;
    }
  }

  public bool IsTroopCapacityFull
  {
    get
    {
      return this.userData == null || this.userData.capacity == this.userData.maxCapacity;
    }
  }

  public int MaxHistoryLevel
  {
    get
    {
      return this._maxHistoryLevel;
    }
  }

  public bool CanShowTipInCurrentLevel(int level)
  {
    bool flag1 = false;
    bool flag2 = false;
    List<int> shopItems = this.UserData.shopItems;
    List<MagicTowerShopInfo> withLayer = ConfigManager.inst.DB_MagicTowerShop.GetWithLayer(level);
    for (int index1 = 0; index1 < withLayer.Count; ++index1)
    {
      if (shopItems != null)
      {
        for (int index2 = 0; index2 < shopItems.Count; ++index2)
        {
          if (withLayer[index1].internalId == shopItems[index2])
          {
            int level1 = ConfigManager.inst.DB_MagicTowerMain.GetData(withLayer[index1].MagicTowerMainId).Level;
            if (level == level1)
            {
              flag1 = true;
              break;
            }
          }
        }
      }
      if (withLayer[index1].type != 2 && this.userData.magic >= (long) withLayer[index1].price)
        flag2 = true;
    }
    return !flag1 && flag2;
  }

  private void DecodeUserInfo(object data)
  {
    if (data == null)
      return;
    Hashtable hashtable = data as Hashtable;
    if (hashtable.ContainsKey((object) "opened") && hashtable[(object) "opened"] != null)
      this.isActivityOn = hashtable[(object) "opened"].ToString().Trim().ToLower() == "true";
    this.userData = new UserMerlinTowerData();
    if (!hashtable.ContainsKey((object) "userData") || hashtable[(object) "userData"] == null)
      return;
    UserMerlinTowerData userMerlinTowerData = JsonReader.Deserialize<UserMerlinTowerData>(Utils.Object2Json(hashtable[(object) "userData"]));
    if (userMerlinTowerData == null)
      return;
    this.userData = userMerlinTowerData;
  }

  private void DecodeBoughtBenefitData(object data)
  {
    if (data == null)
      return;
    this.boughtBenefit = JsonReader.Deserialize<BoughtBenefitData>(Utils.Object2Json(data));
  }

  public void SendGetUserInfoRequest(System.Action<bool, object> callback, bool blockScreen = true)
  {
    MessageHub.inst.GetPortByAction("magicTower:getInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
      {
        Hashtable inData = data as Hashtable;
        if (inData != null)
          DatabaseTools.UpdateData(inData, "top_level", ref this._maxHistoryLevel);
        this.DecodeUserInfo(data);
      }
      if (!ret || callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }

  public void Initialize()
  {
    MessageHub.inst.GetPortByAction("magic_tower_combat_failure").AddEvent(new System.Action<object>(this.OnAttacked));
    MessageHub.inst.GetPortByAction("magic_tower_gather_done").AddEvent(new System.Action<object>(this.OnJobComplete));
    MessageHub.inst.GetPortByAction("magic_tower_over").AddEvent(new System.Action<object>(this.OnMerlinTowerOver));
  }

  public void Dispose()
  {
    MessageHub.inst.GetPortByAction("magic_tower_combat_failure").RemoveEvent(new System.Action<object>(this.OnAttacked));
    MessageHub.inst.GetPortByAction("magic_tower_gather_done").RemoveEvent(new System.Action<object>(this.OnJobComplete));
    MessageHub.inst.GetPortByAction("magic_tower_over").RemoveEvent(new System.Action<object>(this.OnMerlinTowerOver));
    MerlinTowerPayload.instance = (MerlinTowerPayload) null;
  }

  protected void OnJobComplete(object data)
  {
    Hashtable hashData = data as Hashtable;
    if (hashData == null)
      return;
    this.OnJobFinished(hashData);
  }

  protected void OnMerlinTowerOver(object data)
  {
    this.userData = new UserMerlinTowerData();
    this.kingdomData = new MerlinKingdomData();
  }

  protected void OnAttacked(object data)
  {
    Hashtable hashtable = data as Hashtable;
    if (hashtable == null)
      return;
    if (hashtable.ContainsKey((object) "user_data"))
    {
      UserMerlinTowerData userMerlinTowerData = JsonReader.Deserialize<UserMerlinTowerData>(Utils.Object2Json(hashtable[(object) "user_data"]));
      if (userMerlinTowerData != null)
        this.userData = userMerlinTowerData;
    }
    MerlinKingdomData merlinKingdomData = JsonReader.Deserialize<MerlinKingdomData>(Utils.Object2Json((object) hashtable));
    if (merlinKingdomData == null || merlinKingdomData.page != this.kingdomData.page)
      return;
    this.kingdomData = merlinKingdomData;
  }

  public void SendBuyBenefitRequest(System.Action<bool, object> callback, int shopId, bool blockScreen = true)
  {
    Hashtable postData = new Hashtable();
    postData[(object) nameof (shopId)] = (object) shopId;
    MessageHub.inst.GetPortByAction("magicTower:buyItem").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.DecodeBoughtBenefitData(data);
      if (!ret || callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }

  public void SendChallengeTowerRequest(int towerInternalId)
  {
    MessageHub.inst.GetPortByAction("magicTower:challengeTower").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable hashtable = data as Hashtable;
      if (hashtable == null)
        return;
      if (!hashtable.ContainsKey((object) "win") || !hashtable.ContainsKey((object) "detail"))
        ;
      if (hashtable.ContainsKey((object) "userData"))
      {
        Hashtable inData = hashtable[(object) "userData"] as Hashtable;
        if (inData != null)
        {
          DatabaseTools.UpdateData(inData, "towerId", ref this.userData.towerId);
          DatabaseTools.UpdateData(inData, "capacity", ref this.userData.capacity);
          DatabaseTools.UpdateData(inData, "magic", ref this.userData.magic);
          DatabaseTools.UpdateData(inData, "cleared", ref this.userData.cleared);
        }
      }
      this._battleResult = hashtable;
    }), true);
  }

  public void SendClaimbTowerRequest(int towerInternalId)
  {
    RequestManager.inst.SendRequest("magicTower:climbTower", new Hashtable()
    {
      {
        (object) "towerId",
        (object) towerInternalId
      }
    }, (System.Action<bool, object>) ((result, data) =>
    {
      if (!result)
        return;
      Hashtable hashtable = data as Hashtable;
      if (hashtable == null)
        return;
      if (hashtable.ContainsKey((object) "userData"))
      {
        UserMerlinTowerData userMerlinTowerData = JsonReader.Deserialize<UserMerlinTowerData>(Utils.Object2Json(hashtable[(object) "userData"]));
        if (userMerlinTowerData != null)
          this.userData = userMerlinTowerData;
      }
      if (this.userData.CurrentTowerMainInfo == null)
        return;
      this._maxHistoryLevel = Mathf.Max(this._maxHistoryLevel, this.userData.CurrentTowerMainInfo.Level);
    }), 1 != 0);
  }

  public void SendEnterMineFieldRequest(int pageIndex, int openMineIndex = -1)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "page"] = (object) pageIndex;
    RequestManager.inst.SendRequest("magicTower:enterMineField", postData, (System.Action<bool, object>) ((result, data) =>
    {
      if (!result)
        return;
      Hashtable hashtable = data as Hashtable;
      if (hashtable != null)
      {
        MerlinKingdomData merlinKingdomData = JsonReader.Deserialize<MerlinKingdomData>(Utils.Object2Json((object) hashtable));
        this.kingdomData = merlinKingdomData == null ? new MerlinKingdomData() : merlinKingdomData;
      }
      this.RequestOpenMienIndex = openMineIndex;
    }), true);
  }

  public void SendGatherRequest(Hashtable troops, bool withDragon, int pageIndex, int index)
  {
    Hashtable postData = new Hashtable();
    postData[(object) nameof (troops)] = (object) troops;
    postData[(object) "with_dragon"] = (object) withDragon;
    postData[(object) "page"] = (object) pageIndex;
    postData[(object) nameof (index)] = (object) index;
    RequestManager.inst.SendRequest("magicTower:gather", postData, (System.Action<bool, object>) ((result, data) =>
    {
      if (!result)
        return;
      Hashtable hashtable = data as Hashtable;
      if (hashtable == null)
        return;
      MerlinKingdomData merlinKingdomData = JsonReader.Deserialize<MerlinKingdomData>(Utils.Object2Json((object) hashtable));
      if (merlinKingdomData != null)
        this.kingdomData = merlinKingdomData;
      if (!hashtable.ContainsKey((object) "user_data"))
        return;
      UserMerlinTowerData userMerlinTowerData = JsonReader.Deserialize<UserMerlinTowerData>(Utils.Object2Json(hashtable[(object) "user_data"]));
      if (userMerlinTowerData == null)
        return;
      this.userData = userMerlinTowerData;
    }), true);
  }

  public void SendPvPBattleRequest(Hashtable troops, bool withDragon, int pageIndex, int index, long targetUid)
  {
    Hashtable postData = new Hashtable();
    postData[(object) nameof (troops)] = (object) troops;
    postData[(object) "with_dragon"] = (object) withDragon;
    postData[(object) "page"] = (object) pageIndex;
    postData[(object) nameof (index)] = (object) index;
    postData[(object) "target_uid"] = (object) targetUid;
    RequestManager.inst.SendRequest("magicTower:pvpBattle", postData, (System.Action<bool, object>) ((result, data) =>
    {
      if (result)
      {
        Hashtable hashtable = data as Hashtable;
        if (hashtable != null)
        {
          if (hashtable.ContainsKey((object) "win") && hashtable[(object) "win"].ToString().ToLower() == "true")
          {
            MerlinKingdomData merlinKingdomData = JsonReader.Deserialize<MerlinKingdomData>(Utils.Object2Json((object) hashtable));
            if (merlinKingdomData != null)
              this.kingdomData = merlinKingdomData;
          }
          if (hashtable.ContainsKey((object) "user_data"))
          {
            UserMerlinTowerData userMerlinTowerData = JsonReader.Deserialize<UserMerlinTowerData>(Utils.Object2Json(hashtable[(object) "user_data"]));
            if (userMerlinTowerData != null)
              this.userData = userMerlinTowerData;
          }
        }
        this._pvpBattleResult = hashtable;
      }
      else
        UIManager.inst.toast.Show(ScriptLocalization.Get("toast_mine_already_defeated", true), (System.Action) null, 4f, true);
    }), true);
  }

  public void SendFindFreeMineRequest(Hashtable troops, bool withDragon)
  {
    Hashtable postData = new Hashtable();
    postData[(object) nameof (troops)] = (object) troops;
    postData[(object) "with_dragon"] = (object) withDragon;
    RequestManager.inst.SendRequest("magicTower:findFreeMine", postData, (System.Action<bool, object>) ((result, data) =>
    {
      if (result)
      {
        Hashtable hashtable = data as Hashtable;
        if (hashtable != null)
        {
          MerlinKingdomData merlinKingdomData = JsonReader.Deserialize<MerlinKingdomData>(Utils.Object2Json((object) hashtable));
          this.kingdomData = merlinKingdomData == null ? new MerlinKingdomData() : merlinKingdomData;
        }
        if (!hashtable.ContainsKey((object) "user_data"))
          return;
        UserMerlinTowerData userMerlinTowerData = JsonReader.Deserialize<UserMerlinTowerData>(Utils.Object2Json(hashtable[(object) "user_data"]));
        if (userMerlinTowerData == null)
          return;
        this.userData = userMerlinTowerData;
      }
      else
        this.userData.lastFindTime = NetServerTime.inst.ServerTimestamp;
    }), true);
  }

  public void SendResetRequest()
  {
    RequestManager.inst.SendRequest("magicTower:reset", (Hashtable) null, (System.Action<bool, object>) ((result, data) =>
    {
      if (!result)
        return;
      this.userData = new UserMerlinTowerData();
      this.Reset = true;
    }), true);
  }

  public void OnJobFinished(Hashtable hashData)
  {
    if (hashData.ContainsKey((object) "user_data"))
    {
      UserMerlinTowerData userMerlinTowerData = JsonReader.Deserialize<UserMerlinTowerData>(Utils.Object2Json(hashData[(object) "user_data"]));
      if (userMerlinTowerData != null)
        this.userData = userMerlinTowerData;
    }
    if (!hashData.ContainsKey((object) "page") || !(hashData[(object) "page"].ToString() == this.kingdomData.page.ToString()))
      return;
    MerlinKingdomData merlinKingdomData = JsonReader.Deserialize<MerlinKingdomData>(Utils.Object2Json((object) hashData));
    if (merlinKingdomData == null)
      return;
    this.kingdomData = merlinKingdomData;
  }

  public void Revenge()
  {
    if (this.userData.mineJobId > 0L)
      UIManager.inst.toast.Show(Utils.XLAT("toast_mine_occupy_only_one"), (System.Action) null, 4f, false);
    else if (this.UserData.canBattle < NetServerTime.inst.ServerTimestamp)
      UIManager.inst.OpenDlg("MerlinTower/MerlinTowerOreMarchAllocDlg", (UI.Dialog.DialogParameter) new MerlinTowerOreMarchAllocDlg.Parameter()
      {
        marchType = MarchAllocDlg.MarchType.gatherAttack,
        desTroopCount = (int) MerlinTowerPayload.Instance.UserData.capacity,
        isRally = false,
        confirmCallback = new System.Action<Hashtable, bool>(this.OnComfirnHandler)
      }, true, true, true);
    else
      this.ClearCDTime();
  }

  private void ClearCDTime()
  {
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_donate_cd_second_per_gold");
    int price = 1;
    if (data != null)
      price = data.ValueInt;
    int time = this.UserData.canBattle - NetServerTime.inst.ServerTimestamp;
    int cost = ItemBag.CalculateCost(time, price);
    UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
    {
      confirmButtonClickEvent = new System.Action(this.ConFirmCallBack),
      closeButtonCallbackEvent = (System.Action) null,
      lefttime = time,
      goldNum = cost,
      unitprice = price,
      description = ScriptLocalization.Get("tower_troops_resting_description", true)
    });
  }

  private void ConFirmCallBack()
  {
    RequestManager.inst.SendRequest("magicTower:clearCdTime", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, (System.Action<bool, object>) ((arg1, result) =>
    {
      if (arg1)
        ;
    }), true);
  }

  public void StrikeBack(int pageIndex, int oreIndex)
  {
    if (this.userData.mineJobId > 0L)
      UIManager.inst.toast.Show(Utils.XLAT("toast_mine_occupy_only_one"), (System.Action) null, 4f, false);
    else
      this.SendEnterMineFieldRequest(pageIndex, oreIndex);
  }

  private void OnComfirnHandler(Hashtable troops, bool withDragon)
  {
    RequestManager.inst.SendRequest("magicTower:revenge", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) nameof (troops),
        (object) troops
      },
      {
        (object) "with_dragon",
        (object) withDragon
      }
    }, new System.Action<bool, object>(this.OnRevengeHandler), true);
  }

  private void OnRevengeHandler(bool success, object payload)
  {
    if (!success)
      return;
    Hashtable hashtable = payload as Hashtable;
    if (hashtable.ContainsKey((object) "win") && hashtable[(object) "win"].ToString().ToLower() == "false")
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("magic_tower_battle_cd");
      if (data != null)
        UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("tower_revenge_defeat_subtitle", new Dictionary<string, string>()
        {
          {
            "0",
            Utils.FormatTime(data.ValueInt, false, false, true)
          }
        }, true), (System.Action) null, 4f, false);
    }
    this._pvpBattleResult = hashtable;
    if (!hashtable.ContainsKey((object) "user_data"))
      return;
    UserMerlinTowerData userMerlinTowerData = JsonReader.Deserialize<UserMerlinTowerData>(Utils.Object2Json(hashtable[(object) "user_data"]));
    if (userMerlinTowerData == null)
      return;
    this.userData = userMerlinTowerData;
  }
}
