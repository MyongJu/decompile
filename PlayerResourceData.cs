﻿// Decompiled with JetBrains decompiler
// Type: PlayerResourceData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class PlayerResourceData
{
  public int mStronghold;
  public long mSilver;
  public long mWood;
  public long mFood;
  public long mOre;
  public int mIngredient;
  public long mSilverLimit;
  public long mWoodLimit;
  public long mFoodLimit;
  public long mOreLimit;
  public int mExperience;
  public int mLevel;
  public long mGold;
}
