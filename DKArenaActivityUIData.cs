﻿// Decompiled with JetBrains decompiler
// Type: DKArenaActivityUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;

public class DKArenaActivityUIData : ActivityBaseUIData
{
  protected DKArenaActivityData ArenaData
  {
    get
    {
      return this.Data as DKArenaActivityData;
    }
  }

  public override string EventKey
  {
    get
    {
      return "dk_arena_activity";
    }
  }

  public override ActivityBaseUIData.ActivityType Type
  {
    get
    {
      return ActivityBaseUIData.ActivityType.DKAreana;
    }
  }

  public override int GetRemainTime()
  {
    if (this.IsStart())
      return this.EndTime - NetServerTime.inst.ServerTimestamp;
    return (int) this.ArenaData.NextTime - NetServerTime.inst.ServerTimestamp;
  }

  public override int StartTime
  {
    get
    {
      return this.ArenaData.StartTime;
    }
  }

  public override int EndTime
  {
    get
    {
      return this.ArenaData.EndTime;
    }
  }

  public override void DisplayActivityDetail()
  {
    if (PlayerData.inst.dragonKnightData == null)
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_dragon_knight_not_summoned_yet", true), (System.Action) null, 4f, false);
    else
      UIManager.inst.OpenDlg("DKArena/DKArenaMainDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public override IconData GetNormalIconData()
  {
    IconData iconData = new IconData();
    iconData.image = "Texture/Events/icon_dragonsoularean";
    iconData.contents = new string[2];
    iconData.contents[0] = Utils.XLAT("event_dragon_knight_arena_title");
    iconData.contents[1] = Utils.XLAT("event_dragon_knight_arena_one_line_description");
    iconData.Data = (object) this;
    return iconData;
  }

  public override IconData GetADIconData()
  {
    IconData iconData = new IconData();
    iconData.image = "Texture/Events/dragonsoul_Arean_image_big";
    iconData.contents = new string[2];
    iconData.contents[0] = Utils.XLAT("event_dragon_knight_arena_title");
    iconData.contents[1] = Utils.XLAT("event_dragon_knight_arena_one_line_description");
    return iconData;
  }
}
