﻿// Decompiled with JetBrains decompiler
// Type: MailSelectionPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MailSelectionPopup : Popup
{
  private int m_SelectedIndex = -1;
  private List<MailSelectionRenderer> m_ItemList = new List<MailSelectionRenderer>();
  private List<string> m_TargetNames = new List<string>();
  public GameObject m_ItemPrefab;
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  public GameObject m_NoContacts;
  public UIToggle[] m_Toggles;
  public UILabel m_Tip;
  private MailSelectionPopup.Parameter m_Parameter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as MailSelectionPopup.Parameter;
    if (this.m_Parameter != null && this.m_Parameter.targets != null)
      this.m_TargetNames = this.m_Parameter.targets;
    this.m_Toggles[0].Set(true);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.ClearData();
  }

  public void OnToggleChanged()
  {
    int selectedIndex = this.GetSelectedIndex();
    if (this.m_SelectedIndex == selectedIndex || selectedIndex == -1)
      return;
    this.m_SelectedIndex = selectedIndex;
    switch (this.m_SelectedIndex)
    {
      case 0:
        this.GotoContacts();
        break;
      case 1:
        this.GotoMembers();
        break;
    }
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnConfirm()
  {
    this.OnClosePressed();
    if (this.m_Parameter == null || this.m_Parameter.callback == null)
      return;
    this.m_Parameter.callback(this.m_TargetNames);
  }

  private int GetSelectedIndex()
  {
    for (int index = 0; index < this.m_Toggles.Length; ++index)
    {
      if (this.m_Toggles[index].value)
        return index;
    }
    return -1;
  }

  private void GotoContacts()
  {
    this.ClearData();
    this.m_Tip.text = Utils.XLAT("placeholder_no_contacts_description");
    MessageHub.inst.GetPortByAction("Player:loadContacts").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.UpdateContacts();
    }), true);
  }

  private void GotoMembers()
  {
    this.ClearData();
    this.m_Tip.text = Utils.XLAT("placeholder_no_alliance_description");
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.UpdateMembers();
    }), true);
  }

  private void UpdateContacts()
  {
    List<long> friends = DBManager.inst.DB_Contacts.friends;
    for (int index = 0; index < friends.Count; ++index)
    {
      UserData userData = DBManager.inst.DB_User.Get(friends[index]);
      GameObject gameObject = Utils.DuplicateGOB(this.m_ItemPrefab, this.m_Grid.transform);
      gameObject.SetActive(true);
      MailSelectionRenderer component = gameObject.GetComponent<MailSelectionRenderer>();
      component.SetData(userData.PortraitIconPath, userData.Icon, userData.LordTitle, userData.userName_Kingdom_Alliance_Name, userData.userName, this.m_TargetNames);
      this.m_ItemList.Add(component);
    }
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
    this.m_NoContacts.SetActive(this.m_ItemList.Count == 0);
  }

  private void UpdateMembers()
  {
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
    if (allianceData != null)
    {
      Dictionary<long, AllianceMemberData>.Enumerator enumerator = allianceData.members.datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        AllianceMemberData allianceMemberData = enumerator.Current.Value;
        if (allianceMemberData.status == 0)
        {
          UserData userData = DBManager.inst.DB_User.Get(allianceMemberData.uid);
          if (userData.uid != PlayerData.inst.uid)
          {
            string rank = AllianceTitle.TitleToRank(allianceMemberData.title);
            GameObject gameObject = Utils.DuplicateGOB(this.m_ItemPrefab, this.m_Grid.transform);
            gameObject.SetActive(true);
            MailSelectionRenderer component = gameObject.GetComponent<MailSelectionRenderer>();
            component.SetData(userData.PortraitIconPath, userData.Icon, userData.LordTitle, string.Format("{0} - {1}", (object) rank, (object) userData.userName), userData.userName, this.m_TargetNames);
            this.m_ItemList.Add(component);
          }
        }
      }
    }
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
    this.m_NoContacts.SetActive(this.m_ItemList.Count == 0);
  }

  private void ClearData()
  {
    using (List<MailSelectionRenderer>.Enumerator enumerator = this.m_ItemList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MailSelectionRenderer current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ItemList.Clear();
  }

  public class Parameter : Popup.PopupParameter
  {
    public System.Action<List<string>> callback;
    public List<string> targets;
  }
}
