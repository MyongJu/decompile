﻿// Decompiled with JetBrains decompiler
// Type: BarracksTimeBoostPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BarracksTimeBoostPopup : MonoBehaviour
{
  public UILabel mNormalTimeValue;
  public UILabel mBoostTimeValue;
  public UILabel mDecreasedTimeValue;
  public UILabel mTrainingSpeedValue;
  public UILabel mResearchBonusTS1;
  public UILabel mResearchBonusTS2;
  public UILabel mHeroSkillBonusTS1;
  public UILabel mHeroSkillBonusTS2;
  public UILabel mEquipmentBonusTS1;
  public UILabel mEquipmentBonusTS2;

  public void SetDetails(double normalTime, double boostTime, int trainingSpeed, int researchTS1, int researchTS2, int heroSkillTS1, int heroSkillTS2, int equipmentTS1, int equipmentTS2)
  {
    this.mNormalTimeValue.text = Utils.ConvertSecsToString(normalTime);
    this.mBoostTimeValue.text = Utils.ConvertSecsToString(boostTime);
    this.mDecreasedTimeValue.text = "Resulting Time decreased: " + ((int) (boostTime / normalTime * 100.0)).ToString() + "%";
    this.mTrainingSpeedValue.text = "Total Training Speed: " + trainingSpeed.ToString() + "%";
    this.mResearchBonusTS1.text = "Training Speed I: " + researchTS1.ToString() + "%";
    this.mResearchBonusTS2.text = "Training Speed II: " + researchTS2.ToString() + "%";
    this.mHeroSkillBonusTS1.text = "Troop Training I: " + heroSkillTS1.ToString() + "%";
    this.mHeroSkillBonusTS2.text = "Troop Training II: " + heroSkillTS2.ToString() + "%";
    this.mEquipmentBonusTS1.text = "Angle Grace: " + equipmentTS1.ToString() + "%";
    this.mEquipmentBonusTS2.text = "Heaven Lance: " + equipmentTS2.ToString() + "%";
  }

  public void OnCloseBtnPressed()
  {
    this.gameObject.SetActive(false);
  }
}
