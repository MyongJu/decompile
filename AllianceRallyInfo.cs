﻿// Decompiled with JetBrains decompiler
// Type: AllianceRallyInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceRallyInfo : MonoBehaviour
{
  public AllianceWar_TargetSlot slot;
  public UILabel leaderLabel;
  public UILabel leaderName;
  public UILabel numberLabel;
  public UILabel number;
  public UILabel troopsLabel;
  public UILabel troopsCount;
  public UIButton reinforceBt;
  public UIButton disband;
  public UIButton callbackBt;
  private long rally_id;
  public UIProgressBar progressBar;
  public UILabel progresslabel;
  public UISprite attakBg;
  public UISprite defenseBg;

  public void SetRally(long rally_id)
  {
  }

  private RallyInfoData Data { get; set; }

  public void SetData(RallyInfoData data)
  {
    this.Data = data;
    this.RefreshFromData();
    this.AddEventHandler();
  }

  private bool IsDestroy { get; set; }

  private void OnDestroy()
  {
    this.IsDestroy = true;
  }

  private void RefreshFromData()
  {
    string empty = string.Empty;
    List<MarchData> marchDataList = new List<MarchData>();
    NGUITools.SetActive(this.reinforceBt.gameObject, false);
    NGUITools.SetActive(this.disband.gameObject, false);
    NGUITools.SetActive(this.callbackBt.gameObject, false);
    if (this.Data.Type == RallyInfoData.RallyDataType.Knight)
    {
      KnightRallyInfoData data = this.Data as KnightRallyInfoData;
      if (data != null)
      {
        if (data.BuildData != null)
        {
          switch (data.BuildData.Type)
          {
            case RallyBuildingData.DataType.Fortress:
              this.slot.SetFortress(data.BuildData.ID);
              break;
            case RallyBuildingData.DataType.Temple:
              this.slot.TempSetTemple(data.BuildData.Location);
              break;
          }
        }
        else if (data.TargetCityID > 0L)
          this.slot.SetCity(data.TargetCityID, 0.0f);
        else
          this.slot.SetEmpty();
      }
      else
        this.slot.SetEmpty();
    }
    else
    {
      RallyData rallyData = DBManager.inst.DB_Rally.Get(this.Data.DataID);
      float targetOffset = 0.0f;
      if (rallyData != null)
      {
        if (rallyData.bossId != 0)
        {
          if (rallyData.type != RallyData.RallyType.TYPE_RALLY_RAB)
            targetOffset = DBManager.inst.DB_WorldBossDB.Get(rallyData.bossId) == null ? 65f : -75f;
        }
        else if (WonderUtils.IsWonder(rallyData.location))
          targetOffset = 65f;
        else if (WonderUtils.IsWonderTower(rallyData.location))
          targetOffset = -70f;
        else if (DBManager.inst.DB_City.Get(rallyData.targetCityId) != null)
          targetOffset = 20f;
      }
      this.slot.Setup(this.Data.DataID, targetOffset, false);
    }
    this.slot.SetLayer("NGUI");
    NGUITools.SetActive(this.attakBg.gameObject, false);
    NGUITools.SetActive(this.defenseBg.gameObject, false);
    NGUITools.SetActive(this.disband.gameObject, false);
    NGUITools.SetActive(this.reinforceBt.gameObject, false);
    NGUITools.SetActive(this.callbackBt.gameObject, false);
    marchDataList = this.Data.MarchList;
    this.number.text = this.Data.JoinNum;
    this.troopsCount.text = this.Data.TroopsInfo;
    this.leaderName.text = this.Data.LeaderName;
    if (this.Data.IsDefense)
    {
      NGUITools.SetActive(this.defenseBg.gameObject, true);
      NGUITools.SetActive(this.reinforceBt.gameObject, !this.Data.IsJoined);
    }
    else
    {
      NGUITools.SetActive(this.attakBg.gameObject, true);
      if (!this.Data.IsDefense && this.Data.IsMyRally)
      {
        NGUITools.SetActive(this.disband.gameObject, !this.Data.IsCallBack);
        NGUITools.SetActive(this.callbackBt.gameObject, this.Data.IsCallBack);
      }
    }
    if (this.Data.IsDefense)
    {
      this.leaderLabel.text = ScriptLocalization.Get("war_rally_defense_leader_name", true);
      this.numberLabel.text = ScriptLocalization.Get("war_rally_defense_members_name", true);
      this.troopsLabel.text = ScriptLocalization.Get("war_rally_troops_name", true);
    }
    else
    {
      this.leaderLabel.text = ScriptLocalization.Get("war_rally_war_leader_name", true);
      this.numberLabel.text = ScriptLocalization.Get("war_rally_war_members_name", true);
      this.troopsLabel.text = ScriptLocalization.Get("war_rally_troops_name", true);
    }
    this.RefreshProgresssFromData();
  }

  private void RefreshProgresssFromData()
  {
    this.progressBar.value = this.Data.ProgressValue;
    this.progresslabel.text = this.Data.ProgressContent;
    if (this.Data.IsDefense || !this.Data.IsMyRally)
      return;
    NGUITools.SetActive(this.disband.gameObject, !this.Data.IsCallBack);
    NGUITools.SetActive(this.callbackBt.gameObject, this.Data.IsCallBack);
  }

  public void OnCallBackRally()
  {
    GameEngine.Instance.marchSystem.RecallRally(this.Data.DataID, new System.Action(this.OnCallBackRallyHandler));
  }

  private void OnCallBackRallyHandler()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void Clear()
  {
    this.RemoveEventHandler();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
    DBManager.inst.DB_Rally.onRallyDataChanged += new System.Action<long>(this.RallyDataChanged);
    DBManager.inst.DB_March.onDataChanged += new System.Action<long>(this.OnMarchDataChanged);
  }

  private void RallyDataChanged(long rallyId)
  {
    if (rallyId != this.Data.DataID)
      return;
    this.Data.Reset();
    this.RefreshFromData();
  }

  private void OnMarchDataChanged(long marchId)
  {
    if (this.IsDestroy)
    {
      this.RemoveEventHandler();
    }
    else
    {
      this.Data.Reset();
      this.RefreshFromData();
    }
  }

  private void Process(int time)
  {
    if (this.IsDestroy)
    {
      this.RemoveEventHandler();
    }
    else
    {
      this.Data.Refresh();
      this.RefreshProgresssFromData();
    }
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
    DBManager.inst.DB_Rally.onRallyDataChanged -= new System.Action<long>(this.RallyDataChanged);
    DBManager.inst.DB_March.onDataChanged -= new System.Action<long>(this.OnMarchDataChanged);
  }

  public void OnCancelRallyClick()
  {
    UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
    {
      title = ScriptLocalization.Get("id_uppercase_confirm", true),
      description = ScriptLocalization.Get("war_rally_disband_confirmation", true),
      leftButtonClickEvent = (System.Action) null,
      rightButtonClickEvent = (System.Action) (() =>
      {
        GameEngine.Instance.marchSystem.CancelRally(this.Data.DataID);
        UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
      })
    });
  }

  public void OnReinforceHandler()
  {
    UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
    {
      marchType = this.Data.ReinforceMarchType,
      location = this.Data.ReinforceLocation,
      targetUid = this.Data.TargetID,
      targetCityId = this.Data.TargetCityID,
      desTroopCount = this.Data.DesTroopCount
    }, 1 != 0, 1 != 0, 1 != 0);
  }
}
