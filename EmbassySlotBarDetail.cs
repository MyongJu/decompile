﻿// Decompiled with JetBrains decompiler
// Type: EmbassySlotBarDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UnityEngine;

public class EmbassySlotBarDetail : MonoBehaviour
{
  [SerializeField]
  private EmbassySlotBarDetail.Panel panel;

  public void Setup(string troopClass, int count)
  {
    Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(troopClass);
    if (data != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.troopIcon, data.Troop_ICON, (System.Action<bool>) null, true, false, string.Empty);
      this.panel.troopName.text = ScriptLocalization.Get(data.Troop_Name_LOC_ID, true);
      this.panel.troopLevel.text = Utils.GetRomaNumeralsBelowTen(data.Troop_Tier);
    }
    this.panel.troopCount.text = count.ToString();
  }

  private string _GetTroopSpriteName(string troopClass)
  {
    string key = troopClass;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (EmbassySlotBarDetail.\u003C\u003Ef__switch\u0024mapA1 == null)
      {
        // ISSUE: reference to a compiler-generated field
        EmbassySlotBarDetail.\u003C\u003Ef__switch\u0024mapA1 = new Dictionary<string, int>(5)
        {
          {
            "infantry",
            0
          },
          {
            "ranged",
            1
          },
          {
            "cavalry",
            2
          },
          {
            "artillery_close",
            3
          },
          {
            "artillery_distance",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (EmbassySlotBarDetail.\u003C\u003Ef__switch\u0024mapA1.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return "pic_infantry";
          case 1:
            return "pic_ranged";
          case 2:
            return "pic_cavalry";
          case 3:
            return "pic_artillery_close";
          case 4:
            return "pic_artillery_distant";
        }
      }
    }
    return string.Empty;
  }

  private string _GetTroopName(string troopClass)
  {
    return troopClass;
  }

  [Serializable]
  protected class Panel
  {
    public UITexture troopIcon;
    public UILabel troopName;
    public UILabel troopCount;
    public UILabel troopLevel;
  }
}
