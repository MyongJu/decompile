﻿// Decompiled with JetBrains decompiler
// Type: GetMoreDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class GetMoreDialog : UI.Dialog
{
  private GameObjectPool m_ItemPool = new GameObjectPool();
  private Dictionary<int, List<ShopItemUI>> m_ItemDict = new Dictionary<int, List<ShopItemUI>>();
  [SerializeField]
  private UILabel m_Gold;
  [SerializeField]
  private UIScrollView m_ScrollView;
  [SerializeField]
  private UIGrid m_Grid;
  [SerializeField]
  private GameObject m_ItemRoot;
  [SerializeField]
  private GameObject m_Renderer;

  public virtual bool CanBuyAndUse()
  {
    return true;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
  }

  public void OnAddGoldClicked()
  {
    Utils.PopUpCommingSoon();
  }

  public void OnCloseClicked()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  protected void UpdateData(GetMoreDialog.CompareMethod compare, object extra)
  {
    this.ClearData();
    DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid);
    using (List<ShopStaticInfo>.Enumerator enumerator = ItemBag.Instance.GetCurrentSaleShopItems("shop", string.Empty).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShopStaticInfo current = enumerator.Current;
        if (compare(current, extra))
        {
          GameObject gameObject = this.m_ItemPool.AddChild(this.m_Grid.gameObject);
          gameObject.SetActive(true);
          ShopItemUI component = gameObject.GetComponent<ShopItemUI>();
          component.SetShopStaticInfo(current);
          if (this.CanBuyAndUse())
          {
            int itemCount = ItemBag.Instance.GetItemCount(current.Item_InternalId);
            component.SwitchLocation(itemCount == 0);
          }
          else
            component.SwitchLocation(true);
          component.onItemClicked = new System.Action<ShopItemUI>(this.OnItemClicked);
          List<ShopItemUI> shopItemUiList1 = (List<ShopItemUI>) null;
          if (this.m_ItemDict.TryGetValue(current.Item_InternalId, out shopItemUiList1))
            shopItemUiList1.Add(component);
          if (shopItemUiList1 == null)
          {
            List<ShopItemUI> shopItemUiList2 = new List<ShopItemUI>();
            this.m_ItemDict.Add(current.Item_InternalId, shopItemUiList2);
            shopItemUiList2.Add(component);
          }
        }
      }
    }
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
    this.m_ScrollView.gameObject.SetActive(false);
    this.m_ScrollView.gameObject.SetActive(true);
  }

  private void ClearData()
  {
    using (Dictionary<int, List<ShopItemUI>>.ValueCollection.Enumerator enumerator1 = this.m_ItemDict.Values.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (List<ShopItemUI>.Enumerator enumerator2 = enumerator1.Current.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            ShopItemUI current = enumerator2.Current;
            current.gameObject.SetActive(false);
            current.Clear();
            this.m_ItemPool.Release(current.gameObject);
          }
        }
      }
    }
    this.m_ItemDict.Clear();
  }

  private void OnItemDataUpdated(int itemId)
  {
    List<ShopItemUI> shopItemUiList = (List<ShopItemUI>) null;
    this.m_ItemDict.TryGetValue(itemId, out shopItemUiList);
    if (shopItemUiList == null)
      return;
    using (List<ShopItemUI>.Enumerator enumerator = shopItemUiList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShopItemUI current = enumerator.Current;
        ShopStaticInfo shopData = ConfigManager.inst.DB_Shop.GetShopData(current.ShopStaticInfo.ID);
        current.SetShopStaticInfo(shopData);
        if (this.CanBuyAndUse())
        {
          int itemCount = ItemBag.Instance.GetItemCount(shopData.Item_InternalId);
          current.SwitchLocation(itemCount == 0);
        }
        else
          current.SwitchLocation(true);
      }
    }
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  private void OnItemClicked(ShopItemUI shopItemUI)
  {
    UIManager.inst.OpenPopup("BuyAndUsePopup", (Popup.PopupParameter) new StoreBuyAndUseDialog.StoreBuyAndUseDialogParamer()
    {
      shopStaticInfo = shopItemUI.ShopStaticInfo
    });
  }

  private void OnUserDataUpdate(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.m_Gold.text = Utils.FormatThousands(DBManager.inst.DB_User.Get(PlayerData.inst.uid).currency.gold.ToString());
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_ItemPool.Initialize(this.m_Renderer, this.m_ItemRoot);
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemDataUpdated);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnItemDataUpdated);
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdate);
    this.OnUserDataUpdate(PlayerData.inst.uid);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataUpdated);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnItemDataUpdated);
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdate);
  }

  protected delegate bool CompareMethod(ShopStaticInfo shopInfo, object extra);
}
