﻿// Decompiled with JetBrains decompiler
// Type: BigTimerChestCollectPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using UI;
using UnityEngine;

public class BigTimerChestCollectPopup : Popup
{
  public UITexture TT_Item;
  public UILabel LB_Count;
  public UILabel LB_Name;
  public UILabel LB_Discription;
  public Transform Border;
  public UITexture backgroud;
  private ItemStaticInfo itemInfo;
  private int itemCount;
  private int remainTime;
  private int endTime;
  public System.Action onChestOpen;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSeconde);
    BigTimerChestCollectPopup.Parameter parameter = orgParam as BigTimerChestCollectPopup.Parameter;
    if (parameter == null)
      return;
    this.itemInfo = ConfigManager.inst.DB_Items.GetItem(parameter.itemId);
    this.itemCount = parameter.itemCount;
    this.remainTime = parameter.time;
    this.endTime = parameter.endTime;
    if (this.itemInfo != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.TT_Item, this.itemInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this.LB_Discription.text = string.Empty;
      Utils.SetItemBackground(this.backgroud, parameter.itemId);
      Utils.SetItemName(this.LB_Name, parameter.itemId);
    }
    this.LB_Count.text = string.Format("X {0}", (object) this.itemCount);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSeconde);
  }

  public void OnItemClick()
  {
    Utils.ShowItemTip(this.itemInfo.internalId, this.Border, 0L, 0L, 0);
  }

  public void OnItemPress()
  {
    Utils.DelayShowTip(this.itemInfo.internalId, this.Border, 0L, 0L, 0);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  private void OnSeconde(int time)
  {
    this.remainTime = this.endTime - NetServerTime.inst.ServerTimestamp;
    if (this.remainTime < 600)
      this.LB_Discription.color = Color.red;
    this.LB_Discription.text = ScriptLocalization.Get("timed_gift_disappears_in", true) + Utils.FormatTime(this.remainTime, false, false, true);
  }

  public void OnButtonClick()
  {
    MessageHub.inst.GetPortByAction("gift:openBigTimerChest").SendRequest((Hashtable) null, (System.Action<bool, object>) ((success, obj) =>
    {
      if (!success)
        return;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.itemInfo.internalId);
      ItemRewardInfo.Data data = new ItemRewardInfo.Data();
      data.count = (float) this.itemCount;
      data.icon = itemStaticInfo.ImagePath;
      RewardsCollectionAnimator.Instance.Clear();
      RewardsCollectionAnimator.Instance.items.Add(data);
      RewardsCollectionAnimator.Instance.CollectItems(true);
      AudioManager.Instance.PlaySound("sfx_harvest_timed_chest", false);
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
      if (this.onChestOpen == null)
        return;
      this.onChestOpen();
    }), true);
  }

  public void OnCloseButtonClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int itemId;
    public int itemCount;
    public int time;
    public int endTime;
  }
}
