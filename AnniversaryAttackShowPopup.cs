﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryAttackShowPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class AnniversaryAttackShowPopup : Popup
{
  public UITexture tex;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    AttackShowManager.Instance.Startup((orgParam as AnniversaryAttackShowPopup.Parameter).para);
    AttackShowManager.Instance.onAttackShowBegin += new System.Action(this.ShowRenderTexture);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    AttackShowManager.Instance.onAttackShowBegin -= new System.Action(this.ShowRenderTexture);
    AttackShowManager.Instance.Terminate();
  }

  public void OnCloseButtonClicked()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
    AttackShowManager.Instance.StopAttackShowCoroutine();
  }

  private void ShowRenderTexture()
  {
    this.tex.gameObject.SetActive(true);
  }

  public class Parameter : Popup.PopupParameter
  {
    public AttackShowPara para;
  }
}
