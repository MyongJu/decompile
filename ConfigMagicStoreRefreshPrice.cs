﻿// Decompiled with JetBrains decompiler
// Type: ConfigMagicStoreRefreshPrice
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigMagicStoreRefreshPrice
{
  private List<MagicStoreRefreshPriceInfo> refreshPrices = new List<MagicStoreRefreshPriceInfo>();

  public void BuildDB(object res)
  {
    Hashtable sources = res as Hashtable;
    ConfigParse configParse = new ConfigParse();
    if (sources == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      Dictionary<string, MagicStoreRefreshPriceInfo> container;
      configParse.Parse<MagicStoreRefreshPriceInfo, string>(sources, "ID", out container);
      Dictionary<string, MagicStoreRefreshPriceInfo>.ValueCollection.Enumerator enumerator = container.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        string s = enumerator.Current.ID.Replace("magic_shop_refresh_price_", string.Empty);
        enumerator.Current.index = int.Parse(s);
        this.refreshPrices.Add(enumerator.Current);
      }
      this.refreshPrices.Sort((Comparison<MagicStoreRefreshPriceInfo>) ((x, y) => x.index.CompareTo(y.index)));
    }
  }

  public int GetPrice(int index)
  {
    if (index < this.refreshPrices.Count)
      return this.refreshPrices[index].Price;
    return this.refreshPrices[this.refreshPrices.Count - 1].Price;
  }

  public void Clear()
  {
    if (this.refreshPrices == null)
      return;
    this.refreshPrices.Clear();
  }
}
