﻿// Decompiled with JetBrains decompiler
// Type: BossDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class BossDetail
{
  public string name;
  public string portrait;
  public long start_total_troops;
  public long end_total_troops;
  public long original_total_troops;

  public string hpreduced
  {
    get
    {
      return (this.start_total_troops - this.end_total_troops).ToString();
    }
  }

  public string hpremained
  {
    get
    {
      return this.end_total_troops.ToString();
    }
  }
}
