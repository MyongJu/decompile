﻿// Decompiled with JetBrains decompiler
// Type: TableParser
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Text;

public static class TableParser
{
  public static int tableWidth = 20;

  public static string ToTableString(DataTable dataTable, string title, int tableWidth)
  {
    StringBuilder builder = new StringBuilder();
    TableParser.tableWidth = tableWidth;
    TableParser.AddTitle(builder, title, dataTable.Columns.Count);
    for (int index = 0; index < dataTable.Rows.Count; ++index)
    {
      TableParser.AddLine(builder, dataTable.Columns.Count);
      TableParser.AddRow(builder, dataTable[index], dataTable);
    }
    TableParser.AddLine(builder, dataTable.Columns.Count);
    return builder.ToString();
  }

  public static void AddTitle(StringBuilder builder, string title, int columns)
  {
    builder.Append(string.Format("|{0}|", (object) TableParser.AlignCentre(title, TableParser.tableWidth * columns + columns - 1)));
  }

  public static void AddLine(StringBuilder builder, int count)
  {
    builder.Append("\n" + new string('.', TableParser.tableWidth * count + count + 1) + "\n");
  }

  public static void AddRow(StringBuilder builder, DataRow row, DataTable dataTable)
  {
    for (int index = 0; index < row.Count; ++index)
      builder.Append(string.Format("|{0}", (object) TableParser.AlignCentre(row[dataTable.Columns[index]].ToString(), TableParser.tableWidth)));
    builder.Append("|");
  }

  private static string AlignCentre(string text, int width)
  {
    text = text.Length <= width ? text : text.Substring(0, width - 3) + "...";
    if (string.IsNullOrEmpty(text))
      return new string(' ', width);
    return text.PadRight(width - (width - text.Length) / 2).PadLeft(width);
  }
}
