﻿// Decompiled with JetBrains decompiler
// Type: InfoMode
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class InfoMode : MonoBehaviour, INPCMode
{
  public UITexture banner1;
  public UITexture banner2;

  public void SeedData(object param)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.banner1, "Texture/baner_1", (System.Action<bool>) null, false, false, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.banner2, "Texture/baner_2", (System.Action<bool>) null, false, false, string.Empty);
  }
}
