﻿// Decompiled with JetBrains decompiler
// Type: NameManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.IO;

public class NameManager
{
  private Dictionary<string, string> m_NameDict = new Dictionary<string, string>();
  private static NameManager m_Instance;

  public static NameManager Instance
  {
    get
    {
      if (NameManager.m_Instance == null)
        NameManager.m_Instance = new NameManager();
      return NameManager.m_Instance;
    }
  }

  public void Initialize(string text)
  {
    this.m_NameDict.Clear();
    if (string.IsNullOrEmpty(text))
      return;
    string empty = string.Empty;
    using (StringReader stringReader = new StringReader(text))
    {
      string str1;
      while ((str1 = stringReader.ReadLine()) != null)
      {
        string[] strArray = str1.Split(',');
        if (strArray.Length == 2)
        {
          string key = strArray[0];
          string str2 = strArray[1];
          if (!this.m_NameDict.ContainsKey(key))
            this.m_NameDict.Add(key, str2);
        }
      }
    }
  }

  public string GetName(string key)
  {
    string empty = string.Empty;
    this.m_NameDict.TryGetValue(key, out empty);
    return empty;
  }
}
