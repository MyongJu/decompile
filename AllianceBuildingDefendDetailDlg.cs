﻿// Decompiled with JetBrains decompiler
// Type: AllianceBuildingDefendDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Text;
using UI;

public class AllianceBuildingDefendDetailDlg : UI.Dialog
{
  public AllianceBuildingConstructInfo info;
  public AllianceBuildingConstructSlotContainer container;
  public UILabel title;
  public UILabel hint;
  public UIWidget m_FocusTarget;
  public UIButton demolishBtn;
  private Coordinate allianceBuildingCoor;
  private TileType tileType;
  private bool isDestroy;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    AllianceBuildingDefendDetailDlg.Parameter parameter = orgParam as AllianceBuildingDefendDetailDlg.Parameter;
    if (parameter != null)
    {
      this.allianceBuildingCoor = parameter.cor;
      this.tileType = parameter.type;
    }
    PVPSystem.Instance.Map.FocusTile(PVPMapData.MapData.ConvertTileKXYToWorldPosition(this.allianceBuildingCoor), this.m_FocusTarget, 0.0f);
    UIManager.inst.CloseKingdomTouchCircle();
    this.UpdateUI();
    this.AddEventHandler();
  }

  private void OnDestroy()
  {
    this.isDestroy = true;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Clear();
    this.RemoveEventHandler();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void Clear()
  {
    this.info.Clear();
    this.container.Clear();
  }

  private void UpdateUI()
  {
    if (this.isDestroy)
      return;
    this.UpdateInfoData();
    this.container.SetData(AllianceBuildUtils.GetAllianceBuildingMarchData(this.tileType, this.allianceBuildingCoor), true);
    this.title.text = this.GetTitle();
    this.hint.text = this.GetHintByTileType(this.tileType);
    this.UpdateDemolishBtnState();
  }

  private void UpdateInfoData()
  {
    int num = 0;
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(this.allianceBuildingCoor);
    switch (this.tileType)
    {
      case TileType.AllianceWarehouse:
        num = referenceAt.WareHouseData.ConfigId;
        break;
      case TileType.AllianceSuperMine:
        num = referenceAt.SuperMineData.ConfigId;
        break;
      case TileType.AlliancePortal:
        num = referenceAt.PortalData.ConfigId;
        break;
      case TileType.AllianceHospital:
        num = referenceAt.HospitalData.ConfigId;
        break;
      case TileType.AllianceTemple:
        num = referenceAt.TempleData.ConfigId;
        break;
    }
    string buildingStateString = AllianceBuildUtils.GetAllianceBuildingStateString(this.allianceBuildingCoor, this.tileType);
    string str1 = AllianceBuildUtils.CalcMyAllianceBuildingDurability(num).ToString() + "/" + AllianceBuildUtils.GetFullDurability(num).ToString();
    string str2 = AllianceBuildUtils.GetAllianceBuildingCurrentTroopCount(this.allianceBuildingCoor, this.tileType).ToString() + "/" + AllianceBuildUtils.GetAllianceBuildingMaxTroopCount(this.allianceBuildingCoor, this.tileType).ToString();
    AllianceBuildingConstructInfo.InfoData infodata = new AllianceBuildingConstructInfo.InfoData();
    infodata.content.Add(buildingStateString);
    infodata.content.Add(str1);
    infodata.content.Add(str2);
    int buildingJobRemainedTime = AllianceBuildUtils.GetBuildingJobRemainedTime(this.allianceBuildingCoor, this.tileType);
    if (buildingJobRemainedTime > 0)
    {
      infodata.showtime = true;
      infodata.remaintime = buildingJobRemainedTime;
    }
    else
      infodata.showtime = false;
    this.info.SeedData(infodata);
  }

  private void AddEventHandler()
  {
    switch (this.tileType)
    {
      case TileType.AllianceWarehouse:
        DBManager.inst.DB_AllianceWarehouse.onDataUpdate += new System.Action<AllianceWareHouseData>(this.OnWareHouseUpdated);
        DBManager.inst.DB_AllianceWarehouse.onDataRemove += new System.Action<AllianceWareHouseData>(this.OnWareHouseRemoved);
        break;
      case TileType.AllianceSuperMine:
        DBManager.inst.DB_AllianceSuperMine.onDataUpdate += new System.Action<AllianceSuperMineData>(this.OnSuperMineUpdated);
        DBManager.inst.DB_AllianceSuperMine.onDataRemove += new System.Action<AllianceSuperMineData>(this.OnSuperMineRemoved);
        break;
      case TileType.AlliancePortal:
        DBManager.inst.DB_AlliancePortal.onDataUpdate += new System.Action<AlliancePortalData>(this.OnPortalUpdated);
        DBManager.inst.DB_AlliancePortal.onDataRemove += new System.Action<AlliancePortalData>(this.OnPortalRemoved);
        break;
      case TileType.AllianceHospital:
        DBManager.inst.DB_AllianceHospital.onDataUpdate += new System.Action<AllianceHospitalData>(this.OnHospitalUpdated);
        DBManager.inst.DB_AllianceHospital.onDataRemove += new System.Action<AllianceHospitalData>(this.OnHospitalRemoved);
        break;
      case TileType.AllianceTemple:
        DBManager.inst.DB_AllianceTemple.onDataUpdate += new System.Action<AllianceTempleData>(this.OnTempleUpdated);
        DBManager.inst.DB_AllianceTemple.onDataRemoved += new System.Action<AllianceTempleData>(this.OnTempleRemoved);
        break;
    }
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  private void RemoveEventHandler()
  {
    switch (this.tileType)
    {
      case TileType.AllianceWarehouse:
        DBManager.inst.DB_AllianceWarehouse.onDataUpdate -= new System.Action<AllianceWareHouseData>(this.OnWareHouseUpdated);
        DBManager.inst.DB_AllianceWarehouse.onDataRemove -= new System.Action<AllianceWareHouseData>(this.OnWareHouseRemoved);
        break;
      case TileType.AllianceSuperMine:
        DBManager.inst.DB_AllianceSuperMine.onDataUpdate -= new System.Action<AllianceSuperMineData>(this.OnSuperMineUpdated);
        DBManager.inst.DB_AllianceSuperMine.onDataRemove -= new System.Action<AllianceSuperMineData>(this.OnSuperMineRemoved);
        break;
      case TileType.AlliancePortal:
        DBManager.inst.DB_AlliancePortal.onDataUpdate -= new System.Action<AlliancePortalData>(this.OnPortalUpdated);
        DBManager.inst.DB_AlliancePortal.onDataRemove -= new System.Action<AlliancePortalData>(this.OnPortalRemoved);
        break;
      case TileType.AllianceHospital:
        DBManager.inst.DB_AllianceHospital.onDataUpdate -= new System.Action<AllianceHospitalData>(this.OnHospitalUpdated);
        DBManager.inst.DB_AllianceHospital.onDataRemove -= new System.Action<AllianceHospitalData>(this.OnHospitalRemoved);
        break;
      case TileType.AllianceTemple:
        DBManager.inst.DB_AllianceTemple.onDataUpdate -= new System.Action<AllianceTempleData>(this.OnTempleUpdated);
        DBManager.inst.DB_AllianceTemple.onDataRemoved -= new System.Action<AllianceTempleData>(this.OnTempleRemoved);
        break;
    }
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
  }

  private void Process(int time)
  {
    this.UpdateInfoData();
  }

  private void OnWareHouseRemoved(AllianceWareHouseData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.OnCloseHandler();
  }

  private void OnWareHouseUpdated(AllianceWareHouseData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.UpdateUI();
  }

  private void OnSuperMineRemoved(AllianceSuperMineData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.OnCloseHandler();
  }

  private void OnSuperMineUpdated(AllianceSuperMineData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.UpdateUI();
  }

  private void OnPortalRemoved(AlliancePortalData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.OnCloseHandler();
  }

  private void OnPortalUpdated(AlliancePortalData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.UpdateUI();
  }

  private void OnHospitalRemoved(AllianceHospitalData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.OnCloseHandler();
  }

  private void OnHospitalUpdated(AllianceHospitalData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.UpdateUI();
  }

  private void OnTempleUpdated(AllianceTempleData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.UpdateUI();
  }

  private void OnTempleRemoved(AllianceTempleData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.OnCloseHandler();
  }

  private string GetHintByTileType(TileType tileType)
  {
    string empty1 = string.Empty;
    string empty2 = string.Empty;
    string Term;
    switch (tileType)
    {
      case TileType.AllianceWarehouse:
        Term = "alliance_storehouse_resources_tip";
        break;
      case TileType.AllianceSuperMine:
        Term = "alliance_resource_building_gather_speed_tip";
        break;
      case TileType.AlliancePortal:
        Term = "alliance_resource_building_gather_speed_tip";
        break;
      case TileType.AllianceTemple:
        Term = "alliance_fort_troop_hospital_tip";
        break;
      default:
        Term = string.Empty;
        break;
    }
    return ScriptLocalization.Get(Term, true);
  }

  public void OnDemolishBtnClicked()
  {
    string content = string.Format(ScriptLocalization.Get("alliance_buildings_confirm_demolish_description", true), (object) ScriptLocalization.Get(AllianceBuildUtils.GetAllianceBuildingName(this.tileType, this.allianceBuildingCoor), true));
    string title = ScriptLocalization.Get("alliance_buildings_confirm_placement_button", true);
    string left = ScriptLocalization.Get("id_uppercase_confirm", true);
    string right = ScriptLocalization.Get("id_uppercase_cancel", true);
    UIManager.inst.ShowConfirmationBox(title, content, left, right, ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.ConfirmDemolish), (System.Action) null, (System.Action) null);
  }

  private string GetTitle()
  {
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
    StringBuilder stringBuilder = new StringBuilder();
    if (allianceData != null)
      stringBuilder.Append("(" + allianceData.allianceAcronym + ")");
    string allianceBuildingName = AllianceBuildUtils.GetAllianceBuildingName(this.tileType, this.allianceBuildingCoor);
    stringBuilder.Append(allianceBuildingName);
    return stringBuilder.ToString();
  }

  private void UpdateDemolishBtnState()
  {
    this.demolishBtn.isEnabled = Utils.IsR5Member() || Utils.IsR4Member();
  }

  private void ConfirmDemolish()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "k"] = (object) this.allianceBuildingCoor.K;
    postData[(object) "x"] = (object) this.allianceBuildingCoor.X;
    postData[(object) "y"] = (object) this.allianceBuildingCoor.Y;
    if (this.tileType == TileType.AllianceTemple)
      MessageHub.inst.GetPortByAction("Map:takeBackAllianceTemple").SendRequest(postData, (System.Action<bool, object>) null, true);
    else
      MessageHub.inst.GetPortByAction("Map:takeBackAllianceBuilding").SendRequest(postData, (System.Action<bool, object>) null, true);
    this.OnCloseButtonPress();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public Coordinate cor;
    public TileType type;
  }
}
