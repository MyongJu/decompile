﻿// Decompiled with JetBrains decompiler
// Type: DKArenaMainDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DKArenaMainDlg : UI.Dialog
{
  private List<DKArenaRankPlayerData> totalData = new List<DKArenaRankPlayerData>();
  private List<DKArenaPlayerItem> totalItems = new List<DKArenaPlayerItem>();
  public UILabel title;
  public UILabel remainTime;
  public UIGrid grid;
  public DKArenaPlayerItem playerItemPrefab;
  public UITexture userIcon;
  public UITexture titleIcon;
  public UILabel score;
  public UILabel rank;
  public UILabel currentTimes;
  public UILabel winTimes;
  public UILabel loseTimes;
  public UILabel winprecent;
  public UILabel buttonLabel;
  public GameObject chestBox;
  public UILabel chestLabel;
  public UIButton matchBt;
  public UIButton rankBt;
  public UIButton rewardBt;
  public UILabel goldLabel;
  public UILabel freeLabel;
  public GameObject freeGo;
  public GameObject goldGo;
  public UILabel skillReset;
  public GameObject hadReward;
  public UILabel rewardNum;
  private int remainTimeValue;
  private bool needToShowReward;
  private int _currentTime;
  private int _myRank;

  private void UpdateUI()
  {
    this.rank.text = this._myRank <= 0 ? ScriptLocalization.Get("leaderboards_no_rank_notice", true) : (this._myRank <= 2000 ? this._myRank.ToString() : "2000+");
    this.title.text = ScriptLocalization.Get("dragon_knight_pvp_arena_title", true);
    UserDKArenaData currentData = DBManager.inst.DB_DKArenaDB.GetCurrentData();
    int rewardCount = currentData.GetRewardCount();
    NGUITools.SetActive(this.hadReward, rewardCount > 0);
    this.rewardNum.text = rewardCount.ToString();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.userIcon, "Texture/DragonKnight/Portrait/" + (DBManager.inst.DB_DragonKnight.GetDragonKnightData(PlayerData.inst.uid).Gender != DragonKnightGender.Male ? "dk_portrait_female" : "dk_portrait_male"), (System.Action<bool>) null, false, true, string.Empty);
    this.score.text = currentData.score.ToString();
    BuilderFactory.Instance.Build((UIWidget) this.titleIcon, ConfigManager.inst.DB_DKArenaTitleReward.GetTitleImage(currentData.score), (System.Action<bool>) null, true, false, true, string.Empty);
    this.skillReset.text = "[u]" + ScriptLocalization.Get("dragon_knight_uppercase_assign_skills", true) + "[/u]";
    this.currentTimes.text = currentData.stageJoinTimes.ToString();
    this.winTimes.text = currentData.stageWinTimes.ToString();
    this.loseTimes.text = currentData.stageLoseTimes.ToString();
    this.winprecent.text = ((double) currentData.StageWinPrecent * 100.0).ToString() + "%";
    this.remainTimeValue = 86400 - NetServerTime.inst.ServerTimestamp % 86400;
    this.DrawPlayers();
    int totalTimes = ConfigManager.inst.DB_DKArenaParticipateReward.GetTotalTimes();
    this._currentTime = currentData.todayPvpTimes + 1;
    DKArenaParticipateCost byTime = ConfigManager.inst.DB_DKArenaParticipateCost.GetByTime(this._currentTime);
    int freeTime = ConfigManager.inst.DB_DKArenaParticipateCost.GetFreeTime();
    string str1 = "(" + (object) this._currentTime + "/" + (object) freeTime + ")";
    if (byTime != null)
      Utils.SetPriceToLabel(this.goldLabel, byTime.Cost);
    if (freeTime >= this._currentTime)
    {
      NGUITools.SetActive(this.freeGo, true);
      NGUITools.SetActive(this.goldGo, false);
    }
    else
    {
      NGUITools.SetActive(this.freeGo, false);
      NGUITools.SetActive(this.goldGo, true);
    }
    if (byTime == null)
    {
      NGUITools.SetActive(this.freeGo, true);
      NGUITools.SetActive(this.goldGo, false);
    }
    this.chestLabel.text = this._currentTime.ToString() + "/" + (object) totalTimes;
    if (currentData.oppUid > 0L)
    {
      this.needToShowReward = false;
      this.freeLabel.text = ScriptLocalization.Get("dragon_knight_pvp_arena_continue_button", true);
      this.buttonLabel.text = ScriptLocalization.Get("dragon_knight_pvp_arena_continue_button", true);
      NGUITools.SetActive(this.freeGo, true);
      NGUITools.SetActive(this.goldGo, false);
    }
    else
    {
      if (byTime == null)
        str1 = string.Empty;
      UILabel freeLabel = this.freeLabel;
      string str2 = ScriptLocalization.Get("dragon_knight_pvp_arena_find_opponent_button", true) + str1;
      this.buttonLabel.text = str2;
      string str3 = str2;
      freeLabel.text = str3;
      if (ConfigManager.inst.DB_DKArenaParticipateReward.GetReward(this._currentTime) != null)
      {
        this.needToShowReward = true;
        NGUITools.SetActive(this.chestBox, true);
      }
    }
    this.RefreshTime();
    this.CheckState();
  }

  private void CheckState()
  {
    this.matchBt.isEnabled = ActivityManager.Intance.dkArenaActivity.IsStart();
    if (ActivityManager.Intance.dkArenaActivity.IsStart())
      return;
    this.needToShowReward = false;
    NGUITools.SetActive(this.chestBox, false);
  }

  private void ShowReward()
  {
    Dictionary<int, int> rewardDetail = ConfigManager.inst.DB_DKArenaParticipateReward.GetRewardDetail(this._currentTime);
    RewardsCollectionAnimator.Instance.Clear();
    if (rewardDetail.Count <= 0)
      return;
    Dictionary<int, int>.KeyCollection.Enumerator enumerator = rewardDetail.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(enumerator.Current);
      RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
      {
        icon = itemStaticInfo.ImagePath,
        count = (float) rewardDetail[enumerator.Current]
      });
    }
    AudioManager.Instance.PlaySound("sfx_ui_signin", false);
    RewardsCollectionAnimator.Instance.CollectItems(false);
  }

  public void OnViewRewardDetail()
  {
    Dictionary<int, int> rewardDetail = ConfigManager.inst.DB_DKArenaParticipateReward.GetRewardDetail(DBManager.inst.DB_DKArenaDB.GetCurrentData().todayPvpTimes + 1);
    UIManager.inst.OpenPopup("RewardDetailPopUp", (Popup.PopupParameter) new RewardDetailPopUp.Parameter()
    {
      items = rewardDetail,
      title = ScriptLocalization.Get("dragon_knight_pvp_arena_participation_reward_title", true)
    });
  }

  private void DrawPlayers()
  {
    for (int index = 0; index < this.totalData.Count; ++index)
      this.GetItem().SetData(this.totalData[index]);
    this.grid.Reposition();
  }

  private DKArenaPlayerItem GetItem()
  {
    GameObject go = NGUITools.AddChild(this.grid.gameObject, this.playerItemPrefab.gameObject);
    NGUITools.SetActive(go, true);
    DKArenaPlayerItem component = go.GetComponent<DKArenaPlayerItem>();
    this.totalItems.Add(component);
    return component;
  }

  private void RefreshTime()
  {
    int remainTime = ActivityManager.Intance.dkArenaActivity.RemainTime;
    if (remainTime > 0)
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      string str1 = Utils.FormatTime(remainTime, true, true, true);
      para.Add("1", str1);
      string Term = "event_finishes_num";
      if (!ActivityManager.Intance.dkArenaActivity.IsStart())
        Term = "event_starts_num";
      this.remainTime.text = ScriptLocalization.GetWithPara(Term, para, true);
      if (ActivityManager.Intance.dkArenaActivity.IsStart())
        return;
      para.Clear();
      para.Add("0", str1);
      UILabel freeLabel = this.freeLabel;
      string withPara = ScriptLocalization.GetWithPara("dragon_knight_pvp_arena_next_season_start_time", para, true);
      this.buttonLabel.text = withPara;
      string str2 = withPara;
      freeLabel.text = str2;
    }
    else
      UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
    DBManager.inst.DB_DKArenaDB.onDataChanged += new System.Action<long>(this.OnDateChange);
  }

  private void OnDateChange(long id)
  {
    int rewardCount = DBManager.inst.DB_DKArenaDB.GetCurrentData().GetRewardCount();
    NGUITools.SetActive(this.hadReward, rewardCount > 0);
    this.rewardNum.text = rewardCount.ToString();
  }

  private void Process(int time)
  {
    --this.remainTimeValue;
    this.RefreshTime();
  }

  private void RemoveEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
    DBManager.inst.DB_DKArenaDB.onDataChanged -= new System.Action<long>(this.OnDateChange);
  }

  public void OnMatchPlayer()
  {
    DKArenaParticipateCost byTime = ConfigManager.inst.DB_DKArenaParticipateCost.GetByTime(this._currentTime);
    UserDKArenaData currentData = DBManager.inst.DB_DKArenaDB.GetCurrentData();
    if (byTime == null && currentData.oppUid == 0L)
    {
      string withPara = ScriptLocalization.GetWithPara("dragon_knight_pvp_arena_find_opponent_limit_description", new Dictionary<string, string>()
      {
        {
          "0",
          Utils.FormatTime(this.remainTimeValue, false, true, true)
        }
      }, true);
      string title = ScriptLocalization.Get("chat_contacts_error_popup_title", true);
      string left = ScriptLocalization.Get("id_uppercase_cancel", true);
      string right = ScriptLocalization.Get("id_uppercase_confirm", true);
      UIManager.inst.ShowConfirmationBox(title, withPara, left, right, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) null, (System.Action) null, (System.Action) null);
    }
    else if (byTime != null && byTime.Cost > PlayerData.inst.hostPlayer.Currency)
    {
      Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
    }
    else
    {
      this.matchBt.isEnabled = false;
      RequestManager.inst.SendRequest("dragonKnight:match", (Hashtable) null, new System.Action<bool, object>(this.OnMatchCallBack), true);
    }
  }

  private void OnMatchCallBack(bool success, object result)
  {
    if (!success)
      return;
    UIManager.inst.ShowSystemBlocker("FullScreenWait", (SystemBlocker.SystemBlockerParameter) null);
    if (this.needToShowReward)
      this.ShowReward();
    Hashtable hashtable = result as Hashtable;
    long uid = 0;
    ArrayList skills = (ArrayList) null;
    if (hashtable.ContainsKey((object) "opp_uid") && hashtable[(object) "opp_uid"] != null)
      long.TryParse(hashtable[(object) "opp_uid"].ToString(), out uid);
    if (hashtable.ContainsKey((object) "opp_skill") && hashtable[(object) "opp_skill"] != null)
      skills = hashtable[(object) "opp_skill"] as ArrayList;
    if (uid <= 0L)
      return;
    if (DBManager.inst.DB_DragonKnight.Get(uid) == null)
    {
      UIManager.inst.HideSystemBlocker("FullScreenWait", (SystemBlocker.SystemBlockerParameter) null);
      this.needToShowReward = false;
    }
    else
    {
      Hashtable talent = (Hashtable) null;
      BattleManager.Instance.SetFlag();
      if (!this.needToShowReward)
      {
        UIManager.inst.HideSystemBlocker("FullScreenWait", (SystemBlocker.SystemBlockerParameter) null);
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.DragonKnight;
        DragonKnightDungeonData dungeonData = new DragonKnightDungeonData();
        dungeonData.InitWithArenaData(uid, skills, talent, 20000002);
        DragonKnightSystem.Instance.RoomManager.SetMazeData(dungeonData);
      }
      else
        Utils.ExecuteInSecs(2f, (System.Action) (() =>
        {
          UIManager.inst.HideSystemBlocker("FullScreenWait", (SystemBlocker.SystemBlockerParameter) null);
          UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
          GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.DragonKnight;
          DragonKnightDungeonData dungeonData = new DragonKnightDungeonData();
          dungeonData.InitWithArenaData(uid, skills, talent, 20000002);
          DragonKnightSystem.Instance.RoomManager.SetMazeData(dungeonData);
        }));
    }
  }

  public void OnShowRankRewards()
  {
    TimeLimitAllRewardPopup.Parameter parameter = new TimeLimitAllRewardPopup.Parameter()
    {
      isTotalReward = true,
      subTitleString = Utils.XLAT("event_stage_ranking_reward")
    };
    parameter.data = (ActivityBaseData) ActivityManager.Intance.dkArenaActivity;
    parameter.subTitleString = ScriptLocalization.Get("event_uppercase_season_rewards", true);
    UIManager.inst.OpenPopup("Activity/TimeLimitAllRewardPopup", (Popup.PopupParameter) parameter);
  }

  public void OnSetPlayerSkill()
  {
    MessageHub.inst.GetPortByAction("dragonKnight:getArenaDungeonInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) ((_param0, _param1) => UIManager.inst.OpenPopup("DragonKnight/DragonKnightSkillSettingPopup", (Popup.PopupParameter) null)), true);
  }

  public void OnShowBattleHistory()
  {
    UIManager.inst.OpenPopup("DKArena/DKArenaRecodePopup", (Popup.PopupParameter) null);
  }

  public void OnShowScoreRewads()
  {
    UIManager.inst.OpenPopup("DKArena/DKArenaScoreRewardsPopup", (Popup.PopupParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.freeLabel.text = ScriptLocalization.Get("dragon_knight_pvp_arena_find_opponent_button", true);
    NGUITools.SetActive(this.chestBox, false);
    RequestManager.inst.SendRequest("dragonKnight:getDkArenaInfo", (Hashtable) null, new System.Action<bool, object>(this.OnLoadDataCallBack), true);
  }

  private void OnLoadDataCallBack(bool success, object result)
  {
    if (!success)
      return;
    this.totalData.Clear();
    Hashtable hashtable1 = result as Hashtable;
    if (hashtable1.ContainsKey((object) "rank") && hashtable1[(object) "rank"] != null)
      int.TryParse(hashtable1[(object) "rank"].ToString(), out this._myRank);
    if (hashtable1.ContainsKey((object) "top_n") && hashtable1[(object) "top_n"] != null)
    {
      Hashtable hashtable2 = hashtable1[(object) "top_n"] as Hashtable;
      if (hashtable2 != null)
      {
        IEnumerator enumerator = hashtable2.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          string s = enumerator.Current.ToString();
          int result1 = 0;
          if (int.TryParse(s, out result1))
          {
            Hashtable hashtable3 = hashtable2[enumerator.Current] as Hashtable;
            DKArenaRankPlayerData arenaRankPlayerData = new DKArenaRankPlayerData();
            arenaRankPlayerData.Decode((object) hashtable3);
            arenaRankPlayerData.rank = result1;
            this.totalData.Add(arenaRankPlayerData);
          }
        }
        this.totalData.Sort(new Comparison<DKArenaRankPlayerData>(this.CompareItem));
      }
    }
    this.UpdateUI();
    this.AddEventHandler();
  }

  private int CompareItem(DKArenaRankPlayerData a, DKArenaRankPlayerData b)
  {
    return a.rank.CompareTo(b.rank);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.RemoveEventHandler();
    this.Dispose();
  }

  private void Dispose()
  {
    for (int index = 0; index < this.totalItems.Count; ++index)
      this.totalItems[index].Dispose();
    this.totalItems.Clear();
    DBManager.inst.GetInventory(BagType.DragonKnight).ClearThirdPartyInventory();
    DBManager.inst.DB_Gems.ClearThirdPartyInventory();
  }
}
