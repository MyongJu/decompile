﻿// Decompiled with JetBrains decompiler
// Type: LegendSkillItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UI;
using UnityEngine;

public class LegendSkillItem : MonoBehaviour
{
  private Color normalColor = new Color(0.9411765f, 0.8705882f, 0.7411765f, 1f);
  private Color noEnoughColor = Color.red;
  public UITexture skillIcon;
  public UILabel skillName;
  public UILabel skillValue;
  public UILabel skillLevel;
  private LegendSkillInfo _skill;
  public System.Action<long> UpdagteSkillHandler;
  public UILabel sliver;
  public UILabel skillPoint;
  public GameObject upgradeContent;
  private LegendSkillInfo nextLevel;
  public UIButton upgradeButton;
  public GameObject sliverRequire;
  private LegendData _legendData;

  public void SetData(LegendSkillInfo skill, LegendData legendData)
  {
    this._skill = skill;
    this.nextLevel = ConfigManager.inst.DB_LegendSkill.GetNextLevel(this._skill.ID, this._skill.type);
    this._legendData = legendData;
    this.UpdateUI();
  }

  private void Start()
  {
  }

  public void Clear()
  {
    if ((UnityEngine.Object) this.skillIcon != (UnityEngine.Object) null)
      BuilderFactory.Instance.Release((UIWidget) this.skillIcon);
    this.UpdagteSkillHandler = (System.Action<long>) null;
  }

  public void OnSkillIconPress()
  {
    UIManager.inst.OpenPopup("CommonDescPopup", (Popup.PopupParameter) new CommonDetailDlg.Parameter()
    {
      title = ScriptLocalization.Get("hero_altar_uppercase_skill_info", true),
      detail = this._skill.Loc_Desc
    });
  }

  private void UpdateUI()
  {
    if (this.nextLevel == null)
    {
      this.upgradeContent.gameObject.SetActive(false);
      this.skillValue.text = Utils.GetSkillValueDesc(this._skill);
    }
    else
      this.Refresh();
    this.skillLevel.text = ScriptLocalization.Get("id_lv", true) + this._skill.Level.ToString();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.skillIcon, this._skill.Image, (System.Action<bool>) null, true, false, string.Empty);
    this.skillName.text = this._skill.Loc_Name;
    if (this.nextLevel == null || this.nextLevel.cost.silver <= 0)
      NGUITools.SetActive(this.sliverRequire, false);
    else
      NGUITools.SetActive(this.sliverRequire, true);
  }

  private void Refresh()
  {
    this.upgradeContent.gameObject.SetActive(true);
    this.sliver.text = this.nextLevel.cost.silver.ToString();
    this.skillValue.text = Utils.GetSkillValueDesc(this._skill);
    this.sliver.color = !this.nextLevel.cost.IsSilverEnough() ? this.noEnoughColor : this.normalColor;
    this.skillPoint.text = this.nextLevel.skillPoints.ToString();
    this.skillPoint.color = this.nextLevel.skillPoints > PlayerData.inst.userData.skillPoint ? this.noEnoughColor : this.normalColor;
    bool flag = false;
    flag = this.nextLevel.cost.IsSilverEnough() && this.nextLevel.skillPoints <= PlayerData.inst.userData.skillPoint;
    this.upgradeButton.isEnabled = this.nextLevel.generalLevel <= this._legendData.Level;
  }

  private void Update()
  {
    if (this.nextLevel == null)
      return;
    this.Refresh();
  }

  public void OnUpdateSkillBtPress()
  {
    if (this.UpdagteSkillHandler == null || this.nextLevel == null)
      return;
    this.UpdagteSkillHandler((long) this.nextLevel.internalId);
  }
}
