﻿// Decompiled with JetBrains decompiler
// Type: OneStoreIapHelper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using HSMiniJSON;
using IapError;
using IapResponse;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class OneStoreIapHelper : MonoBehaviour
{
  private string ONESTORE_AID = "OA00711627";
  private Dictionary<string, Product> _productDict = new Dictionary<string, Product>();
  private Dictionary<string, OneStoreIapHelper.ProductParams> _unconfirmedRequestedProducts = new Dictionary<string, OneStoreIapHelper.ProductParams>();
  public const string FUNC_PAYMENT_INITIALIZE_SUCCESS = "OnPaymentInitializeSuccess";
  public const string FUNC_PAYMENT_INITIALIZE_ERROR = "OnPaymentInitializeError";
  public const string FUNC_NAME_PURCHASE_ERROR = "OnPaymentPurchaseError";
  public const string FUNC_NAME_PAYMENT_PURCHASE_SUCCESS = "OnPaymentPurchaseSuccess";
  public const string ERROR_MESSAGE = "{errorCode:9999, errorMsg:\"\", errorLocalizedMsg:\"\"}";
  private const string PLUGIN_VERSION = "1.0";
  private const string KOREA_COUNTRY_CODE = "KR";
  private AndroidJavaClass unityPlayerClass;
  private AndroidJavaObject currentActivity;
  private AndroidJavaObject iapRequestAdapter;

  private string CurrecyCode
  {
    get
    {
      return GlobalizationCodeConverter.CountryCodeToCurrencyCode("KR");
    }
  }

  private Product GetProductData(string productId)
  {
    if (this._productDict.ContainsKey(productId))
      return this._productDict[productId];
    return (Product) null;
  }

  private void Awake()
  {
    this.unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
    this.currentActivity = this.unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
    if (this.currentActivity != null)
      this.iapRequestAdapter = new AndroidJavaObject("com.onestore.iap.unity.RequestAdapter", new object[3]
      {
        (object) nameof (OneStoreIapHelper),
        (object) this.currentActivity,
        (object) false
      });
    else
      OneStorePayAndroid.OneStoreLog("Init onestore error!!!");
    this.LoadUnconfirmedRequestedProducts();
  }

  private string RequestedProductsCacheFilePath
  {
    get
    {
      return Path.Combine(Application.persistentDataPath, "onestore.json");
    }
  }

  private void AddUnconfirmedRequestedProduct(string tid, OneStoreIapHelper.ProductParams param)
  {
    if (this._unconfirmedRequestedProducts.ContainsKey(tid))
      return;
    this._unconfirmedRequestedProducts.Add(tid, param);
    this.SaveUnconfirmedRequestedProducts();
  }

  private void RemoveUnconfirmedRequestedProduct(string tid)
  {
    if (!this._unconfirmedRequestedProducts.ContainsKey(tid))
      return;
    this._unconfirmedRequestedProducts.Remove(tid);
    this.SaveUnconfirmedRequestedProducts();
  }

  private void LoadUnconfirmedRequestedProducts()
  {
    if (!File.Exists(this.RequestedProductsCacheFilePath))
      return;
    string str = File.ReadAllText(this.RequestedProductsCacheFilePath);
    OneStorePayAndroid.OneStoreLog(string.Format("LoadPendingRequestProducts {0}", (object) str));
    this._unconfirmedRequestedProducts = JsonReader.Deserialize<Dictionary<string, OneStoreIapHelper.ProductParams>>(str);
  }

  private void SaveUnconfirmedRequestedProducts()
  {
    File.WriteAllText(this.RequestedProductsCacheFilePath, Utils.Object2Json((object) this._unconfirmedRequestedProducts));
  }

  public void RequestProductInfo()
  {
    this.iapRequestAdapter.Call("requestProductInfo", (object) true, (object) this.ONESTORE_AID);
  }

  public void RequestInitTid(string productId, Hashtable initParams)
  {
    OneStorePayAndroid.OneStoreLog(string.Format("RequestInitTid, productId {0}", (object) productId));
    GameObject gameObject = new GameObject();
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
    HttpPostProcessor httpPostProcessor = gameObject.AddComponent<HttpPostProcessor>();
    Product productData = this.GetProductData(productId);
    if (productData == null)
    {
      this.DoFakePaymentReqeust(productId);
    }
    else
    {
      initParams[(object) "amount"] = (object) productData.price;
      initParams[(object) "country_code"] = (object) "KR";
      initParams[(object) "currency_code"] = (object) this.CurrecyCode;
      initParams[(object) "plugion_version"] = (object) "1.0";
      httpPostProcessor.DoPost(this.InitTidUrl, initParams, initParams, new HttpPostProcessor.PostResultCallback(this.OnInitTidCallback));
    }
  }

  private void DoFakePaymentReqeust(string productId)
  {
    this.iapRequestAdapter.Call("requestPayment", (object) this.ONESTORE_AID, (object) productId, (object) "fake_product", (object) "fake_tid", (object) "fake_bpinfo");
  }

  private void OnInitTidCallback(bool result, Hashtable data, Hashtable userData)
  {
    OneStorePayAndroid.OneStoreLog(string.Format("OnInitTidCallback {0} {1} {2}", (object) result, data == null ? (object) "null" : (object) Utils.Object2Json((object) data), userData == null ? (object) "null" : (object) Utils.Object2Json((object) userData)));
    if (result)
    {
      string empty1 = string.Empty;
      if (userData.ContainsKey((object) "product_id") && userData[(object) "product_id"] != null)
      {
        string productId = userData[(object) "product_id"].ToString();
        string empty2 = string.Empty;
        if (userData.ContainsKey((object) "through_cargo") && userData[(object) "through_cargo"] != null)
        {
          string throughCargo = userData[(object) "through_cargo"].ToString();
          Hashtable hashtable = (Hashtable) null;
          if (data.ContainsKey((object) nameof (data)))
            hashtable = data[(object) nameof (data)] as Hashtable;
          if (hashtable != null)
          {
            string empty3 = string.Empty;
            if (hashtable.ContainsKey((object) "tid") && hashtable[(object) "tid"] != null)
            {
              string tid = hashtable[(object) "tid"].ToString();
              this.RequestPayment(productId, tid, throughCargo);
            }
            else
              OneStorePayAndroid.OneStoreLog("InitTidCallback fetch tid failed");
          }
          else
            OneStorePayAndroid.OneStoreLog("InitTidCallback fetch response_data failed");
        }
        else
          OneStorePayAndroid.OneStoreLog("InitTidCallback fetch through_cargo failed");
      }
      else
        OneStorePayAndroid.OneStoreLog("InitTidCallback fetch product_id failed");
    }
    else
      OneStorePayAndroid.OneStoreLog("InitTidCallback failed");
  }

  public void RequestOrderConfirm()
  {
    List<string> stringList = new List<string>();
    using (Dictionary<string, OneStoreIapHelper.ProductParams>.Enumerator enumerator = this._unconfirmedRequestedProducts.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, OneStoreIapHelper.ProductParams> current = enumerator.Current;
        stringList.Add(current.Key);
      }
    }
    if (stringList.Count == 0)
    {
      OneStorePayAndroid.OneStoreLog(string.Format("RequestOrderConfirm, unconfirmed tids is 0"));
    }
    else
    {
      string str = string.Join("|", stringList.ToArray());
      OneStorePayAndroid.OneStoreLog(string.Format("RequestOrderConfirm, tids[{0}]", (object) str));
      GameObject gameObject = new GameObject();
      UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
      HttpPostProcessor httpPostProcessor = gameObject.AddComponent<HttpPostProcessor>();
      Hashtable hashtable = new Hashtable();
      hashtable.Add((object) "tids", (object) str);
      httpPostProcessor.DoPost(this.CallbackUrl, hashtable, hashtable, new HttpPostProcessor.PostResultCallback(this.OnOrderConfirmCallback));
    }
  }

  private void OnOrderConfirmCallback(bool result, Hashtable data, Hashtable userData)
  {
    OneStorePayAndroid.OneStoreLog(string.Format("OnOrderConfirmCallback {0} {1} {2}", (object) result, data == null ? (object) "null" : (object) Utils.Object2Json((object) data), userData == null ? (object) "null" : (object) Utils.Object2Json((object) userData)));
    if (result)
    {
      if (!data.ContainsKey((object) nameof (data)) || data[(object) nameof (data)] == null)
        return;
      Hashtable hashtable = data[(object) nameof (data)] as Hashtable;
      if (hashtable.ContainsKey((object) "tids_ok"))
      {
        string str = hashtable[(object) "tids_ok"].ToString();
        char[] chArray = new char[1]{ '|' };
        foreach (string index in str.Split(chArray))
        {
          if (this._unconfirmedRequestedProducts.ContainsKey(index))
          {
            OneStoreIapHelper.ProductParams requestedProduct = this._unconfirmedRequestedProducts[index];
            OneStorePayAndroid.Instance.TargetGameObject.BroadcastMessage("OnPaymentPurchaseSuccess", (object) Json.Serialize((object) new Dictionary<string, object>()
            {
              {
                "product_id",
                (object) requestedProduct.productId
              },
              {
                "through_cargo",
                (object) requestedProduct.throughCargo
              }
            }));
            this.TracePaymentBIEvent(index, requestedProduct.productId);
            this.RemoveUnconfirmedRequestedProduct(index);
            OneStorePayAndroid.OneStoreLog(string.Format("Confirmed succuess order {0}", (object) index));
          }
        }
      }
      if (!hashtable.ContainsKey((object) "tids_fail"))
        return;
      string str1 = hashtable[(object) "tids_fail"].ToString();
      char[] chArray1 = new char[1]{ '|' };
      foreach (string tid in str1.Split(chArray1))
      {
        OneStorePayAndroid.Instance.TargetGameObject.BroadcastMessage("OnPaymentPurchaseError", (object) "{errorCode:9999, errorMsg:\"\", errorLocalizedMsg:\"\"}");
        this.RemoveUnconfirmedRequestedProduct(tid);
        OneStorePayAndroid.OneStoreLog(string.Format("Confirmed failed order {0}", (object) tid));
      }
    }
    else
      OneStorePayAndroid.OneStoreLog("OnOrderConfirmCallback failed");
  }

  private string InitTidUrl
  {
    get
    {
      string empty = string.Empty;
      if (!string.IsNullOrEmpty(PaymentManager.Instance.PaymentUrl))
        return PaymentManager.Instance.PaymentUrl + "/payment/onestore_init_tid/";
      return !(FunplusSdk.Instance.Environment == "sandbox") ? "https://payment.funplusgame.com/payment/onestore_init_tid/" : "https://payment-sandbox.funplusgame.com/payment/onestore_init_tid/";
    }
  }

  private string CallbackUrl
  {
    get
    {
      string empty = string.Empty;
      if (!string.IsNullOrEmpty(PaymentManager.Instance.PaymentUrl))
        return PaymentManager.Instance.PaymentUrl + "/callback/onestore/";
      return FunplusSdk.Instance.Environment == "sandbox" ? "https://payment-sandbox.funplusgame.com/callback/onestore/" : "https://payment.funplusgame.com/callback/onestore/";
    }
  }

  public void CommandResponse(string response)
  {
    OneStorePayAndroid.OneStoreLog("--------------------------------------------------------");
    OneStorePayAndroid.OneStoreLog("[UNITY] CommandResponse >>> " + response);
    OneStorePayAndroid.OneStoreLog("--------------------------------------------------------");
    Response response1 = JsonReader.Deserialize<Response>(response);
    OneStorePayAndroid.OneStoreLog(">>> " + response1.ToString());
    OneStorePayAndroid.OneStoreLog("--------------------------------------------------------");
    string method = response1.method;
    if (method == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (OneStoreIapHelper.\u003C\u003Ef__switch\u0024map4 == null)
    {
      // ISSUE: reference to a compiler-generated field
      OneStoreIapHelper.\u003C\u003Ef__switch\u0024map4 = new Dictionary<string, int>(1)
      {
        {
          "request_product_info",
          0
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!OneStoreIapHelper.\u003C\u003Ef__switch\u0024map4.TryGetValue(method, out num) || num != 0)
      return;
    this.HandleProductList(response1.result);
  }

  private void HandleProductList(Result result)
  {
    OneStorePayAndroid.OneStoreLog(string.Format("HandleProductList: {0}", (object) result.ToString()));
    string code = result.code;
    if (code.Equals("0000"))
    {
      List<Product> product = result.product;
      List<Dictionary<string, object>> dictionaryList = new List<Dictionary<string, object>>();
      this._productDict.Clear();
      using (List<Product>.Enumerator enumerator = product.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Product current = enumerator.Current;
          double price = current.price;
          long num = (long) (price * 1000000.0);
          Dictionary<string, object> dictionary = new Dictionary<string, object>()
          {
            {
              "productId",
              (object) current.id
            },
            {
              "title",
              (object) current.name
            },
            {
              "description",
              (object) current.name
            },
            {
              "price_currency_code",
              (object) "₩"
            },
            {
              "price",
              (object) string.Format("₩{0:N0}", (object) (int) price)
            },
            {
              "price_amount",
              (object) string.Format("{0:N0}", (object) (int) price)
            },
            {
              "price_amount_micros",
              (object) num
            }
          };
          dictionaryList.Add(dictionary);
          this._productDict.Add(current.id, current);
        }
      }
      string str = Json.Serialize((object) dictionaryList);
      OneStorePayAndroid.Instance.TargetGameObject.BroadcastMessage("OnPaymentInitializeSuccess", (object) str);
      OneStorePayAndroid.OneStoreLog("onestore product list: " + str);
    }
    else
    {
      OneStorePayAndroid.Instance.TargetGameObject.BroadcastMessage("OnPaymentInitializeError", (object) "{errorCode:9999, errorMsg:\"\", errorLocalizedMsg:\"\"}");
      OneStorePayAndroid.OneStoreLog("get onestore product data failed error " + code);
      OneStorePayAndroid.Instance.NoProductListErrorMessage = result.message;
    }
  }

  public void CommandError(string message)
  {
    OneStorePayAndroid.OneStoreLog("--------------------------------------------------------");
    OneStorePayAndroid.OneStoreLog("[UNITY] CommandError >>> " + message);
    OneStorePayAndroid.OneStoreLog("--------------------------------------------------------");
    OneStorePayAndroid.OneStoreLog(">>> " + JsonReader.Deserialize<Error>(message).ToString());
    OneStorePayAndroid.OneStoreLog("--------------------------------------------------------");
  }

  public void RequestPayment(string productId, string tid, string throughCargo)
  {
    Product productData = this.GetProductData(productId);
    if (productData == null)
    {
      OneStorePayAndroid.OneStoreLog("not install onestore app. Cant get product data.");
    }
    else
    {
      this.iapRequestAdapter.Call("requestPayment", (object) this.ONESTORE_AID, (object) productId, (object) productData.name, (object) tid, (object) throughCargo);
      OneStorePayAndroid.OneStoreLog(string.Format("RequestPayment: appId[{0}], productId[{1}], productName[{2}], tid[{3}], bpInfo[{4}]", (object) this.ONESTORE_AID, (object) productId, (object) productData.name, (object) tid, (object) throughCargo));
      this.AddUnconfirmedRequestedProduct(tid, new OneStoreIapHelper.ProductParams()
      {
        productId = productId,
        throughCargo = throughCargo
      });
    }
  }

  public void PaymentResponse(string response)
  {
    OneStorePayAndroid.OneStoreLog("--------------------------------------------------------");
    OneStorePayAndroid.OneStoreLog("[UNITY] PaymentResponse >>> " + response);
    OneStorePayAndroid.OneStoreLog("--------------------------------------------------------");
    Response response1 = JsonReader.Deserialize<Response>(response);
    OneStorePayAndroid.OneStoreLog(">>> " + response1.ToString());
    OneStorePayAndroid.OneStoreLog("--------------------------------------------------------");
    string method = response1.method;
    if (method != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (OneStoreIapHelper.\u003C\u003Ef__switch\u0024map5 == null)
      {
        // ISSUE: reference to a compiler-generated field
        OneStoreIapHelper.\u003C\u003Ef__switch\u0024map5 = new Dictionary<string, int>(1)
        {
          {
            "purchase",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (OneStoreIapHelper.\u003C\u003Ef__switch\u0024map5.TryGetValue(method, out num) && num == 0)
        this.HandlePaymentResult(response1.result);
    }
    this.RequestOrderConfirm();
  }

  private void HandlePaymentResult(Result result)
  {
    OneStorePayAndroid.OneStoreLog(string.Format("HandlePaymentResult: {0}", (object) result.ToString()));
  }

  public void PaymentError(string message)
  {
    OneStorePayAndroid.OneStoreLog("--------------------------------------------------------");
    OneStorePayAndroid.OneStoreLog("[UNITY] PaymentError >>> " + message);
    OneStorePayAndroid.OneStoreLog("--------------------------------------------------------");
    OneStorePayAndroid.OneStoreLog(">>> " + JsonReader.Deserialize<Error>(message).ToString());
    OneStorePayAndroid.OneStoreLog("--------------------------------------------------------");
    this.RequestOrderConfirm();
  }

  private void TracePaymentBIEvent(string tid, string productId)
  {
    try
    {
      if (FunplusSdk.Instance.IsSdkInstalled())
      {
        Dictionary<string, object> dictionary = new Dictionary<string, object>();
        Product productData = this.GetProductData(productId);
        if (productData == null)
        {
          OneStorePayAndroid.OneStoreLog("not install onestore app. Cant get product data.");
        }
        else
        {
          dictionary.Add("amount", (object) productData.price.ToString());
          dictionary.Add("currency", (object) this.CurrecyCode);
          dictionary.Add("iap_product_id", (object) productId);
          dictionary.Add("iap_product_name", (object) productData.name);
          dictionary.Add("transaction_id", (object) tid);
          dictionary.Add("payment_processor", (object) "onestoreiap");
          dictionary.Add("d_currency_received_type", (object) "point");
          OneStorePayAndroid.OneStoreLog(string.Format(nameof (TracePaymentBIEvent)));
          FunplusBi.Instance.TraceEvent("payment", Json.Serialize((object) dictionary));
        }
      }
      else
        OneStorePayAndroid.OneStoreLog(string.Format("TracePaymentBIEvent exception: {0}", (object) "FunplusSdk.Instance.IsSdkInstalled() == false"));
    }
    catch (Exception ex)
    {
      OneStorePayAndroid.OneStoreLog(string.Format("TracePaymentBIEvent exception: {0}", !string.IsNullOrEmpty(ex.Message) ? (object) ex.Message : (object) "unknow"));
    }
  }

  private class ProductParams
  {
    public string productId;
    public string throughCargo;
  }
}
