﻿// Decompiled with JetBrains decompiler
// Type: DungeonStoreRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DungeonStoreRenderer : MonoBehaviour
{
  private Dictionary<string, string> timeParam = new Dictionary<string, string>();
  public ItemIconRenderer _itemIcon;
  public UILabel _itemName;
  public UITexture _currencyIcon;
  public UILabel _currencyCount;
  public UISprite _lock;
  public UIButton _buy;
  public UILabel _unlockCondition;
  public UILabel _leftBuyCount;
  public UILabel _labelRefreshTime;
  private ShopCommonMain _shopInfo;

  public void SetData(ShopCommonMain shopItem)
  {
    this._shopInfo = shopItem;
    this.AddEventHandler();
    this.UpdateMainUI();
    this.UpdateUI();
    this.OnSecond(0);
  }

  public void ClearData()
  {
    this.RemoveEventHandler();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  private void RemoveEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  private void OnSecond(int time)
  {
    if (this._shopInfo == null)
      return;
    int shopItemRefreshTime = ShopCommonPayload.Instance.GetShopItemRefreshTime(this._shopInfo.internalId);
    if (shopItemRefreshTime > 0)
    {
      this.timeParam.Clear();
      this.timeParam.Add("0", Utils.FormatTime(shopItemRefreshTime - NetServerTime.inst.ServerTimestamp, true, false, true));
      this._labelRefreshTime.text = ScriptLocalization.GetWithPara("tower_shop_refreshes_in_status", this.timeParam, true);
      NGUITools.SetActive(this._labelRefreshTime.gameObject, true);
    }
    else
      NGUITools.SetActive(this._labelRefreshTime.gameObject, false);
    if (shopItemRefreshTime == NetServerTime.inst.ServerTimestamp)
      ;
  }

  private void UpdateMainUI()
  {
    this._itemIcon.SetData(this._shopInfo.item_id, string.Empty, false);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._shopInfo.item_id);
    BuilderFactory.Instance.HandyBuild((UIWidget) this._currencyIcon, ConfigManager.inst.DB_Items.GetItem(this._shopInfo.req_item_id).ImagePath, (System.Action<bool>) null, true, true, string.Empty);
    this._itemName.text = itemStaticInfo.LocName;
    this._currencyCount.text = this._shopInfo.req_value.ToString();
    EquipmentManager.Instance.ConfigQualityLabelWithColor(this._itemName, itemStaticInfo.Quality);
  }

  public void UpdateUI()
  {
    this._lock.gameObject.SetActive(!this._shopInfo.MeetCondition);
    this._unlockCondition.gameObject.SetActive(!this._shopInfo.MeetCondition);
    this._unlockCondition.text = this._shopInfo.LocCondition;
    this._buy.gameObject.SetActive(this._shopInfo.MeetCondition);
    this._currencyCount.color = ItemBag.Instance.GetItemCount(this._shopInfo.req_item_id) < this._shopInfo.req_value ? Color.red : new Color(0.9686275f, 0.8f, 0.003921569f);
    if (this._shopInfo.MeetCondition)
    {
      if (ShopCommonPayload.Instance.GetShopItemLeftCount(this._shopInfo.internalId) >= 0)
      {
        NGUITools.SetActive(this._leftBuyCount.gameObject, true);
        Dictionary<string, string> para = new Dictionary<string, string>();
        int shopItemLeftCount = ShopCommonPayload.Instance.GetShopItemLeftCount(this._shopInfo.internalId);
        para.Add("0", string.Format(shopItemLeftCount <= 0 ? "[C80000]{0}[-]" : "[C3B35D]{0}[-]", (object) shopItemLeftCount.ToString()));
        this._leftBuyCount.text = ScriptLocalization.GetWithPara("id_items_left", para, true);
      }
      else
        NGUITools.SetActive(this._leftBuyCount.gameObject, false);
    }
    else
      NGUITools.SetActive(this._leftBuyCount.gameObject, false);
  }

  public void OnBuy()
  {
    if (ItemBag.Instance.GetItemCount(this._shopInfo.req_item_id) < this._shopInfo.req_value)
      UIManager.inst.toast.Show(Utils.XLAT("toast_insufficient_items_cannot_buy", (object) "0", (object) ConfigManager.inst.DB_Items.GetItem(this._shopInfo.req_item_id).LocName), (System.Action) null, 4f, false);
    else
      UIManager.inst.OpenPopup("DragonKnight/DungeonStorePurchasePopup", (Popup.PopupParameter) new DungeonStorePurchasePopup.Parameter()
      {
        shopInfo = this._shopInfo
      });
  }
}
