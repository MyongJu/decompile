﻿// Decompiled with JetBrains decompiler
// Type: ResearchBaseDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ResearchBaseDlg : UI.Dialog
{
  private List<ResearchBaseSlot> slots = new List<ResearchBaseSlot>(6);
  private const string SLOT_NAME_PREFIX = "ResearchBaseSlot";
  private const int SLOT_COUNT = 6;
  private const int LEVEL_LIMIT_TREE = 6;
  public GameObject fx;
  public GameObject researchingUI;
  public GameObject noneResearchingUI;
  public TimerHUDUIItem timerHUDUIItem;
  public GameObject mCancelBuildPopup;
  public UILabel concelPanelContent;
  public UILabel title;
  public UILabel noresearchDescription;
  public UILabel researchingName;
  public UILabel researchingDescription;
  private TechLevel currentLevel;
  public UILabel speedUpPrice;
  public Color normalColor;
  public UILabel goldSpeedUpLabel;

  public void OnCloseButtonClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void OnDataChange(long itemID)
  {
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.UpdateSlot();
    this.UpdateResearchStatus();
  }

  private void InitUI()
  {
  }

  public void UpdateSlot()
  {
    this.noresearchDescription.text = ScriptLocalization.Get("research_description", true);
    this.slots.Clear();
    for (int index = 0; index < 6; ++index)
    {
      ResearchBaseSlot component = GameObject.Find("ResearchBaseSlot" + (object) index).GetComponent<ResearchBaseSlot>();
      component.tree = index + 1;
      GameObject.Find("ResearchBaseSlot" + (object) index + "/Title").GetComponent<UILabel>().text = ScriptLocalization.Get(ResearchConst.RESEARCH_TREE_KEY_PREFIXS[index] + "name", true);
      GameObject.Find("ResearchBaseSlot" + (object) index + "/Description").GetComponent<UILabel>().text = ScriptLocalization.Get(ResearchConst.RESEARCH_TREE_KEY_PREFIXS[index] + "description", true);
      this.slots.Add(component);
      if (index == 2 && LinkerHub.Instance.NeedGuide)
      {
        LinkerHub.Instance.MountHint(component.transform, 0, (object) null);
        LinkerHub.Instance.NeedGuide = false;
      }
    }
  }

  public void OnResearchSlotClick(int tree)
  {
    if (tree == 6)
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("research_battle_2_university_lvl_min");
      if (data != null && PlayerData.inst.CityData.GetHighestBuildingLevelFor("university") < data.ValueInt)
      {
        UIManager.inst.toast.Show(ScriptLocalization.GetWithMultyContents("toast_research_university_level", true, data.ValueInt.ToString()), (System.Action) null, 4f, false);
        return;
      }
    }
    LinkerHub.Instance.DisposeHint(this.slots[2].transform);
    UIManager.inst.OpenDlg("Research/ResearchTreeDlg", (UI.Dialog.DialogParameter) new ResearchTreeDlg.Parameter()
    {
      tree = tree,
      keepLastPositon = true
    }, true, true, true);
  }

  private void UpdateResearchStatus()
  {
    this.currentLevel = ResearchManager.inst.CurrentResearch;
    int researchingTree = -1;
    if (this.currentLevel == null)
    {
      this.researchingUI.SetActive(false);
      this.noneResearchingUI.SetActive(true);
    }
    else
    {
      this.researchingUI.SetActive(true);
      this.noneResearchingUI.SetActive(false);
      this.researchingName.text = this.currentLevel.Name;
      this.researchingDescription.text = this.currentLevel.Desc;
      long jobId = ResearchManager.inst.GetJobId(this.currentLevel);
      JobHandle job = JobManager.Instance.GetJob(jobId);
      double secsRemaining = job != null ? (double) job.LeftTime() : 0.0;
      double totalSeconds = job != null ? (double) job.Duration() : 0.0;
      double num1 = 0.0;
      this.timerHUDUIItem.gameObject.SetActive(true);
      this.timerHUDUIItem.mJobID = jobId;
      this.timerHUDUIItem.mFreeTimeSecs = num1;
      this.timerHUDUIItem.SetDetails("time_bar_researching", string.Empty, TimerType.TIMER_RESEARCH, secsRemaining, totalSeconds, 1, (TimerHUDUIItem.UpdateTimerHUDUIItem) null, (TimerHUDUIItem.UpdateTimerHUDUIItem) null, false);
      Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
      this.OnSecondHandler(0);
      researchingTree = this.currentLevel.Tech.tree;
      Hashtable ht = new Hashtable();
      int num2 = JobManager.Instance.GetJob(jobId).LeftTime();
      ht.Add((object) "time", (object) num2);
      int cost = ItemBag.CalculateCost(ht);
      this.speedUpPrice.text = cost.ToString();
      if (PlayerData.inst.hostPlayer.Currency < cost)
        this.speedUpPrice.color = Color.red;
      else
        this.speedUpPrice.color = this.normalColor;
    }
    this.mCancelBuildPopup.gameObject.SetActive(false);
    for (int index = 0; index < this.slots.Count; ++index)
      this.slots[index].UpdateEffect(researchingTree);
    this.GetComponentInParent<UIPanel>().Refresh();
  }

  private void OnSecondHandler(int timeStamp)
  {
    this.timerHUDUIItem.OnUpdate(timeStamp);
    if (!((UnityEngine.Object) this.goldSpeedUpLabel != (UnityEngine.Object) null) || this.currentLevel == null)
      return;
    long jobId = ResearchManager.inst.GetJobId(this.currentLevel);
    Hashtable ht = new Hashtable();
    int num = JobManager.Instance.GetJob(jobId).LeftTime();
    ht.Add((object) "time", (object) num);
    this.goldSpeedUpLabel.text = ItemBag.CalculateCost(ht).ToString();
  }

  public void OnTimerSpeedUpBtnPressed()
  {
    long jobId = ResearchManager.inst.GetJobId(this.currentLevel);
    Hashtable ht = new Hashtable();
    int num = JobManager.Instance.GetJob(jobId).LeftTime();
    ht.Add((object) "time", (object) num);
    if (PlayerData.inst.hostPlayer.Currency < ItemBag.CalculateCost(ht))
      Utils.ShowNotEnoughGoldTip();
    else
      this.ConFirmCallBack();
  }

  private void ConFirmCallBack()
  {
    long jobId = ResearchManager.inst.GetJobId(this.currentLevel);
    JobHandle job = JobManager.Instance.GetJob(jobId);
    Hashtable ht = new Hashtable();
    int num = job.LeftTime() - 0;
    if (num <= 0)
      return;
    ht.Add((object) "time", (object) num);
    int cost = ItemBag.CalculateCost(ht);
    RequestManager.inst.SendRequest("City:speedUpByGold", new Hashtable()
    {
      {
        (object) "job_id",
        (object) jobId
      },
      {
        (object) "gold",
        (object) cost
      }
    }, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      this.UpdateUI();
    }), true);
  }

  public void OnCancelTimerBtnPressed()
  {
    this.mCancelBuildPopup.SetActive(true);
    this.concelPanelContent.text = string.Format(Utils.XLAT("research_cancel_research_description"), (object) this.currentLevel.Name) + "\n" + Utils.XLAT("half_resource_return");
  }

  public void OnCancelYesPressed()
  {
    this.mCancelBuildPopup.SetActive(false);
    this.CancelResearch();
  }

  private void CancelResearch()
  {
    MessageHub.inst.GetPortByAction("Research:cancelResearch").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "city_id",
        (object) DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid).cityId
      },
      {
        (object) "research_id",
        (object) this.currentLevel.InternalID
      }
    }, new System.Action<bool, object>(this.CancelResearchCallback), true);
  }

  private void CancelResearchCallback(bool ret, object data)
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnCancelNoPressed()
  {
    this.mCancelBuildPopup.SetActive(false);
  }

  public void OnCancelClosePressed()
  {
    this.mCancelBuildPopup.SetActive(false);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
    DBManager.inst.DB_Research.onDataChanged += new System.Action<long>(this.OnDataChange);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.timerHUDUIItem.StopAllCoroutines();
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
    DBManager.inst.DB_Research.onDataChanged -= new System.Action<long>(this.OnDataChange);
  }
}
