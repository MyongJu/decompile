﻿// Decompiled with JetBrains decompiler
// Type: AllianceSearchItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;
using UnityEngine;

public class AllianceSearchItemRenderer : MonoBehaviour
{
  public AllianceSymbol m_AllianceSymbol;
  public UILabel m_AllianceName;
  public UILabel m_LeaderName;
  public UILabel m_AllianceMembers;
  public UILabel m_AlliancePower;
  public UILabel m_AllianceLanguage;
  public GameObject m_ApplyTip;
  private AllianceSearchItemData m_ItemData;

  public void SetData(AllianceSearchItemData itemData)
  {
    this.m_ItemData = itemData;
    this.UpdateUI();
  }

  public void OnItemClick()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) this.m_ItemData.allianceID;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("Alliance/AllianceJoinAndApplyDialog", (UI.Dialog.DialogParameter) new AllianceJoinAndApplyDialog.Parameter()
      {
        allianceData = DBManager.inst.DB_Alliance.Get(this.m_ItemData.allianceID)
      }, true, true, true);
    }), true);
  }

  public void UpdateUI()
  {
    this.m_AllianceSymbol.SetSymbols(this.m_ItemData.symbolCode);
    this.m_AllianceName.text = string.Format("[{0}]{1}", (object) this.m_ItemData.tag, (object) this.m_ItemData.name);
    this.m_AlliancePower.text = Utils.FormatThousands(this.m_ItemData.power.ToString());
    this.m_AllianceMembers.text = this.m_ItemData.memberCount.ToString() + "/" + this.m_ItemData.memberLimit.ToString();
    this.m_AllianceLanguage.text = Language.Instance.GetLanguageName(this.m_ItemData.language);
    this.m_LeaderName.text = this.m_ItemData.creatorName;
    this.m_ApplyTip.SetActive(this.m_ItemData.isPrivate && DBManager.inst.DB_AllianceInviteApply.IsApplied(this.m_ItemData.allianceID));
  }

  public bool IsApplied
  {
    get
    {
      return this.m_ApplyTip.gameObject.activeSelf;
    }
  }

  public string AllianceName
  {
    get
    {
      return this.m_ItemData.name;
    }
  }

  public string AllianceLanguage
  {
    get
    {
      return this.m_ItemData.language;
    }
  }
}
