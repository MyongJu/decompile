﻿// Decompiled with JetBrains decompiler
// Type: BuildQueueShopItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BuildQueueShopItem : MonoBehaviour
{
  public UILabel time;
  public UILabel gold;
  public int index;

  public void UpdateUI(string time, string gold, int index)
  {
    this.time.text = time;
    Utils.SetPriceToLabel(this.gold, int.Parse(gold));
    this.index = index;
  }
}
