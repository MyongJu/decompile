﻿// Decompiled with JetBrains decompiler
// Type: MiniMapTutorialTrigger
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MiniMapTutorialTrigger : MonoBehaviour
{
  public void OnClick()
  {
    if (!TutorialManager.Instance.IsRunning)
      return;
    MiniMapLandMark component = this.gameObject.GetComponent<MiniMapLandMark>();
    if (!((Object) component != (Object) null))
      return;
    Coordinate coordinate = new Coordinate(MiniMapSystem.Instance.TheCamera.Kingdom.WorldID, component.X, component.Y);
    Vector3 worldPoint = (Vector3) MiniMapSystem.Instance.TheCamera.Center.ConvertXYToWorldPoint(component.X, component.Y);
    MiniMapSystem.Instance.TheCamera.TriggerCoordinate = coordinate;
    MiniMapSystem.Instance.TheCamera.TriggerHitPoint = worldPoint;
    MiniMapSystem.Instance.TheCamera.TheCamearState = MiniMapCamera.CameraState.ZOOMING;
  }
}
