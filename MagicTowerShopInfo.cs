﻿// Decompiled with JetBrains decompiler
// Type: MagicTowerShopInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class MagicTowerShopInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "order")]
  public int order;
  [Config(Name = "magic_tower_id")]
  public int MagicTowerMainId;
  [Config(CustomParse = true, CustomType = typeof (Benefits), Name = "Benefits")]
  public Benefits Benefits;
  [Config(Name = "price")]
  public int price;
  [Config(Name = "type")]
  public int type;
  [Config(Name = "para_1")]
  public float param1;
  [Config(Name = "para_2")]
  public int param2;
  [Config(Name = "icon")]
  public string icon;
  [Config(Name = "description")]
  public string description;
}
