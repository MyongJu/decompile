﻿// Decompiled with JetBrains decompiler
// Type: LordTitleMainInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class LordTitleMainInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "sort_id")]
  public int sortId;
  [Config(Name = "icon")]
  public string icon;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "desc")]
  public string description;
  [Config(Name = "link")]
  public string link;
  [Config(Name = "quality")]
  public int quality;
  [Config(Name = "item_id")]
  public int itemId;
  [Config(Name = "priority")]
  public int priority;
  [Config(Name = "avatar_box")]
  public string avatarBox;
  [Config(Name = "name_board")]
  public string nameBoard;
  [Config(CustomParse = true, CustomType = typeof (Benefits), Name = "Benefits")]
  public Benefits benefits;

  public string LocName
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }

  public string LocDescription
  {
    get
    {
      return Utils.XLAT(this.description);
    }
  }

  public string AvatorIconPath
  {
    get
    {
      return "Texture/LordTitle/" + this.AvatarIcon;
    }
  }

  public string BoardIconPath
  {
    get
    {
      return "Texture/LordTitle/" + this.NameBoardIcon;
    }
  }

  public string AvatarIcon
  {
    get
    {
      return this.avatarBox;
    }
  }

  public string NameBoardIcon
  {
    get
    {
      return this.nameBoard;
    }
  }
}
