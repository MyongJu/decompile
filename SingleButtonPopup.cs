﻿// Decompiled with JetBrains decompiler
// Type: SingleButtonPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;
using UnityEngine;

public class SingleButtonPopup : Popup
{
  private System.Action onButtonClick;
  private System.Action onCloseButtonClick;
  [SerializeField]
  private SingleButtonPopup.Panel panel;

  public void OnCloseButtonClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    if (this.onCloseButtonClick == null)
      return;
    this.onCloseButtonClick();
    this.onCloseButtonClick = (System.Action) null;
  }

  public void OnButtonClick()
  {
    if (this.onButtonClick != null)
      this.onButtonClick();
    this.onCloseButtonClick = (System.Action) null;
    AudioManager.Instance.PlaySound("sfx_ui_comfirm", false);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    SingleButtonPopup.Parameter parameter = orgParam as SingleButtonPopup.Parameter;
    if ((parameter.setType & SingleButtonPopup.SetType.title) > (SingleButtonPopup.SetType) 0)
      this.panel.LB_title.text = parameter.title;
    if ((parameter.setType & SingleButtonPopup.SetType.description) > (SingleButtonPopup.SetType) 0)
      this.panel.LB_description.text = parameter.description;
    if ((parameter.setType & SingleButtonPopup.SetType.buttonText) > (SingleButtonPopup.SetType) 0)
      this.panel.LB_buttonText.text = parameter.buttonText;
    if ((parameter.setType & SingleButtonPopup.SetType.buttonEvent) > (SingleButtonPopup.SetType) 0)
      this.onButtonClick = parameter.buttonClickEvent;
    this.onCloseButtonClick = parameter.closeButtonCallbackEvent;
  }

  [Serializable]
  protected class Panel
  {
    public UILabel LB_title;
    public UILabel LB_description;
    public UILabel LB_buttonText;
    public UIButton BTN_button;
    public UIButton BTN_closeButton;
  }

  public enum SetType
  {
    title = 1,
    description = 2,
    buttonEvent = 4,
    buttonText = 8,
  }

  public class Parameter : Popup.PopupParameter
  {
    public SingleButtonPopup.SetType setType = SingleButtonPopup.SetType.title | SingleButtonPopup.SetType.description | SingleButtonPopup.SetType.buttonEvent;
    public const SingleButtonPopup.SetType BASIC_TYPE = SingleButtonPopup.SetType.title | SingleButtonPopup.SetType.description | SingleButtonPopup.SetType.buttonEvent;
    public const SingleButtonPopup.SetType SET_BUTTON_TEXT = SingleButtonPopup.SetType.title | SingleButtonPopup.SetType.description | SingleButtonPopup.SetType.buttonEvent | SingleButtonPopup.SetType.buttonText;
    public const SingleButtonPopup.SetType WARNING_POPUP = SingleButtonPopup.SetType.title | SingleButtonPopup.SetType.description;
    public System.Action buttonClickEvent;
    public string buttonText;
    public string description;
    public string title;
    public System.Action closeButtonCallbackEvent;
  }
}
