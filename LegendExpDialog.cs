﻿// Decompiled with JetBrains decompiler
// Type: LegendExpDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;
using UnityEngine;

public class LegendExpDialog : MonoBehaviour
{
  public LegendExpWindow m_ExpWindow;
  public LegendExpUseSlot[] m_UseSlots;
  public LegendExpSlot[] m_Slots;
  public UIButton m_AddExpButton;
  private LegendData m_LegendData;

  public void Show(LegendData legendData)
  {
    this.gameObject.SetActive(true);
    this.m_LegendData = legendData;
    this.ResetUI();
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemDataChanged);
  }

  private void OnItemDataChanged(int id)
  {
    for (int index = 0; index < this.m_Slots.Length; ++index)
      this.m_Slots[index].SetData(ConfigManager.inst.DB_Items.GetItem(LegendPotion.ITEMS[index]), new System.Action<ItemStaticInfo>(this.OnAddPotion));
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataChanged);
  }

  private void ResetUI()
  {
    this.m_ExpWindow.SetData(this.m_LegendData);
    for (int index = 0; index < this.m_Slots.Length; ++index)
      this.m_Slots[index].SetData(ConfigManager.inst.DB_Items.GetItem(LegendPotion.ITEMS[index]), new System.Action<ItemStaticInfo>(this.OnAddPotion));
    for (int index = 0; index < this.m_UseSlots.Length; ++index)
      this.m_UseSlots[index].SetData((ItemStaticInfo) null, 0, new System.Action<ItemStaticInfo>(this.OnRemovePotion));
    this.m_AddExpButton.isEnabled = false;
  }

  private void OnAddPotion(ItemStaticInfo itemInfo)
  {
    if (ConfigManager.inst.DB_LegendPoint.GetLegenMaxPoint() - (int) this.m_LegendData.Xp <= this.GetGain())
      return;
    int itemCount = ItemBag.Instance.GetItemCount(itemInfo.internalId);
    if (itemCount > 0)
    {
      for (int index = 0; index < this.m_UseSlots.Length; ++index)
      {
        LegendExpUseSlot useSlot = this.m_UseSlots[index];
        ItemStaticInfo itemStaticInfo = useSlot.GetItemStaticInfo();
        if (itemStaticInfo != null && itemStaticInfo.internalId == itemInfo.internalId)
        {
          int useCount1 = useSlot.GetUseCount();
          if (useCount1 < itemCount)
          {
            int useCount2 = useCount1 + 1;
            useSlot.SetUseCount(useCount2);
            goto label_14;
          }
          else
            goto label_14;
        }
      }
      for (int index = 0; index < this.m_UseSlots.Length; ++index)
      {
        LegendExpUseSlot useSlot = this.m_UseSlots[index];
        if (useSlot.GetItemStaticInfo() == null)
        {
          useSlot.SetData(itemInfo, 1, new System.Action<ItemStaticInfo>(this.OnRemovePotion));
          break;
        }
      }
label_14:
      this.UpdateExpWidgets();
    }
    else
    {
      ShopStaticInfo shopData = ConfigManager.inst.DB_Shop.GetShopData("shop" + itemInfo.ID);
      UIManager.inst.OpenPopup("GetMoreItems2", (Popup.PopupParameter) new StoreBuyAndUseDialog.StoreBuyAndUseDialogParamer()
      {
        shopStaticInfo = shopData
      });
    }
  }

  private int GetGain()
  {
    int num = 0;
    for (int index = 0; index < this.m_UseSlots.Length; ++index)
    {
      LegendExpUseSlot useSlot = this.m_UseSlots[index];
      ItemStaticInfo itemStaticInfo = useSlot.GetItemStaticInfo();
      int useCount = useSlot.GetUseCount();
      if (itemStaticInfo != null)
        num += (int) itemStaticInfo.Value * useCount;
    }
    return num;
  }

  private void UpdateExpWidgets()
  {
    int nextXp = 0;
    int legendLevelByXp = ConfigManager.inst.DB_LegendPoint.GetLegendLevelByXP(this.m_LegendData.Xp, out nextXp);
    int gain = this.GetGain();
    this.m_ExpWindow.SetExpText((int) this.m_LegendData.Xp, nextXp, gain);
    this.m_AddExpButton.isEnabled = legendLevelByXp != ConfigManager.inst.DB_LegendPoint.GetLegendMaxLevel() && gain > 0;
  }

  private void OnRemovePotion(ItemStaticInfo itemInfo)
  {
    if (itemInfo == null)
      return;
    for (int index1 = 0; index1 < this.m_UseSlots.Length; ++index1)
    {
      LegendExpUseSlot useSlot1 = this.m_UseSlots[index1];
      if (useSlot1.GetItemStaticInfo().internalId == itemInfo.internalId)
      {
        int useCount1 = useSlot1.GetUseCount() - 1;
        if (useCount1 <= 0)
        {
          int index2 = this.m_UseSlots.Length - 1;
          for (int index3 = index1; index3 < index2; ++index3)
          {
            LegendExpUseSlot useSlot2 = this.m_UseSlots[index3 + 1];
            ItemStaticInfo itemStaticInfo = useSlot2.GetItemStaticInfo();
            int useCount2 = useSlot2.GetUseCount();
            this.m_UseSlots[index3].SetData(itemStaticInfo, useCount2, new System.Action<ItemStaticInfo>(this.OnRemovePotion));
          }
          this.m_UseSlots[index2].SetData((ItemStaticInfo) null, 0, new System.Action<ItemStaticInfo>(this.OnRemovePotion));
          break;
        }
        useSlot1.SetUseCount(useCount1);
        break;
      }
    }
    this.UpdateExpWidgets();
  }

  public void OnAddExpPressed()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "uid"] = (object) PlayerData.inst.uid;
    postData[(object) "legend_id"] = (object) this.m_LegendData.LegendID;
    Hashtable hashtable = new Hashtable();
    postData[(object) "items"] = (object) hashtable;
    for (int index = 0; index < this.m_UseSlots.Length; ++index)
    {
      LegendExpUseSlot useSlot = this.m_UseSlots[index];
      ItemStaticInfo itemStaticInfo = useSlot.GetItemStaticInfo();
      int useCount = useSlot.GetUseCount();
      if (itemStaticInfo != null)
        hashtable[(object) itemStaticInfo.internalId.ToString()] = (object) useCount;
    }
    MessageHub.inst.GetPortByAction("Item:useMultiConsumableItem").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      this.ResetUI();
      UIManager.inst.toast.Show(Utils.XLAT("toast_xp_elixir_added_success"), (System.Action) null, 4f, false);
    }), true);
  }
}
