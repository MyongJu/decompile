﻿// Decompiled with JetBrains decompiler
// Type: UnlockSkillInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UnlockSkillInfo : MonoBehaviour
{
  public UILabel name;
  public UITexture icon;
  public UITexture backgroud;

  public void SeedData(UnlockSkillInfo.Data d)
  {
    this.name.text = d.name;
    Utils.SetItemNormalName(this.name, d.quailty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, d.icon, (System.Action<bool>) null, true, false, string.Empty);
    Utils.SetItemNormalBackground(this.backgroud, d.quailty);
  }

  public class Data
  {
    public string name;
    public string icon;
    public int quailty;
  }
}
