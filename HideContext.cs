﻿// Decompiled with JetBrains decompiler
// Type: HideContext
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class HideContext
{
  public HideMode hideMode = HideMode.move;
  public CtrlType ctrlType = CtrlType.gameObject;
  private float originalAlpha = 1f;
  private Vector3 originalPosition = Vector3.zero;
  public float moveY;
  public GameObject ctrlGameObject;
  private bool originalPosInited;
  private bool originalAlphaInited;
  private bool hidden;
  private bool hideOnce;

  private void ShowParticleEffect(GameObject obj, bool show)
  {
    ParticleSystem[] componentsInChildren = obj.GetComponentsInChildren<ParticleSystem>(true);
    for (int index = 0; index < componentsInChildren.Length; ++index)
    {
      if (show)
      {
        componentsInChildren[index].gameObject.SetActive(true);
        componentsInChildren[index].Play();
      }
      else
      {
        componentsInChildren[index].Stop();
        componentsInChildren[index].Clear();
        componentsInChildren[index].gameObject.SetActive(false);
      }
    }
  }

  protected void EnableWidgets(GameObject obj, bool enable)
  {
    foreach (Behaviour componentsInChild in obj.GetComponentsInChildren<UIButton>(true))
      componentsInChild.enabled = enable;
  }

  protected void EnableAnimator(GameObject obj, bool enable)
  {
    foreach (Behaviour componentsInChild in obj.GetComponentsInChildren<Animator>(true))
      componentsInChild.enabled = enable;
  }

  protected void EnableMesh(GameObject obj, bool enable)
  {
    foreach (Renderer componentsInChild in obj.GetComponentsInChildren<MeshRenderer>(true))
      componentsInChild.enabled = enable;
    foreach (Renderer componentsInChild in obj.GetComponentsInChildren<SkinnedMeshRenderer>(true))
      componentsInChild.enabled = enable;
  }

  private void OnCtrlObjectStart()
  {
    if (!this.originalPosInited)
    {
      this.originalPosition = this.ctrlGameObject.transform.localPosition;
      this.originalPosInited = true;
    }
    if (!this.hidden)
      return;
    this.ctrlGameObject.transform.localPosition = this.originalPosition + new Vector3(0.0f, this.moveY, 0.0f);
  }

  private void TweenGameObjectPos(Vector3 targetPos, float deltaTime)
  {
    if (!this.ctrlGameObject.activeInHierarchy)
    {
      if (!this.originalPosInited)
        this.ctrlGameObject.AddComponent<HiddenObjectPosSaver>().startCallback += new System.Action(this.OnCtrlObjectStart);
      else
        this.ctrlGameObject.transform.localPosition = targetPos;
    }
    else if (!this.originalPosInited)
    {
      this.originalPosition = TweenPosition.Begin(this.ctrlGameObject, deltaTime, targetPos, false).value;
      this.originalPosInited = true;
    }
    else
      TweenPosition.Begin(this.ctrlGameObject, deltaTime, targetPos, false);
  }

  private void SetGameObjectPos(Vector3 targetPos)
  {
    this.ctrlGameObject.transform.localPosition = targetPos;
  }

  public void Hide()
  {
    if (!(bool) ((UnityEngine.Object) this.ctrlGameObject))
      return;
    this.hidden = true;
    this.hideOnce = true;
    this.ShowParticleEffect(this.ctrlGameObject, false);
    if (this.ctrlType == CtrlType.spriteRender)
    {
      SpriteRenderColorTweener renderColorTweener = this.ctrlGameObject.GetComponent<SpriteRenderColorTweener>();
      if ((UnityEngine.Object) renderColorTweener == (UnityEngine.Object) null)
        renderColorTweener = this.ctrlGameObject.AddComponent<SpriteRenderColorTweener>();
      renderColorTweener.Hide();
    }
    if (this.hideMode == HideMode.move)
      this.TweenGameObjectPos(this.ctrlGameObject.transform.localPosition + new Vector3(0.0f, this.moveY, 0.0f), 0.2f);
    else if (this.hideMode == HideMode.alpha)
    {
      if (!this.originalAlphaInited)
      {
        this.originalAlpha = this.TweenAlphaGameObject(this.ctrlGameObject, 0.0f);
        this.originalAlphaInited = true;
      }
      else
      {
        double num = (double) this.TweenAlphaGameObject(this.ctrlGameObject, 0.0f);
      }
      this.EnableWidgets(this.ctrlGameObject, false);
      this.EnableAnimator(this.ctrlGameObject, false);
      this.EnableMesh(this.ctrlGameObject, false);
    }
    else
    {
      if (this.hideMode != HideMode.all)
        return;
      this.TweenGameObjectPos(this.ctrlGameObject.transform.localPosition + new Vector3(0.0f, this.moveY, 0.0f), 0.2f);
      if (!this.originalAlphaInited)
      {
        this.originalAlpha = this.TweenAlphaGameObject(this.ctrlGameObject, 0.0f);
        this.originalAlphaInited = true;
      }
      else
      {
        double num = (double) this.TweenAlphaGameObject(this.ctrlGameObject, 0.0f);
      }
    }
  }

  public void Show()
  {
    this.hidden = false;
    if (!(bool) ((UnityEngine.Object) this.ctrlGameObject) || !this.hideOnce)
      return;
    this.ShowParticleEffect(this.ctrlGameObject, true);
    if (this.ctrlType == CtrlType.spriteRender)
    {
      SpriteRenderColorTweener renderColorTweener = this.ctrlGameObject.GetComponent<SpriteRenderColorTweener>();
      if ((UnityEngine.Object) renderColorTweener == (UnityEngine.Object) null)
        renderColorTweener = this.ctrlGameObject.AddComponent<SpriteRenderColorTweener>();
      renderColorTweener.Show();
    }
    if (this.hideMode == HideMode.move)
    {
      if (!this.originalPosInited)
        return;
      this.TweenGameObjectPos(this.originalPosition, 0.2f);
    }
    else if (this.hideMode == HideMode.alpha)
    {
      if (!this.originalAlphaInited)
        return;
      double num = (double) this.TweenAlphaGameObject(this.ctrlGameObject, this.originalAlpha);
      this.EnableWidgets(this.ctrlGameObject, true);
      this.EnableAnimator(this.ctrlGameObject, true);
      this.EnableMesh(this.ctrlGameObject, true);
    }
    else
    {
      if (this.hideMode != HideMode.all)
        return;
      if (this.originalPosInited)
        this.TweenGameObjectPos(this.originalPosition, 0.2f);
      if (!this.originalAlphaInited)
        return;
      double num = (double) this.TweenAlphaGameObject(this.ctrlGameObject, this.originalAlpha);
    }
  }

  public void QuickShow()
  {
    this.hidden = false;
    if (!(bool) ((UnityEngine.Object) this.ctrlGameObject) || !this.hideOnce)
      return;
    this.ShowParticleEffect(this.ctrlGameObject, true);
    if (this.ctrlType == CtrlType.spriteRender)
    {
      SpriteRenderColorTweener renderColorTweener = this.ctrlGameObject.GetComponent<SpriteRenderColorTweener>();
      if ((UnityEngine.Object) renderColorTweener == (UnityEngine.Object) null)
        renderColorTweener = this.ctrlGameObject.AddComponent<SpriteRenderColorTweener>();
      renderColorTweener.Back();
    }
    if (this.hideMode == HideMode.move)
    {
      if (!this.originalPosInited)
        return;
      this.SetGameObjectPos(this.originalPosition);
    }
    else if (this.hideMode == HideMode.alpha)
    {
      if (!this.originalAlphaInited)
        return;
      this.SetGameObjectAlpha(this.ctrlGameObject, this.originalAlpha);
      this.EnableWidgets(this.ctrlGameObject, true);
      this.EnableAnimator(this.ctrlGameObject, true);
      this.EnableMesh(this.ctrlGameObject, true);
    }
    else
    {
      if (this.hideMode != HideMode.all)
        return;
      if (this.originalPosInited)
        this.SetGameObjectPos(this.originalPosition);
      if (!this.originalAlphaInited)
        return;
      this.SetGameObjectAlpha(this.ctrlGameObject, this.originalAlpha);
    }
  }

  public void QuickHide()
  {
    this.hidden = true;
    if (!(bool) ((UnityEngine.Object) this.ctrlGameObject) || !this.hideOnce)
      return;
    this.ShowParticleEffect(this.ctrlGameObject, false);
    if (this.ctrlType == CtrlType.spriteRender)
    {
      SpriteRenderColorTweener renderColorTweener = this.ctrlGameObject.GetComponent<SpriteRenderColorTweener>();
      if ((UnityEngine.Object) renderColorTweener == (UnityEngine.Object) null)
        renderColorTweener = this.ctrlGameObject.AddComponent<SpriteRenderColorTweener>();
      renderColorTweener.QuickHide();
    }
    if (this.hideMode == HideMode.move)
    {
      if (!this.originalPosInited)
        return;
      this.SetGameObjectPos(this.originalPosition + new Vector3(0.0f, this.moveY, 0.0f));
    }
    else if (this.hideMode == HideMode.alpha)
    {
      if (!this.originalAlphaInited)
        return;
      this.SetGameObjectAlpha(this.ctrlGameObject, 0.0f);
      this.EnableWidgets(this.ctrlGameObject, false);
      this.EnableAnimator(this.ctrlGameObject, false);
      this.EnableMesh(this.ctrlGameObject, false);
    }
    else
    {
      if (this.hideMode != HideMode.all)
        return;
      if (this.originalPosInited)
        this.SetGameObjectPos(this.originalPosition + new Vector3(0.0f, this.moveY, 0.0f));
      if (!this.originalAlphaInited)
        return;
      this.SetGameObjectAlpha(this.ctrlGameObject, 0.0f);
    }
  }

  private void SetGameObjectAlpha(GameObject obj, float alpha)
  {
    TweenAlpha.Begin(this.ctrlGameObject, 0.0f, alpha, 0.0f);
    int childCount = this.ctrlGameObject.transform.childCount;
    for (int index = 0; index < childCount; ++index)
      TweenAlpha.Begin(this.ctrlGameObject.transform.GetChild(index).gameObject, 0.0f, alpha, 0.0f);
  }

  private float TweenAlphaGameObject(GameObject obj, float alpha)
  {
    float num = TweenAlpha.Begin(this.ctrlGameObject, 0.2f, alpha, 0.0f).value;
    int childCount = this.ctrlGameObject.transform.childCount;
    for (int index = 0; index < childCount; ++index)
      TweenAlpha.Begin(this.ctrlGameObject.transform.GetChild(index).gameObject, 0.2f, alpha, 0.0f);
    return num;
  }
}
