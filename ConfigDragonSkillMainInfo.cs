﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragonSkillMainInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ConfigDragonSkillMainInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "group_id")]
  public int group_id;
  [Config(Name = "level")]
  public int level;
  [Config(Name = "req_id")]
  public int req_id;
  [Config(Name = "req_dragon_level")]
  public int req_dragon_level;
  [Config(Name = "dark")]
  public int dark;
  [Config(Name = "light")]
  public int light;
  [Config(CustomParse = true, CustomType = typeof (Benefits), Name = "Benefits")]
  public Benefits benefit;
  [Config(Name = "power")]
  public int power;
  [Config(Name = "is_learn")]
  public bool CanLearn;
  [Config(Name = "cost_item_id1")]
  public int item_id1;
  [Config(Name = "cost_item_count1")]
  public int item_count1;
  [Config(Name = "cost_item_id2")]
  public int item_id2;
  [Config(Name = "cost_item_count2")]
  public int item_count2;
  [Config(Name = "cost_item_id3")]
  public int item_id3;
  [Config(Name = "cost_item_count3")]
  public int item_count3;
}
