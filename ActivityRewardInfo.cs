﻿// Decompiled with JetBrains decompiler
// Type: ActivityRewardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ActivityRewardInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "activity_id")]
  public int ActivityMainSubInfo_ID;
  [Config(Name = "stronghold_lv")]
  public int StrongholdLev;
  [Config(Name = "step_req_value_1")]
  public int RequireScore_1;
  [Config(Name = "step_reward_1")]
  public int ItemRewardID_1;
  [Config(Name = "step_reward_value_1")]
  public int ItemRewardValue_1;
  [Config(Name = "step_req_value_2")]
  public int RequireScore_2;
  [Config(Name = "step_reward_2")]
  public int ItemRewardID_2;
  [Config(Name = "step_reward_value_2")]
  public int ItemRewardValue_2;
  [Config(Name = "step_req_value_3")]
  public int RequireScore_3;
  [Config(Name = "step_reward_3")]
  public int ItemRewardID_3;
  [Config(Name = "step_reward_value_3")]
  public int ItemRewardValue_3;
}
