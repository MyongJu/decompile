﻿// Decompiled with JetBrains decompiler
// Type: MarchReturningState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class MarchReturningState : MarchBaseState
{
  private bool needToReset;

  public override string Key
  {
    get
    {
      return "march_returing";
    }
  }

  protected override void OnStateEnd()
  {
    if (this.StateData.marchType == MarchData.MarchType.scout)
      AudioManager.Instance.PlaySound("sfx_kingdom_scout_return", false);
    else
      AudioManager.Instance.PlaySound("sfx_march_returned", false);
  }

  protected override void Prepare()
  {
    if (this.StateData.marchData != null && this.StateData.marchData.rallyId > 0L)
    {
      bool rallyAttackMarch = GameEngine.Instance.marchSystem.GetRallyAttackMarch(this.StateData.marchData.rallyId);
      if (rallyAttackMarch)
      {
        this.needToReset = true;
        this.Controler.DestoryPath();
        NGUITools.SetActive(this.Controler.gameObject, !rallyAttackMarch);
      }
      else
        base.Prepare();
    }
    else
      base.Prepare();
  }

  protected override void OnProcessHandler()
  {
    base.OnProcessHandler();
    if (!this.needToReset || GameEngine.Instance.marchSystem.GetRallyAttackMarch(this.StateData.marchData.rallyId))
      return;
    this.needToReset = false;
    base.Prepare();
  }

  protected override void OnMoveFinished()
  {
    if (!((Object) this.Controler != (Object) null) || this.Controler.marchData == null || this.Controler.marchData.ownerUid == PlayerData.inst.uid)
      return;
    this.Controler.DestoryPath();
    this.Controler.HideClickCollider();
    NGUITools.SetActive(this.Controler.gameObject, false);
  }
}
