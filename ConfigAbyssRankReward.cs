﻿// Decompiled with JetBrains decompiler
// Type: ConfigAbyssRankReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigAbyssRankReward
{
  private List<AbyssRankRewardInfo> _allRankReward = new List<AbyssRankRewardInfo>();
  public Dictionary<string, AbyssRankRewardInfo> datas;
  private Dictionary<int, AbyssRankRewardInfo> dicByUniqueId;

  public List<AbyssRankRewardInfo> AllRankReward
  {
    get
    {
      return this._allRankReward;
    }
  }

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<AbyssRankRewardInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    using (Dictionary<string, AbyssRankRewardInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this._allRankReward.Add(enumerator.Current);
    }
    this._allRankReward.Sort((Comparison<AbyssRankRewardInfo>) ((x, y) => x.rankMin.CompareTo(y.rankMin)));
  }

  public AbyssRankRewardInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (AbyssRankRewardInfo) null;
  }

  public AbyssRankRewardInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AbyssRankRewardInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, AbyssRankRewardInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, AbyssRankRewardInfo>) null;
  }
}
