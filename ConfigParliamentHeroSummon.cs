﻿// Decompiled with JetBrains decompiler
// Type: ConfigParliamentHeroSummon
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigParliamentHeroSummon
{
  private List<ParliamentHeroSummonInfo> _parliamentHeroSummonInfoList = new List<ParliamentHeroSummonInfo>();
  private Dictionary<string, ParliamentHeroSummonInfo> _datas;
  private Dictionary<int, ParliamentHeroSummonInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ParliamentHeroSummonInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, ParliamentHeroSummonInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._parliamentHeroSummonInfoList.Add(enumerator.Current);
    }
    this._parliamentHeroSummonInfoList.Sort((Comparison<ParliamentHeroSummonInfo>) ((a, b) => int.Parse(a.id).CompareTo(int.Parse(b.id))));
  }

  public List<ParliamentHeroSummonInfo> GetParliamentHeroSummonInfoList()
  {
    return this._parliamentHeroSummonInfoList;
  }

  public ParliamentHeroSummonInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (ParliamentHeroSummonInfo) null;
  }

  public ParliamentHeroSummonInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (ParliamentHeroSummonInfo) null;
  }
}
