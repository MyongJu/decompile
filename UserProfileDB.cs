﻿// Decompiled with JetBrains decompiler
// Type: UserProfileDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class UserProfileDB
{
  private Dictionary<string, long> _profiles = new Dictionary<string, long>();
  public const string KEY = "user_profile";
  private long _updateTime;
  public System.Action<string> onDataChanged;

  public long GetMemorySize()
  {
    return DatabaseUtils.GetMemorySize(typeof (long), (long) this._profiles.Count);
  }

  public void Publish_onDataChanged(string benefitId)
  {
    if (this.onDataChanged == null)
      return;
    this.onDataChanged(benefitId);
  }

  public bool Update(object orgData, long updateTime)
  {
    if (updateTime < this._updateTime)
      return false;
    this._updateTime = updateTime;
    Hashtable data;
    if (!BaseData.CheckAndParseOrgData(orgData, out data))
      return false;
    if (data.Contains((object) "uid"))
    {
      long result;
      long.TryParse(data[(object) "uid"].ToString(), out result);
      if (result != PlayerData.inst.uid)
        return false;
    }
    IDictionaryEnumerator enumerator = data.GetEnumerator();
    while (enumerator.MoveNext())
    {
      long result;
      bool flag = long.TryParse(enumerator.Value.ToString(), out result);
      if (flag)
      {
        if (this._profiles.ContainsKey(enumerator.Key.ToString()))
        {
          if (flag)
            this._profiles[enumerator.Key.ToString()] = result;
        }
        else
          this._profiles.Add(enumerator.Key.ToString(), result);
        this.Publish_onDataChanged(enumerator.Key.ToString());
      }
    }
    return true;
  }

  public void UpdateDatas(object orgDatas, long updateTime)
  {
    ArrayList arrayList = orgDatas as ArrayList;
    if (arrayList == null)
      return;
    int index = 0;
    for (int count = arrayList.Count; index < count; ++index)
      this.Update(arrayList[index], updateTime);
  }

  public long Get(string orgKey)
  {
    if (this._profiles.ContainsKey(orgKey))
      return this._profiles[orgKey];
    return 0;
  }
}
