﻿// Decompiled with JetBrains decompiler
// Type: GuardEventReceiver
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class GuardEventReceiver : MonoBehaviour
{
  private Dictionary<GuardEventType, GuardEventData> _eventDataDict = new Dictionary<GuardEventType, GuardEventData>();

  private void OnAnimation(GuardEventType type)
  {
    GuardEventData guardEventData;
    this._eventDataDict.TryGetValue(type, out guardEventData);
    if (guardEventData == null)
      return;
    Dictionary<string, GuardAction>.Enumerator enumerator = guardEventData.Actions.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Value.Process(this.Player);
  }

  public RoundPlayer Player { get; set; }

  public GuardAction GetAction(GuardEventType eventType, string actionName)
  {
    GuardEventData eventData = this.GetEventData(eventType);
    if (eventData == null)
      return (GuardAction) null;
    GuardAction guardAction = (GuardAction) null;
    eventData.Actions.TryGetValue(actionName, out guardAction);
    return guardAction;
  }

  public void SetEventData(GuardEventData eventData)
  {
    this._eventDataDict[eventData.EventType] = eventData;
  }

  public GuardEventData GetEventData(GuardEventType eventType)
  {
    GuardEventData guardEventData = (GuardEventData) null;
    this._eventDataDict.TryGetValue(eventType, out guardEventData);
    return guardEventData;
  }
}
