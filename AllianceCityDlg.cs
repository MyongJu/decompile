﻿// Decompiled with JetBrains decompiler
// Type: AllianceCityDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceCityDlg : UI.Dialog
{
  private GameObjectPool pools = new GameObjectPool();
  private List<AllianceBuildingItem> items = new List<AllianceBuildingItem>();
  private List<int>[] BUILDING_CATEGORIES = new List<int>[5]
  {
    new List<int>() { 0, 1 },
    new List<int>() { 3, 4, 5, 6 },
    new List<int>() { 2, 8 },
    new List<int>() { 7, 9 },
    new List<int>() { int.MaxValue }
  };
  private List<string> HINT_TEXT_KEYS = new List<string>()
  {
    "alliance_city_fort_brief_description",
    "alliance_city_resources_brief_description",
    "alliance_storehouse_resources_tip",
    "alliance_city_other_brief_description"
  };
  private HubPort cancelRall = MessageHub.inst.GetPortByAction("delFortress");
  public UILabel title;
  public UIButtonBar buttonBar;
  public UIGrid grid;
  public AllianceBuildingItem itemPrefab;
  public UIScrollView scrollView;
  public GameObject itemRoot;
  public UIGrid gridTab2;
  public UIScrollView scrollViewTab2;
  public GameObject content;
  public GameObject contentTab2;
  public UILabel hintLabel;
  private bool init;
  private bool needRefresh;
  private bool isStart;
  private AllianceCityDlg.Parameter m_Parameter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.UpdateUI();
    this.addEventHandler();
    this.m_Parameter = orgParam as AllianceCityDlg.Parameter;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Clear();
    this.pools.Clear();
    this.removeEventHandler();
  }

  private void Start()
  {
    this.isStart = true;
    if (!this.needRefresh)
      return;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (!this.isStart)
    {
      this.needRefresh = true;
    }
    else
    {
      this.title.text = ScriptLocalization.Get("alliance_city_uppercase_title", true);
      this.needRefresh = false;
      this.Init();
      this.Clear();
      List<AllianceBuildingStaticInfo> infos = new List<AllianceBuildingStaticInfo>();
      using (List<int>.Enumerator enumerator = this.BUILDING_CATEGORIES[this.buttonBar.SelectedIndex].GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          int current = enumerator.Current;
          infos.AddRange((IEnumerable<AllianceBuildingStaticInfo>) ConfigManager.inst.DB_AllianceBuildings.GetTotalBuildings(current));
        }
      }
      for (int index = 0; index < infos.Count; ++index)
      {
        AllianceBuildingItem allianceBuildingItem = this.GetItem();
        allianceBuildingItem.SetData(infos[index].internalId, this.m_Parameter);
        this.items.Add(allianceBuildingItem);
      }
      this.hintLabel.text = ScriptLocalization.Get(this.HINT_TEXT_KEYS[this.buttonBar.SelectedIndex], true);
      this.UpdateSuperMineUI(infos);
      this.grid.repositionNow = true;
      this.grid.Reposition();
      this.scrollView.movement = UIScrollView.Movement.Unrestricted;
      this.scrollView.ResetPosition();
      this.scrollView.movement = UIScrollView.Movement.Vertical;
      this.gridTab2.repositionNow = true;
      this.gridTab2.Reposition();
      this.scrollViewTab2.movement = UIScrollView.Movement.Unrestricted;
      this.scrollViewTab2.ResetPosition();
      this.scrollViewTab2.movement = UIScrollView.Movement.Vertical;
    }
  }

  private void UpdateSuperMineUI(List<AllianceBuildingStaticInfo> infos)
  {
    if (this.buttonBar.SelectedIndex != 1)
      return;
    AllianceBuildingStaticInfo buildingStaticInfo = infos.Find((Predicate<AllianceBuildingStaticInfo>) (x => AllianceBuildUtils.CheckBuildIsPlace(x.internalId)));
    if (buildingStaticInfo == null)
      return;
    for (int index = 0; index < infos.Count; ++index)
    {
      AllianceBuildingStaticInfo info = infos[index];
      if (info.internalId != buildingStaticInfo.internalId && !AllianceBuildUtils.CheckBuildIsPlace(info.internalId))
      {
        AllianceBuildingItem allianceBuildingItem = this.items.Find((Predicate<AllianceBuildingItem>) (x => x.BuildingConfigId == info.internalId));
        if ((UnityEngine.Object) allianceBuildingItem != (UnityEngine.Object) null && !AllianceBuildUtils.CheckBuildIsLock(info.internalId))
        {
          NGUITools.SetActive(allianceBuildingItem.button.gameObject, false);
          NGUITools.SetActive(allianceBuildingItem.durabilityRoot, false);
          NGUITools.SetActive(allianceBuildingItem.descRoot.gameObject, true);
          allianceBuildingItem.stateLabel.text = Utils.XLAT("alliance_resource_building_cannot_place");
          allianceBuildingItem.descLabel.text = Utils.XLAT("alliance_resource_building_cannot_place_description");
          GreyUtility.Grey(allianceBuildingItem.iconContainer);
        }
      }
    }
  }

  private void OnMarchCollitionHandler(object orgSrc)
  {
    Hashtable hashtable = orgSrc as Hashtable;
    if (hashtable == null || !hashtable.ContainsKey((object) "fortress_id"))
      return;
    DBManager.inst.DB_AllianceFortress.DelMyFortressData(long.Parse(hashtable[(object) "fortress_id"].ToString()));
    this.UpdateUI();
  }

  private void Init()
  {
    if (!this.init)
    {
      this.init = true;
      this.buttonBar.SelectedIndex = 0;
      this.buttonBar.OnSelectedHandler = new System.Action<int>(this.OnSelectedHandler);
    }
    this.pools.Clear();
    this.pools.Initialize(this.itemPrefab.gameObject, this.gridTab2.gameObject);
    this.content.SetActive(false);
    this.contentTab2.SetActive(true);
  }

  private void addEventHandler()
  {
    DBManager.inst.DB_AllianceFortress.onDataRemoved += new System.Action<AllianceFortressData>(this.OnRemove);
    DBManager.inst.DB_AllianceSuperMine.onDataRemove += new System.Action<AllianceSuperMineData>(this.OnSuperMineDataRemove);
    this.cancelRall.AddEvent(new System.Action<object>(this.OnMarchCollitionHandler));
  }

  private void OnRemove(AllianceFortressData data)
  {
    this.UpdateUI();
  }

  private void removeEventHandler()
  {
    DBManager.inst.DB_AllianceFortress.onDataRemoved -= new System.Action<AllianceFortressData>(this.OnRemove);
    DBManager.inst.DB_AllianceSuperMine.onDataRemove -= new System.Action<AllianceSuperMineData>(this.OnSuperMineDataRemove);
    this.cancelRall.RemoveEvent(new System.Action<object>(this.OnMarchCollitionHandler));
  }

  private void OnSelectedHandler(int index)
  {
    this.UpdateUI();
  }

  private void OnSuperMineDataRemove(AllianceSuperMineData data)
  {
    this.UpdateUI();
  }

  private void Clear()
  {
    for (int index = 0; index < this.items.Count; ++index)
    {
      this.items[index].Clear();
      this.pools.Release(this.items[index].gameObject);
    }
    this.items.Clear();
  }

  private AllianceBuildingItem GetItem()
  {
    GameObject go = this.pools.AddChild(this.gridTab2.gameObject);
    AllianceBuildingItem component = go.GetComponent<AllianceBuildingItem>();
    NGUITools.SetActive(go, true);
    return component;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public Coordinate targetLocation;
  }
}
