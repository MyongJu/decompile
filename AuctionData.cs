﻿// Decompiled with JetBrains decompiler
// Type: AuctionData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class AuctionData
{
  private long _returnGoldAmount;
  private AuctionHelper.AuctionType _auctionType;
  private List<AuctionItemInfo> _auctionItemInfoList;

  public AuctionData(AuctionHelper.AuctionType auctionType, long returnGoldAmount, List<AuctionItemInfo> auctionItemInfoList)
  {
    this._auctionType = auctionType;
    this._returnGoldAmount = returnGoldAmount;
    this._auctionItemInfoList = auctionItemInfoList;
  }

  public long ReturnGoldAmount
  {
    get
    {
      return this._returnGoldAmount;
    }
    set
    {
      this._returnGoldAmount = value;
    }
  }

  public AuctionHelper.AuctionType AuctionType
  {
    get
    {
      return this._auctionType;
    }
  }

  public List<AuctionItemInfo> AuctionItemInfoList
  {
    get
    {
      if (this._auctionItemInfoList == null)
        this._auctionItemInfoList = new List<AuctionItemInfo>();
      return this._auctionItemInfoList;
    }
  }
}
