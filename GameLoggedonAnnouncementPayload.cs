﻿// Decompiled with JetBrains decompiler
// Type: GameLoggedonAnnouncementPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class GameLoggedonAnnouncementPayload
{
  private List<AnnouncementData> _announcementDataList = new List<AnnouncementData>();
  private static GameLoggedonAnnouncementPayload _instance;

  public static GameLoggedonAnnouncementPayload Instance
  {
    get
    {
      if (GameLoggedonAnnouncementPayload._instance == null)
        GameLoggedonAnnouncementPayload._instance = new GameLoggedonAnnouncementPayload();
      return GameLoggedonAnnouncementPayload._instance;
    }
  }

  public List<AnnouncementData> AnnouncementDataList
  {
    get
    {
      return this._announcementDataList;
    }
  }

  private void Decode(object data)
  {
    if (data == null)
      return;
    ArrayList arrayList = data as ArrayList;
    if (arrayList == null)
      return;
    this._announcementDataList.Clear();
    for (int index = 0; index < arrayList.Count; ++index)
    {
      AnnouncementData announcementData = new AnnouncementData();
      Hashtable data1 = arrayList[index] as Hashtable;
      if (data1 != null)
      {
        announcementData.ParseData(data1);
        this._announcementDataList.Add(announcementData);
      }
    }
  }

  public void RequestAnnouncement(System.Action<bool, object> callback, Hashtable param)
  {
    MessageHub.inst.GetPortByAction("Player:getAnnouncementLogin").SendLoader(param, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data);
      if (callback == null)
        return;
      callback(ret, data);
    }), false, false);
  }
}
