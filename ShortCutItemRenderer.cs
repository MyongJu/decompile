﻿// Decompiled with JetBrains decompiler
// Type: ShortCutItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ShortCutItemRenderer : MonoBehaviour
{
  public Color normalColor = Color.white;
  public Icon item;
  public GameObject useContent;
  public GameObject buyContent;
  public UILabel price;
  public UILabel count;
  private bool init;
  private int item_id;
  private int shop_id;

  public void SetShopItemData(int shopItem)
  {
    this.Clear();
    this.shop_id = shopItem;
    this.item_id = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(this.shop_id).Item_InternalId;
    this.Init();
    this.UpdateUI();
  }

  public void SetItemData(int item_interId)
  {
    this.item_id = item_interId;
    ShopStaticInfo byItemInternalId = ConfigManager.inst.DB_Shop.GetShopInfoByItemInternalId(item_interId);
    this.shop_id = byItemInternalId == null ? 0 : byItemInternalId.internalId;
    this.Init();
    this.UpdateUI();
  }

  private void Init()
  {
    this.AddEventHandler();
  }

  public void Dispose()
  {
    this.Clear();
    this.RemoveEventHandler();
  }

  public void OnUseItem()
  {
    if (!ItemBag.Instance.CheckCanUse(this.item_id))
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_item_requirement_tip", new Dictionary<string, string>()
      {
        {
          "num",
          ConfigManager.inst.DB_Items.GetItem(this.item_id).RequireLev.ToString()
        }
      }, true), (System.Action) null, 4f, false);
    else if (ItemBag.Instance.GetItemCount(this.item_id) == 1)
      ItemBag.Instance.UseItem(this.item_id, 1, (Hashtable) null, (System.Action<bool, object>) null);
    else
      UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) new ItemUseOrBuyPopup.Parameter()
      {
        item_id = this.item_id
      });
  }

  public void OnBuyAndUseItem()
  {
    if (!ItemBag.Instance.CheckCanUse(this.item_id))
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_item_requirement_tip", new Dictionary<string, string>()
      {
        {
          "num",
          ConfigManager.inst.DB_Items.GetItem(this.item_id).RequireLev.ToString()
        }
      }, true), (System.Action) null, 4f, false);
    else if (PlayerData.inst.hostPlayer.Currency < ItemBag.Instance.GetShopItemPrice(this.shop_id))
      Utils.ShowNotEnoughGoldTip();
    else
      UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) new ItemUseOrBuyPopup.Parameter()
      {
        shop_id = this.shop_id
      });
  }

  private void Clear()
  {
    this.item.Dispose();
  }

  private void AddEventHandler()
  {
    if (this.init)
      return;
    this.init = true;
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnDataChange);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnDataChange);
    DBManager.inst.DB_User.onDataChanged += new System.Action<long>(this.OnUerDataChange);
  }

  private void UpdateUI()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.item_id);
    this.item.FeedData((IComponentData) new IconData(itemStaticInfo.ImagePath, new string[2]
    {
      itemStaticInfo.LocName,
      itemStaticInfo.LocDescription
    })
    {
      Data = (object) this.item_id
    });
    this.Refresh();
  }

  private void OnUerDataChange(long user_id)
  {
    if (user_id != PlayerData.inst.uid)
      return;
    this.Refresh();
  }

  private void Refresh()
  {
    int itemCount = ItemBag.Instance.GetItemCount(this.item_id);
    NGUITools.SetActive(this.useContent, false);
    NGUITools.SetActive(this.buyContent, false);
    this.count.text = itemCount.ToString();
    int num = 0;
    if (this.shop_id > 0)
      num = ItemBag.Instance.GetShopItemPrice(this.shop_id);
    this.price.text = num.ToString();
    if (PlayerData.inst.hostPlayer.Currency > num)
      this.price.color = this.normalColor;
    else
      this.price.color = Color.red;
    NGUITools.SetActive(this.price.gameObject, num > 0);
    if (itemCount <= 0)
      NGUITools.SetActive(this.buyContent, this.shop_id > 0);
    else
      NGUITools.SetActive(this.useContent, true);
  }

  private void OnDataChange(int item_id)
  {
    if (this.item_id != item_id)
      return;
    this.Refresh();
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnDataChange);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnDataChange);
    DBManager.inst.DB_User.onDataChanged -= new System.Action<long>(this.OnUerDataChange);
    this.init = false;
  }
}
