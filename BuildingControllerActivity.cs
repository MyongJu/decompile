﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerActivity
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingControllerActivity : BuildingControllerNew
{
  public int UPDATE_TIME_DELAY = 120;
  public const int SHOW_TIME = 5;
  public Transform activityTextContainer;
  public UILabel activityText;
  public GameObject newFlag;
  public UILabel title;
  private int _updateTime;
  private bool __isShow;

  private void OnEnable()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  private void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public override void InitBuilding()
  {
    base.InitBuilding();
    this.onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    this.activityText.text = string.Empty;
    this._isShow = false;
    if (!((UnityEngine.Object) this.title != (UnityEngine.Object) null))
      return;
    this.title.text = ScriptLocalization.Get("event_center_name", true);
  }

  public override void Dispose()
  {
    base.Dispose();
    this.onBuildingSelected -= new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public override void AddActionButton(ref List<CityCircleBtnPara> ftl)
  {
    base.AddActionButton(ref ftl);
    ftl.Add(this.CCBPair["sign"]);
  }

  private void OnBuildingSelected()
  {
    if (!Application.isEditor)
      FunplusBi.Instance.TraceEvent("clickbuilding", JsonWriter.Serialize((object) new BuildingControllerNew.BIFormat()
      {
        d_c1 = new BuildingControllerNew.BIClick()
        {
          key = "Activity",
          value = "click"
        }
      }));
    ActivityManager.Intance.LoadTimeLimitActivityData(new System.Action(this.LoadDataCallBack));
  }

  private void LoadDataCallBack()
  {
    AudioManager.Instance.PlaySound("sfx_city_click_event_center", false);
    if (ActivityManager.Intance.isActivityExist)
      UIManager.inst.OpenDlg("Activity/ActivityMainDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    else
      UIManager.inst.toast.Show(ScriptLocalization.Get("event_not_started_description", true), (System.Action) null, 4f, false);
  }

  public void OnShowActivity()
  {
    ActivityManager.Intance.LoadTimeLimitActivityData(new System.Action(this.LoadDataCallBack));
  }

  public void OnSecond(int serverTime)
  {
    if (serverTime - this._updateTime > this.UPDATE_TIME_DELAY)
    {
      this._updateTime = serverTime;
      this.UPDATE_TIME_DELAY = (int) ((double) UnityEngine.Random.value * 60.0 + 120.0);
    }
    if ((UnityEngine.Object) this.newFlag != (UnityEngine.Object) null)
      this.newFlag.SetActive(ActivityManager.Intance.HasNewActivty);
    if (serverTime - this._updateTime <= 5)
      this.ShowBoard();
    else
      this.HideBoard();
  }

  private bool _isShow
  {
    get
    {
      return this.__isShow;
    }
    set
    {
      this.__isShow = value;
      this.activityTextContainer.gameObject.SetActive(value);
    }
  }

  public void ShowBoard()
  {
    if (this._isShow)
      return;
    RoundActivityData timeLimitActivity = ActivityManager.Intance.CurrentTimeLimitActivity;
    if (timeLimitActivity == null)
      return;
    this._isShow = true;
    ActivityMainInfo data = ConfigManager.inst.DB_ActivityMain.GetData(ConfigManager.inst.DB_ActivityMainSub.GetData(timeLimitActivity.CurrentRound.ActivitySubConfigID).ActivityMainID);
    if (NetServerTime.inst.ServerTimestamp < timeLimitActivity.StartTime)
    {
      this.activityText.text = ScriptLocalization.GetWithPara("event_scrolling_event_time", new Dictionary<string, string>()
      {
        {
          "1",
          data.LocalName
        },
        {
          "2",
          Utils.FormatTime1(timeLimitActivity.StartTime - NetServerTime.inst.ServerTimestamp)
        },
        {
          "3",
          Utils.XLAT("event_scrolling_begin")
        }
      }, true);
    }
    else
    {
      if (NetServerTime.inst.ServerTimestamp >= timeLimitActivity.EndTime)
        return;
      this.activityText.text = ScriptLocalization.GetWithPara("event_scrolling_event_time", new Dictionary<string, string>()
      {
        {
          "1",
          data.LocalName
        },
        {
          "2",
          Utils.FormatTime1(timeLimitActivity.EndTime - NetServerTime.inst.ServerTimestamp)
        },
        {
          "3",
          Utils.XLAT("event_scrolling_finish")
        }
      }, true);
    }
  }

  public void HideBoard()
  {
    if (!this._isShow)
      return;
    this._isShow = false;
  }
}
