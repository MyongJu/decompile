﻿// Decompiled with JetBrains decompiler
// Type: GuardStateUnderAttackMoral
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardStateUnderAttackMoral : AbstractGuardState
{
  private Coroutine _delayCoroutine;

  public GuardStateUnderAttackMoral(RoundPlayer player)
    : base(player)
  {
  }

  public override string Key
  {
    get
    {
      return "guard_under_attack";
    }
  }

  public override Hashtable Data { get; set; }

  public override void OnEnter()
  {
    DragonKnightSystem.Instance.Controller.RoomHud.SamllShakeRoom();
    RoundPlayerMoral player = this.Player as RoundPlayerMoral;
    player.PlayerVFX("Prefab/DragonKnight/Objects/VFX/fx_moral_under_attack");
    this.PlaySkillVFX();
    this._delayCoroutine = Utils.ExecuteInSecs(1f, (System.Action) (() =>
    {
      player.StopVFX();
      this._delayCoroutine = (Coroutine) null;
      if (this.Player.Health <= 0)
        this.Player.StateMacine.SetState("guard_death", (Hashtable) null);
      else
        this.Player.StateMacine.SetState("guard_idle", (Hashtable) null);
      BattleManager.Instance.SendPacket((DragonKnightPacket) new DragonKnightPacketFinish(this.Player.PlayerId));
    }));
  }

  public override void OnProcess()
  {
  }

  public override void OnExit()
  {
  }

  public override void Dispose()
  {
    (this.Player as RoundPlayerMoral).StopVFX();
    if (this._delayCoroutine == null)
      return;
    Utils.StopCoroutine(this._delayCoroutine);
    this._delayCoroutine = (Coroutine) null;
  }

  private void PlaySkillVFX()
  {
    RoundPlayer trigger = BattleManager.Instance.Trigger;
    if (trigger == null || !(bool) ((UnityEngine.Object) trigger.Animation) || !(bool) ((UnityEngine.Object) trigger.Animation.gameObject))
      return;
    DragonKnightTalentInfo knightTalentInfo = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(trigger.SkillId);
    if (knightTalentInfo == null)
      return;
    TextAsset script = AssetManager.Instance.HandyLoad("Prefab/DragonKnight/Objects/UnderAttack/" + knightTalentInfo.ID, (System.Type) null) as TextAsset;
    if (!(bool) ((UnityEngine.Object) script))
      return;
    Dictionary<string, GuardAction>.Enumerator enumerator = GuardEventDataParser.ParseActions(DragonKnightSystem.Instance.Controller.BattleScene.gameObject, script).GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Value.Process(this.Player);
  }
}
