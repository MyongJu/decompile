﻿// Decompiled with JetBrains decompiler
// Type: ConfigHeroProfile
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigHeroProfile
{
  private List<HeroProfileInfo> _heroProfileInfoList = new List<HeroProfileInfo>();
  private Dictionary<string, HeroProfileInfo> _datas;
  private Dictionary<int, HeroProfileInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<HeroProfileInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, HeroProfileInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._heroProfileInfoList.Add(enumerator.Current);
    }
    this._heroProfileInfoList.Sort((Comparison<HeroProfileInfo>) ((a, b) => a.internalId.CompareTo(b.internalId)));
  }

  public void Clear()
  {
    if (this._datas != null)
    {
      this._datas.Clear();
      this._datas = (Dictionary<string, HeroProfileInfo>) null;
    }
    if (this._dicByUniqueId == null)
      return;
    this._dicByUniqueId.Clear();
    this._dicByUniqueId = (Dictionary<int, HeroProfileInfo>) null;
  }

  public List<HeroProfileInfo> GetHeroProfileInfoList()
  {
    return this._heroProfileInfoList;
  }

  public HeroProfileInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (HeroProfileInfo) null;
  }

  public HeroProfileInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (HeroProfileInfo) null;
  }

  public HeroProfileInfo GetByImageIndex(int index)
  {
    Dictionary<string, HeroProfileInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (index == enumerator.Current.imageIndex)
        return enumerator.Current;
    }
    return (HeroProfileInfo) null;
  }
}
