﻿// Decompiled with JetBrains decompiler
// Type: HeroRecruitSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HeroRecruitSlot : MonoBehaviour
{
  public UILabel m_TipLabel;
  public UILabel m_TimerLabel;
  public UILabel m_PriceLabel;
  public UIButton m_BuyButton;
  public UIButton m_FreeButton;
  public GameObject m_Lock;
  public GameObject m_Unlock;
  public Color m_CurrencyColor;
  public UITexture m_Background;
  protected HeroRecruitData m_HeroRecruitData;

  public virtual void UpdateUI(HeroRecruitData data)
  {
    this.m_HeroRecruitData = data;
    this.m_Lock.SetActive(data.IsLocked);
    this.m_Unlock.SetActive(!data.IsLocked);
    this.m_TipLabel.text = data.IsFirstUsed() ? data.GetTip2() : data.GetTip();
    this.UpdateCD();
    this.m_PriceLabel.text = data.GetPrice().ToString();
    this.m_PriceLabel.color = data.GetPrice() > data.GetCurrency() ? Color.red : this.m_CurrencyColor;
    this.m_BuyButton.gameObject.SetActive(!data.IsFreeNow());
    this.m_FreeButton.gameObject.SetActive(data.IsFreeNow());
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Background, data.GetImagePath(), (System.Action<bool>) null, true, true, string.Empty);
  }

  public void OnUnlock()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  public void OnFreeRecruit()
  {
    HeroRecruitUtils.Summon(this.m_HeroRecruitData.GetSummonType(), true, (System.Action<bool, HeroRecruitPayload>) ((ret, payload) =>
    {
      if (!ret)
        return;
      this.UpdateUI(this.m_HeroRecruitData);
      this.m_HeroRecruitData.GotoResultDialog(payload);
    }));
  }

  public void OnPayRecruit()
  {
    if (this.m_HeroRecruitData.GetPrice() > this.m_HeroRecruitData.GetCurrency())
      this.m_HeroRecruitData.ShowStore((int) this.m_HeroRecruitData.GetPrice());
    else
      HeroRecruitUtils.Summon(this.m_HeroRecruitData.GetSummonType(), false, (System.Action<bool, HeroRecruitPayload>) ((ret, payload) =>
      {
        if (!ret)
          return;
        this.UpdateUI(this.m_HeroRecruitData);
        this.m_HeroRecruitData.GotoResultDialog(payload);
      }));
  }

  private void UpdateCD()
  {
    this.m_TimerLabel.text = string.Format(Utils.XLAT("hero_summon_num_after_free"), (object) Utils.FormatTime(this.m_HeroRecruitData.GetLeftCD(), false, false, true));
    this.m_TimerLabel.gameObject.SetActive(this.m_HeroRecruitData.CanShowTimer());
  }

  private void Update()
  {
    this.UpdateCD();
  }
}
