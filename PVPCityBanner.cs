﻿// Decompiled with JetBrains decompiler
// Type: PVPCityBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class PVPCityBanner : SpriteBanner
{
  private static Color playerColor = new Color(1f, 1f, 0.6862745f, 1f);
  private static Color enemyColor = Color.red;
  private static Color allyColor = new Color(0.3686275f, 0.9764706f, 0.9764706f, 1f);
  private static Color neutralColor = Color.white;
  protected SpriteCreator m_spriteCreator = new SpriteCreator();
  protected List<PVPCityBanner.IconRefreshData> m_allIconRefreshData = new List<PVPCityBanner.IconRefreshData>();
  public UITexture m_BackgroundLordTitle;
  public PVPAllianceSymbol m_Symbol;
  public GameObject m_Knife;
  public SpriteRenderer m_titleIcon;
  public SpriteRenderer m_titleBackground;
  public GameObject m_titleRoot;
  public GameObject m_kingTitleEffetRoot;
  public GameObject m_positiveTitleEffectRoot;
  public GameObject m_negativeTitleEffectRoot;
  public GameObject m_artifactEffectRoot;
  public Transform m_allianceBuffIconRoot;
  public Transform m_buffPosition;
  public GameObject m_leftProtectTimeRoot;
  public TextMesh m_textLeftProtectTime;
  public SpriteRenderer m_artifactBackground;
  public GameObject m_scoutFlagRoot;
  public GameObject m_allianceWarRoot;
  protected int m_iconExchangeIndex;
  protected bool m_needRefreshLeftProtectTime;
  protected bool m_UpdateAnchor;
  protected TileData m_tileData;

  public void OnEnable()
  {
    Oscillator.Instance.threeSecondEvent += new System.Action(this.OnTimerExchangeIcon);
  }

  public void OnDisable()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.threeSecondEvent -= new System.Action(this.OnTimerExchangeIcon);
    this.m_spriteCreator.DestroyAllCreated();
  }

  protected void OnTimerExchangeIcon()
  {
    ++this.m_iconExchangeIndex;
    this.RefreshIcon();
    this.RefreshScoutStatus();
  }

  protected void RefreshLeftProtectTime(int delta)
  {
    if (!this.m_needRefreshLeftProtectTime || this.m_iconExchangeIndex >= this.m_allIconRefreshData.Count)
      return;
    PVPCityBanner.IconRefreshData iconRefreshData = this.m_allIconRefreshData[this.m_iconExchangeIndex];
    if (iconRefreshData.EffectType != PVPCityBanner.EffectType.Artifact)
      return;
    if (iconRefreshData.ArtifactData.ProtectedLeftTime > 0)
      this.m_textLeftProtectTime.text = Utils.FormatTime(iconRefreshData.ArtifactData.ProtectedLeftTime, false, false, true);
    else
      this.m_leftProtectTimeRoot.SetActive(false);
  }

  protected void RefreshIcon()
  {
    this.m_needRefreshLeftProtectTime = false;
    if (this.m_allIconRefreshData.Count <= 0)
      return;
    if (this.m_iconExchangeIndex >= this.m_allIconRefreshData.Count)
      this.m_iconExchangeIndex = 0;
    PVPCityBanner.IconRefreshData iconRefreshData = this.m_allIconRefreshData[this.m_iconExchangeIndex];
    this.m_titleIcon.sprite = this.m_spriteCreator.CreateSprite(iconRefreshData.SpriteName);
    bool flag1 = false;
    bool flag2 = false;
    bool flag3 = false;
    bool flag4 = false;
    bool flag5 = false;
    if (iconRefreshData.EffectType == PVPCityBanner.EffectType.TitleKing)
      flag1 = true;
    if (iconRefreshData.EffectType == PVPCityBanner.EffectType.TitlePositive)
    {
      flag2 = true;
      flag4 = true;
    }
    if (iconRefreshData.EffectType == PVPCityBanner.EffectType.TitleNegative)
    {
      flag3 = true;
      flag4 = true;
    }
    if (iconRefreshData.EffectType == PVPCityBanner.EffectType.Artifact)
      flag5 = true;
    this.m_kingTitleEffetRoot.SetActive(flag1);
    this.m_titleBackground.gameObject.SetActive(flag4);
    this.m_negativeTitleEffectRoot.SetActive(flag3);
    this.m_positiveTitleEffectRoot.SetActive(flag2);
    if ((bool) ((UnityEngine.Object) this.m_artifactEffectRoot))
    {
      this.m_artifactEffectRoot.SetActive(flag5);
      this.m_artifactBackground.sprite = this.m_spriteCreator.CreateSprite("Texture/Equipment/frame_equipment_5");
    }
    if (flag4)
      this.m_titleBackground.sprite = this.m_spriteCreator.CreateSprite(iconRefreshData.BGSprite);
    if (iconRefreshData.EffectType == PVPCityBanner.EffectType.Artifact && iconRefreshData.ArtifactData != null && iconRefreshData.ArtifactData.ProtectedLeftTime > 0)
    {
      this.m_needRefreshLeftProtectTime = true;
      this.m_leftProtectTimeRoot.SetActive(true);
      this.m_textLeftProtectTime.text = Utils.XLAT("id_protected");
    }
    else
      this.m_leftProtectTimeRoot.SetActive(false);
  }

  public override void UpdateUI(TileData tile)
  {
    base.UpdateUI(tile);
    this.m_tileData = tile;
    this.m_allIconRefreshData.Clear();
    if (tile.TileType != TileType.City)
      return;
    string str1 = string.Empty;
    string ownerName = tile.OwnerName;
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(tile.AllianceID);
    if (allianceData != null)
    {
      str1 = string.Format("({0})", (object) allianceData.allianceAcronym);
      this.m_Symbol.gameObject.SetActive(true);
      this.m_Symbol.SetSymbols(allianceData.allianceSymbolCode);
    }
    else
      this.m_Symbol.gameObject.SetActive(false);
    if (tile.ScoutData != null && tile.ScoutData.HasAvailableScoutReport)
      NGUITools.SetActive(this.m_scoutFlagRoot, true);
    else
      NGUITools.SetActive(this.m_scoutFlagRoot, false);
    CityData cityData = DBManager.inst.DB_City.Get(tile.CityID);
    if (cityData != null)
      this.Level = cityData.buildings.level_stronghold.ToString();
    string empty = string.Empty;
    string str2 = tile.OwnerID != PlayerData.inst.uid ? str1 + ownerName : Utils.XLAT("id_my_city");
    this.m_UpdateAnchor = true;
    bool flag1 = false;
    bool flag2 = false;
    UserData userData = tile.UserData;
    if (userData != null)
    {
      if (userData.Title != 0)
      {
        WonderTitleInfo wonderTitleInfo = ConfigManager.inst.DB_WonderTitle.Get(userData.Title.ToString());
        bool flag3 = wonderTitleInfo != null && wonderTitleInfo.titleType == 0;
        if (userData.Title == 1)
          this.m_allIconRefreshData.Add(new PVPCityBanner.IconRefreshData()
          {
            SpriteName = userData.TitleIconPath,
            EffectType = PVPCityBanner.EffectType.TitleKing
          });
        else if (flag3)
          this.m_allIconRefreshData.Add(new PVPCityBanner.IconRefreshData()
          {
            SpriteName = userData.TitleIconPath,
            EffectType = PVPCityBanner.EffectType.TitlePositive,
            BGSprite = userData.TitleBgPath
          });
        else
          this.m_allIconRefreshData.Add(new PVPCityBanner.IconRefreshData()
          {
            SpriteName = userData.TitleIconPath,
            EffectType = PVPCityBanner.EffectType.TitleNegative,
            BGSprite = userData.TitleBgPath
          });
        flag1 = true;
      }
      if (userData.IsForeigner && tile.OwnerID != PlayerData.inst.uid || userData.IsPit || AllianceWarPayload.Instance.IsPlayerCurrentJoined(0L) && PlayerData.inst.userData.AcGroupId == userData.AcGroupId && PlayerData.inst.userData.AcAllianceId != userData.AcAllianceId)
        str2 = str2 + ".k" + (object) userData.world_id;
    }
    this.Name = str2;
    this.m_Knife.SetActive(userData != null && (userData.IsForeigner || userData.IsPit));
    NGUITools.SetActive(this.m_Background.gameObject, false);
    NGUITools.SetActive(this.m_BackgroundLordTitle.gameObject, false);
    if (userData != null)
    {
      string textureImagePath = string.Empty;
      bool flag3 = false;
      LordTitleMainInfo lordTitleMainInfo = ConfigManager.inst.DB_LordTitleMain.Get(userData.LordTitle);
      if (lordTitleMainInfo != null)
      {
        flag3 = BundleManager.Instance.IsFileCached("Texture/LordTitle/" + lordTitleMainInfo.NameBoardIcon);
        textureImagePath = lordTitleMainInfo.BoardIconPath;
      }
      if (userData.LordTitle > 0 && flag3)
      {
        NGUITools.SetActive(this.m_BackgroundLordTitle.gameObject, true);
        MarksmanPayload.Instance.FeedMarksmanTexture(this.m_BackgroundLordTitle, textureImagePath);
      }
      else
        NGUITools.SetActive(this.m_Background.gameObject, true);
    }
    if (cityData != null && userData != null)
    {
      using (List<int>.Enumerator enumerator = cityData.Artifacts.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          int current = enumerator.Current;
          ArtifactData artifactData = DBManager.inst.DB_Artifact.Get(userData.world_id, current);
          if (artifactData != null)
          {
            ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(artifactData.ArtifactId);
            if (artifactInfo != null && artifactInfo.position != 1)
            {
              flag2 = true;
              this.m_allIconRefreshData.Add(new PVPCityBanner.IconRefreshData()
              {
                SpriteName = artifactInfo.ImagePath,
                EffectType = PVPCityBanner.EffectType.Artifact,
                ArtifactData = artifactData
              });
            }
          }
        }
      }
    }
    this.RefreshIcon();
    this.m_titleRoot.SetActive(flag1 || flag2);
    this.m_allianceBuffIconRoot.position = flag1 || flag2 ? this.m_buffPosition.position : this.m_titleIcon.transform.position;
    this.UpdateNameColor(tile);
    this.UpdateAllianceWarFlag(tile);
  }

  private void Update()
  {
    if (!this.m_UpdateAnchor)
      return;
    this.m_UpdateAnchor = false;
    this.UpdateSymbolAndKnifePosition();
  }

  private void RefreshScoutStatus()
  {
    if (this.m_tileData != null)
    {
      if (this.m_tileData.ScoutData != null && this.m_tileData.ScoutData.HasAvailableScoutReport)
        NGUITools.SetActive(this.m_scoutFlagRoot, true);
      else
        NGUITools.SetActive(this.m_scoutFlagRoot, false);
    }
    this.UpdateSymbolAndKnifePosition();
  }

  private void UpdateSymbolAndKnifePosition()
  {
    if (!(bool) ((UnityEngine.Object) this.m_NameRenderer))
      return;
    Bounds bounds = this.m_NameRenderer.bounds;
    Vector3 vector3_1 = this.m_NameLabel.transform.InverseTransformPoint(new Vector3(bounds.min.x, bounds.center.y, bounds.center.z));
    vector3_1.x -= 30f;
    if ((bool) ((UnityEngine.Object) this.m_scoutFlagRoot))
    {
      if (this.m_scoutFlagRoot.activeInHierarchy)
      {
        this.m_scoutFlagRoot.transform.localPosition = vector3_1;
        vector3_1.x -= 65f;
      }
      else
        vector3_1.x -= 20f;
    }
    this.m_Symbol.transform.localPosition = vector3_1;
    Vector3 vector3_2 = this.m_NameLabel.transform.InverseTransformPoint(new Vector3(bounds.max.x, bounds.center.y, bounds.center.z));
    vector3_2.x += 50f;
    this.m_Knife.transform.localPosition = vector3_2;
  }

  private void CheckOtherAllianceNameColor(TileData tile)
  {
    UserData userData = tile.UserData;
    if (userData != null && userData.IsForeigner)
    {
      if (PlayerData.inst.userData.AcGroupId > 0 && userData.AcGroupId > 0 && PlayerData.inst.userData.AcGroupId != userData.AcGroupId)
        this.m_NameLabel.color = PVPCityBanner.neutralColor;
      else
        this.m_NameLabel.color = PVPCityBanner.enemyColor;
    }
    else if (AllianceWarPayload.Instance.CanAttackTargetTile(tile))
      this.m_NameLabel.color = PVPCityBanner.enemyColor;
    else
      this.m_NameLabel.color = PVPCityBanner.neutralColor;
  }

  private void UpdateNameColor(TileData tile)
  {
    if (tile.OwnerID == PlayerData.inst.uid)
    {
      this.m_NameLabel.color = PVPCityBanner.playerColor;
    }
    else
    {
      if (PlayerData.inst.allianceData != null)
      {
        if (tile.AllianceID == PlayerData.inst.allianceId)
          this.m_NameLabel.color = PVPCityBanner.allyColor;
        else
          this.CheckOtherAllianceNameColor(tile);
      }
      else
        this.CheckOtherAllianceNameColor(tile);
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode || tile.UserData == null || (!tile.UserData.IsForeigner || tile.Location.K != PlayerData.inst.userData.world_id) || (tile.UserData.AcGroupId <= 0 || tile.UserData.AcGroupId == PlayerData.inst.userData.AcGroupId))
        return;
      this.m_NameLabel.color = PVPCityBanner.neutralColor;
      NGUITools.SetActive(this.m_Knife, false);
    }
  }

  private void UpdateAllianceWarFlag(TileData tile)
  {
    NGUITools.SetActive(this.m_allianceWarRoot, false);
    if (tile.UserData == null || tile.UserData.AcGroupId <= 0 || tile.UserData.AcGroupId != PlayerData.inst.userData.AcGroupId)
      return;
    NGUITools.SetActive(this.m_allianceWarRoot, true);
    NGUITools.SetActive(this.m_Knife, false);
  }

  protected enum EffectType
  {
    None,
    TitleKing,
    TitleNegative,
    TitlePositive,
    Artifact,
  }

  protected class IconRefreshData
  {
    public PVPCityBanner.EffectType EffectType;
    public string SpriteName;
    public string BGSprite;
    public ArtifactData ArtifactData;
  }
}
