﻿// Decompiled with JetBrains decompiler
// Type: TradingHallData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class TradingHallData
{
  private List<TradingHallData.BuyGoodData> _buyGoodDataList = new List<TradingHallData.BuyGoodData>();
  private List<TradingHallData.SellGoodData> _sellGoodDataList = new List<TradingHallData.SellGoodData>();

  public bool HasThisGood2Buy(int exchangeId)
  {
    TradingHallData.BuyGoodData buyGoodData = this.BuyGoodDataList.Find((Predicate<TradingHallData.BuyGoodData>) (x => x.ExchangeId == exchangeId));
    return buyGoodData != null && buyGoodData.ExchangeId > 0 && buyGoodData.ItemTotalCount > 0L;
  }

  public bool HasThisGoodInBuyAvailable(int exchangeId)
  {
    TradingHallData.BuyGoodData buyGoodData = this.BuyGoodDataList.Find((Predicate<TradingHallData.BuyGoodData>) (x =>
    {
      if (x.ExchangeId == exchangeId)
        return x.ThisGoodAvailable;
      return false;
    }));
    return buyGoodData != null && buyGoodData.ExchangeId > 0 && buyGoodData.ItemTotalCount > 0L;
  }

  public bool HasThisGoodAvailable(int exchangeId)
  {
    return new TradingHallData.BuyGoodData(exchangeId).ThisGoodAvailable;
  }

  public List<TradingHallData.BuyGoodData> BuyGoodDataList
  {
    get
    {
      return this._buyGoodDataList;
    }
  }

  public List<TradingHallData.SellGoodData> SellGoodDataList
  {
    get
    {
      return this._sellGoodDataList;
    }
  }

  public void Decode(ArrayList data, TradingHallData.ExchangeType type)
  {
    if (data == null)
    {
      if (type == TradingHallData.ExchangeType.BUY)
      {
        this._buyGoodDataList.Clear();
      }
      else
      {
        if (type != TradingHallData.ExchangeType.SELL)
          return;
        this._sellGoodDataList.Clear();
      }
    }
    else
    {
      switch (type)
      {
        case TradingHallData.ExchangeType.BUY:
          this._buyGoodDataList.Clear();
          for (int index = 0; index < data.Count; ++index)
            this._buyGoodDataList.Add(new TradingHallData.BuyGoodData(data[index] as Hashtable));
          break;
        case TradingHallData.ExchangeType.SELL:
          this._sellGoodDataList.Clear();
          for (int index = 0; index < data.Count; ++index)
            this._sellGoodDataList.Add(new TradingHallData.SellGoodData(data[index] as Hashtable));
          break;
      }
    }
  }

  public enum ExchangeType
  {
    BUY,
    SELL,
  }

  public class BuyGoodData
  {
    private List<KeyValuePair<int, int>> _itemSaleUnitPrices = new List<KeyValuePair<int, int>>();
    private int _exchangeId;
    private int _itemId;
    private long _itemTotalCount;
    private int _itemAveragePrice;

    public BuyGoodData(int exchangeId)
    {
      this._exchangeId = exchangeId;
      ExchangeHallInfo exchangeHallInfo = ConfigManager.inst.DB_ExchangeHall.Get(this._exchangeId);
      if (exchangeHallInfo == null)
        return;
      this._itemId = exchangeHallInfo.itemId;
    }

    public BuyGoodData(Hashtable data)
    {
      if (data == null)
        return;
      DatabaseTools.UpdateData(data, "exchange_id", ref this._exchangeId);
      DatabaseTools.UpdateData(data, "average_price", ref this._itemAveragePrice);
      if (data.ContainsKey((object) "price_and_quantity"))
      {
        this._itemSaleUnitPrices.Clear();
        Hashtable hashtable = data[(object) "price_and_quantity"] as Hashtable;
        if (hashtable != null)
        {
          foreach (object key in (IEnumerable) hashtable.Keys)
          {
            int result1 = 0;
            int result2 = 0;
            int.TryParse(key.ToString(), out result1);
            int.TryParse(hashtable[(object) result1.ToString()].ToString(), out result2);
            this._itemSaleUnitPrices.Add(new KeyValuePair<int, int>(result1, result2));
          }
        }
      }
      this._itemTotalCount = 0L;
      for (int index = 0; index < this._itemSaleUnitPrices.Count; ++index)
        this._itemTotalCount += (long) this._itemSaleUnitPrices[index].Value;
      ExchangeHallInfo exchangeHallInfo = ConfigManager.inst.DB_ExchangeHall.Get(this._exchangeId);
      if (exchangeHallInfo == null)
        return;
      this._itemId = exchangeHallInfo.itemId;
    }

    public int ExchangeId
    {
      get
      {
        return this._exchangeId;
      }
    }

    public int ItemId
    {
      get
      {
        return this._itemId;
      }
    }

    public long ItemTotalCount
    {
      get
      {
        return this._itemTotalCount;
      }
    }

    public int ItemAveragePrice
    {
      get
      {
        return this._itemAveragePrice;
      }
    }

    public int EquipmentId
    {
      get
      {
        return TradingHallPayload.Instance.GetEquipmentId(this._exchangeId, this._itemId);
      }
    }

    public int EquipmentEnhanced
    {
      get
      {
        return TradingHallPayload.Instance.GetEquipmentEnhanced(this._exchangeId);
      }
    }

    public bool ThisGoodAvailable
    {
      get
      {
        BagType bagType = TradingHallPayload.Instance.GetBagType(this._exchangeId);
        switch (bagType)
        {
          case BagType.Hero:
          case BagType.DragonKnight:
            ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(this.EquipmentId);
            return data != null && data.heroLevelMin <= PlayerData.inst.heroData.level;
          default:
            return ItemBag.Instance.CheckCanUse(this._itemId);
        }
      }
    }

    public List<KeyValuePair<int, int>> ItemSaleUnitPrices
    {
      get
      {
        return this._itemSaleUnitPrices;
      }
    }
  }

  public class SellGoodData
  {
    private int _exchangeId;
    private int _itemId;
    private int _equipmentId;
    private int _price;
    private int _amount;
    private int _validTime;
    private int _cooldownTime;

    public SellGoodData(int itemId)
    {
      this._itemId = itemId;
    }

    public SellGoodData(int exchangeId, int itemId, int price, int amount, int equipmentId = 0, int cooldownTime = 0)
    {
      this._exchangeId = exchangeId;
      this._itemId = itemId;
      this._price = price;
      this._amount = amount;
      this._equipmentId = equipmentId;
      this._cooldownTime = cooldownTime;
    }

    public SellGoodData(Hashtable data)
    {
      if (data == null)
        return;
      DatabaseTools.UpdateData(data, "exchange_id", ref this._exchangeId);
      DatabaseTools.UpdateData(data, "equipment_id", ref this._equipmentId);
      DatabaseTools.UpdateData(data, "price", ref this._price);
      DatabaseTools.UpdateData(data, "quantity", ref this._amount);
      DatabaseTools.UpdateData(data, "ext_time", ref this._validTime);
      ExchangeHallInfo exchangeHallInfo = ConfigManager.inst.DB_ExchangeHall.Get(this._exchangeId);
      if (exchangeHallInfo == null)
        return;
      this._itemId = exchangeHallInfo.itemId;
    }

    public int ExchangeId
    {
      get
      {
        return this._exchangeId;
      }
    }

    public int ItemId
    {
      get
      {
        return this._itemId;
      }
    }

    public int EquipmentId
    {
      get
      {
        return this._equipmentId;
      }
    }

    public int EquipmentEnhanced
    {
      get
      {
        return TradingHallPayload.Instance.GetEquipmentEnhanced(this._exchangeId);
      }
    }

    public int Price
    {
      get
      {
        return this._price;
      }
    }

    public int Amount
    {
      get
      {
        return this._amount;
      }
    }

    public int ValidTime
    {
      get
      {
        return this._validTime;
      }
    }

    public bool IsValid
    {
      get
      {
        return NetServerTime.inst.ServerTimestamp <= this._validTime;
      }
    }

    public int CooldownTime
    {
      get
      {
        return this._cooldownTime;
      }
    }

    public bool IsCooldown
    {
      get
      {
        return NetServerTime.inst.ServerTimestamp <= this._cooldownTime;
      }
    }
  }
}
