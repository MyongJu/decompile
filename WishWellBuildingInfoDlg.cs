﻿// Decompiled with JetBrains decompiler
// Type: WishWellBuildingInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;

public class WishWellBuildingInfoDlg : BuildingInfoBaseDlg
{
  public UITexture texture1;
  public UILabel nameLabel1;
  public UILabel valueLabel1;
  public UITexture texture2;
  public UILabel nameLabel2;
  public UILabel valueLabel2;
  public UITexture texture3;
  public UILabel nameLabel3;
  public UILabel valueLabel3;
  public UITexture texture4;
  public UILabel nameLabel4;
  public UILabel valueLabel4;
  public UITexture texture5;
  public UILabel nameLabel5;
  public UILabel valueLabel5;
  public UIScrollView scrollView;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    this.nameLabel1.text = ScriptLocalization.Get("wish_well_food", true);
    this.nameLabel2.text = ScriptLocalization.Get("wish_well_wood", true);
    this.nameLabel3.text = ScriptLocalization.Get("wish_well_ore", true);
    this.nameLabel4.text = ScriptLocalization.Get("wish_well_silver", true);
    this.nameLabel5.text = ScriptLocalization.Get("wish_well_free_wishes", true);
    WishWellData wishWellDataByLevel = ConfigManager.inst.DB_WishWell.GetWishWellDataByLevel(this.buildingInfo.Building_Lvl);
    this.valueLabel1.text = wishWellDataByLevel.Food.ToString();
    this.valueLabel2.text = wishWellDataByLevel.Wood.ToString();
    this.valueLabel3.text = wishWellDataByLevel.Ore.ToString();
    this.valueLabel4.text = wishWellDataByLevel.Silver.ToString();
    this.valueLabel5.text = wishWellDataByLevel.FreeGem.ToString();
    this.scrollView.ResetPosition();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    BuilderFactory.Instance.Release((UIWidget) this.texture1);
    base.OnClose(orgParam);
  }
}
