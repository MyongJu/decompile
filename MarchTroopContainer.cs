﻿// Decompiled with JetBrains decompiler
// Type: MarchTroopContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UnityEngine;

public class MarchTroopContainer : MonoBehaviour
{
  private TroopType troopType = TroopType.invalid;
  public bool useTileData = true;
  private const string TEXTURE_PATH_PRE = "Texture/Unit/portrait_unit_";
  public UISprite typeIcon;
  public UILabel title;
  public UILabel marchOwnerName;
  public UILabel marchOwnerLevel;
  public UILabel marchOwnerTroopsCount;
  public UILabel repatriateText;
  public UIButton repatriateButton;
  public UITable marchTroopTable;
  public GameObject marchOwnerRoot;
  public System.Action<MarchData.MarchType> onMarchRecalled;
  private long marchId;
  private bool showMarchOwnerInfo;
  public IconGroup group;

  public void SetData(TroopType type, long marchId, bool showMarchOwnerInfo = false, bool showAllTroopType = false)
  {
    this.marchId = marchId;
    this.troopType = type;
    this.showMarchOwnerInfo = showMarchOwnerInfo;
    if ((UnityEngine.Object) this.typeIcon != (UnityEngine.Object) null)
      this.typeIcon.spriteName = this.GetTroopTypeIconName();
    if ((UnityEngine.Object) this.title != (UnityEngine.Object) null)
      this.title.text = this.GetTroopTypeName();
    MarchData marchData = DBManager.inst.DB_March.Get(marchId);
    if (marchData == null)
      return;
    if (showMarchOwnerInfo)
    {
      if ((UnityEngine.Object) this.marchOwnerName != (UnityEngine.Object) null)
      {
        UserData userData = DBManager.inst.DB_User.Get(marchData.ownerUid);
        this.marchOwnerName.text = userData == null ? string.Empty : userData.userName;
      }
      if ((UnityEngine.Object) this.marchOwnerLevel != (UnityEngine.Object) null)
      {
        DB.HeroData heroData = DBManager.inst.DB_hero.Get(marchData.ownerUid);
        this.marchOwnerLevel.text = heroData == null ? string.Empty : Utils.XLAT("id_lv") + heroData.level.ToString();
      }
      if ((UnityEngine.Object) this.marchOwnerTroopsCount != (UnityEngine.Object) null)
        this.marchOwnerTroopsCount.text = Utils.XLAT("alliance_fort_troops") + Utils.FormatThousands(marchData.troopsInfo.totalCount.ToString());
      NGUITools.SetActive(this.marchOwnerRoot, (UnityEngine.Object) this.marchOwnerName != (UnityEngine.Object) null && (UnityEngine.Object) this.marchOwnerTroopsCount != (UnityEngine.Object) null);
    }
    else if (!showMarchOwnerInfo)
      NGUITools.SetActive(this.marchOwnerRoot, false);
    List<IconData> datas = new List<IconData>();
    Dictionary<Unit_StatisticsInfo, int> dictionary = new Dictionary<Unit_StatisticsInfo, int>();
    List<Unit_StatisticsInfo> unitStatisticsInfoList = new List<Unit_StatisticsInfo>();
    Dictionary<string, Unit>.KeyCollection.Enumerator enumerator = marchData.troopsInfo.troops.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator.Current);
      if (data.GetUnitType() == type || showAllTroopType)
      {
        int count = marchData.troopsInfo.troops[enumerator.Current].Count;
        dictionary.Add(data, count);
        unitStatisticsInfoList.Add(data);
      }
    }
    unitStatisticsInfoList.Sort(new Comparison<Unit_StatisticsInfo>(this.CompareHandler));
    unitStatisticsInfoList.Reverse();
    for (int index1 = 0; index1 < unitStatisticsInfoList.Count; ++index1)
    {
      Unit_StatisticsInfo index2 = unitStatisticsInfoList[index1];
      int num = dictionary[index2];
      IconData iconData = new IconData();
      iconData.image = string.Format("{0}{1}", (object) "Texture/Unit/portrait_unit_", (object) index2.Image);
      iconData.contents = new string[3];
      iconData.contents[0] = ScriptLocalization.Get(string.Format("{0}{1}", (object) index2.ID, (object) "_name"), true);
      iconData.contents[1] = num.ToString();
      iconData.contents[2] = Utils.GetRomaNumeralsBelowTen(index2.Troop_Tier);
      datas.Add(iconData);
    }
    this.group.CreateIcon(datas);
    this.UpdateRepatriateButtonStatus();
    this.UpdateRepatriateButtonPosition();
  }

  private int CompareHandler(Unit_StatisticsInfo a, Unit_StatisticsInfo b)
  {
    if (a.Troop_Tier == b.Troop_Tier)
      return b.Priority.CompareTo(a.Priority);
    return b.Troop_Tier < a.Troop_Tier ? 1 : -1;
  }

  public void Clear()
  {
    this.group.Dispose();
  }

  private string GetTroopTypeIconName()
  {
    string str = string.Empty;
    switch (this.troopType)
    {
      case TroopType.class_infantry:
        str = "icon_parade_ground_infantry_01";
        break;
      case TroopType.class_ranged:
        str = "icon_parade_ground_ranged_01";
        break;
      case TroopType.class_cavalry:
        str = "icon_parade_ground_cavalry_01";
        break;
      case TroopType.class_artyfar:
        str = "icon_parade_ground_seige_01";
        break;
    }
    return str;
  }

  private string GetTroopTypeName()
  {
    string Term = string.Empty;
    switch (this.troopType)
    {
      case TroopType.class_infantry:
        Term = "march_info_infantry_title";
        break;
      case TroopType.class_ranged:
        Term = "march_info_ranged_title";
        break;
      case TroopType.class_cavalry:
        Term = "march_info_cavalry_title";
        break;
      case TroopType.class_artyfar:
        Term = "march_info_siege_title";
        break;
    }
    return ScriptLocalization.Get(Term, true);
  }

  private void UpdateRepatriateButtonStatus()
  {
    if (!((UnityEngine.Object) this.repatriateButton != (UnityEngine.Object) null))
      return;
    int num1 = 0;
    int num2 = 0;
    AllianceData allianceData = PlayerData.inst.allianceData;
    MarchData marchData = DBManager.inst.DB_March.Get(this.marchId);
    if (marchData == null)
      return;
    long num3 = 0;
    if (this.useTileData)
    {
      TileData selectedTile = PVPSystem.Instance.Map.SelectedTile;
      switch (selectedTile.TileType)
      {
        case TileType.City:
          num3 = selectedTile.AllianceID;
          break;
        case TileType.Wonder:
          num3 = selectedTile.WonderData.OWNER_ALLIANCE_ID;
          break;
        case TileType.WonderTower:
          num3 = selectedTile.WonderTowerData.OWNER_ALLIANCE_ID;
          break;
      }
    }
    if (allianceData == null || marchData.ownerAllianceId != allianceData.allianceId || num3 != allianceData.allianceId || marchData.type == MarchData.MarchType.reinforce && marchData.ownerUid != PlayerData.inst.uid)
    {
      if (marchData.ownerUid == PlayerData.inst.uid && this.useTileData)
        return;
      NGUITools.SetActive(this.repatriateButton.transform.parent.gameObject, false);
    }
    else
    {
      if (allianceData != null)
      {
        AllianceMemberData allianceMemberData1 = allianceData.members.Get(PlayerData.inst.uid);
        if (allianceMemberData1 != null)
          num1 = allianceMemberData1.title;
        AllianceMemberData allianceMemberData2 = allianceData.members.Get(marchData.ownerUid);
        if (allianceMemberData2 != null)
          num2 = allianceMemberData2.title;
      }
      this.repatriateButton.isEnabled = 5 <= num1 || (num1 != 4 ? marchData.ownerUid == PlayerData.inst.uid : num2 < num1);
      if (marchData.ownerUid == PlayerData.inst.uid)
      {
        this.repatriateText.text = Utils.XLAT("march_callback");
        this.repatriateButton.isEnabled = true;
        this.repatriateButton.defaultColor = new Color(0.2196078f, 0.7372549f, 0.9921569f, 1f);
        this.repatriateButton.hover = new Color(0.145098f, 0.5058824f, 0.682353f, 1f);
        this.repatriateButton.pressed = new Color(0.145098f, 0.5058824f, 0.682353f, 1f);
        this.repatriateButton.disabledColor = new Color(0.2196078f, 0.7372549f, 0.9921569f, 1f);
        this.repatriateButton.UpdateColor(true);
      }
      else
        this.repatriateText.text = Utils.XLAT("alliance_fort_send_troops_back_button");
      NGUITools.SetActive(this.repatriateButton.transform.parent.gameObject, this.repatriateButton.isEnabled);
      if (this.useTileData)
        return;
      NGUITools.SetActive(this.repatriateButton.transform.parent.gameObject, false);
    }
  }

  private void UpdateRepatriateButtonPosition()
  {
    if (!((UnityEngine.Object) this.repatriateButton != (UnityEngine.Object) null))
      return;
    this.marchTroopTable.repositionNow = true;
    this.marchTroopTable.Reposition();
    this.repatriateButton.transform.localPosition = new Vector3(this.repatriateButton.transform.localPosition.x, this.repatriateButton.transform.localPosition.y - NGUIMath.CalculateRelativeWidgetBounds(this.marchTroopTable.transform, false).size.y, this.repatriateButton.transform.localPosition.z);
  }

  public void OnMarchRecall()
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.marchId);
    if (marchData == null)
      return;
    if (marchData.ownerUid == PlayerData.inst.uid)
      GameEngine.Instance.marchSystem.Recall(this.marchId);
    else if (marchData.type == MarchData.MarchType.wonder_reinforce)
    {
      if (this.useTileData)
      {
        TileData selectedTile = PVPSystem.Instance.Map.SelectedTile;
        long wonderId = 0;
        if (selectedTile.TileType == TileType.WonderTower)
          wonderId = selectedTile.WonderTowerData.TOWER_ID;
        GameEngine.Instance.marchSystem.SendWonderTroopsHome(wonderId, marchData.ownerUid, (System.Action<bool, object>) null);
      }
    }
    else
      GameEngine.Instance.marchSystem.SendTroopHome(this.marchId);
    if (this.onMarchRecalled == null)
      return;
    this.onMarchRecalled(marchData.type);
  }
}
