﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightPacketUnderAttack
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DragonKnightPacketUnderAttack : DragonKnightPacket
{
  public long RoundGuid;
  public RoundResult Result;

  public DragonKnightPacketUnderAttack(long roundGuid, long playerId, RoundResult result)
  {
    this.Type = DragonKnightPacket.PacketType.UnderAttack;
    this.PlayerId = playerId;
    this.RoundGuid = roundGuid;
    this.Result = result;
  }
}
