﻿// Decompiled with JetBrains decompiler
// Type: Base64Coder
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;
using UnityEngine;

public class Base64Coder : MonoBehaviour
{
  private static StringBuilder _encodeBuilder = new StringBuilder();
  private static StringBuilder _decodeBuilder = new StringBuilder();

  public static string Encode(string content, string key)
  {
    Base64Coder._encodeBuilder.Length = 0;
    int length1 = content.Length;
    int length2 = key.Length;
    for (int index = 0; index < length1; ++index)
    {
      int num = (int) content[index] + (int) key[index % length2];
      Base64Coder._encodeBuilder.Append((char) num);
    }
    string s = Base64Coder._encodeBuilder.ToString();
    string empty = string.Empty;
    byte[] bytes = Encoding.UTF8.GetBytes(s);
    try
    {
      return Convert.ToBase64String(bytes);
    }
    catch
    {
      return s;
    }
  }

  public static string Decode(string content, string key)
  {
    string s = content;
    string empty = string.Empty;
    byte[] bytes = Convert.FromBase64String(s);
    string str;
    try
    {
      str = Encoding.UTF8.GetString(bytes);
    }
    catch
    {
      str = s;
    }
    Base64Coder._decodeBuilder.Length = 0;
    int length1 = str.Length;
    int length2 = key.Length;
    for (int index = 0; index < length1; ++index)
    {
      int num = (int) str[index] - (int) key[index % length2];
      Base64Coder._decodeBuilder.Append((char) num);
    }
    return Base64Coder._decodeBuilder.ToString();
  }
}
