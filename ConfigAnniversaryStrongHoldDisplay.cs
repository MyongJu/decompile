﻿// Decompiled with JetBrains decompiler
// Type: ConfigAnniversaryStrongHoldDisplay
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAnniversaryStrongHoldDisplay
{
  private Dictionary<string, AnniversaryStrongHoldDisplayInfo> m_DataByLevel;
  private Dictionary<int, AnniversaryStrongHoldDisplayInfo> m_DataByInternalID;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<AnniversaryStrongHoldDisplayInfo, string>(result as Hashtable, "level", out this.m_DataByLevel, out this.m_DataByInternalID);
  }

  public AnniversaryStrongHoldDisplayInfo Get(string id)
  {
    AnniversaryStrongHoldDisplayInfo strongHoldDisplayInfo;
    this.m_DataByLevel.TryGetValue(id, out strongHoldDisplayInfo);
    return strongHoldDisplayInfo;
  }

  public AnniversaryStrongHoldDisplayInfo Get(int internalId)
  {
    AnniversaryStrongHoldDisplayInfo strongHoldDisplayInfo;
    this.m_DataByInternalID.TryGetValue(internalId, out strongHoldDisplayInfo);
    return strongHoldDisplayInfo;
  }

  public void Clear()
  {
    if (this.m_DataByLevel != null)
    {
      this.m_DataByLevel.Clear();
      this.m_DataByLevel = (Dictionary<string, AnniversaryStrongHoldDisplayInfo>) null;
    }
    if (this.m_DataByInternalID == null)
      return;
    this.m_DataByInternalID.Clear();
    this.m_DataByInternalID = (Dictionary<int, AnniversaryStrongHoldDisplayInfo>) null;
  }
}
