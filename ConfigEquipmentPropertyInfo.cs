﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentPropertyInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ConfigEquipmentPropertyInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "equip_id")]
  public int equipID;
  [Config(Name = "enhance_level")]
  public int enhanceLevel;
  [Config(CustomParse = true, CustomType = typeof (Benefits), Name = "Benefits")]
  public Benefits benefits;
  [Config(Name = "scroll_id")]
  public int scrollID;
  [Config(Name = "steel_value")]
  public long steelValue;
  [Config(Name = "scroll_value")]
  public long scrollValue;
  [Config(CustomParse = true, CustomType = typeof (Materials), Name = "Materials")]
  public Materials Materials;
}
