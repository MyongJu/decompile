﻿// Decompiled with JetBrains decompiler
// Type: MailComposeDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UI;
using UnityEngine;

public class MailComposeDlg : UI.Dialog
{
  private List<string> recipients = new List<string>(20);
  private List<long> recipientsIDs = new List<long>(20);
  private List<RecipientEntry> recipientEntries = new List<RecipientEntry>();
  private List<SuggestionRecipientEntry> suggestionRecipientEntries = new List<SuggestionRecipientEntry>();
  private int _sendItemCount = 1;
  private const int recipentCellHeight = 85;
  private const int recipentBgHeight = 85;
  public UITable contentTable;
  public RecipientEntry recipientEntry;
  public GameObject recipientParent;
  public UIDiv recipientDiv;
  public UIGrid suggestionRecipientGrid;
  public SuggestionRecipientEntry suggestionRecipientEntry;
  public MailController controller;
  public UISprite recipientBg;
  public UIScrollView recipientScrollView;
  public UILabel contentLabel;
  public UIButton sendBtn;
  public UIInput mailContentInputLabel;
  public UIInput inputLabel;
  public UIButton outLine;
  public UIButton selectContactButton;
  public UIButton selectItemButton;
  public UITexture selectItemTexture;
  public UILabel selectItemCount;
  public UILabel selectItemHint;
  public GameObject blockTip;
  public GameObject attachment;
  public GameObject receiveEdit;
  public GameObject addButton;
  public UILabel disPlayLabel;
  private MailComposeDlg.Parameter param;
  private bool isAlliacneMail;
  private bool isModMail;
  private long allianceID;
  private int _sendItemId;

  public AbstractMailEntry mail { get; protected set; }

  private void Reset()
  {
    this.recipients.Clear();
    this.recipientsIDs.Clear();
    if (this.param != null)
    {
      if (this.param.recipents != null)
        this.recipients.AddRange((IEnumerable<string>) this.param.recipents);
      if (this.param.isAllianceMail)
      {
        this.inputLabel.enabled = false;
        GreyUtility.Grey(this.recipientBg.gameObject);
        this.isAlliacneMail = true;
        this.allianceID = this.param.allianceID;
      }
      else if (this.param.isModMail)
      {
        this.isModMail = true;
        this.allianceID = this.param.allianceID;
      }
      else
      {
        this.inputLabel.enabled = true;
        GreyUtility.Normal(this.recipientBg.gameObject);
        this.isAlliacneMail = false;
      }
    }
    this.InitEvts();
    UILabel contentLabel = this.contentLabel;
    string empty = string.Empty;
    this.mailContentInputLabel.value = empty;
    string str1 = empty;
    this.inputLabel.value = str1;
    string str2 = str1;
    contentLabel.text = str2;
    if ((UnityEngine.Object) UIInput.current != (UnityEngine.Object) null)
      UIInput.current.value = (string) null;
    this.DrawRecipient();
    this.DrawSuggestionRecipient(new List<string>());
    this.UpdateSendBtn();
    this.UpdateItemTips();
    this.UpdateItemStatus();
    this.blockTip.SetActive(this.isAlliacneMail);
    this.UpdateModeratorMode();
  }

  private void UpdateModeratorMode()
  {
    this.attachment.SetActive(true);
    this.disPlayLabel.gameObject.SetActive(false);
    this.receiveEdit.SetActive(true);
    this.addButton.gameObject.SetActive(true);
    if (!(bool) ((UnityEngine.Object) this.attachment) || this.param == null || !this.param.isModMail)
      return;
    this.attachment.SetActive(false);
    if (this.param.allianceID == 0L)
      return;
    this.disPlayLabel.gameObject.SetActive(true);
    this.receiveEdit.SetActive(false);
    this.addButton.gameObject.SetActive(false);
    this.disPlayLabel.text = this.param.recipents[0];
  }

  private void OnContentInputLabelChange()
  {
    this.UpdateSendBtn();
    this.UpdateItemStatus();
  }

  private void ValidRecipients(System.Action<bool, string, ArrayList> callback)
  {
    ArrayList recipients = new ArrayList((ICollection) this.recipients);
    ArrayList recipentUids = new ArrayList();
    bool validate = true;
    this.controller.ValidateRecipient(recipients, (System.Action<bool, object>) ((succeeded, response) =>
    {
      Hashtable hashtable = response as Hashtable;
      string str1 = string.Empty;
      string str2 = string.Empty;
      List<string> stringList = new List<string>();
      for (int index = 0; index < this.recipients.Count; ++index)
      {
        if (hashtable.ContainsKey((object) this.recipients[index]) && hashtable[(object) this.recipients[index]].ToString() != "0")
        {
          stringList.Add(this.recipients[index]);
          if (hashtable[(object) this.recipients[index]].ToString() != PlayerData.inst.uid.ToString())
          {
            recipentUids.Add(hashtable[(object) this.recipients[index]]);
            str2 = this.recipients[index];
          }
          this.controller.MailStorage.AddInteractedUsername(this.recipients[index]);
        }
        else
        {
          str1 = str1 + this.recipients[index].ToString() + ",";
          validate = false;
        }
      }
      this.recipients.Clear();
      this.recipients.AddRange((IEnumerable<string>) stringList);
      if (callback == null)
        return;
      callback(validate, str1, recipentUids);
    }));
  }

  private void OnSendBtnClick()
  {
    int levelLimit = ChatAndMailConstraint.Instance.LevelLimit;
    if (ChatAndMailConstraint.Instance.SwitchState && !ChatAndMailConstraint.Instance.HasBindAccount() && CityManager.inst.mStronghold.mLevel < levelLimit)
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_mail_use_forbidden", new Dictionary<string, string>()
      {
        {
          "0",
          levelLimit.ToString()
        }
      }, true), (System.Action) null, 4f, false);
    else if (PlayerData.inst.userData.IsShutUp)
    {
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("chat_silenced_title"),
        Content = ScriptLocalization.GetWithMultyContents("chat_silenced_description", true, Utils.FormatTime(PlayerData.inst.userData.ShutUpLeftTime, false, false, true)),
        Okay = Utils.XLAT("id_uppercase_okay"),
        OkayCallback = (System.Action) null
      });
    }
    else
    {
      if (this.recipients.Count < 1 || string.IsNullOrEmpty(this.mailContentInputLabel.value))
        return;
      this.mailContentInputLabel.value = Utils.GetLimitStr(this.mailContentInputLabel.value, 400);
      if (this.isAlliacneMail)
        this.controller.SendAllianceMail(this.allianceID, this.mailContentInputLabel.value, (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null)));
      else if (this.isModMail)
      {
        if (this.allianceID != 0L)
        {
          this.controller.SendModMail(this.allianceID, this.mailContentInputLabel.value, (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null)));
        }
        else
        {
          if (this.recipients.Count <= 0)
            return;
          this.ValidRecipients((System.Action<bool, string, ArrayList>) ((validate, errMsg, recipentUids) =>
          {
            this.DrawRecipient();
            this.UpdateSendBtn();
            if (validate)
              this.controller.SendModMails(recipentUids, this.mailContentInputLabel.value, (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null)));
            else
              UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
              {
                Title = Utils.XLAT("mail_failure_pop_up_title"),
                Content = (errMsg + " " + Utils.XLAT("mail_failure_name_error_description")),
                Okay = "OKAY",
                OkayCallback = (System.Action) null
              });
          }));
        }
      }
      else
      {
        ArrayList recipients = new ArrayList((ICollection) this.recipients);
        ArrayList recipentUids = new ArrayList();
        bool validate = true;
        this.controller.ValidateRecipient(recipients, (System.Action<bool, object>) ((succeeded, response) =>
        {
          Hashtable hashtable = response as Hashtable;
          string str1 = string.Empty;
          string str2 = string.Empty;
          List<string> stringList = new List<string>();
          for (int index = 0; index < this.recipients.Count; ++index)
          {
            if (hashtable.ContainsKey((object) this.recipients[index]) && hashtable[(object) this.recipients[index]].ToString() != "0")
            {
              stringList.Add(this.recipients[index]);
              if (hashtable[(object) this.recipients[index]].ToString() != PlayerData.inst.uid.ToString())
              {
                recipentUids.Add(hashtable[(object) this.recipients[index]]);
                str2 = this.recipients[index];
              }
              this.controller.MailStorage.AddInteractedUsername(this.recipients[index]);
            }
            else
            {
              str1 = str1 + this.recipients[index].ToString() + ",";
              validate = false;
            }
          }
          this.recipients.Clear();
          this.recipients.AddRange((IEnumerable<string>) stringList);
          this.DrawRecipient();
          this.UpdateSendBtn();
          this.UpdateItemStatus();
          if (validate)
          {
            if (this._sendItemId > 0 && recipentUids.Count == 1)
            {
              Dictionary<string, string> para = new Dictionary<string, string>();
              ItemStaticInfo itemStaticInfo1 = ConfigManager.inst.DB_Items.GetItem(this._sendItemId);
              if (itemStaticInfo1 != null)
                para.Add("0", itemStaticInfo1.LocName);
              ItemStaticInfo itemStaticInfo2 = ConfigManager.inst.DB_Items.GetItem("item_postman");
              if (itemStaticInfo2 != null)
                para.Add("2", itemStaticInfo2.LocName);
              para.Add("1", str2);
              UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
              {
                title = Utils.XLAT("id_uppercase_confirmation"),
                content = ScriptLocalization.GetWithPara("mail_postman_confirm_description", para, true),
                yes = Utils.XLAT("id_uppercase_yes"),
                no = Utils.XLAT("id_uppercase_cancel"),
                yesCallback = (System.Action) (() => this.controller.SendMail(recipentUids, this.contentLabel.text, this._sendItemId, this._sendItemCount, (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null)))),
                noCallback = (System.Action) null
              });
            }
            else
              this.controller.SendMail(recipentUids, this.mailContentInputLabel.value, (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null)));
          }
          else
            UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
            {
              Title = Utils.XLAT("mail_failure_pop_up_title"),
              Content = (str1 + " " + Utils.XLAT("mail_failure_name_error_description")),
              Okay = "OKAY",
              OkayCallback = (System.Action) null
            });
        }));
      }
    }
  }

  private int RecipientTableSort(Transform a, Transform b)
  {
    return int.Parse(a.name) - int.Parse(b.name);
  }

  private void OnInputLabelChange()
  {
    string str = this.inputLabel.value;
    if (str.Length > 0 && str.Contains(";"))
      this.OnInputLabelSubmit();
    else if (!string.IsNullOrEmpty(this.inputLabel.value))
    {
      List<string> contactsForSubstring = this.controller.GetMatchingContactsForSubstring(this.inputLabel.value);
      contactsForSubstring.Insert(0, this.inputLabel.value);
      this.DrawSuggestionRecipient(contactsForSubstring);
    }
    else
      this.DrawSuggestionRecipient(new List<string>());
  }

  private void OnExtendUIInputDeselect()
  {
    string str = this.inputLabel.value;
    if (!string.IsNullOrEmpty(this.inputLabel.value))
    {
      List<string> contactsForSubstring = this.controller.GetMatchingContactsForSubstring(this.inputLabel.value);
      contactsForSubstring.Insert(0, this.inputLabel.value);
      this.DrawSuggestionRecipient(contactsForSubstring);
    }
    else
      this.DrawSuggestionRecipient(new List<string>());
  }

  private void OnInputLabelSubmit()
  {
    string str1 = this.inputLabel.value;
    if (str1.Length > 0)
    {
      string str2 = str1.Replace(";", string.Empty);
      if (!this.recipients.Contains(str2) && str2 != PlayerData.inst.userData.userName)
        this.recipients.Add(str2);
    }
    this.inputLabel.value = string.Empty;
    this.inputLabel.savedAs = string.Empty;
    this.DrawRecipient();
    this.DrawSuggestionRecipient(new List<string>());
    this.UpdateSendBtn();
    this.UpdateItemStatus();
  }

  private void UpdateSendBtn()
  {
    this.sendBtn.isEnabled = this.recipients.Count > 0 && !string.IsNullOrEmpty(this.contentLabel.text);
  }

  private void UpdateItemTips()
  {
    this.selectItemHint.text = string.Empty;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_postman");
    if (itemStaticInfo == null)
      return;
    ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(itemStaticInfo.internalId);
    this.selectItemHint.text = ScriptLocalization.GetWithPara("mail_postman_item_description", new Dictionary<string, string>()
    {
      {
        "0",
        itemStaticInfo.LocName
      },
      {
        "1",
        consumableItemData == null ? "0" : consumableItemData.quantity.ToString()
      }
    }, true);
  }

  private void UpdateItemStatus()
  {
    NGUITools.SetActive(this.selectItemTexture.transform.parent.gameObject, false);
    if (this.recipients.Count <= 1)
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._sendItemId);
      if (itemStaticInfo != null)
      {
        if ((UnityEngine.Object) this.selectItemTexture.mainTexture == (UnityEngine.Object) null || this.selectItemTexture.mainTexture.name != itemStaticInfo.Image)
          BuilderFactory.Instance.HandyBuild((UIWidget) this.selectItemTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
        this.selectItemCount.text = this._sendItemCount.ToString();
        NGUITools.SetActive(this.selectItemTexture.transform.parent.gameObject, true);
      }
    }
    this.selectItemButton.isEnabled = this.recipients.Count == 1;
    ItemStaticInfo itemStaticInfo1 = ConfigManager.inst.DB_Items.GetItem("item_postman");
    if (itemStaticInfo1 == null)
      return;
    ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(itemStaticInfo1.internalId);
    UIButton selectItemButton = this.selectItemButton;
    selectItemButton.isEnabled = ((selectItemButton.isEnabled ? 1 : 0) & (consumableItemData != null ? (consumableItemData.quantity > 0 ? 1 : 0) : 0)) != 0;
  }

  private void OnContentReposition()
  {
    this.recipientBg.height = 170;
    if (this.recipientDiv.rows < 2)
    {
      this.recipientScrollView.movement = UIScrollView.Movement.Custom;
      this.recipientScrollView.customMovement = new Vector2(0.0f, 0.0f);
    }
    else
      this.recipientScrollView.movement = UIScrollView.Movement.Vertical;
    this.contentTable.repositionNow = true;
    this.recipientScrollView.ResetPosition();
    this.recipientScrollView.MoveRelative(Vector3.up * 1000f);
    this.recipientScrollView.RestrictWithinBounds(true);
  }

  private void OnSubjectInputChange()
  {
    this.UpdateSendBtn();
    this.UpdateItemStatus();
  }

  private void OnOutlineClick()
  {
    this.inputLabel.isSelected = true;
  }

  private void OnSelectContactClick()
  {
    UIManager.inst.OpenPopup("Mail/MailSelectContactPopup", (Popup.PopupParameter) new MailSelectionPopup.Parameter()
    {
      callback = new System.Action<List<string>>(this.SelectContactCallback),
      targets = new List<string>((IEnumerable<string>) this.recipients.ToArray())
    });
  }

  private void OnSelectItemClick()
  {
    UIManager.inst.OpenPopup("Mail/MailItemListPopup", (Popup.PopupParameter) new MailItemListPopup.Parameter()
    {
      callback = new System.Action<ConsumableItemData>(this.SelectItemCallback)
    });
  }

  private void SelectItemCallback(ConsumableItemData itemData)
  {
    if (itemData != null)
    {
      this._sendItemId = itemData.internalId;
      this._sendItemCount = 1;
    }
    this.UpdateItemStatus();
  }

  private void SelectContactCallback(List<string> contacts)
  {
    this.recipients.Clear();
    if (contacts != null && contacts.Count > 0)
    {
      for (int index = 0; index < contacts.Count; ++index)
      {
        if (!this.recipients.Contains(contacts[index]))
          this.recipients.Add(contacts[index]);
      }
    }
    this.inputLabel.value = string.Empty;
    this.inputLabel.savedAs = string.Empty;
    this.DrawRecipient();
    this.UpdateSendBtn();
    this.UpdateItemStatus();
  }

  private void InitEvts()
  {
    this.suggestionRecipientGrid.onReposition += new UIGrid.OnReposition(this.OnContentReposition);
    UIDiv recipientDiv = this.recipientDiv;
    recipientDiv.onReposition = recipientDiv.onReposition + new UITable.OnReposition(this.OnContentReposition);
    this.recipientDiv.onCustomSort = new Comparison<Transform>(this.RecipientTableSort);
    this.inputLabel.onChange.Add(new EventDelegate(new EventDelegate.Callback(this.OnInputLabelChange)));
    this.inputLabel.onSubmit.Add(new EventDelegate(new EventDelegate.Callback(this.OnInputLabelSubmit)));
    this.inputLabel.onValidate = new UIInput.OnValidate(this.InputLabelValidate);
    this.mailContentInputLabel.onChange.Add(new EventDelegate(new EventDelegate.Callback(this.OnContentInputLabelChange)));
    this.mailContentInputLabel.onValidate = new UIInput.OnValidate(this.InputLabelValidate);
    this.sendBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnSendBtnClick)));
    this.outLine.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnOutlineClick)));
    this.selectContactButton.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnSelectContactClick)));
    this.selectItemButton.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnSelectItemClick)));
  }

  private char InputLabelValidate(string text, int charIndex, char addedChar)
  {
    if (this.isValidateCharacter(addedChar))
      return addedChar;
    return char.MinValue;
  }

  private bool isValidateCharacter(char codePoint)
  {
    return char.GetUnicodeCategory(codePoint) != UnicodeCategory.Surrogate;
  }

  private void RemoveBtnEvts()
  {
    this.suggestionRecipientGrid.onReposition -= new UIGrid.OnReposition(this.OnContentReposition);
    UIDiv recipientDiv = this.recipientDiv;
    recipientDiv.onReposition = recipientDiv.onReposition - new UITable.OnReposition(this.OnContentReposition);
    this.recipientDiv.onCustomSort = new Comparison<Transform>(this.RecipientTableSort);
    this.inputLabel.onChange.Clear();
    this.inputLabel.onSubmit.Clear();
    this.mailContentInputLabel.onChange.Clear();
    this.sendBtn.onClick.Clear();
  }

  private void DrawRecipient()
  {
    for (int index = 0; index < this.recipientEntries.Count; ++index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.recipientEntries[index].gameObject);
    this.recipientEntries.Clear();
    for (int index = 0; index < this.recipients.Count; ++index)
    {
      RecipientEntry component = NGUITools.AddChild(this.recipientParent, this.recipientEntry.gameObject).GetComponent<RecipientEntry>();
      component.recipient.text = this.recipients[index];
      component.Show();
      component.name = index.ToString();
      this.recipientEntries.Add(component);
      component.OnRecipientChange += new RecipientEntry.RecipientChangeHandler(this.OnRecipientChange);
      component.OnRecipientSubmit += new RecipientEntry.RecipientChangeHandler(this.OnRecipientSubmit);
      component.InputEnalbed = !this.isAlliacneMail;
    }
    this.recipientDiv.repositionNow = true;
  }

  private void OnRecipientChange(RecipientEntry sender)
  {
    int index = this.recipientEntries.IndexOf(sender);
    if (index < 0)
      return;
    this.recipients[index] = sender.inputLabel.value;
    this.recipientDiv.repositionNow = true;
  }

  private void OnRecipientSubmit(RecipientEntry sender)
  {
    int index = this.recipientEntries.IndexOf(sender);
    if (index < 0)
      return;
    if (sender.inputLabel.value == string.Empty)
      this.recipients.RemoveAt(index);
    this.DrawRecipient();
    this.UpdateItemStatus();
  }

  private void DrawSuggestionRecipient(List<string> recipients)
  {
    for (int index = 0; index < this.suggestionRecipientEntries.Count; ++index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.suggestionRecipientEntries[index].gameObject);
    this.suggestionRecipientEntries.Clear();
    for (int index = 0; index < recipients.Count; ++index)
    {
      SuggestionRecipientEntry component = NGUITools.AddChild(this.suggestionRecipientGrid.gameObject, this.suggestionRecipientEntry.gameObject).GetComponent<SuggestionRecipientEntry>();
      component.recipientLabel.text = recipients[index];
      component.Show();
      component.OnSuggestionRecipientClicked += new SuggestionRecipientEntry.SuggestionRecipientClickHandler(this.HandleOnSuggestionRecipientClicked);
      this.suggestionRecipientEntries.Add(component);
    }
    this.suggestionRecipientGrid.repositionNow = true;
  }

  private void HandleOnSuggestionRecipientClicked(SuggestionRecipientEntry sender)
  {
    if (!this.recipients.Contains(sender.recipientLabel.text))
      this.recipients.Add(sender.recipientLabel.text);
    this.inputLabel.value = string.Empty;
    this.inputLabel.savedAs = string.Empty;
    this.DrawRecipient();
    this.DrawSuggestionRecipient(new List<string>());
    this.UpdateSendBtn();
    this.UpdateItemStatus();
  }

  private void OnBackBtnClicked()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void OnCloseBtnClicked()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnEmojiInputClick()
  {
    EmojiManager.Instance.ShowEmojiInput((System.Action<string>) (obj => this.mailContentInputLabel.value += obj));
  }

  public void OnItemTextureClick()
  {
    this.OnSelectItemClick();
  }

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
    this.controller = PlayerData.inst.mail;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.param = orgParam as MailComposeDlg.Parameter;
    this.Reset();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveBtnEvts();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public List<string> recipents = new List<string>();
    public bool isAllianceMail;
    public long allianceID;
    public bool isModMail;
  }
}
