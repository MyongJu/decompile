﻿// Decompiled with JetBrains decompiler
// Type: ConfigAbyssCollectReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigAbyssCollectReward
{
  public Dictionary<string, AbyssCollectRewardInfo> datas;
  private Dictionary<int, AbyssCollectRewardInfo> dicByUniqueId;
  private List<AbyssCollectRewardInfo> allCollectRewardInfo;

  public List<AbyssCollectRewardInfo> AllCollectRewardInfo
  {
    get
    {
      return this.allCollectRewardInfo;
    }
  }

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<AbyssCollectRewardInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.allCollectRewardInfo = new List<AbyssCollectRewardInfo>();
    using (Dictionary<int, AbyssCollectRewardInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.allCollectRewardInfo.Add(enumerator.Current.Value);
    }
    this.allCollectRewardInfo.Sort((Comparison<AbyssCollectRewardInfo>) ((x, y) => x.NumReq.CompareTo(y.NumReq)));
  }

  public AbyssCollectRewardInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (AbyssCollectRewardInfo) null;
  }

  public AbyssCollectRewardInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AbyssCollectRewardInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, AbyssCollectRewardInfo>) null;
    }
    if (this.dicByUniqueId != null)
    {
      this.dicByUniqueId.Clear();
      this.dicByUniqueId = (Dictionary<int, AbyssCollectRewardInfo>) null;
    }
    if (this.allCollectRewardInfo == null)
      return;
    this.allCollectRewardInfo.Clear();
    this.allCollectRewardInfo = (List<AbyssCollectRewardInfo>) null;
  }
}
