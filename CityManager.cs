﻿// Decompiled with JetBrains decompiler
// Type: CityManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class CityManager
{
  private string mCityName = string.Empty;
  private int m_CityId = -1;
  private bool _resortBuildings = true;
  private CityManager.ResourceItem m_FoodResource = new CityManager.ResourceItem(CityManager.ResourceTypes.FOOD);
  private CityManager.ResourceItem m_SilverResource = new CityManager.ResourceItem(CityManager.ResourceTypes.SILVER);
  private CityManager.ResourceItem m_WoodResource = new CityManager.ResourceItem(CityManager.ResourceTypes.WOOD);
  private CityManager.ResourceItem m_OreResource = new CityManager.ResourceItem(CityManager.ResourceTypes.ORE);
  private long _cancelDeconstructID = -1;
  private Hashtable callbacks = new Hashtable();
  public const string TroopNameInfantry = "infantry";
  public const string TroopNameRanged = "ranged";
  public const string TroopNameCavalry = "cavalry";
  public const string TroopNameArtilleryClose = "mage";
  public const string TroopNameArtilleryDistance = "siege";
  public const string TroopNameHero = "hero";
  public const string TroopNameBoss = "boss";
  public const string TroopNameEpicBoss = "epicboss";
  public const string TroopNameVSInfantry = "defense_vsinfantry";
  public const string TroopNameVSRanged = "defense_vsranged";
  public const string TroopNameVSCavalry = "defense_vscavalry";
  public const string TroopNameVSArtilleryClose = "defense_vsartilleryclose";
  public const string TroopDefenseKey = "defense";
  public const string TroopTierKey = "_t";
  public const string TroopTier1Key = "_t1";
  public const string BuildingPath = "Prefab/UI/BuildingPrefab/ui_";
  public const string NewBuildingPath = "Prefab/City/";
  public const string BuildingLevelKey = "_l";
  public long mHospitalJobID;
  public CityManager.BuildingItem mStronghold;
  public CityManager.BuildingItem mWalls;
  public CityManager.BuildingItem mParadeGround;
  public CityManager.BuildingItem mWatchTower;
  public CityManager.BuildingItem mTavern;
  public static CityManager inst;
  private bool m_Initialized;
  private bool m_CityLoaded;
  private HubPort mPortAddObject;
  private HubPort mPortUpgradeObject;
  private HubPort mPortDestroyObject;
  private List<CityManager.BuildingItem> m_Buildings;
  private long cityUpKeep;
  private long oldFood;
  private long oldWood;
  private long oldSilver;
  private long oldOre;
  private System.Action _cancellCallback;
  private long _building_id;
  private int cbSerial;

  public event CityManager.OnBuildingActionState onBuildingActionState;

  public event CityManager.OnCreateBuilding onCreateBuilding;

  public event CityManager.OnResourcesUpdated onResourcesUpdated;

  public event CityManager.OnUpgradeBuilding onUpgradeBuilding;

  public event CityManager.OnUpgradeFinished onUpgradeFinished;

  public event CityManager.OnUpgradeFinished onUpgradeCancelled;

  public event CityManager.OnDestroyBuilding onDestroyBuilding;

  public List<CityManager.BuildingItem> buildings
  {
    get
    {
      return this.m_Buildings;
    }
  }

  public Coordinate Location
  {
    get
    {
      return this.MyCityData.cityLocation;
    }
  }

  public CityData MyCityData
  {
    get
    {
      return DBManager.inst.DB_City.Get((long) this.GetCityID());
    }
  }

  public void Init()
  {
    if (this.m_Initialized)
      return;
    CityManager.inst = this;
    this.m_Initialized = true;
    this.m_CityLoaded = false;
    this.m_Buildings = new List<CityManager.BuildingItem>();
    this.m_FoodResource.Init(20000, 20000);
    this.m_WoodResource.Init(20000, 20000);
    this.m_OreResource.Init(20000, 20000);
    this.m_SilverResource.Init(20000, 20000);
    this.mPortAddObject = MessageHub.inst.GetPortByAction("City:addObject");
    this.mPortUpgradeObject = MessageHub.inst.GetPortByAction("City:upgradeObject");
    this.mPortDestroyObject = MessageHub.inst.GetPortByAction("City:deconstructObject");
    DBManager.inst.DB_CityMap.onDataCreated += new System.Action<CityMapData>(this.OnBuildComplete);
    DBManager.inst.DB_CityMap.onDataUpdated += new System.Action<CityMapData>(this.OnBuildingUpdated);
    DBManager.inst.DB_CityMap.onDataChanged += new System.Action<CityMapData>(this.OnCityDataChange);
    DBManager.inst.DB_City.onDataChanged += new System.Action<long>(this.OnTroopChange);
    DBManager.inst.DB_Local_Benefit.onDataChanged += new System.Action<string>(this.OnBenefitChange);
    MessageHub.inst.GetPortByAction("job_complete").AddEvent(new System.Action<object>(this.OnJobCompleted));
  }

  public void CreateBuilding(string buildingType, int slotid, int level, bool instant, double time = 0.0, int gold = 0)
  {
    Hashtable postData = new Hashtable();
    int cityId = this.GetCityID();
    postData[(object) "city_id"] = (object) cityId;
    int internalId = ConfigManager.inst.DB_Building.GetData(buildingType, level).internalId;
    postData[(object) "type"] = (object) internalId;
    long buildingId = (long) NetServerTime.inst.UpdateTime;
    postData[(object) "building_id"] = (object) buildingId;
    postData[(object) "slot_id"] = (object) slotid;
    postData[(object) nameof (instant)] = (object) instant;
    postData[(object) nameof (gold)] = (object) gold;
    if (instant)
      this.mPortAddObject.SendRequest(postData, (System.Action<bool, object>) ((ret, obj) =>
      {
        if (!ret)
          return;
        BuildingController controllerFromBuildingId = CitadelSystem.inst.GetBuildControllerFromBuildingID(buildingId);
        if (!((UnityEngine.Object) null != (UnityEngine.Object) controllerFromBuildingId))
          return;
        controllerFromBuildingId.UpgradeFinished(true);
        NewTutorial.Instance.CheckTrigger(controllerFromBuildingId);
      }), true);
    else if (TutorialManager.Instance.IsRunning)
    {
      this.UpdateFakeBuildingDB(buildingType, slotid, internalId, time);
    }
    else
    {
      postData[(object) "c_job_id"] = (object) 0;
      this.mPortAddObject.SendRequest(postData, (System.Action<bool, object>) null, true);
    }
  }

  private void UpdateFakeBuildingDB(string buildingType, int slotid, int typeid, double time)
  {
    long updateTime = (long) (NetServerTime.inst.UpdateTime * 1000.0);
    int num1 = 0;
    int num2 = 0;
    int num3 = 0;
    string key = buildingType;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CityManager.\u003C\u003Ef__switch\u0024map5A == null)
      {
        // ISSUE: reference to a compiler-generated field
        CityManager.\u003C\u003Ef__switch\u0024map5A = new Dictionary<string, int>(3)
        {
          {
            "lumber_mill",
            0
          },
          {
            "barracks",
            1
          },
          {
            "farm",
            2
          }
        };
      }
      int num4;
      // ISSUE: reference to a compiler-generated field
      if (CityManager.\u003C\u003Ef__switch\u0024map5A.TryGetValue(key, out num4))
      {
        switch (num4)
        {
          case 0:
            num1 = 72;
            num2 = 0;
            num3 = 1000;
            break;
          case 1:
            num1 = 375;
            num2 = 624;
            num3 = 1001;
            break;
          case 2:
            num1 = 0;
            num2 = 72;
            num3 = 1002;
            break;
        }
      }
    }
    Hashtable hashtable1 = new Hashtable();
    hashtable1[(object) "uid"] = (object) PlayerData.inst.uid;
    hashtable1[(object) "city_id"] = (object) this.GetCityID();
    hashtable1[(object) "building_id"] = (object) num3;
    hashtable1[(object) "slot_id"] = (object) slotid;
    hashtable1[(object) "type_id"] = (object) typeid;
    hashtable1[(object) "level"] = (object) 1;
    DBManager.inst.DB_CityMap.Update((object) hashtable1, updateTime);
    Hashtable hashtable2 = new Hashtable();
    hashtable2[(object) "building_id"] = (object) num3;
    if (BuildQueueManager.Instance.IsNormalQueueFull())
      hashtable2[(object) "queue"] = (object) "pay";
    else
      hashtable2[(object) "queue"] = (object) "free";
    long clientJob = JobManager.Instance.CreateClientJob(JobEvent.JOB_BUILDING_CONSTRUCTION, (int) time, (object) hashtable2, (System.Action) null);
    DBManager.inst.DB_CityMap.Get(PlayerData.inst.uid, (long) this.GetCityID(), (long) num3).jobId = clientJob;
  }

  private void OnBuildComplete(CityMapData cityMapData)
  {
    if (this.FindBuildingItem(cityMapData.buildingId) != null)
      return;
    CityManager.BuildingItem buildingItem = this.AddBuildingItem(cityMapData);
    if (this.onCreateBuilding == null)
      return;
    this.onCreateBuilding(buildingItem, cityMapData.jobId);
  }

  public void UpgradeBuilding(long buildingId, bool instant, double time = 0.0, int gold = 0)
  {
    long uid = PlayerData.inst.uid;
    long cityId = (long) this.GetCityID();
    CityMapData building = DBManager.inst.DB_CityMap.Get(uid, cityId, buildingId);
    if (building == null)
      return;
    Hashtable postData = new Hashtable();
    postData[(object) "city_id"] = (object) cityId;
    postData[(object) "building_id"] = (object) buildingId;
    postData[(object) nameof (instant)] = (object) instant;
    postData[(object) nameof (gold)] = (object) gold;
    if (instant)
    {
      this.mPortUpgradeObject.SendRequest(postData, (System.Action<bool, object>) ((ret, obj) =>
      {
        if (!ret)
          return;
        BuildingController controllerFromBuildingId = CitadelSystem.inst.GetBuildControllerFromBuildingID(building.buildingId);
        if (!((UnityEngine.Object) null != (UnityEngine.Object) controllerFromBuildingId))
          return;
        controllerFromBuildingId.UpgradeFinished(true);
        NewTutorial.Instance.CheckTrigger(controllerFromBuildingId);
      }), true);
    }
    else
    {
      postData[(object) "c_job_id"] = (object) 0;
      this.mPortUpgradeObject.SendRequest(postData, (System.Action<bool, object>) null, true);
    }
  }

  private void OnBuildingUpdated(CityMapData cityMapData)
  {
    CityManager.BuildingItem buildingItem = this.FindBuildingItem(cityMapData.buildingId);
    if (buildingItem == null || this.onUpgradeBuilding == null)
      return;
    this.onUpgradeBuilding(buildingItem, cityMapData.jobId != 0L);
  }

  public void DeconstructBuilding(long buildingId, int secondsRemaining, System.Action callback)
  {
    CityMapData cityMapData = DBManager.inst.DB_CityMap.Get(PlayerData.inst.uid, (long) this.GetCityID(), buildingId);
    if (secondsRemaining == 0)
    {
      string str = "shopitem_building_deconstruct";
      bool flag = ItemBag.Instance.GetItemCountByShopID(str) <= 0;
      int SlotId = cityMapData.slotId;
      Hashtable extra = Utils.Hash((object) "building_id", (object) buildingId.ToString(), (object) "buy", (object) flag);
      if (flag)
        ItemBag.Instance.BuyAndUseShopItem(str, extra, (System.Action<bool, object>) ((bOk, data) =>
        {
          if (bOk && this.onDestroyBuilding != null)
          {
            this.onDestroyBuilding(buildingId, SlotId);
            CityManager.BuildingItem buildingItem = this.FindBuildingItem(buildingId);
            if (buildingItem != null)
              this.buildings.Remove(buildingItem);
          }
          callback();
        }), 1);
      else
        ItemBag.Instance.UseItemByShopItemID(str, extra, (System.Action<bool, object>) ((bOk, data) =>
        {
          if (bOk && this.onDestroyBuilding != null)
          {
            this.onDestroyBuilding(buildingId, SlotId);
            CityManager.BuildingItem buildingItem = this.FindBuildingItem(buildingId);
            if (buildingItem != null)
              this.buildings.Remove(buildingItem);
          }
          callback();
        }), 1);
    }
    else
    {
      this.mPortDestroyObject.SendRequest(Utils.Hash((object) "city_id", (object) this.GetCityID(), (object) "building_id", (object) buildingId.ToString(), (object) "instant", (object) false), (System.Action<bool, object>) null, true);
      callback();
    }
  }

  private void OnCityDataChange(CityMapData data)
  {
    BuildingController fromBuildingItem = CitadelSystem.inst.GetBuildControllerFromBuildingItem(this.FindBuildingItem(data.buildingId));
    if (!((UnityEngine.Object) null != (UnityEngine.Object) fromBuildingItem))
      return;
    bool flag = "barracks" == fromBuildingItem.mBuildingItem.mType || "stables" == fromBuildingItem.mBuildingItem.mType || ("range" == fromBuildingItem.mBuildingItem.mType || "sanctum" == fromBuildingItem.mBuildingItem.mType) || ("workshop" == fromBuildingItem.mBuildingItem.mType || "fortress" == fromBuildingItem.mBuildingItem.mType) || "forge" == fromBuildingItem.mBuildingItem.mType;
    if (data.inBuilding && flag && (UnityEngine.Object) null == (UnityEngine.Object) fromBuildingItem.GetComponentInChildren<CollectionUI>())
    {
      if ("forge" == fromBuildingItem.mBuildingItem.mType)
      {
        CityCircleBtnPara para = new CityCircleBtnPara(AtlasType.Gui_2, "btn_finish_forging", !(fromBuildingItem.mBuildingItem.mType == "forge") ? "id_train" : "id_build", new EventDelegate((EventDelegate.Callback) (() => EquipmentManager.Instance.CollectEquipment(BagType.Hero, (System.Action<bool, object>) ((arg1, arg2) =>
        {
          if (!arg1)
            return;
          Hashtable hashtable = arg2 as Hashtable;
          UIManager.inst.OpenPopup("Equipment/EquipmentForgeSuccessfullyPopup", (Popup.PopupParameter) new EquipmentForgeSuccessfullyPopup.Parameter()
          {
            bagType = BagType.Hero,
            equipmenItemID = (long) int.Parse(hashtable[(object) "item_id"].ToString())
          });
        })))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
        CollectionUIManager.Instance.OpenCollectionUI(fromBuildingItem.transform, para, CollectionUI.CollectionType.TrainSoldier);
      }
      else
      {
        CityCircleBtnPara para = new CityCircleBtnPara(AtlasType.Gui_2, !("walls" == fromBuildingItem.mBuildingItem.mType) ? "btn_finish_train" : "btn_finish_defense", "id_train", new EventDelegate((EventDelegate.Callback) (() => BarracksManager.Instance.CollectBuilding(data.buildingId, (System.Action) null))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
        CollectionUIManager.Instance.OpenCollectionUI(fromBuildingItem.transform, para, CollectionUI.CollectionType.TrainSoldier);
      }
    }
    else
    {
      if (data.inBuilding)
        return;
      CollectionUI componentInChildren = fromBuildingItem.GetComponentInChildren<CollectionUI>();
      if (!((UnityEngine.Object) null != (UnityEngine.Object) componentInChildren) || componentInChildren.CT != CollectionUI.CollectionType.TrainSoldier)
        return;
      fromBuildingItem.GetComponentInChildren<CollectionUI>().Close();
    }
  }

  private void OnJobCompleted(object result)
  {
    Hashtable hashtable = result as Hashtable;
    int result1 = 0;
    if (hashtable == null)
      ;
    if (hashtable == null || !hashtable.ContainsKey((object) "event_type") || !int.TryParse(hashtable[(object) "event_type"].ToString(), out result1))
      return;
    if (result1 == 31)
    {
      long buildingId = long.Parse(hashtable[(object) "building_id"].ToString());
      CityManager.BuildingItem buildingItem = this.FindBuildingItem(buildingId);
      if (buildingItem != null && buildingItem.mBuildingJobID != 0L)
      {
        if (this.onDestroyBuilding != null)
          this.onDestroyBuilding(buildingId, buildingItem.mSlotid);
        this.buildings.Remove(buildingItem);
      }
      else
        D.warn((object) "Building ID {0} doesn't exist in city any more", (object) buildingId);
    }
    if (result1 != 30)
      return;
    CityManager.BuildingItem buildingItem1 = this.FindBuildingItem(long.Parse(hashtable[(object) "building_id"].ToString()));
    if (buildingItem1 == null || this.onUpgradeFinished == null)
      return;
    this.onUpgradeFinished(buildingItem1.mType, buildingItem1.mID);
  }

  private CityManager.BuildingItem AddBuildingItem(CityMapData building)
  {
    CityManager.BuildingItem buildingItem = this.FindBuildingItem(building.buildingId);
    if (buildingItem != null)
      return buildingItem;
    CityManager.BuildingItem building1 = new CityManager.BuildingItem(CityManager.BuildingItem.GetBuildingTypeByTypeId(building.typeId), building);
    this.SetBuildingResourceInfo(building1);
    this.m_Buildings.Add(building1);
    return building1;
  }

  public CityManager.BuildingItem FindBuildingItem(long buildingId)
  {
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mID == buildingId)
          return current;
      }
    }
    return (CityManager.BuildingItem) null;
  }

  private void PropergateCity(DateTime curSvrTime)
  {
    long cityId = (long) this.GetCityID();
    long uid = PlayerData.inst.uid;
    this.SetCityName(DBManager.inst.DB_City.Get(cityId).cityName);
    using (List<CityMapData>.Enumerator enumerator = DBManager.inst.DB_CityMap.GetByUid_CityId(cityId, uid).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem buildingItem = this.AddBuildingItem(enumerator.Current);
        string mType = buildingItem.mType;
        if (mType != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (CityManager.\u003C\u003Ef__switch\u0024map5B == null)
          {
            // ISSUE: reference to a compiler-generated field
            CityManager.\u003C\u003Ef__switch\u0024map5B = new Dictionary<string, int>(5)
            {
              {
                "stronghold",
                0
              },
              {
                "walls",
                1
              },
              {
                "PARADEGROUND",
                2
              },
              {
                "watchtower",
                3
              },
              {
                "dragon_lair",
                4
              }
            };
          }
          int num;
          // ISSUE: reference to a compiler-generated field
          if (CityManager.\u003C\u003Ef__switch\u0024map5B.TryGetValue(mType, out num))
          {
            switch (num)
            {
              case 0:
                this.mStronghold = buildingItem;
                continue;
              case 1:
                this.mWalls = buildingItem;
                continue;
              case 2:
                this.mParadeGround = buildingItem;
                continue;
              case 3:
                this.mWatchTower = buildingItem;
                continue;
              case 4:
                this.mTavern = buildingItem;
                continue;
              default:
                continue;
            }
          }
        }
      }
    }
    this.m_CityLoaded = true;
  }

  public void ClearOldDelegates()
  {
    this.onBuildingActionState = (CityManager.OnBuildingActionState) null;
    this.onCreateBuilding = (CityManager.OnCreateBuilding) null;
    this.onUpgradeBuilding = (CityManager.OnUpgradeBuilding) null;
    this.onUpgradeFinished = (CityManager.OnUpgradeFinished) null;
    this.onUpgradeCancelled = (CityManager.OnUpgradeFinished) null;
    this.onDestroyBuilding = (CityManager.OnDestroyBuilding) null;
  }

  public int GetCityID()
  {
    return this.m_CityId;
  }

  public int GetBuildingsInFlux()
  {
    int num = 0;
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mBuildingJobID != 0L || current.mLevel == 0)
        {
          JobHandle job = JobManager.Instance.GetJob(current.mBuildingJobID);
          if (job != null && !job.IsFinished())
            ++num;
        }
      }
    }
    return num;
  }

  public int GetBuildingsInNormalFlux()
  {
    int num = 0;
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mBuildingJobID != 0L || current.mLevel == 0)
        {
          JobHandle job = JobManager.Instance.GetJob(current.mBuildingJobID);
          if (job != null && !job.IsFinished())
          {
            Hashtable data = job.Data as Hashtable;
            if (data != null && data[(object) "queue"].ToString() == "free")
              ++num;
          }
        }
      }
    }
    return num;
  }

  public int GetBuildingsInAdditionlFlux()
  {
    int num = 0;
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mBuildingJobID != 0L || current.mLevel == 0)
        {
          JobHandle job = JobManager.Instance.GetJob(current.mBuildingJobID);
          if (job != null && !job.IsFinished())
          {
            Hashtable data = job.Data as Hashtable;
            if (data != null && data[(object) "queue"].ToString() == "pay")
              ++num;
          }
        }
      }
    }
    return num;
  }

  public int GetFreeSpeedUpTime()
  {
    return ConfigManager.inst.DB_BenefitCalc.GetFinalData(ConfigManager.inst.DB_GameConfig.GetData("default_free_speedup_time_value").ValueInt, "calc_speedup_time");
  }

  public CityManager.BuildingItem FindNormalFluxBuilding()
  {
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mBuildingJobID != 0L || current.mLevel == 0)
        {
          JobHandle job = JobManager.Instance.GetJob(current.mBuildingJobID);
          if (job != null && !job.IsFinished())
          {
            Hashtable data = job.Data as Hashtable;
            if (data != null && data[(object) "queue"].ToString() == "free")
              return current;
          }
        }
      }
    }
    return (CityManager.BuildingItem) null;
  }

  public CityManager.BuildingItem FindAdditionFluxBuilding()
  {
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mBuildingJobID != 0L || current.mLevel == 0)
        {
          JobHandle job = JobManager.Instance.GetJob(current.mBuildingJobID);
          if (job != null && !job.IsFinished())
          {
            Hashtable data = job.Data as Hashtable;
            if (data != null && data[(object) "queue"].ToString() == "pay")
              return current;
          }
        }
      }
    }
    return (CityManager.BuildingItem) null;
  }

  public CityManager.BuildingItem FindFluxBuilding()
  {
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mBuildingJobID != 0L || current.mLevel == 0)
        {
          JobHandle job = JobManager.Instance.GetJob(current.mBuildingJobID);
          if (job != null && !job.IsFinished())
            return current;
        }
      }
    }
    return (CityManager.BuildingItem) null;
  }

  public CityManager.BuildingItem FindFreeBuildQueueFluxBuilding()
  {
    CityManager.BuildingItem buildingItem = (CityManager.BuildingItem) null;
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mBuildingJobID != 0L || current.mLevel == 0)
        {
          JobHandle job = JobManager.Instance.GetJob(current.mBuildingJobID);
          if (job != null && (job.GetJobEvent() == JobEvent.JOB_BUILDING_CONSTRUCTION || job.GetJobEvent() == JobEvent.JOB_BUILDING_DECONSTRUCTION))
          {
            Hashtable data = job.Data as Hashtable;
            if (data != null && !job.IsFinished() && (data.ContainsKey((object) "queue") && data[(object) "queue"].ToString() == "free"))
            {
              buildingItem = current;
              return buildingItem;
            }
          }
        }
      }
    }
    return buildingItem;
  }

  public CityManager.BuildingItem FindShortestFluxBuilding()
  {
    CityManager.BuildingItem buildingItem = (CityManager.BuildingItem) null;
    float num = float.MaxValue;
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mBuildingJobID != 0L || current.mLevel == 0)
        {
          JobHandle job = JobManager.Instance.GetJob(current.mBuildingJobID);
          if (job != null && (double) job.LeftTime() < (double) num)
          {
            buildingItem = current;
            num = (float) job.LeftTime();
          }
        }
      }
    }
    return buildingItem;
  }

  public int GetIconLevelFromBuildingLevel(int buildingLevel, string type = null)
  {
    if (type == null)
      return 1;
    string key = type;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CityManager.\u003C\u003Ef__switch\u0024map5C == null)
      {
        // ISSUE: reference to a compiler-generated field
        CityManager.\u003C\u003Ef__switch\u0024map5C = new Dictionary<string, int>(1)
        {
          {
            "walls",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (CityManager.\u003C\u003Ef__switch\u0024map5C.TryGetValue(key, out num) && num == 0)
      {
        int[] numArray = new int[32]
        {
          1,
          1,
          1,
          1,
          1,
          1,
          2,
          2,
          2,
          2,
          2,
          3,
          3,
          3,
          3,
          3,
          4,
          4,
          4,
          4,
          4,
          5,
          5,
          5,
          5,
          5,
          5,
          5,
          5,
          5,
          5,
          5
        };
        if (buildingLevel >= numArray.Length)
          return 5;
        return numArray[buildingLevel];
      }
    }
    return 1;
  }

  public void GetBuildingLevelData(ArrayList ruralBuildings, ArrayList urbanBuildings)
  {
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        string mType = current.mType;
        if (mType != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (CityManager.\u003C\u003Ef__switch\u0024map5D == null)
          {
            // ISSUE: reference to a compiler-generated field
            CityManager.\u003C\u003Ef__switch\u0024map5D = new Dictionary<string, int>(7)
            {
              {
                "PARADEGROUND",
                0
              },
              {
                "farm",
                1
              },
              {
                "lumber_mill",
                1
              },
              {
                "mine",
                1
              },
              {
                "house",
                1
              },
              {
                "hospital",
                1
              },
              {
                "military_tent",
                1
              }
            };
          }
          int num;
          // ISSUE: reference to a compiler-generated field
          if (CityManager.\u003C\u003Ef__switch\u0024map5D.TryGetValue(mType, out num))
          {
            switch (num)
            {
              case 0:
                continue;
              case 1:
                ruralBuildings.Add((object) new StrongholdInfoDlg.BuildingLevelInfo(current.mType + "_name", 0, current.mLevel));
                continue;
            }
          }
        }
        urbanBuildings.Add((object) new StrongholdInfoDlg.BuildingLevelInfo(current.mType + "_name", 0, current.mLevel));
      }
    }
  }

  public List<CityManager.BuildingItem> GetBuildingsByType(string buildingType)
  {
    List<CityManager.BuildingItem> buildingItemList = new List<CityManager.BuildingItem>();
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mType == buildingType)
          buildingItemList.Add(current);
      }
    }
    return buildingItemList;
  }

  public void GetBuildingLevelDataOfType(string buildingType, ArrayList buildings)
  {
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mType == buildingType)
        {
          BuildingInfo data = ConfigManager.inst.DB_Building.GetData(current.mType, current.mLevel);
          buildings.Add((object) new StrongholdInfoDlg.BuildingLevelInfo(current.mType, (int) data.Benefit_Value_2, current.mLevel));
        }
      }
    }
  }

  public int GetBuildMaxLevel(string buildingType)
  {
    int num = 0;
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mType == buildingType)
          num = num >= current.mLevel ? num : current.mLevel;
      }
    }
    return num;
  }

  public int GetBuildingCount(string buildingType, bool finish)
  {
    if (buildingType == null)
      return this.m_Buildings.Count;
    int num = 0;
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mType == buildingType && (current.mLevel > 1 || JobManager.Instance.GetJob(current.mBuildingJobID) == null))
          ++num;
      }
    }
    return num;
  }

  public int GetBuildingCount(string buildingType = null)
  {
    if (buildingType == null)
      return this.m_Buildings.Count;
    int num = 0;
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.mType == buildingType)
          ++num;
      }
    }
    return num;
  }

  public int GetBuildingCountOverLevel(int level, string buildingType = null)
  {
    int num = 0;
    for (int index = 0; index < this.m_Buildings.Count; ++index)
    {
      if (this.m_Buildings[index] != null && !string.IsNullOrEmpty(this.m_Buildings[index].mType) && this.m_Buildings[index].mLevel >= level && (buildingType == null || this.m_Buildings[index].mType.Equals(buildingType)))
        ++num;
    }
    return num;
  }

  public CityManager.BuildingItem GetBuildingFromID(long buildingID)
  {
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mID == buildingID)
          return current;
      }
    }
    D.warn((object) "Building ID {0} not found", (object) buildingID);
    return (CityManager.BuildingItem) null;
  }

  public CityManager.BuildingItem GetBuildingFromType(string type)
  {
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mType == type)
          return current;
      }
    }
    return (CityManager.BuildingItem) null;
  }

  public int GetHighestBuildingLevelFor(string buildingType)
  {
    int a = -1;
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mType == buildingType)
          a = Mathf.Max(a, current.mLevel);
      }
    }
    return a;
  }

  public CityManager.BuildingItem GetBuildingWithActionTimer(string buildingType)
  {
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mType == buildingType && current.mTrainingJobID != 0L)
          return current;
      }
    }
    return (CityManager.BuildingItem) null;
  }

  public double GetTotalIncomeForResourceOfType(CityManager.ResourceTypes type)
  {
    double num = 0.0;
    string buildName = string.Empty;
    switch (type)
    {
      case CityManager.ResourceTypes.FOOD:
        buildName = "farm";
        break;
      case CityManager.ResourceTypes.SILVER:
        buildName = "house";
        break;
      case CityManager.ResourceTypes.WOOD:
        buildName = "lumber_mill";
        break;
      case CityManager.ResourceTypes.ORE:
        buildName = "mine";
        break;
    }
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mType == buildName)
        {
          BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildName, current.mLevel);
          num += data.Benefit_Value_2;
        }
      }
    }
    switch (type)
    {
      case CityManager.ResourceTypes.FOOD:
        return (double) ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) num, "calc_food_generation");
      case CityManager.ResourceTypes.SILVER:
        return (double) ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) num, "calc_silver_generation");
      case CityManager.ResourceTypes.WOOD:
        return (double) ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) num, "calc_wood_generation");
      case CityManager.ResourceTypes.ORE:
        return (double) ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) num, "calc_ore_generation");
      default:
        return num;
    }
  }

  public double GetTotalCapacityForResourceOfType(CityManager.ResourceTypes type)
  {
    double num1 = 0.0;
    string buildName = string.Empty;
    switch (type)
    {
      case CityManager.ResourceTypes.FOOD:
        buildName = "farm";
        break;
      case CityManager.ResourceTypes.SILVER:
        buildName = "house";
        break;
      case CityManager.ResourceTypes.WOOD:
        buildName = "lumber_mill";
        break;
      case CityManager.ResourceTypes.ORE:
        buildName = "mine";
        break;
    }
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        if (current.mType == buildName)
        {
          BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildName, current.mLevel);
          num1 += data.Benefit_Value_1;
        }
      }
    }
    BuildingInfo data1 = ConfigManager.inst.DB_Building.GetData("stronghold", this.GetHighestBuildingLevelFor("stronghold"));
    double num2 = num1 + data1.Benefit_Value_3;
    switch (type)
    {
      case CityManager.ResourceTypes.FOOD:
        return (double) ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) num2, "calc_food_storage");
      case CityManager.ResourceTypes.SILVER:
        return (double) ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) num2, "calc_silver_storage");
      case CityManager.ResourceTypes.WOOD:
        return (double) ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) num2, "calc_wood_storage");
      case CityManager.ResourceTypes.ORE:
        return (double) ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) num2, "calc_ore_storage");
      default:
        return num2;
    }
  }

  public long CityUpKeep
  {
    get
    {
      return this.cityUpKeep;
    }
  }

  private void OnBenefitChange(string obj)
  {
    this.CalculateUpKeep();
  }

  private void OnTroopChange(long obj)
  {
    this.CalculateUpKeep();
  }

  public void CalculateUpKeep()
  {
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    if (cityData == null)
      this.cityUpKeep = 0L;
    else
      this.cityUpKeep = cityData.totalTroops.totalUpkeep;
  }

  private void ResourceCollection()
  {
    if (this.onResourcesUpdated == null)
      return;
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    long currentResource1 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD);
    long currentResource2 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD);
    long currentResource3 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
    long currentResource4 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE);
    if (this.oldFood == currentResource1 && this.oldWood == currentResource2 && (this.oldSilver == currentResource3 && this.oldOre == currentResource4))
      return;
    this.oldFood = currentResource1;
    this.oldWood = currentResource2;
    this.oldSilver = currentResource3;
    this.oldOre = currentResource4;
    this.onResourcesUpdated(currentResource1, currentResource2, currentResource3, currentResource4, 0L);
  }

  public void OnSecond(int currTime)
  {
    this.ResourceCollection();
  }

  private void SetBuildingResourceInfo(CityManager.BuildingItem building)
  {
    string mType = building.mType;
    if (mType == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (CityManager.\u003C\u003Ef__switch\u0024map5E == null)
    {
      // ISSUE: reference to a compiler-generated field
      CityManager.\u003C\u003Ef__switch\u0024map5E = new Dictionary<string, int>(4)
      {
        {
          "farm",
          0
        },
        {
          "lumber_mill",
          0
        },
        {
          "mine",
          0
        },
        {
          "house",
          0
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!CityManager.\u003C\u003Ef__switch\u0024map5E.TryGetValue(mType, out num) || num != 0 || ConfigManager.inst.DB_Building.GetData(building.mType, building.mLevel) == null)
      return;
    building.mGatherRate = (float) ConfigManager.inst.DB_Building.GetData(building.mType, building.mLevel).Benefit_Value_2;
    building.mGatherRate /= 3600f;
    building.mStorage = ConfigManager.inst.DB_Building.GetData(building.mType, building.mLevel).Benefit_Value_1;
  }

  public int PVPMarchCapacity
  {
    get
    {
      int buildingLevelFor = this.GetHighestBuildingLevelFor("stronghold");
      if (buildingLevelFor >= 0)
      {
        BuildingInfo data = ConfigManager.inst.DB_Building.GetData("stronghold", buildingLevelFor);
        if (data != null)
          return (int) data.Benefit_Value_1;
      }
      return 0;
    }
  }

  public float GetHeroXPBonus()
  {
    int buildingLevelFor = this.GetHighestBuildingLevelFor("hero_tower");
    if (buildingLevelFor < 0)
    {
      D.warn((object) "Couldn't find Hero Tower on map!");
      return 0.0f;
    }
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData("hero_tower", buildingLevelFor);
    if (data != null)
      return (float) data.Benefit_Value_1 / 100f;
    D.warn((object) "Couldn't find Hero Tower level [{0}] in database!", (object) buildingLevelFor);
    return 0.0f;
  }

  public void CancelBuild(long buildingID, System.Action callback)
  {
    this._building_id = buildingID;
    this._cancellCallback = callback;
    MessageHub.inst.GetPortByAction("City:cancelBuild").SendRequest(Utils.Hash((object) "city_id", (object) CityManager.inst.GetCityID(), (object) "building_id", (object) buildingID), new System.Action<bool, object>(this.CancelBuildingCallback), true);
  }

  public void CancelBuildingCallback(bool ret, object data)
  {
    if (ret)
    {
      CityManager.BuildingItem buildingItem = this.FindBuildingItem(this._building_id);
      if (this.onUpgradeCancelled != null)
        this.onUpgradeCancelled(buildingItem.mType, this._building_id);
    }
    this._cancellCallback();
  }

  public void CancelDeconstruct(long buildingID, System.Action callback = null)
  {
    this._cancelDeconstructID = buildingID;
    this._cancellCallback = callback;
    MessageHub.inst.GetPortByAction("City:cancelBuild").SendRequest(Utils.Hash((object) "city_id", (object) CityManager.inst.GetCityID(), (object) "building_id", (object) this._cancelDeconstructID), new System.Action<bool, object>(this.CancelDeconstructCallback), true);
  }

  private void CancelDeconstructCallback(bool bOk, object data)
  {
    if (bOk)
    {
      CityManager.BuildingItem buildingItem = this.FindBuildingItem(this._cancelDeconstructID);
      if (this.onUpgradeCancelled != null)
        this.onUpgradeCancelled(buildingItem.mType, this._cancelDeconstructID);
    }
    if (this._cancellCallback == null)
      return;
    this._cancellCallback();
  }

  public void MoveBuilding(long buildingId, int slotid, bool bUseItem, System.Action callback)
  {
    if (bUseItem)
    {
      string str = "shopitem_building_deconstruct";
      bool flag = ItemBag.Instance.GetItemCountByShopID(str) <= 0;
      Hashtable extra = Utils.Hash((object) "building_id", (object) buildingId.ToString(), (object) "slot_id", (object) slotid);
      if (flag)
        ItemBag.Instance.BuyAndUseShopItem(str, extra, (System.Action<bool, object>) ((bOk, data) =>
        {
          if (!bOk)
            return;
          callback();
        }), 1);
      else
        ItemBag.Instance.UseItemByShopItemID(str, extra, (System.Action<bool, object>) ((bOk, data) =>
        {
          if (!bOk)
            return;
          callback();
        }), 1);
    }
    else
      callback();
  }

  public double GetCityPower()
  {
    double num = 0.0;
    using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CityManager.BuildingItem current = enumerator.Current;
        BuildingInfo data = ConfigManager.inst.DB_Building.GetData(current.mType, current.mLevel);
        num += (double) data.Power;
      }
    }
    return num;
  }

  public bool IsCityLoaded()
  {
    return this.m_CityLoaded;
  }

  public string GetName()
  {
    return this.mCityName;
  }

  public void SetCityName(string cityName)
  {
    this.mCityName = cityName;
    GameEngine.Instance.events.Publish_OnCityRenamed(cityName);
  }

  public void LoadCityIDs(object data)
  {
    ArrayList arrayList = data as ArrayList;
    if (arrayList.Count > 0)
      this.m_CityId = int.Parse(arrayList[0].ToString());
    else
      Debug.LogError((object) "No City ID found in initialization data returned from backend!");
  }

  public void LoadCityData()
  {
    long serverTimestamp = (long) NetServerTime.inst.ServerTimestamp;
    DateTime curSvrTime = new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();
    curSvrTime = curSvrTime.AddSeconds((double) serverTimestamp);
    this.PropergateCity(curSvrTime);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    this.m_CityLoaded = true;
  }

  public void BuildCityFromDB()
  {
    if (this.m_Buildings == null)
    {
      Debug.LogError((object) "City has not been initialized properly");
    }
    else
    {
      using (List<CityManager.BuildingItem>.Enumerator enumerator = this.m_Buildings.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          CityManager.BuildingItem current = enumerator.Current;
          if (this.onCreateBuilding != null)
            this.onCreateBuilding(current, current.mBuildingJobID);
        }
      }
    }
  }

  public void DefaultCallback(Hashtable para, WWW www, object json)
  {
    if (json == null || !(json is Hashtable))
      return;
    Hashtable hashtable = json as Hashtable;
    if (!hashtable[(object) "status"].Equals((object) 1.0))
      return;
    ResultLogger.logObject((object) hashtable);
    if (this.onBuildingActionState == null)
      return;
    this.onBuildingActionState(true);
  }

  public void TrainTroop(Unit_StatisticsInfo TPI, int tier, int troopCount, int gold, float trainBoost, long building_id, bool bInstant, Hashtable instHT, System.Action callback)
  {
    MessageHub.inst.GetPortByAction("City:trainTroop").SendRequest(Utils.Hash((object) "city_id", (object) this.m_CityId, (object) nameof (building_id), (object) building_id.ToString(), (object) "class", (object) TPI.ID, (object) "count", (object) troopCount, (object) "instant", (object) bInstant), (System.Action<bool, object>) ((bSuccess, data) =>
    {
      if (!bSuccess)
        return;
      callback();
    }), true);
  }

  public int GetTroopTypeCount(string classType)
  {
    return -1;
  }

  public bool OffsetTroopTypeCount(string classType, int inCount)
  {
    return true;
  }

  public void ProcessItem(bool bBuy, Hashtable parms, string itemID, System.Action<Hashtable, Hashtable> retCallback)
  {
    ++this.cbSerial;
    string str = "shopitem_building_relocate";
    parms.Add((object) "cbSerial", (object) this.cbSerial);
    parms.Add((object) "buy", (object) bBuy);
    if (bBuy)
      ItemBag.Instance.BuyAndUseShopItem(str, parms, (System.Action<bool, object>) ((arg1, arg2) => retCallback((Hashtable) null, (Hashtable) null)), 1);
    else
      ItemBag.Instance.UseItemByShopItemID(str, parms, (System.Action<bool, object>) ((_param1, _param2) => retCallback((Hashtable) null, (Hashtable) null)), 1);
  }

  private void ItemCallback(Hashtable ht, Hashtable sends)
  {
    if (!bool.Parse(ht[(object) "ret"].ToString()))
      return;
    string str = sends[(object) "cbSerial"].ToString();
    (this.callbacks[(object) str] as System.Action<Hashtable, Hashtable>)(ht, sends);
    this.callbacks.Remove((object) str);
  }

  public enum ResourceTypes
  {
    FOOD,
    SILVER,
    WOOD,
    ORE,
    GOLD,
  }

  public class BuildingItem
  {
    public float mCollected;
    public float mGatherRate;
    public double mStorage;
    public TimerHUDUIItem mBuildTimer;
    private CityMapData m_CityMapData;

    public BuildingItem(string type, CityMapData cityMapData)
    {
      this.mType = type;
      this.m_CityMapData = cityMapData;
    }

    public string mType { protected set; get; }

    public int mLevel
    {
      get
      {
        return this.m_CityMapData.level;
      }
    }

    public int mGloryLevel
    {
      get
      {
        return this.m_CityMapData.gloryLevel;
      }
    }

    public int mSlotid
    {
      get
      {
        return this.m_CityMapData.slotId;
      }
    }

    public long mID
    {
      get
      {
        return this.m_CityMapData.buildingId;
      }
    }

    public long mBuildingJobID
    {
      get
      {
        return this.m_CityMapData.jobId;
      }
    }

    public long mTrainingJobID
    {
      get
      {
        return this.m_CityMapData.taskJobId;
      }
    }

    public bool mInBuilding
    {
      get
      {
        return this.m_CityMapData.inBuilding;
      }
    }

    public long mCityId
    {
      get
      {
        return this.m_CityMapData.cityId;
      }
    }

    public long mBuildingId
    {
      get
      {
        return this.m_CityMapData.buildingId;
      }
    }

    public List<InBuildingData> inBuildingData
    {
      get
      {
        return this.m_CityMapData.inBuildingData;
      }
    }

    public static string GetBuildingTypeByTypeId(int typeId)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(typeId);
      if (data == null)
        return (string) null;
      string str = data.Type;
      if (str.IndexOf("Properties.ID") > -1)
        str = str.Substring("Properties.ID@".Length);
      return str;
    }

    public enum BuildState
    {
      BUILD_STATE_IDLE,
      BUILD_STATE_BUSY,
    }
  }

  public class ResourceItem
  {
    private int _maxValue;
    private int _value;
    private CityManager.ResourceTypes _resourceType;
    private int _internalID;

    public ResourceItem(CityManager.ResourceTypes resourceType)
    {
      this._resourceType = resourceType;
    }

    public int mMaxValue
    {
      get
      {
        return this._maxValue;
      }
      set
      {
        if (this._maxValue == value)
          return;
        int delta = value - this._maxValue;
        this._maxValue = value;
        GameEngine.Instance.events.Publish_OnResourceCapacityChanged(this._resourceType.ToString(), delta);
      }
    }

    public int mValue
    {
      get
      {
        return this._value;
      }
      set
      {
        if (this._value == value)
          return;
        int delta = value - this._value;
        this._value = value;
        GameEngine.Instance.events.Publish_OnResourceCountChanged(this._resourceType.ToString(), delta);
      }
    }

    public CityManager.ResourceTypes Type
    {
      get
      {
        return this._resourceType;
      }
    }

    public int InternalID
    {
      get
      {
        if (this._internalID == 0)
          this._internalID = this._resourceType != CityManager.ResourceTypes.FOOD ? (this._resourceType != CityManager.ResourceTypes.WOOD ? (this._resourceType != CityManager.ResourceTypes.ORE ? (this._resourceType != CityManager.ResourceTypes.SILVER ? 0 : ConfigManager.inst.DB_Lookup["silver"].InternalID) : ConfigManager.inst.DB_Lookup["ore"].InternalID) : ConfigManager.inst.DB_Lookup["wood"].InternalID) : ConfigManager.inst.DB_Lookup["food"].InternalID;
        return this._internalID;
      }
    }

    public void Init(int initialValue, int initialMaxValue)
    {
      this._value = initialValue;
      this._maxValue = initialMaxValue;
    }
  }

  public delegate void OnBuildingActionState(bool bEnable);

  public delegate void OnCreateBuilding(CityManager.BuildingItem buildingItem, long jobID);

  public delegate void OnUpgradeBuilding(CityManager.BuildingItem buildingItem, bool hasjob);

  public delegate void OnResourcesUpdated(long food, long wood, long silver, long ore, long gold);

  public delegate void OnUpgradeFinished(string type, long buildingId);

  public delegate void OnUpgradeCancelled(string type, long buildingId);

  public delegate void OnDestroyBuilding(long buildingId, int slotid);

  public delegate void OnClearCity();
}
