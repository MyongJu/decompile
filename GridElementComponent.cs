﻿// Decompiled with JetBrains decompiler
// Type: GridElementComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GridElementComponent : ComponentRenderBase
{
  public GameObject element;
  public UISprite backGround;

  public override void Init()
  {
    base.Init();
    GridElementComponentData data = this.data as GridElementComponentData;
    int a = 100;
    for (int index = 0; index < data.elements.Length; ++index)
    {
      UILabel componentInChildren = NGUITools.AddChild(this.gameObject, this.element).GetComponentInChildren<UILabel>();
      componentInChildren.text = data.elements[index];
      a = Mathf.Max(a, componentInChildren.height);
    }
    UIGrid componentInChildren1 = this.GetComponentInChildren<UIGrid>();
    if (!((Object) componentInChildren1 != (Object) null))
      return;
    componentInChildren1.cellWidth = (float) (data.totalWidth / data.elements.Length);
    componentInChildren1.maxPerLine = data.elements.Length;
    componentInChildren1.cellHeight = (float) a;
    componentInChildren1.Reposition();
  }
}
