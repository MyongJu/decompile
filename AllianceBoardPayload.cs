﻿// Decompiled with JetBrains decompiler
// Type: AllianceBoardPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class AllianceBoardPayload
{
  public long uid;
  public int portrait;
  public string name;
  public string content;
  public long ctime;
  public string icon;
  public int lordTitleId;

  public void Decode(object data)
  {
    Hashtable inData = data as Hashtable;
    DatabaseTools.UpdateData(inData, "uid", ref this.uid);
    DatabaseTools.UpdateData(inData, "portrait", ref this.portrait);
    DatabaseTools.UpdateData(inData, "name", ref this.name);
    DatabaseTools.UpdateData(inData, "content", ref this.content);
    DatabaseTools.UpdateData(inData, "ctime", ref this.ctime);
    DatabaseTools.UpdateData(inData, "icon", ref this.icon);
    DatabaseTools.UpdateData(inData, "lord_title", ref this.lordTitleId);
  }
}
