﻿// Decompiled with JetBrains decompiler
// Type: DragonTendencySkillLevelRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DragonTendencySkillLevelRenderer : MonoBehaviour
{
  public UILabel m_DragonLevelRequirement;
  public UILabel m_Bonus;
  public UILabel m_PointRequirement;

  public void SetData(int dragonLevelRequirement, int pointRequirement, float bonus)
  {
    this.m_DragonLevelRequirement.text = dragonLevelRequirement.ToString();
    this.m_Bonus.text = ((double) bonus * 100.0).ToString() + "%";
    this.m_PointRequirement.text = pointRequirement.ToString();
  }
}
