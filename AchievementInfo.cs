﻿// Decompiled with JetBrains decompiler
// Type: AchievementInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class AchievementInfo
{
  [Config(Name = "group_id")]
  public int GroupID = -1;
  [Config(Name = "formula")]
  public string Formula = string.Empty;
  [Config(Name = "value")]
  public int Value = -1;
  [Config(Name = "reward_id_1")]
  public int Item_ID = -1;
  [Config(Name = "reward_value_1")]
  public int ItemCount = -1;
  [Config(Name = "reward_id_2")]
  public int Lord_Item = -1;
  [Config(Name = "reward_value_2")]
  public int LordItemCount = -1;
  [Config(Name = "star")]
  public int Star = 1;
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "google_achievement")]
  public string GooglePlayGameId;
}
