﻿// Decompiled with JetBrains decompiler
// Type: RoomType
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class RoomType
{
  public const string BOSS = "boss";
  public const string MONSTER = "monster";
  public const string BUFF = "buff";
  public const string CHEST = "chest";
  public const string PVP = "pvp";
  public const string ARENA = "arena";
  public const string RAID_CHEST = "raid_chest";
}
