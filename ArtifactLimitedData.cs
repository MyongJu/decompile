﻿// Decompiled with JetBrains decompiler
// Type: ArtifactLimitedData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class ArtifactLimitedData
{
  private int _purchasedIndex;
  private int _artifactId;
  private int _cdTime;
  private int _endTime;
  private int _releaseCount;

  public int PurchasedIndex
  {
    get
    {
      return this._purchasedIndex;
    }
  }

  public int ArtifactId
  {
    get
    {
      return this._artifactId;
    }
  }

  public int CDTime
  {
    get
    {
      return this._cdTime;
    }
  }

  public int EndTime
  {
    get
    {
      return this._endTime;
    }
  }

  public int ReleaseCount
  {
    get
    {
      return this._releaseCount;
    }
  }

  public bool IsTimesLimited
  {
    get
    {
      ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this._artifactId);
      return artifactInfo != null && artifactInfo.activeEffect == "magic_source_stone";
    }
  }

  public void Decode(Hashtable data)
  {
    if (data == null)
      return;
    DatabaseTools.UpdateData(data, "purchased_index", ref this._purchasedIndex);
    DatabaseTools.UpdateData(data, "artifact_id", ref this._artifactId);
    DatabaseTools.UpdateData(data, "cd_time", ref this._cdTime);
    DatabaseTools.UpdateData(data, "end_time", ref this._endTime);
    DatabaseTools.UpdateData(data, "release_count", ref this._releaseCount);
  }

  public struct Params
  {
    public const string PURCHASED_INDEX = "purchased_index";
    public const string ARTIFACT_ID = "artifact_id";
    public const string CD_TIME = "cd_time";
    public const string END_TIME = "end_time";
    public const string RELEASE_COUNT = "release_count";
  }
}
