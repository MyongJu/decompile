﻿// Decompiled with JetBrains decompiler
// Type: AdjustProxy
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using com.adjust.sdk;
using Funplus;
using UnityEngine;

public class AdjustProxy
{
  private bool startManually = true;
  private bool printAttribution = true;
  private bool launchDeferredDeeplink = true;
  private string appToken;
  private string firstLaunchEventToken;
  private string trackerToken;
  private bool eventBuffering;
  private bool sendInBackground;
  private bool isInit;
  private static AdjustProxy _instance;

  public static AdjustProxy Instance
  {
    get
    {
      if (AdjustProxy._instance == null)
        AdjustProxy._instance = new AdjustProxy();
      return AdjustProxy._instance;
    }
  }

  public void SetConfig(string appToken, string firstLaunchEventToken, string trackerToken)
  {
    if (this.isInit)
      return;
    this.isInit = true;
    this.appToken = appToken;
    this.firstLaunchEventToken = firstLaunchEventToken;
    this.trackerToken = trackerToken;
    NetApi.inst.AdjustTracker = trackerToken;
    this.Setup();
  }

  private void Setup()
  {
    AdjustConfig adjustConfig = new AdjustConfig(this.appToken, this.GetEnironment());
    adjustConfig.setLogLevel(AdjustLogLevel.Suppress);
    if (!string.IsNullOrEmpty(this.trackerToken))
      adjustConfig.setDefaultTracker(this.trackerToken);
    adjustConfig.setSendInBackground(this.sendInBackground);
    adjustConfig.setEventBufferingEnabled(this.eventBuffering);
    adjustConfig.setLaunchDeferredDeeplink(this.launchDeferredDeeplink);
    adjustConfig.setAttributionChangedDelegate(new System.Action<AdjustAttribution>(this.AttributionChangedCallback), "Adjust");
    Adjust.start(adjustConfig);
    string adid = Adjust.getAdid();
    if (string.IsNullOrEmpty(adid))
      return;
    FunplusAdjust.Instance.trackEvent(adid);
  }

  private AdjustEnvironment GetEnironment()
  {
    AdjustEnvironment adjustEnvironment = AdjustEnvironment.Production;
    if (CustomDefine.IsProductionSDK())
      adjustEnvironment = AdjustEnvironment.Production;
    if (CustomDefine.IsSandboxSDK())
      adjustEnvironment = AdjustEnvironment.Sandbox;
    return adjustEnvironment;
  }

  public void SendOpenEvent()
  {
    string funplusId = AccountManager.Instance.FunplusID;
    AdjustEvent adjustEvent = new AdjustEvent(this.firstLaunchEventToken);
    adjustEvent.addCallbackParameter("fpid", funplusId);
    Adjust.addSessionCallbackParameter("fpid", funplusId);
    Adjust.trackEvent(adjustEvent);
  }

  public void TraceEvent(string token)
  {
    Adjust.trackEvent(new AdjustEvent(token));
  }

  private void AttributionChangedCallback(AdjustAttribution attributionData)
  {
    if (attributionData.trackerName == null)
      ;
    if (attributionData.clickLabel != null)
    {
      string clickLabel = attributionData.clickLabel;
      Debug.Log((object) ("adjust git label=" + clickLabel));
      AccountManager.Instance.AddGift("label", clickLabel);
    }
    string adid = Adjust.getAdid();
    if (string.IsNullOrEmpty(adid))
      return;
    FunplusAdjust.Instance.trackEvent(adid);
  }

  private void EventSuccessCallback(AdjustEventSuccess eventSuccessData)
  {
  }

  private void EventFailureCallback(AdjustEventFailure eventFailureData)
  {
  }
}
