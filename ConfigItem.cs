﻿// Decompiled with JetBrains decompiler
// Type: ConfigItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigItem
{
  private ConfigParseEx<ItemStaticInfo> _parse = new ConfigParseEx<ItemStaticInfo>();

  public void BuildDB(object raw)
  {
    this._parse.Initialize(raw as Hashtable);
  }

  public List<ItemStaticInfo> GetItemListByType(ItemBag.ItemType type)
  {
    List<ItemStaticInfo> itemStaticInfoList = new List<ItemStaticInfo>();
    List<int> internalIds = this._parse.InternalIds;
    for (int index = 0; index < internalIds.Count; ++index)
    {
      ItemStaticInfo itemStaticInfo = this.GetItem(internalIds[index]);
      if (itemStaticInfo.Type == type)
        itemStaticInfoList.Add(itemStaticInfo);
    }
    return itemStaticInfoList;
  }

  public void Clear()
  {
  }

  public bool Contains(string id)
  {
    return this._parse.Contains(id);
  }

  public bool Contains(int internalIds)
  {
    return this._parse.Contains(internalIds);
  }

  public ItemStaticInfo GetItem(int internalId)
  {
    ItemStaticInfo itemStaticInfo = this._parse.Get(internalId);
    if (itemStaticInfo != null)
      itemStaticInfo.internalId = internalId;
    return itemStaticInfo;
  }

  public ItemStaticInfo GetItem(string id)
  {
    return this.GetItem(this._parse.s2i(id));
  }
}
