﻿// Decompiled with JetBrains decompiler
// Type: ConfigBattleWounded
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigBattleWounded
{
  private Dictionary<string, ConfigBattleWoundedData> m_BattleWoundedByUniqueId;
  private Dictionary<int, ConfigBattleWoundedData> m_BattleWoundedByInternalId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ConfigBattleWoundedData, string>(res as Hashtable, "id", out this.m_BattleWoundedByUniqueId, out this.m_BattleWoundedByInternalId);
  }

  public ConfigBattleWoundedData GetData(int internalId)
  {
    ConfigBattleWoundedData battleWoundedData = (ConfigBattleWoundedData) null;
    this.m_BattleWoundedByInternalId.TryGetValue(internalId, out battleWoundedData);
    return battleWoundedData;
  }

  public ConfigBattleWoundedData GetData(string id)
  {
    ConfigBattleWoundedData battleWoundedData = (ConfigBattleWoundedData) null;
    this.m_BattleWoundedByUniqueId.TryGetValue(id, out battleWoundedData);
    return battleWoundedData;
  }

  public void Clear()
  {
    if (this.m_BattleWoundedByUniqueId != null)
    {
      this.m_BattleWoundedByUniqueId.Clear();
      this.m_BattleWoundedByUniqueId = (Dictionary<string, ConfigBattleWoundedData>) null;
    }
    if (this.m_BattleWoundedByInternalId == null)
      return;
    this.m_BattleWoundedByInternalId.Clear();
    this.m_BattleWoundedByInternalId = (Dictionary<int, ConfigBattleWoundedData>) null;
  }
}
