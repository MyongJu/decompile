﻿// Decompiled with JetBrains decompiler
// Type: ConfigMagicTowerMonster
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigMagicTowerMonster
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, MagicTowerMonsterInfo> datas;
  private Dictionary<int, MagicTowerMonsterInfo> dicByUniqueId;
  private List<int> _costItems;

  public void BuildDB(object res)
  {
    this.parse.Parse<MagicTowerMonsterInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  private int SortDelegate(ItemStaticInfo a, ItemStaticInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }

  public MagicTowerMonsterInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (MagicTowerMonsterInfo) null;
  }

  public MagicTowerMonsterInfo GetData(int internalId)
  {
    if (this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (MagicTowerMonsterInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, MagicTowerMonsterInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, MagicTowerMonsterInfo>) null;
  }
}
