﻿// Decompiled with JetBrains decompiler
// Type: ArtifactBuyInventoryDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ArtifactBuyInventoryDlg : UI.Dialog
{
  private Dictionary<int, ArtifactBuyInventoryItemRenderer> _itemDict = new Dictionary<int, ArtifactBuyInventoryItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private int _pickedArtifactPurchasedIndex = -1;
  public UILabel currency;
  public UILabel leftArtifactName;
  public UILabel leftArtifactEffectText;
  public UILabel leftArtifactEndTimeText;
  public UILabel leftArtifactCooldownTimeText;
  public UILabel leftArtifactTimesLimitedText;
  public UITexture leftArtifactTexture;
  public UITexture leftArtifactFrame;
  public EquipmentBenefits artifactBenefitContent;
  public GameObject noArtifactSelectedNode;
  public GameObject artifactSelectedNode;
  public GameObject leftArtifactEquipStatusNode;
  public UIScrollView leftArtifactDetailScrollView;
  public UITable leftArtifactDetailTable;
  public UIButton leftArtifactRemoveButton;
  public UIButton leftArtifactEquipButton;
  public UIButton leftArtifactReplaceButton;
  public ArtifactSpecialSaleView specialSaleView;
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.AddEventHandler();
    this.UpdateTips();
    this.UpdateLeftUI(false);
    this.UpdateUI(true);
    this.UpdateSpecialSaleUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.ClearData();
    this.specialSaleView.ClearData();
  }

  public void OnBuyGoldBtnPressed()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  public void OnArtifactRemoveBtnPressed()
  {
    ArtifactLimitedData dataByPurchasedId = PlayerData.inst.playerCityData.GetArtifactLimitedDataByPurchasedId(this._pickedArtifactPurchasedIndex);
    if (dataByPurchasedId == null)
      return;
    ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(dataByPurchasedId.ArtifactId);
    if (artifactInfo == null)
      return;
    ArtifactSalePayload.Instance.RemoveLimitedArtifact(this._pickedArtifactPurchasedIndex, artifactInfo.internalId, (System.Action<bool, object>) null);
  }

  public void OnArtifactEquipBtnPressed()
  {
    ArtifactLimitedData dataByPurchasedId = PlayerData.inst.playerCityData.GetArtifactLimitedDataByPurchasedId(this._pickedArtifactPurchasedIndex);
    if (dataByPurchasedId == null)
      return;
    ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(dataByPurchasedId.ArtifactId);
    if (artifactInfo == null)
      return;
    ArtifactSalePayload.Instance.EquipLimitedArtifact(this._pickedArtifactPurchasedIndex, artifactInfo.internalId, (System.Action<bool, object>) null);
  }

  public void OnArtifactReplaceBtnPressed()
  {
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(PlayerData.inst.uid);
    if (heroData == null)
      return;
    Dictionary<int, int>.ValueCollection.Enumerator enumerator = heroData.ArtifactsLimited.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      int current = enumerator.Current;
      ArtifactLimitedData dataByPurchasedId1 = PlayerData.inst.playerCityData.GetArtifactLimitedDataByPurchasedId(current);
      ArtifactLimitedData dataByPurchasedId2 = PlayerData.inst.playerCityData.GetArtifactLimitedDataByPurchasedId(this._pickedArtifactPurchasedIndex);
      if (dataByPurchasedId1 != null && dataByPurchasedId2 != null)
      {
        ArtifactInfo artifactInfo1 = ConfigManager.inst.DB_Artifact.Get(dataByPurchasedId1.ArtifactId);
        ArtifactInfo artifactInfo2 = ConfigManager.inst.DB_Artifact.Get(dataByPurchasedId2.ArtifactId);
        if (artifactInfo1 != null && artifactInfo2 != null)
          ArtifactSalePayload.Instance.ReplaceLimitedArtifact(current, artifactInfo1.internalId, this._pickedArtifactPurchasedIndex, artifactInfo2.internalId, (System.Action<bool, object>) null);
      }
    }
  }

  public void OnArtifactStoreBtnPressed()
  {
    ArtifactSalePayload.Instance.ShowArtifactSaleStore();
  }

  private void UpdateTips()
  {
    this.currency.text = Utils.FormatThousands(PlayerData.inst.userData.currency.gold.ToString());
  }

  private void UpdateLeftUI(bool forceHideLeftContent = false)
  {
    if (this._pickedArtifactPurchasedIndex == -1 && PlayerData.inst.heroData != null)
      this._pickedArtifactPurchasedIndex = PlayerData.inst.heroData.GetEquippedArtifactLimitedBuyIndex();
    NGUITools.SetActive(this.noArtifactSelectedNode, this._pickedArtifactPurchasedIndex <= -1 || forceHideLeftContent);
    NGUITools.SetActive(this.artifactSelectedNode, this._pickedArtifactPurchasedIndex > -1 && !forceHideLeftContent);
    NGUITools.SetActive(this.leftArtifactEquipStatusNode, false);
    NGUITools.SetActive(this.leftArtifactRemoveButton.gameObject, false);
    NGUITools.SetActive(this.leftArtifactEquipButton.gameObject, false);
    NGUITools.SetActive(this.leftArtifactReplaceButton.gameObject, false);
    Dictionary<int, ArtifactLimitedData> artifactsLimited = PlayerData.inst.playerCityData.ArtifactsLimited;
    if (artifactsLimited != null && artifactsLimited.ContainsKey(this._pickedArtifactPurchasedIndex))
    {
      ArtifactLimitedData artifactLimitedData = artifactsLimited[this._pickedArtifactPurchasedIndex];
      if (artifactLimitedData != null)
      {
        ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(artifactLimitedData.ArtifactId);
        if (artifactInfo != null)
        {
          this.leftArtifactName.text = artifactInfo.Name;
          if (artifactInfo.activeEffect == "magic_source_stone")
          {
            this.leftArtifactEffectText.text = artifactInfo.GetSpecialDescription((object) artifactInfo.deleteTypeParam, (object) artifactInfo.activeEffectValue);
            int num = (int) artifactInfo.activeEffectValue2 - artifactLimitedData.ReleaseCount;
            this.leftArtifactTimesLimitedText.text = ScriptLocalization.GetWithPara("artifact_temp_peace_shields_remaining", new Dictionary<string, string>()
            {
              {
                "0",
                string.Format(num > 0 ? "{0}/{1}" : "[ff0000]{0}/{1}[-]", (object) num, (object) (int) artifactInfo.activeEffectValue2)
              }
            }, true);
          }
          else
            this.leftArtifactEffectText.text = artifactInfo.Description;
          this.artifactBenefitContent.Init(artifactInfo.benefits);
          if ((UnityEngine.Object) this.leftArtifactFrame.mainTexture == (UnityEngine.Object) null || this.leftArtifactFrame.mainTexture.name != artifactInfo.TimeLimitedArtifactFrameIcon)
            BuilderFactory.Instance.HandyBuild((UIWidget) this.leftArtifactFrame, artifactInfo.TimeLimitedArtifactFramePath, (System.Action<bool>) null, true, false, string.Empty);
          if ((UnityEngine.Object) this.leftArtifactTexture.mainTexture == (UnityEngine.Object) null || this.leftArtifactTexture.mainTexture.name != artifactInfo.icon)
            BuilderFactory.Instance.HandyBuild((UIWidget) this.leftArtifactTexture, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
          NGUITools.SetActive(this.leftArtifactEffectText.transform.parent.gameObject, !string.IsNullOrEmpty(artifactInfo.Description));
        }
        this.leftArtifactEndTimeText.text = Utils.FormatTime(artifactLimitedData.EndTime - NetServerTime.inst.ServerTimestamp, true, false, true);
        this.leftArtifactCooldownTimeText.text = Utils.XLAT("id_cooldown") + Utils.FormatTime(artifactLimitedData.CDTime - NetServerTime.inst.ServerTimestamp, true, false, true);
        NGUITools.SetActive(this.leftArtifactCooldownTimeText.gameObject, artifactLimitedData.CDTime > 0);
        NGUITools.SetActive(this.leftArtifactTimesLimitedText.gameObject, artifactLimitedData.IsTimesLimited);
      }
    }
    Utils.ExecuteInSecs(0.01f, (System.Action) (() =>
    {
      this.leftArtifactDetailTable.repositionNow = true;
      this.leftArtifactDetailTable.Reposition();
      this.leftArtifactDetailScrollView.ResetPosition();
    }));
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(PlayerData.inst.uid);
    if (heroData == null)
      return;
    if (heroData.IsArtifactLimitedEquiped(this._pickedArtifactPurchasedIndex))
      NGUITools.SetActive(this.leftArtifactEquipStatusNode, true);
    if (!heroData.IsArtifactLimitedEquiped(-1))
      NGUITools.SetActive(this.leftArtifactEquipButton.gameObject, true);
    else if (heroData.IsArtifactLimitedEquiped(this._pickedArtifactPurchasedIndex))
      NGUITools.SetActive(this.leftArtifactRemoveButton.gameObject, true);
    else
      NGUITools.SetActive(this.leftArtifactReplaceButton.gameObject, true);
  }

  private void UpdateSpecialSaleUI()
  {
    this.specialSaleView.UpdateUI();
  }

  private void UpdateUI(bool forceRefreshScrollView = false)
  {
    this.ClearData();
    Dictionary<int, ArtifactLimitedData> artifactsLimited = PlayerData.inst.playerCityData.ArtifactsLimited;
    if (artifactsLimited != null)
    {
      Dictionary<int, ArtifactLimitedData>.KeyCollection.Enumerator enumerator = artifactsLimited.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (artifactsLimited[enumerator.Current].EndTime >= NetServerTime.inst.ServerTimestamp)
        {
          ArtifactBuyInventoryItemRenderer itemRenderer = this.CreateItemRenderer(enumerator.Current, artifactsLimited[enumerator.Current]);
          this._itemDict.Add(enumerator.Current, itemRenderer);
        }
      }
    }
    this.Reposition(forceRefreshScrollView);
  }

  private void OnPurchasedArtifactPressed(int purchasedIndex)
  {
    Dictionary<int, ArtifactBuyInventoryItemRenderer>.KeyCollection.Enumerator enumerator = this._itemDict.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (purchasedIndex != enumerator.Current && (UnityEngine.Object) this._itemDict[enumerator.Current] != (UnityEngine.Object) null)
        this._itemDict[enumerator.Current].RefreshUI(false, -1);
    }
    this._pickedArtifactPurchasedIndex = purchasedIndex;
    this.UpdateLeftUI(false);
  }

  private void ClearData()
  {
    using (Dictionary<int, ArtifactBuyInventoryItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, ArtifactBuyInventoryItemRenderer> current = enumerator.Current;
        current.Value.onArtifactPressed -= new System.Action<int>(this.OnPurchasedArtifactPressed);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition(bool forceRefreshScrollView = false)
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    if (forceRefreshScrollView)
      this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private ArtifactBuyInventoryItemRenderer CreateItemRenderer(int purchasedIndex, ArtifactLimitedData artifactLimitedData)
  {
    GameObject gameObject = this._itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    ArtifactBuyInventoryItemRenderer component = gameObject.GetComponent<ArtifactBuyInventoryItemRenderer>();
    component.SetData(purchasedIndex, artifactLimitedData, this._pickedArtifactPurchasedIndex);
    component.onArtifactPressed += new System.Action<int>(this.OnPurchasedArtifactPressed);
    return component;
  }

  private void OnSecondEvent(int time)
  {
    Dictionary<int, ArtifactLimitedData> artifactsLimited = PlayerData.inst.playerCityData.ArtifactsLimited;
    if (artifactsLimited != null && artifactsLimited.ContainsKey(this._pickedArtifactPurchasedIndex))
    {
      ArtifactLimitedData artifactLimitedData = artifactsLimited[this._pickedArtifactPurchasedIndex];
      if (artifactLimitedData != null)
      {
        this.leftArtifactEndTimeText.text = Utils.FormatTime(artifactLimitedData.EndTime - NetServerTime.inst.ServerTimestamp, true, false, true);
        this.leftArtifactCooldownTimeText.text = Utils.XLAT("id_cooldown") + Utils.FormatTime(artifactLimitedData.CDTime - NetServerTime.inst.ServerTimestamp, true, false, true);
      }
    }
    else
      this.UpdateLeftUI(true);
    ArtifactLimitedData dataByPurchasedId = PlayerData.inst.playerCityData.GetArtifactLimitedDataByPurchasedId(this._pickedArtifactPurchasedIndex);
    if (dataByPurchasedId == null || dataByPurchasedId.EndTime < NetServerTime.inst.ServerTimestamp)
      this._pickedArtifactPurchasedIndex = -1;
    if (this._itemDict == null)
      return;
    Dictionary<int, ArtifactBuyInventoryItemRenderer>.ValueCollection.Enumerator enumerator = this._itemDict.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.CurrentArtifactLimitedData != null && enumerator.Current.CurrentArtifactLimitedData.EndTime < NetServerTime.inst.ServerTimestamp)
      {
        if (enumerator.Current.CurrentArtifactLimitedData.PurchasedIndex == this._pickedArtifactPurchasedIndex)
        {
          this._pickedArtifactPurchasedIndex = -1;
          this.UpdateLeftUI(true);
        }
        else
          this.UpdateLeftUI(false);
        this.UpdateUI(false);
        break;
      }
    }
  }

  private void OnCityDataUpdated(long cityId)
  {
    if (cityId != (long) PlayerData.inst.cityId)
      return;
    this.UpdateLeftUI(false);
    this.UpdateUI(false);
  }

  private void OnHeroDataUpdated(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.UpdateLeftUI(false);
    this.RefreshArtifactStatus();
  }

  private void OnUserDataUpdated(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.UpdateTips();
  }

  private void RefreshArtifactStatus()
  {
    Dictionary<int, ArtifactBuyInventoryItemRenderer>.KeyCollection.Enumerator enumerator = this._itemDict.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if ((UnityEngine.Object) this._itemDict[enumerator.Current] != (UnityEngine.Object) null)
        this._itemDict[enumerator.Current].RefreshUI(true, this._pickedArtifactPurchasedIndex);
    }
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
    DBManager.inst.DB_City.onDataUpdate += new System.Action<long>(this.OnCityDataUpdated);
    DBManager.inst.DB_hero.onDataUpdated += new System.Action<long>(this.OnHeroDataUpdated);
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdated);
  }

  private void RemoveEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    DBManager.inst.DB_City.onDataUpdate -= new System.Action<long>(this.OnCityDataUpdated);
    DBManager.inst.DB_hero.onDataUpdated -= new System.Action<long>(this.OnHeroDataUpdated);
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdated);
  }
}
