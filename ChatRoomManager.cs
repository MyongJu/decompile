﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Chat;
using DB;
using System.Collections.Generic;

public class ChatRoomManager
{
  public const string ACCEPT_CMD = "accept";
  public const string DENY_CMD = "deny";

  public static void InviteJoinRoom(long invite_uid, long roomId)
  {
    MessageHub.inst.GetPortByAction("Chat:inviteJoinRoom").SendRequest(Utils.Hash((object) nameof (invite_uid), (object) invite_uid, (object) "room_id", (object) roomId), (System.Action<bool, object>) null, true);
  }

  public static void AcceptOrDenyInvite(long roomId, string cmd, ChatRoomData roomData)
  {
    MessageHub.inst.GetPortByAction("Chat:acceptOrDenyInvite").SendRequest(Utils.Hash((object) "room_id", (object) roomId, (object) nameof (cmd), (object) cmd), (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1 || !(cmd == "accept"))
        return;
      ChatMessageManager.SendChatMemberWelcomMessage(string.Empty, string.Empty, "chat_room_welcome_3_description", roomData.channelId);
    }), true);
  }

  public static void CancelApply(long roomId)
  {
    MessageHub.inst.GetPortByAction("Chat:cancelApply").SendRequest(Utils.Hash((object) "room_id", (object) roomId), (System.Action<bool, object>) null, true);
  }

  public static List<ChatRoomData> ApplyingRooms()
  {
    List<ChatRoomData> chatRoomDataList = new List<ChatRoomData>();
    using (Dictionary<long, ChatRoomData>.Enumerator enumerator1 = DBManager.inst.DB_room._datas.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        KeyValuePair<long, ChatRoomData> current1 = enumerator1.Current;
        using (Dictionary<long, ChatRoomMember>.Enumerator enumerator2 = current1.Value.members.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            KeyValuePair<long, ChatRoomMember> current2 = enumerator2.Current;
            if (current2.Value.uid == PlayerData.inst.uid && current2.Value.title == ChatRoomMember.Title.application)
              chatRoomDataList.Add(current1.Value);
          }
        }
      }
    }
    return chatRoomDataList;
  }

  public static List<ChatRoomData> InvitingRooms()
  {
    List<ChatRoomData> chatRoomDataList = new List<ChatRoomData>();
    using (Dictionary<long, ChatRoomData>.Enumerator enumerator1 = DBManager.inst.DB_room._datas.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        KeyValuePair<long, ChatRoomData> current1 = enumerator1.Current;
        using (Dictionary<long, ChatRoomMember>.Enumerator enumerator2 = current1.Value.members.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            KeyValuePair<long, ChatRoomMember> current2 = enumerator2.Current;
            if (current2.Value.uid == PlayerData.inst.uid && current2.Value.title == ChatRoomMember.Title.beinvited)
              chatRoomDataList.Add(current1.Value);
          }
        }
      }
    }
    return chatRoomDataList;
  }

  public static List<ChatRoomData> ChattingRooms()
  {
    List<ChatRoomData> chatRoomDataList = new List<ChatRoomData>();
    using (Dictionary<long, ChatRoomData>.Enumerator enumerator1 = DBManager.inst.DB_room._datas.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        KeyValuePair<long, ChatRoomData> current1 = enumerator1.Current;
        using (Dictionary<long, ChatRoomMember>.Enumerator enumerator2 = current1.Value.members.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            KeyValuePair<long, ChatRoomMember> current2 = enumerator2.Current;
            if (current2.Value.uid == PlayerData.inst.uid && current2.Value.IsNormalMember())
              chatRoomDataList.Add(current1.Value);
          }
        }
      }
    }
    return chatRoomDataList;
  }

  public static bool HasApplyingMyRoom()
  {
    using (List<ChatRoomData>.Enumerator enumerator = ChatRoomManager.ChattingRooms().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ChatRoomData current = enumerator.Current;
        if (current.members[PlayerData.inst.uid].IsManageMember() && current.ApplyingCount() > 0)
          return true;
      }
    }
    return false;
  }

  public static bool HasInviting()
  {
    return ChatRoomManager.InvitingRooms().Count > 0;
  }

  public static bool HasApplying()
  {
    return ChatRoomManager.ApplyingRooms().Count > 0;
  }

  public static int UnReadRoomMessageCount()
  {
    int num = 0;
    using (List<ChatRoomData>.Enumerator enumerator = ChatRoomManager.ChattingRooms().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ChatChannel chatChannel = GameEngine.Instance.ChatManager.GetChatChannel(enumerator.Current.channelId, 0L);
        if (chatChannel != null)
          num += chatChannel.unreadCount;
      }
    }
    return num;
  }
}
