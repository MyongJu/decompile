﻿// Decompiled with JetBrains decompiler
// Type: StoreHouseBuildingInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UI;
using UnityEngine;

public class StoreHouseBuildingInfoDlg : BuildingInfoBaseDlg
{
  public GameObject wood;
  public GameObject food;
  public GameObject ore;
  public GameObject silver;
  public UITexture texture1;
  public UILabel nameLabel1;
  public UILabel valueLabel1;
  public UITexture texture2;
  public UILabel nameLabel2;
  public UILabel valueLabel2;
  public UITexture texture3;
  public UILabel nameLabel3;
  public UILabel valueLabel3;
  public UITexture texture4;
  public UILabel nameLabel4;
  public UILabel valueLabel4;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    int num1 = 0;
    int num2 = 0;
    int num3 = 10;
    int num4 = 15;
    int mLevel = PlayerData.inst.CityData.mStronghold.mLevel;
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_food");
    if (data1 != null)
      num1 = data1.ValueInt;
    GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_wood");
    if (data2 != null)
      num2 = data2.ValueInt;
    GameConfigInfo data3 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_ore");
    if (data3 != null)
      num3 = data3.ValueInt;
    GameConfigInfo data4 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_silver");
    if (data4 != null)
      num4 = data4.ValueInt;
    if (mLevel >= num2)
      this.DrawStoreage("wood_load", this.nameLabel1, Utils.XLAT("id_wood"), this.valueLabel1);
    else
      this.wood.SetActive(false);
    if (mLevel >= num1)
      this.DrawStoreage("food_load", this.nameLabel2, Utils.XLAT("id_food"), this.valueLabel2);
    else
      this.food.SetActive(false);
    if (mLevel >= num3)
      this.DrawStoreage("ore_load", this.nameLabel3, Utils.XLAT("id_ore"), this.valueLabel3);
    else
      this.ore.SetActive(false);
    if (mLevel >= num4)
      this.DrawStoreage("silver_load", this.nameLabel4, Utils.XLAT("id_silver"), this.valueLabel4);
    else
      this.silver.SetActive(false);
  }

  private void DrawStoreage(string loadSetting, UILabel nameLabel, string nameLabelString, UILabel valueLabel)
  {
    nameLabel.text = nameLabelString;
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[int.Parse(this.buildingInfo.Benefit_ID_1)];
    int valueInt = ConfigManager.inst.DB_GameConfig.GetData(loadSetting).ValueInt;
    float benefitValue1 = (float) this.buildingInfo.Benefit_Value_1;
    float finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(DBManager.inst.DB_Local_Benefit.Get("prop_storage_protect_base_value").total, "calc_storage_protect");
    string str = (double) finalData < (double) benefitValue1 ? "-" : string.Empty;
    if (dbProperty.Format == PropertyDefinition.FormatType.Percent)
      valueLabel.text = dbProperty.ConvertToDisplayString((double) benefitValue1 / (double) valueInt, false, true) + str + dbProperty.ConvertToDisplayString((double) Math.Abs((finalData - benefitValue1) / (float) valueInt), true, true);
    else
      valueLabel.text = dbProperty.ConvertToDisplayString((double) benefitValue1 / (double) valueInt, false, true) + str + dbProperty.ConvertToDisplayString((double) Math.Abs((finalData - benefitValue1) / (float) valueInt), true, true);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    BuilderFactory.Instance.Release((UIWidget) this.texture1);
    base.OnClose(orgParam);
  }
}
