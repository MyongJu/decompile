﻿// Decompiled with JetBrains decompiler
// Type: LotteryDiyConfirmPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class LotteryDiyConfirmPopup : Popup
{
  public UILabel titleText;
  public UILabel contentText;
  public UILabel buttonText;
  private string _title;
  private string _content;
  private string _buttonText;
  private System.Action _onOkayButtonPressed;
  private System.Action _onCloseButtonPressed;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    LotteryDiyConfirmPopup.Parameter parameter = orgParam as LotteryDiyConfirmPopup.Parameter;
    if (parameter != null)
    {
      this._title = parameter.title;
      this._content = parameter.content;
      this._buttonText = parameter.buttonText;
      this._onOkayButtonPressed = parameter.onOkayButtonPressed;
      this._onCloseButtonPressed = parameter.onCloseButtonPressed;
    }
    this.UpdateUI();
  }

  public void OnOkayBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    if (this._onOkayButtonPressed == null)
      return;
    this._onOkayButtonPressed();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    if (this._onCloseButtonPressed == null)
      return;
    this._onCloseButtonPressed();
  }

  private void UpdateUI()
  {
    this.titleText.text = this._title;
    this.contentText.text = this._content;
    this.buttonText.text = this._buttonText;
  }

  public class Parameter : Popup.PopupParameter
  {
    public string title;
    public string content;
    public string buttonText;
    public System.Action onOkayButtonPressed;
    public System.Action onCloseButtonPressed;
  }
}
