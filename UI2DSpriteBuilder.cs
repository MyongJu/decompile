﻿// Decompiled with JetBrains decompiler
// Type: UI2DSpriteBuilder
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UI2DSpriteBuilder : BaseBuilder
{
  private UI2DSprite uiTexture;

  protected override void Prepare()
  {
    if ((Object) this.Target == (Object) null || !((Object) this.uiTexture == (Object) null))
      return;
    this.uiTexture = this.Target.GetComponent<UI2DSprite>();
    this.IsSuccess = false;
    this.uiTexture.sprite2D = (UnityEngine.Sprite) null;
  }

  protected override void Clear()
  {
    if (!((Object) this.uiTexture != (Object) null))
      return;
    this.uiTexture = (UI2DSprite) null;
  }

  protected override string GetPath()
  {
    return this.ImageName;
  }

  protected override void ResultHandler(Object result)
  {
    Texture2D texture = result as Texture2D;
    if ((Object) texture != (Object) null)
    {
      this.IsSuccess = true;
      this.uiTexture.sprite2D = UnityEngine.Sprite.Create(texture, new Rect(0.0f, 0.0f, (float) texture.width, (float) texture.height), new Vector2(0.5f, 0.5f));
    }
    else
    {
      this.IsSuccess = false;
      this.uiTexture.sprite2D = (UnityEngine.Sprite) null;
    }
  }
}
