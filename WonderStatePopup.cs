﻿// Decompiled with JetBrains decompiler
// Type: WonderStatePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UI;
using UnityEngine;

public class WonderStatePopup : Popup
{
  private bool isWonder = true;
  public UILabel title;
  public UIButtonBar bar;
  public GameObject[] contents;
  public UILabel desc;
  public UILabel remainTime;
  public WonderOccupyDetail occupy;
  public WonderRecordDetail recoders;
  public WonderHallOfFameDetail hallOfFame;
  private StateUIBaseData data;
  private int worldId;
  private long towerId;
  public GameObject normal;
  public GameObject unOpen;
  public UILabel unopenLabel;
  public GameObject content;
  public UITexture image;
  private object payload;
  private bool isDestroy;

  private void HildeTabContents()
  {
    for (int index = 0; index < this.contents.Length; ++index)
      NGUITools.SetActive(this.contents[index], false);
  }

  private void OnSelectedItem(int index)
  {
    switch (index)
    {
      case 1:
        this.recoders.SetData(this.data);
        break;
      case 2:
        this.hallOfFame.SetData(this.data);
        break;
    }
    this.HildeTabContents();
    NGUITools.SetActive(this.contents[index], true);
  }

  private void AddEventHandler()
  {
    this.bar.OnSelectedHandler = new System.Action<int>(this.OnSelectedItem);
  }

  private void RemoveEventHandler()
  {
    this.bar.OnSelectedHandler = (System.Action<int>) null;
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void Clear()
  {
    this.payload = (object) null;
    this.occupy.Clear();
    this.recoders.Clear();
    this.hallOfFame.Clear();
    BuilderFactory.Instance.Release((UIWidget) this.image);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    WonderStatePopup.Parameter parameter = orgParam as WonderStatePopup.Parameter;
    this.worldId = parameter.worldId;
    this.towerId = parameter.townerId;
    this.LoadData(parameter.worldId, parameter.townerId);
    BuilderFactory.Instance.Build((UIWidget) this.image, "Texture/GUI_Textures/avalon_state_top_banner", (System.Action<bool>) null, true, false, true, string.Empty);
  }

  private void OnDestroy()
  {
    this.isDestroy = true;
  }

  private void LoadData(int worldId, long townerId = 0)
  {
    string action = "wonder:loadWonderData";
    Hashtable postData = Utils.Hash((object) "kingdom_id", (object) worldId);
    if (DBManager.inst.DB_Wonder.Get((long) worldId).State == "unOpen")
    {
      string Term = "throne_introduction_description";
      if (townerId > 0L)
        Term = "throne_coming_soon_announcement";
      this.unopenLabel.text = ScriptLocalization.Get(Term, true);
      NGUITools.SetActive(this.unOpen, true);
      NGUITools.SetActive(this.normal, false);
    }
    else
    {
      NGUITools.SetActive(this.unOpen, false);
      NGUITools.SetActive(this.normal, true);
    }
    if (townerId > 0L)
    {
      this.isWonder = false;
      action = "wonder:loadWonderTowerData";
      postData.Add((object) "tower_id", (object) townerId);
    }
    RequestManager.inst.SendRequest(action, postData, new System.Action<bool, object>(this.OnLoadDataCallback), true);
  }

  private void OnLoadDataCallback(bool success, object result)
  {
    if (this.isDestroy)
      return;
    this.payload = result;
    this.CreateData();
    this.UpdateUI();
    this.AddEventHandler();
    this.bar.SelectedIndex = 0;
  }

  private void CreateData()
  {
    this.data = (StateUIBaseData) null;
    if (this.towerId > 0L)
    {
      WonderTowerData wonderTowerData = DBManager.inst.DB_WonderTower.Get(this.towerId, (long) this.worldId);
      this.data = (StateUIBaseData) new TowerStateUIData();
      this.data.Data = (object) wonderTowerData;
    }
    else
    {
      if (this.worldId <= 0)
        return;
      DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) this.worldId);
      this.data = (StateUIBaseData) new WonderStateUIData();
      this.data.Data = (object) wonderData;
    }
  }

  private void UpdateUI()
  {
    if (this.isDestroy)
      return;
    NGUITools.SetActive(this.content, true);
    this.occupy.SetData(this.data, this.payload);
    this.title.text = this.data.Title;
    this.remainTime.text = Utils.FormatTime(this.data.RemainTime, false, false, true);
    this.desc.text = this.data.Desc;
  }

  private void Update()
  {
    if (this.isDestroy || this.data == null)
      return;
    DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) this.worldId);
    int time = 28800;
    if (wonderData == null)
      return;
    if (wonderData.State == "fighting")
    {
      if (wonderData.OWNER_ALLIANCE_ID > 0L)
        time = this.data.RemainTime;
    }
    else
      time = this.data.RemainTime;
    this.remainTime.text = Utils.FormatTime(time, false, false, true);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Clear();
    this.RemoveEventHandler();
    this.data = (StateUIBaseData) null;
  }

  public class Parameter : Popup.PopupParameter
  {
    public int worldId;
    public long townerId;
  }
}
