﻿// Decompiled with JetBrains decompiler
// Type: SqliteDatabase
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

public class SqliteDatabase
{
  private bool CanExQuery = true;
  private const int SQLITE_OK = 0;
  private const int SQLITE_ROW = 100;
  private const int SQLITE_DONE = 101;
  private const int SQLITE_INTEGER = 1;
  private const int SQLITE_FLOAT = 2;
  private const int SQLITE_TEXT = 3;
  private const int SQLITE_BLOB = 4;
  private const int SQLITE_NULL = 5;
  public static string StreamingAssetsPath;
  public static string PersistentDataPath;
  private IntPtr _connection;
  private string pathDB;

  public SqliteDatabase(string dbName)
  {
    if (string.IsNullOrEmpty(SqliteDatabase.PersistentDataPath))
      SqliteDatabase.PersistentDataPath = Application.persistentDataPath;
    if (string.IsNullOrEmpty(SqliteDatabase.StreamingAssetsPath))
      SqliteDatabase.StreamingAssetsPath = Application.streamingAssetsPath;
    this.pathDB = Path.Combine(SqliteDatabase.PersistentDataPath, dbName);
    string str = Path.Combine(SqliteDatabase.StreamingAssetsPath, dbName);
    if (File.Exists(this.pathDB) && !(File.GetLastWriteTimeUtc(str) > File.GetLastWriteTimeUtc(this.pathDB)))
      return;
    if (str.Contains("://"))
    {
      WWW www = new WWW(str);
      do
        ;
      while (!www.isDone);
      if (string.IsNullOrEmpty(www.error))
        File.WriteAllBytes(this.pathDB, www.bytes);
      else
        this.CanExQuery = false;
    }
    else if (File.Exists(str))
    {
      File.Copy(str, this.pathDB, true);
    }
    else
    {
      this.CanExQuery = false;
      Debug.Log((object) ("ERROR: the file DB named " + dbName + " doesn't exist in the StreamingAssets Folder, please copy it there."));
    }
  }

  [DllImport("sqlite3")]
  private static extern int sqlite3_open(string filename, out IntPtr db);

  [DllImport("sqlite3")]
  private static extern int sqlite3_close(IntPtr db);

  [DllImport("sqlite3")]
  private static extern int sqlite3_prepare_v2(IntPtr db, string zSql, int nByte, out IntPtr ppStmpt, IntPtr pzTail);

  [DllImport("sqlite3")]
  private static extern int sqlite3_step(IntPtr stmHandle);

  [DllImport("sqlite3")]
  private static extern int sqlite3_finalize(IntPtr stmHandle);

  [DllImport("sqlite3")]
  private static extern IntPtr sqlite3_errmsg(IntPtr db);

  [DllImport("sqlite3")]
  private static extern int sqlite3_column_count(IntPtr stmHandle);

  [DllImport("sqlite3")]
  private static extern IntPtr sqlite3_column_name(IntPtr stmHandle, int iCol);

  [DllImport("sqlite3")]
  private static extern int sqlite3_column_type(IntPtr stmHandle, int iCol);

  [DllImport("sqlite3")]
  private static extern int sqlite3_column_int(IntPtr stmHandle, int iCol);

  [DllImport("sqlite3")]
  private static extern IntPtr sqlite3_column_text(IntPtr stmHandle, int iCol);

  [DllImport("sqlite3")]
  private static extern double sqlite3_column_double(IntPtr stmHandle, int iCol);

  [DllImport("sqlite3")]
  private static extern IntPtr sqlite3_column_blob(IntPtr stmHandle, int iCol);

  [DllImport("sqlite3")]
  private static extern int sqlite3_column_bytes(IntPtr stmHandle, int iCol);

  private bool IsConnectionOpen { get; set; }

  public static void Setup()
  {
    SqliteDatabase.PersistentDataPath = Application.persistentDataPath;
    SqliteDatabase.StreamingAssetsPath = Application.streamingAssetsPath;
  }

  private void Open()
  {
    this.Open(this.pathDB);
  }

  private void Open(string path)
  {
    if (this.IsConnectionOpen)
      throw new SqliteException("There is already an open connection");
    try
    {
      if (SqliteDatabase.sqlite3_open(path, out this._connection) != 0)
        throw new SqliteException("Could not open database file: " + path);
    }
    catch (Exception ex)
    {
      this.CanExQuery = false;
    }
    finally
    {
      this.IsConnectionOpen = true;
      this.CanExQuery = true;
    }
  }

  private void Close()
  {
    if (this.IsConnectionOpen)
      SqliteDatabase.sqlite3_close(this._connection);
    this.IsConnectionOpen = false;
  }

  public void ExecuteNonQuery(string query)
  {
    if (!this.CanExQuery)
    {
      Debug.Log((object) "ERROR: Can't execute the query, verify DB origin file");
    }
    else
    {
      this.Open();
      if (!this.IsConnectionOpen)
        throw new SqliteException("SQLite database is not open.");
      IntPtr stmHandle = this.Prepare(query);
      if (SqliteDatabase.sqlite3_step(stmHandle) != 101)
        throw new SqliteException("Could not execute SQL statement.");
      this.Finalize(stmHandle);
      this.Close();
    }
  }

  public DataTable ExecuteQuery(string query)
  {
    if (!this.CanExQuery)
    {
      Debug.Log((object) "ERROR: Can't execute the query, verify DB origin file");
      return (DataTable) null;
    }
    this.Open();
    if (!this.IsConnectionOpen)
      throw new SqliteException("SQLite database is not open.");
    IntPtr stmHandle = this.Prepare(query);
    int length1 = SqliteDatabase.sqlite3_column_count(stmHandle);
    DataTable dataTable = new DataTable();
    for (int iCol = 0; iCol < length1; ++iCol)
    {
      string stringAnsi = Marshal.PtrToStringAnsi(SqliteDatabase.sqlite3_column_name(stmHandle, iCol));
      dataTable.Columns.Add(stringAnsi);
    }
    while (SqliteDatabase.sqlite3_step(stmHandle) == 100)
    {
      object[] values = new object[length1];
      for (int iCol = 0; iCol < length1; ++iCol)
      {
        switch (SqliteDatabase.sqlite3_column_type(stmHandle, iCol))
        {
          case 1:
            values[iCol] = (object) SqliteDatabase.sqlite3_column_int(stmHandle, iCol);
            break;
          case 2:
            values[iCol] = (object) SqliteDatabase.sqlite3_column_double(stmHandle, iCol);
            break;
          case 3:
            IntPtr ptr = SqliteDatabase.sqlite3_column_text(stmHandle, iCol);
            values[iCol] = (object) Marshal.PtrToStringAnsi(ptr);
            break;
          case 4:
            IntPtr source = SqliteDatabase.sqlite3_column_blob(stmHandle, iCol);
            int length2 = SqliteDatabase.sqlite3_column_bytes(stmHandle, iCol);
            byte[] destination = new byte[length2];
            Marshal.Copy(source, destination, 0, length2);
            values[iCol] = (object) destination;
            break;
          case 5:
            values[iCol] = (object) null;
            break;
        }
      }
      dataTable.AddRow(values);
    }
    this.Finalize(stmHandle);
    this.Close();
    return dataTable;
  }

  public void ExecuteScript(string script)
  {
    string str = script;
    char[] chArray = new char[1]{ ';' };
    foreach (string query in str.Split(chArray))
    {
      if (!string.IsNullOrEmpty(query.Trim()))
        this.ExecuteNonQuery(query);
    }
  }

  private IntPtr Prepare(string query)
  {
    IntPtr ppStmpt;
    if (SqliteDatabase.sqlite3_prepare_v2(this._connection, query, query.Length, out ppStmpt, IntPtr.Zero) != 0)
      throw new SqliteException(Marshal.PtrToStringAnsi(SqliteDatabase.sqlite3_errmsg(this._connection)));
    return ppStmpt;
  }

  private void Finalize(IntPtr stmHandle)
  {
    if (SqliteDatabase.sqlite3_finalize(stmHandle) != 0)
      throw new SqliteException("Could not finalize SQL statement.");
  }
}
