﻿// Decompiled with JetBrains decompiler
// Type: SevenDaysDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class SevenDaysDlg : UI.Dialog
{
  private Dictionary<int, List<bool>> _daysbtStatus = new Dictionary<int, List<bool>>();
  private int _day = -1;
  public UIButtonBar daysBar;
  public UIButtonBar contentBar;
  public SevenDaysQuestDetail quests;
  public SevenDaysStoreDetail stores;
  public GameObject[] btStatus;
  public GameObject[] viewStatus;
  public UILabel curreny;
  public UILabel title;
  public UILabel timeLabel;

  private void OnDaysChange(int index)
  {
    if (this._day == index + 1)
      return;
    this._day = index + 1;
    this.UpdateUI();
  }

  public void OnIAPClick()
  {
    Utils.ShowNotEnoughGoldTip();
  }

  private void ResetBtStatus()
  {
    this._daysbtStatus.Clear();
    int totalDays = DBManager.inst.DB_SevenDays.TotalDays;
    int num = 1;
    for (int index = 0; index < totalDays; ++index)
    {
      bool flag1 = DBManager.inst.DB_SevenDays.IsFinih(num);
      List<bool> boolList = new List<bool>();
      boolList.Add(flag1);
      bool flag2 = DBManager.inst.DB_SevenQuests.IsFinish(num);
      boolList.Add(flag2);
      if (flag1 || flag2)
        this._daysbtStatus.Add(num, boolList);
      ++num;
    }
    for (int index = 0; index < this.btStatus.Length; ++index)
    {
      bool state = this._daysbtStatus.ContainsKey(index + 1);
      NGUITools.SetActive(this.btStatus[index], state);
    }
    for (int index = 0; index < this.viewStatus.Length; ++index)
    {
      bool state = this._daysbtStatus.ContainsKey(this._day);
      if (!state)
      {
        NGUITools.SetActive(this.viewStatus[index], state);
      }
      else
      {
        List<bool> daysbtStatu = this._daysbtStatus[this._day];
        NGUITools.SetActive(this.viewStatus[index], daysbtStatu[index]);
      }
    }
  }

  private void OnViewChange(int index)
  {
    NGUITools.SetActive(this.quests.gameObject, index == 1);
    NGUITools.SetActive(this.stores.gameObject, index == 0);
    if (index == 1)
      this.quests.SetData(this._day);
    if (index != 0)
      return;
    this.stores.SetData(this._day);
  }

  public override void OnOpen(UIControler.UIParameter orgParam = null)
  {
    base.OnOpen(orgParam);
    this.title.text = ScriptLocalization.Get("event_7_days_title", true);
    this.UpdateUI();
    this.AddEventHandler();
  }

  private void UpdateUI()
  {
    if (this._day == -1)
    {
      this._day = DBManager.inst.DB_SevenDays.GetCurrentDay();
      this.daysBar.SelectedIndex = this._day - 1;
    }
    if (this.quests.gameObject.activeSelf)
      this.quests.SetData(this._day);
    if (this.stores.gameObject.activeSelf)
      this.stores.SetData(this._day);
    this.ResetBtStatus();
    this.curreny.text = Utils.FormatThousands(PlayerData.inst.hostPlayer.Currency.ToString());
    this.ResetTime();
  }

  private void ResetTime()
  {
    int currentDay = DBManager.inst.DB_SevenDays.GetCurrentDay();
    SevenDaysData sevenDaysData = DBManager.inst.DB_SevenDays.GetSevenDaysData(currentDay);
    int num = 0;
    if (sevenDaysData != null)
      num = sevenDaysData.RemainTime;
    Color color = Color.red;
    int time = this._day != currentDay ? (this._day - currentDay - 1) * 86400 + num : num;
    Dictionary<string, string> para = new Dictionary<string, string>();
    string empty = string.Empty;
    string Term;
    if (time < 0)
      Term = "event_reward_expired";
    else if (this._day == currentDay)
    {
      Term = "event_7_days_finishes_in_num";
      para.Add("0", Utils.FormatTime(time, true, false, true));
    }
    else
    {
      color = Color.green;
      Term = "event_7_days_starts_in_num";
      para.Add("0", Utils.FormatTime(time, true, false, true));
    }
    string withPara = ScriptLocalization.GetWithPara(Term, para, true);
    this.timeLabel.color = color;
    this.timeLabel.text = withPara;
  }

  private void OnProcess(int time)
  {
    this.ResetTime();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_SevenDays.onDataChanged += new System.Action<long>(this.OnDataChange);
    DBManager.inst.DB_SevenQuests.onDataChanged += new System.Action<long>(this.OnDataChange);
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdate);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnProcess);
    this.daysBar.OnSelectedHandler += new System.Action<int>(this.OnDaysChange);
    this.contentBar.OnSelectedHandler += new System.Action<int>(this.OnViewChange);
  }

  private void OnUserDataUpdate(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.UpdateUI();
  }

  private void OnDataChange(long day)
  {
    this.UpdateUI();
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_SevenDays.onDataChanged -= new System.Action<long>(this.OnDataChange);
    DBManager.inst.DB_SevenQuests.onDataChanged -= new System.Action<long>(this.OnDataChange);
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdate);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnProcess);
    this.daysBar.OnSelectedHandler -= new System.Action<int>(this.OnDaysChange);
    this.contentBar.OnSelectedHandler -= new System.Action<int>(this.OnViewChange);
  }

  public override void OnClose(UIControler.UIParameter orgParam = null)
  {
    base.OnClose(orgParam);
    this.RemoveEventHandler();
    this.quests.Dispose();
    this.stores.Dispose();
  }
}
