﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsRewardBasePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTrialsRewardBasePopup : Popup
{
  protected Dictionary<int, MerlinTrialsRewardSlot> _itemDict = new Dictionary<int, MerlinTrialsRewardSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UIGrid _grid;
  [SerializeField]
  private GameObject _itemPrefab;
  protected int _merlinTrialsRewardId;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._itemPool.Initialize(this._itemPrefab, this._grid.gameObject);
    this._merlinTrialsRewardId = this.GetRewardId();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  protected virtual int GetRewardId()
  {
    return 0;
  }

  protected void ShowCollectEffect()
  {
    List<Item2RewardInfo.Data> rewards = new List<Item2RewardInfo.Data>();
    MerlinTrialsRewardsInfo trialsRewardsInfo = ConfigManager.inst.DB_MerlinTrialsRewards.Get(this._merlinTrialsRewardId);
    if (trialsRewardsInfo != null && trialsRewardsInfo.rewards != null && trialsRewardsInfo.rewards.rewards != null)
    {
      if (trialsRewardsInfo.score > 0)
        rewards.Add(new Item2RewardInfo.Data()
        {
          image = MerlinTrialsPayload.Instance.MerlinCoinImagePath,
          count = (float) trialsRewardsInfo.score
        });
      using (Dictionary<string, int>.Enumerator enumerator = trialsRewardsInfo.rewards.rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int result = 0;
          int num = current.Value;
          int.TryParse(current.Key, out result);
          rewards.Add(new Item2RewardInfo.Data()
          {
            itemid = result,
            count = (float) num
          });
        }
      }
    }
    DicePayload.Instance.ShowCollectEffect(rewards);
  }

  private void UpdateUI()
  {
    this.ClearData();
    MerlinTrialsRewardsInfo trialsRewardsInfo = ConfigManager.inst.DB_MerlinTrialsRewards.Get(this._merlinTrialsRewardId);
    if (trialsRewardsInfo != null && trialsRewardsInfo.rewards != null && trialsRewardsInfo.rewards.rewards != null)
    {
      if (trialsRewardsInfo.score > 0)
        this._itemDict.Add(-1, this.GenerateSlot(0, trialsRewardsInfo.score, true));
      using (Dictionary<string, int>.Enumerator enumerator = trialsRewardsInfo.rewards.rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int result = 0;
          int count = current.Value;
          int.TryParse(current.Key, out result);
          MerlinTrialsRewardSlot slot = this.GenerateSlot(result, count, false);
          if (!this._itemDict.ContainsKey(result))
            this._itemDict.Add(result, slot);
        }
      }
    }
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, MerlinTrialsRewardSlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this._grid.repositionNow = true;
    this._grid.Reposition();
    this._scrollView.ResetPosition();
  }

  private MerlinTrialsRewardSlot GenerateSlot(int itemId, int count, bool isCoin = false)
  {
    GameObject gameObject = this._itemPool.AddChild(this._grid.gameObject);
    gameObject.SetActive(true);
    MerlinTrialsRewardSlot component = gameObject.GetComponent<MerlinTrialsRewardSlot>();
    component.SetData(itemId, count, isCoin);
    return component;
  }
}
