﻿// Decompiled with JetBrains decompiler
// Type: BaseTask
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public abstract class BaseTask : TaskManager.ITask
{
  private bool _isRunning;
  private bool _isFinish;
  private int _taskId;

  public int TaskId
  {
    get
    {
      return this._taskId;
    }
    set
    {
      this._taskId = value;
    }
  }

  public virtual void Execute()
  {
  }

  public bool IsRunning
  {
    get
    {
      return this._isRunning;
    }
    set
    {
      this._isRunning = value;
    }
  }

  public bool IsFinish
  {
    get
    {
      return this._isFinish;
    }
    set
    {
      this._isFinish = value;
    }
  }
}
