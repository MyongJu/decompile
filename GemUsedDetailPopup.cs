﻿// Decompiled with JetBrains decompiler
// Type: GemUsedDetailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using GemConstant;
using UI;

public class GemUsedDetailPopup : Popup
{
  public UITexture _GemIcon;
  public UILabel _GemName;
  public UILabel _GemCount;
  public UILabel _ButtonDesc;
  public UILabel _Tips;
  public UIButton _ConfirmButton;
  private int itemId;
  private int itemCount;
  private ItemStaticInfo itemInfo;
  private ConfigEquipmentGemInfo gemConfig;

  public void OnGoToArmoryClicked()
  {
    if (this.gemConfig.slotType == SlotType.lord)
      UIManager.inst.OpenDlg("Equipment/EquipmentTreasuryDlg", (UI.Dialog.DialogParameter) new EquipmentTreasuryDlg.Parameter()
      {
        bagType = BagType.Hero,
        selectedGemId = 0
      }, 1 != 0, 1 != 0, 1 != 0);
    else
      UIManager.inst.OpenDlg("Equipment/DragonKnightEquipmentTreasuryDialog", (UI.Dialog.DialogParameter) new EquipmentTreasuryDlg.Parameter()
      {
        bagType = BagType.DragonKnight,
        selectedGemId = 0
      }, 1 != 0, 1 != 0, 1 != 0);
    this.Close();
  }

  public void OnCloseClicked()
  {
    this.Close();
  }

  private void Close()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void InitUI()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this._GemIcon, this.gemConfig.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    this._GemName.text = this.itemInfo.LocName;
    this._GemCount.text = this.itemCount.ToString();
    if (this.gemConfig.slotType == SlotType.lord)
    {
      this._ButtonDesc.text = Utils.XLAT("forge_uppercase_go_to_armory_button");
      this._Tips.text = Utils.XLAT("item_gemstone_lord_armory_description");
      this._ConfirmButton.isEnabled = CityManager.inst.GetBuildingFromType("forge") != null;
    }
    else
    {
      this._ButtonDesc.text = Utils.XLAT("dragon_knight_go_to_armory_button");
      this._Tips.text = Utils.XLAT("item_runestone_dk_armory_description");
      this._ConfirmButton.isEnabled = PlayerData.inst.dragonKnightData != null;
    }
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    GemUsedDetailPopup.Parameter parameter = orgParam as GemUsedDetailPopup.Parameter;
    if (parameter == null)
    {
      D.error((object) "GemUsedDetailPopup.Parameter is null");
    }
    else
    {
      this.itemId = parameter.itemId;
      this.itemCount = parameter.itemCount;
      this.itemInfo = ConfigManager.inst.DB_Items.GetItem(this.itemId);
      this.gemConfig = ConfigManager.inst.DB_EquipmentGem.GetDataByItemId(this.itemId);
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.InitUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int itemId;
    public int itemCount;
  }
}
