﻿// Decompiled with JetBrains decompiler
// Type: GuardActionAttackHit
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GuardActionAttackHit : GuardAction
{
  public override void Decode(GameObject go, object orgData)
  {
    base.Decode(go, orgData);
  }

  public override void Process(RoundPlayer player)
  {
    base.Process(player);
    if (!Application.isPlaying || player == null)
      return;
    BattleManager.Instance.SendPacket((DragonKnightPacket) new DragonKnightPacketHit(player != null ? player.PlayerId : 0L));
  }
}
