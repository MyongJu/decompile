﻿// Decompiled with JetBrains decompiler
// Type: AnimationEvents
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AnimationEvents : MonoBehaviour
{
  public ParticleSystem[] particles;
  public GameObject[] gameObjects;
  public AudioSource[] audioSources;

  private bool IsNullCheck(ParticleSystem[] particleArray, int index)
  {
    return particleArray == null || particleArray.Length <= index || !((Object) particleArray[index] != (Object) null);
  }

  private bool IsNullCheck(GameObject[] gameObjectArray, int index)
  {
    return gameObjectArray == null || gameObjectArray.Length <= index || !((Object) gameObjectArray[index] != (Object) null);
  }

  private bool IsNullCheck(AudioSource[] audioSourceArray, int index)
  {
    return audioSourceArray == null || audioSourceArray.Length <= index || !((Object) audioSourceArray[index] != (Object) null);
  }

  public void PlayParticleSystem(int particleIndex)
  {
    if (this.IsNullCheck(this.particles, particleIndex))
      return;
    this.particles[particleIndex].Play();
  }

  public void StopParticleSystem(int particleIndex)
  {
    if (this.IsNullCheck(this.particles, particleIndex))
      return;
    this.particles[particleIndex].Stop();
  }

  public void SetActiveGameObject(int gameObjectIndex)
  {
    if (this.IsNullCheck(this.gameObjects, gameObjectIndex))
      return;
    this.gameObjects[gameObjectIndex].SetActive(true);
  }

  public void SetInActiveGameObject(int gameObjectIndex)
  {
    if (this.IsNullCheck(this.gameObjects, gameObjectIndex))
      return;
    this.gameObjects[gameObjectIndex].SetActive(false);
  }

  public void PlayAudioClip(int audioIndex)
  {
    if (this.IsNullCheck(this.audioSources, audioIndex))
      return;
    this.audioSources[audioIndex].Play();
  }

  public void StopAudioClip(int audioIndex)
  {
    if (this.IsNullCheck(this.audioSources, audioIndex))
      return;
    this.audioSources[audioIndex].Stop();
  }
}
