﻿// Decompiled with JetBrains decompiler
// Type: PowerReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class PowerReward : QuestReward
{
  public readonly int power;

  public PowerReward(int inPower)
  {
    this.power = inPower;
  }

  public override void Claim()
  {
    GameEngine.Instance.events.Publish_onPowerIncreased(this.power);
    base.Claim();
  }

  public override string GetRewardIconName()
  {
    return "icon_fist";
  }

  public override string GetRewardTypeName()
  {
    return "Power";
  }

  public override int GetValue()
  {
    return this.power;
  }

  public override string GetValueText()
  {
    return Utils.FormatShortThousands(this.power);
  }
}
