﻿// Decompiled with JetBrains decompiler
// Type: EquipmentSuitItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentSuitItem : MonoBehaviour
{
  [SerializeField]
  private UITable _contentContainer;
  [SerializeField]
  private UILabel _labelEquipmentSuitName;
  [SerializeField]
  private UIGrid _equipmentNameContainer;
  [SerializeField]
  private UILabel _labelEquipmentNameTemplate;
  [SerializeField]
  private EquipmentSuitBenefitItem _equipmentSuitBenefitItemTemplate;
  [SerializeField]
  private Color _EquipmentOnColor;
  [SerializeField]
  private Color _EquipmentOffColor;
  [SerializeField]
  private GameObject _rootSuitEnhancement;
  [SerializeField]
  private UILabel _labelCurrentLevel;
  [SerializeField]
  private UILabel _labelNextLevel;
  [SerializeField]
  private UITable _tableSuitEnhancement;

  public void SetData(long uid, BagType bagType, int equipmentSuitGroupId, InventoryItemData inventoryItemData = null)
  {
    this._equipmentSuitBenefitItemTemplate.gameObject.SetActive(false);
    this._labelEquipmentNameTemplate.gameObject.SetActive(false);
    ConfigEquipmentSuitGroupInfo data = ConfigManager.inst.DB_EquipmentSuitGroup.GetData(equipmentSuitGroupId);
    if (data == null)
    {
      D.error((object) string.Format("cannot find equipment suit group info: {0}", (object) equipmentSuitGroupId));
    }
    else
    {
      using (List<ConfigEquipmentMainInfo>.Enumerator enumerator = ConfigManager.inst.GetEquipmentMain(bagType).GetAllInEquipmentSuit(equipmentSuitGroupId).GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ConfigEquipmentMainInfo current = enumerator.Current;
          bool flag = this.IsEquipmentEquiped(uid, bagType, current.internalId);
          UILabel equipmentNameLabel = this.CreateEquipmentNameLabel();
          equipmentNameLabel.text = current.LocName;
          equipmentNameLabel.color = !flag ? this._EquipmentOffColor : this._EquipmentOnColor;
        }
      }
      this._equipmentNameContainer.repositionNow = true;
      this._equipmentNameContainer.Reposition();
      int currentEquipmentCount = this.GetCurrentEquipmentCount(uid, bagType, equipmentSuitGroupId, inventoryItemData);
      this._labelEquipmentSuitName.text = data.LocalName;
      List<ConfigEquipmentSuitMainInfo> equipmentSuitEffectMap = ConfigManager.inst.DB_EquipmentSuitMain.GetEquipmentSuitEffectMap(equipmentSuitGroupId);
      bool haveSuitBenefit = false;
      using (List<ConfigEquipmentSuitMainInfo>.Enumerator enumerator1 = equipmentSuitEffectMap.GetEnumerator())
      {
        while (enumerator1.MoveNext())
        {
          ConfigEquipmentSuitMainInfo current1 = enumerator1.Current;
          using (List<Benefits.BenefitValuePair>.Enumerator enumerator2 = current1.benefits.GetBenefitsPairs().GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              Benefits.BenefitValuePair current2 = enumerator2.Current;
              EquipmentSuitBenefitItem equipmentSuitBenefitItem = this.CreateEquipmentSuitBenefitItem();
              if (currentEquipmentCount >= current1.numberRequired)
                haveSuitBenefit = true;
              equipmentSuitBenefitItem.SetData(current2, current1.numberRequired, currentEquipmentCount);
            }
          }
        }
      }
      this.UpdateEquipmentSuitEnhancementInfo(uid, bagType, inventoryItemData, haveSuitBenefit);
      this._contentContainer.repositionNow = true;
      this._contentContainer.Reposition();
    }
  }

  protected void UpdateEquipmentSuitEnhancementInfo(long uid, BagType bagType, InventoryItemData inventoryItemData, bool haveSuitBenefit)
  {
    bool flag1 = true;
    this._rootSuitEnhancement.SetActive(flag1);
    if (!flag1)
      return;
    int num = int.MaxValue;
    List<InventoryItemData> inventoryDataEquiped = this.GetAllInventoryDataEquiped(uid, bagType);
    if (inventoryItemData != null)
    {
      bool flag2 = false;
      for (int index = 0; index < inventoryDataEquiped.Count; ++index)
      {
        if (inventoryDataEquiped[index].equipment.type == inventoryItemData.equipment.type)
        {
          flag2 = true;
          inventoryDataEquiped[index] = inventoryItemData;
          break;
        }
      }
      if (!flag2)
        inventoryDataEquiped.Add(inventoryItemData);
    }
    if (inventoryDataEquiped.Count < 6 || inventoryItemData == null)
    {
      num = 0;
    }
    else
    {
      for (int index = 0; index < inventoryDataEquiped.Count; ++index)
        num = Mathf.Min(inventoryDataEquiped[index].enhanced, num);
    }
    ConfigEquipmentSuitEnhanceInfo lessEqual = ConfigManager.inst.DB_EquipmentSuitEnhance.GetLessEqual(num, bagType);
    ConfigEquipmentSuitEnhanceInfo greater = ConfigManager.inst.DB_EquipmentSuitEnhance.GetGreater(num, bagType);
    string str1 = string.Format("[FD8900]{0}:[-]", (object) ScriptLocalization.Get("forge_enhance_resonance_current_level", true));
    string str2;
    if (lessEqual == null)
    {
      str2 = str1 + "[9B9B9B]-[-]";
    }
    else
    {
      string withPara = ScriptLocalization.GetWithPara("forge_enhance_resonance_effect_description", new Dictionary<string, string>()
      {
        {
          "0",
          lessEqual.enhanceLevelReq.ToString()
        }
      }, true);
      if (haveSuitBenefit)
        str2 = str1 + string.Format("[00ff00]{0} {1}+{2}%[-]", (object) withPara, (object) ScriptLocalization.Get("forge_enhance_resonance_benefit", true), (object) (float) ((double) lessEqual.enhanceValue * 100.0));
      else if (uid == PlayerData.inst.uid)
        str2 = str1 + string.Format("[9B9B9B]{0} {1}+{2}%[-][ff0000]({3})[-]", (object) withPara, (object) ScriptLocalization.Get("forge_enhance_resonance_benefit", true), (object) (float) ((double) lessEqual.enhanceValue * 100.0), (object) ScriptLocalization.Get("forge_enhance_resonance_boost_description", true));
      else
        str2 = str1 + string.Format("[9B9B9B]{0} {1}+{2}%[-]", (object) withPara, (object) ScriptLocalization.Get("forge_enhance_resonance_benefit", true), (object) (float) ((double) lessEqual.enhanceValue * 100.0));
    }
    this._labelCurrentLevel.text = str2;
    string str3 = string.Format("[FD8900]{0}:[-]", (object) ScriptLocalization.Get("forge_enhance_resonance_next_level", true));
    string str4;
    if (greater == null)
    {
      str4 = str3 + "-";
    }
    else
    {
      string withPara = ScriptLocalization.GetWithPara("forge_enhance_resonance_effect_description", new Dictionary<string, string>()
      {
        {
          "0",
          greater.enhanceLevelReq.ToString()
        }
      }, true);
      str4 = str3 + string.Format("[9B9B9B]{0} {1}+{2}%[-]", (object) withPara, (object) ScriptLocalization.Get("forge_enhance_resonance_benefit", true), (object) (float) ((double) greater.enhanceValue * 100.0));
    }
    this._labelNextLevel.text = str4;
    this._rootSuitEnhancement.transform.SetAsLastSibling();
    this._tableSuitEnhancement.repositionNow = true;
    this._tableSuitEnhancement.Reposition();
  }

  protected EquipmentSuitBenefitItem CreateEquipmentSuitBenefitItem()
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this._equipmentSuitBenefitItemTemplate.gameObject);
    gameObject.transform.SetParent(this._contentContainer.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    return gameObject.GetComponent<EquipmentSuitBenefitItem>();
  }

  protected UILabel CreateEquipmentNameLabel()
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this._labelEquipmentNameTemplate.gameObject);
    gameObject.transform.SetParent(this._equipmentNameContainer.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    return gameObject.GetComponent<UILabel>();
  }

  protected List<InventoryItemData> GetAllInventoryDataEquiped(long uid, BagType bagType)
  {
    return DBManager.inst.GetInventory(bagType).GetEquippedItemsByID(uid);
  }

  protected int GetCurrentEquipmentCount(long uid, BagType bagType, int equipmentSuitGroupId, InventoryItemData inventoryItemData)
  {
    int num = 0;
    List<InventoryItemData> inventoryDataEquiped = this.GetAllInventoryDataEquiped(uid, bagType);
    if (inventoryItemData != null)
    {
      bool flag = false;
      for (int index = 0; index < inventoryDataEquiped.Count; ++index)
      {
        if (inventoryDataEquiped[index].equipment.type == inventoryItemData.equipment.type)
        {
          inventoryDataEquiped[index] = inventoryItemData;
          flag = true;
          break;
        }
      }
      if (!flag)
        inventoryDataEquiped.Add(inventoryItemData);
    }
    using (List<InventoryItemData>.Enumerator enumerator = inventoryDataEquiped.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InventoryItemData current = enumerator.Current;
        if (current != null)
        {
          ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(current.internalId);
          if (data != null)
          {
            if (data.suitGroup == equipmentSuitGroupId)
              ++num;
          }
          else
            D.error((object) string.Format("can not find equipment info : {0}", (object) current.internalId));
        }
      }
    }
    return num;
  }

  protected bool IsEquipmentEquiped(long uid, BagType bagType, int equipmentId)
  {
    using (List<InventoryItemData>.Enumerator enumerator = this.GetAllInventoryDataEquiped(uid, bagType).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InventoryItemData current = enumerator.Current;
        if (current != null && current.internalId == equipmentId)
          return true;
      }
    }
    return false;
  }
}
