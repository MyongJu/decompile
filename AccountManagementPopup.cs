﻿// Decompiled with JetBrains decompiler
// Type: AccountManagementPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AccountManagementPopup : Popup
{
  private List<AccountBindItem> _totals = new List<AccountBindItem>();
  public AccountBindItem itemPrefab;
  public UIGrid grid;
  public UIScrollView scroll;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    for (int index = 0; index < this._totals.Count; ++index)
      this._totals[index].Clear();
    this._totals.Clear();
  }

  private void AddItem(FunplusAccountType type)
  {
    GameObject go = NGUITools.AddChild(this.grid.gameObject, this.itemPrefab.gameObject);
    NGUITools.SetActive(go, true);
    AccountBindItem component = go.GetComponent<AccountBindItem>();
    component.SetData(type);
    this._totals.Add(component);
  }

  private void UpdateUI()
  {
    if (AccountManager.Instance.CanShowWechat())
      this.AddItem(FunplusAccountType.FPAccountTypeWechat);
    if (AccountManager.Instance.CanShowFacebook())
      this.AddItem(FunplusAccountType.FPAccountTypeFacebook);
    switch (Application.platform)
    {
      case RuntimePlatform.IPhonePlayer:
        this.AddItem(FunplusAccountType.FPAccountTypeGameCenter);
        break;
      case RuntimePlatform.Android:
        if (AccountManager.Instance.CanShowGoolgePlay())
        {
          this.AddItem(FunplusAccountType.FPAccountTypeGooglePlus);
          break;
        }
        break;
    }
    if (!AccountManager.Instance.CanShowVK())
      return;
    this.AddItem(FunplusAccountType.FPAccountTypeVK);
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnHelp()
  {
    AccountManager.Instance.ShowFASQs();
  }
}
