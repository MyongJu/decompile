﻿// Decompiled with JetBrains decompiler
// Type: Events
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;

public class Events
{
  public System.Action OnItemDataChanged;

  public event System.Action<CityManager.ResourceTypes> onResourceClicked;

  public event System.Action<TimerHUDUIItem> onSpeedUpTimer;

  public event System.Action onBoostButton;

  public event System.Action<ShopStaticInfo, double, double> onItemClicked;

  public event System.Action<string, int> onBuildingUpgraded;

  public event System.Action<Events.TroopCountChangedEventArgs> onTroopCountChanged;

  public event System.Action<string, int> onResourceCountChanged;

  public event System.Action<string, int> OnResourceCapacityChanged;

  public event System.Action<string> OnCityRenamed;

  public event System.Action onDragonDataChanged;

  public event System.Action<long, TimerHUDUIItem> onClickSpeedupForMarchJob;

  public event System.Action<CityManager.ResourceTypes, double> onUseAddResourceItem;

  public event System.Action<ItemBag.ItemType[]> trigerUseItem;

  public event System.Action<int> getTeleportXYForItem;

  public event System.Action<int, int> mailCountForStatus;

  public event System.Action<Events.PlayerStatChangedArgs> onPlayerStatChanged;

  public event System.Action<Events.HeroStatChangedArgs> onHeroStatChanged;

  public event System.Action<Events.HeroStateChangedArgs> onHeroStateChanged;

  public event System.Action<Events.PlayerVIPStatusChanged> onPlayerVipStatusChanged;

  public event System.Action<Events.PropertyValueChangedArgs> onPropertyValueChanged;

  public event System.Action onQuestDataChanged;

  public event System.Action<int> onPowerIncreased;

  public event System.Action<Events.ResearchStateChangedEventArgs> onResearchStateChanged;

  public event System.Action<Hashtable, bool> ServerPush_OnReceiveMail;

  public event System.Action<Hashtable, bool> ServerPush_OnReceiveFavMail;

  public event System.Action<Hashtable> ServerPush_OnUpdateMail;

  public event System.Action<Hashtable> ServerPush_OnOthersStartMarch;

  public event System.Action<Hashtable> ServerPush_OnOthersRecallMarch;

  public event System.Action<Hashtable> ServerPush_OnMarch;

  public event System.Action<Hashtable> ServerPush_OnChatChannelCmd;

  public event System.Action<Hashtable> ServerPush_OnAllianceHelpRequestRecived;

  public event System.Action<Hashtable> ServerPush_OnAllianceHelpResponseRecived;

  public event System.Action<long> ServerPush_OnAllianceJoined;

  public event System.Action<Hashtable> ServerPush_OnAcceptApplyAlliance;

  public event System.Action ServerPush_OnAllianceQuit;

  public event System.Action<Hashtable> ServerPush_OnAllianceGiftRecived;

  public event System.Action<Hashtable> ServerPush_OnAllianceMemberLeft;

  public event System.Action<Hashtable> ServerPush_OnAllianceDisband;

  public event System.Action<Hashtable> ServerPush_OnAlliancePrivate;

  public event System.Action<Hashtable> ServerPush_OnAllianceModify;

  public event System.Action<Hashtable> ServerPush_OnAllianceRevokeInvite;

  public event System.Action<Hashtable> ServerPush_OnAllianceDenyInvite;

  public event System.Action<Hashtable> ServerPush_OnAllianceAcceptInvite;

  public event System.Action<Hashtable> ServerPush_OnAllianceMemberJoined;

  public event System.Action<Hashtable> ServerPush_OnAllianceMemberLeaveInMap;

  public event System.Action<Hashtable> ServerPush_OnAllianceMemberJoinedInMap;

  public event System.Action<Hashtable> ServerPush_OnAllianceMemberApplied;

  public event System.Action<Hashtable> ServerPush_OnAllianceInvite;

  public event System.Action<Hashtable> ServerPush_OnAllianceDiplomacyNewEnemy;

  public event System.Action<Hashtable> ServerPush_OnAllianceDiplomacyBeEnemy;

  public event System.Action<Hashtable> ServerPush_OnAllianceSetRank;

  public event System.Action<Hashtable> ServerPush_OnAllianceTransferHighlord;

  public event System.Action<Hashtable> ServerPush_OnAllianceEmpowerTempHighlord;

  public event System.Action<Hashtable> ServerPush_OnTeleport;

  public void Publish_onCityHUDResourceClicked(CityManager.ResourceTypes resource)
  {
    if (this.onResourceClicked == null)
      return;
    this.onResourceClicked(resource);
  }

  public void Publish_onSpeedUpTimer(TimerHUDUIItem _timer)
  {
    if (this.onSpeedUpTimer == null)
      return;
    this.onSpeedUpTimer(_timer);
  }

  public void Publish_OnBoostButton()
  {
    if (this.onBoostButton == null)
      return;
    this.onBoostButton();
  }

  public void Publish_onItemClicked(ShopStaticInfo shopInfo, double amount, double needed)
  {
    if (this.onItemClicked == null)
      return;
    this.onItemClicked(shopInfo, amount, needed);
  }

  public void Publish_OnBuildingUpgraded(string buildingType, int buildingLevel)
  {
    if (this.onBuildingUpgraded == null)
      return;
    this.onBuildingUpgraded(buildingType, buildingLevel);
  }

  public void Send_OnTroopCountChanged(Events.TroopCountChangedEventArgs args)
  {
    if (this.onTroopCountChanged == null)
      return;
    this.onTroopCountChanged(args);
  }

  public void Publish_OnResourceCountChanged(string resourceType, int delta)
  {
    if (this.onResourceCountChanged == null)
      return;
    this.onResourceCountChanged(resourceType, delta);
  }

  public void Publish_OnResourceCapacityChanged(string resourceType, int delta)
  {
    if (this.OnResourceCapacityChanged == null)
      return;
    this.OnResourceCapacityChanged(resourceType, delta);
  }

  public void Publish_OnCityRenamed(string newName)
  {
    if (this.OnCityRenamed == null)
      return;
    this.OnCityRenamed(newName);
  }

  public void Publish_onDragonDataChanged()
  {
    if (this.onDragonDataChanged == null)
      return;
    this.onDragonDataChanged();
  }

  public void Publish_OnClickSpeedupForMarchJob(long march_id, TimerHUDUIItem timerItem = null)
  {
    if (this.onClickSpeedupForMarchJob == null)
      return;
    this.onClickSpeedupForMarchJob(march_id, timerItem);
  }

  public void Publish_onUseAddResourceItem(CityManager.ResourceTypes rt, double num)
  {
    if (this.onUseAddResourceItem == null)
      return;
    this.onUseAddResourceItem(rt, num);
  }

  public void Publlish_trigerUseItem(params ItemBag.ItemType[] tp)
  {
    if (this.trigerUseItem == null)
      return;
    this.trigerUseItem(tp);
  }

  public void Publlish_getTeleportXYForItem(int id)
  {
    if (this.getTeleportXYForItem == null)
      return;
    this.getTeleportXYForItem(id);
  }

  public void Publish_mailCountForStatus(int ct, int status)
  {
    if (this.mailCountForStatus == null)
      return;
    this.mailCountForStatus(ct, status);
  }

  public void Publish_onItemDataChanged()
  {
    if (this.OnItemDataChanged == null)
      return;
    this.OnItemDataChanged();
  }

  public void Send_OnPlayerStatChanged(Events.PlayerStatChangedArgs args)
  {
    if (this.onPlayerStatChanged == null)
      return;
    this.onPlayerStatChanged(args);
  }

  public void Send_OnPlayerVIPStateChanged(Events.PlayerVIPStatusChanged args)
  {
    if (this.onPlayerVipStatusChanged == null)
      return;
    this.onPlayerVipStatusChanged(args);
  }

  public void Send_OnHeroStatChanged(Events.HeroStatChangedArgs args)
  {
    if (this.onHeroStatChanged == null)
      return;
    this.onHeroStatChanged(args);
  }

  public void Send_OnHeroStateChanged(Events.HeroStateChangedArgs args)
  {
    if (this.onHeroStateChanged == null)
      return;
    this.onHeroStateChanged(args);
  }

  public void Send_OnPropertyValueChanged(Events.PropertyValueChangedArgs ev)
  {
    if (this.onPropertyValueChanged == null)
      return;
    this.onPropertyValueChanged(ev);
  }

  public void Publish_onQuestDataChanged()
  {
    if (this.onQuestDataChanged == null)
      return;
    this.onQuestDataChanged();
  }

  public void Publish_onPowerIncreased(int increasedPower)
  {
    if (this.onPowerIncreased == null)
      return;
    this.onPowerIncreased(increasedPower);
  }

  public void Send_OnResearchStateChanged(Events.ResearchStateChangedEventArgs args)
  {
    if (this.onResearchStateChanged == null)
      return;
    this.onResearchStateChanged(args);
  }

  public void Publish_OnReceiveMail(Hashtable data, bool fromLoad = false)
  {
    if (data == null || this.ServerPush_OnReceiveMail == null)
      return;
    this.ServerPush_OnReceiveMail(data, fromLoad);
  }

  public void Publish_OnReceiveFavMail(Hashtable data, bool fromLoad = false)
  {
    if (data == null || this.ServerPush_OnReceiveMail == null)
      return;
    this.ServerPush_OnReceiveFavMail(data, fromLoad);
  }

  public void Publish_OnUpdateMail(Hashtable data)
  {
    if (data == null || this.ServerPush_OnUpdateMail == null)
      return;
    this.ServerPush_OnUpdateMail(data);
  }

  public void Publish_OnPvpStartMarch(Hashtable data)
  {
    if (this.ServerPush_OnOthersStartMarch == null)
      return;
    this.ServerPush_OnOthersStartMarch(data);
  }

  public void Publish_OnPvpRecallMarch(Hashtable data)
  {
    if (this.ServerPush_OnOthersRecallMarch == null)
      return;
    this.ServerPush_OnOthersRecallMarch(data);
  }

  public void Publish_OnMarch(Hashtable data)
  {
    if (this.ServerPush_OnMarch == null)
      return;
    this.ServerPush_OnMarch(data);
  }

  public void Publish_onChatChannelCmd(Hashtable data)
  {
    if (this.ServerPush_OnChatChannelCmd == null)
      return;
    this.ServerPush_OnChatChannelCmd(data);
  }

  public void Publish_AllianceHelpRequestRecived(Hashtable data)
  {
    if (this.ServerPush_OnAllianceHelpRequestRecived == null)
      return;
    this.ServerPush_OnAllianceHelpRequestRecived(data);
  }

  public void Publish_AllianceHelpResponseRecived(Hashtable data)
  {
    if (this.ServerPush_OnAllianceHelpResponseRecived == null)
      return;
    this.ServerPush_OnAllianceHelpResponseRecived(data);
  }

  public void Publish_AllianceJoined(long newAllianceId)
  {
    if (this.ServerPush_OnAllianceJoined == null)
      return;
    this.ServerPush_OnAllianceJoined(newAllianceId);
  }

  public void Publish_AcceptApplyAlliance(Hashtable ht)
  {
    if (this.ServerPush_OnAcceptApplyAlliance == null)
      return;
    this.ServerPush_OnAcceptApplyAlliance(ht);
  }

  public void Publish_AllianceQuit()
  {
    if (this.ServerPush_OnAllianceQuit == null)
      return;
    this.ServerPush_OnAllianceQuit();
  }

  public void Publish_AllianceGiftRecived(Hashtable data)
  {
    if (this.ServerPush_OnAllianceGiftRecived == null)
      return;
    this.ServerPush_OnAllianceGiftRecived(data);
  }

  public void Publish_OnAllianceMemberLeft(Hashtable data)
  {
    if (this.ServerPush_OnAllianceMemberLeft == null)
      return;
    this.ServerPush_OnAllianceMemberLeft(data);
  }

  public void Publish_OnAllianceDisband(Hashtable data)
  {
    if (this.ServerPush_OnAllianceDisband == null)
      return;
    this.ServerPush_OnAllianceDisband(data);
  }

  public void Publish_OnAlliancePrivate(Hashtable data)
  {
    if (this.ServerPush_OnAlliancePrivate == null)
      return;
    this.ServerPush_OnAlliancePrivate(data);
  }

  public void Publish_OnAllianceModify(Hashtable data)
  {
    if (this.ServerPush_OnAllianceModify == null)
      return;
    this.ServerPush_OnAllianceModify(data);
  }

  public void Publish_OnAllianceRevokeInvite(Hashtable data)
  {
    if (this.ServerPush_OnAllianceRevokeInvite == null)
      return;
    this.ServerPush_OnAllianceRevokeInvite(data);
  }

  public void Publish_OnAllianceDenyInvite(Hashtable data)
  {
    if (this.ServerPush_OnAllianceDenyInvite == null)
      return;
    this.ServerPush_OnAllianceDenyInvite(data);
  }

  public void Publish_OnAllianceAcceptInvite(Hashtable data)
  {
    if (this.ServerPush_OnAllianceAcceptInvite == null)
      return;
    this.ServerPush_OnAllianceAcceptInvite(data);
  }

  public void Publish_OnAllianceMemberJoined(Hashtable data)
  {
    if (this.ServerPush_OnAllianceMemberJoined == null)
      return;
    this.ServerPush_OnAllianceMemberJoined(data);
  }

  public void Publish_OnAllianceMemberLeaveInMap(Hashtable data)
  {
    if (this.ServerPush_OnAllianceMemberLeaveInMap == null)
      return;
    this.ServerPush_OnAllianceMemberLeaveInMap(data);
  }

  public void Publish_OnAllianceMemberJoinedInMap(Hashtable data)
  {
    if (this.ServerPush_OnAllianceMemberJoinedInMap == null)
      return;
    this.ServerPush_OnAllianceMemberJoinedInMap(data);
  }

  public void Publish_OnAllianceMemberApplied(Hashtable data)
  {
    if (this.ServerPush_OnAllianceMemberApplied == null)
      return;
    this.ServerPush_OnAllianceMemberApplied(data);
  }

  public void Publish_OnAllianceInvite(Hashtable data)
  {
    if (this.ServerPush_OnAllianceInvite == null)
      return;
    this.ServerPush_OnAllianceInvite(data);
  }

  public void Publish_OnAllianceDiplomacyNewEnemy(Hashtable data)
  {
    if (this.ServerPush_OnAllianceDiplomacyNewEnemy == null)
      return;
    this.ServerPush_OnAllianceDiplomacyNewEnemy(data);
  }

  public void Publish_OnAllianceDiplomacyBeEnemy(Hashtable data)
  {
    if (this.ServerPush_OnAllianceDiplomacyBeEnemy == null)
      return;
    this.ServerPush_OnAllianceDiplomacyBeEnemy(data);
  }

  public void Publish_OnAllianceSetRank(Hashtable data)
  {
    if (this.ServerPush_OnAllianceSetRank == null)
      return;
    this.ServerPush_OnAllianceSetRank(data);
  }

  public void Publish_OnAllianceTransferHighlord(Hashtable data)
  {
    if (this.ServerPush_OnAllianceTransferHighlord == null)
      return;
    this.ServerPush_OnAllianceTransferHighlord(data);
  }

  public void Publish_OnAllianceEmpowerTempHighlord(Hashtable data)
  {
    if (this.ServerPush_OnAllianceEmpowerTempHighlord == null)
      return;
    this.ServerPush_OnAllianceEmpowerTempHighlord(data);
  }

  public void Publish_OnTeleport(Hashtable data)
  {
    if (this.ServerPush_OnTeleport == null)
      return;
    this.ServerPush_OnTeleport(data);
  }

  public enum TroopCountChangeReason
  {
    Trained = 1,
    Killed = 2,
    Marched = 3,
    Recalled = 4,
    Healed = 5,
  }

  public class TroopCountChangedEventArgs : EventArgs
  {
    public TroopCountChangedEventArgs(string troopType, int oldValue, int newValue, Events.TroopCountChangeReason cause)
    {
      this.TroopType = troopType;
      this.OldValue = oldValue;
      this.NewValue = newValue;
      this.Cause = cause;
    }

    public string TroopType { get; set; }

    public int OldValue { get; set; }

    public int NewValue { get; set; }

    public Events.TroopCountChangeReason Cause { get; set; }
  }

  public enum PlayerStat
  {
    Currency,
    Power,
    Dedication,
    Alliance,
    VIP,
    Tutorial,
    Essence,
  }

  public enum HeroStat
  {
    Information,
    Experience,
    Level,
  }

  public enum VIPStatus
  {
    None,
    Inactive,
    Active,
  }

  public class PlayerStatChangedArgs : EventArgs
  {
    public PlayerStatChangedArgs(Events.PlayerStat stat, double oldValue, double newValue)
    {
      this.Stat = stat;
      this.OldValue = oldValue;
      this.NewValue = newValue;
      this.Delta = newValue - oldValue;
    }

    public Events.PlayerStat Stat { get; set; }

    public double OldValue { get; set; }

    public double NewValue { get; set; }

    public double Delta { get; set; }
  }

  public class PlayerVIPStatusChanged : EventArgs
  {
    public Events.VIPStatus oldStatus;
    public Events.VIPStatus newStatus;
    public long jobId;

    public PlayerVIPStatusChanged(bool wasActive, bool isActive, long jobId)
    {
      this.oldStatus = !wasActive ? Events.VIPStatus.Inactive : Events.VIPStatus.Active;
      this.newStatus = !isActive ? Events.VIPStatus.Inactive : Events.VIPStatus.Active;
      this.jobId = jobId;
    }
  }

  public class HeroStatChangedArgs : EventArgs
  {
    public HeroStatChangedArgs(int heroID, Events.HeroStat stat, double oldValue, double newValue)
    {
      this.HeroID = heroID;
      this.Stat = stat;
      this.OldValue = oldValue;
      this.NewValue = newValue;
      this.Delta = newValue - oldValue;
    }

    public int HeroID { get; set; }

    public Events.HeroStat Stat { get; set; }

    public double OldValue { get; set; }

    public double NewValue { get; set; }

    public double Delta { get; set; }
  }

  public class HeroStateChangedArgs : EventArgs
  {
    public HeroStateChangedArgs(int heroID, UnitState oldState, UnitState newState)
    {
      this.HeroID = heroID;
      this.OldState = oldState;
      this.NewState = newState;
    }

    public int HeroID { get; set; }

    public UnitState OldState { get; set; }

    public UnitState NewState { get; set; }
  }

  public class PropertyValueChangedArgs : EventArgs
  {
    public PropertyValueChangedArgs(string propertyID, int owner = 0)
    {
      this.PropertyID = propertyID;
      this.Owner = owner;
    }

    public string PropertyID { get; set; }

    public int Owner { get; set; }
  }

  public class ResearchStateChangedEventArgs : EventArgs
  {
    public ResearchStateChangedEventArgs(string techID, string techLevelID, int internalID, TechLevel.ResearchState state)
    {
      this.TechID = techID;
      this.TechLevelID = techLevelID;
      this.InternalID = internalID;
      this.State = state;
    }

    public string TechID { get; set; }

    public string TechLevelID { get; set; }

    public int InternalID { get; set; }

    public TechLevel.ResearchState State { get; set; }
  }
}
