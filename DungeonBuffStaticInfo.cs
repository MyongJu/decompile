﻿// Decompiled with JetBrains decompiler
// Type: DungeonBuffStaticInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DungeonBuffStaticInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "name")]
  public string Name;
  [Config(Name = "dialog_1")]
  public string Dialog1;
  [Config(Name = "dialog_2")]
  public string Dialog2;
  [Config(Name = "image")]
  public string Image;
  [Config(Name = "weight")]
  public float Weight;
  [Config(Name = "effect")]
  public string Effect;
  [Config(Name = "effect_value")]
  public float EffectValue;
  [Config(Name = "benefit")]
  public int Benefit;
  [Config(Name = "benefit_value")]
  public float BenefitValue;
  [Config(Name = "benefit_round")]
  public int BenefitRound;

  public string ImageFullPath
  {
    get
    {
      return "Prefabs/Npc/" + this.Image;
    }
  }
}
