﻿// Decompiled with JetBrains decompiler
// Type: DragonPowerItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;
using UnityEngine;

public class DragonPowerItem : MonoBehaviour
{
  public UILabel power;
  public UILabel exp;
  public UILabel level;
  public UIProgressBar progressBar;
  public UILabel _tendencyType;
  public UILabel _tendencyDesc;
  public UILabel _tendencyBonus;
  public UITexture _tendencyIcon;

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this._tendencyIcon);
  }

  public void Refresh()
  {
    if ((bool) ((UnityEngine.Object) this.power))
      this.power.text = PlayerData.inst.userData.dragon_power.ToString();
    DragonInfo dragonInfoByLevel1 = ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(PlayerData.inst.dragonData.Level);
    if ((UnityEngine.Object) this.progressBar != (UnityEngine.Object) null)
    {
      DragonInfo dragonInfoByLevel2 = ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(PlayerData.inst.dragonData.Level + 1);
      NGUITools.SetActive(this.progressBar.gameObject, true);
      if (dragonInfoByLevel2 != null)
      {
        long num1 = PlayerData.inst.dragonData.Xp - (long) dragonInfoByLevel1.exp;
        long num2 = (long) (dragonInfoByLevel2.exp - dragonInfoByLevel1.exp);
        this.progressBar.value = (float) num1 / (float) num2;
        this.exp.text = num1.ToString() + "/" + num2.ToString();
      }
      else
      {
        this.progressBar.value = 1f;
        this.exp.text = ScriptLocalization.Get("dragon_max_level", true);
      }
      if ((UnityEngine.Object) this.level != (UnityEngine.Object) null)
        this.level.text = PlayerData.inst.dragonData.Level.ToString();
    }
    ConfigDragonTendencyBoostInfo data = ConfigManager.inst.DB_ConfigDragonTendencyBoost.GetData(PlayerData.inst.dragonData.tendency);
    if (data == null)
      return;
    if ((bool) ((UnityEngine.Object) this._tendencyType))
      this._tendencyType.text = Utils.GetDragonTendencyDesc(PlayerData.inst.dragonData);
    if ((bool) ((UnityEngine.Object) this._tendencyBonus))
      this._tendencyBonus.text = string.Format("{0}%", (object) (float) ((double) PlayerData.inst.dragonData.TendencyBonus * 100.0));
    if ((bool) ((UnityEngine.Object) this._tendencyDesc))
      this._tendencyDesc.text = data.LocalSkillDesc;
    if (!(bool) ((UnityEngine.Object) this._tendencyIcon))
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this._tendencyIcon, data.IconPath, (System.Action<bool>) null, true, false, string.Empty);
  }

  public void OnButtonChangeFormatClicked()
  {
    if (PlayerData.inst.dragonData.MarchId != 0L)
      UIManager.inst.toast.Show(ScriptLocalization.Get("dragon_type_change_condition_not_satisfied", true), (System.Action) null, 4f, true);
    else
      UIManager.inst.OpenPopup("Dragon/DragonTendencySkillsPopup", (Popup.PopupParameter) new DragonTendencySkillsPopup.Parameter()
      {
        dragonData = PlayerData.inst.dragonData
      });
  }
}
