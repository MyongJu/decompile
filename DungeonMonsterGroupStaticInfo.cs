﻿// Decompiled with JetBrains decompiler
// Type: DungeonMonsterGroupStaticInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DungeonMonsterGroupStaticInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "level")]
  public int Level;
  [Config(Name = "monster_1")]
  public int MonsterID1;
  [Config(Name = "monster_2")]
  public int MonsterID2;
  [Config(Name = "monster_3")]
  public int MonsterID3;
  [Config(Name = "drop_chest")]
  public int DropChestId;
  [Config(Name = "weight")]
  public int RandomWeight;
}
