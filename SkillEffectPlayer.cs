﻿// Decompiled with JetBrains decompiler
// Type: SkillEffectPlayer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class SkillEffectPlayer
{
  private float _startTime = -1f;
  private HashSet<long> _vfxIds = new HashSet<long>();
  private long _guid;
  private SpriteView _source;
  private IVfxTarget _target;
  private IVfx _sourceVfx;
  private IVfx _targetVfx;
  private SkillEffectData _data;
  private bool _isPlaying;
  private DummyPoint _dummyPoint;

  public event SkillEffectEventHandler OnEvent;

  public long Guid
  {
    get
    {
      return this._guid;
    }
  }

  public virtual void SetData(SkillEffectData data)
  {
    this._data = data;
  }

  public virtual bool Play(SpriteView source, Vector3 position)
  {
    this._dummyPoint = DummyPoint.Create(position, (Transform) null);
    if (this.Play(source, (IVfxTarget) this._dummyPoint))
      return true;
    DummyPoint.Destory(this._dummyPoint);
    this._dummyPoint = (DummyPoint) null;
    return false;
  }

  public virtual bool Play(SpriteView source, IVfxTarget target = null)
  {
    if (this._data == null || source == null || this._isPlaying)
      return false;
    this._isPlaying = true;
    this._guid = ClientIdCreater.CreateLongId(ClientIdCreater.IdType.SKILL_EFFECT);
    this._source = source;
    this._target = target;
    if (this._data.faceTarget && target != null)
      this._source.FaceTo(target.transform);
    this._source.OnAnimationEvent += new System.Action<AnimationEvent>(this.OnSourceAnimationEvent);
    this._source.PlayAnimation(this._data.animation, 0.5f);
    this._startTime = Time.time;
    SkillEffectManager.Instance.Add(this);
    this.TriggerEvent(SkillEffectEvent.Type.OnStart, (object) null);
    return true;
  }

  public virtual void Update()
  {
    if ((double) this._data.time == -1.0 || (double) Time.time - (double) this._startTime <= (double) this._data.time)
      return;
    this.Stop(false);
  }

  public virtual void Stop(bool clearAllVfx = false)
  {
    if (!this._isPlaying)
      return;
    this.TriggerEvent(SkillEffectEvent.Type.OnEnd, (object) null);
    SkillEffectManager.Instance.Remove(this);
    if (clearAllVfx)
      this.ClearAllVfx();
    this._source.OnAnimationEvent -= new System.Action<AnimationEvent>(this.OnSourceAnimationEvent);
    this._source = (SpriteView) null;
    this._target = (IVfxTarget) null;
    this._startTime = -1f;
    this._isPlaying = false;
    if (!((UnityEngine.Object) this._dummyPoint != (UnityEngine.Object) null))
      return;
    DummyPoint.Destory(this._dummyPoint);
    this._dummyPoint = (DummyPoint) null;
  }

  public virtual void Clear(bool clearAllVfx = true)
  {
    this.Stop(clearAllVfx);
    this._data = (SkillEffectData) null;
  }

  private void TriggerEvent(SkillEffectEvent.Type type, object param = null)
  {
    if (this.OnEvent == null)
      return;
    this.OnEvent(type, (object) null);
  }

  protected virtual void OnSourceAnimationEvent(AnimationEvent e)
  {
    if (e == null)
      return;
    if (e.intParameter == this._data.sourceEventId)
      this.TriggerSourceVfx();
    if (e.intParameter == this._data.targetEventId)
      this.TriggerTargetVfx();
    if (e.intParameter != int.MaxValue)
      return;
    this.OnSourceAnimationEnd();
  }

  protected virtual void OnSourceAnimationEnd()
  {
    this.TriggerEvent(SkillEffectEvent.Type.OnSourceActionEnd, (object) null);
    if ((double) this._data.time != -1.0)
      return;
    this.Stop(false);
  }

  protected virtual void TriggerSourceVfx()
  {
    this.PlayVfx(this._data.sourceVfx, (IVfxTarget) this._source);
  }

  protected virtual void TriggerTargetVfx()
  {
    this.PlayVfx(this._data.targetVfx, this._target);
  }

  protected void PlayVfx(string asset, IVfxTarget target)
  {
    if (string.IsNullOrEmpty(asset) || target == null)
      return;
    long andPlay = VfxManager.Instance.CreateAndPlay(asset, target);
    IVfx vfxByGuid = VfxManager.Instance.GetVfxByGuid(andPlay);
    if (vfxByGuid != null && target != null)
      Utils.SetLayer(vfxByGuid.gameObject, target.transform.gameObject.layer);
    this._vfxIds.Add(andPlay);
  }

  protected void ClearAllVfx()
  {
    HashSet<long>.Enumerator enumerator = this._vfxIds.GetEnumerator();
    while (enumerator.MoveNext())
      VfxManager.Instance.Delete(enumerator.Current);
    this._vfxIds.Clear();
  }
}
