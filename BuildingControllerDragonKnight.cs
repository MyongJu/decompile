﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerDragonKnight
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingControllerDragonKnight : BuildingControllerNew
{
  private CollectionUI m_CollectionUI;
  private bool m_Start;
  private CollectionUI mCollectionDragonKnight;

  private void Start()
  {
    this.m_Start = true;
    this.UpdateCollectionUI(PlayerData.inst.dragonKnightData);
  }

  private void OnEnable()
  {
    MessageHub.inst.GetPortByAction("job_complete").AddEvent(new System.Action<object>(this.OnJobCompleted));
    if (!this.m_Start)
      return;
    this.UpdateCollectionUI(PlayerData.inst.dragonKnightData);
  }

  private void OnDisable()
  {
    if (!GameEngine.IsAvailable || !GameEngine.IsReady() || GameEngine.IsShuttingDown)
      return;
    MessageHub.inst.GetPortByAction("job_complete").RemoveEvent(new System.Action<object>(this.OnJobCompleted));
  }

  public override void InitBuilding()
  {
    base.InitBuilding();
    this.onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    this.UpdateCollectionUI(PlayerData.inst.dragonKnightData);
    DBManager.inst.DB_Dragon.onDataChanged += new System.Action<long>(this.OnDragonDataChanged);
    DBManager.inst.DB_DragonKnight.onDataChanged += new System.Action<DragonKnightData>(this.OnDragonKnightDataChanged);
  }

  public override void Dispose()
  {
    base.Dispose();
    this.onBuildingSelected -= new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    MessageHub.inst.GetPortByAction("job_complete").RemoveEvent(new System.Action<object>(this.OnJobCompleted));
    DBManager.inst.DB_Dragon.onDataChanged -= new System.Action<long>(this.OnDragonDataChanged);
    DBManager.inst.DB_DragonKnight.onDataChanged -= new System.Action<DragonKnightData>(this.OnDragonKnightDataChanged);
  }

  private void OnDragonKnightDataChanged(DragonKnightData data)
  {
    if (data.UserId != PlayerData.inst.uid || !((UnityEngine.Object) null != (UnityEngine.Object) this.mCollectionDragonKnight))
      return;
    this.mCollectionDragonKnight.Close();
  }

  private void OnDragonDataChanged(long id)
  {
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.mCollectionDragonKnight || PlayerData.inst.dragonKnightData != null || (PlayerData.inst.uid != id || PlayerData.inst.dragonData.Level <= 9))
      return;
    this.mCollectionDragonKnight = CollectionUIManager.Instance.OpenCollectionUI(this.transform, new CityCircleBtnPara(AtlasType.Gui_2, "btn_dragon_knight_summon", "id_build", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("DragonKnight/DragonKnightProfileDlg", (UI.Dialog.DialogParameter) null, true, true, true))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false)
    {
      closeAfterClick = false
    }, CollectionUI.CollectionType.TrainSoldier);
  }

  private void OnJobCompleted(object orgData)
  {
    Hashtable inData = orgData as Hashtable;
    if (inData == null)
      return;
    int outData = 0;
    DatabaseTools.UpdateData(inData, "event_type", ref outData);
    if (outData != 77)
      return;
    this.UpdateCollectionUI(PlayerData.inst.dragonKnightData);
  }

  private void UpdateCollectionUI(DragonKnightData dragonKnightData)
  {
    if (dragonKnightData == null || dragonKnightData.UserId != PlayerData.inst.uid)
      return;
    if (EquipmentManager.Instance.HasUnCollectedEquipment(BagType.DragonKnight))
    {
      if ((bool) ((UnityEngine.Object) this.m_CollectionUI))
        return;
      this.m_CollectionUI = CollectionUIManager.Instance.OpenCollectionUI(this.transform, new CityCircleBtnPara(AtlasType.Gui_2, "btn_finish_forging", "id_build", new EventDelegate((EventDelegate.Callback) (() => EquipmentManager.Instance.CollectEquipment(BagType.DragonKnight, (System.Action<bool, object>) ((ret, orgData) =>
      {
        if (!ret)
          return;
        Hashtable hashtable = orgData as Hashtable;
        UIManager.inst.OpenPopup("Equipment/EquipmentForgeSuccessfullyPopup", (Popup.PopupParameter) new EquipmentForgeSuccessfullyPopup.Parameter()
        {
          bagType = BagType.DragonKnight,
          equipmenItemID = (long) int.Parse(hashtable[(object) "item_id"].ToString())
        });
      })))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false), CollectionUI.CollectionType.TrainSoldier);
    }
    else
    {
      if (!(bool) ((UnityEngine.Object) this.m_CollectionUI))
        return;
      this.m_CollectionUI.Close();
      this.m_CollectionUI = (CollectionUI) null;
    }
  }

  public override void AddActionButton(ref List<CityCircleBtnPara> ftl)
  {
    this.CCBPair = CityCircleBtnCollection.Instance.CircleBtnPair;
    ftl.Add(this.CCBPair["dk_detail"]);
    DragonKnightData dragonKnightData = PlayerData.inst.dragonKnightData;
    if (dragonKnightData != null && dragonKnightData.ForgeJobId > 0L)
    {
      ftl.Add(this.CCBPair["actionSpeedup"]);
      JobHandle singleJobByClass = JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_DK_FORGE);
      if ((bool) singleJobByClass && singleJobByClass.LeftTime() > 0)
      {
        CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "gold_speed_up", "id_gold_speedup", new EventDelegate((MonoBehaviour) this, "OnUseGoldSpeedUpBtnClicked"), true, (string) null, AssetManager.Instance.HandyLoad("Prefab/City/GoldBoostResAttachment", (System.Type) null) as GameObject, (CityTouchCircleBtnAttachment.AttachmentData) new GoldBoostResAttachment.GBRAData()
        {
          remainTime = singleJobByClass.LeftTime()
        }, false);
        ftl.Add(cityCircleBtnPara);
      }
    }
    ftl.Add(this.CCBPair["dk_forge"]);
    ftl.Add(this.CCBPair["dk_enhance"]);
    ftl.Add(this.CCBPair["dk_armory"]);
  }

  private void OnUseGoldSpeedUpBtnClicked()
  {
    DragonKnightData dragonKnightData = PlayerData.inst.dragonKnightData;
    if (dragonKnightData == null)
      return;
    JobHandle unfinishedJob = JobManager.Instance.GetUnfinishedJob(dragonKnightData.ForgeJobId);
    if (!(bool) unfinishedJob)
      return;
    int num1 = 0;
    int num2 = unfinishedJob.LeftTime();
    int time = num2 - num1;
    GoldConsumePopup.Parameter parameter = new GoldConsumePopup.Parameter();
    int cost = ItemBag.CalculateCost(time, 0);
    parameter.confirmButtonClickEvent = new System.Action(this.ConFirmCallBack);
    parameter.lefttime = num2;
    parameter.goldNum = cost;
    parameter.freetime = num1;
    parameter.description = ScriptLocalization.GetWithPara("confirm_gold_spend_upgrade_instant_desc", new Dictionary<string, string>()
    {
      {
        "num",
        cost.ToString()
      }
    }, true);
    UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) parameter);
  }

  private void ConFirmCallBack()
  {
    DragonKnightData dragonKnightData = PlayerData.inst.dragonKnightData;
    if (dragonKnightData == null)
      return;
    JobHandle unfinishedJob = JobManager.Instance.GetUnfinishedJob(dragonKnightData.ForgeJobId);
    if (!(bool) unfinishedJob)
      return;
    int num1 = 0;
    int num2 = unfinishedJob.LeftTime();
    int num3 = num2 - num1;
    if (num2 - num1 <= 0)
      return;
    int cost = ItemBag.CalculateCost(num2 - num1, 0);
    MessageHub.inst.GetPortByAction("City:speedUpByGold").SendRequest(new Hashtable()
    {
      {
        (object) "job_id",
        (object) dragonKnightData.ForgeJobId
      },
      {
        (object) "gold",
        (object) cost
      }
    }, (System.Action<bool, object>) null, true);
  }

  public override bool IsIdleState()
  {
    return false;
  }

  private void OnBuildingSelected()
  {
    CitadelSystem.inst.OpenTouchCircle();
    CityTouchCircle.Instance.Title = Utils.XLAT("dragon_knight_chamber");
    AudioManager.Instance.StopAndPlaySound("sfx_city_click_dragon_knight_chamber");
  }
}
