﻿// Decompiled with JetBrains decompiler
// Type: AllianceMemberHeaderExpanded
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class AllianceMemberHeaderExpanded : MonoBehaviour, IOnlineUpdateable
{
  private int m_Title = 1;
  private List<AllianceMemberPanel> m_AllianceMembers = new List<AllianceMemberPanel>();
  public UISprite mHeaderIcon;
  public UILabel mHeaderLabel;
  public UILabel mOnlineLabel;
  public GameObject mMemberItemPrefab;
  public UIWidget mPanelFrame;
  private AllianceData m_AllianceData;

  public void SetDetails(AllianceData allianceData, int title, int online, int total, System.Action<AllianceMemberData> callback)
  {
    this.m_AllianceData = allianceData;
    this.m_Title = title;
    this.mHeaderIcon.spriteName = AllianceUtilities.GetTitleIconPath(title);
    this.mHeaderLabel.text = this.m_AllianceData.GetTitleName(title);
    this.mOnlineLabel.text = string.Format("({0}/{1})", (object) online, (object) total);
    float y = this.mMemberItemPrefab.transform.localPosition.y;
    float height = (float) this.mMemberItemPrefab.GetComponent<UIWidget>().height;
    UIWidget component1 = this.GetComponent<UIWidget>();
    using (Dictionary<long, AllianceMemberData>.ValueCollection.Enumerator enumerator = allianceData.members.datas.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceMemberData current = enumerator.Current;
        if (current.title == title && current.status == 0)
        {
          AllianceMemberPanel component2 = Utils.DuplicateGOB(this.mMemberItemPrefab).GetComponent<AllianceMemberPanel>();
          component2.gameObject.SetActive(true);
          component2.SetDetails(current, callback);
          Utils.SetLPY(component2.gameObject, y);
          y -= height;
          this.m_AllianceMembers.Add(component2);
        }
      }
    }
    component1.height = (int) -(double) y;
  }

  public int title
  {
    get
    {
      return this.m_Title;
    }
  }

  public void UpdateOnlineStatus(List<long> status)
  {
    int num = 0;
    int count = this.m_AllianceMembers.Count;
    for (int index = 0; index < count; ++index)
    {
      if (this.m_AllianceMembers[index].UpdateOnlineStatus(status))
        ++num;
    }
    this.mOnlineLabel.text = string.Format("({0}/{1})", (object) num, (object) count);
  }

  GameObject IOnlineUpdateable.get_gameObject()
  {
    return this.gameObject;
  }
}
