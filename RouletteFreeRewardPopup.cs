﻿// Decompiled with JetBrains decompiler
// Type: RouletteFreeRewardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;

public class RouletteFreeRewardPopup : Popup
{
  private const string RECHARGE_GOLD_COUNT = "tavern_dice_gold_topup_tip";
  private const string PAY_GOLD_COUNT = "event_lucky_draw_gold_topup_num";
  private const string AVAILABLE_GOLD_COUNT = "event_lucky_draw_draws_remaining";
  private const string CLIAMED_GOLD_COUNT = "event_lucky_draw_draws_taken";
  private const string DAILY_TIP = "event_lucky_draw_daily_reward_tip";
  private const string ROULETTE_CLOSED = "tavern_chance_closed_description";
  public UILabel topItemTitle;
  public UILabel totalValue;
  public UILabel claimedValue;
  public UILabel remainingValue;
  public UITexture topItemTexture;
  public UILabel requireDis;
  public UITexture bottomItemTexture;
  public UILabel count;
  public UIButton topGetMoreBtn;
  public UIButton topCliamBtn;
  public UIButton bottomGotoBtn;
  public UIButton bottomCliamBtn;
  private int itemGold;
  private int itemNum;
  private int itemDailySocre;
  private int itemDailyNum;
  private int paymentGotGold;
  private int claimedGold;
  private int remainingGold;
  private int itemId;
  private RouletteFreeRewardPopup.CollectType type;
  private System.Action collectCompleteHandler;
  private RouletteFreeRewardPopup.Parameter param;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    if (orgParam != null)
    {
      this.param = orgParam as RouletteFreeRewardPopup.Parameter;
      this.collectCompleteHandler = this.param.CollectCompleteHandler;
    }
    this.AddEventHandler();
    this.InitData();
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void OnSecondEvent(int time)
  {
    if (!RoulettePayload.Instance.IsRouletteClose)
      return;
    this.totalValue.text = ScriptLocalization.GetWithPara("event_lucky_draw_gold_topup_num", new Dictionary<string, string>()
    {
      {
        "0",
        "0"
      }
    }, true);
  }

  private void RemoveEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  private void InitData()
  {
    TavernWheelGroupInfo tavernWheelGroupInfo = ConfigManager.inst.DB_TavernWheelGroup.Get(RoulettePayload.Instance.GroupId);
    if (tavernWheelGroupInfo != null)
    {
      this.itemGold = tavernWheelGroupInfo.itemGold;
      this.itemNum = tavernWheelGroupInfo.itemNumber;
      this.itemDailySocre = tavernWheelGroupInfo.itemDailyScore;
      this.itemDailyNum = tavernWheelGroupInfo.itemDailyNumber;
      this.itemId = tavernWheelGroupInfo.itemId;
    }
    this.paymentGotGold = RoulettePayload.Instance.PaymentGotGold;
    this.claimedGold = RoulettePayload.Instance.ClaimedGold;
    this.remainingGold = this.paymentGotGold / this.itemGold * this.itemNum - this.claimedGold;
  }

  private void UpdateUI()
  {
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("0", this.itemGold.ToString());
    para.Add("1", this.itemNum.ToString());
    this.topItemTitle.text = ScriptLocalization.GetWithPara("tavern_dice_gold_topup_tip", para, true);
    para.Clear();
    para.Add("0", Utils.FormatThousands(this.paymentGotGold.ToString()));
    this.totalValue.text = ScriptLocalization.GetWithPara("event_lucky_draw_gold_topup_num", para, true);
    para.Clear();
    para.Add("0", this.claimedGold.ToString());
    this.claimedValue.text = ScriptLocalization.GetWithPara("event_lucky_draw_draws_taken", para, true);
    para.Clear();
    para.Add("0", (this.remainingGold <= 0 ? 0 : this.remainingGold).ToString());
    this.remainingValue.text = ScriptLocalization.GetWithPara("event_lucky_draw_draws_remaining", para, true);
    this.count.text = this.itemDailyNum.ToString();
    int currentPoint = DBManager.inst.DB_DailyActives.CurrentPoint;
    string str = currentPoint.ToString() + "/" + this.itemDailySocre.ToString();
    para.Clear();
    para.Add("0", str);
    this.requireDis.text = ScriptLocalization.GetWithPara("event_lucky_draw_daily_reward_tip", para, true);
    if (this.remainingGold > 0)
    {
      this.topGetMoreBtn.gameObject.SetActive(false);
      this.topCliamBtn.gameObject.SetActive(true);
    }
    else
    {
      this.topGetMoreBtn.gameObject.SetActive(true);
      this.topCliamBtn.gameObject.SetActive(false);
    }
    if (currentPoint < this.itemDailySocre)
    {
      this.bottomGotoBtn.gameObject.SetActive(true);
      this.bottomCliamBtn.gameObject.SetActive(false);
    }
    else
    {
      this.bottomGotoBtn.gameObject.SetActive(false);
      this.bottomCliamBtn.gameObject.SetActive(true);
      if (!RoulettePayload.Instance.DailyClaimable)
        this.bottomCliamBtn.isEnabled = false;
    }
    string textureImagePath = string.Empty;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.itemId);
    if (itemStaticInfo != null)
      textureImagePath = itemStaticInfo.ImagePath;
    MarksmanPayload.Instance.FeedMarksmanTexture(this.topItemTexture, textureImagePath);
    MarksmanPayload.Instance.FeedMarksmanTexture(this.bottomItemTexture, textureImagePath);
  }

  public void OnGetMoreBtnClicked()
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
    {
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    }
    else
    {
      this.OnCloseBtnClicked();
      Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
    }
  }

  public void OnCliamBtnClicked()
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
    {
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    }
    else
    {
      this.type = RouletteFreeRewardPopup.CollectType.PAYMENT;
      this.CollectReward(RouletteFreeRewardPopup.CollectType.PAYMENT);
    }
  }

  public void OnGotoBtnClicked()
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    else
      RequestManager.inst.SendRequest("Quest:refreshDailyQuest", (Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.OnCloseBtnClicked();
        UIManager.inst.OpenPopup("DailyActivy/DailyActiviesPopup", (Popup.PopupParameter) null);
      }), true);
  }

  public void OnCollectBtnClicked()
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
    {
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    }
    else
    {
      this.type = RouletteFreeRewardPopup.CollectType.DAILY;
      this.CollectReward(RouletteFreeRewardPopup.CollectType.DAILY);
    }
  }

  private void CollectReward(RouletteFreeRewardPopup.CollectType type)
  {
    this.type = type;
    RoulettePayload.Instance.SendGetTokenRequest(new System.Action<bool, object>(this.OnTokenRequestCallback), (int) type, true);
  }

  private void OnTokenRequestCallback(bool ret, object data)
  {
    if (!ret)
      return;
    if (this.type == RouletteFreeRewardPopup.CollectType.DAILY)
    {
      RoulettePayload.Instance.DailyClaimable = false;
      RoulettePayload.Instance.ShowCollectEffect(this.itemId, this.itemDailyNum);
    }
    else
      RoulettePayload.Instance.ShowCollectEffect(this.itemId, this.remainingGold);
    this.InitData();
    this.UpdateUI();
    if (this.collectCompleteHandler == null)
      return;
    this.collectCompleteHandler();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.RemoveEventHandler();
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public enum CollectType
  {
    PAYMENT = 1,
    DAILY = 2,
  }

  public class Parameter : Popup.PopupParameter
  {
    public System.Action CollectCompleteHandler;
  }
}
