﻿// Decompiled with JetBrains decompiler
// Type: ConfigShopCommonGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigShopCommonGroup
{
  private ConfigParse _parse = new ConfigParse();
  private Dictionary<string, ShopCommonGroup> _dataByID;
  private Dictionary<int, ShopCommonGroup> _dataByInternalID;

  public void BuildDB(object res)
  {
    Hashtable sources = res as Hashtable;
    if (sources == null)
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    else
      this._parse.Parse<ShopCommonGroup, string>(sources, "id", out this._dataByID, out this._dataByInternalID);
  }

  public ShopCommonGroup Get(string id)
  {
    ShopCommonGroup shopCommonGroup;
    this._dataByID.TryGetValue(id, out shopCommonGroup);
    return shopCommonGroup;
  }
}
