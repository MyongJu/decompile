﻿// Decompiled with JetBrains decompiler
// Type: FallenKnightRewardsHelpPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;

public class FallenKnightRewardsHelpPopup : Popup
{
  public UIScrollView scrollView;
  public UITable table;
  public UILabel helpInfoLable;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.ShowRewardsHelpInfo();
  }

  public void ShowRewardsHelpInfo()
  {
    List<FallenKnightMainInfo> fallenKnightMainList = ConfigManager.inst.DB_FallenKnightMain.GetFallenKnightMainList();
    string str1 = string.Empty;
    string str2 = string.Empty;
    for (int index = 0; index < fallenKnightMainList.Count; ++index)
    {
      NPC_Info data = ConfigManager.inst.DB_NPC.GetData(int.Parse(fallenKnightMainList[index].NpcId));
      if (data != null)
        str1 = Utils.XLAT(data.name);
      str2 = str2 + string.Format("{0}--{1}", (object) str1, (object) fallenKnightMainList[index].Score) + "\n";
    }
    this.helpInfoLable.text = string.Format(Utils.XLAT("help_event_fallen_knight_rewards_description"), (object) str2);
    this.Reposition();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }
}
