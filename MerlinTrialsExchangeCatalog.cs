﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsExchangeCatalog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MerlinTrialsExchangeCatalog : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelSelected;
  [SerializeField]
  private UILabel _labelUnselected;
  public System.Action<string> OnCategoryClicked;
  private string _category;
  private bool _selected;

  public void SetData(string category)
  {
    this._category = category;
    this.ResetCategory(this._category == "all");
    this.UpdateUI();
  }

  public void ResetCategory(bool showSelected = false)
  {
    this._selected = showSelected;
    NGUITools.SetActive(this._labelSelected.transform.parent.gameObject, showSelected);
  }

  public void OnCategoryClick()
  {
    if (this._selected)
      return;
    this.ResetAllCategory();
    this.ResetCategory(true);
    if (this.OnCategoryClicked == null)
      return;
    this.OnCategoryClicked(this._category);
  }

  private void ResetAllCategory()
  {
    MerlinTrialsExchangeCatalog[] componentsInChildren = this.transform.parent.GetComponentsInChildren<MerlinTrialsExchangeCatalog>();
    if (componentsInChildren == null)
      return;
    foreach (MerlinTrialsExchangeCatalog trialsExchangeCatalog in componentsInChildren)
      trialsExchangeCatalog.ResetCategory(false);
  }

  private void UpdateUI()
  {
    UILabel labelSelected = this._labelSelected;
    string categoryContent = MerlinTrialsPayload.Instance.GetCategoryContent(this._category);
    this._labelUnselected.text = categoryContent;
    string str = categoryContent;
    labelSelected.text = str;
  }
}
