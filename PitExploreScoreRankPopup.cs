﻿// Decompiled with JetBrains decompiler
// Type: PitExploreScoreRankPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PitExploreScoreRankPopup : Popup
{
  [SerializeField]
  private UILabel _MyRank;
  [SerializeField]
  private UILabel _MyScore;
  [SerializeField]
  private UITable _Table;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private GameObject _RankItemTemplate;
  [SerializeField]
  private GameObject _NoRankHint;
  private int mMyRank;
  private long mMyScore;
  private List<PitScoreRankData> mRankDataList;

  private void UpdateUI()
  {
    this._MyRank.text = string.Format("{0} {1}", (object) Utils.XLAT("leader_board_rank"), (object) this.mMyRank);
    this._MyScore.text = string.Format("{0} {1}", (object) Utils.XLAT("event_fire_kingdom_your_fossils_collected"), (object) this.mMyScore);
    NGUITools.SetActive(this._NoRankHint, this.mRankDataList == null || this.mRankDataList.Count == 0);
    NGUITools.SetActive(this._MyRank.gameObject, true);
    NGUITools.SetActive(this._MyScore.gameObject, true);
    if (this.mRankDataList == null || this.mRankDataList.Count <= 0)
      return;
    UIUtils.ClearTable(this._Table);
    using (List<PitScoreRankData>.Enumerator enumerator = this.mRankDataList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PitScoreRankData current = enumerator.Current;
        GameObject go = NGUITools.AddChild(this._Table.gameObject, this._RankItemTemplate);
        NGUITools.SetActive(go, true);
        go.GetComponent<PitExploreScoreRankItemComponent>().FeedData((IComponentData) new PitExploreScoreRankItemComponentData()
        {
          rankData = current
        });
      }
    }
    this._Table.Reposition();
    this._scrollView.ResetPosition();
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnShowRewardPressed()
  {
    UIManager.inst.OpenPopup("Pit/PitAllRewardPopup", (Popup.PopupParameter) null);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    NGUITools.SetActive(this._MyRank.gameObject, false);
    NGUITools.SetActive(this._MyScore.gameObject, false);
    NGUITools.SetActive(this._NoRankHint.gameObject, false);
    MessageHub.inst.GetPortByAction("Abyss:getRankInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable hashtable1 = data as Hashtable;
      if (hashtable1 == null)
        return;
      if (hashtable1.ContainsKey((object) "rank"))
        int.TryParse(hashtable1[(object) "rank"] as string, out this.mMyRank);
      if (hashtable1.ContainsKey((object) "score"))
        long.TryParse(hashtable1[(object) "score"] as string, out this.mMyScore);
      if (hashtable1.ContainsKey((object) "list"))
      {
        this.mRankDataList = new List<PitScoreRankData>();
        ArrayList arrayList = hashtable1[(object) "list"] as ArrayList;
        int num = 1;
        foreach (object obj in arrayList)
        {
          Hashtable hashtable2 = obj as Hashtable;
          int result = 0;
          if (hashtable2.ContainsKey((object) "lord_title"))
            int.TryParse(hashtable2[(object) "lord_title"].ToString(), out result);
          this.mRankDataList.Add(new PitScoreRankData()
          {
            world_id = int.Parse(hashtable2[(object) "world_id"] as string),
            name = hashtable2[(object) "name"] as string,
            acronym = hashtable2[(object) "acronym"] as string,
            score = long.Parse(hashtable2[(object) "score"] as string),
            portrait = int.Parse(hashtable2[(object) "portrait"] as string),
            lordTitleId = result,
            icon = hashtable2[(object) "icon"] as string,
            rank = num++
          });
        }
      }
      this.UpdateUI();
    }), true);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }
}
