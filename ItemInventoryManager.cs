﻿// Decompiled with JetBrains decompiler
// Type: ItemInventoryManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class ItemInventoryManager : MonoBehaviour
{
  private string SLOT_NAME_FORMAT = "SLOT_{0}_{1}";
  private GameObjectPool m_GameObjectPool = new GameObjectPool();
  public GameObject itemContainer;
  public GameObject itemSlotOrg;
  public UIScrollView scrollView;
  public UIGrid slotGrid;
  public GameObject gemContainer;
  private Dictionary<long, InventoryItemData> ItemDict;
  private Dictionary<long, ItemSlot_New> m_ItemSlotDict;
  private bool m_Initialized;
  private List<InventoryItemData> displayList;
  private int displayIndex;
  public System.Action<long> OnItemClick;

  private void OnEnable()
  {
    DBManager.inst.GetInventory(BagType.Hero).onDataRemoved += new System.Action<long, long>(this.OnItemDataDeleted);
  }

  private void OnDisable()
  {
    DBManager.inst.GetInventory(BagType.Hero).onDataRemoved -= new System.Action<long, long>(this.OnItemDataDeleted);
  }

  private void OnItemDataDeleted(long uid, long itemId)
  {
    if (PlayerData.inst.uid != uid)
      return;
    this.displayList.Remove(this.ItemDict[itemId]);
    this.ItemDict.Remove(itemId);
    ItemSlot_New itemSlotNew = (ItemSlot_New) null;
    this.m_ItemSlotDict.TryGetValue(itemId, out itemSlotNew);
    if (!((UnityEngine.Object) itemSlotNew != (UnityEngine.Object) null))
      return;
    itemSlotNew.gameObject.SetActive(false);
    this.m_GameObjectPool.Release(itemSlotNew.gameObject);
    this.m_ItemSlotDict.Remove(itemId);
    this.slotGrid.Reposition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  public void Init()
  {
    if (this.m_Initialized)
      return;
    this.ItemDict = new Dictionary<long, InventoryItemData>();
    this.displayList = new List<InventoryItemData>();
    this.m_ItemSlotDict = new Dictionary<long, ItemSlot_New>();
    this.m_GameObjectPool.Initialize(this.itemSlotOrg, this.gameObject);
    this.m_Initialized = true;
  }

  public InventoryItemData getDisplayItemAfterDelete()
  {
    if (this.displayList.Count == 0)
      return (InventoryItemData) null;
    if (this.displayIndex == this.displayList.Count)
      return this.displayList[this.displayList.Count - 1];
    return this.displayList[this.displayIndex];
  }

  private List<InventoryItemData> sortItemDict(Dictionary<long, InventoryItemData> itemDict)
  {
    return new List<InventoryItemData>();
  }

  public void Clear()
  {
    this.ItemDict.Clear();
    using (Dictionary<long, ItemSlot_New>.ValueCollection.Enumerator enumerator = this.m_ItemSlotDict.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ItemSlot_New current = enumerator.Current;
        current.gameObject.SetActive(false);
        this.m_GameObjectPool.Release(current.gameObject);
      }
    }
    this.m_ItemSlotDict.Clear();
    this.displayList.Clear();
  }

  public void ItemClick(long itemId)
  {
    if (this.OnItemClick == null)
      return;
    this.OnItemClick(itemId);
  }

  public void MarkItem(long itemId, bool mark = true)
  {
    this.MarkItem(itemId, Color.yellow, mark);
  }

  public void MarkItem(long itemId, Color color, bool mark = true)
  {
    ItemSlot_New itemSlotNew = (ItemSlot_New) null;
    this.m_ItemSlotDict.TryGetValue(itemId, out itemSlotNew);
    if (!((UnityEngine.Object) itemSlotNew != (UnityEngine.Object) null))
      return;
    this.m_ItemSlotDict[itemId].isHighlight = mark;
    this.m_ItemSlotDict[itemId].highlightColor = color;
    this.displayIndex = this.displayList.IndexOf(this.ItemDict[itemId]);
  }
}
