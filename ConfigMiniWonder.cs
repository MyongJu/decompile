﻿// Decompiled with JetBrains decompiler
// Type: ConfigMiniWonder
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigMiniWonder
{
  private Dictionary<int, MiniWonderData> m_MiniWonderDict = new Dictionary<int, MiniWonderData>();

  public void BuildDB(object result)
  {
    Hashtable hashtable1 = result as Hashtable;
    if (hashtable1 == null)
    {
      D.error((object) "The Config({0}) load error ; result is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable1[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exsit", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "level");
        int index3 = ConfigManager.GetIndex(inHeader, "prefab");
        int index4 = ConfigManager.GetIndex(inHeader, "stronghold_lv_min");
        int index5 = ConfigManager.GetIndex(inHeader, "stronghold_lv_max");
        int index6 = ConfigManager.GetIndex(inHeader, "benefit_id_1");
        int index7 = ConfigManager.GetIndex(inHeader, "benefit_id_2");
        int index8 = ConfigManager.GetIndex(inHeader, "benefit_id_3");
        int index9 = ConfigManager.GetIndex(inHeader, "benefit_value_1");
        int index10 = ConfigManager.GetIndex(inHeader, "benefit_value_2");
        int index11 = ConfigManager.GetIndex(inHeader, "benefit_value_3");
        int index12 = ConfigManager.GetIndex(inHeader, "AllianceBenefits");
        hashtable1.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable1.Keys)
        {
          MiniWonderData data = new MiniWonderData();
          if (int.TryParse(key.ToString(), out data.InternalId))
          {
            ArrayList arr = hashtable1[key] as ArrayList;
            if (arr != null)
            {
              data.ID = ConfigManager.GetString(arr, index1);
              data.Level = ConfigManager.GetInt(arr, index2);
              data.Prefab = ConfigManager.GetString(arr, index3);
              data.MinCityLevel = ConfigManager.GetInt(arr, index4);
              data.MaxCityLevel = ConfigManager.GetInt(arr, index5);
              data.BenefitId[0] = ConfigManager.GetInt(arr, index6);
              data.BenefitId[1] = ConfigManager.GetInt(arr, index7);
              data.BenefitId[2] = ConfigManager.GetInt(arr, index8);
              data.BenefitValue[0] = ConfigManager.GetDouble(arr, index9);
              data.BenefitValue[1] = ConfigManager.GetDouble(arr, index10);
              data.BenefitValue[2] = ConfigManager.GetDouble(arr, index11);
              Hashtable hashtable2 = arr[index12] as Hashtable;
              if (hashtable2 != null)
              {
                int index13 = 0;
                for (IDictionaryEnumerator enumerator = hashtable2.GetEnumerator(); enumerator.MoveNext() && index13 < 3; ++index13)
                {
                  data.AllianceBenefitId[index13] = int.Parse(enumerator.Key.ToString());
                  data.AllianceBenefitValue[index13] = double.Parse(enumerator.Value.ToString());
                }
              }
              this.PushData(data.InternalId, data);
            }
          }
        }
      }
    }
  }

  public void PushData(int internalId, MiniWonderData data)
  {
    if (this.m_MiniWonderDict.ContainsKey(internalId))
      return;
    this.m_MiniWonderDict.Add(internalId, data);
    ConfigManager.inst.AddData(internalId, (object) data);
  }

  public void Clear()
  {
    if (this.m_MiniWonderDict == null)
      return;
    this.m_MiniWonderDict.Clear();
  }

  public MiniWonderData GetData(int id)
  {
    MiniWonderData miniWonderData = (MiniWonderData) null;
    this.m_MiniWonderDict.TryGetValue(id, out miniWonderData);
    return miniWonderData;
  }
}
