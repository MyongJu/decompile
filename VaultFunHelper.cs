﻿// Decompiled with JetBrains decompiler
// Type: VaultFunHelper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Diagnostics;
using UI;
using UnityEngine;

public class VaultFunHelper
{
  public float minAlpha = 0.05f;
  public float randomMinAlpha = 0.9f;
  public float randomMaxAlpha = 0.99f;
  public float angle = 10f;
  public float animScale = 50f;
  public float absorbBaseSpeedBCR = 1200f;
  public float absorbAccelerationBCR = 1500f;
  public float speedFactorBCR = 1000f;
  public float reductionTimeBCR = 1f;
  public float xPositionOffset = 350f;
  private Matrix4x4 _offsetMatrix = new Matrix4x4();
  public float timeInterval;
  private static VaultFunHelper _instance;

  public static VaultFunHelper Instance
  {
    get
    {
      if (VaultFunHelper._instance == null)
        VaultFunHelper._instance = new VaultFunHelper();
      return VaultFunHelper._instance;
    }
  }

  [DebuggerHidden]
  public IEnumerator BreathingEffect(UIProgressBar progressSlider)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new VaultFunHelper.\u003CBreathingEffect\u003Ec__IteratorBC()
    {
      progressSlider = progressSlider,
      \u003C\u0024\u003EprogressSlider = progressSlider,
      \u003C\u003Ef__this = this
    };
  }

  public void RequestVaultData(System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("player:loadUserBank").SendLoader(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || data == null)
        return;
      DBManager.inst.DB_Treasury.UpdateDatas(data as Hashtable, (long) NetServerTime.inst.ServerTimestamp);
      if (callback == null)
        return;
      callback(ret, data);
    }), true, false);
  }

  public void ShowVault(System.Action callback)
  {
    MessageHub.inst.GetPortByAction("player:loadUserBank").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || data == null)
        return;
      DBManager.inst.DB_Treasury.UpdateDatas(data as Hashtable, (long) NetServerTime.inst.ServerTimestamp);
      UIManager.inst.OpenDlg("Vault/VaultDlg", (UI.Dialog.DialogParameter) new VaultDlg.Parameter()
      {
        func = callback
      }, 1 != 0, 1 != 0, 1 != 0);
    }), true);
  }

  public void SaveGold(int slotId, int goldAmount)
  {
    MessageHub.inst.GetPortByAction("player:saveGold").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "slot_id",
        (object) slotId
      },
      {
        (object) "gold_amount",
        (object) goldAmount
      }
    }, (System.Action<bool, object>) null, true);
  }

  public void DrawGold(bool showCollectEffect = true, System.Action callback = null)
  {
    MessageHub.inst.GetPortByAction("player:drawGold").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      if (showCollectEffect)
        this.ShowCoinCollectEffect();
      if (callback == null)
        return;
      callback();
    }), true);
  }

  public void ShowCoinCollectEffect()
  {
    RewardsCollectionAnimator.Instance.CollectCoins();
  }

  [DebuggerHidden]
  public IEnumerator ShowCoinCollectEffect(GameObject go, Transform gt, int coinCount = 15)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new VaultFunHelper.\u003CShowCoinCollectEffect\u003Ec__IteratorBD()
    {
      gt = gt,
      coinCount = coinCount,
      go = go,
      \u003C\u0024\u003Egt = gt,
      \u003C\u0024\u003EcoinCount = coinCount,
      \u003C\u0024\u003Ego = go,
      \u003C\u003Ef__this = this
    };
  }

  private void Shoot(Vector3 beginPos, Vector3 finalDir, GameObject go, Transform gt, int index)
  {
    GameObject effect = Utils.DuplicateGOB(go);
    effect.transform.localScale = new Vector3(this.animScale, this.animScale, this.animScale);
    effect.transform.localPosition = beginPos;
    effect.name = nameof (Shoot) + (object) index;
    Vector3 vector3 = new Vector3(gt.localPosition.x - this.xPositionOffset, gt.localPosition.y, gt.localPosition.z);
    Shooter st = gt.gameObject.AddComponent<Shooter>();
    st.Shoot(new Shooter.ShooterPara()
    {
      go = effect,
      begin = beginPos,
      end = vector3,
      initspeed = finalDir.normalized,
      onFinished = (System.Action) (() =>
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) effect);
        UnityEngine.Object.Destroy((UnityEngine.Object) st);
      }),
      absorbBaseSpeed = this.absorbBaseSpeedBCR,
      absorbAcceleration = this.absorbAccelerationBCR,
      speedFactor = this.speedFactorBCR,
      reductionTime = this.reductionTimeBCR
    }, 0.0f);
    AudioManager.Instance.PlaySound("sfx_city_vault_withdraw", false);
  }
}
