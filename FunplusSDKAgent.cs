﻿// Decompiled with JetBrains decompiler
// Type: FunplusSDKAgent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;

public class FunplusSDKAgent : FunplusSdk.IDelegate
{
  private Dictionary<string, string> gift = new Dictionary<string, string>();
  private string _urlString = string.Empty;
  public const string FUNPLUS_SDK_NAME = "FunplusSDK";
  public System.Action OnInstallSuccessHandler;
  public System.Action<string> OnRegisterNotificationHandler;
  public System.Action OnReveiveDeepLinkHandler;
  private bool _initSDK;
  private string _pushToken;

  public event System.Action OnHelpShiftReceive;

  public string PushToken
  {
    get
    {
      return this._pushToken;
    }
  }

  public bool InitSDK
  {
    get
    {
      return this._initSDK;
    }
  }

  public void OnSdkInstallSuccess(string config)
  {
    this._initSDK = true;
    if (this.OnInstallSuccessHandler == null)
      return;
    this.OnInstallSuccessHandler();
    this.OnInstallSuccessHandler = (System.Action) null;
  }

  public string DeepLinkURL
  {
    get
    {
      return this._urlString;
    }
  }

  public void OnReveiveDeepLinkURL(string urlString)
  {
    this._urlString = urlString;
    if (string.IsNullOrEmpty(urlString) || this.OnReveiveDeepLinkHandler == null)
      return;
    this.OnReveiveDeepLinkHandler();
  }

  public void OnSdkInstallError(FunplusError error)
  {
    D.error((object) ("[SdkInstallError]" + (object) error.GetErrorCode()));
    NetWorkDetector.Instance.RestartOrQuitGame(ScriptLocalization.Get("data_error_initialization2_description", true), ScriptLocalization.Get("data_error_title", true), (string) null);
  }

  public void OnReceiveNotificationMessage(string message)
  {
    if (string.IsNullOrEmpty(message))
      return;
    Hashtable content = Utils.Json2Object(message, true) as Hashtable;
    if (content == null || !content.ContainsKey((object) "channel") || content[(object) "channel"] == null)
      return;
    NotificationChannel notificationChannel = NotificationChannel.Create(content[(object) "channel"].ToString().Trim());
    if (notificationChannel == null)
      return;
    notificationChannel.Execute(content);
  }

  public void OnRegisterPushReady(string message)
  {
    if (string.IsNullOrEmpty(message))
      return;
    Hashtable hashtable = Utils.Json2Object(message, true) as Hashtable;
    if (hashtable == null || !hashtable.ContainsKey((object) "pushToken") || hashtable[(object) "pushToken"] == null)
      return;
    this._pushToken = hashtable[(object) "pushToken"].ToString();
    if (this.OnRegisterNotificationHandler == null)
      return;
    this.OnRegisterNotificationHandler(this._pushToken);
  }

  public Dictionary<string, string> Gift
  {
    get
    {
      return this.gift;
    }
  }

  public void OnReceiveMarketingMessage(string message)
  {
    if (string.IsNullOrEmpty(message))
      return;
    Hashtable hashtable = Utils.Json2Object(message, true) as Hashtable;
    if (hashtable == null)
      return;
    this.Gift.Clear();
    IEnumerator enumerator = hashtable.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !this.Gift.ContainsKey(enumerator.Current.ToString()))
        this.Gift.Add(enumerator.Current.ToString(), hashtable[enumerator.Current].ToString());
    }
  }
}
