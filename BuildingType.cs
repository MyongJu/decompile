﻿// Decompiled with JetBrains decompiler
// Type: BuildingType
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class BuildingType
{
  public const string FARM = "farm";
  public const string MINE = "mine";
  public const string LUMBER_MILL = "lumber_mill";
  public const string HOUSE = "house";
  public const string MARKETPLACE = "marketplace";
  public const string HEALERS_TENT = "healers_tent";
  public const string STOREHOUSE = "storehouse";
  public const string PRISON = "prison";
  public const string HERO_TOWER = "hero_tower";
  public const string FORGE = "forge";
  public const string KNIGHTS_HALL = "knights_hall";
  public const string TROOP_CAMP = "troop_camp";
  public const string UNIVERSITY = "university";
  public const string HOSPITAL = "hospital";
  public const string BARRACKS = "barracks";
  public const string STABLES = "stables";
  public const string FORTRESS = "fortress";
  public const string RANGE = "range";
  public const string SANCTUM = "sanctum";
  public const string WORKSHOP = "workshop";
  public const string STRONGHOLD = "stronghold";
  public const string WALLS = "walls";
  public const string PARADEGROUND = "PARADEGROUND";
  public const string TEMPLE = "temple";
  public const string WATCH_TOWER = "watchtower";
  public const string EMBASSY = "embassy";
  public const string WAR_RALLY = "war_rally";
  public const string MILITARY_TENT = "military_tent";
  public const string WISH_WELL = "wish_well";
  public const string DRAGON_LAIR = "dragon_lair";
  public const string TIMER_CHEST = "timer_chest";
  public const string VALUT = "valut";
  public const string BLACK_MARKET = "black_market";
  public const string RECEPTION = "reception";
}
