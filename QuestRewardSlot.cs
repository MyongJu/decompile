﻿// Decompiled with JetBrains decompiler
// Type: QuestRewardSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using UnityEngine;

public class QuestRewardSlot : MonoBehaviour
{
  [SerializeField]
  private QuestRewardSlot.Panel panel;

  public void Setup(QuestReward reward)
  {
    this.panel.icon.spriteName = reward.GetRewardIconName();
    this.panel.type.text = ScriptLocalization.Get(reward.GetRewardTypeName(), true);
    this.panel.amount.text = reward.GetValueText();
    if (reward is ConsumableReward)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.itemIcon, reward.GetRewardIconName(), (System.Action<bool>) null, true, false, string.Empty);
      NGUITools.SetActive(this.panel.icon.gameObject, false);
      NGUITools.SetActive(this.panel.itemIcon.gameObject, true);
    }
    else
    {
      NGUITools.SetActive(this.panel.icon.gameObject, true);
      NGUITools.SetActive(this.panel.itemIcon.gameObject, false);
    }
  }

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.panel.itemIcon);
  }

  [Serializable]
  protected class Panel
  {
    public UISprite icon;
    public UILabel type;
    public UILabel amount;
    public UITexture itemIcon;
  }
}
