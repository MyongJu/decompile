﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_Stay_Slot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UnityEngine;

public class AllianceWar_MainDlg_Stay_Slot : AllianceWar_MainDlg_Slot
{
  protected AllianceWar_MainDlg_Stay_Slot.Data data = new AllianceWar_MainDlg_Stay_Slot.Data();
  [SerializeField]
  protected AllianceWar_MainDlg_Stay_Slot.Panel panel;

  public override void Setup(long id, AllianceWar_MainDlg_Slot.SlotParameter param)
  {
    AllianceWar_MainDlg_Stay_Slot.StaySlotParameter staySlotParameter = param as AllianceWar_MainDlg_Stay_Slot.StaySlotParameter;
    if (staySlotParameter == null)
      return;
    this.data.marchId = id;
    MarchData marchData = DBManager.inst.DB_March.Get(id);
    UserData userData = DBManager.inst.DB_User.Get(marchData.ownerUid);
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(marchData.ownerAllianceId);
    AllianceWar_MainDlg_RallyList.param.ownerPortrait = 0;
    if (userData != null)
    {
      AllianceWar_MainDlg_RallyList.param.ownerName = allianceData == null ? userData.userName : string.Format("[{0}]{1}", (object) allianceData.allianceAcronym, (object) userData.userName);
      AllianceWar_MainDlg_RallyList.param.ownerPortrait = userData.portrait;
      AllianceWar_MainDlg_RallyList.param.ownerCustomIconUrl = userData.Icon;
      AllianceWar_MainDlg_RallyList.param.ownerLordTitleId = userData.LordTitle;
    }
    AllianceWar_MainDlg_RallyList.param.memberCount = staySlotParameter.userCount;
    AllianceWar_MainDlg_RallyList.param.canJoin = false;
    AllianceWar_MainDlg_RallyList.param.isAttack = true;
    AllianceWar_MainDlg_RallyList.param.rallyType = AllianceWar_MainDlg_RallyList.Params.RallyListType.MEMBER;
    this.panel.memberList.Setup(AllianceWar_MainDlg_RallyList.param);
    AllianceFortressData dataByCoordinate = DBManager.inst.DB_AllianceFortress.GetDataByCoordinate(marchData.targetLocation);
    if (dataByCoordinate != null)
      this.panel.targetSlot.SetFortress(dataByCoordinate.fortressId);
    else
      this.panel.targetSlot.TempSetTemple(marchData.targetLocation);
    this.OnSecond(NetServerTime.inst.ServerTimestamp);
  }

  public void OnDetailClick()
  {
  }

  public virtual void OnSecond(int timeStamp)
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.data.marchId);
    if (marchData != null && marchData.state == MarchData.MarchState.demolishing_fortress)
    {
      this.panel.marchProgress.gameObject.SetActive(true);
      this.panel.marchProgressText.text = string.Format("{0} {1}", (object) Utils.XLAT("war_rally_fortress_being_destroyed"), (object) Utils.FormatTime1(marchData.endTime - timeStamp));
      this.panel.marchProgress.value = (float) (timeStamp - marchData.startTime) * marchData.timeDuration.oneOverTimeDuration;
    }
    else
    {
      this.panel.marchProgress.gameObject.SetActive(false);
      PlayerData.inst.allianceWarManager.LoadDatas();
    }
  }

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void Reset()
  {
    this.panel.targetSlot = this.transform.Find("TargetSlot").gameObject.GetComponent<AllianceWar_TargetSlot>();
    this.panel.targetSlot.Reset();
    this.panel.memberList = this.transform.Find("Container").gameObject.GetComponent<AllianceWar_MainDlg_RallyList>();
    this.panel.memberList.Reset();
    this.panel.marchProgress = this.transform.Find("Background/MarchTimer").gameObject.GetComponent<UISlider>();
    this.panel.marchProgressText = this.transform.Find("Background/MarchTimer/Exp").gameObject.GetComponent<UILabel>();
    UIEventTrigger component = this.transform.Find("Background/ClickIcon").gameObject.GetComponent<UIEventTrigger>();
    component.onClick.Clear();
    component.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnDetailClick)));
  }

  public class StaySlotParameter : AllianceWar_MainDlg_Slot.SlotParameter
  {
    public int userCount;
  }

  [Serializable]
  public class Panel
  {
    public AllianceWar_TargetSlot targetSlot;
    public AllianceWar_MainDlg_RallyList memberList;
    public UISlider marchProgress;
    public UILabel marchProgressText;
  }

  protected class Data
  {
    public long marchId;
  }
}
