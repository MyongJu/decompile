﻿// Decompiled with JetBrains decompiler
// Type: HeroParliamentTitleItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HeroParliamentTitleItem : ComponentRenderBase
{
  [SerializeField]
  private GameObject _UnassaignedCaseGo;
  [SerializeField]
  private ParliamentHeroItem _HeroItem;
  [SerializeField]
  private UILabel _TitleLabel;
  [SerializeField]
  private UILabel _NameLabel;
  [SerializeField]
  private UILabel _UnlockLabel;
  [SerializeField]
  private GameObject _CivilBg;
  [SerializeField]
  private GameObject _MilitaryBg;
  private LegendCardData legendCardData;
  private ParliamentInfo info;

  public void UpdateAssignedData(LegendCardData cardData)
  {
    if (cardData == null)
      this.SetupEmptySlot();
    else
      this.SetupHeroSlot(cardData);
  }

  public override void Init()
  {
    base.Init();
    HeroParliamentTitleItemData data = this.data as HeroParliamentTitleItemData;
    if (data == null)
      return;
    this.info = data.ParliamentInfo;
    if (this.info == null)
      return;
    this._TitleLabel.text = this.info.Name;
    NGUITools.SetActive(this._CivilBg, this.info.positionType == 1);
    NGUITools.SetActive(this._MilitaryBg, this.info.positionType == 2);
    this.SetupEmptySlot();
  }

  private void SetupEmptySlot()
  {
    NGUITools.SetActive(this._UnassaignedCaseGo, true);
    NGUITools.SetActive(this._HeroItem.gameObject, false);
    bool flag = PlayerData.inst.CityData.mStronghold.mLevel >= this.info.unlockLevel;
    this._NameLabel.text = !flag ? Utils.XLAT("hero_council_position_locked") : Utils.XLAT("hero_council_position_vacant");
    if (!flag)
    {
      string withPara = ScriptLocalization.GetWithPara("tavern_daily_rewards_unlock_requirement", new Dictionary<string, string>()
      {
        {
          "num",
          this.info.unlockLevel.ToString()
        }
      }, true);
      if (withPara != null)
        this._UnlockLabel.text = withPara;
    }
    else
      NGUITools.SetActive(this._UnlockLabel.transform.parent.gameObject, false);
    NGUITools.SetActive(this._UnassaignedCaseGo.transform.Find("PlusIcon").gameObject, HeroCardUtils.ShouldPositionShowPlus(this.info));
    UITexture component = this._UnassaignedCaseGo.transform.Find("Texture").GetComponent<UITexture>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) component, this.info.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
  }

  private void SetupHeroSlot(LegendCardData heroData)
  {
    NGUITools.SetActive(this._UnassaignedCaseGo, false);
    NGUITools.SetActive(this._HeroItem.gameObject, true);
    this._HeroItem.SetData(heroData);
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(heroData.LegendId);
    if (parliamentHeroInfo == null)
      return;
    this._NameLabel.text = parliamentHeroInfo.Name;
  }

  public override void Dispose()
  {
    base.Dispose();
  }

  public int GetTitlePosition()
  {
    return this.info.GetPosition();
  }

  public void OnItemClicked()
  {
    if (PlayerData.inst.CityData.mStronghold.mLevel < this.info.unlockLevel)
      return;
    UIManager.inst.OpenDlg("HeroCard/HeroPositionAssignDlg", (UI.Dialog.DialogParameter) new HeroPositionAssignDlg.Parameter()
    {
      heroPosition = this.GetTitlePosition()
    }, 1 != 0, 1 != 0, 1 != 0);
  }
}
