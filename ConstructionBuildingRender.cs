﻿// Decompiled with JetBrains decompiler
// Type: ConstructionBuildingRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ConstructionBuildingRender : MonoBehaviour
{
  public UITexture mBuildingIcon;
  public UILabel mBuildingName;
  public GameObject lockImage;
  public GameObject selectImage;
  public UIWidget uI2DSpriteIcon;
  private bool isOK;
  private int interalID;
  private string iconName;
  private BuildingInfo buildingInfo;
  private string buildingType;
  public System.Action<ConstructionBuildingRender> OnUnitRenderClick;

  public void SetDetails(ConstructionBuildingRender.RenderData data, System.Action<ConstructionBuildingRender> OnUnitRenderClick, UIScrollView scrollView)
  {
    this.OnUnitRenderClick = OnUnitRenderClick;
    this.iconName = "Prefab/UI/BuildingPrefab/ui_" + data.buildingInfo.Building_ImagePath + "_l1";
    this.IsOK = data.bRequirementsOk;
    this.InternalID = data.buildingInfo.internalId;
    this.buildingType = data.buildingInfo.Type;
    this.buildingInfo = data.buildingInfo;
    this.mBuildingName.text = Utils.XLAT(this.buildingInfo.Type + "_name");
    GameObject prefab = AssetManager.Instance.HandyLoad(this.iconName, (System.Type) null) as GameObject;
    if ((UnityEngine.Object) prefab != (UnityEngine.Object) null)
    {
      for (int index = 0; index < this.uI2DSpriteIcon.transform.childCount; ++index)
        UnityEngine.Object.Destroy((UnityEngine.Object) this.uI2DSpriteIcon.transform.GetChild(index).gameObject);
      NGUITools.AddChild(this.uI2DSpriteIcon.gameObject, prefab).GetComponent<UI2DSprite>().depth = 25;
    }
    if (this.IsOK)
      this.lockImage.SetActive(false);
    else
      this.lockImage.SetActive(true);
    this.UnHighLight();
    this.GetComponent<UIDragScrollView>().scrollView = scrollView;
  }

  public void HighLight()
  {
    this.selectImage.SetActive(true);
  }

  public void UnHighLight()
  {
    this.selectImage.SetActive(false);
  }

  public void OnClick()
  {
    if (this.OnUnitRenderClick == null)
      return;
    this.OnUnitRenderClick(this);
  }

  public bool IsOK
  {
    get
    {
      return this.isOK;
    }
    set
    {
      this.isOK = value;
    }
  }

  public int InternalID
  {
    get
    {
      return this.interalID;
    }
    set
    {
      this.interalID = value;
    }
  }

  public string IconName
  {
    get
    {
      return this.iconName;
    }
  }

  public string BuildingName
  {
    get
    {
      return this.buildingInfo.ID;
    }
  }

  public string BuildingType
  {
    get
    {
      return this.buildingType;
    }
  }

  public class RenderData
  {
    public BuildingInfo buildingInfo;
    public bool bRequirementsOk;

    public RenderData(bool bRequirementsOk, BuildingInfo buildingInfo)
    {
      this.buildingInfo = buildingInfo;
      this.bRequirementsOk = bRequirementsOk;
    }
  }
}
