﻿// Decompiled with JetBrains decompiler
// Type: PlayerProfileInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class PlayerProfileInfo
{
  public UILabel name;
  public UILabel nameValue;
  public UILabel alliance;
  public UILabel allianceValue;
  public UILabel kiongdom;
  public UILabel kingdomValue;
  public UILabel group;
  public UILabel groupValue;
  public UILabel motto;
  public UILabel mottoValue;
  public UILabel power;
  public UILabel powerValue;
  public UILabel kill;
  public UILabel killValue;
}
