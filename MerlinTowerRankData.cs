﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerRankData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class MerlinTowerRankData : MerlinTrialsPayload.RankData
{
  public MerlinTowerRankData(Hashtable data)
    : base(data)
  {
  }

  protected override void ParseOther(Hashtable data)
  {
    if (data == null)
      return;
    DatabaseTools.UpdateData(data, "crystal", ref this.number);
    DatabaseTools.UpdateData(data, "capacity", ref this.lostedNumber);
  }
}
