﻿// Decompiled with JetBrains decompiler
// Type: EquipmentComponentData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class EquipmentComponentData : IComponentData
{
  public ConfigEquipmentMainInfo equipmentInfo;
  public ItemStaticInfo itemInfo;
  public int enhanceLevel;
  public ConfigEquipmentPropertyInfo equipmentPropertyInfo;
  public BagType bagType;
  public long itemId;

  public EquipmentComponentData(int equipmentID, int enhanceLevel, BagType type, long itemId = 0)
  {
    this.bagType = type;
    this.equipmentInfo = ConfigManager.inst.GetEquipmentMain(type).GetData(equipmentID);
    this.itemInfo = ConfigManager.inst.DB_Items.GetItem(this.equipmentInfo.itemID);
    this.itemId = itemId;
    this.enhanceLevel = enhanceLevel;
    this.equipmentPropertyInfo = ConfigManager.inst.GetEquipmentProperty(type).GetDataByEquipIDAndEnhanceLevel(equipmentID, enhanceLevel);
  }
}
