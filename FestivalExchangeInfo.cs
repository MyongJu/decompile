﻿// Decompiled with JetBrains decompiler
// Type: FestivalExchangeInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class FestivalExchangeInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "item_cost_id")]
  public int itemCostId;
  [Config(Name = "item_cost_rate")]
  public int itemCostRate;
  [Config(Name = "free_refresh")]
  public int freeRefresh;
  [Config(Name = "refresh_cost_1")]
  public int refreshCost0;
  [Config(Name = "refresh_cost_2")]
  public int refreshCost1;
  [Config(Name = "refresh_cost_3")]
  public int refreshCost2;
  [Config(Name = "refresh_cost_4")]
  public int refreshCost3;
  [Config(Name = "refresh_cost_5")]
  public int refreshCost4;
  [Config(Name = "refresh_cost_6")]
  public int refreshCost5;
  [Config(Name = "refresh_cost_7")]
  public int refreshCost6;
  [Config(Name = "refresh_cost_8")]
  public int refreshCost7;
  [Config(Name = "refresh_cost_9")]
  public int refreshCost8;
  [Config(Name = "refresh_cost_10")]
  public int refreshCost9;
  public int[] refreshCosts;
  [Config(Name = "refresh_cd")]
  public int refreshCd;
  [Config(Name = "reward_slot_number")]
  public int rewardSlotNumber;

  public int GetCostByRefreshCount(int count)
  {
    if (count < this.freeRefresh || this.refreshCosts == null)
      return 0;
    if (count < this.freeRefresh + this.refreshCosts.Length)
      return this.refreshCosts[count - this.freeRefresh];
    return this.refreshCost9;
  }
}
