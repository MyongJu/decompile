﻿// Decompiled with JetBrains decompiler
// Type: SetColorOnSelection
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("NGUI/Examples/Set Color on Selection")]
[ExecuteInEditMode]
[RequireComponent(typeof (UIWidget))]
public class SetColorOnSelection : MonoBehaviour
{
  private UIWidget mWidget;

  public void SetSpriteBySelection()
  {
    if ((Object) UIPopupList.current == (Object) null)
      return;
    if ((Object) this.mWidget == (Object) null)
      this.mWidget = this.GetComponent<UIWidget>();
    string key = UIPopupList.current.value;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (SetColorOnSelection.\u003C\u003Ef__switch\u0024map7 == null)
    {
      // ISSUE: reference to a compiler-generated field
      SetColorOnSelection.\u003C\u003Ef__switch\u0024map7 = new Dictionary<string, int>(7)
      {
        {
          "White",
          0
        },
        {
          "Red",
          1
        },
        {
          "Green",
          2
        },
        {
          "Blue",
          3
        },
        {
          "Yellow",
          4
        },
        {
          "Cyan",
          5
        },
        {
          "Magenta",
          6
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!SetColorOnSelection.\u003C\u003Ef__switch\u0024map7.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.mWidget.color = Color.white;
        break;
      case 1:
        this.mWidget.color = Color.red;
        break;
      case 2:
        this.mWidget.color = Color.green;
        break;
      case 3:
        this.mWidget.color = Color.blue;
        break;
      case 4:
        this.mWidget.color = Color.yellow;
        break;
      case 5:
        this.mWidget.color = Color.cyan;
        break;
      case 6:
        this.mWidget.color = Color.magenta;
        break;
    }
  }
}
