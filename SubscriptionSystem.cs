﻿// Decompiled with JetBrains decompiler
// Type: SubscriptionSystem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class SubscriptionSystem
{
  private string curPackage = string.Empty;
  private const string subscribeCircleMenuIcon = "icon_subscribe";
  private const string subscribeCircleMenuTextKey = "id_subscription";
  private const string LAST_REWARD_TIME = "last_award_time";
  private const string EXPIRATION_TIME = "expiration_date";
  private const string UID = "uid";
  private const string PACKAGE = "package_name";
  private const string IS_SUBSCRIBED = "is_subscribed";
  private const string collectIcon = "";
  private bool openSubscrition;
  private bool isActive;
  private int hasStarted;
  private double last_award_time;
  private double expirte_time;
  private Coroutine _coroutine;
  private static SubscriptionSystem m_Inst;

  public event System.Action subscriptionSuccessEvent;

  public static SubscriptionSystem Instance
  {
    get
    {
      if (SubscriptionSystem.m_Inst == null)
      {
        SubscriptionSystem.m_Inst = new SubscriptionSystem();
        SubscriptionSystem.m_Inst.Init();
      }
      return SubscriptionSystem.m_Inst;
    }
  }

  public bool OpenSubscribe
  {
    get
    {
      return this.openSubscrition;
    }
    set
    {
      this.openSubscrition = value;
    }
  }

  private void Init()
  {
  }

  private void OnReceiveData(bool ret, object data)
  {
    if (!ret)
      return;
    Hashtable inData = data as Hashtable;
    bool flag = false | DatabaseTools.UpdateData(inData, "last_award_time", ref this.last_award_time) | DatabaseTools.UpdateData(inData, "expiration_date", ref this.expirte_time) | DatabaseTools.UpdateData(inData, "is_subscribed", ref this.hasStarted) | DatabaseTools.UpdateData(inData, "package_name", ref this.curPackage);
    Debug.Log((object) string.Format("{0} request data success,isactive {1}", (object) Time.time, (object) this.IsActive()));
    SubscriptionEvents.updateEvent.Invoke();
    if (this.subscriptionSuccessEvent == null)
      return;
    this.subscriptionSuccessEvent();
  }

  public void RequestSubscribeDataNonBlock()
  {
    MessageHub.inst.GetPortByAction("player:getSubscriptionState").SendLoader((Hashtable) null, new System.Action<bool, object>(this.OnReceiveData), true, false);
  }

  public void Start()
  {
    UIManager.inst.OpenPopup("SubscibePopup", (Popup.PopupParameter) null);
    this.RequestSubscribeDataNonBlock();
  }

  public string CircleMenuIcon
  {
    get
    {
      return "icon_subscribe";
    }
  }

  public string CircleMenuTextKey
  {
    get
    {
      return "id_subscription";
    }
  }

  public bool HasHighLevelQuickOpen()
  {
    return this.CanCollect();
  }

  public string GetHighLevelQuickOpenIcon()
  {
    if (this.CanCollect())
      return string.Empty;
    return string.Empty;
  }

  public bool IsActive()
  {
    return NetServerTime.inst.UpdateTime < this.expirte_time;
  }

  public bool CanCollect()
  {
    bool flag1 = this.IsActive();
    bool flag2 = NetServerTime.inst.IsToday(this.last_award_time);
    if (flag1)
      return !flag2;
    return false;
  }

  public bool HasStarted()
  {
    return this.hasStarted != 0;
  }

  public int RemainDays()
  {
    int num = (int) (this.expirte_time - NetServerTime.inst.UpdateTime) / 86400;
    if (num < 0)
      num = 0;
    return num;
  }

  public void activateSubscription()
  {
    SubscriptionInfo firstInfo = ConfigManager.inst.DB_Subscription.GetFirstInfo();
    this.StartBuy();
    PaymentManager.Instance.BlockBuyProduct(this.GetProductID(firstInfo.pay_id), string.Empty, string.Empty, (System.Action<bool>) (ret =>
    {
      if (ret)
        return;
      this.ClearBuy();
    }), (System.Action) null);
  }

  public void cancelSubscription()
  {
    this.isActive = false;
    SubscriptionEvents.updateEvent.Invoke();
  }

  public void collectSubscription()
  {
    if (!this.CanCollect())
      return;
    UIManager.inst.ShowManualBlocker();
    MessageHub.inst.GetPortByAction("player:subscriptionRewards").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      UIManager.inst.HideManualBlocker();
      if (!ret)
        return;
      this.last_award_time = NetServerTime.inst.UpdateTime;
      SubscriptionEvents.updateEvent.Invoke();
      this.RequestSubscribeDataNonBlock();
      SubscriptionInfo subInfo = this.GetSubInfo();
      List<IconData> iconDataList = new List<IconData>();
      using (Dictionary<long, int>.Enumerator enumerator = subInfo.rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, int> current = enumerator.Current;
          int num = current.Value;
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem((int) current.Key);
          RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
          {
            icon = itemStaticInfo.ImagePath,
            count = (float) num
          });
        }
      }
      RewardsCollectionAnimator.Instance.CollectItems(false);
      UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
    }), false);
  }

  public void resumeSubscription()
  {
    this.activateSubscription();
  }

  public string GetProductID(string payId)
  {
    string str = payId;
    IAPPlatformInfo iapPlatformInfo = ConfigManager.inst.DB_IAPPlatform.Get(Application.platform, payId);
    if (iapPlatformInfo != null)
      str = iapPlatformInfo.product_id;
    return str;
  }

  public SubscriptionInfo GetSubInfo()
  {
    SubscriptionInfo subscriptionInfo = (SubscriptionInfo) null;
    if (this.curPackage != null && this.curPackage != string.Empty)
      subscriptionInfo = ConfigManager.inst.DB_Subscription.GetByPayId(this.curPackage);
    if (subscriptionInfo == null)
      subscriptionInfo = ConfigManager.inst.DB_Subscription.GetFirstInfo();
    return subscriptionInfo;
  }

  private void OnPushPaymentSuccess(object data)
  {
    this.RequestSubscribeDataNonBlock();
    this.ClearBuy();
  }

  private void StopCoroutine()
  {
    if (this._coroutine == null)
      return;
    Utils.StopCoroutine(this._coroutine);
    this._coroutine = (Coroutine) null;
  }

  private void OnTimeOut()
  {
    this.RequestSubscribeDataNonBlock();
    this.ClearBuy();
  }

  private void StartBuy()
  {
    this.StopCoroutine();
    this._coroutine = Utils.ExecuteInSecs(60f, new System.Action(this.OnTimeOut));
    UIManager.inst.ShowManualBlocker();
    MessageHub.inst.GetPortByAction("payment_success").AddEvent(new System.Action<object>(this.OnPushPaymentSuccess));
  }

  private void ClearBuy()
  {
    this.StopCoroutine();
    MessageHub.inst.GetPortByAction("payment_success").RemoveEvent(new System.Action<object>(this.OnPushPaymentSuccess));
    UIManager.inst.HideManualBlocker();
  }
}
