﻿// Decompiled with JetBrains decompiler
// Type: ReinforceAllocPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UI;
using UnityEngine;

public class ReinforceAllocPopup : MonoBehaviour
{
  private ReinforceAllocPopup.Data data = new ReinforceAllocPopup.Data();
  private bool isClose;
  private int leftReinforceCount;
  private int totalCapacity;
  [SerializeField]
  private ReinforceAllocPopup.Panel panel;

  public void OnEnable()
  {
    this.isClose = false;
  }

  public void OnDisable()
  {
    this.isClose = true;
  }

  public void Show(ReinforceAllocPopup.Data inData)
  {
    this.gameObject.SetActive(true);
    this.data.Copy(inData);
    TileData tileData = !MapUtils.IsPitWorld(this.data.targetLocation.K) ? PVPMapData.MapData.GetReferenceAt(this.data.targetLocation) : PitMapData.MapData.GetReferenceAt(this.data.targetLocation);
    if (tileData != null)
    {
      this.data.tileOwnerId = tileData.OwnerID;
      if (WonderUtils.IsWonder(this.data.targetLocation))
      {
        this._freshData(tileData.WonderData.CurrentTroopsCount, tileData.WonderData.MaxTroopsCount);
        return;
      }
      if (WonderUtils.IsWonderTower(this.data.targetLocation))
      {
        this._freshData(tileData.WonderTowerData.CurrentTroopsCount, tileData.WonderTowerData.MaxTroopsCount);
        return;
      }
    }
    else
      this.data.tileOwnerId = 0L;
    Hashtable postData = new Hashtable();
    postData[(object) "uid"] = (object) PlayerData.inst.uid;
    postData[(object) "opp_uid"] = (object) (this.data.targetUid != 0L ? this.data.targetUid : this.data.tileOwnerId);
    MessageHub.inst.GetPortByAction("PVP:getReinforceList").SendRequest(postData, new System.Action<bool, object>(this.OnReinforceDataFresh), true);
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  public void OnReinforceDataFresh(bool result, object orgData)
  {
    if (this.isClose)
      return;
    long totalTroop = 0;
    if (!result)
      return;
    Hashtable hashtable1 = orgData as Hashtable;
    if (hashtable1.ContainsKey((object) "capacity_info"))
    {
      Hashtable hashtable2 = hashtable1[(object) "capacity_info"] as Hashtable;
      if (hashtable2 != null)
      {
        if (hashtable2.ContainsKey((object) "troops_amount"))
          totalTroop = (long) int.Parse(hashtable2[(object) "troops_amount"].ToString());
        if (hashtable2.ContainsKey((object) "capacity"))
          this.totalCapacity = int.Parse(hashtable2[(object) "capacity"].ToString());
      }
    }
    this._freshData(totalTroop, (long) this.totalCapacity);
  }

  private void _freshData(long totalTroop, long totalCapacity)
  {
    if (totalCapacity > 0L)
    {
      this.leftReinforceCount = (int) (totalCapacity - totalTroop);
      if (this.leftReinforceCount > 0)
      {
        this.panel.reinforceContainer.gameObject.SetActive(true);
        this.panel.noEmbassyWarning.gameObject.SetActive(false);
        this.panel.BT_reinforce.isEnabled = true;
        this.panel.reinforceCount.text = string.Format("{0}/{1}", (object) Utils.ConvertNumberToNormalString(totalTroop), (object) Utils.ConvertNumberToNormalString(totalCapacity));
        this.panel.reinforceProgress.value = (float) totalTroop / (float) totalCapacity;
        if (this.leftReinforceCount != 0)
          return;
        UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
        {
          title = Utils.XLAT("id_uppercase_warning"),
          description = "No space left *",
          buttonClickEvent = (System.Action) (() =>
          {
            UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
            UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
          }),
          closeButtonCallbackEvent = (System.Action) (() =>
          {
            UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
            UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
          })
        });
      }
      else
      {
        this.panel.reinforceContainer.gameObject.SetActive(false);
        this.panel.noEmbassyWarning.gameObject.SetActive(true);
        this.panel.noEmbassyWarning.text = Utils.XLAT("embassy_reinforcement_capacity_full");
        this.panel.BT_reinforce.isEnabled = false;
      }
    }
    else
    {
      this.panel.reinforceContainer.gameObject.SetActive(false);
      this.panel.noEmbassyWarning.gameObject.SetActive(true);
      this.panel.noEmbassyWarning.text = Utils.XLAT("embassy_target_no_embassy");
      this.panel.BT_reinforce.isEnabled = false;
    }
  }

  public void OnCancelReinforceButtonClick()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnReinforceButtonClick()
  {
    this.Hide();
    if (this.data.OnPopupCallback == null)
      return;
    this.data.OnPopupCallback(this.leftReinforceCount);
  }

  public void Reset()
  {
    this.panel.reinforceContainer = this.transform.Find("ReinforceContainer").gameObject.transform;
    this.panel.reinforceProgress = (UIProgressBar) this.transform.Find("ReinforceContainer/ProcessBar").gameObject.GetComponent<UISlider>();
    this.panel.reinforceCount = this.transform.Find("ReinforceContainer/ProcessBar/RatioValue").gameObject.GetComponent<UILabel>();
    this.panel.noEmbassyWarning = this.transform.Find("NoEmbassyWarning").gameObject.GetComponent<UILabel>();
    this.panel.BT_cancel = this.transform.Find("CancelBtn").gameObject.GetComponent<UIButton>();
    this.panel.BT_cancel.onClick.Clear();
    this.panel.BT_cancel.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnCancelReinforceButtonClick)));
    this.panel.BT_reinforce = this.transform.Find("ReinforceBtn").gameObject.GetComponent<UIButton>();
    this.panel.BT_reinforce.onClick.Clear();
    this.panel.BT_reinforce.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnReinforceButtonClick)));
  }

  public class Data
  {
    public long targetCityId = -1;
    public long targetUid;
    public long tileOwnerId;
    public Coordinate targetLocation;
    public System.Action<int> OnPopupCallback;

    public void Copy(ReinforceAllocPopup.Data orgData)
    {
      this.targetUid = orgData.targetUid;
      this.tileOwnerId = orgData.tileOwnerId;
      this.targetCityId = orgData.targetCityId;
      this.targetLocation = orgData.targetLocation;
      this.OnPopupCallback = orgData.OnPopupCallback;
    }
  }

  [Serializable]
  public class Panel
  {
    public Transform reinforceContainer;
    public UIProgressBar reinforceProgress;
    public UILabel reinforceCount;
    public UILabel noEmbassyWarning;
    public UIButton BT_cancel;
    public UIButton BT_reinforce;
  }
}
