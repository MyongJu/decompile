﻿// Decompiled with JetBrains decompiler
// Type: BoostItemData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class BoostItemData
{
  public string title;
  public string description;
  public float basicEffect;
  public float tempEffect;
  public int leftTime;
  public float startTime;

  public static BoostItemData CreateFromInfo(BoostItemData.Type type, BenefitItem itemInfo)
  {
    BoostItemData boostItemData = new BoostItemData();
    boostItemData.title = BoostsConst.ItemType2Title[type];
    boostItemData.description = string.Empty;
    boostItemData.leftTime = -1;
    if (type == BoostItemData.Type.Item && itemInfo.arg > 0L)
    {
      JobHandle job = JobManager.Instance.GetJob(itemInfo.arg);
      boostItemData.leftTime = job != null ? job.LeftTime() : 0;
      boostItemData.startTime = Time.time;
    }
    boostItemData.basicEffect = itemInfo.value;
    boostItemData.tempEffect = 0.0f;
    return boostItemData;
  }

  public static BoostItemData CreateRandom()
  {
    return new BoostItemData()
    {
      title = Random.Range(1f, float.MaxValue).ToString(),
      description = Random.Range(1f, float.MaxValue).ToString(),
      leftTime = (int) Random.Range(1f, float.MaxValue),
      basicEffect = Random.Range(1f, float.MaxValue),
      tempEffect = Random.Range(1f, float.MaxValue)
    };
  }

  public enum Type
  {
    Hero,
    Research,
    Item,
  }
}
