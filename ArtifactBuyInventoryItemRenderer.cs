﻿// Decompiled with JetBrains decompiler
// Type: ArtifactBuyInventoryItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class ArtifactBuyInventoryItemRenderer : MonoBehaviour
{
  public UITexture artifactFrame;
  public UITexture artifactTexture;
  public GameObject artifactSelectedNode;
  public GameObject artifactEquipStatusNode;
  public System.Action<int> onArtifactPressed;
  public System.Action<int> onArtifactFinished;
  private int _purchasedIndex;
  private ArtifactLimitedData _artifactLimitedData;
  private int _defaultHighlightIndex;

  public ArtifactLimitedData CurrentArtifactLimitedData
  {
    get
    {
      return this._artifactLimitedData;
    }
  }

  public void SetData(int purchasedIndex, ArtifactLimitedData artifactLimitedData, int highlightIndex = -1)
  {
    this._purchasedIndex = purchasedIndex;
    this._artifactLimitedData = artifactLimitedData;
    this._defaultHighlightIndex = highlightIndex;
    this.UpdateUI();
  }

  public void RefreshUI(bool forceRefresh = false, int highlightIndex = -1)
  {
    NGUITools.SetActive(this.artifactSelectedNode, false);
    this._defaultHighlightIndex = highlightIndex;
    if (!forceRefresh)
      return;
    this._artifactLimitedData = PlayerData.inst.playerCityData.GetArtifactLimitedDataByPurchasedId(this._purchasedIndex);
    this.UpdateUI();
  }

  public void OnArtifactPressed()
  {
    NGUITools.SetActive(this.artifactSelectedNode, true);
    if (this.onArtifactPressed != null)
      this.onArtifactPressed(this._purchasedIndex);
    this._defaultHighlightIndex = -1;
  }

  private void UpdateUI()
  {
    NGUITools.SetActive(this.artifactSelectedNode, false);
    NGUITools.SetActive(this.artifactEquipStatusNode, false);
    if (this._artifactLimitedData == null)
      return;
    ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this._artifactLimitedData.ArtifactId);
    if (artifactInfo != null)
    {
      if ((UnityEngine.Object) this.artifactFrame.mainTexture == (UnityEngine.Object) null || this.artifactFrame.mainTexture.name != artifactInfo.TimeLimitedArtifactFrameIcon)
        BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactFrame, artifactInfo.TimeLimitedArtifactFramePath, (System.Action<bool>) null, true, false, string.Empty);
      if ((UnityEngine.Object) this.artifactTexture.mainTexture == (UnityEngine.Object) null || this.artifactTexture.mainTexture.name != artifactInfo.icon)
        BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactTexture, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    }
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(PlayerData.inst.uid);
    if (heroData != null && heroData.IsArtifactLimitedEquiped(this._purchasedIndex))
    {
      NGUITools.SetActive(this.artifactEquipStatusNode, true);
      NGUITools.SetActive(this.artifactSelectedNode, this._defaultHighlightIndex < 0 || this._defaultHighlightIndex == this._purchasedIndex);
    }
    else
      NGUITools.SetActive(this.artifactSelectedNode, this._defaultHighlightIndex == this._purchasedIndex);
  }
}
