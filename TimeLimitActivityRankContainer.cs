﻿// Decompiled with JetBrains decompiler
// Type: TimeLimitActivityRankContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class TimeLimitActivityRankContainer : MonoBehaviour
{
  private List<TimeLimitActivityRankItem> items = new List<TimeLimitActivityRankItem>();
  public TimeLimitActivityRankItem itemPrefab;
  public UILabel titleLabel;
  public UILabel timeLabel;
  public UIGrid grid;
  public UIScrollView scrollView;

  public void SetData(int time, string title, bool isNormal = true)
  {
    List<ActivityRankData> currentRankData = ActivityManager.Intance.GetCurrentRankData(time, isNormal, false);
    for (int index = 0; index < currentRankData.Count; ++index)
    {
      GameObject go = NGUITools.AddChild(this.grid.gameObject, this.itemPrefab.gameObject);
      NGUITools.SetActive(go, true);
      TimeLimitActivityRankItem component = go.GetComponent<TimeLimitActivityRankItem>();
      component.Normal = isNormal;
      component.SetData(currentRankData[index]);
      this.items.Add(component);
    }
    this.titleLabel.text = title;
    this.timeLabel.text = isNormal ? string.Empty : Utils.FormatTimeYYYYMMDD((long) time, "/");
    this.grid.Reposition();
  }

  private List<ActivityRankData> CreateFakeData()
  {
    List<ActivityRankData> activityRankDataList = new List<ActivityRankData>();
    for (int index = 0; index < 10; ++index)
      activityRankDataList.Add(new ActivityRankData()
      {
        rank = index + 1,
        playerName = "Test",
        acronym = "sss"
      });
    return activityRankDataList;
  }

  public void Clear()
  {
    for (int index = 0; index < this.items.Count; ++index)
      this.items[index].Clear();
    this.items.Clear();
  }
}
