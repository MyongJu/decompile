﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarActivityData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;

public class AllianceWarActivityData : ActivityBaseData
{
  private long firstStartTime;
  private long registerStartTime;
  private long registerEndTime;
  private long matchStartTime;
  private long matchEndTime;
  private long fightStartTime;
  private long fightEndTime;
  private long interval;
  private int allianceRank;
  private int registerCDTime;
  private List<AllianceGroupData> groupDataList;
  private List<AllianceGroupData> historyGroupDataList;
  private bool isRegistered;
  private bool isActivityOff;

  public override ActivityBaseData.ActivityType Type
  {
    get
    {
      return ActivityBaseData.ActivityType.AllianceWar;
    }
  }

  public int FirstStartTime
  {
    get
    {
      return (int) this.firstStartTime;
    }
  }

  public int RegisterStartTime
  {
    get
    {
      return (int) this.registerStartTime;
    }
  }

  public int RegisterEndTime
  {
    get
    {
      return (int) this.registerEndTime;
    }
  }

  public int MatchStartTime
  {
    get
    {
      return (int) this.matchStartTime;
    }
  }

  public int MatchEndTime
  {
    get
    {
      return (int) this.matchEndTime;
    }
  }

  public int FightStartTime
  {
    set
    {
      this.fightStartTime = (long) value;
    }
    get
    {
      return (int) this.fightStartTime;
    }
  }

  public int FightEndTime
  {
    set
    {
      this.fightEndTime = (long) value;
    }
    get
    {
      return (int) this.fightEndTime;
    }
  }

  public int Interval
  {
    get
    {
      return (int) this.interval;
    }
  }

  public int AllianceRank
  {
    get
    {
      return this.allianceRank;
    }
  }

  public int RegisterCDTime
  {
    get
    {
      return this.registerCDTime;
    }
    set
    {
      this.registerCDTime = value;
    }
  }

  public List<AllianceGroupData> GroupDataList
  {
    get
    {
      return this.groupDataList;
    }
  }

  public List<AllianceGroupData> HistoryGroupDataList
  {
    get
    {
      return this.historyGroupDataList;
    }
  }

  public bool IsRegistered
  {
    get
    {
      return this.isRegistered;
    }
  }

  public bool IsActivityOff
  {
    get
    {
      return this.isActivityOff;
    }
    set
    {
      this.isActivityOff = value;
    }
  }

  public AllianceWarActivityData.AllianceWarState GetAllianceWarState()
  {
    if ((long) NetServerTime.inst.ServerTimestamp < this.registerStartTime)
      return this.historyGroupDataList == null ? AllianceWarActivityData.AllianceWarState.NotOpen : AllianceWarActivityData.AllianceWarState.Completed;
    if ((long) NetServerTime.inst.ServerTimestamp >= this.registerStartTime && (long) NetServerTime.inst.ServerTimestamp <= this.registerEndTime)
      return AllianceWarActivityData.AllianceWarState.Registering;
    if ((long) NetServerTime.inst.ServerTimestamp >= this.matchStartTime && (long) NetServerTime.inst.ServerTimestamp <= this.matchEndTime)
    {
      if (!this.isRegistered)
        return AllianceWarActivityData.AllianceWarState.RegisterFail;
      return this.groupDataList == null ? AllianceWarActivityData.AllianceWarState.Matching : AllianceWarActivityData.AllianceWarState.Preparing;
    }
    if ((long) NetServerTime.inst.ServerTimestamp >= this.fightStartTime && (long) NetServerTime.inst.ServerTimestamp <= this.fightEndTime)
      return this.isRegistered ? AllianceWarActivityData.AllianceWarState.Opened : AllianceWarActivityData.AllianceWarState.RegisterFail;
    if ((long) NetServerTime.inst.ServerTimestamp <= this.fightEndTime || (long) NetServerTime.inst.ServerTimestamp > this.fightEndTime + this.interval)
      return AllianceWarActivityData.AllianceWarState.None;
    return this.isRegistered ? AllianceWarActivityData.AllianceWarState.Completed : AllianceWarActivityData.AllianceWarState.NotOpen;
  }

  public override bool IsEmpty()
  {
    return this.isActivityOff;
  }

  public override void Decode(Hashtable source)
  {
    if (source == null)
    {
      this.isActivityOff = true;
    }
    else
    {
      this.isActivityOff = false;
      string empty = string.Empty;
      if (source.ContainsKey((object) "first_start_time") && source[(object) "first_start_time"] != null)
        long.TryParse(source[(object) "first_start_time"].ToString(), out this.firstStartTime);
      if (source.ContainsKey((object) "register_start_time") && source[(object) "register_start_time"] != null)
        long.TryParse(source[(object) "register_start_time"].ToString(), out this.registerStartTime);
      if (source.ContainsKey((object) "register_end_time") && source[(object) "register_end_time"] != null)
        long.TryParse(source[(object) "register_end_time"].ToString(), out this.registerEndTime);
      if (source.ContainsKey((object) "grouping_start_time") && source[(object) "grouping_start_time"] != null)
        long.TryParse(source[(object) "grouping_start_time"].ToString(), out this.matchStartTime);
      if (source.ContainsKey((object) "grouping_end_time") && source[(object) "grouping_end_time"] != null)
        long.TryParse(source[(object) "grouping_end_time"].ToString(), out this.matchEndTime);
      if (source.ContainsKey((object) "fight_start_time") && source[(object) "fight_start_time"] != null)
        long.TryParse(source[(object) "fight_start_time"].ToString(), out this.fightStartTime);
      if (source.ContainsKey((object) "fight_end_time") && source[(object) "fight_end_time"] != null)
        long.TryParse(source[(object) "fight_end_time"].ToString(), out this.fightEndTime);
      if (source.ContainsKey((object) "interval") && source[(object) "interval"] != null)
        long.TryParse(source[(object) "interval"].ToString(), out this.interval);
      if (source.ContainsKey((object) "alliance_rank") && source[(object) "alliance_rank"] != null)
        int.TryParse(source[(object) "alliance_rank"].ToString(), out this.allianceRank);
      if (source.ContainsKey((object) "register_cd_time") && source[(object) "register_cd_time"] != null)
        int.TryParse(source[(object) "register_cd_time"].ToString(), out this.registerCDTime);
      this.groupDataList = !source.ContainsKey((object) "current_rank") || source[(object) "current_rank"] == null ? (List<AllianceGroupData>) null : JsonReader.Deserialize<List<AllianceGroupData>>(Utils.Object2Json(source[(object) "current_rank"]));
      this.historyGroupDataList = !source.ContainsKey((object) "history_rank") || source[(object) "history_rank"] == null ? (List<AllianceGroupData>) null : JsonReader.Deserialize<List<AllianceGroupData>>(Utils.Object2Json(source[(object) "history_rank"]));
      if (!source.ContainsKey((object) "is_registered") || source[(object) "is_registered"] == null)
        return;
      if (source[(object) "is_registered"].ToString().Trim().ToLower() == "true")
        this.isRegistered = true;
      else
        this.isRegistered = false;
    }
  }

  public enum AllianceWarState
  {
    None,
    NotOpen,
    Registering,
    RegisterFail,
    Matching,
    Preparing,
    Opened,
    Completed,
  }
}
