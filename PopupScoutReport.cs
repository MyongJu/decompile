﻿// Decompiled with JetBrains decompiler
// Type: PopupScoutReport
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;
using UnityEngine;

public class PopupScoutReport : UI.Dialog
{
  public PopupScoutDetails detailsPanel;
  public UILabel playerName;
  public UILabel playerPower;
  public UILabel kingdomName;
  public UILabel allianceName;
  public UILabel coordinateK;
  public UILabel coordinateX;
  public UILabel coordinateY;
  public UILabel oreCount;
  public UILabel silverCount;
  public UILabel foodCount;
  public UILabel woodCount;
  public UILabel heroName;
  public UILabel heroLevel;
  public UILabel heroXP;
  public UILabel defendingTroops;
  public UILabel wallTraps;
  public UILabel reinforcingTroops;
  public UILabel warRalliedTroops;
  public UIButton detailsButton;
  public GameObject resourceTable;
  public GameObject lockedResourceMessage;
  public GameObject heroTable;
  public GameObject lockedHeroMessage;
  public GameObject troopTable;
  public GameObject lockedTroopMessage;
  public GameObject allianceData;
  protected Hashtable _dataHT;

  public Hashtable DataHT
  {
    set
    {
      if (this._dataHT == value)
        return;
      this._dataHT = value;
      if (this._dataHT == null)
        return;
      if ((Object) this.playerName != (Object) null)
        this.playerName.text = this._dataHT[(object) "user_name"] as string;
      if ((Object) this.playerPower != (Object) null)
        this.playerPower.text = this._dataHT[(object) "power"] as string;
      Hashtable hashtable1 = this._dataHT[(object) "alliance_id"] as Hashtable;
      if (hashtable1 != null)
      {
        if ((Object) this.allianceName != (Object) null)
          this.allianceName.text = hashtable1[(object) "name"] as string;
        this.allianceData.SetActive(true);
      }
      else
        this.allianceData.SetActive(false);
      int num1;
      int num2 = num1 = 0;
      if ((Object) this.coordinateK != (Object) null)
        this.coordinateK.text = int.Parse(this._dataHT[(object) "k"] as string).ToString();
      if ((Object) this.coordinateX != (Object) null)
        this.coordinateX.text = int.Parse(this._dataHT[(object) "x"] as string).ToString();
      if ((Object) this.coordinateY != (Object) null)
        this.coordinateY.text = int.Parse(this._dataHT[(object) "y"] as string).ToString();
      if ((Object) this.kingdomName != (Object) null)
        this.kingdomName.text = this._dataHT[(object) "city_name"] as string;
      Hashtable hashtable2 = this._dataHT[(object) "resources"] as Hashtable;
      if (hashtable2 != null)
      {
        this.resourceTable.SetActive(true);
        this.lockedResourceMessage.SetActive(false);
        if ((Object) this.oreCount != (Object) null)
          this.oreCount.text = hashtable2[(object) "ore"] as string;
        if ((Object) this.silverCount != (Object) null)
          this.silverCount.text = hashtable2[(object) "silver"] as string;
        if ((Object) this.foodCount != (Object) null)
          this.foodCount.text = hashtable2[(object) "food"] as string;
        if ((Object) this.woodCount != (Object) null)
          this.woodCount.text = hashtable2[(object) "wood"] as string;
      }
      else
      {
        this.resourceTable.SetActive(false);
        this.lockedResourceMessage.SetActive(true);
      }
      Hashtable hashtable3 = this._dataHT[(object) "hero"] as Hashtable;
      if (hashtable3 != null)
      {
        this.heroTable.SetActive(true);
        this.lockedHeroMessage.SetActive(false);
        if ((Object) this.heroName != (Object) null)
          this.heroName.text = hashtable3[(object) "name"] as string;
        if ((Object) this.heroLevel != (Object) null)
          this.heroLevel.text = hashtable3[(object) "level"] as string;
        if ((Object) this.heroXP != (Object) null)
          this.heroXP.text = hashtable3[(object) "xp"] as string;
      }
      else
      {
        this.heroTable.SetActive(false);
        this.lockedHeroMessage.SetActive(true);
      }
      Hashtable hashtable4 = this._dataHT[(object) "troops"] as Hashtable;
      if (hashtable4 != null)
      {
        this.troopTable.SetActive(true);
        this.lockedTroopMessage.SetActive(false);
        int num3 = 0;
        Hashtable hashtable5 = hashtable4[(object) "defending_troops"] as Hashtable;
        if (hashtable5 != null)
        {
          if (hashtable5.ContainsKey((object) "amount"))
          {
            num3 = int.Parse(hashtable5[(object) "amount"] as string);
          }
          else
          {
            foreach (int num4 in (IEnumerable) hashtable5.Values)
              num3 += num4;
          }
          this.defendingTroops.text = num3.ToString();
        }
        else
          this.defendingTroops.text = 0.ToString();
        int num5 = 0;
        Hashtable hashtable6 = hashtable4[(object) "traps"] as Hashtable;
        if (hashtable6 != null)
        {
          if (hashtable6.ContainsKey((object) "amount"))
          {
            num5 = int.Parse(hashtable6[(object) "amount"] as string);
          }
          else
          {
            foreach (int num4 in (IEnumerable) hashtable6.Values)
              num5 += num4;
          }
          this.wallTraps.text = num5.ToString();
        }
        else
          this.wallTraps.text = 0.ToString();
        int num6 = 0;
        Hashtable hashtable7 = hashtable4[(object) "reinforcing_troops"] as Hashtable;
        if (hashtable7 != null)
        {
          if (hashtable7.ContainsKey((object) "amount"))
          {
            num6 = int.Parse(hashtable7[(object) "amount"] as string);
          }
          else
          {
            foreach (int num4 in (IEnumerable) hashtable7.Values)
              num6 += num4;
          }
          this.reinforcingTroops.text = num6.ToString();
        }
        else
          this.reinforcingTroops.text = 0.ToString();
        int num7 = 0;
        Hashtable hashtable8 = hashtable4[(object) "war_rallied_troops"] as Hashtable;
        if (hashtable8 != null)
        {
          if (hashtable8.ContainsKey((object) "amount"))
          {
            num7 = int.Parse(hashtable8[(object) "amount"] as string);
          }
          else
          {
            foreach (int num4 in (IEnumerable) hashtable8.Values)
              num7 += num4;
          }
          this.warRalliedTroops.text = num7.ToString();
        }
        else
          this.warRalliedTroops.text = 0.ToString();
        this.detailsPanel.ApplyData(this._dataHT);
        this.detailsButton.isEnabled = true;
      }
      else
      {
        this.troopTable.SetActive(false);
        this.lockedTroopMessage.SetActive(true);
        this.detailsButton.isEnabled = false;
      }
    }
  }

  public void OnDetailsPressed()
  {
    this.detailsPanel.Show();
  }

  public void OnInfoPressed()
  {
    Debug.LogWarning((object) "Info pressed but screen not yet implemented");
  }

  public void OnCoordinatesPressed()
  {
    Debug.LogWarning((object) "Coordinates pressed but screen not yet implemented");
  }

  public void OnClosePressed()
  {
    this.detailsPanel.Hide();
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnBookmarkPressed()
  {
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.DataHT = (orgParam as PopupScoutReport.Parameter).data;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.DataHT = (Hashtable) null;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public Hashtable data;
  }
}
