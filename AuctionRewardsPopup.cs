﻿// Decompiled with JetBrains decompiler
// Type: AuctionRewardsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UI;

public class AuctionRewardsPopup : BaseReportPopup
{
  public RewardItemComponent ric;
  public ItemIconRenderer itemIconRender;

  private void Init()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, true, false, string.Empty);
    Hashtable hashtable = this.param.mail.data[(object) "attachment"] as Hashtable;
    if (hashtable == null)
    {
      this.ric.gameObject.SetActive(false);
      this.itemIconRender.gameObject.SetActive(false);
    }
    else
    {
      this.ric.FeedData((IComponentData) this.AnalyzeCollectedRewards());
      this.itemIconRender.SetData(int.Parse(JsonReader.Deserialize<RewardData>(Utils.Object2Json((object) hashtable)).item.First<KeyValuePair<string, int>>().Key), string.Empty, false);
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
  }
}
