﻿// Decompiled with JetBrains decompiler
// Type: ConfigUnit_Statistics
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigUnit_Statistics
{
  public const string TROOP_TYPE_BASIC = "basic";
  public const string TROOP_TYPE_MONSTER = "monster";
  public Dictionary<int, Unit_StatisticsInfo> datas;
  private Dictionary<string, int> dicByUniqueId;
  private Dictionary<string, List<Unit_StatisticsInfo>> buildingTypeMap;

  public ConfigUnit_Statistics()
  {
    this.datas = new Dictionary<int, Unit_StatisticsInfo>();
    this.dicByUniqueId = new Dictionary<string, int>();
    this.buildingTypeMap = new Dictionary<string, List<Unit_StatisticsInfo>>();
  }

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exsit", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "class");
        int index3 = ConfigManager.GetIndex(inHeader, "troop_class");
        int index4 = ConfigManager.GetIndex(inHeader, "type");
        int index5 = ConfigManager.GetIndex(inHeader, "tier");
        int index6 = ConfigManager.GetIndex(inHeader, "power");
        int index7 = ConfigManager.GetIndex(inHeader, "strength_1");
        int index8 = ConfigManager.GetIndex(inHeader, "strength_2");
        int index9 = ConfigManager.GetIndex(inHeader, "weakness_1");
        int index10 = ConfigManager.GetIndex(inHeader, "weakness_2");
        int index11 = ConfigManager.GetIndex(inHeader, "attack_level");
        int index12 = ConfigManager.GetIndex(inHeader, "defense_level");
        int index13 = ConfigManager.GetIndex(inHeader, "health_level");
        int index14 = ConfigManager.GetIndex(inHeader, "speed_level");
        int index15 = ConfigManager.GetIndex(inHeader, "damage_type");
        int index16 = ConfigManager.GetIndex(inHeader, "image");
        ConfigManager.GetIndex(inHeader, "loc_name_id");
        int index17 = ConfigManager.GetIndex(inHeader, "req_id_1");
        int index18 = ConfigManager.GetIndex(inHeader, "req_id_2");
        int index19 = ConfigManager.GetIndex(inHeader, "req_value_1");
        int index20 = ConfigManager.GetIndex(inHeader, "req_value_2");
        int index21 = ConfigManager.GetIndex(inHeader, "food");
        int index22 = ConfigManager.GetIndex(inHeader, "wood");
        int index23 = ConfigManager.GetIndex(inHeader, "ore");
        int index24 = ConfigManager.GetIndex(inHeader, "silver");
        int index25 = ConfigManager.GetIndex(inHeader, "upkeep");
        int index26 = ConfigManager.GetIndex(inHeader, "training_time");
        int index27 = ConfigManager.GetIndex(inHeader, "healing_food");
        int index28 = ConfigManager.GetIndex(inHeader, "healing_wood");
        int index29 = ConfigManager.GetIndex(inHeader, "healing_silver");
        int index30 = ConfigManager.GetIndex(inHeader, "healing_ore");
        int index31 = ConfigManager.GetIndex(inHeader, "alliance_heal_honor");
        int index32 = ConfigManager.GetIndex(inHeader, "healing_time");
        int index33 = ConfigManager.GetIndex(inHeader, "health");
        int index34 = ConfigManager.GetIndex(inHeader, "attack");
        int index35 = ConfigManager.GetIndex(inHeader, "defense");
        int index36 = ConfigManager.GetIndex(inHeader, "load");
        int index37 = ConfigManager.GetIndex(inHeader, "speed");
        int index38 = ConfigManager.GetIndex(inHeader, "attack_type");
        int index39 = ConfigManager.GetIndex(inHeader, "defense_type");
        int index40 = ConfigManager.GetIndex(inHeader, "building_type");
        int index41 = ConfigManager.GetIndex(inHeader, "priority");
        int index42 = ConfigManager.GetIndex(inHeader, "build_speed");
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          int result = 0;
          if (int.TryParse(key.ToString(), out result))
          {
            ArrayList arr = hashtable[key] as ArrayList;
            if (arr != null)
            {
              Unit_StatisticsInfo data = new Unit_StatisticsInfo(result, ConfigManager.GetString(arr, index1), ConfigManager.GetInt(arr, index2));
              data.Troop_Class_ID = ConfigManager.GetString(arr, index3);
              data.Type = ConfigManager.GetString(arr, index4);
              data.Troop_Tier = ConfigManager.GetInt(arr, index5);
              data.Power = ConfigManager.GetInt(arr, index6);
              data.Strength_1 = ConfigManager.GetString(arr, index7);
              data.Strength_2 = ConfigManager.GetString(arr, index8);
              data.Weakness_1 = ConfigManager.GetString(arr, index9);
              data.Weakness_2 = ConfigManager.GetString(arr, index10);
              data.Attack_Level = ConfigManager.GetInt(arr, index11);
              data.Defense_Level = ConfigManager.GetInt(arr, index12);
              data.Health_Level = ConfigManager.GetInt(arr, index13);
              data.Speed_Level = ConfigManager.GetInt(arr, index14);
              data.Damage_Type = ConfigManager.GetString(arr, index15);
              data.Image = ConfigManager.GetString(arr, index16);
              data.Requirement_ID_1 = ConfigManager.GetString(arr, index17);
              data.Requirement_ID_2 = ConfigManager.GetString(arr, index18);
              data.Requirement_Value_1 = (double) ConfigManager.GetFloat(arr, index19);
              data.Requirement_Value_2 = (double) ConfigManager.GetFloat(arr, index20);
              data.Food = ConfigManager.GetInt(arr, index21);
              data.Wood = ConfigManager.GetInt(arr, index22);
              data.Ore = ConfigManager.GetInt(arr, index23);
              data.Silver = ConfigManager.GetInt(arr, index24);
              data.Upkeep = ConfigManager.GetFloat(arr, index25);
              data.Training_Time = ConfigManager.GetInt(arr, index26);
              data.Heal_Food = ConfigManager.GetInt(arr, index27);
              data.Heal_Wood = ConfigManager.GetInt(arr, index28);
              data.Heal_Silver = ConfigManager.GetInt(arr, index29);
              data.Heal_Ore = ConfigManager.GetInt(arr, index30);
              data.Heal_Honor = ConfigManager.GetInt(arr, index31);
              data.Heal_Time = ConfigManager.GetInt(arr, index32);
              data.Health = ConfigManager.GetInt(arr, index33);
              data.Attack = ConfigManager.GetInt(arr, index34);
              data.Defense = ConfigManager.GetInt(arr, index35);
              data.GatherCapacity = ConfigManager.GetInt(arr, index36);
              data.Speed = ConfigManager.GetFloat(arr, index37);
              data.AttackType = ConfigManager.GetString(arr, index38);
              data.DefenseType = ConfigManager.GetString(arr, index39);
              data.FromBuildingType = ConfigManager.GetString(arr, index40);
              data.Priority = ConfigManager.GetInt(arr, index41);
              data.BuildSpeed = ConfigManager.GetInt(arr, index42);
              this.PushData(data.internalId, data.ID, data);
            }
          }
        }
      }
    }
  }

  public Unit_StatisticsInfo GetData(string className, int tier)
  {
    return this.GetData(string.Format("{0}{1}{2}", (object) className, (object) "_t", (object) tier.ToString()));
  }

  public Dictionary<int, Unit_StatisticsInfo>.ValueCollection Values
  {
    get
    {
      return this.datas.Values;
    }
  }

  private void PushData(int internalId, string uniqueId, Unit_StatisticsInfo data)
  {
    if (this.datas.ContainsKey(internalId))
      return;
    this.datas.Add(internalId, data);
    if (!this.dicByUniqueId.ContainsKey(uniqueId))
      this.dicByUniqueId.Add(uniqueId, internalId);
    if (!this.buildingTypeMap.ContainsKey(data.FromBuildingType))
    {
      this.buildingTypeMap.Add(data.FromBuildingType, new List<Unit_StatisticsInfo>());
      this.buildingTypeMap[data.FromBuildingType].Add(data);
    }
    else if (!this.buildingTypeMap[data.FromBuildingType].Contains(data))
      this.buildingTypeMap[data.FromBuildingType].Add(data);
    ConfigManager.inst.AddData(internalId, (object) data);
  }

  public void Clear()
  {
    if (this.datas != null)
      this.datas.Clear();
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
  }

  public Unit_StatisticsInfo GetData(int internalId)
  {
    if (this.datas.ContainsKey(internalId))
      return this.datas[internalId];
    return (Unit_StatisticsInfo) null;
  }

  public Unit_StatisticsInfo GetData(string uniqueId)
  {
    if (!this.dicByUniqueId.ContainsKey(uniqueId))
      return (Unit_StatisticsInfo) null;
    return this.datas[this.dicByUniqueId[uniqueId]];
  }

  public Unit_StatisticsInfo GetData(PVEResult.TroopType type, int level)
  {
    switch (type)
    {
      case PVEResult.TroopType.infantry:
        return this.GetData(string.Format("{0}_t{1}", (object) "infantry", (object) level));
      case PVEResult.TroopType.ranged:
        return this.GetData(string.Format("{0}_t{1}", (object) "ranged", (object) level));
      case PVEResult.TroopType.cavalry:
        return this.GetData(string.Format("{0}_t{1}", (object) "cavalry", (object) level));
      case PVEResult.TroopType.mage:
        return this.GetData(string.Format("{0}_t{1}", (object) "mage", (object) level));
      case PVEResult.TroopType.siege:
        return this.GetData(string.Format("{0}_t{1}", (object) "siege", (object) level));
      default:
        return (Unit_StatisticsInfo) null;
    }
  }

  public List<Unit_StatisticsInfo> GetAllUnitStaticsticsInfosByBuildingType(string buildingType)
  {
    List<Unit_StatisticsInfo> unitStatisticsInfoList = (List<Unit_StatisticsInfo>) null;
    this.buildingTypeMap.TryGetValue(buildingType, out unitStatisticsInfoList);
    return unitStatisticsInfoList;
  }

  public List<Unit_StatisticsInfo> GetUnitsUnlockedByBuilding(string buildingType, int buildingLevel)
  {
    List<Unit_StatisticsInfo> unitStatisticsInfoList = new List<Unit_StatisticsInfo>();
    List<Unit_StatisticsInfo> infosByBuildingType = this.GetAllUnitStaticsticsInfosByBuildingType(buildingType);
    for (int index = 0; index < infosByBuildingType.Count; ++index)
    {
      if (infosByBuildingType[index].Requirement_Value_1 == (double) buildingLevel)
        unitStatisticsInfoList.Add(infosByBuildingType[index]);
    }
    return unitStatisticsInfoList;
  }
}
