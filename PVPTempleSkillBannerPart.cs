﻿// Decompiled with JetBrains decompiler
// Type: PVPTempleSkillBannerPart
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class PVPTempleSkillBannerPart : MonoBehaviour
{
  private static int BUFF_ICON_TIME = 5;
  protected SpriteCreator m_spriteCreator = new SpriteCreator();
  protected int m_buffIconTime = PVPTempleSkillBannerPart.BUFF_ICON_TIME;
  protected List<AllianceMagicData> m_allPowerUpMagicData = new List<AllianceMagicData>();
  protected List<AllianceMagicData> m_allEffectMagicData = new List<AllianceMagicData>();
  [SerializeField]
  protected UISpriteMeshProgressBar m_leftTimeProgressBar;
  [SerializeField]
  protected TextMesh m_leftTimeText;
  [SerializeField]
  protected SpriteRenderer m_skillIcon;
  [SerializeField]
  protected SpriteRenderer m_buffIcon;
  [SerializeField]
  protected GameObject m_rootSkill;
  [SerializeField]
  protected GameObject m_rootBuff;
  protected TileData m_tileData;
  protected int m_currentBuffIndex;

  protected BetterList<long> TempleSkillEffected
  {
    get
    {
      return this.m_tileData.MagicIds;
    }
  }

  protected AllianceMagicData CurrentPowerUpMagicData
  {
    get
    {
      if (this.m_allPowerUpMagicData.Count <= 0)
        return (AllianceMagicData) null;
      AllianceMagicData allianceMagicData1 = this.m_allPowerUpMagicData[0];
      for (int index = 1; index < this.m_allPowerUpMagicData.Count; ++index)
      {
        AllianceMagicData allianceMagicData2 = this.m_allPowerUpMagicData[index];
        if (allianceMagicData2.TimerEnd < allianceMagicData1.TimerEnd)
          allianceMagicData1 = allianceMagicData2;
      }
      return allianceMagicData1;
    }
  }

  protected void UpdateMagicData()
  {
    this.m_allPowerUpMagicData.Clear();
    this.m_allEffectMagicData.Clear();
    foreach (long magicId in this.m_tileData.MagicIds)
    {
      AllianceMagicData allianceMagicData = DBManager.inst.DB_AllianceMagic.Get(magicId);
      if (allianceMagicData != null && allianceMagicData.CurrentState == "charging")
        this.m_allPowerUpMagicData.Add(allianceMagicData);
      if (allianceMagicData != null && allianceMagicData.CurrentState == "releasing")
        this.m_allEffectMagicData.Add(allianceMagicData);
    }
  }

  private void OnEnable()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent += new System.Action<int>(this.SecondTick);
    this.m_buffIconTime = PVPTempleSkillBannerPart.BUFF_ICON_TIME;
    this.m_currentBuffIndex = 0;
  }

  private void OnDisable()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.SecondTick);
    if ((bool) ((UnityEngine.Object) this.m_skillIcon))
      this.m_skillIcon.sprite = (UnityEngine.Sprite) null;
    this.m_spriteCreator.DestroyAllCreated();
  }

  protected void SecondTick(int delta)
  {
    if (this.m_tileData == null)
      return;
    if (--this.m_buffIconTime <= 0)
    {
      this.m_buffIconTime = PVPTempleSkillBannerPart.BUFF_ICON_TIME;
      if (++this.m_currentBuffIndex >= this.m_allEffectMagicData.Count)
        this.m_currentBuffIndex = 0;
    }
    this.UpdateUI(this.m_tileData);
  }

  public void SetTileData(TileData tile)
  {
    if (tile == this.m_tileData)
      return;
    this.m_tileData = tile;
    this.UpdateUI(this.m_tileData);
  }

  private void UpdateUI(TileData tileData)
  {
    this.UpdateMagicData();
    this.UpdateBuff();
    this.UpdateTempleSkill();
  }

  protected void UpdateTempleSkill()
  {
    bool flag = false;
    if (this.CurrentPowerUpMagicData != null)
    {
      TempleSkillInfo byId = ConfigManager.inst.DB_TempleSkill.GetById(this.CurrentPowerUpMagicData.ConfigId);
      if (byId != null)
      {
        flag = true;
        this.m_skillIcon.sprite = this.m_spriteCreator.CreateSprite(byId.IconPath);
        long num = this.CurrentPowerUpMagicData.TimerEnd - (long) NetServerTime.inst.ServerTimestamp;
        this.m_leftTimeText.text = Utils.FormatTime((int) num, false, false, true);
        this.m_leftTimeProgressBar.SetProgress((float) num / (float) byId.DonationTime);
      }
    }
    this.m_rootSkill.SetActive(flag);
  }

  protected void UpdateBuff()
  {
    bool flag = false;
    if (this.m_currentBuffIndex < this.m_allEffectMagicData.Count)
    {
      TempleSkillInfo byId = ConfigManager.inst.DB_TempleSkill.GetById(this.m_allEffectMagicData[this.m_currentBuffIndex].ConfigId);
      if (byId != null)
      {
        this.m_buffIcon.sprite = this.m_spriteCreator.CreateSprite(byId.IconPath);
        flag = true;
      }
    }
    this.m_rootBuff.SetActive(flag);
  }
}
