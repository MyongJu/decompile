﻿// Decompiled with JetBrains decompiler
// Type: SettingReader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Internal;
using UnityEngine;

public class SettingReader
{
  private static SettingReader _instance;
  public SettingConfigs settings;
  private bool _isReady;
  private AndroidJavaObject _reader;

  public static SettingReader Instance
  {
    get
    {
      if (SettingReader._instance == null)
        SettingReader._instance = new SettingReader();
      return SettingReader._instance;
    }
  }

  public bool IsReady
  {
    get
    {
      return false;
    }
  }

  public void Start()
  {
    this._isReady = false;
  }

  public bool HasDefine(string key)
  {
    if (this.IsReady && !string.IsNullOrEmpty(this.settings.channel))
      return this.settings.channel == key;
    return false;
  }

  private void SetUp()
  {
    if (!this.IsReady)
      return;
    FunplusSettings.FunplusGameId = this.settings.funplusSettings.gameId;
    FunplusSettings.FunplusGameKey = this.settings.funplusSettings.gameKey;
    FunplusSettings.Environment = this.settings.funplusSettings.environment;
  }

  public string GetRawSettingFile(string fileName = "game_settings")
  {
    if (Application.isEditor)
      return string.Empty;
    string empty = string.Empty;
    return this._androideGtRawSettingFile(fileName);
  }

  private string _androideGtRawSettingFile(string fileName)
  {
    this._reader = new AndroidJavaObject("com.funplus.game.settings.SettingReader", new object[0]);
    return this._reader.Call<string>("getRawTextFile", new object[1]
    {
      (object) fileName
    });
  }
}
