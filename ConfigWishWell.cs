﻿// Decompiled with JetBrains decompiler
// Type: ConfigWishWell
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigWishWell
{
  private List<WishWellData> m_WishWellDataList;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<WishWellData>(result as Hashtable, out this.m_WishWellDataList);
    this.m_WishWellDataList.Sort(new Comparison<WishWellData>(ConfigWishWell.Compare));
  }

  private static int Compare(WishWellData x, WishWellData y)
  {
    return x.Level - y.Level;
  }

  public WishWellData GetWishWellDataByLevel(int level)
  {
    level = Math.Min(Math.Max(1, level), this.m_WishWellDataList.Count);
    return this.m_WishWellDataList[level - 1];
  }

  public int GetLevelCount()
  {
    return this.m_WishWellDataList.Count;
  }
}
