﻿// Decompiled with JetBrains decompiler
// Type: EquipmentScrollChipComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EquipmentScrollChipComponent : ComponentRenderBase
{
  public System.Action<EquipmentScrollChipComponent> OnSelectedHandler;
  public UITexture icon;
  public UILabel amount;
  public UITexture background;
  public GameObject selected;
  public UILabel nameLabel;
  public UILabel lvLabel;
  public GameObject noticeTip;
  public GameObject combineUniqueTip;
  public GameObject combineTip;
  public ConfigEquipmentScrollChipInfo chipInfo;
  public EquipmentScrollChipComponetData scrollChipComponentData;

  public override void Init()
  {
    this.scrollChipComponentData = this.data as EquipmentScrollChipComponetData;
    this.chipInfo = this.scrollChipComponentData.equipmentScrollChipInfo;
    BuilderFactory.Instance.Build((UIWidget) this.icon, this.scrollChipComponentData.ItemInfo.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    Utils.SetItemBackground(this.background, this.scrollChipComponentData.ItemInfo.internalId);
    int itemCount1 = ItemBag.Instance.GetItemCount(this.scrollChipComponentData.ItemInfo.ID);
    if ((UnityEngine.Object) this.amount != (UnityEngine.Object) null)
    {
      int combineReqValue = this.chipInfo.combineReqValue;
      int itemCount2 = ItemBag.Instance.GetItemCount(this.scrollChipComponentData.ItemInfo.internalId);
      this.amount.text = (combineReqValue <= itemCount2 ? "[FFFFFF]" : "[FF0000]") + (object) itemCount2 + "[-]/" + (object) combineReqValue;
    }
    if ((UnityEngine.Object) this.nameLabel != (UnityEngine.Object) null)
      Utils.SetItemName(this.nameLabel, this.scrollChipComponentData.ItemInfo.internalId);
    if (!((UnityEngine.Object) this.lvLabel != (UnityEngine.Object) null))
      ;
    this.Select = false;
    if ((UnityEngine.Object) this.noticeTip != (UnityEngine.Object) null)
      this.noticeTip.SetActive(itemCount1 >= this.chipInfo.combineReqValue);
    this.combineUniqueTip.SetActive(false);
    this.combineTip.SetActive(false);
  }

  public override void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
    BuilderFactory.Instance.Release((UIWidget) this.background);
  }

  public bool Select
  {
    set
    {
      if (!((UnityEngine.Object) this.selected != (UnityEngine.Object) null))
        return;
      this.selected.SetActive(value);
    }
  }

  public void OnClick()
  {
    if (this.OnSelectedHandler == null)
      return;
    this.OnSelectedHandler(this);
  }

  public void OnPressItem()
  {
    Utils.DelayShowTip(this.scrollChipComponentData.ItemInfo.internalId, this.transform, 0L, 0L, 0);
  }

  public void OnReleaseItem()
  {
    Utils.StopShowItemTip();
  }
}
