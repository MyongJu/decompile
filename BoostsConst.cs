﻿// Decompiled with JetBrains decompiler
// Type: BoostsConst
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class BoostsConst
{
  public static readonly Dictionary<BoostsConst.BoostTabType, string[]> BoostTypeDict = new Dictionary<BoostsConst.BoostTabType, string[]>()
  {
    {
      BoostsConst.BoostTabType.Economics,
      new string[11]
      {
        "prop_food_generation_percent",
        "prop_wood_generation_percent",
        "prop_ore_generation_percent",
        "prop_silver_generation_percent",
        "prop_construction_time_percent",
        "prop_research_time_percent",
        "prop_army_training_time_percent",
        "prop_trap_training_time_percent",
        "prop_trap_health_percent",
        "prop_resource_gather_percent",
        "prop_silver_gather_percent"
      }
    },
    {
      BoostsConst.BoostTabType.Army,
      new string[17]
      {
        "prop_infantry_attack_percent",
        "prop_cavalry_attack_percent",
        "prop_ranged_attack_percent",
        "prop_trap_attack_percent",
        "prop_artyclose_attack_percent",
        "prop_infantry_defense_percent",
        "prop_cavalry_upkeep_percent",
        "prop_ranged_upkeep_percent",
        "prop_artyclose_defense_percent",
        "prop_artyfar_defense_percent",
        "prop_army_speed_percent",
        "prop_march_capacity_percent",
        "prop_army_upkeep_percent",
        "prop_army_load_percent",
        "prop_army_health_percent",
        "prop_army_attack_percent",
        "prop_army_defense_percent"
      }
    }
  };
  public static readonly Dictionary<string, string> Key2Title = new Dictionary<string, string>()
  {
    {
      "prop_food_generation_percent",
      "Food Production"
    },
    {
      "prop_wood_generation_percent",
      "Wood Production"
    },
    {
      "prop_ore_generation_percent",
      "Ore Production"
    },
    {
      "prop_silver_generation_percent",
      "Silver Production"
    },
    {
      "prop_construction_time_percent",
      "Construction Speed"
    },
    {
      "prop_research_time_percent",
      "Research Speed"
    },
    {
      "prop_army_training_time_percent",
      "Training Speed"
    },
    {
      "prop_trap_training_time_percent",
      "Trap Training Speed"
    },
    {
      "prop_trap_health_percent",
      "Hospital Healing Speed"
    },
    {
      "prop_resource_gather_percent",
      "Gathering Speeed"
    },
    {
      "prop_silver_gather_percent",
      "Gold Gathering Speed"
    },
    {
      "prop_infantry_attack_percent",
      "Infantry Attack"
    },
    {
      "prop_cavalry_attack_percent",
      "Cavalry Attack"
    },
    {
      "prop_ranged_attack_percent",
      "Ranged Attack"
    },
    {
      "prop_trap_attack_percent",
      "Mages Attack"
    },
    {
      "prop_artyclose_attack_percent",
      "Artillery Attack"
    },
    {
      "prop_infantry_defense_percent",
      "Infantry Defense"
    },
    {
      "prop_cavalry_upkeep_percent",
      "Cavalry Defense"
    },
    {
      "prop_ranged_upkeep_percent",
      "Ranged Defense"
    },
    {
      "prop_artyclose_defense_percent",
      "Mages Defense"
    },
    {
      "prop_artyfar_defense_percent",
      "Artillery Defense"
    },
    {
      "prop_army_speed_percent",
      "March Speed"
    },
    {
      "prop_march_capacity_percent",
      "March Size"
    },
    {
      "prop_army_upkeep_percent",
      "Upkeep Reduction"
    },
    {
      "prop_army_load_percent",
      "Troop Load"
    },
    {
      "prop_army_health_percent",
      "Troop Health Bonus"
    },
    {
      "prop_army_attack_percent",
      "Troops Attack Bonus"
    },
    {
      "prop_army_defense_percent",
      "Troop Defense Bonus"
    }
  };
  public static readonly Dictionary<BoostItemData.Type, string> ItemType2Title = new Dictionary<BoostItemData.Type, string>()
  {
    {
      BoostItemData.Type.Hero,
      "Hero"
    },
    {
      BoostItemData.Type.Item,
      "Item"
    },
    {
      BoostItemData.Type.Research,
      "Research"
    }
  };

  public static void SetupEffectStr(UILabel label, float effect)
  {
    effect *= 100f;
    if ((double) effect > 0.0)
    {
      label.text = string.Format("+{0}%", (object) effect);
      label.color = Color.green;
    }
    else if ((double) effect == 0.0)
    {
      label.text = string.Format("+{0}%", (object) effect);
      label.color = Color.white;
    }
    else
    {
      label.text = string.Format("-{0}%", (object) effect);
      label.color = Color.red;
    }
  }

  public enum BoostTabType
  {
    Economics,
    Army,
  }
}
