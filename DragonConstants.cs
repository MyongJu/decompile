﻿// Decompiled with JetBrains decompiler
// Type: DragonConstants
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public static class DragonConstants
{
  public static Dictionary<int, string> DragonPostfixDict = new Dictionary<int, string>()
  {
    {
      0,
      "green"
    },
    {
      3,
      "white"
    },
    {
      1,
      "black"
    },
    {
      4,
      "yellow"
    },
    {
      2,
      "red"
    }
  };
  public static Dictionary<int, string> DragonModelDict = new Dictionary<int, string>()
  {
    {
      257,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      513,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      769,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      1025,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      1281,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      258,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      514,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      770,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      1026,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      1282,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      259,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      515,
      "Prefab/Dragon/dragon/dragon_lv3_black"
    },
    {
      771,
      "Prefab/Dragon/dragon/dragon_lv3_yellow"
    },
    {
      1027,
      "Prefab/Dragon/dragon/dragon_lv3_white"
    },
    {
      1283,
      "Prefab/Dragon/dragon/dragon_lv3_green"
    },
    {
      260,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      516,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      772,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      1028,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      1284,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      261,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      517,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      773,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      1029,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    },
    {
      1285,
      "Prefab/Dragon/dragon/dragon_lv3_red"
    }
  };

  public static class Character
  {
    public const int Nature = 0;
  }
}
