﻿// Decompiled with JetBrains decompiler
// Type: ConfigLuckyArcherGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigLuckyArcherGroup
{
  private List<LuckyArcherGroupInfo> _luckyArcherGroupInfoList = new List<LuckyArcherGroupInfo>();
  private Dictionary<string, LuckyArcherGroupInfo> _datas;
  private Dictionary<int, LuckyArcherGroupInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<LuckyArcherGroupInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, LuckyArcherGroupInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._luckyArcherGroupInfoList.Add(enumerator.Current);
    }
    this._luckyArcherGroupInfoList.Sort((Comparison<LuckyArcherGroupInfo>) ((a, b) => a.internalId.CompareTo(b.internalId)));
  }

  public List<LuckyArcherGroupInfo> GetLuckyArcherGroupInfoList()
  {
    return this._luckyArcherGroupInfoList;
  }

  public LuckyArcherGroupInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (LuckyArcherGroupInfo) null;
  }

  public LuckyArcherGroupInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (LuckyArcherGroupInfo) null;
  }
}
