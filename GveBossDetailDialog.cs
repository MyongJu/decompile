﻿// Decompiled with JetBrains decompiler
// Type: GveBossDetailDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using March;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class GveBossDetailDialog : UI.Dialog
{
  private List<GveBossItemRenderer> m_ItemList = new List<GveBossItemRenderer>();
  public UIWidget m_FocusTarget;
  public GameObject m_NoBossKilled;
  public GameObject m_RefreshCD;
  public UISlider m_Slider;
  public UILabel m_Duration;
  public UIScrollView m_ScrollView;
  public UITable m_Table;
  public GameObject m_ItemPrefab;
  public GameObject m_NoBoss;
  private GveBossDetailDialog.Parameter m_Parameter;
  private bool m_Reposition;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as GveBossDetailDialog.Parameter;
    this.UpdateUI();
    PVPSystem.Instance.Map.FocusTile(PVPMapData.MapData.ConvertTileKXYToWorldPosition(this.m_Parameter.tileData.Location), this.m_FocusTarget, 0.0f);
    UIManager.inst.CloseKingdomTouchCircle();
    this.OnSecond(NetServerTime.inst.ServerTimestamp);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  private void OnSecond(int serverTime)
  {
    GveCampData gveCampData = this.m_Parameter.gveCampData;
    long num1 = gveCampData.endTime - gveCampData.startTime;
    long num2 = gveCampData.endTime - (long) serverTime;
    long num3 = num1 - num2;
    if (num2 > 0L)
    {
      this.m_Slider.value = (float) num3 / (float) num1;
      this.m_Duration.text = Utils.FormatTime((int) num2, false, false, true);
    }
    else
    {
      this.m_Slider.value = 1f;
      if (gveCampData.knightAmount > 0)
        this.m_Duration.text = Utils.XLAT("event_fallen_knight_title");
      else
        this.m_Duration.text = Utils.FormatTime(0, false, false, true);
    }
  }

  private void UpdateUI()
  {
    this.ClearData();
    GveCampData gveCampData = this.m_Parameter.gveCampData;
    for (int index = 0; index < gveCampData.bossList.Count; ++index)
    {
      GameObject gameObject = Utils.DuplicateGOB(this.m_ItemPrefab, this.m_Table.transform);
      gameObject.SetActive(true);
      ConfigGveBossData data = ConfigManager.inst.DB_GveBoss.GetData(gveCampData.bossList[index].bossId);
      GveBossItemRenderer component = gameObject.GetComponent<GveBossItemRenderer>();
      component.SetData(data, gveCampData.bossList[index].index, new System.Action(this.OnItemChanged), new System.Action<int>(this.OnRally));
      this.m_ItemList.Add(component);
    }
    this.m_Table.Reposition();
    this.m_ScrollView.ResetPosition();
    this.m_NoBossKilled.SetActive(gveCampData.jobId == 0L);
    this.m_RefreshCD.SetActive(gveCampData.jobId > 0L);
    this.m_NoBoss.SetActive(this.m_ItemList.Count == 0);
  }

  private void ClearData()
  {
    using (List<GveBossItemRenderer>.Enumerator enumerator = this.m_ItemList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GveBossItemRenderer current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ItemList.Clear();
  }

  private void OnItemChanged()
  {
    this.m_Table.repositionNow = true;
    this.m_Table.onReposition = (UITable.OnReposition) (() =>
    {
      this.m_ScrollView.panel.Invalidate(true);
      this.m_Table.onReposition = (UITable.OnReposition) null;
      this.m_Reposition = true;
    });
  }

  private void OnRally(int bossIndex)
  {
    GameConfigInfo data = ConfigManager.inst._DB_GameConfig.GetData("gve_boss_spirit_cost");
    if (data != null && PlayerData.inst.heroData.spirit < data.ValueInt)
      Utils.ShowException(1200050);
    else if (CityManager.inst.GetHighestBuildingLevelFor("war_rally") > 0 && PlayerData.inst.allianceId != 0L)
    {
      UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
      {
        location = this.m_Parameter.tileData.Location,
        marchType = MarchAllocDlg.MarchType.startGveRally,
        bossIndex = bossIndex,
        energyCost = data.ValueInt
      }, 1 != 0, 1 != 0, 1 != 0);
    }
    else
    {
      MarchSystem.LocalMarchCheckType type = GameEngine.Instance.marchSystem.CanRally(this.m_Parameter.tileData.Location);
      UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
      {
        title = Utils.XLAT("id_uppercase_warning"),
        description = GameEngine.Instance.marchSystem.GetCheckTypeMsg(type)
      });
    }
  }

  private void Update()
  {
    if (!this.m_Reposition)
      return;
    this.m_Reposition = false;
    this.m_Table.repositionNow = true;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public TileData tileData;
    public GveCampData gveCampData;
  }
}
