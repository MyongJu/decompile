﻿// Decompiled with JetBrains decompiler
// Type: FNVHash
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class FNVHash
{
  private static ulong FNV_64_INIT = 14695981039346656037;
  private static ulong FNV_64_PRIME = 1099511628211;
  private static uint FNV_32_INIT = 2166136261;
  private static uint FNV_32_PRIME = 16777619;

  public static int hash32(char[] k)
  {
    uint num = FNVHash.FNV_32_INIT;
    int length = k.Length;
    for (int index = 0; index < length; ++index)
      num = (num ^ (uint) k[index]) * FNVHash.FNV_32_PRIME;
    return (int) num;
  }

  public static long hash64(char[] k)
  {
    ulong num = FNVHash.FNV_64_INIT;
    int length = k.Length;
    for (int index = 0; index < length; ++index)
      num = (num ^ (ulong) k[index]) * FNVHash.FNV_64_PRIME;
    return (long) num;
  }

  public static int hash32(string k)
  {
    return FNVHash.hash32(k.ToCharArray());
  }

  public static long hash64(string k)
  {
    return FNVHash.hash64(k.ToCharArray());
  }
}
