﻿// Decompiled with JetBrains decompiler
// Type: Puppet2D_EditSkinWeights
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class Puppet2D_EditSkinWeights : MonoBehaviour
{
  public GameObject Bone0;
  public GameObject Bone1;
  public GameObject Bone2;
  public GameObject Bone3;
  public int boneIndex0;
  public int boneIndex1;
  public int boneIndex2;
  public int boneIndex3;
  public float Weight0;
  public float Weight1;
  public float Weight2;
  public float Weight3;
  public Mesh mesh;
  public SkinnedMeshRenderer meshRenderer;
  public int vertNumber;
  private GameObject[] handles;
  public Vector3[] verts;
  private static Mesh skinnedMesh;
  public bool autoUpdate;

  private void Update()
  {
    this.Refresh();
  }

  public void Refresh()
  {
    if ((bool) ((Object) this.transform.parent) && (bool) ((Object) this.transform.parent.GetComponent<SkinnedMeshRenderer>()))
      this.meshRenderer = this.transform.parent.GetComponent<SkinnedMeshRenderer>();
    BoneWeight[] boneWeights = this.mesh.boneWeights;
    if ((bool) ((Object) this.Bone0))
      boneWeights[this.vertNumber].boneIndex0 = this.boneIndex0;
    if ((bool) ((Object) this.Bone1))
      boneWeights[this.vertNumber].boneIndex1 = this.boneIndex1;
    if ((bool) ((Object) this.Bone2))
      boneWeights[this.vertNumber].boneIndex2 = this.boneIndex2;
    if ((bool) ((Object) this.Bone3))
      boneWeights[this.vertNumber].boneIndex3 = this.boneIndex3;
    boneWeights[this.vertNumber].weight0 = this.Weight0;
    boneWeights[this.vertNumber].weight1 = this.Weight1;
    boneWeights[this.vertNumber].weight2 = this.Weight2;
    boneWeights[this.vertNumber].weight3 = this.Weight3;
    this.mesh.boneWeights = boneWeights;
    if (!((Object) this.meshRenderer != (Object) null))
      return;
    this.meshRenderer.sharedMesh = this.mesh;
  }
}
