﻿// Decompiled with JetBrains decompiler
// Type: TalentTreeDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class TalentTreeDlg : UI.Dialog
{
  public static string[] Titles = new string[3]
  {
    "player_profile_skill_uppercase_war",
    "player_profile_skill_uppercase_economy",
    "player_profile_skill_uppercase_balance"
  };
  private int reqid = -1;
  private int treeindex = -1;
  public const int FIRST_SLOT_DELTA = 400;
  public const int SLOT_HER_STEP = 230;
  public const int SLOT_VET_STEP = 180;
  private const string vfxpath = "Prefab/VFX/fx_unlock";
  private const int INVALIDFLAG = -1;
  private bool needUpdate;
  public UIButtonBar _bar;
  private int tagIndex;
  [SerializeField]
  private TalentTreeDlg.Panel panel;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.tagIndex = -1;
    if (orgParam == null)
      this.ChangeTag(PlayerPrefsEx.GetInt("talentTreeIndex", 1));
    else
      this.ChangeTag((orgParam as TalentTreeDlg.Parameter).index);
    DBManager.inst.DB_hero.onDataChanged += new System.Action<long>(this.HeroDataChange);
    DBManager.inst.DB_Talent.onDataChanged += new System.Action<int>(this.OnTalentDataChange);
    DBManager.inst.DB_Talent.onDataRemoved += new System.Action(this.OnTalentDataChange);
    this._bar.SelectedIndex = PlayerData.inst.heroData.skillGroupId - 1;
    this._bar.OnSelectedHandler += new System.Action<int>(this.OnSelectedHandler);
  }

  private void OnSelectedHandler(int index)
  {
    this.OnSlotItemClick();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this._bar.OnSelectedHandler -= new System.Action<int>(this.OnSelectedHandler);
    DBManager.inst.DB_hero.onDataChanged -= new System.Action<long>(this.HeroDataChange);
    DBManager.inst.DB_Talent.onDataChanged -= new System.Action<int>(this.OnTalentDataChange);
    DBManager.inst.DB_Talent.onDataRemoved -= new System.Action(this.OnTalentDataChange);
  }

  public void Update()
  {
    if (!this.needUpdate)
      return;
    this.needUpdate = false;
    this.UpdateTalentSwitchStatus();
    this.FreshTreeData(this.tagIndex, false);
  }

  private void ChangeTag(int treeIndex)
  {
    if (this.tagIndex == treeIndex)
      return;
    this.tagIndex = treeIndex;
    PlayerPrefsEx.SetInt("talentTreeIndex", treeIndex);
    PlayerPrefsEx.Save();
    this.panel.SetTitleIndex(this.tagIndex);
    this.UpdateTalentSwitchStatus();
    this.FreshTreeData(this.tagIndex, true);
  }

  private void UpdateTalentSwitchStatus()
  {
    if (PlayerData.inst.heroData == null)
      return;
    GameObject[] buttons = this._bar.buttons;
    this._bar.Clear();
    int index1 = 0;
    while (index1 < buttons.Length)
    {
      int index2 = index1;
      if (this.GetEquipCanSwitchCount() < index2)
      {
        this._bar.AddLock(index2, new System.Action(this.OnLockItemClick));
        buttons[index1].GetComponent<TalentSwitchSlot>().Lock();
      }
      index1 += 2;
    }
  }

  private void FreshTreeData(int treeIndex = 0, bool scollBack = true)
  {
    this.panel.Init();
    for (int index = 0; index < this.panel.talentTreeTitle.Length; ++index)
      this.panel.talentTreeTitle[index].text = string.Format("{0} [F9B845]{1}[-]", (object) ScriptLocalization.Get(TalentTreeDlg.Titles[index], true), (object) DBManager.inst.DB_Talent.GetTalentTreeTalentCount(index + 1));
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(PlayerData.inst.uid);
    if (heroData != null)
      this.panel.talentLeftPoint.text = string.Format("{0}: {1}", (object) Utils.XLAT("player_profile_skill_points"), (object) heroData.skill_points);
    List<TalentTreeInfo> talentsByTreeIndex = ConfigManager.inst.DB_TalentTree.GetTalentsByTreeIndex(treeIndex + 1);
    this.panel.usedSlotNum = talentsByTreeIndex == null ? 0 : talentsByTreeIndex.Count;
    this.ClearLine();
    for (int index = 0; index < this.panel.usedSlotNum; ++index)
      this.SetSlot(this.panel.slots[index], talentsByTreeIndex[index]);
    if (treeIndex + 1 == this.treeindex)
    {
      this.reqid = -1;
      this.treeindex = -1;
    }
    for (int usedSlotNum = this.panel.usedSlotNum; usedSlotNum < this.panel.slots.Count; ++usedSlotNum)
      this.panel.slots[usedSlotNum].gameObject.SetActive(false);
    if (scollBack)
      this.panel.panelScrollView.ResetPosition();
    this.CheckResetButtonState();
  }

  private void SetSlot(TalentSlot slot, TalentTreeInfo talentTreeInfo)
  {
    slot.gameObject.SetActive(true);
    slot.talentTreeId = talentTreeInfo.internalId;
    slot.onTalentSlotClick = new System.Action<int>(this.OnTalentSlotClick);
    slot.panel.dragScrollView.scrollView = this.panel.panelScrollView;
    slot.transform.localPosition = new Vector3((float) (400 + 230 * talentTreeInfo.tier), (float) (180 * (talentTreeInfo.row - 2)), 0.0f);
    PlayerTalentInfo talentByTalentTreeId = ConfigManager.inst.DB_PlayerTalent.GetCurrentLevelTalentByTalentTreeId(talentTreeInfo.internalId);
    Requirements requirementsByTalentTreeId = ConfigManager.inst.DB_PlayerTalent.GetRequirementsByTalentTreeId(talentTreeInfo.internalId);
    List<int> intList = new List<int>();
    if (requirementsByTalentTreeId != null)
    {
      Dictionary<string, int>.KeyCollection.Enumerator enumerator = requirementsByTalentTreeId.requires.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        int num = int.Parse(enumerator.Current);
        intList.Add(num);
        this.SetLine(talentTreeInfo.internalId, num, talentByTalentTreeId != null && DBManager.inst.DB_Talent.IsTalentOwned(num));
      }
    }
    if (!intList.Contains(this.reqid) || talentByTalentTreeId != null)
      return;
    this.StartCoroutine(this.PlayEffect(0.2f, slot.transform));
  }

  [DebuggerHidden]
  private IEnumerator PlayEffect(float time, Transform parent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TalentTreeDlg.\u003CPlayEffect\u003Ec__Iterator9C()
    {
      time = time,
      parent = parent,
      \u003C\u0024\u003Etime = time,
      \u003C\u0024\u003Eparent = parent
    };
  }

  private void SetLine(int endTalentTreeId, int startTalentId, bool active = false)
  {
    while (this.panel.usedLineNum >= this.panel.lines.Count)
    {
      GameObject gameObject = NGUITools.AddChild(this.panel.linesParent.gameObject, this.panel.lineOrg.gameObject);
      gameObject.name = string.Format("line_{0}", (object) (100 + this.panel.lines.Count));
      this.panel.lines.Add(gameObject.GetComponent<TalentLine>());
      gameObject.SetActive(false);
    }
    TalentTreeInfo talentTreeInfo1 = ConfigManager.inst.DB_TalentTree.GetTalentTreeInfo(ConfigManager.inst.DB_PlayerTalent.GetPlayerTalentInfo(startTalentId).talentTreeInternalId);
    TalentTreeInfo talentTreeInfo2 = ConfigManager.inst.DB_TalentTree.GetTalentTreeInfo(endTalentTreeId);
    this.panel.lines[this.panel.usedLineNum].gameObject.SetActive(true);
    this.panel.lines[this.panel.usedLineNum].SetPosition(talentTreeInfo2.tier, talentTreeInfo2.row, talentTreeInfo1.tier, talentTreeInfo1.row, active);
    ++this.panel.usedLineNum;
  }

  private void ClearLine()
  {
    for (int index = 0; index < this.panel.lines.Count; ++index)
    {
      this.panel.lines[index].gameObject.SetActive(false);
      this.panel.usedLineNum = 0;
    }
  }

  public void CheckResetButtonState()
  {
    this.panel.BT_Reset.isEnabled = DBManager.inst.DB_Talent.CanReset();
  }

  private void OnSlotItemClick()
  {
    Utils.GetAllEquipedItemData(BagType.Hero);
    RequestManager.inst.SendRequest("Hero:changeSkillGroup", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "group_id",
        (object) (this._bar.SelectedIndex + 1)
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.toast.Show(Utils.XLAT("toast_talents_quick_talents_success"), (System.Action) null, 4f, true);
    }), true);
  }

  private void OnLockItemClick()
  {
    if (this.GetShouldActiveVipLevel() < 0)
      return;
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("0", this.GetShouldActiveVipLevel().ToString());
    MessageBoxWith1Button.Parameter parameter = new MessageBoxWith1Button.Parameter();
    parameter.Title = Utils.XLAT("talent_quick_talents_unlock_title");
    parameter.Content = ScriptLocalization.GetWithPara("talent_quick_talents_unlock_description", para, true);
    parameter.Okay = Utils.XLAT("vip_activate_button");
    parameter.OkayCallback += new System.Action(this.Skip2VipView);
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) parameter);
  }

  private int GetShouldActiveVipLevel()
  {
    return ConfigManager.inst.DB_VIP_Level.GetVipLevelByBenefitId("prop_talent_extra_scheme_value");
  }

  private void Skip2VipView()
  {
    int shouldActiveVipLevel = this.GetShouldActiveVipLevel();
    if (shouldActiveVipLevel <= 0)
      return;
    UIManager.inst.OpenDlg("VIP/VipBaseDlg", (UI.Dialog.DialogParameter) new VIPBacePopup.Parameter()
    {
      nextVipLevel = shouldActiveVipLevel
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private int GetEquipCanSwitchCount()
  {
    return ConfigManager.inst.DB_BenefitCalc.GetFinalData(1, "calc_talent_extra_scheme");
  }

  public void OnTalentDataChange(int talentId)
  {
    this.needUpdate = true;
  }

  public void OnTalentDataChange()
  {
    this.needUpdate = true;
  }

  public void HeroDataChange(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.needUpdate = true;
  }

  public void OnTreeTag1Click()
  {
    this.ChangeTag(0);
  }

  public void OnTreeTag2Click()
  {
    this.ChangeTag(1);
  }

  public void OnTreeTag3Click()
  {
    this.ChangeTag(2);
  }

  public void OnTalentSlotClick(int inTalentTreeId)
  {
    (UIManager.inst.OpenPopup("Talent/TalentEnhanceDlg", (Popup.PopupParameter) new TalentEnhanceDlg.Parameter()
    {
      talentTreeId = inTalentTreeId
    }) as TalentEnhanceDlg).onTalentLvlUpFull += new System.Action<int, int>(this.NeedShowEffect);
  }

  private void NeedShowEffect(int internalid, int tree)
  {
    this.reqid = internalid;
    this.treeindex = tree;
  }

  public void OnResetButtonClick()
  {
    ShopStaticInfo shopData = ConfigManager.inst.DB_Shop.GetShopData("shopitem_hero_skill_reset");
    UIManager.inst.OpenPopup("UseOrBuyAndUseItemPopup", (Popup.PopupParameter) new UseOrBuyAndUseItemPopup.Parameter()
    {
      itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(shopData.Item_InternalId),
      callback = (System.Action<bool, object>) null,
      titleText = Utils.XLAT("talent_uppercase_reset_points_title"),
      btnText = Utils.XLAT("talent_reset_points_button")
    });
  }

  [Serializable]
  protected class Panel
  {
    public Color[] backgroundColer = new Color[3]
    {
      new Color((float) byte.MaxValue, 74f, 74f, 141f),
      new Color(73f, (float) byte.MaxValue, 97f, 141f),
      new Color(73f, 81f, (float) byte.MaxValue, 141f)
    };
    public List<TalentLine> lines = new List<TalentLine>();
    public List<TalentSlot> slots = new List<TalentSlot>();
    private int titleCurIndex = -1;
    public const int MAX_TALENT_SLOT_NUM = 50;
    private bool isInit;
    public UITexture backgroundCircle;
    public UITexture backgroundImage;
    public TweenColor backgroundTweenCircle;
    public TweenColor backgroundTweenImage;
    public UILabel talentLeftPoint;
    public UILabel[] talentTreeTitle;
    public Transform[] talentTitleBackground;
    public UIScrollView panelScrollView;
    public int usedLineNum;
    public Transform linesParent;
    public TalentLine lineOrg;
    public Material[] lineMats;
    public int usedSlotNum;
    public Transform slotsParent;
    public TalentSlot talentSlotOrg;
    public UIButton BT_Reset;

    public void SetTitleIndex(int titleIndex)
    {
      this.TweenBackgroundImage(titleIndex, this.titleCurIndex == -1);
      for (int index = 0; index < this.talentTitleBackground.Length; ++index)
        this.talentTitleBackground[index].gameObject.SetActive(false);
      this.talentTitleBackground[titleIndex].gameObject.SetActive(true);
      if (this.lineMats != null)
      {
        for (int index = 0; index < this.lineMats.Length; ++index)
          this.lineMats[index].SetColor("_TintColor", this.backgroundColer[titleIndex]);
      }
      this.titleCurIndex = titleIndex;
    }

    private void TweenBackgroundImage(int titleIndex, bool imd = true)
    {
      if (imd)
      {
        this.backgroundCircle.color = this.backgroundColer[titleIndex];
        this.backgroundImage.color = this.backgroundColer[titleIndex];
      }
      else
      {
        TweenColor.Begin(this.backgroundCircle.gameObject, 0.3f, this.backgroundColer[titleIndex]);
        TweenColor.Begin(this.backgroundImage.gameObject, 0.3f, this.backgroundColer[titleIndex]);
      }
    }

    public void Init()
    {
      if (this.isInit)
        return;
      for (int index = 0; index < 50; ++index)
      {
        GameObject gameObject = NGUITools.AddChild(this.slotsParent.gameObject, this.talentSlotOrg.gameObject);
        gameObject.name = string.Format("Talent_slot_{0}", (object) (100 + index));
        this.slots.Add(gameObject.GetComponent<TalentSlot>());
        gameObject.SetActive(false);
      }
      this.usedSlotNum = 0;
      this.isInit = true;
    }
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int index;
  }
}
