﻿// Decompiled with JetBrains decompiler
// Type: DungeonMopupInventoryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DungeonMopupInventoryPopup : Popup
{
  private List<DungeonMopupItemSlotRenderer> _items = new List<DungeonMopupItemSlotRenderer>();
  public DragonKnightDungeonInventoryPopup _inventory;
  public UIScrollView _scrollView;
  public UIGrid _grid;
  public GameObject _itemPrefab;
  public GameObject _bagFullTip;
  public GameObject _putBagButton;
  private DungeonMopupItemSlotRenderer _selectedItemRenderer;
  private DungeonMopupInventoryPopup.Parameter _param;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    this._param = orgParam as DungeonMopupInventoryPopup.Parameter;
    base.OnShow(orgParam);
    this._inventory.OnShow(orgParam);
    this.UpdateUI();
    DBManager.inst.DB_DragonKnightDungeon.onDataChanged += new System.Action<DragonKnightDungeonData>(this.OnDungeonChanged);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this._inventory.OnHide(orgParam);
    DBManager.inst.DB_DragonKnightDungeon.onDataChanged -= new System.Action<DragonKnightDungeonData>(this.OnDungeonChanged);
  }

  public void OnPutIntoBagClick()
  {
    DragonKnightData dragonKnightData = PlayerData.inst.dragonKnightData;
    DragonKnightDungeonData dungeonData = DragonKnightUtils.GetDungeonData();
    UIManager.inst.OpenPopup("DragonKnight/DungeonMopupChooseAmountPopup", (Popup.PopupParameter) new DungeonMopupChooseAmountPopup.Parameter()
    {
      itemId = this._selectedItemRenderer.ItemInfo.internalId,
      quantity = this._selectedItemRenderer.ItemCount,
      maxWeight = (int) (dragonKnightData.Capacity - (long) dungeonData.CurrentBagCapacity)
    });
  }

  public void OnCloseClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnDungeonChanged(DragonKnightDungeonData dungeon)
  {
    RoomManager roomManager = new RoomManager();
    roomManager.SetMazeData(dungeon);
    Room roomByPosition = roomManager.GetRoomByPosition(this._param.floor, this._param.x, this._param.y);
    List<DungeonMopupItemSlotRenderer> itemSlotRendererList = new List<DungeonMopupItemSlotRenderer>();
    if (roomByPosition.AllRaidChest.Count > 0)
    {
      Dictionary<int, int> reward = roomByPosition.AllRaidChest[0].reward;
      for (int index = 0; index < this._items.Count; ++index)
      {
        DungeonMopupItemSlotRenderer itemSlotRenderer = this._items[index];
        int count;
        if (reward.TryGetValue(itemSlotRenderer.ItemInfo.internalId, out count))
          itemSlotRenderer.SetData(itemSlotRenderer.ItemInfo.internalId, count, new System.Action<DungeonMopupItemSlotRenderer>(this.OnItemSelected));
        else
          itemSlotRendererList.Add(itemSlotRenderer);
      }
    }
    else
    {
      for (int index = 0; index < this._items.Count; ++index)
      {
        DungeonMopupItemSlotRenderer itemSlotRenderer = this._items[index];
        itemSlotRendererList.Add(itemSlotRenderer);
      }
    }
    for (int index = 0; index < itemSlotRendererList.Count; ++index)
    {
      DungeonMopupItemSlotRenderer itemSlotRenderer = itemSlotRendererList[index];
      itemSlotRenderer.gameObject.SetActive(false);
      itemSlotRenderer.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) itemSlotRenderer.gameObject);
      this._items.Remove(itemSlotRenderer);
    }
    this._grid.Reposition();
    if (this._items.Count <= 4)
      this._scrollView.ResetPosition();
    if (this._items.Count > 0)
      this._items[0].Selected = true;
    this.OnItemSelected(this._selectedItemRenderer);
  }

  private void OnItemSelected(DungeonMopupItemSlotRenderer renderer)
  {
    this._selectedItemRenderer = renderer;
    DragonKnightData dragonKnightData = PlayerData.inst.dragonKnightData;
    DragonKnightDungeonData dungeonData = DragonKnightUtils.GetDungeonData();
    bool flag = dungeonData.HasSlotFor(this._selectedItemRenderer.ItemInfo.internalId) && dragonKnightData.Capacity - (long) dungeonData.CurrentBagCapacity >= (long) this._selectedItemRenderer.ItemInfo.DungeonWeight;
    this._bagFullTip.SetActive(!flag);
    this._putBagButton.SetActive(flag);
  }

  private void UpdateUI()
  {
    Dictionary<int, int>.Enumerator enumerator = this._param.raidChest.reward.GetEnumerator();
    while (enumerator.MoveNext())
    {
      GameObject gameObject = Utils.DuplicateGOB(this._itemPrefab, this._grid.transform);
      gameObject.SetActive(true);
      DungeonMopupItemSlotRenderer component = gameObject.GetComponent<DungeonMopupItemSlotRenderer>();
      component.SetData(enumerator.Current.Key, enumerator.Current.Value, new System.Action<DungeonMopupItemSlotRenderer>(this.OnItemSelected));
      if (this._items.Count == 0)
        component.Selected = true;
      this._items.Add(component);
    }
    this._grid.Reposition();
    this._scrollView.ResetPosition();
  }

  public class Parameter : Popup.PopupParameter
  {
    public RoomRaidChest raidChest;
    public int floor;
    public int x;
    public int y;
  }
}
