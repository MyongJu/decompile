﻿// Decompiled with JetBrains decompiler
// Type: SevenDaysRewardItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SevenDaysRewardItemRenderer : MonoBehaviour
{
  public ItemIconRenderer itemRender;
  public GameObject expired;
  public UILabel count;

  public void SetData(int itemId, int count)
  {
    this.itemRender.SetData(itemId, string.Empty, false);
    this.count.text = count.ToString();
  }

  public void Expired()
  {
    GreyUtility.Grey(this.expired);
  }

  public void Normal()
  {
    GreyUtility.Normal(this.expired);
  }
}
