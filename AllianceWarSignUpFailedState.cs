﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarSignUpFailedState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class AllianceWarSignUpFailedState : AllianceWarBaseState
{
  private const string SIGNUP_FAILED = "alliance_warfare_unregistered_notice";
  public UILabel failedDescription;

  public override void Show(bool requestData)
  {
    base.Show(false);
  }

  public override void UpdateUI()
  {
    this.state.text = ScriptLocalization.GetWithPara("alliance_warfare_signup_fail_status", new Dictionary<string, string>()
    {
      {
        "0",
        Utils.FormatTime(this.allianceWarData.FightEndTime - NetServerTime.inst.ServerTimestamp, true, true, true)
      }
    }, true);
    this.instruction.text = Utils.XLAT("alliance_warfare_event_description");
    this.failedDescription.text = Utils.XLAT("alliance_warfare_unregistered_notice");
  }

  public override void Hide()
  {
    base.Hide();
  }
}
