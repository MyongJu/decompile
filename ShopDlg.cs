﻿// Decompiled with JetBrains decompiler
// Type: ShopDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class ShopDlg : MonoBehaviour
{
  private string[] SHOPITEM_CATEGORIES = new string[6]
  {
    "all",
    "general",
    "resources",
    "speed up",
    "combat",
    "chest"
  };
  private Dictionary<int, bool> isInits = new Dictionary<int, bool>();
  private Dictionary<int, List<int>> datas = new Dictionary<int, List<int>>();
  private List<ShopItemGroupRenderer> groupList = new List<ShopItemGroupRenderer>();
  private GameObjectPool pools = new GameObjectPool();
  public UIButtonBar buttonBar;
  private bool init;
  public IconGroup group;
  public UIScrollView[] scrollViews;
  public UIWrapContent[] contents;
  public ShopItemGroupRenderer groupPrefab;
  public Icon shopItemDetail;
  public UILabel normalTip;

  private void InitGroup(GameObject go, int index, int realIndex)
  {
    realIndex = Mathf.Abs(realIndex);
    ShopItemGroupRenderer component = go.GetComponent<ShopItemGroupRenderer>();
    if (component.Selected)
    {
      component.HideHightLight();
      NGUITools.SetActive(this.shopItemDetail.gameObject, false);
      NGUITools.SetActive(this.normalTip.gameObject, true);
    }
    if (!this.datas.ContainsKey(realIndex))
      return;
    List<int> data = this.datas[realIndex];
    component.SetData(data.ToArray());
  }

  private UIScrollView ScrollView
  {
    get
    {
      return this.scrollViews[this.buttonBar.SelectedIndex];
    }
  }

  private UIWrapContent Content
  {
    get
    {
      return this.contents[this.buttonBar.SelectedIndex];
    }
  }

  private int SelectedIndex
  {
    get
    {
      return this.buttonBar.SelectedIndex;
    }
    set
    {
      this.buttonBar.SelectedIndex = value;
    }
  }

  private void Start()
  {
    this.Init();
  }

  private void Init()
  {
    if (this.init)
      return;
    this.init = true;
    this.buttonBar.OnSelectedHandler = new System.Action<int>(this.OnSelectedHandler);
    this.pools.Initialize(this.groupPrefab.gameObject, this.gameObject);
  }

  public void Dispose()
  {
    this.group.Dispose();
    this.shopItemDetail.Dispose();
    this.init = false;
    this.pools.Clear();
  }

  private void OnSelectedHandler(int index)
  {
    this.UpdateUI();
  }

  private void Clear()
  {
    for (int index = 0; index < this.groupList.Count; ++index)
      this.groupList[index].Dispose();
    this.groupList.Clear();
  }

  public void Refresh()
  {
    this.Init();
    if (this.SelectedIndex != -1)
      return;
    this.SelectedIndex = 0;
  }

  private void SetView()
  {
    for (int index = 0; index < this.scrollViews.Length; ++index)
      NGUITools.SetActive(this.scrollViews[index].gameObject, false);
    NGUITools.SetActive(this.ScrollView.gameObject, true);
  }

  private void UpdateUI()
  {
    NGUITools.SetActive(this.shopItemDetail.gameObject, false);
    NGUITools.SetActive(this.normalTip.gameObject, true);
    this.CreateDatas();
    this.SetView();
    if (!this.isInits.ContainsKey(this.buttonBar.SelectedIndex))
    {
      this.isInits.Add(this.buttonBar.SelectedIndex, true);
      if (this.datas.Count > 1)
      {
        this.Content.minIndex = -(this.datas.Count - 1);
        this.Content.maxIndex = 0;
      }
      else
      {
        this.Content.minIndex = -1;
        this.Content.maxIndex = 0;
      }
      for (int index = 0; index < this.datas.Count; ++index)
      {
        if (this.datas.ContainsKey(index))
          this.InitGroup(this.GetGroup().gameObject, 0, index);
        if (index == 4)
          break;
      }
      this.Content.onInitializeItem += new UIWrapContent.OnInitializeItem(this.InitGroup);
      this.Content.enabled = true;
      this.Content.SortBasedOnScrollMovement();
      this.Content.WrapContent();
      Utils.ExecuteInSecs(1f / 1000f, (System.Action) (() => this.ScrollView.ResetPosition()));
    }
    this.ScrollView.ResetPosition();
  }

  private ShopItemGroupRenderer GetGroup()
  {
    GameObject go = NGUITools.AddChild(this.Content.gameObject, this.groupPrefab.gameObject);
    ShopItemGroupRenderer component = go.GetComponent<ShopItemGroupRenderer>();
    component.OnSelectedHandler = new System.Action<bool, int>(this.OnViewShopItemDetail);
    this.groupList.Add(component);
    NGUITools.SetActive(go, true);
    return component;
  }

  private void OnSelectedHandler(ShopItemRenderer selected)
  {
    for (int index = 0; index < this.group.TotalIcons.Count; ++index)
    {
      ShopItemRenderer totalIcon = this.group.TotalIcons[index] as ShopItemRenderer;
      if ((UnityEngine.Object) selected != (UnityEngine.Object) totalIcon)
        totalIcon.Selected = false;
    }
  }

  private void OnViewShopItemDetail(bool view, int shopId)
  {
    NGUITools.SetActive(this.shopItemDetail.gameObject, view);
    NGUITools.SetActive(this.normalTip.gameObject, !view);
    this.shopItemDetail.FeedData((IComponentData) this.GetIconData(shopId));
    for (int index = 0; index < this.groupList.Count; ++index)
    {
      if (!this.groupList[index].Contains(shopId))
        this.groupList[index].HideHightLight();
    }
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  private IconData GetIconData(int shopId)
  {
    ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(shopId);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(dataByInternalId.Item_InternalId);
    IconData iconData = new IconData();
    iconData.image = itemStaticInfo.ImagePath;
    iconData.Data = (object) dataByInternalId.Item_InternalId;
    iconData.contents = new string[2];
    iconData.contents[0] = itemStaticInfo.LocName;
    iconData.contents[1] = itemStaticInfo.LocDescription;
    return iconData;
  }

  private void CreateDatas()
  {
    this.datas.Clear();
    string empty = this.SHOPITEM_CATEGORIES[this.SelectedIndex];
    if (this.SelectedIndex == 0)
      empty = string.Empty;
    List<ShopStaticInfo> currentSaleShopItems = ItemBag.Instance.GetCurrentSaleShopItems("shop", empty);
    currentSaleShopItems.Sort(new Comparison<ShopStaticInfo>(this.CompareShopItem));
    List<int> intList = new List<int>();
    int key = 0;
    for (int index = 0; index < currentSaleShopItems.Count; ++index)
    {
      if (intList.Count < 2)
      {
        intList.Add(currentSaleShopItems[index].internalId);
        if (intList.Count == 2)
        {
          this.datas.Add(key, intList);
          ++key;
          intList = new List<int>();
        }
      }
    }
    if (currentSaleShopItems.Count % 2 <= 0)
      return;
    this.datas.Add(key, intList);
  }

  private List<IconData> GetIconDatas()
  {
    string empty = this.SHOPITEM_CATEGORIES[this.SelectedIndex];
    if (this.SelectedIndex == 0)
      empty = string.Empty;
    List<ShopStaticInfo> currentSaleShopItems = ItemBag.Instance.GetCurrentSaleShopItems("shop", empty);
    currentSaleShopItems.Sort(new Comparison<ShopStaticInfo>(this.CompareShopItem));
    List<IconData> iconDataList = new List<IconData>();
    for (int index = 0; index < currentSaleShopItems.Count; ++index)
      iconDataList.Add(this.GetIconData(currentSaleShopItems[index].internalId));
    return iconDataList;
  }

  private int CompareShopItem(ShopStaticInfo a, ShopStaticInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }
}
