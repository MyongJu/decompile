﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentSuitEnhance
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigEquipmentSuitEnhance
{
  private Dictionary<string, ConfigEquipmentSuitEnhanceInfo> datas;
  private Dictionary<int, ConfigEquipmentSuitEnhanceInfo> dicByUniqueId;

  private int SortByRequireNumber(ConfigEquipmentSuitMainInfo a, ConfigEquipmentSuitMainInfo b)
  {
    return a.numberRequired.CompareTo(b.numberRequired);
  }

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ConfigEquipmentSuitEnhanceInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public ConfigEquipmentSuitEnhanceInfo GetData(int internalID)
  {
    return this.dicByUniqueId[internalID];
  }

  public ConfigEquipmentSuitEnhanceInfo GetLessEqual(int level, BagType bagType)
  {
    string str = bagType != BagType.DragonKnight ? "lord" : "dragon_knight";
    ConfigEquipmentSuitEnhanceInfo equipmentSuitEnhanceInfo = (ConfigEquipmentSuitEnhanceInfo) null;
    using (Dictionary<string, ConfigEquipmentSuitEnhanceInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ConfigEquipmentSuitEnhanceInfo current = enumerator.Current;
        if (current.type == str && current.enhanceLevelReq <= level && (equipmentSuitEnhanceInfo == null || equipmentSuitEnhanceInfo.enhanceLevelReq < current.enhanceLevelReq))
          equipmentSuitEnhanceInfo = current;
      }
    }
    return equipmentSuitEnhanceInfo;
  }

  public ConfigEquipmentSuitEnhanceInfo GetGreater(int level, BagType bagType)
  {
    string str = bagType != BagType.DragonKnight ? "lord" : "dragon_knight";
    ConfigEquipmentSuitEnhanceInfo equipmentSuitEnhanceInfo = (ConfigEquipmentSuitEnhanceInfo) null;
    using (Dictionary<string, ConfigEquipmentSuitEnhanceInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ConfigEquipmentSuitEnhanceInfo current = enumerator.Current;
        if (current.type == str && current.enhanceLevelReq > level && (equipmentSuitEnhanceInfo == null || equipmentSuitEnhanceInfo.enhanceLevelReq > current.enhanceLevelReq))
          equipmentSuitEnhanceInfo = current;
      }
    }
    return equipmentSuitEnhanceInfo;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, ConfigEquipmentSuitEnhanceInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, ConfigEquipmentSuitEnhanceInfo>) null;
  }
}
