﻿// Decompiled with JetBrains decompiler
// Type: LoadingController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LoadingController : MonoBehaviour
{
  public UISlider m_Slider;
  public UILabel m_Tip;

  public void SetProgress(float progress)
  {
    if (!(bool) ((Object) this.m_Slider))
      return;
    this.m_Slider.value = progress;
  }

  public float GetProgress()
  {
    if ((bool) ((Object) this.m_Slider))
      return this.m_Slider.value;
    return 1f;
  }

  public void SetTip(string tip)
  {
    if (!(bool) ((Object) this.m_Tip))
      return;
    this.m_Tip.text = tip;
  }
}
