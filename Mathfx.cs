﻿// Decompiled with JetBrains decompiler
// Type: Mathfx
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Mathfx
{
  public static float Hermite(float start, float end, float value)
  {
    return Mathf.Lerp(start, end, (float) ((double) value * (double) value * (3.0 - 2.0 * (double) value)));
  }

  public static float Sinerp(float start, float end, float value)
  {
    return Mathf.Lerp(start, end, Mathf.Sin((float) ((double) value * 3.14159274101257 * 0.5)));
  }

  public static float Coserp(float start, float end, float value)
  {
    return Mathf.Lerp(start, end, 1f - Mathf.Cos((float) ((double) value * 3.14159274101257 * 0.5)));
  }

  public static float Berp(float start, float end, float value)
  {
    value = Mathf.Clamp01(value);
    value = (float) (((double) Mathf.Sin((float) ((double) value * 3.14159274101257 * (0.200000002980232 + 2.5 * (double) value * (double) value * (double) value))) * (double) Mathf.Pow(1f - value, 2.2f) + (double) value) * (1.0 + 1.20000004768372 * (1.0 - (double) value)));
    return start + (end - start) * value;
  }

  public static float SmoothStep(float x, float min, float max)
  {
    x = Mathf.Clamp(x, min, max);
    float num1 = (float) (((double) x - (double) min) / ((double) max - (double) min));
    float num2 = (float) (((double) x - (double) min) / ((double) max - (double) min));
    return (float) (-2.0 * (double) num1 * (double) num1 * (double) num1 + 3.0 * (double) num2 * (double) num2);
  }

  public static float Lerp(float start, float end, float value)
  {
    return (float) ((1.0 - (double) value) * (double) start + (double) value * (double) end);
  }

  public static Vector3 NearestPoint(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
  {
    Vector3 vector3 = Vector3.Normalize(lineEnd - lineStart);
    float num = Vector3.Dot(point - lineStart, vector3) / Vector3.Dot(vector3, vector3);
    return lineStart + num * vector3;
  }

  public static Vector3 NearestPointStrict(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
  {
    Vector3 a = lineEnd - lineStart;
    Vector3 vector3 = Vector3.Normalize(a);
    float num = Vector3.Dot(point - lineStart, vector3) / Vector3.Dot(vector3, vector3);
    return lineStart + Mathf.Clamp(num, 0.0f, Vector3.Magnitude(a)) * vector3;
  }

  public static float Bounce(float x)
  {
    return Mathf.Abs(Mathf.Sin((float) (6.28000020980835 * ((double) x + 1.0) * ((double) x + 1.0))) * (1f - x));
  }

  public static bool Approx(float val, float about, float range)
  {
    return (double) Mathf.Abs(val - about) < (double) range;
  }

  public static bool Approx(Vector3 val, Vector3 about, float range)
  {
    return (double) (val - about).sqrMagnitude < (double) range * (double) range;
  }

  public static float Clerp(float start, float end, float value)
  {
    float num1 = 0.0f;
    float num2 = 360f;
    float num3 = Mathf.Abs((float) (((double) num2 - (double) num1) / 2.0));
    float num4;
    if ((double) end - (double) start < -(double) num3)
    {
      float num5 = (num2 - start + end) * value;
      num4 = start + num5;
    }
    else if ((double) end - (double) start > (double) num3)
    {
      float num5 = (float) -((double) num2 - (double) end + (double) start) * value;
      num4 = start + num5;
    }
    else
      num4 = start + (end - start) * value;
    return num4;
  }

  public static float ScaleFloat(float target, float sourceMin, float sourceMax, float resultMin, float resultMax)
  {
    return resultMin + (float) ((double) (target - sourceMin) / ((double) sourceMax - (double) sourceMin) * ((double) resultMax - (double) resultMin));
  }

  public static Vector3 PolarToCartesian(Vector2 polar)
  {
    Vector3 forward = Vector3.forward;
    return Quaternion.Euler(-polar.x, -polar.y, 0.0f) * forward;
  }

  public static Vector3 ClampWithRect(Vector3 vec, Rect rect)
  {
    vec.x = Mathf.Clamp(vec.x, rect.xMin, rect.xMax);
    vec.y = Mathf.Clamp(vec.y, rect.yMin, rect.yMax);
    return vec;
  }

  public static bool IsInRect(Vector3 vec, Rect rect)
  {
    return ((((true ? 1 : 0) & ((double) vec.x < (double) rect.xMin ? 0 : ((double) vec.x <= (double) rect.xMax ? 1 : 0))) != 0 ? 1 : 0) & ((double) vec.y < (double) rect.yMin ? 0 : ((double) vec.y <= (double) rect.yMax ? 1 : 0))) != 0;
  }
}
