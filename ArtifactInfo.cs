﻿// Decompiled with JetBrains decompiler
// Type: ArtifactInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class ArtifactInfo
{
  public const string LIGHT_WAND_EFFECT = "light_wand";
  public const string DARK_WAND_EFFECT = "dark_wand";
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "position")]
  public int position;
  [Config(Name = "delete_type")]
  public string deleteType;
  [Config(Name = "delete_type_param_1")]
  public string deleteTypeParam;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "icon")]
  public string icon;
  [Config(Name = "description")]
  public string description;
  [Config(Name = "active_effect")]
  public string activeEffect;
  [Config(Name = "active_effect_value_1")]
  public float activeEffectValue;
  [Config(Name = "active_effect_value_2")]
  public float activeEffectValue2;
  [Config(Name = "active_cd")]
  public int activeCdTime;
  [Config(Name = "gain_method_param_1")]
  public float gainMethodParam1;
  [Config(Name = "gain_method_param_2")]
  public float gainMethodParam2;
  [Config(Name = "gain_method_param_3")]
  public float gainMethodParam3;
  [Config(Name = "gain_method_description")]
  public string gainMethodDescription;
  [Config(CustomParse = true, CustomType = typeof (Benefits), Name = "Benefits")]
  public Benefits benefits;

  public List<Benefits.BenefitValuePair> GetArtifactBenefits()
  {
    if (this.benefits != null)
      return this.benefits.GetBenefitsPairs();
    return (List<Benefits.BenefitValuePair>) null;
  }

  public string Name
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }

  public string Description
  {
    get
    {
      return Utils.XLAT(this.description);
    }
  }

  public string ImagePath
  {
    get
    {
      return "Texture/Artifact/" + this.icon;
    }
  }

  public string NormalArtifactFramePath
  {
    get
    {
      return "Texture/Equipment/frame_equipment_5";
    }
  }

  public string TimeLimitedArtifactFramePath
  {
    get
    {
      return "Texture/Equipment/frame_equipment_3";
    }
  }

  public string TimeLimitedArtifactFrameIcon
  {
    get
    {
      return "frame_equipment_3";
    }
  }

  public string GetSpecialDescription(params object[] options)
  {
    Dictionary<string, string> para = new Dictionary<string, string>();
    if (options != null)
    {
      for (int index = 0; index < options.Length; ++index)
        para.Add(index.ToString(), options[index].ToString());
    }
    return ScriptLocalization.GetWithPara(!string.IsNullOrEmpty(this.description) ? this.description : string.Empty, para, true);
  }

  public string GainMethodDescription
  {
    get
    {
      return ScriptLocalization.GetWithPara(this.gainMethodDescription, new Dictionary<string, string>()
      {
        {
          "0",
          this.gainMethodParam1.ToString()
        },
        {
          "1",
          this.gainMethodParam2.ToString()
        },
        {
          "2",
          this.gainMethodParam3.ToString()
        }
      }, true);
    }
  }

  public bool IsTimeLimitArtifact
  {
    get
    {
      return this.deleteType == "time";
    }
  }

  public int ArtifactLimitTime
  {
    get
    {
      if (!this.IsTimeLimitArtifact)
        return 0;
      float result = 0.0f;
      float.TryParse(this.deleteTypeParam, out result);
      return (int) (float) ((double) result * 60.0 * 60.0);
    }
  }
}
