﻿// Decompiled with JetBrains decompiler
// Type: WatchtowerArmyInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WatchtowerArmyInfo : MonoBehaviour
{
  private List<GameObject> m_RendererList = new List<GameObject>();
  public GameObject m_Renderer;
  public UITable m_Table;
  public UILabel m_TotalLabel;
  public UILabel m_TotalLabelUnlocked;
  public GameObject m_TroopDetailsLocked;
  public GameObject m_TroopDetailsUnlocked;
  public GameObject m_HeroLocked;
  public GameObject m_HeroUnlocked;
  public GameObject m_HeroExist;
  public GameObject m_HeroNotExist;

  public void SetMarchDetailData(Hashtable data)
  {
    this.m_TotalLabel.text = data[(object) "total_troops_estimate"] as string;
    int watchtowerLevel = WatchtowerUtilities.GetWatchtowerLevel();
    if (watchtowerLevel >= 11)
    {
      this.m_TotalLabel.gameObject.SetActive(true);
      this.m_TotalLabelUnlocked.gameObject.SetActive(false);
    }
    else
    {
      this.m_TotalLabel.gameObject.SetActive(false);
      this.m_TotalLabelUnlocked.gameObject.SetActive(true);
    }
    if (watchtowerLevel >= 13)
    {
      this.m_TroopDetailsLocked.SetActive(false);
      this.m_TroopDetailsUnlocked.SetActive(true);
      this.UpdateTroopList(data);
    }
    else
    {
      this.m_TroopDetailsLocked.SetActive(true);
      this.m_TroopDetailsUnlocked.SetActive(false);
    }
    if (watchtowerLevel >= 19)
    {
      this.m_HeroLocked.SetActive(false);
      this.m_HeroUnlocked.SetActive(true);
      this.UpdateHeroInfo(data);
    }
    else
    {
      this.m_HeroLocked.SetActive(true);
      this.m_HeroUnlocked.SetActive(false);
    }
  }

  private void Clear()
  {
    int count = this.m_RendererList.Count;
    for (int index = 0; index < count; ++index)
    {
      this.m_RendererList[index].transform.parent = (Transform) null;
      Object.Destroy((Object) this.m_RendererList[index]);
    }
    this.m_RendererList.Clear();
  }

  private void UpdateTroopList(Hashtable data)
  {
    this.Clear();
    IDictionaryEnumerator enumerator = (data[(object) "troops"] as Hashtable).GetEnumerator();
    while (enumerator.MoveNext())
    {
      string key = enumerator.Key as string;
      string ammount = enumerator.Value as string;
      GameObject gameObject = Object.Instantiate<GameObject>(this.m_Renderer);
      gameObject.transform.parent = this.m_Table.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localRotation = Quaternion.identity;
      gameObject.transform.localScale = Vector3.one;
      gameObject.GetComponent<WatchtowerToopsItemRenderer>().SetData(key, ammount);
      foreach (UIStretch componentsInChild in gameObject.GetComponentsInChildren<UIStretch>())
        componentsInChild.container = this.m_Table.gameObject;
      this.m_RendererList.Add(gameObject);
    }
    this.m_Table.Reposition();
  }

  private void UpdateHeroInfo(Hashtable data)
  {
    Hashtable hashtable = data[(object) "hero"] as Hashtable;
    bool result = false;
    bool.TryParse(hashtable[(object) "exists"] as string, out result);
    if (result)
    {
      this.m_HeroExist.SetActive(true);
      this.m_HeroNotExist.SetActive(false);
    }
    else
    {
      this.m_HeroExist.SetActive(false);
      this.m_HeroNotExist.SetActive(true);
    }
  }
}
