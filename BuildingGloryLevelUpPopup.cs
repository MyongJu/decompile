﻿// Decompiled with JetBrains decompiler
// Type: BuildingGloryLevelUpPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class BuildingGloryLevelUpPopup : Popup
{
  [SerializeField]
  private UILabel _labelOriginLevel;
  [SerializeField]
  private UILabel _labelNextLevel;
  [SerializeField]
  private UILabel _labelAddedPower;
  private int _originLevel;
  private long _addedPower;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow((UIControler.UIParameter) null);
    BuildingGloryLevelUpPopup.Parameter parameter = orgParam as BuildingGloryLevelUpPopup.Parameter;
    if (parameter != null)
    {
      this._originLevel = parameter.originLevel;
      this._addedPower = parameter.addedPower;
    }
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this._labelOriginLevel.text = this._originLevel.ToString();
    this._labelNextLevel.text = (this._originLevel + 1).ToString();
    this._labelAddedPower.text = string.Format("{0}{1}{2}", (object) Utils.XLAT("id_power"), (object) "+", (object) this._addedPower.ToString());
    Utils.ExecuteInSecs(3f, (System.Action) (() => UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null)));
  }

  public class Parameter : Popup.PopupParameter
  {
    public int originLevel;
    public long addedPower;
  }
}
