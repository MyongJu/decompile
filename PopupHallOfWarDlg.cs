﻿// Decompiled with JetBrains decompiler
// Type: PopupHallOfWarDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PopupHallOfWarDlg : UI.Dialog
{
  private List<HallOfWarSlot> rallySlots = new List<HallOfWarSlot>();
  private List<HallOfWarSlot> defenseSlots = new List<HallOfWarSlot>();
  [SerializeField]
  private PopupHallOfWarDlg.Panel panel;
  private bool isRallyListShowed;

  private void ShowRallys()
  {
    this.isRallyListShowed = true;
    this.ChangeList(true, this.panel.rallyContainer, this.panel.rallyTable, this.panel.noRallyLabel, this.panel.orgRallySlot, this.panel.defenseContainer, this.panel.defenseTalbe, DBManager.inst.DB_Rally.rallys, this.rallySlots);
  }

  private void ShowDefenses()
  {
    this.isRallyListShowed = false;
    this.ChangeList(false, this.panel.defenseContainer, this.panel.defenseTalbe, this.panel.noDefenseLabel, this.panel.orgDefenseSlot, this.panel.rallyContainer, this.panel.rallyTable, DBManager.inst.DB_Rally.defenses, this.defenseSlots);
  }

  private void ChangeList(bool isRally, UIWidget showContainer, UITable showTable, UILabel noInfoLabel, HallOfWarSlot showOrgSlot, UIWidget hideContianer, UITable hideTable, List<long> showDataList, List<HallOfWarSlot> slots)
  {
    this.panel.normalContainer.gameObject.SetActive(true);
    this.panel.JoinAllianceContainer.gameObject.SetActive(false);
    this.panel.TAB_rally.gameObject.SetActive(isRally);
    this.panel.TAB_defense.gameObject.SetActive(!isRally);
    hideTable.gameObject.SetActive(false);
    showTable.gameObject.SetActive(true);
    this.panel.scrollView.ResetPosition();
    if (slots.Count < showDataList.Count)
    {
      for (int count = slots.Count; count < showDataList.Count; ++count)
      {
        GameObject gameObject = NGUITools.AddChild(showTable.gameObject, showOrgSlot.gameObject);
        gameObject.name = string.Format("{0}{1}", count >= 10 ? (object) string.Empty : (object) "0", (object) count.ToString());
        gameObject.SetActive(false);
        slots.Add(gameObject.GetComponent<HallOfWarSlot>());
      }
      showContainer.height = showOrgSlot.GetComponent<UIWidget>().height * showDataList.Count + this.panel.noRallyLabel.GetComponent<UIWidget>().height;
    }
    int index1 = 0;
    int index2 = 0;
    for (int count = showDataList.Count; index2 < count; ++index2)
    {
      DBManager.inst.DB_Rally.Get(showDataList[index2]);
      slots[index1].Setup(showDataList[index2]);
      slots[index1].gameObject.SetActive(true);
      ++index1;
    }
    int index3 = index1;
    for (int count = slots.Count; index3 < count; ++index3)
      slots[index3].gameObject.SetActive(false);
    showContainer.gameObject.SetActive(true);
    hideContianer.gameObject.SetActive(false);
    noInfoLabel.gameObject.SetActive(showDataList.Count == 0);
    this.panel.scrollView.ResetPosition();
    showTable.repositionNow = true;
  }

  public void ClickBackButton()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void ClickWarRallyToggle()
  {
    this.ShowRallys();
  }

  public void ClickWarDefenseToggle()
  {
    this.ShowDefenses();
  }

  public void ClickJoinAlliance()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceJoinDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnEnable()
  {
    DBManager.inst.DB_Rally.onRallyDataChanged += new System.Action<long>(this.OnRallyDataChanged);
  }

  public void OnDisable()
  {
    DBManager.inst.DB_Rally.onRallyDataChanged -= new System.Action<long>(this.OnRallyDataChanged);
  }

  private void OnRallyDataChanged(long rallyId)
  {
    if (rallyId == 0L)
      return;
    if (this.isRallyListShowed)
      this.ShowRallys();
    else
      this.ShowDefenses();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (PlayerData.inst.allianceId == 0L)
    {
      this.panel.JoinAllianceContainer.gameObject.SetActive(true);
      this.panel.normalContainer.gameObject.SetActive(false);
    }
    else
    {
      this.panel.JoinAllianceContainer.gameObject.SetActive(true);
      this.panel.normalContainer.gameObject.SetActive(false);
      this.ShowRallys();
    }
  }

  [Serializable]
  protected class Panel
  {
    public UIScrollView scrollView;
    public Transform normalContainer;
    public UIWidget rallyContainer;
    public UITable rallyTable;
    public UILabel noRallyLabel;
    public HallOfWarSlot orgRallySlot;
    public UIWidget defenseContainer;
    public UITable defenseTalbe;
    public UILabel noDefenseLabel;
    public HallOfWarSlot orgDefenseSlot;
    public Transform TAB_rally;
    public Transform TAB_defense;
    public Transform JoinAllianceContainer;
    public UIButton BT_back;
  }
}
