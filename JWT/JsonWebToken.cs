﻿// Decompiled with JetBrains decompiler
// Type: JWT.JsonWebToken
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace JWT
{
  public static class JsonWebToken
  {
    private static Dictionary<JwtHashAlgorithm, Func<byte[], byte[], byte[]>> HashAlgorithms = new Dictionary<JwtHashAlgorithm, Func<byte[], byte[], byte[]>>()
    {
      {
        JwtHashAlgorithm.HS256,
        (Func<byte[], byte[], byte[]>) ((key, value) =>
        {
          using (HMACSHA256 hmacshA256 = new HMACSHA256(key))
            return hmacshA256.ComputeHash(value);
        })
      },
      {
        JwtHashAlgorithm.HS384,
        (Func<byte[], byte[], byte[]>) ((key, value) =>
        {
          using (HMACSHA384 hmacshA384 = new HMACSHA384(key))
            return hmacshA384.ComputeHash(value);
        })
      },
      {
        JwtHashAlgorithm.HS512,
        (Func<byte[], byte[], byte[]>) ((key, value) =>
        {
          using (HMACSHA512 hmacshA512 = new HMACSHA512(key))
            return hmacshA512.ComputeHash(value);
        })
      }
    };

    public static string Encode(object payload, byte[] key, JwtHashAlgorithm algorithm)
    {
      List<string> stringList = new List<string>();
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      \u003C\u003E__AnonType0<string, string> anonType0 = new \u003C\u003E__AnonType0<string, string>("JWT", algorithm.ToString());
      byte[] bytes1 = Encoding.UTF8.GetBytes(Utils.Object2Json((object) anonType0));
      byte[] bytes2 = Encoding.UTF8.GetBytes(Utils.Object2Json(payload));
      stringList.Add(JsonWebToken.Base64UrlEncode(bytes1));
      stringList.Add(JsonWebToken.Base64UrlEncode(bytes2));
      byte[] bytes3 = Encoding.UTF8.GetBytes(string.Join(".", stringList.ToArray()));
      byte[] input = JsonWebToken.HashAlgorithms[algorithm](key, bytes3);
      stringList.Add(JsonWebToken.Base64UrlEncode(input));
      return string.Join(".", stringList.ToArray());
    }

    public static string Encode(object payload, string key, JwtHashAlgorithm algorithm)
    {
      return JsonWebToken.Encode(payload, Encoding.UTF8.GetBytes(key), algorithm);
    }

    public static string Decode(string token, byte[] key, bool verify = true)
    {
      string[] strArray = token.Split('.');
      string input1 = strArray[0];
      string input2 = strArray[1];
      byte[] inArray1 = JsonWebToken.Base64UrlDecode(strArray[2]);
      Hashtable hashtable = (Hashtable) Utils.Json2Object(Encoding.UTF8.GetString(JsonWebToken.Base64UrlDecode(input1)), true);
      string str = Encoding.UTF8.GetString(JsonWebToken.Base64UrlDecode(input2));
      if (verify)
      {
        byte[] bytes = Encoding.UTF8.GetBytes(input1 + "." + input2);
        string algorithm = (string) hashtable[(object) "alg"];
        byte[] inArray2 = JsonWebToken.HashAlgorithms[JsonWebToken.GetHashAlgorithm(algorithm)](key, bytes);
        string base64String1 = Convert.ToBase64String(inArray1);
        string base64String2 = Convert.ToBase64String(inArray2);
        if (base64String1 != base64String2)
          throw new SignatureVerificationException(string.Format("Invalid signature. Expected {0} got {1}", (object) base64String1, (object) base64String2));
      }
      return str;
    }

    public static string Decode(string token, string key, bool verify = true)
    {
      return JsonWebToken.Decode(token, Encoding.UTF8.GetBytes(key), verify);
    }

    public static object DecodeToObject(string token, string key, bool verify = true)
    {
      return Utils.Json2Object(JsonWebToken.Decode(token, key, verify), true);
    }

    private static JwtHashAlgorithm GetHashAlgorithm(string algorithm)
    {
      string key = algorithm;
      if (key != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (JsonWebToken.\u003C\u003Ef__switch\u0024map93 == null)
        {
          // ISSUE: reference to a compiler-generated field
          JsonWebToken.\u003C\u003Ef__switch\u0024map93 = new Dictionary<string, int>(3)
          {
            {
              "HS256",
              0
            },
            {
              "HS384",
              1
            },
            {
              "HS512",
              2
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (JsonWebToken.\u003C\u003Ef__switch\u0024map93.TryGetValue(key, out num))
        {
          switch (num)
          {
            case 0:
              return JwtHashAlgorithm.HS256;
            case 1:
              return JwtHashAlgorithm.HS384;
            case 2:
              return JwtHashAlgorithm.HS512;
          }
        }
      }
      throw new SignatureVerificationException("Algorithm not supported.");
    }

    public static string Base64UrlEncode(byte[] input)
    {
      return Convert.ToBase64String(input).Split('=')[0].Replace('+', '-').Replace('/', '_');
    }

    public static byte[] Base64UrlDecode(string input)
    {
      string s = input.Replace('-', '+').Replace('_', '/');
      switch (s.Length % 4)
      {
        case 0:
          return Convert.FromBase64String(s);
        case 2:
          s += "==";
          goto case 0;
        case 3:
          s += "=";
          goto case 0;
        default:
          throw new Exception("Illegal base64url string!");
      }
    }
  }
}
