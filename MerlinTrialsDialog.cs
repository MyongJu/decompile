﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTrialsDialog : UI.Dialog
{
  private readonly GameObjectPool _pool = new GameObjectPool();
  private readonly List<MerlinTrialElement> _elements = new List<MerlinTrialElement>();
  public UITexture _textureMerlinCoin;
  public GameObject _passIcon;
  public UILabel _eventName;
  public UILabel _eventCompleteStatus;
  public UILabel _merlinCoinValue;
  public UIGrid _grid;
  public UIScrollView _scrollView;
  public MerlinTrialElement _template;
  private int _curIndex;
  private float _lastPositionX;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DBManager.inst.DB_User.onDataChanged += new System.Action<long>(this.OnUserDataChanged);
    this.UpdateTrialInfo();
    this.UpdateCurrency();
  }

  public override void OnOpen(UIControler.UIParameter orgParam = null)
  {
    base.OnOpen(orgParam);
    this._pool.Initialize(this._template.gameObject, this._grid.gameObject);
    this._template.gameObject.SetActive(false);
    this.InitUI();
    MerlinTrialsHelper.inst.CheckIn();
    MerlinTrialsHelper.inst.RequestMerlinStatus(new System.Action(this.OnRequestCallback));
  }

  public override void OnHide(UIControler.UIParameter orgParam = null)
  {
    base.OnHide(orgParam);
    DBManager.inst.DB_User.onDataChanged -= new System.Action<long>(this.OnUserDataChanged);
  }

  public override void OnClose(UIControler.UIParameter orgParam = null)
  {
    base.OnClose(orgParam);
    int index = 0;
    for (int count = this._elements.Count; index < count; ++index)
      this._elements[index].Dispose();
    this._pool.Clear();
    UIUtils.CleanGrid(this._grid);
  }

  private void OnRequestCallback()
  {
    this._curIndex = MerlinTrialsHelper.inst.Day;
    this.UpdateUI();
    this.FocusItem(this._curIndex);
  }

  private void InitUI()
  {
    for (int index = 0; index < 7; ++index)
      this._elements.Add(this._pool.AddChild(this._grid.gameObject).GetComponent<MerlinTrialElement>());
    this._lastPositionX = 0.0f;
  }

  private void UpdateUI()
  {
    for (int day = 0; day < 7; ++day)
    {
      MerlinTrialElement element = this._elements[day];
      element.gameObject.SetActive(true);
      element.FeedData((IComponentData) new MerlinTrialElementData()
      {
        day = day,
        active = (day == this._curIndex),
        rewardable = MerlinTrialsHelper.inst.IsRewardable(day),
        onSelectHandler = new System.Action<int>(this.OnSelectHandler)
      });
    }
    this._grid.Reposition();
    this.UpdateTrialInfo();
    this.UpdateCurrency();
  }

  private void OnUserDataChanged(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.UpdateCurrency();
  }

  private void UpdateCurrency()
  {
    this._merlinCoinValue.text = PlayerData.inst.userData.MerlinCoin.ToString();
  }

  private void UpdateTrialInfo()
  {
    MerlinTrialsGroupInfo byDay = ConfigManager.inst.DB_MerlinTrialsGroup.GetByDay(this._curIndex);
    if (byDay == null)
      return;
    this._eventName.text = byDay.LocName;
    this._eventCompleteStatus.text = Utils.XLAT("trial_completed_num", (object) "0", (object) MerlinTrialsHelper.inst.GetCompleteNumbers(this._curIndex));
    this._passIcon.SetActive(MerlinTrialsHelper.inst.IsGroupComplete(byDay.internalId));
    BuilderFactory.Instance.HandyBuild((UIWidget) this._textureMerlinCoin, MerlinTrialsPayload.Instance.MerlinCoinImagePath, (System.Action<bool>) null, true, true, string.Empty);
  }

  private void FocusItem(int index)
  {
    float num1 = this._scrollView.panel.width - this._grid.cellWidth;
    float num2 = this._elements[index].transform.localPosition.x - this._scrollView.panel.clipOffset.x;
    if ((double) num2 > 0.0 && (double) num2 < (double) num1)
    {
      this.OnSelectHandler(index);
    }
    else
    {
      float num3 = 0.0f;
      if ((double) num2 >= (double) num1)
        num3 = num2 - num1;
      if ((double) num2 <= 0.0)
        num3 = num2;
      this._scrollView.MoveRelative(new Vector3(-num3, 0.0f, 0.0f));
      this.OnSelectHandler(index);
    }
  }

  private void OnSelectHandler(int index)
  {
    int index1 = 0;
    for (int count = this._elements.Count; index1 < count; ++index1)
      this._elements[index1].SetSelected(false);
    if (index < 0 || index >= this._elements.Count)
      return;
    this._elements[index].SetSelected(true);
    this._curIndex = index;
    this.UpdateTrialInfo();
  }

  public void OnRedeemClick()
  {
    UIManager.inst.OpenDlg("MerlinTrials/MerlinExchangeStoreDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnTotalRankClick()
  {
    UIManager.inst.OpenDlg("MerlinTrials/MerlinTrialsRankDlg", (UI.Dialog.DialogParameter) new MerlinTrialsRankDlg.Parameter()
    {
      rankType = MerlinTrialsRankDlg.RankType.TOTAL
    }, 1 != 0, 1 != 0, 1 != 0);
  }
}
