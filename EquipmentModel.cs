﻿// Decompiled with JetBrains decompiler
// Type: EquipmentModel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EquipmentModel
{
  private Hero_EquipmentData _equipmentData;
  private EquipmentModel.MeshDetail[] _modelInstances;
  private EquipmentModel.MeshDetail[] _leftModelInstance;
  private bool _visible;
  private Transform _parent;
  private Transform _boneRoot;
  private bool _instantiateModel;
  private bool _assignBonesSucceed;
  private bool _isFemale;

  public static EquipmentModel Create(Transform inParent, Hero_EquipmentData inData, bool inFemale)
  {
    if (inData == null)
      return (EquipmentModel) null;
    return new EquipmentModel()
    {
      _equipmentData = inData,
      _parent = inParent,
      _isFemale = inFemale
    };
  }

  private static bool InitMeshDetail(string inPrefabName, Transform inParent, out EquipmentModel.MeshDetail[] outDetail)
  {
    outDetail = (EquipmentModel.MeshDetail[]) null;
    Object original = (Object) null;
    if (original == (Object) null)
      return false;
    GameObject gameObject = Object.Instantiate(original) as GameObject;
    if ((Object) gameObject == (Object) null)
    {
      Debug.LogError((object) string.Format("Object {0} is not a GameObject", (object) original.name));
      Object.Destroy(original);
      return false;
    }
    SkinnedMeshRenderer[] componentsInChildren1 = gameObject.GetComponentsInChildren<SkinnedMeshRenderer>();
    MeshFilter[] componentsInChildren2 = gameObject.GetComponentsInChildren<MeshFilter>();
    if (componentsInChildren1.Length + componentsInChildren2.Length <= 0)
    {
      Object.Destroy((Object) gameObject);
      return false;
    }
    bool flag = false;
    outDetail = new EquipmentModel.MeshDetail[componentsInChildren1.Length + componentsInChildren2.Length];
    for (int index = 0; index < componentsInChildren1.Length; ++index)
    {
      outDetail[index] = new EquipmentModel.MeshDetail(inParent, componentsInChildren1[index]);
      if ((bool) ((Object) outDetail[index].skinInstance) && (Object) outDetail[index].skinInstance.gameObject == (Object) gameObject)
        flag = true;
    }
    for (int index1 = 0; index1 < componentsInChildren2.Length; ++index1)
    {
      int index2 = index1 + componentsInChildren1.Length;
      outDetail[index2] = new EquipmentModel.MeshDetail(inParent, componentsInChildren2[index1]);
      if ((bool) ((Object) outDetail[index2].staticInstance) && (Object) outDetail[index2].staticInstance.gameObject == (Object) gameObject)
        flag = true;
    }
    if (!flag)
      Object.Destroy((Object) gameObject);
    return flag;
  }

  public Hero_EquipmentData EquipmentData
  {
    get
    {
      return this._equipmentData;
    }
  }

  public void Destroy()
  {
    if (this._modelInstances != null)
    {
      for (int index = 0; index < this._modelInstances.Length; ++index)
        this._modelInstances[index].Destroy();
      this._modelInstances = (EquipmentModel.MeshDetail[]) null;
    }
    if (this._leftModelInstance == null)
      return;
    for (int index = 0; index < this._leftModelInstance.Length; ++index)
      this._leftModelInstance[index].Destroy();
    this._leftModelInstance = (EquipmentModel.MeshDetail[]) null;
  }

  public bool AssignBones(Transform inRoot)
  {
    if ((Object) inRoot == (Object) null)
      return false;
    if ((Object) this._boneRoot == (Object) inRoot && this._assignBonesSucceed)
      return true;
    this.InstantiateModel();
    this._assignBonesSucceed = false;
    this._boneRoot = inRoot;
    if (this._modelInstances != null)
    {
      for (int index = 0; index < this._modelInstances.Length; ++index)
      {
        this._assignBonesSucceed = this._modelInstances[index].AssignBones(this._boneRoot);
        if (!this._assignBonesSucceed)
          return this._assignBonesSucceed;
      }
    }
    if (this._leftModelInstance != null)
    {
      for (int index = 0; index < this._leftModelInstance.Length; ++index)
      {
        this._assignBonesSucceed = this._leftModelInstance[index].AssignBones(this._boneRoot);
        if (!this._assignBonesSucceed)
          return this._assignBonesSucceed;
      }
    }
    return this._assignBonesSucceed;
  }

  public void Attach(Transform inParent, Transform inRightHand, Transform inLeftHand, bool inVisible)
  {
  }

  private void InstantiateModel()
  {
  }

  public class MeshDetail
  {
    public string rootBoneName = string.Empty;
    public const string DefaultRootBone = "Bip001";
    public SkinnedMeshRenderer skinInstance;
    public MeshFilter staticInstance;
    public Renderer renderer;
    public string[] boneNames;

    public MeshDetail(Transform inParent, SkinnedMeshRenderer inSkin)
    {
      this.skinInstance = inSkin;
      if (!(bool) ((Object) this.skinInstance))
        return;
      this.skinInstance.transform.parent = inParent;
      this.skinInstance.transform.localPosition = Vector3.zero;
      this.skinInstance.transform.localRotation = Quaternion.identity;
      this.skinInstance.transform.localScale = Vector3.one;
      this.rootBoneName = !((Object) this.skinInstance.rootBone == (Object) null) ? this.skinInstance.rootBone.name : "Bip001";
      if (this.skinInstance.bones.Length > 0)
      {
        this.boneNames = new string[this.skinInstance.bones.Length];
        for (int index = 0; index < this.boneNames.Length; ++index)
        {
          if ((Object) this.skinInstance.bones[index] != (Object) null)
            this.boneNames[index] = this.skinInstance.bones[index].name;
        }
      }
      this.skinInstance.gameObject.SetActive(false);
      this.renderer = (Renderer) this.skinInstance;
    }

    public MeshDetail(Transform inParent, MeshFilter inMeshFilter)
    {
      this.staticInstance = inMeshFilter;
      if (!(bool) ((Object) this.staticInstance))
        return;
      this.staticInstance.transform.parent = inParent;
      this.staticInstance.transform.localPosition = Vector3.zero;
      this.staticInstance.transform.localRotation = Quaternion.identity;
      this.staticInstance.transform.localScale = Vector3.one;
      this.staticInstance.gameObject.SetActive(false);
      this.renderer = this.staticInstance.GetComponent<Renderer>();
    }

    public void Destroy()
    {
      if ((bool) ((Object) this.skinInstance))
      {
        Object.Destroy((Object) this.skinInstance.gameObject);
      }
      else
      {
        if (!(bool) ((Object) this.staticInstance))
          return;
        Object.Destroy((Object) this.staticInstance.gameObject);
      }
    }

    public bool AssignBones(Transform inRoot)
    {
      if ((Object) this.skinInstance == (Object) null)
        return true;
      if ((Object) inRoot == (Object) null)
        return false;
      Transform inRoot1 = this.rootBoneName.Length <= 0 ? inRoot : Utils.SearchHierarchyForBone(inRoot, this.rootBoneName, false);
      if ((Object) inRoot1 == (Object) null)
      {
        D.error((object) ("Object: " + this.skinInstance.name + " can not find the root bone: " + this.rootBoneName + " in hierarchy: " + inRoot.name));
        return false;
      }
      Transform[] transformArray = new Transform[this.boneNames.Length];
      for (int index = 0; index < transformArray.Length; ++index)
      {
        if (this.boneNames[index] != null && this.boneNames[index].Length > 0)
        {
          transformArray[index] = Utils.SearchHierarchyForBone(inRoot1, this.boneNames[index], false);
          if ((Object) transformArray[index] == (Object) null)
          {
            D.error((object) ("Object: " + this.skinInstance.name + " can not find the bone: " + this.boneNames[index] + " in hierarchy: " + inRoot1.name));
            return false;
          }
        }
      }
      this.skinInstance.rootBone = inRoot1;
      this.skinInstance.bones = transformArray;
      return true;
    }

    public void Attach(Transform inParent, bool inVisible)
    {
      if ((bool) ((Object) this.skinInstance))
      {
        this.skinInstance.gameObject.SetActive(inVisible);
        this.skinInstance.transform.parent = inParent;
        if ((Object) inParent != (Object) null)
          this.skinInstance.gameObject.layer = inParent.gameObject.layer;
        this.skinInstance.transform.localPosition = Vector3.zero;
        this.skinInstance.transform.localRotation = Quaternion.identity;
        this.skinInstance.transform.localScale = Vector3.one;
      }
      if (!(bool) ((Object) this.staticInstance))
        return;
      this.staticInstance.gameObject.SetActive(inVisible);
      this.staticInstance.transform.parent = inParent;
      if ((Object) inParent != (Object) null)
        this.staticInstance.gameObject.layer = inParent.gameObject.layer;
      this.staticInstance.transform.localPosition = Vector3.zero;
      this.staticInstance.transform.localRotation = Quaternion.identity;
      this.staticInstance.transform.localScale = Vector3.one;
    }
  }
}
