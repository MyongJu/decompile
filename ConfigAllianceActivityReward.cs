﻿// Decompiled with JetBrains decompiler
// Type: ConfigAllianceActivityReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAllianceActivityReward
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, AllianceActivityReward> datas;
  private Dictionary<int, AllianceActivityReward> dicByUniqueId;
  private Dictionary<int, AllianceActivityReward> dicByMainInternalID;
  private Dictionary<string, AllianceActivityReward> rewardInfoMap;

  public void BuildDB(object res)
  {
    this.parse.Parse<AllianceActivityReward, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public AllianceActivityReward GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (AllianceActivityReward) null;
  }

  public AllianceActivityReward GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AllianceActivityReward) null;
  }

  public AllianceActivityReward GetDataByMainInternalID(int internalID)
  {
    if (this.dicByMainInternalID == null)
    {
      using (Dictionary<int, AllianceActivityReward>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, AllianceActivityReward> current = enumerator.Current;
          this.dicByMainInternalID.Add(current.Value.ActivityMainSubInfo_ID, current.Value);
        }
      }
    }
    return this.dicByMainInternalID[internalID];
  }

  public AllianceActivityReward GetCurrentStepReward(int id, int lev)
  {
    AllianceActivitySubInfo data = ConfigManager.inst.DB_AllianceActivityMainSub.GetData(id);
    Dictionary<int, AllianceActivityReward>.ValueCollection.Enumerator enumerator = this.dicByUniqueId.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && enumerator.Current.ActivityMainSubInfo_ID == data.internalId && enumerator.Current.AllianceLev == lev)
        return enumerator.Current;
    }
    return (AllianceActivityReward) null;
  }

  public AllianceActivityReward GetDataByID(string id)
  {
    if (this.rewardInfoMap == null)
    {
      this.rewardInfoMap = new Dictionary<string, AllianceActivityReward>();
      using (Dictionary<int, AllianceActivityReward>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, AllianceActivityReward> current = enumerator.Current;
          this.rewardInfoMap.Add(current.Value.ID, current.Value);
        }
      }
    }
    return this.rewardInfoMap[id];
  }
}
