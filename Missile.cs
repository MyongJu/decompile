﻿// Decompiled with JetBrains decompiler
// Type: Missile
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Missile : MonoBehaviour
{
  public float heightScaleFactor = 1f;
  private float _scaleFactor = 1f;
  private Vector3 _source = Vector3.zero;
  private Vector3 _target = Vector3.zero;
  public MissileSystem system;
  public Animator normlizedAnimation;
  public int tag;
  private GameObject missileAsset;
  private Renderer _missileRednerer;
  private SpriteAnimator _missileAnimation;

  public void PrepareShoot(Vector3 source, Vector3 target)
  {
    this.gameObject.SetActive(false);
    this._source = source;
    this._target = target;
    Vector3 vector3 = this._target - this._source;
    Vector3 worldUp = this.system.OffsetVector3(Vector3.up);
    float assetScaleFactor = this.system.assetScaleFactor;
    if ((double) assetScaleFactor != 0.0)
      this._scaleFactor = (float) ((double) vector3.magnitude * (double) Mathf.Cos(0.5235988f) / (double) assetScaleFactor * 1.10000002384186);
    this.transform.position = source;
    this.transform.LookAt(target, worldUp);
    this.transform.localScale = new Vector3(assetScaleFactor, assetScaleFactor, assetScaleFactor);
  }

  public void OnStartShoot()
  {
    this.gameObject.SetActive(true);
    this.missileAsset = this.system.CreateMissileAsset();
    this.missileAsset.transform.parent = this.transform;
    this.missileAsset.transform.localPosition = Vector3.zero;
    this.missileAsset.transform.localRotation = Quaternion.identity;
    this.missileAsset.transform.localScale = Vector3.one;
    this._missileRednerer = this.missileAsset.GetComponent<Renderer>();
    this._missileRednerer.sortingLayerName = this.system.assetSortingLayer;
    this._missileRednerer.sortingOrder = this.system.assetSortingOrder;
    this._missileAnimation = this.missileAsset.GetComponent<SpriteAnimator>();
  }

  public void UpdateAnimation()
  {
    Vector3 vector3 = this.normlizedAnimation.transform.localPosition * this._scaleFactor;
    vector3.y *= this.heightScaleFactor;
    this.missileAsset.transform.localPosition = vector3;
    if (this.system.faceToCamera)
      this.missileAsset.transform.rotation = Quaternion.identity;
    else
      this.missileAsset.transform.localRotation = this.normlizedAnimation.transform.localRotation;
    this.missileAsset.transform.localScale = this.normlizedAnimation.transform.localScale;
  }

  public void PlayAnimation(string animationName, float time = 0)
  {
    if (string.IsNullOrEmpty(animationName) || (Object) this._missileAnimation == (Object) null)
      return;
    this._missileAnimation.ForcePlay(animationName);
  }

  public void Clear()
  {
    this._missileAnimation = (SpriteAnimator) null;
    this._missileRednerer = (Renderer) null;
    this.system.DestoryMissileAsset(this.missileAsset);
    this.missileAsset = (GameObject) null;
    this._source = Vector3.zero;
    this._target = Vector3.zero;
    this.tag = 0;
    this.transform.parent = (Transform) null;
    this.normlizedAnimation = (Animator) null;
    this.system = (MissileSystem) null;
  }
}
