﻿// Decompiled with JetBrains decompiler
// Type: LogPipeline
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class LogPipeline
{
  public const string FileFolder = "/log_pipeline";
  public static List<LogPipeline.LogItem> logs;
  private static StreamWriter fileWrite;
  private static StringBuilder consoleWrite;
  private static LogPipeline.LogTarget _target;

  public static bool IsFileMode
  {
    get
    {
      return (LogPipeline._target & LogPipeline.LogTarget.File) > LogPipeline.LogTarget.None;
    }
  }

  public static bool IsConsoleMode
  {
    get
    {
      return (LogPipeline._target & LogPipeline.LogTarget.Console) > LogPipeline.LogTarget.None;
    }
  }

  public static void Startup(LogPipeline.LogTarget target)
  {
    LogPipeline._target = target;
    if (LogPipeline.IsConsoleMode)
      LogPipeline.logs = new List<LogPipeline.LogItem>();
    if (!LogPipeline.IsFileMode)
      return;
    string filePath = LogPipeline.GenerateFilePath();
    try
    {
      LogPipeline.fileWrite = new StreamWriter(filePath);
    }
    catch
    {
      LogPipeline._target &= ~LogPipeline.LogTarget.File;
    }
  }

  public static void Terminate()
  {
    if (LogPipeline.IsFileMode && LogPipeline.fileWrite != null)
    {
      LogPipeline.fileWrite.Close();
      LogPipeline.fileWrite = (StreamWriter) null;
    }
    if (LogPipeline.IsConsoleMode)
      LogPipeline.logs = (List<LogPipeline.LogItem>) null;
    LogPipeline._target = LogPipeline.LogTarget.None;
  }

  public static void Log(string logString, string stackTrace, LogType type)
  {
    if (LogPipeline.IsFileMode)
    {
      LogPipeline.fileWrite.WriteLine(LogPipeline.LogFormat(logString, stackTrace, type));
      LogPipeline.fileWrite.Flush();
    }
    if (!LogPipeline.IsConsoleMode)
      return;
    LogPipeline.logs.Add(new LogPipeline.LogItem(logString, stackTrace, type));
  }

  public static string LogFormat(string logString, string stackTrace, LogType type)
  {
    if (type == LogType.Exception)
      return string.Format("[{0}]{1}\n{2}", (object) type, (object) logString, (object) stackTrace);
    return string.Format("[{0}]{1}", (object) type, (object) logString);
  }

  public static string GenerateFileName()
  {
    return string.Format("dw_{0}.log", (object) DateTime.Now.ToString("yyyy_M_d_HHmmss"));
  }

  public static string GenerateFilePath()
  {
    return string.Format("{0}/{1}", (object) LogPipeline.GenerateFolder(), (object) LogPipeline.GenerateFileName());
  }

  public static string GenerateFolder()
  {
    string path = Application.persistentDataPath + "/log_pipeline";
    try
    {
      if (!Directory.Exists(path))
        Directory.CreateDirectory(path);
      return path;
    }
    catch
    {
      D.error((object) "[Log Pipeline]Create log folder error.");
    }
    return Application.persistentDataPath;
  }

  public class LogItem
  {
    public string logTitle;
    public string logString;
    public string stackTrace;
    public LogType type;
    public bool isExpand;

    public LogItem(string logString, string stackTrace, LogType type)
    {
      this.logTitle = logString.Length <= 100 ? logString : logString.Substring(0, 100);
      this.logString = logString;
      this.stackTrace = stackTrace;
      this.type = type;
      this.isExpand = false;
    }
  }

  [Flags]
  public enum LogTarget
  {
    None = 0,
    Console = 1,
    File = 2,
    All = File | Console, // 0x00000003
  }
}
