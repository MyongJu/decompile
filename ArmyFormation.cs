﻿// Decompiled with JetBrains decompiler
// Type: ArmyFormation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class ArmyFormation : MonoBehaviour
{
  private Vector2 _dir2 = new Vector2();
  private string[] TroopViewPath = new string[8]
  {
    "Prefab/TroopModels/Infantry",
    "Prefab/TroopModels/Archer",
    "Prefab/TroopModels/Dragon",
    "Prefab/TroopModels/Knight",
    "Prefab/TroopModels/Catapult",
    "Prefab/TroopModels/Fallen_knight_1",
    "Prefab/TroopModels/Fallen_knight_2",
    "Prefab/TroopModels/Fallen_knight_3"
  };
  private ArmyUnitView[] _unitViews;
  private Vector3 _dir;
  private ArmyView.LODLEVEL curLod;

  public float deltaPos { private set; get; }

  public float unitsLength { private set; get; }

  public MarchViewControler.TroopViewType type { get; private set; }

  public ArmyFormationData formationData { get; private set; }

  public Vector3 dir
  {
    get
    {
      return this._dir;
    }
    private set
    {
      this._dir = value.normalized;
      MarchViewControler.CalcVector3(this.dir, ref this._dir2);
    }
  }

  public void Show()
  {
    if (this._unitViews == null)
      return;
    for (int index = 0; index < this._unitViews.Length; ++index)
    {
      if ((bool) ((UnityEngine.Object) this._unitViews[index]) && this._unitViews[index].gameObject.activeSelf)
        this._unitViews[index].Show(0.0f);
    }
  }

  public void Hide()
  {
    if (this._unitViews == null)
      return;
    for (int index = 0; index < this._unitViews.Length; ++index)
    {
      if ((bool) ((UnityEngine.Object) this._unitViews[index]) && this._unitViews[index].gameObject.activeSelf)
        this._unitViews[index].Hide(0.0f);
    }
  }

  public void FadeIn(float ArmySpeed, Vector3 showPosition, bool isDisplayByLine = true)
  {
    if (this.formationData == null || this._unitViews == null)
      return;
    for (int index = 0; index < this._unitViews.Length; ++index)
    {
      if ((bool) ((UnityEngine.Object) this._unitViews[index]) && this._unitViews[index].gameObject.activeSelf)
      {
        if (isDisplayByLine)
          this._unitViews[index].Show(showPosition);
        else
          this._unitViews[index].Show(0.0f);
      }
    }
  }

  public void FadeOut(float ArmySpeed, bool isDisplayByLine = true)
  {
    for (int index1 = 0; index1 < this.formationData.curUnitFormation_x; ++index1)
    {
      for (int index2 = 0; index2 < this.formationData.curUnitFormation_y; ++index2)
      {
        if (this._unitViews[index1 * this.formationData.curUnitFormation_y + index2].gameObject.activeSelf)
          this._unitViews[index1 * this.formationData.curUnitFormation_y + index2].Hide(!isDisplayByLine || (double) ArmySpeed <= 0.0 ? 0.0f : this.deltaPos / ArmySpeed);
      }
    }
  }

  public float SetUnits(ArmyFormation.Param param, System.Action callback = null)
  {
    param.Show();
    this.deltaPos = param.deltaPos;
    this.formationData = this._CreateArmyFormationData(param.type);
    this.unitsLength = this.formationData.SetData(param.type, param.troopsCount);
    this.type = param.type;
    if (this._unitViews == null)
    {
      this._unitViews = new ArmyUnitView[this.formationData.maxUnitCount];
      this.GetUnitModelOrg(param.type, (System.Action<UnityEngine.Object>) (orgObject =>
      {
        ArmyUnitView.Param obj = new ArmyUnitView.Param();
        obj.parentTrans = this.transform;
        if (this.type == MarchViewControler.TroopViewType.class_dragon)
        {
          obj.dragonLevel = param.dragonLevel;
          obj.dragonTendency = param.dragonTendency;
        }
        for (int index = 0; index < this.formationData.maxUnitCount; ++index)
        {
          GameObject gameObject = UnityEngine.Object.Instantiate(orgObject) as GameObject;
          gameObject.transform.parent = this.transform;
          gameObject.transform.localPosition = Vector3.zero;
          gameObject.name = string.Format("troop_{0}_{1}", (object) param.type.ToString(), (object) (100 + index));
          this._unitViews[index] = gameObject.GetComponent<ArmyUnitView>();
          this._unitViews[index].Setup(this.formationData, obj);
        }
        for (int index = 0; index < this.formationData.maxUnitCount; ++index)
          this._unitViews[index].gameObject.SetActive(index < this.formationData.curUnitCount);
        if (callback == null)
          return;
        callback();
      }));
    }
    else
    {
      for (int index = 0; index < this.formationData.maxUnitCount; ++index)
      {
        if ((UnityEngine.Object) null != (UnityEngine.Object) this._unitViews[index])
          this._unitViews[index].gameObject.SetActive(index < this.formationData.curUnitCount);
      }
      if (callback != null)
        callback();
    }
    return this.unitsLength;
  }

  public void ClearUnitsImd(MarchViewControler.TroopViewType inType)
  {
    this.type = inType;
    double num = (double) this.SetUnits(new ArmyFormation.Param()
    {
      type = inType,
      troopsCount = 0,
      deltaPos = 0.0f
    }, (System.Action) null);
  }

  private ArmyFormationData _CreateArmyFormationData(MarchViewControler.TroopViewType inType)
  {
    switch (inType)
    {
      case MarchViewControler.TroopViewType.class_infantry:
        return (ArmyFormationData) new ArmyFormationData_Infantry();
      case MarchViewControler.TroopViewType.class_ranged:
        return (ArmyFormationData) new ArmyFormationData_Ranged();
      case MarchViewControler.TroopViewType.class_dragon:
        return (ArmyFormationData) new ArmyFormationData_Dragon();
      case MarchViewControler.TroopViewType.class_cavalry:
        return (ArmyFormationData) new ArmyFormationData_Cavalry();
      case MarchViewControler.TroopViewType.class_artyfar:
        return (ArmyFormationData) new ArmyFormationData_ArtyFar();
      case MarchViewControler.TroopViewType.class_darkKnight_1:
      case MarchViewControler.TroopViewType.class_darkKnight_2:
      case MarchViewControler.TroopViewType.class_darkKnight_3:
        return (ArmyFormationData) new ArmyFormationData_DarkKnight();
      default:
        return new ArmyFormationData();
    }
  }

  private void GetUnitModelOrg(MarchViewControler.TroopViewType type, System.Action<UnityEngine.Object> callback = null)
  {
    switch (type)
    {
      case MarchViewControler.TroopViewType.class_infantry:
      case MarchViewControler.TroopViewType.class_ranged:
      case MarchViewControler.TroopViewType.class_dragon:
      case MarchViewControler.TroopViewType.class_cavalry:
      case MarchViewControler.TroopViewType.class_artyfar:
      case MarchViewControler.TroopViewType.class_darkKnight_1:
      case MarchViewControler.TroopViewType.class_darkKnight_2:
      case MarchViewControler.TroopViewType.class_darkKnight_3:
        AssetManager.Instance.LoadAsync(this.TroopViewPath[(int) type], (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
        {
          if (ret && callback != null && (UnityEngine.Object) null != (UnityEngine.Object) this)
            callback(obj);
          AssetManager.Instance.UnLoadAsset(this.TroopViewPath[(int) type], (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
        }), (System.Type) null);
        break;
      default:
        AssetManager.Instance.LoadAsync(this.TroopViewPath[0], (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
        {
          if (ret && callback != null && (UnityEngine.Object) null != (UnityEngine.Object) this)
            callback(obj);
          AssetManager.Instance.UnLoadAsset(this.TroopViewPath[0], (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
        }), (System.Type) null);
        break;
    }
  }

  public void Rotate(Vector3 inDir, bool setImd)
  {
    if (this._unitViews == null)
      return;
    this.dir = inDir;
    Vector3 vector3_1 = inDir;
    vector3_1.z = 0.0f;
    vector3_1 = vector3_1.normalized;
    Vector3 vector3_2 = new Vector3();
    vector3_2.x = vector3_1.y;
    vector3_2.y = -vector3_1.x;
    float num1 = this.formationData.formationHalfWidth / (float) this.formationData.curUnitFormation_y;
    int index1 = 0;
    Vector3 zero = Vector3.zero;
    for (int index2 = 0; index2 < this.formationData.curUnitFormation_x; ++index2)
    {
      int num2 = -this.formationData.curUnitFormation_y + 1;
      while (num2 < this.formationData.curUnitFormation_y)
      {
        if ((bool) ((UnityEngine.Object) this._unitViews[index1]) && this._unitViews[index1].gameObject.activeSelf)
        {
          Vector3 pos = (float) -((double) this.deltaPos + (double) this.formationData.unitHeadLineStep + (double) index2 * (double) this.formationData.unitLineStep) * vector3_1 + (float) num2 * num1 * vector3_2;
          this.SetUnitModel(this._unitViews[index1], inDir, pos, setImd);
        }
        num2 += 2;
        ++index1;
      }
    }
    int[] numArray = new int[this.formationData.curUnitCount];
    for (int index2 = 0; index2 < this.formationData.curUnitCount; ++index2)
      numArray[index2] = index2;
    for (int index2 = 0; index2 < numArray.Length; ++index2)
    {
      for (int index3 = index2; index3 < numArray.Length; ++index3)
      {
        if (!((UnityEngine.Object) null == (UnityEngine.Object) this._unitViews[numArray[index3]]) && !((UnityEngine.Object) null == (UnityEngine.Object) this._unitViews[numArray[index2]]) && ((double) this._unitViews[numArray[index3]].transform.localPosition.y > (double) this._unitViews[numArray[index2]].transform.localPosition.y || (double) this._unitViews[numArray[index3]].transform.localPosition.y == (double) this._unitViews[numArray[index2]].transform.localPosition.y && (double) this._unitViews[numArray[index3]].transform.localPosition.x > (double) this._unitViews[numArray[index2]].transform.localPosition.x))
        {
          int num2 = numArray[index2];
          numArray[index2] = numArray[index3];
          numArray[index3] = num2;
        }
      }
    }
    for (int index2 = 0; index2 < this.formationData.curUnitCount; ++index2)
    {
      if ((UnityEngine.Object) null != (UnityEngine.Object) this._unitViews[numArray[index2]])
        this._unitViews[numArray[index2]].SetRenderer(index2 + 29999, "March");
    }
  }

  private void SetUnitModel(ArmyUnitView unitView, Vector3 dir, Vector3 pos, bool setImd = true)
  {
    unitView.transform.localScale = new Vector3(this.formationData.unitDefaultScale / this.transform.lossyScale.x, this.formationData.unitDefaultScale / this.transform.lossyScale.y, this.formationData.unitDefaultScale / this.transform.lossyScale.z);
    unitView.SetTo(pos);
    unitView.Rotate(dir);
  }

  public void Move()
  {
    if (this._unitViews == null)
      return;
    for (int index = 0; index < this.formationData.curUnitCount; ++index)
    {
      if ((bool) ((UnityEngine.Object) this._unitViews[index]) && this._unitViews[index].gameObject.activeSelf)
        this._unitViews[index].Move();
    }
  }

  public void Attack(Vector3 deltaPos, float radius)
  {
    Vector3[] vector3Array = this.formationData.CalcAttackFormation(deltaPos, radius, this.dir);
    for (int index = 0; index < this.formationData.curUnitCount; ++index)
    {
      if ((UnityEngine.Object) null != (UnityEngine.Object) this._unitViews[index])
        this._unitViews[index].Attack(vector3Array[index], deltaPos);
    }
  }

  public void Action(int actionIndex, Vector3 detalPos, float radius)
  {
    switch (actionIndex)
    {
      case 0:
        this.Attack(detalPos, radius);
        break;
      case 1:
        this.Hide();
        break;
    }
  }

  public void Dispose()
  {
    if (this._unitViews == null)
      return;
    for (int index = 0; index < this._unitViews.Length; ++index)
    {
      if ((UnityEngine.Object) null != (UnityEngine.Object) this._unitViews[index])
        this._unitViews[index].Dispose();
    }
  }

  public void SetLOD(ArmyView.LODLEVEL lod)
  {
    if (this.curLod >= lod)
      return;
    this.curLod = lod;
    if (this._unitViews == null)
      return;
    for (int index = 0; index < this._unitViews.Length; ++index)
    {
      if ((UnityEngine.Object) null != (UnityEngine.Object) this._unitViews[index] && (bool) ((UnityEngine.Object) this._unitViews[index].gameObject) && this._unitViews[index].gameObject.activeSelf)
        this._unitViews[index].SetLOD(lod);
    }
  }

  public class Param
  {
    public bool setImd = true;
    public MarchViewControler.TroopViewType type;
    public int troopsCount;
    public float deltaPos;
    public DragonData.Tendency dragonTendency;
    public int dragonLevel;

    public void Show()
    {
    }
  }
}
