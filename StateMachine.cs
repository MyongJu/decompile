﻿// Decompiled with JetBrains decompiler
// Type: StateMachine
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class StateMachine
{
  public bool Locked;
  private IState _current;
  private Dictionary<string, IState> _states;

  public StateMachine()
  {
    this._current = (IState) null;
    this._states = new Dictionary<string, IState>();
  }

  public void Process()
  {
    if (this._current == null)
      return;
    this._current.OnProcess();
  }

  public void Dispose()
  {
    if (this._current != null)
      this._current.OnExit();
    if (this._states != null)
    {
      using (Dictionary<string, IState>.Enumerator enumerator = this._states.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.Value.Dispose();
      }
      this._states.Clear();
      this._states = (Dictionary<string, IState>) null;
    }
    this._current = (IState) null;
    this.Locked = false;
  }

  public void AddState(IState state)
  {
    if (state == null || this._states.ContainsKey(state.Key))
      return;
    this._states.Add(state.Key, state);
  }

  public void SetState(IState state, Hashtable data = null)
  {
    if (state == null)
      return;
    this.SetState(state.Key, data);
  }

  public void SetState(string key, Hashtable data = null)
  {
    if (this.Locked || !this._states.ContainsKey(key))
      return;
    IState state = this._states[key];
    if (this._current != null)
    {
      if (this._current.Key == key)
        return;
      this._current.OnExit();
    }
    this._current = state;
    if (data != null)
      this._current.Data = data;
    this._current.OnEnter();
  }

  public void ResetState(IState state, Hashtable data)
  {
    if (state == null)
      return;
    this.ResetState(state.Key, data);
  }

  public void ResetState(string key, Hashtable data)
  {
    if (this.Locked || !this._states.ContainsKey(key))
      return;
    IState state = this._states[key];
    if (state == null)
      return;
    state.Data = data;
  }

  public IState CurrState
  {
    get
    {
      return this._current;
    }
  }
}
