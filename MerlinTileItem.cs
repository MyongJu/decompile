﻿// Decompiled with JetBrains decompiler
// Type: MerlinTileItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTileItem : MonoBehaviour
{
  private static Color playerColor = new Color(0.0f, 0.8156863f, 0.02745098f, 1f);
  private static Color enemyColor = new Color(0.7921569f, 0.145098f, 0.145098f, 1f);
  private static Color allyColor = Utils.GetColorFromHex(44543U);
  private string _previousImagePath = string.Empty;
  [SerializeField]
  private UITexture _textureMine;
  [SerializeField]
  private UILabel _labelLevel;
  [SerializeField]
  private GameObject _rootState;
  [SerializeField]
  private UISprite _stateColor;
  [SerializeField]
  private GameObject _rootTagEnemy;
  [SerializeField]
  private GameObject _rootMenu;
  [SerializeField]
  private GameObject _rootMenuCaseFriend;
  [SerializeField]
  private GameObject _rootMenuCaseEnemy;
  [SerializeField]
  private GameObject _rootMenuCaseEmpty;
  [SerializeField]
  private GameObject _rootMenuOccupy;
  [SerializeField]
  private UILabel _labelPlayerName;
  [SerializeField]
  private UILabel _labelGatherInfo;
  [SerializeField]
  private UILabel _labelOccupy;
  private MerlinMineData _mineData;
  private System.Action<MerlinTileItem> _clickedCallback;
  private int _index;
  private int _page;

  public int Index
  {
    get
    {
      return this._index;
    }
  }

  public int Page
  {
    get
    {
      return this._page;
    }
  }

  protected void UpdateLeftGatherTime()
  {
    if (this._mineData == null)
      return;
    this._labelGatherInfo.text = ScriptLocalization.GetWithPara("tower_mine_gathering_time_status", new Dictionary<string, string>()
    {
      {
        "0",
        Utils.FormatTime(this._mineData.end_time - NetServerTime.inst.ServerTimestamp, false, false, true)
      }
    }, 1 != 0);
  }

  public void OnSecondTick()
  {
    this.UpdateLeftGatherTime();
  }

  public MerlinMineData MineData
  {
    get
    {
      return this._mineData;
    }
  }

  public void SetData(MerlinMineData mineData, int page, int index, System.Action<MerlinTileItem> clickedCallback)
  {
    this.CloseCircleMenu();
    this._page = page;
    this._index = index;
    this._mineData = mineData;
    this._clickedCallback = clickedCallback;
    MagicTowerMainInfo currentTowerMainInfo = MerlinTowerPayload.Instance.UserData.CurrentTowerMainInfo;
    if (currentTowerMainInfo != null)
    {
      this._labelLevel.text = currentTowerMainInfo.MineLevel.ToString();
      if (this._previousImagePath != currentTowerMainInfo.MineImagePath)
      {
        BuilderFactory.Instance.HandyBuild((UIWidget) this._textureMine, currentTowerMainInfo.MineImagePath, (System.Action<bool>) null, true, false, string.Empty);
        this._previousImagePath = currentTowerMainInfo.MineImagePath;
      }
    }
    this._rootMenuCaseEmpty.SetActive(false);
    this._rootMenuCaseFriend.SetActive(false);
    this._rootMenuCaseEnemy.SetActive(false);
    if (this._mineData != null)
    {
      this._rootState.SetActive(true);
      this._rootTagEnemy.SetActive(false);
      this._rootMenuOccupy.SetActive(true);
      if (PlayerData.inst.uid == mineData.uid)
        this._stateColor.color = MerlinTileItem.playerColor;
      else if (PlayerData.inst.allianceId != 0L && PlayerData.inst.allianceId == this._mineData.alliance_id)
      {
        this._stateColor.color = MerlinTileItem.allyColor;
        this._rootMenuCaseFriend.SetActive(true);
      }
      else
      {
        this._stateColor.color = MerlinTileItem.enemyColor;
        this._rootTagEnemy.SetActive(true);
        this._rootMenuCaseEnemy.SetActive(true);
      }
      string str = string.Empty;
      if (!string.IsNullOrEmpty(mineData.tag))
        str = string.Format("({0})", (object) mineData.tag);
      this._labelPlayerName.text = mineData.world_id == PlayerData.inst.userData.world_id ? string.Format("{0}{1}.K{2}", (object) str, (object) mineData.name, (object) mineData.world_id) : string.Format("[ff0000]{0}{1}.K{2}[-]", (object) str, (object) mineData.name, (object) mineData.world_id);
      this.UpdateLeftGatherTime();
    }
    else
    {
      this._rootState.SetActive(false);
      this._rootMenuCaseEmpty.SetActive(true);
      this._rootMenuOccupy.SetActive(false);
      this._labelOccupy.text = Utils.XLAT("kingdom_tile_occupy");
      this._labelGatherInfo.text = ScriptLocalization.Get("tower_mine_start_gathering_status", true) + Utils.FormatTime(currentTowerMainInfo.GratherTime, false, false, true);
    }
  }

  public void OnClicked()
  {
    AudioManager.Instance.StopAndPlaySound("sfx_rural_click_mine");
    Logger.Log(nameof (OnClicked));
    if (this._clickedCallback == null)
      return;
    this._clickedCallback(this);
  }

  public void ShowCircleMenu()
  {
    if (this._mineData != null && this._mineData.uid == PlayerData.inst.uid)
      return;
    this._rootMenu.gameObject.SetActive(true);
    this._rootMenu.transform.localScale = Vector3.zero;
    iTween.ScaleTo(this._rootMenu.gameObject, Vector3.one, 0.1f);
  }

  public void CloseCircleMenu()
  {
    this._rootMenu.gameObject.SetActive(false);
  }

  public void OnButtonViewClicked()
  {
    Logger.Log(nameof (OnButtonViewClicked));
    this.CloseCircleMenu();
    if (this._mineData == null)
      return;
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
    {
      uid = this._mineData.uid
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnButtonOccupyClicked()
  {
    Logger.Log(nameof (OnButtonOccupyClicked));
    if (MerlinTowerPayload.Instance.UserData.mineJobId != 0L)
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_mine_occupy_only_one", true), (System.Action) null, 4f, true);
    }
    else
    {
      this.CloseCircleMenu();
      UIManager.inst.OpenDlg("MerlinTower/MerlinTowerOreMarchAllocDlg", (UI.Dialog.DialogParameter) new MerlinTowerOreMarchAllocDlg.Parameter()
      {
        desTroopCount = 1000,
        maxTime = 10000,
        remainTime = (NetServerTime.inst.ServerTimestamp + 360000),
        speed = 100,
        isRally = false,
        marchType = MarchAllocDlg.MarchType.gather,
        confirmCallback = (System.Action<Hashtable, bool>) ((troops, withDragon) => UIManager.inst.OpenIgnorableMessageBox(new IgnorableMessageBoxPopup.Parameter()
        {
          key = "MerlinTower_Occupy",
          titleString = Utils.XLAT("tower_crystal_mines_occupy_confirm_name"),
          contentString = ScriptLocalization.Get("tower_mine_gathering_no_reset_description", true),
          leftLabelString = Utils.XLAT("id_uppercase_confirm"),
          rightLabelString = Utils.XLAT("id_uppercase_cancel"),
          onLeftBtnClick = (System.Action) (() => MerlinTowerPayload.Instance.SendGatherRequest(troops, withDragon, this.Page, this.Index)),
          inverseButtonPosition = true
        }))
      }, true, true, true);
    }
  }

  public void OnButtonAttackClicked()
  {
    Logger.Log(nameof (OnButtonAttackClicked));
    if (MerlinTowerPayload.Instance.UserData.CurrentTowerMainInfo.NoviceProtection > 0)
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_tower_protected_mines_cant_attack", true), (System.Action) null, 4f, true);
    else if (MerlinTowerPayload.Instance.UserData.mineJobId != 0L)
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_mine_occupy_only_one", true), (System.Action) null, 4f, true);
    }
    else
    {
      this.CloseCircleMenu();
      UIManager.inst.OpenDlg("MerlinTower/MerlinTowerOreMarchAllocDlg", (UI.Dialog.DialogParameter) new MerlinTowerOreMarchAllocDlg.Parameter()
      {
        desTroopCount = 1000,
        maxTime = 10000,
        remainTime = (NetServerTime.inst.ServerTimestamp + 360000),
        speed = 100,
        isRally = false,
        marchType = MarchAllocDlg.MarchType.gather,
        confirmCallback = (System.Action<Hashtable, bool>) ((troops, withDragon) => MerlinTowerPayload.Instance.SendPvPBattleRequest(troops, withDragon, this.Page, this.Index, this._mineData.uid))
      }, true, true, true);
    }
  }
}
