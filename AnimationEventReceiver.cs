﻿// Decompiled with JetBrains decompiler
// Type: AnimationEventReceiver
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AnimationEventReceiver : MonoBehaviour
{
  public System.Action onAnimationEnd;

  public void OnAnimationEnd()
  {
    if (this.onAnimationEnd == null)
      return;
    this.onAnimationEnd();
  }
}
