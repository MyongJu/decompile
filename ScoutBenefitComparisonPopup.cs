﻿// Decompiled with JetBrains decompiler
// Type: ScoutBenefitComparisonPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ScoutBenefitComparisonPopup : Popup
{
  public UITexture selfIcon;
  public UILabel selfName;
  public UITexture otherIcon;
  public UILabel otherName;
  public UITable table;
  public GameObject template;
  protected ScoutBenefitComparisonPopup.EnemyBaseInfo enemyBaseInfo;
  protected ScoutBenefitComparisonPopup.TypeStatus typeStatus;
  protected Dictionary<string, BenefitComparisonElement> researchElements;
  protected Dictionary<string, BenefitComparisonElement> equipmentElements;
  protected Dictionary<string, BenefitComparisonElement> talentElements;
  protected Dictionary<string, BenefitComparisonElement> buildingElements;

  private void Init()
  {
    if (this.enemyBaseInfo == null)
    {
      NGUITools.SetActive(this.otherIcon.gameObject, false);
    }
    else
    {
      CustomIconLoader.Instance.requestCustomIcon(this.otherIcon, UserData.BuildPortraitPath(int.Parse(this.enemyBaseInfo.portrait)), this.enemyBaseInfo.icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.otherIcon, this.enemyBaseInfo.lordTitleId, 1);
      this.otherName.text = this.enemyBaseInfo.name;
      UserData userData = PlayerData.inst.userData;
      this.selfName.text = userData.userName_Alliance_Name;
      CustomIconLoader.Instance.requestCustomIcon(this.selfIcon, UserData.BuildPortraitPath(userData.portrait), userData.Icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.selfIcon, userData.LordTitle, 1);
      this.DrawResearchComparison();
      this.DrawTalentComparison();
      this.DrawEquipmentComparison();
      this.DrawBuildingComparison();
      this.table.Reposition();
    }
  }

  private void DrawResearchComparison()
  {
    if (this.researchElements == null || this.researchElements.Count == 0 || this.typeStatus.researchStatus == 1)
      return;
    GameObject go = NGUITools.AddChild(this.table.gameObject, this.template);
    NGUITools.SetActive(go, true);
    go.GetComponent<BenefitComparisonComponent>().FeedData((IComponentData) new BenefitComparisonComponentData(this.researchElements, "id_uppercase_research"));
  }

  private void DrawEquipmentComparison()
  {
    if (this.equipmentElements == null || this.equipmentElements.Count == 0 || this.typeStatus.equipmentStatus == 1)
      return;
    GameObject go = NGUITools.AddChild(this.table.gameObject, this.template);
    NGUITools.SetActive(go, true);
    go.GetComponent<BenefitComparisonComponent>().FeedData((IComponentData) new BenefitComparisonComponentData(this.equipmentElements, "id_uppercase_equipment"));
  }

  private void DrawBuildingComparison()
  {
    if (this.buildingElements == null || this.buildingElements.Count == 0 || this.typeStatus.buildingStatus == 1)
      return;
    GameObject go = NGUITools.AddChild(this.table.gameObject, this.template);
    NGUITools.SetActive(go, true);
    go.GetComponent<BenefitComparisonComponent>().FeedData((IComponentData) new BenefitComparisonComponentData(this.buildingElements, "mail_scout_report_uppercase_buildings_subtitle"));
  }

  private void DrawTalentComparison()
  {
    if (this.talentElements == null || this.talentElements.Count == 0 || this.typeStatus.talentStatus == 1)
      return;
    GameObject go = NGUITools.AddChild(this.table.gameObject, this.template);
    NGUITools.SetActive(go, true);
    go.GetComponent<BenefitComparisonComponent>().FeedData((IComponentData) new BenefitComparisonComponentData(this.talentElements, "id_uppercase_lord_talents"));
  }

  private void Dispose()
  {
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.enemyBaseInfo = (orgParam as ScoutBenefitComparisonPopup.Parameter).enemyBaseInfo;
    this.typeStatus = (orgParam as ScoutBenefitComparisonPopup.Parameter).typeStatus;
    this.researchElements = (orgParam as ScoutBenefitComparisonPopup.Parameter).reseachElements;
    this.equipmentElements = (orgParam as ScoutBenefitComparisonPopup.Parameter).equipmentElements;
    this.talentElements = (orgParam as ScoutBenefitComparisonPopup.Parameter).talentElements;
    this.buildingElements = (orgParam as ScoutBenefitComparisonPopup.Parameter).buildingElements;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.Dispose();
    base.OnClose(orgParam);
  }

  public class Parameter : Popup.PopupParameter
  {
    public ScoutBenefitComparisonPopup.EnemyBaseInfo enemyBaseInfo;
    public ScoutBenefitComparisonPopup.TypeStatus typeStatus;
    public Dictionary<string, BenefitComparisonElement> reseachElements;
    public Dictionary<string, BenefitComparisonElement> equipmentElements;
    public Dictionary<string, BenefitComparisonElement> talentElements;
    public Dictionary<string, BenefitComparisonElement> buildingElements;
  }

  public class EnemyBaseInfo
  {
    public string portrait;
    public string icon;
    public string name;
    public int lordTitleId;
  }

  public class TypeStatus
  {
    public int talentStatus;
    public int equipmentStatus;
    public int researchStatus;
    public int buildingStatus;
  }
}
