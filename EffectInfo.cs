﻿// Decompiled with JetBrains decompiler
// Type: EffectInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class EffectInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "effecttarget")]
  public string effectTarget;
  [Config(Name = "offset")]
  public string offset;
  [Config(Name = "lookattarget")]
  public int lookatTarget;
  [Config(Name = "duration")]
  public float duration;
  [Config(Name = "speedscale")]
  public float speedScale;
  [Config(Name = "fxname")]
  public string fxName;

  public Vector3 GetOffSet()
  {
    string[] strArray = this.offset.Split(new string[1]
    {
      ","
    }, StringSplitOptions.RemoveEmptyEntries);
    float[] numArray = new float[3];
    for (int index = 0; index < 3; ++index)
      numArray[index] = index >= strArray.Length - 1 ? 0.0f : float.Parse(strArray[index]);
    return new Vector3(numArray[0], numArray[1], numArray[2]);
  }

  public enum EffectTarget
  {
    SOURCE,
    TARGET,
  }
}
