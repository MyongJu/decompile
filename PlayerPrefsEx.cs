﻿// Decompiled with JetBrains decompiler
// Type: PlayerPrefsEx
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PlayerPrefsEx
{
  public static long uid
  {
    get
    {
      return PlayerData.inst.uid;
    }
  }

  public static void SetInt(string key, int value)
  {
    PlayerPrefs.SetInt(key, value);
  }

  public static void SetFloat(string key, float value)
  {
    PlayerPrefs.SetFloat(key, value);
  }

  public static void SetString(string key, string value)
  {
    PlayerPrefs.SetString(key, value);
  }

  public static void SetIntByUid(string key, int value)
  {
    if (PlayerPrefsEx.uid == -1L)
      return;
    PlayerPrefsEx.SetInt(key + PlayerPrefsEx.uid.ToString(), value);
  }

  public static void SetFloatByUid(string key, float value)
  {
    if (PlayerPrefsEx.uid == -1L)
      return;
    PlayerPrefsEx.SetFloat(key + PlayerPrefsEx.uid.ToString(), value);
  }

  public static void SetStringByUid(string key, string value)
  {
    if (PlayerPrefsEx.uid == -1L)
      return;
    PlayerPrefsEx.SetString(key + PlayerPrefsEx.uid.ToString(), value);
  }

  public static int GetIntByUid(string key)
  {
    if (PlayerPrefsEx.uid == -1L)
      return 0;
    return PlayerPrefsEx.GetInt(key + PlayerPrefsEx.uid.ToString());
  }

  public static int GetIntByUid(string key, int defaultValue)
  {
    if (PlayerPrefsEx.uid == -1L)
      return 0;
    return PlayerPrefsEx.GetInt(key + PlayerPrefsEx.uid.ToString(), defaultValue);
  }

  public static float GetFloatByUid(string key)
  {
    if (PlayerPrefsEx.uid == -1L)
      return 0.0f;
    return PlayerPrefsEx.GetFloat(key + PlayerPrefsEx.uid.ToString());
  }

  public static float GetFloatByUid(string key, float defaultValue)
  {
    if (PlayerPrefsEx.uid == -1L)
      return 0.0f;
    return PlayerPrefsEx.GetFloat(key + PlayerPrefsEx.uid.ToString(), defaultValue);
  }

  public static string GetStringByUid(string key)
  {
    if (PlayerPrefsEx.uid == -1L)
      return (string) null;
    return PlayerPrefsEx.GetString(key + PlayerPrefsEx.uid.ToString());
  }

  public static string GetStringByUid(string key, string defaultValue)
  {
    if (PlayerPrefsEx.uid == -1L)
      return (string) null;
    return PlayerPrefsEx.GetString(key + PlayerPrefsEx.uid.ToString(), defaultValue);
  }

  public static int GetInt(string key)
  {
    return PlayerPrefs.GetInt(key);
  }

  public static int GetInt(string key, int defaultValue)
  {
    return PlayerPrefs.GetInt(key, defaultValue);
  }

  public static float GetFloat(string key)
  {
    return PlayerPrefs.GetFloat(key);
  }

  public static float GetFloat(string key, float defaultValue)
  {
    return PlayerPrefs.GetFloat(key, defaultValue);
  }

  public static string GetString(string key)
  {
    return PlayerPrefs.GetString(key);
  }

  public static string GetString(string key, string defaultValue)
  {
    return PlayerPrefs.GetString(key, defaultValue);
  }

  public static void DeleteAll()
  {
    PlayerPrefs.DeleteAll();
  }

  public static void DeleteKey(string key)
  {
    PlayerPrefs.DeleteKey(key);
  }

  public static void DeleteKeyByUid(string key)
  {
    PlayerPrefs.DeleteKey(key + PlayerPrefsEx.uid.ToString());
  }

  public static bool HasKey(string key)
  {
    return PlayerPrefs.HasKey(key);
  }

  public static bool HasUidKey(string key)
  {
    return PlayerPrefs.HasKey(key + PlayerPrefsEx.uid.ToString());
  }

  public static void Save()
  {
    PlayerPrefs.Save();
  }
}
