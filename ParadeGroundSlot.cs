﻿// Decompiled with JetBrains decompiler
// Type: ParadeGroundSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ParadeGroundSlot : MonoBehaviour
{
  [SerializeField]
  private TroopType _unitType = TroopType.invalid;
  [SerializeField]
  private int _troopCount;
  [SerializeField]
  private int _totalCount;
  [SerializeField]
  private int _displayCount;
  private int _troopFormationCount;
  private GameObject[] _troopFormationGO;
  private Vector3[] _troopFormationPos;
  private Vector2 __troopFormation;

  public void SetSlot(TroopType inType, int inTroopCount, int inTroopTotalCount)
  {
    this.unitType = inType;
    this.totalCount = inTroopTotalCount;
    this.troopCount = inTroopCount;
  }

  public void Clear()
  {
    this.troopCount = 0;
    this.totalCount = 0;
    this.unitType = TroopType.invalid;
  }

  public TroopType unitType
  {
    set
    {
      if (this._unitType == value)
        return;
      this._unitType = value;
      if (this._unitType == TroopType.invalid)
        this._DestroySprites();
      else
        this._CreateSprites();
    }
    get
    {
      return this._unitType;
    }
  }

  public int troopCount
  {
    set
    {
      this._troopCount = value;
      this._ResetDisplayCount();
    }
    get
    {
      return this._troopCount;
    }
  }

  public int totalCount
  {
    set
    {
      if (this._totalCount == value)
        return;
      this._totalCount = value;
      this._ResetDisplayCount();
    }
    get
    {
      return this._totalCount;
    }
  }

  public int displayCount
  {
    get
    {
      return this._displayCount;
    }
    set
    {
      if (this._displayCount == value)
        return;
      this._displayCount = value <= this._troopFormationCount ? value : this._troopFormationCount;
      for (int index = this._displayCount - 1; index >= 0; --index)
        this._troopFormationGO[index].SetActive(true);
      for (int displayCount = this._displayCount; displayCount < this._troopFormationCount; ++displayCount)
        this._troopFormationGO[displayCount].SetActive(false);
    }
  }

  private Vector2 _troopFormation
  {
    get
    {
      return this.__troopFormation;
    }
    set
    {
      this.__troopFormation = value;
      this._troopFormationCount = (int) ((double) this._troopFormation.x * (double) this._troopFormation.y);
      this._troopFormationPos = new Vector3[this._troopFormationCount];
      Vector3 vector3_1 = ParadeGroundConfig.FORMATION_DIR_X * 100f / (value.x * 2f);
      Vector3 vector3_2 = ParadeGroundConfig.FORMATION_DIR_Y * 100f / (value.y * 2f);
      Vector3 vector3_3 = -vector3_1 * (value.x - 1f) - vector3_2 * (value.y - 1f);
      for (int index1 = 0; index1 < (int) value.y; ++index1)
      {
        for (int index2 = 0; index2 < (int) value.x; ++index2)
        {
          this._troopFormationPos[index1 * (int) value.x + index2] = vector3_3 + vector3_1 * (float) index2 * 2f + vector3_2 * (float) index1 * 2f;
          if (this.unitType == TroopType.class_cavalry && index2 % 2 == 1)
            this._troopFormationPos[index1 * (int) value.x + index2] += vector3_1 * 0.5f;
          this._troopFormationPos[index1 * (int) value.x + index2].z = (float) (-this._troopFormationCount + (index1 * (int) value.x + index2));
        }
      }
    }
  }

  private void _CreateSprites()
  {
    this._DestroySprites();
    this._troopFormation = this._GetTroopFormationByType();
    this._troopFormationGO = new GameObject[this._troopFormationCount];
    UnityEngine.Object troopModelByType = this._GetTroopModelByType();
    for (int index = 0; index < this._troopFormationCount; ++index)
    {
      this._troopFormationGO[index] = UnityEngine.Object.Instantiate(troopModelByType) as GameObject;
      this._troopFormationGO[index].name = string.Format("{0}_{1}", (object) this.unitType.ToString(), (object) (100 + index));
      this._troopFormationGO[index].transform.parent = this.transform;
      this._troopFormationGO[index].transform.localPosition = this._troopFormationPos[index];
      this._troopFormationGO[index].transform.rotation = Quaternion.identity;
      this._troopFormationGO[index].SetActive(false);
      this._troopFormationGO[index].GetComponent<SpriteRenderer>().sortingOrder = this._troopFormationCount - index;
    }
    this._displayCount = 0;
  }

  private void _DestroySprites()
  {
    if (this._troopFormationGO == null)
      return;
    for (int index = 0; index < this._troopFormationGO.Length; ++index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this._troopFormationGO[index]);
  }

  private void _ResetDisplayCount()
  {
    if (this._totalCount == 0)
      this.displayCount = 0;
    else
      this.displayCount = (int) ((double) this._troopCount / (double) this._totalCount * (double) this._troopFormationCount) + 1;
  }

  private Vector2 _GetTroopFormationByType()
  {
    switch (this.unitType)
    {
      case TroopType.class_infantry:
      case TroopType.class_ranged:
      case TroopType.class_artyclose:
        return ParadeGroundConfig.FORMATION_INFANTRY_RANGED_ARTYCLOSE_TROOPS;
      case TroopType.class_cavalry:
        return ParadeGroundConfig.FORMATION_CAVARLY_TROOPS;
      case TroopType.class_artyfar:
        return ParadeGroundConfig.FORMATION_ARTYFAR;
      default:
        return Vector2.zero;
    }
  }

  private UnityEngine.Object _GetTroopModelByType()
  {
    switch (this.unitType)
    {
      case TroopType.class_infantry:
        return AssetManager.Instance.HandyLoad("Prefab/ParadeGround/InfantryParadeGround", (System.Type) null);
      case TroopType.class_ranged:
        return AssetManager.Instance.HandyLoad("Prefab/ParadeGround/Ranged", (System.Type) null);
      case TroopType.class_cavalry:
        return AssetManager.Instance.HandyLoad("Prefab/ParadeGround/Cavalry", (System.Type) null);
      case TroopType.class_artyclose:
        return AssetManager.Instance.HandyLoad("Prefab/ParadeGround/ArtyClose", (System.Type) null);
      case TroopType.class_artyfar:
        return AssetManager.Instance.HandyLoad("Prefab/ParadeGround/ArtyFar", (System.Type) null);
      default:
        return (UnityEngine.Object) null;
    }
  }
}
