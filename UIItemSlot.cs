﻿// Decompiled with JetBrains decompiler
// Type: UIItemSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public abstract class UIItemSlot : MonoBehaviour
{
  private string mText = string.Empty;
  public UISprite icon;
  public UIWidget background;
  public UILabel label;
  public AudioClip grabSound;
  public AudioClip placeSound;
  public AudioClip errorSound;
  private InvGameItem mItem;
  private static InvGameItem mDraggedItem;

  protected abstract InvGameItem observedItem { get; }

  protected abstract InvGameItem Replace(InvGameItem item);

  private void OnTooltip(bool show)
  {
    InvGameItem invGameItem = !show ? (InvGameItem) null : this.mItem;
    if (invGameItem != null)
    {
      InvBaseItem baseItem = invGameItem.baseItem;
      if (baseItem != null)
      {
        string text = "[" + NGUIText.EncodeColor(invGameItem.color) + "]" + invGameItem.name + "[-]\n" + "[AFAFAF]Level " + (object) invGameItem.itemLevel + " " + (object) baseItem.slot;
        List<InvStat> stats = invGameItem.CalculateStats();
        int index = 0;
        for (int count = stats.Count; index < count; ++index)
        {
          InvStat invStat = stats[index];
          if (invStat.amount != 0)
          {
            string str = invStat.amount >= 0 ? text + "\n[00FF00]+" + (object) invStat.amount : text + "\n[FF0000]" + (object) invStat.amount;
            if (invStat.modifier == InvStat.Modifier.Percent)
              str += "%";
            text = str + " " + (object) invStat.id + "[-]";
          }
        }
        if (!string.IsNullOrEmpty(baseItem.description))
          text = text + "\n[FF9900]" + baseItem.description;
        UITooltip.Show(text);
        return;
      }
    }
    UITooltip.Hide();
  }

  private void OnClick()
  {
    if (UIItemSlot.mDraggedItem != null)
    {
      this.OnDrop((GameObject) null);
    }
    else
    {
      if (this.mItem == null)
        return;
      UIItemSlot.mDraggedItem = this.Replace((InvGameItem) null);
      if (UIItemSlot.mDraggedItem != null)
        NGUITools.PlaySound(this.grabSound);
      this.UpdateCursor();
    }
  }

  private void OnDrag(Vector2 delta)
  {
    if (UIItemSlot.mDraggedItem != null || this.mItem == null)
      return;
    UICamera.currentTouch.clickNotification = UICamera.ClickNotification.BasedOnDelta;
    UIItemSlot.mDraggedItem = this.Replace((InvGameItem) null);
    NGUITools.PlaySound(this.grabSound);
    this.UpdateCursor();
  }

  private void OnDrop(GameObject go)
  {
    InvGameItem invGameItem = this.Replace(UIItemSlot.mDraggedItem);
    if (UIItemSlot.mDraggedItem == invGameItem)
      NGUITools.PlaySound(this.errorSound);
    else if (invGameItem != null)
      NGUITools.PlaySound(this.grabSound);
    else
      NGUITools.PlaySound(this.placeSound);
    UIItemSlot.mDraggedItem = invGameItem;
    this.UpdateCursor();
  }

  private void UpdateCursor()
  {
    if (UIItemSlot.mDraggedItem != null && UIItemSlot.mDraggedItem.baseItem != null)
      UICursor.Set(UIItemSlot.mDraggedItem.baseItem.iconAtlas, UIItemSlot.mDraggedItem.baseItem.iconName);
    else
      UICursor.Clear();
  }

  private void Update()
  {
    InvGameItem observedItem = this.observedItem;
    if (this.mItem == observedItem)
      return;
    this.mItem = observedItem;
    InvBaseItem invBaseItem = observedItem == null ? (InvBaseItem) null : observedItem.baseItem;
    if ((Object) this.label != (Object) null)
    {
      string str = observedItem == null ? (string) null : observedItem.name;
      if (string.IsNullOrEmpty(this.mText))
        this.mText = this.label.text;
      this.label.text = str == null ? this.mText : str;
    }
    if ((Object) this.icon != (Object) null)
    {
      if (invBaseItem == null || (Object) invBaseItem.iconAtlas == (Object) null)
      {
        this.icon.enabled = false;
      }
      else
      {
        this.icon.atlas = invBaseItem.iconAtlas;
        this.icon.spriteName = invBaseItem.iconName;
        this.icon.enabled = true;
        this.icon.MakePixelPerfect();
      }
    }
    if (!((Object) this.background != (Object) null))
      return;
    this.background.color = observedItem == null ? Color.white : observedItem.color;
  }
}
