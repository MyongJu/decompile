﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossReportContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class AllianceBossReportContent
{
  public string k;
  public string x;
  public string y;
  public MemberLeader attacker;
  public List<MemberDetail> attacker_troop_detail;
  public BossDetail defender;

  public string site
  {
    get
    {
      return "K: " + this.k + " X: " + this.x + " Y: " + this.y;
    }
  }
}
