﻿// Decompiled with JetBrains decompiler
// Type: AuctionHallRecordPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class AuctionHallRecordPopup : Popup
{
  private Dictionary<int, AuctionRecordItemRenderer> _itemDict = new Dictionary<int, AuctionRecordItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public GameObject noAuctionRecordNode;
  public UILabel panelTitle;
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  private AuctionHelper.AuctionType _auctionType;
  private List<AuctionRecordItemInfo> _auctionRecordItemInfoList;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    AuctionHallRecordPopup.Paramter paramter = orgParam as AuctionHallRecordPopup.Paramter;
    if (paramter != null)
    {
      this._auctionType = paramter.auctionType;
      this._auctionRecordItemInfoList = paramter.auctionRecordItemInfoList;
    }
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.ShowRecordContent();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void ShowRecordContent()
  {
    this.ClearData();
    switch (this._auctionType)
    {
      case AuctionHelper.AuctionType.PUBLIC_AUCTION:
      case AuctionHelper.AuctionType.WORLD_AUCTION:
        this.panelTitle.text = Utils.XLAT("exchange_auction_uppercase_auction_history");
        break;
      case AuctionHelper.AuctionType.NON_PUBLIC_AUCTION:
        this.panelTitle.text = Utils.XLAT("exchange_black_market_uppercase_black_market_history");
        break;
    }
    if (this._auctionRecordItemInfoList != null)
    {
      for (int key = 0; key < this._auctionRecordItemInfoList.Count; ++key)
      {
        AuctionRecordItemRenderer itemRenderer = this.CreateItemRenderer(this._auctionRecordItemInfoList[key]);
        this._itemDict.Add(key, itemRenderer);
      }
    }
    NGUITools.SetActive(this.noAuctionRecordNode, this._auctionRecordItemInfoList == null || this._auctionRecordItemInfoList.Count <= 0);
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, AuctionRecordItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private AuctionRecordItemRenderer CreateItemRenderer(AuctionRecordItemInfo itemInfo)
  {
    GameObject gameObject = this._itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    AuctionRecordItemRenderer component = gameObject.GetComponent<AuctionRecordItemRenderer>();
    component.SetData(itemInfo);
    return component;
  }

  public class Paramter : Popup.PopupParameter
  {
    public AuctionHelper.AuctionType auctionType;
    public List<AuctionRecordItemInfo> auctionRecordItemInfoList;
  }
}
