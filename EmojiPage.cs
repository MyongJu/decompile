﻿// Decompiled with JetBrains decompiler
// Type: EmojiPage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class EmojiPage : MonoBehaviour
{
  public int ROW_LIMIT = 3;
  private List<GameObject> renders = new List<GameObject>();
  public UIGrid grid;
  public GameObject renderTemplate;
  public UIWidget widget;

  public void FeedContent(List<Emoji> emojiList)
  {
    this.renders.ForEach((System.Action<GameObject>) (obj => obj.SetActive(false)));
    UIWidget component = this.renderTemplate.GetComponent<UIWidget>();
    for (int index = 0; index < emojiList.Count; ++index)
    {
      GameObject gameObject;
      if (index < this.renders.Count)
      {
        gameObject = this.renders[index];
      }
      else
      {
        gameObject = NGUITools.AddChild(this.grid.gameObject, this.renderTemplate);
        this.renders.Add(gameObject);
      }
      gameObject.SetActive(true);
      gameObject.name = index.ToString();
      gameObject.GetComponent<EmojiPanelContainer>().ShowEmoji(emojiList[index], (float) component.width, (float) component.height);
    }
    this.grid.sorting = UIGrid.Sorting.Custom;
    this.grid.onCustomSort = (Comparison<Transform>) ((x, y) => int.Parse(x.name).CompareTo(int.Parse(y.name)));
    this.grid.Reposition();
  }

  public int MAXCount
  {
    get
    {
      return this.grid.maxPerLine * this.ROW_LIMIT;
    }
  }
}
