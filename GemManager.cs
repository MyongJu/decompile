﻿// Decompiled with JetBrains decompiler
// Type: GemManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using GemConstant;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class GemManager
{
  private static GemManager instance = new GemManager();

  public static GemManager Instance
  {
    get
    {
      return GemManager.instance;
    }
  }

  public void Init()
  {
    GameEngine.Instance.OnGameModeReady += new System.Action(this.OnGameModeReady);
  }

  public void Dispose()
  {
    GameEngine.Instance.OnGameModeReady -= new System.Action(this.OnGameModeReady);
  }

  private void OnGameModeReady()
  {
  }

  public int GetGemBagCapacity()
  {
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("max_gem_number");
    if (data != null)
      return data.ValueInt;
    return 0;
  }

  public int GetGemBagLeftCapacity(SlotType slotType)
  {
    return this.GetGemBagCapacity() - (DBManager.inst.DB_Gems.GetEdibleGems(slotType, -1L).Count - DBManager.inst.DB_Gems.GetAllExpGems(slotType).Count);
  }

  public bool IsGemBagFull(SlotType slotType)
  {
    return this.GetGemBagLeftCapacity(slotType) <= 0;
  }

  public bool IsGemMaxLevel(int gemConfigId)
  {
    return this.GetNextLevelGemConfig(gemConfigId) == null;
  }

  public ConfigEquipmentGemInfo GetNextLevelGemConfig(int gemConfigId)
  {
    ConfigEquipmentGemInfo data = ConfigManager.inst.DB_EquipmentGem.GetData(gemConfigId);
    if (data == null)
      return (ConfigEquipmentGemInfo) null;
    return ConfigManager.inst.DB_EquipmentGem.GetData(data.nextLevelId);
  }

  public bool IsExpGem(GemData gemData)
  {
    if (gemData == null)
      return false;
    return gemData.GemType == GemType.EXP;
  }

  public long GetGemTotalExp(GemData gemData)
  {
    long num = 0;
    if (gemData == null)
      return num;
    ConfigEquipmentGemInfo data = ConfigManager.inst.DB_EquipmentGem.GetData(gemData.ConfigId);
    if (data == null)
      return 0;
    return gemData.Exp + data.expContain;
  }

  public long GetGemListTotalExp(List<GemData> list)
  {
    if (list == null)
      return 0;
    long num = 0;
    using (List<GemData>.Enumerator enumerator = list.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GemData current = enumerator.Current;
        num += this.GetGemTotalExp(current);
      }
    }
    return num;
  }

  public bool GetLeveledUpGemInfo(GemData gemData, long addExp, ref ConfigEquipmentGemInfo gemConfig, ref long curExp, ref bool isMaxLevel)
  {
    if (gemData == null)
      return false;
    gemConfig = ConfigManager.inst.DB_EquipmentGem.GetData(gemData.ConfigId);
    if (gemConfig == null)
      return false;
    curExp = gemData.Exp + addExp;
    for (long expReqToNext = gemConfig.expReqToNext; curExp >= expReqToNext; expReqToNext = gemConfig.expReqToNext)
    {
      if (gemConfig.nextLevelId > 0)
      {
        isMaxLevel = false;
        gemConfig = ConfigManager.inst.DB_EquipmentGem.GetData(gemConfig.nextLevelId);
        if (gemConfig == null)
        {
          D.error((object) "GetLeveledUpGemInfo error, gem config is null");
          return false;
        }
        curExp -= expReqToNext;
      }
      else
      {
        isMaxLevel = true;
        return true;
      }
    }
    return true;
  }

  public bool WillGemReachMaxLevel(GemData gemData, long addExp)
  {
    if (gemData == null)
    {
      D.error((object) "IsGemFullExp error, gemData is null");
      return false;
    }
    if (this.IsGemMaxLevel(gemData.ConfigId))
      return true;
    ConfigEquipmentGemInfo data = ConfigManager.inst.DB_EquipmentGem.GetData(gemData.ConfigId);
    if (data == null)
    {
      D.error((object) "IsGemFullExp error, gem config is null");
      return false;
    }
    long num = gemData.Exp + addExp;
    for (long expReqToNext = data.expReqToNext; num >= expReqToNext; expReqToNext = data.expReqToNext)
    {
      data = ConfigManager.inst.DB_EquipmentGem.GetData(data.nextLevelId);
      if (this.IsGemMaxLevel(data.internalId))
        return true;
      num -= expReqToNext;
    }
    return false;
  }

  public long GetExpNeedToMaxLevel(GemData gemData)
  {
    if (this.IsGemMaxLevel(gemData.ConfigId))
      return 0;
    ConfigEquipmentGemInfo data = ConfigManager.inst.DB_EquipmentGem.GetData(gemData.ConfigId);
    long num = -gemData.Exp;
    for (; !this.IsGemMaxLevel(data.internalId); data = ConfigManager.inst.DB_EquipmentGem.GetData(data.nextLevelId))
      num += data.expReqToNext;
    return num;
  }

  public string GetGemBenefitsDescription(Benefits benefits)
  {
    if (benefits == null)
      return string.Empty;
    List<Benefits.BenefitValuePair> benefitsPairs = benefits.GetBenefitsPairs();
    StringBuilder stringBuilder = new StringBuilder();
    using (List<Benefits.BenefitValuePair>.Enumerator enumerator = benefitsPairs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Benefits.BenefitValuePair current = enumerator.Current;
        stringBuilder.Append(string.Format("{0}\n", (object) current.ToString()));
      }
    }
    return stringBuilder.ToString();
  }

  public List<GemDataWrapper> GetGemDataWrapper(List<GemData> gemDatas)
  {
    Dictionary<int, int> dictionary1 = new Dictionary<int, int>();
    List<GemDataWrapper> gemDataWrapperList = new List<GemDataWrapper>();
    List<GemData>.Enumerator enumerator1 = gemDatas.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      GemData current = enumerator1.Current;
      if (current.GemType == GemType.EXP)
      {
        if (dictionary1.ContainsKey(current.ConfigId))
        {
          Dictionary<int, int> dictionary2;
          int configId;
          (dictionary2 = dictionary1)[configId = current.ConfigId] = dictionary2[configId] + current.Quantity;
        }
        else
        {
          dictionary1.Add(current.ConfigId, current.Quantity);
          gemDataWrapperList.Add(new GemDataWrapper(current, current.Quantity));
        }
      }
      else
        gemDataWrapperList.Add(new GemDataWrapper(current, current.Quantity));
    }
    List<GemDataWrapper>.Enumerator enumerator2 = gemDataWrapperList.GetEnumerator();
    while (enumerator2.MoveNext())
    {
      GemDataWrapper current = enumerator2.Current;
      if (current.GemData.GemType == GemType.EXP)
        current.GemCount = dictionary1[current.GemData.ConfigId];
    }
    return gemDataWrapperList;
  }

  public void Embed(GemData gemData, InventoryItemData inventoryData, int slotIndex, System.Action<bool, object> callback)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "gem_id"] = (object) gemData.ID;
    postData[(object) "inventory_type"] = (object) (inventoryData.bagType != BagType.Hero ? 2 : 1);
    postData[(object) "inventory_id"] = (object) inventoryData.identifier.itemId;
    postData[(object) "slot_id"] = (object) slotIndex;
    MessageHub.inst.GetPortByAction("UserGem:inlayGem").SendRequest(postData, callback, true);
  }

  public void Destroy(InventoryItemData inventoryData, int slotIndex, System.Action<bool, object> callback)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "inventory_type"] = (object) (inventoryData.bagType != BagType.Hero ? 2 : 1);
    postData[(object) "inventory_id"] = (object) inventoryData.identifier.itemId;
    postData[(object) "slot_id"] = (object) slotIndex;
    MessageHub.inst.GetPortByAction("UserGem:destroyGem").SendRequest(postData, callback, true);
  }

  public void Reclaim(InventoryItemData inventoryData, int slotIndex, System.Action<bool, object> callback)
  {
    Hashtable extra = new Hashtable();
    extra[(object) "inventory_type"] = (object) (inventoryData.bagType != BagType.Hero ? 2 : 1);
    extra[(object) "inventory_id"] = (object) inventoryData.identifier.itemId;
    extra[(object) "slot_id"] = (object) slotIndex;
    ItemBag.Instance.UseItemByShopItemID("shopitem_gemstone_reclaim", extra, callback, 1);
  }

  public void BuyAndReclaim(InventoryItemData inventoryData, int slotIndex, System.Action<bool, object> callback)
  {
    Hashtable extra = new Hashtable();
    extra[(object) "inventory_type"] = (object) (inventoryData.bagType != BagType.Hero ? 2 : 1);
    extra[(object) "inventory_id"] = (object) inventoryData.identifier.itemId;
    extra[(object) "slot_id"] = (object) slotIndex;
    ItemBag.Instance.BuyAndUseShopItem("shopitem_gemstone_reclaim", extra, callback, 1);
  }

  public void Upgrade(long mainGemId, Dictionary<long, long> ateGemIds, System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("UserGem:upgradeMainGem").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "id",
        (object) mainGemId
      },
      {
        (object) "ate_ids",
        (object) ateGemIds
      }
    }, callback, true);
  }
}
