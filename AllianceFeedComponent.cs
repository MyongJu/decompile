﻿// Decompiled with JetBrains decompiler
// Type: AllianceFeedComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceFeedComponent : ComponentRenderBase
{
  public UILabel time;
  public UISprite icon;
  public UILabel content;
  public UISprite backGroundParent;
  private AllianceFeedComponentData d;
  [HideInInspector]
  public int logType;

  public override void Init()
  {
    base.Init();
    this.d = this.data as AllianceFeedComponentData;
    string Term = string.Empty;
    if (AllianceLogManager.Instance.CategoryMap.ContainsKey(this.logType))
      Term = AllianceLogManager.Instance.CategoryMap[this.logType] + this.d.type;
    else
      D.error((object) string.Format("CategoryMap in Alliance log doesn't include key : {0}", (object) this.logType));
    Dictionary<string, string> para = new Dictionary<string, string>();
    if (this.d.data != null)
    {
      for (int index = 0; index < this.d.data.Count; ++index)
      {
        string key = string.Empty;
        if (this.d.data[index] != null)
          key = this.d.data[index].ToString();
        else
          D.error((object) string.Format("Parameter {0} / {1} of {2} is null!", (object) index, (object) this.d.data.Count, (object) Term));
        string type = this.d.type;
        if (type != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (AllianceFeedComponent.\u003C\u003Ef__switch\u0024map13 == null)
          {
            // ISSUE: reference to a compiler-generated field
            AllianceFeedComponent.\u003C\u003Ef__switch\u0024map13 = new Dictionary<string, int>(15)
            {
              {
                "shop_item_online",
                0
              },
              {
                "building_destroyed",
                1
              },
              {
                "building_completed",
                1
              },
              {
                "destroy_building",
                2
              },
              {
                "building_move",
                2
              },
              {
                "building_settle",
                2
              },
              {
                "building_attack",
                3
              },
              {
                "building_destroy",
                3
              },
              {
                "research_level_up",
                4
              },
              {
                "research_choose",
                5
              },
              {
                "research_speed_up",
                5
              },
              {
                "research_mark",
                5
              },
              {
                "BOSS_failed",
                6
              },
              {
                "BOSS_killed",
                6
              },
              {
                "BOSS_start",
                7
              }
            };
          }
          int num;
          // ISSUE: reference to a compiler-generated field
          if (AllianceFeedComponent.\u003C\u003Ef__switch\u0024map13.TryGetValue(type, out num))
          {
            switch (num)
            {
              case 0:
                if (index == 1)
                {
                  this.GetItemKey(int.Parse(this.d.data[index].ToString()), out key);
                  break;
                }
                break;
              case 1:
                if (index == 0)
                {
                  this.GetAllianceBuildingKey(int.Parse(this.d.data[index].ToString()), out key);
                  break;
                }
                break;
              case 2:
                if (index == 1)
                {
                  this.GetAllianceBuildingKey(int.Parse(this.d.data[index].ToString()), out key);
                  break;
                }
                break;
              case 3:
                if (index == 2)
                {
                  this.GetAllianceBuildingKey(int.Parse(this.d.data[index].ToString()), out key);
                  break;
                }
                break;
              case 4:
                if (index == 0)
                {
                  this.GetAllianceTechKey(int.Parse(this.d.data[index].ToString()), out key);
                  break;
                }
                break;
              case 5:
                if (index == 1)
                {
                  this.GetAllianceTechKey(int.Parse(this.d.data[index].ToString()), out key);
                  break;
                }
                break;
              case 6:
                if (index == 0)
                  this.GetPortalMonsterLevel(int.Parse(this.d.data[index].ToString()), out key);
                if (index == 1)
                {
                  this.GetAllianceBossKey(int.Parse(this.d.data[index].ToString()), out key);
                  break;
                }
                break;
              case 7:
                if (index == 1)
                  this.GetPortalMonsterLevel(int.Parse(this.d.data[index].ToString()), out key);
                if (index == 2)
                {
                  this.GetAllianceBossKey(int.Parse(this.d.data[index].ToString()), out key);
                  break;
                }
                break;
            }
          }
        }
        para.Add(index.ToString(), Utils.XLAT(key));
      }
    }
    this.content.text = this.UpdateAllianceTagFormat(ScriptLocalization.GetWithPara(Term, para, true));
    this.time.text = Utils.FormatTimeForMail(long.Parse(this.d.time));
    if (AllianceLogManager.Instance.TypeSpriteMap.ContainsKey(this.d.type))
      this.icon.spriteName = AllianceLogManager.Instance.TypeSpriteMap[this.d.type];
    else
      D.error((object) string.Format("TypeSpriteMap in Alliance log doesn't include key : {0}", (object) this.d.type));
    if (AllianceLogManager.Instance.TypeBGMap.ContainsKey(this.d.type))
      this.backGroundParent.color = AllianceLogManager.Instance.TypeBGMap[this.d.type];
    else
      D.error((object) string.Format("TypeBGMap in Alliance log doesn't include key : {0}", (object) this.d.type));
  }

  private void MemberLogEventDispatcher()
  {
    string type = this.d.type;
    if (type == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (AllianceFeedComponent.\u003C\u003Ef__switch\u0024map14 == null)
    {
      // ISSUE: reference to a compiler-generated field
      AllianceFeedComponent.\u003C\u003Ef__switch\u0024map14 = new Dictionary<string, int>(6)
      {
        {
          "join_in",
          0
        },
        {
          "permited_in",
          0
        },
        {
          "invited_in",
          0
        },
        {
          "title_up",
          0
        },
        {
          "title_down",
          0
        },
        {
          "leader_change",
          0
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!AllianceFeedComponent.\u003C\u003Ef__switch\u0024map14.TryGetValue(type, out num) || num != 0)
      return;
    this.GoToAllianceMember();
  }

  private void EventLogEventDispatcher()
  {
    string type = this.d.type;
    if (type != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceFeedComponent.\u003C\u003Ef__switch\u0024map15 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceFeedComponent.\u003C\u003Ef__switch\u0024map15 = new Dictionary<string, int>(5)
        {
          {
            "research_level_up",
            0
          },
          {
            "research_speed_up",
            0
          },
          {
            "research_choose",
            0
          },
          {
            "research_mark",
            0
          },
          {
            "shop_item_online",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceFeedComponent.\u003C\u003Ef__switch\u0024map15.TryGetValue(type, out num))
      {
        switch (num)
        {
          case 0:
            this.GoToAllianceTech();
            break;
          case 1:
            this.GoToAllianceStore();
            break;
        }
      }
    }
    if (this.d.pos == null)
      return;
    this.GoToTargetPlace(int.Parse(this.d.pos[(object) "k"].ToString()), int.Parse(this.d.pos[(object) "x"].ToString()), int.Parse(this.d.pos[(object) "y"].ToString()));
  }

  private void BattleLogEventDispatcher()
  {
    if (this.d.pos == null)
      return;
    this.GoToTargetPlace(int.Parse(this.d.pos[(object) "k"].ToString()), int.Parse(this.d.pos[(object) "x"].ToString()), int.Parse(this.d.pos[(object) "y"].ToString()));
  }

  public void OnClick()
  {
    switch (this.logType)
    {
      case 0:
        this.MemberLogEventDispatcher();
        break;
      case 1:
        this.EventLogEventDispatcher();
        break;
      case 2:
        this.BattleLogEventDispatcher();
        break;
    }
  }

  private void GetItemKey(int internalId, out string key)
  {
    key = string.Empty;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
    if (itemStaticInfo != null)
      key = itemStaticInfo.Loc_Name_Id;
    else
      D.error((object) string.Format("Can not get item static info with internal id: {0}", (object) internalId));
  }

  private void GetAllianceBuildingKey(int internalId, out string key)
  {
    key = string.Empty;
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(internalId);
    if (buildingStaticInfo != null)
      key = buildingStaticInfo.BuildName;
    else
      D.error((object) string.Format("Can not get alliance building info with internal id: {0}", (object) internalId));
  }

  private void GetAllianceTechKey(int internalId, out string key)
  {
    key = string.Empty;
    AllianceTechInfo allianceTechInfo = ConfigManager.inst.DB_AllianceTech.GetItem(internalId);
    if (allianceTechInfo != null)
      key = allianceTechInfo.LocNameId;
    else
      D.error((object) string.Format("Can not get alliance tech info with internal id: {0}", (object) internalId));
  }

  private void GetAllianceBossKey(int internalId, out string key)
  {
    key = string.Empty;
    AllianceBossInfo allianceBossInfo = ConfigManager.inst.DB_AllianceBoss.Get(internalId);
    if (allianceBossInfo != null)
      key = allianceBossInfo.name;
    else
      D.error((object) string.Format("Can not get alliance boss info with internal id: {0}", (object) internalId));
  }

  private void GetPortalMonsterLevel(int index, out string key)
  {
    key = string.Empty;
    if (AllianceLogManager.Instance.PortalMonsterLevelMap.ContainsKey(index))
      key = AllianceLogManager.Instance.PortalMonsterLevelMap[index];
    else
      D.error((object) string.Format("PortalMonsterLevelMap doesn't have key: {0}", (object) index));
  }

  private void GoToAllianceMember()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    UIManager.inst.OpenDlg("Alliance/AllianceDlg", (UI.Dialog.DialogParameter) new AllianceDlg.Parameter()
    {
      selectedIndex = 1
    }, true, true, true);
  }

  private void GoToAllianceTech()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
    UIManager.inst.OpenDlg("Alliance/AllianceTechDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void GoToAllianceStore()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
    UIManager.inst.OpenDlg("Alliance/AllianceStore", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  protected void GoToTargetPlace(int k, int x, int y)
  {
    if (!MapUtils.CanGotoTarget(k))
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    }
    else
    {
      PVPMap.PendingGotoRequest = new Coordinate(k, x, y);
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
        CitadelSystem.inst.OnLoadWorldMap();
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }

  private string UpdateAllianceTagFormat(string content)
  {
    string str = content;
    if (this.d.data != null)
    {
      for (int index = 0; index < this.d.data.Count; ++index)
      {
        string empty = string.Empty;
        if (this.d.data[index] != null)
          empty = this.d.data[index].ToString();
        string type = this.d.type;
        if (type != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (AllianceFeedComponent.\u003C\u003Ef__switch\u0024map16 == null)
          {
            // ISSUE: reference to a compiler-generated field
            AllianceFeedComponent.\u003C\u003Ef__switch\u0024map16 = new Dictionary<string, int>(4)
            {
              {
                "member_attacked",
                0
              },
              {
                "member_city_attacked",
                0
              },
              {
                "member_attack",
                1
              },
              {
                "member_attack_city",
                1
              }
            };
          }
          int num;
          // ISSUE: reference to a compiler-generated field
          if (AllianceFeedComponent.\u003C\u003Ef__switch\u0024map16.TryGetValue(type, out num))
          {
            switch (num)
            {
              case 0:
                if (index == 3 && empty == "--")
                {
                  str = content.Replace("(--)", string.Empty);
                  continue;
                }
                continue;
              case 1:
                if (index == 1 && empty == "--")
                {
                  str = content.Replace("(--)", string.Empty);
                  continue;
                }
                continue;
              default:
                continue;
            }
          }
        }
      }
    }
    return str;
  }
}
