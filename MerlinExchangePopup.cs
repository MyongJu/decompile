﻿// Decompiled with JetBrains decompiler
// Type: MerlinExchangePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;
using UnityEngine;

public class MerlinExchangePopup : Popup
{
  private bool _autoFix = true;
  private ProgressBarTweenProxy _proxy = new ProgressBarTweenProxy();
  private int _proxyMinSize = 1;
  [SerializeField]
  private UILabel _labelTitle;
  [SerializeField]
  private UILabel _labelItemDescription;
  [SerializeField]
  private ItemIconRenderer _itemIcon;
  [SerializeField]
  private UITexture _textureMerlinCoin;
  [SerializeField]
  private UILabel _labelMerlinCoinCount;
  [SerializeField]
  private UIButton _buttonExchange;
  [SerializeField]
  private UIProgressBar _progressBar;
  [SerializeField]
  private UILabel _labelInputCount;
  [SerializeField]
  private UIInput _inputExchangeCount;
  private MerlinTrialsShopInfo _shopInfo;
  private int _exchangeCount;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    MerlinExchangePopup.Parameter parameter = orgParam as MerlinExchangePopup.Parameter;
    if (parameter != null)
      this._shopInfo = parameter.shopInfo;
    if (this._shopInfo != null && (double) this._shopInfo.price > 0.0)
      this._proxy.MaxSize = (int) (PlayerData.inst.userData.MerlinCoin / (long) this._shopInfo.price) - 1;
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnSliderValueChanged()
  {
    this._exchangeCount = Mathf.RoundToInt((float) this._proxy.MaxSize * this._progressBar.value) + this._proxyMinSize;
    this._labelInputCount.text = this._exchangeCount.ToString();
    this._labelMerlinCoinCount.text = (this._exchangeCount * this._shopInfo.price).ToString();
  }

  public void OnSliderSubtractBtnDown()
  {
    this._autoFix = false;
    this._proxy.Slider = this._progressBar;
    this._proxy.Play(-1, -1f);
  }

  public void OnSliderSubtractBtnUp()
  {
    this._autoFix = true;
    this._proxy.Stop();
  }

  public void OnSliderPlusBtnDown()
  {
    this._autoFix = false;
    this._proxy.Slider = this._progressBar;
    this._proxy.Play(1, -1f);
  }

  public void OnSliderPlusBtnUp()
  {
    this._autoFix = true;
    this._proxy.Stop();
  }

  public void OnInputChange()
  {
    if (this._shopInfo == null)
      return;
    int num = 0;
    try
    {
      num = Convert.ToInt32(this._inputExchangeCount.value);
    }
    catch (FormatException ex)
    {
      num = 0;
    }
    catch (OverflowException ex)
    {
      num = int.MaxValue;
    }
    finally
    {
      this._exchangeCount = Mathf.Clamp(num, 1, this._proxy.MaxSize + 1);
      if (this._autoFix)
        this._progressBar.value = (float) (this._exchangeCount - 1) / (float) this._proxy.MaxSize;
      this._inputExchangeCount.value = this._exchangeCount.ToString();
      this._labelInputCount.text = this._exchangeCount.ToString();
      this._labelMerlinCoinCount.text = (this._exchangeCount * this._shopInfo.price).ToString();
    }
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnExchangePressed()
  {
    if (this._shopInfo == null)
      return;
    this._buttonExchange.isEnabled = false;
    MerlinTrialsPayload.Instance.ExchangeMerlinReward(this._shopInfo, this._exchangeCount, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      DicePayload.Instance.ShowCollectEffect(this._shopInfo.itemId, this._exchangeCount);
      this.OnClosePressed();
    }));
  }

  private void UpdateUI()
  {
    if (this._shopInfo == null)
      return;
    this._itemIcon.SetData(this._shopInfo.itemId, string.Empty, true);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._shopInfo.itemId);
    if (itemStaticInfo != null)
    {
      this._labelTitle.text = itemStaticInfo.LocName;
      this._labelItemDescription.text = itemStaticInfo.LocDescription;
    }
    BuilderFactory.Instance.HandyBuild((UIWidget) this._textureMerlinCoin, MerlinTrialsPayload.Instance.MerlinCoinImagePath, (System.Action<bool>) null, true, true, string.Empty);
  }

  public class Parameter : Popup.PopupParameter
  {
    public MerlinTrialsShopInfo shopInfo;
  }
}
