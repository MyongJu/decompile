﻿// Decompiled with JetBrains decompiler
// Type: DiceData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class DiceData
{
  private Dictionary<int, int> _scoreStatusDict = new Dictionary<int, int>();
  private Dictionary<int, DiceData.RewardData> _rewardsDict = new Dictionary<int, DiceData.RewardData>();
  protected bool _dailyIsClaim = true;
  private int _startTime;
  private int _endTime;
  private int _groupId;
  private long _score;
  private int _dicePlayTimes;
  private int _diceRewardRefreshTimes;
  protected long _currentGold;
  protected long _getItemDailyScore;
  protected long _getItemDailyNumber;
  protected long _currentDailyScore;
  protected long _goldClaimNumber;

  public int StartTime
  {
    get
    {
      return this._startTime;
    }
  }

  public int EndTime
  {
    get
    {
      return this._endTime;
    }
  }

  public int GroupId
  {
    get
    {
      return this._groupId;
    }
  }

  public int PropsId
  {
    get
    {
      DiceGroupInfo diceGroupInfo = ConfigManager.inst.DB_DiceGroup.Get(this._groupId);
      if (diceGroupInfo != null)
        return diceGroupInfo.propsId;
      return 0;
    }
  }

  public string PropsName
  {
    get
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.PropsId);
      if (itemStaticInfo != null)
        return itemStaticInfo.LocName;
      return string.Empty;
    }
  }

  public string PropsImagePath
  {
    get
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.PropsId);
      if (itemStaticInfo != null)
        return itemStaticInfo.ImagePath;
      return string.Empty;
    }
  }

  public long PropsCount
  {
    get
    {
      ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(this.PropsId);
      if (consumableItemData != null)
        return (long) consumableItemData.quantity;
      return 0;
    }
  }

  public long Score
  {
    get
    {
      return this._score;
    }
    set
    {
      this._score = value;
    }
  }

  public Dictionary<int, int> ScoreStatusDict
  {
    get
    {
      return this._scoreStatusDict;
    }
  }

  public int CanExchangeRewardCount
  {
    get
    {
      int num = 0;
      List<DiceScoreBoxInfo> diceScoreBoxList = ConfigManager.inst.DB_DiceScoreBox.DiceScoreBoxList;
      if (diceScoreBoxList != null)
      {
        for (int key = 0; key < diceScoreBoxList.Count; ++key)
        {
          if (this.ScoreStatusDict.ContainsKey(key) && this.ScoreStatusDict[key] <= 0 && (long) diceScoreBoxList[key].score <= this.Score)
            ++num;
        }
      }
      return num;
    }
  }

  public Dictionary<int, DiceData.RewardData> RewardsDict
  {
    get
    {
      return this._rewardsDict;
    }
  }

  public int DicePlayTimes
  {
    get
    {
      return this._dicePlayTimes;
    }
    set
    {
      this._dicePlayTimes = value;
    }
  }

  public int DiceRewardRefreshTimes
  {
    get
    {
      return this._diceRewardRefreshTimes;
    }
  }

  public virtual long CurrentGold
  {
    get
    {
      return this._currentGold;
    }
  }

  public virtual long GetItemDailyScore
  {
    get
    {
      return this._getItemDailyScore;
    }
  }

  public virtual long GetItemDailyNumber
  {
    get
    {
      return this._getItemDailyNumber;
    }
  }

  public virtual long CurrentDailyScore
  {
    get
    {
      return this._currentDailyScore;
    }
  }

  public virtual bool DailyIsClaim
  {
    get
    {
      return this._dailyIsClaim;
    }
  }

  public virtual long GoldClaimNumber
  {
    get
    {
      return this._goldClaimNumber;
    }
  }

  public int GetTargetItemGold
  {
    get
    {
      DiceGroupInfo diceGroupInfo = ConfigManager.inst.DB_DiceGroup.Get(this._groupId);
      if (diceGroupInfo != null)
        return diceGroupInfo.getPropsNeedGold;
      return 0;
    }
  }

  public int GetTargetItemGoldNumber
  {
    get
    {
      DiceGroupInfo diceGroupInfo = ConfigManager.inst.DB_DiceGroup.Get(this._groupId);
      if (diceGroupInfo != null)
        return diceGroupInfo.getPropsCount;
      return 0;
    }
  }

  public long RemainedGoldClaim
  {
    get
    {
      long num = 0;
      if (this.GetTargetItemGold != 0)
        num = this._currentGold / (long) this.GetTargetItemGold * (long) this.GetTargetItemGoldNumber - this._goldClaimNumber;
      return num;
    }
  }

  public bool HasFreeReward
  {
    get
    {
      return ((true ? 1 : 0) & (this._dailyIsClaim ? 0 : (this._currentDailyScore >= this._getItemDailyScore ? 1 : 0))) != 0 | this.RemainedGoldClaim > 0L;
    }
  }

  public void Decode(Hashtable data)
  {
    if (data == null)
      return;
    DatabaseTools.UpdateData(data, "start_time", ref this._startTime);
    DatabaseTools.UpdateData(data, "end_time", ref this._endTime);
    DatabaseTools.UpdateData(data, "group_id", ref this._groupId);
    DatabaseTools.UpdateData(data, "integral", ref this._score);
    DatabaseTools.UpdateData(data, "today_crap", ref this._dicePlayTimes);
    DatabaseTools.UpdateData(data, "today_refresh", ref this._diceRewardRefreshTimes);
    DatabaseTools.UpdateData(data, "get_item_daily_score", ref this._getItemDailyScore);
    DatabaseTools.UpdateData(data, "get_item_daily_number", ref this._getItemDailyNumber);
    DatabaseTools.UpdateData(data, "current_daily_score", ref this._currentDailyScore);
    DatabaseTools.UpdateData(data, "daily_is_claim", ref this._dailyIsClaim);
    DatabaseTools.UpdateData(data, "current_gold", ref this._currentGold);
    DatabaseTools.UpdateData(data, "gold_claim_number", ref this._goldClaimNumber);
    if (data.ContainsKey((object) "integral_reward"))
    {
      ArrayList arrayList = data[(object) "integral_reward"] as ArrayList;
      if (arrayList != null && arrayList.Count > 0)
      {
        this._scoreStatusDict.Clear();
        for (int key = 0; key < arrayList.Count; ++key)
        {
          int result = 0;
          int.TryParse(arrayList[key].ToString(), out result);
          this._scoreStatusDict.Add(key, result);
        }
      }
    }
    if (!data.ContainsKey((object) "reward_list"))
      return;
    ArrayList arrayList1 = data[(object) "reward_list"] as ArrayList;
    if (arrayList1 == null || arrayList1.Count <= 0)
      return;
    this._rewardsDict.Clear();
    for (int key = 0; key < arrayList1.Count; ++key)
      this._rewardsDict.Add(key, new DiceData.RewardData(arrayList1[key] as Hashtable));
  }

  public struct Params
  {
    public const string START_TIME = "start_time";
    public const string END_TIME = "end_time";
    public const string GROUP_ID = "group_id";
    public const string SCORE = "integral";
    public const string SCORE_REWARD = "integral_reward";
    public const string REWARD_LIST = "reward_list";
    public const string DICE_PLAY_TIMES = "today_crap";
    public const string REWARD_REFRESH_TIMES = "today_refresh";
    public const string CURRENT_GOLD = "current_gold";
    public const string GOLD_CLAIM_NUMBER = "gold_claim_number";
    public const string GET_ITEM_DAILY_SCORE = "get_item_daily_score";
    public const string GET_ITEM_DAILY_NUMBER = "get_item_daily_number";
    public const string CURRENT_DAILY_SCORE = "current_daily_score";
    public const string DAILY_IS_CLAIM = "daily_is_claim";
  }

  public class RewardData
  {
    private int _itemId;
    private int _amount;
    private int _multiple;
    private int _type;

    public RewardData(Hashtable data)
    {
      if (data == null)
        return;
      DatabaseTools.UpdateData(data, "item_id", ref this._itemId);
      DatabaseTools.UpdateData(data, "amount", ref this._amount);
      DatabaseTools.UpdateData(data, "type", ref this._type);
      DatabaseTools.UpdateData(data, "multiple", ref this._multiple);
    }

    public int ItemId
    {
      get
      {
        return this._itemId;
      }
    }

    public int Amount
    {
      get
      {
        return this._amount;
      }
    }

    public int Multiple
    {
      get
      {
        return this._multiple;
      }
    }

    public bool IsRewardScore
    {
      get
      {
        return this._type == 1;
      }
    }
  }
}
