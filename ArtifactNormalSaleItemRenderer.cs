﻿// Decompiled with JetBrains decompiler
// Type: ArtifactNormalSaleItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class ArtifactNormalSaleItemRenderer : MonoBehaviour
{
  public UILabel artifactName;
  public UITexture artifactFrame;
  public UITexture artifactTexture;
  public UILabel artifactSalePrice;
  public System.Action onBuyFinished;
  private ArtifactShopInfo _artifactShopInfo;
  private Color _artifactPriceColor;

  public void SetData(ArtifactShopInfo artifactShopInfo)
  {
    this._artifactShopInfo = artifactShopInfo;
    this._artifactPriceColor = this.artifactSalePrice.color;
    this.AddEventHandler();
    this.UpdateUI();
  }

  public void ClearData()
  {
    this.RemoveEventHandler();
  }

  public void OnBuyBtnPressed()
  {
    if (this._artifactShopInfo == null)
      return;
    if (PlayerData.inst.userData.currency.gold < this._artifactShopInfo.price)
    {
      Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
    }
    else
    {
      ArtifactSalePayload instance = ArtifactSalePayload.Instance;
      System.Action action = (System.Action) (() =>
      {
        if (this.onBuyFinished == null)
          return;
        this.onBuyFinished();
      });
      int internalId = this._artifactShopInfo.internalId;
      long price = this._artifactShopInfo.price;
      long specialPrice = -1;
      System.Action onBuyFinished = action;
      instance.ShowArtifactBuyConfirm(internalId, price, specialPrice, onBuyFinished);
    }
  }

  public void OnArtifactPressed()
  {
    if (this._artifactShopInfo == null)
      return;
    ArtifactSalePayload.Instance.ShowArtifactDetail(this._artifactShopInfo.artifactId);
  }

  private void UpdateUI()
  {
    if (this._artifactShopInfo == null)
      return;
    ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this._artifactShopInfo.artifactId);
    if (artifactInfo != null)
    {
      this.artifactName.text = artifactInfo.Name;
      BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactFrame, artifactInfo.TimeLimitedArtifactFramePath, (System.Action<bool>) null, true, false, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactTexture, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    }
    this.artifactSalePrice.text = Utils.FormatThousands(this._artifactShopInfo.price.ToString());
    if (PlayerData.inst.userData.currency.gold >= this._artifactShopInfo.price)
      return;
    this.artifactSalePrice.color = Color.red;
  }

  private void OnUserDataUpdated(long uid)
  {
    if (this._artifactShopInfo == null || uid != PlayerData.inst.uid)
      return;
    if (PlayerData.inst.userData.currency.gold < this._artifactShopInfo.price)
      this.artifactSalePrice.color = Color.red;
    else
      this.artifactSalePrice.color = this._artifactPriceColor;
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdated);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdated);
  }
}
