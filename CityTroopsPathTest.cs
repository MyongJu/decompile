﻿// Decompiled with JetBrains decompiler
// Type: CityTroopsPathTest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using SimplePath;
using UnityEngine;

public class CityTroopsPathTest : MonoBehaviour
{
  public Vector3 tmpStartPoint = new Vector3(-10f, -10f, 0.0f);
  public Vector3 tmpEndPoint = new Vector3(31f, 51f, 0.0f);
  public int troopCount = 5;
  public int troopTotalCount = 10;
  public WayPointGraph wayPointGraph;
  public Transform startPoint;
  public Transform endPoint;
  public ParadeGroundManager groundManager;
  public ParadeGroundSlot groundSlot;
  public TroopType troopType;
  public int infantryCount;
  public int rangedCount;
  public int cavarlyCount;
  public int artyCloseCount;
  public int artyFarCount;

  public void Update()
  {
    if (Input.GetKeyDown(KeyCode.Q))
      CityTroopsAnimManager.inst.SendTroops(this.wayPointGraph, this.startPoint.transform.position, this.endPoint.transform.position, new CityTroopsAnimControler.Parameter()
      {
        startTime = Time.time,
        troopsCount = 10,
        troopType = TroopType.class_infantry
      });
    if (Input.GetKeyDown(KeyCode.W))
    {
      for (int index = 0; index < 5; ++index)
        CityTroopsAnimManager.inst.SendTroops(this.wayPointGraph, this.startPoint.transform.position, this.endPoint.transform.position, new CityTroopsAnimControler.Parameter()
        {
          startTime = Time.time + (float) index * 1.5f,
          troopsCount = 10,
          troopType = TroopType.class_infantry
        });
    }
    if (Input.GetKeyDown(KeyCode.R))
    {
      this.groundSlot.unitType = this.troopType;
      this.groundSlot.totalCount = this.troopTotalCount;
      this.groundSlot.troopCount = this.troopCount;
    }
    if (Input.GetKeyDown(KeyCode.T))
    {
      ParadeGroundManager.troopsParam[TroopType.class_infantry] = this.infantryCount;
      ParadeGroundManager.troopsParam[TroopType.class_cavalry] = this.cavarlyCount;
      ParadeGroundManager.troopsParam[TroopType.class_ranged] = this.rangedCount;
      ParadeGroundManager.troopsParam[TroopType.class_artyclose] = this.artyCloseCount;
      ParadeGroundManager.troopsParam[TroopType.class_artyfar] = this.artyFarCount;
      this.groundManager.SetParadeGround(ParadeGroundManager.troopsParam);
    }
    if (Input.GetKeyDown(KeyCode.A))
      CitadelSystem.inst.cityTroopAnimManager.SendTroops(CitadelSystem.inst.cityTroopAnimManager.cityPath, CitadelSystem.inst.cityTroopAnimManager.startPoint1.position, CitadelSystem.inst.cityTroopAnimManager.endPoint.position, new CityTroopsAnimControler.Parameter()
      {
        troopType = TroopType.class_infantry,
        troopsCount = 100
      });
    if (Input.GetKeyDown(KeyCode.S))
      CitadelSystem.inst.cityTroopAnimManager.SendTroops(CitadelSystem.inst.cityTroopAnimManager.cityPath, CitadelSystem.inst.cityTroopAnimManager.startPoint2.position, CitadelSystem.inst.cityTroopAnimManager.endPoint.position, new CityTroopsAnimControler.Parameter()
      {
        troopType = TroopType.class_ranged,
        troopsCount = 100
      });
    if (Input.GetKeyDown(KeyCode.D))
      CitadelSystem.inst.cityTroopAnimManager.SendTroops(CitadelSystem.inst.cityTroopAnimManager.cityPath, CitadelSystem.inst.cityTroopAnimManager.startPoint3.position, CitadelSystem.inst.cityTroopAnimManager.endPoint.position, new CityTroopsAnimControler.Parameter()
      {
        troopType = TroopType.class_cavalry,
        troopsCount = 100
      });
    if (!Input.GetKeyDown(KeyCode.F))
      return;
    CitadelSystem.inst.cityTroopAnimManager.SendTroops(CitadelSystem.inst.cityTroopAnimManager.cityPath, CitadelSystem.inst.cityTroopAnimManager.startPoint4.position, CitadelSystem.inst.cityTroopAnimManager.endPoint.position, new CityTroopsAnimControler.Parameter()
    {
      troopType = TroopType.class_artyfar,
      troopsCount = 100
    });
  }
}
