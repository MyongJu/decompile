﻿// Decompiled with JetBrains decompiler
// Type: QuestResourcePresent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;

public class QuestResourcePresent : BasePresent
{
  private static float ONE_HOUR = 3600f;
  private double gen_boost;
  private double generateSpeed;
  private double baseGenSpeed;

  public override void Refresh()
  {
    if (this.Data.Type == PresentData.DataType.Quest)
    {
      string formula = this.Formula;
      int num1 = formula.IndexOf('@');
      if (formula.IndexOf("resource_collect") > -1)
      {
        this.UseStats();
      }
      else
      {
        string type = formula;
        if (num1 > -1)
        {
          int num2 = formula.IndexOf('=');
          if (num2 <= -1)
            return;
          this.ShowCalcResProgress(formula.Substring(num2 + 1, formula.Length - num2 - 1));
        }
        else
          this.ShowCalcResProgress(type);
      }
    }
    else if (this.Formula == "resource_collect@type=all")
    {
      int count = this.Count;
      int num = (int) DBManager.inst.DB_UserProfileDB.Get("resources_collected");
      if (num > count)
        num = count;
      this.ProgressValue = (float) num / (float) count;
      this.ProgressContent = num.ToString() + "/" + (object) count;
    }
    else
      this.UseStats();
  }

  private string GetBuildType(string type)
  {
    string str = string.Empty;
    string key = type;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (QuestResourcePresent.\u003C\u003Ef__switch\u0024mapA7 == null)
      {
        // ISSUE: reference to a compiler-generated field
        QuestResourcePresent.\u003C\u003Ef__switch\u0024mapA7 = new Dictionary<string, int>(4)
        {
          {
            "food",
            0
          },
          {
            "wood",
            1
          },
          {
            "ore",
            2
          },
          {
            "silver",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (QuestResourcePresent.\u003C\u003Ef__switch\u0024mapA7.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            str = "farm";
            break;
          case 1:
            str = "lumber_mill";
            break;
          case 2:
            str = "mine";
            break;
          case 3:
            str = "house";
            break;
        }
      }
    }
    return str;
  }

  private void ShowCalcResProgress(string type)
  {
    int count = this.Count;
    List<CityManager.BuildingItem> buildingsByType = CityManager.inst.GetBuildingsByType(this.GetBuildType(type));
    double num = 0.0;
    for (int index = 0; index < buildingsByType.Count; ++index)
    {
      CityManager.BuildingItem buildingItem = buildingsByType[index];
      ResouceGenSpeedData resouceGenSpeedData = new ResouceGenSpeedData();
      resouceGenSpeedData.SetData(buildingsByType[index]);
      num += resouceGenSpeedData.GenerateSpeedInHour;
    }
    if (num > (double) count)
      num = (double) count;
    this.ProgressValue = (float) num / (float) count;
    this.ProgressContent = ((int) num).ToString() + "/" + (object) count;
  }

  private string GetKey(string type)
  {
    string str = string.Empty;
    string key = type;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (QuestResourcePresent.\u003C\u003Ef__switch\u0024mapA8 == null)
      {
        // ISSUE: reference to a compiler-generated field
        QuestResourcePresent.\u003C\u003Ef__switch\u0024mapA8 = new Dictionary<string, int>(4)
        {
          {
            "food",
            0
          },
          {
            "wood",
            1
          },
          {
            "ore",
            2
          },
          {
            "silver",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (QuestResourcePresent.\u003C\u003Ef__switch\u0024mapA8.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            str = "calc_food_generation";
            break;
          case 1:
            str = "calc_wood_generation";
            break;
          case 2:
            str = "calc_ore_generation";
            break;
          case 3:
            str = "calc_silver_generation";
            break;
        }
      }
    }
    return str;
  }

  public override int IconType
  {
    get
    {
      if (this.Data.Type == PresentData.DataType.Achievement)
        return base.IconType;
      return 2;
    }
  }
}
