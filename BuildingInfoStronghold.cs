﻿// Decompiled with JetBrains decompiler
// Type: BuildingInfoStronghold
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingInfoStronghold : UI.Dialog
{
  public BuildingInfoStronghold.DetailsCityPanel mDetailsCityPanel = new BuildingInfoStronghold.DetailsCityPanel();
  private List<GameObject> items = new List<GameObject>();
  public UIWidget mBuildingIcon;
  public float zoomFocusDis;
  public GameObject mBuildingItemHeaderPrefab;
  public GameObject mBuildingItemPrefab;
  public GameObject mCityPanel;
  public GameObject mCancelUpgrade;
  public GameObject mBuildingMove;
  public UIGrid mButtomGroup;
  public UILabel mStrongholdLevel;
  public UILabel benefitNameLabel;
  public UILabel benefitValueLabel;
  protected BuildingInfo buildingInfo;
  protected CityManager.BuildingItem buildingItem;

  public void Update()
  {
    if (BuildingController.mSelectedBuildingItem == null)
      return;
    long mBuildingJobId = BuildingController.mSelectedBuildingItem.mBuildingJobID;
    if (mBuildingJobId > 0L && (bool) JobManager.Instance.GetUnfinishedJob(mBuildingJobId))
    {
      this.mCancelUpgrade.SetActive(true);
      this.mBuildingMove.SetActive(false);
    }
    else
    {
      this.mCancelUpgrade.SetActive(false);
      this.mBuildingMove.SetActive(true);
    }
    this.mButtomGroup.repositionNow = true;
  }

  public void Show()
  {
    CitadelSystem.inst.FocusConstructionBuilding(BuildingController.mSelectedBuildingController.gameObject, this.mBuildingIcon, this.zoomFocusDis);
    using (List<GameObject>.Enumerator enumerator = this.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current);
    }
    this.items.Clear();
    ArrayList ruralBuildings = new ArrayList();
    ArrayList urbanBuildings = new ArrayList();
    CityManager.inst.GetBuildingLevelData(ruralBuildings, urbanBuildings);
    this.mBuildingItemHeaderPrefab.GetComponent<LabelBinder>().SetLabels((object) Utils.XLAT("stronghold_buildinfo_uppercase_bonuses_urban_building"));
    float y = this.mBuildingItemPrefab.transform.localPosition.y;
    float height = (float) this.mBuildingItemPrefab.GetComponent<UIWidget>().height;
    foreach (StrongholdInfoDlg.BuildingLevelInfo buildingLevelInfo in urbanBuildings)
    {
      GameObject gob = Utils.DuplicateGOB(this.mBuildingItemPrefab);
      Utils.SetLPY(gob, y);
      y -= height;
      gob.SetActive(true);
      gob.GetComponent<StrongholdBuildingInfoItem>().SetDetails(Utils.XLAT(buildingLevelInfo.mName), buildingLevelInfo.mLevel);
      this.items.Add(gob);
    }
    GameObject gob1 = Utils.DuplicateGOB(this.mBuildingItemHeaderPrefab);
    gob1.GetComponent<LabelBinder>().SetLabels((object) Utils.XLAT("stronghold_buildinfo_uppercase_bonuses_rural_building"));
    Utils.SetLPY(gob1, y);
    float newY = y - (float) gob1.GetComponent<UIWidget>().height;
    this.items.Add(gob1);
    foreach (StrongholdInfoDlg.BuildingLevelInfo buildingLevelInfo in ruralBuildings)
    {
      GameObject gob2 = Utils.DuplicateGOB(this.mBuildingItemPrefab);
      Utils.SetLPY(gob2, newY);
      newY -= height;
      gob2.SetActive(true);
      gob2.GetComponent<StrongholdBuildingInfoItem>().SetDetails(Utils.XLAT(buildingLevelInfo.mName), buildingLevelInfo.mLevel);
      this.items.Add(gob2);
    }
    string str = "[85847e]K:[f4b52d] " + CityManager.inst.Location.K.ToString() + " [85847e]X:[f4b52d] " + CityManager.inst.Location.X.ToString() + " [85847e]Y:[f4b52d] " + CityManager.inst.Location.Y.ToString();
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    this.DrawBenefit(this.buildingInfo.Benefit_Value_1, this.benefitNameLabel, this.benefitValueLabel, int.Parse(this.buildingInfo.Benefit_ID_1), "prop_march_capacity_base_value", "calc_march_capacity", (UITexture) null);
    List<long> targetListByType = DBManager.inst.DB_March.GetTargetListByType(MarchData.MarchType.reinforce);
    int num1 = 0;
    using (List<long>.Enumerator enumerator = targetListByType.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        long current = enumerator.Current;
        num1 += (int) current;
      }
    }
    long num2 = cityData.totalTroops.totalTroopsCount + cityData.totalTroops.totalTrapsCount + (long) num1;
    long num3 = cityData.totalTroops.totalTroopsCount + cityData.totalTroops.totalTrapsCount;
    this.mStrongholdLevel.text = string.Format("LV. {0}[aaaaaa]/{1}", (object) CityManager.inst.GetHighestBuildingLevelFor("stronghold"), (object) ConfigManager.inst._DB_Building.GetMaxLevelByType("stronghold"));
    GameEngine.Instance.events.OnCityRenamed += new System.Action<string>(this.OnCityRenamed);
  }

  public void OnBackBtnPressed()
  {
    GameEngine.Instance.events.OnCityRenamed -= new System.Action<string>(this.OnCityRenamed);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnRenameCityBtnPressed()
  {
    UIManager.inst.OpenDlg("CityInfoRename", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnMoreInfoBtnPressed()
  {
    UIManager.inst.OpenPopup("BuildingInfo/BuildingInfoMoreInfoBasePopup", (Popup.PopupParameter) new BuildingInfoMoreInfoBasePopup.Parameter()
    {
      info = this.buildingInfo
    });
  }

  public void OnMovingBuildingBtnPressed()
  {
    BuildingMoveManager.Instance.Init();
    BuildingMoveManager.Instance.ShowMovableCircle();
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnCancelUpgradeBtnPressed()
  {
    PopupManager.Instance.Open<MessageBoxWithIconAnd2Buttons>("MessageBoxWithIconAnd2Buttons").Initialize(Utils.XLAT("construction_uppercase_cancel_upgrade"), ScriptLocalization.GetWithPara("construction_cancel_upgrade_description", new Dictionary<string, string>()
    {
      {
        "building_name",
        Utils.XLAT(this.buildingInfo.Building_LOC_ID)
      }
    }, true), Utils.XLAT("construction_cancel_upgrade_warning"), Utils.XLAT("id_uppercase_yes"), Utils.XLAT("id_uppercase_no"), new System.Action(this.OnCancelConstructionYes), (System.Action) null, string.Empty);
  }

  private void OnCancelConstructionYes()
  {
    CityManager.inst.CancelBuild(BuildingController.mSelectedBuildingItem.mID, (System.Action) (() => UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null)));
  }

  public void OnCityRenamed(string newName)
  {
    this.mDetailsCityPanel.mCityName.text = Utils.XLAT("City Name") + ": [aaaaaa]" + newName;
  }

  private void DrawBenefit(double benifitValue, UILabel nameLabel, UILabel valueLabel, int benefitInternalID, string benefitID, string caclID, UITexture texture = null)
  {
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitInternalID];
    nameLabel.text = dbProperty.Name;
    if ((UnityEngine.Object) texture != (UnityEngine.Object) null)
      BuilderFactory.Instance.HandyBuild((UIWidget) texture, dbProperty.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    double num = benifitValue;
    float finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(DBManager.inst.DB_Local_Benefit.Get(benefitID).total, caclID);
    string empty = string.Empty;
    if (dbProperty.Format == PropertyDefinition.FormatType.Percent)
      valueLabel.text = dbProperty.ConvertToDisplayString(num, false, true) + empty + dbProperty.ConvertToDisplayString(Math.Abs((double) finalData - num), true, true);
    else
      valueLabel.text = dbProperty.ConvertToDisplayString(num, false, true) + empty + dbProperty.ConvertToDisplayString(Math.Abs((double) finalData - num), true, true);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.buildingItem = (orgParam as BuildingInfoStronghold.Parameter).buildingItem;
    this.buildingInfo = ConfigManager.inst.DB_Building.GetData(this.buildingItem.mType + "_" + this.buildingItem.mLevel.ToString());
    this.Show();
  }

  [Serializable]
  public class DetailsCityPanel
  {
    public UILabel mCityName;
    public UILabel mKingdom;
    public UILabel mLocation;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public CityManager.BuildingItem buildingItem;
  }
}
