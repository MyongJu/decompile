﻿// Decompiled with JetBrains decompiler
// Type: DismissTroopConfirmPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;
using UnityEngine;

public class DismissTroopConfirmPopup : Popup
{
  public System.Action onOK;
  public System.Action onNO;
  [SerializeField]
  private UILabel m_TitleLabel;
  [SerializeField]
  private UILabel m_ContentLabel;
  [SerializeField]
  private UILabel m_OkayLabel;
  [SerializeField]
  private UIButton m_OkayButton;
  [SerializeField]
  private UILabel m_NoayLabel;
  [SerializeField]
  private UIButton m_NoayButton;
  [SerializeField]
  private UIButton m_CloseButton;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    DismissTroopConfirmPopup.Parameter parameter = orgParam as DismissTroopConfirmPopup.Parameter;
    base.OnShow(orgParam);
    this.Initialize(parameter.titleKey, parameter.contentKey, parameter.okKey, parameter.noKey, parameter.okCallback, parameter.noCallBack, parameter.unit);
  }

  public void Initialize(string titleKey, string contentKey, string okKey, string noKey, System.Action okCallback, System.Action noCallBack, Unit_StatisticsInfo unit)
  {
    this.m_TitleLabel.text = ScriptLocalization.Get(titleKey, true);
    this.m_ContentLabel.text = ScriptLocalization.Get(contentKey, true);
    this.m_OkayLabel.text = ScriptLocalization.Get(okKey, true);
    this.m_NoayLabel.text = ScriptLocalization.Get(noKey, true);
    this.onOK = okCallback;
    this.onNO = noCallBack;
  }

  public void OnOK()
  {
    if (this.onOK != null)
      this.onOK();
    this.OnClose();
  }

  public void OnNo()
  {
    if (this.onNO != null)
      this.onNO();
    this.OnClose();
  }

  public void OnClose()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string titleKey;
    public string contentKey;
    public string okKey;
    public string noKey;
    public System.Action okCallback;
    public System.Action noCallBack;
    public Unit_StatisticsInfo unit;
  }
}
