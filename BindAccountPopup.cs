﻿// Decompiled with JetBrains decompiler
// Type: BindAccountPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class BindAccountPopup : Popup
{
  private System.Action closeCallBack;

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    if (this.closeCallBack == null)
      return;
    this.closeCallBack();
    this.closeCallBack = (System.Action) null;
  }

  public void OnBindHandler()
  {
    this.OnCloseHandler();
    UIManager.inst.OpenPopup("Account/AccountManagementPopup", (Popup.PopupParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    BindAccountPopup.Paramer paramer = orgParam as BindAccountPopup.Paramer;
    if (paramer == null)
      return;
    this.closeCallBack = paramer.closeCallBack;
  }

  public class Paramer : Popup.PopupParameter
  {
    public System.Action closeCallBack;
  }
}
