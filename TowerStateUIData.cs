﻿// Decompiled with JetBrains decompiler
// Type: TowerStateUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class TowerStateUIData : StateUIBaseData
{
  private WonderTowerData _tower;

  public override StateUIBaseData.DataType Type
  {
    get
    {
      return StateUIBaseData.DataType.Tower;
    }
  }

  public override string Title
  {
    get
    {
      if (this._tower != null)
        return this._tower.Name;
      return string.Empty;
    }
  }

  public override long WorldId
  {
    get
    {
      return (long) this.Tower.WORLD_ID;
    }
  }

  protected override long OccupyUserId
  {
    get
    {
      return this.Tower.OWNER_UID;
    }
  }

  protected override long OccupyAllianceId
  {
    get
    {
      return this.Tower.OWNER_ALLIANCE_ID;
    }
  }

  public override int RemainTime
  {
    get
    {
      int num = (int) this.Tower.StateChangeTime - NetServerTime.inst.ServerTimestamp;
      if (num < 0)
        num = 0;
      return num;
    }
  }

  private WonderTowerData Tower
  {
    get
    {
      if (this._tower == null)
        this._tower = this.Data as WonderTowerData;
      return this._tower;
    }
  }
}
