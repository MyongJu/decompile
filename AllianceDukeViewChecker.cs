﻿// Decompiled with JetBrains decompiler
// Type: AllianceDukeViewChecker
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class AllianceDukeViewChecker : MonoBehaviour
{
  public GameObject usurpBtn;
  public UIGrid grid;

  private void OnEnable()
  {
    this.usurpBtn.SetActive((long) NetServerTime.inst.ServerTimestamp - DBManager.inst.DB_User.Get(DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId).creatorId).lastLogin > 604800L);
    this.grid.Reposition();
  }
}
