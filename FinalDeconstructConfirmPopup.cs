﻿// Decompiled with JetBrains decompiler
// Type: FinalDeconstructConfirmPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class FinalDeconstructConfirmPopup : Popup
{
  private const string BUILDING_PREFAB_PATH = "Prefab/UI/BuildingPrefab/";
  public UILabel mTitleLabel;
  public UILabel buildTitle;
  public UILabel mTimeValue;
  public UITexture mIcon;
  public UILabel mLevelValue;
  public UILabel mLosePowerMsg;
  public GameObject mDestructTimeLabel;
  public GameObject mDestructTimeValue;
  public GameObject mDestructTimeIcon;
  private System.Action<bool> _onYesBtn;
  public UIButton yesBtn;
  public UIButton noBtn;
  public UIButton closeBtn;
  private bool inst;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    FinalDeconstructConfirmPopup.Parameter parameter = orgParam as FinalDeconstructConfirmPopup.Parameter;
    base.OnShow(orgParam);
    this.SetDetails(parameter.pinfo, parameter.bInstant, parameter.instanceConfirm);
  }

  public void SetDetails(BuildingInfo info, bool bInstant, System.Action<bool> onYesBtn)
  {
    this.inst = bInstant;
    this.buildTitle.text = Utils.XLAT(info.Type + "_name");
    this.mTimeValue.text = Utils.ConvertSecsToString(info.Build_Time / 2.0);
    this.mLevelValue.text = string.Format("LV. {0}[aaaaaa]/30", (object) info.Building_Lvl);
    this.mLosePowerMsg.text = Utils.XLAT("deconstruct_power_msg").Replace("{power}", this.GetLosePower(info).ToString());
    CityManager.inst.GetIconLevelFromBuildingLevel(info.Building_Lvl, (string) null);
    GameObject prefab = AssetManager.Instance.HandyLoad("Prefab/UI/BuildingPrefab/" + string.Format("ui_{0}{1}1", (object) info.Type, (object) "_l"), (System.Type) null) as GameObject;
    if ((UnityEngine.Object) prefab != (UnityEngine.Object) null)
    {
      for (int index = 0; index < this.mIcon.transform.childCount; ++index)
        UnityEngine.Object.Destroy((UnityEngine.Object) this.mIcon.transform.GetChild(index).gameObject);
      NGUITools.AddChild(this.mIcon.gameObject, prefab).GetComponent<UI2DSprite>().depth = this.mIcon.depth + 1;
    }
    this.mDestructTimeIcon.SetActive(!bInstant);
    this.mDestructTimeValue.SetActive(!bInstant);
    this.mDestructTimeLabel.SetActive(!bInstant);
    this._onYesBtn = onYesBtn;
    this.yesBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnYesBtnPressed)));
    this.noBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnNoBtnPressed)));
    this.closeBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnNoBtnPressed)));
  }

  private int GetLosePower(BuildingInfo info)
  {
    int buildingLvl = info.Building_Lvl;
    int num = 0;
    for (int level = 1; level <= buildingLvl; ++level)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(info.Type, level);
      num += data.Power;
    }
    return num;
  }

  private void OnNoBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnYesBtnPressed()
  {
    if (this._onYesBtn != null)
      this._onYesBtn(this.inst);
    AudioManager.Instance.PlaySound("sfx_rural_deconstruct", false);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public BuildingInfo pinfo;
    public bool bInstant;
    public System.Action<bool> instanceConfirm;
  }
}
