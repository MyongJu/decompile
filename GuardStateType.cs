﻿// Decompiled with JetBrains decompiler
// Type: GuardStateType
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public static class GuardStateType
{
  public const string IDLE = "guard_idle";
  public const string DEATH = "guard_death";
  public const string DEATH_END = "guard_death_end";
  public const string ATTACK = "guard_attack";
  public const string UNDER_ATTACK = "guard_under_attack";
  public const string SPELL = "guard_spell";
  public const string ENHANCE = "guard_enhance";
}
