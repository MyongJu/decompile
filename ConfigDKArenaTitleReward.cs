﻿// Decompiled with JetBrains decompiler
// Type: ConfigDKArenaTitleReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigDKArenaTitleReward
{
  private ConfigParse _parse = new ConfigParse();
  private Dictionary<string, DKArenaTitleReward> _dataByID;
  private Dictionary<int, DKArenaTitleReward> _dataByInternalID;
  private List<DKArenaTitleReward> _dataList;

  public void BuildDB(object res)
  {
    this._parse.Parse<DKArenaTitleReward, string>(res as Hashtable, "ID", out this._dataByID, out this._dataByInternalID);
    this._dataList = new List<DKArenaTitleReward>();
    Dictionary<string, DKArenaTitleReward>.Enumerator enumerator = this._dataByID.GetEnumerator();
    while (enumerator.MoveNext())
      this._dataList.Add(enumerator.Current.Value);
    this._dataList.Sort(new Comparison<DKArenaTitleReward>(this.Compare));
  }

  public List<DKArenaTitleReward> GetTotals()
  {
    this._dataList = new List<DKArenaTitleReward>();
    Dictionary<string, DKArenaTitleReward>.Enumerator enumerator = this._dataByID.GetEnumerator();
    List<DKArenaTitleReward> arenaTitleRewardList = new List<DKArenaTitleReward>();
    while (enumerator.MoveNext())
    {
      if (!DBManager.inst.DB_DKArenaDB.GetCurrentData().IsClaim(enumerator.Current.Key))
        this._dataList.Add(enumerator.Current.Value);
      else
        arenaTitleRewardList.Add(enumerator.Current.Value);
    }
    this._dataList.Sort(new Comparison<DKArenaTitleReward>(this.Compare));
    arenaTitleRewardList.Sort(new Comparison<DKArenaTitleReward>(this.Compare));
    for (int index = 0; index < arenaTitleRewardList.Count; ++index)
      this._dataList.Add(arenaTitleRewardList[index]);
    return this._dataList;
  }

  private int Compare(DKArenaTitleReward a, DKArenaTitleReward b)
  {
    return a.ScoreMin.CompareTo(b.ScoreMin);
  }

  public DKArenaTitleReward GetTitleRewardByScore(long score)
  {
    this._dataList.Sort(new Comparison<DKArenaTitleReward>(this.Compare));
    for (int index = 0; index < this._dataList.Count; ++index)
    {
      if (score <= (long) this._dataList[index].ScoreMax)
        return this._dataList[index];
    }
    return (DKArenaTitleReward) null;
  }

  public string GetTitleImage(long score)
  {
    DKArenaTitleReward titleRewardByScore = this.GetTitleRewardByScore(score);
    if (titleRewardByScore != null)
      return titleRewardByScore.ImagePath;
    return string.Empty;
  }

  public void Clear()
  {
    if (this._dataByID != null)
    {
      this._dataByID.Clear();
      this._dataByID = (Dictionary<string, DKArenaTitleReward>) null;
    }
    if (this._dataByInternalID == null)
      return;
    this._dataByInternalID.Clear();
    this._dataByInternalID = (Dictionary<int, DKArenaTitleReward>) null;
  }

  public bool Contains(int internalId)
  {
    return this._dataByInternalID.ContainsKey(internalId);
  }

  public bool Contains(string id)
  {
    return this._dataByID.ContainsKey(id);
  }

  public DKArenaTitleReward GetItem(int internalId)
  {
    if (this._dataByInternalID != null && this._dataByInternalID.ContainsKey(internalId))
      return this._dataByInternalID[internalId];
    return (DKArenaTitleReward) null;
  }

  public DKArenaTitleReward GetItem(string id)
  {
    if (this._dataByID.ContainsKey(id))
      return this._dataByID[id];
    return (DKArenaTitleReward) null;
  }
}
