﻿// Decompiled with JetBrains decompiler
// Type: ConstructionInfoComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ConstructionInfoComponent : MonoBehaviour
{
  private const int SCROLL_TIP_SHOW_COUNT = 2;
  public UILabel status1Label;
  public UIGrid grid;
  public UIScrollView scrollView;
  public GameObject scrollTip;
  public Icon researchTemplate;
  public Icon unitTemplate;
  private BuildingInfo buildingInfo;
  private GameObject rewardObject;
  private bool _isChanged;

  public void FeedContent(BuildingInfo buildingInfo, GameObject rewardObject, bool isChanged = false)
  {
    this.buildingInfo = buildingInfo;
    this.rewardObject = rewardObject;
    this._isChanged = isChanged;
    string type = buildingInfo.Type;
    if (type != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (ConstructionInfoComponent.\u003C\u003Ef__switch\u0024map3E == null)
      {
        // ISSUE: reference to a compiler-generated field
        ConstructionInfoComponent.\u003C\u003Ef__switch\u0024map3E = new Dictionary<string, int>(7)
        {
          {
            "university",
            0
          },
          {
            "barracks",
            1
          },
          {
            "range",
            1
          },
          {
            "stables",
            1
          },
          {
            "sanctum",
            1
          },
          {
            "workshop",
            1
          },
          {
            "fortress",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (ConstructionInfoComponent.\u003C\u003Ef__switch\u0024map3E.TryGetValue(type, out num))
      {
        switch (num)
        {
          case 0:
            this.DrawUniversity();
            return;
          case 1:
            this.DrawTroop();
            return;
          case 2:
            this.DrawTrap();
            return;
        }
      }
    }
    this.gameObject.SetActive(false);
  }

  private void DrawUniversity()
  {
    this.gameObject.SetActive(true);
    List<TechLevel> unlockTechLevel = ResearchManager.inst.GetUnlockTechLevel(this.buildingInfo.internalId);
    if (unlockTechLevel.Count > 0)
    {
      this.status1Label.text = Utils.XLAT("upgrade_unlocks_research");
      this.ClearGrid();
      for (int index = 0; index < unlockTechLevel.Count; ++index)
      {
        GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.researchTemplate.gameObject);
        gameObject.SetActive(true);
        gameObject.GetComponent<Icon>().FeedData((IComponentData) new IconData(unlockTechLevel[index].Tech.IconPath, new string[1]
        {
          unlockTechLevel[index].Name
        }));
      }
      this.scrollTip.SetActive(unlockTechLevel.Count > 2);
    }
    else
    {
      List<TechLevel> techLevelList = new List<TechLevel>();
      BuildingInfo buildingInfo = (BuildingInfo) null;
      for (int level = this.buildingInfo.Building_Lvl + 1; level <= 30; ++level)
      {
        buildingInfo = ConfigManager.inst.DB_Building.GetData(this.buildingInfo.Type, level);
        techLevelList = ResearchManager.inst.GetUnlockTechLevel(buildingInfo.internalId);
        if (techLevelList.Count > 0)
          break;
      }
      this.ClearGrid();
      for (int index = 0; index < techLevelList.Count; ++index)
      {
        GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.researchTemplate.gameObject);
        gameObject.SetActive(true);
        gameObject.GetComponent<Icon>().FeedData((IComponentData) new IconData(techLevelList[index].Tech.IconPath, new string[1]
        {
          techLevelList[index].Name
        }));
      }
      this.scrollTip.SetActive(techLevelList.Count > 2);
      if (buildingInfo != null)
        this.status1Label.text = string.Format(Utils.XLAT("upgrade_level_unlocks_troops_description"), (object) buildingInfo.Building_Lvl);
      else
        this.gameObject.SetActive(false);
    }
    this.grid.Reposition();
    this.scrollView.ResetPosition();
  }

  private void DrawTroop()
  {
    this.gameObject.SetActive(true);
    List<Unit_StatisticsInfo> unlockedByBuilding = ConfigManager.inst.DB_Unit_Statistics.GetUnitsUnlockedByBuilding(this.buildingInfo.Type, this.buildingInfo.Building_Lvl);
    if (unlockedByBuilding.Count > 0)
    {
      this.status1Label.text = Utils.XLAT("upgrade_unlocks_troops");
      this.rewardObject.SetActive(false);
      this.ClearGrid();
      for (int index = 0; index < unlockedByBuilding.Count; ++index)
      {
        GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.unitTemplate.gameObject);
        gameObject.SetActive(true);
        gameObject.GetComponent<Icon>().FeedData((IComponentData) new IconData(unlockedByBuilding[index].Troop_ICON, new string[2]
        {
          Utils.XLAT(unlockedByBuilding[index].Troop_Name_LOC_ID),
          Utils.GetRomaNumeralsBelowTen(unlockedByBuilding[index].Troop_Tier)
        }));
      }
      this.scrollTip.SetActive(unlockedByBuilding.Count > 2);
    }
    else
    {
      List<Unit_StatisticsInfo> unitStatisticsInfoList = new List<Unit_StatisticsInfo>();
      BuildingInfo buildingInfo = (BuildingInfo) null;
      int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(this.buildingInfo.Type);
      for (int level = this.buildingInfo.Building_Lvl + 1; level <= maxLevelByType; ++level)
      {
        buildingInfo = ConfigManager.inst.DB_Building.GetData(this.buildingInfo.Type, level);
        unitStatisticsInfoList = ConfigManager.inst.DB_Unit_Statistics.GetUnitsUnlockedByBuilding(buildingInfo.Type, buildingInfo.Building_Lvl);
        if (unitStatisticsInfoList.Count > 0)
          break;
      }
      this.ClearGrid();
      for (int index = 0; index < unitStatisticsInfoList.Count; ++index)
      {
        GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.unitTemplate.gameObject);
        gameObject.SetActive(true);
        gameObject.GetComponent<Icon>().FeedData((IComponentData) new IconData(unitStatisticsInfoList[index].Troop_ICON, new string[2]
        {
          Utils.XLAT(unitStatisticsInfoList[index].Troop_Name_LOC_ID),
          Utils.GetRomaNumeralsBelowTen(unitStatisticsInfoList[index].Troop_Tier)
        }));
      }
      this.scrollTip.SetActive(unitStatisticsInfoList.Count > 2);
      if (buildingInfo != null && !this._isChanged)
        this.status1Label.text = string.Format(Utils.XLAT("upgrade_level_unlocks_troops_description"), (object) buildingInfo.Building_Lvl);
      else
        this.gameObject.SetActive(false);
      this.rewardObject.SetActive(unitStatisticsInfoList.Count <= 0 || this._isChanged);
    }
    this.grid.Reposition();
    this.scrollView.ResetPosition();
  }

  private void DrawTrap()
  {
    this.gameObject.SetActive(true);
    List<Unit_StatisticsInfo> unlockedByBuilding = ConfigManager.inst.DB_Unit_Statistics.GetUnitsUnlockedByBuilding(this.buildingInfo.Type, this.buildingInfo.Building_Lvl);
    if (unlockedByBuilding.Count > 0)
    {
      this.rewardObject.SetActive(false);
      this.status1Label.text = Utils.XLAT("upgrade_unlocks_traps");
      this.ClearGrid();
      for (int index = 0; index < unlockedByBuilding.Count; ++index)
      {
        GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.unitTemplate.gameObject);
        gameObject.SetActive(true);
        gameObject.GetComponent<Icon>().FeedData((IComponentData) new IconData(unlockedByBuilding[index].Troop_ICON, new string[2]
        {
          Utils.XLAT(unlockedByBuilding[index].Troop_Name_LOC_ID),
          Utils.GetRomaNumeralsBelowTen(unlockedByBuilding[index].Troop_Tier)
        }));
      }
      this.scrollTip.SetActive(unlockedByBuilding.Count > 2);
    }
    else
      this.gameObject.SetActive(false);
    this.grid.Reposition();
    this.scrollView.ResetPosition();
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.grid);
  }
}
