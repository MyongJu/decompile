﻿// Decompiled with JetBrains decompiler
// Type: PitMarchSpeedUpItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PitMarchSpeedUpItemRenderer : MonoBehaviour
{
  public UILabel m_ItemName;
  public UILabel m_ItemDesc;
  public UILabel m_ItemPrice;
  public UIButton m_BuyButton;
  private System.Action m_Callback;

  public void Initialize(System.Action callback)
  {
    this.m_Callback = callback;
    this.UpdateUI();
  }

  public void UpdateUI()
  {
    int pitSpeedUpCost = PitExplorePayload.Instance.PitSpeedUpCost;
    this.m_ItemPrice.text = Utils.FormatThousands(pitSpeedUpCost.ToString());
    if (PlayerData.inst.hostPlayer.Currency >= pitSpeedUpCost)
    {
      this.m_ItemPrice.color = Color.white;
      this.m_BuyButton.isEnabled = true;
    }
    else
    {
      this.m_ItemPrice.color = Color.red;
      this.m_BuyButton.isEnabled = false;
    }
  }

  public void OnBuyAndUse()
  {
    if (this.m_Callback == null)
      return;
    this.m_Callback();
  }
}
