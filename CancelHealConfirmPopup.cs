﻿// Decompiled with JetBrains decompiler
// Type: CancelHealConfirmPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class CancelHealConfirmPopup : Popup
{
  private System.Action yes;

  public void OnYesBtnClick()
  {
    if (this.yes != null)
      this.yes();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnNoBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void Init(System.Action yes)
  {
    this.yes = yes;
  }
}
