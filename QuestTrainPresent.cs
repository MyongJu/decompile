﻿// Decompiled with JetBrains decompiler
// Type: QuestTrainPresent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class QuestTrainPresent : BasePresent
{
  public override void Refresh()
  {
    this.UseStats();
  }

  public override int IconType
  {
    get
    {
      return 1;
    }
  }

  public override string TexturePath
  {
    get
    {
      string formula = this.Formula;
      if (formula == "infantry" || formula == "ranged" || (formula == "cavalry" || formula == "siege") || formula == "trap")
        return base.TexturePath;
      return "Texture/Unit/";
    }
  }
}
