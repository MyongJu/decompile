﻿// Decompiled with JetBrains decompiler
// Type: KingdomBuffPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class KingdomBuffPayload
{
  private List<KingdomBuffPayload.KingdomBuffData> _kingdomBuffDataList = new List<KingdomBuffPayload.KingdomBuffData>();
  public System.Action onKingdomBuffDataUpdated;
  private static KingdomBuffPayload _instance;

  public static KingdomBuffPayload Instance
  {
    get
    {
      if (KingdomBuffPayload._instance == null)
        KingdomBuffPayload._instance = new KingdomBuffPayload();
      return KingdomBuffPayload._instance;
    }
  }

  public List<KingdomBuffPayload.KingdomBuffData> KingdomBuffDataList
  {
    get
    {
      this._kingdomBuffDataList.RemoveAll((Predicate<KingdomBuffPayload.KingdomBuffData>) (x => NetServerTime.inst.ServerTimestamp > x.finishedTime));
      if (PlayerData.inst != null && PlayerData.inst.allianceId <= 0L)
        this._kingdomBuffDataList.RemoveAll((Predicate<KingdomBuffPayload.KingdomBuffData>) (x => x.buffType == KingdomBuffPayload.KingdomBuffType.ALLIANCE));
      return this._kingdomBuffDataList;
    }
  }

  public bool HasBuff
  {
    get
    {
      if (this._kingdomBuffDataList != null)
        return this._kingdomBuffDataList.Count > 0;
      return false;
    }
  }

  private void Decode(Hashtable data)
  {
    if (data != null)
    {
      IDictionaryEnumerator enumerator = data.GetEnumerator();
      while (enumerator.MoveNext())
      {
        int kingdomBuffId = 0;
        int outData1 = 0;
        int outData2 = 0;
        float outData3 = 0.0f;
        Hashtable inData = enumerator.Value as Hashtable;
        int.TryParse(enumerator.Key.ToString(), out kingdomBuffId);
        if (inData != null)
        {
          DatabaseTools.UpdateData(inData, "value", ref outData3);
          DatabaseTools.UpdateData(inData, "start_time", ref outData1);
          DatabaseTools.UpdateData(inData, "end_time", ref outData2);
        }
        KingdomBuffPayload.KingdomBuffData kingdomBuffData = new KingdomBuffPayload.KingdomBuffData();
        kingdomBuffData.buffId = kingdomBuffId;
        kingdomBuffData.benefitValue = outData3;
        kingdomBuffData.startTime = outData1;
        kingdomBuffData.finishedTime = outData2;
        kingdomBuffData.buffType = KingdomBuffPayload.KingdomBuffType.KINGDOM;
        this._kingdomBuffDataList.RemoveAll((Predicate<KingdomBuffPayload.KingdomBuffData>) (x => x.buffId == kingdomBuffId));
        if (NetServerTime.inst.ServerTimestamp >= outData1 && NetServerTime.inst.ServerTimestamp < outData2 && PlayerData.inst.userData.world_id == PlayerData.inst.userData.current_world_id)
          this._kingdomBuffDataList.Add(kingdomBuffData);
      }
    }
    this.LoadAllianceMagicData();
    if (this.onKingdomBuffDataUpdated == null)
      return;
    this.onKingdomBuffDataUpdated();
  }

  public void RequestServerData(System.Action<bool, object> callback = null)
  {
    if (callback != null)
      MessageHub.inst.GetPortByAction("Wonder:getKingdomBuffList").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          this.Decode(data as Hashtable);
        if (callback == null)
          return;
        callback(ret, data);
      }), true);
    else
      MessageHub.inst.GetPortByAction("Wonder:getKingdomBuffList").SendLoader((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.Decode(data as Hashtable);
      }), false, false);
  }

  private void LoadAllianceMagicData()
  {
    this.OnAllianceMagicDataChanged((AllianceMagicData) null);
  }

  private void OnAllianceMagicDataChanged(AllianceMagicData magicData)
  {
    this._kingdomBuffDataList.RemoveAll((Predicate<KingdomBuffPayload.KingdomBuffData>) (x => x.buffType == KingdomBuffPayload.KingdomBuffType.ALLIANCE));
    this.CheckAllianceMagicData("heal_speed");
    this.CheckAllianceMagicData("research_speed");
    this.CheckAllianceMagicData("building_speed");
    this.CheckAllianceMagicData("gather_speed");
  }

  private void CheckAllianceMagicData(string magicType)
  {
    AllianceMagicData bySkillType = DBManager.inst.DB_AllianceMagic.GetBySkillType(magicType);
    if (bySkillType == null)
      return;
    TempleSkillInfo templeSkillInfo = ConfigManager.inst.DB_TempleSkill.GetBySkillType(magicType);
    if (templeSkillInfo == null)
      return;
    string type = templeSkillInfo.Type;
    if (type == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (KingdomBuffPayload.\u003C\u003Ef__switch\u0024map7F == null)
    {
      // ISSUE: reference to a compiler-generated field
      KingdomBuffPayload.\u003C\u003Ef__switch\u0024map7F = new Dictionary<string, int>(4)
      {
        {
          "heal_speed",
          0
        },
        {
          "research_speed",
          0
        },
        {
          "building_speed",
          0
        },
        {
          "gather_speed",
          0
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!KingdomBuffPayload.\u003C\u003Ef__switch\u0024map7F.TryGetValue(type, out num) || num != 0)
      return;
    this._kingdomBuffDataList.RemoveAll((Predicate<KingdomBuffPayload.KingdomBuffData>) (x => x.buffId == templeSkillInfo.internalId));
    if ((long) NetServerTime.inst.ServerTimestamp < bySkillType.TimerEnd)
      this._kingdomBuffDataList.Add(new KingdomBuffPayload.KingdomBuffData()
      {
        buff = (object) templeSkillInfo,
        buffId = templeSkillInfo.internalId,
        finishedTime = (int) bySkillType.TimerEnd,
        buffType = KingdomBuffPayload.KingdomBuffType.ALLIANCE
      });
    if (this.onKingdomBuffDataUpdated == null)
      return;
    this.onKingdomBuffDataUpdated();
  }

  private void OnKingdomBuffPush(object data)
  {
    this.Decode(data as Hashtable);
    DBManager.inst.DB_UserBenefitParam.MarkDataDirty();
  }

  public void Initialize()
  {
    DBManager.inst.DB_AllianceMagic.onDataCreate += new System.Action<AllianceMagicData>(this.OnAllianceMagicDataChanged);
    DBManager.inst.DB_AllianceMagic.onDataRemoved += new System.Action<AllianceMagicData>(this.OnAllianceMagicDataChanged);
    MessageHub.inst.GetPortByAction("kingdom_buff_update").AddEvent(new System.Action<object>(this.OnKingdomBuffPush));
    this.RequestServerData((System.Action<bool, object>) null);
  }

  public void Dispose()
  {
    DBManager.inst.DB_AllianceMagic.onDataCreate -= new System.Action<AllianceMagicData>(this.OnAllianceMagicDataChanged);
    DBManager.inst.DB_AllianceMagic.onDataRemoved -= new System.Action<AllianceMagicData>(this.OnAllianceMagicDataChanged);
    MessageHub.inst.GetPortByAction("kingdom_buff_update").RemoveEvent(new System.Action<object>(this.OnKingdomBuffPush));
    if (this._kingdomBuffDataList == null)
      return;
    this._kingdomBuffDataList.Clear();
  }

  public class KingdomBuffData
  {
    public object buff;
    public int buffId;
    public int startTime;
    public int finishedTime;
    public float benefitValue;
    public KingdomBuffPayload.KingdomBuffType buffType;
  }

  public enum KingdomBuffType
  {
    KINGDOM,
    ALLIANCE,
  }
}
