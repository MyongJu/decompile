﻿// Decompiled with JetBrains decompiler
// Type: DestroyConfirmPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class DestroyConfirmPopup : Popup
{
  public UIButton closeBtn;
  public UILabel mInstDestroyAmount;
  public UILabel mDestroyTime;
  public GameObject mInstBuyAndUseBtn;
  public GameObject mInstUseBtn;
  public UILabel mTorchesValue;
  public UIButton instBuyAndUseBtn;
  public UIButton instUseBtn;
  public UIButton normalUseBtn;
  private System.Action normalConfirm;
  private System.Action instanceConfirm;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    DestroyConfirmPopup.Parameter parameter = orgParam as DestroyConfirmPopup.Parameter;
    base.OnShow(orgParam);
    this.InitUI(parameter.info, parameter.normalConfirm, parameter.instanceConfirm);
  }

  private void InitUI(BuildingInfo info, System.Action normalConfirm, System.Action instanceConfirm)
  {
    this.closeBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnCloseBtnClicked)));
    string str = "shopitem_building_deconstruct";
    int itemCountByShopId = ItemBag.Instance.GetItemCountByShopID(str);
    this.mTorchesValue.text = Utils.XLAT("id_uppercase_own") + ": [ff0000]" + itemCountByShopId.ToString();
    this.mInstDestroyAmount.text = ItemBag.Instance.GetShopItemPrice(str).ToString();
    this.mDestroyTime.text = Utils.ConvertSecsToString(info.Build_Time / 2.0);
    this.mInstBuyAndUseBtn.SetActive(itemCountByShopId == 0);
    this.mInstUseBtn.SetActive(itemCountByShopId > 0);
    this.normalConfirm = normalConfirm;
    this.instanceConfirm = instanceConfirm;
    this.instBuyAndUseBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnInstanceConfirm)));
    this.instUseBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnInstanceConfirm)));
    this.normalUseBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnNormalConfirm)));
    this.normalUseBtn.isEnabled = CityManager.inst.GetBuildingsInFlux() < 2;
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnInstanceConfirm()
  {
    this.instanceConfirm();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnNormalConfirm()
  {
    this.normalConfirm();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public BuildingInfo info;
    public System.Action normalConfirm;
    public System.Action instanceConfirm;
  }
}
