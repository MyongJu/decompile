﻿// Decompiled with JetBrains decompiler
// Type: U2DSpriteMesh
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class U2DSpriteMesh : MonoBehaviour
{
  [SerializeField]
  [HideInInspector]
  private int m_Width = 100;
  [HideInInspector]
  [SerializeField]
  private int m_Height = 100;
  [SerializeField]
  [HideInInspector]
  private Color m_Color = Color.white;
  [HideInInspector]
  [SerializeField]
  private float m_PixelSize = 1f;
  private Rect m_InnerUV = new Rect();
  private Rect m_OuterUV = new Rect();
  private const int SLICED_VERTEX_COUNT = 16;
  private const int SLICED_INDEX_COUNT = 54;
  private const int SIMPLE_VERTEX_COUNT = 4;
  private const int SIMPLE_INDEX_COUNT = 6;
  [SerializeField]
  [HideInInspector]
  private UnityEngine.Sprite m_Sprite;
  [HideInInspector]
  [SerializeField]
  private Material m_Material;
  [SerializeField]
  [HideInInspector]
  private U2DSpriteMesh.FillType m_FillType;
  private Vector3[] m_Vertices;
  private Vector2[] m_UVs;
  private Color[] m_Colors;
  private int[] m_Indices;
  private Mesh m_Mesh;
  private MeshFilter m_MeshFilter;
  private MeshRenderer m_MeshRenderer;
  private bool m_Changed;

  private void Start()
  {
    this.m_Changed = true;
  }

  private void InitOnce()
  {
    this.m_MeshFilter = this.GetComponent<MeshFilter>();
    if ((Object) this.m_MeshFilter == (Object) null)
      this.m_MeshFilter = this.gameObject.AddComponent<MeshFilter>();
    this.m_MeshRenderer = this.GetComponent<MeshRenderer>();
    if ((Object) this.m_MeshRenderer == (Object) null)
      this.m_MeshRenderer = this.gameObject.AddComponent<MeshRenderer>();
    if (!((Object) this.m_Mesh == (Object) null))
      return;
    this.m_Mesh = new Mesh();
    this.m_Mesh.name = "Mesh";
  }

  public U2DSpriteMesh.FillType fillType
  {
    get
    {
      return this.m_FillType;
    }
    set
    {
      if (this.m_FillType == value)
        return;
      this.m_FillType = value;
      this.m_Changed = true;
    }
  }

  public int width
  {
    get
    {
      return this.m_Width;
    }
    set
    {
      if (this.m_Width == value)
        return;
      this.m_Width = value;
      this.m_Changed = true;
    }
  }

  public int height
  {
    get
    {
      return this.m_Height;
    }
    set
    {
      if (this.m_Height == value)
        return;
      this.m_Height = value;
      this.m_Changed = true;
    }
  }

  public UnityEngine.Sprite sprite
  {
    get
    {
      return this.m_Sprite;
    }
    set
    {
      if (!((Object) this.m_Sprite != (Object) value))
        return;
      this.m_Sprite = value;
      this.m_Changed = true;
    }
  }

  public Color color
  {
    get
    {
      return this.m_Color;
    }
    set
    {
      if (!(this.m_Color != value))
        return;
      this.m_Color = value;
      this.m_Changed = true;
    }
  }

  public float pixelSize
  {
    get
    {
      return this.m_PixelSize;
    }
    set
    {
      if ((double) this.m_PixelSize == (double) value)
        return;
      this.m_PixelSize = value;
      this.m_Changed = true;
    }
  }

  public Material material
  {
    get
    {
      return this.m_Material;
    }
    set
    {
      if (!((Object) this.m_Material != (Object) value))
        return;
      this.m_Material = value;
      this.m_Changed = true;
    }
  }

  public Texture GetTexture()
  {
    if ((Object) this.m_Sprite != (Object) null)
      return (Texture) this.m_Sprite.texture;
    return (Texture) null;
  }

  public Vector4 GetBorder()
  {
    if ((Object) this.m_Sprite != (Object) null)
      return this.m_Sprite.border;
    return Vector4.zero;
  }

  public Rect GetTextureRect()
  {
    if ((Object) this.m_Sprite != (Object) null)
      return this.m_Sprite.textureRect;
    return new Rect();
  }

  private void LateUpdate()
  {
    if (!this.m_Changed)
      return;
    this.Fill();
    this.m_Changed = false;
  }

  private void Fill()
  {
    if ((Object) this.m_Sprite == (Object) null)
      return;
    if (this.IsOriginal)
      this.OriginalFill();
    else
      this.RectFill();
  }

  public bool IsOriginal
  {
    get
    {
      if (this.m_FillType == U2DSpriteMesh.FillType.Original)
        return true;
      if ((Object) this.m_Sprite != (Object) null && this.m_Sprite.packed)
        return this.m_Sprite.packingMode == SpritePackingMode.Tight;
      return false;
    }
  }

  private void OriginalFill()
  {
    this.InitOnce();
    this.m_Vertices = new Vector3[this.m_Sprite.vertices.Length];
    this.m_Colors = new Color[this.m_Sprite.vertices.Length];
    this.m_UVs = this.m_Sprite.uv;
    this.m_Indices = new int[this.m_Sprite.triangles.Length];
    for (int index = 0; index < this.m_Vertices.Length; ++index)
    {
      Vector2 vertex = this.m_Sprite.vertices[index];
      this.m_Vertices[index] = new Vector3(vertex.x, vertex.y, 0.0f);
      this.m_Colors[index] = this.m_Color;
    }
    for (int index = 0; index < this.m_Indices.Length; ++index)
      this.m_Indices[index] = (int) this.m_Sprite.triangles[index];
    this.GenerateMesh();
  }

  private void RectFill()
  {
    Texture texture = this.GetTexture();
    if (!((Object) texture != (Object) null))
      return;
    this.m_OuterUV = this.GetTextureRect();
    this.m_InnerUV = this.m_OuterUV;
    Vector4 border = this.GetBorder();
    this.m_InnerUV.xMin += border.x;
    this.m_InnerUV.yMin += border.y;
    this.m_InnerUV.xMax -= border.z;
    this.m_InnerUV.yMax -= border.w;
    float num1 = 1f / (float) texture.width;
    float num2 = 1f / (float) texture.height;
    this.m_OuterUV.xMin *= num1;
    this.m_OuterUV.xMax *= num1;
    this.m_OuterUV.yMin *= num2;
    this.m_OuterUV.yMax *= num2;
    this.m_InnerUV.xMin *= num1;
    this.m_InnerUV.xMax *= num1;
    this.m_InnerUV.yMin *= num2;
    this.m_InnerUV.yMax *= num2;
    this.InitOnce();
    if (this.m_FillType == U2DSpriteMesh.FillType.Sliced)
    {
      this.SlicedFill();
    }
    else
    {
      if (this.m_FillType != U2DSpriteMesh.FillType.Simple)
        return;
      this.SimpleFill();
    }
  }

  public bool IsValid()
  {
    if ((Object) this.m_Mesh != (Object) null && (Object) this.m_MeshRenderer != (Object) null && ((Object) this.m_Sprite != (Object) null && (Object) this.m_Material != (Object) null) && (this.m_Vertices != null && this.m_UVs != null && this.m_Colors != null))
      return this.m_Indices != null;
    return false;
  }

  public void GenerateMesh()
  {
    this.m_Mesh.Clear();
    this.m_Mesh.vertices = this.m_Vertices;
    this.m_Mesh.uv = this.m_UVs;
    this.m_Mesh.colors = this.m_Colors;
    this.m_Mesh.triangles = this.m_Indices;
    this.m_MeshFilter.mesh = this.m_Mesh;
    this.m_MeshRenderer.material = this.m_Material;
    MaterialPropertyBlock materialPropertyBlock = new MaterialPropertyBlock();
    this.m_MeshRenderer.GetPropertyBlock(materialPropertyBlock);
    materialPropertyBlock.SetTexture("_MainTex", (Texture) this.m_Sprite.texture);
    this.m_MeshRenderer.SetPropertyBlock(materialPropertyBlock);
  }

  private void SlicedFill()
  {
    Vector4 border = this.GetBorder();
    if ((double) border.x == 0.0 && (double) border.y == 0.0 && ((double) border.z == 0.0 && (double) border.w == 0.0))
    {
      this.SimpleFill();
    }
    else
    {
      this.m_Vertices = new Vector3[16];
      this.m_Colors = new Color[16];
      this.m_UVs = new Vector2[16];
      this.m_Indices = new int[54];
      float num1 = 0.5f * (float) this.m_Width;
      float num2 = 0.5f * (float) this.m_Height;
      float new_x1 = -num1;
      float new_x2 = num1;
      float new_y1 = -num2;
      float new_y2 = num2;
      this.m_Vertices[0].Set(new_x1, new_y2, 0.0f);
      this.m_Vertices[1].Set(new_x1 + border.x, new_y2, 0.0f);
      this.m_Vertices[2].Set(new_x2 - border.y, new_y2, 0.0f);
      this.m_Vertices[3].Set(new_x2, new_y2, 0.0f);
      this.m_Vertices[4].Set(new_x1, new_y2 - border.w, 0.0f);
      this.m_Vertices[5].Set(new_x1 + border.x, new_y2 - border.w, 0.0f);
      this.m_Vertices[6].Set(new_x2 - border.y, new_y2 - border.w, 0.0f);
      this.m_Vertices[7].Set(new_x2, new_y2 - border.w, 0.0f);
      this.m_Vertices[8].Set(new_x1, new_y1 + border.z, 0.0f);
      this.m_Vertices[9].Set(new_x1 + border.x, new_y1 + border.z, 0.0f);
      this.m_Vertices[10].Set(new_x2 - border.y, new_y1 + border.z, 0.0f);
      this.m_Vertices[11].Set(new_x2, new_y1 + border.z, 0.0f);
      this.m_Vertices[12].Set(new_x1, new_y1, 0.0f);
      this.m_Vertices[13].Set(new_x1 + border.x, new_y1, 0.0f);
      this.m_Vertices[14].Set(new_x2 - border.y, new_y1, 0.0f);
      this.m_Vertices[15].Set(new_x2, new_y1, 0.0f);
      for (int index = 0; index < 16; ++index)
        this.m_Vertices[index] = this.pixelSize * this.m_Vertices[index];
      this.m_Colors[0] = this.m_Color;
      this.m_Colors[1] = this.m_Color;
      this.m_Colors[2] = this.m_Color;
      this.m_Colors[3] = this.m_Color;
      this.m_Colors[4] = this.m_Color;
      this.m_Colors[5] = this.m_Color;
      this.m_Colors[6] = this.m_Color;
      this.m_Colors[7] = this.m_Color;
      this.m_Colors[8] = this.m_Color;
      this.m_Colors[9] = this.m_Color;
      this.m_Colors[10] = this.m_Color;
      this.m_Colors[11] = this.m_Color;
      this.m_Colors[12] = this.m_Color;
      this.m_Colors[13] = this.m_Color;
      this.m_Colors[14] = this.m_Color;
      this.m_Colors[15] = this.m_Color;
      this.m_UVs[0].Set(this.m_OuterUV.xMin, this.m_OuterUV.yMax);
      this.m_UVs[1].Set(this.m_InnerUV.xMin, this.m_OuterUV.yMax);
      this.m_UVs[2].Set(this.m_InnerUV.xMax, this.m_OuterUV.yMax);
      this.m_UVs[3].Set(this.m_OuterUV.xMax, this.m_OuterUV.yMax);
      this.m_UVs[4].Set(this.m_OuterUV.xMin, this.m_InnerUV.yMax);
      this.m_UVs[5].Set(this.m_InnerUV.xMin, this.m_InnerUV.yMax);
      this.m_UVs[6].Set(this.m_InnerUV.xMax, this.m_InnerUV.yMax);
      this.m_UVs[7].Set(this.m_OuterUV.xMax, this.m_InnerUV.yMax);
      this.m_UVs[8].Set(this.m_OuterUV.xMin, this.m_InnerUV.yMin);
      this.m_UVs[9].Set(this.m_InnerUV.xMin, this.m_InnerUV.yMin);
      this.m_UVs[10].Set(this.m_InnerUV.xMax, this.m_InnerUV.yMin);
      this.m_UVs[11].Set(this.m_OuterUV.xMax, this.m_InnerUV.yMin);
      this.m_UVs[12].Set(this.m_OuterUV.xMin, this.m_OuterUV.yMin);
      this.m_UVs[13].Set(this.m_InnerUV.xMin, this.m_OuterUV.yMin);
      this.m_UVs[14].Set(this.m_InnerUV.xMax, this.m_OuterUV.yMin);
      this.m_UVs[15].Set(this.m_OuterUV.xMax, this.m_OuterUV.yMin);
      this.m_Indices[0] = 4;
      this.m_Indices[1] = 1;
      this.m_Indices[2] = 0;
      this.m_Indices[3] = 4;
      this.m_Indices[4] = 5;
      this.m_Indices[5] = 1;
      this.m_Indices[6] = 5;
      this.m_Indices[7] = 2;
      this.m_Indices[8] = 1;
      this.m_Indices[9] = 5;
      this.m_Indices[10] = 6;
      this.m_Indices[11] = 2;
      this.m_Indices[12] = 6;
      this.m_Indices[13] = 3;
      this.m_Indices[14] = 2;
      this.m_Indices[15] = 6;
      this.m_Indices[16] = 7;
      this.m_Indices[17] = 3;
      this.m_Indices[18] = 8;
      this.m_Indices[19] = 5;
      this.m_Indices[20] = 4;
      this.m_Indices[21] = 8;
      this.m_Indices[22] = 9;
      this.m_Indices[23] = 5;
      this.m_Indices[24] = 9;
      this.m_Indices[25] = 6;
      this.m_Indices[26] = 5;
      this.m_Indices[27] = 9;
      this.m_Indices[28] = 10;
      this.m_Indices[29] = 6;
      this.m_Indices[30] = 10;
      this.m_Indices[31] = 7;
      this.m_Indices[32] = 6;
      this.m_Indices[33] = 10;
      this.m_Indices[34] = 11;
      this.m_Indices[35] = 7;
      this.m_Indices[36] = 12;
      this.m_Indices[37] = 9;
      this.m_Indices[38] = 8;
      this.m_Indices[39] = 12;
      this.m_Indices[40] = 13;
      this.m_Indices[41] = 9;
      this.m_Indices[42] = 13;
      this.m_Indices[43] = 10;
      this.m_Indices[44] = 9;
      this.m_Indices[45] = 13;
      this.m_Indices[46] = 14;
      this.m_Indices[47] = 10;
      this.m_Indices[48] = 14;
      this.m_Indices[49] = 11;
      this.m_Indices[50] = 10;
      this.m_Indices[51] = 14;
      this.m_Indices[52] = 15;
      this.m_Indices[53] = 11;
      this.GenerateMesh();
    }
  }

  private void SimpleFill()
  {
    this.m_Vertices = new Vector3[4];
    this.m_Colors = new Color[4];
    this.m_UVs = new Vector2[4];
    this.m_Indices = new int[6];
    float new_x = 0.5f * (float) this.m_Width;
    float new_y = 0.5f * (float) this.m_Height;
    this.m_Vertices[0].Set(-new_x, -new_y, 0.0f);
    this.m_Vertices[1].Set(new_x, -new_y, 0.0f);
    this.m_Vertices[2].Set(new_x, new_y, 0.0f);
    this.m_Vertices[3].Set(-new_x, new_y, 0.0f);
    for (int index = 0; index < 4; ++index)
      this.m_Vertices[index] = this.pixelSize * this.m_Vertices[index];
    this.m_Colors[0] = this.m_Color;
    this.m_Colors[1] = this.m_Color;
    this.m_Colors[2] = this.m_Color;
    this.m_Colors[3] = this.m_Color;
    this.m_UVs[0].Set(this.m_OuterUV.xMin, this.m_OuterUV.yMin);
    this.m_UVs[1].Set(this.m_OuterUV.xMax, this.m_OuterUV.yMin);
    this.m_UVs[2].Set(this.m_OuterUV.xMax, this.m_OuterUV.yMax);
    this.m_UVs[3].Set(this.m_OuterUV.xMin, this.m_OuterUV.yMax);
    this.m_Indices[0] = 0;
    this.m_Indices[1] = 1;
    this.m_Indices[2] = 2;
    this.m_Indices[3] = 0;
    this.m_Indices[4] = 2;
    this.m_Indices[5] = 3;
    this.GenerateMesh();
  }

  public static Vector3 Vector3Multiply(Vector3 a, Vector3 b)
  {
    return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
  }

  public enum FillType
  {
    Simple,
    Sliced,
    Original,
  }
}
