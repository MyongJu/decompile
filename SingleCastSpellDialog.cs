﻿// Decompiled with JetBrains decompiler
// Type: SingleCastSpellDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class SingleCastSpellDialog : MonoBehaviour
{
  public UITexture m_SkillIcon;
  public UILabel m_SkillTip;
  private TempleSkillInfo m_TempleSkillInfo;
  private KingSkillInfo m_KingSkillInfo;

  public void SetData(TempleSkillInfo skillInfo)
  {
    if (skillInfo == null)
      return;
    this.m_TempleSkillInfo = skillInfo;
    this.m_SkillTip.text = !(skillInfo.TargetType == "ops_alliance_temple") ? (skillInfo.TargetArea <= 1 ? (skillInfo.IsHarmfull != 1 ? ScriptLocalization.Get("alliance_altar_cast_spell_choose_ally_desc", true) : ScriptLocalization.Get("alliance_altar_cast_spell_choose_enemy_desc", true)) : ScriptLocalization.Get("alliance_altar_cast_spell_choose_area_desc", true)) : ScriptLocalization.Get("alliance_altar_cast_spell_choose_enemy_altar_desc", true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_SkillIcon, this.m_TempleSkillInfo.IconPath, (System.Action<bool>) null, true, false, string.Empty);
  }

  public TempleSkillInfo GetTempleSkill()
  {
    return this.m_TempleSkillInfo;
  }

  public void SetData(KingSkillInfo skillInfo)
  {
    if (skillInfo == null)
      return;
    this.m_KingSkillInfo = skillInfo;
    if (skillInfo.IsHarmful)
      this.m_SkillTip.text = ScriptLocalization.Get("alliance_altar_cast_spell_choose_enemy_desc", true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_SkillIcon, this.m_KingSkillInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
  }

  public KingSkillInfo GetKingSkill()
  {
    return this.m_KingSkillInfo;
  }

  public void OnClosePressed()
  {
    PVPSystem.Instance.Map.EnterNormalMode();
  }
}
