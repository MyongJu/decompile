﻿// Decompiled with JetBrains decompiler
// Type: ScoutReportDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ScoutReportDlg : BaseReportPopup
{
  private List<GameObject> pool = new List<GameObject>();
  public float gapX = 100f;
  private ScoutReportContent src;
  public UIScrollView sv;
  public UILabel location;
  public UISprite underline;
  public UILabel tarname;
  public UILabel tarpos;
  public UILabel tarpower;
  public UITexture taricon;
  public ItemInfo resreward;
  public GameObject troopdetailGroup;
  public GameObject troopdetailContent;
  public GameObject trapsGroup;
  public GameObject trapsContent;
  public GameObject reinforceGroup;
  public GameObject reinforceContent;
  public GameObject boostGroup;
  public GameObject boostContent;
  public UITable contentTable;
  public GameObject container;
  public GameObject enemy;

  private void Init()
  {
    List<ItemInfo.Data> res = new List<ItemInfo.Data>();
    this.src.SummarizeResourceReward(ref res);
    this.location.text = MapUtils.GetCoordinateString(this.src.k, this.src.x, this.src.y);
    this.time.text = Utils.FormatTimeForMail(this.mailtime);
    this.tarname.text = (this.src.acronym == null ? (string) null : "(" + this.src.acronym + ")") + this.src.user_name;
    this.tarpos.text = this.location.text;
    this.tarpower.text = this.src.power;
    CustomIconLoader.Instance.requestCustomIcon(this.taricon, UserData.GetPortraitIconPath(int.Parse(this.src.portrait)), this.src.icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.taricon, this.src.lord_title, 1);
    this.underline.width = this.tarpos.width;
    List<Vector3> tarpos = new List<Vector3>(res.Count);
    Utils.ArrangePosition(this.resreward.transform.localPosition, res.Count, this.gapX, 0.0f, ref tarpos);
    for (int index = 0; index < tarpos.Count; ++index)
    {
      GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.resreward.gameObject, this.resreward.transform.parent);
      this.pool.Add(gameObject);
      gameObject.transform.localPosition = tarpos[index];
      gameObject.GetComponent<ItemInfo>().SeedData(res[index]);
    }
    if (this.src.troops == null)
    {
      this.HideTemplate(true);
    }
    else
    {
      DefendTroopDetail defendTroopDetail1 = (DefendTroopDetail) null;
      if (this.src.troops.TryGetValue("defending_troops", out defendTroopDetail1))
      {
        this.troopdetailGroup.SetActive(true);
        DefendInfo.Data did = new DefendInfo.Data();
        defendTroopDetail1.SummarizeDefendTroop(ref did);
        DefendInfo component = this.troopdetailGroup.GetComponent<DefendInfo>();
        component.SeedData(ScriptLocalization.Get("scout_report_section_uppercase_troops", true), did);
        component.toggleDefendInfo += new System.Action(this.ArrangeLayout);
      }
      DefendTroopDetail defendTroopDetail2 = (DefendTroopDetail) null;
      if (this.src.troops.TryGetValue("traps", out defendTroopDetail2))
      {
        this.trapsGroup.SetActive(true);
        DefendInfo.Data did = new DefendInfo.Data();
        defendTroopDetail2.SummarizeDefendTrap(ref did);
        DefendInfo component = this.trapsGroup.GetComponent<DefendInfo>();
        component.SeedData(ScriptLocalization.Get("scout_report_section_uppercase_traps", true), did);
        component.toggleDefendInfo += new System.Action(this.ArrangeLayout);
      }
      DefendTroopDetail defendTroopDetail3 = (DefendTroopDetail) null;
      if (this.src.troops.TryGetValue("reinforcing_troops", out defendTroopDetail3))
      {
        this.reinforceGroup.SetActive(true);
        DefendInfo.Data did = new DefendInfo.Data();
        defendTroopDetail3.SummarizeDefendReinforcement(ref did);
        DefendInfo component = this.reinforceGroup.GetComponent<DefendInfo>();
        component.SeedData(ScriptLocalization.Get("scout_report_section_uppercase_reinforcement", true), did);
        component.toggleDefendInfo += new System.Action(this.ArrangeLayout);
      }
      if (this.src.battle_boost != null)
      {
        this.boostGroup.SetActive(true);
        DefendInfo.Data did = new DefendInfo.Data();
        this.src.SummarizeDefendBoost(ref did);
        DefendInfo component = this.boostGroup.GetComponent<DefendInfo>();
        component.SeedData(ScriptLocalization.Get("scout_report_section_uppercase_boostdetails", true), did);
        component.toggleDefendInfo += new System.Action(this.ArrangeLayout);
      }
      this.ArrangeLayout();
      this.HideTemplate(true);
    }
  }

  public void OnBackBtnPressed()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void GotoTarget()
  {
    if (!MapUtils.CanGotoTarget(int.Parse(this.src.k)))
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    }
    else
    {
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
      {
        UIManager.inst.cityCamera.gameObject.SetActive(false);
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
      }
      PVPMap.PendingGotoRequest = new Coordinate(int.Parse(this.src.k), int.Parse(this.src.x), int.Parse(this.src.y));
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }

  private void ArrangeLayout()
  {
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.trapsGroup)
    {
      float num = this.troopdetailGroup.transform.localPosition.y - NGUIMath.CalculateRelativeWidgetBounds(this.troopdetailGroup.transform).size.y;
      Vector3 localPosition = this.trapsGroup.transform.localPosition;
      localPosition.y = num;
      this.trapsGroup.transform.localPosition = localPosition;
    }
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.reinforceGroup)
    {
      float num = this.trapsGroup.transform.localPosition.y - NGUIMath.CalculateRelativeWidgetBounds(this.trapsGroup.transform).size.y;
      Vector3 localPosition = this.reinforceGroup.transform.localPosition;
      localPosition.y = num;
      this.reinforceGroup.transform.localPosition = localPosition;
    }
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.boostGroup))
      return;
    float num1 = this.reinforceGroup.transform.localPosition.y - NGUIMath.CalculateRelativeWidgetBounds(this.reinforceGroup.transform).size.y;
    Vector3 localPosition1 = this.boostGroup.transform.localPosition;
    localPosition1.y = num1;
    this.boostGroup.transform.localPosition = localPosition1;
  }

  private void Reset()
  {
    this.sv.ResetPosition();
    Rigidbody component = this.sv.transform.GetComponent<Rigidbody>();
    if ((UnityEngine.Object) null != (UnityEngine.Object) component)
    {
      component.isKinematic = true;
      component.useGravity = false;
    }
    this.HideTemplate(false);
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        PrefabManagerEx.Instance.Destroy(current);
      }
    }
    this.pool.Clear();
    this.troopdetailGroup.GetComponent<DefendInfo>().Reset();
    this.trapsGroup.GetComponent<DefendInfo>().Reset();
    this.reinforceGroup.GetComponent<DefendInfo>().Reset();
    this.boostGroup.GetComponent<DefendInfo>().Reset();
    this.troopdetailGroup.SetActive(false);
    this.trapsGroup.SetActive(false);
    this.reinforceGroup.SetActive(false);
    this.boostGroup.SetActive(false);
  }

  private void HideTemplate(bool hide)
  {
    this.resreward.gameObject.SetActive(!hide);
  }

  private void UpdateUI()
  {
    this.DrawEnemyOverView();
    this.container.GetComponent<UITable>().Reposition();
  }

  private void DrawEnemyOverView()
  {
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    NGUITools.AddChild(parent, this.enemy).transform.Find("enemyUser").GetComponent<Icon>().FeedData((IComponentData) new IconData("miss", new string[3]
    {
      "aa",
      "b",
      "c"
    }));
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    ScoutReportDlg.Parameter parameter = orgParam as ScoutReportDlg.Parameter;
    this.src = JsonReader.Deserialize<ScoutReport>(Utils.Object2Json((object) parameter.hashtable)).body;
    this.mailtime = parameter.time;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }

  public class Parameter : BaseReportPopup.Parameter
  {
    public new Hashtable hashtable;
    public long time;
  }
}
