﻿// Decompiled with JetBrains decompiler
// Type: UseItemPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UI;

public class UseItemPopup : Popup
{
  public UILabel m_ItemName;
  public UILabel m_ItemDesc;
  public UITexture m_ItemIcon;
  public UISlider m_Slider;
  public UIInput m_Input;
  public UIButton m_UseButton;
  private int m_UseCount;
  private EventDelegate onSliderChanged;
  private EventDelegate onInputChanged;
  private UseItemPopup.Parameter m_Parameter;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as UseItemPopup.Parameter;
    this.m_ItemName.text = this.m_Parameter.itemStaticInfo.LocName;
    this.m_ItemDesc.text = this.m_Parameter.itemStaticInfo.LocDescription;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, this.m_Parameter.itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.SetUseCount(0);
    this.onSliderChanged = new EventDelegate(new EventDelegate.Callback(this.OnSliderChanged));
    this.onInputChanged = new EventDelegate(new EventDelegate.Callback(this.OnInputChanged));
    this.m_Input.onChange.Clear();
    this.m_Slider.onChange.Clear();
    this.m_Input.onChange.Add(this.onInputChanged);
    this.m_Slider.onChange.Add(this.onSliderChanged);
  }

  private void OnLeave()
  {
    this.OnCloseButtonClick();
  }

  public void OnSliderChanged()
  {
    int itemCount = ItemBag.Instance.GetItemCount(this.m_Parameter.itemStaticInfo.internalId);
    this.m_Slider.onChange.Clear();
    this.SetUseCount((int) Math.Round((double) itemCount * (double) this.m_Slider.value));
    this.m_Slider.onChange.Add(this.onSliderChanged);
  }

  public void OnInputChanged()
  {
    int result = 0;
    int.TryParse(this.m_Input.value, out result);
    this.m_Input.onChange.Clear();
    this.SetUseCount(result);
    this.m_Input.onChange.Add(this.onInputChanged);
  }

  public void OnSubstract()
  {
    this.SetUseCount(this.m_UseCount - 1);
  }

  public void OnAdd()
  {
    this.SetUseCount(this.m_UseCount + 1);
  }

  public void OnUse()
  {
    this.OnCloseButtonClick();
    ItemBag.Instance.UseItem(this.m_Parameter.itemStaticInfo.internalId, this.m_UseCount, (Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (this.m_Parameter.callback == null)
        return;
      this.m_Parameter.callback(ret, data);
    }));
  }

  public void OnCloseButtonClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void SetUseCount(int count)
  {
    int itemCount = ItemBag.Instance.GetItemCount(this.m_Parameter.itemStaticInfo.internalId);
    this.m_UseCount = Math.Max(1, Math.Min(count, itemCount));
    this.m_UseButton.isEnabled = this.m_UseCount > 0;
    this.m_Slider.value = itemCount <= 0 ? 0.0f : (float) this.m_UseCount / (float) itemCount;
    this.m_Input.value = this.m_UseCount.ToString();
  }

  public class Parameter : Popup.PopupParameter
  {
    public ItemStaticInfo itemStaticInfo;
    public Hashtable param;
    public System.Action<bool, object> callback;
  }
}
