﻿// Decompiled with JetBrains decompiler
// Type: DragonAltarReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonAltarReportPopup : BaseReportPopup
{
  private Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>> allmytroopdetail = new Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>>();
  private DragonAltarReportPopup.DragonAltarReportContent darc;
  public Icon skilldetail;
  public Icon donateratio;
  public UILabel donateprocess;
  public UIProgressBar bar;
  public GameObject donategroup;
  public GameObject detailBtn;

  private void Init()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_alliance_hospital", (System.Action<bool>) null, true, false, string.Empty);
    TempleSkillInfo byId = ConfigManager.inst.DB_TempleSkill.GetById(this.darc.spell_id);
    string skillDescription = ConfigManager.inst.DB_TempleSkill.GetSkillDescription(this.darc.spell_id);
    if (byId == null)
      return;
    this.skilldetail.SetData(byId.IconPath, byId.Name, skillDescription);
    if (byId.NeedDonation != 0)
    {
      this.donategroup.SetActive(true);
      float num = (float) this.darc.mana_donation / (float) byId.MaxDonation;
      this.donateprocess.text = this.darc.mana_donation.ToString() + "/" + byId.MaxDonation.ToString() + " (" + (num * 100f).ToString("F1") + "%)";
      this.bar.value = num;
    }
    else
      this.donategroup.SetActive(false);
    if (this.darc.defender != null)
    {
      this.detailBtn.SetActive(true);
      this.darc.SummarizeAllTroopDetail(this.darc.defender, ref this.allmytroopdetail);
    }
    else
      this.detailBtn.SetActive(false);
  }

  public void OnLoadTroopDetail()
  {
    UIManager.inst.OpenPopup("Report/WarReportDetail", (Popup.PopupParameter) new WarReportDetail.Parameter()
    {
      pdefendertroopdetail = this.allmytroopdetail,
      mail = this.param.mail
    });
  }

  public override void OnShareBtnClicked()
  {
    base.OnShareBtnClicked();
    if (!this.canShare)
      return;
    string nameKey = ConfigManager.inst.DB_TempleSkill.GetById(this.darc.spell_id).NameKey;
    if (this.darc.is_attacker == 1)
      ChatMessageManager.SendDragonAltarAttackReport(this.maildata.mailID, PlayerData.inst.uid, nameKey);
    else
      ChatMessageManager.SendDragonAltarBeAttackReport(this.maildata.mailID, PlayerData.inst.uid, nameKey);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.darc = JsonReader.Deserialize<DragonAltarReportPopup.DragonAltarReportContent>(Utils.Object2Json((object) this.param.hashtable));
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public class DragonAltarReportContent
  {
    public int spell_id;
    public int mana_donation;
    public List<TroopDetail> defender;
    public int is_attacker;

    public void SummarizeAllTroopDetail(List<TroopDetail> tds, ref Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>> allmytroopdetail)
    {
      if (tds == null)
        return;
      using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          TroopDetail current = enumerator.Current;
          if (!allmytroopdetail.ContainsKey(current.name))
          {
            Dictionary<string, List<SoldierInfo.Data>> troopdetail = new Dictionary<string, List<SoldierInfo.Data>>();
            current.SummarizeTroopDetail(ref troopdetail);
            allmytroopdetail.Add(current.name, troopdetail);
          }
        }
      }
    }
  }
}
