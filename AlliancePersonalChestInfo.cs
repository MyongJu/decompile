﻿// Decompiled with JetBrains decompiler
// Type: AlliancePersonalChestInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;

public class AlliancePersonalChestInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "category")]
  public string category;
  [Config(Name = "formula")]
  public string formula;
  [Config(Name = "chest_id")]
  public int chestId;
  [Config(Name = "description")]
  public string description;

  public string LocalDescription
  {
    get
    {
      if (string.IsNullOrEmpty(this.formula))
        return ScriptLocalization.Get(this.description, true);
      Dictionary<string, string> para = new Dictionary<string, string>();
      int num1 = 0;
      string[] strArray1 = this.formula.Split(new char[1]
      {
        '&'
      }, StringSplitOptions.RemoveEmptyEntries);
      if (strArray1 != null)
      {
        foreach (string str1 in strArray1)
        {
          char[] separator = new char[1]{ '=' };
          int num2 = 1;
          string[] strArray2 = str1.Split(separator, (StringSplitOptions) num2);
          if (strArray2.Length >= 2)
          {
            string str2 = strArray2[1];
            if (strArray2[0] == "quality")
              str2 = ScriptLocalization.Get("equip_color_" + str2, true);
            para.Add(num1.ToString(), str2);
            ++num1;
          }
        }
      }
      return ScriptLocalization.GetWithPara(this.description, para, true);
    }
  }
}
