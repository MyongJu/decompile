﻿// Decompiled with JetBrains decompiler
// Type: IllegalWordsUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class IllegalWordsUtils
{
  private const string FILTER_SOURCE_PATH = "TextAsset/WordFilter/FilterSource_";
  private const string PUNCTUATION_MARKER = "`";
  private static bool inited;
  private static string[] lineArray;

  public static void Init()
  {
    TextAsset textAsset = AssetManager.Instance.HandyLoad("TextAsset/WordFilter/FilterSource_" + LocalizationManager.CurrentLanguageCode, (System.Type) null) as TextAsset;
    if ((UnityEngine.Object) textAsset != (UnityEngine.Object) null)
      IllegalWordsUtils.lineArray = textAsset.text.Split("|"[0]);
    IllegalWordDetection.Init(IllegalWordsUtils.lineArray);
  }

  public static string Filter(string content)
  {
    string str = string.Copy(content);
    if (!IllegalWordsUtils.inited)
    {
      IllegalWordsUtils.Init();
      IllegalWordsUtils.inited = true;
    }
    try
    {
      if (IllegalWordsUtils.CurrentFilterType() != IllegalWordsUtils.FilterType.MATCH_ALL)
        return IllegalWordDetection.Filter(content, "*");
      content = "`" + content.Replace(" ", "`") + "`";
      return IllegalWordDetection.Filter(content, "*").Substring(1, content.Length - 2).Replace("`", " ");
    }
    catch
    {
      return str;
    }
  }

  public static bool WarningIllegalWords(string content)
  {
    string content1 = string.Copy(content);
    if (content.Equals(IllegalWordsUtils.Filter(content1)))
      return false;
    UIManager.inst.toast.Show(Utils.XLAT("toast_all_name_failure"), (System.Action) null, 4f, false);
    return true;
  }

  public static void Dispose()
  {
    IllegalWordsUtils.inited = false;
  }

  private static IllegalWordsUtils.FilterType CurrentFilterType()
  {
    string currentLanguageCode = LocalizationManager.CurrentLanguageCode;
    if (currentLanguageCode != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (IllegalWordsUtils.\u003C\u003Ef__switch\u0024map77 == null)
      {
        // ISSUE: reference to a compiler-generated field
        IllegalWordsUtils.\u003C\u003Ef__switch\u0024map77 = new Dictionary<string, int>(1)
        {
          {
            "zh-CN",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (IllegalWordsUtils.\u003C\u003Ef__switch\u0024map77.TryGetValue(currentLanguageCode, out num) && num == 0)
        return IllegalWordsUtils.FilterType.MATCH_PART;
    }
    return IllegalWordsUtils.FilterType.MATCH_ALL;
  }

  private enum FilterType
  {
    MATCH_ALL,
    MATCH_PART,
  }
}
