﻿// Decompiled with JetBrains decompiler
// Type: ArmyFormationData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using March;
using UnityEngine;

public class ArmyFormationData
{
  protected float _unitsLength;
  public int curUnitCount;
  public Vector3 centerDeltaPos;

  public MarchViewControler.TroopViewType type { private set; get; }

  public int curTroopCount { private set; get; }

  public int maxUnitCount { private set; get; }

  protected virtual MarchConfig.MarchInfo configInfo
  {
    get
    {
      return (MarchConfig.MarchInfo) MarchConfig.baseInfo;
    }
  }

  public virtual float unitDefaultScale
  {
    get
    {
      return this.configInfo.unitScale_move;
    }
  }

  public virtual float unitAttackScale
  {
    get
    {
      return this.configInfo.unitScale_action;
    }
  }

  public virtual float formationHalfWidth
  {
    get
    {
      return this.configInfo.formationLineWidth;
    }
  }

  public virtual float unitLineStep
  {
    get
    {
      return this.configInfo.formationLineStep;
    }
  }

  public virtual float unitHeadLineStep
  {
    get
    {
      return this.configInfo.formationHeadStep;
    }
  }

  public virtual float unitTailLineStep
  {
    get
    {
      return this.configInfo.formationTailStep;
    }
  }

  public virtual float unitEmptyLineStep
  {
    get
    {
      return this.configInfo.formationHeadStep + this.configInfo.formationTailStep;
    }
  }

  public virtual int[] unitStepCount
  {
    get
    {
      return this.configInfo.unitStepCount;
    }
  }

  public int curUnitStepIndex { private set; get; }

  public float unitsLength
  {
    get
    {
      return this._unitsLength;
    }
  }

  public virtual Vector2[] unitsMoveFormation
  {
    get
    {
      return this.configInfo.formation_move;
    }
  }

  public virtual Vector3[] uintsActionFormation
  {
    get
    {
      return this.configInfo.formation_action;
    }
  }

  public Vector2 curUnitFormation { private set; get; }

  public int curUnitFormation_x { private set; get; }

  public int curUnitFormation_y { private set; get; }

  public virtual MarchSystem.MissileType missileType
  {
    get
    {
      return MarchSystem.MissileType.none;
    }
  }

  public virtual float SetData(MarchViewControler.TroopViewType inType, int inTroopCount)
  {
    this.type = inType;
    this.curTroopCount = inTroopCount;
    if (this.unitStepCount != null)
    {
      this.curUnitStepIndex = this.unitStepCount.Length;
      while (this.curUnitStepIndex > 0 && inTroopCount <= this.unitStepCount[this.curUnitStepIndex - 1])
        --this.curUnitStepIndex;
    }
    this._unitsLength = this.CalcFormationLength(this.curUnitStepIndex);
    if (this.unitsMoveFormation != null)
    {
      this.curUnitFormation = this.unitsMoveFormation[this.curUnitStepIndex];
      this.curUnitFormation_x = (int) this.curUnitFormation.x;
      this.curUnitFormation_y = (int) this.curUnitFormation.y;
      this.curUnitCount = this.curUnitFormation_x * this.curUnitFormation_y;
    }
    this.maxUnitCount = (int) ((double) this.unitsMoveFormation[this.unitStepCount.Length].x * (double) this.unitsMoveFormation[this.unitStepCount.Length].y);
    return this.unitsLength;
  }

  protected virtual float CalcFormationLength(int index = 0)
  {
    if (this.curTroopCount == 0)
      return 0.0f;
    return this.unitsMoveFormation[index].x * this.unitLineStep + this.unitEmptyLineStep;
  }

  public virtual Vector3[] CalcAttackFormation(Vector3 deltaPos, float radius, Vector3 dir)
  {
    this.centerDeltaPos = deltaPos;
    Vector3[] pos = new Vector3[this.curUnitCount];
    float num1 = 3.141593f / (float) this.curUnitCount;
    int index1 = 0;
    for (int index2 = 0; index2 < this.curUnitFormation_x; ++index2)
    {
      int num2 = 0;
      while (num2 < this.curUnitFormation_y)
      {
        pos[index1].x = (float) (1.5 * (double) Mathf.Cos((float) (3.14159274101257 + (double) num1 * (double) index1 * ((double) Random.value * 0.100000001490116 + 0.949999988079071))) * 1.0 * 0.800000011920929 * ((double) Random.value * 0.200000002980232 + 0.800000011920929));
        pos[index1].y = (float) (0.75 * (double) Mathf.Sin((float) (3.14159274101257 + (double) num1 * (double) index1 * ((double) Random.value * 0.100000001490116 + 0.949999988079071))) * 1.0 * 0.800000011920929 * ((double) Random.value * 0.200000002980232 + 0.800000011920929));
        pos[index1].z = 0.0f;
        ++num2;
        ++index1;
      }
    }
    this.RecalcAttackFormation(ref pos, deltaPos, radius);
    return pos;
  }

  public virtual void RecalcAttackFormation(ref Vector3[] pos, Vector3 deltaPos, float radius)
  {
    if (pos == null)
      return;
    for (int index = 0; index < pos.Length; ++index)
    {
      pos[index] *= radius;
      pos[index] += deltaPos;
    }
  }
}
