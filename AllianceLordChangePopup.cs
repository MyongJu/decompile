﻿// Decompiled with JetBrains decompiler
// Type: AllianceLordChangePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Pathfinding.Serialization.JsonFx;
using UI;

public class AllianceLordChangePopup : BaseReportPopup
{
  private AllianceLordChangePopup.TIData tid;
  public Icon oldlord;
  public Icon newlord;
  public UILabel content;

  private void Init()
  {
    this.oldlord.FeedData((IComponentData) new IconData(UserData.GetPortraitIconPath(this.tid.old_portrait), this.tid.old_icon, this.tid.old_lord_title, new string[1]
    {
      this.tid.old_name
    }));
    this.newlord.FeedData((IComponentData) new IconData(UserData.GetPortraitIconPath(this.tid.new_portrait), this.tid.new_icon, this.tid.new_lord_title, new string[1]
    {
      this.tid.new_name
    }));
    this.content.text = this.param.mail.GetBodyString();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.tid = JsonReader.Deserialize<AllianceLordChangePopup.TIData>(Utils.Object2Json(this.param.hashtable[(object) "data"]));
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public class TIData
  {
    public string old_name;
    public int old_portrait;
    public string old_icon;
    public int old_lord_title;
    public string new_name;
    public int new_portrait;
    public string new_icon;
    public int new_lord_title;
  }
}
