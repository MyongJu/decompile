﻿// Decompiled with JetBrains decompiler
// Type: VIPStatEntry
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class VIPStatEntry : MonoBehaviour
{
  public UILabel statBonusAmount;
  public UILabel statBonusName;
  public UILabel rightSideStatBonusAmount;

  public void SetData(Effect effect)
  {
    float modifier = effect.Modifier;
    if (effect.Property == null)
    {
      this.statBonusAmount.text = string.Empty;
      this.statBonusName.text = effect.Name;
      this.rightSideStatBonusAmount.gameObject.SetActive(false);
    }
    else
    {
      switch (effect.Property.Format)
      {
        case PropertyDefinition.FormatType.Integer:
          this.statBonusAmount.text = "+" + ((int) modifier).ToString();
          this.statBonusName.text = effect.Name;
          this.rightSideStatBonusAmount.gameObject.SetActive(false);
          break;
        case PropertyDefinition.FormatType.Percent:
          this.statBonusAmount.text = "+" + ((int) ((double) modifier * 100.0)).ToString() + "%";
          this.statBonusName.text = effect.Name;
          this.rightSideStatBonusAmount.gameObject.SetActive(false);
          break;
        case PropertyDefinition.FormatType.Seconds:
          this.statBonusAmount.text = string.Empty;
          this.rightSideStatBonusAmount.text = ((int) modifier).ToString();
          this.statBonusName.text = effect.Name;
          break;
        case PropertyDefinition.FormatType.Boolean:
          this.statBonusAmount.text = ((int) modifier != 0).ToString();
          this.statBonusName.text = effect.Name;
          this.rightSideStatBonusAmount.gameObject.SetActive(false);
          break;
      }
    }
  }
}
