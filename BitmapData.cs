﻿// Decompiled with JetBrains decompiler
// Type: BitmapData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BitmapData
{
  public int height;
  public int width;
  private Color[] pixels;

  public BitmapData(Texture2D texture)
  {
    this.height = texture.height;
    this.width = texture.width;
    this.pixels = texture.GetPixels();
  }

  public Color getPixelColor(int x, int y)
  {
    if (x >= this.width)
      x = this.width - 1;
    if (y >= this.height)
      y = this.height - 1;
    if (x < 0)
      x = 0;
    if (y < 0)
      y = 0;
    return this.pixels[y * this.width + x];
  }
}
