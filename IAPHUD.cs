﻿// Decompiled with JetBrains decompiler
// Type: IAPHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class IAPHUD : MonoBehaviour
{
  private string m_VfxName = string.Empty;
  private const string VFX_PATH = "Prefab/IAP/";
  public GameObject m_RootNode;
  public UILabel m_Timer;
  private bool m_Start;
  private GameObject m_VFX;

  private void Start()
  {
    this.m_Start = true;
    this.UpdateUI();
  }

  private void OnEnable()
  {
    if (this.m_Start)
      this.UpdateUI();
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  public void OnIAPStoreClicked()
  {
    Utils.ShowRecommendIAPPackagePopupByPolicy();
    OperationTrace.TraceClick(ClickArea.IAPRecommendPackage);
  }

  private void OnSecondEvent(int serverTimestamp)
  {
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (!Utils.IsIAPStoreEnabled)
    {
      this.m_RootNode.SetActive(false);
    }
    else
    {
      IAPStorePackage packageByPolicty = Utils.GetRecommendedIAPStorePackageByPolicty();
      if (packageByPolicty != null)
      {
        this.m_RootNode.SetActive(true);
        this.m_Timer.text = Utils.FormatTime((int) packageByPolicty.LeftTime, true, false, true);
        this.SetVFX(packageByPolicty.group.icon);
      }
      else
      {
        this.DeleteVFX();
        this.m_RootNode.SetActive(false);
      }
    }
  }

  private void SetVFX(string vfxName)
  {
    if (!(this.m_VfxName != vfxName))
      return;
    this.DeleteVFX();
    this.m_VfxName = vfxName;
    this.m_VFX = AssetManager.Instance.HandyLoad("Prefab/IAP/" + vfxName, typeof (GameObject)) as GameObject;
    if (!(bool) ((UnityEngine.Object) this.m_VFX))
      return;
    this.m_VFX = UnityEngine.Object.Instantiate<GameObject>(this.m_VFX);
    this.m_VFX.transform.parent = this.m_RootNode.transform;
    this.m_VFX.transform.localPosition = Vector3.zero;
    this.m_VFX.transform.localScale = Vector3.one;
  }

  private void DeleteVFX()
  {
    this.m_VfxName = string.Empty;
    if (!((UnityEngine.Object) this.m_VFX != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_VFX);
    this.m_VFX = (GameObject) null;
  }
}
