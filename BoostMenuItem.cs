﻿// Decompiled with JetBrains decompiler
// Type: BoostMenuItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class BoostMenuItem : MonoBehaviour
{
  public UISprite icon;
  public UILabel boostName;
  public UIProgressBar progressBar;
  public UILabel remainTime;
  public BoostContainerData parent;
  public BoostContainer ower;
  private UIButton button;
  public GameObject line;
  public UITexture itemIcon;
  public UILabel boostDesc;
  public GameObject arrow;
  public GameObject rootCdTime;
  public UILabel labelCdTime;
  private BuffUIData _data;

  private void Awake()
  {
    this.button = this.gameObject.AddMissingComponent<UIButton>();
    EventDelegate.Add(this.button.onClick, new EventDelegate.Callback(this.OnClickHandler));
  }

  private void OnClickHandler()
  {
    if (!this._data.ListenerClick())
      return;
    if (this._data is BuffFromItemData)
    {
      if (this._data.GetLeftCdTime() > 0)
        return;
      UIManager.inst.OpenDlg("Boost/Boosts_Items", (UI.Dialog.DialogParameter) new BoostsItemsDlg.BoostListDialogParameter()
      {
        data = this._data
      }, true, true, true);
    }
    else
      UIManager.inst.OpenDlg("Alliance/AllianceTempleSkillDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void SetLineVisiable(bool canSee)
  {
    this.line.gameObject.SetActive(canSee);
  }

  public void SetBoostItem(BuffUIData data, BoostContainerData parent)
  {
    this._data = data;
    this.arrow.SetActive(this._data.ListenerClick());
    this.parent = parent;
    this.UpateUI();
  }

  private void AddEventHandler()
  {
  }

  private void RefreshCdTime()
  {
    int leftCdTime = this._data.GetLeftCdTime();
    if (leftCdTime > 0 && !this._data.IsRunning())
    {
      this.rootCdTime.SetActive(true);
      this.labelCdTime.text = Utils.XLAT("id_cooldown") + " " + Utils.FormatTime(leftCdTime, false, false, true);
    }
    else
      this.rootCdTime.SetActive(false);
  }

  private void RefreshEveryFrame()
  {
    float progressValue = this._data.GetProgressValue();
    if ((double) progressValue <= 0.0)
    {
      this.progressBar.gameObject.SetActive(false);
    }
    else
    {
      this.progressBar.gameObject.SetActive(true);
      this.progressBar.value = progressValue;
    }
    this.remainTime.text = Utils.FormatTime(this._data.RemainTime(), true, false, true);
  }

  public void Clear()
  {
    this._data = (BuffUIData) null;
    this.parent = (BoostContainerData) null;
    this.ower = (BoostContainer) null;
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnProcess);
    BuilderFactory.Instance.Release((UIWidget) this.itemIcon);
  }

  private void OnProcess(int time)
  {
    if (this.parent == null || this._data == null)
      return;
    this.RefreshCdTime();
    if (!this._data.IsRunning())
      this.progressBar.gameObject.SetActive(false);
    else
      this.RefreshEveryFrame();
  }

  private void UpateUI()
  {
    this.boostName.text = this._data.Name;
    this.boostDesc.text = this._data.Desc;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.itemIcon, this._data.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    this.RefreshEveryFrame();
    this.RefreshCdTime();
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnProcess);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnProcess);
  }
}
