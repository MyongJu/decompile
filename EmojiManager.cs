﻿// Decompiled with JetBrains decompiler
// Type: EmojiManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Text;
using UI;
using UnityEngine;

public class EmojiManager
{
  private int EMOJI_COUNT = 200;
  private Dictionary<string, Emoji> emojiMap = new Dictionary<string, Emoji>();
  private const string LOCAL_STORE_KEY = "Emoji_Fav_Store";
  private const int FAV_MAX_COUNT = 56;
  private static EmojiManager instance;
  public System.Action<string> OnEmojiInput;
  private Dictionary<int, List<Emoji>> emojiListByGroup;
  private List<Emoji> favorEmojiList;

  public static EmojiManager Instance
  {
    get
    {
      if (EmojiManager.instance == null)
      {
        EmojiManager.instance = new EmojiManager();
        EmojiManager.instance.Init();
      }
      return EmojiManager.instance;
    }
  }

  public void Init()
  {
    int num = this.EMOJI_COUNT;
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("emoji_count");
    if (data != null)
      num = data.ValueInt;
    for (int index = 0; index < num; ++index)
      this.emojiMap.Add("#" + index.ToString("d3"), new Emoji()
      {
        id = "#" + index.ToString("d3"),
        type = 0,
        param = "Texture/Emoji/#" + index.ToString("d3")
      });
  }

  public Dictionary<int, List<Emoji>> GetEmojiList()
  {
    if (this.emojiListByGroup == null)
    {
      this.emojiListByGroup = new Dictionary<int, List<Emoji>>();
      IEnumerator enumerator = (IEnumerator) this.emojiMap.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        Emoji current = enumerator.Current as Emoji;
        if (!this.emojiListByGroup.ContainsKey(current.type))
          this.emojiListByGroup.Add(current.type, new List<Emoji>());
        this.emojiListByGroup[current.type].Add(current);
      }
    }
    return this.emojiListByGroup;
  }

  public List<Emoji> GetEmojiListByGroup(int group)
  {
    if (this.GetEmojiList().ContainsKey(group))
      return this.GetEmojiList()[group];
    return new List<Emoji>();
  }

  public List<Emoji> GetFavorEmojiList()
  {
    if (this.favorEmojiList == null)
    {
      this.favorEmojiList = new List<Emoji>();
      string stringByUid = PlayerPrefsEx.GetStringByUid("Emoji_Fav_Store");
      if (!string.IsNullOrEmpty(stringByUid))
      {
        string[] strArray = stringByUid.Split(';');
        for (int index = 0; index < strArray.Length; ++index)
        {
          if (this.emojiMap.ContainsKey(strArray[index]))
            this.favorEmojiList.Add(this.emojiMap[strArray[index]]);
        }
      }
    }
    return this.favorEmojiList;
  }

  public bool IsEmoji(string emojiID)
  {
    return this.emojiMap.ContainsKey(emojiID);
  }

  public Emoji GetEmojiByID(string emojiID)
  {
    return this.emojiMap[emojiID];
  }

  public void ShowEmojiInput(System.Action<string> emojiInputCallBack)
  {
    this.OnEmojiInput = emojiInputCallBack;
    UIManager.inst.OpenPopup("Emoji/EmojiInputPopup", (Popup.PopupParameter) null);
  }

  public void ProcessEmojiLabel(UILabel uiLabel)
  {
    this.Reset(uiLabel);
    if (!uiLabel.text.Contains("#"))
      return;
    UIWidget uiWidget = NGUITools.AddWidget<UIWidget>(uiLabel.gameObject);
    uiWidget.depth = uiLabel.depth + 1;
    uiWidget.name = "EmojiParent";
    uiLabel.alignment = NGUIText.Alignment.Left;
    uiLabel.pivot = UIWidget.Pivot.TopLeft;
    uiLabel.UpdateNGUIText();
    int fontSize = uiLabel.fontSize;
    float width = (float) uiLabel.width;
    float mLineHeight = (float) uiLabel.fontSize + uiLabel.effectiveSpacingY;
    float effectiveSpacingX = uiLabel.effectiveSpacingX;
    string processedText = uiLabel.processedText;
    uiLabel.UpdateNGUIText();
    StringBuilder sb = new StringBuilder();
    int numOfLine = 0;
    float regionWidth = width;
    for (int offset = 0; offset < processedText.Length; ++offset)
    {
      char ch = processedText[offset];
      switch (ch)
      {
        case '\n':
          ++numOfLine;
          regionWidth = width;
          goto default;
        case '#':
          if (!this.ParseEmoji(ref processedText, ref regionWidth, ref numOfLine, ref offset, ref sb, width, mLineHeight, effectiveSpacingX, uiWidget.gameObject))
            goto default;
          else
            break;
        default:
          float num = NGUIText.GetGlyphWidth((int) ch, 0) + effectiveSpacingX;
          if ((double) regionWidth - (double) num < 0.0)
          {
            sb.Append('\n');
            ++numOfLine;
            regionWidth = width;
          }
          sb.Append(ch);
          regionWidth -= num;
          break;
      }
    }
    uiLabel.text = sb.ToString();
  }

  public void Reset(UILabel uiLabel)
  {
    Transform child = uiLabel.transform.FindChild("EmojiParent");
    if (!((UnityEngine.Object) child != (UnityEngine.Object) null))
      return;
    NGUITools.Destroy((UnityEngine.Object) child.gameObject);
  }

  public void InputEmoji(string emoji)
  {
    if (this.OnEmojiInput != null)
      this.OnEmojiInput(emoji);
    this.AddToFav(emoji);
  }

  public EmojiRender CreateRender(string id, GameObject parent)
  {
    string renderPath = this.emojiMap[id].GetRenderPath();
    return NGUITools.AddChild(parent, AssetManager.Instance.HandyLoad(renderPath, (System.Type) null) as GameObject).GetComponent<EmojiRender>();
  }

  public void AddToFav(string id)
  {
    if (!this.emojiMap.ContainsKey(id) || this.GetFavorEmojiList().Contains(this.emojiMap[id]))
      return;
    this.GetFavorEmojiList().Insert(0, this.emojiMap[id]);
    string str = string.Empty;
    int num = Mathf.Min(56, this.favorEmojiList.Count);
    for (int index = 0; index < num; ++index)
      str = str + this.favorEmojiList[index].id + ";";
    PlayerPrefsEx.SetStringByUid("Emoji_Fav_Store", str);
  }

  private bool ParseEmoji(ref string text, ref float regionWidth, ref int numOfLine, ref int offset, ref StringBuilder sb, float mLineWidth, float mLineHeight, float mSpacingX, GameObject parent)
  {
    int num1 = offset;
    int lastNumberIndex = this.GetLastNumberIndex(text, num1);
    if (lastNumberIndex >= 0)
    {
      string emojiID = text.Substring(num1, lastNumberIndex - num1 + 1);
      if (this.IsEmoji(emojiID))
      {
        float width = NGUIText.GetGlyphWidth(32, 0) + mSpacingX;
        if ((double) width < 0.00999999977648258)
          width = mLineHeight + mSpacingX;
        int repeatCount = this.CalcuNumOfPlaceholder(mLineHeight, width);
        float num2 = (float) repeatCount * width;
        if ((double) regionWidth - (double) num2 < 0.0)
        {
          sb.Append('\n');
          ++numOfLine;
          regionWidth = mLineWidth;
        }
        Vector3 emojiPos = this.CalculateEmojiPos((float) ((double) mLineWidth - (double) regionWidth + (double) num2 / 2.0), (float) numOfLine, mLineHeight);
        this.AddEmoji(parent, emojiPos, emojiID, mLineHeight, mLineHeight);
        sb.Append(' ', repeatCount);
        regionWidth -= num2;
        offset += lastNumberIndex - num1;
        return true;
      }
    }
    return false;
  }

  private int GetLastNumberIndex(string str, int start)
  {
    int num = -1;
    for (int index = start + 1; index < str.Length && char.IsDigit(str[index]); ++index)
      num = index;
    return num;
  }

  private int CalcuNumOfPlaceholder(float mLineHeight, float width)
  {
    return (double) width <= 0.0 ? 4 : Mathf.CeilToInt(mLineHeight / width);
  }

  private Vector3 CalculateEmojiPos(float x, float y, float mLineHeight)
  {
    float num = (y + 0.5f) * mLineHeight;
    return new Vector3(x, -num, 0.0f);
  }

  private void AddEmoji(GameObject parent, Vector3 pos, string emojiID, float width, float height)
  {
    EmojiManager.Instance.CreateRender(emojiID, parent).Show(EmojiManager.Instance.GetEmojiByID(emojiID), pos, new Vector2(width * 1.2f, height * 1.2f));
  }
}
