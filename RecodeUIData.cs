﻿// Decompiled with JetBrains decompiler
// Type: RecodeUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;

public class RecodeUIData
{
  private List<string> _attackers = new List<string>();
  private List<string> _defenders = new List<string>();
  public const int TYPE_REINFORED = 1;
  public const int TYPE_OCCUPY = 2;
  public const int TYPE_ATTACK_OCCUPY = 3;
  private int _type;
  private long _time;
  private string _attaker;
  private string _defender;

  public long Time
  {
    get
    {
      return this._time;
    }
    set
    {
      this._time = value;
    }
  }

  public bool IsWonder { get; set; }

  public int Type
  {
    get
    {
      return this._type;
    }
    set
    {
      this._type = value;
    }
  }

  public string Attacker
  {
    get
    {
      return this._attaker;
    }
    set
    {
      this._attaker = value;
    }
  }

  public string Defender
  {
    get
    {
      return this._defender;
    }
    set
    {
      this._defender = value;
    }
  }

  public string Name { get; set; }

  private string GetAttackerName()
  {
    return this._attaker;
  }

  private string GetDefenserName()
  {
    return this._defender;
  }

  public string GetContent()
  {
    string Term = string.Empty;
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("0", this.GetAttackerName());
    switch (this._type)
    {
      case 1:
        Term = !this.IsWonder ? "throne_history_miniwonder_assistance" : "throne_history_wonder_assistance";
        break;
      case 2:
        Term = !this.IsWonder ? "throne_history_miniwonder_occupant" : "throne_history_wonder_occupant";
        break;
      case 3:
        Term = !this.IsWonder ? "throne_history_miniwonder_battle" : "throne_history_wonder_battle";
        para.Add("1", this.GetDefenserName());
        break;
    }
    return ScriptLocalization.GetWithPara(Term, para, true);
  }

  public bool Decode(object sources)
  {
    Hashtable hashtable = sources as Hashtable;
    if (hashtable == null)
      return false;
    string empty = string.Empty;
    if (hashtable.ContainsKey((object) "type") && hashtable[(object) "type"] != null)
      int.TryParse(hashtable[(object) "type"].ToString(), out this._type);
    if (hashtable.ContainsKey((object) "time") && hashtable[(object) "time"] != null)
      long.TryParse(hashtable[(object) "time"].ToString(), out this._time);
    this._attackers.Clear();
    this._defenders.Clear();
    if (hashtable.ContainsKey((object) "atk") && hashtable[(object) "atk"] != null)
      this._attaker = hashtable[(object) "atk"].ToString();
    if (hashtable.ContainsKey((object) "def") && hashtable[(object) "def"] != null)
      this._defender = hashtable[(object) "def"].ToString();
    return true;
  }
}
