﻿// Decompiled with JetBrains decompiler
// Type: ChatBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Chat;
using DB;
using I2.Loc;
using NLPTrans;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ChatBar : MonoBehaviour
{
  public GameObject coordinateTemple;
  public int index;
  private ChatMessage _sendMessage;
  private int _sendTime;
  private bool _isTranslated;
  private string _displayingIconSymbol;
  public System.Action<ChatMessage> onPortaitClick;
  public System.Action<ChatMessage> onBarClick;
  public System.Action<int> onTranslateEnd;
  [SerializeField]
  private ChatBar.Panel panel;

  private bool isTranslated
  {
    get
    {
      return this._sendMessage.showTranslated;
    }
    set
    {
      if (this._sendMessage.showTranslated)
      {
        this.panel.BT_translate_text.alignment = NGUIText.Alignment.Left;
        this.panel.BT_translate_text.text = ScriptLocalization.GetWithMultyContents("chat_original_lang_description", true, Utils.XLAT("translated_lang_" + this._sendMessage.language));
      }
      else
      {
        this.panel.BT_translate_text.alignment = NGUIText.Alignment.Left;
        this.panel.BT_translate_text.text = Utils.XLAT("language_status_original");
      }
    }
  }

  public void SetInfo(ChatMessage message)
  {
    this._sendMessage = message;
    this.SetName(message);
    this._sendTime = message.sendTime;
    this.SetMessage(message);
    this.FormatTime();
    this.panel.SenderContainer.gameObject.SetActive(message.senderUid == PlayerData.inst.uid);
    this.panel.OthersContainer.gameObject.SetActive(message.senderUid != PlayerData.inst.uid);
    this.DrawIcon();
    this._sendMessage.onTranslateEnd += new System.Action(this.OnTranslateEnd);
  }

  public void DrawIcon()
  {
    this.panel.Other_Mute_Admin_Tip.gameObject.SetActive(false);
    this.panel.Sender_Mute_Admin_Tip.gameObject.SetActive(false);
    if (this._sendMessage.isMod && this._sendMessage.traceData.type == ChatTraceData.TraceType.Empty)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.Sender_userIcon, "Texture/Hero/Portrait_Icon/head_Mod", (System.Action<bool>) null, true, false, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.Other_userIcon, "Texture/Hero/Portrait_Icon/head_Mod", (System.Action<bool>) null, true, false, string.Empty);
      LordTitlePayload.Instance.ApplyUserAvator(this.panel.Sender_userIcon, 0, 1);
      LordTitlePayload.Instance.ApplyUserAvator(this.panel.Other_userIcon, 0, 1);
      NGUITools.SetActive(this.panel.Other_portraitBG.gameObject, false);
      NGUITools.SetActive(this.panel.Sender_protraitBG.gameObject, false);
      ChatMessage sendMessage = this._sendMessage;
      if (sendMessage.senderUid == PlayerData.inst.uid)
      {
        if (!this.ShouldProcessLabel(sendMessage))
          return;
        EmojiManager.Instance.ProcessEmojiLabel(this.panel.Sender_msg);
        UILabelPostProcess.ProcessCoordinate(this.panel.Sender_msg, this.coordinateTemple);
      }
      else
      {
        if (!this.ShouldProcessLabel(sendMessage))
          return;
        EmojiManager.Instance.ProcessEmojiLabel(this.panel.Other_msg);
        UILabelPostProcess.ProcessCoordinate(this.panel.Other_msg, this.coordinateTemple);
      }
    }
    else
    {
      NGUITools.SetActive(this.panel.Other_portraitBG.gameObject, true);
      NGUITools.SetActive(this.panel.Sender_protraitBG.gameObject, true);
      ChatMessage sendMessage = this._sendMessage;
      if (sendMessage.senderUid == PlayerData.inst.uid)
      {
        CustomIconLoader.Instance.requestCustomIcon(this.panel.Sender_userIcon, UserData.GetPortraitIconPath(sendMessage.senderIcon), sendMessage.senderCustomIconUrl, false);
        LordTitlePayload.Instance.ApplyUserAvator(this.panel.Sender_userIcon, PlayerData.inst.userData.LordTitle, 11);
        if (!this.ShouldProcessLabel(sendMessage))
          return;
        EmojiManager.Instance.ProcessEmojiLabel(this.panel.Sender_msg);
        UILabelPostProcess.ProcessCoordinate(this.panel.Sender_msg, this.coordinateTemple);
      }
      else
      {
        CustomIconLoader.Instance.requestCustomIcon(this.panel.Other_userIcon, UserData.GetPortraitIconPath(sendMessage.senderIcon), sendMessage.senderCustomIconUrl, false);
        LordTitlePayload.Instance.ApplyUserAvator(this.panel.Other_userIcon, sendMessage.senderLordTitle, 11);
        if (!this.ShouldProcessLabel(sendMessage))
          return;
        EmojiManager.Instance.ProcessEmojiLabel(this.panel.Other_msg);
        UILabelPostProcess.ProcessCoordinate(this.panel.Other_msg, this.coordinateTemple);
      }
    }
  }

  public void Reset()
  {
    this._displayingIconSymbol = string.Empty;
    if (this._sendMessage != null)
      this._sendMessage.onTranslateEnd -= new System.Action(this.OnTranslateEnd);
    for (int index = 0; index < this.panel.Sender_msg.transform.childCount; ++index)
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.panel.Sender_msg.transform.GetChild(index).gameObject);
    for (int index = 0; index < this.panel.Other_msg.transform.childCount; ++index)
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.panel.Other_msg.transform.GetChild(index).gameObject);
  }

  public void FormatTime()
  {
    int num1 = (NetServerTime.inst.ServerTimestamp - this._sendTime) / 60;
    int num2 = 43200;
    int num3 = 1440;
    int num4 = 60;
    int num5;
    bool flag1 = (num5 = 0) != 0;
    bool flag2 = num5 != 0;
    bool flag3 = num5 != 0;
    bool flag4 = num5 != 0;
    int num6 = num1 / num2;
    int num7 = num1 - num6 * num2;
    bool flag5;
    bool flag6;
    bool flag7;
    bool flag8 = flag4 | (flag7 = flag3 | (flag6 = flag2 | (flag5 = flag1 | num6 > 0)));
    int num8 = num7 / num3;
    int num9 = num7 - num8 * num3;
    bool flag9;
    bool flag10;
    bool flag11 = flag7 | (flag10 = flag6 | (flag9 = flag5 | num8 > 0));
    int num10 = num9 / num4;
    int num11 = num9 - num10 * num4;
    bool flag12;
    bool flag13 = flag10 | (flag12 = flag9 | num10 > 0);
    int num12 = num11;
    if (!(flag12 | num12 > 0))
    {
      UILabel senderTime = this.panel.Sender_time;
      string str1 = Utils.XLAT("chat_time_less_than_min");
      this.panel.Other_time.text = str1;
      string str2 = str1;
      senderTime.text = str2;
    }
    else
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      para.Add("d", num8 <= 0 ? string.Empty : num8.ToString() + Utils.XLAT("chat_time_day"));
      para.Add("h", num10 <= 0 ? string.Empty : num10.ToString() + Utils.XLAT("chat_time_hour"));
      para.Add("m", num12 <= 0 ? string.Empty : num12.ToString() + Utils.XLAT("chat_time_min"));
      UILabel senderTime = this.panel.Sender_time;
      string withPara = ScriptLocalization.GetWithPara("chat_time_display", para, true);
      this.panel.Other_time.text = withPara;
      string str = withPara;
      senderTime.text = str;
    }
  }

  private void SetName(ChatMessage message)
  {
    if (message.isMod && message.traceData.type == ChatTraceData.TraceType.Empty)
    {
      UILabel senderUserName = this.panel.Sender_userName;
      string senderName = message.senderName;
      this.panel.Other_userName.text = senderName;
      string str = senderName;
      senderUserName.text = str;
      this.panel.Sender_VIP_Level.gameObject.SetActive(false);
      this.panel.Other_VIP_Level.gameObject.SetActive(false);
      this.panel.Other_Artifact.gameObject.SetActive(false);
      this.panel.Sender_Artifact.gameObject.SetActive(false);
    }
    else
    {
      UILabel senderUserName = this.panel.Sender_userName;
      string fullName = message.fullName;
      this.panel.Other_userName.text = fullName;
      string str1 = fullName;
      senderUserName.text = str1;
      if (message.senderVipLevel > 0)
      {
        this.panel.Sender_VIP_Level.gameObject.SetActive(true);
        this.panel.Other_VIP_Level.gameObject.SetActive(true);
        UILabel senderVipLevel = this.panel.Sender_VIP_Level;
        string str2 = string.Format("VIP{0}", (object) message.senderVipLevel);
        this.panel.Other_VIP_Level.text = str2;
        string str3 = str2;
        senderVipLevel.text = str3;
      }
      else
      {
        UILabel senderVipLevel = this.panel.Sender_VIP_Level;
        string empty = string.Empty;
        this.panel.Other_VIP_Level.text = empty;
        string str2 = empty;
        senderVipLevel.text = str2;
        this.panel.Sender_VIP_Level.gameObject.SetActive(false);
        this.panel.Other_VIP_Level.gameObject.SetActive(false);
      }
      if (message.senderArtifactID > 0)
      {
        ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(message.senderArtifactID);
        this.panel.Other_Artifact.gameObject.SetActive(true);
        this.panel.Sender_Artifact.gameObject.SetActive(true);
        BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.Other_Artifact, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
        BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.Sender_Artifact, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      }
      else
      {
        this.panel.Other_Artifact.gameObject.SetActive(false);
        this.panel.Sender_Artifact.gameObject.SetActive(false);
      }
    }
    this.panel.Other_Title_Table.repositionNow = true;
    this.panel.Sender_Title_Table.repositionNow = true;
  }

  private void SetMessage(ChatMessage message)
  {
    string text = message.text;
    this.panel.Sender_msg.alignment = NGUIText.Alignment.Left;
    this.panel.Other_msg.alignment = NGUIText.Alignment.Left;
    if (message.traceData.isSpeaker)
    {
      UILabel senderMsg = this.panel.Sender_msg;
      string str1 = text;
      this.panel.Other_msg.text = str1;
      string str2 = str1;
      senderMsg.text = str2;
      this.panel.Other_front_icon.gameObject.SetActive(true);
      this.panel.Sender_front_icon.gameObject.SetActive(true);
      this.panel.Other_tail_icon_container.gameObject.SetActive(false);
      this.panel.Sender_tail_icon_container.gameObject.SetActive(false);
      this.panel.Other_background.color = this.panel.OtherBackgroundColoer;
      this.panel.Sender_background.color = this.panel.SenderBackgroundColor;
      UISprite otherFrontIcon = this.panel.Other_front_icon;
      string str3 = "icon_loud_speaker";
      this.panel.Sender_front_icon.spriteName = str3;
      string str4 = str3;
      otherFrontIcon.spriteName = str4;
    }
    else if (message.traceData.type == ChatTraceData.TraceType.Empty)
    {
      UILabel senderMsg = this.panel.Sender_msg;
      string str1 = text;
      this.panel.Other_msg.text = str1;
      string str2 = str1;
      senderMsg.text = str2;
      this.panel.Other_tail_icon_container.gameObject.SetActive(false);
      this.panel.Sender_tail_icon_container.gameObject.SetActive(false);
      this.panel.Other_front_icon.gameObject.SetActive(false);
      this.panel.Sender_front_icon.gameObject.SetActive(false);
      this.panel.Other_background.color = this.panel.OtherBackgroundColoer;
      this.panel.Sender_background.color = this.panel.SenderBackgroundColor;
      if (message.isMod)
      {
        this.panel.Sender_background.color = this.panel.SystemBackgroundColor;
        this.panel.Other_background.color = this.panel.SystemBackgroundColor;
      }
    }
    else if (message.traceData.type == ChatTraceData.TraceType.NoDefined)
    {
      UILabel senderMsg = this.panel.Sender_msg;
      string str1 = Utils.XLAT("old_version_cannot_do_this_description");
      this.panel.Other_msg.text = str1;
      string str2 = str1;
      senderMsg.text = str2;
      this.panel.Other_tail_icon_container.gameObject.SetActive(false);
      this.panel.Sender_tail_icon_container.gameObject.SetActive(false);
      this.panel.Other_front_icon.gameObject.SetActive(false);
      this.panel.Sender_front_icon.gameObject.SetActive(false);
      this.panel.Other_background.color = this.panel.SystemBackgroundColor;
      this.panel.Sender_background.color = this.panel.SystemBackgroundColor;
    }
    else
    {
      this.panel.Other_background.color = this.panel.SystemBackgroundColor;
      this.panel.Sender_background.color = this.panel.SystemBackgroundColor;
      UILabel senderMsg1 = this.panel.Sender_msg;
      string str1 = string.Format("{0}\n", (object) text);
      this.panel.Other_msg.text = str1;
      string str2 = str1;
      senderMsg1.text = str2;
      this.panel.Other_front_icon.gameObject.SetActive(true);
      this.panel.Sender_front_icon.gameObject.SetActive(true);
      if (!message.traceData.isSpeaker)
      {
        UISprite otherFrontIcon = this.panel.Other_front_icon;
        string str3 = "icon_horn";
        this.panel.Sender_front_icon.spriteName = str3;
        string str4 = str3;
        otherFrontIcon.spriteName = str4;
      }
      else
      {
        UISprite otherFrontIcon = this.panel.Other_front_icon;
        string str3 = "icon_loud_speaker";
        this.panel.Sender_front_icon.spriteName = str3;
        string str4 = str3;
        otherFrontIcon.spriteName = str4;
      }
      this.panel.Other_tail_icon_container.gameObject.SetActive(true);
      this.panel.Sender_tail_icon_container.gameObject.SetActive(true);
      switch (message.traceData.type)
      {
        case ChatTraceData.TraceType.WarReport_Attack:
        case ChatTraceData.TraceType.WarReport_BeAttack:
        case ChatTraceData.TraceType.WarReport_Rally:
        case ChatTraceData.TraceType.WarReport_BeRally:
        case ChatTraceData.TraceType.WarReport_Scout:
          UISprite otherTailIcon1 = this.panel.Other_tail_icon;
          string str5 = "icon_chat_battle_report";
          this.panel.Sender_tail_icon.spriteName = str5;
          string str6 = str5;
          otherTailIcon1.spriteName = str6;
          UILabel otherTailIconText1 = this.panel.Other_tail_icon_text;
          string str7 = Utils.XLAT("chat_view_battle_report");
          this.panel.Sender_tail_icon_text.text = str7;
          string str8 = str7;
          otherTailIconText1.text = str8;
          break;
        case ChatTraceData.TraceType.Forge:
        case ChatTraceData.TraceType.ForgeDK:
          UISprite otherTailIcon2 = this.panel.Other_tail_icon;
          string str9 = "icon_chat_equipment_report";
          this.panel.Sender_tail_icon.spriteName = str9;
          string str10 = str9;
          otherTailIcon2.spriteName = str10;
          UILabel otherTailIconText2 = this.panel.Other_tail_icon_text;
          string str11 = Utils.XLAT("chat_view_equipment_details");
          this.panel.Sender_tail_icon_text.text = str11;
          string str12 = str11;
          otherTailIconText2.text = str12;
          break;
        case ChatTraceData.TraceType.CasinoCard:
          UISprite otherTailIcon3 = this.panel.Other_tail_icon;
          string str13 = "icon_chat_share_casino";
          this.panel.Sender_tail_icon.spriteName = str13;
          string str14 = str13;
          otherTailIcon3.spriteName = str14;
          UILabel otherTailIconText3 = this.panel.Other_tail_icon_text;
          string str15 = Utils.XLAT("chat_tavern_wheel_reward_link");
          this.panel.Sender_tail_icon_text.text = str15;
          string str16 = str15;
          otherTailIconText3.text = str16;
          break;
        case ChatTraceData.TraceType.Rally:
          UISprite otherTailIcon4 = this.panel.Other_tail_icon;
          string str17 = "icon_chat_rally_report";
          this.panel.Sender_tail_icon.spriteName = str17;
          string str18 = str17;
          otherTailIcon4.spriteName = str18;
          UILabel otherTailIconText4 = this.panel.Other_tail_icon_text;
          string str19 = Utils.XLAT("chat_alliance_join_rally");
          this.panel.Sender_tail_icon_text.text = str19;
          string str20 = str19;
          otherTailIconText4.text = str20;
          break;
        case ChatTraceData.TraceType.Bookmark:
          UISprite otherTailIcon5 = this.panel.Other_tail_icon;
          string str21 = "icon_chat_position_report";
          this.panel.Sender_tail_icon.spriteName = str21;
          string str22 = str21;
          otherTailIcon5.spriteName = str22;
          UILabel otherTailIconText5 = this.panel.Other_tail_icon_text;
          string str23 = Utils.XLAT("chat_view_coordinates");
          this.panel.Sender_tail_icon_text.text = str23;
          string str24 = str23;
          otherTailIconText5.text = str24;
          break;
        case ChatTraceData.TraceType.EquimentEnhance:
        case ChatTraceData.TraceType.EquimentEnhanceDK:
          UISprite otherTailIcon6 = this.panel.Other_tail_icon;
          string str25 = "icon_chat_equipment_report";
          this.panel.Sender_tail_icon.spriteName = str25;
          string str26 = str25;
          otherTailIcon6.spriteName = str26;
          UILabel otherTailIconText6 = this.panel.Other_tail_icon_text;
          string str27 = Utils.XLAT("chat_view_equipment_details");
          this.panel.Sender_tail_icon_text.text = str27;
          string str28 = str27;
          otherTailIconText6.text = str28;
          break;
        case ChatTraceData.TraceType.MagicReport_Attack:
        case ChatTraceData.TraceType.MagicReport_BeAttack:
          UISprite otherTailIcon7 = this.panel.Other_tail_icon;
          string str29 = "icon_chat_battle_report";
          this.panel.Sender_tail_icon.spriteName = str29;
          string str30 = str29;
          otherTailIcon7.spriteName = str30;
          UILabel otherTailIconText7 = this.panel.Other_tail_icon_text;
          string str31 = Utils.XLAT("chat_view_details");
          this.panel.Sender_tail_icon_text.text = str31;
          string str32 = str31;
          otherTailIconText7.text = str32;
          break;
        default:
          UILabel senderMsg2 = this.panel.Sender_msg;
          string str33 = string.Format("{0}", (object) text);
          this.panel.Other_msg.text = str33;
          string str34 = str33;
          senderMsg2.text = str34;
          this.panel.Other_tail_icon_container.gameObject.SetActive(false);
          this.panel.Sender_tail_icon_container.gameObject.SetActive(false);
          break;
      }
    }
    this.panel.BT_translate.gameObject.SetActive(message.traceData.type == ChatTraceData.TraceType.Empty);
    this.isTranslated = this._sendMessage.showTranslated;
  }

  private bool ShouldProcessLabel(ChatMessage message)
  {
    return message.traceData.type == ChatTraceData.TraceType.Empty;
  }

  public void OnPortaitClick()
  {
    if (this._sendMessage == null || this.onPortaitClick == null)
      return;
    this.onPortaitClick(this._sendMessage);
  }

  public void OnBarClick()
  {
    if (this._sendMessage != null)
    {
      if (this._sendMessage.traceData.type == ChatTraceData.TraceType.Empty)
      {
        ClipboardPopup.Parameter parameter = new ClipboardPopup.Parameter();
        parameter.uid = this._sendMessage.senderUid;
        parameter.str = this._sendMessage.chatText;
        parameter.reportAble = !string.IsNullOrEmpty(this._sendMessage.senderCustomIconUrl);
        parameter.sourceText = this._sendMessage.chatText;
        parameter.machineTargetText = this._sendMessage.translateText;
        parameter.targetLanguage = Language.TranslateTarget;
        parameter.sourceLanguage = this._sendMessage.language;
        if (this._sendMessage.senderUid == PlayerData.inst.uid)
          parameter.reportAble = false;
        UIManager.inst.OpenPopup("ClipboardPopup", (Popup.PopupParameter) parameter);
      }
      else if (this._sendMessage.senderUid != PlayerData.inst.uid)
        this.OnFunctionClick();
      else if (this._sendMessage.traceData.type == ChatTraceData.TraceType.WarReport_Attack || this._sendMessage.traceData.type == ChatTraceData.TraceType.WarReport_BeAttack || (this._sendMessage.traceData.type == ChatTraceData.TraceType.WarReport_Rally || this._sendMessage.traceData.type == ChatTraceData.TraceType.WarReport_BeRally) || (this._sendMessage.traceData.type == ChatTraceData.TraceType.WarReport_Scout || this._sendMessage.traceData.type == ChatTraceData.TraceType.CasinoCard || (this._sendMessage.traceData.type == ChatTraceData.TraceType.MagicReport_Attack || this._sendMessage.traceData.type == ChatTraceData.TraceType.MagicReport_BeAttack)) || (this._sendMessage.traceData.type == ChatTraceData.TraceType.Rally || this._sendMessage.traceData.type == ChatTraceData.TraceType.Bookmark))
        this.OnFunctionClick();
    }
    if (this.onBarClick == null)
      return;
    this.onBarClick(this._sendMessage);
  }

  public void OnTranslateClick()
  {
    if (this.isTranslated)
    {
      if (this.index == 8)
        Debug.Log((object) ("Transed: " + (object) this.index + ":" + (object) this.isTranslated + ":" + this._sendMessage.chatText + ":" + this._sendMessage.translateText));
      this.panel.Other_msg.text = this._sendMessage.chatText;
      EmojiManager.Instance.ProcessEmojiLabel(this.panel.Other_msg);
      UILabelPostProcess.ProcessCoordinate(this.panel.Other_msg, this.coordinateTemple);
      this._sendMessage.showTranslated = false;
      this.isTranslated = !this.isTranslated;
    }
    else if (!string.IsNullOrEmpty(this._sendMessage.translateText) && this._sendMessage.translateType == Language.TranslateTarget)
    {
      this.panel.Other_msg.text = this._sendMessage.translateText;
      EmojiManager.Instance.ProcessEmojiLabel(this.panel.Other_msg);
      UILabelPostProcess.ProcessCoordinate(this.panel.Other_msg, this.coordinateTemple);
      this._sendMessage.showTranslated = true;
      this.isTranslated = !this.isTranslated;
      if (this.index != 8)
        return;
      Debug.Log((object) ("Already trans" + (object) this.index + ":" + (object) this.isTranslated + ":" + this._sendMessage.chatText + ":" + this._sendMessage.translateText));
    }
    else
    {
      NLPUtils.Instance.TranslateAsyc(this._sendMessage.chatText, Language.TranslateTarget, this._sendMessage.language, this._sendMessage.senderUid.ToString(), ChatDlg.openType.ToString(), (System.Action<bool, string, string>) ((arg1, arg2, arg3) =>
      {
        if (!arg1)
          return;
        this.panel.Other_msg.text = this._sendMessage.translateText = arg2;
        EmojiManager.Instance.ProcessEmojiLabel(this.panel.Other_msg);
        UILabelPostProcess.ProcessCoordinate(this.panel.Other_msg, this.coordinateTemple);
        this._sendMessage.language = arg3;
        this._sendMessage.showTranslated = true;
      }));
      if (this.index == 8)
        Debug.Log((object) ("Transing:" + (object) this.index + ":" + (object) this.isTranslated + ":" + this._sendMessage.chatText + ":" + this._sendMessage.translateText));
      this._sendMessage.translateType = Language.TranslateTarget;
    }
  }

  private void OnTranslateEnd()
  {
    if (this.onTranslateEnd == null)
      return;
    this.onTranslateEnd(this.index);
  }

  public void OnFunctionClick()
  {
    ChatMessageManager.OnChatMessageClick(this._sendMessage.traceData);
  }

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void Release()
  {
    this._displayingIconSymbol = string.Empty;
  }

  public void OnSecond(int serverTime)
  {
    this.FormatTime();
    if (this.index != 8)
      return;
    Debug.Log((object) ("Second:" + (object) this.index + ":" + (object) this.isTranslated + ":" + this._sendMessage.chatText + ":" + this._sendMessage.translateText));
  }

  public void OnSelfMsgClick()
  {
    this.OnBarClick();
  }

  public void OnOtherMsgClick()
  {
    this.OnBarClick();
  }

  [Serializable]
  protected class Panel
  {
    public Transform OthersContainer;
    public UILabel calcText;
    public UILabel Other_userName;
    public UITexture Other_userIcon;
    public UILabel Other_time;
    public UILabel Other_VIP_Level;
    public UITexture Other_Artifact;
    public UILabel Other_msg;
    public UITable Other_Title_Table;
    public Transform Other_tail_icon_container;
    public UISprite Other_tail_icon;
    public UILabel Other_tail_icon_text;
    public UISprite Other_front_icon;
    public UISprite Other_background;
    public GameObject Other_Mute_Admin_Tip;
    public UISprite Other_portraitBG;
    public UIButton BT_translate;
    public UILabel BT_translate_text;
    public Transform SenderContainer;
    public UILabel Sender_userName;
    public UITexture Sender_userIcon;
    public GameObject Sender_Mute_Admin_Tip;
    public UISprite Sender_protraitBG;
    public UILabel Sender_time;
    public UILabel Sender_VIP_Level;
    public UITexture Sender_Artifact;
    public UILabel Sender_msg;
    public UITable Sender_Title_Table;
    public Transform Sender_tail_icon_container;
    public UISprite Sender_tail_icon;
    public UILabel Sender_tail_icon_text;
    public UISprite Sender_front_icon;
    public UISprite Sender_background;
    public Color SenderBackgroundColor;
    public Color OtherBackgroundColoer;
    public Color SystemBackgroundColor;
    public Transform Sender_translateContainer;
  }
}
