﻿// Decompiled with JetBrains decompiler
// Type: QuestDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class QuestDB
{
  private Dictionary<long, QuestData2> _datas = new Dictionary<long, QuestData2>();
  private List<QuestData2> _commonQuests = new List<QuestData2>();
  private List<QuestData2> _groupQuests = new List<QuestData2>();
  public System.Action<long> onDataChanged;
  public System.Action<long> onDataCreated;
  public System.Action<long> onDataUpdated;
  public System.Action<long> onDataRemove;
  public System.Action<long> onDataRemoved;
  private QuestData2 _recommendQuest;

  public long GetMemorySize()
  {
    return DatabaseUtils.GetMemorySize(typeof (QuestData2), (long) this._datas.Count);
  }

  public QuestData2 RecommendQuest
  {
    get
    {
      Dictionary<long, QuestData2>.Enumerator enumerator = this._datas.GetEnumerator();
      this._recommendQuest = (QuestData2) null;
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.Type == 1)
          this._recommendQuest = enumerator.Current.Value;
      }
      return this._recommendQuest;
    }
  }

  public int CompletedCount
  {
    get
    {
      int num = 0;
      Dictionary<long, QuestData2>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.Status == 1 && enumerator.Current.Value.Type < QuestData2.GROUP_QUEST_BEGIN_TYPE)
          ++num;
      }
      return num + DBManager.inst.DB_Local_TimerQuest.GetCompleteCount();
    }
  }

  public List<QuestData2> CommonQuests
  {
    get
    {
      Dictionary<long, QuestData2>.Enumerator enumerator = this._datas.GetEnumerator();
      this._commonQuests.Clear();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.Type == 2)
          this._commonQuests.Add(enumerator.Current.Value);
      }
      return this._commonQuests;
    }
  }

  public List<QuestData2> GroupQuests
  {
    get
    {
      Dictionary<long, QuestData2>.Enumerator enumerator = this._datas.GetEnumerator();
      this._groupQuests.Clear();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.Type > QuestData2.GROUP_QUEST_BEGIN_TYPE && enumerator.Current.Value.Status != 2)
          this._groupQuests.Add(enumerator.Current.Value);
      }
      this._groupQuests.Sort((Comparison<QuestData2>) ((x, y) => int.Parse(x.Info.ID).CompareTo(int.Parse(y.Info.ID))));
      return this._groupQuests;
    }
  }

  public QuestData2 FirstUnCompleteGroupQuest
  {
    get
    {
      List<QuestData2> groupQuests = this.GroupQuests;
      for (int index = 0; index < groupQuests.Count; ++index)
      {
        if (groupQuests[index].Status != 1)
          return groupQuests[index];
      }
      return (QuestData2) null;
    }
  }

  public int CurrentGroupQuestID
  {
    get
    {
      if (this.GroupQuests.Count > 0)
        return this.GroupQuests[0].Type;
      return QuestData2.INVALID_ID;
    }
  }

  public bool IsGroupQuestsComplete()
  {
    bool ret = true;
    this.GroupQuests.ForEach((System.Action<QuestData2>) (obj => ret &= obj.Status == 1));
    return ret;
  }

  public int GroupQuestCompleteCount()
  {
    int ret = 0;
    this.GroupQuests.ForEach((System.Action<QuestData2>) (obj =>
    {
      if (obj.Status != 1)
        return;
      ++ret;
    }));
    return ret;
  }

  public void Publish_onDataChanged(long internalId)
  {
    if (this.onDataChanged == null)
      return;
    this.onDataChanged(internalId);
  }

  public void Publish_onDataCreated(long internalId)
  {
    if (this.onDataCreated != null)
      this.onDataCreated(internalId);
    this.Publish_onDataChanged(internalId);
  }

  public void Publish_OnDataUpdated(long internalId)
  {
    if (this.onDataUpdated != null)
      this.onDataUpdated(internalId);
    this.Publish_onDataChanged(internalId);
  }

  public void Publish_onDataRemove(long internalId)
  {
    if (this.onDataRemove != null)
      this.onDataRemove(internalId);
    this.Publish_onDataChanged(internalId);
  }

  public void Publish_onDataRemoved(long internalId)
  {
    if (this.onDataRemoved == null)
      return;
    this.onDataRemoved(internalId);
  }

  public bool Add(object orgData, long updateTime)
  {
    if (orgData == null)
      return false;
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable == null)
      return false;
    QuestData2 questData2 = new QuestData2();
    if (!questData2.Decode((object) hashtable, updateTime) || questData2.QuestID == (long) QuestData2.INVALID_ID || this._datas.ContainsKey(questData2.QuestID))
      return false;
    this._datas.Add(questData2.QuestID, questData2);
    this.Publish_onDataCreated(questData2.QuestID);
    return true;
  }

  public bool Update(object orgData, long updateTime)
  {
    if (orgData == null)
      return false;
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable == null || !hashtable.ContainsKey((object) "uid") || long.Parse(hashtable[(object) "uid"].ToString()) != PlayerData.inst.uid)
      return false;
    long internalId = QuestData2.GetInternalId((object) hashtable);
    if (internalId == (long) QuestData2.INVALID_ID)
      return false;
    if (!this._datas.ContainsKey(internalId))
      return this.Add(orgData, updateTime);
    if (!this._datas[internalId].Decode((object) hashtable, updateTime))
      return false;
    this.Publish_OnDataUpdated(internalId);
    return true;
  }

  public void UpdateDatas(object orgDatas, long updateTime)
  {
    ArrayList arrayList = orgDatas as ArrayList;
    if (arrayList == null)
      return;
    int index = 0;
    for (int count = arrayList.Count; index < count; ++index)
      this.Update(arrayList[index], updateTime);
  }

  public QuestData2 Get(long key)
  {
    if (this._datas.ContainsKey(key))
      return this._datas[key];
    return (QuestData2) null;
  }

  public void RemoveDatas(object orgDatas, long updateTime)
  {
    ArrayList arrayList = orgDatas as ArrayList;
    if (arrayList == null)
      return;
    int index = 0;
    for (int count = arrayList.Count; index < count; ++index)
      this.Remove(int.Parse((arrayList[index] as Hashtable)[(object) "quest_id"].ToString()), updateTime);
  }

  public bool Remove(int internalId, long updateTime)
  {
    if (!this._datas.ContainsKey((long) internalId) || !this._datas[(long) internalId].CheckUpdateTime(updateTime))
      return false;
    this.Publish_onDataRemove((long) internalId);
    this._datas.Remove((long) internalId);
    this.Publish_onDataRemoved((long) internalId);
    return true;
  }

  public string GetQuestImage(long questID)
  {
    QuestData2 data = this._datas[questID];
    string str = string.Empty;
    string category = data.Category;
    if (category != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (QuestDB.\u003C\u003Ef__switch\u0024mapA5 == null)
      {
        // ISSUE: reference to a compiler-generated field
        QuestDB.\u003C\u003Ef__switch\u0024mapA5 = new Dictionary<string, int>(12)
        {
          {
            "build",
            0
          },
          {
            "train",
            1
          },
          {
            "pve",
            2
          },
          {
            "pvp",
            3
          },
          {
            "research",
            4
          },
          {
            "alliance_join",
            5
          },
          {
            "alliance_help",
            5
          },
          {
            "resource_collect",
            6
          },
          {
            "resource_rate",
            6
          },
          {
            "legend_recruit",
            7
          },
          {
            "legend_skill_up",
            7
          },
          {
            "legend_xp_item",
            7
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (QuestDB.\u003C\u003Ef__switch\u0024mapA5.TryGetValue(category, out num))
      {
        switch (num)
        {
          case 0:
            str = "icon_my_city";
            break;
          case 1:
            str = "icon_troop";
            break;
          case 2:
            str = "icon_event";
            break;
          case 3:
            str = "icon_troop";
            break;
          case 4:
            str = "icon_research";
            break;
          case 5:
            str = "icon_alliance";
            break;
          case 6:
            str = "icon_my_city";
            break;
          case 7:
            str = "icon_my_city";
            break;
        }
      }
    }
    return str;
  }

  public Dictionary<long, QuestData2> Datas
  {
    get
    {
      return this._datas;
    }
  }

  public void ShowInfo()
  {
  }
}
