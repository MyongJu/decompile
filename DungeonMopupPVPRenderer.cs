﻿// Decompiled with JetBrains decompiler
// Type: DungeonMopupPVPRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class DungeonMopupPVPRenderer : MonoBehaviour
{
  public UILabel _winnerName;
  public UILabel _loserName;
  private DragonKnightPVPResultPopup.Parameter _param;

  public void SetData(DragonKnightPVPResultPopup.Parameter param)
  {
    this._param = param;
    if (param.win == 0)
    {
      this._winnerName.text = param.target.userName_Alliance_Name;
      this._loserName.text = param.source.userName_Alliance_Name;
    }
    else
    {
      this._winnerName.text = param.source.userName_Alliance_Name;
      this._loserName.text = param.target.userName_Alliance_Name;
    }
  }

  public void OnView()
  {
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightPVPResultPopup", (Popup.PopupParameter) this._param);
  }
}
