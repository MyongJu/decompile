﻿// Decompiled with JetBrains decompiler
// Type: TradingHallDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TradingHallDlg : UI.Dialog
{
  private int _curPickedTab = -1;
  private bool _curHasGoodsChecked = true;
  private bool _curAvaliableGoodsChecked = true;
  private Dictionary<int, TradingHallCatalog> _catalogItemDict = new Dictionary<int, TradingHallCatalog>();
  private Dictionary<int, TradingHallBuySlot> _buyItemDict = new Dictionary<int, TradingHallBuySlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private const int MAX_BUY_COUNT_SHOW = 10;
  [SerializeField]
  private UIToggle[] _tabCategories;
  [SerializeField]
  private UILabel _labelCurrency;
  [SerializeField]
  private UILabel _labelOverdueGoodsCount;
  [SerializeField]
  private GameObject _leftTabContent;
  [SerializeField]
  private GameObject _rightTabContent;
  [SerializeField]
  private UISprite _hasGoodsSprite;
  [SerializeField]
  private UISprite _hasAvaliableGoodsSprite;
  [SerializeField]
  private GameObject _pickedGoodContent;
  [SerializeField]
  private GameObject _buyGoodsContent;
  [SerializeField]
  private TradingHallBuyGoodView _buyGoodView;
  [SerializeField]
  private TradingHallSellGoodView _sellGoodView;
  [SerializeField]
  private TradingHallShelfGoodView _shelfGoodView;
  [SerializeField]
  private UIScrollView _catalogScrollView;
  [SerializeField]
  private UITable _catalogTable;
  [SerializeField]
  private GameObject _catalogItemPrefab;
  [SerializeField]
  private GameObject _catalogItemRoot;
  [SerializeField]
  private UIScrollView _buyScrollView;
  [SerializeField]
  private UITable _buyTable;
  [SerializeField]
  private GameObject _buyItemPrefab;
  [SerializeField]
  private GameObject _buyItemRoot;
  [SerializeField]
  private GameObject _noGood2Show;
  private bool _tab0Showed;
  private bool _tab1Showed;
  private TradingHallCatalog _curCatalog;
  private TradingHallData.BuyGoodData _curPickedBuyGoodData;
  private UIWrapContentEx _wc;
  private List<ExchangeHallInfo> _curBuyInfoList;

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
    this._curHasGoodsChecked = PlayerPrefsEx.GetIntByUid("exchange_hall_has_goods", 1) == 1;
    this._curAvaliableGoodsChecked = PlayerPrefsEx.GetIntByUid("exchange_hall_has_avaliable_goods", 1) == 1;
    TradingHallPayload.Instance.OnSellDataUpdated -= new System.Action(this.OnSellDataUpdated);
    TradingHallPayload.Instance.OnSellDataUpdated += new System.Action(this.OnSellDataUpdated);
    TradingHallPayload.Instance.OnBuyDataUpdated -= new System.Action(this.OnBuyDataUpdated);
    TradingHallPayload.Instance.OnBuyDataUpdated += new System.Action(this.OnBuyDataUpdated);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    TradingHallDlg.Parameter parameter = orgParam as TradingHallDlg.Parameter;
    if (parameter != null)
      this._curPickedTab = parameter.openTabIndex;
    if (this._curPickedTab < 0)
      this._tabCategories[0].value = true;
    else if (this._curPickedTab >= 0 && this._curPickedTab < this._tabCategories.Length)
      this._tabCategories[this._curPickedTab].value = true;
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.ClearCatalogData();
    this.ClearBuyData();
    if ((UnityEngine.Object) this._buyGoodView != (UnityEngine.Object) null)
      this._buyGoodView.ClearBuyGoodData();
    if ((UnityEngine.Object) this._sellGoodView != (UnityEngine.Object) null)
      this._sellGoodView.ClearSellGoodData();
    if ((UnityEngine.Object) this._shelfGoodView != (UnityEngine.Object) null)
    {
      this._shelfGoodView.ClearShelfGoodData();
      this._shelfGoodView.OnShelfGoodSuccessed -= new System.Action(this.OnShelfGoodSuccessed);
    }
    this._tab0Showed = false;
    this._tab1Showed = false;
    TradingHallPayload.Instance.OnSellDataUpdated -= new System.Action(this.OnSellDataUpdated);
    TradingHallPayload.Instance.OnBuyDataUpdated -= new System.Action(this.OnBuyDataUpdated);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  public void OnBuyGoldPressed()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  public void OnBackBuyGoodsPressed()
  {
    this._curPickedBuyGoodData = (TradingHallData.BuyGoodData) null;
    if (this._curHasGoodsChecked)
      this.ShowBuyContent(true, true);
    else
      this.RequestBuyContent(false, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.ResetBuyContent();
      }));
  }

  public void OnHasGoodsPressed()
  {
    this._curHasGoodsChecked = !this._curHasGoodsChecked;
    this.ShowCheckbox();
    this.ShowBuyContent(false, false);
    PlayerPrefsEx.SetIntByUid("exchange_hall_has_goods", !this._curHasGoodsChecked ? 0 : 1);
    PlayerPrefsEx.Save();
  }

  public void OnHasAvaliableGoodsPressed()
  {
    this._curAvaliableGoodsChecked = !this._curAvaliableGoodsChecked;
    this.ShowCheckbox();
    this.ShowBuyContent(false, false);
    PlayerPrefsEx.SetIntByUid("exchange_hall_has_avaliable_goods", !this._curAvaliableGoodsChecked ? 0 : 1);
    PlayerPrefsEx.Save();
  }

  public void OnTabToggleUpdate()
  {
    int pickedTabIndex = this.GetPickedTabIndex();
    if (pickedTabIndex == this._curPickedTab || pickedTabIndex < 0)
      return;
    this._curPickedTab = pickedTabIndex;
    if (this._curPickedTab <= 0)
      this.ShowTab0Content();
    else
      this.ShowTab1Content();
  }

  public void OnRefreshGoodsPressed()
  {
    this.ShowBuyContent(true, true);
  }

  public void OnRefreshPickedPressed()
  {
    int catalog1Id = -1;
    int catalog2Id = -1;
    int catalog3Id = -1;
    TradingHallPayload.Instance.GetCatalogId(this._curCatalog, ref catalog1Id, ref catalog2Id, ref catalog3Id);
    if (this._curPickedBuyGoodData == null)
      return;
    TradingHallPayload.Instance.GetBuyGoods(catalog1Id, catalog2Id, catalog3Id, true, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      TradingHallData.BuyGoodData buyGoodDataById = TradingHallPayload.Instance.GetBuyGoodDataById(this._curPickedBuyGoodData.ExchangeId);
      if (buyGoodDataById == null || buyGoodDataById.ExchangeId <= 0 || !((UnityEngine.Object) this._buyGoodView != (UnityEngine.Object) null))
        return;
      this._buyGoodView.UpdateUI(buyGoodDataById, this._curCatalog);
    }));
  }

  private void UpdateUI()
  {
    this.ShowCheckbox();
    this.ShowMainContent();
    if (this._curPickedTab <= 0)
      this.ShowTab0Content();
    else
      this.ShowTab1Content();
  }

  private void ShowTab0Content()
  {
    NGUITools.SetActive(this._leftTabContent, true);
    NGUITools.SetActive(this._rightTabContent, false);
    if (!this._tab0Showed)
    {
      this._tab0Showed = true;
      this.GenerateCatalog();
      if (!this._catalogItemDict.ContainsKey(0))
        return;
      this._curCatalog = this._catalogItemDict[0];
      if (!((UnityEngine.Object) this._curCatalog != (UnityEngine.Object) null))
        return;
      this._curCatalog.OnCatalogClicked();
    }
    else if (this._curHasGoodsChecked)
      this.ShowBuyContent(true, true);
    else
      this.RequestBuyContent(true, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.ResetBuyContent();
      }));
  }

  private void ShowTab1Content()
  {
    Utils.ExecuteInSecs(0.05f, (System.Action) (() =>
    {
      NGUITools.SetActive(this._leftTabContent, false);
      NGUITools.SetActive(this._rightTabContent, true);
      if (!this._tab1Showed)
      {
        this._tab1Showed = true;
        this._sellGoodView.UpdateUI(true, true);
        this._shelfGoodView.UpdateUI(true);
        this._shelfGoodView.OnShelfGoodSuccessed -= new System.Action(this.OnShelfGoodSuccessed);
        this._shelfGoodView.OnShelfGoodSuccessed += new System.Action(this.OnShelfGoodSuccessed);
      }
      else
        this._sellGoodView.UpdateUI(true, true);
    }));
  }

  private void OnShelfGoodSuccessed()
  {
    this._sellGoodView.UpdateUI(false, false);
    this._shelfGoodView.UpdateUI(false);
  }

  private void ShowMainContent()
  {
    this._labelCurrency.text = Utils.FormatThousands(PlayerData.inst.userData.currency.gold.ToString());
    this._labelOverdueGoodsCount.text = TradingHallPayload.Instance.GoodOverdueCount.ToString();
    NGUITools.SetActive(this._labelOverdueGoodsCount.gameObject, TradingHallPayload.Instance.GoodOverdueCount > 0);
  }

  private void ShowCheckbox()
  {
    if ((UnityEngine.Object) this._hasGoodsSprite != (UnityEngine.Object) null)
      NGUITools.SetActive(this._hasGoodsSprite.gameObject, this._curHasGoodsChecked);
    if (!((UnityEngine.Object) this._hasAvaliableGoodsSprite != (UnityEngine.Object) null))
      return;
    NGUITools.SetActive(this._hasAvaliableGoodsSprite.gameObject, this._curAvaliableGoodsChecked);
  }

  private void ResetBuyContent()
  {
    if ((UnityEngine.Object) this._buyGoodView != (UnityEngine.Object) null)
      this._buyGoodView.ClearBuyGoodData();
    NGUITools.SetActive(this._pickedGoodContent, this._curPickedBuyGoodData != null);
    NGUITools.SetActive(this._buyGoodsContent, this._curPickedBuyGoodData == null);
    if (this._curPickedBuyGoodData != null)
    {
      TradingHallData.BuyGoodData buyGoodData = TradingHallPayload.Instance.TradingData.BuyGoodDataList.Find((Predicate<TradingHallData.BuyGoodData>) (x => x.ExchangeId == this._curPickedBuyGoodData.ExchangeId));
      if (buyGoodData == null || buyGoodData.ExchangeId <= 0)
        buyGoodData = new TradingHallData.BuyGoodData(this._curPickedBuyGoodData.ExchangeId);
      this._buyGoodView.UpdateUI(buyGoodData, this._curCatalog);
    }
    this.ResetBuyData();
    if ((UnityEngine.Object) this._wc != (UnityEngine.Object) null)
    {
      for (int index = 0; index < this._wc.transform.childCount; ++index)
      {
        Transform child = this._wc.transform.GetChild(index);
        if ((UnityEngine.Object) child != (UnityEngine.Object) null)
        {
          TradingHallBuySlot component = child.GetComponent<TradingHallBuySlot>();
          if ((UnityEngine.Object) component != (UnityEngine.Object) null)
          {
            if (this._curBuyInfoList != null && component.RealIndex < this._curBuyInfoList.Count)
            {
              NGUITools.SetActive(component.gameObject, true);
              this.UpdateSpecialBuyContent(component, component.RealIndex);
            }
            else
              NGUITools.SetActive(component.gameObject, false);
          }
        }
      }
    }
    this.ResetNoBuyHint();
  }

  private void ResetBuyData()
  {
    int catalog1Id = -1;
    int catalog2Id = -1;
    int catalog3Id = -1;
    TradingHallPayload.Instance.GetCatalogId(this._curCatalog, ref catalog1Id, ref catalog2Id, ref catalog3Id);
    this._curBuyInfoList = ConfigManager.inst.DB_ExchangeHall.GetExchangeHallInfoList().FindAll((Predicate<ExchangeHallInfo>) (x =>
    {
      if (catalog1Id > 0 && catalog2Id > 0 && catalog3Id > 0)
      {
        if (x.catalog1stId == catalog1Id && x.catalog2ndId == catalog2Id)
          return x.catalog3rdId == catalog3Id;
        return false;
      }
      if (catalog1Id <= 0 || catalog2Id <= 0)
        return x.catalog1stId == catalog1Id;
      if (x.catalog1stId == catalog1Id)
        return x.catalog2ndId == catalog2Id;
      return false;
    }));
    if (this._curBuyInfoList == null)
      return;
    if (this._curHasGoodsChecked && this._curAvaliableGoodsChecked)
      this._curBuyInfoList = this._curBuyInfoList.FindAll((Predicate<ExchangeHallInfo>) (x => TradingHallPayload.Instance.TradingData.HasThisGoodInBuyAvailable(x.internalId)));
    else if (this._curHasGoodsChecked)
      this._curBuyInfoList = this._curBuyInfoList.FindAll((Predicate<ExchangeHallInfo>) (x => TradingHallPayload.Instance.TradingData.HasThisGood2Buy(x.internalId)));
    else if (this._curAvaliableGoodsChecked)
      this._curBuyInfoList = this._curBuyInfoList.FindAll((Predicate<ExchangeHallInfo>) (x => TradingHallPayload.Instance.TradingData.HasThisGoodAvailable(x.internalId)));
    this.SortBuyContent();
  }

  private void ClearCatalogData()
  {
    using (Dictionary<int, TradingHallCatalog>.Enumerator enumerator = this._catalogItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, TradingHallCatalog> current = enumerator.Current;
        current.Value.OnCurrentCatalogClicked -= new System.Action<TradingHallCatalog>(this.OnCatalogClicked);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._catalogItemDict.Clear();
    this._itemPool.Clear();
  }

  private void RepositionCatalog(bool resetScrollView = true)
  {
    this._catalogTable.repositionNow = true;
    this._catalogTable.Reposition();
    if (!resetScrollView)
      return;
    this._catalogScrollView.ResetPosition();
    this._catalogScrollView.gameObject.SetActive(false);
    this._catalogScrollView.gameObject.SetActive(true);
  }

  private TradingHallCatalog GenerateCatalogSlot(int catalogLevel, int catalogId)
  {
    this._itemPool.Initialize(this._catalogItemPrefab, this._catalogItemRoot);
    GameObject go = this._itemPool.AddChild(this._catalogTable.gameObject);
    NGUITools.SetActive(go, true);
    TradingHallCatalog component = go.GetComponent<TradingHallCatalog>();
    component.SetData(catalogLevel, catalogId);
    component.OnCurrentCatalogClicked += new System.Action<TradingHallCatalog>(this.OnCatalogClicked);
    return component;
  }

  private void GenerateCatalog()
  {
    this.ClearCatalogData();
    List<int> catalog1List = ConfigManager.inst.DB_ExchangeHall.GetCatalog1List();
    if (catalog1List != null)
    {
      for (int key = 0; key < catalog1List.Count; ++key)
      {
        if (catalog1List[key] > 0)
        {
          TradingHallCatalog catalogSlot = this.GenerateCatalogSlot(1, catalog1List[key]);
          this._catalogItemDict.Add(key, catalogSlot);
        }
      }
    }
    this.RepositionCatalog(true);
  }

  private void ClearBuyData()
  {
    if ((UnityEngine.Object) this._wc != (UnityEngine.Object) null)
    {
      this._wc.SortBasedOnScrollMovement();
      UIWrapContentEx wc = this._wc;
      wc.onInitializeItem = wc.onInitializeItem - new UIWrapContent.OnInitializeItem(this.OnInitItem);
    }
    using (Dictionary<int, TradingHallBuySlot>.Enumerator enumerator = this._buyItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, TradingHallBuySlot> current = enumerator.Current;
        current.Value.OnCurrentBuySlotClicked -= new System.Action<TradingHallData.BuyGoodData>(this.OnBuySlotClicked);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._buyItemDict.Clear();
    this._itemPool.Clear();
  }

  private void RepositionBuy(bool forceUpdate = false)
  {
    this._buyTable.repositionNow = true;
    this._buyTable.Reposition();
    if (this._tab0Showed && !forceUpdate)
      return;
    this._buyScrollView.enabled = true;
    this._buyScrollView.ResetPosition();
    this._buyScrollView.restrictWithinPanel = true;
    this._buyScrollView.InvalidateBounds();
  }

  private TradingHallBuySlot GenerateBuySlot(TradingHallData.BuyGoodData buyGoodData, int realIndex)
  {
    this._itemPool.Initialize(this._buyItemPrefab, this._buyItemRoot);
    TradingHallBuySlot component = this._itemPool.AddChild(this._buyTable.gameObject).GetComponent<TradingHallBuySlot>();
    component.OnCurrentBuySlotClicked += new System.Action<TradingHallData.BuyGoodData>(this.OnBuySlotClicked);
    this.UpdateSpecialBuyContent(component, buyGoodData, realIndex);
    return component;
  }

  private void OnCatalogClicked(TradingHallCatalog catalog)
  {
    this.RepositionCatalog(false);
    this.ResetCatalogColor();
    this._curCatalog = catalog;
    this._curPickedBuyGoodData = (TradingHallData.BuyGoodData) null;
    if (!((UnityEngine.Object) catalog != (UnityEngine.Object) null) || catalog.Folded && catalog.CatalogLevel != 1 && catalog.CatalogLevel != 2)
      return;
    this.ShowBuyContent(true, true);
    catalog.SetNameColor();
  }

  private void ResetCatalogColor()
  {
    TradingHallCatalog[] componentsInChildren = this._catalogTable.GetComponentsInChildren<TradingHallCatalog>(true);
    if (componentsInChildren == null)
      return;
    for (int index = 0; index < componentsInChildren.Length; ++index)
      componentsInChildren[index].ResetNameColor();
  }

  private void OnBuySlotClicked(TradingHallData.BuyGoodData buyGoodData)
  {
    if (buyGoodData == null)
      return;
    this.RequestBuyContent(true, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      buyGoodData = TradingHallPayload.Instance.TradingData.BuyGoodDataList.Find((Predicate<TradingHallData.BuyGoodData>) (x => x.ExchangeId == buyGoodData.ExchangeId));
      if (buyGoodData != null && buyGoodData.ItemTotalCount > 0L)
      {
        this._curPickedBuyGoodData = buyGoodData;
        NGUITools.SetActive(this._pickedGoodContent, true);
        NGUITools.SetActive(this._buyGoodsContent, false);
        if (!((UnityEngine.Object) this._buyGoodView != (UnityEngine.Object) null))
          return;
        this._buyGoodView.UpdateUI(buyGoodData, this._curCatalog);
      }
      else
      {
        UIManager.inst.toast.Show(Utils.XLAT("toast_exchange_hall_item_sold_out"), (System.Action) null, 4f, true);
        this.ResetBuyContent();
      }
    }));
  }

  private void ShowBuyContent(bool catalogClicked = false, bool blockScreen = false)
  {
    this.ResetBuyContent();
    this.UpdateBuyContent(catalogClicked, blockScreen);
  }

  private void GenerateBuyContent()
  {
    this.ClearBuyData();
    for (int index = 0; index < 10; ++index)
    {
      if (index < this._curBuyInfoList.Count && this._curBuyInfoList[index].internalId > 0)
      {
        TradingHallBuySlot buySlot = this.GenerateBuySlot(new TradingHallData.BuyGoodData(this._curBuyInfoList[index].internalId), index);
        this.UpdateSpecialBuyContent(buySlot, index);
        this._buyItemDict.Add(index, buySlot);
      }
    }
    this.RepositionBuy(false);
  }

  private void RequestBuyContent(bool blockScreen, System.Action<bool, object> callback)
  {
    int catalog1Id = -1;
    int catalog2Id = -1;
    int catalog3Id = -1;
    TradingHallPayload.Instance.GetCatalogId(this._curCatalog, ref catalog1Id, ref catalog2Id, ref catalog3Id);
    TradingHallPayload.Instance.GetBuyGoods(catalog1Id, catalog2Id, catalog3Id, blockScreen, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }));
  }

  private void UpdateBuyContent(bool catalogClicked, bool blockScreen)
  {
    this.ClearBuyData();
    if (catalogClicked)
      this.RequestBuyContent(blockScreen, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.UpdateBuyContent();
      }));
    else
      this.UpdateBuyContent();
  }

  private void UpdateBuyContent()
  {
    this.ResetBuyData();
    this.GenerateBuyContent();
    if (this._curBuyInfoList != null && this._curBuyInfoList.Count > 10)
    {
      this.UpdateWC(this._curBuyInfoList == null ? 0 : -this._curBuyInfoList.Count + 1, 0);
    }
    else
    {
      this.RemoveWC();
      this.RepositionBuy(true);
    }
    this.ResetNoBuyHint();
  }

  private void ResetNoBuyHint()
  {
    NGUITools.SetActive(this._noGood2Show, this._curBuyInfoList == null || this._curBuyInfoList.Count <= 0);
  }

  private void SortBuyContent()
  {
    if (this._curBuyInfoList == null)
      return;
    this._curBuyInfoList.Sort((Comparison<ExchangeHallInfo>) ((x, y) =>
    {
      if (x.IsEquipment != y.IsEquipment)
        return y.IsEquipment.CompareTo(x.IsEquipment);
      if (x.Quality != y.Quality)
        return y.Quality.CompareTo(x.Quality);
      if (x.Enhanced != y.Enhanced)
        return y.Enhanced.CompareTo(x.Enhanced);
      if (x.RequireLevel != y.RequireLevel)
        return y.RequireLevel.CompareTo(x.RequireLevel);
      return x.Prority.CompareTo(y.Prority);
    }));
  }

  private void UpdateSpecialBuyContent(TradingHallBuySlot buySlot, TradingHallData.BuyGoodData buyGoodData, int realIndex)
  {
    if (!((UnityEngine.Object) buySlot != (UnityEngine.Object) null) || buyGoodData == null || buyGoodData.ExchangeId <= 0)
      return;
    buySlot.SetData(buyGoodData, realIndex);
    GameObject gameObject = buySlot.transform.gameObject;
    if (this._curHasGoodsChecked && this._curAvaliableGoodsChecked)
      NGUITools.SetActive(gameObject, TradingHallPayload.Instance.TradingData.HasThisGoodInBuyAvailable(buyGoodData.ExchangeId));
    else if (this._curHasGoodsChecked)
      NGUITools.SetActive(gameObject, TradingHallPayload.Instance.TradingData.HasThisGood2Buy(buyGoodData.ExchangeId));
    else if (this._curAvaliableGoodsChecked)
      NGUITools.SetActive(gameObject, buyGoodData.ThisGoodAvailable);
    else
      NGUITools.SetActive(gameObject, true);
  }

  private void UpdateSpecialBuyContent(TradingHallBuySlot buySlot, int realIndex)
  {
    if ((UnityEngine.Object) buySlot == (UnityEngine.Object) null || this._curBuyInfoList == null || (realIndex >= this._curBuyInfoList.Count || this._curBuyInfoList[realIndex].internalId <= 0))
      return;
    buySlot.SetData(new TradingHallData.BuyGoodData(this._curBuyInfoList[realIndex].internalId), realIndex);
    TradingHallData.BuyGoodData buyGoodData = TradingHallPayload.Instance.TradingData.BuyGoodDataList.Find((Predicate<TradingHallData.BuyGoodData>) (x => x.ExchangeId == this._curBuyInfoList[realIndex].internalId));
    this.UpdateSpecialBuyContent(buySlot, buyGoodData, realIndex);
  }

  private void RemoveWC()
  {
    if (!(bool) ((UnityEngine.Object) this._buyTable) || !(bool) ((UnityEngine.Object) this._wc))
      return;
    this._wc.RemoveScrollView();
    UnityEngine.Object.Destroy((UnityEngine.Object) this._wc);
  }

  private void UpdateWC(int minIndex, int maxIndex)
  {
    if ((bool) ((UnityEngine.Object) this._buyScrollView))
    {
      this._buyScrollView.restrictWithinPanel = true;
      this._buyScrollView.InvalidateBounds();
    }
    this._wc = this._buyItemRoot.GetComponent<UIWrapContentEx>();
    if ((UnityEngine.Object) this._wc == (UnityEngine.Object) null)
      this._wc = this._buyItemRoot.AddComponent<UIWrapContentEx>();
    if (!((UnityEngine.Object) this._wc != (UnityEngine.Object) null))
      return;
    this._wc.rowNum = 2;
    this._wc.minIndex = minIndex;
    this._wc.maxIndex = maxIndex;
    this._wc.itemWidth = 850;
    this._wc.itemHeight = 340;
    this._wc.cullContent = false;
    UIWrapContentEx wc1 = this._wc;
    wc1.onInitializeItem = wc1.onInitializeItem - new UIWrapContent.OnInitializeItem(this.OnInitItem);
    UIWrapContentEx wc2 = this._wc;
    wc2.onInitializeItem = wc2.onInitializeItem + new UIWrapContent.OnInitializeItem(this.OnInitItem);
    this._wc.SortBasedOnScrollMovement();
    this._wc.WrapContent();
  }

  private void OnInitItem(GameObject obj, int wrapIndex, int realIndex)
  {
    if (realIndex <= -this._wc.minIndex && realIndex > -1)
    {
      NGUITools.SetActive(obj, true);
      this.UpdateSpecialBuyContent(obj.GetComponent<TradingHallBuySlot>(), realIndex);
    }
    else
      NGUITools.SetActive(obj, false);
  }

  private void OnSellDataUpdated()
  {
    this.ShowMainContent();
  }

  private void OnBuyDataUpdated()
  {
    this.ShowMainContent();
  }

  private void OnSecondEvent(int time)
  {
    this.ShowMainContent();
  }

  private int GetPickedTabIndex()
  {
    for (int index = 0; index < this._tabCategories.Length; ++index)
    {
      if (this._tabCategories[index].value)
        return index;
    }
    return -1;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int openTabIndex;
  }
}
