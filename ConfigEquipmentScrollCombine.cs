﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentScrollCombine
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigEquipmentScrollCombine
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, ConfigEquipmentScrollCombineInfo> datas;
  private Dictionary<int, ConfigEquipmentScrollCombineInfo> dicByUniqueId;
  private Dictionary<int, List<int>> combineTarget;

  public void BuildDB(object res)
  {
    this.parse.Parse<ConfigEquipmentScrollCombineInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.combineTarget = new Dictionary<int, List<int>>();
    using (Dictionary<int, ConfigEquipmentScrollCombineInfo>.ValueCollection.Enumerator enumerator = this.dicByUniqueId.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ConfigEquipmentScrollCombineInfo current = enumerator.Current;
        if (!this.combineTarget.ContainsKey(current.scrollChipID))
          this.combineTarget.Add(current.scrollChipID, new List<int>());
        this.combineTarget[current.scrollChipID].Add(current.scrollID);
      }
    }
  }

  public List<int> GetCombineTarget(int scrollChipInternalID)
  {
    List<int> intList = (List<int>) null;
    this.combineTarget.TryGetValue(scrollChipInternalID, out intList);
    return intList;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, ConfigEquipmentScrollCombineInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, ConfigEquipmentScrollCombineInfo>) null;
  }
}
