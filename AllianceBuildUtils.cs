﻿// Decompiled with JetBrains decompiler
// Type: AllianceBuildUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllianceBuildUtils
{
  public static bool CheckIsClearAwway(int buildingConfigId)
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(buildingConfigId);
    if (buildingStaticInfo != null)
    {
      switch (buildingStaticInfo.type)
      {
        case 2:
          AllianceWareHouseData buildingConfigId1 = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseDataByBuildingConfigId(buildingConfigId);
          if (buildingConfigId1 != null)
            return buildingConfigId1.ClearAway != 0;
          return false;
        case 8:
          AllianceHospitalData buildingConfigId2 = DBManager.inst.DB_AllianceHospital.GetMyHospitalDataByBuildingConfigId(buildingConfigId);
          if (buildingConfigId2 != null)
            return buildingConfigId2.ClearAway != 0;
          return false;
      }
    }
    return false;
  }

  public static bool GetMultiTileLocations(int startX, int startY, int xSize, int ySize, int xGridSize, int yGridSize, out List<WorldCoordinate> locations)
  {
    locations = new List<WorldCoordinate>();
    for (int index1 = 0; index1 < xSize; ++index1)
    {
      int x = startX + index1;
      int y = startY + index1;
      if (x >= xGridSize || y >= yGridSize || (x < 0 || y < 0))
        return false;
      locations.Add(new WorldCoordinate(x, y));
      for (int index2 = 1; index2 < ySize; ++index2)
      {
        --x;
        ++y;
        if (x >= xGridSize || y >= yGridSize || (x < 0 || y < 0))
          return false;
        locations.Add(new WorldCoordinate(x, y));
      }
    }
    return locations.Count > 0;
  }

  public static float GetMyAllianceBenefit(int benefitInternalID)
  {
    float num = 0.0f;
    AllianceFortressData allianceFortressData = DBManager.inst.DB_AllianceFortress.Get(PlayerData.inst.playerCityData.in_fortress_id);
    if (allianceFortressData != null)
      allianceFortressData.Benefits.TryGetValue(benefitInternalID, out num);
    return num;
  }

  public static long GetFortressOwner(long fortressId)
  {
    long num1 = (long) int.MaxValue;
    long num2 = 0;
    AllianceFortressData allianceFortressData = DBManager.inst.DB_AllianceFortress.Get(fortressId);
    Dictionary<long, long>.KeyCollection.Enumerator enumerator = allianceFortressData.Troops.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      long troop = allianceFortressData.Troops[enumerator.Current];
      if (num1 > troop)
      {
        num1 = troop;
        num2 = enumerator.Current;
      }
    }
    return num2;
  }

  public static long GetDragonAltarOwner(int fortressId)
  {
    long num1 = (long) int.MaxValue;
    long num2 = 0;
    AllianceTempleData allianceTempleData = DBManager.inst.DB_AllianceTemple.Get((long) fortressId);
    Dictionary<long, long>.KeyCollection.Enumerator enumerator = allianceTempleData.Troops.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      long troop = allianceTempleData.Troops[enumerator.Current];
      if (num1 > troop)
      {
        num1 = troop;
        num2 = enumerator.Current;
      }
    }
    return num2;
  }

  public static List<MarchData> GetMarchsByAllianceTempID(int fortressId)
  {
    List<MarchData> marchDataList = new List<MarchData>();
    Dictionary<long, long>.ValueCollection.Enumerator enumerator = DBManager.inst.DB_AllianceTemple.Get((long) fortressId).Troops.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      MarchData marchData = DBManager.inst.DB_March.Get(enumerator.Current);
      if (marchData != null && marchData.type != MarchData.MarchType.rally_attack)
        marchDataList.Add(marchData);
    }
    return marchDataList;
  }

  public static List<MarchData> GetMarchsByFortressID(long fortressId)
  {
    List<MarchData> marchDataList = new List<MarchData>();
    Dictionary<long, long>.ValueCollection.Enumerator enumerator = DBManager.inst.DB_AllianceFortress.Get(fortressId).Troops.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      MarchData marchData = DBManager.inst.DB_March.Get(enumerator.Current);
      if (marchData != null && marchData.type != MarchData.MarchType.rally_attack)
        marchDataList.Add(marchData);
    }
    return marchDataList;
  }

  public static float GetMyAllianceBenefit(string benefitID)
  {
    if (PlayerData.inst.allianceId <= 0L)
      return 0.0f;
    float num = 0.0f;
    long inFortressId = PlayerData.inst.playerCityData.in_fortress_id;
    int key = 0;
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitID];
    if (dbProperty != null)
      key = dbProperty.InternalID;
    AllianceFortressData allianceFortressData = DBManager.inst.DB_AllianceFortress.Get(inFortressId);
    if (allianceFortressData != null && allianceFortressData.allianceId == PlayerData.inst.allianceId && allianceFortressData.IsComplete)
      allianceFortressData.Benefits.TryGetValue(key, out num);
    return num;
  }

  public static string GetFortressName(long fortressId)
  {
    string str = string.Empty;
    List<AllianceBuildingStaticInfo> totalBuildings = ConfigManager.inst.DB_AllianceBuildings.GetTotalBuildings(0);
    int index = 0;
    if (index < totalBuildings.Count)
      str = totalBuildings[index].LocalName;
    AllianceFortressData allianceFortressData = DBManager.inst.DB_AllianceFortress.Get(fortressId);
    if (!string.IsNullOrEmpty(allianceFortressData.Name))
      str = allianceFortressData.Name;
    return str;
  }

  public static string GetAllianceTempleName(int fortressId)
  {
    string str = string.Empty;
    List<AllianceBuildingStaticInfo> totalBuildings = ConfigManager.inst.DB_AllianceBuildings.GetTotalBuildings(9);
    int index = 0;
    if (index < totalBuildings.Count)
      str = totalBuildings[index].LocalName;
    AllianceTempleData allianceTempleData = DBManager.inst.DB_AllianceTemple.Get((long) fortressId);
    if (!string.IsNullOrEmpty(allianceTempleData.Name))
      str = allianceTempleData.Name;
    return str;
  }

  public static bool InOtherAllianceFortressZone
  {
    get
    {
      long inFortressId = PlayerData.inst.playerCityData.in_fortress_id;
      if (inFortressId == 0L)
        return false;
      AllianceFortressData allianceFortressData = DBManager.inst.DB_AllianceFortress.Get(inFortressId);
      if (allianceFortressData == null)
        return true;
      if (allianceFortressData.allianceId != PlayerData.inst.allianceId)
        return allianceFortressData.IsComplete;
      return false;
    }
  }

  public static double BurningSpeedInSecond
  {
    get
    {
      GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("burning_speed");
      GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("burning_speed_percent");
      double num = data2 == null ? 0.0 : data2.Value;
      return (!AllianceBuildUtils.InOtherAllianceFortressZone ? (double) data1.ValueInt : (double) data1.ValueInt * (1.0 + num)) / 60.0;
    }
  }

  public static bool CheckBuildCanBuild(int buildingConfigId)
  {
    if (!AllianceBuildUtils.CheckBuildIsLock(buildingConfigId))
      return !AllianceBuildUtils.CheckBuildIsPlace(buildingConfigId);
    return false;
  }

  public static bool CheckBuildIsPlace(int buildingConfigId)
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(buildingConfigId);
    if (buildingStaticInfo != null)
    {
      switch (buildingStaticInfo.type)
      {
        case 0:
        case 1:
          return DBManager.inst.DB_AllianceFortress.GetMyFortressDataByBuildingConfigId(buildingConfigId) != null;
        case 2:
          AllianceWareHouseData buildingConfigId1 = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseDataByBuildingConfigId(buildingConfigId);
          if (buildingConfigId1 != null)
            return buildingConfigId1.ClearAway == 0;
          return false;
        case 3:
        case 4:
        case 5:
        case 6:
          return DBManager.inst.DB_AllianceSuperMine.GetSuperMineDataByBuildingConfigId(buildingConfigId) != null;
        case 7:
          return DBManager.inst.DB_AlliancePortal.GetAlliancePortalDataByBuildingConfigId(buildingConfigId) != null;
        case 8:
          AllianceHospitalData buildingConfigId2 = DBManager.inst.DB_AllianceHospital.GetMyHospitalDataByBuildingConfigId(buildingConfigId);
          if (buildingConfigId2 != null)
            return buildingConfigId2.ClearAway == 0;
          return false;
        case 9:
          return DBManager.inst.DB_AllianceTemple.GetAllianceTempleDataByBuildingConfigId(buildingConfigId) != null;
      }
    }
    return false;
  }

  public static bool CheckBuildIsLock(int buildingConfigId)
  {
    return ConfigManager.inst.DB_Alliance_Level.GetData(ConfigManager.inst.DB_AllianceBuildings.Get(buildingConfigId).ReqAllianceLev).Alliance_Level > PlayerData.inst.allianceData.allianceLevel;
  }

  public static bool CheckBuildingIsFreeze(int buildingConfigId)
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(buildingConfigId);
    if (buildingStaticInfo != null)
    {
      switch (buildingStaticInfo.type)
      {
        case 3:
        case 4:
        case 5:
        case 6:
          AllianceSuperMineData buildingConfigId1 = DBManager.inst.DB_AllianceSuperMine.GetSuperMineDataByBuildingConfigId(buildingConfigId);
          if (buildingConfigId1 != null)
            return buildingConfigId1.CurrentState == AllianceSuperMineData.State.FREEZE;
          break;
        case 8:
          AllianceHospitalData buildingConfigId2 = DBManager.inst.DB_AllianceHospital.GetMyHospitalDataByBuildingConfigId(buildingConfigId);
          if (buildingConfigId2 != null)
            return buildingConfigId2.CurrentState == AllianceHospitalData.State.FREEZE;
          break;
      }
    }
    return false;
  }

  internal static List<long> GetSlotData(AllianceFortressData afd)
  {
    List<long> longList = new List<long>();
    using (List<long>.Enumerator enumerator = afd.TroopsSort.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        long current = enumerator.Current;
        if (afd.Troops.ContainsKey(current))
        {
          long troop = afd.Troops[current];
          MarchData marchData = DBManager.inst.DB_March.Get(troop);
          if (marchData != null && marchData.type != MarchData.MarchType.rally_attack)
            longList.Add(troop);
        }
        else
          D.error((object) "can't find troop");
      }
    }
    return longList;
  }

  internal static List<long> GetAllianceBuildingMarchData(TileType type, Coordinate cor)
  {
    List<long> longList1 = new List<long>();
    Dictionary<long, long> dictionary = new Dictionary<long, long>();
    List<long> longList2 = new List<long>();
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(cor);
    switch (type)
    {
      case TileType.AllianceWarehouse:
        dictionary = referenceAt.WareHouseData.Troops;
        longList2 = referenceAt.WareHouseData.TroopsSort;
        break;
      case TileType.AllianceSuperMine:
        dictionary = referenceAt.SuperMineData.Troops;
        longList2 = referenceAt.SuperMineData.TroopsSort;
        break;
      case TileType.AlliancePortal:
        dictionary = referenceAt.PortalData.Troops;
        longList2 = referenceAt.PortalData.TroopsSort;
        break;
      case TileType.AllianceHospital:
        dictionary = referenceAt.HospitalData.Troops;
        longList2 = referenceAt.HospitalData.TroopsSort;
        break;
      case TileType.AllianceTemple:
        dictionary = referenceAt.TempleData.Troops;
        longList2 = referenceAt.TempleData.TroopsSort;
        break;
    }
    using (List<long>.Enumerator enumerator = longList2.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        long current = enumerator.Current;
        if (dictionary.ContainsKey(current))
        {
          long id = dictionary[current];
          MarchData marchData = DBManager.inst.DB_March.Get(id);
          if (marchData != null && marchData.type != MarchData.MarchType.rally_attack)
            longList1.Add(id);
        }
        else
          D.error((object) "can't find troop");
      }
    }
    return longList1;
  }

  internal static string GetAllianceBuildingName(TileType type, Coordinate cor)
  {
    int interalId = 0;
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(cor);
    switch (type)
    {
      case TileType.AllianceWarehouse:
        interalId = referenceAt.WareHouseData.ConfigId;
        break;
      case TileType.AllianceSuperMine:
        interalId = referenceAt.SuperMineData.ConfigId;
        break;
      case TileType.AlliancePortal:
        interalId = referenceAt.PortalData.ConfigId;
        break;
      case TileType.AllianceHospital:
        interalId = referenceAt.HospitalData.ConfigId;
        break;
      case TileType.AllianceTemple:
        interalId = referenceAt.TempleData.ConfigId;
        break;
    }
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(interalId);
    if (buildingStaticInfo == null)
      return (string) null;
    return ScriptLocalization.Get(buildingStaticInfo.BuildName, true);
  }

  internal static long CalcMyAllianceBuildingDurability(int buildingConfigId)
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(buildingConfigId);
    if (buildingStaticInfo == null)
      return 0;
    switch (buildingStaticInfo.type)
    {
      case 0:
      case 1:
        return AllianceBuildUtils.CalcMyFortressDurabilityByConfigId(buildingConfigId);
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
        return AllianceBuildUtils.CalcAllianceBuildingCurrentDurability(buildingConfigId);
      default:
        return 0;
    }
  }

  internal static long CalcAllianceBuildingDurability(int buildingConfigId, long buildingid)
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(buildingConfigId);
    if (buildingStaticInfo == null)
      return 0;
    switch (buildingStaticInfo.type)
    {
      case 0:
      case 1:
        return AllianceBuildUtils.CalcCurrentFortressDurabilityByFortressId(buildingid);
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
        return AllianceBuildUtils.CalcCurrentAllianceBuildingDurabilityByBuildingId(buildingConfigId, buildingid);
      default:
        return 0;
    }
  }

  internal static long CalcAllianceBuildingCurrentDurability(int buildingConfigId)
  {
    long num1 = 0;
    long num2 = 0;
    long num3 = 0;
    int num4 = 0;
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(buildingConfigId);
    switch (buildingStaticInfo.type)
    {
      case 2:
        AllianceWareHouseData buildingConfigId1 = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseDataByBuildingConfigId(buildingConfigId);
        if (buildingConfigId1 == null)
          return (long) buildingStaticInfo.MaxDurability;
        num1 = buildingConfigId1.Durability;
        num2 = buildingConfigId1.TimerEnd;
        num3 = buildingConfigId1.Utime;
        num4 = buildingStaticInfo.MaxDurability - (int) num1;
        break;
      case 3:
      case 4:
      case 5:
      case 6:
        AllianceSuperMineData buildingConfigId2 = DBManager.inst.DB_AllianceSuperMine.GetSuperMineDataByBuildingConfigId(buildingConfigId);
        if (buildingConfigId2 == null)
          return (long) buildingStaticInfo.MaxDurability;
        num1 = buildingConfigId2.Durability;
        num2 = (long) buildingConfigId2.TimerEnd;
        num3 = buildingConfigId2.Utime;
        num4 = buildingStaticInfo.MaxDurability - (int) num1;
        break;
      case 8:
        AllianceHospitalData buildingConfigId3 = DBManager.inst.DB_AllianceHospital.GetMyHospitalDataByBuildingConfigId(buildingConfigId);
        if (buildingConfigId3 == null)
          return (long) buildingStaticInfo.MaxDurability;
        num1 = buildingConfigId3.Durability;
        num2 = buildingConfigId3.TimerEnd;
        num3 = buildingConfigId3.Utime;
        num4 = buildingStaticInfo.MaxDurability - (int) num1;
        break;
      case 9:
        AllianceTempleData buildingConfigId4 = DBManager.inst.DB_AllianceTemple.GetAllianceTempleDataByBuildingConfigId(buildingConfigId);
        if (buildingConfigId4 == null)
          return (long) buildingStaticInfo.MaxDurability;
        num1 = buildingConfigId4.Durability;
        num4 = buildingStaticInfo.MaxDurability - (int) num1;
        if (buildingConfigId4.State == "demolishing")
        {
          Dictionary<long, long>.ValueCollection.Enumerator enumerator = buildingConfigId4.Troops.Values.GetEnumerator();
          while (enumerator.MoveNext())
          {
            if (AllianceBuildUtils.IsValidateMarch(enumerator.Current))
            {
              num2 = (long) DBManager.inst.DB_March.Get(enumerator.Current).endTime;
              break;
            }
          }
          num4 = (int) -buildingConfigId4.Durability;
        }
        else
          num2 = (long) buildingConfigId4.FinishTime;
        num3 = (long) buildingConfigId4.UpdateDurationTime;
        break;
    }
    int num5 = (int) ((long) NetServerTime.inst.ServerTimestamp - num3);
    if ((int) num2 - NetServerTime.inst.ServerTimestamp > 0)
    {
      double num6 = (double) num4 / (double) (num2 - num3);
      num1 += (long) Math.Round((double) num5 * num6);
    }
    if (num1 < 0L)
      num1 = 0L;
    if (num1 > (long) buildingStaticInfo.MaxDurability)
      num1 = (long) buildingStaticInfo.MaxDurability;
    return num1;
  }

  internal static long CalcCurrentAllianceBuildingDurabilityByBuildingId(int buildingConfigId, long buildingid)
  {
    long num1 = 0;
    long num2 = 0;
    long num3 = 0;
    int num4 = 0;
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(buildingConfigId);
    switch (buildingStaticInfo.type)
    {
      case 2:
        AllianceWareHouseData allianceWareHouseData = DBManager.inst.DB_AllianceWarehouse.Get(buildingid);
        if (allianceWareHouseData == null)
          return (long) buildingStaticInfo.MaxDurability;
        num1 = allianceWareHouseData.Durability;
        num2 = allianceWareHouseData.TimerEnd;
        num3 = allianceWareHouseData.Utime;
        num4 = buildingStaticInfo.MaxDurability - (int) num1;
        break;
      case 3:
      case 4:
      case 5:
      case 6:
        AllianceSuperMineData allianceSuperMineData = DBManager.inst.DB_AllianceSuperMine.Get(buildingid);
        if (allianceSuperMineData == null)
          return (long) buildingStaticInfo.MaxDurability;
        num1 = allianceSuperMineData.Durability;
        num2 = (long) allianceSuperMineData.TimerEnd;
        num3 = allianceSuperMineData.Utime;
        num4 = buildingStaticInfo.MaxDurability - (int) num1;
        break;
      case 7:
        AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.Get(buildingid);
        if (alliancePortalData == null)
          return (long) buildingStaticInfo.MaxDurability;
        num1 = alliancePortalData.Durability;
        num2 = (long) alliancePortalData.TimerEnd;
        num3 = alliancePortalData.UTime;
        num4 = buildingStaticInfo.MaxDurability - (int) num1;
        break;
      case 8:
        AllianceHospitalData allianceHospitalData = DBManager.inst.DB_AllianceHospital.Get(buildingid);
        if (allianceHospitalData == null)
          return (long) buildingStaticInfo.MaxDurability;
        num1 = allianceHospitalData.Durability;
        num2 = allianceHospitalData.TimerEnd;
        num3 = allianceHospitalData.Utime;
        num4 = buildingStaticInfo.MaxDurability - (int) num1;
        break;
      case 9:
        AllianceTempleData allianceTempleData = DBManager.inst.DB_AllianceTemple.Get(buildingid);
        if (allianceTempleData == null)
          return (long) buildingStaticInfo.MaxDurability;
        num1 = allianceTempleData.Durability;
        num4 = buildingStaticInfo.MaxDurability - (int) num1;
        if (allianceTempleData.State == "demolishing")
        {
          Dictionary<long, long>.ValueCollection.Enumerator enumerator = allianceTempleData.Troops.Values.GetEnumerator();
          while (enumerator.MoveNext())
          {
            if (AllianceBuildUtils.IsValidateMarch(enumerator.Current))
            {
              num2 = (long) DBManager.inst.DB_March.Get(enumerator.Current).endTime;
              break;
            }
          }
          num4 = (int) -allianceTempleData.Durability;
        }
        else
          num2 = (long) allianceTempleData.FinishTime;
        num3 = (long) allianceTempleData.UpdateDurationTime;
        break;
    }
    int num5 = (int) ((long) NetServerTime.inst.ServerTimestamp - num3);
    if ((int) num2 - NetServerTime.inst.ServerTimestamp > 0)
    {
      double num6 = (double) num4 / (double) (num2 - num3);
      num1 += (long) Math.Round((double) num5 * num6);
    }
    if (num1 < 0L)
      num1 = 0L;
    if (num1 > (long) buildingStaticInfo.MaxDurability)
      num1 = (long) buildingStaticInfo.MaxDurability;
    return num1;
  }

  internal static long CalcMyFortressDurabilityByConfigId(int configId)
  {
    AllianceFortressData buildingConfigId = DBManager.inst.DB_AllianceFortress.GetMyFortressDataByBuildingConfigId(configId);
    if (buildingConfigId != null)
      return AllianceBuildUtils.CalcCurrentFortressDurabilityByFortressData(buildingConfigId);
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(configId);
    if (buildingStaticInfo != null)
      return (long) buildingStaticInfo.MaxDurability;
    return 0;
  }

  internal static long CalcCurrentFortressDurabilityByFortressId(long fortressId)
  {
    return AllianceBuildUtils.CalcCurrentFortressDurabilityByFortressData(DBManager.inst.DB_AllianceFortress.Get(fortressId));
  }

  internal static long CalcCurrentFortressDurabilityByFortressData(AllianceFortressData data)
  {
    if (data == null)
      return 0;
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(data.ConfigId);
    double num1 = (double) data.Durability;
    int num2 = 0;
    int num3 = buildingStaticInfo.MaxDurability - (int) data.Durability;
    if (data.State == "demolishing")
    {
      Dictionary<long, long>.ValueCollection.Enumerator enumerator = data.Troops.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (AllianceBuildUtils.IsValidateMarch(enumerator.Current))
        {
          num2 = DBManager.inst.DB_March.Get(enumerator.Current).timeDuration.endTime;
          break;
        }
      }
      num3 = (int) -data.Durability;
    }
    else
      num2 = data.FinishTime;
    int num4 = NetServerTime.inst.ServerTimestamp - data.UpdateDurationTime;
    if (num2 - NetServerTime.inst.ServerTimestamp > 0)
    {
      double num5 = (double) num3 / (double) (num2 - data.UpdateDurationTime);
      num1 += (double) num4 * num5;
    }
    if (num1 < 0.0)
      num1 = 0.0;
    if (num1 > 0.0 && num1 < 1.0)
      num1 = 1.0;
    if (num1 > (double) buildingStaticInfo.MaxDurability)
      num1 = (double) buildingStaticInfo.MaxDurability;
    return Convert.ToInt64(num1);
  }

  internal static long GetCurrentTroopCount(AllianceFortressData afd)
  {
    long num = 0;
    Dictionary<long, long>.ValueCollection.Enumerator enumerator = afd.Troops.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (AllianceBuildUtils.IsValidateMarch(enumerator.Current))
      {
        MarchData marchData = DBManager.inst.DB_March.Get(enumerator.Current);
        num += (long) marchData.troopsInfo.totalCount;
      }
    }
    return num;
  }

  public static bool IsValidateMarch(long id)
  {
    MarchData marchData = DBManager.inst.DB_March.Get(id);
    return marchData != null && marchData.type != MarchData.MarchType.rally_attack;
  }

  internal static long GetMaxTroopCount(AllianceFortressData afd)
  {
    return afd.MaxTroopsCount;
  }

  internal static void LoadMaxTroopCount(AllianceTempleData afd, System.Action callback)
  {
    RequestManager.inst.SendRequest("Alliance:getAllianceBuildingCapacity", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.hostPlayer.uid
      },
      {
        (object) "alliance_id",
        (object) afd.allianceId
      },
      {
        (object) "building_id",
        (object) afd.templeId
      },
      {
        (object) "building_type",
        (object) 23
      }
    }, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (arg1)
      {
        Hashtable hashtable = arg2 as Hashtable;
        string empty = string.Empty;
        if (hashtable != null && hashtable.ContainsKey((object) "capacity"))
          empty = hashtable[(object) "capacity"].ToString();
        if (afd != null && !string.IsNullOrEmpty(empty))
        {
          long result;
          if (long.TryParse(empty, out result))
            afd.MaxTroopsCount = result;
          if (afd.MaxTroopsCount < 0L)
            afd.MaxTroopsCount = 0L;
        }
      }
      if (callback == null)
        return;
      callback();
    }), true);
  }

  internal static void LoadMaxTroopCount(AllianceFortressData afd, System.Action callback)
  {
    RequestManager.inst.SendRequest("Alliance:getFortressCapacity", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.hostPlayer.uid
      },
      {
        (object) "alliance_id",
        (object) afd.allianceId
      },
      {
        (object) "fortress_id",
        (object) afd.fortressId
      }
    }, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (arg1)
      {
        Hashtable hashtable = arg2 as Hashtable;
        string empty = string.Empty;
        if (hashtable != null && hashtable.ContainsKey((object) "capacity"))
          empty = hashtable[(object) "capacity"].ToString();
        else
          afd.MaxTroopsCount = 0L;
        if (afd != null && !string.IsNullOrEmpty(empty))
        {
          int result;
          if (int.TryParse(empty, out result))
            afd.MaxTroopsCount = (long) result;
          if (afd.MaxTroopsCount < 0L)
            afd.MaxTroopsCount = 0L;
        }
      }
      if (callback == null)
        return;
      callback();
    }), true);
  }

  internal static long GetAllianceBuildingMaxTroopCount(Coordinate cor, TileType tileType)
  {
    long num = 0;
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(cor);
    switch (tileType)
    {
      case TileType.AllianceWarehouse:
        num = referenceAt.WareHouseData.MaxTroopsCount;
        break;
      case TileType.AllianceSuperMine:
        num = referenceAt.SuperMineData.MaxTroopsCount;
        break;
      case TileType.AlliancePortal:
        num = referenceAt.PortalData.MaxTroopsCount;
        break;
      case TileType.AllianceHospital:
        num = referenceAt.HospitalData.MaxTroopsCount;
        break;
      case TileType.AllianceTemple:
        num = referenceAt.TempleData.MaxTroopsCount;
        break;
    }
    return num;
  }

  internal static void LoadAllianceBuildingInfo(Coordinate cor, TileType buildingType, System.Action callback)
  {
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(cor);
    long num1 = 0;
    long num2 = 0;
    switch (buildingType)
    {
      case TileType.AllianceWarehouse:
        num1 = referenceAt.WareHouseData.AllianceId;
        num2 = referenceAt.WareHouseData.WarehouseId;
        break;
      case TileType.AllianceSuperMine:
        num1 = referenceAt.SuperMineData.AllianceId;
        num2 = referenceAt.SuperMineData.SuperMineId;
        break;
      case TileType.AlliancePortal:
        num1 = referenceAt.PortalData.AllianceId;
        num2 = referenceAt.PortalData.PortalId;
        break;
      case TileType.AllianceHospital:
        num1 = referenceAt.HospitalData.AllianceID;
        num2 = referenceAt.HospitalData.BuildingID;
        break;
    }
    RequestManager.inst.SendRequest("Alliance:getAllianceBuildingInfo", new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) num1
      },
      {
        (object) "building_id",
        (object) num2
      },
      {
        (object) "building_type",
        (object) buildingType
      }
    }, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1 || callback == null)
        return;
      callback();
    }), false);
  }

  internal static void LoadAllianceBuildingMaxTroopCount(Coordinate cor, TileType buildingType, System.Action callback)
  {
    TileData tile = PVPMapData.MapData.GetReferenceAt(cor);
    long num1 = 0;
    long num2 = 0;
    switch (buildingType)
    {
      case TileType.AllianceWarehouse:
        num1 = tile.WareHouseData.AllianceId;
        num2 = tile.WareHouseData.WarehouseId;
        break;
      case TileType.AllianceSuperMine:
        num1 = tile.SuperMineData.AllianceId;
        num2 = tile.SuperMineData.SuperMineId;
        break;
      case TileType.AlliancePortal:
        num1 = tile.PortalData.AllianceId;
        num2 = tile.PortalData.PortalId;
        break;
      case TileType.AllianceHospital:
        num1 = tile.HospitalData.AllianceID;
        num2 = tile.HospitalData.BuildingID;
        break;
      case TileType.AllianceTemple:
        num1 = tile.TempleData.allianceId;
        num2 = tile.TempleData.templeId;
        break;
    }
    RequestManager.inst.SendRequest("Alliance:getAllianceBuildingCapacity", new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) num1
      },
      {
        (object) "building_id",
        (object) num2
      },
      {
        (object) "building_type",
        (object) buildingType
      }
    }, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (arg1)
      {
        Hashtable hashtable = arg2 as Hashtable;
        long num3 = 0;
        if (hashtable != null && hashtable.ContainsKey((object) "capacity"))
          num3 = long.Parse(hashtable[(object) "capacity"].ToString());
        if (num3 < 0L)
          num3 = 0L;
        switch (buildingType)
        {
          case TileType.AllianceWarehouse:
            tile.WareHouseData.MaxTroopsCount = num3;
            break;
          case TileType.AllianceSuperMine:
            tile.SuperMineData.MaxTroopsCount = num3;
            break;
          case TileType.AlliancePortal:
            tile.PortalData.MaxTroopsCount = num3;
            break;
          case TileType.AllianceHospital:
            tile.HospitalData.MaxTroopsCount = num3;
            break;
          case TileType.AllianceTemple:
            tile.TempleData.MaxTroopsCount = num3;
            break;
        }
      }
      if (callback == null)
        return;
      callback();
    }), true);
  }

  internal static long GetAllianceBuildingCurrentTroopCount(Coordinate cor, TileType tileType)
  {
    Dictionary<long, long> dictionary = new Dictionary<long, long>();
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(cor);
    switch (tileType)
    {
      case TileType.AllianceWarehouse:
        dictionary = referenceAt.WareHouseData.Troops;
        break;
      case TileType.AllianceSuperMine:
        dictionary = referenceAt.SuperMineData.Troops;
        break;
      case TileType.AlliancePortal:
        dictionary = referenceAt.PortalData.Troops;
        break;
      case TileType.AllianceHospital:
        dictionary = referenceAt.HospitalData.Troops;
        break;
      case TileType.AllianceTemple:
        dictionary = referenceAt.TempleData.Troops;
        break;
    }
    long num = 0;
    Dictionary<long, long>.ValueCollection.Enumerator enumerator = dictionary.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (AllianceBuildUtils.IsValidateMarch(enumerator.Current))
      {
        MarchData marchData = DBManager.inst.DB_March.Get(enumerator.Current);
        num += (long) marchData.troopsInfo.totalCount;
      }
    }
    return num;
  }

  internal static string GetAllianceBuildingStateString(Coordinate cor, TileType tileType)
  {
    int num = 0;
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(cor);
    switch (tileType)
    {
      case TileType.AllianceWarehouse:
        num = (int) referenceAt.WareHouseData.CurrentState;
        break;
      case TileType.AllianceSuperMine:
        num = (int) referenceAt.SuperMineData.CurrentState;
        break;
      case TileType.AlliancePortal:
        num = (int) referenceAt.PortalData.CurrentState;
        break;
      case TileType.AllianceHospital:
        num = (int) referenceAt.HospitalData.CurrentState;
        break;
      case TileType.AllianceTemple:
        return AllianceBuildUtils.GetAllianceTempleStateDescByBuildingId(referenceAt.TempleData.templeId);
    }
    string str = (string) null;
    switch (num)
    {
      case 1:
        str = ScriptLocalization.Get("alliance_fort_status_unfinished", true);
        break;
      case 2:
        str = ScriptLocalization.Get("alliance_fort_player_status_constructing", true);
        break;
      case 3:
        if (tileType == TileType.AllianceSuperMine)
        {
          str = ScriptLocalization.Get("alliance_resource_building_status_gathering", true);
          break;
        }
        break;
    }
    return str;
  }

  internal static int GetBuildingJobLeftTime(AllianceFortressData afd)
  {
    int num1 = 0;
    if (afd.State == "demolishing")
    {
      Dictionary<long, long>.ValueCollection.Enumerator enumerator = afd.Troops.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (AllianceBuildUtils.IsValidateMarch(enumerator.Current))
        {
          num1 = DBManager.inst.DB_March.Get(enumerator.Current).endTime;
          break;
        }
      }
      int num2 = num1 - NetServerTime.inst.ServerTimestamp;
      if (num2 < 0)
        num2 = 0;
      return num2;
    }
    if (afd.FinishTime > 0 && afd.FinishTime > NetServerTime.inst.ServerTimestamp)
      return afd.FinishTime - NetServerTime.inst.ServerTimestamp;
    return 0;
  }

  internal static int GetBuildingJobRemainedTime(object obj)
  {
    if (obj is AllianceFortressData)
      return AllianceBuildUtils.GetBuildingJobLeftTime(obj as AllianceFortressData);
    if (obj is AllianceSuperMineData)
    {
      AllianceSuperMineData allianceSuperMineData = obj as AllianceSuperMineData;
      if (allianceSuperMineData.TimerEnd > 0 && allianceSuperMineData.TimerEnd > NetServerTime.inst.ServerTimestamp)
        return allianceSuperMineData.TimerEnd - NetServerTime.inst.ServerTimestamp;
    }
    if (obj is AllianceWareHouseData)
    {
      AllianceWareHouseData allianceWareHouseData = obj as AllianceWareHouseData;
      if (allianceWareHouseData.TimerEnd > 0L && allianceWareHouseData.TimerEnd > (long) NetServerTime.inst.ServerTimestamp)
        return (int) allianceWareHouseData.TimerEnd - NetServerTime.inst.ServerTimestamp;
    }
    if (obj is AlliancePortalData)
    {
      AlliancePortalData alliancePortalData = obj as AlliancePortalData;
      if (alliancePortalData.TimerEnd > 0 && alliancePortalData.TimerEnd > NetServerTime.inst.ServerTimestamp)
        return alliancePortalData.TimerEnd - NetServerTime.inst.ServerTimestamp;
    }
    if (obj is AllianceHospitalData)
    {
      AllianceHospitalData allianceHospitalData = obj as AllianceHospitalData;
      if (allianceHospitalData.TimerEnd > 0L && allianceHospitalData.TimerEnd > (long) NetServerTime.inst.ServerTimestamp)
        return (int) allianceHospitalData.TimerEnd - NetServerTime.inst.ServerTimestamp;
    }
    if (obj is AllianceTempleData)
    {
      AllianceTempleData allianceTempleData = obj as AllianceTempleData;
      int num1 = 0;
      if (allianceTempleData.State == "demolishing")
      {
        Dictionary<long, long>.ValueCollection.Enumerator enumerator = allianceTempleData.Troops.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (AllianceBuildUtils.IsValidateMarch(enumerator.Current))
          {
            num1 = DBManager.inst.DB_March.Get(enumerator.Current).endTime;
            break;
          }
        }
        int num2 = num1 - NetServerTime.inst.ServerTimestamp;
        if (num2 < 0)
          num2 = 0;
        return num2;
      }
      if (allianceTempleData.FinishTime > 0 && allianceTempleData.FinishTime > NetServerTime.inst.ServerTimestamp)
        return allianceTempleData.FinishTime - NetServerTime.inst.ServerTimestamp;
    }
    return 0;
  }

  internal static int GetBuildingJobRemainedTime(Coordinate cor, TileType tileType)
  {
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(cor);
    object obj = (object) null;
    switch (tileType)
    {
      case TileType.AllianceWarehouse:
        obj = (object) referenceAt.WareHouseData;
        break;
      case TileType.AllianceSuperMine:
        obj = (object) referenceAt.SuperMineData;
        break;
      case TileType.AlliancePortal:
        obj = (object) referenceAt.PortalData;
        break;
      case TileType.AllianceHospital:
        obj = (object) referenceAt.HospitalData;
        break;
      case TileType.AllianceTemple:
        obj = (object) referenceAt.TempleData;
        break;
    }
    return AllianceBuildUtils.GetBuildingJobRemainedTime(obj);
  }

  internal static bool HasRightToDemolish()
  {
    return Utils.IsR5Member();
  }

  public static string GetBuildName(int buildConfigID)
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(buildConfigID);
    switch (buildingStaticInfo.type)
    {
      case 0:
      case 1:
        AllianceFortressData buildingConfigId1 = DBManager.inst.DB_AllianceFortress.GetMyFortressDataByBuildingConfigId(buildConfigID);
        if (buildingConfigId1 != null && !string.IsNullOrEmpty(buildingConfigId1.Name))
          return buildingConfigId1.Name;
        break;
      case 2:
        AllianceWareHouseData buildingConfigId2 = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseDataByBuildingConfigId(buildConfigID);
        if (buildingConfigId2 != null && !string.IsNullOrEmpty(buildingConfigId2.Name))
          return buildingConfigId2.Name;
        break;
      case 3:
      case 4:
      case 5:
      case 6:
        AllianceSuperMineData buildingConfigId3 = DBManager.inst.DB_AllianceSuperMine.GetSuperMineDataByBuildingConfigId(buildConfigID);
        if (buildingConfigId3 != null && !string.IsNullOrEmpty(buildingConfigId3.Name))
          return buildingConfigId3.Name;
        break;
      case 7:
        AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
        if (alliancePortalData != null && !string.IsNullOrEmpty(alliancePortalData.Name))
          return alliancePortalData.Name;
        break;
      case 8:
        AllianceHospitalData buildingConfigId4 = DBManager.inst.DB_AllianceHospital.GetMyHospitalDataByBuildingConfigId(buildConfigID);
        if (buildingConfigId4 != null && !string.IsNullOrEmpty(buildingConfigId4.Name))
          return buildingConfigId4.Name;
        break;
      case 9:
        return buildingStaticInfo.LocalName;
    }
    return buildingStaticInfo.LocalName;
  }

  public static Coordinate GetBuildCoordinate(int allianceBuildingConfigId)
  {
    Coordinate coordinate = new Coordinate(0, 0, 0);
    switch (ConfigManager.inst.DB_AllianceBuildings.Get(allianceBuildingConfigId).type)
    {
      case 0:
      case 1:
        AllianceFortressData buildingConfigId1 = DBManager.inst.DB_AllianceFortress.GetMyFortressDataByBuildingConfigId(allianceBuildingConfigId);
        if (buildingConfigId1 != null)
        {
          coordinate = buildingConfigId1.Location;
          break;
        }
        break;
      case 2:
        AllianceWareHouseData buildingConfigId2 = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseDataByBuildingConfigId(allianceBuildingConfigId);
        if (buildingConfigId2 != null)
        {
          coordinate = buildingConfigId2.Location;
          break;
        }
        break;
      case 3:
      case 4:
      case 5:
      case 6:
        AllianceSuperMineData buildingConfigId3 = DBManager.inst.DB_AllianceSuperMine.GetSuperMineDataByBuildingConfigId(allianceBuildingConfigId);
        if (buildingConfigId3 != null)
        {
          coordinate = buildingConfigId3.Location;
          break;
        }
        break;
      case 7:
        AlliancePortalData buildingConfigId4 = DBManager.inst.DB_AlliancePortal.GetAlliancePortalDataByBuildingConfigId(allianceBuildingConfigId);
        if (buildingConfigId4 != null)
        {
          coordinate = buildingConfigId4.Location;
          break;
        }
        break;
      case 8:
        AllianceHospitalData buildingConfigId5 = DBManager.inst.DB_AllianceHospital.GetMyHospitalDataByBuildingConfigId(allianceBuildingConfigId);
        if (buildingConfigId5 != null)
        {
          coordinate = buildingConfigId5.Location;
          break;
        }
        break;
      case 9:
        AllianceTempleData buildingConfigId6 = DBManager.inst.DB_AllianceTemple.GetAllianceTempleDataByBuildingConfigId(allianceBuildingConfigId);
        if (buildingConfigId6 != null)
        {
          coordinate = buildingConfigId6.Location;
          break;
        }
        break;
    }
    return coordinate;
  }

  public static string GetBuildCoordinatesDesc(int allianceBuildingConfigId)
  {
    return AllianceBuildUtils.GetBuildCoordinate(allianceBuildingConfigId).ToShortString();
  }

  public static string GetBuildDesc(int buildingConfigId)
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(buildingConfigId);
    if (buildingStaticInfo != null)
    {
      switch (buildingStaticInfo.type)
      {
        case 2:
          AllianceWareHouseData buildingConfigId1 = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseDataByBuildingConfigId(buildingConfigId);
          if (buildingConfigId1 != null && (buildingConfigId1.CurrentState == AllianceWareHouseData.State.Complete || buildingConfigId1.CurrentState == AllianceWareHouseData.State.Freeze))
            return string.Format("{0}\r\n{1}", (object) ScriptLocalization.Get("alliance_storehouse_total_resources_stored", true), (object) Utils.FormatThousands(buildingConfigId1.TotalResourceWeightStored.ToString()));
          break;
        case 3:
        case 4:
        case 5:
        case 6:
          return AllianceBuildUtils.GetSuperMineBuildDesc(buildingConfigId);
        case 8:
          return AllianceBuildUtils.GetHospitalBuildDesc(buildingConfigId);
      }
    }
    return (string) null;
  }

  public static string GetBuildStateDesc(int buildingConfigId, ref bool redColor)
  {
    redColor = false;
    string empty = string.Empty;
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(buildingConfigId);
    if (buildingStaticInfo != null)
    {
      if (AllianceBuildUtils.CheckBuildIsLock(buildingConfigId))
      {
        redColor = true;
        return ScriptLocalization.GetWithPara("alliance_fort_status_not_unlocked", new Dictionary<string, string>()
        {
          {
            "0",
            ConfigManager.inst.DB_Alliance_Level.GetData(buildingStaticInfo.ReqAllianceLev).Alliance_Level.ToString()
          }
        }, true);
      }
      switch (buildingStaticInfo.type)
      {
        case 0:
        case 1:
          return AllianceBuildUtils.GetFortressStateDesc(buildingConfigId);
        case 2:
          return AllianceBuildUtils.GetWarehouseStateDesc(buildingConfigId, ref redColor);
        case 3:
        case 4:
        case 5:
        case 6:
          return AllianceBuildUtils.GetSuperMineStateDesc(buildingConfigId);
        case 7:
          return AllianceBuildUtils.GetAlliancePortalStateDesc(buildingConfigId);
        case 8:
          return AllianceBuildUtils.GetHospitalStateDesc(buildingConfigId);
        case 9:
          return AllianceBuildUtils.GetAllianceTempleStateDescByConfigId(buildingConfigId);
      }
    }
    return empty;
  }

  protected static string GetFortressStateDesc(int buildingConfigId)
  {
    AllianceFortressData buildingConfigId1 = DBManager.inst.DB_AllianceFortress.GetMyFortressDataByBuildingConfigId(buildingConfigId);
    ConfigManager.inst.DB_AllianceBuildings.Get(buildingConfigId);
    if (buildingConfigId1 == null)
      return ScriptLocalization.Get("alliance_fort_status_not_built", true);
    string Term = string.Empty;
    string state = buildingConfigId1.State;
    if (state != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceBuildUtils.\u003C\u003Ef__switch\u0024mapF == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceBuildUtils.\u003C\u003Ef__switch\u0024mapF = new Dictionary<string, int>(6)
        {
          {
            "unComplete",
            0
          },
          {
            "building",
            1
          },
          {
            "unDefend",
            2
          },
          {
            "defending",
            3
          },
          {
            "broken",
            4
          },
          {
            "demolishing",
            5
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceBuildUtils.\u003C\u003Ef__switch\u0024mapF.TryGetValue(state, out num))
      {
        switch (num)
        {
          case 0:
            Term = "alliance_fort_status_not_complete";
            break;
          case 1:
            Term = "alliance_fort_player_status_constructing";
            break;
          case 2:
            Term = "alliance_fort_status_not_garrisoned";
            break;
          case 3:
            Term = "alliance_fort_player_status_defending";
            break;
          case 4:
            Term = "alliance_fort_status_damaged";
            break;
          case 5:
            Term = "alliance_fort_status_demolishing";
            break;
        }
      }
    }
    return ScriptLocalization.Get(Term, true);
  }

  protected static string GetWarehouseStateDesc(int buildingConfigId, ref bool redColor)
  {
    redColor = false;
    AllianceWareHouseData buildingConfigId1 = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseDataByBuildingConfigId(buildingConfigId);
    if (buildingConfigId1 == null)
      return ScriptLocalization.Get("alliance_fort_status_not_built", true);
    if (buildingConfigId1.ClearAway != 0)
      return Utils.XLAT("alliance_building_already_closed");
    string Term = string.Empty;
    switch (buildingConfigId1.CurrentState)
    {
      case AllianceWareHouseData.State.UnComplete:
        Term = "alliance_fort_status_unfinished";
        break;
      case AllianceWareHouseData.State.Building:
        Term = "alliance_fort_status_under_construction";
        break;
      case AllianceWareHouseData.State.Complete:
        Term = "alliance_storehouse_status_storing";
        break;
      case AllianceWareHouseData.State.Freeze:
        Term = "alliance_storehouse_status_frozen";
        redColor = true;
        break;
    }
    return ScriptLocalization.Get(Term, true);
  }

  protected static string GetSuperMineStateDesc(int buildingConfigId)
  {
    AllianceSuperMineData buildingConfigId1 = DBManager.inst.DB_AllianceSuperMine.GetSuperMineDataByBuildingConfigId(buildingConfigId);
    if (buildingConfigId1 == null)
      return ScriptLocalization.Get("alliance_fort_status_not_built", true);
    string Term = string.Empty;
    switch (buildingConfigId1.CurrentState)
    {
      case AllianceSuperMineData.State.UNCOMPLETE:
        Term = "alliance_fort_status_unfinished";
        break;
      case AllianceSuperMineData.State.BUILDING:
        Term = "alliance_fort_status_under_construction";
        break;
      case AllianceSuperMineData.State.COMPLETE:
        Term = "alliance_resource_building_status_gathering";
        break;
      case AllianceSuperMineData.State.FREEZE:
        Term = "alliance_storehouse_status_frozen";
        break;
    }
    return ScriptLocalization.Get(Term, true);
  }

  protected static string GetAlliancePortalStateDesc(int buildingConfigId)
  {
    AlliancePortalData buildingConfigId1 = DBManager.inst.DB_AlliancePortal.GetAlliancePortalDataByBuildingConfigId(buildingConfigId);
    if (buildingConfigId1 == null)
      return ScriptLocalization.Get("alliance_fort_status_not_built", true);
    string Term = string.Empty;
    switch (buildingConfigId1.CurrentState)
    {
      case AlliancePortalData.State.UNCOMPLETE:
        Term = "alliance_fort_status_unfinished";
        break;
      case AlliancePortalData.State.BUILDING:
        Term = "alliance_fort_status_under_construction";
        break;
      case AlliancePortalData.State.COMPLETE:
        return AllianceBuildUtils.GetAllianceBossSummonState();
      case AlliancePortalData.State.FREEZE:
        Term = "alliance_storehouse_status_frozen";
        break;
    }
    return ScriptLocalization.Get(Term, true);
  }

  protected static string GetHospitalStateDesc(int buildingConfigId)
  {
    AllianceHospitalData buildingConfigId1 = DBManager.inst.DB_AllianceHospital.GetMyHospitalDataByBuildingConfigId(buildingConfigId);
    if (buildingConfigId1 == null)
      return ScriptLocalization.Get("alliance_fort_status_not_built", true);
    if (buildingConfigId1.ClearAway != 0)
      return Utils.XLAT("alliance_building_already_closed");
    string Term = string.Empty;
    switch (buildingConfigId1.CurrentState)
    {
      case AllianceHospitalData.State.UNCOMPLETE:
        Term = "alliance_fort_status_unfinished";
        break;
      case AllianceHospitalData.State.BUILDING:
        Term = "alliance_fort_status_under_construction";
        break;
      case AllianceHospitalData.State.COMPLETE:
        Term = "alliance_storehouse_status_storing";
        break;
      case AllianceHospitalData.State.FREEZE:
        Term = "alliance_storehouse_status_frozen";
        break;
    }
    return ScriptLocalization.Get(Term, true);
  }

  protected static string GetAllianceTempleStateDescByConfigId(int buildingConfigId)
  {
    AllianceTempleData buildingConfigId1 = DBManager.inst.DB_AllianceTemple.GetAllianceTempleDataByBuildingConfigId(buildingConfigId);
    if (buildingConfigId1 == null)
      return ScriptLocalization.Get("alliance_fort_status_not_built", true);
    string Term = string.Empty;
    string state = buildingConfigId1.State;
    if (state != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceBuildUtils.\u003C\u003Ef__switch\u0024map10 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceBuildUtils.\u003C\u003Ef__switch\u0024map10 = new Dictionary<string, int>(6)
        {
          {
            "unComplete",
            0
          },
          {
            "building",
            1
          },
          {
            "unDefend",
            2
          },
          {
            "defending",
            3
          },
          {
            "broken",
            4
          },
          {
            "demolishing",
            5
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceBuildUtils.\u003C\u003Ef__switch\u0024map10.TryGetValue(state, out num))
      {
        switch (num)
        {
          case 0:
            Term = "alliance_fort_status_not_complete";
            break;
          case 1:
            Term = "alliance_fort_player_status_constructing";
            break;
          case 2:
            Term = "alliance_fort_status_not_garrisoned";
            break;
          case 3:
            Term = "alliance_fort_player_status_defending";
            break;
          case 4:
            Term = "alliance_fort_status_damaged";
            break;
          case 5:
            Term = "alliance_fort_status_demolishing";
            break;
        }
      }
    }
    return ScriptLocalization.Get(Term, true);
  }

  protected static string GetAllianceTempleStateDescByBuildingId(long buildingId)
  {
    AllianceTempleData allianceTempleData = DBManager.inst.DB_AllianceTemple.Get(buildingId);
    if (allianceTempleData == null)
      return ScriptLocalization.Get("alliance_fort_status_not_built", true);
    string Term = string.Empty;
    string state = allianceTempleData.State;
    if (state != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceBuildUtils.\u003C\u003Ef__switch\u0024map11 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceBuildUtils.\u003C\u003Ef__switch\u0024map11 = new Dictionary<string, int>(6)
        {
          {
            "unComplete",
            0
          },
          {
            "building",
            1
          },
          {
            "unDefend",
            2
          },
          {
            "defending",
            3
          },
          {
            "broken",
            4
          },
          {
            "demolishing",
            5
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceBuildUtils.\u003C\u003Ef__switch\u0024map11.TryGetValue(state, out num))
      {
        switch (num)
        {
          case 0:
            Term = "alliance_fort_status_not_complete";
            break;
          case 1:
            Term = "alliance_fort_player_status_constructing";
            break;
          case 2:
            Term = "alliance_fort_status_not_garrisoned";
            break;
          case 3:
            Term = "alliance_fort_player_status_defending";
            break;
          case 4:
            Term = "alliance_fort_status_damaged";
            break;
          case 5:
            Term = "alliance_fort_status_demolishing";
            break;
        }
      }
    }
    return ScriptLocalization.Get(Term, true);
  }

  protected static string GetSuperMineBuildDesc(int buildingConfigId)
  {
    AllianceSuperMineData buildingConfigId1 = DBManager.inst.DB_AllianceSuperMine.GetSuperMineDataByBuildingConfigId(buildingConfigId);
    if (buildingConfigId1 == null)
      return string.Empty;
    long remainingResources = AllianceBuildUtils.GetSuperMineRemainingResources(buildingConfigId);
    switch (buildingConfigId1.CurrentState)
    {
      case AllianceSuperMineData.State.UNCOMPLETE:
      case AllianceSuperMineData.State.BUILDING:
        return string.Empty;
      case AllianceSuperMineData.State.COMPLETE:
        return ScriptLocalization.Get("alliance_resource_building_resources_remaining", true) + "\n" + Utils.FormatThousands(remainingResources.ToString());
      case AllianceSuperMineData.State.FREEZE:
        return ScriptLocalization.Get("alliance_storehouse_status_frozen", true);
      default:
        return string.Empty;
    }
  }

  private static string GetHospitalBuildDesc(int buildingConfigId)
  {
    AllianceHospitalData buildingConfigId1 = DBManager.inst.DB_AllianceHospital.GetMyHospitalDataByBuildingConfigId(buildingConfigId);
    if (buildingConfigId1 == null)
      return string.Empty;
    switch (buildingConfigId1.CurrentState)
    {
      case AllianceHospitalData.State.UNCOMPLETE:
      case AllianceHospitalData.State.BUILDING:
        return string.Empty;
      case AllianceHospitalData.State.COMPLETE:
        return string.Format("{0}\r\n{1}", (object) ScriptLocalization.Get("alliance_hospital_wounded_troops", true), (object) Utils.FormatThousands(buildingConfigId1.GetInjuredTroopCount().ToString()));
      case AllianceHospitalData.State.FREEZE:
        return ScriptLocalization.Get("alliance_storehouse_status_frozen", true);
      default:
        return string.Empty;
    }
  }

  public static bool HasEnemyTroop(MarchData marchData, AllianceFortressData afd)
  {
    return afd != null && afd.Troops != null && (afd.Troops.Count != 0 && afd.allianceId != marchData.ownerAllianceId) && !(afd.State == "demolishing");
  }

  internal static bool HasMyTroop(AllianceFortressData afd)
  {
    Dictionary<long, long>.KeyCollection.Enumerator enumerator = afd.Troops.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current == PlayerData.inst.hostPlayer.uid)
        return true;
    }
    return false;
  }

  internal static long GetMyTroopId(AllianceFortressData afd)
  {
    Dictionary<long, long>.KeyCollection.Enumerator enumerator = afd.Troops.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current == PlayerData.inst.hostPlayer.uid)
        return afd.Troops[enumerator.Current];
    }
    return 0;
  }

  internal static bool IsFullDurability(object obj)
  {
    return obj is AllianceFortressData && AllianceBuildUtils.GetFullDurability((obj as AllianceFortressData).ConfigId) == (obj as AllianceFortressData).Durability || obj is AllianceTempleData && AllianceBuildUtils.GetFullDurability((obj as AllianceTempleData).ConfigId) == (obj as AllianceTempleData).Durability;
  }

  internal static long GetFullDurability(int allianceBuildingId)
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(allianceBuildingId);
    if (buildingStaticInfo == null)
      return 0;
    return (long) buildingStaticInfo.MaxDurability;
  }

  internal static bool HasTroop(AllianceFortressData afd)
  {
    return afd.Troops.Count != 0;
  }

  internal static long GetSuperMineRemainingResources(int allianceBuildingId)
  {
    AllianceSuperMineData buildingConfigId = DBManager.inst.DB_AllianceSuperMine.GetSuperMineDataByBuildingConfigId(allianceBuildingId);
    if (buildingConfigId == null)
      return 0;
    long num = buildingConfigId.Amount - (long) ((double) buildingConfigId.GatherRate * (double) ((long) NetServerTime.inst.ServerTimestamp - buildingConfigId.Mtime));
    return num < 0L ? 0L : num;
  }

  internal static int GetTroopLoad(int allianceBuildingId, long marchId)
  {
    MarchData marchData = DBManager.inst.DB_March.Get(marchId);
    if (marchData == null)
      return 0;
    int troopLoad = marchData.CalculateTroopLoad();
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(allianceBuildingId);
    if (buildingStaticInfo != null)
    {
      string resourceType = buildingStaticInfo.ResourceType;
      if (resourceType != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (AllianceBuildUtils.\u003C\u003Ef__switch\u0024map12 == null)
        {
          // ISSUE: reference to a compiler-generated field
          AllianceBuildUtils.\u003C\u003Ef__switch\u0024map12 = new Dictionary<string, int>(4)
          {
            {
              "food",
              0
            },
            {
              "wood",
              1
            },
            {
              "ore",
              2
            },
            {
              "silver",
              3
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (AllianceBuildUtils.\u003C\u003Ef__switch\u0024map12.TryGetValue(resourceType, out num))
        {
          switch (num)
          {
            case 0:
              troopLoad /= ConfigManager.inst.DB_GameConfig.GetData("food_load").ValueInt;
              break;
            case 1:
              troopLoad /= ConfigManager.inst.DB_GameConfig.GetData("wood_load").ValueInt;
              break;
            case 2:
              troopLoad /= ConfigManager.inst.DB_GameConfig.GetData("ore_load").ValueInt;
              break;
            case 3:
              troopLoad /= ConfigManager.inst.DB_GameConfig.GetData("silver_load").ValueInt;
              break;
          }
        }
      }
    }
    return (int) Mathf.Min((float) troopLoad, (float) buildingStaticInfo.AmountStored);
  }

  internal static long GetSuperMineGatheredResources(int allianceBuildingId)
  {
    AllianceSuperMineData buildingConfigId = DBManager.inst.DB_AllianceSuperMine.GetSuperMineDataByBuildingConfigId(allianceBuildingId);
    if (buildingConfigId == null)
      return 0;
    MarchData marchData = DBManager.inst.DB_March.Get(buildingConfigId.CurrentTroopId);
    if (marchData == null)
      return 0;
    int troopLoad = AllianceBuildUtils.GetTroopLoad(allianceBuildingId, buildingConfigId.CurrentTroopId);
    return (long) ((double) (NetServerTime.inst.ServerTimestamp - marchData.startTime) / (double) marchData.timeDuration.duration * (double) troopLoad);
  }

  internal static long GetSuperMineTotalGatheredResources(int allianceBuildingId)
  {
    AllianceSuperMineData buildingConfigId = DBManager.inst.DB_AllianceSuperMine.GetSuperMineDataByBuildingConfigId(allianceBuildingId);
    long num = 0;
    if (buildingConfigId != null)
    {
      using (Dictionary<long, long>.Enumerator enumerator = buildingConfigId.Troops.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, long> current = enumerator.Current;
          MarchData marchData = DBManager.inst.DB_March.Get(current.Value);
          if (marchData != null)
          {
            int troopLoad = AllianceBuildUtils.GetTroopLoad(allianceBuildingId, current.Value);
            num += (long) ((double) (NetServerTime.inst.ServerTimestamp - marchData.startTime) / (double) marchData.timeDuration.duration * (double) troopLoad);
          }
        }
      }
    }
    return num;
  }

  internal static long GetSuperMineTotalGatheredResources(AllianceSuperMineData asmd)
  {
    long num = 0;
    if (asmd != null)
    {
      using (Dictionary<long, long>.Enumerator enumerator = asmd.Troops.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, long> current = enumerator.Current;
          MarchData marchData = DBManager.inst.DB_March.Get(current.Value);
          if (marchData != null)
          {
            int troopLoad = AllianceBuildUtils.GetTroopLoad(asmd.ConfigId, current.Value);
            num += (long) ((double) (NetServerTime.inst.ServerTimestamp - marchData.startTime) / (double) marchData.timeDuration.duration * (double) troopLoad);
          }
        }
      }
    }
    return num;
  }

  internal static string GetSuperMineGatheringSpeed(int allianceBuildingId, Coordinate cor)
  {
    AllianceSuperMineData buildingConfigId = DBManager.inst.DB_AllianceSuperMine.GetSuperMineDataByBuildingConfigId(allianceBuildingId);
    if (buildingConfigId == null)
      return "0/h";
    MarchData marchData = DBManager.inst.DB_March.Get(buildingConfigId.CurrentTroopId);
    if (marchData == null)
      return "0/h";
    int num1 = AllianceBuildUtils.GetTroopLoad(allianceBuildingId, buildingConfigId.CurrentTroopId);
    if ((long) num1 > buildingConfigId.Amount)
      num1 = (int) buildingConfigId.Amount;
    float mineBaseGatherRate = AllianceBuildUtils.GetSuperMineBaseGatherRate(allianceBuildingId);
    float num2 = marchData.timeDuration.oneOverTimeDuration * (float) num1;
    if (marchData.battleLog != null && marchData.battleLog.ContainsKey((object) "gather_rate"))
      num2 = float.Parse(marchData.battleLog[(object) "gather_rate"].ToString());
    if ((double) num2 == 0.0 || (double) num2 < (double) mineBaseGatherRate)
      num2 = mineBaseGatherRate;
    return string.Format("{0}/h", (object) Utils.FormatThousands(((long) ((double) mineBaseGatherRate * 3600.0)).ToString())) + string.Format("[43E024]+ {0}/h[-]", (object) Utils.FormatThousands(((int) (((double) num2 - (double) mineBaseGatherRate) * 3600.0 + 0.5)).ToString()));
  }

  internal static float GetSuperMineBaseGatherRate(int allianceBuildingId)
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(allianceBuildingId);
    if (buildingStaticInfo != null)
      return ConfigManager.inst.DB_BenefitCalc.GetFinalData(buildingStaticInfo.GatherRate, "calc_alliance_resource_gather_speed");
    return 0.0f;
  }

  internal static bool isAllianceBossSummoned()
  {
    AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
    return DBManager.inst.DB_AllianceBoss.GetAllianceBossData() != null || alliancePortalData != null && alliancePortalData.BossId > 0L;
  }

  internal static AlliancePortalData.BossSummonState GetAlliancePortalBossSummonState()
  {
    AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
    int valueInt1 = ConfigManager.inst.DB_GameConfig.GetData("alliance_boss_donate_energy_max").ValueInt;
    if (AllianceBuildUtils.isAllianceBossSummoned())
    {
      AllianceBossData allianceBossData = DBManager.inst.DB_AllianceBoss.GetAllianceBossData();
      if (allianceBossData != null)
      {
        int valueInt2 = ConfigManager.inst.DB_GameConfig.GetData("alliance_boss_duration").ValueInt;
        if ((int) (allianceBossData.CTime + (long) (valueInt2 * 60) - (long) NetServerTime.inst.ServerTimestamp) > 0)
          return AlliancePortalData.BossSummonState.ACTIVATED_TO_RALLY;
      }
      return AlliancePortalData.BossSummonState.ACTIVATED;
    }
    if (alliancePortalData == null)
      return AlliancePortalData.BossSummonState.INVALID;
    if (NetServerTime.inst.ServerTimestamp - (int) alliancePortalData.CDTime < 0)
      return AlliancePortalData.BossSummonState.COOLING_DOWN;
    return alliancePortalData.CurrentEnergy >= (long) valueInt1 ? AlliancePortalData.BossSummonState.CAN_ACTIVATE : AlliancePortalData.BossSummonState.COOLING_DOWN_OVER;
  }

  internal static string GetAllianceBossSummonState()
  {
    AlliancePortalData.BossSummonState portalBossSummonState = AllianceBuildUtils.GetAlliancePortalBossSummonState();
    string source = string.Empty;
    switch (portalBossSummonState)
    {
      case AlliancePortalData.BossSummonState.COOLING_DOWN:
        source = "alliance_portal_status_cooldown";
        break;
      case AlliancePortalData.BossSummonState.COOLING_DOWN_OVER:
        source = "alliance_portal_status_cooldown_finished";
        break;
      case AlliancePortalData.BossSummonState.CAN_ACTIVATE:
        source = "alliance_portal_status_ready";
        break;
      case AlliancePortalData.BossSummonState.ACTIVATED:
      case AlliancePortalData.BossSummonState.ACTIVATED_TO_RALLY:
        source = "alliance_portal_status_opened";
        break;
    }
    return Utils.XLAT(source);
  }

  internal static string GetAllianceBossSummonDesc()
  {
    AllianceBossData allianceBossData = DBManager.inst.DB_AllianceBoss.GetAllianceBossData();
    AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
    AlliancePortalData.BossSummonState portalBossSummonState = AllianceBuildUtils.GetAlliancePortalBossSummonState();
    string source = string.Empty;
    int time = 0;
    switch (portalBossSummonState)
    {
      case AlliancePortalData.BossSummonState.COOLING_DOWN:
        if (alliancePortalData != null)
          time = (int) Math.Abs((long) NetServerTime.inst.ServerTimestamp - alliancePortalData.CDTime);
        source = "alliance_portal_cooldown_time";
        break;
      case AlliancePortalData.BossSummonState.COOLING_DOWN_OVER:
        source = "alliance_portal_dust_tip";
        break;
      case AlliancePortalData.BossSummonState.CAN_ACTIVATE:
        source = "alliance_portal_ready_tip";
        break;
      case AlliancePortalData.BossSummonState.ACTIVATED:
      case AlliancePortalData.BossSummonState.ACTIVATED_TO_RALLY:
        if (allianceBossData != null)
        {
          int valueInt = ConfigManager.inst.DB_GameConfig.GetData("alliance_boss_duration").ValueInt;
          time = (int) (allianceBossData.CTime + (long) (valueInt * 60) - (long) NetServerTime.inst.ServerTimestamp);
        }
        source = "alliance_portal_close_time";
        break;
    }
    return string.Format(Utils.XLAT(source), (object) Utils.FormatTime(time, true, false, true));
  }
}
