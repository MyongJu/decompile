﻿// Decompiled with JetBrains decompiler
// Type: ChatMessageManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Chat;
using DB;
using System.Collections;
using UI;

public class ChatMessageManager
{
  public const int START_RALLY_ID = 0;
  public const int WAR_REPORT_ID = 0;
  public const int WAR_REPORT_UID = 1;
  public const int BOOKMARK_K = 0;
  public const int BOOKMARK_X = 1;
  public const int BOOKMARK_Y = 2;
  public const int EQUIPMENT_LEVEL = 0;
  public const int EQUIPMENT_ID = 1;
  private static ChatManager _chatMng;

  private static ChatManager _chatManager
  {
    get
    {
      if (ChatMessageManager._chatMng == null && (UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null)
        ChatMessageManager._chatMng = GameEngine.Instance.ChatManager;
      return ChatMessageManager._chatMng;
    }
  }

  private static bool SendSystemMessage(ChatTraceData.TraceType type, long channelId, string msg, string locKey, Hashtable locParam, string[] param, bool isSpeaker = false)
  {
    ChatMessage chatMessage = new ChatMessage();
    chatMessage.traceData.type = type;
    chatMessage.SetDefaultInfo();
    chatMessage.chatText = msg;
    chatMessage.localizationKey = locKey;
    chatMessage.localizationParam = locParam;
    chatMessage.traceData.isSpeaker = isSpeaker;
    chatMessage.traceData.InsertData(param);
    if ((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null && GameEngine.Instance.ChatManager != null && channelId != 0L)
    {
      if (MsgService.Instance.RTMFilter)
      {
        MsgService.Instance.SendGroupMessage(channelId, chatMessage, false);
        GameEngine.Instance.ChatManager.SendSelfMessage(channelId, chatMessage);
      }
      else
        GameEngine.Instance.ChatManager.SendGroupMessage(channelId, chatMessage, true, true);
    }
    return true;
  }

  public static bool OnChatMessageClick(ChatTraceData data)
  {
    switch (data.type)
    {
      case ChatTraceData.TraceType.WarReport_Attack:
      case ChatTraceData.TraceType.WarReport_BeAttack:
      case ChatTraceData.TraceType.WarReport_Rally:
      case ChatTraceData.TraceType.WarReport_BeRally:
        return ChatMessageManager.OnWarReportOpen(data);
      case ChatTraceData.TraceType.WarReport_Scout:
        return ChatMessageManager.OnScoutReportOpen(data);
      case ChatTraceData.TraceType.Forge:
      case ChatTraceData.TraceType.EquimentEnhance:
      case ChatTraceData.TraceType.ForgeDK:
      case ChatTraceData.TraceType.EquimentEnhanceDK:
        return ChatMessageManager.OnForgeDlgOpen(data);
      case ChatTraceData.TraceType.CasinoCard:
        return ChatMessageManager.OnCasinoCardHistoryPopupOpen(data);
      case ChatTraceData.TraceType.Rally:
        return ChatMessageManager.OnRallyDlgOpen(data);
      case ChatTraceData.TraceType.Bookmark:
        return ChatMessageManager.OnBookmarkClick(data);
      case ChatTraceData.TraceType.MagicReport_Attack:
      case ChatTraceData.TraceType.MagicReport_BeAttack:
        return ChatMessageManager.OnMagicReportOpen(data);
      default:
        return false;
    }
  }

  public static bool SendStartRallyMessage(long rallyId, Coordinate target)
  {
    if (!((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null) || GameEngine.Instance.ChatManager == null || GameEngine.Instance.ChatManager.allianceChannel == null)
      return false;
    Hashtable locParam = new Hashtable();
    locParam[(object) "x,y"] = (object) string.Format("{0},{1}", (object) target.X, (object) target.Y);
    string[] strArray = new string[1]{ rallyId.ToString() };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.Rally, GameEngine.Instance.ChatManager.allianceChannelId, strArray[0].ToLower(), "chat_alliance_rally_started_description", locParam, strArray, false);
  }

  public static bool SendStartGveRallyMessage(long rallyId, string replace, Coordinate target)
  {
    WorldBossData worldBossData = DBManager.inst.DB_WorldBossDB.Get(DBManager.inst.DB_Rally.Get(rallyId).bossId);
    string locKey = "chat_alliance_gve_rally_started_description";
    if (worldBossData != null)
      locKey = "chat_alliance_gve_rally_world_boss_started_description";
    if (!((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null) || GameEngine.Instance.ChatManager == null || GameEngine.Instance.ChatManager.allianceChannel == null)
      return false;
    Hashtable locParam = new Hashtable();
    locParam[(object) "0"] = (object) replace;
    string[] strArray = new string[1]{ rallyId.ToString() };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.Rally, GameEngine.Instance.ChatManager.allianceChannelId, strArray[0].ToLower(), locKey, locParam, strArray, false);
  }

  public static bool SendTradeMessage(string res)
  {
    if (!((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null) || GameEngine.Instance.ChatManager == null || GameEngine.Instance.ChatManager.allianceChannel == null)
      return false;
    Hashtable locParam = new Hashtable();
    locParam[(object) nameof (res)] = (object) res;
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.Trade, GameEngine.Instance.ChatManager.allianceChannelId, res, "chat_alliance_resource_request_description", locParam, (string[]) null, false);
  }

  public static bool SendKnightStart(int startTime)
  {
    if (!((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null) || GameEngine.Instance.ChatManager == null || GameEngine.Instance.ChatManager.allianceChannel == null)
      return false;
    Hashtable locParam = new Hashtable();
    locParam.Add((object) "0", (object) startTime);
    string[] strArray = new string[2];
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.Knight_Start, GameEngine.Instance.ChatManager.allianceChannelId, string.Empty, "chat_alliance_fallen_knight_event_started_description", locParam, strArray, false);
  }

  public static bool SendCasinoCardView(int shareTime, long uid, long shareId, bool isKingdomChannel = false)
  {
    if ((!((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null) || GameEngine.Instance.ChatManager == null || GameEngine.Instance.ChatManager.allianceChannel == null) && !isKingdomChannel)
      return false;
    Hashtable locParam = new Hashtable();
    locParam.Add((object) "0", (object) shareTime);
    string[] strArray = new string[2]
    {
      uid.ToString(),
      shareId.ToString()
    };
    long channelId = !isKingdomChannel ? GameEngine.Instance.ChatManager.allianceChannelId : GameEngine.Instance.ChatManager.kingdomChannelId;
    ToastManager.Instance.AddBasicToast("toast_tavern_rewards_share_success");
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.CasinoCard, channelId, strArray[0].ToLower(), "chat_tavern_wheel_reward_description", locParam, strArray, false);
  }

  public static bool SendWarReport_Attack(long warReportId, long warReportOwnerUid, string targetName, bool isWin)
  {
    if (!((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null) || GameEngine.Instance.ChatManager == null || GameEngine.Instance.ChatManager.allianceChannel == null)
      return false;
    Hashtable locParam = new Hashtable();
    locParam[(object) "target_name"] = (object) targetName;
    locParam[(object) "result"] = !isWin ? (object) "result_lost" : (object) "result_won";
    string[] strArray = new string[2]
    {
      warReportId.ToString(),
      warReportOwnerUid.ToString()
    };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.WarReport_Attack, GameEngine.Instance.ChatManager.allianceChannelId, strArray[0].ToLower() + ":" + strArray[1].ToString(), "chat_alliance_battle_report_attack_description", locParam, strArray, false);
  }

  public static bool SendWarReport_BeAttack(long warReportId, long warReportOwnerUid, string targetName, bool isWin)
  {
    if (!((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null) || GameEngine.Instance.ChatManager == null || GameEngine.Instance.ChatManager.allianceChannel == null)
      return false;
    Hashtable locParam = new Hashtable();
    locParam[(object) "target_name"] = (object) targetName;
    locParam[(object) "result"] = !isWin ? (object) "result_lost" : (object) "result_won";
    string[] strArray = new string[2]
    {
      warReportId.ToString(),
      warReportOwnerUid.ToString()
    };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.WarReport_BeAttack, GameEngine.Instance.ChatManager.allianceChannelId, strArray[0].ToLower() + ":" + strArray[1].ToString(), "chat_alliance_battle_report_defense_description", locParam, strArray, false);
  }

  public static bool SendWarReport_Rally(long warReportId, long warReportOwnerUid, string targetName, bool isWin)
  {
    if (!((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null) || GameEngine.Instance.ChatManager == null || GameEngine.Instance.ChatManager.allianceChannel == null)
      return false;
    Hashtable locParam = new Hashtable();
    locParam[(object) "target_name"] = (object) targetName;
    locParam[(object) "result"] = !isWin ? (object) "result_lost" : (object) "result_won";
    string[] strArray = new string[2]
    {
      warReportId.ToString(),
      warReportOwnerUid.ToString()
    };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.WarReport_Rally, GameEngine.Instance.ChatManager.allianceChannelId, strArray[0].ToLower() + ":" + strArray[1].ToString(), "chat_alliance_battle_report_rally_attack_description", locParam, strArray, false);
  }

  public static bool SendWarReport_BeRally(long warReportId, long warReportOwnerUid, string targetName, bool isWin)
  {
    if (!((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null) || GameEngine.Instance.ChatManager == null || GameEngine.Instance.ChatManager.allianceChannel == null)
      return false;
    Hashtable locParam = new Hashtable();
    locParam[(object) "target_name"] = (object) targetName;
    locParam[(object) "result"] = !isWin ? (object) "result_lost" : (object) "result_won";
    string[] strArray = new string[2]
    {
      warReportId.ToString(),
      warReportOwnerUid.ToString()
    };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.WarReport_BeRally, GameEngine.Instance.ChatManager.allianceChannelId, strArray[0].ToLower() + ":" + strArray[1].ToString(), "chat_alliance_battle_report_rally_defense_description", locParam, strArray, false);
  }

  public static bool SendMagicReport_Attack(long warReportId, long warReportOwnerUid, string magicNameLocalKey)
  {
    if (!((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null) || GameEngine.Instance.ChatManager == null || GameEngine.Instance.ChatManager.allianceChannel == null)
      return false;
    Hashtable locParam = new Hashtable();
    locParam[(object) "magic_name"] = (object) magicNameLocalKey;
    string[] strArray = new string[2]
    {
      warReportId.ToString(),
      warReportOwnerUid.ToString()
    };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.MagicReport_Attack, GameEngine.Instance.ChatManager.allianceChannelId, strArray[0].ToLower() + ":" + strArray[1].ToString(), "chat_share_magic_report_attack_description", locParam, strArray, false);
  }

  public static bool SendMagicReport_BeAttack(long warReportId, long warReportOwnerUid, string magicNameLocalKey)
  {
    if (!((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null) || GameEngine.Instance.ChatManager == null || GameEngine.Instance.ChatManager.allianceChannel == null)
      return false;
    Hashtable locParam = new Hashtable();
    locParam[(object) "magic_name"] = (object) magicNameLocalKey;
    string[] strArray = new string[2]
    {
      warReportId.ToString(),
      warReportOwnerUid.ToString()
    };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.MagicReport_BeAttack, GameEngine.Instance.ChatManager.allianceChannelId, strArray[0].ToLower() + ":" + strArray[1].ToString(), "chat_share_magic_report_defense_description", locParam, strArray, false);
  }

  public static bool SendWarReport_Scout(long warReportId, long warReportOwnerUid, string targetName)
  {
    if (!((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null) || GameEngine.Instance.ChatManager == null || GameEngine.Instance.ChatManager.allianceChannel == null)
      return false;
    Hashtable locParam = new Hashtable();
    locParam[(object) "target_name"] = (object) targetName;
    string[] strArray = new string[2]
    {
      warReportId.ToString(),
      warReportOwnerUid.ToString()
    };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.WarReport_Scout, GameEngine.Instance.ChatManager.allianceChannelId, strArray[0].ToLower() + ":" + strArray[1].ToString(), "chat_alliance_battle_report_scout_description", locParam, strArray, false);
  }

  public static bool SendDragonAltarAttackReport(long warReportId, long warReportOwnerUid, string targetName)
  {
    if (!((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null) || GameEngine.Instance.ChatManager == null || GameEngine.Instance.ChatManager.allianceChannel == null)
      return false;
    Hashtable locParam = new Hashtable();
    locParam[(object) "magic_name"] = (object) targetName;
    string[] strArray = new string[2]
    {
      warReportId.ToString(),
      warReportOwnerUid.ToString()
    };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.MagicReport_Attack, GameEngine.Instance.ChatManager.allianceChannelId, strArray[0].ToLower() + ":" + strArray[1].ToString(), "chat_share_magic_report_attack_description", locParam, strArray, false);
  }

  public static bool SendDragonAltarBeAttackReport(long warReportId, long warReportOwnerUid, string targetName)
  {
    if (!((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null) || GameEngine.Instance.ChatManager == null || GameEngine.Instance.ChatManager.allianceChannel == null)
      return false;
    Hashtable locParam = new Hashtable();
    locParam[(object) "magic_name"] = (object) targetName;
    string[] strArray = new string[2]
    {
      warReportId.ToString(),
      warReportOwnerUid.ToString()
    };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.MagicReport_BeAttack, GameEngine.Instance.ChatManager.allianceChannelId, strArray[0].ToLower() + ":" + strArray[1].ToString(), "chat_share_magic_report_defense_description", locParam, strArray, false);
  }

  public static bool SendAllianceMessage(ChatMessageManager.AllianceMessageType messageType, string name, string from, string to)
  {
    Hashtable locParam = new Hashtable();
    switch (messageType)
    {
      case ChatMessageManager.AllianceMessageType.promote:
        locParam.Clear();
        locParam[(object) nameof (name)] = (object) name;
        locParam[(object) nameof (from)] = (object) from;
        locParam[(object) nameof (to)] = (object) to;
        return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.AllianceAdimMessage, GameEngine.Instance.ChatManager.allianceChannelId, string.Empty, "chat_alliance_member_promoted_description", locParam, (string[]) null, false);
      case ChatMessageManager.AllianceMessageType.demote:
        locParam.Clear();
        locParam[(object) nameof (name)] = (object) name;
        locParam[(object) nameof (from)] = (object) from;
        locParam[(object) nameof (to)] = (object) to;
        return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.AllianceAdimMessage, GameEngine.Instance.ChatManager.allianceChannelId, string.Empty, "chat_alliance_member_demoted_description", locParam, (string[]) null, false);
      case ChatMessageManager.AllianceMessageType.kick:
        locParam.Clear();
        locParam[(object) nameof (name)] = (object) name;
        return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.AllianceAdimMessage, GameEngine.Instance.ChatManager.allianceChannelId, string.Empty, "chat_alliance_member_kicked_description", locParam, (string[]) null, false);
      case ChatMessageManager.AllianceMessageType.transferLeadership:
        locParam.Clear();
        locParam[(object) nameof (name)] = (object) name;
        return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.AllianceAdimMessage, GameEngine.Instance.ChatManager.allianceChannelId, string.Empty, "chat_alliance_leadership_transfer_description", locParam, (string[]) null, false);
      default:
        return false;
    }
  }

  public static bool SendUsurpAllianceMessage()
  {
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.JoinAlliance, GameEngine.Instance.ChatManager.allianceChannelId, string.Empty, "chat_alliance_leadership_usurp_description", (Hashtable) null, (string[]) null, false);
  }

  public static bool SendSpeakerMessage(string text)
  {
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.Empty, GameEngine.Instance.ChatManager.kingdomChannelId, text, string.Empty, (Hashtable) null, (string[]) null, true);
  }

  public static bool SendJoinAllianceMessage()
  {
    Hashtable hashtable = new Hashtable();
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.JoinAlliance, GameEngine.Instance.ChatManager.allianceChannelId, string.Empty, "chat_alliance_new_member_greeting", (Hashtable) null, (string[]) null, false);
  }

  public static bool SendAcceptAllianceInviteMessage(string newMemberName)
  {
    Hashtable locParam = new Hashtable();
    locParam[(object) "0"] = (object) newMemberName;
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.AcceptJoinAlliance, GameEngine.Instance.ChatManager.allianceChannelId, string.Empty, "chat_alliance_new_member_welcome", locParam, (string[]) null, false);
  }

  public static bool SendBookmark(Coordinate coordinate)
  {
    Hashtable locParam = new Hashtable();
    locParam[(object) "x,y"] = (object) string.Format("{0},{1}", (object) coordinate.X, (object) coordinate.Y);
    string[] strArray = new string[3]
    {
      coordinate.K.ToString(),
      coordinate.X.ToString(),
      coordinate.Y.ToString()
    };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.Bookmark, GameEngine.Instance.ChatManager.allianceChannelId, string.Empty, "chat_share_coordinates_description", locParam, strArray, false);
  }

  public static bool SendForgeSuccessMessage(string itemId, int quality, int id, int level)
  {
    string locKey = "chat_alliance_forge_equipment_description_" + (object) quality;
    Hashtable locParam = new Hashtable();
    locParam[(object) "0"] = (object) itemId;
    string[] strArray = new string[2]
    {
      level.ToString(),
      id.ToString()
    };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.Forge, GameEngine.Instance.ChatManager.allianceChannelId, string.Empty, locKey, locParam, strArray, false);
  }

  public static bool SendForgeDKSuccessMessage(string itemId, int quality, int id, int level)
  {
    string locKey = "chat_alliance_forge_equipment_description_" + (object) quality;
    Hashtable locParam = new Hashtable();
    locParam[(object) "0"] = (object) itemId;
    string[] strArray = new string[2]
    {
      level.ToString(),
      id.ToString()
    };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.ForgeDK, GameEngine.Instance.ChatManager.allianceChannelId, string.Empty, locKey, locParam, strArray, false);
  }

  public static bool SendEquiomentEnhance(string item, int quality, string toLevel, int id, int level)
  {
    string locKey = "chat_share_enhance_description_" + (object) quality;
    Hashtable locParam = new Hashtable();
    locParam[(object) "0"] = (object) item;
    locParam[(object) "1"] = (object) toLevel;
    string[] strArray = new string[2]
    {
      level.ToString(),
      id.ToString()
    };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.EquimentEnhance, GameEngine.Instance.ChatManager.allianceChannelId, string.Empty, locKey, locParam, strArray, false);
  }

  public static bool SendEquiomentEnhanceDK(string item, int quality, string toLevel, int id, int level)
  {
    string locKey = "chat_share_enhance_description_" + (object) quality;
    Hashtable locParam = new Hashtable();
    locParam[(object) "0"] = (object) item;
    locParam[(object) "1"] = (object) toLevel;
    string[] strArray = new string[2]
    {
      level.ToString(),
      id.ToString()
    };
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.EquimentEnhanceDK, GameEngine.Instance.ChatManager.allianceChannelId, string.Empty, locKey, locParam, strArray, false);
  }

  public static bool SendChatMemberMessage(string userName, string locKey, long channelID)
  {
    Hashtable locParam = new Hashtable();
    locParam[(object) "0"] = (object) userName;
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.Chat_Member, channelID, string.Empty, locKey, locParam, (string[]) null, false);
  }

  public static bool SendChatMemberWelcomMessage(string userName, string roomName, string locKey, long channelID)
  {
    Hashtable locParam = new Hashtable();
    locParam[(object) "0"] = (object) userName;
    locParam[(object) "1"] = (object) roomName;
    return ChatMessageManager.SendSystemMessage(ChatTraceData.TraceType.Chat_Member, channelID, string.Empty, locKey, locParam, (string[]) null, false);
  }

  public static bool SendChatMemberWelcomMessage1(string userName, string roomName, string locKey, long channelID, string optionUserName, int optionIcon, long senderUid)
  {
    ChatMessage message = new ChatMessage();
    message.senderName = optionUserName;
    message.senderIcon = optionIcon;
    message.senderUid = senderUid;
    Hashtable hashtable = new Hashtable();
    hashtable[(object) "0"] = (object) userName;
    hashtable[(object) "1"] = (object) roomName;
    message.localizationKey = locKey;
    message.localizationParam = hashtable;
    message.traceData.type = ChatTraceData.TraceType.Chat_Member;
    message.traceData.isSpeaker = false;
    message.sendTime = NetServerTime.inst.ServerTimestamp;
    if ((UnityEngine.Object) GameEngine.Instance != (UnityEngine.Object) null && GameEngine.Instance.ChatManager != null && channelID != 0L)
      GameEngine.Instance.ChatManager.SendGroupMessage(channelID, message, true, true);
    return true;
  }

  public static bool OnWarReportOpen(ChatTraceData data)
  {
    PlayerData.inst.mail.GetSharedMail(long.Parse(data.GetLong(1).ToString()), long.Parse(data.GetLong(0).ToString()), 0);
    return true;
  }

  public static bool OnScoutReportOpen(ChatTraceData data)
  {
    PlayerData.inst.mail.GetSharedMail(long.Parse(data.GetLong(1).ToString()), long.Parse(data.GetLong(0).ToString()), 1);
    return true;
  }

  public static bool OnMagicReportOpen(ChatTraceData data)
  {
    PlayerData.inst.mail.GetSharedMail(long.Parse(data.GetLong(1).ToString()), long.Parse(data.GetLong(0).ToString()), 2);
    return true;
  }

  public static bool OnRallyDlgOpen(ChatTraceData data)
  {
    long result = -1;
    long.TryParse(data.GetLong(0).ToString(), out result);
    if (DBManager.inst.DB_Rally.Get(result) != null)
    {
      if (DBManager.inst.DB_Rally.Get(result).location.K != PlayerData.inst.userData.current_world_id)
      {
        UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_rally_in_other_kingdom"), (System.Action) null, 4f, false);
        return false;
      }
      UIManager.inst.OpenDlg("Alliance/AllianceRallyDetialDlg", (UI.Dialog.DialogParameter) new AllianceRallyDetailPopUp.Parameter()
      {
        data_id = result
      }, 1 != 0, 1 != 0, 1 != 0);
      return true;
    }
    UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_rally_unavailable"), (System.Action) null, 4f, false);
    return false;
  }

  public static bool OnCasinoCardHistoryPopupOpen(ChatTraceData traceData)
  {
    long userId = long.Parse(traceData.GetLong(0).ToString());
    long num = long.Parse(traceData.GetLong(1).ToString());
    if (userId == -1L)
      return false;
    MessageHub.inst.GetPortByAction("casino:getChest").SendRequest(new Hashtable()
    {
      {
        (object) "shareid",
        (object) num
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable inData = data as Hashtable;
      int outData = -1;
      ArrayList data1 = new ArrayList();
      if (inData == null || inData[(object) "chest_id"] == null)
        return;
      DatabaseTools.UpdateData(inData, "chest_id", ref outData);
      DatabaseTools.CheckAndParseOrgData(inData[(object) "opened"], out data1);
      if (outData == -1)
        UIManager.inst.toast.Show(Utils.XLAT("toast_chat_share_expired"), (System.Action) null, 4f, false);
      else
        UIManager.inst.OpenPopup("Casino/CardHistoryPopup", (Popup.PopupParameter) new CasinoCardHistoryPopup.Parameter()
        {
          uid = userId,
          chestId = outData,
          openedCellList = data1,
          isFromShare = true
        });
    }), true);
    return true;
  }

  public static bool OnForgeDlgOpen(ChatTraceData data)
  {
    EquipmentInfoPopup.Parameter parameter = new EquipmentInfoPopup.Parameter();
    parameter.enhanceLevel = data.GetInt(0);
    parameter.equipmenID = data.GetInt(1);
    if (parameter.equipmenID == 0)
      return false;
    UIManager.inst.OpenPopup("Equipment/EquipmentInfoPopup", (Popup.PopupParameter) parameter);
    return true;
  }

  public static bool OnBookmarkClick(ChatTraceData data)
  {
    LinkerHub.Instance.LHGotoTile(new LPTile()
    {
      k = data.GetInt(0),
      x = data.GetInt(1),
      y = data.GetInt(2)
    });
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    return true;
  }

  public enum AllianceMessageType
  {
    promote,
    demote,
    kick,
    transferLeadership,
  }
}
