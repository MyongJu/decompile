﻿// Decompiled with JetBrains decompiler
// Type: TavernWheelGroupInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class TavernWheelGroupInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "item_id")]
  public int itemId;
  [Config(Name = "spend")]
  public int spend;
  [Config(Name = "win_threshold")]
  public int winThreshold;
  [Config(Name = "item_gold")]
  public int itemGold;
  [Config(Name = "item_number")]
  public int itemNumber;
  [Config(Name = "item_daily_score")]
  public int itemDailyScore;
  [Config(Name = "item_daily_number")]
  public int itemDailyNumber;
}
