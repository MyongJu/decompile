﻿// Decompiled with JetBrains decompiler
// Type: PersonalChestItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;
using UnityEngine;

public class PersonalChestItem : MonoBehaviour
{
  [SerializeField]
  private UILabel _LabelChestName;
  [SerializeField]
  private UILabel _LabelTotalCount;
  [SerializeField]
  private UILabel _LabelPeopleCount;
  [SerializeField]
  private UILabel _sendLabel;
  [SerializeField]
  private UILabel _viewLabel;
  [SerializeField]
  private GameObject _rootSend;
  [SerializeField]
  private GameObject _rootView;
  private UIToggle _toggleAnonymous;
  private UserTreasureChestData _userTreasureChestData;

  public void SetData(UserTreasureChestData userTreasureChestData, UIToggle toggleAnonymous)
  {
    this._userTreasureChestData = userTreasureChestData;
    AlliancePersonalChestInfo data1 = ConfigManager.inst.DB_AlliancePersonalChest.GetData(userTreasureChestData.ConfigId);
    if (data1 != null)
    {
      AllianceTreasuryVaultMainInfo data2 = ConfigManager.inst.DB_AllianceTreasuryVaultMain.GetData(data1.chestId);
      if (data2 != null)
      {
        this._LabelChestName.text = data2.LocalChestName;
        this._LabelTotalCount.text = data2.Gold.ToString();
        this._LabelPeopleCount.text = data2.Num.ToString();
      }
    }
    this._toggleAnonymous = toggleAnonymous;
    this._rootSend.SetActive(!userTreasureChestData.AlreadySend);
    this._rootView.SetActive(userTreasureChestData.AlreadySend);
    this._sendLabel.text = Utils.XLAT("alliance_giftbox_send_button");
    this._viewLabel.text = Utils.XLAT("alliance_giftbox_view_button");
  }

  public void OnButtonSendClicked()
  {
    Hashtable postData = new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "type",
        (object) this._userTreasureChestData.ConfigId
      },
      {
        (object) "info_hidden",
        !this._toggleAnonymous.value ? (object) "0" : (object) "1"
      }
    };
    RequestManager.inst.SendRequest("TreasuryVault:sendUserTreasury", postData, (System.Action<bool, object>) ((result, data) =>
    {
      if (!result)
        return;
      Hashtable hashtable1 = data as Hashtable;
      if (hashtable1 == null || !hashtable1.ContainsKey((object) "treasure_chest_info"))
        return;
      Hashtable hashtable2 = hashtable1[(object) "treasure_chest_info"] as Hashtable;
      if (hashtable2 == null)
        return;
      AllianceTreasuryVaultData treasuryVaultData = new AllianceTreasuryVaultData();
      treasuryVaultData.Decode(hashtable2);
      UIManager.inst.OpenPopup("AllianceChest/PersonalChestPopup", (Popup.PopupParameter) new PersonalChestPopup.Parameter()
      {
        sendedChestData = treasuryVaultData,
        firstTime = true
      });
      AudioManager.Instance.StopAndPlaySound("sfx_city_vault_withdraw");
    }), true);
  }

  public void OnButtonViewClicked()
  {
    Hashtable postData = new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "type",
        (object) this._userTreasureChestData.ConfigId
      }
    };
    RequestManager.inst.SendRequest("TreasuryVault:getUserTreasureChestInfo", postData, (System.Action<bool, object>) ((result, data) =>
    {
      if (!result)
        return;
      Hashtable hashtable1 = data as Hashtable;
      if (hashtable1 == null || !hashtable1.ContainsKey((object) "treasure_chest_info"))
        return;
      Hashtable hashtable2 = hashtable1[(object) "treasure_chest_info"] as Hashtable;
      if (hashtable2 == null)
        return;
      AllianceTreasuryVaultData treasuryVaultData = new AllianceTreasuryVaultData();
      treasuryVaultData.Decode(hashtable2);
      UIManager.inst.OpenPopup("AllianceChest/PersonalChestPopup", (Popup.PopupParameter) new PersonalChestPopup.Parameter()
      {
        sendedChestData = treasuryVaultData,
        firstTime = false
      });
    }), true);
  }
}
