﻿// Decompiled with JetBrains decompiler
// Type: MarchResetDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MarchResetDlg : UI.Dialog
{
  public Dictionary<string, MarchResetDlg.TroopData> troops = new Dictionary<string, MarchResetDlg.TroopData>();
  public List<TroopSlider> list = new List<TroopSlider>();
  private int resourceSize = 1;
  private int _armyIndex = 1;
  public UILabel label;
  public UILabel troopsCount;
  public UILabel troopLoads;
  public UILabel troopPower;
  public UIButton button;
  public TroopSlider troopItem;
  public UIScrollView view;
  public DragonSlider dragonSlider;
  public UILabel noTroosText;
  public UIGrid listParent;
  private int maxTroopCount;
  private bool isInited;
  private bool isDragonIn;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._armyIndex = (orgParam as MarchResetDlg.Parameter).armyIndex;
    string Term = "march_save_army_num_title";
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("0", this._armyIndex.ToString());
    CityData.MarchSetData marchSetData = PlayerData.inst.CityData.MyCityData.GetMarchSetData(this._armyIndex);
    if (marchSetData != null)
      this.isDragonIn = marchSetData.isDragonIn;
    this.label.text = ScriptLocalization.GetWithPara(Term, para, true);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.dragonSlider.onDragonSelectedChanged += new System.Action<bool>(this.OnDragonSelectedChanged);
    this.FreshPanel();
    this.SetData(this._armyIndex);
  }

  public void FreshPanel()
  {
    if (!this.isInited)
      this.InitTroopList();
    this.FreshDragonData();
    this.CalcTroopLimitCount();
    this.FreshLeftTroopCount();
    this.FreshTroopList();
    this.FreshTroopData();
    this.FreshButtons();
    this.FreshTroopData();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.dragonSlider.onDragonSelectedChanged -= new System.Action<bool>(this.OnDragonSelectedChanged);
    this.troops.Clear();
  }

  public void InitTroopList()
  {
    if (this.list.Count > 0)
    {
      for (int index = 0; index < this.list.Count; ++index)
      {
        string id = this.list[index].troopInfo.ID;
        if (this.troops.ContainsKey(id))
          this.troops[id].troopCount = this.list[index].troopCount;
        this.list[index].onValueChange -= new System.Action<string, int>(this.OnTroopCountChanged);
        UnityEngine.Object.Destroy((UnityEngine.Object) this.list[index].gameObject);
      }
      this.list.Clear();
    }
    CityTroopsInfo totalTroops = PlayerData.inst.playerCityData.totalTroops;
    if (totalTroops.troops == null)
      return;
    List<Unit_StatisticsInfo> unitStatisticsInfoList = new List<Unit_StatisticsInfo>();
    Dictionary<string, int>.KeyCollection.Enumerator enumerator = totalTroops.troops.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator.Current);
      if (data != null && data.Type != "trap")
        unitStatisticsInfoList.Add(data);
    }
    unitStatisticsInfoList.Sort((Comparison<Unit_StatisticsInfo>) ((a, b) =>
    {
      if (a.Troop_Tier == b.Troop_Tier)
        return a.Priority.CompareTo(b.Priority);
      return b.Troop_Tier.CompareTo(a.Troop_Tier);
    }));
    for (int index = 0; index < unitStatisticsInfoList.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.listParent.gameObject, this.troopItem.gameObject);
      gameObject.name = "troop_" + (object) (100 + index);
      gameObject.transform.localPosition = new Vector3(0.0f, -250f * (float) (index + 1), 0.0f);
      if (!this.troops.ContainsKey(unitStatisticsInfoList[index].ID))
        this.troops.Add(unitStatisticsInfoList[index].ID, new MarchResetDlg.TroopData()
        {
          troopId = unitStatisticsInfoList[index].ID
        });
      TroopSlider component = gameObject.GetComponent<TroopSlider>();
      component.onValueChange += new System.Action<string, int>(this.OnTroopCountChanged);
      this.list.Add(component);
      gameObject.SetActive(false);
    }
    this.noTroosText.gameObject.SetActive(this.list.Count == 0);
    this.CalcTroopLimitCount();
    this.FreshTroopList();
  }

  private void SetData(int index)
  {
    CityData.MarchSetData marchSetData = PlayerData.inst.CityData.MyCityData.GetMarchSetData(index);
    if (marchSetData != null)
    {
      this.isDragonIn = marchSetData.isDragonIn;
      Dictionary<string, int> troops = marchSetData.troops;
      for (int index1 = 0; index1 < this.list.Count; ++index1)
      {
        if (troops.ContainsKey(this.list[index1].troopInfo.ID))
          this.list[index1].troopCount = troops[this.list[index1].troopInfo.ID];
      }
    }
    this.dragonSlider.isSelected = this.isDragonIn;
  }

  public void OnTroopCountChanged(string troopId, int count)
  {
    int num1 = 0;
    for (int index = 0; index < this.list.Count; ++index)
      num1 += this.list[index].troopCount;
    if (num1 > this.maxTroopCount)
    {
      int num2 = num1 - this.maxTroopCount;
      for (int index = this.list.Count - 1; index >= 0; --index)
      {
        if (this.list[index].troopInfo.ID == troopId)
        {
          this.list[index].troopCount -= num2;
          break;
        }
      }
      int maxTroopCount = this.maxTroopCount;
    }
    this.FreshLeftTroopCount();
    this.FreshTroopData();
    this.FreshButtons();
  }

  public void OnDragonSelectedChanged(bool isSelected)
  {
    this.isDragonIn = isSelected;
    this.CalcTroopLimitCount();
    int num = 0;
    for (int index = 0; index < this.list.Count; ++index)
      num += this.list[index].troopCount;
    this.FreshDragonData();
    this.FreshTroopData();
    this.FreshLeftTroopCount();
  }

  public void OnCityDataChanged(long cityId)
  {
    if (cityId != (long) PlayerData.inst.cityId || TutorialManager.Instance.IsRunning)
      return;
    this.InitTroopList();
    this.FreshTroopData();
    this.FreshButtons();
  }

  public void OnDragonDataChanged(long dragonId)
  {
    if (dragonId != PlayerData.inst.uid)
      return;
    this.OnDragonSelectedChanged(this.isDragonIn);
    this.FreshDragonData();
  }

  private void CalcTroopLimitCount()
  {
    this.maxTroopCount = (int) DBManager.inst.DB_Local_Benefit.Get("prop_march_capacity_base_value").total;
    if (this.isDragonIn)
      this.maxTroopCount = (int) ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) this.maxTroopCount, "calc_march_capacity", ConfigBenefitCalc.CalcOption.dragonAll);
    else
      this.maxTroopCount = (int) ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) this.maxTroopCount, "calc_march_capacity");
  }

  public void FreshTroopData()
  {
    int internalId = ConfigManager.inst.DB_Properties["prop_army_load_percent"].InternalID;
    BenefitInfo benefitInfo = DBManager.inst.DB_Local_Benefit.Get(internalId);
    float num1 = 1f;
    if (benefitInfo != null)
      num1 += benefitInfo.total;
    if (this.isDragonIn)
    {
      DragonData dragonData = DBManager.inst.DB_Dragon.Get(PlayerData.inst.uid);
      if (dragonData != null)
        num1 += dragonData.benefits.GetBenefit(internalId);
    }
    int num2 = 0;
    int num3 = 0;
    int num4 = 0;
    for (int index = 0; index < this.list.Count; ++index)
    {
      ConfigManager.inst.DB_BenefitCalc.GetUnitFinalData(this.list[index].troopInfo.ID, ConfigBenefitCalc.UnitCalcType.load | ConfigBenefitCalc.UnitCalcType.speed, ConfigBenefitCalc.CalcOption.none);
      num2 += this.list[index].troopCount;
      num3 += Mathf.RoundToInt((float) (this.list[index].troopInfo.GatherCapacity * this.list[index].troopCount) * num1);
      num4 += this.list[index].troopInfo.Power * this.list[index].troopCount;
    }
    this.troopsCount.text = string.Format("{0} / {1}", (object) Utils.FormatThousands(num2.ToString()), (object) Utils.FormatThousands(this.maxTroopCount.ToString()));
    this.troopLoads.text = Utils.FormatThousands((num3 / this.resourceSize).ToString());
    this.troopPower.text = Utils.FormatThousands(num4.ToString());
    this.FreshButtons();
  }

  public void FreshDragonData()
  {
    this.dragonSlider.JustShow((string) null);
  }

  public void FreshTroopList()
  {
    CityTroopsInfo totalTroops = PlayerData.inst.playerCityData.totalTroops;
    if (totalTroops.troops == null)
      return;
    List<Unit_StatisticsInfo> unitStatisticsInfoList = new List<Unit_StatisticsInfo>();
    Dictionary<string, int>.KeyCollection.Enumerator enumerator = totalTroops.troops.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator.Current);
      if (data != null && data.Type != "trap")
        unitStatisticsInfoList.Add(data);
    }
    unitStatisticsInfoList.Sort((Comparison<Unit_StatisticsInfo>) ((a, b) =>
    {
      if (a.Troop_Tier == b.Troop_Tier)
        return a.Priority.CompareTo(b.Priority);
      return b.Troop_Tier.CompareTo(a.Troop_Tier);
    }));
    for (int index = 0; index < unitStatisticsInfoList.Count; ++index)
    {
      if (totalTroops.troops[unitStatisticsInfoList[index].ID] > 0)
        this.list[index].gameObject.SetActive(true);
      TroopSlider troopSlider = this.list[index];
      troopSlider.troopInfo = unitStatisticsInfoList[index];
      troopSlider.troopMaxCount = totalTroops.troops[unitStatisticsInfoList[index].ID];
      troopSlider.troopCount = this.troops[unitStatisticsInfoList[index].ID].troopCount;
    }
  }

  private void FreshLeftTroopCount()
  {
    int num1 = 0;
    for (int index = 0; index < this.list.Count; ++index)
      num1 += this.list[index].troopCount;
    int num2 = this.maxTroopCount - num1;
    for (int index = 0; index < this.list.Count; ++index)
      this.list[index].targetCount = this.list[index].troopCount + num2;
  }

  public void FreshButtons()
  {
    for (int index = 0; index < this.list.Count; ++index)
    {
      if (this.list[index].troopCount > 0)
      {
        this.button.isEnabled = true;
        return;
      }
    }
    this.button.isEnabled = false;
  }

  public void Save()
  {
    Hashtable hashtable1 = new Hashtable();
    for (int index = 0; index < this.list.Count; ++index)
    {
      if (this.list[index].troopCount > 0)
        hashtable1.Add((object) this.list[index].troopInfo.ID, (object) this.list[index].troopCount);
    }
    Hashtable hashtable2 = new Hashtable();
    hashtable2.Add((object) "is_dragon_in", (object) this.isDragonIn);
    hashtable2.Add((object) "troops", (object) hashtable1);
    Hashtable hashtable3 = new Hashtable();
    hashtable3.Add((object) this._armyIndex, (object) hashtable2);
    int setMarchSlot = PlayerData.inst.playerCityData.GetSetMarchSlot();
    for (int index1 = 0; index1 < setMarchSlot; ++index1)
    {
      int index2 = index1 + 1;
      if (index2 != this._armyIndex)
      {
        CityData.MarchSetData marchSetData = PlayerData.inst.playerCityData.GetMarchSetData(index2);
        if (marchSetData != null)
          hashtable3.Add((object) index2, (object) marchSetData.GetOrginData());
      }
    }
    RequestManager.inst.SendRequest("city:setTroopMarshalling", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "troop_marshalling",
        (object) hashtable3
      }
    }, new System.Action<bool, object>(this.OnSaveCallBack), true);
  }

  private void OnSaveCallBack(bool success, object result)
  {
    if (!success)
      return;
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_march_save_army_success", true), (System.Action) null, 4f, false);
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public class TroopData
  {
    public string troopId;
    public int troopCount;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int armyIndex = 1;
  }
}
