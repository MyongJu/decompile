﻿// Decompiled with JetBrains decompiler
// Type: AllianceChangeFortressHeader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceChangeFortressHeader : AllianceCustomizeHeader
{
  private List<AllianceChangeFortressItemRenderer> m_ItemList = new List<AllianceChangeFortressItemRenderer>();
  private List<AllianceChangeFortressHeader.ChangeNameOperation> m_allChangeNameOperation = new List<AllianceChangeFortressHeader.ChangeNameOperation>();
  public UIGrid m_Grid;
  public UIButton m_ChangeButton;
  public GameObject m_FortressItemPrefab;

  public void OnChangeFortressNames()
  {
    for (int index = 0; index < this.m_ItemList.Count; ++index)
      this.m_ItemList[index].DoModify();
  }

  private void AddChangeNameOperation(TileType tileType, long buildingId, string name)
  {
    this.m_allChangeNameOperation.Add(new AllianceChangeFortressHeader.ChangeNameOperation(tileType, buildingId, name));
  }

  private void SetupAllianceCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.UpdateUI();
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_alliance_building_name_change_success", true), (System.Action) null, 4f, false);
  }

  protected override void UpdatePanel()
  {
    base.UpdatePanel();
    if (this.IsExpanded)
      this.UpdateUI();
    else
      this.ClearData();
  }

  protected AllianceChangeFortressItemRenderer CreateRenameItem()
  {
    GameObject gameObject = Utils.DuplicateGOB(this.m_FortressItemPrefab, this.m_Grid.transform);
    gameObject.SetActive(true);
    gameObject.transform.localScale = Vector3.one;
    return gameObject.GetComponent<AllianceChangeFortressItemRenderer>();
  }

  protected override void UpdateUI()
  {
    this.ClearData();
    List<AllianceBuildingStaticInfo> totalBuildings1 = ConfigManager.inst.DB_AllianceBuildings.GetTotalBuildings(0);
    for (int index = 0; index < totalBuildings1.Count; ++index)
    {
      AllianceChangeFortressItemRenderer item = this.CreateRenameItem();
      AllianceFortressData fortressData = DBManager.inst.DB_AllianceFortress.GetMyFortressDataByBuildingConfigId(totalBuildings1[index].internalId);
      string name = fortressData != null ? fortressData.Name : string.Empty;
      item.SetData(totalBuildings1[index].LocalName, name, fortressData != null, (System.Action) (() => this.AddChangeNameOperation(TileType.AllianceFortress, fortressData.fortressId, item.Name)));
      this.m_ItemList.Add(item);
    }
    List<AllianceBuildingStaticInfo> totalBuildings2 = ConfigManager.inst.DB_AllianceBuildings.GetTotalBuildings(1);
    for (int index = 0; index < totalBuildings2.Count; ++index)
    {
      AllianceChangeFortressItemRenderer item = this.CreateRenameItem();
      AllianceFortressData fortressData = DBManager.inst.DB_AllianceFortress.GetMyFortressDataByBuildingConfigId(totalBuildings2[index].internalId);
      string name = fortressData != null ? fortressData.Name : string.Empty;
      item.SetData(totalBuildings2[index].LocalName, name, fortressData != null, (System.Action) (() => this.AddChangeNameOperation(TileType.AllianceFortress, fortressData.fortressId, item.Name)));
      this.m_ItemList.Add(item);
    }
    List<AllianceBuildingStaticInfo> totalBuildings3 = ConfigManager.inst.DB_AllianceBuildings.GetTotalBuildings(2);
    for (int index = 0; index < totalBuildings3.Count; ++index)
    {
      AllianceChangeFortressItemRenderer item = this.CreateRenameItem();
      AllianceWareHouseData warehouseData = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseDataByBuildingConfigId(totalBuildings3[index].internalId);
      string name = warehouseData != null ? warehouseData.Name : string.Empty;
      item.SetData(totalBuildings3[index].LocalName, name, warehouseData != null, (System.Action) (() => this.AddChangeNameOperation(TileType.AllianceWarehouse, warehouseData.WarehouseId, item.Name)));
      this.m_ItemList.Add(item);
    }
    List<AllianceBuildingStaticInfo> totalBuildings4 = ConfigManager.inst.DB_AllianceBuildings.GetTotalBuildings(8);
    for (int index = 0; index < totalBuildings4.Count; ++index)
    {
      AllianceChangeFortressItemRenderer item = this.CreateRenameItem();
      AllianceHospitalData hospitalData = DBManager.inst.DB_AllianceHospital.GetMyHospitalDataByBuildingConfigId(totalBuildings4[index].internalId);
      string name = hospitalData != null ? hospitalData.Name : string.Empty;
      item.SetData(totalBuildings4[index].LocalName, name, hospitalData != null, (System.Action) (() => this.AddChangeNameOperation(TileType.AllianceHospital, hospitalData.BuildingID, item.Name)));
      this.m_ItemList.Add(item);
    }
    this.m_Grid.onReposition = new UIGrid.OnReposition(this.OnReposition);
    this.m_Grid.Reposition();
  }

  private void OnReposition()
  {
    this.m_Grid.transform.localPosition = new Vector3(-470f, -358f, 0.0f);
  }

  private static int Compare(AllianceFortressData x, AllianceFortressData y)
  {
    return Math.Sign(x.fortressId - y.fortressId);
  }

  private void ClearData()
  {
    int count = this.m_ItemList.Count;
    for (int index = 0; index < count; ++index)
    {
      AllianceChangeFortressItemRenderer fortressItemRenderer = this.m_ItemList[index];
      fortressItemRenderer.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) fortressItemRenderer.gameObject);
    }
    this.m_ItemList.Clear();
  }

  private bool IsOkay
  {
    get
    {
      bool flag1 = true;
      bool flag2 = false;
      for (int index = 0; index < this.m_ItemList.Count; ++index)
      {
        if (this.m_ItemList[index].Editable)
        {
          if (this.m_ItemList[index].IsChanged)
            flag2 = true;
          flag1 &= this.m_ItemList[index].isOkay;
        }
      }
      if (flag2)
        return flag1;
      return false;
    }
  }

  private void Update()
  {
    this.m_ChangeButton.isEnabled = this.IsOkay;
    if (this.m_allChangeNameOperation.Count <= 0)
      return;
    ArrayList arrayList = new ArrayList();
    using (List<AllianceChangeFortressHeader.ChangeNameOperation>.Enumerator enumerator = this.m_allChangeNameOperation.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceChangeFortressHeader.ChangeNameOperation current = enumerator.Current;
        Hashtable hashtable = new Hashtable();
        hashtable[(object) "type"] = (object) current.tileType;
        hashtable[(object) "building_id"] = (object) current.buildingId;
        hashtable[(object) "name"] = (object) IllegalWordsUtils.Filter(current.name);
        arrayList.Add((object) hashtable);
      }
    }
    Hashtable postData = new Hashtable();
    postData[(object) "building_names"] = (object) arrayList;
    MessageHub.inst.GetPortByAction("Alliance:changeAllianceBuildingName").SendRequest(postData, new System.Action<bool, object>(this.SetupAllianceCallback), true);
    this.m_allChangeNameOperation.Clear();
  }

  private struct ChangeNameOperation
  {
    public TileType tileType;
    public long buildingId;
    public string name;

    public ChangeNameOperation(TileType tileType, long buildingId, string name)
    {
      this.tileType = tileType;
      this.buildingId = buildingId;
      this.name = name;
    }
  }
}
