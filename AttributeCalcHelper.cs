﻿// Decompiled with JetBrains decompiler
// Type: AttributeCalcHelper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class AttributeCalcHelper
{
  private Dictionary<string, string> _attributes2Benefits = new Dictionary<string, string>();
  private static AttributeCalcHelper _instance;

  private AttributeCalcHelper()
  {
    this._attributes2Benefits.Add("str", "strength");
    this._attributes2Benefits.Add("int", "intelligent");
    this._attributes2Benefits.Add("arm", "armor");
    this._attributes2Benefits.Add("con", "constitution");
    this._attributes2Benefits.Add("atk", "attack");
    this._attributes2Benefits.Add("mtk", "magic");
    this._attributes2Benefits.Add("def", "defence");
    this._attributes2Benefits.Add("hp", "health");
    this._attributes2Benefits.Add("sp", "speed");
    this._attributes2Benefits.Add("reb", "rebound");
    this._attributes2Benefits.Add("mp", "mana");
    this._attributes2Benefits.Add("hp_re", "health_recover");
    this._attributes2Benefits.Add("mp_re", "mana_recover");
    this._attributes2Benefits.Add("ftk", "fire_power");
    this._attributes2Benefits.Add("wtk", "water_power");
    this._attributes2Benefits.Add("ntk", "nature_power");
    this._attributes2Benefits.Add("sta", "dungeon_stamina");
    this._attributes2Benefits.Add("bag", "bag_capacity");
    this._attributes2Benefits.Add("load", "load");
    this._attributes2Benefits.Add("init_sta", "addition_stamina_count");
    this._attributes2Benefits.Add("init_mp", "addition_mana_count");
    this._attributes2Benefits.Add("init_hp", "addition_health_count");
    this._attributes2Benefits.Add("sta_eff", "addition_stamina_effect");
    this._attributes2Benefits.Add("mana_eff", "addition_mana_effect");
    this._attributes2Benefits.Add("hp_eff", "addition_health_effect");
    this._attributes2Benefits.Add("skill_lim", "skill_equip");
  }

  public static AttributeCalcHelper Instance
  {
    get
    {
      if (AttributeCalcHelper._instance == null)
        AttributeCalcHelper._instance = new AttributeCalcHelper();
      return AttributeCalcHelper._instance;
    }
  }

  public int GetCurrentAttibuteValueInDungen(DragonKnightAttibute.Type type, long uid = 0)
  {
    if (type != DragonKnightAttibute.Type.MP && type != DragonKnightAttibute.Type.Health)
      return 0;
    return DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(uid).GetAttribute(DragonKnightAttibute.Type2String(type));
  }

  public string GetAttributes2Benefitkey(string key)
  {
    string str = key;
    if (this._attributes2Benefits.ContainsKey(key))
      str = this._attributes2Benefits[key];
    return str;
  }

  public string GetBenefitKey(DragonKnightAttibute.Type type, bool isInBattle = true)
  {
    string str = "calc_dragon_knight_";
    if (isInBattle)
      str = "calc_dragon_knight_inbattle_";
    string key = DragonKnightAttibute.Type2String(type);
    if (this._attributes2Benefits.ContainsKey(key))
      key = this._attributes2Benefits[key];
    return str + key;
  }

  public int CalcAttributeInBattleFromOutBattle(float baseValue, DragonKnightAttibute.Type type, bool isEvil = false, List<BattleBuff> buffs = null, DragonKnightData data = null)
  {
    Dictionary<string, float> dictionary = (Dictionary<string, float>) null;
    if (data == null)
      data = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (buffs != null)
    {
      for (int index = 0; index < buffs.Count; ++index)
      {
        BattleBuff buff = buffs[index];
        dictionary = this.Combine(dictionary, buff.Benefits);
      }
    }
    if (!isEvil)
      dictionary = this.Combine(data.Benefits, dictionary);
    else if (data != null && data.UserId != PlayerData.inst.uid)
      dictionary = this.Combine(data.Benefits, dictionary);
    string benefitKey = this.GetBenefitKey(type, true);
    return Mathf.CeilToInt(this.GetFinalData(baseValue, benefitKey, dictionary));
  }

  public int CalcAttributeInBattle(float baseValue, DragonKnightAttibute.Type type, bool isEvil = false, List<BattleBuff> buffs = null, DragonKnightData data = null)
  {
    Dictionary<string, float> dictionary = (Dictionary<string, float>) null;
    if (data == null)
      data = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (buffs != null)
    {
      for (int index = 0; index < buffs.Count; ++index)
      {
        BattleBuff buff = buffs[index];
        dictionary = this.Combine(dictionary, buff.Benefits);
      }
    }
    if (!isEvil)
      dictionary = this.Combine(data.Benefits, dictionary);
    else if (data != null && data.UserId != PlayerData.inst.uid)
      dictionary = this.Combine(data.Benefits, dictionary);
    baseValue = (float) this.CalcAttribute(baseValue, type, dictionary);
    string benefitKey = this.GetBenefitKey(type, true);
    return Mathf.CeilToInt(this.GetFinalData(baseValue, benefitKey, dictionary));
  }

  public int CalcAttributeOutBattle(float baseValue, DragonKnightAttibute.Type type, bool isEvil = false, List<BattleBuff> buffs = null, DragonKnightData data = null)
  {
    Dictionary<string, float> dictionary = (Dictionary<string, float>) null;
    if (data == null)
      data = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (buffs != null)
    {
      for (int index = 0; index < buffs.Count; ++index)
      {
        BattleBuff buff = buffs[index];
        dictionary = this.Combine(dictionary, buff.Benefits);
      }
    }
    if (!isEvil)
      dictionary = this.Combine(data.Benefits, dictionary);
    else if (data != null && data.UserId != PlayerData.inst.uid)
      dictionary = this.Combine(data.Benefits, dictionary);
    baseValue = (float) this.CalcAttribute(baseValue, type, dictionary);
    return Mathf.CeilToInt(baseValue);
  }

  private int AddPercent(float baseValue, DragonKnightAttibute.Type type)
  {
    bool isEvil = false;
    List<BattleBuff> buffs = (List<BattleBuff>) null;
    baseValue = (1f + AttributeCalcHelper.Instance.GetAttributePercent(type, isEvil, buffs, (DragonKnightData) null)) * baseValue;
    return Mathf.CeilToInt(baseValue);
  }

  protected float GetOutBattleAttibutePlus(float baseValue, DragonKnightAttibute.Type targetType, DragonKnightAttibute.Type type)
  {
    if ((double) baseValue == 0.0)
      return 0.0f;
    bool isEvil = false;
    List<BattleBuff> buffs = (List<BattleBuff>) null;
    int num1 = AttributeCalcHelper.Instance.CalcAttributeOutBattle(baseValue, type, isEvil, buffs, (DragonKnightData) null);
    float num2 = 0.0f;
    string id = string.Empty;
    switch (type)
    {
      case DragonKnightAttibute.Type.Strong:
        id = "strength";
        break;
      case DragonKnightAttibute.Type.Intelligence:
        id = "intelligent";
        break;
      case DragonKnightAttibute.Type.Constitution:
        id = "constitution";
        break;
      case DragonKnightAttibute.Type.Armor:
        id = "armor";
        break;
    }
    DragonKnightPropertyRateStaticInfo data = ConfigManager.inst.DBDragonKnightPropertyRate.GetData(id);
    if (data != null)
    {
      switch (targetType)
      {
        case DragonKnightAttibute.Type.Damge:
          num2 = data.Attack;
          break;
        case DragonKnightAttibute.Type.MagiceDamge:
          num2 = data.Magic;
          break;
        case DragonKnightAttibute.Type.Defense:
          num2 = data.Defence;
          break;
        case DragonKnightAttibute.Type.Health:
          num2 = data.Health;
          break;
      }
    }
    return (float) num1 * num2;
  }

  private int CalcAttribute(float baseValue, DragonKnightAttibute.Type type, Dictionary<string, float> benefits)
  {
    string benefitKey = this.GetBenefitKey(type, false);
    return Mathf.CeilToInt(this.GetFinalData(baseValue, benefitKey, benefits));
  }

  public int CalcDragonKnightAttribute(DragonKnightAttibute.Type type, DragonKnightData data = null)
  {
    if (type == DragonKnightAttibute.Type.SlotLimit)
      return DragonKnightUtils.GetActiveSkillSlotCount();
    if (data == null)
      data = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    int attibute = data.GetAttibute(type);
    bool isEvil = data.UserId != PlayerData.inst.uid;
    return this.CalcAttributeOutBattle((float) attibute, type, isEvil, (List<BattleBuff>) null, data);
  }

  private Dictionary<string, float> Combine(Dictionary<string, float> benefits, Dictionary<string, float> buffBenefits)
  {
    Dictionary<string, float> dictionary = new Dictionary<string, float>();
    if (benefits == null || benefits.Count == 0)
    {
      if (buffBenefits != null && buffBenefits.Count > 0)
        dictionary = buffBenefits;
      return dictionary;
    }
    if (buffBenefits == null || buffBenefits.Count == 0)
    {
      if (benefits != null && benefits.Count > 0)
        dictionary = benefits;
      return dictionary;
    }
    Dictionary<string, float>.KeyCollection.Enumerator enumerator1 = benefits.Keys.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      string current = enumerator1.Current;
      float benefit = benefits[current];
      if (buffBenefits.ContainsKey(current))
        benefit += buffBenefits[current];
      dictionary.Add(current, benefit);
    }
    Dictionary<string, float>.KeyCollection.Enumerator enumerator2 = buffBenefits.Keys.GetEnumerator();
    while (enumerator2.MoveNext())
    {
      string current = enumerator2.Current;
      float buffBenefit = buffBenefits[current];
      if (!dictionary.ContainsKey(current))
        dictionary.Add(current, buffBenefit);
    }
    return dictionary;
  }

  public float GetFinalDataFromDragonKnightData(float baseValue, string calcId, DragonKnightData data = null)
  {
    if (data == null)
      data = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    return this.GetFinalData(baseValue, calcId, data.Benefits);
  }

  public float GetFinalData(float baseValue, string benefitCalcID, Dictionary<string, float> benefit)
  {
    BenefitCalcInfo data = ConfigManager.inst.DB_BenefitCalc.GetData(benefitCalcID);
    if (data == null)
      return baseValue;
    float p1 = 0.0f;
    float p2 = 0.0f;
    if (data.param1 != null)
    {
      for (int index = 0; index < data.param1.Length; ++index)
      {
        if (!string.IsNullOrEmpty(data.param1[index]))
          p1 += this.GetBenefit(data.param1[index], benefit);
      }
    }
    if (data.param2 != null)
    {
      for (int index = 0; index < data.param2.Length; ++index)
      {
        if (!string.IsNullOrEmpty(data.param2[index]))
          p2 += this.GetBenefit(data.param2[index], benefit);
      }
    }
    switch (data.formula)
    {
      case 1:
        baseValue = BenefitCalcInfo.BenefitFormula1(baseValue, p1, p2);
        break;
      case 2:
        baseValue = BenefitCalcInfo.BenefitFormula2(baseValue, p1, p2);
        break;
      case 4:
        baseValue = BenefitCalcInfo.BenefitFormula4(baseValue, p1, p2);
        break;
    }
    return baseValue;
  }

  public float GetAttributePercent(DragonKnightAttibute.Type type, bool isEvil = false, List<BattleBuff> buffs = null, DragonKnightData data = null)
  {
    Dictionary<string, float> dictionary = (Dictionary<string, float>) null;
    if (data == null)
      data = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (buffs != null)
    {
      for (int index = 0; index < buffs.Count; ++index)
      {
        BattleBuff buff = buffs[index];
        dictionary = this.Combine(dictionary, buff.Benefits);
      }
    }
    if (!isEvil)
      dictionary = this.Combine(data.Benefits, dictionary);
    else if (data != null && data.UserId != PlayerData.inst.uid)
      dictionary = this.Combine(data.Benefits, dictionary);
    string key = DragonKnightAttibute.Type2String(type);
    if (this._attributes2Benefits.ContainsKey(key))
      key = this._attributes2Benefits[key];
    return this.GetBenefit("prop_dragon_knight_" + key + "_percent", dictionary);
  }

  private float GetBenefit(string key, Dictionary<string, float> benefit)
  {
    if (benefit == null || benefit.Count == 0)
      return 0.0f;
    if (benefit.ContainsKey(key))
      return benefit[key];
    string shortId = DBManager.inst.DB_Local_Benefit.GetShortID(key);
    if (!string.IsNullOrEmpty(shortId) && benefit.ContainsKey(shortId))
      return benefit[shortId];
    return 0.0f;
  }

  public int DragonKnightOutBattleAttackAttribute
  {
    get
    {
      DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
      int attibute1 = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Strong);
      int attibute2 = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Intelligence);
      return this.AddPercent((float) dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Damge) + this.GetOutBattleAttibutePlus((float) attibute1, DragonKnightAttibute.Type.Damge, DragonKnightAttibute.Type.Strong) + this.GetOutBattleAttibutePlus((float) attibute2, DragonKnightAttibute.Type.Damge, DragonKnightAttibute.Type.Intelligence), DragonKnightAttibute.Type.Damge);
    }
  }

  public int DragonKnightOutBattleMagicAttackAttribute
  {
    get
    {
      DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
      int attibute = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Intelligence);
      return this.AddPercent((float) dragonKnightData.GetAttibute(DragonKnightAttibute.Type.MagiceDamge) + this.GetOutBattleAttibutePlus((float) attibute, DragonKnightAttibute.Type.MagiceDamge, DragonKnightAttibute.Type.Intelligence), DragonKnightAttibute.Type.MagiceDamge);
    }
  }

  public int DragonKnightOutBattleDefenceAttribute
  {
    get
    {
      DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
      int attibute = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Armor);
      return this.AddPercent((float) dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Defense) + this.GetOutBattleAttibutePlus((float) attibute, DragonKnightAttibute.Type.Defense, DragonKnightAttibute.Type.Armor), DragonKnightAttibute.Type.Defense);
    }
  }

  public int DragonKnightOutBattleHealthAttribute
  {
    get
    {
      DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
      int attibute = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Constitution);
      return this.AddPercent((float) dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Health) + this.GetOutBattleAttibutePlus((float) attibute, DragonKnightAttibute.Type.Health, DragonKnightAttibute.Type.Constitution), DragonKnightAttibute.Type.Health);
    }
  }

  public long DragonKnightOutBattleManaAttribute
  {
    get
    {
      return (long) DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L).GetAttibute(DragonKnightAttibute.Type.MP);
    }
  }
}
