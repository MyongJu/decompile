﻿// Decompiled with JetBrains decompiler
// Type: DragonRenamePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Text;
using UI;

public class DragonRenamePopup : Popup
{
  private const string CHANGE_NAME_SHOPITEM_ID = "shopitem_dragon_rename";
  private const string ICON_PATH = "Texture/ItemIcons/item_dragon_rename";
  public UILabel mPropsOwnedValue;
  public UILabel mCharsRemainingValue;
  public UIInput mCityNameInput;
  public UILabel mUseBtnValue;
  public UILabel mGetAndUseBtnValue;
  public UILabel mCityNameInputLabel;
  public UIButton mUseBtn;
  public UIButton mGetAndUseBtn;
  public UISprite mStatusSprite;
  public UITexture icon;
  private int _itemsAvailable;

  public void SetDetails()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, "Texture/ItemIcons/item_dragon_rename", (System.Action<bool>) null, true, false, string.Empty);
    this.mCityNameInput.value = DBManager.inst.DB_Dragon.Datas[PlayerData.inst.uid].Name;
    this._itemsAvailable = ItemBag.Instance.GetItemCountByShopID("shopitem_dragon_rename");
    int shopItemPrice = ItemBag.Instance.GetShopItemPrice("shopitem_dragon_rename");
    this.mPropsOwnedValue.text = ScriptLocalization.Get("id_own_colon", true) + "[aaaaaa]" + this._itemsAvailable.ToString();
    if (this._itemsAvailable > 0)
    {
      this.mUseBtn.gameObject.SetActive(true);
      this.mGetAndUseBtn.gameObject.SetActive(false);
    }
    else
    {
      this.mUseBtn.gameObject.SetActive(false);
      this.mGetAndUseBtn.gameObject.SetActive(true);
    }
    this.mGetAndUseBtnValue.text = Utils.FormatThousands(shopItemPrice.ToString());
    this.mCharsRemainingValue.text = string.Format("{0}/15", (object) Encoding.UTF8.GetBytes(this.mCityNameInputLabel.text).Length);
    this.mStatusSprite.spriteName = "red_cross";
    this.mUseBtn.isEnabled = false;
    this.mGetAndUseBtn.isEnabled = false;
  }

  public void OnCloseBtn()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnInputChanged()
  {
    if (this.CheckName() && this.mCityNameInput.value != DBManager.inst.DB_Dragon.Datas[PlayerData.inst.uid].Name)
    {
      this.mStatusSprite.spriteName = "green_tick";
      this.mUseBtn.isEnabled = true;
      this.mGetAndUseBtn.isEnabled = true;
    }
    else
    {
      this.mStatusSprite.spriteName = "red_cross";
      this.mUseBtn.isEnabled = false;
      this.mGetAndUseBtn.isEnabled = false;
    }
    string text = this.mCityNameInputLabel.text;
    int num = 0;
    if (text.Length != 0)
      num = Encoding.UTF8.GetBytes(text).Length;
    this.mCharsRemainingValue.text = string.Format("{0}/15", (object) num);
  }

  private bool CheckName()
  {
    string text = this.mCityNameInputLabel.text;
    if (text.Length == 0)
      return false;
    byte[] bytes = Encoding.UTF8.GetBytes(text);
    return bytes.Length >= 3 && bytes.Length <= 15;
  }

  public void OnUseBtnPressed()
  {
    if (IllegalWordsUtils.WarningIllegalWords(this.mCityNameInput.value))
      return;
    ItemBag.Instance.UseItemByShopItemID("shopitem_dragon_rename", Utils.Hash((object) "buy", (object) false, (object) "name", (object) this.mCityNameInput.value), (System.Action<bool, object>) ((_param1, _param2) => this.OnCloseBtn()), 1);
  }

  public void OnGetAndUseBtnPressed()
  {
    if (IllegalWordsUtils.WarningIllegalWords(this.mCityNameInput.value))
      return;
    if (ItemBag.Instance.GetShopItemPrice("shopitem_dragon_rename") > PlayerData.inst.hostPlayer.Currency)
    {
      Utils.ShowNotEnoughGoldTip();
      this.OnCloseBtn();
    }
    else
      ItemBag.Instance.BuyAndUseShopItem("shopitem_dragon_rename", Utils.Hash((object) "buy", (object) true, (object) "name", (object) this.mCityNameInput.value), (System.Action<bool, object>) ((arg1, arg2) => this.OnCloseBtn()), 1);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.SetDetails();
  }
}
