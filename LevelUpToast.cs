﻿// Decompiled with JetBrains decompiler
// Type: LevelUpToast
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;

public class LevelUpToast : ToastDialog
{
  private const string vfxPath = "Prefab/VFX/exp01";
  public UILabel content;

  private void Start()
  {
  }

  private void Update()
  {
  }

  public override void SeedData(object args)
  {
    this.content.text = args as string;
  }

  public override void Play()
  {
    this.isplaying = true;
    this.StartCoroutine(this.Animation());
  }

  [DebuggerHidden]
  private IEnumerator Animation()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LevelUpToast.\u003CAnimation\u003Ec__Iterator9F()
    {
      \u003C\u003Ef__this = this
    };
  }
}
