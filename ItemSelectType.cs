﻿// Decompiled with JetBrains decompiler
// Type: ItemSelectType
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ItemSelectType : UI.Dialog
{
  public GameObject itemTemp;
  public UIScrollView itemScroll;
  public UITable itemTable;
  public UILabel labelType;
  public UIToggle toggleInventory;
  public UIToggle toggleStore;
  public ItemBag.ItemType currentType;
  private bool _isBuying;

  public bool IsBuying
  {
    get
    {
      return this._isBuying;
    }
    set
    {
      this._isBuying = value;
    }
  }

  private void Start()
  {
  }

  private void Update()
  {
  }

  public void ShowFilteredResult()
  {
  }
}
