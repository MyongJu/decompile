﻿// Decompiled with JetBrains decompiler
// Type: RtmCallback
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Connection;
using Rtm.Rpc;
using System.Collections;
using System.Collections.Generic;

public class RtmCallback
{
  public static int LastTimeMark = -1;
  private static ObjectPool<RtmCallback.Rtm_Auth> _rtm_auth_pool = new ObjectPool<RtmCallback.Rtm_Auth>();
  private static ObjectPool<RtmCallback.Rtm_Send> _rtm_send_pool = new ObjectPool<RtmCallback.Rtm_Send>();
  private static ObjectPool<RtmCallback.Rtm_Ping> _rtm_ping_pool = new ObjectPool<RtmCallback.Rtm_Ping>();
  private static ObjectPool<RtmCallback.Rtm_SendGroup> _rtm_sendgroup_pool = new ObjectPool<RtmCallback.Rtm_SendGroup>();
  private static ObjectPool<RtmCallback.Rtm_HistoryMsg> _rtm_historymsg_pool = new ObjectPool<RtmCallback.Rtm_HistoryMsg>();
  private static ObjectPool<RtmCallback.Rtm_UnreadMsg> _rtm_unreadmsg_pool = new ObjectPool<RtmCallback.Rtm_UnreadMsg>();
  private static ObjectPool<RtmCallback.Rtm_ClearUnread> _rtm_clearunread_pool = new ObjectPool<RtmCallback.Rtm_ClearUnread>();
  private static ObjectPool<RtmCallback.Rtm_CheckUnread> _rtm_checkunread_pool = new ObjectPool<RtmCallback.Rtm_CheckUnread>();
  private static ObjectPool<RtmCallback.Rtm_GetOnlineUsers> _rtm_getOnlineUsers_pool = new ObjectPool<RtmCallback.Rtm_GetOnlineUsers>();

  public static RtmClientCallback PopRtm_Auth(System.Action<bool> callback)
  {
    RtmCallback.Rtm_Auth rtmAuth = RtmCallback._rtm_auth_pool.Allocate();
    rtmAuth.Callback = callback;
    return (RtmClientCallback) rtmAuth;
  }

  public static RtmClientCallback PopRtm_Send(System.Action<bool> callback)
  {
    RtmCallback.Rtm_Send rtmSend = RtmCallback._rtm_send_pool.Allocate();
    rtmSend.Callback = callback;
    return (RtmClientCallback) rtmSend;
  }

  public static RtmClientCallback PopRtm_SendGroup(System.Action<bool> callback)
  {
    RtmCallback.Rtm_SendGroup rtmSendGroup = RtmCallback._rtm_sendgroup_pool.Allocate();
    rtmSendGroup.Callback = callback;
    return (RtmClientCallback) rtmSendGroup;
  }

  public static RtmClientCallback PopRtm_Ping()
  {
    return (RtmClientCallback) RtmCallback._rtm_ping_pool.Allocate();
  }

  public static RtmClientCallback PopRtm_ClearUnread()
  {
    return (RtmClientCallback) RtmCallback._rtm_clearunread_pool.Allocate();
  }

  public static RtmClientCallback PopRtm_CheckUnread(System.Action<List<MsgParam>> callback)
  {
    RtmCallback.Rtm_CheckUnread rtmCheckUnread = RtmCallback._rtm_checkunread_pool.Allocate();
    rtmCheckUnread.Callback = callback;
    return (RtmClientCallback) rtmCheckUnread;
  }

  public static RtmClientCallback PopRtm_UnreadMsg()
  {
    return (RtmClientCallback) RtmCallback._rtm_unreadmsg_pool.Allocate();
  }

  public static RtmClientCallback PopRtm_HistoryMsg(System.Action<MsgResult> callback)
  {
    RtmCallback.Rtm_HistoryMsg rtmHistoryMsg = RtmCallback._rtm_historymsg_pool.Allocate();
    rtmHistoryMsg.Callback = callback;
    return (RtmClientCallback) rtmHistoryMsg;
  }

  public static RtmClientCallback PopRtm_GetOnlineUsers(System.Action<List<long>> callback)
  {
    RtmCallback.Rtm_GetOnlineUsers rtmGetOnlineUsers = RtmCallback._rtm_getOnlineUsers_pool.Allocate();
    rtmGetOnlineUsers.Callback = callback;
    return (RtmClientCallback) rtmGetOnlineUsers;
  }

  public static void OnMessage(string message)
  {
    if (message == null)
      return;
    Hashtable hashtable = Utils.Json2Object(message, true) as Hashtable;
    if (hashtable == null)
      return;
    MessageInfo msgInfo = MessageHub.inst.PopMsgInfo();
    msgInfo.Message = message;
    if (hashtable.ContainsKey((object) "cmd"))
      msgInfo.Action = hashtable[(object) "cmd"].ToString();
    if (hashtable.ContainsKey((object) "time") && hashtable[(object) "time"] != null)
      msgInfo.TimeStamp = long.Parse(hashtable[(object) "time"].ToString());
    if (hashtable.ContainsKey((object) "data"))
      msgInfo.Data = hashtable[(object) "data"];
    if (hashtable.ContainsKey((object) "payload"))
      msgInfo.Payload = hashtable[(object) "payload"];
    MessageHub.inst.OnMessage(msgInfo);
  }

  public class Rtm_Process : RtmClientProcessor
  {
    private System.Action _connection_closed_callback;

    public Rtm_Process(System.Action func)
    {
      this._connection_closed_callback = func;
    }

    public override void EventConnectionClosed()
    {
      MessageHub.inst.OnError("RTM", nameof (EventConnectionClosed));
      if (this._connection_closed_callback == null)
        return;
      this._connection_closed_callback();
    }

    public override void KickedOut()
    {
      PushManager.inst.NotifyKickedOut();
    }

    public override void RecvMessage(long from, byte mtype, string message, long mid, int time)
    {
      if ((int) mtype == 120 || !MessageFilter.CanDispatch(from, mid))
        return;
      if ((int) mtype == 90)
        RtmPushSystem._lastPushMsgContent = message;
      RtmCallback.OnMessage(message);
    }

    public override void RecvMessageGroup(long group_id, long from, byte mtype, string message, long mid, int time)
    {
      if ((int) mtype == 120 || !MessageFilter.CanDispatch(from, mid))
        return;
      RtmCallback.OnMessage(message);
    }

    public override void RecvMessageBroadcast(long from, byte mtype, string message, long mid, int time)
    {
      if ((int) mtype == 120 || !MessageFilter.CanDispatch(from, mid))
        return;
      RtmCallback.OnMessage(message);
    }
  }

  public class Rtm_Auth : RtmClientCallback
  {
    private System.Action<bool> _auth_callback;

    public override void AuthCallback(bool result)
    {
      if (this._auth_callback != null)
        this._auth_callback(result);
      this._auth_callback = (System.Action<bool>) null;
      RtmCallback._rtm_auth_pool.Release(this);
    }

    public override void AuthException(RtmClientException e)
    {
      MessageHub.inst.OnError("RTM", string.Format("AuthException: {0}, {1}", (object) e.Code.ToString(), (object) e.Reason));
      bool flag = e.Code.ToString() == "8601";
      if (this._auth_callback != null)
        this._auth_callback(flag);
      this._auth_callback = (System.Action<bool>) null;
      RtmCallback._rtm_auth_pool.Release(this);
    }

    public System.Action<bool> Callback
    {
      set
      {
        this._auth_callback = value;
      }
    }
  }

  public class Rtm_Send : RtmClientCallback
  {
    private System.Action<bool> _send_callback;

    public override void SendCallback(int result)
    {
      if (this._send_callback != null)
        this._send_callback(true);
      this._send_callback = (System.Action<bool>) null;
      RtmCallback._rtm_send_pool.Release(this);
    }

    public override void SendException(RtmClientException e)
    {
      MessageHub.inst.OnError("RTM", string.Format("SendException: {0}, {1}", (object) e.Code.ToString(), (object) e.Reason));
      if (this._send_callback != null)
        this._send_callback(false);
      this._send_callback = (System.Action<bool>) null;
      RtmCallback._rtm_send_pool.Release(this);
    }

    public System.Action<bool> Callback
    {
      set
      {
        this._send_callback = value;
      }
    }
  }

  public class Rtm_SendGroup : RtmClientCallback
  {
    private System.Action<bool> _sendgroup_callback;

    public override void SendGroupCallback(int result)
    {
      if (this._sendgroup_callback != null)
        this._sendgroup_callback(true);
      this._sendgroup_callback = (System.Action<bool>) null;
      RtmCallback._rtm_sendgroup_pool.Release(this);
    }

    public override void SendGroupException(RtmClientException e)
    {
      MessageHub.inst.OnError("RTM", string.Format("SendGroupException: {0}, {1}", (object) e.Code.ToString(), (object) e.Reason));
      if (this._sendgroup_callback != null)
        this._sendgroup_callback(false);
      this._sendgroup_callback = (System.Action<bool>) null;
      RtmCallback._rtm_sendgroup_pool.Release(this);
    }

    public System.Action<bool> Callback
    {
      set
      {
        this._sendgroup_callback = value;
      }
    }
  }

  public class Rtm_Ping : RtmClientCallback
  {
    public override void PingCallback()
    {
      RtmCallback._rtm_ping_pool.Release(this);
    }

    public override void PingException(RtmClientException e)
    {
      MessageHub.inst.OnError("RTM", string.Format("PingException: {0}, {1}", (object) e.Code.ToString(), (object) e.Reason));
      RtmCallback._rtm_ping_pool.Release(this);
    }
  }

  public class Rtm_CheckUnread : RtmClientCallback
  {
    private System.Action<List<MsgParam>> _checkunread_callback;

    public override void CheckUnreadCallback(List<MsgNum> result)
    {
      List<MsgParam> msgParamList = new List<MsgParam>(result.Count);
      for (int index = 0; index < result.Count; ++index)
        msgParamList.Add(new MsgParam()
        {
          From_xid = result[index].From_xid,
          Msg_type = result[index].Msg_type,
          Num = result[index].Num,
          Offset = -1L
        });
      if (this._checkunread_callback != null)
        this._checkunread_callback(msgParamList);
      this._checkunread_callback = (System.Action<List<MsgParam>>) null;
      RtmCallback._rtm_checkunread_pool.Release(this);
    }

    public override void CheckUnreadException(RtmClientException e)
    {
      MessageHub.inst.OnError("RTM", string.Format("CheckUnreadException: {0}, {1}", (object) e.Code.ToString(), (object) e.Reason));
      this._checkunread_callback = (System.Action<List<MsgParam>>) null;
      RtmCallback._rtm_checkunread_pool.Release(this);
    }

    public System.Action<List<MsgParam>> Callback
    {
      set
      {
        this._checkunread_callback = value;
      }
    }
  }

  public class Rtm_ClearUnread : RtmClientCallback
  {
    public override void CheckUnreadCallback(List<MsgNum> result)
    {
      RtmCallback._rtm_clearunread_pool.Release(this);
    }

    public override void CheckUnreadException(RtmClientException e)
    {
      MessageHub.inst.OnError("RTM", string.Format("ClearUnreadCallback: {0}, {1}", (object) e.Code.ToString(), (object) e.Reason));
      RtmCallback._rtm_clearunread_pool.Release(this);
    }
  }

  public class Rtm_UnreadMsg : RtmClientCallback
  {
    public override void HistoryMsgCallback(MsgResult result)
    {
      for (int index = 0; index < result.Msgs.Count; ++index)
      {
        MsgContent msg = result.Msgs[index];
        if ((int) msg.Mtype != 120)
          RtmCallback.OnMessage(msg.Body);
      }
      RtmCallback._rtm_unreadmsg_pool.Release(this);
    }

    public override void HistoryMsgException(RtmClientException e)
    {
      MessageHub.inst.OnError("RTM", string.Format("UnreadyMsgException: {0}, {1}", (object) e.Code.ToString(), (object) e.Reason));
      RtmCallback._rtm_unreadmsg_pool.Release(this);
    }
  }

  public class Rtm_HistoryMsg : RtmClientCallback
  {
    private System.Action<MsgResult> _hostory_msg_callback;

    public System.Action<MsgResult> Callback
    {
      set
      {
        this._hostory_msg_callback = value;
      }
    }

    public override void HistoryMsgCallback(MsgResult result)
    {
      MessageHub.inst.PushHistoryResult(new MessageHub.HistoryMsgReslut()
      {
        callback = this._hostory_msg_callback,
        param = result
      });
      this._hostory_msg_callback = (System.Action<MsgResult>) null;
      RtmCallback._rtm_historymsg_pool.Release(this);
    }

    public override void HistoryMsgNewCallback(MsgResult result)
    {
      MessageHub.inst.PushHistoryResult(new MessageHub.HistoryMsgReslut()
      {
        callback = this._hostory_msg_callback,
        param = result
      });
      this._hostory_msg_callback = (System.Action<MsgResult>) null;
      RtmCallback._rtm_historymsg_pool.Release(this);
    }

    public override void HistoryMsgP2PCallback(MsgResult result)
    {
      MessageHub.inst.PushHistoryResult(new MessageHub.HistoryMsgReslut()
      {
        callback = this._hostory_msg_callback,
        param = result
      });
      this._hostory_msg_callback = (System.Action<MsgResult>) null;
      RtmCallback._rtm_historymsg_pool.Release(this);
    }

    public override void HistoryMsgException(RtmClientException e)
    {
      MessageHub.inst.OnError("RTM", string.Format("HistoryMsgException: {0}, {1}", (object) e.Code.ToString(), (object) e.Reason));
      this._hostory_msg_callback = (System.Action<MsgResult>) null;
      RtmCallback._rtm_historymsg_pool.Release(this);
    }

    public override void HistoryMsgNewException(RtmClientException e)
    {
      MessageHub.inst.OnError("RTM", string.Format("HistoryMsgNewException: {0}, {1}", (object) e.Code.ToString(), (object) e.Reason));
      this._hostory_msg_callback = (System.Action<MsgResult>) null;
      RtmCallback._rtm_historymsg_pool.Release(this);
    }

    public override void HistoryMsgP2PException(RtmClientException e)
    {
      MessageHub.inst.OnError("RTM", string.Format("HistoryMsgP2PException: {0}, {1}", (object) e.Code.ToString(), (object) e.Reason));
      this._hostory_msg_callback = (System.Action<MsgResult>) null;
      RtmCallback._rtm_historymsg_pool.Release(this);
    }
  }

  public class Rtm_GetOnlineUsers : RtmClientCallback
  {
    private System.Action<List<long>> _getOnlineUsers_callback;

    public System.Action<List<long>> Callback
    {
      set
      {
        this._getOnlineUsers_callback = value;
      }
    }

    public override void GetOnlineUsersCallback(List<long> result)
    {
      if (this._getOnlineUsers_callback != null)
        this._getOnlineUsers_callback(result);
      this._getOnlineUsers_callback = (System.Action<List<long>>) null;
      RtmCallback._rtm_getOnlineUsers_pool.Release(this);
    }

    public override void GetOnlineUsersException(RtmClientException e)
    {
      MessageHub.inst.OnError("RTM", string.Format("GetOnlineUsersException: {0}, {1}", (object) e.Code.ToString(), (object) e.Reason));
      RtmCallback._rtm_getOnlineUsers_pool.Release(this);
    }
  }
}
