﻿// Decompiled with JetBrains decompiler
// Type: IllegalWordDetection
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

public class IllegalWordDetection
{
  private static HashSet<string> wordsSet = new HashSet<string>();
  private static byte[] fastCheck = new byte[(int) ushort.MaxValue];
  private static byte[] fastLength = new byte[(int) ushort.MaxValue];
  private static byte[] startCache = new byte[(int) ushort.MaxValue];
  private static char[] dectectedBuffer = (char[]) null;
  private static string SkipList = " \t\r\n~!@#$%^&*()_+-=【】、{}|;:\"，。、《》？αβγδεζηθικλμνξοπρστυφχψωΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ。，、；：？！…—·ˉ¨‘’“”々～‖∶＂＇｀｜〃〔〕〈〉《》「」『』．〖〗【】（）［］｛｝ⅠⅡⅢⅣⅤⅥⅦⅧⅨⅩⅪⅫ\x2488\x2489\x248A\x248B\x248C\x248D\x248E\x248F\x2490\x2491\x2492\x2493\x2494\x2495\x2496\x2497\x2498\x2499\x249A\x249B\x3220\x3221\x3222\x3223\x3224\x3225\x3226\x3227\x3228\x3229\x2460\x2461\x2462\x2463\x2464\x2465\x2466\x2467\x2468\x2469\x2474\x2475\x2476\x2477\x2478\x2479\x247A\x247B\x247C\x247D\x247E\x247F\x2480\x2481\x2482\x2483\x2484\x2485\x2486\x2487≈≡≠＝≤≥＜＞≮≯∷±＋－×÷／∫∮∝∞∧∨∑∏∪∩∈∵∴⊥∥∠⌒⊙≌∽√§№☆★○●◎◇◆□℃‰€■△▲※→←↑↓〓¤°＃＆＠＼︿＿￣―♂♀┌┍┎┐┑┒┓─┄┈├┝┞┟┠┡┢┣│┆┊┬┭┮┯┰┱┲┳┼┽┾┿╀╁╂╃└┕┖┗┘┙┚┛━┅┉┤┥┦┧┨┩┪┫┃┇┋┴┵┶┷┸┹┺┻╋╊╉╈╇╆╅╄";
  private static BitArray SkipBitArray = new BitArray((int) ushort.MaxValue);
  private static BitArray endCache = new BitArray((int) ushort.MaxValue);

  public static unsafe void Init(string[] badwords)
  {
    IllegalWordDetection.wordsSet.Clear();
    IllegalWordDetection.dectectedBuffer = new char[1];
    if (badwords == null || badwords.Length == 0)
      return;
    int val2 = int.MinValue;
    int index1 = 0;
    for (int length1 = badwords.Length; index1 < length1; ++index1)
    {
      int length2 = badwords[index1].Length;
      if (length2 > 0)
      {
        val2 = Math.Max(length2, val2);
        for (int index2 = 0; index2 < length2; ++index2)
        {
          if (index2 < 7)
            IllegalWordDetection.fastCheck[(int) badwords[index1][index2]] |= (byte) (1 << index2);
          else
            IllegalWordDetection.fastCheck[(int) badwords[index1][index2]] |= (byte) 128;
        }
        int num = Math.Min(8, length2);
        IllegalWordDetection.fastLength[(int) badwords[index1][0]] |= (byte) (1 << num - 1);
        if ((int) IllegalWordDetection.startCache[(int) badwords[index1][0]] < num)
          IllegalWordDetection.startCache[(int) badwords[index1][0]] = (byte) num;
        IllegalWordDetection.endCache[(int) badwords[index1][length2 - 1]] = true;
        if (!IllegalWordDetection.wordsSet.Contains(badwords[index1]))
          IllegalWordDetection.wordsSet.Add(badwords[index1]);
      }
    }
    IllegalWordDetection.dectectedBuffer = new char[val2];
    string str = IllegalWordDetection.SkipList;
    char* chPtr1 = (char*) ((IntPtr) str + RuntimeHelpers.OffsetToStringData);
    char* chPtr2 = chPtr1;
    char* chPtr3 = chPtr1 + IllegalWordDetection.SkipList.Length;
    while (chPtr2 < chPtr3)
      IllegalWordDetection.SkipBitArray[(int) *chPtr2++] = true;
    str = (string) null;
  }

  public static unsafe string Filter(string text, string mask = "*")
  {
    Dictionary<int, int> dictionary = IllegalWordDetection.DetectIllegalWords(text);
    if (dictionary.Count == 0)
      return text;
    string str1 = text;
    char* chPtr1 = (char*) ((IntPtr) str1 + RuntimeHelpers.OffsetToStringData);
    string str2 = mask;
    char* chPtr2 = (char*) ((IntPtr) str2 + RuntimeHelpers.OffsetToStringData);
    using (Dictionary<int, int>.Enumerator enumerator = dictionary.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, int> current = enumerator.Current;
        char* chPtr3 = chPtr1 + current.Key;
        for (int index = 0; index < current.Value; ++index)
          *chPtr3++ = *chPtr2;
      }
    }
    str1 = (string) null;
    str2 = (string) null;
    return text;
  }

  public static unsafe Dictionary<int, int> DetectIllegalWords(string text)
  {
    Dictionary<int, int> dictionary = new Dictionary<int, int>();
    if (string.IsNullOrEmpty(text))
      return dictionary;
    string str = text;
    char* chPtr1 = (char*) ((IntPtr) str + RuntimeHelpers.OffsetToStringData);
    char* chPtr2 = IllegalWordDetection.dectectedBuffer == null || IllegalWordDetection.dectectedBuffer.Length == 0 ? (char*) null : &IllegalWordDetection.dectectedBuffer[0];
    char* chPtr3 = ((int) IllegalWordDetection.fastCheck[(int) *chPtr1] & 1) != 0 ? chPtr1 : (char*) ((IntPtr) chPtr1 + 2);
    for (char* chPtr4 = chPtr1 + text.Length; chPtr3 < chPtr4; ++chPtr3)
    {
      if (((int) IllegalWordDetection.fastCheck[(int) *chPtr3] & 1) == 0)
      {
        while ((UIntPtr) chPtr3 < (UIntPtr) chPtr4 - new UIntPtr(2) && ((int) IllegalWordDetection.fastCheck[(int) *++chPtr3] & 1) == 0)
          ;
      }
      if ((int) IllegalWordDetection.startCache[(int) *chPtr3] != 0 && ((int) IllegalWordDetection.fastLength[(int) *chPtr3] & 1) > 0)
        dictionary.Add((int) (chPtr3 - chPtr1), 1);
      char* chPtr5 = chPtr2;
      IntPtr num1 = new IntPtr(2);
      char* chPtr6 = (char*) ((IntPtr) chPtr5 + num1);
      int num2 = (int) *chPtr3;
      *chPtr5 = (char) num2;
      int num3 = (int) (chPtr4 - chPtr3 - 1L);
      int num4 = 0;
      for (int index1 = 1; index1 <= num3; ++index1)
      {
        char* chPtr7 = chPtr3 + index1;
        if (IllegalWordDetection.SkipBitArray[(int) *chPtr7])
          ++num4;
        else if ((int) IllegalWordDetection.fastCheck[(int) *chPtr7] >> Math.Min(index1 - num4, 7) != 0)
        {
          *chPtr6++ = *chPtr7;
          if ((int) IllegalWordDetection.fastLength[(int) *chPtr3] >> Math.Min(index1 - 1 - num4, 7) > 0 && IllegalWordDetection.endCache[(int) *chPtr7])
          {
            if (IllegalWordDetection.wordsSet.Contains(new string(IllegalWordDetection.dectectedBuffer, 0, (int) (chPtr6 - chPtr2))))
            {
              int index2 = (int) (chPtr3 - chPtr1);
              dictionary[index2] = index1 + 1;
              chPtr3 = chPtr7;
              break;
            }
          }
          else if (index1 - num4 > (int) IllegalWordDetection.startCache[(int) *chPtr3] && (int) IllegalWordDetection.startCache[(int) *chPtr3] < 128)
            break;
        }
        else
          break;
      }
    }
    str = (string) null;
    chPtr2 = (char*) null;
    return dictionary;
  }
}
