﻿// Decompiled with JetBrains decompiler
// Type: PathTimeCalaculater
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PathTimeCalaculater
{
  public static Vector2[] vertexs = new Vector2[4]
  {
    new Vector2(704f, 1278f),
    new Vector2(640f, 1214f),
    new Vector2(576f, 1278f),
    new Vector2(640f, 1342f)
  };
  private static int[] startPoints = new int[4]
  {
    0,
    1,
    2,
    3
  };
  private static int[] endPoints = new int[4]{ 1, 2, 3, 0 };
  public const int MIN_X = 576;
  public const int MAX_X = 704;
  public const int MIN_Y = 1214;
  public const int MAX_Y = 1342;
  public const int MID_X = 640;
  public const int MID_Y = 1278;
  private const int LINE_COUNT = 4;

  public static float[] CalcPathPercent(int s1x, int s1y, int e2x, int e2y)
  {
    float[] numArray = new float[3];
    numArray[0] = numArray[1] = numArray[2] = 1f;
    Vector2 p1 = new Vector2((float) s1x, (float) s1y);
    Vector2 p2 = new Vector2((float) e2x, (float) e2y);
    if (!PathTimeCalaculater.AreaCheck(p1, p2))
      return numArray;
    Vector2[] vector2Array = new Vector2[2];
    int num1 = 0;
    for (int index = 0; index < 4; ++index)
    {
      Vector2 p = PathTimeCalaculater.vertexs[PathTimeCalaculater.startPoints[index]];
      Vector2 vector2 = PathTimeCalaculater.vertexs[PathTimeCalaculater.endPoints[index]];
      if (PathTimeCalaculater.CrossCheck(p - p1, p - p2, p1 - p, p1 - vector2, p2 - p1, vector2 - p))
      {
        vector2Array[num1++] = PathTimeCalaculater.GetPoint(p1, p2, PathTimeCalaculater.vertexs[PathTimeCalaculater.startPoints[index]], PathTimeCalaculater.vertexs[PathTimeCalaculater.endPoints[index]]);
        if (num1 >= 2)
          break;
      }
      else if (PathTimeCalaculater.isPointOnLine(p, p1, p2))
        vector2Array[num1++] = p;
    }
    switch (num1)
    {
      case 1:
        numArray[0] = (double) p1.x == (double) p2.x ? (float) (((double) vector2Array[0].y - (double) p1.y) / ((double) p2.y - (double) p1.y)) : (float) (((double) vector2Array[0].x - (double) p1.x) / ((double) p2.x - (double) p1.x));
        numArray[1] = 1f;
        numArray[2] = 1f;
        break;
      case 2:
        if ((double) p1.x != (double) p2.x)
        {
          float num2 = (float) (1.0 / ((double) p2.x - (double) p1.x));
          if ((double) Mathf.Abs(vector2Array[0].x - p1.x) < (double) Mathf.Abs(vector2Array[1].x - p1.x))
          {
            numArray[0] = (vector2Array[0].x - p1.x) * num2;
            numArray[1] = (vector2Array[1].x - p1.x) * num2;
          }
          else
          {
            numArray[0] = (vector2Array[1].x - p1.x) * num2;
            numArray[1] = (vector2Array[0].x - p1.x) * num2;
          }
        }
        else
        {
          float num2 = (float) (1.0 / ((double) p2.y - (double) p1.y));
          if ((double) Mathf.Abs(vector2Array[0].y - p1.y) < (double) Mathf.Abs(vector2Array[1].y - p1.y))
          {
            numArray[0] = (vector2Array[0].y - p1.y) * num2;
            numArray[1] = (vector2Array[1].y - p1.y) * num2;
          }
          else
          {
            numArray[0] = (vector2Array[1].y - p1.y) * num2;
            numArray[1] = (vector2Array[0].y - p1.y) * num2;
          }
        }
        numArray[2] = 1f;
        break;
    }
    return numArray;
  }

  private static bool AreaCheck(Vector2 p1, Vector2 p2)
  {
    if ((double) Mathf.Min(p1.x, p2.x) < 704.0 && (double) Mathf.Max(p1.x, p2.x) > 576.0 && (double) Mathf.Min(p1.y, p2.y) < 1342.0)
      return (double) Mathf.Max(p1.y, p2.y) > 1214.0;
    return false;
  }

  private static bool isPointOnLine(Vector2 p, Vector2 p1, Vector2 p2)
  {
    if (PathTimeCalaculater.VectorCross(p - p1, p - p2) == 0.0)
      return PathTimeCalaculater.InArea(p, Mathf.Min(p1.x, p2.x), Mathf.Max(p1.x, p2.x), Mathf.Min(p1.y, p2.y), Mathf.Max(p1.y, p2.y));
    return false;
  }

  private static bool InArea(Vector2 p, float left, float right, float top, float bottom)
  {
    if ((double) p.x > (double) left && (double) p.x < (double) right && (double) p.y > (double) top)
      return (double) p.y < (double) bottom;
    return false;
  }

  private static bool CrossCheck(Vector2 v1, Vector2 v2, Vector2 v3, Vector2 v4, Vector2 vp, Vector2 vq)
  {
    if (PathTimeCalaculater.CheckAngle(v1, v2, vq) < 0.0)
      return PathTimeCalaculater.CheckAngle(v3, v4, vp) < 0.0;
    return false;
  }

  public static Vector2 GetPoint(Vector2 p1, Vector2 p2, Vector2 q1, Vector2 q2)
  {
    double x1 = (double) p1.x;
    double y1 = (double) p1.y;
    double x2 = (double) p2.x;
    double y2 = (double) p2.y;
    double x3 = (double) q1.x;
    double y3 = (double) q1.y;
    double x4 = (double) q2.x;
    double y4 = (double) q2.y;
    double a11 = x2 - x1;
    double num1 = y2 - y1;
    double a21 = x4 - x3;
    double num2 = y4 - y3;
    double a12 = (y2 - y1) * x1 + (x1 - x2) * y1;
    double a22 = (y4 - y3) * x3 + (x3 - x4) * y3;
    double determinantValue1 = PathTimeCalaculater.GetDeterminantValue(a11, num1, a21, num2);
    double determinantValue2 = PathTimeCalaculater.GetDeterminantValue(a11, a12, a21, a22);
    double determinantValue3 = PathTimeCalaculater.GetDeterminantValue(num1, a12, num2, a22);
    return new Vector2((float) (determinantValue2 / determinantValue1), (float) (determinantValue3 / determinantValue1));
  }

  private static double CheckAngle(Vector2 v1, Vector2 v2, Vector2 v0)
  {
    return PathTimeCalaculater.VectorCross(v1, v0) * PathTimeCalaculater.VectorCross(v2, v0);
  }

  private static double VectorCross(Vector2 v1, Vector2 v2)
  {
    return (double) v1.x * (double) v2.y - (double) v1.y * (double) v2.x;
  }

  private static double GetDeterminantValue(double a11, double a12, double a21, double a22)
  {
    return a11 * a22 - a12 * a21;
  }
}
