﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsRewardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTrialsRewardPopup : MerlinTrialsRewardBasePopup
{
  [SerializeField]
  private UILabel _rewardDescription;
  private int _dayOfWeek;
  private int _challengeCount;
  private MerlinTrialsMainInfo _merlinMainInfo;
  private System.Action _onRewardClaimedSuccess;

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
    MerlinTrialsRewardPopup.Parameter parameter = orgParam as MerlinTrialsRewardPopup.Parameter;
    if (parameter == null)
      return;
    this._dayOfWeek = parameter.dayOfWeek;
    this._challengeCount = parameter.challengeCount;
    this._merlinMainInfo = parameter.merlinMainInfo;
    this._onRewardClaimedSuccess = parameter.onRewardClaimedSuccess;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnCollectPressed()
  {
    if (this._merlinMainInfo == null)
      return;
    MessageHub.inst.GetPortByAction("Merlin:claimDailyReward").SendRequest(new Hashtable()
    {
      {
        (object) "battleId",
        (object) this._merlinMainInfo.internalId
      },
      {
        (object) "day",
        (object) this._dayOfWeek
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.ShowCollectEffect();
      this.OnClosePressed();
      MerlinTrialsHelper.inst.UpdateDailyData(this._dayOfWeek, true);
      if (this._onRewardClaimedSuccess == null)
        return;
      this._onRewardClaimedSuccess();
    }), true);
  }

  protected override int GetRewardId()
  {
    if (this._merlinMainInfo != null)
      return this._merlinMainInfo.dailyRewardId;
    return 0;
  }

  private void UpdateUI()
  {
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("0", MerlinTrialsHelper.inst.GetDayString(this._dayOfWeek));
    para.Add("2", this._challengeCount.ToString());
    if (this._merlinMainInfo != null)
    {
      MerlinTrialsGroupInfo merlinTrialsGroupInfo = ConfigManager.inst.DB_MerlinTrialsGroup.Get(this._merlinMainInfo.trialId);
      if (merlinTrialsGroupInfo != null)
        para.Add("1", merlinTrialsGroupInfo.LocName);
    }
    this._rewardDescription.text = ScriptLocalization.GetWithPara("trial_challenge_rewards_final_description", para, true);
  }

  public class Parameter : Popup.PopupParameter
  {
    public MerlinTrialsMainInfo merlinMainInfo;
    public int dayOfWeek;
    public int challengeCount;
    public System.Action onRewardClaimedSuccess;
  }
}
