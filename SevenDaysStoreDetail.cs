﻿// Decompiled with JetBrains decompiler
// Type: SevenDaysStoreDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class SevenDaysStoreDetail : MonoBehaviour
{
  private GameObjectPool pools = new GameObjectPool();
  private GameObjectPool _pool = new GameObjectPool();
  private List<SevenDaysStoreItemRenderer> _items = new List<SevenDaysStoreItemRenderer>();
  private int defaultHeight = 673;
  private int itemHeight = 388;
  public SevenDaysStoreItemRenderer itemPrefab;
  public SevenDaysRewardDetail rewards;
  private bool init;
  public UIScrollView scrollView;
  public UIGrid grid;
  private int _day;
  private bool _init;
  public UISprite background;

  public void SetData(int day)
  {
    if (this._day == day)
      return;
    this._day = day;
    this.Clear();
    SevenDayRewardInfo rewardInfo = ConfigManager.inst.DB_SevenDayRewards.GetRewardInfo(this._day);
    int count = rewardInfo.SaleItemList.Count;
    for (int index = 0; index < count; ++index)
    {
      SevenDaysStoreItemRenderer storeItemRenderer = this.GetItem();
      SaleItemData data = new SaleItemData();
      int saleItem = rewardInfo.SaleItemList[index];
      data.ItemId = saleItem;
      data.MaxCount = rewardInfo.SaleItems[saleItem];
      data.Price = rewardInfo.Prices[saleItem];
      data.Discount = rewardInfo.Discounts[saleItem];
      data.Remain = DBManager.inst.DB_SevenDays.GetSaleItemDataRemain(this._day, saleItem, data.MaxCount);
      storeItemRenderer.SetData(this._day, data);
      NGUITools.SetActive(storeItemRenderer.gameObject, false);
      NGUITools.SetActive(storeItemRenderer.gameObject, true);
    }
    int num = Mathf.RoundToInt((float) (this.itemHeight * Mathf.CeilToInt((float) count * 0.5f))) + 70;
    if (num < this.defaultHeight)
      num = this.defaultHeight;
    this.background.height = num;
    this.grid.Reposition();
    this.scrollView.ResetPosition();
    this.rewards.SetData(day);
  }

  private SevenDaysStoreItemRenderer GetItem()
  {
    if (!this._init)
    {
      this._init = true;
      this._pool.Initialize(this.itemPrefab.gameObject, this.gameObject);
    }
    GameObject go = this._pool.AddChild(this.grid.gameObject);
    SevenDaysStoreItemRenderer component = go.GetComponent<SevenDaysStoreItemRenderer>();
    NGUITools.SetActive(go, true);
    this._items.Add(component);
    return component;
  }

  private void Clear()
  {
    for (int index = 0; index < this._items.Count; ++index)
      this._pool.Release(this._items[index].gameObject);
    this._items.Clear();
  }

  public void Dispose()
  {
    this.Clear();
    this._pool.Clear();
    this._init = false;
    this.rewards.Dispose();
  }

  public void Hide()
  {
  }

  private void Expired()
  {
  }
}
