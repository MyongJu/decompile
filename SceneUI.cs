﻿// Decompiled with JetBrains decompiler
// Type: SceneUI
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SceneUI : MonoBehaviour
{
  public float factor = 100f;
  public float max = 1f;
  public float min = 0.8f;
  public float initScale = 1.7f;
  public float farPos;
  public float nearPos;

  public virtual void Open(Transform parent)
  {
    if (!((Object) null != (Object) parent))
      return;
    this.transform.parent = parent;
    this.transform.localPosition = Vector3.back;
    if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.CityMode)
      this.transform.localPosition = new Vector3(0.0f, 0.0f, -2000f);
    else
      this.transform.localPosition = new Vector3(0.0f, 0.0f, -100f);
    this.transform.localScale = new Vector3(this.initScale, this.initScale, 1f);
    this.gameObject.SetActive(true);
  }

  public virtual void Close()
  {
    this.gameObject.SetActive(false);
  }

  protected void ChangeParentBCSts(bool enbale)
  {
    if (!((Object) null != (Object) this.transform.parent))
      return;
    BoxCollider component = this.transform.parent.GetComponent<BoxCollider>();
    if (!((Object) null != (Object) component))
      return;
    component.enabled = enbale;
  }

  private void Start()
  {
  }

  private void Update()
  {
  }

  protected void ScaleInCity()
  {
  }

  protected void ScaleInKingdom()
  {
  }

  public enum UIOffset
  {
    CityTouchCircleOffset = -10, // -0x0000000A
    CollectionUIOffset = -5,
    CityTimerBarOffset = -1,
  }
}
