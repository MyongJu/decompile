﻿// Decompiled with JetBrains decompiler
// Type: AllianceDragonAltarController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class AllianceDragonAltarController : MonoBehaviour, IRecycle, IConstructionController
{
  private static string[] m_VFXPaths = new string[2]
  {
    "Prefab/VFX/Altar/fx_altar_charging",
    "Prefab/VFX/Altar/fx_altar_interrupt"
  };
  private long[] m_VFXHandles = new long[2]{ -1L, -1L };
  private bool[] m_LastVFX = new bool[2];
  private bool[] m_CurrentVFX = new bool[2];
  public GameObject m_Scaffold;
  public GameObject m_Workers;
  public GameObject m_Dust;

  public void OnInitialize()
  {
  }

  public void OnFinalize()
  {
    this.StopCurrentEffect(AllianceDragonAltarController.VisualEffect.CHARGING);
    this.StopCurrentEffect(AllianceDragonAltarController.VisualEffect.INTERRUPT);
    for (int index = 0; index < 2; ++index)
    {
      this.m_LastVFX[index] = false;
      this.m_CurrentVFX[index] = false;
    }
  }

  private void FigureOut(TileData tile)
  {
    for (int index = 0; index < 2; ++index)
    {
      this.m_LastVFX[index] = this.m_CurrentVFX[index];
      this.m_CurrentVFX[index] = false;
    }
    if (DBManager.inst.DB_AllianceMagic.GetCurrentPowerUpSkillByAllianceId(tile.AllianceID) != null)
    {
      this.m_CurrentVFX[0] = true;
      this.m_CurrentVFX[1] = false;
    }
    AllianceMagicData skillByAllianceId = DBManager.inst.DB_AllianceMagic.GetCurrentInterruptedSkillByAllianceId(tile.AllianceID);
    if (skillByAllianceId == null)
      return;
    skillByAllianceId.Interrupt = false;
    this.m_CurrentVFX[0] = false;
    this.m_CurrentVFX[1] = true;
  }

  public void UpdateUI(TileData tile)
  {
    AllianceTempleData templeData = tile.TempleData;
    if (templeData != null)
    {
      this.UpdateScaffold(templeData);
      this.UpdateWorkers(templeData);
      this.UpdateDust(templeData);
    }
    this.FigureOut(tile);
    for (int index = 0; index < 2; ++index)
    {
      if (this.m_LastVFX[index] && !this.m_CurrentVFX[index])
        this.StopCurrentEffect((AllianceDragonAltarController.VisualEffect) index);
      if (!this.m_LastVFX[index] && this.m_CurrentVFX[index])
        this.PlayEffect((AllianceDragonAltarController.VisualEffect) index, this.transform);
    }
  }

  public void UpdateForWarList(AllianceTempleData templeData)
  {
    this.m_Scaffold.SetActive(false);
    this.m_Workers.SetActive(false);
    this.m_Dust.SetActive(false);
  }

  public void Reset()
  {
    this.m_Scaffold.SetActive(false);
    this.m_Workers.SetActive(false);
    this.m_Dust.SetActive(false);
  }

  private void UpdateScaffold(AllianceTempleData templeData)
  {
    bool flag = false;
    string state = templeData.State;
    if (state != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceDragonAltarController.\u003C\u003Ef__switch\u0024map78 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceDragonAltarController.\u003C\u003Ef__switch\u0024map78 = new Dictionary<string, int>(1)
        {
          {
            "building",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceDragonAltarController.\u003C\u003Ef__switch\u0024map78.TryGetValue(state, out num) && num == 0)
        flag = true;
    }
    this.m_Scaffold.SetActive(flag);
  }

  private void UpdateWorkers(AllianceTempleData templeData)
  {
    bool flag = false;
    string state = templeData.State;
    if (state != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceDragonAltarController.\u003C\u003Ef__switch\u0024map79 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceDragonAltarController.\u003C\u003Ef__switch\u0024map79 = new Dictionary<string, int>(2)
        {
          {
            "building",
            0
          },
          {
            "demolishing",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceDragonAltarController.\u003C\u003Ef__switch\u0024map79.TryGetValue(state, out num) && num == 0)
        flag = true;
    }
    this.m_Workers.SetActive(flag);
  }

  private void UpdateDust(AllianceTempleData templeData)
  {
    bool flag = false;
    string state = templeData.State;
    if (state != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceDragonAltarController.\u003C\u003Ef__switch\u0024map7A == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceDragonAltarController.\u003C\u003Ef__switch\u0024map7A = new Dictionary<string, int>(2)
        {
          {
            "building",
            0
          },
          {
            "demolishing",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceDragonAltarController.\u003C\u003Ef__switch\u0024map7A.TryGetValue(state, out num) && num == 0)
        flag = true;
    }
    this.m_Dust.SetActive(flag);
  }

  public void SetSortingLayerID(int sortingLayerID)
  {
    for (int index = 0; index < this.m_VFXHandles.Length; ++index)
    {
      IVfx vfxByGuid = VfxManager.Instance.GetVfxByGuid(this.m_VFXHandles[index]);
      if (vfxByGuid != null)
      {
        ParticleSortingOrderModifier component = vfxByGuid.gameObject.GetComponent<ParticleSortingOrderModifier>();
        if ((Object) component != (Object) null)
          component.SortingLayerName = SortingLayer.IDToName(sortingLayerID);
      }
    }
  }

  private void PlayEffect(AllianceDragonAltarController.VisualEffect slot, Transform parent)
  {
    this.StopCurrentEffect(slot);
    this.m_VFXHandles[(int) slot] = VfxManager.Instance.CreateAndPlay(AllianceDragonAltarController.m_VFXPaths[(int) slot], parent);
  }

  private void StopCurrentEffect(AllianceDragonAltarController.VisualEffect effect)
  {
    if (this.m_VFXHandles[(int) effect] == -1L)
      return;
    VfxManager.Instance.Delete(this.m_VFXHandles[(int) effect]);
    this.m_VFXHandles[(int) effect] = -1L;
  }

  private enum VisualEffect
  {
    CHARGING,
    INTERRUPT,
    COUNT,
  }
}
