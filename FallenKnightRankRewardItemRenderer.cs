﻿// Decompiled with JetBrains decompiler
// Type: FallenKnightRankRewardItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class FallenKnightRankRewardItemRenderer : MonoBehaviour
{
  private Dictionary<int, FallenKnightRankRewardItemRenderer.RankRewardPair> dicRankReward = new Dictionary<int, FallenKnightRankRewardItemRenderer.RankRewardPair>();
  public UILabel title;
  public UILabel subtitle;
  public FallenKnightRankRewardItemRenderer.IconInfo[] itemIconInfos;
  private FallenKnightRankRewardInfo rankRewardInfo;

  public void SetData(FallenKnightRankRewardInfo info)
  {
    this.rankRewardInfo = info;
    this.UpdateUI();
  }

  public void UpdateUI()
  {
    this.title.text = this.rankRewardInfo.Type != 0 ? Utils.XLAT("event_fallen_knight_individual_ranking_subtitle") : Utils.XLAT("event_fallen_knight_alliance_points_ranking_subtitle");
    this.subtitle.text = this.rankRewardInfo.RankMin != this.rankRewardInfo.RankMax ? string.Format("{0}" + Utils.XLAT("event_ranks_between_num"), (object) string.Empty, (object) this.rankRewardInfo.RankMin, (object) this.rankRewardInfo.RankMax) : string.Format(Utils.XLAT("event_rank_num"), (object) this.rankRewardInfo.RankMin);
    this.dicRankReward.Clear();
    this.dicRankReward.Add(0, new FallenKnightRankRewardItemRenderer.RankRewardPair()
    {
      rankRewardName = "alliance_fund",
      rankRewardCount = this.rankRewardInfo.Fund
    });
    this.dicRankReward.Add(1, new FallenKnightRankRewardItemRenderer.RankRewardPair()
    {
      rankRewardName = "alliance_honor",
      rankRewardCount = this.rankRewardInfo.Honor
    });
    this.dicRankReward.Add(2, new FallenKnightRankRewardItemRenderer.RankRewardPair()
    {
      rankRewardName = this.rankRewardInfo.RankReward_1,
      rankRewardCount = this.rankRewardInfo.RankRewardValue_1
    });
    this.dicRankReward.Add(3, new FallenKnightRankRewardItemRenderer.RankRewardPair()
    {
      rankRewardName = this.rankRewardInfo.RankReward_2,
      rankRewardCount = this.rankRewardInfo.RankRewardValue_2
    });
    this.dicRankReward.Add(4, new FallenKnightRankRewardItemRenderer.RankRewardPair()
    {
      rankRewardName = this.rankRewardInfo.RankReward_3,
      rankRewardCount = this.rankRewardInfo.RankRewardValue_3
    });
    this.dicRankReward.Add(5, new FallenKnightRankRewardItemRenderer.RankRewardPair()
    {
      rankRewardName = this.rankRewardInfo.RankReward_4,
      rankRewardCount = this.rankRewardInfo.RankRewardValue_4
    });
    this.dicRankReward.Add(6, new FallenKnightRankRewardItemRenderer.RankRewardPair()
    {
      rankRewardName = this.rankRewardInfo.RankReward_5,
      rankRewardCount = this.rankRewardInfo.RankRewardValue_5
    });
    this.dicRankReward.Add(7, new FallenKnightRankRewardItemRenderer.RankRewardPair()
    {
      rankRewardName = this.rankRewardInfo.RankReward_6,
      rankRewardCount = this.rankRewardInfo.RankRewardValue_6
    });
    int index1 = 0;
    int index2 = 0;
    for (; index1 < this.dicRankReward.Count; ++index1)
    {
      if (!string.IsNullOrEmpty(this.dicRankReward[index1].rankRewardName) && this.dicRankReward[index1].rankRewardCount > 0L)
      {
        IconData iconData = this.GetIconData(this.dicRankReward[index1].rankRewardName, this.dicRankReward[index1].rankRewardCount);
        this.itemIconInfos[index2].itemIcon.FeedData((IComponentData) iconData);
        this.itemIconInfos[index2].itemIcon.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
        this.itemIconInfos[index2].itemIcon.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
        this.itemIconInfos[index2].itemIcon.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
        this.itemIconInfos[index2].itemName.text = iconData.contents[0];
        this.itemIconInfos[index2].itemNumber.text = Utils.FormatThousands(this.dicRankReward[index1].rankRewardCount.ToString());
        this.itemIconInfos[index2].itemName.transform.parent.gameObject.SetActive(true);
        if (iconData.Data == null)
          this.itemIconInfos[index2].itemIcon.LoadDefaultBackgroud();
        ++index2;
      }
    }
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  private IconData GetIconData(string item_id, long count)
  {
    IconData iconData = new IconData();
    iconData.contents = new string[2];
    if (item_id.Contains("alliance_fund") || item_id.Contains("alliance_honor"))
    {
      iconData.image = "Texture/Alliance/" + item_id;
      iconData.contents[0] = Utils.XLAT(!item_id.Contains("alliance_fund") ? "id_honor" : "id_fund");
    }
    else
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(int.Parse(item_id));
      iconData.image = itemStaticInfo.ImagePath;
      iconData.contents[0] = itemStaticInfo.LocName;
      iconData.Data = (object) itemStaticInfo.internalId;
    }
    iconData.contents[1] = count.ToString();
    return iconData;
  }

  [Serializable]
  public class IconInfo
  {
    public Icon itemIcon;
    public UILabel itemName;
    public UILabel itemNumber;
  }

  public class RankRewardPair
  {
    public string rankRewardName;
    public long rankRewardCount;
  }
}
