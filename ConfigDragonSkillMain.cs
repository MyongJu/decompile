﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragonSkillMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigDragonSkillMain
{
  private ConfigParse parse = new ConfigParse();
  private List<ConfigDragonSkillMainInfo> dragonSkills = new List<ConfigDragonSkillMainInfo>();
  private Dictionary<int, Dictionary<int, ConfigDragonSkillMainInfo>> skillMainInfoListofGroup = new Dictionary<int, Dictionary<int, ConfigDragonSkillMainInfo>>();
  private Dictionary<string, ConfigDragonSkillMainInfo> datas;
  private Dictionary<int, ConfigDragonSkillMainInfo> dicByUniqueId;

  public List<ConfigDragonSkillMainInfo> DragonSkills
  {
    get
    {
      return this.dragonSkills;
    }
  }

  public void BuildDB(object res)
  {
    this.parse.Parse<ConfigDragonSkillMainInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    using (Dictionary<string, ConfigDragonSkillMainInfo>.Enumerator enumerator = this.datas.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, ConfigDragonSkillMainInfo> current = enumerator.Current;
        this.dragonSkills.Add(current.Value);
        if (!this.skillMainInfoListofGroup.ContainsKey(current.Value.group_id))
          this.skillMainInfoListofGroup.Add(current.Value.group_id, new Dictionary<int, ConfigDragonSkillMainInfo>());
        this.skillMainInfoListofGroup[current.Value.group_id].Add(current.Value.level, current.Value);
      }
    }
  }

  public ConfigDragonSkillMainInfo GetSkillMainInfo(int internalID)
  {
    ConfigDragonSkillMainInfo dragonSkillMainInfo;
    this.dicByUniqueId.TryGetValue(internalID, out dragonSkillMainInfo);
    return dragonSkillMainInfo;
  }

  public Dictionary<int, ConfigDragonSkillMainInfo> GetSkillMainInfosofGroup(int groupID)
  {
    return this.skillMainInfoListofGroup[groupID];
  }

  public ConfigDragonSkillMainInfo GetSkillMainInfoOfGrounpByLevel(int groupID, int level, bool all = false)
  {
    if (this.skillMainInfoListofGroup[groupID].ContainsKey(level))
    {
      ConfigDragonSkillMainInfo dragonSkillMainInfo = this.skillMainInfoListofGroup[groupID][level];
      if (all || dragonSkillMainInfo.CanLearn)
        return dragonSkillMainInfo;
    }
    return (ConfigDragonSkillMainInfo) null;
  }

  public ConfigDragonSkillMainInfo GetCurrentMinLevSkillByGrounpId(int groupId)
  {
    return this.GetSkillMainInfoOfGrounpByLevel(groupId, 1, false);
  }

  public ConfigDragonSkillMainInfo GetCurrentMaxLevSkillByGrounpId(int groupId)
  {
    List<long> skillDataByGroupId = DBManager.inst.DB_DragonSkill.GetSkillDataByGroupId(groupId);
    if (skillDataByGroupId == null)
      return (ConfigDragonSkillMainInfo) null;
    List<ConfigDragonSkillMainInfo> dragonSkillMainInfoList = new List<ConfigDragonSkillMainInfo>();
    for (int index = 0; index < skillDataByGroupId.Count; ++index)
    {
      ConfigDragonSkillMainInfo skillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo((int) skillDataByGroupId[index]);
      if (skillMainInfo.CanLearn)
        dragonSkillMainInfoList.Add(skillMainInfo);
    }
    dragonSkillMainInfoList.Sort(new Comparison<ConfigDragonSkillMainInfo>(this.CompareByLevl));
    if (dragonSkillMainInfoList.Count > 0)
      return dragonSkillMainInfoList[dragonSkillMainInfoList.Count - 1];
    return (ConfigDragonSkillMainInfo) null;
  }

  public bool CheckIsMaxLev(int skillId)
  {
    ConfigDragonSkillMainInfo skillMainInfo = this.GetSkillMainInfo(skillId);
    return this.GetSkillMainInfoOfGrounpByLevel(skillMainInfo.group_id, skillMainInfo.level + 1, false) == null;
  }

  public bool CheckCanUpgrade(int skillId)
  {
    ConfigDragonSkillMainInfo skillMainInfo = this.GetSkillMainInfo(skillId);
    ConfigDragonSkillMainInfo infoOfGrounpByLevel = this.GetSkillMainInfoOfGrounpByLevel(skillMainInfo.group_id, skillMainInfo.level + 1, false);
    if (infoOfGrounpByLevel == null)
      return false;
    if (infoOfGrounpByLevel.dark > 0)
      return (long) infoOfGrounpByLevel.dark <= PlayerData.inst.dragonData.DarkPoint;
    if (infoOfGrounpByLevel.light > 0)
      return (long) infoOfGrounpByLevel.light <= PlayerData.inst.dragonData.LightPoint;
    return true;
  }

  private int CompareByLevl(ConfigDragonSkillMainInfo a, ConfigDragonSkillMainInfo b)
  {
    return a.level.CompareTo(b.level);
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, ConfigDragonSkillMainInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, ConfigDragonSkillMainInfo>) null;
  }
}
