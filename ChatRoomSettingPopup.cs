﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomSettingPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;

public class ChatRoomSettingPopup : Popup
{
  public const string POPUP_TYPE = "Chat/ChatRoomSettingPopup";
  public ChatRoomSetting chatRoomSetting;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.chatRoomSetting.Setup((orgParam as ChatRoomSettingPopup.Parameter).roomData);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnCloseClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public ChatRoomData roomData;
  }
}
