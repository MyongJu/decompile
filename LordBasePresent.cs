﻿// Decompiled with JetBrains decompiler
// Type: LordBasePresent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class LordBasePresent : BasePresent
{
  public override void Refresh()
  {
    int count = this.Count;
    int num = DBManager.inst.DB_hero.Get(PlayerData.inst.uid).level;
    if (num > count)
      num = count;
    this.ProgressValue = (float) num / (float) count;
    this.ProgressContent = num.ToString() + "/" + (object) count;
  }

  public override bool IsUpgarde
  {
    get
    {
      return this.Count < DBManager.inst.DB_hero.Get(PlayerData.inst.uid).level;
    }
  }

  public override int IconType
  {
    get
    {
      return 1;
    }
  }
}
