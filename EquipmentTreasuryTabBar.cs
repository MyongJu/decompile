﻿// Decompiled with JetBrains decompiler
// Type: EquipmentTreasuryTabBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EquipmentTreasuryTabBar : MonoBehaviour
{
  public System.Action<int> OnSelectedHandler;
  public UIButtonBar bar;

  private void OnEnable()
  {
    this.bar.OnSelectedHandler += new System.Action<int>(this.ButtonBarClickHandler);
  }

  private void OnDisable()
  {
    this.bar.OnSelectedHandler -= new System.Action<int>(this.ButtonBarClickHandler);
  }

  private void ButtonBarClickHandler(int index)
  {
    if (this.OnSelectedHandler == null)
      return;
    this.OnSelectedHandler(index);
  }

  public int SelectedIndex
  {
    set
    {
      this.bar.SelectedIndex = value;
    }
    get
    {
      return this.bar.SelectedIndex;
    }
  }
}
