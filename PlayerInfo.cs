﻿// Decompiled with JetBrains decompiler
// Type: PlayerInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

public class PlayerInfo
{
  public const int INVALIED_USER_ID = -1;
  public long uid;
  public string userName;
  public string iconURL;
  public int promitionLevel;

  public PlayerInfo()
  {
    this.userName = string.Empty;
    this.iconURL = string.Empty;
    this.promitionLevel = 0;
  }

  public PlayerInfo(long inUid)
  {
    this.uid = inUid;
    this.userName = string.Format("Player {0}", (object) inUid);
    this.iconURL = string.Empty;
    this.promitionLevel = 0;
  }

  public override string ToString()
  {
    return string.Format("[PlayerInfo] uid : {0} ; name : {1} ; iconURL : {2} ; level : {3}", (object) this.uid.ToString(), (object) this.userName, (object) this.iconURL, (object) this.promitionLevel.ToString());
  }

  public string encode()
  {
    return string.Format("{0},{1},{2},{3}", (object) this.uid.ToString(), (object) this.userName, (object) this.iconURL, (object) this.promitionLevel);
  }

  public void decodeFromString(string data)
  {
    string[] strArray = data.Split(new char[1]{ ',' }, StringSplitOptions.RemoveEmptyEntries);
    if (strArray.Length > 0)
      long.TryParse(strArray[0], out this.uid);
    if (strArray.Length > 1)
    {
      this.userName = strArray[1];
      if (string.IsNullOrEmpty(this.userName))
        this.userName = string.Format("Player {0}", (object) this.uid);
    }
    if (strArray.Length > 2)
      this.iconURL = strArray[2];
    if (strArray.Length <= 3)
      return;
    int.TryParse(strArray[3], out this.promitionLevel);
  }
}
