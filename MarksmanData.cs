﻿// Decompiled with JetBrains decompiler
// Type: MarksmanData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class MarksmanData
{
  private List<MarksmanData.RewardData> _rewardDataList = new List<MarksmanData.RewardData>();
  private int _propsConfigId;
  private int _canMarksmanOpen;
  private int _oncePlayFinished;
  private int _rewardRefreshTimes;

  public int Round
  {
    get
    {
      if (this._propsConfigId > 0)
        return this._propsConfigId;
      return 1;
    }
  }

  public int PropsConfigId
  {
    get
    {
      return this._propsConfigId;
    }
  }

  public int PropsId
  {
    get
    {
      LuckyArcherInfo luckyArcherInfo = ConfigManager.inst.DB_LuckyArcher.Get(this._propsConfigId.ToString());
      if (luckyArcherInfo != null)
        return luckyArcherInfo.propsId;
      return 0;
    }
  }

  public int PropsValidTime
  {
    get
    {
      LuckyArcherInfo luckyArcherInfo = ConfigManager.inst.DB_LuckyArcher.Get((this.Round + 1).ToString());
      if (luckyArcherInfo != null)
        return (int) Utils.DateTime2ServerTime(luckyArcherInfo.startTime, "_");
      return 0;
    }
  }

  public string PropsName
  {
    get
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.PropsId);
      if (itemStaticInfo != null)
        return itemStaticInfo.LocName;
      return string.Empty;
    }
  }

  public string PropsImagePath
  {
    get
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.PropsId);
      if (itemStaticInfo != null)
        return itemStaticInfo.ImagePath;
      return string.Empty;
    }
  }

  public long PropsCount
  {
    get
    {
      ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(this.PropsId);
      if (consumableItemData != null)
        return (long) consumableItemData.quantity;
      return 0;
    }
  }

  public bool CanMarksmanOpen
  {
    get
    {
      return this._canMarksmanOpen >= 1;
    }
    set
    {
      this._canMarksmanOpen = !value ? 0 : 1;
    }
  }

  public bool OncePlayFinished
  {
    get
    {
      return this._oncePlayFinished == 0;
    }
  }

  public bool ShootDone
  {
    get
    {
      List<MarksmanData.RewardData> all = this.RewardDataList.FindAll((Predicate<MarksmanData.RewardData>) (x => x.RewardOpenedStep > 0));
      if (this._oncePlayFinished == 1 && all != null)
        return all.Count == this.RewardDataList.Count;
      return false;
    }
  }

  public int RewardRefreshTimes
  {
    get
    {
      return this._rewardRefreshTimes;
    }
  }

  public List<MarksmanData.RewardData> RewardDataList
  {
    get
    {
      this._rewardDataList.Sort((Comparison<MarksmanData.RewardData>) ((a, b) => a.RewardStayIndex.CompareTo(b.RewardStayIndex)));
      return this._rewardDataList;
    }
  }

  public void Decode(Hashtable data)
  {
    if (data == null)
      return;
    DatabaseTools.UpdateData(data, "coin_config_id", ref this._propsConfigId);
    DatabaseTools.UpdateData(data, "is_pay", ref this._canMarksmanOpen);
    DatabaseTools.UpdateData(data, "is_start", ref this._oncePlayFinished);
    DatabaseTools.UpdateData(data, "refresh_number", ref this._rewardRefreshTimes);
    if (data[(object) "rewards"] == null)
      return;
    Hashtable hashtable = data[(object) "rewards"] as Hashtable;
    if (hashtable == null)
      return;
    this._rewardDataList.Clear();
    int outData1 = 0;
    int outData2 = 0;
    IEnumerator enumerator = hashtable.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      Hashtable inData = hashtable[(object) enumerator.Current.ToString()] as Hashtable;
      int result = 0;
      int.TryParse(enumerator.Current.ToString(), out result);
      if (inData != null)
      {
        DatabaseTools.UpdateData(inData, "config_id", ref outData1);
        DatabaseTools.UpdateData(inData, "open_index", ref outData2);
        if (outData1 > 0)
          this._rewardDataList.Add(new MarksmanData.RewardData(outData1, outData2, result));
      }
    }
  }

  public struct Params
  {
    public const string PROPS_CONFIG_ID = "coin_config_id";
    public const string MARKSMAN_CAN_OPEN = "is_pay";
    public const string ONCE_PLAY_FINISHED = "is_start";
    public const string REWARD_REFRESH_TIMES = "refresh_number";
    public const string REWARDS = "rewards";
  }

  public class RewardData
  {
    private int _rewardConfigId;
    private int _rewardOpenedStep;
    private int _rewardStayIndex;

    public RewardData(int rewardConfigId, int rewardOpenedStep, int rewardStayIndex)
    {
      this._rewardConfigId = rewardConfigId;
      this._rewardOpenedStep = rewardOpenedStep;
      this._rewardStayIndex = rewardStayIndex;
    }

    public int RewardOpenedStep
    {
      get
      {
        return this._rewardOpenedStep;
      }
    }

    public int RewardConfigId
    {
      get
      {
        return this._rewardConfigId;
      }
    }

    public int RewardStayIndex
    {
      get
      {
        return this._rewardStayIndex;
      }
    }

    public int RewardId
    {
      get
      {
        LuckyArcherRewardsInfo archerRewardsInfo = ConfigManager.inst.DB_LuckyArcherRewards.Get(this._rewardConfigId.ToString());
        if (archerRewardsInfo != null)
          return archerRewardsInfo.rewardId;
        return 0;
      }
    }

    public int RewardCount
    {
      get
      {
        LuckyArcherRewardsInfo archerRewardsInfo = ConfigManager.inst.DB_LuckyArcherRewards.Get(this._rewardConfigId.ToString());
        if (archerRewardsInfo != null)
          return archerRewardsInfo.rewardCount;
        return 0;
      }
    }
  }

  public enum RewardType
  {
    PICK,
    PLAY,
    RECORD,
  }
}
