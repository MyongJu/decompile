﻿// Decompiled with JetBrains decompiler
// Type: IapRebatePayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UI;

public class IapRebatePayload
{
  private IapRebateRewards iapRebateReward = new IapRebateRewards();
  private int startTime;
  private int endTime;
  private long rechargedAmount;
  private string title;
  private string description;
  private static IapRebatePayload instance;

  public event System.Action OnIapRebateDataChanged;

  public int StartTime
  {
    get
    {
      return this.startTime;
    }
  }

  public int EndTime
  {
    get
    {
      return this.endTime;
    }
  }

  public long RechargedAmount
  {
    get
    {
      return this.rechargedAmount;
    }
  }

  public string Title
  {
    get
    {
      return this.title;
    }
    set
    {
      this.title = value;
    }
  }

  public string Description
  {
    get
    {
      return this.description;
    }
    set
    {
      this.description = value;
    }
  }

  public string CurrentLanguage
  {
    get
    {
      return LocalizationManager.CurrentLanguageCode;
    }
  }

  public IapRebateRewards IapRebateReward
  {
    get
    {
      return this.iapRebateReward;
    }
  }

  public int CanCollectTotalCount
  {
    get
    {
      return this.iapRebateReward != null && this.iapRebateReward.TargetRechargeAmount != -1 && this.rechargedAmount >= (long) this.iapRebateReward.TargetRechargeAmount ? 1 : 0;
    }
  }

  public static IapRebatePayload Instance
  {
    get
    {
      if (IapRebatePayload.instance == null)
        IapRebatePayload.instance = new IapRebatePayload();
      return IapRebatePayload.instance;
    }
  }

  public bool CanShowIapRebateHUD
  {
    get
    {
      if (NetServerTime.inst.ServerTimestamp >= this.startTime)
        return NetServerTime.inst.ServerTimestamp < this.endTime;
      return false;
    }
  }

  public void Decode(Hashtable data)
  {
    if (data == null || data != null && data[(object) "target_gold"] == null)
    {
      this.endTime = 0;
    }
    else
    {
      Hashtable data1 = new Hashtable();
      Hashtable data2 = new Hashtable();
      ArrayList data3 = new ArrayList();
      int outData1 = 0;
      if (this.iapRebateReward == null)
        this.iapRebateReward = new IapRebateRewards();
      DatabaseTools.CheckAndParseOrgData(data[(object) "title"], out data1);
      DatabaseTools.CheckAndParseOrgData(data[(object) "content"], out data2);
      DatabaseTools.CheckAndParseOrgData(data[(object) "reward"], out data3);
      DatabaseTools.UpdateData(data, "target_gold", ref outData1);
      DatabaseTools.UpdateData(data, "current_gold", ref this.rechargedAmount);
      DatabaseTools.UpdateData(data, "end_time", ref this.endTime);
      if (data1 != null)
      {
        if (data1.ContainsKey((object) this.CurrentLanguage))
          DatabaseTools.UpdateData(data1, this.CurrentLanguage, ref this.title);
        else if (data1.ContainsKey((object) "en"))
          DatabaseTools.UpdateData(data1, "en", ref this.title);
        else
          this.title = string.Empty;
      }
      if (data2 != null)
      {
        if (data2.ContainsKey((object) this.CurrentLanguage))
          DatabaseTools.UpdateData(data2, this.CurrentLanguage, ref this.description);
        else if (data2.ContainsKey((object) "en"))
          DatabaseTools.UpdateData(data2, "en", ref this.description);
        else
          this.description = string.Empty;
      }
      this.iapRebateReward.TargetRechargeAmount = outData1;
      if (data3 != null)
      {
        this.iapRebateReward.rewardItemInfoList.Clear();
        for (int index = 0; index < data3.Count; ++index)
        {
          int outData2 = 0;
          int outData3 = 0;
          DatabaseTools.UpdateData(data3[index] as Hashtable, "id", ref outData3);
          DatabaseTools.UpdateData(data3[index] as Hashtable, "amount", ref outData2);
          this.iapRebateReward.rewardItemInfoList.Add(new IapRebateRewards.RewardItemInfo()
          {
            rewardItemId = outData3,
            rewardItemCount = outData2
          });
        }
      }
      if (this.rechargedAmount < (long) this.iapRebateReward.TargetRechargeAmount)
        return;
      this.endTime = 0;
    }
  }

  public void Initialize()
  {
    this.RequestServerData((System.Action<bool, object>) null);
    MessageHub.inst.GetPortByAction("open_payment_return").AddEvent(new System.Action<object>(this.OnPushIapRebateSuccess));
  }

  public void Dispose()
  {
    MessageHub.inst.GetPortByAction("open_payment_return").RemoveEvent(new System.Action<object>(this.OnPushIapRebateSuccess));
  }

  private void OnPushIapRebateSuccess(object result)
  {
    if (!UIManager.inst.IsPopupExist("IAP/LimitedTimeRebatePopup"))
      this.RequestServerData((System.Action<bool, object>) null);
    else
      this.RequestServerData((System.Action<bool, object>) ((arg1, arg2) =>
      {
        if (!arg1 || this.OnIapRebateDataChanged == null)
          return;
        this.OnIapRebateDataChanged();
      }));
  }

  public void RequestServerData(System.Action<bool, object> callback)
  {
    if (callback != null)
      MessageHub.inst.GetPortByAction("player:paymentPackage").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          this.Decode(data as Hashtable);
        if (callback == null)
          return;
        callback(ret, data);
      }), true);
    else
      MessageHub.inst.GetPortByAction("player:paymentPackage").SendLoader((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.Decode(data as Hashtable);
      }), false, false);
  }
}
