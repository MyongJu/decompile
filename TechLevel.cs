﻿// Decompiled with JetBrains decompiler
// Type: TechLevel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class TechLevel : IComparable<TechLevel>
{
  private readonly TechLevelData _techLevel;
  public double ResearchStart;
  public double ResearchEnd;
  private TechLevel.ResearchState _state;

  public TechLevel(TechLevelData techLevelData)
  {
    this._techLevel = techLevelData;
  }

  public int InternalID
  {
    get
    {
      return this._techLevel.InternalID;
    }
  }

  public string ID
  {
    get
    {
      return this._techLevel.ID;
    }
  }

  public string Desc
  {
    get
    {
      return this._techLevel.Description;
    }
  }

  public Tech Tech
  {
    get
    {
      return ConfigManager.inst.DB_Techs[this._techLevel.TechInternalID];
    }
  }

  public int Level
  {
    get
    {
      return this._techLevel.TechLevel;
    }
  }

  public int Time
  {
    get
    {
      return this._techLevel.Duration;
    }
  }

  public int Power
  {
    get
    {
      return this._techLevel.Power;
    }
  }

  public List<int> RequiredTechs
  {
    get
    {
      return this._techLevel.RequiredTechLevels;
    }
  }

  public List<int> RequiredBuildings
  {
    get
    {
      return this._techLevel.RequiredBuildingLevels;
    }
  }

  public List<Effect> Effects
  {
    get
    {
      return this._techLevel.Effects;
    }
  }

  public List<KeyValuePair<CityResourceInfo.ResourceType, int>> Costs
  {
    get
    {
      return this._techLevel.Costs;
    }
  }

  public string Name
  {
    get
    {
      return this._techLevel.Name;
    }
  }

  public string NameKey
  {
    get
    {
      return this._techLevel.NameLocKey;
    }
  }

  public int CompareTo(TechLevel other)
  {
    return this.Level.CompareTo(other.Level);
  }

  public TechLevel.ResearchState State
  {
    get
    {
      return this._state;
    }
    set
    {
      this.SetState(value);
    }
  }

  public void SetState(TechLevel.ResearchState state)
  {
    if (this._state == state)
      return;
    this._state = state;
  }

  public bool IsAvailable
  {
    get
    {
      return this.State == TechLevel.ResearchState.Available;
    }
  }

  public bool IsInProgress
  {
    get
    {
      return this.State == TechLevel.ResearchState.InProgress;
    }
  }

  public bool IsCompleted
  {
    get
    {
      return this.State == TechLevel.ResearchState.Completed;
    }
  }

  public bool PlayerHasReqTechLevels
  {
    get
    {
      for (int index = 0; index < this.RequiredTechs.Count; ++index)
      {
        if (ResearchManager.inst.Contains(this.RequiredTechs[index]) && ResearchManager.inst.Rank(ResearchManager.inst.GetTechLevel(this.RequiredTechs[index]).Tech) < ResearchManager.inst.GetTechLevel(this.RequiredTechs[index]).Level)
          return false;
      }
      return true;
    }
  }

  public bool PlayerHasReqBuildingLevel
  {
    get
    {
      for (int index = 0; index < this.RequiredBuildings.Count; ++index)
      {
        BuildingInfo data = ConfigManager.inst.DB_Building.GetData(this.RequiredBuildings[index]);
        if (CityManager.inst.GetBuildingCountOverLevel(data.Building_Lvl, data.Type) == 0)
          return false;
      }
      return true;
    }
  }

  public bool PlayerHasReqCosts
  {
    get
    {
      CityData byUid = DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid);
      for (int index = 0; index < this.Costs.Count; ++index)
      {
        if ((int) byUid.resources.GetCurrentResource(this.Costs[index].Key) < this.BenefitCost(this.Costs[index].Key, (double) this.Costs[index].Value))
          return false;
      }
      return true;
    }
  }

  public bool PlayerHasPrereqs
  {
    get
    {
      if (this.IsAvailable && this.PlayerHasReqCosts && this.PlayerHasReqBuildingLevel)
        return this.PlayerHasReqTechLevels;
      return false;
    }
  }

  public int BenefitCost(CityResourceInfo.ResourceType type, double baseValue)
  {
    switch (type)
    {
      case CityResourceInfo.ResourceType.FOOD:
        return (int) Mathf.Max(0.0f, ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) baseValue, "calc_research_food_cost"));
      case CityResourceInfo.ResourceType.WOOD:
        return (int) Mathf.Max(0.0f, ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) baseValue, "calc_research_wood_cost"));
      case CityResourceInfo.ResourceType.ORE:
        return (int) Mathf.Max(0.0f, ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) baseValue, "calc_research_ore_cost"));
      case CityResourceInfo.ResourceType.SILVER:
        return (int) Mathf.Max(0.0f, ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) baseValue, "calc_research_silver_cost"));
      default:
        return (int) baseValue;
    }
  }

  public enum ResearchState
  {
    Available,
    InProgress,
    Completed,
  }
}
