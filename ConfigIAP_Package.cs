﻿// Decompiled with JetBrains decompiler
// Type: ConfigIAP_Package
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigIAP_Package
{
  private Dictionary<string, IAPPackageInfo> m_DataByID;
  private Dictionary<int, IAPPackageInfo> m_DataByInternalID;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<IAPPackageInfo, string>(result as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
  }

  public IAPPackageInfo Get(string id)
  {
    IAPPackageInfo iapPackageInfo;
    this.m_DataByID.TryGetValue(id, out iapPackageInfo);
    return iapPackageInfo;
  }

  public IAPPackageInfo Get(int internalID)
  {
    IAPPackageInfo iapPackageInfo;
    this.m_DataByInternalID.TryGetValue(internalID, out iapPackageInfo);
    return iapPackageInfo;
  }

  public void Clear()
  {
    this.m_DataByID = (Dictionary<string, IAPPackageInfo>) null;
    this.m_DataByInternalID = (Dictionary<int, IAPPackageInfo>) null;
  }
}
