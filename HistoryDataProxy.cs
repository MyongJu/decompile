﻿// Decompiled with JetBrains decompiler
// Type: HistoryDataProxy
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;

public class HistoryDataProxy
{
  private string _action = string.Empty;
  private List<IHistoryData> _datas = new List<IHistoryData>();
  private bool _isDestroy;

  public HistoryDataProxy(string action)
  {
    this._action = action;
  }

  public event System.Action OnFetchDataFinish;

  public List<IHistoryData> GetData()
  {
    return this._datas;
  }

  public string GetTitle()
  {
    string Term = string.Empty;
    string action = this._action;
    if (action != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (HistoryDataProxy.\u003C\u003Ef__switch\u0024map28 == null)
      {
        // ISSUE: reference to a compiler-generated field
        HistoryDataProxy.\u003C\u003Ef__switch\u0024map28 = new Dictionary<string, int>(2)
        {
          {
            "AllianceTemple:loadHistory",
            0
          },
          {
            "anniversary:getChallengeHistory",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (HistoryDataProxy.\u003C\u003Ef__switch\u0024map28.TryGetValue(action, out num))
      {
        switch (num)
        {
          case 0:
            Term = "alliance_altar_uppercase_spell_history";
            break;
          case 1:
            Term = "event_1_year_tournament_record_name";
            break;
        }
      }
    }
    return ScriptLocalization.Get(Term, true);
  }

  public void Destroy()
  {
    this._isDestroy = true;
  }

  public string GetEmptyLabel()
  {
    string Term = string.Empty;
    string action = this._action;
    if (action != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (HistoryDataProxy.\u003C\u003Ef__switch\u0024map29 == null)
      {
        // ISSUE: reference to a compiler-generated field
        HistoryDataProxy.\u003C\u003Ef__switch\u0024map29 = new Dictionary<string, int>(2)
        {
          {
            "AllianceTemple:loadHistory",
            0
          },
          {
            "anniversary:getChallengeHistory",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (HistoryDataProxy.\u003C\u003Ef__switch\u0024map29.TryGetValue(action, out num))
      {
        switch (num)
        {
          case 0:
            Term = "alliance_altar_no_spell_history_description";
            break;
          case 1:
            Term = "log_no_history_yet_description";
            break;
        }
      }
    }
    return ScriptLocalization.Get(Term, true);
  }

  public void FetchData()
  {
    this._datas.Clear();
    RequestManager.inst.SendRequest(this._action, this.GetParams(), new System.Action<bool, object>(this.LoadDataCallBack), true);
  }

  private Hashtable GetParams()
  {
    Hashtable hashtable = (Hashtable) null;
    string action = this._action;
    if (action != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (HistoryDataProxy.\u003C\u003Ef__switch\u0024map2A == null)
      {
        // ISSUE: reference to a compiler-generated field
        HistoryDataProxy.\u003C\u003Ef__switch\u0024map2A = new Dictionary<string, int>(2)
        {
          {
            "AllianceTemple:loadHistory",
            0
          },
          {
            "anniversary:getChallengeHistory",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (HistoryDataProxy.\u003C\u003Ef__switch\u0024map2A.TryGetValue(action, out num))
      {
        if (num != 0)
        {
          if (num == 1)
            ;
        }
        else
          hashtable = Utils.Hash((object) "world_id", (object) PlayerData.inst.playerCityData.cityLocation.K, (object) "alliance_id", (object) PlayerData.inst.allianceId);
      }
    }
    return hashtable;
  }

  private void LoadDataCallBack(bool success, object playload)
  {
    if (this._isDestroy || !success || playload == null)
      return;
    Hashtable hashtable = playload as Hashtable;
    System.Action<Hashtable> paraseFunc = this.GetParaseFunc();
    if (paraseFunc == null)
      return;
    paraseFunc(hashtable);
    this.RefreshData();
  }

  private void RefreshData()
  {
    this._datas.Sort(new Comparison<IHistoryData>(this.CompareData));
    if (this.OnFetchDataFinish == null)
      return;
    this.OnFetchDataFinish();
  }

  private int CompareData(IHistoryData a, IHistoryData b)
  {
    if (a.StartTime > b.StartTime)
      return -1;
    return a.StartTime == b.StartTime ? 0 : 1;
  }

  private System.Action<Hashtable> GetParaseFunc()
  {
    System.Action<Hashtable> action1 = (System.Action<Hashtable>) null;
    string action2 = this._action;
    if (action2 != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (HistoryDataProxy.\u003C\u003Ef__switch\u0024map2B == null)
      {
        // ISSUE: reference to a compiler-generated field
        HistoryDataProxy.\u003C\u003Ef__switch\u0024map2B = new Dictionary<string, int>(2)
        {
          {
            "AllianceTemple:loadHistory",
            0
          },
          {
            "anniversary:getChallengeHistory",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (HistoryDataProxy.\u003C\u003Ef__switch\u0024map2B.TryGetValue(action2, out num))
      {
        switch (num)
        {
          case 0:
            action1 = new System.Action<Hashtable>(this.DecodeMagic);
            break;
          case 1:
            action1 = new System.Action<Hashtable>(this.DecodeAnniversaryHistroy);
            break;
        }
      }
    }
    return action1;
  }

  private void DecodeMagic(Hashtable hash)
  {
    if (hash == null || !hash.ContainsKey((object) "magic_history"))
      return;
    ArrayList arrayList = hash[(object) "magic_history"] as ArrayList;
    if (arrayList == null || arrayList.Count <= 0)
      return;
    for (int index = 0; index < arrayList.Count; ++index)
    {
      AllianceMagicHistoryData magicHistoryData = new AllianceMagicHistoryData();
      if (magicHistoryData.Decode(arrayList[index] as Hashtable))
        this._datas.Add((IHistoryData) magicHistoryData);
    }
  }

  private void DecodeAnniversaryHistroy(Hashtable hash)
  {
    if (hash == null || !hash.ContainsKey((object) "list"))
      return;
    ArrayList arrayList = hash[(object) "list"] as ArrayList;
    if (arrayList == null || arrayList.Count <= 0)
      return;
    for (int index = 0; index < arrayList.Count; ++index)
    {
      AnniversaryHistroyData anniversaryHistroyData = new AnniversaryHistroyData();
      string json = arrayList[index].ToString();
      if (anniversaryHistroyData.Parse(json))
        this._datas.Add((IHistoryData) anniversaryHistroyData);
    }
  }
}
