﻿// Decompiled with JetBrains decompiler
// Type: ActivityFestivalItemSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class ActivityFestivalItemSlot : MonoBehaviour
{
  private int _itemId;
  [SerializeField]
  private ActivityFestivalItemSlot.Panel panel;

  public void Setup(int itemId)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo == null)
      return;
    this._itemId = itemId;
    BuilderFactory.Instance.Build((UIWidget) this.panel.itemIcon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    this.panel.itemName.text = itemStaticInfo.LocName;
  }

  public void Release()
  {
    BuilderFactory.Instance.Release((UIWidget) this.panel.itemIcon);
  }

  public void OnIconClick()
  {
    Utils.ShowItemTip(this._itemId, this.panel.border.transform, 0L, 0L, 0);
  }

  public void OnIconPress()
  {
    Utils.DelayShowTip(this._itemId, this.panel.border.transform, 0L, 0L, 0);
  }

  public void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  private void Reset()
  {
    this.panel.itemIcon = this.transform.FindChild("icon").GetComponent<UITexture>();
    this.panel.itemName = this.transform.FindChild("name").GetComponent<UILabel>();
  }

  [Serializable]
  protected class Panel
  {
    public UITexture itemIcon;
    public UILabel itemName;
    public UISprite border;
  }
}
