﻿// Decompiled with JetBrains decompiler
// Type: AllianceSymbolPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceSymbolPopup : PopupBase
{
  [SerializeField]
  private AllianceSymbolPiecePanel m_FlagSymbolPanel;
  [SerializeField]
  private AllianceSymbolPiecePanel m_ShieldSymbolPanel;
  [SerializeField]
  private AllianceSymbolPiecePanel m_PatternSymbolPanel;
  [SerializeField]
  private AllianceSymbol m_AllianceSymbol;
  private System.Action<int> m_Callback;
  private int m_Flag;
  private int m_Shield;
  private int m_Pattern;

  public void Initialize(int symbol, System.Action<int> callback)
  {
    this.m_Callback = callback;
    this.m_Flag = AllianceSymbol.GetFlag(symbol);
    this.m_Shield = AllianceSymbol.GetShield(symbol);
    this.m_Pattern = AllianceSymbol.GetPattern(symbol);
    this.m_AllianceSymbol.SetSymbols(this.m_Flag, this.m_Shield, this.m_Pattern);
    this.m_FlagSymbolPanel.Initialize(AllianceSymbol.FLAG_SPRITES, this.m_Flag, new System.Action<GameObject>(this.OnFlagSelected));
    this.m_ShieldSymbolPanel.Initialize(AllianceSymbol.SHIELD_SPRITES, this.m_Shield, new System.Action<GameObject>(this.OnShieldSelected));
    this.m_PatternSymbolPanel.Initialize(AllianceSymbol.PATTERN_SPRITES, this.m_Pattern, new System.Action<GameObject>(this.OnPatternSelected));
  }

  private void OnFlagSelected(GameObject go)
  {
    this.m_Flag = go.GetComponent<AllianceSymbolPiece>().index;
    this.m_AllianceSymbol.SetSymbols(this.m_Flag, this.m_Shield, this.m_Pattern);
  }

  private void OnShieldSelected(GameObject go)
  {
    this.m_Shield = go.GetComponent<AllianceSymbolPiece>().index;
    this.m_AllianceSymbol.SetSymbols(this.m_Flag, this.m_Shield, this.m_Pattern);
  }

  private void OnPatternSelected(GameObject go)
  {
    this.m_Pattern = go.GetComponent<AllianceSymbolPiece>().index;
    this.m_AllianceSymbol.SetSymbols(this.m_Flag, this.m_Shield, this.m_Pattern);
  }

  public void OnClose()
  {
    this.Close();
  }

  public void OnOkay()
  {
    this.Close();
    if (this.m_Callback == null)
      return;
    this.m_Callback(AllianceSymbol.GetSymbol(this.m_Flag, this.m_Shield, this.m_Pattern));
  }
}
