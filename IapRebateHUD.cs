﻿// Decompiled with JetBrains decompiler
// Type: IapRebateHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class IapRebateHUD : MonoBehaviour
{
  public GameObject rootNode;
  public GameObject tipInfoNode;
  public UILabel tipInfoCount;
  public UILabel timeText;
  private bool start;
  private bool requestDataFlag;

  private void Start()
  {
    this.start = true;
    this.rootNode.SetActive(false);
    this.UpdateUI();
  }

  private void OnEnable()
  {
    if (this.start)
      this.UpdateUI();
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void OnSecondEvent(int time)
  {
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (IapRebatePayload.Instance.CanShowIapRebateHUD && Utils.IsIAPStoreEnabled)
    {
      NGUITools.SetActive(this.rootNode, true);
      this.requestDataFlag = false;
    }
    else
    {
      if (this.rootNode.activeInHierarchy)
        NGUITools.SetActive(this.rootNode, false);
      if (!this.requestDataFlag)
      {
        IapRebatePayload.Instance.RequestServerData((System.Action<bool, object>) null);
        this.requestDataFlag = true;
      }
    }
    if (IapRebatePayload.Instance.CanCollectTotalCount > 0)
    {
      NGUITools.SetActive(this.tipInfoNode, true);
      this.tipInfoCount.text = IapRebatePayload.Instance.CanCollectTotalCount.ToString();
    }
    else
    {
      NGUITools.SetActive(this.tipInfoNode, false);
      this.tipInfoCount.text = string.Empty;
    }
    this.timeText.text = Utils.FormatTime(IapRebatePayload.Instance.EndTime - NetServerTime.inst.ServerTimestamp, true, false, true);
  }

  public void OnIapRebateBtnPressed()
  {
    IapRebatePayload.Instance.RequestServerData((System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      UIManager.inst.OpenPopup("IAP/LimitedTimeRebatePopup", (Popup.PopupParameter) null);
    }));
    OperationTrace.TraceClick(ClickArea.IapRebate);
  }
}
