﻿// Decompiled with JetBrains decompiler
// Type: OpeningTalk1
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class OpeningTalk1 : MonoBehaviour
{
  private const string talk2path = "Prefab/OpeningCinematic/OpeningTalk2";

  public void OnClickHandle()
  {
    AssetManager.Instance.LoadAsync("Prefab/OpeningCinematic/OpeningTalk2", (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      Utils.DuplicateGOB(obj as GameObject, UIManager.inst.ui2DCamera.transform);
      AssetManager.Instance.UnLoadAsset("Prefab/OpeningCinematic/OpeningTalk2", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    }), (System.Type) null);
  }
}
