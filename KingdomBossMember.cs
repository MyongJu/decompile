﻿// Decompiled with JetBrains decompiler
// Type: KingdomBossMember
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class KingdomBossMember
{
  public string k;
  public long uid;
  public string x;
  public string y;
  public string name;
  public string acronym;
  public string portrait;
  public string icon;
  public int lord_title;
  public long start_total_troops;
  public long end_total_troops;
  public long kill_total_troops;
  public long original_total_troops;

  public string fullname
  {
    get
    {
      if (this.acronym != null)
        return "(" + this.acronym + ")" + this.name;
      return this.name;
    }
  }

  public string site
  {
    get
    {
      return "K: " + this.k + " X: " + this.x + " Y: " + this.y;
    }
  }
}
