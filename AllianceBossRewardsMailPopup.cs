﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossRewardsMailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceBossRewardsMailPopup : BaseReportPopup
{
  private List<GameObject> pool = new List<GameObject>();
  private AllianceBossRewardsMailPopup.AllianceBossRewardsContent grc;
  public UIScrollView sv;
  public GameObject rewardsTemplate;

  private void Init()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, true, false, string.Empty);
    float y = NGUIMath.CalculateRelativeWidgetBounds(this.rewardsTemplate.transform).size.y;
    float num1 = this.rewardsTemplate.transform.localPosition.y + y / 2f;
    if (this.grc.rank_reward.Count != 0)
    {
      AllianceBossRewardsGroup.Data d = new AllianceBossRewardsGroup.Data();
      d.header = new string[2]
      {
        ScriptLocalization.Get("mail_subtitle_damage_rewards", true),
        ScriptLocalization.GetWithPara("mail_subject_damage_rewards", new Dictionary<string, string>()
        {
          {
            "0",
            this.grc.rank
          }
        }, true)
      };
      List<IconData> data = this.SummarizeRewards(this.grc.rank_reward, 1);
      float num2 = 0.0f;
      AllianceBossRankRewardInfo bossRankRewardInfo = ConfigManager.inst.DB_AllianceBossRankReward.Get(this.grc.rank);
      if (bossRankRewardInfo != null)
        num2 = bossRankRewardInfo.fundRatio;
      if (this.grc.fund != 0)
      {
        IconData iconData = new IconData("Texture/Alliance/alliance_fund", new string[2]
        {
          ScriptLocalization.Get("id_fund", true),
          "+" + ((int) ((double) this.grc.fund * (double) num2)).ToString()
        });
        data.Add(iconData);
      }
      d.ilcd = new IconListComponentData(data);
      GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.rewardsTemplate, this.rewardsTemplate.transform.parent);
      this.pool.Add(gameObject);
      gameObject.GetComponent<AllianceBossRewardsGroup>().SeedData(d);
      float num3 = y + 15f;
      float num4 = num1 - num3 / 2f;
      Vector3 localPosition = this.rewardsTemplate.transform.localPosition;
      localPosition.y = num4;
      gameObject.transform.localPosition = localPosition;
      num1 = num4 - num3 / 2f;
    }
    if (this.grc.base_reward.Count != 0)
    {
      AllianceBossRewardsGroup.Data d = new AllianceBossRewardsGroup.Data();
      d.header = new string[2]
      {
        ScriptLocalization.Get("mail_subtitle_victory_rewards", true),
        ScriptLocalization.GetWithPara("mail_subject_victory_rewards", new Dictionary<string, string>()
        {
          {
            "0",
            ScriptLocalization.Get(ConfigManager.inst.DB_AllianceBoss.Get(this.grc.boss_id).name, true)
          }
        }, true)
      };
      List<IconData> data = this.SummarizeRewards(this.grc.base_reward, 1);
      if (this.grc.fund != 0)
      {
        IconData iconData = new IconData("Texture/Alliance/alliance_fund", new string[2]
        {
          ScriptLocalization.Get("id_fund", true),
          "+" + this.grc.fund.ToString()
        });
        data.Add(iconData);
      }
      if (this.grc.honor != 0)
      {
        IconData iconData = new IconData("Texture/Alliance/alliance_honor", new string[2]
        {
          ScriptLocalization.Get("id_honor", true),
          "+" + this.grc.honor.ToString()
        });
        data.Add(iconData);
      }
      d.ilcd = new IconListComponentData(data);
      GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.rewardsTemplate, this.rewardsTemplate.transform.parent);
      this.pool.Add(gameObject);
      gameObject.GetComponent<AllianceBossRewardsGroup>().SeedData(d);
      float num2 = y + 15f;
      float num3 = num1 - num2 / 2f;
      Vector3 localPosition = this.rewardsTemplate.transform.localPosition;
      localPosition.y = num3;
      gameObject.transform.localPosition = localPosition;
      num1 = num3 - num2 / 2f;
    }
    Vector3 localPosition1 = this.btnCollectionBase.transform.localPosition;
    localPosition1.y = num1;
    this.btnCollectionBase.transform.localPosition = localPosition1;
    this.HideTemplate(true);
  }

  private void HideTemplate(bool hide)
  {
    this.rewardsTemplate.SetActive(!hide);
  }

  private List<IconData> SummarizeRewards(Dictionary<string, int> rewards, int scale = 1)
  {
    if (rewards == null)
      return (List<IconData>) null;
    List<IconData> iconDataList = new List<IconData>();
    using (Dictionary<string, int>.Enumerator enumerator = rewards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, int> current = enumerator.Current;
        int internalId = int.Parse(current.Key);
        int num = current.Value * scale;
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
        IconData iconData = new IconData(itemStaticInfo.ImagePath, new string[2]
        {
          ScriptLocalization.Get(itemStaticInfo.Loc_Name_Id, true),
          "X" + num.ToString()
        });
        iconDataList.Add(iconData);
      }
    }
    return iconDataList;
  }

  private void Reset()
  {
    this.sv.ResetPosition();
    this.HideTemplate(false);
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        PrefabManagerEx.Instance.Destroy(current);
      }
    }
    this.pool.Clear();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.grc = JsonReader.Deserialize<AllianceBossRewardsMailPopup.AllianceBossRewardsContent>(Utils.Object2Json((object) this.param.hashtable));
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }

  public class AllianceBossRewardsContent
  {
    public Dictionary<string, int> rank_reward = new Dictionary<string, int>();
    public Dictionary<string, int> base_reward = new Dictionary<string, int>();
    public string rank;
    public int boss_id;
    public string damage_rate;
    public int fund;
    public int honor;
  }
}
