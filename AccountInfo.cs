﻿// Decompiled with JetBrains decompiler
// Type: AccountInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;

public class AccountInfo
{
  private bool _isSyn;
  private int _cTime;

  public FunplusAccountType AccountType { get; set; }

  public string AccountID { get; set; }

  public string AccountName { get; set; }

  public string AccountEmail { get; set; }

  public string Type { get; set; }

  public int CTime
  {
    get
    {
      return this._cTime;
    }
    set
    {
      this._cTime = value;
    }
  }

  public bool IsSyn
  {
    get
    {
      return this._isSyn;
    }
    set
    {
      this._isSyn = value;
    }
  }
}
