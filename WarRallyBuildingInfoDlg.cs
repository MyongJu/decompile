﻿// Decompiled with JetBrains decompiler
// Type: WarRallyBuildingInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;

public class WarRallyBuildingInfoDlg : BuildingInfoBaseDlg
{
  public UITexture texture1;
  public UILabel nameLabel1;
  public UILabel valueLabel1;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    if (this.buildingInfo.Benefit_ID_1.Length <= 0)
      return;
    this.DrawBenefit(this.buildingInfo.Benefit_Value_1, this.nameLabel1, this.valueLabel1, int.Parse(this.buildingInfo.Benefit_ID_1), (int) this.buildingInfo.Benefit_Value_1, "calc_rally_capacity", this.texture1);
  }

  private void DrawBenefit(double benifitValue, UILabel nameLabel, UILabel valueLabel, int benefitInternalID, int baseValue, string caclID, UITexture texture = null)
  {
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitInternalID];
    nameLabel.text = dbProperty.Name;
    if ((UnityEngine.Object) texture != (UnityEngine.Object) null)
      BuilderFactory.Instance.HandyBuild((UIWidget) texture, dbProperty.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    float finalData = (float) ConfigManager.inst.DB_BenefitCalc.GetFinalData(baseValue, caclID);
    string str = (double) baseValue <= 0.0 ? "-" : string.Empty;
    if (dbProperty.Format == PropertyDefinition.FormatType.Percent)
      valueLabel.text = dbProperty.ConvertToDisplayString((double) baseValue, false, true) + str + dbProperty.ConvertToDisplayString((double) Math.Abs(finalData - (float) baseValue), true, true);
    else
      valueLabel.text = dbProperty.ConvertToDisplayString((double) baseValue, false, true) + str + dbProperty.ConvertToDisplayString((double) Math.Abs(finalData - (float) baseValue), true, true);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    BuilderFactory.Instance.Release((UIWidget) this.texture1);
    base.OnClose(orgParam);
  }
}
