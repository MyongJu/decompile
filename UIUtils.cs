﻿// Decompiled with JetBrains decompiler
// Type: UIUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class UIUtils
{
  public static bool FormatResourceRequireLabel(UILabel label, double need, CityResourceInfo.ResourceType resourceType)
  {
    long currentResource = (long) DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).resources.GetCurrentResource(resourceType);
    label.text = need <= (double) currentResource ? Utils.FormatShortThousandsLong(currentResource) + "/" + Utils.FormatShortThousands((int) need) : "[ff0000]" + Utils.FormatShortThousandsLong(currentResource) + "[-]/" + Utils.FormatShortThousands((int) need);
    return need <= (double) currentResource;
  }

  public static void CleanGrid(UIGrid parent)
  {
    using (List<Transform>.Enumerator enumerator = parent.GetChildList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Transform current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        Object.Destroy((Object) current.gameObject);
      }
    }
  }

  public static void ClearTable(UITable parent)
  {
    using (List<Transform>.Enumerator enumerator = parent.GetChildList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Transform current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        Object.Destroy((Object) current.gameObject);
      }
    }
  }
}
