﻿// Decompiled with JetBrains decompiler
// Type: PVEMonsterDetailDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PVEMonsterDetailDialog : UI.Dialog
{
  public UILabel title;
  public UILabel desc;
  public UILabel labelX;
  public UILabel labelY;
  public UILabel power;
  public UITexture monsterIcon;
  public RewardNormalItem[] rewardItems;
  public UILabel spirit;
  public UIButton attackBt;
  public UILabel normalTip;
  public PVEMonsterPreview previewPrefab;
  private Coordinate _targetLocation;
  private TileData _tileData;
  private GameObject monster;
  private PVEMonsterPreview preview;
  private bool _open;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    PVEMonsterDetailDialog.Parameter parameter = orgParam as PVEMonsterDetailDialog.Parameter;
    if (orgParam != null)
    {
      this._targetLocation = parameter.coordinate;
      this._tileData = PVPMapData.MapData.GetReferenceAt(this._targetLocation);
    }
    this.monster = UnityEngine.Object.Instantiate<GameObject>(this.previewPrefab.gameObject);
    NGUITools.SetActive(this.monster, true);
    this.preview = this.monster.GetComponent<PVEMonsterPreview>();
    NGUITools.SetLayer(this.monster, LayerMask.NameToLayer("UI3D"));
    this.preview.InitializeCharacter(this._tileData);
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  private void OnSecond(int serverTime)
  {
    this.CheckAttack();
    if (this._tileData == null || this._tileData.OwnerID <= 0L || this._tileData.Value > 0)
      return;
    this.OnCloseHandler();
  }

  private void CheckAttack()
  {
    WorldEncounterData data = ConfigManager.inst.DB_WorldEncount.GetData((long) this._tileData.ResourceId);
    if (data == null)
      return;
    int result = 0;
    int.TryParse(data.level, out result);
    this._open = PlayerData.inst.userData.monster_level >= result - 1;
    NGUITools.SetActive(this.attackBt.gameObject, this._open);
    NGUITools.SetActive(this.normalTip.gameObject, !this._open);
  }

  public void OnCloseHandler()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnInfoHandler()
  {
    UIManager.inst.OpenPopup("InfoKingdom", (Popup.PopupParameter) new InfoKingdom.Parameter()
    {
      initIndex = 1
    });
  }

  public void OnFindMonsterHandler()
  {
    MessageHub.inst.GetPortByAction("map:findNearbyMaxLevelMonsterUserCanFight").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, orgData) =>
    {
      if (!ret)
        return;
      this.OnCloseHandler();
      Hashtable inData = orgData as Hashtable;
      if (inData != null)
      {
        Coordinate location = new Coordinate();
        DatabaseTools.UpdateData(inData, "world_id", ref location.K);
        DatabaseTools.UpdateData(inData, "map_x", ref location.X);
        DatabaseTools.UpdateData(inData, "map_y", ref location.Y);
        PVPSystem.Instance.Map.GotoLocation(location, true);
      }
      else
        UIManager.inst.toast.Show(string.Format(Utils.XLAT("toast_no_monster_nearby"), (object) (PlayerData.inst.userData.monster_level + 1)), (System.Action) null, 4f, false);
    }), true);
  }

  private void Clear()
  {
    for (int index = 0; index < this.rewardItems.Length; ++index)
      this.rewardItems[index].Clear();
    UnityEngine.Object.Destroy((UnityEngine.Object) this.monster);
    this.preview = (PVEMonsterPreview) null;
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  private void UpdateUI()
  {
    if (this._tileData == null)
      return;
    this.attackBt.isEnabled = (!PVPSystem.Instance.Map.IsForeigner() || PVPMap.InHeitudi(this._targetLocation)) && this._tileData.Location.K == PlayerData.inst.playerCityData.cityLocation.K;
    WorldEncounterData data = ConfigManager.inst.DB_WorldEncount.GetData((long) this._tileData.ResourceId);
    this.labelX.text = this._targetLocation.X.ToString();
    this.labelY.text = this._targetLocation.Y.ToString();
    if (data == null)
      return;
    int result = 0;
    int.TryParse(data.level, out result);
    this._open = PlayerData.inst.userData.monster_level >= result - 1;
    if (TutorialManager.Instance.EarlyGoalType == TutorialManager.ABTestType.Second)
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      if (data.equipment_level != -1)
      {
        para.Add("0", data.Name);
        para.Add("1", data.equipment_level.ToString());
        this.desc.text = ScriptLocalization.GetWithPara("tutorial_pointer_craft_equipment_description", para, true);
      }
      else
      {
        para.Add("0", data.Name);
        this.desc.text = ScriptLocalization.GetWithPara("tutorial_pointer_slain_monster_items_description", para, true);
      }
      this.desc.color = Utils.GetColorFromHex(16777215U);
    }
    else
      this.desc.text = data.Desc;
    if ((UnityEngine.Object) this.title != (UnityEngine.Object) null)
    {
      string name = data.Name;
      this.title.text = Utils.XLAT("id_lv") + (object) result + " " + name;
    }
    this.power.text = data.recommended_power.ToString();
    int finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(data.cost_energy, "calc_monster_spirit_cost");
    this.spirit.text = finalData.ToString();
    this.rewardItems[0].SetData(RewardNormalItem.RewardType.item, ScriptLocalization.Get(data.reward_display_name_1, true), Utils.GetUITextPath() + data.reward_display_image_1);
    this.rewardItems[1].SetData(RewardNormalItem.RewardType.item, ScriptLocalization.Get(data.reward_display_name_2, true), Utils.GetUITextPath() + data.reward_display_image_2);
    this.rewardItems[2].SetData(RewardNormalItem.RewardType.item, ScriptLocalization.Get(data.reward_display_name_3, true), Utils.GetUITextPath() + data.reward_display_image_3);
    this.rewardItems[3].SetData(RewardNormalItem.RewardType.item, ScriptLocalization.Get(data.reward_display_name_4, true), Utils.GetUITextPath() + data.reward_display_image_4);
    this.rewardItems[4].SetData(RewardNormalItem.RewardType.item, ScriptLocalization.Get(data.reward_display_name_5, true), Utils.GetUITextPath() + data.reward_display_image_5);
    if (DBManager.inst.DB_hero.Get((long) PlayerData.inst.cityId).spirit < finalData)
      this.spirit.color = Color.red;
    else
      this.spirit.color = Color.white;
    this.normalTip.text = ScriptLocalization.GetWithPara("monster_attack_recommended_monster_level_description", new Dictionary<string, string>()
    {
      {
        "num",
        (PlayerData.inst.userData.monster_level + 1).ToString()
      }
    }, true);
    NGUITools.SetActive(this.attackBt.gameObject, this._open);
    NGUITools.SetActive(this.normalTip.gameObject, !this._open);
  }

  public void AttackHandler()
  {
    if (DBManager.inst.DB_March.marchOwner.Count >= PlayerData.inst.playerCityData.marchSlot)
    {
      GameEngine.Instance.marchSystem.ShowMaxMarchSlotMessage();
    }
    else
    {
      DB.HeroData heroData = DBManager.inst.DB_hero.Get((long) PlayerData.inst.cityId);
      WorldEncounterData data = ConfigManager.inst.DB_WorldEncount.GetData((long) this._tileData.ResourceId);
      if (data == null)
        return;
      int finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(data.cost_energy, "calc_monster_spirit_cost");
      if (heroData.spirit < finalData)
        UIManager.inst.toast.Show(ScriptLocalization.Get("monster_attack_not_enough_stamina_description", true), (System.Action) null, 4f, false);
      else
        UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
        {
          location = this._targetLocation,
          marchType = MarchAllocDlg.MarchType.monsterAttack,
          energyCost = ConfigManager.inst.DB_BenefitCalc.GetFinalData(data.cost_energy, "calc_monster_spirit_cost")
        }, 1 != 0, 1 != 0, 1 != 0);
    }
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.Clear();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public Coordinate coordinate;
  }
}
