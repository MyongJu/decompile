﻿// Decompiled with JetBrains decompiler
// Type: GoldConsumePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using UI;
using UnityEngine;

public class GoldConsumePopup : Popup
{
  private GoldConsumePopup.Parameter param;
  private float goldnum;
  private int lefttime;
  private int unitprice;
  private int freeTime;
  private System.Action onConfirmButtonClick;
  private System.Action confirmButtonGoldNotEnoughClickEvent;
  private System.Action onCloseButtonClick;
  [SerializeField]
  private GoldConsumePopup.Panel panel;

  public void OnCloseButtonClick()
  {
    if (this.onCloseButtonClick != null)
      this.onCloseButtonClick();
    if (this.param.lefttime > 0 && Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.Instance_secondEvent);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnButtonClick()
  {
    if ((double) GameEngine.Instance.PlayerData.hostPlayer.Currency >= (double) this.goldnum)
    {
      if (this.onConfirmButtonClick != null)
        this.onConfirmButtonClick();
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    }
    else
    {
      if (this.confirmButtonGoldNotEnoughClickEvent != null)
        this.confirmButtonGoldNotEnoughClickEvent();
      this.OnCloseButtonClick();
      Utils.ShowNotEnoughGoldTip();
    }
  }

  private void Instance_secondEvent(int obj)
  {
    --this.lefttime;
    int time = this.lefttime - this.freeTime;
    this.goldnum = time <= 0 ? 0.0f : (float) ItemBag.CalculateCost(time, this.unitprice);
    this.panel.LB_GOLDNUM.text = ((int) this.goldnum).ToString();
    if ((double) GameEngine.Instance.PlayerData.hostPlayer.Currency >= (double) this.goldnum)
      this.panel.LB_GOLDNUM.color = this.panel.normalColor;
    else
      this.panel.LB_GOLDNUM.color = Color.red;
    if ((double) this.goldnum <= 0.0)
    {
      this.panel.freeLabel.gameObject.SetActive(true);
      this.panel.goldIcon.gameObject.SetActive(false);
      this.panel.LB_GOLDNUM.gameObject.SetActive(false);
    }
    if (time <= 0 && (double) this.goldnum <= 0.0)
    {
      if (this.onCloseButtonClick != null)
        this.onCloseButtonClick();
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    }
    else
      this.panel.LB_LEFTTIME.text = ScriptLocalization.Get("id_time_remaining", true) + "[F57F00FF]" + Utils.FormatTime(this.lefttime, true, false, true) + "[-]";
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.param = orgParam as GoldConsumePopup.Parameter;
    this.panel.LB_description.text = this.param.description;
    this.onConfirmButtonClick = this.param.confirmButtonClickEvent;
    this.confirmButtonGoldNotEnoughClickEvent = this.param.confirmButtonGoldNotEnoughClickEvent;
    this.goldnum = (float) this.param.goldNum;
    this.panel.LB_GOLDNUM.text = this.param.goldNum.ToString();
    if ((double) GameEngine.Instance.PlayerData.hostPlayer.Currency >= (double) this.goldnum)
      this.panel.LB_GOLDNUM.color = this.panel.normalColor;
    else
      this.panel.LB_GOLDNUM.color = Color.red;
    this.lefttime = this.param.lefttime;
    this.panel.LB_LEFTTIME.text = ScriptLocalization.Get("id_time_remaining", true) + "[F57F00FF]" + Utils.FormatTime(this.lefttime, true, false, true) + "[-]";
    this.onCloseButtonClick = this.param.closeButtonCallbackEvent;
    this.freeTime = this.param.freetime;
    this.unitprice = this.param.unitprice;
    this.panel.freeLabel.gameObject.SetActive(false);
    if (this.param.lefttime > 0)
    {
      Oscillator.Instance.secondEvent += new System.Action<int>(this.Instance_secondEvent);
      this.panel.LB_LEFTTIME.gameObject.SetActive(true);
    }
    else
    {
      this.panel.LB_LEFTTIME.gameObject.SetActive(false);
      if ((double) this.goldnum > 0.0)
        return;
      this.panel.freeLabel.gameObject.SetActive(true);
      this.panel.goldIcon.gameObject.SetActive(false);
      this.panel.LB_GOLDNUM.gameObject.SetActive(false);
    }
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    if (this.onCloseButtonClick != null)
      this.onCloseButtonClick();
    if (this.param.lefttime <= 0 || !Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Instance_secondEvent);
  }

  [Serializable]
  protected class Panel
  {
    public Color normalColor = Color.white;
    public UILabel LB_description;
    public UILabel LB_GOLDNUM;
    public UILabel LB_LEFTTIME;
    public UILabel freeLabel;
    public UISprite goldIcon;
  }

  public class Parameter : Popup.PopupParameter
  {
    public System.Action confirmButtonClickEvent;
    public System.Action confirmButtonGoldNotEnoughClickEvent;
    public string description;
    public int goldNum;
    public int lefttime;
    public int freetime;
    public int unitprice;
    public System.Action closeButtonCallbackEvent;
  }
}
