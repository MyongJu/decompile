﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryKingOverDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class AnniversaryKingOverDlg : UI.Dialog
{
  public AnniversaryKingItem[] kings;
  private bool _isDesroy;

  protected override void _Open(UIControler.UIParameter orgParam)
  {
    base._Open(orgParam);
    this.AddEventHandler();
    AnniversaryManager.Instance.FetchKingsData();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.RemoveEventHandler();
  }

  private void UpdateUI()
  {
    if (this._isDesroy)
      return;
    for (int index = 0; index < this.kings.Length; ++index)
    {
      AnniversaryManager.AnniversarySlotData slotData = AnniversaryManager.Instance.GetSlotData(index + 1);
      AnniversaryChampionMainInfo data = ConfigManager.inst.DB_AnniversaryChampionMain.GetData((index + 1).ToString());
      if (slotData != null && data != null)
        this.kings[index].SetData(slotData, data);
    }
  }

  public void OnShowLeaderBorad()
  {
    UIManager.inst.OpenDlg("LeaderBoard/LeaderRankBoardDlg", (UI.Dialog.DialogParameter) new RankBoardDlg.Parameter()
    {
      LeaderBoardType = RankBoardDlg.LeaderBoardType.Kingdom
    }, true, true, true);
  }

  private void OnDestroy()
  {
    this._isDesroy = true;
  }

  private void AddEventHandler()
  {
    AnniversaryManager.Instance.OnDataUpdate += new System.Action(this.UpdateUI);
  }

  private void RemoveEventHandler()
  {
    AnniversaryManager.Instance.OnDataUpdate -= new System.Action(this.UpdateUI);
  }
}
