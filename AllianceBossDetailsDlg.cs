﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossDetailsDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UI;
using UnityEngine;

public class AllianceBossDetailsDlg : UI.Dialog
{
  private System.Action onRightButtonClick;
  [SerializeField]
  private AllianceBossDetailsDlg.Panel panel;

  public void OnSecond(int serverTime)
  {
    this.SetData();
  }

  public void OnRankingClick()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceBossRankingPopup", (Popup.PopupParameter) null);
  }

  private void OnBossListClick()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceBossTrialListDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnDonateClick()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceBossDonatePopup", (Popup.PopupParameter) null);
  }

  private void OnRallyClick()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceBossRallyPopup", (Popup.PopupParameter) null);
  }

  public void OnRightButtonClick()
  {
    if (this.onRightButtonClick == null)
      return;
    this.onRightButtonClick();
  }

  public void Reset()
  {
    this.panel.TT_Banner = this.transform.Find("Content/Cases/BossBanner").gameObject.GetComponent<UITexture>();
    this.panel.timeContainer = this.transform.Find("Content/Cases/LittleTitle").gameObject;
    this.panel.EndTime = this.transform.Find("Content/Cases/LittleTitle/TimeLabel").gameObject.GetComponent<UILabel>();
    this.panel.donateSliderContainer = this.transform.Find("Content/Cases/AllainceBossTechPoint").gameObject;
    this.panel.donateSlider = this.transform.Find("Content/Cases/AllainceBossTechPoint/ProgressBar").gameObject.GetComponent<UISlider>();
    this.panel.donateSliderText = this.transform.Find("Content/Cases/AllainceBossTechPoint/Exp").gameObject.GetComponent<UILabel>();
    this.panel.BT_viewRandking = this.transform.Find("Content/Cases/Btn_ViewRanking").gameObject.GetComponent<UIButton>();
    this.panel.BT_rightButton = this.transform.Find("Content/Cases/Btn_RightButton").gameObject.GetComponent<UIButton>();
    this.panel.BT_rightButtonText = this.transform.Find("Content/Cases/Btn_RightButton/Label").gameObject.GetComponent<UILabel>();
  }

  private void SetData()
  {
    AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_boss_donate_energy_max");
    switch (AllianceBuildUtils.GetAlliancePortalBossSummonState())
    {
      case AlliancePortalData.BossSummonState.CAN_ACTIVATE:
        this.panel.BT_rightButtonText.text = Utils.XLAT("alliance_portal_uppercase_open_portal");
        this.onRightButtonClick = new System.Action(this.OnBossListClick);
        this.panel.donateSliderContainer.SetActive(true);
        this.panel.donateSlider.value = 1f;
        this.panel.donateSliderText.text = string.Format("{0}/{1}", (object) data.ValueInt, (object) data.ValueInt);
        this.panel.timeContainer.SetActive(true);
        this.panel.EndTime.text = ActivityManager.Intance.GetPortalTimeStr();
        break;
      case AlliancePortalData.BossSummonState.ACTIVATED_TO_RALLY:
        this.panel.BT_rightButtonText.text = Utils.XLAT("alliance_portal_uppercase_enter_portal");
        this.onRightButtonClick = new System.Action(this.OnRallyClick);
        this.panel.donateSliderContainer.SetActive(false);
        this.panel.timeContainer.SetActive(true);
        this.panel.EndTime.text = ActivityManager.Intance.GetPortalTimeStr();
        break;
      default:
        int valueInt = ConfigManager.inst.DB_GameConfig.GetData("alliance_boss_donate_energy_max").ValueInt;
        if (alliancePortalData.CurrentEnergy == (long) valueInt)
        {
          this.panel.BT_rightButtonText.text = Utils.XLAT("alliance_portal_uppercase_open_portal");
          this.onRightButtonClick = new System.Action(this.OnBossListClick);
          this.panel.donateSliderContainer.SetActive(true);
          this.panel.donateSlider.value = 1f;
          this.panel.donateSliderText.text = string.Format("{0}/{1}", (object) data.ValueInt, (object) data.ValueInt);
          this.panel.timeContainer.SetActive(true);
          this.panel.EndTime.text = ActivityManager.Intance.GetPortalTimeStr();
          break;
        }
        this.panel.BT_rightButtonText.text = Utils.XLAT("id_uppercase_donate");
        this.onRightButtonClick = new System.Action(this.OnDonateClick);
        this.panel.donateSliderContainer.SetActive(true);
        this.panel.donateSlider.value = (float) alliancePortalData.CurrentEnergy / (float) data.ValueInt;
        this.panel.donateSliderText.text = string.Format("{0}/{1}", (object) alliancePortalData.CurrentEnergy, (object) data.ValueInt);
        this.panel.timeContainer.SetActive(true);
        this.panel.EndTime.text = ActivityManager.Intance.GetPortalTimeStr();
        break;
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    BuilderFactory.Instance.Build((UIWidget) this.panel.TT_Banner, "Texture/AllianceBoss/alliance_boss", (System.Action<bool>) null, true, false, true, string.Empty);
    this.SetData();
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  protected override void _Hide(UIControler.UIParameter orgParam)
  {
    base._Hide(orgParam);
    BuilderFactory.Instance.Release((UIWidget) this.panel.TT_Banner);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  [Serializable]
  public class Panel
  {
    public UIButton BT_viewRandking;
    public UITexture TT_Banner;
    public UIButton BT_rightButton;
    public UILabel BT_rightButtonText;
    public GameObject timeContainer;
    public UILabel EndTime;
    public GameObject donateSliderContainer;
    public UISlider donateSlider;
    public UILabel donateSliderText;
  }
}
