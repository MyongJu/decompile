﻿// Decompiled with JetBrains decompiler
// Type: BenefitItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BenefitItemRenderer : MonoBehaviour
{
  public static Color32 INCR_COLOR = new Color32((byte) 75, (byte) 155, (byte) 23, byte.MaxValue);
  public static Color32 DECR_COLOR = new Color32(byte.MaxValue, (byte) 0, (byte) 0, byte.MaxValue);
  public static Color32 DESC_COLOR = new Color32((byte) 152, (byte) 152, (byte) 152, (byte) 152);
  private bool useTargetImage = true;
  public const string TEXTURE_PATH = "Texture/Benefits/";
  public UITexture m_Icon;
  public UILabel m_Label;
  public UILabel valueLabel;

  public bool UseTargetImage
  {
    set
    {
      this.useTargetImage = value;
    }
  }

  public void SetData(double value, PropertyDefinition prop, bool reverseColor, Color32 positive, Color32 negative, Color32 tint)
  {
    if ((UnityEngine.Object) this.valueLabel != (UnityEngine.Object) null)
    {
      this.valueLabel.text = Utils.GetBenefitDescription(prop.Format, value, reverseColor, positive, negative);
      this.m_Label.text = Utils.GetColoredString(prop.Name, tint);
    }
    else
      this.m_Label.text = Utils.GetColoredString(Utils.GetBenefitDescription(prop.Format, value, reverseColor, positive, negative) + " " + prop.Name, tint);
    if (this.useTargetImage)
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, "Texture/Benefits/" + prop.TargetImage, (System.Action<bool>) null, true, false, string.Empty);
    else
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, prop.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
  }
}
