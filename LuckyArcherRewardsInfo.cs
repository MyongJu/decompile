﻿// Decompiled with JetBrains decompiler
// Type: LuckyArcherRewardsInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class LuckyArcherRewardsInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "group_id")]
  public int groupId;
  [Config(Name = "step")]
  public int step;
  [Config(Name = "refresh_weight")]
  public float refreshWeight;
  [Config(Name = "reward_item")]
  public int rewardId;
  [Config(Name = "value")]
  public int rewardCount;
  [Config(Name = "shoot_weight")]
  public float shootWeight;
  [Config(Name = "is_rare")]
  public int isRare;

  public bool IsRare
  {
    get
    {
      return this.isRare > 0;
    }
  }
}
