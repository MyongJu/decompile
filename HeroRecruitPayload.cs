﻿// Decompiled with JetBrains decompiler
// Type: HeroRecruitPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroRecruitPayload
{
  private List<HeroRecruitPayloadData> m_PayloadData = new List<HeroRecruitPayloadData>();

  public void Initialize(object orgData)
  {
    this.m_PayloadData.Clear();
    ArrayList arrayList = orgData as ArrayList;
    if (arrayList == null)
      return;
    for (int index = 0; index < arrayList.Count; ++index)
    {
      HeroRecruitPayloadData recruitPayloadData = new HeroRecruitPayloadData();
      if (recruitPayloadData.Decode(arrayList[index]))
        this.m_PayloadData.Add(recruitPayloadData);
    }
  }

  public List<HeroRecruitPayloadData> GetPayloadData()
  {
    for (int min = 0; min < this.m_PayloadData.Count; ++min)
    {
      int index = Random.Range(min, this.m_PayloadData.Count);
      HeroRecruitPayloadData recruitPayloadData = this.m_PayloadData[min];
      this.m_PayloadData[min] = this.m_PayloadData[index];
      this.m_PayloadData[index] = recruitPayloadData;
    }
    return this.m_PayloadData;
  }
}
