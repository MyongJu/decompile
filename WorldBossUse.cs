﻿// Decompiled with JetBrains decompiler
// Type: WorldBossUse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;

public class WorldBossUse : ItemBaseUse
{
  public WorldBossUse(int id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  public WorldBossUse(string id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  protected override void CustomUse(System.Action<bool, object> resultHandler)
  {
    if (MapUtils.IsPitWorld(PlayerData.inst.CityData.Location.K))
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("exception_2300150", true), (System.Action) null, 4f, true);
    }
    else
    {
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
        UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
        {
          GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
          PVPMap.PendingGotoRequest = PlayerData.inst.CityData.Location;
          PVPSystem.Instance.Map.EnterTeleportMode(PlayerData.inst.CityData.Location, TeleportMode.PLACE_WORLD_BOSS, this.Info.internalId, true);
        }));
      else
        PVPSystem.Instance.Map.EnterTeleportMode(PlayerData.inst.CityData.Location, TeleportMode.PLACE_WORLD_BOSS, this.Info.internalId, true);
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }
}
