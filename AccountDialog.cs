﻿// Decompiled with JetBrains decompiler
// Type: AccountDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Funplus;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AccountDialog : UI.Dialog
{
  private List<AccountBindInfoRenderer> m_ItemList = new List<AccountBindInfoRenderer>();
  public UITexture m_PlayerIcon;
  public UILabel m_PlayerName;
  public UILabel m_KingdomName;
  public UILabel m_CityLevel;
  public GameObject m_BindingPrefab;
  public UIGrid m_BindingGrid;
  private bool isDestroy;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    RequestManager.inst.SendRequest("player:syncAccounts", (Hashtable) null, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (this.isDestroy)
        return;
      if (arg1)
      {
        AccountManager.Instance.ClearAccounts();
        Hashtable hashtable = arg2 as Hashtable;
        for (int index = 0; index < 5; ++index)
        {
          string str1 = "account_name_" + (object) (index + 1);
          string str2 = "account_type_" + (object) (index + 1);
          string str3 = "account_mtime_" + (object) (index + 1);
          if (hashtable.ContainsKey((object) str1) && hashtable[(object) str1] != null && (hashtable.ContainsKey((object) str2) && hashtable[(object) str2] != null))
          {
            string id = hashtable[(object) str1].ToString();
            string type = hashtable[(object) str2].ToString();
            int result = 0;
            if (hashtable[(object) str3] != null)
              int.TryParse(hashtable[(object) str3].ToString(), out result);
            AccountManager.Instance.AddAccount(id, type, result);
          }
        }
      }
      this.UpdateUI();
    }), true);
  }

  private void OnDestroy()
  {
    this.isDestroy = true;
  }

  private void UpdateUI()
  {
    UserData userData = PlayerData.inst.userData;
    CityData playerCityData = PlayerData.inst.playerCityData;
    AllianceData allianceData = PlayerData.inst.allianceData;
    CustomIconLoader.Instance.requestCustomIcon(this.m_PlayerIcon, userData.PortraitIconPath, userData.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.m_PlayerIcon, userData.LordTitle, 1);
    string str = userData.userName;
    if (allianceData != null)
      str = string.Format("({0}){1}", (object) allianceData.allianceAcronym, (object) str);
    this.m_PlayerName.text = str;
    this.m_KingdomName.text = ScriptLocalization.GetWithPara("account_kingdom", new Dictionary<string, string>()
    {
      {
        "0",
        PlayerData.inst.userData.world_id.ToString()
      }
    }, true);
    this.m_CityLevel.text = ScriptLocalization.Get("account_stronghold_level", true) + " " + (object) playerCityData.level;
    this.UpdateAccount();
    this.AddEventHandler();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    BuilderFactory.Instance.Release((UIWidget) this.m_PlayerIcon);
    this.RemoveEventHandler();
  }

  private void ClearAccount()
  {
    for (int index = 0; index < this.m_ItemList.Count; ++index)
    {
      AccountBindInfoRenderer bindInfoRenderer = this.m_ItemList[index];
      bindInfoRenderer.transform.parent = (Transform) null;
      bindInfoRenderer.gameObject.SetActive(false);
      UnityEngine.Object.Destroy((UnityEngine.Object) bindInfoRenderer.gameObject);
    }
    this.m_ItemList.Clear();
  }

  private void AddEventHandler()
  {
    AccountManager.Instance.OnGetFacebookDataSuccess += new System.Action(this.OnBindAccountSuccessHandler);
    AccountManager.Instance.OnBindAccountSuccess += new System.Action(this.OnBindAccountSuccessHandler);
    AccountManager.Instance.OnUnBindAccountSuccess += new System.Action(this.OnBindAccountSuccessHandler);
  }

  private void OnBindAccountSuccessHandler()
  {
    this.UpdateAccount();
  }

  private void RemoveEventHandler()
  {
    AccountManager.Instance.OnGetFacebookDataSuccess -= new System.Action(this.OnBindAccountSuccessHandler);
    AccountManager.Instance.OnBindAccountSuccess -= new System.Action(this.OnBindAccountSuccessHandler);
    AccountManager.Instance.OnUnBindAccountSuccess -= new System.Action(this.OnBindAccountSuccessHandler);
  }

  private void UpdateAccount()
  {
    this.ClearAccount();
    if (AccountManager.Instance.CanShowFacebook())
    {
      if (AccountManager.Instance.IsBind(FunplusAccountType.FPAccountTypeFacebook))
        this.AddItem(this.GetTypeName(FunplusAccountType.FPAccountTypeFacebook), AccountManager.Instance.GetAccountName(FunplusAccountType.FPAccountTypeFacebook));
      else
        this.AddItem(this.GetTypeName(FunplusAccountType.FPAccountTypeFacebook), ScriptLocalization.Get("account_not_bound_status", true));
    }
    switch (Application.platform)
    {
      case RuntimePlatform.IPhonePlayer:
        if (AccountManager.Instance.IsBind(FunplusAccountType.FPAccountTypeGameCenter))
        {
          this.AddItem(this.GetTypeName(FunplusAccountType.FPAccountTypeGameCenter), AccountManager.Instance.GetAccountName(FunplusAccountType.FPAccountTypeGameCenter));
          break;
        }
        this.AddItem(this.GetTypeName(FunplusAccountType.FPAccountTypeGameCenter), ScriptLocalization.Get("account_not_bound_status", true));
        break;
      case RuntimePlatform.Android:
        if (AccountManager.Instance.CanShowGoolgePlay())
        {
          if (AccountManager.Instance.IsBind(FunplusAccountType.FPAccountTypeGooglePlus))
          {
            this.AddItem(this.GetTypeName(FunplusAccountType.FPAccountTypeGooglePlus), AccountManager.Instance.GetAccountName(FunplusAccountType.FPAccountTypeGooglePlus));
            break;
          }
          this.AddItem(this.GetTypeName(FunplusAccountType.FPAccountTypeGooglePlus), ScriptLocalization.Get("account_not_bound_status", true));
          break;
        }
        break;
    }
    if (AccountManager.Instance.CanShowVK())
    {
      if (AccountManager.Instance.IsBind(FunplusAccountType.FPAccountTypeVK))
        this.AddItem(this.GetTypeName(FunplusAccountType.FPAccountTypeVK), AccountManager.Instance.GetAccountName(FunplusAccountType.FPAccountTypeVK));
      else
        this.AddItem(this.GetTypeName(FunplusAccountType.FPAccountTypeVK), ScriptLocalization.Get("account_not_bound_status", true));
    }
    if (AccountManager.Instance.CanShowWechat())
    {
      if (AccountManager.Instance.IsBind(FunplusAccountType.FPAccountTypeWechat))
        this.AddItem(this.GetTypeName(FunplusAccountType.FPAccountTypeWechat), AccountManager.Instance.GetAccountName(FunplusAccountType.FPAccountTypeWechat));
      else
        this.AddItem(this.GetTypeName(FunplusAccountType.FPAccountTypeWechat), ScriptLocalization.Get("account_not_bound_status", true));
    }
    this.m_BindingGrid.Reposition();
  }

  private string GetTypeName(FunplusAccountType type)
  {
    switch (type)
    {
      case FunplusAccountType.FPAccountTypeFacebook:
        return ScriptLocalization.Get("account_facebook", true);
      case FunplusAccountType.FPAccountTypeVK:
        return ScriptLocalization.Get("account_vk", true);
      case FunplusAccountType.FPAccountTypeWechat:
        return ScriptLocalization.Get("account_wechat", true);
      case FunplusAccountType.FPAccountTypeGooglePlus:
        return ScriptLocalization.Get("account_googleplay", true);
      case FunplusAccountType.FPAccountTypeGameCenter:
        return ScriptLocalization.Get("account_gamecenter", true);
      default:
        return string.Empty;
    }
  }

  private void AddItem(string type, string id)
  {
    if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(id))
      return;
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_BindingPrefab);
    if (!(bool) ((UnityEngine.Object) gameObject))
      return;
    gameObject.SetActive(true);
    gameObject.transform.parent = this.m_BindingGrid.transform;
    gameObject.transform.localScale = Vector3.one;
    AccountBindInfoRenderer component = gameObject.GetComponent<AccountBindInfoRenderer>();
    component.SetAccount(type, id);
    this.m_ItemList.Add(component);
  }

  public void OnAccountManagement()
  {
    UIManager.inst.OpenPopup("Account/AccountManagementPopup", (Popup.PopupParameter) null);
  }

  public void OnSwitchAccount()
  {
    UIManager.inst.OpenPopup("Account/AccountSwitchPopup", (Popup.PopupParameter) null);
  }

  public void OnStartNewGame()
  {
    string empty = string.Empty;
    if (AccountManager.Instance.HadBindInfo())
      Utils.ShowWarning(ScriptLocalization.Get("account_warning_bound_new_game_description", true), ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.ConfirmSignupGame), (string) null);
    else
      Utils.ShowWarning(ScriptLocalization.Get("account_warning_not_bound_switch_description", true), ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.AgainConfirm), (string) null);
  }

  private void AgainConfirm()
  {
    Utils.ShowWarning(ScriptLocalization.Get("account_warning_not_bound_reset_description", true), ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.ConfirmSignupGame), (string) null);
  }

  private void ConfirmSignupGame()
  {
    AccountManager.Instance.Signup();
  }
}
