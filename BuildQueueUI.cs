﻿// Decompiled with JetBrains decompiler
// Type: BuildQueueUI
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BuildQueueUI : MonoBehaviour
{
  public BuildQueueIcon normal;
  public BuildQueueIcon addition;

  public void Init()
  {
    this.normal.Init();
    this.addition.Init();
  }

  public void Dispose()
  {
  }

  public void StartNormalJob(JobHandle jobHandle)
  {
    this.StartCoroutine(this.normal.StartJob(jobHandle));
  }

  public void StartAdditionJob(JobHandle jobHandle)
  {
    this.StartCoroutine(this.addition.StartJob(jobHandle));
  }
}
