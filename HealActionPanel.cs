﻿// Decompiled with JetBrains decompiler
// Type: HealActionPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HealActionPanel : MonoBehaviour
{
  private Dictionary<string, int> m_lackitem = new Dictionary<string, int>(1);
  private Dictionary<string, long> m_lackRes = new Dictionary<string, long>(4);
  private Hashtable itemHT = new Hashtable();
  public UILabel mFoodValue;
  public UILabel mWoodValue;
  public UILabel mOreValue;
  public UILabel mSilverValue;
  public UILabel selectedCount;
  public GameObject healingGameobject;
  public GameObject toHealGameobject;
  public HealBusyPanel busyPanel;
  public UIButton queueBtn;
  public UIButton instantBtn;
  public UIButton normalBtn;
  public UILabel instantLabel;
  public UILabel normalLabel;
  private List<HealTroopInfo> healList;
  private int gold;
  private System.Action OnQueueAllPressed;

  public void Init(List<HealTroopInfo> healList, System.Action OnQueueAllPressed)
  {
    this.healList = healList;
    this.OnQueueAllPressed = OnQueueAllPressed;
    this.SwitchStatus();
  }

  private void SwitchStatus()
  {
    CityManager.BuildingItem buildingWithActionTimer = CityManager.inst.GetBuildingWithActionTimer("hospital");
    this.toHealGameobject.SetActive(buildingWithActionTimer == null);
    this.healingGameobject.SetActive(buildingWithActionTimer != null);
    if (buildingWithActionTimer != null)
      this.busyPanel.Init();
    else
      this.CalculateCost();
    this.selectedCount.text = this.CalcTotalQueuedTroops().ToString();
  }

  private void CalculateCost()
  {
    double need1 = this.CalcTotalResourceCost(CityManager.ResourceTypes.FOOD, (string) null);
    double need2 = this.CalcTotalResourceCost(CityManager.ResourceTypes.WOOD, (string) null);
    double need3 = this.CalcTotalResourceCost(CityManager.ResourceTypes.SILVER, (string) null);
    double need4 = this.CalcTotalResourceCost(CityManager.ResourceTypes.ORE, (string) null);
    UIUtils.FormatResourceRequireLabel(this.mFoodValue, need1, CityResourceInfo.ResourceType.FOOD);
    UIUtils.FormatResourceRequireLabel(this.mWoodValue, need2, CityResourceInfo.ResourceType.WOOD);
    UIUtils.FormatResourceRequireLabel(this.mSilverValue, need3, CityResourceInfo.ResourceType.SILVER);
    UIUtils.FormatResourceRequireLabel(this.mOreValue, need4, CityResourceInfo.ResourceType.ORE);
    double seconds = this.CalcHealTime();
    this.normalLabel.text = Utils.ConvertSecsToString(seconds);
    UIButton instantBtn = this.instantBtn;
    bool flag = this.CalcTotalQueuedTroops() > 0L;
    this.normalBtn.isEnabled = flag;
    int num = flag ? 1 : 0;
    instantBtn.isEnabled = num != 0;
    this.queueBtn.isEnabled = this.healList.Count > 0;
    this.itemHT.Clear();
    this.itemHT.Add((object) "time", (object) (int) seconds);
    this.itemHT.Add((object) "resources", (object) new Hashtable()
    {
      {
        (object) ItemBag.ItemType.food,
        (object) (int) need1
      },
      {
        (object) ItemBag.ItemType.wood,
        (object) (int) need2
      },
      {
        (object) ItemBag.ItemType.silver,
        (object) (int) need3
      },
      {
        (object) ItemBag.ItemType.ore,
        (object) (int) need4
      }
    });
    this.instantLabel.text = ItemBag.CalculateCost(this.itemHT).ToString();
    this.itemHT.Remove((object) "time");
    this.gold = ItemBag.CalculateCost(this.itemHT);
    this.m_lackRes = ItemBag.CalculateLackResources(this.itemHT);
    this.m_lackitem = ItemBag.CalculateLackItems(this.itemHT);
  }

  public void OnQueueAllBtnPressed()
  {
    if (this.OnQueueAllPressed == null)
      return;
    this.OnQueueAllPressed();
  }

  public void OnInstantHealPressed()
  {
    int num = int.Parse(this.instantLabel.text);
    string withPara = ScriptLocalization.GetWithPara("confirm_gold_spend_heal_instant_desc", new Dictionary<string, string>()
    {
      {
        "num",
        num.ToString()
      }
    }, true);
    UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
    {
      confirmButtonClickEvent = (System.Action) (() => this.HealInstan()),
      description = withPara,
      goldNum = num
    });
  }

  private void HealInstan()
  {
    long mId = BuildingController.mSelectedBuildingItem.mID;
    MessageHub.inst.GetPortByAction("City:healTroops").SendRequest(Utils.Hash((object) "city_id", (object) CityManager.inst.GetCityID(), (object) "building_id", (object) mId.ToString(), (object) "troops", (object) this.CollectQueuedTroops(), (object) "instant", (object) true), (System.Action<bool, object>) ((bSuccess, data) =>
    {
      if (bSuccess)
        UIManager.inst.ShowBasicToast("toast_heal_timer_complete");
      Utils.ExecuteInSecs(0.1f, (System.Action) (() => UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null)));
    }), true);
  }

  private Hashtable CollectQueuedTroops()
  {
    Hashtable hashtable = new Hashtable();
    using (List<HealTroopInfo>.Enumerator enumerator = this.healList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HealTroopInfo current = enumerator.Current;
        hashtable[(object) current.ID] = (object) current.healing;
      }
    }
    return hashtable;
  }

  public void OnHealPressed()
  {
    this.CalculateCost();
    if (this.m_lackRes.Count > 0 || this.m_lackitem.Count > 0)
      UIManager.inst.OpenPopup("BuildingResPopUp", (Popup.PopupParameter) new BuildingResPopUp.Parameter()
      {
        lackItem = this.m_lackitem,
        lackRes = this.m_lackRes,
        requireRes = this.itemHT,
        action = Utils.XLAT("id_uppercase_instant_heal"),
        gold = this.gold,
        buyResourCallBack = new System.Action<int>(this.SendHealRequire),
        title = ScriptLocalization.Get("heal_notenoughresource_title", true),
        content = ScriptLocalization.Get("heal_notenoughresource_description", true)
      });
    else
      this.SendHealRequire(0);
  }

  private void SendHealRequire(int gold = 0)
  {
    if (PlayerData.inst.hostPlayer.Currency < gold)
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_gold_not_enough", true), (System.Action) null, 4f, false);
    }
    else
    {
      long mId = CityManager.inst.GetBuildingFromType("hospital").mID;
      MessageHub.inst.GetPortByAction("City:healTroops").SendRequest(Utils.Hash((object) "city_id", (object) CityManager.inst.GetCityID(), (object) "building_id", (object) mId.ToString(), (object) "troops", (object) this.CollectQueuedTroops(), (object) nameof (gold), (object) gold, (object) "instant", (object) false), (System.Action<bool, object>) ((bSuccess, data) =>
      {
        if (bSuccess)
          return;
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      }), true);
    }
  }

  private double CalcHealTime()
  {
    double num = 0.0;
    using (List<HealTroopInfo>.Enumerator enumerator = this.healList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HealTroopInfo current = enumerator.Current;
        Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(current.ID);
        num += (double) ConfigManager.inst.DB_BenefitCalc.GetFinalData(current.healing * data.Heal_Time, data.HEAL_TIME_CAL_LEY);
      }
    }
    return num;
  }

  private long CalcTotalQueuedTroops()
  {
    long num = 0;
    using (List<HealTroopInfo>.Enumerator enumerator = this.healList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HealTroopInfo current = enumerator.Current;
        num += (long) current.healing;
      }
    }
    return num;
  }

  private double CalcTotalResourceCost(CityManager.ResourceTypes resType, string excludeTroop = null)
  {
    double num = 0.0;
    using (List<HealTroopInfo>.Enumerator enumerator = this.healList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HealTroopInfo current = enumerator.Current;
        switch (resType)
        {
          case CityManager.ResourceTypes.FOOD:
            num += (double) current.healing * (double) ConfigManager.inst.DB_Unit_Statistics.GetData(current.ID).Heal_Food_Benifit;
            continue;
          case CityManager.ResourceTypes.SILVER:
            num += (double) current.healing * (double) ConfigManager.inst.DB_Unit_Statistics.GetData(current.ID).Heal_Silver_Benifit;
            continue;
          case CityManager.ResourceTypes.WOOD:
            num += (double) current.healing * (double) ConfigManager.inst.DB_Unit_Statistics.GetData(current.ID).Heal_Wood_Benifit;
            continue;
          case CityManager.ResourceTypes.ORE:
            num += (double) current.healing * (double) ConfigManager.inst.DB_Unit_Statistics.GetData(current.ID).Heal_Ore_Benifit;
            continue;
          default:
            continue;
        }
      }
    }
    return num;
  }
}
