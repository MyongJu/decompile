﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarRankItemData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class AllianceWarRankItemData
{
  public string AllianceName = string.Empty;
  public string LeaderName = string.Empty;
  public string Language = string.Empty;
  public long AllianceId;
  public int Rank;
  public int Kingdom;
  public long Score;
  public int AllianceSymbole;
  public int MemberCount;
  public int MaxMemberCount;
  public long Power;
  public int Portrait;

  public void Decode(Hashtable data)
  {
    DatabaseTools.UpdateData(data, "alliance_id", ref this.AllianceId);
    DatabaseTools.UpdateData(data, "portrait", ref this.Portrait);
    DatabaseTools.UpdateData(data, "world_id", ref this.Kingdom);
    DatabaseTools.UpdateData(data, "name", ref this.AllianceName);
    DatabaseTools.UpdateData(data, "score", ref this.Score);
    DatabaseTools.UpdateData(data, "symbol_code", ref this.AllianceSymbole);
    DatabaseTools.UpdateData(data, "alliance_creator", ref this.LeaderName);
    DatabaseTools.UpdateData(data, "language", ref this.Language);
    DatabaseTools.UpdateData(data, "member_count", ref this.MemberCount);
    DatabaseTools.UpdateData(data, "member_max", ref this.MaxMemberCount);
    DatabaseTools.UpdateData(data, "power", ref this.Power);
  }
}
