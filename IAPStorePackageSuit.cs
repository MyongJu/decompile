﻿// Decompiled with JetBrains decompiler
// Type: IAPStorePackageSuit
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class IAPStorePackageSuit : IAPStoreRenderer
{
  private List<IAPStorePackageBanner> m_ItemList = new List<IAPStorePackageBanner>();
  public IAPStorePackageRenderer m_PackageRenderer;
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  public GameObject m_ItemPrefab;
  public System.Action OnPurchaseBegin;
  public System.Action<int> OnPurchaseEnd;

  public void SetData(List<IAPStorePackage> packages)
  {
    for (int index = 0; index < packages.Count; ++index)
    {
      IAPStorePackage package = packages[index];
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_Grid.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localScale = Vector3.one;
      IAPStorePackageBanner component = gameObject.GetComponent<IAPStorePackageBanner>();
      component.SetData(package, new System.Action<IAPStorePackage>(this.OnItemSelected));
      this.m_ItemList.Add(component);
    }
    this.m_ItemList[0].Select();
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  public override void UpdateUI()
  {
    this.m_PackageRenderer.UpdateUI();
  }

  public void FocusPackage(int groudId)
  {
    IAPStorePackageBanner packageItem = this.FindPackageItem(groudId);
    if (!(bool) ((UnityEngine.Object) packageItem))
      return;
    packageItem.Select();
    this.FocusChild(packageItem.transform);
  }

  private void OnItemSelected(IAPStorePackage package)
  {
    this.m_PackageRenderer.SetData(package, this.OnPurchaseBegin, this.OnPurchaseEnd, (System.Action) null);
  }

  private IAPStorePackageBanner FindPackageItem(int groupId)
  {
    for (int index = 0; index < this.m_ItemList.Count; ++index)
    {
      IAPStorePackageBanner storePackageBanner = this.m_ItemList[index];
      if ((UnityEngine.Object) storePackageBanner != (UnityEngine.Object) null)
      {
        IAPStorePackage package = storePackageBanner.Package;
        if (groupId == package.group.internalId)
          return storePackageBanner;
      }
    }
    return (IAPStorePackageBanner) null;
  }

  private void FocusChild(Transform t)
  {
    Vector3[] worldCorners = this.m_ScrollView.panel.worldCorners;
    Vector3 position = (worldCorners[2] + worldCorners[0]) * 0.5f;
    Vector3 vector3 = this.m_ScrollView.panel.transform.InverseTransformPoint(t.position) - this.m_ScrollView.panel.transform.InverseTransformPoint(position);
    this.m_ScrollView.panel.transform.localPosition = this.m_ScrollView.panel.transform.localPosition - vector3;
    Vector4 clipOffset = (Vector4) this.m_ScrollView.panel.clipOffset;
    clipOffset.x += vector3.x;
    clipOffset.y += vector3.y;
    this.m_ScrollView.panel.clipOffset = (Vector2) clipOffset;
    Bounds bounds = this.m_ScrollView.bounds;
    this.m_ScrollView.MoveRelative(this.m_ScrollView.panel.CalculateConstrainOffset((Vector2) bounds.min, (Vector2) bounds.max));
  }
}
