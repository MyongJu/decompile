﻿// Decompiled with JetBrains decompiler
// Type: MarchActionState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class MarchActionState : MarchBaseState
{
  private bool actionComplete;
  private float time;
  private bool flag;

  public override string Key
  {
    get
    {
      return "march_action";
    }
  }

  protected override void Prepare()
  {
    this.time = 0.0f;
    this.Controler.DestoryPath();
    this.Controler.HideClickCollider();
    NGUITools.SetActive(this.Controler.gameObject, false);
    MarchData.MarchType marchType = this.StateData.marchType;
    switch (marchType)
    {
      case MarchData.MarchType.city_attack:
      case MarchData.MarchType.encamp_attack:
      case MarchData.MarchType.monster_attack:
      case MarchData.MarchType.gve_rally_attack:
        this.NormalAttack();
        break;
      case MarchData.MarchType.fortress_attack:
        if (this.StateData.isCanAttackFortress)
        {
          this.NormalAttack();
          break;
        }
        this.actionComplete = true;
        break;
      case MarchData.MarchType.rally_attack:
        if (!this.StateData.marchData.IsWonderOrTower)
        {
          this.NormalAttack();
          break;
        }
        this.actionComplete = true;
        break;
      default:
        if (marchType != MarchData.MarchType.attack && marchType != MarchData.MarchType.world_boss_attack)
        {
          this.actionComplete = true;
          break;
        }
        goto case MarchData.MarchType.city_attack;
    }
  }

  private void NormalAttack()
  {
    if ((this.StateData.marchState == MarchData.MarchState.encamping || this.StateData.marchState == MarchData.MarchState.gathering) && this.IsInitState)
    {
      NGUITools.SetActive(this.Controler.gameObject, false);
      this.actionComplete = true;
    }
    else
    {
      this.time = Time.time;
      NGUITools.SetActive(this.Controler.gameObject, true);
      this.actionComplete = false;
      this.Controler.AttackAction();
      AudioManager.Instance.PlaySound("sfx_march_attacking", false);
      PVPSystem.Instance.Controller.AddAttacker(this.StateData.marchId, this.StateData.targetLocation);
    }
  }

  public override void OnExit()
  {
    base.OnExit();
    PVPSystem.Instance.Controller.RemoveAttacker(this.StateData.marchId, this.StateData.targetLocation);
  }

  private void ActionAnimationComplete()
  {
    this.actionComplete = true;
  }

  protected override void OnProcessHandler()
  {
    if ((Object) this.Controler == (Object) null || this.Controler.marchData == null || (Object) this.Controler.gameObject == (Object) null)
      return;
    if ((double) this.time > 0.0)
    {
      if ((double) Time.time - (double) this.time < 10.0)
        return;
      this.actionComplete = true;
    }
    if (!this.actionComplete)
      return;
    if (this.actionComplete && (this.StateData.marchType == MarchData.MarchType.attack || this.StateData.marchType == MarchData.MarchType.encamp_attack || this.StateData.marchType == MarchData.MarchType.gather_attack) && (this.StateData.marchData.state == MarchData.MarchState.encamping || this.StateData.marchData.state == MarchData.MarchState.gathering))
      NGUITools.SetActive(this.Controler.gameObject, false);
    if (this.StateData.marchData.rallyId <= 0L && !this.StateData.isFortressAttack)
    {
      this.ChangeState();
    }
    else
    {
      if (this.StateData.marchData.rallyId > 0L && !this.StateData.isFortressAttack)
      {
        if (this.RallyAttack)
        {
          this.ForceDone();
          return;
        }
        if (this.Controler.RallyIsDone())
          this.ChangeState();
      }
      if (!this.StateData.isFortressAttack)
        return;
      this.ChangeState();
    }
  }

  private bool RallyAttack
  {
    get
    {
      if (this.StateData.marchType != MarchData.MarchType.rally_attack && this.StateData.marchType != MarchData.MarchType.gve_rally_attack)
        return this.StateData.marchType == MarchData.MarchType.rab_rally_attack;
      return true;
    }
  }

  private void ForceDone()
  {
    if (!this.flag)
    {
      GameEngine.Instance.marchSystem.MarchDoneHandler(this.Controler.marchData.rallyId, this.Controler.rallyJoinMarchId);
      this.flag = true;
    }
    this.ChangeState();
  }

  private void ChangeState()
  {
    if ((Object) this.Controler == (Object) null || this.Controler.marchData == null)
      return;
    if (this.Controler.HasFinishStateData())
    {
      if (this.RallyAttack)
        GameEngine.Instance.marchSystem.MarchDoneHandler(this.Controler.marchData.rallyId, this.Controler.rallyJoinMarchId);
      this.Controler.SetFinishState();
    }
    else if (this.Controler.HasReturingStateData())
    {
      this.Controler.NextState();
    }
    else
    {
      if (!((Object) this.Controler.gameObject != (Object) null))
        return;
      NGUITools.SetActive(this.Controler.gameObject, false);
    }
  }
}
