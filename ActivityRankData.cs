﻿// Decompiled with JetBrains decompiler
// Type: ActivityRankData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;

public class ActivityRankData
{
  public int rank;
  public string playerName;
  public string acronym;
  public long worldId;
  public long allianceId;
  private string _worldName;

  public string WorldName
  {
    get
    {
      if (!string.IsNullOrEmpty(this._worldName))
        return this._worldName;
      if (this.worldId <= 0L)
        return string.Empty;
      return ScriptLocalization.GetWithPara("kingdom_name", new Dictionary<string, string>()
      {
        {
          "0",
          this.worldId.ToString()
        }
      }, true);
    }
  }

  public bool Decode(string rankContent, object orgData)
  {
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable == null || !int.TryParse(rankContent, out this.rank))
      return false;
    this.playerName = hashtable[(object) "name"].ToString();
    this.acronym = hashtable[(object) "acronym"].ToString();
    string empty = string.Empty;
    if (hashtable.ContainsKey((object) "aid") && hashtable[(object) "aid"] != null)
      long.TryParse(hashtable[(object) "aid"].ToString(), out this.allianceId);
    if (hashtable.ContainsKey((object) "world_id") && hashtable[(object) "world_id"] != null)
      long.TryParse(hashtable[(object) "world_id"].ToString(), out this.worldId);
    if (hashtable.ContainsKey((object) "kingdom_name") && hashtable[(object) "kingdom_name"] != null)
      this._worldName = hashtable[(object) "kingdom_name"].ToString();
    return true;
  }

  public string FullName
  {
    get
    {
      if (string.IsNullOrEmpty(this.acronym))
        return this.playerName;
      return "[" + this.acronym + "] " + this.playerName;
    }
  }
}
