﻿// Decompiled with JetBrains decompiler
// Type: LeaderDetailItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;
using UnityEngine;

public class LeaderDetailItem : MonoBehaviour
{
  private bool _showKingdom = true;
  private const string ICON_FOLDER_PATH = "Texture/LeaderboardIcons/";
  private const string FIRST_CROWN_PATH = "icon_no1";
  private const string SECOND_CROWN_PATH = "icon_no2";
  private const string THIRD_CROWN_PATH = "icon_no3";
  public UITexture IconTexture;
  public UILabel Name;
  public UILabel Fighting;
  public UILabel rank;
  public UILabel Level;
  public UITexture texture;
  public UILabel kingdom;
  private long itemOrder;
  private long playerID;
  public AllianceSymbol symbol;
  public GameObject playerIcon;
  public UISprite[] bgs;
  private RankData _data;
  private bool _showKingdomName;

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.IconTexture);
    BuilderFactory.Instance.Release((UIWidget) this.texture);
  }

  public void SetData(RankData data, bool showKingdomInName = false, bool hideKingdom = false)
  {
    this._showKingdomName = showKingdomInName;
    this._showKingdom = !hideKingdom;
    this._data = data;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this._data == null)
      return;
    this.Level.text = string.Empty;
    string remoteIcon = string.Empty;
    int lordTitleId = 0;
    if (this._data is AllianceRankData)
    {
      if ((UnityEngine.Object) this.playerIcon != (UnityEngine.Object) null)
      {
        NGUITools.SetActive(this.symbol.gameObject, true);
        NGUITools.SetActive(this.playerIcon, false);
      }
    }
    else if (this._data is DragonLevelRankData)
      this.Level.text = (this._data as DragonLevelRankData).DragonLevel.ToString();
    else if (this._data is LordLevelRankData)
      this.Level.text = (this._data as LordLevelRankData).LordLevel.ToString();
    else if ((UnityEngine.Object) this.symbol != (UnityEngine.Object) null)
    {
      NGUITools.SetActive(this.symbol.gameObject, false);
      NGUITools.SetActive(this.playerIcon, true);
    }
    NGUITools.SetActive(this.rank.gameObject, false);
    NGUITools.SetActive(this.texture.gameObject, true);
    for (int index = 0; index < this.bgs.Length; ++index)
      NGUITools.SetActive(this.bgs[index].gameObject, false);
    this.symbol.SetSymbols(this._data.Symbol);
    int index1;
    if (this._data.Rank == 1)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.texture, "Texture/LeaderboardIcons/icon_no1", (System.Action<bool>) null, true, false, string.Empty);
      index1 = 0;
    }
    else if (this._data.Rank == 2)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.texture, "Texture/LeaderboardIcons/icon_no2", (System.Action<bool>) null, true, false, string.Empty);
      index1 = 1;
    }
    else if (this._data.Rank == 3)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.texture, "Texture/LeaderboardIcons/icon_no3", (System.Action<bool>) null, true, false, string.Empty);
      index1 = 2;
    }
    else
    {
      NGUITools.SetActive(this.texture.gameObject, false);
      NGUITools.SetActive(this.rank.gameObject, true);
      this.rank.text = this._data.Rank.ToString();
      index1 = 3;
      if (this._data.Rank % 2 == 0)
        index1 = 4;
    }
    if (this.bgs.Length > 0)
      NGUITools.SetActive(this.bgs[index1].gameObject, true);
    if (this._data is PlayerRankData)
    {
      PlayerRankData data = this._data as PlayerRankData;
      remoteIcon = data.CustomIconUrl;
      lordTitleId = data.LordTitleId;
    }
    CustomIconLoader.Instance.requestCustomIcon(this.IconTexture, this._data.Image, remoteIcon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.IconTexture, lordTitleId, 2);
    this.Name.text = this._showKingdomName ? this._data.FullName : this._data.Name;
    this.Fighting.text = Utils.FormatThousands(this._data.Score.ToString());
    if (!(bool) ((UnityEngine.Object) this.kingdom))
      return;
    if (this._showKingdom)
      this.kingdom.text = this._data.KingdomId.ToString();
    else
      this.kingdom.text = string.Empty;
  }

  public void OnItemClicked()
  {
    if (this._data is AllianceRankData)
    {
      AllianceRankData allianceRankData = this._data as AllianceRankData;
      Hashtable postData = new Hashtable();
      postData[(object) "alliance_id"] = (object) allianceRankData.AllianceId;
      MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        UIManager.inst.OpenDlg("Alliance/AllianceJoinAndApplyDialog", (UI.Dialog.DialogParameter) new AllianceJoinAndApplyDialog.Parameter()
        {
          allianceData = DBManager.inst.DB_Alliance.Get((long) allianceRankData.AllianceId),
          justView = (PlayerData.inst.allianceId > 0L)
        }, true, true, true);
      }), true);
    }
    else
      UIManager.inst.OpenPopup("LeaderBoard/LeaderBoardPlayerInfo", (Popup.PopupParameter) new LeaderBoardPlayerInfo.LeaderBoardPlayerInfoParameter()
      {
        playerID = this.playerID,
        data = this._data,
        showKingdom = this._showKingdom
      });
  }
}
