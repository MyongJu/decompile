﻿// Decompiled with JetBrains decompiler
// Type: DungeonChestStaticInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DungeonChestStaticInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "level")]
  public string Level;
  [Config(Name = "name")]
  public string Name;
  [Config(Name = "description")]
  public string Description;
  [Config(Name = "image")]
  public string Image;

  public string ImageFullPath
  {
    get
    {
      return "Prefabs/Chest/" + this.Image;
    }
  }
}
