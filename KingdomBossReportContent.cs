﻿// Decompiled with JetBrains decompiler
// Type: KingdomBossReportContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class KingdomBossReportContent
{
  public string k;
  public string x;
  public string y;
  public KingdomBossMember attacker;
  public List<KingdomBossMemberDetail> attacker_troop_detail;
  public KingdomBossMember defender;

  public string site
  {
    get
    {
      return "K: " + this.k + " X: " + this.x + " Y: " + this.y;
    }
  }
}
