﻿// Decompiled with JetBrains decompiler
// Type: FallenKnightScoreReqItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class FallenKnightScoreReqItemRenderer : MonoBehaviour
{
  private Dictionary<int, FallenKnightScoreReqItemRenderer.RankRewardPair> dicScoreReqInfo = new Dictionary<int, FallenKnightScoreReqItemRenderer.RankRewardPair>();
  public UILabel rankRewardNum;
  public UILabel individualPointsReq;
  public UILabel alliancePointsReq;
  public UILabel resultAchieved;
  public UILabel resultUnachieved;
  public GameObject tickIcon1;
  public GameObject tickIcon2;
  public GameObject crossIcon1;
  public GameObject crossIcon2;
  public FallenKnightScoreReqItemRenderer.IconInfo[] itemIconInfos;
  private FallenKnightScoreReqInfo scoreReqInfo;
  private long individualScore;
  private long allianceScore;

  public void SetData(FallenKnightScoreReqInfo info, long selfScore, long allianceScore)
  {
    this.scoreReqInfo = info;
    this.individualScore = selfScore;
    this.allianceScore = allianceScore;
    this.UpdateUI();
  }

  public void UpdateUI()
  {
    this.rankRewardNum.text = string.Format(Utils.XLAT("event_fallen_knight_points_reward"), (object) ConfigManager.inst._DB_FallenKnightScoreReq.GetScoreInfoId(this.scoreReqInfo));
    this.individualPointsReq.text = string.Format(Utils.XLAT("event_fallen_knight_required_player_points"), (object) this.scoreReqInfo.IndividualReqScore);
    this.alliancePointsReq.text = string.Format(Utils.XLAT("event_fallen_knight_required_alliance_points"), (object) this.scoreReqInfo.AllianceReqScore);
    if (this.individualScore >= this.scoreReqInfo.IndividualReqScore)
    {
      this.individualPointsReq.color = Color.green;
      this.tickIcon1.gameObject.SetActive(true);
    }
    else
    {
      this.individualPointsReq.color = Color.red;
      this.crossIcon1.SetActive(true);
    }
    if (this.allianceScore >= this.scoreReqInfo.AllianceReqScore)
    {
      this.alliancePointsReq.color = Color.green;
      this.tickIcon2.SetActive(true);
    }
    else
    {
      this.alliancePointsReq.color = Color.red;
      this.crossIcon2.SetActive(true);
    }
    if (this.individualScore < this.scoreReqInfo.IndividualReqScore || this.allianceScore < this.scoreReqInfo.AllianceReqScore)
      this.resultUnachieved.gameObject.SetActive(true);
    else
      this.resultAchieved.gameObject.SetActive(true);
    this.dicScoreReqInfo.Clear();
    this.dicScoreReqInfo.Add(0, new FallenKnightScoreReqItemRenderer.RankRewardPair()
    {
      rankRewardName = "alliance_fund",
      rankRewardCount = this.scoreReqInfo.RewardFund.ToString()
    });
    this.dicScoreReqInfo.Add(1, new FallenKnightScoreReqItemRenderer.RankRewardPair()
    {
      rankRewardName = "alliance_honor",
      rankRewardCount = this.scoreReqInfo.RewardHonor.ToString()
    });
    this.dicScoreReqInfo.Add(2, new FallenKnightScoreReqItemRenderer.RankRewardPair()
    {
      rankRewardName = this.scoreReqInfo.RankReward_1,
      rankRewardCount = this.scoreReqInfo.RankRewardValue_1
    });
    this.dicScoreReqInfo.Add(3, new FallenKnightScoreReqItemRenderer.RankRewardPair()
    {
      rankRewardName = this.scoreReqInfo.RankReward_2,
      rankRewardCount = this.scoreReqInfo.RankRewardValue_2
    });
    this.dicScoreReqInfo.Add(4, new FallenKnightScoreReqItemRenderer.RankRewardPair()
    {
      rankRewardName = this.scoreReqInfo.RankReward_3,
      rankRewardCount = this.scoreReqInfo.RankRewardValue_3
    });
    this.dicScoreReqInfo.Add(5, new FallenKnightScoreReqItemRenderer.RankRewardPair()
    {
      rankRewardName = this.scoreReqInfo.RankReward_4,
      rankRewardCount = this.scoreReqInfo.RankRewardValue_4
    });
    this.dicScoreReqInfo.Add(6, new FallenKnightScoreReqItemRenderer.RankRewardPair()
    {
      rankRewardName = this.scoreReqInfo.RankReward_5,
      rankRewardCount = this.scoreReqInfo.RankRewardValue_5
    });
    this.dicScoreReqInfo.Add(7, new FallenKnightScoreReqItemRenderer.RankRewardPair()
    {
      rankRewardName = this.scoreReqInfo.RankReward_6,
      rankRewardCount = this.scoreReqInfo.RankRewardValue_6
    });
    int index1 = 0;
    int index2 = 0;
    for (; index1 < this.dicScoreReqInfo.Count; ++index1)
    {
      if (!string.IsNullOrEmpty(this.dicScoreReqInfo[index1].rankRewardName) && long.Parse(this.dicScoreReqInfo[index1].rankRewardCount) > 0L)
      {
        IconData iconData = this.GetIconData(this.dicScoreReqInfo[index1].rankRewardName, this.dicScoreReqInfo[index1].rankRewardCount);
        this.itemIconInfos[index2].itemIcon.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
        this.itemIconInfos[index2].itemIcon.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
        this.itemIconInfos[index2].itemIcon.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
        this.itemIconInfos[index2].itemIcon.FeedData((IComponentData) iconData);
        this.itemIconInfos[index2].itemName.text = iconData.contents[0];
        this.itemIconInfos[index2].itemNumber.text = Utils.FormatThousands(this.dicScoreReqInfo[index1].rankRewardCount.ToString());
        this.itemIconInfos[index2].itemName.transform.parent.gameObject.SetActive(true);
        if (iconData.Data == null)
          this.itemIconInfos[index2].itemIcon.LoadDefaultBackgroud();
        ++index2;
      }
    }
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  private IconData GetIconData(string item_id, string count)
  {
    IconData iconData = new IconData();
    iconData.contents = new string[2];
    if (item_id.Contains("alliance_fund") || item_id.Contains("alliance_honor"))
    {
      iconData.image = "Texture/Alliance/" + item_id;
      iconData.contents[0] = Utils.XLAT(!item_id.Contains("alliance_fund") ? "id_honor" : "id_fund");
    }
    else
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(int.Parse(item_id));
      iconData.image = itemStaticInfo.ImagePath;
      iconData.contents[0] = itemStaticInfo.LocName;
      iconData.Data = (object) itemStaticInfo.internalId;
    }
    iconData.contents[1] = count;
    return iconData;
  }

  [Serializable]
  public class IconInfo
  {
    public Icon itemIcon;
    public UILabel itemName;
    public UILabel itemNumber;
  }

  public class RankRewardPair
  {
    public string rankRewardName;
    public string rankRewardCount;
  }
}
