﻿// Decompiled with JetBrains decompiler
// Type: NumberController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class NumberController : MonoBehaviour
{
  public GameObject NumberPrefab;

  public void Play(string text, Color color)
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this.NumberPrefab);
    gameObject.SetActive(true);
    gameObject.transform.SetParent(this.transform, false);
    TextMesh component1 = gameObject.GetComponent<TextMesh>();
    if ((bool) ((Object) component1))
    {
      component1.text = text;
      component1.color = color;
    }
    UILabel component2 = gameObject.GetComponent<UILabel>();
    if (!(bool) ((Object) component2))
      return;
    component2.text = text;
    component2.color = color;
  }
}
