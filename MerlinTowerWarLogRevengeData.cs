﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerWarLogRevengeData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class MerlinTowerWarLogRevengeData
{
  public int pageIndex = -1;
  public int index = -1;
  public long uid;
  public int k;
  public string allianceTag;
  public string name;
  public int reward;
  public int endTime;
  public int item_id;
  public long count;

  public bool Decode(object orgData)
  {
    Hashtable inData = orgData as Hashtable;
    if (inData == null)
      return false;
    DatabaseTools.UpdateData(inData, "uid", ref this.uid);
    DatabaseTools.UpdateData(inData, "uname", ref this.name);
    DatabaseTools.UpdateData(inData, "atag", ref this.allianceTag);
    DatabaseTools.UpdateData(inData, "k", ref this.k);
    DatabaseTools.UpdateData(inData, "end_time", ref this.endTime);
    DatabaseTools.UpdateData(inData, "item_id", ref this.item_id);
    DatabaseTools.UpdateData(inData, "count", ref this.count);
    DatabaseTools.UpdateData(inData, "page", ref this.pageIndex);
    DatabaseTools.UpdateData(inData, "index", ref this.index);
    return this.endTime <= 0 || this.endTime - NetServerTime.inst.ServerTimestamp > 0;
  }

  public string FullName
  {
    get
    {
      string str = this.name;
      if (!string.IsNullOrEmpty(this.allianceTag))
        str = string.Format("[{0}]{1}", (object) this.allianceTag, (object) this.name);
      if (this.k > 0)
        str = str + ".K" + (object) this.k;
      return str;
    }
  }

  public struct Params
  {
    public const string USER_ID = "uid";
    public const string USER_NAME = "uname";
    public const string ALLIANCE_TAG = "atag";
    public const string KID = "k";
    public const string END_TIME = "end_time";
    public const string ITEM_ID = "item_id";
    public const string COUNT = "count";
    public const string PAGE = "page";
    public const string INDEX = "index";
  }
}
