﻿// Decompiled with JetBrains decompiler
// Type: OptionItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class OptionItem : MonoBehaviour
{
  private const string ICON_FOLDER_PATH = "Texture/LeaderboardIcons/";
  public const int CATEGORY_ALLIANCE_POWER = 0;
  public const int CATEGORY_ALLIANCE_TROOPS_KILLED = 1;
  public const int CATEGORY_PLAYER_POWER = 2;
  public const int CATEGORY_PLAYER_TROOPS_KILLED = 5;
  public const int CATEGORY_DRAGON_LEVEL = 3;
  public const int CATEGORY_STRONGHOLD = 6;
  public const int CATEGORY_LORD_LEVEL = 4;
  public const int CATEGORY_MAX = 7;
  public UITexture optIconTexture;
  public UILabel optText;
  private int optItemID;
  private RankBoardDlg.LeaderBoardType _LeadBoardType;
  private string title;

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.optIconTexture);
    NGUITools.SetActive(this.gameObject, false);
  }

  public void SetInfo(int id, RankBoardDlg.LeaderBoardType leadboardType = RankBoardDlg.LeaderBoardType.Kingdom)
  {
    this._LeadBoardType = leadboardType;
    this.optItemID = id;
    switch (this.optItemID)
    {
      case 0:
        BuilderFactory.Instance.HandyBuild((UIWidget) this.optIconTexture, "Texture/LeaderboardIcons/icon_alliance_power", (System.Action<bool>) null, true, false, string.Empty);
        this.optText.text = ScriptLocalization.Get("leaderboards_uppercase_alliance_power", true);
        break;
      case 1:
        BuilderFactory.Instance.HandyBuild((UIWidget) this.optIconTexture, "Texture/LeaderboardIcons/icon_alliance_troops_killed", (System.Action<bool>) null, true, false, string.Empty);
        this.optText.text = ScriptLocalization.Get("leaderboards_uppercase_alliance_kills", true);
        break;
      case 2:
        BuilderFactory.Instance.HandyBuild((UIWidget) this.optIconTexture, "Texture/LeaderboardIcons/icon_individual_power", (System.Action<bool>) null, true, false, string.Empty);
        this.optText.text = ScriptLocalization.Get("leaderboards_uppercase_individual_power", true);
        break;
      case 3:
        BuilderFactory.Instance.HandyBuild((UIWidget) this.optIconTexture, "Texture/LeaderboardIcons/icon_individual_dragon_Lv", (System.Action<bool>) null, true, false, string.Empty);
        this.optText.text = ScriptLocalization.Get("leaderboards_uppercase_dragon_level", true);
        break;
      case 4:
        BuilderFactory.Instance.HandyBuild((UIWidget) this.optIconTexture, "Texture/LeaderboardIcons/icon_individual_lord_lv", (System.Action<bool>) null, true, false, string.Empty);
        this.optText.text = ScriptLocalization.Get("leaderboards_uppercase_lord_level", true);
        break;
      case 5:
        BuilderFactory.Instance.HandyBuild((UIWidget) this.optIconTexture, "Texture/LeaderboardIcons/icon_individual_troops_killed", (System.Action<bool>) null, true, false, string.Empty);
        this.optText.text = ScriptLocalization.Get("leaderboards_uppercase_individual_kills", true);
        break;
      case 6:
        BuilderFactory.Instance.HandyBuild((UIWidget) this.optIconTexture, "Texture/LeaderboardIcons/icon_stronghold_level", (System.Action<bool>) null, true, false, string.Empty);
        this.optText.text = ScriptLocalization.Get("leaderboards_uppercase_stronghold", true);
        break;
    }
  }

  public void OnOptionItemClicked()
  {
    this.SendRequest();
  }

  private void SendRequest()
  {
    string str1 = "Player:loadUserLeaderboard";
    Hashtable hashtable = new Hashtable();
    string str2 = string.Empty;
    if (this.optItemID < 2)
    {
      str1 = "Alliance:loadAllianceLeaderboard";
      hashtable.Add((object) "alliance_id", (object) PlayerData.inst.allianceId);
    }
    switch (this.optItemID)
    {
      case 0:
        str2 = "power";
        break;
      case 1:
        str2 = "troops_killed";
        break;
      case 2:
        str2 = "power";
        break;
      case 3:
        str2 = "dragon";
        break;
      case 4:
        str2 = "lord_level";
        break;
      case 5:
        str2 = "troops_killed";
        break;
      case 6:
        str2 = "stronghold_level";
        break;
    }
    hashtable.Add((object) "category", (object) str2);
    UIManager.inst.OpenDlg("LeaderBoard/LeaderBoardDetailDlg", (UI.Dialog.DialogParameter) new LeaderBoardDetailDlg.LeaderBoardDetailDlgParamer()
    {
      method = str1,
      param = hashtable,
      optItemID = this.optItemID,
      title = this.optText.text
    }, true, true, true);
  }

  private void ResultHandler(bool success, object result)
  {
    if (!success)
      return;
    LeaderBoardDetailDlg.LeaderBoardDetailDlgParamer detailDlgParamer = new LeaderBoardDetailDlg.LeaderBoardDetailDlgParamer();
    detailDlgParamer.optItemID = this.optItemID;
    detailDlgParamer.datas = this.ParseData(result as Hashtable);
    detailDlgParamer.current = this.GetCurrentRankData(result as Hashtable, detailDlgParamer.datas);
    detailDlgParamer.title = this.optText.text;
    UIManager.inst.OpenDlg("LeaderBoard/LeaderBoardDetailDlg", (UI.Dialog.DialogParameter) detailDlgParamer, true, true, true);
  }

  private RankData GetCurrentRankData(Hashtable sources, List<RankData> allRankData)
  {
    if (!sources.ContainsKey((object) "mine"))
      return (RankData) null;
    return this.CreateRankData(sources[(object) "mine"] as Hashtable);
  }

  private List<RankData> ParseData(Hashtable sources)
  {
    List<RankData> rankDataList = new List<RankData>();
    if (sources.ContainsKey((object) "ranks"))
    {
      ArrayList source = sources[(object) "ranks"] as ArrayList;
      for (int index = 0; index < source.Count; ++index)
      {
        RankData rankData = this.CreateRankData(source[index] as Hashtable);
        rankDataList.Add(rankData);
      }
    }
    return rankDataList;
  }

  private RankData CreateRankData(Hashtable source)
  {
    RankData rankData;
    if (source.ContainsKey((object) "alliance_id"))
      rankData = (RankData) new AllianceRankData()
      {
        AllianceId = int.Parse(source[(object) "alliance_id"].ToString()),
        Lang = source[(object) "lang"].ToString(),
        Member = int.Parse(source[(object) "member"].ToString())
      };
    else
      rankData = (RankData) new PlayerRankData()
      {
        Portrait = int.Parse(source[(object) "portrait"].ToString())
      };
    rankData.Name = source[(object) "name"].ToString();
    rankData.Score = long.Parse(source[(object) "score"].ToString());
    rankData.Rank = int.Parse(source[(object) "rank"].ToString());
    if (source.ContainsKey((object) "uid"))
      rankData.UID = source[(object) "uid"].ToString();
    if (source.ContainsKey((object) "tag"))
    {
      rankData.Tag = source[(object) "tag"].ToString();
      rankData.Symbol = int.Parse(source[(object) "symbol_code"].ToString());
    }
    return rankData;
  }
}
