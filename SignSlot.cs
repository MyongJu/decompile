﻿// Decompiled with JetBrains decompiler
// Type: SignSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class SignSlot : MonoBehaviour
{
  public UITexture m_ItemIcon;
  public UISprite m_Highlight;
  public UISprite m_Check;
  public UISprite m_Grayout;
  public UILabel m_Tip;
  public UITexture m_Background;
  public GameObject m_NormalBg;
  private SignInData m_SignInData;
  private int m_SlotIndex;
  private int m_ItemId;

  public void SetData(SignInData signInData, int slotIndex)
  {
    this.m_SignInData = signInData;
    this.m_SlotIndex = slotIndex;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    List<DropMainData> rewardList = this.m_SignInData.GetRewardList();
    if (rewardList.Count == 1)
    {
      this.m_ItemId = rewardList[0].ItemId;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardList[0].ItemId);
      Utils.SetItemBackground(this.m_Background, itemStaticInfo.internalId);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      NGUITools.SetActive(this.m_NormalBg, false);
      NGUITools.SetActive(this.m_Background.gameObject, true);
    }
    else
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, "Texture/ItemIcons/item_mystery_box", (System.Action<bool>) null, true, false, string.Empty);
      NGUITools.SetActive(this.m_NormalBg, true);
      NGUITools.SetActive(this.m_Background.gameObject, false);
    }
    Dictionary<string, string> para = new Dictionary<string, string>();
    para["num"] = (this.m_SlotIndex + 1).ToString();
    this.m_Tip.text = ScriptLocalization.GetWithPara("continuous_sign_in_number", para, true);
  }

  public void OnItemPress()
  {
    Utils.DelayShowTip(this.m_ItemId, this.m_Background.transform, 0L, 0L, 0);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  public void OnRewardClicked()
  {
    List<DropMainData> rewardList = this.m_SignInData.GetRewardList();
    if (rewardList.Count > 1)
    {
      UIManager.inst.OpenPopup("Sign/SignRewardPopup", (Popup.PopupParameter) new SignRewardPopup.Parameter()
      {
        items = rewardList
      });
    }
    else
    {
      if (rewardList.Count != 1)
        return;
      UIManager.inst.OpenPopup("Sign/SignSingleRewardPopup", (Popup.PopupParameter) new SignSingleRewardPopup.Parameter()
      {
        dropMainData = rewardList[0]
      });
    }
  }

  public void SetPast()
  {
    this.SetHighlightActive(false);
    this.SetCheckActive(true);
    this.SetGrayoutActive(true);
  }

  public void SetUnsigned()
  {
    this.SetHighlightActive(true);
    this.SetCheckActive(false);
    this.SetGrayoutActive(false);
  }

  public void SetNormal()
  {
    this.SetHighlightActive(false);
    this.SetCheckActive(false);
    this.SetGrayoutActive(false);
  }

  private void SetHighlightActive(bool active)
  {
    this.m_Highlight.gameObject.SetActive(active);
  }

  private void SetCheckActive(bool active)
  {
    this.m_Check.gameObject.SetActive(active);
  }

  private void SetGrayoutActive(bool active)
  {
    this.m_Grayout.gameObject.SetActive(active);
  }
}
