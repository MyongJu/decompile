﻿// Decompiled with JetBrains decompiler
// Type: Tile_StatisticsInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class Tile_StatisticsInfo
{
  public int internalId;
  public string ID;
  public string Tile_ID;
  public string Tile_Name_LOC_ID;
  public string Tile_Description_LOC_ID;
  public string Tile_Image_ID;
  public int Level;
  public string Resource_Type;
  public int Amount_Stored;
  public float Gathering_Rate;
  public string Reward_ID_1;
  public string Reward_ID_2;
  public string Reward_ID_3;
  public string Reward_ID_4;
  public string Reward_ID_5;
  public string Reward_ID_6;
  public string Reward_ID_7;
  public string Reward_ID_8;
  public string Reward_ID_9;
}
