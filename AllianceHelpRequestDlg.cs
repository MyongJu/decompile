﻿// Decompiled with JetBrains decompiler
// Type: AllianceHelpRequestDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceHelpRequestDlg : UI.Dialog
{
  private List<GameObject> m_ItemList = new List<GameObject>();
  public GameObject m_HelpRequestItemPrefab;
  public GameObject m_NoDataItem;
  public GameObject m_HelpAllButton;
  public UIScrollView m_ScrollView;
  public UITable m_Table;

  public void OnCancelPressed()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void OnPushLeave(object data)
  {
    this.OnHelpChanged();
  }

  private void OnHelpChanged()
  {
    this.LoadHelpRequests();
  }

  private void OnLeaveAlliance()
  {
    this.OnCancelPressed();
  }

  private void ClearData()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_ItemList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.m_ItemList.Clear();
  }

  private void LoadHelpRequests()
  {
    if (this._state.isStatesInvalid)
      return;
    this.ClearData();
    List<AllianceHelpData> allianceHelpDataList1 = new List<AllianceHelpData>();
    List<AllianceHelpData> allianceHelpDataList2 = new List<AllianceHelpData>();
    AllianceData allianceData = PlayerData.inst.allianceData;
    if (allianceData != null)
    {
      using (Dictionary<AllianceJobID, AllianceHelpData>.ValueCollection.Enumerator enumerator = allianceData.helps.datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AllianceHelpData current = enumerator.Current;
          int logCount = current.GetLogCount();
          int num = current.helpAvailable + logCount;
          if (current.identifier.uid != PlayerData.inst.uid)
          {
            if (!current.FindMyLog(PlayerData.inst.uid) && logCount < num)
              allianceHelpDataList1.Add(current);
          }
          else
            allianceHelpDataList2.Add(current);
        }
      }
    }
    this.m_NoDataItem.SetActive(allianceHelpDataList1.Count + allianceHelpDataList2.Count == 0);
    this.m_HelpAllButton.SetActive(allianceHelpDataList1.Count > 0);
    allianceHelpDataList2.AddRange((IEnumerable<AllianceHelpData>) allianceHelpDataList1);
    using (List<AllianceHelpData>.Enumerator enumerator = allianceHelpDataList2.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceHelpData current = enumerator.Current;
        if (current != null)
        {
          GameObject gameObject = Utils.DuplicateGOB(this.m_HelpRequestItemPrefab);
          gameObject.SetActive(true);
          this.m_ItemList.Add(gameObject);
          gameObject.GetComponent<AllianceHelpRequestItem>().SetDetails(current);
        }
      }
    }
    this.m_Table.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  public void OnHelpAllPressed()
  {
    AllianceManager.Instance.HelpAll((System.Action<bool, object>) ((_param1, _param2) =>
    {
      AudioManager.Instance.StopAndPlaySound("sfx_alliance_help_request");
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_help_success", true), (System.Action) null, 4f, false);
      this.LoadHelpRequests();
    }));
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    AllianceManager.Instance.onHelpChanged += new System.Action(this.OnHelpChanged);
    AllianceManager.Instance.onPushLeave += new System.Action<object>(this.OnPushLeave);
    AllianceManager.Instance.onLeave += new System.Action(this.OnLeaveAlliance);
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((_param1, _param2) => this.LoadHelpRequests()), true);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    AllianceManager.Instance.onHelpChanged -= new System.Action(this.OnHelpChanged);
    AllianceManager.Instance.onPushLeave -= new System.Action<object>(this.OnPushLeave);
    AllianceManager.Instance.onLeave -= new System.Action(this.OnLeaveAlliance);
    this.ClearData();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public bool showBack;
  }
}
