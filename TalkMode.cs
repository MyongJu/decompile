﻿// Decompiled with JetBrains decompiler
// Type: TalkMode
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class TalkMode : MonoBehaviour, INPCMode
{
  public UILabel content;

  public void SeedData(object param)
  {
    if (LocalizationManager.CurrentLanguageCode == "ar")
      this.content.text = ScriptLocalization.Get(param as string, true);
    else
      this.content.text = "[381400]" + ScriptLocalization.Get(param as string, true) + "[-]";
  }
}
