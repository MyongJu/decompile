﻿// Decompiled with JetBrains decompiler
// Type: CityInfoRename
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Diagnostics;
using UI;

public class CityInfoRename : UI.Dialog
{
  private const string CHANGE_NAME_SHOPITEM_ID = "shopitem_city_rename";
  public UILabel mPropsOwnedValue;
  public UILabel mCharsRemainingValue;
  public UILabel mEnterNameLabel;
  public UIInput mCityNameInput;
  public UILabel mUseBtnValue;
  public UILabel mGetAndUseBtnValue;
  public UILabel mCityNameInputLabel;
  public UIButton mUseBtn;
  public UIButton mGetAndUseBtn;
  public UISprite mBusySprite;
  public UISprite mStatusSprite;
  public UILabel mErrorMsg;
  public UILabel mNormalMsg;
  private IEnumerator _CR;
  private int _itemsAvailable;
  private bool _ignoreInput;

  public void SetDetails()
  {
    this._ignoreInput = true;
    this.mCityNameInput.value = CityManager.inst.GetName();
    Utils.ExecuteInSecs(0.1f, (System.Action) (() => this._ignoreInput = false));
    this._itemsAvailable = ItemBag.Instance.GetItemCountByShopID("shopitem_city_rename");
    int shopItemPrice = ItemBag.Instance.GetShopItemPrice("shopitem_city_rename");
    this.mPropsOwnedValue.text = ScriptLocalization.Get("id_own_colon", true) + "[aaaaaa]" + this._itemsAvailable.ToString();
    if (this._itemsAvailable > 0)
    {
      this.mUseBtn.gameObject.SetActive(true);
      this.mGetAndUseBtn.gameObject.SetActive(false);
    }
    else
    {
      this.mUseBtn.gameObject.SetActive(false);
      this.mGetAndUseBtn.gameObject.SetActive(true);
    }
    this.mUseBtnValue.text = "1";
    this.mGetAndUseBtnValue.text = Utils.FormatThousands(shopItemPrice.ToString());
    this.mCharsRemainingValue.text = "0/15";
    this.mStatusSprite.spriteName = "red_cross";
    this.mBusySprite.gameObject.SetActive(false);
    this.mUseBtn.isEnabled = false;
    this.mGetAndUseBtn.isEnabled = false;
    this.mCharsRemainingValue.gameObject.SetActive(false);
    this.mStatusSprite.gameObject.SetActive(false);
    this.mErrorMsg.gameObject.SetActive(false);
  }

  public void OnCloseBtn()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnInputChanged()
  {
    if (this._ignoreInput)
      return;
    this.mCharsRemainingValue.gameObject.SetActive(true);
    this.mStatusSprite.gameObject.SetActive(true);
    this.mErrorMsg.gameObject.SetActive(false);
    this.mStatusSprite.spriteName = "red_cross";
    this.mCharsRemainingValue.text = string.Format("{0}/15", (object) this.mCityNameInputLabel.text.Length);
    this.mUseBtn.isEnabled = false;
    this.mGetAndUseBtn.isEnabled = false;
    this.mNormalMsg.gameObject.SetActive(true);
    this.mErrorMsg.gameObject.SetActive(false);
    if (this._CR != null)
      this.StopCoroutine(this._CR);
    this.StartCoroutine(this._CR = this.CheckName());
  }

  [DebuggerHidden]
  private IEnumerator CheckName()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CityInfoRename.\u003CCheckName\u003Ec__Iterator3A()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void CheckCityCallback(bool ret, object data)
  {
    if (ret)
    {
      this.mStatusSprite.gameObject.SetActive(true);
      this.mBusySprite.gameObject.SetActive(false);
      this.mStatusSprite.spriteName = "green_tick";
      if (this._itemsAvailable > 0)
        this.mUseBtn.isEnabled = true;
      else
        this.mGetAndUseBtn.isEnabled = true;
    }
    else
    {
      this.mStatusSprite.gameObject.SetActive(true);
      this.mBusySprite.gameObject.SetActive(false);
      this.mStatusSprite.spriteName = "red_cross";
      this.mNormalMsg.gameObject.SetActive(false);
      this.mErrorMsg.gameObject.SetActive(true);
      this.mErrorMsg.text = ScriptLocalization.Get((data as Hashtable)[(object) "errmsg"].ToString(), true);
    }
  }

  public void OnUseBtnPressed()
  {
    ItemBag.Instance.UseItemByShopItemID("shopitem_city_rename", Utils.Hash((object) "buy", (object) false, (object) "name", (object) this.mCityNameInput.value), (System.Action<bool, object>) ((_param1, _param2) =>
    {
      CityManager.inst.SetCityName(this.mCityNameInput.value);
      UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
      UIManager.inst.toast.Show(Utils.XLAT("toast_city_name_change_success"), (System.Action) null, 4f, false);
    }), 1);
  }

  public void OnGetAndUseBtnPressed()
  {
    ItemBag.Instance.BuyAndUseShopItem("shopitem_city_rename", Utils.Hash((object) "buy", (object) true, (object) "name", (object) this.mCityNameInput.value), (System.Action<bool, object>) ((arg1, arg2) =>
    {
      CityManager.inst.SetCityName(this.mCityNameInput.value);
      UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
      UIManager.inst.toast.Show(Utils.XLAT("toast_city_name_change_success"), (System.Action) null, 4f, false);
    }), 1);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.SetDetails();
  }
}
