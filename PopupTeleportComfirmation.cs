﻿// Decompiled with JetBrains decompiler
// Type: PopupTeleportComfirmation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using UI;
using UnityEngine;

public class PopupTeleportComfirmation : UI.Dialog
{
  private const string SHOPITEM_TELEPORT = "shopitem_targettele";
  private const string SHOPITEM_RANDOM = "shopitem_randomtele";
  private const string SHOPITEM_BEGINNER = "shopitem_beginnertele";
  private Coordinate _teleportTargetLocation;
  [SerializeField]
  private PopupTeleportComfirmation.Panel panel;

  public void OnTeleportPressed()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    if (DBManager.inst.DB_March.marchOwner.Count > 0)
      UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
      {
        title = Utils.XLAT("id_uppercase_warning"),
        description = Utils.XLAT("teleport_no_march"),
        buttonClickEvent = (System.Action) (() => UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null))
      });
    else if (DBManager.inst.DB_March.GetTargetListByType(MarchData.MarchType.reinforce).Count > 0)
      UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
      {
        title = "Warning",
        description = "Can't teleport with reinforcement.",
        buttonClickEvent = (System.Action) (() => UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null))
      });
    else if (this.CanTeleport())
    {
      this.Teleport();
    }
    else
    {
      if (!ItemBag.Instance.CheckCanBuyShopItem("shopitem_targettele", false))
        return;
      this.BuyAndTeleport();
    }
  }

  public void OnClosePressed()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void ShowStoreGUI()
  {
    UIManager.inst.OpenDlg("ItemStore/ItemStoreDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private bool CanTeleport()
  {
    CityData myCityData = PlayerData.inst.CityData.MyCityData;
    if (this._teleportTargetLocation.K != myCityData.cityLocation.K)
    {
      if (ItemBag.Instance.GetItemCountByShopID("shopitem_randomtele") > 0)
        return myCityData.level <= 5;
      return false;
    }
    if (ItemBag.Instance.GetItemCountByShopID("shopitem_targettele") > 0)
      return true;
    if (ItemBag.Instance.GetItemCountByShopID("shopitem_targettele") > 0)
      return myCityData.level <= 5;
    return false;
  }

  private void Teleport()
  {
    if (this._teleportTargetLocation.K != PlayerData.inst.CityData.Location.K)
    {
      Hashtable extra = new Hashtable();
      extra[(object) "kingdom_id"] = (object) this._teleportTargetLocation.K;
      extra[(object) "map_x"] = (object) this._teleportTargetLocation.X;
      extra[(object) "map_y"] = (object) this._teleportTargetLocation.Y;
      ItemBag.Instance.UseItem(ConfigManager.inst.DB_Shop.GetShopData("shopitem_randomtele").Item_InternalId, 1, extra, new System.Action<bool, object>(this.TeleportCallback));
    }
    else
    {
      Hashtable extra = new Hashtable();
      extra[(object) "kingdom_id"] = (object) this._teleportTargetLocation.K;
      extra[(object) "map_x"] = (object) this._teleportTargetLocation.X;
      extra[(object) "map_y"] = (object) this._teleportTargetLocation.Y;
      ItemBag.Instance.UseItem(ConfigManager.inst.DB_Shop.GetShopData("shopitem_targettele").Item_InternalId, 1, extra, new System.Action<bool, object>(this.TeleportCallback));
    }
  }

  private void BuyAndTeleport()
  {
    Hashtable extra = new Hashtable();
    extra[(object) "kingdom_id"] = (object) this._teleportTargetLocation.K;
    extra[(object) "map_x"] = (object) this._teleportTargetLocation.X;
    extra[(object) "map_y"] = (object) this._teleportTargetLocation.Y;
    ItemBag.Instance.BuyAndUseShopItem("shopitem_targettele", extra, new System.Action<bool, object>(this.BuyAndTeleportCallback), 1);
  }

  private void BuyAndTeleportCallback(bool ret, object data)
  {
  }

  private void TeleportCallback(bool ret, object data)
  {
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    PopupTeleportComfirmation.Parameter parameter = orgParam as PopupTeleportComfirmation.Parameter;
    if (parameter == null)
      return;
    this._teleportTargetLocation = parameter.coordinate;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.panel.coordinateK.text = this._teleportTargetLocation.K.ToString();
    this.panel.coordinateX.text = this._teleportTargetLocation.X.ToString();
    this.panel.coordinateY.text = this._teleportTargetLocation.Y.ToString();
    this.panel.ownNumber.text = string.Format("Own: {0}", (object) ItemBag.Instance.GetItemCountByShopID("shopitem_targettele"));
    if (this.CanTeleport())
    {
      this.panel.teleport.gameObject.SetActive(true);
      this.panel.buyAndTeleport.gameObject.SetActive(false);
    }
    else if (ItemBag.Instance.CheckCanBuyShopItem("shopitem_targettele", false))
    {
      ConfigManager.inst.DB_Items.GetItem(ConfigManager.inst.DB_Shop.GetShopData("shopitem_targettele").Item_InternalId);
      this.panel.teleport.gameObject.SetActive(false);
      this.panel.buyAndTeleport.gameObject.SetActive(true);
      this.panel.buyAndTeleport.isEnabled = true;
      this.panel.goldPrice.text = ItemBag.Instance.GetShopItemPrice("shopitem_targettele").ToString();
    }
    else
    {
      this.panel.teleport.gameObject.SetActive(false);
      this.panel.buyAndTeleport.gameObject.SetActive(true);
      this.panel.buyAndTeleport.isEnabled = false;
      this.panel.goldPrice.text = ItemBag.Instance.GetShopItemPrice("shopitem_targettele").ToString();
    }
  }

  [Serializable]
  protected class Panel
  {
    public UILabel coordinateK;
    public UILabel coordinateX;
    public UILabel coordinateY;
    public UILabel ownNumber;
    public UIButton teleport;
    public UIButton buyAndTeleport;
    public UILabel goldPrice;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public Coordinate coordinate;
  }
}
