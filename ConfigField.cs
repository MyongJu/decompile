﻿// Decompiled with JetBrains decompiler
// Type: ConfigField
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Reflection;

public class ConfigField
{
  public FieldInfo field;
  public ConfigAttribute attrib;

  public ConfigField(FieldInfo field, ConfigAttribute attrib)
  {
    this.field = field;
    this.attrib = attrib;
  }
}
