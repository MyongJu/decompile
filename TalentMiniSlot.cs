﻿// Decompiled with JetBrains decompiler
// Type: TalentMiniSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UnityEngine;

[RequireComponent(typeof (UIEventTrigger))]
[RequireComponent(typeof (UIDragScrollView))]
[RequireComponent(typeof (BoxCollider))]
public class TalentMiniSlot : MonoBehaviour
{
  private TalentMiniSlot.Data _data = new TalentMiniSlot.Data();
  public System.Action<int> onSkillClick;
  [SerializeField]
  private TalentMiniSlot.Panel panel;

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void Setup(int skillInternalId)
  {
    this._data.skillActiveId = skillInternalId;
    this.FreshSlot();
    PlayerTalentInfo playerTalentInfo = ConfigManager.inst.DB_PlayerTalent.GetPlayerTalentInfo(this._data.skillInfo.skillId);
    if (playerTalentInfo == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.icon, playerTalentInfo.imagePath, (System.Action<bool>) null, true, false, string.Empty);
  }

  public void FreshSlot()
  {
    if (this._data.skillActiveId == 0)
      return;
    DB.HeroData heroData = PlayerData.inst.heroData;
    if (heroData == null)
      return;
    HeroSkills.Skill skill = heroData.skills.GetSkill((long) this._data.skillInfo.skillId);
    TalentData talentData = DBManager.inst.DB_Talent.Get(this._data.skillInfo.skillId);
    if (skill != null && talentData != null && talentData.InCurrentGroup(PlayerData.inst.heroData.skillGroupId))
    {
      this.panel.eventTriggle.enabled = true;
      GreyUtility.Normal(this.panel.icon.gameObject);
      if (skill != null)
      {
        if (skill.jobId != 0L)
        {
          this.panel.activeTime.gameObject.SetActive(true);
          this.panel.cdTime.gameObject.SetActive(false);
          this.panel.activeTime.text = Utils.FormatTime(JobManager.Instance.GetJob(skill.jobId).LeftTime(), false, false, true);
        }
        else
        {
          this.panel.activeTime.gameObject.SetActive(false);
          if (skill.activeTime > NetServerTime.inst.ServerTimestamp)
          {
            this.panel.cdTime.gameObject.SetActive(true);
            this.panel.cdTime.text = Utils.FormatTime(skill.activeTime - NetServerTime.inst.ServerTimestamp, false, false, true);
          }
          else
            this.panel.cdTime.gameObject.SetActive(false);
        }
      }
      else
      {
        this.panel.activeTime.gameObject.SetActive(false);
        this.panel.cdTime.gameObject.SetActive(false);
      }
    }
    else
    {
      this.panel.eventTriggle.enabled = false;
      this.panel.activeTime.gameObject.SetActive(false);
      this.panel.cdTime.gameObject.SetActive(false);
      GreyUtility.Grey(this.panel.icon.gameObject);
    }
    this.panel.timerBackground.gameObject.SetActive(this.panel.activeTime.gameObject.activeSelf || this.panel.cdTime.gameObject.activeSelf);
  }

  public void OnSkillClick()
  {
    if (this.onSkillClick == null)
      return;
    this.onSkillClick(this._data.skillActiveId);
  }

  public void OnSecond(int serverTime)
  {
    this.FreshSlot();
  }

  public void Reset()
  {
    this.panel.icon = this.transform.Find("TimerPanel/Icon").gameObject.GetComponent<UITexture>();
    this.panel.timerBackground = this.transform.Find("TimerPanel/TimeBg").gameObject.transform;
    this.panel.activeTime = this.transform.Find("TimerPanel/ActiveTime").gameObject.GetComponent<UILabel>();
    this.panel.cdTime = this.transform.Find("TimerPanel/CoolDownTime").gameObject.GetComponent<UILabel>();
    this.panel.eventTriggle = this.GetComponent<UIEventTrigger>();
  }

  private class Data
  {
    private int _skillActiveId;

    public int skillActiveId
    {
      get
      {
        return this._skillActiveId;
      }
      set
      {
        this._skillActiveId = value;
        this.skillInfo = ConfigManager.inst.DB_HeroTalentActive.GetItem(this._skillActiveId);
      }
    }

    public HeroTalentActiveInfo skillInfo { get; private set; }
  }

  [Serializable]
  protected class Panel
  {
    public UITexture icon;
    public Transform timerBackground;
    public UILabel activeTime;
    public UILabel cdTime;
    public UIEventTrigger eventTriggle;
  }
}
