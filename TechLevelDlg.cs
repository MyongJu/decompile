﻿// Decompiled with JetBrains decompiler
// Type: TechLevelDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TechLevelDlg : MonoBehaviour
{
  public UILabel Title;
  public UILabel Description;
  public UILabel Effect;
  public UILabel NextEffect;
  public UILabel[] Requirements;
  public UISprite Image;
  public UILabel RankLabel;
  public UILabel NextEffectLabel;
  public UILabel RequirementsLabel;
  public UIButton researchButton;
  private TechLevel _nextTechLevel;
  [HideInInspector]
  public string tech;

  private void Start()
  {
  }

  private void OnDestroy()
  {
  }

  public void OnCloseButtonPressed()
  {
    this.gameObject.SetActive(false);
  }

  public void OnResearchButtonPressed()
  {
    if (this._nextTechLevel == null)
      return;
    this.researchButton.enabled = false;
    this._nextTechLevel.State = TechLevel.ResearchState.InProgress;
    this.gameObject.SetActive(false);
  }

  public void Show(string tech)
  {
    this.tech = tech;
    this._nextTechLevel = (TechLevel) null;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
  }
}
