﻿// Decompiled with JetBrains decompiler
// Type: BuildingResPopUp
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingResPopUp : Popup
{
  private Queue<GameObject> gos = new Queue<GameObject>(4);
  private GameObjectPool resPool = new GameObjectPool();
  private Dictionary<string, Texture2D> ImagePools = new Dictionary<string, Texture2D>();
  public GameObject ResGroup;
  public GameObject ItemGroup;
  public UILabel needGold;
  public UILabel actionType;
  public UILabel title;
  public UILabel content;
  public Hashtable requireRes;
  public Color normarlColor;
  public EventDelegate ed;
  private int gold;
  private System.Action<int> buyResourCallBack;
  public Dictionary<string, int> mlackItem;
  public Dictionary<string, long> mlackRes;
  public float gapX;

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    this.buyResourCallBack = (System.Action<int>) null;
    this.ed = (EventDelegate) null;
  }

  public void OnActionBtnPressed()
  {
    if (this.buyResourCallBack != null)
      this.buyResourCallBack(this.gold);
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  private void Init()
  {
    if (this.mlackItem.Count > 0)
    {
      this.ItemGroup.SetActive(true);
      Dictionary<string, int>.Enumerator enumerator = this.mlackItem.GetEnumerator();
      while (enumerator.MoveNext())
      {
        this.ItemGroup.GetComponentInChildren<UILabel>().text = enumerator.Current.Value.ToString();
        BuilderFactory.Instance.HandyBuild((UIWidget) this.ItemGroup.GetComponentInChildren<UITexture>(), enumerator.Current.Key, (System.Action<bool>) null, true, false, string.Empty);
      }
    }
    if (this.mlackRes.Count > 0)
    {
      int count = this.mlackRes.Count;
      List<Vector3> vector3List = new List<Vector3>(count);
      for (int index = 0; index < count; ++index)
        vector3List.Add(Vector3.zero);
      Vector3 localPosition = this.ResGroup.transform.localPosition;
      if (count % 2 == 0)
      {
        for (int index = count / 2 - 1; index >= 0; --index)
        {
          if (count / 2 - 1 == index)
          {
            vector3List[index] = new Vector3(localPosition.x - this.gapX / 2f, localPosition.y, localPosition.z);
            vector3List[count - 1 - index] = new Vector3(localPosition.x + this.gapX / 2f, localPosition.y, localPosition.z);
          }
          else
          {
            Vector3 vector3_1 = vector3List[index + 1];
            Vector3 vector3_2 = vector3List[count - 1 - index - 1];
            vector3List[index] = new Vector3(vector3_1.x - this.gapX, vector3_1.y, vector3_1.z);
            vector3List[count - 1 - index] = new Vector3(vector3_2.x + this.gapX, vector3_2.y, vector3_2.z);
          }
        }
      }
      else
      {
        vector3List[count / 2] = localPosition;
        for (int index = count / 2 - 1; index >= 0; --index)
        {
          Vector3 vector3_1 = vector3List[index + 1];
          Vector3 vector3_2 = vector3List[count - 1 - index - 1];
          vector3List[index] = new Vector3(vector3_1.x - this.gapX, vector3_1.y, vector3_1.z);
          vector3List[count - 1 - index] = new Vector3(vector3_2.x + this.gapX, vector3_2.y, vector3_2.z);
        }
      }
      int index1 = 0;
      Queue<GameObject> gameObjectQueue = new Queue<GameObject>();
      Dictionary<string, long>.Enumerator enumerator1 = this.mlackRes.GetEnumerator();
      while (enumerator1.MoveNext())
      {
        string s = enumerator1.Current.Value.ToString();
        if (!string.IsNullOrEmpty(s))
        {
          GameObject go = this.GetItem();
          if (!go.activeSelf)
            NGUITools.SetActive(go, true);
          go.transform.localPosition = vector3List[index1];
          UILabel componentInChildren = go.GetComponentInChildren<UILabel>();
          int result = 0;
          int.TryParse(s, out result);
          componentInChildren.text = Utils.FormatShortThousands(result);
          BuilderFactory.Instance.HandyBuild((UIWidget) go.GetComponentInChildren<UITexture>(), enumerator1.Current.Key, (System.Action<bool>) null, true, false, string.Empty);
          gameObjectQueue.Enqueue(go);
          ++index1;
        }
      }
      using (Queue<GameObject>.Enumerator enumerator2 = this.gos.GetEnumerator())
      {
        while (enumerator2.MoveNext())
        {
          GameObject current = enumerator2.Current;
          if (!gameObjectQueue.Contains(current))
            this.resPool.Release(current);
        }
      }
      this.gos = gameObjectQueue;
    }
    this.ItemGroup.SetActive(false);
    this.ResGroup.SetActive(false);
  }

  private GameObject GetItem()
  {
    if (this.gos.Count > 0)
      return this.gos.Dequeue();
    return this.resPool.AddChild(this.ResGroup.transform.parent.gameObject);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.resPool.Initialize(this.ResGroup, this.gameObject);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    BuildingResPopUp.Parameter parameter = orgParam as BuildingResPopUp.Parameter;
    this.mlackItem = parameter.lackItem;
    this.mlackRes = parameter.lackRes;
    this.actionType.text = parameter.action;
    this.needGold.text = parameter.gold.ToString();
    if (parameter.gold > PlayerData.inst.hostPlayer.Currency)
      this.needGold.color = Color.red;
    else
      this.needGold.color = this.normarlColor;
    this.requireRes = parameter.requireRes;
    this.ed = parameter.ed;
    this.gold = parameter.gold;
    this.buyResourCallBack = parameter.buyResourCallBack;
    this.title.text = parameter.title;
    this.content.text = parameter.content;
    this.Init();
    this.Refresh(0);
    this.AddEventhandler();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.ReleaseTexture();
    this.ItemGroup.SetActive(true);
    this.ResGroup.SetActive(true);
    this.Clear();
    this.RemoveEventhandler();
    this.ImagePools.Clear();
    this.resPool.Clear();
  }

  private void ReleaseTexture()
  {
    foreach (UIWidget componentsInChild in this.ItemGroup.GetComponentsInChildren<UITexture>())
      BuilderFactory.Instance.Release(componentsInChild);
    using (Queue<GameObject>.Enumerator enumerator = this.gos.GetEnumerator())
    {
      while (enumerator.MoveNext())
        BuilderFactory.Instance.Release((UIWidget) enumerator.Current.GetComponentInChildren<UITexture>());
    }
  }

  private void Clear()
  {
    using (Queue<GameObject>.Enumerator enumerator = this.gos.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.resPool.Release(enumerator.Current);
    }
    this.gos.Clear();
  }

  private void OnResourcesUpdated(long food, long wood, long silver, long ore, long gold)
  {
    Dictionary<string, long> lackResources = ItemBag.CalculateLackResources(this.requireRes);
    this.mlackItem = ItemBag.CalculateLackItems(this.requireRes);
    this.mlackRes = lackResources;
    this.Init();
    this.RefreshGold();
  }

  private void RefreshGold()
  {
    if (this.requireRes == null || this.requireRes.Count <= 0)
      return;
    int cost = ItemBag.CalculateCost(this.requireRes);
    this.needGold.text = cost.ToString();
    if (cost > PlayerData.inst.hostPlayer.Currency)
      this.needGold.color = Color.red;
    else
      this.needGold.color = this.normarlColor;
  }

  private void OnCityDataChange(CityMapData obj)
  {
    Dictionary<string, long> lackResources = ItemBag.CalculateLackResources(this.requireRes);
    this.mlackItem = ItemBag.CalculateLackItems(this.requireRes);
    this.mlackRes = lackResources;
    this.Clear();
    this.Init();
    this.RefreshGold();
  }

  private void AddEventhandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Refresh);
  }

  private void Refresh(int time)
  {
    Dictionary<string, long> lackResources = ItemBag.CalculateLackResources(this.requireRes);
    Dictionary<string, int> lackItems = ItemBag.CalculateLackItems(this.requireRes);
    Dictionary<string, int>.KeyCollection.Enumerator enumerator1 = lackItems.Keys.GetEnumerator();
    while (enumerator1.MoveNext())
      lackResources.Add(enumerator1.Current, (long) lackItems[enumerator1.Current]);
    bool flag = false;
    Dictionary<string, long>.KeyCollection.Enumerator enumerator2 = lackResources.Keys.GetEnumerator();
    while (enumerator2.MoveNext())
    {
      if (!this.mlackRes.ContainsKey(enumerator2.Current))
      {
        flag = true;
        break;
      }
      long num = lackResources[enumerator2.Current];
      if (this.mlackRes[enumerator2.Current] != num)
      {
        flag = true;
        break;
      }
    }
    if (flag)
    {
      this.mlackRes = lackResources;
      this.Clear();
      this.Init();
    }
    this.RefreshGold();
  }

  private void RemoveEventhandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Refresh);
  }

  public class Parameter : Popup.PopupParameter
  {
    public Dictionary<string, int> lackItem;
    public Dictionary<string, long> lackRes;
    public Hashtable requireRes;
    public string action;
    public int gold;
    public string content;
    public string title;
    public EventDelegate ed;
    public System.Action<int> buyResourCallBack;
  }
}
