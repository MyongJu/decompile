﻿// Decompiled with JetBrains decompiler
// Type: ConfigParseEx`1
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class ConfigParseEx<T> : ConfigRawData where T : new()
{
  private Dictionary<string, ConfigField> _fields;
  private Dictionary<int, T> _cache;

  public new void Initialize(Hashtable raw)
  {
    base.Initialize(raw);
    this._fields = new Dictionary<string, ConfigField>();
    this._cache = new Dictionary<int, T>();
    foreach (FieldInfo field in typeof (T).GetFields())
    {
      object[] customAttributes = field.GetCustomAttributes(typeof (ConfigAttribute), false);
      if (customAttributes.Length > 0)
      {
        ConfigAttribute attrib = customAttributes[0] as ConfigAttribute;
        this._fields.Add(attrib.Name, new ConfigField(field, attrib));
      }
      else
        this._fields.Add(field.Name, new ConfigField(field, (ConfigAttribute) null));
    }
  }

  public List<int> InternalIds
  {
    get
    {
      return this._ids;
    }
  }

  public int s2i(string id)
  {
    int num;
    this._s2i.TryGetValue(id, out num);
    return num;
  }

  public bool Contains(string id)
  {
    return this._s2i.ContainsKey(id);
  }

  public bool Contains(int internalId)
  {
    return this._i2s.ContainsKey(internalId);
  }

  public T Get(int internalId)
  {
    T instance;
    if (!this._cache.TryGetValue(internalId, out instance) && this.ParseRecord(internalId, out instance))
      this._cache.Add(internalId, instance);
    return instance;
  }

  private bool ParseRecord(int internalId, out T instance)
  {
    instance = default (T);
    ArrayList arrayList = this._raw[(object) internalId.ToString()] as ArrayList;
    if (arrayList == null)
      return false;
    instance = new T();
    for (int index = 0; index < this._header.Count; ++index)
    {
      string key = this._header[index].ToString();
      object obj1 = arrayList[index];
      ConfigField configField;
      this._fields.TryGetValue(key, out configField);
      if (configField != null)
      {
        ConfigAttribute attrib = configField.attrib;
        if (attrib != null && attrib.CustomParse)
        {
          object obj2 = ConfigParseEx<T>.CustomParse(attrib.CustomType, obj1);
          configField.field.SetValue((object) instance, obj2);
        }
        else
        {
          object target;
          if (ConfigRawData.TryConvertValue(configField.field.FieldType, obj1, out target))
            configField.field.SetValue((object) instance, target);
        }
      }
    }
    return true;
  }

  private static object CustomParse(System.Type typeinfo, object content)
  {
    ICustomParse instance = Activator.CreateInstance(typeinfo) as ICustomParse;
    if (instance != null)
      instance.Parse(content);
    return (object) instance;
  }
}
