﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerOreMarchAllocDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using March;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTowerOreMarchAllocDlg : UI.Dialog
{
  public static int[] setTimeSecond = new int[6]
  {
    300,
    900,
    1800,
    3600,
    7200,
    14400
  };
  private MerlinTowerOreMarchAllocDlg.Data _data = new MerlinTowerOreMarchAllocDlg.Data();
  [SerializeField]
  private MerlinTowerOreMarchAllocDlg.Panel panel;
  private MerlinTowerOreMarchAllocDlg.Parameter _parameter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    MerlinTowerOreMarchAllocDlg.Parameter parameter = orgParam as MerlinTowerOreMarchAllocDlg.Parameter;
    parameter.desTroopCount = (int) MerlinTowerPayload.Instance.UserData.capacity;
    this._parameter = parameter;
    this._data.targetUid = parameter.targetUid;
    this._data.targetCityId = parameter.targetCityId;
    this._data.targetLocation = parameter.location;
    this._data.marchType = parameter.marchType;
    this._data.rallyId = parameter.rallyId;
    this._data.bossIndex = parameter.bossIndex;
    this._data.tileType = TileType.None;
    this._data.energyCost = parameter.energyCost;
    this._data.remainTime = parameter.remainTime;
    this._data.speed = parameter.speed;
    this._data.maxTime = parameter.maxTime;
    if (this._data.marchType == MarchAllocDlg.MarchType.gather)
    {
      TileData tileData = GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PitMode ? PVPMapData.MapData.GetReferenceAt(parameter.location) : PitMapData.MapData.GetReferenceAt(parameter.location);
      if (tileData != null)
      {
        this._data.tileType = tileData.TileType;
        this._data.orgDesLoadCount = tileData.Value * this._data.resourceSize;
      }
    }
    else
      this._data.orgDesLoadCount = int.MaxValue;
    this._data.orgDesTroopCount = parameter.desTroopCount < 0 ? (this._data.desTroopCount = int.MaxValue) : (this._data.desTroopCount = parameter.desTroopCount);
    this._data.isInited = false;
    this.CheckSlot();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.panel.dragonSlider.onDragonSelectedChanged += new System.Action<bool>(this.OnDragonSelectedChanged);
    this.panel.POPUP_RallyTimerSet.onSetTime += new System.Action<int>(this.SetRallyTimerIndex);
    DBManager.inst.DB_City.onDataChanged += new System.Action<long>(this.OnCityDataChanged);
    DBManager.inst.DB_Dragon.onDataChanged += new System.Action<long>(this.OnDragonDataChanged);
    this.panel.marchBar.OnSelectedHandler += new System.Action<int>(this.OnSlockSelectedIndex);
    this.FreshPanel();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.panel.dragonSlider.onDragonSelectedChanged -= new System.Action<bool>(this.OnDragonSelectedChanged);
    this.panel.POPUP_RallyTimerSet.onSetTime -= new System.Action<int>(this.SetRallyTimerIndex);
    this.panel.marchBar.OnSelectedHandler -= new System.Action<int>(this.OnSlockSelectedIndex);
    DBManager.inst.DB_City.onDataChanged -= new System.Action<long>(this.OnCityDataChanged);
    DBManager.inst.DB_Dragon.onDataChanged -= new System.Action<long>(this.OnDragonDataChanged);
  }

  public void FreshPanel()
  {
    if (!this._data.isInited)
    {
      this.InitTroopList();
      this.PreCheck();
      this.PrePopup();
    }
    this.FreshDragonData();
    this.CalcTroopLimitCount();
    this.FreshLeftTroopCount();
    this.FreshTroopList();
    this.FreshTroopData();
    this.FreshButtons();
    this.OnDefaultSuggestSelect();
    this.FreshTroopData();
  }

  public void InitTroopList()
  {
    if (this.panel.list.Count > 0)
    {
      for (int index = 0; index < this.panel.list.Count; ++index)
      {
        string id = this.panel.list[index].troopInfo.ID;
        if (this._data.troops.ContainsKey(id))
          this._data.troops[id].troopCount = this.panel.list[index].troopCount;
        this.panel.list[index].onValueChange -= new System.Action<string, int>(this.OnTroopCountChanged);
        UnityEngine.Object.Destroy((UnityEngine.Object) this.panel.list[index].gameObject);
      }
      this.panel.list.Clear();
    }
    CityTroopsInfo inhospiCityTroopsInfo = this.GetNoInhospiCityTroopsInfo();
    if (inhospiCityTroopsInfo.troops == null)
      return;
    List<Unit_StatisticsInfo> unitStatisticsInfoList = new List<Unit_StatisticsInfo>();
    Dictionary<string, int>.KeyCollection.Enumerator enumerator = inhospiCityTroopsInfo.troops.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator.Current);
      if (data != null && data.Type != "trap")
        unitStatisticsInfoList.Add(data);
    }
    unitStatisticsInfoList.Sort((Comparison<Unit_StatisticsInfo>) ((a, b) =>
    {
      if (a.Troop_Tier == b.Troop_Tier)
        return a.Priority.CompareTo(b.Priority);
      return b.Troop_Tier.CompareTo(a.Troop_Tier);
    }));
    for (int index = 0; index < unitStatisticsInfoList.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.panel.listParent.gameObject, this.panel.orgTroopSlider.gameObject);
      gameObject.name = "troop_" + (object) (100 + index);
      gameObject.transform.localPosition = new Vector3(0.0f, -250f * (float) (index + 1), 0.0f);
      if (!this._data.troops.ContainsKey(unitStatisticsInfoList[index].ID))
        this._data.troops.Add(unitStatisticsInfoList[index].ID, new MerlinTowerOreMarchAllocDlg.Data.TroopData()
        {
          troopId = unitStatisticsInfoList[index].ID
        });
      TroopSlider component = gameObject.GetComponent<TroopSlider>();
      component.onValueChange += new System.Action<string, int>(this.OnTroopCountChanged);
      this.panel.list.Add(component);
      gameObject.SetActive(false);
    }
    this.panel.NoTroosText.gameObject.SetActive(this.panel.list.Count == 0);
    this.CalcTroopLimitCount();
    this.FreshTroopList();
  }

  private void CalcTroopLimitCount()
  {
    this._data.maxTroopCount = this._data.desTroopCount;
  }

  public void FreshTroopData()
  {
    int internalId = ConfigManager.inst.DB_Properties["prop_army_load_percent"].InternalID;
    BenefitInfo benefitInfo = DBManager.inst.DB_Local_Benefit.Get(internalId);
    float num1 = 1f;
    if (benefitInfo != null)
      num1 += benefitInfo.total;
    if (this._data.isDragonIn && this._data.benefits != null && this._data.benefits.ContainsKey(internalId))
    {
      float num2 = num1 + this._data.benefits[internalId];
    }
    int num3 = 0;
    int num4 = 0;
    int num5 = 0;
    float slowestUnitSpeed = float.MaxValue;
    bool flag = this._data.marchType == MarchAllocDlg.MarchType.monsterAttack || this._data.marchType == MarchAllocDlg.MarchType.worldBossAttack || this._data.marchType == MarchAllocDlg.MarchType.startGveRally || this._data.marchType == MarchAllocDlg.MarchType.joinGveRally;
    for (int index = 0; index < this.panel.list.Count; ++index)
    {
      float load = ConfigManager.inst.DB_BenefitCalc.GetUnitFinalData(this.panel.list[index].troopInfo.ID, ConfigBenefitCalc.UnitCalcType.load | ConfigBenefitCalc.UnitCalcType.speed, ConfigBenefitCalc.CalcOption.none).load;
      Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(this.panel.list[index].troopInfo.ID);
      num3 += this.panel.list[index].troopCount;
      string BenefitCalcID = string.Format("{0}_load", (object) this.panel.list[index].troopInfo.Troop_Class_ID.Replace("class_", "calc_"));
      int num6 = this.panel.list[index].troopInfo.GatherCapacity * this.panel.list[index].troopCount;
      num4 += Mathf.FloorToInt(ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) num6, BenefitCalcID, !this._data.isDragonIn ? ConfigBenefitCalc.CalcOption.none : this._data.calcType));
      num5 += this.panel.list[index].troopInfo.Power * this.panel.list[index].troopCount;
      float finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(data.Speed, string.Format("{0}_speed", (object) this.panel.list[index].troopInfo.Troop_Class_ID.Replace("class_", "calc_")), !this._data.isDragonIn ? ConfigBenefitCalc.CalcOption.none : this._data.calcType);
      if (flag)
        finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(finalData, "calc_pve_speed", !this._data.isDragonIn ? ConfigBenefitCalc.CalcOption.none : this._data.calcType);
      if (this.panel.list[index].troopCount != 0 && (double) finalData < (double) slowestUnitSpeed && (double) finalData != 0.0)
        slowestUnitSpeed = finalData;
    }
    bool isTargetCity = false;
    if (this._data.marchType == MarchAllocDlg.MarchType.cityAttack || this._data.marchType == MarchAllocDlg.MarchType.startRabRally || (this._data.marchType == MarchAllocDlg.MarchType.startGveRally || this._data.marchType == MarchAllocDlg.MarchType.startRally) || this._data.marchType == MarchAllocDlg.MarchType.reinforce)
      isTargetCity = true;
    if (this._data.marchType == MarchAllocDlg.MarchType.worldBossAttack)
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("attack_worldboss_marchspeed");
      if (data != null)
      {
        float num6 = slowestUnitSpeed;
        slowestUnitSpeed = (float) data.ValueInt * slowestUnitSpeed;
        if ((double) slowestUnitSpeed <= 0.0)
          slowestUnitSpeed = num6;
      }
    }
    MarchData.CalcMarchTime(PlayerData.inst.CityData.Location, this._data.targetLocation, slowestUnitSpeed, isTargetCity);
    int time = 3;
    MagicTowerMainInfo currentTowerMainInfo = MerlinTowerPayload.Instance.UserData.CurrentTowerMainInfo;
    long leftTime = (long) MerlinTowerPayload.Instance.KingdomData.LeftTime;
    int speed = currentTowerMainInfo.Speed;
    int gratherTime = currentTowerMainInfo.GratherTime;
    long num7 = Math.Min(leftTime, (long) gratherTime);
    this.panel.troopCount.text = string.Format("{0} / {1}", (object) Utils.FormatThousands(num3.ToString()), (object) Utils.FormatThousands(this._data.maxTroopCount.ToString()));
    this.panel.troopLoad.text = Utils.FormatThousands((num7 * (long) speed).ToString());
    this.panel.troopLoadIcon.spriteName = this._data.marchLoadSpriteName;
    this.panel.troopPower.text = Utils.FormatThousands(num5.ToString());
    UILabel marchTime = this.panel.marchTime;
    string str1 = Utils.FormatTime(time, false, false, true);
    this.panel.marchTime2.text = str1;
    string str2 = str1;
    marchTime.text = str2;
    this.FreshButtons();
  }

  public void FreshDragonData()
  {
    this.panel.dragonSlider.Refresh(this._data.dragonType, false);
  }

  public void FreshTroopList()
  {
    CityTroopsInfo inhospiCityTroopsInfo = this.GetNoInhospiCityTroopsInfo();
    if (inhospiCityTroopsInfo.troops == null)
      return;
    List<Unit_StatisticsInfo> unitStatisticsInfoList = new List<Unit_StatisticsInfo>();
    Dictionary<string, int>.KeyCollection.Enumerator enumerator = inhospiCityTroopsInfo.troops.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator.Current);
      if (data != null && data.Type != "trap")
        unitStatisticsInfoList.Add(data);
    }
    unitStatisticsInfoList.Sort((Comparison<Unit_StatisticsInfo>) ((a, b) =>
    {
      if (a.Troop_Tier == b.Troop_Tier)
        return a.Priority.CompareTo(b.Priority);
      return b.Troop_Tier.CompareTo(a.Troop_Tier);
    }));
    for (int index = 0; index < unitStatisticsInfoList.Count; ++index)
    {
      if (inhospiCityTroopsInfo.troops[unitStatisticsInfoList[index].ID] > 0 && this.panel.list.Count > index)
      {
        this.panel.list[index].gameObject.SetActive(true);
        TroopSlider troopSlider = this.panel.list[index];
        troopSlider.troopInfo = unitStatisticsInfoList[index];
        troopSlider.troopMaxCount = inhospiCityTroopsInfo.troops[unitStatisticsInfoList[index].ID];
        troopSlider.troopCount = this._data.troops[unitStatisticsInfoList[index].ID].troopCount;
      }
    }
  }

  private void FreshLeftTroopCount()
  {
    int num1 = 0;
    for (int index = 0; index < this.panel.list.Count; ++index)
      num1 += this.panel.list[index].troopCount;
    int num2 = this._data.maxTroopCount - num1;
    for (int index = 0; index < this.panel.list.Count; ++index)
      this.panel.list[index].targetCount = this.panel.list[index].troopCount + num2;
  }

  public void FreshButtons()
  {
    for (int index = 0; index < this.panel.list.Count; ++index)
    {
      if (this.panel.list[index].troopCount > 0)
      {
        this.panel.BT_march.isEnabled = true;
        this.panel.BT_march2.isEnabled = true;
        return;
      }
    }
    this.panel.BT_march.isEnabled = false;
    this.panel.BT_march2.isEnabled = false;
  }

  public void OnTroopCountChanged(string troopId, int count)
  {
    int num1 = 0;
    for (int index = 0; index < this.panel.list.Count; ++index)
      num1 += this.panel.list[index].troopCount;
    if (num1 > this._data.maxTroopCount)
    {
      int num2 = num1 - this._data.desTroopCount;
      for (int index = this.panel.list.Count - 1; index >= 0; --index)
      {
        if (this.panel.list[index].troopInfo.ID == troopId)
        {
          this.panel.list[index].troopCount -= num2;
          break;
        }
      }
      int maxTroopCount = this._data.maxTroopCount;
    }
    this.FreshLeftTroopCount();
    this.FreshTroopData();
    this.FreshButtons();
  }

  public void OnDragonSelectedChanged(bool isSelected)
  {
    this._data.isDragonIn = isSelected;
    this.CalcTroopLimitCount();
    int num = 0;
    for (int index = 0; index < this.panel.list.Count; ++index)
      num += this.panel.list[index].troopCount;
    if (num > this._data.maxTroopCount)
      this.OnQuickQueueClick();
    this.FreshDragonData();
    this.FreshTroopData();
    this.FreshLeftTroopCount();
  }

  public void OnCityDataChanged(long cityId)
  {
    if (cityId != (long) PlayerData.inst.cityId || TutorialManager.Instance.IsRunning)
      return;
    this.InitTroopList();
    this.FreshTroopData();
    this.FreshButtons();
  }

  public void OnSlockSelectedIndex(int index)
  {
    this.OnSlotClick(index + 1);
  }

  public void OnGotoParadeGround()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    LinkerHub.Instance.Distribute("{\"classname\":\"LPFocusConstantBuilding\",\"content\":\"{\\\"buildingType\\\":\\\"PARADEGROUND\\\"}\"}");
  }

  public void OnDragonDataChanged(long dragonId)
  {
    if (dragonId != PlayerData.inst.uid)
      return;
    this.OnDragonSelectedChanged(this._data.isDragonIn);
    this.FreshDragonData();
  }

  private void PreCheck()
  {
    this._data.isInited = true;
  }

  private void PrePopup()
  {
    if (this._data.marchType == MarchAllocDlg.MarchType.startRally)
    {
      MarchSystem.LocalMarchCheckType type = GameEngine.Instance.marchSystem.CanRally(this._data.targetLocation);
      if (type != MarchSystem.LocalMarchCheckType.successed)
        UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
        {
          title = Utils.XLAT("id_uppercase_warning"),
          description = GameEngine.Instance.marchSystem.GetCheckTypeMsg(type),
          buttonClickEvent = (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null)),
          closeButtonCallbackEvent = (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null))
        });
      else
        this.panel.POPUP_RallyTimerSet.ShowPopup(0, 4);
    }
    else if (this._data.marchType == MarchAllocDlg.MarchType.startGveRally)
    {
      MarchSystem.LocalMarchCheckType type = GameEngine.Instance.marchSystem.CanRally(this._data.targetLocation);
      switch (type)
      {
        case MarchSystem.LocalMarchCheckType.successed:
          this.panel.POPUP_RallyTimerSet.ShowPopup(0, 4);
          break;
        case MarchSystem.LocalMarchCheckType.already_in_rally:
          UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
          {
            title = Utils.XLAT("id_uppercase_warning"),
            description = Utils.XLAT("march_already_in_rally_warning"),
            buttonClickEvent = (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null)),
            closeButtonCallbackEvent = (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null))
          });
          break;
        default:
          UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
          {
            title = Utils.XLAT("id_uppercase_warning"),
            description = GameEngine.Instance.marchSystem.GetCheckTypeMsg(type),
            buttonClickEvent = (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null)),
            closeButtonCallbackEvent = (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null))
          });
          break;
      }
    }
    else if (this._data.marchType == MarchAllocDlg.MarchType.startRabRally)
    {
      MarchSystem.LocalMarchCheckType type = GameEngine.Instance.marchSystem.CanRally(this._data.targetLocation);
      switch (type)
      {
        case MarchSystem.LocalMarchCheckType.successed:
          this.panel.POPUP_RallyTimerSet.ShowPopup(0, 2);
          break;
        case MarchSystem.LocalMarchCheckType.already_in_rally:
          UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
          {
            title = Utils.XLAT("id_uppercase_warning"),
            description = Utils.XLAT("alliance_portal_one_battle_at_a_time_warning"),
            buttonClickEvent = (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null)),
            closeButtonCallbackEvent = (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null))
          });
          break;
        default:
          UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
          {
            title = Utils.XLAT("id_uppercase_warning"),
            description = GameEngine.Instance.marchSystem.GetCheckTypeMsg(type),
            buttonClickEvent = (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null)),
            closeButtonCallbackEvent = (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null))
          });
          break;
      }
    }
    else
    {
      if (this._data.marchType != MarchAllocDlg.MarchType.reinforce && this._data.marchType != MarchAllocDlg.MarchType.wonderReinforce)
        return;
      MarchSystem.LocalMarchCheckType type = GameEngine.Instance.marchSystem.CanReinforce(this._data.targetLocation);
      if (type != MarchSystem.LocalMarchCheckType.successed)
        UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
        {
          title = Utils.XLAT("id_uppercase_warning"),
          description = GameEngine.Instance.marchSystem.GetCheckTypeMsg(type),
          buttonClickEvent = (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null)),
          closeButtonCallbackEvent = (System.Action) (() => UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null))
        });
      else
        this.panel.POPUP_ReinforceCheck.Show(new ReinforceAllocPopup.Data()
        {
          targetUid = this._data.targetUid,
          targetLocation = this._data.targetLocation,
          targetCityId = this._data.targetCityId,
          tileOwnerId = this._data.tileOwnerId,
          OnPopupCallback = new System.Action<int>(this.OnReinforceCheckCallback)
        });
    }
  }

  private bool _isRallyReinforce()
  {
    return false;
  }

  private void OnReinforceCheckCallback(int desReinforceCount)
  {
    this._data.orgDesLoadCount = int.MaxValue;
    this._data.orgDesTroopCount = this._data.desTroopCount = desReinforceCount;
    this.CalcTroopLimitCount();
    this.OnDefaultSuggestSelect();
    this.FreshTroopData();
  }

  private void OnDefaultSuggestSelect()
  {
    this.panel.dragonSlider.isSelected = PlayerData.inst.dragonData == null || PlayerData.inst.dragonData.MarchId != 0L || GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.DragonKnight ? (this._data.isDragonIn = false) : (this._data.isDragonIn = true);
    this.CalcTroopLimitCount();
    for (int index = 0; index < this.panel.list.Count; ++index)
      this.panel.list[index].Zero();
    this.OnQuickQueueClick();
    this.FreshTroopData();
  }

  public void SetRallyTimerIndex(int timerIndex)
  {
    this._data.rallyTimerIndex = timerIndex;
  }

  public void OnCloseBtPress()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnMarchButtonClick()
  {
    Hashtable hashtable = new Hashtable();
    for (int index = 0; index < this.panel.list.Count; ++index)
    {
      if (this.panel.list[index].troopCount > 0)
        hashtable.Add((object) this.panel.list[index].troopInfo.ID, (object) this.panel.list[index].troopCount);
    }
    bool isDragonIn = this._data.isDragonIn;
    if (this._parameter.confirmCallback != null)
      this._parameter.confirmCallback(hashtable, isDragonIn);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void DoMarch()
  {
    if (PlayerData.inst.playerCityData.cityLocation.K != this._data.targetLocation.K)
    {
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      UIManager.inst.ShowSystemBlocker("CommonBlocker", (SystemBlocker.SystemBlockerParameter) new CommonBlocker.Parameter()
      {
        type = CommonBlocker.CommonBlockerType.confirmation,
        displayType = 11,
        title = Utils.XLAT("exception_title"),
        buttonEventText = Utils.XLAT("exception_button"),
        descriptionText = ScriptLocalization.Get("exception_1200200", true),
        buttonEvent = (System.Action) null
      });
    }
    else
    {
      Hashtable troops = new Hashtable();
      for (int index = 0; index < this.panel.list.Count; ++index)
      {
        if (this.panel.list[index].troopCount > 0)
          troops.Add((object) this.panel.list[index].troopInfo.ID, (object) this.panel.list[index].troopCount);
      }
      bool flag = false;
      MarchAllocDlg.MarchType marchType = this._data.marchType;
      switch (marchType)
      {
        case MarchAllocDlg.MarchType.cityAttack:
        case MarchAllocDlg.MarchType.encampAttack:
        case MarchAllocDlg.MarchType.gatherAttack:
        case MarchAllocDlg.MarchType.fortressAttack:
        case MarchAllocDlg.MarchType.wonderAttack:
        case MarchAllocDlg.MarchType.wonderReinforce:
        case MarchAllocDlg.MarchType.wonderOccupy:
        case MarchAllocDlg.MarchType.templeAttack:
        case MarchAllocDlg.MarchType.miniWonderAttack:
        case MarchAllocDlg.MarchType.digAttack:
          flag = true;
          break;
        default:
          if (marchType == MarchAllocDlg.MarchType.startRally || marchType == MarchAllocDlg.MarchType.joinRally)
            goto case MarchAllocDlg.MarchType.cityAttack;
          else
            break;
      }
      if (flag)
        BoostManager.Instance.ShowPeaceShieldWarning((System.Action) (() => this.SendMarch(troops)));
      else
        this.SendMarch(troops);
    }
  }

  private void CheckSlot()
  {
    int setMarchSlot = PlayerData.inst.playerCityData.GetSetMarchSlot();
    if (setMarchSlot == 0)
    {
      NGUITools.SetActive(this.panel.normal, true);
      NGUITools.SetActive(this.panel.hasSlot, false);
    }
    else
    {
      bool state = false;
      for (int index = 0; index < setMarchSlot; ++index)
      {
        if (PlayerData.inst.playerCityData.GetMarchSetData(index + 1) != null)
        {
          state = true;
          break;
        }
      }
      NGUITools.SetActive(this.panel.marchBar.gameObject, state);
      NGUITools.SetActive(this.panel.emptyMarchData.gameObject, !state);
      for (int index = 0; index < this.panel.slots.Length; ++index)
      {
        if (index + 1 > setMarchSlot)
        {
          this.panel.slotBts[index].isEnabled = false;
          GreyUtility.Grey(this.panel.slots[index]);
        }
      }
    }
  }

  public void OnSlot1Click()
  {
    this.OnSlotClick(1);
  }

  public void OnSlot2Click()
  {
    this.OnSlotClick(2);
  }

  public void OnSlot3Click()
  {
    this.OnSlotClick(3);
  }

  public void OnSlotClick(int index)
  {
    if (PlayerData.inst.playerCityData.GetSetMarchSlot() < index)
      return;
    CityData.MarchSetData marchSetData = PlayerData.inst.playerCityData.GetMarchSetData(index);
    if (marchSetData != null)
    {
      DragonData dragonData = PlayerData.inst.dragonData;
      if (dragonData != null && dragonData.MarchId <= 0L)
      {
        this.panel.dragonSlider.isSelected = marchSetData.isDragonIn;
        this._data.isDragonIn = marchSetData.isDragonIn;
      }
      this.CalcTroopLimitCount();
      int num1 = this._data.maxTroopCount;
      Dictionary<string, int> troops = marchSetData.troops;
      CityTroopsInfo inhospiCityTroopsInfo = this.GetNoInhospiCityTroopsInfo();
      for (int index1 = 0; index1 < this.panel.list.Count; ++index1)
      {
        if (troops.ContainsKey(this.panel.list[index1].troopInfo.ID))
        {
          int a = troops[this.panel.list[index1].troopInfo.ID];
          if (inhospiCityTroopsInfo.troops.ContainsKey(this.panel.list[index1].troopInfo.ID))
            a = Mathf.Min(a, inhospiCityTroopsInfo.troops[this.panel.list[index1].troopInfo.ID]);
          int num2 = num1 - a;
          if (num2 <= 0)
            a = num1;
          num1 = num2;
          this.panel.list[index1].troopCount = a;
        }
        else
          this.panel.list[index1].troopCount = 0;
      }
    }
    this.FreshTroopData();
    this.FreshButtons();
  }

  public CityTroopsInfo GetNoInhospiCityTroopsInfo()
  {
    CityTroopsInfo totalTroops = PlayerData.inst.playerCityData.totalTroops;
    CityTroopsInfo cityTroopsInfo = new CityTroopsInfo();
    cityTroopsInfo.troops = new Dictionary<string, int>();
    List<string> stringList = new List<string>();
    Dictionary<string, int>.KeyCollection.Enumerator enumerator = totalTroops.troops.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      long troop = (long) totalTroops.troops[enumerator.Current];
      if (troop > 0L)
      {
        if (!cityTroopsInfo.troops.ContainsKey(enumerator.Current))
        {
          cityTroopsInfo.troops.Add(enumerator.Current, (int) troop);
        }
        else
        {
          Dictionary<string, int> troops;
          string current;
          (troops = cityTroopsInfo.troops)[current = enumerator.Current] = troops[current] + (int) troop;
        }
      }
    }
    return cityTroopsInfo;
  }

  private void SendMarch(Hashtable troops)
  {
    switch (this._data.marchType)
    {
      case MarchAllocDlg.MarchType.startRally:
      case MarchAllocDlg.MarchType.startGveRally:
        if (this._data.bossIndex > 0)
        {
          GameEngine.Instance.marchSystem.StartGvERally(this._data.targetLocation, this._data.bossIndex, troops, MerlinTowerOreMarchAllocDlg.setTimeSecond[this._data.rallyTimerIndex], this._data.isDragonIn, new System.Action<bool, object>(this.OnStartGveRallyCallback));
          break;
        }
        GameEngine.Instance.marchSystem.StartRally(this._data.targetLocation, troops, MerlinTowerOreMarchAllocDlg.setTimeSecond[this._data.rallyTimerIndex], this._data.isDragonIn, new System.Action<bool, object>(this.OnStartMarchCallback));
        break;
      case MarchAllocDlg.MarchType.joinRally:
      case MarchAllocDlg.MarchType.joinGveRally:
      case MarchAllocDlg.MarchType.joinRabRally:
        GameEngine.Instance.marchSystem.JoinRally(this._data.rallyId, troops, this._data.isDragonIn, new System.Action<bool, object>(this.OnStartMarchCallback));
        break;
      case MarchAllocDlg.MarchType.startRabRally:
        GameEngine.Instance.marchSystem.StartRabRally(this._data.targetLocation, troops, MerlinTowerOreMarchAllocDlg.setTimeSecond[this._data.rallyTimerIndex], this._data.isDragonIn, new System.Action<bool, object>(this.OnStartMarchCallback));
        break;
      case MarchAllocDlg.MarchType.reinforce:
        GameEngine.Instance.marchSystem.StartReinforce(this._data.targetLocation, troops, this._data.isDragonIn, new System.Action<bool, object>(this.OnStartMarchCallback));
        break;
      case MarchAllocDlg.MarchType.fortressReinforce:
        GameEngine.Instance.marchSystem.StartJoinFortress(this._data.targetLocation, troops, this._data.isDragonIn, new System.Action<bool, object>(this.OnStartMarchCallback));
        break;
      case MarchAllocDlg.MarchType.wonderReinforce:
      case MarchAllocDlg.MarchType.wonderOccupy:
        GameEngine.Instance.marchSystem.StartJoinWonder(this._data.targetLocation, troops, this._data.isDragonIn, new System.Action<bool, object>(this.OnStartMarchCallback));
        break;
      case MarchAllocDlg.MarchType.templeReinforce:
        GameEngine.Instance.marchSystem.StartJoinTemple(this._data.targetLocation, troops, this._data.isDragonIn, new System.Action<bool, object>(this.OnStartMarchCallback));
        break;
      default:
        GameEngine.Instance.marchSystem.StartMarch(this._data.marchType, this._data.targetLocation, troops, this._data.isDragonIn, new System.Action<bool, object>(this.OnStartMarchCallback));
        break;
    }
  }

  private void ShowAnimation()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void ShowResult()
  {
  }

  public void OnBoostButtonClick()
  {
    int num1 = 1;
    int num2 = 0;
    for (int index = 0; index < this.panel.list.Count; ++index)
      num2 += this.panel.list[index].troopMaxCount;
    int maxTroopCount = Mathf.CeilToInt((float) num2 / (float) num1);
    if (maxTroopCount > this._data.maxTroopCount)
      maxTroopCount = this._data.maxTroopCount;
    float num3 = (float) maxTroopCount / (float) num2;
    int num4 = 0;
    for (int index = 0; index < this.panel.list.Count; ++index)
    {
      int num5 = Mathf.CeilToInt(num3 * (float) this.panel.list[index].troopMaxCount);
      if (num4 + num5 < this._data.maxTroopCount)
      {
        this.panel.list[index].troopCount = num5;
        num4 += num5;
      }
      else
      {
        this.panel.list[index].troopCount = this._data.maxTroopCount - num4;
        break;
      }
    }
    this.FreshTroopData();
  }

  public void OnQuickQueueClick()
  {
    int num1 = 0;
    int num2 = 0;
    this.panel.marchBar.SelectedIndex = -1;
    for (int index = 0; index < this.panel.list.Count; ++index)
      num2 += this.panel.list[index].troopCount;
    if (num2 != 0)
    {
      for (int index = 0; index < this.panel.list.Count; ++index)
        this.panel.list[index].Zero();
    }
    else
    {
      int num3 = this._data.maxTroopCount - num2;
      for (int index = 0; index < this.panel.list.Count && num3 != 0; ++index)
      {
        if (this.panel.list[index].troopMaxCount - this.panel.list[index].troopCount != 0)
        {
          if (this.panel.list[index].troopMaxCount - this.panel.list[index].troopCount < num3)
          {
            num3 -= this.panel.list[index].troopMaxCount - this.panel.list[index].troopCount;
            this.panel.list[index].Max();
          }
          else
          {
            this.panel.list[index].troopCount += num3;
            num1 = 0;
            break;
          }
        }
      }
    }
    this.FreshTroopData();
  }

  public void OnStartMarchCallback(bool result, object orgData)
  {
    if (result)
    {
      if (this._data.marchType == MarchAllocDlg.MarchType.monsterAttack)
        UIManager.inst.publicHUD.heroMiniDlg.spiritController.Add((-this._data.energyCost).ToString());
      Hashtable data;
      DatabaseTools.CheckAndParseOrgData(orgData, out data);
      if (data.ContainsKey((object) "forced"))
        data[(object) "forced"] = (object) 1;
      else
        data.Add((object) "forced", (object) 1);
      if (data.ContainsKey((object) "confirm"))
        UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
        {
          title = Utils.XLAT("id_uppercase_warning"),
          description = ScriptLocalization.Get("kingdom_occupy_warning_description", true),
          leftButtonText = ScriptLocalization.Get("id_uppercase_cancel", true),
          rightButtonText = ScriptLocalization.Get("id_uppercase_continue", true),
          leftButtonClickEvent = (System.Action) (() => UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null)),
          setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
          rightButtonClickEvent = (System.Action) (() => MessageHub.inst.GetPortByAction("PVP:startMarch").SendRequest(data, new System.Action<bool, object>(this.OnStartMarchCallback), true))
        });
      else
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable != null && hashtable.ContainsKey((object) "errno"))
      {
        int result1 = 0;
        int.TryParse(hashtable[(object) "errno"].ToString(), out result1);
        if (result1 == 1200080)
          Utils.RefreshBlock(this._data.targetLocation, (System.Action<bool, object>) null);
      }
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }

  public void OnStartGveRallyCallback(bool result, object orgData)
  {
    if (result)
    {
      UIManager.inst.publicHUD.heroMiniDlg.spiritController.Add((-this._data.energyCost).ToString());
      Hashtable data;
      DatabaseTools.CheckAndParseOrgData(orgData, out data);
      if (data != null && data.ContainsKey((object) "confirm"))
      {
        data[(object) "force"] = (object) 1;
        UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
        {
          title = Utils.XLAT("id_uppercase_warning"),
          description = ScriptLocalization.Get("march_gve_already_rallied_warning", true),
          leftButtonClickEvent = (System.Action) (() => UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null)),
          rightButtonClickEvent = (System.Action) (() => MessageHub.inst.GetPortByAction("Rally:startGveRally").SendRequest(data, new System.Action<bool, object>(this.OnStartGveRallyCallback), true))
        });
      }
      else
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int desTroopCount = -1;
    public int desLoadCount = -1;
    public System.Action<Hashtable, bool> confirmCallback;
    public Coordinate location;
    public MarchAllocDlg.MarchType marchType;
    public bool isRally;
    public long rallyId;
    public long targetUid;
    public long targetCityId;
    public int remainTime;
    public int maxTime;
    public int speed;
    public int bossIndex;
    public int energyCost;
  }

  protected class Data
  {
    public bool isInited = true;
    public int orgDesTroopCount = -1;
    public int desTroopCount = -1;
    public Dictionary<string, MerlinTowerOreMarchAllocDlg.Data.TroopData> troops = new Dictionary<string, MerlinTowerOreMarchAllocDlg.Data.TroopData>();
    public bool isRally;
    public int rallyTimerIndex;
    public int maxTroopCount;
    public long targetUid;
    public long targetCityId;
    public Coordinate targetLocation;
    public long tileOwnerId;
    public long rallyId;
    public int bossIndex;
    public int energyCost;
    public int remainTime;
    public int maxTime;
    public int speed;
    public MarchAllocDlg.MarchType marchType;
    public int orgDesLoadCount;
    public TileType tileType;
    public bool isDragonIn;

    public int resourceSize
    {
      get
      {
        switch (this.tileType)
        {
          case TileType.Resource1:
            GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("food_load");
            if (data1 != null)
              return data1.ValueInt;
            return 1;
          case TileType.Resource2:
            GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("wood_load");
            if (data2 != null)
              return data2.ValueInt;
            return 1;
          case TileType.Resource3:
            GameConfigInfo data3 = ConfigManager.inst.DB_GameConfig.GetData("ore_load");
            if (data3 != null)
              return data3.ValueInt;
            return 5;
          case TileType.Resource4:
            GameConfigInfo data4 = ConfigManager.inst.DB_GameConfig.GetData("silver_load");
            if (data4 != null)
              return data4.ValueInt;
            return 20;
          case TileType.AllianceSuperMine:
            AllianceSuperMineData dataByCoordinate = DBManager.inst.DB_AllianceSuperMine.GetDataByCoordinate(this.targetLocation);
            if (dataByCoordinate == null)
              return 1;
            AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(dataByCoordinate.ConfigId);
            if (buildingStaticInfo != null)
            {
              string resourceType = buildingStaticInfo.ResourceType;
              if (resourceType != null)
              {
                if (MerlinTowerOreMarchAllocDlg.Data.\u003C\u003Ef__switch\u0024map88 == null)
                  MerlinTowerOreMarchAllocDlg.Data.\u003C\u003Ef__switch\u0024map88 = new Dictionary<string, int>(4)
                  {
                    {
                      "food",
                      0
                    },
                    {
                      "wood",
                      1
                    },
                    {
                      "ore",
                      2
                    },
                    {
                      "silver",
                      3
                    }
                  };
                int num;
                if (MerlinTowerOreMarchAllocDlg.Data.\u003C\u003Ef__switch\u0024map88.TryGetValue(resourceType, out num))
                {
                  switch (num)
                  {
                    case 0:
                      return ConfigManager.inst.DB_GameConfig.GetData("food_load").ValueInt;
                    case 1:
                      return ConfigManager.inst.DB_GameConfig.GetData("wood_load").ValueInt;
                    case 2:
                      return ConfigManager.inst.DB_GameConfig.GetData("ore_load").ValueInt;
                    case 3:
                      return ConfigManager.inst.DB_GameConfig.GetData("silver_load").ValueInt;
                  }
                }
              }
            }
            return 1;
          case TileType.PitMine:
            GameConfigInfo data5 = ConfigManager.inst.DB_GameConfig.GetData("abyss_load");
            if (data5 != null)
              return data5.ValueInt;
            return 1;
          default:
            return 1;
        }
      }
    }

    public string marchLoadSpriteName
    {
      get
      {
        switch (this.tileType)
        {
          case TileType.Resource1:
            return "food_icon";
          case TileType.Resource2:
            return "wood_icon";
          case TileType.Resource3:
            return "ore_icon";
          case TileType.Resource4:
            return "silver_icon";
          case TileType.AllianceSuperMine:
            AllianceSuperMineData dataByCoordinate = DBManager.inst.DB_AllianceSuperMine.GetDataByCoordinate(this.targetLocation);
            if (dataByCoordinate == null)
              return "trading_icon";
            AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(dataByCoordinate.ConfigId);
            if (buildingStaticInfo != null)
            {
              string resourceType = buildingStaticInfo.ResourceType;
              if (resourceType != null)
              {
                if (MerlinTowerOreMarchAllocDlg.Data.\u003C\u003Ef__switch\u0024map89 == null)
                  MerlinTowerOreMarchAllocDlg.Data.\u003C\u003Ef__switch\u0024map89 = new Dictionary<string, int>(4)
                  {
                    {
                      "food",
                      0
                    },
                    {
                      "wood",
                      1
                    },
                    {
                      "ore",
                      2
                    },
                    {
                      "silver",
                      3
                    }
                  };
                int num;
                if (MerlinTowerOreMarchAllocDlg.Data.\u003C\u003Ef__switch\u0024map89.TryGetValue(resourceType, out num))
                {
                  switch (num)
                  {
                    case 0:
                      return "food_icon";
                    case 1:
                      return "wood_icon";
                    case 2:
                      return "ore_icon";
                    case 3:
                      return "silver_icon";
                  }
                }
              }
            }
            return "trading_icon";
          case TileType.PitMine:
            return "dragon_fossil_icon";
          default:
            return "trading_icon";
        }
      }
    }

    public string dragonType
    {
      get
      {
        return "attack";
      }
    }

    public ConfigBenefitCalc.CalcOption calcType
    {
      get
      {
        string dragonType = this.dragonType;
        if (dragonType != null)
        {
          if (MerlinTowerOreMarchAllocDlg.Data.\u003C\u003Ef__switch\u0024map8A == null)
            MerlinTowerOreMarchAllocDlg.Data.\u003C\u003Ef__switch\u0024map8A = new Dictionary<string, int>(4)
            {
              {
                "gather",
                0
              },
              {
                "attack",
                1
              },
              {
                "attack_monster",
                2
              },
              {
                "defend",
                3
              }
            };
          int num;
          if (MerlinTowerOreMarchAllocDlg.Data.\u003C\u003Ef__switch\u0024map8A.TryGetValue(dragonType, out num))
          {
            switch (num)
            {
              case 0:
                return ConfigBenefitCalc.CalcOption.dragonGather;
              case 1:
                return ConfigBenefitCalc.CalcOption.dragonAttack;
              case 2:
                return ConfigBenefitCalc.CalcOption.dragonAttackMonster;
              case 3:
                return ConfigBenefitCalc.CalcOption.dragonDefense;
            }
          }
        }
        return ConfigBenefitCalc.CalcOption.none;
      }
    }

    public Dictionary<int, float> benefits
    {
      get
      {
        if (PlayerData.inst.dragonData != null && PlayerData.inst.dragonData.benefits != null && (PlayerData.inst.dragonData.benefits.datas != null && PlayerData.inst.dragonData.benefits.datas.ContainsKey(this.dragonType)))
          return PlayerData.inst.dragonData.benefits.datas[this.dragonType];
        return new Dictionary<int, float>();
      }
    }

    public class TroopData
    {
      public string troopId;
      public int troopCount;
    }
  }

  public enum MarchType
  {
    startRally,
    joinRally,
    startGveRally,
    joinGveRally,
    startRabRally,
    joinRabRally,
    reinforce,
    cityAttack,
    encampAttack,
    gatherAttack,
    fortressReinforce,
    fortressAttack,
    wonderAttack,
    wonderReinforce,
    wonderOccupy,
    templeReinforce,
    templeAttack,
    allianceCityBuild,
    gather,
    encamp,
    monsterAttack,
    worldBossAttack,
    miniWonderAttack,
    dig,
    digAttack,
    merlinTrialsAttack,
  }

  [Serializable]
  protected class Panel
  {
    public List<TroopSlider> list = new List<TroopSlider>();
    public UILabel troopCount;
    public UILabel troopLoad;
    public UILabel troopPower;
    public UISprite troopLoadIcon;
    public GameObject[] slots;
    public UIButton[] slotBts;
    public UILabel marchTime;
    public UILabel marchTime2;
    public UIButton BT_boost;
    public UIButton BT_quickQueue;
    public UIButton BT_march;
    public UIButton BT_march2;
    public DragonSlider dragonSlider;
    public TroopSlider orgTroopSlider;
    public Transform listParent;
    public Transform NoTroosText;
    public UIButtonBar marchBar;
    public GameObject emptyMarchData;
    public RallyAllocTimePopup POPUP_RallyTimerSet;
    public ReinforceAllocPopup POPUP_ReinforceCheck;
    public GameObject normal;
    public GameObject hasSlot;

    public TroopSlider GetTroopSliderById(string troopId)
    {
      for (int index = 0; index < this.list.Count; ++index)
      {
        if (this.list[index].troopInfo.ID == troopId)
          return this.list[index];
      }
      return (TroopSlider) null;
    }
  }
}
