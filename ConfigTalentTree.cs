﻿// Decompiled with JetBrains decompiler
// Type: ConfigTalentTree
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigTalentTree
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, TalentTreeInfo> datas;
  private Dictionary<int, TalentTreeInfo> dicByUniqueId;
  private TalentTreeInfo[,,] _talentTrees;

  public TalentTreeInfo[,,] talentTrees
  {
    get
    {
      return this._talentTrees;
    }
  }

  public int treeCount { get; private set; }

  public int tierCount { get; private set; }

  public int rowCount { get; private set; }

  public void BuildDB(object res)
  {
    this.parse.Parse<TalentTreeInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    Dictionary<string, TalentTreeInfo>.ValueCollection.Enumerator enumerator1 = this.datas.Values.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      if (enumerator1.Current.tree > this.treeCount)
        this.treeCount = enumerator1.Current.tree;
      if (enumerator1.Current.tier > this.tierCount)
        this.tierCount = enumerator1.Current.tier;
      if (enumerator1.Current.row > this.rowCount)
        this.rowCount = enumerator1.Current.row;
    }
    ++this.treeCount;
    ++this.tierCount;
    ++this.rowCount;
    this._talentTrees = new TalentTreeInfo[this.treeCount, this.tierCount, this.rowCount];
    Dictionary<string, TalentTreeInfo>.ValueCollection.Enumerator enumerator2 = this.datas.Values.GetEnumerator();
    while (enumerator2.MoveNext())
      this._talentTrees[enumerator2.Current.tree, enumerator2.Current.tier, enumerator2.Current.row] = enumerator2.Current;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, TalentTreeInfo>) null;
    }
    if (this.dicByUniqueId != null)
    {
      this.dicByUniqueId.Clear();
      this.dicByUniqueId = (Dictionary<int, TalentTreeInfo>) null;
    }
    this._talentTrees = (TalentTreeInfo[,,]) null;
  }

  public TalentTreeInfo GetTalentTreeInfoByPosition(int treeIndex, int tierIndex, int rowIndex)
  {
    if (treeIndex >= 0 && treeIndex < this.treeCount && (tierIndex >= 0 && tierIndex < this.tierCount) && (rowIndex >= 0 && rowIndex < this.rowCount))
      return this._talentTrees[treeIndex, tierIndex, rowIndex];
    return (TalentTreeInfo) null;
  }

  public TalentTreeInfo GetTalentTreeInfo(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (TalentTreeInfo) null;
  }

  public TalentTreeInfo GetTalentTreeInfo(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (TalentTreeInfo) null;
  }

  public List<TalentTreeInfo> GetTalentsByTreeIndex(int treeIndex)
  {
    List<TalentTreeInfo> talentTreeInfoList = (List<TalentTreeInfo>) null;
    if (treeIndex < this.treeCount)
    {
      talentTreeInfoList = new List<TalentTreeInfo>();
      for (int index1 = 0; index1 < this.tierCount; ++index1)
      {
        for (int index2 = 0; index2 < this.rowCount; ++index2)
        {
          if (this._talentTrees[treeIndex, index1, index2] != null)
            talentTreeInfoList.Add(this._talentTrees[treeIndex, index1, index2]);
        }
      }
    }
    return talentTreeInfoList;
  }
}
