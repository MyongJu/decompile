﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentGemHandbookSort
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigEquipmentGemHandbookSort
{
  private Dictionary<string, EquipmentGemHandbookSortInfo> m_DataByID;
  private Dictionary<int, EquipmentGemHandbookSortInfo> m_DataByInternalID;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<EquipmentGemHandbookSortInfo, string>(result as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
  }

  public EquipmentGemHandbookSortInfo Get(string id)
  {
    EquipmentGemHandbookSortInfo handbookSortInfo;
    this.m_DataByID.TryGetValue(id, out handbookSortInfo);
    return handbookSortInfo;
  }

  public EquipmentGemHandbookSortInfo Get(int internalId)
  {
    EquipmentGemHandbookSortInfo handbookSortInfo;
    this.m_DataByInternalID.TryGetValue(internalId, out handbookSortInfo);
    return handbookSortInfo;
  }

  public List<EquipmentGemHandbookSortInfo> GetDatas()
  {
    List<EquipmentGemHandbookSortInfo> handbookSortInfoList = new List<EquipmentGemHandbookSortInfo>();
    Dictionary<string, EquipmentGemHandbookSortInfo>.Enumerator enumerator = this.m_DataByID.GetEnumerator();
    while (enumerator.MoveNext())
    {
      EquipmentGemHandbookSortInfo handbookSortInfo = enumerator.Current.Value;
      handbookSortInfoList.Add(handbookSortInfo);
    }
    handbookSortInfoList.Sort(new Comparison<EquipmentGemHandbookSortInfo>(this.Compare));
    return handbookSortInfoList;
  }

  private int Compare(EquipmentGemHandbookSortInfo a, EquipmentGemHandbookSortInfo b)
  {
    return Math.Sign(a.priority - b.priority);
  }

  public void Clear()
  {
    if (this.m_DataByID != null)
    {
      this.m_DataByID.Clear();
      this.m_DataByID = (Dictionary<string, EquipmentGemHandbookSortInfo>) null;
    }
    if (this.m_DataByInternalID == null)
      return;
    this.m_DataByInternalID.Clear();
    this.m_DataByInternalID = (Dictionary<int, EquipmentGemHandbookSortInfo>) null;
  }
}
