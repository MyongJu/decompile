﻿// Decompiled with JetBrains decompiler
// Type: ChestItemDetailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;

public class ChestItemDetailPopup : Popup
{
  public IconGroup group;
  public UILabel title;
  private object result;
  public UIScrollView scroll;

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.result = (orgParam as ChestItemDetailPopup.Parameter).result;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    List<ConsumableItemData> consumableItemDataList = new List<ConsumableItemData>();
    this.title.text = ScriptLocalization.Get("id_open_chest_title", true);
    Hashtable result1 = this.result as Hashtable;
    if (result1 != null && result1.Count > 0)
    {
      IEnumerator enumerator = result1.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        string s = enumerator.Current.ToString();
        int result2 = 0;
        if (int.TryParse(s, out result2))
        {
          ConsumableItemData consumableItemData = new ConsumableItemData();
          consumableItemData.internalId = result2;
          int result3 = 0;
          int.TryParse(result1[enumerator.Current].ToString(), out result3);
          consumableItemData.quantity = result3;
          consumableItemDataList.Add(consumableItemData);
        }
      }
    }
    consumableItemDataList.Sort(new Comparison<ConsumableItemData>(this.CompareShopItem));
    List<IconData> datas = new List<IconData>();
    for (int index = 0; index < consumableItemDataList.Count; ++index)
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(consumableItemDataList[index].internalId);
      IconData iconData = new IconData();
      iconData.image = itemStaticInfo.ImagePath;
      iconData.contents = new string[2];
      iconData.contents[0] = itemStaticInfo.LocName;
      iconData.contents[1] = consumableItemDataList[index].quantity.ToString();
      datas.Add(iconData);
    }
    this.group.CreateIcon(datas);
    this.scroll.ResetPosition();
  }

  private int CompareShopItem(ConsumableItemData a, ConsumableItemData b)
  {
    return ConfigManager.inst.DB_Items.GetItem(a.internalId).Priority.CompareTo(ConfigManager.inst.DB_Items.GetItem(b.internalId).Priority);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.group.Dispose();
    this.result = (object) null;
  }

  public class Parameter : Popup.PopupParameter
  {
    public object result;
  }
}
