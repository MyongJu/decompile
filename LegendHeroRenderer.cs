﻿// Decompiled with JetBrains decompiler
// Type: LegendHeroRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class LegendHeroRenderer : MonoBehaviour
{
  public UITexture m_Portrait;
  public UILabel m_Level;
  public UILabel m_Power;
  private LegendData m_LegendData;

  public void SetData(LegendData legendData)
  {
    this.m_LegendData = legendData;
    LegendInfo legendInfo = ConfigManager.inst.DB_Legend.GetLegendInfo(legendData.LegendID);
    if (legendInfo != null)
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Portrait, legendInfo.Icon, (System.Action<bool>) null, true, false, string.Empty);
    else
      D.error((object) "Bad Legend Data.");
    this.m_Power.text = Utils.FormatThousands(legendData.Power.ToString());
    int nextXp = 0;
    this.m_Level.text = "Lv. " + Utils.FormatThousands(ConfigManager.inst.DB_LegendPoint.GetLegendLevelByXP(legendData.Xp, out nextXp).ToString());
  }

  public LegendData GetLegendData()
  {
    return this.m_LegendData;
  }

  public void OnInfoPressed()
  {
  }
}
