﻿// Decompiled with JetBrains decompiler
// Type: UITextureAutoSetter
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UITexture))]
public class UITextureAutoSetter : MonoBehaviour
{
  [SerializeField]
  private string _texturePath;

  private void OnEnable()
  {
    UITexture component = this.GetComponent<UITexture>();
    if (!(bool) ((UnityEngine.Object) component))
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) component, this._texturePath, (System.Action<bool>) null, true, true, string.Empty);
  }
}
