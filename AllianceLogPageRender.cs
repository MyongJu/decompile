﻿// Decompiled with JetBrains decompiler
// Type: AllianceLogPageRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllianceLogPageRender : MonoBehaviour
{
  private List<AllianceFeedComponent> m_ItemList = new List<AllianceFeedComponent>();
  private List<Hashtable> m_DataList = new List<Hashtable>();
  private const int LOG_COUNT_PER_REQUEST = 10;
  public UIScrollView m_ScrollView;
  public GameObject m_NewsfeedPrefab;
  public UIWrapContent wrapContent;
  public GameObject loadMoreSymbol;
  public UILabel loadingTip;
  private int logType;
  private int categoryIndex;
  private bool loadMore;
  private bool isLoadingMore;
  private bool noDataNeedRequired;
  private bool isFirstLoad;
  private Coroutine loadMoreCoroutine;

  private void Start()
  {
    this.RegisterEventHandler();
  }

  public void Show(int index)
  {
    this.ClearData();
    this.m_ScrollView.ResetPosition();
    this.isFirstLoad = true;
    this.wrapContent.enabled = false;
    this.gameObject.SetActive(true);
    this.categoryIndex = index;
    this.SetLogType(this.categoryIndex);
    AllianceLogManager.Instance.RequestAllianceCategoryLog(new System.Action<bool, object>(this.OnRequestedDataReceived), this.categoryIndex, true);
  }

  private void RegisterEventHandler()
  {
    this.wrapContent.onInitializeItem += new UIWrapContent.OnInitializeItem(this.InitItem);
  }

  private void HandleOnDataReceived()
  {
    if (this.isFirstLoad)
      return;
    this.RefreshData();
  }

  private void RemoveEventHandler()
  {
    this.wrapContent.onInitializeItem -= new UIWrapContent.OnInitializeItem(this.InitItem);
    if (this.loadMoreCoroutine == null)
      return;
    Utils.StopCoroutine(this.loadMoreCoroutine);
  }

  private void RefreshData()
  {
    ArrayList dataList = AllianceLogManager.Instance.DataList;
    for (int index = 0; index < dataList.Count; ++index)
      this.m_DataList.Add(dataList[index] as Hashtable);
    if (this.m_DataList.Count > 1)
    {
      this.wrapContent.maxIndex = 0;
      this.wrapContent.minIndex = 1 - this.m_DataList.Count;
    }
    else
    {
      this.wrapContent.minIndex = -1;
      this.wrapContent.maxIndex = 0;
    }
    this.wrapContent.enabled = true;
    this.wrapContent.SortBasedOnScrollMovement();
    this.wrapContent.WrapContent();
    this.m_ScrollView.ResetPosition();
  }

  private void InitItem(GameObject go, int wrapIndex, int realIndex)
  {
    int index = Mathf.Abs(realIndex);
    if (this.m_DataList.Count > 0 && index >= 0 && index < this.m_DataList.Count)
    {
      go.SetActive(true);
      Hashtable data = this.m_DataList[index];
      AllianceFeedComponent component = go.GetComponent<AllianceFeedComponent>();
      component.logType = this.logType;
      component.FeedData<AllianceFeedComponentData>(data);
      this.m_ItemList.Add(component);
    }
    else
      go.SetActive(false);
  }

  private void InitUIWrapContent()
  {
    this.wrapContent.SortBasedOnScrollMovement();
    this.wrapContent.WrapContent();
  }

  private void SetLogType(int index)
  {
    this.logType = index;
  }

  private void OnRequestedDataReceived(bool ret, object orgData)
  {
    if (!ret)
      return;
    this.RefreshData();
    this.m_ScrollView.ResetPosition();
  }

  private void ClearData()
  {
    this.m_ItemList.Clear();
    this.m_DataList.Clear();
  }

  private void UpdateList(ArrayList data)
  {
    if (data == null)
      return;
    for (int index = 0; index < data.Count; ++index)
      this.AddItem(data[index] as Hashtable);
  }

  private void AddItem(Hashtable data)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_NewsfeedPrefab);
    if (!(bool) ((UnityEngine.Object) gameObject))
      return;
    gameObject.SetActive(true);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
    AllianceFeedComponent component = gameObject.GetComponent<AllianceFeedComponent>();
    component.logType = this.logType;
    component.FeedData<AllianceFeedComponentData>(data);
    this.m_ItemList.Add(component);
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
    this.RemoveEventHandler();
    this.ClearData();
  }
}
