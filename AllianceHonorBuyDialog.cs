﻿// Decompiled with JetBrains decompiler
// Type: AllianceHonorBuyDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using UI;

public class AllianceHonorBuyDialog : Popup
{
  public UILabel m_ItemName;
  public UILabel m_ItemDesc;
  public UILabel m_HonorPrice;
  public UITexture m_ItemIcon;
  public UITexture m_HonorIcon;
  public UISlider m_Slider;
  public UIInput m_Input;
  public UIButton m_BuyButton;
  public UITexture m_Background;
  private AllianceHonorBuyDialog.Parameter m_Parameter;
  private int m_PurchaseCount;
  private EventDelegate onSliderChanged;
  private EventDelegate onInputChanged;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as AllianceHonorBuyDialog.Parameter;
    this.m_ItemName.text = this.m_Parameter.allianceShopInfo.Name;
    this.m_ItemDesc.text = this.m_Parameter.allianceShopInfo.Description;
    ConfigManager.inst.DB_Items.GetItem(this.m_Parameter.allianceShopInfo.ItemId);
    Utils.SetItemName(this.m_ItemName, this.m_Parameter.allianceShopInfo.ItemId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, this.m_Parameter.allianceShopInfo.ImageIconPath, (System.Action<bool>) null, true, false, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_HonorIcon, "Texture/Alliance/alliance_honor", (System.Action<bool>) null, true, false, string.Empty);
    Utils.SetItemBackground(this.m_Background, this.m_Parameter.allianceShopInfo.ItemId);
    this.SetPurchaseCount(0);
    this.onSliderChanged = new EventDelegate(new EventDelegate.Callback(this.OnSliderChanged));
    this.onInputChanged = new EventDelegate(new EventDelegate.Callback(this.OnInputChanged));
    this.m_Input.onChange.Clear();
    this.m_Slider.onChange.Clear();
    this.m_Input.onChange.Add(this.onInputChanged);
    this.m_Slider.onChange.Add(this.onSliderChanged);
    AllianceManager.Instance.onLeave += new System.Action(this.OnLeave);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    AllianceManager.Instance.onLeave -= new System.Action(this.OnLeave);
  }

  private void OnLeave()
  {
    this.OnCloseButtonClick();
  }

  public void OnSliderChanged()
  {
    int num = Math.Min(this.m_Parameter.allianceStoreData.quantity, (int) PlayerData.inst.userData.currency.honor / this.m_Parameter.allianceShopInfo.HonorPrice);
    this.m_Slider.onChange.Clear();
    this.SetPurchaseCount((int) Math.Round((double) num * (double) this.m_Slider.value));
    this.m_Slider.onChange.Add(this.onSliderChanged);
  }

  public void OnInputChanged()
  {
    int result = 0;
    int.TryParse(this.m_Input.value, out result);
    this.m_Input.onChange.Clear();
    this.SetPurchaseCount(result);
    this.m_Input.onChange.Add(this.onInputChanged);
  }

  public void OnSubstract()
  {
    this.SetPurchaseCount(this.m_PurchaseCount - 1);
  }

  public void OnAdd()
  {
    this.SetPurchaseCount(this.m_PurchaseCount + 1);
  }

  public void OnBuy()
  {
    this.OnCloseButtonClick();
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemChanged);
    Hashtable postData = new Hashtable();
    postData[(object) "internalId"] = (object) this.m_Parameter.allianceShopInfo.internalId;
    postData[(object) "quantity"] = (object) this.m_PurchaseCount;
    MessageHub.inst.GetPortByAction("AllianceStore:buyAllianceStoreItem").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        UIManager.inst.toast.Show("Failed to purchase!", (System.Action) null, 4f, false);
      DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemChanged);
    }), true);
  }

  private void OnItemChanged(int itemId)
  {
    if (this.m_Parameter.allianceShopInfo.ItemId != itemId)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    ItemRewardInfo.Data data = new ItemRewardInfo.Data();
    data.icon = itemStaticInfo.ImagePath;
    data.count = (float) this.m_PurchaseCount;
    RewardsCollectionAnimator.Instance.items.Clear();
    RewardsCollectionAnimator.Instance.items.Add(data);
    RewardsCollectionAnimator.Instance.CollectItems(true);
  }

  public void OnCloseButtonClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void SetPurchaseCount(int count)
  {
    int honorPrice = this.m_Parameter.allianceShopInfo.HonorPrice;
    int val2 = Math.Min(this.m_Parameter.allianceStoreData.quantity, (int) PlayerData.inst.userData.currency.honor / honorPrice);
    this.m_PurchaseCount = Math.Max(1, Math.Min(count, val2));
    this.m_HonorPrice.text = Utils.FormatThousands((this.m_PurchaseCount * honorPrice).ToString());
    this.m_BuyButton.isEnabled = this.m_PurchaseCount > 0;
    this.m_Slider.value = val2 <= 0 ? 0.0f : (float) this.m_PurchaseCount / (float) val2;
    this.m_Input.value = this.m_PurchaseCount.ToString();
  }

  public class Parameter : Popup.PopupParameter
  {
    public AllianceShopInfo allianceShopInfo;
    public AllianceStoreData allianceStoreData;
  }
}
