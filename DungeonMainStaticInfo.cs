﻿// Decompiled with JetBrains decompiler
// Type: DungeonMainStaticInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DungeonMainStaticInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "level")]
  public int Level;
  [Config(Name = "room_background")]
  public string RoomBackground;

  public string RoomBackgroundPath
  {
    get
    {
      return "Prefab/DragonKnight/Objects/Room/" + this.RoomBackground;
    }
  }
}
