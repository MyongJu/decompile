﻿// Decompiled with JetBrains decompiler
// Type: DungeonRewardSubItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DungeonRewardSubItemRenderer : MonoBehaviour
{
  public UITexture itemTexture;
  public UILabel itemName;
  public UILabel itemCount;
  public UISprite itemFrame1;
  public UISprite itemFrame2;
  private ItemStaticInfo _itemInfo;
  private int _itemCount;
  private int _itemIndex;

  public void SetData(ItemStaticInfo itemInfo, int itemCount, int itemIndex)
  {
    this._itemInfo = itemInfo;
    this._itemCount = itemCount;
    this._itemIndex = itemIndex;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this._itemInfo != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.itemTexture, this._itemInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this.itemName.text = this._itemInfo.LocName;
      this.itemCount.text = Utils.FormatThousands(this._itemCount.ToString());
    }
    NGUITools.SetActive(this.itemFrame1.gameObject, this._itemIndex % 2 == 0);
    NGUITools.SetActive(this.itemFrame2.gameObject, this._itemIndex % 2 == 1);
  }
}
