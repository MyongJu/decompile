﻿// Decompiled with JetBrains decompiler
// Type: SignSingleRewardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class SignSingleRewardPopup : Popup
{
  public UITexture m_ItemIcon;
  public UILabel m_ItemName;
  public UILabel m_ItemDescription;
  public UILabel m_ItemCount;
  public UITexture m_Background;
  private int m_ItemId;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    SignSingleRewardPopup.Parameter parameter = orgParam as SignSingleRewardPopup.Parameter;
    this.m_ItemId = parameter.dropMainData.ItemId;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(parameter.dropMainData.ItemId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.m_ItemCount.text = "x " + parameter.dropMainData.MinValue.ToString();
    this.m_ItemDescription.text = itemStaticInfo.LocDescription;
    Utils.SetItemBackground(this.m_Background, itemStaticInfo.internalId);
    Utils.SetItemName(this.m_ItemName, itemStaticInfo.internalId);
  }

  public void OnItemPress()
  {
    Utils.DelayShowTip(this.m_ItemId, this.m_Background.transform, 0L, 0L, 0);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  public void OnItemClick()
  {
    Utils.ShowItemTip(this.m_ItemId, this.m_Background.transform, 0L, 0L, 0);
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public DropMainData dropMainData;
  }
}
