﻿// Decompiled with JetBrains decompiler
// Type: AllianceDonationPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceDonationPopup : BaseReportPopup
{
  public UILabel content;
  public UILabel firstreward;
  public UILabel secondreward;
  public UILabel thirdreward;
  public UILabel result;
  public RewardItemComponent ric;
  public GameObject btnGroup;
  public GameObject btnDonate;
  public GameObject btnCollection;

  private void Init()
  {
    this.content.text = this.param.mail.GetBodyString();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, true, false, string.Empty);
    Dictionary<string, string> para = new Dictionary<string, string>();
    para["0"] = ConfigManager.inst.DB_ConfigADR.GetRewardsContent(1);
    this.firstreward.text = ScriptLocalization.GetWithPara("mail_subject_donation_first_place", para, true);
    para["0"] = ConfigManager.inst.DB_ConfigADR.GetRewardsContent(2);
    this.secondreward.text = ScriptLocalization.GetWithPara("mail_subject_donation_second_place", para, true);
    para["0"] = ConfigManager.inst.DB_ConfigADR.GetRewardsContent(3);
    this.thirdreward.text = ScriptLocalization.GetWithPara("mail_subject_donation_third_place", para, true);
    if (this.param.mail.body != null)
    {
      int result = 0;
      int.TryParse(this.param.mail.body[(object) "rank"].ToString(), out result);
      para["0"] = result.ToString();
      this.result.text = ScriptLocalization.GetWithPara("mail_subject_donation_ranking_place", para, true);
      this.ric.FeedData((IComponentData) this.AnalyzeCollectedRewards());
    }
    else
      this.result.text = ScriptLocalization.Get("mail_subject_donation_ranking_no_place", true);
    this.ArrangeLayout();
    this.UpdateBtnState();
  }

  private void CollectRewards()
  {
    Hashtable hashtable = this.param.mail.data[(object) "attachment"] as Hashtable;
    if (hashtable == null)
      return;
    RewardData rewardData = JsonReader.Deserialize<RewardData>(Utils.Object2Json((object) hashtable));
    List<IconData> iconDataList = new List<IconData>();
    if (rewardData.item != null)
    {
      RewardsCollectionAnimator.Instance.Clear();
      using (Dictionary<string, int>.Enumerator enumerator = rewardData.item.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int internalId = int.Parse(current.Key);
          int num = current.Value;
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
          RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
          {
            icon = itemStaticInfo.ImagePath,
            count = (float) num
          });
        }
      }
    }
    if (rewardData.resource != null)
    {
      using (Dictionary<string, int>.Enumerator enumerator = rewardData.resource.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int num = current.Value;
          ResRewardsInfo.Data data = new ResRewardsInfo.Data();
          data.count = num;
          if ("food" == current.Key)
            data.rt = ResRewardsInfo.ResType.Food;
          else if ("wood" == current.Key)
            data.rt = ResRewardsInfo.ResType.Wood;
          RewardsCollectionAnimator.Instance.Ress.Add(data);
        }
      }
    }
    RewardsCollectionAnimator.Instance.CollectItems(false);
    RewardsCollectionAnimator.Instance.CollectResource(false);
  }

  private void ArrangeLayout()
  {
    if (this.param.mail.body != null)
    {
      this.ric.gameObject.SetActive(true);
      this.btnDonate.SetActive(false);
      this.btnCollection.SetActive(true);
    }
    else
    {
      this.ric.gameObject.SetActive(false);
      this.btnDonate.SetActive(true);
      this.btnCollection.SetActive(false);
    }
  }

  private void UpdateBtnState()
  {
    if (this.param.mail.attachment_status == 2)
      this.btnCollection.GetComponent<UIButton>().isEnabled = false;
    else
      this.btnCollection.GetComponent<UIButton>().isEnabled = true;
  }

  public void OnDonationClicked()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    UIManager.inst.OpenDlg("Alliance/AllianceTechDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public new void OnClaimRewardClicked()
  {
    this.CollectRewards();
    PlayerData.inst.mail.API.GainAttachment((int) this.param.mail.category, this.param.mail.mailID, new System.Action<bool, object>(this.GainAttachmentCallBack));
  }

  private void GainAttachmentCallBack(bool ok, object obj)
  {
    if (!ok)
      return;
    this.param.mail.attachment_status = 2;
    this.UpdateBtnState();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }
}
