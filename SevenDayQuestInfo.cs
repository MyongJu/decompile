﻿// Decompiled with JetBrains decompiler
// Type: SevenDayQuestInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class SevenDayQuestInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "day")]
  public int Day;
  [Config(Name = "sort")]
  public int Sort;
  [Config(Name = "type")]
  public string Type;
  [Config(Name = "param_1")]
  public string Param1;
  [Config(Name = "param_2")]
  public string Param2;
  [Config(Name = "description")]
  public string Desc;
  [Config(Name = "reward_id_1")]
  public int ItemId;
  [Config(Name = "reward_value_1")]
  public int ItemCount;

  public string LocalDesc
  {
    get
    {
      if (string.IsNullOrEmpty(this.Desc))
        return string.Empty;
      Dictionary<string, string> para = new Dictionary<string, string>();
      if (!string.IsNullOrEmpty(this.Param1))
        para.Add("0", this.Param1);
      if (this.Type == "epic_quest")
      {
        QuestGroupInfo data = ConfigManager.inst.DB_QuestGroup.GetData(this.Param1);
        if (data != null)
          para["0"] = Utils.XLAT(data.loc_name_id);
      }
      if (!string.IsNullOrEmpty(this.Param2))
      {
        string Term = "equip_color_" + this.Param2;
        string str = this.Param2;
        if (this.Type == "equipment")
          str = ScriptLocalization.Get(Term, true);
        if (para.Count == 0)
          para.Add("0", str);
        else
          para.Add("1", str);
      }
      if (para.Count > 0)
        return ScriptLocalization.GetWithPara(this.Desc, para, true);
      return ScriptLocalization.Get(this.Desc, true);
    }
  }
}
