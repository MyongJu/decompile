﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryMainDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class AnniversaryMainDlg : UI.Dialog
{
  private Dictionary<int, AnniversaryCatalog> _catalogItemDict = new Dictionary<int, AnniversaryCatalog>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private AnniversaryIapView _iapView;
  [SerializeField]
  private UIScrollView _catalogScrollView;
  [SerializeField]
  private UITable _catalogTable;
  [SerializeField]
  private GameObject _catalogItemPrefab;
  [SerializeField]
  private GameObject _catalogItemRoot;
  [SerializeField]
  private AnniversaryKingArena _kingArena;
  [SerializeField]
  private NewLordController _newLord;
  [SerializeField]
  private CommunityEventsController _communityEvent;
  private AnniversaryCatalog.CatalogType _defaultCatalogType;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    AnniversaryMainDlg.Parameter parameter = orgParam as AnniversaryMainDlg.Parameter;
    if (parameter != null)
      this._defaultCatalogType = parameter.catalogType;
    this.UpdateCatalog();
    AnniversaryManager.Instance.OnDataUpdate += new System.Action(this.OnDataChange);
  }

  private void OnDataChange()
  {
    if (AnniversaryManager.Instance.EndTime > NetServerTime.inst.ServerTimestamp)
      return;
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.HideAllView();
    this.ClearCatalogData();
    AnniversaryManager.Instance.OnDataUpdate -= new System.Action(this.OnDataChange);
  }

  private void UpdateCatalog()
  {
    this.ClearCatalogData();
    string empty = string.Empty;
    if (AnniversaryIapPayload.Instance.ShouldShowAnnvIap)
      this._catalogItemDict.Add(0, this.GenerateCatalogSlot(string.Format("{0}\n{1}", (object) Utils.XLAT("event_1_year_celebration_carnival_name"), (object) AnniversaryIapPayload.Instance.FormatTimeStamp(AnniversaryIapPayload.Instance.KcStartTime, AnniversaryIapPayload.Instance.KcEndTime, AnniversaryIapPayload.Instance.EndTime)), AnniversaryCatalog.CatalogType.IAP_PACKAGE));
    if ((long) NetServerTime.inst.ServerTimestamp >= AnniversaryIapPayload.Instance.TournamentShowTime && (long) NetServerTime.inst.ServerTimestamp < AnniversaryIapPayload.Instance.EndTime)
      this._catalogItemDict.Add(1, this.GenerateCatalogSlot(string.Format("{0}\n{1}", (object) Utils.XLAT("event_1_year_celebration_1_year_tournament_name"), (object) AnniversaryIapPayload.Instance.FormatTimeStamp(AnniversaryIapPayload.Instance.KcStartTime, AnniversaryIapPayload.Instance.KcEndTime, AnniversaryIapPayload.Instance.EndTime)), AnniversaryCatalog.CatalogType.TOURNAMENT));
    if (NetServerTime.inst.ServerTimestamp >= AnniversaryIapPayload.Instance.PortraitStartTime && NetServerTime.inst.ServerTimestamp < AnniversaryIapPayload.Instance.PortraitEndTime)
      this._catalogItemDict.Add(2, this.GenerateCatalogSlot(string.Format("{0}\n{1}", (object) Utils.XLAT("event_1_year_celebration_new_equipment_name"), (object) AnniversaryIapPayload.Instance.FormatTimeStamp(AnniversaryIapPayload.Instance.PortraitStartTime, AnniversaryIapPayload.Instance.PortraitEndTime, 0L)), AnniversaryCatalog.CatalogType.NEW_SKIN));
    if (NetServerTime.inst.ServerTimestamp >= AnniversaryIapPayload.Instance.CommunityStartTime && NetServerTime.inst.ServerTimestamp < AnniversaryIapPayload.Instance.CommunityEndTime)
      this._catalogItemDict.Add(3, this.GenerateCatalogSlot(string.Format("{0}\n{1}", (object) Utils.XLAT("event_1_year_celebration_community_event_name"), (object) AnniversaryIapPayload.Instance.FormatTimeStamp(AnniversaryIapPayload.Instance.CommunityStartTime, AnniversaryIapPayload.Instance.CommunityEndTime, 0L)), AnniversaryCatalog.CatalogType.COMMUNITY_EVENT));
    this.RepositionCatalog();
  }

  private AnniversaryCatalog GenerateCatalogSlot(string catalogName, AnniversaryCatalog.CatalogType catalogType)
  {
    this._itemPool.Initialize(this._catalogItemPrefab, this._catalogItemRoot);
    GameObject go = this._itemPool.AddChild(this._catalogTable.gameObject);
    NGUITools.SetActive(go, true);
    AnniversaryCatalog component = go.GetComponent<AnniversaryCatalog>();
    bool flag1 = catalogType == AnniversaryCatalog.CatalogType.IAP_PACKAGE || catalogType == AnniversaryCatalog.CatalogType.TOURNAMENT;
    component.OnCurrentCatalogClicked += new System.Action<AnniversaryCatalog.CatalogType, int>(this.OnCatalogClicked);
    AnniversaryCatalog anniversaryCatalog = component;
    bool flag2 = catalogType == this._defaultCatalogType;
    string name = catalogName;
    int num1 = (int) catalogType;
    int catalogIndex = -1;
    int num2 = flag2 ? 1 : 0;
    int num3 = flag1 ? 1 : 0;
    anniversaryCatalog.SetData(name, (AnniversaryCatalog.CatalogType) num1, catalogIndex, num2 != 0, num3 != 0);
    return component;
  }

  private void RepositionCatalog()
  {
    this._catalogTable.repositionNow = true;
    this._catalogTable.Reposition();
    this._catalogScrollView.ResetPosition();
  }

  private void ClearCatalogData()
  {
    using (Dictionary<int, AnniversaryCatalog>.Enumerator enumerator = this._catalogItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, AnniversaryCatalog> current = enumerator.Current;
        current.Value.OnCurrentCatalogClicked -= new System.Action<AnniversaryCatalog.CatalogType, int>(this.OnCatalogClicked);
        current.Value.ClearData();
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._catalogItemDict.Clear();
    this._itemPool.Clear();
  }

  private void OnCatalogClicked(AnniversaryCatalog.CatalogType catalogType, int catalogIndex)
  {
    this.HideAllView();
    switch (catalogType)
    {
      case AnniversaryCatalog.CatalogType.IAP_PACKAGE:
        AnniversaryIapPayload.Instance.RequestServerData((System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          this._iapView.Show();
        }));
        break;
      case AnniversaryCatalog.CatalogType.TOURNAMENT:
        this.ShowTournamentButtons();
        this._kingArena.Show();
        break;
      case AnniversaryCatalog.CatalogType.NEW_SKIN:
        this._newLord.Show();
        break;
      case AnniversaryCatalog.CatalogType.COMMUNITY_EVENT:
        this._communityEvent.Show();
        break;
    }
    this._defaultCatalogType = catalogType;
  }

  private void HideAllView()
  {
    this._iapView.Hide();
    this._kingArena.Hide();
    this._newLord.Hide();
    this._communityEvent.Hide();
    this.HideTournamentButtons();
  }

  public void OnOpenRewards()
  {
    UIManager.inst.OpenPopup("Anniversary/AnniversaryKingRewardPopup", (Popup.PopupParameter) null);
  }

  public void OnOpenHistroy()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceMagicWarLogPop", (Popup.PopupParameter) new AllianceMagicHistoryPopup.Parameter()
    {
      action = "anniversary:getChallengeHistory",
      viewMail = true
    });
  }

  private void ShowTournamentButtons()
  {
    Transform child1 = this.transform.FindChild("Root/Top/Btn_Help");
    if ((bool) ((UnityEngine.Object) child1))
      NGUITools.SetActive(child1.gameObject, true);
    Transform child2 = this.transform.FindChild("Root/Top/Btn_histroy");
    if ((bool) ((UnityEngine.Object) child2))
      NGUITools.SetActive(child2.gameObject, true);
    Transform child3 = this.transform.FindChild("Root/Top/Btn_reward");
    if (!(bool) ((UnityEngine.Object) child3))
      return;
    NGUITools.SetActive(child3.gameObject, true);
  }

  private void HideTournamentButtons()
  {
    Transform child1 = this.transform.FindChild("Root/Top/Btn_Help");
    if ((bool) ((UnityEngine.Object) child1))
      NGUITools.SetActive(child1.gameObject, false);
    Transform child2 = this.transform.FindChild("Root/Top/Btn_histroy");
    if ((bool) ((UnityEngine.Object) child2))
      NGUITools.SetActive(child2.gameObject, false);
    Transform child3 = this.transform.FindChild("Root/Top/Btn_reward");
    if (!(bool) ((UnityEngine.Object) child3))
      return;
    NGUITools.SetActive(child3.gameObject, false);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public AnniversaryCatalog.CatalogType catalogType;
  }
}
