﻿// Decompiled with JetBrains decompiler
// Type: MarchDetailSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class MarchDetailSlot : MonoBehaviour
{
  private const string TEXTURE_PATH_PRE = "Texture/Unit/portrait_unit_";
  public UITexture TT_troopIcon;
  public UILabel LB_troopName;
  public UILabel LB_troopCount;

  public string troopID
  {
    set
    {
      Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(value);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.TT_troopIcon, string.Format("{0}{1}", (object) "Texture/Unit/portrait_unit_", (object) data.Image), (System.Action<bool>) null, true, false, string.Empty);
      this.LB_troopName.text = ScriptLocalization.Get(string.Format("{0}{1}", (object) data.ID, (object) "_name"), true);
    }
  }

  public int troopCount
  {
    set
    {
      this.LB_troopCount.text = Utils.ConvertNumberToNormalString(value);
    }
  }
}
