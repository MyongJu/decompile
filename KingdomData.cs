﻿// Decompiled with JetBrains decompiler
// Type: KingdomData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class KingdomData
{
  private int m_KingdomX;
  private int m_KingdomY;
  private int m_ID;
  private Rect m_Dimension;
  private DynamicMapData m_MapData;
  private KingdomView m_KingdomView;

  public int KingdomX
  {
    get
    {
      return this.m_KingdomX;
    }
  }

  public int KingdomY
  {
    get
    {
      return this.m_KingdomY;
    }
  }

  public string PrefabName
  {
    get
    {
      return this.m_MapData.KingdomGround;
    }
  }

  public int ID
  {
    get
    {
      return this.m_ID;
    }
  }

  public Rect Dimension
  {
    get
    {
      return this.m_Dimension;
    }
  }

  public DynamicMapData MapData
  {
    get
    {
      return this.m_MapData;
    }
  }

  public void Reset(int kingdomX, int kingdomY, DynamicMapData mapData)
  {
    this.m_MapData = mapData;
    this.m_KingdomX = kingdomX;
    this.m_KingdomY = kingdomY;
    this.m_ID = HelixArrayCalaculater.GetIndex(kingdomX, kingdomY);
    Vector2 kingdomPixelOrigin = this.m_MapData.CalculateKingdomPixelOrigin(kingdomX, kingdomY);
    this.m_Dimension = new Rect(kingdomPixelOrigin.x, kingdomPixelOrigin.y, this.m_MapData.PixelsPerKingdom.x, -this.m_MapData.PixelsPerKingdom.y);
  }

  public void CullAndShow(Rect viewport, Transform transform)
  {
    if (this.m_KingdomView == null)
      this.m_KingdomView = new KingdomView(this);
    this.m_KingdomView.CullAndShow(viewport, transform);
  }

  public void Dispose()
  {
    if (this.m_KingdomView != null)
    {
      this.m_KingdomView.Dispose();
      this.m_KingdomView = (KingdomView) null;
    }
    this.m_KingdomX = 0;
    this.m_KingdomY = 0;
    this.m_ID = 0;
    this.m_Dimension = new Rect();
  }

  public bool Showing
  {
    get
    {
      if (this.m_KingdomView != null)
        return this.m_KingdomView.Showing;
      return false;
    }
  }

  public void DrawBlockBorders(Color color)
  {
    if (!this.Showing)
      return;
    int x1 = (int) this.m_MapData.BlocksPerKingdom.x;
    int y1 = (int) this.m_MapData.BlocksPerKingdom.y;
    int x2 = (int) this.m_MapData.PixelsPerBlock.x;
    int y2 = (int) this.m_MapData.PixelsPerBlock.y;
    float num = 0.01f;
    if (GameEngine.IsAvailable)
      num = GameEngine.Instance.tileMapScale;
    Vector3 vector3_1 = new Vector3(this.m_Dimension.x, this.m_Dimension.y, 0.0f);
    for (int index1 = 0; index1 < x1; ++index1)
    {
      for (int index2 = 0; index2 < y1; ++index2)
      {
        Vector3 vector3_2 = new Vector3(vector3_1.x + (float) (index1 * x2), vector3_1.y - (float) (index2 * y2));
        Vector3 vector3_3 = new Vector3(vector3_2.x + (float) x2, vector3_2.y);
        Vector3 vector3_4 = new Vector3(vector3_2.x + (float) x2, vector3_2.y - (float) y2);
        Vector3 vector3_5 = new Vector3(vector3_2.x, vector3_2.y - (float) y2);
        vector3_2 *= num;
        vector3_3 *= num;
        vector3_4 *= num;
        vector3_5 *= num;
        Debug.DrawLine(vector3_2, vector3_3, color);
        Debug.DrawLine(vector3_3, vector3_4, color);
        Debug.DrawLine(vector3_4, vector3_5, color);
        Debug.DrawLine(vector3_5, vector3_2, color);
      }
    }
  }
}
