﻿// Decompiled with JetBrains decompiler
// Type: AccountManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Funplus;
using Funplus.Internal;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AccountManager
{
  private string _accountId = string.Empty;
  private int maxTime = 30;
  private bool _isVerification = true;
  private int showTime = 7200;
  private Dictionary<string, string> _gift = new Dictionary<string, string>();
  private Dictionary<FunplusAccountType, AccountInfo> accounts = new Dictionary<FunplusAccountType, AccountInfo>();
  private int[] recommendLevels = new int[3]{ 10, 13, 16 };
  private FunplusAccountType unBindType = FunplusAccountType.FPAccountTypeUnknown;
  public const int WALL_LEVEL = 5;
  public const int MAX_ACCOUNT = 5;
  public const string AccountKey = "dw_account_id";
  public const string ACCOUNT_TYPE_EMAIL = "email";
  public const string ACCOUNT_TYPE_FACEBOOK = "facebook";
  public const string ACCOUNT_TYPE_GOOGLE_PLUS = "googlePlus";
  public const string ACCOUNT_TYPE_GAME_CENTER = "gameCenter";
  public const string ACCOUNT_TYPE_VK = "vk";
  public const string ACCOUNT_TYPE_WE_CHAT = "weChat";
  private static AccountManager _instance;
  private FunplusAccountAgent accountAgent;
  private FunplusSDKAgent sdkAgent;
  private FunplusFacebookAgent fbAgent;
  private System.Action<bool> OnInitCompleteCallBack;
  private bool helpShift;
  private float time;
  private bool _replaceLock;
  private bool init;
  private int taskId;
  private bool needToBind;
  private bool _isShowContact;
  private bool _delayShow;
  private bool _isShowCertifying;
  private bool _oldSkipFlag;

  public event System.Action OnBindAccountSuccess;

  public event System.Action OnUnBindAccountSuccess;

  public event System.Action OnGetFacebookDataSuccess;

  public static AccountManager Instance
  {
    get
    {
      if (AccountManager._instance == null)
        AccountManager._instance = new AccountManager();
      return AccountManager._instance;
    }
  }

  public bool HelpShift
  {
    get
    {
      return this.helpShift;
    }
    set
    {
      this.helpShift = value;
    }
  }

  public void SetVerification()
  {
    this._isVerification = true;
  }

  public bool IsVerification()
  {
    return this._isVerification;
  }

  public bool CanShowVerification()
  {
    if (CustomDefine.IsSQWanPackage() || this.IsVerification() || (PlayerData.inst.CityData.mStronghold.mLevel < 6 || NetServerTime.inst.ServerTimestamp - PlayerPrefsEx.GetIntByUid("verification_time") < this.showTime))
      return false;
    PlayerPrefsEx.SetIntByUid("verification_time", NetServerTime.inst.ServerTimestamp);
    return true;
  }

  public void ShowVerification()
  {
    if (CustomDefine.IsSQWanPackage() || !this.CanShowVerification())
      return;
    UIManager.inst.OpenDlg("VerificationDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnHelpShiftCallBack()
  {
    this.HelpShift = true;
    ToastManager.Instance.AddBasicToast("toast_helpshift_message");
  }

  public void AddGift(string key, string value)
  {
    if (!this._gift.ContainsKey(key))
      this._gift.Add(key, value);
    else
      this._gift[key] = value;
  }

  public Dictionary<string, string> Gift
  {
    get
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      if (this._gift.Count > 0)
      {
        Dictionary<string, string>.KeyCollection.Enumerator enumerator = this._gift.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (!dictionary.ContainsKey(enumerator.Current))
            dictionary.Add(enumerator.Current, this._gift[enumerator.Current]);
        }
      }
      if (this.sdkAgent != null && this.sdkAgent.Gift.Count > 0)
      {
        Dictionary<string, string>.KeyCollection.Enumerator enumerator = this.sdkAgent.Gift.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (!dictionary.ContainsKey(enumerator.Current))
            dictionary.Add(enumerator.Current, this.sdkAgent.Gift[enumerator.Current]);
        }
      }
      return dictionary;
    }
  }

  public bool CanShowVK()
  {
    return Application.isEditor || !CustomDefine.IsiOSCNDefine() && (PlayerData.inst.userData == null && (Language.Instance.GetLocalizationLanguage() == "ru" || Language.Instance.GetLocalizationLanguage() == "ru_RU") || PlayerData.inst.userData != null && (PlayerData.inst.userData.language == "ru" || PlayerData.inst.userData.language == "ru_RU"));
  }

  public bool NeedPathCheck { get; set; }

  public bool CheckDeepLink()
  {
    if (Application.isEditor || this.sdkAgent == null || string.IsNullOrEmpty(this.sdkAgent.DeepLinkURL))
      return false;
    if (this.sdkAgent.DeepLinkURL.IndexOf("page=1") > -1)
    {
      if (PlayerData.inst.allianceId > 0L)
        UIManager.inst.OpenDlg("Alliance/AllianceDlg", (UI.Dialog.DialogParameter) null, true, true, true);
      else
        UIManager.inst.OpenDlg("Alliance/AllianceJoinDlg", (UI.Dialog.DialogParameter) null, true, true, true);
      return true;
    }
    if (this.sdkAgent.DeepLinkURL.IndexOf("page=2") <= -1)
      return false;
    UIManager.inst.OpenDlg("Activity/ActivityMainDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    return true;
  }

  public bool CanShowGoolgePlay()
  {
    return true;
  }

  public bool CanShowFacebook()
  {
    return !CustomDefine.IsiOSCNDefine();
  }

  public void LockReplaceAccount()
  {
    this._replaceLock = true;
  }

  public bool ReplaceLock()
  {
    return this._replaceLock;
  }

  public bool CanShowHelpShift()
  {
    return this._isShowContact && (this.CanShowWechat() || PlayerData.inst.userData == null && Language.Instance.GetLocalizationLanguage() == "zh-TW" || PlayerData.inst.userData != null && PlayerData.inst.userData.language == "zh-TW");
  }

  public bool CanShowWechat()
  {
    return PlayerData.inst.userData == null && Language.Instance.GetLocalizationLanguage() == "zh-CN" || PlayerData.inst.userData != null && PlayerData.inst.userData.language == "zh-CN";
  }

  public string AccountId
  {
    get
    {
      if (string.IsNullOrEmpty(this._accountId))
        this.GetAccount();
      return this._accountId;
    }
  }

  public string CreateAccount()
  {
    return NativeManager.inst.DeviceId;
  }

  public void ChangeAccount(string accountId)
  {
    this._accountId = accountId;
    this.SaveAccount();
    GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Deep);
  }

  public void SaveAccount()
  {
    PlayerPrefsEx.SetString("dw_account_id", this._accountId);
    PlayerPrefsEx.Save();
  }

  public void GetAccount()
  {
    if (PlayerPrefsEx.HasKey("dw_account_id"))
    {
      this._accountId = PlayerPrefsEx.GetString("dw_account_id");
    }
    else
    {
      this._accountId = this.CreateAccount();
      this.SaveAccount();
    }
  }

  public void ClearAccounts()
  {
    this.accounts.Clear();
  }

  public void AddAccount(string id, string type, int ctime)
  {
    if (Application.isEditor || string.IsNullOrEmpty(id) || string.IsNullOrEmpty(type))
      return;
    this.CacheAccount(new AccountInfo()
    {
      AccountID = id,
      AccountType = this.GetAccountType(type),
      AccountName = this.GetAccountName(type),
      AccountEmail = this.GetAccountEmail(type),
      Type = type,
      CTime = ctime,
      IsSyn = true
    });
  }

  private void CacheAccount(AccountInfo account)
  {
    if (!this.accounts.ContainsKey(account.AccountType))
      this.accounts.Add(account.AccountType, account);
    else
      this.accounts[account.AccountType] = account;
    PlayerPrefs.SetString(account.Type + "_" + this.accountAgent.FunplusID.ToString() + "_accountName", account.AccountName);
    PlayerPrefs.SetString(account.Type + "_" + this.accountAgent.FunplusID.ToString() + "_accountEmail", account.AccountEmail);
  }

  private string GetAccountName(string type)
  {
    return PlayerPrefs.GetString(type + "_" + this.accountAgent.FunplusID.ToString() + "_accountName");
  }

  private string GetAccountEmail(string type)
  {
    return PlayerPrefs.GetString(type + "_" + this.accountAgent.FunplusID.ToString() + "_accountEmail");
  }

  private FunplusAccountType GetAccountType(string type)
  {
    FunplusAccountType funplusAccountType = FunplusAccountType.FPAccountTypeUnknown;
    string key = type;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AccountManager.\u003C\u003Ef__switch\u0024map57 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AccountManager.\u003C\u003Ef__switch\u0024map57 = new Dictionary<string, int>(6)
        {
          {
            "facebook",
            0
          },
          {
            "googlePlus",
            1
          },
          {
            "email",
            2
          },
          {
            "gameCenter",
            3
          },
          {
            "vk",
            4
          },
          {
            "weChat",
            5
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AccountManager.\u003C\u003Ef__switch\u0024map57.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            funplusAccountType = FunplusAccountType.FPAccountTypeFacebook;
            break;
          case 1:
            funplusAccountType = FunplusAccountType.FPAccountTypeGooglePlus;
            break;
          case 2:
            funplusAccountType = FunplusAccountType.FPAccountTypeEmail;
            break;
          case 3:
            funplusAccountType = FunplusAccountType.FPAccountTypeGameCenter;
            break;
          case 4:
            funplusAccountType = FunplusAccountType.FPAccountTypeVK;
            break;
          case 5:
            funplusAccountType = FunplusAccountType.FPAccountTypeWechat;
            break;
        }
      }
    }
    return funplusAccountType;
  }

  public void GetFaceBookUserData()
  {
    FunplusFacebook.Instance.GetUserData();
  }

  public void Initialize(System.Action<bool> callBack)
  {
    if (CustomDefine.IsSQWanPackage() && this.init)
    {
      callBack(true);
    }
    else
    {
      this.accounts.Clear();
      this.OnInitCompleteCallBack = callBack;
      if (this.init && this.accountAgent != null)
      {
        FunplusSdk.Instance.SetDelegate((FunplusSdk.IDelegate) this.sdkAgent);
        FunplusAccount.Instance.SetDelegate((FunplusAccount.IDelegate) this.accountAgent);
        if (this.accountAgent.IsLoggedIn)
        {
          this.OnAccountInstallFinish(this.accountAgent.IsLoggedIn);
        }
        else
        {
          this.accountAgent.OnInitCompleteCallBack = new System.Action<bool>(this.OnAccountInstallFinish);
          FunplusAccount.Instance.OpenSession();
        }
      }
      else
      {
        if (this.sdkAgent == null)
        {
          this.sdkAgent = new FunplusSDKAgent();
          this.sdkAgent.OnInstallSuccessHandler += new System.Action(this.OnFunplusSDKInstallSuccess);
          this.sdkAgent.OnHelpShiftReceive += new System.Action(this.OnHelpShiftCallBack);
          this.sdkAgent.OnReveiveDeepLinkHandler += new System.Action(this.OnDeepLinkHandler);
        }
        FunplusSdk.Instance.SetDelegate((FunplusSdk.IDelegate) this.sdkAgent);
        this.Install();
      }
    }
  }

  private void OnDeepLinkHandler()
  {
    if (Application.isEditor || this.sdkAgent == null || !GameEngine.IsReady() || this.sdkAgent.DeepLinkURL.IndexOf("page=1") <= -1 && this.sdkAgent.DeepLinkURL.IndexOf("page=2") <= -1)
      return;
    GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Lite);
  }

  private void Install()
  {
    string str = "production";
    if (CustomDefine.IsProductionSDK())
      str = "production";
    if (CustomDefine.IsSandboxSDK())
      str = "sandbox";
    if (!SettingReader.Instance.IsReady)
      FunplusSettings.Environment = str;
    FunplusSdk.Instance.Install();
  }

  public FunplusSocialUser FacebookUser
  {
    get
    {
      return this.fbAgent.UserData;
    }
  }

  private void OnGetUserDataSuccess()
  {
    this.fbAgent.OnFetchUserDataSuccess -= new System.Action(this.OnGetUserDataSuccess);
    if (this.OnGetFacebookDataSuccess != null)
      this.OnGetFacebookDataSuccess();
    AccountInfo account = !this.accounts.ContainsKey(FunplusAccountType.FPAccountTypeFacebook) ? new AccountInfo() : this.accounts[FunplusAccountType.FPAccountTypeFacebook];
    account.AccountID = this.accountAgent.SNS;
    account.AccountEmail = this.fbAgent.UserData.GetEmail();
    account.AccountName = this.fbAgent.UserData.GetName();
    account.AccountType = this.accountAgent.AccountType;
    account.Type = this.GetNormalType(this.accountAgent.AccountType);
    this.CacheAccount(account);
    if (this.needToBind)
    {
      this.needToBind = false;
      RequestManager.inst.SendRequest("player:bindAccount", Utils.Hash((object) "account_name", (object) account.AccountID, (object) "account_type", (object) account.Type), (System.Action<bool, object>) null, true);
    }
    if (this.OnBindAccountSuccess == null)
      return;
    this.OnBindAccountSuccess();
  }

  private void OnFunplusSDKInstallSuccess()
  {
    if (CustomDefine.IsSQWanPackage())
    {
      this.init = true;
      if (this.OnInitCompleteCallBack == null)
        return;
      this.OnInitCompleteCallBack(true);
    }
    else
    {
      if (this.accountAgent == null)
      {
        this.accountAgent = new FunplusAccountAgent();
        this.accountAgent.OnLoginAccountFail += new System.Action<string>(this.OnFirstLoginFail);
        this.accountAgent.OnSwitchAccountSuccess += new System.Action(this.OnSwitchAccount);
        this.accountAgent.OnBindAccountCallBack += new System.Action<bool>(this.OnBindAccountCallBack);
        this.accountAgent.OnBindAccountFail += new System.Action<string>(this.OnBindAccountFail);
        this.accountAgent.OnUnbindAccountFail += new System.Action<string>(this.OnUnBindAccountFail);
        this.accountAgent.OnUnbindAccountSuccessDelegate += new System.Action(this.OnUnBindAccountSuccessDelegate);
        this.accountAgent.OnInitCompleteCallBack = new System.Action<bool>(this.OnAccountInstallFinish);
      }
      if (this.fbAgent == null)
        this.fbAgent = new FunplusFacebookAgent();
      FunplusFacebook.Instance.SetDelegate((FunplusFacebook.IDelegate) this.fbAgent);
      FunplusAccount.Instance.SetDelegate((FunplusAccount.IDelegate) this.accountAgent);
      FunplusAccount.Instance.OpenSession();
    }
  }

  public void SetGMContact(string value)
  {
    if (string.IsNullOrEmpty(value))
      return;
    value = value.ToLower();
    this._isShowContact = value == "true";
  }

  private void OnFirstLoginFail(string msg)
  {
    if (!NetWorkDetector.Instance.IsReadyRestart())
      NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.SDK, "LoginFail", msg);
    NetWorkDetector.Instance.RestartOrQuitGame(ScriptLocalization.Get("network_error_login_fail_description", true), ScriptLocalization.Get("network_error_title", true), (string) null);
  }

  private void OnUnBindAccountSuccessDelegate()
  {
    string str = string.Empty;
    if (this.accounts.ContainsKey(this.unBindType))
    {
      str = this.accounts[this.unBindType].AccountID;
      this.accounts.Remove(this.unBindType);
    }
    string normalType = this.GetNormalType(this.unBindType);
    PlayerPrefs.DeleteKey(normalType + "_" + this.accountAgent.FunplusID.ToString() + "_accountName");
    PlayerPrefs.DeleteKey(normalType + "_" + this.accountAgent.FunplusID.ToString() + "_accountEmail");
    if (!string.IsNullOrEmpty(str))
      RequestManager.inst.SendRequest("player:unbindAccount", new Hashtable()
      {
        {
          (object) "account_name",
          (object) str
        }
      }, (System.Action<bool, object>) null, true);
    if (this.OnUnBindAccountSuccess == null)
      return;
    this.OnUnBindAccountSuccess();
  }

  private void OnBindAccountFail(string msg)
  {
    Utils.ShowWarning(msg, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) null, ScriptLocalization.Get("id_uppercase_notice", true));
  }

  private void OnUnBindAccountFail(string msg)
  {
    Utils.ShowWarning(msg, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) null, (string) null);
  }

  private void OnAccountInstallFinish(bool success)
  {
    this.init = success;
    if (success && !string.IsNullOrEmpty(this.accountAgent.SNS) && this.accountAgent.AccountType == FunplusAccountType.FPAccountTypeFacebook)
    {
      if (string.IsNullOrEmpty(this.accountAgent.SNSName))
      {
        this.needToBind = false;
        AccountInfo accountInfo = new AccountInfo();
        accountInfo.AccountType = FunplusAccountType.FPAccountTypeFacebook;
        accountInfo.CTime = NetServerTime.inst.ServerTimestamp;
        if (!this.accounts.ContainsKey(accountInfo.AccountType))
          this.accounts.Add(accountInfo.AccountType, accountInfo);
        else
          this.accounts[accountInfo.AccountType] = accountInfo;
        this.fbAgent.OnFetchUserDataSuccess += new System.Action(this.OnGetUserDataSuccess);
        this.GetFaceBookUserData();
      }
      else
        this.CacheAccount(this.GetAccountInfo());
    }
    else if (this.accountAgent.AccountType == FunplusAccountType.FPAccountTypeGooglePlus || this.accountAgent.AccountType == FunplusAccountType.FPAccountTypeGameCenter || (this.accountAgent.AccountType == FunplusAccountType.FPAccountTypeVK || this.accountAgent.AccountType == FunplusAccountType.FPAccountTypeWechat))
      this.CacheAccount(this.GetAccountInfo());
    if (this.OnInitCompleteCallBack == null)
      return;
    this.OnInitCompleteCallBack(success);
  }

  public void Dispose()
  {
  }

  public void CheckAccountType()
  {
    if (CustomDefine.IsSQWanPackage() || Application.isEditor)
      return;
    if (this.accountAgent == null)
    {
      D.error((object) "[Account] accountAgent is not init!");
    }
    else
    {
      if (!this.CanBindType())
        return;
      if (this.IsBind(this.accountAgent.AccountType))
      {
        if (this.accounts[this.accountAgent.AccountType].IsSyn)
          return;
        this.SaveCurrentAccount();
      }
      else
      {
        if (this.IsBind(this.accountAgent.AccountType))
          return;
        this.SaveCurrentAccount();
      }
    }
  }

  private bool CanBindType()
  {
    if (this.accountAgent.AccountType != FunplusAccountType.FPAccountTypeGooglePlus && this.accountAgent.AccountType != FunplusAccountType.FPAccountTypeFacebook && (this.accountAgent.AccountType != FunplusAccountType.FPAccountTypeGameCenter && this.accountAgent.AccountType != FunplusAccountType.FPAccountTypeVK))
      return this.accountAgent.AccountType == FunplusAccountType.FPAccountTypeWechat;
    return true;
  }

  public int KingdomID { set; get; }

  private void OnSwitchAccount()
  {
    RequestManager.inst.SendRequest("player:unSubTopics", (Hashtable) null, (System.Action<bool, object>) null, true);
    GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Deep);
    LocalizationManager.CurrentLanguageCode = Language.Instance.GetLocalizationLanguage();
    LoginErrDialog.LoginErrLog("Call MarkRestartGame");
  }

  private void OnBindAccountCallBack(bool success)
  {
    if (!GameEngine.IsReady())
      D.error((object) "[Account] GameEngine is not ready!");
    else if (this.accountAgent == null)
    {
      D.error((object) "[Account] accountAgent is not init!");
    }
    else
    {
      if (success)
      {
        string Term = "toast_account_bind_success";
        if (this.IsBind(this.accountAgent.AccountType))
          Term = "toast_account_switch_binding_success";
        string content = ScriptLocalization.Get(Term, true);
        if ((UnityEngine.Object) UIManager.inst != (UnityEngine.Object) null && (UnityEngine.Object) UIManager.inst.toast != (UnityEngine.Object) null)
          UIManager.inst.toast.Show(content, (System.Action) null, 4f, false);
      }
      if (!success)
        return;
      this.SaveCurrentAccount();
    }
  }

  private void SaveCurrentAccount()
  {
    if (this.accountAgent == null)
    {
      D.error((object) "[Account] accountAgent is not init!");
    }
    else
    {
      switch (this.accountAgent.AccountType)
      {
        case FunplusAccountType.FPAccountTypeFacebook:
          this.needToBind = true;
          AccountInfo accountInfo1 = new AccountInfo();
          accountInfo1.AccountType = FunplusAccountType.FPAccountTypeFacebook;
          accountInfo1.CTime = NetServerTime.inst.ServerTimestamp;
          if (!this.accounts.ContainsKey(accountInfo1.AccountType))
            this.accounts.Add(accountInfo1.AccountType, accountInfo1);
          else
            this.accounts[accountInfo1.AccountType] = accountInfo1;
          this.fbAgent.OnFetchUserDataSuccess += new System.Action(this.OnGetUserDataSuccess);
          this.GetFaceBookUserData();
          break;
        case FunplusAccountType.FPAccountTypeVK:
        case FunplusAccountType.FPAccountTypeWechat:
        case FunplusAccountType.FPAccountTypeGooglePlus:
        case FunplusAccountType.FPAccountTypeGameCenter:
          AccountInfo accountInfo2 = this.GetAccountInfo();
          accountInfo2.IsSyn = true;
          accountInfo2.CTime = NetServerTime.inst.ServerTimestamp;
          this.CacheAccount(accountInfo2);
          RequestManager.inst.SendRequest("player:bindAccount", Utils.Hash((object) "account_name", (object) accountInfo2.AccountID, (object) "account_type", (object) accountInfo2.Type), (System.Action<bool, object>) null, true);
          if (this.OnBindAccountSuccess == null)
            break;
          this.OnBindAccountSuccess();
          break;
      }
    }
  }

  private AccountInfo GetAccountInfo()
  {
    return new AccountInfo()
    {
      AccountID = this.accountAgent.SNS,
      AccountEmail = this.accountAgent.Email,
      AccountName = this.accountAgent.SNSName,
      AccountType = this.accountAgent.AccountType,
      Type = this.GetNormalType(this.accountAgent.AccountType)
    };
  }

  public string GetAccountName(FunplusAccountType AccountType)
  {
    string accountName = ScriptLocalization.Get("account_bound_no_access_user_info", true);
    if (this.accounts.ContainsKey(AccountType))
    {
      AccountInfo account = this.accounts[AccountType];
      if (!string.IsNullOrEmpty(account.AccountName))
        accountName = account.AccountName;
    }
    return accountName;
  }

  private string GetNormalType(FunplusAccountType AccountType)
  {
    string str = string.Empty;
    switch (AccountType)
    {
      case FunplusAccountType.FPAccountTypeEmail:
        str = "email";
        break;
      case FunplusAccountType.FPAccountTypeFacebook:
        str = "facebook";
        break;
      case FunplusAccountType.FPAccountTypeVK:
        str = "vk";
        break;
      case FunplusAccountType.FPAccountTypeWechat:
        str = "weChat";
        break;
      case FunplusAccountType.FPAccountTypeGooglePlus:
        str = "googlePlus";
        break;
      case FunplusAccountType.FPAccountTypeGameCenter:
        str = "gameCenter";
        break;
    }
    return str;
  }

  public bool DelayShowBind
  {
    get
    {
      return this._delayShow;
    }
    set
    {
      this._delayShow = value;
    }
  }

  public void ShowBindPop()
  {
    if (CustomDefine.IsSQWanPackage() || Application.isEditor || (!this.IsNotBindAccount || this.DelayShowBind))
      return;
    UIManager.inst.OpenPopup("BindAccountPopup", (Popup.PopupParameter) null);
  }

  public void LogUserInfo()
  {
    RequestManager.inst.SendLoader("Player:getPaymentLevel", Utils.Hash((object) "uid", (object) PlayerData.inst.uid), new System.Action<bool, object>(this.GetLevel), true, false);
    if (this.NeedPathCheck)
    {
      RequestManager.inst.SendLoader("player:patch", (Hashtable) null, (System.Action<bool, object>) null, true, false);
      this.NeedPathCheck = false;
    }
    RequestManager.inst.SendLoader("UserIdentity:isRequired", (Hashtable) null, new System.Action<bool, object>(this.GetIdentity), true, false);
    if (this.sdkAgent == null || string.IsNullOrEmpty(this.sdkAgent.PushToken))
      return;
    RequestManager.inst.SendLoader("Player:setPushToken", Utils.Hash((object) "push_token", (object) this.sdkAgent.PushToken), (System.Action<bool, object>) null, true, false);
  }

  private void GetIdentity(bool success, object result)
  {
    string str = "0";
    if (!success)
      return;
    Hashtable hashtable = result as Hashtable;
    if (hashtable == null)
      return;
    if (hashtable.ContainsKey((object) "required") && hashtable[(object) "required"] != null)
      str = hashtable[(object) "required"].ToString();
    this._isVerification = str == "0";
  }

  private void GetLevel(bool success, object result)
  {
    string userVipLevel = "0";
    if (success)
    {
      Hashtable hashtable = result as Hashtable;
      if (hashtable != null && hashtable.ContainsKey((object) "level") && hashtable[(object) "level"] != null)
        userVipLevel = hashtable[(object) "level"].ToString();
    }
    if (Application.isEditor)
      return;
    DBManager.inst.DB_hero.Get(PlayerData.inst.hostPlayer.uid);
    FunplusSdk.Instance.LogUserInfoUpdate(PlayerData.inst.playerCityData.cityLocation.K.ToString(), PlayerData.inst.hostPlayer.uid.ToString(), DBManager.inst.DB_User.Get(PlayerData.inst.hostPlayer.uid).userName, CityManager.inst.mStronghold.mLevel.ToString(), userVipLevel, PaymentManager.Instance.IsPaidUser, string.Empty);
  }

  private int IsCanRecommend
  {
    get
    {
      int num = 0;
      StatsData statsData = DBManager.inst.DB_Stats.Get("is_commented");
      if (statsData != null && statsData.count > 0L)
        return num;
      for (int index = 0; index < this.recommendLevels.Length; ++index)
      {
        if (PlayerData.inst.CityData.mStronghold.mLevel == this.recommendLevels[index])
        {
          num = this.recommendLevels[index];
          break;
        }
      }
      if (num > 0 && PlayerPrefsEx.GetIntByUid("recommend_" + (object) num, 0) > 0)
        num = 0;
      return num;
    }
  }

  public void ShowRecommendReview()
  {
    if (!GameEngine.IsReady() || GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.CityMode || CustomDefine.IsSpecialChannel())
      return;
    int isCanRecommend = this.IsCanRecommend;
    if (isCanRecommend <= 0)
      return;
    UIManager.inst.priorityPopupQueue.OpenPopup("RecommendPopUp", (Popup.PopupParameter) new RecommendPopUp.Parameter()
    {
      level = isCanRecommend
    }, -1);
  }

  public void ShowFASQs()
  {
    FunplusHelpshift.Instance.ShowFAQs();
  }

  public void ShowConversation()
  {
    FunplusHelpshift.Instance.ShowConversation();
  }

  public string FunplusID
  {
    get
    {
      if (this.accountAgent == null)
        return string.Empty;
      return this.accountAgent.FunplusID;
    }
  }

  public string SessionKey
  {
    get
    {
      if (this.accountAgent == null)
        return string.Empty;
      return this.accountAgent.SessionKey;
    }
  }

  public void ShowUserCenter()
  {
    if (this.accountAgent == null || !this.accountAgent.IsLoggedIn)
      return;
    FunplusAccount.Instance.ShowUserCenter();
  }

  public void Login()
  {
    if (this.accountAgent.IsLoggedIn)
      return;
    this.Login(FunplusAccountType.FPAccountTypeExpress);
  }

  private void LoginAccount()
  {
    this.time = Time.time;
    if (!this.accountAgent.IsLoggedIn)
    {
      this.Login(FunplusAccountType.FPAccountTypeExpress);
    }
    else
    {
      if (this.accountAgent == null || !this.accountAgent.IsLoggedIn)
        return;
      this.OnAccountInstallFinish(true);
    }
  }

  public void Login(FunplusAccountType type)
  {
    FunplusAccount.Instance.Login(type);
  }

  public void LogoutNow()
  {
    if (Application.isEditor)
      return;
    if (!NetWorkDetector.Instance.IsReadyRestart())
      NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.Logout_Start, "logout", "LogOut Start");
    this.accountAgent.OnLogoutDelegate += new System.Action(this.OnLogoutFinish);
    FunplusAccount.Instance.Logout();
  }

  private void OnLogoutFinish()
  {
    if (!NetWorkDetector.Instance.IsReadyRestart())
      NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.Logout_Finish, "logout", "LogOut Finish");
    this.accountAgent.OnLogoutDelegate -= new System.Action(this.OnLogoutFinish);
    GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Deep);
  }

  private void Logout()
  {
    this.accountAgent.OnLoginAccountFail += new System.Action<string>(this.OnLoginErrorHandler);
    FunplusAccount.Instance.Logout();
  }

  private void OnLoginErrorHandler(string errorMessage)
  {
    this.accountAgent.OnLoginAccountFail -= new System.Action<string>(this.OnLoginErrorHandler);
    this.Login();
  }

  public void SwitchAccount(FunplusAccountType type)
  {
    if (Application.isEditor)
      return;
    if (type == FunplusAccountType.FPAccountTypeGameCenter && this.accountAgent.AccountType == FunplusAccountType.FPAccountTypeGameCenter)
    {
      string empty = string.Empty;
      if (string.IsNullOrEmpty(empty))
        empty = ScriptLocalization.Get("account_warning_title", true);
      string content = ScriptLocalization.Get("account_gamecenter_switch_account_description", true);
      string left = ScriptLocalization.Get("id_uppercase_confirm", true);
      string right = ScriptLocalization.Get("id_uppercase_cancel", true);
      UIManager.inst.ShowConfirmationBox(empty, content, left, right, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) (() => FunplusAccount.Instance.SwitchAccount(type)), (System.Action) null, (System.Action) null);
    }
    else if (type == FunplusAccountType.FPAccountTypeWechat && this.accountAgent.AccountType == FunplusAccountType.FPAccountTypeWechat)
    {
      string empty = string.Empty;
      if (string.IsNullOrEmpty(empty))
        empty = ScriptLocalization.Get("account_warning_title", true);
      string content = ScriptLocalization.Get("account_wechat_switch_account_description", true);
      string left = ScriptLocalization.Get("id_uppercase_confirm", true);
      string right = ScriptLocalization.Get("id_uppercase_cancel", true);
      UIManager.inst.ShowConfirmationBox(empty, content, left, right, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) (() => FunplusAccount.Instance.SwitchAccount(type)), (System.Action) null, (System.Action) null);
    }
    else
      FunplusAccount.Instance.SwitchAccount(type);
  }

  public void Login(string email, string password)
  {
    if (Application.isEditor)
      return;
    this.Logout();
    FunplusAccount.Instance.LoginWithEmail(email, password);
  }

  public void Signup(string email, string password)
  {
    this.Logout();
  }

  public void Signup()
  {
    this.Logout();
    FunplusAccount.Instance.CreateNewExpressAccount();
  }

  private void OnLogoutHandler()
  {
    FunplusAccount.Instance.CreateNewExpressAccount();
  }

  public void UnBindAccount(FunplusAccountType type)
  {
    if (Application.isEditor)
      return;
    AccountInfo account = this.accounts[type];
    if (account == null)
      return;
    this.unBindType = type;
    FunplusAccount.Instance.UnbindAccount(type, account.AccountID);
  }

  public void ForgotPW(string email)
  {
    if (Application.isEditor)
      return;
    FunplusAccount.Instance.ResetPassword(email);
  }

  public void BindAccount(FunplusAccountType type)
  {
    if (Application.isEditor)
      return;
    FunplusAccount.Instance.BindAccount(type);
  }

  public string AccountEmail
  {
    get
    {
      return this.accountAgent.Email;
    }
  }

  public bool IsNotBindAccount
  {
    get
    {
      return this.accounts.Count <= 0;
    }
  }

  public bool IsBind(FunplusAccountType type)
  {
    return this.accounts.ContainsKey(type);
  }

  public bool HadBindInfo()
  {
    return this.accounts.Count > 0;
  }

  public bool IsLastBind(FunplusAccountType type)
  {
    if (CustomDefine.IsAccountUnbindUnLock() || this.ReplaceLock())
      return false;
    AccountInfo account = this.accounts[type];
    return this.accounts.Count == 1;
  }

  public string GetTypeName(FunplusAccountType type)
  {
    switch (type)
    {
      case FunplusAccountType.FPAccountTypeFacebook:
        return ScriptLocalization.Get("account_facebook", true);
      case FunplusAccountType.FPAccountTypeVK:
        return ScriptLocalization.Get("account_vk", true);
      case FunplusAccountType.FPAccountTypeWechat:
        return ScriptLocalization.Get("account_wechat", true);
      case FunplusAccountType.FPAccountTypeGooglePlus:
        return ScriptLocalization.Get("account_googleplay", true);
      case FunplusAccountType.FPAccountTypeGameCenter:
        return ScriptLocalization.Get("account_gamecenter", true);
      default:
        return string.Empty;
    }
  }

  public string GetIcon(FunplusAccountType type)
  {
    string str = "icon_accout";
    switch (type)
    {
      case FunplusAccountType.FPAccountTypeFacebook:
        str += "_facebook";
        break;
      case FunplusAccountType.FPAccountTypeVK:
        str = "icon_account_vk";
        break;
      case FunplusAccountType.FPAccountTypeWechat:
        str += "_wechat";
        break;
      case FunplusAccountType.FPAccountTypeGooglePlus:
        str += "_google";
        break;
      case FunplusAccountType.FPAccountTypeGameCenter:
        str = "icon_account_game_center";
        break;
    }
    return str;
  }

  public void SetCertifyingRequired(string value)
  {
    value = value.Trim();
    this._isShowCertifying = value == "1";
    this._oldSkipFlag = NewTutorial.skipTutorial;
    if (!this._isShowCertifying)
      return;
    NewTutorial.skipTutorial = true;
  }

  public bool NeedToShowCertifying()
  {
    return this._isShowCertifying;
  }

  public void LockShowCertifyingFlag()
  {
    this._isShowCertifying = false;
    NewTutorial.skipTutorial = this._oldSkipFlag;
  }

  public void ShowCertifying()
  {
    if (!this._isShowCertifying)
      return;
    UIManager.inst.OpenPopup("VerificationPopup", (Popup.PopupParameter) null);
  }
}
