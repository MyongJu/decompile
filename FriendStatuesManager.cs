﻿// Decompiled with JetBrains decompiler
// Type: FriendStatuesManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class FriendStatuesManager
{
  private bool isInited;
  public System.Action onOnlineFriendStatuesChanged;
  public Dictionary<long, bool> friendStatues;
  private HubPort onlinePort;
  private HubPort offlinePort;
  public System.Action onEndFreshOnlineStatues;

  public int onlineFriendCount { get; private set; }

  public void Init(object orgData)
  {
    if (this.isInited)
      return;
    MessageHub.inst.GetPortByAction("online").AddEvent(new System.Action<object>(this.OnFriendOnlineRecived));
    MessageHub.inst.GetPortByAction("bye").AddEvent(new System.Action<object>(this.OnFriendOfflineRecived));
    DBManager.inst.DB_Contacts.onDataRemove += new System.Action<long>(this.OnFriendRemoved);
    this.friendStatues = new Dictionary<long, bool>();
    this.onlineFriendCount = 0;
    this.UpdateOnlineStatues(orgData);
    this.isInited = true;
  }

  public bool IsUserOnline(long uid)
  {
    if (this.friendStatues.ContainsKey(uid))
      return this.friendStatues[uid];
    return false;
  }

  public void FreshOnlineStatues()
  {
    MessageHub.inst.GetPortByAction("Player:getFriendsOnlineStatus").SendRequest((Hashtable) null, new System.Action<bool, object>(this.FreshOnlineStatuesCallback), true);
  }

  public void FreshOnlineStatuesCallback(bool result, object orgdata)
  {
    if (!result || this.onEndFreshOnlineStatues == null)
      return;
    this.onEndFreshOnlineStatues();
  }

  public void Logoff()
  {
  }

  private void Publish_onlineFriendsStatuesChanged()
  {
    if (this.onOnlineFriendStatuesChanged == null)
      return;
    this.onOnlineFriendStatuesChanged();
  }

  public void OnFriendOnline(long onlineFriendUid)
  {
    if (onlineFriendUid == 0L)
      return;
    if (!this.friendStatues.ContainsKey(onlineFriendUid))
    {
      this.friendStatues.Add(onlineFriendUid, true);
      ++this.onlineFriendCount;
      this.Publish_onlineFriendsStatuesChanged();
    }
    if (this.friendStatues[onlineFriendUid])
      return;
    this.friendStatues[onlineFriendUid] = true;
    ++this.onlineFriendCount;
    this.Publish_onlineFriendsStatuesChanged();
  }

  public void OnFriendOffline(long offlineFriendUid)
  {
    if (offlineFriendUid == 0L)
      return;
    if (!this.friendStatues.ContainsKey(offlineFriendUid))
    {
      this.friendStatues.Add(offlineFriendUid, false);
      --this.onlineFriendCount;
      this.Publish_onlineFriendsStatuesChanged();
    }
    if (!this.friendStatues[offlineFriendUid])
      return;
    this.friendStatues[offlineFriendUid] = false;
    --this.onlineFriendCount;
    this.Publish_onlineFriendsStatuesChanged();
  }

  private void UpdateOnlineStatues(object orgdata)
  {
    Hashtable data;
    if (DatabaseTools.CheckAndParseOrgData(orgdata, out data))
    {
      foreach (object key1 in (IEnumerable) data.Keys)
      {
        long key2 = long.Parse(key1.ToString());
        bool flag = bool.Parse(data[key1].ToString());
        if (this.friendStatues.ContainsKey(key2))
          this.friendStatues[key2] = flag;
        else
          this.friendStatues.Add(key2, flag);
      }
    }
    int num = 0;
    using (Dictionary<long, bool>.KeyCollection.Enumerator enumerator = this.friendStatues.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (this.friendStatues[enumerator.Current])
          ++num;
      }
    }
    if (num == this.onlineFriendCount)
      return;
    this.onlineFriendCount = num;
    this.Publish_onlineFriendsStatuesChanged();
  }

  private void OnFriendOnlineRecived(object orgdata)
  {
    this.OnFriendOnline(this.CheckUserId(orgdata));
  }

  private void OnFriendOfflineRecived(object orgdata)
  {
    this.OnFriendOffline(this.CheckUserId(orgdata));
  }

  private void OnFriendRemoved(long uid)
  {
    if (!this.friendStatues.ContainsKey(uid))
      return;
    this.onlineFriendCount -= !this.friendStatues[uid] ? 0 : 1;
    this.friendStatues.Remove(uid);
  }

  private long CheckUserId(object orgdata)
  {
    Hashtable data;
    long result;
    if (!DatabaseTools.CheckAndParseOrgData(orgdata, out data) || !long.TryParse(data[(object) "uid"].ToString(), out result))
      return 0;
    return result;
  }
}
