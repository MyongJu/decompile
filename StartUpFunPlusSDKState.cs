﻿// Decompiled with JetBrains decompiler
// Type: StartUpFunPlusSDKState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StartUpFunPlusSDKState : LoadBaseState
{
  public StartUpFunPlusSDKState(int step)
    : base(step)
  {
    this.NeedLoad = false;
  }

  public override string Key
  {
    get
    {
      return nameof (StartUpFunPlusSDKState);
    }
  }

  protected override SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.SetupSDK;
    }
  }

  protected override void Prepare()
  {
    this.RefreshProgress();
    if (Application.isEditor)
      this.Finish();
    else
      AccountManager.Instance.Initialize(new System.Action<bool>(this.OnInitCallBack));
  }

  private void OnInitCallBack(bool isLogin)
  {
    if (this.IsDestroy)
      return;
    if (isLogin)
    {
      D.UserId = AccountManager.Instance.FunplusID;
      PaymentManager.Instance.Initialize();
      AdjustProxy.Instance.SendOpenEvent();
      this.Finish();
    }
    else
      AccountManager.Instance.Login();
  }

  protected override void Finish()
  {
    if (this.IsDestroy)
      return;
    base.Finish();
    NativeManager.inst.GetGaid();
    this.Preloader.OnStartUpFunPlusSDKFinish();
  }
}
