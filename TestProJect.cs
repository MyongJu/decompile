﻿// Decompiled with JetBrains decompiler
// Type: TestProJect
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TestProJect : MonoBehaviour
{
  private void Start()
  {
  }

  private void Update()
  {
  }

  private void OnAnimatorMove()
  {
    Animator component = this.GetComponent<Animator>();
    if (!(bool) ((Object) component))
      return;
    Vector3 position = this.transform.position;
    position.z += component.GetFloat("Runspeed") * Time.deltaTime;
    this.transform.position = position;
  }
}
