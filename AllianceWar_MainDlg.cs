﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceWar_MainDlg : UI.Dialog
{
  private Dictionary<long, int> fortressDemolishinges = new Dictionary<long, int>();
  private AllianceWar_MainDlg.Data _data = new AllianceWar_MainDlg.Data();
  private int templeDemolishingeCount;
  [SerializeField]
  private AllianceWar_MainDlg.Panel panel;

  public int magicCount
  {
    set
    {
      this.panel.magicNotifyContainer.gameObject.SetActive(false);
    }
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    if (orgParam != null)
    {
      AllianceWar_MainDlg.Parameter parameter = orgParam as AllianceWar_MainDlg.Parameter;
      if (parameter != null)
        this._data.selectType = parameter.selectType;
    }
    else
      this._data.selectType = AllianceWar_MainDlg.Parameter.SelectType.war;
    this.panel.viewPort.sourceCamera = UIManager.inst.ui2DCamera;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    PlayerData.inst.allianceWarManager.loadDatasCallback += new System.Action(this.FreshPanel);
    DBManager.inst.DB_AllianceMagic.onDataChanged += new System.Action<AllianceMagicData>(this.OnMagicChanged);
    if (PlayerData.inst.allianceWarManager.LoadDatas())
      return;
    this.FreshPanel();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    PlayerData.inst.allianceWarManager.loadDatasCallback -= new System.Action(this.FreshPanel);
    DBManager.inst.DB_AllianceMagic.onDataChanged -= new System.Action<AllianceMagicData>(this.OnMagicChanged);
  }

  public void FreshPanel()
  {
    if (!this.gameObject.activeSelf)
      return;
    this.magicCount = DBManager.inst.DB_AllianceMagic.GetAllianceWarMagicList().Count;
    switch (this._data.selectType)
    {
      case AllianceWar_MainDlg.Parameter.SelectType.war:
        this.SelectWar();
        break;
      case AllianceWar_MainDlg.Parameter.SelectType.knight:
        this.SelectEvent();
        break;
      case AllianceWar_MainDlg.Parameter.SelectType.magic:
        this.SelectSkill();
        break;
    }
    this.UpdateNotifyInfo();
  }

  private void UpdateNotifyInfo()
  {
    if ((UnityEngine.Object) this.panel.warNotifyCount != (UnityEngine.Object) null)
    {
      int warNotifyCount = this.GetWarNotifyCount();
      this.panel.warNotifyCount.text = warNotifyCount.ToString();
      NGUITools.SetActive(this.panel.warNotifyCount.transform.parent.gameObject, warNotifyCount > 0);
    }
    if ((UnityEngine.Object) this.panel.magicNotifyCount != (UnityEngine.Object) null)
    {
      int magicNotifyCount = this.GetMagicNotifyCount();
      this.panel.magicNotifyCount.text = magicNotifyCount.ToString();
      NGUITools.SetActive(this.panel.magicNotifyCount.transform.parent.gameObject, magicNotifyCount > 0);
    }
    if (!((UnityEngine.Object) this.panel.eventNotifyCount != (UnityEngine.Object) null))
      return;
    int eventNotifyCount = this.GetEventNotifyCount();
    this.panel.eventNotifyCount.text = eventNotifyCount.ToString();
    NGUITools.SetActive(this.panel.eventNotifyCount.transform.parent.gameObject, eventNotifyCount > 0);
  }

  private int GetWarNotifyCount()
  {
    if ((UnityEngine.Object) this.gameObject == (UnityEngine.Object) null)
      return 0;
    this.fortressDemolishinges.Clear();
    List<MarchData> marchDataList = new List<MarchData>();
    List<RallyData> rallyDataList = new List<RallyData>();
    List<long> warMarchs = PlayerData.inst.allianceWarManager.warMarchs;
    for (int index = 0; index < warMarchs.Count; ++index)
    {
      MarchData marchData = DBManager.inst.DB_March.Get(warMarchs[index]);
      if (marchData != null && !marchData.IsReturn)
      {
        if (marchData.state == MarchData.MarchState.demolishing_fortress)
        {
          if (marchData.type == MarchData.MarchType.fortress_attack)
          {
            AllianceFortressData dataByCoordinate = DBManager.inst.DB_AllianceFortress.GetDataByCoordinate(marchData.targetLocation);
            if (this.fortressDemolishinges.ContainsKey(dataByCoordinate.fortressId))
            {
              this.fortressDemolishinges[dataByCoordinate.fortressId] = this.fortressDemolishinges[dataByCoordinate.fortressId] + 1;
            }
            else
            {
              this.fortressDemolishinges.Add(dataByCoordinate.fortressId, 1);
              marchDataList.Add(DBManager.inst.DB_March.Get(marchData.marchId));
            }
          }
          else if (marchData.type == MarchData.MarchType.temple_attack)
            marchDataList.Add(DBManager.inst.DB_March.Get(marchData.marchId));
        }
        else
          marchDataList.Add(DBManager.inst.DB_March.Get(warMarchs[index]));
      }
    }
    Dictionary<long, AllianceWarManager.WarData>.KeyCollection.Enumerator enumerator = PlayerData.inst.allianceWarManager.warRallys.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      RallyData rallyData = DBManager.inst.DB_Rally.Get(enumerator.Current);
      if (rallyData != null && GameEngine.Instance.marchSystem.allianceWarCount > 0)
        rallyDataList.Add(rallyData);
    }
    int index1 = 0;
    int index2 = 0;
    int num = 0;
    while (index1 < marchDataList.Count && index2 < rallyDataList.Count)
    {
      if (marchDataList[index1].startTime < rallyDataList[index2].waitTimeDuration.startTime)
      {
        ++num;
        ++index1;
      }
      else
      {
        ++num;
        ++index2;
      }
    }
    for (; index1 < marchDataList.Count; ++index1)
      ++num;
    for (; index2 < rallyDataList.Count; ++index2)
      ++num;
    return num;
  }

  private int GetEventNotifyCount()
  {
    return PlayerData.inst.allianceWarManager.knightMarches.Count;
  }

  private int GetMagicNotifyCount()
  {
    List<AllianceMagicData> allianceWarMagicList = DBManager.inst.DB_AllianceMagic.GetAllianceWarMagicList();
    if (allianceWarMagicList != null && allianceWarMagicList.Count > 0)
      return allianceWarMagicList.Count;
    return 0;
  }

  private void SelectWar()
  {
    this.panel.Tag_Event.gameObject.SetActive(false);
    this.panel.Tag_Combat.gameObject.SetActive(true);
    this.panel.Tag_Skill.gameObject.SetActive(false);
    this.panel.BT_WarLog.gameObject.SetActive(true);
    this.panel.BT_MagicLog.gameObject.SetActive(false);
    this.panel.NoEventConatiner.gameObject.SetActive(false);
    this.panel.NoSkillContainer.gameObject.SetActive(false);
    if (PlayerData.inst.allianceId == 0L)
    {
      this.panel.JoinAllianceContainer.gameObject.SetActive(true);
      this.panel.mainPanel.gameObject.SetActive(false);
    }
    else
    {
      this.panel.JoinAllianceContainer.gameObject.SetActive(false);
      if (!this.panel.mainPanel.gameObject.activeSelf)
        this.panel.mainPanel.gameObject.SetActive(true);
    }
    if ((UnityEngine.Object) this.gameObject == (UnityEngine.Object) null)
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    for (int index = 0; index < this.panel.slots.Count; ++index)
    {
      this.panel.slots[index].gameObject.SetActive(false);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.panel.slots[index].gameObject);
    }
    this.panel.slots.Clear();
    this.fortressDemolishinges.Clear();
    this.templeDemolishingeCount = 0;
    List<MarchData> marchDataList = new List<MarchData>();
    List<RallyData> rallyDataList = new List<RallyData>();
    List<long> warMarchs = PlayerData.inst.allianceWarManager.warMarchs;
    for (int index = 0; index < warMarchs.Count; ++index)
    {
      MarchData marchData = DBManager.inst.DB_March.Get(warMarchs[index]);
      if (marchData != null && !marchData.IsReturn)
      {
        if (marchData.state == MarchData.MarchState.demolishing_fortress)
        {
          if (marchData.type == MarchData.MarchType.fortress_attack)
          {
            AllianceFortressData dataByCoordinate = DBManager.inst.DB_AllianceFortress.GetDataByCoordinate(marchData.targetLocation);
            if (dataByCoordinate != null)
            {
              if (this.fortressDemolishinges.ContainsKey(dataByCoordinate.fortressId))
              {
                this.fortressDemolishinges[dataByCoordinate.fortressId] = this.fortressDemolishinges[dataByCoordinate.fortressId] + 1;
              }
              else
              {
                this.fortressDemolishinges.Add(dataByCoordinate.fortressId, 1);
                marchDataList.Add(DBManager.inst.DB_March.Get(marchData.marchId));
              }
            }
          }
          else if (marchData.type == MarchData.MarchType.temple_attack)
          {
            ++this.templeDemolishingeCount;
            marchDataList.Add(DBManager.inst.DB_March.Get(marchData.marchId));
          }
        }
        else
          marchDataList.Add(DBManager.inst.DB_March.Get(warMarchs[index]));
      }
    }
    Dictionary<long, AllianceWarManager.WarData>.KeyCollection.Enumerator enumerator = PlayerData.inst.allianceWarManager.warRallys.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      RallyData rallyData = DBManager.inst.DB_Rally.Get(enumerator.Current);
      if (rallyData != null && GameEngine.Instance.marchSystem.allianceWarCount > 0)
        rallyDataList.Add(rallyData);
    }
    marchDataList.Sort((Comparison<MarchData>) ((a, b) => a.startTime.CompareTo(b.startTime)));
    rallyDataList.Sort((Comparison<RallyData>) ((a, b) => a.waitTimeDuration.startTime.CompareTo(b.waitTimeDuration.startTime)));
    this.panel.NoWarContainer.SetActive(marchDataList.Count + rallyDataList.Count == 0);
    int index1 = 0;
    int index2 = 0;
    while (index1 < marchDataList.Count && index2 < rallyDataList.Count)
    {
      if (marchDataList[index1].startTime < rallyDataList[index2].waitTimeDuration.startTime)
      {
        this.AddSlot(marchDataList[index1], index1 + index2);
        ++index1;
      }
      else
      {
        this.AddSlot(rallyDataList[index2], index1 + index2);
        ++index2;
      }
    }
    for (; index1 < marchDataList.Count; ++index1)
      this.AddSlot(marchDataList[index1], index1 + index2);
    for (; index2 < rallyDataList.Count; ++index2)
      this.AddSlot(rallyDataList[index2], index1 + index2);
    this.panel.list.repositionNow = true;
    this.panel.scrollView.restrictWithinPanel = true;
  }

  private void SelectEvent()
  {
    this.panel.Tag_Event.gameObject.SetActive(true);
    this.panel.Tag_Combat.gameObject.SetActive(false);
    this.panel.Tag_Skill.gameObject.SetActive(false);
    this.panel.BT_WarLog.gameObject.SetActive(true);
    this.panel.BT_MagicLog.gameObject.SetActive(false);
    this.panel.NoWarContainer.gameObject.SetActive(false);
    this.panel.NoSkillContainer.gameObject.SetActive(false);
    for (int index = 0; index < this.panel.slots.Count; ++index)
    {
      this.panel.slots[index].gameObject.SetActive(false);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.panel.slots[index].gameObject);
    }
    this.panel.slots.Clear();
    for (int index = 0; index < PlayerData.inst.allianceWarManager.knightMarches.Count; ++index)
      this.AddSlot(PlayerData.inst.allianceWarManager.knightMarches[index], index);
    this.panel.NoEventConatiner.gameObject.SetActive(PlayerData.inst.allianceWarManager.knightMarches.Count == 0);
    this.panel.list.repositionNow = true;
    this.panel.scrollView.restrictWithinPanel = true;
  }

  private void SelectSkill()
  {
    this.panel.Tag_Combat.gameObject.SetActive(false);
    this.panel.Tag_Event.gameObject.SetActive(false);
    this.panel.Tag_Skill.gameObject.SetActive(true);
    this.panel.BT_WarLog.gameObject.SetActive(false);
    this.panel.BT_MagicLog.gameObject.SetActive(true);
    this.panel.NoWarContainer.gameObject.SetActive(false);
    this.panel.NoEventConatiner.gameObject.SetActive(false);
    for (int index = 0; index < this.panel.slots.Count; ++index)
    {
      this.panel.slots[index].gameObject.SetActive(false);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.panel.slots[index].gameObject);
    }
    this.panel.slots.Clear();
    List<AllianceMagicData> allianceWarMagicList = DBManager.inst.DB_AllianceMagic.GetAllianceWarMagicList();
    if (allianceWarMagicList != null && allianceWarMagicList.Count > 0)
    {
      this.panel.NoSkillContainer.gameObject.SetActive(false);
      for (int index = 0; index < allianceWarMagicList.Count; ++index)
        this.AddSlot(allianceWarMagicList[index], index);
    }
    else
      this.panel.NoSkillContainer.gameObject.SetActive(true);
    this.panel.list.repositionNow = true;
    this.panel.scrollView.restrictWithinPanel = true;
  }

  private void AddSlot(MarchData march, int index)
  {
    if (march.state == MarchData.MarchState.demolishing_fortress)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[9].gameObject);
      gameObject.name = "Slot_" + (object) (1000 + index);
      gameObject.transform.parent = this.panel.list.transform;
      gameObject.transform.localScale = Vector3.one;
      gameObject.SetActive(true);
      AllianceWar_MainDlg_Slot component = gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
      if (march.type == MarchData.MarchType.fortress_attack)
      {
        AllianceFortressData dataByCoordinate = DBManager.inst.DB_AllianceFortress.GetDataByCoordinate(march.targetLocation);
        component.Setup(march.marchId, (AllianceWar_MainDlg_Slot.SlotParameter) new AllianceWar_MainDlg_Stay_Slot.StaySlotParameter()
        {
          userCount = this.fortressDemolishinges[dataByCoordinate.fortressId]
        });
        this.panel.slots.Add(component);
      }
      else
      {
        if (march.type != MarchData.MarchType.temple_attack)
          return;
        component.Setup(march.marchId, (AllianceWar_MainDlg_Slot.SlotParameter) new AllianceWar_MainDlg_Stay_Slot.StaySlotParameter()
        {
          userCount = this.templeDemolishingeCount
        });
        this.panel.slots.Add(component);
      }
    }
    else
    {
      GameObject gameObject = march.type == MarchData.MarchType.fortress_attack || march.type == MarchData.MarchType.temple_attack ? (march.ownerAllianceId != PlayerData.inst.allianceId ? UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[5].gameObject) : UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[4].gameObject)) : (march.type != MarchData.MarchType.fortress_reinforce || march.ownerAllianceId != PlayerData.inst.allianceId || (march.targetAllianceId != PlayerData.inst.allianceId || march.targetAllianceId == 0L) ? (march.ownerAllianceId != PlayerData.inst.allianceId ? UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[1].gameObject) : UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[0].gameObject)) : UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[6].gameObject));
      gameObject.name = "Slot_" + (object) (1000 + index);
      gameObject.transform.parent = this.panel.list.transform;
      gameObject.transform.localScale = Vector3.one;
      gameObject.SetActive(true);
      AllianceWar_MainDlg_Slot component = gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
      component.Setup(march.marchId);
      this.panel.slots.Add(component);
    }
  }

  private void AddSlot(RallyData rally, int index)
  {
    GameObject gameObject = rally.fortressId == 0 ? (rally.bossId == 0 ? (rally.ownerAllianceId != PlayerData.inst.allianceId ? UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[3].gameObject) : UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[2].gameObject)) : (rally.type != RallyData.RallyType.TYPE_RALLY_RAB ? UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[10].gameObject) : UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[14].gameObject))) : (rally.ownerAllianceId != PlayerData.inst.allianceId ? UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[8].gameObject) : UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[7].gameObject));
    gameObject.name = "Slot_" + (object) (1000 + index);
    gameObject.transform.parent = this.panel.list.transform;
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    AllianceWar_MainDlg_Slot component = gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    component.Setup(rally.rallyId);
    this.panel.slots.Add(component);
  }

  private void AddSlot(AllianceWarManager.KnightWarData knightData, int index)
  {
    if (DBManager.inst.DB_March.Get(knightData.marchId) == null)
      return;
    string para2 = knightData.knightInfo.para_2;
    GameObject gameObject;
    if (para2 != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceWar_MainDlg.\u003C\u003Ef__switch\u0024map1F == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceWar_MainDlg.\u003C\u003Ef__switch\u0024map1F = new Dictionary<string, int>(3)
        {
          {
            "event_fallen_knight_troop",
            0
          },
          {
            "event_fallen_knight_lord",
            1
          },
          {
            "event_fallen_knight_army",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceWar_MainDlg.\u003C\u003Ef__switch\u0024map1F.TryGetValue(para2, out num))
      {
        switch (num)
        {
          case 0:
            gameObject = UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[11].gameObject);
            goto label_10;
          case 1:
            gameObject = UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[12].gameObject);
            goto label_10;
        }
      }
    }
    gameObject = UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[11].gameObject);
label_10:
    gameObject.name = "Slot_" + (object) (1000 + index);
    gameObject.transform.parent = this.panel.list.transform;
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    AllianceWar_MainDlg_Slot component = gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    component.Setup(knightData.marchId);
    this.panel.slots.Add(component);
  }

  private void AddSlot(AllianceMagicData magic, int index)
  {
    if (magic.CurrentState == "releasing")
      return;
    GameObject gameObject = magic.AllianceId == 0L || magic.AllianceId != PlayerData.inst.allianceId ? UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[17].gameObject) : (magic.allTargetAlliance == null || !magic.allTargetAlliance.Contains(PlayerData.inst.allianceId) ? UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[16].gameObject) : UnityEngine.Object.Instantiate<GameObject>(this.panel.orgSlots[15].gameObject));
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
      return;
    gameObject.name = "Slot_" + (object) (1000 + index);
    gameObject.transform.parent = this.panel.list.transform;
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    AllianceWar_MainDlg_Slot component = (AllianceWar_MainDlg_Slot) gameObject.GetComponent<AllianceWar_MainDlg_Skill_Slot>();
    component.Setup(magic.MagicId);
    this.panel.slots.Add(component);
  }

  public void Reset()
  {
    this.uiManagerPanel.BT_Back = this.transform.Find("Background/Full_Screen_Title/Btn_Full_Screen_Back").gameObject.GetComponent<UIButton>();
    this.uiManagerPanel.BT_Close = this.transform.Find("Background/Full_Screen_Title/Btn_Full_Screen_Close").gameObject.GetComponent<UIButton>();
    this.panel.Tag_Combat = this.transform.Find("MainPanel/Full_Screen_Title_Sub_Tabs_2/Sub_Tabs_2/Tab_0/1_Sub_Tab_Pressed").gameObject.transform;
    this.panel.Tag_Event = this.transform.Find("MainPanel/Full_Screen_Title_Sub_Tabs_2/Sub_Tabs_2/Tab_1/3_Sub_Tab_Pressed").gameObject.transform;
    this.panel.Tag_Skill = this.transform.Find("MainPanel/Full_Screen_Title_Sub_Tabs_2/Sub_Tabs_2/Tab_2/3_Sub_Tab_Pressed").gameObject.transform;
    this.panel.magicNotifyContainer = this.transform.Find("MainPanel/Full_Screen_Title_Sub_Tabs_2/Sub_Tabs_2/Tab_2/MagicNotify").gameObject.transform;
    this.panel.magicNotifyCount = this.transform.Find("MainPanel/Full_Screen_Title_Sub_Tabs_2/Sub_Tabs_2/Tab_2/MagicNotify/Count").gameObject.GetComponent<UILabel>();
    this.panel.warNotifyCount = this.transform.Find("MainPanel/Full_Screen_Title_Sub_Tabs_2/Sub_Tabs_2/Tab_0/WarNotify/Count").gameObject.GetComponent<UILabel>();
    this.panel.eventNotifyCount = this.transform.Find("MainPanel/Full_Screen_Title_Sub_Tabs_2/Sub_Tabs_2/Tab_1/EventNotify/Count").gameObject.GetComponent<UILabel>();
    this.panel.mainPanel = this.transform.Find("MainPanel").gameObject.transform;
    this.panel.NoWarContainer = this.transform.Find("MainPanel/NoWarContainer").gameObject;
    this.panel.NoEventConatiner = this.transform.Find("MainPanel/NoEventContainer").gameObject;
    this.panel.NoSkillContainer = this.transform.Find("MainPanel/NoMagicWarContainer").gameObject;
    this.panel.scrollView = this.transform.Find("MainPanel/ListPanel").gameObject.GetComponent<UIScrollView>();
    this.panel.viewPort = this.transform.Find("MainPanel/svCamera").gameObject.GetComponent<UIViewport>();
    this.panel.list = this.transform.Find("MainPanel/ListPanel/ListTable").gameObject.GetComponent<UITable>();
    this.panel.BT_WarLog = this.transform.Find("MainPanel/BT_WarLog").gameObject.GetComponent<UIButton>();
    this.panel.BT_MagicLog = this.transform.Find("MainPanel/BT_MagicLog").gameObject.GetComponent<UIButton>();
    this.panel.orgSlots = new AllianceWar_MainDlg_Slot[18];
    this.panel.orgSlots[0] = this.transform.Find("MainPanel/ListPanel/AllianceWar_MainSlot_City_March_Attack").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[1] = this.transform.Find("MainPanel/ListPanel/AllianceWar_MainSlot_City_March_Defense").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[2] = this.transform.Find("MainPanel/ListPanel/AllianceWar_MainSlot_City_Rally_Attack").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[3] = this.transform.Find("MainPanel/ListPanel/AllianceWar_MainSlot_City_Rally_Defense").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[4] = this.transform.Find("MainPanel/ListPanel/AllianceWar_MainSlot_Fortress_March_Attack").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[5] = this.transform.Find("MainPanel/ListPanel/AllianceWar_MainSlot_Fortress_March_Defense").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[6] = this.transform.Find("MainPanel/ListPanel/AllianceWar_MainSlot_Fortress_March_Reinforce").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[7] = this.transform.Find("MainPanel/ListPanel/AllianceWar_MainSlot_Fortress_Rally_Attack").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[8] = this.transform.Find("MainPanel/ListPanel/AllianceWar_MainSlot_Fortress_Rally_Defense").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[9] = this.transform.Find("MainPanel/ListPanel/AllianceWar_MainSlot_Fortress_Depolishing").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[10] = this.transform.Find("MainPanel/ListPanel/AllianceWar_MainSlot_Boss_Rally_Attack").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[11] = this.transform.Find("MainPanel/ListPanel/FallenKnight_MainSlot_City_March_Defense").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[12] = this.transform.Find("MainPanel/ListPanel/FallenKnight_MainSlot_City_Rally_Defense").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[13] = this.transform.Find("MainPanel/ListPanel/FallenKnight_MainSlot_Fortress_Depolishing").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[14] = this.transform.Find("MainPanel/ListPanel/AllianceWar_MainSlot_AlliancePortal_Rally_Attack").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[15] = this.transform.Find("MainPanel/ListPanel/AllianceWar_MainSlot_Magic_Benefit").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[17] = this.transform.Find("MainPanel/ListPanel/AllianceWar_MainSlot_Magic_Defense").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    this.panel.orgSlots[16] = this.transform.Find("MainPanel/ListPanel/AllianceWar_MainSlot_Magic_Attack").gameObject.GetComponent<AllianceWar_MainDlg_Slot>();
    for (int index = 0; index < 18; ++index)
      this.panel.orgSlots[index].gameObject.SetActive(false);
    this.panel.JoinAllianceContainer = this.transform.Find("Background/JoinAllianceContainer").gameObject.transform;
    this.panel.JoinAllianceContainer.gameObject.SetActive(false);
  }

  public void OnWarLogButtonClick()
  {
    UIManager.inst.OpenPopup("AllianceWar/AllianceWarLogPop", (Popup.PopupParameter) null);
  }

  public void OnMagicButtonClick()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceMagicWarLogPop", (Popup.PopupParameter) null);
  }

  public void OnHelpClick()
  {
    UIManager.inst.OpenPopup("HelpPopup", (Popup.PopupParameter) new HelpPopup.Parameter()
    {
      id = "help_dragon_skill"
    });
  }

  public void OnTagCombatSelect()
  {
    this._data.selectType = AllianceWar_MainDlg.Parameter.SelectType.war;
    this.FreshPanel();
  }

  public void OnTagEventSelect()
  {
    this._data.selectType = AllianceWar_MainDlg.Parameter.SelectType.knight;
    this.FreshPanel();
  }

  public void OnSkillEventSelect()
  {
    RequestManager.inst.SendRequest("AllianceTemple:loadMagic", Utils.Hash((object) "alliance_id", (object) PlayerData.inst.allianceId), (System.Action<bool, object>) ((result, orgSrc) =>
    {
      if (!result)
        return;
      this.FreshPanel();
    }), true);
    this._data.selectType = AllianceWar_MainDlg.Parameter.SelectType.magic;
    this.FreshPanel();
  }

  public void ClickJoinAlliance()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceJoinDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    this.FreshPanel();
  }

  public void OnSecond(int time)
  {
    if (this._data.selectType == AllianceWar_MainDlg.Parameter.SelectType.magic)
      return;
    List<long> warMarchs = PlayerData.inst.allianceWarManager.warMarchs;
    List<RallyData> rallyesByAllianceId = DBManager.inst.DB_Rally.GetRallyesByAllianceId(PlayerData.inst.allianceId);
    int num = 0;
    if (warMarchs != null)
      num += warMarchs.Count;
    if (rallyesByAllianceId != null)
      num += rallyesByAllianceId.Count;
    if (num == this.panel.slots.Count)
      return;
    this.FreshPanel();
  }

  public void OnMagicChanged(AllianceMagicData magic)
  {
    if (this._data.selectType == AllianceWar_MainDlg.Parameter.SelectType.magic)
      this.FreshPanel();
    this.magicCount = DBManager.inst.DB_AllianceMagic.GetAllianceWarMagicList().Count;
  }

  public class Params : UI.Dialog.DialogParameter
  {
  }

  private class Data
  {
    public AllianceWar_MainDlg.Parameter.SelectType selectType;
  }

  [Serializable]
  protected class Panel
  {
    [HideInInspector]
    public List<AllianceWar_MainDlg_Slot> slots = new List<AllianceWar_MainDlg_Slot>();
    public const int SLOT_CITY_MARCH_ATTACK_INDEX = 0;
    public const int SLOT_CITY_MARCH_DEFENSE_INDEX = 1;
    public const int SLOT_CITY_RALLY_ATTACK_INDEX = 2;
    public const int SLOT_CITY_RALLY_DEFENSE_INDEX = 3;
    public const int SLOT_FORTRESS_MARCH_ATTACK_INDEX = 4;
    public const int SLOT_FORTRESS_MARCH_DEFENSE_INDEX = 5;
    public const int SLOT_FORTRESS_MARCH_REINFORCE_INDEX = 6;
    public const int SLOT_FORTRESS_RALLY_ATTACK_INDEX = 7;
    public const int SLOT_FORTRESS_RALLY_DEFENSE_INDEX = 8;
    public const int SLOT_FORTRESS_DEPOLISHING_INDEX = 9;
    public const int SLOT_BOSS_RALLY_ATTACK_INDEX = 10;
    public const int SLOT_FALLEN_KNIGHT_CITY_ATTACK = 11;
    public const int SLOT_FALLEN_KNIGHT_RALLY_ATTACK = 12;
    public const int SLOT_FALLEN_KNIGHT_FORTRESS_ATTACK = 13;
    public const int SLOT_ALLIANCE_BOSS_RALLY_ATTACK = 14;
    public const int SLOT_ALLIANCE_SKILL_BENEFIT = 15;
    public const int SLOT_ALLIANCE_SKILL_ATTACK = 16;
    public const int SLOT_ALLIANCE_SKILL_DEFENSE = 17;
    public const int SLOT_ORG_COUNT = 18;
    public Transform mainPanel;
    public Transform Tag_Event;
    public Transform Tag_Combat;
    public Transform Tag_Skill;
    public GameObject NoWarContainer;
    public GameObject NoEventConatiner;
    public GameObject NoSkillContainer;
    public UIViewport viewPort;
    public UIScrollView scrollView;
    public UITable list;
    public UILabel warNotifyCount;
    public UILabel eventNotifyCount;
    public Transform magicNotifyContainer;
    public UILabel magicNotifyCount;
    public UIButton BT_WarLog;
    public UIButton BT_MagicLog;
    public AllianceWar_MainDlg_Slot[] orgSlots;
    public Transform JoinAllianceContainer;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public AllianceWar_MainDlg.Parameter.SelectType selectType;
    public bool isKnight;

    public enum SelectType
    {
      war,
      knight,
      magic,
    }
  }
}
