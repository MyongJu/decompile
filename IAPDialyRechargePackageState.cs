﻿// Decompiled with JetBrains decompiler
// Type: IAPDialyRechargePackageState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class IAPDialyRechargePackageState
{
  public bool all;
  public ArrayList bought;

  public void Decode(Hashtable data)
  {
    DatabaseTools.UpdateData(data, "all", ref this.all);
    if (!data.Contains((object) "bought"))
      return;
    this.bought = data[(object) "bought"] as ArrayList;
  }
}
