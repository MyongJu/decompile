﻿// Decompiled with JetBrains decompiler
// Type: PlayerMessage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;

public class PlayerMessage : AbstractMailEntry
{
  private List<PlayerMessageEntry> playerMessageEntries;

  public string content
  {
    get
    {
      if (this.body == null)
        return "Loading...";
      return this.body[(object) "text"] as string;
    }
    set
    {
      if (!this.body.ContainsKey((object) "text"))
        this.body.Add((object) "text", (object) string.Empty);
      this.body[(object) "text"] = (object) value;
      if (this.mailUpdated == null)
        return;
      this.mailUpdated();
    }
  }

  public override void ApplyData(Hashtable ht)
  {
    base.ApplyData(ht);
    if (this.playerMessageEntries == null)
      return;
    this.playerMessageEntries.Clear();
    this.playerMessageEntries = (List<PlayerMessageEntry>) null;
  }

  public List<PlayerMessageEntry> GetPlayerMessageEntries()
  {
    this.playerMessageEntries = new List<PlayerMessageEntry>();
    ArrayList arrayList = this.HtBody[(object) "params"] as ArrayList;
    for (int index = 0; index < arrayList.Count; ++index)
    {
      Hashtable hashtable = arrayList[index] as Hashtable;
      PlayerMessageEntry playerMessageEntry = new PlayerMessageEntry();
      playerMessageEntry.time = (long) int.Parse(hashtable[(object) "t"].ToString());
      playerMessageEntry.sendToAll = hashtable.ContainsKey((object) "g") && bool.Parse(hashtable[(object) "g"].ToString());
      playerMessageEntry.content = hashtable[(object) "c"].ToString();
      playerMessageEntry.fromUID = long.Parse(hashtable[(object) "u"].ToString());
      playerMessageEntry.fromPortrait = playerMessageEntry.fromUID != PlayerData.inst.uid ? this.from_portrait : PlayerData.inst.userData.portrait.ToString();
      playerMessageEntry.fromIcon = playerMessageEntry.fromUID != PlayerData.inst.uid ? this.from_icon : PlayerData.inst.userData.Icon;
      playerMessageEntry.fromLordTitleId = playerMessageEntry.fromUID != PlayerData.inst.uid ? this.from_lord_title : PlayerData.inst.userData.LordTitle;
      playerMessageEntry.blocked = hashtable.ContainsKey((object) "b") && bool.Parse(hashtable[(object) "b"].ToString());
      this.playerMessageEntries.Add(playerMessageEntry);
    }
    return this.playerMessageEntries;
  }

  public void GetPreMessage()
  {
  }

  public void AddMessageEntries(PlayerMessageEntry entry)
  {
    if (this.playerMessageEntries == null)
      this.playerMessageEntries = new List<PlayerMessageEntry>();
    this.playerMessageEntries.Add(entry);
    if (this.mailUpdated == null)
      return;
    this.mailUpdated();
  }

  public override string GetBodyString()
  {
    return this.GetPlayerMessageEntries()[this.GetPlayerMessageEntries().Count - 1].Content;
  }

  public override string ToString()
  {
    return base.ToString() + "content=" + this.content;
  }

  public override void Display()
  {
    UIManager.inst.OpenDlg("Mail/MailHistroryDlg", (UI.Dialog.DialogParameter) new MailHistrotyDlg.Parameter()
    {
      mail = this,
      focusMailID = this.mailID,
      mailCategory = this.category
    }, 1 != 0, 1 != 0, 1 != 0);
    base.Display();
  }
}
