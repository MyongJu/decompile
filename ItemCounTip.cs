﻿// Decompiled with JetBrains decompiler
// Type: ItemCounTip
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ItemCounTip : MonoBehaviour
{
  public GameObject newItem;
  public UILabel count;

  private void Start()
  {
  }

  private void OnEnable()
  {
    this.AddEventHandler();
  }

  private void AddEventHandler()
  {
    ItemBag.Instance.OnNewItemChange += new System.Action(this.Refresh);
  }

  private void RemoveEventHandler()
  {
    ItemBag.Instance.OnNewItemChange -= new System.Action(this.Refresh);
  }

  private void Refresh()
  {
    if (GameEngine.IsReady())
      NGUITools.SetActive(this.newItem, ItemBag.Instance.HadNewItems());
    this.count.text = string.Empty;
  }

  private void OnDisable()
  {
    this.RemoveEventHandler();
  }
}
