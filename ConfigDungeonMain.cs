﻿// Decompiled with JetBrains decompiler
// Type: ConfigDungeonMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDungeonMain
{
  private Dictionary<string, DungeonMainStaticInfo> _datas;
  private Dictionary<int, DungeonMainStaticInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<DungeonMainStaticInfo, string>(res as Hashtable, "ID", out this._datas, out this._dicByUniqueId);
  }

  public DungeonMainStaticInfo Get(int internalId)
  {
    if (this._dicByUniqueId != null && this._dicByUniqueId.ContainsKey(internalId))
      return this._dicByUniqueId[internalId];
    return (DungeonMainStaticInfo) null;
  }

  public DungeonMainStaticInfo Get(string id)
  {
    if (this._datas != null && this._datas.ContainsKey(id))
      return this._datas[id];
    return (DungeonMainStaticInfo) null;
  }

  public void Clear()
  {
    if (this._datas != null)
      this._datas.Clear();
    if (this._dicByUniqueId == null)
      return;
    this._dicByUniqueId.Clear();
  }
}
