﻿// Decompiled with JetBrains decompiler
// Type: AllianceHospitalController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class AllianceHospitalController : MonoBehaviour, IConstructionController
{
  public GameObject m_Scaffold;
  public GameObject m_Workers;
  public GameObject m_Dust;

  public void UpdateUI(TileData tile)
  {
    AllianceHospitalData hospitalData = tile.HospitalData;
    if (hospitalData == null)
      return;
    this.UpdateScaffold(hospitalData);
    this.UpdateWorkers(hospitalData);
    this.UpdateDust(hospitalData);
  }

  public void UpdateForWarList(AllianceHospitalData hospital)
  {
    this.m_Scaffold.SetActive(false);
    this.m_Workers.SetActive(false);
    this.m_Dust.SetActive(false);
  }

  public void Reset()
  {
    this.m_Scaffold.SetActive(false);
    this.m_Workers.SetActive(false);
    this.m_Dust.SetActive(false);
  }

  private void UpdateScaffold(AllianceHospitalData hospital)
  {
    bool flag = false;
    if (hospital.CurrentState == AllianceHospitalData.State.BUILDING)
      flag = true;
    this.m_Scaffold.SetActive(flag);
  }

  private void UpdateWorkers(AllianceHospitalData hospital)
  {
    bool flag = false;
    if (hospital.CurrentState == AllianceHospitalData.State.BUILDING)
      flag = true;
    this.m_Workers.SetActive(flag);
  }

  private void UpdateDust(AllianceHospitalData hospital)
  {
    bool flag = false;
    if (hospital.CurrentState == AllianceHospitalData.State.BUILDING)
      flag = true;
    this.m_Dust.SetActive(flag);
  }

  public void SetSortingLayerID(int sortingLayerID)
  {
  }
}
