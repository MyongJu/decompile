﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerMerlinTower
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingControllerMerlinTower : BuildingControllerNew
{
  private const string CAN_NOT_TELEPORT = "toast_tower_mine_unable_to_go";
  private const string NOT_OPEN_TOAST = "toast_tower_stronghold_low";
  private const string VFX_PATH = "Prefab/VFX/fx_merlin_tower_01";
  private GameObject vfx;

  public override void InitBuilding()
  {
    base.InitBuilding();
    this.onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public override void Dispose()
  {
    base.Dispose();
    this.onBuildingSelected -= new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void OnBuildingSelected()
  {
    AudioManager.Instance.StopAndPlaySound("sfx_kingdom_map_click_alliance_portal");
    MerlinTowerPayload.Instance.SendGetUserInfoRequest(new System.Action<bool, object>(this.OnUserInfoRequestCallback), true);
  }

  private void OnUserInfoRequestCallback(bool ret, object data)
  {
    if (MerlinTowerPayload.Instance.IsActivityOn)
    {
      if (MerlinTowerPayload.Instance.TowerState == MerlinTowerState.Opened)
        UIManager.inst.OpenDlg("MerlinTower/MerlinTowerDlg", (UI.Dialog.DialogParameter) null, true, true, true);
      else
        UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_tower_stronghold_low", new Dictionary<string, string>()
        {
          {
            "0",
            MerlinTowerPayload.Instance.OpenLevel.ToString()
          }
        }, true), (System.Action) null, 4f, false);
    }
    else
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_tower_mine_unable_to_go", true), (System.Action) null, 4f, false);
  }

  private void OnSecond(int time)
  {
    if (MerlinTowerPayload.Instance.TowerState == MerlinTowerState.Opened)
      this.SetVFX();
    else
      this.DeleteVFX();
  }

  private void SetVFX()
  {
    if (!((UnityEngine.Object) null == (UnityEngine.Object) this.vfx))
      return;
    this.vfx = AssetManager.Instance.HandyLoad("Prefab/VFX/fx_merlin_tower_01", typeof (GameObject)) as GameObject;
    if (!(bool) ((UnityEngine.Object) this.vfx))
      return;
    this.vfx = UnityEngine.Object.Instantiate<GameObject>(this.vfx);
    this.vfx.transform.parent = this.transform;
    this.vfx.transform.localPosition = Vector3.zero;
    this.vfx.transform.localScale = Vector3.one;
  }

  private void DeleteVFX()
  {
    if (!((UnityEngine.Object) this.vfx != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.vfx);
    this.vfx = (GameObject) null;
  }
}
