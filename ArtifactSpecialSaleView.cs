﻿// Decompiled with JetBrains decompiler
// Type: ArtifactSpecialSaleView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class ArtifactSpecialSaleView : MonoBehaviour
{
  private Dictionary<int, ArtifactSpecialSaleItemRenderer> _itemDict = new Dictionary<int, ArtifactSpecialSaleItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private const int ARTIFACT_SPECIAL_SALE_COUNT = 3;
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;

  public void UpdateUI()
  {
    this.Initialize();
    this.ClearData();
    List<ArtifactSpecialSaleData> specialSaleDataList = ArtifactSalePayload.Instance.GetArtifactSpecialSaleDataList();
    for (int i = 1; i <= 3; ++i)
    {
      ArtifactSpecialSaleItemRenderer itemRenderer = this.CreateItemRenderer(specialSaleDataList != null ? specialSaleDataList.Find((Predicate<ArtifactSpecialSaleData>) (x => x.Slot == i)) : (ArtifactSpecialSaleData) null);
      this._itemDict.Add(i, itemRenderer);
    }
    this.Reposition();
  }

  private void Initialize()
  {
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
  }

  public void ClearData()
  {
    using (Dictionary<int, ArtifactSpecialSaleItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, ArtifactSpecialSaleItemRenderer> current = enumerator.Current;
        current.Value.ClearData();
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private ArtifactSpecialSaleItemRenderer CreateItemRenderer(ArtifactSpecialSaleData saleData)
  {
    GameObject gameObject = this._itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    ArtifactSpecialSaleItemRenderer component = gameObject.GetComponent<ArtifactSpecialSaleItemRenderer>();
    component.SetData(saleData);
    return component;
  }
}
