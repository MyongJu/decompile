﻿// Decompiled with JetBrains decompiler
// Type: SpeedupConfimPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class SpeedupConfimPopup : PopupBase
{
  public UIButton closeBtn;
  public UIButton yesBtn;
  public UIButton noBtn;
  private System.Action yes;

  public void InitUI(System.Action yes)
  {
    this.closeBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnCloseBtnClicked)));
    this.yesBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnYesBtnClicked)));
    this.noBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnNoBtnClicked)));
    this.yes = yes;
  }

  private void OnCloseBtnClicked()
  {
    this.Close();
  }

  private void OnYesBtnClicked()
  {
    this.Close();
    if (this.yes == null)
      return;
    this.yes();
  }

  private void OnNoBtnClicked()
  {
    this.Close();
  }
}
