﻿// Decompiled with JetBrains decompiler
// Type: EquipmentScrollChipComponetData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class EquipmentScrollChipComponetData : IComponentData
{
  public ConfigEquipmentScrollChipInfo equipmentScrollChipInfo;
  public ItemStaticInfo ItemInfo;
  public BagType bagType;

  public EquipmentScrollChipComponetData(ConfigEquipmentScrollChipInfo equipmentScrollChipInfo, BagType bagType = BagType.DragonKnight)
  {
    this.equipmentScrollChipInfo = equipmentScrollChipInfo;
    this.ItemInfo = ConfigManager.inst.DB_Items.GetItem(equipmentScrollChipInfo.itemID);
    this.bagType = bagType;
  }
}
