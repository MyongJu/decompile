﻿// Decompiled with JetBrains decompiler
// Type: AllianceDonateRankItemData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class AllianceDonateRankItemData
{
  private int _lordTitle;

  public long uid { get; set; }

  public string Image { get; set; }

  public string Name { get; set; }

  public long Honor { get; set; }

  public long Donation { get; set; }

  public int Portrait { get; set; }

  public long Alliance_EXP { get; set; }

  public int Rank { get; set; }

  public string Icon { get; set; }

  public int LordTitle
  {
    get
    {
      return this._lordTitle;
    }
    set
    {
      this._lordTitle = value;
    }
  }

  public void Decode(object orgData)
  {
    Hashtable data = orgData as Hashtable;
    if (data == null)
      return;
    bool flag = false;
    if (data.ContainsKey((object) "uid"))
      this.uid = long.Parse(data[(object) "uid"].ToString());
    if (data.ContainsKey((object) "name"))
      this.Name = data[(object) "name"].ToString();
    if (data.ContainsKey((object) "icon"))
      this.Icon = data[(object) "icon"].ToString();
    if (data.ContainsKey((object) "lord_title"))
      int.TryParse(data[(object) "lord_title"].ToString(), out this._lordTitle);
    if (data.ContainsKey((object) "rank"))
      this.Rank = int.Parse(data[(object) "rank"].ToString());
    if (data.ContainsKey((object) "portrait"))
      this.Portrait = int.Parse(data[(object) "portrait"].ToString());
    if (data.ContainsKey((object) "day_donation"))
    {
      this.Donation = Utils.TryParseLong(data, (object) "day_donation");
      flag = true;
    }
    if (data.ContainsKey((object) "day_alliance_exp"))
      this.Alliance_EXP = Utils.TryParseLong(data, (object) "day_alliance_exp");
    if (data.ContainsKey((object) "day_honor"))
      this.Honor = Utils.TryParseLong(data, (object) "day_honor");
    if (flag)
      return;
    if (data.ContainsKey((object) "week_donation"))
    {
      this.Donation = Utils.TryParseLong(data, (object) "week_donation");
      flag = true;
    }
    if (data.ContainsKey((object) "week_alliance_exp"))
      this.Alliance_EXP = Utils.TryParseLong(data, (object) "week_alliance_exp");
    if (data.ContainsKey((object) "week_honor"))
      this.Honor = Utils.TryParseLong(data, (object) "week_honor");
    if (flag)
      return;
    if (data.ContainsKey((object) "history_donation"))
      this.Donation = Utils.TryParseLong(data, (object) "history_donation");
    if (data.ContainsKey((object) "history_alliance_exp"))
      this.Alliance_EXP = Utils.TryParseLong(data, (object) "history_alliance_exp");
    if (!data.ContainsKey((object) "history_honor"))
      return;
    this.Honor = Utils.TryParseLong(data, (object) "history_honor");
  }

  public struct Params
  {
    public const string UID = "uid";
    public const string PLAYER_NAME = "name";
    public const string RANK = "rank";
    public const string DAY_DONATION = "day_donation";
    public const string DAY_XP = "day_alliance_exp";
    public const string DAY_HONOR = "day_honor";
    public const string PORTRAIT = "portrait";
    public const string WEEK_DONATION = "week_donation";
    public const string WEEK_XP = "week_alliance_exp";
    public const string WEEK_HONOR = "week_honor";
    public const string HISTORY_DONATION = "history_donation";
    public const string HISTORY_XP = "history_alliance_exp";
    public const string HISTORY_HONOR = "history_honor";
    public const string PLAYER_ICON = "icon";
    public const string PLAYE_LORD_TITLE = "lord_title";
  }
}
