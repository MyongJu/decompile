﻿// Decompiled with JetBrains decompiler
// Type: NetApi
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetApi
{
  public static string DefaultServerURL = "http://dw-us.funplusgame.com/api/";
  public static string DefaultToolServerURL = "http://ec2-52-6-228-115.compute-1.amazonaws.com:8080/";
  private NetApi.AppVersioState _state = NetApi.AppVersioState.None;
  private string _adjustTracker = string.Empty;
  private List<string> CN_Languages = new List<string>()
  {
    "zh-CN",
    "zh-Hans",
    "zh-Hans-CN"
  };
  private string _rtmError = string.Empty;
  private string _rtmNodeHost = string.Empty;
  private string _PZTest = "0";
  private string _ip = string.Empty;
  private string _mac = string.Empty;
  public const string DATA_RETURN_PARAM = "ret";
  public const string ServerUrlField = "ServerURL";
  public const string ServerToolUrlFiled = "ServerToolURL";
  public const string SERVER_IP = "server_ip";
  private bool UseRandomUrl;
  private string _token;
  private string _rtmToken;
  private string _assetform;
  private Hashtable _manifest;
  private Dictionary<string, string> _headers;
  private Dictionary<string, string> _encryptHeaders;
  private Dictionary<string, string> _configHeaders;
  private int _seqNum;
  private string _bundlesVersion;
  private string _bundlesVersionR;
  private string _bundlesVersionG;
  private string _configFilesVersion;
  private string _upgradeUrl;
  private static NetApi _instance;
  private bool _inited;
  private string _rootUrl;
  private NetApi.RTMSTATE _RTM_Status;
  private NetApi.OfficialUpdateState _needOfficialUpdate;
  private string _officialUpgradeUrl;
  private object _hotfixBundleContent;
  private object _hotfixAssetContent;
  private string _server_ip;

  public static NetApi inst
  {
    get
    {
      if (NetApi._instance == null)
      {
        NetApi._instance = new NetApi();
        NetApi._instance.Init();
      }
      return NetApi._instance;
    }
  }

  public string AdjustTracker
  {
    get
    {
      return this._adjustTracker;
    }
    set
    {
      this._adjustTracker = value;
    }
  }

  public void Init()
  {
    this._inited = false;
  }

  public void Dispose()
  {
    this._adjustTracker = string.Empty;
    this._inited = false;
    this._bundlesVersion = (string) null;
    this._bundlesVersionR = (string) null;
    this._bundlesVersionG = (string) null;
    this._configFilesVersion = (string) null;
    this._upgradeUrl = (string) null;
    this._state = NetApi.AppVersioState.None;
    this._RTM_Status = NetApi.RTMSTATE.None;
    this._server_ip = (string) null;
    this._PZTest = (string) null;
  }

  public static string GetCurrentUrl()
  {
    return NetApi.DefaultServerURL;
  }

  public static string GetCurrentToolUrl()
  {
    return NetApi.DefaultToolServerURL;
  }

  public string GetRootUrl()
  {
    return NetApi.GetCurrentUrl();
  }

  public string BuildApiUrl(string uri = "")
  {
    return this.RandomUrl(string.Format("{0}/{1}", (object) this.GetRootUrl(), (object) uri));
  }

  public string BuildDefaultServerURL(string domain = null)
  {
    if (string.IsNullOrEmpty(domain))
      return this.GetServerDomain() + "/api/";
    return domain + "/api/";
  }

  public void BuildFinalServerURL()
  {
    NetApi.DefaultServerURL = this.Server_Ip;
    if (NetApi.DefaultServerURL != null)
      return;
    NetApi.DefaultServerURL = this.BuildDefaultServerURL((string) null);
  }

  private string GetServerDomain()
  {
    if (CustomDefine.IsSQWanPackage())
      return "http://dw-cn.ifunplus.cn:8000";
    return this.MatchCN() ? "http://koa-cn-gs-uc.ifunplus.cn" : "http://dw-us.funplusgame.com";
  }

  public bool MatchCN()
  {
    return this.CN_Languages.Contains(NativeManager.inst.SysLanguage);
  }

  public string BuildConfigUrl(string fileName)
  {
    return this.RandomUrl(string.Format("{0}/{1}/{2}/{3}/{4}", this.AssetsAddrs[0], (object) "assets", (object) "config", (object) this.ConfigFilesVersion, (object) this.UrlEncode(fileName)));
  }

  public string BuildAssetLocalServerUrl(string fileName)
  {
    return this.RandomUrl(string.Format("{0}/{1}/{2}/{3}/{4}/{5}", this.AssetsAddrs[1], (object) "assets", (object) "bundle", (object) this.BundlesVersion, (object) this.AssetForm, (object) this.UrlEncode(fileName)));
  }

  public string BuildLocalUrl(string fileName)
  {
    return string.Format("file:///{0}/{1}", (object) AssetConfig.localPath, (object) fileName);
  }

  public string BuildLocalPath(string fileName)
  {
    return string.Format("{0}/{1}", (object) AssetConfig.localPath, (object) fileName);
  }

  public string BuildLocalBundleUrl(string fileName)
  {
    return string.Format("file:///{0}/{1}", (object) AssetConfig.BundleCacheDir, (object) fileName);
  }

  public string BuildLocalBundlePath(string fileName)
  {
    return string.Format("{0}/{1}", (object) AssetConfig.BundleCacheDir, (object) fileName);
  }

  public string BuildStreamingAssetsPath(string fileName)
  {
    return string.Format("{0}/{1}", (object) Application.streamingAssetsPath, (object) fileName);
  }

  public string BuildStreamingAssetsCacheUrl(string fileName)
  {
    return Application.platform != RuntimePlatform.Android ? string.Format("file:///{0}/{1}", (object) BuildConfig.BUNDLE_CACHE_FOLDER, (object) fileName) : string.Format("{0}/{1}", (object) BuildConfig.BUNDLE_CACHE_FOLDER, (object) fileName);
  }

  public string BuildStreamingConfigCacheUrl(string fileName)
  {
    return Application.platform != RuntimePlatform.Android ? string.Format("file:///{0}/{1}", (object) BuildConfig.CONFIG_CACHE_FOLDER, (object) fileName) : string.Format("{0}/{1}", (object) BuildConfig.CONFIG_CACHE_FOLDER, (object) fileName);
  }

  public string BuildStreamingAssetsCachePath(string fileName)
  {
    return string.Format("{0}/{1}", (object) BuildConfig.BUNDLE_CACHE_FOLDER, (object) fileName);
  }

  public string BuildBundleUrlByVersion(string fileName, string bundleVersion)
  {
    return this.RandomUrl(string.Format("{0}/{1}/{2}/{3}/{4}/{5}", this.AssetsAddrs[0], (object) "assets", (object) "bundle", (object) bundleVersion, (object) this.AssetForm, (object) this.UrlEncode(fileName)));
  }

  public string BuildBundleUrl(string fileName)
  {
    return this.RandomUrl(string.Format("{0}/{1}/{2}/{3}/{4}/{5}", this.AssetsAddrs[0], (object) "assets", (object) "bundle", (object) VersionManager.Instance.GetBundleVersionHead(fileName), (object) this.AssetForm, (object) this.UrlEncode(fileName)));
  }

  public void Enqueue(string action, Hashtable ht = null, System.Action<Hashtable, Hashtable> callback = null)
  {
  }

  public string FacebookURL
  {
    get
    {
      return "https://www.facebook.com/koadw/";
    }
  }

  public string ReBuildUrl(string url, int count)
  {
    if (count < 1)
      return this.RandomUrl(url);
    string url1 = url;
    if (url.Contains("/api/") && this.ProxyAddrs != null && this.ProxyAddrs.Count > 0)
    {
      int index1 = (count - 1) % this.ProxyAddrs.Count;
      int index2 = count % this.ProxyAddrs.Count;
      return this.RandomUrl(url.Replace(this.ProxyAddrs[index1].ToString(), this.ProxyAddrs[index2].ToString()));
    }
    if (!url.Contains("/assets/") || this.AssetsAddrs == null || this.AssetsAddrs.Count <= 0)
      return this.RandomUrl(url1);
    int index3 = (count - 1) % this.AssetsAddrs.Count;
    int index4 = count % this.AssetsAddrs.Count;
    return this.RandomUrl(url.Replace(this.AssetsAddrs[index3].ToString(), this.AssetsAddrs[index4].ToString()));
  }

  private string UrlEncode(string str)
  {
    if (str == null)
      return (string) null;
    return WWW.EscapeURL(str);
  }

  private string RandomUrl(string url)
  {
    if (this.UseRandomUrl)
      return string.Format("{0}?key={1}{2}", (object) url, (object) NetServerTime.inst.UpdateTime, (object) UnityEngine.Random.value);
    return url;
  }

  public string Token
  {
    get
    {
      return this._token;
    }
    set
    {
      this._token = value;
    }
  }

  public string RtmToken
  {
    get
    {
      return this._rtmToken;
    }
    set
    {
      this._rtmToken = value;
    }
  }

  public Hashtable Manifest
  {
    get
    {
      return this._manifest;
    }
    set
    {
      this._manifest = value;
    }
  }

  public int SeqNum
  {
    get
    {
      return this._seqNum;
    }
    set
    {
      this._seqNum = value;
    }
  }

  public Hashtable MainVersion
  {
    get
    {
      if (this._manifest != null)
        return this._manifest[(object) "build"] as Hashtable;
      return (Hashtable) null;
    }
  }

  public Hashtable PushSystemInfo
  {
    get
    {
      if (this._manifest != null && this._manifest.ContainsKey((object) "rtm"))
        return this._manifest[(object) "rtm"] as Hashtable;
      return (Hashtable) null;
    }
  }

  public int PushSystemId
  {
    get
    {
      Hashtable pushSystemInfo = this.PushSystemInfo;
      int result;
      if (pushSystemInfo != null && pushSystemInfo.ContainsKey((object) "project_id") && int.TryParse(pushSystemInfo[(object) "project_id"] as string, out result))
        return result;
      return 10001;
    }
  }

  public ArrayList PushSystemHosts
  {
    get
    {
      Hashtable pushSystemInfo = this.PushSystemInfo;
      if (pushSystemInfo != null && pushSystemInfo.ContainsKey((object) "client_hosts"))
        return pushSystemInfo[(object) "client_hosts"] as ArrayList;
      return (ArrayList) null;
    }
  }

  public ArrayList PushSystemNodes
  {
    get
    {
      Hashtable pushSystemInfo = this.PushSystemInfo;
      if (pushSystemInfo != null && pushSystemInfo.ContainsKey((object) "client_nodes"))
        return pushSystemInfo[(object) "client_nodes"] as ArrayList;
      return (ArrayList) null;
    }
  }

  public string RTM_URL { get; set; }

  public string RTMError
  {
    get
    {
      return this._rtmError;
    }
    set
    {
      this._rtmError = value;
    }
  }

  public NetApi.RTMSTATE RTM_Status
  {
    get
    {
      return this._RTM_Status;
    }
    set
    {
      this._RTM_Status = value;
    }
  }

  public string RTMNodeHost
  {
    get
    {
      return this._rtmNodeHost;
    }
    set
    {
      this._rtmNodeHost = value;
    }
  }

  public void ResetRTM()
  {
    this._RTM_Status = NetApi.RTMSTATE.None;
    this._rtmError = string.Empty;
  }

  public int ConnectTimout
  {
    get
    {
      Hashtable pushSystemInfo = this.PushSystemInfo;
      int result = 0;
      if (pushSystemInfo != null && pushSystemInfo.ContainsKey((object) "connect_timeout") && int.TryParse(pushSystemInfo[(object) "connect_timeout"] as string, out result))
        return result;
      result = this.PushSystemTimeout / 1000;
      return result;
    }
  }

  public int PushSystemTimeout
  {
    get
    {
      Hashtable pushSystemInfo = this.PushSystemInfo;
      int result = 0;
      if (pushSystemInfo != null && pushSystemInfo.ContainsKey((object) "timeout") && int.TryParse(pushSystemInfo[(object) "timeout"] as string, out result))
        return result;
      return 30000;
    }
  }

  public int PushSystemPingTimeout
  {
    get
    {
      Hashtable pushSystemInfo = this.PushSystemInfo;
      int result = 0;
      if (pushSystemInfo != null && pushSystemInfo.ContainsKey((object) "ping") && int.TryParse(pushSystemInfo[(object) "ping"] as string, out result))
        return result;
      return 25000;
    }
  }

  public string BundlesVersion
  {
    get
    {
      if (string.IsNullOrEmpty(this._bundlesVersion) && this._manifest != null && this._manifest.ContainsKey((object) "app_ver"))
      {
        Hashtable hashtable = this._manifest[(object) "app_ver"] as Hashtable;
        if (hashtable != null && hashtable.ContainsKey((object) "bundle_version") && hashtable[(object) "bundle_version"] != null)
          this._bundlesVersion = hashtable[(object) "bundle_version"].ToString();
      }
      return this._bundlesVersion;
    }
  }

  public string BundlesVersionR
  {
    get
    {
      if (string.IsNullOrEmpty(this._bundlesVersionR) && this._manifest != null && this._manifest.ContainsKey((object) "app_ver"))
      {
        Hashtable hashtable = this._manifest[(object) "app_ver"] as Hashtable;
        if (hashtable != null && hashtable.ContainsKey((object) "bundle_version_r") && hashtable[(object) "bundle_version_r"] != null)
          this._bundlesVersionR = hashtable[(object) "bundle_version_r"].ToString();
      }
      return this._bundlesVersionR;
    }
  }

  public string BundlesVersionG
  {
    get
    {
      if (string.IsNullOrEmpty(this._bundlesVersionG) && this._manifest != null && this._manifest.ContainsKey((object) "app_ver"))
      {
        Hashtable hashtable = this._manifest[(object) "app_ver"] as Hashtable;
        if (hashtable != null && hashtable.ContainsKey((object) "bundle_version_g") && hashtable[(object) "bundle_version_g"] != null)
          this._bundlesVersionG = hashtable[(object) "bundle_version_g"].ToString();
      }
      return this._bundlesVersionG;
    }
  }

  public string ConfigFilesVersion
  {
    get
    {
      if (string.IsNullOrEmpty(this._configFilesVersion) && this._manifest != null && this._manifest.ContainsKey((object) "app_ver"))
      {
        Hashtable hashtable = this._manifest[(object) "app_ver"] as Hashtable;
        if (hashtable != null && hashtable.ContainsKey((object) "config_version"))
          this._configFilesVersion = hashtable[(object) "config_version"].ToString();
      }
      return this._configFilesVersion;
    }
  }

  public NetApi.AppVersioState State
  {
    get
    {
      if (this._state == NetApi.AppVersioState.None && this._manifest != null && this._manifest.ContainsKey((object) "app_ver"))
      {
        Hashtable hashtable = this._manifest[(object) "app_ver"] as Hashtable;
        if (hashtable != null && hashtable.ContainsKey((object) "state"))
          this._state = (NetApi.AppVersioState) int.Parse(hashtable[(object) "state"].ToString());
      }
      return this._state;
    }
  }

  public NetApi.OfficialUpdateState NeedOfficialUpdate
  {
    get
    {
      if (this._manifest != null && this._manifest.ContainsKey((object) "update"))
      {
        int result = 0;
        int.TryParse(this._manifest[(object) "update"].ToString(), out result);
        this._needOfficialUpdate = (NetApi.OfficialUpdateState) result;
      }
      return this._needOfficialUpdate;
    }
  }

  public string OfficialUpdateURL
  {
    get
    {
      if (string.IsNullOrEmpty(this._officialUpgradeUrl) && this._manifest != null && this._manifest.ContainsKey((object) "app_ver"))
      {
        Hashtable hashtable = this._manifest[(object) "app_ver"] as Hashtable;
        if (hashtable != null && hashtable.ContainsKey((object) "official_url") && hashtable[(object) "official_url"] != null)
          this._officialUpgradeUrl = hashtable[(object) "official_url"].ToString();
      }
      return this._officialUpgradeUrl;
    }
  }

  public object HotfixBundleContent
  {
    get
    {
      if (this._hotfixBundleContent == null && this._manifest != null && this._manifest.ContainsKey((object) "bundle_dict"))
        this._hotfixBundleContent = this._manifest[(object) "bundle_dict"];
      return this._hotfixBundleContent;
    }
  }

  public object HotfixAssetContent
  {
    get
    {
      if (this._hotfixAssetContent == null && this._manifest != null && this._manifest.ContainsKey((object) "asset_dict"))
        this._hotfixAssetContent = this._manifest[(object) "asset_dict"];
      return this._hotfixAssetContent;
    }
  }

  public string PZTest
  {
    get
    {
      return this._PZTest;
    }
    set
    {
      this._PZTest = value;
    }
  }

  public string UpdateURL
  {
    get
    {
      if (string.IsNullOrEmpty(this._upgradeUrl) && this._manifest != null && this._manifest.ContainsKey((object) "app_ver"))
      {
        Hashtable hashtable = this._manifest[(object) "app_ver"] as Hashtable;
        if (hashtable != null && hashtable.ContainsKey((object) "update_url"))
          this._upgradeUrl = hashtable[(object) "update_url"].ToString();
      }
      return this._upgradeUrl;
    }
  }

  public ArrayList ProxyAddrs
  {
    get
    {
      if (this._manifest != null)
        return this._manifest[(object) "proxy_ip"] as ArrayList;
      return (ArrayList) null;
    }
  }

  public ArrayList AssetsAddrs
  {
    get
    {
      if (this._manifest != null)
        return this._manifest[(object) "cdn"] as ArrayList;
      return (ArrayList) null;
    }
  }

  public string ImType
  {
    get
    {
      if (this._manifest != null)
        return this._manifest[(object) "im"] as string;
      return (string) null;
    }
  }

  public bool Encrypted
  {
    get
    {
      if (this._manifest == null || !this._manifest.ContainsKey((object) "en"))
        return false;
      return (bool) this._manifest[(object) "en"];
    }
  }

  public Dictionary<string, string> Headers
  {
    get
    {
      if (this._headers == null)
      {
        this._headers = new Dictionary<string, string>();
        this._headers["Content-Type"] = "application/json";
        this._headers["Accept-Encoding"] = "gzip";
        this._headers["Pragma"] = "no-cache";
        this._headers["Cache-Control"] = "no-cache";
        this._headers["Expires"] = "0";
        this._headers["Connection"] = "Keep-Alive";
        this._headers["GDataVer"] = "v1";
      }
      return this._headers;
    }
  }

  public Dictionary<string, string> EncryptHeaders
  {
    get
    {
      if (this._encryptHeaders == null)
      {
        this._encryptHeaders = new Dictionary<string, string>();
        this._encryptHeaders["Content-Type"] = "application/octet-stream";
        this._encryptHeaders["Accept-Encoding"] = "gzip";
        this._encryptHeaders["Pragma"] = "no-cache";
        this._encryptHeaders["Cache-Control"] = "no-cache";
        this._encryptHeaders["Expires"] = "0";
        this._encryptHeaders["Connection"] = "Keep-Alive";
        this._encryptHeaders["GDataVer"] = "v1";
      }
      return this._encryptHeaders;
    }
  }

  public Dictionary<string, string> ConfigHeaders
  {
    get
    {
      if (this._configHeaders == null)
      {
        this._configHeaders = new Dictionary<string, string>();
        this._configHeaders["Content-Type"] = "application/json";
        this._configHeaders["Accept-Encoding"] = "gzip";
      }
      return this._configHeaders;
    }
  }

  public string AssetForm
  {
    get
    {
      return "androidhd";
    }
  }

  public string Server_Ip
  {
    get
    {
      if (!PlayerPrefsEx.HasKey("server_ip") || this.MatchCN())
        return (string) null;
      if (string.IsNullOrEmpty(this._server_ip))
      {
        string[] strArray = PlayerPrefsEx.GetString("server_ip").Split('|');
        int index = UnityEngine.Random.Range(0, strArray.Length - 1);
        this._server_ip = strArray[index];
      }
      return this._server_ip;
    }
  }

  public bool IsUseServerIP()
  {
    return this._server_ip != null && NetApi.DefaultServerURL.Contains(this._server_ip);
  }

  public string HostIp
  {
    get
    {
      if (string.IsNullOrEmpty(this._ip))
        this._ip = this.GetIp();
      return this._ip;
    }
  }

  public string MacAddress
  {
    get
    {
      if (string.IsNullOrEmpty(this._mac))
        this._mac = this.GetMac();
      return this._mac;
    }
  }

  public string GetIp()
  {
    return string.Empty;
  }

  public string GetMac()
  {
    return string.Empty;
  }

  public static string GetServerToolUrl(string arg)
  {
    Debug.Log((object) ("GetServerToolUrl --------- " + arg));
    string key = arg;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (NetApi.\u003C\u003Ef__switch\u0024map95 == null)
      {
        // ISSUE: reference to a compiler-generated field
        NetApi.\u003C\u003Ef__switch\u0024map95 = new Dictionary<string, int>(5)
        {
          {
            "SERVER_DW_DEV",
            0
          },
          {
            "SERVER_DW_DEVTEST",
            1
          },
          {
            "SERVER_DW_GLOBAL",
            2
          },
          {
            "SERVER_DW_CHINA",
            3
          },
          {
            "SERVER_DW_WONDER_WAR",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (NetApi.\u003C\u003Ef__switch\u0024map95.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return "http://koa-dev.kingsgroup.cc:8080/";
          case 1:
            return "http://koa-release.kingsgroup.cc:8080/";
          case 2:
            return "http://ec2-52-6-228-115.compute-1.amazonaws.com:8080/";
          case 3:
            return "http://ec2-52-6-228-115.compute-1.amazonaws.com:8080/";
          case 4:
            return "http://ec2-50-17-168-96.compute-1.amazonaws.com:8080/";
        }
      }
    }
    return "http://koa-dev.kingsgroup.cc:8080/";
  }

  public static string GetServerConfigUrl(string arg)
  {
    Debug.Log((object) ("GetServerConfigUrl --------- " + arg));
    string key = arg;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (NetApi.\u003C\u003Ef__switch\u0024map96 == null)
      {
        // ISSUE: reference to a compiler-generated field
        NetApi.\u003C\u003Ef__switch\u0024map96 = new Dictionary<string, int>(4)
        {
          {
            "SERVER_DW_DEV",
            0
          },
          {
            "SERVER_DW_DEVTEST",
            1
          },
          {
            "SERVER_DW_GLOBAL",
            2
          },
          {
            "SERVER_DW_CHINA",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (NetApi.\u003C\u003Ef__switch\u0024map96.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return "http://koa-dev.kingsgroup.cc:8080/data.php";
          case 1:
            return "http://koa-release.kingsgroup.cc:8080/data.php";
          case 2:
            return "http://dw-us-gm.funplus.io:555/data_online.php";
          case 3:
            return "http://dw-us-gm.funplus.io:555/data_online.php";
        }
      }
    }
    return "http://dw-us-gm.funplus.io:555/data_online.php";
  }

  public enum RTMSTATE
  {
    None,
    InConnection,
    DispatchError,
    ConnectionSuccess,
    ConnectionFail,
    InAuth,
    AuthFail,
    Success,
  }

  public enum AppVersioState
  {
    NotSupported = -1,
    Update = 0,
    Normal = 1,
    None = 999, // 0x000003E7
  }

  public enum OfficialUpdateState
  {
    Close,
    Normal,
    Open,
  }
}
