﻿// Decompiled with JetBrains decompiler
// Type: LoopScrollView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (UIGrid))]
public class LoopScrollView : MonoBehaviour
{
  private List<UIWidget> _itemList = new List<UIWidget>();
  private Vector4 _posParam;
  private Transform _cachedTransform;

  private void Awake()
  {
    this._cachedTransform = this.transform;
    UIPanel componentInParent1 = this.transform.GetComponentInParent<UIPanel>();
    if ((bool) ((Object) componentInParent1))
      componentInParent1.clipping = UIDrawCall.Clipping.SoftClip;
    UIScrollView componentInParent2 = this.transform.GetComponentInParent<UIScrollView>();
    if ((bool) ((Object) componentInParent2))
      componentInParent2.restrictWithinPanel = true;
    UIGrid component = this.GetComponent<UIGrid>();
    this._posParam = new Vector4(component.cellWidth, component.cellHeight, component.arrangement != UIGrid.Arrangement.Horizontal ? 0.0f : 1f, component.arrangement != UIGrid.Arrangement.Vertical ? 0.0f : 1f);
  }

  private void Start()
  {
    this.CheckChildCount();
  }

  private void LateUpdate()
  {
    if (this._itemList.Count > 1)
    {
      int index1 = -1;
      int index2 = -1;
      int num = 0;
      bool isVisible1 = this._itemList[0].isVisible;
      bool isVisible2 = this._itemList[this._itemList.Count - 1].isVisible;
      if (isVisible1 == isVisible2)
        return;
      if (isVisible1)
      {
        index1 = this._itemList.Count - 1;
        index2 = 0;
        num = -1;
      }
      else if (isVisible2)
      {
        index1 = 0;
        index2 = this._itemList.Count - 1;
        num = 1;
      }
      if (index1 <= -1)
        return;
      UIWidget uiWidget = this._itemList[index1];
      Vector3 vector3 = new Vector3((float) num * this._posParam.x * this._posParam.z, (float) num * this._posParam.y * this._posParam.w, 0.0f);
      uiWidget.cachedTransform.localPosition = this._itemList[index2].cachedTransform.localPosition + vector3;
      this._itemList.RemoveAt(index1);
      this._itemList.Insert(index2, uiWidget);
    }
    else
      this.CheckChildCount();
  }

  private void CheckChildCount()
  {
    if (this._itemList.Count == this._cachedTransform.childCount)
      return;
    this._itemList.Clear();
    for (int index = 0; index < this._cachedTransform.childCount; ++index)
      this._itemList.Add(this._cachedTransform.GetChild(index).GetComponent<UIWidget>());
  }
}
