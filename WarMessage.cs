﻿// Decompiled with JetBrains decompiler
// Type: WarMessage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;

public class WarMessage : AbstractMailEntry
{
  public string Cordinate()
  {
    if (this.body != null && this.body.ContainsKey((object) "k"))
      return MapUtils.GetCoordinateString(this.body[(object) "k"].ToString(), this.body[(object) "x"].ToString(), this.body[(object) "y"].ToString());
    if (this.body == null || !this.body.ContainsKey((object) "default") || !(this.body[(object) "default"] as Hashtable).ContainsKey((object) "k"))
      return string.Empty;
    Hashtable hashtable = this.body[(object) "default"] as Hashtable;
    return MapUtils.GetCoordinateString(hashtable[(object) "k"].ToString(), hashtable[(object) "x"].ToString(), hashtable[(object) "y"].ToString());
  }

  public int k
  {
    get
    {
      int result = 0;
      int.TryParse(this.body[(object) nameof (k)].ToString(), out result);
      return result;
    }
  }

  public int x
  {
    get
    {
      int result = 0;
      int.TryParse(this.body[(object) nameof (x)].ToString(), out result);
      return result;
    }
  }

  public int y
  {
    get
    {
      int result = 0;
      int.TryParse(this.body[(object) nameof (y)].ToString(), out result);
      return result;
    }
  }

  public string content
  {
    get
    {
      if (this.body == null)
        return "Loading...";
      return this.body[(object) "text"] as string;
    }
    set
    {
      if (!this.body.ContainsKey((object) "text"))
        this.body.Add((object) "text", (object) string.Empty);
      this.body[(object) "text"] = (object) value;
      if (this.mailUpdated == null)
        return;
      this.mailUpdated();
    }
  }

  public string Portrait
  {
    get
    {
      if (this.body.ContainsKey((object) "portrait"))
        return this.body[(object) "portrait"].ToString();
      return string.Empty;
    }
  }

  public string Acronym
  {
    get
    {
      if (this.body.ContainsKey((object) "acronym"))
        return this.body[(object) "acronym"].ToString();
      return string.Empty;
    }
  }

  public string Name
  {
    get
    {
      if (this.body.ContainsKey((object) "name"))
        return this.body[(object) "name"].ToString();
      return string.Empty;
    }
  }

  public string Power
  {
    get
    {
      if (this.body.ContainsKey((object) "power"))
        return this.body[(object) "power"].ToString();
      return string.Empty;
    }
  }

  public bool Won
  {
    get
    {
      long result = -1;
      long.TryParse(this.body[(object) "winner_uid"] as string, out result);
      if (result != -1L)
        return result == PlayerData.inst.uid;
      return false;
    }
  }

  public bool IsAttacker
  {
    get
    {
      Hashtable hashtable = this.body[(object) "attacker"] as Hashtable;
      if (hashtable != null)
      {
        long result = -1;
        if (long.TryParse(hashtable[(object) "uid"] as string, out result))
          return result == PlayerData.inst.uid;
      }
      return false;
    }
  }

  public string OpponentName
  {
    get
    {
      if (this.IsAttacker)
      {
        Hashtable hashtable = this.body[(object) "defender"] as Hashtable;
        if (hashtable != null)
          return hashtable[(object) "name"] as string;
      }
      else
      {
        Hashtable hashtable = this.body[(object) "attacker"] as Hashtable;
        if (hashtable != null)
          return hashtable[(object) "name"] as string;
      }
      return string.Empty;
    }
  }

  public string OpponentAllianceName
  {
    get
    {
      if (this.IsAttacker)
      {
        Hashtable hashtable1 = this.body[(object) "defender"] as Hashtable;
        if (hashtable1 != null)
        {
          Hashtable hashtable2 = hashtable1[(object) "alliance"] as Hashtable;
          if (hashtable2 == null)
            return (string) null;
          return hashtable2[(object) "name"].ToString();
        }
      }
      else
      {
        Hashtable hashtable1 = this.body[(object) "attacker"] as Hashtable;
        if (hashtable1 != null)
        {
          Hashtable hashtable2 = hashtable1[(object) "alliance"] as Hashtable;
          if (hashtable2 == null)
            return (string) null;
          return hashtable2[(object) "name"].ToString();
        }
      }
      return (string) null;
    }
  }

  public string OpponentAllianceTag
  {
    get
    {
      if (this.IsAttacker)
      {
        Hashtable hashtable1 = this.body[(object) "defender"] as Hashtable;
        if (hashtable1 != null)
        {
          Hashtable hashtable2 = hashtable1[(object) "alliance"] as Hashtable;
          if (hashtable2 == null)
            return (string) null;
          return hashtable2[(object) "acronym"].ToString();
        }
      }
      else
      {
        Hashtable hashtable1 = this.body[(object) "attacker"] as Hashtable;
        if (hashtable1 != null)
        {
          Hashtable hashtable2 = hashtable1[(object) "alliance"] as Hashtable;
          if (hashtable2 == null)
            return (string) null;
          return hashtable2[(object) "acronym"].ToString();
        }
      }
      return (string) null;
    }
  }

  public int UserPowerChange
  {
    get
    {
      Hashtable hashtable = !this.IsAttacker ? this.body[(object) "defender"] as Hashtable : this.body[(object) "attacker"] as Hashtable;
      int result1 = 0;
      if (hashtable != null && hashtable.ContainsKey((object) "start_power"))
        int.TryParse(hashtable[(object) "start_power"].ToString(), out result1);
      int result2 = 0;
      if (hashtable != null && hashtable.ContainsKey((object) "end_power"))
        int.TryParse(hashtable[(object) "end_power"].ToString(), out result2);
      return result2 - result1;
    }
  }

  public int OpponentPowerChange
  {
    get
    {
      Hashtable hashtable = !this.IsAttacker ? this.body[(object) "attacker"] as Hashtable : this.body[(object) "defender"] as Hashtable;
      IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
      do
        ;
      while (enumerator.MoveNext());
      int result1 = 0;
      if (hashtable != null && hashtable.ContainsKey((object) "start_power"))
        int.TryParse(hashtable[(object) "start_power"].ToString(), out result1);
      int result2 = 0;
      if (hashtable != null && hashtable.ContainsKey((object) "end_power"))
        int.TryParse(hashtable[(object) "end_power"].ToString(), out result2);
      return result2 - result1;
    }
  }

  public Dictionary<string, long> Resource()
  {
    Dictionary<string, long> dictionary = new Dictionary<string, long>();
    if (this.body.ContainsKey((object) "resource"))
    {
      Hashtable hashtable = this.body[(object) "resource"] as Hashtable;
      if (hashtable.ContainsKey((object) "food"))
        dictionary.Add("food", long.Parse(hashtable[(object) "food"].ToString()));
      if (hashtable.ContainsKey((object) "silver"))
        dictionary.Add("silver", long.Parse(hashtable[(object) "silver"].ToString()));
      if (hashtable.ContainsKey((object) "ore"))
        dictionary.Add("ore", long.Parse(hashtable[(object) "ore"].ToString()));
      if (hashtable.ContainsKey((object) "wood"))
        dictionary.Add("wood", long.Parse(hashtable[(object) "wood"].ToString()));
    }
    return dictionary;
  }

  public override void Display()
  {
    MailType type = this.type;
    switch (type)
    {
      case MailType.MAIL_TYPE_PVP_BATTLE_REPORT:
      case MailType.MAIL_TYPE_RALLY_BATTLE_REPORT:
      case MailType.MAIL_TYPE_MERLIN_TOWER_GATHER_DEFEND_LOSE:
        this.OpenWarReport();
        break;
      case MailType.MAIL_TYPE_PVP_SCOUT_REPORT:
        this.OpenScoutReport();
        break;
      case MailType.MAIL_TYPE_PVP_BE_SCOUTED_REPORT:
      case MailType.MAIL_TYPE_PVP_BE_RALLIED:
      case MailType.MAIL_TYPE_PVP_RALLY_CANCEL:
      case MailType.MAIL_TYPE_PVP_ENCAMP_BACK:
      case MailType.MAIL_TYPE_ALLIANCE_REINFORCE_ARRIVED:
      case MailType.MAIL_TYPE_ALLIANCE_CANCEL_RALLY:
      case MailType.MAIL_TYPE_ALLIANCE_GVE_CANCEL_RALLY:
      case MailType.MAIL_TYPE_PVE_MISS_TARGET:
      case MailType.MAIL_TYPE_GVE_MISS_TARGET:
      case MailType.MAIL_TYPE_VISIT_NPC_MISS_TARGET:
        this.OpenOtherReport();
        break;
      case MailType.MAIL_TYPE_PVE_BATTLE_REPORT:
        this.OpenMonsterReport();
        break;
      case MailType.MAIL_TYPE_GATHERING_REPORT:
        this.OpenGatherReport();
        break;
      case MailType.MAIL_TYPE_SYSTEM_FIRST_BLOOD:
        this.OpenFirstBloodReport();
        break;
      case MailType.MAIL_TYPE_ALLIANCE_TRADE_ARRIVED:
        this.OpenAllianceTradeReport();
        break;
      case MailType.MAIL_TYPE_ALLIANCE_REFUSE:
      case MailType.MAIL_TYPE_ALLIANCE_KICKOUT:
        this.OpenAllianceIOReport();
        break;
      case MailType.MAIL_TYPE_ALLIANCE_DONATION:
        this.OpenAllianceDonationReport();
        break;
      case MailType.MAIL_TYPE_ALLIANCE_TELEPORT:
        this.OpenTeleportInviteReport();
        break;
      case MailType.MAIL_TYPE_GVE_BATTLE_REPORT:
        this.OpenGVEReportReport();
        break;
      case MailType.MAIL_TYPE_GVE_REWARDS_REPORT:
        this.OpenGVERewardsReport();
        break;
      case MailType.MAIL_TYPE_SYSTEM_INTEGRAL_REWARD:
      case MailType.MAIL_TYPE_ALLIANCE_FIRST_JOIN:
      case MailType.MAIL_TYPE_ALLIANCE_PAYMENT_PACKAGE:
      case MailType.MAIL_TYPE_SYSTEM_KNIGHT_STAGE_REWARD:
      case MailType.MAIL_TYPE_SYSTEM_KNIGHT_PERSON_RANK:
      case MailType.MAIL_TYPE_SYSTEM_KNIGHT_ALLIANCE_RANK:
      case MailType.MAIL_TYPE_VISIT_NPC_REPORT:
        this.OpenActivityRewardsReport();
        break;
      case MailType.MAIL_TYPE_ALLIANCE_HIGHLORD:
        this.OpenAllianceLordChangeReport();
        break;
      case MailType.MAIL_TYPE_SYSTEM_FRESHER_ITEMS:
      case MailType.MAIL_TYPE_SYSTEM_VIP_TRY:
        this.OpenFresherRewardsReport();
        break;
      case MailType.MAIL_TYPE_DISASTROUS_REPORT:
        this.OpenDisastrousReport();
        break;
      case MailType.MAIL_TYPE_ALLIANCE_SYS_INVITE_TELEPORT:
        this.OpenSystemInviteTeleport();
        break;
      case MailType.MAIL_TYPE_KNIGHT_REPORT:
        this.OpenKnightReport();
        break;
      case MailType.MAIL_TYPE_ALLIANCE_HOSPITAL_DEMOLISH:
      case MailType.MAIL_TYPE_ALLIANCE_HOSPITAL_DISAPPEAR:
        this.OpenAllianceHospitalReport();
        break;
      case MailType.MAIL_TYPE_RAB_BATTLE_REPORT:
        this.OpenAllianceBossReport();
        break;
      case MailType.MAIL_TYPE_SYSTEM_ALLIANCE_BOSS_REWARD:
        this.OpenAllianceBossRewardResult();
        break;
      case MailType.MAIL_TYPE_ALLIANCE_BOSS_FINISH:
        this.OpenAllianceBossResult();
        break;
      case MailType.MAIL_TYPE_DRAGON_ALTER_RESULT:
        this.OpenDragonAlterResult();
        break;
      case MailType.MAIL_TYPE_SYSTEM_PAYMENT_PACKAGE_SUCCESS:
      case MailType.MAIL_TYPE_SYSTEM_PAYMENT_GOLD_SUCCESS:
      case MailType.MAIL_TYPE_MERLIN_TRIALS_MYSTERY_SHOP_BUY:
        this.OpenIAPSuccessReport();
        break;
      case MailType.MAIL_TYPE_SYSTEM_EXCALIBUR_RECEIVE:
      case MailType.MAIL_TYPE_SYSTEM_EXCALIBUR_DISAPPEAR:
        this.OpenExcaliburReport();
        break;
      case MailType.MAIL_TYPE_FESTIVAL_EVENT_REPORT:
        this.OpenFestivalBossReport();
        break;
      case MailType.MAIL_TYPE_KINGDOM_BOSS_BATTLE_REPORT:
        this.OpenKingdomBossReport();
        break;
      case MailType.MAIL_TYPE_SYSTEM_AUCTION_NORMAL:
      case MailType.MAIL_TYPE_SYSTEM_AUCTION_HIDDEN:
        this.OpenAuctionRewardsReport();
        break;
      case MailType.MAIL_TYPE_SYSTEM_ARTIFACT_DROP:
        this.OpenArtifactReport();
        break;
      case MailType.MAIL_TYPE_DIG_TREASURE_COMPLETE:
        this.OpenDigReport();
        break;
      case MailType.MAIL_TYPE_SYSTEM_PAYMENT_DAILY_PACKAGE_WITH_SUBSCRIPTION_SUCCESS:
      case MailType.MAIL_TYPE_SYSTEM_PAYMENT_DAILY_PACKAGE_NO_SUBSCRIPTION_SUCCESS:
        this.OenIAPDailySuccessReport();
        break;
      case MailType.MAIL_TYPE_SYSTEM_TRADING_HALL_NOTIFY:
        this.OpenTradingHallReport();
        break;
      case MailType.MAIL_TYPE_SYSTEM_TRADING_HALL_EXPIRE:
        this.OnTradingHallExpiredReport();
        break;
      case MailType.MAIL_TYPE_MERLIN_TOWER_GATHER:
        this.OpenMerlinTowerGatherReport();
        break;
      default:
        if (type != MailType.MAIL_TYPE_ANNI_BATTLE_REPORT)
        {
          this.OpenOtherReport();
          break;
        }
        goto case MailType.MAIL_TYPE_PVP_BATTLE_REPORT;
    }
    base.Display();
  }

  private void OpenTradingHallReport()
  {
    UIManager.inst.OpenPopup("TradingHall/TradingHallMailPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OnTradingHallExpiredReport()
  {
    UIManager.inst.OpenPopup("TradingHall/TradingHallExpiredMailPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenAuctionRewardsReport()
  {
    UIManager.inst.OpenPopup("Activity/AuctionRewardsPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenArtifactReport()
  {
    UIManager.inst.OpenPopup("Report/AritfactReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenExcaliburReport()
  {
    UIManager.inst.OpenPopup("Report/ExcaliburReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenIAPSuccessReport()
  {
    UIManager.inst.OpenPopup("Mail/IAPSuccessPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OenIAPDailySuccessReport()
  {
    UIManager.inst.OpenPopup("Mail/IAPDailySuccessPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenFresherRewardsReport()
  {
    UIManager.inst.OpenPopup("Activity/FresherRewardsPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenAllianceLordChangeReport()
  {
    UIManager.inst.OpenPopup("AllianceMail/AllianceLordChangePopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenAllianceTradeReport()
  {
    UIManager.inst.OpenPopup("AllianceMail/AllianceTradePopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenActivityRewardsReport()
  {
    UIManager.inst.OpenPopup("Activity/ActivityRewardsPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenFestivalBossReport()
  {
    UIManager.inst.OpenPopup("Report/FestivalBossReport", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenAllianceDonationReport()
  {
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_donate_min_level");
    if (data != null)
    {
      int valueInt = data.ValueInt;
      if (valueInt > CityManager.inst.mStronghold.mLevel)
      {
        UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_item_requirement_tip", new Dictionary<string, string>()
        {
          {
            "num",
            valueInt.ToString()
          }
        }, true), (System.Action) null, 4f, false);
        return;
      }
    }
    UIManager.inst.OpenPopup("AllianceMail/AllianceDonationPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenFirstBloodReport()
  {
    UIManager.inst.OpenPopup("Report/FirstKillReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenGVEReportReport()
  {
    UIManager.inst.OpenPopup("Report/GVEWarReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenGVERewardsReport()
  {
    UIManager.inst.OpenPopup("Report/GVERewardsPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenTeleportInviteReport()
  {
    UIManager.inst.OpenPopup("AllianceMail/TeleportInvitePopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenAllianceIOReport()
  {
    UIManager.inst.OpenPopup("AllianceMail/AllianceIOPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenSystemInviteTeleport()
  {
    UIManager.inst.OpenPopup("AllianceMail/SystemTeleportInvitePopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenMonsterReport()
  {
    UIManager.inst.OpenPopup("Report/MonsterReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenDigReport()
  {
    Hashtable hashtable = new Hashtable();
    hashtable.Add((object) "data", this.data[(object) "data"]);
    UIManager inst = UIManager.inst;
    string popupType = "Report/TreasureMapReportPopup";
    TreasureMapReportPopup.Parameter parameter1 = new TreasureMapReportPopup.Parameter();
    parameter1.hashtable = hashtable;
    parameter1.mail = (AbstractMailEntry) this;
    TreasureMapReportPopup.Parameter parameter2 = parameter1;
    inst.OpenPopup(popupType, (Popup.PopupParameter) parameter2);
  }

  private void OpenScoutReport()
  {
    UIManager inst = UIManager.inst;
    string popupType = "ScoutReportPopup";
    ScoutReportPopup.Parameter parameter1 = new ScoutReportPopup.Parameter();
    parameter1.hashtable = this.data[(object) "data"] as Hashtable;
    parameter1.mail = (AbstractMailEntry) this;
    ScoutReportPopup.Parameter parameter2 = parameter1;
    inst.OpenPopup(popupType, (Popup.PopupParameter) parameter2);
  }

  private void OpenAllianceHospitalReport()
  {
    UIManager inst = UIManager.inst;
    string popupType = "AllianceHospitalMailPopup";
    ScoutReportPopup.Parameter parameter1 = new ScoutReportPopup.Parameter();
    parameter1.hashtable = this.data[(object) "data"] as Hashtable;
    parameter1.mail = (AbstractMailEntry) this;
    ScoutReportPopup.Parameter parameter2 = parameter1;
    inst.OpenPopup(popupType, (Popup.PopupParameter) parameter2);
  }

  private void OpenKingdomBossReport()
  {
    UIManager.inst.OpenPopup("Report/KingdomBossReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = (this.data[(object) "data"] as Hashtable),
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenAllianceBossReport()
  {
    UIManager.inst.OpenPopup("Report/AllianceBossReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = (this.data[(object) "data"] as Hashtable),
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenAllianceBossResult()
  {
    UIManager.inst.OpenPopup("Report/AllianceBossResultReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = (this.data[(object) "data"] as Hashtable),
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenAllianceBossRewardResult()
  {
    UIManager.inst.OpenPopup("Report/AllianceBossRewardsMailPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = (this.data[(object) "data"] as Hashtable),
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenDragonAlterResult()
  {
    UIManager.inst.OpenPopup("Report/DragonAltarReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = (this.data[(object) "data"] as Hashtable),
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenWarReport()
  {
    UIManager.inst.OpenPopup("Report/WarReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenKnightReport()
  {
    UIManager.inst.OpenPopup("FallenKnightReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenDisastrousReport()
  {
    UIManager.inst.OpenPopup("Report/DisastrousReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = new Hashtable()
      {
        {
          (object) "data",
          this.data[(object) "data"]
        }
      },
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenOtherReport()
  {
    UIManager.inst.OpenPopup("Mail/OtherReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = (this.data[(object) "data"] as Hashtable),
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenGatherReport()
  {
    UIManager.inst.OpenPopup("Mail/GatherReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = this.body,
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenMerlinTowerGatherReport()
  {
    UIManager.inst.OpenPopup("Mail/MerlinTowerGatherReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
    {
      hashtable = this.body,
      mail = (AbstractMailEntry) this
    });
  }

  private void OpenReinforceReport()
  {
  }

  private void OpenRallyReport()
  {
  }
}
