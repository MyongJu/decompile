﻿// Decompiled with JetBrains decompiler
// Type: RoundPlayerAttribute
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public abstract class RoundPlayerAttribute
{
  private List<int> _changeValues = new List<int>();
  private List<int> _orginValues = new List<int>();
  private int _baseValue;
  private int _currentValue;
  private int _maxValue;
  private RoundPlayer _owner;

  protected RoundPlayerAttribute(RoundPlayer owner)
  {
    this._owner = owner;
  }

  protected RoundPlayer Owner
  {
    get
    {
      return this._owner;
    }
  }

  public int TotalHeal
  {
    get
    {
      int num = 0;
      for (int index = 0; index < this._orginValues.Count; ++index)
      {
        if (this._orginValues[index] > 0)
          num += this._orginValues[index];
      }
      return num;
    }
  }

  public int TotalHurt
  {
    get
    {
      int num = 0;
      for (int index = 0; index < this._orginValues.Count; ++index)
      {
        if (this._orginValues[index] < 0)
          num += this._orginValues[index];
      }
      return Mathf.Abs(num);
    }
  }

  protected int BaseValue
  {
    get
    {
      return this._baseValue;
    }
  }

  protected virtual int InBattleBaseValue
  {
    get
    {
      List<BattleBuff> allBuffs = this.Owner.GetAllBuffs();
      bool isEvil = this.Owner.PlayerCamp == RoundPlayer.Camp.Evil;
      DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(this.Owner.Uid);
      return AttributeCalcHelper.Instance.CalcAttributeInBattleFromOutBattle((float) this.OutBattleBaseValue, this.Type, isEvil, allBuffs, dragonKnightData);
    }
  }

  public virtual int OutBattleBaseValue
  {
    get
    {
      return this.BaseValue;
    }
  }

  public void Setup(int baseValue, int maxValue = 0)
  {
    this._baseValue = baseValue;
    this._maxValue = 0;
    this.Init();
  }

  protected virtual DragonKnightAttibute.Type Type
  {
    get
    {
      return DragonKnightAttibute.Type.None;
    }
  }

  protected void Init()
  {
    this.Reset();
  }

  public void AddValue(int value)
  {
    this._orginValues.Add(value);
    if (this.CurrentValue + value > this.MaxValue)
      value = this.MaxValue - this.CurrentValue;
    this._changeValues.Add(value);
  }

  public void SetCurrentValue(int value)
  {
    if (value <= 0)
      return;
    this._currentValue = value;
    if (this._currentValue <= this._maxValue)
      return;
    this._currentValue = this._maxValue;
  }

  public float Progress
  {
    get
    {
      return (float) this._currentValue / (float) this._maxValue;
    }
  }

  public int CurrentValue
  {
    get
    {
      int currentValue = this._currentValue;
      for (int index = 0; index < this._changeValues.Count; ++index)
        currentValue += this._changeValues[index];
      if (currentValue < 0)
        return 0;
      if (this.AutoFix && currentValue > this.MaxValue)
        return this.MaxValue;
      return currentValue;
    }
    protected set
    {
      this._currentValue = value;
    }
  }

  protected virtual bool AutoFix
  {
    get
    {
      return false;
    }
  }

  public int MaxValue
  {
    get
    {
      return this._maxValue;
    }
    set
    {
      this._maxValue = value;
    }
  }

  public virtual void Reset()
  {
  }

  protected float GetOutBattleAttibutePlus(float baseValue, DragonKnightAttibute.Type type)
  {
    if ((double) baseValue == 0.0)
      return 0.0f;
    bool isEvil = this._owner.PlayerCamp == RoundPlayer.Camp.Evil;
    List<BattleBuff> allBuffs = this._owner.GetAllBuffs();
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(this.Owner.Uid);
    int num1 = AttributeCalcHelper.Instance.CalcAttributeOutBattle(baseValue, type, isEvil, allBuffs, dragonKnightData);
    float num2 = 0.0f;
    string id = string.Empty;
    switch (type)
    {
      case DragonKnightAttibute.Type.Strong:
        id = "strength";
        break;
      case DragonKnightAttibute.Type.Intelligence:
        id = "intelligent";
        break;
      case DragonKnightAttibute.Type.Constitution:
        id = "constitution";
        break;
      case DragonKnightAttibute.Type.Armor:
        id = "armor";
        break;
    }
    DragonKnightPropertyRateStaticInfo data = ConfigManager.inst.DBDragonKnightPropertyRate.GetData(id);
    if (data != null)
    {
      switch (this.Type)
      {
        case DragonKnightAttibute.Type.Damge:
          num2 = data.Attack;
          break;
        case DragonKnightAttibute.Type.MagiceDamge:
          num2 = data.Magic;
          break;
        case DragonKnightAttibute.Type.Defense:
          num2 = data.Defence;
          break;
        case DragonKnightAttibute.Type.Health:
          num2 = data.Health;
          break;
      }
    }
    return (float) num1 * num2;
  }

  protected float GetInBattleAttibutePlus(float baseValue, DragonKnightAttibute.Type type)
  {
    if ((double) baseValue == 0.0)
      return 0.0f;
    bool isEvil = this._owner.PlayerCamp == RoundPlayer.Camp.Evil;
    List<BattleBuff> allBuffs = this._owner.GetAllBuffs();
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(this.Owner.Uid);
    int num1 = AttributeCalcHelper.Instance.CalcAttributeInBattle(baseValue, type, isEvil, allBuffs, dragonKnightData);
    int num2 = AttributeCalcHelper.Instance.CalcAttributeOutBattle(baseValue, type, isEvil, allBuffs, dragonKnightData);
    float num3 = 0.0f;
    string id = string.Empty;
    switch (type)
    {
      case DragonKnightAttibute.Type.Strong:
        id = "strength";
        break;
      case DragonKnightAttibute.Type.Intelligence:
        id = "intelligent";
        break;
      case DragonKnightAttibute.Type.Constitution:
        id = "constitution";
        break;
      case DragonKnightAttibute.Type.Armor:
        id = "armor";
        break;
    }
    DragonKnightPropertyRateStaticInfo data = ConfigManager.inst.DBDragonKnightPropertyRate.GetData(id);
    if (data != null)
    {
      switch (this.Type)
      {
        case DragonKnightAttibute.Type.Damge:
          num3 = data.Attack;
          break;
        case DragonKnightAttibute.Type.MagiceDamge:
          num3 = data.Magic;
          break;
        case DragonKnightAttibute.Type.Defense:
          num3 = data.Defence;
          break;
        case DragonKnightAttibute.Type.Health:
          num3 = data.Health;
          break;
      }
    }
    return (float) (num1 - num2) * num3;
  }

  protected int AddPercent(float baseValue, DragonKnightAttibute.Type type)
  {
    bool isEvil = this.Owner.PlayerCamp == RoundPlayer.Camp.Evil;
    List<BattleBuff> allBuffs = this.Owner.GetAllBuffs();
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(this.Owner.Uid);
    baseValue = (1f + AttributeCalcHelper.Instance.GetAttributePercent(type, isEvil, allBuffs, dragonKnightData)) * baseValue;
    return Mathf.CeilToInt(baseValue);
  }

  protected int BuffFactor(int result)
  {
    List<BattleBuff> allBuffs = this.Owner.GetAllBuffs();
    for (int index = 0; index < allBuffs.Count; ++index)
    {
      BattleBuff battleBuff = allBuffs[index];
      if (battleBuff.Type == this.Type)
        result += battleBuff.Value;
    }
    return result;
  }
}
