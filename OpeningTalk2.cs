﻿// Decompiled with JetBrains decompiler
// Type: OpeningTalk2
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class OpeningTalk2 : MonoBehaviour
{
  private const string rallypath = "Prefab/OpeningCinematic/OpeningRally";

  public void OnClickHandle()
  {
    if (OpeningCinematicManager.Instance.Resolution == OpeningCinematicManager.ResolutionType.Basic)
      AssetManager.Instance.LoadAsync("Prefab/OpeningCinematic/OpeningRally", (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
      {
        Utils.DuplicateGOB(obj as GameObject, UIManager.inst.ui2DCamera.transform);
        AssetManager.Instance.UnLoadAsset("Prefab/OpeningCinematic/OpeningRally", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      }), (System.Type) null);
    else if (OpeningCinematicManager.Instance.Resolution == OpeningCinematicManager.ResolutionType.NoRally)
      OpeningCinematicManager.Instance.board.PlayAction(2);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }
}
