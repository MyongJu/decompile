﻿// Decompiled with JetBrains decompiler
// Type: UIHideHelperManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class UIHideHelperManager
{
  private List<UIHideHelper> allUIHelpers = new List<UIHideHelper>();
  private List<UIHideHelper> allDeleting = new List<UIHideHelper>();
  private static UIHideHelperManager m_Inst;

  public static UIHideHelperManager Instance
  {
    get
    {
      if (UIHideHelperManager.m_Inst == null)
        UIHideHelperManager.m_Inst = new UIHideHelperManager();
      return UIHideHelperManager.m_Inst;
    }
  }

  public void ShowHudUI()
  {
    this.UpdateList();
    List<UIHideHelper>.Enumerator enumerator = this.allUIHelpers.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Show();
  }

  public void HideHudUI()
  {
    this.UpdateList();
    List<UIHideHelper>.Enumerator enumerator = this.allUIHelpers.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Hide();
  }

  public void AddHelper(UIHideHelper helper)
  {
    this.RemoveHelper(helper);
    this.allUIHelpers.Add(helper);
    helper.NeedRemove = false;
  }

  public void RemoveHelper(UIHideHelper helper)
  {
    this.allDeleting.Add(helper);
    helper.NeedRemove = true;
  }

  public void BackToOriginal()
  {
    this.UpdateList();
    List<UIHideHelper>.Enumerator enumerator = this.allUIHelpers.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.QuickShow();
  }

  public void QuickHide()
  {
    this.UpdateList();
    List<UIHideHelper>.Enumerator enumerator = this.allUIHelpers.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.QuickHide();
  }

  private void UpdateList()
  {
    if (this.allDeleting.Count <= 0)
      return;
    this.allUIHelpers.RemoveAll((Predicate<UIHideHelper>) (cur => cur.NeedRemove));
  }
}
