﻿// Decompiled with JetBrains decompiler
// Type: WonderTowerVFX
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class WonderTowerVFX : MonoBehaviour, IConstructionController
{
  private const string vfx1path = "Prefab/VFX/fx_wonder_attack_new";
  private const string vfx2path = "Prefab/VFX/fx_wonder_attack_fire";
  public RendererSortingLayerIdModifier m_PeaceShield;
  private TileData m_TileData;
  private int towerid;

  public void OnInitialize()
  {
    MessageHub.inst.GetPortByAction("wonder_tower_attack").AddEvent(new System.Action<object>(this.OnWonderTowerAttack));
  }

  public void OnFinalize()
  {
    MessageHub.inst.GetPortByAction("wonder_tower_attack").RemoveEvent(new System.Action<object>(this.OnWonderTowerAttack));
  }

  public void UpdateUI(TileData tile)
  {
    this.m_TileData = tile;
    if (this.m_TileData == null || this.m_TileData.WonderTowerData == null || !(bool) ((UnityEngine.Object) this.m_PeaceShield))
      return;
    this.m_PeaceShield.gameObject.SetActive(this.m_TileData.WonderTowerData.CanShowPeaceShield);
  }

  private void Awake()
  {
    this.OnInitialize();
  }

  private void OnDestroy()
  {
    this.OnFinalize();
  }

  public void Reset()
  {
    if (!(bool) ((UnityEngine.Object) this.m_PeaceShield))
      return;
    this.m_PeaceShield.gameObject.SetActive(false);
  }

  public void SetSortingLayerID(int sortingLayerID)
  {
    if (!(bool) ((UnityEngine.Object) this.m_PeaceShield))
      return;
    this.m_PeaceShield.SortingLayerName = SortingLayer.IDToName(sortingLayerID);
  }

  private void OnWonderTowerAttack(object data)
  {
    if (this.m_TileData == null)
      return;
    Hashtable hashtable = data as Hashtable;
    if (hashtable == null || !hashtable.ContainsKey((object) "tower_id"))
      return;
    int num = int.Parse(hashtable[(object) "tower_id"].ToString());
    if (num != this.m_TileData.ResourceId)
      return;
    this.towerid = num;
    this.PlayAttackEffect();
    if (this.towerid != 2 && this.towerid != 4)
      return;
    Utils.ExecuteInSecs(0.8f, new System.Action(this.PlayAttackEffect));
    Utils.ExecuteInSecs(2f, new System.Action(this.PlayAttackEffect));
  }

  private void PlayAttackEffect()
  {
    if (!this.gameObject.activeSelf)
      return;
    Coordinate wonderLocation = WonderUtils.GetWonderLocation();
    wonderLocation.K = this.m_TileData.Location.K;
    TileData tileAt = PVPMapData.MapData.GetTileAt(wonderLocation);
    if ((UnityEngine.Object) null == (UnityEngine.Object) tileAt.TileView.TileAsset)
      return;
    Vector3 position = tileAt.TileView.TileAsset.transform.position;
    string uri = "Prefab/VFX/fx_wonder_attack_new";
    if (this.towerid == 2 || this.towerid == 4)
    {
      AudioManager.Instance.PlaySound("sfx_kingdom_miniwonder_fire_attack", false);
      uri = "Prefab/VFX/fx_wonder_attack_fire";
    }
    else
      AudioManager.Instance.PlaySound("sfx_kingdom_miniwonder_lightning_attack", false);
    GameObject gameObject = VfxManager.Instance.Get(VfxManager.Instance.CreateAndPlay(uri, this.transform.FindChild("Holo"))).gameObject;
    RendererSortingLayerIdModifier component = gameObject.GetComponent<RendererSortingLayerIdModifier>();
    if ((bool) ((UnityEngine.Object) this.m_PeaceShield))
      component.SortingLayerName = this.m_PeaceShield.SortingLayerName;
    switch (this.towerid)
    {
      case 1:
        gameObject.transform.localEulerAngles = new Vector3(90f, 0.0f, 0.0f);
        break;
      case 2:
        this.Hit(gameObject, position);
        break;
      case 3:
        gameObject.transform.localEulerAngles = new Vector3(-90f, 0.0f, 0.0f);
        break;
      case 4:
        this.Hit(gameObject, position);
        break;
    }
  }

  private void Hit(GameObject go, Vector3 tarpos)
  {
    Hashtable args = iTween.Hash((object) "position", (object) tarpos, (object) "time", (object) 0.6, (object) "easetype", (object) iTween.EaseType.linear, (object) "islocal", (object) false);
    iTween.MoveTo(go, args);
  }
}
