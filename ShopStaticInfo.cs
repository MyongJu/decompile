﻿// Decompiled with JetBrains decompiler
// Type: ShopStaticInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ShopStaticInfo
{
  public const string LOCATION_SHOP = "shop";
  public const string LOCATION_ALLIANCE = "alliance";
  public const string CATEGORY_COMBAT = "combat";
  public const string CATEGORY_RESOURCES = "resources";
  public const string CURRENCY_GOLD = "gold";
  public const string CURRENCY_LOYALTY = "loyalty";
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "itemid")]
  public int Item_InternalId;
  [Config(Name = "location")]
  public string Location;
  [Config(Name = "category")]
  public string Category;
  [Config(Name = "priority")]
  public int Priority;
  [Config(Name = "value")]
  public int Value;
  [Config(Name = "currency_type")]
  public string Currency_Buy_type;
  [Config(Name = "image")]
  public string Image;
  [Config(Name = "price")]
  public int Price;
  [Config(IsTimeSpan = true, Name = "sale_start_time")]
  public long Sale_Start_Time;
  [Config(Name = "discount")]
  public float DisCount;
  [Config(Name = "discount_in_limit_time")]
  public float Discount_In_Limit_Time;
  [Config(IsTimeSpan = true, Name = "limit_discount_start_time")]
  public long Limit_Discount_Start_Time;
  [Config(IsTimeSpan = true, Name = "limit_discount_end_time")]
  public long Limit_Discount_End_Time;
  [Config(Name = "req_id_1")]
  public string Req_Id_1;
  [Config(Name = "req_id_2")]
  public int Req_Id_2;
  [Config(Name = "req_vip_level")]
  public int Req_Vip_Level;
  [Config(Name = "limited_sales_interval")]
  public int Limited_Sales_Interval;
  [Config(Name = "limited_sales_number")]
  public int Limited_Sales_Number;
  [Config(IsTimeSpan = true, Name = "limited_sales_start_time")]
  public long Limited_Sales_Start_Time;
  [Config(IsTimeSpan = true, Name = "limited_sales_end_time")]
  public long Limited_Sales_End_Time;
  [Config(Name = "return_gold")]
  public float Return_Gold;
  [Config(Name = "visible")]
  public int Visible;
  [Config(Name = "max_buy_number")]
  public int MaxBuyCount;

  public string LOC_ID
  {
    get
    {
      return this.ID + "_name";
    }
  }

  public string LOC_Description_ID
  {
    get
    {
      return this.ID + "_description";
    }
  }
}
