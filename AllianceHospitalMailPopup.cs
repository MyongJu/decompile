﻿// Decompiled with JetBrains decompiler
// Type: AllianceHospitalMailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceHospitalMailPopup : BaseReportPopup
{
  public UITexture titleImage;
  public UITable contentTable;
  public GameObject container;
  public UILabel contentLabel;
  public GameObject subTitle;
  public GameObject enemy;
  public GameObject resource;
  public GameObject troopCount;
  public GameObject tier;
  public GameObject reinforceTroop;
  public GameObject research;
  public GameObject talent;
  public GameObject oneLabel;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    BuilderFactory.Instance.Release((UIWidget) this.titleImage);
  }

  public override void OnShareBtnClicked()
  {
    base.OnShareBtnClicked();
    if (!this.canShare)
      return;
    Hashtable hashtable = this.param.hashtable[(object) "default"] as Hashtable;
    string str = (string) null;
    if (hashtable.Contains((object) "acronym"))
      str = hashtable[(object) "acronym"].ToString();
    ChatMessageManager.SendWarReport_Scout(this.maildata.mailID, PlayerData.inst.uid, str + hashtable[(object) "user_name"].ToString());
  }

  private void UpdateUI()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.titleImage, "Texture/GUI_Textures/mail_report_alliance_hospital", (System.Action<bool>) null, false, false, string.Empty);
    this.contentLabel.text = this.maildata.GetBodyString();
    this.DrawTroops();
    this.container.GetComponent<UITable>().Reposition();
  }

  private void DrawEnemyOverView()
  {
    if (!this.param.hashtable.ContainsKey((object) "default"))
      return;
    this.AddSubtitle("mail_scout_report_uppercase_enemy_overview_subtitle");
    Hashtable hashtable = this.param.hashtable[(object) "default"] as Hashtable;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    GameObject gameObject = NGUITools.AddChild(parent, this.enemy);
    DefaultPlayerComponent component = gameObject.transform.Find("enemyUser").GetComponent<DefaultPlayerComponent>();
    if (this.param.hashtable.ContainsKey((object) "lord"))
      component.lv = "Lv." + (this.param.hashtable[(object) "lord"] as Hashtable)[(object) "level"].ToString();
    component.cityDefence = !hashtable.ContainsKey((object) "city_defense") ? string.Empty : Utils.XLAT("id_city_defense") + ": " + hashtable[(object) "city_defense"].ToString();
    component.FeedData<DefaultPlayerComponentData>(this.param.hashtable[(object) "default"] as Hashtable);
    if (this.param.hashtable.ContainsKey((object) "dragon"))
      gameObject.transform.Find("enmeyDragon").GetComponent<DragonWithSkillComponent>().FeedData<DragonWithSkillComponentData>(this.param.hashtable[(object) "dragon"] as Hashtable);
    else
      gameObject.transform.Find("enmeyDragon").gameObject.SetActive(false);
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawResource()
  {
    if (!this.param.hashtable.ContainsKey((object) "resource"))
      return;
    this.AddSubtitle("mail_scout_report_resources_subtitle");
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    NGUITools.AddChild(parent, this.resource).GetComponentInChildren<ResourceWithUnCollectComponent>().FeedData<ResourceWithUnCollectComponentData>(this.param.hashtable[(object) "resource"] as Hashtable);
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawTroops()
  {
    if (!this.param.hashtable.ContainsKey((object) "troops"))
      return;
    this.AddSubtitle("id_uppercase_troops");
    Hashtable hashtable1 = this.param.hashtable[(object) "troops"] as Hashtable;
    this.AddUnitCount(this.contentTable.gameObject, hashtable1[(object) "amount"].ToString(), Utils.XLAT("id_total_troops") + ":");
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    Hashtable hashtable2 = hashtable1[(object) "tier"] as Hashtable;
    if (hashtable2 != null)
    {
      List<string> stringList = new List<string>((IEnumerable<string>) new ArrayList(hashtable2.Keys).ToArray(typeof (string)));
      stringList.Sort((Comparison<string>) ((x, y) => int.Parse(x.Substring(1)).CompareTo(int.Parse(y.Substring(1)))));
      for (int index = stringList.Count - 1; index >= 0; --index)
        NGUITools.AddChild(parent, this.tier).GetComponent<TierListComponent>().FeedData((IComponentData) new TierComponentData(stringList[index].ToString(), hashtable2[(object) stringList[index]] as Hashtable));
    }
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawDefendingTroop(Hashtable data)
  {
  }

  private void DrawTrap(Hashtable data)
  {
    if (!data.ContainsKey((object) "traps"))
      return;
    this.AddSubtitle("mail_scout_report_uppercase_traps_subtitle");
    Hashtable hashtable1 = data[(object) "traps"] as Hashtable;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    this.AddUnitCount(parent, hashtable1[(object) "amount"].ToString(), Utils.XLAT("mail_scout_report_total_traps"));
    Hashtable hashtable2 = hashtable1[(object) "tier"] as Hashtable;
    if (hashtable2 != null)
    {
      ArrayList arrayList = new ArrayList(hashtable2.Keys);
      List<string> stringList = new List<string>((IEnumerable<string>) arrayList.ToArray(typeof (string)));
      stringList.Sort((Comparison<string>) ((x, y) => int.Parse(x.Substring(1)).CompareTo(int.Parse(y.Substring(1)))));
      for (int index = stringList.Count - 1; index >= 0; --index)
        NGUITools.AddChild(parent, this.tier).GetComponent<TierListComponent>().FeedData((IComponentData) new TierComponentData(stringList[index].ToString(), hashtable2[arrayList[index]] as Hashtable));
    }
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawReinforceTroop(Hashtable data)
  {
    if (!data.ContainsKey((object) "reinforcing_troops"))
      return;
    this.AddSubtitle(Utils.XLAT("mail_scout_report_uppercase_reinforcements_subtitle"));
    Hashtable hashtable1 = data[(object) "reinforcing_troops"] as Hashtable;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    this.AddUnitCount(parent, hashtable1[(object) "amount"].ToString(), Utils.XLAT("mail_scout_report_total_troops"));
    foreach (DictionaryEntry dictionaryEntry in hashtable1)
    {
      if (dictionaryEntry.Key.ToString() != "amount" && (dictionaryEntry.Value as Hashtable).ContainsKey((object) "troops"))
      {
        Hashtable hashtable2 = ((dictionaryEntry.Value as Hashtable)[(object) "troops"] as Hashtable)[(object) "tier"] as Hashtable;
        List<TierComponentData> tiers = new List<TierComponentData>();
        if (hashtable2 != null)
        {
          List<string> stringList = new List<string>((IEnumerable<string>) new ArrayList(hashtable2.Keys).ToArray(typeof (string)));
          stringList.Sort((Comparison<string>) ((x, y) => int.Parse(x.Substring(1)).CompareTo(int.Parse(y.Substring(1)))));
          for (int index = stringList.Count - 1; index >= 0; --index)
          {
            TierComponentData tierComponentData = new TierComponentData(stringList[index], hashtable2[(object) stringList[index]] as Hashtable);
            tiers.Add(tierComponentData);
          }
        }
        ReinforcePlayerWithTierComponent component = NGUITools.AddChild(this.contentTable.gameObject, this.reinforceTroop).GetComponent<ReinforcePlayerWithTierComponent>();
        component.parent = parent;
        component.FeedData((IComponentData) new ReinforcePlayerWithTierComponentData(tiers, dictionaryEntry.Value as Hashtable));
      }
    }
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawResearch()
  {
    if (!this.param.hashtable.ContainsKey((object) "research_benefits"))
      return;
    this.AddSubtitle("id_uppercase_research");
    Hashtable table = this.param.hashtable[(object) "research_benefits"] as Hashtable;
    ResearchComponent component = NGUITools.AddChild(this.contentTable.gameObject, this.research).GetComponent<ResearchComponent>();
    component.parent = this.contentTable.gameObject;
    component.FeedData((IComponentData) new ResearchComponentData(table));
  }

  private void DrawTalent()
  {
    if (!this.param.hashtable.ContainsKey((object) "lord_benefits"))
      return;
    this.AddSubtitle("id_uppercase_lord_talents");
    Hashtable table = this.param.hashtable[(object) "lord_benefits"] as Hashtable;
    TalentComponent component = NGUITools.AddChild(this.contentTable.gameObject, this.talent).GetComponent<TalentComponent>();
    component.parent = this.contentTable.gameObject;
    component.FeedData((IComponentData) new TalentComponentData(table));
  }

  private void DrawFailure()
  {
    if (!this.param.hashtable.ContainsKey((object) "fail"))
      return;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    NGUITools.AddChild(parent, this.oneLabel).GetComponentInChildren<UILabel>().text = Utils.XLAT("scout_fail_reason_" + this.param.hashtable[(object) "fail"].ToString());
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void AddSubtitle(string titleKey)
  {
    NGUITools.AddChild(this.contentTable.gameObject, this.subTitle).GetComponentInChildren<UILabel>().text = Utils.XLAT(titleKey);
  }

  private void AddUnitCount(GameObject parent, string count, string name)
  {
    GameObject gameObject = NGUITools.AddChild(parent, this.troopCount);
    gameObject.transform.Find("valueLabel").GetComponent<UILabel>().text = count;
    gameObject.transform.Find("nameLabel").GetComponent<UILabel>().text = name;
  }
}
