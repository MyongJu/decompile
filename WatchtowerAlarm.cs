﻿// Decompiled with JetBrains decompiler
// Type: WatchtowerAlarm
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class WatchtowerAlarm : MonoBehaviour
{
  public UISprite m_WarningSprite;
  public UILabel m_LeftTimeLabel;
  private bool m_Start;
  private WatchEntity m_Entity;
  private IVfx currentScreenVfx;
  private IVfx currentBuildingVfx;
  private IVfx redSmokeVfx;
  private IVfx greenSmokeVfx;
  private IVfx blueSmokeVfx;

  public IVfx RedSmokeVfx
  {
    get
    {
      if (this.redSmokeVfx == null)
        this.redSmokeVfx = VfxManager.Instance.Create("Prefab/VFX/fx_red_smoke");
      return this.redSmokeVfx;
    }
  }

  public IVfx GreenSmokeVfx
  {
    get
    {
      if (this.greenSmokeVfx == null)
        this.greenSmokeVfx = VfxManager.Instance.Create("Prefab/VFX/fx_green_smoke");
      return this.greenSmokeVfx;
    }
  }

  public IVfx BlueSmokeVfx
  {
    get
    {
      if (this.blueSmokeVfx == null)
        this.blueSmokeVfx = VfxManager.Instance.Create("Prefab/VFX/fx_blue_smoke");
      return this.blueSmokeVfx;
    }
  }

  public void UpdateEntity(WatchEntity entity)
  {
    this.gameObject.SetActive(true);
    this.m_Entity = entity;
    this.UpdateTimer(entity);
    this.ChangeColor(entity);
  }

  public void AddScreenVfx(WatchEntity entity)
  {
    if (this.currentScreenVfx == null)
    {
      this.currentScreenVfx = VfxManager.Instance.Create("Prefab/VFX/flash_red");
      this.currentScreenVfx.Play(UIManager.inst.ui2DCamera.transform);
      this.currentScreenVfx.gameObject.GetComponentInChildren<UISprite>().color = this.ConfigColor(entity);
    }
    if (this.IsCurrentScreenVfxAlive)
    {
      this.currentScreenVfx.Play(UIManager.inst.ui2DCamera.transform);
      this.currentScreenVfx.gameObject.GetComponentInChildren<UISprite>().color = this.ConfigColor(entity);
    }
    if (!WatchtowerUtilities.IgnorMap[entity])
    {
      if (!this.IsCurrentScreenVfxAlive)
        return;
      this.currentScreenVfx.Play(UIManager.inst.ui2DCamera.transform);
      this.currentScreenVfx.gameObject.GetComponentInChildren<UISprite>().color = this.ConfigColor(entity);
    }
    else
    {
      if (!this.IsCurrentScreenVfxAlive)
        return;
      this.currentScreenVfx.gameObject.SetActive(false);
    }
  }

  public void AddBuildingVfx(WatchEntity entity)
  {
    IVfx vfx = (IVfx) null;
    Color color = this.ConfigColor(entity);
    if (color == Color.red)
      vfx = this.RedSmokeVfx;
    else if (color == Color.blue)
      vfx = this.BlueSmokeVfx;
    else if (color == Color.green)
      vfx = this.GreenSmokeVfx;
    if (this.currentBuildingVfx != vfx)
    {
      this.StopBuildingVfx();
      this.currentBuildingVfx = vfx;
    }
    if (!this.IsCurrentBuildingVfxAlive || !((UnityEngine.Object) CitadelSystem.inst.WatchTower != (UnityEngine.Object) null) || this.currentBuildingVfx.IsPlaying)
      return;
    this.currentBuildingVfx.Play(CitadelSystem.inst.WatchTower.transform);
  }

  private bool IsCurrentScreenVfxAlive
  {
    get
    {
      VfxBase currentScreenVfx = this.currentScreenVfx as VfxBase;
      if ((bool) ((UnityEngine.Object) currentScreenVfx))
        return currentScreenVfx.IsAlive;
      return false;
    }
  }

  private bool IsCurrentBuildingVfxAlive
  {
    get
    {
      VfxBase currentBuildingVfx = this.currentBuildingVfx as VfxBase;
      if ((bool) ((UnityEngine.Object) currentBuildingVfx))
        return currentBuildingVfx.IsAlive;
      return false;
    }
  }

  public void StopBuildingVfx()
  {
    if (!this.IsCurrentBuildingVfxAlive)
      return;
    this.currentBuildingVfx.gameObject.SetActive(false);
  }

  public void StopVfx()
  {
    if (!this.IsCurrentScreenVfxAlive)
      return;
    this.currentScreenVfx.gameObject.SetActive(false);
  }

  private void UpdateTimer(WatchEntity entity)
  {
    if (!((UnityEngine.Object) this.m_LeftTimeLabel != (UnityEngine.Object) null))
      return;
    if (WatchtowerUtilities.GetWatchtowerLevel() >= 3)
      this.m_LeftTimeLabel.gameObject.SetActive(true);
    else
      this.m_LeftTimeLabel.gameObject.SetActive(false);
  }

  private void ChangeColor(WatchEntity entity)
  {
    this.m_WarningSprite.color = entity.GetColor();
  }

  private Color ConfigColor(WatchEntity entity)
  {
    return entity.GetColor();
  }

  private void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
    if (!this.m_Start)
      return;
    this.OnSecondHandler(NetServerTime.inst.ServerTimestamp);
  }

  private void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
  }

  private void OnSecondHandler(int timeStamp)
  {
    if (this.m_Entity.m_Id <= 0L)
      return;
    this.UpdateTimer(this.m_Entity);
    if (this.m_Entity.m_Type == WatchType.March)
    {
      MarchData marchData = DBManager.inst.DB_March.Get(this.m_Entity.m_Id);
      int watchtowerLevel = WatchtowerUtilities.GetWatchtowerLevel();
      if (marchData != null && watchtowerLevel >= 0)
      {
        int num = marchData.endTime - timeStamp;
        this.m_LeftTimeLabel.text = Utils.FormatTime(num <= 0 ? 0 : num, false, false, true);
      }
      else
        this.m_LeftTimeLabel.text = string.Empty;
    }
    else
    {
      RallyData rallyData = DBManager.inst.DB_Rally.Get(this.m_Entity.m_Id);
      int watchtowerLevel = WatchtowerUtilities.GetWatchtowerLevel();
      if (rallyData != null && watchtowerLevel >= 0)
      {
        int num = rallyData.waitTimeDuration.endTime - timeStamp;
        this.m_LeftTimeLabel.text = Utils.FormatTime(num <= 0 ? 0 : num, false, false, true);
      }
      else
        this.m_LeftTimeLabel.text = string.Empty;
    }
    this.AddScreenVfx(this.m_Entity);
    this.AddBuildingVfx(this.m_Entity);
  }
}
