﻿// Decompiled with JetBrains decompiler
// Type: ChannelBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Chat;
using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ChannelBar : MonoBehaviour
{
  private long _channelId;
  public System.Action<long> onChannelBarClick;
  private int _unreadMessageCount;
  [SerializeField]
  private ChannelBar.Panel panel;

  public int unreadMessageCount
  {
    get
    {
      return this._unreadMessageCount;
    }
    set
    {
      this._unreadMessageCount = value;
      if (value == 0)
      {
        this.panel.channelUnreadMsgCount.gameObject.SetActive(false);
      }
      else
      {
        this.panel.channelUnreadMsgCount.gameObject.SetActive(true);
        this.panel.channelUnreadMsgCount.text = ScriptLocalization.GetWithPara("chat_number_new_messages", new Dictionary<string, string>()
        {
          {
            "0",
            value.ToString()
          }
        }, true);
      }
    }
  }

  public void SetInfo(ChatRoomData room)
  {
    if (room == null || room == null)
      return;
    this.panel.channelName.text = room.roomName;
    this.panel.channelWelcomMsg.text = room.roomTitle;
    this._channelId = room.channelId;
    ChatChannel chatChannel = GameEngine.Instance.ChatManager.GetChatChannel(room.channelId, 0L);
    this.unreadMessageCount = chatChannel == null ? 0 : chatChannel.unreadCount;
    this.panel.channelMemberCount.text = string.Format("{0}/{1}", (object) room.MemberCount, (object) ConfigManager.inst.DB_GameConfig.GetData("chat_room_max_number").ValueInt);
    this.panel.applyingSymbol.SetActive(room.members[PlayerData.inst.uid].IsManageMember() && room.ApplyingCount() > 0);
  }

  public void OnBarClick()
  {
    if (this.onChannelBarClick == null)
      return;
    this.onChannelBarClick(this._channelId);
  }

  [Serializable]
  protected class Panel
  {
    public UILabel channelName;
    public UILabel channelWelcomMsg;
    public UILabel channelUnreadMsgCount;
    public UILabel channelMemberCount;
    public GameObject applyingSymbol;
  }
}
