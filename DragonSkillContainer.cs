﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonSkillContainer : MonoBehaviour
{
  private int MaxCount = 8;
  public UILabel title;
  public UITexture typeIcon;
  public DragonSkillItem[] skills;
  public DragonSkillContainer.SkillType type;
  public UITable table;
  public DragonSkillItem SkillItemPrefab;

  private void Start()
  {
  }

  private void Init()
  {
    if (this.skills != null && this.skills.Length != 0)
      return;
    this.skills = new DragonSkillItem[this.MaxCount];
    for (int index = 0; index < this.MaxCount; ++index)
    {
      GameObject go = NGUITools.AddChild(this.table.gameObject, this.SkillItemPrefab.gameObject);
      NGUITools.SetActive(go, true);
      DragonSkillItem component = go.GetComponent<DragonSkillItem>();
      this.skills[index] = component;
    }
  }

  public void ConfigSkill()
  {
    UIManager.inst.OpenDlg("Dragon/DragonSkillConfigDialog", (UI.Dialog.DialogParameter) new DragonSkillConfigDialog.Parameter()
    {
      tabIndex = this.GetIndex()
    }, true, true, true);
  }

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.typeIcon);
    for (int index = 0; index < this.skills.Length; ++index)
      this.skills[index].Clear();
  }

  public void Refresh()
  {
    this.Init();
    List<int> skillData = this.GetSkillData();
    this.title.text = this.GetTitle();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.typeIcon, this.GetTypeIconPath(), (System.Action<bool>) null, true, false, string.Empty);
    List<DragonInfo> dragonInfoList = ConfigManager.inst.DB_ConfigDragon.GetDragonInfoList();
    dragonInfoList.Sort(new Comparison<DragonInfo>(this.CompareDragon));
    DragonInfo dragonInfoByLevel = ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(PlayerData.inst.dragonData.Level);
    int skillSlot = dragonInfoByLevel.skill_slot;
    int num = dragonInfoList.IndexOf(dragonInfoByLevel);
    for (int index1 = 0; index1 < this.skills.Length; ++index1)
    {
      DragonSkillItem skill = this.skills[index1];
      if (index1 < skillData.Count && skillData[index1] > 0)
        skill.SetData(skillData[index1]);
      else if (skillSlot > index1)
      {
        skill.Empty(this.GetSkillTypeName());
      }
      else
      {
        int reqLev = 0;
        for (int index2 = num + 1; index2 < dragonInfoList.Count; ++index2)
        {
          DragonInfo dragonInfo = dragonInfoList[index2];
          if (dragonInfo.skill_slot > skillSlot)
          {
            reqLev = dragonInfo.level;
            break;
          }
        }
        skill.Lock(reqLev);
        ++skillSlot;
      }
    }
  }

  private string GetTypeIconPath()
  {
    string str = "Texture/Dragon/";
    switch (this.type)
    {
      case DragonSkillContainer.SkillType.Attack:
        str += "icon_dragon_skill_attack";
        break;
      case DragonSkillContainer.SkillType.Defense:
        str += "icon_dragon_skill_defense";
        break;
      case DragonSkillContainer.SkillType.Gather:
        str += "icon_dragon_skill_gather";
        break;
      case DragonSkillContainer.SkillType.Monster:
        str += "icon_dragon_skill_monster";
        break;
    }
    return str;
  }

  private string GetTitle()
  {
    string Term = string.Empty;
    switch (this.type)
    {
      case DragonSkillContainer.SkillType.Attack:
        Term = "dragon_skill_attack_title";
        break;
      case DragonSkillContainer.SkillType.Defense:
        Term = "dragon_skill_defense_title";
        break;
      case DragonSkillContainer.SkillType.Gather:
        Term = "dragon_skill_gather_title";
        break;
      case DragonSkillContainer.SkillType.Monster:
        Term = "dragon_skill_monster_title";
        break;
    }
    return ScriptLocalization.Get(Term, true);
  }

  private int GetIndex()
  {
    int num = 0;
    switch (this.type)
    {
      case DragonSkillContainer.SkillType.Defense:
        num = 1;
        break;
      case DragonSkillContainer.SkillType.Gather:
        num = 2;
        break;
      case DragonSkillContainer.SkillType.Monster:
        num = 3;
        break;
    }
    return num;
  }

  private string GetSkillTypeName()
  {
    string str = string.Empty;
    switch (this.type)
    {
      case DragonSkillContainer.SkillType.Attack:
        str = "attack";
        break;
      case DragonSkillContainer.SkillType.Defense:
        str = "defend";
        break;
      case DragonSkillContainer.SkillType.Gather:
        str = "gather";
        break;
      case DragonSkillContainer.SkillType.Monster:
        str = "attack_monster";
        break;
    }
    return str;
  }

  private int CompareDragon(DragonInfo a, DragonInfo b)
  {
    return a.level.CompareTo(b.level);
  }

  private List<int> GetSkillData()
  {
    DragonData.DragonSkillInfo skills = PlayerData.inst.dragonData.Skills;
    string skillTypeName = this.GetSkillTypeName();
    List<int> intList = new List<int>();
    if (skills.datas.Count > 0 && skills.datas.ContainsKey(skillTypeName))
      intList = skills.datas[skillTypeName];
    return intList;
  }

  public enum SkillType
  {
    Attack,
    Defense,
    Gather,
    Monster,
  }
}
