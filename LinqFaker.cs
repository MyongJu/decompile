﻿// Decompiled with JetBrains decompiler
// Type: LinqFaker
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public static class LinqFaker
{
  public static T FirstOrDefault<T>(ICollection<T> collection, Func<T, bool> predicate)
  {
    IEnumerator<T> enumerator = collection.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (predicate(enumerator.Current))
        return enumerator.Current;
    }
    return default (T);
  }

  public static float Min<T>(ICollection<T> collection, Func<T, float> selector, float defaultValue = 3.402823E+38f)
  {
    if (selector == null || collection == null || collection.Count <= 0)
      return defaultValue;
    float num1 = float.MaxValue;
    IEnumerator<T> enumerator = collection.GetEnumerator();
    while (enumerator.MoveNext())
    {
      float num2 = selector(enumerator.Current);
      if ((double) num2 < (double) num1)
        num1 = num2;
    }
    return num1;
  }

  public static float Max<T>(ICollection<T> collection, Func<T, float> selector, float defaultValue = -3.402823E+38f)
  {
    if (selector == null || collection == null || collection.Count <= 0)
      return defaultValue;
    float num1 = float.MinValue;
    IEnumerator<T> enumerator = collection.GetEnumerator();
    while (enumerator.MoveNext())
    {
      float num2 = selector(enumerator.Current);
      if ((double) num2 > (double) num1)
        num1 = num2;
    }
    return num1;
  }
}
