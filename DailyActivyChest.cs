﻿// Decompiled with JetBrains decompiler
// Type: DailyActivyChest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class DailyActivyChest : MonoBehaviour
{
  public UI2DSprite openBox;
  public UI2DSprite closeBox;
  public GameObject effect;
  private int id;
  public System.Action onCloseHandler;
  public System.Action onShowHandler;
  private bool effectShow;

  public void SetData(DailyActiveRewardInfo info)
  {
    this.id = info.internalId;
    NGUITools.SetActive(this.closeBox.gameObject, false);
    NGUITools.SetActive(this.openBox.gameObject, false);
    if (info.RequrieScore > DBManager.inst.DB_DailyActives.CurrentPoint)
    {
      NGUITools.SetActive(this.effect, false);
      NGUITools.SetActive(this.closeBox.gameObject, true);
      GreyUtility.Grey(this.gameObject);
    }
    else if (DBManager.inst.DB_DailyActives.IsHadOpen(info.internalId))
    {
      NGUITools.SetActive(this.effect, false);
      NGUITools.SetActive(this.openBox.gameObject, true);
    }
    else
    {
      NGUITools.SetActive(this.closeBox.gameObject, true);
      NGUITools.SetActive(this.effect, true);
      GreyUtility.Normal(this.gameObject);
    }
    this.effectShow = this.effect.gameObject.activeSelf;
  }

  public void HideChestEffect()
  {
    NGUITools.SetActive(this.effect, false);
  }

  public void ShowChestEffect()
  {
    NGUITools.SetActive(this.effect, this.effectShow);
  }

  public void Clear()
  {
    this.onCloseHandler = (System.Action) null;
    this.onShowHandler = (System.Action) null;
  }

  public void Refresh()
  {
    DailyActiveRewardInfo activeRewardInfo = ConfigManager.inst.DB_DailyActivies.GetDailyActiveRewardInfo(this.id);
    if (activeRewardInfo == null)
      return;
    this.SetData(activeRewardInfo);
  }

  public void DisplayRewawrdItem()
  {
    if (this.onShowHandler != null)
      this.onShowHandler();
    UIManager.inst.OpenPopup("DailyActivy/DailyActivyRewardPopUp", (Popup.PopupParameter) new DailyActiveRewardListPopup.Parameter()
    {
      reward_id = this.id,
      onCloseHandler = this.onCloseHandler
    });
  }
}
