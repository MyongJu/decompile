﻿// Decompiled with JetBrains decompiler
// Type: DailyRewardFactor
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DailyRewardFactor
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "stronghold_level")]
  public int StrongholdLevel;
  [Config(Name = "daily_reward_wood")]
  public float RewardWoodFactor;
  [Config(Name = "daily_reward_food")]
  public float RewardFoodFactor;
  [Config(Name = "daily_reward_ore")]
  public float RewardOreFactor;
  [Config(Name = "daily_reward_silver")]
  public float RewardSilverFactor;
  [Config(Name = "daily_reward_lordexp")]
  public float RewardLordEXPFactor;
  [Config(Name = "daily_reward_dragonexp")]
  public float RewardDragonEXPFactor;
  [Config(Name = "daily_reward_item_1")]
  public float RewardItemFactor1;
  [Config(Name = "daily_reward_item_2")]
  public float RewardItemFactor2;
  [Config(Name = "daily_reward_item_3")]
  public float RewardItemFactor3;
  [Config(Name = "daily_reward_item_4")]
  public float RewardItemFactor4;
  [Config(Name = "casino_wood_ratio")]
  public float CasinoWoodRatio;
  [Config(Name = "casino_food_ratio")]
  public float CasinoFoodRatio;
  [Config(Name = "casino_ore_ratio")]
  public float CasinoOreRatio;
  [Config(Name = "casino_sliver_ratio")]
  public float CasinoSilverRatio;
  [Config(Name = "alliance_storage_daily")]
  public int AllianceStorageDaily;
  [Config(Name = "alliance_storage_total")]
  public int AllianceStorageTotal;
}
