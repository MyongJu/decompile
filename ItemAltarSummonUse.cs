﻿// Decompiled with JetBrains decompiler
// Type: ItemAltarSummonUse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class ItemAltarSummonUse : ItemBaseUse
{
  public ItemAltarSummonUse(int id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  public ItemAltarSummonUse(string id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  protected override void CustomUse(System.Action<bool, object> resultHandler = null)
  {
    base.CustomUse(resultHandler);
    UIManager.inst.OpenDlg("AltarSummon/AltarSummonDialog", (UI.Dialog.DialogParameter) new AltarSummonDlg.Parameter()
    {
      itemId = this.item_internal
    }, 1 != 0, 1 != 0, 1 != 0);
  }
}
