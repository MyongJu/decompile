﻿// Decompiled with JetBrains decompiler
// Type: WatchtowerMarchesItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class WatchtowerMarchesItemRenderer : MonoBehaviour
{
  private Color32 COORDS_COLOR = new Color32((byte) 221, (byte) 164, (byte) 41, byte.MaxValue);
  public UIButton m_GotoButton;
  public UIButton m_InfoButton;
  public UILabel m_ToLabel;
  public UILabel m_LeftTimeLabel;
  public UISprite m_ProgressForeground;
  public UISlider m_ProgressBar;
  public UILabel m_PurposeLabel;
  public UITexture iconTexture;
  public UILabel ownerNameLable;
  public UIButton ignoreBtn;
  public UIButton repatriateBtn;
  private WatchEntity m_Entity;
  private bool m_Start;

  public event System.Action<Hashtable> OnMarchDetail;

  public MarchData marchData { protected set; get; }

  public RallyData rallyData { protected set; get; }

  public WatchEntity entity
  {
    get
    {
      return this.m_Entity;
    }
  }

  private void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
    if (!this.m_Start)
      return;
    this.OnSecondHandler(NetServerTime.inst.ServerTimestamp);
  }

  private void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
  }

  private void OnStart()
  {
    this.OnSecondHandler(NetServerTime.inst.ServerTimestamp);
    this.m_Start = true;
  }

  private void OnSecondHandler(int timeStamp)
  {
    if (this.m_Entity.m_Type == WatchType.March)
    {
      if (WatchtowerUtilities.GetWatchtowerLevel() >= 3)
      {
        int startTime = this.marchData.startTime;
        int endTime = this.marchData.endTime;
        this.m_ProgressBar.value = Mathf.Clamp01((float) (timeStamp - startTime) / (float) (endTime - startTime));
        int num = endTime - timeStamp;
        this.m_LeftTimeLabel.text = Utils.XLAT("watchtower_march_arriving_in") + Utils.FormatTime(num <= 0 ? 0 : num, false, false, true);
      }
      else
      {
        this.m_ProgressBar.value = 0.0f;
        this.m_LeftTimeLabel.text = Utils.XLAT("watchtower_march_unlock_level3_requirement");
      }
    }
    else if (WatchtowerUtilities.GetWatchtowerLevel() >= 3)
    {
      int startTime = this.rallyData.waitTimeDuration.startTime;
      int endTime = this.rallyData.waitTimeDuration.endTime;
      this.m_ProgressBar.value = Mathf.Clamp01((float) (timeStamp - startTime) / (float) (endTime - startTime));
      int num = endTime - timeStamp;
      this.m_LeftTimeLabel.text = Utils.XLAT("watchtower_march_rallying_time_remaining") + Utils.FormatTime(num <= 0 ? 0 : num, false, false, true);
    }
    else
    {
      this.m_ProgressBar.value = 0.0f;
      this.m_LeftTimeLabel.text = Utils.XLAT("watchtower_march_unlock_level3_requirement");
    }
  }

  public void SetData(WatchEntity entity, System.Action<Hashtable> onMarchDetail)
  {
    this.OnMarchDetail += onMarchDetail;
    this.m_Entity = entity;
    if (this.m_Entity.m_Type == WatchType.March)
      this.marchData = DBManager.inst.DB_March.Get(this.m_Entity.m_Id);
    else
      this.rallyData = DBManager.inst.DB_Rally.Get(this.m_Entity.m_Id);
    int watchtowerLevel = WatchtowerUtilities.GetWatchtowerLevel();
    this.UpdateMarchType(watchtowerLevel);
    this.UpdateCoordinate(watchtowerLevel);
    this.UpdateProgressBar(watchtowerLevel);
    this.UpdateIgnoreBtn();
    this.UpdateRepatriateBtn();
    this.UpdateUserInfo();
    this.OnSecondHandler(NetServerTime.inst.ServerTimestamp);
  }

  private void UpdateIgnoreBtn()
  {
    this.ignoreBtn.gameObject.SetActive(!WatchtowerUtilities.IgnorMap[this.entity]);
    if (this.marchData == null || this.marchData.type != MarchData.MarchType.trade)
      return;
    this.ignoreBtn.gameObject.SetActive(false);
  }

  private void UpdateRepatriateBtn()
  {
    if (this.m_Entity.m_Type == WatchType.March)
    {
      this.marchData = DBManager.inst.DB_March.Get(this.m_Entity.m_Id);
      if (this.marchData.type == MarchData.MarchType.reinforce)
      {
        this.repatriateBtn.gameObject.SetActive(true);
        this.ignoreBtn.gameObject.SetActive(false);
      }
      else if (this.marchData.type == MarchData.MarchType.fortress_reinforce)
      {
        this.repatriateBtn.gameObject.SetActive(false);
        this.ignoreBtn.gameObject.SetActive(false);
      }
      else if (this.marchData.type == MarchData.MarchType.wonder_reinforce)
      {
        this.repatriateBtn.gameObject.SetActive(false);
        this.ignoreBtn.gameObject.SetActive(false);
      }
      else
        this.repatriateBtn.gameObject.SetActive(false);
    }
    else
      this.repatriateBtn.gameObject.SetActive(false);
  }

  private void UpdateUserInfo()
  {
    if ((this.marchData == null ? DBManager.inst.DB_User.Get(this.rallyData.ownerUid) : DBManager.inst.DB_User.Get(this.marchData.ownerUid)) != null)
    {
      this.SetUserInfo();
    }
    else
    {
      Hashtable postData = new Hashtable();
      postData[(object) "uids"] = (object) new List<long>()
      {
        this.marchData.ownerUid
      };
      MessageHub.inst.GetPortByAction("PVP:getIncomingMarchOwnerDefaultInfo").SendRequest(postData, (System.Action<bool, object>) ((arg1, arg2) =>
      {
        if (!arg1)
          return;
        Utils.ExecuteInSecs(0.01f, new System.Action(this.SetUserInfo));
      }), true);
    }
  }

  private void SetUserInfo()
  {
    if (this.m_Entity.m_Type == WatchType.March)
    {
      this.marchData = DBManager.inst.DB_March.Get(this.m_Entity.m_Id);
      UserData userData = DBManager.inst.DB_User.Get(this.marchData.ownerUid);
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(this.marchData.ownerAllianceId);
      if (userData == null)
        return;
      CustomIconLoader.Instance.requestCustomIcon(this.iconTexture, userData.PortraitIconPath, userData.Icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.iconTexture, userData.LordTitle, 1);
      this.ownerNameLable.text = allianceData == null ? string.Empty + userData.userName : string.Format("({0})", (object) allianceData.allianceAcronym) + userData.userName;
    }
    else
    {
      this.rallyData = DBManager.inst.DB_Rally.Get(this.m_Entity.m_Id);
      UserData userData = DBManager.inst.DB_User.Get(this.rallyData.ownerUid);
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(this.rallyData.ownerAllianceId);
      if (userData == null)
        return;
      CustomIconLoader.Instance.requestCustomIcon(this.iconTexture, userData.PortraitIconPath, userData.Icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.iconTexture, userData.LordTitle, 1);
      this.ownerNameLable.text = allianceData == null ? string.Empty + userData.userName : string.Format("({0})", (object) allianceData.allianceAcronym) + userData.userName;
    }
  }

  public void OnGotoClick()
  {
    if (this.m_Entity.m_Type == WatchType.March)
    {
      if (!MapUtils.CanGotoTarget(this.marchData.endLocation.K))
      {
        UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
        return;
      }
      PVPMap.PendingGotoRequest = this.marchData.endLocation;
    }
    else
    {
      DBManager.inst.DB_City.Get(this.rallyData.ownerCityId);
      MarchData marchData = DBManager.inst.DB_March.Get(this.rallyData.rallyMarchId);
      Coordinate coordinate = marchData == null ? this.rallyData.location : marchData.endLocation;
      if (!MapUtils.CanGotoTarget(coordinate.K))
      {
        UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
        return;
      }
      PVPMap.PendingGotoRequest = coordinate;
    }
    if (PlayerData.inst.IsInPit)
    {
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PitMode)
        UIManager.inst.Cloud.CoverCloud((System.Action) (() => CitadelSystem.inst.OnLoadWorldMap()));
    }
    else if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
      UIManager.inst.Cloud.CoverCloud((System.Action) (() => CitadelSystem.inst.OnLoadWorldMap()));
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void UpdateMarchType(int towerLevel)
  {
    if (towerLevel < 0)
      this.m_PurposeLabel.text = "Activity";
    else if (this.m_Entity.m_Type == WatchType.March)
    {
      if (this.marchData.type == MarchData.MarchType.trade)
        this.m_PurposeLabel.text = Utils.XLAT("watchtower_march_trade");
      else if (this.marchData.type == MarchData.MarchType.reinforce)
        this.m_PurposeLabel.text = Utils.XLAT("watchtower_march_reinforce");
      else if (this.marchData.type == MarchData.MarchType.scout)
        this.m_PurposeLabel.text = Utils.XLAT("watchtower_march_scout");
      else if (this.marchData.isNormalAttack)
        this.m_PurposeLabel.text = Utils.XLAT("watchtower_march_attack");
      else if (this.marchData.type == MarchData.MarchType.rally_attack)
        this.m_PurposeLabel.text = Utils.XLAT("watchtower_march_rally");
      else if (this.marchData.type == MarchData.MarchType.fortress_reinforce)
        this.m_PurposeLabel.text = Utils.XLAT("watchtower_march_reinforce");
      else if (this.marchData.type == MarchData.MarchType.wonder_reinforce)
        this.m_PurposeLabel.text = Utils.XLAT("watchtower_march_reinforce");
      else
        this.m_PurposeLabel.text = Utils.XLAT("watchtower_march_attack");
    }
    else
      this.m_PurposeLabel.text = Utils.XLAT("watchtower_march_rallying");
  }

  private void UpdateCoordinate(int towerLevel)
  {
    if (towerLevel >= 0)
    {
      Coordinate coordinate;
      if (this.m_Entity.m_Type == WatchType.March)
      {
        coordinate = this.marchData.endLocation;
      }
      else
      {
        DBManager.inst.DB_City.Get(this.rallyData.targetCityId);
        MarchData marchData = DBManager.inst.DB_March.Get(this.rallyData.rallyMarchId);
        coordinate = marchData == null ? this.rallyData.location : marchData.endLocation;
      }
      Utils.GetRGBFromColor32(this.COORDS_COLOR);
      if (MapUtils.IsPitWorld(coordinate.K))
      {
        string str = "[0000ff]";
        this.m_ToLabel.text = string.Format(" {0}{1}{2} X:{3}{4}{5} Y:{6}{7}{8}", (object) str, (object) Utils.XLAT("id_fire_kingdom"), (object) "[-]", (object) str, (object) coordinate.X, (object) "[-]", (object) str, (object) coordinate.Y, (object) "[-]");
      }
      else
      {
        string str = "[0000ff]";
        this.m_ToLabel.text = string.Format(" K:{0}{1}{2} X:{3}{4}{5} Y:{6}{7}{8}", (object) str, (object) coordinate.K, (object) "[-]", (object) str, (object) coordinate.X, (object) "[-]", (object) str, (object) coordinate.Y, (object) "[-]");
      }
      this.m_GotoButton.gameObject.SetActive(true);
    }
    else
    {
      this.m_ToLabel.text = string.Empty;
      this.m_GotoButton.gameObject.SetActive(false);
    }
  }

  private void UpdateProgressBar(int towerLevel)
  {
    if (towerLevel >= 3)
      this.m_ProgressForeground.color = this.m_Entity.GetColor();
    else
      this.m_LeftTimeLabel.text = Utils.XLAT("watchtower_march_unlock_level3_requirement");
  }

  private void UpdateInfoBtn(int towerLevel)
  {
  }

  public void OnInfoClick()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "city_id"] = (object) PlayerData.inst.cityId;
    postData[(object) "uid"] = (object) PlayerData.inst.uid;
    if (this.m_Entity.m_Type == WatchType.March)
    {
      postData[(object) "type"] = (object) this.marchData.type.ToString();
      postData[(object) "opp_uid"] = (object) this.marchData.ownerUid;
      postData[(object) "entity_id"] = (object) this.marchData.marchId;
    }
    else
    {
      postData[(object) "type"] = (object) MarchData.MarchType.rally.ToString();
      postData[(object) "opp_uid"] = (object) this.rallyData.ownerUid;
      postData[(object) "entity_id"] = (object) this.rallyData.rallyId;
    }
    MessageHub.inst.GetPortByAction("PVP:getIncomingMarchDetail").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      Hashtable hashtable = data as Hashtable;
      if (hashtable == null)
        return;
      UIManager.inst.OpenPopup("WatchTowerDetailPopup", (Popup.PopupParameter) new WatchTowerDetailPopup.Parameter()
      {
        hashtable = hashtable,
        entity = this.entity
      });
    }), true);
  }

  public void OnIgnoreBtnClick()
  {
    this.ignoreBtn.gameObject.SetActive(false);
    WatchtowerUtilities.IgnorMap[this.entity] = true;
  }

  public void OnRepatriateBtnClick()
  {
    this.SendTroopHome(this.marchData.marchId);
  }

  public void SendTroopHome(long marchUid)
  {
    if (this.marchData != null && this.marchData.state == MarchData.MarchState.marching && this.marchData.type == MarchData.MarchType.reinforce)
      UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
      {
        title = Utils.XLAT("id_uppercase_confirm"),
        description = Utils.XLAT("send_troops_back_description"),
        leftButtonText = Utils.XLAT("id_uppercase_no"),
        leftButtonClickEvent = (System.Action) null,
        rightButtonText = Utils.XLAT("id_uppercase_yes"),
        rightButtonClickEvent = (System.Action) (() => MessageHub.inst.GetPortByAction("PVP:sendIncomingReinforceBack").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "march_id", (object) this.marchData.marchId, (object) "back_uid", (object) this.marchData.ownerUid), (System.Action<bool, object>) null, true))
      });
    else
      UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
      {
        title = Utils.XLAT("id_uppercase_confirm"),
        description = Utils.XLAT("embassy_reinforcement_send_back_description"),
        leftButtonText = Utils.XLAT("id_uppercase_no"),
        leftButtonClickEvent = (System.Action) null,
        rightButtonText = Utils.XLAT("id_uppercase_yes"),
        rightButtonClickEvent = (System.Action) (() => MessageHub.inst.GetPortByAction("City:sendEmbassyTroopsBack").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "city_id", (object) PlayerData.inst.cityId, (object) "back_uid", (object) marchUid), (System.Action<bool, object>) null, true))
      });
  }
}
