﻿// Decompiled with JetBrains decompiler
// Type: ActivityFestivalShopPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ActivityFestivalShopPopup : MonoBehaviour
{
  private ActivityFestivalShopPopup.Data data = new ActivityFestivalShopPopup.Data();
  private bool secondEvent;
  private int revertFlag;
  private int buyItemId;
  private int _count;
  private bool _isDestroy;
  [SerializeField]
  private ActivityFestivalShopPopup.Panel panel;

  public void Show(int activityId)
  {
    if ((UnityEngine.Object) this.panel.viewPort.sourceCamera == (UnityEngine.Object) null)
      this.panel.viewPort.sourceCamera = UIManager.inst.ui2DCamera;
    this.gameObject.SetActive(true);
    this.data.activityId = activityId;
    this._count = 0;
    this.revertFlag = 0;
    this.buyItemId = 0;
    RequestManager.inst.SendRequest("activity:loadItems", Utils.Hash(), new System.Action<bool, object>(this.OnRefreshCallback), true);
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemUpdate);
  }

  private void OnDestroy()
  {
    this._isDestroy = true;
  }

  private void OnDisable()
  {
    if (GameEngine.IsAvailable)
      DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemUpdate);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  private void OnItemUpdate(int itemid)
  {
    if (this.data.festivalExchange == null || this.data.item == null || itemid != this.data.item.internalId)
      return;
    for (int index = 0; index < this.panel.slots.Count; ++index)
      this.panel.slots[index].Check(this.data.itemCount);
  }

  public void Hide()
  {
    for (int index = 0; index < this.panel.slots.Count; ++index)
      this.panel.slots[index].Release();
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemUpdate);
    if (this.secondEvent)
    {
      this.secondEvent = false;
      if (Oscillator.IsAvailable)
        Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    }
    if ((UnityEngine.Object) this.panel.costIcon.mainTexture != (UnityEngine.Object) null)
      BuilderFactory.Instance.Release((UIWidget) this.panel.costIcon);
    if ((UnityEngine.Object) this.panel.TT_RefreshIcon.mainTexture != (UnityEngine.Object) null)
      BuilderFactory.Instance.Release((UIWidget) this.panel.TT_RefreshIcon);
    this.gameObject.SetActive(false);
  }

  public void OnRefreshCallback(bool result, object orgSrc)
  {
    if (this._isDestroy || !result)
      return;
    this.data.Decode(orgSrc);
    if (this.buyItemId != 0)
    {
      FestivalExchangeRewardInfo data1 = ConfigManager.inst.DB_FestivalExchangeReward.GetData(this.buyItemId);
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(data1.itemRewardId);
      ItemRewardInfo.Data data2 = new ItemRewardInfo.Data();
      data2.count = (float) data1.itemRewardValue;
      data2.icon = itemStaticInfo.ImagePath;
      RewardsCollectionAnimator.Instance.Clear();
      RewardsCollectionAnimator.Instance.items.Add(data2);
      RewardsCollectionAnimator.Instance.CollectItems(false);
      AudioManager.Instance.PlaySound("sfx_harvest_timed_chest", false);
    }
    if ((UnityEngine.Object) this.panel.costIcon.mainTexture == (UnityEngine.Object) null)
      BuilderFactory.Instance.Build((UIWidget) this.panel.costIcon, this.data.item.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    if ((UnityEngine.Object) this.panel.TT_RefreshIcon.mainTexture == (UnityEngine.Object) null)
      BuilderFactory.Instance.Build((UIWidget) this.panel.TT_RefreshIcon, this.data.item.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    this.SetTopValue();
    if (this.data.festivalCost == 0)
    {
      this.panel.BT_FreeRefresh.gameObject.SetActive(true);
      this.panel.BT_Refresh.gameObject.SetActive(false);
    }
    else
    {
      this.panel.BT_FreeRefresh.gameObject.SetActive(false);
      this.panel.BT_Refresh.gameObject.SetActive(true);
      this.panel.BT_Refresh.isEnabled = this.data.itemCount >= this.data.festivalCost;
    }
    for (int index = 0; index < this.panel.slots.Count && index < this.data.items.Count; ++index)
    {
      this.panel.slots[index].gameObject.SetActive(true);
      this.panel.slots[index].currentCount = this.data.itemCount;
      this.panel.slots[index].Setup(index + 1, this.data.activityId, this.data.items[index], (this.revertFlag & 1 << index) > 0);
      this.panel.slots[index].Check(this.data.itemCount);
    }
    if (this.panel.slots.Count < this.data.items.Count)
    {
      for (int count = this.panel.slots.Count; count < this.data.items.Count; ++count)
      {
        GameObject gameObject = NGUITools.AddChild(this.panel.slotContainer.gameObject, this.panel.orgSlot.gameObject);
        gameObject.name = (100 + count).ToString();
        gameObject.SetActive(true);
        ActivityFestivalShopSlot component = gameObject.GetComponent<ActivityFestivalShopSlot>();
        component.currentCount = this.data.itemCount;
        component.Setup(count + 1, this.data.activityId, this.data.items[count], (this.revertFlag & 1 << count) > 0);
        component.onBuyClick += new System.Action<int>(this.OnBuyClick);
        this.panel.slots.Add(component);
      }
      this.panel.slotContainer.repositionNow = true;
    }
    for (int count = this.data.items.Count; count < this.panel.slots.Count; ++count)
      this.panel.slots[count].gameObject.SetActive(false);
    if (this.secondEvent)
      return;
    this.secondEvent = true;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public void OnSecond(int serverTime)
  {
    if (this.data == null || this.data.lastRefreshTime <= 0)
      return;
    if (this.data.lastRefreshTime + this.data.festivalExchange.refreshCd > NetServerTime.inst.ServerTimestamp)
    {
      this.SetTopValue();
    }
    else
    {
      ++this._count;
      if (this._count >= 3)
      {
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      }
      else
      {
        if (this.secondEvent)
        {
          this.secondEvent = false;
          if (Oscillator.IsAvailable)
            Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnBuyClick);
        }
        this.revertFlag = 63;
        this.buyItemId = 0;
        RequestManager.inst.SendRequest("activity:loadItems", Utils.Hash(), new System.Action<bool, object>(this.OnRefreshCallback), true);
      }
    }
  }

  private void SetTopValue()
  {
    this.panel.LB_RefreshCost.text = this.data.festivalCost.ToString();
    this.panel.costValue.text = DBManager.inst.DB_Item.GetQuantity(this.data.item.internalId).ToString();
    this.panel.refreshTime.text = Utils.FormatTime1(this.data.lastRefreshTime + this.data.festivalExchange.refreshCd - NetServerTime.inst.ServerTimestamp);
  }

  public void OnBackClick()
  {
    ActivityMainDlg dlg = UIManager.inst.GetDlg<ActivityMainDlg>("Activity/ActivityMainDlg");
    if (!((UnityEngine.Object) dlg != (UnityEngine.Object) null))
      return;
    dlg.ShowFestivalEntrance(this.data.activityId);
  }

  public void OnBuyClick(int slotIndex)
  {
    this.revertFlag = 1 << slotIndex - 1;
    this.buyItemId = this.data.items[slotIndex - 1];
    RequestManager.inst.SendRequest("activity:exchange", Utils.Hash((object) "slot_id", (object) slotIndex), new System.Action<bool, object>(this.OnRefreshCallback), true);
  }

  public void OnRefreshClick()
  {
    this.revertFlag = 63;
    this.buyItemId = 0;
    RequestManager.inst.SendRequest("activity:refresh", Utils.Hash(), new System.Action<bool, object>(this.OnRefreshCallback), true);
  }

  public void Reset()
  {
    this.panel.BT_Back = this.transform.Find("Btn_Full_Screen_Back").gameObject.GetComponent<UIButton>();
    this.panel.costIcon = this.transform.Find("top/icon").gameObject.GetComponent<UITexture>();
    this.panel.costIcon.mainTexture = (Texture) null;
    this.panel.costValue = this.transform.Find("top/price").gameObject.GetComponent<UILabel>();
    this.panel.refreshTime = this.transform.Find("top/time/time").gameObject.GetComponent<UILabel>();
    this.panel.BT_FreeRefresh = this.transform.Find("top/Btn_Free").gameObject.GetComponent<UIButton>();
    this.panel.BT_FreeRefresh.onClick.Clear();
    EventDelegate.Add(this.panel.BT_FreeRefresh.onClick, new EventDelegate.Callback(this.OnRefreshClick));
    GameObject gameObject1 = this.transform.Find("top/Btn_Refresh").gameObject;
    this.panel.BT_Refresh = gameObject1.GetComponent<UIButton>();
    gameObject1.SetActive(false);
    this.panel.LB_RefreshCost = this.transform.Find("top/Btn_Refresh/cost").gameObject.GetComponent<UILabel>();
    this.panel.TT_RefreshIcon = this.transform.Find("top/Btn_Refresh/icon").gameObject.GetComponent<UITexture>();
    this.panel.TT_RefreshIcon.mainTexture = (Texture) null;
    GameObject gameObject2 = this.transform.Find("OrgStoreItemSlot").gameObject;
    this.panel.orgSlot = gameObject2.GetComponent<ActivityFestivalShopSlot>();
    gameObject2.SetActive(false);
    this.panel.viewPort = this.transform.Find("svCamera").gameObject.GetComponent<UIViewport>();
    this.panel.slotScrollView = this.transform.Find("Items").gameObject.GetComponent<UIScrollView>();
    this.panel.slotContainer = this.transform.Find("Items/Grid").gameObject.GetComponent<UIGrid>();
  }

  public class Data
  {
    public List<int> items = new List<int>();
    public int activityId;
    public int lastRefreshTime;
    public int refreshCount;
    public FestivalExchangeInfo festivalExchange;
    public ItemStaticInfo item;

    public int itemCount
    {
      get
      {
        return DBManager.inst.DB_Item.GetQuantity(this.item.internalId);
      }
    }

    public int festivalCost
    {
      get
      {
        return this.festivalExchange.GetCostByRefreshCount(this.refreshCount);
      }
    }

    public void Decode(object res)
    {
      FestivalInfo data = ConfigManager.inst.DB_FestivalActives.GetData(this.activityId);
      if (data != null)
        this.festivalExchange = ConfigManager.inst.DB_FestivalExchange.GetData(data.exchange_group_id);
      if (this.festivalExchange != null)
        this.item = ConfigManager.inst.DB_Items.GetItem(this.festivalExchange.itemCostId);
      Hashtable inData1 = res as Hashtable;
      if (inData1 == null)
        return;
      DatabaseTools.UpdateData(inData1, "last_sync_time", ref this.lastRefreshTime);
      DatabaseTools.UpdateData(inData1, "buy_times", ref this.refreshCount);
      Hashtable inData2 = inData1[(object) "items"] as Hashtable;
      if (inData2 == null)
        return;
      this.items.Clear();
      int outData = 0;
      for (int index = 1; index <= inData2.Count; ++index)
      {
        DatabaseTools.UpdateData(inData2, index.ToString(), ref outData);
        this.items.Add(outData);
      }
    }

    public struct Param
    {
      public const string ACTIVITY_ID = "items";
      public const string LAST_REFRESH_TIME = "last_sync_time";
      public const string REFRESH_COUNT = "buy_times";
      public const string ITEMS = "items";
    }
  }

  [Serializable]
  public class Panel
  {
    public List<ActivityFestivalShopSlot> slots = new List<ActivityFestivalShopSlot>();
    public UIButton BT_Back;
    public UIViewport viewPort;
    public UITexture costIcon;
    public UILabel costValue;
    public UILabel refreshTime;
    public UIButton BT_Refresh;
    public UITexture TT_RefreshIcon;
    public UILabel LB_RefreshCost;
    public UIButton BT_FreeRefresh;
    public UIScrollView slotScrollView;
    public UIGrid slotContainer;
    public ActivityFestivalShopSlot orgSlot;
  }
}
