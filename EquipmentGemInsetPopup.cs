﻿// Decompiled with JetBrains decompiler
// Type: EquipmentGemInsetPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using GemConstant;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class EquipmentGemInsetPopup : Popup
{
  private List<GemItemSlot> m_ItemList = new List<GemItemSlot>();
  public GameObject m_GemItemSlot;
  public GameObject m_NoGemItem;
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  public UILabel title;
  public UILabel tip;
  public UILabel emptyTip;
  public HelpBtnComponent bt;
  private EquipmentGemInsetPopup.Parameter m_Parameter;
  private GemItemSlot m_CurrentSelection;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as EquipmentGemInsetPopup.Parameter;
    string source1 = "forge_armory_gemstone_socket_subtitle";
    string source2 = "toast_forge_gemstone_select_gemstone";
    string source3 = "forge_armory_no_suitable_gemstones_description";
    if (this.m_Parameter.slotType == SlotType.dragon_knight)
    {
      source1 = "forge_armory_dk_runestone_socket_subtitle";
      source2 = "toast_forge_dk_runestone_select_gemstone";
      source3 = "forge_armory_dk_no_suitable_runestones_description";
      this.bt.ID = "help_forge_dk_embed_runestone";
    }
    this.title.text = Utils.XLAT(source1);
    this.tip.text = Utils.XLAT(source2);
    this.emptyTip.text = Utils.XLAT(source3);
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  public void OnClosePressed()
  {
    this.ClearData();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnEmbedGemPressed()
  {
    if ((UnityEngine.Object) this.m_CurrentSelection != (UnityEngine.Object) null && this.m_CurrentSelection.Selected)
    {
      GemManager.Instance.Embed(this.m_CurrentSelection.GemData, this.m_Parameter.inventoryData, this.m_Parameter.slotIndex, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        if (this.m_Parameter.callback != null)
          this.m_Parameter.callback();
        this.OnClosePressed();
        AudioManager.Instance.StopAndPlaySound("sfx_city_gemstone_embed");
      }));
    }
    else
    {
      string source = "toast_forge_gemstone_select_gemstone";
      if (this.m_Parameter.slotType == SlotType.dragon_knight)
        source = "toast_forge_armory_dk_select_gemstone";
      UIManager.inst.toast.Show(Utils.XLAT(source), (System.Action) null, 2f, true);
    }
  }

  private void ClearData()
  {
    using (List<GemItemSlot>.Enumerator enumerator = this.m_ItemList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GemItemSlot current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ItemList.Clear();
  }

  private void UpdateUI()
  {
    this.ClearData();
    List<GemData> gemsBySlotType = DBManager.inst.DB_Gems.GetGemsBySlotType(this.m_Parameter.slotType);
    for (int index = 0; index < gemsBySlotType.Count; ++index)
    {
      GemData gemData = gemsBySlotType[index];
      if (gemData.GemType == this.m_Parameter.gemType && !gemData.Inlaid)
      {
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_GemItemSlot);
        gameObject.SetActive(true);
        gameObject.transform.parent = this.m_Grid.transform;
        gameObject.transform.localPosition = Vector3.zero;
        gameObject.transform.localRotation = Quaternion.identity;
        gameObject.transform.localScale = Vector3.one;
        GemItemSlot component = gameObject.GetComponent<GemItemSlot>();
        component.SetData(gemData, new System.Action<GemItemSlot>(this.OnGemSelected));
        this.m_ItemList.Add(component);
      }
      this.m_NoGemItem.SetActive(this.m_ItemList.Count == 0);
      this.m_Grid.sorting = UIGrid.Sorting.Custom;
      this.m_Grid.onCustomSort = new Comparison<Transform>(EquipmentGemInsetPopup.Comparison);
      this.m_Grid.Reposition();
      this.m_ScrollView.ResetPosition();
    }
  }

  private static int Comparison(Transform x, Transform y)
  {
    GemItemSlot component1 = x.GetComponent<GemItemSlot>();
    GemItemSlot component2 = y.GetComponent<GemItemSlot>();
    int num = component2.GemConfig.level - component1.GemConfig.level;
    if (num != 0)
      return num;
    return component1.GemConfig.internalId - component2.GemConfig.internalId;
  }

  private void OnGemSelected(GemItemSlot gemData)
  {
    this.m_CurrentSelection = gemData;
  }

  public class Parameter : Popup.PopupParameter
  {
    public SlotType slotType;
    public GemType gemType;
    public int slotIndex;
    public InventoryItemData inventoryData;
    public System.Action callback;
  }
}
