﻿// Decompiled with JetBrains decompiler
// Type: ConfigRuralBuildingLimitInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class ConfigRuralBuildingLimitInfo
{
  public Dictionary<string, int> limitDic = new Dictionary<string, int>();
  [Config(Name = "stronghold_level")]
  public int strongHoldLevel;
  [Config(Name = "farm_num")]
  public int farm_num;
  [Config(Name = "lumber_mill_num")]
  public int lumber_mill_num;
  [Config(Name = "mine_num")]
  public int mine_num;
  [Config(Name = "house_num")]
  public int house_num;
  [Config(Name = "military_tent_num")]
  public int military_tent_num;
  [Config(Name = "hospital_num")]
  public int hospital_num;

  public void BuildLimitDic()
  {
    this.limitDic.Add("farm", this.farm_num);
    this.limitDic.Add("lumber_mill", this.lumber_mill_num);
    this.limitDic.Add("mine", this.mine_num);
    this.limitDic.Add("house", this.house_num);
    this.limitDic.Add("military_tent", this.military_tent_num);
    this.limitDic.Add("hospital", this.hospital_num);
  }
}
