﻿// Decompiled with JetBrains decompiler
// Type: VfxManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class VfxManager
{
  private Dictionary<long, IVfx> _vfxMap = new Dictionary<long, IVfx>();
  private Queue<IVfx> _deleteCache = new Queue<IVfx>();
  private static VfxManager _instance;

  public static VfxManager Instance
  {
    get
    {
      if (VfxManager._instance == null)
        VfxManager._instance = new VfxManager();
      return VfxManager._instance;
    }
  }

  public void Startup()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnUpdate);
  }

  public void Terminate()
  {
    this.DeleteAll();
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnUpdate);
  }

  private void OnUpdate(int delta)
  {
    Dictionary<long, IVfx>.Enumerator enumerator = this._vfxMap.GetEnumerator();
    while (enumerator.MoveNext())
    {
      IVfx vfx = enumerator.Current.Value;
      if (vfx == null || !vfx.IsAlive)
        this._deleteCache.Enqueue(vfx);
    }
    while (this._deleteCache.Count > 0)
      this.Delete(this._deleteCache.Dequeue());
  }

  public bool IsAlive(long guid)
  {
    IVfx vfx = (IVfx) null;
    if (this._vfxMap.TryGetValue(guid, out vfx) && vfx != null)
      return vfx.IsAlive;
    return false;
  }

  public long CreateAndPlay(string uri, IVfxTarget target)
  {
    if (target == null)
      return -1;
    IVfx vfx = this.Create(uri);
    if (vfx == null)
      return -1;
    vfx.Play(target);
    return vfx.Guid;
  }

  public IVfx Get(long uid)
  {
    if (this._vfxMap.ContainsKey(uid))
      return this._vfxMap[uid];
    return (IVfx) null;
  }

  public long CreateAndPlay(string uri, Transform target)
  {
    if ((UnityEngine.Object) target == (UnityEngine.Object) null)
      return -1;
    IVfx vfx = this.Create(uri);
    if (vfx == null)
      return -1;
    vfx.Play(target);
    return vfx.Guid;
  }

  public IVfx Create(string uri)
  {
    if (string.IsNullOrEmpty(uri))
      return (IVfx) null;
    GameObject prefab = AssetManager.Instance.HandyLoad(uri, (System.Type) null) as GameObject;
    long longId = ClientIdCreater.CreateLongId(ClientIdCreater.IdType.EFFECT);
    GameObject go = PrefabManagerEx.Instance.Spawn(prefab, PrefabManagerEx.Instance.transform);
    if (!(bool) ((UnityEngine.Object) go))
      return (IVfx) null;
    IVfx component = (IVfx) go.GetComponent(typeof (IVfx));
    if (component == null)
    {
      if ((bool) ((UnityEngine.Object) go))
        PrefabManagerEx.Instance.Destroy(go);
      return (IVfx) null;
    }
    component.Guid = longId;
    this._vfxMap.Add(longId, component);
    return component;
  }

  public void Delete(long guid)
  {
    if (guid < 0L || !this._vfxMap.ContainsKey(guid))
      return;
    this.Delete(this._vfxMap[guid]);
  }

  public IVfx GetVfxByGuid(long guid)
  {
    IVfx vfx = (IVfx) null;
    this._vfxMap.TryGetValue(guid, out vfx);
    return vfx;
  }

  public void Delete(IVfx vfx)
  {
    if (vfx == null || !this._vfxMap.Remove(vfx.Guid))
      return;
    if ((bool) ((UnityEngine.Object) (vfx as Component)))
    {
      if (!PrefabManagerEx.IsAlive)
        return;
      PrefabManagerEx.Instance.Destroy(vfx.gameObject);
    }
    else
      D.warn((object) "vfx has destoryed without PrefabManager.");
  }

  public void DeleteAll()
  {
    Dictionary<long, IVfx>.Enumerator enumerator = this._vfxMap.GetEnumerator();
    while (enumerator.MoveNext())
    {
      IVfx vfx = enumerator.Current.Value;
      if (vfx != null && (bool) ((UnityEngine.Object) (vfx as Component)) && (vfx.IsAlive && PrefabManagerEx.IsAlive))
        PrefabManagerEx.Instance.Destroy(vfx.gameObject);
    }
    this._vfxMap.Clear();
  }
}
