﻿// Decompiled with JetBrains decompiler
// Type: EnterNextFloorConfirmPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class EnterNextFloorConfirmPopup : Popup
{
  private EnterNextFloorConfirmPopup.Parameter _parameter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._parameter = orgParam as EnterNextFloorConfirmPopup.Parameter;
  }

  public void OnButtonConfirmClicked()
  {
    if (this._parameter != null && this._parameter.ConfirmCallback != null)
      this._parameter.ConfirmCallback();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnButtonCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public System.Action ConfirmCallback;
  }
}
