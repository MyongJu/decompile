﻿// Decompiled with JetBrains decompiler
// Type: MessageFilter
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class MessageFilter
{
  private static bool isRunning = true;
  private static MessageFilter.IDList msgIds = new MessageFilter.IDList();
  private static MessageFilter.IDList fromIds = new MessageFilter.IDList();
  private static int[] duplicateIndex = new int[3]
  {
    -1,
    -1,
    -1
  };
  private static bool find = false;
  private const int MAX_COUNT = 3;

  public static void Start()
  {
    MessageFilter.isRunning = true;
  }

  public static void Stop()
  {
    MessageFilter.isRunning = false;
  }

  public static bool CanDispatch(long from, long mid)
  {
    if (!MessageFilter.isRunning)
      return true;
    bool flag = false;
    MessageFilter.msgIds.FindIndexs(mid);
    if (!MessageFilter.find)
    {
      MessageFilter.msgIds.Set(mid);
      MessageFilter.fromIds.Set(from);
    }
    else
    {
      for (int index = 0; index < MessageFilter.duplicateIndex.Length; ++index)
      {
        flag = MessageFilter.fromIds.IsDuplicate(from, MessageFilter.duplicateIndex[index]);
        if (flag)
          break;
      }
      if (!flag)
      {
        MessageFilter.msgIds.Set(mid);
        MessageFilter.fromIds.Set(from);
      }
    }
    if (flag)
      D.error((object) ("[MessageFilter] Message is duplicate from is " + (object) from + " mid is " + (object) mid));
    return !flag;
  }

  private class IDList
  {
    public long[] numbers = new long[3];
    private int _index;

    public void FindIndexs(long id)
    {
      MessageFilter.find = false;
      for (int index = 0; index < this.numbers.Length; ++index)
      {
        MessageFilter.duplicateIndex[index] = -1;
        if (this.numbers[index] == id)
        {
          MessageFilter.find = true;
          MessageFilter.duplicateIndex[index] = index;
        }
      }
    }

    public void Set(long id)
    {
      this.numbers[this.Next()] = id;
    }

    private int Index
    {
      get
      {
        return this._index;
      }
      set
      {
        this._index = value;
      }
    }

    public bool IsDuplicate(long id, int index = -1)
    {
      if (index > -1)
      {
        if (index < this.numbers.Length)
          return this.numbers[index] == id;
      }
      else
      {
        for (int index1 = 0; index1 < this.numbers.Length; ++index1)
        {
          if (this.numbers[index1] == id)
            return true;
        }
      }
      return false;
    }

    private int Next()
    {
      int index = this.Index;
      ++this.Index;
      if (3 <= this.Index)
        this.Index = 0;
      return index;
    }
  }
}
