﻿// Decompiled with JetBrains decompiler
// Type: SmallRoundSpell
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallRoundSpell
{
  private long _spellId;
  private DragonKnightTalentInfo _talent;
  public IRound iRound;

  public List<RoundResult> Spell(RoundPlayer trigger, List<RoundPlayer> targets)
  {
    List<RoundResult> roundResultList = new List<RoundResult>();
    int num1 = 0;
    RoundPlayer roundPlayer = (RoundPlayer) null;
    Hashtable resultData = new Hashtable();
    for (int index = 0; index < targets.Count; ++index)
    {
      RoundPlayer target = targets[index];
      if (roundPlayer == null)
        roundPlayer = target;
      RoundResult result = new RoundResult();
      result.Type = this.ResultType;
      if (this.HasDamage())
      {
        if (num1 < 0)
        {
          if (index > 0 && this.Talent.IsSplash())
          {
            string str = target.PlayerId.ToString() + "_" + (object) index;
            result.Value = Mathf.CeilToInt(this.Talent.activeValue2 * (float) num1);
            resultData.Add((object) ("main_damge" + str), (object) num1);
            resultData.Add((object) ("splash" + str), (object) result.Value);
          }
          else
            result.Value = this.CalcResult(this.GetDamage(trigger), this.GetCoefficient(), target);
        }
        else
          result.Value = this.CalcResult(this.GetDamage(trigger), this.GetCoefficient(), target);
      }
      else if (this.Talent.IsHeal())
        result.Value = this.CalcResult(this.GetDamage(trigger), this.GetCoefficient(), target);
      if (result.Value != 0)
      {
        target.SetHurt(result.Value);
        if (num1 == 0)
          num1 = result.Value;
      }
      result.Target = target;
      this.ResultFill(result);
      roundResultList.Add(result);
    }
    trigger.Mana -= this.Talent.manaCost;
    resultData.Add((object) "mana", (object) trigger.Mana);
    resultData.Add((object) "isHeal", (object) this.Talent.IsHeal());
    if (this.Talent.HasRebound())
    {
      int num2 = Mathf.CeilToInt((float) trigger.Health * this.Talent.activeValue3);
      trigger.SetHurt(-num2);
      resultData.Add((object) "rebund_spell", (object) -num2);
    }
    if (roundPlayer != null && this.Talent.magicType == "physics")
    {
      resultData.Add((object) "rebund", (object) -roundPlayer.Rebund);
      trigger.SetHurt(-roundPlayer.Rebund);
    }
    BattleManager.Instance.AddRoundLogData(this.iRound, resultData);
    return roundResultList;
  }

  public RoundResult GetRebound(RoundPlayer trigger, List<RoundPlayer> targets)
  {
    RoundResult roundResult = (RoundResult) null;
    if (this.Talent.magicType == "physics" && targets.Count > 0)
    {
      roundResult = new RoundResult();
      roundResult.Target = trigger;
      roundResult.Type = RoundResult.ResultType.Rebound;
      roundResult.Value = -targets[0].Rebund;
    }
    return roundResult;
  }

  protected virtual void ResultFill(RoundResult result)
  {
    if (this.Talent.buffId <= 0)
      return;
    BattleBuff buff = BattleBuff.Create(this.Talent.buffId, this.Talent.activeBuffRound);
    result.AddBuff(buff);
    BattleManager.Instance.AddRoundLogData(this.iRound, new Hashtable()
    {
      {
        (object) "buff",
        (object) buff.BuffId
      },
      {
        (object) "duration",
        (object) this.Talent.activeBuffRound
      }
    });
  }

  protected virtual RoundResult.ResultType ResultType
  {
    get
    {
      return RoundResult.ResultType.SkillDamage;
    }
  }

  protected bool HasDamage()
  {
    return this.Talent.HasDamge();
  }

  protected int GetDamage(RoundPlayer trigger)
  {
    int magicDamge = trigger.MagicDamge;
    string magicType = this.Talent.magicType;
    if ("physics" == magicType)
    {
      if (!this.Talent.IsSplash())
        return Mathf.CeilToInt((float) trigger.Damge + this.Talent.activeValue2);
      return trigger.Damge;
    }
    bool isEvil = trigger.PlayerCamp == RoundPlayer.Camp.Evil;
    List<BattleBuff> allBuffs = trigger.GetAllBuffs();
    DragonKnightData dragonData = DBManager.inst.DB_DragonKnight.Get(trigger.Uid);
    int num1 = 0;
    string key = magicType;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (SmallRoundSpell.\u003C\u003Ef__switch\u0024map54 == null)
      {
        // ISSUE: reference to a compiler-generated field
        SmallRoundSpell.\u003C\u003Ef__switch\u0024map54 = new Dictionary<string, int>(3)
        {
          {
            "fire",
            0
          },
          {
            "nature",
            1
          },
          {
            "water",
            2
          }
        };
      }
      int num2;
      // ISSUE: reference to a compiler-generated field
      if (SmallRoundSpell.\u003C\u003Ef__switch\u0024map54.TryGetValue(key, out num2))
      {
        switch (num2)
        {
          case 0:
            num1 = this.GetExtraDamge(trigger.FireDamge, DragonKnightAttibute.Type.FireDamge, isEvil, allBuffs, dragonData);
            break;
          case 1:
            num1 = this.GetExtraDamge(trigger.NatureDamge, DragonKnightAttibute.Type.NaturalDamge, isEvil, allBuffs, dragonData);
            break;
          case 2:
            num1 = this.GetExtraDamge(trigger.WaterDamge, DragonKnightAttibute.Type.WaterDamge, isEvil, allBuffs, dragonData);
            break;
        }
      }
    }
    int num3 = magicDamge + num1;
    if (!this.Talent.IsSplash())
      num3 = Mathf.CeilToInt((float) num3 + this.Talent.activeValue2);
    return num3;
  }

  private int GetExtraDamge(int baseValue, DragonKnightAttibute.Type type, bool isEvil = false, List<BattleBuff> buffs = null, DragonKnightData dragonData = null)
  {
    return AttributeCalcHelper.Instance.CalcAttributeInBattle((float) baseValue, type, isEvil, buffs, dragonData);
  }

  protected DragonKnightTalentInfo Talent
  {
    get
    {
      if (this._talent == null)
        this._talent = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo((int) this.SpellId);
      return this._talent;
    }
  }

  protected long SpellId
  {
    get
    {
      return this._spellId;
    }
    set
    {
      this._spellId = value;
    }
  }

  protected virtual int CalcResult(int magicDamge, double coefficient, RoundPlayer underAttacker)
  {
    int num = !(this.Talent.magicType == "physics") ? DamgeCalcUtils.CalcSpellDamge(magicDamge, coefficient) : DamgeCalcUtils.CalcPhysicsDamge(magicDamge, underAttacker.Defense, coefficient);
    this.AddNormalLog(magicDamge, coefficient, underAttacker.Defense, num);
    return num;
  }

  protected void AddNormalLog(int magicDamge, double coefficient, int defendse, int value)
  {
    BattleManager.Instance.AddRoundLogData(this.iRound, new Hashtable()
    {
      {
        (object) "type",
        (object) "spell"
      },
      {
        (object) "skill",
        (object) this.Talent.internalId
      },
      {
        (object) "magicType",
        (object) this.Talent.magicType
      },
      {
        (object) nameof (magicDamge),
        (object) magicDamge
      },
      {
        (object) nameof (coefficient),
        (object) coefficient
      },
      {
        (object) "defense",
        (object) defendse
      },
      {
        (object) nameof (value),
        (object) value
      }
    });
  }

  protected double GetCoefficient()
  {
    return (double) this.Talent.activeValue1;
  }

  public static SmallRoundSpell Create(long skillId)
  {
    SmallRoundSpell smallRoundSpell = !ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo((int) skillId).IsHeal() ? (SmallRoundSpell) new NormalDamgeSpell() : (SmallRoundSpell) new HealSpell();
    smallRoundSpell.SpellId = skillId;
    return smallRoundSpell;
  }
}
