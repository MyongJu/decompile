﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomViewController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Chat;
using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ChatRoomViewController : MonoBehaviour
{
  private List<ChannelBar> _channelBars = new List<ChannelBar>();
  public GameObject roomApplySymbol;
  public GameObject invitingSymbol;
  public GameObject tabInvitingSymbol;
  public UIButton BT_ChannelBack;
  public UIButton BT_ChannelManager;
  public Transform channelContainer;
  public UIScrollView channelScrollView;
  public UITable channelTable;
  public ChannelBar orgChannelBar;
  public Transform noRoomWarning;
  public UILabel channelTitle;
  public Transform inputConatiner;
  public Transform msgContainer;
  public UILabel unReadRoomMessage;
  private int _channelBarTop;
  private ChatManager _chatManager;
  private ChatChannel _channel;
  public ChatDlg chatDlg;
  public GameObject content;
  private bool chanelDisplay;
  private bool showingRoom;

  public void Show(ChatManager chatManager, ChatChannel chatChannel, ChatDlg chatDlg)
  {
    this.showingRoom = true;
    this.content.SetActive(true);
    this._channel = chatChannel;
    this._chatManager = chatManager;
    this.chatDlg = chatDlg;
    this.chanelDisplay = false;
    this.channelTable.onReposition += new UITable.OnReposition(this.OnRoomTableReset);
    this._chatManager.onMessageReceived = new System.Action<ChatMessage>(this.OnMessageReceived);
    DBManager.inst.DB_room.onDataRemove += new System.Action<long>(this.OnRoomRemove);
    DBManager.inst.DB_room.onDataChanged += new System.Action<long>(this.OnRoomChanged);
    DBManager.inst.DB_room.onMemberChanged += new System.Action<long, long>(this.OnRoomMemberChanged);
    DBManager.inst.DB_room.onMemberRemoved += new System.Action<long>(this.OnRoomChanged);
    this.Refresh();
  }

  public void Hide()
  {
    this.showingRoom = false;
    DBManager.inst.DB_room.onDataRemove -= new System.Action<long>(this.OnRoomRemove);
    DBManager.inst.DB_room.onDataChanged -= new System.Action<long>(this.OnRoomChanged);
    DBManager.inst.DB_room.onMemberChanged -= new System.Action<long, long>(this.OnRoomMemberChanged);
    DBManager.inst.DB_room.onMemberRemoved -= new System.Action<long>(this.OnRoomChanged);
    this.BT_ChannelBack.gameObject.SetActive(false);
    this.channelContainer.gameObject.SetActive(false);
    this.BT_ChannelManager.gameObject.SetActive(false);
    this.roomApplySymbol.SetActive(false);
    this.invitingSymbol.SetActive(false);
    this.content.SetActive(false);
  }

  public void OnCreateRoomClick()
  {
    UIManager.inst.OpenPopup("Chat/ChatCreateRoomPopup", (Popup.PopupParameter) null);
  }

  public void OnJoinRoomClick()
  {
    UIManager.inst.OpenPopup("Chat/ChatJoinRoomPopup", (Popup.PopupParameter) null);
  }

  public void OnInviteOrApplyClick()
  {
    UIManager.inst.OpenPopup("Chat/ChatRoomApplyOrInvitePopup", (Popup.PopupParameter) null);
  }

  private void Refresh()
  {
    this.channelTitle.text = ScriptLocalization.Get("chat_room_list_title", true);
    this.BT_ChannelBack.gameObject.SetActive(false);
    this.BT_ChannelManager.gameObject.SetActive(false);
    this.roomApplySymbol.SetActive(false);
    this.invitingSymbol.SetActive(false);
    this.channelContainer.gameObject.SetActive(true);
    this.RefreshRoomList();
    this.UpdateRoomTabSymbol();
  }

  private void RefreshRoomList()
  {
    for (int index = 0; index < this._channelBars.Count; ++index)
      this._channelBars[index].gameObject.SetActive(false);
    this._channelBarTop = 0;
    if (DBManager.inst.DB_room._datas.Count > 0)
    {
      this.noRoomWarning.gameObject.SetActive(false);
      List<ChatRoomData> chatRoomDataList = new List<ChatRoomData>();
      Dictionary<long, ChatRoomData>.ValueCollection.Enumerator enumerator = DBManager.inst.DB_room._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.members.ContainsKey(PlayerData.inst.uid) && enumerator.Current.members[PlayerData.inst.uid].IsNormalMember())
          chatRoomDataList.Add(enumerator.Current);
      }
      chatRoomDataList.Sort((Comparison<ChatRoomData>) ((x, y) =>
      {
        ChatChannel chatChannel1 = GameEngine.Instance.ChatManager.GetChatChannel(x.channelId, 0L);
        ChatChannel chatChannel2 = GameEngine.Instance.ChatManager.GetChatChannel(y.channelId, 0L);
        if (chatChannel1 != null && chatChannel2 != null)
          return -chatChannel1.unreadCount.CompareTo(chatChannel2.unreadCount);
        return 0;
      }));
      for (int index = 0; index < chatRoomDataList.Count; ++index)
        this.AddRoom(chatRoomDataList[index]);
    }
    else
      this.noRoomWarning.gameObject.SetActive(true);
    this.channelTable.repositionNow = true;
    this.channelScrollView.ResetPosition();
  }

  private void AddRoom(ChatRoomData room)
  {
    if (this._channelBarTop >= this._channelBars.Count)
    {
      GameObject gameObject = NGUITools.AddChild(this.channelTable.gameObject, this.orgChannelBar.gameObject);
      gameObject.SetActive(true);
      gameObject.name = string.Format("channel_{0}", (object) (100 + this._channelBars.Count));
      this._channelBars.Add(gameObject.GetComponent<ChannelBar>());
    }
    this._channelBars[this._channelBarTop].gameObject.SetActive(true);
    this._channelBars[this._channelBarTop].SetInfo(room);
    this._channelBars[this._channelBarTop].onChannelBarClick = new System.Action<long>(this.OpenRoomChannel);
    ++this._channelBarTop;
    this.channelTable.repositionNow = true;
  }

  private void OpenRoomChannel(long channelId)
  {
    this._channel = this._chatManager.GetChatChannel(channelId, 0L);
    if (this._channel == null)
      return;
    this.BT_ChannelBack.gameObject.SetActive(true);
    this.BT_ChannelBack.onClick.Clear();
    this.BT_ChannelBack.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.chatDlg.OnTagRoomButtonClick)));
    this.BT_ChannelManager.gameObject.SetActive(true);
    this.BT_ChannelManager.onClick.Clear();
    this.BT_ChannelManager.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnManagerClick)));
    ChatRoomData roomByChannelId = DBManager.inst.DB_room.GetRoomByChannelId(channelId);
    this.roomApplySymbol.SetActive(roomByChannelId.members[PlayerData.inst.uid].IsManageMember() && roomByChannelId.ApplyingCount() > 0);
    this.channelTitle.text = this._channel.channelName;
    this.channelContainer.gameObject.SetActive(false);
    this.msgContainer.gameObject.SetActive(true);
    this.inputConatiner.gameObject.SetActive(true);
    this._channel.ReadMessages();
    this.chatDlg.SelectChannel(channelId, 0L);
    this.chatDlg.CheckSilence();
    this.chanelDisplay = true;
  }

  public void OnManagerClick()
  {
    if (this._channel == null || this.chatDlg.isPrivateChannel)
      return;
    ChatRoomData roomByChannelId = DBManager.inst.DB_room.GetRoomByChannelId(this._channel.channelId);
    if (roomByChannelId == null)
      return;
    UIManager.inst.OpenPopup("Chat/ChatRoomControllerPopUp", (Popup.PopupParameter) new ChatRoomControllerPopup.Parameter()
    {
      roomId = roomByChannelId.roomId
    });
  }

  private void OnRoomChanged(long roomId)
  {
    ChatRoomData chatRoomData = DBManager.inst.DB_room.Get(roomId);
    if (chatRoomData == null || this._channel == null || chatRoomData.channelId != this._channel.channelId)
      return;
    if (this.channelContainer.gameObject.activeSelf)
      this.RefreshRoomList();
    if (this.inputConatiner.gameObject.activeSelf)
      this.chatDlg.CheckSilence();
    this.UpdateRoomTabSymbol();
  }

  private void OnRoomMemberChanged(long roomId, long uid)
  {
    if (this.channelContainer.gameObject.activeSelf)
      this.RefreshRoomList();
    this.UpdateRoomTabSymbol();
  }

  private void OnMessageReceived(ChatMessage message)
  {
    if (this.channelContainer.gameObject.activeSelf)
      this.RefreshRoomList();
    this.UpdateRoomTabSymbol();
  }

  public void OnRoomTableReset()
  {
    this.channelScrollView.ResetPosition();
  }

  private void OnRoomRemove(long roomId)
  {
    ChatRoomData chatRoomData = DBManager.inst.DB_room.Get(roomId);
    if (chatRoomData != null && this._channel != null && chatRoomData.channelId == this._channel.channelId)
    {
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
      this.UpdateRoomTabSymbol();
  }

  private void OnRoomChanged(long roomId, long uid)
  {
    this.OnRoomChanged(roomId);
  }

  public void UpdateRoomTabSymbol()
  {
    this.tabInvitingSymbol.SetActive(false);
    this.unReadRoomMessage.gameObject.SetActive(false);
    int num = ChatRoomManager.UnReadRoomMessageCount();
    if (ChatRoomManager.HasInviting() || ChatRoomManager.HasApplyingMyRoom())
      this.tabInvitingSymbol.SetActive(true);
    else if (num > 0)
    {
      this.unReadRoomMessage.gameObject.SetActive(true);
      this.unReadRoomMessage.text = num.ToString();
    }
    this.invitingSymbol.SetActive(ChatRoomManager.HasInviting() && this.showingRoom);
    if (!this.chanelDisplay || this._channel == null)
      return;
    ChatRoomData roomByChannelId = DBManager.inst.DB_room.GetRoomByChannelId(this._channel.channelId);
    if (roomByChannelId == null || !roomByChannelId.members.ContainsKey(PlayerData.inst.uid))
      return;
    this.roomApplySymbol.SetActive(roomByChannelId.members[PlayerData.inst.uid].IsManageMember() && roomByChannelId.ApplyingCount() > 0 && this.showingRoom);
  }
}
