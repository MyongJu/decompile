﻿// Decompiled with JetBrains decompiler
// Type: JackpotRankSlotRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class JackpotRankSlotRenderer : MonoBehaviour
{
  public UILabel player;
  public UILabel winGoldAmount;
  public UILabel winTime;
  public UITexture winnerHeadPortrait;
  public UISprite dividingLineSprite;
  private JackpotRankInfo jackpotRankInfo;
  private bool showDividingLine;

  public void SetData(JackpotRankInfo jri, bool showDividingLine = true)
  {
    this.jackpotRankInfo = jri;
    this.showDividingLine = showDividingLine;
    this.AddEventHandler();
    this.UpdateUI();
    this.UpdateWinnerHeadInfo();
  }

  private void UpdateUI()
  {
    if (this.jackpotRankInfo == null)
      return;
    this.player.text = string.Empty;
    UILabel player1 = this.player;
    player1.text = player1.text + "K" + (object) this.jackpotRankInfo.winnerKingdom + " ";
    if (!string.IsNullOrEmpty(this.jackpotRankInfo.winnerAllianceAcronym))
    {
      UILabel player2 = this.player;
      player2.text = player2.text + "(" + this.jackpotRankInfo.winnerAllianceAcronym + ")";
    }
    this.player.text += this.jackpotRankInfo.winner;
    this.winGoldAmount.text = Utils.FormatThousands(this.jackpotRankInfo.winGoldAmount.ToString());
    int num1 = NetServerTime.inst.ServerTimestamp - this.jackpotRankInfo.winTime;
    int num2 = num1 / 3600 / 24;
    int num3 = num1 / 3600 % 24;
    int num4 = num1 % 3600 / 60;
    this.winTime.text = ScriptLocalization.GetWithPara("chat_time_display", new Dictionary<string, string>()
    {
      {
        "d",
        num2 <= 0 ? string.Empty : num2.ToString() + Utils.XLAT("chat_time_day")
      },
      {
        "h",
        num3 <= 0 ? string.Empty : num3.ToString() + Utils.XLAT("chat_time_hour")
      },
      {
        "m",
        num4 < 0 ? string.Empty : num4.ToString() + Utils.XLAT("chat_time_min")
      }
    }, true);
    NGUITools.SetActive(this.dividingLineSprite.gameObject, this.showDividingLine);
  }

  private void UpdateWinnerHeadInfo()
  {
    if (this.jackpotRankInfo == null)
      return;
    CustomIconLoader.Instance.requestCustomIcon(this.winnerHeadPortrait, "Texture/Hero/Portrait_Icon/player_portrait_icon_" + (!string.IsNullOrEmpty(this.jackpotRankInfo.winnerPortrait) ? this.jackpotRankInfo.winnerPortrait : "0"), this.jackpotRankInfo.winnerImage, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.winnerHeadPortrait, this.jackpotRankInfo.winnerLordTitleId, 1);
  }

  public void ClearData()
  {
    this.RemoveEventHandler();
  }

  private void OnSecondEventHandler(int time)
  {
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEventHandler);
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEventHandler);
  }
}
