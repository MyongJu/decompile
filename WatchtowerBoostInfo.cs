﻿// Decompiled with JetBrains decompiler
// Type: WatchtowerBoostInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class WatchtowerBoostInfo : MonoBehaviour
{
  public GameObject m_BoostLocked;
  public GameObject m_BoostUnlocked;

  public void SetMarchDetailData(Hashtable data)
  {
    if (WatchtowerUtilities.GetWatchtowerLevel() >= 15)
    {
      this.m_BoostLocked.SetActive(false);
      this.m_BoostUnlocked.SetActive(true);
    }
    else
    {
      this.m_BoostLocked.SetActive(true);
      this.m_BoostUnlocked.SetActive(false);
    }
  }
}
