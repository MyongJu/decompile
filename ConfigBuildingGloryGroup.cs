﻿// Decompiled with JetBrains decompiler
// Type: ConfigBuildingGloryGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigBuildingGloryGroup
{
  private List<BuildingGloryGroupInfo> _buildingGloryGroupInfoList = new List<BuildingGloryGroupInfo>();
  private Dictionary<string, BuildingGloryGroupInfo> _datas;
  private Dictionary<int, BuildingGloryGroupInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<BuildingGloryGroupInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, BuildingGloryGroupInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._buildingGloryGroupInfoList.Add(enumerator.Current);
    }
  }

  public void Clear()
  {
    if (this._datas != null)
      this._datas.Clear();
    if (this._dicByUniqueId != null)
      this._dicByUniqueId.Clear();
    if (this._buildingGloryGroupInfoList == null)
      return;
    this._buildingGloryGroupInfoList.Clear();
  }

  public List<BuildingGloryGroupInfo> GetBuildingGloryGroupInfoList()
  {
    return this._buildingGloryGroupInfoList;
  }

  public BuildingGloryGroupInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (BuildingGloryGroupInfo) null;
  }

  public BuildingGloryGroupInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (BuildingGloryGroupInfo) null;
  }
}
