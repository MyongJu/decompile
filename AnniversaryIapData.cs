﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryIapData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class AnniversaryIapData
{
  private Dictionary<int, int> _packageDict = new Dictionary<int, int>();
  private long _payGoldNum;
  private long _haveReceivedPropsNum;

  public long PayGoldNum
  {
    get
    {
      return this._payGoldNum;
    }
  }

  public long HaveReceivedPropsNum
  {
    get
    {
      return this._haveReceivedPropsNum;
    }
  }

  public long CanReceivePropsNum
  {
    get
    {
      long num = 0;
      if (AnniversaryIapPayload.Instance.TopupGoldNumber != 0)
        num = this._payGoldNum / (long) AnniversaryIapPayload.Instance.TopupGoldNumber * (long) AnniversaryIapPayload.Instance.TopupGoldReturnNumber - this._haveReceivedPropsNum;
      return num;
    }
  }

  public Dictionary<int, int> PackageDict
  {
    get
    {
      return this._packageDict;
    }
  }

  public void Decode(Hashtable data)
  {
    if (data == null)
      return;
    DatabaseTools.UpdateData(data, "pay_gold", ref this._payGoldNum);
    DatabaseTools.UpdateData(data, "have_to_receive", ref this._haveReceivedPropsNum);
    this._packageDict.Clear();
    if (!data.ContainsKey((object) "package_list"))
      return;
    ArrayList arrayList = data[(object) "package_list"] as ArrayList;
    if (arrayList == null)
      return;
    for (int index = 0; index < arrayList.Count; ++index)
    {
      Hashtable hashtable = arrayList[index] as Hashtable;
      if (hashtable != null)
      {
        int result1 = 0;
        int result2 = 0;
        if (hashtable.ContainsKey((object) "pid"))
          int.TryParse(hashtable[(object) "pid"].ToString(), out result1);
        if (hashtable.ContainsKey((object) "number"))
          int.TryParse(hashtable[(object) "number"].ToString(), out result2);
        if (!this._packageDict.ContainsKey(result1))
          this._packageDict.Add(result1, result2);
      }
    }
  }

  public class Param
  {
    public const string PACKAGE_LIST = "package_list";
    public const string PAY_GOLD_NUM = "pay_gold";
    public const string HAVE_RECEIVED_PROPS_NUM = "have_to_receive";
  }
}
