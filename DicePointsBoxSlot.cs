﻿// Decompiled with JetBrains decompiler
// Type: DicePointsBoxSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DicePointsBoxSlot : MonoBehaviour
{
  private int _selectIndex = -1;
  public UILabel BoxDescribe;
  public DiceRewardRender[] UIRewards;
  public UIButton button;
  public UILabel buttonDesc;
  private DicePointsBox _boxData;

  public event DicePointsBoxSlot.OnExchangRewardHandler OnExchangReward;

  private void Start()
  {
    for (int index = 0; index < this.UIRewards.Length; ++index)
    {
      this.UIRewards[index].Index = index;
      this.UIRewards[index].OnRewardClickHandler += new DiceRewardRender.ClickEventHandler(this.onRewardClick);
    }
    this.BoxDescribe.text = ScriptLocalization.GetWithPara("tavern_dice_choose_reward_tip", new Dictionary<string, string>()
    {
      {
        "0",
        Utils.FormatThousands(this._boxData.Score.ToString())
      }
    }, true);
    this.UpdateUI();
  }

  public void UpdateData(DicePointsBox box)
  {
    this._boxData.SelectedIndex = box.SelectedIndex;
  }

  public void UpdateUI()
  {
    this.updateButtonState();
    this.updateRewardsState();
  }

  public void SetData(DicePointsBox box)
  {
    this._boxData = box;
    if (box.Rewards == null)
      return;
    List<Reward.RewardsValuePair> rewards = box.Rewards;
    int index = 0;
    using (List<Reward.RewardsValuePair>.Enumerator enumerator = rewards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Reward.RewardsValuePair current = enumerator.Current;
        if (ConfigManager.inst.DB_Items.GetItem(current.internalID) != null)
        {
          this.UIRewards[index].icon.SetData(current.internalID, string.Empty, true);
          this.UIRewards[index].desc.text = current.value.ToString();
          this.UIRewards[index].gameObject.SetActive(true);
          ++index;
        }
      }
    }
  }

  public void OnExchangeButtonClick()
  {
    if (this._selectIndex == -1)
      UIManager.inst.toast.Show(ScriptLocalization.Get("tavern_dice_choose_reward_title", true), (System.Action) null, 1f, false);
    else
      DicePayload.Instance.ExchangeDiceReward(this._boxData.Level, this._selectIndex + 1, (System.Action<bool, object>) ((succ, ret) =>
      {
        if (!succ || this.OnExchangReward == null)
          return;
        Reward.RewardsValuePair reward = this._boxData.Rewards[this._selectIndex];
        this.OnExchangReward(reward.internalID, reward.value);
      }));
  }

  private void onRewardClick(int index)
  {
    if (this._boxData.State != DicePointsBox.BoxState.active)
      return;
    this._selectIndex = index;
    this.clearRewardsState();
    this.UIRewards[index].SetState(DiceRewardRender.State.selected);
  }

  private void updateButtonState()
  {
    this.buttonDesc.gameObject.SetActive(false);
    this.button.gameObject.SetActive(true);
    this.button.isEnabled = true;
    if (this._boxData.State == DicePointsBox.BoxState.disabled)
    {
      this.button.isEnabled = false;
    }
    else
    {
      if (this._boxData.State != DicePointsBox.BoxState.opened)
        return;
      this.button.gameObject.SetActive(false);
      this.buttonDesc.gameObject.SetActive(true);
    }
  }

  private void updateRewardsState()
  {
    this.clearRewardsState();
    if (this._boxData.SelectedIndex == 0)
      return;
    this.UIRewards[this._boxData.SelectedIndex - 1].SetState(DiceRewardRender.State.gotten);
  }

  private void clearRewardsState()
  {
    foreach (DiceRewardRender uiReward in this.UIRewards)
      uiReward.ClearState();
  }

  public delegate void OnExchangRewardHandler(int itemID, int count);
}
