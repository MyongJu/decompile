﻿// Decompiled with JetBrains decompiler
// Type: SplashDataConfig
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class SplashDataConfig
{
  public static Dictionary<SplashDataConfig.Phase, float> Key2Values = new Dictionary<SplashDataConfig.Phase, float>();
  public static float CACHE_BUNDLE_TIME = 80f;
  public static int CACHE_BUNDLE_COUT_FRAME = 5;
  public static float PRASE_CONFIG_TIME = 1f;
  public static int PRASE_CONFIG_COUNT_FRAME = 1;
  public static float PRLOAD_UI_TIME = 5f;
  private static bool init = false;

  public static void Init()
  {
    if (SplashDataConfig.init)
      return;
    SplashDataConfig.init = true;
    SplashDataConfig.Key2Values.Add(SplashDataConfig.Phase.Logo, 0.0f);
    SplashDataConfig.Key2Values.Add(SplashDataConfig.Phase.First, 0.01f);
    SplashDataConfig.Key2Values.Add(SplashDataConfig.Phase.Init, 0.02f);
    SplashDataConfig.Key2Values.Add(SplashDataConfig.Phase.ClientVersion, 0.03f);
    SplashDataConfig.Key2Values.Add(SplashDataConfig.Phase.SetupSDK, 0.04f);
    SplashDataConfig.Key2Values.Add(SplashDataConfig.Phase.ConfigFileVersion, 0.05f);
    SplashDataConfig.Key2Values.Add(SplashDataConfig.Phase.BunldeInfoVersion, 0.3f);
    SplashDataConfig.Key2Values.Add(SplashDataConfig.Phase.LoadBundles, 0.6f);
    SplashDataConfig.Key2Values.Add(SplashDataConfig.Phase.LoadConfigFiles, 0.65f);
    SplashDataConfig.Key2Values.Add(SplashDataConfig.Phase.Prelogin, 0.67f);
    SplashDataConfig.Key2Values.Add(SplashDataConfig.Phase.LoadUserData, 0.7f);
    SplashDataConfig.Key2Values.Add(SplashDataConfig.Phase.ConnectRTM, 0.8f);
    SplashDataConfig.Key2Values.Add(SplashDataConfig.Phase.LoadExtraData, 0.85f);
    SplashDataConfig.Key2Values.Add(SplashDataConfig.Phase.LoadPreUI, 0.9f);
    SplashDataConfig.Key2Values.Add(SplashDataConfig.Phase.PreOpenUpdate, 0.95f);
  }

  public static string GetTipFromPhaseKey(SplashDataConfig.Phase phase)
  {
    string Term = string.Empty;
    switch (phase)
    {
      case SplashDataConfig.Phase.Init:
        Term = "load_setup_tip";
        break;
      case SplashDataConfig.Phase.ClientVersion:
        Term = "load_version_tip";
        break;
      case SplashDataConfig.Phase.SetupSDK:
        Term = "load_set_up_sdk_tip";
        break;
      case SplashDataConfig.Phase.ConfigFileVersion:
        Term = "load_configversion_tip";
        break;
      case SplashDataConfig.Phase.BunldeInfoVersion:
        Term = "load_bundleversion_tip";
        break;
      case SplashDataConfig.Phase.LoadBundles:
        Term = "load_cache_bundle";
        break;
      case SplashDataConfig.Phase.LoadConfigFiles:
        Term = "load_parse_config";
        break;
      case SplashDataConfig.Phase.LoadUserData:
        Term = "load_data_tip";
        break;
      case SplashDataConfig.Phase.First:
        Term = "load_setup_tip";
        break;
      case SplashDataConfig.Phase.ConnectRTM:
        Term = "load_connect_rtm";
        break;
      case SplashDataConfig.Phase.LoadPreUI:
        Term = "load_scene_tip";
        break;
      case SplashDataConfig.Phase.PreOpenUpdate:
        Term = "load_cache_bundle";
        break;
      case SplashDataConfig.Phase.LoadExtraData:
        Term = "load_scene_tip";
        break;
      case SplashDataConfig.Phase.OTAUpdate:
        Term = "load_scene_tip";
        break;
      case SplashDataConfig.Phase.Prelogin:
        Term = "id_uppercase_tap_to_start";
        break;
    }
    if (string.IsNullOrEmpty(Term))
      return string.Empty;
    return ScriptLocalization.Get(Term, true);
  }

  public static float GetValue(SplashDataConfig.Phase phase)
  {
    if (SplashDataConfig.Key2Values.ContainsKey(phase))
      return SplashDataConfig.Key2Values[phase];
    return 0.0f;
  }

  public enum Phase
  {
    None,
    Init,
    ClientVersion,
    SetupSDK,
    ConfigFileVersion,
    BunldeInfoVersion,
    LoadBundles,
    LoadConfigFiles,
    LoadUserData,
    First,
    ConnectRTM,
    LoadPreUI,
    PreOpenUpdate,
    LoadExtraData,
    OTAUpdate,
    Logo,
    Prelogin,
  }
}
