﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightSkillSettingItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class DragonKnightSkillSettingItemRenderer : MonoBehaviour
{
  public UITexture skillTexture;
  public UILabel skillName;
  public UILabel skillHintText;
  public UILabel skillAssignedHint;
  private DragonKnightTalentInfo _talentInfo;

  public DragonKnightTalentInfo TalentInfo
  {
    get
    {
      return this._talentInfo;
    }
    set
    {
      this._talentInfo = value;
    }
  }

  public void SetData(DragonKnightTalentInfo talentInfo)
  {
    this._talentInfo = talentInfo;
    this.AddEventHandler();
    this.UpdateUI();
  }

  public void ClearData()
  {
    this.RemoveEventHandler();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_DragonKnightDungeon.onDataUpdate += new System.Action<DragonKnightDungeonData>(this.OnDungeonDataUpdated);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_DragonKnightDungeon.onDataUpdate -= new System.Action<DragonKnightDungeonData>(this.OnDungeonDataUpdated);
  }

  private void OnDungeonDataUpdated(DragonKnightDungeonData dungeonData)
  {
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this._talentInfo == null)
      return;
    this.skillName.text = this._talentInfo.LocName;
    this.skillHintText.text = this._talentInfo.LocDescription;
    if ((UnityEngine.Object) this.skillTexture.mainTexture == (UnityEngine.Object) null || this.skillTexture.mainTexture.name != this._talentInfo.image)
      BuilderFactory.Instance.HandyBuild((UIWidget) this.skillTexture, this._talentInfo.imagePath, (System.Action<bool>) null, true, false, string.Empty);
    DragonKnightDungeonData knightDungeonData = DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L);
    NGUITools.SetActive(this.skillAssignedHint.gameObject, knightDungeonData != null && knightDungeonData.IsSkillAssigned(this._talentInfo.internalId));
  }
}
