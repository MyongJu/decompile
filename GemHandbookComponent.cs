﻿// Decompiled with JetBrains decompiler
// Type: GemHandbookComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GemHandbookComponent : MonoBehaviour
{
  public System.Action<GemHandbookComponent> OnSelectedHandler;
  public UITexture icon;
  public GameObject selected;
  public ConfigEquipmentGemInfo gemConfigInfo;
  private int _itemId;
  public UITexture bacgourd;

  public void FeedData(ConfigEquipmentGemInfo gemInfo)
  {
    this.gemConfigInfo = gemInfo;
    this.UpdateUI();
  }

  public void UpdateUI()
  {
    this.Select = false;
    int itemId = this.gemConfigInfo.itemID;
    this._itemId = itemId;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo == null)
      return;
    Utils.SetItemBackground(this.bacgourd, itemId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, itemStaticInfo.ImagePath, (System.Action<bool>) null, false, true, string.Empty);
  }

  public void OnPressItem()
  {
    Utils.DelayShowTip(this._itemId, this.transform, 0L, 0L, 0);
  }

  public void OnReleaseItem()
  {
    Utils.StopShowItemTip();
  }

  public bool Select
  {
    set
    {
      NGUITools.SetActive(this.selected, value);
    }
  }

  public void OnClick()
  {
    if (this.OnSelectedHandler == null)
      return;
    this.OnSelectedHandler(this);
  }
}
