﻿// Decompiled with JetBrains decompiler
// Type: TroopsInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class TroopsInfo : MonoBehaviour
{
  [SerializeField]
  public long marchIdShowed = -1;
  [SerializeField]
  public long ownerIdShowed = -1;
  public MarchViewControler troopsView;
  private MarchData _marchData;

  public MarchData marchData
  {
    set
    {
      this._marchData = value;
      if (this._marchData != null)
      {
        this.marchIdShowed = this._marchData.marchId;
        this.ownerIdShowed = this._marchData.ownerUid;
      }
      else
      {
        this.marchIdShowed = 0L;
        this.ownerIdShowed = 0L;
      }
    }
    get
    {
      return this._marchData;
    }
  }

  public void Dispose()
  {
    this.marchData = (MarchData) null;
  }
}
