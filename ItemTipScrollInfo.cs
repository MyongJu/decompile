﻿// Decompiled with JetBrains decompiler
// Type: ItemTipScrollInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class ItemTipScrollInfo : MonoBehaviour
{
  private BagType bagType = BagType.DragonKnight;
  private int _itemId;
  public EquipmentComponent equipComponent;
  public EquipmentScrollComponent scrollEquipment;
  private ConfigEquipmentScrollInfo scrollInfo;
  public UILabel desc;
  public UITable table;

  public void SetData(int itemId)
  {
    this._itemId = itemId;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._itemId);
    if (itemStaticInfo != null)
    {
      if (itemStaticInfo.Type == ItemBag.ItemType.equipment_scroll)
        this.bagType = BagType.Hero;
      this.desc.text = itemStaticInfo.LocDescription;
    }
    this.scrollInfo = ConfigManager.inst.GetEquipmentScroll(this.bagType).GetDataByItemID(this._itemId);
    this.DrawEquipment();
    this.DrawScroll();
    this.table.Reposition();
  }

  public Benefits GetBenefit()
  {
    return ConfigManager.inst.GetEquipmentProperty(this.bagType).GetDataByEquipIDAndEnhanceLevel(this.scrollInfo.equipmenID, 0).benefits;
  }

  public bool HadGemSlots()
  {
    ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(this.bagType).GetData(this.scrollInfo.equipmenID);
    int num = 0;
    if (data != null)
    {
      List<GemSlotConditions.Condition> conditionList = data.GemSlotConditions.ConditionList;
      for (int index = 0; index < conditionList.Count && conditionList[index].requiredLv <= 0; ++index)
        ++num;
    }
    return num > 0;
  }

  public ConfigEquipmentScrollInfo GetScrollInfo()
  {
    return this.scrollInfo;
  }

  public int GetEquipSuitID()
  {
    ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(this.bagType).GetData(this.scrollInfo.equipmenID);
    if (data != null && data.suitGroup != 0)
      return data.suitGroup;
    return 0;
  }

  private void DrawEquipment()
  {
    InventoryItemData itemByInternalId = DBManager.inst.GetInventory(this.bagType).GetMyItemByInternalID((long) this.scrollInfo.equipmenID);
    this.equipComponent.FeedData((IComponentData) new EquipmentComponentData(this.scrollInfo.equipmenID, itemByInternalId != null ? itemByInternalId.enhanced : 0, this.bagType, itemByInternalId != null ? itemByInternalId.itemId : 0L));
  }

  private void DrawScroll()
  {
    this.scrollEquipment.FeedData((IComponentData) new EquipmentScrollComponetData(this.scrollInfo, this.bagType));
  }
}
