﻿// Decompiled with JetBrains decompiler
// Type: AllianceMemberOptionPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceMemberOptionPopup : Popup
{
  private const string PROMOTE_EFFECT = "Prefab/VFX/fx_Alliance_up";
  private const string DEMOTE_EFFECT = "Prefab/VFX/fx_Alliance_falling";
  public UITexture m_Portrait;
  public UITexture m_Crown;
  public UIButton m_Application;
  public UIButton m_TransferLeadership;
  public UIButton m_InviteTeleport;
  public UIButton m_Promote;
  public UIButton m_Demote;
  public UIButton m_Kick;
  public UIButton m_Reinforce;
  public UIButton m_Trade;
  public UIButton m_Refuse;
  public UIButton m_Approve;
  public UIButton m_Mail;
  public UIButton m_Detail;
  public UILabel m_PlayerName;
  public RoundTable m_Left;
  public RoundTable m_Right;
  private AllianceMemberOptionPopup.Parameter m_Parameter;
  private GameObject m_Particle;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as AllianceMemberOptionPopup.Parameter;
    this.UpdateUI();
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnApplication()
  {
    AllianceInvitedApplyData invitedApplyData = DBManager.inst.DB_AllianceInviteApply.Get(PlayerData.inst.allianceId, this.m_Parameter.targetUserData.uid);
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = ScriptLocalization.Get("alliance_application_message_title", true),
      Content = invitedApplyData.comment,
      Okay = ScriptLocalization.Get("id_uppercase_okay", true),
      OkayCallback = (System.Action) null
    });
  }

  public void OnTransferLeadership()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceTransferPopup", (Popup.PopupParameter) new AllianceTransferPopup.Parameter()
    {
      targetUid = this.m_Parameter.targetUserData.uid,
      callback = (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        ChatMessageManager.SendAllianceMessage(ChatMessageManager.AllianceMessageType.transferLeadership, this.m_Parameter.targetUserData.userName, string.Empty, string.Empty);
        this.m_Parameter.yourTitle = 3;
        this.m_Parameter.targetTitle = 6;
        this.UpdateUI();
        this.PlayParticle("Prefab/VFX/fx_Alliance_up");
      })
    });
  }

  public void OnInviteTeleport()
  {
    this.OnClosePressed();
    UIManager.inst.OpenPopup("Alliance/AllianceInviteTeleportPopup", (Popup.PopupParameter) new AllianceInviteTeleportPopup.Parameter()
    {
      userData = this.m_Parameter.targetUserData
    });
  }

  public void OnPromote()
  {
    AlliancePromotePopup.Parameter parameter = new AlliancePromotePopup.Parameter();
    parameter.uid = this.m_Parameter.targetUserData.uid;
    parameter.oldTitle = this.m_Parameter.targetTitle;
    parameter.title = this.m_Parameter.targetTitle + 1;
    parameter.name = this.m_Parameter.targetUserData.userName;
    parameter.onsuccess = (System.Action) (() =>
    {
      this.m_Parameter.targetTitle = parameter.title;
      this.UpdateUI();
      this.PlayParticle("Prefab/VFX/fx_Alliance_up");
    });
    UIManager.inst.OpenPopup("Alliance/AlliancePromotePopup", (Popup.PopupParameter) parameter);
  }

  public void OnDemote()
  {
    AllianceDemotePopup.Parameter parameter = new AllianceDemotePopup.Parameter();
    parameter.uid = this.m_Parameter.targetUserData.uid;
    parameter.oldTitle = this.m_Parameter.targetTitle;
    parameter.title = this.m_Parameter.targetTitle - 1;
    parameter.name = this.m_Parameter.targetUserData.userName;
    parameter.onsuccess = (System.Action) (() =>
    {
      this.m_Parameter.targetTitle = parameter.title;
      this.UpdateUI();
      this.PlayParticle("Prefab/VFX/fx_Alliance_falling");
    });
    UIManager.inst.OpenPopup("Alliance/AllianceDemotePopup", (Popup.PopupParameter) parameter);
  }

  private void PlayParticle(string path)
  {
    if ((bool) ((UnityEngine.Object) this.m_Particle))
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_Particle);
      this.m_Particle = (GameObject) null;
    }
    GameObject original = AssetManager.Instance.Load(path, (System.Type) null) as GameObject;
    if ((bool) ((UnityEngine.Object) original))
    {
      this.m_Particle = UnityEngine.Object.Instantiate<GameObject>(original);
      this.m_Particle.transform.parent = this.m_Crown.transform;
      this.m_Particle.transform.localPosition = Vector3.zero;
      this.m_Particle.transform.localScale = Vector3.one;
    }
    AssetManager.Instance.UnLoadAsset(path, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
  }

  public void OnKick()
  {
    if (AllianceWarPayload.Instance.IsAllianceCurrentJoined(0L))
      AllianceWarPayload.Instance.ShowAllianceQuitConfirm(2, (System.Action) null, (System.Action) (() => this.KickMember()));
    else
      this.KickMember();
  }

  private void KickMember()
  {
    this.OnClosePressed();
    bool flag = false;
    AllianceHospitalData myHospitalData = DBManager.inst.DB_AllianceHospital.GetMyHospitalData();
    if (myHospitalData != null && myHospitalData.GetHealingData(this.m_Parameter.targetUserData.uid) != null)
      flag = true;
    if (flag)
      UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
      {
        title = ScriptLocalization.Get("id_uppercase_warning", true),
        content = ScriptLocalization.Get("alliance_kick_member_hospital_troops_warning", true),
        yes = ScriptLocalization.Get("id_uppercase_yes", true),
        no = ScriptLocalization.Get("id_uppercase_no", true),
        yesCallback = (System.Action) (() => this.ShowKickPopup()),
        noCallback = (System.Action) (() => {})
      });
    else
      this.ShowKickPopup();
  }

  private void ShowKickPopup()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceKickPopup", (Popup.PopupParameter) new AllianceKickPopup.Parameter()
    {
      uid = this.m_Parameter.targetUserData.uid,
      name = this.m_Parameter.targetUserData.userName
    });
  }

  public void OnReinforce()
  {
    this.OnClosePressed();
    CityData byUid = DBManager.inst.DB_City.GetByUid(this.m_Parameter.targetUserData.uid);
    UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
    {
      marchType = MarchAllocDlg.MarchType.reinforce,
      location = byUid.cityLocation,
      targetUid = this.m_Parameter.targetUserData.uid,
      targetCityId = byUid.cityId
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnTrade()
  {
    this.OnClosePressed();
    int buildingLevelFor = CityManager.inst.GetHighestBuildingLevelFor("marketplace");
    if (buildingLevelFor > 0)
    {
      AllianceSendResources.Parameter parameter = new AllianceSendResources.Parameter();
      parameter.Uid = this.m_Parameter.targetUserData.uid;
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData("marketplace", buildingLevelFor);
      float finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(0.0f, "calc_tradetax");
      parameter.TaxPercent = Math.Round((double) finalData, 2);
      parameter.MaxResourceLoad = (long) ConfigManager.inst.DB_BenefitCalc.GetFinalData((int) data.Benefit_Value_1, "calc_trade_capacity");
      CityData byUid = DBManager.inst.DB_City.GetByUid(this.m_Parameter.targetUserData.uid);
      parameter.TargetLocation = byUid.cityLocation;
      UIManager.inst.OpenPopup("AllianceSendTradePopup", (Popup.PopupParameter) parameter);
    }
    else
      UIManager.inst.OpenDlg("NoMarketplaceDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnRefuse()
  {
    this.OnClosePressed();
    AllianceManager.Instance.AcceptOrDenyApplication(PlayerData.inst.allianceId, this.m_Parameter.targetUserData.uid, false, (System.Action<bool, object>) null);
  }

  public void OnApprove()
  {
    this.OnClosePressed();
    AllianceManager.Instance.AcceptOrDenyApplication(PlayerData.inst.allianceId, this.m_Parameter.targetUserData.uid, true, (System.Action<bool, object>) null);
  }

  public void OnMail()
  {
    this.OnClosePressed();
    new Hashtable()[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    PlayerData.inst.mail.ComposeMail(new List<string>()
    {
      this.m_Parameter.targetUserData.userName
    }, (string) null, (string) null, false);
  }

  public void OnDetail()
  {
    this.OnClosePressed();
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
    {
      uid = this.m_Parameter.targetUserData.uid
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private void UpdateUI()
  {
    UserData targetUserData = this.m_Parameter.targetUserData;
    UserData yourUserData = this.m_Parameter.yourUserData;
    int targetTitle = this.m_Parameter.targetTitle;
    int yourTitle = this.m_Parameter.yourTitle;
    AllianceInvitedApplyData invitedApplyData = DBManager.inst.DB_AllianceInviteApply.Get(PlayerData.inst.allianceId, targetUserData.uid);
    this.m_Application.isEnabled = invitedApplyData != null && !string.IsNullOrEmpty(invitedApplyData.comment);
    this.m_Application.gameObject.SetActive(AllianceUtilities.CanHandleApplication(yourTitle, targetTitle));
    this.m_Refuse.gameObject.SetActive(AllianceUtilities.CanHandleApplication(yourTitle, targetTitle));
    this.m_Approve.gameObject.SetActive(AllianceUtilities.CanHandleApplication(yourTitle, targetTitle));
    this.m_InviteTeleport.gameObject.SetActive(AllianceUtilities.CanInviteTeleport(yourUserData.uid, yourTitle, targetUserData.uid, targetTitle));
    this.m_TransferLeadership.gameObject.SetActive(AllianceUtilities.CanTransfer(yourUserData.uid, yourTitle, targetUserData.uid, targetTitle));
    this.m_Promote.gameObject.SetActive(AllianceUtilities.CanPromote(yourUserData.uid, yourTitle, targetUserData.uid, targetTitle));
    this.m_Demote.gameObject.SetActive(AllianceUtilities.CanDemote(yourUserData.uid, yourTitle, targetUserData.uid, targetTitle));
    this.m_Kick.gameObject.SetActive(AllianceUtilities.CanKick(yourUserData.uid, yourTitle, targetUserData.uid, targetTitle));
    this.m_Reinforce.gameObject.SetActive(targetTitle != 0 && yourUserData.uid != targetUserData.uid && yourTitle != 0);
    this.m_Trade.gameObject.SetActive(targetTitle != 0 && yourUserData.uid != targetUserData.uid && yourTitle != 0);
    this.m_Mail.gameObject.SetActive(yourUserData.uid != targetUserData.uid);
    this.m_Detail.gameObject.SetActive(yourUserData.uid != targetUserData.uid);
    Utils.SetPortrait(this.m_Portrait, targetUserData.PortraitPath);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Crown, AllianceUtilities.GetCrownIconPath(targetTitle), (System.Action<bool>) null, false, false, string.Empty);
    this.m_Crown.gameObject.SetActive(targetTitle != 0);
    this.m_PlayerName.text = targetUserData.userName;
    this.m_Left.Reposition();
    this.m_Right.Reposition();
  }

  public class Parameter : Popup.PopupParameter
  {
    public UserData yourUserData;
    public int yourTitle;
    public UserData targetUserData;
    public int targetTitle;
  }
}
