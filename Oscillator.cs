﻿// Decompiled with JetBrains decompiler
// Type: Oscillator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class Oscillator : MonoBehaviour
{
  private WaitForSeconds m_WaitOneSecond = new WaitForSeconds(1f);
  private WaitForSeconds m_WaitThreeSecond = new WaitForSeconds(3f);
  private static Oscillator m_Instance;

  public event System.Action<double> updateEvent;

  public event System.Action<int> secondEvent;

  public event System.Action threeSecondEvent;

  public static Oscillator Instance
  {
    get
    {
      if ((UnityEngine.Object) Oscillator.m_Instance == (UnityEngine.Object) null)
      {
        Oscillator.m_Instance = UnityEngine.Object.FindObjectOfType(typeof (Oscillator)) as Oscillator;
        if ((UnityEngine.Object) Oscillator.m_Instance == (UnityEngine.Object) null)
          Oscillator.m_Instance = new GameObject(nameof (Oscillator)).AddComponent<Oscillator>();
      }
      return Oscillator.m_Instance;
    }
  }

  public static bool IsAvailable
  {
    get
    {
      return (UnityEngine.Object) Oscillator.m_Instance != (UnityEngine.Object) null;
    }
  }

  public void Dispose()
  {
    this.updateEvent = (System.Action<double>) null;
    this.secondEvent = (System.Action<int>) null;
    this.threeSecondEvent = (System.Action) null;
    this.StopAllCoroutines();
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    Oscillator.m_Instance = (Oscillator) null;
  }

  private void Start()
  {
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) this.gameObject);
    this.StartCoroutine(this.TriggerSecondEvent());
    this.StartCoroutine(this.TriggerThreeSecondEvent());
  }

  private void Update()
  {
    if (this.updateEvent == null)
      return;
    this.updateEvent(this.ServerTime.SmoothServerTime);
  }

  public NetServerTime ServerTime
  {
    get
    {
      return NetServerTime.inst;
    }
  }

  [DebuggerHidden]
  private IEnumerator TriggerSecondEvent()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Oscillator.\u003CTriggerSecondEvent\u003Ec__Iterator6B()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator TriggerThreeSecondEvent()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Oscillator.\u003CTriggerThreeSecondEvent\u003Ec__Iterator6C()
    {
      \u003C\u003Ef__this = this
    };
  }
}
