﻿// Decompiled with JetBrains decompiler
// Type: MessageBoxWith1Button
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class MessageBoxWith1Button : Popup
{
  public System.Action onOK;
  public System.Action onClose;
  [SerializeField]
  private UILabel m_TitleLabel;
  [SerializeField]
  private UILabel m_ContentLabel;
  [SerializeField]
  private UILabel m_OkayLabel;
  [SerializeField]
  private UIButton m_OkayButton;
  [SerializeField]
  private UIButton m_CloseButton;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    MessageBoxWith1Button.Parameter parameter = orgParam as MessageBoxWith1Button.Parameter;
    this.m_TitleLabel.text = parameter.Title;
    this.m_ContentLabel.text = parameter.Content;
    this.m_OkayLabel.text = parameter.Okay;
    this.onOK = parameter.OkayCallback;
    this.onClose = parameter.OnCloseCallback;
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    if (this.onClose == null)
      return;
    this.onClose();
  }

  public void OnOkayPressed()
  {
    if (this.onOK != null)
      this.onOK();
    this.OnClosePressed();
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string Title;
    public string Content;
    public string Okay;
    public System.Action OkayCallback;
    public System.Action OnCloseCallback;
  }
}
