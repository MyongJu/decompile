﻿// Decompiled with JetBrains decompiler
// Type: RecipientEntry
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RecipientEntry : MonoBehaviour
{
  public UILabel recipient;
  public ExtendUIInput inputLabel;

  public event RecipientEntry.RecipientChangeHandler OnRecipientChange;

  public event RecipientEntry.RecipientChangeHandler OnRecipientSubmit;

  public void Show()
  {
    this.gameObject.SetActive(true);
    this.inputLabel.value = this.recipient.text;
    this.recipient.ResizeCollider();
  }

  public bool InputEnalbed
  {
    set
    {
      this.inputLabel.GetComponent<BoxCollider>().enabled = value;
    }
  }

  private void Awake()
  {
    this.inputLabel.onChange.Add(new EventDelegate(new EventDelegate.Callback(this.OnInputLabelChange)));
    this.inputLabel.onSubmit.Add(new EventDelegate(new EventDelegate.Callback(this.OnInputLabelSubmit)));
    this.inputLabel.OnExtendUIInputDeselectEvent += new ExtendUIInput.OnExtendUIInputDeselect(this.HandleOnExtendUIInputDeselectEvent);
  }

  private void HandleOnExtendUIInputDeselectEvent()
  {
    this.OnInputLabelSubmit();
  }

  private void OnInputLabelChange()
  {
    string str = this.inputLabel.value;
    if (str.Length > 0 && str.Contains(";"))
    {
      this.inputLabel.Submit();
    }
    else
    {
      if (this.OnRecipientChange == null)
        return;
      this.OnRecipientChange(this);
    }
  }

  private void OnInputLabelSubmit()
  {
    string str = this.inputLabel.value;
    if (str.Length > 0)
      this.inputLabel.value = str.Replace(";", string.Empty);
    if (this.OnRecipientSubmit == null)
      return;
    this.OnRecipientSubmit(this);
  }

  private void OnDestroy()
  {
    this.inputLabel.onChange.Clear();
    this.inputLabel.onSubmit.Clear();
    this.inputLabel.OnExtendUIInputDeselectEvent -= new ExtendUIInput.OnExtendUIInputDeselect(this.HandleOnExtendUIInputDeselectEvent);
  }

  public delegate void RecipientChangeHandler(RecipientEntry sender);
}
