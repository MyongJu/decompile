﻿// Decompiled with JetBrains decompiler
// Type: SingleCastSpellSelection
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using UI;
using UnityEngine;

public class SingleCastSpellSelection : MonoBehaviour
{
  private TempleSkillInfo m_TempleSkillInfo;
  private KingSkillInfo m_KingSkillInfo;
  private TileData m_TileData;

  public void SetData(TempleSkillInfo skillInfo, TileData tileData)
  {
    this.m_TempleSkillInfo = skillInfo;
    this.m_TileData = tileData;
  }

  public void SetData(KingSkillInfo skillInfo, TileData tileData)
  {
    this.m_KingSkillInfo = skillInfo;
    this.m_TileData = tileData;
  }

  public void OnCastSpell()
  {
    if (this.m_TempleSkillInfo != null)
      this.CastTempleSkill();
    if (this.m_KingSkillInfo == null)
      return;
    this.CastKingSkill();
  }

  private void CastTempleSkill()
  {
    if (this.m_TileData.Location.K != PlayerData.inst.userData.world_id)
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_spell_other_kingdom_forbidden", true), (System.Action) null, 4f, true);
    }
    else
    {
      string empty = string.Empty;
      if (!Utils.CanSpellToTargetTile(this.m_TempleSkillInfo, this.m_TileData, ref empty))
      {
        UIManager.inst.toast.Show(ScriptLocalization.Get(empty, true), (System.Action) null, 4f, true);
      }
      else
      {
        Hashtable hashtable = new Hashtable();
        hashtable[(object) "x"] = (object) this.m_TileData.Location.X;
        hashtable[(object) "y"] = (object) this.m_TileData.Location.Y;
        Hashtable postData = new Hashtable();
        postData[(object) "config_id"] = (object) this.m_TempleSkillInfo.internalId;
        postData[(object) "target"] = (object) hashtable;
        MessageHub.inst.GetPortByAction("AllianceTemple:castSpell").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          PVPSystem.Instance.Map.EnterNormalMode();
          if (this.m_TempleSkillInfo.NeedDonation != 0)
            return;
          AudioManager.Instance.PlaySound("sfx_alliance_altar_cast_spell", false);
        }), true);
      }
    }
  }

  private void CastKingSkill()
  {
    Hashtable hashtable = new Hashtable();
    hashtable[(object) "target_uid"] = (object) this.m_TileData.OwnerID;
    Hashtable postData = new Hashtable();
    postData[(object) "skill_id"] = (object) this.m_KingSkillInfo.internalId;
    postData[(object) "params"] = (object) hashtable;
    MessageHub.inst.GetPortByAction("king:castKingSkill").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      PVPSystem.Instance.Map.EnterNormalMode();
    }), true);
  }
}
