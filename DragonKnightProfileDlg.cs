﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightProfileDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonKnightProfileDlg : UI.Dialog
{
  public const string DIALOG_TYPE = "DragonKnight/DragonKnightProfileDlg";
  private const float DEFAULT_DESIGN_RATIO = 1.777778f;
  public GameObject summoned;
  public GameObject unSummon;
  public UILabel talentPointLabel;
  public UILabel dragonKnightNameLabel;
  public UILabel dragonKnightLevel;
  public Icon contentTemplate;
  public Icon titleTemplate;
  public UITable detailTable;
  public DragonKnightPreview preview;
  public UILabel skillBtnLabel;
  public Transform portrait;
  public UIToggle maleToggle;
  public UIToggle femaleToggle;
  [SerializeField]
  private DragonKnightProfileDlg.XpComponent xpComponent;
  private DragonKnightData dragonKnightData;
  private DragonKnightLevelInfo currentLevelInfo;
  private DragonKnightLevelInfo nextLevelInfo;
  private bool needRefresh;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.Refresh();
    this.preview.transform.parent = (Transform) null;
    this.preview.transform.localPosition = Vector3.zero;
    this.preview.transform.localScale = Vector3.one;
    if (this.dragonKnightData == null)
      this.maleToggle.Set(true);
    DBManager.inst.DB_DragonKnight.onDataChanged += new System.Action<DragonKnightData>(this.OnDragonKnightDataChanged);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.preview.gameObject);
    DBManager.inst.DB_DragonKnight.onDataChanged -= new System.Action<DragonKnightData>(this.OnDragonKnightDataChanged);
  }

  public void OnGenderChanged()
  {
    this.needRefresh = true;
  }

  private void OnDragonKnightDataChanged(DragonKnightData dragonKnightData)
  {
    this.needRefresh = true;
  }

  private void Update()
  {
    if (!this.needRefresh)
      return;
    this.needRefresh = false;
    this.Refresh();
  }

  private DragonKnightGender GetGender()
  {
    if (this.maleToggle.value)
      return DragonKnightGender.Male;
    return this.femaleToggle.value ? DragonKnightGender.Female : DragonKnightGender.Unknown;
  }

  private string GetGenderString()
  {
    return this.GetGender() == DragonKnightGender.Female ? "f" : "m";
  }

  public void OnSummonBtnClick()
  {
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("dragon_knight_summon_limit");
    if (PlayerData.inst.dragonData == null || data1 != null && PlayerData.inst.dragonData.Level < data1.ValueInt)
    {
      Utils.ShowException(2041080);
    }
    else
    {
      Hashtable postData = new Hashtable();
      postData[(object) "gender"] = (object) this.GetGenderString();
      MessageHub.inst.GetPortByAction("DragonKnight:summon").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        AudioManager.Instance.StopAndPlaySound("sfx_dragon_knight_summon_spirit");
        IVfx vfx = VfxManager.Instance.Create("Prefab/VFX/fx_dragon_rider_advance1");
        vfx.Play(this.portrait);
        DragonKnightProfileDlg.ScaleShurikenSystems(vfx.gameObject, DragonKnightProfileDlg.AdjustResolution());
        Animator componentInChildren = this.preview.GetComponentInChildren<Animator>();
        if ((bool) ((UnityEngine.Object) componentInChildren))
          componentInChildren.CrossFade("Attack", 0.01f);
        this.Refresh();
        if (NewTutorial.skipTutorial)
          return;
        string recordPoint = NewTutorial.Instance.GetRecordPoint("Tutorial_pre_dragon_rider");
        if (!("finished" != recordPoint))
          return;
        NewTutorial.Instance.InitTutorial("Tutorial_pre_dragon_rider");
        NewTutorial.Instance.LoadTutorialData(recordPoint);
      }), true);
    }
  }

  private static float AdjustResolution()
  {
    float num1 = (float) Screen.width * 1f / (float) Screen.height;
    float num2 = num1 / 1.777778f;
    if ((double) num1 >= 1.77777779102325)
      num2 = 1f;
    return num2;
  }

  private static void ScaleShurikenSystems(GameObject go, float scaleFactor)
  {
    foreach (ParticleSystemRenderer componentsInChild in go.GetComponentsInChildren<ParticleSystemRenderer>())
    {
      componentsInChild.cameraVelocityScale *= scaleFactor;
      componentsInChild.lengthScale *= scaleFactor;
      componentsInChild.velocityScale *= scaleFactor;
    }
    foreach (ParticleSystem componentsInChild in go.GetComponentsInChildren<ParticleSystem>(true))
    {
      componentsInChild.Stop();
      componentsInChild.startSpeed *= scaleFactor;
      componentsInChild.startSize *= scaleFactor;
      componentsInChild.gravityModifier *= scaleFactor;
      componentsInChild.Clear();
      componentsInChild.Play();
    }
  }

  public void OnChangeNameBtnClick()
  {
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightProfileRenamePopup", (Popup.PopupParameter) null);
  }

  public void OnTalentBtnClidk()
  {
    UIManager.inst.OpenDlg("DragonKnight/DragonKnightTalentTreeDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnSkillBtnClick()
  {
    UIManager.inst.OpenDlg("DragonKnight/DragonKnightSkillDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void Refresh()
  {
    this.InitData();
    this.UpdateUI();
  }

  private void InitData()
  {
    this.dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (this.dragonKnightData == null)
      return;
    this.currentLevelInfo = ConfigManager.inst.DB_DragonKnightLevel.GetByLevel(this.dragonKnightData.Level);
    if (this.dragonKnightData.Level >= ConfigManager.inst.DB_DragonKnightLevel.DragonKnightMaxLevel)
      return;
    this.nextLevelInfo = ConfigManager.inst.DB_DragonKnightLevel.GetByLevel(this.dragonKnightData.Level + 1);
  }

  private void UpdateUI()
  {
    this.summoned.SetActive(false);
    this.unSummon.SetActive(false);
    this.preview.SetLightEnable(this.dragonKnightData != null);
    if (this.dragonKnightData == null)
      this.UpdateUnSummonUI();
    else
      this.UpdateSummonUI();
  }

  private void UpdateSummonUI()
  {
    this.summoned.SetActive(true);
    for (int index = 0; index < this.detailTable.transform.childCount; ++index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.detailTable.transform.GetChild(index).gameObject);
    if (this.nextLevelInfo != null)
    {
      long num1 = this.dragonKnightData.XP - this.currentLevelInfo.totalExperience;
      long num2 = this.nextLevelInfo.totalExperience - this.currentLevelInfo.totalExperience;
      this.xpComponent.xpValue.text = num1.ToString() + "/" + (object) num2;
      this.xpComponent.slider.value = (float) num1 / (float) num2;
    }
    else
    {
      this.xpComponent.slider.value = 1f;
      long num = this.currentLevelInfo.totalExperience - ConfigManager.inst.DB_DragonKnightLevel.GetByLevel(this.dragonKnightData.Level - 1).totalExperience;
      this.xpComponent.xpValue.text = num.ToString() + "/" + (object) num;
    }
    this.dragonKnightLevel.text = this.dragonKnightData.Level.ToString();
    this.talentPointLabel.text = ScriptLocalization.GetWithMultyContents("dragon_knight_talent_points_num", true, this.dragonKnightData.TalentPoint.ToString());
    this.skillBtnLabel.text = ScriptLocalization.GetWithMultyContents("dragon_knight_skill_points_num", true);
    this.dragonKnightNameLabel.text = Utils.XLAT(this.dragonKnightData.Name);
    this.AddBacicPlayerprofileInfo("dragon_knight");
    this.detailTable.Reposition();
    this.preview.InitializeCharacter(this.dragonKnightData.Gender);
  }

  private void UpdateUnSummonUI()
  {
    this.unSummon.SetActive(true);
    this.preview.InitializeCharacter(this.GetGender());
  }

  private void AddBacicPlayerprofileInfo(string category)
  {
    this.AddInfoTitle(category);
    List<ConfigDragonKnightProfileInfo> profileInfoByCategory = ConfigManager.inst.DB_DragonKnightProfile.GetDragonKnightProfileInfoByCategory(category);
    profileInfoByCategory.Sort((Comparison<ConfigDragonKnightProfileInfo>) ((x, y) => x.oder.CompareTo(y.oder)));
    for (int index = 0; index < profileInfoByCategory.Count; ++index)
    {
      ConfigDragonKnightProfileInfo info = profileInfoByCategory[index];
      GameObject gameObject = NGUITools.AddChild(this.detailTable.gameObject, this.contentTemplate.gameObject);
      gameObject.SetActive(true);
      gameObject.GetComponent<Icon>().FeedData((IComponentData) new IconData((string) null, new string[2]
      {
        info.Name,
        this.DisplayContent(info)
      }));
    }
  }

  private string DisplayContent(ConfigDragonKnightProfileInfo info)
  {
    if (info.isBenefit)
    {
      if (!string.IsNullOrEmpty(info.attribute))
        return AttributeCalcHelper.Instance.CalcDragonKnightAttribute((DragonKnightAttibute.Type) Enum.Parse(typeof (DragonKnightAttibute.Type), info.attribute), (DragonKnightData) null).ToString();
      return string.Empty;
    }
    if (info.ID == "dragon_knight_attack")
      return AttributeCalcHelper.Instance.DragonKnightOutBattleAttackAttribute.ToString();
    if (info.ID == "dragon_knight_defence")
      return AttributeCalcHelper.Instance.DragonKnightOutBattleDefenceAttribute.ToString();
    if (info.ID == "dragon_knight_health")
      return AttributeCalcHelper.Instance.DragonKnightOutBattleHealthAttribute.ToString();
    if (info.ID == "dragon_knight_magic")
      return AttributeCalcHelper.Instance.DragonKnightOutBattleMagicAttackAttribute.ToString();
    return string.Empty;
  }

  private void AddInfoTitle(string title)
  {
    GameObject gameObject = NGUITools.AddChild(this.detailTable.gameObject, this.titleTemplate.gameObject);
    gameObject.SetActive(true);
    gameObject.GetComponent<Icon>().FeedData((IComponentData) new IconData((string) null, new string[1]
    {
      Utils.XLAT(string.Format("lord_details_{0}_subtitle", (object) title))
    }));
  }

  public void OnButtonViewEquipmentEffectClicked()
  {
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (dragonKnightData == null)
      return;
    List<int> allEquipments = dragonKnightData.equipments.GetAllEquipments();
    List<InventoryItemData> inventoryItemDataList = new List<InventoryItemData>();
    using (List<int>.Enumerator enumerator = allEquipments.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InventoryItemData myItemByItemId = DBManager.inst.GetInventory(BagType.DragonKnight).GetMyItemByItemID((long) enumerator.Current);
        inventoryItemDataList.Add(myItemByItemId);
      }
    }
    List<ArtifactData> artifactDataList = new List<ArtifactData>();
    UIManager.inst.OpenPopup("Equipment/EquipmentEffectPopup", (Popup.PopupParameter) new EquipmentEffectPopup.Parameter()
    {
      BagType = BagType.DragonKnight,
      AllInventoryItemData = inventoryItemDataList,
      AllArtifactData = artifactDataList
    });
  }

  [Serializable]
  protected class XpComponent
  {
    public UISlider slider;
    public UILabel xpValue;
  }
}
