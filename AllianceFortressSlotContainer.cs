﻿// Decompiled with JetBrains decompiler
// Type: AllianceFortressSlotContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class AllianceFortressSlotContainer : MonoBehaviour
{
  private List<AllianceFortressSlotItem> items = new List<AllianceFortressSlotItem>();
  private Queue<AllianceFortressSlotItem> emptyItems = new Queue<AllianceFortressSlotItem>();
  public AllianceFortressSlotItem itemPrefab;
  public UIScrollView scroll;
  public UITable tabel;
  private AllianceFortressData afd;

  public void SetData(AllianceFortressData afd)
  {
    this.afd = afd;
    this.Clear();
    if (this.IsMyFortress())
    {
      List<long> slotData = AllianceBuildUtils.GetSlotData(afd);
      int index1 = 0;
      for (int index2 = 0; index2 < slotData.Count; ++index2)
      {
        ++index1;
        AllianceFortressSlotItem fortressSlotItem = this.GetItem();
        fortressSlotItem.OnOpenCallBackDelegate = new System.Action(this.OnOpenHandler);
        fortressSlotItem.SetData(index1, slotData[index2], afd.State);
        this.items.Add(fortressSlotItem);
      }
      bool flag = false;
      if (slotData.Count > 0)
      {
        if (DBManager.inst.DB_March.Get(slotData[0]).ownerAllianceId == PlayerData.inst.allianceId)
          flag = true;
      }
      else
        flag = true;
      if (flag)
      {
        int index2 = index1 + 1;
        AllianceFortressSlotItem fortressSlotItem = this.GetItem();
        fortressSlotItem.Empty(index2, 0L);
        this.items.Add(fortressSlotItem);
        this.emptyItems.Enqueue(fortressSlotItem);
      }
    }
    else
    {
      List<long> slotData = AllianceBuildUtils.GetSlotData(afd);
      if (slotData.Count == 0)
        return;
      if (DBManager.inst.DB_March.Get(slotData[0]).ownerAllianceId == PlayerData.inst.allianceId)
      {
        int index1 = 0;
        for (int index2 = 0; index2 < slotData.Count; ++index2)
        {
          ++index1;
          AllianceFortressSlotItem fortressSlotItem = this.GetItem();
          fortressSlotItem.OnOpenCallBackDelegate = new System.Action(this.OnOpenHandler);
          fortressSlotItem.SetData(index1, slotData[index2], afd.State);
          this.items.Add(fortressSlotItem);
        }
      }
    }
    this.tabel.Reposition();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.scroll.ResetPosition()));
  }

  private bool IsMyFortress()
  {
    return PlayerData.inst.allianceId == this.afd.allianceId;
  }

  private bool IsMyOwnedFortress()
  {
    if (this.afd.OwnerAllianceID == 0L)
      return this.IsMyFortress();
    return PlayerData.inst.allianceId == this.afd.OwnerAllianceID;
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.RefreshUI);
  }

  private void OnMarchDataChanged(long rally_id)
  {
  }

  private void RefreshUI(int time)
  {
    for (int index = 0; index < this.items.Count; ++index)
      this.items[index].Refresh();
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.RefreshUI);
  }

  private void OnOpenHandler()
  {
    this.tabel.repositionNow = true;
    this.tabel.Reposition();
  }

  private AllianceFortressSlotItem GetEmptyItem()
  {
    if (this.emptyItems.Count > 0)
      return this.emptyItems.Dequeue();
    return (AllianceFortressSlotItem) null;
  }

  private AllianceFortressSlotItem GetItem()
  {
    GameObject go = Utils.DuplicateGOB(this.itemPrefab.gameObject, this.tabel.transform);
    AllianceFortressSlotItem component = go.GetComponent<AllianceFortressSlotItem>();
    NGUITools.SetActive(go, true);
    return component;
  }

  public void Clear()
  {
    for (int index = 0; index < this.items.Count; ++index)
    {
      this.items[index].Clear();
      NGUITools.SetActive(this.items[index].gameObject, false);
      this.items[index].transform.parent = this.transform;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.items[index]);
    }
    this.items.Clear();
  }
}
