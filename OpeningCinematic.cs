﻿// Decompiled with JetBrains decompiler
// Type: OpeningCinematic
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class OpeningCinematic : MonoBehaviour
{
  public System.Action<int> onFinished;

  private void OnClipFinished(AnimationEvent obj)
  {
    if (this.onFinished == null)
      return;
    this.onFinished(obj.intParameter);
  }
}
