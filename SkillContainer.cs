﻿// Decompiled with JetBrains decompiler
// Type: SkillContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UnityEngine;

public class SkillContainer : MonoBehaviour
{
  private int MaxCount = 8;
  private GameObjectPool pool = new GameObjectPool();
  private bool firstTime = true;
  private Dictionary<int, DragonSkillItem> items = new Dictionary<int, DragonSkillItem>();
  private const string IMAGE_PATH = "Texture/Dragon/";
  public UILabel title;
  public UITexture typeIcon;
  public List<DragonSkillItem> skills;
  public UITable table;
  public DragonSkillItem SkillItemPrefab;
  public UIScrollView scroll;
  public SkillContainer.SkillPowerType type;
  [NonSerialized]
  public SkillContainer.SkillType subType;
  public UILabel darkValue;
  public UILabel lightValue;
  private bool init;

  private void Start()
  {
  }

  public void Clear()
  {
    for (int index = 0; index < this.skills.Count; ++index)
    {
      DragonSkillItem skill = this.skills[index];
      skill.Clear();
      this.pool.Release(skill.gameObject);
    }
    this.skills.Clear();
    if ((UnityEngine.Object) this.typeIcon != (UnityEngine.Object) null)
      BuilderFactory.Instance.Release((UIWidget) this.typeIcon);
    this.items.Clear();
  }

  private void Init()
  {
    if (this.init)
      return;
    this.init = true;
    this.pool.Initialize(this.SkillItemPrefab.gameObject, this.gameObject);
  }

  public void Refresh()
  {
    this.Init();
    List<ConfigDragonSkillGroupInfo> skillData = this.GetSkillData();
    for (int index = 0; index < skillData.Count; ++index)
    {
      ConfigDragonSkillGroupInfo dragonSkillGroupInfo = skillData[index];
      DragonSkillItem dragonSkillItem = this.GetItem(dragonSkillGroupInfo.internalId);
      dragonSkillItem.ShowLock(false);
      this.skills.Add(dragonSkillItem);
      if (!ConfigManager.inst.DB_ConfigDragonSkillGroup.CheckDragonSkillGroupIsUnLock(dragonSkillGroupInfo.internalId))
      {
        ConfigDragonSkillMainInfo levSkillByGrounpId = ConfigManager.inst.DB_ConfigDragonSkillMain.GetCurrentMinLevSkillByGrounpId(dragonSkillGroupInfo.internalId);
        dragonSkillItem.SetData(levSkillByGrounpId.internalId);
        if (dragonSkillGroupInfo.learn_method == 1)
        {
          dragonSkillItem.ShowLock(true);
          if (!ConfigManager.inst.DB_ConfigDragonSkillGroup.IsDragonLevelSatisfied(dragonSkillGroupInfo.internalId, 1))
            dragonSkillItem.MarkMask();
        }
        else
          dragonSkillItem.MarkMask();
      }
      else
      {
        ConfigDragonSkillMainInfo dragonSkillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetCurrentMaxLevSkillByGrounpId(dragonSkillGroupInfo.internalId) ?? ConfigManager.inst.DB_ConfigDragonSkillMain.GetCurrentMinLevSkillByGrounpId(dragonSkillGroupInfo.internalId);
        dragonSkillItem.Reresh(dragonSkillMainInfo.internalId);
        if (ConfigManager.inst.DB_ConfigDragonSkillMain.CheckCanUpgrade(dragonSkillMainInfo.internalId))
          dragonSkillItem.DisplayUpgrade();
      }
    }
    if (this.firstTime)
    {
      this.table.repositionNow = true;
      this.table.Reposition();
      this.scroll.ResetPosition();
      this.firstTime = false;
    }
    if (!((UnityEngine.Object) this.typeIcon != (UnityEngine.Object) null))
      return;
    if ((UnityEngine.Object) this.darkValue != (UnityEngine.Object) null)
      this.darkValue.text = Utils.FormatThousands(PlayerData.inst.dragonData.DarkPoint.ToString());
    if ((UnityEngine.Object) this.lightValue != (UnityEngine.Object) null)
      this.lightValue.text = Utils.FormatThousands(PlayerData.inst.dragonData.LightPoint.ToString());
    if (this.type == SkillContainer.SkillPowerType.Light)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.typeIcon, "Texture/Dragon/icon_dragon_power_light", (System.Action<bool>) null, true, false, string.Empty);
      this.title.text = ScriptLocalization.Get("dragon_skill_light_title", true);
    }
    else
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.typeIcon, "Texture/Dragon/icon_dragon_power_dark", (System.Action<bool>) null, true, false, string.Empty);
      this.title.text = ScriptLocalization.Get("dragon_skill_dark_title", true);
    }
  }

  private DragonSkillItem GetItem(int groupId)
  {
    if (this.firstTime)
    {
      GameObject go = this.pool.AddChild(this.table.gameObject);
      NGUITools.SetActive(go, true);
      DragonSkillItem component1 = go.GetComponent<DragonSkillItem>();
      UIDragScrollView component2 = go.GetComponent<UIDragScrollView>();
      if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
        component2.scrollView = this.scroll;
      this.items.Add(groupId, component1);
      return component1;
    }
    if (this.items.ContainsKey(groupId))
      return this.items[groupId];
    return (DragonSkillItem) null;
  }

  private string GetSkillTypeName()
  {
    string str = string.Empty;
    switch (this.subType)
    {
      case SkillContainer.SkillType.Attack:
        str = "attack";
        break;
      case SkillContainer.SkillType.Defense:
        str = "defend";
        break;
      case SkillContainer.SkillType.Gather:
        str = "gather";
        break;
      case SkillContainer.SkillType.Monster:
        str = "attack_monster";
        break;
    }
    return str;
  }

  private int CompareDragon(DragonInfo a, DragonInfo b)
  {
    return a.level.CompareTo((object) b);
  }

  protected List<ConfigDragonSkillGroupInfo> GetSkillData()
  {
    return this.type != SkillContainer.SkillPowerType.Light ? ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDarkDragonSkillGroupInfo() : ConfigManager.inst.DB_ConfigDragonSkillGroup.GetLightDragonSkillGroupInfo();
  }

  public enum SkillType
  {
    None,
    Attack,
    Defense,
    Gather,
    Monster,
  }

  public enum SkillPowerType
  {
    Dark,
    Light,
  }
}
