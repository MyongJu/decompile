﻿// Decompiled with JetBrains decompiler
// Type: BattleManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class BattleManager
{
  private Dictionary<long, RoundPlayer> _allCombatants = new Dictionary<long, RoundPlayer>();
  private Dictionary<long, RoundPlayer> _aliveCombatants = new Dictionary<long, RoundPlayer>();
  private Dictionary<long, RoundPlayer> _morals = new Dictionary<long, RoundPlayer>();
  private Dictionary<long, RoundPlayer> _evils = new Dictionary<long, RoundPlayer>();
  private Queue<long> _attackQueue = new Queue<long>();
  private List<long> _attackFlags = new List<long>();
  private RoundReplayService _replay = new RoundReplayService();
  private int _roundGuid = 1;
  private int _roundIndex = 1;
  private RoundPlayerCreater _creater = new RoundPlayerCreater();
  private SmallRoundResultCreater _resultCreater = new SmallRoundResultCreater();
  private Hashtable _logDatas = new Hashtable();
  public const int BATTLE_RESULT_LOST = 0;
  public const int BATTLE_RESULT_WIN = 1;
  public const int BATTLE_RESULT_DRAW = 2;
  public System.Action<long> OnTargetChanged;
  private static BattleManager _instance;
  private int _skillId;
  private long _skillOwner;
  private long _targetId;
  private bool _needToValidate;
  private bool _nextRoundTargetChange;
  private bool _gameOver;
  private int _groupId;
  private bool _isFinish;
  private Coroutine _coroutine;
  public System.Action<Hashtable> OnResultHandler;
  private bool _pause;
  private int _isWin;
  private List<long> _uids;
  private List<List<int>> _playerSkill;

  public void SetFlag()
  {
    this._isFinish = false;
  }

  public static BattleManager Instance
  {
    get
    {
      if (BattleManager._instance == null)
        BattleManager._instance = new BattleManager();
      return BattleManager._instance;
    }
  }

  public bool TargetIsMultiple(RoundPlayer trigger)
  {
    if (trigger.PlayerCamp == RoundPlayer.Camp.Evil)
      return this._morals.Count > 1;
    return this._evils.Count > 1;
  }

  public void AutoAttack()
  {
    if (this._groupId <= 0 && this._uids == null)
      return;
    Oscillator.Instance.updateEvent -= new System.Action<double>(this.Process);
    this.Pause();
    if (this._coroutine != null)
    {
      Utils.StopCoroutine(this._coroutine);
      this._coroutine = (Coroutine) null;
    }
    bool flag = this.RoundNextStep(false);
    Stopwatch stopwatch = new Stopwatch();
    stopwatch.Reset();
    stopwatch.Start();
    while (flag)
      flag = this.RoundNextStep(false);
    stopwatch.Stop();
  }

  public RoundPlayer GetMyPlayer()
  {
    return this.GetPlayer(1L);
  }

  public RoundPlayer GetPlayer(long playerId)
  {
    RoundPlayer roundPlayer;
    this._allCombatants.TryGetValue(playerId, out roundPlayer);
    return roundPlayer;
  }

  public void Pause()
  {
    this._pause = true;
  }

  public void Resume()
  {
    this._pause = false;
    if (!this._replay.CurrentRound.IsFinish)
      return;
    this.NextRound(-1);
  }

  public long TargetId
  {
    get
    {
      return this._targetId;
    }
  }

  public RoundPlayer Trigger
  {
    get
    {
      if (this._replay.CurrentRound != null)
        return this._replay.CurrentRound.Trigger;
      return (RoundPlayer) null;
    }
  }

  public void SendPacket(DragonKnightPacket packet)
  {
    IRound currentRound = this._replay.CurrentRound;
    if (currentRound == null)
      return;
    switch (packet.Type)
    {
      case DragonKnightPacket.PacketType.Hit:
      case DragonKnightPacket.PacketType.SpellHit:
        currentRound.UnderAttack();
        break;
      case DragonKnightPacket.PacketType.Finish:
        if (currentRound.IsFinish)
          break;
        currentRound.MarkFinish();
        break;
      default:
        RoundPlayer roundPlayer;
        this._aliveCombatants.TryGetValue(packet.PlayerId, out roundPlayer);
        if (roundPlayer == null)
          break;
        roundPlayer.Receive(packet);
        break;
    }
  }

  public void NextRound(int guid = -1)
  {
    if (this._pause || guid > 0 && this._replay.CurrentRound != null && this._replay.CurrentRound.GUID != guid)
      return;
    this.RoundNextStep(true);
  }

  private bool RoundNextStep(bool needPlayer = true)
  {
    if (this.BattleIsOver())
      return false;
    this.AutoCheckTarget();
    IRound smallRound = this.CreateSmallRound(needPlayer);
    if (smallRound != null)
    {
      if (!this._attackFlags.Contains(smallRound.Trigger.PlayerId))
        this._attackFlags.Add(smallRound.Trigger.PlayerId);
      if (smallRound.Trigger.SkillId > 0)
      {
        DragonKnightTalentInfo knightTalentInfo = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(smallRound.Trigger.SkillId);
        smallRound.SetData("isHeal", (object) knightTalentInfo.IsHeal());
        smallRound.SetData("targetIsEnemy", (object) knightTalentInfo.HasDamge());
      }
      this._replay.AddRound(smallRound);
      if (needPlayer)
        this._replay.Start();
      else
        this._replay.AutoStep();
    }
    return true;
  }

  private bool BattleIsOver()
  {
    if (this._replay.CurrentRound != null)
      this.SmallRoundFinish(this._replay.CurrentRound);
    if (this.IsRoundFinish())
      this.CalculationResults();
    if (this.IsBattleFinish())
    {
      this.Finish();
      return true;
    }
    if (!this._gameOver)
      return false;
    this.Finish();
    return true;
  }

  public void Shutdown()
  {
    this.Stop();
    Dictionary<long, RoundPlayer>.Enumerator enumerator = this._allCombatants.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Value.Shutdown();
    this.Clear();
  }

  protected int IsWin
  {
    get
    {
      return this._isWin;
    }
    set
    {
      this._isWin = value;
    }
  }

  private void Finish()
  {
    this._isFinish = true;
    if (this.IsArenaBattle && !ActivityManager.Intance.dkArenaActivity.IsStart())
    {
      GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.CityMode;
      string str1 = ScriptLocalization.Get("dragon_knight_pvp_arena_season_finish_title", true);
      string str2 = ScriptLocalization.Get("dragon_knight_pvp_arena_season_finish_description", true);
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Content = str2,
        Title = str1
      });
    }
    else
    {
      this.Stop();
      if (this._evils.Count == 0)
        this.IsWin = 1;
      else if (this._morals.Count == 0)
        this.IsWin = 0;
      Dictionary<long, RoundPlayer>.Enumerator enumerator = this._allCombatants.GetEnumerator();
      int myHeal = 0;
      int myHurt = 0;
      int evilHeal = 0;
      int evilHurt = 0;
      int win = this.IsWin;
      int totalRound = this._roundIndex - 1;
      if (totalRound == 0)
        totalRound = 1;
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.PlayerCamp == RoundPlayer.Camp.Moral)
        {
          myHeal += enumerator.Current.Value.TotalHeal;
          myHurt += enumerator.Current.Value.TotalHurt;
        }
        else
        {
          evilHeal += enumerator.Current.Value.TotalHeal;
          evilHurt += enumerator.Current.Value.TotalHurt;
        }
        enumerator.Current.Value.Shutdown();
      }
      if (this.OnResultHandler != null)
      {
        this.OnResultHandler(new Hashtable()
        {
          {
            (object) "win",
            (object) win
          },
          {
            (object) "totalRound",
            (object) totalRound
          },
          {
            (object) "sourceHeal",
            (object) myHeal
          },
          {
            (object) "sourceDamage",
            (object) evilHurt
          },
          {
            (object) "targetHeal",
            (object) evilHeal
          },
          {
            (object) "targetDamage",
            (object) myHurt
          }
        });
      }
      else
      {
        Hashtable battleResult = this.GetBattleResult();
        if (!DragonKnightSystem.Instance.IsReady)
          return;
        DragonKnightSystem.Instance.Controller.BattleScene.Shutdown();
        DragonKnightSystem.Instance.Controller.BattleHud.ShowBagAndItems();
        DragonKnightSystem.Instance.Controller.BattleHud.HideSpeedAndQuickBattle();
        this.SendResult(win, battleResult, this._replay.GetNeedValidateData(), (System.Action<bool, object>) ((requestResult, data) =>
        {
          if (requestResult)
          {
            if (Application.isEditor)
              this.SendLogData();
            if (!DragonKnightSystem.Instance.IsReady)
              return;
            bool flag = DragonKnightSystem.Instance.RoomManager.CurrentRoom.RoomType == "pvp";
            bool isArena = DragonKnightSystem.Instance.RoomManager.CurrentRoom.RoomType == "arena";
            if (flag || isArena)
            {
              long uid = DragonKnightSystem.Instance.RoomManager.CurrentRoom.AllPlayer[0];
              Hashtable postData = new Hashtable();
              postData[(object) "opp_uid"] = (object) uid;
              MessageHub.inst.GetPortByAction("dragonKnight:getPvpCompareInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, orgData) =>
              {
                if (!ret)
                  return;
                DragonKnightPVPResultPopup.Parameter parameter = new DragonKnightPVPResultPopup.Parameter();
                parameter.source = PlayerData.inst.userData;
                parameter.sourceAlliance = PlayerData.inst.allianceData;
                parameter.sourceDK = PlayerData.inst.dragonKnightData;
                parameter.sourceEquipments = DBManager.inst.GetInventory(BagType.DragonKnight).GetEquippedItemsByID(parameter.source.uid);
                parameter.sourceHeal = myHeal;
                parameter.sourceDamage = evilHurt;
                parameter.sourceTalent1 = DBManager.inst.DB_DragonKnightTalent.GetTalentTreeTalentCount(1);
                parameter.sourceTalent2 = DBManager.inst.DB_DragonKnightTalent.GetTalentTreeTalentCount(2);
                parameter.sourceTalent3 = DBManager.inst.DB_DragonKnightTalent.GetTalentTreeTalentCount(3);
                parameter.target = DBManager.inst.DB_User.Get(uid);
                parameter.targetAlliance = DBManager.inst.DB_Alliance.Get(parameter.target.allianceId);
                parameter.targetDK = DBManager.inst.DB_DragonKnight.Get(uid);
                parameter.targetEquipments = DBManager.inst.GetInventory(BagType.DragonKnight).GetEquippedItemsByID(parameter.target.uid);
                parameter.targetHeal = evilHeal;
                parameter.targetDamage = myHurt;
                Hashtable hashtable = orgData as Hashtable;
                if (hashtable != null && hashtable.ContainsKey((object) "talent"))
                {
                  Hashtable inData = hashtable[(object) "talent"] as Hashtable;
                  if (inData != null)
                  {
                    DatabaseTools.UpdateData(inData, "1", ref parameter.targetTalent1);
                    DatabaseTools.UpdateData(inData, "2", ref parameter.targetTalent2);
                    DatabaseTools.UpdateData(inData, "3", ref parameter.targetTalent3);
                  }
                }
                parameter.win = win;
                parameter.totalRounds = totalRound;
                UserDKArenaData currentData = DBManager.inst.DB_DKArenaDB.GetCurrentData();
                UserDKArenaData dataById = DBManager.inst.DB_DKArenaDB.GetDataById(uid);
                parameter.oldSocre = currentData == null ? 0L : currentData.oldSocre;
                parameter.score = currentData == null ? 0L : currentData.score;
                parameter.oppScore = dataById == null ? 0L : dataById.score;
                if (isArena)
                {
                  parameter.isArena = true;
                  parameter.callback = (System.Action) (() => ActivityManager.Intance.ContinueDKArean());
                  DragonKnightUtils.SaveArenaHistory(parameter);
                  UIManager.inst.OpenPopup("DragonKnight/DragonKnightArenaResultPopup", (Popup.PopupParameter) parameter);
                }
                else
                  UIManager.inst.OpenPopup("DragonKnight/DragonKnightPVPResultPopup", (Popup.PopupParameter) parameter);
              }), true);
            }
            if (!this.IsArenaBattle)
              DragonKnightSystem.Instance.Controller.BattleHud.MyHUD.Reset();
            string Term = "toast_dragon_knight_fight_success";
            if (this.IsWin > 0)
            {
              if (!this.IsArenaBattle)
              {
                DragonKnightSystem.Instance.RoomManager.CurrentRoom.ClearAllMonster();
                DragonKnightSystem.Instance.RoomManager.CurrentRoomEventCompelted(false);
              }
            }
            else if (this.IsWin == 0)
            {
              Term = "toast_dragon_knight_fight_fail";
              if (!this.IsArenaBattle)
                DragonKnightSystem.Instance.RoomManager.CurrentRoomEventCompelted(!flag);
            }
            if (this.IsWin != 2)
              UIManager.inst.toast.Show(ScriptLocalization.Get(Term, true), (System.Action) null, 1f, false);
            if (isArena || (double) ((float) AttributeCalcHelper.Instance.GetCurrentAttibuteValueInDungen(DragonKnightAttibute.Type.Health, 0L) / (float) AttributeCalcHelper.Instance.DragonKnightOutBattleHealthAttribute) >= 0.200000002980232 || NewTutorial.skipTutorial)
              return;
            string recordPoint = NewTutorial.Instance.GetRecordPoint("Tutorial_magic_dragon_rider");
            if (!("finished" != recordPoint) || TutorialManager.Instance.IsRunning)
              return;
            NewTutorial.Instance.InitTutorial("Tutorial_magic_dragon_rider");
            NewTutorial.Instance.LoadTutorialData(recordPoint);
          }
          else
          {
            this.SendLogData();
            GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.CityMode;
          }
        }));
      }
    }
  }

  private Hashtable GetBattleResult()
  {
    if (this.IsWin != 1)
      return (Hashtable) null;
    Hashtable hashtable = new Hashtable();
    RoundPlayer myPlayer = this.GetMyPlayer();
    if (myPlayer != null)
    {
      hashtable.Add((object) "hp", (object) myPlayer.Health);
      hashtable.Add((object) "mp", (object) myPlayer.Mana);
    }
    hashtable.Add((object) "total_round", (object) (this._roundIndex - 1));
    return hashtable;
  }

  private RoundPlayer GetAlivePlayer()
  {
    if (this._aliveCombatants.Count > 0)
    {
      Dictionary<long, RoundPlayer>.ValueCollection.Enumerator enumerator = this._aliveCombatants.Values.GetEnumerator();
      if (enumerator.MoveNext())
        return enumerator.Current;
    }
    return (RoundPlayer) null;
  }

  private bool IsArenaBattle
  {
    get
    {
      return DragonKnightSystem.Instance.RoomManager.CurrentRoom.RoomType == "arena";
    }
  }

  private void SendResult(int isWin, Hashtable result = null, Hashtable roundSeq = null, System.Action<bool, object> callback = null)
  {
    Hashtable postData = new Hashtable();
    string action = "dragonKnight:battle";
    if (this.IsArenaBattle)
      action = "dragonKnight:arenaBattle";
    postData.Add((object) "is_win", (object) isWin);
    if (result != null && result.Count > 0)
      postData.Add((object) nameof (result), (object) result);
    if (roundSeq != null && roundSeq.Count > 0)
      postData.Add((object) "op_seq", (object) roundSeq);
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    RequestManager.inst.SendRequest(action, postData, callback, true);
  }

  public bool IsFinish
  {
    get
    {
      return this._isFinish;
    }
  }

  private void Clear()
  {
    this._logDatas = new Hashtable();
    this._isFinish = false;
    this._isWin = 0;
    this._groupId = 0;
    this._uids = (List<long>) null;
    this._playerSkill = (List<List<int>>) null;
    this._gameOver = false;
    this._roundIndex = 1;
    this._roundGuid = 1;
    this._targetId = 0L;
    this._skillId = 0;
    this._skillOwner = 0L;
    this._replay.Clear();
    this._allCombatants.Clear();
    this._aliveCombatants.Clear();
    this._evils.Clear();
    this._morals.Clear();
    this._pause = false;
    this._attackQueue.Clear();
    this._attackFlags.Clear();
    this.OnTargetChanged = (System.Action<long>) null;
  }

  public void AddMoral(RoundPlayer unit)
  {
    this._allCombatants[unit.PlayerId] = unit;
    this._aliveCombatants[unit.PlayerId] = unit;
    this._morals[unit.PlayerId] = unit;
  }

  public void AddEvil(RoundPlayer unit)
  {
    this._allCombatants[unit.PlayerId] = unit;
    this._aliveCombatants[unit.PlayerId] = unit;
    this._evils[unit.PlayerId] = unit;
  }

  public void Setup(int groupId)
  {
    this.Initialize(groupId);
    DragonKnightSystem.Instance.Controller.BattleScene.Startup(this._morals, this._evils);
  }

  public void SetupPVP(List<long> uids, List<List<int>> playerSkill, bool setupScene = true)
  {
    this.InitializePVP(uids, playerSkill);
    if (!setupScene)
      return;
    DragonKnightSystem.Instance.Controller.BattleScene.Startup(this._morals, this._evils);
  }

  public void Start()
  {
    Oscillator.Instance.updateEvent -= new System.Action<double>(this.Process);
    Oscillator.Instance.updateEvent += new System.Action<double>(this.Process);
    Dictionary<long, RoundPlayer>.Enumerator enumerator = this._allCombatants.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Value.StateMacine.SetState("guard_idle", (Hashtable) null);
    this.AutoCheckTarget();
    this._coroutine = Utils.ExecuteInSecs(2f, (System.Action) (() =>
    {
      this._coroutine = (Coroutine) null;
      this.NextRound(-1);
    }));
  }

  private void Process(double time)
  {
    if (this._allCombatants.Count <= 0)
      return;
    Dictionary<long, RoundPlayer>.Enumerator enumerator = this._allCombatants.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Value.StateMacine.Process();
    this._replay.Process();
  }

  public void Stop()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.Process);
    if (this._coroutine == null)
      return;
    Utils.StopCoroutine(this._coroutine);
    this._coroutine = (Coroutine) null;
  }

  public void UserSkill(int skillId, long playerId = 1)
  {
    this._skillId = skillId;
    this._skillOwner = playerId;
  }

  public void ChangeTarget(long playerId)
  {
    if (this._targetId != playerId)
    {
      this._targetId = playerId;
      if (this.OnTargetChanged != null)
        this.OnTargetChanged(this._targetId);
    }
    if (this.IsMoralAttackFinish())
      this._nextRoundTargetChange = true;
    else
      this._needToValidate = false;
    this._needToValidate = true;
  }

  private IRound CreateSmallRound(bool needPlayer = true)
  {
    SmallRound smallRound = new SmallRound();
    this._resultCreater.currentRound = (IRound) smallRound;
    RoundPlayer smallRoundTrigger = this.GetNextSmallRoundTrigger(needPlayer);
    if (smallRoundTrigger == null)
      return (IRound) null;
    smallRound.SkillId = (long) smallRoundTrigger.SkillId;
    smallRound.Trigger = smallRoundTrigger;
    smallRound.Targets = this.GetNextSmallRoundTargets(smallRoundTrigger);
    smallRound.GUID = this._roundGuid++;
    smallRound.RoundID = this._roundIndex;
    smallRound.Results = this._resultCreater.CreateSmallRoundResults(smallRoundTrigger, smallRound.Targets);
    if (!needPlayer)
    {
      smallRoundTrigger.ResetRoundPlayerState();
      for (int index = 0; index < smallRound.Results.Count; ++index)
      {
        RoundResult result = smallRound.Results[index];
        if (result.Buffs != null && result.Buffs.Count > 0)
          result.Target.AddBuffs(result.Buffs);
      }
    }
    if (smallRoundTrigger.PlayerCamp == RoundPlayer.Camp.Moral)
      smallRound.IsNeedToValidate = this.NeedToValidate || smallRound.SkillId > 0L;
    smallRound.MainTargetId = this._targetId;
    if (smallRound.SkillId > 0L && smallRound.Trigger.PlayerId == this._skillOwner)
    {
      this._skillOwner = 0L;
      this._skillId = 0;
    }
    return (IRound) smallRound;
  }

  private RoundPlayer GetNextSmallRoundTrigger(bool needPlayer = true)
  {
    long key = this._attackQueue.Dequeue();
    while (!this._aliveCombatants.ContainsKey(key))
      key = this._attackQueue.Dequeue();
    this._attackQueue.Enqueue(key);
    int skillId = 0;
    if (this._skillOwner == key)
      skillId = this._skillId;
    RoundPlayer aliveCombatant = this._aliveCombatants[key];
    while (!aliveCombatant.CanAttack(skillId))
    {
      long index = this._attackQueue.Dequeue();
      this._attackQueue.Enqueue(index);
      aliveCombatant = this._aliveCombatants[index];
      skillId = 0;
      if (this._skillOwner == index)
        skillId = this._skillId;
    }
    if (!aliveCombatant.CanAttack(skillId))
      return (RoundPlayer) null;
    aliveCombatant.SkillId = skillId;
    if (!needPlayer || aliveCombatant.PlayerCamp == RoundPlayer.Camp.Evil)
      aliveCombatant.AutoAction();
    return aliveCombatant;
  }

  private List<RoundPlayer> GetNextSmallRoundTargets(RoundPlayer trigger)
  {
    List<RoundPlayer> roundPlayerList = new List<RoundPlayer>();
    bool flag1 = this.IsMyself(trigger);
    bool flag2 = this.IsSingleAttack(trigger);
    bool isEvilAttack = false;
    Dictionary<long, RoundPlayer> evils = this._evils;
    if (this._evils.ContainsKey(trigger.PlayerId))
      isEvilAttack = true;
    if (flag1)
    {
      if (flag2)
      {
        roundPlayerList.Add(trigger);
        return roundPlayerList;
      }
      Dictionary<long, RoundPlayer> dictionary = !isEvilAttack ? this._morals : this._evils;
      Dictionary<long, RoundPlayer>.KeyCollection.Enumerator enumerator = dictionary.Keys.GetEnumerator();
      if (enumerator.MoveNext())
        roundPlayerList.Add(dictionary[enumerator.Current]);
    }
    else if (flag2)
    {
      if (isEvilAttack)
      {
        Dictionary<long, RoundPlayer>.KeyCollection.Enumerator enumerator = this._morals.Keys.GetEnumerator();
        if (enumerator.MoveNext())
          roundPlayerList.Add(this._morals[enumerator.Current]);
      }
      else
        roundPlayerList.Add(this._evils[this._targetId]);
    }
    else
      roundPlayerList = this.FindTargets(!isEvilAttack ? this._evils : this._morals, isEvilAttack);
    return roundPlayerList;
  }

  private bool NeedToValidate
  {
    get
    {
      return this._needToValidate;
    }
  }

  private List<RoundPlayer> FindTargets(Dictionary<long, RoundPlayer> tmp, bool isEvilAttack = false)
  {
    List<RoundPlayer> roundPlayerList = new List<RoundPlayer>();
    RoundPlayer roundPlayer1 = (RoundPlayer) null;
    Dictionary<long, RoundPlayer>.KeyCollection.Enumerator enumerator = tmp.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (this._targetId == enumerator.Current)
        roundPlayer1 = tmp[enumerator.Current];
      roundPlayerList.Add(tmp[enumerator.Current]);
    }
    if (!isEvilAttack)
    {
      if (roundPlayer1 == null)
      {
        if (roundPlayerList.Count > 0)
          this.ChangeTarget(roundPlayerList[0].PlayerId);
      }
      else
      {
        int index = roundPlayerList.IndexOf(roundPlayer1);
        if (index > 0)
        {
          RoundPlayer roundPlayer2 = roundPlayerList[0];
          roundPlayerList[0] = roundPlayer1;
          roundPlayerList[index] = roundPlayer2;
        }
      }
    }
    return roundPlayerList;
  }

  private bool IsMyself(RoundPlayer trigger)
  {
    if (trigger.SkillId > 0)
      return ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(trigger.SkillId).IsMyself();
    return false;
  }

  private bool IsSingleAttack(RoundPlayer trigger)
  {
    if (trigger.SkillId > 0)
      return ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(trigger.SkillId).IsSingleAttack();
    return true;
  }

  private void SmallRoundFinish(IRound smallRound)
  {
    RoundPlayer trigger = smallRound.Trigger;
    List<RoundPlayer> targets = smallRound.Targets;
    List<RoundResult> results = smallRound.Results;
    this.Check(trigger);
    for (int index = 0; index < targets.Count; ++index)
      this.Check(targets[index]);
  }

  private void AutoCheckTarget()
  {
    List<RoundPlayer> roundPlayerList = new List<RoundPlayer>();
    Dictionary<long, RoundPlayer> evils = this._evils;
    if (evils.ContainsKey(this._targetId))
      return;
    long num = this._targetId + 1L;
    if (evils.ContainsKey(num))
    {
      this.ChangeTarget(num);
    }
    else
    {
      Dictionary<long, RoundPlayer>.KeyCollection.Enumerator enumerator = evils.Keys.GetEnumerator();
      while (enumerator.MoveNext())
        roundPlayerList.Add(evils[enumerator.Current]);
      roundPlayerList.Sort(new Comparison<RoundPlayer>(this.CompareByPlayerId));
      if (roundPlayerList.Count <= 0)
        return;
      this.ChangeTarget(roundPlayerList[0].PlayerId);
    }
  }

  private int CompareByPlayerId(RoundPlayer a, RoundPlayer b)
  {
    return a.PlayerId.CompareTo(b.PlayerId);
  }

  private void Check(RoundPlayer player)
  {
    if (player.Health > 0)
      return;
    this._aliveCombatants.Remove(player.PlayerId);
    if (this._evils.ContainsKey(player.PlayerId))
      this._evils.Remove(player.PlayerId);
    if (!this._morals.ContainsKey(player.PlayerId))
      return;
    this._morals.Remove(player.PlayerId);
  }

  private void Initialize(int groupId)
  {
    this.Clear();
    this._groupId = groupId;
    this.SetUpPlayers(this._creater.CreateData(groupId));
  }

  private void InitializePVP(List<long> uids, List<List<int>> playerSkill)
  {
    this.Clear();
    this._uids = uids;
    this._playerSkill = playerSkill;
    this.SetUpPlayers(this._creater.CreateData(uids, playerSkill));
  }

  private void SetUpPlayers(List<RoundPlayer> tmp)
  {
    this._attackQueue.Clear();
    for (int index = 0; index < tmp.Count; ++index)
    {
      this._attackQueue.Enqueue(tmp[index].PlayerId);
      if (tmp[index] is RoundPlayerEvil)
        this.AddEvil(tmp[index]);
      else
        this.AddMoral(tmp[index]);
    }
    this.SetPlayerLog("init_attribute", 0L);
  }

  private int Compare(RoundPlayer a, RoundPlayer b)
  {
    if (a.Speed > b.Speed)
      return -1;
    if (a.Speed == b.Speed)
      return a.PlayerId.CompareTo(b.PlayerId);
    return 1;
  }

  private bool IsRoundFinish()
  {
    if (this._attackQueue.Count == 0 || this._attackFlags.Count == 0)
      return false;
    if (this.IsBattleFinish())
      return true;
    bool flag = true;
    using (Queue<long>.Enumerator enumerator = this._attackQueue.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        long current = enumerator.Current;
        if (!this._attackFlags.Contains(current) && this._aliveCombatants.ContainsKey(current))
        {
          flag = false;
          break;
        }
      }
    }
    return flag;
  }

  private void CalculationResults()
  {
    if (!this._nextRoundTargetChange)
      this._needToValidate = false;
    this._nextRoundTargetChange = false;
    List<RoundPlayer> roundPlayerList = new List<RoundPlayer>();
    Dictionary<long, RoundPlayer>.ValueCollection.Enumerator enumerator = this._aliveCombatants.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      enumerator.Current.RoundFinish();
      roundPlayerList.Add(enumerator.Current);
    }
    roundPlayerList.Sort(new Comparison<RoundPlayer>(this.Compare));
    this._attackQueue.Clear();
    this._attackFlags.Clear();
    for (int index = 0; index < roundPlayerList.Count; ++index)
      this._attackQueue.Enqueue(roundPlayerList[index].PlayerId);
    this.RoundFinish();
  }

  private bool IsMoralAttackFinish()
  {
    bool flag = true;
    Dictionary<long, RoundPlayer>.KeyCollection.Enumerator enumerator = this._morals.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (!this._attackFlags.Contains(enumerator.Current))
      {
        flag = false;
        break;
      }
    }
    return flag;
  }

  private void RoundFinish()
  {
    ++this._roundIndex;
    string uniqueId = "dungeon_monster_max_round";
    if (this.IsArenaBattle)
      uniqueId = "dk_arena_max_round";
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData(uniqueId);
    if (data == null || this._roundIndex <= data.ValueInt)
      return;
    this._gameOver = true;
    if (this.IsArenaBattle)
      this.IsWin = 2;
    else
      this.IsWin = 0;
  }

  private bool IsBattleFinish()
  {
    return this._evils.Count == 0 || this._morals.Count == 0;
  }

  public void SetPlayerLog(string key = "", long playId = 0)
  {
    Hashtable playerInfo = this.GetPlayerInfo(playId);
    if (!string.IsNullOrEmpty(key))
    {
      this.AddLogData(key, this.GetPlayerInfo(playId));
    }
    else
    {
      Hashtable smallRoundData = this.GetSmallRoundData(this._replay.CurrentRound.RoundID, this._replay.CurrentRound.Trigger.PlayerId, 0L);
      IEnumerator enumerator = playerInfo.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (!smallRoundData.ContainsKey(enumerator.Current))
          smallRoundData.Add(enumerator.Current, playerInfo[enumerator.Current]);
      }
    }
  }

  public void AddRoundLogData(IRound round, Hashtable resultData)
  {
    if (round.Targets.Count == 1)
    {
      Hashtable smallRoundData = this.GetSmallRoundData(round.RoundID, round.Trigger.PlayerId, 0L);
      IEnumerator enumerator = resultData.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (!smallRoundData.ContainsKey(enumerator.Current))
          smallRoundData.Add(enumerator.Current, resultData[enumerator.Current]);
      }
    }
    else
    {
      for (int index = 0; index < round.Targets.Count; ++index)
      {
        Hashtable smallRoundData = this.GetSmallRoundData(round.RoundID, round.Trigger.PlayerId, round.Targets[index].PlayerId);
        IEnumerator enumerator = resultData.Keys.GetEnumerator();
        while (enumerator.MoveNext() && !smallRoundData.ContainsKey(enumerator.Current))
          smallRoundData.Add(enumerator.Current, resultData[enumerator.Current]);
      }
    }
  }

  private void AddLogData(string key, Hashtable data)
  {
    if (this._logDatas.ContainsKey((object) key))
      return;
    this._logDatas.Add((object) key, (object) data);
  }

  private void SendLogData()
  {
    RequestManager.inst.SendLoader("dragonKnight:battleLog", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "log", (object) this._logDatas), (System.Action<bool, object>) null, false, false);
  }

  private Hashtable GetSmallRoundData(int roundId, long playId, long targetId = 0)
  {
    Hashtable hashtable1;
    if (this._logDatas.ContainsKey((object) "op_seq"))
    {
      hashtable1 = this._logDatas[(object) "op_seq"] as Hashtable;
    }
    else
    {
      hashtable1 = new Hashtable();
      this._logDatas.Add((object) "op_seq", (object) hashtable1);
    }
    Hashtable hashtable2;
    if (hashtable1.ContainsKey((object) roundId))
    {
      hashtable2 = hashtable1[(object) roundId] as Hashtable;
    }
    else
    {
      hashtable2 = new Hashtable();
      hashtable1.Add((object) roundId, (object) hashtable2);
    }
    string str = playId.ToString() + string.Empty;
    if (targetId > 0L)
      str = playId.ToString() + "_" + (object) targetId;
    Hashtable hashtable3;
    if (hashtable2.ContainsKey((object) str))
    {
      hashtable3 = hashtable2[(object) str] as Hashtable;
    }
    else
    {
      hashtable3 = new Hashtable();
      hashtable2.Add((object) str, (object) hashtable3);
    }
    return hashtable3;
  }

  private Hashtable GetPlayerInfo(long playId = 0)
  {
    Dictionary<long, RoundPlayer>.Enumerator enumerator = this._allCombatants.GetEnumerator();
    Hashtable hashtable = new Hashtable();
    while (enumerator.MoveNext())
    {
      if (playId == 0L || enumerator.Current.Value.PlayerId == playId)
        hashtable.Add((object) enumerator.Current.Key, (object) new Hashtable()
        {
          {
            (object) "hp",
            (object) enumerator.Current.Value.Health
          },
          {
            (object) "mp",
            (object) enumerator.Current.Value.Mana
          },
          {
            (object) "attack",
            (object) enumerator.Current.Value.Damge
          },
          {
            (object) "defens",
            (object) enumerator.Current.Value.Defense
          },
          {
            (object) "magic",
            (object) enumerator.Current.Value.MagicDamge
          },
          {
            (object) "strong",
            (object) enumerator.Current.Value.Strong
          },
          {
            (object) "aromor",
            (object) enumerator.Current.Value.Aromor
          },
          {
            (object) "intelligent",
            (object) enumerator.Current.Value.Intelligent
          },
          {
            (object) "constitution",
            (object) enumerator.Current.Value.Constitution
          },
          {
            (object) "fireDamge",
            (object) enumerator.Current.Value.FireDamge
          },
          {
            (object) "waterDamge",
            (object) enumerator.Current.Value.WaterDamge
          },
          {
            (object) "natureDamge",
            (object) enumerator.Current.Value.NatureDamge
          },
          {
            (object) "rebund",
            (object) enumerator.Current.Value.Rebund
          },
          {
            (object) "speed",
            (object) enumerator.Current.Value.Speed
          },
          {
            (object) "skills",
            (object) enumerator.Current.Value.GetAllSkills()
          }
        });
    }
    return hashtable;
  }
}
