﻿// Decompiled with JetBrains decompiler
// Type: FrequencyCache`1
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class FrequencyCache<T>
{
  private List<FrequencyCache<T>.DataBlock> cache;
  private int cacheSize;
  private System.Action<T> destoryFunc;

  public FrequencyCache(int size, System.Action<T> func = null)
  {
    this.cache = new List<FrequencyCache<T>.DataBlock>(size);
    this.cacheSize = size;
    this.destoryFunc = func;
  }

  public bool TryGetObject(string name, out T obj)
  {
    if (name == null)
      throw new ArgumentNullException(nameof (name));
    for (int index = 0; index < this.cache.Count; ++index)
    {
      if (name == this.cache[index].name)
      {
        obj = this.cache[index].obj;
        FrequencyCache<T>.DataBlock dataBlock = this.cache[index];
        this.cache.RemoveAt(index);
        this.cache.Add(dataBlock);
        return true;
      }
    }
    obj = default (T);
    return false;
  }

  public void AddObject(string name, T obj)
  {
    if (name == null)
      throw new ArgumentNullException(nameof (name));
    if (this.cache.Find((Predicate<FrequencyCache<T>.DataBlock>) (b => b.name == name)) != null)
      return;
    if (this.cacheSize == this.cache.Count)
    {
      if (this.destoryFunc != null)
        this.destoryFunc(this.cache[0].obj);
      this.cache.RemoveAt(0);
    }
    this.cache.Add(new FrequencyCache<T>.DataBlock(name, obj));
  }

  public void Clear()
  {
    if (this.destoryFunc != null)
    {
      for (int index = 0; index < this.cache.Count; ++index)
        this.destoryFunc(this.cache[index].obj);
    }
    this.cache.Clear();
  }

  public class DataBlock
  {
    public string name;
    public T obj;

    public DataBlock(string name, T obj)
    {
      this.name = name;
      this.obj = obj;
    }
  }
}
