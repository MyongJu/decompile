﻿// Decompiled with JetBrains decompiler
// Type: PreLoginState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class PreLoginState : LoadBaseState
{
  private const int MaxRetry = 5;
  private int retryTimes;

  public PreLoginState(int step)
    : base(step)
  {
  }

  public override string Key
  {
    get
    {
      return nameof (PreLoginState);
    }
  }

  protected override SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.Prelogin;
    }
  }

  protected override void Prepare()
  {
    base.Prepare();
    this.Finish();
  }

  private void Update37AppConfig()
  {
  }

  protected override void Finish()
  {
    base.Finish();
    this.Preloader.OnPreLoginFinish();
  }

  private void ShowLoginWindow()
  {
  }

  private void OnStartGameClicked()
  {
    this.ShowLoginWindow();
  }

  private void OnLoginSuccessHandler()
  {
  }

  private void OnLoginFailureHandler()
  {
  }
}
