﻿// Decompiled with JetBrains decompiler
// Type: StorehouseInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class StorehouseInfoDlg : MonoBehaviour
{
  public StorehouseInfoDlg.ResourceGroup mResourcesProtected = new StorehouseInfoDlg.ResourceGroup();
  public StorehouseInfoDlg.ResourceGroup mResourcesUnprotected = new StorehouseInfoDlg.ResourceGroup();
  private System.Action _onFinished;

  public void SetDetails(System.Action onFinished)
  {
    this._onFinished = onFinished;
    this.mResourcesProtected.mFoodValue.text = "0";
    this.mResourcesProtected.mWoodValue.text = "0";
    this.mResourcesProtected.mOreValue.text = "0";
    this.mResourcesUnprotected.mFoodValue.text = "0";
    this.mResourcesUnprotected.mWoodValue.text = "0";
    this.mResourcesUnprotected.mOreValue.text = "0";
    PageTabsMonitor componentInChildren = this.gameObject.transform.GetComponentInChildren<PageTabsMonitor>();
    componentInChildren.onTabSelected += new System.Action<int>(this.OnTabSelected);
    componentInChildren.SetCurrentTab(0, true);
  }

  private void OnTabSelected(int selectedTab)
  {
    switch (selectedTab)
    {
    }
  }

  public void OnCloseBtnPressed()
  {
    if (this._onFinished != null)
      this._onFinished();
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  [Serializable]
  public class ResourceGroup
  {
    public UILabel mFoodValue;
    public UILabel mWoodValue;
    public UILabel mOreValue;
  }
}
