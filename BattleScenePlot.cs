﻿// Decompiled with JetBrains decompiler
// Type: BattleScenePlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BattleScenePlot : MonoBehaviour
{
  public RoundPlayer Player;
  private RoundPlayerHud _playerHud;

  public void Initialize(string model)
  {
    if (!(bool) ((Object) this._playerHud))
    {
      GameObject gameObject = Object.Instantiate<GameObject>(DragonKnightSystem.Instance.Controller.BattleScene.PlayerHudPrefab);
      gameObject.SetActive(false);
      this._playerHud = gameObject.GetComponent<RoundPlayerHud>();
      this._playerHud.transform.SetParent(this.transform, false);
    }
    if (!(bool) ((Object) this._playerHud))
      return;
    float y;
    if (!DragonKnightUIConfiguration.Instance.GetConfiguration().TryGetValue(model, out y))
      y = 5.3f;
    this._playerHud.transform.localPosition = new Vector3(0.0f, y, 0.0f);
  }

  public void SetHudActive(bool active)
  {
    if (!(bool) ((Object) this._playerHud))
      return;
    this._playerHud.gameObject.SetActive(active);
  }

  public RoundPlayerHud PlayerHud
  {
    get
    {
      return this._playerHud;
    }
  }
}
