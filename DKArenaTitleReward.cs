﻿// Decompiled with JetBrains decompiler
// Type: DKArenaTitleReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DKArenaTitleReward
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "title_name")]
  public string Name;
  [Config(Name = "title_icon")]
  public string Image;
  [Config(Name = "score_min")]
  public int ScoreMin;
  [Config(Name = "score_max")]
  public int ScoreMax;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "Rewards")]
  public Reward Rewards;

  public string ImagePath
  {
    get
    {
      return "Texture/DKArena/" + this.Image;
    }
  }
}
