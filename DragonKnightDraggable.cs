﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightDraggable
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DragonKnightDraggable : MonoBehaviour
{
  public static bool Dragging;
  public DragonKnightPreview m_Preview;

  private void OnDragStart()
  {
    DragonKnightDraggable.Dragging = true;
  }

  private void OnDragEnd()
  {
    DragonKnightDraggable.Dragging = false;
  }

  private void OnDrag(Vector2 delta)
  {
    if (!(bool) ((Object) this.m_Preview))
      return;
    this.m_Preview.Rotate(-delta.x);
  }
}
