﻿// Decompiled with JetBrains decompiler
// Type: MsgChecker
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public class MsgChecker
{
  public long uid;
  public string content;
  public int type;
  public string code;
  public string raw;

  public WWWForm ToWWWForm()
  {
    WWWForm wwwForm = new WWWForm();
    wwwForm.AddField("uid", this.uid.ToString());
    wwwForm.AddField("content", this.content);
    wwwForm.AddField("type", this.type);
    wwwForm.AddField("code", this.code);
    return wwwForm;
  }

  public byte[] ToJsonBytes()
  {
    return Encoding.UTF8.GetBytes(JsonWriter.Serialize((object) this));
  }

  public static string GetSign(long uid, string content)
  {
    byte[] hash = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(string.Format("{0}{1}", (object) uid, (object) content)));
    StringBuilder stringBuilder = new StringBuilder();
    for (int index = 0; index < hash.Length; ++index)
      stringBuilder.Append(hash[index].ToString("x2"));
    return stringBuilder.ToString().ToLower();
  }
}
