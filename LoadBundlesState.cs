﻿// Decompiled with JetBrains decompiler
// Type: LoadBundlesState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class LoadBundlesState : LoadBaseState
{
  public static List<string> preloadBundle = new List<string>()
  {
    "static+basic_common",
    "static+scene_ui",
    "static+basic_gui2",
    "static+ui_pre",
    "static+ui_1",
    "static+troops",
    "static+kingdom_tiles",
    "static+kingdom_buildings",
    "static+sfx",
    "static+vfx",
    "static+ui_tutorial",
    "static+localize",
    "static+missiles",
    "static+citybg",
    "static+citybase",
    "static+citybuildings",
    "static+walls"
  };
  private List<string> failBundleNames = new List<string>();
  private int taskId = -1;
  private Stack<string> bundleNames;
  private int COMPLETE_COUNT;
  private int current;
  private float total;
  private Dictionary<string, LoaderBatch> bundleBatch;
  private Dictionary<string, bool> currentBundles;
  private Dictionary<string, float> bundleProgress;
  private bool pause;

  public LoadBundlesState(int step)
    : base(step)
  {
    this.bundleProgress = new Dictionary<string, float>();
    this.bundleBatch = new Dictionary<string, LoaderBatch>();
    this.currentBundles = new Dictionary<string, bool>();
  }

  public override string Key
  {
    get
    {
      return nameof (LoadBundlesState);
    }
  }

  protected override SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.LoadBundles;
    }
  }

  protected override void Prepare()
  {
    base.Prepare();
    if (!AssetManager.IsLoadAssetFromBundle)
    {
      this.Finish();
    }
    else
    {
      this.AddCacheEventHalder();
      this.bundleNames = new Stack<string>();
      this.total = 0.0f;
      using (List<string>.Enumerator enumerator = LoadBundlesState.preloadBundle.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          if (!this.bundleNames.Contains(current))
          {
            ++this.total;
            this.bundleNames.Push(current);
          }
          if (!this.currentBundles.ContainsKey(current))
            this.currentBundles.Add(current, true);
        }
      }
    }
  }

  private void AddCacheEventHalder()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.UpdateEventHandler);
    BundleManager.Instance.onBundleLoaded += new System.Action<string, bool>(this.OnBundleLoadFinished);
    BundleManager.Instance.onBundleBeginToDownload += new System.Action<string, LoaderBatch>(this.OnBunlderStartDownload);
  }

  private void RemoveCacheEventHalder()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.UpdateEventHandler);
    BundleManager.Instance.onBundleLoaded -= new System.Action<string, bool>(this.OnBundleLoadFinished);
    BundleManager.Instance.onBundleBeginToDownload -= new System.Action<string, LoaderBatch>(this.OnBunlderStartDownload);
  }

  protected override void OnDispose()
  {
    this.RemoveCacheEventHalder();
  }

  private void OnBunlderStartDownload(string bunldName, LoaderBatch batch)
  {
    if (!this.currentBundles.ContainsKey(bunldName))
      return;
    this.bundleBatch[bunldName] = batch;
  }

  private void UpdateEventHandler(int t)
  {
    GameEngine.Instance.iTweenValue = Mathf.Lerp(GameEngine.Instance.iTweenValue, SplashDataConfig.GetValue(this.CurrentPhase), 0.02f);
    this.RefreshProgress(GameEngine.Instance.iTweenValue);
    if (this.bundleNames == null || this.bundleNames.Count == 0)
    {
      this.CheckFailList();
    }
    else
    {
      while (this.current < SplashDataConfig.CACHE_BUNDLE_COUT_FRAME && this.bundleNames.Count > 0)
      {
        ++this.current;
        BundleManager.Instance.CacheBundle(this.bundleNames.Pop());
      }
    }
  }

  private void OnBundleLoadFinished(string name, bool success)
  {
    --this.current;
    if (success)
    {
      ++this.COMPLETE_COUNT;
      this.bundleProgress[name] = 1f;
      if (this.bundleBatch.ContainsKey(name))
        this.bundleBatch.Remove(name);
      this.RefreshCurrentProgress();
      if ((double) this.COMPLETE_COUNT != (double) this.total)
        return;
      this.Finish();
    }
    else
      this.failBundleNames.Add(name);
  }

  private void CheckFailList()
  {
    if (this.pause || this.failBundleNames.Count <= 0)
      return;
    if (this.taskId < 0)
      this.taskId = TaskManager.Instance.Execute((TaskManager.ITask) new AutoRetryTask()
      {
        ExecuteHandler = new System.Action(this.RetryCache)
      });
    else
      TaskManager.Instance.SetAutoRetryTask(this.taskId, false);
    if (!TaskManager.Instance.IsFinish(this.taskId))
      return;
    this.taskId = -1;
    this.pause = true;
    NetWorkDetector.Instance.RetryTip(new System.Action(this.OnRetry), ScriptLocalization.Get("data_error_title", true), ScriptLocalization.Get("data_error_loading2_description", true));
  }

  private void OnRetry()
  {
    this.pause = false;
    this.RetryCache();
  }

  private void RetryCache()
  {
    for (int index = 0; index < this.failBundleNames.Count; ++index)
    {
      string t = this.failBundleNames[index];
      int length = t.IndexOf('.');
      if (length > -1)
        t = t.Substring(0, length);
      if (!string.IsNullOrEmpty(t))
        this.bundleNames.Push(t);
    }
    this.failBundleNames.Clear();
  }

  private void RefreshCurrentProgress()
  {
    float bunldeProgress = this.GetBunldeProgress();
    float num1 = SplashDataConfig.GetValue(SplashDataConfig.Phase.BunldeInfoVersion);
    float num2 = SplashDataConfig.GetValue(this.CurrentPhase);
    float num3 = num1 + (num2 - num1) * bunldeProgress;
    float num4 = SplashDataConfig.GetValue(this.CurrentPhase);
    if ((double) num3 > (double) num4)
      num3 = num4;
    this.RefreshProgress(num3);
  }

  protected override void RefreshProgress(float value)
  {
    string str = string.Empty;
    if ((double) this.total > 0.0)
      str = string.Format(" {0}/{1}", (object) this.COMPLETE_COUNT, (object) this.total);
    this.Preloader.SplashScreen(SplashDataConfig.GetTipFromPhaseKey(this.CurrentPhase) + str, SplashDataConfig.GetValue(this.CurrentPhase) * value);
  }

  private float GetBunldeProgress()
  {
    float num = 0.0f;
    Dictionary<string, LoaderBatch>.Enumerator enumerator1 = this.bundleBatch.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      LoaderBatch loaderBatch = enumerator1.Current.Value;
      this.bundleProgress[enumerator1.Current.Key] = loaderBatch.Progress;
    }
    Dictionary<string, float>.Enumerator enumerator2 = this.bundleProgress.GetEnumerator();
    while (enumerator2.MoveNext())
      num += enumerator2.Current.Value;
    return num / this.total;
  }

  protected override void Finish()
  {
    if (this.IsDestroy)
      return;
    base.Finish();
    this.RefreshProgress();
    TaskManager.Instance.SetAutoRetryTask(this.taskId, true);
    this.taskId = -1;
    this.Preloader.OnLoadBundlesFinish();
    this.currentBundles.Clear();
    this.RemoveCacheEventHalder();
    BundleManager.Instance.ClearUnzipMarkCollection();
    if (this.bundleNames == null)
      return;
    this.bundleNames.Clear();
  }
}
