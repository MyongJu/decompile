﻿// Decompiled with JetBrains decompiler
// Type: NetErrorBlocker
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class NetErrorBlocker : SystemBlocker
{
  public NetErrorBlocker.ButtonState buttonState = NetErrorBlocker.ButtonState.OK_CENTER;
  public UILabel title;
  public UILabel message;
  public UILabel leftlabel;
  public UILabel rightlabel;
  public UILabel centerlabel;
  public UIButton yesButton;
  public UIButton noButton;
  public UIButton okButton;
  private NetErrorBlocker.Parameter m_Parameter;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as NetErrorBlocker.Parameter;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    Vector3 zero = Vector3.zero;
    zero.z = -100f;
    this.transform.localPosition = zero;
    this.title.text = this.m_Parameter.title;
    this.message.text = this.m_Parameter.message;
    this.centerlabel.text = this.m_Parameter.yesOrOklabel_left;
    this.leftlabel.text = this.m_Parameter.yesOrOklabel_left;
    this.rightlabel.text = this.m_Parameter.nolabel_right;
    NGUITools.SetActive(this.yesButton.gameObject, (this.m_Parameter.buttonState & NetErrorBlocker.ButtonState.YES_LEFT) > (NetErrorBlocker.ButtonState) 0);
    NGUITools.SetActive(this.noButton.gameObject, (this.m_Parameter.buttonState & NetErrorBlocker.ButtonState.NO_RIGHT) > (NetErrorBlocker.ButtonState) 0);
    NGUITools.SetActive(this.okButton.gameObject, (this.m_Parameter.buttonState & NetErrorBlocker.ButtonState.OK_CENTER) > (NetErrorBlocker.ButtonState) 0);
  }

  private void Close()
  {
    UIManager.inst.HideNetErrorBox();
  }

  public void OnCloseHandler()
  {
    this.Close();
    if (this.m_Parameter.closeHandler != null)
      this.m_Parameter.closeHandler();
    this.m_Parameter = (NetErrorBlocker.Parameter) null;
  }

  public void OnYesHandler()
  {
    this.Close();
    if (this.m_Parameter.noHandler_left != null)
      this.m_Parameter.noHandler_left();
    this.m_Parameter = (NetErrorBlocker.Parameter) null;
  }

  public void OnNoHandler()
  {
    this.Close();
    if (this.m_Parameter.yesOrOkHandler_right != null)
      this.m_Parameter.yesOrOkHandler_right();
    this.m_Parameter = (NetErrorBlocker.Parameter) null;
  }

  public void OnCenterHandler()
  {
    this.Close();
    if (this.m_Parameter.yesOrOkHandler_right != null)
      this.m_Parameter.yesOrOkHandler_right();
    this.m_Parameter = (NetErrorBlocker.Parameter) null;
  }

  public class Parameter : SystemBlocker.SystemBlockerParameter
  {
    public string yesOrOklabel_left = "YES";
    public string nolabel_right = "NO";
    public NetErrorBlocker.ButtonState buttonState = NetErrorBlocker.ButtonState.OK_CENTER;
    public string title;
    public string message;
    public System.Action yesOrOkHandler_right;
    public System.Action noHandler_left;
    public System.Action closeHandler;
  }

  public enum ButtonState
  {
    YES_LEFT = 2,
    NO_RIGHT = 4,
    OK_CENTER = 8,
  }
}
