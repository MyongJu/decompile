﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightLevelUpPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class DragonKnightLevelUpPopup : Popup
{
  public UILabel m_Level;
  public UITexture m_Banner;
  public DragonKnightPreview m_Preview;
  public UITexture m_Skill;
  public UITexture m_Talent;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Level.text = Utils.XLAT("barracks_02_uppercase_level") + " " + (orgParam as DragonKnightLevelUpPopup.Parameter).dragonKnightData.Level.ToString();
    this.m_Preview.transform.parent = (Transform) null;
    this.m_Preview.transform.localPosition = Vector3.zero;
    this.m_Preview.transform.localScale = Vector3.one;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Banner, "Texture/lord_level_up_banner", (System.Action<bool>) null, false, true, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Skill, "Texture/DragonKnight/DungeonTexture/dragon_knight_level_up_skills", (System.Action<bool>) null, true, true, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Talent, "Texture/DragonKnight/DungeonTexture/dragon_knight_level_up_talents", (System.Action<bool>) null, true, true, string.Empty);
    this.m_Preview.InitializeCharacter(PlayerData.inst.dragonKnightData.Gender);
    AudioManager.Instance.StopAndPlaySound("sfx_ui_dragon_levelup");
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_Preview.gameObject);
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public DragonKnightData dragonKnightData;
  }
}
