﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossRewardsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceBossRewardsPopup : Popup
{
  private Dictionary<string, AllianceBossRewardItemRenderer> itemDict = new Dictionary<string, AllianceBossRewardItemRenderer>();
  private GameObjectPool itemPool = new GameObjectPool();
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.itemPool.Initialize(this.itemPrefab, this.itemRoot);
    if (!AllianceBuildUtils.isAllianceBossSummoned())
      return;
    this.ShowRankRewardContent();
  }

  private void ShowRankRewardContent()
  {
    this.ClearData();
    List<AllianceBossRankRewardInfo> rankRewardInfoList = ConfigManager.inst.DB_AllianceBossRankReward.GetAllianceBossRankRewardInfoList();
    for (int index = 0; index < rankRewardInfoList.Count; ++index)
    {
      AllianceBossRewardItemRenderer itemRenderer = this.CreateItemRenderer(rankRewardInfoList[index]);
      this.itemDict.Add(rankRewardInfoList[index].id, itemRenderer);
    }
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<string, AllianceBossRewardItemRenderer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this.itemPool.Release(gameObject);
      }
    }
    this.itemDict.Clear();
    this.itemPool.Clear();
  }

  private AllianceBossRewardItemRenderer CreateItemRenderer(AllianceBossRankRewardInfo info)
  {
    AllianceBossInfo info1 = ConfigManager.inst.DB_AllianceBoss.Get((int) DBManager.inst.DB_AllianceBoss.GetAllianceBossData().BossId);
    GameObject gameObject = this.itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    AllianceBossRewardItemRenderer component = gameObject.GetComponent<AllianceBossRewardItemRenderer>();
    component.SetData(info1, info);
    return component;
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }
}
