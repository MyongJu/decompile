﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightSkillSettingPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonKnightSkillSettingPopup : Popup
{
  private Dictionary<int, DragonKnightSkillSettingItemRenderer> _itemDict = new Dictionary<int, DragonKnightSkillSettingItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private const int SKILL_SLOTS_COUNT = 3;
  public GameObject noActiveSkillHintNode;
  public DragonKnightSkillSettingPopup.SkillSlotInfo[] skillTopSlots;
  public UIScrollView scrollView;
  public UIGrid grid;
  public GameObject itemPrefab;
  public GameObject itemRoot;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.AddEventHandler();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.ClearData();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.ShowSkillSlotContent();
    this.ShowSkillSettingContent();
  }

  private void ShowSkillSlotContent()
  {
    int activeSkillSlotCount = DragonKnightUtils.GetActiveSkillSlotCount();
    DragonKnightDungeonData knightDungeonData = DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L);
    for (int index = 0; index < 3; ++index)
    {
      if (index < this.skillTopSlots.Length)
      {
        if (index < activeSkillSlotCount)
          GreyUtility.Normal(this.skillTopSlots[index].slotInfoNode);
        else
          GreyUtility.Grey(this.skillTopSlots[index].slotInfoNode);
        NGUITools.SetActive(this.skillTopSlots[index].lockedSprite.gameObject, index >= activeSkillSlotCount);
      }
      if (knightDungeonData != null && knightDungeonData.AssignedSkill != null)
      {
        int internalId = 0;
        knightDungeonData.AssignedSkill.talentSkills.TryGetValue(index + 1, out internalId);
        DragonKnightTalentInfo knightTalentInfo = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(internalId);
        if (knightTalentInfo != null)
        {
          if ((UnityEngine.Object) this.skillTopSlots[index].skillTexture.mainTexture == (UnityEngine.Object) null || this.skillTopSlots[index].skillTexture.mainTexture.name != knightTalentInfo.image)
            BuilderFactory.Instance.HandyBuild((UIWidget) this.skillTopSlots[index].skillTexture, knightTalentInfo.imagePath, (System.Action<bool>) null, true, false, string.Empty);
        }
        else
          this.skillTopSlots[index].skillTexture.mainTexture = (Texture) null;
      }
      else
        this.skillTopSlots[index].skillTexture.mainTexture = (Texture) null;
    }
  }

  private void ShowSkillSettingContent()
  {
    this.ClearData();
    List<DragonKnightTalentInfo> activeSkillsByType = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightActiveSkillsByType(1);
    if (activeSkillsByType != null)
    {
      for (int key = 0; key < activeSkillsByType.Count; ++key)
      {
        DragonKnightSkillSettingItemRenderer itemRenderer = this.CreateItemRenderer(activeSkillsByType[key]);
        this._itemDict.Add(key, itemRenderer);
      }
    }
    NGUITools.SetActive(this.noActiveSkillHintNode, activeSkillsByType == null || activeSkillsByType.Count <= 0);
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, DragonKnightSkillSettingItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, DragonKnightSkillSettingItemRenderer> current = enumerator.Current;
        current.Value.ClearData();
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.grid.repositionNow = true;
    this.grid.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private DragonKnightSkillSettingItemRenderer CreateItemRenderer(DragonKnightTalentInfo talentInfo)
  {
    GameObject gameObject = this._itemPool.AddChild(this.grid.gameObject);
    gameObject.SetActive(true);
    DragonKnightSkillSettingItemRenderer component = gameObject.GetComponent<DragonKnightSkillSettingItemRenderer>();
    component.SetData(talentInfo);
    return component;
  }

  private void OnDragonKnightDataUpdated(DragonKnightDungeonData dungeonData)
  {
    this.ShowSkillSlotContent();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_DragonKnightDungeon.onDataUpdate += new System.Action<DragonKnightDungeonData>(this.OnDragonKnightDataUpdated);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_DragonKnightDungeon.onDataUpdate -= new System.Action<DragonKnightDungeonData>(this.OnDragonKnightDataUpdated);
  }

  [Serializable]
  public class SkillSlotInfo
  {
    public GameObject slotInfoNode;
    public UISprite lockedSprite;
    public UITexture skillTexture;
  }
}
