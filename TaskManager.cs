﻿// Decompiled with JetBrains decompiler
// Type: TaskManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class TaskManager
{
  private int _taskId = 1;
  private static TaskManager _instance;
  private AsynTaskModule _asynTaskModule;
  private AutoRetryTaskModule _retryModule;

  public static TaskManager Instance
  {
    get
    {
      if (TaskManager._instance == null)
        TaskManager._instance = new TaskManager();
      return TaskManager._instance;
    }
  }

  public int Execute(TaskManager.ITask task)
  {
    task.TaskId = this._taskId++;
    if (task is AsynTask)
      this._asynTaskModule.Execute(task as AsynTask);
    else
      this._retryModule.Execute(task as AutoRetryTask);
    return task.TaskId;
  }

  public void Remove(int taskId)
  {
    this._asynTaskModule.Remove(taskId);
    this._retryModule.Remove(taskId);
  }

  public bool IsFinish(int taskId)
  {
    bool flag = this._asynTaskModule.IsFinish(taskId);
    if (!flag)
      flag = this._retryModule.IsFinish(taskId);
    return flag;
  }

  public void SetAutoRetryTask(int taskId, bool success)
  {
    this._retryModule.SetSuccess(taskId, success);
  }

  public void Initialize()
  {
    this._asynTaskModule = new AsynTaskModule();
    this._retryModule = new AutoRetryTaskModule();
    this._asynTaskModule.Initialize();
    this._retryModule.Initialize();
    Oscillator.Instance.updateEvent += new System.Action<double>(this.Process);
  }

  private void Process(double time)
  {
    this._asynTaskModule.Process();
    this._retryModule.Process();
  }

  public void Dispose()
  {
    this._taskId = 1;
    this._asynTaskModule.Dispose();
    this._retryModule.Dispose();
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.updateEvent -= new System.Action<double>(this.Process);
  }

  public interface ITask
  {
    int TaskId { get; set; }

    bool IsRunning { get; set; }

    bool IsFinish { get; set; }

    void Execute();
  }
}
