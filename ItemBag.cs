﻿// Decompiled with JetBrains decompiler
// Type: ItemBag
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;

public class ItemBag
{
  private List<int> newItems = new List<int>();
  private static ItemBag m_Instance;
  private List<ShopStaticInfo> _CurrentSaleShopItem;
  private List<ShopStaticInfo> _WillStaleShopItems;
  private List<ShopStaticInfo> _WillTakeOffShelves;

  private ItemBag()
  {
  }

  public event System.Action OnCurrentSaleShopItemUpdate;

  public event System.Action OnNewItemChange;

  public static ItemBag Instance
  {
    get
    {
      if (ItemBag.m_Instance == null)
        ItemBag.m_Instance = new ItemBag();
      return ItemBag.m_Instance;
    }
  }

  public bool HasSpeedupItem()
  {
    return this.GetSpeedUpItems().Count > 0;
  }

  public void UseFreeSpeedup(long job_id, System.Action<bool, object> callback)
  {
    Hashtable postData = new Hashtable();
    postData[(object) nameof (job_id)] = (object) job_id;
    MessageHub.inst.GetPortByAction("City:freeSpeedup").SendRequest(postData, callback, true);
  }

  public void SendGamble(long itemId, System.Action<bool, object> callback)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "item_id"] = (object) itemId;
    postData[(object) "currency"] = (object) "essence";
    MessageHub.inst.GetPortByAction("Item:buyConsumableItem").SendRequest(postData, callback, true);
  }

  public static Hashtable CalculateInstantCost(Hashtable ht)
  {
    int cost = 0;
    Hashtable hashtable1 = new Hashtable();
    List<ShopStaticInfo> shopStaticInfoList = new List<ShopStaticInfo>();
    ArrayList addToList = new ArrayList();
    if (ht.ContainsKey((object) "time"))
    {
      int left = int.Parse(ht[(object) "time"].ToString());
      List<ShopStaticInfo> currentSaleShopItems = ItemBag.Instance.GetCurrentSaleShopItems("shop", ItemBag.ItemType.speedup);
      ItemBag.AddToList(currentSaleShopItems, ref addToList, left, ref cost);
      currentSaleShopItems.Clear();
    }
    if (ht.ContainsKey((object) "resources"))
    {
      Hashtable hashtable2 = ht[(object) "resources"] as Hashtable;
      ItemBag.ItemType itemType = ItemBag.ItemType.Invalid;
      CityData byUid = DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid);
      IDictionaryEnumerator enumerator = hashtable2.GetEnumerator();
      while (enumerator.MoveNext())
      {
        CityResourceInfo.ResourceType type = CityResourceInfo.ResourceType.FOOD;
        ItemBag.ItemType key = (ItemBag.ItemType) enumerator.Key;
        switch (key)
        {
          case ItemBag.ItemType.food:
            type = CityResourceInfo.ResourceType.FOOD;
            break;
          case ItemBag.ItemType.wood:
            type = CityResourceInfo.ResourceType.WOOD;
            break;
          case ItemBag.ItemType.ore:
            type = CityResourceInfo.ResourceType.ORE;
            break;
          case ItemBag.ItemType.silver:
            type = CityResourceInfo.ResourceType.SILVER;
            break;
        }
        List<ShopStaticInfo> currentSaleShopItems = ItemBag.Instance.GetCurrentSaleShopItems("shop", key);
        int left = int.Parse(enumerator.Value.ToString());
        if ((int) byUid.resources.GetCurrentResource(type) >= left)
        {
          itemType = ItemBag.ItemType.Invalid;
        }
        else
        {
          ItemBag.AddToList(currentSaleShopItems, ref addToList, left, ref cost);
          itemType = ItemBag.ItemType.Invalid;
        }
      }
    }
    if (ht.ContainsKey((object) "items"))
    {
      Hashtable hashtable2 = ht[(object) "items"] as Hashtable;
      foreach (object key in (IEnumerable) hashtable2.Keys)
      {
        string s = key.ToString();
        int num1 = int.Parse(hashtable2[(object) s].ToString());
        int result = -1;
        int.TryParse(s, out result);
        if (result > 0)
        {
          ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(result);
          if (dataByInternalId != null && num1 > 0)
          {
            int num2 = num1 - ItemBag.Instance.GetItemCount(dataByInternalId.Item_InternalId);
            for (int index = 0; index < num2; ++index)
            {
              addToList.Add((object) dataByInternalId);
              cost += ItemBag.Instance.GetShopItemPrice(dataByInternalId.ID);
            }
          }
        }
      }
    }
    hashtable1.Add((object) "itemList", (object) addToList);
    hashtable1.Add((object) "cost", (object) cost);
    return hashtable1;
  }

  public static int CalculateCost(int time, int price = 0)
  {
    if (time <= 0)
      return 0;
    int num1 = 0;
    if (price == 0)
    {
      price = 1;
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("time_per_gold");
      if (data != null)
        price = data.ValueInt;
    }
    int num2 = num1 + time / price;
    if (time % price > 0)
      ++num2;
    return num2;
  }

  public static int CalculateCost(Hashtable ht)
  {
    double num1 = 0.0;
    if (ht.ContainsKey((object) "resources"))
    {
      Hashtable hashtable = ht[(object) "resources"] as Hashtable;
      ItemBag.ItemType itemType = ItemBag.ItemType.Invalid;
      CityData byUid = DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid);
      IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
      while (enumerator.MoveNext())
      {
        CityResourceInfo.ResourceType type = CityResourceInfo.ResourceType.FOOD;
        ItemBag.ItemType key = (ItemBag.ItemType) enumerator.Key;
        int num2 = 1;
        GameConfigInfo gameConfigInfo = (GameConfigInfo) null;
        switch (key)
        {
          case ItemBag.ItemType.food:
            type = CityResourceInfo.ResourceType.FOOD;
            gameConfigInfo = ConfigManager.inst.DB_GameConfig.GetData("food_per_gold");
            break;
          case ItemBag.ItemType.wood:
            type = CityResourceInfo.ResourceType.WOOD;
            gameConfigInfo = ConfigManager.inst.DB_GameConfig.GetData("wood_per_gold");
            break;
          case ItemBag.ItemType.ore:
            type = CityResourceInfo.ResourceType.ORE;
            gameConfigInfo = ConfigManager.inst.DB_GameConfig.GetData("ore_per_gold");
            break;
          case ItemBag.ItemType.silver:
            type = CityResourceInfo.ResourceType.SILVER;
            gameConfigInfo = ConfigManager.inst.DB_GameConfig.GetData("silver_per_gold");
            break;
          case ItemBag.ItemType.steel:
            type = CityResourceInfo.ResourceType.ORE;
            gameConfigInfo = ConfigManager.inst.DB_GameConfig.GetData("steel_per_gold");
            break;
        }
        if (gameConfigInfo != null)
          num2 = gameConfigInfo.ValueInt;
        long num3 = (long) int.Parse(enumerator.Value.ToString());
        double num4 = byUid.resources.GetCurrentResource(type);
        if (key == ItemBag.ItemType.steel)
          num4 = (double) (int) PlayerData.inst.userData.currency.steel;
        if (num4 >= (double) num3)
        {
          itemType = ItemBag.ItemType.Invalid;
        }
        else
        {
          double num5 = (double) num3 - num4;
          num1 += num5 / (double) num2;
          if (num5 % (double) num2 > 0.0)
            ++num1;
        }
      }
    }
    if (ht.ContainsKey((object) "time"))
    {
      int num2 = int.Parse(ht[(object) "time"].ToString());
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("time_per_gold");
      int num3 = 1;
      if (data != null)
        num3 = data.ValueInt;
      num1 += (double) (num2 / num3);
      if (num2 % num3 > 0)
        ++num1;
    }
    if (ht.ContainsKey((object) "kingdom_hospital_heal_time"))
    {
      int num2 = int.Parse(ht[(object) "kingdom_hospital_heal_time"].ToString());
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("time_per_gold_kingdom_hospital");
      int num3 = 1;
      if (data != null)
        num3 = data.ValueInt;
      num1 += (double) (num2 / num3);
      if (num2 % num3 > 0)
        ++num1;
    }
    if (ht.ContainsKey((object) "items"))
    {
      Hashtable hashtable = ht[(object) "items"] as Hashtable;
      foreach (object key in (IEnumerable) hashtable.Keys)
      {
        string s = key.ToString();
        int num2 = int.Parse(hashtable[(object) s].ToString());
        int result = -1;
        int.TryParse(s, out result);
        if (result > 0)
        {
          ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(result);
          if (dataByInternalId != null && num2 > 0)
          {
            int num3 = num2 - ItemBag.Instance.GetItemCount(dataByInternalId.Item_InternalId);
            for (int index = 0; index < num3; ++index)
              num1 += (double) ItemBag.Instance.GetShopItemPrice(dataByInternalId.ID);
          }
        }
      }
    }
    return (int) num1;
  }

  public static Dictionary<string, long> CalculateLackResources(Hashtable ht)
  {
    Dictionary<string, long> dictionary = new Dictionary<string, long>(4);
    Hashtable hashtable1 = new Hashtable();
    List<ShopStaticInfo> shopStaticInfoList = new List<ShopStaticInfo>();
    ArrayList arrayList = new ArrayList();
    if (ht.ContainsKey((object) "resources"))
    {
      Hashtable hashtable2 = ht[(object) "resources"] as Hashtable;
      ItemBag.ItemType itemType = ItemBag.ItemType.Invalid;
      CityData byUid = DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid);
      IDictionaryEnumerator enumerator = hashtable2.GetEnumerator();
      while (enumerator.MoveNext())
      {
        CityResourceInfo.ResourceType type = CityResourceInfo.ResourceType.FOOD;
        ItemBag.ItemType key1 = (ItemBag.ItemType) enumerator.Key;
        switch (key1)
        {
          case ItemBag.ItemType.food:
            type = CityResourceInfo.ResourceType.FOOD;
            break;
          case ItemBag.ItemType.wood:
            type = CityResourceInfo.ResourceType.WOOD;
            break;
          case ItemBag.ItemType.ore:
            type = CityResourceInfo.ResourceType.ORE;
            break;
          case ItemBag.ItemType.silver:
            type = CityResourceInfo.ResourceType.SILVER;
            break;
        }
        shopStaticInfoList = ItemBag.Instance.GetCurrentSaleShopItems("shop", key1);
        long num1 = long.Parse(enumerator.Value.ToString());
        long num2 = (long) byUid.resources.GetCurrentResource(type);
        if (key1 == ItemBag.ItemType.steel)
          num2 = PlayerData.inst.userData.currency.steel;
        if (num2 >= num1)
        {
          itemType = ItemBag.ItemType.Invalid;
        }
        else
        {
          string key2 = string.Format("Texture/BuildingConstruction/{0}_icon", (object) key1.ToString());
          dictionary.Add(key2, num1 - num2);
        }
      }
    }
    return dictionary;
  }

  public static Dictionary<string, int> CalculateLackItems(Hashtable ht)
  {
    Dictionary<string, int> dictionary = new Dictionary<string, int>(4);
    if (ht.ContainsKey((object) "items"))
    {
      IDictionaryEnumerator enumerator = (ht[(object) "items"] as Hashtable).GetEnumerator();
      while (enumerator.MoveNext())
      {
        string s = enumerator.Key.ToString();
        int num1 = int.Parse(enumerator.Value.ToString());
        int result = -1;
        int.TryParse(s, out result);
        if (result > 0)
        {
          ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(result);
          if (dataByInternalId != null && ConfigManager.inst.DB_Items.GetItem(dataByInternalId.Item_InternalId) != null)
          {
            int itemCount = ItemBag.Instance.GetItemCount(dataByInternalId.Item_InternalId);
            ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(dataByInternalId.Item_InternalId);
            int num2 = num1;
            string imagePath = itemStaticInfo.ImagePath;
            if (itemCount < num2 && !dictionary.ContainsKey(imagePath))
              dictionary.Add(imagePath, num2 - itemCount);
          }
        }
      }
    }
    return dictionary;
  }

  private static void AddToList(List<ShopStaticInfo> list, ref ArrayList addToList, int left, ref int cost)
  {
    while (left > 0)
    {
      for (int index = 0; index < list.Count; ++index)
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(list[index].Item_InternalId);
        int num = (int) itemStaticInfo.Value;
        if (num >= left)
        {
          if (itemStaticInfo.Type != ItemBag.ItemType.speedup)
            addToList.Add((object) list[index].internalId);
          left -= num;
          cost += ItemBag.Instance.GetShopItemPrice(list[index].ID);
          break;
        }
        if (index == list.Count - 1)
        {
          if (itemStaticInfo.Type != ItemBag.ItemType.speedup)
            addToList.Add((object) list[index].internalId);
          left -= num;
          cost += ItemBag.Instance.GetShopItemPrice(list[index].ID);
        }
      }
    }
  }

  private void InitShowShelves()
  {
    this._CurrentSaleShopItem = new List<ShopStaticInfo>();
    this._WillStaleShopItems = new List<ShopStaticInfo>();
    this._WillTakeOffShelves = new List<ShopStaticInfo>();
    Dictionary<string, ShopStaticInfo>.Enumerator enumerator = ConfigManager.inst.DB_Shop.GetTotalShopStaticDatas().GetEnumerator();
    while (enumerator.MoveNext())
    {
      if ((long) NetServerTime.inst.ServerTimestamp >= enumerator.Current.Value.Sale_Start_Time)
        this._CurrentSaleShopItem.Add(enumerator.Current.Value);
      else
        this._WillStaleShopItems.Add(enumerator.Current.Value);
      if (enumerator.Current.Value.Limited_Sales_End_Time > (long) NetServerTime.inst.ServerTimestamp)
        this._WillTakeOffShelves.Add(enumerator.Current.Value);
    }
  }

  public void StartSyncTime()
  {
    this.InitShowShelves();
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  private void Process(int time)
  {
    if (this._WillStaleShopItems == null)
      return;
    List<ShopStaticInfo> shopStaticInfoList = new List<ShopStaticInfo>();
    if (this._WillStaleShopItems.Count > 0)
    {
      List<ShopStaticInfo>.Enumerator enumerator = this._WillStaleShopItems.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if ((long) NetServerTime.inst.ServerTimestamp >= enumerator.Current.Sale_Start_Time)
        {
          shopStaticInfoList.Add(enumerator.Current);
          if (!this._CurrentSaleShopItem.Contains(enumerator.Current))
            this._CurrentSaleShopItem.Add(enumerator.Current);
        }
      }
    }
    if (this._WillTakeOffShelves.Count > 0)
    {
      List<ShopStaticInfo>.Enumerator enumerator = this._WillTakeOffShelves.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if ((long) NetServerTime.inst.ServerTimestamp >= enumerator.Current.Limited_Sales_End_Time)
        {
          shopStaticInfoList.Add(enumerator.Current);
          if (this._CurrentSaleShopItem.Contains(enumerator.Current))
            this._CurrentSaleShopItem.Remove(enumerator.Current);
        }
      }
    }
    if (shopStaticInfoList.Count <= 0)
      return;
    List<ShopStaticInfo>.Enumerator enumerator1 = shopStaticInfoList.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      if (this._WillTakeOffShelves.Contains(enumerator1.Current))
        this._WillTakeOffShelves.Remove(enumerator1.Current);
      if (this._WillStaleShopItems.Contains(enumerator1.Current))
        this._WillStaleShopItems.Remove(enumerator1.Current);
    }
  }

  public void StopSyncTime()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
    if (this._CurrentSaleShopItem != null)
    {
      this._CurrentSaleShopItem.Clear();
      this._CurrentSaleShopItem = (List<ShopStaticInfo>) null;
    }
    if (this._WillStaleShopItems != null)
    {
      this._WillStaleShopItems.Clear();
      this._WillStaleShopItems = (List<ShopStaticInfo>) null;
    }
    if (this._WillTakeOffShelves == null)
      return;
    this._WillTakeOffShelves.Clear();
    this._WillTakeOffShelves = (List<ShopStaticInfo>) null;
  }

  public List<ShopStaticInfo> GetCurrentSaleShopItemsByItem_InternalId(int item_internalId)
  {
    List<ShopStaticInfo> shopStaticInfoList = new List<ShopStaticInfo>();
    List<ShopStaticInfo>.Enumerator enumerator = this.GetCurrentSaleShopItems("shop", string.Empty).GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.Item_InternalId == item_internalId)
        shopStaticInfoList.Add(enumerator.Current);
    }
    return shopStaticInfoList;
  }

  public List<ShopStaticInfo> GetCurrentSaleShopItems(string location, ItemBag.ItemType Type)
  {
    List<ShopStaticInfo> shopStaticInfoList = new List<ShopStaticInfo>();
    List<ShopStaticInfo>.Enumerator enumerator = this.GetCurrentSaleShopItems(location, string.Empty).GetEnumerator();
    while (enumerator.MoveNext())
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(enumerator.Current.Item_InternalId);
      if (itemStaticInfo != null && itemStaticInfo.Type == Type)
        shopStaticInfoList.Add(enumerator.Current);
    }
    return shopStaticInfoList;
  }

  public List<ShopStaticInfo> GetCurrentSaleShopItems(string location = "shop", string category = "")
  {
    List<ShopStaticInfo> shopStaticInfoList = new List<ShopStaticInfo>();
    if (this._CurrentSaleShopItem == null)
      this.InitShowShelves();
    for (int index = 0; index < this._CurrentSaleShopItem.Count; ++index)
    {
      ShopStaticInfo shopStaticInfo = this._CurrentSaleShopItem[index];
      if (!(shopStaticInfo.Location != location) && shopStaticInfo.Visible != 0 && (string.IsNullOrEmpty(category) || !(shopStaticInfo.Category != category)))
        shopStaticInfoList.Add(shopStaticInfo);
    }
    return shopStaticInfoList;
  }

  public void BuySevenDayItem(int itemId, int amount = 1, Hashtable extra = null, System.Action<bool, object> callback = null)
  {
    ItemStaticInfo info = ConfigManager.inst.DB_Items.GetItem(itemId);
    RewardsCollectionAnimator.Instance.Clear();
    RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
    {
      count = (float) amount,
      icon = info.ImagePath
    });
    RewardsCollectionAnimator.Instance.CollectItems(true);
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    postData.Add((object) "item_id", (object) itemId);
    postData.Add((object) nameof (amount), (object) amount);
    if (extra != null)
    {
      IDictionaryEnumerator enumerator = extra.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (!postData.ContainsKey(enumerator.Key))
          postData[enumerator.Key] = enumerator.Value;
      }
    }
    MessageHub.inst.GetPortByAction("sevenDays:buyItems").SendRequest(postData, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (arg1)
      {
        ConfigManager.inst.DB_Toast.GetData("toast_item_buy_item").icon = info.ImagePath;
        AudioManager.Instance.PlaySound("sfx_item_buy", false);
        ToastManager.Instance.AddBasicToast("toast_item_buy_item", "item_name", info.LocName, "num", amount.ToString()).SetPriority(-1);
      }
      if (callback == null)
        return;
      callback(arg1, arg2);
    }), true);
  }

  public void BuyShopItem(int shopItemID, int amount = 1, Hashtable extra = null, System.Action<bool, object> callback = null)
  {
    ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(shopItemID);
    ItemStaticInfo info = ConfigManager.inst.DB_Items.GetItem(dataByInternalId.Item_InternalId);
    if (!this.CheckCanBuyShopItem(dataByInternalId.ID, false))
      return;
    RewardsCollectionAnimator.Instance.Clear();
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(dataByInternalId.Item_InternalId);
    RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
    {
      count = (float) amount,
      icon = itemStaticInfo.ImagePath
    });
    RewardsCollectionAnimator.Instance.CollectItems(true);
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    postData.Add((object) "goods_id", (object) dataByInternalId.internalId);
    postData.Add((object) nameof (amount), (object) amount);
    if (PlayerData.inst.userData.current_world_id > 0)
      postData.Add((object) "current_world_id", (object) PlayerData.inst.userData.current_world_id);
    if (extra != null)
    {
      IDictionaryEnumerator enumerator = extra.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (!postData.ContainsKey(enumerator.Key))
          postData[enumerator.Key] = enumerator.Value;
      }
    }
    MessageHub.inst.GetPortByAction("Item:buyConsumableGoods").SendRequest(postData, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (arg1)
      {
        ConfigManager.inst.DB_Toast.GetData("toast_item_buy_item").icon = info.ImagePath;
        AudioManager.Instance.PlaySound("sfx_item_buy", false);
        ToastManager.Instance.AddBasicToast("toast_item_buy_item", "item_name", info.LocName, "num", amount.ToString()).SetPriority(-1);
      }
      if (callback == null)
        return;
      callback(arg1, arg2);
    }), true);
  }

  public bool CheckCanBuyShopItem(string shopItemID, bool displayTip = false)
  {
    ShopStaticInfo shopData = ConfigManager.inst.DB_Shop.GetShopData(shopItemID);
    if (shopData.Limited_Sales_End_Time > 0L && shopData.Limited_Sales_End_Time < (long) NetServerTime.inst.ServerTimestamp)
    {
      if (displayTip)
        this.DisplayTip("buy_shopitem_takeoffshlaves", "shop_buy_failtitle", "shop_buy_okbtlabel");
      return false;
    }
    if (shopData.Req_Vip_Level > PlayerData.inst.hostPlayer.VIPLevel)
    {
      if (displayTip)
        this.DisplayTip("buy_shopitem_failvip", "shop_buy_failtitle", "shop_buy_okbtlabel");
      return false;
    }
    if (shopData.Location == "alliance" && PlayerData.inst.allianceId <= 0L)
    {
      if (displayTip)
        this.DisplayTip("buy_shopitem_nojoinalliance", "shop_buy_failtitle", "shop_buy_okbtlabel");
      return false;
    }
    if (!string.IsNullOrEmpty(shopData.Req_Id_1))
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(shopData.Req_Id_1);
      BuildingController buildingByType = CitadelSystem.inst.GetBuildingByType(data.Type);
      if ((UnityEngine.Object) buildingByType == (UnityEngine.Object) null || buildingByType.mBuildingItem.mLevel < data.Building_Lvl)
      {
        if (displayTip)
          this.DisplayTip("buy_shopitem_nobuilding", "shop_buy_failtitle", "shop_buy_okbtlabel");
        return false;
      }
    }
    if (shopData.Req_Id_2 > 0 && shopData.Req_Id_2 > PlayerData.inst.allianceData.allianceLevel)
    {
      if (displayTip)
        this.DisplayTip("buy_shopitem_alliancelevel", "shop_buy_failtitle", "shop_buy_okbtlabel");
      return false;
    }
    int shopItemPrice = this.GetShopItemPrice(shopItemID);
    if (shopData.Currency_Buy_type == "gold")
    {
      if (shopItemPrice > PlayerData.inst.hostPlayer.Currency)
      {
        if (displayTip)
          this.DisplayTip("buy_shopitem_noenoughgold", "shop_buy_failtitle", "shop_buy_okbtlabel");
        return false;
      }
    }
    else if (shopData.Currency_Buy_type == "loyalty")
    {
      UserData userData = DBManager.inst.DB_User.Get(PlayerData.inst.uid);
      if (PlayerData.inst.allianceData != null && (long) shopItemPrice > userData.currency.honor)
      {
        if (displayTip)
          this.DisplayTip("buy_shopitem_noenoughhonur", "shop_buy_failtitle", "shop_buy_okbtlabel");
        return false;
      }
    }
    return true;
  }

  public int GetShopItemPrice(int shopId)
  {
    return this.GetShopItemPrice(ConfigManager.inst.DB_Shop.GetShopDataByInternalId(shopId).ID);
  }

  public int GetShopItemPrice(string shopItemID)
  {
    int price = ConfigManager.inst.DB_Shop.GetShopData(shopItemID).Price;
    float num = this.GetShopItemDiscount(shopItemID);
    if ((double) num == 0.0)
      num = 1f;
    return (int) Math.Ceiling((double) num * (double) price);
  }

  public float GetShopItemDiscount(string shopItemID)
  {
    ShopStaticInfo shopData = ConfigManager.inst.DB_Shop.GetShopData(shopItemID);
    float num = shopData.DisCount;
    if (shopData.Limit_Discount_Start_Time < (long) NetServerTime.inst.ServerTimestamp && shopData.Limit_Discount_End_Time > (long) NetServerTime.inst.ServerTimestamp && (double) shopData.Discount_In_Limit_Time > 0.0)
      num = shopData.Discount_In_Limit_Time;
    return num;
  }

  public void DisplayTip(string tip, string title = "shop_buy_failtitle", string label = "shop_buy_okbtlabel")
  {
    tip = ScriptLocalization.Get(tip, true);
    title = ScriptLocalization.Get(label, true);
    label = ScriptLocalization.Get(label, true);
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = title,
      Content = tip,
      Okay = label,
      OkayCallback = (System.Action) null
    });
  }

  public int GetShopStaticQuanlity(string shopItemID)
  {
    return DBManager.inst.DB_Item.GetQuantity(ConfigManager.inst.DB_Items.GetItem(ConfigManager.inst.DB_Shop.GetShopData(shopItemID).Item_InternalId).internalId);
  }

  public void BuyAllianceGift(string shopItemID, int amount = 1, Hashtable extra = null, System.Action<bool, object> callback = null)
  {
    if (PlayerData.inst.allianceId <= 0L || !this.CheckCanBuyShopItem(ConfigManager.inst.DB_Shop.GetShopData(shopItemID).ID, false))
      return;
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    postData.Add((object) "his_uid", (object) PlayerData.inst.allianceId);
    postData.Add((object) nameof (amount), (object) amount);
    postData.Add((object) "goods_id", (object) shopItemID);
    if (PlayerData.inst.userData.current_world_id > 0)
      postData.Add((object) "current_world_id", (object) PlayerData.inst.userData.current_world_id);
    if (extra != null)
    {
      IDictionaryEnumerator enumerator = extra.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (!postData.ContainsKey(enumerator.Key))
          postData[enumerator.Key] = enumerator.Value;
      }
    }
    MessageHub.inst.GetPortByAction("Item:buyConsumableGoodsAsGift").SendRequest(postData, callback, true);
  }

  public static bool CanShopItemBeUsedInStore(ShopStaticInfo shopInfo)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(shopInfo.Item_InternalId);
    if (itemStaticInfo.CanUse && itemStaticInfo.Type != ItemBag.ItemType.marchrecall && (itemStaticInfo.Type != ItemBag.ItemType.speedup && itemStaticInfo.Type != ItemBag.ItemType.marchspeed) && (itemStaticInfo.Type != ItemBag.ItemType.targetteleport && itemStaticInfo.Type != ItemBag.ItemType.buildingdeconstruct && itemStaticInfo.Type != ItemBag.ItemType.buildingrelocate))
      return itemStaticInfo.Type != ItemBag.ItemType.rename;
    return false;
  }

  public static bool CanUseInItemBag(int item_id)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(item_id);
    bool canUse = itemStaticInfo.CanUse;
    switch (itemStaticInfo.Type)
    {
      case ItemBag.ItemType.boss_summoner:
      case ItemBag.ItemType.treasure_map:
        UserData userData = PlayerData.inst.userData;
        if (!userData.IsForeigner && canUse)
          return !userData.IsPit;
        return false;
      default:
        return canUse;
    }
  }

  public static int GetItemLeftCdTime(int itemId)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo != null)
      return itemStaticInfo.RemainTime;
    return 0;
  }

  public static bool IsDragonItemUse(int item_id)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(item_id);
    bool flag = true;
    switch (itemStaticInfo.Type)
    {
      case ItemBag.ItemType.dragonrename:
      case ItemBag.ItemType.dragonexp:
      case ItemBag.ItemType.dragondark:
      case ItemBag.ItemType.dragonlight:
        flag = PlayerData.inst.dragonData != null;
        break;
    }
    return flag;
  }

  public static bool IsDragonKnightItemUse(int item_id)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(item_id);
    bool flag = true;
    switch (itemStaticInfo.Type)
    {
      case ItemBag.ItemType.dragon_knight_xp:
      case ItemBag.ItemType.dragon_knight_rename:
        flag = PlayerData.inst.dragonKnightData != null;
        break;
    }
    return flag;
  }

  public int GetItemCount(int itemInternalId)
  {
    return DBManager.inst.DB_Item.GetQuantity(itemInternalId);
  }

  public int GetItemCountByShopID(string shopID)
  {
    ShopStaticInfo shopData = ConfigManager.inst.DB_Shop.GetShopData(shopID);
    if (shopData == null)
      return 0;
    return this.GetItemCount(shopData.Item_InternalId);
  }

  public int GetItemCountByShopID(int shopInternalId)
  {
    ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(shopInternalId);
    if (dataByInternalId == null)
      return 0;
    return this.GetItemCount(dataByInternalId.Item_InternalId);
  }

  public int GetItemCount(string itemId)
  {
    int num = 0;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo != null)
      num = DBManager.inst.DB_Item.GetQuantity(itemStaticInfo.internalId);
    return num;
  }

  public void UseItemByShopItemID(string shopId, Hashtable extra = null, System.Action<bool, object> callback = null, int useCount = 1)
  {
    this.UseItem(ConfigManager.inst.DB_Items.GetItem(ConfigManager.inst.DB_Shop.GetShopData(shopId).Item_InternalId).internalId, useCount, extra, callback);
  }

  public void UseItem(string itemID, Hashtable extra = null, System.Action<bool, object> callback = null, int useCount = 1)
  {
    this.UseItem(ConfigManager.inst.DB_Items.GetItem(itemID).internalId, useCount, extra, callback);
  }

  public bool CheckCanUse(int item_id)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(item_id);
    return CityManager.inst.mStronghold.mLevel >= itemStaticInfo.RequireLev;
  }

  public void UseItem(int itemInternalId, int useCount, Hashtable extra, System.Action<bool, object> callback)
  {
    ConsumableItemData item = DBManager.inst.DB_Item.Get(itemInternalId);
    ItemStaticInfo info = ConfigManager.inst.DB_Items.GetItem(itemInternalId);
    if (!this.CheckCanUse(itemInternalId))
    {
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_item_requirement_tip", new Dictionary<string, string>()
      {
        {
          "num",
          info.RequireLev.ToString()
        }
      }, true), (System.Action) null, 4f, false);
    }
    else
    {
      if (item == null)
        return;
      Hashtable postData = new Hashtable();
      postData.Add((object) "uid", (object) PlayerData.inst.uid);
      postData.Add((object) "item_id", (object) itemInternalId);
      postData.Add((object) "city_id", (object) PlayerData.inst.cityId);
      postData.Add((object) "amount", (object) useCount);
      if (PlayerData.inst.userData.current_world_id > 0)
        postData.Add((object) "current_world_id", (object) PlayerData.inst.userData.current_world_id);
      if (extra != null)
      {
        IDictionaryEnumerator enumerator = extra.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (!postData.ContainsKey(enumerator.Key))
            postData[enumerator.Key] = enumerator.Value;
        }
      }
      if (item.quantity < useCount)
        return;
      MessageHub.inst.GetPortByAction("Item:useConsumableItem").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
        {
          if (info.Type == ItemBag.ItemType.chest)
            UIManager.inst.OpenPopup("ChestItemDetailPopup", (Popup.PopupParameter) new ChestItemDetailPopup.Parameter()
            {
              result = data
            });
          else if (info.Type == ItemBag.ItemType.equipment_gem)
            UIManager.inst.OpenPopup("Gem/GemUsedDetailPopup", (Popup.PopupParameter) new GemUsedDetailPopup.Parameter()
            {
              itemId = item.internalId,
              itemCount = useCount
            });
          ConfigManager.inst.DB_Toast.GetData("toast_item_use_item").icon = info.ImagePath;
          ToastManager.Instance.AddBasicToast("toast_item_use_item", "item_name", info.LocName, "num", useCount.ToString()).SetPriority(-1);
          AudioManager.Instance.PlaySound("sfx_item_use", false);
        }
        if (callback == null)
          return;
        callback(ret, data);
      }), true);
    }
  }

  public void BuyAndUseShopItem(int shopInternalId, System.Action<bool, object> callback = null, Hashtable extra = null, int amount = 1)
  {
    this.BuyAndUseShopItem(shopInternalId, amount, extra, callback);
  }

  public void BuyAndUseShopItem(string shopID, Hashtable extra = null, System.Action<bool, object> callback = null, int amount = 1)
  {
    this.BuyAndUseShopItem(ConfigManager.inst.DB_Shop.GetShopData(shopID).internalId, amount, extra, callback);
  }

  public List<ConsumableItemData> GetSpeedUpItemsByType(ItemBag.ItemType speedupType)
  {
    if (speedupType == ItemBag.ItemType.Invalid)
      return (List<ConsumableItemData>) null;
    List<ConsumableItemData> consumableItemDataList = new List<ConsumableItemData>();
    Dictionary<long, ConsumableItemData>.Enumerator enumerator = DBManager.inst.DB_Item.Datas.GetEnumerator();
    while (enumerator.MoveNext())
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem((int) enumerator.Current.Key);
      if (itemStaticInfo == null)
        D.error((object) ("can not found item ! internal id is " + (object) (int) enumerator.Current.Key));
      if (itemStaticInfo.Type == speedupType)
        consumableItemDataList.Add(enumerator.Current.Value);
    }
    return consumableItemDataList;
  }

  public List<ConsumableItemData> GetSpeedUpItems()
  {
    return this.GetSpeedUpItemsByType(ItemBag.ItemType.speedup);
  }

  public List<ConsumableItemData> GetSpeedUpItems(ItemBag.ItemType itemType)
  {
    return this.GetSpeedUpItemsByType(itemType);
  }

  public List<ConsumableItemData> GetConsumableScrollItems(BagType bagType)
  {
    List<ConsumableItemData> consumableItemDataList = new List<ConsumableItemData>();
    Dictionary<long, ConsumableItemData>.Enumerator enumerator = DBManager.inst.DB_Item.Datas.GetEnumerator();
    while (enumerator.MoveNext())
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem((int) enumerator.Current.Key);
      if (itemStaticInfo != null)
      {
        if (bagType == BagType.Hero && itemStaticInfo.Type == ItemBag.ItemType.equipment_scroll)
          consumableItemDataList.Add(enumerator.Current.Value);
        else if (bagType == BagType.DragonKnight && itemStaticInfo.Type == ItemBag.ItemType.dk_equipment_scroll)
          consumableItemDataList.Add(enumerator.Current.Value);
      }
    }
    return consumableItemDataList;
  }

  public List<ConsumableItemData> GetConsumableScrollChipItems(BagType bagType)
  {
    List<ConsumableItemData> consumableItemDataList = new List<ConsumableItemData>();
    Dictionary<long, ConsumableItemData>.Enumerator enumerator = DBManager.inst.DB_Item.Datas.GetEnumerator();
    while (enumerator.MoveNext())
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem((int) enumerator.Current.Key);
      if (itemStaticInfo == null)
        D.warn((object) ("itemInfo not foud ! id =" + (object) (int) enumerator.Current.Key));
      else if (bagType == BagType.Hero && itemStaticInfo.Type == ItemBag.ItemType.equipment_scroll_chip)
        consumableItemDataList.Add(enumerator.Current.Value);
      else if (bagType == BagType.DragonKnight && itemStaticInfo.Type == ItemBag.ItemType.dk_equipment_scroll_chip)
        consumableItemDataList.Add(enumerator.Current.Value);
    }
    return consumableItemDataList;
  }

  public List<ConsumableItemData> GetConsumableEquipmentMaterialItems(BagType bagType)
  {
    List<ConsumableItemData> consumableItemDataList = new List<ConsumableItemData>();
    Dictionary<long, ConsumableItemData>.Enumerator enumerator = DBManager.inst.DB_Item.Datas.GetEnumerator();
    while (enumerator.MoveNext())
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem((int) enumerator.Current.Key);
      if (itemStaticInfo == null)
        D.warn((object) ("itemInfo not foud ! id =" + (object) (int) enumerator.Current.Key));
      else if (bagType == BagType.Hero && itemStaticInfo.Type == ItemBag.ItemType.equipment_material)
        consumableItemDataList.Add(enumerator.Current.Value);
      else if (bagType == BagType.DragonKnight && itemStaticInfo.Type == ItemBag.ItemType.dk_equipment_material)
        consumableItemDataList.Add(enumerator.Current.Value);
    }
    return consumableItemDataList;
  }

  public List<ItemStaticInfo> GetVIPItems()
  {
    return ConfigManager.inst.DB_Items.GetItemListByType(ItemBag.ItemType.vip);
  }

  public List<ItemStaticInfo> GetSteelItems()
  {
    return ConfigManager.inst.DB_Items.GetItemListByType(ItemBag.ItemType.steel);
  }

  public List<ItemStaticInfo> GetVIPPointItems()
  {
    return ConfigManager.inst.DB_Items.GetItemListByType(ItemBag.ItemType.vippoint);
  }

  public List<ItemStaticInfo> GetBoostItems()
  {
    return ConfigManager.inst.DB_Items.GetItemListByType(ItemBag.ItemType.boost);
  }

  public List<ConsumableItemData> GetItemsNoInShop(ItemBag.ItemType speedupType = ItemBag.ItemType.Invalid)
  {
    List<ConsumableItemData> consumableItemDataList = new List<ConsumableItemData>();
    Dictionary<long, ConsumableItemData>.Enumerator enumerator = DBManager.inst.DB_Item.Datas.GetEnumerator();
    while (enumerator.MoveNext())
    {
      ShopStaticInfo byItemInternalId = ConfigManager.inst.DB_Shop.GetShopInfoByItemInternalId(enumerator.Current.Value.internalId);
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem((int) enumerator.Current.Key);
      if ((byItemInternalId == null || byItemInternalId.Value != 1) && (speedupType == ItemBag.ItemType.Invalid || itemStaticInfo.Type == speedupType))
        consumableItemDataList.Add(enumerator.Current.Value);
    }
    return consumableItemDataList;
  }

  public void BuyAndUseShopItem(int shopInternalId, int amount = 1, Hashtable extra = null, System.Action<bool, object> callback = null)
  {
    ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(shopInternalId);
    ItemStaticInfo info = ConfigManager.inst.DB_Items.GetItem(dataByInternalId.Item_InternalId);
    if (!this.CheckCanUse(info.internalId))
    {
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_item_requirement_tip", new Dictionary<string, string>()
      {
        {
          "num",
          info.RequireLev.ToString()
        }
      }, true), (System.Action) null, 4f, false);
    }
    else
    {
      if (!this.CheckCanBuyShopItem(dataByInternalId.ID, false))
        return;
      Hashtable postData = new Hashtable();
      postData.Add((object) "uid", (object) PlayerData.inst.uid);
      postData.Add((object) "goods_id", (object) shopInternalId);
      postData.Add((object) "city_id", (object) PlayerData.inst.cityId);
      postData.Add((object) nameof (amount), (object) amount);
      bool flag = PlayerData.inst.moderatorInfo.IsModerator && PlayerData.inst.moderatorInfo.OpenModMode;
      postData.Add((object) "is_mod", (object) (!flag ? 0 : 1));
      if (PlayerData.inst.userData.current_world_id > 0)
        postData.Add((object) "current_world_id", (object) PlayerData.inst.userData.current_world_id);
      if (extra != null)
      {
        IDictionaryEnumerator enumerator = extra.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (!postData.ContainsKey(enumerator.Key))
            postData[enumerator.Key] = enumerator.Value;
        }
      }
      MessageHub.inst.GetPortByAction("Item:buyAndUseConsumableGoods").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
        {
          if (info.Type == ItemBag.ItemType.chest)
            UIManager.inst.OpenPopup("ChestItemDetailPopup", (Popup.PopupParameter) new ChestItemDetailPopup.Parameter()
            {
              result = data
            });
          ConfigManager.inst.DB_Toast.GetData("toast_item_use_item").icon = info.ImagePath;
          ToastManager.Instance.AddBasicToast("toast_item_use_item", "item_name", info.LocName, "num", amount.ToString()).SetPriority(-1);
        }
        if (callback == null)
          return;
        callback(ret, data);
      }), true);
    }
  }

  public bool IsNewItem(int itemId)
  {
    return this.newItems.IndexOf(itemId) > -1;
  }

  public bool HadNewItems()
  {
    return this.newItems.Count > 0;
  }

  public void ClearNewItems()
  {
    this.newItems.Clear();
    if (this.OnNewItemChange == null)
      return;
    this.OnNewItemChange();
  }

  public void RemoveNew(int itemId)
  {
    if (!this.newItems.Contains(itemId))
      return;
    this.newItems.Remove(itemId);
  }

  public void MarkNewItem(int itemId)
  {
    if (!this.newItems.Contains(itemId))
      this.newItems.Add(itemId);
    if (this.OnNewItemChange == null)
      return;
    this.OnNewItemChange();
  }

  public void Initialize()
  {
    this.AddEventHandler();
  }

  public void Dispose()
  {
    this.RemoveEventHandler();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Item.onDataCreated += new System.Action<int>(this.OnCreateHandler);
    DBManager.inst.DB_Item.onDataUpdated += new System.Action<int>(this.OnUpdateHandler);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnRemoveHandler);
  }

  private void OnRemoveHandler(int itemId)
  {
    if (!this.newItems.Contains(itemId))
      return;
    this.newItems.Remove(itemId);
    if (this.OnNewItemChange == null)
      return;
    this.OnNewItemChange();
  }

  private void OnUpdateHandler(int itemId)
  {
    ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(itemId);
    if (consumableItemData == null || !consumableItemData.IsIncrease)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo != null && !itemStaticInfo.InStore)
      return;
    this.MarkNewItem(itemId);
  }

  private void OnCreateHandler(int itemId)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo != null && !itemStaticInfo.InStore)
      return;
    this.MarkNewItem(itemId);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Item.onDataCreated -= new System.Action<int>(this.OnCreateHandler);
    DBManager.inst.DB_Item.onDataUpdated -= new System.Action<int>(this.OnUpdateHandler);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnRemoveHandler);
  }

  public void SellItem(int itemID, int count, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("Blacksmith:sellItem").SendRequest(new Hashtable()
    {
      {
        (object) "item_id",
        (object) itemID
      },
      {
        (object) "amount",
        (object) count
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public enum ItemType
  {
    Invalid,
    refresh,
    speedup,
    heroxp,
    boost,
    marchspeed,
    marchsize,
    attack,
    defense,
    beginnerteleport,
    randomteleport,
    targetteleport,
    food,
    wood,
    ore,
    silver,
    chest,
    casino,
    vip,
    material,
    gem,
    buildingreq,
    warrally,
    starterpack,
    attackbundle,
    defensebundle,
    resourcebundle,
    strongholdbundle,
    socketkey,
    skillreset,
    monsterattack,
    resourceboost,
    foodboost,
    oreboost,
    silverboost,
    woodboost,
    gemsaver,
    pvetrophy,
    rename,
    upkeepreduction,
    runes,
    buildingdeconstruct,
    equipmentgamble,
    buildingrelocate,
    herorevive,
    questchest,
    dailyrefresh,
    alliancerefresh,
    heroprofile,
    alliancechest,
    marchrecall,
    peaceshield,
    antiscout,
    refreshcasino,
    gatherspeed,
    playerportrait,
    energy,
    vippoint,
    player_rename,
    alliance_rename,
    dragonrename,
    lordexp,
    dragonexp,
    dragondark,
    dragonlight,
    dragonskillresetall,
    dragonskillresetsingle,
    wish_well_gem,
    gold,
    allianceteleport,
    buildqueue,
    equipment,
    equipment_scroll_chip,
    equipment_material,
    equipment_scroll,
    steel,
    megaphone,
    casino1,
    casino2,
    alliance_boss_energy,
    boss_summoner,
    castle_display,
    dragon_knight_xp,
    dragon_knight_talent_reset,
    dk_equipment,
    dk_equipment_scroll_chip,
    dk_equipment_scroll,
    dk_equipment_material,
    training_speedup,
    healing_speedup,
    building_speedup,
    research_speedup,
    forge_speedup,
    trap_speedup,
    dungeon_cd_speedup,
    treasure_map,
    buytroop,
    dragon_knight_rename,
    item_postman,
    fortress_teleport,
    equipment_gem,
    parliament_hero_exp,
    parliament_hero_card,
    parliament_hero_card_chip,
    altar_summon,
    lord_title,
  }
}
