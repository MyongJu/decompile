﻿// Decompiled with JetBrains decompiler
// Type: UnitNameCountImageComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class UnitNameCountImageComponent : ComponentRenderBase
{
  public UITexture iconTexture;
  public UILabel nameLabel;
  public UILabel amountLabel;

  public override void Init()
  {
    base.Init();
    UnitNameCountImageComponentData data1 = this.data as UnitNameCountImageComponentData;
    Unit_StatisticsInfo data2 = ConfigManager.inst.DB_Unit_Statistics.GetData(data1.unitInternalID);
    this.nameLabel.text = Utils.XLAT(data2.Troop_Name_LOC_ID);
    this.amountLabel.text = data1.amountString;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.iconTexture, data2.Troop_ICON, (System.Action<bool>) null, true, false, string.Empty);
  }

  public override void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.iconTexture);
  }
}
