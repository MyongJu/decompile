﻿// Decompiled with JetBrains decompiler
// Type: ResearchBennifitContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ResearchBennifitContent : MonoBehaviour
{
  public UILabel headDescription;
  public UILabel level;
  public UILabel content;

  public void SetContent(string bennifitName, string level, string content, NGUIText.Alignment alignment)
  {
    this.headDescription.text = bennifitName;
    this.level.text = level;
    this.content.alignment = alignment;
    this.content.text = content;
  }
}
