﻿// Decompiled with JetBrains decompiler
// Type: SpriteBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SpriteBanner : MonoBehaviour, IRecycle
{
  public TextMesh m_NameLabel;
  public TextMesh m_LevelLabel;
  public UISpriteMesh m_Background;
  public SortingOrderModifier[] m_SortingOrderModifiers;
  public PVPTempleSkillBannerPart m_TempleSkillBannerPart;
  public PVPKingSkillBannerPart m_KingSkillBannerPart;
  protected Renderer m_NameRenderer;
  private ISortingLayerCalculator m_SortingLayerCalculator;
  private bool m_SortingLayerChanged;

  private void Start()
  {
    if (!(bool) ((Object) this.m_NameLabel))
      return;
    this.m_NameRenderer = this.m_NameLabel.GetComponent<Renderer>();
  }

  public virtual void OnInitialize()
  {
  }

  public virtual void OnFinalize()
  {
  }

  public string Name
  {
    set
    {
      if (!((Object) this.m_NameLabel != (Object) null))
        return;
      this.m_NameLabel.text = value;
    }
    get
    {
      if ((Object) this.m_NameLabel != (Object) null)
        return this.m_NameLabel.text;
      return string.Empty;
    }
  }

  public string Level
  {
    set
    {
      if (!((Object) this.m_LevelLabel != (Object) null))
        return;
      this.m_LevelLabel.text = value;
    }
    get
    {
      if ((Object) this.m_LevelLabel != (Object) null)
        return this.m_LevelLabel.text;
      return string.Empty;
    }
  }

  public static bool CanHaveBanner(TileType type)
  {
    switch (type)
    {
      case TileType.City:
      case TileType.Camp:
      case TileType.Resource1:
      case TileType.Resource2:
      case TileType.Resource3:
      case TileType.Resource4:
      case TileType.Encounter:
      case TileType.Wonder:
      case TileType.WonderTower:
      case TileType.GveCamp:
      case TileType.AllianceFortress:
      case TileType.EventVisitNPC:
      case TileType.AllianceWarehouse:
      case TileType.AllianceSuperMine:
      case TileType.AlliancePortal:
      case TileType.AllianceHospital:
      case TileType.AllianceTemple:
      case TileType.WorldBoss:
      case TileType.Digsite:
      case TileType.PitMine:
      case TileType.KingdomBoss:
        return true;
      default:
        return false;
    }
  }

  public virtual void UpdateUI(TileData tile)
  {
    if ((bool) ((Object) this.m_TempleSkillBannerPart))
      this.m_TempleSkillBannerPart.SetTileData(tile);
    if (!(bool) ((Object) this.m_KingSkillBannerPart))
      return;
    this.m_KingSkillBannerPart.SetTileData(tile);
  }

  public void SetSortingLayerID(ISortingLayerCalculator calculator)
  {
    this.m_SortingLayerCalculator = calculator;
    this.m_SortingLayerChanged = true;
  }

  private void Update()
  {
    if (!this.m_SortingLayerChanged)
      return;
    this.m_SortingLayerChanged = false;
    if (this.m_SortingOrderModifiers == null)
      return;
    for (int index = 0; index < this.m_SortingOrderModifiers.Length; ++index)
      this.m_SortingOrderModifiers[index].sortingLayer = this.m_SortingLayerCalculator.GetSortingLayerName();
  }
}
