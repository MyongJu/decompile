﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomManageList
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UI;
using UnityEngine;

public class ChatRoomManageList : MonoBehaviour
{
  private ChatRoomManageList.Data data = new ChatRoomManageList.Data();
  private HubPort searchHub;
  [SerializeField]
  private ChatRoomManageList.Panel panel;

  public void OnSearchValueChanged()
  {
    this.panel.invitePanel.FreshSearchButtonState();
  }

  public void Setup(long roomId)
  {
    this.Reset();
    this.data.roomId = roomId;
    this.data.roomData = DBManager.inst.DB_room.Get(this.data.roomId);
    this.panel.invitePanel.roomID = roomId;
    if (this.data.roomData == null)
      return;
    Dictionary<long, ChatRoomMember>.ValueCollection.Enumerator enumerator = this.data.roomData.members.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.uid == PlayerData.inst.uid)
      {
        this.data.userMemberData = enumerator.Current;
        break;
      }
    }
    if (this.data.userMemberData.title > ChatRoomMember.Title.admin)
    {
      this.panel.messageTitle.text = Utils.XLAT("chat_edit_welcome_message_subtitle");
      this.panel.messageButtonText.text = Utils.XLAT("chat_uppercase_edit_button");
      this.panel.leaveRoomTitle.text = Utils.XLAT("chat_disband_room_subtitle");
      this.panel.leaveRoomButtonText.text = Utils.XLAT("chat_uppercase_disband_button");
      this.panel.applicantContainer.gameObject.SetActive(true);
      ChatRoomManageContainer.param.roomId = this.data.roomId;
      this.panel.applicantContainer.Setup(ChatRoomManageContainer.param);
      this.panel.invitePanel.panel.SetActive(true);
    }
    else if (this.data.userMemberData.title > ChatRoomMember.Title.member)
    {
      this.panel.messageTitle.text = Utils.XLAT("chat_edit_welcome_message_subtitle");
      this.panel.messageButtonText.text = Utils.XLAT("chat_uppercase_edit_button");
      this.panel.leaveRoomTitle.text = Utils.XLAT("chat_leave_room_subtitle");
      this.panel.leaveRoomButtonText.text = Utils.XLAT("chat_uppercase_leave_button");
      this.panel.applicantContainer.gameObject.SetActive(true);
      ChatRoomManageContainer.param.roomId = this.data.roomId;
      this.panel.applicantContainer.Setup(ChatRoomManageContainer.param);
      this.panel.invitePanel.panel.SetActive(true);
    }
    else
    {
      this.panel.messageTitle.text = Utils.XLAT("chat_view_welcome_message_subtitle");
      this.panel.messageButtonText.text = Utils.XLAT("chat_uppercase_view_button");
      this.panel.leaveRoomTitle.text = Utils.XLAT("chat_leave_room_subtitle");
      this.panel.leaveRoomButtonText.text = Utils.XLAT("chat_uppercase_leave_button");
      this.panel.applicantContainer.gameObject.SetActive(false);
      this.panel.invitePanel.panel.SetActive(false);
    }
  }

  public void OnViewButtonClick()
  {
    if (this.data.userMemberData.title > ChatRoomMember.Title.member)
      UIManager.inst.OpenPopup("Chat/ChatRoomSettingPopup", (Popup.PopupParameter) new ChatRoomSettingPopup.Parameter()
      {
        roomData = this.data.roomData
      });
    else
      UIManager.inst.OpenPopup("Chat/ChatRoomSettingViewPopup", (Popup.PopupParameter) new ChatRoomSettingViewPopup.Parameter()
      {
        roomData = this.data.roomData
      });
  }

  public void OnLeaveButtonClick()
  {
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = ScriptLocalization.Get("id_uppercase_warning", true),
      content = (this.data.userMemberData.title != ChatRoomMember.Title.owner ? ScriptLocalization.Get("chat_room_leave_confirm_description", true) : ScriptLocalization.Get("chat_room_disband_confirm_description", true)),
      yes = ScriptLocalization.Get("id_uppercase_yes", true),
      no = ScriptLocalization.Get("id_uppercase_no", true),
      yesCallback = (System.Action) (() =>
      {
        if (this.data.userMemberData.title == ChatRoomMember.Title.owner)
        {
          MessageHub.inst.GetPortByAction("Chat:deleteRoom").SendRequest(Utils.Hash((object) "room_id", (object) this.data.roomId), (System.Action<bool, object>) ((result, orgSrc) =>
          {
            if (!result)
              return;
            UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
          }), true);
        }
        else
        {
          ChatMessageManager.SendChatMemberMessage(this.data.userMemberData.userName, "chat_room_leave_notice", this.data.roomData.channelId);
          MessageHub.inst.GetPortByAction("Chat:leaveRoom").SendRequest(Utils.Hash((object) "room_id", (object) this.data.roomId), (System.Action<bool, object>) ((result, orgSrc) =>
          {
            if (!result)
              return;
            UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
          }), true);
        }
      }),
      noCallback = (System.Action) (() => {})
    });
  }

  public void Reset()
  {
    this.panel.messageTitle = this.transform.Find("Table/0_WelcomMessageContainer/BaseInfoTitle").gameObject.GetComponent<UILabel>();
    this.panel.BT_view = this.transform.Find("Table/0_WelcomMessageContainer/Btn_Message").gameObject.GetComponent<UIButton>();
    this.panel.messageButtonText = this.transform.Find("Table/0_WelcomMessageContainer/Btn_Message/Label").gameObject.GetComponent<UILabel>();
    this.panel.leaveRoomTitle = this.transform.Find("Table/1_LeaveContainer/BaseInfoTitle").gameObject.GetComponent<UILabel>();
    this.panel.BT_leave = this.transform.Find("Table/1_LeaveContainer/Btn_Leave").gameObject.GetComponent<UIButton>();
    this.panel.leaveRoomButtonText = this.transform.Find("Table/1_LeaveContainer/Btn_Leave/Label").gameObject.GetComponent<UILabel>();
    this.panel.applicantContainer = this.transform.Find("Table/2_ApplicationContainer").gameObject.GetComponent<ChatRoomManageContainer>();
    this.panel.applicantContainer.title = ChatRoomMember.Title.application;
    this.panel.applicantContainer.Reset();
    this.panel.applicantContainer.title = ChatRoomMember.Title.application;
  }

  public void OnSearchUserClick()
  {
    if (this.searchHub == null)
      this.searchHub = MessageHub.inst.GetPortByAction("Player:searchUser");
    this.searchHub.SendRequest(Utils.Hash((object) "name", (object) this.panel.invitePanel.input.value), new System.Action<bool, object>(this.panel.invitePanel.SearchUserCallback), true);
  }

  public class Data
  {
    public long roomId;
    public ChatRoomData roomData;
    public ChatRoomMember userMemberData;
  }

  [Serializable]
  public class Panel
  {
    public UILabel messageTitle;
    public UILabel messageButtonText;
    public UIButton BT_view;
    public UILabel leaveRoomTitle;
    public UILabel leaveRoomButtonText;
    public UIButton BT_leave;
    public ChatRoomManageContainer applicantContainer;
    public ChatRoomManageList.InvitePanel invitePanel;
  }

  [Serializable]
  public class InvitePanel
  {
    private List<ChatRoomInviteBar> inviteBarList = new List<ChatRoomInviteBar>();
    public GameObject panel;
    public Transform inviteList;
    public ContactExpendTool inviteExpander;
    public UITable listTable;
    public ChatRoomInviteBar detailBarOrg;
    public UIInput input;
    public UIButton BT_search;
    public UIWidget SearchContainer;
    public UIWidget noResultContainer;
    public long roomID;

    public bool searchEnable
    {
      set
      {
        this.BT_search.isEnabled = value;
      }
    }

    public void FreshSearchButtonState()
    {
      bool flag = true;
      if (Encoding.UTF8.GetBytes(this.input.value).Length < 3)
        flag = false;
      this.BT_search.isEnabled = flag;
    }

    public void SearchUserCallback(bool result, object orgData)
    {
      if (!result)
        return;
      ArrayList data;
      if (DatabaseTools.CheckAndParseOrgData(orgData, out data))
      {
        int num = (int) ConfigManager.inst.DB_GameConfig.GetData("contact_search_limit_value").Value;
        if (this.inviteBarList.Count < data.Count)
        {
          int height = this.detailBarOrg.GetComponent<UIWidget>().height;
          for (int count = this.inviteBarList.Count; count < data.Count && count < num; ++count)
          {
            GameObject gameObject = NGUITools.AddChild(this.listTable.gameObject, this.detailBarOrg.gameObject);
            gameObject.SetActive(true);
            gameObject.name = string.Format("invited_{0}", (object) (100 + count));
            gameObject.transform.localPosition = new Vector3(0.0f, (float) (-(height >> 1) - height * count), 0.0f);
            this.inviteBarList.Add(gameObject.GetComponent<ChatRoomInviteBar>());
          }
          this.listTable.repositionNow = true;
        }
        if (data.Count <= 0)
          return;
        for (int index = 0; index < data.Count && index < num; ++index)
        {
          Hashtable hashtable = data[index] as Hashtable;
          string str = string.Empty;
          if (hashtable.Contains((object) "alliance_tag") && hashtable[(object) "alliance_tag"] != null && !string.IsNullOrEmpty(hashtable[(object) "alliance_tag"].ToString()))
            str = "(" + hashtable[(object) "alliance_tag"].ToString() + ")";
          int result1 = 0;
          if (hashtable.ContainsKey((object) "lord_title"))
            int.TryParse(hashtable[(object) "lord_title"].ToString(), out result1);
          this.inviteBarList[index].gameObject.SetActive(true);
          this.inviteBarList[index].Setup(long.Parse(hashtable[(object) "uid"].ToString()), "K" + hashtable[(object) "world_id"].ToString() + " - " + str + hashtable[(object) "name"].ToString(), hashtable[(object) "icon"].ToString(), hashtable[(object) "portrait"].ToString(), result1, this.roomID);
          this.inviteBarList[index].invitedEnable = true;
        }
        for (int count = data.Count; count < this.inviteBarList.Count; ++count)
          this.inviteBarList[count].gameObject.SetActive(false);
        this.noResultContainer.gameObject.SetActive(false);
        this.inviteExpander.ResetExpandArea(this.SearchContainer.height + (data.Count >= num ? num : data.Count) * this.detailBarOrg.GetComponent<UIWidget>().height);
        this.inviteExpander.isExpanded = true;
      }
      else
      {
        for (int index = 0; index < this.inviteBarList.Count; ++index)
          this.inviteBarList[index].gameObject.SetActive(false);
        this.noResultContainer.gameObject.SetActive(false);
        this.noResultContainer.gameObject.SetActive(true);
        this.inviteExpander.ResetExpandArea(this.SearchContainer.height + this.noResultContainer.height);
        this.inviteExpander.isExpanded = true;
      }
    }

    public void RefreshList()
    {
      this.noResultContainer.gameObject.SetActive(false);
      this.input.value = string.Empty;
      this.FreshSearchButtonState();
      for (int index = 0; index < this.inviteBarList.Count; ++index)
        this.inviteBarList[index].gameObject.SetActive(false);
      this.inviteExpander.ResetExpandArea(this.SearchContainer.height + this.noResultContainer.height);
      this.inviteExpander.isExpanded = false;
    }
  }
}
