﻿// Decompiled with JetBrains decompiler
// Type: WatchtowerUtilities
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public static class WatchtowerUtilities
{
  public static Color TRADE_COLOR = Utils.GetColorFromHex(65280U);
  public static Color REINFORCE_COLOR = Utils.GetColorFromHex(249U);
  public static Color SCOUT_COLOR = Utils.GetColorFromHex(16711680U);
  public static Color ATTACK_COLOR = Utils.GetColorFromHex(16711680U);
  public static Color RALLY_COLOR = Utils.GetColorFromHex(16711680U);
  public static Color UNKNOWN_COLOR = Utils.GetColorFromHex(8388736U);
  public static string ATTACK_ICON_NAME = "icon_Watchtower_attack";
  public static string REINFORCE_ICON_NAME = "icon_Watchtower_reinforcing";
  public static string TRADE_ICON_NAME = "icon_Watchtower_trade";
  public static List<WatchEntity> Entities = new List<WatchEntity>();
  public static Dictionary<WatchEntity, bool> IgnorMap = new Dictionary<WatchEntity, bool>();
  private static bool inited = false;
  public const string SCREEN_EFFECT_PATH = "Prefab/VFX/flash_red";
  public const string RED_SMOKE_PATH = "Prefab/VFX/fx_red_smoke";
  public const string BLUE_SMOKE_PATH = "Prefab/VFX/fx_blue_smoke";
  public const string GREEN_SMOKE_PATH = "Prefab/VFX/fx_green_smoke";

  public static event System.Action<WatchEntity> onAdded;

  public static event System.Action<WatchEntity> onRemoved;

  public static event System.Action onUpdate;

  public static void Init()
  {
    if (WatchtowerUtilities.inited)
      return;
    DBManager.inst.DB_WatchTower.onDataCreated += new System.Action<long>(WatchtowerUtilities.OnWatchTowerDataCreated);
    DBManager.inst.DB_WatchTower.onDataChanged += new System.Action<long>(WatchtowerUtilities.OnWatchTowerDataChanged);
    DBManager.inst.DB_WatchTower.onDataRemove += new System.Action<long>(WatchtowerUtilities.OnWatchTowerDataRemove);
    WatchtowerUtilities.Initialize();
    WatchtowerUtilities.inited = true;
  }

  private static void OnWatchTowerDataCreated(long eventId)
  {
    UserWatchTowerData dataById = DBManager.inst.DB_WatchTower.GetDataById(eventId);
    if (dataById == null)
      return;
    if (dataById.IsMarch)
      WatchtowerUtilities.OnMarchAdded(eventId);
    else
      WatchtowerUtilities.OnRallyAdded(eventId);
  }

  private static void OnWatchTowerDataChanged(long eventId)
  {
    UserWatchTowerData dataById = DBManager.inst.DB_WatchTower.GetDataById(eventId);
    if (dataById == null)
      return;
    if (dataById.IsMarch)
      WatchtowerUtilities.OnMarchChanged(eventId);
    else
      WatchtowerUtilities.OnRallyChanged(eventId);
  }

  private static void OnWatchTowerDataRemove(long eventId)
  {
    UserWatchTowerData dataById = DBManager.inst.DB_WatchTower.GetDataById(eventId);
    if (dataById == null)
      return;
    if (dataById.IsMarch)
      WatchtowerUtilities.OnMarchRemoved(eventId);
    else
      WatchtowerUtilities.OnRallyRemoved(eventId);
  }

  public static void Update()
  {
  }

  public static void Deinit()
  {
    if (GameEngine.IsAvailable && !GameEngine.IsShuttingDown)
    {
      DBManager.inst.DB_WatchTower.onDataCreated -= new System.Action<long>(WatchtowerUtilities.OnWatchTowerDataCreated);
      DBManager.inst.DB_WatchTower.onDataChanged -= new System.Action<long>(WatchtowerUtilities.OnWatchTowerDataChanged);
      DBManager.inst.DB_WatchTower.onDataRemove -= new System.Action<long>(WatchtowerUtilities.OnWatchTowerDataRemove);
      WatchtowerUtilities.IgnorMap.Clear();
      WatchtowerUtilities.Entities.Clear();
    }
    WatchtowerUtilities.inited = false;
  }

  private static void Initialize()
  {
    List<long> allDatas = DBManager.inst.DB_WatchTower.GetAllDatas();
    for (int index = 0; index < allDatas.Count; ++index)
    {
      long num = allDatas[index];
      if (DBManager.inst.DB_WatchTower.GetDataById(num).IsMarch)
        WatchtowerUtilities.OnMarchAdded(num);
      else
        WatchtowerUtilities.OnRallyAdded(num);
    }
  }

  public static int GetWatchtowerLevel()
  {
    return CityManager.inst.GetHighestBuildingLevelFor("watchtower");
  }

  public static bool IsAlive()
  {
    return WatchtowerUtilities.Entities.Count > 0;
  }

  private static void OnMarchAdded(long marchId)
  {
    WatchEntity key = new WatchEntity(WatchType.March, marchId);
    if (!WatchtowerUtilities.Entities.Contains(key) && !WatchtowerUtilities.IgnorMap.ContainsKey(key))
    {
      WatchtowerUtilities.Entities.Add(key);
      Vibration.Vibrate();
      WatchtowerUtilities.IgnorMap.Add(key, key.IsDefaultIgnore());
      if (WatchtowerUtilities.onAdded == null)
        return;
      WatchtowerUtilities.onAdded(key);
    }
    else
      D.warn((object) "[WatchtowerUtilites]OnMarchAdded: duplicated entity found.");
  }

  private static void OnMarchChanged(long marchId)
  {
    if (WatchtowerUtilities.onUpdate == null)
      return;
    WatchtowerUtilities.onUpdate();
  }

  private static void OnMarchRemoved(long marchId)
  {
    int count = WatchtowerUtilities.Entities.Count;
    for (int index = 0; index < count; ++index)
    {
      WatchEntity entity = WatchtowerUtilities.Entities[index];
      if (entity.m_Type == WatchType.March && entity.m_Id == marchId)
      {
        if (WatchtowerUtilities.IgnorMap.ContainsKey(entity))
          WatchtowerUtilities.IgnorMap.Remove(entity);
        WatchtowerUtilities.Entities.RemoveAt(index);
        if (WatchtowerUtilities.onRemoved == null)
          break;
        WatchtowerUtilities.onRemoved(entity);
        break;
      }
    }
  }

  public static WatchEntity GetPriorityEntity()
  {
    WatchtowerUtilities.Entities.Sort();
    for (int index = 0; index < WatchtowerUtilities.Entities.Count; ++index)
    {
      if (!WatchtowerUtilities.IgnorMap[WatchtowerUtilities.Entities[index]])
        return WatchtowerUtilities.Entities[index];
    }
    return WatchtowerUtilities.Entities[0];
  }

  private static void OnRallyAdded(long rallyId)
  {
    if (WatchtowerUtilities.GetWatchtowerLevel() < 5)
      return;
    WatchEntity key = new WatchEntity(WatchType.Rally, rallyId);
    if (!WatchtowerUtilities.Entities.Contains(key) && !WatchtowerUtilities.IgnorMap.ContainsKey(key))
    {
      WatchtowerUtilities.Entities.Add(key);
      WatchtowerUtilities.IgnorMap.Add(key, key.IsDefaultIgnore());
      if (WatchtowerUtilities.onAdded != null)
        WatchtowerUtilities.onAdded(key);
      Vibration.Vibrate();
    }
    else
      D.warn((object) "[WatchtowerUtilites]OnRallyAdded: duplicated entity found.");
  }

  private static void OnRallyChanged(long rallyId)
  {
  }

  private static void OnRallyRemoved(long rallyId)
  {
    int count = WatchtowerUtilities.Entities.Count;
    for (int index = 0; index < count; ++index)
    {
      WatchEntity entity = WatchtowerUtilities.Entities[index];
      if (entity.m_Type == WatchType.Rally && entity.m_Id == rallyId)
      {
        WatchtowerUtilities.Entities.RemoveAt(index);
        if (WatchtowerUtilities.onRemoved == null)
          break;
        WatchtowerUtilities.onRemoved(entity);
        break;
      }
    }
  }
}
