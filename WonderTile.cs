﻿// Decompiled with JetBrains decompiler
// Type: WonderTile
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WonderTile : MonoBehaviour
{
  private Vector2 normalIdInVector;
  private Vector2 coord;

  public string NormalIdInString
  {
    get
    {
      return this.NormalIdInVector.x.ToString() + "_" + this.NormalIdInVector.y.ToString();
    }
  }

  public Vector2 NormalIdInVector
  {
    get
    {
      return this.normalIdInVector;
    }
    internal set
    {
      this.normalIdInVector = value;
    }
  }

  public Vector2 Coord
  {
    get
    {
      return this.coord;
    }
    internal set
    {
      this.coord = value;
    }
  }
}
