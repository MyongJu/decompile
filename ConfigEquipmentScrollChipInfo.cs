﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentScrollChipInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ConfigEquipmentScrollChipInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "item_id")]
  public int itemID;
  [Config(Name = "level")]
  public int level;
  [Config(Name = "quality")]
  public int quality;
  [Config(Name = "combine_req_value")]
  public int combineReqValue;
  [Config(Name = "sell_wood")]
  public int sellWood;

  public string QualityImagePath
  {
    get
    {
      return "Texture/Equipment/frame_equipment_" + (object) 0;
    }
  }
}
