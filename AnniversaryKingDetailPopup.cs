﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryKingDetailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;

public class AnniversaryKingDetailPopup : Popup
{
  private AnniversaryManager.AnniversarySlotData _slotData;
  public UILabel title;
  public UIButton challengeBt;
  public UILabel playerName;
  public UILabel troopCount;
  public UILabel troopPower;
  public UITexture playerIcon;
  public int _slotIndex;
  private AnniversaryChampionMainInfo _mainInfo;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    AnniversaryKingDetailPopup.Parameter parameter = orgParam as AnniversaryKingDetailPopup.Parameter;
    this._slotData = parameter.slotData;
    this._mainInfo = parameter.mainInfo;
    this._slotIndex = parameter.slotIndex;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this._slotData != null && this._slotData.Uid < 6000000000L)
    {
      this.title.text = ScriptLocalization.GetWithPara("id_uppercase_troop_details", new Dictionary<string, string>()
      {
        {
          "0",
          this._slotData.Name
        }
      }, true);
      this.playerName.text = this._slotData.Name;
      CustomIconLoader.Instance.requestCustomIcon(this.playerIcon, UserData.GetPortraitIconPath(this._slotData.Portrait), this._slotData.Icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.playerIcon, this._slotData.LordTitleId, 1);
      this.troopCount.text = this._slotData.TroopCount.ToString();
      this.troopPower.text = Utils.FormatThousands(this._slotData.TroopPower.ToString());
    }
    else
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.playerIcon, "Texture/Hero/Portrait_Icon/player_anonymous", (System.Action<bool>) null, true, false, string.Empty);
      UILabel title = this.title;
      string str1 = ScriptLocalization.Get("id_mysterious_troops_name", true);
      this.playerName.text = str1;
      string str2 = str1;
      title.text = str2;
      List<UnitGroup.UnitRecord> unitList = this._mainInfo.Units.GetUnitList();
      int num1 = 0;
      int num2 = 0;
      for (int index = 0; index < unitList.Count; ++index)
      {
        num1 += unitList[index].value;
        Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(unitList[index].internalId);
        num2 += data.Power * unitList[index].value;
      }
      this.troopCount.text = num1.ToString();
      this.troopPower.text = Utils.FormatThousands(num2.ToString());
    }
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    BuilderFactory.Instance.Release((UIWidget) this.playerIcon);
  }

  public void OnChallenge()
  {
    this.OnCloseHandler();
    UIManager.inst.OpenDlg("Anniversary/AnniversarySendMarchDlg", (UI.Dialog.DialogParameter) new AnniversarySendMarchDlg.Parameter()
    {
      isChangellen = true,
      slotIndex = this._slotIndex
    }, true, true, true);
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private bool CanChallenge()
  {
    if (AnniversaryManager.Instance.EndTime < NetServerTime.inst.ServerTimestamp)
      return false;
    return PlayerData.inst.CityData.mStronghold.mLevel >= 6;
  }

  public class Parameter : Popup.PopupParameter
  {
    public AnniversaryManager.AnniversarySlotData slotData;
    public AnniversaryChampionMainInfo mainInfo;
    public int slotIndex;
  }
}
