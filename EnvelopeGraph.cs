﻿// Decompiled with JetBrains decompiler
// Type: EnvelopeGraph
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

internal class EnvelopeGraph
{
  private ArrayList mItems = new ArrayList();
  private CRSpline mSpline;

  public void AddItem(Vector2 item)
  {
    this.mItems.Add((object) new V2(item.x, item.y));
  }

  public void SetSpline(CRSpline spline)
  {
    this.mSpline = spline;
  }

  public float GetValue(float time)
  {
    for (int index = 0; index < this.mItems.Count - 1; ++index)
    {
      V2 mItem1 = this.mItems[index] as V2;
      V2 mItem2 = this.mItems[index + 1] as V2;
      if ((double) time >= (double) mItem1.x && (double) time <= (double) mItem2.x)
      {
        float t = Utils.GraphLerp(mItem1.x, mItem2.x, mItem1.y, mItem2.y, time);
        if (this.mSpline != null)
          t = this.mSpline.GetInterpolatedSplinePoint(t).x;
        return t;
      }
    }
    return 0.0f;
  }
}
