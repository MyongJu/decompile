﻿// Decompiled with JetBrains decompiler
// Type: CityMapPrefabs
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class CityMapPrefabs : MonoBehaviour
{
  public float farsightfactor = 1f;
  public float urbanScale = 1f;
  public float focusBuildingTime = 0.3f;
  public GameObject mHouseBuilding;
  public GameObject mFarmBuilding;
  public GameObject mLumberMillBuilding;
  public GameObject mMineBuilding;
  public GameObject mStrongholdBuilding;
  public GameObject mWallBuilding;
  public GameObject mParadeGround;
  public GameObject mBarracksBuilding;
  public GameObject mRangeBuilding;
  public GameObject mStablesBuilding;
  public GameObject mSanctumBuilding;
  public GameObject mWorkshopBuilding;
  public GameObject mForgeBuilding;
  public GameObject mMarketplaceBuilding;
  public GameObject mStorehouseBuilding;
  public GameObject mUniversityBuilding;
  public GameObject mHospitalBuilding;
  public GameObject mEmbassyBuilding;
  public GameObject mWarRallyBuilding;
  public GameObject mWatchTowerBuilding;
  public GameObject mDragonLairBuilding;
  public GameObject mFortressBuilding;
  public GameObject mMilitaryTentBuildng;
  public GameObject mWishWellBuilding;
  public GameObject mDragonKnight;
  public GameObject mBlockerSprite;
  public GameObject mUrbanPlots;
  public GameObject mRuralPlots;
  public UIWidget boundary;
  private CityCamera _cityCamera;
  public GameObject farsight;
  public GameObject MapRoot;
  public GameObject BuildingRoot;
  private Vector3 oriPos;

  public event CityMapPrefabs.OnCityMapClickedEvent onCityMapClicked;

  public void Init()
  {
    if ((UnityEngine.Object) UIManager.inst != (UnityEngine.Object) null && (UnityEngine.Object) UIManager.inst.cityCamera != (UnityEngine.Object) null)
    {
      this._cityCamera = UIManager.inst.cityCamera;
      if ((bool) ((UnityEngine.Object) this._cityCamera))
      {
        this._cityCamera.UseBoundary = true;
        Rect rect = new Rect();
        Vector3[] worldCorners = this.boundary.worldCorners;
        rect.Set(worldCorners[0].x, worldCorners[0].y, worldCorners[2].x - worldCorners[0].x, worldCorners[2].y - worldCorners[0].y);
        this._cityCamera.Boundary = rect;
        this._cityCamera.onCityCameraMove += new System.Action<Vector2>(this.FarSightMove);
      }
    }
    this.oriPos = this.farsight.transform.localPosition;
  }

  public void Dispose()
  {
    if ((UnityEngine.Object) this._cityCamera != (UnityEngine.Object) null)
    {
      this._cityCamera.UseBoundary = false;
      this._cityCamera.onCityCameraMove -= new System.Action<Vector2>(this.FarSightMove);
    }
    this.farsight.transform.localPosition = this.oriPos;
  }

  private void FarSightMove(Vector2 obj)
  {
    if ((UnityEngine.Object) this.farsight == (UnityEngine.Object) null)
      return;
    Vector3 localPosition = this.farsight.transform.localPosition;
    Vector2 vector2 = obj * this.farsightfactor;
    this.farsight.transform.localPosition = localPosition + new Vector3(vector2.x, vector2.y);
  }

  public void OnCityMapClicked()
  {
    if (this.onCityMapClicked == null)
      return;
    this.onCityMapClicked();
  }

  public RuralBlock GetRuralBlock(int id)
  {
    foreach (RuralBlock componentsInChild in this.GetComponentsInChildren<RuralBlock>())
    {
      if (componentsInChild.id == id.ToString())
        return componentsInChild;
    }
    return (RuralBlock) null;
  }

  public delegate void OnCityMapClickedEvent();
}
