﻿// Decompiled with JetBrains decompiler
// Type: BlockReference
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.IO;

public struct BlockReference
{
  public short x;
  public short y;
  public short xRef;
  public short yRef;

  public void Read(BinaryReader reader)
  {
    this.x = reader.ReadInt16();
    this.y = reader.ReadInt16();
    this.xRef = reader.ReadInt16();
    this.yRef = reader.ReadInt16();
  }

  public void Write(BinaryWriter writer)
  {
    writer.Write(this.x);
    writer.Write(this.y);
    writer.Write(this.xRef);
    writer.Write(this.yRef);
  }
}
