﻿// Decompiled with JetBrains decompiler
// Type: PitExploreDetailsDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PitExploreDetailsDlg : UI.Dialog
{
  private Dictionary<string, string> _timeParam = new Dictionary<string, string>();
  [SerializeField]
  private UITexture _textureTopPitBanner;
  [SerializeField]
  private UILabel _labelTopStatus;
  [SerializeField]
  private UILabel _labelBottomStatus;
  [SerializeField]
  private UILabel _buttonLabelPitSignUp;
  [SerializeField]
  private UILabel _labelTotalCollect;
  [SerializeField]
  private RedDotTipItem _scoreRewardTip;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
    this.OnSecondEvent(0);
    this.AddEventHandler();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
  }

  public void OnViewRankBtnPressed()
  {
    PitExplorePayload.Instance.ShowPitRankHistory();
  }

  public void OnViewRewardsBtnPressed()
  {
    UIManager.inst.OpenPopup("Pit/PitAllRewardPopup", (Popup.PopupParameter) null);
  }

  public void OnPitSignUpBtnPressed()
  {
    if (!PitExplorePayload.Instance.ActivityData.IsActivityOpened && (PitExplorePayload.Instance.CurrentActivityRound <= 0 || PitExplorePayload.Instance.CurrentActivityRound != PitExplorePayload.Instance.ActivityData.CurSignupData.SignupRound))
      UIManager.inst.ShowConfirmationBox(Utils.XLAT("id_uppercase_notice"), Utils.XLAT("event_fire_kingdom_sudden_closure_description"), Utils.XLAT("id_uppercase_okay"), string.Empty, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) null, (System.Action) null, (System.Action) null);
    else if (PitExplorePayload.Instance.CurrentActivitySigned)
    {
      if (PitExplorePayload.Instance.CurrentActivityRound > PitExplorePayload.Instance.ActivityData.CurSignupData.SignupRound)
        UIManager.inst.toast.Show(Utils.XLAT("toast_event_fire_kingdom_already_entered"), (System.Action) null, 4f, true);
      else
        PitExplorePayload.Instance.EnterPitExplore();
    }
    else
      PitExplorePayload.Instance.ShowPitSignupScene((System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.UpdateUI();
      }));
  }

  private void UpdateSignupButtonStatus()
  {
    UIButton componentInParent = this._buttonLabelPitSignUp.transform.GetComponentInParent<UIButton>();
    if (!(bool) ((UnityEngine.Object) componentInParent))
      return;
    if (PitExplorePayload.Instance.CurrentActivitySigned)
    {
      if (PitExplorePayload.Instance.CurrentActivityRound > PitExplorePayload.Instance.ActivityData.CurSignupData.SignupRound)
      {
        componentInParent.isEnabled = true;
        this._buttonLabelPitSignUp.text = Utils.XLAT("event_fire_kingdom_register_button");
      }
      else
      {
        componentInParent.isEnabled = PitExplorePayload.Instance.CurrentActivityRound == PitExplorePayload.Instance.ActivityData.CurSignupData.SignupRound;
        this._buttonLabelPitSignUp.text = Utils.XLAT("event_fire_kingdom_enter_button");
      }
    }
    else
    {
      componentInParent.isEnabled = PitExplorePayload.Instance.CanSignup;
      this._buttonLabelPitSignUp.text = Utils.XLAT("event_fire_kingdom_register_button");
    }
  }

  private void UpdateUI()
  {
    this.UpdateSignupButtonStatus();
    BuilderFactory.Instance.HandyBuild((UIWidget) this._textureTopPitBanner, "Texture/GUI_Textures/pit_explore_banner", (System.Action<bool>) null, true, true, string.Empty);
    this._labelTotalCollect.text = PitExplorePayload.Instance.ActivityData.CurrentScore.ToString();
    this.UpdateTipCount();
  }

  private void UpdateTipCount()
  {
    this._scoreRewardTip.SetTipCount(PitExplorePayload.Instance.ActivityData.GetCanClaimScoreRewardCount());
  }

  private void OnSecondEvent(int time)
  {
    this._timeParam.Clear();
    this._timeParam.Add("0", Utils.FormatTime(PitExplorePayload.Instance.NextRoundStartTime, true, false, true));
    this._labelTopStatus.text = ScriptLocalization.GetWithPara("event_fire_kingdom_next_round", this._timeParam, true);
    this._timeParam.Clear();
    if (NetServerTime.inst.ServerTimestamp < PitExplorePayload.Instance.ActivityData.RegStartTime)
    {
      string str = ((float) PitExplorePayload.Instance.ActivityData.Duration / 3600f).ToString("f1");
      string[] strArray = str.Split('.');
      if (strArray != null && strArray.Length == 2 && strArray[1] == "0")
        str = strArray[0];
      this._timeParam.Add("0", str);
      this._labelBottomStatus.text = ScriptLocalization.GetWithPara("event_fire_kingdom_enrol_time_tip", this._timeParam, true);
    }
    else if (PitExplorePayload.Instance.CurrentActivityRound < PitExplorePayload.Instance.ActivityData.CurSignupData.SignupRound)
    {
      this._timeParam.Add("0", Utils.FormatTime(PitExplorePayload.Instance.NextRoundStartTime, true, false, true));
      this._labelBottomStatus.text = ScriptLocalization.GetWithPara("event_fire_kingdom_countdown_num", this._timeParam, true);
    }
    else
    {
      this._timeParam.Add("0", Utils.FormatTime(PitExplorePayload.Instance.CurrentRoundEndTime, true, false, true));
      this._labelBottomStatus.text = ScriptLocalization.GetWithPara("event_fire_kingdom_closes_in", this._timeParam, true);
    }
    NGUITools.SetActive(this._labelBottomStatus.transform.parent.gameObject, !(NetServerTime.inst.ServerTimestamp >= PitExplorePayload.Instance.ActivityData.RegStartTime & NetServerTime.inst.ServerTimestamp < PitExplorePayload.Instance.ActivityData.StartTime) || PitExplorePayload.Instance.ActivityData.CurSignupData.SignupRound > 0);
    this.UpdateSignupButtonStatus();
    this.UpdateTipCount();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void RemoveEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  public void OnButtonViewRewardClicked()
  {
    UIManager.inst.OpenPopup("Pit/PitScoreRewardPopup", (Popup.PopupParameter) null);
  }
}
