﻿// Decompiled with JetBrains decompiler
// Type: ResourceWithUnCollectComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class ResourceWithUnCollectComponent : ComponentRenderBase
{
  public UILabel food;
  public UILabel wood;
  public UILabel ore;
  public UILabel silver;
  public UILabel food1;
  public UILabel wood1;
  public UILabel ore1;
  public UILabel silver1;

  public override void Init()
  {
    base.Init();
    ResourceWithUnCollectComponentData data = this.data as ResourceWithUnCollectComponentData;
    if (data == null)
      this.gameObject.SetActive(false);
    Dictionary<string, string> collect = data.collect;
    Dictionary<string, string> unCollect = data.un_collect;
    this.food.text = collect == null || !collect.ContainsKey("food") ? string.Empty : Utils.FormatThousands(collect["food"]);
    this.wood.text = collect == null || !collect.ContainsKey("wood") ? string.Empty : Utils.FormatThousands(collect["wood"]);
    this.ore.text = collect == null || !collect.ContainsKey("ore") ? string.Empty : Utils.FormatThousands(collect["ore"]);
    this.silver.text = collect == null || !collect.ContainsKey("silver") ? string.Empty : Utils.FormatThousands(collect["silver"]);
    this.food1.text = unCollect == null || !unCollect.ContainsKey("food") ? string.Empty : "+" + Utils.FormatThousands(unCollect["food"]);
    this.wood1.text = unCollect == null || !unCollect.ContainsKey("wood") ? string.Empty : "+" + Utils.FormatThousands(unCollect["wood"]);
    this.ore1.text = unCollect == null || !unCollect.ContainsKey("ore") ? string.Empty : "+" + Utils.FormatThousands(unCollect["ore"]);
    if (unCollect != null && unCollect.ContainsKey("silver"))
      this.silver1.text = "+" + Utils.FormatThousands(unCollect["silver"]);
    else
      this.silver1.text = string.Empty;
  }
}
