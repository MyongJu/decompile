﻿// Decompiled with JetBrains decompiler
// Type: ConfigDiceMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDiceMain
{
  private List<DiceMainInfo> _diceMainInfoList = new List<DiceMainInfo>();
  private Dictionary<string, DiceMainInfo> _datas;
  private Dictionary<int, DiceMainInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<DiceMainInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, DiceMainInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._diceMainInfoList.Add(enumerator.Current);
    }
  }

  public DiceMainInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (DiceMainInfo) null;
  }

  public DiceMainInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (DiceMainInfo) null;
  }

  public List<DiceMainInfo> GetDiceMainList()
  {
    return this._diceMainInfoList;
  }
}
