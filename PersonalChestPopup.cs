﻿// Decompiled with JetBrains decompiler
// Type: PersonalChestPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PersonalChestPopup : Popup
{
  private List<ChestReceiveDetailItem> _AllChestReceiveDetailItem = new List<ChestReceiveDetailItem>();
  [SerializeField]
  private UITexture _TextureSenderIcon;
  [SerializeField]
  private UILabel _LabelSenderName;
  [SerializeField]
  private UILabel _LabelGetCondition;
  [SerializeField]
  private UILabel _LabelGetCount;
  [SerializeField]
  private UILabel _LabelProgress;
  [SerializeField]
  private UILabel _LabelLeftCount;
  [SerializeField]
  private UIScrollView _ScrollView;
  [SerializeField]
  private UITable _TableContainer;
  [SerializeField]
  private ChestReceiveDetailItem _ChestReceiveDetailItemTemplate;
  [SerializeField]
  private GameObject _RootClaimed;
  [SerializeField]
  private GameObject _RootHavntClaimed;
  [SerializeField]
  private GameObject _rootOpenEffect;
  private AllianceTreasuryVaultData _sendedChestData;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    PersonalChestPopup.Parameter parameter = orgParam as PersonalChestPopup.Parameter;
    this._sendedChestData = parameter.sendedChestData;
    this._ChestReceiveDetailItemTemplate.gameObject.SetActive(false);
    this.UpdateUI();
    this._rootOpenEffect.SetActive(parameter.firstTime);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.OnClose(orgParam);
  }

  private void UpdateUI()
  {
    AlliancePersonalChestInfo data1 = ConfigManager.inst.DB_AlliancePersonalChest.GetData(this._sendedChestData.personalConfigId);
    if (data1 != null)
      this._LabelGetCondition.text = data1.LocalDescription;
    AllianceTreasuryVaultMainInfo data2 = ConfigManager.inst.DB_AllianceTreasuryVaultMain.GetData(this._sendedChestData.configId);
    if (data2 == null)
      return;
    string str = ScriptLocalization.Get("id_anonymous", true);
    if (this._sendedChestData.senderUid == PlayerData.inst.uid)
    {
      CustomIconLoader.Instance.requestCustomIcon(this._TextureSenderIcon, PlayerData.inst.userData.PortraitIconPath, PlayerData.inst.userData.Icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this._TextureSenderIcon, PlayerData.inst.userData.LordTitle, 12);
      str = PlayerData.inst.userData.userName;
    }
    else if (!string.IsNullOrEmpty(this._sendedChestData.senderName))
    {
      CustomIconLoader.Instance.requestCustomIcon(this._TextureSenderIcon, UserData.BuildPortraitPath(this._sendedChestData.senderPortrait), this._sendedChestData.senderIcon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this._TextureSenderIcon, this._sendedChestData.senderLordTitle, 12);
      str = this._sendedChestData.senderName;
    }
    else
      BuilderFactory.Instance.HandyBuild((UIWidget) this._TextureSenderIcon, "Texture/Hero/Portrait_Icon/player_anonymous", (System.Action<bool>) null, true, false, string.Empty);
    bool flag = this._sendedChestData.HaveClaimed(PlayerData.inst.uid);
    this._RootClaimed.SetActive(flag);
    this._RootHavntClaimed.SetActive(!flag);
    this._LabelSenderName.text = ScriptLocalization.GetWithPara("alliance_giftbox_sender_name", new Dictionary<string, string>()
    {
      {
        "0",
        str
      }
    }, true);
    this._LabelProgress.text = ScriptLocalization.GetWithPara("alliance_giftbox_collected_num", new Dictionary<string, string>()
    {
      {
        "0",
        this._sendedChestData.allClaimedInfo.Count.ToString()
      },
      {
        "1",
        data2.Num.ToString()
      }
    }, true);
    int num = data2.Gold - this._sendedChestData.AlreadySendGold;
    if (num <= 0)
      num = 0;
    this._LabelLeftCount.text = ScriptLocalization.Get("alliance_giftbox_gold_balance", true) + (object) num;
    this.DestoryAllChestReceiveDetailItem();
    using (List<AllianceTreasuryVaultData.ClaimedInfo>.Enumerator enumerator = this._sendedChestData.allClaimedInfo.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceTreasuryVaultData.ClaimedInfo current = enumerator.Current;
        if (current.uid == PlayerData.inst.uid)
          this._LabelGetCount.text = current.gold.ToString();
        string userName = current.userName;
        if (current.uid == PlayerData.inst.uid)
          userName = PlayerData.inst.userData.userName;
        else if (this._sendedChestData.senderUid == current.uid && string.IsNullOrEmpty(this._sendedChestData.senderName))
          userName = ScriptLocalization.Get("id_anonymous", true);
        this.CreateChestReceiveDetailItem().SetData(current.uid, userName, current.gold, current.isLuckiest);
      }
    }
    this._TableContainer.Reposition();
    this._ScrollView.ResetPosition();
  }

  private ChestReceiveDetailItem CreateChestReceiveDetailItem()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._ChestReceiveDetailItemTemplate.gameObject);
    gameObject.transform.SetParent(this._TableContainer.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    ChestReceiveDetailItem component = gameObject.GetComponent<ChestReceiveDetailItem>();
    this._AllChestReceiveDetailItem.Add(component);
    return component;
  }

  private void DestoryAllChestReceiveDetailItem()
  {
    using (List<ChestReceiveDetailItem>.Enumerator enumerator = this._AllChestReceiveDetailItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this._AllChestReceiveDetailItem.Clear();
  }

  public void OnButtonCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public AllianceTreasuryVaultData sendedChestData;
    public bool firstTime;
  }
}
