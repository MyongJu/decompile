﻿// Decompiled with JetBrains decompiler
// Type: ParadeGroundTroopsPercentProgress
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ParadeGroundTroopsPercentProgress : MonoBehaviour
{
  public float _ANIM_DURING_TIME = 1f;
  public UIProgressBar progress;
  public UILabel percentText;
  private float _startPercent;
  private float _targetPercent;
  private float _deltaPercent;
  private float _startTime;
  private bool _startAnim;
  public AnimationCurve curve;

  public void Setup(float maxPercent, float troopPercent, bool isTrap = false)
  {
    this._startPercent = this.progress.value;
    this._targetPercent = (double) maxPercent == 0.0 ? 0.0f : troopPercent / maxPercent;
    this._deltaPercent = this._targetPercent - this._startPercent;
    this._startTime = Time.time;
    this._startAnim = true;
    if (!isTrap)
      this.percentText.text = string.Format("{0}%", (object) (int) ((double) troopPercent * 100.0 + 9.99999974737875E-05));
    else
      this.percentText.text = string.Format("{0}%", (object) (float) ((double) (int) ((double) troopPercent * 1000.0 + 9.99999974737875E-05) / 10.0));
  }

  public void Update()
  {
    if (!this._startAnim)
      return;
    if ((double) Time.time - (double) this._startTime > (double) this._ANIM_DURING_TIME)
    {
      this.progress.value = this._targetPercent;
      this._startAnim = false;
    }
    else
      this.progress.value = this._startPercent + this._deltaPercent * this.curve.Evaluate((Time.time - this._startTime) / this._ANIM_DURING_TIME);
  }
}
