﻿// Decompiled with JetBrains decompiler
// Type: HttpPostProcessor
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class HttpPostProcessor : MonoBehaviour
{
  private string _url;
  private Hashtable _postData;
  private Hashtable _userData;
  private HttpPostProcessor.PostResultCallback _callback;

  public void DoPost(string url, Hashtable postData, Hashtable userData, HttpPostProcessor.PostResultCallback callback)
  {
    this._url = url;
    this._postData = postData;
    this._userData = userData;
    this._callback = callback;
    this.StartCoroutine(this.PostCoroutine());
  }

  [DebuggerHidden]
  public IEnumerator PostCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new HttpPostProcessor.\u003CPostCoroutine\u003Ec__IteratorB()
    {
      \u003C\u003Ef__this = this
    };
  }

  public delegate void PostResultCallback(bool result, Hashtable data, Hashtable userData);
}
