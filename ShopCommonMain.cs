﻿// Decompiled with JetBrains decompiler
// Type: ShopCommonMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class ShopCommonMain
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "group_id")]
  public int group_id;
  [Config(Name = "item_id")]
  public int item_id;
  [Config(Name = "item_value")]
  public int item_value;
  [Config(Name = "category_id")]
  public int category_id;
  [Config(Name = "priority")]
  public int priority;
  [Config(Name = "cost_item_id")]
  public int req_item_id;
  [Config(Name = "cost_value")]
  public int req_value;
  [Config(Name = "formula")]
  public string formula;
  [Config(Name = "refresh_time")]
  public int refresh_time;
  [Config(Name = "refresh_num")]
  public int refresh_num;
  public string conditionKey;
  public string conditionValue;

  public void UpdateCondition()
  {
    string[] strArray = this.formula.Split('=');
    this.conditionKey = strArray[0].Trim();
    this.conditionValue = strArray[1].Trim();
  }

  public bool MeetCondition
  {
    get
    {
      string conditionKey = this.conditionKey;
      if (conditionKey != null)
      {
        if (ShopCommonMain.\u003C\u003Ef__switch\u0024map47 == null)
          ShopCommonMain.\u003C\u003Ef__switch\u0024map47 = new Dictionary<string, int>(3)
          {
            {
              "dk_dungeon_layer",
              0
            },
            {
              "dk_level",
              1
            },
            {
              "magic_tower_layer",
              2
            }
          };
        int num;
        if (ShopCommonMain.\u003C\u003Ef__switch\u0024map47.TryGetValue(conditionKey, out num))
        {
          switch (num)
          {
            case 0:
              int result1;
              int.TryParse(this.conditionValue, out result1);
              if (DragonKnightUtils.MaxDungeonLevel >= result1)
                return true;
              break;
            case 1:
              int result2;
              int.TryParse(this.conditionValue, out result2);
              if (DragonKnightUtils.GetLevel() >= result2)
                return true;
              break;
            case 2:
              int result3;
              int.TryParse(this.conditionValue, out result3);
              if (ShopCommonPayload.Instance.MaxMerlinTowerLevel >= result3)
                return true;
              break;
          }
        }
      }
      return false;
    }
  }

  public string LocCondition
  {
    get
    {
      string conditionKey = this.conditionKey;
      if (conditionKey != null)
      {
        if (ShopCommonMain.\u003C\u003Ef__switch\u0024map48 == null)
          ShopCommonMain.\u003C\u003Ef__switch\u0024map48 = new Dictionary<string, int>(3)
          {
            {
              "dk_dungeon_layer",
              0
            },
            {
              "dk_level",
              1
            },
            {
              "magic_tower_layer",
              2
            }
          };
        int num;
        if (ShopCommonMain.\u003C\u003Ef__switch\u0024map48.TryGetValue(conditionKey, out num))
        {
          switch (num)
          {
            case 0:
              return ScriptLocalization.GetWithPara("dragon_knight_store_item_restriction_status", new Dictionary<string, string>()
              {
                {
                  "0",
                  this.conditionValue
                }
              }, 1 != 0);
            case 1:
              return ScriptLocalization.GetWithPara("dragon_knight_store_item_restriction_dk_level", new Dictionary<string, string>()
              {
                {
                  "0",
                  this.conditionValue
                }
              }, 1 != 0);
            case 2:
              return ScriptLocalization.GetWithPara("tower_shop_floors_cleared_status", new Dictionary<string, string>()
              {
                {
                  "0",
                  this.conditionValue
                }
              }, 1 != 0);
          }
        }
      }
      return string.Empty;
    }
  }
}
