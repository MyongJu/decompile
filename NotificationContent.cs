﻿// Decompiled with JetBrains decompiler
// Type: NotificationContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class NotificationContent : MonoBehaviour
{
  public UISprite allianceOnOff;
  public UISprite battleOnOff;
  public UISprite constructOnOff;
  public UISprite trainOnOff;
  public UISprite researchOnOff;
  public UISprite reinforceOnOff;
  public UISprite tradeOnOff;

  public void OnAllianceSliderClicked()
  {
    if (this.allianceOnOff.gameObject.activeSelf)
      this.allianceOnOff.gameObject.SetActive(false);
    else
      this.allianceOnOff.gameObject.SetActive(true);
  }

  public void OnBattleSliderClicked()
  {
    if (this.battleOnOff.gameObject.activeSelf)
      this.battleOnOff.gameObject.SetActive(false);
    else
      this.battleOnOff.gameObject.SetActive(true);
  }

  public void OnConstructSliderClicked()
  {
    if (this.constructOnOff.gameObject.activeSelf)
      this.constructOnOff.gameObject.SetActive(false);
    else
      this.constructOnOff.gameObject.SetActive(true);
  }

  public void OnTrainSliderClicked()
  {
    if (this.trainOnOff.gameObject.activeSelf)
      this.trainOnOff.gameObject.SetActive(false);
    else
      this.trainOnOff.gameObject.SetActive(true);
  }

  public void OnResearchSliderClicked()
  {
    if (this.researchOnOff.gameObject.activeSelf)
      this.researchOnOff.gameObject.SetActive(false);
    else
      this.researchOnOff.gameObject.SetActive(true);
  }

  public void OnReinforceSliderClicked()
  {
    if (this.reinforceOnOff.gameObject.activeSelf)
      this.reinforceOnOff.gameObject.SetActive(false);
    else
      this.reinforceOnOff.gameObject.SetActive(true);
  }

  public void OnTradeSliderClicked()
  {
    if (this.tradeOnOff.gameObject.activeSelf)
      this.tradeOnOff.gameObject.SetActive(false);
    else
      this.tradeOnOff.gameObject.SetActive(true);
  }
}
