﻿// Decompiled with JetBrains decompiler
// Type: JoinRoomBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using UnityEngine;

public class JoinRoomBar : MonoBehaviour
{
  private bool _isChecked;
  public System.Action<long> onJoinRoomBarClicked;
  [SerializeField]
  private JoinRoomBar.Panel panel;

  public bool isChecked
  {
    get
    {
      return this._isChecked;
    }
    set
    {
      this._isChecked = value;
      this.panel.checkBox.gameObject.SetActive(this._isChecked);
    }
  }

  public long roomId { get; private set; }

  public void Show(object orgData)
  {
    Hashtable data;
    BaseData.CheckAndParseOrgData(orgData, out data);
    if (data != null)
    {
      this.roomId = long.Parse(data[(object) "entityId"].ToString());
      this.panel.roomName.text = data[(object) "name"].ToString();
      if (data.ContainsKey((object) "memberCount"))
        this.panel.memberCount.text = string.Format("{0}/{1}", (object) data[(object) "memberCount"].ToString(), (object) ConfigManager.inst.DB_GameConfig.GetData("chat_room_max_number").ValueInt);
    }
    this.isChecked = false;
  }

  public void OnJoinRoomBarClicked()
  {
    if (this.onJoinRoomBarClicked == null)
      return;
    this.onJoinRoomBarClicked(this.roomId);
  }

  [Serializable]
  protected class Panel
  {
    public UILabel roomName;
    public Transform checkBox;
    public UILabel memberCount;
  }
}
