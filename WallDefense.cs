﻿// Decompiled with JetBrains decompiler
// Type: WallDefense
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using UI;
using UnityEngine;

public class WallDefense : UI.Dialog
{
  public float ZoomFocusDis = 400f;
  public UIWidget AlignTarget;
  public UIButton m_Extinguish;
  public UIButton m_IncreaseDefense;
  public GameObject m_CoolDown;
  public UISlider m_Slider;
  public UILabel m_DefenseLabel;
  public UILabel m_TipLabel;
  public UILabel m_BurnLabel;
  public UILabel m_IncreaseLabel;
  public UILabel m_CoolDownTimer;
  public UILabel m_IncreaseValue;
  public GameObject m_BottomNode;
  public UISprite m_SliderBar;
  public TweenAlpha m_TweenAlpha;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    CitadelSystem.inst.FocusConstructionBuilding(CitadelSystem.inst.GetBuildingByType("walls").gameObject, this.AlignTarget, this.ZoomFocusDis);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnExtinguish()
  {
    CityData playerCityData = PlayerData.inst.playerCityData;
    Hashtable postData = new Hashtable();
    postData[(object) "city_id"] = (object) playerCityData.cityId;
    MessageHub.inst.GetPortByAction("City:outfire").SendRequest(postData, (System.Action<bool, object>) null, true);
  }

  public void OnIncrease()
  {
    CityData playerCityData = PlayerData.inst.playerCityData;
    Hashtable postData = new Hashtable();
    postData[(object) "city_id"] = (object) playerCityData.cityId;
    MessageHub.inst.GetPortByAction("City:addCityDefenseValue").SendRequest(postData, (System.Action<bool, object>) null, true);
  }

  private void UpdateUI()
  {
    this.m_IncreaseValue.text = "+" + ConfigManager.inst.DB_GameConfig.GetData("increase_city_defense_value").ValueString;
    JobHandle singleJobByClass = JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_WALL_ONFIRE);
    CityData playerCityData = PlayerData.inst.playerCityData;
    int num1 = singleJobByClass == null || singleJobByClass.IsFinished() ? NetServerTime.inst.ServerTimestamp : singleJobByClass.EndTime();
    int num2 = singleJobByClass == null || singleJobByClass.IsFinished() ? 0 : NetServerTime.inst.ServerTimestamp - singleJobByClass.StartTime();
    double burningSpeedInSecond = AllianceBuildUtils.BurningSpeedInSecond;
    double val1 = (double) playerCityData.defenseValue - burningSpeedInSecond * (double) num2;
    double wallDefenceLimit = Utils.GetWallDefenceLimit();
    bool flag1 = val1 < wallDefenceLimit;
    int valueInt = ConfigManager.inst.DB_GameConfig.GetData("increase_city_defense_period").ValueInt;
    int num3 = NetServerTime.inst.ServerTimestamp - playerCityData.lastCityDefenseTime;
    bool flag2 = num3 >= valueInt;
    int time = num1 - NetServerTime.inst.ServerTimestamp;
    bool flag3 = time > 0;
    this.m_BottomNode.SetActive(flag3 || flag1);
    if (flag3 || flag1)
    {
      this.m_SliderBar.color = Color.red;
      this.m_TweenAlpha.enabled = true;
    }
    else
    {
      this.m_SliderBar.color = new Color(0.2196078f, 0.7333333f, 0.9882353f);
      this.m_TweenAlpha.enabled = false;
    }
    if (flag3)
    {
      val1 = Math.Max(0.0, Math.Min(val1, wallDefenceLimit));
      this.m_TipLabel.text = ScriptLocalization.Get("walls_defense_status_burning", true);
      this.m_TipLabel.color = Color.red;
      this.m_BurnLabel.text = string.Format(ScriptLocalization.Get("walls_defense_burning_description", true), (object) Utils.FormatTime(time, false, false, true));
      this.m_IncreaseLabel.text = string.Format(ScriptLocalization.Get("walls_defense_increase_defense_loss_description", true), (object) ((int) (burningSpeedInSecond * 60.0 + 0.5)).ToString());
    }
    else if (flag1)
    {
      this.m_TipLabel.text = ScriptLocalization.Get("walls_defense_status_repair", true);
      this.m_TipLabel.color = Color.yellow;
      this.m_BurnLabel.text = ScriptLocalization.Get("walls_defense_not_burning_description", true);
      this.m_IncreaseLabel.text = ScriptLocalization.Get("walls_defense_increase_defense_description", true);
    }
    else
    {
      this.m_TipLabel.text = ScriptLocalization.Get("walls_defense_status_intact", true);
      this.m_TipLabel.color = Color.green;
      this.m_BurnLabel.text = ScriptLocalization.Get("walls_defense_not_burning_description", true);
      this.m_IncreaseLabel.text = ScriptLocalization.Get("walls_defense_increase_defense_description", true);
    }
    this.m_Extinguish.isEnabled = flag3;
    this.m_IncreaseDefense.gameObject.SetActive(!flag1 || flag2);
    this.m_IncreaseDefense.isEnabled = flag1;
    this.m_CoolDown.SetActive(flag1 && !flag2);
    this.m_CoolDownTimer.text = Utils.FormatTime(valueInt - num3, false, false, true);
    this.m_Slider.value = (float) (val1 / wallDefenceLimit);
    this.m_DefenseLabel.text = string.Format(ScriptLocalization.Get("id_uppercase_city_defense", true) + ": {0}/{1}", (object) Utils.FormatThousands(((int) val1).ToString()), (object) Utils.FormatThousands(((int) wallDefenceLimit).ToString()));
  }

  private void Update()
  {
    this.UpdateUI();
  }
}
