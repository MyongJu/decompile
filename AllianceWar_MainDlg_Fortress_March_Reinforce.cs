﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_Fortress_March_Reinforce
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class AllianceWar_MainDlg_Fortress_March_Reinforce : AllianceWar_MainDlg_March_Slot
{
  public override void Setup(long marchId)
  {
    MarchData marchData = DBManager.inst.DB_March.Get(marchId);
    if (marchData == null)
      return;
    this.data.marchId = marchId;
    this.SetLeftItems(marchData, DBManager.inst.DB_User.Get(marchData.ownerUid), marchData.ownerLocation);
    this.SetRightItems(marchData, DBManager.inst.DB_User.Get(marchData.targetUid), marchData.targetLocation);
    this.OnSecond(NetServerTime.inst.ServerTimestamp);
  }

  protected override void SetRightItems(MarchData marchData, UserData userData, Coordinate location)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad(string.Format("{0}{1}", (object) "Prefab/Tiles/", (object) "tiles_alliance_fortress"), (System.Type) null) as GameObject);
    if ((UnityEngine.Object) gameObject != (UnityEngine.Object) null)
    {
      gameObject.name = "fortress";
      Transform[] componentsInChildren = gameObject.GetComponentsInChildren<Transform>();
      for (int index = 0; componentsInChildren != null && index < componentsInChildren.Length; ++index)
        componentsInChildren[index].gameObject.layer = LayerMask.NameToLayer("SmallViewport");
      gameObject.transform.parent = this.panel.rightIcon.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localScale = Vector3.one;
    }
    AllianceFortressData dataByCoordinate = DBManager.inst.DB_AllianceFortress.GetDataByCoordinate(location);
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(dataByCoordinate.ConfigId);
    if (dataByCoordinate != null && !string.IsNullOrEmpty(dataByCoordinate.Name))
      this.panel.rightName.text = dataByCoordinate.Name;
    else
      this.panel.rightName.text = buildingStaticInfo.LocalName;
    this.panel.rightLocation.text = string.Format("X:{0}, Y:{1}", (object) location.X, (object) location.Y);
    this.data.rightLocation = location;
  }
}
