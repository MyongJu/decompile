﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossHistoryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceBossHistoryPopup : Popup
{
  private Dictionary<int, AllianceBossHistoryItemRenderer> itemDict = new Dictionary<int, AllianceBossHistoryItemRenderer>();
  private GameObjectPool itemPool = new GameObjectPool();
  public UILabel panelTitle;
  public UILabel panelTip;
  public UILabel noBossHistoryTip;
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.ShowBossHistoryContent();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void ShowBossHistoryContent()
  {
    this.panelTitle.text = Utils.XLAT("alliance_portal_history_title");
    NGUITools.SetActive(this.panelTip.gameObject, false);
    NGUITools.SetActive(this.noBossHistoryTip.gameObject, false);
    this.ClearData();
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    postData.Add((object) "alliance_id", (object) PlayerData.inst.allianceId);
    List<AllianceBossHistoryPopup.BossHistoryItem> bossHistoryList = new List<AllianceBossHistoryPopup.BossHistoryItem>();
    MessageHub.inst.GetPortByAction("alliance:loadAllianceBossHistory").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      Hashtable hashtable = data as Hashtable;
      if (hashtable != null)
      {
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          string str = key.ToString();
          Hashtable inData = hashtable[(object) str] as Hashtable;
          if (inData != null)
          {
            int outData = 0;
            string empty1 = string.Empty;
            string empty2 = string.Empty;
            DatabaseTools.UpdateData(inData, "ts", ref outData);
            DatabaseTools.UpdateData(inData, "user_name", ref empty1);
            DatabaseTools.UpdateData(inData, "boss_name", ref empty2);
            if (!string.IsNullOrEmpty(empty2))
              bossHistoryList.Add(new AllianceBossHistoryPopup.BossHistoryItem(outData, empty1, empty2));
          }
        }
      }
      bossHistoryList.Sort((Comparison<AllianceBossHistoryPopup.BossHistoryItem>) ((a, b) => b.time.CompareTo(a.time)));
      if (bossHistoryList.Count > 0)
      {
        for (int key = 0; key < bossHistoryList.Count; ++key)
        {
          AllianceBossHistoryItemRenderer itemRenderer = this.CreateItemRenderer(bossHistoryList[key].time, bossHistoryList[key].playerName, bossHistoryList[key].bossName);
          this.itemDict.Add(key, itemRenderer);
        }
        this.Reposition();
        NGUITools.SetActive(this.panelTip.gameObject, this.scrollView.shouldMoveVertically);
      }
      else
        NGUITools.SetActive(this.noBossHistoryTip.gameObject, true);
    }), true);
  }

  private AllianceBossHistoryItemRenderer CreateItemRenderer(int time, string playerName, string bossName)
  {
    GameObject gameObject = this.itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    AllianceBossHistoryItemRenderer component = gameObject.GetComponent<AllianceBossHistoryItemRenderer>();
    component.SetData(time, playerName, bossName);
    return component;
  }

  private void ClearData()
  {
    using (Dictionary<int, AllianceBossHistoryItemRenderer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this.itemPool.Release(gameObject);
      }
    }
    this.itemDict.Clear();
    this.itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  public class BossHistoryItem
  {
    public int time;
    public string playerName;
    public string bossName;

    public BossHistoryItem(int time, string playerName, string bossName)
    {
      this.time = time;
      this.playerName = playerName;
      this.bossName = bossName;
    }
  }
}
