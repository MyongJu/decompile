﻿// Decompiled with JetBrains decompiler
// Type: AuctionHelper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;

public class AuctionHelper
{
  private Dictionary<AuctionHelper.AuctionType, AuctionData> _auctionItemInfoDict = new Dictionary<AuctionHelper.AuctionType, AuctionData>();
  public System.Action<bool> onAuctionItemDataUpdated;
  private static AuctionHelper _instance;
  private bool _isWorldAuctionOpen;
  private Dictionary<AuctionHelper.AuctionType, AuctionData> _auctionItemInfoBackupDict;

  public static AuctionHelper Instance
  {
    get
    {
      if (AuctionHelper._instance == null)
        AuctionHelper._instance = new AuctionHelper();
      return AuctionHelper._instance;
    }
  }

  public bool IsWorldAuctionOpen
  {
    get
    {
      return this._isWorldAuctionOpen;
    }
    set
    {
      this._isWorldAuctionOpen = value;
    }
  }

  public bool HasWorldAuctionGoods
  {
    get
    {
      if (this._auctionItemInfoDict.ContainsKey(AuctionHelper.AuctionType.WORLD_AUCTION))
        return this._auctionItemInfoDict[AuctionHelper.AuctionType.WORLD_AUCTION].AuctionItemInfoList.Count > 0;
      return false;
    }
  }

  public void Initialize()
  {
    MessageHub.inst.GetPortByAction("world_auction_exceeded").AddEvent(new System.Action<object>(this.OnPushWorldAuctionExceeded));
  }

  public void Dispose()
  {
    MessageHub.inst.GetPortByAction("world_auction_exceeded").RemoveEvent(new System.Action<object>(this.OnPushWorldAuctionExceeded));
  }

  private void OnPushWorldAuctionExceeded(object orgData)
  {
    if (!this.IsWorldAuctionOpen)
      return;
    Hashtable inData = orgData as Hashtable;
    if (inData == null)
      return;
    int outData1 = 0;
    int outData2 = 0;
    DatabaseTools.UpdateData(inData, "item_id", ref outData1);
    DatabaseTools.UpdateData(inData, "count", ref outData2);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(outData1);
    if (itemStaticInfo == null)
      return;
    ToastManager.Instance.AddBasicToast("world_auction_exceeded", "0", itemStaticInfo.LocName, "1", outData2.ToString()).SetPriority(-1);
  }

  public long GetReturnGoldAmount(AuctionHelper.AuctionType auctionType)
  {
    if (this._auctionItemInfoDict.ContainsKey(auctionType))
      return this._auctionItemInfoDict[auctionType].ReturnGoldAmount;
    return 0;
  }

  public void ShowAuctionRecord(AuctionHelper.AuctionType auctionType, System.Action<bool, List<AuctionRecordItemInfo>> callback)
  {
    MessageHub.inst.GetPortByAction("auction:getTodayAuction").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "type",
        (object) auctionType
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      ArrayList arrayList = data as ArrayList;
      List<AuctionRecordItemInfo> auctionRecordItemInfoList = new List<AuctionRecordItemInfo>();
      if (arrayList != null)
      {
        for (int index = 0; index < arrayList.Count; ++index)
        {
          Hashtable inData = arrayList[index] as Hashtable;
          if (inData != null)
          {
            int outData1 = 0;
            int outData2 = 0;
            int outData3 = 0;
            int outData4 = 0;
            long outData5 = 0;
            string empty = string.Empty;
            DatabaseTools.UpdateData(inData, "item_id", ref outData1);
            DatabaseTools.UpdateData(inData, "number", ref outData2);
            DatabaseTools.UpdateData(inData, "mtime", ref outData3);
            DatabaseTools.UpdateData(inData, "artifact", ref outData4);
            DatabaseTools.UpdateData(inData, "winner_price", ref outData5);
            DatabaseTools.UpdateData(inData, "name", ref empty);
            auctionRecordItemInfoList.Add(new AuctionRecordItemInfo()
            {
              itemId = outData1,
              itemCount = outData2,
              itemWinTime = outData3,
              itemArtifactId = outData4,
              itemWinPrice = outData5,
              winerName = empty
            });
          }
        }
      }
      if (callback == null)
        return;
      auctionRecordItemInfoList.Sort((Comparison<AuctionRecordItemInfo>) ((a, b) => b.itemWinTime.CompareTo(a.itemWinTime)));
      callback(ret, auctionRecordItemInfoList);
    }), true);
  }

  public void ShowAuctionScene(AuctionHelper.AuctionType auctionType)
  {
    MessageHub.inst.GetPortByAction("auction:getAuction").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "type",
        (object) auctionType
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.ClearAuctionCache();
      this.Decode(data as Hashtable, true);
      UIManager.inst.OpenDlg("Auction/AuctionHallDlg", (UI.Dialog.DialogParameter) new AuctionHallDlg.Parameter()
      {
        auctionType = auctionType
      }, 1 != 0, 1 != 0, 1 != 0);
    }), true);
  }

  public void LoadAuctionData(AuctionHelper.AuctionType auctionType, System.Action<bool, object> callback = null, bool blockScreen = false)
  {
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    postData.Add((object) "type", (object) auctionType);
    if (callback != null)
      MessageHub.inst.GetPortByAction("auction:getAuction").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.UpdateData(auctionType, data as Hashtable, true, false);
        callback(ret, data);
      }), blockScreen);
    else
      MessageHub.inst.GetPortByAction("auction:getAuction").SendLoader(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.UpdateData(auctionType, data as Hashtable, true, false);
      }), true, false);
  }

  public List<AuctionItemInfo> GetAuctionItemInfoList(AuctionHelper.AuctionType auctionType)
  {
    if (this._auctionItemInfoDict.ContainsKey(auctionType))
      return this._auctionItemInfoDict[auctionType].AuctionItemInfoList;
    return (List<AuctionItemInfo>) null;
  }

  public AuctionHelper.AuctionItemType GetAuctionItemType(AuctionItemInfo itemInfo)
  {
    if (itemInfo == null)
      return AuctionHelper.AuctionItemType.INVALID;
    if (itemInfo.IsArtifact)
      return AuctionHelper.AuctionItemType.ARTIFACT;
    return this.GetAuctionItemType(itemInfo.itemId);
  }

  public AuctionHelper.AuctionItemType GetAuctionItemType(int itemId)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo == null)
      return AuctionHelper.AuctionItemType.INVALID;
    if (itemStaticInfo.Type == ItemBag.ItemType.equipment)
      return AuctionHelper.AuctionItemType.EQUIPMENT;
    if (itemStaticInfo.Type == ItemBag.ItemType.equipment_scroll)
      return AuctionHelper.AuctionItemType.EQUIPMENT_SCROLL;
    if (itemStaticInfo.Type == ItemBag.ItemType.dk_equipment)
      return AuctionHelper.AuctionItemType.DK_EQUIPMENT;
    return itemStaticInfo.Type == ItemBag.ItemType.dk_equipment_scroll ? AuctionHelper.AuctionItemType.DK_EQUIPMENT_SCROLL : AuctionHelper.AuctionItemType.PROPS;
  }

  public void ResetReturnGold2Zero(AuctionHelper.AuctionType auctionType)
  {
    if (!this._auctionItemInfoDict.ContainsKey(auctionType))
      return;
    this._auctionItemInfoDict[auctionType].ReturnGoldAmount = 0L;
  }

  public void UpdateData(AuctionHelper.AuctionType auctionType, Hashtable data, bool refresh = true, bool updateWithBackup = false)
  {
    this.Decode(data, refresh);
    List<AuctionItemInfo> auctionItemInfoList = this.GetAuctionItemInfoList(auctionType);
    List<AuctionItemInfo> auctionItemInfoBackupList = (List<AuctionItemInfo>) null;
    if (this._auctionItemInfoBackupDict != null && this._auctionItemInfoBackupDict.ContainsKey(auctionType))
      auctionItemInfoBackupList = this._auctionItemInfoBackupDict[auctionType].AuctionItemInfoList;
    if (!updateWithBackup || auctionItemInfoList == null)
      return;
    auctionItemInfoList.ForEach((System.Action<AuctionItemInfo>) (x =>
    {
      if (auctionItemInfoBackupList == null)
        return;
      AuctionItemInfo auctionItemInfo = auctionItemInfoBackupList.Find((Predicate<AuctionItemInfo>) (y =>
      {
        if (y.auctionType == x.auctionType && y.itemId == x.itemId)
          return y.itemIndex == x.itemIndex;
        return false;
      }));
      if (auctionItemInfo == null)
        return;
      x.itemYourBidPrice = auctionItemInfo.itemYourBidPrice;
    }));
  }

  private void Decode(Hashtable data, bool refresh = true)
  {
    if (data == null)
      return;
    this._auctionItemInfoBackupDict = new Dictionary<AuctionHelper.AuctionType, AuctionData>((IDictionary<AuctionHelper.AuctionType, AuctionData>) this._auctionItemInfoDict);
    List<AuctionItemInfo> auctionItemInfoList = new List<AuctionItemInfo>();
    long outData1 = 0;
    int outData2 = 0;
    ArrayList data1 = new ArrayList();
    if (refresh)
      DatabaseTools.UpdateData(data, "return_gold", ref outData1);
    DatabaseTools.UpdateData(data, "type", ref outData2);
    DatabaseTools.CheckAndParseOrgData(data[(object) "auction_item"], out data1);
    if (data1 != null)
    {
      for (int index = 0; index < data1.Count; ++index)
      {
        Hashtable inData = data1[index] as Hashtable;
        if (inData != null)
        {
          int num1;
          int outData3 = num1 = 0;
          int outData4 = num1;
          int outData5 = num1;
          int outData6 = num1;
          int outData7 = num1;
          int outData8 = num1;
          int outData9 = num1;
          long num2;
          long outData10 = num2 = 0L;
          long outData11 = num2;
          long outData12 = num2;
          long outData13 = num2;
          long outData14 = num2;
          DatabaseTools.UpdateData(inData, "id", ref outData9);
          DatabaseTools.UpdateData(inData, "number", ref outData7);
          DatabaseTools.UpdateData(inData, "item_id", ref outData8);
          DatabaseTools.UpdateData(inData, "end_time", ref outData6);
          DatabaseTools.UpdateData(inData, "server_type", ref outData4);
          DatabaseTools.UpdateData(inData, "artifact", ref outData3);
          DatabaseTools.UpdateData(inData, "price", ref outData14);
          DatabaseTools.UpdateData(inData, "min_add_price", ref outData11);
          DatabaseTools.UpdateData(inData, "is_self", ref outData5);
          DatabaseTools.UpdateData(inData, "top_uid", ref outData10);
          DatabaseTools.UpdateData(inData, "fee", ref outData13);
          DatabaseTools.UpdateData(inData, "bid", ref outData12);
          auctionItemInfoList.Add(new AuctionItemInfo()
          {
            itemIndex = outData9,
            itemCurrentPrice = outData14,
            itemId = outData8,
            itemCount = outData7,
            itemMinAddAmount = outData11,
            itemRemainedTime = outData6,
            itemServerType = outData4,
            itemArtifactId = outData3,
            itemFeePrice = outData13,
            itemYourBidPrice = outData12,
            isYourBiddingPrice = outData10 == PlayerData.inst.uid,
            auctionType = (AuctionHelper.AuctionType) outData2
          });
        }
      }
    }
    AuctionHelper.AuctionType index1 = (AuctionHelper.AuctionType) outData2;
    AuctionData auctionData = new AuctionData(index1, outData1, auctionItemInfoList);
    if (this._auctionItemInfoDict.ContainsKey(index1))
      this._auctionItemInfoDict[index1] = auctionData;
    else
      this._auctionItemInfoDict.Add(index1, auctionData);
    if (this.onAuctionItemDataUpdated == null)
      return;
    this.onAuctionItemDataUpdated(refresh);
  }

  private void ClearAuctionCache()
  {
    if (this._auctionItemInfoDict != null)
      this._auctionItemInfoDict.Clear();
    if (this._auctionItemInfoBackupDict == null)
      return;
    this._auctionItemInfoBackupDict.Clear();
  }

  public enum AuctionType
  {
    PUBLIC_AUCTION,
    NON_PUBLIC_AUCTION,
    WORLD_AUCTION,
  }

  public enum AuctionItemType
  {
    INVALID,
    PROPS,
    EQUIPMENT,
    EQUIPMENT_SCROLL,
    ARTIFACT,
    DK_EQUIPMENT,
    DK_EQUIPMENT_SCROLL,
  }

  public enum AuctionHistoryType
  {
    OTHER_WIN,
    AUCTION_OTHER_WIN_RETURN,
    YOU_WIN,
    AUCTION_HIGHER_PRICE_RETURN,
    AUCTION_BLACK_MARKET_YOU_WIN,
  }
}
