﻿// Decompiled with JetBrains decompiler
// Type: PitMapData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public static class PitMapData
{
  public static DynamicMapData MapData = new DynamicMapData();

  public static void Initialize()
  {
    int currentWorldId = PlayerData.inst.playerCityData.current_world_id;
    PitMapData.LoadKingdom(currentWorldId);
    PitMapData.MapData.LoadAssetPath = "TextAsset/Pit/";
    PitMapData.MapData.KingdomGround = "GROUND_VOLCANO";
    PrefabManagerEx.Instance.AddRouteMapping("GROUND_VOLCANO", "Prefab/Kingdom/Map/GROUND_VOLCANO");
    PitMapData.MapData.LoadDefault("TextAsset/Pit/default_tileset");
    PitMapData.MapData.LoadTileSet("TextAsset/Pit/master_tileset", "Prefab/Pit/", currentWorldId, 1000000);
  }

  public static void LoadKingdom(int worldId)
  {
    PitMapData.MapData.ActiveKingdoms.Clear();
    PitMapData.MapData.ActiveKingdoms.Add(worldId, new ActiveKingdom(worldId, 0));
  }
}
