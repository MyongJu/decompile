﻿// Decompiled with JetBrains decompiler
// Type: QuestTipDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UnityEngine;

public class QuestTipDlg : MonoBehaviour
{
  private const string hasQuest_vfxPath = "Prefab/VFX/fx_TIps_02";
  private const string claim_vfxPath = "Prefab/VFX/fx_Tip_03";
  private const string food_vfxPath = "Prefab/VFX/fx_fly";
  private const string wood_vfxPath = "Prefab/VFX/fx_fly";
  private const string ore_vfxPath = "Prefab/VFX/fx_fly";
  private const string silver_vfxPath = "Prefab/VFX/fx_fly";
  private QuestData2 quest;
  public UILabel questName;
  public UISprite finish;
  public UISprite questIcon;
  private long hasId;
  private IVfx cliamEffect;
  public Transform foodTarget;
  public Transform woodTarget;
  public Transform oreTarget;
  public Transform silverTarget;
  public GameObject hadQuest;
  private IVfx foodEffect;
  private IVfx woodEffect;
  private IVfx oreEffect;
  private IVfx silverEffect;
  private bool _isDestroy;

  private void PlayEffect(QuestRewardAgent agent)
  {
    if (agent.HadFoodReward())
    {
      this.foodEffect = (IVfx) null;
      this.PlayEffect(ref this.foodEffect, "Prefab/VFX/fx_fly", this.foodTarget);
    }
    if (agent.HadOreReward())
    {
      this.oreEffect = (IVfx) null;
      this.PlayEffect(ref this.oreEffect, "Prefab/VFX/fx_fly", this.oreTarget);
    }
    if (agent.HadWoodReward())
    {
      this.woodEffect = (IVfx) null;
      this.PlayEffect(ref this.woodEffect, "Prefab/VFX/fx_fly", this.woodTarget);
    }
    if (!agent.HadSilverReward())
      return;
    this.silverEffect = (IVfx) null;
    this.PlayEffect(ref this.silverEffect, "Prefab/VFX/fx_fly", this.silverTarget);
  }

  private void StopEffect()
  {
    if (this.foodEffect != null && this.foodEffect.IsAlive)
    {
      this.foodEffect.Stop();
      VfxManager.Instance.Delete(this.foodEffect);
      this.foodEffect = (IVfx) null;
    }
    if (this.woodEffect != null && this.woodEffect.IsAlive)
    {
      this.woodEffect.Stop();
      VfxManager.Instance.Delete(this.woodEffect);
      this.woodEffect = (IVfx) null;
    }
    if (this.oreEffect != null && this.oreEffect.IsAlive)
    {
      this.oreEffect.Stop();
      VfxManager.Instance.Delete(this.oreEffect);
      this.oreEffect = (IVfx) null;
    }
    if (this.silverEffect == null || !this.silverEffect.IsAlive)
      return;
    this.silverEffect.Stop();
    VfxManager.Instance.Delete(this.silverEffect);
    this.silverEffect = (IVfx) null;
  }

  private void PlayEffect(ref IVfx effect, string path, Transform to)
  {
    if (effect != null && !effect.IsPlaying)
    {
      effect.Play(this.transform);
    }
    else
    {
      MoveVfx moveVfx = VfxManager.Instance.Create(path) as MoveVfx;
      moveVfx.from = moveVfx.transform;
      moveVfx.to = to;
      moveVfx.OnMoveCompleteHandler = new System.Action<MoveVfx>(this.OnVfxComplete);
      if (effect == null)
        effect = (IVfx) moveVfx;
      moveVfx.Play(this.transform);
    }
  }

  private void OnVfxComplete(MoveVfx vfx)
  {
    HUDResource component = vfx.to.GetComponent<HUDResource>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.PlayAnimation();
  }

  private void Start()
  {
    this.Refresh();
  }

  private void OnEnable()
  {
    this.AddEventHandler();
    this.Refresh();
  }

  private void OnRecommendChangedHandler(long questId)
  {
    if (this._isDestroy)
      return;
    this.Refresh();
  }

  private void OnDisable()
  {
    if (!GameEngine.IsReady())
      return;
    this.RemoveEventHandler();
    this.StopEffect();
    if (this.cliamEffect == null || !this.cliamEffect.IsAlive)
      return;
    this.cliamEffect.Stop();
  }

  private void Refresh()
  {
    if (DBManager.inst.DB_Quest.RecommendQuest != null)
    {
      this.quest = DBManager.inst.DB_Quest.RecommendQuest;
      this.UpdateUI();
      VfxManager.Instance.Delete(this.hasId);
      NGUITools.SetActive(this.hadQuest, true);
    }
    else
    {
      this.RemoveEventHandler();
      this.gameObject.SetActive(false);
      NGUITools.SetActive(this.hadQuest, false);
    }
  }

  private void AddEventHandler()
  {
    this.RemoveEventHandler();
    PlayerData.inst.QuestManager.OnRecommendQuestChanged += new System.Action<long>(this.OnRecommendChangedHandler);
    DBManager.inst.DB_Quest.onDataChanged += new System.Action<long>(this.OnRecommendChangedHandler);
    PlayerData.inst.QuestManager.OnQuestClaimed += new System.Action<QuestRewardAgent>(this.OnFinishTask);
  }

  private void OnDestroy()
  {
    this._isDestroy = true;
  }

  private void OnFinishTask(QuestRewardAgent agent)
  {
    if (this._isDestroy)
      return;
    this.cliamEffect = VfxManager.Instance.Create("Prefab/VFX/fx_Tip_03");
    this.cliamEffect.Play(this.transform);
    this.PlayEffect(agent);
  }

  private void RemoveEventHandler()
  {
    PlayerData.inst.QuestManager.OnRecommendQuestChanged -= new System.Action<long>(this.OnRecommendChangedHandler);
    DBManager.inst.DB_Quest.onDataChanged -= new System.Action<long>(this.OnRecommendChangedHandler);
    PlayerData.inst.QuestManager.OnQuestClaimed -= new System.Action<QuestRewardAgent>(this.OnFinishTask);
  }

  private void UpdateUI()
  {
    this.questName.text = this.quest.Info == null ? string.Empty : ScriptLocalization.Get(this.quest.Info.Loc_Name, true);
    NGUITools.SetActive(this.finish.gameObject, this.quest.Status == 1);
  }

  public void OnClickHandler()
  {
    long questId = this.quest.QuestID;
    if (this.quest.Status == 0)
    {
      QuestLinkHUDInfo questLinkHudInfo = ConfigManager.inst.DB_QuestLinkHud.GetQuestLinkHudInfo(this.quest.Info.LinkHudId);
      if (questLinkHudInfo == null)
        return;
      LinkerHub.Instance.Distribute(questLinkHudInfo.Param);
      LinkerHub.Instance.Init();
    }
    else
    {
      if (this.quest.Status != 1)
        return;
      PlayerData.inst.QuestManager.ClaimQuest(questId);
    }
  }
}
