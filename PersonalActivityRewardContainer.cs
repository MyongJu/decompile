﻿// Decompiled with JetBrains decompiler
// Type: PersonalActivityRewardContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class PersonalActivityRewardContainer : MonoBehaviour
{
  private List<Icon> icons = new List<Icon>();
  public UILabel label;
  public GameObject normal;
  public GameObject achieved;
  public GameObject normalItem;
  public GameObject backBgItem;
  public UIScrollView scroll;
  public UITable table;
  public GameObject header;
  public AllianceActivityStepStatus status;

  private UILabel GetHeader()
  {
    GameObject gameObject = NGUITools.AddChild(this.table.gameObject, this.header);
    gameObject.SetActive(true);
    return gameObject.transform.GetChild(0).gameObject.GetComponent<UILabel>();
  }

  private AllianceActivityStepStatus GetStatus()
  {
    GameObject gameObject = NGUITools.AddChild(this.table.gameObject, this.status.gameObject);
    gameObject.SetActive(true);
    return gameObject.GetComponent<AllianceActivityStepStatus>();
  }

  private GameObject GetStatus(string name)
  {
    GameObject gameObject = NGUITools.AddChild(this.table.gameObject, (bool) ((UnityEngine.Object) this.status));
    gameObject.SetActive(true);
    return gameObject;
  }

  private Icon GetIcon(int index)
  {
    GameObject gameObject = index == 0 || index % 2 != 0 ? NGUITools.AddChild(this.table.gameObject, this.backBgItem) : NGUITools.AddChild(this.table.gameObject, this.normalItem);
    Icon component = gameObject.GetComponent<Icon>();
    component.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
    component.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
    component.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
    gameObject.SetActive(true);
    this.icons.Add(component);
    return component;
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  public void Refresh(ActivityBaseData data)
  {
    List<PersonalActivityReward> toatalRewards = ConfigManager.inst.DB_PersonalActivtyReward.GetToatalRewards();
    PersonalActivityReward data1 = ConfigManager.inst.DB_PersonalActivtyReward.GetData(ActivityManager.Intance.personalData.ActivityId);
    for (int index = 0; index < toatalRewards.Count; ++index)
    {
      PersonalActivityReward personalActivityReward = toatalRewards[index];
      if (data1 == null || personalActivityReward.StrongholdLevel > data1.StrongholdLevel)
        this.SetData(index + 1, PlayerData.inst.CityData.mStronghold.mLevel, personalActivityReward.StrongholdLevel, personalActivityReward.ItemRewardID_1, personalActivityReward.ItemRewardValue_1, personalActivityReward.ItemRewardID_2, personalActivityReward.ItemRewardValue_2, personalActivityReward.ItemRewardID_3, personalActivityReward.ItemRewardValue_3, personalActivityReward.ItemRewardID_4, personalActivityReward.ItemRewardValue_4, personalActivityReward.ItemRewardID_5, personalActivityReward.ItemRewardValue_5, personalActivityReward.ItemRewardID_6, personalActivityReward.ItemRewardValue_6);
    }
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scroll.ResetPosition();
  }

  public void Clear()
  {
    for (int index = 0; index < this.icons.Count; ++index)
      this.icons[index].Dispose();
    this.icons.Clear();
  }

  private void SetData(int index, int currentScore, int targetScore, params int[] items)
  {
    this.GetHeader().text = ScriptLocalization.GetWithPara("event_alliance_step_reward_num", new Dictionary<string, string>()
    {
      {
        "0",
        Utils.GetRomaNumeralsBelowTen(index)
      }
    }, true);
    AllianceActivityStepStatus status = this.GetStatus();
    string withPara = ScriptLocalization.GetWithPara("event_solo_event_stronghold_level", new Dictionary<string, string>()
    {
      {
        "0",
        targetScore.ToString()
      }
    }, true);
    status.SetData(targetScore, (long) currentScore, withPara);
    int index1 = 0;
    while (index1 < items.Length)
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(items[index1]);
      if (itemStaticInfo != null)
      {
        IconData iconData = new IconData();
        iconData.image = itemStaticInfo.ImagePath;
        iconData.contents = new string[2]
        {
          string.Empty,
          string.Empty
        };
        iconData.contents[0] = itemStaticInfo.LocName;
        iconData.contents[1] = items[index1 + 1].ToString();
        iconData.Data = (object) itemStaticInfo.internalId;
        this.GetIcon(index1).FeedData((IComponentData) iconData);
      }
      index1 += 2;
    }
  }
}
