﻿// Decompiled with JetBrains decompiler
// Type: DragonWithSkillComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class DragonWithSkillComponent : ComponentRenderBase
{
  public Icon dragon;
  public IconListComponent skillList;
  public UILabel noDragonLabel;

  public override void Init()
  {
    base.Init();
    DragonWithSkillComponentData data1 = this.data as DragonWithSkillComponentData;
    if (data1 == null)
    {
      this.gameObject.SetActive(false);
    }
    else
    {
      if (int.Parse(data1.no_dragon) == 1)
      {
        this.noDragonLabel.gameObject.SetActive(true);
        this.noDragonLabel.text = Utils.XLAT("mail_scout_report_no_dragon_notice");
        this.dragon.gameObject.SetActive(false);
      }
      else
      {
        this.noDragonLabel.gameObject.SetActive(false);
        this.dragon.gameObject.SetActive(true);
        this.dragon.FeedData((IComponentData) new IconData(DragonUtils.GetDragonIconPath(int.Parse(data1.tendency), int.Parse(data1.level)), new string[1]
        {
          data1.level
        }));
      }
      List<IconData> data2 = new List<IconData>();
      if (data1.skill != null)
      {
        using (Dictionary<string, string>.Enumerator enumerator = data1.skill.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, string> current = enumerator.Current;
            ConfigDragonSkillGroupInfo dragonSkillGroupInfo = ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupInfo(int.Parse(current.Key));
            ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(data1.artifact);
            string str = string.Empty;
            if (artifactInfo != null && dragonSkillGroupInfo.learn_method == 0)
            {
              int num = !(artifactInfo.activeEffect == "light_wand") ? (!(dragonSkillGroupInfo.type == ConfigDragonSkillGroupInfo.DARK) ? 0 : 1) : (!(dragonSkillGroupInfo.type == ConfigDragonSkillGroupInfo.LIGHT) ? 0 : 1);
              if (num > 0)
                str = "+" + (object) num;
            }
            int result = 0;
            if (data1.skill_star != null && data1.skill_star.ContainsKey(current.Key))
              int.TryParse(data1.skill_star[current.Key], out result);
            bool[] visibles = new bool[1]{ result > 0 };
            IconData iconData = new IconData(dragonSkillGroupInfo.IconPath, visibles, new string[3]
            {
              int.Parse(current.Value) <= 0 ? string.Empty : current.Value.ToString(),
              str,
              result.ToString()
            });
            data2.Add(iconData);
          }
        }
      }
      this.skillList.FeedData((IComponentData) new IconListComponentData(data2));
    }
  }
}
