﻿// Decompiled with JetBrains decompiler
// Type: GroupQuestPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;

public class GroupQuestPopup : Popup
{
  public GroupQuestComplete completeStatus;
  public GroupQuestUncomplete unCompleteStatus;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
  }

  public void UpdateUI()
  {
    if (DBManager.inst.DB_Quest.GroupQuests.Count < 1)
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    if (DBManager.inst.DB_Quest.IsGroupQuestsComplete())
    {
      this.completeStatus.gameObject.SetActive(true);
      this.unCompleteStatus.gameObject.SetActive(false);
      this.completeStatus.UpdateUI();
    }
    else
    {
      this.completeStatus.gameObject.SetActive(false);
      this.unCompleteStatus.gameObject.SetActive(true);
      this.unCompleteStatus.UpdateUI();
    }
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnClaimBtnClick()
  {
    PlayerData.inst.QuestManager.ClaimGroupQuest((System.Action) (() => this.UpdateUI()));
  }

  public class Parameter : Popup.PopupParameter
  {
  }
}
