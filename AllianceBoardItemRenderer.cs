﻿// Decompiled with JetBrains decompiler
// Type: AllianceBoardItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class AllianceBoardItemRenderer : MonoBehaviour
{
  public UITexture m_PortraitIcon;
  public UILabel m_UserName;
  public UILabel m_DateTime;
  public UILabel m_Text;
  private AllianceBoardPayload m_Payload;

  public void SetData(AllianceBoardPayload payload)
  {
    this.m_Payload = payload;
    CustomIconLoader.Instance.requestCustomIcon(this.m_PortraitIcon, "Texture/Hero/Portrait_Icon/player_portrait_icon_" + (object) payload.portrait, payload.icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.m_PortraitIcon, payload.lordTitleId, 1);
    this.m_UserName.text = payload.name;
    this.m_DateTime.text = Utils.FormatOfflineTime((long) NetServerTime.inst.ServerTimestamp - payload.ctime);
    this.m_Text.text = payload.content;
  }

  public AllianceBoardPayload Payload
  {
    get
    {
      return this.m_Payload;
    }
  }

  public void OnItemClick()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceBoardMenuPopup", (Popup.PopupParameter) new AllianceBoardMenuPopup.Parameter()
    {
      payload = this.m_Payload
    });
  }
}
