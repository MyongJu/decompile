﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarRankItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceWarRankItem : MonoBehaviour
{
  private static Dictionary<string, bool> _openState = new Dictionary<string, bool>();
  [SerializeField]
  private GameObject _rootShowWhenOpened;
  [SerializeField]
  private GameObject _rootShowWhenClosed;
  [SerializeField]
  private GameObject _rootSelfAllianceTag;
  [SerializeField]
  private UILabel _labelRank;
  [SerializeField]
  private UILabel _labelKingdom;
  [SerializeField]
  private UILabel _labelAllianceName;
  [SerializeField]
  private UILabel _labelScore;
  [SerializeField]
  private UILabel _labelLeaderName;
  [SerializeField]
  private UILabel _labelLanguage;
  [SerializeField]
  private UILabel _labelMemberCount;
  [SerializeField]
  private UILabel _labelPower;
  [SerializeField]
  private AllianceSymbol _allianceSymbol;
  [SerializeField]
  private UIButton _buttonTraceMemberLocation;
  [SerializeField]
  private UIButton _buttonGoto;
  [SerializeField]
  private UISprite[] _allSpriteChangeColor;
  private AllianceWarRankData _allianceWarRankData;
  private AllianceWarRankItemData _allianceWarRankItemData;
  private UITable _container;

  private static string GetOpenStateKey(int startTime, long allianceId)
  {
    return string.Format("{0}_{1}", (object) startTime, (object) allianceId);
  }

  private static void SetOpenState(int startTime, long allianceId, bool openState)
  {
    string openStateKey = AllianceWarRankItem.GetOpenStateKey(startTime, allianceId);
    if (AllianceWarRankItem._openState.ContainsKey(openStateKey))
      AllianceWarRankItem._openState.Remove(openStateKey);
    if (!openState)
      return;
    AllianceWarRankItem._openState.Add(openStateKey, true);
  }

  private static bool GetOpenState(int startTime, long allianceId)
  {
    string openStateKey = AllianceWarRankItem.GetOpenStateKey(startTime, allianceId);
    bool flag = false;
    AllianceWarRankItem._openState.TryGetValue(openStateKey, out flag);
    return flag;
  }

  public void SetData(AllianceWarRankData allianceWarRankData, AllianceWarRankItemData allianceWarRankItemData, UITable container)
  {
    this._allianceWarRankData = allianceWarRankData;
    this._allianceWarRankItemData = allianceWarRankItemData;
    this._container = container;
    this._labelRank.text = this._allianceWarRankItemData.Rank.ToString();
    this._labelKingdom.text = string.Format("#{0}", (object) this._allianceWarRankItemData.Kingdom);
    this._labelAllianceName.text = this._allianceWarRankItemData.AllianceName;
    this._labelScore.text = this._allianceWarRankItemData.Score.ToString();
    this._labelLeaderName.text = this._allianceWarRankItemData.LeaderName;
    this._labelLanguage.text = Language.Instance.GetLanguageName(this._allianceWarRankItemData.Language);
    this._labelMemberCount.text = string.Format("{0}/{1}", (object) this._allianceWarRankItemData.MemberCount, (object) this._allianceWarRankItemData.MaxMemberCount);
    this._labelPower.text = this._allianceWarRankItemData.Power.ToString();
    this._buttonTraceMemberLocation.gameObject.SetActive(PlayerData.inst.userData.AcAllianceId != this._allianceWarRankItemData.AllianceId);
    this._allianceSymbol.SetSymbols(this._allianceWarRankItemData.AllianceSymbole);
    this._buttonGoto.isEnabled = this._allianceWarRankItemData.Kingdom != PlayerData.inst.userData.current_world_id;
    Color color32 = NGUIText.ParseColor32("FFD664FF", 0);
    if (this._allianceWarRankItemData.AllianceId == PlayerData.inst.userData.AcAllianceId)
    {
      this._rootSelfAllianceTag.SetActive(true);
      this._labelAllianceName.color = color32;
    }
    else
      this._rootSelfAllianceTag.SetActive(false);
    if (this._allianceWarRankItemData.Kingdom == PlayerData.inst.userData.current_world_id)
    {
      foreach (UIWidget uiWidget in this._allSpriteChangeColor)
        uiWidget.color = color32;
    }
    this.ToggleOpenState(AllianceWarRankItem.GetOpenState(this._allianceWarRankData.StartTime, this._allianceWarRankItemData.AllianceId));
  }

  public void SetGotoButtonEnabled(bool enabled)
  {
    this._buttonGoto.gameObject.SetActive(enabled);
  }

  public void OnOpenButtonClicked()
  {
    AllianceWarRankItem.SetOpenState(this._allianceWarRankData.StartTime, this._allianceWarRankItemData.AllianceId, true);
    this.ToggleOpenState(true);
  }

  public void OnCloseButtonClicked()
  {
    AllianceWarRankItem.SetOpenState(this._allianceWarRankData.StartTime, this._allianceWarRankItemData.AllianceId, false);
    this.ToggleOpenState(false);
  }

  public void OnTraceMemberPositionClicked()
  {
    Logger.Log(nameof (OnTraceMemberPositionClicked));
    AllianceWarPayload.Instance.ShowAllianceTrack(this._allianceWarRankItemData.AllianceId);
  }

  public void OnButtonGotoClicked()
  {
    Logger.Log(nameof (OnButtonGotoClicked));
    if (this._allianceWarRankData.StartTime > NetServerTime.inst.ServerTimestamp)
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_not_open"), (System.Action) null, 4f, true);
    else
      AllianceWarPayload.Instance.GetInvadePosition(PlayerData.inst.userData.uid, this._allianceWarRankItemData.AllianceId, (System.Action<bool, Coordinate>) ((ret, location) =>
      {
        if (!ret)
          return;
        UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
        {
          UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
          UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
          GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
          UIManager.inst.Cloud.PokeCloud((System.Action) (() => AllianceWarPayload.Instance.GotoTarget(location)));
        }));
      }));
  }

  protected void ToggleOpenState(bool open)
  {
    this._rootShowWhenOpened.SetActive(open);
    this._rootShowWhenClosed.SetActive(!open);
    this._container.repositionNow = true;
    this._container.Reposition();
  }
}
