﻿// Decompiled with JetBrains decompiler
// Type: InitUserDataState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Funplus;
using I2.Loc;
using JWT;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class InitUserDataState : LoadBaseState
{
  public static bool Visited;

  public InitUserDataState(int step)
    : base(step)
  {
  }

  public override string Key
  {
    get
    {
      return nameof (InitUserDataState);
    }
  }

  protected override SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.LoadUserData;
    }
  }

  protected override void Prepare()
  {
    base.Prepare();
    this.Login();
    Oscillator.Instance.updateEvent += new System.Action<double>(this.UpdateEventHandler);
  }

  private void UpdateEventHandler(double t)
  {
    GameEngine.Instance.iTweenValue = Mathf.Lerp(GameEngine.Instance.iTweenValue, SplashDataConfig.GetValue(this.CurrentPhase), 0.02f);
    this.RefreshProgress(GameEngine.Instance.iTweenValue);
  }

  protected override void OnDispose()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.updateEvent -= new System.Action<double>(this.UpdateEventHandler);
  }

  private void Login()
  {
    InitUserDataState.Visited = true;
    Language.Instance.Initialize();
    PerformanceConfiguration.Instance.Initialize();
    Hashtable hashtable1 = new Hashtable();
    hashtable1.Add((object) "app_version", (object) AppSetting.AppVersion);
    hashtable1.Add((object) "fpid", (object) AccountManager.Instance.AccountId);
    hashtable1.Add((object) "push_service", (object) ThirdPartyPushManager.Instance.GetPushService());
    hashtable1.Add((object) "lang", (object) Language.Instance.GetLocalizationLanguage());
    hashtable1.Add((object) "client_version", (object) NativeManager.inst.AppMajorVersion);
    hashtable1.Add((object) "kingdom_id", (object) AccountManager.Instance.KingdomID);
    hashtable1.Add((object) "cv", (object) NetApi.inst.ConfigFilesVersion);
    if (!string.IsNullOrEmpty(NetApi.inst.AdjustTracker))
      hashtable1.Add((object) "adjust_tracker", (object) NetApi.inst.AdjustTracker);
    string severChannel = CustomDefine.GetSeverChannel();
    if (!string.IsNullOrEmpty(severChannel))
      hashtable1.Add((object) "channel", (object) severChannel);
    string empty = string.Empty;
    if (CustomDefine.IsOfficialDefine() && Application.platform == RuntimePlatform.Android)
    {
      string androidImei = FunplusSdkUtils.Instance.GetAndroidIMEI();
      if (!string.IsNullOrEmpty(androidImei))
        hashtable1.Add((object) "imei", (object) androidImei);
    }
    string sysLanguage = NativeManager.inst.SysLanguage;
    hashtable1.Add((object) "sys_lang", !string.IsNullOrEmpty(sysLanguage) ? (object) sysLanguage : (object) "en_US");
    if (!string.IsNullOrEmpty(AccountManager.Instance.FunplusID))
    {
      hashtable1[(object) "fpid"] = (object) AccountManager.Instance.FunplusID;
      hashtable1.Add((object) "session_key", (object) AccountManager.Instance.SessionKey);
    }
    hashtable1[(object) "social_id"] = (object) string.Empty;
    hashtable1[(object) "os"] = (object) NativeManager.inst.GetOS();
    hashtable1[(object) "os_version"] = (object) FunplusSdkUtils.Instance.GetOsVersion();
    if (Application.platform == RuntimePlatform.IPhonePlayer)
    {
      string idfa = FunplusSdkUtils.Instance.GetIDFA();
      if (!string.IsNullOrEmpty(idfa))
        hashtable1[(object) "idfa"] = (object) idfa;
      string idfv = FunplusSdkUtils.Instance.GetIDFV();
      if (!string.IsNullOrEmpty(idfv))
        hashtable1[(object) "idfv"] = (object) idfv;
    }
    hashtable1[(object) "currency_code"] = (object) PaymentManager.Instance.GetCurrencyCode();
    hashtable1[(object) "time_zone"] = (object) FunplusSdkUtils.Instance.GetCurrentTimeZone();
    hashtable1[(object) "device_lang"] = (object) FunplusSdkUtils.Instance.GetLanguage();
    if (Application.platform == RuntimePlatform.Android)
    {
      string androidId = FunplusSdkUtils.Instance.GetAndroidID();
      if (!string.IsNullOrEmpty(androidId))
        hashtable1[(object) "android_id"] = (object) androidId;
    }
    hashtable1[(object) "gaid"] = (object) string.Empty;
    if (Application.platform == RuntimePlatform.Android)
    {
      string gaid = NativeManager.inst.GAID;
      if (!string.IsNullOrEmpty(gaid))
        hashtable1[(object) "gaid"] = (object) gaid;
    }
    Hashtable hashtable2 = new Hashtable();
    hashtable2[(object) "class"] = (object) "call";
    hashtable2[(object) "method"] = (object) "init";
    hashtable2[(object) "params"] = (object) hashtable1;
    LoaderInfo loaderInfo = this.GetLoaderInfo("call init", NetApi.inst.BuildApiUrl(string.Empty));
    loaderInfo.PostData = hashtable2;
    loaderInfo.ForceEncrypt = true;
    this.batch.Add(loaderInfo);
    LoaderManager.inst.Add(this.batch, false, false);
    BundleManager.Instance.LoadBasicBundle("static+basic_common");
    BundleManager.Instance.LoadBasicBundle("static+basic_gui2");
    BundleManager.Instance.LoadBasicBundle("static+scene_ui");
    BundleManager.Instance.LoadBasicBundle("static+citybase");
    BundleManager.Instance.LoadBasicBundle("static+citybuildings");
    BundleManager.Instance.LoadBasicBundle("static+walls");
    CitadelSystem.inst.StartCoroutine(this.InitBigPrefab());
  }

  [DebuggerHidden]
  public IEnumerator InitBigPrefab()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    InitUserDataState.\u003CInitBigPrefab\u003Ec__Iterator8A prefabCIterator8A = new InitUserDataState.\u003CInitBigPrefab\u003Ec__Iterator8A();
    return (IEnumerator) prefabCIterator8A;
  }

  private void CallInit(int fpid)
  {
    Hashtable hashtable = new Hashtable();
    hashtable.Add((object) "app_version", (object) AppSetting.AppVersion);
    hashtable.Add((object) nameof (fpid), (object) fpid);
    hashtable.Add((object) "lang", (object) Language.Instance.GetLocalizationLanguage());
    hashtable.Add((object) "kingdom_id", (object) AccountManager.Instance.KingdomID);
    string language = FunplusSdkUtils.Instance.GetLanguage();
    hashtable.Add((object) "sys_lang", !string.IsNullOrEmpty(language) ? (object) language : (object) "en_US");
    if (!string.IsNullOrEmpty(AccountManager.Instance.FunplusID))
    {
      hashtable[(object) nameof (fpid)] = (object) AccountManager.Instance.FunplusID;
      hashtable.Add((object) "session_key", (object) AccountManager.Instance.SessionKey);
    }
    Hashtable postHt = new Hashtable();
    postHt[(object) "class"] = (object) "call";
    postHt[(object) "method"] = (object) "init";
    postHt[(object) "params"] = (object) hashtable;
    this.batch.Add("call init" + (object) fpid, NetApi.inst.BuildApiUrl(string.Empty), postHt, false, 3, true);
    LoaderManager.inst.Add(this.batch, false, false);
  }

  protected override void Retry()
  {
    this.ResetBatch();
    this.Login();
  }

  protected override void OnAllFileFinish(LoaderBatch batch)
  {
    base.OnAllFileFinish(batch);
    LoaderInfo resultInfo = batch.GetResultInfo("call init");
    if (resultInfo.ResData == null || !this.CheckResult(resultInfo))
      return;
    PlayerPrefsEx.SetInt("last_login", PlayerPrefsEx.GetInt("login_time", 0));
    PlayerPrefsEx.SetInt("login_time", NetServerTime.inst.ServerTimestamp);
    PlayerPrefsEx.SetInt("recommend_package", 0);
    Hashtable hashtable1 = Utils.Json2Object(Encryptor.LoaderDecode(resultInfo), true) as Hashtable;
    string empty1 = string.Empty;
    if (hashtable1.Contains((object) "ok"))
      empty1 = hashtable1[(object) "ok"].ToString();
    long updateTime = 0;
    if (hashtable1.ContainsKey((object) "time") && hashtable1[(object) "time"] != null)
      updateTime = long.Parse(hashtable1[(object) "time"].ToString());
    Hashtable hashtable2 = (Hashtable) null;
    Hashtable hashtable3 = (Hashtable) null;
    string empty2 = string.Empty;
    if (hashtable1.Contains((object) "payload") && hashtable1[(object) "payload"] != null)
    {
      hashtable2 = hashtable1[(object) "payload"] as Hashtable;
      if (hashtable2.ContainsKey((object) "is_alipay_open") && hashtable2[(object) "is_alipay_open"] != null)
      {
        bool outData = false;
        DatabaseTools.UpdateData(hashtable2, "is_alipay_open", ref outData);
        IapPaymentModePopup.AlipayEnabled = outData;
      }
      if (hashtable2.ContainsKey((object) "kingdom") && hashtable2[(object) "kingdom"] != null)
      {
        hashtable3 = hashtable2[(object) "kingdom"] as Hashtable;
        PVPMapData.LoadKingdom((object) hashtable3);
      }
      if (hashtable2.ContainsKey((object) "token") && hashtable2[(object) "token"] != null)
        empty2 = hashtable2[(object) "token"].ToString();
      if (hashtable2.ContainsKey((object) "is_open_account_unbind") && hashtable2[(object) "is_open_account_unbind"] != null && hashtable2[(object) "is_open_account_unbind"].ToString().Trim().ToLower() != "true")
        AccountManager.Instance.LockReplaceAccount();
      if (hashtable2.ContainsKey((object) "chat_token") && hashtable2[(object) "chat_token"] != null)
        NetApi.inst.RtmToken = hashtable2[(object) "chat_token"].ToString();
      for (int index = 0; index < 5; ++index)
      {
        string str1 = "account_name_" + (object) (index + 1);
        string str2 = "account_type_" + (object) (index + 1);
        string str3 = "account_mtime_" + (object) (index + 1);
        if (hashtable2.ContainsKey((object) str1) && hashtable2[(object) str1] != null && (hashtable2.ContainsKey((object) str2) && hashtable2[(object) str2] != null))
        {
          string id = hashtable2[(object) str1].ToString();
          string type = hashtable2[(object) str2].ToString();
          int result = 0;
          if (hashtable2[(object) str3] != null)
            int.TryParse(hashtable2[(object) str3].ToString(), out result);
          AccountManager.Instance.AddAccount(id, type, result);
        }
      }
      if (hashtable2.ContainsKey((object) "alliance_treasury_vault_count"))
      {
        int result = 0;
        if (int.TryParse(hashtable2[(object) "alliance_treasury_vault_count"].ToString(), out result))
          AllianceTreasuryCountManager.Instance.SetUnopendChestCount(result);
      }
      if (hashtable2.ContainsKey((object) "gm_contact") && hashtable2[(object) "gm_contact"] != null)
        AccountManager.Instance.SetGMContact(hashtable2[(object) "gm_contact"].ToString());
      if (hashtable2.ContainsKey((object) "mail_constraint"))
      {
        Hashtable hashtable4 = hashtable2[(object) "mail_constraint"] as Hashtable;
        if (hashtable4 != null)
        {
          if (hashtable4.ContainsKey((object) "enabled") && hashtable4[(object) "enabled"] != null)
            ChatAndMailConstraint.Instance.SwitchState = hashtable4[(object) "enabled"].ToString().Trim().ToLower() == "true";
          if (hashtable4.ContainsKey((object) "level") && hashtable4[(object) "level"] != null)
            ChatAndMailConstraint.Instance.LevelLimit = int.Parse(hashtable4[(object) "level"].ToString());
        }
      }
      bool result1;
      if (hashtable2.ContainsKey((object) "mail_client_filter") && hashtable2[(object) "mail_client_filter"] != null && bool.TryParse(hashtable2[(object) "mail_client_filter"].ToString(), out result1))
        MsgService.Instance.MsgCheckFilter = result1;
      bool result2;
      if (hashtable2.ContainsKey((object) "rtm_filter") && hashtable2[(object) "rtm_filter"] != null && bool.TryParse(hashtable2[(object) "rtm_filter"].ToString(), out result2))
        MsgService.Instance.RTMFilter = result2;
      if (hashtable2.ContainsKey((object) "msg_check_url") && hashtable2[(object) "msg_check_url"] != null)
        MsgService.Instance.MsgCheckUrl = hashtable2[(object) "msg_check_url"].ToString();
      if (hashtable2.ContainsKey((object) "rtm_check_url") && hashtable2[(object) "rtm_check_url"] != null)
        MsgService.Instance.RTMCheckUrl = hashtable2[(object) "rtm_check_url"].ToString();
      PlayerData.inst.Country = !hashtable2.ContainsKey((object) "country") || hashtable2[(object) "country"] == null ? (string) null : hashtable2[(object) "country"].ToString();
      if (hashtable2.ContainsKey((object) "is_open_upgrade_troop") && hashtable2[(object) "is_open_upgrade_troop"] != null)
        BarracksManager.Instance.IsUpgradeTroopOpen = hashtable2[(object) "is_open_upgrade_troop"].ToString().Trim().ToLower() == "true";
      ThirdPartyPushManager.Instance.SetAlias(AccountManager.Instance.FunplusID);
      if (hashtable2.ContainsKey((object) "open_tavern_wheel") && hashtable2[(object) "open_tavern_wheel"] != null)
      {
        RoulettePayload.Instance.IsOpen = true;
        Hashtable hashtable4 = hashtable2[(object) "open_tavern_wheel"] as Hashtable;
        if (hashtable4.ContainsKey((object) "groupId") && hashtable4[(object) "groupId"] != null)
          RoulettePayload.Instance.GroupId = int.Parse(hashtable4[(object) "groupId"].ToString());
        if (hashtable4.ContainsKey((object) "end") && hashtable4[(object) "end"] != null)
          RoulettePayload.Instance.EndTime = int.Parse(hashtable4[(object) "end"].ToString());
      }
      else
        RoulettePayload.Instance.IsOpen = false;
      if (hashtable2.ContainsKey((object) "client_switch"))
      {
        SwitchManager.Instance.Init(hashtable2[(object) "client_switch"] as Hashtable);
        if (SwitchManager.Instance.GetSwitch("test_switch"))
        {
          Debug.Log((object) "test exception");
          throw new Exception("test exception");
        }
      }
    }
    object orgData = (object) null;
    if (hashtable1.ContainsKey((object) "data") && hashtable1[(object) "data"] != null)
      orgData = hashtable1[(object) "data"];
    if (empty1 == "1" && updateTime != 0L && (empty2 != null && hashtable2 != null) && (orgData != null && hashtable3 != null))
    {
      NetServerTime.inst.SetServerTime((double) updateTime / 1000.0);
      NetServerTime.inst.StartSyncTime();
      FunplusPayment.Instance.SetCurrencyWhitelist((string) null);
      NetApi.inst.Token = empty2;
      AccountManager.Instance.CheckAccountType();
      Hashtable hashtable4 = (Hashtable) JsonWebToken.DecodeToObject(empty2, "_FUN+GaMe_", true);
      if (hashtable4 != null && hashtable4[(object) "uid"] != null)
      {
        PlayerData.inst.hostPlayer.SetUid(long.Parse(hashtable4[(object) "uid"].ToString()));
        PlayerPrefsEx.SetString("saved_uid", hashtable4[(object) "uid"].ToString());
      }
      else
        Debug.LogError((object) "token decode fail!!!");
      DBManager.inst.LoadDatas(orgData, updateTime);
      PlayerData.inst.InitData(hashtable2);
      PlayerData.inst.moderatorInfo.InitFromHashtable(orgData as Hashtable);
      if (hashtable2.ContainsKey((object) "annv_data") && hashtable2[(object) "annv_data"] != null)
        AnniversaryManager.Instance.SetUp(hashtable2[(object) "annv_data"]);
      if (hashtable2.ContainsKey((object) "is_certifying_required") && hashtable2[(object) "is_certifying_required"] != null)
        AccountManager.Instance.SetCertifyingRequired(hashtable2[(object) "is_certifying_required"].ToString());
      if (hashtable2.ContainsKey((object) "patch_needed") && hashtable2[(object) "patch_needed"] != null)
        AccountManager.Instance.NeedPathCheck = hashtable2[(object) "patch_needed"].ToString() == "1";
      if (hashtable2.ContainsKey((object) "annv_conf") && hashtable2[(object) "annv_conf"] != null)
        AnniversaryIapPayload.Instance.SetTimeFromServer(hashtable2[(object) "annv_conf"] as Hashtable);
      if (PlayerData.inst.userData != null)
        AccountManager.Instance.KingdomID = PlayerData.inst.userData.world_id;
      LanguageSDK.Instance.SetBiLanguage(true);
      this.Finish();
      if (PaymentManager.Instance.IsPaidUser)
        TrackEvent.Instance.Trace("paying_user", TrackEvent.Type.Adjust);
      this.LogLoginAdjust();
    }
    else
    {
      string empty3 = string.Empty;
      if (hashtable1.Contains((object) "msg"))
        empty3 = hashtable1[(object) "msg"].ToString();
      string title = ScriptLocalization.Get("data_error_title", true);
      string tip = ScriptLocalization.Get("data_error_initialization3_description", true);
      if (!string.IsNullOrEmpty(empty3))
      {
        if (!NetWorkDetector.Instance.IsReadyRestart())
          NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.Loading, "InitUserDataFail", empty3);
        NetWorkDetector.Instance.RestartOrQuitGame(tip, title, (string) null);
      }
      else
      {
        string content = ScriptLocalization.Get("data_error_initialization1_description", true);
        NetWorkDetector.Instance.RetryTip(new System.Action(((LoadBaseState) this).Retry), title, content);
      }
    }
    CitadelSystem.inst.StartCoroutine(this.CacheCityBuildings());
  }

  [DebuggerHidden]
  public IEnumerator CacheCityBuildings()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    InitUserDataState.\u003CCacheCityBuildings\u003Ec__Iterator8B buildingsCIterator8B = new InitUserDataState.\u003CCacheCityBuildings\u003Ec__Iterator8B();
    return (IEnumerator) buildingsCIterator8B;
  }

  private void LogLoginAdjust()
  {
    int num = (NetServerTime.inst.ServerTimestamp - (int) PlayerData.inst.userData.CTime / 86400 * 86400) / 86400;
    if (num == 1)
      TrackEvent.Instance.Trace("day_2_login", TrackEvent.Type.Adjust);
    if (num != 7)
      return;
    TrackEvent.Instance.Trace("day_7_login", TrackEvent.Type.Adjust);
  }

  protected override void Finish()
  {
    if (this.IsDestroy)
      return;
    base.Finish();
    this.RefreshProgress();
    if (Oscillator.IsAvailable)
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.UpdateEventHandler);
    LocalizationManager.CurrentLanguageCode = PlayerData.inst.userData.language;
    string lanname = this.GetLanguageSourceNameFromCode(PlayerData.inst.userData.language);
    AssetManager.Instance.LoadAsync(lanname, (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      LocalizationManager.GlobalSources = new List<string>()
      {
        lanname,
        "I2Loading"
      }.ToArray();
      LocalizationManager.UpdateSources();
      this.Preloader.OnInitDataFinish();
    }), (System.Type) null);
  }

  private string GetLanguageSourceNameFromCode(string code)
  {
    return "I2Languages_" + code;
  }
}
