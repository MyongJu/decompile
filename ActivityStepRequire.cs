﻿// Decompiled with JetBrains decompiler
// Type: ActivityStepRequire
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ActivityStepRequire : MonoBehaviour
{
  public Icon requireRender;
  public UIGrid grid;
  public UIScrollView scrollView;
  private bool inited;

  public void UpdatUI(int mainInternalID)
  {
    if (this.inited)
      return;
    this.ClearGrid();
    List<ActivityRequirementInfo> currentRequirementInfo = ConfigManager.inst.DB_ActivityRequirement.GetCurrentRequirementInfo(ActivityManager.Intance.CurrentTimeLimitActivity.CurrentRound.ActivitySubConfigID);
    for (int index = 0; index < currentRequirementInfo.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.requireRender.gameObject);
      gameObject.SetActive(true);
      gameObject.GetComponent<Icon>().FeedData((IComponentData) new IconData((string) null, new string[2]
      {
        currentRequirementInfo[index].Description,
        string.Empty
      }));
    }
    this.grid.Reposition();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.scrollView.ResetPosition()));
    this.inited = true;
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.grid);
  }
}
