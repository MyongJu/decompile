﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.BasicApi.Quests.QuestState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace GooglePlayGames.BasicApi.Quests
{
  public enum QuestState
  {
    Upcoming = 1,
    Open = 2,
    Accepted = 3,
    Completed = 4,
    Expired = 5,
    Failed = 6,
  }
}
