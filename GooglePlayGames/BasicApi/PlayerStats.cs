﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.BasicApi.PlayerStats
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace GooglePlayGames.BasicApi
{
  public class PlayerStats
  {
    private static float UNSET_VALUE = -1f;

    public PlayerStats()
    {
      this.Valid = false;
    }

    public bool Valid { get; set; }

    public int NumberOfPurchases { get; set; }

    public float AvgSessonLength { get; set; }

    public int DaysSinceLastPlayed { get; set; }

    public int NumberOfSessions { get; set; }

    public float SessPercentile { get; set; }

    public float SpendPercentile { get; set; }

    public float SpendProbability { get; set; }

    public float ChurnProbability { get; set; }

    public bool HasNumberOfPurchases()
    {
      return this.NumberOfPurchases != (int) PlayerStats.UNSET_VALUE;
    }

    public bool HasAvgSessonLength()
    {
      return (double) this.AvgSessonLength != (double) PlayerStats.UNSET_VALUE;
    }

    public bool HasDaysSinceLastPlayed()
    {
      return this.DaysSinceLastPlayed != (int) PlayerStats.UNSET_VALUE;
    }

    public bool HasNumberOfSessions()
    {
      return this.NumberOfSessions != (int) PlayerStats.UNSET_VALUE;
    }

    public bool HasSessPercentile()
    {
      return (double) this.SessPercentile != (double) PlayerStats.UNSET_VALUE;
    }

    public bool HasSpendPercentile()
    {
      return (double) this.SpendPercentile != (double) PlayerStats.UNSET_VALUE;
    }

    public bool HasChurnProbability()
    {
      return (double) this.ChurnProbability != (double) PlayerStats.UNSET_VALUE;
    }
  }
}
