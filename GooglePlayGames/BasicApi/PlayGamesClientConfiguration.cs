﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.BasicApi.PlayGamesClientConfiguration
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.BasicApi
{
  public struct PlayGamesClientConfiguration
  {
    public static readonly PlayGamesClientConfiguration DefaultConfiguration = new PlayGamesClientConfiguration.Builder().WithPermissionRationale("Select email address to send to this game or hit cancel to not share.").Build();
    private readonly bool mEnableSavedGames;
    private readonly bool mRequireGooglePlus;
    private readonly InvitationReceivedDelegate mInvitationDelegate;
    private readonly MatchDelegate mMatchDelegate;
    private readonly string mPermissionRationale;

    private PlayGamesClientConfiguration(PlayGamesClientConfiguration.Builder builder)
    {
      this.mEnableSavedGames = builder.HasEnableSaveGames();
      this.mInvitationDelegate = builder.GetInvitationDelegate();
      this.mMatchDelegate = builder.GetMatchDelegate();
      this.mPermissionRationale = builder.GetPermissionRationale();
      this.mRequireGooglePlus = builder.HasRequireGooglePlus();
    }

    public bool EnableSavedGames
    {
      get
      {
        return this.mEnableSavedGames;
      }
    }

    public bool RequireGooglePlus
    {
      get
      {
        return this.mRequireGooglePlus;
      }
    }

    public InvitationReceivedDelegate InvitationDelegate
    {
      get
      {
        return this.mInvitationDelegate;
      }
    }

    public MatchDelegate MatchDelegate
    {
      get
      {
        return this.mMatchDelegate;
      }
    }

    public string PermissionRationale
    {
      get
      {
        return this.mPermissionRationale;
      }
    }

    public class Builder
    {
      private InvitationReceivedDelegate mInvitationDelegate = (InvitationReceivedDelegate) ((_param0, _param1) => {});
      private MatchDelegate mMatchDelegate = (MatchDelegate) ((_param0, _param1) => {});
      private bool mEnableSaveGames;
      private bool mRequireGooglePlus;
      private string mRationale;

      public PlayGamesClientConfiguration.Builder EnableSavedGames()
      {
        this.mEnableSaveGames = true;
        return this;
      }

      public PlayGamesClientConfiguration.Builder RequireGooglePlus()
      {
        this.mRequireGooglePlus = true;
        return this;
      }

      public PlayGamesClientConfiguration.Builder WithInvitationDelegate(InvitationReceivedDelegate invitationDelegate)
      {
        this.mInvitationDelegate = Misc.CheckNotNull<InvitationReceivedDelegate>(invitationDelegate);
        return this;
      }

      public PlayGamesClientConfiguration.Builder WithMatchDelegate(MatchDelegate matchDelegate)
      {
        this.mMatchDelegate = Misc.CheckNotNull<MatchDelegate>(matchDelegate);
        return this;
      }

      public PlayGamesClientConfiguration.Builder WithPermissionRationale(string rationale)
      {
        this.mRationale = rationale;
        return this;
      }

      public PlayGamesClientConfiguration Build()
      {
        this.mRequireGooglePlus = GameInfo.RequireGooglePlus();
        return new PlayGamesClientConfiguration(this);
      }

      internal bool HasEnableSaveGames()
      {
        return this.mEnableSaveGames;
      }

      internal bool HasRequireGooglePlus()
      {
        return this.mRequireGooglePlus;
      }

      internal MatchDelegate GetMatchDelegate()
      {
        return this.mMatchDelegate;
      }

      internal InvitationReceivedDelegate GetInvitationDelegate()
      {
        return this.mInvitationDelegate;
      }

      internal string GetPermissionRationale()
      {
        return this.mRationale;
      }
    }
  }
}
