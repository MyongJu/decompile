﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.BasicApi.UIStatus
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace GooglePlayGames.BasicApi
{
  public enum UIStatus
  {
    LeftRoom = -18, // -0x00000012
    UiBusy = -12, // -0x0000000C
    UserClosedUI = -6,
    Timeout = -5,
    VersionUpdateRequired = -4,
    NotAuthorized = -3,
    InternalError = -2,
    Valid = 1,
  }
}
