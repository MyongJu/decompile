﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.GameInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace GooglePlayGames
{
  public static class GameInfo
  {
    private const string UnescapedApplicationId = "APP_ID";
    private const string UnescapedIosClientId = "IOS_CLIENTID";
    private const string UnescapedWebClientId = "WEB_CLIENTID";
    private const string UnescapedNearbyServiceId = "NEARBY_SERVICE_ID";
    private const string UnescapedRequireGooglePlus = "REQUIRE_GOOGLE_PLUS";
    public const string ApplicationId = "839054467581";
    public const string IosClientId = "";
    public const string WebClientId = "";
    public const string NearbyConnectionServiceId = "";

    public static bool RequireGooglePlus()
    {
      return false;
    }

    public static bool ApplicationIdInitialized()
    {
      if (!string.IsNullOrEmpty("839054467581"))
        return !"839054467581".Equals(GameInfo.ToEscapedToken("APP_ID"));
      return false;
    }

    public static bool IosClientIdInitialized()
    {
      if (!string.IsNullOrEmpty(string.Empty))
        return !string.Empty.Equals(GameInfo.ToEscapedToken("IOS_CLIENTID"));
      return false;
    }

    public static bool WebClientIdInitialized()
    {
      if (!string.IsNullOrEmpty(string.Empty))
        return !string.Empty.Equals(GameInfo.ToEscapedToken("WEB_CLIENTID"));
      return false;
    }

    public static bool NearbyConnectionsInitialized()
    {
      if (!string.IsNullOrEmpty(string.Empty))
        return !string.Empty.Equals(GameInfo.ToEscapedToken("NEARBY_SERVICE_ID"));
      return false;
    }

    private static string ToEscapedToken(string token)
    {
      return string.Format("__{0}__", (object) token);
    }
  }
}
