﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.Android.TokenResult
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Com.Google.Android.Gms.Common.Api;
using Google.Developers;
using System;

namespace GooglePlayGames.Android
{
  internal class TokenResult : JavaObjWrapper, Result
  {
    public TokenResult(IntPtr ptr)
      : base(ptr)
    {
    }

    public Status getStatus()
    {
      return new Status(this.InvokeCall<IntPtr>(nameof (getStatus), "()Lcom/google/android/gms/common/api/Status;"));
    }

    public string getAccessToken()
    {
      return this.InvokeCall<string>(nameof (getAccessToken), "()Ljava/lang/String;");
    }

    public string getEmail()
    {
      return this.InvokeCall<string>(nameof (getEmail), "()Ljava/lang/String;");
    }

    public string getIdToken()
    {
      return this.InvokeCall<string>(nameof (getIdToken), "()Ljava/lang/String;");
    }
  }
}
