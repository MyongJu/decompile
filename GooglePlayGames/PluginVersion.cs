﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.PluginVersion
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace GooglePlayGames
{
  public class PluginVersion
  {
    public const string VersionKeyCPP = "00911";
    public const string VersionKeyU5 = "00915";
    public const string VersionKey27Patch = "00927a";
    public const string VersionKeyJarResolver = "00928";
    public const string VersionKeyNativeCRM = "00930";
    public const string VersionKeyJNIStats = "00934";
    public const int VersionInt = 2356;
    public const string VersionString = "0.9.34";
    public const string VersionKey = "00934";
    public const int MinGmsCoreVersionCode = 8487000;
    public const string PlayServicesVersionConstraint = "8.4.0";
  }
}
