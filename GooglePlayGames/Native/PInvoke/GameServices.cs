﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.Native.PInvoke.GameServices
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;
using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.PInvoke
{
  internal class GameServices : BaseReferenceHolder
  {
    internal GameServices(IntPtr selfPointer)
      : base(selfPointer)
    {
    }

    internal bool IsAuthenticated()
    {
      return GooglePlayGames.Native.Cwrapper.GameServices.GameServices_IsAuthorized(this.SelfPtr());
    }

    internal void SignOut()
    {
      GooglePlayGames.Native.Cwrapper.GameServices.GameServices_SignOut(this.SelfPtr());
    }

    internal void StartAuthorizationUI()
    {
      GooglePlayGames.Native.Cwrapper.GameServices.GameServices_StartAuthorizationUI(this.SelfPtr());
    }

    public AchievementManager AchievementManager()
    {
      return new AchievementManager(this);
    }

    public LeaderboardManager LeaderboardManager()
    {
      return new LeaderboardManager(this);
    }

    public PlayerManager PlayerManager()
    {
      return new PlayerManager(this);
    }

    public StatsManager StatsManager()
    {
      return new StatsManager(this);
    }

    internal HandleRef AsHandle()
    {
      return this.SelfPtr();
    }

    protected override void CallDispose(HandleRef selfPointer)
    {
      GooglePlayGames.Native.Cwrapper.GameServices.GameServices_Dispose(selfPointer);
    }

    internal void FetchServerAuthCode(string server_client_id, Action<GameServices.FetchServerAuthCodeResponse> callback)
    {
      Misc.CheckNotNull<Action<GameServices.FetchServerAuthCodeResponse>>(callback);
      Misc.CheckNotNull<string>(server_client_id);
      GooglePlayGames.Native.Cwrapper.GameServices.GameServices_FetchServerAuthCode(this.AsHandle(), server_client_id, new GooglePlayGames.Native.Cwrapper.GameServices.FetchServerAuthCodeCallback(GameServices.InternalFetchServerAuthCodeCallback), Callbacks.ToIntPtr<GameServices.FetchServerAuthCodeResponse>(callback, new Func<IntPtr, GameServices.FetchServerAuthCodeResponse>(GameServices.FetchServerAuthCodeResponse.FromPointer)));
    }

    [MonoPInvokeCallback(typeof (GooglePlayGames.Native.Cwrapper.GameServices.FetchServerAuthCodeCallback))]
    private static void InternalFetchServerAuthCodeCallback(IntPtr response, IntPtr data)
    {
      Callbacks.PerformInternalCallback("GameServices#InternalFetchServerAuthCodeCallback", Callbacks.Type.Temporary, response, data);
    }

    internal class FetchServerAuthCodeResponse : BaseReferenceHolder
    {
      internal FetchServerAuthCodeResponse(IntPtr selfPointer)
        : base(selfPointer)
      {
      }

      internal CommonErrorStatus.ResponseStatus Status()
      {
        return GooglePlayGames.Native.Cwrapper.GameServices.GameServices_FetchServerAuthCodeResponse_GetStatus(this.SelfPtr());
      }

      internal string Code()
      {
        return PInvokeUtilities.OutParamsToString((PInvokeUtilities.OutStringMethod) ((out_string, out_size) => GooglePlayGames.Native.Cwrapper.GameServices.GameServices_FetchServerAuthCodeResponse_GetCode(this.SelfPtr(), out_string, out_size)));
      }

      protected override void CallDispose(HandleRef selfPointer)
      {
        GooglePlayGames.Native.Cwrapper.GameServices.GameServices_FetchServerAuthCodeResponse_Dispose(selfPointer);
      }

      internal static GameServices.FetchServerAuthCodeResponse FromPointer(IntPtr pointer)
      {
        if (pointer.Equals((object) IntPtr.Zero))
          return (GameServices.FetchServerAuthCodeResponse) null;
        return new GameServices.FetchServerAuthCodeResponse(pointer);
      }
    }
  }
}
