﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.Native.PInvoke.MultiplayerInvitation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;
using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.PInvoke
{
  internal class MultiplayerInvitation : BaseReferenceHolder
  {
    internal MultiplayerInvitation(IntPtr selfPointer)
      : base(selfPointer)
    {
    }

    internal MultiplayerParticipant Inviter()
    {
      MultiplayerParticipant multiplayerParticipant = new MultiplayerParticipant(GooglePlayGames.Native.Cwrapper.MultiplayerInvitation.MultiplayerInvitation_InvitingParticipant(this.SelfPtr()));
      if (multiplayerParticipant.Valid())
        return multiplayerParticipant;
      multiplayerParticipant.Dispose();
      return (MultiplayerParticipant) null;
    }

    internal uint Variant()
    {
      return GooglePlayGames.Native.Cwrapper.MultiplayerInvitation.MultiplayerInvitation_Variant(this.SelfPtr());
    }

    internal Types.MultiplayerInvitationType Type()
    {
      return GooglePlayGames.Native.Cwrapper.MultiplayerInvitation.MultiplayerInvitation_Type(this.SelfPtr());
    }

    internal string Id()
    {
      return PInvokeUtilities.OutParamsToString((PInvokeUtilities.OutStringMethod) ((out_string, size) => GooglePlayGames.Native.Cwrapper.MultiplayerInvitation.MultiplayerInvitation_Id(this.SelfPtr(), out_string, size)));
    }

    protected override void CallDispose(HandleRef selfPointer)
    {
      GooglePlayGames.Native.Cwrapper.MultiplayerInvitation.MultiplayerInvitation_Dispose(selfPointer);
    }

    internal uint AutomatchingSlots()
    {
      return GooglePlayGames.Native.Cwrapper.MultiplayerInvitation.MultiplayerInvitation_AutomatchingSlotsAvailable(this.SelfPtr());
    }

    internal uint ParticipantCount()
    {
      return GooglePlayGames.Native.Cwrapper.MultiplayerInvitation.MultiplayerInvitation_Participants_Length(this.SelfPtr()).ToUInt32();
    }

    private static Invitation.InvType ToInvType(Types.MultiplayerInvitationType invitationType)
    {
      switch (invitationType)
      {
        case Types.MultiplayerInvitationType.TURN_BASED:
          return Invitation.InvType.TurnBased;
        case Types.MultiplayerInvitationType.REAL_TIME:
          return Invitation.InvType.RealTime;
        default:
          Logger.d("Found unknown invitation type: " + (object) invitationType);
          return Invitation.InvType.Unknown;
      }
    }

    internal Invitation AsInvitation()
    {
      Invitation.InvType invType = MultiplayerInvitation.ToInvType(this.Type());
      string invId = this.Id();
      int variant = (int) this.Variant();
      Participant inviter;
      using (MultiplayerParticipant multiplayerParticipant = this.Inviter())
        inviter = multiplayerParticipant != null ? multiplayerParticipant.AsParticipant() : (Participant) null;
      return new Invitation(invType, invId, inviter, variant);
    }

    internal static MultiplayerInvitation FromPointer(IntPtr selfPointer)
    {
      if (PInvokeUtilities.IsNull(selfPointer))
        return (MultiplayerInvitation) null;
      return new MultiplayerInvitation(selfPointer);
    }
  }
}
