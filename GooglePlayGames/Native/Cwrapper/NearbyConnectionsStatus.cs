﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.Native.Cwrapper.NearbyConnectionsStatus
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace GooglePlayGames.Native.Cwrapper
{
  internal static class NearbyConnectionsStatus
  {
    internal enum InitializationStatus
    {
      ERROR_VERSION_UPDATE_REQUIRED = -4,
      ERROR_INTERNAL = -2,
      VALID = 1,
    }
  }
}
