﻿// Decompiled with JetBrains decompiler
// Type: QuestDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class QuestDlg : UI.Dialog
{
  private int _selectedIndex = -1;
  private const string food_vfxPath = "Prefab/VFX/fx_acquire_wheat";
  private const string wood_vfxPath = "Prefab/VFX/fx_acquire_wood";
  private const string ore_vfxPath = "Prefab/VFX/fx_acquire_mineral";
  private const string silver_vfxPath = "Prefab/VFX/fx_acquire_coin";
  public QuestItemRenderer[] commonQuests;
  public QuestItemRenderer recommendQuest;
  public UIButton empireSelctedBt;
  public UIButton empireNormalBt;
  public UIButton dailySelctedBt;
  public UIButton dailyNormalBt;
  public UIButton allianceSelctedBt;
  public UIButton allianceNormalBt;
  public UIButton vipSelctedBt;
  public UIButton vipNormalBt;
  public GameObject recommendContent;
  public GameObject commonContent;
  public GameObject recommendFinish;
  public GameObject commonFinish;
  public GameObject empireContent;
  public GameObject allianceContent;
  public AllianceQuestDlg allianceQuestDlg;
  public UILabel title;
  private IVfx foodEffect;
  private IVfx woodEffect;
  private IVfx oreEffect;
  private IVfx silverEffect;

  public int SelectedIndex
  {
    get
    {
      return this._selectedIndex;
    }
    set
    {
      if (this._selectedIndex == value)
        return;
      this._selectedIndex = value;
      this.ResetButton();
      this.UpdateUI();
    }
  }

  private void ResetButton()
  {
    this.SetItemVisiable((MonoBehaviour) this.empireSelctedBt, true);
    this.SetItemVisiable((MonoBehaviour) this.empireNormalBt, false);
    this.SetItemVisiable((MonoBehaviour) this.dailySelctedBt, false);
    this.SetItemVisiable((MonoBehaviour) this.dailyNormalBt, true);
    this.SetItemVisiable((MonoBehaviour) this.allianceSelctedBt, false);
    this.SetItemVisiable((MonoBehaviour) this.allianceNormalBt, true);
    this.SetItemVisiable((MonoBehaviour) this.vipSelctedBt, false);
    this.SetItemVisiable((MonoBehaviour) this.vipNormalBt, true);
    if (this.SelectedIndex > 0)
    {
      this.SetItemVisiable((MonoBehaviour) this.empireSelctedBt, false);
      this.SetItemVisiable((MonoBehaviour) this.empireNormalBt, true);
    }
    NGUITools.SetActive(this.empireContent, false);
    NGUITools.SetActive(this.allianceContent, false);
    switch (this._selectedIndex)
    {
      case 0:
        NGUITools.SetActive(this.empireContent, true);
        NGUITools.SetActive(this.allianceContent, false);
        break;
      case 1:
        this.SetItemVisiable((MonoBehaviour) this.dailySelctedBt, true);
        this.SetItemVisiable((MonoBehaviour) this.dailyNormalBt, false);
        break;
      case 2:
        this.SetItemVisiable((MonoBehaviour) this.allianceSelctedBt, true);
        this.SetItemVisiable((MonoBehaviour) this.allianceNormalBt, false);
        NGUITools.SetActive(this.empireContent, false);
        NGUITools.SetActive(this.allianceContent, true);
        break;
      case 3:
        this.SetItemVisiable((MonoBehaviour) this.vipSelctedBt, true);
        this.SetItemVisiable((MonoBehaviour) this.vipNormalBt, false);
        break;
    }
  }

  private void Start()
  {
  }

  private void CreateFakeDatas()
  {
    if (ConfigManager.inst.DB_AllianceQuest == null)
      ConfigManager.inst.DB_AllianceQuest = new ConfigAllianceQuest();
    for (int index = 0; index < 5; ++index)
    {
      int longId = (int) ClientIdCreater.CreateLongId(ClientIdCreater.IdType.ITEM);
      ConfigManager.inst.DB_AllianceQuest.CreateFakeData(longId);
      DBManager.inst.DB_Local_TimerQuest.CreateFakeData(longId, TimerQuestData.QuestStatus.STATUS_ACTIVE);
    }
  }

  private void SetItemVisiable(MonoBehaviour item, bool visable = false)
  {
    NGUITools.SetActive(item.gameObject, visable);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.StopParticle();
    this.AddEventHandler();
    if (this.SelectedIndex >= 0)
      return;
    this.SelectedIndex = 0;
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this._selectedIndex = -1;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.Clear();
    this.allianceQuestDlg.Clear();
    this.StopParticle();
    this.DestroyEffect();
    base.OnClose(orgParam);
  }

  private void DestroyEffect()
  {
    if (this.foodEffect != null)
      VfxManager.Instance.Delete(this.foodEffect.Guid);
    if (this.woodEffect != null)
      VfxManager.Instance.Delete(this.woodEffect.Guid);
    if (this.oreEffect != null)
      VfxManager.Instance.Delete(this.oreEffect.Guid);
    if (this.silverEffect != null)
      VfxManager.Instance.Delete(this.silverEffect.Guid);
    this.foodEffect = (IVfx) null;
    this.woodEffect = (IVfx) null;
    this.oreEffect = (IVfx) null;
    this.silverEffect = (IVfx) null;
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Quest.onDataRemoved += new System.Action<long>(this.Refresh);
    PlayerData.inst.QuestManager.OnQuestClaimed += new System.Action<QuestRewardAgent>(this.ClaimHandler);
  }

  private void Refresh(long id)
  {
    this.UpdateUI();
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Quest.onDataRemoved -= new System.Action<long>(this.Refresh);
    PlayerData.inst.QuestManager.OnQuestClaimed -= new System.Action<QuestRewardAgent>(this.ClaimHandler);
  }

  private void Clear()
  {
    for (int index = 0; index < this.commonQuests.Length; ++index)
      this.commonQuests[index].Clear();
    this.recommendQuest.Clear();
  }

  private void UpdateUI()
  {
    switch (this.SelectedIndex)
    {
      case 0:
        this.RefreshEmpireQuest();
        break;
      case 2:
        this.RefreshAllainceQuest();
        break;
    }
  }

  private void RefreshAllainceQuest()
  {
    this.title.text = ScriptLocalization.Get("id_uppercase_alliance_quests", true);
    this.allianceQuestDlg.Clear();
    this.allianceQuestDlg.StartLoadAllainceQuest();
  }

  private void RefreshEmpireQuest()
  {
    if (DBManager.inst.DB_Quest.RecommendQuest == null)
    {
      NGUITools.SetActive(this.recommendFinish, true);
      NGUITools.SetActive(this.recommendContent, false);
    }
    else
    {
      NGUITools.SetActive(this.recommendFinish, false);
      NGUITools.SetActive(this.recommendContent, true);
      this.recommendQuest.SetData(DBManager.inst.DB_Quest.RecommendQuest.QuestID);
    }
    this.title.text = ScriptLocalization.Get("id_uppercase_quests", true);
    List<QuestData2> commonQuests = DBManager.inst.DB_Quest.CommonQuests;
    if (commonQuests == null || commonQuests.Count == 0)
    {
      NGUITools.SetActive(this.commonFinish, true);
      NGUITools.SetActive(this.commonContent, false);
    }
    else
    {
      NGUITools.SetActive(this.commonFinish, false);
      NGUITools.SetActive(this.commonContent, true);
      for (int index = 0; index < this.commonQuests.Length; ++index)
      {
        QuestItemRenderer commonQuest = this.commonQuests[index];
        if (index < commonQuests.Count)
        {
          NGUITools.SetActive(commonQuest.gameObject, true);
          commonQuest.SetData(commonQuests[index].QuestID);
        }
        else
          NGUITools.SetActive(commonQuest.gameObject, false);
      }
    }
  }

  public void SelectedEmpireQuest()
  {
    this.SelectedIndex = 0;
  }

  public void SelectedDailyQuest()
  {
    Utils.PopUpCommingSoon();
  }

  public void SelectedAllainceQuest()
  {
    Utils.PopUpCommingSoon();
  }

  public void SelectedVIPQuest()
  {
    Utils.PopUpCommingSoon();
  }

  private void StopParticle()
  {
    if (this.foodEffect != null)
      this.foodEffect.Stop();
    if (this.woodEffect != null)
      this.woodEffect.Stop();
    if (this.oreEffect != null)
      this.oreEffect.Stop();
    if (this.silverEffect == null)
      return;
    this.silverEffect.Stop();
  }

  private void ClaimHandler(QuestRewardAgent agent)
  {
    this.StopParticle();
    if (agent.FoodRewardMax())
    {
      if (this.foodEffect == null)
        this.foodEffect = VfxManager.Instance.Create("Prefab/VFX/fx_acquire_wheat");
      this.foodEffect.Play(this.transform);
    }
    if (agent.WoodRewardMax())
    {
      if (this.woodEffect == null)
        this.woodEffect = VfxManager.Instance.Create("Prefab/VFX/fx_acquire_wood");
      this.woodEffect.Play(this.transform);
    }
    if (agent.OreRewardMax())
    {
      if (this.oreEffect == null)
        this.oreEffect = VfxManager.Instance.Create("Prefab/VFX/fx_acquire_mineral");
      this.oreEffect.Play(this.transform);
    }
    if (agent.SilverRewardMax())
    {
      if (this.silverEffect == null)
        this.silverEffect = VfxManager.Instance.Create("Prefab/VFX/fx_acquire_coin");
      this.silverEffect.Play(this.transform);
    }
    this.UpdateUI();
  }
}
