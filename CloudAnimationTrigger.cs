﻿// Decompiled with JetBrains decompiler
// Type: CloudAnimationTrigger
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CloudAnimationTrigger : MonoBehaviour
{
  public System.Action OnStartHandler;
  public System.Action OnEndHandler;

  private void Start()
  {
  }

  public void OnAnimationStart()
  {
    if (this.OnStartHandler == null)
      return;
    this.OnStartHandler();
  }

  public void OnAnimationEnd()
  {
    if (this.OnEndHandler == null)
      return;
    this.OnEndHandler();
  }
}
