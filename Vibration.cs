﻿// Decompiled with JetBrains decompiler
// Type: Vibration
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class Vibration
{
  public static AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
  public static AndroidJavaObject currentActivity = Vibration.unityPlayer.GetStatic<AndroidJavaObject>(nameof (currentActivity));
  public static AndroidJavaObject vibrator = Vibration.currentActivity.Call<AndroidJavaObject>("getSystemService", new object[1]
  {
    (object) nameof (vibrator)
  });

  public static void Vibrate()
  {
    try
    {
      Vibration.Vibrate(500L);
    }
    catch
    {
    }
  }

  public static void Vibrate(long milliseconds)
  {
    Vibration.vibrator.Call("vibrate", new object[1]
    {
      (object) milliseconds
    });
  }
}
