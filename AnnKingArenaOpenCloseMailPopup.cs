﻿// Decompiled with JetBrains decompiler
// Type: AnnKingArenaOpenCloseMailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;

public class AnnKingArenaOpenCloseMailPopup : BaseReportPopup
{
  private const string HEAD_PATH = "Texture/GUI_Textures/mail_report_winner";
  private const string BOSS_BANNER_PATH = "Texture/AllianceBoss/world_boss";
  public UITable contentTable;
  public UILabel actionBtnLabel;
  public UILabel messageLabel;
  public UIButton actionButton;
  private SystemMessage systemMessage;

  private void UpdateUI()
  {
    this.LoadHeader();
    this.UpdateMessage();
    this.UpdateButtonDisplay();
    this.RegistEventHandler();
    this.RepositionContentTable();
  }

  private void LoadHeader()
  {
    string path = "Texture/GUI_Textures/mail_report_winner";
    if (this.maildata.type == MailType.MAIL_TYPE_WORLD_BOSS_START)
      path = "Texture/AllianceBoss/world_boss";
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, path, (System.Action<bool>) null, true, true, string.Empty);
  }

  private void UpdateMessage()
  {
    this.messageLabel.text = this.maildata.GetBodyString();
  }

  private void UpdateButtonDisplay()
  {
    MailType type = this.maildata.type;
    switch (type)
    {
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_END:
        this.actionBtnLabel.text = Utils.XLAT("event_1_year_tournament_hall_of_champions_name");
        break;
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_START:
        this.actionBtnLabel.text = Utils.XLAT("id_view_details");
        this.UpdateButtonState();
        break;
      default:
        switch (type - 82)
        {
          case MailType.MAIL_TYPE_NORMAL:
            this.actionBtnLabel.text = Utils.XLAT("event_1_year_celebration_1_year_tournament_name");
            this.UpdateButtonState();
            return;
          case MailType.MAIL_TYPE_PVP_BATTLE_REPORT:
            return;
          case MailType.MAIL_TYPE_PVP_SCOUT_REPORT:
            this.actionBtnLabel.text = Utils.XLAT("pve_monster_uppercase_attack");
            return;
          default:
            return;
        }
    }
  }

  private void UpdateButtonState()
  {
    if (this.maildata.type != MailType.MAIL_TYPE_WORLD_BOSS_START)
      ;
    if ((long) NetServerTime.inst.ServerTimestamp >= AnniversaryIapPayload.Instance.TournamentShowTime && (long) NetServerTime.inst.ServerTimestamp < AnniversaryIapPayload.Instance.EndTime)
      this.actionButton.isEnabled = true;
    else
      this.actionButton.isEnabled = false;
  }

  private void RepositionContentTable()
  {
    this.contentTable.repositionNow = true;
  }

  private void OnKingArenaBtnClicked()
  {
    this.CloseCurrentPopup();
    UIManager.inst.OpenDlg("Anniversary/AnniversaryMainDlg", (UI.Dialog.DialogParameter) new AnniversaryMainDlg.Parameter()
    {
      catalogType = AnniversaryCatalog.CatalogType.TOURNAMENT
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private void OnFinalKingBtnClicked()
  {
    this.CloseCurrentPopup();
    UIManager.inst.OpenDlg("Anniversary/AnniversaryOverDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnGotoViewBoss()
  {
    this.CloseCurrentPopup();
    if (this.systemMessage == null || this.systemMessage.HtBody == null || (!this.systemMessage.HtBody.ContainsKey((object) "data") || this.systemMessage.HtBody[(object) "data"] == null))
      return;
    int result1 = -1;
    int result2 = -1;
    Hashtable hashtable = this.systemMessage.HtBody[(object) "data"] as Hashtable;
    if (hashtable != null && hashtable.ContainsKey((object) "x") && hashtable[(object) "x"] != null)
      int.TryParse(hashtable[(object) "x"].ToString(), out result1);
    if (hashtable != null && hashtable.ContainsKey((object) "y") && hashtable[(object) "y"] != null)
      int.TryParse(hashtable[(object) "y"].ToString(), out result2);
    if (result1 <= -1 || result2 <= -1)
      return;
    if (!MapUtils.CanGotoTarget(PlayerData.inst.userData.world_id))
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    }
    else
    {
      PVPMap.PendingGotoRequest = new Coordinate(PlayerData.inst.userData.world_id, result1, result2);
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
        CitadelSystem.inst.OnLoadWorldMap();
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }

  private void OnCheckDetailBtnClicked()
  {
    this.CloseCurrentPopup();
    UIManager.inst.OpenDlg("Anniversary/AnniversaryMainDlg", (UI.Dialog.DialogParameter) new AnniversaryMainDlg.Parameter()
    {
      catalogType = AnniversaryCatalog.CatalogType.IAP_PACKAGE
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private void CloseCurrentPopup()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void RegistEventHandler()
  {
    this.actionButton.onClick.Clear();
    MailType type = this.maildata.type;
    switch (type)
    {
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_END:
        EventDelegate.Add(this.actionButton.onClick, new EventDelegate.Callback(this.OnFinalKingBtnClicked));
        break;
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_START:
        EventDelegate.Add(this.actionButton.onClick, new EventDelegate.Callback(this.OnCheckDetailBtnClicked));
        break;
      default:
        switch (type - 82)
        {
          case MailType.MAIL_TYPE_NORMAL:
            EventDelegate.Add(this.actionButton.onClick, new EventDelegate.Callback(this.OnKingArenaBtnClicked));
            return;
          case MailType.MAIL_TYPE_PVP_BATTLE_REPORT:
            return;
          case MailType.MAIL_TYPE_PVP_SCOUT_REPORT:
            EventDelegate.Add(this.actionButton.onClick, new EventDelegate.Callback(this.OnGotoViewBoss));
            return;
          default:
            return;
        }
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    BaseReportPopup.Parameter parameter = orgParam as BaseReportPopup.Parameter;
    if (parameter != null)
      this.systemMessage = parameter.mail as SystemMessage;
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.OnClose(orgParam);
    this.CancelInvoke();
  }
}
