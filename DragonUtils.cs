﻿// Decompiled with JetBrains decompiler
// Type: DragonUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public static class DragonUtils
{
  public static readonly string[] DRAGON_TENDENCY_SKILL_IDS = new string[5]
  {
    "dark_2",
    "dark_1",
    "normal",
    "light_1",
    "light_2"
  };
  public const string TENDENCY_DARK2 = "dark_2";
  public const string TENDENCY_DARK1 = "dark_1";
  public const string TENDENCY_NORMAL = "normal";
  public const string TENDENCY_LIGHT1 = "light_1";
  public const string TENDENCY_LIGHT2 = "light_2";

  public static DragonData.Tendency GetTendency(string formationId)
  {
    string key = formationId;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DragonUtils.\u003C\u003Ef__switch\u0024map52 == null)
      {
        // ISSUE: reference to a compiler-generated field
        DragonUtils.\u003C\u003Ef__switch\u0024map52 = new Dictionary<string, int>(5)
        {
          {
            "dark_2",
            0
          },
          {
            "dark_1",
            1
          },
          {
            "normal",
            2
          },
          {
            "light_1",
            3
          },
          {
            "light_2",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DragonUtils.\u003C\u003Ef__switch\u0024map52.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return DragonData.Tendency.ferocious;
          case 1:
            return DragonData.Tendency.swift;
          case 2:
            return DragonData.Tendency.normal;
          case 3:
            return DragonData.Tendency.pure;
          case 4:
            return DragonData.Tendency.shine;
        }
      }
    }
    return DragonData.Tendency.normal;
  }

  public static void GetFormationRequirement(string formationId, int dragonLevel, out int dark, out int light)
  {
    dark = light = 0;
    DragonInfo dragonInfoByLevel = ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(dragonLevel);
    if (dragonInfoByLevel == null)
      return;
    if (formationId == "dark_2")
      dark = dragonInfoByLevel.tendency_dark_2_min;
    else if (formationId == "dark_1")
    {
      dark = dragonInfoByLevel.tendency_dark_1_min;
    }
    else
    {
      if (formationId == "normal")
        return;
      if (formationId == "light_1")
      {
        light = dragonInfoByLevel.tendency_light_1_min;
      }
      else
      {
        if (!(formationId == "light_2"))
          return;
        light = dragonInfoByLevel.tendency_light_2_min;
      }
    }
  }

  public static void GetFormationRequirement(string formationId, out int dark, out int light)
  {
    dark = light = 0;
    DragonData dragonData = PlayerData.inst.dragonData;
    if (dragonData == null)
      return;
    DragonUtils.GetFormationRequirement(formationId, dragonData.Level, out dark, out light);
  }

  public static bool IsFormationEnabled(string formationId)
  {
    DragonData dragonData = PlayerData.inst.dragonData;
    if (dragonData == null)
      return false;
    int dark;
    int light;
    DragonUtils.GetFormationRequirement(formationId, out dark, out light);
    if (dragonData.Skill_cost.totalDarkPoint >= dark)
      return dragonData.Skill_cost.totalLightPoint >= light;
    return false;
  }

  public static string GetDragonTendencySkillID(DragonData dragonData)
  {
    int tendencySkillIdIndex = DragonUtils.GetDragonTendencySkillIDIndex(dragonData);
    return DragonUtils.DRAGON_TENDENCY_SKILL_IDS[tendencySkillIdIndex];
  }

  public static int GetDragonTendencySkillCount()
  {
    return DragonUtils.DRAGON_TENDENCY_SKILL_IDS.Length;
  }

  public static int GetDragonTendencySkillIDIndex(DragonData dragonData)
  {
    string tendencySkillId = ConfigManager.inst.DB_ConfigDragonTendencyBoost.GetTendencySkillID(dragonData.tendency);
    for (int index = 0; index < DragonUtils.DRAGON_TENDENCY_SKILL_IDS.Length; ++index)
    {
      if (tendencySkillId == DragonUtils.DRAGON_TENDENCY_SKILL_IDS[index])
        return index;
    }
    return 2;
  }

  public static string GetDragonModelPath(int character, int level, int lodLevel = 0)
  {
    string str1 = (string) null;
    DragonInfo dragonInfoByLevel = ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(level);
    if (dragonInfoByLevel != null)
    {
      int age = dragonInfoByLevel.age;
      if (DragonConstants.DragonPostfixDict.TryGetValue(character, out str1))
      {
        string str2 = string.Format("Prefab/Dragon/dragon/lv{0}/dragon_lv{0}_{1}", (object) age, (object) str1);
        if (lodLevel > 0)
          str2 += "_high";
        return str2;
      }
    }
    return "Prefab/Dragon/dragon/dragon_lv3_green";
  }

  public static Color GetDragonColor(int character, int level)
  {
    return Color.black;
  }

  public static string GetDragonImagePath(int character, int level)
  {
    return "missing";
  }

  public static string GetDragonIconPath(int character, int level)
  {
    return "Texture/Dragon/icon_dragon_portrait_" + (object) ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(level).age;
  }

  public static DragonViewData GetDragonViewData(int character, int level, int lodLevel = 0)
  {
    DragonViewData dragonViewData = new DragonViewData();
    dragonViewData.mainAsset = DragonUtils.GetDragonModelPath(character, level, lodLevel);
    dragonViewData.color = DragonUtils.GetDragonColor(character, level);
    return dragonViewData;
  }

  public static DragonViewData GetDragonViewData(DragonData data, int lodLevel = 0)
  {
    if (data == null)
      return (DragonViewData) null;
    return DragonUtils.GetDragonViewData((int) data.tendency, data.Level, lodLevel);
  }

  public static List<IconData> GetSkillIconData(string skillType, long dragonId, bool useArtifact = true)
  {
    List<IconData> iconDataList = new List<IconData>();
    DragonData dragonData = DBManager.inst.DB_Dragon.Get(dragonId);
    if (dragonData == null)
      return iconDataList;
    List<int> skillBar = dragonData.Skills.GetSkillBar(skillType);
    if (skillBar == null)
      return iconDataList;
    for (int index = 0; index < skillBar.Count; ++index)
    {
      if ((long) skillBar[index] != 0L)
      {
        ConfigDragonSkillMainInfo skillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo(skillBar[index]);
        ConfigDragonSkillGroupInfo skillGroupBySkillId = ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupBySkillId(skillBar[index]);
        string str = string.Empty;
        if (useArtifact)
        {
          int extarLevel = DBManager.inst.DB_Artifact.GetExtarLevel(dragonData.Uid, skillGroupBySkillId.internalId);
          str = extarLevel <= 0 ? string.Empty : "+" + (object) extarLevel;
        }
        int skillGroupStarLevel = dragonData.GetSkillGroupStarLevel(skillGroupBySkillId.internalId);
        bool[] visibles = new bool[1]
        {
          skillGroupStarLevel > 0
        };
        IconData iconData = new IconData(skillGroupBySkillId.IconPath, visibles, new string[3]
        {
          skillMainInfo.level.ToString(),
          str,
          skillGroupStarLevel.ToString()
        });
        iconDataList.Add(iconData);
      }
    }
    return iconDataList;
  }
}
