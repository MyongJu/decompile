﻿// Decompiled with JetBrains decompiler
// Type: StoreBuyAndUseDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using UI;
using UnityEngine;

public class StoreBuyAndUseDialog : Popup
{
  [NonSerialized]
  public string WarningContent = string.Empty;
  private Color32 GOLD_COLOR = new Color32((byte) 246, (byte) 203, (byte) 1, byte.MaxValue);
  private int Count = 5;
  public UILabel m_ItemName;
  public UILabel m_Description;
  public UILabel m_Price;
  public UILabel m_OwnCount;
  public UIButton m_UseButton;
  public UITexture m_Icon;
  public Texture2D m_MissingIcon;
  public UILabel m_totalPrice;
  [NonSerialized]
  public bool IsShowWarning;
  public System.Action<bool, object> onUsecallBackFun;
  private ShopStaticInfo info;

  public void OnShowPopup()
  {
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnConsumableCountChanged);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnConsumableCountChanged);
  }

  public void OnClosePopup()
  {
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnConsumableCountChanged);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnConsumableCountChanged);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    StoreBuyAndUseDialog.StoreBuyAndUseDialogParamer useDialogParamer = orgParam as StoreBuyAndUseDialog.StoreBuyAndUseDialogParamer;
    this.info = useDialogParamer.shopStaticInfo;
    this.onUsecallBackFun = useDialogParamer.onUsecallBackFun;
    this.UpdateNewUI();
    this.OnShowPopup();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.OnClosePopup();
    BuilderFactory.Instance.Release((UIWidget) this.m_Icon);
  }

  private void OnConsumableCountChanged(int itemId)
  {
    this.UpdateNewUI();
  }

  public void OnCloseBtnPress()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnBuy5()
  {
    DBManager.inst.DB_User.Get(PlayerData.inst.uid);
    if (!ItemBag.Instance.CheckCanBuyShopItem(this.info.ID, true))
      return;
    ItemBag.Instance.BuyShopItem(this.info.internalId, this.Count, (Hashtable) null, (System.Action<bool, object>) null);
  }

  public void OnBuy()
  {
    DBManager.inst.DB_User.Get(PlayerData.inst.uid);
    if (!ItemBag.Instance.CheckCanBuyShopItem(this.info.ID, true))
      return;
    ItemBag.Instance.BuyShopItem(this.info.internalId, 1, (Hashtable) null, (System.Action<bool, object>) null);
  }

  public void OnUse()
  {
    if (!this.IsShowWarning)
    {
      ItemBag.Instance.UseItem(this.info.Item_InternalId, 1, (Hashtable) null, this.onUsecallBackFun);
    }
    else
    {
      string str1 = ScriptLocalization.Get("shop_buy_yesbtlabel", true);
      string str2 = ScriptLocalization.Get("shop_buy_nobtlabel", true);
      string str3 = ScriptLocalization.Get("boost_replace_title", true);
      UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
      {
        setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
        title = str3,
        leftButtonText = str2,
        leftButtonClickEvent = (System.Action) null,
        rightButtonText = str1,
        rightButtonClickEvent = new System.Action(this.BackConfirm),
        description = this.WarningContent
      });
    }
  }

  private void BackConfirm()
  {
    ItemBag.Instance.UseItem(this.info.Item_InternalId, 1, (Hashtable) null, (System.Action<bool, object>) null);
  }

  private void UpdateNewUI()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.info.Item_InternalId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, string.Format("Texture/ItemIcons/{0}", (object) this.info.Image), (System.Action<bool>) null, true, false, string.Empty);
    this.m_ItemName.text = ScriptLocalization.Get(itemStaticInfo.Loc_Name_Id, true);
    this.m_Description.text = ScriptLocalization.Get(itemStaticInfo.Loc_Description_Id, true);
    this.m_Price.text = ItemBag.Instance.GetShopItemPrice(this.info.ID).ToString() + string.Empty;
    if ((UnityEngine.Object) this.m_totalPrice != (UnityEngine.Object) null)
      this.m_totalPrice.text = (ItemBag.Instance.GetShopItemPrice(this.info.ID) * this.Count).ToString() + string.Empty;
    uint rgbFromColor32 = Utils.GetRGBFromColor32(this.GOLD_COLOR);
    int shopStaticQuanlity = ItemBag.Instance.GetShopStaticQuanlity(this.info.ID);
    this.m_OwnCount.text = string.Format("Own: [{0}]{1}[-]", (object) rgbFromColor32.ToString("X2"), (object) shopStaticQuanlity);
    if (!((UnityEngine.Object) this.m_UseButton != (UnityEngine.Object) null))
      return;
    if (shopStaticQuanlity > 0 && ItemBag.CanShopItemBeUsedInStore(this.info))
      this.m_UseButton.isEnabled = true;
    else
      this.m_UseButton.isEnabled = false;
  }

  public class StoreBuyAndUseDialogParamer : Popup.PopupParameter
  {
    public ShopStaticInfo shopStaticInfo;
    public System.Action<bool, object> onUsecallBackFun;
  }
}
