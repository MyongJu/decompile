﻿// Decompiled with JetBrains decompiler
// Type: MailContactItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MailContactItem : MonoBehaviour
{
  public UILabel _name;
  public UISprite allianceShield;
  private string _player;
  private bool _alliance;
  public System.Action<string> onClick;

  public string Player
  {
    get
    {
      return this._player;
    }
    set
    {
      this._player = value;
      this._name.text = this._player;
    }
  }

  public bool Alliance
  {
    get
    {
      return this._alliance;
    }
    set
    {
      this._alliance = value;
      if (!((UnityEngine.Object) this.allianceShield != (UnityEngine.Object) null))
        return;
      this.allianceShield.gameObject.SetActive(value);
    }
  }

  public void OnClickItem()
  {
    if (this.onClick == null)
      return;
    this.onClick(this.Player);
  }
}
