﻿// Decompiled with JetBrains decompiler
// Type: FallenKnightReportContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class FallenKnightReportContent
{
  public string k;
  public string x;
  public string y;
  public bool is_attacker;
  public string winner_uid;
  public KnightData attacker;
  public TroopLeader defender;
  public List<TroopDetail> defender_troop_detail;
  public Dictionary<string, long> npc_score;
  public Dictionary<string, string> kill_rate;

  public bool IsWinner()
  {
    return this.winner_uid == this.defender.uid;
  }

  public void SummarizeAllTroopDetail(List<TroopDetail> tds, ref Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>> allmytroopdetail)
  {
    if (tds == null)
      return;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        Dictionary<string, List<SoldierInfo.Data>> troopdetail = new Dictionary<string, List<SoldierInfo.Data>>();
        current.SummarizeTroopDetail(ref troopdetail);
        allmytroopdetail.Add(current.name, troopdetail);
      }
    }
  }

  public long CalculateAllTroop(List<TroopDetail> tds)
  {
    long num = 0;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        num += current.CalculateTroop();
      }
    }
    return num;
  }

  public long CalculateAllTroopSurvive(List<TroopDetail> tds)
  {
    long num = 0;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        num += current.CalculateTroopSurvived();
      }
    }
    return num;
  }

  public long CalculateAllTroopWound(List<TroopDetail> tds)
  {
    long num = 0;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        num += current.CalculateTroopWounded();
      }
    }
    return num;
  }

  public long CalculateAllTroopNotGood(List<TroopDetail> tds)
  {
    long num = 0;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        num += current.CalculateTroopLost();
        num += current.CalculateTroopWounded();
      }
    }
    return num;
  }

  public long CalculateAllTroopLost(List<TroopDetail> tds)
  {
    long num = 0;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        num += current.CalculateTroopLost();
      }
    }
    return num;
  }

  public long CalculateAllTroopKill(List<TroopDetail> tds)
  {
    long num = 0;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        num += current.CalculateTroopKill();
      }
    }
    return num;
  }

  public long CalculateAllPowerLost(List<TroopDetail> tds)
  {
    long num = 0;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        num += current.CalculatePowerLost();
      }
    }
    return num;
  }
}
