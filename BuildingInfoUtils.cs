﻿// Decompiled with JetBrains decompiler
// Type: BuildingInfoUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class BuildingInfoUtils
{
  public static Dictionary<string, string> AssetMap = new Dictionary<string, string>()
  {
    {
      "stronghold",
      "BuildingInfo/BuildingInfoStronghold"
    },
    {
      "university",
      "BuildingInfo/UniversityBuildingInfoDlg"
    },
    {
      "watchtower",
      "BuildingInfo/WatchTowerBuildingInfoDlg"
    },
    {
      "barracks",
      "BuildingInfo/TroopBuildingInfoDlg"
    },
    {
      "range",
      "BuildingInfo/TroopBuildingInfoDlg"
    },
    {
      "stables",
      "BuildingInfo/TroopBuildingInfoDlg"
    },
    {
      "workshop",
      "BuildingInfo/TroopBuildingInfoDlg"
    },
    {
      "sanctum",
      "BuildingInfo/TroopBuildingInfoDlg"
    },
    {
      "fortress",
      "BuildingInfo/TrapBuildingInfoDlg"
    },
    {
      "wish_well",
      "BuildingInfo/WishWellBuildingInfoDlg"
    },
    {
      "farm",
      "BuildingInfo/ResourceBuildingInfoDlg"
    },
    {
      "mine",
      "BuildingInfo/ResourceBuildingInfoDlg"
    },
    {
      "house",
      "BuildingInfo/ResourceBuildingInfoDlg"
    },
    {
      "lumber_mill",
      "BuildingInfo/ResourceBuildingInfoDlg"
    },
    {
      "military_tent",
      "BuildingInfo/MilitaryTentBuildingInfoDlg"
    },
    {
      "hospital",
      "BuildingInfo/HospitalBuildingInfoDlg"
    },
    {
      "walls",
      "BuildingInfo/WallBuildingInfoDlg"
    },
    {
      "war_rally",
      "BuildingInfo/WarRallyBuildingInfoDlg"
    },
    {
      "marketplace",
      "BuildingInfo/MarketBuildingInfoDlg"
    },
    {
      "embassy",
      "BuildingInfo/EmbassyBuildingInfoDlg"
    },
    {
      "storehouse",
      "BuildingInfo/StoreHouseBuildingInfoDlg"
    },
    {
      "dragon_lair",
      "BuildingInfo/DragonLiarBuildingInfoDlg"
    },
    {
      "forge",
      "BuildingInfo/ForgeBuildingInfoDlg"
    }
  };
  public static Dictionary<string, BuildingInfoUtils.PrepareDataAction> PrepareDataMap = new Dictionary<string, BuildingInfoUtils.PrepareDataAction>()
  {
    {
      "farm",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareResourceData)
    },
    {
      "lumber_mill",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareResourceData)
    },
    {
      "mine",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareResourceData)
    },
    {
      "house",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareResourceData)
    },
    {
      "hospital",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareHospitalData)
    },
    {
      "military_tent",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareMilitaryTentData)
    },
    {
      "walls",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareWallsData)
    },
    {
      "barracks",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareTroopData)
    },
    {
      "range",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareTroopData)
    },
    {
      "stables",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareTroopData)
    },
    {
      "workshop",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareTroopData)
    },
    {
      "fortress",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareTrapData)
    },
    {
      "university",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareUniversityData)
    },
    {
      "forge",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareForgeData)
    },
    {
      "watchtower",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareWatchTowerData)
    },
    {
      "marketplace",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareMarketData)
    },
    {
      "embassy",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareEmbassyData)
    },
    {
      "war_rally",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareWarRallyData)
    },
    {
      "wish_well",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareWishWellData)
    },
    {
      "dragon_lair",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareDragonLiarData)
    },
    {
      "storehouse",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareStoreHouseData)
    },
    {
      "stronghold",
      new BuildingInfoUtils.PrepareDataAction(BuildingInfoUtils.PrepareStrongHoldData)
    }
  };

  public static string GetAssetName(string buildingType)
  {
    if (BuildingInfoUtils.AssetMap.ContainsKey(buildingType))
      return BuildingInfoUtils.AssetMap[buildingType];
    return "BuildingInfo/BuildingInfoBaseDlg";
  }

  public static void PrepareStoreHouseData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    headerData = new string[6]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      Utils.XLAT("id_food"),
      Utils.XLAT("id_wood"),
      Utils.XLAT("id_ore"),
      Utils.XLAT("id_silver"),
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int valueInt1 = ConfigManager.inst.DB_GameConfig.GetData("food_load").ValueInt;
    int valueInt2 = ConfigManager.inst.DB_GameConfig.GetData("wood_load").ValueInt;
    int valueInt3 = ConfigManager.inst.DB_GameConfig.GetData("ore_load").ValueInt;
    int valueInt4 = ConfigManager.inst.DB_GameConfig.GetData("silver_load").ValueInt;
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(buildingInfo.Type);
    for (int index = 1; index <= maxLevelByType; ++index)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) index);
      int benefitValue1 = (int) data.Benefit_Value_1;
      elementsData.Add(new string[6]
      {
        index.ToString(),
        Utils.FormatThousands((benefitValue1 / valueInt1).ToString()),
        Utils.FormatThousands((benefitValue1 / valueInt2).ToString()),
        Utils.FormatThousands((benefitValue1 / valueInt3).ToString()),
        Utils.FormatThousands((benefitValue1 / valueInt4).ToString()),
        BuildingInfoUtils.GetPowerValueString(data, data.Building_Lvl)
      });
    }
  }

  public static void PrepareResourceData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    PropertyDefinition dbProperty1 = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_1)];
    PropertyDefinition dbProperty2 = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_2)];
    headerData = new string[4]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      dbProperty1.Name,
      dbProperty2.Name,
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(buildingInfo.Type);
    for (int index = 1; index <= maxLevelByType; ++index)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) index);
      elementsData.Add(new string[4]
      {
        index.ToString(),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_1, data.Benefit_Value_1),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_2, data.Benefit_Value_2),
        BuildingInfoUtils.GetPowerValueString(data, data.Building_Lvl)
      });
    }
  }

  public static void PrepareHospitalData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_1)];
    headerData = new string[3]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      dbProperty.Name,
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(buildingInfo.Type);
    for (int index = 1; index <= maxLevelByType; ++index)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) index);
      elementsData.Add(new string[3]
      {
        index.ToString(),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_1, data.Benefit_Value_1),
        BuildingInfoUtils.GetPowerValueString(data, data.Building_Lvl)
      });
    }
  }

  public static void PrepareMilitaryTentData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    PropertyDefinition dbProperty1 = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_1)];
    PropertyDefinition dbProperty2 = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_2)];
    headerData = new string[4]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      dbProperty1.Name,
      dbProperty2.Name,
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(buildingInfo.Type);
    for (int index = 1; index <= maxLevelByType; ++index)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) index);
      elementsData.Add(new string[4]
      {
        index.ToString(),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_1, data.Benefit_Value_1),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_2, data.Benefit_Value_2),
        BuildingInfoUtils.GetPowerValueString(data, data.Building_Lvl)
      });
    }
  }

  public static void PrepareWallsData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    headerData = new string[3]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      Utils.XLAT("upgrade_info_wall_defense_subtitle"),
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(buildingInfo.Type);
    for (int index = 1; index <= maxLevelByType; ++index)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) index);
      elementsData.Add(new string[3]
      {
        index.ToString(),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_1, data.Benefit_Value_1),
        BuildingInfoUtils.GetPowerValueString(data, data.Building_Lvl)
      });
    }
  }

  public static void PrepareTroopData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    headerData = new string[4]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      Utils.XLAT("barracks_building_info_upgrade_bonuses_name"),
      Utils.XLAT("id_benefits"),
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int buildingLevel = 1;
    while (true)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) buildingLevel);
      if (data != null)
      {
        string buildingUnlockUnitName = BarracksManager.Instance.GetBuildingUnlockUnitName(buildingInfo.Type, buildingLevel);
        string str = !data.Benefit_ID_4.Equals(string.Empty) ? BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_4, data.Benefit_Value_4) : string.Empty;
        if (str.Length != 0)
          str = ConfigManager.inst.DB_Properties[int.Parse(data.Benefit_ID_4)].Name + str;
        elementsData.Add(new string[4]
        {
          buildingLevel.ToString(),
          buildingUnlockUnitName,
          str,
          BuildingInfoUtils.GetPowerValueString(data, data.Building_Lvl)
        });
        ++buildingLevel;
      }
      else
        break;
    }
  }

  public static void PrepareTrapData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    headerData = new string[4]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      Utils.XLAT("upgrade_info_trap_capacity_subtitle"),
      Utils.XLAT("barracks_building_info_upgrade_bonuses_name"),
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(buildingInfo.Type);
    for (int buildingLevel = 1; buildingLevel <= maxLevelByType; ++buildingLevel)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) buildingLevel);
      string buildingUnlockUnitName = BarracksManager.Instance.GetBuildingUnlockUnitName(buildingInfo.Type, buildingLevel);
      elementsData.Add(new string[4]
      {
        buildingLevel.ToString(),
        Utils.FormatThousands(data.Benefit_Value_1.ToString()),
        buildingUnlockUnitName,
        BuildingInfoUtils.GetPowerValueString(data, data.Building_Lvl)
      });
    }
  }

  public static void PrepareUniversityData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    headerData = new string[2]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(buildingInfo.Type);
    for (int index = 1; index <= maxLevelByType; ++index)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) index);
      elementsData.Add(new string[2]
      {
        index.ToString(),
        BuildingInfoUtils.GetPowerValueString(data, data.Building_Lvl)
      });
    }
  }

  public static void PrepareForgeData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    PropertyDefinition dbProperty1 = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_1)];
    PropertyDefinition dbProperty2 = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_2)];
    headerData = new string[4]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      dbProperty1.Name,
      dbProperty2.Name,
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(buildingInfo.Type);
    for (int index = 1; index <= maxLevelByType; ++index)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) index);
      elementsData.Add(new string[4]
      {
        index.ToString(),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_1, data.Benefit_Value_1),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_2, data.Benefit_Value_2),
        BuildingInfoUtils.GetPowerValueString(data, data.Building_Lvl)
      });
    }
  }

  public static void PrepareWatchTowerData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    PropertyDefinition dbProperty1 = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_2)];
    PropertyDefinition dbProperty2 = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_3)];
    headerData = new string[5]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      Utils.XLAT("upgrade_info_benefit_subtitle"),
      Utils.XLAT("watchtower_building_info_upgrade_bonuses_name"),
      Utils.XLAT("watchtower_building_info_upgrade_bonuses_2_name"),
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(buildingInfo.Type);
    for (int index = 1; index <= maxLevelByType; ++index)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) index);
      elementsData.Add(new string[5]
      {
        index.ToString(),
        Utils.XLAT("watchtower_ability_level" + (object) index),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_2, data.Benefit_Value_2),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_3, data.Benefit_Value_3),
        BuildingInfoUtils.GetPowerValueString(data, data.Building_Lvl)
      });
    }
  }

  public static void PrepareMarketData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    PropertyDefinition dbProperty1 = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_1)];
    PropertyDefinition dbProperty2 = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_2)];
    headerData = new string[4]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      dbProperty1.Name,
      dbProperty2.Name,
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(buildingInfo.Type);
    for (int index = 1; index <= maxLevelByType; ++index)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) index);
      elementsData.Add(new string[4]
      {
        index.ToString(),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_1, data.Benefit_Value_1),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_2, data.Benefit_Value_2),
        BuildingInfoUtils.GetPowerValueString(data, data.Building_Lvl)
      });
    }
  }

  public static void PrepareEmbassyData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    PropertyDefinition dbProperty1 = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_1)];
    PropertyDefinition dbProperty2 = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_2)];
    PropertyDefinition dbProperty3 = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_3)];
    headerData = new string[5]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      dbProperty1.Name,
      dbProperty2.Name,
      dbProperty3.Name,
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(buildingInfo.Type);
    for (int index = 1; index <= maxLevelByType; ++index)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) index);
      elementsData.Add(new string[5]
      {
        index.ToString(),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_1, data.Benefit_Value_1),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_2, data.Benefit_Value_2),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_3, data.Benefit_Value_3),
        BuildingInfoUtils.GetPowerValueString(data, data.Building_Lvl)
      });
    }
  }

  public static void PrepareWarRallyData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_1)];
    headerData = new string[3]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      dbProperty.Name,
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(buildingInfo.Type);
    for (int index = 1; index <= maxLevelByType; ++index)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) index);
      elementsData.Add(new string[3]
      {
        index.ToString(),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_1, data.Benefit_Value_1),
        BuildingInfoUtils.GetPowerValueString(data, data.Building_Lvl)
      });
    }
  }

  public static void PrepareWishWellData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    headerData = new string[7]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      Utils.XLAT("id_wood"),
      Utils.XLAT("id_food"),
      Utils.XLAT("id_ore"),
      Utils.XLAT("id_silver"),
      Utils.XLAT("id_free_wishes"),
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(buildingInfo.Type);
    for (int level = 1; level <= maxLevelByType; ++level)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) level);
      WishWellData wishWellDataByLevel = ConfigManager.inst.DB_WishWell.GetWishWellDataByLevel(level);
      elementsData.Add(new string[7]
      {
        level.ToString(),
        wishWellDataByLevel.Food.ToString(),
        wishWellDataByLevel.Wood.ToString(),
        wishWellDataByLevel.Ore.ToString(),
        wishWellDataByLevel.Silver.ToString(),
        wishWellDataByLevel.FreeGem.ToString(),
        data.Power.ToString()
      });
    }
  }

  public static void PrepareDragonLiarData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    headerData = new string[4]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      Utils.XLAT("dragon_lair_building_info_upgrade_bonuses_name"),
      Utils.XLAT("dragon_lair_building_info_upgrade_bonuses_2_name"),
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(buildingInfo.Type);
    for (int index = 1; index <= maxLevelByType; ++index)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) index);
      elementsData.Add(new string[4]
      {
        index.ToString(),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_1, data.Benefit_Value_1),
        BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_2, data.Benefit_Value_2),
        BuildingInfoUtils.GetPowerValueString(data, data.Building_Lvl)
      });
    }
  }

  public static void PrepareStrongHoldData(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo)
  {
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_1)];
    headerData = new string[4]
    {
      Utils.XLAT("upgrade_info_level_subtitle"),
      dbProperty.Name,
      Utils.XLAT("prop_march_capacity_boost_name"),
      Utils.XLAT("upgrade_info_power_subtitle")
    };
    elementsData = new List<string[]>();
    int num = 1;
    while (true)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) num);
      if (data != null)
      {
        string str = !(data.Benefit_ID_4 == string.Empty) ? BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_4, data.Benefit_Value_4) : string.Empty;
        if (str.Length != 0)
          str = ConfigManager.inst.DB_Properties[int.Parse(data.Benefit_ID_4)].Name + str;
        elementsData.Add(new string[4]
        {
          num.ToString(),
          BuildingInfoUtils.GetBenefitDisplayString(data.Benefit_ID_1, data.Benefit_Value_1),
          str,
          BuildingInfoUtils.GetPowerValueString(data, data.Building_Lvl)
        });
        ++num;
      }
      else
        break;
    }
  }

  private static string GetPowerValueString(BuildingInfo buildingInfo, int lv)
  {
    int num = 0;
    for (int index = 1; index <= lv; ++index)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(buildingInfo.Type + "_" + (object) index);
      num += data.Power;
    }
    return Utils.FormatThousands(num.ToString());
  }

  private static string GetBenefitDisplayString(string benefitInternalID, double baseValue)
  {
    return ConfigManager.inst.DB_Properties[int.Parse(benefitInternalID)].ConvertToDisplayString(baseValue, false, true);
  }

  public delegate void PrepareDataAction(ref string[] headerData, ref List<string[]> elementsData, BuildingInfo buildingInfo);
}
