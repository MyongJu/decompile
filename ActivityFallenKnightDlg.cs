﻿// Decompiled with JetBrains decompiler
// Type: ActivityFallenKnightDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ActivityFallenKnightDlg : UI.Dialog
{
  private int currentDifficultyLevel = 1;
  private int selectableMaxDifficulty = 1;
  public GameObject AbleToStartR4;
  public GameObject AbleToStartR123;
  public GameObject AbleToStartPanel;
  public GameObject OnGoingPanel;
  public Icon KnightIcon;
  public UILabel EventUnstartLabel;
  public UILabel EventOverLabel;
  public UILabel UnstartOrOverTimeLabel;
  public UILabel StartedOrGoingTimeLabel;
  public UILabel DifficultyLevelLabel;
  public UILabel R4HintInfoLabel;
  public UILabel FallenKnightWaves;
  public UILabel FallenKnightRemainedWaves;
  public UILabel NextFallenKnightAppearTime;
  public UILabel DifficultyHintInfoLabel;
  public UILabel KnightLocationX;
  public UILabel KnightLocationY;
  public UILabel FallenKnightName;
  public UILabel FallenKnightTroopConut;
  private int internalId;
  private long alliancePoints;
  private Transform UnStartOrOverParent;
  private Transform StartedOrGoingParent;

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
    this.UnStartOrOverParent = this.EventUnstartLabel.transform.parent;
    this.StartedOrGoingParent = this.AbleToStartPanel.transform.parent;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    ActivityFallenKnightDlg.Params @params = orgParam as ActivityFallenKnightDlg.Params;
    if (@params != null)
      this.internalId = @params.internalId;
    this.AddEventHandler();
    this.ExecuteInSecond(0);
    this.ShowKnightTexture();
    this.ShowFirstEntryPanel();
    this.ShowSelectableMaxDiff();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
  }

  public void ShowFirstEntryPanel()
  {
    if ((ActivityManager.Intance.knightData.isActivity && !this.isInvadeFinished() || ActivityManager.Intance.knightData.isActivityAbleToStart) && !ActivityManager.Intance.knightData.isActivityForbidden)
      this.ShowEventStartPanel();
    else
      this.ShowEventUnstartPanel();
  }

  public void ShowEventUnstartPanel()
  {
    this.UnStartOrOverParent.gameObject.SetActive(true);
    this.StartedOrGoingParent.gameObject.SetActive(false);
    if (ActivityManager.Intance.knightData.isActivity && this.isInvadeFinished() || ActivityManager.Intance.knightData.isActivityForbidden)
    {
      this.EventUnstartLabel.gameObject.SetActive(false);
      this.EventOverLabel.gameObject.SetActive(true);
    }
    else
    {
      this.EventUnstartLabel.gameObject.SetActive(true);
      this.EventOverLabel.gameObject.SetActive(false);
    }
  }

  public void ShowEventStartPanel()
  {
    this.UnStartOrOverParent.gameObject.SetActive(false);
    this.StartedOrGoingParent.gameObject.SetActive(true);
    if (ActivityManager.Intance.knightData.isActivity && !this.isInvadeFinished() && this.isJoinedInAlliance())
      this.ShowOnGoingPanel();
    else
      this.ShowAbleToStartPanel();
  }

  public void ShowAbleToStartPanel()
  {
    this.OnGoingPanel.SetActive(false);
    this.AbleToStartPanel.SetActive(true);
    if (PlayerData.inst.allianceData != null && PlayerData.inst.allianceData.members != null && (PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid) != null && PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid).title >= 4))
    {
      this.AbleToStartR4.SetActive(true);
      this.AbleToStartR123.SetActive(false);
      this.UpdateCurrentDifficulty();
    }
    else
    {
      this.AbleToStartR4.SetActive(false);
      this.AbleToStartR123.SetActive(true);
    }
  }

  public void ShowOnGoingPanel()
  {
    this.OnGoingPanel.SetActive(true);
    this.AbleToStartPanel.SetActive(false);
  }

  public void ShowKnightTexture()
  {
    this.KnightIcon.FeedData((IComponentData) new IconData("Texture/Events/fallen_knight_image_big", new string[2]
    {
      string.Empty,
      string.Empty
    }));
  }

  public void ShowSelectableMaxDiff()
  {
    this.currentDifficultyLevel = this.GetSelectableMaxDifficulty();
    this.DifficultyLevelLabel.text = this.currentDifficultyLevel.ToString();
  }

  public void BeginToEngage()
  {
    UIManager.inst.OpenPopup("Activity/StartConfirmationPopup", (Popup.PopupParameter) new ActivityFallenKnightStartPopup.Parameter()
    {
      difficluty = this.currentDifficultyLevel
    });
  }

  public void ViewRewardsBtnPressed()
  {
    UIManager.inst.OpenDlg("Activity/FallenKnightRewardsDetailDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void GotoFallenKnightDispatchLocation()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    LinkerHub.Instance.LHGotoTile(new LPTile()
    {
      k = ActivityManager.Intance.knightData.curCoordinate.K,
      x = ActivityManager.Intance.knightData.curCoordinate.X,
      y = ActivityManager.Intance.knightData.curCoordinate.Y
    });
  }

  public void OnBattleDetailsBtnClick()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceWarMainDlg", (UI.Dialog.DialogParameter) new AllianceWar_MainDlg.Parameter()
    {
      selectType = AllianceWar_MainDlg.Parameter.SelectType.knight
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void IncreaseDifficultyLevel()
  {
    if (this.currentDifficultyLevel == this.selectableMaxDifficulty)
      return;
    ++this.currentDifficultyLevel;
    this.DifficultyLevelLabel.text = this.currentDifficultyLevel.ToString();
  }

  public void DecreaseDifficultyLevel()
  {
    --this.currentDifficultyLevel;
    if (this.currentDifficultyLevel < 1)
      ++this.currentDifficultyLevel;
    this.DifficultyLevelLabel.text = this.currentDifficultyLevel.ToString();
  }

  private bool isInvadeFinished()
  {
    if (this.isJoinedInAlliance())
      return ActivityManager.Intance.knightData.isFinished;
    return false;
  }

  private bool isJoinedInAlliance()
  {
    if (PlayerData.inst.allianceData != null && PlayerData.inst.allianceData.members != null)
      return PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid) != null;
    return false;
  }

  private int GetSelectableMaxDifficulty()
  {
    this.alliancePoints = ActivityManager.Intance.knightData.topScore;
    List<FallenKnightDifficultInfo> knightDifficultList = ConfigManager.inst.DB_FallenKnightDifficult.GetFallenKnightDifficultList();
    for (int index = 0; index < knightDifficultList.Count; ++index)
    {
      if (this.alliancePoints == knightDifficultList[index].RequireScore)
        return int.Parse(knightDifficultList[index].ID);
      if (this.alliancePoints < knightDifficultList[index].RequireScore)
        return int.Parse(knightDifficultList[index].ID) - 1;
    }
    return knightDifficultList.Count;
  }

  private void UpdateCurrentDifficulty()
  {
    long num = 0;
    List<FallenKnightDifficultInfo> knightDifficultList = ConfigManager.inst.DB_FallenKnightDifficult.GetFallenKnightDifficultList();
    this.selectableMaxDifficulty = this.GetSelectableMaxDifficulty();
    int selectableMaxDifficulty;
    if (this.selectableMaxDifficulty < knightDifficultList.Count)
    {
      num = knightDifficultList[this.selectableMaxDifficulty].RequireScore;
      selectableMaxDifficulty = int.Parse(knightDifficultList[this.selectableMaxDifficulty].ID);
    }
    else
      selectableMaxDifficulty = this.selectableMaxDifficulty;
    if (this.selectableMaxDifficulty < selectableMaxDifficulty)
      this.DifficultyHintInfoLabel.text = string.Format(Utils.XLAT("event_difficulty_description"), (object) this.selectableMaxDifficulty, (object) selectableMaxDifficulty, (object) num);
    else
      this.DifficultyHintInfoLabel.text = string.Format(Utils.XLAT("event_difficulty_max_description"), (object) this.selectableMaxDifficulty);
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.ExecuteInSecond);
    ActivityManager.Intance.onActivityDataUpdate += new System.Action(this.OnActivityDataUpdate);
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.ExecuteInSecond);
    ActivityManager.Intance.onActivityDataUpdate -= new System.Action(this.OnActivityDataUpdate);
  }

  private void OnActivityDataUpdate()
  {
    this.ShowFirstEntryPanel();
  }

  private void ExecuteInSecond(int time = 0)
  {
    Dictionary<string, string> para = new Dictionary<string, string>();
    int num1 = ActivityManager.Intance.knightData.endTime - NetServerTime.inst.ServerTimestamp;
    int num2 = 43200;
    int time1 = num1 - num2;
    if ((ActivityManager.Intance.knightData.isActivity && !this.isInvadeFinished() || ActivityManager.Intance.knightData.isActivityAbleToStart) && !ActivityManager.Intance.knightData.isActivityForbidden)
    {
      if (num1 > 0)
      {
        if (ActivityManager.Intance.knightData.isActivity && this.isJoinedInAlliance())
        {
          int time2 = ActivityManager.Intance.knightData.activityTime + ActivityManager.Intance.knightData.GetFallenKnightTotalInvadeTime() - NetServerTime.inst.ServerTimestamp;
          para.Add("0", Utils.FormatTime(time2, true, false, true));
          this.StartedOrGoingTimeLabel.text = ScriptLocalization.GetWithPara("event_fallen_knight_siege_timer", para, true);
        }
        else
        {
          para.Add("1", Utils.FormatTime(time1 < 0 ? 0 : time1, true, false, true));
          this.StartedOrGoingTimeLabel.text = ScriptLocalization.GetWithPara("event_finishes_num", para, true);
        }
      }
      if (ActivityManager.Intance.knightData.isActivity && this.isInvadeFinished())
        this.ShowEventUnstartPanel();
      else if (ActivityManager.Intance.knightData.isActivityAbleToStart && !ActivityManager.Intance.knightData.isActivity)
      {
        this.ShowEventStartPanel();
      }
      else
      {
        int knightCurrentWaves = ActivityManager.Intance.knightData.GetFallenKnightCurrentWaves();
        this.FallenKnightWaves.text = string.Format(Utils.XLAT("event_fallen_knight_wave_number"), (object) knightCurrentWaves);
        this.NextFallenKnightAppearTime.text = string.Format(Utils.XLAT("event_fallen_knight_next_wave_timer"), (object) ActivityManager.Intance.knightData.GetNextWaveAppearTime());
        this.FallenKnightRemainedWaves.text = "x" + ActivityManager.Intance.knightData.GetFallenKnightRemainedWaves().ToString();
        this.KnightLocationX.text = ActivityManager.Intance.knightData.curCoordinate.X.ToString();
        this.KnightLocationY.text = ActivityManager.Intance.knightData.curCoordinate.Y.ToString();
        this.FallenKnightName.text = ActivityManager.Intance.knightData.GetCurrentFallenKnightName(knightCurrentWaves);
        this.FallenKnightTroopConut.text = "x" + ActivityManager.Intance.knightData.marchCount.ToString();
      }
    }
    else if (ActivityManager.Intance.knightData.isActivityOver)
    {
      if (num1 <= num2)
        return;
      para.Add("1", Utils.FormatTime(time1, true, false, true));
      this.UnstartOrOverTimeLabel.text = ScriptLocalization.GetWithPara("event_finishes_num", para, true);
    }
    else
    {
      int time2 = ActivityManager.Intance.knightData.startTime - NetServerTime.inst.ServerTimestamp;
      if (time2 < 0)
        time2 = ActivityManager.Intance.knightData.endTime - NetServerTime.inst.ServerTimestamp;
      para.Add("1", Utils.FormatTime(time2, true, false, true));
      if (ActivityManager.Intance.knightData.isActivity || ActivityManager.Intance.knightData.isActivityAbleToStart)
      {
        if (time2 > num2)
        {
          para.Remove("1");
          para.Add("1", Utils.FormatTime(time1, true, false, true));
          this.UnstartOrOverTimeLabel.text = ScriptLocalization.GetWithPara("event_finishes_num", para, true);
        }
        else
          this.UnstartOrOverTimeLabel.text = ScriptLocalization.GetWithPara("event_rewards_timer", para, true);
      }
      else if (time2 >= 0 && (!ActivityManager.Intance.knightData.isActivity || !this.isInvadeFinished()) && !ActivityManager.Intance.knightData.isActivityForbidden)
      {
        this.UnstartOrOverTimeLabel.text = ScriptLocalization.GetWithPara("event_starts_num", para, true);
      }
      else
      {
        para.Remove("1");
        para.Add("1", Utils.FormatTime(time1 < 0 ? 0 : time1, true, false, true));
        this.UnstartOrOverTimeLabel.text = ScriptLocalization.GetWithPara("event_finishes_num", para, true);
      }
      if (!ActivityManager.Intance.knightData.isActivityForbidden)
        return;
      this.ShowEventUnstartPanel();
    }
  }

  public class Params : UI.Dialog.DialogParameter
  {
    public int internalId;
  }
}
