﻿// Decompiled with JetBrains decompiler
// Type: Puppet2DRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Puppet2DRender : MonoBehaviour
{
  private string texturePath = string.Empty;
  private string animationPath = string.Empty;
  public UITexture texture;
  public Transform animationRoot;
  private bool isGrey;

  public void Build(string texturePath, string animationPath, bool isGrey = false)
  {
    this.isGrey = isGrey;
    if (this.texturePath != texturePath && this.animationPath != animationPath)
    {
      this.texture.mainTexture = (Texture) null;
      while (this.animationRoot.childCount > 0)
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.animationRoot.GetChild(0).gameObject);
      this.texturePath = texturePath;
      this.animationPath = animationPath;
      AssetManager.Instance.LoadAsync(animationPath, new System.Action<UnityEngine.Object, bool>(this.OnAnimationResultHandler), (System.Type) null);
    }
    else if (isGrey)
      GreyUtility.Grey(this.gameObject);
    else
      GreyUtility.Normal(this.gameObject);
  }

  private void OnAnimationResultHandler(UnityEngine.Object result, bool success)
  {
    if (result != (UnityEngine.Object) null)
    {
      NGUITools.AddChild(this.animationRoot.gameObject, result as GameObject).transform.localScale = Vector3.one * 100f;
      if (this.isGrey)
        GreyUtility.Grey(this.gameObject);
      else
        GreyUtility.Normal(this.gameObject);
    }
    else
      AssetManager.Instance.LoadAsync(this.texturePath, new System.Action<UnityEngine.Object, bool>(this.OnTextureHandler), (System.Type) null);
  }

  private void OnTextureHandler(UnityEngine.Object result, bool success)
  {
    if (!(result != (UnityEngine.Object) null))
      return;
    this.texture.mainTexture = (Texture) (result as Texture2D);
    if (this.isGrey)
      GreyUtility.Grey(this.gameObject);
    else
      GreyUtility.Normal(this.gameObject);
  }

  public void Release()
  {
    this.texturePath = string.Empty;
    this.animationPath = string.Empty;
    AssetManager.Instance.UnLoadAsset(this.texturePath, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    AssetManager.Instance.UnLoadAsset(this.animationPath, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
  }
}
