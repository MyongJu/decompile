﻿// Decompiled with JetBrains decompiler
// Type: StateButton
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class StateButton : MonoBehaviour
{
  public List<StateParam> allStates = new List<StateParam>();

  private void Awake()
  {
    this.ResetAll();
  }

  public void ChangeState(string state)
  {
    this.ResetAll();
    StateParam stateParam = this.allStates.Find((Predicate<StateParam>) (x => x.statename == state));
    if (stateParam == null)
      return;
    stateParam.sprite.SetActive(true);
  }

  private void ResetAll()
  {
    List<StateParam>.Enumerator enumerator = this.allStates.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if ((bool) ((UnityEngine.Object) enumerator.Current.sprite))
        enumerator.Current.sprite.SetActive(false);
    }
  }
}
