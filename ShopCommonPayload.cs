﻿// Decompiled with JetBrains decompiler
// Type: ShopCommonPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class ShopCommonPayload
{
  private Dictionary<int, ShopCommonPayload.ShopItemStatus> _shopItemStatusDict = new Dictionary<int, ShopCommonPayload.ShopItemStatus>();
  private static ShopCommonPayload _instance;
  private int _maxMerlinTowerLevel;
  public System.Action OnShopItemStatusChanged;

  public static ShopCommonPayload Instance
  {
    get
    {
      if (ShopCommonPayload._instance == null)
        ShopCommonPayload._instance = new ShopCommonPayload();
      return ShopCommonPayload._instance;
    }
  }

  public Dictionary<int, ShopCommonPayload.ShopItemStatus> ShopItemStatusDict
  {
    get
    {
      return this._shopItemStatusDict;
    }
  }

  public int MaxMerlinTowerLevel
  {
    get
    {
      return this._maxMerlinTowerLevel;
    }
  }

  public void UpdateShopItemStatus(System.Action<bool, object> callback = null)
  {
    this.ClearShopItemStatus();
    MessageHub.inst.GetPortByAction("item:getCommonShopStatus").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      this.DecodeShopItemStatus(data as Hashtable);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public int GetShopItemRefreshTime(int shopMainId)
  {
    if (this._shopItemStatusDict.ContainsKey(shopMainId) && this._shopItemStatusDict[shopMainId].expiresTime > 0)
      return this._shopItemStatusDict[shopMainId].expiresTime;
    ShopCommonMain shopCommonMain = ConfigManager.inst.DB_ShopCommonMain.Get(shopMainId);
    if (shopCommonMain != null)
      return shopCommonMain.refresh_time;
    return -1;
  }

  public int GetShopItemLeftCount(int shopMainId)
  {
    int num = -1;
    if (this._shopItemStatusDict.ContainsKey(shopMainId))
    {
      ShopCommonMain shopCommonMain = ConfigManager.inst.DB_ShopCommonMain.Get(shopMainId);
      if (shopCommonMain != null)
        num = shopCommonMain.refresh_num >= 0 ? shopCommonMain.refresh_num - this._shopItemStatusDict[shopMainId].boughtCount : shopCommonMain.refresh_num;
    }
    return num;
  }

  public void SecondCheckShopStatus()
  {
    Dictionary<int, ShopCommonPayload.ShopItemStatus>.KeyCollection.Enumerator enumerator = this._shopItemStatusDict.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (this._shopItemStatusDict[enumerator.Current].expiresTime <= NetServerTime.inst.ServerTimestamp)
      {
        ShopCommonMain shopCommonMain = ConfigManager.inst.DB_ShopCommonMain.Get(enumerator.Current);
        if (shopCommonMain != null && shopCommonMain.refresh_time > 0)
        {
          this._shopItemStatusDict[enumerator.Current].expiresTime += shopCommonMain.refresh_time;
          this._shopItemStatusDict[enumerator.Current].boughtCount = 0;
        }
        if (this.OnShopItemStatusChanged != null)
          this.OnShopItemStatusChanged();
      }
    }
  }

  public void ClearShopItemStatus()
  {
    this._shopItemStatusDict.Clear();
  }

  public void DecodeShopItemStatus(Hashtable data)
  {
    if (data == null)
      return;
    if (data.ContainsKey((object) "items"))
    {
      Hashtable hashtable = data[(object) "items"] as Hashtable;
      if (hashtable != null)
      {
        IEnumerator enumerator = hashtable.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          int result = 0;
          int.TryParse(enumerator.Current.ToString(), out result);
          if (result > 0)
          {
            Hashtable inData = hashtable[(object) enumerator.Current.ToString()] as Hashtable;
            if (inData != null)
            {
              int outData1 = 0;
              int outData2 = 0;
              DatabaseTools.UpdateData(inData, "bought", ref outData1);
              DatabaseTools.UpdateData(inData, "expires", ref outData2);
              ShopCommonPayload.ShopItemStatus shopItemStatus = new ShopCommonPayload.ShopItemStatus();
              shopItemStatus.boughtCount = outData1;
              shopItemStatus.expiresTime = outData2;
              this._shopItemStatusDict.Remove(result);
              this._shopItemStatusDict.Add(result, shopItemStatus);
            }
          }
        }
      }
    }
    if (data.ContainsKey((object) "magic_tower_layer"))
      int.TryParse(data[(object) "magic_tower_layer"].ToString(), out this._maxMerlinTowerLevel);
    if (this.OnShopItemStatusChanged == null)
      return;
    this.OnShopItemStatusChanged();
  }

  public class ShopItemStatus
  {
    public int boughtCount;
    public int expiresTime;
  }
}
