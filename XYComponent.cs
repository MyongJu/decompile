﻿// Decompiled with JetBrains decompiler
// Type: XYComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class XYComponent : ComponentRenderBase
{
  public UILabel x;
  public UILabel y;
  public UITexture texture;

  private void Start()
  {
  }

  public override void Init()
  {
    base.Init();
    XYComponentData data = this.data as XYComponentData;
    this.x.text = data.x.ToString();
    this.y.text = data.y.ToString();
  }
}
