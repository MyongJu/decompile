﻿// Decompiled with JetBrains decompiler
// Type: BaseBuilder
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BaseBuilder : IBuilder
{
  public GameObject Target { get; set; }

  protected string ImageName { get; set; }

  public bool needTempIcon { get; set; }

  public bool useWidgetSetting { get; set; }

  public UIAtlas MissingAtlas { get; set; }

  public string MissingSpriteName { get; set; }

  public Texture MissingTexture { get; set; }

  public Texture CustomMissingTexture { get; set; }

  protected bool IsSuccess { get; set; }

  public void Build(string image)
  {
    this.ImageName = image;
    this.Prepare();
    string path = this.GetPath();
    if (string.IsNullOrEmpty(path))
      return;
    AssetManager.Instance.LoadAsync(path, new System.Action<UnityEngine.Object, bool>(this.OnResultHandler), this.GetResourceType());
  }

  public System.Action<bool> OnCompleteHandler { get; set; }

  private void OnResultHandler(UnityEngine.Object result, bool sts)
  {
    if ((UnityEngine.Object) this.Target == (UnityEngine.Object) null)
      return;
    this.ResultHandler(result);
    if (this.OnCompleteHandler == null)
      return;
    this.OnCompleteHandler(this.IsSuccess);
  }

  protected virtual System.Type GetResourceType()
  {
    return typeof (UnityEngine.Object);
  }

  protected virtual void Prepare()
  {
  }

  protected virtual string GetPath()
  {
    return string.Empty;
  }

  protected virtual void ResultHandler(UnityEngine.Object result)
  {
  }

  public void Release()
  {
    if (!string.IsNullOrEmpty(this.GetPath()))
      AssetManager.Instance.UnLoadAsset(this.GetPath(), this.GetType(), (System.Action<UnityEngine.Object, bool>) null);
    this.Clear();
    this.Target = (GameObject) null;
    this.ImageName = (string) null;
    this.MissingAtlas = (UIAtlas) null;
    this.MissingSpriteName = string.Empty;
    this.MissingTexture = (Texture) null;
    this.OnCompleteHandler = (System.Action<bool>) null;
  }

  protected virtual void Clear()
  {
  }
}
