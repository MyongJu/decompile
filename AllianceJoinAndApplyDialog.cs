﻿// Decompiled with JetBrains decompiler
// Type: AllianceJoinAndApplyDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceJoinAndApplyDialog : UI.Dialog
{
  public AllianceSymbol m_AllianceSymbol;
  public UILabel m_AllianceName;
  public UILabel m_LeaderName;
  public UILabel m_Power;
  public UILabel m_MemberCount;
  public UILabel m_Language;
  public UILabel m_GiftLevel;
  public UILabel m_Message;
  public UILabel m_PowerRequired;
  public UILabel m_LevelRequired;
  public GameObject m_PowerRoot;
  public GameObject m_LevelRoot;
  public UITable m_Table;
  public UIButton m_JoinButton;
  public UIButton m_ApplyButton;
  public UIButton m_CancelButton;
  public UIButton m_ContactLeaderButton;
  public GameObject m_AppliedTip;
  private AllianceData m_AllianceData;
  private bool m_JustView;
  private bool m_NeedReload;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    AllianceJoinAndApplyDialog.Parameter parameter = orgParam as AllianceJoinAndApplyDialog.Parameter;
    this.m_AllianceData = parameter.allianceData;
    this.m_JustView = parameter.justView;
    this.UpdateUI();
    DBManager.inst.DB_AllianceInviteApply.onDataChanged += new System.Action<long, long>(this.OnAllianceInviteApplyChanged);
    AllianceManager.Instance.onPushAcceptApply += new System.Action<object>(this.OnAcceptApply);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    DBManager.inst.DB_AllianceInviteApply.onDataChanged -= new System.Action<long, long>(this.OnAllianceInviteApplyChanged);
    AllianceManager.Instance.onPushAcceptApply -= new System.Action<object>(this.OnAcceptApply);
  }

  private void OnAcceptApply(object data)
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void Update()
  {
    if (!this.m_NeedReload)
      return;
    this.m_NeedReload = false;
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) this.m_AllianceData.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) => this.UpdateUI()), true);
  }

  private void OnAllianceInviteApplyChanged(long uid, long allianceId)
  {
    if (allianceId != this.m_AllianceData.allianceId)
      return;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this.m_AllianceData != null)
    {
      this.m_ContactLeaderButton.isEnabled = this.m_AllianceData.creatorId != PlayerData.inst.uid;
      this.m_AllianceSymbol.SetSymbols(this.m_AllianceData.allianceSymbolCode);
      this.m_AllianceName.text = string.Format("[{0}] {1}", (object) this.m_AllianceData.allianceAcronym, (object) this.m_AllianceData.allianceName);
      this.m_Language.text = Language.Instance.GetLanguageName(this.m_AllianceData.language);
      this.m_Power.text = Utils.FormatThousands(this.m_AllianceData.power.ToString());
      this.m_MemberCount.text = string.Format("{0}/{1}", (object) this.m_AllianceData.memberCount, (object) this.m_AllianceData.memberMax);
      this.m_GiftLevel.text = ConfigManager.inst.DB_Gift_Level.GetLevelByPoints((int) this.m_AllianceData.giftPoints).ToString();
      UserData userData = DBManager.inst.DB_User.Get(this.m_AllianceData.creatorId);
      if (userData != null)
        this.m_LeaderName.text = userData.userName;
      else
        D.warn((object) "Bad user data.");
      this.m_Message.text = string.IsNullOrEmpty(this.m_AllianceData.publicMessage) ? ScriptLocalization.Get("alliance_no_motto_description", true) : this.m_AllianceData.publicMessage;
      this.m_PowerRequired.text = Utils.FormatThousands(this.m_AllianceData.powerLimit.ToString());
      this.m_LevelRequired.text = Utils.FormatThousands(this.m_AllianceData.levelLimit.ToString());
      if (this.m_JustView || this.m_AllianceData.worldId != (long) PlayerData.inst.userData.world_id)
      {
        this.m_CancelButton.gameObject.SetActive(false);
        this.m_AppliedTip.gameObject.SetActive(false);
        this.m_ApplyButton.gameObject.SetActive(false);
        this.m_JoinButton.gameObject.SetActive(false);
        this.m_PowerRoot.SetActive(false);
        this.m_LevelRoot.SetActive(false);
      }
      else
      {
        bool flag = DBManager.inst.DB_AllianceInviteApply.IsApplied(this.m_AllianceData.allianceId);
        this.m_CancelButton.gameObject.SetActive(this.m_AllianceData.isPirvate && flag);
        this.m_AppliedTip.gameObject.SetActive(this.m_AllianceData.isPirvate && flag);
        this.m_ApplyButton.gameObject.SetActive(!flag && this.m_AllianceData.isPirvate);
        this.m_JoinButton.gameObject.SetActive(!flag && !this.m_AllianceData.isPirvate);
        this.m_PowerRoot.SetActive(!flag && this.m_AllianceData.powerLimit > 0);
        this.m_LevelRoot.SetActive(!flag && this.m_AllianceData.levelLimit > 1);
        this.m_Table.Reposition();
      }
    }
    else
      D.warn((object) "Bad alliance data.");
  }

  public void OnJoinClick()
  {
    AllianceManager.Instance.JoinAlliance(this.m_AllianceData.allianceId, new System.Action<bool, object>(this.JoinAllianceCallback));
  }

  public void OnApplyClick()
  {
    AllianceManager.Instance.Apply(this.m_AllianceData.allianceId, new System.Action<bool, object>(this.ApplyAllianceCallback));
  }

  public void OnCancelClick()
  {
    AllianceManager.Instance.RevokeApplication(this.m_AllianceData.allianceId, new System.Action<bool, object>(this.OnRevokeApplication));
  }

  public void OnAllianceMember()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceOtherMemberDialog", (UI.Dialog.DialogParameter) new AllianceOtherMemberDialog.Parameter()
    {
      allianceData = this.m_AllianceData
    }, true, true, true);
  }

  public void OnAllianceBoard()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceBoardDialog", (UI.Dialog.DialogParameter) new AllianceBoardDialog.Parameter()
    {
      allianceData = this.m_AllianceData
    }, true, true, true);
  }

  public void OnContactLeader()
  {
    UserData userData = DBManager.inst.DB_User.Get(this.m_AllianceData.creatorId);
    if (userData == null)
      return;
    UIManager.inst.OpenDlg("Mail/MailComposeDlg", (UI.Dialog.DialogParameter) new MailComposeDlg.Parameter()
    {
      recipents = new List<string>() { userData.userName }
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private void OnRevokeApplication(bool ret, object data)
  {
    this.UpdateUI();
  }

  private void JoinAllianceCallback(bool ret, object data)
  {
    if (ret)
    {
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
      Dictionary<string, string> para = new Dictionary<string, string>();
      para["Alliance_Name"] = allianceData.allianceName;
      ToastManager.Instance.AddToast("BASIC_TOAST", (object) new ToastArgs(ConfigManager.inst.DB_Toast.GetData("toast_alliance_join"), para));
    }
    else
      this.m_NeedReload = true;
  }

  private void ApplyAllianceCallback(bool ret, object data)
  {
    if (!ret)
      return;
    UIManager.inst.OpenPopup("Alliance/AllianceApplication", (Popup.PopupParameter) new AllianceApplication.Parameter()
    {
      allianceId = this.m_AllianceData.allianceId
    });
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(this.m_AllianceData.allianceId);
    Dictionary<string, string> para = new Dictionary<string, string>();
    para["Alliance_Name"] = allianceData.allianceName;
    ToastManager.Instance.AddToast("BASIC_TOAST", (object) new ToastArgs(ConfigManager.inst.DB_Toast.GetData("toast_alliance_application"), para));
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public AllianceData allianceData;
    public bool justView;
  }
}
