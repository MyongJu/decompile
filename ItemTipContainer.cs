﻿// Decompiled with JetBrains decompiler
// Type: ItemTipContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ItemTipContainer : MonoBehaviour
{
  private int delay = 1;
  private int MaxHeight = 1493;
  public UITable table;
  public UISprite background;
  private bool init;
  private bool _isDestroy;
  private int contentHeight;

  public void Clear()
  {
  }

  private void Update()
  {
    if (!this.init)
      return;
    --this.delay;
    if (this.delay > 0)
      return;
    this.init = false;
    this.DelayHandler();
  }

  public void RefreshTable()
  {
    if (this._isDestroy)
      return;
    this.table.Reposition();
  }

  public void AddChild(GameObject child)
  {
    this.init = true;
    child.transform.parent = this.table.transform;
    NGUITools.SetActive(this.background.gameObject, false);
    this.table.Reposition();
  }

  private void OnDestroy()
  {
    this._isDestroy = true;
  }

  private void DelayHandler()
  {
    if (this._isDestroy)
      return;
    this.contentHeight = (int) NGUIMath.CalculateRelativeWidgetBounds(this.table.transform).size.y;
    this.background.height = this.contentHeight + 50;
    NGUITools.SetActive(this.background.gameObject, true);
  }

  public bool IsFull(GameObject go, Vector3 position)
  {
    return (double) this.MaxHeight <= (double) this.contentHeight + (double) NGUIMath.CalculateRelativeWidgetBounds(go.transform).size.y;
  }
}
