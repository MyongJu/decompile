﻿// Decompiled with JetBrains decompiler
// Type: HeroMainDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class HeroMainDlg : MonoBehaviour
{
  [SerializeField]
  private HeroMainDlg.Panel panel;
  public HeroItemFilter equipmentFilter;
  public GameObject customizeHeroPopup;
  public GameObject customizeHeroConfirmPopup;
  public HeroDlg2 heroDlg;

  public void OnEnable()
  {
    this.Init();
  }

  public void Init()
  {
    this.Fresh();
  }

  public void Fresh()
  {
  }

  public void CheckHelm()
  {
  }

  public void CheckChest()
  {
  }

  public void CheckGlove()
  {
  }

  public void CheckLeg()
  {
  }

  public void CheckWeapon()
  {
  }

  public void CheckAccessory0()
  {
  }

  public void CheckAccessory1()
  {
  }

  public void CheckAccessory2()
  {
  }

  private void CheckAccessorySlot(int slotIndex, HeroData tmpHeroData)
  {
  }

  public void CustomizeButton()
  {
    this.customizeHeroPopup.SetActive(true);
    this.heroDlg.heroModelDlg.gameObject.SetActive(false);
  }

  public void CustomizeSubmitButton()
  {
    this.customizeHeroConfirmPopup.SetActive(true);
  }

  public void CustomizeCloseButton()
  {
    this.customizeHeroPopup.SetActive(false);
    this.heroDlg.heroModelDlg.gameObject.SetActive(true);
  }

  public void CustomizeConfirmOkayButton()
  {
    this.customizeHeroConfirmPopup.SetActive(false);
    this.customizeHeroPopup.SetActive(false);
    this.heroDlg.heroModelDlg.gameObject.SetActive(true);
  }

  public void CustomizeConfirmCloseButton()
  {
    this.customizeHeroConfirmPopup.SetActive(false);
  }

  [Serializable]
  protected class Panel
  {
    public UILabel heroName;
    public UILabel heroLevel;
    public UISlider heroExpSlider;
    public UILabel heroExpSliderText;
    public UILabel heroSkill;
    public UITexture heroIcon;
    public EquipmentSlot slotHelm;
    public EquipmentSlot slotChest;
    public EquipmentSlot slotGlove;
    public EquipmentSlot slotLeg;
    public EquipmentSlot slotWeapon;
  }
}
