﻿// Decompiled with JetBrains decompiler
// Type: EquipmentSuitBenefitItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentSuitBenefitItem : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelRequireNumber;
  [SerializeField]
  private UILabel _labelBenefitName;
  [SerializeField]
  private UILabel _labelBenefitValue;
  [SerializeField]
  private UITexture _textureBenefitIcon;
  [SerializeField]
  private Color _colorOn;
  [SerializeField]
  private Color _colorOff;

  public void SetData(Benefits.BenefitValuePair benefitValuePair, int requireNumber, int currentNumber)
  {
    this._labelRequireNumber.text = ScriptLocalization.GetWithPara("suit_num_pieces_benefit", new Dictionary<string, string>()
    {
      {
        "0",
        requireNumber.ToString()
      }
    }, true);
    this._labelBenefitName.text = benefitValuePair.propertyDefinition.Name;
    this._labelBenefitValue.text = string.Format("{0}", (object) benefitValuePair.propertyDefinition.ConvertToDisplayString((double) benefitValuePair.value, false, true));
    BuilderFactory.Instance.HandyBuild((UIWidget) this._textureBenefitIcon, benefitValuePair.propertyDefinition.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    if (currentNumber >= requireNumber)
    {
      this._labelRequireNumber.color = this._colorOn;
      this._labelBenefitName.color = this._colorOn;
      this._labelBenefitValue.color = this._colorOn;
      GreyUtility.Normal(this._textureBenefitIcon.gameObject);
    }
    else
    {
      this._labelRequireNumber.color = this._colorOff;
      this._labelBenefitName.color = this._colorOff;
      this._labelBenefitValue.color = this._colorOff;
      GreyUtility.Grey(this._textureBenefitIcon.gameObject);
    }
  }
}
