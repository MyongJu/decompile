﻿// Decompiled with JetBrains decompiler
// Type: StaticMapData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticMapData
{
  public string PrefabPath = string.Empty;
  public int BlankID = -1;
  public Vector2 WorldSize = new Vector2(4f, 4f);
  public Rect WorldDimension = new Rect();
  public Vector2 BlocksPerKingdom = new Vector2();
  public Vector2 TilesPerBlock = new Vector2();
  public Vector2 PixelsPerTile = new Vector2(300f, 150f);
  public Vector2 PixelsPerQuarterTile = new Vector2(150f, 75f);
  public Vector2 PixelsPerBlock = new Vector2();
  public Vector2 PixelsPerKingdom = new Vector2();
  public Vector2 TilesPerKingdom = new Vector2(1280f, 2560f);
  public Vector2 TilesPerWorld = new Vector2();
  public Dictionary<long, TileDefinition> Tileset = new Dictionary<long, TileDefinition>();
  public Dictionary<int, string> DefaultAssets = new Dictionary<int, string>();
  public Dictionary<int, ActiveKingdom> ActiveKingdoms = new Dictionary<int, ActiveKingdom>();
  private int m_KingdomCount;
  private int m_KingdomOffset;

  public int KingdomCount
  {
    get
    {
      return this.m_KingdomCount;
    }
  }

  public void LoadDefault(string DefaultFileName)
  {
    this.DefaultAssets = new Dictionary<int, string>();
    Dictionary<string, string>.Enumerator enumerator = JsonReader.Deserialize<Dictionary<string, string>>((AssetManager.Instance.HandyLoad(DefaultFileName, typeof (TextAsset)) as TextAsset).text).GetEnumerator();
    while (enumerator.MoveNext())
    {
      int result = 0;
      if (int.TryParse(enumerator.Current.Key, out result))
        this.DefaultAssets.Add(result, enumerator.Current.Value);
    }
  }

  public void LoadTileSet(string TilesetFileName, string prefabPath, int kingdomCount, int kingdomOffset = 0)
  {
    this.Tileset.Clear();
    this.PrefabPath = prefabPath;
    this.m_KingdomCount = kingdomCount;
    this.m_KingdomOffset = kingdomOffset;
    string text = (AssetManager.Instance.HandyLoad(TilesetFileName, typeof (TextAsset)) as TextAsset).text;
    if (text == null)
      return;
    Hashtable hashtable1 = Utils.Json2Object(text, true) as Hashtable;
    if (hashtable1 == null)
      return;
    this.BlankID = int.Parse(hashtable1[(object) "BLANK_TILE"] as string);
    Hashtable hashtable2 = hashtable1[(object) "TILES"] as Hashtable;
    Hashtable hashtable3 = hashtable1[(object) "CHUNKS"] as Hashtable;
    this.BlocksPerKingdom = new Vector2((float) int.Parse(hashtable3[(object) "CHUNKS_X"] as string), (float) int.Parse(hashtable3[(object) "CHUNKS_Y"] as string));
    this.TilesPerBlock = new Vector2((float) int.Parse(hashtable2[(object) "TILES_X"] as string), (float) int.Parse(hashtable2[(object) "TILES_Y"] as string));
    this.PixelsPerBlock = new Vector2(this.TilesPerBlock.x * this.PixelsPerQuarterTile.x, this.TilesPerBlock.y * this.PixelsPerQuarterTile.y);
    this.PixelsPerKingdom = new Vector2(this.PixelsPerBlock.x * this.BlocksPerKingdom.x, this.PixelsPerBlock.y * this.BlocksPerKingdom.y);
    this.TilesPerKingdom = new Vector2(this.TilesPerBlock.x * this.BlocksPerKingdom.x, this.TilesPerBlock.y * this.BlocksPerKingdom.y);
    ArrayList arrayList = hashtable1[(object) "TILESET"] as ArrayList;
    for (int index = 0; index < arrayList.Count; ++index)
    {
      Hashtable hashtable4 = arrayList[index] as Hashtable;
      string str = hashtable4[(object) "PREFAB"] as string;
      long key = long.Parse(hashtable4[(object) "ID"] as string);
      TileType tileType = (TileType) long.Parse(hashtable4[(object) "TILE_TYPE"] as string);
      TileDefinition tileDefinition = new TileDefinition()
      {
        ID = key,
        PrefabName = str,
        TileType = tileType
      };
      tileDefinition.UpdatePrefab(prefabPath);
      this.Tileset.Add(key, tileDefinition);
    }
    this.WorldDimension = StaticMapData.CalculateWorldSize(kingdomCount - kingdomOffset);
    this.WorldSize = new Vector2(this.WorldDimension.width, this.WorldDimension.height);
    this.TilesPerWorld = new Vector2(this.TilesPerKingdom.x * this.WorldSize.x, this.TilesPerKingdom.y * this.WorldSize.y);
  }

  public WorldCoordinate ConvertPixelPositionToWorldCoordinate(Vector2 pixelPosition)
  {
    float f1 = 2f * pixelPosition.x / this.PixelsPerTile.x;
    float f2 = 2f * pixelPosition.y / this.PixelsPerTile.y;
    int x = Mathf.FloorToInt(f1);
    int num1 = Mathf.CeilToInt(f2);
    if ((x & 1 ^ num1 & 1) == 1)
    {
      float num2 = (float) (num1 + x);
      if ((double) f2 > -(double) f1 + (double) num2)
        return new WorldCoordinate(x + 1, -num1);
      return new WorldCoordinate(x, 1 - num1);
    }
    float num3 = (float) (num1 - (x + 1));
    if ((double) f2 > (double) f1 + (double) num3)
      return new WorldCoordinate(x, -num1);
    return new WorldCoordinate(x + 1, 1 - num1);
  }

  public WorldCoordinate ConvertWorldPositionToWorldCoordinate(Vector3 point)
  {
    point = 1f / GameEngine.Instance.tileMapScale * point;
    return this.ConvertPixelPositionToWorldCoordinate(new Vector2(point.x, point.y));
  }

  public Coordinate ConvertPixelPositionToTileKXY(Vector3 pixelPosition)
  {
    return this.ConvertWorldCoordinateToKXY(this.ConvertPixelPositionToWorldCoordinate((Vector2) pixelPosition));
  }

  public Coordinate ConvertWorldCoordinateToKXY(WorldCoordinate worldCoords)
  {
    WorldCoordinate kingdomCoordinate = this.CalculateKingdomCoordinate(worldCoords);
    int index = HelixArrayCalaculater.GetIndex(kingdomCoordinate.X, kingdomCoordinate.Y);
    WorldCoordinate originWorldCoordinate = this.CalculateKingdomOriginWorldCoordinate(kingdomCoordinate.X, kingdomCoordinate.Y);
    int x = worldCoords.X - originWorldCoordinate.X;
    int y = worldCoords.Y - originWorldCoordinate.Y;
    return new Coordinate(index + this.m_KingdomOffset, x, y);
  }

  public WorldCoordinate CalculateKingdomOriginWorldCoordinate(int kingdomX, int kingdomY)
  {
    return this.ConvertPixelPositionToWorldCoordinate(this.CalculateKingdomPixelOrigin(kingdomX, kingdomY));
  }

  public Vector2 CalculateKingdomPixelOrigin(int kingdomX, int kingdomY)
  {
    return new Vector2((float) kingdomX * this.PixelsPerKingdom.x, (float) kingdomY * this.PixelsPerKingdom.y);
  }

  public WorldCoordinate CalculateKingdomCoordinate(WorldCoordinate worldCoords)
  {
    return this.CalculateKingdomCoordinate(this.ConvertWorldCoordinateToPixelPosition(worldCoords));
  }

  public WorldCoordinate CalculateKingdomCoordinate(Vector3 pixelPosition)
  {
    return new WorldCoordinate(Mathf.FloorToInt(pixelPosition.x / this.PixelsPerKingdom.x), Mathf.CeilToInt(pixelPosition.y / this.PixelsPerKingdom.y));
  }

  public Vector3 ConvertWorldCoordinateToPixelPosition(WorldCoordinate worldCoords)
  {
    return new Vector3(this.PixelsPerQuarterTile.x * (float) worldCoords.X, -this.PixelsPerQuarterTile.y * (float) worldCoords.Y, 0.0f);
  }

  public Vector3 ConvertWorldCoordinateToWorldPosition(WorldCoordinate worldCoords)
  {
    float num = 0.01f;
    if ((Object) GameEngine.Instance != (Object) null)
      num = GameEngine.Instance.tileMapScale;
    return this.ConvertWorldCoordinateToPixelPosition(worldCoords) * num;
  }

  public WorldCoordinate ConvertTileKXYToWorldCoordinate(Coordinate KXY)
  {
    return this.ConvertPixelPositionToWorldCoordinate((Vector2) this.ConvertTileKXYToPixelPosition(KXY));
  }

  public Coordinate ConvertWorldPositionToTileKXY(Vector3 point)
  {
    point = 1f / GameEngine.Instance.tileMapScale * point;
    return this.ConvertPixelPositionToTileKXY((Vector3) new Vector2(point.x, point.y));
  }

  public Vector3 ConvertTileKXYToPixelPosition(Coordinate coordinate)
  {
    WorldCoordinate coordinate1 = HelixArrayCalaculater.GetCoordinate(coordinate.K - this.m_KingdomOffset);
    Vector2 kingdomPixelOrigin = this.CalculateKingdomPixelOrigin(coordinate1.X, coordinate1.Y);
    return new Vector3(kingdomPixelOrigin.x + this.PixelsPerQuarterTile.x * (float) coordinate.X, kingdomPixelOrigin.y - this.PixelsPerQuarterTile.y * (float) coordinate.Y, 0.0f);
  }

  public Vector3 ConvertTileKXYToWorldPosition(Coordinate coordinate)
  {
    float num = !((Object) GameEngine.Instance != (Object) null) ? 0.01f : GameEngine.Instance.tileMapScale;
    return this.ConvertTileKXYToPixelPosition(coordinate) * num;
  }

  public int GetBlockXIndexByTileX(int tileX)
  {
    return tileX / (int) this.TilesPerBlock.x;
  }

  public int GetBlockYIndexByTileY(int tileY)
  {
    return tileY / (int) this.TilesPerBlock.y;
  }

  public ActiveKingdom FindKingdomByID(int worldID)
  {
    ActiveKingdom activeKingdom;
    this.ActiveKingdoms.TryGetValue(worldID, out activeKingdom);
    return activeKingdom;
  }

  public ActiveKingdom FindKingdomByLocation(int kingdomX, int kingdomY)
  {
    return this.FindKingdomByID(HelixArrayCalaculater.GetIndex(kingdomX, kingdomY) + this.m_KingdomOffset);
  }

  public bool IsKingdomActive(int worldID)
  {
    if (worldID < 0 || worldID > this.m_KingdomCount)
      return false;
    return this.FindKingdomByID(worldID) != null;
  }

  public bool IsKingdomActiveAt(Vector3 worldPos)
  {
    WorldCoordinate kingdomCoordinate = this.CalculateKingdomCoordinate(1f / GameEngine.Instance.tileMapScale * worldPos);
    return this.IsKingdomActive(HelixArrayCalaculater.GetIndex(kingdomCoordinate.X, kingdomCoordinate.Y) + this.m_KingdomOffset);
  }

  public bool IsKingdomActiveAt(WorldCoordinate worldLocation)
  {
    WorldCoordinate kingdomCoordinate = this.CalculateKingdomCoordinate(worldLocation);
    return this.IsKingdomActive(HelixArrayCalaculater.GetIndex(kingdomCoordinate.X, kingdomCoordinate.Y) + this.m_KingdomOffset);
  }

  private static Rect CalculateWorldSize(int maxID)
  {
    if (maxID < 1)
      return new Rect();
    int loopIndex = HelixArrayCalaculater.GetLoopIndex(maxID);
    List<int> corners = new List<int>();
    corners.Add(maxID);
    StaticMapData.GetCorners(loopIndex - 1, maxID, corners);
    StaticMapData.GetCorners(loopIndex, maxID, corners);
    int num1 = int.MaxValue;
    int num2 = int.MaxValue;
    int num3 = int.MinValue;
    int num4 = int.MinValue;
    for (int index = 0; index < corners.Count; ++index)
    {
      WorldCoordinate coordinate = HelixArrayCalaculater.GetCoordinate(corners[index]);
      int x = coordinate.X;
      int y = coordinate.Y;
      if (x < num1)
        num1 = x;
      if (x > num3)
        num3 = x;
      if (y < num2)
        num2 = y;
      if (y > num4)
        num4 = y;
    }
    return new Rect()
    {
      xMin = (float) num1,
      xMax = (float) (num3 + 1),
      yMin = (float) (num2 - 1),
      yMax = (float) num4
    };
  }

  private static void GetCorners(int loopIndex, int maxID, List<int> corners)
  {
    if (loopIndex < 0)
      return;
    if (loopIndex == 0)
    {
      if (1 >= maxID)
        return;
      corners.Add(1);
    }
    else
    {
      int edgeCount = HelixArrayCalaculater.GetEdgeCount(loopIndex);
      int num1 = edgeCount * edgeCount;
      int num2 = num1 - (edgeCount - 1);
      int num3 = num2 - (edgeCount - 1);
      int num4 = num3 - (edgeCount - 1);
      if (num1 < maxID)
        corners.Add(num1);
      if (num2 < maxID)
        corners.Add(num2);
      if (num3 < maxID)
        corners.Add(num3);
      if (num4 >= maxID)
        return;
      corners.Add(num4);
    }
  }
}
