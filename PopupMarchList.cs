﻿// Decompiled with JetBrains decompiler
// Type: PopupMarchList
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PopupMarchList : UI.Dialog
{
  private List<MarchListItemBar> _itemBars = new List<MarchListItemBar>();
  public const string MARCH_LIST_STATE = "march_list_state";
  public MarchListItemBar listItemOrg;
  [SerializeField]
  private PopupMarchList.Panel panel;

  public void OnEnable()
  {
    DBManager.inst.DB_March.onDataChanged += new System.Action<long>(this.DataChanged);
    DBManager.inst.DB_March.onDataRemoved += new System.Action(this.SetInfo);
    this.panel.scrollView.onStoppedMoving += new UIScrollView.OnDragNotification(this.OnScrollStropMoving);
  }

  public void OnDisable()
  {
    DBManager.inst.DB_March.onDataChanged -= new System.Action<long>(this.DataChanged);
    DBManager.inst.DB_March.onDataRemoved -= new System.Action(this.SetInfo);
    this.panel.scrollView.onStoppedMoving -= new UIScrollView.OnDragNotification(this.OnScrollStropMoving);
  }

  private void DataChanged(long marchId)
  {
    this.SetInfo();
  }

  public void OnBackButtonPressed()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnCloseButtonPressed()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void SetInfo()
  {
    List<long> marchOwner = DBManager.inst.DB_March.marchOwner;
    if (this._itemBars.Count < marchOwner.Count)
    {
      for (int count = this._itemBars.Count; count < marchOwner.Count; ++count)
      {
        GameObject gameObject = NGUITools.AddChild(this.panel.table.gameObject, this.listItemOrg.gameObject);
        gameObject.name = count.ToString();
        this._itemBars.Add(gameObject.GetComponent<MarchListItemBar>());
        gameObject.SetActive(true);
      }
      this.panel.table.repositionNow = true;
      this.panel.scrollView.ResetPosition();
    }
    for (int index = 0; index < marchOwner.Count; ++index)
    {
      this._itemBars[index].marchId = marchOwner[index];
      this._itemBars[index].gameObject.SetActive(true);
    }
    for (int count = marchOwner.Count; count < this._itemBars.Count; ++count)
      this._itemBars[count].gameObject.SetActive(false);
    this.panel.scrollView.ResetPosition();
  }

  public void OnScrollStropMoving()
  {
    this.panel.table.repositionNow = true;
  }

  private void OnItemBarDispose()
  {
    this.panel.table.repositionNow = true;
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.SetInfo();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.SetInfo();
  }

  public enum MarchListState
  {
    PVP,
    PVE,
  }

  [Serializable]
  protected class Panel
  {
    public UITable table;
    public UIScrollView scrollView;
    public UIButton BT_Back;
    public UIButton BT_Close;
  }
}
