﻿// Decompiled with JetBrains decompiler
// Type: DailyActiveDetailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DailyActiveDetailPopup : Popup
{
  public UILabel title;
  public UILabel desc;
  public UITexture icon;
  public UIButton gotoBt;
  public UILabel completeLabel;
  public UILabel normalLabel;
  public ComplexTypeIcon complexIcon;
  private int activy_id;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DailyActiveDetailPopup.Parameter parameter = orgParam as DailyActiveDetailPopup.Parameter;
    if (parameter == null)
      return;
    this.activy_id = parameter.activy_id;
    this.UpdateUI();
    this.AddEventHandler();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Clear();
    this.RemoveEventHandler();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void Clear()
  {
    this.complexIcon.Clear();
  }

  private void AddEventHandler()
  {
  }

  private void RemoveEventHandler()
  {
  }

  private void UpdateUI()
  {
    DailyActivyInfo dailyActivyInfo = ConfigManager.inst.DB_DailyActivies.GetDailyActivyInfo(this.activy_id);
    DailyActivyData dailyActivyData = DBManager.inst.DB_DailyActives.Get(this.activy_id);
    this.title.text = dailyActivyInfo.Name;
    this.desc.text = dailyActivyInfo.Desc;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, dailyActivyInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    float num1 = 0.0f;
    NGUITools.SetActive(this.completeLabel.gameObject, false);
    NGUITools.SetActive(this.normalLabel.gameObject, true);
    if (dailyActivyData != null)
    {
      float f = (float) dailyActivyData.Progress / (float) dailyActivyInfo.Value * (float) dailyActivyInfo.Score;
      if ((double) f >= (double) dailyActivyInfo.MaxScore)
      {
        f = (float) dailyActivyInfo.MaxScore;
        NGUITools.SetActive(this.normalLabel.gameObject, false);
        NGUITools.SetActive(this.completeLabel.gameObject, true);
      }
      num1 = Mathf.Floor(f);
    }
    this.normalLabel.text = num1.ToString() + "/" + dailyActivyInfo.MaxScore.ToString();
    this.completeLabel.text = this.normalLabel.text;
    QuestLinkHUDInfo questLinkHudInfo = ConfigManager.inst.DB_QuestLinkHud.GetQuestLinkHudInfo(dailyActivyInfo.GotoId);
    string imageType = dailyActivyInfo.ImageType;
    ComplexTypeIcon.ImageType type;
    ComplexTypeIcon.SubType subType;
    if (imageType != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DailyActiveDetailPopup.\u003C\u003Ef__switch\u0024map4E == null)
      {
        // ISSUE: reference to a compiler-generated field
        DailyActiveDetailPopup.\u003C\u003Ef__switch\u0024map4E = new Dictionary<string, int>(2)
        {
          {
            "building",
            0
          },
          {
            "daily",
            1
          }
        };
      }
      int num2;
      // ISSUE: reference to a compiler-generated field
      if (DailyActiveDetailPopup.\u003C\u003Ef__switch\u0024map4E.TryGetValue(imageType, out num2))
      {
        switch (num2)
        {
          case 0:
            type = ComplexTypeIcon.ImageType.GO;
            subType = ComplexTypeIcon.SubType.Building;
            goto label_12;
          case 1:
            type = ComplexTypeIcon.ImageType.UITexture;
            subType = ComplexTypeIcon.SubType.Daily;
            goto label_12;
        }
      }
    }
    type = ComplexTypeIcon.ImageType.UITexture;
    subType = ComplexTypeIcon.SubType.Items;
label_12:
    this.complexIcon.SetData(dailyActivyInfo.Image, type, subType);
    NGUITools.SetActive(this.gotoBt.gameObject, questLinkHudInfo != null);
  }

  public void OnGotoClickHandler()
  {
    QuestLinkHUDInfo questLinkHudInfo = ConfigManager.inst.DB_QuestLinkHud.GetQuestLinkHudInfo(ConfigManager.inst.DB_DailyActivies.GetDailyActivyInfo(this.activy_id).GotoId);
    if (questLinkHudInfo != null)
      LinkerHub.Instance.Distribute(questLinkHudInfo.Param);
    this.OnCloseHandler();
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int activy_id;
  }
}
