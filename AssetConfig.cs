﻿// Decompiled with JetBrains decompiler
// Type: AssetConfig
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AssetConfig : MonoBehaviour
{
  public static bool CompressSwitch = true;
  public static List<string> basicBundleList = new List<string>()
  {
    "static+basic_common",
    "static+scene_ui",
    "static+basic_gui2",
    "static+walls",
    "static+troops",
    "static+kingdom_tiles",
    "static+kingdom_buildings",
    "static+citybase",
    "ota+dragon1",
    "ota+dragon2",
    "ota+dragon3",
    "ota+dragon4",
    "ota+troop_archer",
    "ota+troop_catapult",
    "ota+troop_fallen_knight_1",
    "ota+troop_fallen_knight_2",
    "ota+troop_fallen_knight_3",
    "ota+troop_gather",
    "ota+troop_infantry",
    "ota+troop_knight",
    "ota+troop_scout",
    "ota+troop_trade",
    "static+citybuildings",
    "static+localize"
  };
  public const string ASSETS_CONFIG_NAME = "assetsconfig.json";
  public const string ASSETS_CONFIG_NAME_R = "assetsconfig_r.json";
  public const string ASSETS_CONFIG_NAME_G = "assetsconfig_g.json";
  public const string ASSET_BUNDLE_MANIFEST_NAME = "assetbundlemanifest";
  public const string VERSION_LIST_NAME = "VersionList.json";
  public const string BUNDLE_CAPCITY_NAME = "bundlecapcity.json";
  public const string BUNDLE_ORIGIN_CAPCITY_NAME = "bundleorigincapcity.json";
  public const string BUNDLE_VERSION_LIST_NAME = "BundleVersionList.json";
  public const string BUNDLE_VERSION_LIST_NAME_R = "BundleVersionList_R.json";
  public const string BUNDLE_VERSION_LIST_NAME_G = "BundleVersionList_G.json";
  public const string CONFIG_VERSION_LIST_NAME = "ConfigVersionList.json";
  public const string APP_VERSION_FIEL_NAME = "version.json";
  public const string CONFIG_ZIP_FILE_NAME = "config.zip";
  public const string LOCAL_CONFIG_VERSION_NAME = "local_config_version";
  public const string BUNDLE_PATH = "iPhone/SD";
  public const string BUNDLE_EX = ".assetbundle";
  public const char CacheSymbol = '=';
  public const string BUNDLE_OTA_FLAG = "ota+";
  public const string BUNDLE_STATIC_FLAG = "static+";
  public const string BUNDLE_DYNAMIC_FLAG = "dyna+";
  public const string LOCALIZE_FLAG = "localize_";
  public static string localPath;

  private void Awake()
  {
    AssetConfig.localPath = Application.persistentDataPath;
  }

  public static string BundleCacheDir
  {
    get
    {
      return AssetConfig.localPath;
    }
  }

  public static bool IsUseCompress(string bundlename)
  {
    return AssetConfig.CompressSwitch || !bundlename.Contains("static+");
  }
}
