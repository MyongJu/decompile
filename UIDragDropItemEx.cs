﻿// Decompiled with JetBrains decompiler
// Type: UIDragDropItemEx
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIDragDropItemEx : UIDragDropItem
{
  protected const string DROP_OBJ_TAG = "DragDropTarget";
  public GameObject topViewNode;
  public Transform leftTopTransform;
  public Transform rightBottomTransform;
  private bool _isDestroyed;
  private Transform _dropItemParent;
  private Vector3 _dropItemOriginPosition;

  protected virtual string DragImagePath
  {
    get
    {
      return string.Empty;
    }
  }

  protected virtual bool GenerateNewSurface
  {
    get
    {
      return true;
    }
  }

  private void OnDestroy()
  {
    this._isDestroyed = true;
  }

  protected override void OnDragDropStart()
  {
    base.OnDragDropStart();
    this._dropItemParent = this.transform.parent;
    this._dropItemOriginPosition = this.transform.localPosition;
    if ((UnityEngine.Object) this.topViewNode != (UnityEngine.Object) null)
      this.transform.parent = this.topViewNode.transform;
    this.BringForward();
  }

  protected override void OnDragDropMove(Vector2 delta)
  {
    base.OnDragDropMove(delta);
    if ((UnityEngine.Object) this.leftTopTransform == (UnityEngine.Object) null || (UnityEngine.Object) this.rightBottomTransform == (UnityEngine.Object) null)
      return;
    this.mTrans.localPosition = new Vector3(Mathf.Clamp(this.mTrans.localPosition.x, this.leftTopTransform.localPosition.x, this.rightBottomTransform.localPosition.x), Mathf.Clamp(this.mTrans.localPosition.y, this.rightBottomTransform.localPosition.y, this.leftTopTransform.localPosition.y), 0.0f);
  }

  protected override void OnDragDropRelease(GameObject surface)
  {
    base.OnDragDropRelease(surface);
    this.BringBackward();
    if (surface.tag.Contains("DragDropTarget"))
    {
      this.transform.parent = surface.transform.parent;
      this.transform.localPosition = Vector3.zero;
      if (this.GenerateNewSurface)
      {
        UITexture componentInChildren1 = this.gameObject.GetComponentInChildren<UITexture>();
        UITexture componentInChildren2 = surface.gameObject.GetComponentInChildren<UITexture>();
        if ((UnityEngine.Object) componentInChildren2 == (UnityEngine.Object) null)
        {
          GameObject gameObject1 = NGUITools.AddChild(surface.transform.parent.gameObject, this.gameObject);
          if ((UnityEngine.Object) gameObject1 != (UnityEngine.Object) null)
          {
            this.EnableGeneratedObject(gameObject1);
            GameObject gameObject2 = gameObject1;
            string str1 = "DragDropTarget";
            gameObject1.name = str1;
            string str2 = str1;
            gameObject2.tag = str2;
            componentInChildren2 = gameObject1.GetComponentInChildren<UITexture>();
          }
        }
        if ((UnityEngine.Object) componentInChildren2 != (UnityEngine.Object) null && (UnityEngine.Object) componentInChildren1 != (UnityEngine.Object) null && ((UnityEngine.Object) componentInChildren1.mainTexture != (UnityEngine.Object) null && componentInChildren1.mainTexture.name != "icon_missing"))
          BuilderFactory.Instance.HandyBuild((UIWidget) componentInChildren2, this.DragImagePath + componentInChildren1.mainTexture.name, (System.Action<bool>) null, true, false, string.Empty);
      }
      if (this.cloneOnDrag)
        return;
      this.ResetDropItem();
      this.ClearCurrentTexture(false);
    }
    else
      this.ResetDropItem();
  }

  protected void EnableGeneratedObject(GameObject obj)
  {
    if ((UnityEngine.Object) obj == (UnityEngine.Object) null)
      return;
    UITexture componentInChildren1 = obj.GetComponentInChildren<UITexture>();
    if ((UnityEngine.Object) componentInChildren1 != (UnityEngine.Object) null)
      componentInChildren1.enabled = true;
    BoxCollider componentInChildren2 = obj.GetComponentInChildren<BoxCollider>();
    if ((UnityEngine.Object) componentInChildren2 != (UnityEngine.Object) null)
      componentInChildren2.enabled = true;
    UIPanel componentInChildren3 = obj.GetComponentInChildren<UIPanel>();
    if ((UnityEngine.Object) componentInChildren3 != (UnityEngine.Object) null)
      componentInChildren3.enabled = true;
    UIDragDropItemEx componentInParent = obj.GetComponentInParent<UIDragDropItemEx>();
    if (!((UnityEngine.Object) componentInParent != (UnityEngine.Object) null))
      return;
    componentInParent.enabled = true;
    componentInParent.cloneOnDrag = false;
    componentInParent.leftTopTransform = this.leftTopTransform;
    componentInParent.rightBottomTransform = this.rightBottomTransform;
  }

  protected void BringForward()
  {
    if (this._isDestroyed)
      return;
    NGUITools.BringForward(this.gameObject);
    UIPanel component = this.gameObject.GetComponent<UIPanel>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.sortingOrder = 2;
  }

  protected void BringBackward()
  {
    if (this._isDestroyed)
      return;
    UIPanel component = this.gameObject.GetComponent<UIPanel>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.sortingOrder = 0;
  }

  protected void ResetDropItem()
  {
    if (this._isDestroyed)
      return;
    this.transform.parent = this._dropItemParent;
    this.transform.localPosition = this._dropItemOriginPosition;
  }

  protected void ClearCurrentTexture(bool destroyObject = false)
  {
    if (this._isDestroyed)
      return;
    if (destroyObject)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    }
    else
    {
      UITexture componentInChildren = this.gameObject.GetComponentInChildren<UITexture>();
      if (!((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null))
        return;
      componentInChildren.mainTexture = (Texture) null;
    }
  }
}
