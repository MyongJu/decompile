﻿// Decompiled with JetBrains decompiler
// Type: HeroCardSuitInfoPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HeroCardSuitInfoPopup : Popup
{
  [SerializeField]
  private UIGrid _Grid;
  [SerializeField]
  private GameObject _Template;
  [SerializeField]
  private UILabel _Title;
  protected LegendCardData legendCardData;

  private void InitParams(HeroCardSuitInfoPopup.Parameter param)
  {
    if (param == null)
      return;
    this.legendCardData = param.legendCardData;
  }

  private void InitUI()
  {
    if (this.legendCardData == null)
      return;
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this.legendCardData.LegendId);
    if (parliamentHeroInfo == null)
      return;
    ParliamentSuitGroupInfo parliamentSuitGroupInfo = ConfigManager.inst.DB_ParliamentSuitGroup.Get(parliamentHeroInfo.heroSuitGroup);
    if (parliamentSuitGroupInfo == null)
      return;
    this._Title.text = ScriptLocalization.GetWithPara("hero_appoint_group_benefits_name", new Dictionary<string, string>()
    {
      {
        "0",
        parliamentSuitGroupInfo.Name
      }
    }, true);
    List<ParliamentHeroInfo> parliamentHerosBySuit = ConfigManager.inst.DB_ParliamentHero.GetParliamentHerosBySuit(parliamentHeroInfo.heroSuitGroup);
    if (parliamentHerosBySuit == null || parliamentHerosBySuit.Count == 0)
      return;
    parliamentHerosBySuit.Sort((Comparison<ParliamentHeroInfo>) ((x, y) => x.internalId.CompareTo(y.internalId)));
    UIUtils.CleanGrid(this._Grid);
    using (List<ParliamentHeroInfo>.Enumerator enumerator = parliamentHerosBySuit.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ParliamentHeroInfo current = enumerator.Current;
        GameObject go = NGUITools.AddChild(this._Grid.gameObject, this._Template);
        NGUITools.SetActive(go, true);
        go.transform.Find("ParliamentHeroItem").GetComponent<ParliamentHeroItem>().SetData(current, false, false);
        UILabel component = go.transform.Find("NameLabel").GetComponent<UILabel>();
        component.text = current.Name;
        component.color = current.QualityColor;
        go.transform.Find("TitleLabel").GetComponent<UILabel>().text = HeroCardUtils.GetHeroTitle(current.internalId);
      }
    }
    this._Grid.Reposition();
  }

  private void Dispose()
  {
  }

  public void OnCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.InitParams(orgParam as HeroCardSuitInfoPopup.Parameter);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.InitUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.Dispose();
    base.OnClose(orgParam);
  }

  public class Parameter : Popup.PopupParameter
  {
    public LegendCardData legendCardData;
  }
}
