﻿// Decompiled with JetBrains decompiler
// Type: BoostContainerData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;

public class BoostContainerData
{
  private List<BuffUIData> _CurrentStartBoosts = new List<BuffUIData>();
  private List<BuffUIData> _buffDatas = new List<BuffUIData>();
  public BoostCategory Type;

  public BoostContainerData(BoostCategory Type)
  {
    this.Type = Type;
    this.Init();
  }

  public event System.Action<BoostType> OnUpdateBoostState;

  public bool IsEmpty()
  {
    return this._buffDatas.Count == 0;
  }

  public List<BuffUIData> GetSubBoost()
  {
    return this._buffDatas;
  }

  private void Init()
  {
    Dictionary<BoostType, bool> dictionary = new Dictionary<BoostType, bool>();
    this._CurrentStartBoosts = new List<BuffUIData>();
    if (this.Type != BoostCategory.alliancetemple)
    {
      using (Dictionary<int, BoostStaticInfo>.ValueCollection.Enumerator enumerator = ConfigManager.inst.DB_Boosts.Item2Boosts.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          BoostStaticInfo current = enumerator.Current;
          if (current.Category == this.Type && !dictionary.ContainsKey(current.Type))
          {
            BuffFromItemData buffFromItemData = new BuffFromItemData();
            buffFromItemData.Data = (object) current;
            buffFromItemData.Refresh();
            dictionary.Add(current.Type, true);
            if (buffFromItemData.IsRunning())
              this._CurrentStartBoosts.Add((BuffUIData) buffFromItemData);
            this._buffDatas.Add((BuffUIData) buffFromItemData);
          }
        }
      }
      JobManager.Instance.OnJobCreated += new System.Action<long>(this.OnJobCreate);
      JobManager.Instance.OnJobRemove += new System.Action<long>(this.OnJobRemove);
    }
    else
    {
      this.Create("heal_speed");
      this.Create("research_speed");
      this.Create("building_speed");
      this.Create("gather_speed");
      DBManager.inst.DB_AllianceMagic.onDataCreate += new System.Action<AllianceMagicData>(this.OnMagiceCreate);
      DBManager.inst.DB_AllianceMagic.onDataRemoved += new System.Action<AllianceMagicData>(this.OnMagiceRemove);
    }
  }

  private void OnMagiceCreate(AllianceMagicData obj)
  {
    this.RefreshBoostState();
  }

  private void OnMagiceRemove(AllianceMagicData obj)
  {
    this.RefreshBoostState();
  }

  private void Create(string skillType)
  {
    TempleSkillInfo bySkillType = ConfigManager.inst.DB_TempleSkill.GetBySkillType(skillType);
    BuffFromAllianceTempleData allianceTempleData = new BuffFromAllianceTempleData();
    allianceTempleData.Data = (object) bySkillType.internalId;
    allianceTempleData.Refresh();
    if (allianceTempleData.IsRunning())
      this._CurrentStartBoosts.Add((BuffUIData) allianceTempleData);
    this._buffDatas.Add((BuffUIData) allianceTempleData);
  }

  public void Clear()
  {
    JobManager.Instance.OnJobCreated -= new System.Action<long>(this.OnJobCreate);
    JobManager.Instance.OnJobRemove -= new System.Action<long>(this.OnJobRemove);
    DBManager.inst.DB_AllianceMagic.onDataCreate -= new System.Action<AllianceMagicData>(this.OnMagiceCreate);
    DBManager.inst.DB_AllianceMagic.onDataRemoved -= new System.Action<AllianceMagicData>(this.OnMagiceRemove);
    this._buffDatas.Clear();
    this._CurrentStartBoosts.Clear();
  }

  private void OnJobRemove(long jobId)
  {
    this.RefreshBoostState();
  }

  private void RefreshBoostState()
  {
    using (List<BuffUIData>.Enumerator enumerator = this._buffDatas.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuffUIData current = enumerator.Current;
        if (current.IsRunning())
        {
          if (!this._CurrentStartBoosts.Contains(current))
          {
            this._CurrentStartBoosts.Add(current);
            if (this.OnUpdateBoostState != null)
              this.OnUpdateBoostState(current.Type);
          }
        }
        else if (this._CurrentStartBoosts.Contains(current))
        {
          this._CurrentStartBoosts.Remove(current);
          if (this.OnUpdateBoostState != null)
            this.OnUpdateBoostState(current.Type);
        }
      }
    }
  }

  public float GetProgreeValue(BoostType type)
  {
    JobHandle job = this.GetJob(type);
    if (job != null)
      return (float) job.LeftTime() / (float) job.Duration();
    return 0.0f;
  }

  private JobHandle GetJob(BoostType type)
  {
    return JobManager.Instance.GetSingleJobByClass(Utils.BoostType2JobEvent(type));
  }

  public int GetBoostRemainTime(BoostType type)
  {
    JobHandle job = this.GetJob(type);
    if (job != null)
      return job.LeftTime();
    return 0;
  }

  private void OnJobCreate(long jobId)
  {
    this.RefreshBoostState();
  }
}
