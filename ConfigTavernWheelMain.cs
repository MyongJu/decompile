﻿// Decompiled with JetBrains decompiler
// Type: ConfigTavernWheelMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigTavernWheelMain
{
  private Dictionary<string, TavernWheelMainInfo> m_DataByID;
  private Dictionary<int, TavernWheelMainInfo> m_DataByInternalID;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<TavernWheelMainInfo, string>(result as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
  }

  public TavernWheelMainInfo Get(string id)
  {
    TavernWheelMainInfo tavernWheelMainInfo;
    this.m_DataByID.TryGetValue(id, out tavernWheelMainInfo);
    return tavernWheelMainInfo;
  }

  public TavernWheelMainInfo Get(int internalId)
  {
    TavernWheelMainInfo tavernWheelMainInfo;
    this.m_DataByInternalID.TryGetValue(internalId, out tavernWheelMainInfo);
    return tavernWheelMainInfo;
  }

  public TavernWheelMainInfo GetWithItemId(int itemId, int itemCount)
  {
    TavernWheelMainInfo tavernWheelMainInfo = (TavernWheelMainInfo) null;
    Dictionary<int, TavernWheelMainInfo>.Enumerator enumerator = this.m_DataByInternalID.GetEnumerator();
    while (enumerator.MoveNext())
    {
      List<Reward.RewardsValuePair> rewards = enumerator.Current.Value.rewards.GetRewards();
      for (int index = 0; index < rewards.Count; ++index)
      {
        if (rewards[index].internalID > 0 && rewards[index].value > 0 && (rewards[index].internalID == itemId && rewards[index].value == itemCount))
        {
          tavernWheelMainInfo = enumerator.Current.Value;
          break;
        }
      }
    }
    return tavernWheelMainInfo;
  }

  public List<TavernWheelMainInfo> GetDatas()
  {
    List<TavernWheelMainInfo> tavernWheelMainInfoList = new List<TavernWheelMainInfo>();
    Dictionary<int, TavernWheelMainInfo>.Enumerator enumerator = this.m_DataByInternalID.GetEnumerator();
    while (enumerator.MoveNext())
    {
      TavernWheelMainInfo tavernWheelMainInfo = enumerator.Current.Value;
      tavernWheelMainInfoList.Add(tavernWheelMainInfo);
    }
    return tavernWheelMainInfoList;
  }

  public List<TavernWheelMainInfo> GetMainWithGradeId(int gradeId)
  {
    List<TavernWheelMainInfo> tavernWheelMainInfoList = new List<TavernWheelMainInfo>();
    Dictionary<int, TavernWheelMainInfo>.Enumerator enumerator = this.m_DataByInternalID.GetEnumerator();
    while (enumerator.MoveNext())
    {
      TavernWheelMainInfo tavernWheelMainInfo = enumerator.Current.Value;
      if (tavernWheelMainInfo.gradeId == gradeId)
        tavernWheelMainInfoList.Add(tavernWheelMainInfo);
    }
    return tavernWheelMainInfoList;
  }

  public TavernWheelMainInfo GetWithReward(int itemId, int itemCount)
  {
    TavernWheelMainInfo tavernWheelMainInfo1 = (TavernWheelMainInfo) null;
    Dictionary<int, TavernWheelMainInfo>.Enumerator enumerator = this.m_DataByInternalID.GetEnumerator();
    while (enumerator.MoveNext())
    {
      TavernWheelMainInfo tavernWheelMainInfo2 = enumerator.Current.Value;
      List<Reward.RewardsValuePair> rewards = tavernWheelMainInfo2.rewards.GetRewards();
      if (rewards[0].internalID == itemId && rewards[0].value == itemCount)
      {
        tavernWheelMainInfo1 = tavernWheelMainInfo2;
        break;
      }
    }
    return tavernWheelMainInfo1;
  }

  public void Clear()
  {
    if (this.m_DataByID != null)
    {
      this.m_DataByID.Clear();
      this.m_DataByID = (Dictionary<string, TavernWheelMainInfo>) null;
    }
    if (this.m_DataByInternalID == null)
      return;
    this.m_DataByInternalID.Clear();
    this.m_DataByInternalID = (Dictionary<int, TavernWheelMainInfo>) null;
  }
}
