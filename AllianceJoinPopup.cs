﻿// Decompiled with JetBrains decompiler
// Type: AllianceJoinPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class AllianceJoinPopup : Popup
{
  public static bool Displayed;
  public UITexture m_Girl;
  public UITexture m_Banner;
  public UIButton BT_join;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    BuilderFactory.Instance.Build((UIWidget) this.m_Girl, "Texture/Alliance/alliance_welcome_character", (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.m_Banner, "Texture/Alliance/first_join_alliance_banner", (System.Action<bool>) null, true, false, true, string.Empty);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    BuilderFactory.Instance.Release((UIWidget) this.m_Girl);
    BuilderFactory.Instance.Release((UIWidget) this.m_Banner);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (orgParam != null)
      this.BT_join.gameObject.SetActive((orgParam as AllianceJoinPopup.Parameter).showButton);
    else
      this.BT_join.gameObject.SetActive(false);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnJoinClicked()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceJoinDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public bool showButton;
  }
}
