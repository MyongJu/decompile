﻿// Decompiled with JetBrains decompiler
// Type: AllianceFortressSlotItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceFortressSlotItem : MonoBehaviour
{
  private bool isEmpty = true;
  public UILabel index;
  public GameObject emptyContent;
  public GameObject joinedContent;
  public AllianceFortressEmptySlotItem emtpyItem;
  public AllianceFortressJoinedSlotItem joinItem;
  public System.Action OnOpenCallBackDelegate;

  public void SetData(int index, long march_id, string rally_id)
  {
    this.index.text = index.ToString();
    this.SetData(march_id, rally_id);
  }

  public void SetData(long march_id, string rally_id)
  {
    NGUITools.SetActive(this.emptyContent, false);
    NGUITools.SetActive(this.joinedContent, true);
    this.emtpyItem.SetData(rally_id);
    this.joinItem.SetData(march_id, rally_id);
    this.joinItem.OnOpenCallBackDelegate = this.OnOpenCallBackDelegate;
    this.isEmpty = false;
  }

  public void Empty(int index, long rally_id)
  {
    this.joinItem.isLock = false;
    NGUITools.SetActive(this.emptyContent, true);
    NGUITools.SetActive(this.joinedContent, false);
    this.index.text = index.ToString();
    this.emtpyItem.UnLock();
  }

  public void Refresh()
  {
    if (!this.isEmpty)
      this.joinItem.Refresh();
    if (!this.isEmpty)
      return;
    this.emtpyItem.Refresh();
  }

  public void Clear()
  {
    this.OnOpenCallBackDelegate = (System.Action) null;
    NGUITools.SetActive(this.emptyContent, false);
    NGUITools.SetActive(this.joinedContent, false);
    this.joinItem.Clear();
  }

  public void Lock(int index)
  {
    NGUITools.SetActive(this.emptyContent, true);
    NGUITools.SetActive(this.joinedContent, false);
    this.index.text = index.ToString();
    this.emtpyItem.Lock();
  }
}
