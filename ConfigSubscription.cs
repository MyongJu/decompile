﻿// Decompiled with JetBrains decompiler
// Type: ConfigSubscription
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigSubscription
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, SubscriptionInfo> datas;
  private Dictionary<long, SubscriptionInfo> dicByUniqueId;

  public object ParseDict(object rs)
  {
    Hashtable hashtable = rs as Hashtable;
    Dictionary<long, int> dictionary = new Dictionary<long, int>();
    if (hashtable == null)
      return (object) dictionary;
    IEnumerator enumerator = hashtable.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      long result1;
      int result2;
      if (long.TryParse(enumerator.Current.ToString(), out result1) && int.TryParse(hashtable[enumerator.Current].ToString(), out result2))
        dictionary.Add(result1, result2);
    }
    return (object) dictionary;
  }

  public object ParseBenefits(object rs)
  {
    Hashtable hashtable = rs as Hashtable;
    Dictionary<long, float> dictionary = new Dictionary<long, float>();
    if (hashtable == null)
      return (object) dictionary;
    IEnumerator enumerator = hashtable.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      long result1;
      float result2;
      if (long.TryParse(enumerator.Current.ToString(), out result1) && float.TryParse(hashtable[enumerator.Current].ToString(), out result2))
        dictionary.Add(result1, result2);
    }
    return (object) dictionary;
  }

  public void BuildDB(object res)
  {
    this.parse.AddParseFunc("ParseComposeItems", new ParseFunc(this.ParseDict));
    this.parse.AddParseFunc("ParseComposeBenefits", new ParseFunc(this.ParseBenefits));
    this.parse.Parse<SubscriptionInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, SubscriptionInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<long, SubscriptionInfo>) null;
  }

  public SubscriptionInfo GetMonthCardInfo(long internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (SubscriptionInfo) null;
  }

  public SubscriptionInfo GetSubscriptionInfo(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (SubscriptionInfo) null;
  }

  public Dictionary<long, SubscriptionInfo> GetAllSubscriptionInfo()
  {
    return this.dicByUniqueId;
  }

  public SubscriptionInfo GetFirstInfo()
  {
    SubscriptionInfo subscriptionInfo = (SubscriptionInfo) null;
    Dictionary<long, SubscriptionInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator();
    while (enumerator.MoveNext())
      subscriptionInfo = enumerator.Current.Value;
    return subscriptionInfo;
  }

  public SubscriptionInfo GetByPayId(string payId)
  {
    SubscriptionInfo subscriptionInfo = (SubscriptionInfo) null;
    Dictionary<long, SubscriptionInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.Value.pay_id == payId)
        subscriptionInfo = enumerator.Current.Value;
    }
    return subscriptionInfo;
  }
}
