﻿// Decompiled with JetBrains decompiler
// Type: AllianceTechSpeedUpLogic
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class AllianceTechSpeedUpLogic
{
  public static AllianceTechSpeedUpLogic _Instance = new AllianceTechSpeedUpLogic();
  private AllianceTechSpeedUpPopup mPopup;
  private long mFundTotal;
  private int mSelectedCount;
  private int mResearchId;
  private long mFundUnit;
  private int mSpeedUpUnitTime;
  private bool mNeedReset;

  private AllianceTechSpeedUpLogic()
  {
    this.InitDatas();
  }

  public static AllianceTechSpeedUpLogic Instance
  {
    get
    {
      return AllianceTechSpeedUpLogic._Instance;
    }
  }

  public long FundSelected
  {
    get
    {
      return this.mFundUnit * (long) this.mSelectedCount;
    }
  }

  public long FundTotal
  {
    get
    {
      this.mFundTotal = PlayerData.inst.allianceData == null ? 0L : PlayerData.inst.allianceData.allianceFund;
      return this.mFundTotal;
    }
  }

  public int SelectedCount
  {
    set
    {
      this.mSelectedCount = value;
      this.mPopup.UpdateViews();
    }
    get
    {
      return this.mSelectedCount;
    }
  }

  public long FundUnit
  {
    get
    {
      return this.mFundUnit;
    }
  }

  public int SpeedUpUnitTime
  {
    get
    {
      return this.mSpeedUpUnitTime;
    }
  }

  public int SpeedUpTime
  {
    get
    {
      return this.mSelectedCount * this.mSpeedUpUnitTime;
    }
  }

  public int MaxCanUseCount
  {
    get
    {
      return Mathf.Max(0, (int) (this.FundTotal / this.mFundUnit));
    }
  }

  public int RecommendCount
  {
    get
    {
      if (this.JobId == 0L)
        return 0;
      return Mathf.Min(Mathf.Max(0, Mathf.CeilToInt((float) JobManager.Instance.GetJob(this.JobId).LeftTime() / (float) this.SpeedUpUnitTime)), this.MaxCanUseCount);
    }
  }

  public void Init(AllianceTechSpeedUpPopup popup, int researchId)
  {
    this.mPopup = popup;
    this.mResearchId = researchId;
    this.RegisterEvents();
  }

  public void Release()
  {
    this.UnRegisterEvents();
    this.mPopup = (AllianceTechSpeedUpPopup) null;
  }

  private void RegisterEvents()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondTrigger);
    DBManager.inst.DB_Alliance.onDataChanged += new System.Action<long>(this.OnAllianceDataChanged);
  }

  private void UnRegisterEvents()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondTrigger);
    DBManager.inst.DB_Alliance.onDataChanged -= new System.Action<long>(this.OnAllianceDataChanged);
  }

  private void InitDatas()
  {
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("alliance_research_duration_speedup_credit_cost");
    if (data1 != null)
      this.mFundUnit = (long) data1.ValueInt;
    GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("alliance_research_duration_speedup_time");
    if (data2 == null)
      return;
    this.mSpeedUpUnitTime = data2.ValueInt * 60;
  }

  public void OnSecondTrigger(int serverTime)
  {
    if ((UnityEngine.Object) this.mPopup == (UnityEngine.Object) null)
      return;
    if (this.JobId == 0L)
    {
      this.mPopup.ClosePopup();
    }
    else
    {
      this.mPopup.UpdateResearchSlider();
      this.mPopup.FixProgressBar(Mathf.Min(this.SelectedCount, this.RecommendCount));
      if (!this.mNeedReset)
        return;
      this.mNeedReset = false;
      this.mPopup.ResetProgressBar();
    }
  }

  public void OnAllianceDataChanged(long allianceId)
  {
    if ((UnityEngine.Object) this.mPopup == (UnityEngine.Object) null)
      return;
    this.mPopup.UpdateViews();
    this.mNeedReset = true;
  }

  public long JobId
  {
    get
    {
      long num = 0;
      AllianceTechData allianceTechData = DBManager.inst.DB_AllianceTech.Get(this.mResearchId);
      if (allianceTechData != null)
        num = allianceTechData.jobId;
      return num;
    }
  }

  public void ReqeustSpeedup()
  {
    MessageHub.inst.GetPortByAction("AllianceTech:speedUp").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "research_id", (object) this.mResearchId, (object) "amount", (object) this.SelectedCount), (System.Action<bool, object>) ((ret, args) =>
    {
      if (!ret || (UnityEngine.Object) this.mPopup == (UnityEngine.Object) null)
        return;
      if (this.RecommendCount == 0)
        this.mPopup.ClosePopup();
      else
        this.mPopup.ResetProgressBar();
    }), true);
  }
}
