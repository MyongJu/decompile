﻿// Decompiled with JetBrains decompiler
// Type: OccupyUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class OccupyUIData
{
  private string _allianceName;
  private string _tag;
  private int _portrait;
  private int _symbol;
  private string _userName;
  private long _uid;
  private int _lordTitleId;

  public int Symbol
  {
    get
    {
      return this._symbol;
    }
    set
    {
      this._symbol = value;
    }
  }

  public string UserName
  {
    get
    {
      return this._userName;
    }
    set
    {
      this._userName = value;
    }
  }

  public string Icon { get; set; }

  public int Portrait
  {
    get
    {
      return this._portrait;
    }
    set
    {
      this._portrait = value;
    }
  }

  public int LordTitleId
  {
    get
    {
      return this._lordTitleId;
    }
    set
    {
      this._lordTitleId = value;
    }
  }

  public bool IsJoinAlliance
  {
    get
    {
      return !string.IsNullOrEmpty(this._allianceName);
    }
  }

  public string AllianceName
  {
    get
    {
      if (string.IsNullOrEmpty(this._allianceName))
        return string.Empty;
      return this._allianceName;
    }
    set
    {
      this._allianceName = value;
    }
  }

  public bool Decode(object source)
  {
    Hashtable hashtable = source as Hashtable;
    if (hashtable == null)
      return false;
    string empty = string.Empty;
    if (hashtable.ContainsKey((object) "uid") && hashtable[(object) "uid"] != null)
      long.TryParse(hashtable[(object) "uid"].ToString(), out this._uid);
    if (hashtable.ContainsKey((object) "tag") && hashtable[(object) "tag"] != null)
      this._tag = hashtable[(object) "tag"].ToString();
    if (hashtable.ContainsKey((object) "user_name") && hashtable[(object) "user_name"] != null)
      this._userName = hashtable[(object) "user_name"].ToString();
    if (hashtable.ContainsKey((object) "symbol") && hashtable[(object) "symbol"] != null)
      int.TryParse(hashtable[(object) "symbol"].ToString(), out this._symbol);
    if (hashtable.ContainsKey((object) "portrait") && hashtable[(object) "portrait"] != null)
      int.TryParse(hashtable[(object) "portrait"].ToString(), out this._portrait);
    if (hashtable.ContainsKey((object) "alliance_name") && hashtable[(object) "alliance_name"] != null)
      this._allianceName = hashtable[(object) "alliance_name"].ToString();
    return true;
  }
}
