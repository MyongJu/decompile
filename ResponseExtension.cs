﻿// Decompiled with JetBrains decompiler
// Type: ResponseExtension
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.IO;
using System.Net;
using System.Text;
using System.Threading;

public static class ResponseExtension
{
  public static void WriteString(this HttpListenerResponse response, string input, string type = "text/plain")
  {
    response.StatusCode = 200;
    response.StatusDescription = "OK";
    if (string.IsNullOrEmpty(input))
      return;
    byte[] bytes = Encoding.UTF8.GetBytes(input);
    response.ContentLength64 = (long) bytes.Length;
    response.ContentType = type;
    response.OutputStream.Write(bytes, 0, bytes.Length);
  }

  public static void WriteFile(this HttpListenerResponse response, string path, string type = "application/octet-stream", bool download = false)
  {
    using (FileStream fileStream = File.OpenRead(path))
    {
      response.StatusCode = 200;
      response.StatusDescription = "OK";
      response.ContentLength64 = fileStream.Length;
      response.ContentType = type;
      if (download)
        response.AddHeader("Content-disposition", string.Format("attachment; filename={0}", (object) Path.GetFileName(path)));
      byte[] numArray = new byte[65536];
      int count;
      while ((count = fileStream.Read(numArray, 0, numArray.Length)) > 0)
      {
        Thread.Sleep(0);
        response.OutputStream.Write(numArray, 0, count);
      }
    }
  }
}
