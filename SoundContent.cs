﻿// Decompiled with JetBrains decompiler
// Type: SoundContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SoundContent : MonoBehaviour
{
  public UISprite musicOnOff;
  public UISprite sfxOnOff;

  public void OnMusicClicked()
  {
    if (this.musicOnOff.gameObject.activeSelf)
      this.musicOnOff.gameObject.SetActive(false);
    else
      this.musicOnOff.gameObject.SetActive(true);
  }

  public void OnSFXClicked()
  {
    if (this.sfxOnOff.gameObject.activeSelf)
      this.sfxOnOff.gameObject.SetActive(false);
    else
      this.sfxOnOff.gameObject.SetActive(true);
  }
}
