﻿// Decompiled with JetBrains decompiler
// Type: TroopOverview
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TroopOverview : MonoBehaviour
{
  public UILabel kill;
  public UILabel dragonAoeKill;
  public UILabel killed;
  public UILabel wounded;
  public UILabel survived;

  public void SeedData(TroopOverview.Data d)
  {
    this.kill.text = d.kill.ToString();
    this.killed.text = d.dead.ToString();
    this.wounded.text = d.wounded.ToString();
    this.survived.text = d.survived.ToString();
    this.dragonAoeKill.text = d.dragonAoeKill.ToString();
  }

  public class Data
  {
    public long kill;
    public long dragonAoeKill;
    public long survived;
    public long wounded;
    public long dead;
  }
}
