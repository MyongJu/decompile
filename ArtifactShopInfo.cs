﻿// Decompiled with JetBrains decompiler
// Type: ArtifactShopInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ArtifactShopInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "artifact_id")]
  public int artifactId;
  [Config(Name = "price")]
  public long price;
  [Config(Name = "priority")]
  public int priority;
  [Config(Name = "limit_start")]
  public float limitStartTime;
  [Config(Name = "limit_end")]
  public float limitEndTime;
  [Config(Name = "limit_num")]
  public int limitNum;
  [Config(Name = "initial_price")]
  public long initialPrice;
  [Config(Name = "is_limit")]
  public int isLimit;

  public bool IsSpecialPriceArtifact
  {
    get
    {
      return this.isLimit >= 1;
    }
  }
}
