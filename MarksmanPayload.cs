﻿// Decompiled with JetBrains decompiler
// Type: MarksmanPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UI;

public class MarksmanPayload
{
  private static MarksmanPayload _instance;
  private MarksmanData _tempMarksmanData;
  public System.Action onMarksmanDataUpdated;

  public static MarksmanPayload Instance
  {
    get
    {
      if (MarksmanPayload._instance == null)
        MarksmanPayload._instance = new MarksmanPayload();
      return MarksmanPayload._instance;
    }
  }

  public MarksmanData TempMarksmanData
  {
    get
    {
      if (this._tempMarksmanData == null)
        this._tempMarksmanData = new MarksmanData();
      return this._tempMarksmanData;
    }
  }

  public bool IsRefreshFree
  {
    get
    {
      if (this.RefreshNeedGold <= 0)
        return true;
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("lucky_archer_free_refresh");
      return data != null && data.ValueInt > this.TempMarksmanData.RewardRefreshTimes;
    }
  }

  public int RefreshNeedGold
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("lucky_archer_refresh_gold");
      if (data != null)
        return data.ValueInt;
      return 0;
    }
  }

  public int CurrentShotStep
  {
    get
    {
      List<MarksmanData.RewardData> all = this.TempMarksmanData.RewardDataList.FindAll((Predicate<MarksmanData.RewardData>) (x => x.RewardOpenedStep > 0));
      if (all != null)
        return all.Count + 1;
      return 1;
    }
  }

  public int NeedPropsCount2Shot
  {
    get
    {
      LuckyArcherStepCostInfo archerStepCostInfo = ConfigManager.inst.DB_LuckyArcherStepCost.Get(this.CurrentShotStep.ToString());
      if (archerStepCostInfo != null)
        return archerStepCostInfo.cost;
      return 0;
    }
  }

  public bool IsPropsEnough
  {
    get
    {
      return this.TempMarksmanData.PropsCount >= (long) this.NeedPropsCount2Shot;
    }
  }

  public List<LuckyArcherGroupInfo> GetSelectableGroupInfo()
  {
    List<LuckyArcherGroupInfo> luckyArcherGroupInfoList = new List<LuckyArcherGroupInfo>();
    List<LuckyArcherGroupInfo> archerGroupInfoList = ConfigManager.inst.DB_LuckyArcherGroup.GetLuckyArcherGroupInfoList();
    if (archerGroupInfoList != null)
      return archerGroupInfoList.FindAll((Predicate<LuckyArcherGroupInfo>) (x =>
      {
        if (x.groupWeight > 0)
          return !string.IsNullOrEmpty(x.GroupName);
        return false;
      }));
    return new List<LuckyArcherGroupInfo>();
  }

  public void Initialize()
  {
    this.RequestServerData((System.Action<bool, object>) null);
    MessageHub.inst.GetPortByAction("payment_success").AddEvent(new System.Action<object>(this.OnPushPaymentSuccess));
  }

  public void Dispose()
  {
    this._tempMarksmanData = (MarksmanData) null;
    MessageHub.inst.GetPortByAction("payment_success").RemoveEvent(new System.Action<object>(this.OnPushPaymentSuccess));
  }

  public void ShowUnlockMarksmanScene()
  {
    CommonConfirmPopup.Parameter parameter = new CommonConfirmPopup.Parameter();
    parameter.title = Utils.XLAT("id_uppercase_confirmation");
    parameter.content = Utils.XLAT("tavern_lucky_archer_unlock_description");
    parameter.confirmButtonText = Utils.XLAT("tavern_lucky_archer_unlock_button");
    parameter.onOkayCallback += new System.Action(this.UnlockMarksmanCallback);
    UIManager.inst.OpenPopup("Marksman/CommonConfirmPopup", (Popup.PopupParameter) parameter);
  }

  public void ShowMarksmanHelp()
  {
    UIManager.inst.OpenPopup("HelpPopup", (Popup.PopupParameter) new HelpPopup.Parameter()
    {
      id = "help_tavern_lucky_archer",
      descParam = new Dictionary<string, string>()
      {
        {
          "0",
          MarksmanPayload.Instance.TempMarksmanData.PropsName
        }
      }
    });
  }

  public void ShowRecordScene()
  {
    UIManager.inst.OpenPopup("Marksman/MarksmanHistoryPopup", (Popup.PopupParameter) null);
  }

  public void ShowPropsNotEnoughConfirm()
  {
    UIManager.inst.OpenPopup("Marksman/GetMorePropsPopup", (Popup.PopupParameter) new GetMorePropsPopup.Parameter()
    {
      itemId = this.TempMarksmanData.PropsId
    });
  }

  public void ShowMarksmanPickScene()
  {
    UIManager.inst.OpenDlg("Marksman/MarksmanPickDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void ShowFirstMarksmanScene()
  {
    this.RequestServerData((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      if (this.TempMarksmanData.OncePlayFinished)
        UIManager.inst.OpenDlg("Marksman/MarksmanPickDlg", (UI.Dialog.DialogParameter) null, true, true, true);
      else
        UIManager.inst.OpenDlg("Marksman/MarksmanPlayDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    }));
  }

  public void StartMarksman(System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("casino:luckyStart").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data as Hashtable);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void OpenMarksmanByIndex(int index, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("casino:luckyOpen").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) nameof (index),
        (object) index
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data as Hashtable);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void OpenBatchMarksman(System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("casino:luckyBatchOpen").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void RefreshMarksmanRewards(int groupId = -1, bool restart = false, System.Action<bool, object> callback = null)
  {
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    if (groupId > 0)
      postData.Add((object) "group_id", (object) groupId);
    if (restart)
      postData.Add((object) "start", (object) 1);
    MessageHub.inst.GetPortByAction("casino:luckyRefresh").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data as Hashtable);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void RequestServerData(System.Action<bool, object> callback = null)
  {
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    if (callback != null)
      MessageHub.inst.GetPortByAction("casino:getLuckyInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          this.Decode(data as Hashtable);
        if (callback == null)
          return;
        callback(ret, data);
      }), true);
    else
      MessageHub.inst.GetPortByAction("casino:getLuckyInfo").SendLoader(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.Decode(data as Hashtable);
      }), false, false);
  }

  public void FeedMarksmanTexture(UITexture texture, string textureImagePath)
  {
    string[] strArray = textureImagePath.Split('/');
    if ((UnityEngine.Object) texture == (UnityEngine.Object) null || strArray == null || !((UnityEngine.Object) texture.mainTexture == (UnityEngine.Object) null) && !(texture.mainTexture.name != strArray[strArray.Length - 1]))
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) texture, textureImagePath, (System.Action<bool>) null, true, true, string.Empty);
  }

  private void UnlockMarksmanCallback()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  public void Decode(Hashtable data)
  {
    if (this._tempMarksmanData == null)
      this._tempMarksmanData = new MarksmanData();
    this._tempMarksmanData.Decode(data);
    if (this.onMarksmanDataUpdated == null)
      return;
    this.onMarksmanDataUpdated();
  }

  private void OnPushPaymentSuccess(object result)
  {
    this.TempMarksmanData.CanMarksmanOpen = true;
  }
}
