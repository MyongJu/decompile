﻿// Decompiled with JetBrains decompiler
// Type: ChatContactList
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ChatContactList : MonoBehaviour
{
  private List<ChatContactBar> _contactList = new List<ChatContactBar>();
  private List<long> m_Result;
  public System.Action<long> onContactBarClick;
  [SerializeField]
  private ChatContactList.Panel panel;

  public void Show()
  {
    this.panel.BT_ShowOn.gameObject.SetActive(false);
    DBManager.inst.DB_Contacts.onDataChanged += new System.Action<long>(this.OnContactDataChanged);
    DBManager.inst.DB_Contacts.onDataRemoved += new System.Action(this.OnContactDataRemoved);
    GameEngine.Instance.ChatManager.onPrivateUnreadChanged += new System.Action<int>(this.OnUnreadMessageChanged);
    this.gameObject.SetActive(true);
    this.FreshList();
  }

  public void Hide()
  {
    this.panel.BT_ShowOn.gameObject.SetActive(true);
    DBManager.inst.DB_Contacts.onDataChanged -= new System.Action<long>(this.OnContactDataChanged);
    DBManager.inst.DB_Contacts.onDataRemoved -= new System.Action(this.OnContactDataRemoved);
    GameEngine.Instance.ChatManager.onPrivateUnreadChanged -= new System.Action<int>(this.OnUnreadMessageChanged);
    this.gameObject.SetActive(false);
  }

  public void FreshList()
  {
    List<long> friends = DBManager.inst.DB_Contacts.friends;
    if (this._contactList.Count < friends.Count)
    {
      for (int count = this._contactList.Count; count < friends.Count; ++count)
      {
        GameObject gameObject = NGUITools.AddChild(this.panel.contactList.gameObject, this.panel.orgContactBar.gameObject);
        gameObject.name = string.Format("contact_{0}", (object) (100 + count));
        ChatContactBar component = gameObject.GetComponent<ChatContactBar>();
        component.onBarClick = new System.Action<long>(this.OnContactBarClick);
        this._contactList.Add(component);
      }
    }
    this.ClearList();
    for (int index = 0; index < friends.Count; ++index)
    {
      this._contactList[index].gameObject.SetActive(true);
      this._contactList[index].Setup(friends[index], true);
    }
    for (int count = friends.Count; count < this._contactList.Count; ++count)
      this._contactList[count].gameObject.SetActive(false);
    this.RepositionTable();
    this.panel.contactScrollView.ResetPosition();
    this.CheckOnlineState();
  }

  public void OnContactDataChanged(long uid)
  {
    this.FreshList();
  }

  public void OnContactDataRemoved()
  {
    this.FreshList();
  }

  public void OnContactBarClick(long uid)
  {
    this.Hide();
    if (this.onContactBarClick == null)
      return;
    this.onContactBarClick(uid);
  }

  public void OnContactOffClick()
  {
    this.Hide();
  }

  private void ClearList()
  {
    for (int index = 0; index < this._contactList.Count; ++index)
      this._contactList[index].gameObject.SetActive(false);
  }

  public void OnBackButtonClick()
  {
    this.Hide();
  }

  private void CheckOnlineState()
  {
    List<long> friends = DBManager.inst.DB_Contacts.friends;
    List<long> uid = new List<long>();
    for (int index = 0; index < friends.Count; ++index)
    {
      UserData userData = DBManager.inst.DB_User.Get(friends[index]);
      if (userData != null)
        uid.Add(userData.channelId);
    }
    if (uid.Count <= 0)
      return;
    PushManager.inst.GetOnlineUsers(uid, new System.Action<List<long>>(this.GetOnlineUsersCallback));
  }

  private void GetOnlineUsersCallback(List<long> result)
  {
    this.m_Result = result;
  }

  public void OnUnreadMessageChanged(int totalCount)
  {
    for (int index = 0; index < this._contactList.Count; ++index)
      this._contactList[index].unreadMsg = GameEngine.Instance.ChatManager.GetUserUnreadCount(this._contactList[index].uid);
  }

  private void RepositionTable()
  {
    for (int index = 0; index < this._contactList.Count; ++index)
      this._contactList[index].transform.localPosition = new Vector3(0.0f, (float) (-108 - index * 204), 0.0f);
  }

  private void Update()
  {
    if (this.m_Result == null)
      return;
    using (List<ChatContactBar>.Enumerator enumerator = this._contactList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ChatContactBar current = enumerator.Current;
        if (current.isActiveAndEnabled)
          current.UpdateOnlineStatus(this.m_Result);
      }
    }
    this.m_Result = (List<long>) null;
  }

  [Serializable]
  protected class Panel
  {
    public UIScrollView contactScrollView;
    public UITable contactList;
    public ChatContactBar orgContactBar;
    public UIButton BT_ShowOn;
    public UIButton BT_ShowOff;
  }
}
