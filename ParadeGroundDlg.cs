﻿// Decompiled with JetBrains decompiler
// Type: ParadeGroundDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ParadeGroundDlg : UI.Dialog
{
  [HideInInspector]
  public Vector3 panelTrans = Vector3.zero;
  private ParadeGroundDlg.TroopsData cityTroopsData = new ParadeGroundDlg.TroopsData();
  private ParadeGroundDlg.TroopsData allTroopsData = new ParadeGroundDlg.TroopsData();
  private List<GameObject> _detailBars = new List<GameObject>();
  public float panelZeroPoint;
  [SerializeField]
  private ParadeGroundDlg.Panel panel;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DBManager.inst.DB_City.onCityTroopChanged += new System.Action<long>(this.OnDataChanged);
    this.RefreshData();
    this.CheckSlot();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    DBManager.inst.DB_City.onCityTroopChanged -= new System.Action<long>(this.OnDataChanged);
  }

  public void OnDataChanged(long cityId)
  {
    if (cityId != (long) PlayerData.inst.cityId)
      return;
    this.RefreshData();
  }

  public void OnEnable()
  {
  }

  public void OnSlot1Click()
  {
    this.OnSlotClick(1);
  }

  public void OnSlot2Click()
  {
    this.OnSlotClick(2);
  }

  public void OnSlot3Click()
  {
    this.OnSlotClick(3);
  }

  public void OnSlotClick(int index)
  {
    if (PlayerData.inst.playerCityData.GetSetMarchSlot() < index)
      UIManager.inst.ShowConfirmationBox(ScriptLocalization.Get("help_march_save_army_unlock_title", true), ScriptLocalization.GetWithPara("help_march_save_army_unlock_description", new Dictionary<string, string>()
      {
        {
          "0",
          "\n" + ScriptLocalization.Get("march_save_army_unlock_condition_1", true) + "\n"
        },
        {
          "1",
          ScriptLocalization.Get("march_save_army_unlock_condition_2", true) + "\n"
        },
        {
          "2",
          ScriptLocalization.Get("march_save_army_unlock_condition_3", true) + "\n"
        }
      }, true), ScriptLocalization.Get("exception_button", true), string.Empty, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) null, (System.Action) null, (System.Action) null);
    else
      UIManager.inst.OpenDlg("MarchResetDlg", (UI.Dialog.DialogParameter) new MarchResetDlg.Parameter()
      {
        armyIndex = index
      }, true, true, true);
  }

  private void CheckSlot()
  {
    int setMarchSlot = PlayerData.inst.playerCityData.GetSetMarchSlot();
    for (int index = 0; index < this.panel.slots.Length; ++index)
    {
      if (index >= setMarchSlot)
        GreyUtility.Grey(this.panel.slots[index]);
    }
  }

  public void RefreshData()
  {
    CityData playerCityData = PlayerData.inst.playerCityData;
    this.cityTroopsData.FreshData(playerCityData.cityTroops);
    this.allTroopsData.FreshData(playerCityData.totalTroops);
    this.panel.totalTroopsCount.text = string.Format("{0}/{1}", (object) Utils.ConvertNumberToNormalString(this.cityTroopsData.allTroopsCount), (object) Utils.ConvertNumberToNormalString(this.allTroopsData.allTroopsCount));
    this.panel.totakUpkeepCount.text = string.Format("{0}/H", (object) Utils.FormatShortThousands((int) playerCityData.totalTroops.totalUpkeep));
    this.panel.marchSlot.text = string.Format("{0}/{1}", (object) Utils.FormatShortThousands(this.cityTroopsData.allMarchCount), (object) Utils.FormatShortThousands(this.cityTroopsData.maxMarchCount));
    this.panel.trapsCount.text = string.Format("{0}/{1}", (object) Utils.FormatShortThousands(this.cityTroopsData.allTrapsCount), (object) Utils.FormatShortThousands(this.cityTroopsData.maxTrapsCount));
    this.panel.injuredTroops.text = string.Format("{0}/{1}", (object) Utils.FormatShortThousands(this.cityTroopsData.allInjuredTroopsCount), (object) Utils.FormatShortThousands(this.cityTroopsData.maxInjuredTroopsCount));
    float[] numArray1 = new float[5];
    int[] numArray2 = new int[5];
    for (int index = 0; index < 5; ++index)
    {
      numArray1[index] = 0.0f;
      numArray2[index] = 0;
    }
    Dictionary<string, int>.KeyCollection.Enumerator enumerator1 = playerCityData.cityTroops.troops.Keys.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator1.Current);
      if (data != null && !data.Troop_Class_ID.Contains("class_trap"))
      {
        TroopType unitType = data.GetUnitType();
        if (unitType < (TroopType) numArray2.Length)
          numArray2[(int) unitType] += playerCityData.cityTroops.troops[enumerator1.Current];
      }
    }
    int num1 = 0;
    int num2 = 0;
    for (int index = 0; index < numArray2.Length; ++index)
    {
      if (numArray2[index] > num2)
        num2 = numArray2[index];
      num1 += numArray2[index];
    }
    TroopType troopType = TroopType.invalid;
    if (numArray2[0] == num2)
      troopType = TroopType.class_infantry;
    else if (numArray2[2] == num2)
      troopType = TroopType.class_cavalry;
    else if (numArray2[1] == num2)
      troopType = TroopType.class_ranged;
    else if (numArray2[4] == num2)
      troopType = TroopType.class_artyfar;
    if (num2 != 0 && troopType != TroopType.invalid)
    {
      for (int index = 0; index < 5; ++index)
      {
        if ((TroopType) index != troopType)
          numArray1[index] = (float) (int) (100.0 * (double) numArray2[index] / (double) num1 + 0.5) / 100f;
      }
      float num3 = 1f;
      for (int index = 0; index < 5; ++index)
        num3 -= numArray1[index];
      numArray1[(int) troopType] = num3;
      float maxPercent = 0.0f;
      for (int index = 0; index < 5; ++index)
      {
        if ((double) numArray1[index] > (double) maxPercent)
          maxPercent = numArray1[index];
      }
      for (int index = 0; index < 5; ++index)
      {
        this.panel.troopProgress[index].Setup(maxPercent, numArray1[index], false);
        this.panel.troopCount[index].text = string.Format("{0}/{1}", (object) Utils.FormatShortThousands(this.cityTroopsData.troops[(TroopType) index].troopCount).ToString(), (object) Utils.FormatShortThousands(this.allTroopsData.troops[(TroopType) index].troopCount).ToString());
      }
    }
    else
    {
      for (int index = 0; index < 5; ++index)
      {
        this.panel.troopProgress[index].Setup(0.0f, 0.0f, false);
        this.panel.troopCount[index].text = string.Format("{0}/{1}", (object) Utils.FormatShortThousands(this.cityTroopsData.troops[(TroopType) index].troopCount).ToString(), (object) Utils.FormatShortThousands(this.allTroopsData.troops[(TroopType) index].troopCount).ToString());
      }
    }
    this.panel.trapProgress.Setup(1f, this.cityTroopsData.maxTrapsCount == 0 ? 0.0f : (float) this.cityTroopsData.allTrapsCount / (float) this.cityTroopsData.maxTrapsCount, true);
    this.panel.trapCount.text = Utils.FormatShortThousands(this.cityTroopsData.allTrapsCount);
    for (int index = 0; index < this._detailBars.Count; ++index)
    {
      this._detailBars[index].transform.parent = (Transform) null;
      this._detailBars[index].SetActive(false);
      UnityEngine.Object.Destroy((UnityEngine.Object) this._detailBars[index]);
    }
    this._detailBars.Clear();
    for (int index1 = 0; index1 < 5; ++index1)
    {
      if (this.allTroopsData.troops[(TroopType) index1].troops.Count == 0)
      {
        this.panel.troopContainer[index1].gameObject.SetActive(false);
      }
      else
      {
        this.panel.troopContainer[index1].gameObject.SetActive(true);
        this.panel.troopContainer[index1].height = (this.allTroopsData.troops[(TroopType) index1].troops.Count - 1) / 2 * 200 + 200;
        List<ParadeGroundDlg.TroopData.InTroopData> inTroopDataList = new List<ParadeGroundDlg.TroopData.InTroopData>();
        Dictionary<string, ParadeGroundDlg.TroopData.InTroopData>.ValueCollection.Enumerator enumerator2 = this.allTroopsData.troops[(TroopType) index1].troops.Values.GetEnumerator();
        while (enumerator2.MoveNext())
          inTroopDataList.Add(enumerator2.Current);
        inTroopDataList.Sort((Comparison<ParadeGroundDlg.TroopData.InTroopData>) ((a, b) => b.unitInfo.Troop_Tier.CompareTo(a.unitInfo.Troop_Tier)));
        for (int index2 = 0; index2 < inTroopDataList.Count; ++index2)
        {
          GameObject gameObject = NGUITools.AddChild(this.panel.troopContainer[index1].gameObject, this.panel.detailSlotOrg.gameObject);
          gameObject.name = string.Format("{0}_{1}_slot", (object) index1, (object) index2);
          gameObject.SetActive(true);
          gameObject.transform.localPosition = new Vector3(index2 % 2 != 0 ? 350f : -350f, -100f - (float) (index2 / 2 * 200), 0.0f);
          this._detailBars.Add(gameObject);
          ParadeGroundDlgDetailSlot component = gameObject.GetComponent<ParadeGroundDlgDetailSlot>();
          int cityTroopCount = 0;
          int allTroopCount = 0;
          if (this.cityTroopsData != null && this.cityTroopsData.troops.ContainsKey((TroopType) index1) && (this.cityTroopsData.troops[(TroopType) index1] != null && this.cityTroopsData.troops[(TroopType) index1].troops != null) && (this.cityTroopsData.troops[(TroopType) index1].troops.ContainsKey(inTroopDataList[index2].id) && this.cityTroopsData.troops[(TroopType) index1].troops[inTroopDataList[index2].id] != null))
            cityTroopCount = this.cityTroopsData.troops[(TroopType) index1].troops[inTroopDataList[index2].id].count;
          if (this.allTroopsData != null && this.allTroopsData.troops.ContainsKey((TroopType) index1) && (this.allTroopsData.troops[(TroopType) index1] != null && this.allTroopsData.troops[(TroopType) index1].troops != null) && (this.allTroopsData.troops[(TroopType) index1].troops.ContainsKey(inTroopDataList[index2].id) && this.allTroopsData.troops[(TroopType) index1].troops[inTroopDataList[index2].id] != null))
            allTroopCount = this.allTroopsData.troops[(TroopType) index1].troops[inTroopDataList[index2].id].count;
          component.SetupTroop(inTroopDataList[index2].id, cityTroopCount, allTroopCount);
        }
      }
    }
    if (this.cityTroopsData.traps.troops.Count == 0)
    {
      this.panel.trapContainer.gameObject.SetActive(false);
    }
    else
    {
      this.panel.trapContainer.gameObject.SetActive(true);
      this.panel.trapContainer.height = (this.cityTroopsData.traps.troops.Count - 1) / 2 * 200 + 200;
    }
    List<ParadeGroundDlg.TroopData.InTroopData> inTroopDataList1 = new List<ParadeGroundDlg.TroopData.InTroopData>();
    Dictionary<string, ParadeGroundDlg.TroopData.InTroopData>.ValueCollection.Enumerator enumerator3 = this.cityTroopsData.traps.troops.Values.GetEnumerator();
    while (enumerator3.MoveNext())
      inTroopDataList1.Add(enumerator3.Current);
    inTroopDataList1.Sort((Comparison<ParadeGroundDlg.TroopData.InTroopData>) ((a, b) => b.unitInfo.Troop_Tier.CompareTo(a.unitInfo.Troop_Tier)));
    for (int index = 0; index < inTroopDataList1.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.panel.trapContainer.gameObject, this.panel.detailSlotOrg.gameObject);
      gameObject.name = string.Format("{0}_{1}_slot", (object) 6, (object) index);
      gameObject.SetActive(true);
      gameObject.transform.localPosition = new Vector3(index % 2 != 0 ? 350f : -350f, -100f - (float) (index / 2 * 200), 0.0f);
      this._detailBars.Add(gameObject);
      gameObject.GetComponent<ParadeGroundDlgDetailSlot>().SetupTrap(inTroopDataList1[index].id, this.cityTroopsData.traps.troops[inTroopDataList1[index].id].count);
    }
    this.panel.detailTable.Reposition();
    this.panel.detailTable.onReposition += new UITable.OnReposition(this.OnTableReset);
    this.panel.detailScrollView.ResetPosition();
  }

  public void OnBackButtonClick()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnTableReset()
  {
    this.panelZeroPoint = this.panel.troopBars[0].localPosition.y + this.panel.detailScrollView.gameObject.transform.localPosition.y;
  }

  public void OnInfantryButtonClick()
  {
    this.OnTroopsClick(0);
  }

  public void OnRangedButtonClick()
  {
    this.OnTroopsClick(1);
  }

  public void OnCavalryButtonClick()
  {
    this.OnTroopsClick(2);
  }

  public void OnArtyCloseButtonClick()
  {
    this.OnTroopsClick(3);
  }

  public void OnArtyFarButtonClick()
  {
    this.OnTroopsClick(4);
  }

  public void OnTroopsClick(int troopType)
  {
    this._SetPanelPosition(this.panel.troopBars[troopType].localPosition.y);
  }

  public void OnTrapClick()
  {
    this._SetPanelPosition(this.panel.trapBar.localPosition.y);
  }

  private void _SetPanelPosition(float endPosition)
  {
    this.panelTrans.x = this.panel.detailScrollView.gameObject.transform.localPosition.x;
    this.panelTrans.y = this.panelZeroPoint - endPosition;
    SpringPanel.Begin(this.panel.detailScrolPanel.gameObject, this.panelTrans, 13f);
  }

  [Serializable]
  protected class Panel
  {
    public ParadeGroundTroopsPercentProgress[] troopProgress = new ParadeGroundTroopsPercentProgress[5];
    public UILabel[] troopCount = new UILabel[5];
    public Transform[] troopBars = new Transform[5];
    public UIWidget[] troopContainer = new UIWidget[5];
    public UILabel totalTroopsCount;
    public UILabel totakUpkeepCount;
    public UILabel marchSlot;
    public UILabel trapsCount;
    public UILabel injuredTroops;
    public GameObject[] slots;
    public ParadeGroundTroopsPercentProgress trapProgress;
    public UILabel trapCount;
    public UIScrollView detailScrollView;
    public UITable detailTable;
    public ParadeGroundDlgDetailSlot detailSlotOrg;
    public UIPanel detailScrolPanel;
    public Transform trapBar;
    public UIWidget trapContainer;
  }

  protected class TroopsData
  {
    public Dictionary<TroopType, ParadeGroundDlg.TroopData> troops;
    public ParadeGroundDlg.TroopData traps;

    public TroopsData()
    {
      this.troops = new Dictionary<TroopType, ParadeGroundDlg.TroopData>();
      for (int index = 0; index < 5; ++index)
        this.troops.Add((TroopType) index, new ParadeGroundDlg.TroopData());
      this.traps = new ParadeGroundDlg.TroopData();
    }

    public int allTroopsCount { get; private set; }

    public int maxTroopsCount { get; private set; }

    public int allUpkeepCount { get; private set; }

    public int allMarchCount { get; private set; }

    public int maxMarchCount { get; private set; }

    public int allTrapsCount { get; private set; }

    public int maxTrapsCount { get; private set; }

    public int allInjuredTroopsCount { get; private set; }

    public int maxInjuredTroopsCount { get; private set; }

    public float maxTroopsPercent { get; private set; }

    public void FreshData(CityTroopsInfo cityTroopsInfo)
    {
      this.traps.Clear();
      for (int index = 0; index < 5; ++index)
        this.troops[(TroopType) index].Clear();
      using (Dictionary<string, int>.KeyCollection.Enumerator enumerator = cityTroopsInfo.troops.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(current);
          if (data != null)
          {
            if (data.Troop_Class_ID.Contains("class_trap"))
            {
              this.traps.ResetTroop(current, cityTroopsInfo.troops[current]);
            }
            else
            {
              TroopType unitType = data.GetUnitType();
              if (this.troops.ContainsKey(unitType))
                this.troops[unitType].ResetTroop(current, cityTroopsInfo.troops[current]);
            }
          }
        }
      }
      this.allTroopsCount = 0;
      this.allUpkeepCount = 0;
      for (int index = 0; index < 5; ++index)
      {
        this.allTroopsCount += this.troops[(TroopType) index].troopCount;
        this.allUpkeepCount += this.troops[(TroopType) index].upkeepCount;
      }
      this.maxTroopsCount = (int) ConfigManager.inst.DB_BenefitCalc.GetFinalData(DBManager.inst.DB_Local_Benefit.Get("prop_march_capacity_value").total, string.Empty);
      this.maxTroopsPercent = 0.0f;
      for (int index = 0; index < 5; ++index)
      {
        float num = this.allTroopsCount == 0 ? 0.0f : (float) this.troops[(TroopType) index].troopCount / (float) this.allTroopsCount;
        this.troops[(TroopType) index].troopPercent = num;
        if ((double) num > (double) this.maxTroopsPercent)
          this.maxTroopsPercent = num;
      }
      this.allMarchCount = DBManager.inst.DB_March.marchOwner.Count;
      this.maxMarchCount = PlayerData.inst.playerCityData.marchSlot;
      this.allTrapsCount = this.traps.troopCount;
      this.maxTrapsCount = (int) ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) ConfigManager.inst.DB_Building.GetData(string.Format("fortress_{0}", (object) (CityManager.inst.GetHighestBuildingLevelFor("fortress") >= 0 ? CityManager.inst.GetHighestBuildingLevelFor("fortress") : 0))).Benefit_Value_1, "calc_trap_capacity");
      CityHealingTroopsInfo hospitalTroops = PlayerData.inst.playerCityData.hospitalTroops;
      PlayerData.inst.CityData.GetHighestBuildingLevelFor("hospital");
      this.allInjuredTroopsCount = (int) hospitalTroops.totalTroopsCount;
      this.maxInjuredTroopsCount = ConfigManager.inst.DB_BenefitCalc.GetFinalData((int) DBManager.inst.DB_Local_Benefit.Get("prop_healing_capacity_base_value").total, "calc_healing_capacity");
      AllianceHospitalData myHospitalData = DBManager.inst.DB_AllianceHospital.GetMyHospitalData();
      AllianceBuildingStaticInfo allianceHospitalInfo = ConfigManager.inst.DB_AllianceBuildings.AllianceHospitalInfo;
      if (myHospitalData == null || myHospitalData.CurrentState != AllianceHospitalData.State.COMPLETE)
        return;
      this.allInjuredTroopsCount += (int) myHospitalData.GetMyInjuredTroopCount();
      this.maxInjuredTroopsCount += ConfigManager.inst.DB_BenefitCalc.GetFinalData((int) allianceHospitalInfo.Param1, "calc_alliance_hospital_capacity");
    }
  }

  protected class TroopData
  {
    public Dictionary<string, ParadeGroundDlg.TroopData.InTroopData> troops = new Dictionary<string, ParadeGroundDlg.TroopData.InTroopData>();
    public float troopPercent;
    public int upkeepCount;
    public int troopCount;

    public void ResetTroop(string troopID, int inTroopCount)
    {
      if (this.troops.ContainsKey(troopID))
      {
        this.troopCount -= this.troops[troopID].count;
        this.upkeepCount -= this.troops[troopID].upkeep;
        this.troops[troopID].count = inTroopCount;
        this.troopCount += this.troops[troopID].count;
        this.upkeepCount += this.troops[troopID].upkeep;
      }
      else
      {
        this.troops.Add(troopID, new ParadeGroundDlg.TroopData.InTroopData(troopID));
        this.troops[troopID].count = inTroopCount;
        this.troopCount += this.troops[troopID].count;
        this.upkeepCount += this.troops[troopID].upkeep;
      }
      this.troops[troopID].count = inTroopCount;
    }

    public void Clear()
    {
      this.troops.Clear();
      this.troopPercent = 0.0f;
      this.upkeepCount = this.troopCount = 0;
    }

    public class InTroopData
    {
      public readonly string id;
      public readonly Unit_StatisticsInfo unitInfo;
      private int _count;

      public InTroopData(string inID)
      {
        this.id = inID;
        this.unitInfo = ConfigManager.inst.DB_Unit_Statistics.GetData(this.id);
      }

      public int count
      {
        get
        {
          return this._count;
        }
        set
        {
          this._count = value;
          this.upkeep = (int) ((double) this.unitInfo.Upkeep * (double) this._count);
        }
      }

      public int upkeep { get; private set; }
    }
  }
}
