﻿// Decompiled with JetBrains decompiler
// Type: IPushSystem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Connection;
using System.Collections.Generic;

public interface IPushSystem
{
  event System.Action OnConsistentError;

  bool ConnectionState { get; }

  void Init(long userChannel);

  void Dispose();

  void DoConnect(List<long> ids, System.Action<bool> callback);

  void GroupJoin(long id, System.Action<bool> callback);

  void GroupLeave(long id, System.Action<bool> callback);

  void Send(long id, object msg, System.Action<bool> callback);

  void SendGroup(long id, object msg, System.Action<bool> callback);

  void PushGroup(long id, object msg, System.Action<bool> callback);

  void GetOnlineUsers(List<long> uids, System.Action<List<long>> callback);

  void HistoryMessage(long id, int num, long offset, System.Action<MsgResult> callback, byte mType);

  void GroupHistoryMessage(long id, int num, long offset, System.Action<MsgResult> callback, byte mType);

  void PersenalHistoryMessage(long id, int num, long offset, System.Action<MsgResult> callback);
}
