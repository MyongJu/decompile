﻿// Decompiled with JetBrains decompiler
// Type: AllianceRewardGroupRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class AllianceRewardGroupRender : MonoBehaviour
{
  private List<IAPDailyRewardItemRender> allianceItems = new List<IAPDailyRewardItemRender>();
  private List<IAPDailyRewardItemRender> individualItems = new List<IAPDailyRewardItemRender>();
  private List<AllianceFundRender> fundList = new List<AllianceFundRender>();
  private List<PersonalHonorRender> honorList = new List<PersonalHonorRender>();
  private const string SINGLE_RANK = "alliance_warfare_rank_single_name";
  private const string MULTI_RANK = "alliance_warfare_rank_multiple_name";
  private const string ALLIANCE_REWARD = "id_alliance_rewards";
  private const string SINGLE_REWARD = "id_individual_rewards";
  public UILabel rank;
  public UILabel allianceRewardTitle;
  public UILabel individualRewardTitle;
  public UIGrid allianceGrid;
  public UIGrid individualGrid;
  public GameObject itemTemplate;
  public GameObject fundTemplate;
  public GameObject honorTemplate;
  private AllianceRewardGroupData data;

  public AllianceRewardGroupData Data
  {
    get
    {
      return this.data;
    }
  }

  public void FeedData(AllianceRewardGroupData data)
  {
    this.data = data;
    if (data == null)
      return;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.rank.text = "Rank";
    this.allianceRewardTitle.text = Utils.XLAT("id_alliance_rewards");
    this.individualRewardTitle.text = Utils.XLAT("id_individual_rewards");
    this.UpdateRank();
    this.UpdateAllianceRewards();
    this.UpdateIndividualRewards();
  }

  private void UpdateRank()
  {
    Dictionary<string, string> para = new Dictionary<string, string>();
    if (this.data.rankMin == this.data.rankMax)
    {
      para.Add("0", this.data.rankMin.ToString());
      this.rank.text = ScriptLocalization.GetWithPara("alliance_warfare_rank_single_name", para, true);
    }
    else
    {
      para.Add("0", this.data.rankMin.ToString());
      para.Add("1", this.data.rankMax.ToString());
      this.rank.text = ScriptLocalization.GetWithPara("alliance_warfare_rank_multiple_name", para, true);
    }
  }

  private void UpdateAllianceRewards()
  {
    if (this.data.allianceRewards == null)
      return;
    Dictionary<ItemStaticInfo, int>.Enumerator enumerator = this.data.allianceRewards.GetEnumerator();
    while (enumerator.MoveNext())
    {
      GameObject gameObject = Object.Instantiate<GameObject>(this.itemTemplate);
      gameObject.transform.parent = this.allianceGrid.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localScale = Vector3.one;
      gameObject.SetActive(true);
      IAPDailyRewardItemRender component = gameObject.GetComponent<IAPDailyRewardItemRender>();
      ItemStaticInfo key = enumerator.Current.Key;
      int itemCount = enumerator.Current.Value;
      component.SetData(key, itemCount);
      this.allianceItems.Add(component);
    }
    this.allianceGrid.Reposition();
  }

  private void UpdateIndividualRewards()
  {
    if (this.data.individualRewards == null)
      return;
    GameObject go1 = NGUITools.AddChild(this.individualGrid.gameObject, this.fundTemplate);
    NGUITools.SetActive(go1, true);
    AllianceFundRender component1 = go1.GetComponent<AllianceFundRender>();
    component1.FeedData(this.data.fund);
    this.fundList.Add(component1);
    GameObject go2 = NGUITools.AddChild(this.individualGrid.gameObject, this.honorTemplate);
    NGUITools.SetActive(go2, true);
    PersonalHonorRender component2 = go2.GetComponent<PersonalHonorRender>();
    component2.FeedData(this.data.honor);
    this.honorList.Add(component2);
    Dictionary<ItemStaticInfo, int>.Enumerator enumerator = this.data.individualRewards.GetEnumerator();
    while (enumerator.MoveNext())
    {
      GameObject gameObject = Object.Instantiate<GameObject>(this.itemTemplate);
      gameObject.transform.parent = this.individualGrid.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localScale = Vector3.one;
      gameObject.SetActive(true);
      IAPDailyRewardItemRender component3 = gameObject.GetComponent<IAPDailyRewardItemRender>();
      ItemStaticInfo key = enumerator.Current.Key;
      int itemCount = enumerator.Current.Value;
      component3.SetData(key, itemCount);
      this.individualItems.Add(component3);
    }
    this.individualGrid.Reposition();
  }

  public void ClearData()
  {
    for (int index = 0; index < this.allianceItems.Count; ++index)
    {
      this.allianceItems[index].transform.parent = (Transform) null;
      this.allianceItems[index].Release();
      this.allianceItems[index].gameObject.SetActive(false);
      Object.Destroy((Object) this.allianceItems[index].gameObject);
    }
    this.allianceItems.Clear();
    for (int index = 0; index < this.individualItems.Count; ++index)
    {
      this.individualItems[index].transform.parent = (Transform) null;
      this.individualItems[index].Release();
      this.individualItems[index].gameObject.SetActive(false);
      Object.Destroy((Object) this.individualItems[index].gameObject);
    }
    this.individualItems.Clear();
    for (int index = 0; index < this.fundList.Count; ++index)
    {
      this.fundList[index].transform.parent = (Transform) null;
      this.fundList[index].gameObject.SetActive(false);
      Object.Destroy((Object) this.fundList[index].gameObject);
    }
    this.fundList.Clear();
    for (int index = 0; index < this.honorList.Count; ++index)
    {
      this.honorList[index].transform.parent = (Transform) null;
      this.honorList[index].gameObject.SetActive(false);
      Object.Destroy((Object) this.honorList[index].gameObject);
    }
    this.honorList.Clear();
  }
}
