﻿// Decompiled with JetBrains decompiler
// Type: ConfigBuilding
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigBuilding
{
  private ConfigParseEx<BuildingInfo> _parse = new ConfigParseEx<BuildingInfo>();
  private List<BuildingInfo> _ruralBuildings = new List<BuildingInfo>();
  private List<BuildingInfo> _urbanBuildings = new List<BuildingInfo>();
  private Dictionary<string, int> _maxLevels = new Dictionary<string, int>();
  private bool _zoneBuildingReady;
  private bool _maxLevelsReady;

  public void BuildDB(object res)
  {
    this._parse.Initialize(res as Hashtable);
  }

  public BuildingInfo GetData(string buildName, int level)
  {
    return this.GetData(buildName + "_" + level.ToString());
  }

  public BuildingInfo GetData(string id)
  {
    return this.GetData(this._parse.s2i(id));
  }

  public BuildingInfo GetData(int internalId)
  {
    BuildingInfo buildingInfo = this._parse.Get(internalId);
    if (buildingInfo != null)
      buildingInfo.internalId = internalId;
    return buildingInfo;
  }

  public int GetMaxLevelByType(string type)
  {
    if (!this._maxLevelsReady)
    {
      List<int> internalIds = this._parse.InternalIds;
      for (int index = 0; index < internalIds.Count; ++index)
      {
        BuildingInfo data = this.GetData(internalIds[index]);
        int num;
        if (!this._maxLevels.TryGetValue(data.Type, out num))
          this._maxLevels[data.Type] = num = 0;
        if (data.Building_Lvl > num)
          this._maxLevels[data.Type] = data.Building_Lvl;
      }
      this._maxLevelsReady = true;
    }
    return this._maxLevels[type];
  }

  public void Clear()
  {
    this._ruralBuildings.Clear();
    this._urbanBuildings.Clear();
    this._maxLevels.Clear();
    this._zoneBuildingReady = false;
    this._maxLevelsReady = false;
  }

  public List<BuildingInfo> GetBuildingInfosOfZone(int cityZone)
  {
    if (!this._zoneBuildingReady)
    {
      List<int> internalIds = this._parse.InternalIds;
      for (int index = 0; index < internalIds.Count; ++index)
      {
        BuildingInfo data = this.GetData(internalIds[index]);
        if (data.Building_Lvl == 1)
        {
          if (data.City_Zone == "rural")
            this._ruralBuildings.Add(data);
          else if (data.City_Zone == "urban")
            this._urbanBuildings.Add(data);
        }
      }
      this._zoneBuildingReady = true;
    }
    switch (cityZone)
    {
      case 1:
        return this._ruralBuildings;
      case 2:
        return this._urbanBuildings;
      default:
        return (List<BuildingInfo>) null;
    }
  }
}
