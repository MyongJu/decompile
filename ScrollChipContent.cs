﻿// Decompiled with JetBrains decompiler
// Type: ScrollChipContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ScrollChipContent : MonoBehaviour
{
  public GameObject empty;
  public UIGrid grid;
  public EquipmentScrollChipComponent template;
  public UIScrollView scrollView;
  public EquipmentScrollChipComponent leftChip;
  public EquipmentScrollComponent scroll;
  public GameObject questionMark;
  public GameObject leftInfo;
  public Icon requireIcon;
  public UILabel descriptionLabel;
  public Icon tipIcon;
  public UIButton synthesizeBtn;
  private List<EquipmentScrollChipComponent> contentList;
  private List<ConfigEquipmentScrollChipInfo> currentList;
  private EquipmentScrollChipComponent current;
  private int currentSelectInfoID;
  private BagType bagType;

  public void Init(BagType bagType, int selectInfoID = -1)
  {
    this.ClearGrid();
    this.bagType = bagType;
    this.current = (EquipmentScrollChipComponent) null;
    if (selectInfoID != -1)
      this.currentSelectInfoID = selectInfoID;
    List<ConsumableItemData> consumableScrollChipItems = ItemBag.Instance.GetConsumableScrollChipItems(bagType);
    this.currentList = new List<ConfigEquipmentScrollChipInfo>();
    for (int index = 0; index < consumableScrollChipItems.Count; ++index)
      this.currentList.Add(ConfigManager.inst.GetEquipmentScrollChip(bagType).GetDataByItemID(consumableScrollChipItems[index].internalId));
    this.Sort(this.currentList);
    this.empty.SetActive(this.currentList.Count == 0);
    this.leftInfo.SetActive(this.currentList.Count > 0);
    this.contentList = new List<EquipmentScrollChipComponent>();
    for (int index = 0; index < this.currentList.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.template.gameObject);
      gameObject.SetActive(true);
      EquipmentScrollChipComponent component = gameObject.GetComponent<EquipmentScrollChipComponent>();
      component.OnSelectedHandler = new System.Action<EquipmentScrollChipComponent>(this.OnSelectedHandler);
      this.contentList.Add(component);
      component.FeedData((IComponentData) new EquipmentScrollChipComponetData(this.currentList[index], bagType));
      if (component.chipInfo.internalId == this.currentSelectInfoID)
      {
        this.current = component;
        this.current.Select = true;
      }
    }
    if ((UnityEngine.Object) this.current == (UnityEngine.Object) null && this.contentList.Count > 0)
    {
      this.current = this.contentList[0];
      this.current.Select = true;
    }
    if ((UnityEngine.Object) this.current != (UnityEngine.Object) null)
      this.FeedCurrent();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() =>
    {
      this.grid.Reposition();
      this.scrollView.ResetPosition();
    }));
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataChanged);
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemDataChanged);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnItemDataChanged);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnItemDataChanged);
  }

  public void Dispose()
  {
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataChanged);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnItemDataChanged);
  }

  private void OnItemDataChanged(int itemID)
  {
    this.Init(this.bagType, this.currentSelectInfoID);
  }

  private void OnSelectedHandler(EquipmentScrollChipComponent selected)
  {
    if (!((UnityEngine.Object) this.current != (UnityEngine.Object) null) || this.current.chipInfo.internalId == selected.chipInfo.internalId)
      return;
    this.currentSelectInfoID = selected.chipInfo.internalId;
    this.current.Select = false;
    this.current = selected;
    this.current.Select = true;
    this.FeedCurrent();
  }

  private void FeedCurrent()
  {
    this.leftChip.FeedData((IComponentData) new EquipmentScrollChipComponetData(this.current.chipInfo, this.bagType));
    int combineReqValue = this.current.chipInfo.combineReqValue;
    int itemCount = ItemBag.Instance.GetItemCount(this.current.scrollChipComponentData.ItemInfo.internalId);
    this.requireIcon.FeedData((IComponentData) new IconData(this.current.scrollChipComponentData.ItemInfo.ImagePath, new string[1]
    {
      (combineReqValue <= itemCount ? "[FFFFFF]" : "[FF0000]") + (object) itemCount + "[-]/" + (object) combineReqValue
    }));
    this.synthesizeBtn.isEnabled = combineReqValue <= itemCount;
    this.currentSelectInfoID = this.current.chipInfo.internalId;
    ConfigEquipmentScrollCombine scrollChipCombine = ConfigManager.inst.GetEquipmentScrollChipCombine(this.bagType);
    if (scrollChipCombine.GetCombineTarget(this.currentSelectInfoID) == null)
      return;
    if (scrollChipCombine.GetCombineTarget(this.currentSelectInfoID).Count == 1)
    {
      ConfigEquipmentScrollInfo data = ConfigManager.inst.GetEquipmentScroll(this.bagType).GetData(scrollChipCombine.GetCombineTarget(this.currentSelectInfoID)[0]);
      int itemId = data.itemID;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
      this.descriptionLabel.text = ScriptLocalization.GetWithMultyContents("forge_armory_scroll_fragments_synthesize_description", true, "[u][" + EquipmentConst.QULITY_HEX_COLOR_MAP[data.quality] + "]" + itemStaticInfo.LocName + "[/u]");
      this.scroll.gameObject.SetActive(true);
      this.questionMark.SetActive(false);
      this.scroll.FeedData((IComponentData) new EquipmentScrollComponetData(data, this.bagType));
      this.tipIcon.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
      this.tipIcon.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
      this.tipIcon.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
      this.tipIcon.data = (IComponentData) new IconData()
      {
        Data = (object) itemId
      };
    }
    else
    {
      this.descriptionLabel.text = this.current.scrollChipComponentData.ItemInfo.LocDescription;
      this.scroll.gameObject.SetActive(false);
      this.questionMark.SetActive(true);
      this.tipIcon.OnIconClickDelegate = (System.Action<Icon>) null;
      this.tipIcon.OnIconPressDelegate = (System.Action<Icon>) null;
      this.tipIcon.OnIconRelaseDelegate = (System.Action<Icon>) null;
    }
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.ShowItemTip((int) data.Data, this.tipIcon.transform, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.DelayShowTip((int) data.Data, this.tipIcon.transform, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.grid);
  }

  public void OnSynthesizeClick()
  {
    ConfigEquipmentScrollChipInfo chipInfo = this.current.chipInfo;
    EquipmentManager.Instance.CombinScroll(this.bagType, this.current.chipInfo.internalId, 1, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      ConfigEquipmentScrollInfo data1 = ConfigManager.inst.GetEquipmentScroll(this.bagType).GetData(int.Parse((data as Hashtable)[(object) "item_property_id"].ToString()));
      UIManager.inst.OpenPopup("Equipment/SynthesizeSuccessfullyPopup", (Popup.PopupParameter) new ScrollChipSynthesizeSuccessfullyPopup.Parameter()
      {
        bagType = this.bagType,
        scrollInfo = data1,
        scrollChipInfo = chipInfo,
        OnViewScroll = (System.Action<int>) (obj => this.GetComponentInParent<EquipmentTreasuryDlg>().ViewScroll(obj))
      });
    }));
  }

  public void OnSellClick()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.current.chipInfo.itemID);
    (UIManager.inst.OpenPopup("SellScrollChipPopup", (Popup.PopupParameter) new SellItemBasePopup.Parameter()
    {
      itemInfo = itemStaticInfo,
      bagType = this.bagType
    }) as SellScrollChipPopup).UpdateScrollChipInfo(this.current.chipInfo);
  }

  private void Sort(List<ConfigEquipmentScrollChipInfo> list)
  {
    list.Sort((Comparison<ConfigEquipmentScrollChipInfo>) ((x, y) =>
    {
      if (x.quality == y.quality)
        return y.level.CompareTo(x.level);
      return y.quality.CompareTo(x.quality);
    }));
  }
}
