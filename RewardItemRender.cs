﻿// Decompiled with JetBrains decompiler
// Type: RewardItemRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RewardItemRender : MonoBehaviour
{
  public UITexture icon;
  public UITexture itemFrame;
  public UILabel itemCount;
  public GameObject checkbox;
  private int count;
  private int itemId;
  public System.Action<RewardItemRender> OnItemClickedHandler;
  public System.Action<RewardItemRender> OnCheckBoxClickedHandler;
  private RewardItemData data;
  private bool isSelected;

  public RewardItemData Data
  {
    get
    {
      return this.data;
    }
  }

  public bool Select
  {
    get
    {
      return this.isSelected;
    }
    set
    {
      this.isSelected = value;
      if (value)
        this.checkbox.SetActive(true);
      else
        this.checkbox.SetActive(false);
    }
  }

  public void FeedData(RewardItemData data)
  {
    if (data == null)
      return;
    this.data = data;
    this.count = data.count;
    this.itemId = data.itemId;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.itemId);
    if (itemStaticInfo == null)
      return;
    this.itemCount.text = this.count.ToString();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    Utils.SetItemBackground(this.itemFrame, this.itemId);
  }

  public void OnItemClicked()
  {
    if (this.OnItemClickedHandler == null || this.isSelected)
      return;
    this.OnItemClickedHandler(this);
  }

  public void OnCheckBoxClicked()
  {
    if (this.OnCheckBoxClickedHandler == null || !this.isSelected)
      return;
    this.OnCheckBoxClickedHandler(this);
  }

  public void OnInfoBtnClicked()
  {
    Utils.ShowItemTip(this.itemId, this.itemFrame.transform, 0L, 0L, 0);
  }
}
