﻿// Decompiled with JetBrains decompiler
// Type: AllianceHealItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class AllianceHealItem : MonoBehaviour
{
  [SerializeField]
  protected Icon m_icon;
  [SerializeField]
  protected StandardProgressBar m_progressBar;
  [SerializeField]
  protected UILabel m_totalCountLabel;
  [SerializeField]
  protected UIButton m_dismissButton;
  public System.Action<AllianceHealItem> OnValueChanged;
  protected string m_unitId;
  protected int m_maxCount;
  protected DissmissHealTroopPopup.HospitalType m_hospitalType;
  protected Unit_StatisticsInfo m_unitInfo;

  public Unit_StatisticsInfo UnitInfo
  {
    get
    {
      return this.m_unitInfo;
    }
  }

  public string GetUnitId()
  {
    return this.m_unitId;
  }

  public void SetData(string unitId, int currentCount, int maxCount, bool underHealing, DissmissHealTroopPopup.HospitalType hospitalType = DissmissHealTroopPopup.HospitalType.AllianceHospital)
  {
    this.m_unitId = unitId;
    this.m_maxCount = maxCount;
    this.m_hospitalType = hospitalType;
    this.m_unitInfo = ConfigManager.inst.DB_Unit_Statistics.GetData(this.m_unitId);
    if (this.m_unitId == null)
      return;
    this.m_progressBar.Init(currentCount, maxCount);
    this.m_icon.FeedData((IComponentData) new IconData(this.m_unitInfo.Troop_ICON, new string[2]
    {
      Utils.XLAT(this.m_unitInfo.Troop_Name_LOC_ID),
      Utils.GetRomaNumeralsBelowTen(this.m_unitInfo.Troop_Tier)
    }));
    this.m_totalCountLabel.text = "/" + maxCount.ToString();
    this.m_progressBar.Enabled(!underHealing);
    this.m_dismissButton.gameObject.SetActive(!underHealing);
    this.m_progressBar.onValueChanged += new System.Action<int>(this.OnProgressBarValueChange);
  }

  public void Dispose()
  {
    this.m_icon.Dispose();
  }

  public void OnDismissButtonClick()
  {
    (UIManager.inst.OpenPopup("DissmissHealTroopPopup", (Popup.PopupParameter) null) as DissmissHealTroopPopup).Init(new HealTroopInfo(this.m_unitId, this.m_maxCount, 0), this.m_hospitalType);
  }

  public int CurrenCount
  {
    get
    {
      return this.m_progressBar.CurrentCount;
    }
    set
    {
      this.m_progressBar.CurrentCount = value;
    }
  }

  public int MaxCount
  {
    get
    {
      return this.m_maxCount;
    }
  }

  protected void OnProgressBarValueChange(int count)
  {
    this.OnValueChanged(this);
  }
}
