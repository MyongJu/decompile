﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossRankRewardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class AllianceBossRankRewardInfo
{
  private Dictionary<int, float> rewardRatioDic = new Dictionary<int, float>();
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "rank")]
  public int rank;
  [Config(Name = "fund_ratio")]
  public float fundRatio;
  [Config(Name = "honor_ratio")]
  public float honorRatio;
  [Config(Name = "reward_ratio_1")]
  public float rewardRatio1;
  [Config(Name = "reward_ratio_2")]
  public float rewardRatio2;
  [Config(Name = "reward_ratio_3")]
  public float rewardRatio3;
  [Config(Name = "reward_ratio_4")]
  public float rewardRatio4;
  [Config(Name = "reward_ratio_5")]
  public float rewardRatio5;
  [Config(Name = "reward_ratio_6")]
  public float rewardRatio6;

  public Dictionary<int, float> GetRewardRatioDic()
  {
    if (this.rewardRatioDic.Count > 0)
      return this.rewardRatioDic;
    this.rewardRatioDic.Add(0, this.rewardRatio1);
    this.rewardRatioDic.Add(1, this.rewardRatio2);
    this.rewardRatioDic.Add(2, this.rewardRatio3);
    this.rewardRatioDic.Add(3, this.rewardRatio4);
    this.rewardRatioDic.Add(4, this.rewardRatio5);
    this.rewardRatioDic.Add(5, this.rewardRatio6);
    return this.rewardRatioDic;
  }
}
