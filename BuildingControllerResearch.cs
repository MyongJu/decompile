﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerResearch
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class BuildingControllerResearch : BuildingControllerNew
{
  public override void AddActionButton(ref List<CityCircleBtnPara> ftl)
  {
    base.AddActionButton(ref ftl);
    if (this.IsBuilding())
      return;
    ftl.Add(this.CCBPair["research"]);
  }

  public override bool IsIdleState()
  {
    if (base.IsIdleState())
      return null == ResearchManager.inst.CurrentResearch;
    return false;
  }
}
