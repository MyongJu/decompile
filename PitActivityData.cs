﻿// Decompiled with JetBrains decompiler
// Type: PitActivityData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class PitActivityData
{
  private PitActivityData.SignupData _curSignupData = new PitActivityData.SignupData();
  private List<int> _allClaimedScoreReward = new List<int>();
  private int _startTime;
  private int _endTime;
  private int _regStartTime;
  private int _regEndTime;
  private int _round;
  private int _duration;
  private int _interval;
  private int _status;
  private int _reopenTime;
  private long _currentScore;

  public int StartTime
  {
    get
    {
      return this._startTime;
    }
  }

  public int EndTime
  {
    get
    {
      return this._endTime;
    }
  }

  public int RegStartTime
  {
    get
    {
      return this._regStartTime;
    }
  }

  public int RegEndTime
  {
    get
    {
      return this._regEndTime;
    }
  }

  public int Round
  {
    get
    {
      return this._round;
    }
  }

  public int Duration
  {
    get
    {
      return this._duration;
    }
  }

  public int Interval
  {
    get
    {
      return this._interval;
    }
  }

  public bool IsActivityOpened
  {
    get
    {
      return this._status == 1;
    }
  }

  public bool IsActivityShouldOpen
  {
    get
    {
      return this._startTime > 0;
    }
  }

  public int ReopenTime
  {
    get
    {
      return this._reopenTime;
    }
  }

  public PitActivityData.SignupData CurSignupData
  {
    get
    {
      return this._curSignupData;
    }
  }

  public long CurrentScore
  {
    get
    {
      return this._currentScore;
    }
  }

  public List<int> AllClaimedScoreReward
  {
    get
    {
      return this._allClaimedScoreReward;
    }
  }

  public int GetCanClaimScoreRewardCount()
  {
    int num = 0;
    using (List<AbyssCollectRewardInfo>.Enumerator enumerator = ConfigManager.inst.DB_AbyssCollectReward.AllCollectRewardInfo.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AbyssCollectRewardInfo current = enumerator.Current;
        if ((long) current.NumReq <= this.CurrentScore && !this.AllClaimedScoreReward.Contains(current.internalId))
          ++num;
      }
    }
    return num;
  }

  public void Decode(Hashtable data)
  {
    if (data == null)
      return;
    if (data.ContainsKey((object) "config"))
    {
      Hashtable inData = data[(object) "config"] as Hashtable;
      if (inData != null)
      {
        DatabaseTools.UpdateData(inData, "start_time", ref this._startTime);
        DatabaseTools.UpdateData(inData, "end_time", ref this._endTime);
        DatabaseTools.UpdateData(inData, "reg_start_time", ref this._regStartTime);
        DatabaseTools.UpdateData(inData, "reg_end_time", ref this._regEndTime);
        DatabaseTools.UpdateData(inData, "round", ref this._round);
        DatabaseTools.UpdateData(inData, "duration", ref this._duration);
        DatabaseTools.UpdateData(inData, "interval", ref this._interval);
        DatabaseTools.UpdateData(inData, "status", ref this._status);
        DatabaseTools.UpdateData(inData, "reopen_time", ref this._reopenTime);
      }
    }
    if (data.ContainsKey((object) "reg_info"))
      this._curSignupData.Decode(data[(object) "reg_info"] as Hashtable);
    this._currentScore = 0L;
    this._allClaimedScoreReward.Clear();
    if (!data.Contains((object) "step_rewards"))
      return;
    Hashtable inData1 = data[(object) "step_rewards"] as Hashtable;
    if (inData1 == null)
      return;
    DatabaseTools.UpdateData(inData1, "total_score", ref this._currentScore);
    if (!inData1.ContainsKey((object) "claimed"))
      return;
    ArrayList arrayList = inData1[(object) "claimed"] as ArrayList;
    if (arrayList == null)
      return;
    foreach (object obj in arrayList)
    {
      int result = 0;
      if (int.TryParse(obj.ToString(), out result))
        this._allClaimedScoreReward.Add(result);
    }
  }

  public class Params
  {
    public const string START_TIME = "start_time";
    public const string END_TIME = "end_time";
    public const string REG_START_TIME = "reg_start_time";
    public const string REG_END_TIME = "reg_end_time";
    public const string ROUND = "round";
    public const string DURATION = "duration";
    public const string INTERVAL = "interval";
    public const string STATUS = "status";
    public const string REOPEN_TIME = "reopen_time";
    public const string CTIME = "ctime";
    public const string STEP_REWARDS = "step_rewards";
    public const string TOTAL_SCORE = "total_score";
    public const string CLAIMED = "claimed";
  }

  public class SignupData
  {
    private int _signupTime;
    private int _signupRound;
    private int _startTime;
    private int _endTime;
    private int _entered;

    public int SignupTime
    {
      get
      {
        return this._signupTime;
      }
    }

    public int SignupRound
    {
      get
      {
        return this._signupRound;
      }
    }

    public int StartTime
    {
      get
      {
        return this._startTime;
      }
    }

    public int EndTime
    {
      get
      {
        return this._endTime;
      }
    }

    public bool Entered
    {
      get
      {
        return this._entered != 0;
      }
    }

    public void Decode(Hashtable data)
    {
      if (data == null)
      {
        this.Clear();
      }
      else
      {
        DatabaseTools.UpdateData(data, "reg_time", ref this._signupTime);
        DatabaseTools.UpdateData(data, "round", ref this._signupRound);
        DatabaseTools.UpdateData(data, "start_time", ref this._startTime);
        DatabaseTools.UpdateData(data, "end_time", ref this._endTime);
        DatabaseTools.UpdateData(data, "entered", ref this._entered);
      }
    }

    public void Clear()
    {
      this._signupTime = 0;
      this._signupRound = 0;
    }

    public class Params
    {
      public const string REG_TIME = "reg_time";
      public const string START_TIME = "start_time";
      public const string END_TIME = "end_time";
      public const string ROUND = "round";
      public const string ENTERED = "entered";
    }
  }
}
