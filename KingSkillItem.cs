﻿// Decompiled with JetBrains decompiler
// Type: KingSkillItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class KingSkillItem : MonoBehaviour
{
  [SerializeField]
  private UITexture _textureIcon;
  [SerializeField]
  private UILabel _labelName;
  [SerializeField]
  private UILabel _labelDesc;
  private KingSkillInfo _kingSkillInfo;

  public void SetSkillData(KingSkillInfo kingSkillInfo)
  {
    this._kingSkillInfo = kingSkillInfo;
    if (kingSkillInfo == null)
      return;
    HallOfKingData hallOfKingData = DBManager.inst.DB_HallOfKing.Get((long) PlayerData.inst.userData.world_id);
    if (hallOfKingData == null)
    {
      D.error((object) "can not find hall of king data.");
    }
    else
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureIcon, kingSkillInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this._labelName.text = kingSkillInfo.Loc_Name;
      this._labelDesc.text = ScriptLocalization.GetWithPara(this._kingSkillInfo.Loc_Desc, new Dictionary<string, string>()
      {
        {
          "0",
          this._kingSkillInfo.Cost.ToString()
        },
        {
          "1",
          ((int) ((double) this._kingSkillInfo.SkillParam1 - (double) this._kingSkillInfo.SkillParam2 * (double) hallOfKingData.GetSkillUsedTimes(this._kingSkillInfo.internalId))).ToString()
        },
        {
          "2",
          this._kingSkillInfo.MaxUse.ToString()
        }
      }, false);
    }
  }

  public void OnButtonUseClicked()
  {
    UIManager.inst.OpenPopup("HallOfKing/KingSkillInfoPopup", (Popup.PopupParameter) new KingSkillInfoPopup.Parameter()
    {
      SkillInfo = this._kingSkillInfo
    });
  }
}
