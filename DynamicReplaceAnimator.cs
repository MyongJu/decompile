﻿// Decompiled with JetBrains decompiler
// Type: DynamicReplaceAnimator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DynamicReplaceAnimator
{
  public virtual void PlayReplaceAnimation(DynamicReplaceItem ori, object args)
  {
    GameObject gameObject = Utils.DuplicateGOB(ori.gameObject);
    Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(ori.transform);
    Vector3 localPosition = ori.transform.localPosition;
    localPosition.x -= relativeWidgetBounds.size.x;
    ori.transform.localPosition = localPosition;
    gameObject.GetComponent<DynamicReplaceItem>().FlyOut((System.Action) null);
    ori.Init(args);
    ori.FlyIn((System.Action) (() => {}));
  }
}
