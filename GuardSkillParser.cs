﻿// Decompiled with JetBrains decompiler
// Type: GuardSkillParser
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public static class GuardSkillParser
{
  public static GuardSkill Parse(GameObject go, TextAsset script)
  {
    if (!(bool) ((UnityEngine.Object) script))
      return (GuardSkill) null;
    Hashtable hashtable1 = Utils.Json2Object(script.text, true) as Hashtable;
    if (hashtable1 == null)
      return (GuardSkill) null;
    ArrayList arrayList = hashtable1[(object) "Stages"] as ArrayList;
    if (arrayList == null)
      return (GuardSkill) null;
    if (string.IsNullOrEmpty(hashtable1[(object) "Take"] as string))
      return (GuardSkill) null;
    GuardSkill guardSkill = new GuardSkill();
    guardSkill.Take = hashtable1[(object) "Take"] as string;
    for (int index = 0; index < arrayList.Count; ++index)
    {
      Hashtable hashtable2 = arrayList[index] as Hashtable;
      if (hashtable2 != null)
        guardSkill.EventDataList.Add(new GuardEventData()
        {
          EventType = GuardSkillParser.ParseEventType(hashtable2[(object) "Event"] as string),
          Actions = GuardEventDataParser.ParseActions(go, (object) hashtable2)
        });
    }
    return guardSkill;
  }

  private static GuardEventType ParseEventType(string eventType)
  {
    try
    {
      return (GuardEventType) Enum.Parse(typeof (GuardEventType), eventType);
    }
    catch
    {
      return GuardEventType.Unknown;
    }
  }
}
