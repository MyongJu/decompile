﻿// Decompiled with JetBrains decompiler
// Type: VerificationPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using UI;
using UnityEngine;

public class VerificationPopup : Popup
{
  private List<Icon> totals = new List<Icon>();
  private string[] _city = new string[35]
  {
    "11",
    "12",
    "13",
    "14",
    "15",
    "21",
    "22",
    "23",
    "31",
    "32",
    "33",
    "34",
    "35",
    "36",
    "37",
    "41",
    "42",
    "43",
    "44",
    "45",
    "46",
    "50",
    "51",
    "52",
    "53",
    "54",
    "61",
    "62",
    "63",
    "64",
    "65",
    "71",
    "81",
    "82",
    "91"
  };
  public UILabel title;
  public UIGrid grid;
  public Icon itemRewardPrefab;
  public UIInput userName;
  public UILabel userNameError;
  public UIInput userCard;
  public UILabel userCardError;
  public UIInput userTel;
  public UILabel userTelError;
  public UIInput validateCode;
  public UILabel validateCodeError;
  public UIButton sendButton;
  public UIButton saveButton;
  public UILabel msgCd;
  private int remainTime;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.title.text = ScriptLocalization.Get("verification_user_title", true);
    this.UpdateUI();
  }

  public void OnBackHandler()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnUserNameChange()
  {
    bool isValidate = this.IsChinaName(this.userName.value);
    if (string.IsNullOrEmpty(this.userName.value))
      isValidate = true;
    this.VerificationItem(this.userName, this.userNameError, isValidate);
    this.ChangeButtonState();
  }

  public void OnUserCardChange()
  {
    bool isValidate = this.IsCardNumber(this.userCard.value);
    if (string.IsNullOrEmpty(this.userCard.value))
      isValidate = true;
    this.VerificationItem(this.userCard, this.userCardError, isValidate);
    this.ChangeButtonState();
  }

  public void OnUserTelChange()
  {
    bool isValidate = this.IsPhoneNo(this.userTel.value);
    if (string.IsNullOrEmpty(this.userTel.value))
      isValidate = true;
    this.VerificationItem(this.userTel, this.userTelError, isValidate);
    this.ChangeButtonState();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    for (int index = 0; index < this.totals.Count; ++index)
    {
      this.totals[index].OnIconClickDelegate = (System.Action<Icon>) null;
      this.totals[index].OnIconPressDelegate = (System.Action<Icon>) null;
      this.totals[index].OnIconRelaseDelegate = (System.Action<Icon>) null;
      this.totals[index].Dispose();
    }
    this.totals.Clear();
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnProcess);
    if (!AccountManager.Instance.NeedToShowCertifying())
      return;
    AccountManager.Instance.LockShowCertifyingFlag();
    CitadelSystem.inst.CheckTutorial();
    CitadelSystem.inst.OpenSignInPopUp();
  }

  private void UpdateUI()
  {
    this.DrawRewardItems();
    this.Verification();
  }

  public void SendValidCode()
  {
    if (this.remainTime > 0)
      return;
    RequestManager.inst.SendRequest("UserIdentity:sentCertCode", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "tel", (object) this.userTel.value), new System.Action<bool, object>(this.OnGetValidateCode), true);
  }

  public void OnSaveData()
  {
    this.Verification();
    if (!this.IsChinaName(this.userName.value))
      this.Warning("validate_user_name_error");
    else if (!this.IsCardNumber(this.userCard.value))
      this.Warning("validate_user_card_error");
    else if (!this.IsPhoneNo(this.userTel.value))
      this.Warning("validate_user_tel_error");
    else
      RequestManager.inst.SendRequest("UserIdentity:identify", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "name", (object) this.userName.value, (object) "identity", (object) this.userCard.value, (object) "tel", (object) this.userTel.value, (object) "code", (object) this.validateCode.value), new System.Action<bool, object>(this.OnSaveValidateCode), true);
  }

  private void Warning(string contentKey)
  {
    UIManager.inst.ShowConfirmationBox(ScriptLocalization.Get("validate_warning_title", true), ScriptLocalization.Get(contentKey, true), ScriptLocalization.Get("id_uppercase_confirm", true), ScriptLocalization.Get("id_uppercase_cancel", true), ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) null, (System.Action) null, (System.Action) null);
  }

  private void OnSaveValidateCode(bool success, object payload)
  {
    if (!success)
      return;
    Dictionary<int, int> totalRewards = ConfigManager.inst.DB_VerificationReward.GetTotalRewards();
    RewardsCollectionAnimator.Instance.Clear();
    if (totalRewards.Count > 0)
    {
      Dictionary<int, int>.KeyCollection.Enumerator enumerator = totalRewards.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(enumerator.Current);
        RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
        {
          icon = itemStaticInfo.ImagePath,
          count = (float) totalRewards[enumerator.Current]
        });
      }
      AudioManager.Instance.PlaySound("sfx_ui_signin", false);
      RewardsCollectionAnimator.Instance.CollectItems(false);
    }
    AccountManager.Instance.SetVerification();
    this.OnCloseHandler();
  }

  private void DrawRewardItems()
  {
    Dictionary<int, int> totalRewards = ConfigManager.inst.DB_VerificationReward.GetTotalRewards();
    List<int> totalItems = ConfigManager.inst.DB_VerificationReward.GetTotalItems();
    RewardsCollectionAnimator.Instance.Clear();
    if (totalRewards.Count <= 0)
      return;
    for (int index = 0; index < totalItems.Count; ++index)
    {
      IconData iconData = new IconData(ConfigManager.inst.DB_Items.GetItem(totalItems[index]).ImagePath, new string[1]
      {
        totalRewards[totalItems[index]].ToString()
      });
      iconData.Data = (object) totalItems[index];
      Icon icon = this.GetIcon();
      icon.StopAutoFillName();
      icon.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
      icon.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
      icon.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
      icon.FeedData((IComponentData) iconData);
    }
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  private Icon GetIcon()
  {
    GameObject go = NGUITools.AddChild(this.grid.gameObject, this.itemRewardPrefab.gameObject);
    NGUITools.SetActive(go, true);
    Icon component = go.GetComponent<Icon>();
    this.totals.Add(component);
    return component;
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnGetValidateCode(bool success, object payload)
  {
    if (!success)
      return;
    this.remainTime = 60;
    this.sendButton.isEnabled = false;
    this.msgCd.text = ScriptLocalization.Get("account_verify_identity_phone_code_send", true) + " " + this.remainTime.ToString();
    this.AddEventHandler();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnProcess);
  }

  private void OnProcess(int time)
  {
    --this.remainTime;
    this.msgCd.text = ScriptLocalization.Get("account_verify_identity_phone_code_send", true) + " " + this.remainTime.ToString();
    if (this.remainTime > 0)
      return;
    this.remainTime = 0;
    this.sendButton.isEnabled = this.IsPhoneNo(this.userTel.value);
    this.msgCd.text = ScriptLocalization.Get("account_verify_identity_phone_code_send", true);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnProcess);
  }

  private void ChangeButtonState()
  {
    this.saveButton.isEnabled = this.IsChinaName(this.userName.value) && this.IsCardNumber(this.userCard.value) && this.IsPhoneNo(this.userTel.value) && this.IsCodeNumber(this.validateCode.value);
    this.sendButton.isEnabled = this.IsPhoneNo(this.userTel.value) && this.remainTime <= 0 && this.IsCardNumber(this.userCard.value) && this.IsChinaName(this.userName.value);
  }

  private void Verification()
  {
    bool isValidate1 = this.IsChinaName(this.userName.value);
    if (string.IsNullOrEmpty(this.userName.value))
      isValidate1 = true;
    this.VerificationItem(this.userName, this.userNameError, isValidate1);
    bool isValidate2 = this.IsCardNumber(this.userCard.value);
    if (string.IsNullOrEmpty(this.userCard.value))
      isValidate2 = true;
    this.VerificationItem(this.userCard, this.userCardError, isValidate2);
    bool isValidate3 = this.IsPhoneNo(this.userTel.value);
    if (string.IsNullOrEmpty(this.userTel.value))
      isValidate3 = true;
    this.VerificationItem(this.userTel, this.userTelError, isValidate3);
    this.ChangeButtonState();
  }

  public void OnVarificationCodeChange()
  {
    bool isValidate = this.IsCodeNumber(this.validateCode.value);
    if (string.IsNullOrEmpty(this.validateCode.value))
      isValidate = true;
    this.VerificationItem(this.validateCode, this.validateCodeError, isValidate);
    this.ChangeButtonState();
  }

  private void VerificationItem(UIInput input, UILabel error, bool isValidate)
  {
    NGUITools.SetActive(error.gameObject, false);
    if (isValidate)
      return;
    NGUITools.SetActive(error.gameObject, true);
  }

  private bool IsChinaName(string name)
  {
    return new Regex("^[一-龥]{1,8}$").IsMatch(name);
  }

  private bool IsPhoneNo(string no)
  {
    return new Regex("^1[34578]\\d{9}$").IsMatch(no);
  }

  private bool IsCodeNumber(string no)
  {
    return new Regex("^\\d{6}$").IsMatch(no);
  }

  private bool IsCardNumber(string card)
  {
    if (card.Length > 1 && Array.IndexOf<string>(this._city, card.Substring(0, 2)) < 0 || (!new Regex("(^\\d{15}$)|(^\\d{6}[1|2]\\d{10}(\\d|X|x)$)").IsMatch(card) || !this.CheckCardBirthday(card)))
      return false;
    return this.GetIDVerifyNumber(card);
  }

  private string Change15To18(string idCard)
  {
    if (idCard.Length != 15)
      return idCard;
    int[] numArray = new int[17]
    {
      7,
      9,
      10,
      5,
      8,
      4,
      2,
      1,
      6,
      3,
      7,
      9,
      10,
      5,
      8,
      4,
      2
    };
    string[] strArray = new string[11]
    {
      "1",
      "0",
      "X",
      "9",
      "8",
      "7",
      "6",
      "5",
      "4",
      "3",
      "2"
    };
    int num = 0;
    idCard = idCard.Substring(0, 6) + "19" + idCard.Substring(6, idCard.Length - 6);
    for (int startIndex = 0; startIndex < 17; ++startIndex)
    {
      string s = idCard.Substring(startIndex, 1);
      int result = 0;
      if (int.TryParse(s, out result))
        num += result * numArray[startIndex];
    }
    idCard += strArray[num % 11];
    return idCard;
  }

  private bool GetIDVerifyNumber(string idCard)
  {
    if (idCard.Length == 15)
      idCard = this.Change15To18(idCard);
    if (idCard.Length != 18)
      return false;
    string str = idCard.Substring(17, 1);
    int[] numArray = new int[17]
    {
      7,
      9,
      10,
      5,
      8,
      4,
      2,
      1,
      6,
      3,
      7,
      9,
      10,
      5,
      8,
      4,
      2
    };
    string[] strArray = new string[11]
    {
      "1",
      "0",
      "X",
      "9",
      "8",
      "7",
      "6",
      "5",
      "4",
      "3",
      "2"
    };
    int num = 0;
    for (int startIndex = 0; startIndex < 17; ++startIndex)
    {
      string s = idCard.Substring(startIndex, 1);
      int result = 0;
      if (int.TryParse(s, out result))
        num += result * numArray[startIndex];
    }
    return strArray[num % 11] == str;
  }

  private bool CheckCardBirthday(string card)
  {
    string empty = string.Empty;
    return this.VerifyBirthday(card.Length != 18 ? card.Substring(6, 6) : card.Substring(6, 8));
  }

  private bool VerifyBirthday(string date)
  {
    DateTime now = DateTime.Now;
    if (date.Length < 8)
      date = "19" + date;
    DateTime result;
    if (!DateTime.TryParseExact(date, "yyyyMMdd", (IFormatProvider) null, DateTimeStyles.None, out result))
      return false;
    int num = now.Year - result.Year;
    return num >= 0 && num <= 100;
  }

  private delegate bool Validate(string data);
}
