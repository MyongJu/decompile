﻿// Decompiled with JetBrains decompiler
// Type: MerlinMarchSlotItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class MerlinMarchSlotItem : MonoBehaviour
{
  [SerializeField]
  private UISlider _sliderProgress;
  [SerializeField]
  private UILabel _labelLeftTime;
  [SerializeField]
  private UILabel _labelDescription;
  protected System.Action<MerlinMarchSlotItem> _clickCallback;

  public void SetClickCallback(System.Action<MerlinMarchSlotItem> callback)
  {
    this._clickCallback = callback;
  }

  public void UpdateUI()
  {
    UserMerlinTowerData userData = MerlinTowerPayload.Instance.UserData;
    MagicTowerMainInfo currentTowerMainInfo = userData.CurrentTowerMainInfo;
    if (currentTowerMainInfo != null)
    {
      JobHandle job = JobManager.Instance.GetJob(userData.mineJobId);
      if (job != null)
      {
        this.gameObject.SetActive(true);
        int gratherTime = currentTowerMainInfo.GratherTime;
        int num = NetServerTime.inst.ServerTimestamp - job.StartTime();
        if (num > gratherTime)
          num = gratherTime;
        string str = Utils.FormatShortThousands(num * currentTowerMainInfo.Speed);
        this._sliderProgress.value = (float) num / (float) gratherTime;
        this._labelDescription.text = ScriptLocalization.Get("kingdom_gather_gathered_name", true) + str;
        this._labelLeftTime.text = Utils.FormatTime(job.LeftTime(), false, false, true);
      }
      else
        this.gameObject.SetActive(false);
    }
    else
      Logger.Error((object) "CurrentTowerMainInfo Can Not Be Null");
  }

  public void SecondTick()
  {
    this.UpdateUI();
  }

  public void OnButtonGotoClicked()
  {
    Logger.Log(nameof (OnButtonGotoClicked));
    if (this._clickCallback == null)
      return;
    this._clickCallback(this);
  }
}
