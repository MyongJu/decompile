﻿// Decompiled with JetBrains decompiler
// Type: WonderKingItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class WonderKingItem : MonoBehaviour
{
  public UITexture texture;
  public UILabel rank;
  public UILabel userName;
  public UILabel time;

  public void SetData(WonderKingUIData data)
  {
    CustomIconLoader.Instance.requestCustomIcon(this.texture, UserData.GetPortraitIconPath(data.Portrait), data.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.texture, data.LordTitleId, 2);
    this.userName.text = data.UserName;
    Color color = new Color(0.6941177f, 0.6941177f, 0.6941177f, 1f);
    if (!data.IsCurrentKing)
      this.userName.color = color;
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("0", data.Rank.ToString());
    this.rank.text = ScriptLocalization.GetWithPara("throne_history_king_num", para, true);
    color = new Color(0.4784314f, 0.4784314f, 0.4784314f, 1f);
    if (!data.IsCurrentKing)
      this.rank.color = color;
    para.Clear();
    para.Add("0", Utils.FormatTimeYYYYMMDD(data.Time, "/"));
    this.time.text = ScriptLocalization.GetWithPara("throne_history_king_date", para, true);
    if (data.IsCurrentKing)
      return;
    this.time.color = color;
  }

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.texture);
  }
}
