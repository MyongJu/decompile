﻿// Decompiled with JetBrains decompiler
// Type: AllianceTreasuryCountManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class AllianceTreasuryCountManager
{
  public AllianceTreasuryCountManager.TreasuryCountChangedHandler OnTreasuryCountChanged;
  protected int _unOpenedChestCount;
  private static AllianceTreasuryCountManager _Instance;

  public int UnOpenedChestCount
  {
    get
    {
      return this._unOpenedChestCount;
    }
    set
    {
      this._unOpenedChestCount = value;
    }
  }

  public static AllianceTreasuryCountManager Instance
  {
    get
    {
      if (AllianceTreasuryCountManager._Instance == null)
        AllianceTreasuryCountManager._Instance = new AllianceTreasuryCountManager();
      return AllianceTreasuryCountManager._Instance;
    }
  }

  public void Init()
  {
    this.OnTreasuryCountChanged = (AllianceTreasuryCountManager.TreasuryCountChangedHandler) null;
    MessageHub.inst.GetPortByAction("alliance_treasury_vault_count").AddEvent(new System.Action<object>(this.OnTreasuryVaultCountChanged));
  }

  public void SetUnopendChestCount(int unopendChestCount)
  {
    this._unOpenedChestCount = unopendChestCount;
    if (this.OnTreasuryCountChanged == null)
      return;
    this.OnTreasuryCountChanged(this._unOpenedChestCount);
  }

  protected void OnTreasuryVaultCountChanged(object data)
  {
    Hashtable hashtable1 = data as Hashtable;
    if (hashtable1 != null && hashtable1.ContainsKey((object) "count_list"))
    {
      Hashtable hashtable2 = hashtable1[(object) "count_list"] as Hashtable;
      if (hashtable2 != null && hashtable2.ContainsKey((object) PlayerData.inst.uid.ToString()))
        int.TryParse(hashtable2[(object) PlayerData.inst.uid.ToString()].ToString(), out this._unOpenedChestCount);
    }
    if (this.OnTreasuryCountChanged == null)
      return;
    this.OnTreasuryCountChanged(this.UnOpenedChestCount);
  }

  public delegate void TreasuryCountChangedHandler(int chestLibraryCount);
}
