﻿// Decompiled with JetBrains decompiler
// Type: ResearchingStatePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;

public class ResearchingStatePopup : Popup
{
  protected TechLevel techLevel;
  public UILabel titleLabel;
  public UILabel descriptionLabel;
  public UILabel levelLabel;
  public UITexture researchIcon;
  public UILabel totalLabel;
  public UILabel nextLabel;
  public UIButton infoBtn;
  public UIButton speedupBtn;
  public UIButton cancelResearchBtn;
  public UILabel researchLeftTime;
  public UILabel speedupGoldCostLabel;
  public UISlider progressSlider;
  private JobHandle currentJobHandle;
  private bool _isDestroy;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.techLevel = (orgParam as ResearchingStatePopup.Parameter).techLevel;
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Clean();
  }

  protected virtual void UpdateUI()
  {
    this.Init();
    this.UpdateResearchInfo();
    this.UpdateBonus();
  }

  private void OnDestroy()
  {
    this._isDestroy = true;
  }

  protected virtual void Clean()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
    this.cancelResearchBtn.onClick.Clear();
    this.speedupBtn.onClick.Clear();
  }

  protected virtual void Init()
  {
    this.currentJobHandle = JobManager.Instance.GetJob(ResearchManager.inst.GetJobId(this.techLevel));
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
    this.OnSecondHandler(0);
    this.cancelResearchBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnCancelResearchBtnClick)));
    this.speedupBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnGoldSpeedupBtnClick)));
  }

  private void OnSecondHandler(int timeStamp)
  {
    this.progressSlider.value = (float) (1.0 - (double) this.currentJobHandle.LeftTime() / (double) this.currentJobHandle.Duration());
    this.researchLeftTime.text = Utils.FormatTime(this.currentJobHandle.LeftTime(), true, false, true);
    this.speedupGoldCostLabel.text = ItemBag.CalculateCost(new Hashtable()
    {
      {
        (object) "time",
        (object) this.currentJobHandle.LeftTime()
      }
    }).ToString();
    if (!this.currentJobHandle.IsFinished())
      return;
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  protected virtual void UpdateResearchInfo()
  {
    this.descriptionLabel.text = this.techLevel.Desc;
    this.titleLabel.text = this.techLevel.Name;
    this.infoBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnInfoBtnClick)));
  }

  protected virtual void UpdateBonus()
  {
    if ((UnityEngine.Object) this.totalLabel != (UnityEngine.Object) null)
      this.totalLabel.text = this.FormattedModifier(this.techLevel.Effects[0].Property.Format, ResearchManager.inst.CurrentBonus(this.techLevel));
    if ((UnityEngine.Object) this.nextLabel != (UnityEngine.Object) null)
      this.nextLabel.text = this.FormattedModifier(this.techLevel.Effects[0].Property.Format, ResearchManager.inst.NextRank(this.techLevel));
    this.levelLabel.text = this.techLevel.Level.ToString() + "/" + (object) ResearchManager.inst.GetTechSteps(this.techLevel.Tech);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.researchIcon, this.techLevel.Tech.IconPath, (System.Action<bool>) null, true, false, string.Empty);
  }

  protected string FormattedModifier(PropertyDefinition.FormatType format, float Modifier)
  {
    switch (format)
    {
      case PropertyDefinition.FormatType.Percent:
        return string.Format("{0:P1}", (object) Modifier);
      case PropertyDefinition.FormatType.Seconds:
        return string.Format("{0}s", (object) Modifier);
      case PropertyDefinition.FormatType.Boolean:
        return string.Format("{0}", (object) ((double) Modifier > 0.0));
      default:
        return string.Format("{0:0}", (object) Modifier);
    }
  }

  private void OnInfoBtnClick()
  {
    UIManager.inst.OpenPopup("ResearchDetailInfoPopup", (Popup.PopupParameter) new ResearchDetailInfoPopup.UIParam()
    {
      techLevel = this.techLevel
    });
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnGoldSpeedupBtnClick()
  {
    long jobId = ResearchManager.inst.GetJobId(this.techLevel);
    GoldConsumePopup.Parameter parameter = new GoldConsumePopup.Parameter();
    Hashtable ht = new Hashtable();
    int num1 = JobManager.Instance.GetJob(jobId).LeftTime();
    int num2 = num1;
    ht.Add((object) "time", (object) num2);
    int cost = ItemBag.CalculateCost(ht);
    parameter.confirmButtonClickEvent = new System.Action(this.ConFirmCallBack);
    parameter.lefttime = num1;
    parameter.goldNum = cost;
    parameter.freetime = 0;
    parameter.description = ScriptLocalization.GetWithPara("confirm_gold_spend_cooldown_instant_desc", new Dictionary<string, string>()
    {
      {
        "num",
        cost.ToString()
      }
    }, true);
    UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) parameter);
  }

  private void ConFirmCallBack()
  {
    if (this._isDestroy)
      return;
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
    long jobId = ResearchManager.inst.GetJobId(this.techLevel);
    JobHandle job = JobManager.Instance.GetJob(jobId);
    Hashtable ht = new Hashtable();
    int num = job.LeftTime() - 0;
    if (num <= 0)
      return;
    ht.Add((object) "time", (object) num);
    int cost = ItemBag.CalculateCost(ht);
    RequestManager.inst.SendRequest("City:speedUpByGold", new Hashtable()
    {
      {
        (object) "job_id",
        (object) jobId
      },
      {
        (object) "gold",
        (object) cost
      }
    }, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    }), true);
  }

  private void OnCancelResearchBtnClick()
  {
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = ScriptLocalization.Get("research_cancel_research_title", true),
      content = (string.Format(Utils.XLAT("research_cancel_research_description"), (object) this.techLevel.Name) + "\n" + Utils.XLAT("half_resource_return")),
      yes = ScriptLocalization.Get("id_uppercase_yes", true),
      no = ScriptLocalization.Get("id_uppercase_no", true),
      yesCallback = new System.Action(this.OnCancelResearchYesBtnClick)
    });
  }

  private void OnCancelResearchYesBtnClick()
  {
    MessageHub.inst.GetPortByAction("Research:cancelResearch").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "city_id",
        (object) DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid).cityId
      },
      {
        (object) "research_id",
        (object) this.techLevel.InternalID
      }
    }, new System.Action<bool, object>(this.CancelResearchCallback), true);
  }

  private void CancelResearchCallback(bool ret, object data)
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public TechLevel techLevel;
  }
}
