﻿// Decompiled with JetBrains decompiler
// Type: EquipmentForgeTabBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EquipmentForgeTabBar : MonoBehaviour
{
  public System.Action<int> OnSelectedHandler;
  public UIScrollView scrollView;
  public GameObject upFlag;
  public GameObject downFlag;
  public UIButtonBar bar;

  private void OnEnable()
  {
    this.bar.OnSelectedHandler += new System.Action<int>(this.ButtonBarClickHandler);
    this.scrollView.onStoppedMoving += new UIScrollView.OnDragNotification(this.OnScrollViewStopped);
    this.scrollView.onMomentumMove += new UIScrollView.OnDragNotification(this.OnScrollViewStopped);
  }

  private void OnDisable()
  {
    this.bar.OnSelectedHandler -= new System.Action<int>(this.ButtonBarClickHandler);
    this.scrollView.onStoppedMoving -= new UIScrollView.OnDragNotification(this.OnScrollViewStopped);
    this.scrollView.onMomentumMove -= new UIScrollView.OnDragNotification(this.OnScrollViewStopped);
  }

  private void ButtonBarClickHandler(int index)
  {
    if (this.OnSelectedHandler == null)
      return;
    this.OnSelectedHandler(index);
  }

  public int SelectedIndex
  {
    set
    {
      this.bar.SelectedIndex = value;
      this.CenterOn(this.bar.buttons[this.SelectedIndex * 2].transform);
    }
    get
    {
      return this.bar.SelectedIndex;
    }
  }

  private void OnScrollViewStopped()
  {
    Vector4 finalClipRegion = this.scrollView.panel.finalClipRegion;
    Bounds bounds = this.scrollView.bounds;
    float num = (double) finalClipRegion.w != 0.0 ? finalClipRegion.w * 0.5f : (float) Screen.height;
    this.downFlag.SetActive((double) bounds.min.y < (double) finalClipRegion.y - (double) num);
    this.upFlag.SetActive((double) bounds.max.y > (double) finalClipRegion.y + (double) num);
  }

  public void CenterOn(Transform target)
  {
    if (!((UnityEngine.Object) this.scrollView != (UnityEngine.Object) null) || !((UnityEngine.Object) this.scrollView.panel != (UnityEngine.Object) null))
      return;
    Vector3[] worldCorners = this.scrollView.panel.worldCorners;
    Vector3 panelBottom = (worldCorners[1] + worldCorners[2]) * 0.5f;
    this.CenterOn(target, panelBottom);
  }

  private void CenterOn(Transform target, Vector3 panelBottom)
  {
    if (!((UnityEngine.Object) target != (UnityEngine.Object) null) || !((UnityEngine.Object) this.scrollView != (UnityEngine.Object) null) || !((UnityEngine.Object) this.scrollView.panel != (UnityEngine.Object) null))
      return;
    Transform cachedTransform = this.scrollView.panel.cachedTransform;
    Vector3 vector3 = cachedTransform.InverseTransformPoint(target.position) - cachedTransform.InverseTransformPoint(panelBottom);
    if (!this.scrollView.canMoveHorizontally)
      vector3.x = 0.0f;
    if (!this.scrollView.canMoveVertically)
      vector3.y = 0.0f;
    vector3.z = 0.0f;
    vector3.y += (float) (this.bar.buttons[0].GetComponent<UIWidget>().height / 2);
    cachedTransform.localPosition -= vector3;
    Vector4 clipOffset = (Vector4) this.scrollView.panel.clipOffset;
    clipOffset.x += vector3.x;
    clipOffset.y += vector3.y;
    this.scrollView.panel.clipOffset = (Vector2) clipOffset;
    this.scrollView.RestrictWithinBounds(true);
  }
}
