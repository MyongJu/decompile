﻿// Decompiled with JetBrains decompiler
// Type: TradingHallSellPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UI;
using UnityEngine;

public class TradingHallSellPopup : Popup
{
  private ProgressBarTweenProxy _priceProxy = new ProgressBarTweenProxy();
  private ProgressBarTweenProxy _amountProxy = new ProgressBarTweenProxy();
  [SerializeField]
  private ItemIconRenderer _itemIconRenderer;
  [SerializeField]
  private UILabel _labelItemName;
  [SerializeField]
  private UILabel _labelEquipmentEnhanced;
  [SerializeField]
  private UILabel _labelItemAveragePrice;
  [SerializeField]
  private UILabel _labelTotalPrice;
  [SerializeField]
  private GameObject _noSellRecordNode;
  [SerializeField]
  private UIButton _buttonSell;
  [SerializeField]
  private UIProgressBar _pickPriceProgress;
  [SerializeField]
  private UIProgressBar _pickAmountProgress;
  [SerializeField]
  private UIInput _pickPriceInput;
  [SerializeField]
  private UIInput _pickAmountInput;
  private TradingHallData.SellGoodData _sellGoodData;
  private System.Action _onSellSuccess;
  private bool _isResell;
  private int _minUnitPrice;
  private int _maxUnitPrice;
  private int _curUnitPrice;
  private int _maxAmount;
  private int _curAmount;
  private int _priceProgressSteps;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    TradingHallSellPopup.Parameter parameter = orgParam as TradingHallSellPopup.Parameter;
    if (parameter != null)
    {
      this._sellGoodData = parameter.sellGoodData;
      this._onSellSuccess = parameter.onSellSuccess;
      this._isResell = parameter.isResell;
    }
    if (this._sellGoodData != null)
    {
      ExchangeHallInfo exchangeHallInfo = ConfigManager.inst.DB_ExchangeHall.Get(this._sellGoodData.ExchangeId);
      if (exchangeHallInfo != null)
      {
        int length = exchangeHallInfo.PriceScaleArray.Length;
        bool flag = false;
        for (int index = 0; index < length; ++index)
        {
          if ((double) exchangeHallInfo.PriceScaleArray[index] <= 0.0 && index > 0)
          {
            this._minUnitPrice = Mathf.FloorToInt(exchangeHallInfo.PriceScaleArray[0] * exchangeHallInfo.itemPrice);
            this._maxUnitPrice = Mathf.FloorToInt(exchangeHallInfo.PriceScaleArray[index - 1] * exchangeHallInfo.itemPrice);
            this._curUnitPrice = Mathf.FloorToInt(exchangeHallInfo.itemPrice);
            this._pickPriceProgress.numberOfSteps = this._priceProgressSteps = index;
            flag = true;
            break;
          }
        }
        if (!flag && (double) exchangeHallInfo.PriceScaleArray[length - 1] > 0.0 && length > 0)
        {
          this._minUnitPrice = Mathf.FloorToInt(exchangeHallInfo.PriceScaleArray[0] * exchangeHallInfo.itemPrice);
          this._maxUnitPrice = Mathf.FloorToInt(exchangeHallInfo.PriceScaleArray[length - 1] * exchangeHallInfo.itemPrice);
          this._curUnitPrice = Mathf.FloorToInt(exchangeHallInfo.itemPrice);
          this._pickPriceProgress.numberOfSteps = this._priceProgressSteps = length - 1;
        }
      }
      this._curAmount = this._maxAmount = this._sellGoodData.Amount;
      this._priceProxy.MaxSize = this._maxUnitPrice - this._minUnitPrice;
      this._amountProxy.MaxSize = this._sellGoodData.Amount;
    }
    this._pickPriceProgress.value = this._maxUnitPrice == this._minUnitPrice ? 0.0f : Mathf.Clamp01((float) (this._curUnitPrice - this._minUnitPrice) / (float) (this._maxUnitPrice - this._minUnitPrice));
    this._pickAmountProgress.value = this._maxAmount == 0 ? 0.0f : Mathf.Clamp01((float) this._curAmount / (float) this._maxAmount);
    this._pickAmountProgress.numberOfSteps = this._maxAmount + 1;
    if (this._isResell)
      this.DisableAmountProgress();
    this.UpdateUI();
    this.UpdateSlider();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnItemInfoClick()
  {
    if (this._sellGoodData == null)
      return;
    Utils.ShowItemTip(this._sellGoodData.ItemId, this._itemIconRenderer.transform, (long) this._sellGoodData.EquipmentId, 0L, this._sellGoodData.EquipmentEnhanced);
  }

  public void OnSellBtnPressed()
  {
    if (this._sellGoodData == null)
      return;
    TradingHallPayload.Instance.SellGood(this._sellGoodData.ExchangeId, this._curUnitPrice, this._curAmount, this._sellGoodData.EquipmentId, this._isResell, this._sellGoodData.Price, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
      {
        if (this._onSellSuccess != null)
          this._onSellSuccess();
        this.OnCloseBtnPressed();
        TradingHallPayload.Instance.ShowBasicToast("toast_exchange_hall_list_success", this._sellGoodData.ExchangeId, this._curAmount);
      }
      else
        TradingHallPayload.Instance.CheckErrorMessage(data as Hashtable);
    }));
  }

  public void OnAmountSliderValueChanged()
  {
    int num = Mathf.RoundToInt((float) this._maxAmount * this._pickAmountProgress.value);
    if (this._curAmount == num)
      return;
    this._curAmount = num;
    this.UpdateSlider();
  }

  public void OnAmountSliderSubtractBtnDown()
  {
    this._amountProxy.Slider = this._pickAmountProgress;
    this._amountProxy.Play(-1, -1f);
  }

  public void OnAmountSliderSubtractBtnUp()
  {
    this._amountProxy.Stop();
    this.UpdateSlider();
  }

  public void OnAmountSliderPlusBtnDown()
  {
    this._amountProxy.Slider = this._pickAmountProgress;
    this._amountProxy.Play(1, -1f);
  }

  public void OnAmountSliderPlusBtnUp()
  {
    this._amountProxy.Stop();
    this.UpdateSlider();
  }

  public void OnPriceSliderValueChanged()
  {
    int nextUnitPrice = Mathf.FloorToInt((float) (this._maxUnitPrice - this._minUnitPrice) * this._pickPriceProgress.value + (float) this._minUnitPrice);
    if (nextUnitPrice == this._curUnitPrice)
      return;
    this.CheckPriceSection(nextUnitPrice <= this._curUnitPrice ? -1 : 1, nextUnitPrice);
  }

  public void OnPriceSliderSubtractBtnDown()
  {
    this._pickPriceProgress.numberOfSteps = 0;
    this._priceProxy.Slider = this._pickPriceProgress;
    this._priceProxy.Play(-1, -1f);
  }

  public void OnPriceSliderSubtractBtnUp()
  {
    this._priceProxy.Stop();
    this.UpdateSlider();
  }

  public void OnPriceSliderPlusBtnDown()
  {
    this._pickPriceProgress.numberOfSteps = 0;
    this._priceProxy.Slider = this._pickPriceProgress;
    this._priceProxy.Play(1, -1f);
  }

  public void OnPriceSliderPlusBtnUp()
  {
    this._priceProxy.Stop();
    this.UpdateSlider();
  }

  private void CheckPriceSection(int direction, int nextUnitPrice)
  {
    if (this._sellGoodData != null)
    {
      ExchangeHallInfo exchangeHallInfo = ConfigManager.inst.DB_ExchangeHall.Get(this._sellGoodData.ExchangeId);
      if (exchangeHallInfo != null)
      {
        for (int index = 0; index < exchangeHallInfo.PriceScaleArray.Length; ++index)
        {
          int num = Mathf.FloorToInt(exchangeHallInfo.PriceScaleArray[index] * exchangeHallInfo.itemPrice);
          if (direction > 0)
          {
            if (nextUnitPrice <= num)
            {
              nextUnitPrice = num;
              break;
            }
          }
          else if (nextUnitPrice < num && index > 0)
          {
            nextUnitPrice = Mathf.FloorToInt(exchangeHallInfo.PriceScaleArray[index - 1] * exchangeHallInfo.itemPrice);
            break;
          }
        }
      }
    }
    if (nextUnitPrice != this._curUnitPrice)
    {
      this._curUnitPrice = Mathf.Clamp(nextUnitPrice, this._minUnitPrice, this._maxUnitPrice);
      this._pickPriceProgress.value = Mathf.Clamp01((float) (this._curUnitPrice - this._minUnitPrice) / (float) (this._maxUnitPrice - this._minUnitPrice));
      this._pickPriceProgress.numberOfSteps = this._priceProgressSteps;
      this._priceProxy.Slider = this._pickPriceProgress;
      this._priceProxy.Pause();
    }
    this.UpdateSlider();
  }

  private void UpdateSlider()
  {
    this._pickPriceInput.label.text = this._curUnitPrice.ToString();
    this._pickAmountInput.label.text = this._curAmount.ToString();
    this._labelTotalPrice.text = (this._curAmount * this._curUnitPrice).ToString();
    this._buttonSell.isEnabled = this._curAmount > 0;
  }

  private void UpdateUI()
  {
    if (this._sellGoodData == null)
      return;
    this._itemIconRenderer.SetData(this._sellGoodData.ItemId, string.Empty, true);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._sellGoodData.ItemId);
    if (itemStaticInfo != null)
    {
      this._labelItemName.text = itemStaticInfo.LocName;
      this._labelEquipmentEnhanced.text = this._sellGoodData.EquipmentEnhanced <= 0 ? string.Empty : "+" + this._sellGoodData.EquipmentEnhanced.ToString();
      this._labelItemName.text += this._labelEquipmentEnhanced.text;
      EquipmentManager.Instance.ConfigQualityLabelWithColor(this._labelItemName, itemStaticInfo.Quality);
    }
    TradingHallData.BuyGoodData buyGoodData = TradingHallPayload.Instance.TradingData.BuyGoodDataList.Find((Predicate<TradingHallData.BuyGoodData>) (x => x.ExchangeId == this._sellGoodData.ExchangeId));
    if (buyGoodData != null)
      this._labelItemAveragePrice.text = Utils.FormatThousands(buyGoodData.ItemAveragePrice.ToString());
    NGUITools.SetActive(this._labelItemAveragePrice.transform.parent.gameObject, buyGoodData != null && buyGoodData.ItemAveragePrice > 0);
    NGUITools.SetActive(this._noSellRecordNode, buyGoodData == null || buyGoodData.ItemAveragePrice <= 0);
  }

  private void DisableAmountProgress()
  {
    this._pickAmountProgress.enabled = false;
    StandardProgressBar componentInParent = this._pickAmountProgress.GetComponentInParent<StandardProgressBar>();
    if ((UnityEngine.Object) componentInParent != (UnityEngine.Object) null)
    {
      if ((UnityEngine.Object) componentInParent.plusBtn != (UnityEngine.Object) null)
        componentInParent.plusBtn.isEnabled = false;
      if ((UnityEngine.Object) componentInParent.minusBtn != (UnityEngine.Object) null)
        componentInParent.minusBtn.isEnabled = false;
    }
    GreyUtility.Custom(this._pickAmountProgress.foregroundWidget.gameObject, new Color(0.09803922f, 0.1843137f, 0.227451f, 1f));
  }

  public class Parameter : Popup.PopupParameter
  {
    public TradingHallData.SellGoodData sellGoodData;
    public System.Action onSellSuccess;
    public bool isResell;
  }
}
