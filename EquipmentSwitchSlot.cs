﻿// Decompiled with JetBrains decompiler
// Type: EquipmentSwitchSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class EquipmentSwitchSlot : MonoBehaviour
{
  public UILabel sortIndexText;
  public GameObject highlightNode;
  private EquipmentType _equipmentType;
  private int _curSortIndex;
  private int _curPickedIndex;

  public void SetData(EquipmentType equipmentType, int curSortIndex, int curPickedIndex)
  {
    this._equipmentType = equipmentType;
    this._curSortIndex = curSortIndex;
    this._curPickedIndex = curPickedIndex;
    this.UpdateUI();
  }

  public void RefreshUI(int curPickedIndex)
  {
    this._curPickedIndex = curPickedIndex;
    this.UpdateUI();
  }

  public void OnItemSlotClicked()
  {
    if (this._curPickedIndex == this._curSortIndex)
      return;
    if (this.IsCurrentSlotAvailabe())
    {
      if (this._equipmentType != EquipmentType.PLAYER)
        return;
      List<InventoryItemData> allEquipedBefore = Utils.GetAllEquipedItemData(BagType.Hero);
      RequestManager.inst.SendRequest("Hero:changeOutfit", new Hashtable()
      {
        {
          (object) "uid",
          (object) PlayerData.inst.uid
        },
        {
          (object) "outfit_index",
          (object) this._curSortIndex
        }
      }, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.RefreshUI(this._curSortIndex);
        UIManager.inst.toast.Show(Utils.XLAT("toast_equipment_quick_equip_success"), (System.Action) null, 4f, true);
        List<Benefits.BenefitValuePair> allDelProperty;
        List<Benefits.BenefitValuePair> allAddProperty;
        ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance;
        ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance;
        Utils.GetEquipmentBenefitChangeInfoAfterReequipAll(BagType.Hero, allEquipedBefore, Utils.GetAllEquipedItemData(BagType.Hero), out allDelProperty, out allAddProperty, out addEquipmentSuitEnhance, out delEquipmentSuitEnhance);
        if (allDelProperty.Count <= 0 && allAddProperty.Count <= 0 && (addEquipmentSuitEnhance == null && delEquipmentSuitEnhance == null))
          return;
        UIManager.inst.ShowEquipmentSuitBenefitChangeTip(allAddProperty, allDelProperty, addEquipmentSuitEnhance, delEquipmentSuitEnhance);
      }), true);
    }
    else
    {
      if (this._equipmentType != EquipmentType.PLAYER)
        return;
      Dictionary<string, string> para = new Dictionary<string, string>();
      para.Add("0", this.GetShouldActiveVipLevel().ToString());
      MessageBoxWith1Button.Parameter parameter = new MessageBoxWith1Button.Parameter();
      parameter.Title = Utils.XLAT("equipment_quick_equip_unlock_title");
      parameter.Content = ScriptLocalization.GetWithPara("equipment_quick_equip_unlock_description", para, true);
      parameter.Okay = Utils.XLAT("vip_activate_button");
      parameter.OkayCallback += new System.Action(this.Skip2VipView);
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) parameter);
    }
  }

  private int GetShouldActiveVipLevel()
  {
    return ConfigManager.inst.DB_VIP_Level.GetVipLevelByBenefitNeed("prop_equipment_save_base_value", "prop_equipment_save_value", (float) (this._curSortIndex - 1));
  }

  private void Skip2VipView()
  {
    UIManager.inst.OpenDlg("VIP/VipBaseDlg", (UI.Dialog.DialogParameter) new VIPBacePopup.Parameter()
    {
      nextVipLevel = this.GetShouldActiveVipLevel()
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private int GetEquipCanSwitchCount()
  {
    int num = 1;
    if (this._equipmentType == EquipmentType.PLAYER)
    {
      float total = DBManager.inst.DB_Local_Benefit.Get("prop_equipment_save_base_value").total;
      num += (int) ConfigManager.inst.DB_BenefitCalc.GetFinalData(total, "calc_equipment_save");
    }
    if (num > 1)
      return num;
    return 1;
  }

  private bool IsCurrentSlotAvailabe()
  {
    if (this._curSortIndex > this.GetEquipCanSwitchCount())
      return this._curPickedIndex == this._curSortIndex;
    return true;
  }

  private void UpdateUI()
  {
    this.sortIndexText.text = this._curSortIndex.ToString();
    if (!this.IsCurrentSlotAvailabe())
      GreyUtility.Grey(this.gameObject);
    else
      GreyUtility.Normal(this.gameObject);
    NGUITools.SetActive(this.highlightNode, this._curSortIndex == this._curPickedIndex);
  }
}
