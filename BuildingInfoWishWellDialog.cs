﻿// Decompiled with JetBrains decompiler
// Type: BuildingInfoWishWellDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using UI;

public class BuildingInfoWishWellDialog : UI.Dialog
{
  public UILabel m_Level;
  public UIWidget m_Pivot;
  public UILabel m_Food;
  public UILabel m_Wood;
  public UILabel m_Ore;
  public UILabel m_Silver;
  public UILabel m_Gem;
  private CityManager.BuildingItem m_BuildingItem;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_BuildingItem = CityManager.inst.GetBuildingFromType("wish_well");
    CitadelSystem.inst.FocusConstructionBuilding(BuildingController.mSelectedBuildingController.gameObject, this.m_Pivot, 0.0f);
    MessageHub.inst.GetPortByAction("job_complete").AddEvent(new System.Action<object>(this.OnJobCompleted));
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    MessageHub.inst.GetPortByAction("job_complete").RemoveEvent(new System.Action<object>(this.OnJobCompleted));
  }

  public void OnMoreInfoButtonPressed()
  {
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData(this.m_BuildingItem.mType + "_" + this.m_BuildingItem.mLevel.ToString());
    UIManager.inst.OpenPopup("BuildingInfo/BuildingInfoMoreInfoBasePopup", (Popup.PopupParameter) new BuildingInfoMoreInfoBasePopup.Parameter()
    {
      info = data
    });
  }

  private void OnJobCompleted(object result)
  {
    Hashtable hashtable = result as Hashtable;
    int result1 = 0;
    if (hashtable == null || !hashtable.ContainsKey((object) "event_type") || !int.TryParse(hashtable[(object) "event_type"].ToString(), out result1) || (result1 != 30 && result1 != 31 || long.Parse(hashtable[(object) "building_id"].ToString()) != this.m_BuildingItem.mID))
      return;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.m_Level.text = string.Format("{0}/{1}", (object) this.m_BuildingItem.mLevel, (object) 30);
    WishWellData wishWellDataByLevel = ConfigManager.inst.DB_WishWell.GetWishWellDataByLevel(this.m_BuildingItem.mLevel);
    this.m_Food.text = string.Format("{0}: {1}", (object) ScriptLocalization.Get("wish_well_food", true), (object) wishWellDataByLevel.Food);
    this.m_Wood.text = string.Format("{0}: {1}", (object) ScriptLocalization.Get("wish_well_wood", true), (object) wishWellDataByLevel.Wood);
    this.m_Ore.text = string.Format("{0}: {1}", (object) ScriptLocalization.Get("wish_well_ore", true), (object) wishWellDataByLevel.Ore);
    this.m_Silver.text = string.Format("{0}: {1}", (object) ScriptLocalization.Get("wish_well_silver", true), (object) wishWellDataByLevel.Silver);
    this.m_Gem.text = string.Format("{0}: {1}", (object) ScriptLocalization.Get("wish_well_free_wishes", true), (object) wishWellDataByLevel.FreeGem);
  }
}
