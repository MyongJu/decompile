﻿// Decompiled with JetBrains decompiler
// Type: ParliamentHeroStarInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ParliamentHeroStarInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "next_need_level")]
  public int nextNeedLevel;
  [Config(Name = "next_star_chip_req")]
  public int nextStarChipReq;
}
