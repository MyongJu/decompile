﻿// Decompiled with JetBrains decompiler
// Type: MailController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class MailController
{
  public Dictionary<long, AbstractMailEntry> mailsById = new Dictionary<long, AbstractMailEntry>();
  private Dictionary<MailCategory, MailList> _mailListsByType = new Dictionary<MailCategory, MailList>();
  private List<PlayerInfo> _interactedContacts = new List<PlayerInfo>();
  public const int mailRequestSize = 200;
  public const int MAX_MAIL_STORY_COUNT = 200;
  public const int DELETE_MESSAGE_SHOW_COUNT = 10;
  public const int MAX_MAIL_HISTORY_COUNT = 1;
  public const string interactedUsersStorageKey = "interactedUsers";
  public MailCategory currentTypeFilter;
  public MailMainDlg mailPanel;
  private PublicHUD _publicHUD;
  private bool initialized;
  private HashSet<PlayerInfo> _contactList;
  private int _mailCount;

  public MailController(MailAPI api)
  {
    this.API = api;
    api.parent = this;
  }

  public MailAPI API { get; private set; }

  public MailList CurrentList
  {
    get
    {
      return this._mailListsByType[this.currentTypeFilter];
    }
  }

  public IMailStorage MailStorage { get; private set; }

  public MailListRender CurrentView { get; private set; }

  public PublicHUD publicHUD
  {
    get
    {
      return this._publicHUD;
    }
    set
    {
      this._publicHUD = value;
      this.MailCount = this._mailCount;
    }
  }

  public int MailCount
  {
    get
    {
      return this._mailCount;
    }
    set
    {
      this._mailCount = value;
      if (!((UnityEngine.Object) this.publicHUD != (UnityEngine.Object) null))
        return;
      this.publicHUD.SetMailBadgeCount(this._mailCount);
    }
  }

  public void Init()
  {
    this.initialized = false;
    GameEngine.Instance.OnGameModeStarted += new System.Action<bool>(this.OnGameModeStarted);
  }

  public void Dispose()
  {
    GameEngine.Instance.OnGameModeStarted -= new System.Action<bool>(this.OnGameModeStarted);
    this.initialized = false;
  }

  private void OnGameModeStarted(bool clear)
  {
    if (this.initialized)
      return;
    this.MailStorage = (IMailStorage) new ClientMailStorage();
    this.MailStorage.Initialize();
    this.API.Initialize();
    this.API.GetUnreadCount(new System.Action<Hashtable>(this.OnGetUnreadCount));
    GameEngine.Instance.events.ServerPush_OnReceiveMail += new System.Action<Hashtable, bool>(this.OnReceiveMail);
    GameEngine.Instance.events.ServerPush_OnReceiveFavMail += new System.Action<Hashtable, bool>(this.OnReceiveFavMail);
    GameEngine.Instance.events.ServerPush_OnUpdateMail += new System.Action<Hashtable>(this.GameEngine_Instance_events_ServerPush_OnUpdateMail);
    foreach (string name in Enum.GetNames(typeof (MailCategory)))
    {
      MailCategory mailCategory = (MailCategory) Enum.Parse(typeof (MailCategory), name);
      this._mailListsByType.Add(mailCategory, new MailList(mailCategory, this, this.API));
    }
    Dictionary<MailCategory, MailList>.Enumerator enumerator1 = this._mailListsByType.GetEnumerator();
    while (enumerator1.MoveNext())
      enumerator1.Current.Value.mailAdded += new System.Action<AbstractMailEntry, bool>(this.OnMailAdded);
    HashSet<string>.Enumerator enumerator2 = this.MailStorage.InteractedUsernames().GetEnumerator();
    while (enumerator2.MoveNext())
      this._interactedContacts.Add(new PlayerInfo()
      {
        userName = enumerator2.Current
      });
    this.LoadMoreMail((System.Action) null);
    this.initialized = true;
  }

  private void GameEngine_Instance_events_ServerPush_OnUpdateMail(Hashtable obj)
  {
    long outData = 0;
    DatabaseTools.UpdateData(obj, "mail_id", ref outData);
    if (this.mailsById.ContainsKey(outData))
    {
      AbstractMailEntry abstractMailEntry = this.mailsById[outData];
      bool isRead = abstractMailEntry.isRead;
      abstractMailEntry.ApplyData(obj);
      if (!abstractMailEntry.isRead && isRead)
        ++this._mailListsByType[abstractMailEntry.category].UnReadCount;
      this._mailListsByType[abstractMailEntry.category].Sort();
      this.UpdateUnReadCount();
    }
    else
      this.AddMail(obj, false);
  }

  public void AddMail(AbstractMailEntry entry, bool fromLoad)
  {
    if (this.mailsById.ContainsKey(entry.mailID))
    {
      this.mailsById[entry.mailID].ApplyData(entry.data);
    }
    else
    {
      this.mailsById[entry.mailID] = entry;
      entry.controller = this;
      entry.mailAPI = this.API;
    }
    this._mailListsByType[entry.category].AddMailID(entry.mailID, fromLoad);
    this.UpdateUnReadCount();
  }

  public void AddMail(Hashtable ht, bool fromLoad)
  {
    AbstractMailEntry mailForType = this.GetMailForType(int.Parse(ht[(object) "type"].ToString()));
    mailForType.ApplyData(ht);
    this.AddMail(mailForType, fromLoad);
    if (fromLoad)
      return;
    this.ShowToast(mailForType);
  }

  private AbstractMailEntry GetMailForType(int type)
  {
    MailType mailType = (MailType) type;
    switch (mailType)
    {
      case MailType.MAIL_TYPE_SYSTEM_RECOMMEND_ALLIANCE:
      case MailType.MAIL_TYPE_SYSTEM_USER_SENT_ATTACHMENT:
      case MailType.MAIL_TYPE_SYSTEM_DUNGEON_RANKING_REWARD:
      case MailType.MAIL_TYPE_SYSTEM_DK_ARENA_RANKING_REWARD:
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_END:
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_WORSHIPPED_REWARD:
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_KINGS_REWARD:
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_START:
      case MailType.MAIL_TYPE_SYSTEM_ABYSS_RESULT_NOTICE:
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_KINGS_START:
      case MailType.MAIL_TYPE_SYSTEM_ANNIVERSARY_COUNTDOWN_REWARD:
      case MailType.MAIL_TYPE_WORLD_BOSS_START:
      case MailType.MAIL_TYPE_WORLD_BOSS_END:
      case MailType.MAIL_TYPE_WORLD_BOSS_KILLED:
      case MailType.MAIL_TYPE_WORLD_BOSS_KILL_REWARD:
      case MailType.MAIL_TYPE_SYSTEM_WONDER_DISMISS:
      case MailType.MAIL_TYPE_ALLIANCE_AC_REWARD:
label_5:
        return (AbstractMailEntry) new SystemMessage();
      default:
        switch (mailType - 49)
        {
          case MailType.MAIL_TYPE_NORMAL:
          case MailType.MAIL_TYPE_PVP_BATTLE_REPORT:
          case MailType.MAIL_TYPE_PVP_SCOUT_REPORT:
            goto label_5;
          default:
            if (mailType == MailType.MAIL_TYPE_NORMAL)
              return (AbstractMailEntry) new PlayerMessage();
            if (mailType != MailType.MAIL_TYPE_SYSTEM_NOTICE && mailType != MailType.MAIL_TYPE_SYSTEM_INTEGRAL_REWARD && (mailType != MailType.MAIL_TYPE_SYSTEM_VERSION && mailType != MailType.MAIL_TYPE_SYSTEM_NOTICE_MULTI_LANG))
              return (AbstractMailEntry) new WarMessage();
            goto label_5;
        }
    }
  }

  public HashSet<PlayerInfo> ContactList()
  {
    if (this._contactList != null)
      return this._contactList;
    this._contactList = new HashSet<PlayerInfo>();
    this._contactList.UnionWith((IEnumerable<PlayerInfo>) this._interactedContacts);
    Dictionary<long, PlayerInfo>.ValueCollection.Enumerator enumerator = PlayerData.inst.contacts.friends.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (!this.InContactList(enumerator.Current.userName))
        this._contactList.Add(enumerator.Current);
    }
    this._contactList.RemoveWhere(new Predicate<PlayerInfo>(this.IsContained));
    return this._contactList;
  }

  public List<string> GetMatchingContactsForSubstring(string current)
  {
    HashSet<PlayerInfo> playerInfoSet = this.ContactList();
    List<string> stringList = new List<string>();
    if (playerInfoSet == null)
      return stringList;
    HashSet<PlayerInfo>.Enumerator enumerator = playerInfoSet.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (!string.IsNullOrEmpty(enumerator.Current.userName) && enumerator.Current.userName.ToUpper().Contains(current.ToUpper()) && !stringList.Contains(enumerator.Current.userName))
        stringList.Add(enumerator.Current.userName);
    }
    return stringList;
  }

  public int GetMailIndex(AbstractMailEntry entry)
  {
    return this._mailListsByType[this.currentTypeFilter].GetMailIndex(entry.mailID);
  }

  public MailList GetMailListOfType(MailCategory type)
  {
    return this._mailListsByType[type];
  }

  public int GetCurrentListCount()
  {
    return this._mailListsByType[this.currentTypeFilter].TotalCount;
  }

  public void GetMailOfIndex(int index, System.Action<AbstractMailEntry> callback)
  {
    this._mailListsByType[this.currentTypeFilter].GetMailByIndex(index, callback);
  }

  public void MarkMailAsRead(int category, List<AbstractMailEntry> entries, bool status, bool syncToServer = true)
  {
    List<long> mailIds = new List<long>(entries.Count);
    for (int index = 0; index < entries.Count; ++index)
    {
      AbstractMailEntry entry = entries[index];
      if (this.mailsById.ContainsKey(entry.mailID) && status != entry.isRead)
      {
        if (status && !entry.isRead)
          --this._mailListsByType[entry.category].UnReadCount;
        else
          ++this._mailListsByType[entry.category].UnReadCount;
        entry.isRead = status;
        this.mailsById[entry.mailID].isRead = status;
        mailIds.Add(entry.mailID);
        this.CurrentList.UpdateMailEntry(entry);
      }
    }
    if (syncToServer)
      this.API.TagRead(category, mailIds, (System.Action) null);
    this.UpdateUnReadCount();
  }

  public void BatchGainAttachments(int category, List<AbstractMailEntry> entries)
  {
    ArrayList mailIDs = new ArrayList(entries.Count);
    for (int index = 0; index < entries.Count; ++index)
    {
      AbstractMailEntry entry = entries[index];
      bool flag = true;
      SystemMessage systemMessage = entry as SystemMessage;
      if (systemMessage != null)
        flag = entry.type != MailType.MAIL_TYPE_SYSTEM_VERSION ? string.IsNullOrEmpty(systemMessage.link) || systemMessage.linkClicked == 1 : systemMessage.GetCurrentVersion() >= systemMessage.targetVersion;
      if (!entry.isRead && flag)
      {
        --this._mailListsByType[entry.category].UnReadCount;
        entry.isRead = true;
        this.mailsById[entry.mailID].isRead = true;
      }
      if (this.mailsById.ContainsKey(entry.mailID) && flag)
      {
        mailIDs.Add((object) entry.mailID);
        if (entry.CanReceiveAttachment())
          entry.attachment_status = 2;
      }
      this.CurrentList.UpdateMailEntry(entry);
    }
    this.API.BatchGainAttachments(category, mailIDs, (System.Action<bool, object>) null);
    this.UpdateUnReadCount();
  }

  public void MarkSubjectAsRead(int category, List<string> subjecs, bool status)
  {
    for (int index = 0; index < subjecs.Count; ++index)
    {
      List<AbstractMailEntry> mailListBySubject = this.GetMailListBySubject(subjecs[index]);
      this.MarkMailAsRead(category, mailListBySubject, true, true);
    }
  }

  public void OpenMail(AbstractMailEntry entry)
  {
    entry.Display();
  }

  public void SetView(MailListRender view)
  {
    if ((UnityEngine.Object) this.CurrentView != (UnityEngine.Object) null)
      this.CurrentView.Hide();
    this.CurrentView = view;
    this.CurrentView.Show();
  }

  public void DeleteMailList(int category, List<AbstractMailEntry> deleteList, System.Action<bool> callBack)
  {
    bool notDeleteFully = false;
    List<long> mailIds = new List<long>();
    Dictionary<MailCategory, MailList>.Enumerator enumerator = this._mailListsByType.GetEnumerator();
    while (enumerator.MoveNext())
    {
      for (int index = 0; index < deleteList.Count; ++index)
      {
        AbstractMailEntry delete = deleteList[index];
        if (this.CanDelete(delete))
        {
          mailIds.Add(deleteList[index].mailID);
          enumerator.Current.Value.RemoveMail(delete.mailID, false);
        }
        else
          notDeleteFully = true;
      }
      if (enumerator.Current.Value.mailRemoved != null)
        enumerator.Current.Value.mailRemoved((AbstractMailEntry) null);
    }
    this.API.DeleteMail(category, mailIds, (System.Action) (() => callBack(notDeleteFully)));
    this.UpdateUnReadCount();
  }

  public void DeleteMailsByCategoryLocal(MailCategory category)
  {
    MailList mailList = this._mailListsByType[category];
    List<AbstractMailEntry> mail = mailList.GetMail();
    List<long> longList = new List<long>(15);
    for (int index = 0; index < mail.Count; ++index)
      longList.Add(mail[index].mailID);
    for (int index = 0; index < longList.Count; ++index)
      mailList.RemoveMail(longList[index], true);
    this.UpdateUnReadCount();
  }

  private bool CanDelete(AbstractMailEntry entry)
  {
    bool flag = !entry.isFavorite;
    if (entry.attachment_status == 1 && entry.AttachmentExpirationLeftTime > 0)
      flag = false;
    return flag;
  }

  public void LoadMailList(MailCategory mailCategory, long startId, System.Action initCallBack, bool blockScene = true)
  {
    this.API.GetList(startId, (int) mailCategory, initCallBack, blockScene);
  }

  public void LoadFavMailList(long startId, System.Action initCallBack, bool blockScene = true)
  {
    this.API.GetFavList(startId, initCallBack, blockScene);
  }

  public void SetCurrentList(MailCategory mailType)
  {
    this.currentTypeFilter = mailType;
    if (!this._mailListsByType.ContainsKey(mailType))
      return;
    this.mailPanel.SetMailList(this._mailListsByType[mailType]);
  }

  public void ComposeMail(List<string> recipients = null, string subject = null, string body = null, bool isAllianceMail = false)
  {
    UIManager.inst.OpenDlg("Mail/MailComposeDlg", (UI.Dialog.DialogParameter) new MailComposeDlg.Parameter()
    {
      recipents = recipients,
      isAllianceMail = isAllianceMail
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void SendAllianceMail(long alliance_id, List<string> recipients = null)
  {
    UIManager.inst.OpenDlg("Mail/MailComposeDlg", (UI.Dialog.DialogParameter) new MailComposeDlg.Parameter()
    {
      recipents = recipients,
      isAllianceMail = true,
      allianceID = alliance_id
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void SendModMail(long uid, List<string> recipients = null)
  {
    UIManager.inst.OpenDlg("Mail/MailComposeDlg", (UI.Dialog.DialogParameter) new MailComposeDlg.Parameter()
    {
      recipents = recipients,
      isModMail = true,
      allianceID = uid
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void BlockSender(long uid)
  {
    UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
    {
      setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
      description = "By blocking this user, you block the user from sending Comments, Chat Messages, Emails, and seeing their Headline",
      leftButtonText = "Yes",
      leftButtonClickEvent = (System.Action) (() => PlayerData.inst.contacts.BlockUser(new PlayerInfo()
      {
        uid = uid
      }, 1 != 0)),
      rightButtonText = "No",
      rightButtonClickEvent = (System.Action) null
    });
  }

  public void SendMail(ArrayList recipents, string content, System.Action callBack)
  {
    if (recipents == null || recipents.Count == 0)
      Debug.LogWarning((object) "Attempted to send incorrect message type");
    else
      this.API.SendMail(recipents, content, callBack);
  }

  public void SendMail(ArrayList recipents, string content, int itemId, int itemCount, System.Action callBack)
  {
    if (recipents == null || recipents.Count == 0)
      Debug.LogWarning((object) "Attempted to send incorrect message type");
    else
      this.API.SendMail(recipents, content, itemId, itemCount, callBack);
  }

  public void ValidateRecipient(ArrayList recipients, System.Action<bool, object> callBack)
  {
    this.API.ValidateRecipient(recipients, callBack);
  }

  public void SendAllianceMail(long alliance_id, string content, System.Action callBack)
  {
    this.API.SendAllianceMail(alliance_id, content, callBack);
  }

  public void SendModMail(long modId, string content, System.Action callBack)
  {
    this.API.SendModMail(modId, content, callBack);
  }

  public void SendModMails(ArrayList modIds, string content, System.Action callback)
  {
    this.API.SendModMails(modIds, content, callback);
  }

  public void MailFavChanged(AbstractMailEntry entry, bool favorite)
  {
    MailList mailList = this._mailListsByType[MailCategory.Favorite];
    if (favorite)
      mailList.AddMailID(entry.mailID, false);
    else
      mailList.RemoveMail(entry.mailID, true);
    this.mailsById[entry.mailID].isFavorite = favorite;
  }

  public void MailGainAttachChanged(AbstractMailEntry entry)
  {
    this.CurrentList.UpdateMailEntry(entry);
  }

  public void MarkFavToMails(List<AbstractMailEntry> mails, bool fav, int category, System.Action callback = null)
  {
    Dictionary<long, bool> dictionary = new Dictionary<long, bool>();
    for (int index = 0; index < mails.Count; ++index)
    {
      mails[index].isFavorite = fav;
      dictionary.Add(mails[index].mailID, fav);
      if (!fav)
        this._mailListsByType[MailCategory.Favorite].RemoveMail(mails[index].mailID, true);
      this.mailsById[mails[index].mailID].isFavorite = fav;
    }
    this.API.BatchUnTagFavorite(mails, callback);
  }

  public void TagFavToMail(long mailID, bool fav, int category)
  {
    Dictionary<long, bool> mails = new Dictionary<long, bool>();
    mails.Add(mailID, fav);
    this.mailsById[mailID].isFavorite = fav;
    if (!fav)
      this._mailListsByType[MailCategory.Favorite].RemoveMail(mailID, true);
    this.MailFavChanged(this.mailsById[mailID], fav);
    this.API.TagFavorite(mails, category, (System.Action) null);
  }

  public void AddFriend(string userName)
  {
    if (PlayerData.inst.hostPlayer.userName == userName || PlayerData.inst.contacts.IsFriend(userName))
      return;
    this.API.GetUserInfoByUsername(userName, (System.Action<long>) (userId => UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
    {
      setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
      description = string.Format("Add {0} as friend?", (object) userName),
      leftButtonText = "Yes",
      leftButtonClickEvent = (System.Action) (() => PlayerData.inst.contacts.AddContact(new PlayerInfo()
      {
        uid = userId
      })),
      rightButtonText = "No",
      rightButtonClickEvent = (System.Action) null
    })), (System.Action) (() => UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
    {
      setType = (SingleButtonPopup.SetType.description | SingleButtonPopup.SetType.buttonText),
      description = "Invalid username",
      buttonText = "Okay"
    })));
  }

  public List<Hashtable> ReportsByPosition(int k, int x, int y, MailType mailType)
  {
    List<Hashtable> hashtableList = new List<Hashtable>();
    MailList mailList = this._mailListsByType[MailCategory.Report];
    List<AbstractMailEntry> mail = mailList.GetMail();
    for (int index = 0; index < mail.Count; ++index)
    {
      WarMessage mailById = mailList.GetMailByID(mail[index].mailID) as WarMessage;
      if (mailById != null && mailById.type == mailType && MapUtils.GetCoordinateString(k, x, y) == mailById.Cordinate())
        hashtableList.Add(mailById.data);
    }
    return hashtableList;
  }

  public List<AbstractMailEntry> GetMailListBySubject(string subject)
  {
    List<AbstractMailEntry> abstractMailEntryList = new List<AbstractMailEntry>();
    using (Dictionary<long, AbstractMailEntry>.Enumerator enumerator = this.mailsById.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<long, AbstractMailEntry> current = enumerator.Current;
        if (current.Value.subject == subject)
          abstractMailEntryList.Add(current.Value);
      }
    }
    return abstractMailEntryList;
  }

  public bool IsSubjectHasFav(string subject)
  {
    List<AbstractMailEntry> mailListBySubject = this.GetMailListBySubject(subject);
    for (int index = 0; index < mailListBySubject.Count; ++index)
    {
      if (mailListBySubject[index].isFavorite)
        return true;
    }
    return false;
  }

  public bool IsSubjectRead(string subject)
  {
    List<AbstractMailEntry> mailListBySubject = this.GetMailListBySubject(subject);
    for (int index = 0; index < mailListBySubject.Count; ++index)
    {
      if (!mailListBySubject[index].isRead)
        return false;
    }
    return true;
  }

  public void LoadMoreMail(System.Action callBack = null)
  {
    if (this.mailsById.Count < 200)
      ;
  }

  public void GetSharedMail(long sharedUser, long mailid, int type)
  {
    this.API.GetSharedMail(sharedUser, mailid, (System.Action<Hashtable>) (ht =>
    {
      Hashtable ht1 = ht[(object) "mail"] as Hashtable;
      if (ht1.Contains((object) "deleted"))
      {
        UIManager.inst.toast.Show(ScriptLocalization.Get("toast_battle_report_share_deleted", true), (System.Action) null, 4f, true);
      }
      else
      {
        AbstractMailEntry abstractMailEntry = new AbstractMailEntry();
        abstractMailEntry.ApplyData(ht1);
        abstractMailEntry.isShared = true;
        Hashtable hashtable = new Hashtable();
        hashtable.Add((object) "data", ht1[(object) "data"]);
        long result = 0;
        if (ht1.ContainsKey((object) "uid"))
          long.TryParse(ht1[(object) "uid"] as string, out result);
        switch (type)
        {
          case 0:
            UIManager.inst.OpenPopup("Report/WarReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
            {
              hashtable = hashtable,
              mail = abstractMailEntry
            });
            break;
          case 1:
            UIManager inst = UIManager.inst;
            string popupType = "ScoutReportPopup";
            ScoutReportPopup.Parameter parameter = new ScoutReportPopup.Parameter()
            {
              hashtable = ht1[(object) "data"] as Hashtable,
              mail = abstractMailEntry,
              fromType = FromType.Share,
              isSlef = PlayerData.inst.uid == result
            };
            inst.OpenPopup(popupType, (Popup.PopupParameter) parameter);
            break;
          case 2:
            UIManager.inst.OpenPopup("Report/DragonAltarReportPopup", (Popup.PopupParameter) new BaseReportPopup.Parameter()
            {
              hashtable = (hashtable[(object) "data"] as Hashtable),
              mail = abstractMailEntry
            });
            break;
        }
      }
    }));
  }

  public bool NeedShowDeletMessage()
  {
    return this.mailsById.Count > 190;
  }

  public int FullCount()
  {
    return 200;
  }

  public void ShowFullWarning()
  {
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = "Failure",
      Content = Utils.XLAT("mail_load_upper_limit"),
      Okay = "OKAY",
      OkayCallback = (System.Action) null
    });
  }

  private void OnReceiveMail(Hashtable mailHT, bool fromLoad)
  {
    this.AddMail(mailHT, fromLoad);
  }

  private void OnReceiveFavMail(Hashtable mailHT, bool fromLoad)
  {
    AbstractMailEntry mailForType = this.GetMailForType(int.Parse(mailHT[(object) "type"].ToString()));
    mailForType.ApplyData(mailHT);
    if (this.mailsById.ContainsKey(mailForType.mailID))
    {
      this.mailsById[mailForType.mailID].ApplyData(mailForType.data);
    }
    else
    {
      this.mailsById[mailForType.mailID] = mailForType;
      mailForType.controller = this;
      mailForType.mailAPI = this.API;
    }
    this._mailListsByType[MailCategory.Favorite].AddMailEntry(mailForType, fromLoad);
  }

  private void OnReceiveMailChat(Hashtable mailChat)
  {
    PlayerMessage playerMessage = (PlayerMessage) this.mailsById[long.Parse(mailChat[(object) "mail_id"].ToString())];
    if (playerMessage == null)
      return;
    PlayerMessageEntry entry = new PlayerMessageEntry();
    ArrayList arrayList = mailChat[(object) "text"] as ArrayList;
    entry.time = long.Parse(arrayList[0].ToString());
    entry.content = arrayList[1].ToString();
    entry.fromUID = playerMessage.fromUID;
    entry.fromPortrait = playerMessage.from_portrait;
    entry.fromIcon = playerMessage.from_icon;
    entry.fromLordTitleId = playerMessage.from_lord_title;
    playerMessage.AddMessageEntries(entry);
  }

  private bool IsContained(PlayerInfo entry)
  {
    Dictionary<long, PlayerInfo>.ValueCollection.Enumerator enumerator = PlayerData.inst.contacts.blockeds.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.uid == entry.uid)
        return true;
    }
    return false;
  }

  private bool InContactList(string userName)
  {
    HashSet<PlayerInfo>.Enumerator enumerator = this._contactList.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.userName == userName)
        return true;
    }
    return false;
  }

  private void AddInteractedUserEntry(string entry)
  {
    for (int index = 0; index < this._interactedContacts.Count; ++index)
    {
      if (this._interactedContacts[index].userName == entry)
        return;
    }
    this._interactedContacts.Add(new PlayerInfo()
    {
      userName = entry
    });
    this._contactList = (HashSet<PlayerInfo>) null;
    string str = this._interactedContacts[0].userName;
    for (int index = 1; index < this._interactedContacts.Count; ++index)
      str = str + "," + this._interactedContacts[index].userName;
    this.MailStorage.AddInteractedUsername(entry);
  }

  [DebuggerHidden]
  private IEnumerator SetViewAfterFrame(MailListRender view)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MailController.\u003CSetViewAfterFrame\u003Ec__IteratorBF()
    {
      view = view,
      \u003C\u0024\u003Eview = view,
      \u003C\u003Ef__this = this
    };
  }

  private void OnMailAdded(AbstractMailEntry mail, bool fromLoad)
  {
    if (string.IsNullOrEmpty(mail.sender))
      return;
    this.AddInteractedUserEntry(mail.sender);
  }

  private void ShowToast(AbstractMailEntry mail)
  {
    switch (mail.type)
    {
      case MailType.MAIL_TYPE_PVP_BATTLE_REPORT:
      case MailType.MAIL_TYPE_ANNI_BATTLE_REPORT:
        this.ShowPvpBattleToast(mail);
        break;
      case MailType.MAIL_TYPE_PVE_BATTLE_REPORT:
        this.ShowPveBattleToast(mail);
        break;
    }
  }

  private void ShowPvpBattleToast(AbstractMailEntry mail)
  {
  }

  private void ShowPveBattleToast(AbstractMailEntry mail)
  {
    WarReportContent data1 = JsonReader.Deserialize<WarReport>(Utils.Object2Json((object) mail.data)).data;
    string empty = string.Empty;
    Dictionary<string, string> para = new Dictionary<string, string>();
    WorldEncounterData data2 = ConfigManager.inst.DB_WorldEncount.GetData(long.Parse(data1.defender.uid));
    para["0"] = data2.level;
    para["1"] = data2.Name;
    ToastData data3 = ConfigManager.inst.DB_Toast.GetData(!data1.IsWinner() ? "toast_battle_failed_pve" : "toast_battle_victory_pve");
    if (data3 == null)
      return;
    ToastManager.Instance.AddToast("BASIC_TOAST", (object) new ToastArgs(data3, para));
  }

  private void OnGetUnreadCount(Hashtable data)
  {
    foreach (int num in Enum.GetValues(typeof (MailCategory)))
    {
      if (data.ContainsKey((object) num.ToString()))
        this._mailListsByType[(MailCategory) num].UnReadCount = int.Parse(data[(object) num.ToString()].ToString());
      else
        this._mailListsByType[(MailCategory) num].UnReadCount = 0;
    }
    this.MailCount = int.Parse(data[(object) "total"].ToString());
  }

  public void UpdateUnReadCount()
  {
    int b = 0;
    foreach (string name in Enum.GetNames(typeof (MailCategory)))
    {
      MailCategory index = (MailCategory) Enum.Parse(typeof (MailCategory), name);
      if (index != MailCategory.Favorite)
        b += this._mailListsByType[index].UnReadCount;
    }
    this.MailCount = Mathf.Max(0, b);
  }
}
