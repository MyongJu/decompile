﻿// Decompiled with JetBrains decompiler
// Type: MarchSpeedUpItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MarchSpeedUpItemRenderer : MonoBehaviour
{
  public UILabel m_ItemName;
  public UILabel m_ItemDesc;
  public UILabel m_ItemOwned;
  public UILabel m_ItemPrice;
  public UITexture m_ItemIcon;
  public UIButton m_BuyButton;
  public GameObject m_FreePanel;
  public GameObject m_GoldPanel;
  public string m_ShopItemID;
  private ShopStaticInfo m_ShopItem;
  private ItemStaticInfo m_ItemInfo;
  private System.Action<ShopStaticInfo, bool> m_Callback;

  public void Initialize(System.Action<ShopStaticInfo, bool> callback)
  {
    this.m_Callback = callback;
    this.m_ShopItem = ConfigManager.inst.DB_Shop.GetShopData(this.m_ShopItemID);
    this.m_ItemInfo = ConfigManager.inst.DB_Items.GetItem(this.m_ShopItem.Item_InternalId);
    this.UpdateUI();
  }

  public void UpdateUI()
  {
    if (this.m_ItemInfo == null)
      return;
    this.m_ItemName.text = this.m_ItemInfo.LocName;
    this.m_ItemDesc.text = this.m_ItemInfo.LocDescription;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, this.m_ItemInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    long shopItemPrice = (long) ItemBag.Instance.GetShopItemPrice(this.m_ShopItem.internalId);
    this.m_ItemPrice.text = Utils.FormatThousands(shopItemPrice.ToString());
    int itemCountByShopId = ItemBag.Instance.GetItemCountByShopID(this.m_ShopItem.ID);
    this.m_ItemOwned.text = Utils.FormatThousands(itemCountByShopId.ToString());
    this.m_FreePanel.SetActive(itemCountByShopId > 0);
    this.m_GoldPanel.SetActive(itemCountByShopId == 0);
    if (PlayerData.inst.userData.currency.gold >= shopItemPrice)
      this.m_ItemPrice.color = Color.white;
    else
      this.m_ItemPrice.color = Color.red;
  }

  public void OnUse()
  {
    if (this.m_Callback == null)
      return;
    this.m_Callback(this.m_ShopItem, false);
  }

  public void OnBuyAndUse()
  {
    if (this.m_Callback == null)
      return;
    this.m_Callback(this.m_ShopItem, true);
  }
}
