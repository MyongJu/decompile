﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarTrackData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class AllianceWarTrackData
{
  private List<AllianceWarTrackData.TrackMemberData> _memberDataList = new List<AllianceWarTrackData.TrackMemberData>();
  private string _startTracker = string.Empty;
  private const int ONE_HOUR_SECONDS = 3600;
  private int _lastRefreshTime;
  private int _trackEndTime;
  private int _nextRefreshTime;
  private long _startTrackUid;
  private bool _fromCache;
  private string _trackedFortressLocation;

  public int LastRefreshTime
  {
    get
    {
      return this._lastRefreshTime;
    }
  }

  public int TrackEndTime
  {
    get
    {
      return this._trackEndTime;
    }
  }

  public int NextRefreshTime
  {
    get
    {
      return this._nextRefreshTime;
    }
  }

  public long StartTrackUid
  {
    get
    {
      return this._startTrackUid;
    }
  }

  public bool FromCache
  {
    get
    {
      return this._fromCache;
    }
  }

  public List<AllianceWarTrackData.TrackMemberData> MemberDataList
  {
    get
    {
      return this._memberDataList;
    }
  }

  public Coordinate TrackedFortressLocation
  {
    get
    {
      int num;
      int result1 = num = -1;
      int result2 = num;
      int result3 = num;
      if (!string.IsNullOrEmpty(this._trackedFortressLocation))
      {
        string[] strArray = this._trackedFortressLocation.Split(':');
        if (strArray != null && strArray.Length == 3)
        {
          int.TryParse(strArray[0], out result3);
          int.TryParse(strArray[1], out result2);
          int.TryParse(strArray[2], out result1);
        }
      }
      return new Coordinate(result3, result2, result1);
    }
  }

  public bool IsTracking
  {
    get
    {
      return NetServerTime.inst.ServerTimestamp < this.TrackEndTime;
    }
  }

  public int TrackCost
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_warfare_track_cost");
      if (data != null)
        return data.ValueInt;
      return 0;
    }
  }

  public bool IsGoldEnough2Track
  {
    get
    {
      return PlayerData.inst.userData.currency.gold >= (long) this.TrackCost;
    }
  }

  public int TrackingDuration
  {
    get
    {
      int num1 = this.TrackEndTime - NetServerTime.inst.ServerTimestamp;
      if (!this.IsTracking)
      {
        GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_warfare_track_duration");
        if (data != null)
          num1 = data.ValueInt;
      }
      if (num1 <= 0)
        return 0;
      int num2 = num1 / 3600;
      if (num1 % 3600 > 0)
        return num2 + 1;
      return num2;
    }
  }

  public string StartTracker
  {
    get
    {
      return this._startTracker;
    }
  }

  public string GetByTrackingName(long uid)
  {
    for (int index = 0; index < this.MemberDataList.Count; ++index)
    {
      if (this.MemberDataList[index].Uid == uid)
        return this.MemberDataList[index].Name;
    }
    return string.Empty;
  }

  private void Clear()
  {
    this._nextRefreshTime = 0;
    this._trackEndTime = 0;
    this._startTrackUid = 0L;
    this._lastRefreshTime = 0;
    this._fromCache = false;
    this._memberDataList.Clear();
  }

  public void Decode(Hashtable data)
  {
    if (data == null)
      return;
    this.Clear();
    DatabaseTools.UpdateData(data, "next_refresh_time", ref this._nextRefreshTime);
    DatabaseTools.UpdateData(data, "from_cache", ref this._fromCache);
    DatabaseTools.UpdateData(data, "track_end_time", ref this._trackEndTime);
    DatabaseTools.UpdateData(data, "start_track_uid", ref this._startTrackUid);
    DatabaseTools.UpdateData(data, "start_track_name", ref this._startTracker);
    DatabaseTools.UpdateData(data, "last_refresh_time", ref this._lastRefreshTime);
    DatabaseTools.UpdateData(data, "fortress", ref this._trackedFortressLocation);
    if (!data.ContainsKey((object) "users") || data[(object) "users"] == null)
      return;
    Hashtable hashtable = data[(object) "users"] as Hashtable;
    if (hashtable == null)
      return;
    this._memberDataList.Clear();
    IEnumerator enumerator = hashtable.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      long result = 0;
      long.TryParse(enumerator.Current.ToString(), out result);
      Hashtable data1 = hashtable[(object) enumerator.Current.ToString()] as Hashtable;
      if (data1 != null)
        this._memberDataList.Add(new AllianceWarTrackData.TrackMemberData(result, data1));
    }
  }

  public class TrackMemberData
  {
    private string _name;
    private string _kxy;
    private string _title;
    private long _power;
    private long _uid;
    private int _portrait;

    public TrackMemberData(long uid, Hashtable data)
    {
      if (data == null)
        return;
      this._uid = uid;
      DatabaseTools.UpdateData(data, "name", ref this._name);
      DatabaseTools.UpdateData(data, "kxy", ref this._kxy);
      DatabaseTools.UpdateData(data, "title", ref this._title);
      DatabaseTools.UpdateData(data, "power", ref this._power);
      DatabaseTools.UpdateData(data, "portrait", ref this._portrait);
    }

    public string Name
    {
      get
      {
        return this._name;
      }
    }

    public Coordinate Location
    {
      get
      {
        int num;
        int result1 = num = -1;
        int result2 = num;
        int result3 = num;
        if (!string.IsNullOrEmpty(this._kxy))
        {
          string[] strArray = this._kxy.Split(':');
          if (strArray != null && strArray.Length == 3)
          {
            int.TryParse(strArray[0], out result3);
            int.TryParse(strArray[1], out result2);
            int.TryParse(strArray[2], out result1);
          }
        }
        return new Coordinate(result3, result2, result1);
      }
    }

    public string Title
    {
      get
      {
        return this._title;
      }
    }

    public long Power
    {
      get
      {
        return this._power;
      }
    }

    public long Uid
    {
      get
      {
        return this._uid;
      }
    }

    public int Portrait
    {
      get
      {
        return this._portrait;
      }
    }
  }
}
