﻿// Decompiled with JetBrains decompiler
// Type: AllianceTechDonateDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceTechDonateDlg : Popup
{
  private Dictionary<int, Color> progressColors = new Dictionary<int, Color>();
  public UILabel allianceTechName;
  public UILabel allianceDesc;
  public UITexture icon;
  public UILabel allainceLev;
  public UILabel currentValue;
  public UILabel nextValue;
  public UILabel exp;
  public UILabel title;
  public UIProgressBar progressbar;
  public UILabel normalTip;
  public UILabel researchTip;
  public UIButton researchButton;
  public UILabel remainTime;
  public GameObject remainTimeContent;
  public GameObject donateContainer;
  public GameObject mark;
  public AllianceTechDonateItem[] donateItems;
  private int researchId;
  public UISprite[] progressBars;
  public UILabel rewardPlus;
  private bool _isShow;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    this._isShow = true;
    base.OnShow(orgParam);
    AllianceTechDonateDlg.AllianceTechDonateParam allianceTechDonateParam = orgParam as AllianceTechDonateDlg.AllianceTechDonateParam;
    if (allianceTechDonateParam == null)
      return;
    this.Init();
    this.researchId = allianceTechDonateParam.researchId;
    for (int index = 0; index < this.progressBars.Length; ++index)
      NGUITools.SetActive(this.progressBars[index].gameObject, false);
    this.RefreshNormalUI();
    this.UpdateUI();
    this.RefreshCallBackUI();
    this.AddEventHandler();
  }

  private void Init()
  {
  }

  private void ResetProgressBarColorAndValue()
  {
    float currentProgressValue = 0.0f;
    int donateProgressIndex = this.GetDonateProgressIndex(ref currentProgressValue);
    UISprite progressBar = this.progressBars[donateProgressIndex];
    this.progressbar.value = 1f;
    this.progressbar.foregroundWidget = (UIWidget) progressBar;
    this.progressbar.value = !this.CurrentDonateIsFull() ? currentProgressValue : 1f;
    this.SetPreProgressBar(donateProgressIndex);
  }

  private void SetPreProgressBar(int index)
  {
    for (int index1 = 0; index1 <= index; ++index1)
    {
      UISprite progressBar = this.progressBars[index1];
      progressBar.width = 1516;
      NGUITools.SetActive(progressBar.gameObject, true);
    }
  }

  private int GetDonateProgressIndex(ref float currentProgressValue)
  {
    AllianceTechData allianceTechData = DBManager.inst.DB_AllianceTech.Get(this.researchId);
    AllianceTechInfo allianceTechInfo = ConfigManager.inst.DB_AllianceTech.GetItem(this.researchId);
    int num1 = 0;
    int num2 = allianceTechData != null ? allianceTechData.donation : 0;
    currentProgressValue = 0.0f;
    string str = string.Empty;
    if (allianceTechInfo.Exp1 > num2)
    {
      num1 = 0;
      currentProgressValue = (float) num2 / (float) allianceTechInfo.Exp1;
      str = num2.ToString() + "/" + allianceTechInfo.Exp1.ToString();
    }
    else if (allianceTechInfo.Exp2 > num2)
    {
      num1 = 1;
      currentProgressValue = (float) (num2 - allianceTechInfo.Exp1) / (float) (allianceTechInfo.Exp2 - allianceTechInfo.Exp1);
      str = (num2 - allianceTechInfo.Exp1).ToString() + "/" + (allianceTechInfo.Exp2 - allianceTechInfo.Exp1).ToString();
    }
    else if (allianceTechInfo.Exp3 > num2)
    {
      num1 = 2;
      currentProgressValue = (float) (num2 - allianceTechInfo.Exp2) / (float) (allianceTechInfo.Exp3 - allianceTechInfo.Exp2);
      str = (num2 - allianceTechInfo.Exp2).ToString() + "/" + (allianceTechInfo.Exp3 - allianceTechInfo.Exp2).ToString();
    }
    else if (allianceTechInfo.Exp4 > num2)
    {
      num1 = 3;
      currentProgressValue = (float) (num2 - allianceTechInfo.Exp3) / (float) (allianceTechInfo.Exp4 - allianceTechInfo.Exp3);
      str = (num2 - allianceTechInfo.Exp3).ToString() + "/" + (allianceTechInfo.Exp4 - allianceTechInfo.Exp3).ToString();
    }
    else if (allianceTechInfo.Exp5 > num2)
    {
      num1 = 4;
      currentProgressValue = (float) (num2 - allianceTechInfo.Exp4) / (float) (allianceTechInfo.Exp5 - allianceTechInfo.Exp4);
      str = (num2 - allianceTechInfo.Exp4).ToString() + "/" + (allianceTechInfo.Exp5 - allianceTechInfo.Exp4).ToString();
    }
    this.exp.text = str;
    return num1;
  }

  private void UpdateUIBySecond(int time)
  {
    this.UpdateUI();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void RefreshNormalUI()
  {
    this.title.text = ScriptLocalization.Get("alliance_knowledge_donations_title", true);
    this.rewardPlus.text = "+50%";
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_donate_mark_boost");
    if (data != null)
      this.rewardPlus.text = "+" + (object) (data.Value * 100.0) + "%";
    DBManager.inst.DB_AllianceTech.Get(this.researchId);
    AllianceTechInfo allianceTechInfo1 = ConfigManager.inst.DB_AllianceTech.GetItem(this.researchId);
    this.allianceTechName.text = allianceTechInfo1.LocName;
    this.allianceDesc.text = allianceTechInfo1.LocDescription;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, allianceTechInfo1.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.allainceLev.text = allianceTechInfo1.Level.ToString();
    this.currentValue.text = Utils.GetBenefitsOneDesc(allianceTechInfo1.Benefits);
    if (allianceTechInfo1.ResearchType == "alliance_temple_level_1")
      this.currentValue.text = "[99ff00]" + (object) allianceTechInfo1.Level;
    AllianceTechInfo allianceTechInfo2 = ConfigManager.inst.DB_AllianceTech.GetNextAllianceTechInfo(this.researchId);
    if (allianceTechInfo2 == null)
    {
      NGUITools.SetActive(this.nextValue.gameObject, allianceTechInfo2 == null);
    }
    else
    {
      this.nextValue.text = Utils.GetBenefitsOneDesc(allianceTechInfo2.Benefits);
      if (!(allianceTechInfo1.ResearchType == "alliance_temple_level_1"))
        return;
      this.nextValue.text = "[99ff00]" + (object) allianceTechInfo2.Level;
    }
  }

  private void UpdateUI()
  {
    if (!this._isShow || PlayerData.inst.allianceData == null)
      return;
    this.normalHide();
    int time = PlayerData.inst.allianceTechManager.codeDownTime - NetServerTime.inst.ServerTimestamp;
    if (time > 0)
      this.remainTime.text = Utils.FormatTime(time, false, false, true);
    this.remainTime.color = !PlayerData.inst.allianceTechManager.canDonate ? new Color(1f, 0.0f, 0.0f) : new Color(0.7176471f, 0.3843137f, 0.0f);
  }

  private void RefreshCallBackUI()
  {
    AllianceTechDonateData donateData = PlayerData.inst.allianceTechManager.GetDonateData(this.researchId);
    if (donateData == null)
    {
      AllianceTechDonateData.AllianceTechDonateStruct donateItem = this.CreateDonateItem();
      for (int index = 0; index < this.donateItems.Length; ++index)
      {
        AllianceTechDonateData.AllianceTechDonateStruct allianceTechDonateStruct = (AllianceTechDonateData.AllianceTechDonateStruct) null;
        if (index == 0)
          allianceTechDonateStruct = donateItem;
        this.donateItems[index].SetData(allianceTechDonateStruct);
        this.donateItems[index].OnDonateDelegater = new System.Action<int, int>(this.DonateHandler);
        this.donateItems[index].OnRefreshHandler = new System.Action(this.OnRefreshHandler);
      }
    }
    else
    {
      for (int index = 0; index < donateData.datas.Count; ++index)
      {
        AllianceTechDonateData.AllianceTechDonateStruct data = donateData.datas[index];
        if (this.donateItems.Length > index)
        {
          this.donateItems[index].SetData(data);
          this.donateItems[index].OnDonateDelegater = new System.Action<int, int>(this.DonateHandler);
          this.donateItems[index].OnRefreshHandler = new System.Action(this.OnRefreshHandler);
        }
      }
    }
    this.ResetProgressBarColorAndValue();
  }

  private AllianceTechDonateData.AllianceTechDonateStruct CreateDonateItem()
  {
    AllianceTechDonateData allianceTechDonateData = new AllianceTechDonateData();
    AllianceTechInfo allianceTechInfo = ConfigManager.inst.DB_AllianceTech.GetItem(this.researchId);
    AllianceTechTier allianceTechTier = ConfigManager.inst.DB_AllianceTech.GetAllianceTechTier(allianceTechInfo.TierId);
    AllianceTechDonateData.AllianceTechDonateStruct techDonateStruct = new AllianceTechDonateData.AllianceTechDonateStruct();
    techDonateStruct.rewardFund = (long) allianceTechTier.Reward_Fund_Value;
    techDonateStruct.rewardHonor = (long) allianceTechTier.Reward_Honor_Value;
    techDonateStruct.rewardXp = (long) allianceTechTier.Reward_EXP_Value;
    techDonateStruct.rewardDonation = (long) allianceTechTier.Reward_Donation_Value;
    techDonateStruct.type = allianceTechInfo.InitResources;
    string type = techDonateStruct.type;
    if (type != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceTechDonateDlg.\u003C\u003Ef__switch\u0024map17 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceTechDonateDlg.\u003C\u003Ef__switch\u0024map17 = new Dictionary<string, int>(5)
        {
          {
            "food",
            0
          },
          {
            "wood",
            1
          },
          {
            "ore",
            2
          },
          {
            "silver",
            3
          },
          {
            "gold",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceTechDonateDlg.\u003C\u003Ef__switch\u0024map17.TryGetValue(type, out num))
      {
        switch (num)
        {
          case 0:
            techDonateStruct.count = (long) allianceTechTier.Donate_Food_Value;
            break;
          case 1:
            techDonateStruct.count = (long) allianceTechTier.Donate_Wood_Value;
            break;
          case 2:
            techDonateStruct.count = (long) allianceTechTier.Donate_Ore_Value;
            break;
          case 3:
            techDonateStruct.count = (long) allianceTechTier.Donate_Silver_Value;
            break;
          case 4:
            techDonateStruct.count = (long) allianceTechTier.Donate_Gold_Value;
            break;
        }
      }
    }
    return techDonateStruct;
  }

  private void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.UpdateUIBySecond);
    AllianceManager.Instance.onLeave += new System.Action(this.OnLeavelHandler);
  }

  private void OnAllianceDataChange(long allianceId)
  {
    if (allianceId != PlayerData.inst.allianceId)
      return;
    this.UpdateUI();
    this.RefreshCallBackUI();
  }

  private void OnLeavelHandler()
  {
    this.OnCloseHandler();
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.UpdateUIBySecond);
    AllianceManager.Instance.onLeave -= new System.Action(this.OnLeavelHandler);
  }

  private void DonateHandler(int index, int gold)
  {
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_donate_min_level");
    if (data != null && PlayerData.inst.playerCityData.level < data.ValueInt)
    {
      Utils.ShowException(1300090);
    }
    else
    {
      if (!PlayerData.inst.allianceTechManager.canDonate)
        return;
      Hashtable postData = new Hashtable();
      postData.Add((object) "uid", (object) PlayerData.inst.uid);
      postData.Add((object) "research_id", (object) this.researchId);
      postData.Add((object) nameof (index), (object) index);
      if (gold > 0)
        postData.Add((object) nameof (gold), (object) gold.ToString());
      RequestManager.inst.SendRequest("AllianceTech:donate", postData, new System.Action<bool, object>(this.DonateCallBack), true);
    }
  }

  private void OnRefreshHandler()
  {
    this.UpdateUI();
    this.RefreshCallBackUI();
  }

  private void DonateCallBack(bool success, object result)
  {
    if (!success)
      return;
    DBManager.inst.DB_AllianceTech.UpdateDatas(result, (long) NetServerTime.inst.ServerTimestamp);
    PlayerData.inst.allianceTechManager.Update(result);
    this.UpdateUI();
    this.RefreshCallBackUI();
  }

  public void Research()
  {
    if (DBManager.inst.DB_AllianceTech.researchingId > 0)
    {
      this.OnCloseHandler();
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_alliance_tech_already_researching", true), (System.Action) null, 4f, false);
    }
    else if (!this.CurrentDonateIsFull())
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_alliance_tech_already_researching", true), (System.Action) null, 4f, false);
      this.OnCloseHandler();
    }
    else
      RequestManager.inst.SendRequest("AllianceTech:research", new Hashtable()
      {
        {
          (object) "uid",
          (object) PlayerData.inst.uid
        },
        {
          (object) "research_id",
          (object) this.researchId
        }
      }, new System.Action<bool, object>(this.OnResearchCallBack), true);
  }

  private void OnResearchCallBack(bool success, object result)
  {
    if (!success)
      return;
    this.OnCloseHandler();
  }

  private void normalHide()
  {
    NGUITools.SetActive(this.mark.gameObject, PlayerData.inst.allianceData.techMark == this.researchId);
    int num = PlayerData.inst.allianceTechManager.codeDownTime - NetServerTime.inst.ServerTimestamp;
    bool flag = DBManager.inst.DB_AllianceTech.researchingId > 0;
    NGUITools.SetActive(this.remainTimeContent, num > 0);
    NGUITools.SetActive(this.donateContainer, !this.CurrentDonateIsFull());
    NGUITools.SetActive(this.researchTip.gameObject, flag && this.IsR4Member());
    if (this.CurrentDonateIsFull())
    {
      NGUITools.SetActive(this.normalTip.gameObject, !this.IsR4Member());
      NGUITools.SetActive(this.researchButton.gameObject, this.IsR4Member() && !flag);
      NGUITools.SetActive(this.remainTimeContent, false);
      this.progressbar.value = 1f;
    }
    else
    {
      NGUITools.SetActive(this.researchButton.gameObject, false);
      NGUITools.SetActive(this.normalTip.gameObject, false);
      NGUITools.SetActive(this.researchTip.gameObject, false);
    }
  }

  private bool CurrentDonateIsFull()
  {
    AllianceTechData allianceTechData = DBManager.inst.DB_AllianceTech.Get(this.researchId);
    if (allianceTechData == null)
      return false;
    AllianceTechInfo allianceTechInfo = ConfigManager.inst.DB_AllianceTech.GetItem(this.researchId);
    return allianceTechData.donation >= allianceTechInfo.TotalExp;
  }

  private bool IsR4Member()
  {
    int num = 1;
    AllianceMemberData allianceMemberData = PlayerData.inst.allianceData.members.Get(PlayerData.inst.userData.uid);
    if (allianceMemberData != null)
      num = allianceMemberData.title;
    return num >= 4;
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this._isShow = false;
    base.OnHide(orgParam);
    this.Clear();
    this.RemoveEventHandler();
  }

  public class AllianceTechDonateParam : Popup.PopupParameter
  {
    public int researchId;
  }
}
