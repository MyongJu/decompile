﻿// Decompiled with JetBrains decompiler
// Type: GoldReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class GoldReward : QuestReward
{
  public readonly long value;

  public GoldReward(long goldCount)
  {
    this.value = goldCount;
  }

  public override void Claim()
  {
    base.Claim();
  }

  public override string GetRewardIconName()
  {
    return "icon_currency_premium";
  }

  public override string GetRewardTypeName()
  {
    return "Gold";
  }

  public override string GetValueText()
  {
    return Utils.FormatShortThousands((int) this.value);
  }

  public override int GetValue()
  {
    return (int) this.value;
  }
}
