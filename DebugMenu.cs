﻿// Decompiled with JetBrains decompiler
// Type: DebugMenu
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UI;
using UnityEngine;

public class DebugMenu : MonoBehaviour
{
  public static bool ShowBlockBoundary = false;
  private static int MAX_COUNT = 5;
  public static char[] SERVER_URL_SPLIT_CHARS = new char[2]
  {
    ' ',
    '\t'
  };
  private static List<KeyValuePair<string, string>> _serverURLs = (List<KeyValuePair<string, string>>) null;
  private static List<KeyValuePair<string, string>> _serverToolURLs = (List<KeyValuePair<string, string>>) null;
  private static string[] _serverNameList = (string[]) null;
  private static string[] _serverUrlList = (string[]) null;
  public static float SCALE_FACTOR_X = (float) Screen.width / 800f;
  public static float SCALE_FACTOR_Y = (float) Screen.height / 480f;
  public static int FIRST_LINE_START_X = 0;
  public static int FIRST_LINE_START_Y = 100;
  public static int FIRST_LABEL_WIDTH = 80;
  public static int FIRST_LABEL_HEITH = 80;
  public static int FIRST_LABEL_X = DebugMenu.FIRST_LINE_START_X;
  public static int FIRST_LABEL_Y = DebugMenu.FIRST_LINE_START_Y;
  public static int FIRST_BUTTON_1_WIDTH = 80;
  public static int FIRST_BUTTON_1_HEIGHT = 80;
  public static int FIRST_BUTTON_1_X = DebugMenu.FIRST_LABEL_X + DebugMenu.FIRST_LABEL_WIDTH;
  public static int FIRST_BUTTON_1_Y = DebugMenu.FIRST_LINE_START_Y;
  public static int FIRST_TEXT_WIDTH = 360;
  public static int FIRST_TEXT_HEIGHT = 80;
  public static int FIRST_TEXT_X = DebugMenu.FIRST_BUTTON_1_X + DebugMenu.FIRST_BUTTON_1_WIDTH;
  public static int FIRST_TEXT_Y = DebugMenu.FIRST_LINE_START_Y;
  public static int FIRST_BUTTON_2_WIDTH = 80;
  public static int FIRST_BUTTON_2_HEIGHT = 80;
  public static int FIRST_BUTTON_2_X = DebugMenu.FIRST_TEXT_X + DebugMenu.FIRST_TEXT_WIDTH;
  public static int FIRST_BUTTON_2_Y = DebugMenu.FIRST_LINE_START_Y;
  public static int SECOND_LINE_START_X = 0;
  public static int SECOND_LINE_START_Y = 200;
  public static int SECOND_LABEL_WIDTH = 80;
  public static int SECOND_LABEL_HEITH = 80;
  public static int SECOND_LABEL_X = DebugMenu.SECOND_LINE_START_X;
  public static int SECOND_LABEL_Y = DebugMenu.SECOND_LINE_START_Y;
  public static int SECOND_BUTTON_1_WIDTH = 80;
  public static int SECOND_BUTTON_1_HEIGHT = 80;
  public static int SECOND_BUTTON_1_X = DebugMenu.SECOND_LABEL_X + DebugMenu.SECOND_LABEL_WIDTH;
  public static int SECOND_BUTTON_1_Y = DebugMenu.SECOND_LINE_START_Y;
  public static int SECOND_TEXT_WIDTH = 360;
  public static int SECOND_TEXT_HEIGHT = 80;
  public static int SECOND_TEXT_X = DebugMenu.SECOND_BUTTON_1_X + DebugMenu.SECOND_BUTTON_1_WIDTH;
  public static int SECOND_TEXT_Y = DebugMenu.SECOND_LINE_START_Y;
  public static int SECOND_BUTTON_2_WIDTH = 80;
  public static int SECOND_BUTTON_2_HEIGHT = 80;
  public static int SECOND_BUTTON_2_X = DebugMenu.SECOND_TEXT_X + DebugMenu.SECOND_TEXT_WIDTH;
  public static int SECOND_BUTTON_2_Y = DebugMenu.SECOND_LINE_START_Y;
  public static int THIRD_LINE_START_X = 0;
  public static int THIRD_LINE_START_Y = 300;
  public static int THIRD_BUTTON_WIDTH = 600;
  public static int THIRD_BUTTON_HEIGHT = 80;
  public static int THIRD_BUTTON_X = DebugMenu.THIRD_LINE_START_X;
  public static int THIRD_BUTTON_Y = DebugMenu.THIRD_LINE_START_Y;
  private float _updateInterval = 1f;
  private string _eqpIDName = string.Empty;
  private string _eqpTypeName = string.Empty;
  private float lastClickTime = -10f;
  private Vector3 scale = Vector3.one;
  private string consoleInput = string.Empty;
  private string focusedControlName = string.Empty;
  private string annoucementTest = string.Empty;
  private string tab3InputStr = "http://";
  private string nativeRtn = string.Empty;
  private List<int> popups = new List<int>();
  private string[] Tab4str = new string[10];
  private string _accountId = string.Empty;
  private const float debugMenuWidthConst = 800f;
  private const float debugMenuHeightConst = 600f;
  private const float DELTA_CLICK_TIME = 1f;
  public const int SERVER_URL_CUSTOM_INDEX = 0;
  public const int SERVER_URL_DEFAULT_INDEX = 1;
  public const int ADD_EQP_RECT_X = 0;
  public const int ADD_EQP_RECT_Y = 100;
  public const int ADD_EQP_RECT_WIDTH = 600;
  public const int ADD_EQP_RECT_HEIGHT = 300;
  public const int EACH_LINE_STEP = 100;
  public const int LINE_HEIGHT = 80;
  public const int LINE_WIDTH = 600;
  public const int LABEL_WIDTH = 80;
  public const int LABEL_HEIGHT = 80;
  public const int BUTTON_WIDTH = 80;
  public const int BUTTON_HEIGHT = 80;
  public const int TEXT_AREA_WIDTH = 360;
  public const int TEXT_AREA_HEIGHT = 80;
  private bool _display;
  private float _lastInterval;
  private int _frames;
  private float _ms;
  private float _fps;
  private string _eqpQualityName;
  private static string _eqpFilePath;
  private ArrayList _eqpType;
  private int _eqpTypeIndex;
  private ArrayList _eqps;
  private ArrayList _eqpsIndex;
  private bool initFinish;
  private DebugConsole console;
  private int selectTab;
  private System.Action[] uifunc;
  private long fakeHostUid;
  private long orgHostUid;
  private long contactUid;
  private bool testFullscreen;
  private PlayerData _playerData;
  private int popupId;
  private int Tab4Type;
  private LogConsole _log;
  private Vector2 _scrollPosition;

  private void Start()
  {
    this._lastInterval = Time.realtimeSinceStartup;
    this._frames = 0;
    for (int index = 0; index < 10; ++index)
      this.Tab4str[index] = string.Empty;
    this.uifunc = (System.Action[]) null;
    this.uifunc = new System.Action[2]
    {
      new System.Action(this.LogPageRenderer),
      new System.Action(this.AccountPageRenderer)
    };
    this.SetupLogPage();
  }

  private void Update()
  {
    if (!this.initFinish && (UnityEngine.Object) UnityEngine.Object.FindObjectOfType<PublicHUD>() != (UnityEngine.Object) null)
      this.initFinish = true;
    ++this._frames;
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    if ((double) realtimeSinceStartup <= (double) this._lastInterval + (double) this._updateInterval)
      return;
    this._fps = (float) this._frames / (realtimeSinceStartup - this._lastInterval);
    this._ms = 1000f / Mathf.Max(this._fps, 1E-05f);
    this._frames = 0;
    this._lastInterval = realtimeSinceStartup;
  }

  private bool CheckTouchCount()
  {
    if (Input.touchCount < 5)
      return false;
    int num = 0;
    foreach (UnityEngine.Touch touch in Input.touches)
    {
      if (touch.phase == TouchPhase.Stationary)
        ++num;
    }
    return num == DebugMenu.MAX_COUNT;
  }

  private void Console()
  {
    if (Event.current.type == UnityEngine.EventType.KeyDown && string.Equals(this.focusedControlName, "DebugConsole"))
    {
      if ((Event.current.keyCode == KeyCode.Return || Event.current.keyCode == KeyCode.KeypadEnter || (int) Event.current.character == 10) && !string.IsNullOrEmpty(this.consoleInput))
      {
        DebugConsole.Instance.EnterCommand(this.consoleInput);
        this.consoleInput = string.Empty;
      }
      if (Event.current.keyCode == KeyCode.UpArrow)
        this.consoleInput = DebugConsole.Instance.PreviousCommand();
      if (Event.current.keyCode == KeyCode.DownArrow)
        this.consoleInput = DebugConsole.Instance.NextCommand();
    }
    this.focusedControlName = GUI.GetNameOfFocusedControl();
    GUI.SetNextControlName("DebugConsole");
    this.consoleInput = GUI.TextField(new Rect(0.0f, (float) (Screen.height - 24), (float) Screen.width, 24f), this.consoleInput);
  }

  private void tab5()
  {
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("Bomb!!\n(Delete Player)", GUILayout.Width(200f), GUILayout.Height(100f)))
    {
      string uniqueIdentifier = SystemInfo.deviceUniqueIdentifier;
      WWW www = new WWW(NetApi.GetCurrentUrl() + "?key=1&class=Debug&method=resetAccountByUUID&params[uuid]=" + AccountManager.Instance.AccountId);
      GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Deep);
    }
    if (GUILayout.Button("Build All Building to 30", GUILayout.Width(200f), GUILayout.Height(100f)))
      RequestManager.inst.SendLoader("debug:allBuildingFullLevel", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "city_id", (object) PlayerData.inst.cityId), (System.Action<bool, object>) null, true, false);
    if (GUILayout.Button("Unlock All Tech", GUILayout.Width(200f), GUILayout.Height(100f)))
      RequestManager.inst.SendLoader("debug:allResearchFullLevel", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "city_id", (object) PlayerData.inst.cityId), (System.Action<bool, object>) null, true, false);
    if (!GUILayout.Button("Send LocalNotifaction", GUILayout.Width(100f), GUILayout.Height(100f)))
      ;
    if (GUILayout.Button("Show HelpShift FASQS", GUILayout.Width(100f), GUILayout.Height(100f)))
      AccountManager.Instance.ShowFASQs();
    if (GUILayout.Button("Show HelpShift ShowConversation", GUILayout.Width(100f), GUILayout.Height(100f)))
    {
      List<FunplusProduct> productList = PaymentManager.Instance.ProductList;
      string str = string.Empty;
      for (int index = 0; index < productList.Count; ++index)
        str = productList[index].GetProductId();
    }
    if (GUILayout.Button("HIDE", GUILayout.Width(200f), GUILayout.Height(100f)))
    {
      if (!this.testFullscreen)
        Alert.Show("123");
      this.testFullscreen = !this.testFullscreen;
    }
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("build hall of war1", GUILayout.Width(100f), GUILayout.Height(100f)))
      RequestManager.inst.SendLoader("debug:addBuilding", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "city_id", (object) PlayerData.inst.cityId, (object) "type", (object) "war_rally_1"), (System.Action<bool, object>) null, true, false);
    if (GUILayout.Button("build Embassy", GUILayout.Width(100f), GUILayout.Height(100f)))
      RequestManager.inst.SendLoader("debug:addBuilding", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "city_id", (object) PlayerData.inst.cityId, (object) "type", (object) "embassy_1"), (System.Action<bool, object>) null, true, false);
    if (GUILayout.Button("build Market", GUILayout.Width(100f), GUILayout.Height(100f)))
      RequestManager.inst.SendLoader("debug:addBuilding", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "city_id", (object) PlayerData.inst.cityId, (object) "type", (object) "marketplace_1"), (System.Action<bool, object>) null, true, false);
    if (GUILayout.Button("unload unuesd resource", GUILayout.Width(100f), GUILayout.Height(100f)))
      Resources.UnloadUnusedAssets();
    if (GUILayout.Button("Test Annoucement", GUILayout.Width(100f), GUILayout.Height(100f)))
      RequestManager.inst.SendLoader("player:sendAnnouncement", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "content", (object) this.annoucementTest), (System.Action<bool, object>) null, true, false);
    if (GUILayout.Button("TEST GOOGLE PLAY GAMES", GUILayout.Width(200f), GUILayout.Height(100f)))
      GooglePlayGameManager.Instance.ToggleDebugUI();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    this.annoucementTest = GUILayout.TextArea(this.annoucementTest);
    GUILayout.EndHorizontal();
  }

  private void tab3()
  {
    GUILayout.BeginVertical();
    this.tab3InputStr = GUILayout.TextField(this.tab3InputStr);
    if (GUILayout.Button("Change Server"))
    {
      PlayerPrefs.SetInt("ServerIndex", 0);
      PlayerPrefs.SetString("ServerURL", this.tab3InputStr);
      this._display = false;
    }
    GUILayout.EndVertical();
  }

  public static List<KeyValuePair<string, string>> serverURLs
  {
    get
    {
      if (DebugMenu._serverURLs == null)
      {
        DebugMenu._serverURLs = new List<KeyValuePair<string, string>>();
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("Custom", string.Empty));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-default", NetApi.DefaultServerURL));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-dev", "http://koa-dev.kingsgroup.cc/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-devtest", "http://koa-release.kingsgroup.cc/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-china", "http://ec2-52-6-228-115.compute-1.amazonaws.com/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-global", "http://dw-us.funplusgame.com/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-self", "http://127.0.0.1/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("37wan_master", "http://dw-cn-review.ifunplus.cn:8000/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-tianli", "http://10.0.15.39/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-zhandong", "http://10.0.33.48/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-gengbing", "http://10.0.15.42/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-wangwei", "http://10.0.0.74:8080/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-Jipeng", "http://10.0.0.126:8080/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-junjie", "http://10.0.15.33/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-wangzhe", "http://10.0.61.43/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-wangzhe-stable", "http://10.0.61.43:81/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-zhiyan", "http://10.0.0.151:8080/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-shiheng", "http://10.0.15.32/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-wonderwar_kuafu", "http://ec2-34-194-56-68.compute-1.amazonaws.com/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-docker", "http://192.168.99.100/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-wonderwar", "http://ec2-50-17-168-96.compute-1.amazonaws.com/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-ptr", "http://ec2-54-82-80-38.compute-1.amazonaws.com/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-jiacan", "http://10.0.33.138/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-wangzhe", "http://10.0.61.43/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-zhouzhirui", "http://10.0.32.185/api/"));
      }
      return DebugMenu._serverURLs;
    }
  }

  public static List<KeyValuePair<string, string>> serverToolURLs
  {
    get
    {
      if (DebugMenu._serverToolURLs == null)
      {
        DebugMenu._serverToolURLs = new List<KeyValuePair<string, string>>();
        DebugMenu._serverToolURLs.Add(new KeyValuePair<string, string>("Custom", string.Empty));
        DebugMenu._serverToolURLs.Add(new KeyValuePair<string, string>("dw-default", NetApi.DefaultToolServerURL));
        DebugMenu._serverToolURLs.Add(new KeyValuePair<string, string>("dw-dev", "http://koa-dev.kingsgroup.cc:8080/"));
        DebugMenu._serverToolURLs.Add(new KeyValuePair<string, string>("dw-devtest", "http://koa-release.kingsgroup.cc:8080/"));
        DebugMenu._serverToolURLs.Add(new KeyValuePair<string, string>("dw-china", "http://ec2-52-6-228-115.compute-1.amazonaws.com:8080/"));
        DebugMenu._serverToolURLs.Add(new KeyValuePair<string, string>("dw-global", "http://ec2-52-6-228-115.compute-1.amazonaws.com:8080/"));
        DebugMenu._serverToolURLs.Add(new KeyValuePair<string, string>("dw-self", "http://192.168.99.100/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("37wan_master", "http://dw-cn-review.ifunplus.cn:8000/api/"));
        DebugMenu._serverToolURLs.Add(new KeyValuePair<string, string>("dw-yuying", "http://10.0.1.121/api/"));
        DebugMenu._serverToolURLs.Add(new KeyValuePair<string, string>("dw-wangwei", "http://10.0.1.112:8000/"));
        DebugMenu._serverToolURLs.Add(new KeyValuePair<string, string>("dw-Jipeng", "http://10.0.0.126:8080/api/"));
        DebugMenu._serverToolURLs.Add(new KeyValuePair<string, string>("dw-zhiyan", "http://10.0.0.151:8080/api/"));
        DebugMenu._serverToolURLs.Add(new KeyValuePair<string, string>("dw-shiheng", "http://10.0.15.32/api/"));
        DebugMenu._serverToolURLs.Add(new KeyValuePair<string, string>("dw-shiheng-wuxian", "http://10.0.38.37/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-docker", "http://192.168.99.100/api/"));
        DebugMenu._serverURLs.Add(new KeyValuePair<string, string>("dw-ptr", "http://ec2-54-82-80-38.compute-1.amazonaws.com/api/"));
      }
      return DebugMenu._serverToolURLs;
    }
  }

  public static string[] serverNameList
  {
    get
    {
      DebugMenu._serverNameList = (string[]) null;
      if (DebugMenu._serverNameList == null)
      {
        DebugMenu._serverNameList = new string[DebugMenu.serverURLs.Count];
        int index = 0;
        for (int count = DebugMenu.serverURLs.Count; index < count; ++index)
          DebugMenu._serverNameList[index] = DebugMenu.serverURLs[index].Key;
      }
      return DebugMenu._serverNameList;
    }
  }

  public static string[] serverUrlList
  {
    get
    {
      DebugMenu._serverUrlList = (string[]) null;
      if (DebugMenu._serverUrlList == null)
      {
        DebugMenu._serverUrlList = new string[DebugMenu.serverURLs.Count];
        int index = 0;
        for (int count = DebugMenu.serverURLs.Count; index < count; ++index)
          DebugMenu._serverUrlList[index] = DebugMenu.serverURLs[index].Value;
      }
      return DebugMenu._serverUrlList;
    }
  }

  private void tab2()
  {
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("restart game " + NetApi.GetCurrentUrl(), GUILayout.Width(400f), GUILayout.Height(60f)))
      GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Deep);
    if (GUILayout.Button("clear " + Application.persistentDataPath, GUILayout.Width(400f), GUILayout.Height(60f)))
      Directory.Delete(Application.persistentDataPath, true);
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    for (int index = 0; index < DebugMenu.serverURLs.Count - 1; ++index)
    {
      if (GUILayout.Button(DebugMenu.serverURLs[index + 1].Key, GUILayout.Width(266.6667f), GUILayout.Height(50f)))
        PlayerPrefs.SetString("ServerURL", DebugMenu.serverURLs[index + 1].Value);
      if (index % 3 == 2)
      {
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
      }
    }
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("boost", GUILayout.Width(266.6667f), GUILayout.Height(50f)))
      GameEngine.Instance.events.Publish_OnBoostButton();
    if (!GUILayout.Button("callnative", GUILayout.Width(266.6667f), GUILayout.Height(50f)))
      ;
    GUILayout.Label(this.nativeRtn);
    GUILayout.EndHorizontal();
  }

  private void tab6()
  {
    GUILayout.BeginHorizontal();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    for (int index = 0; index < DebugMenu.serverToolURLs.Count - 1; ++index)
    {
      if (GUILayout.Button(DebugMenu.serverURLs[index + 1].Key, GUILayout.Width(266.6667f), GUILayout.Height(50f)))
        PlayerPrefs.SetString(NetApi.DefaultToolServerURL, DebugMenu._serverToolURLs[index + 1].Value);
      if (index % 3 == 2)
      {
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
      }
    }
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Label(this.nativeRtn);
    GUILayout.EndHorizontal();
  }

  private void tab1()
  {
    GUILayout.BeginHorizontal();
    GUILayout.BeginVertical(GUILayout.Width(400f));
    if (GUILayout.Button("Default Armor Load", GUILayout.Height(50f)))
    {
      ConfigManager inst = ConfigManager.inst;
      ItemInventory.Instance.SendAddInventory(inst.DB_Hero_Equipment.GetData("EQUIP_BOOTS_SCOUT_White"));
      ItemInventory.Instance.SendAddInventory(inst.DB_Hero_Equipment.GetData("EQUIP_CHEST_SCOUT_White"));
      ItemInventory.Instance.SendAddInventory(inst.DB_Hero_Equipment.GetData("EQUIP_GAUNTLETS_SCOUT_White"));
      ItemInventory.Instance.SendAddInventory(inst.DB_Hero_Equipment.GetData("EQUIP_HELM_SCOUT_White"));
      ItemInventory.Instance.SendAddInventory(inst.DB_Hero_Equipment.GetData("EQUIP_WEAP_PLAINSWORDSHIELD_White"));
    }
    GUILayout.BeginVertical(GUILayout.Width(400f));
    GUILayout.BeginHorizontal();
    GUILayout.Label("Type");
    if (this._eqpType == null || this._eqps.Count <= 0)
      GUILayout.Label("Equipment Type not exist");
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Label("ID", GUILayout.Height(50f));
    int eqpTypeIndex = this._eqpTypeIndex;
    int num1 = 0;
    if (this._eqpsIndex != null && this._eqpsIndex.Count > 0)
      num1 = (int) this._eqpsIndex[this._eqpTypeIndex];
    if (this._eqps == null || this._eqps.Count <= 0)
      GUILayout.Label("Equipment ID not exist");
    GUILayout.EndHorizontal();
    if (GUILayout.Button("Add Equipment", GUILayout.Height(50f)))
    {
      int num2 = 0;
      if (this._eqpType != null && this._eqpType.Count > 0 && (this._eqps != null && this._eqps.Count > 0))
      {
        int num3;
        if ((num3 = this._eqpType.IndexOf((object) this._eqpTypeName)) >= 0)
          this._eqpTypeIndex = num3;
        else
          num2 = -1;
        num2 -= 2;
        foreach (DebugMenu.EquipmentInfo equipmentInfo in (ArrayList) this._eqps[this._eqpTypeIndex])
        {
          if (equipmentInfo.ID.Equals(this._eqpIDName))
          {
            this._eqpsIndex[this._eqpTypeIndex] = (object) ((ArrayList) this._eqps[this._eqpTypeIndex]).IndexOf((object) equipmentInfo);
            num2 += 2;
            break;
          }
        }
        this._eqpTypeName = this.GetCurEquipmentInfo().type;
        this._eqpIDName = this.GetCurEquipmentInfo().ID;
        string str;
        switch (num2)
        {
          case -2:
            str = "ID ERROR";
            break;
          case -1:
            str = "Type ERROR";
            break;
          case 0:
            str = string.Empty;
            break;
          default:
            str = "Type & ID ERROR";
            break;
        }
        this._eqpQualityName = str;
        GUI.TextField(new Rect(215f, 252f, 200f, 20f), this._eqpIDName);
        GUI.TextField(new Rect(215f, 212f, 200f, 20f), this._eqpTypeName);
        GUI.Label(new Rect(215f, 290f, 200f, 20f), this._eqpQualityName);
      }
      if (num2 >= 0)
        this.AddEquipment(this.GetCurEquipmentInfo().ID);
    }
    DebugMenu.ShowBlockBoundary = GUILayout.Toggle(DebugMenu.ShowBlockBoundary, "Show Block Boundary");
    if (GUILayout.Button("Default English", GUILayout.Height(50f)))
      PlayerPrefs.SetString("I2 Language", "English");
    GUILayout.EndVertical();
    GUILayout.EndVertical();
    GUILayout.BeginVertical(GUILayout.Width(400f));
    if (GUILayout.Button("Add one troop", GUILayout.Width(266.6667f), GUILayout.Height(60f)))
      PresureTest.Instance.AddTroops1();
    if (GUILayout.Button("Add ten troop", GUILayout.Width(266.6667f), GUILayout.Height(60f)))
      PresureTest.Instance.AddTroops2();
    if (GUILayout.Button("Add one troop no dragon", GUILayout.Width(266.6667f), GUILayout.Height(60f)))
      FramerateSampler.Instance.needFake = !FramerateSampler.Instance.needFake;
    if (GUILayout.Button("Add ten troop no dragon", GUILayout.Width(266.6667f), GUILayout.Height(60f)))
      PresureTest.Instance.AddTroops4();
    if (GUILayout.Button("Add one troop no troop", GUILayout.Width(266.6667f), GUILayout.Height(60f)))
      PresureTest.Instance.AddTroops5();
    if (GUILayout.Button("Add ten troop no troop", GUILayout.Width(266.6667f), GUILayout.Height(60f)))
      PresureTest.Instance.AddTroops6();
    GUILayout.EndVertical();
    GUILayout.EndHorizontal();
  }

  public void AddEquipment(string equipment)
  {
    ItemInventory.Instance.SendAddInventory(ConfigManager.inst.DB_Hero_Equipment.GetData(equipment));
  }

  private static void _FreshData()
  {
    DebugMenu.FIRST_LINE_START_X = (int) ((double) DebugMenu.FIRST_LINE_START_X * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.FIRST_LINE_START_Y = (int) ((double) DebugMenu.FIRST_LINE_START_Y * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.FIRST_LABEL_WIDTH = (int) ((double) DebugMenu.FIRST_LABEL_WIDTH * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.FIRST_LABEL_HEITH = (int) ((double) DebugMenu.FIRST_LABEL_HEITH * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.FIRST_LABEL_X = (int) ((double) DebugMenu.FIRST_LABEL_X * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.FIRST_LABEL_Y = (int) ((double) DebugMenu.FIRST_LABEL_Y * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.FIRST_BUTTON_1_WIDTH = (int) ((double) DebugMenu.FIRST_BUTTON_1_WIDTH * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.FIRST_BUTTON_1_HEIGHT = (int) ((double) DebugMenu.FIRST_BUTTON_1_HEIGHT * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.FIRST_BUTTON_1_X = (int) ((double) DebugMenu.FIRST_BUTTON_1_X * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.FIRST_BUTTON_1_Y = (int) ((double) DebugMenu.FIRST_BUTTON_1_Y * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.FIRST_TEXT_WIDTH = (int) ((double) DebugMenu.FIRST_TEXT_WIDTH * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.FIRST_TEXT_HEIGHT = (int) ((double) DebugMenu.FIRST_TEXT_HEIGHT * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.FIRST_TEXT_X = (int) ((double) DebugMenu.FIRST_TEXT_X * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.FIRST_TEXT_Y = (int) ((double) DebugMenu.FIRST_TEXT_Y * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.FIRST_BUTTON_2_WIDTH = (int) ((double) DebugMenu.FIRST_BUTTON_2_WIDTH * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.FIRST_BUTTON_2_HEIGHT = (int) ((double) DebugMenu.FIRST_BUTTON_2_HEIGHT * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.FIRST_BUTTON_2_X = (int) ((double) DebugMenu.FIRST_BUTTON_2_X * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.FIRST_BUTTON_2_Y = (int) ((double) DebugMenu.FIRST_BUTTON_2_Y * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.SECOND_LINE_START_X = (int) ((double) DebugMenu.SECOND_LINE_START_X * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.SECOND_LINE_START_Y = (int) ((double) DebugMenu.SECOND_LINE_START_Y * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.SECOND_LABEL_WIDTH = (int) ((double) DebugMenu.SECOND_LABEL_WIDTH * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.SECOND_LABEL_HEITH = (int) ((double) DebugMenu.SECOND_LABEL_HEITH * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.SECOND_LABEL_X = (int) ((double) DebugMenu.SECOND_LABEL_X * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.SECOND_LABEL_Y = (int) ((double) DebugMenu.SECOND_LABEL_Y * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.SECOND_BUTTON_1_WIDTH = (int) ((double) DebugMenu.SECOND_BUTTON_1_WIDTH * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.SECOND_BUTTON_1_HEIGHT = (int) ((double) DebugMenu.SECOND_BUTTON_1_HEIGHT * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.SECOND_BUTTON_1_X = (int) ((double) DebugMenu.SECOND_BUTTON_1_X * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.SECOND_BUTTON_1_Y = (int) ((double) DebugMenu.SECOND_BUTTON_1_Y * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.SECOND_TEXT_WIDTH = (int) ((double) DebugMenu.SECOND_TEXT_WIDTH * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.SECOND_TEXT_HEIGHT = (int) ((double) DebugMenu.SECOND_TEXT_HEIGHT * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.SECOND_TEXT_X = (int) ((double) DebugMenu.SECOND_TEXT_X * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.SECOND_TEXT_Y = (int) ((double) DebugMenu.SECOND_TEXT_Y * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.SECOND_BUTTON_2_WIDTH = (int) ((double) DebugMenu.SECOND_BUTTON_2_WIDTH * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.SECOND_BUTTON_2_HEIGHT = (int) ((double) DebugMenu.SECOND_BUTTON_2_HEIGHT * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.SECOND_BUTTON_2_X = (int) ((double) DebugMenu.SECOND_BUTTON_2_X * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.SECOND_BUTTON_2_Y = (int) ((double) DebugMenu.SECOND_BUTTON_2_Y * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.THIRD_BUTTON_WIDTH = (int) ((double) DebugMenu.THIRD_BUTTON_WIDTH * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.THIRD_BUTTON_HEIGHT = (int) ((double) DebugMenu.THIRD_BUTTON_HEIGHT * (double) DebugMenu.SCALE_FACTOR_Y);
    DebugMenu.THIRD_BUTTON_X = (int) ((double) DebugMenu.THIRD_BUTTON_X * (double) DebugMenu.SCALE_FACTOR_X);
    DebugMenu.THIRD_BUTTON_Y = (int) ((double) DebugMenu.THIRD_BUTTON_Y * (double) DebugMenu.SCALE_FACTOR_Y);
  }

  public void tab4()
  {
    GUILayout.BeginVertical();
    int.TryParse(GUILayout.TextField(this.popupId.ToString()), out this.popupId);
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("Show") && (UnityEngine.Object) UIManager.inst != (UnityEngine.Object) null)
      this.popups.Add(UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
      {
        description = "Test",
        title = "Test"
      }).ID);
    if (GUILayout.Button("Hide") && (UnityEngine.Object) UIManager.inst != (UnityEngine.Object) null && this.popups.Contains(this.popupId))
    {
      this.popups.Remove(this.popupId);
      UIManager.inst.ClosePopup(this.popupId, (Popup.PopupParameter) null);
    }
    if (GUILayout.Button("Top") && (UnityEngine.Object) UIManager.inst != (UnityEngine.Object) null && this.popups.Count > 0)
    {
      UIManager.inst.ClosePopup(this.popups[this.popups.Count - 1], (Popup.PopupParameter) null);
      this.popups.RemoveAt(this.popups.Count - 1);
    }
    if (GUILayout.Button("List"))
    {
      int num = 0;
      while (num < this.popups.Count)
        ++num;
    }
    GUILayout.EndHorizontal();
    GUILayout.EndVertical();
  }

  private void InitEquipArea()
  {
    if (this.initFinish || (UnityEngine.Object) ConfigManager.inst == (UnityEngine.Object) null)
      return;
    this._eqpType = new ArrayList();
    this._eqpTypeIndex = 0;
    this._eqps = new ArrayList();
    this._eqpsIndex = new ArrayList();
    DebugMenu._FreshData();
    this.initFinish = true;
  }

  private DebugMenu.EquipmentInfo GetCurEquipmentInfo()
  {
    return (DebugMenu.EquipmentInfo) ((ArrayList) this._eqps[this._eqpTypeIndex])[(int) this._eqpsIndex[this._eqpTypeIndex]];
  }

  private void addReource(CityManager.ResourceTypes inType, int inCount)
  {
    string lower = inType.ToString().ToLower();
    Hashtable postData = new Hashtable();
    postData[(object) "city_id"] = (object) CityManager.inst.GetCityID();
    postData[(object) "type"] = (object) lower;
    postData[(object) "value"] = (object) inCount;
    RequestManager.inst.SendLoader("Debug:addResources", postData, (System.Action<bool, object>) null, true, false);
    this.addResourceCallBack(inType, inCount);
  }

  public void addResourceCallBack(CityManager.ResourceTypes inType, int inCount)
  {
    GameEngine.Instance.events.Publish_onUseAddResourceItem(inType, (double) inCount);
  }

  public static string GetHashtableInfo(Hashtable hashtable)
  {
    string str = " { ";
    foreach (string key in (IEnumerable) hashtable.Keys)
    {
      if (hashtable[(object) key] != null)
      {
        if (hashtable[(object) key] is Hashtable)
          str = str + key + " : " + DebugMenu.GetHashtableInfo(hashtable[(object) key] as Hashtable);
        else if (hashtable[(object) key] is ArrayList)
          str = str + key + " : " + DebugMenu.GetArrayListInfo(hashtable[(object) key] as ArrayList);
        else
          str = str + key + " : " + hashtable[(object) key].ToString() + ", ";
      }
    }
    return str + " }, ";
  }

  public static string GetArrayListInfo(ArrayList arraylist)
  {
    string str = " [ ";
    foreach (int index in arraylist)
    {
      if (arraylist[index] != null)
      {
        if (arraylist[index] is Hashtable)
          str = str + (object) index + " : " + DebugMenu.GetHashtableInfo(arraylist[index] as Hashtable);
        else if (arraylist[index] is ArrayList)
          str = str + (object) index + " : " + DebugMenu.GetArrayListInfo(arraylist[index] as ArrayList);
        else
          str = str + (object) index + " : " + arraylist[index].ToString();
      }
    }
    return str + " ], ";
  }

  public static void printIndex(ArrayList arr)
  {
    string str = string.Empty;
    for (int index = 0; index < arr.Count; ++index)
      str = str + "int i_" + arr[index] + " = GetIndex(header, \"" + arr[index] + "\");\n";
  }

  private void SetupLogPage()
  {
    this._log = this.gameObject.AddComponent<LogConsole>();
    this._log.enabled = false;
    this._log.width = 800f;
    this._log.height = 400f;
  }

  private void LogPageRenderer()
  {
  }

  private void AccountPageRenderer()
  {
    GUILayout.Label("Current Account:" + AccountManager.Instance.AccountId);
    GUILayout.BeginHorizontal();
    GUILayout.Label("Account Id:");
    this._accountId = GUILayout.TextArea(this._accountId);
    GUILayout.EndHorizontal();
    if (!GUILayout.Button("Change And Restart"))
      return;
    AccountManager.Instance.ChangeAccount(this._accountId);
  }

  private class EquipmentInfo
  {
    public string ID;
    public string type;

    public EquipmentInfo()
    {
      this.ID = this.type = (string) null;
    }

    public EquipmentInfo(string tmpID, string tmpType, string tmpQuality)
    {
      this.ID = tmpID;
      this.type = tmpType;
    }
  }
}
