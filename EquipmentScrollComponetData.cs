﻿// Decompiled with JetBrains decompiler
// Type: EquipmentScrollComponetData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class EquipmentScrollComponetData : IComponentData
{
  public ConfigEquipmentScrollInfo equipmentScrollInfo;
  public ItemStaticInfo ItemInfo;
  public BagType bagType;

  public EquipmentScrollComponetData(ConfigEquipmentScrollInfo equipmentScrollInfo, BagType bagType)
  {
    this.equipmentScrollInfo = equipmentScrollInfo;
    this.ItemInfo = ConfigManager.inst.DB_Items.GetItem(equipmentScrollInfo.itemID);
    this.bagType = bagType;
  }
}
