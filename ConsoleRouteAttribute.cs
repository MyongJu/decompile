﻿// Decompiled with JetBrains decompiler
// Type: ConsoleRouteAttribute
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Net;
using System.Text.RegularExpressions;

[AttributeUsage(AttributeTargets.Method)]
public class ConsoleRouteAttribute : Attribute
{
  public Regex m_route;
  public Regex m_methods;
  public ConsoleRouteAttribute.Callback m_callback;

  public ConsoleRouteAttribute(string route, string methods = "(GET|HEAD)")
  {
    this.m_route = new Regex(route, RegexOptions.IgnoreCase);
    if (methods == null)
      return;
    this.m_methods = new Regex(methods);
  }

  public delegate bool CallbackSimple(HttpListenerContext context);

  public delegate bool Callback(HttpListenerContext context, Match match);
}
