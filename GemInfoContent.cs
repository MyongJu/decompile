﻿// Decompiled with JetBrains decompiler
// Type: GemInfoContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class GemInfoContent : MonoBehaviour
{
  public Icon icon;
  public UISlider progressBar;
  public UIButton btnPlus;
  public UILabel curLvTemplate;
  public UISprite line;
  public UILabel nextLvlTemplate;
  public UILabel lblExp;
  public UILabel lblGemLevel;
  public GameObject benefitsGo;
  public UILabel lblExpGemDesc;
  public UITable table;
  private EquipmentGemComponent gemComponent;
  public GemData gemData;
  public ConfigEquipmentGemInfo gemConfig;
  public ItemStaticInfo gemItemConfig;
  private int _itemId;

  public void showInfo(EquipmentGemComponent gemComponent)
  {
    this.gemComponent = gemComponent;
    if ((Object) gemComponent == (Object) null)
      return;
    this.gemData = gemComponent.gemDataWrapper.GemData;
    if (this.gemData == null)
      return;
    this.gemConfig = ConfigManager.inst.DB_EquipmentGem.GetData(this.gemData.ConfigId);
    if (this.gemConfig == null)
      return;
    this.gemItemConfig = ConfigManager.inst.DB_Items.GetItem(this.gemConfig.itemID);
    if (this.gemItemConfig == null)
      return;
    this.icon.FeedData((IComponentData) new IconData(this.gemItemConfig.ImagePath, new string[2]
    {
      this.gemItemConfig.LocName,
      this.gemItemConfig.LocDescription
    })
    {
      Data = (object) this.gemItemConfig.internalId
    });
    this.lblGemLevel.text = this.gemData.Level.ToString();
    bool flag = GemManager.Instance.IsGemMaxLevel(this.gemData.ConfigId);
    bool state = GemManager.Instance.IsExpGem(this.gemData);
    float num = 0.0f;
    if (state)
    {
      num = 1f;
      this.lblExp.text = this.gemConfig.expContain.ToString();
      this.btnPlus.gameObject.SetActive(false);
    }
    else if (flag)
    {
      num = 1f;
      this.lblExp.text = ScriptLocalization.Get("id_uppercase_maxed", true);
      this.btnPlus.gameObject.SetActive(false);
    }
    else
    {
      if (this.gemConfig.expReqToNext != 0L)
        num = (float) this.gemData.Exp / (float) this.gemConfig.expReqToNext;
      this.lblExp.text = string.Format("{0}/{1}", (object) this.gemData.Exp, (object) this.gemConfig.expReqToNext);
      this.btnPlus.gameObject.SetActive(true);
    }
    this.progressBar.value = Mathf.Clamp01(num);
    NGUITools.SetActive(this.benefitsGo, !state);
    NGUITools.SetActive(this.lblExpGemDesc.gameObject, state);
    if (state)
      this.lblExpGemDesc.text = this.gemItemConfig.LocDescription;
    else
      this.SetupBenefits();
  }

  private void SetupBenefits()
  {
    UIUtils.ClearTable(this.table);
    ConfigEquipmentGemInfo nextLevelGemConfig = GemManager.Instance.GetNextLevelGemConfig(this.gemData.ConfigId);
    if (nextLevelGemConfig != null)
    {
      this.AddBenefitLabels(this.gemConfig.benefits.GetBenefitsPairs(), this.curLvTemplate, "id_current_level");
      this.AddLine();
      this.AddBenefitLabels(nextLevelGemConfig.benefits.GetBenefitsPairs(), this.nextLvlTemplate, "id_next_level");
    }
    else
    {
      this.AddBenefitLabels(this.gemConfig.benefits.GetBenefitsPairs(), this.curLvTemplate, "id_current_level");
      this.AddLine();
      this.AddReachMaxHint();
    }
    this.table.Reposition();
  }

  private void AddBenefitLabels(List<Benefits.BenefitValuePair> benefits, UILabel template, string descId)
  {
    int count = benefits.Count;
    int num = 0;
    for (int index = count + 1; num < index; ++num)
    {
      UILabel component = NGUITools.AddChild(this.table.gameObject, template.gameObject).GetComponent<UILabel>();
      NGUITools.SetActive(component.gameObject, true);
      if (num == 0)
      {
        component.text = ScriptLocalization.Get(descId, true);
      }
      else
      {
        Benefits.BenefitValuePair benefit = benefits[num - 1];
        component.text = string.Format("{0}:{1}", (object) benefit.propertyDefinition.Name, (object) benefit.propertyDefinition.ConvertToDisplayString((double) benefit.value, true, true));
      }
    }
  }

  private void AddLine()
  {
    NGUITools.SetActive(NGUITools.AddChild(this.table.gameObject, this.line.gameObject), true);
  }

  private void AddReachMaxHint()
  {
    GameObject go = NGUITools.AddChild(this.table.gameObject, this.nextLvlTemplate.gameObject);
    NGUITools.SetActive(go, true);
    go.GetComponent<UILabel>().text = ScriptLocalization.Get("id_uppercase_maxed", true);
  }
}
