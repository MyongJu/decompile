﻿// Decompiled with JetBrains decompiler
// Type: MissileSystemManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class MissileSystemManager : MonoBehaviour
{
  private Dictionary<string, string> _assetMap = new Dictionary<string, string>();
  private Dictionary<string, MissileSystem> _activeMap = new Dictionary<string, MissileSystem>();
  public const string RendererLayerName = "Missiles";
  private static MissileSystemManager _instance;
  public static int RendererLayer;

  public static MissileSystemManager Instance
  {
    get
    {
      if ((Object) MissileSystemManager._instance == (Object) null)
      {
        GameObject gameObject = new GameObject();
        gameObject.hideFlags = HideFlags.DontSave;
        gameObject.name = nameof (MissileSystemManager);
        MissileSystemManager._instance = gameObject.AddComponent<MissileSystemManager>();
      }
      return MissileSystemManager._instance;
    }
  }

  private void Awake()
  {
    MissileSystemManager._instance = this;
    MissileSystemManager.RendererLayer = LayerMask.NameToLayer("Missiles");
  }

  private void OnDestroy()
  {
    this.UnloadAll();
    MissileSystemManager._instance = (MissileSystemManager) null;
  }

  private void OnApplicationQuit()
  {
    Object.DestroyImmediate((Object) this.gameObject);
  }

  public bool RegisterMissileSystem(string id, string path)
  {
    if (!this._assetMap.ContainsKey(id))
      this._assetMap.Add(id, path);
    else
      this._assetMap[id] = path;
    return true;
  }

  public bool UnRegisterMissileSystem(string id)
  {
    return this._assetMap.Remove(id);
  }

  public MissileSystem CreateMissileSystem(string id)
  {
    MissileSystem missileSystem = (MissileSystem) null;
    string fullname;
    if (this._assetMap.TryGetValue(id, out fullname))
    {
      GameObject original = AssetManager.Instance.HandyLoad(fullname, typeof (GameObject)) as GameObject;
      if ((Object) original != (Object) null)
      {
        GameObject gameObject = Object.Instantiate<GameObject>(original);
        gameObject.transform.parent = this.transform;
        missileSystem = gameObject.GetComponent<MissileSystem>();
      }
    }
    return missileSystem;
  }

  public MissileSystem GetMissileSystem(string id)
  {
    MissileSystem missileSystem = (MissileSystem) null;
    if (!this._activeMap.TryGetValue(id, out missileSystem))
    {
      missileSystem = this.CreateMissileSystem(id);
      if ((Object) missileSystem != (Object) null)
        this._activeMap.Add(id, missileSystem);
    }
    return missileSystem;
  }

  public bool UnloadMissileSystem(string id)
  {
    MissileSystem missileSystem = (MissileSystem) null;
    if (this._activeMap.TryGetValue(id, out missileSystem))
      return false;
    missileSystem.Dispose();
    return this._activeMap.Remove(id);
  }

  public void ClearAllMissiles()
  {
    using (Dictionary<string, MissileSystem>.ValueCollection.Enumerator enumerator = this._activeMap.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.ClearMissiles();
    }
  }

  public void UnloadAll()
  {
    using (Dictionary<string, MissileSystem>.ValueCollection.Enumerator enumerator = this._activeMap.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Dispose();
    }
    this._activeMap.Clear();
  }
}
