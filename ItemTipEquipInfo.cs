﻿// Decompiled with JetBrains decompiler
// Type: ItemTipEquipInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class ItemTipEquipInfo : MonoBehaviour
{
  private BagType bagType = BagType.DragonKnight;
  private int _itemId;
  public EquipmentComponent equipComponent;
  private long _equipmentId;
  public UILabel desc;
  private long _userId;
  private int _enhanced;

  public void SetData(int itemId, long equipmentId = 0, long userId = 0, int enhanced = 0)
  {
    this._itemId = itemId;
    this._equipmentId = equipmentId;
    this._userId = userId;
    this._enhanced = enhanced;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._itemId);
    if (itemStaticInfo != null)
    {
      if (itemStaticInfo.Type == ItemBag.ItemType.equipment)
        this.bagType = BagType.Hero;
      this.desc.text = itemStaticInfo.LocDescription;
    }
    this.DrawEquipment();
  }

  public int GetEquipSuitID()
  {
    ConfigEquipmentMainInfo byItemInternalId = ConfigManager.inst.GetEquipmentMain(this.bagType).GetDataByItemInternalId(this._itemId);
    if (byItemInternalId != null && byItemInternalId.suitGroup != 0)
      return byItemInternalId.suitGroup;
    return 0;
  }

  public ConfigEquipmentMainInfo GetEquipmentInfo()
  {
    return ConfigManager.inst.GetEquipmentMain(this.bagType).GetDataByItemInternalId(this._itemId);
  }

  public InventoryItemData GetInventoryItemData()
  {
    ConfigManager.inst.GetEquipmentMain(this.bagType).GetDataByItemInternalId(this._itemId);
    return this.Inventory;
  }

  public bool HadGemSlots()
  {
    ConfigEquipmentMainInfo byItemInternalId = ConfigManager.inst.GetEquipmentMain(this.bagType).GetDataByItemInternalId(this._itemId);
    int num1 = 0;
    int num2 = 0;
    if (this.Inventory != null)
      num2 = this.Inventory.enhanced;
    if (this._enhanced > 0)
      num2 = this._enhanced;
    if (byItemInternalId != null)
    {
      List<GemSlotConditions.Condition> conditionList = byItemInternalId.GemSlotConditions.ConditionList;
      for (int index = 0; index < conditionList.Count && conditionList[index].requiredLv <= num2; ++index)
        ++num1;
    }
    return num1 > 0;
  }

  private void DrawEquipment()
  {
    ConfigEquipmentMainInfo byItemInternalId = ConfigManager.inst.GetEquipmentMain(this.bagType).GetDataByItemInternalId(this._itemId);
    if (this.Inventory == null)
      this.equipComponent.FeedData((IComponentData) new EquipmentComponentData(byItemInternalId.internalId, this._enhanced, this.bagType, 0L));
    else
      this.equipComponent.FeedData((IComponentData) new EquipmentComponentData(byItemInternalId.internalId, this.Inventory.enhanced, this.bagType, this.Inventory.itemId));
  }

  private InventoryItemData Inventory
  {
    get
    {
      long uid = this._userId;
      if (uid == 0L)
        uid = PlayerData.inst.hostPlayer.uid;
      return DBManager.inst.GetInventory(this.bagType).GetByItemID(uid, this._equipmentId);
    }
  }

  public Benefits GetBenefit()
  {
    ConfigEquipmentMainInfo byItemInternalId = ConfigManager.inst.GetEquipmentMain(this.bagType).GetDataByItemInternalId(this._itemId);
    int enhanceLevel = 0;
    if (this.Inventory != null)
      enhanceLevel = this.Inventory.enhanced;
    else if (this._enhanced > 0)
      enhanceLevel = this._enhanced;
    return ConfigManager.inst.GetEquipmentProperty(this.bagType).GetDataByEquipIDAndEnhanceLevel(byItemInternalId.internalId, enhanceLevel).benefits;
  }
}
