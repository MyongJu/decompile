﻿// Decompiled with JetBrains decompiler
// Type: AllianceRallyDetailPopUp
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;
using UnityEngine;

public class AllianceRallyDetailPopUp : UI.Dialog
{
  public AllianceRallyInfo info;
  public AllianceRallySlotContainer container;
  private bool needClose;
  private float closeTime;
  private long dataId;
  private bool isDestroy;

  private RallyInfoData Data { get; set; }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    this.OnShow(orgParam);
    AllianceRallyDetailPopUp.Parameter parameter = orgParam as AllianceRallyDetailPopUp.Parameter;
    if (parameter == null)
    {
      this.needClose = true;
      this.closeTime = Time.time + 0.1f;
    }
    else
    {
      this.dataId = parameter.data_id;
      this.ResetData();
    }
  }

  private void NeedToGetWarList()
  {
    if (UIManager.inst.dialogStackCount != 0)
      return;
    PlayerData.inst.allianceWarManager.LoadDatas();
  }

  private void ResetData()
  {
    this.Data = RallyInfoData.Create(this.dataId);
    if (this.Data == null)
    {
      this.needClose = true;
      this.closeTime = Time.time + 0.1f;
    }
    else
    {
      this.NeedToGetWarList();
      this.Data.OnInitCompleteHandler += new System.Action(this.HandlerDataCallBack);
      this.Data.OnPrepareFail += new System.Action(this.OnParseDataFail);
      this.Data.Refresh();
    }
  }

  private void OnDestroy()
  {
    this.isDestroy = true;
  }

  private void OnParseDataFail()
  {
    this.needClose = true;
    this.closeTime = Time.time + 0.1f;
  }

  private void HandlerDataCallBack()
  {
    this.Data.OnInitCompleteHandler -= new System.Action(this.HandlerDataCallBack);
    this.Data.OnPrepareFail -= new System.Action(this.OnParseDataFail);
    if (this.Data.IsDefense)
    {
      if (!this.Data.TargetIsAllianceBuild)
      {
        Hashtable postData = new Hashtable();
        postData[(object) "uid"] = (object) PlayerData.inst.uid;
        postData[(object) "opp_uid"] = (object) this.Data.TargetID;
        MessageHub.inst.GetPortByAction("PVP:getReinforceList").SendRequest(postData, new System.Action<bool, object>(this.OnReinforceDataFresh), true);
      }
      else
        this.RereshUI();
    }
    else
      this.RereshUI();
  }

  private void RereshUI()
  {
    if (this.isDestroy)
      return;
    this.info.SetData(this.Data);
    this.container.SetData(this.Data);
    this.AddEventHandler();
  }

  private void OnReinforceDataFresh(bool success, object result)
  {
    this.RereshUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.OnHide(orgParam);
    this.Clear();
    this.RemoveEventHandler();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void Clear()
  {
    this.info.Clear();
    this.container.Clear();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Rally.onRallyDataRemove += new System.Action<long>(this.OnRallyDataRemove);
    DBManager.inst.DB_March.onDataRemove += new System.Action<long>(this.OnRallyDataRemove);
    PlayerData.inst.allianceWarManager.OnRefreshRallyDetail += new System.Action(this.OnRereshUI);
    if (!this.Data.TargetIsAllianceBuild || !this.Data.IsDefense)
      return;
    DBManager.inst.DB_AllianceFortress.onDataChanged += new System.Action<AllianceFortressData>(this.OnAllianceBuildChange);
    DBManager.inst.DB_AllianceTemple.onDataChanged += new System.Action<AllianceTempleData>(this.OnAllianceTempleBuildChange);
  }

  private void OnAllianceBuildChange(AllianceFortressData data)
  {
    this.RereshUI();
  }

  private void OnAllianceTempleBuildChange(AllianceTempleData data)
  {
    this.RereshUI();
  }

  private void OnRereshUI()
  {
    this.Clear();
    this.RemoveEventHandler();
    this.ResetData();
  }

  private void OnRallyDataRemove(long rally_id)
  {
    if (rally_id != this.dataId)
      return;
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Rally.onRallyDataRemove -= new System.Action<long>(this.OnRallyDataRemove);
    DBManager.inst.DB_March.onDataRemove -= new System.Action<long>(this.OnRallyDataRemove);
    PlayerData.inst.allianceWarManager.OnRefreshRallyDetail -= new System.Action(this.OnRereshUI);
    if (this.Data.TargetIsAllianceBuild && this.Data.IsDefense)
    {
      DBManager.inst.DB_AllianceFortress.onDataChanged -= new System.Action<AllianceFortressData>(this.OnAllianceBuildChange);
      DBManager.inst.DB_AllianceTemple.onDataChanged -= new System.Action<AllianceTempleData>(this.OnAllianceTempleBuildChange);
    }
    if (this.Data == null)
      return;
    this.Data.OnInitCompleteHandler -= new System.Action(this.HandlerDataCallBack);
    this.Data.OnPrepareFail -= new System.Action(this.OnParseDataFail);
  }

  public void Update()
  {
    if (!this.needClose || (double) this.closeTime <= (double) Time.time)
      return;
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public long data_id;
  }
}
