﻿// Decompiled with JetBrains decompiler
// Type: ResRewardsInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ResRewardsInfo : MonoBehaviour
{
  public UISprite icon;
  public UILabel enhance;

  public void SeedData(ResRewardsInfo.Data d)
  {
    if ((Object) null != (Object) this.enhance)
      this.enhance.text = "+" + (object) d.count;
    string str = (string) null;
    switch (d.rt)
    {
      case ResRewardsInfo.ResType.Wood:
        str = "wood_icon";
        break;
      case ResRewardsInfo.ResType.Food:
        str = "food_icon";
        break;
      case ResRewardsInfo.ResType.Ore:
        str = "ore_icon";
        break;
      case ResRewardsInfo.ResType.Silver:
        str = "silver_icon";
        break;
      case ResRewardsInfo.ResType.Coin:
        str = "icon_currency_premium";
        break;
      case ResRewardsInfo.ResType.Steel:
        str = "steel_icon";
        break;
    }
    this.icon.spriteName = str;
  }

  public enum ResType
  {
    Wood,
    Food,
    Ore,
    Silver,
    Coin,
    Steel,
  }

  public class Data
  {
    public ResRewardsInfo.ResType rt;
    public int count;
  }
}
