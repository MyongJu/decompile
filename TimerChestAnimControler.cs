﻿// Decompiled with JetBrains decompiler
// Type: TimerChestAnimControler
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TimerChestAnimControler
{
  public TimerChestAnimControler.State STATE_DISPLAYING = TimerChestAnimControler.State.arrived | TimerChestAnimControler.State.leaving;
  public const int TIME_CHEST_ANIM_TIME = 10;
  public Animator animator;

  public TimerChestAnimControler.State state { get; private set; }

  public void Init(TimerChestAnimControler.State newState)
  {
    this.state = newState;
  }

  public void Left()
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.CityMode)
      return;
    this.state = TimerChestAnimControler.State.left;
  }

  public void Coming()
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.CityMode)
      return;
    this.state = TimerChestAnimControler.State.coming;
    GameEngine.Instance.StartCoroutine("Arrived");
  }

  public void Leaving()
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.CityMode)
      return;
    this.state = TimerChestAnimControler.State.leaving;
  }

  public void Arrived()
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.CityMode)
      return;
    this.state = TimerChestAnimControler.State.arrived;
  }

  public enum State
  {
    left = 1,
    coming = 2,
    arrived = 4,
    leaving = 8,
  }
}
