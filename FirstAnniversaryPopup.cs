﻿// Decompiled with JetBrains decompiler
// Type: FirstAnniversaryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class FirstAnniversaryPopup : Popup
{
  [SerializeField]
  private GameObject _itemRoot;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
  }

  public void OnCloseBtnPressed()
  {
    AnnouncementPopupController.Instance.isFirstAnnPopupClosed = true;
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    FirstAnniversaryItemSlot[] componentsInChildren = this._itemRoot.GetComponentsInChildren<FirstAnniversaryItemSlot>();
    if (componentsInChildren != null)
    {
      for (int index = 0; index < componentsInChildren.Length; ++index)
      {
        componentsInChildren[index].OnCardPressed -= new System.Action<SuperLoginMainInfo>(this.OnDailyCardPressed);
        componentsInChildren[index].OnCardPressed += new System.Action<SuperLoginMainInfo>(this.OnDailyCardPressed);
        componentsInChildren[index].UpdateUI();
      }
    }
    this.UpdateNPC();
  }

  private void UpdateNPC()
  {
    Transform transform = this.transform.Find("Content/people");
    if (!(bool) ((UnityEngine.Object) transform))
      return;
    UITexture component = transform.GetComponent<UITexture>();
    if (!(bool) ((UnityEngine.Object) component))
      return;
    Utils.SetPortrait(component, "Texture/Hero/player_portrait_9");
  }

  private void OnDailyCardPressed(SuperLoginMainInfo mainInfo)
  {
    if (mainInfo == null)
      return;
    UIManager.inst.OpenPopup("SuperLogin/FirstAnniversaryRewardPopup", (Popup.PopupParameter) new FirstAnniversaryRewardPopup.Parameter()
    {
      mainInfo = mainInfo,
      onGiftReceived = new System.Action<int>(this.OnGiftClaimed)
    });
  }

  private void OnGiftClaimed(int day)
  {
    this.UpdateUI();
  }
}
