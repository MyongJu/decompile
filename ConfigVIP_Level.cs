﻿// Decompiled with JetBrains decompiler
// Type: ConfigVIP_Level
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigVIP_Level
{
  public Dictionary<int, VIP_Level_Info> datas = new Dictionary<int, VIP_Level_Info>();
  public Dictionary<int, VIP_Level_Info> levelDatas = new Dictionary<int, VIP_Level_Info>();
  private const int benefitCount = 21;
  private const int privilegeCount = 10;
  private List<VIP_Level_Info> _vipLevels;

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exist", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exist", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "req_id_1");
        int index3 = ConfigManager.GetIndex(inHeader, "req_value_1");
        int[] numArray1 = new int[21];
        int[] numArray2 = new int[21];
        for (int index4 = 0; index4 < 21; ++index4)
        {
          numArray1[index4] = ConfigManager.GetIndex(inHeader, string.Format("benefit_id_{0}", (object) (index4 + 1)));
          numArray2[index4] = ConfigManager.GetIndex(inHeader, string.Format("benefit_value_{0}", (object) (index4 + 1)));
        }
        int[] numArray3 = new int[10];
        for (int index4 = 0; index4 < 10; ++index4)
          numArray3[index4] = ConfigManager.GetIndex(inHeader, string.Format("vip_right_{0}", (object) (index4 + 1)));
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          int result = 0;
          if (int.TryParse(key.ToString(), out result))
          {
            ArrayList arr = hashtable[key] as ArrayList;
            if (arr != null)
            {
              VIP_Level_Info data = new VIP_Level_Info(result, ConfigManager.GetString(arr, index1), ConfigManager.GetString(arr, index2), ConfigManager.GetInt(arr, index3));
              for (int index4 = 0; index4 < 21; ++index4)
              {
                int propertyInternalID = ConfigManager.GetInt(arr, numArray1[index4]);
                float modifier = ConfigManager.GetFloat(arr, numArray2[index4]);
                if (propertyInternalID > 0)
                  data.Effects.Add(new Effect(propertyInternalID, 0, modifier, Effect.SourceType.VIP, (long) data.InternalID));
              }
              for (int index4 = 0; index4 < 10; ++index4)
              {
                int privilegeInternalID = ConfigManager.GetInt(arr, numArray3[index4]);
                if (privilegeInternalID > 0)
                  data.Effects.Add(new Effect(0, privilegeInternalID, 1f, Effect.SourceType.VIP, (long) data.InternalID));
              }
              this.Add(data);
            }
          }
        }
      }
    }
  }

  public Dictionary<int, VIP_Level_Info>.ValueCollection Values
  {
    get
    {
      return this.datas.Values;
    }
  }

  private void Add(VIP_Level_Info data)
  {
    if (this.datas.ContainsKey(data.InternalID))
      return;
    this.datas.Add(data.InternalID, data);
    ConfigManager.inst.AddData(data.InternalID, (object) data);
    this.levelDatas.Add(data.Level, data);
  }

  public void Clear()
  {
    this.levelDatas.Clear();
    this.datas.Clear();
  }

  public VIP_Level_Info GetData(int internalId)
  {
    if (this.datas.ContainsKey(internalId))
      return this.datas[internalId];
    return (VIP_Level_Info) null;
  }

  public VIP_Level_Info VIPInfoForPoints(int vipPoints)
  {
    if (this._vipLevels == null)
    {
      this._vipLevels = new List<VIP_Level_Info>((IEnumerable<VIP_Level_Info>) this.datas.Values);
      this._vipLevels.Sort();
    }
    if (this._vipLevels.Count == 0)
      return (VIP_Level_Info) null;
    VIP_Level_Info vipLevel = this._vipLevels[0];
    for (int index = 1; index < this._vipLevels.Count && vipPoints >= this._vipLevels[index].req_value_1; ++index)
      vipLevel = this._vipLevels[index];
    return vipLevel;
  }

  public int GetVipLevelByBenefitId(string benefitId)
  {
    List<VIP_Level_Info> vipLevelInfoList = new List<VIP_Level_Info>((IEnumerable<VIP_Level_Info>) this.levelDatas.Values);
    vipLevelInfoList.Sort((Comparison<VIP_Level_Info>) ((a, b) => a.Level.CompareTo(b.Level)));
    for (int index = 0; index < vipLevelInfoList.Count; ++index)
    {
      VIP_Level_Info vipLevelInfo = vipLevelInfoList[index];
      List<Effect> effects = vipLevelInfo.Effects;
      if (effects != null)
      {
        Effect effect = effects.Find((Predicate<Effect>) (x =>
        {
          if (x.Property != null)
            return x.Property.ID == benefitId;
          return false;
        }));
        if (effect != null && (double) effect.Modifier > 0.0)
          return vipLevelInfo.Level;
      }
    }
    return -1;
  }

  public int GetVipLevelByBenefitNeed(string baseBenefit, string saveBenefit, float targetValue)
  {
    float finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(DBManager.inst.DB_Local_Benefit.Get(baseBenefit).total, saveBenefit);
    int vipLevel = PlayerData.inst.hostPlayer.VIPLevel;
    if ((double) finalData >= (double) targetValue)
      return vipLevel;
    List<VIP_Level_Info> vipLevelInfoList = new List<VIP_Level_Info>((IEnumerable<VIP_Level_Info>) this.levelDatas.Values);
    vipLevelInfoList.Sort((Comparison<VIP_Level_Info>) ((a, b) => a.Level.CompareTo(b.Level)));
    for (int index = 0; index < vipLevelInfoList.Count; ++index)
    {
      VIP_Level_Info vipLevelInfo = vipLevelInfoList[index];
      List<Effect> effects = vipLevelInfo.Effects;
      if (effects != null)
      {
        Effect effect1 = effects.Find((Predicate<Effect>) (x =>
        {
          if (x.Property != null)
            return x.Property.ID == baseBenefit;
          return false;
        }));
        if (effect1 != null)
          finalData += effect1.Modifier;
        Effect effect2 = effects.Find((Predicate<Effect>) (x =>
        {
          if (x.Property != null)
            return x.Property.ID == saveBenefit;
          return false;
        }));
        if (effect2 != null)
          finalData += effect2.Modifier;
        if ((double) finalData >= (double) targetValue)
          return vipLevelInfo.Level;
      }
    }
    return vipLevel;
  }

  public VIP_Level_Info GetMinLevelLimitForPrivilege(string privilege)
  {
    int num = int.MaxValue;
    VIP_Level_Info vipLevelInfo1 = (VIP_Level_Info) null;
    Dictionary<int, VIP_Level_Info>.Enumerator enumerator = this.levelDatas.GetEnumerator();
    while (enumerator.MoveNext())
    {
      VIP_Level_Info vipLevelInfo2 = enumerator.Current.Value;
      if (vipLevelInfo2.ContainsPrivilege(privilege) && vipLevelInfo2.Level < num)
      {
        num = vipLevelInfo2.Level;
        vipLevelInfo1 = vipLevelInfo2;
      }
    }
    return vipLevelInfo1;
  }
}
