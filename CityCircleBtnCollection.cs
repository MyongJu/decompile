﻿// Decompiled with JetBrains decompiler
// Type: CityCircleBtnCollection
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class CityCircleBtnCollection
{
  private Dictionary<string, CityCircleBtnPara> circleBtnPair = new Dictionary<string, CityCircleBtnPara>();
  private static CityCircleBtnCollection _instance;

  public static CityCircleBtnCollection Instance
  {
    get
    {
      if (CityCircleBtnCollection._instance == null)
      {
        CityCircleBtnCollection._instance = new CityCircleBtnCollection();
        if (CityCircleBtnCollection._instance == null)
          throw new ArgumentException("lost of CircleBtnCollection");
        CityCircleBtnCollection._instance.Init();
      }
      return CityCircleBtnCollection._instance;
    }
  }

  public Dictionary<string, CityCircleBtnPara> CircleBtnPair
  {
    get
    {
      return this.circleBtnPair;
    }
  }

  private void Init()
  {
    this.circleBtnPair.Add("dk_detail", new CityCircleBtnPara(AtlasType.Gui_2, "icon_dragon_knight", "id_dragon_knight_details", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnDKDetailPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("dk_forge", new CityCircleBtnPara(AtlasType.Gui_2, "icon_forging", "id_dragon_knight_equipment", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnDKBlacksmithPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("dk_armory", new CityCircleBtnPara(AtlasType.Gui_2, "icon_storage_box", "id_dragon_knight_armory", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnDKArmoryPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("dk_enhance", new CityCircleBtnPara(AtlasType.Gui_2, "icon_equipment_strengthen", "id_equipment_enhance", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnDKEnhancePressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("buildinginfo", new CityCircleBtnPara(AtlasType.Gui_2, "info", "id_info", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnInfoBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("cityinfo", new CityCircleBtnPara(AtlasType.Gui_2, "city_info", "id_boost_list", new EventDelegate((MonoBehaviour) CitadelSystem.inst, "OnCityInfoBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("buildingupgrade", new CityCircleBtnPara(AtlasType.Gui_2, "icon_building_upgrade", "id_upgrade", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnUpgradeBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("forge", new CityCircleBtnPara(AtlasType.Gui_2, "icon_forging", "id_blacksmith", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnBlacksmithBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("armory", new CityCircleBtnPara(AtlasType.Gui_2, "icon_storage_box", "id_armory", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnGamblingBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("enhance", new CityCircleBtnPara(AtlasType.Gui_2, "icon_equipment_strengthen", "id_enhance", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnEnhanceBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("research", new CityCircleBtnPara(AtlasType.Gui_2, "icon_research", "id_research", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnResearchBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("hospital", new CityCircleBtnPara(AtlasType.Gui_2, "icon_heal", "id_heal", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnHospitalBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("alliance_hospital", new CityCircleBtnPara(AtlasType.Gui_2, "icon_alliance_heal", "id_alliance_heal", new EventDelegate((MonoBehaviour) CitadelSystem.inst, "OnAllianceHospitalBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("kingdom_hospital", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_heal", "id_kingdom_hospital", new EventDelegate((MonoBehaviour) CitadelSystem.inst, "OnKingdomHospitalBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("buildingrally", new CityCircleBtnPara(AtlasType.Gui_2, "icon_rally", "id_rally", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnRallyBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("buildingreinforce", new CityCircleBtnPara(AtlasType.Gui_2, "icon_reinforce", "id_reinforce", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnReinforceBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("buildingtrade", new CityCircleBtnPara(AtlasType.Gui_2, "icon_trading", "id_market", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnTradeBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("watchtower", new CityCircleBtnPara(AtlasType.Gui_2, "icon_watchtower", "id_watch", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnWatchtowerBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("actionSpeedup", new CityCircleBtnPara(AtlasType.Gui_2, "icon_city_speedup", "march_speedup", new EventDelegate((MonoBehaviour) CityTouchCircle.Instance, "OnSpeedUpBtnClicked"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("trainTroops", new CityCircleBtnPara(AtlasType.Gui_2, "icon_troop", "id_train", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnTrainBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("buildTraps", new CityCircleBtnPara(AtlasType.Gui_2, "icon_defense", "id_build", new EventDelegate((MonoBehaviour) UIManager.inst.publicHUD, "OnTrainBtnPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("collectTroops", new CityCircleBtnPara(AtlasType.Gui_2, "icon_troop", "id_train", new EventDelegate((MonoBehaviour) CitadelSystem.inst, "OnCollectTroopsPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("recruitLegend", new CityCircleBtnPara(AtlasType.Gui_2, "icon_hero_recruit", "id_summon_hero", new EventDelegate((MonoBehaviour) CitadelSystem.inst, "OnrecruitLegendPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("ownedLegend", new CityCircleBtnPara(AtlasType.Gui_2, "icon_hero_list", "id_hero_chamber", new EventDelegate((MonoBehaviour) CitadelSystem.inst, "OnownedLegendPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("cityBoost", new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_city_boost", "id_city_buff", new EventDelegate((MonoBehaviour) CitadelSystem.inst, "OnBoostBtPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("sign", new CityCircleBtnPara(AtlasType.Gui_2, "icon_defend", "id_build", new EventDelegate((MonoBehaviour) CitadelSystem.inst, "OnSignPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("wish", new CityCircleBtnPara(AtlasType.Gui_2, "icon_wishing_well", "id_wish", new EventDelegate((MonoBehaviour) CitadelSystem.inst, "OnWishPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("decoration", new CityCircleBtnPara(AtlasType.Gui_2, "icon_castle_decoration", "id_appearance", new EventDelegate((MonoBehaviour) CitadelSystem.inst, "OnDecoration"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("dragonAttribute", new CityCircleBtnPara(AtlasType.Gui_2, "icon_dragon_stats", "id_dragon_info", new EventDelegate((MonoBehaviour) CitadelSystem.inst, "OnDragonAttributePressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("dragonSkillEnhance", new CityCircleBtnPara(AtlasType.Gui_2, "icon_dragon_skill_enhance", "id_skill_enhancer", new EventDelegate((MonoBehaviour) CitadelSystem.inst, "OnDragonSkillEnhancePressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("dragonSkill", new CityCircleBtnPara(AtlasType.Gui_2, "icon_dragon_skills", "id_dragon_skills", new EventDelegate((MonoBehaviour) CitadelSystem.inst, "OnDragonSkillPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
    this.circleBtnPair.Add("dailyActivy", new CityCircleBtnPara(AtlasType.Gui_2, "icon_daily_rewards", "id_daily_activity", new EventDelegate((MonoBehaviour) CitadelSystem.inst, "OnDailyActivyPressed"), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false));
  }

  public void Create()
  {
    this.circleBtnPair.Clear();
    this.Init();
  }
}
