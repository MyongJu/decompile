﻿// Decompiled with JetBrains decompiler
// Type: ConfigIAP_Gold
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigIAP_Gold
{
  private Dictionary<string, IAPGoldInfo> m_DataByID;
  private Dictionary<int, IAPGoldInfo> m_DataByInternalID;
  private IAPGoldInfo[] m_DataList;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<IAPGoldInfo, string>(result as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
    this.m_DataList = new IAPGoldInfo[this.m_DataByID.Count];
    this.m_DataByID.Values.CopyTo(this.m_DataList, 0);
    Array.Sort<IAPGoldInfo>(this.m_DataList, new Comparison<IAPGoldInfo>(ConfigIAP_Gold.Compare));
  }

  public IAPGoldInfo[] GetDataList()
  {
    return this.m_DataList;
  }

  private static int Compare(IAPGoldInfo x, IAPGoldInfo y)
  {
    return (int) (x.gold - y.gold);
  }

  public IAPGoldInfo Get(string id)
  {
    IAPGoldInfo iapGoldInfo;
    this.m_DataByID.TryGetValue(id, out iapGoldInfo);
    return iapGoldInfo;
  }

  public IAPGoldInfo Get(int internalId)
  {
    IAPGoldInfo iapGoldInfo;
    this.m_DataByInternalID.TryGetValue(internalId, out iapGoldInfo);
    return iapGoldInfo;
  }

  public void Clear()
  {
    this.m_DataByID = (Dictionary<string, IAPGoldInfo>) null;
    this.m_DataByInternalID = (Dictionary<int, IAPGoldInfo>) null;
    this.m_DataList = (IAPGoldInfo[]) null;
  }
}
