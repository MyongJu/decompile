﻿// Decompiled with JetBrains decompiler
// Type: WorldEncounterData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class WorldEncounterData
{
  [Config(Name = "monster_description")]
  public string monster_description = string.Empty;
  public long internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "image")]
  public string image;
  [Config(Name = "encounter_level")]
  public string level;
  [Config(Name = "type")]
  public string type;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "FBRewards")]
  public Reward FBRewards;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "Rewards")]
  public Reward Rewards;
  [Config(Name = "reward_display_name_1")]
  public string reward_display_name_1;
  [Config(Name = "reward_display_image_1")]
  public string reward_display_image_1;
  [Config(Name = "reward_display_name_2")]
  public string reward_display_name_2;
  [Config(Name = "reward_display_image_2")]
  public string reward_display_image_2;
  [Config(Name = "reward_display_name_3")]
  public string reward_display_name_3;
  [Config(Name = "reward_display_image_3")]
  public string reward_display_image_3;
  [Config(Name = "reward_display_name_4")]
  public string reward_display_name_4;
  [Config(Name = "reward_display_image_4")]
  public string reward_display_image_4;
  [Config(Name = "reward_display_name_5")]
  public string reward_display_name_5;
  [Config(Name = "reward_display_image_5")]
  public string reward_display_image_5;
  [Config(Name = "cost_energy")]
  public int cost_energy;
  [Config(Name = "recommended_power")]
  public int recommended_power;
  [Config(Name = "equipment_level")]
  public int equipment_level;

  public string Name
  {
    get
    {
      return ScriptLocalization.Get(this.name, true);
    }
  }

  public string Desc
  {
    get
    {
      return ScriptLocalization.Get(this.monster_description, true);
    }
  }

  public string MonsterImage
  {
    get
    {
      return "Texture/Monster/" + this.type;
    }
  }
}
