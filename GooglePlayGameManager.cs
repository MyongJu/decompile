﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGameManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using GooglePlayGames;
using GooglePlayGames.BasicApi.Quests;
using System.Collections.Generic;
using UnityEngine;

public class GooglePlayGameManager : MonoBehaviour
{
  [SerializeField]
  protected static bool m_functionEnabled = true;
  [SerializeField]
  protected string ID_RANK_ALLIANCE_POWER = string.Empty;
  [SerializeField]
  protected string ID_RANK_INDIVIDUAL_POWER = string.Empty;
  [SerializeField]
  protected string ID_RANK_DRAGON_LEVEL = string.Empty;
  [SerializeField]
  protected string ID_RANK_INDIVIDUAL_KILLS = string.Empty;
  [SerializeField]
  protected string ID_RANK_STRONGHOLD_LEVEL = string.Empty;
  [SerializeField]
  protected string ID_RANK_LORD_LEVEL = string.Empty;
  protected bool m_ignoreLoginResult;
  private static GooglePlayGameManager ms_instance;

  public static bool FunctionEnabled
  {
    get
    {
      if (CustomDefine.IsSQWanPackage())
        return false;
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("google_play_achievment_open");
      if (data != null && data.ValueInt <= 0 || Application.platform != RuntimePlatform.Android)
        return false;
      return GooglePlayGameManager.m_functionEnabled;
    }
  }

  public static GooglePlayGameManager Instance
  {
    get
    {
      if ((UnityEngine.Object) GooglePlayGameManager.ms_instance == (UnityEngine.Object) null)
      {
        GameObject gameObject = UnityEngine.Object.Instantiate(Resources.Load(nameof (GooglePlayGameManager))) as GameObject;
        UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
        GooglePlayGameManager.ms_instance = gameObject.GetComponent<GooglePlayGameManager>();
        GooglePlayGameManager.ms_instance.Init();
      }
      return GooglePlayGameManager.ms_instance;
    }
  }

  protected void ReportAllScores()
  {
    if (PlayerData.inst.allianceData != null)
      this.ReportScore(this.ID_RANK_ALLIANCE_POWER, PlayerData.inst.allianceData.power);
    if (PlayerData.inst.dragonData != null)
      this.ReportScore(this.ID_RANK_DRAGON_LEVEL, (long) PlayerData.inst.dragonData.Level);
    if (PlayerData.inst.userData != null)
    {
      this.ReportScore(this.ID_RANK_INDIVIDUAL_POWER, PlayerData.inst.userData.power);
      this.ReportScore(this.ID_RANK_INDIVIDUAL_KILLS, DBManager.inst.DB_UserProfileDB.Get("troops_killed"));
    }
    if (PlayerData.inst.heroData != null)
      this.ReportScore(this.ID_RANK_LORD_LEVEL, (long) PlayerData.inst.heroData.level);
    if (PlayerData.inst.playerCityData == null)
      return;
    this.ReportScore(this.ID_RANK_STRONGHOLD_LEVEL, (long) PlayerData.inst.playerCityData.level);
  }

  public void ReportAllAchievement()
  {
    using (List<AchievementData>.Enumerator enumerator = DBManager.inst.DB_Achievements.GetTotalDatas().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AchievementData current = enumerator.Current;
        if (current.State == 1)
        {
          AchievementInfo data = ConfigManager.inst.DB_Achievement.GetData(current.AchievementId);
          if (data != null && !string.IsNullOrEmpty(data.GooglePlayGameId))
            this.FinishAchievement(data.GooglePlayGameId);
        }
      }
    }
  }

  public void Init()
  {
    if (!GooglePlayGameManager.FunctionEnabled)
      return;
    PlayGamesPlatform.Activate();
  }

  public void IgnoreLoginResult()
  {
    this.m_ignoreLoginResult = true;
  }

  public void Login()
  {
    this.m_ignoreLoginResult = false;
    if (!GooglePlayGameManager.FunctionEnabled || !this.IsPlayGameInstalled() || (!this.IsPlayServicesAvailable() || this.GetGoogleAccountCount() <= 0))
      return;
    Social.localUser.Authenticate((System.Action<bool>) (result =>
    {
      if (!result || this.m_ignoreLoginResult)
        return;
      this.ReportAllScores();
    }));
  }

  public void Logout()
  {
    if (!Social.localUser.authenticated)
      return;
    PlayGamesPlatform.Instance.SignOut();
  }

  public int GetGoogleAccountCount()
  {
    AndroidJavaObject[] androidJavaObjectArray = new AndroidJavaClass("android.accounts.AccountManager").CallStatic<AndroidJavaObject>("get", new object[1]
    {
      (object) new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity").Call<AndroidJavaObject>("getBaseContext")
    }).Call<AndroidJavaObject[]>("getAccountsByType", new object[1]
    {
      (object) "com.google"
    });
    if (androidJavaObjectArray == null)
      return 0;
    return androidJavaObjectArray.Length;
  }

  public bool IsPlayGameInstalled()
  {
    AndroidJavaObject androidJavaObject1 = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity").Call<AndroidJavaObject>("getBaseContext").Call<AndroidJavaObject>("getPackageManager");
    string str = "com.google.android.play.games";
    AndroidJavaObject androidJavaObject2 = androidJavaObject1.Call<AndroidJavaObject>("getInstalledPackages", new object[1]
    {
      (object) 0
    });
    if (androidJavaObject2 != null)
    {
      int num = androidJavaObject2.Call<int>("size");
      for (int index = 0; index < num; ++index)
      {
        if (androidJavaObject2.Call<AndroidJavaObject>("get", new object[1]
        {
          (object) index
        }).Get<string>("packageName") == str)
          return true;
      }
    }
    return false;
  }

  public bool IsPlayServicesAvailable()
  {
    return new AndroidJavaClass("com.google.android.gms.common.GoogleApiAvailability").CallStatic<AndroidJavaObject>("getInstance").Call<int>("isGooglePlayServicesAvailable", new object[1]
    {
      (object) new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity")
    }) == 0;
  }

  public void FinishAchievement(string achievementId)
  {
    Social.ReportProgress(achievementId, 100.0, (System.Action<bool>) (result => {}));
  }

  public void ReportScore(string rankId, long score)
  {
    Social.ReportScore(score, rankId, (System.Action<bool>) (result => {}));
  }

  public void ShowAchievementUI()
  {
    Social.ShowAchievementsUI();
  }

  public void ShowLeaderboardUI()
  {
    Social.ShowLeaderboardUI();
  }

  public void ShowQuestUI()
  {
    PlayGamesPlatform.Instance.Quests.ShowAllQuestsUI((System.Action<QuestUiResult, IQuest, IQuestMilestone>) ((result, quest, milestone) => {}));
  }

  public void ToggleDebugUI()
  {
    GooglePlayGameManager.TestMonobehaviour component = this.gameObject.GetComponent<GooglePlayGameManager.TestMonobehaviour>();
    if ((bool) ((UnityEngine.Object) component))
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) component);
    else
      this.gameObject.AddComponent<GooglePlayGameManager.TestMonobehaviour>();
  }

  public class TestMonobehaviour : MonoBehaviour
  {
    public void OnGUI()
    {
      GUILayout.BeginVertical();
      if (GUILayout.Button("login", GUILayout.Height(100f)))
        GooglePlayGameManager.Instance.Login();
      if (GUILayout.Button("show achievement", GUILayout.Height(100f)))
        GooglePlayGameManager.Instance.ShowAchievementUI();
      if (GUILayout.Button("show leaderboard", GUILayout.Height(100f)))
        GooglePlayGameManager.Instance.ShowLeaderboardUI();
      if (GUILayout.Button("log out", GUILayout.Height(100f)))
        GooglePlayGameManager.Instance.Logout();
      GUILayout.EndVertical();
    }
  }
}
