﻿// Decompiled with JetBrains decompiler
// Type: ResultLogger
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Text;
using UnityEngine;

public class ResultLogger : Object
{
  public static void logObject(object result)
  {
    if (result.GetType() == typeof (ArrayList))
    {
      ResultLogger.logArraylist((ArrayList) result);
    }
    else
    {
      if (result.GetType() != typeof (Hashtable))
        return;
      ResultLogger.logHashtable((Hashtable) result);
    }
  }

  public static void logArraylist(ArrayList result)
  {
    StringBuilder builder = new StringBuilder();
    foreach (Hashtable hashtable in result)
    {
      ResultLogger.addHashtableToString(builder, hashtable);
      builder.Append("\n--------------------\n");
    }
  }

  public static void logHashtable(Hashtable result)
  {
    ResultLogger.addHashtableToString(new StringBuilder(), result);
  }

  public static void addHashtableToString(StringBuilder builder, Hashtable item)
  {
    foreach (DictionaryEntry dictionaryEntry in item)
    {
      if (dictionaryEntry.Value is Hashtable)
      {
        builder.AppendFormat("{0}: ", dictionaryEntry.Key);
        ResultLogger.addHashtableToString(builder, (Hashtable) dictionaryEntry.Value);
      }
      else if (dictionaryEntry.Value is ArrayList)
      {
        builder.AppendFormat("{0}: ", dictionaryEntry.Key);
        ResultLogger.addArraylistToString(builder, (ArrayList) dictionaryEntry.Value);
      }
      else
        builder.AppendFormat("{0}: {1}\n", dictionaryEntry.Key, dictionaryEntry.Value);
    }
  }

  public static void addArraylistToString(StringBuilder builder, ArrayList result)
  {
    foreach (object obj in result)
    {
      if (obj is Hashtable)
        ResultLogger.addHashtableToString(builder, (Hashtable) obj);
      else if (obj is ArrayList)
        ResultLogger.addArraylistToString(builder, (ArrayList) obj);
      builder.Append("\n--------------------\n");
    }
  }
}
