﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsMainInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class MerlinTrialsMainInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "trial_id")]
  public int trialId;
  [Config(Name = "layer_id")]
  public int layerId;
  [Config(Name = "number")]
  public int number;
  [Config(Name = "city_level")]
  public int cityLevel;
  [Config(Name = "icon_path")]
  public string icon;
  [Config(Name = "image_path")]
  public string image;
  [Config(Name = "coordinate_x")]
  public int coordinateX;
  [Config(Name = "coordinate_y")]
  public int coordinateY;
  [Config(Name = "next_id")]
  public int nextId;
  [Config(Name = "init_march_capacity")]
  public int initMarchCapacity;
  [Config(Name = "player_unit_id")]
  public int playerUnitId;
  [Config(Name = "first_reward_id")]
  public int firstRewardId;
  [Config(Name = "daily_reward_id")]
  public int dailyRewardId;
  [Config(Name = "monster_id")]
  public int monsterId;
  [Config(Name = "countdown")]
  public float countdown;
  [Config(Name = "iap_id_1")]
  public int iapId1;
  [Config(Name = "iap_id_2")]
  public int iapId2;
  [Config(Name = "iap_id_3")]
  public int iapId3;
  [Config(Name = "iap_id_4")]
  public int iapId4;
  [Config(Name = "iap_id_5")]
  public int iapId5;
  [Config(Name = "iap_id_6")]
  public int iapId6;
  [Config(Name = "iap_id_7")]
  public int iapId7;
  [Config(Name = "iap_id_8")]
  public int iapId8;
  [Config(Name = "iap_id_9")]
  public int iapId9;
  [Config(Name = "iap_id_10")]
  public int iapId10;

  public string LocName
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }

  public string IconPath
  {
    get
    {
      return this.icon;
    }
  }

  public string CityImagePath
  {
    get
    {
      return string.Format("Prefab/Tiles/tiles_stronghold_lv{0}", (object) Mathf.Min(Mathf.Max(1, this.cityLevel), MapUtils.CityPrefabNames.Length));
    }
  }

  public string MonsterImagePath
  {
    get
    {
      return this.image;
    }
  }

  public int[] IapIdArray
  {
    get
    {
      return new int[10]
      {
        this.iapId1,
        this.iapId2,
        this.iapId3,
        this.iapId4,
        this.iapId5,
        this.iapId6,
        this.iapId7,
        this.iapId8,
        this.iapId9,
        this.iapId10
      };
    }
  }

  public List<MerlinTrialsShopSpecificInfo> MerlinTrialsIapInfoList
  {
    get
    {
      List<MerlinTrialsShopSpecificInfo> shopSpecificInfoList = new List<MerlinTrialsShopSpecificInfo>();
      if (this.IapIdArray != null)
      {
        for (int index = 0; index < this.IapIdArray.Length; ++index)
        {
          MerlinTrialsShopSpecificInfo shopSpecificInfo = ConfigManager.inst.DB_MerlinTrialsShopSpecific.Get(this.IapIdArray[index]);
          if (shopSpecificInfo != null)
            shopSpecificInfoList.Add(shopSpecificInfo);
        }
      }
      return shopSpecificInfoList;
    }
  }

  public int TrialIndex
  {
    get
    {
      string s = this.id.Substring(this.id.Length - 1);
      int result = 0;
      int.TryParse(s, out result);
      return result;
    }
  }
}
