﻿// Decompiled with JetBrains decompiler
// Type: PVPFortressBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class PVPFortressBanner : SpriteBanner
{
  public TextMesh m_Status;
  public GameObject m_RootNode;

  public override void UpdateUI(TileData tile)
  {
    base.UpdateUI(tile);
    AllianceFortressData fortressData = tile.FortressData;
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(fortressData.allianceId);
    string empty = string.Empty;
    string str = string.Empty;
    if (allianceData != null)
      empty += string.Format("({0})", (object) allianceData.allianceAcronym);
    if (fortressData != null)
    {
      str = string.Format("({0})", (object) PVPFortressBanner.GetStateString(fortressData.State));
      AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(fortressData.ConfigId);
      if (buildingStaticInfo.type == 1)
      {
        empty += string.IsNullOrEmpty(fortressData.Name) ? buildingStaticInfo.LocalName : fortressData.Name;
        this.m_RootNode.transform.localPosition = new Vector3(0.0f, -60f, 0.0f);
      }
      else
      {
        empty += string.IsNullOrEmpty(fortressData.Name) ? Utils.XLAT("alliance_fortress_name") : fortressData.Name;
        this.m_RootNode.transform.localPosition = new Vector3(0.0f, -140f, 0.0f);
      }
    }
    this.Name = empty;
    this.m_Status.text = str;
  }

  public static string GetStateString(string state)
  {
    string key = state;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (PVPFortressBanner.\u003C\u003Ef__switch\u0024map9B == null)
      {
        // ISSUE: reference to a compiler-generated field
        PVPFortressBanner.\u003C\u003Ef__switch\u0024map9B = new Dictionary<string, int>(7)
        {
          {
            "unComplete",
            0
          },
          {
            "building",
            1
          },
          {
            "unDefend",
            2
          },
          {
            "defending",
            3
          },
          {
            "broken",
            4
          },
          {
            "repairing",
            5
          },
          {
            "demolishing",
            6
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (PVPFortressBanner.\u003C\u003Ef__switch\u0024map9B.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return Utils.XLAT("alliance_fort_status_unfinished");
          case 1:
            return Utils.XLAT("alliance_fort_status_under_construction");
          case 2:
            return Utils.XLAT("alliance_fort_status_not_garrisoned");
          case 3:
            return Utils.XLAT("alliance_fort_status_garrisoned");
          case 4:
            return Utils.XLAT("alliance_fort_status_damaged");
          case 5:
            return Utils.XLAT("alliance_fort_status_repair");
          case 6:
            return Utils.XLAT("alliance_fort_status_demolishing");
        }
      }
    }
    return string.Empty;
  }
}
