﻿// Decompiled with JetBrains decompiler
// Type: ConfigBuildingIdleCommon
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigBuildingIdleCommon
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, BuildingIdleCommonInfo> datas;
  private Dictionary<long, BuildingIdleCommonInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<BuildingIdleCommonInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, BuildingIdleCommonInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<long, BuildingIdleCommonInfo>) null;
  }

  public BuildingIdleCommonInfo GetBuildingIdleCommonInfo(long internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (BuildingIdleCommonInfo) null;
  }

  public BuildingIdleCommonInfo GetBuildingIdleCommonInfo(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (BuildingIdleCommonInfo) null;
  }

  public int GetBuildingIdleCommonCount()
  {
    return this.datas.Count;
  }
}
