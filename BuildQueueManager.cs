﻿// Decompiled with JetBrains decompiler
// Type: BuildQueueManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;

public class BuildQueueManager
{
  private JobEvent jobEvent = JobEvent.JOB_ONE_MORE_BUILD_QUEUE;
  private const int MAX_NORMAL_FLUX_QUEUE = 1;
  private const int MAX_ADDITION_FLUX_QUEUE = 1;
  private static BuildQueueManager instance;
  private JobHandle additonQueueHandle;
  private BuildQueueUI buildQueueUI;

  public static BuildQueueManager Instance
  {
    get
    {
      if (BuildQueueManager.instance == null)
        BuildQueueManager.instance = new BuildQueueManager();
      return BuildQueueManager.instance;
    }
  }

  public void Init()
  {
    this.additonQueueHandle = JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_ONE_MORE_BUILD_QUEUE);
    this.buildQueueUI = UIManager.inst.publicHUD.buildQueueUI;
    this.buildQueueUI.Init();
    JobManager.Instance.OnJobCreated += new System.Action<long>(this.OnJobCreate);
    JobManager.Instance.OnJobRemove += new System.Action<long>(this.OnJobRemove);
    Dictionary<long, JobHandle> jobDictByClass1 = JobManager.Instance.GetJobDictByClass(JobEvent.JOB_BUILDING_CONSTRUCTION);
    Dictionary<long, JobHandle> jobDictByClass2 = JobManager.Instance.GetJobDictByClass(JobEvent.JOB_BUILDING_DECONSTRUCTION);
    if (jobDictByClass1 != null)
    {
      using (Dictionary<long, JobHandle>.Enumerator enumerator = jobDictByClass1.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.StartJob(enumerator.Current.Value);
      }
    }
    if (jobDictByClass2 == null)
      return;
    using (Dictionary<long, JobHandle>.Enumerator enumerator = jobDictByClass2.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.StartJob(enumerator.Current.Value);
    }
  }

  public void Dispose()
  {
    JobManager.Instance.OnJobCreated -= new System.Action<long>(this.OnJobCreate);
    JobManager.Instance.OnJobRemove -= new System.Action<long>(this.OnJobRemove);
  }

  public int PossibleMaxQueue
  {
    get
    {
      return 2;
    }
  }

  public bool IsBuildQueueFull(double buildTime)
  {
    if (this.IsNormalQueueFull())
      return this.IsAdditionQueueFull(buildTime);
    return false;
  }

  public bool IsNormalQueueFull()
  {
    return CityManager.inst.GetBuildingsInNormalFlux() >= 1;
  }

  public bool IsAdditionQueueFull(double buildTime)
  {
    if (this.additonQueueHandle == null || CityManager.inst.GetBuildingsInAdditionlFlux() >= 1)
      return true;
    if (this.additonQueueHandle != null)
      return (double) this.additonQueueHandle.LeftTime() < buildTime;
    return false;
  }

  public int AllowedBuildQueue(double buildTime = 0)
  {
    return 1 + (this.additonQueueHandle == null || (double) this.additonQueueHandle.LeftTime() <= buildTime ? 0 : 1);
  }

  public bool HasAdditionQueue()
  {
    if (this.additonQueueHandle != null)
      return !this.additonQueueHandle.IsFinished();
    return false;
  }

  public void GuideJob()
  {
  }

  public void FinishJob()
  {
  }

  public void FocusJob()
  {
  }

  public void GetMoreQueueTime(BuildQueuePopup.Parameter param = null)
  {
    UIManager.inst.OpenPopup("BuildQueue/BuilderRecruitPopup", (Popup.PopupParameter) param);
  }

  public int GetRemainQueueTime()
  {
    if (this.additonQueueHandle != null)
      return this.additonQueueHandle.LeftTime();
    return 0;
  }

  private void OnJobCreate(long jobId)
  {
    JobHandle job = JobManager.Instance.GetJob(jobId);
    if (job.GetJobEvent() == JobEvent.JOB_BUILDING_CONSTRUCTION || job.GetJobEvent() == JobEvent.JOB_BUILDING_DECONSTRUCTION)
    {
      this.StartJob(job);
    }
    else
    {
      if (job.GetJobEvent() != this.jobEvent)
        return;
      this.additonQueueHandle = job;
    }
  }

  private void StartJob(JobHandle handle)
  {
    Hashtable data = handle.Data as Hashtable;
    if (data == null)
      return;
    if (data[(object) "queue"].ToString() == "free")
      this.buildQueueUI.StartNormalJob(handle);
    else
      this.buildQueueUI.StartAdditionJob(handle);
  }

  private void OnJobRemove(long jobId)
  {
  }
}
