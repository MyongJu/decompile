﻿// Decompiled with JetBrains decompiler
// Type: SQChatChecker
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public class SQChatChecker
{
  public string uid = string.Empty;
  public string dsid = string.Empty;
  public string sign = string.Empty;
  public string chat_time = string.Empty;
  public string content = string.Empty;
  public string actor_name = string.Empty;
  public int time;
  public int gid;
  public int type;

  public WWWForm ToWWWForm()
  {
    WWWForm wwwForm = new WWWForm();
    wwwForm.AddField("time", this.time);
    wwwForm.AddField("uid", this.uid);
    wwwForm.AddField("gid", this.gid);
    wwwForm.AddField("dsid", this.dsid);
    wwwForm.AddField("type", this.type);
    wwwForm.AddField("sign", this.sign);
    wwwForm.AddField("chat_time", this.chat_time);
    wwwForm.AddField("content", this.content);
    wwwForm.AddField("actor_name", this.actor_name);
    return wwwForm;
  }

  public static string GetSign(string uid, int gid, string dsid, int time, int type)
  {
    byte[] hash = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(string.Format("{0}{1}{2}{3}{4}{5}", (object) "#d1lv8UTT56L!Szy", (object) uid, (object) gid, (object) dsid, (object) time, (object) type)));
    StringBuilder stringBuilder = new StringBuilder();
    for (int index = 0; index < hash.Length; ++index)
      stringBuilder.Append(hash[index].ToString("x2"));
    string str = stringBuilder.ToString();
    string lower = str.ToLower();
    Debug.Log((object) string.Format("[37chat] key:{0} uid:{1} gid:{2} dsid:{3} time:{4} type:{5} md5:{6} sign:{7}", (object) "#d1lv8UTT56L!Szy", (object) uid, (object) gid, (object) dsid, (object) time, (object) type, (object) str, (object) lower));
    return lower;
  }
}
