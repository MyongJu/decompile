﻿// Decompiled with JetBrains decompiler
// Type: KingdomWarActivityData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;

public class KingdomWarActivityData : ActivityBaseData
{
  private List<KingdomWarActivityData.KingdomInfo> _items = new List<KingdomWarActivityData.KingdomInfo>();
  private int _nextWarTime;
  private int _isInWorldWar;
  private int _nextDivideTime;

  public bool IsInWar
  {
    get
    {
      return this._isInWorldWar != 0;
    }
  }

  public override int RemainTime
  {
    get
    {
      return this._isInWorldWar != 1 ? this._nextWarTime - NetServerTime.inst.ServerTimestamp : this._nextDivideTime - NetServerTime.inst.ServerTimestamp;
    }
  }

  public override bool IsEmpty()
  {
    return this._nextWarTime <= 0;
  }

  public override void Decode(Hashtable source)
  {
    if (source == null)
      return;
    this._items.Clear();
    string empty = string.Empty;
    if (source.ContainsKey((object) "is_in_world_war") && source[(object) "is_in_world_war"] != null)
      int.TryParse(source[(object) "is_in_world_war"].ToString(), out this._isInWorldWar);
    if (source.ContainsKey((object) "next_war_time") && source[(object) "next_war_time"] != null)
      int.TryParse(source[(object) "next_war_time"].ToString(), out this._nextWarTime);
    if (source.ContainsKey((object) "next_divide_time") && source[(object) "next_divide_time"] != null)
      int.TryParse(source[(object) "next_divide_time"].ToString(), out this._nextDivideTime);
    if (source.ContainsKey((object) "group_kingdoms") && source[(object) "group_kingdoms"] != null)
    {
      ArrayList arrayList = source[(object) "group_kingdoms"] as ArrayList;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        KingdomWarActivityData.KingdomInfo kingdomInfo = new KingdomWarActivityData.KingdomInfo();
        if (kingdomInfo.Decode(arrayList[index] as Hashtable))
        {
          kingdomInfo.NextTime = this._nextWarTime;
          this._items.Add(kingdomInfo);
        }
      }
    }
    this._items.Sort(new Comparison<KingdomWarActivityData.KingdomInfo>(this.Compare));
  }

  public List<KingdomWarActivityData.KingdomInfo> Datas
  {
    get
    {
      return this._items;
    }
  }

  private int Compare(KingdomWarActivityData.KingdomInfo a, KingdomWarActivityData.KingdomInfo b)
  {
    return a.KingdomId.CompareTo(b.KingdomId);
  }

  public struct Status
  {
    public const string UNOPEN = "unOpen";
    public const string FIGHTING = "fighting";
    public const string PROTECTED = "protected";
  }

  private struct Params
  {
    public const string NEXT_WORLD_TIME = "next_war_time";
    public const string IS_IN_WORLD_WAR = "is_in_world_war";
    public const string NEXT_DIVIDE_TIME = "next_divide_time";
    public const string GROUP_KINGDOMS = "group_kingdoms";
  }

  public class KingdomInfo
  {
    private string _kingdomName = string.Empty;
    private string _status = string.Empty;
    private string _flag = string.Empty;
    private int _mTime;
    private int _kingdomId;
    private int _protectedEndTime;
    private int _kingUid;
    private int _nextTime;
    private int _isInWar;

    public string GetContent()
    {
      string empty1 = string.Empty;
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      string Term = string.Empty;
      string empty2 = string.Empty;
      string str = Utils.FormatTime(this._nextTime - NetServerTime.inst.ServerTimestamp, true, false, true);
      if (this._status == "fighting" && this.IsInWar)
        Term = "throne_invasion_in_progress";
      if (!string.IsNullOrEmpty(Term))
        return ScriptLocalization.Get(Term, true);
      return str;
    }

    public string Status
    {
      get
      {
        return this._status;
      }
    }

    public int KingdomId
    {
      get
      {
        return this._kingdomId;
      }
    }

    public string KingdomName
    {
      get
      {
        if (!string.IsNullOrEmpty(this._kingdomName))
          return string.Format("{0} #{1}", (object) this._kingdomName, (object) this._kingdomId);
        return string.Format("{0} #{1}", (object) Utils.XLAT("id_kingdom"), (object) this._kingdomId);
      }
    }

    public int MTime
    {
      get
      {
        return this._mTime;
      }
    }

    public string GetTime()
    {
      return Utils.FormatTime(this._mTime, true, false, true);
    }

    public bool IsInWar
    {
      get
      {
        return this._isInWar > 0;
      }
    }

    public int NextTime
    {
      get
      {
        return this._nextTime;
      }
      set
      {
        this._nextTime = value;
      }
    }

    public bool Decode(Hashtable souces)
    {
      if (souces == null)
        return false;
      string empty = string.Empty;
      if (souces.ContainsKey((object) "state_change_time") && souces[(object) "state_change_time"] != null)
        int.TryParse(souces[(object) "state_change_time"].ToString(), out this._mTime);
      if (souces.ContainsKey((object) "world_id") && souces[(object) "world_id"] != null)
        int.TryParse(souces[(object) "world_id"].ToString(), out this._kingdomId);
      if (souces.ContainsKey((object) "name") && souces[(object) "name"] != null)
        this._kingdomName = souces[(object) "name"].ToString();
      if (souces.ContainsKey((object) "state") && souces[(object) "state"] != null)
        this._status = souces[(object) "state"].ToString();
      if (souces.ContainsKey((object) "flag") && souces[(object) "flag"] != null)
        this._flag = souces[(object) "flag"].ToString();
      if (souces.ContainsKey((object) "king_uid") && souces[(object) "king_uid"] != null)
        int.TryParse(souces[(object) "king_uid"].ToString(), out this._kingUid);
      if (souces.ContainsKey((object) "protected_end_time") && souces[(object) "protected_end_time"] != null)
        int.TryParse(souces[(object) "protected_end_time"].ToString(), out this._protectedEndTime);
      if (souces.ContainsKey((object) "is_in_world_war") && souces[(object) "is_in_world_war"] != null)
        int.TryParse(souces[(object) "is_in_world_war"].ToString(), out this._isInWar);
      return true;
    }

    private struct Params
    {
      public const string KINGDOM_ID = "world_id";
      public const string KINGDOM_NAME = "name";
      public const string STATUS = "state";
      public const string FLAG = "flag";
      public const string PROTECTED_END_TIME = "protected_end_time";
      public const string KING_UID = "king_uid";
      public const string MTIME = "state_change_time";
      public const string IS_IN_WORLD_WAR = "is_in_world_war";
    }
  }
}
