﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomManageContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ChatRoomManageContainer : MonoBehaviour
{
  public static ChatRoomManageContainer.Parameter param = new ChatRoomManageContainer.Parameter();
  private ChatRoomManageContainer.Data data = new ChatRoomManageContainer.Data();
  public ChatRoomMember.Title title;
  private bool _isExpanded;
  [SerializeField]
  private ChatRoomManageContainer.Panel panel;

  public string titleText
  {
    set
    {
      this.panel.baseInfoTitle.text = value;
    }
  }

  public string titleIconName
  {
    set
    {
      this.panel.baseInfoIcon.spriteName = value;
    }
  }

  public bool isExpanded
  {
    get
    {
      return this._isExpanded;
    }
    set
    {
      if (value == this._isExpanded)
        return;
      this._isExpanded = value;
      this.panel.Tween_DetailScale.Play(value);
      this.panel.Tween_ExpandRotate.Play(value);
    }
  }

  public void Setup(ChatRoomManageContainer.Parameter param)
  {
    this.data.SetData(param);
    this.data.containerTitle = this.title;
    this.Refresh();
  }

  public void Refresh()
  {
    ChatRoomData chatRoomData = DBManager.inst.DB_room.Get(this.data.roomId);
    this.isExpanded = false;
    List<ChatRoomMember> chatRoomMemberList = new List<ChatRoomMember>();
    Dictionary<long, ChatRoomMember>.ValueCollection.Enumerator enumerator = chatRoomData.members.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.uid == PlayerData.inst.uid)
        this.data.selfTitle = enumerator.Current.title;
      if (enumerator.Current.title == this.data.containerTitle)
        chatRoomMemberList.Add(enumerator.Current);
    }
    for (int count = this.panel.memberSlots.Count; count < chatRoomMemberList.Count; ++count)
    {
      GameObject gameObject = NGUITools.AddChild(this.panel.memberContainer.gameObject, this.panel.memberBarOrg.gameObject);
      gameObject.SetActive(true);
      gameObject.name = "member" + (object) (100 + count);
      gameObject.transform.localPosition = new Vector3(0.0f, (float) (count * -200 - 100), 0.0f);
      this.panel.memberSlots.Add(gameObject.GetComponent<ChatRoomManageBar>());
    }
    this.panel.baseInfoCount.text = string.Format("({0})", (object) chatRoomMemberList.Count);
    for (int index = 0; index < chatRoomMemberList.Count; ++index)
    {
      ChatRoomManageBar.param.member = chatRoomMemberList[index];
      this.panel.memberSlots[index].gameObject.SetActive(true);
      this.panel.memberSlots[index].Setup(ChatRoomManageBar.param);
    }
    for (int count = chatRoomMemberList.Count; count < this.panel.memberSlots.Count; ++count)
      this.panel.memberSlots[count].gameObject.SetActive(false);
    this.panel.memberContainer.height = chatRoomMemberList.Count * 200;
  }

  public void OnExpandClick()
  {
    this.isExpanded = !this.isExpanded;
  }

  public void Reset()
  {
    this.panel.baseInfoIcon = this.transform.Find("BaseInfoContainer/BaseInfoIcon").gameObject.GetComponent<UISprite>();
    this.panel.baseInfoTitle = this.transform.Find("BaseInfoContainer/BaseInfoTitle").gameObject.GetComponent<UILabel>();
    this.panel.baseInfoCount = this.transform.Find("BaseInfoContainer/BaseInfoCount").gameObject.GetComponent<UILabel>();
    this.panel.BT_Expand = this.transform.Find("BaseInfoContainer/Btn_expand").gameObject.GetComponent<UIButton>();
    this.panel.BT_Expand.transform.localRotation = Quaternion.identity;
    GameObject gameObject = this.transform.Find("MemberContainer").gameObject;
    this.panel.memberContainer = gameObject.GetComponent<UIWidget>();
    this.panel.memberContainer.transform.localScale = new Vector3(1f, 0.0f, 1f);
    this.panel.Tween_DetailScale = gameObject.GetComponent<TweenScale>();
    this.panel.Tween_DetailScale.updateTable = true;
    this.panel.Tween_DetailScale.from = new Vector3(1f, 0.0f, 1f);
    this.panel.Tween_DetailScale.to = new Vector3(1f, 1f, 1f);
    this.panel.Tween_DetailScale.duration = 0.1f;
    this.panel.Tween_DetailScale.enabled = false;
    this.panel.memberBarOrg = this.transform.Find("MemberContainer/MemberBarOrg").gameObject.GetComponent<ChatRoomManageBar>();
    this.panel.memberBarOrg.Reset();
    this.panel.memberBarOrg.gameObject.SetActive(false);
    this.panel.Tween_ExpandRotate = this.transform.Find("BaseInfoContainer/Btn_expand").gameObject.GetComponent<TweenRotation>();
    this.panel.Tween_ExpandRotate.from = Vector3.zero;
    this.panel.Tween_ExpandRotate.to = new Vector3(0.0f, 0.0f, -90f);
    this.panel.Tween_ExpandRotate.duration = 0.1f;
    this.panel.Tween_ExpandRotate.enabled = false;
  }

  public class Parameter
  {
    public long roomId;
  }

  public class Data
  {
    public long roomId;
    public ChatRoomMember.Title selfTitle;
    public ChatRoomMember.Title containerTitle;
    public List<ChatRoomMember> members;

    public void SetData(ChatRoomManageContainer.Parameter param)
    {
      this.roomId = param.roomId;
    }
  }

  [Serializable]
  public class Panel
  {
    public List<ChatRoomManageBar> memberSlots = new List<ChatRoomManageBar>();
    public UISprite baseInfoIcon;
    public UILabel baseInfoTitle;
    public UILabel baseInfoCount;
    public UIButton BT_Expand;
    public UIWidget memberContainer;
    public ChatRoomManageBar memberBarOrg;
    public TweenRotation Tween_ExpandRotate;
    public TweenScale Tween_DetailScale;
  }
}
