﻿// Decompiled with JetBrains decompiler
// Type: AllianceBoardMenuPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

[ExecuteInEditMode]
public class AllianceBoardMenuPopup : Popup
{
  public bool m_ResizeNow;
  public UIGrid m_Grid;
  public UISprite m_Frame;
  public UILabel m_PlayerName;
  public UIButton m_Invite;
  public UIButton m_View;
  public UIButton m_Mail;
  public UIButton m_Block;
  public UIButton m_Kick;
  private AllianceBoardMenuPopup.Parameter m_Parameter;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as AllianceBoardMenuPopup.Parameter;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.m_PlayerName.text = this.m_Parameter.payload.name;
    long uid1 = PlayerData.inst.uid;
    long uid2 = this.m_Parameter.payload.uid;
    int yourTitle = 0;
    int targetTitle = 0;
    if (PlayerData.inst.allianceData != null)
    {
      AllianceMemberData allianceMemberData1 = PlayerData.inst.allianceData.members.Get(uid2);
      if (allianceMemberData1 != null)
        targetTitle = allianceMemberData1.title;
      AllianceMemberData allianceMemberData2 = PlayerData.inst.allianceData.members.Get(uid1);
      if (allianceMemberData2 != null)
        yourTitle = allianceMemberData2.title;
    }
    this.m_Invite.gameObject.SetActive(AllianceUtilities.CanInvite(uid1, yourTitle, uid2, targetTitle));
    this.m_Block.gameObject.SetActive(false);
    this.m_Kick.gameObject.SetActive(AllianceUtilities.CanKick(uid1, yourTitle, uid2, targetTitle));
    this.Resize();
  }

  private void Update()
  {
    if (!this.m_ResizeNow)
      return;
    this.m_ResizeNow = false;
    this.Resize();
  }

  private void Resize()
  {
    this.m_Grid.Reposition();
    Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.m_Grid.transform);
    this.m_Frame.height = (int) ((double) relativeWidgetBounds.max.y - (double) relativeWidgetBounds.min.y + 200.0);
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnInviteAlliance()
  {
    this.OnClosePressed();
    UIManager.inst.OpenPopup("Alliance/AllianceInviteConfirmPopup", (Popup.PopupParameter) new AllianceInviteConfirmPopup.Parameter()
    {
      uid = this.m_Parameter.payload.uid
    });
  }

  public void OnViewLord()
  {
    this.OnClosePressed();
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
    {
      uid = this.m_Parameter.payload.uid
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnSendMail()
  {
    this.OnClosePressed();
    UIManager.inst.OpenDlg("Mail/MailComposeDlg", (UI.Dialog.DialogParameter) new MailComposeDlg.Parameter()
    {
      recipents = new List<string>()
      {
        this.m_Parameter.payload.name
      }
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnBlockLord()
  {
    this.OnClosePressed();
    MessageHub.inst.GetPortByAction("Alliance:banByAdmin").SendRequest(new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) PlayerData.inst.allianceId
      },
      {
        (object) "banned_uid",
        (object) this.m_Parameter.payload.uid
      },
      {
        (object) "operate",
        (object) "ban"
      }
    }, (System.Action<bool, object>) null, true);
  }

  public void OnKickOut()
  {
    this.OnClosePressed();
    AllianceManager.Instance.Kick(this.m_Parameter.payload.uid, (System.Action<bool, object>) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public AllianceBoardPayload payload;
  }
}
