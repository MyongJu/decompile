﻿// Decompiled with JetBrains decompiler
// Type: ArmyUnitView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using March;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class ArmyUnitView : MonoBehaviour
{
  private float _missileDelayTime = -1f;
  private float _missileLastTime = -1f;
  [SerializeField]
  public Vector3 shootTargetDeltaPos = Vector3.zero;
  public const string TROOP_ANIMATION_DIR_X = "x";
  public const string TROOP_ANIMATION_DIR_Y = "y";
  protected ArmyUnitView.Data _data;
  public Animator animator;
  public bool isGizmos;
  protected ArmyView.LODLEVEL curLod;

  public bool isHide { get; private set; }

  public void Awake()
  {
    this.isHide = true;
  }

  public virtual void Setup(ArmyFormationData inFormationData, ArmyUnitView.Param param)
  {
    this._data = new ArmyUnitView.Data();
    this._data.formationData = inFormationData;
    if (inFormationData == null)
      return;
    MarchConfig.MissileDelayTimeMap.TryGetValue(inFormationData.type, out this._missileDelayTime);
    MarchConfig.MissileLastTimeMap.TryGetValue(inFormationData.type, out this._missileLastTime);
  }

  public void Hide(float delayTime = 0.0f)
  {
    if (this.isHide)
      return;
    this.isHide = true;
    this._FadeOut(delayTime);
  }

  public void Show(Vector3 showPosition)
  {
    this._data._showPosition = showPosition;
  }

  public void Show(float delayTime = 0.0f)
  {
    if (!this.isHide)
      return;
    this.isHide = false;
    this._FadeIn(delayTime);
  }

  public virtual void Rotate(Vector3 dirV3)
  {
    this._data._dirV3 = dirV3;
    this.Rotate(MarchViewControler.CalcVector3(this._data._dirV3));
  }

  private void Rotate(Vector2 dirV2)
  {
    this._data._dirV2 = dirV2;
    this.animator.SetInteger("x", (int) this._data._dirV2.x);
    this.animator.SetInteger("y", (int) this._data._dirV2.y);
    this.transform.localScale = new Vector3(Mathf.Abs(this.transform.localScale.x) * ((double) this._data._dirV2.x <= 0.0 ? 1f : -1f), this.transform.localScale.y, this.transform.localScale.z);
  }

  protected virtual void _FadeIn(float delayTime = 0.0f)
  {
    TweenAlpha.Begin(this.gameObject, 0.3f, 1f, delayTime);
  }

  protected virtual void _FadeOut(float delayTime = 0.0f)
  {
    TweenAlpha.Begin(this.gameObject, 0.3f, 1f, delayTime);
  }

  public virtual void SetTo(Vector3 desPos)
  {
    this.transform.localPosition = desPos;
  }

  public virtual void Move()
  {
    this.MoveAnim();
    this.ResetScale(this._data.formationData.unitDefaultScale);
  }

  public virtual void Attack(Vector3 attackLocalPosition, Vector3 attackCenter)
  {
    this._data._isAttackAfterMoving = true;
    this._data._isFadeOutAfterMoving = false;
    this.ResetScale(this._data.formationData.unitDefaultScale);
    Vector3 scale = Vector3.one * this._data.formationData.unitAttackScale;
    scale.x *= (double) this._data._dirV2.x <= 0.0 ? 1f : -1f;
    TweenScale.Begin(this.gameObject, 1f, scale);
    this.ResetAlpha();
    this.LocalMoveTo(attackLocalPosition, attackCenter, 0.0f);
  }

  protected void LocalMoveTo(Vector3 inEndlocalPosition, Vector3 inActionCenterPosition, float delayTime = 0)
  {
    this.Show(0.0f);
    this._data.CalcData(this.transform.localPosition, inEndlocalPosition, inActionCenterPosition);
    this.Rotate(this._data.endLocalPosition - this._data.startLocalPosition);
    this._data.CalcTimeNow(delayTime);
    this._data._isSelfMoving = true;
  }

  protected virtual void MoveAnim()
  {
    this.animator.SetBool("attack", false);
    GameEngine.Instance.marchSystem.StopMissileShoot(this._data.missileType, this.GetHashCode());
    this._data._isAttacking = false;
  }

  protected virtual void AttackAnim()
  {
    this.animator.SetBool("attack", true);
    this._data._isAttacking = true;
    this._data.missileStartTime = Time.time + this._missileDelayTime - this._missileLastTime;
  }

  public void Update()
  {
    if (this._data == null)
      return;
    if (this._data._isSelfMoving)
    {
      if ((double) Time.time > (double) this._data.endSelfMovingTime)
        this.EndLocalMove();
      else if ((double) this._data.endSelfMovingTime > (double) this._data.startSelfMovingTime)
      {
        float num = (float) (((double) Time.time - (double) this._data.startSelfMovingTime) / ((double) this._data.endSelfMovingTime - (double) this._data.startSelfMovingTime));
        if ((double) num < (double) this._data.rotateProgress)
        {
          this.transform.localPosition = Vector3.Lerp(this._data.startLocalPosition, this._data.rotateLocalPosition, num / this._data.rotateProgress);
          this.Rotate(this._data.rotateLocalPosition - this._data.startLocalPosition);
        }
        else
        {
          this.transform.localPosition = Vector3.Lerp(this._data.rotateLocalPosition, this._data.endLocalPosition, (float) (((double) num - (double) this._data.rotateProgress) / (1.0 - (double) this._data.rotateProgress)));
          this.Rotate(this._data.endLocalPosition - this._data.rotateLocalPosition);
        }
      }
      else
        this.EndLocalMove();
    }
    if (this._data._isAttacking && this._data.missileType != MarchSystem.MissileType.none && (double) Time.time - (double) this._data.missileStartTime > (double) this._missileLastTime)
    {
      this._data.missileStartTime = Time.time;
      this.shootTargetDeltaPos.x = (float) (((double) Random.value - 0.5) * 0.200000002980232);
      this.shootTargetDeltaPos.y = (float) (((double) Random.value - 0.5) * 0.200000002980232);
      if ((Object) GameEngine.Instance != (Object) null && GameEngine.Instance.marchSystem != null)
        GameEngine.Instance.marchSystem.MissileShoot(this._data.missileType, this.transform.position, this.transform.parent.position + this._data.actionCenterLocalPosition + this.shootTargetDeltaPos, this.GetHashCode());
    }
    if (!this.isHide || (double) Vector3.Dot(this._data._showPosition - this.transform.position, this._data._dirV3) >= 0.0)
      return;
    this.isHide = false;
    this._FadeIn(0.0f);
  }

  [DebuggerHidden]
  private IEnumerator AttackDelay()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ArmyUnitView.\u003CAttackDelay\u003Ec__Iterator83()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void EndLocalMove()
  {
    if (this._data._isAttackAfterMoving)
    {
      this.ResetAlpha();
      this.ResetScale(this._data.formationData.unitAttackScale);
      this.Rotate(this.transform.parent.position + this._data.actionCenterLocalPosition - this.transform.position);
      if (!this.name.Contains("class_cavalry_106"))
        ;
      this.StartCoroutine("AttackDelay");
    }
    else
    {
      this.ResetAlpha();
      this.ResetScale(this._data.formationData.unitDefaultScale);
      this.MoveAnim();
    }
    if (this._data._isFadeOutAfterMoving)
      this._FadeOut(0.0f);
    this._data._isSelfMoving = false;
  }

  private void OnDrawGizmos()
  {
  }

  protected virtual void ResetScale(float scaleFactor)
  {
    Object.Destroy((Object) this.GetComponent<TweenScale>());
    Vector3 vector3 = Vector3.one * scaleFactor;
    vector3.x *= (double) this._data._dirV2.x <= 0.0 ? 1f : -1f;
    this.transform.localScale = vector3;
  }

  protected virtual void ResetAlpha()
  {
    Object.Destroy((Object) this.GetComponent<TweenAlpha>());
    SpriteRenderer component = this.GetComponent<SpriteRenderer>();
    Color color = component.color;
    color.a = 1f;
    component.color = color;
  }

  public virtual void SetRenderer(int order, string layer)
  {
    SpriteRenderer component = this.gameObject.GetComponent<SpriteRenderer>();
    if (!((Object) component != (Object) null))
      return;
    component.sortingOrder = order;
    component.sortingLayerName = layer;
  }

  public virtual void Dispose()
  {
    this.StopAllCoroutines();
    if (this._data != null)
      GameEngine.Instance.marchSystem.StopMissileShoot(this._data.missileType, this.GetHashCode());
    if ((Object) null != (Object) this.gameObject)
      this.gameObject.SetActive(true);
    if (!((Object) null != (Object) this.animator))
      return;
    this.animator.enabled = true;
  }

  public virtual void SetLOD(ArmyView.LODLEVEL lod)
  {
    if (this.curLod >= lod)
      return;
    this.curLod = lod;
    switch (lod)
    {
      case ArmyView.LODLEVEL.LOD1:
        if (!((Object) null != (Object) this.animator) || !this.animator.isActiveAndEnabled)
          break;
        this.animator.enabled = false;
        break;
      case ArmyView.LODLEVEL.LOD2:
        this.Dispose();
        if (!(bool) ((Object) this.gameObject) || !this.gameObject.activeSelf)
          break;
        this.gameObject.SetActive(false);
        break;
    }
  }

  public class Data
  {
    public ArmyFormationData formationData = new ArmyFormationData();
    public float missileStartTime = -0.8f;
    public const float MISSLE_LAST_STEP_TIME = 0.8f;
    private const float LOCAL_MOVE_SPEED = 0.7f;
    private const float ONE_OVER_LOCAL_MOVE_SPEED = 1.428571f;
    public bool _isSelfMoving;
    public bool _isAttacking;
    public bool _isAttackAfterMoving;
    public bool _isFadeOutAfterMoving;
    public Vector3 startLocalPosition;
    public Vector3 endLocalPosition;
    public Vector3 actionCenterLocalPosition;
    public float startSelfMovingTime;
    public float endSelfMovingTime;
    public float durationTime;
    public float rotateProgress;
    public Vector3 rotateLocalPosition;
    public Vector3 _showPosition;
    public Vector3 _dirV3;
    public Vector2 _dirV2;

    public MarchSystem.MissileType missileType
    {
      get
      {
        return this.formationData.missileType;
      }
    }

    public void CalcData(Vector3 inStartLocalPosition, Vector3 inEndLocalPosition, Vector3 inActionCenterPosition)
    {
      this.startLocalPosition = inStartLocalPosition;
      this.endLocalPosition = inEndLocalPosition;
      this.actionCenterLocalPosition = inActionCenterPosition;
      this.rotateLocalPosition = this.CalcMidpoint(inStartLocalPosition, inEndLocalPosition, (Vector2) (inActionCenterPosition - inStartLocalPosition).normalized, 0.4f);
      this.durationTime = (inEndLocalPosition - this.rotateLocalPosition).magnitude * 1.428571f;
      this.rotateProgress = (this.rotateLocalPosition - inStartLocalPosition).magnitude * 1.428571f;
      this.durationTime += this.rotateProgress;
      this.rotateProgress /= this.durationTime;
    }

    public void CalcTimeNow(float delayTime)
    {
      this.startSelfMovingTime = Time.time + delayTime;
      this.endSelfMovingTime = this.startSelfMovingTime + this.durationTime;
    }

    private Vector3 CalcMidpoint(Vector3 source, Vector3 target, Vector2 walkDir, float factor = 0.7f)
    {
      Vector2 lhs;
      lhs.x = target.x - source.x;
      lhs.y = target.y - source.y;
      return target - (Vector3) (walkDir * Vector2.Dot(lhs, walkDir)) * factor;
    }
  }

  public class Param
  {
    public Transform parentTrans;
    public int dragonIndex;
    public DragonData.Tendency dragonTendency;
    public int dragonLevel;
  }
}
