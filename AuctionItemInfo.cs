﻿// Decompiled with JetBrains decompiler
// Type: AuctionItemInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class AuctionItemInfo
{
  public int itemIndex;
  public int itemId;
  public int itemCount;
  public int itemRemainedTime;
  public int itemServerType;
  public long itemCurrentPrice;
  public long itemFeePrice;
  public long itemYourBidPrice;
  public long itemMinAddAmount;
  public int itemArtifactId;
  public bool isYourBiddingPrice;
  public AuctionHelper.AuctionType auctionType;

  public bool IsArtifact
  {
    get
    {
      return this.itemArtifactId > 0;
    }
  }
}
