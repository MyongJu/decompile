﻿// Decompiled with JetBrains decompiler
// Type: FortressTeleportUse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UI;

public class FortressTeleportUse : ItemBaseUse
{
  public FortressTeleportUse(int id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  public FortressTeleportUse(string id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  protected override void CustomUse(System.Action<bool, object> resultHandler)
  {
    if (MapUtils.IsPitWorld(PlayerData.inst.CityData.Location.K))
      UIManager.inst.toast.Show(ScriptLocalization.Get("exception_2300150", true), (System.Action) null, 4f, false);
    else if (PlayerData.inst.allianceId <= 0L)
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_not_join_alliance_use", true), (System.Action) null, 4f, false);
    }
    else
    {
      AllianceFortressData buildingConfigId = DBManager.inst.DB_AllianceFortress.GetMyFortressDataByBuildingConfigId(ConfigManager.inst.DB_AllianceBuildings.Get("alliance_fortress").internalId);
      if (buildingConfigId == null || !buildingConfigId.IsComplete)
        UIManager.inst.toast.Show(ScriptLocalization.Get("toast_alliance_fortress_incomplete", true), (System.Action) null, 4f, false);
      else
        UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
        {
          title = ScriptLocalization.Get("alliance_fortress_teleport_uppercase_title", true),
          content = ScriptLocalization.Get("alliance_fortress_teleport_confirm_use_description", true),
          yesCallback = new System.Action(FortressTeleportUse.OnUseAllianceTeleport),
          noCallback = (System.Action) null
        });
    }
  }

  public static void OnUseAllianceTeleport()
  {
    if (PlayerData.inst.userData.IsForeigner || PlayerData.inst.IsInPit)
      UIManager.inst.toast.Show(Utils.XLAT("toast_excalibur_war_fortress_not_in_same_kingdom"), (System.Action) null, 4f, false);
    else
      FortressTeleportUse.TakeAction();
  }

  private static void TakeAction()
  {
    TeleportManager.Instance.OnFortressTeleport(TeleportMode.FORTRESS_TELEPORT);
  }
}
