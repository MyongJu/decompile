﻿// Decompiled with JetBrains decompiler
// Type: TalentTreeInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class TalentTreeInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "tree")]
  public int tree;
  [Config(Name = "tier")]
  public int tier;
  [Config(Name = "row")]
  public int row;

  public string ShowInfo()
  {
    return string.Empty + "id : " + (object) this.internalId + "\n" + "ID : " + this.ID + "\n" + "tree : " + (object) this.tree + "\n" + "tier : " + (object) this.tier + "\n" + "row : " + (object) this.row + "\n";
  }
}
