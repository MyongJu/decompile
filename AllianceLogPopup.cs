﻿// Decompiled with JetBrains decompiler
// Type: AllianceLogPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class AllianceLogPopup : Popup
{
  public UILabel memberNormal;
  public UILabel memberPressed;
  public UILabel eventNormal;
  public UILabel eventPressed;
  public UILabel battleNormal;
  public UILabel battlePressed;
  public UILabel title;
  public UIButtonBar uiButtonBar;
  public AllianceLogPageRender logPageRender;

  public void OnCloseBtnClicked()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  public void UpdateUI()
  {
    this.LocalizeCategoryLabel();
    this.SetDefaultCategory();
  }

  public void LocalizeCategoryLabel()
  {
    this.title.text = Utils.XLAT("alliance_log_header");
    UILabel memberNormal = this.memberNormal;
    string str1 = Utils.XLAT("alliance_log_member");
    this.memberPressed.text = str1;
    string str2 = str1;
    memberNormal.text = str2;
    UILabel eventNormal = this.eventNormal;
    string str3 = Utils.XLAT("alliance_log_event");
    this.eventPressed.text = str3;
    string str4 = str3;
    eventNormal.text = str4;
    UILabel battleNormal = this.battleNormal;
    string str5 = Utils.XLAT("alliance_log_battle");
    this.battlePressed.text = str5;
    string str6 = str5;
    battleNormal.text = str6;
  }

  private void SetDefaultCategory()
  {
    this.uiButtonBar.SelectedIndex = 0;
  }

  private void OnCategoryBtnClicked(int index)
  {
    this.logPageRender.Show(index);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.uiButtonBar.OnSelectedHandler += new System.Action<int>(this.OnCategoryBtnClicked);
    AllianceLogManager.Instance.InitCommonData();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.uiButtonBar.OnSelectedHandler -= new System.Action<int>(this.OnCategoryBtnClicked);
    this.logPageRender.Hide();
    AllianceLogManager.Instance.ClearCommonData();
  }
}
