﻿// Decompiled with JetBrains decompiler
// Type: ConfigAllianceDonateRewards
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class ConfigAllianceDonateRewards
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, AllianceDonateRewardsInfo> datas;
  private Dictionary<int, AllianceDonateRewardsInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<AllianceDonateRewardsInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public AllianceDonateRewardsInfo GetAllianceDonateRewardsInfo(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (AllianceDonateRewardsInfo) null;
  }

  public AllianceDonateRewardsInfo GetAllianceDonateRewardsInfo(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AllianceDonateRewardsInfo) null;
  }

  public string GetRewardsContent(int rank)
  {
    StringBuilder stringBuilder = new StringBuilder();
    AllianceDonateRewardsInfo donateRewardsInfo = this.GetAllianceDonateRewardsInfo("alliance_reward_" + rank.ToString());
    if (donateRewardsInfo.cost.food != 0)
      stringBuilder.Append(donateRewardsInfo.cost.food.ToString() + " " + ScriptLocalization.Get("id_food", true) + ", ");
    if (donateRewardsInfo.cost.wood != 0)
      stringBuilder.Append(donateRewardsInfo.cost.wood.ToString() + " " + ScriptLocalization.Get("id_wood", true) + ", ");
    if (donateRewardsInfo.honor != 0L)
      stringBuilder.Append(donateRewardsInfo.honor.ToString() + " " + ScriptLocalization.Get("id_honor", true) + ", ");
    if (donateRewardsInfo.fund != 0L)
      stringBuilder.Append(donateRewardsInfo.fund.ToString() + " " + ScriptLocalization.Get("id_fund", true) + ", ");
    if (donateRewardsInfo.Rewards.rewards.Count > 0)
    {
      using (Dictionary<string, int>.Enumerator enumerator = donateRewardsInfo.Rewards.rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int internalId = int.Parse(current.Key);
          int num = current.Value;
          string str = ScriptLocalization.Get(ConfigManager.inst.DB_Items.GetItem(internalId).Loc_Name_Id, true);
          stringBuilder.Append(num.ToString() + " " + str);
          stringBuilder.Append(", ");
        }
      }
    }
    stringBuilder.Remove(stringBuilder.Length - 2, 2);
    return stringBuilder.ToString();
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, AllianceDonateRewardsInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, AllianceDonateRewardsInfo>) null;
  }
}
