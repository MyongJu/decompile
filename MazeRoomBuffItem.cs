﻿// Decompiled with JetBrains decompiler
// Type: MazeRoomBuffItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using UnityEngine;

public class MazeRoomBuffItem : MonoBehaviour, IRoomEventProcessor
{
  [SerializeField]
  protected GameObject _rootTalk;
  [SerializeField]
  protected UILabel _labelTalk;
  [SerializeField]
  protected UILabel _labelEffectTip;
  protected GameObject _gameObjectNpc;
  protected DungeonBuffStaticInfo _dungeonBuffInfo;
  protected Room _currentRoom;
  protected bool _buffAdded;
  public static int test_number;

  public void SetEnabled(bool enabled)
  {
    this.gameObject.SetActive(enabled);
    this._rootTalk.gameObject.SetActive(false);
    this._buffAdded = false;
  }

  public void SetBuffInfo(int buffId)
  {
    this._dungeonBuffInfo = ConfigManager.inst.DB_DungeonBuff.GetData(buffId);
    if ((bool) ((UnityEngine.Object) this._gameObjectNpc))
    {
      UnityEngine.Object.DestroyObject((UnityEngine.Object) this._gameObjectNpc);
      this._gameObjectNpc = (GameObject) null;
    }
    if (this._dungeonBuffInfo != null)
    {
      string imageFullPath = this._dungeonBuffInfo.ImageFullPath;
      UnityEngine.Object original = AssetManager.Instance.Load(imageFullPath, (System.Type) null);
      if ((bool) original)
      {
        this._gameObjectNpc = UnityEngine.Object.Instantiate(original) as GameObject;
        this._gameObjectNpc.transform.SetParent(this.transform);
        this._gameObjectNpc.transform.localScale = Vector3.one;
        this._gameObjectNpc.transform.localPosition = Vector3.zero;
        UIButton component = this._gameObjectNpc.GetComponent<UIButton>();
        if ((bool) ((UnityEngine.Object) component))
          component.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnTalkBubbleClicked)));
        AssetManager.Instance.UnLoadAsset(imageFullPath, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      }
      else
        D.error((object) string.Format("can not find npc prefab: {0}", (object) imageFullPath));
    }
    else
      D.error((object) string.Format("can not find buff template data , id = {0}", (object) this._dungeonBuffInfo.ID));
  }

  public void OnTalkBubbleClicked()
  {
    if (this._buffAdded)
      return;
    RequestManager.inst.SendRequest("DragonKnight:getDungeonBuff", (Hashtable) null, (System.Action<bool, object>) ((result, data) =>
    {
      if (!result)
        return;
      this._buffAdded = true;
      PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[this._dungeonBuffInfo.Benefit];
      if (dbProperty != null)
        DragonKnightSystem.Instance.Controller.RoomHud.PlayBuffTip(dbProperty.ImagePath, dbProperty.Name);
      this.PlayCompleteTalkAnimation();
    }), true);
  }

  protected void PlayStartTalkAnimation()
  {
    if (!string.IsNullOrEmpty(this._dungeonBuffInfo.Dialog1))
    {
      this._labelTalk.text = ScriptLocalization.Get(this._dungeonBuffInfo.Dialog1, true);
      this._rootTalk.SetActive(true);
    }
    else
      this._rootTalk.SetActive(false);
  }

  protected void OnStartTalkAnimationCompleted()
  {
  }

  protected void PlayCompleteTalkAnimation()
  {
    if (!string.IsNullOrEmpty(this._dungeonBuffInfo.Dialog2))
    {
      this._labelTalk.text = ScriptLocalization.Get(this._dungeonBuffInfo.Dialog2, true);
      this._rootTalk.SetActive(true);
    }
    else
      this._rootTalk.SetActive(false);
    this.OnCompleteTalkAnimationCompleted();
  }

  protected void OnCompleteTalkAnimationCompleted()
  {
    this._currentRoom.AllBuff.RemoveAt(0);
    DragonKnightSystem.Instance.RoomManager.CurrentRoomEventCompelted(false);
  }

  public bool IsSkipAble()
  {
    return true;
  }

  public void ProcessRoomEvent(Room room)
  {
    this._currentRoom = room;
    if (room.AllBuff.Count > 0)
    {
      this.SetEnabled(true);
      this.SetBuffInfo(room.AllBuff[0]);
      this.PlayStartTalkAnimation();
    }
    else
      D.error((object) "there is no buff in this room");
  }
}
