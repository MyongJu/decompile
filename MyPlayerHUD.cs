﻿// Decompiled with JetBrains decompiler
// Type: MyPlayerHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class MyPlayerHUD : MonoBehaviour, IRoundPlayerHud
{
  public float m_Delay = 0.1f;
  public UITexture m_Portrait;
  public UIProgressBar m_Health;
  public UILabel m_HealthNumber;
  public UISprite m_HealthFront;
  public UISprite m_HealthBack;
  public UIProgressBar m_Mana;
  public UILabel m_ManaNumber;
  public UISprite m_ManaFront;
  public UISprite m_ManaBack;
  public NumberController Decrease;
  public NumberController Increase;
  public MyPlayerBuffBar BuffBar;

  public void Initialize()
  {
    this.BuffBar.HideAll();
  }

  public void Reset()
  {
    this.BuffBar.HideAll();
    int attibuteValueInDungen1 = AttributeCalcHelper.Instance.GetCurrentAttibuteValueInDungen(DragonKnightAttibute.Type.Health, 0L);
    int battleHealthAttribute = AttributeCalcHelper.Instance.DragonKnightOutBattleHealthAttribute;
    this.SetHealthNumber(attibuteValueInDungen1, battleHealthAttribute);
    this.SetHealthProgress((float) attibuteValueInDungen1 / (float) battleHealthAttribute);
    int attibuteValueInDungen2 = AttributeCalcHelper.Instance.GetCurrentAttibuteValueInDungen(DragonKnightAttibute.Type.MP, 0L);
    int manaMax = AttributeCalcHelper.Instance.CalcDragonKnightAttribute(DragonKnightAttibute.Type.MP, (DragonKnightData) null);
    this.SetManaNumber(attibuteValueInDungen2, manaMax);
    this.SetManaProgress((float) attibuteValueInDungen2 / (float) manaMax);
    this.UpdatePortait();
  }

  private void UpdatePortait()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Portrait, "Texture/DragonKnight/Portrait/" + (PlayerData.inst.dragonKnightData.Gender != DragonKnightGender.Male ? "dk_portrait_female" : "dk_portrait_male"), (System.Action<bool>) null, false, true, string.Empty);
  }

  public void SetHealthProgress(float progress)
  {
    this.m_Health.value = progress;
    TweenWidth.Begin((UIWidget) this.m_HealthBack, 0.45f, (int) ((double) this.m_HealthFront.width * (double) this.m_Health.value)).delay = this.m_Delay;
  }

  public void SetHealthNumber(int health, int healthMax)
  {
    this.m_HealthNumber.text = string.Format("{0}/{1}", (object) health, (object) healthMax);
  }

  public void SetManaProgress(float progress)
  {
    this.m_Mana.value = progress;
    TweenWidth.Begin((UIWidget) this.m_ManaBack, 0.45f, (int) ((double) this.m_ManaFront.width * (double) this.m_Mana.value)).delay = this.m_Delay;
  }

  public void SetManaNumber(int mana, int manaMax)
  {
    this.m_ManaNumber.text = string.Format("{0}/{1}", (object) mana, (object) manaMax);
  }

  public void SetDecreaseHealth(int number, Color color)
  {
    this.Decrease.Play(number.ToString(), color);
  }

  public void SetIncreaseHealth(int number, Color color)
  {
    this.Increase.Play(number.ToString(), color);
  }

  public void SetBuffs(List<BattleBuff> buffs)
  {
    this.BuffBar.SetBuffs(buffs);
  }

  public void DecreaseCDTime()
  {
    DragonKnightSystem.Instance.Controller.BattleHud.DecreaseCDTime();
  }
}
