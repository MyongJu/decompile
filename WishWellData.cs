﻿// Decompiled with JetBrains decompiler
// Type: WishWellData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class WishWellData
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "level")]
  public int Level;
  [Config(Name = "wood")]
  public int Wood;
  [Config(Name = "food")]
  public int Food;
  [Config(Name = "ore")]
  public int Ore;
  [Config(Name = "silver")]
  public int Silver;
  [Config(Name = "steel")]
  public int Steel;
  [Config(Name = "free_gem")]
  public int FreeGem;
  [Config(Name = "critical_2")]
  public double Critical2;
  [Config(Name = "critical_5")]
  public double Critical5;
  [Config(Name = "critical_10")]
  public double Critical10;
}
