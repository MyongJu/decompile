﻿// Decompiled with JetBrains decompiler
// Type: FirstKillReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UI;

public class FirstKillReportPopup : BaseReportPopup
{
  private FirstKillReportPopup.FirstKillContent fkc;
  public UIScrollView sv;
  public Icon monsteroverview;
  public RewardItemComponent ric;

  private void Init()
  {
    string str1 = "K: " + (object) this.fkc.k + " X: " + (object) this.fkc.x + " Y: " + (object) this.fkc.y;
    this.time.text = Utils.FormatTimeForMail(this.mailtime);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, true, false, string.Empty);
    WorldEncounterData data = ConfigManager.inst.DB_WorldEncount.GetData((long) this.fkc.resource_id);
    string str2 = ScriptLocalization.Get("id_lv", true) + data.level + " " + data.Name;
    this.monsteroverview.FeedData((IComponentData) new IconData(data.MonsterImage, new string[3]
    {
      str2,
      str1,
      this.param.mail.GetBodyString()
    }));
    this.SummarizeRewards();
  }

  private void SummarizeRewards()
  {
    List<ItemInfo.Data> dataList = new List<ItemInfo.Data>();
    if (this.fkc.rewards != null)
    {
      this.ric.gameObject.SetActive(true);
      List<IconData> data = new List<IconData>();
      using (Dictionary<string, int>.Enumerator enumerator = this.fkc.rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int internalId = int.Parse(current.Key);
          int num = current.Value;
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
          data.Add(new IconData(itemStaticInfo.ImagePath, new string[2]
          {
            ScriptLocalization.Get(itemStaticInfo.Loc_Name_Id, true),
            "X" + num.ToString()
          })
          {
            Data = (object) internalId
          });
        }
      }
      this.ric.FeedData((IComponentData) new IconListComponentData(data));
    }
    else
      this.ric.gameObject.SetActive(false);
  }

  private void Reset()
  {
    this.sv.ResetPosition();
  }

  public void GotoMonster()
  {
    this.GoToTargetPlace(this.fkc.k, this.fkc.x, this.fkc.y);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.fkc = JsonReader.Deserialize<FirstKillReportPopup.FirstKillContent>(Utils.Object2Json(this.param.hashtable[(object) "data"]));
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }

  public class FirstKillContent
  {
    public int k;
    public int x;
    public int y;
    public int resource_id;
    public Dictionary<string, int> rewards;
  }
}
