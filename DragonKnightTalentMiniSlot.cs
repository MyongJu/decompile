﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightTalentMiniSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UnityEngine;

[RequireComponent(typeof (UIEventTrigger))]
[RequireComponent(typeof (UIDragScrollView))]
[RequireComponent(typeof (BoxCollider))]
public class DragonKnightTalentMiniSlot : MonoBehaviour
{
  private DragonKnightTalentMiniSlot.Data _data = new DragonKnightTalentMiniSlot.Data();
  public System.Action<int> onSkillClick;
  [SerializeField]
  private DragonKnightTalentMiniSlot.Panel panel;

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void Setup(int talentInternalId)
  {
    this._data.TalentActiveId = talentInternalId;
    this.FreshSlot();
    DragonKnightTalentInfo knightTalentInfo = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(this._data.TalentActiveInfo.talentId);
    if (knightTalentInfo == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.icon, knightTalentInfo.imagePath, (System.Action<bool>) null, true, false, string.Empty);
  }

  public void FreshSlot()
  {
    if (this._data.TalentActiveId == 0)
      return;
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (dragonKnightData == null)
      return;
    DragonKnightActiveTalent.Talent talent = dragonKnightData.activeTalents.GetTalent((long) this._data.TalentActiveInfo.talentId);
    DragonKnightTalentData knightTalentData = DBManager.inst.DB_DragonKnightTalent.Get(this._data.TalentActiveInfo.talentId);
    if (talent != null && knightTalentData != null)
    {
      this.panel.eventTriggle.enabled = true;
      GreyUtility.Normal(this.panel.icon.gameObject);
      if (talent != null)
      {
        if (talent.JobId != 0L)
        {
          this.panel.activeTime.gameObject.SetActive(true);
          this.panel.cdTime.gameObject.SetActive(false);
          this.panel.activeTime.text = Utils.FormatTime(JobManager.Instance.GetJob(talent.JobId).LeftTime(), false, false, true);
        }
        else
        {
          this.panel.activeTime.gameObject.SetActive(false);
          if (talent.ActiveTime > NetServerTime.inst.ServerTimestamp)
          {
            this.panel.cdTime.gameObject.SetActive(true);
            this.panel.cdTime.text = Utils.FormatTime(talent.ActiveTime - NetServerTime.inst.ServerTimestamp, false, false, true);
          }
          else
            this.panel.cdTime.gameObject.SetActive(false);
        }
      }
      else
      {
        this.panel.activeTime.gameObject.SetActive(false);
        this.panel.cdTime.gameObject.SetActive(false);
      }
    }
    else
    {
      this.panel.eventTriggle.enabled = false;
      this.panel.activeTime.gameObject.SetActive(false);
      this.panel.cdTime.gameObject.SetActive(false);
      GreyUtility.Grey(this.panel.icon.gameObject);
    }
    this.panel.timerBackground.gameObject.SetActive(this.panel.activeTime.gameObject.activeSelf || this.panel.cdTime.gameObject.activeSelf);
  }

  public void OnSkillClick()
  {
    if (this.onSkillClick == null)
      return;
    this.onSkillClick(this._data.TalentActiveId);
  }

  public void OnSecond(int serverTime)
  {
    this.FreshSlot();
  }

  public void Reset()
  {
    this.panel.icon = this.transform.Find("TimerPanel/Icon").gameObject.GetComponent<UITexture>();
    this.panel.timerBackground = this.transform.Find("TimerPanel/TimeBg").gameObject.transform;
    this.panel.activeTime = this.transform.Find("TimerPanel/ActiveTime").gameObject.GetComponent<UILabel>();
    this.panel.cdTime = this.transform.Find("TimerPanel/CoolDownTime").gameObject.GetComponent<UILabel>();
    this.panel.eventTriggle = this.GetComponent<UIEventTrigger>();
  }

  private class Data
  {
    private int _talentActiveId;

    public int TalentActiveId
    {
      get
      {
        return this._talentActiveId;
      }
      set
      {
        this._talentActiveId = value;
        this.TalentActiveInfo = ConfigManager.inst.DB_DragonKnightTalentActive.GetItem(this._talentActiveId);
      }
    }

    public DragonKnightTalentActiveInfo TalentActiveInfo { get; private set; }
  }

  [Serializable]
  protected class Panel
  {
    public UITexture icon;
    public Transform timerBackground;
    public UILabel activeTime;
    public UILabel cdTime;
    public UIEventTrigger eventTriggle;
  }
}
