﻿// Decompiled with JetBrains decompiler
// Type: ChatAndMailConstraint
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class ChatAndMailConstraint
{
  private static ChatAndMailConstraint _instance;
  private bool _switchState;
  private int _levelLimit;

  public static ChatAndMailConstraint Instance
  {
    get
    {
      if (ChatAndMailConstraint._instance == null)
        ChatAndMailConstraint._instance = new ChatAndMailConstraint();
      return ChatAndMailConstraint._instance;
    }
  }

  public bool SwitchState
  {
    get
    {
      return this._switchState;
    }
    set
    {
      this._switchState = value;
    }
  }

  public int LevelLimit
  {
    get
    {
      return this._levelLimit;
    }
    set
    {
      this._levelLimit = value;
    }
  }

  public bool HasBindAccount()
  {
    return AccountManager.Instance.HadBindInfo();
  }

  public void Initialize()
  {
    MessageHub.inst.GetPortByAction("mail_limit").AddEvent(new System.Action<object>(this.OnChatAndMailContraintUpdated));
  }

  public void Dispose()
  {
    MessageHub.inst.GetPortByAction("mail_limit").RemoveEvent(new System.Action<object>(this.OnChatAndMailContraintUpdated));
  }

  private void OnChatAndMailContraintUpdated(object data)
  {
    if (data == null)
      return;
    this.Decode(data as Hashtable);
  }

  private void Decode(Hashtable data)
  {
    if (data == null || !data.ContainsKey((object) "mail_constraint"))
      return;
    Hashtable hashtable = data[(object) "mail_constraint"] as Hashtable;
    if (hashtable == null)
      return;
    if (hashtable.ContainsKey((object) "enabled") && hashtable[(object) "enabled"] != null)
      this._switchState = hashtable[(object) "enabled"].ToString().Trim().ToLower() == "true";
    if (!hashtable.ContainsKey((object) "level") || hashtable[(object) "level"] == null)
      return;
    this._levelLimit = int.Parse(hashtable[(object) "level"].ToString());
  }
}
