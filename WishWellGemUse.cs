﻿// Decompiled with JetBrains decompiler
// Type: WishWellGemUse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;

public class WishWellGemUse : ItemBaseUse
{
  public WishWellGemUse(int id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  public WishWellGemUse(string id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  protected override void CustomUse(System.Action<bool, object> resultHandler)
  {
    if (ItemBag.Instance.GetItemCount(this.Info.internalId) > 1)
      UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) new ItemUseOrBuyPopup.Parameter()
      {
        item_id = this.Info.internalId,
        onUseOrBuyCallBack = new System.Action<bool, object>(this.OnHandlePayload)
      });
    else
      ItemBag.Instance.UseItem(this.Info.internalId, 1, (Hashtable) null, new System.Action<bool, object>(this.OnHandlePayload));
  }

  private void OnHandlePayload(bool ret, object data)
  {
    if (!ret)
      return;
    WishWellPayload.Instance.Decode(data);
  }
}
