﻿// Decompiled with JetBrains decompiler
// Type: EffectData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EffectData
{
  public int internalID;
  public Vector3 marchPosition;
  public Vector3 targetPosition;
  private EffectInfo info;

  public EffectData(int internalID, Vector3 sourcePosition, Vector3 targetPosition)
  {
    this.internalID = internalID;
    this.marchPosition = sourcePosition;
    this.targetPosition = targetPosition;
    this.info = ConfigManager.inst.DB_Effect.GetEffectInfoByInternalID(internalID);
  }

  public string GetEffectResourceName()
  {
    if (this.info != null)
      return this.info.fxName;
    return string.Empty;
  }

  public Vector3 GetPosition()
  {
    if (this.info.effectTarget == EffectInfo.EffectTarget.SOURCE.ToString().ToLower())
      return this.marchPosition + this.info.GetOffSet();
    return this.targetPosition + this.info.GetOffSet();
  }

  public float GetDuration()
  {
    return this.info.duration;
  }
}
