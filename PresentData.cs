﻿// Decompiled with JetBrains decompiler
// Type: PresentData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class PresentData
{
  public string ImageParentPath { get; set; }

  public int ConfigID { get; set; }

  public PresentData.DataType Type { get; set; }

  public string RewardIcon { get; set; }

  public long DataID { get; set; }

  public int Count { get; set; }

  public string Image { get; set; }

  public string Formula { get; set; }

  public string TexturePath { set; get; }

  public enum DataType
  {
    None,
    Quest,
    Achievement,
  }
}
