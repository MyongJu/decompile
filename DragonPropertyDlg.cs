﻿// Decompiled with JetBrains decompiler
// Type: DragonPropertyDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UI;
using UnityEngine;

public class DragonPropertyDlg : UI.Dialog
{
  public float dragonRotateFactor = 1f;
  public UILabel title;
  public UILabel dragonName;
  public DragonSkillContainer[] container;
  public DragonPowerItem powerItem;
  public UITexture renameIcon;
  public UITexture dragonTexture;
  public GameObject dragonRoot;
  private DragonView _dragonView;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    if (!((UnityEngine.Object) this.dragonRoot != (UnityEngine.Object) null))
      return;
    this._dragonView = new DragonView();
    this._dragonView.Mount(this.dragonRoot.transform);
    this._dragonView.SetPosition(Vector3.zero);
    this._dragonView.SetRotation(Quaternion.identity);
    this._dragonView.SetScale(new Vector3(400f, 400f, 400f));
    this._dragonView.SetRendererLayer(this.dragonRoot.layer);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.AddEventHandler();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    if (this._dragonView != null)
    {
      this._dragonView.Dispose();
      this._dragonView = (DragonView) null;
    }
    base.OnClose(orgParam);
  }

  private void UpdateUI()
  {
    this.title.text = ScriptLocalization.Get("dragon_info_title", true);
    this.dragonName.text = ScriptLocalization.Get(PlayerData.inst.dragonData.Name, true);
    this.powerItem.Refresh();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.renameIcon, Utils.GetUITextPath() + "item_dragon_rename", (System.Action<bool>) null, true, false, string.Empty);
    for (int index = 0; index < this.container.Length; ++index)
      this.container[index].Refresh();
    this.powerItem.Refresh();
    if (this._dragonView == null)
      return;
    this._dragonView.LoadAsync((SpriteViewData) DragonUtils.GetDragonViewData(PlayerData.inst.dragonData, 1), true, (System.Action<bool, UnityEngine.Object>) ((ret, asset) =>
    {
      if (!ret)
        return;
      this._dragonView.SetRendererLayer(this.dragonRoot.layer);
      this._dragonView.PlayAnimation("idle", 0.0f);
    }));
  }

  private void Clear()
  {
    this.RemoveEventHandler();
    for (int index = 0; index < this.container.Length; ++index)
      this.container[index].Clear();
    this.powerItem.Clear();
    BuilderFactory.Instance.Release((UIWidget) this.renameIcon);
  }

  public void ResetSkill()
  {
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Dragon.onDataChanged += new System.Action<long>(this.OnDataChanged);
    UICamera.onDrag += new UICamera.VectorDelegate(this.OnDragEvent);
    UICamera.onClick += new UICamera.VoidDelegate(this.OnClickEvent);
  }

  private void OnDragonDataChange()
  {
    if (this._dragonView == null)
      return;
    this._dragonView.LoadAsync((SpriteViewData) DragonUtils.GetDragonViewData(PlayerData.inst.dragonData, 1), true, (System.Action<bool, UnityEngine.Object>) ((ret, asset) =>
    {
      if (!ret)
        return;
      this._dragonView.SetRendererLayer(this.dragonRoot.layer);
      this._dragonView.PlayAnimation("idle", 0.0f);
    }));
  }

  private void OnDataChanged(long id)
  {
    if (id != PlayerData.inst.uid)
      return;
    this.UpdateUI();
  }

  private void RemoveEventHandler()
  {
    UICamera.onClick -= new UICamera.VoidDelegate(this.OnClickEvent);
    UICamera.onDrag -= new UICamera.VectorDelegate(this.OnDragEvent);
    DBManager.inst.DB_Dragon.onDataChanged -= new System.Action<long>(this.OnDataChanged);
  }

  private void OnDragEvent(GameObject go, Vector2 delta)
  {
    if ((UnityEngine.Object) go != (UnityEngine.Object) this.dragonTexture.gameObject)
      return;
    this.dragonRoot.transform.Rotate(Vector3.up, -delta.x / this.dragonRotateFactor);
  }

  private void OnClickEvent(GameObject go)
  {
    if ((UnityEngine.Object) go != (UnityEngine.Object) this.dragonTexture.gameObject || this._dragonView == null)
      ;
  }

  public void ChangeDragonName()
  {
    UIManager.inst.OpenPopup("Dragon/DragonRenamePopup", (Popup.PopupParameter) null);
  }

  public void OnDragonStatsInfoClick()
  {
    UIManager.inst.OpenPopup(DragonStatsInfoPopup.DRAGON_STATS_INFO_POPUP, (Popup.PopupParameter) new DragonStatsInfoPopup.Parameter()
    {
      dragonInfo = ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(PlayerData.inst.dragonData.Level)
    });
  }
}
