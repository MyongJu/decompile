﻿// Decompiled with JetBrains decompiler
// Type: IAPDailySuccessPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class IAPDailySuccessPopup : BaseReportPopup
{
  private List<RewardRender> rewardsrenders = new List<RewardRender>();
  private const string GOOGLE_CHANEL = "google";
  public UITable rewardsTable;
  public UITable rewardsContentTable;
  public UITable subRewardsTable;
  public UITable subscribeTable;
  public UITable contentTable;
  public UITable messageTable;
  public GameObject rewardRender;
  public UILabel messageLabel;
  public UILabel subscribeLabel;
  public UILabel subBtnLabel;
  public UIButton subscribeButton;
  private Hashtable cachedTable;
  private IAPDailySuccessPopup.IAPDailySuccessData iapdDailyRewards;
  private IAPDailySuccessPopup.IAPDailySuccessData iapDailySubRewards;

  private bool hasSubscribed
  {
    get
    {
      return this.maildata.type == MailType.MAIL_TYPE_SYSTEM_PAYMENT_DAILY_PACKAGE_WITH_SUBSCRIPTION_SUCCESS;
    }
  }

  private void UpdateUI()
  {
    this.UpdateMessage();
    this.UpdateReward();
  }

  private void UpdateMessage()
  {
    this.messageLabel.text = this.maildata.GetBodyString();
  }

  private void UpdateReward()
  {
    this.UpdateItems();
  }

  private void UpdateItems()
  {
    if (CustomDefine.GetChannel() == "google" || Application.platform == RuntimePlatform.IPhonePlayer || (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor))
    {
      this.subscribeTable.gameObject.SetActive(true);
      this.subscribeLabel.text = Utils.XLAT(this.cachedTable[(object) "subKey"].ToString());
      if (this.hasSubscribed)
      {
        this.subscribeButton.gameObject.SetActive(false);
        this.subRewardsTable.gameObject.SetActive(true);
        if (this.iapDailySubRewards != null)
          this.UpdateRewardsItems(this.iapDailySubRewards, this.subRewardsTable);
      }
      else
      {
        this.subscribeButton.gameObject.SetActive(true);
        this.subRewardsTable.gameObject.SetActive(false);
        this.subBtnLabel.text = Utils.XLAT("id_uppercase_subscribe");
      }
    }
    else
      this.subscribeTable.gameObject.SetActive(false);
    if (this.iapdDailyRewards == null)
      return;
    this.UpdateRewardsItems(this.iapdDailyRewards, this.rewardsTable);
  }

  private void UpdateRewardsItems(IAPDailySuccessPopup.IAPDailySuccessData data, UITable parent)
  {
    using (Dictionary<string, int>.Enumerator enumerator = data.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, int> current = enumerator.Current;
        GameObject gameObject = NGUITools.AddChild(parent.gameObject, this.rewardRender);
        gameObject.SetActive(true);
        RewardRender component = gameObject.GetComponent<RewardRender>();
        component.Feed(new SystemMessage.ItemReward()
        {
          internalID = int.Parse(current.Key),
          amount = current.Value
        });
        this.rewardsrenders.Add(component);
      }
    }
  }

  private void Clean()
  {
    using (List<RewardRender>.Enumerator enumerator = this.rewardsrenders.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.cachedTable = (Hashtable) null;
    this.rewardsrenders.Clear();
  }

  protected virtual void AddEventHandlers()
  {
    this.rewardsTable.onReposition += new UITable.OnReposition(this.HandleRewardsTableReposition);
    this.rewardsContentTable.onReposition += new UITable.OnReposition(this.HandleRewardsContentTableReposition);
    this.subRewardsTable.onReposition += new UITable.OnReposition(this.HandleSubRewardsTableReposition);
  }

  private void RemoveEventHandlers()
  {
    this.rewardsTable.onReposition = (UITable.OnReposition) null;
    this.rewardsContentTable.onReposition = (UITable.OnReposition) null;
  }

  private void HandleSubRewardsTableReposition()
  {
    this.subRewardsTable.onReposition -= new UITable.OnReposition(this.HandleSubRewardsTableReposition);
    this.subscribeTable.repositionNow = true;
  }

  protected void HandleRewardsTableReposition()
  {
    this.rewardsTable.onReposition -= new UITable.OnReposition(this.HandleRewardsTableReposition);
    this.rewardsContentTable.repositionNow = true;
  }

  protected void HandleRewardsContentTableReposition()
  {
    this.rewardsContentTable.onReposition -= new UITable.OnReposition(this.HandleRewardsContentTableReposition);
    Utils.ExecuteAtTheEndOfFrame(new System.Action(this.RepositionContentTable));
  }

  public void OnSubscribeBtnPressed()
  {
    UIManager.inst.OpenPopup("SubscibePopup", (Popup.PopupParameter) null);
  }

  private void RepositionContentTable()
  {
    this.contentTable.repositionNow = true;
  }

  private void DeserializeDailyRewards(out IAPDailySuccessPopup.IAPDailySuccessData rewards, Hashtable hash, string key)
  {
    string str = Utils.Object2Json((object) new Hashtable()
    {
      {
        (object) "items",
        hash[(object) key]
      }
    });
    rewards = JsonReader.Deserialize<IAPDailySuccessPopup.IAPDailySuccessData>(str);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.cachedTable = this.param.hashtable[(object) "data"] as Hashtable;
    if (this.cachedTable == null)
      return;
    if (this.cachedTable.ContainsKey((object) "rewards"))
    {
      if (this.cachedTable.ContainsKey((object) "subRewards"))
        this.DeserializeDailyRewards(out this.iapDailySubRewards, this.cachedTable, "subRewards");
      this.DeserializeDailyRewards(out this.iapdDailyRewards, this.cachedTable, "rewards");
    }
    this.UpdateUI();
    this.AddEventHandlers();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.OnClose(orgParam);
    this.RemoveEventHandlers();
    this.CancelInvoke();
    this.Clean();
  }

  public class IAPDailySuccessData
  {
    public Dictionary<string, int> items = new Dictionary<string, int>();
  }
}
