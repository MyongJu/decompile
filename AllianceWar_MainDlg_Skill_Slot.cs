﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_Skill_Slot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceWar_MainDlg_Skill_Slot : AllianceWar_MainDlg_Slot
{
  protected AllianceWar_MainDlg_Skill_Slot.Data _data = new AllianceWar_MainDlg_Skill_Slot.Data();
  [SerializeField]
  protected AllianceWar_MainDlg_Skill_Slot.Panel panel;

  public override void Setup(long magicId)
  {
    base.Setup(magicId);
    this._data.magic = DBManager.inst.DB_AllianceMagic.Get(magicId);
    if (this._data.magic != null)
    {
      this._data.skillInfo = ConfigManager.inst.DB_TempleSkill.GetById(this._data.magic.ConfigId);
      if (!this._data.dataExist)
        return;
      this.FreshSlot();
    }
    else
      this._data.skillInfo = (TempleSkillInfo) null;
  }

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public virtual void FreshSlot()
  {
    if (!this._data.dataExist)
      return;
    this.SetSkill((long) this._data.skillInfo.internalId);
    if (this._data.magic.allTargetAlliance != null && this._data.magic.allTargetAlliance.Count > 0)
      this.SetAlliance(this._data.magic.allTargetAlliance[0]);
    else if (this._data.magic.allTargetUser != null)
    {
      if (this._data.magic.allTargetUser.Count == 1)
        this.SetSinglePlayer(this._data.magic.allTargetUser[0]);
      else
        this.SetMultPlayer(this._data.magic.allTargetUser);
    }
    this.OnSecond(NetServerTime.inst.ServerTimestamp);
  }

  public virtual void SetSkill(long id)
  {
    if (!this._data.dataExist)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.skillIcon, this._data.skillInfo.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    this.panel.skillName.text = this._data.skillInfo.Name;
  }

  public virtual void SetSinglePlayer(long uid)
  {
    this.panel.singlePlayerContainer.gameObject.SetActive(true);
    this.panel.multPlayerContainer.gameObject.SetActive(false);
    this.panel.allianceContainer.gameObject.SetActive(false);
    UserData userData = DBManager.inst.DB_User.Get(uid);
    if (userData == null)
      return;
    CustomIconLoader.Instance.requestCustomIcon(this.panel.singlePlayerPortrait, userData.PortraitIconPath, userData.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.panel.singlePlayerPortrait, userData.LordTitle, 1);
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(userData.allianceId);
    this.panel.singlePlayerName.text = allianceData == null ? userData.userName : "[" + allianceData.allianceAcronym + "]" + userData.userName;
    CityData cityData = DBManager.inst.DB_City.Get(userData.uid);
    if (cityData != null)
    {
      this.panel.singlePlayerLocation.text = cityData.cityLocation.ToString();
      this._data.singlePlayerLoaction = cityData.cityLocation;
    }
    else
      this.panel.singlePlayerLocation.text = string.Empty;
  }

  public virtual void SetMultPlayer(List<long> uids)
  {
    this.panel.singlePlayerContainer.gameObject.SetActive(false);
    this.panel.multPlayerContainer.gameObject.SetActive(true);
    this.panel.allianceContainer.gameObject.SetActive(false);
    this._data.multiPlayerUids = new List<long>((IEnumerable<long>) uids);
    for (int index = 0; index < this.panel.multPlayerPortraitsContainer.Length; ++index)
    {
      if (index >= uids.Count)
      {
        this.panel.multPlayerPortraitsContainer[index].gameObject.SetActive(false);
      }
      else
      {
        UserData userData = DBManager.inst.DB_User.Get(uids[index]);
        if (userData != null)
        {
          this.panel.multPlayerPortraitsContainer[index].gameObject.SetActive(true);
          CustomIconLoader.Instance.requestCustomIcon(this.panel.multPlayerPortraits[index], userData.PortraitIconPath, userData.Icon, false);
          LordTitlePayload.Instance.ApplyUserAvator(this.panel.multPlayerPortraits[index], userData.LordTitle, 1);
        }
      }
    }
    this.panel.multPlayerMoreSymbol.gameObject.SetActive(uids.Count > 3);
  }

  public virtual void SetAlliance(long allianceId)
  {
    this.panel.singlePlayerContainer.gameObject.SetActive(false);
    this.panel.multPlayerContainer.gameObject.SetActive(false);
    this.panel.allianceContainer.gameObject.SetActive(true);
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(allianceId);
    if (allianceData == null)
      return;
    int layer = LayerMask.NameToLayer("SmallViewport");
    Transform[] componentsInChildren = this.panel.allianceSysmbol.gameObject.GetComponentsInChildren<Transform>();
    for (int index = 0; componentsInChildren != null && index < componentsInChildren.Length; ++index)
      componentsInChildren[index].gameObject.layer = layer;
    this.panel.allianceSysmbol.SetSymbols(allianceData.allianceSymbolCode);
    this.panel.allianceName.text = allianceData.allianceFullName;
  }

  public virtual void OnSecond(int serverTime)
  {
    if (!this._data.dataExist)
      return;
    if (this._data.magic.AllianceId == PlayerData.inst.allianceId && this._data.skillInfo.MaxDonation != 0)
    {
      this.panel.BT_PowerUp.gameObject.SetActive(true);
      this.panel.powerProgress.gameObject.SetActive(true);
      this.panel.powerProgress.value = (float) this._data.magic.Donation / (float) this._data.skillInfo.MaxDonation;
      this.panel.powerText.text = string.Format("{0} {1}/{2}({3}%)", (object) Utils.XLAT("alliance_altar_spell_power"), (object) this._data.magic.Donation.ToString(), (object) this._data.skillInfo.MaxDonation, (object) (float) ((double) this.panel.powerProgress.value * 100.0));
      if (this._data.magic.Donation > this._data.skillInfo.MaxDonation)
        this.panel.BT_PowerUp.isEnabled = false;
    }
    else
    {
      this.panel.BT_PowerUp.gameObject.SetActive(false);
      this.panel.powerProgress.gameObject.SetActive(false);
    }
    int time = (int) this._data.magic.TimerEnd - NetServerTime.inst.ServerTimestamp;
    if (time > 0)
    {
      this.panel.chargingTimeProgress.value = (float) (1.0 - (double) time / (double) this._data.skillInfo.DonationTime);
      this.panel.chargingTimeText.text = string.Format("{0} {1}", (object) Utils.XLAT("alliance_altar_charge_time_remaining"), (object) Utils.FormatTime1(time));
    }
    else
    {
      this.panel.chargingTimeProgress.value = 1f;
      this.panel.chargingTimeText.text = string.Empty;
    }
  }

  public virtual void OnSkillButtonClick()
  {
    UIManager.inst.OpenPopup("Alliance/DragonAltarPowerUpPopup", (Popup.PopupParameter) null);
  }

  public virtual void OnGotoButtonClick()
  {
    if (!this._data.dataExist)
      return;
    LinkerHub.Instance.LHGotoTile(new LPTile()
    {
      k = this._data.singlePlayerLoaction.K,
      x = this._data.singlePlayerLoaction.X,
      y = this._data.singlePlayerLoaction.Y
    });
  }

  public virtual void OnMultsButtonClick()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceMagicWarTargetListPopup", (Popup.PopupParameter) new AllianceWarTargetListPopup.Param()
    {
      uids = new List<long>((IEnumerable<long>) this._data.multiPlayerUids)
    });
  }

  public void Reset()
  {
    Transform transform = this.transform.Find("SkillContainer");
    this.panel.skillIcon = transform.Find("SkillIcon").GetComponent<UITexture>();
    this.panel.skillName = transform.Find("SkillName").GetComponent<UILabel>();
    this.panel.BT_PowerUp = transform.Find("Btn_PowerUp").GetComponent<UIButton>();
    this.panel.powerProgress = transform.Find("PowerSlider").GetComponent<UISlider>();
    this.panel.powerText = transform.Find("PowerSlider/Exp").GetComponent<UILabel>();
    this.panel.chargingTimeProgress = transform.Find("ChargeSlider").GetComponent<UISlider>();
    this.panel.chargingTimeText = transform.Find("ChargeSlider/Exp").GetComponent<UILabel>();
    this.panel.allianceContainer = this.transform.Find("TargetContainer/AllianceContainer");
    this.panel.allianceName = this.panel.allianceContainer.Find("Name").GetComponent<UILabel>();
    this.panel.allianceSysmbol = this.panel.allianceContainer.Find("AllianceSymbol").GetComponent<AllianceSymbol>();
    this.panel.singlePlayerContainer = this.transform.Find("TargetContainer/SinglePlayerContainer");
    this.panel.singlePlayerName = this.panel.singlePlayerContainer.Find("Name").GetComponent<UILabel>();
    this.panel.singlePlayerLocation = this.panel.singlePlayerContainer.Find("LocationText").GetComponent<UILabel>();
    this.panel.singlePlayerPortrait = this.panel.singlePlayerContainer.Find("Portrait").GetComponent<UITexture>();
    this.panel.BT_Goto = this.panel.singlePlayerContainer.Find("Btn_Goto").GetComponent<UIButton>();
    this.panel.multPlayerContainer = this.transform.Find("TargetContainer/MultiPlayersContainer");
    this.panel.multPlayerName = this.panel.multPlayerContainer.Find("Name").GetComponent<UILabel>();
    this.panel.multPlayerPortraitsContainer[0] = this.panel.multPlayerContainer.Find("Player1").transform;
    this.panel.multPlayerPortraitsContainer[1] = this.panel.multPlayerContainer.Find("Player2").transform;
    this.panel.multPlayerPortraitsContainer[2] = this.panel.multPlayerContainer.Find("Player3").transform;
    this.panel.multPlayerPortraits[0] = this.panel.multPlayerContainer.Find("Player1/Portrait").GetComponent<UITexture>();
    this.panel.multPlayerPortraits[1] = this.panel.multPlayerContainer.Find("Player2/Portrait").GetComponent<UITexture>();
    this.panel.multPlayerPortraits[2] = this.panel.multPlayerContainer.Find("Player3/Portrait").GetComponent<UITexture>();
    this.panel.multPlayerMoreSymbol = this.panel.multPlayerContainer.Find("MoreIcon").transform;
  }

  public class Data
  {
    public List<long> multiPlayerUids = new List<long>();
    public Coordinate singlePlayerLoaction;
    public AllianceMagicData magic;
    public TempleSkillInfo skillInfo;

    public bool dataExist
    {
      get
      {
        if (this.magic != null)
          return this.skillInfo != null;
        return false;
      }
    }
  }

  [Serializable]
  public class Panel
  {
    public Transform[] multPlayerPortraitsContainer = new Transform[3];
    public UITexture[] multPlayerPortraits = new UITexture[3];
    public const int MULT_PLAYER_COUNT = 3;
    public UITexture skillIcon;
    public UILabel skillName;
    public UIButton BT_PowerUp;
    public UISlider powerProgress;
    public UILabel powerText;
    public UISlider chargingTimeProgress;
    public UILabel chargingTimeText;
    public Transform allianceContainer;
    public UILabel allianceName;
    public AllianceSymbol allianceSysmbol;
    public Transform singlePlayerContainer;
    public UILabel singlePlayerName;
    public UILabel singlePlayerLocation;
    public UITexture singlePlayerPortrait;
    public UIButton BT_Goto;
    public Transform multPlayerContainer;
    public UILabel multPlayerName;
    public Transform multPlayerMoreSymbol;
  }
}
