﻿// Decompiled with JetBrains decompiler
// Type: AbyssRankRewardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class AbyssRankRewardInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "rank_min")]
  public int rankMin;
  [Config(Name = "rank_max")]
  public int rankMax;
  [Config(Name = "reward_id_1")]
  public int rewardId1;
  [Config(Name = "reward_value_1")]
  public int rewardValue1;
  [Config(Name = "reward_id_2")]
  public int rewardId2;
  [Config(Name = "reward_value_2")]
  public int rewardValue2;
  [Config(Name = "reward_id_3")]
  public int rewardId3;
  [Config(Name = "reward_value_3")]
  public int rewardValue3;
  [Config(Name = "reward_id_4")]
  public int rewardId4;
  [Config(Name = "reward_value_4")]
  public int rewardValue4;
  [Config(Name = "reward_id_5")]
  public int rewardId5;
  [Config(Name = "reward_value_5")]
  public int rewardValue5;
  [Config(Name = "reward_id_6")]
  public int rewardId6;
  [Config(Name = "reward_value_6")]
  public int rewardValue6;
  private int[] _allRewardId;
  private int[] _allRewardValue;

  public int[] allRewardId
  {
    get
    {
      if (this._allRewardId == null)
        this._allRewardId = new int[6]
        {
          this.rewardId1,
          this.rewardId2,
          this.rewardId3,
          this.rewardId4,
          this.rewardId5,
          this.rewardId6
        };
      return this._allRewardId;
    }
  }

  public int[] allRewardValue
  {
    get
    {
      if (this._allRewardValue == null)
        this._allRewardValue = new int[6]
        {
          this.rewardValue1,
          this.rewardValue2,
          this.rewardValue3,
          this.rewardValue4,
          this.rewardValue5,
          this.rewardValue6
        };
      return this._allRewardValue;
    }
  }
}
