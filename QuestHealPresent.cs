﻿// Decompiled with JetBrains decompiler
// Type: QuestHealPresent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class QuestHealPresent : BasePresent
{
  public override void Refresh()
  {
    int count = this.Count;
    int num = (int) DBManager.inst.DB_UserProfileDB.Get("troops_healed");
    if (num > count)
      num = count;
    this.ProgressValue = (float) num / (float) count;
    this.ProgressContent = num.ToString() + "/" + (object) count;
  }
}
