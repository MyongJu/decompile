﻿// Decompiled with JetBrains decompiler
// Type: LegendDevourDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class LegendDevourDialog : MonoBehaviour
{
  private List<LegendDevourSlot> m_LegendList = new List<LegendDevourSlot>();
  public GameObject m_LegendDevourPrefab;
  public LegendExpWindow m_ExpWindow;
  public LegendDevourSlot[] m_DevourSlots;
  public UIGrid m_Grid;
  public UIScrollView m_ScrollView;
  public UIButton m_AutoSelectButton;
  public UIButton m_DevourButton;
  private LegendData m_LegendData;

  public void Show(LegendData legendData)
  {
    this.gameObject.SetActive(true);
    this.m_LegendData = legendData;
    this.ResetUI();
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  public void OnDevourLegend()
  {
    Hashtable ht = new Hashtable();
    ht[(object) "uid"] = (object) PlayerData.inst.uid;
    ht[(object) "legend_id"] = (object) this.m_LegendData.LegendID;
    ArrayList arrayList = new ArrayList();
    ht[(object) "devoured_legends"] = (object) arrayList;
    for (int index = 0; index < this.m_DevourSlots.Length; ++index)
    {
      LegendData legendData = this.m_DevourSlots[index].GetLegendData();
      if (legendData != null)
        arrayList.Add((object) legendData.LegendID);
    }
    UIManager.inst.OpenPopup("DevourLegendPopup", (Popup.PopupParameter) new DevourLegendPopup.Parameter()
    {
      OkayCallback = (System.Action) (() => MessageHub.inst.GetPortByAction("Legend:devour").SendRequest(ht, (System.Action<bool, object>) ((ret, data) =>
      {
        this.ResetUI();
        UIManager.inst.toast.Show(string.Format(Utils.XLAT("toast_devourlegend_success"), (object) Utils.XLAT("hero_altar_uppercase_hero")), (System.Action) null, 4f, false);
      }), true))
    });
  }

  public void OnAutoSelect()
  {
    int num = ConfigManager.inst.DB_LegendPoint.GetLegenMaxPoint() - (int) this.m_LegendData.Xp;
    this.m_LegendList.Sort(new Comparison<LegendDevourSlot>(LegendDevourDialog.CompareSlot));
    int index1 = 0;
    for (int index2 = 0; index1 < this.m_DevourSlots.Length && index2 < this.m_LegendList.Count; ++index1)
    {
      int gain = this.GetGain();
      if (num > gain)
      {
        LegendDevourSlot devourSlot = this.m_DevourSlots[index1];
        if (devourSlot.GetLegendData() == null)
        {
          LegendData legendData = this.m_LegendList[index2].GetLegendData();
          this.RemoveLegendFromList(legendData);
          devourSlot.SetData(legendData, new System.Action<LegendData>(this.OnDevourSlotClicked));
        }
      }
      else
        break;
    }
    this.UpdateExpWidgets();
  }

  private static int CompareSlot(LegendDevourSlot a, LegendDevourSlot b)
  {
    return LegendDevourDialog.CompareLegends(a.GetLegendData(), b.GetLegendData());
  }

  private void ResetUI()
  {
    this.UpdateLegendList();
    this.m_ExpWindow.SetData(this.m_LegendData);
    for (int index = 0; index < this.m_DevourSlots.Length; ++index)
      this.m_DevourSlots[index].SetData((LegendData) null, new System.Action<LegendData>(this.OnDevourSlotClicked));
    this.m_AutoSelectButton.isEnabled = ConfigManager.inst.DB_LegendPoint.GetLegenMaxPoint() - (int) this.m_LegendData.Xp > 0 && this.m_LegendList.Count > 0;
    this.m_DevourButton.isEnabled = false;
  }

  private void UpdateExpWidgets()
  {
    int nextXp = 0;
    int legendLevelByXp = ConfigManager.inst.DB_LegendPoint.GetLegendLevelByXP(this.m_LegendData.Xp, out nextXp);
    int gain = this.GetGain();
    this.m_ExpWindow.SetExpText((int) this.m_LegendData.Xp, nextXp, gain);
    this.m_AutoSelectButton.isEnabled = ConfigManager.inst.DB_LegendPoint.GetLegenMaxPoint() - (int) this.m_LegendData.Xp > gain && this.m_LegendList.Count > 0;
    this.m_DevourButton.isEnabled = legendLevelByXp != ConfigManager.inst.DB_LegendPoint.GetLegendMaxLevel() && gain > 0;
  }

  private int GetGain()
  {
    int num = 0;
    for (int index = 0; index < this.m_DevourSlots.Length; ++index)
    {
      LegendData legendData = this.m_DevourSlots[index].GetLegendData();
      if (legendData != null)
        num += (int) legendData.Xp;
    }
    return num;
  }

  private void ClearLegendList()
  {
    int count = this.m_LegendList.Count;
    for (int index = 0; index < count; ++index)
    {
      LegendDevourSlot legend = this.m_LegendList[index];
      legend.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) legend.gameObject);
    }
    this.m_LegendList.Clear();
  }

  private LegendDevourSlot CreateLegendSlot()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_LegendDevourPrefab);
    gameObject.SetActive(true);
    gameObject.transform.SetParent(this.m_Grid.transform, false);
    return gameObject.GetComponent<LegendDevourSlot>();
  }

  private void UpdateLegendList()
  {
    this.ClearLegendList();
    List<LegendData> currentLegend = DBManager.inst.DB_Legend.GetCurrentLegend(0);
    int count = currentLegend.Count;
    for (int index = 0; index < count; ++index)
    {
      LegendData legendData = currentLegend[index];
      if (this.m_LegendData != legendData && legendData.Xp > 0L)
      {
        LegendDevourSlot legendSlot = this.CreateLegendSlot();
        legendSlot.SetData(legendData, new System.Action<LegendData>(this.OnSelectSlotClicked));
        this.m_LegendList.Add(legendSlot);
      }
    }
    this.m_Grid.sorting = UIGrid.Sorting.Custom;
    this.m_Grid.onCustomSort = new Comparison<Transform>(LegendDevourDialog.Compare);
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  private static int CompareLegends(LegendData h0, LegendData h1)
  {
    long num1 = (long) (h0.Level - h1.Level);
    if (num1 != 0L)
      return (int) num1;
    long num2 = h0.Power - h1.Power;
    if (num2 != 0L)
      return (int) num2;
    return ConfigManager.inst.DB_Legend.GetLegendInfo(h0.LegendID).priority - ConfigManager.inst.DB_Legend.GetLegendInfo(h1.LegendID).priority;
  }

  private static int Compare(Transform x, Transform y)
  {
    return LegendDevourDialog.CompareLegends(x.GetComponent<LegendDevourSlot>().GetLegendData(), y.GetComponent<LegendDevourSlot>().GetLegendData());
  }

  private void OnDevourSlotClicked(LegendData legendData)
  {
    if (legendData == null)
      return;
    for (int index1 = 0; index1 < this.m_DevourSlots.Length; ++index1)
    {
      LegendDevourSlot devourSlot1 = this.m_DevourSlots[index1];
      if (legendData == devourSlot1.GetLegendData())
      {
        int index2 = this.m_DevourSlots.Length - 1;
        for (int index3 = index1; index3 < index2; ++index3)
        {
          LegendDevourSlot devourSlot2 = this.m_DevourSlots[index3 + 1];
          this.m_DevourSlots[index3].SetData(devourSlot2.GetLegendData(), new System.Action<LegendData>(this.OnDevourSlotClicked));
        }
        this.m_DevourSlots[index2].SetData((LegendData) null, new System.Action<LegendData>(this.OnDevourSlotClicked));
        LegendDevourSlot legendSlot = this.CreateLegendSlot();
        legendSlot.SetData(legendData, new System.Action<LegendData>(this.OnSelectSlotClicked));
        this.m_LegendList.Add(legendSlot);
        this.m_Grid.Reposition();
        break;
      }
    }
    this.UpdateExpWidgets();
  }

  private void OnSelectSlotClicked(LegendData legendData)
  {
    if (ConfigManager.inst.DB_LegendPoint.GetLegenMaxPoint() - (int) this.m_LegendData.Xp <= this.GetGain())
      return;
    for (int index = 0; index < this.m_DevourSlots.Length; ++index)
    {
      LegendDevourSlot devourSlot = this.m_DevourSlots[index];
      if (devourSlot.GetLegendData() == null)
      {
        devourSlot.SetData(legendData, new System.Action<LegendData>(this.OnDevourSlotClicked));
        this.RemoveLegendFromList(legendData);
        break;
      }
    }
    this.UpdateExpWidgets();
  }

  private void RemoveLegendFromList(LegendData legendData)
  {
    int count = this.m_LegendList.Count;
    for (int index = 0; index < count; ++index)
    {
      LegendDevourSlot legend = this.m_LegendList[index];
      if (legend.GetLegendData() == legendData)
      {
        legend.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) legend.gameObject);
        this.m_LegendList.Remove(legend);
        this.m_Grid.Reposition();
        break;
      }
    }
  }
}
