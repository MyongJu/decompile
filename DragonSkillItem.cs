﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonSkillItem : MonoBehaviour
{
  private const string IMAGE_PATH = "Texture/Dragon/";
  public UISprite bg;
  public GameObject lockContent;
  public GameObject newskillLockGo;
  public GameObject skillContent;
  public GameObject levContent;
  public GameObject stateContent;
  public GameObject maskContent;
  public UILabel requireLabel;
  public UISprite upgradeArrow;
  public UITexture emptyIcon;
  public UITexture dragonKillIcon;
  public UILabel skillLev;
  public UILabel skillLevState;
  public GameObject rootStarLevel;
  public UILabel labelStarLevel;
  private long skill_id;
  public bool showState;
  public UILabel extraLevel;

  public void Reresh(int skill_id)
  {
    ConfigDragonSkillMainInfo skillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo(skill_id);
    ConfigDragonSkillGroupInfo dragonSkillGroupInfo = ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupInfo(skillMainInfo.group_id);
    int extarLevel = DBManager.inst.DB_Artifact.GetExtarLevel(PlayerData.inst.uid, dragonSkillGroupInfo.internalId);
    this.extraLevel.text = extarLevel <= 0 ? string.Empty : "+" + (object) extarLevel;
    if (this.skill_id == 0L)
    {
      this.SetData(skill_id);
    }
    else
    {
      if ((long) skill_id == this.skill_id || this.GroupId != dragonSkillGroupInfo.internalId)
        return;
      this.skill_id = (long) skill_id;
      this.skillLev.text = skillMainInfo.level.ToString();
    }
  }

  public int GroupId { get; set; }

  public void SetData(int skill_id)
  {
    this.Clear();
    this.NormalHide();
    if (skill_id <= 0)
      return;
    this.skill_id = (long) skill_id;
    ConfigDragonSkillMainInfo skillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo(skill_id);
    ConfigDragonSkillGroupInfo dragonSkillGroupInfo = ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupInfo(skillMainInfo.group_id);
    this.skillLev.text = skillMainInfo.level.ToString();
    this.GroupId = skillMainInfo.group_id;
    if (this.showState && this.IsForgoten())
      this.skillLevState.text = skillMainInfo.level != 0 ? string.Empty : "Forgoten";
    this.SetChildVisable(this.skillContent, true);
    this.SetChildVisable(this.levContent, true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.dragonKillIcon, dragonSkillGroupInfo.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    int extarLevel = DBManager.inst.DB_Artifact.GetExtarLevel(PlayerData.inst.uid, dragonSkillGroupInfo.internalId);
    this.extraLevel.text = extarLevel <= 0 ? string.Empty : "+" + (object) extarLevel;
    if (PlayerData.inst.dragonData == null)
      return;
    int starLevelBySkillId = PlayerData.inst.dragonData.GetSkillGroupStarLevelBySkillId(skill_id);
    if (starLevelBySkillId <= 0 || !(bool) ((UnityEngine.Object) this.rootStarLevel))
      return;
    this.SetChildVisable(this.rootStarLevel, true);
    this.labelStarLevel.text = starLevelBySkillId.ToString();
  }

  private bool IsForgoten()
  {
    return PlayerData.inst.dragonData.Skills.IsSetupSkill((int) this.skill_id);
  }

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.dragonKillIcon);
    BuilderFactory.Instance.Release((UIWidget) this.emptyIcon);
    UIWidget component = this.gameObject.GetComponent<UIWidget>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.alpha = 1f;
  }

  public void DisplayUpgrade()
  {
    if (this.skill_id <= 0L)
      return;
    this.SetChildVisable((UIWidget) this.upgradeArrow, true);
  }

  public void ShowLock(bool show)
  {
    NGUITools.SetActive(this.newskillLockGo, show);
    NGUITools.SetActive(this.levContent, !show);
  }

  public void Empty(string type)
  {
    this.NormalHide();
    string path = "Texture/Dragon/";
    string key = type;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DragonSkillItem.\u003C\u003Ef__switch\u0024map51 == null)
      {
        // ISSUE: reference to a compiler-generated field
        DragonSkillItem.\u003C\u003Ef__switch\u0024map51 = new Dictionary<string, int>(4)
        {
          {
            "attack",
            0
          },
          {
            "defend",
            1
          },
          {
            "gather",
            2
          },
          {
            "attack_monster",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DragonSkillItem.\u003C\u003Ef__switch\u0024map51.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            path += "icon_dragon_skill_attack";
            break;
          case 1:
            path += "icon_dragon_skill_defense";
            break;
          case 2:
            path += "icon_dragon_skill_gather";
            break;
          case 3:
            path += "icon_dragon_skill_monster";
            break;
        }
      }
    }
    this.SetChildVisable((UIWidget) this.emptyIcon, true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.emptyIcon, path, (System.Action<bool>) null, true, false, string.Empty);
  }

  public void MarkMask()
  {
    UIWidget component = this.gameObject.GetComponent<UIWidget>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      component.alpha = 0.3f;
    this.SetChildVisable(this.levContent, false);
  }

  public void Lock(int reqLev = 0)
  {
    this.NormalHide();
    this.SetChildVisable((UIWidget) this.requireLabel, reqLev > 0);
    if ((UnityEngine.Object) this.requireLabel != (UnityEngine.Object) null)
    {
      if (reqLev > 0)
        this.requireLabel.text = ScriptLocalization.GetWithPara("dragon_skill_require", new Dictionary<string, string>()
        {
          {
            "num",
            reqLev.ToString()
          }
        }, true);
      else
        this.requireLabel.text = string.Empty;
    }
    this.SetChildVisable(this.lockContent, true);
  }

  private void NormalHide()
  {
    this.SetChildVisable((UIWidget) this.emptyIcon, false);
    this.SetChildVisable((UIWidget) this.upgradeArrow, false);
    this.SetChildVisable(this.levContent, false);
    this.SetChildVisable(this.skillContent, false);
    this.SetChildVisable(this.levContent, false);
    this.SetChildVisable(this.stateContent, this.showState);
    this.SetChildVisable(this.maskContent, false);
    this.SetChildVisable((UIWidget) this.requireLabel, false);
    this.SetChildVisable(this.lockContent, false);
    this.SetChildVisable(this.newskillLockGo, false);
    if (!(bool) ((UnityEngine.Object) this.rootStarLevel))
      return;
    this.SetChildVisable(this.rootStarLevel, false);
  }

  private void SetChildVisable(UIWidget widget, bool visiable = false)
  {
    if (!((UnityEngine.Object) widget != (UnityEngine.Object) null))
      return;
    this.SetChildVisable(widget.gameObject, visiable);
  }

  private void SetChildVisable(GameObject widget, bool visiable = false)
  {
    if (!((UnityEngine.Object) widget != (UnityEngine.Object) null))
      return;
    NGUITools.SetActive(widget, visiable);
  }

  public void OnDragonSkillClick()
  {
    if (this.skill_id <= 0L)
      return;
    ConfigDragonSkillMainInfo skillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo((int) this.skill_id);
    UIManager.inst.OpenPopup("Dragon/DragonSkillUpgradePopup", (Popup.PopupParameter) new DragonSkillUpgradePopup.Parameter()
    {
      dragonSkillGroupID = skillMainInfo.group_id
    });
  }
}
