﻿// Decompiled with JetBrains decompiler
// Type: RebateByRechargePayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class RebateByRechargePayload
{
  private List<RebateByRechargeRewards> rebateRewardsList = new List<RebateByRechargeRewards>();
  private int startTime;
  private int endTime;
  private long rechargedAmount;
  private static RebateByRechargePayload instance;

  public event System.Action OnRebateDataChanged;

  public int StartTime
  {
    get
    {
      return this.startTime;
    }
  }

  public int EndTime
  {
    get
    {
      return this.endTime;
    }
  }

  public long RechargedAmount
  {
    get
    {
      return this.rechargedAmount;
    }
  }

  public List<RebateByRechargeRewards> RebateRewardsList
  {
    get
    {
      this.rebateRewardsList.Sort(new Comparison<RebateByRechargeRewards>(this.SortByStep));
      return this.rebateRewardsList;
    }
  }

  private void AddRebateReward(RebateByRechargeRewards rrr)
  {
    RebateByRechargeRewards byRechargeRewards = this.rebateRewardsList.Find((Predicate<RebateByRechargeRewards>) (x => x.Step == rrr.Step));
    if (byRechargeRewards != null)
      this.rebateRewardsList.Remove(byRechargeRewards);
    this.rebateRewardsList.Add(rrr);
  }

  public int CanCollectTotalCount
  {
    get
    {
      int num = 0;
      for (int index = 0; index < this.rebateRewardsList.Count; ++index)
      {
        RebateByRechargeRewards rebateRewards = this.rebateRewardsList[index];
        if (this.rechargedAmount >= (long) rebateRewards.TargetRechargeAmount && !rebateRewards.Rebated)
          ++num;
      }
      return num;
    }
  }

  public static RebateByRechargePayload Instance
  {
    get
    {
      if (RebateByRechargePayload.instance == null)
        RebateByRechargePayload.instance = new RebateByRechargePayload();
      return RebateByRechargePayload.instance;
    }
  }

  public bool CanShowRebateHUD
  {
    get
    {
      if (NetServerTime.inst.ServerTimestamp >= this.startTime)
        return NetServerTime.inst.ServerTimestamp < this.endTime;
      return false;
    }
  }

  public void Decode(Hashtable data)
  {
    Hashtable data1 = new Hashtable();
    Hashtable data2 = new Hashtable();
    Hashtable data3 = new Hashtable();
    ArrayList data4 = new ArrayList();
    if (data == null || data[(object) "ret"] == null && data[(object) "user_payment_return_info"] == null)
      this.endTime = -1;
    this.ResetRewardsData();
    if (data != null && data.ContainsKey((object) "payment_return_info"))
    {
      DatabaseTools.CheckAndParseOrgData(data[(object) "payment_return_info"], out data3);
      if (data3 != null)
      {
        DatabaseTools.UpdateData(data3, "start", ref this.startTime);
        DatabaseTools.UpdateData(data3, "end", ref this.endTime);
        DatabaseTools.CheckAndParseOrgData(data3[(object) "rewards"], out data1);
        if (data1 != null && data1.Count > 0)
        {
          Hashtable data5 = new Hashtable();
          ArrayList data6 = new ArrayList();
          int outData1 = 0;
          int outData2 = 0;
          int outData3 = 0;
          string empty = string.Empty;
          IEnumerator enumerator = data1.Keys.GetEnumerator();
          while (enumerator.MoveNext())
          {
            data5.Clear();
            DatabaseTools.CheckAndParseOrgData(data1[(object) enumerator.Current.ToString()], out data5);
            if (data5 != null && data5.Count > 0)
            {
              data6.Clear();
              DatabaseTools.UpdateData(data5, "payment_amount", ref outData1);
              DatabaseTools.CheckAndParseOrgData(data5[(object) "rewards"], out data6);
              RebateByRechargeRewards rrr = new RebateByRechargeRewards();
              if (outData1 >= 0)
                rrr.TargetRechargeAmount = outData1;
              int result;
              int.TryParse(enumerator.Current.ToString(), out result);
              rrr.Step = result;
              if (data6 != null)
              {
                for (int index = 0; index < data6.Count; ++index)
                {
                  Hashtable hashtable = data6[index] as Hashtable;
                  if (hashtable != null)
                  {
                    foreach (object key in (IEnumerable) hashtable.Keys)
                    {
                      RebateByRechargeRewards.RewardItemInfo rewardItemInfo = new RebateByRechargeRewards.RewardItemInfo();
                      string str = key.ToString();
                      DatabaseTools.UpdateData(hashtable[(object) str] as Hashtable, "item_amount", ref outData2);
                      DatabaseTools.UpdateData(hashtable[(object) str] as Hashtable, "shine", ref outData3);
                      rewardItemInfo.rewardItemId = str;
                      rewardItemInfo.rewardItemCount = outData2;
                      rewardItemInfo.hasSpecialEffect = outData3 == 1;
                      rrr.rewardItemInfoList.Add(rewardItemInfo);
                    }
                  }
                }
              }
              this.AddRebateReward(rrr);
            }
          }
        }
      }
    }
    if (data == null || !data.ContainsKey((object) "user_payment_return_info"))
      return;
    DatabaseTools.CheckAndParseOrgData(data[(object) "user_payment_return_info"], out data2);
    if (data2 == null)
      return;
    DatabaseTools.UpdateData(data2, "total_payment", ref this.rechargedAmount);
    DatabaseTools.CheckAndParseOrgData(data2[(object) "has_gain_rewards"], out data4);
    this.UpdateRewardsData(data4);
  }

  private void UpdateRewardsData(ArrayList collectedRewards)
  {
    if (collectedRewards == null)
      return;
    for (int index = 0; index < collectedRewards.Count; ++index)
    {
      int step = 0;
      int.TryParse(collectedRewards[index].ToString(), out step);
      RebateByRechargeRewards rrr = this.rebateRewardsList.Find((Predicate<RebateByRechargeRewards>) (x => x.Step == step));
      if (rrr != null)
      {
        rrr.Rebated = true;
        this.AddRebateReward(rrr);
      }
    }
  }

  private void ResetRewardsData()
  {
    this.rechargedAmount = 0L;
    if (this.rebateRewardsList == null)
      return;
    for (int index = 0; index < this.rebateRewardsList.Count; ++index)
      this.rebateRewardsList[index].Rebated = false;
  }

  public void Initialize()
  {
    this.RequestServerData((System.Action<bool, object>) null);
    MessageHub.inst.GetPortByAction("payment_return").AddEvent(new System.Action<object>(this.OnPushRebateSuccess));
  }

  public void Dispose()
  {
    MessageHub.inst.GetPortByAction("payment_return").RemoveEvent(new System.Action<object>(this.OnPushRebateSuccess));
  }

  public void RequestServerData(System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("PaymentReturn:getPaymentReturnInfo").SendLoader((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data as Hashtable);
      if (callback == null)
        return;
      callback(ret, data);
    }), false, false);
  }

  private void OnPushRebateSuccess(object result)
  {
    this.Decode(result as Hashtable);
    if (this.OnRebateDataChanged == null)
      return;
    this.OnRebateDataChanged();
  }

  public int SortByStep(RebateByRechargeRewards a, RebateByRechargeRewards b)
  {
    return a.Step.CompareTo(b.Step);
  }
}
