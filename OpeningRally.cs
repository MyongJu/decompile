﻿// Decompiled with JetBrains decompiler
// Type: OpeningRally
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class OpeningRally : MonoBehaviour
{
  public OpeningRallySlotContainer orsc;
  public UITexture tex;

  public void OnClick()
  {
    this.orsc.Clear();
    AudioManager.Instance.PlaySound("sfx_intro_attack", false);
    BuilderFactory.Instance.Release((UIWidget) this.tex);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    OpeningCinematicManager.Instance.board.PlayAction(2);
  }

  public void Start()
  {
    BuilderFactory.Instance.Build((UIWidget) this.tex, "Texture/Daily_reward/tiles_monstercamp2", (System.Action<bool>) null, true, false, true, string.Empty);
  }
}
