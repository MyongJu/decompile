﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragonTendencyBoost
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class ConfigDragonTendencyBoost
{
  private ConfigParse parse = new ConfigParse();
  private List<ConfigDragonTendencyBoostInfo> dragonTendencyBoosts = new List<ConfigDragonTendencyBoostInfo>();
  private Dictionary<string, ConfigDragonTendencyBoostInfo> datas;
  private Dictionary<int, ConfigDragonTendencyBoostInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<ConfigDragonTendencyBoostInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    using (Dictionary<string, ConfigDragonTendencyBoostInfo>.Enumerator enumerator = this.datas.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.dragonTendencyBoosts.Add(enumerator.Current.Value);
    }
  }

  public ConfigDragonTendencyBoostInfo GetData(DragonData.Tendency tendence)
  {
    ConfigDragonTendencyBoostInfo tendencyBoostInfo;
    this.datas.TryGetValue(this.GetTendencySkillID(tendence), out tendencyBoostInfo);
    return tendencyBoostInfo;
  }

  public string GetTendencySkillID(DragonData.Tendency tendence)
  {
    switch (tendence)
    {
      case DragonData.Tendency.normal:
        return "normal";
      case DragonData.Tendency.ferocious:
        return "dark_2";
      case DragonData.Tendency.swift:
        return "dark_1";
      case DragonData.Tendency.pure:
        return "light_1";
      case DragonData.Tendency.shine:
        return "light_2";
      default:
        return string.Empty;
    }
  }

  public ConfigDragonTendencyBoostInfo Get(string id)
  {
    ConfigDragonTendencyBoostInfo tendencyBoostInfo;
    this.datas.TryGetValue(id, out tendencyBoostInfo);
    return tendencyBoostInfo;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, ConfigDragonTendencyBoostInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, ConfigDragonTendencyBoostInfo>) null;
  }
}
