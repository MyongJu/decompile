﻿// Decompiled with JetBrains decompiler
// Type: TradingHallCatalog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class TradingHallCatalog : MonoBehaviour
{
  private Color _catalogClickColor = new Color(0.9215686f, 0.5372549f, 0.0f);
  private Dictionary<int, TradingHallCatalog> _catalogItemDict = new Dictionary<int, TradingHallCatalog>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UILabel _catalogName;
  [SerializeField]
  private UISprite _catalogSprite;
  [SerializeField]
  private UISprite _catalogArrowSprite;
  [SerializeField]
  private TradingHallCatalog _parentCatalog;
  [SerializeField]
  private UITable _subcatalogTable;
  [SerializeField]
  private GameObject _subcatalogItemPrefab;
  [SerializeField]
  private GameObject _subcatalogItemRoot;
  private int _catalogLevel;
  private int _catalogId;
  private bool _catalogClicked;
  private Color _catalogOriginColor;
  public System.Action<TradingHallCatalog> OnCurrentCatalogClicked;

  public int CatalogLevel
  {
    get
    {
      return this._catalogLevel;
    }
  }

  public int CatalogId
  {
    get
    {
      return this._catalogId;
    }
  }

  public TradingHallCatalog ParentCatalog
  {
    get
    {
      return this._parentCatalog;
    }
  }

  public bool Folded
  {
    get
    {
      if (this.CatalogLevel == 3)
        return false;
      return !this._catalogClicked;
    }
  }

  public void SetData(int catalogLevel, int catalogId)
  {
    this._catalogLevel = catalogLevel;
    this._catalogId = catalogId;
    this._catalogOriginColor = this._catalogName.color;
    if (this._catalogOriginColor == Color.black)
      this._catalogOriginColor = Color.white;
    this.UpdateUI();
  }

  public void OnCatalogClicked()
  {
    this._catalogClicked = !this._catalogClicked;
    if ((UnityEngine.Object) this._catalogArrowSprite != (UnityEngine.Object) null)
      this._catalogArrowSprite.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, !this._catalogClicked ? 0.0f : -90f));
    if (this._catalogClicked && (UnityEngine.Object) this._subcatalogTable != (UnityEngine.Object) null)
      this.GenerateSubCatalog();
    else
      this.ClearCatalogData();
    this.RepositionCatalog();
    switch (this._catalogLevel)
    {
      case 1:
        if (this.OnCurrentCatalogClicked == null)
          break;
        this.OnCurrentCatalogClicked(this);
        break;
      case 2:
        if (!((UnityEngine.Object) this._parentCatalog != (UnityEngine.Object) null) || this._parentCatalog.OnCurrentCatalogClicked == null)
          break;
        this._parentCatalog.OnCurrentCatalogClicked(this);
        break;
      case 3:
        if (!((UnityEngine.Object) this._parentCatalog != (UnityEngine.Object) null) || !((UnityEngine.Object) this._parentCatalog.ParentCatalog != (UnityEngine.Object) null) || this._parentCatalog.ParentCatalog.OnCurrentCatalogClicked == null)
          break;
        this._parentCatalog.ParentCatalog.OnCurrentCatalogClicked(this);
        break;
    }
  }

  public void ResetNameColor()
  {
    if ((double) this._catalogOriginColor.a <= 0.0)
      return;
    this._catalogName.color = this._catalogOriginColor;
  }

  public void SetNameColor()
  {
    this._catalogName.color = this._catalogClickColor;
  }

  private void UpdateUI()
  {
    switch (this._catalogLevel)
    {
      case 1:
        ExchangeHallSort1Info exchangeHallSort1Info = ConfigManager.inst.DB_ExchangeHallSort1.Get(this._catalogId);
        if (exchangeHallSort1Info == null)
          break;
        this._catalogName.text = exchangeHallSort1Info.Name;
        if (!((UnityEngine.Object) this._catalogSprite != (UnityEngine.Object) null))
          break;
        this._catalogSprite.spriteName = exchangeHallSort1Info.SpriteName;
        break;
      case 2:
        ExchangeHallSort2Info exchangeHallSort2Info = ConfigManager.inst.DB_ExchangeHallSort2.Get(this._catalogId);
        if (exchangeHallSort2Info == null)
          break;
        this._catalogName.text = exchangeHallSort2Info.Name;
        if (!((UnityEngine.Object) this._catalogSprite != (UnityEngine.Object) null))
          break;
        this._catalogSprite.spriteName = exchangeHallSort2Info.SpriteName;
        break;
      case 3:
        ExchangeHallSort3Info exchangeHallSort3Info = ConfigManager.inst.DB_ExchangeHallSort3.Get(this._catalogId);
        if (exchangeHallSort3Info == null)
          break;
        this._catalogName.text = exchangeHallSort3Info.Name;
        if (!((UnityEngine.Object) this._catalogSprite != (UnityEngine.Object) null))
          break;
        this._catalogSprite.spriteName = exchangeHallSort3Info.SpriteName;
        break;
    }
  }

  private void ClearCatalogData()
  {
    using (Dictionary<int, TradingHallCatalog>.Enumerator enumerator = this._catalogItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._catalogItemDict.Clear();
    this._itemPool.Clear();
  }

  public void RepositionCatalog()
  {
    if ((UnityEngine.Object) this._subcatalogTable != (UnityEngine.Object) null)
    {
      this._subcatalogTable.repositionNow = true;
      this._subcatalogTable.Reposition();
    }
    if (!((UnityEngine.Object) this._parentCatalog != (UnityEngine.Object) null))
      return;
    this._parentCatalog.RepositionCatalog();
  }

  private TradingHallCatalog GenerateCatalogSlot(int catalogLevel, int catalogId)
  {
    this._itemPool.Initialize(this._subcatalogItemPrefab, this._subcatalogItemRoot);
    GameObject go = this._itemPool.AddChild(this._subcatalogTable.gameObject);
    NGUITools.SetActive(go, true);
    TradingHallCatalog component = go.GetComponent<TradingHallCatalog>();
    component.SetData(catalogLevel, catalogId);
    return component;
  }

  private void GenerateSubCatalog()
  {
    this.ClearCatalogData();
    List<int> intList = (List<int>) null;
    switch (this._catalogLevel)
    {
      case 1:
        intList = ConfigManager.inst.DB_ExchangeHall.GetCatalog2List(this._catalogId);
        break;
      case 2:
        if ((UnityEngine.Object) this._parentCatalog != (UnityEngine.Object) null)
        {
          intList = ConfigManager.inst.DB_ExchangeHall.GetCatalog3List(this._parentCatalog.CatalogId, this._catalogId);
          break;
        }
        break;
    }
    if (intList == null)
      return;
    for (int key = 0; key < intList.Count; ++key)
    {
      if (intList[key] > 0)
      {
        TradingHallCatalog catalogSlot = this.GenerateCatalogSlot(this._catalogLevel + 1, intList[key]);
        this._catalogItemDict.Add(key, catalogSlot);
      }
    }
  }
}
