﻿// Decompiled with JetBrains decompiler
// Type: AudioManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
  private List<string> deleteSA = new List<string>();
  private const string sfxPath = "Audio/SFX/";
  private const string bgmPath = "Audio/BGM/";
  private static AudioManager _instance;
  private AudioSource BGMAudioSource;
  private Dictionary<string, AudioSource> saPair;
  private FrequencyCache<AudioClip> audioCache;
  private string musicName;
  [DevSetting("Mute Sound", null, null)]
  private static bool devMuteSound;
  private string oldBGM;

  public static AudioManager Instance
  {
    get
    {
      if ((UnityEngine.Object) AudioManager._instance == (UnityEngine.Object) null)
      {
        AudioManager._instance = UnityEngine.Object.FindObjectOfType<AudioManager>();
        if ((UnityEngine.Object) null == (UnityEngine.Object) AudioManager._instance)
          throw new ArgumentException("lost of audio manager");
      }
      return AudioManager._instance;
    }
  }

  public bool IsEnabled
  {
    get
    {
      return true;
    }
  }

  private void Awake()
  {
    AudioManager._instance = this;
    this.BGMAudioSource = this.gameObject.GetComponent<AudioSource>();
    this.saPair = new Dictionary<string, AudioSource>(2);
    this.audioCache = new FrequencyCache<AudioClip>(5, (System.Action<AudioClip>) null);
    SettingManager.Instance.OnSettingChanged += new System.Action(this.OnSettingChangeHandler);
  }

  public void Dispose()
  {
    SettingManager.Instance.OnSettingChanged -= new System.Action(this.OnSettingChangeHandler);
  }

  private void OnSettingChangeHandler()
  {
    if (!SettingManager.Instance.IsMusicOff)
    {
      if (this.BGMAudioSource.isPlaying)
        return;
      if ((UnityEngine.Object) this.BGMAudioSource.clip == (UnityEngine.Object) null)
      {
        if (string.IsNullOrEmpty(this.musicName))
          return;
        this.PlayBGM(this.musicName);
      }
      else
        this.BGMAudioSource.Play();
    }
    else
      this.BGMAudioSource.Stop();
  }

  private void Update()
  {
    this.UpdateSounds();
  }

  private void UpdateSounds()
  {
    if (this.saPair.Count == 0)
      return;
    this.deleteSA.Clear();
    Dictionary<string, AudioSource>.Enumerator enumerator1 = this.saPair.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      if (!enumerator1.Current.Value.isPlaying)
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator1.Current.Value);
        this.deleteSA.Add(enumerator1.Current.Key);
      }
    }
    List<string>.Enumerator enumerator2 = this.deleteSA.GetEnumerator();
    while (enumerator2.MoveNext())
      this.saPair.Remove(enumerator2.Current);
  }

  public void StopAndPlaySound(string soundName)
  {
    this.StopSound(soundName);
    this.PlaySound(soundName, false);
  }

  public void PlaySound(string soundName, bool isLoop = false)
  {
    if (SettingManager.Instance.IsSFXOff || this.saPair.ContainsKey(soundName) || ConfigManager.inst.DB_Audio == null)
      return;
    AudioClip audioClip = (AudioClip) null;
    if (!this.audioCache.TryGetObject(soundName, out audioClip))
    {
      AudioData data = ConfigManager.inst.DB_Audio.GetData(soundName);
      if (data == null)
        return;
      audioClip = AssetManager.Instance.HandyLoad(data.fullname, (System.Type) null) as AudioClip;
      if ((UnityEngine.Object) audioClip == (UnityEngine.Object) null)
        return;
      this.audioCache.AddObject(soundName, audioClip);
    }
    AudioSource audioSource = this.gameObject.AddComponent<AudioSource>();
    audioSource.clip = audioClip;
    audioSource.loop = isLoop;
    audioSource.Play();
    this.saPair.Add(soundName, audioSource);
  }

  public void StopSound(string soundName)
  {
    AudioSource audioSource = (AudioSource) null;
    if (!this.saPair.TryGetValue(soundName, out audioSource))
      return;
    audioSource.Stop();
    UnityEngine.Object.Destroy((UnityEngine.Object) this.saPair[soundName]);
    this.saPair.Remove(soundName);
  }

  public void PlayBGM(string bgmName)
  {
    this.musicName = bgmName;
    if (SettingManager.Instance.IsMusicOff)
      return;
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.BGMAudioSource)
      throw new ArgumentException("lost of audio source for bgm");
    AudioData ad = ConfigManager.inst.DB_Audio.GetData(bgmName);
    if (ad == null)
      return;
    AssetManager.Instance.LoadAsync(ad.fullname, (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      if (!ret)
        return;
      this.BGMAudioSource.clip = obj as AudioClip;
      this.BGMAudioSource.Play();
      if (this.oldBGM != null)
        AssetManager.Instance.UnLoadAsset(this.oldBGM, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      this.oldBGM = ad.fullname;
    }), (System.Type) null);
  }

  public void StopCurrentBGM()
  {
    this.BGMAudioSource.Stop();
    this.BGMAudioSource.clip = (AudioClip) null;
  }
}
