﻿// Decompiled with JetBrains decompiler
// Type: IconData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class IconData : IComponentData
{
  public string[] contents;
  public bool[] visibles;
  public string image;
  public string seticon;
  public int lordTitleId;

  public IconData()
  {
  }

  public IconData(string path, bool[] visibles, params string[] contents)
  {
    this.contents = contents;
    this.image = path;
    this.seticon = (string) null;
    this.visibles = visibles;
  }

  public IconData(string path, params string[] contents)
  {
    this.contents = contents;
    this.image = path;
    this.seticon = (string) null;
    this.visibles = (bool[]) null;
  }

  public IconData(string path, string seticon, int lordTitleId, bool[] visibles, params string[] contents)
  {
    this.contents = contents;
    this.image = path;
    this.seticon = seticon;
    this.lordTitleId = lordTitleId;
    this.visibles = visibles;
  }

  public IconData(string path, string seticon, int lordTitleId, params string[] contents)
  {
    this.contents = contents;
    this.image = path;
    this.seticon = seticon;
    this.lordTitleId = lordTitleId;
    this.visibles = (bool[]) null;
  }

  public object Data { get; set; }
}
