﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryKingItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AnniversaryKingItem : MonoBehaviour
{
  private int _slotIndex = -1;
  public const string IMAGE_PREFIX = "Texture/Hero/player_portrait_";
  public UILabel title;
  public UILabel userName;
  public UITexture playerIcon;
  public GameObject worshipBt;
  public GameObject benefitBt;
  public AllianceSymbol symbol;
  public UILabel worshipCount;
  public UILabel tip;
  private int remainTime;
  public UILabel timeLabel;
  public GameObject challengeBt;
  private AnniversaryManager.AnniversarySlotData _slotData;
  private AnniversaryChampionMainInfo _mainInfo;
  private int _worshipRemain;

  public void SetData(AnniversaryManager.AnniversarySlotData data, AnniversaryChampionMainInfo info)
  {
    AnniversaryManager.AnniversarySlotData slotData = this._slotData;
    this._slotData = data;
    this.CalculateTime(data.Slot);
    this.RefreshTime();
    if (AnniversaryManager.Instance.IsStart())
      this.AddEventHandler();
    if (this._slotData.Uid > 6000000000L)
    {
      this.SetData(info, data.Slot);
    }
    else
    {
      NGUITools.SetActive(this.symbol.gameObject, false);
      if (!string.IsNullOrEmpty(data.AllianceTag))
      {
        NGUITools.SetActive(this.symbol.gameObject, true);
        this.symbol.SetSymbols(data.AllianceSymbol);
      }
      this._slotIndex = data.Slot;
      this.title.text = info.LocalName;
      this._mainInfo = info;
      this.userName.text = this._slotData.Name;
      Color color = new Color(1f, 1f, 0.92f);
      if (this._slotData.Uid == PlayerData.inst.uid)
        color = new Color(1f, 0.8f, 0.0f);
      this.userName.color = color;
      this.worshipCount.text = Utils.FormatShortThousandsLong(data.WorshippedCount);
      if ((UnityEngine.Object) this.benefitBt != (UnityEngine.Object) null)
      {
        NGUITools.SetActive(this.benefitBt.gameObject, this._slotData.Uid == PlayerData.inst.uid);
        NGUITools.SetActive(this.tip.gameObject, false);
      }
      if ((UnityEngine.Object) this.challengeBt != (UnityEngine.Object) null)
        NGUITools.SetActive(this.challengeBt.gameObject, true);
      if (!this.CanWorkship())
        GreyUtility.Grey(this.worshipBt);
      else if (!AnniversaryManager.Instance.IsFinish())
        GreyUtility.Normal(this.worshipBt);
      this.HideButtons();
      UserData.GetPortraitIconPath(data.Portrait);
      if (slotData != null && slotData.Uid == this._slotData.Uid)
        return;
      Utils.SetPortrait(this.playerIcon, "Texture/Hero/player_portrait_" + (object) data.Portrait);
    }
  }

  public void Clear()
  {
    this.RemoveEventHandler();
  }

  private void AddEventHandler()
  {
    this.RemoveEventHandler();
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnProcess);
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnProcess);
  }

  private void CalculateTime(int slotIndex)
  {
    if (!AnniversaryManager.Instance.IsStart())
    {
      this.remainTime = 0;
      if (!((UnityEngine.Object) this.challengeBt != (UnityEngine.Object) null))
        return;
      this.challengeBt.GetComponent<UIButton>().isEnabled = false;
    }
    else
    {
      this._worshipRemain = NetServerTime.inst.ServerTimestamp % 86400;
      int challengeTime = AnniversaryManager.Instance.Data.GetChallengeTime(slotIndex);
      int num = 1800;
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("anniversary_champion_attack_cd");
      if (data != null)
        num = data.ValueInt;
      this.remainTime = num - (NetServerTime.inst.ServerTimestamp - challengeTime);
      if (this.remainTime <= 0 || !((UnityEngine.Object) this.challengeBt != (UnityEngine.Object) null))
        return;
      this.challengeBt.GetComponent<UIButton>().isEnabled = false;
    }
  }

  private void OnProcess(int time)
  {
    --this.remainTime;
    --this._worshipRemain;
    this.RefreshTime();
  }

  private void RefreshTime()
  {
    string str = ScriptLocalization.Get("event_1_year_tournament_challenge_button", true) + " " + Utils.FormatTime(this.remainTime, false, false, true);
    if (this.remainTime <= 0)
    {
      this.RemoveEventHandler();
      this.remainTime = 0;
      if ((UnityEngine.Object) this.challengeBt != (UnityEngine.Object) null)
        this.challengeBt.GetComponent<UIButton>().isEnabled = AnniversaryManager.Instance.IsStart() && !AnniversaryManager.Instance.IsFinish();
      str = ScriptLocalization.Get("event_1_year_tournament_challenge_button", true);
    }
    if (!((UnityEngine.Object) this.timeLabel != (UnityEngine.Object) null))
      return;
    this.timeLabel.text = str;
  }

  public void SetData(AnniversaryChampionMainInfo info, int slotIndex)
  {
    if (AnniversaryManager.Instance.IsStart())
      this.AddEventHandler();
    if (this._slotIndex == slotIndex)
      return;
    this.CalculateTime(slotIndex);
    NGUITools.SetActive(this.symbol.gameObject, false);
    this.RefreshTime();
    this._slotIndex = slotIndex;
    this.title.text = info.LocalName;
    this._mainInfo = info;
    Color color = new Color(1f, 1f, 0.92f);
    this.userName.text = ScriptLocalization.Get("id_mysterious_troops_name", true);
    this.userName.color = color;
    this.worshipCount.text = "0";
    if ((UnityEngine.Object) this.benefitBt != (UnityEngine.Object) null)
      NGUITools.SetActive(this.benefitBt.gameObject, false);
    GreyUtility.Grey(this.worshipBt);
    if ((UnityEngine.Object) this.tip != (UnityEngine.Object) null)
      NGUITools.SetActive(this.tip.gameObject, true);
    this.SetDefaultIcon();
    this.HideButtons();
  }

  public void SysBenefit()
  {
    if (this._slotData.Uid != PlayerData.inst.uid)
      return;
    UIManager.inst.OpenDlg("Anniversary/AnniversarySendMarchDlg", (UI.Dialog.DialogParameter) new AnniversarySendMarchDlg.Parameter()
    {
      isChangellen = false
    }, true, true, true);
  }

  public void OnWorkship()
  {
    if (this._slotData.Uid == PlayerData.inst.uid)
      return;
    int num = 6;
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("anniversary_champion_worship_city_level_limit");
    if (data != null)
      num = data.ValueInt;
    if (PlayerData.inst.CityData.mStronghold.mLevel < num)
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_1_year_tournament_praise_level_low", new Dictionary<string, string>()
      {
        {
          "0",
          num.ToString() + string.Empty
        }
      }, true), (System.Action) null, 4f, false);
    else
      AnniversaryManager.Instance.Workship(this._slotData.Slot);
  }

  public void OnChallenge()
  {
    if (AnniversaryManager.Instance.IsKing())
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      AnniversaryChampionMainInfo data = ConfigManager.inst.DB_AnniversaryChampionMain.GetData(AnniversaryManager.Instance.GetKingSlot(PlayerData.inst.uid).ToString() + string.Empty);
      if (data != null)
        para.Add("1", data.LocalName);
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_1_year_tournament_title_taken", para, true), (System.Action) null, 4f, false);
    }
    else
    {
      int num1 = 6;
      GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("anniversary_champion_challenge_level_min");
      if (data1 != null)
        num1 = data1.ValueInt;
      int mLevel = PlayerData.inst.CityData.mStronghold.mLevel;
      int num2 = this._slotData.CityLevel - mLevel;
      int num3 = 0;
      GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("anniversary_champion_challenge_level_disc_min");
      if (data2 != null)
        num3 = data2.ValueInt;
      if (mLevel < num1 || num2 >= num3 && this._slotData.Uid < 6000000000L)
        UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_1_year_tournament_level_too_low", new Dictionary<string, string>()
        {
          {
            "1",
            num1.ToString()
          },
          {
            "2",
            num3.ToString()
          }
        }, true), (System.Action) null, 4f, false);
      else
        UIManager.inst.OpenPopup("Anniversary/AnniversaryKingDetailPopup", (Popup.PopupParameter) new AnniversaryKingDetailPopup.Parameter()
        {
          slotData = this._slotData,
          mainInfo = this._mainInfo,
          slotIndex = this._slotIndex
        });
    }
  }

  private void SetDefaultIcon()
  {
    Utils.SetPortrait(this.playerIcon, string.Empty);
  }

  private void HideButtons()
  {
    if (this._slotData.Uid == PlayerData.inst.uid)
    {
      NGUITools.SetActive(this.worshipBt.gameObject, false);
      if ((UnityEngine.Object) this.challengeBt != (UnityEngine.Object) null)
        NGUITools.SetActive(this.challengeBt.gameObject, false);
    }
    if ((UnityEngine.Object) this.challengeBt != (UnityEngine.Object) null && AnniversaryManager.Instance.IsFinish())
      NGUITools.SetActive(this.challengeBt.gameObject, false);
    if (this._slotData.Uid == PlayerData.inst.uid && AnniversaryManager.Instance.IsStart() && !AnniversaryManager.Instance.IsFinish())
    {
      if ((UnityEngine.Object) this.benefitBt != (UnityEngine.Object) null)
        NGUITools.SetActive(this.benefitBt.gameObject, true);
    }
    else if ((UnityEngine.Object) this.benefitBt != (UnityEngine.Object) null)
      NGUITools.SetActive(this.benefitBt.gameObject, false);
    int workshipTime = AnniversaryManager.Instance.Data.GetWorkshipTime(this._slotData.Slot);
    int num = NetServerTime.inst.ServerTimestamp / 86400 * 86400;
    if (workshipTime > num)
    {
      NGUITools.SetActive(this.worshipBt.gameObject, false);
      if (workshipTime < AnniversaryManager.Instance.EndTime && AnniversaryManager.Instance.IsFinish())
        NGUITools.SetActive(this.worshipBt.gameObject, true);
    }
    if (this._slotData.Uid <= 6000000000L)
      return;
    NGUITools.SetActive(this.worshipBt.gameObject, false);
  }

  private bool CanWorkship()
  {
    if (this._slotData.Uid == PlayerData.inst.uid)
      return false;
    int workshipTime = AnniversaryManager.Instance.Data.GetWorkshipTime(this._slotData.Slot);
    int num1 = NetServerTime.inst.ServerTimestamp / 86400 * 86400;
    if (workshipTime < num1 || AnniversaryManager.Instance.IsFinish() && workshipTime < AnniversaryManager.Instance.EndTime)
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("anniversary_champion_worship_city_level_limit");
      if (data != null)
        return PlayerData.inst.CityData.mStronghold.mLevel < data.ValueInt;
    }
    int num2 = 6;
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("anniversary_champion_worship_city_level_limit");
    if (data1 != null)
      num2 = data1.ValueInt;
    return PlayerData.inst.CityData.mStronghold.mLevel >= num2;
  }
}
