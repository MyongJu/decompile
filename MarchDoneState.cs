﻿// Decompiled with JetBrains decompiler
// Type: MarchDoneState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MarchDoneState : MarchBaseState
{
  private bool isRemove;

  public override string Key
  {
    get
    {
      return "march_done";
    }
  }

  protected override void Prepare()
  {
    NGUITools.SetActive(this.Controler.gameObject, false);
  }

  private void DoneHandler()
  {
    if (this.isRemove || !GameEngine.Instance.marchSystem.IsReady)
      return;
    this.isRemove = true;
    if (!((Object) this.Controler != (Object) null))
      return;
    this.Controler.owner.Dispose();
  }

  protected override void OnProcessHandler()
  {
    this.DoneHandler();
  }

  protected override bool NeedSetUpMarchView()
  {
    return false;
  }
}
