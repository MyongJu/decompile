﻿// Decompiled with JetBrains decompiler
// Type: AllianceHonourReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class AllianceHonourReward : QuestReward
{
  public readonly int honour;

  public AllianceHonourReward(int inExp)
  {
    this.honour = inExp;
  }

  public int activeValue
  {
    get
    {
      return this.honour;
    }
  }

  public override int GetValue()
  {
    return this.honour;
  }

  public override void Claim()
  {
    base.Claim();
  }

  public override string GetRewardIconName()
  {
    return Utils.GetUITextPath() + "alliancequest_reward_honor";
  }

  public override string GetRewardTypeName()
  {
    return ScriptLocalization.Get("alliance_honor", true);
  }

  public override string GetValueText()
  {
    return this.activeValue.ToString();
  }
}
