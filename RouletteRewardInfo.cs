﻿// Decompiled with JetBrains decompiler
// Type: RouletteRewardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class RouletteRewardInfo
{
  private int rouletteSlotConut = 12;
  private List<RouletteRewardItem> rewardItemList = new List<RouletteRewardItem>();
  private static RouletteRewardInfo _inst;

  public long NeedTalusToSpinOnce
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("casino_roulette_cost");
      if (data != null)
        return (long) data.ValueInt;
      return 0;
    }
  }

  public static RouletteRewardInfo Instance
  {
    get
    {
      if (RouletteRewardInfo._inst == null)
        RouletteRewardInfo._inst = new RouletteRewardInfo();
      return RouletteRewardInfo._inst;
    }
  }

  public void Initialize()
  {
    if (this.rewardItemList != null && this.rewardItemList.Count > 0)
      return;
    for (int index = 0; index < this.rouletteSlotConut; ++index)
      this.rewardItemList.Add(new RouletteRewardItem()
      {
        index = index,
        rewardItemName = string.Empty,
        rewardItemType = "chest",
        groupId = index,
        strongholdRank = PlayerData.inst.CityData.mStronghold.mLevel
      });
    List<CasinoRouletteInfo> rouletteInfoList = ConfigManager.inst.DB_CasinoRoulette.GetCasinoRouletteInfoList();
    if (rouletteInfoList != null && this.rouletteSlotConut < rouletteInfoList.Count)
    {
      for (int index1 = 0; index1 < rouletteInfoList.Count; ++index1)
      {
        int index2 = rouletteInfoList[index1].groupId - 1;
        if (this.rewardItemList != null && this.rewardItemList[index2] != null && rouletteInfoList[index1].minLevel <= this.rewardItemList[index2].strongholdRank)
        {
          this.rewardItemList[index2].groupId = rouletteInfoList[index1].groupId;
          this.rewardItemList[index2].rewardItemName = rouletteInfoList[index1].reward;
          this.rewardItemList[index2].rewardItemType = rouletteInfoList[index1].type;
          this.rewardItemList[index2].rewardItemCount = rouletteInfoList[index1].value;
        }
      }
    }
    if (ConfigManager.inst.DB_CasinoRoulette.buildCasinoDataFlag)
    {
      this.rewardItemList.Sort(new Comparison<RouletteRewardItem>(this.SortByRandom));
      ConfigManager.inst.DB_CasinoRoulette.buildCasinoDataFlag = false;
    }
    this.UpdateRewardItemAngleData(this.rewardItemList);
  }

  public void UpdateRewardItemAngleData(List<RouletteRewardItem> itemList)
  {
    float num1 = 360f;
    float num2 = num1 / (float) this.rouletteSlotConut;
    float num3 = num2 / 2f * -1f;
    for (int index = 0; index < itemList.Count; ++index)
    {
      itemList[index].startAngle = num1 - num3;
      itemList[index].endAngle = itemList[index].startAngle - num2;
      itemList[index].stopAngle = (float) (((double) itemList[index].startAngle + (double) itemList[index].endAngle) / 2.0);
      num3 += num2;
    }
  }

  public List<RouletteRewardItem> GetRouletteRewardItemList()
  {
    if (this.rewardItemList != null && this.rewardItemList.Count > 0)
      return this.rewardItemList;
    return (List<RouletteRewardItem>) null;
  }

  public RouletteRewardItem GetRewardRouletteItem(int index)
  {
    if (this.rewardItemList != null && index < this.rewardItemList.Count)
      return this.rewardItemList[index];
    return (RouletteRewardItem) null;
  }

  public int SortByID(RouletteRewardItem a, RouletteRewardItem b)
  {
    return a.index.CompareTo(b.index);
  }

  public int SortByRandom(RouletteRewardItem a, RouletteRewardItem b)
  {
    int[] numArray = new int[3]{ -1, 0, 1 };
    int index = UnityEngine.Random.Range(0, numArray.Length);
    return numArray[index];
  }
}
