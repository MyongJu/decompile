﻿// Decompiled with JetBrains decompiler
// Type: StrongholdInfoHospitalItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StrongholdInfoHospitalItem : MonoBehaviour
{
  public UILabel mTroopName;
  public UILabel mTroopCount;

  public void SetDetails(string troopName, long troopCount)
  {
    this.mTroopName.text = troopName;
    this.mTroopCount.text = Utils.FormatThousands(troopCount.ToString());
  }
}
