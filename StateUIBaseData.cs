﻿// Decompiled with JetBrains decompiler
// Type: StateUIBaseData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;

public class StateUIBaseData
{
  private string _title;

  public object Data { get; set; }

  public List<RecodeUIData> Recoders { get; set; }

  public List<WonderKingUIData> Kings { get; set; }

  public OccupyUIData Occupyer
  {
    get
    {
      UserData userData = DBManager.inst.DB_User.Get(this.OccupyUserId);
      if (userData == null)
        return (OccupyUIData) null;
      OccupyUIData occupyUiData = new OccupyUIData();
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(this.OccupyAllianceId);
      if (allianceData != null)
      {
        occupyUiData.Symbol = allianceData.allianceSymbolCode;
        occupyUiData.AllianceName = allianceData.allianceName;
      }
      occupyUiData.Icon = userData.Icon;
      occupyUiData.Portrait = userData.portrait;
      occupyUiData.LordTitleId = userData.LordTitle;
      occupyUiData.UserName = userData.userName;
      return occupyUiData;
    }
  }

  private OccupyUIData CreateFakeData()
  {
    return new OccupyUIData()
    {
      Symbol = 10,
      AllianceName = "TestAlliance",
      Portrait = 1,
      UserName = "river~~~"
    };
  }

  public virtual StateUIBaseData.DataType Type
  {
    get
    {
      return StateUIBaseData.DataType.None;
    }
  }

  public string State
  {
    get
    {
      return DBManager.inst.DB_Wonder.Get(this.WorldId).State;
    }
  }

  public string Desc
  {
    get
    {
      string Term = string.Empty;
      Dictionary<string, string> para = new Dictionary<string, string>();
      para.Add("0", Utils.FormatTime(this.RemainTime, false, false, true));
      string state = this.State;
      if (state != null)
      {
        if (StateUIBaseData.\u003C\u003Ef__switch\u0024mapB5 == null)
          StateUIBaseData.\u003C\u003Ef__switch\u0024mapB5 = new Dictionary<string, int>(3)
          {
            {
              "unOpen",
              0
            },
            {
              "protected",
              1
            },
            {
              "fighting",
              2
            }
          };
        int num;
        if (StateUIBaseData.\u003C\u003Ef__switch\u0024mapB5.TryGetValue(state, out num))
        {
          switch (num)
          {
            case 0:
              Term = "throne_preparation_status_time";
              break;
            case 1:
              Term = "throne_protection_status_time";
              break;
            case 2:
              Term = "throne_challenge_status_time";
              break;
          }
        }
      }
      return ScriptLocalization.GetWithPara(Term, para, true);
    }
  }

  public virtual string Title
  {
    get
    {
      return string.Empty;
    }
  }

  public virtual string Image
  {
    get
    {
      return string.Empty;
    }
  }

  public virtual long WorldId
  {
    get
    {
      return 0;
    }
  }

  protected virtual long OccupyUserId
  {
    get
    {
      return 0;
    }
  }

  protected virtual long OccupyAllianceId
  {
    get
    {
      return 0;
    }
  }

  public virtual int RemainTime
  {
    get
    {
      int num = (int) DBManager.inst.DB_Wonder.Get(this.WorldId).StateChangeTime - NetServerTime.inst.ServerTimestamp;
      if (num < 0)
        num = 0;
      return num;
    }
  }

  public enum DataType
  {
    Avalon,
    Tower,
    None,
  }
}
