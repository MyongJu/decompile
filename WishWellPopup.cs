﻿// Decompiled with JetBrains decompiler
// Type: WishWellPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;
using UnityEngine;

public class WishWellPopup : Popup
{
  public const string ITEM_WISH_WELL_GEM = "item_wish_well_gem";
  public UITexture m_GemIcon;
  public UITexture m_Banner;
  public UILabel m_GemCount;
  public UIButton m_AddButton;
  public WishWellItemRenderer[] m_WishWellItems;
  public GameObject m_AnimControlPrefab;
  public GameObject m_RootNode;
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    BuilderFactory.Instance.Build((UIWidget) this.m_Banner, "Texture/GUI_Textures/wishing_well_top_banner", (System.Action<bool>) null, true, false, true, string.Empty);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    BuilderFactory.Instance.Release((UIWidget) this.m_Banner);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    for (int index = 0; index < this.m_WishWellItems.Length; ++index)
      this.m_WishWellItems[index].gameObject.SetActive(false);
    MessageHub.inst.GetPortByAction("Wishing:loadData").SendRequest(new System.Action<bool, object>(this.OnHandlePayload));
  }

  public void OnCloseButtonPressed()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
  }

  public void OnAddButtonPressed()
  {
    UIManager.inst.OpenPopup("UseItemPopup", (Popup.PopupParameter) new UseItemPopup.Parameter()
    {
      itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_wish_well_gem"),
      callback = new System.Action<bool, object>(this.OnHandlePayload)
    });
  }

  private void OnHandlePayload(bool ret, object data)
  {
    if (!ret)
      return;
    WishWellPayload.Instance.Decode(data);
    this.UpdateUI(true);
  }

  private void UpdateUI(bool needResetScrollView = false)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_GemIcon, "Texture/ItemIcons/item_wishing_well_gem", (System.Action<bool>) null, true, false, string.Empty);
    int itemCount = ItemBag.Instance.GetItemCount("item_wish_well_gem");
    this.m_GemCount.text = string.Format("{0} : {1}", (object) ScriptLocalization.Get("id_uppercase_free", true), (object) Utils.FormatThousands(WishWellPayload.Instance.gems.ToString()));
    this.m_AddButton.gameObject.SetActive(WishWellPayload.Instance.gems == 0 && itemCount > 0);
    int[] numArray = new int[5]{ 1, 1, 10, 15, 8 };
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_food");
    if (data1 != null)
      numArray[0] = data1.ValueInt;
    GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_wood");
    if (data2 != null)
      numArray[1] = data2.ValueInt;
    GameConfigInfo data3 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_ore");
    if (data3 != null)
      numArray[2] = data3.ValueInt;
    GameConfigInfo data4 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_silver");
    if (data4 != null)
      numArray[3] = data4.ValueInt;
    GameConfigInfo data5 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_steel");
    if (data5 != null)
      numArray[4] = data5.ValueInt;
    int mLevel = PlayerData.inst.CityData.mStronghold.mLevel;
    for (int index = 0; index < this.m_WishWellItems.Length; ++index)
    {
      WishWellItemRenderer wishWellItem = this.m_WishWellItems[index];
      wishWellItem.UpdateUI(new WishWellItemRenderer.OnWishCallback(this.OnWish));
      wishWellItem.gameObject.SetActive(mLevel >= numArray[index]);
    }
    this.m_Grid.Reposition();
    if (!needResetScrollView)
      return;
    this.m_ScrollView.ResetPosition();
  }

  private void OnWish()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_AnimControlPrefab);
    gameObject.transform.parent = this.m_RootNode.transform;
    gameObject.transform.localScale = Vector3.one;
    int quantity = 0;
    string categoryAndQuantity = WishWellPayload.Instance.GetCategoryAndQuantity(out quantity);
    gameObject.GetComponent<WishWellAnimController>().SetData(categoryAndQuantity, quantity, WishWellPayload.Instance.critical);
    this.UpdateUI(false);
  }
}
