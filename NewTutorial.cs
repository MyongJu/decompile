﻿// Decompiled with JetBrains decompiler
// Type: NewTutorial
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class NewTutorial
{
  private const string dataPath = "TextAsset/TutorialData/source";
  public const string Finished = "finished";
  public const string Beginner = "beginner";
  public const string TESTB_Beginner = "testb_beginner";
  public const string Quest = "Tutorial_quest";
  public const string TESTB_Quest = "testb_Tutorial_quest";
  public const string Military = "Tutorial_military_tent";
  public const string Hospital = "Tutorial_hospital";
  public const string Newsite = "Tutorial_new_site";
  public const string Embassy = "Tutorial_embassy";
  public const string Warrally = "Tutorial_war_rally";
  public const string Wishwell = "Tutorial_wish_well";
  public const string Marketplace = "Tutorial_marketplace";
  public const string JoinAlliance = "Tutorial_join_alliance";
  public const string TalentPoint = "Tutorial_talent_point";
  public const string DragonSkill = "Tutorial_dragon_skill";
  public const string Mail = "Tutorial_mail";
  public const string University = "Tutorial_university";
  public const string Forge = "Tutorial_forge";
  public const string DragonRider = "Tutorial_pre_dragon_rider";
  public const string DragonRiderHole = "Tutorial_hole_dragon_rider";
  public const string DragonRiderMagic = "Tutorial_magic_dragon_rider";
  public const string DragonRiderSkill = "Tutorial_skill_dragon_rider";
  public const string DragonRiderPVP = "Tutorial_pvp_dragon_rider";
  public const string HERO_SUMMON = "Tutorial_hero_summon";
  public const string ActivityCenter = "Tutorial_activity_center";
  public const string MerlinTower = "Tutorial_merlin_tower";
  public const string EarlyJoinAlliance = "testb_early_tutorial_join_alliance";
  public const string EarlyEnterWonder = "testb_early_tutorial_enter_wonder";
  public const string JoinAllianceTutorial = "join_alliance";
  public const string EnterWonder = "wonder";
  private static NewTutorial _instance;
  [DevSetting(null, "General", null)]
  public static bool skipTutorial;
  private List<string> tutorialList;
  private TutorialSource _dataSource;

  private NewTutorial()
  {
    this.tutorialList = new List<string>(5);
  }

  public static NewTutorial Instance
  {
    get
    {
      if (NewTutorial._instance == null)
      {
        NewTutorial._instance = new NewTutorial();
        NewTutorial._instance.DeserializeSource();
      }
      return NewTutorial._instance;
    }
  }

  public TutorialSource DataSource
  {
    get
    {
      return this._dataSource;
    }
    set
    {
      this._dataSource = value;
    }
  }

  public void Dispose()
  {
    this.tutorialList.Clear();
    this.tutorialList = (List<string>) null;
    this._dataSource = (TutorialSource) null;
    NewTutorial._instance = (NewTutorial) null;
  }

  public void InitTutorial(string step)
  {
    this.tutorialList.Clear();
    List<string> stringList;
    if (!this.DataSource.pipeline.TryGetValue(step, out stringList))
      return;
    this.tutorialList.AddRange((IEnumerable<string>) stringList);
  }

  public void LoadTutorialData(string begin)
  {
    if ("finished" == begin || this.DataSource.seqData.Count <= 0)
      return;
    TutorialManager.Instance.DataQueue.Clear();
    string[] bstr = begin.Split('.');
    int index1 = this.tutorialList.FindIndex((Predicate<string>) (m => m.Contains(bstr[0])));
    if (index1 == -1)
      return;
    List<TutorialData> alldata = new List<TutorialData>();
    for (int index2 = index1; index2 < this.tutorialList.Count; ++index2)
    {
      string[] stepname = this.tutorialList[index2].Split('.');
      TutorialSequence tutorialSequence = this.DataSource.seqData.Find((Predicate<TutorialSequence>) (seq => string.Equals(stepname[0], seq.name)));
      if (tutorialSequence != null)
        alldata.AddRange((IEnumerable<TutorialData>) tutorialSequence.tData);
    }
    if (alldata.Count == 0 || TutorialManager.Instance.DataQueue.Find((Predicate<TutorialData>) (m => m.name == alldata[0].name)) != null)
      return;
    TutorialManager.Instance.DataQueue.AddRange((IEnumerable<TutorialData>) alldata);
    TutorialManager.Instance.Startup();
  }

  public void DeserializeSource()
  {
    TextAsset textAsset = AssetManager.Instance.HandyLoad("TextAsset/TutorialData/source", (System.Type) null) as TextAsset;
    this.DataSource = !((UnityEngine.Object) null != (UnityEngine.Object) textAsset) ? new TutorialSource() : JsonReader.Deserialize<TutorialSource>(textAsset.text);
    this.DebugLog();
  }

  public NewTutorialRecord GetNewRecord()
  {
    NewTutorialRecord newTutorialRecord = (NewTutorialRecord) null;
    UserData userData = DBManager.inst.DB_User.Get(PlayerData.inst.uid);
    if (userData.toturialStep != null && userData.toturialStep.Length != 0)
      newTutorialRecord = JsonReader.Deserialize<NewTutorialRecord>(Encoding.Default.GetString(Convert.FromBase64String(userData.toturialStep)));
    if (newTutorialRecord == null)
      newTutorialRecord = new NewTutorialRecord();
    return newTutorialRecord;
  }

  public void SetTutorialProgress(NewTutorialRecord record)
  {
    string base64String = Convert.ToBase64String(Encoding.Default.GetBytes(JsonWriter.Serialize((object) record)));
    Hashtable postData = new Hashtable();
    postData.Add((object) "tutorial", (object) base64String);
    if (record.currentProcess.ContainsKey("beginner"))
    {
      string str = record.currentProcess["beginner"];
      if (TutorialManager.Instance.DataQueue[0].name.Contains("Tutorial_lumber_mill") && str.Contains("Tutorial_barracks"))
      {
        int internalId = ConfigManager.inst.DB_Building.GetData("lumber_mill", 0).internalId;
        postData[(object) "type"] = (object) internalId;
        postData.Add((object) "step", (object) 1000);
      }
      else if (TutorialManager.Instance.DataQueue[0].name.Contains("Tutorial_barracks") && str.Contains("Tutorial_farm"))
      {
        int internalId = ConfigManager.inst.DB_Building.GetData("barracks", 0).internalId;
        postData[(object) "type"] = (object) internalId;
        postData.Add((object) "step", (object) 1001);
      }
      else if (TutorialManager.Instance.DataQueue[0].name.Contains("Tutorial_farm") && str.Contains("Tutorial_troop"))
      {
        int internalId = ConfigManager.inst.DB_Building.GetData("farm", 0).internalId;
        postData[(object) "type"] = (object) internalId;
        postData.Add((object) "step", (object) 1002);
      }
    }
    if (record.currentProcess.ContainsKey("testb_beginner"))
    {
      string str = record.currentProcess["testb_beginner"];
      if (str.Contains("testb_Tutorial_barracks"))
      {
        int internalId = ConfigManager.inst.DB_Building.GetData("lumber_mill", 0).internalId;
        postData[(object) "type"] = (object) internalId;
        postData.Add((object) "step", (object) 1000);
      }
      else if (str.Contains("testb_Tutorial_troop"))
      {
        int internalId = ConfigManager.inst.DB_Building.GetData("barracks", 0).internalId;
        postData[(object) "type"] = (object) internalId;
        postData.Add((object) "step", (object) 1001);
      }
    }
    if (TutorialManager.Instance.DataQueue[0].name.Contains("Tutorial_military_tent"))
      TrackEvent.Instance.Trace("completed_tutorial", TrackEvent.Type.Facebook | TrackEvent.Type.Adjust);
    postData.Add((object) "city_id", (object) CityManager.inst.GetCityID());
    MessageHub.inst.GetPortByAction("Player:setTutorial").SendRequest(postData, (System.Action<bool, object>) null, false);
  }

  public string GetRecordPoint(string step)
  {
    string str = (string) null;
    if (!NewTutorial.Instance.GetNewRecord().currentProcess.TryGetValue(step, out str))
      str = NewTutorial.Instance.DataSource.pipeline[step][0];
    return str;
  }

  public void CheckTrigger(BuildingController bc)
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) bc || bc.mBuildingItem == null || NewTutorial.skipTutorial)
      return;
    if (TutorialManager.Instance.Resolution == TutorialManager.ABTestType.Basic)
    {
      string recordPoint = this.GetRecordPoint("Tutorial_quest");
      if ("finished" != recordPoint && "farm" == bc.mBuildingItem.mType && (bc.mBuildingItem.mLevel == 1 && CitadelSystem.inst.GetBuildingsByType("farm").Count == 2))
      {
        NewTutorial.Instance.InitTutorial("Tutorial_quest");
        NewTutorial.Instance.LoadTutorialData(recordPoint);
      }
    }
    else
    {
      string recordPoint = this.GetRecordPoint("testb_Tutorial_quest");
      if ("finished" != recordPoint && "farm" == bc.mBuildingItem.mType && (bc.mBuildingItem.mLevel == 1 && CitadelSystem.inst.GetBuildingsByType("farm").Count == 2))
      {
        NewTutorial.Instance.InitTutorial("testb_Tutorial_quest");
        NewTutorial.Instance.LoadTutorialData(recordPoint);
      }
    }
    if (TutorialManager.Instance.Resolution == TutorialManager.ABTestType.Basic)
    {
      string recordPoint = this.GetRecordPoint("Tutorial_military_tent");
      if ("finished" != recordPoint && "stronghold" == bc.mBuildingItem.mType && bc.mBuildingItem.mLevel == 2)
      {
        NewTutorial.Instance.InitTutorial("Tutorial_military_tent");
        NewTutorial.Instance.LoadTutorialData(recordPoint);
      }
    }
    string recordPoint1 = this.GetRecordPoint("Tutorial_hospital");
    if ("finished" != recordPoint1 && "stronghold" == bc.mBuildingItem.mType && bc.mBuildingItem.mLevel == 4)
    {
      NewTutorial.Instance.InitTutorial("Tutorial_hospital");
      NewTutorial.Instance.LoadTutorialData(recordPoint1);
    }
    string recordPoint2 = this.GetRecordPoint("Tutorial_new_site");
    if ("finished" != recordPoint2 && "stronghold" == bc.mBuildingItem.mType && bc.mBuildingItem.mLevel == 5)
    {
      NewTutorial.Instance.InitTutorial("Tutorial_new_site");
      NewTutorial.Instance.LoadTutorialData(recordPoint2);
    }
    string begin = this.GetRecordPoint("testb_early_tutorial_join_alliance");
    if (TutorialManager.Instance.EarlyGoalType == TutorialManager.ABTestType.Second && "stronghold" == bc.mBuildingItem.mType && (bc.mBuildingItem.mLevel != TutorialManager.Instance.LastJoinAllianceLevel && bc.mBuildingItem.mLevel >= TutorialManager.Instance.JoinAllianceStrongholdLevel) && PlayerData.inst.allianceData == null)
    {
      NewTutorial.Instance.InitTutorial("testb_early_tutorial_join_alliance");
      if (begin == "finished")
        begin = "testb_early_tutorial_join_alliance.1";
      NewTutorial.Instance.LoadTutorialData(begin);
      TutorialManager.Instance.SendRecordEarlyGoalRequest("join_alliance", new System.Action<bool, object>(this.OnRecordEarlyGoalCallback));
    }
    string recordPoint3 = this.GetRecordPoint("Tutorial_activity_center");
    if ("finished" != recordPoint3 && "stronghold" == bc.mBuildingItem.mType && bc.mBuildingItem.mLevel == 15)
    {
      NewTutorial.Instance.InitTutorial("Tutorial_activity_center");
      NewTutorial.Instance.LoadTutorialData(recordPoint3);
    }
    string recordPoint4 = this.GetRecordPoint("Tutorial_merlin_tower");
    if ("finished" != recordPoint4 && "stronghold" == bc.mBuildingItem.mType && bc.mBuildingItem.mLevel == MerlinTowerPayload.Instance.OpenLevel)
    {
      NewTutorial.Instance.InitTutorial("Tutorial_merlin_tower");
      NewTutorial.Instance.LoadTutorialData(recordPoint4);
    }
    string recordPoint5 = this.GetRecordPoint("Tutorial_embassy");
    if ("finished" != recordPoint5 && "embassy" == bc.mBuildingItem.mType && bc.mBuildingItem.mLevel == 1)
    {
      NewTutorial.Instance.InitTutorial("Tutorial_embassy");
      NewTutorial.Instance.LoadTutorialData(recordPoint5);
    }
    string recordPoint6 = this.GetRecordPoint("Tutorial_war_rally");
    if ("finished" != recordPoint6 && "war_rally" == bc.mBuildingItem.mType && bc.mBuildingItem.mLevel == 1)
    {
      NewTutorial.Instance.InitTutorial("Tutorial_war_rally");
      NewTutorial.Instance.LoadTutorialData(recordPoint6);
    }
    string recordPoint7 = this.GetRecordPoint("Tutorial_wish_well");
    if ("finished" != recordPoint7 && "wish_well" == bc.mBuildingItem.mType && bc.mBuildingItem.mLevel == 1)
    {
      NewTutorial.Instance.InitTutorial("Tutorial_wish_well");
      NewTutorial.Instance.LoadTutorialData(recordPoint7);
    }
    string recordPoint8 = this.GetRecordPoint("Tutorial_marketplace");
    if ("finished" != recordPoint8 && "marketplace" == bc.mBuildingItem.mType && bc.mBuildingItem.mLevel == 1)
    {
      NewTutorial.Instance.InitTutorial("Tutorial_marketplace");
      NewTutorial.Instance.LoadTutorialData(recordPoint8);
    }
    string recordPoint9 = this.GetRecordPoint("Tutorial_join_alliance");
    if ("finished" != recordPoint9 && "walls" == bc.mBuildingItem.mType && bc.mBuildingItem.mLevel == 2)
    {
      NewTutorial.Instance.InitTutorial("Tutorial_join_alliance");
      NewTutorial.Instance.LoadTutorialData(recordPoint9);
    }
    string recordPoint10 = this.GetRecordPoint("Tutorial_talent_point");
    if ("finished" != recordPoint10 && "stronghold" == bc.mBuildingItem.mType && bc.mBuildingItem.mLevel == 3)
    {
      NewTutorial.Instance.InitTutorial("Tutorial_talent_point");
      NewTutorial.Instance.LoadTutorialData(recordPoint10);
    }
    string recordPoint11 = this.GetRecordPoint("Tutorial_mail");
    if ("finished" != recordPoint11 && "military_tent" == bc.mBuildingItem.mType && bc.mBuildingItem.mLevel == 1)
    {
      NewTutorial.Instance.InitTutorial("Tutorial_mail");
      NewTutorial.Instance.LoadTutorialData(recordPoint11);
    }
    string recordPoint12 = this.GetRecordPoint("Tutorial_university");
    if (!("finished" != recordPoint12) || !("university" == bc.mBuildingItem.mType) || bc.mBuildingItem.mLevel != 1)
      return;
    NewTutorial.Instance.InitTutorial("Tutorial_university");
    NewTutorial.Instance.LoadTutorialData(recordPoint12);
  }

  public void TriggerEnterWonderEarlyTutorial()
  {
    string recordPoint = this.GetRecordPoint("testb_early_tutorial_enter_wonder");
    if (TutorialManager.Instance.EarlyGoalType != TutorialManager.ABTestType.Second || PlayerData.inst.CityData.mStronghold.mLevel != TutorialManager.Instance.EnterWonderStrongholdLevel)
      return;
    NewTutorial.Instance.InitTutorial("testb_early_tutorial_enter_wonder");
    NewTutorial.Instance.LoadTutorialData(recordPoint);
    TutorialManager.Instance.SendRecordEarlyGoalRequest("wonder", (System.Action<bool, object>) null);
  }

  private void OnRecordEarlyGoalCallback(bool ret, object data)
  {
    if (!ret)
      return;
    TutorialManager.Instance.LastJoinAllianceLevel = PlayerData.inst.CityData.mStronghold.mLevel;
  }

  public void DebugLog()
  {
  }
}
