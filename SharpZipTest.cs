﻿// Decompiled with JetBrains decompiler
// Type: SharpZipTest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using ICSharpCode.SharpZipLib.Zip;
using System;
using System.IO;
using UnityEngine;

public class SharpZipTest : MonoBehaviour
{
  private void Start()
  {
    SharpZipUtil.ZipConfigs("config.zip");
  }

  public void ZipFiles()
  {
    try
    {
      string[] files = Directory.GetFiles(Application.streamingAssetsPath);
      using (ZipOutputStream zipOutputStream = new ZipOutputStream((Stream) File.Create(Application.streamingAssetsPath + "/data.zip")))
      {
        zipOutputStream.SetLevel(9);
        byte[] numArray = new byte[4096];
        foreach (string path in files)
        {
          ZipEntry entry = new ZipEntry(Path.GetFileName(path));
          entry.set_DateTime(DateTime.Now);
          zipOutputStream.PutNextEntry(entry);
          using (FileStream fileStream = File.OpenRead(path))
          {
            int count;
            do
            {
              count = fileStream.Read(numArray, 0, numArray.Length);
              zipOutputStream.Write(numArray, 0, count);
            }
            while (count > 0);
          }
        }
        zipOutputStream.Finish();
        zipOutputStream.Close();
      }
    }
    catch (Exception ex)
    {
    }
  }

  public bool UnZipFile(string zipFilePath, string unZipDir, out string err)
  {
    err = string.Empty;
    if (zipFilePath == string.Empty)
    {
      err = "压缩文件不能为空！";
      return false;
    }
    if (!File.Exists(zipFilePath))
    {
      err = "压缩文件不存在！";
      return false;
    }
    try
    {
      using (ZipInputStream zipInputStream = new ZipInputStream((Stream) File.OpenRead(zipFilePath)))
      {
label_16:
        ZipEntry nextEntry;
        while ((nextEntry = zipInputStream.GetNextEntry()) != null)
        {
          string directoryName = Path.GetDirectoryName(nextEntry.Name);
          string fileName = Path.GetFileName(nextEntry.Name);
          if (directoryName.Length > 0)
            Directory.CreateDirectory(unZipDir + directoryName);
          if (fileName != string.Empty)
          {
            using (FileStream fileStream = File.Create(Path.Combine(unZipDir, nextEntry.Name)))
            {
              byte[] numArray = new byte[2048];
              while (true)
              {
                int count = zipInputStream.Read(numArray, 0, numArray.Length);
                if (count > 0)
                  fileStream.Write(numArray, 0, count);
                else
                  goto label_16;
              }
            }
          }
        }
      }
    }
    catch (Exception ex)
    {
      err = ex.Message;
      return false;
    }
    return true;
  }

  private void Update()
  {
  }
}
