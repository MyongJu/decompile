﻿// Decompiled with JetBrains decompiler
// Type: AllianceRewardInstructionPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceRewardInstructionPopup : Popup
{
  private List<AllianceRewardGroupRender> grouplist = new List<AllianceRewardGroupRender>();
  private const string TITLE = "alliance_warfare_reward_rules_title";
  private const string TIP_DIS = "alliance_warfare_reward_rules_description";
  public UILabel popupTitle;
  public UILabel tip;
  public UITable table;
  public UIScrollView scrollView;
  public GameObject groupTemplate;
  private List<AllianceRewardsInfo> dataList;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    this.dataList = ConfigManager.inst.DB_AllianceWarRewards.GetDatas();
    if (this.dataList == null)
      return;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.popupTitle.text = Utils.XLAT("alliance_warfare_reward_rules_title");
    this.tip.text = Utils.XLAT("alliance_warfare_reward_rules_description");
    this.UpdateView();
    this.scrollView.ResetPosition();
  }

  private void UpdateView()
  {
    for (int index1 = 0; index1 < this.dataList.Count; ++index1)
    {
      GameObject gameObject = Object.Instantiate<GameObject>(this.groupTemplate);
      gameObject.transform.parent = this.table.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localScale = Vector3.one;
      gameObject.SetActive(true);
      AllianceRewardGroupRender component = gameObject.GetComponent<AllianceRewardGroupRender>();
      AllianceRewardGroupData data = new AllianceRewardGroupData();
      data.rankMin = this.dataList[index1].rank_min;
      data.rankMax = this.dataList[index1].rank_max;
      data.fund = this.dataList[index1].fund;
      data.honor = this.dataList[index1].honor;
      Dictionary<ItemStaticInfo, int> dictionary1 = new Dictionary<ItemStaticInfo, int>();
      Dictionary<ItemStaticInfo, int> dictionary2 = new Dictionary<ItemStaticInfo, int>();
      List<Reward.RewardsValuePair> rewards1 = this.dataList[index1].allianceRewards.GetRewards();
      for (int index2 = 0; index2 < rewards1.Count; ++index2)
      {
        Reward.RewardsValuePair rewardsValuePair = rewards1[index2];
        if (rewardsValuePair.internalID > 0 && rewardsValuePair.value > 0)
        {
          ItemStaticInfo key = ConfigManager.inst.DB_Items.GetItem(rewardsValuePair.internalID);
          int num = rewardsValuePair.value;
          if (key != null)
            dictionary1.Add(key, num);
        }
      }
      data.allianceRewards = dictionary1;
      List<Reward.RewardsValuePair> rewards2 = this.dataList[index1].individualRewards.GetRewards();
      for (int index2 = 0; index2 < rewards2.Count; ++index2)
      {
        Reward.RewardsValuePair rewardsValuePair = rewards2[index2];
        if (rewardsValuePair.internalID > 0 && rewardsValuePair.value > 0)
        {
          ItemStaticInfo key = ConfigManager.inst.DB_Items.GetItem(rewardsValuePair.internalID);
          int num = rewardsValuePair.value;
          if (key != null)
            dictionary2.Add(key, num);
        }
      }
      data.individualRewards = dictionary2;
      component.FeedData(data);
      this.grouplist.Add(component);
    }
    this.table.Reposition();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.ClearData();
  }

  private void ClearData()
  {
    for (int index = 0; index < this.grouplist.Count; ++index)
    {
      this.grouplist[index].gameObject.transform.parent = (Transform) null;
      this.grouplist[index].ClearData();
      this.grouplist[index].gameObject.SetActive(false);
      Object.Destroy((Object) this.grouplist[index].gameObject);
    }
    this.grouplist.Clear();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }
}
