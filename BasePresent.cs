﻿// Decompiled with JetBrains decompiler
// Type: BasePresent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;

public class BasePresent
{
  private int _questIconType = 1;
  public const int ICON_UISPRITE = 0;
  public const int ICON_UITEXTURE = 1;
  public const int ICON_GAMEOBJECT = 2;
  private bool _upgrade;
  private bool _needDisplayCount;

  public PresentData Data { get; set; }

  protected float ProgressValue { get; set; }

  protected string ProgressContent { get; set; }

  public int ConfigID { get; set; }

  public long DataID { get; set; }

  public virtual int IconType
  {
    get
    {
      return this._questIconType;
    }
  }

  public string IconPath
  {
    get
    {
      string str = string.Empty;
      switch (this.IconType)
      {
        case 1:
          str = this.TexturePath + this.Image;
          break;
        case 2:
          str = this.BuildIconPath + this.Image;
          break;
      }
      return str;
    }
  }

  public string Image
  {
    get
    {
      return this.Data.Image;
    }
  }

  public virtual string TexturePath
  {
    get
    {
      return this.Data.ImageParentPath;
    }
  }

  public string BuildIconPath
  {
    get
    {
      return "Prefab/UI/BuildingPrefab/ui_";
    }
  }

  public virtual bool IsUpgarde
  {
    get
    {
      return this._upgrade;
    }
  }

  public int Count
  {
    get
    {
      return this.Data.Count;
    }
  }

  public virtual bool NeedDisplayCount
  {
    get
    {
      return this._needDisplayCount;
    }
  }

  public string GetRewardIcon()
  {
    string empty = string.Empty;
    return this.Data.RewardIcon;
  }

  public float GetProgressBarValue()
  {
    return this.ProgressValue;
  }

  public virtual void Refresh()
  {
    this.ProgressContent = "0/1";
    this.ProgressValue = 0.0f;
    if (!this.IsFinish)
      return;
    this.ProgressValue = 1f;
    this.ProgressContent = "1/1";
  }

  public bool IsFinish
  {
    get
    {
      return PresentDataFactory.Instance.IsFinish(this.Data);
    }
  }

  public virtual string Formula
  {
    get
    {
      return this.Data.Formula;
    }
  }

  protected void UseStats()
  {
    StatsData statsData = DBManager.inst.DB_Stats.Get(this.Formula);
    int count = this.Count;
    int num = 0;
    if (statsData != null)
      num = (int) statsData.count;
    if (num > count)
      num = count;
    this.ProgressValue = (float) num / (float) count;
    this.ProgressContent = num.ToString() + "/" + (object) count;
  }

  public string GetProgressContent()
  {
    return this.ProgressContent;
  }

  public static BasePresent Create(string category, int configId, long questId)
  {
    string key = category;
    BasePresent basePresent;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (BasePresent.\u003C\u003Ef__switch\u0024mapA6 == null)
      {
        // ISSUE: reference to a compiler-generated field
        BasePresent.\u003C\u003Ef__switch\u0024mapA6 = new Dictionary<string, int>(25)
        {
          {
            "build",
            0
          },
          {
            "train",
            1
          },
          {
            "pve",
            2
          },
          {
            "pvp",
            3
          },
          {
            "research",
            4
          },
          {
            "alliance_join",
            5
          },
          {
            "alliance_help",
            5
          },
          {
            "resource_collect",
            6
          },
          {
            "resource_rate",
            6
          },
          {
            "resource",
            6
          },
          {
            "legend_recruit",
            7
          },
          {
            "legend_skill_up",
            7
          },
          {
            "legend_xp_item",
            7
          },
          {
            "legend_quest",
            8
          },
          {
            "dragon",
            9
          },
          {
            "place",
            10
          },
          {
            "wish_well",
            11
          },
          {
            "wish",
            11
          },
          {
            "lord",
            12
          },
          {
            "equip",
            13
          },
          {
            "cure_wounded",
            14
          },
          {
            "tele",
            15
          },
          {
            "reward",
            15
          },
          {
            "wounded",
            15
          },
          {
            "dragon_knight_arena",
            16
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (BasePresent.\u003C\u003Ef__switch\u0024mapA6.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            basePresent = (BasePresent) new QuestBuildPresent();
            goto label_23;
          case 1:
            basePresent = (BasePresent) new QuestTrainPresent();
            goto label_23;
          case 2:
            basePresent = (BasePresent) new QuestPVEPresent();
            goto label_23;
          case 3:
            basePresent = (BasePresent) new QuestPVPPresent();
            goto label_23;
          case 4:
            basePresent = (BasePresent) new QuestResearchPresent();
            goto label_23;
          case 5:
            basePresent = (BasePresent) new QuestAllaincePresent();
            goto label_23;
          case 6:
            basePresent = (BasePresent) new QuestResourcePresent();
            goto label_23;
          case 7:
            basePresent = (BasePresent) new QuestLegendPresent();
            goto label_23;
          case 8:
            basePresent = (BasePresent) new AllianceQuestUIPresent();
            goto label_23;
          case 9:
            basePresent = (BasePresent) new DragonBasePresent();
            goto label_23;
          case 10:
            basePresent = (BasePresent) new PlaceBasePresent();
            goto label_23;
          case 11:
            basePresent = (BasePresent) new WishBasePresent();
            goto label_23;
          case 12:
            basePresent = (BasePresent) new LordBasePresent();
            goto label_23;
          case 13:
            basePresent = (BasePresent) new EquipBasePresent();
            goto label_23;
          case 14:
            basePresent = (BasePresent) new QuestHealPresent();
            goto label_23;
          case 15:
            basePresent = (BasePresent) new UseStatsPresent();
            goto label_23;
          case 16:
            basePresent = (BasePresent) new DragonKnightPresent();
            goto label_23;
        }
      }
    }
    basePresent = new BasePresent();
label_23:
    if (basePresent != null)
    {
      basePresent.ConfigID = configId;
      basePresent.DataID = questId;
    }
    return basePresent;
  }
}
