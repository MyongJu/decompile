﻿// Decompiled with JetBrains decompiler
// Type: ExchangeHallInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ExchangeHallInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "item_id")]
  public int itemId;
  [Config(Name = "item_type")]
  public string itemType;
  [Config(Name = "para1")]
  public string param1;
  [Config(Name = "sort_1_id")]
  public int catalog1stId;
  [Config(Name = "sort_2_id")]
  public int catalog2ndId;
  [Config(Name = "sort_3_id")]
  public int catalog3rdId;
  [Config(Name = "freeze_time")]
  public int freezeTime;
  [Config(Name = "price")]
  public float itemPrice;
  [Config(Name = "price_scale_1")]
  public float itemPriceScale1;
  [Config(Name = "price_scale_2")]
  public float itemPriceScale2;
  [Config(Name = "price_scale_3")]
  public float itemPriceScale3;
  [Config(Name = "price_scale_4")]
  public float itemPriceScale4;
  [Config(Name = "price_scale_5")]
  public float itemPriceScale5;
  [Config(Name = "price_scale_6")]
  public float itemPriceScale6;
  [Config(Name = "price_scale_7")]
  public float itemPriceScale7;
  [Config(Name = "price_scale_8")]
  public float itemPriceScale8;
  [Config(Name = "price_scale_9")]
  public float itemPriceScale9;
  [Config(Name = "price_scale_10")]
  public float itemPriceScale10;
  [Config(Name = "price_scale_11")]
  public float itemPriceScale11;

  public float[] PriceScaleArray
  {
    get
    {
      return new float[11]
      {
        this.itemPriceScale1,
        this.itemPriceScale2,
        this.itemPriceScale3,
        this.itemPriceScale4,
        this.itemPriceScale5,
        this.itemPriceScale6,
        this.itemPriceScale7,
        this.itemPriceScale8,
        this.itemPriceScale9,
        this.itemPriceScale10,
        this.itemPriceScale11
      };
    }
  }

  public int Quality
  {
    get
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.itemId);
      if (itemStaticInfo != null)
        return itemStaticInfo.Quality;
      return 0;
    }
  }

  public bool IsEquipment
  {
    get
    {
      return this.itemType == "equipment" || this.itemType == "dk_equipment";
    }
  }

  public int RequireLevel
  {
    get
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.itemId);
      if (itemStaticInfo == null)
        return 0;
      if (!this.IsEquipment)
        return itemStaticInfo.RequireLev;
      BagType bagType = TradingHallPayload.Instance.GetBagType(this.internalId);
      switch (bagType)
      {
        case BagType.Hero:
        case BagType.DragonKnight:
          if (itemStaticInfo != null)
          {
            ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(itemStaticInfo.ID);
            if (data != null)
              return data.heroLevelMin;
            break;
          }
          break;
      }
      return 0;
    }
  }

  public int Enhanced
  {
    get
    {
      int result = 0;
      if (this.IsEquipment)
        int.TryParse(this.param1, out result);
      return result;
    }
  }

  public int Prority
  {
    get
    {
      int result = 0;
      int.TryParse(this.id, out result);
      return result;
    }
  }
}
