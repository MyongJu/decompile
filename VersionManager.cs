﻿// Decompiled with JetBrains decompiler
// Type: VersionManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class VersionManager
{
  private string[] bundleVersionFileNames = new string[2]
  {
    "BundleVersionList_R.json",
    "BundleVersionList_G.json"
  };
  private string[] bundleVersionFullFileNames = new string[2];
  private string[] assetConfigVersions = new string[2]
  {
    "assetsconfig_r.json",
    "assetsconfig_g.json"
  };
  private string[] assetConfigFullNames = new string[2];
  private Dictionary<string, bool> assetConfigIsFromRemote = new Dictionary<string, bool>();
  private Dictionary<string, bool> bundleVersionIsFromRemote = new Dictionary<string, bool>();
  private Dictionary<string, string> newversionlist = new Dictionary<string, string>();
  private Dictionary<string, string> bundleVersionDict = new Dictionary<string, string>();
  private AssetConfigData asset2Bundle = new AssetConfigData();
  private const int BundleVersionCount = 2;
  private static VersionManager _instance;
  private string[] bundleVersions;
  public System.Action<bool> onVersionLoadFinished;

  public static VersionManager Instance
  {
    get
    {
      if (VersionManager._instance == null)
      {
        VersionManager._instance = new VersionManager();
        if (VersionManager._instance == null)
          throw new ArgumentException("fail to create VersionManager");
      }
      return VersionManager._instance;
    }
  }

  private void MarkAssetConfigLoadFromRemote(string assetConfig, bool isLoadFromRemote)
  {
    if (this.assetConfigIsFromRemote.ContainsKey(assetConfig))
      this.assetConfigIsFromRemote.Remove(assetConfig);
    this.assetConfigIsFromRemote.Add(assetConfig, isLoadFromRemote);
  }

  private bool IsAssetConfigFromRemote(string assetConfig)
  {
    bool flag = false;
    if (!this.assetConfigIsFromRemote.TryGetValue(assetConfig, out flag))
      D.error((object) string.Format("the assetConfig file:{0} must be contained in assetConfigIsFromRemote", (object) assetConfig));
    return flag;
  }

  private void MarkBundleVersionLoadFromRemote(string bundleVersion, bool isLoadFromRemote)
  {
    if (this.bundleVersionIsFromRemote.ContainsKey(bundleVersion))
      this.bundleVersionIsFromRemote.Remove(bundleVersion);
    this.bundleVersionIsFromRemote.Add(bundleVersion, isLoadFromRemote);
  }

  private bool IsBundleVersionFromRemote(string bundleVersion)
  {
    bool flag = false;
    if (!this.bundleVersionIsFromRemote.TryGetValue(bundleVersion, out flag))
      D.error((object) string.Format("the bundleVersion file:{0} must be contained in bundleVersionIsFromRemote", (object) bundleVersion));
    return flag;
  }

  public void Init()
  {
    if (!Directory.Exists(AssetConfig.BundleCacheDir))
      Directory.CreateDirectory(AssetConfig.BundleCacheDir);
    this.bundleVersions = new string[2]
    {
      NetApi.inst.BundlesVersionR,
      NetApi.inst.BundlesVersionG
    };
    this.newversionlist.Add("ConfigVersionList.json", NetApi.inst.ConfigFilesVersion);
    this.newversionlist.Add("BundleVersionList_R.json", NetApi.inst.BundlesVersionR);
    this.newversionlist.Add("BundleVersionList_G.json", NetApi.inst.BundlesVersionG);
    this.LoadOrUpdateBundleVersionList();
  }

  public AssetConfigData Asset2Bundle
  {
    get
    {
      return this.asset2Bundle;
    }
  }

  public Dictionary<string, string> Newversionlist
  {
    get
    {
      return this.newversionlist;
    }
  }

  private void CheckBundleHotfix()
  {
    string str1 = Utils.Object2Json(NetApi.inst.HotfixBundleContent);
    if (string.IsNullOrEmpty(str1) || str1 == "null")
      return;
    using (Dictionary<string, Dictionary<string, string>>.Enumerator enumerator1 = JsonReader.Deserialize<Dictionary<string, Dictionary<string, string>>>(str1).GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        KeyValuePair<string, Dictionary<string, string>> current1 = enumerator1.Current;
        string key1 = current1.Key;
        using (Dictionary<string, string>.Enumerator enumerator2 = current1.Value.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            KeyValuePair<string, string> current2 = enumerator2.Current;
            string key2 = current2.Key;
            string str2 = current2.Value;
            if (this.bundleVersionDict.ContainsKey(key2))
              this.bundleVersionDict[key2] = key1;
            else
              this.bundleVersionDict.Add(key2, key1);
            if (this.newversionlist.ContainsKey(key2))
              this.newversionlist[key2] = str2;
            else
              this.newversionlist.Add(key2, str2);
          }
        }
      }
    }
  }

  private void LoadOrUpdateAssetsConfig()
  {
    this.CheckBundleHotfix();
    CacheManager.Instance.Init();
    LoaderBatch batch = LoaderManager.inst.PopBatch();
    for (int index = 0; index < this.assetConfigVersions.Length; ++index)
    {
      string version = this.GetVersion(this.assetConfigVersions[index], VersionType.Config);
      bool flag = CacheManager.Instance.IsCached(this.assetConfigVersions[index], version);
      if (!flag)
      {
        batch.Add(this.assetConfigVersions[index], NetApi.inst.BuildBundleUrlByVersion("assetsconfig.json", this.bundleVersions[index]), (Hashtable) null, false, 3, false);
      }
      else
      {
        string fileName = this.GetVersion(this.assetConfigVersions[index], VersionType.Config) + (object) '=' + this.assetConfigVersions[index];
        this.assetConfigFullNames[index] = AssetConfig.BundleCacheDir + "/" + fileName;
        batch.Add(this.assetConfigVersions[index], NetApi.inst.BuildLocalBundleUrl(fileName), (Hashtable) null, false, 3, false);
      }
      this.MarkAssetConfigLoadFromRemote(this.assetConfigVersions[index], !flag);
    }
    batch.finishedEvent += new System.Action<LoaderBatch>(this.OnLoadOrUpdateAssetsConfig);
    LoaderManager.inst.Add(batch, false, false);
  }

  private void OnLoadOrUpdateAssetsConfig(LoaderBatch batch)
  {
    batch.finishedEvent -= new System.Action<LoaderBatch>(this.OnLoadOrUpdateAssetsConfig);
    int num = 0;
    for (int index = 0; index < this.bundleVersions.Length; ++index)
    {
      bool flag = this.IsAssetConfigFromRemote(this.assetConfigVersions[index]);
      LoaderInfo resultInfo = batch.GetResultInfo(this.assetConfigVersions[index]);
      if (flag)
      {
        if (resultInfo != null && resultInfo.ResData != null)
        {
          string str = Utils.ByteArray2String(resultInfo.ResData);
          if (this.asset2Bundle != null)
            this.asset2Bundle.Merge(JsonReader.Deserialize<AssetConfigData>(str));
          CacheManager.Instance.WriteToCache(this.assetConfigVersions[index], resultInfo.ResData, VersionType.Config);
          ++num;
          if (num == 2 && this.onVersionLoadFinished != null)
            this.onVersionLoadFinished(true);
        }
        else
        {
          D.error((object) "update assets config failed!");
          NetWorkDetector.Instance.RetryTip(new System.Action(this.LoadOrUpdateAssetsConfig), ScriptLocalization.Get("data_error_title", true), ScriptLocalization.Get("data_error_version_description", true));
          break;
        }
      }
      else
      {
        if (resultInfo != null)
        {
          if (resultInfo.ResData != null)
          {
            try
            {
              string str = Utils.ByteArray2String(resultInfo.ResData);
              if (this.asset2Bundle != null)
                this.asset2Bundle.Merge(JsonReader.Deserialize<AssetConfigData>(str));
            }
            catch
            {
              if (File.Exists(this.assetConfigFullNames[index]))
              {
                File.Delete(this.assetConfigFullNames[index]);
                this.LoadOrUpdateAssetsConfig();
                break;
              }
            }
            ++num;
            if (num == 2 && this.onVersionLoadFinished != null)
            {
              this.onVersionLoadFinished(true);
              continue;
            }
            continue;
          }
        }
        D.error((object) "load assets config failed!");
      }
    }
  }

  private void LoadOrUpdateBundleVersionList()
  {
    LoaderBatch batch = LoaderManager.inst.PopBatch();
    for (int index = 0; index < this.bundleVersions.Length; ++index)
    {
      this.bundleVersionFullFileNames[index] = AssetConfig.BundleCacheDir + "/" + this.bundleVersions[index] + (object) '=' + this.bundleVersionFileNames[index];
      bool flag = File.Exists(this.bundleVersionFullFileNames[index]);
      if (!flag)
        batch.Add(this.bundleVersionFileNames[index], NetApi.inst.BuildBundleUrlByVersion("VersionList.json", this.bundleVersions[index]), (Hashtable) null, false, 3, false);
      else
        batch.Add(this.bundleVersionFileNames[index], NetApi.inst.BuildLocalBundleUrl(this.bundleVersions[index] + (object) '=' + this.bundleVersionFileNames[index]), (Hashtable) null, false, 3, false);
      this.MarkBundleVersionLoadFromRemote(this.bundleVersionFileNames[index], !flag);
    }
    batch.finishedEvent += new System.Action<LoaderBatch>(this.OnLoadOrUpdateBundleVersionList);
    LoaderManager.inst.Add(batch, false, false);
  }

  private void OnLoadOrUpdateBundleVersionList(LoaderBatch batch)
  {
    batch.finishedEvent -= new System.Action<LoaderBatch>(this.OnLoadOrUpdateBundleVersionList);
    string str1 = string.Empty;
    string empty = string.Empty;
    for (int index = 0; index < this.bundleVersionFileNames.Length; ++index)
    {
      str1 = this.bundleVersions[index] + (object) '=';
      LoaderInfo resultInfo = batch.GetResultInfo(this.bundleVersionFileNames[index]);
      if (this.IsBundleVersionFromRemote(this.bundleVersionFileNames[index]))
      {
        if (resultInfo != null && resultInfo.ResData != null)
        {
          using (Dictionary<string, string>.Enumerator enumerator = JsonReader.Deserialize<Dictionary<string, string>>(Utils.ByteArray2String(resultInfo.ResData)).GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              KeyValuePair<string, string> current = enumerator.Current;
              if (!this.newversionlist.ContainsKey(current.Key))
              {
                string str2 = !current.Key.ToLower().Equals("assetsconfig.json".ToLower()) ? string.Empty : this.assetConfigVersions[index];
                this.newversionlist.Add(!string.IsNullOrEmpty(str2) ? str2 : current.Key, current.Value);
                if (current.Key.Contains(".assetbundle"))
                  this.bundleVersionDict.Add(current.Key, this.bundleVersions[index]);
              }
              else
                break;
            }
          }
          Utils.WriteFile(this.bundleVersionFullFileNames[index], resultInfo.ResData);
        }
        else
        {
          D.error((object) "updated bundle version list failed!");
          NetWorkDetector.Instance.RetryTip(new System.Action(this.LoadOrUpdateBundleVersionList), ScriptLocalization.Get("data_error_title", true), ScriptLocalization.Get("data_error_version_description", true));
          return;
        }
      }
      else if (resultInfo != null && resultInfo.ResData != null)
      {
        Dictionary<string, string> dictionary = (Dictionary<string, string>) null;
        try
        {
          dictionary = JsonReader.Deserialize<Dictionary<string, string>>(Utils.ByteArray2String(resultInfo.ResData));
        }
        catch
        {
          if (File.Exists(this.bundleVersionFullFileNames[index]))
          {
            File.Delete(this.bundleVersionFullFileNames[index]);
            this.LoadOrUpdateBundleVersionList();
            return;
          }
        }
        using (Dictionary<string, string>.Enumerator enumerator = dictionary.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, string> current = enumerator.Current;
            if (!this.newversionlist.ContainsKey(current.Key))
            {
              string str2 = !current.Key.ToLower().Equals("assetsconfig.json".ToLower()) ? string.Empty : this.assetConfigVersions[index];
              this.newversionlist.Add(!string.IsNullOrEmpty(str2) ? str2 : current.Key, current.Value);
              if (current.Key.Contains(".assetbundle"))
                this.bundleVersionDict.Add(current.Key, this.bundleVersions[index]);
            }
            else
              break;
          }
        }
      }
      else
        D.error((object) "load bundle version list failed!");
    }
    this.LoadOrUpdateAssetsConfig();
  }

  public string GetVersion(string name, VersionType type)
  {
    if (name == null)
      return (string) null;
    if (this.newversionlist.Count == 0 && AssetManager.IsLoadAssetFromBundle)
    {
      D.error((object) "version list init error");
      return (string) null;
    }
    string str;
    this.newversionlist.TryGetValue(name, out str);
    return str;
  }

  public string GetBundleVersionHead(string bundleName)
  {
    if (this.bundleVersionDict.ContainsKey(bundleName))
      return this.bundleVersionDict[bundleName];
    return string.Empty;
  }

  public void Dispose()
  {
    this.newversionlist.Clear();
    VersionManager._instance = (VersionManager) null;
  }
}
