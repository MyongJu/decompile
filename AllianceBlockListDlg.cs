﻿// Decompiled with JetBrains decompiler
// Type: AllianceBlockListDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceBlockListDlg : UI.Dialog
{
  public AllianceBlockListDlg.MembersSearchPage mMembersSearchPage = new AllianceBlockListDlg.MembersSearchPage();
  public AllianceBlockListDlg.BlockListPage mMemberBlockListPage = new AllianceBlockListDlg.BlockListPage();
  public PageTabsMonitor mTabs;
  public GameObject mSearchPage;
  public GameObject mBlockPage;

  public void OnBack()
  {
    this.OnCancel();
  }

  public void OnCancel()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void OnTabPressed(int index)
  {
    switch (index)
    {
      case 0:
        this.OnSearchMembersTabPressed();
        break;
      case 1:
        this.OnBlockedMembersTabPressed();
        break;
    }
  }

  private void OnSearchMembersTabPressed()
  {
    this.mSearchPage.SetActive(true);
    this.mBlockPage.SetActive(false);
  }

  private void ClearSearchList()
  {
    using (List<GameObject>.Enumerator enumerator = this.mMembersSearchPage.mSearchResultList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.mMembersSearchPage.mSearchResultList.Clear();
  }

  public void OnSearchInputChanged()
  {
    this.mMembersSearchPage.mSearchButton.isEnabled = this.mMembersSearchPage.mSearchInput.value.Length >= 3;
  }

  public void OnSearch()
  {
    this.ClearSearchList();
    string str = this.mMembersSearchPage.mSearchInput.value;
    Hashtable postData = new Hashtable();
    postData[(object) "context"] = (object) str;
    postData[(object) "type"] = (object) "banned";
    MessageHub.inst.GetPortByAction("Alliance:searchUserByAlliance").SendRequest(postData, new System.Action<bool, object>(this.AllianceMemberListCallback), true);
  }

  private void AllianceMemberListCallback(bool ret, object data)
  {
    Hashtable hashtable = data as Hashtable;
    if (!hashtable.ContainsKey((object) "res"))
      return;
    ArrayList arrayList = hashtable[(object) "res"] as ArrayList;
    if (arrayList == null)
      return;
    foreach (Hashtable inData in arrayList)
    {
      AllianceBlockUnblockItem component = Utils.DuplicateGOB(this.mMembersSearchPage.mSearchItemPrefab).GetComponent<AllianceBlockUnblockItem>();
      this.mMembersSearchPage.mSearchResultList.Add(component.gameObject);
      component.gameObject.SetActive(true);
      component.onBlockBtnPressed += new System.Action<int>(this.OnBlockMemberPressed);
      int outData1 = 0;
      DatabaseTools.UpdateData(inData, "uid", ref outData1);
      string empty = string.Empty;
      DatabaseTools.UpdateData(inData, "name", ref empty);
      int outData2 = 0;
      DatabaseTools.UpdateData(inData, "power", ref outData2);
      int outData3 = 1;
      DatabaseTools.UpdateData(inData, "level", ref outData3);
      int outData4 = 0;
      DatabaseTools.UpdateData(inData, "portrait", ref outData4);
      int outData5 = 0;
      DatabaseTools.UpdateData(inData, "lord_title", ref outData5);
      component.SetDetails(outData1, empty, outData4, outData2, outData3, string.Empty, outData5);
    }
    this.mMembersSearchPage.mTable.Reposition();
    this.mMembersSearchPage.mScrollView.ResetPosition();
    this.mMembersSearchPage.mNoDataItemPrefab.SetActive(arrayList.Count == 0);
    this.mMembersSearchPage.mNoDataLabel.text = "Nobody matches your search";
  }

  private void OnBlockMemberPressed(int memberID)
  {
    MessageHub.inst.GetPortByAction("Alliance:banByAdmin").SendRequest(new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) PlayerData.inst.allianceId
      },
      {
        (object) "banned_uid",
        (object) memberID
      },
      {
        (object) "operate",
        (object) "ban"
      }
    }, new System.Action<bool, object>(this.OnBanCallback), true);
  }

  private void OnBanCallback(bool ret, object data)
  {
    if (!ret)
      ;
  }

  public void OnBlockedMembersTabPressed()
  {
    this.mSearchPage.SetActive(false);
    this.mBlockPage.SetActive(true);
    this.RefreshBlockedMembersList();
  }

  private void RefreshBlockedMembersList()
  {
    using (List<GameObject>.Enumerator enumerator = this.mMemberBlockListPage.mSearchResultList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.mMemberBlockListPage.mSearchResultList.Clear();
    int num = 0;
    using (Dictionary<long, AllianceMemberData>.ValueCollection.Enumerator enumerator = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId).members.datas.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceMemberData current = enumerator.Current;
        if (current.status == -1)
        {
          AllianceBlockUnblockItem component = Utils.DuplicateGOB(this.mMemberBlockListPage.mBlockedMemberItemPrefab).GetComponent<AllianceBlockUnblockItem>();
          this.mMemberBlockListPage.mSearchResultList.Add(component.gameObject);
          component.gameObject.SetActive(true);
          component.onUnblockBtnPressed += new System.Action<int>(this.OnUnblockMemberPressed);
          int uid = (int) current.uid;
          UserData userData = DBManager.inst.DB_User.Get((long) uid);
          CityData byUid = DBManager.inst.DB_City.GetByUid((long) uid);
          string userName = userData.userName;
          int power = (int) userData.power;
          int level = byUid.level;
          int portrait = userData.portrait;
          component.SetDetails(uid, userName, portrait, power, level, userData.Icon, userData.LordTitle);
          ++num;
        }
      }
    }
    this.mMemberBlockListPage.mTable.Reposition();
    this.mMemberBlockListPage.mScrollView.ResetPosition();
    this.mMemberBlockListPage.mNoDataItemPrefab.SetActive(num == 0);
  }

  private void OnUnblockMemberPressed(int memberID)
  {
    MessageHub.inst.GetPortByAction("Alliance:banByAdmin").SendRequest(new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) PlayerData.inst.allianceId
      },
      {
        (object) "banned_uid",
        (object) memberID
      },
      {
        (object) "operate",
        (object) "unban"
      }
    }, new System.Action<bool, object>(this.UnblockMemberCallback), true);
  }

  private void UnblockMemberCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.RefreshBlockedMembersList();
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.mTabs.onTabSelected += new System.Action<int>(this.OnTabPressed);
    this.mTabs.SetCurrentTab(0, true);
    this.mMembersSearchPage.mSearchInput.defaultText = ScriptLocalization.Get("alliance_tap_here_to_search", true);
    this.mMembersSearchPage.mSearchInput.value = string.Empty;
    this.mMembersSearchPage.mSearchButton.isEnabled = false;
    this.ClearSearchList();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.mTabs.onTabSelected -= new System.Action<int>(this.OnTabPressed);
  }

  [Serializable]
  public class MembersSearchPage
  {
    [HideInInspector]
    public List<GameObject> mSearchResultList = new List<GameObject>();
    public GameObject mSearchItemPrefab;
    public UIInput mSearchInput;
    public GameObject mNoDataItemPrefab;
    public UILabel mNoDataLabel;
    public UIScrollView mScrollView;
    public UITable mTable;
    public UIButton mSearchButton;
  }

  [Serializable]
  public class BlockListPage
  {
    [HideInInspector]
    public List<GameObject> mSearchResultList = new List<GameObject>();
    public GameObject mBlockedMemberItemPrefab;
    public GameObject mNoDataItemPrefab;
    public UIScrollView mScrollView;
    public UITable mTable;
  }
}
