﻿// Decompiled with JetBrains decompiler
// Type: WonderSettingDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UI;
using UnityEngine;

public class WonderSettingDlg : UI.Dialog
{
  protected string m_previousName = string.Empty;
  private WonderSettingDlg.Data data = new WonderSettingDlg.Data();
  [SerializeField]
  private WonderSettingDlg.Panel panel;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    WonderSettingDlg.Parameter param = orgParam as WonderSettingDlg.Parameter;
    if (param != null)
      RequestManager.inst.SendRequest("wonder:loadWonderData", Utils.Hash((object) "kingdom_id", (object) param.kingdomId), (System.Action<bool, object>) ((result, orgSrc) =>
      {
        if (!result)
          return;
        WonderFlagManager.Instance.Parse(orgSrc);
        this.Setup(param.kingdomId);
      }), true);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void Setup(long kingdomId)
  {
    this.m_previousName = string.Empty;
    DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get(kingdomId);
    this.SetupFlag(ConfigManager.inst.DB_WorldFlag.GetAllWorldFlagInfo());
    if (ConfigManager.inst.DB_GameConfig.GetData("world_map_wonder_max") != null)
      this.SetupCity(ConfigManager.inst.DB_GameConfig.GetData("world_map_wonder_max").ValueInt);
    else
      this.SetupCity(1);
    this.panel.kingNameChecker.spriteName = "red_cross";
    this.panel.BT_ChangeName.isEnabled = false;
    this.panel.wonder_name.text = string.Format("#{0}", (object) kingdomId);
    if (wonderData == null)
      return;
    this.data.wonder = wonderData;
    this.panel.input_changeName.value = !string.IsNullOrEmpty(wonderData.Name) ? wonderData.Name : string.Empty;
    if (wonderData.FLAG != 0)
    {
      if (this.data.flagIndex != 0)
      {
        WonderSettingFlagSlot flagSlotByFlagId = this.panel.GetWonderSettingFlagSlotByFlagId(this.data.flagIndex);
        if ((bool) ((UnityEngine.Object) flagSlotByFlagId))
          flagSlotByFlagId.Selected(false);
      }
      this.data.flagIndex = wonderData.FLAG;
      WonderSettingFlagSlot flagSlotByFlagId1 = this.panel.GetWonderSettingFlagSlotByFlagId(this.data.flagIndex);
      if ((bool) ((UnityEngine.Object) flagSlotByFlagId1))
        flagSlotByFlagId1.Selected(true);
    }
    if (wonderData.PORTRAIT != 0)
    {
      if (this.data.cityIndex != 0)
        this.panel.citys[this.data.cityIndex - 1].Selected(false);
      this.data.cityIndex = wonderData.PORTRAIT;
      if (this.data.cityIndex > 0 && this.data.cityIndex <= this.panel.citys.Count)
        this.panel.citys[this.data.cityIndex - 1].Selected(true);
    }
    if (wonderData.KING_UID == 0L)
    {
      this.panel.kingContainer.gameObject.SetActive(false);
      if (wonderData.KING_ALLIANCE_ID == PlayerData.inst.allianceId && PlayerData.inst.allianceData.creatorId == PlayerData.inst.uid && wonderData.State == "protected")
        this.panel.transKingPlace.gameObject.SetActive(true);
      else
        this.panel.transKingPlace.gameObject.SetActive(true);
    }
    else
    {
      this.panel.transKingPlace.gameObject.SetActive(false);
      this.panel.kingContainer.gameObject.SetActive(true);
      UserData userData = DBManager.inst.DB_User.Get(wonderData.KING_UID);
      if (userData == null)
        return;
      CustomIconLoader.Instance.requestCustomIcon(this.panel.kingPortrait, userData.PortraitIconPath, userData.Icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.panel.kingPortrait, userData.LordTitle, 2);
      WonderSettingDlg.Data data = this.data;
      string userName = userData.userName;
      this.panel.kingName.text = userName;
      string str = userName;
      data.kingdomName = str;
      this.OnSecond(NetServerTime.inst.ServerTimestamp);
    }
  }

  public void OnSecond(int second)
  {
    if (this.data.wonder == null)
      return;
    this.CheckState(second);
    if (!this.data.isKing)
      return;
    if (!this.data.changeNameFlag)
      this.panel.changeNameCD.text = Utils.FormatTime1((int) (this.data.wonder.LAST_CHANGE_NAME_TIME + 86400L - (long) second));
    if (!this.data.changeFlagFlag)
      this.panel.changeFlagCD.text = Utils.FormatTime1((int) (this.data.wonder.LAST_CHANGE_FLAG_TIME + 86400L - (long) second));
    if (this.data.changeCityFlag)
      return;
    this.panel.changeCityCD.text = Utils.FormatTime1((int) (this.data.wonder.LAST_CHANGE_PORTRAIT_TIME + 86400L - (long) second));
  }

  public void CheckState(int second)
  {
    UserData userData = DBManager.inst.DB_User.Get(this.data.wonder.KING_UID);
    if (userData != null)
      this.data.isKing = userData.uid == PlayerData.inst.uid;
    if (!this.data.isKing)
    {
      if (this.data.changeNameFlag)
      {
        this.data.changeNameFlag = false;
        this.panel.changeNameTitle.gameObject.SetActive(false);
        this.panel.changeNameCD.gameObject.SetActive(false);
        this.panel.BT_ChangeName.isEnabled = false;
        this.panel.input_changeName.enabled = false;
      }
      if (this.data.changeFlagFlag)
      {
        this.data.changeFlagFlag = false;
        this.panel.changeFlagTitle.gameObject.SetActive(false);
        this.panel.changeFlagCD.gameObject.SetActive(false);
        for (int index = 0; index < this.panel.flags.Count; ++index)
          this.panel.flags[index].SetEnable(false);
      }
      if (!this.data.changeCityFlag)
        return;
      this.data.changeCityFlag = false;
      this.panel.changeCityTitle.gameObject.SetActive(false);
      this.panel.changeCityCD.gameObject.SetActive(false);
      for (int index = 0; index < this.panel.citys.Count; ++index)
        this.panel.citys[index].SetEnable(false);
    }
    else
    {
      if ((long) second < this.data.wonder.LAST_CHANGE_NAME_TIME + 86400L)
      {
        if (this.data.changeNameFlag)
        {
          this.data.changeNameFlag = false;
          this.panel.changeNameTitle.gameObject.SetActive(true);
          this.panel.changeNameCD.gameObject.SetActive(true);
          this.panel.BT_ChangeName.isEnabled = false;
          this.panel.input_changeName.enabled = false;
        }
      }
      else if (!this.data.changeNameFlag)
      {
        this.data.changeNameFlag = true;
        this.panel.changeNameTitle.gameObject.SetActive(false);
        this.panel.changeNameCD.gameObject.SetActive(false);
        this.panel.input_changeName.enabled = true;
      }
      if ((long) second < this.data.wonder.LAST_CHANGE_FLAG_TIME + 86400L)
      {
        if (this.data.changeFlagFlag)
        {
          this.data.changeFlagFlag = false;
          this.panel.changeFlagTitle.gameObject.SetActive(true);
          this.panel.changeFlagCD.gameObject.SetActive(true);
          for (int index = 0; index < this.panel.flags.Count; ++index)
            this.panel.flags[index].SetEnable(false);
        }
      }
      else if (!this.data.changeFlagFlag)
      {
        this.data.changeFlagFlag = true;
        this.panel.changeFlagTitle.gameObject.SetActive(false);
        this.panel.changeFlagCD.gameObject.SetActive(false);
        for (int index = 0; index < this.panel.flags.Count; ++index)
          this.panel.flags[index].SetEnable(true);
      }
      if ((long) second < this.data.wonder.LAST_CHANGE_PORTRAIT_TIME + 86400L)
      {
        if (!this.data.changeCityFlag)
          return;
        this.data.changeCityFlag = false;
        this.panel.changeCityTitle.gameObject.SetActive(true);
        this.panel.changeCityCD.gameObject.SetActive(true);
        for (int index = 0; index < this.panel.citys.Count; ++index)
          this.panel.citys[index].SetEnable(false);
      }
      else
      {
        if (this.data.changeCityFlag)
          return;
        this.data.changeCityFlag = true;
        this.panel.changeCityTitle.gameObject.SetActive(false);
        this.panel.changeCityCD.gameObject.SetActive(false);
        for (int index = 0; index < this.panel.citys.Count; ++index)
          this.panel.citys[index].SetEnable(true);
      }
    }
  }

  public void OnNameChanged()
  {
    if (!this.data.changeNameFlag)
    {
      this.panel.input_changeName.value = this.m_previousName;
    }
    else
    {
      string s = this.panel.input_changeName.value.Replace("[", "【").Replace("]", "】").Replace("\n", string.Empty);
      if (Encoding.UTF8.GetBytes(s).Length > 20)
        s = this.m_previousName;
      if (s.Length < 3)
      {
        this.panel.kingNameChecker.spriteName = "red_cross";
        this.panel.BT_ChangeName.isEnabled = false;
      }
      else if (s == this.data.wonder.Name)
      {
        this.panel.kingNameChecker.spriteName = "red_cross";
        this.panel.BT_ChangeName.isEnabled = false;
      }
      else
      {
        this.panel.kingNameChecker.spriteName = "green_tick";
        this.panel.BT_ChangeName.isEnabled = true;
      }
      this.m_previousName = s;
      this.panel.input_changeName.value = s;
    }
  }

  public void TransKing()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceOtherMemberDialog", (UI.Dialog.DialogParameter) new AllianceOtherMemberDialog.Parameter()
    {
      allianceData = PlayerData.inst.allianceData,
      nominate = true
    }, true, true, true);
  }

  public void SetName()
  {
    if (IllegalWordsUtils.WarningIllegalWords(this.panel.input_changeName.value))
      return;
    UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
    {
      title = ScriptLocalization.Get(Utils.XLAT("id_uppercase_confirm"), true),
      description = ScriptLocalization.Get(Utils.XLAT("throne_king_settings_change_confirm"), true),
      leftButtonClickEvent = (System.Action) null,
      rightButtonClickEvent = (System.Action) (() => RequestManager.inst.SendRequest("wonder:changeKingdomName", new Hashtable()
      {
        {
          (object) "kingdom_name",
          (object) this.panel.input_changeName.value
        }
      }, (System.Action<bool, object>) ((result, data) =>
      {
        if (!result)
          return;
        UIManager.inst.toast.Show(Utils.XLAT("toast_throne_name_change_success"), (System.Action) null, 4f, false);
      }), true))
    });
  }

  public void SetFlag(int flagIndex)
  {
    UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
    {
      title = ScriptLocalization.Get(Utils.XLAT("id_uppercase_confirm"), true),
      description = ScriptLocalization.Get(Utils.XLAT("throne_king_settings_change_confirm"), true),
      leftButtonClickEvent = (System.Action) null,
      rightButtonClickEvent = (System.Action) (() => RequestManager.inst.SendRequest("wonder:changeKingdomFlag", new Hashtable()
      {
        {
          (object) "flag",
          (object) flagIndex
        }
      }, (System.Action<bool, object>) ((result, orgSrc) =>
      {
        if (!result)
          return;
        if (this.data.flagIndex != 0)
        {
          WonderSettingFlagSlot flagSlotByFlagId = this.panel.GetWonderSettingFlagSlotByFlagId(this.data.flagIndex);
          if ((bool) ((UnityEngine.Object) flagSlotByFlagId))
            flagSlotByFlagId.Selected(false);
        }
        this.data.flagIndex = flagIndex;
        if (this.data.flagIndex != 0)
        {
          WonderSettingFlagSlot flagSlotByFlagId = this.panel.GetWonderSettingFlagSlotByFlagId(this.data.flagIndex);
          if ((bool) ((UnityEngine.Object) flagSlotByFlagId))
            flagSlotByFlagId.Selected(true);
        }
        UIManager.inst.toast.Show(Utils.XLAT("toast_throne_flag_change_success"), (System.Action) null, 4f, false);
      }), true))
    });
  }

  public void SetCity(int cityIndex)
  {
    if (cityIndex < 1 || cityIndex > this.panel.citys.Count)
      return;
    UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
    {
      title = ScriptLocalization.Get("id_uppercase_confirm", true),
      description = ScriptLocalization.Get("throne_king_settings_change_confirm", true),
      leftButtonClickEvent = (System.Action) null,
      rightButtonClickEvent = (System.Action) (() => RequestManager.inst.SendRequest("wonder:changeKingdomPortrait", new Hashtable()
      {
        {
          (object) "portrait",
          (object) cityIndex
        }
      }, (System.Action<bool, object>) ((result, orgSrc) =>
      {
        if (!result)
          return;
        if (this.data.cityIndex != 0)
          this.panel.citys[this.data.cityIndex - 1].Selected(false);
        this.data.cityIndex = cityIndex;
        this.panel.citys[this.data.cityIndex - 1].Selected(true);
        UIManager.inst.toast.Show(Utils.XLAT("toast_throne_castle_change_success"), (System.Action) null, 4f, false);
      }), true))
    });
  }

  public void SetupFlag(List<WorldFlagInfo> allWorldFlagInfo)
  {
    int num = 0;
    for (int index = 0; index < allWorldFlagInfo.Count; ++index)
    {
      WorldFlagInfo worldFlagInfo = allWorldFlagInfo[index];
      if (WonderFlagManager.Instance.ContainsSelectFlag(worldFlagInfo.ServerStoredId))
      {
        GameObject gameObject = NGUITools.AddChild(this.panel.flagRoot.gameObject, this.panel.orgFlagSlot.gameObject);
        gameObject.SetActive(true);
        gameObject.transform.localPosition = new Vector3((float) (num % 6 * 340), (float) (-num / 6 * 230), 0.0f);
        WonderSettingFlagSlot component = gameObject.GetComponent<WonderSettingFlagSlot>();
        component.onButtonClick += new System.Action<int>(this.SetFlag);
        component.Selected(false);
        component.Setup(worldFlagInfo.ServerStoredId, worldFlagInfo.SpriteName);
        this.panel.flags.Add(component);
        ++num;
      }
    }
    this.panel.changeFlagExpand.SetDetailHeight(319 + (allWorldFlagInfo.Count - 1) / 6 * 230);
  }

  public void SetupCity(int count)
  {
    for (int index = 0; index < count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.panel.cityRoot.gameObject, this.panel.orgCitySlot.gameObject);
      gameObject.SetActive(true);
      gameObject.transform.localPosition = new Vector3((float) (index % 4 * 520), (float) (-index / 4 * 400), 0.0f);
      WonderSettingCitySlot component = gameObject.GetComponent<WonderSettingCitySlot>();
      component.onButtonClick += new System.Action<int>(this.SetCity);
      component.Selected(false);
      component.Setup(index + 1, "Texture/WorldMap/worldmap_stronghold_" + (object) (index + 1));
      this.panel.citys.Add(component);
    }
    this.panel.changeCityExpand.SetDetailHeight(459 + (count - 1) / 4 * 400);
  }

  public class Data
  {
    public bool changeNameFlag = true;
    public bool changeFlagFlag = true;
    public bool changeCityFlag = true;
    public string kingdomName;
    public DB.WonderData wonder;
    public bool isKing;
    public int flagIndex;
    public int cityIndex;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public long kingdomId;
  }

  [Serializable]
  protected class Panel
  {
    public Transform transKingPlace;
    public Transform kingContainer;
    public UITexture kingPortrait;
    public UILabel kingName;
    public UISprite kingNameChecker;
    public UILabel wonder_name;
    public WonderSettingExpandTool changeNameExpand;
    public UIInput input_changeName;
    public Transform changeNameTitle;
    public UILabel changeNameCD;
    public UIButton BT_ChangeName;
    public WonderSettingExpandTool changeFlagExpand;
    public Transform changeFlagTitle;
    public UILabel changeFlagCD;
    public Transform flagRoot;
    public WonderSettingFlagSlot orgFlagSlot;
    public List<WonderSettingFlagSlot> flags;
    public WonderSettingExpandTool changeCityExpand;
    public Transform changeCityTitle;
    public UILabel changeCityCD;
    public Transform cityRoot;
    public WonderSettingCitySlot orgCitySlot;
    public List<WonderSettingCitySlot> citys;

    public WonderSettingFlagSlot GetWonderSettingFlagSlotByFlagId(int serverStoredId)
    {
      return this.flags.Find((Predicate<WonderSettingFlagSlot>) (p => p.serverStoredId == serverStoredId));
    }
  }
}
