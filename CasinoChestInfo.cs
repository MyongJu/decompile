﻿// Decompiled with JetBrains decompiler
// Type: CasinoChestInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class CasinoChestInfo
{
  public Dictionary<int, CasinoChestInfo.RewardItemInfo> dicRewardItemInfo = new Dictionary<int, CasinoChestInfo.RewardItemInfo>();
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "lvl_min")]
  public int minLevel;
  [Config(Name = "lvl_max")]
  public int maxLevel;
  [Config(Name = "group_weight")]
  public float groupWeight;
  [Config(Name = "reward_type_1")]
  public string rewardType1;
  [Config(Name = "reward_item_1")]
  public string rewardItemId1;
  [Config(Name = "weight_1")]
  public float rewardWeight1;
  [Config(Name = "reward_type_2")]
  public string rewardType2;
  [Config(Name = "reward_item_2")]
  public string rewardItemId2;
  [Config(Name = "weight_2")]
  public float rewardWeight2;
  [Config(Name = "reward_type_3")]
  public string rewardType3;
  [Config(Name = "reward_item_3")]
  public string rewardItemId3;
  [Config(Name = "weight_3")]
  public float rewardWeight3;
  [Config(Name = "reward_type_4")]
  public string rewardType4;
  [Config(Name = "reward_item_4")]
  public string rewardItemId4;
  [Config(Name = "weight_4")]
  public float rewardWeight4;
  [Config(Name = "reward_type_5")]
  public string rewardType5;
  [Config(Name = "reward_item_5")]
  public string rewardItemId5;
  [Config(Name = "weight_5")]
  public float rewardWeight5;
  [Config(Name = "reward_type_6")]
  public string rewardType6;
  [Config(Name = "reward_item_6")]
  public string rewardItemId6;
  [Config(Name = "weight_6")]
  public float rewardWeight6;
  [Config(Name = "reward_type_7")]
  public string rewardType7;
  [Config(Name = "reward_item_7")]
  public string rewardItemId7;
  [Config(Name = "weight_7")]
  public float rewardWeight7;
  [Config(Name = "reward_type_8")]
  public string rewardType8;
  [Config(Name = "reward_item_8")]
  public string rewardItemId8;
  [Config(Name = "weight_8")]
  public float rewardWeight8;
  [Config(Name = "reward_type_9")]
  public string rewardType9;
  [Config(Name = "reward_item_9")]
  public string rewardItemId9;
  [Config(Name = "weight_9")]
  public float rewardWeight9;
  [Config(Name = "high_light")]
  public int highlightIndex;

  public Dictionary<int, CasinoChestInfo.RewardItemInfo> GetRewardItemInfo()
  {
    this.dicRewardItemInfo.Clear();
    this.dicRewardItemInfo.Add(0, new CasinoChestInfo.RewardItemInfo()
    {
      type = this.rewardType1,
      reward = this.rewardItemId1,
      weight = this.rewardWeight1
    });
    this.dicRewardItemInfo.Add(1, new CasinoChestInfo.RewardItemInfo()
    {
      type = this.rewardType2,
      reward = this.rewardItemId2,
      weight = this.rewardWeight2
    });
    this.dicRewardItemInfo.Add(2, new CasinoChestInfo.RewardItemInfo()
    {
      type = this.rewardType3,
      reward = this.rewardItemId3,
      weight = this.rewardWeight3
    });
    this.dicRewardItemInfo.Add(3, new CasinoChestInfo.RewardItemInfo()
    {
      type = this.rewardType4,
      reward = this.rewardItemId4,
      weight = this.rewardWeight4
    });
    this.dicRewardItemInfo.Add(4, new CasinoChestInfo.RewardItemInfo()
    {
      type = this.rewardType5,
      reward = this.rewardItemId5,
      weight = this.rewardWeight5
    });
    this.dicRewardItemInfo.Add(5, new CasinoChestInfo.RewardItemInfo()
    {
      type = this.rewardType6,
      reward = this.rewardItemId6,
      weight = this.rewardWeight6
    });
    this.dicRewardItemInfo.Add(6, new CasinoChestInfo.RewardItemInfo()
    {
      type = this.rewardType7,
      reward = this.rewardItemId7,
      weight = this.rewardWeight7
    });
    this.dicRewardItemInfo.Add(7, new CasinoChestInfo.RewardItemInfo()
    {
      type = this.rewardType8,
      reward = this.rewardItemId8,
      weight = this.rewardWeight8
    });
    this.dicRewardItemInfo.Add(8, new CasinoChestInfo.RewardItemInfo()
    {
      type = this.rewardType9,
      reward = this.rewardItemId9,
      weight = this.rewardWeight9
    });
    return this.dicRewardItemInfo;
  }

  public class RewardItemInfo
  {
    public string type;
    public string reward;
    public float weight;
  }
}
