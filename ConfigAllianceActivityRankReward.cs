﻿// Decompiled with JetBrains decompiler
// Type: ConfigAllianceActivityRankReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigAllianceActivityRankReward
{
  private ConfigParse parse = new ConfigParse();
  public Dictionary<string, AllianceActivityRankRewardInfo> datas;
  private Dictionary<int, AllianceActivityRankRewardInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<AllianceActivityRankRewardInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public AllianceActivityRankRewardInfo GetRewardInfo(int activity_id, int rank)
  {
    Dictionary<string, AllianceActivityRankRewardInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && enumerator.Current.ActivityMainSubInfo_ID == activity_id && (rank >= enumerator.Current.MinRank && rank <= enumerator.Current.MaxRank))
        return enumerator.Current;
    }
    return (AllianceActivityRankRewardInfo) null;
  }

  public List<AllianceActivityRankRewardInfo> GetTotalRewardInfos()
  {
    List<AllianceActivityRankRewardInfo> activityRankRewardInfoList = new List<AllianceActivityRankRewardInfo>();
    Dictionary<string, AllianceActivityRankRewardInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && enumerator.Current.ActivityMainSubInfo_ID == 0)
        activityRankRewardInfoList.Add(enumerator.Current);
    }
    activityRankRewardInfoList.Sort((Comparison<AllianceActivityRankRewardInfo>) ((x, y) => x.MinRank.CompareTo(y.MinRank)));
    return activityRankRewardInfoList;
  }

  public List<AllianceActivityRankRewardInfo> GetRewardInfos(int activity_id)
  {
    List<AllianceActivityRankRewardInfo> activityRankRewardInfoList = new List<AllianceActivityRankRewardInfo>();
    Dictionary<string, AllianceActivityRankRewardInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && enumerator.Current.ActivityMainSubInfo_ID == activity_id)
        activityRankRewardInfoList.Add(enumerator.Current);
    }
    activityRankRewardInfoList.Sort((Comparison<AllianceActivityRankRewardInfo>) ((x, y) => x.MinRank.CompareTo(y.MinRank)));
    return activityRankRewardInfoList;
  }

  public AllianceActivityRankRewardInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (AllianceActivityRankRewardInfo) null;
  }

  public AllianceActivityRankRewardInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AllianceActivityRankRewardInfo) null;
  }
}
