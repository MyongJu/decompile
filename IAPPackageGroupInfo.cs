﻿// Decompiled with JetBrains decompiler
// Type: IAPPackageGroupInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class IAPPackageGroupInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "type")]
  public int type;
  [Config(Name = "validate_value")]
  public int validate_value;
  [Config(Name = "data_value1")]
  public long data_value1;
  [Config(Name = "data_value2")]
  public long data_value2;
  [Config(Name = "version_special_offer_priority")]
  public int specialOfferPriority;
  [Config(Name = "version_special_offer_description")]
  public string specialOfferDes;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "icon")]
  public string icon;
  [Config(Name = "graph")]
  public string graph;
  [Config(Name = "graph_background")]
  public string graphBackground;
  [Config(Name = "package_id_1")]
  public int package_id_1;
  [Config(Name = "package_id_2")]
  public int package_id_2;
  [Config(Name = "package_id_3")]
  public int package_id_3;
  [Config(Name = "package_id_4")]
  public int package_id_4;
  [Config(Name = "package_id_5")]
  public int package_id_5;
  [Config(Name = "package_id_6")]
  public int package_id_6;
  [Config(Name = "package_id_7")]
  public int package_id_7;
  [Config(Name = "package_id_8")]
  public int package_id_8;
  [Config(Name = "chest_display_rate")]
  public int chestDisplayRate;
  [Config(Name = "thumbnail_bg_color")]
  public string thumbnailBGColor;

  public long Duration
  {
    get
    {
      switch (this.type)
      {
        case 2:
          return 86400L * (this.data_value2 - this.data_value1 + 1L);
        case 3:
        case 4:
          return this.data_value1;
        default:
          return 0;
      }
    }
  }

  public string GraphPath
  {
    get
    {
      return "Texture/IAP/" + this.graph;
    }
  }

  public string GraphBackGroudPath
  {
    get
    {
      return "Texture/IAP/" + this.graphBackground;
    }
  }

  public int GetPackagePosition(int packageId)
  {
    if (packageId == this.package_id_1)
      return 1;
    if (packageId == this.package_id_2)
      return 2;
    if (packageId == this.package_id_3)
      return 3;
    if (packageId == this.package_id_4)
      return 4;
    if (packageId == this.package_id_5)
      return 5;
    if (packageId == this.package_id_6)
      return 6;
    if (packageId == this.package_id_7)
      return 7;
    return packageId == this.package_id_8 ? 8 : 0;
  }

  public int GetPackageIdByPosition(int position)
  {
    switch (position)
    {
      case 1:
        return this.package_id_1;
      case 2:
        return this.package_id_2;
      case 3:
        return this.package_id_3;
      case 4:
        return this.package_id_4;
      case 5:
        return this.package_id_5;
      case 6:
        return this.package_id_6;
      case 7:
        return this.package_id_7;
      case 8:
        return this.package_id_8;
      default:
        return 0;
    }
  }

  public string SpecialOfferDescription
  {
    get
    {
      return ScriptLocalization.Get(this.specialOfferDes, true);
    }
  }
}
