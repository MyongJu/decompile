﻿// Decompiled with JetBrains decompiler
// Type: PageTabsMonitor
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class PageTabsMonitor : MonoBehaviour
{
  public List<PageTabsMonitor.TabButton> mTabs = new List<PageTabsMonitor.TabButton>();
  public int selection = -1;
  public System.Action<int> onTabSelected;

  public void OnTabSelected()
  {
    using (List<PageTabsMonitor.TabButton>.Enumerator enumerator = this.mTabs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PageTabsMonitor.TabButton current = enumerator.Current;
        if (current.mSelected.activeSelf)
        {
          current.mSelected.SetActive(false);
          current.mNormal.SetActive(true);
        }
        if ((UnityEngine.Object) UIButton.current.gameObject == (UnityEngine.Object) current.mNormal)
        {
          current.mSelected.SetActive(true);
          current.mNormal.SetActive(false);
          this.selection = current.mID;
        }
      }
    }
    if (this.onTabSelected == null)
      return;
    this.onTabSelected(this.selection);
  }

  public void SetCurrentTab(int tabID, bool bCallAction = true)
  {
    using (List<PageTabsMonitor.TabButton>.Enumerator enumerator = this.mTabs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PageTabsMonitor.TabButton current = enumerator.Current;
        if (current.mSelected.activeSelf)
        {
          current.mSelected.SetActive(false);
          current.mNormal.SetActive(true);
        }
        if (tabID == current.mID)
        {
          current.mSelected.SetActive(true);
          current.mNormal.SetActive(false);
          this.selection = current.mID;
        }
      }
    }
    if (!bCallAction || this.onTabSelected == null)
      return;
    this.onTabSelected(this.selection);
  }

  public void ShowButton(int count)
  {
    this.HideAllButtons();
    for (int index = 0; index < count && index < this.mTabs.Count; ++index)
    {
      this.mTabs[index].mNormal.SetActive(true);
      this.mTabs[index].mSelected.SetActive(false);
    }
    if (this.selection < count)
    {
      this.mTabs[this.selection].mNormal.SetActive(false);
      this.mTabs[this.selection].mSelected.SetActive(true);
    }
    else
      this.selection = -1;
  }

  public void HideAllButtons()
  {
    using (List<PageTabsMonitor.TabButton>.Enumerator enumerator = this.mTabs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PageTabsMonitor.TabButton current = enumerator.Current;
        current.mSelected.SetActive(false);
        current.mNormal.SetActive(false);
      }
    }
  }

  [Serializable]
  public class TabButton
  {
    public int mID;
    public GameObject mNormal;
    public GameObject mSelected;
    public GameObject mDisabled;
  }
}
