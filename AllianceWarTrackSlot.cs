﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarTrackSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceWarTrackSlot : MonoBehaviour
{
  [SerializeField]
  private UITexture _texturePortrait;
  [SerializeField]
  private UILabel _labelPower;
  [SerializeField]
  private UILabel _labelK;
  [SerializeField]
  private UILabel _labelX;
  [SerializeField]
  private UILabel _labelY;
  [SerializeField]
  private UILabel _labelName;
  [SerializeField]
  private GameObject _objLocationKnown;
  [SerializeField]
  private GameObject _objLocationUnknown;
  [SerializeField]
  private GameObject _objCantTrack;
  private AllianceWarTrackData.TrackMemberData _trackMemberData;

  public void SetData(AllianceWarTrackData.TrackMemberData trackMemberData)
  {
    this._trackMemberData = trackMemberData;
    this.UpdateUI();
  }

  public void RefreshUI()
  {
    this.UpdateUI();
  }

  public void OnTrackLocationClicked()
  {
    if (this._trackMemberData == null)
      return;
    if (this._trackMemberData.Location.K != PlayerData.inst.userData.current_world_id)
    {
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_alliance_warfare_tracking_kingdom", new Dictionary<string, string>()
      {
        {
          "0",
          this._trackMemberData.Location.K.ToString()
        }
      }, true), (System.Action) null, 4f, true);
    }
    else
    {
      if (this._trackMemberData.Location.K <= 0)
        return;
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
        UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
        {
          CitadelSystem.inst.ForceHide();
          GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
          if ((UnityEngine.Object) PVPSystem.Instance.Controller == (UnityEngine.Object) null)
          {
            PVPMapData.Initialize();
            PVPSystem.Instance.Setup(PVPMapData.MapData);
          }
          Utils.ExecuteInSecs(0.0f, (System.Action) (() => UIManager.inst.Cloud.PokeCloud((System.Action) (() =>
          {
            PVPSystem.Instance.Map.GotoLocation(this._trackMemberData.Location, false);
            UIManager.inst.tileCamera.StartAutoZoom();
          }))));
        }));
      else
        PVPSystem.Instance.Map.GotoLocation(this._trackMemberData.Location, false);
    }
  }

  private void UpdateUI()
  {
    if (this._trackMemberData == null)
      return;
    Utils.SetPortrait(this._texturePortrait, "Texture/Hero/player_portrait_" + (object) this._trackMemberData.Portrait);
    this._labelPower.text = Utils.FormatThousands(this._trackMemberData.Power.ToString());
    this._labelK.text = string.Format("K:{0}", (object) this._trackMemberData.Location.K);
    this._labelX.text = this._trackMemberData.Location.X.ToString();
    this._labelY.text = this._trackMemberData.Location.Y.ToString();
    this._labelName.text = this._trackMemberData.Name;
    NGUITools.SetActive(this._objLocationKnown, false);
    NGUITools.SetActive(this._objLocationUnknown, false);
    NGUITools.SetActive(this._objCantTrack, false);
    if (!AllianceWarPayload.Instance.TrackData.IsTracking)
    {
      NGUITools.SetActive(this._objLocationUnknown, true);
    }
    else
    {
      NGUITools.SetActive(this._objLocationKnown, this._trackMemberData.Location.K > 0);
      NGUITools.SetActive(this._objCantTrack, this._trackMemberData.Location.K <= 0);
    }
  }
}
