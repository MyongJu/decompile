﻿// Decompiled with JetBrains decompiler
// Type: DynamicMapData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DynamicMapData : StaticMapData
{
  public string LoadAssetPath = string.Empty;
  public string KingdomGround = string.Empty;
  private KingdomAgent m_KingdomAgent;
  private BlockAgent m_BlockAgent;

  public DynamicMapData()
  {
    this.m_KingdomAgent = new KingdomAgent(this);
    this.m_BlockAgent = new BlockAgent(this);
    this.m_BlockAgent.OnRequestKingdomBlock = new System.Action(this.PublishRequestKingdomBlock);
    this.m_BlockAgent.OnEnterKingdomBlock = new System.Action(this.PublishEnterKingdomBlock);
  }

  public event System.Action OnRequestKingdomBlock;

  public event System.Action OnEnterKingdomBlock;

  public void Dispose(bool leave = true)
  {
    this.m_KingdomAgent.Dispose();
    this.m_BlockAgent.Dispose(leave);
  }

  public void ClearListeners()
  {
    this.OnRequestKingdomBlock = (System.Action) null;
    this.OnEnterKingdomBlock = (System.Action) null;
  }

  public void UpdateCenter(WorldCoordinate worldCoords)
  {
    this.m_BlockAgent.UpdateCenter(worldCoords);
    this.m_KingdomAgent.UpdateCenter(worldCoords);
  }

  public bool NeedRequestEnterKingdom(Coordinate location)
  {
    return this.NeedRequestEnterKingdom(this.ConvertTileKXYToWorldCoordinate(location));
  }

  public bool NeedRequestEnterKingdom(WorldCoordinate worldCoords)
  {
    return this.m_BlockAgent.NeedRequestEnterKingdom(worldCoords);
  }

  public void UpdateViewport(Rect viewport)
  {
    this.m_BlockAgent.UpdateViewport(viewport);
    this.m_KingdomAgent.UpdateViewport(viewport);
  }

  public void CullAndShow(Transform transform)
  {
    this.m_BlockAgent.CullAndShow(transform);
    this.m_KingdomAgent.CullAndShow(transform);
  }

  public void UpdateUI()
  {
    this.m_BlockAgent.UpdateUI();
  }

  public TileData GetTileAt(Coordinate loc)
  {
    return this.GetTileAt(loc.K, loc.X, loc.Y);
  }

  public TileData GetTileAt(int k, int x, int y)
  {
    BlockData blockAt = this.GetBlockAt(k, x, y);
    if (blockAt != null)
      return blockAt.GetTileAt(k, x, y);
    return (TileData) null;
  }

  public TileData GetReferenceAt(int k, int x, int y)
  {
    TileData tileAt = this.GetTileAt(k, x, y);
    if (tileAt != null && tileAt.ReferenceX != -1 && tileAt.ReferenceY != -1)
      return this.GetTileAt(k, tileAt.ReferenceX, tileAt.ReferenceY);
    return tileAt;
  }

  public TileData GetReferenceAt(Coordinate loc)
  {
    return this.GetReferenceAt(loc.K, loc.X, loc.Y);
  }

  public PVPTile GetPVPTile(Coordinate location)
  {
    TileData tileAt = this.GetTileAt(location);
    if (tileAt != null)
      return tileAt.TileView;
    return (PVPTile) null;
  }

  public TileData GetSingleNearFarmOrSwamill(Coordinate location)
  {
    return this.m_BlockAgent.GetSingleNearFarmOrSwamill(location);
  }

  public TileData GetSingleNearMonster(Coordinate location)
  {
    return this.m_BlockAgent.GetSingleNearMonster(location);
  }

  public TileData GetNearEmptyTileForCity(Coordinate location, bool ignoreWonderArea)
  {
    return this.m_BlockAgent.GetNearEmptyTileForCity(location, ignoreWonderArea);
  }

  public BlockData GetBlockAt(Coordinate location)
  {
    return this.GetBlockAt(location.K, location.X, location.Y);
  }

  public void DrawBlockBorders(Color color)
  {
    this.m_KingdomAgent.DrawBlockBorders(color);
    this.m_BlockAgent.DrawBlockID();
  }

  private BlockData GetBlockAt(Vector2 point)
  {
    return this.GetBlockAt(this.ConvertPixelPositionToTileKXY((Vector3) point));
  }

  private BlockData GetBlockAt(int k, int x, int y)
  {
    return this.m_BlockAgent.GetBlockAt(k, x, y);
  }

  public BlockData GetBlockByID(int blockID)
  {
    return this.m_BlockAgent.GetBlockByID(blockID);
  }

  public bool IsLoaded
  {
    get
    {
      return this.m_BlockAgent.IsLoaded;
    }
  }

  private void PublishEnterKingdomBlock()
  {
    if (this.OnEnterKingdomBlock == null)
      return;
    this.OnEnterKingdomBlock();
  }

  private void PublishRequestKingdomBlock()
  {
    if (this.OnRequestKingdomBlock == null)
      return;
    this.OnRequestKingdomBlock();
  }
}
