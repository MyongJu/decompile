﻿// Decompiled with JetBrains decompiler
// Type: MonthCardItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MonthCardItem : MonoBehaviour
{
  public UISprite bg;
  public UILabel num;

  public void SetActiveState(bool sts)
  {
    if (sts)
    {
      this.bg.color = new Color(0.827451f, 0.4470588f, 0.003921569f, 0.4901961f);
      this.num.color = new Color(0.827451f, 0.4470588f, 0.003921569f);
    }
    else
    {
      this.bg.color = new Color(0.8039216f, 0.8039216f, 0.8039216f, 0.2156863f);
      this.num.color = new Color(0.8039216f, 0.8039216f, 0.8039216f);
    }
  }
}
