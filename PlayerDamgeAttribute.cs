﻿// Decompiled with JetBrains decompiler
// Type: PlayerDamgeAttribute
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class PlayerDamgeAttribute : RoundPlayerAttribute
{
  public PlayerDamgeAttribute(RoundPlayer player)
    : base(player)
  {
  }

  protected override DragonKnightAttibute.Type Type
  {
    get
    {
      return DragonKnightAttibute.Type.Damge;
    }
  }

  public override void Reset()
  {
    int result = Mathf.CeilToInt((float) this.InBattleBaseValue + this.GetInBattleAttibutePlus((float) this.Owner.Strong, DragonKnightAttibute.Type.Strong) + this.GetInBattleAttibutePlus((float) this.Owner.Intelligent, DragonKnightAttibute.Type.Intelligence));
    this.CurrentValue = this.BuffFactor(result);
    if (this.MaxValue > 0)
      return;
    this.MaxValue = result;
  }

  public override int OutBattleBaseValue
  {
    get
    {
      return this.AddPercent((float) this.BaseValue + this.GetOutBattleAttibutePlus((float) this.Owner.Strong, DragonKnightAttibute.Type.Strong) + this.GetOutBattleAttibutePlus((float) this.Owner.Intelligent, DragonKnightAttibute.Type.Intelligence), DragonKnightAttibute.Type.Damge);
    }
  }
}
