﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightTemporaryItemPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonKnightTemporaryItemPopup : Popup
{
  private Dictionary<int, DragonKnightTemporaryItemRenderer> m_Items = new Dictionary<int, DragonKnightTemporaryItemRenderer>();
  public GameObject m_ItemPrefab;
  public GameObject m_NoItems;
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.ClearData();
    base.OnClose(orgParam);
  }

  private void UpdateUI()
  {
    this.ClearData();
    using (Dictionary<int, DragonKnightDungeonData.BagInfo>.Enumerator enumerator = DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L).TemporaryItems.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DragonKnightDungeonData.BagInfo bagInfo = enumerator.Current.Value;
        if (bagInfo.count > 0)
        {
          GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
          gameObject.SetActive(true);
          gameObject.transform.parent = this.m_Grid.transform;
          gameObject.transform.localPosition = Vector3.zero;
          gameObject.transform.localRotation = Quaternion.identity;
          gameObject.transform.localScale = Vector3.one;
          DragonKnightTemporaryItemRenderer component = gameObject.GetComponent<DragonKnightTemporaryItemRenderer>();
          component.SetData(bagInfo, new System.Action<int>(this.OnUseItem));
          this.m_Items.Add(bagInfo.itemId, component);
        }
      }
    }
    this.Reposition();
  }

  private void OnUseItem(int itemId)
  {
    DragonKnightTemporaryItemRenderer temporaryItemRenderer;
    this.m_Items.TryGetValue(itemId, out temporaryItemRenderer);
    if (!((UnityEngine.Object) temporaryItemRenderer != (UnityEngine.Object) null) || temporaryItemRenderer.BagItem.count > 0)
      return;
    temporaryItemRenderer.gameObject.SetActive(false);
    temporaryItemRenderer.transform.parent = (Transform) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) temporaryItemRenderer.gameObject);
    this.m_Items.Remove(itemId);
    this.Reposition();
  }

  private void ClearData()
  {
    Dictionary<int, DragonKnightTemporaryItemRenderer>.Enumerator enumerator = this.m_Items.GetEnumerator();
    while (enumerator.MoveNext())
    {
      enumerator.Current.Value.gameObject.SetActive(false);
      enumerator.Current.Value.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.Value.gameObject);
    }
    this.m_Items.Clear();
  }

  private void Reposition()
  {
    this.m_ScrollView.ResetPosition();
    this.m_Grid.Reposition();
    this.m_NoItems.SetActive(this.m_Items.Count == 0);
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }
}
