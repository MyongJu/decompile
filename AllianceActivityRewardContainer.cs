﻿// Decompiled with JetBrains decompiler
// Type: AllianceActivityRewardContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class AllianceActivityRewardContainer : MonoBehaviour
{
  private List<Icon> icons = new List<Icon>();
  public UILabel label;
  public UILabel title;
  public GameObject normal;
  public GameObject achieved;
  public GameObject normalItem;
  public GameObject backBgItem;
  public UIScrollView scroll;
  public UITable table;
  public GameObject header;
  public AllianceActivityStepStatus status;

  private UILabel GetHeader()
  {
    GameObject gameObject = NGUITools.AddChild(this.table.gameObject, this.header);
    gameObject.SetActive(true);
    return gameObject.transform.GetChild(0).gameObject.GetComponent<UILabel>();
  }

  private AllianceActivityStepStatus GetStatus()
  {
    GameObject gameObject = NGUITools.AddChild(this.table.gameObject, this.status.gameObject);
    gameObject.SetActive(true);
    return gameObject.GetComponent<AllianceActivityStepStatus>();
  }

  private GameObject GetStatus(string name)
  {
    GameObject gameObject = NGUITools.AddChild(this.table.gameObject, (bool) ((UnityEngine.Object) this.status));
    gameObject.SetActive(true);
    return gameObject;
  }

  private Icon GetIcon(int index)
  {
    GameObject gameObject = index == 0 || index % 2 != 0 ? NGUITools.AddChild(this.table.gameObject, this.backBgItem) : NGUITools.AddChild(this.table.gameObject, this.normalItem);
    Icon component = gameObject.GetComponent<Icon>();
    component.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
    component.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
    component.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
    gameObject.SetActive(true);
    this.icons.Add(component);
    return component;
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  public void Refresh(ActivityBaseData data)
  {
    RoundActivityData roundActivityData = data as RoundActivityData;
    AllianceActivityData allianceActivityData = data as AllianceActivityData;
    AllianceActivityReward currentStepReward = ConfigManager.inst.DB_AllianceActivityReward.GetCurrentStepReward(roundActivityData.CurrentRound.configId, allianceActivityData.AllianceLevel);
    this.SetData(1, roundActivityData.CurrentRoundScore, currentStepReward.RequireScore1_1, currentStepReward.ItemRewardID1_1, currentStepReward.ItemRewardValue1_1, currentStepReward.ItemRewardID1_2, currentStepReward.ItemRewardValue1_2, currentStepReward.ItemRewardID1_3, currentStepReward.ItemRewardValue1_3, currentStepReward.ItemRewardID1_4, currentStepReward.ItemRewardValue1_4, currentStepReward.ItemRewardID1_5, currentStepReward.ItemRewardValue1_5);
    this.SetData(2, roundActivityData.CurrentRoundScore, currentStepReward.RequireScore2_1, currentStepReward.ItemRewardID2_1, currentStepReward.ItemRewardValue2_1, currentStepReward.ItemRewardID2_2, currentStepReward.ItemRewardValue2_2, currentStepReward.ItemRewardID2_3, currentStepReward.ItemRewardValue2_3, currentStepReward.ItemRewardID2_4, currentStepReward.ItemRewardValue2_4, currentStepReward.ItemRewardID2_5, currentStepReward.ItemRewardValue2_5);
    this.SetData(3, roundActivityData.CurrentRoundScore, currentStepReward.RequireScore3_1, currentStepReward.ItemRewardID3_1, currentStepReward.ItemRewardValue3_1, currentStepReward.ItemRewardID3_2, currentStepReward.ItemRewardValue3_2, currentStepReward.ItemRewardID3_3, currentStepReward.ItemRewardValue3_3, currentStepReward.ItemRewardID3_4, currentStepReward.ItemRewardValue3_4, currentStepReward.ItemRewardID3_5, currentStepReward.ItemRewardValue3_5);
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scroll.ResetPosition();
  }

  public void Clear()
  {
    for (int index = 0; index < this.icons.Count; ++index)
      this.icons[index].Dispose();
    this.icons.Clear();
  }

  private void SetData(int index, long currentScore, int targetScore, params int[] items)
  {
    this.GetHeader().text = ScriptLocalization.Get("event_alliance_step_reward" + (object) index, true);
    AllianceActivityStepStatus status = this.GetStatus();
    string content = ScriptLocalization.Get("event_alliance_step_reward", true) + " " + (object) targetScore;
    status.SetData(targetScore, currentScore, content);
    int index1 = 0;
    while (index1 < items.Length)
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(items[index1]);
      if (itemStaticInfo != null)
      {
        IconData iconData = new IconData();
        iconData.image = itemStaticInfo.ImagePath;
        iconData.contents = new string[2]
        {
          string.Empty,
          string.Empty
        };
        iconData.contents[0] = itemStaticInfo.LocName;
        iconData.contents[1] = items[index1 + 1].ToString();
        iconData.Data = (object) itemStaticInfo.internalId;
        this.GetIcon(index1).FeedData((IComponentData) iconData);
      }
      index1 += 2;
    }
  }
}
