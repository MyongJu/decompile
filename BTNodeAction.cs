﻿// Decompiled with JetBrains decompiler
// Type: BTNodeAction
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class BTNodeAction : BTNode
{
  private System.Action<object> _handler;
  private object _param;

  public BTNodeAction(System.Action<object> executeHandler, object param)
  {
    this._handler = executeHandler;
    this._param = param;
  }

  public override void Execute()
  {
    this.Status = BTNode.NodeStatus.Success;
    if (this._handler == null)
      return;
    this._handler(this._param);
  }
}
