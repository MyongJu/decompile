﻿// Decompiled with JetBrains decompiler
// Type: ConfigTreasureMap
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigTreasureMap
{
  private Dictionary<string, TreasureMapInfo> _dataById;
  private Dictionary<int, TreasureMapInfo> _dataByInternalId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<TreasureMapInfo, string>(res as Hashtable, "id", out this._dataById, out this._dataByInternalId);
  }

  public TreasureMapInfo Get(int interalId)
  {
    TreasureMapInfo treasureMapInfo;
    this._dataByInternalId.TryGetValue(interalId, out treasureMapInfo);
    return treasureMapInfo;
  }

  public TreasureMapInfo Get(string id)
  {
    TreasureMapInfo treasureMapInfo;
    this._dataById.TryGetValue(id, out treasureMapInfo);
    return treasureMapInfo;
  }

  public void Clear()
  {
    this._dataById = (Dictionary<string, TreasureMapInfo>) null;
    this._dataByInternalId = (Dictionary<int, TreasureMapInfo>) null;
  }
}
