﻿// Decompiled with JetBrains decompiler
// Type: HallOfKingDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UI;
using UnityEngine;

public class HallOfKingDlg : UI.Dialog
{
  private List<KingSkillItem> _allKingSkillItem = new List<KingSkillItem>();
  [SerializeField]
  private KingSkillItem _kingSkillItemTemplate;
  [SerializeField]
  private UITable _table;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UISprite _textureKingdomFlag;
  [SerializeField]
  private UILabel _labelKingdomName;
  [SerializeField]
  private UILabel _labelKingName;
  [SerializeField]
  private UIButton _buttonModifyMessage;
  [SerializeField]
  private UIInput _inputMessage;
  [SerializeField]
  private UILabel _labelKingdomSilver;
  private bool _listDirty;
  private string _previousMessage;

  protected HallOfKingData CurrentHallOfKingData
  {
    get
    {
      return DBManager.inst.DB_HallOfKing.Get((long) PlayerData.inst.userData.world_id);
    }
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._kingSkillItemTemplate.gameObject.SetActive(false);
    this.ClearAll();
    RequestManager.inst.SendRequest("king:loadHallOfKingData", (Hashtable) null, new System.Action<bool, object>(this.OnSendRequestCallback), true);
    MessageHub.inst.GetPortByAction("king:castKingSkill").AddEvent(new System.Action<object>(this.OnUseSkillCallback));
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    MessageHub.inst.GetPortByAction("king:castKingSkill").RemoveEvent(new System.Action<object>(this.OnUseSkillCallback));
  }

  protected void OnUseSkillCallback(object data)
  {
    this.UpdateAll();
  }

  protected void ClearAll()
  {
    this._textureKingdomFlag.gameObject.SetActive(false);
    this._labelKingdomName.text = string.Empty;
    this._labelKingName.text = string.Empty;
    this._inputMessage.value = string.Empty;
    this._inputMessage.GetComponent<BoxCollider>().enabled = false;
    this._labelKingdomSilver.text = string.Empty;
  }

  protected void OnSendRequestCallback(bool result, object data)
  {
    if (result)
      this.UpdateAll();
    else
      D.error((object) "PortType.KING_LOAD_HALL_OF_KING_DATA failed");
  }

  protected void UpdateAll()
  {
    this._listDirty = true;
    DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) PlayerData.inst.userData.world_id);
    if (wonderData == null)
    {
      D.error((object) "can not find wonder data.");
    }
    else
    {
      HallOfKingData currentHallOfKingData = this.CurrentHallOfKingData;
      if (currentHallOfKingData == null)
      {
        D.error((object) "can not find hall of king data");
      }
      else
      {
        UserData userData = DBManager.inst.DB_User.Get(wonderData.KING_UID);
        if (wonderData.FLAG != 0)
        {
          this._textureKingdomFlag.gameObject.SetActive(true);
          this._textureKingdomFlag.spriteName = wonderData.FlagIconPath;
        }
        this._labelKingdomName.text = wonderData.LabelName;
        this._labelKingName.text = userData == null ? string.Empty : userData.userName_Alliance_Name;
        this._inputMessage.value = currentHallOfKingData.KingMessage;
        this._labelKingdomSilver.text = ScriptLocalization.Get("throne_kings_chamber_currency", true) + (object) currentHallOfKingData.KingdomCoin;
        this._buttonModifyMessage.isEnabled = false;
        this._buttonModifyMessage.gameObject.SetActive(PlayerData.inst.uid == wonderData.KING_UID);
        this._inputMessage.GetComponent<BoxCollider>().enabled = PlayerData.inst.uid == wonderData.KING_UID;
      }
    }
  }

  public void Update()
  {
    if (!this._listDirty)
      return;
    this._listDirty = false;
    this.UpdateSkillList();
  }

  public void UpdateSkillList()
  {
    this.DestoryAllKingSkillItem();
    using (List<KingSkillInfo>.Enumerator enumerator = ConfigManager.inst.DB_KingSkill.GetAll().GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.CreateKingSkillItem().SetSkillData(enumerator.Current);
    }
    this._table.repositionNow = true;
    this._table.Reposition();
    this._scrollView.movement = UIScrollView.Movement.Unrestricted;
    this._scrollView.ResetPosition();
    this._scrollView.currentMomentum = Vector3.zero;
    this._scrollView.movement = UIScrollView.Movement.Vertical;
  }

  protected void DestoryAllKingSkillItem()
  {
    using (List<KingSkillItem>.Enumerator enumerator = this._allKingSkillItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this._allKingSkillItem.Clear();
  }

  protected KingSkillItem CreateKingSkillItem()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._kingSkillItemTemplate.gameObject);
    gameObject.SetActive(true);
    gameObject.transform.SetParent(this._table.transform);
    gameObject.transform.localScale = Vector3.one;
    KingSkillItem component = gameObject.GetComponent<KingSkillItem>();
    this._allKingSkillItem.Add(component);
    return component;
  }

  public void OnButtonModifyMessageClicked()
  {
    string str = IllegalWordsUtils.Filter(this._inputMessage.value);
    this._inputMessage.value = string.Empty;
    this._inputMessage.value = str;
    Hashtable postData = new Hashtable()
    {
      {
        (object) "msg",
        (object) this._inputMessage.value
      }
    };
    RequestManager.inst.SendRequest("king:setHallOfKingMessage", postData, new System.Action<bool, object>(this.OnRequestModifyMessageCallback), true);
  }

  protected void OnRequestModifyMessageCallback(bool result, object data)
  {
    if (!result)
      return;
    this.OnTextChanged();
  }

  public void OnButtonShowHistoryClicked()
  {
    UIManager.inst.OpenPopup("HallOfKing/KingSkillHistoryPopup", (Popup.PopupParameter) null);
  }

  public void OnTextChanged()
  {
    string s = this._inputMessage.value.Replace("[", "【").Replace("]", "】").Replace("\n", string.Empty);
    if (Encoding.UTF8.GetBytes(s).Length > 300)
      s = this._previousMessage;
    this._previousMessage = s;
    this._inputMessage.value = s;
    int length = Encoding.UTF8.GetBytes(s).Length;
    this._buttonModifyMessage.isEnabled = this.CurrentHallOfKingData != null && this._inputMessage.value != this.CurrentHallOfKingData.KingMessage && length >= 0 && length <= 300;
  }

  public void OnGotoAvalonClicked()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    LinkerHub.Instance.LHGotoTile(new LPTile()
    {
      k = PlayerData.inst.userData.world_id,
      x = 640,
      y = 1274
    });
  }
}
