﻿// Decompiled with JetBrains decompiler
// Type: LocalDataConst
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class LocalDataConst
{
  public const string talentTreeIndex = "talentTreeIndex";
  public const string DRAGON_KNIGHT_TALENT_TREE_INDEX = "dragon_knight_talent_tree_index";
  public const string SETTING_INIT = "setting_init";
  public const string SETTING_MUSIC_PLAY = "music_play";
  public const string SETTING_SFX_PLAY = "sfx_play";
  public const string SETTING_ENVIRONMENT = "environment";
  public const string SETTING_DALTON_MODE = "dalton_mode";
  public const string SETTING_PRIORITY_SELECTION = "priority_selection";
  public const string SETTING_NOFICATION = "setting_nofications";
  public const string NOTIFICATION_REQUEST_CODE = "notification_request_code";
  public const string ACTIVITY_KNIGHT_SHOW_TIME = "activity_kngith_start_show_time";
  public const string ACTIVITY_KNIGHT_START_TIME = "activity_kngith_start_time";
  public const string ACTIVITY_KNIGHT_END_TIME = "activity_kngith_end_time";
  public const string ACTIVITY_GOLD_SHOW_TIME = "activity_gold_show_time";
  public const string ACTIVITY_GOLD_START_TIME = "activity_gold_start_time";
  public const string ACTIVITY_GOLD_END_TIME = "activity_gold_end_time";
  public const string SEVEN_DAYS_TIME = "seven_days_show_time";
  public const string MONTHLY_SPECIAL_SHOW_TIME = "monthly_special_show_time";
  public const string ROULETTE_LAST_STOP_POSITION = "roulette_last_stop_position";
  public const string FLAG_TIME_SHARED_BOOKMARK = "bookmarch_shared_time";
  public const string CHAT_UNREAD_MESSAGE_PRE = "chat_unread_";
  public const string CHAT_READ_MESSAGE_TIME_PRE = "chat_read_time_";
}
