﻿// Decompiled with JetBrains decompiler
// Type: ItemSkillBenefitCompareSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class ItemSkillBenefitCompareSlot : MonoBehaviour
{
  private static Color INCREASED_COLOR = Color.green;
  private static Color DECREASED_COLOR = Color.red;
  [SerializeField]
  private ItemSkillBenefitCompareSlot.Panel panel;

  public void Setup(PropertyDefinition info, string beforeValue, string afterValue, bool isIncreased, UIScrollView scrollView)
  {
    this.panel.icon.spriteName = info.Image;
    this.panel.title.text = info.Name;
    this.panel.beforeValue.text = beforeValue;
    this.panel.afterValue.text = afterValue;
    if (isIncreased)
    {
      this.panel.arrowIcon.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 90f);
      this.panel.arrowIcon.color = ItemSkillBenefitCompareSlot.INCREASED_COLOR;
      this.panel.beforeValue.color = ItemSkillBenefitCompareSlot.INCREASED_COLOR;
      this.panel.beforeValue.fontSize = 45;
      this.panel.afterValue.color = ItemSkillBenefitCompareSlot.INCREASED_COLOR;
      this.panel.afterValue.fontSize = 60;
    }
    else
    {
      this.panel.arrowIcon.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, -90f);
      this.panel.arrowIcon.color = ItemSkillBenefitCompareSlot.DECREASED_COLOR;
      this.panel.beforeValue.color = ItemSkillBenefitCompareSlot.DECREASED_COLOR;
      this.panel.beforeValue.fontSize = 60;
      this.panel.afterValue.color = ItemSkillBenefitCompareSlot.DECREASED_COLOR;
      this.panel.afterValue.fontSize = 45;
    }
    this.panel.dragScrollView.scrollView = scrollView;
  }

  [Serializable]
  protected class Panel
  {
    public UISprite icon;
    public UISprite affectIcon;
    public UILabel title;
    public UILabel beforeValue;
    public UILabel afterValue;
    public UISprite arrowIcon;
    public UIDragScrollView dragScrollView;
  }
}
