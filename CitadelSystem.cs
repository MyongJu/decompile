﻿// Decompiled with JetBrains decompiler
// Type: CitadelSystem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class CitadelSystem : MonoBehaviour
{
  public float mFocusTime = 0.1f;
  private Vector2 _targetMapRoot = new Vector2(0.0f, 0.0f);
  private List<GameObject> gobBuildings = new List<GameObject>();
  private CitadelSystem.CameraPara cp = new CitadelSystem.CameraPara();
  private List<long> collectionList = new List<long>();
  public const float buildingOffset = 2000f;
  public const float SceneUIZOffset = -2000f;
  public const string EffectPath = "Prefab/VFX/";
  public const string idleEffect = "fx_jianzhukongxian";
  public const string clickEffect = "fx_shubiaodianji";
  public const string foodCollectionAnim = "fx_foodcollection";
  public const string woodCollectionAnim = "fx_woodcollection";
  public const string oreCollectionAnim = "fx_orecollection";
  public const string silverCollectionAnim = "fx_coincollection_02";
  public const string animPath = "Prefab/City/CityAnimation";
  public const string effectPath = "Prefab/City/CityEffect";
  public const string citymapPath = "Prefab/City/CityMap";
  public const string bgPath = "Prefab/City/CityBG";
  public const string animName = "CityAnimation(Clone)";
  public const string effectName = "CityMap/CityEffect(Clone)";
  public const string citymapName = "CityMap";
  public const string bgName = "CityBG(Clone)";
  public PublicHUD mPublicHUD;
  private bool _inFocusState;
  private float _dragFactorCache;
  private float _zoomFactorCache;
  private CityMapPrefabs mCMP;
  public CityTroopsAnimManager cityTroopAnimManager;
  public ParadeGroundManager paradeGroundManager;
  private GameObject m3DPeopleRoot;
  private BuildingController mStronghold;
  private BuildingController mWalls;
  private BuildingController mWatchTower;
  private static CitadelSystem _singleton;
  private bool isInZoomState;
  private BuildingController bp;
  private Twinkler twinklerInst;
  private GameObject clickEffectMountPoint;
  public GameObject idleEffectGOB;
  public GameObject foodCollectionGOB;
  public GameObject woodCollectionGOB;
  public GameObject oreCollectionGOB;
  public GameObject silverCollectionGOB;
  private bool initialized;
  private bool preloadFinished;
  public GameObject citybggo;
  private GameObject cityanimgo;
  private GameObject cityeffectgo;

  public GameObject MapRoot
  {
    get
    {
      return this.mCMP.MapRoot;
    }
  }

  public CityMapPrefabs CMP
  {
    get
    {
      return this.mCMP;
    }
    set
    {
      this.mCMP = value;
    }
  }

  public Transform BuildingRoot
  {
    get
    {
      return this.mCMP.BuildingRoot.transform;
    }
  }

  public BuildingController MStronghold
  {
    get
    {
      return this.mStronghold;
    }
  }

  public BuildingController WatchTower
  {
    get
    {
      return this.mWatchTower;
    }
  }

  public List<GameObject> GobBuildings
  {
    get
    {
      return this.gobBuildings;
    }
  }

  public static CitadelSystem inst
  {
    get
    {
      if ((UnityEngine.Object) CitadelSystem._singleton == (UnityEngine.Object) null)
      {
        GameObject gameObject = new GameObject();
        CitadelSystem._singleton = gameObject.AddComponent<CitadelSystem>();
        gameObject.AddComponent<DontDestroy>();
        gameObject.name = nameof (CitadelSystem);
      }
      return CitadelSystem._singleton;
    }
  }

  internal bool CheckSlotHasBuilding(int s)
  {
    using (List<GameObject>.Enumerator enumerator = this.gobBuildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuildingController component = enumerator.Current.GetComponent<BuildingController>();
        if (component.mBuildingItem != null && s == component.mBuildingItem.mSlotid)
          return true;
      }
    }
    return false;
  }

  public void FocusUIPoint(GameObject obj, Vector3 offset)
  {
    Vector3 worldPoint = UIManager.inst.ui2DCamera.GetComponent<Camera>().ScreenToWorldPoint(offset);
    this.Goto(obj, worldPoint);
  }

  public void FocusCityMapPoint(GameObject obj, Vector3 offset)
  {
    Vector3 worldPoint = UIManager.inst.cityCamera.GetComponent<Camera>().ScreenToWorldPoint(offset);
    this.Goto(obj, worldPoint);
  }

  public void Goto(GameObject obj, Vector3 offset)
  {
    Vector3 inTarget = UIManager.inst.cityCamera.transform.position + obj.transform.position - offset;
    CityCamera cityCamera = UIManager.inst.cityCamera;
    cityCamera.SetTargetPosition(inTarget, true, true);
    if (this._inFocusState)
      return;
    this._zoomFactorCache = cityCamera.zoomSmoothFactor;
    this._dragFactorCache = cityCamera.dragSmoothEaseFactor;
    this._inFocusState = true;
    cityCamera.dragSmoothEaseFactor = this.mFocusTime;
    cityCamera.zoomSmoothFactor = this.mFocusTime;
  }

  private void OnPlotClicked(int zoneType, GameObject targetPlot, string buildingType)
  {
    BuildingController.UnselectBuilding();
    this.OnBuildBuilding(zoneType, targetPlot, buildingType);
    AudioManager.Instance.PlaySound("sfx_city_click_space", false);
  }

  private void OnBuildBuilding(int zoneType, GameObject targetPlot, string buildingType)
  {
    BuildingController.UnselectBuilding();
    UIManager.inst.OpenDlg("Construction/ConstructionSelectDlg", (UI.Dialog.DialogParameter) new ConstructionSelectDlg.Parameter()
    {
      zone = zoneType,
      targetPlot = targetPlot,
      buildingType = buildingType
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private void OnForgeBtnPressed()
  {
    UIManager.inst.OpenDlg("Equipment/EquipmentForgeDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnArmoryBtnPressed()
  {
    UIManager.inst.OpenDlg("Equipment/EquipmentTreasuryDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnEnhanceBtnPressed()
  {
    UIManager.inst.OpenDlg("Equipment/EquipmentEnhanceDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnBuildingInfoPressed()
  {
    string mType = BuildingController.mSelectedBuildingItem.mType;
    if (mType != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CitadelSystem.\u003C\u003Ef__switch\u0024map33 == null)
      {
        // ISSUE: reference to a compiler-generated field
        CitadelSystem.\u003C\u003Ef__switch\u0024map33 = new Dictionary<string, int>(1)
        {
          {
            "stronghold",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (CitadelSystem.\u003C\u003Ef__switch\u0024map33.TryGetValue(mType, out num) && num == 0)
      {
        UIManager.inst.OpenDlg(BuildingInfoUtils.GetAssetName(BuildingController.mSelectedBuildingItem.mType), (UI.Dialog.DialogParameter) new BuildingInfoStronghold.Parameter()
        {
          buildingItem = BuildingController.mSelectedBuildingItem
        }, 1 != 0, 1 != 0, 1 != 0);
        return;
      }
    }
    UIManager.inst.OpenDlg(BuildingInfoUtils.GetAssetName(BuildingController.mSelectedBuildingItem.mType), (UI.Dialog.DialogParameter) new BuildingInfoBaseDlg.Parameter()
    {
      buildingItem = BuildingController.mSelectedBuildingItem
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnCityInfoBtnPressed()
  {
    UIManager.inst.OpenPopup("CityStatsPopup", (Popup.PopupParameter) null);
  }

  private void LockHUD()
  {
    this.mPublicHUD.LockButtons();
  }

  private void UnlockHUD()
  {
    this.mPublicHUD.UnlockButtons();
  }

  private void OnMessagesBtn()
  {
    UIManager.inst.OpenDlg("Mail/MailMainDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnItemsBtn()
  {
    UIManager.inst.OpenDlg("ItemStore/ItemStoreDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnUpgradeBuildingPressed()
  {
    int mLevel = BuildingController.mSelectedBuildingItem.mLevel;
    BuildingInfo info = this.GetCurSelectedBuildingInfo();
    string str = info.Building_ImagePath + "_l" + CityManager.inst.GetIconLevelFromBuildingLevel(mLevel + 1, (string) null).ToString();
    if ("stronghold" == BuildingController.mSelectedBuildingItem.mType && mLevel == 5)
      this.ShowPeaceShieldWarning((System.Action) (() => UIManager.inst.OpenDlg("BuildingConstruction", (UI.Dialog.DialogParameter) new BuildingConstruction.Parameter()
      {
        buildingid = info.Type,
        tarLevel = (BuildingController.mSelectedBuildingItem.mLevel + 1)
      }, 1 != 0, 1 != 0, 1 != 0)));
    else
      UIManager.inst.OpenDlg("BuildingConstruction", (UI.Dialog.DialogParameter) new BuildingConstruction.Parameter()
      {
        buildingid = info.Type,
        tarLevel = (BuildingController.mSelectedBuildingItem.mLevel + 1)
      }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void ShowPeaceShieldWarning(System.Action callBack)
  {
    long peaceShieldJobId = PlayerData.inst.playerCityData.peaceShieldJobId;
    if (peaceShieldJobId == 0L)
    {
      if (callBack == null)
        return;
      callBack();
    }
    else
    {
      JobHandle job = JobManager.Instance.GetJob(peaceShieldJobId);
      bool flag = false;
      if (job != null && job.Data != null)
      {
        Hashtable data = job.Data as Hashtable;
        if (data != null && data.ContainsKey((object) "fresher"))
          flag = bool.Parse(data[(object) "fresher"].ToString());
      }
      if (flag)
      {
        UIManager.inst.OpenPopup("ConfirmWithPeaceShield", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
        {
          setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
          title = ScriptLocalization.Get("upgrade_stronghold_lv6_ warning_title", true),
          description = ScriptLocalization.Get("upgrade_stronghold_lv6_ warning_description", true),
          leftButtonText = ScriptLocalization.Get("id_uppercase_no", true),
          rightButtonText = ScriptLocalization.Get("id_uppercase_yes", true),
          leftButtonClickEvent = (System.Action) null,
          rightButtonClickEvent = (System.Action) (() =>
          {
            if (callBack == null)
              return;
            callBack();
          })
        });
      }
      else
      {
        if (callBack == null)
          return;
        callBack();
      }
    }
  }

  public bool IsInZoomState
  {
    get
    {
      return this.isInZoomState;
    }
    set
    {
      this.isInZoomState = value;
    }
  }

  public void FocusConstructionBuilding(GameObject obj, UIWidget tar, float zoom = 0.0f)
  {
    if (this.cp.isreord)
      return;
    this.IsInZoomState = true;
    CityCamera cityCamera = UIManager.inst.cityCamera;
    Camera component = cityCamera.GetComponent<Camera>();
    float orthographicSize = component.orthographicSize;
    float num = Mathf.Clamp(zoom, cityCamera.zoomMinDis, cityCamera.zoomMaxDis);
    component.orthographicSize = num;
    Vector3 viewportPoint = UIManager.inst.ui2DCamera.WorldToViewportPoint(tar.transform.position);
    Vector3 worldPoint = component.ViewportToWorldPoint(viewportPoint);
    component.orthographicSize = orthographicSize;
    this.cp.cwpos = cityCamera.transform.position;
    this.cp.zoom = orthographicSize;
    this.cp.isreord = true;
    cityCamera.UseBoundary = false;
    cityCamera.TargetZoom = num;
    this.Goto(obj, worldPoint);
  }

  [DebuggerHidden]
  public IEnumerator BackToOriStats()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CitadelSystem.\u003CBackToOriStats\u003Ec__Iterator3C()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void MoveCameraBackToNormal()
  {
    CitadelSystem.inst.StopCoroutine(CitadelSystem.inst.BackToOriStats());
    CitadelSystem.inst.StartCoroutine(CitadelSystem.inst.BackToOriStats());
  }

  public BuildingInfo GetCurSelectedBuildingInfo()
  {
    if (BuildingController.mSelectedBuildingItem == null)
      return (BuildingInfo) null;
    return ConfigManager.inst.DB_Building.GetData(BuildingController.mSelectedBuildingItem.mType + "_" + BuildingController.mSelectedBuildingItem.mLevel.ToString());
  }

  private void OnTrainTroopsPressed()
  {
    UIManager.inst.OpenDlg("Barracks/TrainTroopDlg", (UI.Dialog.DialogParameter) new TrainTroopDlg.Parameter()
    {
      BuildingID = BuildingController.mSelectedBuildingItem.mID,
      BuildingType = BuildingController.mSelectedBuildingItem.mType
    }, true, true, true);
  }

  private void OnHospitalBtnPressed()
  {
    UIManager.inst.OpenDlg("HospitalHealDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnRallyBtnPressed()
  {
    BuildingController.UnselectBuilding();
    UIManager.inst.OpenDlg("Alliance/AllianceWarMainDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnReinforceBtnPressed()
  {
    BuildingController.UnselectBuilding();
    UIManager.inst.OpenDlg("EmbassyDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnTradeBtnPressed()
  {
    BuildingController.UnselectBuilding();
    if (CityManager.inst.GetHighestBuildingLevelFor("marketplace") > 0)
      UIManager.inst.OpenDlg("Alliance/AllianceTradeDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    else
      UIManager.inst.OpenDlg("NoMarketplaceDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnResearchBtnPressed()
  {
    UIManager.inst.OpenDlg("Research/ResearchBaseDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnLegendStationPressed()
  {
    UIManager.inst.OpenDlg("Legend/LegendTowerDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnrecruitLegendPressed()
  {
    UIManager.inst.OpenDlg("Legend/LegendRecruitmentDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnownedLegendPressed()
  {
    UIManager.inst.OpenDlg("Legend/LegendListDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnDragonAttributePressed()
  {
    UIManager.inst.OpenDlg("Dragon/DragonPropertyDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnDragonSkillPressed()
  {
    UIManager.inst.OpenDlg("Dragon/DragonSkillDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnDragonSkillEnhancePressed()
  {
    UIManager.inst.OpenDlg("Dragon/DragonSkillEnhanceDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnboostLegendPressed()
  {
    UIManager.inst.OpenDlg("Legend/LegendExpMenuDialog", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnBoostBtPressed()
  {
    UIManager.inst.OpenDlg("Boost/BoostMenu", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private GameObject GetFAB(CityMapPrefabs CMP, GameObject buildingType, int level)
  {
    GameObject gameObject = Utils.DuplicateGOB(buildingType);
    gameObject.transform.parent = this.BuildingRoot;
    gameObject.GetComponent<BuildingController>().SetLevel(level, false);
    gameObject.GetComponent<Collider>().enabled = false;
    gameObject.GetComponent<BuildingController>().enabled = false;
    return gameObject;
  }

  public GameObject GetRawBuilding(string buildingType, int level = 1)
  {
    string key = buildingType;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CitadelSystem.\u003C\u003Ef__switch\u0024map34 == null)
      {
        // ISSUE: reference to a compiler-generated field
        CitadelSystem.\u003C\u003Ef__switch\u0024map34 = new Dictionary<string, int>(21)
        {
          {
            "farm",
            0
          },
          {
            "lumber_mill",
            1
          },
          {
            "mine",
            2
          },
          {
            "house",
            3
          },
          {
            "barracks",
            4
          },
          {
            "stables",
            5
          },
          {
            "range",
            6
          },
          {
            "sanctum",
            7
          },
          {
            "workshop",
            8
          },
          {
            "forge",
            9
          },
          {
            "marketplace",
            10
          },
          {
            "storehouse",
            11
          },
          {
            "hospital",
            12
          },
          {
            "university",
            13
          },
          {
            "watchtower",
            14
          },
          {
            "embassy",
            15
          },
          {
            "war_rally",
            16
          },
          {
            "dragon_lair",
            17
          },
          {
            "fortress",
            18
          },
          {
            "military_tent",
            19
          },
          {
            "wish_well",
            20
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (CitadelSystem.\u003C\u003Ef__switch\u0024map34.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return this.GetFAB(this.CMP, this.CMP.mFarmBuilding, level);
          case 1:
            return this.GetFAB(this.CMP, this.CMP.mLumberMillBuilding, level);
          case 2:
            return this.GetFAB(this.CMP, this.CMP.mMineBuilding, level);
          case 3:
            return this.GetFAB(this.CMP, this.CMP.mHouseBuilding, level);
          case 4:
            return this.GetFAB(this.CMP, this.CMP.mBarracksBuilding, level);
          case 5:
            return this.GetFAB(this.CMP, this.CMP.mStablesBuilding, level);
          case 6:
            return this.GetFAB(this.CMP, this.CMP.mRangeBuilding, level);
          case 7:
            return this.GetFAB(this.CMP, this.CMP.mSanctumBuilding, level);
          case 8:
            return this.GetFAB(this.CMP, this.CMP.mWorkshopBuilding, level);
          case 9:
            return this.GetFAB(this.CMP, this.CMP.mForgeBuilding, level);
          case 10:
            return this.GetFAB(this.CMP, this.CMP.mMarketplaceBuilding, level);
          case 11:
            return this.GetFAB(this.CMP, this.CMP.mStorehouseBuilding, level);
          case 12:
            return this.GetFAB(this.CMP, this.CMP.mHospitalBuilding, level);
          case 13:
            return this.GetFAB(this.CMP, this.CMP.mUniversityBuilding, level);
          case 14:
            return this.GetFAB(this.CMP, this.CMP.mWatchTowerBuilding, level);
          case 15:
            return this.GetFAB(this.CMP, this.CMP.mEmbassyBuilding, level);
          case 16:
            return this.GetFAB(this.CMP, this.CMP.mWarRallyBuilding, level);
          case 17:
            return this.GetFAB(this.CMP, this.CMP.mDragonLairBuilding, level);
          case 18:
            return this.GetFAB(this.CMP, this.CMP.mFortressBuilding, level);
          case 19:
            return this.GetFAB(this.CMP, this.CMP.mMilitaryTentBuildng, level);
          case 20:
            return this.GetFAB(this.CMP, this.CMP.mWishWellBuilding, level);
        }
      }
    }
    D.warn((object) "Building type {0} not found!!", (object) buildingType);
    return (GameObject) null;
  }

  private void UpdateGoBuildings(BuildingController bc)
  {
    if (!this.gobBuildings.Contains(bc.gameObject))
      this.gobBuildings.Add(bc.gameObject);
    if (CityManager.inst.buildings.Count != this.gobBuildings.Count || this.Initialized)
      return;
    this.Initialized = true;
    this.InitConstBuildings();
    LinkerHub.Instance.Init();
    TimerBarManager.Instance.Startup();
    NotificationManager.Instance.CheckNotificationData();
    AndroidMonitorChecker.CreateChecker();
  }

  public void OnCreateBuilding(CityManager.BuildingItem buildingItem, long jobID)
  {
    string mType = buildingItem.mType;
    int mSlotid = buildingItem.mSlotid;
    int mLevel = buildingItem.mLevel;
    string key1 = mType;
    int num;
    if (key1 != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CitadelSystem.\u003C\u003Ef__switch\u0024map35 == null)
      {
        // ISSUE: reference to a compiler-generated field
        CitadelSystem.\u003C\u003Ef__switch\u0024map35 = new Dictionary<string, int>(5)
        {
          {
            "walls",
            0
          },
          {
            "stronghold",
            1
          },
          {
            "PARADEGROUND",
            2
          },
          {
            "watchtower",
            3
          },
          {
            "dragon_lair",
            4
          }
        };
      }
      // ISSUE: reference to a compiler-generated field
      if (CitadelSystem.\u003C\u003Ef__switch\u0024map35.TryGetValue(key1, out num))
      {
        switch (num)
        {
          case 0:
            BuildingControllerNew component1 = this.CMP.mWallBuilding.GetComponent<BuildingControllerNew>();
            component1.mBuildingItem = buildingItem;
            component1.mBuildingID = buildingItem.mID;
            component1.SetMapLocation(5);
            this.mWalls = (BuildingController) component1;
            this.CheckUpgrading(jobID, (BuildingController) component1);
            this.UpdateGoBuildings((BuildingController) component1);
            return;
          case 1:
            BuildingControllerNew component2 = this.CMP.mStrongholdBuilding.GetComponent<BuildingControllerNew>();
            component2.mBuildingItem = buildingItem;
            component2.mBuildingID = buildingItem.mID;
            component2.SetMapLocation(4);
            this.mStronghold = (BuildingController) component2;
            this.CheckUpgrading(jobID, (BuildingController) component2);
            this.UpdateGoBuildings((BuildingController) component2);
            return;
          case 2:
            BuildingControllerNew component3 = this.CMP.mParadeGround.GetComponent<BuildingControllerNew>();
            component3.SetMapLocation(6);
            this.UpdateGoBuildings((BuildingController) component3);
            return;
          case 3:
            BuildingControllerNew component4 = this.CMP.mWatchTowerBuilding.GetComponent<BuildingControllerNew>();
            component4.mBuildingItem = buildingItem;
            component4.mBuildingID = buildingItem.mID;
            component4.SetMapLocation(10);
            this.mWatchTower = (BuildingController) component4;
            this.CheckUpgrading(jobID, (BuildingController) component4);
            this.UpdateGoBuildings((BuildingController) component4);
            return;
          case 4:
            BuildingControllerNew component5 = this.CMP.mDragonLairBuilding.GetComponent<BuildingControllerNew>();
            component5.mBuildingItem = buildingItem;
            component5.mBuildingID = buildingItem.mID;
            component5.SetMapLocation(12);
            this.CheckUpgrading(jobID, (BuildingController) component5);
            this.UpdateGoBuildings((BuildingController) component5);
            return;
        }
      }
    }
    string key2 = mType;
    if (key2 == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (CitadelSystem.\u003C\u003Ef__switch\u0024map36 == null)
    {
      // ISSUE: reference to a compiler-generated field
      CitadelSystem.\u003C\u003Ef__switch\u0024map36 = new Dictionary<string, int>(18)
      {
        {
          "farm",
          0
        },
        {
          "lumber_mill",
          1
        },
        {
          "mine",
          2
        },
        {
          "house",
          3
        },
        {
          "barracks",
          4
        },
        {
          "stables",
          5
        },
        {
          "range",
          6
        },
        {
          "workshop",
          7
        },
        {
          "forge",
          8
        },
        {
          "marketplace",
          9
        },
        {
          "storehouse",
          10
        },
        {
          "hospital",
          11
        },
        {
          "university",
          12
        },
        {
          "embassy",
          13
        },
        {
          "war_rally",
          14
        },
        {
          "fortress",
          15
        },
        {
          "military_tent",
          16
        },
        {
          "wish_well",
          17
        }
      };
    }
    // ISSUE: reference to a compiler-generated field
    if (!CitadelSystem.\u003C\u003Ef__switch\u0024map36.TryGetValue(key2, out num))
      return;
    switch (num)
    {
      case 0:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mFarmBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 1:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mLumberMillBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 2:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mMineBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 3:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mHouseBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 4:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mBarracksBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 5:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mStablesBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 6:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mRangeBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 7:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mWorkshopBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 8:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mForgeBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 9:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mMarketplaceBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 10:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mStorehouseBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 11:
        this.SpawnBuildingCallback(PrefabManagerEx.Instance.Spawn(this.CMP.mHospitalBuilding, this.BuildingRoot), (object) buildingItem);
        break;
      case 12:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mUniversityBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 13:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mEmbassyBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 14:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mWarRallyBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 15:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mFortressBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 16:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mMilitaryTentBuildng, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
      case 17:
        PrefabManagerEx.Instance.SpawnAsync(this.CMP.mWishWellBuilding, this.BuildingRoot, new PrefabSpawnRequestCallback(this.SpawnBuildingCallback), (object) buildingItem);
        break;
    }
  }

  public void SpawnBuildingCallback(GameObject go, object userData)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      return;
    CityManager.BuildingItem buildingItem = userData as CityManager.BuildingItem;
    go.SetActive(true);
    float urbanScale = this.MapRoot.transform.parent.GetComponent<CityMapPrefabs>().urbanScale;
    BuildingControllerNew component = go.GetComponent<BuildingControllerNew>();
    component.onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    component.SetMapLocation(buildingItem.mSlotid);
    component.mBuildingItem = buildingItem;
    component.mBuildingID = buildingItem.mID;
    component.InitBuilding();
    this.UpdateGoBuildings((BuildingController) component);
    this.CheckUpgrading(buildingItem.mBuildingJobID, (BuildingController) component);
    string mType = buildingItem.mType;
    if ("barracks" == mType || "stables" == mType || ("range" == mType || "sanctum" == mType) || ("workshop" == mType || "fortress" == mType))
      this.CheckCollection((BuildingController) component);
    if ("forge" == mType)
      this.CheckForgeCollection((BuildingController) component);
    NewTutorial.Instance.CheckTrigger((BuildingController) component);
  }

  private void CheckUpgrading(long jobID, BuildingController BC)
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) BC))
      return;
    BC.RefreshStats();
  }

  private void CheckWarning(BuildingController BC)
  {
    if (WatchtowerUtilities.Entities.Count <= 0)
      return;
    BC.ShowWatchTowerWarningAnim();
  }

  private void CheckCollection(BuildingController BC)
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) BC) || !BC.mBuildingItem.mInBuilding || !((UnityEngine.Object) null == (UnityEngine.Object) BC.GetComponentInChildren<CollectionUI>()))
      return;
    CityCircleBtnPara para = new CityCircleBtnPara(AtlasType.Gui_2, "btn_finish_train", !(BC.mBuildingItem.mType == "fortress") ? "id_train" : "id_build", new EventDelegate((EventDelegate.Callback) (() => BarracksManager.Instance.CollectBuilding(BC.mBuildingID, (System.Action) null))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    CollectionUIManager.Instance.OpenCollectionUI(BC.transform, para, CollectionUI.CollectionType.TrainSoldier);
  }

  private void CheckForgeCollection(BuildingController BC)
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) BC) || !BC.mBuildingItem.mInBuilding || !((UnityEngine.Object) null == (UnityEngine.Object) BC.GetComponentInChildren<CollectionUI>()))
      return;
    CityCircleBtnPara para = new CityCircleBtnPara(AtlasType.Gui_2, "btn_finish_forging", !(BC.mBuildingItem.mType == "forge") ? "id_train" : "id_build", new EventDelegate((EventDelegate.Callback) (() => EquipmentManager.Instance.CollectEquipment(BagType.Hero, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      Hashtable hashtable = arg2 as Hashtable;
      UIManager.inst.OpenPopup("Equipment/EquipmentForgeSuccessfullyPopup", (Popup.PopupParameter) new EquipmentForgeSuccessfullyPopup.Parameter()
      {
        bagType = BagType.Hero,
        equipmenItemID = (long) int.Parse(hashtable[(object) "item_id"].ToString())
      });
    })))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    CollectionUIManager.Instance.OpenCollectionUI(BC.transform, para, CollectionUI.CollectionType.TrainSoldier);
  }

  public void CreateBuildingPreview(string buildingType, int slotid)
  {
    this.DeleteBuildingPreview();
    this.bp = this.GetRawBuilding(buildingType, 1).GetComponent<BuildingController>();
    this.bp.name = "preview";
    this.bp.transform.localScale = Vector3.one;
    this.bp.SetMapLocation(slotid);
  }

  public void DeleteBuildingPreview()
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.bp))
      return;
    CityPlotController.SetVisible(this.bp.SlotID, true);
    this.bp.transform.parent = (Transform) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.bp.gameObject);
    this.bp = (BuildingController) null;
  }

  public void OnUpgradeBuilding(CityManager.BuildingItem buildingItem, bool hasjob)
  {
    string mType = buildingItem.mType;
    if (mType != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CitadelSystem.\u003C\u003Ef__switch\u0024map37 == null)
      {
        // ISSUE: reference to a compiler-generated field
        CitadelSystem.\u003C\u003Ef__switch\u0024map37 = new Dictionary<string, int>(0);
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (!CitadelSystem.\u003C\u003Ef__switch\u0024map37.TryGetValue(mType, out num))
        ;
    }
    using (List<GameObject>.Enumerator enumerator = this.gobBuildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuildingController component = enumerator.Current.GetComponent<BuildingController>();
        if (component.IsID(buildingItem.mID))
        {
          if (hasjob)
            component.ShowUpgradeAnim();
          else
            component.HideUpgradeAnim();
          component.OnChanged();
          break;
        }
      }
    }
  }

  public BuildingController GetUniversity()
  {
    return this.GetBuildingByType("university");
  }

  public BuildingController GetFortress()
  {
    return this.GetBuildingByType("fortress");
  }

  public BuildingController GetForge()
  {
    return this.GetBuildingByType("forge");
  }

  public BuildingController GetDKForge()
  {
    return this.CMP.mDragonKnight.GetComponent<BuildingController>();
  }

  public BuildingController GetTrainBuildingbyUnitID(string unitID)
  {
    return this.GetBuildingByType(ConfigManager.inst.DB_Unit_Statistics.GetData(unitID).FromBuildingType);
  }

  public string GetBuildingTypebyUnitID(string unitID)
  {
    return ConfigManager.inst.DB_Unit_Statistics.GetData(unitID).FromBuildingType;
  }

  public List<BuildingController> GetHospitals()
  {
    List<BuildingController> buildingControllerList = new List<BuildingController>();
    using (List<GameObject>.Enumerator enumerator = this.gobBuildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuildingController component = enumerator.Current.GetComponent<BuildingController>();
        if (!((UnityEngine.Object) component == (UnityEngine.Object) null) && component.mBuildingItem != null && (component.mBuildingItem.mType == "hospital" && component.mBuildingItem.mBuildingJobID == 0L))
          buildingControllerList.Add(component);
      }
    }
    return buildingControllerList;
  }

  public BuildingController GetBuildingByType(string type)
  {
    using (List<GameObject>.Enumerator enumerator = this.gobBuildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuildingController component = enumerator.Current.GetComponent<BuildingController>();
        if (component.mBuildingItem != null && component.mBuildingItem.mType == type)
          return component;
      }
    }
    return (BuildingController) null;
  }

  public List<BuildingController> GetBuildingsByType(string type)
  {
    List<BuildingController> buildingControllerList = new List<BuildingController>();
    using (List<GameObject>.Enumerator enumerator = this.gobBuildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuildingController component = enumerator.Current.GetComponent<BuildingController>();
        if (component.mBuildingItem != null && component.mBuildingItem.mType == type)
          buildingControllerList.Add(component);
      }
    }
    buildingControllerList.Sort((Comparison<BuildingController>) ((bc1, bc2) =>
    {
      if (bc1.mBuildingItem.mLevel < bc2.mBuildingItem.mLevel)
        return -1;
      return bc1.mBuildingItem.mLevel > bc2.mBuildingItem.mLevel ? 1 : 0;
    }));
    return buildingControllerList;
  }

  public BuildingController GetBuildControllerFromBuildingItem(CityManager.BuildingItem buildingItem)
  {
    if (buildingItem == null)
      return (BuildingController) null;
    BuildingController buildingController = (BuildingController) null;
    string mType = buildingItem.mType;
    if (mType != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CitadelSystem.\u003C\u003Ef__switch\u0024map38 == null)
      {
        // ISSUE: reference to a compiler-generated field
        CitadelSystem.\u003C\u003Ef__switch\u0024map38 = new Dictionary<string, int>(0);
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (!CitadelSystem.\u003C\u003Ef__switch\u0024map38.TryGetValue(mType, out num))
        ;
    }
    using (List<GameObject>.Enumerator enumerator = this.gobBuildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuildingController component = enumerator.Current.GetComponent<BuildingController>();
        if ((UnityEngine.Object) null != (UnityEngine.Object) component && component.IsID(buildingItem.mID))
        {
          buildingController = component;
          break;
        }
      }
    }
    return buildingController;
  }

  public BuildingController GetBuildControllerFromBuildingID(long buildingid)
  {
    BuildingController buildingController = (BuildingController) null;
    using (List<GameObject>.Enumerator enumerator = this.gobBuildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuildingController component = enumerator.Current.GetComponent<BuildingController>();
        if ((UnityEngine.Object) null != (UnityEngine.Object) component && component.IsID(buildingid))
        {
          buildingController = component;
          break;
        }
      }
    }
    return buildingController;
  }

  public BuildingControllerResource GetAlmostFullResourceBuilding()
  {
    BuildingControllerResource controllerResource = (BuildingControllerResource) null;
    float maxValue = float.MaxValue;
    using (List<GameObject>.Enumerator enumerator = this.gobBuildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuildingControllerResource component = enumerator.Current.GetComponent<BuildingControllerResource>();
        if ((UnityEngine.Object) null != (UnityEngine.Object) component && (double) component.GetFullLeftTime() < (double) maxValue)
          controllerResource = component;
      }
    }
    return controllerResource;
  }

  public BuildingController GetBuildControllerFromBuildingInfo(BuildingInfo bi)
  {
    if (bi == null)
      return (BuildingController) null;
    BuildingController buildingController = (BuildingController) null;
    string type = bi.Type;
    if (type != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CitadelSystem.\u003C\u003Ef__switch\u0024map39 == null)
      {
        // ISSUE: reference to a compiler-generated field
        CitadelSystem.\u003C\u003Ef__switch\u0024map39 = new Dictionary<string, int>(0);
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (!CitadelSystem.\u003C\u003Ef__switch\u0024map39.TryGetValue(type, out num))
        ;
    }
    using (List<GameObject>.Enumerator enumerator = this.gobBuildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuildingController component = enumerator.Current.GetComponent<BuildingController>();
        if ((UnityEngine.Object) null != (UnityEngine.Object) component && component.mBuildingItem != null && (component.mBuildingItem.mType == bi.Type && component.mBuildingItem.mLevel == bi.Building_Lvl))
        {
          buildingController = component;
          break;
        }
      }
    }
    return buildingController;
  }

  public void OnUpgradeFinished(string type, long buildingId)
  {
    using (List<GameObject>.Enumerator enumerator = this.gobBuildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuildingController component = enumerator.Current.GetComponent<BuildingController>();
        if (component.IsID(buildingId))
        {
          component.UpgradeFinished(true);
          break;
        }
      }
    }
  }

  public void OnUpgradeCancelled(string type, long buildingId)
  {
    string key = type;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CitadelSystem.\u003C\u003Ef__switch\u0024map3A == null)
      {
        // ISSUE: reference to a compiler-generated field
        CitadelSystem.\u003C\u003Ef__switch\u0024map3A = new Dictionary<string, int>(0);
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (!CitadelSystem.\u003C\u003Ef__switch\u0024map3A.TryGetValue(key, out num))
        ;
    }
    using (List<GameObject>.Enumerator enumerator = this.gobBuildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuildingController component = enumerator.Current.GetComponent<BuildingController>();
        if (component.IsID(buildingId))
        {
          component.UpgradeFinished(false);
          break;
        }
      }
    }
  }

  public void OnDestroyBuilding(long buildingId, int slotid)
  {
    BuildingController.UnselectBuilding();
    using (List<GameObject>.Enumerator enumerator = this.gobBuildings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        BuildingControllerNew component = current.GetComponent<BuildingControllerNew>();
        if (component.mBuildingID == buildingId)
        {
          CityPlotController.SetVisible(slotid, true);
          CollectionUI componentInChildren = current.GetComponentInChildren<CollectionUI>();
          if ((UnityEngine.Object) null != (UnityEngine.Object) componentInChildren && componentInChildren.CT == CollectionUI.CollectionType.IdleHint)
            BuildingHintManager.Instance.Hashint = false;
          component.Dispose();
          UnityEngine.Object.DestroyObject((UnityEngine.Object) current);
          this.gobBuildings.Remove(current);
          break;
        }
      }
    }
  }

  public void OnTroopsTrained(string troopClass, int trainingCount)
  {
    UIManager.inst.toast.Show("Troops have finished training.", (System.Action) null, 4f, false);
  }

  public void OnBuildingSelected()
  {
    this.OnShowActionButtons();
    this.PlaySound();
  }

  private void PlaySound()
  {
    if (BuildingController.mSelectedBuildingItem == null)
      return;
    string mType = BuildingController.mSelectedBuildingItem.mType;
    if (mType == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (CitadelSystem.\u003C\u003Ef__switch\u0024map3B == null)
    {
      // ISSUE: reference to a compiler-generated field
      CitadelSystem.\u003C\u003Ef__switch\u0024map3B = new Dictionary<string, int>(24)
      {
        {
          "stronghold",
          0
        },
        {
          "university",
          1
        },
        {
          "barracks",
          2
        },
        {
          "fortress",
          3
        },
        {
          "stables",
          4
        },
        {
          "range",
          5
        },
        {
          "sanctum",
          6
        },
        {
          "workshop",
          7
        },
        {
          "war_rally",
          8
        },
        {
          "hospital",
          9
        },
        {
          "embassy",
          10
        },
        {
          "walls",
          11
        },
        {
          "watchtower",
          12
        },
        {
          "marketplace",
          13
        },
        {
          "PARADEGROUND",
          14
        },
        {
          "storehouse",
          15
        },
        {
          "forge",
          16
        },
        {
          "farm",
          17
        },
        {
          "military_tent",
          18
        },
        {
          "lumber_mill",
          19
        },
        {
          "mine",
          20
        },
        {
          "house",
          21
        },
        {
          "dragon_lair",
          22
        },
        {
          "wish_well",
          23
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!CitadelSystem.\u003C\u003Ef__switch\u0024map3B.TryGetValue(mType, out num))
      return;
    switch (num)
    {
      case 0:
        AudioManager.Instance.PlaySound("sfx_city_click_stronghold", false);
        break;
      case 1:
        AudioManager.Instance.PlaySound("sfx_city_click_university", false);
        break;
      case 2:
        AudioManager.Instance.PlaySound("sfx_city_click_barracks", false);
        break;
      case 3:
        AudioManager.Instance.PlaySound("sfx_city_click_trap_factory", false);
        break;
      case 4:
        AudioManager.Instance.PlaySound("sfx_city_click_stables", false);
        break;
      case 5:
        AudioManager.Instance.PlaySound("sfx_city_click_range", false);
        break;
      case 6:
        AudioManager.Instance.PlaySound("sfx_city_click_sanctum", false);
        break;
      case 7:
        AudioManager.Instance.PlaySound("sfx_city_click_workshop", false);
        break;
      case 8:
        AudioManager.Instance.PlaySound("sfx_city_click_hall_of_war", false);
        break;
      case 9:
        AudioManager.Instance.PlaySound("sfx_city_click_hospital", false);
        break;
      case 10:
        AudioManager.Instance.PlaySound("sfx_city_click_embassy", false);
        break;
      case 11:
        AudioManager.Instance.PlaySound("sfx_city_click_wall", false);
        break;
      case 12:
        AudioManager.Instance.PlaySound("sfx_city_click_watchtower", false);
        break;
      case 13:
        AudioManager.Instance.PlaySound("sfx_city_click_merchant_guild", false);
        break;
      case 14:
        AudioManager.Instance.PlaySound("sfx_city_click_parade_ground", false);
        break;
      case 15:
        AudioManager.Instance.PlaySound("sfx_city_click_storehouse", false);
        break;
      case 16:
        AudioManager.Instance.PlaySound("sfx_city_click_forge", false);
        break;
      case 17:
        AudioManager.Instance.PlaySound("sfx_rural_click_farm", false);
        break;
      case 18:
        AudioManager.Instance.PlaySound("sfx_rural_click_military_tent", false);
        break;
      case 19:
        AudioManager.Instance.PlaySound("sfx_rural_click_sawmill", false);
        break;
      case 20:
        AudioManager.Instance.PlaySound("sfx_rural_click_mine", false);
        break;
      case 21:
        AudioManager.Instance.PlaySound("sfx_rural_click_house", false);
        break;
      case 22:
        AudioManager.Instance.PlaySound("sfx_city_hero_altar", false);
        break;
      case 23:
        AudioManager.Instance.StopAndPlaySound("sfx_city_click_wishing_well");
        break;
    }
  }

  public void OnShowActionButtons()
  {
    if (BuildingController.mSelectedBuildingItem == null || (UnityEngine.Object) null == (UnityEngine.Object) BuildingController.mSelectedBuilding)
      return;
    this.OpenTouchCircle();
    CityCamera cityCamera = UIManager.inst.cityCamera;
    cityCamera.PushArg(new CityCamera.CameraArgs()
    {
      zoomFactor = cityCamera.zoomSmoothFactor,
      dragFactor = this.CMP.focusBuildingTime
    });
  }

  public Twinkler TwinklerInst
  {
    get
    {
      return this.twinklerInst;
    }
    set
    {
      this.twinklerInst = value;
    }
  }

  public void OpenTouchCircle()
  {
    BuildingController buildingController = BuildingController.mSelectedBuildingController;
    UIManager.inst.CloseCityTouchCircle();
    List<CityCircleBtnPara> typeList = CityTouchCircle.Instance.TypeList;
    buildingController.AddActionButton(ref typeList);
    buildingController.AddAdditionActionButton(ref typeList);
    HintPrefab componentInChildren = buildingController.GetComponentInChildren<HintPrefab>();
    if ((UnityEngine.Object) null != (UnityEngine.Object) componentInChildren && componentInChildren.Para != null)
    {
      string str = componentInChildren.Para.ToString();
      if (str != null)
      {
        for (int index = 0; index < typeList.Count; ++index)
        {
          if (str == typeList[index].titleid)
            typeList[index].needEffect = true;
        }
      }
    }
    UIManager.inst.OpenCityTouchCircle(BuildingController.mSelectedBuilding.transform);
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.twinklerInst)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.twinklerInst);
      this.twinklerInst = (Twinkler) null;
    }
    this.twinklerInst = BuildingController.mSelectedBuilding.AddComponent<Twinkler>();
    this.twinklerInst.Twinkle(Twinkler.Mode.LOOP);
  }

  public void OnWallDefensePressed()
  {
    MessageHub.inst.GetPortByAction("City:getCityDefenseInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("Wall/WallDefense", (UI.Dialog.DialogParameter) null, true, true, true);
    }), true);
  }

  public void OnSignPressed()
  {
    UIManager.inst.OpenPopup("Sign/SignPopup", (Popup.PopupParameter) null);
  }

  public void OnWishPressed()
  {
    UIManager.inst.OpenPopup("WishWell/WishWellPopup", (Popup.PopupParameter) null);
  }

  public void OnDecoration()
  {
    UIManager.inst.OpenDlg("CastleDecorationDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnDailyActivyPressed()
  {
    UIManager.inst.OpenPopup("DailyActivy/DailyActiviesPopup", (Popup.PopupParameter) null);
  }

  public void SetPlayerName()
  {
    GameDataManager.inst.LockScrolling();
    NGUITools.AddChild(GameObject.Find("/UIManager/UI Root"), AssetManager.Instance.HandyLoad("Prefab/City/NameEntryDlg", (System.Type) null) as GameObject).GetComponent<NameEntryDlg>().Show((System.Action) (() => GameDataManager.inst.UnlockScrolling()));
  }

  public void OnCityMapClicked()
  {
    if (UIManager.inst.cityCamera.IsInputLocked() || !GameEngine.IsAvailable)
      return;
    BuildingController.UnselectBuilding();
    RaycastHit hitInfo;
    Physics.Raycast(UIManager.inst.cityCamera.GetComponent<Camera>().ScreenPointToRay((Vector3) UICamera.currentTouch.pos), out hitInfo, float.PositiveInfinity, 1 << GameEngine.Instance.TileLayer);
    Vector3 point = hitInfo.point;
    point.z -= 10f;
    this.clickEffectMountPoint.transform.position = point;
    VfxManager.Instance.CreateAndPlay("Prefab/VFX/fx_shubiaodianji", this.clickEffectMountPoint.transform);
  }

  public void OnLoadWorldMap()
  {
    if (PlayerData.inst.IsInPit)
    {
      GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PitMode;
      PVPSystem.Instance.Map.GotoLocation(PlayerData.inst.playerCityData.cityLocation, false);
    }
    else
    {
      BuildingController.UnselectBuilding();
      if ((bool) ((UnityEngine.Object) this.mPublicHUD))
        this.mPublicHUD.SwitchToPart(GAME_LOCATION.LOADING);
      if ((UnityEngine.Object) this.cityTroopAnimManager != (UnityEngine.Object) null)
        this.cityTroopAnimManager.ClearTroops();
      if ((UnityEngine.Object) UIManager.inst != (UnityEngine.Object) null && (UnityEngine.Object) UIManager.inst.cityCamera != (UnityEngine.Object) null)
        UIManager.inst.cityCamera.gameObject.SetActive(false);
      GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
    }
  }

  private void Start()
  {
    this.cp.zoom = UIManager.inst.cityCamera.zoomStartDis;
  }

  public bool Initialized
  {
    get
    {
      return this.initialized;
    }
    protected set
    {
      this.initialized = value;
    }
  }

  public bool IsPreloadFinished
  {
    get
    {
      return this.preloadFinished;
    }
  }

  [DebuggerHidden]
  public IEnumerator PreloadCity(System.Action callback)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CitadelSystem.\u003CPreloadCity\u003Ec__Iterator3D()
    {
      callback = callback,
      \u003C\u0024\u003Ecallback = callback,
      \u003C\u003Ef__this = this
    };
  }

  public void Init()
  {
    AudioManager.Instance.PlayBGM("bgm_city2");
    if (this.Initialized)
    {
      using (List<GameObject>.Enumerator enumerator = this.gobBuildings.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.GetComponent<BuildingController>().RefreshStats();
      }
      this.paradeGroundManager.FreshCityTroops();
    }
    else
      this.InitCity();
  }

  public void InitCamera()
  {
    CityBG component = this.citybggo.GetComponent<CityBG>();
    if ((UnityEngine.Object) null != (UnityEngine.Object) component)
      component.Init();
    UIManager.inst.cityCamera.gameObject.SetActive(true);
  }

  private void InitCity()
  {
    this.mCMP.Init();
    this.InitCamera();
    this.AddEventListener();
    CityManager.inst.BuildCityFromDB();
    this.cityTroopAnimManager = this.mCMP.GetComponent<CityTroopsAnimManager>();
    this.paradeGroundManager = this.mCMP.transform.FindChild("ParadeGround").gameObject.GetComponent<ParadeGroundManager>();
    this.paradeGroundManager.FreshCityTroops();
    this.cityTroopAnimManager.onTroopArraived += new System.Action<TroopType, int>(this.paradeGroundManager.AddMoreTroops);
    foreach (RuralBlock componentsInChild in this.mCMP.GetComponentsInChildren<RuralBlock>(true))
      componentsInChild.Init();
    foreach (SuperPlotController componentsInChild in this.mCMP.GetComponentsInChildren<SuperPlotController>())
      componentsInChild.Init();
    this.CMP.onCityMapClicked += new CityMapPrefabs.OnCityMapClickedEvent(this.OnCityMapClicked);
    CityPlotController.onPlotClicked = (System.Action<int, GameObject, string>) null;
    CityPlotController.onPlotClicked += new System.Action<int, GameObject, string>(this.OnPlotClicked);
    this.CMP.mStrongholdBuilding.GetComponent<BuildingController>().onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    if (!this.gobBuildings.Contains(this.CMP.mStrongholdBuilding))
      this.gobBuildings.Add(this.CMP.mStrongholdBuilding);
    this.CMP.mWallBuilding.GetComponent<BuildingController>().onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    if (!this.gobBuildings.Contains(this.CMP.mWallBuilding))
      this.gobBuildings.Add(this.CMP.mWallBuilding);
    this.CMP.mParadeGround.GetComponent<BuildingController>().onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    if (!this.gobBuildings.Contains(this.CMP.mParadeGround))
      this.gobBuildings.Add(this.CMP.mParadeGround);
    this.CMP.mWatchTowerBuilding.GetComponent<BuildingController>().onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    if (!this.gobBuildings.Contains(this.CMP.mWatchTowerBuilding))
      this.gobBuildings.Add(this.CMP.mWatchTowerBuilding);
    this.CMP.mDragonLairBuilding.GetComponent<BuildingController>().onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    if (!this.gobBuildings.Contains(this.CMP.mDragonLairBuilding))
      this.gobBuildings.Add(this.CMP.mDragonLairBuilding);
    if (!SettingManager.Instance.IsEnvironmentOff)
    {
      if ((UnityEngine.Object) null == (UnityEngine.Object) this.cityanimgo)
        AssetManager.Instance.LoadAsync("Prefab/City/CityAnimation", (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
        {
          this.cityanimgo = UnityEngine.Object.Instantiate(obj) as GameObject;
          this.cityanimgo.transform.localPosition = Vector3.zero;
          this.cityanimgo.transform.localScale = !this.CMP.gameObject.activeSelf ? Vector3.zero : Vector3.one;
          AssetManager.Instance.UnLoadAsset("Prefab/City/CityAnimation", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
        }), (System.Type) null);
      else
        this.cityanimgo.SetActive(true);
      if ((UnityEngine.Object) null == (UnityEngine.Object) this.cityeffectgo)
        AssetManager.Instance.LoadAsync("Prefab/City/CityEffect", (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
        {
          this.cityeffectgo = Utils.DuplicateGOB(obj as GameObject, this.CMP.transform);
          AssetManager.Instance.UnLoadAsset("Prefab/City/CityEffect", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
        }), (System.Type) null);
    }
    GameEngine.Instance.GameModeReady();
    if ((UnityEngine.Object) this.mPublicHUD != (UnityEngine.Object) null)
    {
      this.mPublicHUD.onWorldMapBtnPressed += new System.Action(this.OnLoadWorldMap);
      this.mPublicHUD.onInfoBtnPressed += new System.Action(this.OnBuildingInfoPressed);
      this.mPublicHUD.onUpgradeBtnPressed += new System.Action(this.OnUpgradeBuildingPressed);
      this.mPublicHUD.onTrainBtnPressed += new System.Action(this.OnTrainTroopsPressed);
      this.mPublicHUD.onHospitalBtnPressed += new System.Action(this.OnHospitalBtnPressed);
      this.mPublicHUD.onRallyBtnPressed += new System.Action(this.OnRallyBtnPressed);
      this.mPublicHUD.onReinforceBtnPressed += new System.Action(this.OnReinforceBtnPressed);
      this.mPublicHUD.onBlacksmithBtnPressed += new System.Action(this.OnForgeBtnPressed);
      this.mPublicHUD.onGamblingBtnPressed += new System.Action(this.OnArmoryBtnPressed);
      this.mPublicHUD.onEnhanceBtnPressed += new System.Action(this.OnEnhanceBtnPressed);
      this.mPublicHUD.onResearchBtnPressed += new System.Action(this.OnResearchBtnPressed);
      this.mPublicHUD.onTradeBtnPressed += new System.Action(this.OnTradeBtnPressed);
    }
    this.clickEffectMountPoint = new GameObject("click");
    this.clickEffectMountPoint.transform.parent = this.MapRoot.transform;
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.idleEffectGOB)
      this.idleEffectGOB = AssetManager.Instance.HandyLoad("Prefab/VFX/fx_jianzhukongxian", (System.Type) null) as GameObject;
    Vector3 vector3 = new Vector3(0.0f, -5000f, 0.0f);
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.foodCollectionGOB)
      this.foodCollectionGOB = UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/VFX/fx_foodcollection", (System.Type) null)) as GameObject;
    this.foodCollectionGOB.transform.parent = this.MapRoot.transform;
    this.foodCollectionGOB.transform.position = vector3;
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.woodCollectionGOB)
      this.woodCollectionGOB = UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/VFX/fx_woodcollection", (System.Type) null)) as GameObject;
    this.woodCollectionGOB.transform.parent = this.MapRoot.transform;
    this.woodCollectionGOB.transform.position = vector3;
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.oreCollectionGOB)
      this.oreCollectionGOB = UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/VFX/fx_orecollection", (System.Type) null)) as GameObject;
    this.oreCollectionGOB.transform.parent = this.MapRoot.transform;
    this.oreCollectionGOB.transform.position = vector3;
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.silverCollectionGOB)
      this.silverCollectionGOB = UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/VFX/fx_coincollection_02", (System.Type) null)) as GameObject;
    this.silverCollectionGOB.transform.parent = this.MapRoot.transform;
    this.silverCollectionGOB.transform.position = vector3;
    CityManager.inst.CalculateUpKeep();
    UIManager.inst.GetComponent<FingerUpDetector>().OnFingerUp += new FingerEventDetector<FingerUpEvent>.FingerEventHandler(this.HandleOnFingerEvent);
    DBManager.inst.DB_hero.onDataChanged += new System.Action<long>(this.OnInfoChanged);
    DBManager.inst.DB_Dragon.onDataChanged += new System.Action<long>(this.OnInfoChanged);
    UIManager.inst.onPublicHudStateChange += new System.Action<bool>(this.OnPublicHudShow);
    UIManager.inst.splashScreen.onSplashScreenHide += new System.Action(this.OpenDragonLevelUpPopUp);
    UIManager.inst.splashScreen.onSplashScreenHide += new System.Action(this.OpenLordLevelUpPopUp);
    UIManager.inst.splashScreen.onSplashScreenHide += new System.Action(this.OpenSignInPopUp);
    UIManager.inst.splashScreen.onSplashScreenHide += new System.Action(this.OpenRecommend);
    this.CheckTutorial();
    this.CheckGiftPackageFreeDelivery();
    if (TutorialManager.Instance.IsRunning)
      return;
    BuildingHintManager.Instance.Init();
  }

  private void AddEventListener()
  {
    CityManager.inst.onCreateBuilding += new CityManager.OnCreateBuilding(this.OnCreateBuilding);
    CityManager.inst.onUpgradeBuilding += new CityManager.OnUpgradeBuilding(this.OnUpgradeBuilding);
    CityManager.inst.onUpgradeFinished += new CityManager.OnUpgradeFinished(this.OnUpgradeFinished);
    CityManager.inst.onUpgradeCancelled += new CityManager.OnUpgradeFinished(this.OnUpgradeCancelled);
    CityManager.inst.onDestroyBuilding += new CityManager.OnDestroyBuilding(this.OnDestroyBuilding);
  }

  private void RemoveEventListener()
  {
    CityManager.inst.onCreateBuilding -= new CityManager.OnCreateBuilding(this.OnCreateBuilding);
    CityManager.inst.onUpgradeBuilding -= new CityManager.OnUpgradeBuilding(this.OnUpgradeBuilding);
    CityManager.inst.onUpgradeFinished -= new CityManager.OnUpgradeFinished(this.OnUpgradeFinished);
    CityManager.inst.onUpgradeCancelled -= new CityManager.OnUpgradeFinished(this.OnUpgradeCancelled);
    CityManager.inst.onDestroyBuilding -= new CityManager.OnDestroyBuilding(this.OnDestroyBuilding);
  }

  public void Dispose(bool hard = true)
  {
    this.RemoveEventListener();
    BuildingHintManager.Instance.Dispose();
    this.DisposeBuildings();
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.mCMP)
    {
      this.mCMP.Dispose();
      foreach (RuralBlock componentsInChild in this.mCMP.GetComponentsInChildren<RuralBlock>(true))
        componentsInChild.Dispose();
      foreach (SuperPlotController componentsInChild in this.mCMP.GetComponentsInChildren<SuperPlotController>(true))
        componentsInChild.Dispose();
      if (hard)
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) this.mCMP.gameObject);
        this.mCMP = (CityMapPrefabs) null;
      }
      else
        this.mCMP.gameObject.SetActive(false);
    }
    if (hard)
    {
      if ((UnityEngine.Object) null != (UnityEngine.Object) this.citybggo)
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) this.citybggo);
        this.citybggo = (GameObject) null;
      }
      if ((UnityEngine.Object) null != (UnityEngine.Object) this.cityanimgo)
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) this.cityanimgo);
        this.cityanimgo = (GameObject) null;
      }
      if ((UnityEngine.Object) null != (UnityEngine.Object) this.cityeffectgo)
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) this.cityeffectgo);
        this.cityeffectgo = (GameObject) null;
      }
    }
    else if ((UnityEngine.Object) null != (UnityEngine.Object) this.cityanimgo)
      this.cityanimgo.SetActive(false);
    this.preloadFinished = false;
    this.Initialized = false;
    BuildingMoveManager.Instance.Dispose();
    TimerBarManager.Instance.Terminate();
    LinkerHub.Instance.Dispose();
    UnityEngine.Object.Destroy((UnityEngine.Object) CitadelSystem._singleton.gameObject);
    CitadelSystem._singleton = (CitadelSystem) null;
  }

  private void InitBuildings()
  {
    foreach (BuildingControllerNew componentsInChild in this.mCMP.GetComponentsInChildren<BuildingControllerNew>(true))
      componentsInChild.InitBuilding();
  }

  private void InitConstBuildings()
  {
    foreach (BuildingControllerNew componentsInChild in this.mCMP.GetComponentsInChildren<BuildingControllerNew>(true))
    {
      if (!componentsInChild.CanMove)
        componentsInChild.InitBuilding();
    }
  }

  private void DisposeBuildings()
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.mCMP))
      return;
    this.gobBuildings.Clear();
    foreach (BuildingControllerNew componentsInChild in this.mCMP.GetComponentsInChildren<BuildingControllerNew>(true))
    {
      componentsInChild.Dispose();
      if (componentsInChild.CanMove)
        UnityEngine.Object.Destroy((UnityEngine.Object) componentsInChild.gameObject);
    }
    foreach (CityPlotController componentsInChild in this.mCMP.GetComponentsInChildren<CityPlotController>(true))
      componentsInChild.Reset();
    CityPlotController.Clear();
    CityTouchCircle.Instance.Dispose();
  }

  private void CheckGiftPackageFreeDelivery()
  {
    Dictionary<string, string> gift = AccountManager.Instance.Gift;
    if (gift == null || !gift.ContainsKey("label"))
      return;
    MessageHub.inst.GetPortByAction("player:getCdkeyReward").SendLoader(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "cdkey",
        (object) gift["label"]
      }
    }, new System.Action<bool, object>(this.CheckGiftDeliveryStatus), true, false);
  }

  private void CheckGiftDeliveryStatus(bool ret, object data)
  {
    if (ret)
      return;
    Hashtable hashtable = data as Hashtable;
    if (hashtable == null)
      return;
    string str = hashtable[(object) "errmsg"] as string;
  }

  public void CheckTutorial()
  {
    if (NewTutorial.skipTutorial)
      return;
    NewTutorial.Instance.GetNewRecord();
    if (TutorialManager.Instance.Resolution == TutorialManager.ABTestType.Basic)
    {
      string recordPoint = NewTutorial.Instance.GetRecordPoint("beginner");
      if (!("finished" != recordPoint))
        return;
      NewTutorial.Instance.InitTutorial("beginner");
      NewTutorial.Instance.LoadTutorialData(recordPoint);
    }
    else
    {
      string recordPoint = NewTutorial.Instance.GetRecordPoint("testb_beginner");
      if (!("finished" != recordPoint))
        return;
      NewTutorial.Instance.InitTutorial("testb_beginner");
      NewTutorial.Instance.LoadTutorialData(recordPoint);
    }
  }

  public List<long> CollectionList
  {
    get
    {
      return this.collectionList;
    }
  }

  private void OnInfoChanged(long id)
  {
    if (PlayerData.inst.uid != id || UIManager.inst.dialogStackCount != 0)
      return;
    this.OpenDragonLevelUpPopUp();
    this.OpenLordLevelUpPopUp();
  }

  private void OnPublicHudShow(bool show)
  {
    if (!show)
      return;
    this.OpenDragonLevelUpPopUp();
    this.OpenLordLevelUpPopUp();
  }

  public void OpenLordLevelUpPopUp()
  {
    int num = 1;
    StatsData statsData = DBManager.inst.DB_Stats.Get("last_notify_level");
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(PlayerData.inst.uid);
    if (statsData != null)
      num = (int) statsData.count;
    if (num >= heroData.level)
      return;
    UIManager.inst.priorityPopupQueue.OpenPopup("LordLevelUp", (Popup.PopupParameter) new LordLevelUp.Parameter()
    {
      level = (num + 1)
    }, 1);
  }

  public void OpenDragonLevelUpPopUp()
  {
    int num = 1;
    StatsData statsData = DBManager.inst.DB_Stats.Get("last_notify_dragon_level");
    if (!DBManager.inst.DB_Dragon.Datas.ContainsKey(PlayerData.inst.uid))
      return;
    DragonData data = DBManager.inst.DB_Dragon.Datas[PlayerData.inst.uid];
    if (statsData != null)
      num = (int) statsData.count;
    if (num >= data.Level)
      return;
    UIManager.inst.priorityPopupQueue.OpenPopup("Dragon/DragonLevelupPopup", (Popup.PopupParameter) new DragonLevelUpPopup.Parameter()
    {
      level = (num + 1)
    }, 0);
  }

  public void OpenRecommend()
  {
    AccountManager.Instance.ShowRecommendReview();
  }

  public void OpenPersonalVerify()
  {
  }

  public void OpenSignInPopUp()
  {
    try
    {
      UIManager.inst.AutoOpen = true;
      if (TutorialManager.Instance.IsRunning || AccountManager.Instance.CheckDeepLink())
        return;
      if (AccountManager.Instance.NeedToShowCertifying())
        AccountManager.Instance.ShowCertifying();
      else if (Utils.IsShowSevenDay())
      {
        UIManager.inst.OpenDlg("Activity/SevenDaysEventDlg", (UI.Dialog.DialogParameter) null, true, true, true);
        PlayerPrefsEx.SetIntByUid("seven_days_show_time", NetServerTime.inst.ServerTimestamp);
        PlayerPrefsEx.Save();
      }
      else if (!Utils.IsSignedIn())
      {
        UIManager.inst.priorityPopupQueue.OpenPopup("Sign/SignPopup", (Popup.PopupParameter) null, 2);
        this.LoadDKtutorial();
        this.LoadMerlinTowerTutorial();
        AnniversaryManager.Instance.OnGameReady();
        AnnouncementPopupController.Instance.ShowAnnouncementPopup();
      }
      else if (NetServerTime.inst.ServerTimestamp - ActivityManager.Intance.loc_knightShowTime > 86400)
      {
        PlayerPrefsEx.SetIntByUid("activity_kngith_start_show_time", NetServerTime.inst.ServerTimestamp);
        PlayerPrefsEx.Save();
        UIManager.inst.OpenDlg("Activity/ActivityMainDlg", (UI.Dialog.DialogParameter) null, true, true, true);
        this.LoadDKtutorial();
        this.LoadMerlinTowerTutorial();
        AnniversaryManager.Instance.OnGameReady();
        AnnouncementPopupController.Instance.ShowAnnouncementPopup();
      }
      else if (NetServerTime.inst.ServerTimestamp - ActivityManager.Intance.loc_monthlySpecialTime > 86400)
      {
        PlayerPrefsEx.SetIntByUid("monthly_special_show_time", NetServerTime.inst.ServerTimestamp);
        PlayerPrefsEx.Save();
        Utils.ShowMonthlySpecialStore((UI.Dialog.DialogParameter) null);
      }
      else
      {
        if (PlayerData.inst.playerCityData.level >= 6)
        {
          Utils.ShowRecommendIAPPackagePopupByPolicy();
          this.LoadDKtutorial();
          this.LoadMerlinTowerTutorial();
        }
        AnniversaryManager.Instance.OnGameReady();
        AnnouncementPopupController.Instance.ShowAnnouncementPopup();
      }
    }
    finally
    {
      UIManager.inst.AutoOpen = false;
    }
  }

  public void LoadDKtutorial()
  {
    string recordPoint = NewTutorial.Instance.GetRecordPoint("Tutorial_activity_center");
    if (!("finished" != recordPoint) || CityManager.inst.mStronghold.mLevel < 15)
      return;
    NewTutorial.Instance.InitTutorial("Tutorial_activity_center");
    NewTutorial.Instance.LoadTutorialData(recordPoint);
  }

  public void LoadMerlinTowerTutorial()
  {
    string recordPoint = NewTutorial.Instance.GetRecordPoint("Tutorial_merlin_tower");
    if (!("finished" != recordPoint) || CityManager.inst.mStronghold.mLevel < MerlinTowerPayload.Instance.OpenLevel)
      return;
    NewTutorial.Instance.InitTutorial("Tutorial_merlin_tower");
    NewTutorial.Instance.LoadTutorialData(recordPoint);
  }

  public void OnAllianceHospitalBtnPressed()
  {
    if (DBManager.inst.DB_AllianceHospital.GetMyHospitalData() == null)
      return;
    UIManager.inst.OpenDlg("Alliance/AllianceHospitalHealDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnKingdomHospitalBtnPressed()
  {
    UIManager.inst.OpenDlg("KingdomHospitalDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void HandleOnFingerEvent(FingerEvent eventData)
  {
    if (UIManager.inst.dialogStackCount != 0)
      return;
    UIManager.inst.cityCamera.LockInput = false;
    if (this.collectionList.Count <= 0)
      return;
    Hashtable postData = new Hashtable();
    postData[(object) "building_ids"] = (object) this.collectionList.ToArray();
    AccountManager.Instance.ShowVerification();
    MessageHub.inst.GetPortByAction("City:collectResource").SendRequest(postData, (System.Action<bool, object>) ((ret, obj) =>
    {
      this.collectionList.Clear();
      if (obj == null)
        return;
      RewardsCollectionAnimator.Instance.Clear();
      foreach (DictionaryEntry dictionaryEntry in obj as Hashtable)
      {
        if (!(dictionaryEntry.Key.ToString() == nameof (ret)))
        {
          int result1 = 0;
          int result2 = 0;
          if (int.TryParse(dictionaryEntry.Key.ToString(), out result1) && int.TryParse(dictionaryEntry.Value.ToString(), out result2))
          {
            ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(result1);
            RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
            {
              icon = itemStaticInfo.ImagePath,
              count = (float) result2
            });
          }
        }
      }
      RewardsCollectionAnimator.Instance.CollectItems(false);
    }), false);
  }

  private void OnHeroInfoChanged(long id)
  {
    if (PlayerData.inst.uid != id || UIManager.inst.dialogStackCount != 0)
      return;
    this.OpenLordLevelUpPopUp();
  }

  [DebuggerHidden]
  public IEnumerator Show(System.Action callback)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CitadelSystem.\u003CShow\u003Ec__Iterator3E()
    {
      callback = callback,
      \u003C\u0024\u003Ecallback = callback,
      \u003C\u003Ef__this = this
    };
  }

  public void Hide()
  {
    if (!this.Initialized)
      return;
    this.gameObject.SetActive(false);
    this.mCMP.gameObject.SetActive(false);
    this.citybggo.SetActive(false);
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.cityanimgo)
      this.cityanimgo.transform.localScale = Vector3.zero;
    UIManager.inst.cityCamera.gameObject.SetActive(false);
    BuildingController.UnselectBuilding();
  }

  public void ForceHide()
  {
    NGUITools.SetActive(this.citybggo, false);
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.cityanimgo)
      this.cityanimgo.transform.localScale = Vector3.zero;
    UIManager.inst.cityCamera.gameObject.SetActive(false);
    BuildingController.UnselectBuilding();
  }

  public void Teleport2Lord(Coordinate tar)
  {
    if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.CityMode)
    {
      UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
      {
        this.OnLoadWorldMap();
        PVPSystem.Instance.Map.EnterTeleportMode(tar, TeleportMode.ADVANCE_TELEPORT, 0, false);
        PVPSystem.Instance.Map.GotoLocation(tar, false);
      }));
    }
    else
    {
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
        return;
      PVPSystem.Instance.Map.EnterTeleportMode(tar, TeleportMode.ADVANCE_TELEPORT, 0, false);
      PVPSystem.Instance.Map.GotoLocation(tar, false);
    }
  }

  private class CameraPara
  {
    public Vector3 cwpos;
    public float zoom;
    public bool isreord;
  }
}
