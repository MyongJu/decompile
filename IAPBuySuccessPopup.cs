﻿// Decompiled with JetBrains decompiler
// Type: IAPBuySuccessPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class IAPBuySuccessPopup : Popup
{
  private Dictionary<int, IAPStoreContentRenderer> m_ItemList = new Dictionary<int, IAPStoreContentRenderer>();
  public UITexture m_Post;
  public UILabel m_GoldCount;
  public GameObject m_ItemPrefab;
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  public UIButton m_buyButton;
  public UILabel m_leftCount;
  public UILabel m_buyLable;
  private IAPBuySuccessPopup.Parameter m_Parameter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as IAPBuySuccessPopup.Parameter;
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.ClearData();
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnBuyButtonPressed()
  {
    this.OnClosePressed();
    IAPStorePackagePayload.Instance.BuyProduct(this.m_Parameter.package.package.productId, this.m_Parameter.package.package.id, this.m_Parameter.package.group.id, (System.Action) null);
  }

  private void UpdateUI()
  {
    if (this.m_Parameter.annvIapMainInfo != null)
    {
      if (this.m_Parameter.annvIapMainInfo.IapPackageInfo != null)
      {
        this.m_GoldCount.text = ScriptLocalization.Get("Gold", true) + " +" + Utils.FormatThousands(this.m_Parameter.annvIapMainInfo.IapPackageInfo.gold.ToString());
        BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Post, this.m_Parameter.annvIapMainInfo.IconPath, (System.Action<bool>) null, true, false, string.Empty);
      }
    }
    else
    {
      this.m_GoldCount.text = ScriptLocalization.Get("Gold", true) + " +" + Utils.FormatThousands(this.m_Parameter.package.package.gold.ToString());
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Post, this.m_Parameter.package.group.GraphPath, (System.Action<bool>) null, true, false, string.Empty);
      this.m_buyLable.text = Utils.XLAT("id_package_buy_again");
      if (this.m_Parameter.isCrossLevel)
      {
        this.m_leftCount.gameObject.SetActive(false);
      }
      else
      {
        this.m_leftCount.gameObject.SetActive(true);
        this.m_leftCount.text = string.Format(ScriptLocalization.Get("iap_gold_num_remaining", true), (object) this.m_Parameter.package.LeftPurchaseCount);
      }
    }
    if (this.m_Parameter.isCrossLevel)
    {
      this.m_buyButton.gameObject.SetActive(false);
    }
    else
    {
      this.m_buyButton.gameObject.SetActive(true);
      this.m_buyButton.isEnabled = this.m_Parameter.package.LeftPurchaseCount > 0L;
    }
    this.UpdateData();
  }

  private void ClearData()
  {
    using (Dictionary<int, IAPStoreContentRenderer>.ValueCollection.Enumerator enumerator = this.m_ItemList.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IAPStoreContentRenderer current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ItemList.Clear();
  }

  private void UpdateData()
  {
    this.ClearData();
    IAPPackageInfo iapPackageInfo = this.m_Parameter.annvIapMainInfo == null ? this.m_Parameter.package.package : this.m_Parameter.annvIapMainInfo.IapPackageInfo;
    IAPRewardInfo rewardInfo1 = ConfigManager.inst.DB_IAPRewards.Get(iapPackageInfo.reward_group_id);
    IAPRewardInfo rewardInfo2 = ConfigManager.inst.DB_IAPRewards.Get(iapPackageInfo.alliance_reward_group);
    this.UpdateData(rewardInfo1);
    this.UpdateData(rewardInfo2);
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  private void UpdateData(IAPRewardInfo rewardInfo)
  {
    if (rewardInfo == null)
      return;
    List<Reward.RewardsValuePair> rewards = rewardInfo.GetRewards();
    rewards.Sort(new Comparison<Reward.RewardsValuePair>(IAPBuySuccessPopup.Compare));
    for (int index = 0; index < rewards.Count; ++index)
    {
      Reward.RewardsValuePair rewardsValuePair = rewards[index];
      int internalId = rewardsValuePair.internalID;
      int itemCount = rewardsValuePair.value;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_Grid.transform;
      gameObject.transform.localScale = Vector3.one;
      IAPStoreContentRenderer component = gameObject.GetComponent<IAPStoreContentRenderer>();
      component.SetData(itemStaticInfo, itemCount);
      this.m_ItemList.Add(internalId, component);
    }
  }

  private static int Compare(Reward.RewardsValuePair x, Reward.RewardsValuePair y)
  {
    return ConfigManager.inst.DB_Items.GetItem(x.internalID).Priority - ConfigManager.inst.DB_Items.GetItem(y.internalID).Priority;
  }

  public class Parameter : Popup.PopupParameter
  {
    public IAPStorePackage package;
    public bool isCrossLevel;
    public AnniversaryIapMainInfo annvIapMainInfo;
  }
}
