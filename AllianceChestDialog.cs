﻿// Decompiled with JetBrains decompiler
// Type: AllianceChestDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class AllianceChestDialog : UI.Dialog
{
  [SerializeField]
  private AllianceChestPage _AllianceChestPage;
  [SerializeField]
  private PersonalChestPage _PersonalChestPage;
  [SerializeField]
  private ChestLibraryPage _ChestLibraryPage;
  [SerializeField]
  private RedDotTipItem _UnsendPsersonalChestTipITem;
  [SerializeField]
  private RedDotTipItem _UnopenedChestTipITem;
  [SerializeField]
  private UIToggle _ToggleAllianceChest;
  [SerializeField]
  private UIToggle _TogglePersonalChest;
  [SerializeField]
  private UIToggle _ToggleChestLibrary;
  [SerializeField]
  private HelpBtnComponent _helpBtnComponent;

  private int UnsendPersonalChestCount
  {
    get
    {
      return DBManager.inst.DB_UserTreasureChest.GetAllCanSendUserChest(PlayerData.inst.uid);
    }
  }

  private int UnopenedChestCount
  {
    get
    {
      return AllianceTreasuryCountManager.Instance.UnOpenedChestCount;
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondTick);
    RequestManager.inst.AddObserver("TreasuryVault:sendAllianceTreasury", new System.Action<string, bool>(this.OnRequestCallback));
    RequestManager.inst.AddObserver("TreasuryVault:sendUserTreasury", new System.Action<string, bool>(this.OnRequestCallback));
    RequestManager.inst.AddObserver("TreasuryVault:claimTreasury", new System.Action<string, bool>(this.OnRequestCallback));
    RequestManager.inst.AddObserver("TreasuryVault:getTreasuryVaultList", new System.Action<string, bool>(this.OnRequestCallback));
    RequestManager.inst.AddObserver("TreasuryVault:sendUserTreasury", new System.Action<string, bool>(this.OnSendUserTreasuryCallback));
    AllianceTreasuryCountManager.Instance.OnTreasuryCountChanged += new AllianceTreasuryCountManager.TreasuryCountChangedHandler(this.OnTreasuryVaultCountChanged);
    MessageHub.inst.GetPortByAction("alliance_treasury_vault_count").AddEvent(new System.Action<object>(this.OnReceivedCountPush));
    DBManager.inst.DB_UserTreasureChest.onDataChanged += new System.Action<UserTreasureChestData>(this.OnUserTreasureChestDataChanged);
    AllianceChestDialog.Parameter parameter = orgParam as AllianceChestDialog.Parameter;
    UIToggle toggleAllianceChest = this._ToggleAllianceChest;
    bool flag1 = false;
    this._ToggleChestLibrary.value = flag1;
    bool flag2 = flag1;
    this._TogglePersonalChest.value = flag2;
    int num = flag2 ? 1 : 0;
    toggleAllianceChest.value = num != 0;
    if (parameter != null)
    {
      switch (parameter.page)
      {
        case AllianceChestDialog.Page.AllianceChest:
          this._ToggleAllianceChest.value = true;
          break;
        case AllianceChestDialog.Page.PersonalChest:
          this._TogglePersonalChest.value = true;
          break;
        case AllianceChestDialog.Page.ChestLibrary:
          this._ToggleChestLibrary.value = true;
          break;
      }
    }
    else
      this._ToggleAllianceChest.value = true;
    this.UpdateUI();
  }

  protected void OnUserTreasureChestDataChanged(UserTreasureChestData data)
  {
    this._PersonalChestPage.NotifyUpdate();
  }

  protected void OnTreasuryVaultCountChanged(int chestLibraryCount)
  {
    this.UpdateRedDotTip();
  }

  protected void OnReceivedCountPush(object data)
  {
    this._ChestLibraryPage.NotifyUpdate();
  }

  public void OnRequestCallback(string action, bool result)
  {
    this.UpdateRedDotTip();
  }

  public void OnSendUserTreasuryCallback(string action, bool result)
  {
    this._PersonalChestPage.NotifyUpdate();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.OnClose(orgParam);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondTick);
    RequestManager.inst.RemoveObserver("TreasuryVault:sendAllianceTreasury", new System.Action<string, bool>(this.OnRequestCallback));
    RequestManager.inst.RemoveObserver("TreasuryVault:sendUserTreasury", new System.Action<string, bool>(this.OnRequestCallback));
    RequestManager.inst.RemoveObserver("TreasuryVault:claimTreasury", new System.Action<string, bool>(this.OnRequestCallback));
    RequestManager.inst.RemoveObserver("TreasuryVault:getTreasuryVaultList", new System.Action<string, bool>(this.OnRequestCallback));
    MessageHub.inst.GetPortByAction("alliance_treasury_vault_count").RemoveEvent(new System.Action<object>(this.OnReceivedCountPush));
    RequestManager.inst.RemoveObserver("TreasuryVault:sendUserTreasury", new System.Action<string, bool>(this.OnSendUserTreasuryCallback));
    AllianceTreasuryCountManager.Instance.OnTreasuryCountChanged -= new AllianceTreasuryCountManager.TreasuryCountChangedHandler(this.OnTreasuryVaultCountChanged);
    DBManager.inst.DB_UserTreasureChest.onDataChanged -= new System.Action<UserTreasureChestData>(this.OnUserTreasureChestDataChanged);
  }

  protected void OnSecondTick(int delta)
  {
    if (!this._ChestLibraryPage.gameObject.activeInHierarchy)
      return;
    this._ChestLibraryPage.OnSecondTick(delta);
  }

  private void UpdateRedDotTip()
  {
    this._UnsendPsersonalChestTipITem.SetTipCount(this.UnsendPersonalChestCount);
    this._UnopenedChestTipITem.SetTipCount(this.UnopenedChestCount);
  }

  private void UpdateUI()
  {
    this.UpdateRedDotTip();
    this._AllianceChestPage.gameObject.SetActive(false);
    this._PersonalChestPage.gameObject.SetActive(false);
    this._ChestLibraryPage.gameObject.SetActive(false);
    if (this._ToggleAllianceChest.value)
    {
      this._helpBtnComponent.ID = "help_alliance_giftbox";
      this._AllianceChestPage.gameObject.SetActive(true);
      this._AllianceChestPage.NotifyUpdate();
    }
    if (this._TogglePersonalChest.value)
    {
      this._helpBtnComponent.ID = "help_alliance_giftbox_personal";
      this._PersonalChestPage.gameObject.SetActive(true);
      this._PersonalChestPage.NotifyUpdate();
    }
    if (!this._ToggleChestLibrary.value)
      return;
    this._helpBtnComponent.ID = "help_alliance_giftbox_storage";
    this._ChestLibraryPage.gameObject.SetActive(true);
    this._ChestLibraryPage.NotifyUpdate();
  }

  public void OnToggleValueChanged()
  {
    this.UpdateUI();
  }

  public enum Page
  {
    AllianceChest,
    PersonalChest,
    ChestLibrary,
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public AllianceChestDialog.Page page;
  }
}
