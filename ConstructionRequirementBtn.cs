﻿// Decompiled with JetBrains decompiler
// Type: ConstructionRequirementBtn
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ConstructionRequirementBtn : MonoBehaviour
{
  public UITexture mIcon;
  public UILabel mRequirementLabel;
  public UILabel mName;
  public GameObject mGoodIcon;
  public GameObject mBadIcon;
  public GameObject mGetMoreBtn;
  public UILabel mLabel;
  public UITexture mBackIcon;
  public UILabel getMoreLabel;
  private ItemStaticInfo item;
  private ShopStaticInfo shopInfo;
  private double m_Amount;
  private double m_Needed;
  private string iconName;

  public void SetDetails(string iconName, string reqDetails, string reqName, bool bGood, bool bGetMore)
  {
    if (iconName != null && iconName != this.iconName)
    {
      for (int index = 0; index < this.mIcon.transform.childCount; ++index)
      {
        Transform child = this.mIcon.transform.GetChild(index);
        child.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) child.gameObject);
      }
      this.mIcon.mainTexture = (Texture) null;
      if (iconName.Contains("Prefab/UI/BuildingPrefab/ui_"))
      {
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad(iconName, (System.Type) null) as GameObject);
        gameObject.transform.parent = this.mIcon.transform;
        gameObject.transform.localPosition = Vector3.zero;
        int width = gameObject.GetComponent<UIWidget>().width;
        int height = gameObject.GetComponent<UIWidget>().height;
        float num = width < height ? 150f / (float) height : 150f / (float) width;
        gameObject.transform.localScale = new Vector3(num, num, 1f);
      }
      else
        BuilderFactory.Instance.HandyBuild((UIWidget) this.mIcon, iconName, (System.Action<bool>) null, true, false, string.Empty);
      this.iconName = iconName;
    }
    this.mRequirementLabel.text = reqDetails;
    if (reqName != null && (UnityEngine.Object) this.mName != (UnityEngine.Object) null)
    {
      this.mName.text = reqName;
      if (!bGood)
      {
        this.mRequirementLabel.color = new Color(0.9058824f, 0.07058824f, 0.0f);
        this.mName.color = new Color(0.9058824f, 0.07058824f, 0.0f);
      }
      else
      {
        this.mRequirementLabel.color = new Color(0.7686275f, 0.7686275f, 0.7686275f);
        this.mName.color = new Color(0.7686275f, 0.7686275f, 0.7686275f);
      }
    }
    this.mGoodIcon.SetActive(bGood);
    this.mBadIcon.SetActive(!bGood);
    this.SetGetMoreActive(false);
    if (!bGood && bGetMore)
      this.SetGetMoreActive(true);
    if (!bGetMore || !((UnityEngine.Object) null != (UnityEngine.Object) this.mLabel))
      return;
    this.mLabel.text = Utils.XLAT("construction_requirements_get_more");
  }

  public void SetDetails2(string iconName, string reqDetails, string reqName, bool showGood, bool showBad, bool showGetMore, string getMoreBtnContent)
  {
    if (iconName != null && iconName != this.iconName)
    {
      for (int index = 0; index < this.mIcon.transform.childCount; ++index)
      {
        Transform child = this.mIcon.transform.GetChild(index);
        child.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) child.gameObject);
      }
      this.mIcon.mainTexture = (Texture) null;
      if (iconName.Contains("Prefab/UI/BuildingPrefab/ui_"))
      {
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad(iconName, (System.Type) null) as GameObject);
        gameObject.transform.parent = this.mIcon.transform;
        gameObject.transform.localPosition = Vector3.zero;
        int width = gameObject.GetComponent<UIWidget>().width;
        int height = gameObject.GetComponent<UIWidget>().height;
        float num = width < height ? 150f / (float) height : 150f / (float) width;
        gameObject.transform.localScale = new Vector3(num, num, 1f);
      }
      else
        BuilderFactory.Instance.HandyBuild((UIWidget) this.mIcon, iconName, (System.Action<bool>) null, true, false, string.Empty);
      this.iconName = iconName;
    }
    this.mRequirementLabel.text = reqDetails;
    if (reqName != null && (UnityEngine.Object) this.mName != (UnityEngine.Object) null)
      this.mName.text = reqName;
    this.mGoodIcon.SetActive(showGood);
    this.mBadIcon.SetActive(showBad);
    this.SetGetMoreActive(showGetMore);
    if (!showGetMore || !((UnityEngine.Object) this.mLabel != (UnityEngine.Object) null))
      return;
    this.mLabel.text = getMoreBtnContent;
  }

  public void SetGetMoreActive(bool bActive)
  {
    if (!((UnityEngine.Object) this.mGetMoreBtn != (UnityEngine.Object) null))
      return;
    this.mGetMoreBtn.SetActive(bActive);
  }

  public void SetShopInfo(ShopStaticInfo shopInfo)
  {
    this.shopInfo = shopInfo;
  }

  public void SetItemInfo(ItemStaticInfo itemInfo)
  {
    this.item = itemInfo;
  }

  public ShopStaticInfo GetShopInfo()
  {
    return this.shopInfo;
  }

  public ItemStaticInfo GetItemInfo()
  {
    return this.item;
  }

  public void SetRequirement(double amount, double needed)
  {
    this.m_Amount = amount;
    this.m_Needed = needed;
  }

  public double GetAmount()
  {
    return this.m_Amount;
  }

  public double GetNeeded()
  {
    return this.m_Needed;
  }

  public void SetLabel(string text)
  {
    this.mLabel.text = text;
  }

  public void SetBackImage(string path)
  {
    if (!((UnityEngine.Object) this.mBackIcon != (UnityEngine.Object) null))
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.mBackIcon, path, (System.Action<bool>) null, true, false, string.Empty);
  }
}
