﻿// Decompiled with JetBrains decompiler
// Type: HeroTalentActiveInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class HeroTalentActiveInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "skill_id")]
  public int skillId;
  [Config(Name = "active_slot_id")]
  public int activeSlotId;
  [Config(Name = "priority")]
  public int priority;
  [Config(Name = "description")]
  public string description;
}
