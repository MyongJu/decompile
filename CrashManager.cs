﻿// Decompiled with JetBrains decompiler
// Type: CrashManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CrashManager : MonoBehaviour
{
  public HockeyAppIOS iosAdaptor;
  public HockeyAppAndroid androidAdaptor;

  public void Awake()
  {
    Object.DontDestroyOnLoad((Object) this.gameObject);
    switch (Application.platform)
    {
      case RuntimePlatform.IPhonePlayer:
        this.iosAdaptor.appID = "5af0b915a1a741498a245c549d825b2d";
        this.iosAdaptor.secret = "3ec1037d922a9c87a4accd640cf3f7ef";
        this.iosAdaptor.gameObject.SetActive(true);
        this.iosAdaptor.enabled = true;
        break;
      case RuntimePlatform.Android:
        this.androidAdaptor.appID = "bfc9635728e4429fa69bb88e956fc60c";
        this.androidAdaptor.gameObject.SetActive(true);
        this.androidAdaptor.enabled = true;
        break;
    }
    Profiler.maxNumberOfSamplesPerFrame = -1;
  }
}
