﻿// Decompiled with JetBrains decompiler
// Type: RoundActivityRankRewardUIBaseData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class RoundActivityRankRewardUIBaseData
{
  public object Data { get; set; }

  public virtual int MinRank
  {
    get
    {
      return 0;
    }
  }

  public virtual int MaxRank
  {
    get
    {
      return 0;
    }
  }

  public string Title
  {
    get
    {
      string empty = string.Empty;
      return this.MinRank != this.MaxRank ? string.Format("{0}" + Utils.XLAT("event_ranks_between_num"), (object) string.Empty, (object) this.MinRank, (object) this.MaxRank) : string.Format(Utils.XLAT("event_rank_num"), (object) this.MinRank);
    }
  }

  public virtual Dictionary<int, int> CurrentRewardItems
  {
    get
    {
      return (Dictionary<int, int>) null;
    }
  }

  public virtual List<int> CurrentItems
  {
    get
    {
      return (List<int>) null;
    }
  }

  protected void AddDict(Dictionary<int, int> souces, int key, int value)
  {
    if (key <= 0 || value <= 0 || souces.ContainsKey(key))
      return;
    souces.Add(key, value);
  }

  protected void AddList(List<int> souces, int value)
  {
    if (value <= 0 || souces.Contains(value))
      return;
    souces.Add(value);
  }
}
