﻿// Decompiled with JetBrains decompiler
// Type: RankBoardDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class RankBoardDlg : UI.Dialog
{
  private GameObjectPool m_ItemPool = new GameObjectPool();
  private List<RankEntranceItem> optItemUI = new List<RankEntranceItem>();
  private float m_DesignResolutionRatio = 1.777778f;
  private const float DEFAULT_DESIGN_RATIO = 1.777778f;
  public GameObject itemTemplate;
  public GameObject itemRoot;
  public UILabel title;
  public UIGrid grid;
  public UIScrollView scroll;
  [SerializeField]
  private UIPanel leftBar;
  [SerializeField]
  private UIPanel rightBar;
  private bool m_ItemPoolInitialized;
  private bool hasOpen;
  private RankBoardDlg.Parameter _Parameter;

  private void Clear()
  {
    for (int index = 0; index < this.optItemUI.Count; ++index)
    {
      this.optItemUI[index].Clear();
      this.m_ItemPool.Release(this.optItemUI[index].gameObject);
    }
    this.optItemUI.Clear();
  }

  private float getAdjustValue()
  {
    float num1 = (float) Screen.width * 1f / (float) Screen.height;
    float num2 = num1 / this.m_DesignResolutionRatio;
    if ((double) num1 >= (double) this.m_DesignResolutionRatio)
      num2 = 1f;
    if ((double) num2 != (double) this.transform.localScale.x || (double) num2 != (double) this.transform.localScale.y)
      return num2;
    return 1f;
  }

  private void initBar()
  {
    UIPanel component = this.scroll.gameObject.GetComponent<UIPanel>();
    UITexture componentInChildren = this.leftBar.gameObject.GetComponentInChildren<UITexture>();
    float width = (float) componentInChildren.width;
    float num1 = (float) ((double) component.width / 2.0 - (double) width / 2.0);
    float num2 = width - component.width / 2f;
    float height = component.height;
    this.leftBar.SetRect(0.0f, 0.0f, num2 + 30f, height);
    this.leftBar.clipOffset = new Vector2((float) (-(double) num1 - (double) num2 / 2.0), component.clipOffset.y);
    Vector2 size = new Vector2(componentInChildren.transform.parent.localScale.x, componentInChildren.transform.parent.localScale.y);
    Vector2 position = new Vector2(1f - size.x, (float) ((1.0 - (double) size.y) / 2.0));
    componentInChildren.uvRect = new Rect(position, size);
    this.rightBar.SetRect(0.0f, 0.0f, num2 + 30f, height);
    this.rightBar.clipOffset = new Vector2((float) (-(double) num1 - (double) num2 / 2.0), component.clipOffset.y);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._Parameter = orgParam as RankBoardDlg.Parameter;
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Clear();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.initBar();
    if (!this.m_ItemPoolInitialized)
    {
      this.m_ItemPool.Initialize(this.itemTemplate, this.grid.gameObject);
      this.m_ItemPoolInitialized = true;
    }
    this.Clear();
    this.title.text = ScriptLocalization.Get("leaderboards_uppercase_title", true);
    this.InitItems();
    MessageHub.inst.GetPortByAction(this._Parameter.LeaderBoardType != RankBoardDlg.LeaderBoardType.Kingdom ? "player:loadAllLeaderboardForGlobal" : "player:loadAllLeaderboard").SendLoader((Hashtable) null, new System.Action<bool, object>(this.RefreshData), true, false);
  }

  private Hashtable initData(bool result, object sourceData)
  {
    Hashtable hashtable = sourceData as Hashtable;
    if (!result || hashtable == null)
      return (Hashtable) null;
    return hashtable;
  }

  private void RefreshData(bool result, object oriData)
  {
    Hashtable data = this.initData(result, oriData);
    using (List<RankEntranceItem>.Enumerator enumerator = this.optItemUI.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetInfo(data, this._Parameter.LeaderBoardType);
    }
  }

  private void InitItems()
  {
    for (int id = 0; id < 7; ++id)
    {
      GameObject gameObject = this.m_ItemPool.AddChild(this.grid.gameObject);
      gameObject.SetActive(true);
      RankEntranceItem component = gameObject.GetComponent<RankEntranceItem>();
      this.optItemUI.Add(component);
      component.InitItem(id);
    }
    this.grid.Reposition();
    if (this.hasOpen)
      return;
    this.scroll.ResetPosition();
    this.hasOpen = true;
  }

  public void OnCloseBtnClicked()
  {
    this.Clear();
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnBckBtnClicked()
  {
    this.Clear();
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnButtonWorldRankClicked()
  {
    UIManager.inst.OpenDlg("LeaderBoard/WorldLeaderRankBoardDlg", (UI.Dialog.DialogParameter) new RankBoardDlg.Parameter()
    {
      LeaderBoardType = RankBoardDlg.LeaderBoardType.World
    }, true, true, true);
  }

  public enum LeaderBoardType
  {
    Kingdom,
    World,
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public RankBoardDlg.LeaderBoardType LeaderBoardType;
  }
}
