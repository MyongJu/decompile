﻿// Decompiled with JetBrains decompiler
// Type: PlayerEquipmentDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PlayerEquipmentDialog : UI.Dialog
{
  private Dictionary<long, HeroItemSlot> m_ItemDict = new Dictionary<long, HeroItemSlot>();
  private List<HeroItemBenefitRenderer> m_BenefitList = new List<HeroItemBenefitRenderer>();
  private List<EquipmentSuitItem> m_AllEquipmentSuitItem = new List<EquipmentSuitItem>();
  private Group m_Group = new Group();
  public const string SUIT_GEM_PATH = "Prefab/UI/Common/EquipmentGemItem";
  public const string SUIT_ITEM_PATH = "Prefab/UI/Common/EquipmentSuitItem";
  [SerializeField]
  private UILabel m_ItemTypeName;
  [SerializeField]
  private UITexture m_ItemCategory;
  [SerializeField]
  private UIButton m_EquipButton;
  [SerializeField]
  private UIButton m_UnequipButton;
  [SerializeField]
  private UIButton m_ReplaceButton;
  [SerializeField]
  private UIButton m_GotoForgeButton;
  [SerializeField]
  private UIButton m_GotoArmoryButton;
  [SerializeField]
  private HeroItemSlot m_ItemSlot;
  [SerializeField]
  private UILabel m_ItemName;
  [SerializeField]
  private UILabel m_ItemDescription;
  [SerializeField]
  private UILabel m_ItemFreezeDescription;
  [SerializeField]
  private UIScrollView m_BenefitScrollView;
  [SerializeField]
  private UITable m_BenefitTable;
  [SerializeField]
  private GameObject m_BenefitRenderer;
  [SerializeField]
  private UIScrollView m_ItemScrollView;
  [SerializeField]
  private UIGrid m_ItemGrid;
  [SerializeField]
  private GameObject m_ItemRenderer;
  [SerializeField]
  private GameObject m_RightNormal;
  [SerializeField]
  private GameObject m_RightEmpty;
  [SerializeField]
  private GameObject m_LeftNormal;
  [SerializeField]
  private GameObject m_LeftEmpty;
  [SerializeField]
  private GameObject m_BenefitTitle;
  [SerializeField]
  private Transform m_equipmentGemContainer;
  [SerializeField]
  private Transform m_equipmentSuitContainer;
  private PlayerEquipmentDialog.Parameter m_Parameter;
  private EquipmentGemItem _equipmentGemItem;
  private bool _scrollViewDirty;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as PlayerEquipmentDialog.Parameter;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (orgParam != null)
      this.m_Parameter = orgParam as PlayerEquipmentDialog.Parameter;
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearItemList();
    this.ClearBenefitList();
  }

  public void OnGotoForge()
  {
    UIManager.inst.OpenDlg(this.m_Parameter.bagType != BagType.Hero ? "Equipment/DragonKnightEquipmentForgeDialog" : "Equipment/EquipmentForgeDlg", (UI.Dialog.DialogParameter) new EquipmentForgeDlg.Parameter()
    {
      bagType = this.m_Parameter.bagType
    }, true, true, true);
  }

  public void OnGotoArmory()
  {
    UIManager.inst.OpenDlg(this.m_Parameter.bagType != BagType.Hero ? "Equipment/DragonKnightEquipmentTreasuryDialog" : "Equipment/EquipmentTreasuryDlg", (UI.Dialog.DialogParameter) new EquipmentTreasuryDlg.Parameter()
    {
      selectedEquipmentID = this.m_ItemSlot.ItemData.equipment.internalId,
      bagType = this.m_Parameter.bagType
    }, true, true, true);
  }

  private void UpdateUI()
  {
    this.m_GotoForgeButton.isEnabled = this.IsForgeAvailable;
    this.m_GotoArmoryButton.isEnabled = this.m_GotoForgeButton.isEnabled;
    this.m_ItemTypeName.text = HeroItemUtils.GetItemTypeName(this.m_Parameter.equipmentType, this.m_Parameter.bagType);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemCategory, ConfigEquipmentMainInfo.GetCategoryPath(this.m_Parameter.equipmentType, this.m_Parameter.bagType), (System.Action<bool>) null, true, true, string.Empty);
    this.UpdateItemList();
  }

  private bool IsForgeAvailable
  {
    get
    {
      if (this.m_Parameter.bagType == BagType.Hero)
        return CityManager.inst.GetHighestBuildingLevelFor("forge") > 0;
      return true;
    }
  }

  private void ClearItemList()
  {
    using (Dictionary<long, HeroItemSlot>.ValueCollection.Enumerator enumerator = this.m_ItemDict.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HeroItemSlot current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ItemDict.Clear();
    this.m_Group.ClearMembers();
  }

  private void UpdateItemList()
  {
    this.ClearItemList();
    List<InventoryItemData> myItemListByType = DBManager.inst.GetInventory(this.m_Parameter.bagType).GetMyItemListByType((int) this.m_Parameter.equipmentType);
    myItemListByType.Sort(new Comparison<InventoryItemData>(PlayerEquipmentDialog.CompareItemData));
    for (int index = 0; index < myItemListByType.Count; ++index)
    {
      InventoryItemData itemData = myItemListByType[index];
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemRenderer);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_ItemGrid.transform;
      gameObject.transform.localScale = Vector3.one;
      HeroItemSlot component = gameObject.GetComponent<HeroItemSlot>();
      component.SetData(itemData, this.m_Group, new OnHeroItemSlotClicked(this.OnItemSlotClicked));
      if (index == 0)
      {
        this.OnItemSlotClicked(itemData);
        component.Selected();
      }
      this.m_ItemDict.Add(itemData.itemId, component);
    }
    this.m_RightNormal.SetActive(this.m_ItemDict.Count > 0);
    this.m_RightEmpty.SetActive(this.m_ItemDict.Count == 0);
    this.m_LeftNormal.SetActive(this.m_ItemDict.Count > 0);
    this.m_LeftEmpty.SetActive(this.m_ItemDict.Count == 0);
    this.m_GotoForgeButton.gameObject.SetActive(this.m_ItemDict.Count == 0);
    this.m_GotoArmoryButton.gameObject.SetActive(this.m_ItemDict.Count > 0);
    this.RepositonItemGrid();
    this.m_ItemScrollView.ResetPosition();
  }

  private void RepositonItemGrid()
  {
    this.m_ItemGrid.sorting = UIGrid.Sorting.Custom;
    this.m_ItemGrid.onCustomSort = new Comparison<Transform>(PlayerEquipmentDialog.CompareItemRenderer);
    this.m_ItemGrid.Reposition();
  }

  private static int CompareItemRenderer(Transform x, Transform y)
  {
    return PlayerEquipmentDialog.CompareItemData(x.GetComponent<HeroItemSlot>().ItemData, y.GetComponent<HeroItemSlot>().ItemData);
  }

  private static int CompareItemData(InventoryItemData x, InventoryItemData y)
  {
    int num1 = y.equipped - x.equipped;
    if (num1 != 0)
      return num1;
    int num2 = y.equipment.quality - x.equipment.quality;
    if (num2 != 0)
      return num2;
    int num3 = y.equipment.heroLevelMin - x.equipment.heroLevelMin;
    if (num3 != 0)
      return num3;
    return x.equipment.tendency - y.equipment.tendency;
  }

  private void OnItemSlotClicked(InventoryItemData itemData)
  {
    this.m_ItemSlot.SetData(itemData, (Group) null, (OnHeroItemSlotClicked) null);
    this.UpdateBenefitList(itemData);
    this.UpdateEquipmentGem(itemData);
    this.UpdateEquipmentSuitBenefit(itemData);
    this.UpdateButtons(itemData);
    this.UpdateItemName(itemData);
    this.UpadteItemLevelRequired(itemData);
  }

  private void ClearEquipmentSuitItem()
  {
    using (List<EquipmentSuitItem>.Enumerator enumerator = this.m_AllEquipmentSuitItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EquipmentSuitItem current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_AllEquipmentSuitItem.Clear();
  }

  private void UpdateEquipmentSuitBenefit(InventoryItemData itemData)
  {
    this.ClearEquipmentSuitItem();
    ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(this.m_Parameter.bagType).GetData(itemData.internalId);
    if (data != null && data.suitGroup != 0)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load("Prefab/UI/Common/EquipmentSuitItem", (System.Type) null)) as GameObject;
      AssetManager.Instance.UnLoadAsset("Prefab/UI/Common/EquipmentSuitItem", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      Transform parent = this.m_equipmentSuitContainer.transform.parent;
      this.m_equipmentSuitContainer.transform.SetParent((Transform) null, true);
      this.m_equipmentSuitContainer.SetParent(parent, true);
      gameObject.transform.SetParent(this.m_equipmentSuitContainer, true);
      gameObject.transform.localScale = Vector3.one;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.SetActive(true);
      EquipmentSuitItem component = gameObject.GetComponent<EquipmentSuitItem>();
      component.SetData(PlayerData.inst.uid, this.m_Parameter.bagType, data.suitGroup, itemData);
      this.m_BenefitTable.Reposition();
      this.m_BenefitScrollView.ResetPosition();
      this.m_AllEquipmentSuitItem.Add(component);
    }
    this._scrollViewDirty = true;
  }

  private void UpdateEquipmentGem(InventoryItemData itemData)
  {
    if ((bool) ((UnityEngine.Object) this._equipmentGemItem))
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this._equipmentGemItem.gameObject);
      this._equipmentGemItem = (EquipmentGemItem) null;
    }
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load("Prefab/UI/Common/EquipmentGemItem", (System.Type) null)) as GameObject;
    AssetManager.Instance.UnLoadAsset("Prefab/UI/Common/EquipmentGemItem", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    gameObject.SetActive(true);
    Transform parent = this.m_equipmentGemContainer.transform.parent;
    this.m_equipmentGemContainer.transform.SetParent((Transform) null, true);
    this.m_equipmentGemContainer.SetParent(parent, true);
    gameObject.transform.SetParent(this.m_equipmentGemContainer, true);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    this._equipmentGemItem = gameObject.GetComponent<EquipmentGemItem>();
    this._equipmentGemItem.SetData(itemData, true, (UIScrollView) null);
    this._scrollViewDirty = true;
  }

  private void UpdateItemName(InventoryItemData itemData)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemData.equipment.itemID);
    if (itemStaticInfo == null)
      return;
    this.m_ItemName.text = itemData.enhanced <= 0 ? itemStaticInfo.LocName : itemStaticInfo.LocName + "+" + itemData.enhanced.ToString();
    EquipmentManager.Instance.ConfigQualityLabelWithColor(this.m_ItemName, itemData.equipment.quality);
  }

  private void UpadteItemLevelRequired(InventoryItemData itemData)
  {
    int heroLevelMin = itemData.equipment.heroLevelMin;
    this.m_ItemDescription.text = string.Format(this.m_Parameter.bagType != BagType.Hero ? Utils.XLAT("dragon_knight_equip_level_requirement") : Utils.XLAT("forge_equip_requirement"), (object) heroLevelMin);
    if (heroLevelMin > this.GetLevel())
      this.m_ItemDescription.color = Color.red;
    else
      this.m_ItemDescription.color = Color.white;
    this.m_ItemFreezeDescription.text = itemData.FreezeHint;
    NGUITools.SetActive(this.m_ItemFreezeDescription.gameObject, itemData.IsFreeze);
  }

  private void UpdateButtons(InventoryItemData itemData)
  {
    long equippedItemId = (long) this.GetEquippedItemID();
    bool flag1 = equippedItemId != 0L && itemData.itemId != equippedItemId;
    bool flag2 = this.GetLevel() >= itemData.equipment.heroLevelMin;
    this.m_EquipButton.gameObject.SetActive(!itemData.isEquipped && !flag1);
    this.m_ReplaceButton.gameObject.SetActive(!itemData.isEquipped && flag1);
    this.m_UnequipButton.gameObject.SetActive(itemData.isEquipped);
    this.m_EquipButton.isEnabled = flag2;
    this.m_ReplaceButton.isEnabled = flag2;
  }

  private void ClearBenefitList()
  {
    using (List<HeroItemBenefitRenderer>.Enumerator enumerator = this.m_BenefitList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HeroItemBenefitRenderer current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_BenefitList.Clear();
  }

  private void UpdateBenefitList(InventoryItemData itemData)
  {
    this.m_BenefitTitle.transform.SetParent((Transform) null, true);
    this.m_BenefitTitle.transform.SetParent(this.m_BenefitTable.transform, true);
    this.ClearBenefitList();
    List<Benefits.BenefitValuePair> benefitsPairs = ConfigManager.inst.GetEquipmentProperty(this.m_Parameter.bagType).GetDataByEquipIDAndEnhanceLevel(itemData.equipment.internalId, itemData.enhanced).benefits.GetBenefitsPairs();
    for (int index = 0; index < benefitsPairs.Count; ++index)
    {
      Benefits.BenefitValuePair benefitValuePair = benefitsPairs[index];
      PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitValuePair.internalID];
      string name = dbProperty.Name;
      string displayString = dbProperty.ConvertToDisplayString((double) benefitValuePair.value, true, true);
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_BenefitRenderer);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_BenefitTable.transform;
      gameObject.transform.localScale = Vector3.one;
      HeroItemBenefitRenderer component = gameObject.GetComponent<HeroItemBenefitRenderer>();
      component.SetData(name, displayString, dbProperty.ImagePath);
      this.m_BenefitList.Add(component);
    }
    this._scrollViewDirty = true;
  }

  private void LateUpdate()
  {
    if (!this._scrollViewDirty)
      return;
    this._scrollViewDirty = false;
    this.m_BenefitTable.Reposition();
    this.m_BenefitScrollView.ResetPosition();
  }

  public void OnCloseClicked()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnEquip()
  {
    List<Benefits.BenefitValuePair> allAddedBenifit;
    List<Benefits.BenefitValuePair> allRemovedBenefit;
    ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance;
    ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance;
    Utils.GetEquipmentBenefitChangedInfo(this.m_Parameter.bagType, this.m_ItemSlot.ItemData, (InventoryItemData) null, out allRemovedBenefit, out allAddedBenifit, out addEquipmentSuitEnhance, out delEquipmentSuitEnhance);
    EquipmentManager.Instance.Equip(this.m_Parameter.bagType, (long) (int) this.m_ItemSlot.ItemData.itemId, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.UpdateItemListAfterOperation();
      if (allAddedBenifit.Count <= 0 && allRemovedBenefit.Count <= 0 && (addEquipmentSuitEnhance == null && delEquipmentSuitEnhance == null))
        return;
      UIManager.inst.ShowEquipmentSuitBenefitChangeTip(allAddedBenifit, allRemovedBenefit, addEquipmentSuitEnhance, delEquipmentSuitEnhance);
    }));
  }

  public void OnUnequip()
  {
    List<Benefits.BenefitValuePair> allAddedBenifit;
    List<Benefits.BenefitValuePair> allRemovedBenefit;
    ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance;
    ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance;
    Utils.GetEquipmentBenefitChangedInfo(this.m_Parameter.bagType, (InventoryItemData) null, this.m_ItemSlot.ItemData, out allRemovedBenefit, out allAddedBenifit, out addEquipmentSuitEnhance, out delEquipmentSuitEnhance);
    EquipmentManager.Instance.UnEquip(this.m_Parameter.bagType, (long) (int) this.m_ItemSlot.ItemData.itemId, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.UpdateItemListAfterOperation();
      if (allAddedBenifit.Count <= 0 && allRemovedBenefit.Count <= 0 && (addEquipmentSuitEnhance == null && delEquipmentSuitEnhance == null))
        return;
      UIManager.inst.ShowEquipmentSuitBenefitChangeTip(allAddedBenifit, allRemovedBenefit, addEquipmentSuitEnhance, delEquipmentSuitEnhance);
    }));
  }

  public void OnReplace()
  {
    int equippedItemId = this.GetEquippedItemID();
    List<Benefits.BenefitValuePair> allAddedBenifit;
    List<Benefits.BenefitValuePair> allRemovedBenefit;
    ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance;
    ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance;
    Utils.GetEquipmentBenefitChangedInfo(this.m_Parameter.bagType, this.m_ItemSlot.ItemData, DBManager.inst.GetInventory(this.m_Parameter.bagType).GetByItemID(PlayerData.inst.uid, (long) equippedItemId), out allRemovedBenefit, out allAddedBenifit, out addEquipmentSuitEnhance, out delEquipmentSuitEnhance);
    EquipmentManager.Instance.ReplaceEquipment(this.m_Parameter.bagType, (long) equippedItemId, (long) (int) this.m_ItemSlot.ItemData.itemId, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.UpdateItemListAfterOperation();
      if (allAddedBenifit.Count <= 0 && allRemovedBenefit.Count <= 0 && (addEquipmentSuitEnhance == null && delEquipmentSuitEnhance == null))
        return;
      UIManager.inst.ShowEquipmentSuitBenefitChangeTip(allAddedBenifit, allRemovedBenefit, addEquipmentSuitEnhance, delEquipmentSuitEnhance);
    }));
  }

  private int GetEquippedItemID()
  {
    if (this.m_Parameter.bagType == BagType.DragonKnight)
    {
      if (PlayerData.inst.dragonKnightData != null)
        return PlayerData.inst.dragonKnightData.equipments.GetSlotEquipID((int) this.m_Parameter.equipmentType);
      return 0;
    }
    if (this.m_Parameter.bagType == BagType.Hero)
      return PlayerData.inst.heroData.equipments.GetSlotEquipID((int) this.m_Parameter.equipmentType);
    return 0;
  }

  private int GetLevel()
  {
    if (this.m_Parameter.bagType == BagType.DragonKnight)
      return PlayerData.inst.dragonKnightData.Level;
    if (this.m_Parameter.bagType == BagType.Hero)
      return PlayerData.inst.heroData.level;
    return 0;
  }

  private void UpdateItemListAfterOperation()
  {
    this.OnItemSlotClicked(this.m_ItemSlot.ItemData);
    Dictionary<long, HeroItemSlot>.Enumerator enumerator = this.m_ItemDict.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Value.UpdateUI();
    this.RepositonItemGrid();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public HeroItemType equipmentType;
    public BagType bagType;
  }
}
