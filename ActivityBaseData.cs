﻿// Decompiled with JetBrains decompiler
// Type: ActivityBaseData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ActivityBaseData
{
  private ActivityBaseData.ActivityType _type;
  private long _startTime;
  private long _endTime;
  private int _activityId;

  public virtual ActivityBaseData.ActivityType Type
  {
    get
    {
      return this._type;
    }
    set
    {
      this._type = value;
    }
  }

  public virtual void Decode(Hashtable source)
  {
    if (source == null)
      return;
    string empty = string.Empty;
    if (source.ContainsKey((object) "start_time") && source[(object) "start_time"] != null)
      long.TryParse(source[(object) "start_time"].ToString(), out this._startTime);
    if (source.ContainsKey((object) "end_time") && source[(object) "end_time"] != null)
      long.TryParse(source[(object) "end_time"].ToString(), out this._endTime);
    if (!source.ContainsKey((object) "activity_id") || source[(object) "activity_id"] == null)
      return;
    int.TryParse(source[(object) "activity_id"].ToString(), out this._activityId);
  }

  public virtual bool IsEmpty()
  {
    if (this._startTime > 0L)
      return this._endTime <= 0L;
    return true;
  }

  public int StartTime
  {
    get
    {
      return (int) this._startTime;
    }
  }

  public int EndTime
  {
    get
    {
      return (int) this._endTime;
    }
  }

  public virtual int ActivityId
  {
    get
    {
      return this._activityId;
    }
  }

  protected void AddDict(Dictionary<int, int> souces, int key, int value)
  {
    if (key <= 0 || value <= 0 || souces.ContainsKey(key))
      return;
    souces.Add(key, value);
  }

  protected void AddList(List<int> souces, int value)
  {
    if (value <= 0 || souces.Contains(value))
      return;
    souces.Add(value);
  }

  public bool IsStart()
  {
    if (this._startTime <= (long) NetServerTime.inst.ServerTimestamp)
      return this._endTime > (long) NetServerTime.inst.ServerTimestamp;
    return false;
  }

  public virtual int RemainTime
  {
    get
    {
      if (this.IsStart())
        return this.EndTime - NetServerTime.inst.ServerTimestamp;
      return this.StartTime - NetServerTime.inst.ServerTimestamp;
    }
  }

  public enum ActivityType
  {
    None,
    TimeLimit,
    AllianceBoss,
    AllianceActivity,
    AllianceWar,
    Festival,
    Knight,
    Personal,
    WorldBoss,
    DKArena,
  }

  private struct Params
  {
    public const string START_TIME = "start_time";
    public const string END_TIME = "end_time";
    public const string ACTIVITY_MAIN_ID = "activity_id";
  }
}
