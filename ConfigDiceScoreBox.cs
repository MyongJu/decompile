﻿// Decompiled with JetBrains decompiler
// Type: ConfigDiceScoreBox
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigDiceScoreBox
{
  private List<DiceScoreBoxInfo> _diceScoreBoxInfoList = new List<DiceScoreBoxInfo>();
  private Dictionary<string, DiceScoreBoxInfo> _datas;
  private Dictionary<int, DiceScoreBoxInfo> _dicByUniqueId;

  public List<DiceScoreBoxInfo> DiceScoreBoxList
  {
    get
    {
      return this._diceScoreBoxInfoList;
    }
  }

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<DiceScoreBoxInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, DiceScoreBoxInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._diceScoreBoxInfoList.Add(enumerator.Current);
    }
    this._diceScoreBoxInfoList.Sort((Comparison<DiceScoreBoxInfo>) ((a, b) => a.level.CompareTo(b.level)));
  }

  public DiceScoreBoxInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (DiceScoreBoxInfo) null;
  }

  public DiceScoreBoxInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (DiceScoreBoxInfo) null;
  }
}
