﻿// Decompiled with JetBrains decompiler
// Type: NoDragonSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class NoDragonSlot : MonoBehaviour
{
  public UISlider timer;
  public UILabel timerText;

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    this.OnSecond(NetServerTime.inst.ServerTimestamp);
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void OnSecond(int serverTime)
  {
    CityManager.BuildingItem buildingFromType = PlayerData.inst.CityData.GetBuildingFromType("dragon_lair");
    if (buildingFromType != null)
    {
      JobHandle job = JobManager.Instance.GetJob(buildingFromType.mTrainingJobID);
      if (job != null)
      {
        if (!this.timer.gameObject.activeSelf)
          this.gameObject.SetActive(true);
        this.timer.value = (float) (serverTime - job.StartTime()) / (float) job.Duration();
        this.timerText.text = Utils.FormatTime(job.EndTime() - serverTime, false, false, true);
      }
      else
        this.timer.gameObject.SetActive(false);
    }
    else
      this.timer.gameObject.SetActive(false);
  }
}
