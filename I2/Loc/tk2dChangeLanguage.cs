﻿// Decompiled with JetBrains decompiler
// Type: I2.Loc.tk2dChangeLanguage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace I2.Loc
{
  public class tk2dChangeLanguage : MonoBehaviour
  {
    public void SetLanguage_English()
    {
      this.SetLanguage("English");
    }

    public void SetLanguage_French()
    {
      this.SetLanguage("French");
    }

    public void SetLanguage_Spanish()
    {
      this.SetLanguage("Spanish");
    }

    public void SetLanguage(string LangName)
    {
      if (!LocalizationManager.HasLanguage(LangName, true, true))
        return;
      LocalizationManager.CurrentLanguage = LangName;
    }
  }
}
