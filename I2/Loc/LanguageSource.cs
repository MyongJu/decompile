﻿// Decompiled with JetBrains decompiler
// Type: I2.Loc.LanguageSource
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

namespace I2.Loc
{
  [AddComponentMenu("I2/Localization/Source")]
  public class LanguageSource : MonoBehaviour
  {
    public static string EmptyCategory = "Default";
    public static char[] CategorySeparators = "/\\".ToCharArray();
    public LanguageSource.eGoogleUpdateFrequency GoogleUpdateFrequency = LanguageSource.eGoogleUpdateFrequency.Weekly;
    public List<TermData> mTerms = new List<TermData>();
    public List<LanguageData> mLanguages = new List<LanguageData>();
    [NonSerialized]
    public Dictionary<string, TermData> mDictionary = new Dictionary<string, TermData>((IEqualityComparer<string>) StringComparer.Ordinal);
    public bool NeverDestroy = true;
    public string Google_WebServiceURL;
    public string Google_SpreadsheetKey;
    public string Google_SpreadsheetName;
    public string Google_LastUpdatedVersion;
    public bool CaseInsensitiveTerms;
    public UnityEngine.Object[] Assets;
    public bool UserAgreesToHaveItOnTheScene;

    public event Action<LanguageSource> Event_OnSourceUpdateFromGoogle;

    public string Export_CSV(string Category, char Separator = ',')
    {
      StringBuilder Builder = new StringBuilder();
      int count = this.mLanguages.Count;
      Builder.AppendFormat("Key{0}Type{0}Desc", (object) Separator);
      using (List<LanguageData>.Enumerator enumerator = this.mLanguages.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          LanguageData current = enumerator.Current;
          Builder.Append(Separator);
          LanguageSource.AppendString(Builder, GoogleLanguages.GetCodedLanguage(current.Name, current.Code), Separator);
        }
      }
      Builder.Append("\n");
      using (List<TermData>.Enumerator enumerator = this.mTerms.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          TermData current = enumerator.Current;
          string Text;
          if (string.IsNullOrEmpty(Category) || Category == LanguageSource.EmptyCategory && current.Term.IndexOfAny(LanguageSource.CategorySeparators) < 0)
            Text = current.Term;
          else if (current.Term.StartsWith(Category) && Category != current.Term)
            Text = current.Term.Substring(Category.Length + 1);
          else
            continue;
          LanguageSource.AppendString(Builder, Text, Separator);
          Builder.AppendFormat(((int) Separator).ToString() + current.TermType.ToString());
          Builder.Append(Separator);
          LanguageSource.AppendString(Builder, current.Description, Separator);
          for (int index = 0; index < Mathf.Min(count, current.Languages.Length); ++index)
          {
            Builder.Append(Separator);
            LanguageSource.AppendString(Builder, current.Languages[index], Separator);
          }
          Builder.Append("\n");
        }
      }
      return Builder.ToString();
    }

    private static void AppendString(StringBuilder Builder, string Text, char Separator)
    {
      if (string.IsNullOrEmpty(Text))
        return;
      Text = Text.Replace("\\n", "\n");
      if (Text.IndexOfAny((((int) Separator).ToString() + "\n\"").ToCharArray()) >= 0)
      {
        Text = Text.Replace("\"", "\"\"");
        Builder.AppendFormat("\"{0}\"", (object) Text);
      }
      else
        Builder.Append(Text);
    }

    public WWW Export_Google_CreateWWWcall(eSpreadsheetUpdateMode UpdateMode = eSpreadsheetUpdateMode.Replace)
    {
      string data = this.Export_Google_CreateData();
      WWWForm form = new WWWForm();
      form.AddField("key", this.Google_SpreadsheetKey);
      form.AddField("action", "SetLanguageSource");
      form.AddField("data", data);
      form.AddField("updateMode", UpdateMode.ToString());
      return new WWW(this.Google_WebServiceURL, form);
    }

    private string Export_Google_CreateData()
    {
      List<string> categories = this.GetCategories(true, (List<string>) null);
      StringBuilder stringBuilder = new StringBuilder();
      bool flag = true;
      using (List<string>.Enumerator enumerator = categories.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          if (flag)
            flag = false;
          else
            stringBuilder.Append("<I2Loc>");
          string str = this.Export_CSV(current, ',');
          stringBuilder.Append(current);
          stringBuilder.Append("<I2Loc>");
          stringBuilder.Append(str);
        }
      }
      return stringBuilder.ToString();
    }

    public string Import_CSV(string Category, string CSVstring, eSpreadsheetUpdateMode UpdateMode = eSpreadsheetUpdateMode.Replace, char Separator = ',')
    {
      List<string[]> csv = LocalizationReader.ReadCSV(CSVstring, Separator);
      this.PreprocessCSV(ref csv);
      string[] strArray1 = csv[0];
      int num1 = 1;
      int index1 = -1;
      int index2 = -1;
      string[] strArray2 = new string[1]{ "Key" };
      string[] strArray3 = new string[1]{ "Type" };
      string[] strArray4 = new string[2]
      {
        "Desc",
        "Description"
      };
      if (strArray1.Length <= 1 || !this.ArrayContains(strArray1[0], strArray2))
        return "Bad Spreadsheet Format.\nFirst columns should be 'Key', 'Type' and 'Desc'";
      if (UpdateMode == eSpreadsheetUpdateMode.Replace)
        this.ClearAllData();
      if (strArray1.Length > 2)
      {
        if (this.ArrayContains(strArray1[1], strArray3))
        {
          index1 = 1;
          num1 = 2;
        }
        if (this.ArrayContains(strArray1[1], strArray4))
        {
          index2 = 1;
          num1 = 2;
        }
      }
      if (strArray1.Length > 3)
      {
        if (this.ArrayContains(strArray1[2], strArray3))
        {
          index1 = 2;
          num1 = 3;
        }
        if (this.ArrayContains(strArray1[2], strArray4))
        {
          index2 = 2;
          num1 = 3;
        }
      }
      int length = Mathf.Max(strArray1.Length - num1, 0);
      int[] numArray = new int[length];
      for (int index3 = 0; index3 < length; ++index3)
      {
        string Language;
        string code;
        GoogleLanguages.UnPackCodeFromLanguageName(strArray1[index3 + num1], out Language, out code);
        int num2 = string.IsNullOrEmpty(code) ? this.GetLanguageIndex(Language, true) : this.GetLanguageIndexFromCode(code);
        if (num2 < 0)
        {
          this.mLanguages.Add(new LanguageData()
          {
            Name = Language,
            Code = code
          });
          num2 = this.mLanguages.Count - 1;
        }
        numArray[index3] = num2;
      }
      int count1 = this.mLanguages.Count;
      int index4 = 0;
      for (int count2 = this.mTerms.Count; index4 < count2; ++index4)
      {
        TermData mTerm = this.mTerms[index4];
        if (mTerm.Languages.Length < count1)
          Array.Resize<string>(ref mTerm.Languages, count1);
      }
      int index5 = 1;
      for (int count2 = csv.Count; index5 < count2; ++index5)
      {
        string[] strArray5 = csv[index5];
        string Term = !string.IsNullOrEmpty(Category) ? Category + "/" + strArray5[0] : strArray5[0];
        LanguageSource.ValidateFullTerm(ref Term);
        TermData termData = this.GetTermData(Term);
        if (termData == null)
        {
          termData = new TermData();
          termData.Term = Term;
          termData.Languages = new string[this.mLanguages.Count];
          for (int index3 = 0; index3 < this.mLanguages.Count; ++index3)
            termData.Languages[index3] = string.Empty;
          this.mTerms.Add(termData);
          this.mDictionary.Add(Term, termData);
        }
        else if (UpdateMode == eSpreadsheetUpdateMode.AddNewTerms)
          continue;
        if (index1 > 0)
          termData.TermType = LanguageSource.GetTermType(strArray5[index1]);
        if (index2 > 0)
          termData.Description = strArray5[index2];
        for (int index3 = 0; index3 < numArray.Length && index3 < strArray5.Length - num1; ++index3)
        {
          if (!string.IsNullOrEmpty(strArray5[index3 + num1]))
            termData.Languages[numArray[index3]] = strArray5[index3 + num1];
        }
      }
      return string.Empty;
    }

    private void PreprocessCSV(ref List<string[]> csv)
    {
      List<string[]> csv1 = new List<string[]>();
      List<int> intList = new List<int>();
      string[] strArray = csv[0];
      for (int index = 0; index < strArray.Length; ++index)
      {
        if (strArray[index].Contains("*") || string.Empty == strArray[index])
          intList.Add(index);
      }
      using (List<string[]>.Enumerator enumerator = csv.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          List<string> stringList = new List<string>((IEnumerable<string>) enumerator.Current);
          for (int index = 0; index < intList.Count; ++index)
          {
            if (stringList.Count <= intList[index] - index)
              Debug.LogWarning((object) ("something wrong " + stringList[0]));
            else
              stringList.RemoveAt(intList[index] - index);
          }
          csv1.Add(stringList.ToArray());
        }
      }
      this.CheckCorrect(csv1);
      csv = csv1;
    }

    private void CheckCorrect(List<string[]> csv)
    {
      using (List<string[]>.Enumerator enumerator = csv.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string[] current = enumerator.Current;
          for (int index1 = 0; index1 < current.Length; ++index1)
          {
            string input = current[index1];
            if (index1 != 2 && string.IsNullOrEmpty(input))
              current[index1] = string.Copy(current[3]);
            for (int index2 = 0; index2 < 5; ++index2)
            {
              Regex regex1 = new Regex(index2.ToString() + "(\\s*?)}");
              Regex regex2 = new Regex("{(\\s*?)" + (object) index2);
              string str1 = "{" + (object) index2 + "}";
              string str2 = "_" + (object) index2 + "}";
              if ((regex1.Match(input).Success || regex2.Match(input).Success) && (!input.Contains(str1) && !input.Contains(str2)))
                Debug.LogWarning((object) ("maybe has error parameter!!! " + input));
            }
          }
        }
      }
    }

    private bool ArrayContains(string MainText, params string[] texts)
    {
      int index = 0;
      for (int length = texts.Length; index < length; ++index)
      {
        if (MainText.IndexOf(texts[index], StringComparison.OrdinalIgnoreCase) >= 0)
          return true;
      }
      return false;
    }

    public static eTermType GetTermType(string type)
    {
      int num = 0;
      for (int index = 8; num <= index; ++num)
      {
        if (string.Equals(((eTermType) num).ToString(), type, StringComparison.OrdinalIgnoreCase))
          return (eTermType) num;
      }
      return eTermType.Text;
    }

    public void Import_Google(bool ForceUpdate = false)
    {
      if (this.GoogleUpdateFrequency == LanguageSource.eGoogleUpdateFrequency.Never || !Application.isPlaying)
        return;
      string sourcePlayerPrefName = this.GetSourcePlayerPrefName();
      DateTime result;
      if (!ForceUpdate && this.GoogleUpdateFrequency != LanguageSource.eGoogleUpdateFrequency.Always && DateTime.TryParse(PlayerPrefs.GetString("LastGoogleUpdate_" + sourcePlayerPrefName, string.Empty), out result))
      {
        double totalDays = (DateTime.Now - result).TotalDays;
        switch (this.GoogleUpdateFrequency)
        {
          case LanguageSource.eGoogleUpdateFrequency.Daily:
            if (totalDays < 1.0)
              return;
            break;
          case LanguageSource.eGoogleUpdateFrequency.Weekly:
            if (totalDays < 8.0)
              return;
            break;
          case LanguageSource.eGoogleUpdateFrequency.Monthly:
            if (totalDays < 31.0)
              return;
            break;
        }
      }
      PlayerPrefs.SetString("LastGoogleUpdate_" + sourcePlayerPrefName, DateTime.Now.ToString());
      string JsonString = PlayerPrefs.GetString("I2Source_" + this.Google_SpreadsheetKey, string.Empty);
      if (!string.IsNullOrEmpty(JsonString))
        this.Import_Google_Result(JsonString, eSpreadsheetUpdateMode.Replace);
      CoroutineManager.pInstance.StartCoroutine(this.Import_Google_Coroutine());
    }

    private string GetSourcePlayerPrefName()
    {
      if (Array.IndexOf<string>(LocalizationManager.GlobalSources, this.name) >= 0)
        return this.name;
      return Application.loadedLevelName + "_" + this.name;
    }

    [DebuggerHidden]
    private IEnumerator Import_Google_Coroutine()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new LanguageSource.\u003CImport_Google_Coroutine\u003Ec__Iterator15()
      {
        \u003C\u003Ef__this = this
      };
    }

    public WWW Import_Google_CreateWWWcall(bool ForceUpdate = false)
    {
      if (!this.HasGoogleSpreadsheet())
        return (WWW) null;
      return new WWW(string.Format("{0}?key={1}&action=GetLanguageSource&version={2}", (object) this.Google_WebServiceURL, (object) this.Google_SpreadsheetKey, !ForceUpdate ? (object) this.Google_LastUpdatedVersion : (object) "0"));
    }

    public bool HasGoogleSpreadsheet()
    {
      if (!string.IsNullOrEmpty(this.Google_WebServiceURL))
        return !string.IsNullOrEmpty(this.Google_SpreadsheetKey);
      return false;
    }

    public string Import_Google_Result(string JsonString, eSpreadsheetUpdateMode UpdateMode)
    {
      string str = string.Empty;
      if (JsonString == "\"\"")
      {
        Debug.Log((object) "Language Source was up to date");
        return str;
      }
      JSONClass asObject = JSON.Parse(JsonString).AsObject;
      if (UpdateMode == eSpreadsheetUpdateMode.Replace)
        this.ClearAllData();
      this.Google_LastUpdatedVersion = (string) asObject["version"];
      if ((string) asObject["script_version"] != LocalizationManager.GetRequiredWebServiceVersion())
        str = "The current Google WebService is not supported.\nPlease, delete the WebService from the Google Drive and Install the latest version.";
      foreach (KeyValuePair<string, JSONNode> keyValuePair in asObject["spreadsheet"].AsObject)
      {
        this.Import_CSV(keyValuePair.Key, (string) keyValuePair.Value, UpdateMode, ',');
        if (UpdateMode == eSpreadsheetUpdateMode.Replace)
          UpdateMode = eSpreadsheetUpdateMode.Merge;
      }
      return str;
    }

    public List<string> GetCategories(bool OnlyMainCategory = false, List<string> Categories = null)
    {
      if (Categories == null)
        Categories = new List<string>();
      using (List<TermData>.Enumerator enumerator = this.mTerms.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string categoryFromFullTerm = LanguageSource.GetCategoryFromFullTerm(enumerator.Current.Term, OnlyMainCategory);
          if (!Categories.Contains(categoryFromFullTerm))
            Categories.Add(categoryFromFullTerm);
        }
      }
      Categories.Sort();
      return Categories;
    }

    public static string GetKeyFromFullTerm(string FullTerm, bool OnlyMainCategory = false)
    {
      int num = !OnlyMainCategory ? FullTerm.LastIndexOfAny(LanguageSource.CategorySeparators) : FullTerm.IndexOfAny(LanguageSource.CategorySeparators);
      if (num < 0)
        return FullTerm;
      return FullTerm.Substring(num + 1);
    }

    public static string GetCategoryFromFullTerm(string FullTerm, bool OnlyMainCategory = false)
    {
      int length = !OnlyMainCategory ? FullTerm.LastIndexOfAny(LanguageSource.CategorySeparators) : FullTerm.IndexOfAny(LanguageSource.CategorySeparators);
      if (length < 0)
        return LanguageSource.EmptyCategory;
      return FullTerm.Substring(0, length);
    }

    public static void DeserializeFullTerm(string FullTerm, out string Key, out string Category, bool OnlyMainCategory = false)
    {
      int length = !OnlyMainCategory ? FullTerm.LastIndexOfAny(LanguageSource.CategorySeparators) : FullTerm.IndexOfAny(LanguageSource.CategorySeparators);
      if (length < 0)
      {
        Category = LanguageSource.EmptyCategory;
        Key = FullTerm;
      }
      else
      {
        Category = FullTerm.Substring(0, length);
        Key = FullTerm.Substring(length + 1);
      }
    }

    internal static string GetKeyFromFullTerm(string FullTerm)
    {
      int num = FullTerm.LastIndexOfAny(LanguageSource.CategorySeparators);
      return FullTerm.Substring(num + 1);
    }

    internal static string GetCategoryFromFullTerm(string FullTerm)
    {
      int length = FullTerm.LastIndexOfAny(LanguageSource.CategorySeparators);
      if (length < 0)
        return LanguageSource.EmptyCategory;
      return FullTerm.Substring(0, length);
    }

    internal static void DeserializeFullTerm(string FullTerm, out string Key, out string Category)
    {
      int length = FullTerm.LastIndexOfAny(LanguageSource.CategorySeparators);
      if (length < 0)
      {
        Category = LanguageSource.EmptyCategory;
        Key = FullTerm;
      }
      else
      {
        Category = FullTerm.Substring(0, length);
        Key = FullTerm.Substring(length + 1);
      }
    }

    private void Awake()
    {
      if (this.NeverDestroy)
      {
        if (this.ManagerHasASimilarSource())
        {
          UnityEngine.Object.Destroy((UnityEngine.Object) this);
          return;
        }
        UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) this.gameObject);
      }
      LocalizationManager.AddSource(this);
      this.UpdateDictionary();
    }

    public void UpdateDictionary()
    {
      StringComparer stringComparer = !this.CaseInsensitiveTerms ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase;
      if (this.mDictionary.Comparer != stringComparer)
        this.mDictionary = new Dictionary<string, TermData>((IEqualityComparer<string>) stringComparer);
      else
        this.mDictionary.Clear();
      int index = 0;
      for (int count = this.mTerms.Count; index < count; ++index)
      {
        LanguageSource.ValidateFullTerm(ref this.mTerms[index].Term);
        this.mDictionary[this.mTerms[index].Term] = this.mTerms[index];
      }
    }

    public string GetSourceName()
    {
      string str = this.gameObject.name;
      for (Transform parent = this.transform.parent; (bool) ((UnityEngine.Object) parent); parent = parent.parent)
        str = parent.name + "_" + str;
      return str;
    }

    public int GetLanguageIndex(string language, bool AllowDiscartingRegion = true)
    {
      int index1 = 0;
      for (int count = this.mLanguages.Count; index1 < count; ++index1)
      {
        if (string.Compare(this.mLanguages[index1].Name, language, StringComparison.OrdinalIgnoreCase) == 0)
          return index1;
      }
      if (AllowDiscartingRegion)
      {
        int index2 = 0;
        for (int count = this.mLanguages.Count; index2 < count; ++index2)
        {
          if (LanguageSource.AreTheSameLanguage(this.mLanguages[index2].Name, language))
            return index2;
        }
      }
      return -1;
    }

    public int GetLanguageIndexFromCode(string Code)
    {
      int index = 0;
      for (int count = this.mLanguages.Count; index < count; ++index)
      {
        if (string.Compare(this.mLanguages[index].Code, Code, StringComparison.OrdinalIgnoreCase) == 0)
          return index;
      }
      return -1;
    }

    public static bool AreTheSameLanguage(string Language1, string Language2)
    {
      Language1 = LanguageSource.GetLanguageWithoutRegion(Language1);
      Language2 = LanguageSource.GetLanguageWithoutRegion(Language2);
      return string.Compare(Language1, Language2, StringComparison.OrdinalIgnoreCase) == 0;
    }

    public static string GetLanguageWithoutRegion(string Language)
    {
      int length = Language.IndexOfAny("(/\\[,{".ToCharArray());
      if (length < 0)
        return Language;
      return Language.Substring(0, length).Trim();
    }

    public void AddLanguage(string LanguageName, string LanguageCode)
    {
      if (this.GetLanguageIndex(LanguageName, false) >= 0)
        return;
      this.mLanguages.Add(new LanguageData()
      {
        Name = LanguageName,
        Code = LanguageCode
      });
      int count1 = this.mLanguages.Count;
      int index = 0;
      for (int count2 = this.mTerms.Count; index < count2; ++index)
        Array.Resize<string>(ref this.mTerms[index].Languages, count1);
    }

    public void RemoveLanguage(string LanguageName)
    {
      int languageIndex = this.GetLanguageIndex(LanguageName, true);
      if (languageIndex < 0)
        return;
      int count1 = this.mLanguages.Count;
      int index1 = 0;
      for (int count2 = this.mTerms.Count; index1 < count2; ++index1)
      {
        for (int index2 = languageIndex + 1; index2 < count1; ++index2)
          this.mTerms[index1].Languages[index2 - 1] = this.mTerms[index1].Languages[index2];
        Array.Resize<string>(ref this.mTerms[index1].Languages, count1 - 1);
      }
      this.mLanguages.RemoveAt(languageIndex);
    }

    public List<string> GetLanguages()
    {
      List<string> stringList = new List<string>();
      int index = 0;
      for (int count = this.mLanguages.Count; index < count; ++index)
        stringList.Add(this.mLanguages[index].Name);
      return stringList;
    }

    public string GetTermTranslation(string term)
    {
      int languageIndex = this.GetLanguageIndex(LocalizationManager.CurrentLanguage, true);
      if (languageIndex < 0)
        return string.Empty;
      TermData termData = this.GetTermData(term);
      if (termData != null)
        return termData.Languages[languageIndex];
      return string.Empty;
    }

    public bool TryGetTermTranslation(string term, out string Translation)
    {
      int languageIndex = this.GetLanguageIndex(LocalizationManager.CurrentLanguage, true);
      if (languageIndex >= 0)
      {
        TermData termData = this.GetTermData(term);
        if (termData != null)
        {
          Translation = termData.Languages[languageIndex];
          if (string.Empty == Translation)
            Translation = termData.Languages[0];
          return true;
        }
      }
      Translation = string.Empty;
      return false;
    }

    public TermData AddTerm(string term)
    {
      return this.AddTerm(term, eTermType.Text);
    }

    public TermData GetTermData(string term)
    {
      if (this.mDictionary.Count == 0)
        this.UpdateDictionary();
      TermData termData;
      if (!this.mDictionary.TryGetValue(term, out termData))
        return (TermData) null;
      return termData;
    }

    public bool ContainsTerm(string term)
    {
      return this.GetTermData(term) != null;
    }

    public List<string> GetTermsList()
    {
      if (this.mDictionary.Count != this.mTerms.Count)
        this.UpdateDictionary();
      return new List<string>((IEnumerable<string>) this.mDictionary.Keys);
    }

    public TermData AddTerm(string NewTerm, eTermType termType)
    {
      LanguageSource.ValidateFullTerm(ref NewTerm);
      NewTerm = NewTerm.Trim();
      TermData termData = this.GetTermData(NewTerm);
      if (termData == null)
      {
        termData = new TermData();
        termData.Term = NewTerm;
        termData.TermType = termType;
        termData.Languages = new string[this.mLanguages.Count];
        this.mTerms.Add(termData);
        this.mDictionary.Add(NewTerm, termData);
      }
      return termData;
    }

    public void RemoveTerm(string term)
    {
      int index = 0;
      for (int count = this.mTerms.Count; index < count; ++index)
      {
        if (this.mTerms[index].Term == term)
        {
          this.mTerms.RemoveAt(index);
          this.mDictionary.Remove(term);
          break;
        }
      }
    }

    public static void ValidateFullTerm(ref string Term)
    {
      Term = Term.Replace('\\', '/');
      Term = Term.Trim();
      string[] strArray = Term.Split('/');
      Term = strArray[strArray.Length - 1];
      if (!Term.StartsWith(LanguageSource.EmptyCategory) || Term.Length <= LanguageSource.EmptyCategory.Length || (int) Term[LanguageSource.EmptyCategory.Length] != 47)
        return;
      Term = Term.Substring(LanguageSource.EmptyCategory.Length + 1);
    }

    public bool IsEqualTo(LanguageSource Source)
    {
      if (Source.mLanguages.Count != this.mLanguages.Count)
        return false;
      int index1 = 0;
      for (int count = this.mLanguages.Count; index1 < count; ++index1)
      {
        if (Source.GetLanguageIndex(this.mLanguages[index1].Name, true) < 0)
          return false;
      }
      if (Source.mTerms.Count != this.mTerms.Count)
        return false;
      for (int index2 = 0; index2 < this.mTerms.Count; ++index2)
      {
        if (Source.GetTermData(this.mTerms[index2].Term) == null)
          return false;
      }
      return true;
    }

    internal bool ManagerHasASimilarSource()
    {
      int index = 0;
      for (int count = LocalizationManager.Sources.Count; index < count; ++index)
      {
        LanguageSource source = LocalizationManager.Sources[index];
        if ((UnityEngine.Object) source != (UnityEngine.Object) null && source.IsEqualTo(this) && (UnityEngine.Object) source != (UnityEngine.Object) this)
          return true;
      }
      return false;
    }

    public void ClearAllData()
    {
      this.mTerms.Clear();
      this.mLanguages.Clear();
      this.mDictionary.Clear();
    }

    public UnityEngine.Object FindAsset(string Name)
    {
      if (this.Assets != null)
      {
        int index = 0;
        for (int length = this.Assets.Length; index < length; ++index)
        {
          if (this.Assets[index] != (UnityEngine.Object) null && this.Assets[index].name == Name)
            return this.Assets[index];
        }
      }
      return (UnityEngine.Object) null;
    }

    public bool HasAsset(UnityEngine.Object Obj)
    {
      return Array.IndexOf<UnityEngine.Object>(this.Assets, Obj) >= 0;
    }

    public enum eGoogleUpdateFrequency
    {
      Always,
      Never,
      Daily,
      Weekly,
      Monthly,
    }
  }
}
