﻿// Decompiled with JetBrains decompiler
// Type: I2.Loc.ToggleLanguage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace I2.Loc
{
  public class ToggleLanguage : MonoBehaviour
  {
    private void Start()
    {
      this.Invoke("test", 3f);
    }

    private void test()
    {
      List<string> allLanguages = LocalizationManager.GetAllLanguages();
      int num1 = allLanguages.IndexOf(LocalizationManager.CurrentLanguage);
      int num2 = num1 >= 0 ? (num1 + 1) % allLanguages.Count : 0;
      this.Invoke(nameof (test), 3f);
    }
  }
}
