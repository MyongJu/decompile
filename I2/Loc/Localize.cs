﻿// Decompiled with JetBrains decompiler
// Type: I2.Loc.Localize
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using ArabicSupport;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace I2.Loc
{
  [AddComponentMenu("I2/Localization/Localize")]
  public class Localize : MonoBehaviour
  {
    public string mTerm = string.Empty;
    public string mTermSecondary = string.Empty;
    public EventCallback LocalizeCallBack = new EventCallback();
    private string FinalTerm;
    private string FinalSecondaryTerm;
    public Localize.TermModification PrimaryTermModifier;
    public Localize.TermModification SecondaryTermModifier;
    public UnityEngine.Object mTarget;
    public Localize.DelegateSetFinalTerms EventSetFinalTerms;
    public Localize.DelegateDoLocalize EventDoLocalize;
    public bool CanUseSecondaryTerm;
    public bool AllowMainTermToBeRTL;
    public bool AllowSecondTermToBeRTL;
    public bool IgnoreRTL;
    public UnityEngine.Object[] TranslatedObjects;
    public static string MainTranslation;
    public static string SecondaryTranslation;
    public static string CallBackTerm;
    public static string CallBackSecondaryTerm;
    private UILabel mTarget_UILabel;
    private UISprite mTarget_UISprite;
    private UITexture mTarget_UITexture;
    private Text mTarget_uGUI_Text;
    private Image mTarget_uGUI_Image;
    private RawImage mTarget_uGUI_RawImage;
    private GUIText mTarget_GUIText;
    private TextMesh mTarget_TextMesh;
    private AudioSource mTarget_AudioSource;
    private GUITexture mTarget_GUITexture;
    private GameObject mTarget_Child;

    public event System.Action EventFindTarget;

    public string Term
    {
      get
      {
        return this.mTerm;
      }
      set
      {
        this.mTerm = value;
      }
    }

    public string SecondaryTerm
    {
      get
      {
        return this.mTermSecondary;
      }
      set
      {
        this.mTermSecondary = value;
      }
    }

    private void Awake()
    {
      this.RegisterTargets();
      this.EventFindTarget();
    }

    private void RegisterTargets()
    {
      if (this.EventFindTarget != null)
        return;
      this.RegisterEvents_NGUI();
      Localize.RegisterEvents_DFGUI();
      this.RegisterEvents_UGUI();
      Localize.RegisterEvents_2DToolKit();
      Localize.RegisterEvents_TextMeshPro();
      this.RegisterEvents_UnityStandard();
    }

    private void OnEnable()
    {
      this.OnLocalize();
    }

    public void OnLocalize()
    {
      if (!this.enabled || !this.gameObject.activeInHierarchy || string.IsNullOrEmpty(LocalizationManager.CurrentLanguage))
        return;
      if (!this.HasTargetCache())
        this.FindTarget();
      if (!this.HasTargetCache())
        return;
      if (string.IsNullOrEmpty(this.FinalTerm) || string.IsNullOrEmpty(this.FinalSecondaryTerm))
        this.GetFinalTerms(out this.FinalTerm, out this.FinalSecondaryTerm);
      if (string.IsNullOrEmpty(this.FinalTerm) && string.IsNullOrEmpty(this.FinalSecondaryTerm))
        return;
      Localize.CallBackTerm = this.FinalTerm;
      Localize.CallBackSecondaryTerm = this.FinalSecondaryTerm;
      Localize.MainTranslation = LocalizationManager.GetTermTranslation(this.FinalTerm, false);
      Localize.SecondaryTranslation = LocalizationManager.GetTermTranslation(this.FinalSecondaryTerm, false);
      if (string.IsNullOrEmpty(Localize.MainTranslation) && string.IsNullOrEmpty(Localize.SecondaryTranslation))
        return;
      this.LocalizeCallBack.Execute((UnityEngine.Object) this);
      if (LocalizationManager.IsRight2Left && !this.IgnoreRTL)
      {
        if (this.AllowMainTermToBeRTL && !string.IsNullOrEmpty(Localize.MainTranslation))
          Localize.MainTranslation = ArabicFixer.Fix(Localize.MainTranslation);
        if (this.AllowSecondTermToBeRTL && !string.IsNullOrEmpty(Localize.SecondaryTranslation))
          Localize.SecondaryTranslation = ArabicFixer.Fix(Localize.SecondaryTranslation);
      }
      switch (this.PrimaryTermModifier)
      {
        case Localize.TermModification.ToUpper:
          Localize.MainTranslation = Localize.MainTranslation.ToUpper();
          break;
        case Localize.TermModification.ToLower:
          Localize.MainTranslation = Localize.MainTranslation.ToLower();
          break;
      }
      switch (this.SecondaryTermModifier)
      {
        case Localize.TermModification.ToUpper:
          Localize.SecondaryTranslation = Localize.SecondaryTranslation.ToUpper();
          break;
        case Localize.TermModification.ToLower:
          Localize.SecondaryTranslation = Localize.SecondaryTranslation.ToLower();
          break;
      }
      this.EventDoLocalize(Localize.MainTranslation, Localize.SecondaryTranslation);
    }

    public bool FindTarget()
    {
      if (this.EventFindTarget == null)
        this.RegisterTargets();
      this.EventFindTarget();
      return this.HasTargetCache();
    }

    public void FindAndCacheTarget<T>(ref T targetCache, Localize.DelegateSetFinalTerms setFinalTerms, Localize.DelegateDoLocalize doLocalize, bool UseSecondaryTerm, bool MainRTL, bool SecondRTL) where T : Component
    {
      if (this.mTarget != (UnityEngine.Object) null)
        targetCache = this.mTarget as T;
      else
        this.mTarget = (UnityEngine.Object) (targetCache = this.GetComponent<T>());
      if (!((UnityEngine.Object) targetCache != (UnityEngine.Object) null))
        return;
      this.EventSetFinalTerms = setFinalTerms;
      this.EventDoLocalize = doLocalize;
      this.CanUseSecondaryTerm = UseSecondaryTerm;
      this.AllowMainTermToBeRTL = MainRTL;
      this.AllowSecondTermToBeRTL = SecondRTL;
    }

    private void FindAndCacheTarget(ref GameObject targetCache, Localize.DelegateSetFinalTerms setFinalTerms, Localize.DelegateDoLocalize doLocalize, bool UseSecondaryTerm, bool MainRTL, bool SecondRTL)
    {
      if (this.mTarget != (UnityEngine.Object) targetCache && (bool) ((UnityEngine.Object) targetCache))
        UnityEngine.Object.Destroy((UnityEngine.Object) targetCache);
      if (this.mTarget != (UnityEngine.Object) null)
      {
        targetCache = this.mTarget as GameObject;
      }
      else
      {
        Transform transform = this.transform;
        this.mTarget = (UnityEngine.Object) (targetCache = transform.childCount >= 1 ? transform.GetChild(0).gameObject : (GameObject) null);
      }
      if (!((UnityEngine.Object) targetCache != (UnityEngine.Object) null))
        return;
      this.EventSetFinalTerms = setFinalTerms;
      this.EventDoLocalize = doLocalize;
      this.CanUseSecondaryTerm = UseSecondaryTerm;
      this.AllowMainTermToBeRTL = MainRTL;
      this.AllowSecondTermToBeRTL = SecondRTL;
    }

    private bool HasTargetCache()
    {
      return this.EventDoLocalize != null;
    }

    public void GetFinalTerms(out string PrimaryTerm, out string SecondaryTerm)
    {
      if (!(bool) this.mTarget && !this.HasTargetCache())
        this.FindTarget();
      PrimaryTerm = string.Empty;
      SecondaryTerm = string.Empty;
      if (this.mTarget != (UnityEngine.Object) null && (string.IsNullOrEmpty(this.mTerm) || string.IsNullOrEmpty(this.mTermSecondary)) && this.EventSetFinalTerms != null)
        this.EventSetFinalTerms(this.mTerm, this.mTermSecondary, out PrimaryTerm, out SecondaryTerm);
      if (!string.IsNullOrEmpty(this.mTerm))
        PrimaryTerm = this.mTerm;
      if (string.IsNullOrEmpty(this.mTermSecondary))
        return;
      SecondaryTerm = this.mTermSecondary;
    }

    private void SetFinalTerms(string Main, string Secondary, out string PrimaryTerm, out string SecondaryTerm)
    {
      PrimaryTerm = Main;
      SecondaryTerm = Secondary;
    }

    public void SetTerm(string primary, string secondary = null)
    {
      if (!string.IsNullOrEmpty(primary))
      {
        string str = primary;
        this.Term = str;
        this.FinalTerm = str;
      }
      if (!string.IsNullOrEmpty(secondary))
      {
        string str = secondary;
        this.SecondaryTerm = str;
        this.FinalSecondaryTerm = str;
      }
      this.OnLocalize();
    }

    private T GetSecondaryTranslatedObj<T>(ref string MainTranslation, ref string SecondaryTranslation) where T : UnityEngine.Object
    {
      string secondary;
      this.DeserializeTranslation(MainTranslation, out MainTranslation, out secondary);
      if (string.IsNullOrEmpty(secondary))
        secondary = SecondaryTranslation;
      if (string.IsNullOrEmpty(secondary))
        return (T) null;
      T translatedObject = this.GetTranslatedObject<T>(secondary);
      if ((UnityEngine.Object) translatedObject == (UnityEngine.Object) null)
      {
        int num = secondary.LastIndexOfAny("/\\".ToCharArray());
        if (num >= 0)
          translatedObject = this.GetTranslatedObject<T>(secondary.Substring(num + 1));
      }
      return translatedObject;
    }

    private T GetTranslatedObject<T>(string Translation) where T : UnityEngine.Object
    {
      return this.FindTranslatedObject<T>(Translation);
    }

    private void DeserializeTranslation(string translation, out string value, out string secondary)
    {
      if (!string.IsNullOrEmpty(translation) && translation.Length > 1 && (int) translation[0] == 91)
      {
        int num = translation.IndexOf(']');
        if (num > 0)
        {
          secondary = translation.Substring(1, num - 1);
          value = translation.Substring(num + 1);
          return;
        }
      }
      value = translation;
      secondary = string.Empty;
    }

    public T FindTranslatedObject<T>(string value) where T : UnityEngine.Object
    {
      if (string.IsNullOrEmpty(value))
        return (T) null;
      if (this.TranslatedObjects != null)
      {
        int index = 0;
        for (int length = this.TranslatedObjects.Length; index < length; ++index)
        {
          if ((UnityEngine.Object) (this.TranslatedObjects[index] as T) != (UnityEngine.Object) null && value == this.TranslatedObjects[index].name)
            return this.TranslatedObjects[index] as T;
        }
      }
      T asset = LocalizationManager.FindAsset(value) as T;
      if ((bool) ((UnityEngine.Object) asset))
        return asset;
      return ResourceManager.pInstance.GetAsset<T>(value);
    }

    private bool HasTranslatedObject(UnityEngine.Object Obj)
    {
      if (Array.IndexOf<UnityEngine.Object>(this.TranslatedObjects, Obj) >= 0)
        return true;
      return ResourceManager.pInstance.HasAsset(Obj);
    }

    public void SetGlobalLanguage(string Language)
    {
      LocalizationManager.CurrentLanguage = Language;
    }

    public static void RegisterEvents_2DToolKit()
    {
    }

    public static void RegisterEvents_DFGUI()
    {
    }

    public void RegisterEvents_NGUI()
    {
      this.EventFindTarget += new System.Action(this.FindTarget_UILabel);
      this.EventFindTarget += new System.Action(this.FindTarget_UISprite);
      this.EventFindTarget += new System.Action(this.FindTarget_UITexture);
    }

    private void FindTarget_UILabel()
    {
      this.FindAndCacheTarget<UILabel>(ref this.mTarget_UILabel, new Localize.DelegateSetFinalTerms(this.SetFinalTerms_UIlabel), new Localize.DelegateDoLocalize(this.DoLocalize_UILabel), true, true, false);
    }

    private void FindTarget_UISprite()
    {
      this.FindAndCacheTarget<UISprite>(ref this.mTarget_UISprite, new Localize.DelegateSetFinalTerms(this.SetFinalTerms_UISprite), new Localize.DelegateDoLocalize(this.DoLocalize_UISprite), true, false, false);
    }

    private void FindTarget_UITexture()
    {
      this.FindAndCacheTarget<UITexture>(ref this.mTarget_UITexture, new Localize.DelegateSetFinalTerms(this.SetFinalTerms_UITexture), new Localize.DelegateDoLocalize(this.DoLocalize_UITexture), false, false, false);
    }

    private void SetFinalTerms_UIlabel(string Main, string Secondary, out string primaryTerm, out string secondaryTerm)
    {
      this.SetFinalTerms(this.mTarget_UILabel.text, !(this.mTarget_UILabel.ambigiousFont != (UnityEngine.Object) null) ? string.Empty : this.mTarget_UILabel.ambigiousFont.name, out primaryTerm, out secondaryTerm);
    }

    public void SetFinalTerms_UISprite(string Main, string Secondary, out string primaryTerm, out string secondaryTerm)
    {
      this.SetFinalTerms(this.mTarget_UISprite.spriteName, !((UnityEngine.Object) this.mTarget_UISprite.atlas != (UnityEngine.Object) null) ? string.Empty : this.mTarget_UISprite.atlas.name, out primaryTerm, out secondaryTerm);
    }

    public void SetFinalTerms_UITexture(string Main, string Secondary, out string primaryTerm, out string secondaryTerm)
    {
      this.SetFinalTerms(this.mTarget_UITexture.mainTexture.name, (string) null, out primaryTerm, out secondaryTerm);
    }

    public void DoLocalize_UILabel(string MainTranslation, string SecondaryTranslation)
    {
      UIInput inParents = NGUITools.FindInParents<UIInput>(this.mTarget_UILabel.gameObject);
      if ((UnityEngine.Object) inParents != (UnityEngine.Object) null && (UnityEngine.Object) inParents.label == (UnityEngine.Object) this.mTarget_UILabel)
      {
        if (!(inParents.defaultText != MainTranslation))
          return;
        inParents.defaultText = MainTranslation;
      }
      else
      {
        if (!(this.mTarget_UILabel.text != MainTranslation))
          return;
        this.mTarget_UILabel.text = MainTranslation;
        if (!("ENTER CITY NAME" == MainTranslation))
          return;
        Debug.LogWarning((object) ("mTarget_UILabel is " + this.mTarget_UILabel.name));
      }
    }

    public void DoLocalize_UISprite(string MainTranslation, string SecondaryTranslation)
    {
      if (this.mTarget_UISprite.spriteName == MainTranslation)
        return;
      UIAtlas secondaryTranslatedObj = this.GetSecondaryTranslatedObj<UIAtlas>(ref MainTranslation, ref SecondaryTranslation);
      bool flag = false;
      if ((UnityEngine.Object) secondaryTranslatedObj != (UnityEngine.Object) null && (UnityEngine.Object) this.mTarget_UISprite.atlas != (UnityEngine.Object) secondaryTranslatedObj)
      {
        this.mTarget_UISprite.atlas = secondaryTranslatedObj;
        flag = true;
      }
      if (this.mTarget_UISprite.spriteName != MainTranslation && this.mTarget_UISprite.atlas.GetSprite(MainTranslation) != null)
      {
        this.mTarget_UISprite.spriteName = MainTranslation;
        flag = true;
      }
      if (!flag)
        return;
      this.mTarget_UISprite.MakePixelPerfect();
    }

    public void DoLocalize_UITexture(string MainTranslation, string SecondaryTranslation)
    {
      Texture mainTexture = this.mTarget_UITexture.mainTexture;
      if (!((UnityEngine.Object) mainTexture != (UnityEngine.Object) null) || !(mainTexture.name != MainTranslation))
        return;
      this.mTarget_UITexture.mainTexture = this.FindTranslatedObject<Texture>(MainTranslation);
    }

    public static void RegisterEvents_TextMeshPro()
    {
    }

    public void RegisterEvents_UGUI()
    {
      this.EventFindTarget += new System.Action(this.FindTarget_uGUI_Text);
      this.EventFindTarget += new System.Action(this.FindTarget_uGUI_Image);
      this.EventFindTarget += new System.Action(this.FindTarget_uGUI_RawImage);
    }

    private void FindTarget_uGUI_Text()
    {
      this.FindAndCacheTarget<Text>(ref this.mTarget_uGUI_Text, new Localize.DelegateSetFinalTerms(this.SetFinalTerms_uGUI_Text), new Localize.DelegateDoLocalize(this.DoLocalize_uGUI_Text), true, true, false);
    }

    private void FindTarget_uGUI_Image()
    {
      this.FindAndCacheTarget<Image>(ref this.mTarget_uGUI_Image, new Localize.DelegateSetFinalTerms(this.SetFinalTerms_uGUI_Image), new Localize.DelegateDoLocalize(this.DoLocalize_uGUI_Image), false, false, false);
    }

    private void FindTarget_uGUI_RawImage()
    {
      this.FindAndCacheTarget<RawImage>(ref this.mTarget_uGUI_RawImage, new Localize.DelegateSetFinalTerms(this.SetFinalTerms_uGUI_RawImage), new Localize.DelegateDoLocalize(this.DoLocalize_uGUI_RawImage), false, false, false);
    }

    private void SetFinalTerms_uGUI_Text(string Main, string Secondary, out string primaryTerm, out string secondaryTerm)
    {
      this.SetFinalTerms(this.mTarget_uGUI_Text.text, !((UnityEngine.Object) this.mTarget_uGUI_Text.font != (UnityEngine.Object) null) ? string.Empty : this.mTarget_uGUI_Text.font.name, out primaryTerm, out secondaryTerm);
    }

    public void SetFinalTerms_uGUI_Image(string Main, string Secondary, out string primaryTerm, out string secondaryTerm)
    {
      this.SetFinalTerms(this.mTarget_uGUI_Image.mainTexture.name, (string) null, out primaryTerm, out secondaryTerm);
    }

    public void SetFinalTerms_uGUI_RawImage(string Main, string Secondary, out string primaryTerm, out string secondaryTerm)
    {
      this.SetFinalTerms(this.mTarget_uGUI_RawImage.texture.name, (string) null, out primaryTerm, out secondaryTerm);
    }

    public static T FindInParents<T>(Transform tr) where T : Component
    {
      if (!(bool) ((UnityEngine.Object) tr))
        return (T) null;
      T component;
      for (component = tr.GetComponent<T>(); !(bool) ((UnityEngine.Object) component) && (bool) ((UnityEngine.Object) tr); tr = tr.parent)
        component = tr.GetComponent<T>();
      return component;
    }

    public void DoLocalize_uGUI_Text(string MainTranslation, string SecondaryTranslation)
    {
      if (this.mTarget_uGUI_Text.text != MainTranslation)
        this.mTarget_uGUI_Text.text = MainTranslation;
      Font secondaryTranslatedObj = this.GetSecondaryTranslatedObj<Font>(ref MainTranslation, ref SecondaryTranslation);
      if (!((UnityEngine.Object) secondaryTranslatedObj != (UnityEngine.Object) null) || !((UnityEngine.Object) secondaryTranslatedObj != (UnityEngine.Object) this.mTarget_uGUI_Text.font))
        return;
      this.mTarget_uGUI_Text.font = secondaryTranslatedObj;
    }

    public void DoLocalize_uGUI_Image(string MainTranslation, string SecondaryTranslation)
    {
      UnityEngine.Sprite sprite = this.mTarget_uGUI_Image.sprite;
      if (!((UnityEngine.Object) sprite == (UnityEngine.Object) null) && !(sprite.name != MainTranslation))
        return;
      this.mTarget_uGUI_Image.sprite = this.FindTranslatedObject<UnityEngine.Sprite>(MainTranslation);
    }

    public void DoLocalize_uGUI_RawImage(string MainTranslation, string SecondaryTranslation)
    {
      Texture texture = this.mTarget_uGUI_RawImage.texture;
      if (!((UnityEngine.Object) texture == (UnityEngine.Object) null) && !(texture.name != MainTranslation))
        return;
      this.mTarget_uGUI_RawImage.texture = this.FindTranslatedObject<Texture>(MainTranslation);
    }

    public void RegisterEvents_UnityStandard()
    {
      this.EventFindTarget += new System.Action(this.FindTarget_GUIText);
      this.EventFindTarget += new System.Action(this.FindTarget_TextMesh);
      this.EventFindTarget += new System.Action(this.FindTarget_AudioSource);
      this.EventFindTarget += new System.Action(this.FindTarget_GUITexture);
      this.EventFindTarget += new System.Action(this.FindTarget_Child);
    }

    private void FindTarget_GUIText()
    {
      this.FindAndCacheTarget<GUIText>(ref this.mTarget_GUIText, new Localize.DelegateSetFinalTerms(this.SetFinalTerms_GUIText), new Localize.DelegateDoLocalize(this.DoLocalize_GUIText), true, true, false);
    }

    private void FindTarget_TextMesh()
    {
      this.FindAndCacheTarget<TextMesh>(ref this.mTarget_TextMesh, new Localize.DelegateSetFinalTerms(this.SetFinalTerms_TextMesh), new Localize.DelegateDoLocalize(this.DoLocalize_TextMesh), true, true, false);
    }

    private void FindTarget_AudioSource()
    {
      this.FindAndCacheTarget<AudioSource>(ref this.mTarget_AudioSource, new Localize.DelegateSetFinalTerms(this.SetFinalTerms_AudioSource), new Localize.DelegateDoLocalize(this.DoLocalize_AudioSource), false, false, false);
    }

    private void FindTarget_GUITexture()
    {
      this.FindAndCacheTarget<GUITexture>(ref this.mTarget_GUITexture, new Localize.DelegateSetFinalTerms(this.SetFinalTerms_GUITexture), new Localize.DelegateDoLocalize(this.DoLocalize_GUITexture), false, false, false);
    }

    private void FindTarget_Child()
    {
      this.FindAndCacheTarget(ref this.mTarget_Child, new Localize.DelegateSetFinalTerms(this.SetFinalTerms_Child), new Localize.DelegateDoLocalize(this.DoLocalize_Child), false, false, false);
    }

    public void SetFinalTerms_GUIText(string Main, string Secondary, out string PrimaryTerm, out string SecondaryTerm)
    {
      if (string.IsNullOrEmpty(Secondary) && (UnityEngine.Object) this.mTarget_GUIText.font != (UnityEngine.Object) null)
        Secondary = this.mTarget_GUIText.font.name;
      this.SetFinalTerms(this.mTarget_GUIText.text, Secondary, out PrimaryTerm, out SecondaryTerm);
    }

    public void SetFinalTerms_TextMesh(string Main, string Secondary, out string PrimaryTerm, out string SecondaryTerm)
    {
      this.SetFinalTerms(this.mTarget_TextMesh.text, !((UnityEngine.Object) this.mTarget_TextMesh.font != (UnityEngine.Object) null) ? string.Empty : this.mTarget_TextMesh.font.name, out PrimaryTerm, out SecondaryTerm);
    }

    public void SetFinalTerms_GUITexture(string Main, string Secondary, out string PrimaryTerm, out string SecondaryTerm)
    {
      if (!(bool) ((UnityEngine.Object) this.mTarget_GUITexture) || !(bool) ((UnityEngine.Object) this.mTarget_GUITexture.texture))
        this.SetFinalTerms(string.Empty, string.Empty, out PrimaryTerm, out SecondaryTerm);
      else
        this.SetFinalTerms(this.mTarget_GUITexture.texture.name, string.Empty, out PrimaryTerm, out SecondaryTerm);
    }

    public void SetFinalTerms_AudioSource(string Main, string Secondary, out string PrimaryTerm, out string SecondaryTerm)
    {
      if (!(bool) ((UnityEngine.Object) this.mTarget_AudioSource) || !(bool) ((UnityEngine.Object) this.mTarget_AudioSource.clip))
        this.SetFinalTerms(string.Empty, string.Empty, out PrimaryTerm, out SecondaryTerm);
      else
        this.SetFinalTerms(this.mTarget_AudioSource.clip.name, string.Empty, out PrimaryTerm, out SecondaryTerm);
    }

    public void SetFinalTerms_Child(string Main, string Secondary, out string PrimaryTerm, out string SecondaryTerm)
    {
      this.SetFinalTerms(this.mTarget_Child.name, string.Empty, out PrimaryTerm, out SecondaryTerm);
    }

    private void DoLocalize_GUIText(string MainTranslation, string SecondaryTranslation)
    {
      Font secondaryTranslatedObj = this.GetSecondaryTranslatedObj<Font>(ref MainTranslation, ref SecondaryTranslation);
      if ((UnityEngine.Object) secondaryTranslatedObj != (UnityEngine.Object) null && (UnityEngine.Object) this.mTarget_GUIText.font != (UnityEngine.Object) secondaryTranslatedObj)
        this.mTarget_GUIText.font = secondaryTranslatedObj;
      if (!(this.mTarget_GUIText.text != MainTranslation))
        return;
      this.mTarget_GUIText.text = MainTranslation;
    }

    private void DoLocalize_TextMesh(string MainTranslation, string SecondaryTranslation)
    {
      Font secondaryTranslatedObj = this.GetSecondaryTranslatedObj<Font>(ref MainTranslation, ref SecondaryTranslation);
      if ((UnityEngine.Object) secondaryTranslatedObj != (UnityEngine.Object) null && (UnityEngine.Object) this.mTarget_TextMesh.font != (UnityEngine.Object) secondaryTranslatedObj)
      {
        this.mTarget_TextMesh.font = secondaryTranslatedObj;
        this.GetComponent<Renderer>().sharedMaterial = secondaryTranslatedObj.material;
      }
      if (!(this.mTarget_TextMesh.text != MainTranslation))
        return;
      this.mTarget_TextMesh.text = MainTranslation;
    }

    private void DoLocalize_AudioSource(string MainTranslation, string SecondaryTranslation)
    {
      bool isPlaying = this.mTarget_AudioSource.isPlaying;
      AudioClip clip = this.mTarget_AudioSource.clip;
      AudioClip translatedObject = this.FindTranslatedObject<AudioClip>(MainTranslation);
      if ((UnityEngine.Object) clip != (UnityEngine.Object) translatedObject)
        this.mTarget_AudioSource.clip = translatedObject;
      if (!isPlaying || !(bool) ((UnityEngine.Object) this.mTarget_AudioSource.clip))
        return;
      this.mTarget_AudioSource.Play();
    }

    private void DoLocalize_GUITexture(string MainTranslation, string SecondaryTranslation)
    {
      Texture texture = this.mTarget_GUITexture.texture;
      if (!((UnityEngine.Object) texture != (UnityEngine.Object) null) || !(texture.name != MainTranslation))
        return;
      this.mTarget_GUITexture.texture = this.FindTranslatedObject<Texture>(MainTranslation);
    }

    private void DoLocalize_Child(string MainTranslation, string SecondaryTranslation)
    {
      if ((bool) ((UnityEngine.Object) this.mTarget_Child) && this.mTarget_Child.name == MainTranslation)
        return;
      GameObject mTargetChild = this.mTarget_Child;
      GameObject translatedObject = this.FindTranslatedObject<GameObject>(MainTranslation);
      if ((bool) ((UnityEngine.Object) translatedObject))
      {
        this.mTarget_Child = UnityEngine.Object.Instantiate<GameObject>(translatedObject);
        Transform transform1 = this.mTarget_Child.transform;
        Transform transform2 = !(bool) ((UnityEngine.Object) mTargetChild) ? translatedObject.transform : mTargetChild.transform;
        transform1.parent = this.transform;
        transform1.localScale = transform2.localScale;
        transform1.localRotation = transform2.localRotation;
        transform1.localPosition = transform2.localPosition;
      }
      if (!(bool) ((UnityEngine.Object) mTargetChild))
        return;
      UnityEngine.Object.Destroy((UnityEngine.Object) mTargetChild);
    }

    public enum TermModification
    {
      DontModify,
      ToUpper,
      ToLower,
    }

    public delegate void DelegateSetFinalTerms(string Main, string Secondary, out string primaryTerm, out string secondaryTerm);

    public delegate void DelegateDoLocalize(string primaryTerm, string secondaryTerm);
  }
}
