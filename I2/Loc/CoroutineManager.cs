﻿// Decompiled with JetBrains decompiler
// Type: I2.Loc.CoroutineManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace I2.Loc
{
  public class CoroutineManager : MonoBehaviour
  {
    private static CoroutineManager mInstance;

    public static CoroutineManager pInstance
    {
      get
      {
        if ((Object) CoroutineManager.mInstance == (Object) null)
        {
          GameObject gameObject = new GameObject("GoogleTranslation");
          gameObject.hideFlags |= HideFlags.HideAndDontSave;
          CoroutineManager.mInstance = gameObject.AddComponent<CoroutineManager>();
        }
        return CoroutineManager.mInstance;
      }
    }
  }
}
