﻿// Decompiled with JetBrains decompiler
// Type: I2.Loc.LanguagePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace I2.Loc
{
  public class LanguagePopup : MonoBehaviour
  {
    public LanguageSource Source;

    private void Start()
    {
      UIPopupList component = this.GetComponent<UIPopupList>();
      component.items = this.Source.GetLanguages();
      EventDelegate.Add(component.onChange, new EventDelegate.Callback(this.OnValueChange));
      component.value = component.items[0];
    }

    public void OnValueChange()
    {
      LocalizationManager.CurrentLanguage = UIPopupList.current.value;
    }
  }
}
