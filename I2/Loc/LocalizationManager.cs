﻿// Decompiled with JetBrains decompiler
// Type: I2.Loc.LocalizationManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using ArabicSupport;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace I2.Loc
{
  public static class LocalizationManager
  {
    public static bool IsRight2Left = false;
    public static List<LanguageSource> Sources = new List<LanguageSource>();
    public static string[] GlobalSources = new string[1]
    {
      "I2Loading"
    };
    private static string[] LanguagesRTL = new string[20]
    {
      "ar-DZ",
      "ar",
      "ar-BH",
      "ar-EG",
      "ar-IQ",
      "ar-JO",
      "ar-KW",
      "ar-LB",
      "ar-LY",
      "ar-MA",
      "ar-OM",
      "ar-QA",
      "ar-SA",
      "ar-SY",
      "ar-TN",
      "ar-AE",
      "ar-YE",
      "he",
      "ur",
      "ji"
    };
    private static string mCurrentLanguage;
    private static string mLanguageCode;

    public static event LocalizationManager.OnLocalizeCallback OnLocalizeEvent;

    public static string CurrentLanguage
    {
      get
      {
        LocalizationManager.InitializeIfNeeded();
        return LocalizationManager.mCurrentLanguage;
      }
      set
      {
        string supportedLanguage = LocalizationManager.GetSupportedLanguage(value);
        if (string.IsNullOrEmpty(supportedLanguage) || !(LocalizationManager.mCurrentLanguage != supportedLanguage))
          return;
        LocalizationManager.SetLanguageAndCode(supportedLanguage, LocalizationManager.GetLanguageCode(supportedLanguage), true);
      }
    }

    public static string CurrentLanguageCode
    {
      get
      {
        LocalizationManager.InitializeIfNeeded();
        return LocalizationManager.mLanguageCode;
      }
      set
      {
        if (!(LocalizationManager.mLanguageCode != value))
          return;
        string languageFromCode = LocalizationManager.GetLanguageFromCode(value);
        if (string.IsNullOrEmpty(languageFromCode))
          return;
        LocalizationManager.SetLanguageAndCode(languageFromCode, value, true);
      }
    }

    private static void InitializeIfNeeded()
    {
      if (!string.IsNullOrEmpty(LocalizationManager.mCurrentLanguage))
        return;
      LocalizationManager.UpdateSources();
      LocalizationManager.SelectStartupLanguage();
    }

    private static void SetLanguageAndCode(string LanguageName, string LanguageCode, bool RememberLanguage = true)
    {
      if (!(LocalizationManager.mCurrentLanguage != LanguageName) && !(LocalizationManager.mLanguageCode != LanguageCode))
        return;
      if (RememberLanguage)
        PlayerPrefs.SetString("I2 Language", LanguageName);
      LocalizationManager.mCurrentLanguage = LanguageName;
      LocalizationManager.mLanguageCode = LanguageCode;
      LocalizationManager.IsRight2Left = LocalizationManager.IsRTL(LocalizationManager.mLanguageCode);
      LocalizationManager.LocalizeAll();
    }

    private static void SelectStartupLanguage()
    {
      string Language1 = PlayerPrefs.GetString("I2 Language", string.Empty);
      string Language2 = ((Enum) Application.systemLanguage).ToString();
      if (LocalizationManager.HasLanguage(Language1, true, false))
      {
        LocalizationManager.CurrentLanguage = Language1;
      }
      else
      {
        string supportedLanguage = LocalizationManager.GetSupportedLanguage(Language2);
        if (!string.IsNullOrEmpty(supportedLanguage))
        {
          LocalizationManager.SetLanguageAndCode(supportedLanguage, LocalizationManager.GetLanguageCode(supportedLanguage), false);
        }
        else
        {
          int index = 0;
          for (int count = LocalizationManager.Sources.Count; index < count; ++index)
          {
            if (LocalizationManager.Sources[index].mLanguages.Count > 0)
            {
              LocalizationManager.SetLanguageAndCode(LocalizationManager.Sources[index].mLanguages[0].Name, LocalizationManager.Sources[index].mLanguages[0].Code, false);
              break;
            }
          }
        }
      }
    }

    public static string GetTermTranslation(string Term, bool FixForRTL = false)
    {
      if (string.IsNullOrEmpty(Term))
        return Term;
      string Translation;
      if (LocalizationManager.TryGetTermTranslation(Term, out Translation, FixForRTL))
        return Translation;
      if (LocalizationManager.IsRight2Left && FixForRTL)
        return ArabicFixer.Fix(Term, false, false);
      return Term;
    }

    public static bool TryGetTermTranslation(string Term, out string Translation, bool FixForRTL = false)
    {
      Translation = string.Empty;
      if (string.IsNullOrEmpty(Term))
        return false;
      LocalizationManager.InitializeIfNeeded();
      int index = 0;
      for (int count = LocalizationManager.Sources.Count; index < count; ++index)
      {
        if (LocalizationManager.Sources[index].TryGetTermTranslation(Term, out Translation))
        {
          if (LocalizationManager.IsRight2Left && FixForRTL)
          {
            Translation = ArabicFixer.Fix(Translation, false, false);
            Translation = Translation.Replace("}.{NO", "{NO.}");
          }
          return true;
        }
      }
      return false;
    }

    internal static void LocalizeAll()
    {
      Localize[] objectsOfTypeAll = (Localize[]) Resources.FindObjectsOfTypeAll(typeof (Localize));
      int index = 0;
      for (int length = objectsOfTypeAll.Length; index < length; ++index)
        objectsOfTypeAll[index].OnLocalize();
      if (LocalizationManager.OnLocalizeEvent != null)
        LocalizationManager.OnLocalizeEvent();
      ResourceManager.pInstance.CleanResourceCache();
    }

    public static bool UpdateSources()
    {
      LocalizationManager.UnregisterDeletededSources();
      LocalizationManager.RegisterSourceInResources();
      return LocalizationManager.Sources.Count > 0;
    }

    private static void UnregisterDeletededSources()
    {
      for (int index = LocalizationManager.Sources.Count - 1; index >= 0; --index)
      {
        if ((UnityEngine.Object) LocalizationManager.Sources[index] == (UnityEngine.Object) null)
          LocalizationManager.RemoveSource(LocalizationManager.Sources[index]);
      }
    }

    private static void RegisterSceneSources()
    {
      LanguageSource[] objectsOfTypeAll = (LanguageSource[]) Resources.FindObjectsOfTypeAll(typeof (LanguageSource));
      int index = 0;
      for (int length = objectsOfTypeAll.Length; index < length; ++index)
      {
        if (!LocalizationManager.Sources.Contains(objectsOfTypeAll[index]))
          LocalizationManager.AddSource(objectsOfTypeAll[index]);
      }
    }

    private static void RegisterSourceInResources()
    {
      foreach (string globalSource in LocalizationManager.GlobalSources)
      {
        GameObject asset = ResourceManager.pInstance.GetAsset<GameObject>(globalSource);
        LanguageSource Source = !(bool) ((UnityEngine.Object) asset) ? (LanguageSource) null : asset.GetComponent<LanguageSource>();
        if ((bool) ((UnityEngine.Object) Source) && !LocalizationManager.Sources.Contains(Source))
          LocalizationManager.AddSource(Source);
      }
    }

    internal static void AddSource(LanguageSource Source)
    {
      if (LocalizationManager.Sources.Contains(Source))
        return;
      LocalizationManager.Sources.Add(Source);
      Source.UpdateDictionary();
      Source.Import_Google(false);
    }

    internal static void RemoveSource(LanguageSource Source)
    {
      LocalizationManager.Sources.Remove(Source);
    }

    public static bool IsGlobalSource(string SourceName)
    {
      return Array.IndexOf<string>(LocalizationManager.GlobalSources, SourceName) >= 0;
    }

    public static bool HasLanguage(string Language, bool AllowDiscartingRegion = true, bool Initialize = true)
    {
      if (Initialize)
        LocalizationManager.InitializeIfNeeded();
      int index1 = 0;
      for (int count = LocalizationManager.Sources.Count; index1 < count; ++index1)
      {
        if (LocalizationManager.Sources[index1].GetLanguageIndex(Language, false) >= 0)
          return true;
      }
      if (AllowDiscartingRegion)
      {
        int index2 = 0;
        for (int count = LocalizationManager.Sources.Count; index2 < count; ++index2)
        {
          if (LocalizationManager.Sources[index2].GetLanguageIndex(Language, true) >= 0)
            return true;
        }
      }
      return false;
    }

    public static string GetSupportedLanguage(string Language)
    {
      int index1 = 0;
      for (int count = LocalizationManager.Sources.Count; index1 < count; ++index1)
      {
        int languageIndex = LocalizationManager.Sources[index1].GetLanguageIndex(Language, false);
        if (languageIndex >= 0)
          return LocalizationManager.Sources[index1].mLanguages[languageIndex].Name;
      }
      int index2 = 0;
      for (int count = LocalizationManager.Sources.Count; index2 < count; ++index2)
      {
        int languageIndex = LocalizationManager.Sources[index2].GetLanguageIndex(Language, true);
        if (languageIndex >= 0)
          return LocalizationManager.Sources[index2].mLanguages[languageIndex].Name;
      }
      return string.Empty;
    }

    public static string GetLanguageCode(string Language)
    {
      int index = 0;
      for (int count = LocalizationManager.Sources.Count; index < count; ++index)
      {
        int languageIndex = LocalizationManager.Sources[index].GetLanguageIndex(Language, true);
        if (languageIndex >= 0)
          return LocalizationManager.Sources[index].mLanguages[languageIndex].Code;
      }
      return string.Empty;
    }

    public static string GetLanguageFromCode(string Code)
    {
      int index = 0;
      for (int count = LocalizationManager.Sources.Count; index < count; ++index)
      {
        int languageIndexFromCode = LocalizationManager.Sources[index].GetLanguageIndexFromCode(Code);
        if (languageIndexFromCode >= 0)
          return LocalizationManager.Sources[index].mLanguages[languageIndexFromCode].Name;
      }
      return string.Empty;
    }

    public static List<string> GetAllLanguages()
    {
      List<string> stringList = new List<string>();
      int index1 = 0;
      for (int count1 = LocalizationManager.Sources.Count; index1 < count1; ++index1)
      {
        int index2 = 0;
        for (int count2 = LocalizationManager.Sources[index1].mLanguages.Count; index2 < count2; ++index2)
        {
          if (!stringList.Contains(LocalizationManager.Sources[index1].mLanguages[index2].Name))
            stringList.Add(LocalizationManager.Sources[index1].mLanguages[index2].Name);
        }
      }
      return stringList;
    }

    public static List<string> GetCategories()
    {
      List<string> Categories = new List<string>();
      int index = 0;
      for (int count = LocalizationManager.Sources.Count; index < count; ++index)
        LocalizationManager.Sources[index].GetCategories(false, Categories);
      return Categories;
    }

    public static List<string> GetTermsList()
    {
      if (LocalizationManager.Sources.Count == 0)
        LocalizationManager.UpdateSources();
      List<string> stringList = new List<string>();
      int index1 = 0;
      for (int count1 = LocalizationManager.Sources.Count; index1 < count1; ++index1)
      {
        List<string> termsList = LocalizationManager.Sources[index1].GetTermsList();
        int index2 = 0;
        for (int count2 = termsList.Count; index2 < count2; ++index2)
        {
          if (!stringList.Contains(termsList[index2]))
            stringList.Add(termsList[index2]);
        }
      }
      return stringList;
    }

    public static TermData GetTermData(string term)
    {
      int index = 0;
      for (int count = LocalizationManager.Sources.Count; index < count; ++index)
      {
        TermData termData = LocalizationManager.Sources[index].GetTermData(term);
        if (termData != null)
          return termData;
      }
      return (TermData) null;
    }

    public static LanguageSource GetSourceContaining(string term, bool fallbackToFirst = true)
    {
      int index = 0;
      for (int count = LocalizationManager.Sources.Count; index < count; ++index)
      {
        if (LocalizationManager.Sources[index].GetTermData(term) != null)
          return LocalizationManager.Sources[index];
      }
      if (fallbackToFirst && LocalizationManager.Sources.Count > 0)
        return LocalizationManager.Sources[0];
      return (LanguageSource) null;
    }

    public static UnityEngine.Object FindAsset(string value)
    {
      int index = 0;
      for (int count = LocalizationManager.Sources.Count; index < count; ++index)
      {
        UnityEngine.Object asset = LocalizationManager.Sources[index].FindAsset(value);
        if ((bool) asset)
          return asset;
      }
      return (UnityEngine.Object) null;
    }

    public static string GetVersion()
    {
      return "2.4.5 f3";
    }

    public static string GetRequiredWebServiceVersion()
    {
      return "2";
    }

    private static bool IsRTL(string Code)
    {
      return Array.IndexOf<string>(LocalizationManager.LanguagesRTL, Code) >= 0;
    }

    public delegate void OnLocalizeCallback();
  }
}
