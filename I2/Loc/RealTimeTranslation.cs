﻿// Decompiled with JetBrains decompiler
// Type: I2.Loc.RealTimeTranslation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace I2.Loc
{
  public class RealTimeTranslation : MonoBehaviour
  {
    private string OriginalText = "This is an example showing how to use the google translator to translate chat messages within the game.\nIt also supports multiline translations.";
    private string TranslatedText = string.Empty;
    private bool IsTranslating;

    private void StartTranslating(string fromCode, string toCode)
    {
      this.IsTranslating = true;
      GoogleTranslation.Translate(this.OriginalText, fromCode, toCode, new Action<string>(this.OnTranslationReady));
    }

    private void OnTranslationReady(string Translation)
    {
      this.TranslatedText = Translation;
      this.IsTranslating = false;
    }
  }
}
