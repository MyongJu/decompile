﻿// Decompiled with JetBrains decompiler
// Type: I2.Loc.ScriptLocalization
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace I2.Loc
{
  public static class ScriptLocalization
  {
    public static string Get(string Term, bool FixForRTL = true)
    {
      return LocalizationManager.GetTermTranslation(Term, FixForRTL);
    }

    public static string GetWithPara(string Term, Dictionary<string, string> para, bool FixForRTL = true)
    {
      string str = LocalizationManager.GetTermTranslation(Term, FixForRTL);
      Dictionary<string, string>.Enumerator enumerator = para.GetEnumerator();
      while (enumerator.MoveNext())
      {
        string oldValue = "{" + enumerator.Current.Key + "}";
        str = str.Replace(oldValue, enumerator.Current.Value);
      }
      return str;
    }

    public static string GetWithMultyContents(string Term, bool FixForRTL = true, params string[] param)
    {
      string str = LocalizationManager.GetTermTranslation(Term, FixForRTL);
      for (int index = 0; index < param.Length; ++index)
      {
        string oldValue = "{" + (object) index + "}";
        str = str.Replace(oldValue, param[index]);
      }
      return str;
    }
  }
}
