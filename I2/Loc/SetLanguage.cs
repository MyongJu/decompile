﻿// Decompiled with JetBrains decompiler
// Type: I2.Loc.SetLanguage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace I2.Loc
{
  [AddComponentMenu("I2/Localization/SetLanguage")]
  public class SetLanguage : MonoBehaviour
  {
    public string _Language;

    private void OnClick()
    {
      this.ApplyLanguage();
    }

    public void ApplyLanguage()
    {
      if (!LocalizationManager.HasLanguage(this._Language, true, true))
        return;
      LocalizationManager.CurrentLanguage = this._Language;
    }
  }
}
