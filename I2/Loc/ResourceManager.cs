﻿// Decompiled with JetBrains decompiler
// Type: I2.Loc.ResourceManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace I2.Loc
{
  public class ResourceManager : MonoBehaviour
  {
    private Dictionary<string, UnityEngine.Object> mResourcesCache = new Dictionary<string, UnityEngine.Object>();
    private static ResourceManager mInstance;
    public UnityEngine.Object[] Assets;
    private bool mCleaningScheduled;

    public static ResourceManager pInstance
    {
      get
      {
        if ((UnityEngine.Object) ResourceManager.mInstance == (UnityEngine.Object) null)
          ResourceManager.mInstance = (ResourceManager) UnityEngine.Object.FindObjectOfType(typeof (ResourceManager));
        if ((UnityEngine.Object) ResourceManager.mInstance == (UnityEngine.Object) null)
        {
          GameObject gameObject = new GameObject("I2ResourceManager", new System.Type[1]
          {
            typeof (ResourceManager)
          });
          gameObject.hideFlags |= HideFlags.HideAndDontSave;
          ResourceManager.mInstance = gameObject.GetComponent<ResourceManager>();
        }
        UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) ResourceManager.mInstance.gameObject);
        return ResourceManager.mInstance;
      }
    }

    public void OnLevelWasLoaded()
    {
      LocalizationManager.UpdateSources();
    }

    public T GetAsset<T>(string Name) where T : UnityEngine.Object
    {
      T asset = this.FindAsset(Name) as T;
      if ((UnityEngine.Object) asset != (UnityEngine.Object) null)
        return asset;
      return this.LoadFromResources<T>(Name);
    }

    private UnityEngine.Object FindAsset(string Name)
    {
      if (this.Assets != null)
      {
        int index = 0;
        for (int length = this.Assets.Length; index < length; ++index)
        {
          if (this.Assets[index] != (UnityEngine.Object) null && this.Assets[index].name == Name)
            return this.Assets[index];
        }
      }
      return (UnityEngine.Object) null;
    }

    public bool HasAsset(UnityEngine.Object Obj)
    {
      if (this.Assets == null)
        return false;
      return Array.IndexOf<UnityEngine.Object>(this.Assets, Obj) >= 0;
    }

    public T LoadFromResources<T>(string Path) where T : UnityEngine.Object
    {
      UnityEngine.Object object1;
      if (this.mResourcesCache.TryGetValue(Path, out object1) && object1 != (UnityEngine.Object) null)
        return object1 as T;
      T obj = (T) null;
      if (Path.EndsWith("]"))
      {
        int length1 = Path.LastIndexOf("[");
        int length2 = Path.Length - length1 - 2;
        string str = Path.Substring(length1 + 1, length2);
        Path = Path.Substring(0, length1);
        T[] objArray = Resources.LoadAll<T>(Path);
        int index = 0;
        for (int length3 = objArray.Length; index < length3; ++index)
        {
          if (objArray[index].name.Equals(str))
          {
            obj = objArray[index];
            break;
          }
        }
      }
      else if (Application.isEditor)
      {
        obj = Resources.Load<T>(Path);
      }
      else
      {
        UnityEngine.Object object2 = AssetManager.Instance.HandyLoad(Path, (System.Type) null);
        obj = !((UnityEngine.Object) null != object2) ? (T) null : object2 as T;
      }
      this.mResourcesCache[Path] = (UnityEngine.Object) obj;
      if (!this.mCleaningScheduled)
      {
        this.Invoke("CleanResourceCache", 0.1f);
        this.mCleaningScheduled = true;
      }
      return obj;
    }

    public void CleanResourceCache()
    {
      this.mResourcesCache.Clear();
      Resources.UnloadUnusedAssets();
      this.CancelInvoke();
      this.mCleaningScheduled = false;
    }
  }
}
