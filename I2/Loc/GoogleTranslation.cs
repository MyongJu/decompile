﻿// Decompiled with JetBrains decompiler
// Type: I2.Loc.GoogleTranslation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Text.RegularExpressions;
using UnityEngine;

namespace I2.Loc
{
  public static class GoogleTranslation
  {
    public static void Translate(string text, string LanguageCodeFrom, string LanguageCodeTo, Action<string> OnTranslationReady)
    {
      CoroutineManager.pInstance.StartCoroutine(GoogleTranslation.WaitForTranslation(GoogleTranslation.GetTranslationWWW(text, LanguageCodeFrom, LanguageCodeTo), OnTranslationReady, text.ToUpper() == text));
    }

    [DebuggerHidden]
    private static IEnumerator WaitForTranslation(WWW www, Action<string> OnTranslationReady, bool MakeAllCaps)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new GoogleTranslation.\u003CWaitForTranslation\u003Ec__Iterator14()
      {
        www = www,
        OnTranslationReady = OnTranslationReady,
        MakeAllCaps = MakeAllCaps,
        \u003C\u0024\u003Ewww = www,
        \u003C\u0024\u003EOnTranslationReady = OnTranslationReady,
        \u003C\u0024\u003EMakeAllCaps = MakeAllCaps
      };
    }

    public static string ForceTranslate(string text, string LanguageCodeFrom, string LanguageCodeTo)
    {
      WWW translationWww = GoogleTranslation.GetTranslationWWW(text, LanguageCodeFrom, LanguageCodeTo);
      do
        ;
      while (!translationWww.isDone);
      if (string.IsNullOrEmpty(translationWww.error))
        return GoogleTranslation.ParseTranslationResult(translationWww.text, text.ToUpper() == text);
      Debug.LogError((object) translationWww.error);
      return string.Empty;
    }

    private static WWW GetTranslationWWW(string text, string LanguageCodeFrom, string LanguageCodeTo)
    {
      LanguageCodeFrom = GoogleLanguages.GetGoogleLanguageCode(LanguageCodeFrom);
      LanguageCodeTo = GoogleLanguages.GetGoogleLanguageCode(LanguageCodeTo);
      return new WWW(string.Format("http://www.google.com/translate_t?hl=en&vi=c&ie=UTF8&oe=UTF8&submit=Translate&langpair={0}|{1}&text={2}", (object) LanguageCodeFrom, (object) LanguageCodeTo, (object) Uri.EscapeUriString(text)));
    }

    private static string ParseTranslationResult(string html, bool MakeAllCaps)
    {
      try
      {
        int startIndex = html.IndexOf("TRANSLATED_TEXT") + "TRANSLATED_TEXT='".Length;
        int num = html.IndexOf("';INPUT_TOOL_PATH", startIndex);
        string str = Regex.Replace(Regex.Replace(html.Substring(startIndex, num - startIndex), "\\\\x([a-fA-F0-9]{2})", (MatchEvaluator) (match => char.ConvertFromUtf32(int.Parse(match.Groups[1].Value, NumberStyles.HexNumber)))), "&#(\\d+);", (MatchEvaluator) (match => char.ConvertFromUtf32(int.Parse(match.Groups[1].Value)))).Replace("<br>", "\n");
        if (MakeAllCaps)
          str = str.ToUpper();
        return str;
      }
      catch (Exception ex)
      {
        Debug.LogError((object) ex.Message);
        return string.Empty;
      }
    }
  }
}
