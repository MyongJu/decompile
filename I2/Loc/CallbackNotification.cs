﻿// Decompiled with JetBrains decompiler
// Type: I2.Loc.CallbackNotification
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace I2.Loc
{
  public class CallbackNotification : MonoBehaviour
  {
    public void OnModifyLocalization()
    {
      if (string.IsNullOrEmpty(Localize.MainTranslation))
        return;
      string termTranslation = LocalizationManager.GetTermTranslation("Color/Red", false);
      Localize.MainTranslation = Localize.MainTranslation.Replace("{PLAYER_COLOR}", termTranslation);
    }
  }
}
