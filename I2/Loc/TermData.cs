﻿// Decompiled with JetBrains decompiler
// Type: I2.Loc.TermData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

namespace I2.Loc
{
  [Serializable]
  public class TermData
  {
    public string Term = string.Empty;
    public string Description = string.Empty;
    public string[] Languages = new string[0];
    public eTermType TermType;
  }
}
