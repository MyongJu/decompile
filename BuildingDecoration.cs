﻿// Decompiled with JetBrains decompiler
// Type: BuildingDecoration
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BuildingDecoration : MonoBehaviour
{
  private string _decoration = string.Empty;
  private float scale = 1f;
  private const string ASSET_PATH = "Prefab/Decoration/";
  private GameObject _decorationObject;
  private string _currentDecoration;
  private Renderer[] _renderers;
  private int _level;
  private bool flag;
  private bool _isDestroy;

  public void ClearFlag()
  {
    this.flag = false;
  }

  public void PreView(string decoration)
  {
    this.flag = true;
    this.scale = this.scale;
    if (decoration != this._decoration)
    {
      this.flag = true;
      this._decoration = decoration;
    }
    if (!string.IsNullOrEmpty(decoration) && this.flag)
    {
      decoration = string.Format("{0}_{1}", (object) decoration, (object) "city");
      if (this._currentDecoration != decoration)
      {
        this._currentDecoration = decoration;
        this.DestroyDecorationObject();
        AssetManager.Instance.LoadAsync("Prefab/Decoration/" + decoration, new System.Action<UnityEngine.Object, bool>(this.OnResultHandler), (System.Type) null);
      }
    }
    if (!string.IsNullOrEmpty(this._decoration))
      return;
    this.OnFinalize();
  }

  public void UpdateUI(string decoration, float scale = 1f)
  {
    if (this.flag)
      return;
    this.scale = scale;
    if (decoration != this._decoration)
    {
      this.flag = true;
      this._decoration = decoration;
    }
    if (!string.IsNullOrEmpty(decoration) && this.flag)
    {
      decoration = string.Format("{0}_{1}", (object) decoration, (object) "city");
      if (this._currentDecoration != decoration)
      {
        this._currentDecoration = decoration;
        this.DestroyDecorationObject();
        AssetManager.Instance.LoadAsync("Prefab/Decoration/" + decoration, new System.Action<UnityEngine.Object, bool>(this.OnResultHandler), (System.Type) null);
      }
    }
    if (!string.IsNullOrEmpty(this._decoration))
      return;
    this.OnFinalize();
  }

  private void OnResultHandler(object go, bool success)
  {
    if (!success || this._isDestroy)
      return;
    this._decorationObject = Utils.DuplicateGOB(go as GameObject, this.transform);
    Vector3 localScale = this._decorationObject.transform.localScale;
    localScale.x = localScale.y = this.scale;
    this._decorationObject.transform.localScale = localScale;
    AssetManager.Instance.UnLoadAsset("Prefab/Decoration/" + this._currentDecoration, (System.Type) null, new System.Action<UnityEngine.Object, bool>(this.OnResultHandler));
    this._renderers = this._decorationObject.GetComponentsInChildren<Renderer>(true);
  }

  public void SetSortingLayerID(int sortingLayerID)
  {
    if (this._renderers == null)
      return;
    for (int index = 0; index < this._renderers.Length; ++index)
      this._renderers[index].sortingLayerID = sortingLayerID;
  }

  private void OnDestroy()
  {
    this._isDestroy = true;
  }

  public void Clear()
  {
    this.OnFinalize();
  }

  private void OnFinalize()
  {
    this.DestroyDecorationObject();
    this._currentDecoration = (string) null;
    this._renderers = (Renderer[]) null;
  }

  private void DestroyDecorationObject()
  {
    if ((bool) ((UnityEngine.Object) this._decorationObject))
    {
      PrefabManagerEx.Instance.Destroy(this._decorationObject);
      AssetManager.Instance.UnLoadAsset("Prefab/Decoration/" + this._currentDecoration, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    }
    this._decorationObject = (GameObject) null;
  }
}
