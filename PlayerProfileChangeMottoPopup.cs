﻿// Decompiled with JetBrains decompiler
// Type: PlayerProfileChangeMottoPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Globalization;
using System.Text;
using UI;

public class PlayerProfileChangeMottoPopup : Popup
{
  public UILabel title;
  public UILabel tip1;
  public UILabel tip2;
  public UILabel mottonLabel;
  public UIInput inputLabel;
  public UIButton confirmBtn;
  private System.Action closeCallBack;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.closeCallBack = (orgParam as PlayerProfileChangeMottoPopup.Parameter).closeCallBack;
    this.InitUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    if (this.closeCallBack == null)
      return;
    this.closeCallBack();
  }

  public void InitUI()
  {
    this.closeCallBack = this.closeCallBack;
    UIInput inputLabel = this.inputLabel;
    string signature = DBManager.inst.DB_User.Get(PlayerData.inst.uid).signature;
    this.mottonLabel.text = signature;
    string str = signature;
    inputLabel.value = str;
    this.inputLabel.onValidate = new UIInput.OnValidate(this.InputValidator);
  }

  public void OnConfirmBtnClick()
  {
    MessageHub.inst.GetPortByAction("player:updateSignature").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "signature",
        (object) IllegalWordsUtils.Filter(this.mottonLabel.text)
      }
    }, new System.Action<bool, object>(this.UpdageSignatureCallback), true);
  }

  public void OnCloseBtnClick()
  {
    if (this.closeCallBack != null)
      this.closeCallBack();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdageSignatureCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.OnCloseBtnClick();
  }

  private char InputValidator(string text, int charIndex, char addedChar)
  {
    bool flag = Encoding.UTF8.GetBytes(this.inputLabel.value + addedChar.ToString()).Length <= 25;
    if (this.isValidateCharacter(addedChar) && flag)
      return addedChar;
    return char.MinValue;
  }

  private bool isValidateCharacter(char codePoint)
  {
    bool flag = codePoint.ToString() == "\n";
    if (char.GetUnicodeCategory(codePoint) != UnicodeCategory.Surrogate)
      return !flag;
    return false;
  }

  public class Parameter : Popup.PopupParameter
  {
    public System.Action closeCallBack;
  }
}
