﻿// Decompiled with JetBrains decompiler
// Type: SamsungIapAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using Funplus.Abstract;
using HSMiniJSON;
using Pathfinding.Serialization.JsonFx;
using SamsungIap.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UI;
using UnityEngine;

public class SamsungIapAndroid : BasePaymentWrapper
{
  private static readonly object locker = new object();
  private Dictionary<string, string> _localSavedData = new Dictionary<string, string>();
  private Dictionary<string, string> _unconfirmedPaymentRecord = new Dictionary<string, string>();
  private Dictionary<string, ItemVo> _allItemVo = new Dictionary<string, ItemVo>();
  private Dictionary<string, string> productId2ThroughCargoDict = new Dictionary<string, string>();
  private const string FUNC_PAYMENT_INITIALIZE_SUCCESS = "OnPaymentInitializeSuccess";
  private const string FUNC_PAYMENT_INITIALIZE_ERROR = "OnPaymentInitializeError";
  private const string FUNC_NAME_PURCHASE_ERROR = "OnPaymentPurchaseError";
  private const string FUNC_NAME_PAYMENT_PURCHASE_SUCCESS = "OnPaymentPurchaseSuccess";
  private const string ERROR_MESSAGE = "{errorCode:9999, errorMsg:\"\", errorLocalizedMsg:\"\"}";
  private const int IAP_MODE_TEST_SUCCESS = 1;
  private const int IAP_MODE_TEST_FAIL = -1;
  private const int IAP_MODE_COMMERCIAL = 0;
  private const int CONSULT_START_NUM = 1;
  private const int CONSULT_END_NUM = 10;
  private string _lastPurchasedDate;
  private SamsungIapAndroidMonoHelper _monoHelper;
  private static SamsungIapAndroid _instance;
  private string _targetGameObjectName;
  private GameObject _targetGameObject;

  public SamsungIapAndroid()
  {
    GameObject gameObject = new GameObject("SamsungIapAndroidMonoHelper");
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
    this._monoHelper = gameObject.AddComponent<SamsungIapAndroidMonoHelper>();
    this.LoadLastPurchasedDate();
    this.LoadUnconfirmedPaymentRecords();
  }

  private string ConsultStartDate
  {
    get
    {
      if (this._lastPurchasedDate == null)
        return DateTime.Now.AddDays(-7.0).ToString("yyyyMMdd");
      return this._lastPurchasedDate;
    }
  }

  private string ConsultEndDate
  {
    get
    {
      return DateTime.Now.ToString("yyyyMMdd");
    }
  }

  private void LoadLastPurchasedDate()
  {
    if (!File.Exists(this.LastPurchasedCacheFilePath))
      return;
    string str = File.ReadAllText(this.LastPurchasedCacheFilePath);
    SamsungIapAndroid.SamsungIapLog(string.Format("LoadLastPurchasedDate {0}", (object) str));
    this._localSavedData = JsonReader.Deserialize<Dictionary<string, string>>(str);
    if (!this._localSavedData.ContainsKey("last_purchase_date"))
      return;
    this._lastPurchasedDate = this._localSavedData["last_purchase_date"];
  }

  private void SaveLastPurchasedDate()
  {
    this._lastPurchasedDate = DateTime.Now.ToString("yyyyMMdd");
    this._localSavedData["last_purchase_date"] = this._lastPurchasedDate;
    string contents = Utils.Object2Json((object) this._localSavedData);
    SamsungIapAndroid.SamsungIapLog(string.Format("Save local data: {0}", (object) contents));
    File.WriteAllText(this.LastPurchasedCacheFilePath, contents);
  }

  private string LastPurchasedCacheFilePath
  {
    get
    {
      return Path.Combine(Application.persistentDataPath, "samsungiap_last_purchase.json");
    }
  }

  private void AddUnconfirmedPaymentRecord(string paymentId, string throughCargo)
  {
    if (this._unconfirmedPaymentRecord.ContainsKey(paymentId))
      return;
    this._unconfirmedPaymentRecord.Add(paymentId, throughCargo);
    this.SaveUnconfirmedPaymentRecords();
  }

  private void RemoveUnconfirmedPaymentRecord(string paymentId)
  {
    if (!this._unconfirmedPaymentRecord.ContainsKey(paymentId))
      return;
    this._unconfirmedPaymentRecord.Remove(paymentId);
    this.SaveUnconfirmedPaymentRecords();
  }

  private void LoadUnconfirmedPaymentRecords()
  {
    if (!File.Exists(this.LastPurchasedCacheFilePath))
      return;
    string str = File.ReadAllText(this.LastPurchasedCacheFilePath);
    SamsungIapAndroid.SamsungIapLog(string.Format("LoadUnconfirmedRequestedProducts {0}", (object) str));
    this._unconfirmedPaymentRecord = JsonReader.Deserialize<Dictionary<string, string>>(str);
  }

  private void SaveUnconfirmedPaymentRecords()
  {
    File.WriteAllText(this.LastPurchasedCacheFilePath, Utils.Object2Json((object) this._unconfirmedPaymentRecord));
  }

  private string UnconfirmPaymentsCacheFilePath
  {
    get
    {
      return Path.Combine(Application.persistentDataPath, "samsungiap.json");
    }
  }

  private ItemVo GetItemVo(string productId)
  {
    if (this._allItemVo.ContainsKey(productId))
      return this._allItemVo[productId];
    return (ItemVo) null;
  }

  public static SamsungIapAndroid Instance
  {
    get
    {
      if (SamsungIapAndroid._instance == null)
      {
        lock (SamsungIapAndroid.locker)
          SamsungIapAndroid._instance = new SamsungIapAndroid();
      }
      return SamsungIapAndroid._instance;
    }
  }

  public override bool GetSubsRenewing(string productId)
  {
    return false;
  }

  public override bool CanCheckSubs()
  {
    return false;
  }

  public GameObject TargetGameObject
  {
    get
    {
      if (!(bool) ((UnityEngine.Object) this._targetGameObject))
        this._targetGameObject = GameObject.Find(this._targetGameObjectName);
      return this._targetGameObject;
    }
  }

  public override void SetGameObject(string gameObjectName)
  {
    this._targetGameObjectName = gameObjectName;
  }

  public override bool CanMakePurchases()
  {
    return true;
  }

  public override void StartHelper()
  {
    SamsungIapAndroid.SamsungIapLog(nameof (StartHelper));
    this.CallSamsungIapInit();
    this.CallInitItemList();
  }

  private void CallSamsungIapInit()
  {
    SamsungIapAndroid.SamsungIapLog(nameof (CallSamsungIapInit));
    AndroidJavaObject unityActivity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
    unityActivity.Call("runOnUiThread", new object[1]
    {
      (object) (AndroidJavaRunnable) (() => new AndroidJavaClass("com.funplus.kingofavalon.SamsungIapFacade").CallStatic("init", (object) unityActivity, (object) "SamsungIapAndroidMonoHelper", (object) 0))
    });
  }

  private void CallInitItemList()
  {
    SamsungIapAndroid.SamsungIapLog("CallIapItemList");
    new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity").Call("runOnUiThread", new object[1]
    {
      (object) (AndroidJavaRunnable) (() => new AndroidJavaClass("com.funplus.kingofavalon.SamsungIapFacade").CallStatic("doGetItemList"))
    });
  }

  private void CallGetInboxList()
  {
    SamsungIapAndroid.SamsungIapLog(nameof (CallGetInboxList));
    new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity").Call("runOnUiThread", new object[1]
    {
      (object) (AndroidJavaRunnable) (() => new AndroidJavaClass("com.funplus.kingofavalon.SamsungIapFacade").CallStatic("doGetInboxList", (object) 1, (object) 10, (object) this.ConsultStartDate, (object) this.ConsultEndDate))
    });
  }

  public override void Buy(string productId, string throughCargo)
  {
    D.error((object) "have not implement");
  }

  private Hashtable GetReceiptParamFromThroughCargon(string throughCargon)
  {
    Hashtable hashtable = new Hashtable();
    hashtable[(object) "uid"] = (object) AccountManager.Instance.FunplusID;
    string[] strArray = throughCargon.Split('|');
    if (strArray.Length >= 4)
    {
      hashtable[(object) "appservid"] = (object) strArray[3];
    }
    else
    {
      D.error((object) "GetReceiptParamFromThroughCargon Error");
      hashtable[(object) "appservid"] = (object) string.Empty;
    }
    return hashtable;
  }

  public override void Buy(string productId, string serverId, string throughCargo)
  {
    SamsungIapAndroid.SamsungIapLog(string.Format("Buy {0} {1} {2}", (object) productId, (object) serverId, (object) throughCargo));
    this.productId2ThroughCargoDict[productId] = throughCargo;
    this.CallPayment(productId);
  }

  private void RequestOrderConfirm(Hashtable param)
  {
    if (GameEngine.IsReady())
      UIManager.inst.ShowSystemBlocker("FullScreenWait", (SystemBlocker.SystemBlockerParameter) null);
    GameObject gameObject = new GameObject();
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
    gameObject.AddComponent<HttpPostProcessor>().DoPost(this.PaymentConfirmUrl, param, param, new HttpPostProcessor.PostResultCallback(this.OnOrderConfirmCallback));
  }

  private void OnOrderConfirmCallback(bool result, Hashtable data, Hashtable userData)
  {
    if (GameEngine.IsReady())
      UIManager.inst.HideSystemBlocker("FullScreenWait", (SystemBlocker.SystemBlockerParameter) null);
    SamsungIapAndroid.SamsungIapLog(string.Format("OnOrderConfirmCallback {0} {1} {2}", (object) result, data == null ? (object) "null" : (object) Utils.Object2Json((object) data), userData == null ? (object) "null" : (object) Utils.Object2Json((object) userData)));
    if (result)
    {
      if (!data.ContainsKey((object) nameof (data)) || data[(object) nameof (data)] == null)
        return;
      Hashtable hashtable1 = data[(object) nameof (data)] as Hashtable;
      foreach (object key in (IEnumerable) hashtable1.Keys)
      {
        string index = key as string;
        Hashtable hashtable2 = hashtable1[(object) index] as Hashtable;
        if (hashtable2.ContainsKey((object) "status") && hashtable2[(object) "status"].ToString() == "1")
        {
          string productId = hashtable2[(object) "product_id"] as string;
          Dictionary<string, object> dictionary = new Dictionary<string, object>();
          dictionary.Add("product_id", (object) productId);
          string empty = string.Empty;
          if (this._unconfirmedPaymentRecord.ContainsKey(index))
            empty = this._unconfirmedPaymentRecord[index];
          dictionary.Add("through_cargo", (object) empty);
          this.TargetGameObject.BroadcastMessage("OnPaymentPurchaseSuccess", (object) Json.Serialize((object) dictionary));
          this.TracePaymentBIEvent(index, productId);
          this.RemoveUnconfirmedPaymentRecord(index);
          this.SaveLastPurchasedDate();
        }
        else if (hashtable2.ContainsKey((object) "status") && hashtable2[(object) "status"].ToString() == "2")
        {
          this.RemoveUnconfirmedPaymentRecord(index);
          this.SaveLastPurchasedDate();
        }
        else
          this.TargetGameObject.BroadcastMessage("OnPaymentPurchaseError", (object) "{errorCode:9999, errorMsg:\"\", errorLocalizedMsg:\"\"}");
      }
    }
    else
      SamsungIapAndroid.SamsungIapLog("OnOrderConfirmCallback failed");
  }

  private void TracePaymentBIEvent(string paymentId, string productId)
  {
    try
    {
      if (FunplusSdk.Instance.IsSdkInstalled())
      {
        Dictionary<string, object> dictionary = new Dictionary<string, object>();
        ItemVo itemVo = this.GetItemVo(productId);
        dictionary.Add("amount", (object) itemVo.mItemPrice.ToString());
        dictionary.Add("currency", (object) itemVo.mCurrencyCode);
        dictionary.Add("iap_product_id", (object) itemVo.mItemId);
        dictionary.Add("iap_product_name", (object) itemVo.mItemName);
        dictionary.Add("transaction_id", (object) paymentId);
        dictionary.Add("payment_processor", (object) "samsungiap");
        dictionary.Add("d_currency_received_type", (object) "point");
        SamsungIapAndroid.SamsungIapLog(string.Format(nameof (TracePaymentBIEvent)));
        FunplusBi.Instance.TraceEvent("payment", Json.Serialize((object) dictionary));
      }
      else
        SamsungIapAndroid.SamsungIapLog(string.Format("TracePaymentBIEvent exception: {0}", (object) "FunplusSdk.Instance.IsSdkInstalled() == false"));
    }
    catch (Exception ex)
    {
      SamsungIapAndroid.SamsungIapLog(string.Format("TracePaymentBIEvent exception: {0}", !string.IsNullOrEmpty(ex.Message) ? (object) ex.Message : (object) "unknow"));
    }
  }

  public override void SetCurrencyWhitelist(string whitelist)
  {
  }

  private void CallPayment(string productId)
  {
    SamsungIapAndroid.SamsungIapLog(string.Format("DoPurchase product_id {0}", (object) productId));
    new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity").Call("runOnUiThread", new object[1]
    {
      (object) (AndroidJavaRunnable) (() => new AndroidJavaClass("com.funplus.kingofavalon.SamsungIapFacade").CallStatic("doPurchase", new object[1]
      {
        (object) productId
      }))
    });
  }

  public void OnPaymentCallback(string result)
  {
    SamsungIapAndroid.SamsungIapLog(string.Format("OnPaymentCallback result: {0}", (object) result));
    Hashtable hashtable = Utils.Json2Object(result, true) as Hashtable;
    if (hashtable != null && hashtable.ContainsKey((object) "status") && hashtable[(object) "status"].ToString() == "ok")
    {
      if (!hashtable.ContainsKey((object) "purchaseVo"))
        return;
      PurchaseVo purchaseVo = JsonReader.Deserialize<PurchaseVo>(hashtable[(object) "purchaseVo"] as string);
      SamsungIapAndroid.SamsungIapLog(purchaseVo.ToString());
      string empty = string.Empty;
      if (this.productId2ThroughCargoDict.ContainsKey(purchaseVo.mItemId))
        empty = this.productId2ThroughCargoDict[purchaseVo.mItemId];
      Hashtable fromThroughCargon = this.GetReceiptParamFromThroughCargon(empty);
      fromThroughCargon.Add((object) "appid", (object) this.AppId);
      fromThroughCargon.Add((object) "udid", (object) FunplusSdkUtils.Instance.GetAndroidID());
      string str = Utils.Object2Json((object) new ArrayList()
      {
        (object) new Hashtable()
        {
          {
            (object) "product_id",
            (object) purchaseVo.mItemId
          },
          {
            (object) "through_cargo",
            (object) empty
          },
          {
            (object) "amount",
            (object) purchaseVo.mItemPrice
          },
          {
            (object) "currency_code",
            (object) purchaseVo.mCurrencyCode
          },
          {
            (object) "country_code",
            (object) FunplusSdkUtils.Instance.GetCountry()
          },
          {
            (object) "payment_id",
            (object) purchaseVo.mPaymentId
          },
          {
            (object) "purchase_id",
            (object) purchaseVo.mPurchaseId
          }
        }
      });
      fromThroughCargon.Add((object) "orders", (object) str);
      this.RequestOrderConfirm(fromThroughCargon);
      this.AddUnconfirmedPaymentRecord(purchaseVo.mPaymentId, empty);
    }
    else
    {
      this.TargetGameObject.BroadcastMessage("OnPaymentPurchaseError", (object) "{errorCode:9999, errorMsg:\"\", errorLocalizedMsg:\"\"}");
      if (hashtable == null || !hashtable.ContainsKey((object) "err_msg"))
        return;
      SamsungIapAndroid.SamsungIapLog(string.Format("samsung payment return failed, error msg:{0}", hashtable[(object) "err_msg"]));
    }
  }

  public void OnGetInboxListCallback(string result)
  {
    SamsungIapAndroid.SamsungIapLog(string.Format("OnGetInboxListCallback({0})", (object) result));
    Hashtable hashtable1 = Utils.Json2Object(result, true) as Hashtable;
    if (hashtable1 != null && hashtable1.ContainsKey((object) "status") && hashtable1[(object) "status"].ToString() == "ok")
    {
      if (!hashtable1.ContainsKey((object) "arrayList"))
        return;
      ArrayList arrayList1 = JsonReader.Deserialize<ArrayList>(hashtable1[(object) "arrayList"] as string);
      Hashtable hashtable2 = (Hashtable) null;
      ArrayList arrayList2 = new ArrayList();
      foreach (object obj in arrayList1)
      {
        string str = obj as string;
        SamsungIapAndroid.SamsungIapLog(string.Format("Inbox Json: {0}", (object) str));
        InboxVo inboxVo = JsonReader.Deserialize<InboxVo>(str);
        SamsungIapAndroid.SamsungIapLog(string.Format("Inbox ToString: {0}", (object) inboxVo.ToString()));
        string empty = string.Empty;
        if (this._unconfirmedPaymentRecord.ContainsKey(inboxVo.mPaymentId))
          empty = this._unconfirmedPaymentRecord[inboxVo.mPaymentId];
        if (hashtable2 == null)
        {
          hashtable2 = this.GetReceiptParamFromThroughCargon(empty);
          hashtable2.Add((object) "appid", (object) this.AppId);
          hashtable2.Add((object) "udid", (object) FunplusSdkUtils.Instance.GetAndroidID());
        }
        arrayList2.Add((object) new Hashtable()
        {
          {
            (object) "product_id",
            (object) inboxVo.mItemId
          },
          {
            (object) "through_cargo",
            (object) empty
          },
          {
            (object) "amount",
            (object) inboxVo.mItemPrice
          },
          {
            (object) "currency_code",
            (object) inboxVo.mCurrencyCode
          },
          {
            (object) "country_code",
            (object) FunplusSdkUtils.Instance.GetCountry()
          },
          {
            (object) "payment_id",
            (object) inboxVo.mPaymentId
          },
          {
            (object) "purchase_id",
            (object) inboxVo.mPurchaseId
          }
        });
        SamsungIapAndroid.SamsungIapLog(string.Format("reconfirm order: {0}", (object) str));
      }
      if (arrayList1.Count <= 0 || hashtable2 == null)
        return;
      string str1 = Utils.Object2Json((object) arrayList2);
      hashtable2.Add((object) "orders", (object) str1);
      this.RequestOrderConfirm(hashtable2);
    }
    else
    {
      if (hashtable1 == null || !hashtable1.ContainsKey((object) "err_msg"))
        return;
      SamsungIapAndroid.SamsungIapLog(string.Format("samsung GetInboxList return failed, error msg:{0}", hashtable1[(object) "err_msg"]));
    }
  }

  public void OnGetItemListCallback(string result)
  {
    SamsungIapAndroid.SamsungIapLog(string.Format("OnGetItemListCallback({0})", (object) result));
    Hashtable hashtable = Utils.Json2Object(result, true) as Hashtable;
    if (hashtable != null && hashtable.ContainsKey((object) "status") && hashtable[(object) "status"].ToString() == "ok")
    {
      if (hashtable.ContainsKey((object) "arrayList"))
      {
        ArrayList arrayList = JsonReader.Deserialize<ArrayList>(hashtable[(object) "arrayList"] as string);
        List<Dictionary<string, object>> dictionaryList = new List<Dictionary<string, object>>();
        foreach (object obj in arrayList)
        {
          ItemVo itemVo = JsonReader.Deserialize<ItemVo>(obj as string);
          double mItemPrice = itemVo.mItemPrice;
          long num = (long) (mItemPrice * 1000000.0);
          Dictionary<string, object> dictionary = new Dictionary<string, object>()
          {
            {
              "productId",
              (object) itemVo.mItemId
            },
            {
              "title",
              (object) itemVo.mItemName
            },
            {
              "description",
              (object) itemVo.mItemDesc
            },
            {
              "price_currency_code",
              (object) itemVo.mCurrencyCode
            },
            {
              "price",
              (object) string.Format("{0}{1:N2}", (object) itemVo.mCurrencyUnit, (object) mItemPrice)
            },
            {
              "price_amount",
              (object) string.Format("{0:N2}", (object) mItemPrice)
            },
            {
              "price_amount_micros",
              (object) num
            }
          };
          dictionaryList.Add(dictionary);
          SamsungIapAndroid.SamsungIapLog(string.Format("save ItemVO {0}", (object) itemVo.mItemId));
          if (!this._allItemVo.ContainsKey(itemVo.mItemId))
            this._allItemVo.Add(itemVo.mItemId, itemVo);
        }
        string str = Json.Serialize((object) dictionaryList);
        this.TargetGameObject.BroadcastMessage("OnPaymentInitializeSuccess", (object) str);
        SamsungIapAndroid.SamsungIapLog("SamsungIap product list: " + str);
      }
    }
    else
    {
      this.TargetGameObject.BroadcastMessage("OnPaymentPurchaseError", (object) "{errorCode:9999, errorMsg:\"\", errorLocalizedMsg:\"\"}");
      if (hashtable != null && hashtable.ContainsKey((object) "err_msg"))
        SamsungIapAndroid.SamsungIapLog(string.Format("samsung GetItemList return failed, error msg:{0}", hashtable[(object) "err_msg"]));
    }
    this.CallGetInboxList();
  }

  private string AppId
  {
    get
    {
      if (!string.IsNullOrEmpty(PaymentManager.Instance.PaymentAppId))
        return PaymentManager.Instance.PaymentAppId;
      return FunplusSdk.Instance.Environment == "sandbox" ? "158" : "140";
    }
  }

  private string PaymentConfirmUrl
  {
    get
    {
      if (!string.IsNullOrEmpty(PaymentManager.Instance.PaymentUrl))
        return PaymentManager.Instance.PaymentUrl + "/callback/samsungiap/";
      return FunplusSdk.Instance.Environment == "sandbox" ? "https://payment-sandbox.funplusgame.com/callback/samsungiap/" : "https://payment.funplusgame.com/callback/samsungiap/";
    }
  }

  public static void SamsungIapLog(string logDetail)
  {
    Debug.Log((object) ("SamsangIapLog:" + logDetail));
  }
}
