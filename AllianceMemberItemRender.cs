﻿// Decompiled with JetBrains decompiler
// Type: AllianceMemberItemRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceMemberItemRender : MonoBehaviour
{
  private const uint BG_BRIGHT = 16777215;
  private const uint BG_GREY = 7237230;
  private const uint NAME_BIRGHT = 16768159;
  private const int MAX_TITLE = 5;
  public UISprite bg;
  public UILabel memberTitle;
  public UILabel memberName;
  public UILabel memberScore;
  public GameObject selected;
  public System.Action<AllianceMemberItemRender> OnClickedHandler;
  private bool isRewardNotReady;
  private AllianceMemberItemData data;

  public AllianceMemberItemData Data
  {
    get
    {
      return this.data;
    }
  }

  public void FeedData(AllianceMemberItemData data, bool isRewardReady)
  {
    this.isRewardNotReady = isRewardReady;
    this.FeedData(data);
  }

  public void FeedData(AllianceMemberItemData data)
  {
    this.data = data;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    int num = this.data.title;
    if (num > 5)
      num = 5;
    this.memberTitle.text = "R" + num.ToString();
    this.memberName.text = this.data.name;
    this.memberScore.text = this.data.score.ToString();
    this.GrayOutItem();
  }

  private void GrayOutItem()
  {
    if (this.isRewardNotReady)
    {
      this.bg.color = Utils.GetColorFromHex(7237230U);
      this.memberTitle.color = Utils.GetColorFromHex(7237230U);
      this.memberName.color = Utils.GetColorFromHex(7237230U);
      this.memberScore.color = Utils.GetColorFromHex(7237230U);
    }
    else if (this.data.hasBeenDistributed || this.data.hasLeftAlliace || this.data.hasNotContributed)
    {
      this.bg.color = Utils.GetColorFromHex(7237230U);
      this.memberTitle.color = Utils.GetColorFromHex(7237230U);
      this.memberName.color = Utils.GetColorFromHex(7237230U);
      this.memberScore.color = Utils.GetColorFromHex(7237230U);
    }
    else
    {
      this.bg.color = Utils.GetColorFromHex(16777215U);
      this.memberTitle.color = Utils.GetColorFromHex(16777215U);
      this.memberName.color = Utils.GetColorFromHex(16768159U);
      this.memberScore.color = Utils.GetColorFromHex(16777215U);
    }
  }

  public bool Select
  {
    set
    {
      NGUITools.SetActive(this.selected, value);
    }
  }

  public void OnClick()
  {
    if (this.OnClickedHandler == null)
      return;
    this.OnClickedHandler(this);
  }
}
