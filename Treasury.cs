﻿// Decompiled with JetBrains decompiler
// Type: Treasury
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Treasury : MonoBehaviour
{
  public GameObject coinEffect;
  public GameObject doorEffect;
  public Animator doorAnimator;
  public System.Action onCoinClicked;

  public void OnCoinBtnPressed()
  {
    if (this.onCoinClicked == null)
      return;
    this.onCoinClicked();
  }
}
