﻿// Decompiled with JetBrains decompiler
// Type: ChristmasEveItemSlotRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ChristmasEveItemSlotRenderer : MonoBehaviour
{
  public int currentDay;
  public UILabel dayText;
  public UILabel dayGhostingText;
  public UISprite giftSprite;
  public GameObject dailyCardClaimed;
  public GameObject dailyCardExpired;
  public GameObject dailyCardActived;
  public GameObject normalDayNode;
  public GameObject eveDayNode;
  public System.Action<SuperLoginMainInfo> onDailyCardPressed;
  private SuperLoginMainInfo _mainInfo;

  public void UpdateUI()
  {
    List<SuperLoginMainInfo> mainInfoListByGroup = ConfigManager.inst.DB_SuperLoginMain.GetSuperLoginMainInfoListByGroup("super_log_in_christmas");
    if (mainInfoListByGroup != null)
      this._mainInfo = mainInfoListByGroup.Find((Predicate<SuperLoginMainInfo>) (x => x.day == this.currentDay));
    if (this._mainInfo == null)
      return;
    UILabel dayText = this.dayText;
    string str1 = this._mainInfo.day.ToString();
    this.dayGhostingText.text = str1;
    string str2 = str1;
    dayText.text = str2;
    this.giftSprite.spriteName = this._mainInfo.rewardIcon;
    NGUITools.SetActive(this.normalDayNode, this._mainInfo.day != 25);
    NGUITools.SetActive(this.eveDayNode, this._mainInfo.day == 25);
    NGUITools.SetActive(this.dailyCardClaimed, false);
    NGUITools.SetActive(this.dailyCardExpired, false);
    NGUITools.SetActive(this.dailyCardActived, false);
    switch (this._mainInfo.RewardState + 1)
    {
      case SuperLoginPayload.DailyRewardState.CAN_GET:
        NGUITools.SetActive(this.dailyCardExpired, true);
        break;
      case SuperLoginPayload.DailyRewardState.HAS_GOT:
        NGUITools.SetActive(this.dailyCardActived, true);
        break;
      case SuperLoginPayload.DailyRewardState.CANT_GET:
        NGUITools.SetActive(this.dailyCardClaimed, true);
        break;
    }
  }

  public void OnDailyCardPressed()
  {
    if (this._mainInfo != null && this._mainInfo.RewardState == SuperLoginPayload.DailyRewardState.CANT_GET || this.onDailyCardPressed == null)
      return;
    this.onDailyCardPressed(this._mainInfo);
  }
}
