﻿// Decompiled with JetBrains decompiler
// Type: SuperLoginPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperLoginPayload
{
  private Dictionary<string, SuperLoginBaseData> _superLoginDataDict = new Dictionary<string, SuperLoginBaseData>();
  private static SuperLoginPayload instance;

  public static SuperLoginPayload Instance
  {
    get
    {
      if (SuperLoginPayload.instance == null)
        SuperLoginPayload.instance = new SuperLoginPayload();
      return SuperLoginPayload.instance;
    }
  }

  public Dictionary<string, SuperLoginBaseData> SuperLoginDataDict
  {
    get
    {
      return this._superLoginDataDict;
    }
  }

  public void RequestServerData(System.Action<bool, object> callback = null, bool blockScreen = true)
  {
    if (callback != null)
      MessageHub.inst.GetPortByAction("Activity:getSuperLoginState").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          this.Decode(data as Hashtable);
        if (callback == null)
          return;
        callback(ret, data);
      }), blockScreen);
    else
      MessageHub.inst.GetPortByAction("Activity:getSuperLoginState").SendLoader((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.Decode(data as Hashtable);
      }), false, false);
  }

  public void ReceiveDailyReward(int whichDay, string activityId, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("Activity:receiveSuperLoginReward").SendRequest(new Hashtable()
    {
      {
        (object) "day",
        (object) whichDay
      },
      {
        (object) "activity_id",
        (object) activityId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.Decode(data as Hashtable);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public bool CanActivityShow(string activityId)
  {
    if (SuperLoginPayload.Instance.SuperLoginDataDict.ContainsKey(activityId))
    {
      SuperLoginBaseData superLoginBaseData = SuperLoginPayload.Instance.SuperLoginDataDict[activityId];
      if (superLoginBaseData != null && NetServerTime.inst.ServerTimestamp >= superLoginBaseData.StartTime)
        return NetServerTime.inst.ServerTimestamp < superLoginBaseData.EndTime;
    }
    return false;
  }

  public int CurrentDay
  {
    get
    {
      SuperLoginGroupInfo superLoginGroupInfo = ConfigManager.inst.DB_SuperLoginGroup.Get("super_log_in_1_year_celebration");
      if (superLoginGroupInfo != null)
        return Mathf.CeilToInt((float) (NetServerTime.inst.ServerTimestamp - (int) Utils.DateTime2ServerTime(string.Format("{0}-{1}-{2}", (object) superLoginGroupInfo.startYear, (object) superLoginGroupInfo.startMonth, (object) superLoginGroupInfo.startDate), "-")) / 86400f);
      return 0;
    }
  }

  public void Initialize()
  {
    this.RequestServerData((System.Action<bool, object>) null, true);
  }

  public void Dispose()
  {
    this._superLoginDataDict.Clear();
  }

  private void Decode(Hashtable data)
  {
    this._superLoginDataDict.Clear();
    if (data == null)
      return;
    IDictionaryEnumerator enumerator = data.GetEnumerator();
    while (enumerator.MoveNext())
    {
      string key1 = enumerator.Key.ToString();
      SuperLoginBaseData superLoginBaseData = (SuperLoginBaseData) null;
      string key2 = key1;
      if (key2 != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (SuperLoginPayload.\u003C\u003Ef__switch\u0024mapAA == null)
        {
          // ISSUE: reference to a compiler-generated field
          SuperLoginPayload.\u003C\u003Ef__switch\u0024mapAA = new Dictionary<string, int>(2)
          {
            {
              "super_log_in_christmas",
              0
            },
            {
              "super_log_in_1_year_celebration",
              1
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (SuperLoginPayload.\u003C\u003Ef__switch\u0024mapAA.TryGetValue(key2, out num))
        {
          switch (num)
          {
            case 0:
              superLoginBaseData = (SuperLoginBaseData) new SuperLoginChristmasEveData();
              break;
            case 1:
              superLoginBaseData = new SuperLoginBaseData();
              break;
          }
        }
      }
      if (superLoginBaseData != null)
      {
        superLoginBaseData.Decode(enumerator.Value as Hashtable);
        if (!this._superLoginDataDict.ContainsKey(key1))
          this._superLoginDataDict.Add(key1, superLoginBaseData);
      }
    }
  }

  public enum DailyRewardState
  {
    EXPIRED = -1,
    CAN_GET = 0,
    HAS_GOT = 1,
    CANT_GET = 2,
    INVALID = 3,
  }
}
