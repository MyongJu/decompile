﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightPreview
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DragonKnightPreview : MonoBehaviour
{
  private DragonKnightGender m_Gender = DragonKnightGender.Unknown;
  private const string MODEL_MALE_PATH = "Prefab/UI/DragonKnight/DragonKnightMale";
  private const string MODEL_FEMALE_PATH = "Prefab/UI/DragonKnight/DragonKnightFemale";
  public Light m_Light;
  private GameObject m_HeroInstance;

  public void InitializeCharacter(DragonKnightGender gender)
  {
    if (this.m_Gender == gender)
      return;
    this.m_Gender = gender;
    if ((bool) ((UnityEngine.Object) this.m_HeroInstance))
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_HeroInstance);
      this.m_HeroInstance = (GameObject) null;
    }
    AssetManager.Instance.LoadAsync(this.ModelPath, (System.Action<UnityEngine.Object, bool>) ((o, ret) =>
    {
      try
      {
        this.m_HeroInstance = UnityEngine.Object.Instantiate<GameObject>(o as GameObject);
        this.m_HeroInstance.transform.parent = this.gameObject.transform;
        this.m_HeroInstance.transform.localPosition = Vector3.zero;
        this.m_HeroInstance.transform.localRotation = Quaternion.identity;
        this.m_HeroInstance.transform.localScale = Vector3.one;
      }
      catch
      {
      }
      AssetManager.Instance.UnLoadAsset(this.ModelPath, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    }), (System.Type) null);
  }

  private string ModelPath
  {
    get
    {
      return this.m_Gender == DragonKnightGender.Male || this.m_Gender != DragonKnightGender.Female ? "Prefab/UI/DragonKnight/DragonKnightMale" : "Prefab/UI/DragonKnight/DragonKnightFemale";
    }
  }

  public void SetLightEnable(bool enable)
  {
    this.m_Light.gameObject.SetActive(enable);
  }

  public void Rotate(float angle)
  {
    if (!(bool) ((UnityEngine.Object) this.m_HeroInstance))
      return;
    this.m_HeroInstance.transform.localRotation = Quaternion.Euler(0.0f, angle, 0.0f) * this.m_HeroInstance.transform.localRotation;
  }
}
