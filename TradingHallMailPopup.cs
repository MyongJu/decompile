﻿// Decompiled with JetBrains decompiler
// Type: TradingHallMailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections;
using UI;

public class TradingHallMailPopup : BaseReportPopup
{
  public RewardItemComponent ric;
  public ItemIconRenderer itemIconRender;
  public UIScrollView reportScrollView;
  public UILabel receiveTimeLabel;
  public UILabel receiveItemName;
  public UILabel receiveItemCount;
  private UIButton _btnCollect;

  private void Init()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, true, false, string.Empty);
    this._btnCollect = this.btnCollectionBase.GetComponentInChildren<UIButton>();
    Hashtable hashtable1 = this.param.mail.data[(object) "attachment"] as Hashtable;
    if (hashtable1 == null)
    {
      Hashtable hashtable2 = this.param.mail.data[(object) "data"] as Hashtable;
      if (hashtable2 != null && hashtable2.ContainsKey((object) "exchange_id") && hashtable2.ContainsKey((object) "count"))
      {
        int result1 = 0;
        int result2 = 0;
        int.TryParse(hashtable2[(object) "exchange_id"].ToString(), out result1);
        int.TryParse(hashtable2[(object) "count"].ToString(), out result2);
        ExchangeHallInfo exchangeHallInfo = ConfigManager.inst.DB_ExchangeHall.Get(result1);
        if (exchangeHallInfo != null)
        {
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(exchangeHallInfo.itemId);
          if (itemStaticInfo != null)
          {
            this.receiveItemName.text = itemStaticInfo.LocName;
            if (exchangeHallInfo.Enhanced > 0)
              this.receiveItemName.text += string.Format("+{0}", (object) exchangeHallInfo.Enhanced);
            this.receiveItemCount.text = "X" + result2.ToString();
            EquipmentManager.Instance.ConfigQualityLabelWithColor(this.receiveItemName, itemStaticInfo.Quality);
          }
          this.itemIconRender.SetData(exchangeHallInfo.itemId, string.Empty, false);
          NGUITools.SetActive(this.btnCollectionBase, false);
          NGUITools.SetActive(this.receiveTimeLabel.gameObject, false);
        }
      }
      else
      {
        NGUITools.SetActive(this.ric.gameObject, false);
        NGUITools.SetActive(this.itemIconRender.gameObject, false);
      }
    }
    else
    {
      RewardData rewardData = JsonReader.Deserialize<RewardData>(Utils.Object2Json((object) hashtable1));
      if (rewardData.item == null && rewardData.gold > 0)
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_gold_1");
        if (itemStaticInfo != null)
        {
          this.itemIconRender.SetData(itemStaticInfo.internalId, string.Empty, false);
          this.receiveItemName.text = Utils.XLAT("id_gold");
          this.receiveItemCount.text = rewardData.gold.ToString();
          EquipmentManager.Instance.ConfigQualityLabelWithColor(this.receiveItemName, itemStaticInfo.Quality);
        }
        Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
        this.UpdateReceiveTime();
      }
    }
    this.reportScrollView.ResetPosition();
    this.reportScrollView.restrictWithinPanel = true;
    this.reportScrollView.InvalidateBounds();
  }

  private void UpdateReceiveTime()
  {
    if ((UnityEngine.Object) this._btnCollect == (UnityEngine.Object) null)
      this._btnCollect = this.btnCollectionBase.GetComponentInChildren<UIButton>();
    if ((UnityEngine.Object) this._btnCollect == (UnityEngine.Object) null)
      return;
    if (this.param.mail.attachment_status == 2)
    {
      this.receiveTimeLabel.text = Utils.XLAT("mail_notice_reward_received");
      this._btnCollect.isEnabled = false;
      this.CancelInvoke();
    }
    else if (this.param.mail.AttachmentExpirationLeftTime > 0)
    {
      this.receiveTimeLabel.text = Utils.XLAT("mail_notice_reward_time_remaining") + Utils.ConvertSecsToString((double) this.param.mail.AttachmentExpirationLeftTime);
      this._btnCollect.isEnabled = true;
    }
    else
    {
      this.receiveTimeLabel.text = Utils.XLAT("mail_notice_reward_expired");
      this._btnCollect.isEnabled = false;
      this.CancelInvoke();
    }
  }

  private void OnSecondEvent(int time)
  {
    this.UpdateReceiveTime();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }
}
