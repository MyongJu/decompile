﻿// Decompiled with JetBrains decompiler
// Type: ItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ItemRenderer : Icon
{
  public UISprite selectedImage;
  private bool _selected;
  public System.Action<int> OnSelectedItemDelegate;
  public GameObject newTip;
  public GameObject rootCdTime;
  public UILabel labelCdTime;
  protected int leftCdTime;
  public UITexture background;

  public override void Dispose()
  {
    base.Dispose();
    this.OnSelectedItemDelegate = (System.Action<int>) null;
  }

  public override void SetData(string[] contents, string path)
  {
    base.SetData(contents, path);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.ItemID);
    if (this.labels.Length > 1)
    {
      if (CityManager.inst.mStronghold.mLevel >= itemStaticInfo.RequireLev)
        NGUITools.SetActive(this.labels[1].gameObject, false);
      else
        NGUITools.SetActive(this.labels[1].gameObject, true);
    }
    Utils.SetItemBackground(this.background, this.ItemID);
    NGUITools.SetActive(this.newTip, ItemBag.Instance.IsNewItem(this.ItemID));
    this.leftCdTime = itemStaticInfo.RemainTime;
    if ((UnityEngine.Object) this.rootCdTime != (UnityEngine.Object) null)
      NGUITools.SetActive(this.rootCdTime, this.leftCdTime > 0);
    if (!((UnityEngine.Object) this.labelCdTime != (UnityEngine.Object) null))
      return;
    this.labelCdTime.text = Utils.XLAT("id_cooldown") + Utils.FormatTime(this.leftCdTime, false, false, true);
  }

  public int UpdateLeftCdTime()
  {
    this.leftCdTime = ItemBag.GetItemLeftCdTime(this.ItemID);
    if (this.leftCdTime <= 0)
    {
      NGUITools.SetActive(this.rootCdTime, false);
    }
    else
    {
      NGUITools.SetActive(this.rootCdTime, true);
      this.labelCdTime.text = Utils.XLAT("id_cooldown") + Utils.FormatTime(this.leftCdTime, false, false, true);
    }
    return this.leftCdTime;
  }

  public bool Selected
  {
    get
    {
      return this._selected;
    }
    set
    {
      if (this._selected == value)
        return;
      this._selected = value;
      NGUITools.SetActive(this.selectedImage.gameObject, this._selected);
    }
  }

  public int ItemID
  {
    get
    {
      IconData data = this.data as IconData;
      int num = 0;
      if (data != null)
        num = (int) data.Data;
      return num;
    }
  }

  public void OnItemClickHandler()
  {
    this.Selected = !this.Selected;
    NGUITools.SetActive(this.newTip, false);
    ItemBag.Instance.RemoveNew(this.ItemID);
    if (this.OnSelectedItemDelegate == null)
      return;
    this.OnSelectedItemDelegate(this.ItemID);
  }
}
