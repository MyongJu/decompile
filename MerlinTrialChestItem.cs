﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialChestItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class MerlinTrialChestItem : ComponentRenderBase
{
  public GameObject _chestCanClaim;
  public GameObject _chestClaimed;
  public GameObject _chestNotReady;
  public GameObject _effectGo;
  private int _battleId;
  private MerlinChestStatus _chestStatus;

  public MerlinChestStatus ChestStatus
  {
    set
    {
      this._chestStatus = value;
      switch (this._chestStatus)
      {
        case MerlinChestStatus.not_ready:
          this._chestCanClaim.SetActive(false);
          this._chestClaimed.SetActive(false);
          this._chestNotReady.SetActive(true);
          this._effectGo.SetActive(false);
          break;
        case MerlinChestStatus.can_claim:
          this._chestCanClaim.SetActive(true);
          this._chestClaimed.SetActive(false);
          this._chestNotReady.SetActive(false);
          this._effectGo.SetActive(true);
          break;
        case MerlinChestStatus.claimed:
          this._chestCanClaim.SetActive(false);
          this._chestClaimed.SetActive(true);
          this._chestNotReady.SetActive(false);
          this._effectGo.SetActive(false);
          break;
      }
    }
  }

  public override void Init()
  {
    base.Init();
    MerlinTrialChestItemData data = this.data as MerlinTrialChestItemData;
    if (data == null)
      return;
    this._battleId = data.battleId;
    this.ChestStatus = MerlinTrialsHelper.inst.GetChestStatus(this._battleId);
  }

  public void OnChestClick()
  {
    if (this._chestStatus == MerlinChestStatus.claimed)
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_trial_rewards_collect_success"), (System.Action) null, 4f, true);
    }
    else
    {
      MerlinTrialsMainInfo merlinTrialsMainInfo = ConfigManager.inst.DB_MerlinTrialsMain.Get(this._battleId);
      if (merlinTrialsMainInfo == null)
        return;
      UIManager.inst.OpenPopup("MerlinTrials/MerlinTrialsChestPopup", (Popup.PopupParameter) new MerlinTrialsChestPopup.Parameter()
      {
        merlinMainInfo = merlinTrialsMainInfo,
        chestStatus = this._chestStatus,
        onChestCollected = (System.Action<bool>) (ret =>
        {
          if (!ret)
            return;
          this.ChestStatus = MerlinChestStatus.claimed;
        })
      });
    }
  }
}
