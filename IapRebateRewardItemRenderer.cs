﻿// Decompiled with JetBrains decompiler
// Type: IapRebateRewardItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class IapRebateRewardItemRenderer : MonoBehaviour
{
  public UILabel itemName;
  public UILabel itemCount;
  public UITexture itemTexture;
  public UITexture border;
  private int itemId;
  private IapRebateRewards.RewardItemInfo rewardItemInfo;

  public void SetData(IapRebateRewards.RewardItemInfo rewardItemInfo)
  {
    this.rewardItemInfo = rewardItemInfo;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this.rewardItemInfo == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.rewardItemInfo.rewardItemId);
    if (itemStaticInfo == null)
      return;
    Utils.SetItemName(this.itemName, itemStaticInfo.internalId);
    this.itemCount.text = Utils.FormatThousands(this.rewardItemInfo.rewardItemCount.ToString());
    this.itemId = itemStaticInfo.internalId;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.itemTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    Utils.SetItemBackground(this.border, this.rewardItemInfo.rewardItemId);
  }

  public void OnClickHandler()
  {
    Utils.ShowItemTip(this.itemId, this.border.transform, 0L, 0L, 0);
  }

  public void OnReleaseHandler()
  {
    Utils.StopShowItemTip();
  }

  public void OnPressHandler()
  {
    Utils.DelayShowTip(this.itemId, this.border.transform, 0L, 0L, 0);
  }
}
