﻿// Decompiled with JetBrains decompiler
// Type: AllianceChangeRankHeader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class AllianceChangeRankHeader : AllianceCustomizeHeader
{
  public AllianceChangeRankItemRenderer[] m_Items;
  public UIButton m_ChangeButton;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    this.m_Items[0].SetData(this.allianceData, 6);
    this.m_Items[1].SetData(this.allianceData, 4);
    this.m_Items[2].SetData(this.allianceData, 3);
    this.m_Items[3].SetData(this.allianceData, 2);
    this.m_Items[4].SetData(this.allianceData, 1);
  }

  public void OnChangeRank()
  {
    AllianceManager.Instance.ChangeRosterNames(IllegalWordsUtils.Filter(!string.IsNullOrEmpty(this.m_Items[0].Name) ? this.m_Items[0].Name : this.allianceData.GetTitleName(6)), IllegalWordsUtils.Filter(!string.IsNullOrEmpty(this.m_Items[1].Name) ? this.m_Items[1].Name : this.allianceData.GetTitleName(4)), IllegalWordsUtils.Filter(!string.IsNullOrEmpty(this.m_Items[2].Name) ? this.m_Items[2].Name : this.allianceData.GetTitleName(3)), IllegalWordsUtils.Filter(!string.IsNullOrEmpty(this.m_Items[3].Name) ? this.m_Items[3].Name : this.allianceData.GetTitleName(2)), IllegalWordsUtils.Filter(!string.IsNullOrEmpty(this.m_Items[4].Name) ? this.m_Items[4].Name : this.allianceData.GetTitleName(1)), new System.Action<bool, object>(this.SetupAllianceCallback));
  }

  private void SetupAllianceCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.UpdateUI();
    UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_rank_titles_changed"), (System.Action) null, 4f, false);
  }

  private bool IsOkay
  {
    get
    {
      bool flag = true;
      for (int index = 0; index < this.m_Items.Length; ++index)
        flag &= this.m_Items[index].isOkay;
      return flag;
    }
  }

  private void Update()
  {
    this.m_ChangeButton.isEnabled = this.IsOkay;
  }
}
