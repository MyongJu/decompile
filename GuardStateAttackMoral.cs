﻿// Decompiled with JetBrains decompiler
// Type: GuardStateAttackMoral
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class GuardStateAttackMoral : AbstractGuardState
{
  public GuardStateAttackMoral(RoundPlayer player)
    : base(player)
  {
  }

  public override string Key
  {
    get
    {
      return "guard_attack";
    }
  }

  public override Hashtable Data { get; set; }

  public override void OnEnter()
  {
    RoundPlayer player = BattleManager.Instance.GetPlayer(BattleManager.Instance.TargetId);
    DragonKnightSystem.Instance.Controller.BattleAudio.Play("Prefab/DragonKnight/Objects/SFX/dragon_knight_standard_atk_1", false);
    GuardVFXHelper.PlayVFX("Prefab/DragonKnight/Objects/VFX/fx_blade_trail", Vector3.zero, Vector3.zero, Vector3.one, GuardVFXHelper.GetAttachPoint(player.Animation.gameObject, "Action").transform);
    BattleManager.Instance.SendPacket((DragonKnightPacket) new DragonKnightPacketHit(this.Player.PlayerId));
    this.Player.StateMacine.SetState("guard_idle", (Hashtable) null);
  }

  public override void OnProcess()
  {
  }

  public override void OnExit()
  {
  }

  public override void Dispose()
  {
  }
}
