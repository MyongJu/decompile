﻿// Decompiled with JetBrains decompiler
// Type: ActivityRequirementInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class ActivityRequirementInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "activity_id")]
  public int ActivityMainSubInfo_ID;
  [Config(Name = "priority")]
  public int Priority;
  [Config(Name = "requirement")]
  public string Requirement;
  [Config(Name = "req_param_1")]
  public int Unit;
  [Config(Name = "req_value_1")]
  public int ValueOneUnit;
  [Config(Name = "description")]
  public string DescriptionKey;

  public string Description
  {
    get
    {
      return ScriptLocalization.GetWithPara(this.DescriptionKey, new Dictionary<string, string>()
      {
        {
          "1",
          this.Unit.ToString()
        },
        {
          "2",
          this.ValueOneUnit.ToString()
        }
      }, true);
    }
  }
}
