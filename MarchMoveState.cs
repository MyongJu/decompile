﻿// Decompiled with JetBrains decompiler
// Type: MarchMoveState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class MarchMoveState : MarchBaseState
{
  public override string Key
  {
    get
    {
      return "march_move";
    }
  }

  protected override void Prepare()
  {
    base.Prepare();
    switch (this.StateData.marchType)
    {
      case MarchData.MarchType.trade:
      case MarchData.MarchType.gather:
        AudioManager.Instance.PlaySound("sfx_march_resources", false);
        break;
      case MarchData.MarchType.scout:
        AudioManager.Instance.PlaySound("sfx_kingdom_scout_begin", false);
        break;
      default:
        AudioManager.Instance.PlaySound("sfx_march_begin", false);
        break;
    }
  }

  protected override void OnProcessHandler()
  {
    base.OnProcessHandler();
    if (this.StateData.marchType != MarchData.MarchType.rally || !this.Controler.RallyIsDone() || !this.Controler.HasReturingStateData())
      return;
    this.Controler.RecallHandler();
  }

  protected override void OnStateEnd()
  {
    switch (this.StateData.marchType)
    {
      case MarchData.MarchType.scout:
        AudioManager.Instance.PlaySound("sfx_kingdom_scout_arrive", false);
        break;
      case MarchData.MarchType.gather:
        AudioManager.Instance.PlaySound("sfx_march_arrive", false);
        break;
    }
  }
}
