﻿// Decompiled with JetBrains decompiler
// Type: TreasureMapReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TreasureMapReportPopup : BaseReportPopup
{
  private List<TreasureItemRenderer> _items = new List<TreasureItemRenderer>();
  public UIGrid Grid;
  public GameObject ItemPrefab;
  private TreasureMapReportPopup.TreasureReport _report;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._report = JsonReader.Deserialize<TreasureMapReportPopup.TreasureReport>(Utils.Object2Json((object) this.param.hashtable));
    if ((bool) ((UnityEngine.Object) this.mailcontent))
      this.mailcontent.text = string.Format(this.maildata.GetBodyString(), (object) string.Format("X:{0} Y:{1}", (object) this._report.data.x, (object) this._report.data.y));
    if ((bool) ((UnityEngine.Object) this.headerTexture))
      BuilderFactory.Instance.Build((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, false, true, true, string.Empty);
    this.UpdateItems();
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void ClearItems()
  {
    using (List<TreasureItemRenderer>.Enumerator enumerator = this._items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TreasureItemRenderer current = enumerator.Current;
        current.gameObject.SetActive(true);
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this._items.Clear();
  }

  private void UpdateItems()
  {
    this.ClearItems();
    using (Dictionary<string, string>.Enumerator enumerator = this._report.data.rewards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, string> current = enumerator.Current;
        GameObject gameObject = Utils.DuplicateGOB(this.ItemPrefab);
        gameObject.SetActive(true);
        int itemId = int.Parse(current.Key);
        int count = int.Parse(current.Value);
        TreasureItemRenderer component = gameObject.GetComponent<TreasureItemRenderer>();
        component.SetData(itemId, count);
        this._items.Add(component);
      }
    }
    this.Grid.Reposition();
  }

  public class Parameter : BaseReportPopup.Parameter
  {
  }

  public class TreasureReportData
  {
    public int k;
    public int x;
    public int y;
    public Dictionary<string, string> rewards;
  }

  public class TreasureReport
  {
    public TreasureMapReportPopup.TreasureReportData data;
  }
}
