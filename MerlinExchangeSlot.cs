﻿// Decompiled with JetBrains decompiler
// Type: MerlinExchangeSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinExchangeSlot : MonoBehaviour
{
  [SerializeField]
  private ItemIconRenderer _itemIcon;
  [SerializeField]
  private UILabel _labelItemName;
  [SerializeField]
  private UILabel _labelCoinNeed;
  [SerializeField]
  private UITexture _textureMerlinCoin;
  private MerlinTrialsShopInfo _shopInfo;
  private Color _coinCountColor;

  public void SetData(MerlinTrialsShopInfo shopInfo)
  {
    this._shopInfo = shopInfo;
    this._coinCountColor = this._labelCoinNeed.color;
    this.UpdateUI();
  }

  public void RefreshUI()
  {
    this.UpdateUI();
  }

  public void OnExchangePressed()
  {
    if (this._shopInfo == null)
      return;
    if (PlayerData.inst.userData.MerlinCoin >= (long) this._shopInfo.price)
      UIManager.inst.OpenPopup("MerlinTrials/MerlinExchangePopup", (Popup.PopupParameter) new MerlinExchangePopup.Parameter()
      {
        shopInfo = this._shopInfo
      });
    else
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_insufficient_items_cannot_buy", new Dictionary<string, string>()
      {
        {
          "0",
          MerlinTrialsPayload.Instance.MerlinCoinName
        }
      }, true), (System.Action) null, 4f, true);
  }

  private void UpdateUI()
  {
    if (this._shopInfo == null)
      return;
    this._itemIcon.SetData(this._shopInfo.itemId, string.Empty, true);
    this._labelCoinNeed.text = Utils.FormatThousands(this._shopInfo.price.ToString());
    this._labelCoinNeed.color = PlayerData.inst.userData.MerlinCoin < (long) this._shopInfo.price ? Color.red : this._coinCountColor;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._shopInfo.itemId);
    if (itemStaticInfo != null)
    {
      this._labelItemName.text = itemStaticInfo.LocName;
      EquipmentManager.Instance.ConfigQualityLabelWithColor(this._labelItemName, itemStaticInfo.Quality);
    }
    BuilderFactory.Instance.HandyBuild((UIWidget) this._textureMerlinCoin, MerlinTrialsPayload.Instance.MerlinCoinImagePath, (System.Action<bool>) null, true, true, string.Empty);
  }
}
