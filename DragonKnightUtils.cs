﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UI;
using UnityEngine;

public static class DragonKnightUtils
{
  public const int LEVEL_GRID_COUNT = 16;
  public const int STAMINA_FOR_LEVEL = 15;
  public const string VFX_PATH = "Prefab/DragonKnight/Objects/VFX/";
  public const string SFX_PATH = "Prefab/DragonKnight/Objects/SFX/";
  public const string BGM_PATH = "Prefab/DragonKnight/Objects/BGM/";
  public const string SKILL_PATH = "Prefab/DragonKnight/Objects/Skills/";
  public const string NPC_PATH = "Prefab/DragonKnight/Objects/NPC/";
  public const string ROOM_PATH = "Prefab/DragonKnight/Objects/Room/";
  public const string SYSTEM_PATH = "Prefab/DragonKnight/Objects/System/";
  public const string NPC_SKILL_PATH = "Prefab/DragonKnight/Objects/NPCSKills/";
  public const string UNDER_ATTACK_PATH = "Prefab/DragonKnight/Objects/UnderAttack/";

  public static void MopupOneLevelWithJob(int level, System.Action<bool> callback)
  {
    DragonKnightUtils.MopupMultiLevels(level, level, callback);
  }

  public static void MopupMultiLevels(int from, int to, System.Action<bool> callback)
  {
    Hashtable postData1 = new Hashtable();
    postData1[(object) "from_level"] = (object) from;
    postData1[(object) "to_level"] = (object) to;
    MessageHub.inst.GetPortByAction("DragonKnight:genPvpUser").SendRequest(postData1, (System.Action<bool, object>) ((ret1, data1) =>
    {
      if (ret1)
      {
        Hashtable pvpInfo = new Hashtable();
        Hashtable pvpTime = new Hashtable();
        GenPvpUserPayload genPvpUserPayload = new GenPvpUserPayload();
        genPvpUserPayload.Decode(data1);
        for (int index = 0; index < genPvpUserPayload.dkPvPUsers.Count; ++index)
        {
          DKPvPUser pvpUser = genPvpUserPayload.dkPvPUsers[index];
          BattleManager.Instance.OnResultHandler = (System.Action<Hashtable>) (data =>
          {
            pvpInfo.Add((object) pvpUser.dungeonLevel, data[(object) "win"]);
            pvpUser.MakeLog(data);
            pvpTime.Add((object) pvpUser.dungeonLevel, (object) DragonKnightUtils.SaveMopupHistory(pvpUser.dungeonLevel, pvpUser.param));
          });
          BattleManager.Instance.SetupPVP(new List<long>()
          {
            pvpUser.uid
          }, new List<List<int>>() { pvpUser.skills }, 0 != 0);
          BattleManager.Instance.AutoAttack();
        }
        BattleManager.Instance.OnResultHandler = (System.Action<Hashtable>) null;
        Hashtable postData2 = new Hashtable();
        postData2[(object) "from_level"] = (object) from;
        postData2[(object) "to_level"] = (object) to;
        postData2[(object) "pvp_info"] = (object) pvpInfo;
        postData2[(object) "pvp_time"] = (object) pvpTime;
        MessageHub.inst.GetPortByAction("DragonKnight:dungeonRaid").SendRequest(postData2, (System.Action<bool, object>) ((ret2, data2) =>
        {
          if (ret2)
          {
            DragonKnightDungeonData dungeonData = DragonKnightUtils.GetDungeonData();
            DragonKnightSystem.Instance.Controller.BattleHud.HideSpeedAndQuickBattle();
            DragonKnightSystem.Instance.Controller.RoomHud.HideAll();
            DragonKnightSystem.Instance.RoomManager.SetMazeData(dungeonData);
            DragonKnightSystem.Instance.RoomManager.StartSearch();
            DragonKnightSystem.Instance.Controller.RoomHud.SetTagPosition(DragonKnightSystem.Instance.RoomManager.CurrentRoom);
            if (callback == null)
              return;
            callback(true);
          }
          else
          {
            if (callback == null)
              return;
            callback(false);
          }
        }), true);
      }
      else
      {
        if (callback == null)
          return;
        callback(false);
      }
    }), true);
  }

  public static int MaxMopupLevelsAllowed
  {
    get
    {
      DragonKnightData dragonKnightData = PlayerData.inst.dragonKnightData;
      DragonKnightDungeonData dungeonData = DragonKnightUtils.GetDungeonData();
      if (dungeonData != null && dragonKnightData != null)
      {
        int maxRaidLevel = DragonKnightUtils.MaxRaidLevel;
        if (dungeonData.CurrentLevel <= maxRaidLevel)
        {
          RoomManager roomManager = new RoomManager();
          roomManager.SetMazeData(dungeonData);
          int num1 = 0;
          int num2 = 16 - roomManager.GetSearchedRoomCount(dungeonData.CurrentLevel);
          int num3 = (int) dragonKnightData.Spirit - num2;
          if (num3 >= 0)
          {
            int num4 = num2 <= 0 ? 0 : 1;
            int b = maxRaidLevel - dungeonData.CurrentLevel + num4;
            num1 = Mathf.Min(1 + num3 / 15, b);
          }
          return num1;
        }
      }
      return 0;
    }
  }

  public static bool IsMopupButtonEnabled
  {
    get
    {
      if (DragonKnightUtils.GetDungeonData().CurrentLevel <= DragonKnightUtils.MaxRaidLevel)
        return DragonKnightUtils.MaxMopupLevelsAllowed > 0;
      return false;
    }
  }

  public static bool CanMopupFurther
  {
    get
    {
      DragonKnightData dragonKnightData = PlayerData.inst.dragonKnightData;
      DragonKnightDungeonData dungeonData = DragonKnightUtils.GetDungeonData();
      int maxRaidLevel = DragonKnightUtils.MaxRaidLevel;
      int num = 0;
      VIP_Level_Info vipLevelInfo = PlayerData.inst.hostPlayer.VIPLevelInfo;
      if (vipLevelInfo.ContainsPrivilege("dk_dungeon_layer_complete_3"))
        num = DragonKnightUtils.GetMopupStaminaNeeded(DragonKnightUtils.MaxMopupLevelsAllowed);
      else if (vipLevelInfo.ContainsPrivilege("dk_dungeon_layer_complete_1") || vipLevelInfo.ContainsPrivilege("dk_dungeon_layer_complete_2"))
        num = DragonKnightUtils.GetMopupStaminaNeeded(1);
      if (dungeonData.CurrentLevel <= maxRaidLevel)
        return num > 0;
      return false;
    }
  }

  public static bool IsCurrentLevelDone
  {
    get
    {
      return DragonKnightUtils.GetMopupStaminaNeeded(1) <= 0;
    }
  }

  public static bool IsSpiritEnough
  {
    get
    {
      int mopupStaminaNeeded = DragonKnightUtils.GetMopupStaminaNeeded(1);
      if (mopupStaminaNeeded == 0)
        mopupStaminaNeeded = DragonKnightUtils.GetMopupStaminaNeeded(2);
      DragonKnightData dragonKnightData = PlayerData.inst.dragonKnightData;
      if (dragonKnightData != null && dragonKnightData.Spirit >= (long) mopupStaminaNeeded)
        return dragonKnightData.Spirit > 0L;
      return false;
    }
  }

  public static int MaxRaidLevel
  {
    get
    {
      DragonKnightData dragonKnightData = PlayerData.inst.dragonKnightData;
      if (dragonKnightData.MaxDungeonLevel <= 0)
        return 0;
      return (dragonKnightData.MaxDungeonLevel - 1) / 10 * 10;
    }
  }

  public static int GetMopupStaminaNeeded(int levelCount)
  {
    DragonKnightDungeonData dungeonData = DragonKnightUtils.GetDungeonData();
    if (dungeonData == null || levelCount <= 0)
      return 0;
    RoomManager roomManager = new RoomManager();
    roomManager.SetMazeData(dungeonData);
    return 16 - roomManager.GetSearchedRoomCount(dungeonData.CurrentLevel) + 15 * (levelCount - 1);
  }

  public static string MopupHistoryRootPath
  {
    get
    {
      return Path.Combine(Application.persistentDataPath, "DKMopupHistory");
    }
  }

  public static string MopupHistoryUserPath
  {
    get
    {
      return Path.Combine(DragonKnightUtils.MopupHistoryRootPath, PlayerData.inst.uid.ToString());
    }
  }

  public static string SaveMopupHistory(int level, DragonKnightPVPResultPopup.Parameter parameter)
  {
    if (!Directory.Exists(DragonKnightUtils.MopupHistoryRootPath))
      Directory.CreateDirectory(DragonKnightUtils.MopupHistoryRootPath);
    if (!Directory.Exists(DragonKnightUtils.MopupHistoryUserPath))
      Directory.CreateDirectory(DragonKnightUtils.MopupHistoryUserPath);
    using (List<InventoryItemData>.Enumerator enumerator = parameter.sourceEquipments.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.inlaidGems.LoadAllGems();
    }
    using (List<InventoryItemData>.Enumerator enumerator = parameter.targetEquipments.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.inlaidGems.LoadAllGems();
    }
    parameter.time = NetServerTime.inst.ServerTimestamp;
    string contents = JsonWriter.Serialize((object) parameter);
    string path2 = parameter.time.ToString() + "_" + (object) level;
    File.WriteAllText(Path.Combine(DragonKnightUtils.MopupHistoryUserPath, path2), contents);
    return path2;
  }

  public static DragonKnightPVPResultPopup.Parameter LoadMopupHistory(string name)
  {
    if (Directory.Exists(DragonKnightUtils.MopupHistoryUserPath))
    {
      string path = Path.Combine(DragonKnightUtils.MopupHistoryUserPath, name);
      if (File.Exists(path))
        return JsonReader.Deserialize<DragonKnightPVPResultPopup.Parameter>(File.ReadAllText(path));
    }
    return (DragonKnightPVPResultPopup.Parameter) null;
  }

  public static void ClearMopupHistory()
  {
    if (!Directory.Exists(DragonKnightUtils.MopupHistoryUserPath))
      return;
    Directory.Delete(DragonKnightUtils.MopupHistoryUserPath, true);
  }

  public static string ArenaHistoryRootPath
  {
    get
    {
      return Path.Combine(Application.persistentDataPath, "DKArenaHistory");
    }
  }

  public static string ArenaHistoryUserPath
  {
    get
    {
      return Path.Combine(DragonKnightUtils.ArenaHistoryRootPath, PlayerData.inst.uid.ToString());
    }
  }

  public static void SaveArenaHistory(DragonKnightPVPResultPopup.Parameter parameter)
  {
    if (!Directory.Exists(DragonKnightUtils.ArenaHistoryRootPath))
      Directory.CreateDirectory(DragonKnightUtils.ArenaHistoryRootPath);
    if (!Directory.Exists(DragonKnightUtils.ArenaHistoryUserPath))
      Directory.CreateDirectory(DragonKnightUtils.ArenaHistoryUserPath);
    using (List<InventoryItemData>.Enumerator enumerator = parameter.sourceEquipments.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.inlaidGems.LoadAllGems();
    }
    using (List<InventoryItemData>.Enumerator enumerator = parameter.targetEquipments.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.inlaidGems.LoadAllGems();
    }
    File.WriteAllText(Path.Combine(DragonKnightUtils.ArenaHistoryUserPath, NetServerTime.inst.ServerTimestamp.ToString()), JsonWriter.Serialize((object) parameter));
    List<string> stringList = new List<string>((IEnumerable<string>) Directory.GetFiles(DragonKnightUtils.ArenaHistoryUserPath));
    stringList.Sort();
    int num = 0;
    for (int index = stringList.Count - 1; index >= 0; --index)
    {
      long result = 0;
      if (long.TryParse(Path.GetFileNameWithoutExtension(stringList[index]), out result))
        ++num;
      if (num > 5)
        File.Delete(stringList[index]);
    }
  }

  public static int GetActiveSkillSlotCount()
  {
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("prop_dragon_knight_skill_equip_base_value");
    if (PlayerData.inst.dragonKnightData != null)
      return (int) AttributeCalcHelper.Instance.GetFinalData((float) data.ValueInt, "calc_dragon_knight_skill_equip", PlayerData.inst.dragonKnightData.Benefits);
    return 0;
  }

  public static void ShowDungeonScene()
  {
    RequestManager.inst.SendRequest("DragonKnight:getDungeonInfo", (Hashtable) null, (System.Action<bool, object>) ((ret, orgData) =>
    {
      if (!ret)
        return;
      Hashtable inData = orgData as Hashtable;
      ArrayList arrayList = new ArrayList();
      long outData1 = -1;
      int outData2 = -1;
      if (inData != null)
      {
        arrayList = inData[(object) "rank"] as ArrayList;
        DatabaseTools.UpdateData(inData, "my_rank", ref outData1);
        DatabaseTools.UpdateData(inData, "rank_end_time", ref outData2);
      }
      UIManager.inst.OpenPopup("DragonKnight/DragonKnightDungeonEntryPopup", (Popup.PopupParameter) new DragonKnightDungeonEntryPopup.Parameter()
      {
        dungeonRankList = arrayList,
        myRank = outData1,
        rankEndTime = outData2
      });
    }), true);
  }

  public static List<DragonKnightPVPResultPopup.Parameter> GetDKArenaHistory()
  {
    List<DragonKnightPVPResultPopup.Parameter> parameterList = new List<DragonKnightPVPResultPopup.Parameter>();
    if (Directory.Exists(DragonKnightUtils.ArenaHistoryUserPath))
    {
      foreach (FileInfo file in new DirectoryInfo(DragonKnightUtils.ArenaHistoryUserPath).GetFiles())
      {
        string name = file.Name;
        int result = 0;
        if (int.TryParse(name, out result))
        {
          DragonKnightPVPResultPopup.Parameter parameter = JsonReader.Deserialize<DragonKnightPVPResultPopup.Parameter>(File.ReadAllText(file.FullName));
          if (parameter != null)
          {
            parameter.time = result;
            parameterList.Add(parameter);
          }
        }
      }
    }
    parameterList.Sort((Comparison<DragonKnightPVPResultPopup.Parameter>) ((x, y) => x.time.CompareTo(y.time)));
    parameterList.Reverse();
    return parameterList;
  }

  public static int GetLevel()
  {
    if (PlayerData.inst.dragonKnightData != null)
      return PlayerData.inst.dragonKnightData.Level;
    return 0;
  }

  public static int CurrentDungeonLevel
  {
    get
    {
      DragonKnightDungeonData dungeonData = DragonKnightUtils.GetDungeonData();
      if (dungeonData != null)
        return dungeonData.CurrentLevel;
      return 0;
    }
  }

  public static int MaxDungeonLevel
  {
    get
    {
      DragonKnightData dragonKnightData = PlayerData.inst.dragonKnightData;
      if (dragonKnightData != null)
        return dragonKnightData.MaxDungeonLevel;
      return 0;
    }
  }

  public static JobHandle DungeonRaidJob
  {
    get
    {
      return JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_DRAGON_KNIGHT_DUNGEON_RAID);
    }
  }

  public static long DungeonRaidJobId
  {
    get
    {
      if (DragonKnightUtils.DungeonRaidJob != null)
        return DragonKnightUtils.DungeonRaidJob.GetJobID();
      return 0;
    }
  }

  public static long DungeonRaidJobLeftTime
  {
    get
    {
      JobHandle dungeonRaidJob = DragonKnightUtils.DungeonRaidJob;
      if (dungeonRaidJob != null)
        return (long) dungeonRaidJob.LeftTime();
      return 0;
    }
  }

  public static void UpdateAndPlay(string fileName, RoundPlayer player)
  {
    if (player == null || !(bool) ((UnityEngine.Object) player.Receiver))
      return;
    TextAsset script = AssetManager.Instance.HandyLoad("Prefab/DragonKnight/Objects/NPCSKills/" + fileName, (System.Type) null) as TextAsset;
    GuardSkill guardSkill = GuardSkillParser.Parse(player.Receiver.gameObject, script);
    if (guardSkill == null)
      return;
    for (int index = 0; index < guardSkill.EventDataList.Count; ++index)
    {
      GuardEventData eventData = guardSkill.EventDataList[index];
      player.Receiver.SetEventData(eventData);
    }
    player.Animation.CrossFade(guardSkill.Take);
  }

  public static DragonKnightDungeonData GetDungeonData()
  {
    return DBManager.inst.DB_DragonKnightDungeon.Get(PlayerData.inst.uid);
  }
}
