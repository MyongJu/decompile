﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarFightStartState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceWarFightStartState : AllianceWarBaseState
{
  private List<GroupItemRender> renderList = new List<GroupItemRender>();
  private const string GROUP_RESULT = "alliance_warfare_grouping_results_name";
  private const string TELEPORT_DESCRIPTION = "alliance_warfare_choose_opponent_description";
  private const string TELEPORT_WARNING = "toast_alliance_warfare_return_inmotherland";
  public GameObject itemPrefab;
  public UITable table;
  public UILabel groupingResult;
  public UILabel tipsStart;
  private GroupItemRender currentSelected;

  public override void Show(bool requestData)
  {
    base.Show(false);
    this.RefreshGroupingDatas();
  }

  public override void UpdateUI()
  {
    this.instruction.text = Utils.XLAT("alliance_warfare_event_description");
    this.state.text = ScriptLocalization.GetWithPara("alliance_warfare_opened_status", new Dictionary<string, string>()
    {
      {
        "0",
        Utils.FormatTime(this.allianceWarData.FightEndTime - NetServerTime.inst.ServerTimestamp, true, true, true)
      }
    }, true);
    this.groupingResult.text = Utils.XLAT("alliance_warfare_grouping_results_name");
    this.tipsStart.text = Utils.XLAT("alliance_warfare_choose_opponent_description");
  }

  private void RefreshGroupingDatas()
  {
    if (this.allianceWarData.GroupDataList == null)
      return;
    for (int index = 0; index < this.allianceWarData.GroupDataList.Count; ++index)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.itemPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.table.transform;
      gameObject.transform.localScale = new Vector3(0.85f, 0.85f, 1f);
      gameObject.transform.localPosition = Vector3.zero;
      GroupItemRender component = gameObject.GetComponent<GroupItemRender>();
      component.onSelectedHandler += new System.Action<GroupItemRender>(this.OnSelectedHanlder);
      component.onEnterBtnClicked += new System.Action<GroupItemRender>(this.OnEnterBtnPressed);
      component.onBackBtnClicked += new System.Action<GroupItemRender>(this.OnBackBtnPressed);
      component.FeedData(this.allianceWarData.GroupDataList[index]);
      this.renderList.Add(component);
    }
  }

  private void OnSelectedHanlder(GroupItemRender render)
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.currentSelected)
    {
      this.currentSelected = render;
      this.currentSelected.Select = true;
    }
    else
    {
      if (!((UnityEngine.Object) this.currentSelected != (UnityEngine.Object) render))
        return;
      this.currentSelected.Select = false;
      this.currentSelected = render;
      this.currentSelected.Select = true;
    }
  }

  private void OnEnterBtnPressed(GroupItemRender render)
  {
    AllianceWarPayload.Instance.GetInvadePosition(PlayerData.inst.userData.uid, render.GroupData.alliance_id, (System.Action<bool, Coordinate>) ((ret, location) =>
    {
      if (!ret)
        return;
      AllianceWarPayload.Instance.GotoTarget(location);
    }));
  }

  private void OnBackBtnPressed(GroupItemRender render)
  {
    if (AllianceWarPayload.Instance.IsForeigner())
      AllianceWarPayload.Instance.GotoBack();
    else
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_return_inmotherland"), (System.Action) null, 4f, false);
  }

  private void ClearData()
  {
    for (int index = 0; index < this.renderList.Count; ++index)
    {
      this.renderList[index].gameObject.transform.parent = (Transform) null;
      this.renderList[index].gameObject.SetActive(false);
      this.renderList[index].onSelectedHandler -= new System.Action<GroupItemRender>(this.OnSelectedHanlder);
      this.renderList[index].onEnterBtnClicked -= new System.Action<GroupItemRender>(this.OnEnterBtnPressed);
      this.renderList[index].onBackBtnClicked -= new System.Action<GroupItemRender>(this.OnBackBtnPressed);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.renderList[index].gameObject);
    }
    this.renderList.Clear();
  }

  public override void Hide()
  {
    base.Hide();
    this.ClearData();
  }
}
