﻿// Decompiled with JetBrains decompiler
// Type: AllianceUtilities
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public static class AllianceUtilities
{
  public const string CROWN_PATH = "Texture/LeaderboardIcons/";
  public const float ONLINE_UPDATE_DURATION = 5f;
  public const string DEVOTE_REWARD_STATUS = "alliance_devote_reward_status";
  public const string DEVOTE_REWARD_TIPS_VIEWED = "alliance_devote_reward_tips_viewed";
  public static System.Action OnAllianceDevoteRewardTipsUpdate;

  public static bool CanBlock(long yourUid, int yourTitle, long targetUid, int targetTitle)
  {
    if (yourUid == targetUid || targetTitle != 0)
      return false;
    if (yourTitle != 6)
      return yourTitle == 4;
    return true;
  }

  public static bool CanInvite(long yourUid, int yourTitle, long targetUid, int targetTitle)
  {
    if (yourUid == targetUid || targetTitle != 0)
      return false;
    if (yourTitle != 6)
      return yourTitle == 4;
    return true;
  }

  public static bool CanPromote(long yourUid, int yourTitle, long targetUid, int targetTitle)
  {
    if (yourUid == targetUid || targetTitle == 0)
      return false;
    if (yourTitle == 6)
      return targetTitle < 4;
    return yourTitle - targetTitle >= 2;
  }

  public static bool CanDemote(long yourUid, int yourTitle, long targetUid, int targetTitle)
  {
    if (yourUid == targetUid || targetTitle == 0 || targetTitle == 1)
      return false;
    if (yourTitle == 6)
      return true;
    if (yourTitle - targetTitle >= 1)
      return yourTitle >= 3;
    return false;
  }

  public static bool CanKick(long yourUid, int yourTitle, long targetUid, int targetTitle)
  {
    if (yourUid == targetUid || yourTitle == targetTitle || targetTitle == 0)
      return false;
    if (yourTitle == 6)
      return true;
    if (yourTitle == 4)
      return targetTitle != 6;
    return false;
  }

  public static bool CanTransfer(long yourUid, int yourTitle, long targetUid, int targetTitle)
  {
    if (yourUid == targetUid || targetTitle == 0)
      return false;
    return yourTitle == 6;
  }

  public static bool CanInviteTeleport(long yourUid, int yourTitle, long targetUid, int targetTitle)
  {
    if (yourUid == targetUid || targetTitle == 0)
      return false;
    if (yourTitle != 6 && yourTitle != 3)
      return yourTitle == 4;
    return true;
  }

  public static bool CanHandleApplication(int yourTitle, int targetTitle)
  {
    if (targetTitle != 0)
      return false;
    if (yourTitle != 6)
      return yourTitle == 4;
    return true;
  }

  public static string GetTitleIconPath(int title)
  {
    switch (title)
    {
      case 1:
        return "icon_rank_vassal";
      case 2:
        return "icon_rank_knight";
      case 3:
        return "icon_rank_baron";
      case 4:
        return "icon_rank_duke";
      case 5:
      case 6:
        return "icon_rank_high_lord";
      default:
        return string.Empty;
    }
  }

  public static string GetCrownIconPath(int title)
  {
    switch (title)
    {
      case 1:
        return "Texture/LeaderboardIcons/member_crown_r1";
      case 2:
        return "Texture/LeaderboardIcons/member_crown_r2";
      case 3:
        return "Texture/LeaderboardIcons/member_crown_r3";
      case 4:
        return "Texture/LeaderboardIcons/member_crown_r4";
      case 6:
        return "Texture/LeaderboardIcons/member_crown_r5";
      default:
        return string.Empty;
    }
  }

  public static void CalculateMembers(AllianceData alliance, out int totalDukes, out int totalBarons, out int totalKnights, out int totalVassals, out int totalMembers, out int onlineDukes, out int onlineBarons, out int onlineKnights, out int onlineVassals, out int onlineMembers)
  {
    totalDukes = 0;
    totalBarons = 0;
    totalKnights = 0;
    totalVassals = 0;
    totalMembers = 0;
    onlineDukes = 0;
    onlineBarons = 0;
    onlineKnights = 0;
    onlineVassals = 0;
    onlineMembers = 0;
    using (Dictionary<long, AllianceMemberData>.ValueCollection.Enumerator enumerator = alliance.members.datas.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceMemberData current = enumerator.Current;
        if (current.status == 0)
        {
          UserData userData = DBManager.inst.DB_User.Get(current.uid);
          switch (current.title)
          {
            case 1:
              ++totalVassals;
              if (userData.online)
              {
                ++onlineVassals;
                break;
              }
              break;
            case 2:
              ++totalKnights;
              if (userData.online)
              {
                ++onlineKnights;
                break;
              }
              break;
            case 3:
              ++totalBarons;
              if (userData.online)
              {
                ++onlineBarons;
                break;
              }
              break;
            case 4:
              ++totalDukes;
              if (userData.online)
              {
                ++onlineDukes;
                break;
              }
              break;
          }
          ++totalMembers;
          if (userData.online)
            ++onlineMembers;
        }
      }
    }
  }

  public static long DevoteRewardSettleTime
  {
    get
    {
      string dayOfWeek = string.Empty;
      string time = string.Empty;
      GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("alliance_devote_reward_settlement_day");
      if (data1 != null)
        dayOfWeek = data1.ValueString;
      GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("alliance_devote_reward_settlement_time");
      if (data2 != null)
        time = data2.ValueString;
      return Utils.GetSpecificTime(dayOfWeek, time);
    }
  }

  public static bool IsDevoteRewardAvailable
  {
    get
    {
      if ((long) NetServerTime.inst.ServerTimestamp < AllianceUtilities.DevoteRewardSettleTime)
      {
        AllianceUtilities.SetDevoteRewardCollected(false);
        AllianceUtilities.SetDevoteRewardTipsViewed(false);
      }
      bool flag1 = PlayerPrefsEx.GetIntByUid("alliance_devote_reward_status", 0) == 1;
      bool flag2 = PlayerPrefsEx.GetIntByUid("alliance_devote_reward_tips_viewed", 0) == 1;
      bool flag3 = PlayerData.inst.allianceId > 0L;
      if ((long) NetServerTime.inst.ServerTimestamp >= AllianceUtilities.DevoteRewardSettleTime && !flag1 && !flag2)
        return flag3;
      return false;
    }
  }

  public static void SetDevoteRewardCollected(bool collected)
  {
    PlayerPrefsEx.SetIntByUid("alliance_devote_reward_status", !collected ? 0 : 1);
  }

  public static void SetDevoteRewardTipsViewed(bool viewed)
  {
    PlayerPrefsEx.SetIntByUid("alliance_devote_reward_tips_viewed", !viewed ? 0 : 1);
  }

  public static void DevoteRewardCollect(System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("AllianceTech:devoteRewardCollect").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        AllianceUtilities.SetDevoteRewardCollected(true);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public static void UpdateDevoteRewardTips()
  {
    if (AllianceUtilities.OnAllianceDevoteRewardTipsUpdate == null)
      return;
    AllianceUtilities.OnAllianceDevoteRewardTipsUpdate();
  }
}
