﻿// Decompiled with JetBrains decompiler
// Type: AllianceBuildingConstructDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UI;

public class AllianceBuildingConstructDetailDlg : UI.Dialog
{
  public AllianceBuildingConstructInfo info;
  public AllianceBuildingConstructSlotContainer container;
  public UILabel title;
  public UILabel hint;
  public UIWidget m_FocusTarget;
  public UIButton demolishBtn;
  private Coordinate allianceBuildingCoor;
  private TileType tileType;
  private bool isDestroy;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    this.OnShow(orgParam);
    AllianceBuildingConstructDetailDlg.Parameter parameter = orgParam as AllianceBuildingConstructDetailDlg.Parameter;
    if (parameter != null)
    {
      this.allianceBuildingCoor = parameter.cor;
      this.tileType = parameter.type;
    }
    PVPSystem.Instance.Map.FocusTile(PVPMapData.MapData.ConvertTileKXYToWorldPosition(this.allianceBuildingCoor), this.m_FocusTarget, 0.0f);
    UIManager.inst.CloseKingdomTouchCircle();
    this.UpdateUI();
    this.AddEventHandler();
  }

  private void OnDestroy()
  {
    this.isDestroy = true;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.OnHide(orgParam);
    this.Clear();
    this.RemoveEventHandler();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void Clear()
  {
    this.info.Clear();
    this.container.Clear();
  }

  private void UpdateUI()
  {
    if (this.isDestroy)
      return;
    this.UpdateInfoData();
    this.UpdateItemsData();
    this.title.text = this.GetTitle();
    this.hint.text = this.GetHintByTileType(this.tileType);
    this.UpdateDemolishBtnState();
  }

  private void UpdateItemsData()
  {
    List<long> buildingMarchData = AllianceBuildUtils.GetAllianceBuildingMarchData(this.tileType, this.allianceBuildingCoor);
    bool showEmptyItem = true;
    if (buildingMarchData.Count != 0)
    {
      MarchData marchData = DBManager.inst.DB_March.Get(buildingMarchData[0]);
      if (marchData == null)
        return;
      if (marchData.ownerAllianceId != PlayerData.inst.allianceId)
        showEmptyItem = false;
    }
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(this.allianceBuildingCoor);
    long num = 0;
    if (referenceAt.TileType == TileType.AllianceWarehouse)
      num = referenceAt.WareHouseData.AllianceId;
    else if (referenceAt.TileType == TileType.AllianceSuperMine)
      num = referenceAt.SuperMineData.AllianceId;
    else if (referenceAt.TileType == TileType.AllianceHospital)
      num = referenceAt.HospitalData.AllianceID;
    else if (referenceAt.TileType == TileType.AllianceTemple)
    {
      num = referenceAt.TempleData.allianceId;
      if (referenceAt.TempleData.OwnerAllianceID != PlayerData.inst.allianceId && referenceAt.TempleData.allianceId != PlayerData.inst.allianceId)
        return;
    }
    else if (referenceAt.TileType == TileType.AlliancePortal)
      num = referenceAt.PortalData.AllianceId;
    if (num != PlayerData.inst.allianceId)
      showEmptyItem = false;
    this.container.SetData(buildingMarchData, showEmptyItem);
  }

  private void UpdateInfoData()
  {
    int num = 0;
    long buildingid = 0;
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(this.allianceBuildingCoor);
    string str1 = "The tile type data of {0} is NULL, the coordinate is {1}.";
    if (referenceAt == null)
      return;
    switch (this.tileType)
    {
      case TileType.AllianceWarehouse:
        if (referenceAt.WareHouseData == null)
        {
          D.error((object) str1, (object) this.tileType.ToString(), (object) this.allianceBuildingCoor.ToString());
          break;
        }
        num = referenceAt.WareHouseData.ConfigId;
        buildingid = referenceAt.WareHouseData.WarehouseId;
        break;
      case TileType.AllianceSuperMine:
        if (referenceAt.SuperMineData == null)
        {
          D.error((object) str1, (object) this.tileType.ToString(), (object) this.allianceBuildingCoor.ToString());
          break;
        }
        num = referenceAt.SuperMineData.ConfigId;
        buildingid = referenceAt.SuperMineData.SuperMineId;
        break;
      case TileType.AlliancePortal:
        if (referenceAt.PortalData == null)
        {
          D.error((object) str1, (object) this.tileType.ToString(), (object) this.allianceBuildingCoor.ToString());
          break;
        }
        num = referenceAt.PortalData.ConfigId;
        buildingid = referenceAt.PortalData.PortalId;
        break;
      case TileType.AllianceHospital:
        if (referenceAt.HospitalData == null)
        {
          D.error((object) str1, (object) this.tileType.ToString(), (object) this.allianceBuildingCoor.ToString());
          break;
        }
        num = referenceAt.HospitalData.ConfigId;
        buildingid = referenceAt.HospitalData.BuildingID;
        break;
      case TileType.AllianceTemple:
        if (referenceAt.TempleData == null)
        {
          D.error((object) str1, (object) this.tileType.ToString(), (object) this.allianceBuildingCoor.ToString());
          break;
        }
        num = referenceAt.TempleData.ConfigId;
        buildingid = referenceAt.TempleData.templeId;
        break;
    }
    string buildingStateString = AllianceBuildUtils.GetAllianceBuildingStateString(this.allianceBuildingCoor, this.tileType);
    string str2 = AllianceBuildUtils.CalcAllianceBuildingDurability(num, buildingid).ToString() + "/" + AllianceBuildUtils.GetFullDurability(num).ToString();
    string str3 = AllianceBuildUtils.GetAllianceBuildingCurrentTroopCount(this.allianceBuildingCoor, this.tileType).ToString() + "/" + AllianceBuildUtils.GetAllianceBuildingMaxTroopCount(this.allianceBuildingCoor, this.tileType).ToString();
    AllianceBuildingConstructInfo.InfoData infodata = new AllianceBuildingConstructInfo.InfoData();
    infodata.content.Add(buildingStateString);
    infodata.content.Add(str2);
    infodata.content.Add(str3);
    int buildingJobRemainedTime = AllianceBuildUtils.GetBuildingJobRemainedTime(this.allianceBuildingCoor, this.tileType);
    if (buildingJobRemainedTime > 0)
    {
      infodata.showtime = true;
      infodata.remaintime = buildingJobRemainedTime;
    }
    else
      infodata.showtime = false;
    if (!((UnityEngine.Object) this.info != (UnityEngine.Object) null))
      return;
    this.info.SeedData(infodata);
  }

  private void AddEventHandler()
  {
    switch (this.tileType)
    {
      case TileType.AllianceWarehouse:
        DBManager.inst.DB_AllianceWarehouse.onDataUpdate += new System.Action<AllianceWareHouseData>(this.OnWareHouseUpdated);
        DBManager.inst.DB_AllianceWarehouse.onDataRemove += new System.Action<AllianceWareHouseData>(this.OnWareHouseRemoved);
        break;
      case TileType.AllianceSuperMine:
        DBManager.inst.DB_AllianceSuperMine.onDataUpdate += new System.Action<AllianceSuperMineData>(this.OnSuperMineUpdated);
        DBManager.inst.DB_AllianceSuperMine.onDataRemove += new System.Action<AllianceSuperMineData>(this.OnSuperMineRemoved);
        break;
      case TileType.AlliancePortal:
        DBManager.inst.DB_AlliancePortal.onDataUpdate += new System.Action<AlliancePortalData>(this.OnPortalUpdated);
        DBManager.inst.DB_AlliancePortal.onDataRemove += new System.Action<AlliancePortalData>(this.OnPortalRemoved);
        break;
      case TileType.AllianceHospital:
        DBManager.inst.DB_AllianceHospital.onDataUpdate += new System.Action<AllianceHospitalData>(this.OnHospitalUpdated);
        DBManager.inst.DB_AllianceHospital.onDataRemove += new System.Action<AllianceHospitalData>(this.OnHospitalRemoved);
        break;
      case TileType.AllianceTemple:
        DBManager.inst.DB_AllianceTemple.onDataUpdate += new System.Action<AllianceTempleData>(this.OnTempleUpdated);
        DBManager.inst.DB_AllianceTemple.onDataRemoved += new System.Action<AllianceTempleData>(this.OnTempleRemoved);
        break;
    }
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  private void RemoveEventHandler()
  {
    switch (this.tileType)
    {
      case TileType.AllianceWarehouse:
        DBManager.inst.DB_AllianceWarehouse.onDataUpdate -= new System.Action<AllianceWareHouseData>(this.OnWareHouseUpdated);
        DBManager.inst.DB_AllianceWarehouse.onDataRemove -= new System.Action<AllianceWareHouseData>(this.OnWareHouseRemoved);
        break;
      case TileType.AllianceSuperMine:
        DBManager.inst.DB_AllianceSuperMine.onDataUpdate -= new System.Action<AllianceSuperMineData>(this.OnSuperMineUpdated);
        DBManager.inst.DB_AllianceSuperMine.onDataRemove -= new System.Action<AllianceSuperMineData>(this.OnSuperMineRemoved);
        break;
      case TileType.AlliancePortal:
        DBManager.inst.DB_AlliancePortal.onDataUpdate -= new System.Action<AlliancePortalData>(this.OnPortalUpdated);
        DBManager.inst.DB_AlliancePortal.onDataRemove -= new System.Action<AlliancePortalData>(this.OnPortalRemoved);
        break;
      case TileType.AllianceHospital:
        DBManager.inst.DB_AllianceHospital.onDataUpdate -= new System.Action<AllianceHospitalData>(this.OnHospitalUpdated);
        DBManager.inst.DB_AllianceHospital.onDataRemove -= new System.Action<AllianceHospitalData>(this.OnHospitalRemoved);
        break;
      case TileType.AllianceTemple:
        DBManager.inst.DB_AllianceTemple.onDataUpdate -= new System.Action<AllianceTempleData>(this.OnTempleUpdated);
        DBManager.inst.DB_AllianceTemple.onDataRemoved -= new System.Action<AllianceTempleData>(this.OnTempleRemoved);
        break;
    }
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
  }

  private void Process(int time)
  {
    this.UpdateInfoData();
  }

  private void OnWareHouseRemoved(AllianceWareHouseData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.OnCloseHandler();
  }

  private void OnWareHouseUpdated(AllianceWareHouseData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.UpdateUI();
  }

  private void OnSuperMineRemoved(AllianceSuperMineData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.OnCloseHandler();
  }

  private void OnSuperMineUpdated(AllianceSuperMineData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.UpdateUI();
  }

  private void OnPortalRemoved(AlliancePortalData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.OnCloseHandler();
  }

  private void OnPortalUpdated(AlliancePortalData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.UpdateUI();
  }

  private void OnHospitalRemoved(AllianceHospitalData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.OnCloseHandler();
  }

  private void OnHospitalUpdated(AllianceHospitalData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.UpdateUI();
  }

  private void OnTempleRemoved(AllianceTempleData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.OnCloseHandler();
  }

  private void OnTempleUpdated(AllianceTempleData obj)
  {
    if (!this.allianceBuildingCoor.Equals(obj.Location))
      return;
    this.UpdateUI();
  }

  private string GetHintByTileType(TileType tileType)
  {
    string empty1 = string.Empty;
    string empty2 = string.Empty;
    string Term;
    switch (tileType)
    {
      case TileType.AllianceWarehouse:
        Term = "alliance_city_storage_brief_description";
        break;
      case TileType.AllianceSuperMine:
        Term = "alliance_resource_building_gather_speed_tip";
        break;
      case TileType.AlliancePortal:
        Term = "alliance_fort_troop_construction_speed_tip";
        break;
      case TileType.AllianceHospital:
        Term = "alliance_hospital_lose_troops_tip";
        break;
      case TileType.AllianceTemple:
        if (PVPMapData.MapData.GetReferenceAt(this.allianceBuildingCoor).TempleData.allianceId != PlayerData.inst.allianceId)
          return ScriptLocalization.Get("alliance_fort_troop_demolish_tip", true);
        Term = "alliance_fort_troop_construction_speed_tip";
        break;
      default:
        Term = string.Empty;
        break;
    }
    return ScriptLocalization.Get(Term, true);
  }

  public void OnDemolishBtnClicked()
  {
    string content = string.Format(ScriptLocalization.Get("alliance_buildings_confirm_demolish_description", true), (object) ScriptLocalization.Get(AllianceBuildUtils.GetAllianceBuildingName(this.tileType, this.allianceBuildingCoor), true));
    string title = ScriptLocalization.Get("alliance_buildings_confirm_placement_button", true);
    string left = ScriptLocalization.Get("id_uppercase_confirm", true);
    string right = ScriptLocalization.Get("id_uppercase_cancel", true);
    UIManager.inst.ShowConfirmationBox(title, content, left, right, ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.ConfirmDemolish), (System.Action) null, (System.Action) null);
  }

  private string GetTitle()
  {
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(PVPMapData.MapData.GetReferenceAt(this.allianceBuildingCoor).AllianceID);
    StringBuilder stringBuilder = new StringBuilder();
    if (allianceData != null)
      stringBuilder.Append("(" + allianceData.allianceAcronym + ")");
    string allianceBuildingName = AllianceBuildUtils.GetAllianceBuildingName(this.tileType, this.allianceBuildingCoor);
    stringBuilder.Append(allianceBuildingName);
    return stringBuilder.ToString();
  }

  private void UpdateDemolishBtnState()
  {
    TileData selectedTile = PVPSystem.Instance.Map.SelectedTile;
    long num = 0;
    if (selectedTile.TileType == TileType.AllianceWarehouse)
      num = selectedTile.WareHouseData.AllianceId;
    else if (selectedTile.TileType == TileType.AllianceSuperMine)
      num = selectedTile.SuperMineData.AllianceId;
    else if (selectedTile.TileType == TileType.AllianceHospital)
      num = selectedTile.HospitalData.AllianceID;
    else if (selectedTile.TileType == TileType.AllianceTemple)
      num = selectedTile.TempleData.allianceId;
    AllianceData allianceData = PlayerData.inst.allianceData;
    if (allianceData == null || num != allianceData.allianceId)
    {
      this.demolishBtn.isEnabled = false;
    }
    else
    {
      bool flag = false;
      switch (this.tileType)
      {
        case TileType.AllianceWarehouse:
        case TileType.AllianceSuperMine:
        case TileType.AlliancePortal:
        case TileType.AllianceTemple:
          flag = Utils.IsR5Member() || Utils.IsR4Member();
          break;
        case TileType.AllianceHospital:
          flag = Utils.IsR5Member();
          break;
      }
      this.demolishBtn.isEnabled = flag;
    }
  }

  private void ConfirmDemolish()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "k"] = (object) this.allianceBuildingCoor.K;
    postData[(object) "x"] = (object) this.allianceBuildingCoor.X;
    postData[(object) "y"] = (object) this.allianceBuildingCoor.Y;
    if (this.tileType == TileType.AllianceTemple)
      MessageHub.inst.GetPortByAction("Map:takeBackAllianceTemple").SendRequest(postData, (System.Action<bool, object>) null, true);
    else
      MessageHub.inst.GetPortByAction("Map:takeBackAllianceBuilding").SendRequest(postData, (System.Action<bool, object>) null, true);
    this.OnCloseButtonPress();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public Coordinate cor;
    public TileType type;
  }
}
