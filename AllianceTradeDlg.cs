﻿// Decompiled with JetBrains decompiler
// Type: AllianceTradeDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceTradeDlg : UI.Dialog
{
  private List<GameObject> m_RequestList = new List<GameObject>();
  private List<IOnlineUpdateable> m_MemberList = new List<IOnlineUpdateable>();
  private List<long> m_ChannelList = new List<long>();
  public GameObject mRequestsPage;
  public GameObject mMembersPage;
  public PageTabsMonitor mTabs;
  public GameObject mRequestItem;
  public GameObject mRequestBadge;
  public UIScrollView mRequestScrollView;
  public UITable mRequestTable;
  public GameObject mRequestBoxNormal;
  public GameObject mRequestBoxUsed;
  public GameObject mNoRequests;
  public PageTabsMonitor mRequestType;
  public UIButton mRequestBtn;
  public GameObject mMembersCompressed;
  public GameObject mMembersExpanded;
  public UIScrollView mMembersScrollView;
  public UITable mMemberTable;
  public UIWidget mMemberContainer;
  public AllianceHighLordPanel mHighLordPanel;
  public AllianceHighLordPanel mTempHighLordPanel;
  public GameObject mJoinPanel;
  public GameObject mPagePanel;
  private bool m_ShowDuke;
  private bool m_ShowBaron;
  private bool m_ShowKnight;
  private bool m_ShowVassal;
  private List<long> m_Result;
  private float m_OnlineUpdateTime;

  private void Update()
  {
    if (PlayerData.inst.allianceId == 0L)
      return;
    if (this.m_Result == null)
    {
      this.m_OnlineUpdateTime += Time.deltaTime;
      if ((double) this.m_OnlineUpdateTime < 5.0)
        return;
      this.m_OnlineUpdateTime = 0.0f;
      AllianceMembersContainer members = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId).members;
      this.m_ChannelList.Clear();
      Dictionary<long, AllianceMemberData>.Enumerator enumerator = members.datas.GetEnumerator();
      while (enumerator.MoveNext())
        this.m_ChannelList.Add(DBManager.inst.DB_User.Get(enumerator.Current.Value.uid).channelId);
      PushManager.inst.GetOnlineUsers(this.m_ChannelList, new System.Action<List<long>>(this.GetOnlineUsersCallback));
    }
    else
    {
      int count = this.m_MemberList.Count;
      for (int index = 0; index < count; ++index)
        this.m_MemberList[index].UpdateOnlineStatus(this.m_Result);
      this.m_Result = (List<long>) null;
    }
  }

  private void GetOnlineUsersCallback(List<long> result)
  {
    this.m_Result = result;
  }

  public override void OnCreate(UIControler.UIParameter orgParam = null)
  {
    if ((UnityEngine.Object) this.mRequestBtn != (UnityEngine.Object) null)
      this.mRequestBtn.isEnabled = true;
    MessageHub.inst.GetPortByAction("alliance_trade_help_request").AddEvent(new System.Action<object>(this.OnReceiveRequest));
    MessageHub.inst.GetPortByAction("alliance_trade_help_cancel").AddEvent(new System.Action<object>(this.OnCancelRequest));
  }

  public override void OnFinalize(UIControler.UIParameter orgParam = null)
  {
    MessageHub.inst.GetPortByAction("alliance_trade_help_request").RemoveEvent(new System.Action<object>(this.OnReceiveRequest));
    MessageHub.inst.GetPortByAction("alliance_trade_help_cancel").RemoveEvent(new System.Action<object>(this.OnCancelRequest));
  }

  public override void OnShow(UIControler.UIParameter orgParam = null)
  {
    base.OnShow(orgParam);
    this.mRequestBadge.SetActive(false);
    this.mJoinPanel.SetActive(false);
    this.mPagePanel.SetActive(false);
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((_param1, _param2) => this.InitializePage()), true);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  private void InitializePage()
  {
    if (PlayerData.inst.allianceId == 0L)
    {
      this.mJoinPanel.SetActive(true);
      this.mPagePanel.SetActive(false);
    }
    else
    {
      this.mJoinPanel.SetActive(false);
      this.mPagePanel.SetActive(true);
      if (CityManager.inst.GetHighestBuildingLevelFor("marketplace") <= 0)
        return;
      this.mRequestBadge.SetActive(false);
      this.mTabs.onTabSelected += new System.Action<int>(this.OnTabSelected);
      this.mTabs.SetCurrentTab(0, true);
      this.RefreshRequests();
      this.m_OnlineUpdateTime = 5f;
      this.m_ShowDuke = false;
      this.m_ShowBaron = false;
      this.m_ShowKnight = false;
      this.m_ShowVassal = false;
      int num1 = 0;
      int num2 = 0;
      int num3 = 10;
      int num4 = 15;
      GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_food");
      if (data1 != null)
        num1 = data1.ValueInt;
      GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_wood");
      if (data2 != null)
        num2 = data2.ValueInt;
      GameConfigInfo data3 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_ore");
      if (data3 != null)
        num3 = data3.ValueInt;
      GameConfigInfo data4 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_silver");
      if (data4 != null)
        num4 = data4.ValueInt;
      if (PlayerData.inst.CityData.mStronghold.mLevel >= num4)
        this.mRequestType.ShowButton(4);
      else if (PlayerData.inst.CityData.mStronghold.mLevel >= num3)
        this.mRequestType.ShowButton(3);
      else if (PlayerData.inst.CityData.mStronghold.mLevel >= num2)
      {
        this.mRequestType.ShowButton(2);
      }
      else
      {
        if (PlayerData.inst.CityData.mStronghold.mLevel < num1)
          return;
        this.mRequestType.ShowButton(1);
      }
    }
  }

  public void OnJoin()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceJoinDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnReceiveRequest(object data)
  {
    this.RefreshRequests();
  }

  private void OnCancelRequest(object data)
  {
    this.RefreshRequests();
  }

  private void ClearData()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_RequestList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.m_RequestList.Clear();
  }

  public void RefreshRequests()
  {
    this.ClearData();
    int num1 = 0;
    int num2 = 0;
    using (Dictionary<AllianceJobID, AllianceTradeData>.ValueCollection.Enumerator enumerator = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId).trades.datas.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceTradeData current = enumerator.Current;
        long jobId = current.identifier.jobId;
        long uid = current.identifier.uid;
        string resourceClass = current.resourceClass;
        long num3 = current.expireTime - (long) NetServerTime.inst.ServerTimestamp;
        if (num3 > 0L)
        {
          GameObject gameObject = Utils.DuplicateGOB(this.mRequestItem);
          gameObject.SetActive(true);
          this.m_RequestList.Add(gameObject);
          CityManager.ResourceTypes resType = CityManager.ResourceTypes.FOOD;
          string key = resourceClass;
          if (key != null)
          {
            // ISSUE: reference to a compiler-generated field
            if (AllianceTradeDlg.\u003C\u003Ef__switch\u0024map19 == null)
            {
              // ISSUE: reference to a compiler-generated field
              AllianceTradeDlg.\u003C\u003Ef__switch\u0024map19 = new Dictionary<string, int>(3)
              {
                {
                  "wood",
                  0
                },
                {
                  "silver",
                  1
                },
                {
                  "ore",
                  2
                }
              };
            }
            int num4;
            // ISSUE: reference to a compiler-generated field
            if (AllianceTradeDlg.\u003C\u003Ef__switch\u0024map19.TryGetValue(key, out num4))
            {
              switch (num4)
              {
                case 0:
                  resType = CityManager.ResourceTypes.WOOD;
                  break;
                case 1:
                  resType = CityManager.ResourceTypes.SILVER;
                  break;
                case 2:
                  resType = CityManager.ResourceTypes.ORE;
                  break;
              }
            }
          }
          try
          {
            UserData userData = DBManager.inst.DB_User.Get(uid);
            CityData byUid = DBManager.inst.DB_City.GetByUid(uid);
            gameObject.GetComponent<AllianceTradeRequestItem>().SetDetails(jobId, uid, userData.userName, userData.PortraitIconPath, userData.Icon, userData.LordTitle, (int) userData.power, resType, (double) num3, byUid.cityLocation);
          }
          catch
          {
          }
          if (uid == PlayerData.inst.uid)
            ++num1;
          else
            ++num2;
        }
      }
    }
    this.mRequestTable.Reposition();
    this.mRequestScrollView.ResetPosition();
    this.mRequestBoxNormal.SetActive(num1 == 0);
    this.mRequestBoxUsed.SetActive(num1 > 0);
    this.mRequestBadge.SetActive(num2 > 0);
    this.mNoRequests.SetActive(num1 + num2 == 0);
    this.SetRequestBadgeLabel(num2.ToString());
  }

  private void SetRequestBadgeLabel(string text)
  {
    UILabel[] componentsInChildren = this.mRequestBadge.GetComponentsInChildren<UILabel>(true);
    if (componentsInChildren.Length <= 0)
      return;
    componentsInChildren[0].text = text;
  }

  public void OnTabSelected(int index)
  {
    switch (index)
    {
      case 0:
        this.mRequestsPage.SetActive(true);
        this.mMembersPage.SetActive(false);
        this.RefreshRequests();
        break;
      case 1:
        this.mRequestsPage.SetActive(false);
        this.ShowMemberPage();
        break;
    }
  }

  private void ShowMemberPage()
  {
    if (this.mMembersPage.activeSelf)
      return;
    this.mMembersPage.SetActive(true);
    this.mHighLordPanel.gameObject.SetActive(false);
    this.mTempHighLordPanel.gameObject.SetActive(false);
    this.ClearMembers();
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.RefreshMembers();
    }), true);
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnRequestBtnPressed()
  {
    string str = "food";
    switch (this.mRequestType.selection)
    {
      case 0:
        str = "food";
        break;
      case 1:
        str = "wood";
        break;
      case 2:
        str = "ore";
        break;
      case 3:
        str = "silver";
        break;
    }
    MessageHub.inst.GetPortByAction("Alliance:askForTradeHelp").SendRequest(Utils.Hash((object) "class", (object) str), new System.Action<bool, object>(this.RequestCallback), true);
  }

  private void RequestCallback(bool result, object orgData)
  {
    Hashtable data1;
    DatabaseTools.CheckAndParseOrgData(orgData, out data1);
    Debug.Log((object) Utils.Object2Json(orgData));
    if (data1.ContainsKey((object) "job_id"))
    {
      long result1 = 0;
      if (long.TryParse(data1[(object) "job_id"].ToString(), out result1))
      {
        JobHandle job = JobManager.Instance.GetJob(result1);
        Debug.Log((object) ("Job" + Utils.Object2Json(job.Data)));
        if (job != null)
        {
          Hashtable data2 = job.Data as Hashtable;
          if (data2 != null && data2.ContainsKey((object) "class"))
          {
            string res = string.Empty;
            string key = data2[(object) "class"].ToString();
            if (key != null)
            {
              // ISSUE: reference to a compiler-generated field
              if (AllianceTradeDlg.\u003C\u003Ef__switch\u0024map1A == null)
              {
                // ISSUE: reference to a compiler-generated field
                AllianceTradeDlg.\u003C\u003Ef__switch\u0024map1A = new Dictionary<string, int>(4)
                {
                  {
                    "food",
                    0
                  },
                  {
                    "wood",
                    1
                  },
                  {
                    "ore",
                    2
                  },
                  {
                    "silver",
                    3
                  }
                };
              }
              int num;
              // ISSUE: reference to a compiler-generated field
              if (AllianceTradeDlg.\u003C\u003Ef__switch\u0024map1A.TryGetValue(key, out num))
              {
                switch (num)
                {
                  case 0:
                    res = "id_food";
                    break;
                  case 1:
                    res = "id_wood";
                    break;
                  case 2:
                    res = "id_ore";
                    break;
                  case 3:
                    res = "id_silver";
                    break;
                }
              }
            }
            ChatMessageManager.SendTradeMessage(res);
          }
        }
      }
    }
    this.RefreshRequests();
  }

  private void ClearMembers()
  {
    using (List<IOnlineUpdateable>.Enumerator enumerator = this.m_MemberList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IOnlineUpdateable current = enumerator.Current;
        current.gameObject.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_MemberList.Clear();
  }

  private void RefreshMembers()
  {
    this.ClearMembers();
    int totalDukes = 0;
    int totalBarons = 0;
    int totalKnights = 0;
    int totalVassels = 0;
    int totalMembers = 0;
    int onlineDukes = 0;
    int onlineBarons = 0;
    int onlineKnights = 0;
    int onlineVassals = 0;
    int onlineMembers = 0;
    AllianceData alliance = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
    this.mHighLordPanel.SetDetails(alliance.GetHighLord(), new System.Action<AllianceMemberData>(this.SendMemberHelp));
    alliance.GetTempHighLord();
    AllianceUtilities.CalculateMembers(alliance, out totalDukes, out totalBarons, out totalKnights, out totalVassels, out totalMembers, out onlineDukes, out onlineBarons, out onlineKnights, out onlineVassals, out onlineMembers);
    Utils.ExecuteInSecs(0.1f, (System.Action) (() =>
    {
      this.DisplayRank(1, this.DisplayRank(2, this.DisplayRank(3, this.DisplayRank(4, 0.0f, this.m_ShowDuke, onlineDukes, totalDukes), this.m_ShowBaron, onlineBarons, totalBarons), this.m_ShowKnight, onlineKnights, totalKnights), this.m_ShowVassal, onlineVassals, totalVassels);
      this.mMemberTable.Reposition();
      this.mMembersScrollView.ResetPosition();
    }));
  }

  private float DisplayRank(int title, float ypos, bool bExpand, int online, int total)
  {
    if (total == 0)
      return ypos;
    if (!bExpand)
    {
      AllianceMemberHeader component = Utils.DuplicateGOB(this.mMembersCompressed).GetComponent<AllianceMemberHeader>();
      this.m_MemberList.Add((IOnlineUpdateable) component);
      component.SetDetails(PlayerData.inst.allianceData, title, online, total);
      component.gameObject.SetActive(true);
      Utils.SetLPY(component.gameObject, ypos);
      Utils.SetLPX(component.gameObject, 0.0f);
      ypos -= (float) component.GetComponent<UIWidget>().height;
    }
    else
    {
      AllianceMemberHeaderExpanded component = Utils.DuplicateGOB(this.mMembersExpanded).GetComponent<AllianceMemberHeaderExpanded>();
      this.m_MemberList.Add((IOnlineUpdateable) component);
      component.SetDetails(PlayerData.inst.allianceData, title, online, total, new System.Action<AllianceMemberData>(this.SendMemberHelp));
      component.gameObject.SetActive(true);
      Utils.SetLPY(component.gameObject, ypos);
      Utils.SetLPX(component.gameObject, 0.0f);
      ypos -= (float) component.GetComponent<UIWidget>().height;
    }
    return ypos;
  }

  public void OnExpandRank()
  {
    switch (UIButton.current.GetComponent<AllianceMemberHeader>().title)
    {
      case 1:
        this.m_ShowVassal = true;
        break;
      case 2:
        this.m_ShowKnight = true;
        break;
      case 3:
        this.m_ShowBaron = true;
        break;
      case 4:
        this.m_ShowDuke = true;
        break;
    }
    this.RefreshMembers();
  }

  public void OnCollapseRank()
  {
    switch (UIButton.current.transform.parent.gameObject.GetComponent<AllianceMemberHeaderExpanded>().title)
    {
      case 1:
        this.m_ShowVassal = false;
        break;
      case 2:
        this.m_ShowKnight = false;
        break;
      case 3:
        this.m_ShowBaron = false;
        break;
      case 4:
        this.m_ShowDuke = false;
        break;
    }
    this.RefreshMembers();
  }

  private void SendMemberHelp(AllianceMemberData member)
  {
    int level = CityManager.inst.GetHighestBuildingLevelFor("marketplace");
    if (level < 1)
      level = 1;
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData("marketplace", level);
    CityData byUid = DBManager.inst.DB_City.GetByUid(member.uid);
    AllianceSendResources.Parameter parameter = new AllianceSendResources.Parameter();
    parameter.Uid = member.uid;
    float finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(0.0f, "calc_tradetax");
    parameter.TaxPercent = Math.Round((double) finalData, 2);
    parameter.MaxResourceLoad = (long) ConfigManager.inst.DB_BenefitCalc.GetFinalData((int) data.Benefit_Value_1, "calc_trade_capacity");
    parameter.TargetLocation = byUid.cityLocation;
    UIManager.inst.OpenPopup("AllianceSendTradePopup", (Popup.PopupParameter) parameter);
  }
}
