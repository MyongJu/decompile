﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerVault
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class BuildingControllerVault : BuildingControllerNew
{
  private const string prefabPath = "Prefab/City/treasury";
  public Transform vaultRootNode;
  private Treasury treasury;
  private bool requestVaultDataFlag;
  private bool isVaultPanelOpened;
  private bool isVaultBuildingHide;

  private void OnEnable()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
  }

  private void OnDisable()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
    this.isVaultBuildingHide = true;
  }

  public override void InitBuilding()
  {
    base.InitBuilding();
    this.onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
    AssetManager.Instance.LoadAsync("Prefab/City/treasury", (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      if (obj == (UnityEngine.Object) null)
        return;
      this.treasury = Utils.DuplicateGOB(obj as GameObject, this.vaultRootNode).GetComponent<Treasury>();
      this.treasury.onCoinClicked += new System.Action(this.ShowVaultContent);
      this.isVaultBuildingHide = false;
      this.UpdateVaultStatus();
      AssetManager.Instance.UnLoadAsset("Prefab/City/treasury", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    }), (System.Type) null);
  }

  public override void Dispose()
  {
    base.Dispose();
    this.onBuildingSelected -= new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
    this.ClearData();
    this.isVaultBuildingHide = true;
  }

  private void ClearData()
  {
    if ((UnityEngine.Object) this.treasury != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.treasury.gameObject);
    this.treasury = (Treasury) null;
    this.requestVaultDataFlag = false;
    this.isVaultPanelOpened = false;
  }

  private void OnSecondHandler(int time)
  {
    TreasuryData treasuryData = DBManager.inst.DB_Treasury.GetTreasuryData();
    if (treasuryData == null || (UnityEngine.Object) this.treasury == (UnityEngine.Object) null)
      return;
    AnimatorStateInfo animatorStateInfo = this.treasury.doorAnimator.GetCurrentAnimatorStateInfo(0);
    if (treasuryData.SlotId == 0)
    {
      if (animatorStateInfo.IsName("treasury_door_close") || this.isVaultPanelOpened)
        return;
      this.treasury.doorAnimator.Play("treasury_door_close", 0, 0.0f);
    }
    else
    {
      if (!animatorStateInfo.IsName("treasury_door_open"))
        this.treasury.doorAnimator.Play("treasury_door_open", 0, 0.0f);
      if (NetServerTime.inst.ServerTimestamp < treasuryData.EndTime)
        return;
      NGUITools.SetActive(this.treasury.coinEffect, true);
    }
  }

  private void UpdateVaultStatus()
  {
    if (this.requestVaultDataFlag)
      return;
    if ((UnityEngine.Object) this.treasury != (UnityEngine.Object) null)
    {
      NGUITools.SetActive(this.treasury.coinEffect, false);
      NGUITools.SetActive(this.treasury.doorEffect, false);
    }
    VaultFunHelper.Instance.RequestVaultData((System.Action<bool, object>) ((arg1, arg2) =>
    {
      try
      {
        if (!arg1 || this.isVaultBuildingHide)
          return;
        TreasuryData treasuryData = DBManager.inst.DB_Treasury.GetTreasuryData();
        if (treasuryData == null || !((UnityEngine.Object) this.treasury != (UnityEngine.Object) null) || (!this.treasury.gameObject.activeInHierarchy || !((UnityEngine.Object) this.treasury.doorAnimator != (UnityEngine.Object) null)))
          return;
        if (treasuryData.SlotId != 0)
        {
          this.treasury.doorAnimator.Play("treasury_door_open", 0, 0.0f);
          this.StartCoroutine(this.ShowVaultEffect(!((UnityEngine.Object) this.treasury != (UnityEngine.Object) null) ? (GameObject) null : this.treasury.doorEffect, true, 0.35f));
        }
        else
        {
          this.treasury.doorAnimator.Play("treasury_door_close", 0, 0.0f);
          this.StartCoroutine(this.ShowVaultEffect(!((UnityEngine.Object) this.treasury != (UnityEngine.Object) null) ? (GameObject) null : this.treasury.coinEffect, true, 0.0f));
        }
      }
      catch
      {
      }
    }));
    this.requestVaultDataFlag = true;
  }

  private void OnBuildingSelected()
  {
    this.ShowVaultContent();
  }

  private void ShowVaultContent()
  {
    if (!Utils.IsIAPStoreEnabled)
      return;
    this.isVaultPanelOpened = true;
    TreasuryData treasuryData = DBManager.inst.DB_Treasury.GetTreasuryData();
    if (treasuryData != null)
    {
      if (treasuryData.SlotId == 0 && (UnityEngine.Object) this.treasury != (UnityEngine.Object) null && (UnityEngine.Object) this.treasury.doorAnimator != (UnityEngine.Object) null)
      {
        this.treasury.doorAnimator.Play("treasury_door_open", 0, 0.0f);
        AudioManager.Instance.StopAndPlaySound("sfx_city_vault_open");
        this.StartCoroutine(this.ShowVaultEffect(this.treasury.doorEffect, true, 0.35f));
      }
      else
        AudioManager.Instance.StopAndPlaySound("sfx_city_vault_in_use");
    }
    VaultFunHelper.Instance.ShowVault((System.Action) (() =>
    {
      if (treasuryData != null && (UnityEngine.Object) this.treasury != (UnityEngine.Object) null && (UnityEngine.Object) this.treasury.doorAnimator != (UnityEngine.Object) null)
      {
        AnimatorStateInfo animatorStateInfo = this.treasury.doorAnimator.GetCurrentAnimatorStateInfo(0);
        if (treasuryData.SlotId == 0)
        {
          if (!animatorStateInfo.IsName("treasury_door_close"))
          {
            this.treasury.doorAnimator.Play("treasury_door_close", 0, 0.0f);
            AudioManager.Instance.StopAndPlaySound("sfx_city_vault_close");
          }
          this.StartCoroutine(this.ShowVaultEffect(this.treasury.coinEffect, true, 0.0f));
          this.StartCoroutine(this.ShowVaultEffect(this.treasury.doorEffect, false, animatorStateInfo.length - 0.05f));
        }
        else
        {
          if (!animatorStateInfo.IsName("treasury_door_open"))
          {
            this.treasury.doorAnimator.Play("treasury_door_open", 0, 0.0f);
            AudioManager.Instance.StopAndPlaySound("sfx_city_vault_open");
          }
          this.StartCoroutine(this.ShowVaultEffect(this.treasury.doorEffect, true, animatorStateInfo.length - 0.05f));
          this.StartCoroutine(this.ShowVaultEffect(this.treasury.coinEffect, false, 0.0f));
        }
      }
      this.isVaultPanelOpened = false;
    }));
  }

  [DebuggerHidden]
  private IEnumerator ShowVaultEffect(GameObject obj, bool show, float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BuildingControllerVault.\u003CShowVaultEffect\u003Ec__Iterator39()
    {
      delay = delay,
      obj = obj,
      show = show,
      \u003C\u0024\u003Edelay = delay,
      \u003C\u0024\u003Eobj = obj,
      \u003C\u0024\u003Eshow = show
    };
  }
}
