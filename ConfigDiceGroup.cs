﻿// Decompiled with JetBrains decompiler
// Type: ConfigDiceGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDiceGroup
{
  private List<DiceGroupInfo> _diceGroupInfoList = new List<DiceGroupInfo>();
  private Dictionary<string, DiceGroupInfo> _datas;
  private Dictionary<int, DiceGroupInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<DiceGroupInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, DiceGroupInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._diceGroupInfoList.Add(enumerator.Current);
    }
  }

  public DiceGroupInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (DiceGroupInfo) null;
  }

  public DiceGroupInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (DiceGroupInfo) null;
  }

  public List<DiceGroupInfo> GetDiceGroupList()
  {
    return this._diceGroupInfoList;
  }
}
