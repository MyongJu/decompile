﻿// Decompiled with JetBrains decompiler
// Type: SlotLine
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SlotLine : MonoBehaviour
{
  private bool highLighted = true;
  public Vector2 position;

  public override string ToString()
  {
    return this.position.ToString();
  }

  public bool HighLighted
  {
    get
    {
      return this.highLighted;
    }
    set
    {
      if (value == this.highLighted)
        return;
      this.highLighted = value;
      if (this.highLighted)
        GreyUtility.Normal(this.gameObject);
      else
        GreyUtility.Grey(this.gameObject);
    }
  }
}
