﻿// Decompiled with JetBrains decompiler
// Type: GoldBoostResAttachment
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class GoldBoostResAttachment : CityTouchCircleBtnAttachment
{
  private int remainTime;
  public UILabel header;

  public override void SeedData(CityTouchCircleBtnAttachment.AttachmentData ad)
  {
    GoldBoostResAttachment.GBRAData gbraData = ad as GoldBoostResAttachment.GBRAData;
    if (gbraData != null)
      this.header.text = gbraData.content;
    this.remainTime = gbraData.remainTime;
    if (this.remainTime <= 0)
      return;
    this.RefreshTime();
    this.AddEventHandler();
  }

  private void OnDisable()
  {
    if (!GameEngine.IsAvailable)
      return;
    this.RemoveEventHandler();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSeconde);
  }

  private void OnSeconde(int time)
  {
    --this.remainTime;
    this.RefreshTime();
  }

  private void RefreshTime()
  {
    int cost = ItemBag.CalculateCost(this.remainTime, 0);
    string str = "[fdf691]" + cost.ToString() + "[-]";
    if (PlayerData.inst.hostPlayer.Currency < cost)
      str = "[ff0000]" + cost.ToString() + "[-]";
    this.header.text = str;
    if (cost != 0)
      return;
    this.enabled = false;
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSeconde);
  }

  public class GBRAData : CityTouchCircleBtnAttachment.AttachmentData
  {
    public string content;
    public int remainTime;
  }
}
