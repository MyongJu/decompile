﻿// Decompiled with JetBrains decompiler
// Type: LegendListDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class LegendListDlg : UI.Dialog
{
  private int buyprice = 30;
  private GameObjectPool m_ItemPool = new GameObjectPool();
  private List<LegendItem> lstItemUI = new List<LegendItem>();
  public UIGrid grid;
  public UIScrollView scroll;
  public GameObject itemTemplate;
  public GameObject itemRoot;
  public UILabel title;
  public UILabel ownLegend;
  public UILabel totalLegend;
  private bool m_Initialized;
  private int totalNum;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (!this.m_Initialized)
    {
      this.m_ItemPool.Initialize(this.itemTemplate, this.grid.gameObject);
      this.m_Initialized = true;
    }
    this.Refresh();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Clear();
  }

  public void GetMoreHeroes()
  {
    if (this.uiManagerPanel.BT_Back.gameObject.activeSelf)
      UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
    else
      UIManager.inst.OpenDlg("Legend/LegendRecruitmentDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnLegendItemClick(long legendId)
  {
    LegendData data = DBManager.inst.DB_Legend.Datas[legendId];
    UIManager.inst.OpenDlg("Legend/LegendDetailDlg", (UI.Dialog.DialogParameter) new LegendDetailDlg.LegendDetailDlgParameter()
    {
      legendData = data
    }, true, true, true);
  }

  private void ResultHandler(bool success, object result)
  {
    if (!success)
      return;
    DBManager.inst.DB_Legend.UpdateDatas(result, (long) NetServerTime.inst.ServerTimestamp);
    this.Refresh();
  }

  private void Clear()
  {
    for (int index = 0; index < this.lstItemUI.Count; ++index)
    {
      this.lstItemUI[index].Clear();
      this.m_ItemPool.Release(this.lstItemUI[index].gameObject);
    }
    this.lstItemUI.Clear();
  }

  private void Refresh()
  {
    this.Clear();
    List<LegendData> underRecruitLegends = DBManager.inst.DB_Legend.GetTotalUnderRecruitLegends();
    this.title.text = ScriptLocalization.Get("hero_altar_uppercase_hero_chamber", true);
    int count = underRecruitLegends.Count;
    underRecruitLegends.Sort((IComparer<LegendData>) new LegendListDlg.LengendSortRule());
    this.totalNum = DBManager.inst.DB_Legend.GetSlotNum();
    if (count > this.totalNum)
      UIManager.inst.toast.Show(ScriptLocalization.Get(string.Format("You have {0} heroes,but only {1} slot!", (object) count, (object) this.totalNum), true), (System.Action) null, 4f, false);
    else if (count == this.totalNum)
      UIManager.inst.toast.Show(ScriptLocalization.Get(ScriptLocalization.Get("toast_hero_slots_full", true), true), (System.Action) null, 4f, false);
    for (int index = 0; index < this.totalNum; ++index)
    {
      GameObject gameObject = this.m_ItemPool.AddChild(this.grid.gameObject);
      gameObject.SetActive(true);
      LegendItem component = gameObject.GetComponent<LegendItem>();
      component.Clear();
      component.HideContent();
      this.lstItemUI.Add(component);
    }
    int num = 0;
    using (List<LegendData>.Enumerator enumerator = underRecruitLegends.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        LegendData current = enumerator.Current;
        if (num < this.totalNum)
        {
          LegendItem legendItem = this.lstItemUI[num++];
          legendItem.LegendClickHandler = new System.Action<long>(this.OnLegendItemClick);
          ConfigManager.inst.DB_Legend.GetLegendInfo(current.LegendID);
          legendItem.SetData((long) current.LegendID);
        }
      }
    }
    this.grid.Reposition();
    this.scroll.ResetPosition();
    this.ownLegend.text = count.ToString();
    this.totalLegend.text = this.totalNum.ToString();
  }

  public void OnAddBtnPressed()
  {
    if (DBManager.inst.DB_Legend.IsMaxSlot())
      UIManager.inst.toast.Show(ScriptLocalization.Get("legend_max_slot", true), (System.Action) null, 4f, false);
    else
      UIManager.inst.OpenPopup("GetMoreLegend", (Popup.PopupParameter) new GetMoreLegendDlg.GetMoreSlotDialogParamer()
      {
        RefreshHandler = new GetMoreLegendDlg.UpdateHandler(this.Refresh)
      });
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    this.Clear();
  }

  public class LengendSortRule : IComparer<LegendData>
  {
    public int Compare(LegendData x, LegendData y)
    {
      return x.Level > y.Level || x.Level >= y.Level && (x.Power > y.Power || x.Power >= y.Power && x.LegendID > y.LegendID) ? -1 : 1;
    }
  }
}
