﻿// Decompiled with JetBrains decompiler
// Type: FallenKnightRankListItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class FallenKnightRankListItemRenderer : MonoBehaviour
{
  public UILabel rankedTimeLabel;
  public FallenKnightRankListItemRenderer.ItemInfo[] subItemInfos;
  private ArrayList rankedList;
  private int rankType;
  private string rankedTime;

  public void SetData(int type, ArrayList rankList, string rankedTime)
  {
    this.rankedList = rankList;
    this.rankType = type;
    this.rankedTime = rankedTime;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.rankedTimeLabel.text = Utils.FormatTimeYYYYMMDD(long.Parse(this.rankedTime), "/");
    if (this.subItemInfos.Length < this.rankedList.Count)
      return;
    string empty = string.Empty;
    for (int index = 0; index < this.rankedList.Count; ++index)
    {
      this.subItemInfos[index].displayedNameLabel.text = string.Empty;
      string str = this.rankedList[index].ToString();
      this.subItemInfos[index].displayedNameLabel.text = !string.IsNullOrEmpty(str) ? str : "~";
      if (!string.IsNullOrEmpty(this.subItemInfos[index].displayedNameLabel.text))
      {
        this.subItemInfos[index].rankNumberLabel.text = "NO." + (object) (index + 1);
        this.subItemInfos[index].rankNumberLabel.transform.parent.gameObject.SetActive(true);
      }
    }
  }

  [Serializable]
  public class ItemInfo
  {
    public UILabel rankNumberLabel;
    public UILabel displayedNameLabel;
  }
}
