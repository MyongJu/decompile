﻿// Decompiled with JetBrains decompiler
// Type: GestureController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class GestureController : MonoBehaviour
{
  private Vector3 _mouseStartPos = new Vector3(0.0f, 0.0f, 0.0f);
  private float _currentScale = 1f;
  private float _fixScale = 1f;
  private int _LevelCheck = -1;
  private List<IGestureHandler> gestureHandlers = new List<IGestureHandler>();
  private bool _LMB;
  private bool _ScaleLMB;
  private float _LMBTimer;
  private bool _clicked;
  private float _mouseScaleVector;
  private float _oldScale;
  private bool _finishedPan;
  private GameObject _tracker;
  private Camera _touchCam;
  public System.Action onFirstPress;
  public System.Action<Vector2, Vector2> onMotion;
  public System.Action onMotionFinished;
  public System.Action<float> onPinchDelta;
  public System.Action<float> onPinchAbsolute;
  private static GestureController _singleton;

  public static GestureController inst
  {
    get
    {
      if ((UnityEngine.Object) GestureController._singleton == (UnityEngine.Object) null)
      {
        GameObject gameObject = new GameObject();
        GestureController._singleton = gameObject.AddComponent<GestureController>();
        gameObject.name = nameof (GestureController);
        UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
      }
      return GestureController._singleton;
    }
  }

  public static void Create()
  {
    GestureController.inst._LMB = GestureController.inst._LMB;
  }

  private void Start()
  {
  }

  public void ResetClick()
  {
    this._clicked = false;
  }

  public void ResetLevelCheck()
  {
    this._LevelCheck = -1;
  }

  public void SetTouchPlane(GameObject tracker, Camera touchCam)
  {
    this._tracker = tracker;
    this._touchCam = touchCam;
  }

  public bool IsLMB()
  {
    return Input.touchCount == 1;
  }

  public Vector3 GetMouseStartPos()
  {
    return this._mouseStartPos;
  }

  public bool WasClicked()
  {
    return this._clicked;
  }

  public void ResetLMBTimer()
  {
    this._LMBTimer = 0.0f;
  }

  public void SetLMBTimer(float value)
  {
    this._LMBTimer = value;
  }

  private void CityMapUpdate()
  {
    if (Input.touchCount == 1 && !this._ScaleLMB)
    {
      Ray ray = this._touchCam.ScreenPointToRay((Vector3) Input.GetTouch(0).position);
      Vector2 vector2 = Vector2.one;
      RaycastHit hitInfo;
      if (Physics.Raycast(ray, out hitInfo))
        vector2 = new Vector2(hitInfo.point.x, hitInfo.point.y);
      if (!this._LMB)
      {
        this._LMB = true;
        this._mouseStartPos.x = vector2.x;
        this._mouseStartPos.y = vector2.y;
        if (this.onFirstPress != null)
          this.onFirstPress();
        this._LMBTimer = 0.0f;
        this._clicked = false;
        this._finishedPan = false;
      }
      if (this.onMotion != null)
        this.onMotion(new Vector2(vector2.x - this._mouseStartPos.x, vector2.y - this._mouseStartPos.y), vector2);
      this._mouseStartPos.x = vector2.x;
      this._mouseStartPos.y = vector2.y;
      this._LMBTimer += Time.deltaTime;
    }
    else if (Input.touchCount == 2 && !this._LMB)
    {
      if (!this._ScaleLMB)
      {
        this._ScaleLMB = true;
        this._mouseScaleVector = (Input.GetTouch(1).position - Input.GetTouch(0).position).sqrMagnitude;
        this._oldScale = this._currentScale;
      }
      float sqrMagnitude = (Input.GetTouch(1).position - Input.GetTouch(0).position).sqrMagnitude;
      if (this.onPinchDelta != null)
        this.onPinchDelta((float) (-((double) sqrMagnitude - (double) this._mouseScaleVector) / 1000.0));
      this._mouseScaleVector = sqrMagnitude;
    }
    else
    {
      if (!this._finishedPan)
      {
        if (this.onMotionFinished != null)
          this.onMotionFinished();
        this._finishedPan = true;
      }
      if (this._LMB && (double) this._LMBTimer < 0.5)
        this._clicked = true;
      if (this._ScaleLMB)
        this._currentScale = this._fixScale;
      this._LMB = false;
      this._ScaleLMB = false;
    }
  }

  private void Update()
  {
    if ((UnityEngine.Object) this._tracker == (UnityEngine.Object) null)
      return;
    if (this._LevelCheck < 0)
      this._LevelCheck = !((UnityEngine.Object) Utils.FindClass<CitadelSystem>() != (UnityEngine.Object) null) ? 0 : 1;
    if (this._LevelCheck == 1)
      this.CityMapUpdate();
    else if (Input.touchCount == 1 && !this._ScaleLMB)
    {
      if (!this._LMB)
      {
        this._LMB = true;
        this._mouseStartPos = (Vector3) Input.GetTouch(0).position;
        if (this.onFirstPress != null)
          this.onFirstPress();
        this._LMBTimer = 0.0f;
        this._clicked = false;
      }
      if (this.onMotion != null)
        this.onMotion(new Vector2(Input.GetTouch(0).position.x - this._mouseStartPos.x, Input.GetTouch(0).position.y - this._mouseStartPos.y), (Vector2) this._mouseStartPos);
      this._LMBTimer += Time.deltaTime;
    }
    else if (Input.touchCount == 2 && !this._LMB)
    {
      if (!this._ScaleLMB)
      {
        this._ScaleLMB = true;
        this._mouseScaleVector = (Input.GetTouch(1).position - Input.GetTouch(0).position).sqrMagnitude;
        this._oldScale = this._currentScale;
      }
      float sqrMagnitude = (Input.GetTouch(1).position - Input.GetTouch(0).position).sqrMagnitude;
      if (this.onPinchDelta != null)
        this.onPinchDelta((float) (((double) sqrMagnitude - (double) this._mouseScaleVector) / 100000.0));
      this._mouseScaleVector = sqrMagnitude;
    }
    else
    {
      if (this._LMB && (double) this._LMBTimer < 0.5)
        this._clicked = true;
      if (this._ScaleLMB)
        this._currentScale = this._fixScale;
      this._LMB = false;
      this._ScaleLMB = false;
    }
  }

  public void RegisterGestureHandler(IGestureHandler handler)
  {
    if (handler == null || this.gestureHandlers.Contains(handler))
      return;
    this.gestureHandlers.Add(handler);
  }

  public void ClearAllGestureHandler()
  {
    this.gestureHandlers.Clear();
    this.onFirstPress = (System.Action) null;
    this.onMotion = (System.Action<Vector2, Vector2>) null;
    this.onPinchDelta = (System.Action<float>) null;
    this.onPinchAbsolute = (System.Action<float>) null;
    this.onMotionFinished = (System.Action) null;
  }

  public bool HandleGesture(Vector2 param1, Vector2 param2)
  {
    bool flag = false;
    using (List<IGestureHandler>.Enumerator enumerator = this.gestureHandlers.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.HandlePan(param1, param2))
        {
          flag = true;
          break;
        }
      }
    }
    return flag;
  }
}
