﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillPropertyChangeItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DragonSkillPropertyChangeItem : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelPropertyName;
  [SerializeField]
  private UILabel _labelCurrentValue;
  [SerializeField]
  private GameObject _rootCanLevelUp;
  [SerializeField]
  private UILabel _labelNextValue;

  public void SetData(Benefits.BenefitValuePair current, Benefits.BenefitValuePair next, bool canLevelUp)
  {
    if (current == null && next == null)
    {
      Logger.Error((object) "invalid inpout parameter , both null");
    }
    else
    {
      this._rootCanLevelUp.SetActive(canLevelUp);
      PropertyDefinition propertyDefinition = current == null ? next.propertyDefinition : current.propertyDefinition;
      this._labelPropertyName.text = propertyDefinition.Name;
      float num1 = current == null ? 0.0f : current.value;
      float num2 = next == null ? 0.0f : next.value;
      this._labelCurrentValue.text = propertyDefinition.ConvertToDisplayString((double) num1, false, true);
      this._labelNextValue.text = propertyDefinition.ConvertToDisplayString((double) num2, false, true);
    }
  }
}
