﻿// Decompiled with JetBrains decompiler
// Type: RedDotTipItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RedDotTipItem : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelCount;

  public void SetTipCount(int tipCount)
  {
    this.gameObject.SetActive(tipCount > 0);
    this._labelCount.text = tipCount.ToString();
  }
}
