﻿// Decompiled with JetBrains decompiler
// Type: GameObjectCommands
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Reflection;
using UnityEngine;

public static class GameObjectCommands
{
  [ConsoleCommand("object list", "lists all the game objects in the scene", true)]
  public static void ListGameObjects()
  {
    foreach (Object @object in Object.FindObjectsOfType(typeof (GameObject)))
      Console.Log(@object.name);
  }

  [ConsoleCommand("object print", "lists properties of the object", true)]
  public static void PrintGameObject(string[] args)
  {
    if (args.Length < 1)
    {
      Console.Log("expected : object print <Object Name>");
    }
    else
    {
      GameObject gameObject = GameObject.Find(args[0]);
      if ((Object) gameObject == (Object) null)
      {
        Console.Log("GameObject not found : " + args[0]);
      }
      else
      {
        Console.Log("Game Object : " + gameObject.name);
        foreach (Component component in gameObject.GetComponents(typeof (Component)))
        {
          Console.Log("  Component : " + (object) ((object) component).GetType());
          foreach (FieldInfo field in ((object) component).GetType().GetFields())
            Console.Log("    " + field.Name + " : " + field.GetValue((object) component));
        }
      }
    }
  }
}
