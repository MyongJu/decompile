﻿// Decompiled with JetBrains decompiler
// Type: ItemStore
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ItemStore : UI.Dialog
{
  public static Color ZeroColor = new Color(1f, 1f, 1f, 1f);
  public static Color NonZeroColor = new Color(0.9294118f, 0.7647059f, 0.01176471f, 1f);
  private string[] SHOPITEM_CATEGORIES = new string[5]
  {
    "general",
    "resources",
    "speed up",
    "combat",
    "treasure"
  };
  private Dictionary<int, List<ShopItemUI>> m_ItemDict = new Dictionary<int, List<ShopItemUI>>();
  private GameObjectPool m_ItemPool = new GameObjectPool();
  private int selectedIndex = -1;
  private const int INVENTORY = 0;
  public UIGrid grid;
  public UIScrollView scroll;
  public GameObject itemTemplate;
  public UILabel goldVal;
  public UIToggle[] locationToggles;
  public UIToggle[] categoryToggles;
  public GameObject itemRoot;
  private bool m_Initialized;

  private void Init()
  {
    if (this.m_Initialized)
      return;
    this.m_ItemPool.Initialize(this.itemTemplate, this.itemRoot);
    this.m_Initialized = true;
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataUpdated);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnItemDataRemoved);
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdate);
    ItemBag.Instance.OnCurrentSaleShopItemUpdate -= new System.Action(this.Refresh);
  }

  public void BuyMoreGold()
  {
    Utils.PopUpCommingSoon();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemDataUpdated);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnItemDataRemoved);
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdate);
    ItemBag.Instance.OnCurrentSaleShopItemUpdate += new System.Action(this.Refresh);
  }

  private void Refresh()
  {
    this.SwitchCategory();
  }

  private void OnItemDataUpdated(int itemId)
  {
    List<ShopItemUI> shopItemUiList = (List<ShopItemUI>) null;
    this.m_ItemDict.TryGetValue(itemId, out shopItemUiList);
    if (shopItemUiList == null)
      return;
    using (List<ShopItemUI>.Enumerator enumerator = shopItemUiList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShopItemUI current = enumerator.Current;
        if (current.ShopStaticInfo != null)
        {
          ShopStaticInfo shopData = ConfigManager.inst.DB_Shop.GetShopData(current.ShopStaticInfo.ID);
          current.SetShopStaticInfo(shopData);
        }
        else
        {
          ConsumableItemData data = DBManager.inst.DB_Item.Get(itemId);
          current.SetItemData(data);
        }
      }
    }
  }

  private void OnItemDataRemoved(int itemId)
  {
    if (this.GetLocationIndex() == 0)
    {
      List<ShopItemUI> shopItemUiList = (List<ShopItemUI>) null;
      this.m_ItemDict.TryGetValue(itemId, out shopItemUiList);
      if (shopItemUiList != null)
      {
        using (List<ShopItemUI>.Enumerator enumerator = shopItemUiList.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            ShopItemUI current = enumerator.Current;
            current.gameObject.SetActive(false);
            current.Clear();
            this.m_ItemPool.Release(current.gameObject);
          }
        }
      }
      this.grid.Reposition();
    }
    else
      this.OnItemDataUpdated(itemId);
  }

  public void OnCloseButtonClick()
  {
    if (this.GetLocationIndex() == 0)
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    else
      UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
    this.ClearData();
    this.RemoveEventHandler();
  }

  private void SwitchCategory()
  {
    int locationIndex = this.GetLocationIndex();
    int categoryIndex = this.GetCategoryIndex();
    string category = this.SHOPITEM_CATEGORIES[categoryIndex];
    if (category == "treasure")
    {
      Utils.PopUpCommingSoon();
      this.SetSelectedTroggle();
    }
    else
    {
      this.selectedIndex = categoryIndex;
      this.UpdateUI(category, locationIndex == 0);
    }
  }

  private void SetButtonLabelColor(GameObject go, Color color)
  {
    UILabel componentInChildren = go.GetComponentInChildren<UILabel>();
    if (!((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null))
      return;
    componentInChildren.color = color;
  }

  private void SetSelectedTroggle()
  {
    Color color1 = new Color(0.8117647f, 0.8117647f, 0.8117647f, 1f);
    Color color2 = new Color(0.6196079f, 0.6196079f, 0.6196079f, 1f);
    for (int index = 0; index < this.categoryToggles.Length; ++index)
    {
      this.categoryToggles[index].Set(false);
      this.SetButtonLabelColor(this.categoryToggles[index].gameObject, color2);
    }
    if (this.categoryToggles.Length <= this.selectedIndex || this.selectedIndex <= -1)
      return;
    this.categoryToggles[this.selectedIndex].Set(true);
    this.SetButtonLabelColor(this.categoryToggles[this.selectedIndex].gameObject, color1);
  }

  private void UpdateUI(string category, bool owned)
  {
    this.ClearData();
    this.scroll.gameObject.SetActive(false);
    DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid);
    List<ShopStaticInfo> currentSaleShopItems = ItemBag.Instance.GetCurrentSaleShopItems("shop", category);
    currentSaleShopItems.Sort(new Comparison<ShopStaticInfo>(this.CompareShopItem));
    using (List<ShopStaticInfo>.Enumerator enumerator = currentSaleShopItems.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShopStaticInfo current = enumerator.Current;
        int shopStaticQuanlity = ItemBag.Instance.GetShopStaticQuanlity(current.ID);
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(current.Item_InternalId);
        if (!owned || shopStaticQuanlity > 0)
        {
          GameObject gameObject = this.m_ItemPool.AddChild(this.grid.gameObject);
          gameObject.SetActive(true);
          ShopItemUI component = gameObject.GetComponent<ShopItemUI>();
          component.SetShopStaticInfo(current);
          component.onItemClicked = new System.Action<ShopItemUI>(this.OnItemClicked);
          component.SwitchLocation(!owned);
          UIDragScrollView[] componentsInChildren = gameObject.GetComponentsInChildren<UIDragScrollView>(true);
          int length = componentsInChildren.Length;
          for (int index = 0; index < length; ++index)
            componentsInChildren[index].scrollView = this.scroll;
          List<ShopItemUI> shopItemUiList = (List<ShopItemUI>) null;
          if (this.m_ItemDict.TryGetValue(itemStaticInfo.internalId, out shopItemUiList))
            shopItemUiList.Add(component);
          if (shopItemUiList == null)
          {
            shopItemUiList = new List<ShopItemUI>();
            this.m_ItemDict.Add(itemStaticInfo.internalId, shopItemUiList);
            shopItemUiList.Add(component);
          }
        }
      }
    }
    if (owned)
    {
      List<ConsumableItemData> itemsNoInShop = ItemBag.Instance.GetItemsNoInShop(ItemBag.ItemType.Invalid);
      for (int index1 = 0; index1 < itemsNoInShop.Count; ++index1)
      {
        ConsumableItemData data = itemsNoInShop[index1];
        if (!(ConfigManager.inst.DB_Items.GetItem(data.internalId).Category != category))
        {
          GameObject gameObject = this.m_ItemPool.AddChild(this.grid.gameObject);
          gameObject.SetActive(true);
          ShopItemUI component = gameObject.GetComponent<ShopItemUI>();
          component.SetItemData(data);
          component.SwitchLocation(!owned);
          UIDragScrollView[] componentsInChildren = gameObject.GetComponentsInChildren<UIDragScrollView>(true);
          int length = componentsInChildren.Length;
          for (int index2 = 0; index2 < length; ++index2)
            componentsInChildren[index2].scrollView = this.scroll;
          List<ShopItemUI> shopItemUiList = (List<ShopItemUI>) null;
          if (this.m_ItemDict.TryGetValue(data.internalId, out shopItemUiList))
            shopItemUiList.Add(component);
          if (shopItemUiList == null)
          {
            shopItemUiList = new List<ShopItemUI>();
            this.m_ItemDict.Add(data.internalId, shopItemUiList);
            shopItemUiList.Add(component);
          }
        }
      }
    }
    Utils.ExecuteInSecs(0.1f, (System.Action) (() =>
    {
      this.scroll.gameObject.SetActive(true);
      this.grid.Reposition();
      this.scroll.ResetPosition();
    }));
  }

  private int CompareShopItem(ShopStaticInfo a, ShopStaticInfo b)
  {
    if (a.Priority == b.Priority)
      return 0;
    return a.Priority > b.Priority ? 1 : -1;
  }

  private void OnItemClicked(ShopItemUI itemUI)
  {
    if (itemUI.GetCategory().ToString().Equals("treasure") || this.GetLocationIndex() == 0)
      return;
    UIManager.inst.OpenPopup("BuyAndUsePopup", (Popup.PopupParameter) new StoreBuyAndUseDialog.StoreBuyAndUseDialogParamer()
    {
      shopStaticInfo = itemUI.ShopStaticInfo
    });
  }

  private void ClearData()
  {
    using (Dictionary<int, List<ShopItemUI>>.ValueCollection.Enumerator enumerator1 = this.m_ItemDict.Values.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (List<ShopItemUI>.Enumerator enumerator2 = enumerator1.Current.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            ShopItemUI current = enumerator2.Current;
            current.gameObject.SetActive(false);
            current.Clear();
            this.m_ItemPool.Release(current.gameObject);
          }
        }
      }
    }
    this.m_ItemDict.Clear();
  }

  private void SetSelectIndex(int index)
  {
    foreach (UIToggle locationToggle in this.locationToggles)
      locationToggle.Set(false);
    foreach (UIToggle categoryToggle in this.categoryToggles)
      categoryToggle.Set(false);
    this.locationToggles[0].value = true;
    this.categoryToggles[index].value = true;
    this.SwitchCategory();
  }

  private int GetLocationIndex()
  {
    Color color1 = new Color(0.8823529f, 0.8431373f, 0.8088889f, 1f);
    Color color2 = new Color(0.5529412f, 0.5254902f, 0.454902f, 1f);
    int num = 0;
    for (int index = 0; index < this.locationToggles.Length; ++index)
    {
      if (this.locationToggles[index].value)
      {
        num = index;
        this.SetButtonLabelColor(this.locationToggles[index].gameObject, color1);
      }
      else
        this.SetButtonLabelColor(this.locationToggles[index].gameObject, color2);
    }
    return num;
  }

  private int GetCategoryIndex()
  {
    Color color1 = new Color(0.8117647f, 0.8117647f, 0.8117647f, 1f);
    Color color2 = new Color(0.6196079f, 0.6196079f, 0.6196079f, 1f);
    int num = 0;
    for (int index = 0; index < this.categoryToggles.Length; ++index)
    {
      Color color3;
      if (this.categoryToggles[index].value)
      {
        num = index;
        color3 = color1;
      }
      else
        color3 = color2;
      this.SetButtonLabelColor(this.categoryToggles[index].gameObject, color3);
    }
    return num;
  }

  private void OnUserDataUpdate(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.goldVal.text = Utils.FormatThousands(DBManager.inst.DB_User.Get(PlayerData.inst.uid).currency.gold.ToString());
  }

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
    this.Init();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.AddEventHandler();
    ItemStore.Parameter parameter = orgParam as ItemStore.Parameter;
    ItemBag.Instance.StartSyncTime();
    this.OnUserDataUpdate(PlayerData.inst.uid);
    if (parameter != null)
      this.SetSelectIndex(parameter.index);
    else
      this.SetSelectIndex(0);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
    this.m_ItemPool.Clear();
    this.RemoveEventHandler();
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.OnUserDataUpdate(PlayerData.inst.uid);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    foreach (UIToggle locationToggle in this.locationToggles)
      locationToggle.value = false;
    foreach (UIToggle categoryToggle in this.categoryToggles)
      categoryToggle.value = false;
    ItemBag.Instance.StopSyncTime();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int index;
  }
}
