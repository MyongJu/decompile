﻿// Decompiled with JetBrains decompiler
// Type: ConfigParliamentHero
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigParliamentHero
{
  private List<ParliamentHeroInfo> _parliamentHeroInfoList = new List<ParliamentHeroInfo>();
  private Dictionary<int, List<ParliamentHeroInfo>> _parliamentHerosBySuit = new Dictionary<int, List<ParliamentHeroInfo>>();
  private List<ParliamentHeroInfo> _allParliamentHeroInfoOrderByQuality = new List<ParliamentHeroInfo>();
  private Dictionary<string, ParliamentHeroInfo> _datas;
  private Dictionary<int, ParliamentHeroInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ParliamentHeroInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, ParliamentHeroInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null)
      {
        if (!string.IsNullOrEmpty(enumerator.Current.id))
          this._parliamentHeroInfoList.Add(enumerator.Current);
        if (!this._parliamentHerosBySuit.ContainsKey(enumerator.Current.heroSuitGroup))
          this._parliamentHerosBySuit.Add(enumerator.Current.heroSuitGroup, new List<ParliamentHeroInfo>()
          {
            enumerator.Current
          });
        else
          this._parliamentHerosBySuit[enumerator.Current.heroSuitGroup].Add(enumerator.Current);
      }
    }
    this._parliamentHeroInfoList.Sort(new Comparison<ParliamentHeroInfo>(this.SortById));
    this._allParliamentHeroInfoOrderByQuality.AddRange((IEnumerable<ParliamentHeroInfo>) this._parliamentHeroInfoList);
    this._allParliamentHeroInfoOrderByQuality.Sort((Comparison<ParliamentHeroInfo>) ((x, y) =>
    {
      if (x.quality != y.quality)
        return y.quality.CompareTo(x.quality);
      return x.priority.CompareTo(y.priority);
    }));
  }

  public List<ParliamentHeroInfo> GetParliamentHeroInfoListOrderByQuality()
  {
    return this._allParliamentHeroInfoOrderByQuality;
  }

  public List<ParliamentHeroInfo> GetParliamentHeroInfoList()
  {
    this._parliamentHeroInfoList.Sort((Comparison<ParliamentHeroInfo>) ((a, b) => a.internalId.CompareTo(b.internalId)));
    this._parliamentHeroInfoList.Sort(new Comparison<ParliamentHeroInfo>(this.SortById));
    return this._parliamentHeroInfoList;
  }

  public ParliamentHeroInfo GetByChipId(int chipId)
  {
    Dictionary<string, ParliamentHeroInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.chipItem == chipId)
        return enumerator.Current;
    }
    return (ParliamentHeroInfo) null;
  }

  public ParliamentHeroInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (ParliamentHeroInfo) null;
  }

  public ParliamentHeroInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (ParliamentHeroInfo) null;
  }

  public List<ParliamentHeroInfo> GetParliamentHerosBySuit(int suitGroupId)
  {
    if (this._parliamentHerosBySuit.ContainsKey(suitGroupId))
      return this._parliamentHerosBySuit[suitGroupId];
    return (List<ParliamentHeroInfo>) null;
  }

  private int SortById(ParliamentHeroInfo heroInfo1, ParliamentHeroInfo heroInfo2)
  {
    LegendCardData legendCardData1 = DBManager.inst.DB_LegendCard.Get(PlayerData.inst.uid, heroInfo1.internalId);
    LegendCardData legendCardData2 = DBManager.inst.DB_LegendCard.Get(PlayerData.inst.uid, heroInfo2.internalId);
    ParliamentHeroQualityInfo parliamentHeroQualityInfo1 = ConfigManager.inst.DB_ParliamentHeroQuality.Get(heroInfo1.quality.ToString());
    ParliamentHeroQualityInfo parliamentHeroQualityInfo2 = ConfigManager.inst.DB_ParliamentHeroQuality.Get(heroInfo2.quality.ToString());
    if (legendCardData1 == null)
      legendCardData1 = new LegendCardData();
    if (legendCardData2 == null)
      legendCardData2 = new LegendCardData();
    if (legendCardData2.PositionAssigned.CompareTo(legendCardData1.PositionAssigned) != 0)
      return legendCardData2.PositionAssigned.CompareTo(legendCardData1.PositionAssigned);
    if ((legendCardData2.LegendId > 0).CompareTo(legendCardData1.LegendId > 0) != 0)
      return (legendCardData2.LegendId > 0).CompareTo(legendCardData1.LegendId > 0);
    if (legendCardData1.LegendId <= 0 && legendCardData2.LegendId <= 0 && (parliamentHeroQualityInfo1 != null && parliamentHeroQualityInfo2 != null) && (heroInfo2.ChipCount >= parliamentHeroQualityInfo2.summonChipsReq).CompareTo(heroInfo1.ChipCount >= parliamentHeroQualityInfo1.summonChipsReq) != 0)
      return (heroInfo2.ChipCount >= parliamentHeroQualityInfo2.summonChipsReq).CompareTo(heroInfo1.ChipCount >= parliamentHeroQualityInfo1.summonChipsReq);
    if (heroInfo1 == null || heroInfo2 == null)
      return 1;
    if (heroInfo2.quality.CompareTo(heroInfo1.quality) != 0)
      return heroInfo2.quality.CompareTo(heroInfo1.quality);
    if (legendCardData2.Level.CompareTo(legendCardData1.Level) != 0)
      return legendCardData2.Level.CompareTo(legendCardData1.Level);
    if (legendCardData2.Star.CompareTo(legendCardData1.Star) != 0)
      return legendCardData2.Star.CompareTo(legendCardData1.Star);
    return heroInfo2.internalId.CompareTo(heroInfo1.internalId);
  }
}
