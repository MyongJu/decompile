﻿// Decompiled with JetBrains decompiler
// Type: ContactListView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ContactListView : MonoBehaviour
{
  private Dictionary<PlayerInfo, MailContactItem> _contactMap = new Dictionary<PlayerInfo, MailContactItem>();
  private Queue<MailContactItem> _itemPool = new Queue<MailContactItem>();
  public UISortableTable table;
  public MailContactItem contactTemplate;
  public UIButton okayButton;
  public UIScrollView scrollView;
  public MailController controller;
  public System.Action<List<string>> onContactListSelected;

  public void Init()
  {
    this.scrollView.ResetPosition();
    HashSet<PlayerInfo>.Enumerator enumerator = this.controller.ContactList().GetEnumerator();
    while (enumerator.MoveNext())
      this.AddContact(enumerator.Current);
    this.okayButton.isEnabled = false;
  }

  public void AddContact(PlayerInfo player)
  {
  }

  public void RemoveContact(PlayerInfo player)
  {
    if (!this._contactMap.ContainsKey(player))
      return;
    MailContactItem contact = this._contactMap[player];
    this._contactMap.Remove(player);
    this._itemPool.Enqueue(contact);
    contact.gameObject.SetActive(false);
    contact.onClick -= new System.Action<string>(this.OnPlayerClick);
    this.table.repositionNow = true;
  }

  public void Show()
  {
    UISortableTable table = this.table;
    table.onReposition = table.onReposition + new UITable.OnReposition(this.OnTableReposition);
    this.gameObject.SetActive(true);
  }

  public void Hide()
  {
    UISortableTable table = this.table;
    table.onReposition = table.onReposition - new UITable.OnReposition(this.OnTableReposition);
    this.onContactListSelected = (System.Action<List<string>>) null;
    this.gameObject.SetActive(false);
    this.ClearScene();
  }

  public void OnOkayClicked()
  {
  }

  private void ClearScene()
  {
    Dictionary<PlayerInfo, MailContactItem>.ValueCollection.Enumerator enumerator = this._contactMap.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      MailContactItem current = enumerator.Current;
      current.gameObject.SetActive(false);
      this._itemPool.Enqueue(current);
    }
    this._contactMap.Clear();
  }

  private void OnTableReposition()
  {
    this.scrollView.ResetPosition();
  }

  private void OnPlayerClick(string userName)
  {
    if (this.onContactListSelected == null)
      return;
    this.onContactListSelected(new List<string>()
    {
      userName
    });
  }

  private MailContactItem GetContactItem()
  {
    if (this._itemPool.Count > 0)
      return this._itemPool.Dequeue();
    return NGUITools.AddChild(this.table.gameObject, this.contactTemplate.gameObject).GetComponent<MailContactItem>();
  }
}
