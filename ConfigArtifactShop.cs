﻿// Decompiled with JetBrains decompiler
// Type: ConfigArtifactShop
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigArtifactShop
{
  private List<ArtifactShopInfo> artifactShopInfoList = new List<ArtifactShopInfo>();
  private Dictionary<string, ArtifactShopInfo> datas;
  private Dictionary<int, ArtifactShopInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ArtifactShopInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    Dictionary<string, ArtifactShopInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this.artifactShopInfoList.Add(enumerator.Current);
    }
    this.artifactShopInfoList.Sort((Comparison<ArtifactShopInfo>) ((a, b) => int.Parse(a.id).CompareTo(int.Parse(b.id))));
  }

  public List<ArtifactShopInfo> GetArtifactShopInfoList()
  {
    return this.artifactShopInfoList.FindAll((Predicate<ArtifactShopInfo>) (x => !x.IsSpecialPriceArtifact));
  }

  public ArtifactShopInfo Get(int interalId)
  {
    if (this.dicByUniqueId.ContainsKey(interalId))
      return this.dicByUniqueId[interalId];
    return (ArtifactShopInfo) null;
  }

  public ArtifactShopInfo Get(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (ArtifactShopInfo) null;
  }
}
