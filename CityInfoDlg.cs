﻿// Decompiled with JetBrains decompiler
// Type: CityInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UI;
using UnityEngine;

public class CityInfoDlg : UI.Dialog
{
  public CityInfoDlg.DetailsCombatPanel mDetailsCombatPanel = new CityInfoDlg.DetailsCombatPanel();
  public CityInfoDlg.DetailsResourcePanel mFoodResourcePanel = new CityInfoDlg.DetailsResourcePanel();
  public CityInfoDlg.DetailsResourcePanel mSilverResourcePanel = new CityInfoDlg.DetailsResourcePanel();
  public CityInfoDlg.DetailsResourcePanel mWoodResourcePanel = new CityInfoDlg.DetailsResourcePanel();
  public CityInfoDlg.DetailsResourcePanel mOreResourcePanel = new CityInfoDlg.DetailsResourcePanel();
  public GameObject mCombatPanel;
  public GameObject mFoodPanel;
  public GameObject mWoodPanel;
  public GameObject mSilverPanel;
  public GameObject mOrePanel;
  public UILabel mStrongholdLevel;
  public UIWidget mIcon;
  public float zoomFocusDis;

  public void Show()
  {
    CitadelSystem.inst.FocusConstructionBuilding(BuildingController.mSelectedBuildingController.gameObject, this.mIcon, this.zoomFocusDis);
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    int buildingLevelFor = CityManager.inst.GetHighestBuildingLevelFor("stronghold");
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData("stronghold", buildingLevelFor);
    this.mCombatPanel.GetComponent<LabelBinder>().SetLabels((object) Utils.FormatThousands(DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).totalTroops.totalTroopsCount.ToString()), (object) data.Benefit_Value_2.ToString(), (object) data.Benefit_Value_1.ToString(), (object) cityData.totalTroops.totalLoad.ToString());
    this.mFoodPanel.GetComponent<LabelBinder>().SetLabels((object) Utils.FormatThousands(CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.FOOD).ToString()), (object) Utils.FormatThousands(cityData.totalTroops.totalUpkeep.ToString()), (object) Utils.FormatThousands(((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD)).ToString()), (object) Utils.FormatThousands(CityManager.inst.GetTotalCapacityForResourceOfType(CityManager.ResourceTypes.FOOD).ToString()));
    this.mWoodPanel.GetComponent<LabelBinder>().SetLabels((object) Utils.FormatThousands(CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.WOOD).ToString()), (object) Utils.FormatThousands(((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD)).ToString()), (object) Utils.FormatThousands(CityManager.inst.GetTotalCapacityForResourceOfType(CityManager.ResourceTypes.WOOD).ToString()));
    this.mSilverPanel.GetComponent<LabelBinder>().SetLabels((object) Utils.FormatThousands(CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.SILVER).ToString()), (object) Utils.FormatThousands(((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER)).ToString()), (object) Utils.FormatThousands(CityManager.inst.GetTotalCapacityForResourceOfType(CityManager.ResourceTypes.SILVER).ToString()));
    this.mOrePanel.GetComponent<LabelBinder>().SetLabels((object) Utils.FormatThousands(CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.ORE).ToString()), (object) Utils.FormatThousands(((long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE)).ToString()), (object) Utils.FormatThousands(CityManager.inst.GetTotalCapacityForResourceOfType(CityManager.ResourceTypes.ORE).ToString()));
    this.mStrongholdLevel.text = string.Format("LV. {0}[aaaaaa]/30", (object) buildingLevelFor);
  }

  public void OnCombatPanelPressed()
  {
    UIManager.inst.OpenPopup("CityInfoDetail", (Popup.PopupParameter) new CityInfoDetailDlg.Parameter()
    {
      type = CityInfoDetailDlg.DLGType.combat
    });
  }

  public void OnFoodPanelPressed()
  {
    UIManager.inst.OpenPopup("CityInfoDetail", (Popup.PopupParameter) new CityInfoDetailDlg.Parameter()
    {
      type = CityInfoDetailDlg.DLGType.food
    });
  }

  public void OnWoodPanelPressed()
  {
    UIManager.inst.OpenPopup("CityInfoDetail", (Popup.PopupParameter) new CityInfoDetailDlg.Parameter()
    {
      type = CityInfoDetailDlg.DLGType.wood
    });
  }

  public void OnSilverPanelPressed()
  {
    UIManager.inst.OpenPopup("CityInfoDetail", (Popup.PopupParameter) new CityInfoDetailDlg.Parameter()
    {
      type = CityInfoDetailDlg.DLGType.silver
    });
  }

  public void OnOrePanelPressed()
  {
    UIManager.inst.OpenPopup("CityInfoDetail", (Popup.PopupParameter) new CityInfoDetailDlg.Parameter()
    {
      type = CityInfoDetailDlg.DLGType.ore
    });
  }

  public void OnBackBtnPressed()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void OnResourcesUpdated(long food, long wood, long silver, long ore, long gold)
  {
    this.Show();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Show();
    CityManager.inst.onResourcesUpdated += new CityManager.OnResourcesUpdated(this.OnResourcesUpdated);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    CityManager.inst.onResourcesUpdated -= new CityManager.OnResourcesUpdated(this.OnResourcesUpdated);
  }

  [Serializable]
  public class DetailsCombatPanel
  {
    public UILabel mTotalTroops;
    public UILabel mTotalMarches;
    public UILabel mTroopsPerMarch;
    public UILabel mMaxResourceLoad;
  }

  [Serializable]
  public class DetailsResourcePanel
  {
    public UILabel mOverallIncome;
    public UILabel mUpKeep;
    public UILabel mYouOwn;
    public UILabel mCityCapacity;
  }
}
