﻿// Decompiled with JetBrains decompiler
// Type: GuardActionSFX
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UnityEngine;

public class GuardActionSFX : GuardAction
{
  public string URI;

  public override void Decode(GameObject go, object orgData)
  {
    base.Decode(go, orgData);
    DatabaseTools.UpdateData(orgData as Hashtable, "URI", ref this.URI);
  }

  public override void Process(RoundPlayer player)
  {
    base.Process(player);
    if (!Application.isPlaying || !((Object) null != (Object) DragonKnightSystem.Instance.Controller) || player == null)
      return;
    DragonKnightSystem.Instance.Controller.BattleAudio.Play(this.URI, false);
  }
}
