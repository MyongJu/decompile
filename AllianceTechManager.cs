﻿// Decompiled with JetBrains decompiler
// Type: AllianceTechManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class AllianceTechManager
{
  private Dictionary<int, AllianceTechDonateData> _datas = new Dictionary<int, AllianceTechDonateData>();
  private int _codeDownTime;
  private int _donateTime;
  private bool initialized;

  public int codeDownTime
  {
    get
    {
      return this._codeDownTime;
    }
  }

  public bool canDonate
  {
    get
    {
      return this._donateTime < NetServerTime.inst.ServerTimestamp;
    }
  }

  public void OnGameModeReady()
  {
    if (this.initialized)
      return;
    if (PlayerData.inst.allianceId > 0L)
      RequestManager.inst.SendLoader("AllianceTech:getResearches", (Hashtable) null, (System.Action<bool, object>) null, true, false);
    this.initialized = true;
  }

  public void LoadData()
  {
  }

  public void ResearchTech()
  {
  }

  public void MarkTech()
  {
  }

  public void ServerPush_MarchChanged()
  {
  }

  public void SeverPush_JobComplete()
  {
  }

  public void Decode(object orgData)
  {
    Hashtable data1;
    DatabaseTools.CheckAndParseOrgData(orgData, out data1);
    Hashtable data2;
    if (DatabaseTools.CheckAndParseOrgData(data1[(object) "cool_down"], out data2))
    {
      DatabaseTools.UpdateData(data2, "cd", ref this._codeDownTime);
      DatabaseTools.UpdateData(data2, "can_donation", ref this._donateTime);
    }
    Hashtable data3;
    if (!DatabaseTools.CheckAndParseOrgData(data1[(object) "donate"], out data3))
      return;
    IEnumerator enumerator = data3.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      int index = int.Parse(enumerator.Current.ToString());
      if (this._datas.ContainsKey(index))
      {
        this._datas[index].Decode(index, data3[enumerator.Current]);
      }
      else
      {
        AllianceTechDonateData allianceTechDonateData = new AllianceTechDonateData();
        allianceTechDonateData.Decode(index, data3[enumerator.Current]);
        this._datas.Add(index, allianceTechDonateData);
      }
    }
  }

  public void Update(object orgData)
  {
    this.Decode(orgData);
  }

  public AllianceTechDonateData GetDonateData(int key)
  {
    if (this._datas.ContainsKey(key))
      return this._datas[key];
    return (AllianceTechDonateData) null;
  }

  public struct Param
  {
    public const string COOL_DOWN = "cool_down";
    public const string COOL_DOWN_CD = "cd";
    public const string COOL_DOWN_DONATETIME = "can_donation";
    public const string DONATE = "donate";
  }
}
