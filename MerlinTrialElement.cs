﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialElement
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class MerlinTrialElement : ComponentRenderBase
{
  public GameObject _maskGO;
  public GameObject _selectGO;
  public UILabel _day;
  public UILabel _dayTips;
  public UILabel _countdown;
  public UITexture _imageTexture;
  public UILabel _trialName;
  public UISprite _backgroundColor;
  public GameObject _rank;
  public GameObject _check;
  public GameObject _challenge;
  public GameObject _dailyChest;
  public GameObject _helpGo;
  public UILabel _checkLabel;
  public UILabel _helpLabel;
  public UILabel _rankLable;
  public UILabel _battleLabel;
  private MerlinTrialElementData _elementData;
  private IVfx _vfx;

  public override void Init()
  {
    base.Init();
    this._elementData = this.data as MerlinTrialElementData;
    if (this._elementData == null)
      return;
    this._rankLable.text = Utils.XLAT("trial_ranking");
    this._maskGO.SetActive(!this._elementData.active);
    this._countdown.gameObject.SetActive(this._elementData.active);
    this._rank.SetActive(true);
    this._dailyChest.SetActive(this._elementData.rewardable);
    this._selectGO.SetActive(false);
    this._check.SetActive(false);
    this._challenge.SetActive(false);
    this._day.text = MerlinTrialsHelper.inst.GetDayString(this._elementData.day);
    this._dayTips.text = !this._elementData.active ? string.Empty : string.Format("({0})", (object) Utils.XLAT("trial_active_status"));
    MerlinTrialsGroupInfo byDay = ConfigManager.inst.DB_MerlinTrialsGroup.GetByDay(this._elementData.day);
    BuilderFactory.Instance.HandyBuild((UIWidget) this._imageTexture, byDay.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this._trialName.text = byDay.LocName;
    Color color;
    ColorUtility.TryParseHtmlString(byDay.backgroundColor, out color);
    this._backgroundColor.color = color;
    this.SetSelected(this._elementData.active);
    if (this._elementData.active)
    {
      this._vfx = VfxManager.Instance.Create(byDay.EffectPath);
      NGUITools.SetLayer(this._vfx.gameObject, LayerMask.NameToLayer("NGUI"));
      this._vfx.Play(this.transform);
    }
    if (!this._elementData.active || !Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public override void Dispose()
  {
    base.Dispose();
    if (this._vfx != null)
    {
      VfxManager.Instance.Delete(this._vfx);
      this._vfx = (IVfx) null;
    }
    if (!this._elementData.active || !Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void OnSecond(int serverTime)
  {
    if (MerlinTrialsHelper.inst.Deadline <= 0)
      return;
    this._countdown.text = Utils.FormatTime1(MerlinTrialsHelper.inst.Deadline - serverTime);
  }

  public void OnDetailClick()
  {
    UIManager.inst.OpenPopup("MerlinTrials/TrialDetailsPopup", (Popup.PopupParameter) new MerlinTrialsDetailPopup.Parameter()
    {
      index = this._elementData.day
    });
  }

  public void OnRankClick()
  {
    MerlinTrialsGroupInfo byDay = ConfigManager.inst.DB_MerlinTrialsGroup.GetByDay(this._elementData.day);
    if (byDay == null)
      return;
    UIManager.inst.OpenDlg("MerlinTrials/MerlinTrialsRankDlg", (UI.Dialog.DialogParameter) new MerlinTrialsRankDlg.Parameter()
    {
      rankType = MerlinTrialsRankDlg.RankType.SINGLE,
      groupId = byDay.internalId
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnCheckClick()
  {
    int day1 = this._elementData.day;
    int day2 = MerlinTrialsHelper.inst.Day;
    int num1 = ConfigManager.inst.DB_MerlinTrialsGroup.Day2Group(day1);
    int num2 = ConfigManager.inst.DB_MerlinTrialsGroup.Day2Group(day2);
    if (day1 != day2 && num1 == num2)
      UIManager.inst.toast.Show(Utils.XLAT("toast_same_trial_reopen"), (System.Action) null, 4f, false);
    else
      this.GotoLevelDialog();
  }

  public void OnChallengeClick()
  {
    this.GotoLevelDialog();
  }

  public void OnSelected()
  {
    if (this._elementData.onSelectHandler == null)
      return;
    this._elementData.onSelectHandler(this._elementData.day);
  }

  public int GetIndex()
  {
    return this._elementData.day;
  }

  public bool IsActive()
  {
    return this._elementData.active;
  }

  public void SetSelected(bool selected)
  {
    this._selectGO.SetActive(selected);
    this._helpGo.SetActive(selected);
    if (this._elementData.active)
    {
      this._check.SetActive(false);
      this._challenge.SetActive(selected);
      this._battleLabel.text = Utils.XLAT("trial_set_off_button");
    }
    else
    {
      this._check.SetActive(selected);
      this._challenge.SetActive(false);
    }
    if (!selected)
      return;
    this._helpLabel.text = Utils.XLAT("trial_details_title");
    this._checkLabel.text = Utils.XLAT("trial_challenge_view_button");
  }

  private void GotoLevelDialog()
  {
    UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
    {
      UIManager.inst.Cloud.PokeCloud((System.Action) null);
      UIManager.inst.OpenDlg("MerlinTrials/MelinTrialLevelDlg", (UI.Dialog.DialogParameter) new MerlinTrialsLevelDialog.Parameter()
      {
        day = this._elementData.day
      }, true, true, true);
    }));
  }

  public void OnDailyChestClick()
  {
    if (this._elementData == null)
      return;
    int completeNumbers = MerlinTrialsHelper.inst.GetCompleteNumbers(this._elementData.day);
    MerlinTrialsGroupInfo byDay = ConfigManager.inst.DB_MerlinTrialsGroup.GetByDay(this._elementData.day);
    MerlinTrialsMainInfo merlinTrialsMainInfo = (MerlinTrialsMainInfo) null;
    if (byDay != null)
      merlinTrialsMainInfo = ConfigManager.inst.DB_MerlinTrialsMain.GetByNumber(byDay.internalId, completeNumbers);
    if (merlinTrialsMainInfo == null)
      return;
    UIManager.inst.OpenPopup("MerlinTrials/MerlinTrialsRewardPopup", (Popup.PopupParameter) new MerlinTrialsRewardPopup.Parameter()
    {
      dayOfWeek = this._elementData.day,
      challengeCount = completeNumbers,
      merlinMainInfo = merlinTrialsMainInfo,
      onRewardClaimedSuccess = new System.Action(this.OnDailyRewardClaimed)
    });
  }

  private void OnDailyRewardClaimed()
  {
    NGUITools.SetActive(this._dailyChest, false);
  }
}
