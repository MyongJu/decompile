﻿// Decompiled with JetBrains decompiler
// Type: AllianceRallySlotItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceRallySlotItem : MonoBehaviour
{
  private bool isEmpty = true;
  public UILabel index;
  public GameObject emptyContent;
  public GameObject joinedContent;
  public AllianceRallyEmptySlotItem emtpyItem;
  public AllianceRallyJoinedSlotItem joinItem;
  public System.Action OnOpenCallBackDelegate;
  private int _req_lev;

  public void SetData(int index, long march_id, RallyInfoData data)
  {
    this.index.text = index.ToString();
    this.SetData(march_id, data);
  }

  public void SetData(long march_id, RallyInfoData data)
  {
    NGUITools.SetActive(this.emptyContent, false);
    NGUITools.SetActive(this.joinedContent, true);
    this.emtpyItem.SetData(data);
    this.joinItem.SetData(march_id, data);
    this.joinItem.OnOpenCallBackDelegate = this.OnOpenCallBackDelegate;
    this.isEmpty = false;
  }

  public void Empty(int index, RallyInfoData data)
  {
    this.emtpyItem.SetData(data);
    this.joinItem.Data = data;
    this.joinItem.isLock = false;
    NGUITools.SetActive(this.emptyContent, true);
    NGUITools.SetActive(this.joinedContent, false);
    this.index.text = index.ToString();
    this.emtpyItem.UnLock();
  }

  public void Refresh()
  {
    if (!this.isEmpty)
      this.joinItem.Refresh();
    if (!this.isEmpty)
      return;
    this.emtpyItem.Refresh();
  }

  public void Clear()
  {
    this.OnOpenCallBackDelegate = (System.Action) null;
    NGUITools.SetActive(this.emptyContent, false);
    NGUITools.SetActive(this.joinedContent, false);
    this.joinItem.Clear();
  }

  public void Lock(int index, RallyInfoData data)
  {
    NGUITools.SetActive(this.emptyContent, true);
    NGUITools.SetActive(this.joinedContent, false);
    this.index.text = index.ToString();
    this.joinItem.Data = data;
    this.emtpyItem.Lock();
  }
}
