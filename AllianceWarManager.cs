﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllianceWarManager
{
  public Dictionary<long, AllianceWarManager.WarData> warRallys = new Dictionary<long, AllianceWarManager.WarData>();
  public List<long> warMarchs = new List<long>();
  public List<AllianceWarManager.KnightWarData> knightMarches = new List<AllianceWarManager.KnightWarData>();
  private float _lastLoadListTime = -11f;
  private Dictionary<long, long> _rallyWonderData = new Dictionary<long, long>();
  public List<AllianceWarManager.LogData> logs = new List<AllianceWarManager.LogData>();
  private List<long> _logId = new List<long>();
  private const int _LOAD_LIST_TIME_STEP = 10;
  private bool initialized;
  public System.Action loadDatasCallback;

  public event System.Action OnRefreshRallyDetail;

  public void Init()
  {
  }

  public void Dispose()
  {
  }

  public long GetRallyWonderData(long rallyId)
  {
    if (this._rallyWonderData.ContainsKey(rallyId))
      return this._rallyWonderData[rallyId];
    return -1;
  }

  private void ParseRallyExtraData(Hashtable data)
  {
    this._rallyWonderData.Clear();
    if (!data.ContainsKey((object) "rally_wonder"))
      return;
    Hashtable hashtable = data[(object) "rally_wonder"] as Hashtable;
    if (hashtable == null)
      return;
    IEnumerator enumerator = hashtable.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && hashtable[enumerator.Current] != null)
      {
        string s1 = hashtable[enumerator.Current].ToString();
        string s2 = enumerator.Current.ToString();
        long result1 = -1;
        long result2 = -1;
        long.TryParse(s1, out result1);
        long.TryParse(s2, out result2);
        if (result1 > 0L && result2 > 0L)
        {
          if (this._rallyWonderData.ContainsKey(result2))
            ;
          this._rallyWonderData.Add(result2, result1);
        }
      }
    }
  }

  public void OnGameModeReady()
  {
    if (this.initialized)
      return;
    if (PlayerData.inst.allianceId > 0L)
      RequestManager.inst.SendLoader("Alliance:loadWarList", (Hashtable) null, (System.Action<bool, object>) null, true, false);
    this.initialized = true;
  }

  public void RequestRefreshRallyDetail()
  {
    if (this.OnRefreshRallyDetail == null)
      return;
    this.OnRefreshRallyDetail();
  }

  public void LoadWarDetalData(long rallyId, long alliance_id, System.Action<long> callback = null)
  {
    RequestManager.inst.SendRequest("Alliance:loadWarDetail", Utils.Hash((object) "rally_id", (object) rallyId, (object) nameof (alliance_id), (object) alliance_id), (System.Action<bool, object>) ((result, orgRes) =>
    {
      if (!result)
        return;
      Hashtable data = orgRes as Hashtable;
      this.ParseRallyExtraData(data);
      if (data != null && data.ContainsKey((object) "rally_limit"))
      {
        Hashtable hashtable1 = data[(object) "rally_limit"] as Hashtable;
        if (hashtable1 != null && hashtable1.ContainsKey((object) rallyId.ToString()))
        {
          Hashtable hashtable2 = hashtable1[(object) rallyId.ToString()] as Hashtable;
          if (this.warRallys.ContainsKey(rallyId))
          {
            this.warRallys[rallyId].Decode((object) hashtable2);
          }
          else
          {
            AllianceWarManager.WarData warData = new AllianceWarManager.WarData();
            warData.Decode((object) hashtable2);
            this.warRallys.Add(rallyId, warData);
          }
        }
      }
      if (callback == null)
        return;
      callback(rallyId);
    }), true);
  }

  public bool LoadDatas()
  {
    if ((double) Time.realtimeSinceStartup - (double) this._lastLoadListTime <= 10.0)
      return false;
    this._lastLoadListTime = Time.realtimeSinceStartup;
    RequestManager.inst.SendRequest("Alliance:loadWarList", Utils.Hash((object) "m", (object) this.warMarchs.Count, (object) "r", (object) this.warRallys.Count, (object) "k", (object) this.knightMarches.Count), new System.Action<bool, object>(this.LoadDataCallback), true);
    return true;
  }

  private void LoadDataCallback(bool result, object orgData)
  {
    if (!result)
      return;
    this.UpdateDatas(orgData);
    if (this.loadDatasCallback == null)
      return;
    this.loadDatasCallback();
  }

  private void UpdateDatas(object orgData)
  {
    Hashtable data;
    if (!DatabaseTools.CheckAndParseOrgData(orgData, out data))
      return;
    Hashtable hashtable = data[(object) "rally_limit"] as Hashtable;
    if (hashtable != null)
    {
      this.warRallys.Clear();
      IEnumerator enumerator = hashtable.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        long result;
        if (long.TryParse(enumerator.Current.ToString(), out result) && !this.warRallys.ContainsKey(result))
        {
          AllianceWarManager.WarData warData = new AllianceWarManager.WarData();
          if (warData.Decode(hashtable[enumerator.Current]))
            this.warRallys.Add(result, warData);
        }
      }
    }
    ArrayList arrayList1 = data[(object) "march_list"] as ArrayList;
    if (arrayList1 != null)
    {
      this.warMarchs.Clear();
      for (int index = 0; index < arrayList1.Count; ++index)
        this.warMarchs.Add(long.Parse(arrayList1[index].ToString()));
    }
    this.knightMarches.Clear();
    this.ParseRallyExtraData(data);
    ArrayList arrayList2 = data[(object) "knight_limit"] as ArrayList;
    if (arrayList2 == null)
      return;
    for (int index = 0; index < arrayList2.Count; ++index)
    {
      AllianceWarManager.KnightWarData knightWarData = new AllianceWarManager.KnightWarData();
      knightWarData.Decode(arrayList2[index]);
      this.knightMarches.Add(knightWarData);
    }
  }

  public AllianceWarManager.WarData GetData(long rallyId)
  {
    if (this.warRallys.ContainsKey(rallyId))
      return this.warRallys[rallyId];
    return (AllianceWarManager.WarData) null;
  }

  public AllianceWarManager.KnightWarData GetKnightWarData(long marchId)
  {
    AllianceWarManager.KnightWarData knightWarData = (AllianceWarManager.KnightWarData) null;
    for (int index = 0; index < this.knightMarches.Count; ++index)
    {
      if (this.knightMarches[index].marchId == marchId)
        knightWarData = this.knightMarches[index];
    }
    return knightWarData;
  }

  public long lastLogIndex { get; private set; }

  public void LoadLogs(long lastIndex, System.Action callback)
  {
    RequestManager.inst.SendLoader("Alliance:loadWarLog", Utils.Hash((object) "index", (object) lastIndex), (System.Action<bool, object>) ((result, orgData) =>
    {
      if (!result)
        return;
      if (lastIndex == 0L)
      {
        this.logs.Clear();
        this._logId.Clear();
        this.lastLogIndex = 0L;
      }
      this.UpdateLogs(orgData);
      if (callback == null)
        return;
      callback();
    }), true, false);
  }

  public void UpdateLogs(object orgData)
  {
    Hashtable data1;
    ArrayList data2;
    if (!DatabaseTools.CheckAndParseOrgData(orgData, out data1) || !DatabaseTools.CheckAndParseOrgData(data1[(object) "war_log"], out data2))
      return;
    for (int index = 0; index < data2.Count; ++index)
    {
      AllianceWarManager.LogData logData = new AllianceWarManager.LogData();
      if (logData.Decode(data2[index]) && !this._logId.Contains(logData.index))
      {
        this.logs.Add(logData);
        this._logId.Add(logData.index);
        this.lastLogIndex = logData.index;
      }
    }
  }

  public class WarData
  {
    public long rallyCapacity;
    public int rallyCount;
    public long defenseCapacity;
    public int defenseCount;

    public bool Decode(object orgData)
    {
      Hashtable data;
      if (!DatabaseTools.CheckAndParseOrgData(orgData, out data))
        return false;
      DatabaseTools.UpdateData(data, "rally_limit", ref this.rallyCapacity);
      DatabaseTools.UpdateData(data, "rally_number", ref this.rallyCount);
      DatabaseTools.UpdateData(data, "defense_limit", ref this.defenseCapacity);
      DatabaseTools.UpdateData(data, "defense_number", ref this.defenseCount);
      return true;
    }

    public struct Params
    {
      public const string RALLY_CAPACITY = "rally_limit";
      public const string RALLY_COUNT = "rally_number";
      public const string REINFORCE_CAPACITY = "defense_limit";
      public const string REINFORCE_COUNT = "defense_number";
    }
  }

  public class KnightWarData
  {
    public long marchId;
    public int defenseCount;
    public NPC_Info knightInfo;

    public bool Decode(object orgData)
    {
      Hashtable data;
      if (!DatabaseTools.CheckAndParseOrgData(orgData, out data))
        return false;
      DatabaseTools.UpdateData(data, "march_id", ref this.marchId);
      DatabaseTools.UpdateData(data, "defense_number", ref this.defenseCount);
      MarchData marchData = DBManager.inst.DB_March.Get(this.marchId);
      if (marchData != null)
        this.knightInfo = ConfigManager.inst.DB_NPC.GetDataByUid(marchData.ownerUid);
      return true;
    }

    public struct Params
    {
      public const string MARCH_ID = "march_id";
      public const string REINFORCE_COUNT = "defense_number";
    }
  }

  public class LogData
  {
    public AllianceWarManager.LogData.AllianceWarPlayerData owner = new AllianceWarManager.LogData.AllianceWarPlayerData();
    public AllianceWarManager.LogData.AllianceWarPlayerData target = new AllianceWarManager.LogData.AllianceWarPlayerData();
    public long index;
    public long win_id;
    public int endTime;

    public virtual AllianceWarManager.LogData.AllianceWarPlayerData ally
    {
      get
      {
        if (this.owner.allianceId == PlayerData.inst.allianceId)
          return this.owner;
        return this.target;
      }
    }

    public virtual AllianceWarManager.LogData.AllianceWarPlayerData enemy
    {
      get
      {
        if (this.owner.allianceId != PlayerData.inst.allianceId)
          return this.owner;
        return this.target;
      }
    }

    public virtual bool Decode(object orgData)
    {
      Hashtable data;
      if (!DatabaseTools.CheckAndParseOrgData(orgData, out data))
        return false;
      this.owner.Decode(data[(object) "owner"]);
      this.target.Decode(data[(object) "target"]);
      DatabaseTools.UpdateData(data, "id", ref this.index);
      DatabaseTools.UpdateData(data, "win_id", ref this.win_id);
      DatabaseTools.UpdateData(data, "end_time", ref this.endTime);
      return true;
    }

    public virtual bool isAllyWin
    {
      get
      {
        if (this.owner.allianceId == PlayerData.inst.allianceId)
          return this.owner.uid == this.win_id;
        return this.target.uid == this.win_id;
      }
    }

    public virtual bool isStarter
    {
      get
      {
        return this.owner.allianceId == PlayerData.inst.allianceId;
      }
    }

    public struct Params
    {
      public const string OWNER = "owner";
      public const string TARGET = "target";
      public const string INDEX = "id";
      public const string WIN_ID = "win_id";
      public const string END_TIME = "end_time";
    }

    public class AllianceWarPlayerData
    {
      public int k = -1;
      public long uid;
      public string name;
      public long allianceId;
      public string allianceName;
      public string allianceTag;

      public bool Decode(object orgData)
      {
        Hashtable data;
        if (!DatabaseTools.CheckAndParseOrgData(orgData, out data))
          return false;
        DatabaseTools.UpdateData(data, "uid", ref this.uid);
        DatabaseTools.UpdateData(data, "uname", ref this.name);
        DatabaseTools.UpdateData(data, "aid", ref this.allianceId);
        DatabaseTools.UpdateData(data, "aname", ref this.allianceName);
        DatabaseTools.UpdateData(data, "atag", ref this.allianceTag);
        DatabaseTools.UpdateData(data, "k", ref this.k);
        return true;
      }

      public struct Params
      {
        public const string USER_ID = "uid";
        public const string USER_NAME = "uname";
        public const string ALLIANCE_ID = "aid";
        public const string ALLIANCE_NAME = "aname";
        public const string ALLIANCE_TAG = "atag";
        public const string KID = "k";
      }
    }
  }
}
