﻿// Decompiled with JetBrains decompiler
// Type: LegendRecruitment
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class LegendRecruitment : UI.Dialog
{
  public static int fakeMark = 100;
  private Dictionary<int, LegendCard.Data> legends = new Dictionary<int, LegendCard.Data>();
  private List<GameObject> lcPool = new List<GameObject>();
  private int greyLevel = -1;
  private int nplevel = -1;
  public const string markPic = "Texture/Legend/Image/card_hero_locked";
  public UIScrollView sv;
  public UIWrapContent wc;
  public LegendCard lcTemplate;
  public UILabel title;
  public UILabel curLvl;
  public GameObject content;
  public GameObject noherohint;
  private int pivot;
  private bool greyPreview;
  private bool noPreview;
  private int specialcardnum;

  public void OnCloseBtnPressed()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnBackBtnPressed()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OpenHeroList()
  {
    if (this.uiManagerPanel.BT_Back.gameObject.activeSelf)
      UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
    else
      UIManager.inst.OpenDlg("Legend/LegendListDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void Init()
  {
    this.title.text = ScriptLocalization.Get("hero_altar_uppercase_summon_hero", true);
    int num = 0;
    BuildingController buildingByType = CitadelSystem.inst.GetBuildingByType("dragon_lair");
    if ((UnityEngine.Object) null != (UnityEngine.Object) buildingByType)
      num = buildingByType.mBuildingItem.mLevel;
    this.curLvl.text = ScriptLocalization.Get("hero_altar_name", true) + " " + ScriptLocalization.Get("id_lv", true) + " " + (object) num;
    this.FilterLegend(ConfigManager.inst.DB_Legend.Legends);
    if (this.legends.Count == 0)
    {
      this.content.SetActive(false);
      this.noherohint.SetActive(true);
    }
    else
    {
      this.content.SetActive(true);
      this.noherohint.SetActive(false);
      Vector3 localPosition = this.wc.transform.localPosition;
      localPosition.x = (float) -this.lcTemplate.GetComponent<UIWidget>().width;
      this.wc.minIndex = 0;
      this.wc.maxIndex = this.legends.Count - 1;
      this.wc.transform.localPosition = localPosition;
      this.GenerateCard();
      this.wc.SortBasedOnScrollMovement();
      this.sv.ResetPosition();
      this.wc.WrapContent();
    }
  }

  private void InitItem(GameObject go, int wrapIndex, int realIndex)
  {
    int legendIndex = this.MappingToLegendIndex(realIndex);
    LegendCard component = go.GetComponent<LegendCard>();
    LegendCard.Data data;
    if (this.legends.TryGetValue(legendIndex, out data))
    {
      component.SeedData(data);
      component.wrapIndex = wrapIndex;
      component.realIndex = realIndex;
      if (this.greyPreview && data.reqLvl == this.greyLevel)
        GreyUtility.Grey(component.card.gameObject);
      else
        GreyUtility.Normal(component.card.gameObject);
    }
    else
      Debug.LogWarning((object) ("wrong index " + (object) legendIndex));
  }

  private int MappingToLegendIndex(int realIndex)
  {
    return realIndex;
  }

  private void FilterLegend(List<LegendInfo> alllegends)
  {
    this.legends.Clear();
    this.greyPreview = false;
    this.noPreview = false;
    List<LegendData> underRecruitLegends = DBManager.inst.DB_Legend.GetTotalUnderRecruitLegends();
    List<int> intList = new List<int>();
    using (List<LegendData>.Enumerator enumerator = underRecruitLegends.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        LegendData current = enumerator.Current;
        intList.Add(current.LegendID);
      }
    }
    int num = 0;
    BuildingController buildingByType = CitadelSystem.inst.GetBuildingByType("dragon_lair");
    if ((UnityEngine.Object) null != (UnityEngine.Object) buildingByType)
      num = buildingByType.mBuildingItem.mLevel;
    int index = 0;
    int key = 0;
    for (; index < alllegends.Count; ++index)
    {
      int internalId = alllegends[index].internalId;
      int requirementBuildingLevel = this.GetRequirementBuildingLevel(alllegends[index]);
      if (!intList.Contains(internalId))
      {
        if (num >= requirementBuildingLevel)
        {
          this.legends.Add(key, new LegendCard.Data()
          {
            configId = alllegends[index].internalId,
            image = alllegends[index].Image,
            name = alllegends[index].Loc_Name,
            reqLvl = requirementBuildingLevel,
            reqCoin = alllegends[index].cost.silver
          });
          ++key;
        }
        else
        {
          if (!this.greyPreview)
          {
            this.greyPreview = true;
            this.greyLevel = requirementBuildingLevel;
          }
          if (this.greyLevel == requirementBuildingLevel)
          {
            this.legends.Add(key, new LegendCard.Data()
            {
              configId = alllegends[index].internalId,
              image = alllegends[index].Image,
              name = alllegends[index].Loc_Name,
              reqLvl = requirementBuildingLevel,
              reqCoin = alllegends[index].cost.silver
            });
            ++key;
            ++this.specialcardnum;
          }
          else if (!this.noPreview)
          {
            this.noPreview = true;
            this.legends.Add(key, new LegendCard.Data()
            {
              configId = alllegends[index].internalId,
              image = "Texture/Legend/Image/card_hero_locked",
              name = alllegends[index].Loc_Name,
              reqLvl = requirementBuildingLevel,
              reqCoin = alllegends[index].cost.silver
            });
            ++key;
            ++this.specialcardnum;
          }
        }
      }
    }
  }

  private void GenerateCard()
  {
    int realIndex = 0;
    using (Dictionary<int, LegendCard.Data>.Enumerator enumerator = this.legends.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, LegendCard.Data> current = enumerator.Current;
        if (realIndex == 5)
          break;
        GameObject go = Utils.DuplicateGOB(this.lcTemplate.gameObject, this.wc.transform);
        go.SetActive(true);
        Vector3 localPosition = go.transform.localPosition;
        localPosition.x = (float) (go.GetComponent<UIWidget>().width * realIndex);
        go.transform.localPosition = localPosition;
        this.InitItem(go, 0, realIndex);
        go.GetComponent<LegendCard>().recruiteSuccess += new System.Action<GameObject, int, int>(this.OnRecruiteSuccess);
        this.lcPool.Add(go);
        ++realIndex;
      }
    }
  }

  private void OnRecruiteSuccess(GameObject go, int wrapindex, int realindex)
  {
    AudioManager.Instance.PlaySound("sfx_hero_summoned", false);
    this.Reset();
    this.Init();
    this.sv.MoveRelative(new Vector3((float) (-realindex * this.wc.itemSize), 0.0f));
    this.sv.InvalidateBounds();
    this.sv.RestrictWithinBounds(false);
  }

  private int GetRequirementBuildingLevel(LegendInfo legendInfo)
  {
    using (List<Requirements.RequireValuePair>.Enumerator enumerator = legendInfo.require.RequireBuildings().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuildingInfo data = ConfigManager.inst.DB_Building.GetData(enumerator.Current.internalID);
        if (data != null && "dragon_lair" == data.Type)
          return data.Building_Lvl;
      }
    }
    return 0;
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.wc.onInitializeItem += new UIWrapContent.OnInitializeItem(this.InitItem);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }

  private void Reset()
  {
    this.legends.Clear();
    foreach (LegendCard componentsInChild in this.wc.GetComponentsInChildren<LegendCard>())
      componentsInChild.Reset();
    using (List<GameObject>.Enumerator enumerator = this.lcPool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if ((UnityEngine.Object) null != (UnityEngine.Object) current)
        {
          current.transform.parent = (Transform) null;
          UnityEngine.Object.Destroy((UnityEngine.Object) current);
        }
      }
    }
    this.lcPool.Clear();
    this.greyLevel = -1;
    this.nplevel = -1;
    this.specialcardnum = 0;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.wc.onInitializeItem -= new UIWrapContent.OnInitializeItem(this.InitItem);
    foreach (LegendCard componentsInChild in this.wc.GetComponentsInChildren<LegendCard>())
      componentsInChild.Clear();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
  }
}
