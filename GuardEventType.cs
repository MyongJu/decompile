﻿// Decompiled with JetBrains decompiler
// Type: GuardEventType
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public enum GuardEventType
{
  Unknown = -1,
  Attack = 0,
  Death = 1,
  SpellStart = 2,
  Spell = 3,
  SpellEnd = 4,
  AttackStart = 5,
  AttackEnd = 6,
}
