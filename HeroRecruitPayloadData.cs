﻿// Decompiled with JetBrains decompiler
// Type: HeroRecruitPayloadData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class HeroRecruitPayloadData
{
  public string type;
  public int hero_id;
  public int item_id;
  public int num;

  public bool Decode(object orgData)
  {
    Hashtable inData = orgData as Hashtable;
    if (inData == null)
      return false;
    return false | DatabaseTools.UpdateData(inData, "type", ref this.type) | DatabaseTools.UpdateData(inData, "hero_id", ref this.hero_id) | DatabaseTools.UpdateData(inData, "item_id", ref this.item_id) | DatabaseTools.UpdateData(inData, "num", ref this.num);
  }
}
