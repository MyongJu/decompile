﻿// Decompiled with JetBrains decompiler
// Type: MiniMapCamera
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class MiniMapCamera : MonoBehaviour
{
  public Plane MiniMapPlane = new Plane(Vector3.back, 0.0f);
  private float m_Damping = 0.1f;
  private float m_Strength = 5f;
  private float m_Smoothing = 30f;
  private Vector3 m_Velocity = (Vector3) Vector2.zero;
  private float m_ZoomVelocity = 0.3f;
  private const float SCROLLVIEW_TOLERANCE = 0.5f;
  private const float ZOOM_THRESHOLD = 1f;
  private const float MOVE_SPEED = 3f;
  private const float SWITCH_THRESHOLD = 10f;
  public MiniMapController Center;
  public ActiveKingdom Kingdom;
  public System.Action<Coordinate> OnClicked;
  private MiniMapCamera.CameraState m_CameraState;
  private Vector3 m_Hitpoint;
  private WorldCoordinate m_WorldLocation;
  private Coordinate m_Coordinate;
  private Vector2 m_NewCenter;
  private System.Action m_SwitchCallback;

  public MiniMapCamera.CameraState TheCamearState
  {
    get
    {
      return this.m_CameraState;
    }
    set
    {
      this.m_CameraState = value;
    }
  }

  public Vector3 TriggerHitPoint
  {
    get
    {
      return this.m_Hitpoint;
    }
    set
    {
      this.m_Hitpoint = value;
    }
  }

  public Coordinate TriggerCoordinate
  {
    get
    {
      return this.m_Coordinate;
    }
    set
    {
      this.m_Coordinate = value;
    }
  }

  public void MoveTo(int tileX, int tileY)
  {
    this.MoveToWorld(this.Center.ConvertXYToWorldPoint(tileX, tileY), true);
  }

  private void MoveToWorld(Vector2 point, bool constraint = true)
  {
    this.transform.position = new Vector3(point.x, point.y, this.transform.position.z);
    Vector3 vector3 = Vector3.zero;
    if (constraint)
      vector3 = this.GetConstraintOffset();
    this.transform.position = this.transform.position + vector3;
  }

  private Collider Pick(Vector2 position)
  {
    RaycastHit hitInfo;
    if (Physics.Raycast(this.GetComponent<Camera>().ScreenPointToRay((Vector3) position), out hitInfo, float.PositiveInfinity, 1 << GameEngine.Instance.TileLayer))
      return hitInfo.collider;
    return (Collider) null;
  }

  private void OnTap(TapGesture gesture)
  {
    if (TutorialManager.Instance.IsRunning || !(bool) ((UnityEngine.Object) this.Center) || (this.m_CameraState == MiniMapCamera.CameraState.ZOOMING || this.m_CameraState == MiniMapCamera.CameraState.SWITCHING))
      return;
    if (this.m_CameraState == MiniMapCamera.CameraState.SLIDING)
    {
      this.m_Velocity = (Vector3) Vector2.zero;
      this.m_CameraState = MiniMapCamera.CameraState.IDLE;
    }
    Collider collider = this.Pick(gesture.Position);
    if ((UnityEngine.Object) collider == (UnityEngine.Object) null)
      return;
    UIEventTrigger component1 = collider.GetComponent<UIEventTrigger>();
    if ((UnityEngine.Object) null != (UnityEngine.Object) component1 && component1.onClick != null && (UnityEngine.Object) UIEventTrigger.current == (UnityEngine.Object) null)
    {
      UIEventTrigger.current = component1;
      EventDelegate.Execute(component1.onClick);
      UIEventTrigger.current = (UIEventTrigger) null;
    }
    else
    {
      MiniMapLandMark component2 = collider.GetComponent<MiniMapLandMark>();
      if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
      {
        this.m_Coordinate = new Coordinate(this.Kingdom.WorldID, component2.X, component2.Y);
        this.m_WorldLocation = PVPMapData.MapData.ConvertTileKXYToWorldCoordinate(this.m_Coordinate);
        this.m_Hitpoint = (Vector3) this.Center.ConvertXYToWorldPoint(component2.X, component2.Y);
      }
      else
      {
        this.m_Hitpoint = this.GetWorldHitPoint((Vector3) gesture.Position);
        Vector3 hitpoint = this.m_Hitpoint;
        hitpoint.x = hitpoint.x / MiniMapSystem.Inner.x - (float) this.Kingdom.KingdomX;
        hitpoint.y = (float) this.Kingdom.KingdomY - hitpoint.y / MiniMapSystem.Inner.y;
        hitpoint.x *= PVPMapData.MapData.PixelsPerKingdom.x;
        hitpoint.y *= PVPMapData.MapData.PixelsPerKingdom.y;
        hitpoint.x = this.Kingdom.pixelOrigin.x + hitpoint.x;
        hitpoint.y = this.Kingdom.pixelOrigin.y - hitpoint.y;
        this.m_WorldLocation = PVPMapData.MapData.ConvertPixelPositionToWorldCoordinate((Vector2) hitpoint);
        this.m_Coordinate = PVPMapData.MapData.ConvertWorldCoordinateToKXY(this.m_WorldLocation);
      }
      if (this.m_Coordinate.K != this.Kingdom.WorldID || this.m_Coordinate.X < 0 || (this.m_Coordinate.X >= (int) PVPMapData.MapData.TilesPerKingdom.x || this.m_Coordinate.Y < 0) || this.m_Coordinate.Y >= (int) PVPMapData.MapData.TilesPerKingdom.y)
        return;
      this.Center.Location.SetActive(true);
      this.Center.MoveLocation(this.m_Coordinate);
      this.m_CameraState = MiniMapCamera.CameraState.ZOOMING;
    }
  }

  private void OnDrag(DragGesture gesture)
  {
    if (!(bool) ((UnityEngine.Object) this.Center) || this.m_CameraState == MiniMapCamera.CameraState.ZOOMING || this.m_CameraState == MiniMapCamera.CameraState.SWITCHING || (double) Input.mouseScrollDelta.sqrMagnitude > 0.0)
      return;
    if (gesture.Phase == ContinuousGesturePhase.Started)
    {
      this.m_Velocity = (Vector3) Vector2.zero;
      this.m_CameraState = MiniMapCamera.CameraState.DRAGGING;
    }
    else if (gesture.Phase == ContinuousGesturePhase.Updated)
    {
      Vector2 deltaMove = gesture.DeltaMove;
      Vector2 position1 = gesture.Position;
      Vector2 vector2 = position1 - deltaMove;
      Vector3 vector3 = this.GetWorldHitPoint((Vector3) position1) - this.GetWorldHitPoint((Vector3) vector2);
      Vector3 position2 = this.transform.position;
      this.transform.position = position2 - vector3;
      Vector3 constraintOffset = this.GetConstraintOffset();
      if ((double) constraintOffset.sqrMagnitude > 0.0)
        this.transform.position = this.transform.position + constraintOffset;
      this.m_Velocity = (this.transform.position - position2) * this.m_Smoothing;
    }
    else
      this.m_CameraState = MiniMapCamera.CameraState.SLIDING;
  }

  private Vector3 GetConstraintOffset()
  {
    Rect viewport = this.GetViewport();
    Rect constraint = this.GetConstraint();
    float x = 0.0f;
    if ((double) viewport.xMin < (double) constraint.xMin)
      x = constraint.xMin - viewport.xMin;
    else if ((double) viewport.xMax > (double) constraint.xMax)
      x = constraint.xMax - viewport.xMax;
    float y = 0.0f;
    if ((double) viewport.yMin < (double) constraint.yMin)
      y = constraint.yMin - viewport.yMin;
    else if ((double) viewport.yMax > (double) constraint.yMax)
      y = constraint.yMax - viewport.yMax;
    return new Vector3(x, y, 0.0f);
  }

  private Vector3[] GetCorners()
  {
    return this.GetComponent<Camera>().GetWorldCorners(this.MiniMapPlane.GetDistanceToPoint(this.transform.position));
  }

  private Rect GetConstraint()
  {
    Rect rect = new Rect();
    Vector2 vector2 = new Vector2(this.Center.Origin.x - (float) (0.5 * ((double) MiniMapSystem.Outter.x - (double) MiniMapSystem.Inner.x)), this.Center.Origin.y + (float) (0.5 * ((double) MiniMapSystem.Outter.y - (double) MiniMapSystem.Inner.y)));
    rect.xMin = vector2.x;
    rect.xMax = vector2.x + MiniMapSystem.Outter.x;
    rect.yMin = vector2.y - MiniMapSystem.Outter.y;
    rect.yMax = vector2.y;
    return rect;
  }

  private Rect GetViewport()
  {
    Vector3[] corners = this.GetCorners();
    float b1 = float.MaxValue;
    float b2 = float.MaxValue;
    float b3 = float.MinValue;
    float b4 = float.MinValue;
    for (int index = 0; index < 4; ++index)
    {
      b1 = Mathf.Min(corners[index].x, b1);
      b2 = Mathf.Min(corners[index].y, b2);
      b3 = Mathf.Max(corners[index].x, b3);
      b4 = Mathf.Max(corners[index].y, b4);
    }
    return new Rect()
    {
      x = b1,
      y = b2,
      width = b3 - b1,
      height = b4 - b2
    };
  }

  private void Update()
  {
    if (this.Kingdom != null)
      UIManager.inst.publicHUD.SetKingdomInfo(this.Kingdom);
    if (this.m_CameraState == MiniMapCamera.CameraState.SLIDING)
    {
      if ((double) this.m_Velocity.sqrMagnitude > 0.5)
      {
        this.transform.position = this.transform.position + Utils.Dampen(ref this.m_Velocity, this.m_Damping, this.m_Strength, Time.deltaTime);
        this.transform.position = this.transform.position + this.GetConstraintOffset();
      }
      else
      {
        this.m_Velocity = (Vector3) Vector2.zero;
        this.m_CameraState = MiniMapCamera.CameraState.IDLE;
      }
    }
    else
    {
      if (this.m_CameraState != MiniMapCamera.CameraState.SWITCHING)
        return;
      this.MoveToWorld((Vector2) Vector3.Lerp(this.transform.position, (Vector3) this.m_NewCenter, 3f * Time.fixedDeltaTime), false);
      if ((double) Mathf.Abs(this.transform.position.x - this.m_NewCenter.x) >= 10.0 || (double) Mathf.Abs(this.transform.position.y - this.m_NewCenter.y) >= 10.0)
        return;
      if (this.m_SwitchCallback != null)
        this.m_SwitchCallback();
      this.m_CameraState = MiniMapCamera.CameraState.IDLE;
    }
  }

  private void FixedUpdate()
  {
    if (this.m_CameraState != MiniMapCamera.CameraState.ZOOMING)
      return;
    this.MoveToWorld((Vector2) Vector3.Lerp(this.transform.position, this.m_Hitpoint, 3f * Time.fixedDeltaTime), true);
    Vector3 position = this.transform.position;
    position.z = Mathf.SmoothDamp(this.transform.position.z, -50f, ref this.m_ZoomVelocity, 0.3f);
    this.transform.position = position;
    if ((double) Mathf.Abs(this.transform.position.z + 50f) >= 1.0 || this.OnClicked == null)
      return;
    this.OnClicked(this.m_Coordinate);
  }

  public void SwitchKingdom(Vector2 worldPoint, System.Action callback)
  {
    this.m_NewCenter = worldPoint;
    this.m_SwitchCallback = callback;
    this.m_CameraState = MiniMapCamera.CameraState.SWITCHING;
  }

  public Vector3 GetWorldHitPoint(Vector3 screenPosition)
  {
    float enter = 0.0f;
    Ray ray = this.GetComponent<Camera>().ScreenPointToRay(screenPosition);
    if (this.MiniMapPlane.Raycast(ray, out enter))
      return ray.GetPoint(enter);
    return Vector3.zero;
  }

  public enum CameraState
  {
    IDLE,
    DRAGGING,
    SLIDING,
    ZOOMING,
    SWITCHING,
  }
}
