﻿// Decompiled with JetBrains decompiler
// Type: PlayerProfileChangePortraitDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PlayerProfileChangePortraitDlg : UI.Dialog
{
  private const string IMAGE_PRE = "Texture/Hero/player_portrait_";
  private const string GALLERY_RENDER_PATH = "Prefab/UI/Dialogs/PlayerProfile/GalleryRender";
  private const string COST_SHOPITEM_NAME = "shopitem_player_profile_change";
  public GameObject galleryGrid;
  public UICenterOnChild uiCenterOnChild;
  public UIScrollView scrollView;
  public UIButton mUseBtn;
  public UIButton mGetAndUseBtn;
  public UILabel mUseBtnValue;
  public UILabel mGetAndUseBtnValue;
  public UILabel mLabelIconCd;
  public UIButton mButtonUploadIcon;
  public UIButton mButtonResetCustomIcon;
  public UIButton mButtonUnlockPortrait;
  public UITexture mTextureUnlockPortraitProps;
  public UILabel mLabelUnlockPortraitProps;
  public UILabel mLabelIconStatus;
  private PlayerProfilePortaitRender currentRender;
  private List<PlayerProfilePortaitRender> renders;
  private int _itemsAvailable;
  private Color _unlockButtonTextColor;

  private void Update()
  {
    if (!this.scrollView.isDragging)
      return;
    using (List<PlayerProfilePortaitRender>.Enumerator enumerator = this.renders.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PlayerProfilePortaitRender current = enumerator.Current;
        Transform cachedTransform = this.scrollView.panel.cachedTransform;
        Vector3[] worldCorners = this.scrollView.panel.worldCorners;
        Vector3 position = (worldCorners[2] + worldCorners[0]) * 0.5f;
        if ((double) Mathf.Abs((cachedTransform.InverseTransformPoint(current.transform.position) - cachedTransform.InverseTransformPoint(position)).x) < 300.0)
        {
          if (this.currentRender.index != current.index)
          {
            current.HighLight();
            this.currentRender = current;
          }
        }
        else
          current.Normal();
      }
    }
  }

  public override void OnCreate(UIControler.UIParameter param)
  {
    this.InitImage();
  }

  public override void OnOpen(UIControler.UIParameter param)
  {
  }

  public override void OnHide(UIControler.UIParameter param)
  {
    base.OnHide(param);
    this.uiCenterOnChild.onFinished -= new SpringPanel.OnFinished(this.Refresh);
    this.scrollView.onDragStarted -= new UIScrollView.OnDragNotification(this.OnScrollViewDragStarted);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.SecondTick);
  }

  public override void OnShow(UIControler.UIParameter param)
  {
    base.OnShow(param);
    this._unlockButtonTextColor = this.mLabelUnlockPortraitProps.color;
    HeroProfileUtils.SetProfileNewStatus(false);
    this.uiCenterOnChild.onFinished += new SpringPanel.OnFinished(this.Refresh);
    this.scrollView.onDragStarted += new UIScrollView.OnDragNotification(this.OnScrollViewDragStarted);
    this.UpdateUI();
    Oscillator.Instance.secondEvent += new System.Action<int>(this.SecondTick);
  }

  protected void SecondTick(int delta)
  {
    this.UpdateUploadIconCd();
  }

  protected void UpdateUploadIconCd()
  {
    long num = PlayerData.inst.userData.IconCd - (long) NetServerTime.inst.ServerTimestamp;
    if (num > 0L)
    {
      this.mLabelIconCd.gameObject.SetActive(true);
      this.mLabelIconCd.text = Utils.FormatTime((int) num, false, false, true);
      this.mButtonUploadIcon.isEnabled = false;
    }
    else
    {
      this.mLabelIconCd.gameObject.SetActive(false);
      this.mButtonUploadIcon.isEnabled = true;
    }
    this.mButtonResetCustomIcon.gameObject.SetActive(!string.IsNullOrEmpty(PlayerData.inst.userData.Icon));
    int iconStatus = PlayerData.inst.userData.IconStatus;
    if (iconStatus == 0)
    {
      this.mLabelIconStatus.gameObject.SetActive(false);
    }
    else
    {
      this.mLabelIconStatus.gameObject.SetActive(true);
      switch (iconStatus - 1)
      {
        case 0:
          this.mLabelIconStatus.text = ScriptLocalization.Get("avatar_upload_check_not_pass", true);
          break;
        case 1:
          this.mLabelIconStatus.text = ScriptLocalization.Get("avatar_upload_check_process", true);
          break;
        case 2:
          this.mLabelIconStatus.text = ScriptLocalization.Get("avatar_upload_check_pass", true);
          break;
        default:
          this.mLabelIconStatus.text = string.Empty;
          break;
      }
    }
  }

  private void ResetPosition()
  {
    if ((bool) ((UnityEngine.Object) this.galleryGrid))
    {
      UIGrid component = this.galleryGrid.GetComponent<UIGrid>();
      if ((bool) ((UnityEngine.Object) component))
      {
        component.repositionNow = true;
        component.Reposition();
      }
    }
    if (!(bool) ((UnityEngine.Object) this.scrollView))
      return;
    this.scrollView.ResetPosition();
  }

  public void OnBackBtnClick()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnChangePortraitClick()
  {
  }

  private void OnScrollViewDragStarted()
  {
    this.mUseBtn.isEnabled = false;
    this.mGetAndUseBtn.isEnabled = false;
  }

  private void InitImage()
  {
    List<HeroProfileInfo> heroProfileInfoList = ConfigManager.inst.DB_HeroProfile.GetHeroProfileInfoList();
    if (heroProfileInfoList == null)
      return;
    this.renders = new List<PlayerProfilePortaitRender>(heroProfileInfoList.Count);
    GameObject prefab = AssetManager.Instance.HandyLoad("Prefab/UI/Dialogs/PlayerProfile/GalleryRender", (System.Type) null) as GameObject;
    for (int index = 0; index < heroProfileInfoList.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.galleryGrid, prefab);
      gameObject.name = heroProfileInfoList[index].imageIndex.ToString();
      PlayerProfilePortaitRender component = gameObject.GetComponent<PlayerProfilePortaitRender>();
      component.index = heroProfileInfoList[index].imageIndex;
      component.Init();
      this.renders.Add(component);
    }
    this.ResetPosition();
  }

  private void UpdateUI()
  {
    this.SetDetails();
    this.UpdateUploadIconCd();
  }

  public void SetDetails()
  {
    NGUITools.SetActive(this.mUseBtn.gameObject, false);
    NGUITools.SetActive(this.mGetAndUseBtn.gameObject, false);
    NGUITools.SetActive(this.mButtonUnlockPortrait.gameObject, false);
    if (HeroProfileUtils.IsUnlockedHeroPortrait(!((UnityEngine.Object) this.currentRender != (UnityEngine.Object) null) ? -1 : this.currentRender.index))
    {
      this.mUseBtn.isEnabled = false;
      this.mGetAndUseBtn.isEnabled = false;
      this._itemsAvailable = ItemBag.Instance.GetItemCountByShopID("shopitem_player_profile_change");
      int shopItemPrice = ItemBag.Instance.GetShopItemPrice("shopitem_player_profile_change");
      if (this._itemsAvailable > 0)
      {
        this.mUseBtn.gameObject.SetActive(true);
        this.mGetAndUseBtn.gameObject.SetActive(false);
        this.mUseBtnValue.text = "1";
        this.mUseBtn.isEnabled = true;
      }
      else
      {
        this.mUseBtn.gameObject.SetActive(false);
        this.mGetAndUseBtn.gameObject.SetActive(true);
        this.mGetAndUseBtnValue.text = Utils.FormatThousands(shopItemPrice.ToString());
        this.mGetAndUseBtn.isEnabled = true;
      }
      if (!((UnityEngine.Object) this.currentRender == (UnityEngine.Object) null) && PlayerData.inst.userData.portrait != this.currentRender.index)
        return;
      this.mUseBtn.isEnabled = false;
      this.mGetAndUseBtn.isEnabled = false;
    }
    else
    {
      NGUITools.SetActive(this.mButtonUnlockPortrait.gameObject, true);
      HeroProfileInfo byImageIndex = ConfigManager.inst.DB_HeroProfile.GetByImageIndex(!((UnityEngine.Object) this.currentRender != (UnityEngine.Object) null) ? -1 : this.currentRender.index);
      if (byImageIndex == null)
        return;
      this.mLabelUnlockPortraitProps.text = byImageIndex.costValue.ToString();
      if (ItemBag.Instance.GetItemCount(byImageIndex.costId) >= byImageIndex.costValue)
        this.mLabelUnlockPortraitProps.color = this._unlockButtonTextColor;
      else
        this.mLabelUnlockPortraitProps.color = Color.red;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(byImageIndex.costId);
      if (itemStaticInfo == null)
        return;
      BuilderFactory.Instance.HandyBuild((UIWidget) this.mTextureUnlockPortraitProps, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, true, string.Empty);
    }
  }

  private void Refresh()
  {
    if (!((UnityEngine.Object) this.uiCenterOnChild.centeredObject != (UnityEngine.Object) null))
      return;
    PlayerProfilePortaitRender component = this.uiCenterOnChild.centeredObject.GetComponent<PlayerProfilePortaitRender>();
    component.HighLight();
    this.currentRender = component;
    using (List<PlayerProfilePortaitRender>.Enumerator enumerator = this.renders.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PlayerProfilePortaitRender current = enumerator.Current;
        if (current.index != component.index)
          current.Normal();
      }
    }
    this.UpdateUI();
  }

  public void OnUseBtnPressed()
  {
    ItemBag.Instance.UseItem(ConfigManager.inst.DB_Shop.GetShopData("shopitem_player_profile_change").Item_InternalId, 1, Utils.Hash((object) "city_id", (object) CityManager.inst.GetCityID(), (object) "portrait", (object) this.currentRender.index), (System.Action<bool, object>) ((_param1, _param2) => this.OnBackBtnClick()));
  }

  public void OnGetAndUseBtnPressed()
  {
    UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
    {
      goldNum = ItemBag.Instance.GetShopItemPrice("shopitem_player_profile_change"),
      description = Utils.XLAT("player_profile_portrait_change_confirm"),
      confirmButtonClickEvent = (System.Action) (() => ItemBag.Instance.BuyAndUseShopItem("shopitem_player_profile_change", Utils.Hash((object) "city_id", (object) CityManager.inst.GetCityID(), (object) "portrait", (object) this.currentRender.index), (System.Action<bool, object>) ((_param1, _param2) => this.OnBackBtnClick()), 1))
    });
  }

  public void OnUnlockPortraitBtnPressed()
  {
    if (!((UnityEngine.Object) this.currentRender != (UnityEngine.Object) null))
      return;
    HeroProfileInfo byImageIndex = ConfigManager.inst.DB_HeroProfile.GetByImageIndex(this.currentRender.index);
    if (byImageIndex == null)
      return;
    if (ItemBag.Instance.GetItemCount(byImageIndex.costId) >= byImageIndex.costValue)
    {
      string str1 = Utils.XLAT("id_uppercase_okay");
      string str2 = Utils.XLAT("player_profile_uppercase_unlock_portraits");
      string str3 = Utils.XLAT("player_profile_unlock_portraits_confirm_description");
      UIManager.inst.OpenPopup("Marksman/CommonConfirmPopup", (Popup.PopupParameter) new CommonConfirmPopup.Parameter()
      {
        title = str2,
        content = str3,
        confirmButtonText = str1,
        onOkayCallback = new System.Action(this.UnlockHeroPortrait)
      });
    }
    else
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(byImageIndex.costId);
      if (itemStaticInfo == null)
        return;
      UIManager.inst.OpenPopup("UseItemCannotGoldPopup", (Popup.PopupParameter) new UseItemCannotGoldPopup.Parameter()
      {
        itemStaticInfo = itemStaticInfo,
        btnText = Utils.XLAT("id_uppercase_get_more"),
        needvalue = byImageIndex.costValue,
        contentText = itemStaticInfo.LocDescription,
        titleText = itemStaticInfo.LocName
      });
    }
  }

  public void OnUploadCustomIconClicked()
  {
    UIManager.inst.OpenPopup("UpLoadCustomIconPopup", (Popup.PopupParameter) null);
  }

  public void OnResetCustomIconClicked()
  {
    string str1 = ScriptLocalization.Get("player_profile_avatar_use_default_confirm_title", true);
    string str2 = ScriptLocalization.Get("player_profile_avatar_use_default_confirm_description", true);
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = str1,
      content = str2,
      yes = ScriptLocalization.Get("id_uppercase_yes", true),
      no = ScriptLocalization.Get("id_uppercase_no", true),
      yesCallback = new System.Action(this.OnConfirmResetCustomIcon),
      noCallback = (System.Action) null
    });
  }

  protected void OnConfirmResetCustomIcon()
  {
    RequestManager.inst.SendRequest("Player:resetIcon", (Hashtable) null, new System.Action<bool, object>(this.OnResetCustomIconResult), true);
  }

  protected void OnResetCustomIconResult(bool result, object data)
  {
    if (!result)
      return;
    this.mButtonResetCustomIcon.gameObject.SetActive(false);
  }

  protected void UnlockHeroPortrait()
  {
    if (!((UnityEngine.Object) this.currentRender != (UnityEngine.Object) null))
      return;
    HeroProfileUtils.UnlockHeroProfile(this.currentRender.index, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.Refresh();
    }));
  }
}
