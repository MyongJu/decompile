﻿// Decompiled with JetBrains decompiler
// Type: TimerChestConfirmPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class TimerChestConfirmPopup : Popup
{
  public UITexture TT_Item;
  public UILabel LB_Count;
  public UILabel LB_Name;
  public UILabel LB_Discription;
  private ItemStaticInfo itemInfo;
  private int itemCount;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    TimerChestConfirmPopup.Parameter parameter = orgParam as TimerChestConfirmPopup.Parameter;
    if (parameter == null)
      return;
    this.itemInfo = ConfigManager.inst.DB_Items.GetItem(parameter.itemId);
    this.itemCount = parameter.itemCount;
    if (this.itemInfo != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.TT_Item, this.itemInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this.LB_Name.text = this.itemInfo.LocName;
      this.LB_Discription.text = this.itemInfo.LocDescription;
    }
    this.LB_Count.text = string.Format("X {0}", (object) this.itemCount);
  }

  public void OnButtonClick()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.itemInfo.internalId);
    ItemRewardInfo.Data data = new ItemRewardInfo.Data();
    data.count = (float) this.itemCount;
    data.icon = itemStaticInfo.ImagePath;
    RewardsCollectionAnimator.Instance.Clear();
    RewardsCollectionAnimator.Instance.items.Add(data);
    RewardsCollectionAnimator.Instance.CollectItems(true);
    AudioManager.Instance.PlaySound("sfx_harvest_timed_chest", false);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int itemId;
    public int itemCount;
  }
}
