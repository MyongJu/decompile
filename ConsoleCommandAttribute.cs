﻿// Decompiled with JetBrains decompiler
// Type: ConsoleCommandAttribute
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

[AttributeUsage(AttributeTargets.Method)]
public class ConsoleCommandAttribute : Attribute
{
  public string m_command;
  public string m_help;
  public bool m_runOnMainThread;

  public ConsoleCommandAttribute(string cmd, string help, bool runOnMainThread = true)
  {
    this.m_command = cmd;
    this.m_help = help;
    this.m_runOnMainThread = runOnMainThread;
  }
}
