﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossDonatePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceBossDonatePopup : Popup
{
  private bool canDonate = true;
  public UILabel coolDownTime;
  public UILabel coolDownDesc;
  public UILabel energyCount;
  public UILabel allianceFund;
  public UILabel allianceHonor;
  public UILabel donateEnergyCount;
  public UILabel progTextLabel;
  public UIProgressBar progressBar;
  public System.Action OnRefreshHandler;
  private bool displayClearCD;
  private int currentCdTime;
  private Color timeTextColor;
  private Color energyCountTextColor;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if ((UnityEngine.Object) this.coolDownTime != (UnityEngine.Object) null)
    {
      this.timeTextColor = this.coolDownTime.color;
      this.energyCountTextColor = this.donateEnergyCount.color;
    }
    this.AddEventHandler();
    this.UpdateDonateCooldownStatus();
    this.OnCooldownTextUpdate(0);
    this.ShowDonatePanelDetails((AlliancePortalData) null);
    this.UpdateCurrentPanelDetail();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnDonateBtnPressed()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_alliance_boss_energy");
    if (itemStaticInfo != null)
    {
      ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(itemStaticInfo.internalId);
      if (consumableItemData != null && consumableItemData.quantity < 1 || consumableItemData == null)
      {
        this.donateEnergyCount.color = Color.red;
        UIManager.inst.ShowConfirmationBox(ScriptLocalization.Get("alliance_portal_uppercase_name", true), string.Format(ScriptLocalization.Get("alliance_portal_items_not_enough_description", true), (object) itemStaticInfo.LocName), ScriptLocalization.Get("id_okay", true), (string) null, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) null, (System.Action) null, (System.Action) null);
        return;
      }
      this.donateEnergyCount.color = this.energyCountTextColor;
    }
    if (!this.canDonate)
    {
      if (this.displayClearCD)
        return;
      this.displayClearCD = true;
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_donate_cd_second_per_gold");
      int price = 1;
      if (data != null)
        price = data.ValueInt;
      int time = this.currentCdTime - NetServerTime.inst.ServerTimestamp;
      int cost = ItemBag.CalculateCost(time, price);
      UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
      {
        confirmButtonClickEvent = new System.Action(this.ConfirmCallback),
        confirmButtonGoldNotEnoughClickEvent = new System.Action(this.OnCloseBtnPressed),
        closeButtonCallbackEvent = new System.Action(this.OnPopupCloseHandler),
        lefttime = time,
        goldNum = cost,
        unitprice = price,
        description = ScriptLocalization.GetWithPara("confirm_gold_spend_cooldown_instant_desc", new Dictionary<string, string>()
        {
          {
            "num",
            cost.ToString()
          }
        }, true)
      });
    }
    else
    {
      AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
      int valueInt = ConfigManager.inst.DB_GameConfig.GetData("alliance_boss_donate_energy_max").ValueInt;
      if (alliancePortalData == null)
        return;
      if (alliancePortalData.CurrentEnergy >= (long) valueInt)
      {
        UIManager.inst.OpenDlg("Alliance/AllianceBossTrialListDlg", (UI.Dialog.DialogParameter) null, true, true, true);
        UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
      }
      else
        RequestManager.inst.SendRequest("alliance:donateAllianceBoss", new Hashtable()
        {
          {
            (object) "uid",
            (object) PlayerData.inst.uid
          },
          {
            (object) "building_id",
            (object) alliancePortalData.PortalId
          }
        }, new System.Action<bool, object>(this.DonateCallback), true);
    }
  }

  private void UpdateDonateCooldownStatus()
  {
    AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
    if (alliancePortalData == null)
      return;
    MessageHub.inst.GetPortByAction("alliance:loadBossCdTime").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "kingdom_id",
        (object) alliancePortalData.WorldId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable hashtable = data as Hashtable;
      int outData1 = 0;
      int outData2 = 0;
      if (hashtable != null && hashtable[(object) "cool_down"] != null)
      {
        DatabaseTools.UpdateData(hashtable[(object) "cool_down"] as Hashtable, "cd", ref outData1);
        DatabaseTools.UpdateData(hashtable[(object) "cool_down"] as Hashtable, "can_donation", ref outData2);
        this.canDonate = outData2 < NetServerTime.inst.ServerTimestamp;
        this.currentCdTime = outData1;
      }
      this.OnCooldownTextUpdate(0);
    }), true);
  }

  private void DonateCallback(bool success, object result)
  {
    if (!success)
      return;
    Hashtable hashtable = result as Hashtable;
    int outData1 = 0;
    int outData2 = 0;
    if (hashtable != null && hashtable[(object) "cool_down"] != null)
    {
      DatabaseTools.UpdateData(hashtable[(object) "cool_down"] as Hashtable, "cd", ref outData1);
      DatabaseTools.UpdateData(hashtable[(object) "cool_down"] as Hashtable, "can_donation", ref outData2);
      this.canDonate = outData2 < NetServerTime.inst.ServerTimestamp;
      this.currentCdTime = outData1;
    }
    this.OnCooldownTextUpdate(0);
    this.UpdateCurrentPanelDetail();
  }

  private void UpdateCurrentPanelDetail()
  {
    AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
    int valueInt = ConfigManager.inst.DB_GameConfig.GetData("alliance_boss_donate_energy_max").ValueInt;
    if (alliancePortalData == null || alliancePortalData.CurrentEnergy != (long) valueInt)
      return;
    UIManager.inst.OpenDlg("Alliance/AllianceBossTrialListDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnPopupCloseHandler()
  {
    this.displayClearCD = false;
  }

  private void ConfirmCallback()
  {
    this.displayClearCD = false;
    RequestManager.inst.SendRequest("alliance:clearDonateBossCdTime", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, (System.Action<bool, object>) ((arg1, result) =>
    {
      if (!arg1)
        return;
      if (this.OnRefreshHandler != null)
        this.OnRefreshHandler();
      this.UpdateDonateCooldownStatus();
    }), true);
  }

  private void ShowDonatePanelDetails(AlliancePortalData alliancePortalData = null)
  {
    int valueInt1 = ConfigManager.inst.DB_GameConfig.GetData("alliance_boss_donate_energy_max").ValueInt;
    int valueInt2 = ConfigManager.inst.DB_GameConfig.GetData("alliance_boss_donate_fund").ValueInt;
    int valueInt3 = ConfigManager.inst.DB_GameConfig.GetData("alliance_boss_donate_honor").ValueInt;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_alliance_boss_energy");
    this.energyCount.text = Utils.FormatThousands(itemStaticInfo == null ? "0" : itemStaticInfo.Value.ToString());
    this.allianceFund.text = Utils.FormatThousands(valueInt2.ToString());
    this.allianceHonor.text = Utils.FormatThousands(valueInt3.ToString());
    this.donateEnergyCount.text = "1";
    if (itemStaticInfo != null)
    {
      ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(itemStaticInfo.internalId);
      if (consumableItemData != null && consumableItemData.quantity < 1 || consumableItemData == null)
        this.donateEnergyCount.color = Color.red;
      else
        this.donateEnergyCount.color = this.energyCountTextColor;
    }
    AlliancePortalData alliancePortalData1 = alliancePortalData == null ? DBManager.inst.DB_AlliancePortal.GetAlliancePortalData() : alliancePortalData;
    int num = alliancePortalData1 == null ? 0 : (int) alliancePortalData1.CurrentEnergy;
    this.progTextLabel.text = string.Format("{0}/{1}", (object) Utils.FormatThousands(num.ToString()), (object) Utils.FormatThousands(valueInt1.ToString()));
    this.progressBar.value = (float) num / (float) valueInt1;
  }

  private void OnCooldownTextUpdate(int time)
  {
    if ((UnityEngine.Object) this.coolDownTime == (UnityEngine.Object) null || (UnityEngine.Object) this.coolDownDesc == (UnityEngine.Object) null)
      return;
    int time1 = this.currentCdTime - NetServerTime.inst.ServerTimestamp;
    if (time1 > 0)
    {
      this.coolDownTime.text = Utils.FormatTime(time1, true, false, true);
      NGUITools.SetActive(this.coolDownTime.gameObject, true);
      NGUITools.SetActive(this.coolDownDesc.gameObject, true);
    }
    else
    {
      NGUITools.SetActive(this.coolDownTime.gameObject, false);
      NGUITools.SetActive(this.coolDownDesc.gameObject, false);
    }
    this.coolDownTime.color = !this.canDonate ? Color.red : this.timeTextColor;
  }

  private void OnPortalDataUpdate(AlliancePortalData apd)
  {
    this.ShowDonatePanelDetails(apd);
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_AlliancePortal.onDataChanged += new System.Action<AlliancePortalData>(this.OnPortalDataUpdate);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnCooldownTextUpdate);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_AlliancePortal.onDataChanged -= new System.Action<AlliancePortalData>(this.OnPortalDataUpdate);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnCooldownTextUpdate);
  }
}
