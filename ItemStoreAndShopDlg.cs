﻿// Decompiled with JetBrains decompiler
// Type: ItemStoreAndShopDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class ItemStoreAndShopDlg : UI.Dialog
{
  public UIButtonBar buttonBar;
  public ShopDlg shop;
  public ItemStoreDlg itemStore;
  public UILabel gold;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.gold.text = PlayerData.inst.hostPlayer.Currency.ToString();
    this.buttonBar.OnSelectedHandler = new System.Action<int>(this.OnSelectedHandler);
    this.OnSelectedHandler(1);
    this.AddEventHandler();
    this.ShowNpc();
  }

  private void OnSelectedHandler(int index)
  {
    if (this.buttonBar.SelectedIndex == 1)
    {
      NGUITools.SetActive(this.shop.gameObject, true);
      NGUITools.SetActive(this.itemStore.gameObject, false);
      this.shop.Refresh();
      OperationTrace.TraceClick(ClickArea.PackageShop);
    }
    else
    {
      NGUITools.SetActive(this.shop.gameObject, false);
      NGUITools.SetActive(this.itemStore.gameObject, true);
      this.itemStore.Refresh();
    }
  }

  private void OnUserDataUpdate(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.gold.text = Utils.FormatThousands(DBManager.inst.DB_User.Get(PlayerData.inst.uid).currency.gold.ToString());
  }

  public void OnAddGoldHandler()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdate);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdate);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.RemoveEventHandler();
    this.shop.Dispose();
    this.itemStore.Dispose();
    ItemBag.Instance.ClearNewItems();
    DragonKnightData dragonKnightData = PlayerData.inst.dragonKnightData;
    if (dragonKnightData == null || !dragonKnightData.IsLevelUp)
      return;
    dragonKnightData.IsLevelUp = false;
    UIManager.inst.priorityPopupQueue.OpenPopup("DragonKnight/DragonKnightLevelUpPopup", (Popup.PopupParameter) new DragonKnightLevelUpPopup.Parameter()
    {
      dragonKnightData = dragonKnightData
    }, 0);
  }

  private void ShowNpc()
  {
    Transform transform = this.transform.Find("Content/npc");
    if (!(bool) ((UnityEngine.Object) transform))
      return;
    UITexture component = transform.GetComponent<UITexture>();
    if (!(bool) ((UnityEngine.Object) component))
      return;
    Utils.SetPortrait(component, "Texture/STATIC_TEXTURE/tutorial_npc");
  }
}
