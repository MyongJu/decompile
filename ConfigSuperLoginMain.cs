﻿// Decompiled with JetBrains decompiler
// Type: ConfigSuperLoginMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigSuperLoginMain
{
  private List<SuperLoginMainInfo> superLoginMainInfoList = new List<SuperLoginMainInfo>();
  private Dictionary<string, SuperLoginMainInfo> datas;
  private Dictionary<int, SuperLoginMainInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<SuperLoginMainInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    Dictionary<string, SuperLoginMainInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this.superLoginMainInfoList.Add(enumerator.Current);
    }
    this.superLoginMainInfoList.Sort((Comparison<SuperLoginMainInfo>) ((a, b) => a.day.CompareTo(b.day)));
  }

  public List<SuperLoginMainInfo> GetSuperLoginMainInfoListByGroup(string group)
  {
    SuperLoginGroupInfo groupInfo = ConfigManager.inst.DB_SuperLoginGroup.Get(group);
    if (groupInfo == null)
      return (List<SuperLoginMainInfo>) null;
    List<SuperLoginMainInfo> all = this.superLoginMainInfoList.FindAll((Predicate<SuperLoginMainInfo>) (x => x.group == groupInfo.internalId));
    if (all != null)
      all.Sort((Comparison<SuperLoginMainInfo>) ((a, b) => a.day.CompareTo(b.day)));
    return all;
  }

  public SuperLoginMainInfo Get(int interalId)
  {
    if (this.dicByUniqueId.ContainsKey(interalId))
      return this.dicByUniqueId[interalId];
    return (SuperLoginMainInfo) null;
  }

  public SuperLoginMainInfo Get(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (SuperLoginMainInfo) null;
  }
}
