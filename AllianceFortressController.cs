﻿// Decompiled with JetBrains decompiler
// Type: AllianceFortressController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class AllianceFortressController : MonoBehaviour, IConstructionController
{
  public SpriteRenderer m_Renderer;
  public UnityEngine.Sprite m_Broken;
  public UnityEngine.Sprite m_Intact;
  public GameObject m_Scaffold;
  public GameObject m_Workers;
  public GameObject m_Dust;
  public PVPAllianceSymbol m_AllianceSymbol;
  public TextMesh m_AllianceName;
  private Renderer m_NameRenderer;

  private void Start()
  {
    if (!(bool) ((Object) this.m_AllianceName))
      return;
    this.m_NameRenderer = this.m_AllianceName.GetComponent<Renderer>();
  }

  public void UpdateUI(TileData tile)
  {
    if ((Object) this.m_AllianceSymbol != (Object) null)
      NGUITools.SetActive(this.m_AllianceSymbol.gameObject, true);
    if ((Object) this.m_AllianceName != (Object) null)
      NGUITools.SetActive(this.m_AllianceName.gameObject, true);
    if (tile == null)
      return;
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(tile.AllianceID);
    AllianceFortressData fortressData = tile.FortressData;
    if (allianceData != null)
    {
      this.m_AllianceSymbol.SetSymbols(allianceData.allianceSymbolCode);
      this.m_AllianceName.text = allianceData.allianceAcronym;
    }
    if (fortressData == null)
      return;
    this.UpdateSprite(fortressData);
    this.UpdateScaffold(fortressData);
    this.UpdateWorkers(fortressData);
    this.UpdateDust(fortressData);
  }

  public void UpdateForWarList(AllianceFortressData fortressData)
  {
    this.m_AllianceSymbol.gameObject.SetActive(false);
    this.m_AllianceName.gameObject.SetActive(false);
    this.m_Renderer.sprite = this.m_Intact;
    this.m_Scaffold.SetActive(false);
    this.m_Workers.SetActive(false);
    this.m_Dust.SetActive(false);
  }

  public void Reset()
  {
    this.m_Renderer.sprite = this.m_Intact;
    this.m_Scaffold.SetActive(false);
    this.m_Workers.SetActive(false);
    this.m_Dust.SetActive(false);
    this.m_AllianceSymbol.gameObject.SetActive(false);
    this.m_AllianceName.gameObject.SetActive(false);
  }

  private void UpdateSprite(AllianceFortressData fortressData)
  {
    string state = fortressData.State;
    if (state != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceFortressController.\u003C\u003Ef__switch\u0024map7B == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceFortressController.\u003C\u003Ef__switch\u0024map7B = new Dictionary<string, int>(5)
        {
          {
            "unComplete",
            0
          },
          {
            "building",
            0
          },
          {
            "repairing",
            0
          },
          {
            "demolishing",
            0
          },
          {
            "broken",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceFortressController.\u003C\u003Ef__switch\u0024map7B.TryGetValue(state, out num) && num == 0)
      {
        this.m_Renderer.sprite = this.m_Broken;
        return;
      }
    }
    this.m_Renderer.sprite = this.m_Intact;
  }

  private void UpdateScaffold(AllianceFortressData fortressData)
  {
    bool flag = false;
    string state = fortressData.State;
    if (state != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceFortressController.\u003C\u003Ef__switch\u0024map7C == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceFortressController.\u003C\u003Ef__switch\u0024map7C = new Dictionary<string, int>(2)
        {
          {
            "building",
            0
          },
          {
            "repairing",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceFortressController.\u003C\u003Ef__switch\u0024map7C.TryGetValue(state, out num) && num == 0)
        flag = true;
    }
    this.m_Scaffold.SetActive(flag);
  }

  private void UpdateWorkers(AllianceFortressData fortressData)
  {
    bool flag = false;
    string state = fortressData.State;
    if (state != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceFortressController.\u003C\u003Ef__switch\u0024map7D == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceFortressController.\u003C\u003Ef__switch\u0024map7D = new Dictionary<string, int>(3)
        {
          {
            "building",
            0
          },
          {
            "repairing",
            0
          },
          {
            "demolishing",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceFortressController.\u003C\u003Ef__switch\u0024map7D.TryGetValue(state, out num) && num == 0)
        flag = true;
    }
    this.m_Workers.SetActive(flag);
  }

  private void UpdateDust(AllianceFortressData fortressData)
  {
    bool flag = false;
    string state = fortressData.State;
    if (state != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceFortressController.\u003C\u003Ef__switch\u0024map7E == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceFortressController.\u003C\u003Ef__switch\u0024map7E = new Dictionary<string, int>(3)
        {
          {
            "building",
            0
          },
          {
            "repairing",
            0
          },
          {
            "demolishing",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceFortressController.\u003C\u003Ef__switch\u0024map7E.TryGetValue(state, out num) && num == 0)
        flag = true;
    }
    this.m_Dust.SetActive(flag);
  }

  public void SetSortingLayerID(int sortingLayerID)
  {
    this.m_NameRenderer.sortingLayerID = sortingLayerID;
  }
}
