﻿// Decompiled with JetBrains decompiler
// Type: BenefitCalcInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class BenefitCalcInfo
{
  public static char[] paramSeperateChar = new char[1]
  {
    ','
  };
  public int internalId;
  public string ID;
  public int formula;
  public string[] param1;
  public string[] param2;

  public static float BenefitFormula1(float baseValue, float p1, float p2)
  {
    return baseValue / (1f + p1) - p2;
  }

  public static float BenefitFormula2(float baseValue, float p1, float p2)
  {
    return baseValue * (1f + p1) + p2;
  }

  public static float BenefitFormula4(float baseValue, float p1, float p2)
  {
    return (float) ((double) baseValue / (1.0 + (double) p1) * (1.0 + (double) p2));
  }

  public struct Formula
  {
    public const int FORMULA_1 = 1;
    public const int FORMULA_2 = 2;
    public const int FORMULA_3 = 3;
    public const int FORMULA_4 = 4;
  }
}
