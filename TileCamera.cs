﻿// Decompiled with JetBrains decompiler
// Type: TileCamera
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TileCamera : MonoBehaviour
{
  public float m_Damping = 0.1f;
  public float m_Strength = 5f;
  public float m_Smoothing = 30f;
  public float m_AutoZoom = 6f;
  public float m_NormalSize = 4f;
  private Vector3 m_Velocity = (Vector3) Vector2.zero;
  private float m_SmoothDuration = 0.3f;
  public float m_MinSize = 4f;
  public float m_MaxSize = 6f;
  public float m_ZoomBuffer = 1f;
  public float m_ZoomDuration = 0.3f;
  public float m_DeltaFactor = 10f;
  public bool EnableDragging = true;
  private const float SCROLLVIEW_TOLERANCE = 0.5f;
  private const float SMOOTH_TRANSLATION_TOLERANCE = 0.01f;
  private const float ZOOM_TOLERANCE = 0.05f;
  private TileCamera.CameraState m_CameraState;
  private Plane m_Plane;
  private Vector3 m_SmoothVelocity;
  private Vector3 m_SmoothTargetPosition;
  private bool m_IsAutoZoom;
  private float m_ZoomTarget;
  private Vector2 m_ZoomVelocity;
  private Vector3 m_WorldPositionBeforeZoom;
  private Vector3 m_CurrentGesturePositon;
  private Rect m_Boundary;
  public DynamicMapData MapData;
  public bool ActiveKingdomOnly;

  public event System.Action OnZoomCallback;

  public event System.Action<TapGesture> OnTapCallback;

  public event System.Action<DragGesture> OnDragCallback;

  public Rect Boundary
  {
    get
    {
      return this.m_Boundary;
    }
    set
    {
      this.m_Boundary = value;
    }
  }

  public float Size
  {
    get
    {
      return this.GetComponent<Camera>().orthographicSize;
    }
    set
    {
      this.GetComponent<Camera>().orthographicSize = value;
    }
  }

  public void SetAutoZoom()
  {
    this.Size = this.m_AutoZoom;
    this.m_ZoomTarget = this.m_NormalSize;
  }

  public void StartAutoZoom()
  {
    this.m_IsAutoZoom = true;
  }

  public void StopAutoZoom()
  {
    this.m_IsAutoZoom = false;
  }

  public void SetXYPlane(Vector3 inPosition, Vector3 inNormal)
  {
    this.m_Plane.SetNormalAndPosition(inNormal, inPosition);
  }

  public void SetXYPlane(Plane plane)
  {
    this.m_Plane = plane;
  }

  public bool SmoothTranslation
  {
    get
    {
      return this.m_CameraState == TileCamera.CameraState.TRANSLATING;
    }
  }

  public void SetTargetLookPosition(Vector3 targetPosition, bool smoothTranslation)
  {
    Vector3 lookPosition = this.GetLookPosition();
    Vector3 vector3 = targetPosition - lookPosition;
    vector3.z = 0.0f;
    if (smoothTranslation)
    {
      this.m_SmoothVelocity = Vector3.zero;
      this.m_SmoothTargetPosition = this.GetComponent<Camera>().transform.position + vector3;
      this.m_CameraState = TileCamera.CameraState.TRANSLATING;
    }
    else
    {
      this.GetComponent<Camera>().transform.position = this.GetComponent<Camera>().transform.position + vector3;
      this.m_CameraState = TileCamera.CameraState.IDLE;
    }
  }

  public void SetTargetLookLocation(Coordinate location, bool smoothTranslation)
  {
    this.SetTargetLookPosition(this.MapData.ConvertTileKXYToWorldPosition(location), smoothTranslation);
  }

  public void Translate(Vector3 offset)
  {
    Vector3 worldPos = this.GetComponent<Camera>().transform.position + offset;
    if (!this.ActiveKingdomOnly || this.MapData.IsKingdomActiveAt(worldPos))
    {
      this.GetComponent<Camera>().transform.position = worldPos;
      offset = TileCamera.GetConstraintOffset(this.GetViewport(), this.m_Boundary);
      this.GetComponent<Camera>().transform.position = this.GetComponent<Camera>().transform.position + offset;
    }
    this.m_CameraState = TileCamera.CameraState.IDLE;
  }

  public Vector3 GetLookPosition()
  {
    return this.GetWorldHitPoint(new Vector3(0.5f * (float) Screen.width, 0.5f * (float) Screen.height, 0.0f));
  }

  public Rect GetViewport()
  {
    Vector3[] corners = this.GetCorners();
    float b1 = float.MaxValue;
    float b2 = float.MaxValue;
    float b3 = float.MinValue;
    float b4 = float.MinValue;
    for (int index = 0; index < 4; ++index)
    {
      b1 = Mathf.Min(corners[index].x, b1);
      b2 = Mathf.Min(corners[index].y, b2);
      b3 = Mathf.Max(corners[index].x, b3);
      b4 = Mathf.Max(corners[index].y, b4);
    }
    return new Rect()
    {
      x = b1,
      y = b2,
      width = b3 - b1,
      height = b4 - b2
    };
  }

  private Vector3[] GetCorners()
  {
    return this.GetComponent<Camera>().GetWorldCorners(this.m_Plane.GetDistanceToPoint(this.GetComponent<Camera>().transform.position));
  }

  private void OnDrag(DragGesture gesture)
  {
    if (this.m_CameraState == TileCamera.CameraState.PINCHING)
      return;
    if (this.OnDragCallback != null)
      this.OnDragCallback(gesture);
    if (!this.EnableDragging)
      return;
    if (gesture.Phase == ContinuousGesturePhase.Started)
    {
      this.m_Velocity = (Vector3) Vector2.zero;
      this.m_CameraState = TileCamera.CameraState.DRAGGING;
    }
    else if (gesture.Phase == ContinuousGesturePhase.Updated)
    {
      Vector2 deltaMove = gesture.DeltaMove;
      Vector2 position1 = gesture.Position;
      Vector2 vector2 = position1 - deltaMove;
      Vector3 vector3 = this.GetWorldHitPoint((Vector3) position1) - this.GetWorldHitPoint((Vector3) vector2);
      Vector3 position2 = this.GetComponent<Camera>().transform.position;
      Vector3 worldPos = position2 - vector3;
      if (!this.ActiveKingdomOnly || this.MapData.IsKingdomActiveAt(worldPos))
      {
        this.GetComponent<Camera>().transform.position = worldPos;
        Vector3 constraintOffset = TileCamera.GetConstraintOffset(this.GetViewport(), this.m_Boundary);
        if ((double) constraintOffset.sqrMagnitude > 0.0)
          this.GetComponent<Camera>().transform.position = this.GetComponent<Camera>().transform.position + constraintOffset;
        this.m_Velocity = (this.GetComponent<Camera>().transform.position - position2) * this.m_Smoothing;
      }
      else
        this.m_Velocity = (Vector3) Vector2.zero;
    }
    else
      this.m_CameraState = TileCamera.CameraState.SLIDING;
  }

  public Vector3 GetWorldHitPoint(Vector3 screenPosition)
  {
    float enter = 0.0f;
    Ray ray = this.GetComponent<Camera>().ScreenPointToRay(screenPosition);
    if (this.m_Plane.Raycast(ray, out enter))
      return ray.GetPoint(enter);
    return Vector3.zero;
  }

  private static Vector3 GetConstraintOffset(Rect viewport, Rect constraint)
  {
    float x = 0.0f;
    if ((double) viewport.xMin < (double) constraint.xMin)
      x = constraint.xMin - viewport.xMin;
    else if ((double) viewport.xMax > (double) constraint.xMax)
      x = constraint.xMax - viewport.xMax;
    float y = 0.0f;
    if ((double) viewport.yMin < (double) constraint.yMin)
      y = constraint.yMin - viewport.yMin;
    else if ((double) viewport.yMax > (double) constraint.yMax)
      y = constraint.yMax - viewport.yMax;
    return new Vector3(x, y, 0.0f);
  }

  private void Update()
  {
    if (this.m_CameraState == TileCamera.CameraState.SLIDING)
    {
      if ((double) this.m_Velocity.sqrMagnitude > 0.5)
      {
        Vector3 worldPos = this.GetComponent<Camera>().transform.position + Utils.Dampen(ref this.m_Velocity, this.m_Damping, this.m_Strength, Time.deltaTime);
        if (!this.ActiveKingdomOnly || this.MapData.IsKingdomActiveAt(worldPos))
        {
          this.GetComponent<Camera>().transform.position = worldPos;
          this.GetComponent<Camera>().transform.position = this.GetComponent<Camera>().transform.position + TileCamera.GetConstraintOffset(this.GetViewport(), this.m_Boundary);
        }
        else
        {
          this.m_Velocity = (Vector3) Vector2.zero;
          this.m_CameraState = TileCamera.CameraState.IDLE;
        }
      }
      else
      {
        this.m_Velocity = (Vector3) Vector2.zero;
        this.m_CameraState = TileCamera.CameraState.IDLE;
      }
    }
    else if (this.m_CameraState == TileCamera.CameraState.TRANSLATING)
    {
      this.GetComponent<Camera>().transform.position = Vector3.SmoothDamp(this.GetComponent<Camera>().transform.position, this.m_SmoothTargetPosition, ref this.m_SmoothVelocity, this.m_SmoothDuration);
      if ((double) Vector3.Distance(this.GetComponent<Camera>().transform.position, this.m_SmoothTargetPosition) < 0.00999999977648258)
        this.m_CameraState = TileCamera.CameraState.IDLE;
    }
    else if (this.m_CameraState == TileCamera.CameraState.RESTORE_ZOOMING)
    {
      this.Size = Vector2.SmoothDamp(new Vector2(this.Size, 0.0f), new Vector2(this.m_ZoomTarget, 0.0f), ref this.m_ZoomVelocity, this.m_ZoomDuration).x;
      TileCamera.AdjustPositionAfterZooming(this.m_WorldPositionBeforeZoom, this.GetWorldHitPoint(this.m_CurrentGesturePositon), this.GetComponent<Camera>());
      if (this.OnZoomCallback != null)
        this.OnZoomCallback();
      if ((double) Mathf.Abs(this.Size - this.m_ZoomTarget) < 0.0500000007450581)
        this.m_CameraState = TileCamera.CameraState.IDLE;
    }
    if (!this.m_IsAutoZoom)
      return;
    this.Size = Vector2.SmoothDamp(new Vector2(this.Size, 0.0f), new Vector2(this.m_ZoomTarget, 0.0f), ref this.m_ZoomVelocity, this.m_ZoomDuration).x;
    if (this.OnZoomCallback != null)
      this.OnZoomCallback();
    if ((double) Mathf.Abs(this.Size - this.m_ZoomTarget) >= 0.0500000007450581)
      return;
    this.m_IsAutoZoom = false;
  }

  private static void AdjustPositionAfterZooming(Vector3 beforePivot, Vector3 afterPivot, Camera camera)
  {
    Vector3 vector3 = afterPivot - beforePivot;
    camera.transform.position = camera.transform.position - vector3;
  }

  private void OnPinch(PinchGesture gesture)
  {
    if (gesture.Phase == ContinuousGesturePhase.Started || gesture.Phase == ContinuousGesturePhase.Updated)
    {
      this.m_CameraState = TileCamera.CameraState.PINCHING;
      this.m_CurrentGesturePositon = (Vector3) gesture.Position;
      this.m_WorldPositionBeforeZoom = this.GetWorldHitPoint(this.m_CurrentGesturePositon);
      float num = Mathf.Sqrt((float) (Screen.width * Screen.width + Screen.height * Screen.height));
      this.Size = Mathf.Clamp(this.Size + (float) (-(double) this.m_DeltaFactor * (double) gesture.Delta / (double) num * ((double) this.m_MaxSize - (double) this.m_MinSize)), this.m_MinSize - this.m_ZoomBuffer, this.m_MaxSize + this.m_ZoomBuffer);
      TileCamera.AdjustPositionAfterZooming(this.m_WorldPositionBeforeZoom, this.GetWorldHitPoint(this.m_CurrentGesturePositon), this.GetComponent<Camera>());
      if (this.OnZoomCallback == null)
        return;
      this.OnZoomCallback();
    }
    else
    {
      this.m_ZoomTarget = Mathf.Clamp(this.Size, this.m_MinSize, this.m_MaxSize);
      this.m_ZoomVelocity = Vector2.zero;
      this.m_CameraState = TileCamera.CameraState.RESTORE_ZOOMING;
    }
  }

  private void OnTap(TapGesture gesture)
  {
    if (this.m_CameraState != TileCamera.CameraState.IDLE || TutorialManager.Instance.IsRunning || this.OnTapCallback == null)
      return;
    this.OnTapCallback(gesture);
  }

  private enum CameraState
  {
    IDLE,
    DRAGGING,
    PINCHING,
    SLIDING,
    TRANSLATING,
    RESTORE_ZOOMING,
  }
}
