﻿// Decompiled with JetBrains decompiler
// Type: Preserve3dInUI
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Preserve3dInUI : MonoBehaviour
{
  private GameObject _parent;

  private void Awake()
  {
    this._parent = this.transform.parent.gameObject;
    this.transform.parent = (Transform) null;
  }

  private void LateUpdate()
  {
    if (!this._parent.activeInHierarchy && this.gameObject.activeSelf)
      this.gameObject.SetActive(false);
    if (!this._parent.activeInHierarchy || this.gameObject.activeSelf)
      return;
    this.gameObject.SetActive(true);
  }
}
