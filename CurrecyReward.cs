﻿// Decompiled with JetBrains decompiler
// Type: CurrecyReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class CurrecyReward : QuestReward
{
  private readonly int value;

  public CurrecyReward(int inValue)
  {
    this.value = inValue;
  }

  public override void Claim()
  {
    base.Claim();
  }

  public override string GetRewardIconName()
  {
    return "icon_currency_premium";
  }

  public override string GetRewardTypeName()
  {
    return "Premium Currency";
  }

  public override string GetValueText()
  {
    return this.value.ToString();
  }
}
