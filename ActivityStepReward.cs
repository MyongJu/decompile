﻿// Decompiled with JetBrains decompiler
// Type: ActivityStepReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ActivityStepReward : MonoBehaviour
{
  public UIWidget step1;
  public UIWidget step2;
  public UIWidget stepParent;
  public Icon stepReward1;
  public Icon stepReward2;
  public Icon stepReward3;
  public Icon stepReward4;
  public Icon stepReward5;
  public UIButton helpBtn;
  public UILabel nameLabel;
  public UILabel timeLabel;
  public UIProgressBar progressSlider;
  private int requireScore3;
  private RoundActivityData activityData;

  public void UpdateUI(RoundActivityData activityData)
  {
    this.activityData = activityData;
    this.progressSlider.value = activityData.ProgressValue;
    Dictionary<int, int> roundRewardItems = activityData.CurrentRoundRewardItems;
    List<int> currentItems = activityData.CurrentItems;
    List<int> roundTargetScores = activityData.CurrentRoundTargetScores;
    int num = currentItems.Count / roundTargetScores.Count;
    int index1 = 0;
    for (int index2 = 0; index2 < currentItems.Count; ++index2)
    {
      this.SetItem(index2, roundTargetScores[index1], currentItems[index2], roundRewardItems[currentItems[index2]]);
      if ((index2 + 1) % num == 0)
        ++index1;
    }
    this.nameLabel.text = Utils.XLAT("event_points_target_bonus");
  }

  private void SetItem(int index, int score, int itemId, int value)
  {
    Icon icon = (Icon) null;
    switch (index)
    {
      case 0:
        icon = this.stepReward1;
        break;
      case 1:
        icon = this.stepReward2;
        break;
      case 2:
        icon = this.stepReward3;
        break;
      case 3:
        icon = this.stepReward4;
        break;
      case 4:
        icon = this.stepReward5;
        break;
    }
    if (!((UnityEngine.Object) icon != (UnityEngine.Object) null))
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    IconData iconData = new IconData(itemStaticInfo.ImagePath, new string[3]
    {
      itemStaticInfo.LocName,
      Utils.FormatThousands(score.ToString()),
      "X" + (object) value
    });
    iconData.Data = (object) itemId;
    icon.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
    icon.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
    icon.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
    icon.FeedData((IComponentData) iconData);
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  private void Update()
  {
    this.timeLabel.text = Utils.FormatTime(this.activityData.RemainTime, false, false, true);
  }

  private void OnDispose()
  {
    this.stepReward1.Dispose();
    this.stepReward2.Dispose();
    this.stepReward3.Dispose();
  }
}
