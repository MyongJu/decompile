﻿// Decompiled with JetBrains decompiler
// Type: LoadingScreen
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class LoadingScreen
{
  public const string ASSET_PATH = "Prefab/UI/DragonKnight/LoadingScreen";
  private LoadingController _controller;
  private float _time;
  private float _duration;
  private bool _externalHide;
  private System.Action _callback;
  private static LoadingScreen _instance;

  public static LoadingScreen Instance
  {
    get
    {
      if (LoadingScreen._instance == null)
        LoadingScreen._instance = new LoadingScreen();
      return LoadingScreen._instance;
    }
  }

  public void SetTip(string tip)
  {
    if (!(bool) ((UnityEngine.Object) this._controller))
      return;
    this._controller.SetTip(tip);
  }

  public void Show(float duration, System.Action callback = null)
  {
    this.Hide();
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.Load("Prefab/UI/DragonKnight/LoadingScreen", (System.Type) null) as GameObject);
    AssetManager.Instance.UnLoadAsset("Prefab/UI/DragonKnight/LoadingScreen", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    gameObject.transform.parent = UIManager.inst.ui2DCamera.transform;
    gameObject.transform.localScale = Vector3.one;
    this._controller = gameObject.GetComponent<LoadingController>();
    Oscillator.Instance.updateEvent += new System.Action<double>(this.OnUpdateEvent);
    this._time = 0.0f;
    this._duration = duration;
    this._externalHide = false;
    this._callback = callback;
  }

  public void Hide()
  {
    if ((bool) ((UnityEngine.Object) this._controller))
    {
      this._controller.gameObject.SetActive(false);
      UnityEngine.Object.Destroy((UnityEngine.Object) this._controller.gameObject);
      this._controller = (LoadingController) null;
    }
    if (Oscillator.IsAvailable)
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.OnUpdateEvent);
    if (this._callback == null)
      return;
    this._callback();
    this._callback = (System.Action) null;
  }

  public void MarkHide()
  {
    this._externalHide = true;
  }

  private void OnUpdateEvent(double t)
  {
    if (!(bool) ((UnityEngine.Object) this._controller))
      return;
    if (this._externalHide && (double) this._controller.GetProgress() >= 1.0)
    {
      this.Hide();
    }
    else
    {
      this._controller.SetProgress(this._time / this._duration);
      this._time += Time.deltaTime;
    }
  }
}
