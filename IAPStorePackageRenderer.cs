﻿// Decompiled with JetBrains decompiler
// Type: IAPStorePackageRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class IAPStorePackageRenderer : IAPStoreRenderer
{
  private Dictionary<int, IAPStoreContentRenderer> m_ItemList = new Dictionary<int, IAPStoreContentRenderer>();
  public GameObject m_ItemPrefab;
  public IAPStorePackagePanel m_Panel;
  public UIGrid m_Grid;
  private IAPStorePackage m_StorePackage;

  public void SetData(IAPStorePackage storePackage, System.Action onPurchaseBegin, System.Action<int> onPurchaseEnd, System.Action detailButtonCallback = null)
  {
    this.m_StorePackage = storePackage;
    this.m_Panel.OnPurchaseBegin = onPurchaseBegin;
    this.m_Panel.OnPurchaseEnd = onPurchaseEnd;
    this.m_Panel.OnDetailButtonClicked = detailButtonCallback;
    this.UpdateUI();
  }

  public override void UpdateUI()
  {
    this.m_Panel.SetData(this.m_StorePackage);
    IAPPackageInfo package = this.m_StorePackage.package;
    IAPRewardInfo rewardInfo1 = ConfigManager.inst.DB_IAPRewards.Get(package.reward_group_id);
    IAPRewardInfo rewardInfo2 = ConfigManager.inst.DB_IAPRewards.Get(package.alliance_reward_group);
    this.ClearData();
    this.UpdateData(rewardInfo1);
    this.UpdateData(rewardInfo2);
    this.m_Grid.Reposition();
  }

  public IAPStorePackage Package
  {
    get
    {
      return this.m_StorePackage;
    }
  }

  private void UpdateData(IAPRewardInfo rewardInfo)
  {
    if (rewardInfo == null || rewardInfo.Rewards == null)
      return;
    List<Reward.RewardsValuePair> rewards = rewardInfo.GetRewards();
    rewards.Sort(new Comparison<Reward.RewardsValuePair>(IAPStorePackageRenderer.Compare));
    for (int index = 0; index < rewards.Count && this.m_ItemList.Count < 3; ++index)
    {
      Reward.RewardsValuePair rewardsValuePair = rewards[index];
      int internalId = rewardsValuePair.internalID;
      int itemCount = rewardsValuePair.value;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_Grid.transform;
      gameObject.transform.localScale = Vector3.one;
      IAPStoreContentRenderer component = gameObject.GetComponent<IAPStoreContentRenderer>();
      component.SetData(itemStaticInfo, itemCount);
      this.m_ItemList.Add(internalId, component);
    }
  }

  private void ClearData()
  {
    using (Dictionary<int, IAPStoreContentRenderer>.ValueCollection.Enumerator enumerator = this.m_ItemList.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IAPStoreContentRenderer current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ItemList.Clear();
  }

  private static int Compare(Reward.RewardsValuePair x, Reward.RewardsValuePair y)
  {
    return ConfigManager.inst.DB_Items.GetItem(x.internalID).Priority - ConfigManager.inst.DB_Items.GetItem(y.internalID).Priority;
  }
}
