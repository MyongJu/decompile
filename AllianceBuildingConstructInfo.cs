﻿// Decompiled with JetBrains decompiler
// Type: AllianceBuildingConstructInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AllianceBuildingConstructInfo : MonoBehaviour
{
  public Icon defaultGroup;
  public GameObject timeGroup;
  public UILabel remainTime;
  private bool hasInited;
  private AllianceBuildingConstructInfo.InfoData data;

  public void SeedData(AllianceBuildingConstructInfo.InfoData infodata)
  {
    this.data = infodata;
    this.UpdateData();
  }

  private void UpdateData()
  {
    if (this.data.showtime)
    {
      this.timeGroup.SetActive(true);
      this.remainTime.text = Utils.FormatTime(this.data.remaintime, true, false, true);
    }
    else
      this.timeGroup.SetActive(false);
    this.defaultGroup.SetData((string) null, this.data.content.ToArray());
  }

  private void UpdateRemainTime()
  {
  }

  public void Clear()
  {
    this.RemoveEventHandler();
  }

  private void AddEventHandler()
  {
    this.hasInited = true;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  private void Process(int time)
  {
    if (this.data == null || !this.data.updateTime)
      return;
    this.UpdateRemainTime();
  }

  private void RemoveEventHandler()
  {
    this.hasInited = false;
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
  }

  public class InfoData
  {
    public List<string> content = new List<string>();
    public bool updateTime = true;
    public bool showtime;
    public int remaintime;
  }
}
