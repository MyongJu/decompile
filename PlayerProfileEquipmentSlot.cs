﻿// Decompiled with JetBrains decompiler
// Type: PlayerProfileEquipmentSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class PlayerProfileEquipmentSlot : MonoBehaviour
{
  [SerializeField]
  private GameObject m_Dot;
  [SerializeField]
  private GameObject m_Equipped;
  [SerializeField]
  private GameObject m_Unequipped;
  [SerializeField]
  private UITexture m_ItemIcon;
  [SerializeField]
  private UITexture m_ItemQuality;
  [SerializeField]
  private UITexture m_ItemCategory;
  [SerializeField]
  private HeroItemType m_EquipmentType;
  [SerializeField]
  private BagType m_BagType;
  [SerializeField]
  private Transform m_Border;
  private bool m_Start;
  private InventoryItemData m_InventoryItemData;

  private void OnEnable()
  {
    if (this.m_Start)
    {
      this.UpdateCategory();
      this.UpdateSlot();
    }
    this.AddEventHandler();
  }

  private void OnDisable()
  {
    this.RemoveEventHandler();
  }

  private void Start()
  {
    this.m_Start = true;
    this.UpdateCategory();
    this.UpdateSlot();
  }

  private void UpdateCategory()
  {
    this.SetItemCategory(ConfigEquipmentMainInfo.GetCategoryPath(this.m_EquipmentType, this.m_BagType));
    this.SetUnequipped(true);
  }

  private void SetUnequipped(bool active)
  {
    if (!((UnityEngine.Object) this.m_Unequipped != (UnityEngine.Object) null))
      return;
    this.m_Unequipped.SetActive(active);
  }

  private int GetItemEquipId()
  {
    if (this.m_BagType == BagType.DragonKnight)
    {
      if (PlayerData.inst.dragonKnightData != null && PlayerData.inst.dragonKnightData != null)
        return PlayerData.inst.dragonKnightData.equipments.GetSlotEquipID((int) this.m_EquipmentType);
    }
    else if (this.m_BagType == BagType.Hero)
      return PlayerData.inst.heroData.equipments.GetSlotEquipID((int) this.m_EquipmentType);
    return 0;
  }

  private void UpdateSlot()
  {
    InventoryItemData myItemByItemId = DBManager.inst.GetInventory(this.m_BagType).GetMyItemByItemID((long) this.GetItemEquipId());
    if (myItemByItemId != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, myItemByItemId.equipment.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemQuality, myItemByItemId.equipment.QualityImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this.m_Equipped.SetActive(true);
      this.m_Unequipped.SetActive(false);
      this.m_Dot.SetActive(false);
    }
    else
    {
      bool flag = DBManager.inst.GetInventory(this.m_BagType).HasMyEquipmentByType((int) this.m_EquipmentType);
      this.m_Equipped.SetActive(false);
      this.m_Unequipped.SetActive(true);
      this.m_Dot.SetActive(flag);
    }
  }

  private void SetItemCategory(string path)
  {
    if (!((UnityEngine.Object) this.m_ItemCategory != (UnityEngine.Object) null))
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemCategory, path, (System.Action<bool>) null, true, false, string.Empty);
  }

  public void OnPressHandler()
  {
    InventoryItemData myItemByItemId = DBManager.inst.GetInventory(this.m_BagType).GetMyItemByItemID((long) this.GetItemEquipId());
    if (myItemByItemId == null)
      return;
    Utils.DelayShowTip(myItemByItemId.equipment.itemID, this.m_Border, myItemByItemId.itemId, myItemByItemId.uid, 0);
  }

  public void OnReleaseHandler()
  {
    Utils.StopShowItemTip();
  }

  private void OnHeroDataUpdated(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.UpdateCategory();
    this.UpdateSlot();
  }

  private void OnHeroInventoryDataUpdated(long uid, long itemId)
  {
    this.OnHeroDataUpdated(uid);
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_hero.onDataUpdated += new System.Action<long>(this.OnHeroDataUpdated);
    DBManager.inst.GetInventory(BagType.Hero).onDataUpdated += new System.Action<long, long>(this.OnHeroInventoryDataUpdated);
  }

  private void RemoveEventHandler()
  {
    if (!GameEngine.IsAvailable)
      return;
    DBManager.inst.DB_hero.onDataUpdated -= new System.Action<long>(this.OnHeroDataUpdated);
    DBManager.inst.GetInventory(BagType.Hero).onDataUpdated -= new System.Action<long, long>(this.OnHeroInventoryDataUpdated);
  }

  public void OnClicked()
  {
    PlayerEquipmentDialog.Parameter parameter = new PlayerEquipmentDialog.Parameter();
    parameter.equipmentType = this.m_EquipmentType;
    parameter.bagType = this.m_BagType;
    if (this.m_BagType == BagType.Hero)
      UIManager.inst.OpenDlg("PlayerProfile/PlayerEquipmentDlg", (UI.Dialog.DialogParameter) parameter, true, true, true);
    else
      UIManager.inst.OpenDlg("PlayerProfile/DragonKnightEquipmentDialog", (UI.Dialog.DialogParameter) parameter, true, true, true);
  }
}
