﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.ProductData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using com.amazon.device.iap.cpt.json;
using System;
using System.Collections.Generic;

namespace com.amazon.device.iap.cpt
{
  public sealed class ProductData : Jsonable
  {
    private static AmazonLogger logger = new AmazonLogger("Pi");

    public string Sku { get; set; }

    public string ProductType { get; set; }

    public string Price { get; set; }

    public string Title { get; set; }

    public string Description { get; set; }

    public string SmallIconUrl { get; set; }

    public CoinsReward CoinsReward { get; set; }

    public string ToJson()
    {
      try
      {
        return Json.Serialize((object) this.GetObjectDictionary());
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while Jsoning", (Exception) ex);
      }
    }

    public override Dictionary<string, object> GetObjectDictionary()
    {
      try
      {
        return new Dictionary<string, object>()
        {
          {
            "sku",
            (object) this.Sku
          },
          {
            "productType",
            (object) this.ProductType
          },
          {
            "price",
            (object) this.Price
          },
          {
            "title",
            (object) this.Title
          },
          {
            "description",
            (object) this.Description
          },
          {
            "smallIconUrl",
            (object) this.SmallIconUrl
          },
          {
            "coinsReward",
            this.CoinsReward == null ? (object) (Dictionary<string, object>) null : (object) this.CoinsReward.GetObjectDictionary()
          }
        };
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while getting object dictionary", (Exception) ex);
      }
    }

    public static ProductData CreateFromDictionary(Dictionary<string, object> jsonMap)
    {
      try
      {
        if (jsonMap == null)
          return (ProductData) null;
        ProductData productData = new ProductData();
        if (jsonMap.ContainsKey("sku"))
          productData.Sku = (string) jsonMap["sku"];
        if (jsonMap.ContainsKey("productType"))
          productData.ProductType = (string) jsonMap["productType"];
        if (jsonMap.ContainsKey("price"))
          productData.Price = (string) jsonMap["price"];
        if (jsonMap.ContainsKey("title"))
          productData.Title = (string) jsonMap["title"];
        if (jsonMap.ContainsKey("description"))
          productData.Description = (string) jsonMap["description"];
        if (jsonMap.ContainsKey("smallIconUrl"))
          productData.SmallIconUrl = (string) jsonMap["smallIconUrl"];
        if (jsonMap.ContainsKey("coinsReward"))
          productData.CoinsReward = CoinsReward.CreateFromDictionary(jsonMap["coinsReward"] as Dictionary<string, object>);
        return productData;
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while creating Object from dicionary", (Exception) ex);
      }
    }

    public static ProductData CreateFromJson(string jsonMessage)
    {
      try
      {
        Dictionary<string, object> jsonMap = Json.Deserialize(jsonMessage) as Dictionary<string, object>;
        Jsonable.CheckForErrors(jsonMap);
        return ProductData.CreateFromDictionary(jsonMap);
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while UnJsoning", (Exception) ex);
      }
    }

    public static Dictionary<string, ProductData> MapFromJson(Dictionary<string, object> jsonMap)
    {
      Dictionary<string, ProductData> dictionary = new Dictionary<string, ProductData>();
      using (Dictionary<string, object>.Enumerator enumerator = jsonMap.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, object> current = enumerator.Current;
          ProductData fromDictionary = ProductData.CreateFromDictionary(current.Value as Dictionary<string, object>);
          dictionary.Add(current.Key, fromDictionary);
        }
      }
      return dictionary;
    }

    public static List<ProductData> ListFromJson(List<object> array)
    {
      List<ProductData> productDataList = new List<ProductData>();
      using (List<object>.Enumerator enumerator = array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          object current = enumerator.Current;
          productDataList.Add(ProductData.CreateFromDictionary(current as Dictionary<string, object>));
        }
      }
      return productDataList;
    }
  }
}
