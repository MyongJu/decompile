﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.Jsonable
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace com.amazon.device.iap.cpt
{
  public abstract class Jsonable
  {
    public static Dictionary<string, object> unrollObjectIntoMap<T>(Dictionary<string, T> obj) where T : Jsonable
    {
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      using (Dictionary<string, T>.Enumerator enumerator = obj.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, T> current = enumerator.Current;
          dictionary.Add(current.Key, (object) current.Value.GetObjectDictionary());
        }
      }
      return dictionary;
    }

    public static List<object> unrollObjectIntoList<T>(List<T> obj) where T : Jsonable
    {
      List<object> objectList = new List<object>();
      using (List<T>.Enumerator enumerator = obj.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Jsonable current = (Jsonable) enumerator.Current;
          objectList.Add((object) current.GetObjectDictionary());
        }
      }
      return objectList;
    }

    public abstract Dictionary<string, object> GetObjectDictionary();

    public static void CheckForErrors(Dictionary<string, object> jsonMap)
    {
      object obj;
      if (jsonMap.TryGetValue("error", out obj))
        throw new AmazonException(obj as string);
    }
  }
}
