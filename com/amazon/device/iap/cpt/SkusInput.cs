﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.SkusInput
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using com.amazon.device.iap.cpt.json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace com.amazon.device.iap.cpt
{
  public sealed class SkusInput : Jsonable
  {
    private static AmazonLogger logger = new AmazonLogger("Pi");

    public List<string> Skus { get; set; }

    public string ToJson()
    {
      try
      {
        return Json.Serialize((object) this.GetObjectDictionary());
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while Jsoning", (Exception) ex);
      }
    }

    public override Dictionary<string, object> GetObjectDictionary()
    {
      try
      {
        return new Dictionary<string, object>()
        {
          {
            "skus",
            (object) this.Skus
          }
        };
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while getting object dictionary", (Exception) ex);
      }
    }

    public static SkusInput CreateFromDictionary(Dictionary<string, object> jsonMap)
    {
      try
      {
        if (jsonMap == null)
          return (SkusInput) null;
        SkusInput skusInput = new SkusInput();
        if (jsonMap.ContainsKey("skus"))
          skusInput.Skus = ((IEnumerable<object>) jsonMap["skus"]).Select<object, string>((Func<object, string>) (element => (string) element)).ToList<string>();
        return skusInput;
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while creating Object from dicionary", (Exception) ex);
      }
    }

    public static SkusInput CreateFromJson(string jsonMessage)
    {
      try
      {
        Dictionary<string, object> jsonMap = Json.Deserialize(jsonMessage) as Dictionary<string, object>;
        Jsonable.CheckForErrors(jsonMap);
        return SkusInput.CreateFromDictionary(jsonMap);
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while UnJsoning", (Exception) ex);
      }
    }

    public static Dictionary<string, SkusInput> MapFromJson(Dictionary<string, object> jsonMap)
    {
      Dictionary<string, SkusInput> dictionary = new Dictionary<string, SkusInput>();
      using (Dictionary<string, object>.Enumerator enumerator = jsonMap.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, object> current = enumerator.Current;
          SkusInput fromDictionary = SkusInput.CreateFromDictionary(current.Value as Dictionary<string, object>);
          dictionary.Add(current.Key, fromDictionary);
        }
      }
      return dictionary;
    }

    public static List<SkusInput> ListFromJson(List<object> array)
    {
      List<SkusInput> skusInputList = new List<SkusInput>();
      using (List<object>.Enumerator enumerator = array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          object current = enumerator.Current;
          skusInputList.Add(SkusInput.CreateFromDictionary(current as Dictionary<string, object>));
        }
      }
      return skusInputList;
    }
  }
}
