﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.GetPurchaseUpdatesResponse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using com.amazon.device.iap.cpt.json;
using System;
using System.Collections.Generic;

namespace com.amazon.device.iap.cpt
{
  public sealed class GetPurchaseUpdatesResponse : Jsonable
  {
    private static AmazonLogger logger = new AmazonLogger("Pi");

    public string RequestId { get; set; }

    public AmazonUserData AmazonUserData { get; set; }

    public List<PurchaseReceipt> Receipts { get; set; }

    public string Status { get; set; }

    public bool HasMore { get; set; }

    public string ToJson()
    {
      try
      {
        return Json.Serialize((object) this.GetObjectDictionary());
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while Jsoning", (Exception) ex);
      }
    }

    public override Dictionary<string, object> GetObjectDictionary()
    {
      try
      {
        return new Dictionary<string, object>()
        {
          {
            "requestId",
            (object) this.RequestId
          },
          {
            "amazonUserData",
            this.AmazonUserData == null ? (object) (Dictionary<string, object>) null : (object) this.AmazonUserData.GetObjectDictionary()
          },
          {
            "receipts",
            this.Receipts == null ? (object) (List<object>) null : (object) Jsonable.unrollObjectIntoList<PurchaseReceipt>(this.Receipts)
          },
          {
            "status",
            (object) this.Status
          },
          {
            "hasMore",
            (object) this.HasMore
          }
        };
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while getting object dictionary", (Exception) ex);
      }
    }

    public static GetPurchaseUpdatesResponse CreateFromDictionary(Dictionary<string, object> jsonMap)
    {
      try
      {
        if (jsonMap == null)
          return (GetPurchaseUpdatesResponse) null;
        GetPurchaseUpdatesResponse purchaseUpdatesResponse = new GetPurchaseUpdatesResponse();
        if (jsonMap.ContainsKey("requestId"))
          purchaseUpdatesResponse.RequestId = (string) jsonMap["requestId"];
        if (jsonMap.ContainsKey("amazonUserData"))
          purchaseUpdatesResponse.AmazonUserData = AmazonUserData.CreateFromDictionary(jsonMap["amazonUserData"] as Dictionary<string, object>);
        if (jsonMap.ContainsKey("receipts"))
          purchaseUpdatesResponse.Receipts = PurchaseReceipt.ListFromJson(jsonMap["receipts"] as List<object>);
        if (jsonMap.ContainsKey("status"))
          purchaseUpdatesResponse.Status = (string) jsonMap["status"];
        if (jsonMap.ContainsKey("hasMore"))
          purchaseUpdatesResponse.HasMore = (bool) jsonMap["hasMore"];
        return purchaseUpdatesResponse;
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while creating Object from dicionary", (Exception) ex);
      }
    }

    public static GetPurchaseUpdatesResponse CreateFromJson(string jsonMessage)
    {
      try
      {
        Dictionary<string, object> jsonMap = Json.Deserialize(jsonMessage) as Dictionary<string, object>;
        Jsonable.CheckForErrors(jsonMap);
        return GetPurchaseUpdatesResponse.CreateFromDictionary(jsonMap);
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while UnJsoning", (Exception) ex);
      }
    }

    public static Dictionary<string, GetPurchaseUpdatesResponse> MapFromJson(Dictionary<string, object> jsonMap)
    {
      Dictionary<string, GetPurchaseUpdatesResponse> dictionary = new Dictionary<string, GetPurchaseUpdatesResponse>();
      using (Dictionary<string, object>.Enumerator enumerator = jsonMap.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, object> current = enumerator.Current;
          GetPurchaseUpdatesResponse fromDictionary = GetPurchaseUpdatesResponse.CreateFromDictionary(current.Value as Dictionary<string, object>);
          dictionary.Add(current.Key, fromDictionary);
        }
      }
      return dictionary;
    }

    public static List<GetPurchaseUpdatesResponse> ListFromJson(List<object> array)
    {
      List<GetPurchaseUpdatesResponse> purchaseUpdatesResponseList = new List<GetPurchaseUpdatesResponse>();
      using (List<object>.Enumerator enumerator = array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          object current = enumerator.Current;
          purchaseUpdatesResponseList.Add(GetPurchaseUpdatesResponse.CreateFromDictionary(current as Dictionary<string, object>));
        }
      }
      return purchaseUpdatesResponseList;
    }
  }
}
