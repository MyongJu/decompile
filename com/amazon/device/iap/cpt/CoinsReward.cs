﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.CoinsReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using com.amazon.device.iap.cpt.json;
using System;
using System.Collections.Generic;

namespace com.amazon.device.iap.cpt
{
  public sealed class CoinsReward : Jsonable
  {
    private static AmazonLogger logger = new AmazonLogger("Pi");

    public int Amount { get; set; }

    public string ToJson()
    {
      try
      {
        return Json.Serialize((object) this.GetObjectDictionary());
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while Jsoning", (Exception) ex);
      }
    }

    public override Dictionary<string, object> GetObjectDictionary()
    {
      try
      {
        return new Dictionary<string, object>()
        {
          {
            "amount",
            (object) this.Amount
          }
        };
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while getting object dictionary", (Exception) ex);
      }
    }

    public static CoinsReward CreateFromDictionary(Dictionary<string, object> jsonMap)
    {
      try
      {
        if (jsonMap == null)
          return (CoinsReward) null;
        CoinsReward coinsReward = new CoinsReward();
        if (jsonMap.ContainsKey("amount"))
          coinsReward.Amount = Convert.ToInt32(jsonMap["amount"]);
        return coinsReward;
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while creating Object from dicionary", (Exception) ex);
      }
    }

    public static CoinsReward CreateFromJson(string jsonMessage)
    {
      try
      {
        Dictionary<string, object> jsonMap = Json.Deserialize(jsonMessage) as Dictionary<string, object>;
        Jsonable.CheckForErrors(jsonMap);
        return CoinsReward.CreateFromDictionary(jsonMap);
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while UnJsoning", (Exception) ex);
      }
    }

    public static Dictionary<string, CoinsReward> MapFromJson(Dictionary<string, object> jsonMap)
    {
      Dictionary<string, CoinsReward> dictionary = new Dictionary<string, CoinsReward>();
      using (Dictionary<string, object>.Enumerator enumerator = jsonMap.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, object> current = enumerator.Current;
          CoinsReward fromDictionary = CoinsReward.CreateFromDictionary(current.Value as Dictionary<string, object>);
          dictionary.Add(current.Key, fromDictionary);
        }
      }
      return dictionary;
    }

    public static List<CoinsReward> ListFromJson(List<object> array)
    {
      List<CoinsReward> coinsRewardList = new List<CoinsReward>();
      using (List<object>.Enumerator enumerator = array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          object current = enumerator.Current;
          coinsRewardList.Add(CoinsReward.CreateFromDictionary(current as Dictionary<string, object>));
        }
      }
      return coinsRewardList;
    }
  }
}
