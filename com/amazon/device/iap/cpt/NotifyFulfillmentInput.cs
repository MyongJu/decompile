﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.NotifyFulfillmentInput
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using com.amazon.device.iap.cpt.json;
using System;
using System.Collections.Generic;

namespace com.amazon.device.iap.cpt
{
  public sealed class NotifyFulfillmentInput : Jsonable
  {
    private static AmazonLogger logger = new AmazonLogger("Pi");

    public string ReceiptId { get; set; }

    public string FulfillmentResult { get; set; }

    public string ToJson()
    {
      try
      {
        return Json.Serialize((object) this.GetObjectDictionary());
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while Jsoning", (Exception) ex);
      }
    }

    public override Dictionary<string, object> GetObjectDictionary()
    {
      try
      {
        return new Dictionary<string, object>()
        {
          {
            "receiptId",
            (object) this.ReceiptId
          },
          {
            "fulfillmentResult",
            (object) this.FulfillmentResult
          }
        };
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while getting object dictionary", (Exception) ex);
      }
    }

    public static NotifyFulfillmentInput CreateFromDictionary(Dictionary<string, object> jsonMap)
    {
      try
      {
        if (jsonMap == null)
          return (NotifyFulfillmentInput) null;
        NotifyFulfillmentInput fulfillmentInput = new NotifyFulfillmentInput();
        if (jsonMap.ContainsKey("receiptId"))
          fulfillmentInput.ReceiptId = (string) jsonMap["receiptId"];
        if (jsonMap.ContainsKey("fulfillmentResult"))
          fulfillmentInput.FulfillmentResult = (string) jsonMap["fulfillmentResult"];
        return fulfillmentInput;
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while creating Object from dicionary", (Exception) ex);
      }
    }

    public static NotifyFulfillmentInput CreateFromJson(string jsonMessage)
    {
      try
      {
        Dictionary<string, object> jsonMap = Json.Deserialize(jsonMessage) as Dictionary<string, object>;
        Jsonable.CheckForErrors(jsonMap);
        return NotifyFulfillmentInput.CreateFromDictionary(jsonMap);
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while UnJsoning", (Exception) ex);
      }
    }

    public static Dictionary<string, NotifyFulfillmentInput> MapFromJson(Dictionary<string, object> jsonMap)
    {
      Dictionary<string, NotifyFulfillmentInput> dictionary = new Dictionary<string, NotifyFulfillmentInput>();
      using (Dictionary<string, object>.Enumerator enumerator = jsonMap.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, object> current = enumerator.Current;
          NotifyFulfillmentInput fromDictionary = NotifyFulfillmentInput.CreateFromDictionary(current.Value as Dictionary<string, object>);
          dictionary.Add(current.Key, fromDictionary);
        }
      }
      return dictionary;
    }

    public static List<NotifyFulfillmentInput> ListFromJson(List<object> array)
    {
      List<NotifyFulfillmentInput> fulfillmentInputList = new List<NotifyFulfillmentInput>();
      using (List<object>.Enumerator enumerator = array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          object current = enumerator.Current;
          fulfillmentInputList.Add(NotifyFulfillmentInput.CreateFromDictionary(current as Dictionary<string, object>));
        }
      }
      return fulfillmentInputList;
    }
  }
}
