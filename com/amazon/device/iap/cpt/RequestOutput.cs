﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.RequestOutput
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using com.amazon.device.iap.cpt.json;
using System;
using System.Collections.Generic;

namespace com.amazon.device.iap.cpt
{
  public sealed class RequestOutput : Jsonable
  {
    private static AmazonLogger logger = new AmazonLogger("Pi");

    public string RequestId { get; set; }

    public string ToJson()
    {
      try
      {
        return Json.Serialize((object) this.GetObjectDictionary());
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while Jsoning", (Exception) ex);
      }
    }

    public override Dictionary<string, object> GetObjectDictionary()
    {
      try
      {
        return new Dictionary<string, object>()
        {
          {
            "requestId",
            (object) this.RequestId
          }
        };
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while getting object dictionary", (Exception) ex);
      }
    }

    public static RequestOutput CreateFromDictionary(Dictionary<string, object> jsonMap)
    {
      try
      {
        if (jsonMap == null)
          return (RequestOutput) null;
        RequestOutput requestOutput = new RequestOutput();
        if (jsonMap.ContainsKey("requestId"))
          requestOutput.RequestId = (string) jsonMap["requestId"];
        return requestOutput;
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while creating Object from dicionary", (Exception) ex);
      }
    }

    public static RequestOutput CreateFromJson(string jsonMessage)
    {
      try
      {
        Dictionary<string, object> jsonMap = Json.Deserialize(jsonMessage) as Dictionary<string, object>;
        Jsonable.CheckForErrors(jsonMap);
        return RequestOutput.CreateFromDictionary(jsonMap);
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while UnJsoning", (Exception) ex);
      }
    }

    public static Dictionary<string, RequestOutput> MapFromJson(Dictionary<string, object> jsonMap)
    {
      Dictionary<string, RequestOutput> dictionary = new Dictionary<string, RequestOutput>();
      using (Dictionary<string, object>.Enumerator enumerator = jsonMap.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, object> current = enumerator.Current;
          RequestOutput fromDictionary = RequestOutput.CreateFromDictionary(current.Value as Dictionary<string, object>);
          dictionary.Add(current.Key, fromDictionary);
        }
      }
      return dictionary;
    }

    public static List<RequestOutput> ListFromJson(List<object> array)
    {
      List<RequestOutput> requestOutputList = new List<RequestOutput>();
      using (List<object>.Enumerator enumerator = array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          object current = enumerator.Current;
          requestOutputList.Add(RequestOutput.CreateFromDictionary(current as Dictionary<string, object>));
        }
      }
      return requestOutputList;
    }
  }
}
