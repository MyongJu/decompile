﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.AmazonLogger
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using com.amazon.device.iap.cpt.log;

namespace com.amazon.device.iap.cpt
{
  public class AmazonLogger
  {
    private readonly string tag;

    public AmazonLogger(string tag)
    {
      this.tag = tag;
    }

    public void Debug(string msg)
    {
      AmazonLogging.Log(AmazonLogging.AmazonLoggingLevel.Verbose, this.tag, msg);
    }

    public string getTag()
    {
      return this.tag;
    }
  }
}
