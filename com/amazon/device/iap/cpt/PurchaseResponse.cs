﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.PurchaseResponse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using com.amazon.device.iap.cpt.json;
using System;
using System.Collections.Generic;

namespace com.amazon.device.iap.cpt
{
  public sealed class PurchaseResponse : Jsonable
  {
    private static AmazonLogger logger = new AmazonLogger("Pi");

    public string RequestId { get; set; }

    public AmazonUserData AmazonUserData { get; set; }

    public PurchaseReceipt PurchaseReceipt { get; set; }

    public string Status { get; set; }

    public string ToJson()
    {
      try
      {
        return Json.Serialize((object) this.GetObjectDictionary());
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while Jsoning", (Exception) ex);
      }
    }

    public override Dictionary<string, object> GetObjectDictionary()
    {
      try
      {
        return new Dictionary<string, object>()
        {
          {
            "requestId",
            (object) this.RequestId
          },
          {
            "amazonUserData",
            this.AmazonUserData == null ? (object) (Dictionary<string, object>) null : (object) this.AmazonUserData.GetObjectDictionary()
          },
          {
            "purchaseReceipt",
            this.PurchaseReceipt == null ? (object) (Dictionary<string, object>) null : (object) this.PurchaseReceipt.GetObjectDictionary()
          },
          {
            "status",
            (object) this.Status
          }
        };
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while getting object dictionary", (Exception) ex);
      }
    }

    public static PurchaseResponse CreateFromDictionary(Dictionary<string, object> jsonMap)
    {
      try
      {
        if (jsonMap == null)
          return (PurchaseResponse) null;
        PurchaseResponse purchaseResponse = new PurchaseResponse();
        if (jsonMap.ContainsKey("requestId"))
          purchaseResponse.RequestId = (string) jsonMap["requestId"];
        if (jsonMap.ContainsKey("amazonUserData"))
          purchaseResponse.AmazonUserData = AmazonUserData.CreateFromDictionary(jsonMap["amazonUserData"] as Dictionary<string, object>);
        if (jsonMap.ContainsKey("purchaseReceipt"))
          purchaseResponse.PurchaseReceipt = PurchaseReceipt.CreateFromDictionary(jsonMap["purchaseReceipt"] as Dictionary<string, object>);
        if (jsonMap.ContainsKey("status"))
          purchaseResponse.Status = (string) jsonMap["status"];
        return purchaseResponse;
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while creating Object from dicionary", (Exception) ex);
      }
    }

    public static PurchaseResponse CreateFromJson(string jsonMessage)
    {
      try
      {
        Dictionary<string, object> jsonMap = Json.Deserialize(jsonMessage) as Dictionary<string, object>;
        Jsonable.CheckForErrors(jsonMap);
        return PurchaseResponse.CreateFromDictionary(jsonMap);
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while UnJsoning", (Exception) ex);
      }
    }

    public static Dictionary<string, PurchaseResponse> MapFromJson(Dictionary<string, object> jsonMap)
    {
      Dictionary<string, PurchaseResponse> dictionary = new Dictionary<string, PurchaseResponse>();
      using (Dictionary<string, object>.Enumerator enumerator = jsonMap.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, object> current = enumerator.Current;
          PurchaseResponse fromDictionary = PurchaseResponse.CreateFromDictionary(current.Value as Dictionary<string, object>);
          dictionary.Add(current.Key, fromDictionary);
        }
      }
      return dictionary;
    }

    public static List<PurchaseResponse> ListFromJson(List<object> array)
    {
      List<PurchaseResponse> purchaseResponseList = new List<PurchaseResponse>();
      using (List<object>.Enumerator enumerator = array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          object current = enumerator.Current;
          purchaseResponseList.Add(PurchaseResponse.CreateFromDictionary(current as Dictionary<string, object>));
        }
      }
      return purchaseResponseList;
    }
  }
}
