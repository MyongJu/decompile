﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.PurchaseReceipt
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using com.amazon.device.iap.cpt.json;
using System;
using System.Collections.Generic;

namespace com.amazon.device.iap.cpt
{
  public sealed class PurchaseReceipt : Jsonable
  {
    private static AmazonLogger logger = new AmazonLogger("Pi");

    public string ReceiptId { get; set; }

    public long CancelDate { get; set; }

    public long PurchaseDate { get; set; }

    public string Sku { get; set; }

    public string ProductType { get; set; }

    public string ToJson()
    {
      try
      {
        return Json.Serialize((object) this.GetObjectDictionary());
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while Jsoning", (Exception) ex);
      }
    }

    public override Dictionary<string, object> GetObjectDictionary()
    {
      try
      {
        return new Dictionary<string, object>()
        {
          {
            "receiptId",
            (object) this.ReceiptId
          },
          {
            "cancelDate",
            (object) this.CancelDate
          },
          {
            "purchaseDate",
            (object) this.PurchaseDate
          },
          {
            "sku",
            (object) this.Sku
          },
          {
            "productType",
            (object) this.ProductType
          }
        };
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while getting object dictionary", (Exception) ex);
      }
    }

    public static PurchaseReceipt CreateFromDictionary(Dictionary<string, object> jsonMap)
    {
      try
      {
        if (jsonMap == null)
          return (PurchaseReceipt) null;
        PurchaseReceipt purchaseReceipt = new PurchaseReceipt();
        if (jsonMap.ContainsKey("receiptId"))
          purchaseReceipt.ReceiptId = (string) jsonMap["receiptId"];
        if (jsonMap.ContainsKey("cancelDate"))
          purchaseReceipt.CancelDate = (long) jsonMap["cancelDate"];
        if (jsonMap.ContainsKey("purchaseDate"))
          purchaseReceipt.PurchaseDate = (long) jsonMap["purchaseDate"];
        if (jsonMap.ContainsKey("sku"))
          purchaseReceipt.Sku = (string) jsonMap["sku"];
        if (jsonMap.ContainsKey("productType"))
          purchaseReceipt.ProductType = (string) jsonMap["productType"];
        return purchaseReceipt;
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while creating Object from dicionary", (Exception) ex);
      }
    }

    public static PurchaseReceipt CreateFromJson(string jsonMessage)
    {
      try
      {
        Dictionary<string, object> jsonMap = Json.Deserialize(jsonMessage) as Dictionary<string, object>;
        Jsonable.CheckForErrors(jsonMap);
        return PurchaseReceipt.CreateFromDictionary(jsonMap);
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while UnJsoning", (Exception) ex);
      }
    }

    public static Dictionary<string, PurchaseReceipt> MapFromJson(Dictionary<string, object> jsonMap)
    {
      Dictionary<string, PurchaseReceipt> dictionary = new Dictionary<string, PurchaseReceipt>();
      using (Dictionary<string, object>.Enumerator enumerator = jsonMap.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, object> current = enumerator.Current;
          PurchaseReceipt fromDictionary = PurchaseReceipt.CreateFromDictionary(current.Value as Dictionary<string, object>);
          dictionary.Add(current.Key, fromDictionary);
        }
      }
      return dictionary;
    }

    public static List<PurchaseReceipt> ListFromJson(List<object> array)
    {
      List<PurchaseReceipt> purchaseReceiptList = new List<PurchaseReceipt>();
      using (List<object>.Enumerator enumerator = array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          object current = enumerator.Current;
          purchaseReceiptList.Add(PurchaseReceipt.CreateFromDictionary(current as Dictionary<string, object>));
        }
      }
      return purchaseReceiptList;
    }
  }
}
