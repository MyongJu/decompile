﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.GetProductDataResponse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using com.amazon.device.iap.cpt.json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace com.amazon.device.iap.cpt
{
  public sealed class GetProductDataResponse : Jsonable
  {
    private static AmazonLogger logger = new AmazonLogger("Pi");

    public string RequestId { get; set; }

    public Dictionary<string, ProductData> ProductDataMap { get; set; }

    public List<string> UnavailableSkus { get; set; }

    public string Status { get; set; }

    public string ToJson()
    {
      try
      {
        return Json.Serialize((object) this.GetObjectDictionary());
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while Jsoning", (Exception) ex);
      }
    }

    public override Dictionary<string, object> GetObjectDictionary()
    {
      try
      {
        return new Dictionary<string, object>()
        {
          {
            "requestId",
            (object) this.RequestId
          },
          {
            "productDataMap",
            this.ProductDataMap == null ? (object) (Dictionary<string, object>) null : (object) Jsonable.unrollObjectIntoMap<ProductData>(this.ProductDataMap)
          },
          {
            "unavailableSkus",
            (object) this.UnavailableSkus
          },
          {
            "status",
            (object) this.Status
          }
        };
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while getting object dictionary", (Exception) ex);
      }
    }

    public static GetProductDataResponse CreateFromDictionary(Dictionary<string, object> jsonMap)
    {
      try
      {
        if (jsonMap == null)
          return (GetProductDataResponse) null;
        GetProductDataResponse productDataResponse = new GetProductDataResponse();
        if (jsonMap.ContainsKey("requestId"))
          productDataResponse.RequestId = (string) jsonMap["requestId"];
        if (jsonMap.ContainsKey("productDataMap"))
          productDataResponse.ProductDataMap = ProductData.MapFromJson(jsonMap["productDataMap"] as Dictionary<string, object>);
        if (jsonMap.ContainsKey("unavailableSkus"))
          productDataResponse.UnavailableSkus = ((IEnumerable<object>) jsonMap["unavailableSkus"]).Select<object, string>((Func<object, string>) (element => (string) element)).ToList<string>();
        if (jsonMap.ContainsKey("status"))
          productDataResponse.Status = (string) jsonMap["status"];
        return productDataResponse;
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while creating Object from dicionary", (Exception) ex);
      }
    }

    public static GetProductDataResponse CreateFromJson(string jsonMessage)
    {
      try
      {
        Dictionary<string, object> jsonMap = Json.Deserialize(jsonMessage) as Dictionary<string, object>;
        Jsonable.CheckForErrors(jsonMap);
        return GetProductDataResponse.CreateFromDictionary(jsonMap);
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while UnJsoning", (Exception) ex);
      }
    }

    public static Dictionary<string, GetProductDataResponse> MapFromJson(Dictionary<string, object> jsonMap)
    {
      Dictionary<string, GetProductDataResponse> dictionary = new Dictionary<string, GetProductDataResponse>();
      using (Dictionary<string, object>.Enumerator enumerator = jsonMap.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, object> current = enumerator.Current;
          GetProductDataResponse fromDictionary = GetProductDataResponse.CreateFromDictionary(current.Value as Dictionary<string, object>);
          dictionary.Add(current.Key, fromDictionary);
        }
      }
      return dictionary;
    }

    public static List<GetProductDataResponse> ListFromJson(List<object> array)
    {
      List<GetProductDataResponse> productDataResponseList = new List<GetProductDataResponse>();
      using (List<object>.Enumerator enumerator = array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          object current = enumerator.Current;
          productDataResponseList.Add(GetProductDataResponse.CreateFromDictionary(current as Dictionary<string, object>));
        }
      }
      return productDataResponseList;
    }
  }
}
