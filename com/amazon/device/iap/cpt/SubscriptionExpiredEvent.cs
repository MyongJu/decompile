﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.SubscriptionExpiredEvent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using com.amazon.device.iap.cpt.json;
using System;
using System.Collections.Generic;

namespace com.amazon.device.iap.cpt
{
  public sealed class SubscriptionExpiredEvent : Jsonable
  {
    private static AmazonLogger logger = new AmazonLogger("Pi");

    public string Sku { get; set; }

    public string ToJson()
    {
      try
      {
        return Json.Serialize((object) this.GetObjectDictionary());
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while Jsoning", (Exception) ex);
      }
    }

    public override Dictionary<string, object> GetObjectDictionary()
    {
      try
      {
        return new Dictionary<string, object>()
        {
          {
            "sku",
            (object) this.Sku
          }
        };
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while getting object dictionary", (Exception) ex);
      }
    }

    public static SubscriptionExpiredEvent CreateFromDictionary(Dictionary<string, object> jsonMap)
    {
      try
      {
        if (jsonMap == null)
          return (SubscriptionExpiredEvent) null;
        SubscriptionExpiredEvent subscriptionExpiredEvent = new SubscriptionExpiredEvent();
        if (jsonMap.ContainsKey("sku"))
          subscriptionExpiredEvent.Sku = (string) jsonMap["sku"];
        return subscriptionExpiredEvent;
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while creating Object from dicionary", (Exception) ex);
      }
    }

    public static SubscriptionExpiredEvent CreateFromJson(string jsonMessage)
    {
      try
      {
        Dictionary<string, object> jsonMap = Json.Deserialize(jsonMessage) as Dictionary<string, object>;
        Jsonable.CheckForErrors(jsonMap);
        return SubscriptionExpiredEvent.CreateFromDictionary(jsonMap);
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while UnJsoning", (Exception) ex);
      }
    }

    public static Dictionary<string, SubscriptionExpiredEvent> MapFromJson(Dictionary<string, object> jsonMap)
    {
      Dictionary<string, SubscriptionExpiredEvent> dictionary = new Dictionary<string, SubscriptionExpiredEvent>();
      using (Dictionary<string, object>.Enumerator enumerator = jsonMap.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, object> current = enumerator.Current;
          SubscriptionExpiredEvent fromDictionary = SubscriptionExpiredEvent.CreateFromDictionary(current.Value as Dictionary<string, object>);
          dictionary.Add(current.Key, fromDictionary);
        }
      }
      return dictionary;
    }

    public static List<SubscriptionExpiredEvent> ListFromJson(List<object> array)
    {
      List<SubscriptionExpiredEvent> subscriptionExpiredEventList = new List<SubscriptionExpiredEvent>();
      using (List<object>.Enumerator enumerator = array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          object current = enumerator.Current;
          subscriptionExpiredEventList.Add(SubscriptionExpiredEvent.CreateFromDictionary(current as Dictionary<string, object>));
        }
      }
      return subscriptionExpiredEventList;
    }
  }
}
