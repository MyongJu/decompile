﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.AmazonException
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.Serialization;

namespace com.amazon.device.iap.cpt
{
  public class AmazonException : ApplicationException
  {
    public AmazonException()
    {
    }

    public AmazonException(string message)
      : base(message)
    {
    }

    public AmazonException(string message, Exception inner)
      : base(message, inner)
    {
    }

    protected AmazonException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }
  }
}
