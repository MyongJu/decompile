﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.log.AmazonLogging
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace com.amazon.device.iap.cpt.log
{
  public class AmazonLogging
  {
    private const string errorMessage = "{0} error: {1}";
    private const string warningMessage = "{0} warning: {1}";
    private const string logMessage = "{0}: {1}";

    public static void LogError(AmazonLogging.AmazonLoggingLevel reportLevel, string service, string message)
    {
      if (reportLevel == AmazonLogging.AmazonLoggingLevel.Silent)
        return;
      string message1 = string.Format("{0} error: {1}", (object) service, (object) message);
      switch (reportLevel - 1)
      {
        case AmazonLogging.AmazonLoggingLevel.Silent:
        case AmazonLogging.AmazonLoggingLevel.ErrorsAsExceptions:
        case AmazonLogging.AmazonLoggingLevel.Errors:
        case AmazonLogging.AmazonLoggingLevel.Warnings:
          Debug.LogError((object) message1);
          break;
        case AmazonLogging.AmazonLoggingLevel.Critical:
          throw new Exception(message1);
      }
    }

    public static void LogWarning(AmazonLogging.AmazonLoggingLevel reportLevel, string service, string message)
    {
      switch (reportLevel)
      {
        case AmazonLogging.AmazonLoggingLevel.Warnings:
        case AmazonLogging.AmazonLoggingLevel.Verbose:
          Debug.LogWarning((object) string.Format("{0} warning: {1}", (object) service, (object) message));
          break;
      }
    }

    public static void Log(AmazonLogging.AmazonLoggingLevel reportLevel, string service, string message)
    {
      if (reportLevel != AmazonLogging.AmazonLoggingLevel.Verbose)
        return;
      Debug.Log((object) string.Format("{0}: {1}", (object) service, (object) message));
    }

    public static AmazonLogging.SDKLoggingLevel pluginToSDKLoggingLevel(AmazonLogging.AmazonLoggingLevel pluginLoggingLevel)
    {
      switch (pluginLoggingLevel)
      {
        case AmazonLogging.AmazonLoggingLevel.Silent:
          return AmazonLogging.SDKLoggingLevel.LogOff;
        case AmazonLogging.AmazonLoggingLevel.Critical:
          return AmazonLogging.SDKLoggingLevel.LogCritical;
        case AmazonLogging.AmazonLoggingLevel.ErrorsAsExceptions:
        case AmazonLogging.AmazonLoggingLevel.Errors:
          return AmazonLogging.SDKLoggingLevel.LogError;
        case AmazonLogging.AmazonLoggingLevel.Warnings:
        case AmazonLogging.AmazonLoggingLevel.Verbose:
          return AmazonLogging.SDKLoggingLevel.LogWarning;
        default:
          return AmazonLogging.SDKLoggingLevel.LogWarning;
      }
    }

    public enum AmazonLoggingLevel
    {
      Silent,
      Critical,
      ErrorsAsExceptions,
      Errors,
      Warnings,
      Verbose,
    }

    public enum SDKLoggingLevel
    {
      LogOff,
      LogCritical,
      LogError,
      LogWarning,
    }
  }
}
