﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.AmazonUserData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using com.amazon.device.iap.cpt.json;
using System;
using System.Collections.Generic;

namespace com.amazon.device.iap.cpt
{
  public sealed class AmazonUserData : Jsonable
  {
    private static AmazonLogger logger = new AmazonLogger("Pi");

    public string UserId { get; set; }

    public string Marketplace { get; set; }

    public string ToJson()
    {
      try
      {
        return Json.Serialize((object) this.GetObjectDictionary());
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while Jsoning", (Exception) ex);
      }
    }

    public override Dictionary<string, object> GetObjectDictionary()
    {
      try
      {
        return new Dictionary<string, object>()
        {
          {
            "userId",
            (object) this.UserId
          },
          {
            "marketplace",
            (object) this.Marketplace
          }
        };
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while getting object dictionary", (Exception) ex);
      }
    }

    public static AmazonUserData CreateFromDictionary(Dictionary<string, object> jsonMap)
    {
      try
      {
        if (jsonMap == null)
          return (AmazonUserData) null;
        AmazonUserData amazonUserData = new AmazonUserData();
        if (jsonMap.ContainsKey("userId"))
          amazonUserData.UserId = (string) jsonMap["userId"];
        if (jsonMap.ContainsKey("marketplace"))
          amazonUserData.Marketplace = (string) jsonMap["marketplace"];
        return amazonUserData;
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while creating Object from dicionary", (Exception) ex);
      }
    }

    public static AmazonUserData CreateFromJson(string jsonMessage)
    {
      try
      {
        Dictionary<string, object> jsonMap = Json.Deserialize(jsonMessage) as Dictionary<string, object>;
        Jsonable.CheckForErrors(jsonMap);
        return AmazonUserData.CreateFromDictionary(jsonMap);
      }
      catch (ApplicationException ex)
      {
        throw new AmazonException("Error encountered while UnJsoning", (Exception) ex);
      }
    }

    public static Dictionary<string, AmazonUserData> MapFromJson(Dictionary<string, object> jsonMap)
    {
      Dictionary<string, AmazonUserData> dictionary = new Dictionary<string, AmazonUserData>();
      using (Dictionary<string, object>.Enumerator enumerator = jsonMap.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, object> current = enumerator.Current;
          AmazonUserData fromDictionary = AmazonUserData.CreateFromDictionary(current.Value as Dictionary<string, object>);
          dictionary.Add(current.Key, fromDictionary);
        }
      }
      return dictionary;
    }

    public static List<AmazonUserData> ListFromJson(List<object> array)
    {
      List<AmazonUserData> amazonUserDataList = new List<AmazonUserData>();
      using (List<object>.Enumerator enumerator = array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          object current = enumerator.Current;
          amazonUserDataList.Add(AmazonUserData.CreateFromDictionary(current as Dictionary<string, object>));
        }
      }
      return amazonUserDataList;
    }
  }
}
