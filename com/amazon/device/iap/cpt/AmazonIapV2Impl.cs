﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.AmazonIapV2Impl
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using com.amazon.device.iap.cpt.json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using UnityEngine;

namespace com.amazon.device.iap.cpt
{
  public abstract class AmazonIapV2Impl : MonoBehaviour, IAmazonIapV2
  {
    private static readonly Dictionary<string, IDelegator> callbackDictionary = new Dictionary<string, IDelegator>();
    private static readonly object callbackLock = new object();
    private static readonly Dictionary<string, List<IDelegator>> eventListeners = new Dictionary<string, List<IDelegator>>();
    private static readonly object eventLock = new object();
    private static AmazonLogger logger;

    private AmazonIapV2Impl()
    {
    }

    public static IAmazonIapV2 Instance
    {
      get
      {
        return AmazonIapV2Impl.Builder.instance;
      }
    }

    public static void callback(string jsonMessage)
    {
      try
      {
        AmazonIapV2Impl.logger.Debug("Executing callback");
        Dictionary<string, object> dictionary = Json.Deserialize(jsonMessage) as Dictionary<string, object>;
        string callerId = dictionary["callerId"] as string;
        AmazonIapV2Impl.callbackCaller(dictionary["response"] as Dictionary<string, object>, callerId);
      }
      catch (KeyNotFoundException ex)
      {
        AmazonIapV2Impl.logger.Debug("callerId not found in callback");
        throw new AmazonException("Internal Error: Unknown callback id", (Exception) ex);
      }
      catch (AmazonException ex)
      {
        AmazonIapV2Impl.logger.Debug("Async call threw exception: " + ex.ToString());
      }
    }

    private static void callbackCaller(Dictionary<string, object> response, string callerId)
    {
      IDelegator delegator = (IDelegator) null;
      try
      {
        Jsonable.CheckForErrors(response);
        lock (AmazonIapV2Impl.callbackLock)
        {
          delegator = AmazonIapV2Impl.callbackDictionary[callerId];
          AmazonIapV2Impl.callbackDictionary.Remove(callerId);
          delegator.ExecuteSuccess(response);
        }
      }
      catch (AmazonException ex)
      {
        lock (AmazonIapV2Impl.callbackLock)
        {
          if (delegator == null)
            delegator = AmazonIapV2Impl.callbackDictionary[callerId];
          AmazonIapV2Impl.callbackDictionary.Remove(callerId);
          delegator.ExecuteError(ex);
        }
      }
    }

    public static void FireEvent(string jsonMessage)
    {
      try
      {
        AmazonIapV2Impl.logger.Debug("eventReceived");
        Dictionary<string, object> dictionary1 = Json.Deserialize(jsonMessage) as Dictionary<string, object>;
        string key = dictionary1["eventId"] as string;
        Dictionary<string, object> dictionary2 = (Dictionary<string, object>) null;
        if (dictionary1.ContainsKey("response"))
        {
          dictionary2 = dictionary1["response"] as Dictionary<string, object>;
          Jsonable.CheckForErrors(dictionary2);
        }
        lock (AmazonIapV2Impl.eventLock)
        {
          List<IDelegator> delegatorList;
          if (AmazonIapV2Impl.eventListeners.TryGetValue(key, out delegatorList))
          {
            using (List<IDelegator>.Enumerator enumerator = delegatorList.GetEnumerator())
            {
              while (enumerator.MoveNext())
              {
                IDelegator current = enumerator.Current;
                if (dictionary2 != null)
                  current.ExecuteSuccess(dictionary2);
                else
                  current.ExecuteSuccess();
              }
            }
          }
          else
            AmazonIapV2Impl.logger.Debug("No listeners found for event " + key + ". Ignoring event");
        }
      }
      catch (AmazonException ex)
      {
        AmazonIapV2Impl.logger.Debug("Event call threw exception: " + ex.ToString());
      }
    }

    public abstract RequestOutput GetUserData();

    public abstract RequestOutput Purchase(SkuInput skuInput);

    public abstract RequestOutput GetProductData(SkusInput skusInput);

    public abstract RequestOutput GetPurchaseUpdates(ResetInput resetInput);

    public abstract void NotifyFulfillment(NotifyFulfillmentInput notifyFulfillmentInput);

    public abstract void UnityFireEvent(string jsonMessage);

    public abstract void AddGetUserDataResponseListener(GetUserDataResponseDelegate responseDelegate);

    public abstract void RemoveGetUserDataResponseListener(GetUserDataResponseDelegate responseDelegate);

    public abstract void AddPurchaseResponseListener(PurchaseResponseDelegate responseDelegate);

    public abstract void RemovePurchaseResponseListener(PurchaseResponseDelegate responseDelegate);

    public abstract void AddGetProductDataResponseListener(GetProductDataResponseDelegate responseDelegate);

    public abstract void RemoveGetProductDataResponseListener(GetProductDataResponseDelegate responseDelegate);

    public abstract void AddGetPurchaseUpdatesResponseListener(GetPurchaseUpdatesResponseDelegate responseDelegate);

    public abstract void RemoveGetPurchaseUpdatesResponseListener(GetPurchaseUpdatesResponseDelegate responseDelegate);

    private abstract class AmazonIapV2Base : AmazonIapV2Impl
    {
      private static readonly object startLock = new object();
      private static volatile bool startCalled = false;

      public AmazonIapV2Base()
      {
        AmazonIapV2Impl.logger = new AmazonLogger(((object) this).GetType().Name);
      }

      protected void Start()
      {
        if (AmazonIapV2Impl.AmazonIapV2Base.startCalled)
          return;
        lock (AmazonIapV2Impl.AmazonIapV2Base.startLock)
        {
          if (AmazonIapV2Impl.AmazonIapV2Base.startCalled)
            return;
          this.Init();
          this.RegisterCallback();
          this.RegisterEventListener();
          this.RegisterCrossPlatformTool();
          AmazonIapV2Impl.AmazonIapV2Base.startCalled = true;
        }
      }

      protected abstract void Init();

      protected abstract void RegisterCallback();

      protected abstract void RegisterEventListener();

      protected abstract void RegisterCrossPlatformTool();

      public override void UnityFireEvent(string jsonMessage)
      {
        AmazonIapV2Impl.FireEvent(jsonMessage);
      }

      public override RequestOutput GetUserData()
      {
        this.Start();
        return RequestOutput.CreateFromJson(this.GetUserDataJson("{}"));
      }

      private string GetUserDataJson(string jsonMessage)
      {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        string userDataJson = this.NativeGetUserDataJson(jsonMessage);
        stopwatch.Stop();
        AmazonIapV2Impl.logger.Debug(string.Format("Successfully called native code in {0} ms", (object) stopwatch.ElapsedMilliseconds));
        return userDataJson;
      }

      protected abstract string NativeGetUserDataJson(string jsonMessage);

      public override RequestOutput Purchase(SkuInput skuInput)
      {
        this.Start();
        return RequestOutput.CreateFromJson(this.PurchaseJson(skuInput.ToJson()));
      }

      private string PurchaseJson(string jsonMessage)
      {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        string str = this.NativePurchaseJson(jsonMessage);
        stopwatch.Stop();
        AmazonIapV2Impl.logger.Debug(string.Format("Successfully called native code in {0} ms", (object) stopwatch.ElapsedMilliseconds));
        return str;
      }

      protected abstract string NativePurchaseJson(string jsonMessage);

      public override RequestOutput GetProductData(SkusInput skusInput)
      {
        this.Start();
        return RequestOutput.CreateFromJson(this.GetProductDataJson(skusInput.ToJson()));
      }

      private string GetProductDataJson(string jsonMessage)
      {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        string productDataJson = this.NativeGetProductDataJson(jsonMessage);
        stopwatch.Stop();
        AmazonIapV2Impl.logger.Debug(string.Format("Successfully called native code in {0} ms", (object) stopwatch.ElapsedMilliseconds));
        return productDataJson;
      }

      protected abstract string NativeGetProductDataJson(string jsonMessage);

      public override RequestOutput GetPurchaseUpdates(ResetInput resetInput)
      {
        this.Start();
        return RequestOutput.CreateFromJson(this.GetPurchaseUpdatesJson(resetInput.ToJson()));
      }

      private string GetPurchaseUpdatesJson(string jsonMessage)
      {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        string purchaseUpdatesJson = this.NativeGetPurchaseUpdatesJson(jsonMessage);
        stopwatch.Stop();
        AmazonIapV2Impl.logger.Debug(string.Format("Successfully called native code in {0} ms", (object) stopwatch.ElapsedMilliseconds));
        return purchaseUpdatesJson;
      }

      protected abstract string NativeGetPurchaseUpdatesJson(string jsonMessage);

      public override void NotifyFulfillment(NotifyFulfillmentInput notifyFulfillmentInput)
      {
        this.Start();
        Jsonable.CheckForErrors(Json.Deserialize(this.NotifyFulfillmentJson(notifyFulfillmentInput.ToJson())) as Dictionary<string, object>);
      }

      private string NotifyFulfillmentJson(string jsonMessage)
      {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        string str = this.NativeNotifyFulfillmentJson(jsonMessage);
        stopwatch.Stop();
        AmazonIapV2Impl.logger.Debug(string.Format("Successfully called native code in {0} ms", (object) stopwatch.ElapsedMilliseconds));
        return str;
      }

      protected abstract string NativeNotifyFulfillmentJson(string jsonMessage);

      public override void AddGetUserDataResponseListener(GetUserDataResponseDelegate responseDelegate)
      {
        this.Start();
        string key = "getUserDataResponse";
        lock (AmazonIapV2Impl.eventLock)
        {
          if (AmazonIapV2Impl.eventListeners.ContainsKey(key))
            AmazonIapV2Impl.eventListeners[key].Add((IDelegator) new GetUserDataResponseDelegator(responseDelegate));
          else
            AmazonIapV2Impl.eventListeners.Add(key, new List<IDelegator>()
            {
              (IDelegator) new GetUserDataResponseDelegator(responseDelegate)
            });
        }
      }

      public override void RemoveGetUserDataResponseListener(GetUserDataResponseDelegate responseDelegate)
      {
        this.Start();
        string key = "getUserDataResponse";
        lock (AmazonIapV2Impl.eventLock)
        {
          if (!AmazonIapV2Impl.eventListeners.ContainsKey(key))
            return;
          using (List<IDelegator>.Enumerator enumerator = AmazonIapV2Impl.eventListeners[key].GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              GetUserDataResponseDelegator current = (GetUserDataResponseDelegator) enumerator.Current;
              if ((MulticastDelegate) current.responseDelegate == (MulticastDelegate) responseDelegate)
              {
                AmazonIapV2Impl.eventListeners[key].Remove((IDelegator) current);
                break;
              }
            }
          }
        }
      }

      public override void AddPurchaseResponseListener(PurchaseResponseDelegate responseDelegate)
      {
        this.Start();
        string key = "purchaseResponse";
        lock (AmazonIapV2Impl.eventLock)
        {
          if (AmazonIapV2Impl.eventListeners.ContainsKey(key))
            AmazonIapV2Impl.eventListeners[key].Add((IDelegator) new PurchaseResponseDelegator(responseDelegate));
          else
            AmazonIapV2Impl.eventListeners.Add(key, new List<IDelegator>()
            {
              (IDelegator) new PurchaseResponseDelegator(responseDelegate)
            });
        }
      }

      public override void RemovePurchaseResponseListener(PurchaseResponseDelegate responseDelegate)
      {
        this.Start();
        string key = "purchaseResponse";
        lock (AmazonIapV2Impl.eventLock)
        {
          if (!AmazonIapV2Impl.eventListeners.ContainsKey(key))
            return;
          using (List<IDelegator>.Enumerator enumerator = AmazonIapV2Impl.eventListeners[key].GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              PurchaseResponseDelegator current = (PurchaseResponseDelegator) enumerator.Current;
              if ((MulticastDelegate) current.responseDelegate == (MulticastDelegate) responseDelegate)
              {
                AmazonIapV2Impl.eventListeners[key].Remove((IDelegator) current);
                break;
              }
            }
          }
        }
      }

      public override void AddGetProductDataResponseListener(GetProductDataResponseDelegate responseDelegate)
      {
        this.Start();
        string key = "getProductDataResponse";
        lock (AmazonIapV2Impl.eventLock)
        {
          if (AmazonIapV2Impl.eventListeners.ContainsKey(key))
            AmazonIapV2Impl.eventListeners[key].Add((IDelegator) new GetProductDataResponseDelegator(responseDelegate));
          else
            AmazonIapV2Impl.eventListeners.Add(key, new List<IDelegator>()
            {
              (IDelegator) new GetProductDataResponseDelegator(responseDelegate)
            });
        }
      }

      public override void RemoveGetProductDataResponseListener(GetProductDataResponseDelegate responseDelegate)
      {
        this.Start();
        string key = "getProductDataResponse";
        lock (AmazonIapV2Impl.eventLock)
        {
          if (!AmazonIapV2Impl.eventListeners.ContainsKey(key))
            return;
          using (List<IDelegator>.Enumerator enumerator = AmazonIapV2Impl.eventListeners[key].GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              GetProductDataResponseDelegator current = (GetProductDataResponseDelegator) enumerator.Current;
              if ((MulticastDelegate) current.responseDelegate == (MulticastDelegate) responseDelegate)
              {
                AmazonIapV2Impl.eventListeners[key].Remove((IDelegator) current);
                break;
              }
            }
          }
        }
      }

      public override void AddGetPurchaseUpdatesResponseListener(GetPurchaseUpdatesResponseDelegate responseDelegate)
      {
        this.Start();
        string key = "getPurchaseUpdatesResponse";
        lock (AmazonIapV2Impl.eventLock)
        {
          if (AmazonIapV2Impl.eventListeners.ContainsKey(key))
            AmazonIapV2Impl.eventListeners[key].Add((IDelegator) new GetPurchaseUpdatesResponseDelegator(responseDelegate));
          else
            AmazonIapV2Impl.eventListeners.Add(key, new List<IDelegator>()
            {
              (IDelegator) new GetPurchaseUpdatesResponseDelegator(responseDelegate)
            });
        }
      }

      public override void RemoveGetPurchaseUpdatesResponseListener(GetPurchaseUpdatesResponseDelegate responseDelegate)
      {
        this.Start();
        string key = "getPurchaseUpdatesResponse";
        lock (AmazonIapV2Impl.eventLock)
        {
          if (!AmazonIapV2Impl.eventListeners.ContainsKey(key))
            return;
          using (List<IDelegator>.Enumerator enumerator = AmazonIapV2Impl.eventListeners[key].GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              GetPurchaseUpdatesResponseDelegator current = (GetPurchaseUpdatesResponseDelegator) enumerator.Current;
              if ((MulticastDelegate) current.responseDelegate == (MulticastDelegate) responseDelegate)
              {
                AmazonIapV2Impl.eventListeners[key].Remove((IDelegator) current);
                break;
              }
            }
          }
        }
      }
    }

    private class AmazonIapV2Default : AmazonIapV2Impl.AmazonIapV2Base
    {
      protected override void Init()
      {
      }

      protected override void RegisterCallback()
      {
      }

      protected override void RegisterEventListener()
      {
      }

      protected override void RegisterCrossPlatformTool()
      {
      }

      protected override string NativeGetUserDataJson(string jsonMessage)
      {
        return "{}";
      }

      protected override string NativePurchaseJson(string jsonMessage)
      {
        return "{}";
      }

      protected override string NativeGetProductDataJson(string jsonMessage)
      {
        return "{}";
      }

      protected override string NativeGetPurchaseUpdatesJson(string jsonMessage)
      {
        return "{}";
      }

      protected override string NativeNotifyFulfillmentJson(string jsonMessage)
      {
        return "{}";
      }
    }

    private abstract class AmazonIapV2DelegatesBase : AmazonIapV2Impl.AmazonIapV2Base
    {
      private const string CrossPlatformTool = "XAMARIN";
      protected AmazonIapV2Impl.CallbackDelegate callbackDelegate;
      protected AmazonIapV2Impl.CallbackDelegate eventDelegate;

      protected override void Init()
      {
        this.NativeInit();
      }

      protected override void RegisterCallback()
      {
        this.callbackDelegate = new AmazonIapV2Impl.CallbackDelegate(AmazonIapV2Impl.callback);
        this.NativeRegisterCallback(this.callbackDelegate);
      }

      protected override void RegisterEventListener()
      {
        this.eventDelegate = new AmazonIapV2Impl.CallbackDelegate(AmazonIapV2Impl.FireEvent);
        this.NativeRegisterEventListener(this.eventDelegate);
      }

      protected override void RegisterCrossPlatformTool()
      {
        this.NativeRegisterCrossPlatformTool("XAMARIN");
      }

      public override void UnityFireEvent(string jsonMessage)
      {
        throw new NotSupportedException("UnityFireEvent is not supported");
      }

      protected abstract void NativeInit();

      protected abstract void NativeRegisterCallback(AmazonIapV2Impl.CallbackDelegate callback);

      protected abstract void NativeRegisterEventListener(AmazonIapV2Impl.CallbackDelegate callback);

      protected abstract void NativeRegisterCrossPlatformTool(string crossPlatformTool);
    }

    private class Builder
    {
      internal static readonly IAmazonIapV2 instance = (IAmazonIapV2) AmazonIapV2Impl.AmazonIapV2UnityAndroid.Instance;
    }

    private class AmazonIapV2UnityAndroid : AmazonIapV2Impl.AmazonIapV2UnityBase
    {
      [DllImport("AmazonIapV2Bridge")]
      private static extern string nativeRegisterCallbackGameObject(string name);

      [DllImport("AmazonIapV2Bridge")]
      private static extern string nativeInit();

      [DllImport("AmazonIapV2Bridge")]
      private static extern string nativeGetUserDataJson(string jsonMessage);

      [DllImport("AmazonIapV2Bridge")]
      private static extern string nativePurchaseJson(string jsonMessage);

      [DllImport("AmazonIapV2Bridge")]
      private static extern string nativeGetProductDataJson(string jsonMessage);

      [DllImport("AmazonIapV2Bridge")]
      private static extern string nativeGetPurchaseUpdatesJson(string jsonMessage);

      [DllImport("AmazonIapV2Bridge")]
      private static extern string nativeNotifyFulfillmentJson(string jsonMessage);

      public static AmazonIapV2Impl.AmazonIapV2UnityAndroid Instance
      {
        get
        {
          return AmazonIapV2Impl.AmazonIapV2UnityBase.getInstance<AmazonIapV2Impl.AmazonIapV2UnityAndroid>();
        }
      }

      protected override void NativeInit()
      {
        AmazonIapV2Impl.AmazonIapV2UnityAndroid.nativeInit();
      }

      protected override void RegisterCallback()
      {
        AmazonIapV2Impl.AmazonIapV2UnityAndroid.nativeRegisterCallbackGameObject(this.gameObject.name);
      }

      protected override void RegisterEventListener()
      {
        AmazonIapV2Impl.AmazonIapV2UnityAndroid.nativeRegisterCallbackGameObject(this.gameObject.name);
      }

      protected override void NativeRegisterCrossPlatformTool(string crossPlatformTool)
      {
      }

      protected override string NativeGetUserDataJson(string jsonMessage)
      {
        return AmazonIapV2Impl.AmazonIapV2UnityAndroid.nativeGetUserDataJson(jsonMessage);
      }

      protected override string NativePurchaseJson(string jsonMessage)
      {
        return AmazonIapV2Impl.AmazonIapV2UnityAndroid.nativePurchaseJson(jsonMessage);
      }

      protected override string NativeGetProductDataJson(string jsonMessage)
      {
        return AmazonIapV2Impl.AmazonIapV2UnityAndroid.nativeGetProductDataJson(jsonMessage);
      }

      protected override string NativeGetPurchaseUpdatesJson(string jsonMessage)
      {
        return AmazonIapV2Impl.AmazonIapV2UnityAndroid.nativeGetPurchaseUpdatesJson(jsonMessage);
      }

      protected override string NativeNotifyFulfillmentJson(string jsonMessage)
      {
        return AmazonIapV2Impl.AmazonIapV2UnityAndroid.nativeNotifyFulfillmentJson(jsonMessage);
      }
    }

    private abstract class AmazonIapV2UnityBase : AmazonIapV2Impl.AmazonIapV2Base
    {
      private static volatile bool quit = false;
      private static object initLock = new object();
      private const string CrossPlatformTool = "UNITY";
      private static AmazonIapV2Impl.AmazonIapV2UnityBase instance;
      private static System.Type instanceType;

      public static T getInstance<T>() where T : AmazonIapV2Impl.AmazonIapV2UnityBase
      {
        if (AmazonIapV2Impl.AmazonIapV2UnityBase.quit)
          return (T) null;
        if ((UnityEngine.Object) AmazonIapV2Impl.AmazonIapV2UnityBase.instance != (UnityEngine.Object) null)
          return (T) AmazonIapV2Impl.AmazonIapV2UnityBase.instance;
        lock (AmazonIapV2Impl.AmazonIapV2UnityBase.initLock)
        {
          System.Type type = typeof (T);
          AmazonIapV2Impl.AmazonIapV2UnityBase.assertTrue((UnityEngine.Object) AmazonIapV2Impl.AmazonIapV2UnityBase.instance == (UnityEngine.Object) null || (UnityEngine.Object) AmazonIapV2Impl.AmazonIapV2UnityBase.instance != (UnityEngine.Object) null && AmazonIapV2Impl.AmazonIapV2UnityBase.instanceType == type, "Only 1 instance of 1 subtype of AmazonIapV2UnityBase can exist.");
          if ((UnityEngine.Object) AmazonIapV2Impl.AmazonIapV2UnityBase.instance == (UnityEngine.Object) null)
          {
            AmazonIapV2Impl.AmazonIapV2UnityBase.instanceType = type;
            GameObject gameObject = new GameObject();
            AmazonIapV2Impl.AmazonIapV2UnityBase.instance = (AmazonIapV2Impl.AmazonIapV2UnityBase) gameObject.AddComponent<T>();
            gameObject.name = type.ToString() + "_Singleton";
            UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
          }
          return (T) AmazonIapV2Impl.AmazonIapV2UnityBase.instance;
        }
      }

      public void OnDestroy()
      {
        AmazonIapV2Impl.AmazonIapV2UnityBase.quit = true;
      }

      private static void assertTrue(bool statement, string errorMessage)
      {
        if (!statement)
          throw new AmazonException("FATAL: An internal error occurred", (Exception) new InvalidOperationException(errorMessage));
      }

      protected override void Init()
      {
        this.NativeInit();
      }

      protected override void RegisterCrossPlatformTool()
      {
        this.NativeRegisterCrossPlatformTool("UNITY");
      }

      protected abstract void NativeInit();

      protected abstract void NativeRegisterCrossPlatformTool(string crossPlatformTool);
    }

    protected delegate void CallbackDelegate(string jsonMessage);
  }
}
