﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.IAmazonIapV2
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace com.amazon.device.iap.cpt
{
  public interface IAmazonIapV2
  {
    RequestOutput GetUserData();

    RequestOutput Purchase(SkuInput skuInput);

    RequestOutput GetProductData(SkusInput skusInput);

    RequestOutput GetPurchaseUpdates(ResetInput resetInput);

    void NotifyFulfillment(NotifyFulfillmentInput notifyFulfillmentInput);

    void UnityFireEvent(string jsonMessage);

    void AddGetUserDataResponseListener(GetUserDataResponseDelegate responseDelegate);

    void RemoveGetUserDataResponseListener(GetUserDataResponseDelegate responseDelegate);

    void AddPurchaseResponseListener(PurchaseResponseDelegate responseDelegate);

    void RemovePurchaseResponseListener(PurchaseResponseDelegate responseDelegate);

    void AddGetProductDataResponseListener(GetProductDataResponseDelegate responseDelegate);

    void RemoveGetProductDataResponseListener(GetProductDataResponseDelegate responseDelegate);

    void AddGetPurchaseUpdatesResponseListener(GetPurchaseUpdatesResponseDelegate responseDelegate);

    void RemoveGetPurchaseUpdatesResponseListener(GetPurchaseUpdatesResponseDelegate responseDelegate);
  }
}
