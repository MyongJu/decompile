﻿// Decompiled with JetBrains decompiler
// Type: com.amazon.device.iap.cpt.GetProductDataResponseDelegator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace com.amazon.device.iap.cpt
{
  public sealed class GetProductDataResponseDelegator : IDelegator
  {
    public readonly GetProductDataResponseDelegate responseDelegate;

    public GetProductDataResponseDelegator(GetProductDataResponseDelegate responseDelegate)
    {
      this.responseDelegate = responseDelegate;
    }

    public void ExecuteSuccess()
    {
    }

    public void ExecuteSuccess(Dictionary<string, object> objectDictionary)
    {
      this.responseDelegate(GetProductDataResponse.CreateFromDictionary(objectDictionary));
    }

    public void ExecuteError(AmazonException e)
    {
    }
  }
}
