﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.AdjustAttribution
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace com.adjust.sdk
{
  public class AdjustAttribution
  {
    public AdjustAttribution()
    {
    }

    public AdjustAttribution(string jsonString)
    {
      JSONNode node = JSON.Parse(jsonString);
      if (node == (object) null)
        return;
      this.trackerName = AdjustUtils.GetJsonString(node, AdjustUtils.KeyTrackerName);
      this.trackerToken = AdjustUtils.GetJsonString(node, AdjustUtils.KeyTrackerToken);
      this.network = AdjustUtils.GetJsonString(node, AdjustUtils.KeyNetwork);
      this.campaign = AdjustUtils.GetJsonString(node, AdjustUtils.KeyCampaign);
      this.adgroup = AdjustUtils.GetJsonString(node, AdjustUtils.KeyAdgroup);
      this.creative = AdjustUtils.GetJsonString(node, AdjustUtils.KeyCreative);
      this.clickLabel = AdjustUtils.GetJsonString(node, AdjustUtils.KeyClickLabel);
      this.adid = AdjustUtils.GetJsonString(node, AdjustUtils.KeyAdid);
    }

    public AdjustAttribution(Dictionary<string, string> dicAttributionData)
    {
      if (dicAttributionData == null)
        return;
      this.trackerName = AdjustAttribution.TryGetValue(dicAttributionData, AdjustUtils.KeyTrackerName);
      this.trackerToken = AdjustAttribution.TryGetValue(dicAttributionData, AdjustUtils.KeyTrackerToken);
      this.network = AdjustAttribution.TryGetValue(dicAttributionData, AdjustUtils.KeyNetwork);
      this.campaign = AdjustAttribution.TryGetValue(dicAttributionData, AdjustUtils.KeyCampaign);
      this.adgroup = AdjustAttribution.TryGetValue(dicAttributionData, AdjustUtils.KeyAdgroup);
      this.creative = AdjustAttribution.TryGetValue(dicAttributionData, AdjustUtils.KeyCreative);
      this.clickLabel = AdjustAttribution.TryGetValue(dicAttributionData, AdjustUtils.KeyClickLabel);
      this.adid = AdjustAttribution.TryGetValue(dicAttributionData, AdjustUtils.KeyAdid);
    }

    public string adid { get; set; }

    public string network { get; set; }

    public string adgroup { get; set; }

    public string campaign { get; set; }

    public string creative { get; set; }

    public string clickLabel { get; set; }

    public string trackerName { get; set; }

    public string trackerToken { get; set; }

    private static string TryGetValue(Dictionary<string, string> dic, string key)
    {
      string str;
      if (dic.TryGetValue(key, out str))
        return str;
      return (string) null;
    }
  }
}
