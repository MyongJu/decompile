﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.JSONBinaryTag
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace com.adjust.sdk
{
  public enum JSONBinaryTag
  {
    Array = 1,
    Class = 2,
    Value = 3,
    IntValue = 4,
    DoubleValue = 5,
    BoolValue = 6,
    FloatValue = 7,
  }
}
