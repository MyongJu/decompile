﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.Adjust
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace com.adjust.sdk
{
  public class Adjust : MonoBehaviour
  {
    public bool startManually = true;
    public bool printAttribution = true;
    public bool launchDeferredDeeplink = true;
    public string appToken = "{Your App Token}";
    public AdjustLogLevel logLevel = AdjustLogLevel.Info;
    private const string errorMessage = "adjust: SDK not started. Start it manually using the 'start' method.";
    private static IAdjust instance;
    private static Action<string> deferredDeeplinkDelegate;
    private static Action<AdjustEventSuccess> eventSuccessDelegate;
    private static Action<AdjustEventFailure> eventFailureDelegate;
    private static Action<AdjustSessionSuccess> sessionSuccessDelegate;
    private static Action<AdjustSessionFailure> sessionFailureDelegate;
    private static Action<AdjustAttribution> attributionChangedDelegate;
    public bool eventBuffering;
    public bool sendInBackground;
    public AdjustEnvironment environment;

    private void Awake()
    {
      if (Adjust.instance != null)
        return;
      UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) this.transform.gameObject);
      if (this.startManually)
        return;
      AdjustConfig adjustConfig = new AdjustConfig(this.appToken, this.environment, this.logLevel == AdjustLogLevel.Suppress);
      adjustConfig.setLogLevel(this.logLevel);
      adjustConfig.setSendInBackground(this.sendInBackground);
      adjustConfig.setEventBufferingEnabled(this.eventBuffering);
      adjustConfig.setLaunchDeferredDeeplink(this.launchDeferredDeeplink);
      if (this.printAttribution)
      {
        adjustConfig.setEventSuccessDelegate(new Action<AdjustEventSuccess>(this.EventSuccessCallback), nameof (Adjust));
        adjustConfig.setEventFailureDelegate(new Action<AdjustEventFailure>(this.EventFailureCallback), nameof (Adjust));
        adjustConfig.setSessionSuccessDelegate(new Action<AdjustSessionSuccess>(this.SessionSuccessCallback), nameof (Adjust));
        adjustConfig.setSessionFailureDelegate(new Action<AdjustSessionFailure>(this.SessionFailureCallback), nameof (Adjust));
        adjustConfig.setDeferredDeeplinkDelegate(new Action<string>(this.DeferredDeeplinkCallback), nameof (Adjust));
        adjustConfig.setAttributionChangedDelegate(new Action<AdjustAttribution>(this.AttributionChangedCallback), nameof (Adjust));
      }
      Adjust.start(adjustConfig);
    }

    private void OnApplicationPause(bool pauseStatus)
    {
      if (Adjust.instance == null)
        return;
      if (pauseStatus)
        Adjust.instance.onPause();
      else
        Adjust.instance.onResume();
    }

    public static void start(AdjustConfig adjustConfig)
    {
      if (Adjust.instance != null)
        Debug.Log((object) "adjust: Error, SDK already started.");
      else if (adjustConfig == null)
      {
        Debug.Log((object) "adjust: Missing config to start.");
      }
      else
      {
        Adjust.instance = (IAdjust) new AdjustAndroid();
        if (Adjust.instance == null)
        {
          Debug.Log((object) "adjust: SDK can only be used in Android, iOS, Windows Phone 8 or Windows Store apps.");
        }
        else
        {
          Adjust.eventSuccessDelegate = adjustConfig.getEventSuccessDelegate();
          Adjust.eventFailureDelegate = adjustConfig.getEventFailureDelegate();
          Adjust.sessionSuccessDelegate = adjustConfig.getSessionSuccessDelegate();
          Adjust.sessionFailureDelegate = adjustConfig.getSessionFailureDelegate();
          Adjust.deferredDeeplinkDelegate = adjustConfig.getDeferredDeeplinkDelegate();
          Adjust.attributionChangedDelegate = adjustConfig.getAttributionChangedDelegate();
          Adjust.instance.start(adjustConfig);
        }
      }
    }

    public static void trackEvent(AdjustEvent adjustEvent)
    {
      if (Adjust.instance == null)
        Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      else if (adjustEvent == null)
        Debug.Log((object) "adjust: Missing event to track.");
      else
        Adjust.instance.trackEvent(adjustEvent);
    }

    public static void setEnabled(bool enabled)
    {
      if (Adjust.instance == null)
        Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      else
        Adjust.instance.setEnabled(enabled);
    }

    public static bool isEnabled()
    {
      if (Adjust.instance != null)
        return Adjust.instance.isEnabled();
      Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      return false;
    }

    public static void setOfflineMode(bool enabled)
    {
      if (Adjust.instance == null)
        Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      else
        Adjust.instance.setOfflineMode(enabled);
    }

    public static void sendFirstPackages()
    {
      if (Adjust.instance == null)
        Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      else
        Adjust.instance.sendFirstPackages();
    }

    public static void addSessionPartnerParameter(string key, string value)
    {
      AdjustAndroid.addSessionPartnerParameter(key, value);
    }

    public static void addSessionCallbackParameter(string key, string value)
    {
      AdjustAndroid.addSessionCallbackParameter(key, value);
    }

    public static void removeSessionPartnerParameter(string key)
    {
      AdjustAndroid.removeSessionPartnerParameter(key);
    }

    public static void removeSessionCallbackParameter(string key)
    {
      AdjustAndroid.removeSessionCallbackParameter(key);
    }

    public static void resetSessionPartnerParameters()
    {
      AdjustAndroid.resetSessionPartnerParameters();
    }

    public static void resetSessionCallbackParameters()
    {
      AdjustAndroid.resetSessionCallbackParameters();
    }

    public static string getAdid()
    {
      if (Adjust.instance != null)
        return Adjust.instance.getAdid();
      Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      return (string) null;
    }

    public static AdjustAttribution getAttribution()
    {
      if (Adjust.instance != null)
        return Adjust.instance.getAttribution();
      Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      return (AdjustAttribution) null;
    }

    public static void setDeviceToken(string deviceToken)
    {
      if (Adjust.instance == null)
        Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      else
        Adjust.instance.setDeviceToken(deviceToken);
    }

    public static string getIdfa()
    {
      if (Adjust.instance != null)
        return Adjust.instance.getIdfa();
      Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      return (string) null;
    }

    public static void setReferrer(string referrer)
    {
      if (Adjust.instance == null)
        Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      else
        Adjust.instance.setReferrer(referrer);
    }

    public static void getGoogleAdId(Action<string> onDeviceIdsRead)
    {
      if (Adjust.instance == null)
        Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      else
        Adjust.instance.getGoogleAdId(onDeviceIdsRead);
    }

    public static void runAttributionChangedDictionary(Dictionary<string, string> dicAttributionData)
    {
      if (Adjust.instance == null)
        Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      else if (Adjust.attributionChangedDelegate == null)
      {
        Debug.Log((object) "adjust: Attribution changed delegate was not set.");
      }
      else
      {
        AdjustAttribution adjustAttribution = new AdjustAttribution(dicAttributionData);
        Adjust.attributionChangedDelegate(adjustAttribution);
      }
    }

    public void GetNativeAttribution(string attributionData)
    {
      if (Adjust.instance == null)
        Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      else if (Adjust.attributionChangedDelegate == null)
      {
        Debug.Log((object) "adjust: Attribution changed delegate was not set.");
      }
      else
      {
        AdjustAttribution adjustAttribution = new AdjustAttribution(attributionData);
        Adjust.attributionChangedDelegate(adjustAttribution);
      }
    }

    public void GetNativeEventSuccess(string eventSuccessData)
    {
      if (Adjust.instance == null)
        Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      else if (Adjust.eventSuccessDelegate == null)
      {
        Debug.Log((object) "adjust: Event success delegate was not set.");
      }
      else
      {
        AdjustEventSuccess adjustEventSuccess = new AdjustEventSuccess(eventSuccessData);
        Adjust.eventSuccessDelegate(adjustEventSuccess);
      }
    }

    public void GetNativeEventFailure(string eventFailureData)
    {
      if (Adjust.instance == null)
        Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      else if (Adjust.eventFailureDelegate == null)
      {
        Debug.Log((object) "adjust: Event failure delegate was not set.");
      }
      else
      {
        AdjustEventFailure adjustEventFailure = new AdjustEventFailure(eventFailureData);
        Adjust.eventFailureDelegate(adjustEventFailure);
      }
    }

    public void GetNativeSessionSuccess(string sessionSuccessData)
    {
      if (Adjust.instance == null)
        Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      else if (Adjust.sessionSuccessDelegate == null)
      {
        Debug.Log((object) "adjust: Session success delegate was not set.");
      }
      else
      {
        AdjustSessionSuccess adjustSessionSuccess = new AdjustSessionSuccess(sessionSuccessData);
        Adjust.sessionSuccessDelegate(adjustSessionSuccess);
      }
    }

    public void GetNativeSessionFailure(string sessionFailureData)
    {
      if (Adjust.instance == null)
        Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      else if (Adjust.sessionFailureDelegate == null)
      {
        Debug.Log((object) "adjust: Session failure delegate was not set.");
      }
      else
      {
        AdjustSessionFailure adjustSessionFailure = new AdjustSessionFailure(sessionFailureData);
        Adjust.sessionFailureDelegate(adjustSessionFailure);
      }
    }

    public void GetNativeDeferredDeeplink(string deeplinkURL)
    {
      if (Adjust.instance == null)
        Debug.Log((object) "adjust: SDK not started. Start it manually using the 'start' method.");
      else if (Adjust.deferredDeeplinkDelegate == null)
        Debug.Log((object) "adjust: Deferred deeplink delegate was not set.");
      else
        Adjust.deferredDeeplinkDelegate(deeplinkURL);
    }

    private void AttributionChangedCallback(AdjustAttribution attributionData)
    {
      Debug.Log((object) "Attribution changed!");
      if (attributionData.trackerName != null)
        Debug.Log((object) ("Tracker name: " + attributionData.trackerName));
      if (attributionData.trackerToken != null)
        Debug.Log((object) ("Tracker token: " + attributionData.trackerToken));
      if (attributionData.network != null)
        Debug.Log((object) ("Network: " + attributionData.network));
      if (attributionData.campaign != null)
        Debug.Log((object) ("Campaign: " + attributionData.campaign));
      if (attributionData.adgroup != null)
        Debug.Log((object) ("Adgroup: " + attributionData.adgroup));
      if (attributionData.creative != null)
        Debug.Log((object) ("Creative: " + attributionData.creative));
      if (attributionData.clickLabel != null)
        Debug.Log((object) ("Click label: " + attributionData.clickLabel));
      if (attributionData.adid == null)
        return;
      Debug.Log((object) ("ADID: " + attributionData.adid));
    }

    private void EventSuccessCallback(AdjustEventSuccess eventSuccessData)
    {
      Debug.Log((object) "Event tracked successfully!");
      if (eventSuccessData.Message != null)
        Debug.Log((object) ("Message: " + eventSuccessData.Message));
      if (eventSuccessData.Timestamp != null)
        Debug.Log((object) ("Timestamp: " + eventSuccessData.Timestamp));
      if (eventSuccessData.Adid != null)
        Debug.Log((object) ("Adid: " + eventSuccessData.Adid));
      if (eventSuccessData.EventToken != null)
        Debug.Log((object) ("EventToken: " + eventSuccessData.EventToken));
      if (eventSuccessData.JsonResponse == null)
        return;
      Debug.Log((object) ("JsonResponse: " + eventSuccessData.GetJsonResponse()));
    }

    private void EventFailureCallback(AdjustEventFailure eventFailureData)
    {
      Debug.Log((object) "Event tracking failed!");
      if (eventFailureData.Message != null)
        Debug.Log((object) ("Message: " + eventFailureData.Message));
      if (eventFailureData.Timestamp != null)
        Debug.Log((object) ("Timestamp: " + eventFailureData.Timestamp));
      if (eventFailureData.Adid != null)
        Debug.Log((object) ("Adid: " + eventFailureData.Adid));
      if (eventFailureData.EventToken != null)
        Debug.Log((object) ("EventToken: " + eventFailureData.EventToken));
      Debug.Log((object) ("WillRetry: " + eventFailureData.WillRetry.ToString()));
      if (eventFailureData.JsonResponse == null)
        return;
      Debug.Log((object) ("JsonResponse: " + eventFailureData.GetJsonResponse()));
    }

    private void SessionSuccessCallback(AdjustSessionSuccess sessionSuccessData)
    {
      Debug.Log((object) "Session tracked successfully!");
      if (sessionSuccessData.Message != null)
        Debug.Log((object) ("Message: " + sessionSuccessData.Message));
      if (sessionSuccessData.Timestamp != null)
        Debug.Log((object) ("Timestamp: " + sessionSuccessData.Timestamp));
      if (sessionSuccessData.Adid != null)
        Debug.Log((object) ("Adid: " + sessionSuccessData.Adid));
      if (sessionSuccessData.JsonResponse == null)
        return;
      Debug.Log((object) ("JsonResponse: " + sessionSuccessData.GetJsonResponse()));
    }

    private void SessionFailureCallback(AdjustSessionFailure sessionFailureData)
    {
      Debug.Log((object) "Session tracking failed!");
      if (sessionFailureData.Message != null)
        Debug.Log((object) ("Message: " + sessionFailureData.Message));
      if (sessionFailureData.Timestamp != null)
        Debug.Log((object) ("Timestamp: " + sessionFailureData.Timestamp));
      if (sessionFailureData.Adid != null)
        Debug.Log((object) ("Adid: " + sessionFailureData.Adid));
      Debug.Log((object) ("WillRetry: " + sessionFailureData.WillRetry.ToString()));
      if (sessionFailureData.JsonResponse == null)
        return;
      Debug.Log((object) ("JsonResponse: " + sessionFailureData.GetJsonResponse()));
    }

    private void DeferredDeeplinkCallback(string deeplinkURL)
    {
      Debug.Log((object) "Deferred deeplink reported!");
      if (deeplinkURL != null)
        Debug.Log((object) ("Deeplink URL: " + deeplinkURL));
      else
        Debug.Log((object) "Deeplink URL is null!");
    }
  }
}
