﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.AdjustLogLevelExtension
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace com.adjust.sdk
{
  public static class AdjustLogLevelExtension
  {
    public static string lowercaseToString(this AdjustLogLevel AdjustLogLevel)
    {
      switch (AdjustLogLevel)
      {
        case AdjustLogLevel.Verbose:
          return "verbose";
        case AdjustLogLevel.Debug:
          return "debug";
        case AdjustLogLevel.Info:
          return "info";
        case AdjustLogLevel.Warn:
          return "warn";
        case AdjustLogLevel.Error:
          return "error";
        case AdjustLogLevel.Assert:
          return "assert";
        case AdjustLogLevel.Suppress:
          return "suppress";
        default:
          return "unknown";
      }
    }

    public static string uppercaseToString(this AdjustLogLevel AdjustLogLevel)
    {
      switch (AdjustLogLevel)
      {
        case AdjustLogLevel.Verbose:
          return "VERBOSE";
        case AdjustLogLevel.Debug:
          return "DEBUG";
        case AdjustLogLevel.Info:
          return "INFO";
        case AdjustLogLevel.Warn:
          return "WARN";
        case AdjustLogLevel.Error:
          return "ERROR";
        case AdjustLogLevel.Assert:
          return "ASSERT";
        case AdjustLogLevel.Suppress:
          return "SUPPRESS";
        default:
          return "unknown";
      }
    }
  }
}
