﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.AdjustSessionFailure
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace com.adjust.sdk
{
  public class AdjustSessionFailure
  {
    public AdjustSessionFailure()
    {
    }

    public AdjustSessionFailure(string jsonString)
    {
      JSONNode node = JSON.Parse(jsonString);
      if (node == (object) null)
        return;
      this.Adid = AdjustUtils.GetJsonString(node, AdjustUtils.KeyAdid);
      this.Message = AdjustUtils.GetJsonString(node, AdjustUtils.KeyMessage);
      this.Timestamp = AdjustUtils.GetJsonString(node, AdjustUtils.KeyTimestamp);
      this.WillRetry = Convert.ToBoolean(AdjustUtils.GetJsonString(node, AdjustUtils.KeyWillRetry));
      JSONNode jsonNode = node[AdjustUtils.KeyJsonResponse];
      if (jsonNode == (object) null || (JSONNode) jsonNode.AsObject == (object) null)
        return;
      this.JsonResponse = new Dictionary<string, object>();
      AdjustUtils.WriteJsonResponseDictionary(jsonNode.AsObject, this.JsonResponse);
    }

    public string Adid { get; set; }

    public string Message { get; set; }

    public string Timestamp { get; set; }

    public bool WillRetry { get; set; }

    public Dictionary<string, object> JsonResponse { get; set; }

    public void BuildJsonResponseFromString(string jsonResponseString)
    {
      JSONNode jsonNode = JSON.Parse(jsonResponseString);
      if (jsonNode == (object) null)
        return;
      this.JsonResponse = new Dictionary<string, object>();
      AdjustUtils.WriteJsonResponseDictionary(jsonNode.AsObject, this.JsonResponse);
    }

    public string GetJsonResponse()
    {
      return AdjustUtils.GetJsonResponseCompact(this.JsonResponse);
    }
  }
}
