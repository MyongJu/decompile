﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.AdjustAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace com.adjust.sdk
{
  public class AdjustAndroid : IAdjust
  {
    private static bool launchDeferredDeeplink = true;
    private const string sdkPrefix = "unity4.11.4";
    private static AndroidJavaClass ajcAdjust;
    private AndroidJavaObject ajoCurrentActivity;
    private AdjustAndroid.DeferredDeeplinkListener onDeferredDeeplinkListener;
    private AdjustAndroid.AttributionChangeListener onAttributionChangedListener;
    private AdjustAndroid.EventTrackingFailedListener onEventTrackingFailedListener;
    private AdjustAndroid.EventTrackingSucceededListener onEventTrackingSucceededListener;
    private AdjustAndroid.SessionTrackingFailedListener onSessionTrackingFailedListener;
    private AdjustAndroid.SessionTrackingSucceededListener onSessionTrackingSucceededListener;

    public AdjustAndroid()
    {
      if (AdjustAndroid.ajcAdjust == null)
        AdjustAndroid.ajcAdjust = new AndroidJavaClass("com.adjust.sdk.Adjust");
      this.ajoCurrentActivity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
    }

    public void start(AdjustConfig adjustConfig)
    {
      AndroidJavaObject androidJavaObject1 = adjustConfig.environment != AdjustEnvironment.Sandbox ? new AndroidJavaClass("com.adjust.sdk.AdjustConfig").GetStatic<AndroidJavaObject>("ENVIRONMENT_PRODUCTION") : new AndroidJavaClass("com.adjust.sdk.AdjustConfig").GetStatic<AndroidJavaObject>("ENVIRONMENT_SANDBOX");
      AndroidJavaObject androidJavaObject2;
      if (adjustConfig.allowSuppressLogLevel.HasValue)
        androidJavaObject2 = new AndroidJavaObject("com.adjust.sdk.AdjustConfig", new object[4]
        {
          (object) this.ajoCurrentActivity,
          (object) adjustConfig.appToken,
          (object) androidJavaObject1,
          (object) adjustConfig.allowSuppressLogLevel
        });
      else
        androidJavaObject2 = new AndroidJavaObject("com.adjust.sdk.AdjustConfig", new object[3]
        {
          (object) this.ajoCurrentActivity,
          (object) adjustConfig.appToken,
          (object) androidJavaObject1
        });
      AdjustAndroid.launchDeferredDeeplink = adjustConfig.launchDeferredDeeplink;
      if (adjustConfig.logLevel.HasValue)
      {
        AndroidJavaObject androidJavaObject3 = !adjustConfig.logLevel.Value.uppercaseToString().Equals("SUPPRESS") ? new AndroidJavaClass("com.adjust.sdk.LogLevel").GetStatic<AndroidJavaObject>(adjustConfig.logLevel.Value.uppercaseToString()) : new AndroidJavaClass("com.adjust.sdk.LogLevel").GetStatic<AndroidJavaObject>("SUPRESS");
        if (androidJavaObject3 != null)
          androidJavaObject2.Call("setLogLevel", new object[1]
          {
            (object) androidJavaObject3
          });
      }
      if (adjustConfig.delayStart.HasValue)
        androidJavaObject2.Call("setDelayStart", new object[1]
        {
          (object) adjustConfig.delayStart
        });
      if (adjustConfig.eventBufferingEnabled.HasValue)
      {
        AndroidJavaObject androidJavaObject3 = new AndroidJavaObject("java.lang.Boolean", new object[1]
        {
          (object) adjustConfig.eventBufferingEnabled.Value
        });
        androidJavaObject2.Call("setEventBufferingEnabled", new object[1]
        {
          (object) androidJavaObject3
        });
      }
      if (adjustConfig.sendInBackground.HasValue)
        androidJavaObject2.Call("setSendInBackground", new object[1]
        {
          (object) adjustConfig.sendInBackground.Value
        });
      if (adjustConfig.userAgent != null)
        androidJavaObject2.Call("setUserAgent", new object[1]
        {
          (object) adjustConfig.userAgent
        });
      if (!string.IsNullOrEmpty(adjustConfig.processName))
        androidJavaObject2.Call("setProcessName", new object[1]
        {
          (object) adjustConfig.processName
        });
      if (adjustConfig.defaultTracker != null)
        androidJavaObject2.Call("setDefaultTracker", new object[1]
        {
          (object) adjustConfig.defaultTracker
        });
      if (adjustConfig.attributionChangedDelegate != null)
      {
        this.onAttributionChangedListener = new AdjustAndroid.AttributionChangeListener(adjustConfig.attributionChangedDelegate);
        androidJavaObject2.Call("setOnAttributionChangedListener", new object[1]
        {
          (object) this.onAttributionChangedListener
        });
      }
      if (adjustConfig.eventSuccessDelegate != null)
      {
        this.onEventTrackingSucceededListener = new AdjustAndroid.EventTrackingSucceededListener(adjustConfig.eventSuccessDelegate);
        androidJavaObject2.Call("setOnEventTrackingSucceededListener", new object[1]
        {
          (object) this.onEventTrackingSucceededListener
        });
      }
      if (adjustConfig.eventFailureDelegate != null)
      {
        this.onEventTrackingFailedListener = new AdjustAndroid.EventTrackingFailedListener(adjustConfig.eventFailureDelegate);
        androidJavaObject2.Call("setOnEventTrackingFailedListener", new object[1]
        {
          (object) this.onEventTrackingFailedListener
        });
      }
      if (adjustConfig.sessionSuccessDelegate != null)
      {
        this.onSessionTrackingSucceededListener = new AdjustAndroid.SessionTrackingSucceededListener(adjustConfig.sessionSuccessDelegate);
        androidJavaObject2.Call("setOnSessionTrackingSucceededListener", new object[1]
        {
          (object) this.onSessionTrackingSucceededListener
        });
      }
      if (adjustConfig.sessionFailureDelegate != null)
      {
        this.onSessionTrackingFailedListener = new AdjustAndroid.SessionTrackingFailedListener(adjustConfig.sessionFailureDelegate);
        androidJavaObject2.Call("setOnSessionTrackingFailedListener", new object[1]
        {
          (object) this.onSessionTrackingFailedListener
        });
      }
      if (adjustConfig.deferredDeeplinkDelegate != null)
      {
        this.onDeferredDeeplinkListener = new AdjustAndroid.DeferredDeeplinkListener(adjustConfig.deferredDeeplinkDelegate);
        androidJavaObject2.Call("setOnDeeplinkResponseListener", new object[1]
        {
          (object) this.onDeferredDeeplinkListener
        });
      }
      androidJavaObject2.Call("setSdkPrefix", new object[1]
      {
        (object) "unity4.11.4"
      });
      AdjustAndroid.ajcAdjust.CallStatic("onCreate", new object[1]
      {
        (object) androidJavaObject2
      });
      AdjustAndroid.ajcAdjust.CallStatic("onResume");
    }

    public void trackEvent(AdjustEvent adjustEvent)
    {
      AndroidJavaObject androidJavaObject = new AndroidJavaObject("com.adjust.sdk.AdjustEvent", new object[1]
      {
        (object) adjustEvent.eventToken
      });
      if (adjustEvent.revenue.HasValue)
        androidJavaObject.Call("setRevenue", (object) adjustEvent.revenue.Value, (object) adjustEvent.currency);
      if (adjustEvent.callbackList != null)
      {
        int index = 0;
        while (index < adjustEvent.callbackList.Count)
        {
          string callback1 = adjustEvent.callbackList[index];
          string callback2 = adjustEvent.callbackList[index + 1];
          androidJavaObject.Call("addCallbackParameter", (object) callback1, (object) callback2);
          index += 2;
        }
      }
      if (adjustEvent.partnerList != null)
      {
        int index = 0;
        while (index < adjustEvent.partnerList.Count)
        {
          string partner1 = adjustEvent.partnerList[index];
          string partner2 = adjustEvent.partnerList[index + 1];
          androidJavaObject.Call("addPartnerParameter", (object) partner1, (object) partner2);
          index += 2;
        }
      }
      if (adjustEvent.transactionId != null)
        androidJavaObject.Call("setOrderId", new object[1]
        {
          (object) adjustEvent.transactionId
        });
      AdjustAndroid.ajcAdjust.CallStatic(nameof (trackEvent), new object[1]
      {
        (object) androidJavaObject
      });
    }

    public bool isEnabled()
    {
      return AdjustAndroid.ajcAdjust.CallStatic<bool>(nameof (isEnabled));
    }

    public void setEnabled(bool enabled)
    {
      AdjustAndroid.ajcAdjust.CallStatic(nameof (setEnabled), new object[1]
      {
        (object) enabled
      });
    }

    public void setOfflineMode(bool enabled)
    {
      AdjustAndroid.ajcAdjust.CallStatic(nameof (setOfflineMode), new object[1]
      {
        (object) enabled
      });
    }

    public void sendFirstPackages()
    {
      AdjustAndroid.ajcAdjust.CallStatic(nameof (sendFirstPackages));
    }

    public void setDeviceToken(string deviceToken)
    {
      AdjustAndroid.ajcAdjust.CallStatic("setPushToken", new object[1]
      {
        (object) deviceToken
      });
    }

    public string getAdid()
    {
      return AdjustAndroid.ajcAdjust.CallStatic<string>(nameof (getAdid));
    }

    public AdjustAttribution getAttribution()
    {
      try
      {
        AndroidJavaObject androidJavaObject = AdjustAndroid.ajcAdjust.CallStatic<AndroidJavaObject>(nameof (getAttribution));
        if (androidJavaObject == null)
          return (AdjustAttribution) null;
        return new AdjustAttribution()
        {
          trackerName = androidJavaObject.Get<string>(AdjustUtils.KeyTrackerName),
          trackerToken = androidJavaObject.Get<string>(AdjustUtils.KeyTrackerToken),
          network = androidJavaObject.Get<string>(AdjustUtils.KeyNetwork),
          campaign = androidJavaObject.Get<string>(AdjustUtils.KeyCampaign),
          adgroup = androidJavaObject.Get<string>(AdjustUtils.KeyAdgroup),
          creative = androidJavaObject.Get<string>(AdjustUtils.KeyCreative),
          clickLabel = androidJavaObject.Get<string>(AdjustUtils.KeyClickLabel),
          adid = androidJavaObject.Get<string>(AdjustUtils.KeyAdid)
        };
      }
      catch (Exception ex)
      {
      }
      return (AdjustAttribution) null;
    }

    public static void addSessionPartnerParameter(string key, string value)
    {
      if (AdjustAndroid.ajcAdjust == null)
        AdjustAndroid.ajcAdjust = new AndroidJavaClass("com.adjust.sdk.Adjust");
      AdjustAndroid.ajcAdjust.CallStatic(nameof (addSessionPartnerParameter), (object) key, (object) value);
    }

    public static void addSessionCallbackParameter(string key, string value)
    {
      if (AdjustAndroid.ajcAdjust == null)
        AdjustAndroid.ajcAdjust = new AndroidJavaClass("com.adjust.sdk.Adjust");
      AdjustAndroid.ajcAdjust.CallStatic(nameof (addSessionCallbackParameter), (object) key, (object) value);
    }

    public static void removeSessionPartnerParameter(string key)
    {
      if (AdjustAndroid.ajcAdjust == null)
        AdjustAndroid.ajcAdjust = new AndroidJavaClass("com.adjust.sdk.Adjust");
      AdjustAndroid.ajcAdjust.CallStatic(nameof (removeSessionPartnerParameter), new object[1]
      {
        (object) key
      });
    }

    public static void removeSessionCallbackParameter(string key)
    {
      if (AdjustAndroid.ajcAdjust == null)
        AdjustAndroid.ajcAdjust = new AndroidJavaClass("com.adjust.sdk.Adjust");
      AdjustAndroid.ajcAdjust.CallStatic(nameof (removeSessionCallbackParameter), new object[1]
      {
        (object) key
      });
    }

    public static void resetSessionPartnerParameters()
    {
      if (AdjustAndroid.ajcAdjust == null)
        AdjustAndroid.ajcAdjust = new AndroidJavaClass("com.adjust.sdk.Adjust");
      AdjustAndroid.ajcAdjust.CallStatic(nameof (resetSessionPartnerParameters));
    }

    public static void resetSessionCallbackParameters()
    {
      if (AdjustAndroid.ajcAdjust == null)
        AdjustAndroid.ajcAdjust = new AndroidJavaClass("com.adjust.sdk.Adjust");
      AdjustAndroid.ajcAdjust.CallStatic(nameof (resetSessionCallbackParameters));
    }

    public void onPause()
    {
      AdjustAndroid.ajcAdjust.CallStatic(nameof (onPause));
    }

    public void onResume()
    {
      AdjustAndroid.ajcAdjust.CallStatic(nameof (onResume));
    }

    public void setReferrer(string referrer)
    {
      AdjustAndroid.ajcAdjust.CallStatic(nameof (setReferrer), new object[1]
      {
        (object) referrer
      });
    }

    public void getGoogleAdId(Action<string> onDeviceIdsRead)
    {
      AdjustAndroid.DeviceIdsReadListener deviceIdsReadListener = new AdjustAndroid.DeviceIdsReadListener(onDeviceIdsRead);
      AdjustAndroid.ajcAdjust.CallStatic(nameof (getGoogleAdId), (object) this.ajoCurrentActivity, (object) deviceIdsReadListener);
    }

    public string getIdfa()
    {
      return (string) null;
    }

    private class AttributionChangeListener : AndroidJavaProxy
    {
      private Action<AdjustAttribution> callback;

      public AttributionChangeListener(Action<AdjustAttribution> pCallback)
        : base("com.adjust.sdk.OnAttributionChangedListener")
      {
        this.callback = pCallback;
      }

      public void onAttributionChanged(AndroidJavaObject attribution)
      {
        if (this.callback == null)
          return;
        this.callback(new AdjustAttribution()
        {
          trackerName = attribution.Get<string>(AdjustUtils.KeyTrackerName),
          trackerToken = attribution.Get<string>(AdjustUtils.KeyTrackerToken),
          network = attribution.Get<string>(AdjustUtils.KeyNetwork),
          campaign = attribution.Get<string>(AdjustUtils.KeyCampaign),
          adgroup = attribution.Get<string>(AdjustUtils.KeyAdgroup),
          creative = attribution.Get<string>(AdjustUtils.KeyCreative),
          clickLabel = attribution.Get<string>(AdjustUtils.KeyClickLabel),
          adid = attribution.Get<string>(AdjustUtils.KeyAdid)
        });
      }
    }

    private class DeferredDeeplinkListener : AndroidJavaProxy
    {
      private Action<string> callback;

      public DeferredDeeplinkListener(Action<string> pCallback)
        : base("com.adjust.sdk.OnDeeplinkResponseListener")
      {
        this.callback = pCallback;
      }

      public bool launchReceivedDeeplink(AndroidJavaObject deeplink)
      {
        if (this.callback == null)
          return AdjustAndroid.launchDeferredDeeplink;
        this.callback(deeplink.Call<string>("toString"));
        return AdjustAndroid.launchDeferredDeeplink;
      }
    }

    private class EventTrackingSucceededListener : AndroidJavaProxy
    {
      private Action<AdjustEventSuccess> callback;

      public EventTrackingSucceededListener(Action<AdjustEventSuccess> pCallback)
        : base("com.adjust.sdk.OnEventTrackingSucceededListener")
      {
        this.callback = pCallback;
      }

      public void onFinishedEventTrackingSucceeded(AndroidJavaObject eventSuccessData)
      {
        if (this.callback == null || eventSuccessData == null)
          return;
        AdjustEventSuccess adjustEventSuccess = new AdjustEventSuccess();
        adjustEventSuccess.Adid = eventSuccessData.Get<string>(AdjustUtils.KeyAdid);
        adjustEventSuccess.Message = eventSuccessData.Get<string>(AdjustUtils.KeyMessage);
        adjustEventSuccess.Timestamp = eventSuccessData.Get<string>(AdjustUtils.KeyTimestamp);
        adjustEventSuccess.EventToken = eventSuccessData.Get<string>(AdjustUtils.KeyEventToken);
        try
        {
          string jsonResponseString = eventSuccessData.Get<AndroidJavaObject>(AdjustUtils.KeyJsonResponse).Call<string>("toString");
          adjustEventSuccess.BuildJsonResponseFromString(jsonResponseString);
        }
        catch (Exception ex)
        {
        }
        this.callback(adjustEventSuccess);
      }
    }

    private class EventTrackingFailedListener : AndroidJavaProxy
    {
      private Action<AdjustEventFailure> callback;

      public EventTrackingFailedListener(Action<AdjustEventFailure> pCallback)
        : base("com.adjust.sdk.OnEventTrackingFailedListener")
      {
        this.callback = pCallback;
      }

      public void onFinishedEventTrackingFailed(AndroidJavaObject eventFailureData)
      {
        if (this.callback == null || eventFailureData == null)
          return;
        AdjustEventFailure adjustEventFailure = new AdjustEventFailure();
        adjustEventFailure.Adid = eventFailureData.Get<string>(AdjustUtils.KeyAdid);
        adjustEventFailure.Message = eventFailureData.Get<string>(AdjustUtils.KeyMessage);
        adjustEventFailure.WillRetry = eventFailureData.Get<bool>(AdjustUtils.KeyWillRetry);
        adjustEventFailure.Timestamp = eventFailureData.Get<string>(AdjustUtils.KeyTimestamp);
        adjustEventFailure.EventToken = eventFailureData.Get<string>(AdjustUtils.KeyEventToken);
        try
        {
          string jsonResponseString = eventFailureData.Get<AndroidJavaObject>(AdjustUtils.KeyJsonResponse).Call<string>("toString");
          adjustEventFailure.BuildJsonResponseFromString(jsonResponseString);
        }
        catch (Exception ex)
        {
        }
        this.callback(adjustEventFailure);
      }
    }

    private class SessionTrackingSucceededListener : AndroidJavaProxy
    {
      private Action<AdjustSessionSuccess> callback;

      public SessionTrackingSucceededListener(Action<AdjustSessionSuccess> pCallback)
        : base("com.adjust.sdk.OnSessionTrackingSucceededListener")
      {
        this.callback = pCallback;
      }

      public void onFinishedSessionTrackingSucceeded(AndroidJavaObject sessionSuccessData)
      {
        if (this.callback == null || sessionSuccessData == null)
          return;
        AdjustSessionSuccess adjustSessionSuccess = new AdjustSessionSuccess();
        adjustSessionSuccess.Adid = sessionSuccessData.Get<string>(AdjustUtils.KeyAdid);
        adjustSessionSuccess.Message = sessionSuccessData.Get<string>(AdjustUtils.KeyMessage);
        adjustSessionSuccess.Timestamp = sessionSuccessData.Get<string>(AdjustUtils.KeyTimestamp);
        try
        {
          string jsonResponseString = sessionSuccessData.Get<AndroidJavaObject>(AdjustUtils.KeyJsonResponse).Call<string>("toString");
          adjustSessionSuccess.BuildJsonResponseFromString(jsonResponseString);
        }
        catch (Exception ex)
        {
        }
        this.callback(adjustSessionSuccess);
      }
    }

    private class SessionTrackingFailedListener : AndroidJavaProxy
    {
      private Action<AdjustSessionFailure> callback;

      public SessionTrackingFailedListener(Action<AdjustSessionFailure> pCallback)
        : base("com.adjust.sdk.OnSessionTrackingFailedListener")
      {
        this.callback = pCallback;
      }

      public void onFinishedSessionTrackingFailed(AndroidJavaObject sessionFailureData)
      {
        if (this.callback == null || sessionFailureData == null)
          return;
        AdjustSessionFailure adjustSessionFailure = new AdjustSessionFailure();
        adjustSessionFailure.Adid = sessionFailureData.Get<string>(AdjustUtils.KeyAdid);
        adjustSessionFailure.Message = sessionFailureData.Get<string>(AdjustUtils.KeyMessage);
        adjustSessionFailure.WillRetry = sessionFailureData.Get<bool>(AdjustUtils.KeyWillRetry);
        adjustSessionFailure.Timestamp = sessionFailureData.Get<string>(AdjustUtils.KeyTimestamp);
        try
        {
          string jsonResponseString = sessionFailureData.Get<AndroidJavaObject>(AdjustUtils.KeyJsonResponse).Call<string>("toString");
          adjustSessionFailure.BuildJsonResponseFromString(jsonResponseString);
        }
        catch (Exception ex)
        {
        }
        this.callback(adjustSessionFailure);
      }
    }

    private class DeviceIdsReadListener : AndroidJavaProxy
    {
      private Action<string> onPlayAdIdReadCallback;

      public DeviceIdsReadListener(Action<string> pCallback)
        : base("com.adjust.sdk.OnDeviceIdsRead")
      {
        this.onPlayAdIdReadCallback = pCallback;
      }

      public void onGoogleAdIdRead(string playAdId)
      {
        if (this.onPlayAdIdReadCallback == null)
          return;
        this.onPlayAdIdReadCallback(playAdId);
      }

      public void onGoogleAdIdRead(AndroidJavaObject ajoAdId)
      {
        if (ajoAdId == null)
          this.onGoogleAdIdRead((string) null);
        else
          this.onGoogleAdIdRead(ajoAdId.Call<string>("toString"));
      }
    }
  }
}
