﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.AdjustEnvironmentExtension
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace com.adjust.sdk
{
  public static class AdjustEnvironmentExtension
  {
    public static string lowercaseToString(this AdjustEnvironment adjustEnvironment)
    {
      switch (adjustEnvironment)
      {
        case AdjustEnvironment.Sandbox:
          return "sandbox";
        case AdjustEnvironment.Production:
          return "production";
        default:
          return "unknown";
      }
    }
  }
}
