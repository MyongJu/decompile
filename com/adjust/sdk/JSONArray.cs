﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.JSONArray
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace com.adjust.sdk
{
  public class JSONArray : JSONNode, IEnumerable
  {
    private List<JSONNode> m_List = new List<JSONNode>();

    public override JSONNode this[int aIndex]
    {
      get
      {
        if (aIndex < 0 || aIndex >= this.m_List.Count)
          return (JSONNode) new JSONLazyCreator((JSONNode) this);
        return this.m_List[aIndex];
      }
      set
      {
        if (aIndex < 0 || aIndex >= this.m_List.Count)
          this.m_List.Add(value);
        else
          this.m_List[aIndex] = value;
      }
    }

    public override JSONNode this[string aKey]
    {
      get
      {
        return (JSONNode) new JSONLazyCreator((JSONNode) this);
      }
      set
      {
        this.m_List.Add(value);
      }
    }

    public override int Count
    {
      get
      {
        return this.m_List.Count;
      }
    }

    public override void Add(string aKey, JSONNode aItem)
    {
      this.m_List.Add(aItem);
    }

    public override JSONNode Remove(int aIndex)
    {
      if (aIndex < 0 || aIndex >= this.m_List.Count)
        return (JSONNode) null;
      JSONNode jsonNode = this.m_List[aIndex];
      this.m_List.RemoveAt(aIndex);
      return jsonNode;
    }

    public override JSONNode Remove(JSONNode aNode)
    {
      this.m_List.Remove(aNode);
      return aNode;
    }

    public override IEnumerable<JSONNode> Childs
    {
      get
      {
        JSONArray.\u003C\u003Ec__Iterator2 cIterator2 = new JSONArray.\u003C\u003Ec__Iterator2()
        {
          \u003C\u003Ef__this = this
        };
        cIterator2.\u0024PC = -2;
        return (IEnumerable<JSONNode>) cIterator2;
      }
    }

    [DebuggerHidden]
    public IEnumerator GetEnumerator()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new JSONArray.\u003CGetEnumerator\u003Ec__Iterator3()
      {
        \u003C\u003Ef__this = this
      };
    }

    public override string ToString()
    {
      string str = "[ ";
      using (List<JSONNode>.Enumerator enumerator = this.m_List.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          JSONNode current = enumerator.Current;
          if (str.Length > 2)
            str += ", ";
          str += current.ToString();
        }
      }
      return str + " ]";
    }

    public override string ToString(string aPrefix)
    {
      string str = "[ ";
      using (List<JSONNode>.Enumerator enumerator = this.m_List.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          JSONNode current = enumerator.Current;
          if (str.Length > 3)
            str += ", ";
          str = str + "\n" + aPrefix + "   ";
          str += current.ToString(aPrefix + "   ");
        }
      }
      return str + "\n" + aPrefix + "]";
    }

    public override void Serialize(BinaryWriter aWriter)
    {
      aWriter.Write((byte) 1);
      aWriter.Write(this.m_List.Count);
      for (int index = 0; index < this.m_List.Count; ++index)
        this.m_List[index].Serialize(aWriter);
    }
  }
}
