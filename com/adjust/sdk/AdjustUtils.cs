﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.AdjustUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace com.adjust.sdk
{
  public class AdjustUtils
  {
    public static string KeyAdid = "adid";
    public static string KeyMessage = "message";
    public static string KeyNetwork = "network";
    public static string KeyAdgroup = "adgroup";
    public static string KeyCampaign = "campaign";
    public static string KeyCreative = "creative";
    public static string KeyWillRetry = "willRetry";
    public static string KeyTimestamp = "timestamp";
    public static string KeyEventToken = "eventToken";
    public static string KeyClickLabel = "clickLabel";
    public static string KeyTrackerName = "trackerName";
    public static string KeyTrackerToken = "trackerToken";
    public static string KeyJsonResponse = "jsonResponse";

    public static int ConvertLogLevel(AdjustLogLevel? logLevel)
    {
      if (!logLevel.HasValue)
        return -1;
      return (int) logLevel.Value;
    }

    public static int ConvertBool(bool? value)
    {
      if (!value.HasValue)
        return -1;
      return value.Value ? 1 : 0;
    }

    public static double ConvertDouble(double? value)
    {
      if (!value.HasValue)
        return -1.0;
      return value.Value;
    }

    public static string ConvertListToJson(List<string> list)
    {
      if (list == null)
        return (string) null;
      JSONArray jsonArray = new JSONArray();
      using (List<string>.Enumerator enumerator = list.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          jsonArray.Add((JSONNode) new JSONData(current));
        }
      }
      return jsonArray.ToString();
    }

    public static string GetJsonResponseCompact(Dictionary<string, object> dictionary)
    {
      string empty = string.Empty;
      if (dictionary == null)
        return empty;
      int num = 0;
      string str1 = empty + "{";
      using (Dictionary<string, object>.Enumerator enumerator = dictionary.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, object> current = enumerator.Current;
          string str2 = current.Value as string;
          if (str2 != null)
          {
            if (++num > 1)
              str1 += ",";
            str1 = str1 + "\"" + current.Key + "\":\"" + str2 + "\"";
          }
          else
          {
            Dictionary<string, object> dictionary1 = current.Value as Dictionary<string, object>;
            if (++num > 1)
              str1 += ",";
            str1 = str1 + "\"" + current.Key + "\":";
            str1 += AdjustUtils.GetJsonResponseCompact(dictionary1);
          }
        }
      }
      return str1 + "}";
    }

    public static string GetJsonString(JSONNode node, string key)
    {
      if (node == (object) null)
        return (string) null;
      JSONData jsonData = node[key] as JSONData;
      if ((JSONNode) jsonData == (object) null)
        return (string) null;
      return jsonData.Value;
    }

    public static void WriteJsonResponseDictionary(JSONClass jsonObject, Dictionary<string, object> output)
    {
      foreach (KeyValuePair<string, JSONNode> keyValuePair in jsonObject)
      {
        JSONClass asObject = keyValuePair.Value.AsObject;
        string key = keyValuePair.Key;
        if ((JSONNode) asObject == (object) null)
        {
          string str = keyValuePair.Value.Value;
          output.Add(key, (object) str);
        }
        else
        {
          Dictionary<string, object> output1 = new Dictionary<string, object>();
          output.Add(key, (object) output1);
          AdjustUtils.WriteJsonResponseDictionary(asObject, output1);
        }
      }
    }
  }
}
