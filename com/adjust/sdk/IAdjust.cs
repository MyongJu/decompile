﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.IAdjust
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

namespace com.adjust.sdk
{
  public interface IAdjust
  {
    bool isEnabled();

    string getAdid();

    AdjustAttribution getAttribution();

    void onPause();

    void onResume();

    void sendFirstPackages();

    void setEnabled(bool enabled);

    void setOfflineMode(bool enabled);

    void setDeviceToken(string deviceToken);

    void start(AdjustConfig adjustConfig);

    void trackEvent(AdjustEvent adjustEvent);

    string getIdfa();

    void setReferrer(string referrer);

    void getGoogleAdId(Action<string> onDeviceIdsRead);
  }
}
