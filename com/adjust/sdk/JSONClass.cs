﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.JSONClass
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace com.adjust.sdk
{
  public class JSONClass : JSONNode, IEnumerable
  {
    private Dictionary<string, JSONNode> m_Dict = new Dictionary<string, JSONNode>();

    public override JSONNode this[string aKey]
    {
      get
      {
        if (this.m_Dict.ContainsKey(aKey))
          return this.m_Dict[aKey];
        return (JSONNode) new JSONLazyCreator((JSONNode) this, aKey);
      }
      set
      {
        if (this.m_Dict.ContainsKey(aKey))
          this.m_Dict[aKey] = value;
        else
          this.m_Dict.Add(aKey, value);
      }
    }

    public override JSONNode this[int aIndex]
    {
      get
      {
        if (aIndex < 0 || aIndex >= this.m_Dict.Count)
          return (JSONNode) null;
        return this.m_Dict.ElementAt<KeyValuePair<string, JSONNode>>(aIndex).Value;
      }
      set
      {
        if (aIndex < 0 || aIndex >= this.m_Dict.Count)
          return;
        this.m_Dict[this.m_Dict.ElementAt<KeyValuePair<string, JSONNode>>(aIndex).Key] = value;
      }
    }

    public override int Count
    {
      get
      {
        return this.m_Dict.Count;
      }
    }

    public override void Add(string aKey, JSONNode aItem)
    {
      if (!string.IsNullOrEmpty(aKey))
      {
        if (this.m_Dict.ContainsKey(aKey))
          this.m_Dict[aKey] = aItem;
        else
          this.m_Dict.Add(aKey, aItem);
      }
      else
        this.m_Dict.Add(Guid.NewGuid().ToString(), aItem);
    }

    public override JSONNode Remove(string aKey)
    {
      if (!this.m_Dict.ContainsKey(aKey))
        return (JSONNode) null;
      JSONNode jsonNode = this.m_Dict[aKey];
      this.m_Dict.Remove(aKey);
      return jsonNode;
    }

    public override JSONNode Remove(int aIndex)
    {
      if (aIndex < 0 || aIndex >= this.m_Dict.Count)
        return (JSONNode) null;
      KeyValuePair<string, JSONNode> keyValuePair = this.m_Dict.ElementAt<KeyValuePair<string, JSONNode>>(aIndex);
      this.m_Dict.Remove(keyValuePair.Key);
      return keyValuePair.Value;
    }

    public override JSONNode Remove(JSONNode aNode)
    {
      try
      {
        this.m_Dict.Remove(this.m_Dict.Where<KeyValuePair<string, JSONNode>>((Func<KeyValuePair<string, JSONNode>, bool>) (k => k.Value == (object) aNode)).First<KeyValuePair<string, JSONNode>>().Key);
        return aNode;
      }
      catch
      {
        return (JSONNode) null;
      }
    }

    public override IEnumerable<JSONNode> Childs
    {
      get
      {
        JSONClass.\u003C\u003Ec__Iterator4 cIterator4 = new JSONClass.\u003C\u003Ec__Iterator4()
        {
          \u003C\u003Ef__this = this
        };
        cIterator4.\u0024PC = -2;
        return (IEnumerable<JSONNode>) cIterator4;
      }
    }

    [DebuggerHidden]
    public IEnumerator GetEnumerator()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new JSONClass.\u003CGetEnumerator\u003Ec__Iterator5()
      {
        \u003C\u003Ef__this = this
      };
    }

    public override string ToString()
    {
      string str = "{";
      using (Dictionary<string, JSONNode>.Enumerator enumerator = this.m_Dict.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, JSONNode> current = enumerator.Current;
          if (str.Length > 2)
            str += ", ";
          str = str + "\"" + JSONNode.Escape(current.Key) + "\":" + current.Value.ToString();
        }
      }
      return str + "}";
    }

    public override string ToString(string aPrefix)
    {
      string str = "{ ";
      using (Dictionary<string, JSONNode>.Enumerator enumerator = this.m_Dict.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, JSONNode> current = enumerator.Current;
          if (str.Length > 3)
            str += ", ";
          str = str + "\n" + aPrefix + "   ";
          str = str + "\"" + JSONNode.Escape(current.Key) + "\" : " + current.Value.ToString(aPrefix + "   ");
        }
      }
      return str + "\n" + aPrefix + "}";
    }

    public override void Serialize(BinaryWriter aWriter)
    {
      aWriter.Write((byte) 2);
      aWriter.Write(this.m_Dict.Count);
      using (Dictionary<string, JSONNode>.KeyCollection.Enumerator enumerator = this.m_Dict.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          aWriter.Write(current);
          this.m_Dict[current].Serialize(aWriter);
        }
      }
    }
  }
}
