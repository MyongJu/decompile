﻿// Decompiled with JetBrains decompiler
// Type: Com.Google.Android.Gms.Common.Api.PendingResult`1
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Google.Developers;
using System;

namespace Com.Google.Android.Gms.Common.Api
{
  public class PendingResult<R> : JavaObjWrapper where R : Result
  {
    private const string CLASS_NAME = "com/google/android/gms/common/api/PendingResult";

    public PendingResult(IntPtr ptr)
      : base(ptr)
    {
    }

    public PendingResult()
      : base("com.google.android.gms.common.api.PendingResult")
    {
    }

    public R await(long arg_long_1, object arg_object_2)
    {
      return this.InvokeCall<R>(nameof (await), "(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/Result;", (object) arg_long_1, arg_object_2);
    }

    public R await()
    {
      return this.InvokeCall<R>(nameof (await), "()Lcom/google/android/gms/common/api/Result;");
    }

    public bool isCanceled()
    {
      return this.InvokeCall<bool>(nameof (isCanceled), "()Z");
    }

    public void cancel()
    {
      this.InvokeCallVoid(nameof (cancel), "()V");
    }

    public void setResultCallback(ResultCallback<R> arg_ResultCallback_1)
    {
      this.InvokeCallVoid(nameof (setResultCallback), "(Lcom/google/android/gms/common/api/ResultCallback;)V", (object) arg_ResultCallback_1);
    }

    public void setResultCallback(ResultCallback<R> arg_ResultCallback_1, long arg_long_2, object arg_object_3)
    {
      this.InvokeCallVoid(nameof (setResultCallback), "(Lcom/google/android/gms/common/api/ResultCallback;JLjava/util/concurrent/TimeUnit;)V", (object) arg_ResultCallback_1, (object) arg_long_2, arg_object_3);
    }
  }
}
