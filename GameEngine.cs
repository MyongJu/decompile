﻿// Decompiled with JetBrains decompiler
// Type: GameEngine
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Chat;
using DB;
using I2.Loc;
using March;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using UI;
using UnityEngine;
using UnityEngine.Experimental.Director;

public class GameEngine : MonoBehaviour
{
  [DevSetting("Chat/RTM disable", null, null)]
  public static bool devChatDisable = true;
  private Events _events = new Events();
  [Tooltip("The scale we want for both pve and pvp tile map")]
  public float tileMapScale = 0.01f;
  [Tooltip("The up vector angle we want for both pve and pvp tile map")]
  public Vector3 tileMapUpAngle = new Vector3(-30f, 0.0f, 0.0f);
  [Tooltip("The character scale we want for both pve and pvp tile map")]
  public Vector3 tileCharacterScale = new Vector3(80f, 80f, 80f);
  public int logLevel = 4;
  protected float m_restartTime = 120f;
  private ConcurrentQueue<GameEngine.LogMessage> logqueue = new ConcurrentQueue<GameEngine.LogMessage>();
  public const string CityScene = "CityView";
  public const string CampaignScene = "CampaignMap";
  public const string KingdomScene = "KingdomMap";
  public const string WorldScene = "WorldMap";
  public const string ForceStartSceneField = "ForceStartScene";
  public const string RaceMode = "RaceMode";
  public const string bgPath = "Prefab/City/CityBG";
  private const string movPath = "openingcinematic.mp4";
  private const string ockey = "openingcinematic";
  [DevSetting("Delete player on start", null, null)]
  public static bool devDeletePlayerOnStart;
  public static int GameLoopCount;
  private static GameEngine _instance;
  private static bool _shuttingDown;
  private static bool _initialized;
  public GameObject[] needDeactive;
  private UIManager _uiManager;
  private PlayerData _playerData;
  private MarchSystem _marchSystem;
  private ChatManager _chatManager;
  private GameEngine.GameMode _gameMode;
  public TextAsset iconLookupTable;
  public TextAsset nameLookupTable;
  public bool resourceLoadOK;
  [HideInInspector]
  public bool canStartGame;
  public string logFilter;
  protected DateTime m_timeEnterPause;
  private float _resumeTime;
  private int _defaultLayer;
  private int _transparentFXLayer;
  private int _futileLayer;
  private int _uiLayer;
  private int _popupLayer;
  private int _ui3DLayer;
  private int _actorLayer;
  private int _pvpUILayer;
  private int _tutorialLayer;
  private int _buildingMoveLayer;
  private int _tileLayer;
  private Vector3 _tileMapUp;
  private bool restartGameFlag;
  private bool lockBack;

  public event System.Action<bool> OnGameModeStarted;

  public event System.Action OnGameModeReady;

  [DllImport("__Internal")]
  private static extern void CCLog(string msg);

  public float ResumeTime
  {
    get
    {
      return this._resumeTime;
    }
  }

  private void GameModeStarted(bool clear = true)
  {
    if (this.OnGameModeStarted == null)
      return;
    this.OnGameModeStarted(clear);
  }

  public void GameModeReady()
  {
    if (this.OnGameModeReady == null)
      return;
    this.OnGameModeReady();
  }

  public Events events
  {
    get
    {
      return this._events;
    }
  }

  public bool ChatEnabled
  {
    get
    {
      return true;
    }
  }

  public PlayerData PlayerData
  {
    get
    {
      return this._playerData;
    }
  }

  public UIManager UIManager
  {
    get
    {
      return this._uiManager;
    }
  }

  public ChatManager ChatManager
  {
    get
    {
      return this._chatManager;
    }
  }

  public MarchSystem marchSystem
  {
    get
    {
      return this._marchSystem;
    }
  }

  public static GameEngine Instance
  {
    get
    {
      if (!((UnityEngine.Object) GameEngine._instance == (UnityEngine.Object) null))
        ;
      return GameEngine._instance;
    }
  }

  public static bool IsAvailable
  {
    get
    {
      return (UnityEngine.Object) GameEngine._instance != (UnityEngine.Object) null;
    }
  }

  public static bool IsShuttingDown
  {
    get
    {
      return GameEngine._shuttingDown;
    }
  }

  public int DefaultLayer
  {
    get
    {
      return this._defaultLayer;
    }
  }

  public int TransparentFXLayer
  {
    get
    {
      return this._transparentFXLayer;
    }
  }

  public int FutileLayer
  {
    get
    {
      return this._futileLayer;
    }
  }

  public int UILayer
  {
    get
    {
      return this._uiLayer;
    }
  }

  public int PopupLayer
  {
    get
    {
      return this._popupLayer;
    }
  }

  public int UI3DLayer
  {
    get
    {
      return this._ui3DLayer;
    }
  }

  public int ActorLayer
  {
    get
    {
      return this._actorLayer;
    }
  }

  public int PVPUILayer
  {
    get
    {
      return this._pvpUILayer;
    }
  }

  public int TutorialLayer
  {
    get
    {
      return this._tutorialLayer;
    }
  }

  public int BuildingMoveLayer
  {
    get
    {
      return this._buildingMoveLayer;
    }
  }

  public int TileLayer
  {
    get
    {
      return this._tileLayer;
    }
  }

  public Vector3 TileMapUp
  {
    get
    {
      return this._tileMapUp;
    }
  }

  public GameEngine.GameMode CurrentGameMode
  {
    get
    {
      return this._gameMode;
    }
    set
    {
      if (this._gameMode == value || this._gameMode == GameEngine.GameMode.LoadingMode)
        return;
      switch (this._gameMode)
      {
        case GameEngine.GameMode.InitMode:
          UIManager.inst.cityCamera.gameObject.SetActive(false);
          UIManager.inst.tileCamera.gameObject.SetActive(false);
          break;
        case GameEngine.GameMode.CityMode:
          CitadelSystem.inst.Hide();
          PrefabManagerEx.Instance.ClearCache();
          PrefabManagerEx.Instance.CancelAllSpawnRequests();
          AssetManager.Instance.PrintContent();
          break;
        case GameEngine.GameMode.DragonKnight:
          DragonKnightSystem.Instance.Hide();
          DragonKnightSystem.Instance.Shutdown();
          PrefabManagerEx.Instance.ClearCache();
          PrefabManagerEx.Instance.CancelAllSpawnRequests();
          AssetManager.Instance.PrintContent();
          break;
        case GameEngine.GameMode.PVPMode:
          PVPMapData.MapData.Dispose(true);
          PVPSystem.Instance.Hide();
          DBManager.inst.DB_March.ClearThirdPartMarch();
          this._marchSystem.ExitScene();
          PrefabManagerEx.Instance.ClearCache();
          PrefabManagerEx.Instance.CancelAllSpawnRequests();
          AssetManager.Instance.PrintContent();
          break;
        case GameEngine.GameMode.PitMode:
          PitMapData.MapData.Dispose(true);
          PVPSystem.Instance.Hide();
          DBManager.inst.DB_March.ClearThirdPartMarch();
          this._marchSystem.ExitScene();
          PrefabManagerEx.Instance.ClearCache();
          PrefabManagerEx.Instance.CancelAllSpawnRequests();
          AssetManager.Instance.PrintContent();
          break;
        case GameEngine.GameMode.MerlinTowerMode:
          MerlinTowerFacade.OnExit();
          PrefabManagerEx.Instance.ClearCache();
          PrefabManagerEx.Instance.CancelAllSpawnRequests();
          AssetManager.Instance.PrintContent();
          break;
      }
      this._gameMode = GameEngine.GameMode.LoadingMode;
      this.StopAllCoroutines();
      this.GameModeStarted(true);
      this.LoadScene(value);
    }
  }

  public static GameEngine.LoadMode ReloadMode { get; set; }

  private void Awake()
  {
    D.Level = this.logLevel;
    D.Filter = this.logFilter;
    if ((UnityEngine.Object) GameEngine._instance != (UnityEngine.Object) null && (UnityEngine.Object) GameEngine._instance != (UnityEngine.Object) this)
    {
      D.error((object) "There is should be only one GameEngine object");
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    }
    else
    {
      GameEngine._instance = this;
      if (GameEngine.GameLoopCount == 0)
        GameEngine.ReloadMode = GameEngine.LoadMode.Deep;
      ++GameEngine.GameLoopCount;
      if ((UnityEngine.Object) BlackCamera.Instance != (UnityEngine.Object) null)
        BlackCamera.Instance.SetActive(false);
      Screen.sleepTimeout = -1;
      PlatformAdaptor.NotifyApplicationStart();
      FingerGestures.GlobalTouchFilter = new FingerGestures.GlobalTouchFilterDelegate(this.NGUITouchFilter);
      this._tileMapUp = Quaternion.Euler(this.tileMapUpAngle) * Vector3.up;
      SqliteDatabase.Setup();
      SettingManager.Instance.Init();
      ConfigManager.inst.Init();
      AssetManager.Instance.Init();
      PerformanceConfiguration.Instance.InitializeDefault();
      for (int index = 0; index < this.needDeactive.Length; ++index)
      {
        if ((UnityEngine.Object) this.needDeactive[index] != (UnityEngine.Object) null)
          this.needDeactive[index].SetActive(false);
      }
      this._defaultLayer = LayerMask.NameToLayer("Default");
      this._transparentFXLayer = LayerMask.NameToLayer("TransparentFX");
      this._futileLayer = LayerMask.NameToLayer("Futile1");
      this._uiLayer = LayerMask.NameToLayer("NGUI");
      this._popupLayer = LayerMask.NameToLayer("Overlay");
      this._ui3DLayer = LayerMask.NameToLayer("UI3D");
      this._actorLayer = LayerMask.NameToLayer("Actor");
      this._tileLayer = LayerMask.NameToLayer("Tile");
      this._pvpUILayer = LayerMask.NameToLayer("PVPUI");
      this._tutorialLayer = LayerMask.NameToLayer("Tutorial");
      this._buildingMoveLayer = LayerMask.NameToLayer("BuildingMove");
      this._uiManager = UIManager.inst;
      this.m_timeEnterPause = DateTime.Now;
      UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) this.gameObject);
    }
  }

  [DebuggerHidden]
  private IEnumerator OnLogoHidded()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GameEngine.\u003COnLogoHidded\u003Ec__Iterator60()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void Start()
  {
    this.OnLogoScreenHidden();
    this.RegisterConstructor();
  }

  private void OnEnable()
  {
    Application.logMessageReceivedThreaded += new Application.LogCallback(this.LogHandle);
    AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(this.OnHandleUnresolvedException);
  }

  private void OnDisable()
  {
    AppDomain.CurrentDomain.UnhandledException -= new UnhandledExceptionEventHandler(this.OnHandleUnresolvedException);
    Application.logMessageReceivedThreaded -= new Application.LogCallback(this.LogHandle);
  }

  private void OnLogoScreenHidden()
  {
    NetWorkDetector.Instance.Init();
    TaskManager.Instance.Initialize();
    if (this.ChatEnabled)
      this._chatManager = new ChatManager();
    this._marchSystem = new MarchSystem();
    this._playerData = new PlayerData();
    this._playerData.Init();
    this.gameObject.GetComponent<RequestManager>();
    LoaderManager.inst.Init();
    Preloader.inst.Init();
    MessageHub.inst.Init();
    VfxManager.Instance.Startup();
    EquipmentManager.Instance.Init();
    GemManager.Instance.Init();
    for (int index = 0; index < this.needDeactive.Length; ++index)
    {
      if ((UnityEngine.Object) this.needDeactive[index] != (UnityEngine.Object) null)
        this.needDeactive[index].SetActive(true);
    }
  }

  private void AdjustResolution()
  {
    if (PerformanceConfiguration.Instance.Rating >= 2000 || Screen.height <= 720)
      return;
    float num1 = 720f / (float) Screen.height;
    float num2 = 1280f / (float) Screen.width;
    if ((double) num1 < (double) num2)
    {
      int height = 720;
      Screen.SetResolution((int) ((double) Screen.width * (double) num1), height, true);
    }
    else
      Screen.SetResolution(1280, (int) ((double) Screen.height * (double) num2), true);
    UIManager.inst.fullScreenGrey.SetActive(false);
  }

  public void StartGame()
  {
    D.warn((object) "<color=yellow>START GAME</color>");
    PlatformAdaptor.NotifyGameStart();
    this.InitAllMgrs();
    CityData playerCityData = PlayerData.inst.playerCityData;
    if (playerCityData.triggerClearAway)
    {
      switch (playerCityData.clearAway)
      {
        case 4:
          GameEngine._initialized = true;
          TeleportManager.Instance.OnKingdomTeleport(TeleportMode.WORLD_TELEPORT, PlayerData.inst.playerCityData.cityLocation, false);
          break;
        case 5:
          GameEngine._initialized = true;
          TeleportManager.Instance.EnterPit(TeleportMode.ENTER_PIT_TELEPORT, false);
          break;
        case 6:
          GameEngine._initialized = true;
          TeleportManager.Instance.LeavePit(TeleportMode.LEAVE_PIT_TELEPORT, false);
          break;
        case 7:
          GameEngine._initialized = true;
          TeleportManager.Instance.RequestTeleport_FirstEnterPit(TeleportMode.ENTER_PIT_TELEPORT, false);
          break;
        case 9:
          GameEngine._initialized = true;
          TeleportManager.Instance.EnterAC(TeleportMode.ALLIANCE_WAR_TELEPORT, false);
          break;
        case 10:
          GameEngine._initialized = true;
          TeleportManager.Instance.LeaveAC(TeleportMode.ALLIANCE_WAR_BACK_TELEPORT, false);
          break;
        default:
          this.CurrentGameMode = GameEngine.GameMode.CityMode;
          break;
      }
    }
    else
      this.CurrentGameMode = GameEngine.GameMode.CityMode;
  }

  private void InitAllMgrs()
  {
    UIManager.inst.Init();
    PopupSearchCoordinates.Init();
    AllianceManager.Instance.Initialize();
    TeleportManager.Instance.Initialize();
    NotificationManager.Instance.Initialize();
    ResearchManager.inst.Init();
    ItemBag.Instance.Initialize();
    WatchtowerUtilities.Init();
    AllianceTreasuryCountManager.Instance.Init();
    this.GameModeStarted(false);
  }

  private void InitAll()
  {
    BuildQueueManager.Instance.Init();
    this.LoadCity();
    GameEngine._initialized = true;
    GameEngine.ReloadMode = GameEngine.LoadMode.Lite;
    if (!MerlinTowerPayload.Instance.Reset)
      return;
    MerlinTowerPayload.Instance.Reset = false;
    UIManager.inst.OpenDlg("MerlinTower/MerlinTowerDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public string GetBuildTarget()
  {
    return "Android";
  }

  protected void StopAllAnimationsInTheScene()
  {
    foreach (UnityEngine.Animation animation in UnityEngine.Object.FindObjectsOfType<UnityEngine.Animation>())
      animation.Stop();
    foreach (DirectorPlayer directorPlayer in UnityEngine.Object.FindObjectsOfType<Animator>())
      directorPlayer.Stop();
  }

  public void RestartGame()
  {
    LoginErrDialog.LoginErrLog(nameof (RestartGame));
    GameEngine._initialized = false;
    Time.timeScale = 1f;
    if (!Application.isEditor)
      NetWorkDetector.Instance.SaveRUMData("GameRestart", "EmptyURL", "200", 0, 0, 0);
    GooglePlayGameManager.Instance.IgnoreLoginResult();
    this.StopAllAnimationsInTheScene();
    this.canStartGame = false;
    this.DisposeCoroutines();
    ActivityManager.Intance.Dispose();
    TaskManager.Instance.Dispose();
    AccountManager.Instance.Dispose();
    if (this._chatManager != null)
      this._chatManager.Dispose();
    Preloader.inst.Dispose();
    NetApi.inst.Dispose();
    PopupManager.Instance.Dispose();
    PushManager.inst.Dispose();
    RequestManager.inst.Dispose();
    JobManager.Instance.Dispose();
    Oscillator.Instance.Dispose();
    LoaderManager.inst.Dispose();
    MessageHub.inst.Dispose();
    AudioManager.Instance.Dispose();
    SkillEffectManager.Instance.StopAll(false);
    VfxManager.Instance.DeleteAll();
    AllianceManager.Instance.Dispose();
    TeleportManager.Instance.Dispose();
    UIManager.inst.Dispose();
    ResearchManager.inst.Dispose();
    PrefabManagerEx.Instance.ClearCache();
    PrefabManagerEx.Instance.CancelAllSpawnRequests();
    CollectionUIManager.Instance.Dispose();
    EquipmentManager.Instance.Dispose();
    GemManager.Instance.Dispose();
    CitadelSystem.inst.Hide();
    CitadelSystem.inst.Dispose(true);
    this._marchSystem.Dispose();
    IAPStorePackagePayload.Instance.Dispose();
    IapRebatePayload.Instance.Dispose();
    IAPDailyRechargePackagePayload.Instance.Dispose();
    RebateByRechargePayload.Instance.Dispose();
    JackpotPayload.Instance.Dispose();
    SuperLoginPayload.Instance.Dispose();
    KingdomBuffPayload.Instance.Dispose();
    LotteryDiyPayload.Instance.Dispose();
    MarksmanPayload.Instance.Dispose();
    WishWellPayload.Dispose();
    DicePayload.Instance.Dispose();
    TradingHallPayload.Instance.Dispose();
    AnniversaryIapPayload.Instance.Dispose();
    WishWellPayload.Dispose();
    KingdomBossPayload.Instance.Dispose();
    MerlinTrialsPayload.Instance.Dispose();
    RoulettePayload.Instance.Dispose();
    AllianceWarPayload.Instance.Dispose();
    MerlinTowerPayload.Instance.Dispose();
    ItemBag.Instance.Dispose();
    DBManager.inst.Dispose();
    PVPMapData.MapData.ClearListeners();
    PitMapData.MapData.ClearListeners();
    PVPMapData.MapData.Dispose(false);
    PitMapData.MapData.Dispose(false);
    AnniversaryManager.Instance.Dispose();
    ABTestManager.Instance.Dispose();
    AuctionHelper.Instance.Dispose();
    ChatAndMailConstraint.Instance.Dispose();
    this.StopAllCoroutines();
    for (int index = 0; index < this.needDeactive.Length; ++index)
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.needDeactive[index]);
    UnityEngine.Object.Destroy((UnityEngine.Object) GameObject.Find("UIManager"));
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    GameEngine._instance = (GameEngine) null;
    GameEngine._shuttingDown = false;
    LocalizationManager.GlobalSources = new string[1]
    {
      "I2Loading"
    };
    LocalizationManager.Sources.Clear();
    NetWorkDetector.Instance.Dispose();
    PlatformAdaptor.NotifyGameRestart();
    NotificationManager.Instance.Stop();
    switch (GameEngine.ReloadMode)
    {
      case GameEngine.LoadMode.Lite:
        if (InitUserDataState.Visited)
          break;
        goto case GameEngine.LoadMode.Deep;
      case GameEngine.LoadMode.Deep:
        Encryptor.Close();
        ConfigManager.inst.ClearData();
        IllegalWordsUtils.Dispose();
        VersionManager.Instance.Dispose();
        AssetManager.Instance.ClearTempData();
        CacheManager.Instance.Dispose();
        break;
    }
    if ((UnityEngine.Object) BlackCamera.Instance != (UnityEngine.Object) null)
      BlackCamera.Instance.SetActive(true);
    this.lockBack = false;
    Application.LoadLevel("BootScene");
    LoginErrDialog.LoginErrLog("Load BootScene");
  }

  private void DisposeCoroutines()
  {
    foreach (MonoBehaviour monoBehaviour in UnityEngine.Object.FindObjectsOfType<MonoBehaviour>())
      monoBehaviour.StopAllCoroutines();
  }

  public static bool IsReady()
  {
    return GameEngine._initialized;
  }

  private void LoadCity()
  {
    TeleportManager.Instance.CheckCityState();
    UIManager.inst.timerHUD.Display = false;
    this.StartCoroutine(CitadelSystem.inst.Show((System.Action) (() =>
    {
      CitadelSystem.inst.Init();
      UIManager.inst.Cloud.PokeCloud((System.Action) null);
    })));
  }

  private void LoadScene(GameEngine.GameMode inNewMode)
  {
    this.PreLoadScene();
    this._gameMode = inNewMode;
    switch (this._gameMode)
    {
      case GameEngine.GameMode.CityMode:
        MapUtils.RegisterRouteMapping();
        if (!CitadelSystem.inst.IsPreloadFinished)
        {
          this.StartCoroutine(CitadelSystem.inst.PreloadCity((System.Action) (() =>
          {
            CitadelSystem.inst.InitCamera();
            this.DelayToInvoke(new System.Action(this.InitAll), 0.2f);
          })));
          break;
        }
        this.LoadCity();
        break;
      case GameEngine.GameMode.DragonKnight:
        CitadelSystem.inst.Dispose(true);
        PVPSystem.Instance.Shutdown();
        DragonKnightSystem.Instance.Setup();
        DragonKnightSystem.Instance.Show();
        break;
      case GameEngine.GameMode.PVPMode:
        UIManager.inst.timerHUD.Display = true;
        DBManager.inst.DB_March.ClearThirdPartMarch();
        MapUtils.RegisterRouteMapping();
        PVPMapData.Initialize();
        PitMapData.Initialize();
        PVPSystem.Instance.Setup(PVPMapData.MapData);
        PVPSystem.Instance.Show(true);
        UIManager.inst.Cloud.PokeCloud((System.Action) (() => UIManager.inst.tileCamera.StartAutoZoom()));
        this.OnGameModeReady();
        break;
      case GameEngine.GameMode.PitMode:
        UIManager.inst.timerHUD.Display = true;
        DBManager.inst.DB_March.ClearThirdPartMarch();
        MapUtils.RegisterRouteMapping();
        PVPMapData.Initialize();
        PitMapData.Initialize();
        PVPSystem.Instance.Setup(PitMapData.MapData);
        PVPSystem.Instance.Show(true);
        UIManager.inst.Cloud.PokeCloud((System.Action) (() => UIManager.inst.tileCamera.StartAutoZoom()));
        this.OnGameModeReady();
        break;
      case GameEngine.GameMode.MerlinTowerMode:
        CitadelSystem.inst.Dispose(true);
        PVPSystem.Instance.Shutdown();
        MerlinTowerFacade.OnEnter();
        UIManager.inst.Cloud.PokeCloud((System.Action) null);
        break;
    }
    this.PostLoadScene();
  }

  private void PreLoadScene()
  {
    SkillEffectManager.Instance.StopAll(true);
    UIManager.inst.timerHUD.Display = false;
  }

  private void PostLoadScene()
  {
    Resources.UnloadUnusedAssets();
    GC.Collect();
  }

  private void CheckRestartGame(bool pause)
  {
    if (pause)
    {
      this.m_timeEnterPause = DateTime.Now;
    }
    else
    {
      if ((DateTime.Now - this.m_timeEnterPause).TotalSeconds < (double) this.m_restartTime)
        return;
      this.MarkRestartGame(GameEngine.LoadMode.Lite);
    }
  }

  private void OnApplicationPause(bool state)
  {
    this.CheckRestartGame(state);
    NetWorkDetector.Instance.OnApplicationFocus(!state);
    if (!state)
    {
      this._resumeTime = Time.time;
      Application.targetFrameRate = 60;
      NotificationManager.Instance.Stop();
    }
    else
    {
      if (GameEngine.IsReady() && GameEngine.IsAvailable)
      {
        NotificationManager.Instance.Start();
        UIManager.inst.OnLostDevice();
      }
      Application.targetFrameRate = 1;
    }
  }

  private void OnApplicationFocus(bool state)
  {
  }

  private void OnApplicationQuit()
  {
    if (PlayerData.inst != null)
      PlayerData.inst.onlineFriendsManager.Logoff();
    PushManager.inst.Dispose();
    GameEngine._shuttingDown = true;
    GameEngine._initialized = false;
    PlatformAdaptor.NotifyApplicationQuit();
  }

  public void MarkRestartGame(GameEngine.LoadMode mode = GameEngine.LoadMode.Deep)
  {
    this.restartGameFlag = true;
    if (GameEngine.ReloadMode != mode)
      GameEngine.ReloadMode = mode;
    LoginErrDialog.LoginErrLog(nameof (MarkRestartGame));
  }

  public void LockEscape()
  {
    this.lockBack = true;
  }

  public void UnLockEscape()
  {
    this.lockBack = false;
  }

  private void Update()
  {
    if (this.logqueue.Count > 0)
    {
      GameEngine.LogMessage result = (GameEngine.LogMessage) null;
      this.logqueue.TryDequeue(out result);
      if (result != null)
        this.LogCallback(result.logstring, result.stacktrace, result.logtype);
    }
    if (this.restartGameFlag)
    {
      this.restartGameFlag = false;
      this.RestartGame();
    }
    if (!GameEngine._initialized || Application.platform != RuntimePlatform.Android && !Application.isEditor || (!Input.GetKeyDown(KeyCode.Escape) || TutorialManager.Instance.IsRunning || (RebuildPopup.IsRunning || ForceBackPopup.IsRunning)) || (SendBackPopup.IsRunning || this.lockBack || this._uiManager.OnEscapeHandler()))
      return;
    string str1 = ScriptLocalization.Get("exit_game_description", true);
    string str2 = ScriptLocalization.Get("exit_game_confirm_button", true);
    string str3 = ScriptLocalization.Get("exit_game_cancel_button", true);
    string str4 = ScriptLocalization.Get("exit_game_title", true);
    UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
    {
      setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
      title = str4,
      leftButtonText = str3,
      leftButtonClickEvent = (System.Action) null,
      rightButtonText = str2,
      rightButtonClickEvent = new System.Action(this.ConfirmQuitGameCallBack),
      description = str1
    });
  }

  private void ConfirmQuitGameCallBack()
  {
    this.QuitGame();
  }

  public void QuitGame()
  {
    if (GameEngine._initialized)
      NotificationManager.Instance.Start();
    Application.Quit();
  }

  private void FixedUpdate()
  {
    if (!GameEngine._initialized || GameDataManager.inst.mScriptCommands.Count <= 0)
      return;
    for (int index = GameDataManager.inst.mScriptCommands.Count - 1; index >= 0; --index)
    {
      CmdAction mScriptCommand = GameDataManager.inst.mScriptCommands[index] as CmdAction;
      --mScriptCommand.counter;
      if (mScriptCommand.counter <= 0)
        GameDataManager.inst.mScriptCommands.RemoveAt(index);
    }
  }

  private bool NGUITouchFilter(int fingerIndex, Vector2 position)
  {
    FingerGestures.Finger finger = FingerGestures.GetFinger(fingerIndex);
    if (finger == null)
      return false;
    UICamera.MouseOrTouch mouseOrTouch = !finger.IsTouch ? UICamera.GetMouse(fingerIndex) : UICamera.GetTouch(fingerIndex);
    if (mouseOrTouch == null)
      return false;
    if (!((UnityEngine.Object) mouseOrTouch.current == (UnityEngine.Object) null) && !((UnityEngine.Object) mouseOrTouch.current.GetComponent<UIRoot>() != (UnityEngine.Object) null) && !mouseOrTouch.current.CompareTag("ClickThrough"))
      return mouseOrTouch.current.layer == this.TileLayer;
    return true;
  }

  public static void devDeletePlayer()
  {
    string uniqueIdentifier = SystemInfo.deviceUniqueIdentifier;
    WWW www = new WWW(PlayerPrefsEx.GetString("ServerURL", "http://koa-dev.kingsgroup.cc/api/") + "?key=1&class=Debug&method=resetAccountByUUID&params[uuid]=" + AccountManager.Instance.AccountId);
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    while (!www.isDone)
    {
      if ((double) Time.realtimeSinceStartup - (double) realtimeSinceStartup > 3.0)
      {
        D.error((object) "Timeout resetting player");
        break;
      }
    }
  }

  public void PlayOpeningCinematic()
  {
    if (PlayerPrefsEx.HasKey("openingcinematic"))
      return;
    Handheld.PlayFullScreenMovie("openingcinematic.mp4", Color.black, FullScreenMovieControlMode.Hidden);
    PlayerPrefsEx.SetInt("openingcinematic", 1);
  }

  private void LogCallback(string logString, string stackTrace, LogType type)
  {
    LogPipeline.Log(logString, stackTrace, type);
    if (type == LogType.Exception)
      this.HandleException(logString, stackTrace);
    else if (type == LogType.Assert)
      ;
  }

  private void LogHandle(string logString, string stackTrace, LogType type)
  {
    this.logqueue.Enqueue(new GameEngine.LogMessage()
    {
      logstring = logString,
      stacktrace = stackTrace,
      logtype = type
    });
  }

  public float iTweenValue { get; set; }

  public void iTweenUpdateValueToHandler(float value)
  {
    this.iTweenValue = value;
  }

  [DebuggerHidden]
  private IEnumerator DelayToInvokeDo(System.Action action, float delaySeconds)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GameEngine.\u003CDelayToInvokeDo\u003Ec__Iterator61()
    {
      delaySeconds = delaySeconds,
      action = action,
      \u003C\u0024\u003EdelaySeconds = delaySeconds,
      \u003C\u0024\u003Eaction = action
    };
  }

  public void DelayToInvoke(System.Action action, float delaySeconds)
  {
    this.StartCoroutine(this.DelayToInvokeDo(action, delaySeconds));
  }

  public void OnHandleUnresolvedException(object sender, UnhandledExceptionEventArgs args)
  {
    if (args == null || args.ExceptionObject == null || args.ExceptionObject.GetType() != typeof (Exception))
      return;
    Exception exceptionObject = (Exception) args.ExceptionObject;
    LogPipeline.Log(exceptionObject.Source, exceptionObject.StackTrace, LogType.Exception);
    this.HandleException(exceptionObject.Source, exceptionObject.StackTrace);
  }

  public void HandleException(string log, string stack)
  {
    D.warn((object) ("HandleException~~ " + log));
    NetWorkDetector.Instance.PushClientError(log, stack);
    if (NetWorkDetector.Instance.WaittingRestart)
      return;
    UIButton.current = (UIButton) null;
    if (!NetWorkDetector.Instance.IsReadyRestart())
      NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.Other, "UnHandleException", log + "|" + stack);
    NetWorkDetector.Instance.RestartGame(true);
  }

  public static void ClearResourceCache()
  {
    new DirectoryInfo(Application.persistentDataPath).Delete(true);
  }

  public void ReceiveLocalNotificaitonData(string jsonData)
  {
    if (string.IsNullOrEmpty(jsonData))
      return;
    string data = jsonData;
    if (jsonData.Contains("="))
      data = jsonData.Split('=')[1];
    NotificationManager.Instance.OnReviceNotificationData(data);
  }

  private void RegisterConstructor()
  {
    AotTypes._unused1.GetType();
    AotTypes._unused2.GetType();
    AotTypes._unused3.GetType();
    AotTypes._unused4.GetType();
    AotTypes._unused5.GetType();
    AotTypes._unused6.GetType();
  }

  public enum GameMode
  {
    InitMode,
    LoadingMode,
    CityMode,
    DragonKnight,
    PVPMode,
    PitMode,
    MerlinTowerMode,
  }

  public enum LoadMode
  {
    Lite,
    Deep,
  }

  public class LogMessage
  {
    public string logstring;
    public string stacktrace;
    public LogType logtype;
  }

  private delegate void ReadConfigFunction(string str);

  private delegate void ReadBundleFunction(WWW www);
}
