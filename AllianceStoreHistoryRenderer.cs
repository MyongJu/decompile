﻿// Decompiled with JetBrains decompiler
// Type: AllianceStoreHistoryRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UnityEngine;

public class AllianceStoreHistoryRenderer : MonoBehaviour
{
  public UILabel m_History;

  public void SetData(AllianceStoreType type, Hashtable itemData)
  {
    string format = type != AllianceStoreType.Fund ? Utils.XLAT("alliance_store_honor_history_description") : Utils.XLAT("alliance_store_fund_history_description");
    long serverTimestamp = (long) NetServerTime.inst.ServerTimestamp;
    DatabaseTools.UpdateData(itemData, "ctime", ref serverTimestamp);
    long duration = (long) NetServerTime.inst.ServerTimestamp - serverTimestamp;
    int outData1 = 0;
    DatabaseTools.UpdateData(itemData, "item_id", ref outData1);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(outData1);
    string empty = string.Empty;
    DatabaseTools.UpdateData(itemData, "name", ref empty);
    int outData2 = 0;
    DatabaseTools.UpdateData(itemData, "amount", ref outData2);
    this.m_History.text = string.Format(format, (object) Utils.FormatOfflineTime(duration), (object) empty, (object) itemStaticInfo.LocName, (object) outData2);
  }
}
