﻿// Decompiled with JetBrains decompiler
// Type: EnhanceEquipmentComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class EnhanceEquipmentComponent : EquipmentComponent
{
  public UILabel currentLevelLabel;
  public UILabel nextLevelLabel;
  public GameObject nextLevelGameObject;

  public override void Init()
  {
    this.componentData = this.data as EquipmentComponentData;
    InventoryItemData myItemByItemId = DBManager.inst.GetInventory(this.componentData.bagType).GetMyItemByItemID(this.componentData.itemId);
    int enhanced = myItemByItemId.enhanced;
    this.nextLevelGameObject.SetActive(!EquipmentManager.Instance.IsEnhancedToMax(this.componentData.bagType, this.componentData.itemId));
    this.currentLevelLabel.text = enhanced.ToString();
    this.nextLevelLabel.text = (enhanced + 1).ToString();
    this.equipmentScrollName.text = this.componentData.itemInfo.LocName + (myItemByItemId.enhanced <= 0 ? string.Empty : "+" + (object) myItemByItemId.enhanced);
    int level = PlayerData.inst.heroData.level;
    if (this.componentData.bagType == BagType.Hero)
      this.levelRequireLabel.text = (this.componentData.equipmentInfo.heroLevelMin > level ? "[FF0000]" : string.Empty) + string.Format(Utils.XLAT("forge_equip_requirement"), (object) this.componentData.equipmentInfo.heroLevelMin);
    else
      this.levelRequireLabel.text = (this.componentData.equipmentInfo.heroLevelMin > DragonKnightUtils.GetLevel() ? "[FF0000]" : string.Empty) + string.Format(Utils.XLAT("dragon_knight_equip_level_requirement"), (object) this.componentData.equipmentInfo.heroLevelMin);
    if ((UnityEngine.Object) this.isEquiped != (UnityEngine.Object) null)
      this.isEquiped.SetActive(myItemByItemId.isEquipped);
    EquipmentManager.Instance.ConfigQualityLabelWithColor(this.equipmentScrollName, this.componentData.equipmentInfo.quality);
    BuilderFactory.Instance.Build((UIWidget) this.icon, this.componentData.itemInfo.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.background, this.componentData.equipmentInfo.QualityImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
  }

  public override void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
    BuilderFactory.Instance.Release((UIWidget) this.background);
  }
}
