﻿// Decompiled with JetBrains decompiler
// Type: PVPDigsiteBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UnityEngine;

public class PVPDigsiteBanner : SpriteBanner
{
  public GameObject m_RootNode;
  public UISpriteMeshProgressBar m_Slider;
  public TextMesh m_TextTime;
  private TileData m_TileData;

  public override void UpdateUI(TileData tile)
  {
    base.UpdateUI(tile);
    if (this.m_TileData != tile)
      this.m_TileData = tile;
    this.UpdateUI();
  }

  public void OnEnable()
  {
    Oscillator.Instance.updateEvent += new System.Action<double>(this.OnUpdate);
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.updateEvent -= new System.Action<double>(this.OnUpdate);
  }

  protected void OnUpdate(double delta)
  {
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this.m_TileData != null)
    {
      DigSiteData digSiteData = this.m_TileData.DigSiteData;
      if (digSiteData != null)
      {
        this.m_RootNode.SetActive(true);
        TreasureMapInfo treasureMapInfo = ConfigManager.inst.DB_TreasureMap.Get(digSiteData.ConfigId);
        long val2 = this.m_TileData.MarchID != 0L ? digSiteData.DigTime - ((long) NetServerTime.inst.ServerTimestamp - digSiteData.DigStartTime) : digSiteData.DigTime;
        float progress = Mathf.Clamp01((float) ((long) treasureMapInfo.dig_time - val2) / (float) treasureMapInfo.dig_time);
        long num = Math.Max(0L, val2);
        this.m_Slider.SetProgress(progress);
        this.m_TextTime.text = Utils.FormatTime1((int) num);
      }
      else
        this.OnError();
    }
    else
      this.OnError();
  }

  private void OnError()
  {
    this.m_RootNode.SetActive(false);
  }
}
