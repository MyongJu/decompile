﻿// Decompiled with JetBrains decompiler
// Type: GUIView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUIView : MonoBehaviour
{
  private string playername = "Noname";
  private const string PROGRESS_KEY = "Progress";
  private const string MUTED_KEY = "IsSoundMuted";
  private const string SCORE_KEY = "Highscore";
  private const string PLAYERNAME_KEY = "PlayerName";
  private float progress;
  private bool muted;
  private int score;

  private void Start()
  {
    this.RefreshData();
  }

  private void Update()
  {
    if (!Input.anyKeyDown)
      return;
    this.RefreshData();
  }

  public void RefreshData()
  {
    this.progress = PlayerPrefs.GetFloat("Progress", 100f);
    this.muted = PlayerPrefs.GetString("IsSoundMuted", "true") == "true";
    this.score = PlayerPrefs.GetInt("Highscore", 123);
    this.playername = PlayerPrefs.GetString("PlayerName", "Noname");
  }

  public void SaveData()
  {
    PlayerPrefs.SetFloat("Progress", this.progress);
    PlayerPrefs.SetString("IsSoundMuted", !this.muted ? "false" : "true");
    PlayerPrefs.SetInt("Highscore", this.score);
    PlayerPrefs.SetString("PlayerName", this.playername);
  }
}
