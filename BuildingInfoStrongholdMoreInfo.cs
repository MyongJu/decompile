﻿// Decompiled with JetBrains decompiler
// Type: BuildingInfoStrongholdMoreInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingInfoStrongholdMoreInfo : Popup
{
  private List<GameObject> items = new List<GameObject>();
  public GameObject mBuildingStatsItemPrefab;

  private void ToShow()
  {
    float y = this.mBuildingStatsItemPrefab.transform.localPosition.y;
    float height = (float) this.mBuildingStatsItemPrefab.GetComponent<UIWidget>().height;
    using (List<GameObject>.Enumerator enumerator = this.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Object.Destroy((Object) enumerator.Current);
    }
    this.items.Clear();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType("stronghold");
    for (int level = 1; level <= maxLevelByType; ++level)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData("stronghold_" + level.ToString());
      GameObject gob = Utils.DuplicateGOB(this.mBuildingStatsItemPrefab);
      gob.SetActive(true);
      Utils.SetLPY(gob, y);
      y -= height;
      int benefitValue1 = (int) data.Benefit_Value_1;
      int benefitValue2 = (int) data.Benefit_Value_2;
      gob.GetComponent<BuildInfoResourceStatItem>().SetDetails(benefitValue1.ToString(), benefitValue2, level);
      this.items.Add(gob);
    }
  }

  public void OnBackBtnPressed()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.ToShow();
  }
}
