﻿// Decompiled with JetBrains decompiler
// Type: RestoreManaPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class RestoreManaPopup : Popup
{
  [SerializeField]
  protected UILabel m_labelMana;
  [SerializeField]
  protected UISlider m_sliderManaCurrent;
  [SerializeField]
  protected UISlider m_sliderManaTarget;
  [SerializeField]
  protected UILabel m_labelElixirLimit;
  [SerializeField]
  protected UISlider m_sliderElixirLimitCurrent;
  [SerializeField]
  protected UISlider m_sliderElixirLimitTarget;
  [SerializeField]
  protected StandardProgressBar m_progressBar;
  [SerializeField]
  protected Icon m_itemIcon;
  [SerializeField]
  protected UIButton m_buttonRestore;
  protected ItemStaticInfo _ElixirItemInfo;

  protected int CurrentSelectCount
  {
    get
    {
      return this.m_progressBar.CurrentCount;
    }
  }

  protected int ElixirCount
  {
    get
    {
      ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(this.ElixirItemInfo.internalId);
      if (consumableItemData != null)
        return consumableItemData.quantity;
      return 0;
    }
  }

  protected int MaxSelectCount
  {
    get
    {
      int val1 = (this.MaxElixirLimitCount - this.CurrentElixirLimitCount) / this.ManaPointAddedPerElixir;
      int val2 = (this.MaxManaPoint - this.CurrentManaPoint) / this.ManaPointAddedPerElixir;
      if (this.CurrentManaPoint + val2 * this.ManaPointAddedPerElixir < this.MaxManaPoint)
        ++val2;
      return Math.Min(this.ElixirCount, Math.Min(val1, val2));
    }
  }

  protected int MaxManaPoint
  {
    get
    {
      AllianceTempleData allianceTempleData = DBManager.inst.DB_AllianceTemple.GetMyAllianceTempleData();
      if (allianceTempleData != null)
      {
        AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(allianceTempleData.ConfigId);
        if (buildingStaticInfo != null)
          return (int) buildingStaticInfo.Param1;
      }
      return 100;
    }
  }

  protected int CurrentManaPoint
  {
    get
    {
      AllianceData allianceData = PlayerData.inst.allianceData;
      if (allianceData != null)
        return allianceData.manaPoint;
      return 0;
    }
  }

  protected int TargetManaPoint
  {
    get
    {
      return this.CurrentManaPoint + this.CurrentSelectCount * this.ManaPointAddedPerElixir;
    }
  }

  protected int ManaPointAddedPerElixir
  {
    get
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_alliance_temple_mana");
      if (itemStaticInfo != null)
        return (int) itemStaticInfo.Value;
      return 0;
    }
  }

  protected int MaxElixirLimitCount
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_temple_donation_repletion_max");
      if (data != null)
        return data.ValueInt;
      return 0;
    }
  }

  protected int CurrentElixirLimitCount
  {
    get
    {
      AllianceData allianceData = PlayerData.inst.allianceData;
      if (allianceData != null)
        return allianceData.elixirLimit;
      return 0;
    }
  }

  protected int TargetElixirLimitCount
  {
    get
    {
      return this.CurrentElixirLimitCount + this.CurrentSelectCount * this.ManaPointAddedPerElixir;
    }
  }

  public ItemStaticInfo ElixirItemInfo
  {
    get
    {
      if (this._ElixirItemInfo == null)
        this._ElixirItemInfo = ConfigManager.inst.DB_Items.GetItem("item_alliance_temple_mana");
      return this._ElixirItemInfo;
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_progressBar.CurrentCount = 0;
    this.m_progressBar.onValueChanged += new System.Action<int>(this.OnSelectCountChanged);
    DBManager.inst.DB_Alliance.onDataChanged += new System.Action<long>(this.OnAllianceDataChanged);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    DBManager.inst.DB_Alliance.onDataChanged -= new System.Action<long>(this.OnAllianceDataChanged);
  }

  protected void OnAllianceDataChanged(long allianceId)
  {
    if (allianceId != PlayerData.inst.allianceId)
      return;
    this.UpdateUI();
  }

  protected double CdInHours
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_temple_mana_donation_join_cd");
      if (data != null)
        return data.Value;
      return 0.0;
    }
  }

  protected void UpdateUI()
  {
    this.m_labelMana.text = string.Format("{0}/{1}({2:0.00%})", (object) this.TargetManaPoint, (object) this.MaxManaPoint, (object) (float) ((double) this.TargetManaPoint / (double) this.MaxManaPoint));
    this.m_sliderManaCurrent.value = (float) this.CurrentManaPoint / (float) this.MaxManaPoint;
    this.m_sliderManaTarget.value = (float) this.TargetManaPoint / (float) this.MaxManaPoint;
    this.m_labelElixirLimit.text = string.Format("{0}/{1}({2:0.00%})", (object) this.TargetElixirLimitCount, (object) this.MaxElixirLimitCount, (object) (float) ((double) this.TargetElixirLimitCount / (double) this.MaxElixirLimitCount));
    this.m_sliderElixirLimitCurrent.value = (float) this.CurrentElixirLimitCount / (float) this.MaxElixirLimitCount;
    this.m_sliderElixirLimitTarget.value = (float) this.TargetElixirLimitCount / (float) this.MaxElixirLimitCount;
    int pointAddedPerElixir = this.ManaPointAddedPerElixir;
    this.m_itemIcon.SetData(this.ElixirItemInfo.ImagePath, new string[3]
    {
      this.ElixirItemInfo.LocName,
      ScriptLocalization.GetWithPara("alliance_altar_use_elixir_description", new Dictionary<string, string>()
      {
        {
          "0",
          pointAddedPerElixir.ToString()
        },
        {
          "1",
          pointAddedPerElixir.ToString()
        },
        {
          "2",
          this.CdInHours.ToString()
        }
      }, true),
      this.ElixirCount.ToString()
    });
    this.m_progressBar.Init(Math.Min(this.CurrentSelectCount, this.MaxSelectCount), this.MaxSelectCount);
    this.m_buttonRestore.isEnabled = this.CurrentSelectCount > 0;
  }

  protected void OnSelectCountChanged(int count)
  {
    this.UpdateUI();
  }

  public void OnGiveToPrietessButtonClicked()
  {
    if (PlayerData.inst.allianceData == null)
      return;
    AllianceMemberData allianceMemberData = PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid);
    if (allianceMemberData == null)
      return;
    int num1 = NetServerTime.inst.ServerTimestamp - (int) allianceMemberData.joinTime;
    int num2 = (int) (this.CdInHours * 60.0 * 60.0);
    if (num1 <= num2)
    {
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_wait_time_remaining", new Dictionary<string, string>()
      {
        {
          "0",
          ((num2 - num1) / 60 + 1).ToString()
        }
      }, true), (System.Action) null, 4f, true);
    }
    else
    {
      if (this.CurrentSelectCount <= 0)
        return;
      MessageHub.inst.GetPortByAction("Item:useConsumableItem").SendRequest(new Hashtable()
      {
        {
          (object) "uid",
          (object) PlayerData.inst.uid
        },
        {
          (object) "item_id",
          (object) this.ElixirItemInfo.internalId
        },
        {
          (object) "city_id",
          (object) PlayerData.inst.cityId
        },
        {
          (object) "amount",
          (object) this.CurrentSelectCount
        }
      }, new System.Action<bool, object>(this.OnGriveToPrietessCallback), true);
    }
  }

  protected void OnGriveToPrietessCallback(bool result, object data)
  {
    if (!result)
      return;
    this.OnCloseButtonClicked();
  }

  public void OnCloseButtonClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }
}
