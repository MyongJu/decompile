﻿// Decompiled with JetBrains decompiler
// Type: ConfigMagicStore
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigMagicStore : MonoBehaviour
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, MagicStoreInfo> datas;
  private Dictionary<int, MagicStoreInfo> dicByUniqueId;
  private List<int> _costItems;

  public void BuildDB(object res)
  {
    this.parse.Parse<MagicStoreInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public List<int> GetAllCostItems()
  {
    if (this._costItems == null)
    {
      this._costItems = new List<int>();
      List<ItemStaticInfo> itemStaticInfoList = new List<ItemStaticInfo>();
      Dictionary<string, MagicStoreInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current != null && enumerator.Current.CostItemId > 0)
        {
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(enumerator.Current.CostItemId);
          if (!itemStaticInfoList.Contains(itemStaticInfo))
            itemStaticInfoList.Add(itemStaticInfo);
        }
      }
      itemStaticInfoList.Sort(new Comparison<ItemStaticInfo>(this.SortDelegate));
      for (int index = 0; index < itemStaticInfoList.Count; ++index)
        this._costItems.Add(itemStaticInfoList[index].internalId);
    }
    return this._costItems;
  }

  private int SortDelegate(ItemStaticInfo a, ItemStaticInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }

  public MagicStoreInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (MagicStoreInfo) null;
  }

  public MagicStoreInfo GetData(int internalId)
  {
    if (this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (MagicStoreInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, MagicStoreInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, MagicStoreInfo>) null;
  }
}
