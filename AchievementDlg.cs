﻿// Decompiled with JetBrains decompiler
// Type: AchievementDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AchievementDlg : UI.Dialog
{
  private Dictionary<int, AchievementItemRenderer> items = new Dictionary<int, AchievementItemRenderer>();
  private Dictionary<int, int> group2itemId = new Dictionary<int, int>();
  public UILabel title;
  public UIGrid container;
  public UIScrollView scrollView;
  public AchievementItemRenderer itemPrefab;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.container.sorting = UIGrid.Sorting.Custom;
    this.container.onCustomSort += new Comparison<Transform>(this.compare);
    this.UpdateUI();
    this.AddEventHandler();
    if (!GooglePlayGameManager.FunctionEnabled)
      return;
    GooglePlayGameManager.Instance.ReportAllAchievement();
  }

  private int compare(Transform a, Transform b)
  {
    AchievementItemRenderer component1 = a.gameObject.GetComponent<AchievementItemRenderer>();
    AchievementItemRenderer component2 = b.gameObject.GetComponent<AchievementItemRenderer>();
    AchievementData achievementData1 = DBManager.inst.DB_Achievements.Get(component1.AchievementId);
    AchievementData achievementData2 = DBManager.inst.DB_Achievements.Get(component2.AchievementId);
    return Math.Sign(achievementData1.GetAchievementPriority(achievementData1.State) - achievementData2.GetAchievementPriority(achievementData2.State));
  }

  private void ReSortItems()
  {
    this.container.Reposition();
    this.scrollView.ResetPosition();
  }

  private void UpdateUI()
  {
    List<AchievementData> totalDatas = DBManager.inst.DB_Achievements.GetTotalDatas();
    this.items.Clear();
    for (int index = 0; index < totalDatas.Count; ++index)
    {
      GameObject go = NGUITools.AddChild(this.container.gameObject, this.itemPrefab.gameObject);
      NGUITools.SetActive(go, true);
      AchievementItemRenderer component = go.GetComponent<AchievementItemRenderer>();
      component.SetData(totalDatas[index].AchievementId);
      component.OnResortHandler = new System.Action(this.ReSortItems);
      this.items.Add(totalDatas[index].AchievementId, component);
      this.group2itemId.Add(component.GroupId, totalDatas[index].AchievementId);
    }
    this.container.Reposition();
    this.scrollView.ResetPosition();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Achievements.onDataRemove += new System.Action<long>(this.OnRemoveHandler);
    DBManager.inst.DB_Achievements.onDataCreated += new System.Action<long>(this.OnAddHandler);
    DBManager.inst.DB_Achievements.onDataChanged += new System.Action<long>(this.OnAddHandler);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Achievements.onDataRemove -= new System.Action<long>(this.OnRemoveHandler);
    DBManager.inst.DB_Achievements.onDataCreated -= new System.Action<long>(this.OnAddHandler);
    DBManager.inst.DB_Achievements.onDataChanged -= new System.Action<long>(this.OnAddHandler);
  }

  private void OnRemoveHandler(long id)
  {
    if (!this.items.ContainsKey((int) id))
      return;
    this.items[(int) id].Clear();
  }

  private void OnAddHandler(long id)
  {
    AchievementData achievementData = DBManager.inst.DB_Achievements.Get((int) id);
    if (!this.group2itemId.ContainsKey(achievementData.GroupId))
      return;
    this.items[this.group2itemId[achievementData.GroupId]].SetData((int) id);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    Dictionary<int, AchievementItemRenderer>.ValueCollection.Enumerator enumerator = this.items.Values.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Clear();
    this.group2itemId.Clear();
    this.items.Clear();
    this.RemoveEventHandler();
  }
}
