﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerHospital
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class BuildingControllerHospital : BuildingControllerNew
{
  private const string canhealingvfx = "Prefab/VFX/fx_yiyuanzhiliao_01";
  public const string hospitalworkingEffect = "Prefab/VFX/fx_yiyuanzhiliao_02";
  public GameObject mHealingIcon;
  private long vfxhandle;
  private bool haseffect;
  public Transform effectroot;
  private long workingvfxhandle;

  public override void InitBuilding()
  {
    base.InitBuilding();
    Oscillator.Instance.secondEvent += new System.Action<int>(this.CheckState);
    DBManager.inst.DB_City.onDataChanged += new System.Action<long>(this.HandleonDataChangedged);
    DBManager.inst.DB_AllianceHospital.onDataChanged += new System.Action<AllianceHospitalData>(this.OnAllianceHospitalDataChanged);
  }

  public override void Dispose()
  {
    base.Dispose();
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.CheckState);
    DBManager.inst.DB_City.onDataChanged -= new System.Action<long>(this.HandleonDataChangedged);
    DBManager.inst.DB_AllianceHospital.onDataChanged -= new System.Action<AllianceHospitalData>(this.OnAllianceHospitalDataChanged);
  }

  private void HandleonDataChangedged(long obj)
  {
    this.UpdateHealingIcon();
  }

  private void OnAllianceHospitalDataChanged(AllianceHospitalData allianceHospitalData)
  {
    this.UpdateHealingIcon();
  }

  private void UpdateHealingIcon()
  {
    bool show = false;
    if (DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).hospitalTroops.totalTroopsCount > 0L)
      show = true;
    if (DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).kingdomTroops.totalTroopsCount > 0L)
      show = true;
    AllianceHospitalData myHospitalData = DBManager.inst.DB_AllianceHospital.GetMyHospitalData();
    if (myHospitalData != null && myHospitalData.GetMyInjuredTroopCount() > 0L)
      show = true;
    this.ShowCanHealingIcon(show);
  }

  private void ShowCanHealingIcon(bool show)
  {
    if (show)
    {
      if (this.vfxhandle != 0L)
        return;
      this.vfxhandle = VfxManager.Instance.CreateAndPlay("Prefab/VFX/fx_yiyuanzhiliao_01", this.mHealingIcon.transform);
    }
    else
    {
      if (this.vfxhandle == 0L)
        return;
      VfxManager.Instance.Delete(this.vfxhandle);
      this.vfxhandle = 0L;
    }
  }

  private void CheckState(int obj)
  {
    bool flag = this.isHealing();
    if (flag && !this.haseffect)
    {
      this.haseffect = true;
      if (this.workingvfxhandle != 0L)
        return;
      this.workingvfxhandle = VfxManager.Instance.CreateAndPlay("Prefab/VFX/fx_yiyuanzhiliao_02", this.effectroot);
    }
    else
    {
      if (flag || !this.haseffect)
        return;
      this.haseffect = false;
      if (this.workingvfxhandle == 0L)
        return;
      VfxManager.Instance.Delete(this.workingvfxhandle);
      this.workingvfxhandle = 0L;
    }
  }

  public override void SetLevel(int value, bool loadanim = true)
  {
    base.SetLevel(value, loadanim);
    if (!loadanim)
      return;
    this.UpdateHealingIcon();
  }

  public override void AddActionButton(ref List<CityCircleBtnPara> ftl)
  {
    this.CCBPair = CityCircleBtnCollection.Instance.CircleBtnPair;
    ftl.Add(this.CCBPair["buildinginfo"]);
    bool flag1 = this.mBuildingItem.mLevel < ConfigManager.inst.DB_Building.GetMaxLevelByType("hospital");
    bool flag2 = this.mBuildingItem.mLevel > 0;
    bool flag3 = false;
    int num = 0;
    long mBuildingJobId = this.mBuildingItem.mBuildingJobID;
    if (mBuildingJobId > 0L)
    {
      JobHandle unfinishedJob = JobManager.Instance.GetUnfinishedJob(mBuildingJobId);
      if (unfinishedJob != null)
      {
        flag3 = true;
        num = unfinishedJob.LeftTime() - CityManager.inst.GetFreeSpeedUpTime();
        if (unfinishedJob.GetJobEvent() == JobEvent.JOB_BUILDING_DECONSTRUCTION)
          flag2 = false;
      }
    }
    if (flag1 && !this.isHealing() && !flag3)
      ftl.Add(this.CCBPair["buildingupgrade"]);
    if (this.isHealing() || flag3)
    {
      ftl.Add(this.CCBPair["actionSpeedup"]);
      JobHandle singleJobByClass = JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_HEALING_TROOPS);
      if (singleJobByClass != null)
        num = singleJobByClass.LeftTime();
      if (num > 0)
      {
        CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "gold_speed_up", "id_gold_speedup", new EventDelegate((MonoBehaviour) CityTouchCircle.Instance, "OnUseGoldSpeedUpBtnClicked"), true, (string) null, AssetManager.Instance.HandyLoad("Prefab/City/GoldBoostResAttachment", (System.Type) null) as GameObject, (CityTouchCircleBtnAttachment.AttachmentData) new GoldBoostResAttachment.GBRAData()
        {
          remainTime = num
        }, false);
        ftl.Add(cityCircleBtnPara);
      }
    }
    if (flag2)
      ftl.Add(this.CCBPair["hospital"]);
    AllianceHospitalData myHospitalData = DBManager.inst.DB_AllianceHospital.GetMyHospitalData();
    if (flag2 && myHospitalData != null && myHospitalData.CurrentState == AllianceHospitalData.State.COMPLETE)
      ftl.Add(this.CCBPair["alliance_hospital"]);
    ftl.Add(this.CCBPair["kingdom_hospital"]);
  }

  private bool isHealing()
  {
    Dictionary<long, JobHandle> jobDictByClass = JobManager.Instance.GetJobDictByClass(JobEvent.JOB_HEALING_TROOPS);
    return jobDictByClass != null && jobDictByClass.Count > 0;
  }

  public override bool IsIdleState()
  {
    if (!this.isHealing())
      return base.IsIdleState();
    return false;
  }
}
