﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomMemberBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ChatRoomMemberBar : MonoBehaviour
{
  public static ChatRoomMemberBar.Parameter param = new ChatRoomMemberBar.Parameter();
  private ChatRoomMemberBar.Data data = new ChatRoomMemberBar.Data();
  private List<long> onlineChannels;
  [SerializeField]
  private ChatRoomMemberBar.Panel panel;

  public void Setup(ChatRoomMemberBar.Parameter param)
  {
    this.data.SetData(param);
    if (this.data.selfTitle == ChatRoomMember.Title.owner)
    {
      this.panel.BT_Demote.gameObject.SetActive(this.data.member.title == ChatRoomMember.Title.admin);
      this.panel.BT_Promote.gameObject.SetActive(this.data.member.title == ChatRoomMember.Title.member);
    }
    else
    {
      this.panel.BT_Demote.gameObject.SetActive(false);
      this.panel.BT_Promote.gameObject.SetActive(false);
    }
    this.panel.BT_Silence.gameObject.SetActive(this.data.selfTitle == ChatRoomMember.Title.owner);
    this.panel.BT_Kick.gameObject.SetActive(this.data.selfTitle == ChatRoomMember.Title.owner);
    this.panel.BT_Ban.gameObject.SetActive(this.data.selfTitle == ChatRoomMember.Title.owner);
    this.panel.SilenceTitle.text = this.data.member.status == ChatRoomMember.Status.silence ? Utils.XLAT("chat_uppercase_unsilence_button") : Utils.XLAT("chat_uppercase_silence_button");
    if (this.data.member.title == ChatRoomMember.Title.blackList)
    {
      this.panel.BT_Silence.gameObject.SetActive(false);
      this.panel.BT_Kick.gameObject.SetActive(false);
      this.panel.BT_Ban.gameObject.SetActive(true);
      this.panel.BanTitle.text = Utils.XLAT("chat_uppercase_lift_ban_button");
    }
    else
    {
      this.panel.BT_Silence.gameObject.SetActive(this.data.selfTitle > this.data.member.title);
      this.panel.BT_Kick.gameObject.SetActive(this.data.selfTitle > this.data.member.title);
      this.panel.BT_Ban.gameObject.SetActive(this.data.selfTitle > this.data.member.title);
      this.panel.BanTitle.text = Utils.XLAT("chat_uppercase_ban_button");
    }
    this.panel.playerName.text = this.data.member.userName;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.playerIcon, UserData.GetPortraitIconPath(this.data.member.portrait), (System.Action<bool>) null, true, false, string.Empty);
    List<long> uid = new List<long>();
    uid.Add(this.data.member.channelId);
    if (uid.Count <= 0)
      return;
    PushManager.inst.GetOnlineUsers(uid, new System.Action<List<long>>(this.GetOnlineUsersCallback));
  }

  private void GetOnlineUsersCallback(List<long> result)
  {
    this.onlineChannels = result;
  }

  private void Update()
  {
    if (this.onlineChannels == null)
      return;
    if (this.onlineChannels.Contains(this.data.member.channelId))
      this.panel.onlineState.color = Color.green;
    else
      this.panel.onlineState.color = Color.red;
    this.onlineChannels = (List<long>) null;
  }

  public void OnPlayerClick()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    if (this.data.member.uid == PlayerData.inst.uid)
      UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    else
      UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
      {
        uid = this.data.member.uid
      }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnSilenceClick()
  {
    if (this.data.member.status == ChatRoomMember.Status.normal)
      MessageHub.inst.GetPortByAction("Chat:silenceUser").SendRequest(Utils.Hash((object) "s_uid", (object) this.data.member.uid, (object) "room_id", (object) this.data.member.roomId), (System.Action<bool, object>) ((result, orgSrc) =>
      {
        if (!result)
          return;
        this.panel.SilenceTitle.text = Utils.XLAT("chat_uppercase_unsilence_button");
        UIManager.inst.toast.Show(Utils.XLAT("toast_chat_room_block_success"), (System.Action) null, 4f, false);
        ChatMessageManager.SendChatMemberMessage(this.data.member.userName, "chat_room_silence_notice", DBManager.inst.DB_room.Get(this.data.member.roomId).channelId);
      }), true);
    else
      MessageHub.inst.GetPortByAction("Chat:unUserChat").SendRequest(Utils.Hash((object) "type", (object) "silence", (object) "be_uid", (object) this.data.member.uid, (object) "room_id", (object) this.data.member.roomId), (System.Action<bool, object>) ((result, orgSrc) =>
      {
        if (!result)
          return;
        this.panel.SilenceTitle.text = Utils.XLAT("chat_uppercase_silence_button");
      }), true);
  }

  public void OnKickClick()
  {
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = ScriptLocalization.Get("id_uppercase_warning", true),
      content = ScriptLocalization.Get("chat_room_kick_confirm_description", true),
      yes = ScriptLocalization.Get("id_uppercase_yes", true),
      no = ScriptLocalization.Get("id_uppercase_no", true),
      yesCallback = (System.Action) (() =>
      {
        string kickedUserName = this.data.member.userName;
        MessageHub.inst.GetPortByAction("Chat:kickUser").SendRequest(Utils.Hash((object) "kick_uid", (object) this.data.member.uid, (object) "room_id", (object) this.data.member.roomId), (System.Action<bool, object>) ((arg1, arg2) =>
        {
          if (!arg1)
            return;
          UIManager.inst.toast.Show(Utils.XLAT("toast_chat_room_kick_success"), (System.Action) null, 4f, false);
          ChatMessageManager.SendChatMemberMessage(kickedUserName, "chat_room_kick_notice", DBManager.inst.DB_room.Get(this.data.member.roomId).channelId);
          UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
        }), true);
      }),
      noCallback = (System.Action) (() => {})
    });
  }

  public void OnBanClick()
  {
    if (this.data.member.title != ChatRoomMember.Title.blackList)
      UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
      {
        title = ScriptLocalization.Get("id_uppercase_warning", true),
        content = ScriptLocalization.Get("chat_room_block_confirm_description", true),
        yes = ScriptLocalization.Get("id_uppercase_yes", true),
        no = ScriptLocalization.Get("id_uppercase_no", true),
        yesCallback = (System.Action) (() => MessageHub.inst.GetPortByAction("Chat:banUser").SendRequest(Utils.Hash((object) "ban_uid", (object) this.data.member.uid, (object) "room_id", (object) this.data.member.roomId), (System.Action<bool, object>) ((arg1, arg2) =>
        {
          if (!arg1)
            return;
          ChatMessageManager.SendChatMemberMessage(this.data.member.userName, "chat_room_block_notice", DBManager.inst.DB_room.Get(this.data.member.roomId).channelId);
          UIManager.inst.toast.Show(Utils.XLAT("toast_chat_room_ban_success"), (System.Action) null, 4f, false);
          UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
        }), true)),
        noCallback = (System.Action) (() => {})
      });
    else
      MessageHub.inst.GetPortByAction("Chat:unUserChat").SendRequest(Utils.Hash((object) "type", (object) "ban", (object) "be_uid", (object) this.data.member.uid, (object) "room_id", (object) this.data.member.roomId), (System.Action<bool, object>) ((arg1, arg2) =>
      {
        if (!arg1)
          return;
        UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      }), true);
  }

  public void OnPromoteClick()
  {
    string promotedUserName = this.data.member.userName;
    MessageHub.inst.GetPortByAction("Chat:promote").SendRequest(Utils.Hash((object) "cmd", (object) "promote", (object) "be_uid", (object) this.data.member.uid, (object) "room_id", (object) this.data.member.roomId), (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      UIManager.inst.toast.Show(Utils.XLAT("toast_chat_room_promotion_success"), (System.Action) null, 4f, false);
      ChatMessageManager.SendChatMemberMessage(promotedUserName, "chat_room_promotion_notice", DBManager.inst.DB_room.Get(this.data.member.roomId).channelId);
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    }), true);
  }

  public void OnDemoteClick()
  {
    if (this.data.member.title != ChatRoomMember.Title.blackList)
    {
      string deomotedUserName = this.data.member.userName;
      MessageHub.inst.GetPortByAction("Chat:promote").SendRequest(Utils.Hash((object) "cmd", (object) "demote", (object) "be_uid", (object) this.data.member.uid, (object) "room_id", (object) this.data.member.roomId), (System.Action<bool, object>) ((arg1, arg2) =>
      {
        if (!arg1)
          return;
        UIManager.inst.toast.Show(Utils.XLAT("toast_chat_room_demotion_success"), (System.Action) null, 4f, false);
        ChatMessageManager.SendChatMemberMessage(deomotedUserName, "chat_room_demotion_notice", DBManager.inst.DB_room.Get(this.data.member.roomId).channelId);
        UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      }), true);
    }
    else
      MessageHub.inst.GetPortByAction("Chat:unUserChat").SendRequest(Utils.Hash((object) "type", (object) "ban", (object) "be_uid", (object) this.data.member.uid, (object) "room_id", (object) this.data.member.roomId), (System.Action<bool, object>) null, true);
  }

  public void Reset()
  {
    this.panel.onlineState = this.transform.Find("OnlineStateIcon").gameObject.GetComponent<UISprite>();
    this.panel.BT_Player = this.transform.Find("PlayerFrame").gameObject.GetComponent<UIButton>();
    this.panel.playerIcon = this.transform.Find("PlayerFrame/PlayerIcon").gameObject.GetComponent<UITexture>();
    this.panel.playerName = this.transform.Find("PlayerName").gameObject.GetComponent<UILabel>();
    this.panel.BT_Silence = this.transform.Find("Btn_Silence").gameObject.GetComponent<UIButton>();
    this.panel.SilenceTitle = this.transform.Find("Btn_Silence/Label").gameObject.GetComponent<UILabel>();
    this.panel.BT_Kick = this.transform.Find("Btn_Kick").gameObject.GetComponent<UIButton>();
    this.panel.BT_Ban = this.transform.Find("Btn_Ban").gameObject.GetComponent<UIButton>();
    this.panel.BanTitle = this.transform.Find("Btn_Ban/Label").gameObject.GetComponent<UILabel>();
    this.panel.BT_Promote = this.transform.Find("Btn_Promote").gameObject.GetComponent<UIButton>();
    this.panel.BT_Demote = this.transform.Find("Btn_Demote").gameObject.GetComponent<UIButton>();
  }

  public class Data
  {
    public ChatRoomMember member;
    public ChatRoomMember.Title selfTitle;

    public void SetData(ChatRoomMemberBar.Parameter param)
    {
      this.member = param.member;
      this.selfTitle = param.selfTitle;
    }
  }

  public class Parameter
  {
    public ChatRoomMember member;
    public ChatRoomMember.Title selfTitle;
  }

  [Serializable]
  public class Panel
  {
    public UISprite onlineState;
    public UITexture playerIcon;
    public UILabel playerName;
    public UIButton BT_Player;
    public UILabel SilenceTitle;
    public UILabel BanTitle;
    public UIButton BT_Silence;
    public UIButton BT_Kick;
    public UIButton BT_Ban;
    public UIButton BT_Promote;
    public UIButton BT_Demote;
    public UIButton BT_Info;
  }
}
