﻿// Decompiled with JetBrains decompiler
// Type: AllianceDiplomacyInviteItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;
using UnityEngine;

public class AllianceDiplomacyInviteItem : MonoBehaviour
{
  private int _allianceID = -1;
  public UISprite mAllianceIcon;
  public UILabel mAllianceName;
  public UILabel mAllianceMemeberCount;
  public UILabel mAllianceGiftLevel;
  public UILabel mAlliancePower;
  public UISprite mRelationIcon;
  public GameObject mAcceptBtn;
  public GameObject mDenyBtn;
  public AllianceDiplomacyInvitePopup mAcceptInvitePopup;
  public AllianceDiplomacyInvitePopup mDenyInvitePopup;
  public AllianceDiplomacyInvitePopup mRevokeInvitePopup;
  private System.Action<int, AllianceDiplomacyInviteItem.DiplomacyInviteAction> onActionSelected;

  public void SetDetails(bool bSent, int ID, string iconName, string allianceName, string allianceTag, string memberCount, string maxMemberCount, string giftLevel, string power, string level, float levelPercent, string diplomacy, System.Action<int, AllianceDiplomacyInviteItem.DiplomacyInviteAction> onActionSelected)
  {
    this._allianceID = ID;
    this.mAllianceIcon.spriteName = iconName;
    this.mAllianceName.text = string.Format("({0}) {1}", (object) allianceTag, (object) allianceName);
    this.mAllianceMemeberCount.text = Utils.XLAT("MEMBERS") + ": " + memberCount;
    this.mAllianceGiftLevel.text = Utils.XLAT("GIFT LEVEL") + ": " + giftLevel;
    this.mAlliancePower.text = power;
    this.mRelationIcon.spriteName = diplomacy;
    if (bSent)
    {
      this.mAcceptBtn.SetActive(false);
      this.mDenyBtn.SetActive(false);
    }
    else
    {
      this.mAcceptBtn.SetActive(true);
      this.mDenyBtn.SetActive(true);
    }
    this.onActionSelected = onActionSelected;
    if (DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId).members.Get(PlayerData.inst.uid).title == 6)
      return;
    this.mAcceptBtn.SetActive(false);
    this.mDenyBtn.SetActive(false);
  }

  public void OnAskAccept()
  {
    this.mAcceptInvitePopup.SetDetails(this.mAllianceIcon.spriteName, this.mAllianceName.text, new System.Action(this.OnAcceptBtnPressed), new System.Action(this.CancelPopup), (System.Action) null);
    this.mAcceptInvitePopup.gameObject.SetActive(true);
  }

  public void OnAskDeny()
  {
    this.mDenyInvitePopup.SetDetails(this.mAllianceIcon.spriteName, this.mAllianceName.text, new System.Action(this.OnDenyBtnPressed), new System.Action(this.CancelPopup), (System.Action) null);
    this.mDenyInvitePopup.gameObject.SetActive(true);
  }

  public void OnAskDenyEnemy()
  {
  }

  public void OnAskRevoke()
  {
    this.mRevokeInvitePopup.SetDetails(this.mAllianceIcon.spriteName, this.mAllianceName.text, new System.Action(this.OnRevokeBtnPressed), new System.Action(this.CancelPopup), (System.Action) null);
    this.mRevokeInvitePopup.gameObject.SetActive(true);
  }

  public void OnMsgLeader()
  {
  }

  public void OnInfo()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) this._allianceID;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("Alliance/AllianceJoinAndApplyDialog", (UI.Dialog.DialogParameter) new AllianceJoinAndApplyDialog.Parameter()
      {
        allianceData = DBManager.inst.DB_Alliance.Get((long) this._allianceID),
        justView = (PlayerData.inst.allianceId > 0L)
      }, true, true, true);
    }), true);
  }

  public void OnAcceptBtnPressed()
  {
    this.CancelPopup();
    if (this.onActionSelected == null)
      return;
    this.onActionSelected(this._allianceID, AllianceDiplomacyInviteItem.DiplomacyInviteAction.ACCEPT);
  }

  public void OnDenyBtnPressed()
  {
    this.CancelPopup();
    if (this.onActionSelected == null)
      return;
    this.onActionSelected(this._allianceID, AllianceDiplomacyInviteItem.DiplomacyInviteAction.DENY);
  }

  public void OnRevokeBtnPressed()
  {
    this.CancelPopup();
    if (this.onActionSelected == null)
      return;
    this.onActionSelected(this._allianceID, AllianceDiplomacyInviteItem.DiplomacyInviteAction.REVOKE);
  }

  public void CancelPopup()
  {
    this.mAcceptInvitePopup.gameObject.SetActive(false);
    this.mDenyInvitePopup.gameObject.SetActive(false);
    this.mRevokeInvitePopup.gameObject.SetActive(false);
  }

  public enum DiplomacyInviteAction
  {
    ACCEPT,
    DENY,
    MAKE_ENEMY,
    REVOKE,
  }
}
