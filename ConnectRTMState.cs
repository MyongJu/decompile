﻿// Decompiled with JetBrains decompiler
// Type: ConnectRTMState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class ConnectRTMState : LoadBaseState
{
  private int taskId = -1;
  private float time;
  private bool pasue;
  private int maxTime;

  public ConnectRTMState(int step)
    : base(step)
  {
    this.NeedLoad = false;
  }

  protected override SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.ConnectRTM;
    }
  }

  public override string Key
  {
    get
    {
      return nameof (ConnectRTMState);
    }
  }

  protected override void Prepare()
  {
    this.RefreshProgress();
    NetWorkDetector.Instance.StopMonitor();
    this.InitRTM();
    this.maxTime = NetApi.inst.ConnectTimout * 2;
    this.time = Time.time;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnProcessHandler);
  }

  protected override void OnDispose()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnProcessHandler);
  }

  private void OnProcessHandler(int preTime)
  {
    if (this.pasue)
      return;
    if (NetWorkDetector.Instance.ConnectionInited)
    {
      if (NetWorkDetector.Instance.IsConnectionAvailable)
        this.Finish();
      else if (NetApi.inst.RTM_Status == NetApi.RTMSTATE.AuthFail)
      {
        this.pasue = true;
        if (Oscillator.IsAvailable)
          Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnProcessHandler);
        string title = ScriptLocalization.Get("data_error_title", true);
        string tip = ScriptLocalization.Get("data_error_initialization3_description", true);
        if (!NetWorkDetector.Instance.IsReadyRestart())
          NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.RTM, "AuthFail", "token=" + NetApi.inst.RtmToken);
        NetWorkDetector.Instance.RestartOrQuitGame(tip, title, (string) null);
      }
      else
        this.NormalFail();
    }
    else
    {
      if ((double) Time.time - (double) this.time <= (double) this.maxTime)
        return;
      this.time = Time.time;
      this.NormalFail();
    }
  }

  private void NormalFail()
  {
    NetWorkDetector.Instance.ResetConnection();
    if (this.taskId < 0)
    {
      this.taskId = TaskManager.Instance.Execute((TaskManager.ITask) new AutoRetryTask()
      {
        RetryMaxTime = 60,
        ExecuteHandler = new System.Action(this.Connect),
        IntervalTime = 3f
      });
    }
    else
    {
      TaskManager.Instance.SetAutoRetryTask(this.taskId, false);
      if (!TaskManager.Instance.IsFinish(this.taskId))
        return;
      this.taskId = -1;
      this.IsFail = true;
      this.CanRetry = true;
      this.pasue = true;
      this.ShowRetry();
    }
  }

  private void InitRTM()
  {
    if (!GameEngine.Instance.ChatEnabled)
      return;
    GameEngine.Instance.ChatManager.Init();
    GameEngine.Instance.ChatManager.Connect();
  }

  private void Connect()
  {
    this.time = Time.time;
    if (string.IsNullOrEmpty(NetApi.inst.RTM_URL))
      GameEngine.Instance.ChatManager.Connect();
    else
      GameEngine.Instance.ChatManager.ReConnect();
  }

  protected override void Retry()
  {
    this.pasue = false;
    this.Connect();
  }

  protected override void Finish()
  {
    if (this.IsDestroy)
      return;
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnProcessHandler);
    this.pasue = true;
    this.taskId = -1;
    TaskManager.Instance.SetAutoRetryTask(this.taskId, true);
    base.Finish();
    NetWorkDetector.Instance.StartMonitor();
    this.Preloader.OnConnectRTMFinish();
  }
}
