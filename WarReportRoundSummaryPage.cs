﻿// Decompiled with JetBrains decompiler
// Type: WarReportRoundSummaryPage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarReportRoundSummaryPage : MonoBehaviour, WarReportDetailPage.IReportSubPage
{
  private List<WarReportRoundEntry> _roundEntries = new List<WarReportRoundEntry>();
  public UITable warReportTable;
  public WarReportRoundEntry roundTemplate;
  public GameObject noBattleReportMessage;
  public UIScrollView scrollView;
  public UIPanel panel;
  public EnvelopContent contentContainer;
  public GameObject layoutBackground;
  private Hashtable _data;
  private char _currentChar;

  public void SetData(Hashtable data)
  {
    this._data = data;
    this.Clear();
    Hashtable hashtable = this._data[(object) "battle_log"] as Hashtable;
    if (hashtable == null)
    {
      this.layoutBackground.SetActive(false);
      this.noBattleReportMessage.SetActive(true);
    }
    else
    {
      this.layoutBackground.SetActive(true);
      this.noBattleReportMessage.SetActive(false);
      long uid = PlayerData.inst.uid;
      IEnumerator enumerator = hashtable.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        int round = int.Parse(enumerator.Current.ToString());
        WarReportRoundEntry component = NGUITools.AddChild(this.warReportTable.gameObject, this.roundTemplate.gameObject).GetComponent<WarReportRoundEntry>();
        component.gameObject.transform.parent = this.warReportTable.gameObject.transform;
        component.OnPosition += new System.Action<WarReportRoundEntry>(this.OnEntryPosition);
        component.SetData(hashtable[enumerator.Current] as Hashtable, uid, round);
        component.name = this._currentChar.ToString();
        ++this._currentChar;
        component.gameObject.SetActive(true);
        this._roundEntries.Add(component);
      }
    }
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
    this.scrollView.ResetPosition();
    this.panel.alpha = 1f;
  }

  public void Hide()
  {
    this.panel.alpha = 0.01f;
  }

  private void OnEntryPosition(WarReportRoundEntry entry)
  {
    entry.OnPosition -= new System.Action<WarReportRoundEntry>(this.OnEntryPosition);
    bool flag = true;
    List<WarReportRoundEntry>.Enumerator enumerator = this._roundEntries.GetEnumerator();
    while (enumerator.MoveNext())
      flag &= enumerator.Current.positioned;
    if (!flag)
      return;
    this.warReportTable.onReposition += new UITable.OnReposition(this.OnReposition);
    this.warReportTable.repositionNow = true;
  }

  private void OnReposition()
  {
    this.warReportTable.onReposition -= new UITable.OnReposition(this.OnReposition);
    this.warReportTable.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
    this.contentContainer.Execute();
    this.scrollView.ResetPosition();
    this.panel.alpha = 1f;
  }

  private void Clear()
  {
    this._currentChar = 'a';
    List<WarReportRoundEntry>.Enumerator enumerator = this._roundEntries.GetEnumerator();
    while (enumerator.MoveNext())
      UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    this._roundEntries.Clear();
  }
}
