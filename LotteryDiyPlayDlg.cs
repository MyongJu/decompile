﻿// Decompiled with JetBrains decompiler
// Type: LotteryDiyPlayDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class LotteryDiyPlayDlg : UI.Dialog
{
  private Dictionary<int, LotteryDiyItemRendererEx> _itemDict = new Dictionary<int, LotteryDiyItemRendererEx>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private bool _specialShowFinished = true;
  private const float SPECIAL_ANIM_SHOW_DELAY_TIME = 0.1f;
  private const string SPECIAL_MARK_ANIMATION = "fx_SurpriseItem_ain";
  private const string SPECIAL_MARK_RESET_ANIMATION = "fx_SurpriseItem_reset";
  public UILabel panelTitle;
  public GameObject freeBadgeNode;
  public UITexture panelBkgFrame;
  public UITexture panelNpcTexture;
  public GameObject itemTipsNode;
  public UILabel itemName;
  public UILabel itemDetail;
  public GameObject specialPropsHitEffect;
  public UnityEngine.Animation specialPropsAnim;
  public GameObject specialQuestionMarkNode;
  public UITexture rewardPropsTexture;
  public UILabel rewardPropsCountText;
  public UITexture currentPropsTexture;
  public UILabel currentPropsCountText;
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  private int _rewardItemId;
  private int _rewardItemCount;
  private int _rewardPropsCount;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.AddEventHandler();
    this.ResetSpecialAnim();
    this.StopSpecialAnim();
    this.ShowMainContent();
    this.ShowSpecial();
    this.ShowProps();
    this.ShowLotteryContent();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ResetSpecialAnim();
    this.HideTips();
    this.RemoveEventHandler();
    this.ClearData();
  }

  public void OnRewardHistoryBtnPressed()
  {
    UIManager.inst.OpenPopup("LotteryDiy/LotteryDiyHistoryPopup", (Popup.PopupParameter) null);
  }

  public void OnFreeRewardBtnPressed()
  {
    LotteryDiyPayload.Instance.RequestServerData((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenPopup("LotteryDiy/LotteryDiyFreeRewardPopup", (Popup.PopupParameter) new LotteryDiyFreeRewardPopup.Parameter()
      {
        freeClaimedHandler = new System.Action(this.ShowMainContent)
      });
    }));
  }

  public void OnSpecialFramePressed()
  {
    UIManager.inst.toast.Show(Utils.XLAT("toast_event_lucky_draw_auto_start"), (System.Action) null, 4f, true);
  }

  private void ShowMainContent()
  {
    this.panelTitle.text = Utils.XLAT(LotteryDiyPayload.Instance.LotteryData.Title);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panelBkgFrame, "Texture/STATIC_TEXTURE/bg_diy_lottery", (System.Action<bool>) null, false, true, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panelNpcTexture, "Texture/STATIC_TEXTURE/tutorial_npc_04", (System.Action<bool>) null, false, true, string.Empty);
    NGUITools.SetActive(this.freeBadgeNode, LotteryDiyPayload.Instance.LotteryData.HasFreeReward);
  }

  private void ShowProps()
  {
    this.OnItemDataChanged(LotteryDiyPayload.Instance.LotteryData.SpendItem);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(LotteryDiyPayload.Instance.LotteryData.SpendItem);
    if (itemStaticInfo == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.currentPropsTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
  }

  private void OnItemDataChanged(int itemId)
  {
    if (itemId != LotteryDiyPayload.Instance.LotteryData.SpendItem)
      return;
    this.ShowRemainProps();
    this.CheckEachLotteryCard();
  }

  private void ShowRemainProps()
  {
    this.currentPropsCountText.text = Utils.FormatThousands(LotteryDiyPayload.Instance.LotteryData.SpendItemCount.ToString());
  }

  private void ShowLotteryContent()
  {
    this.ClearData();
    this.CheckEachLotteryCard();
    Dictionary<int, LotteryDiyBaseData.RewardData> lotteryPrevItemDict = LotteryDiyPayload.Instance.LotteryData.LotteryPrevItemDict;
    if (lotteryPrevItemDict != null)
    {
      for (int index = 1; index <= lotteryPrevItemDict.Count; ++index)
      {
        if (lotteryPrevItemDict.ContainsKey(index))
        {
          LotteryDiyItemRendererEx itemRenderer = this.CreateItemRenderer(lotteryPrevItemDict[index], LotteryDiyPayload.LotteryArea.RIGHT, index);
          this._itemDict.Add(index, itemRenderer);
        }
      }
    }
    this.Reposition();
  }

  private void OnItemTipsDisplay(LotteryDiyBaseData.RewardData rewardData, LotteryDiyPayload.LotteryArea lotteryArea, Vector3 itemPosition)
  {
    if (rewardData == null)
      return;
    this.StopCoroutine("HideItemTips");
    this.UpdateItemTipsPosition(lotteryArea, itemPosition);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardData.itemId);
    if (itemStaticInfo != null)
    {
      this.itemName.text = itemStaticInfo.LocName;
      this.itemDetail.text = itemStaticInfo.LocDescription;
      NGUITools.SetActive(this.itemTipsNode, true);
    }
    this.StartCoroutine("HideItemTips");
  }

  private void UpdateItemTipsPosition(LotteryDiyPayload.LotteryArea lotteryArea, Vector3 itemPosition)
  {
    float num1 = -420f;
    float num2 = 500f;
    this.itemTipsNode.transform.localPosition = new Vector3(itemPosition.x + num1, itemPosition.y + num2, itemPosition.z);
  }

  [DebuggerHidden]
  private IEnumerator HideItemTips()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LotteryDiyPlayDlg.\u003CHideItemTips\u003Ec__Iterator7F()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void HideTips()
  {
    NGUITools.SetActive(this.itemTipsNode, false);
  }

  private void OnLotteryCardPicked(int rewardItemId, int rewardItemCount, int rewardPropsCount)
  {
    this._rewardItemId = rewardItemId;
    this._rewardItemCount = rewardItemCount;
    this._rewardPropsCount = rewardPropsCount;
    this.PlayPickedSound();
    this.ShowRewardReceivedEffect();
    this.ShowSpecial();
    this.CheckEachLotteryCard();
    this.CheckLotteryPickFinished(this._rewardPropsCount > 0);
  }

  private void OnLotterCardTouched()
  {
    this.HideTips();
    if (!this._specialShowFinished)
      return;
    this.OnLotteryCardPicked(0, 0, 0);
  }

  private void CheckEachLotteryCard()
  {
    int needPropsCount2Play = LotteryDiyPayload.Instance.NeedPropsCount2Play;
    if (this._itemDict == null)
      return;
    Dictionary<int, LotteryDiyItemRendererEx>.ValueCollection.Enumerator enumerator = this._itemDict.Values.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.UpdatePropsTips(needPropsCount2Play);
  }

  private void CheckLotteryPickFinished(bool isSpecialWin)
  {
    if (!LotteryDiyPayload.Instance.LotteryData.IsPrevLotteryFinished)
      return;
    if (isSpecialWin)
    {
      float num = 0.0f;
      if ((UnityEngine.Object) this.specialPropsAnim != (UnityEngine.Object) null)
        num = this.specialPropsAnim.clip.length;
      this.StartCoroutine(this.ConfirmFinished(0.1f + ((double) num >= 2.5 ? num : 2.5f)));
    }
    else
      this.StartCoroutine(this.ConfirmFinished(2.6f));
  }

  [DebuggerHidden]
  private IEnumerator ConfirmFinished(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LotteryDiyPlayDlg.\u003CConfirmFinished\u003Ec__Iterator80()
    {
      delay = delay,
      \u003C\u0024\u003Edelay = delay,
      \u003C\u003Ef__this = this
    };
  }

  private void PickLottery()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    LotteryDiyPayload.Instance.ShowFirstLotteryScene();
  }

  private void PlayPickedSound()
  {
    AudioManager.Instance.StopAndPlaySound("sfx_city_casino_turn");
  }

  private void ShowSpecial()
  {
    if (this._rewardPropsCount > 0)
    {
      this.StartCoroutine("ShowSpecialPropsEffect");
    }
    else
    {
      this.StopCoroutine("ShowSpecialPropsEffect");
      this.ResetSpecialAnim();
      NGUITools.SetActive(this.specialQuestionMarkNode, true);
      NGUITools.SetActive(this.rewardPropsTexture.gameObject, false);
      NGUITools.SetActive(this.rewardPropsCountText.transform.parent.gameObject, false);
    }
  }

  private void StopSpecialAnim()
  {
    if (!((UnityEngine.Object) this.specialPropsAnim != (UnityEngine.Object) null))
      return;
    this.specialPropsAnim.Stop();
  }

  private void ResetSpecialAnim()
  {
    if ((UnityEngine.Object) this.specialPropsAnim != (UnityEngine.Object) null)
    {
      if ((TrackedReference) this.specialPropsAnim["fx_SurpriseItem_ain"] != (TrackedReference) null)
      {
        this.specialPropsAnim.Stop();
        this.specialPropsAnim["fx_SurpriseItem_ain"].time = 0.0f;
      }
      this.specialPropsAnim.Play("fx_SurpriseItem_reset");
    }
    CasinoFunHelper.Instance.StopParticles(this.specialPropsHitEffect);
  }

  [DebuggerHidden]
  private IEnumerator ShowSpecialPropsEffect()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LotteryDiyPlayDlg.\u003CShowSpecialPropsEffect\u003Ec__Iterator81()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void NotifySpecialFinished(bool specialShowFinished)
  {
    this._specialShowFinished = specialShowFinished;
    if (this._itemDict == null)
      return;
    Dictionary<int, LotteryDiyItemRendererEx>.ValueCollection.Enumerator enumerator = this._itemDict.Values.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.SpecialShowFinished = specialShowFinished;
  }

  private void ShowRewardReceivedEffect()
  {
    if (this._rewardItemCount <= 0)
      return;
    RewardsCollectionAnimator.Instance.Clear();
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._rewardItemId);
    if (itemStaticInfo != null)
      RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
      {
        icon = itemStaticInfo.ImagePath,
        count = (float) this._rewardItemCount
      });
    RewardsCollectionAnimator.Instance.CollectItems(false);
  }

  private void ClearData()
  {
    using (Dictionary<int, LotteryDiyItemRendererEx>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, LotteryDiyItemRendererEx> current = enumerator.Current;
        current.Value.onLotteryCardTouched -= new System.Action(this.OnLotterCardTouched);
        current.Value.onLotteryCardPicked -= new System.Action<int, int, int>(this.OnLotteryCardPicked);
        LotteryDiyItemRendererEx diyItemRendererEx = current.Value;
        diyItemRendererEx.onItemTipsDisplay = diyItemRendererEx.onItemTipsDisplay - new System.Action<LotteryDiyBaseData.RewardData, LotteryDiyPayload.LotteryArea, Vector3>(this.OnItemTipsDisplay);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
    this._rewardItemId = 0;
    this._rewardItemCount = 0;
    this._rewardPropsCount = 0;
    this._specialShowFinished = true;
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private LotteryDiyItemRendererEx CreateItemRenderer(LotteryDiyBaseData.RewardData rewardData, LotteryDiyPayload.LotteryArea lotteryArea, int index = -1)
  {
    GameObject gameObject = this._itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    LotteryDiyItemRendererEx component = gameObject.GetComponent<LotteryDiyItemRendererEx>();
    component.SetData(rewardData, lotteryArea, index, false);
    component.onLotteryCardTouched += new System.Action(this.OnLotterCardTouched);
    component.onLotteryCardPicked += new System.Action<int, int, int>(this.OnLotteryCardPicked);
    LotteryDiyItemRendererEx diyItemRendererEx = component;
    diyItemRendererEx.onItemTipsDisplay = diyItemRendererEx.onItemTipsDisplay + new System.Action<LotteryDiyBaseData.RewardData, LotteryDiyPayload.LotteryArea, Vector3>(this.OnItemTipsDisplay);
    return component;
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemDataChanged);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataChanged);
  }
}
