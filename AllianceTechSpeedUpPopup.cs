﻿// Decompiled with JetBrains decompiler
// Type: AllianceTechSpeedUpPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class AllianceTechSpeedUpPopup : Popup
{
  [SerializeField]
  private UISlider _researchSlider;
  [SerializeField]
  private UILabel _reserchTime;
  [SerializeField]
  private UILabel _fundValue;
  [SerializeField]
  private UILabel _speedUpTime;
  [SerializeField]
  private UIButton _confirmButton;
  [SerializeField]
  private AllianceTechSpeedUpProgressBar _progressBar;
  private AllianceTechSpeedUpLogic mLogic;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    int researchId = (orgParam as AllianceTechSpeedUpPopup.Parameter).researchId;
    this.mLogic = AllianceTechSpeedUpLogic.Instance;
    this.mLogic.Init(this, researchId);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.mLogic.Release();
    this.mLogic = (AllianceTechSpeedUpLogic) null;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.InitViews();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnCloseButtonClicked()
  {
    this.ClosePopup();
  }

  public void OnSpeedUpConfirmClicked()
  {
    this.mLogic.ReqeustSpeedup();
  }

  private void InitViews()
  {
    this._progressBar.onValueChanged = new System.Action<int>(this.OnSelectCountChanged);
    this._progressBar.Init(this.mLogic.RecommendCount, this.mLogic.RecommendCount);
    this.UpdateViews();
  }

  public void ResetProgressBar()
  {
    this._progressBar.Init(this.mLogic.RecommendCount, this.mLogic.RecommendCount);
  }

  public void FixProgressBar(int curCount)
  {
    this._progressBar.Init(curCount, this.mLogic.RecommendCount);
  }

  private void OnSelectCountChanged(int count)
  {
    this.mLogic.SelectedCount = count;
  }

  public void UpdateViews()
  {
    this.UpdateResearchSlider();
    this._fundValue.text = string.Format("{0}/{1}", (object) this.mLogic.FundSelected, (object) this.mLogic.FundTotal);
    this._speedUpTime.text = Utils.FormatTime1(this.mLogic.SpeedUpTime);
    if (this.mLogic.RecommendCount > 0)
      this._fundValue.color = Color.white;
    else
      this._fundValue.color = Color.red;
    this._confirmButton.isEnabled = this.mLogic.FundSelected > 0L;
  }

  public void UpdateResearchSlider()
  {
    JobHandle job = JobManager.Instance.GetJob(this.mLogic.JobId);
    if (job == null)
      return;
    this._reserchTime.text = Utils.FormatTime(job.LeftTime() <= 0 ? 0 : job.LeftTime(), false, false, true);
    this._researchSlider.value = Mathf.Clamp01((float) (1.0 - (double) job.LeftTime() / (double) job.Duration()));
  }

  public void ClosePopup()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int researchId;
  }
}
