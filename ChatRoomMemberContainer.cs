﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomMemberContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ChatRoomMemberContainer : MonoBehaviour
{
  public static ChatRoomMemberContainer.Parameter param = new ChatRoomMemberContainer.Parameter();
  private ChatRoomMemberContainer.Data data = new ChatRoomMemberContainer.Data();
  public ChatRoomMember.Title title;
  private bool _isExpanded;
  private List<ChatRoomMember> members;
  private List<long> channelList;
  private List<long> onlineChannels;
  [SerializeField]
  private ChatRoomMemberContainer.Panel panel;

  public string titleText
  {
    set
    {
      this.panel.baseInfoTitle.text = value;
    }
  }

  public string titleIconName
  {
    set
    {
      this.panel.baseInfoIcon.spriteName = value;
    }
  }

  public bool isExpanded
  {
    get
    {
      return this._isExpanded;
    }
    set
    {
      if (value == this._isExpanded)
        return;
      this._isExpanded = value;
      this.panel.Tween_DetailScale.Play(value);
      this.panel.Tween_ExpandRotate.Play(value);
      if (this._isExpanded)
        return;
      this.GetComponentInParent<UIScrollView>().ResetPosition();
    }
  }

  public void Setup(ChatRoomMemberContainer.Parameter param)
  {
    this.data.SetData(param);
    this.data.containerTitle = this.title;
    this.Refresh();
  }

  public void Refresh()
  {
    ChatRoomData chatRoomData = DBManager.inst.DB_room.Get(this.data.roomId);
    this.isExpanded = false;
    this.members = new List<ChatRoomMember>();
    this.channelList = new List<long>();
    Dictionary<long, ChatRoomMember>.ValueCollection.Enumerator enumerator = chatRoomData.members.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.uid == PlayerData.inst.uid)
        this.data.selfTitle = enumerator.Current.title;
      if (enumerator.Current.title == this.data.containerTitle)
      {
        this.members.Add(enumerator.Current);
        this.channelList.Add(enumerator.Current.channelId);
      }
    }
    for (int count = this.panel.memberSlots.Count; count < this.members.Count; ++count)
    {
      GameObject gameObject = NGUITools.AddChild(this.panel.memberContainer.gameObject, this.panel.memberBarOrg.gameObject);
      gameObject.name = "member" + (object) (100 + count);
      gameObject.transform.localPosition = new Vector3(0.0f, (float) (count * -200 - 100), 0.0f);
      this.panel.memberSlots.Add(gameObject.GetComponent<ChatRoomMemberBar>());
    }
    for (int index = 0; index < this.members.Count; ++index)
    {
      ChatRoomMemberBar.param.selfTitle = this.data.selfTitle;
      ChatRoomMemberBar.param.member = this.members[index];
      this.panel.memberSlots[index].gameObject.SetActive(true);
      this.panel.memberSlots[index].Setup(ChatRoomMemberBar.param);
    }
    for (int count = this.members.Count; count < this.panel.memberSlots.Count; ++count)
      this.panel.memberSlots[count].gameObject.SetActive(false);
    this.panel.memberContainer.height = this.members.Count * 200;
    if (this.channelList.Count > 0)
      PushManager.inst.GetOnlineUsers(this.channelList, new System.Action<List<long>>(this.GetOnlineUsersCallback));
    this.panel.baseInfoCount.text = string.Format("([43e024]{0}[-]/{1})", (object) 0, (object) this.members.Count);
  }

  private void GetOnlineUsersCallback(List<long> result)
  {
    this.onlineChannels = result;
  }

  private void Update()
  {
    if (this.onlineChannels == null)
      return;
    this.panel.baseInfoCount.text = string.Format("([43e024]{0}[-]/{1})", (object) this.onlineChannels.Count, (object) this.members.Count);
    this.onlineChannels = (List<long>) null;
  }

  public void OnExpandClick()
  {
    this.isExpanded = !this.isExpanded;
  }

  public void Reset()
  {
    this.panel.baseInfoIcon = this.transform.Find("BaseInfoContainer/BaseInfoIcon").gameObject.GetComponent<UISprite>();
    this.panel.baseInfoTitle = this.transform.Find("BaseInfoContainer/BaseInfoTitle").gameObject.GetComponent<UILabel>();
    this.panel.baseInfoCount = this.transform.Find("BaseInfoContainer/BaseInfoCount").gameObject.GetComponent<UILabel>();
    this.panel.BT_Expand = this.transform.Find("BaseInfoContainer/Btn_expand").gameObject.GetComponent<UIButton>();
    this.panel.BT_Expand.transform.localRotation = Quaternion.identity;
    GameObject gameObject = this.transform.Find("MemberContainer").gameObject;
    this.panel.memberContainer = gameObject.GetComponent<UIWidget>();
    this.panel.memberContainer.transform.localScale = new Vector3(1f, 0.0f, 1f);
    this.panel.Tween_DetailScale = gameObject.GetComponent<TweenScale>();
    this.panel.Tween_DetailScale.updateTable = true;
    this.panel.Tween_DetailScale.from = new Vector3(1f, 0.0f, 1f);
    this.panel.Tween_DetailScale.to = new Vector3(1f, 1f, 1f);
    this.panel.Tween_DetailScale.duration = 0.1f;
    this.panel.Tween_DetailScale.enabled = false;
    this.panel.memberBarOrg = this.transform.Find("MemberContainer/MemberBarOrg").gameObject.GetComponent<ChatRoomMemberBar>();
    this.panel.memberBarOrg.Reset();
    this.panel.memberBarOrg.gameObject.SetActive(false);
    this.panel.Tween_ExpandRotate = this.transform.Find("BaseInfoContainer/Btn_expand").gameObject.GetComponent<TweenRotation>();
    this.panel.Tween_ExpandRotate.from = Vector3.zero;
    this.panel.Tween_ExpandRotate.to = new Vector3(0.0f, 0.0f, -90f);
    this.panel.Tween_ExpandRotate.duration = 0.1f;
    this.panel.Tween_ExpandRotate.enabled = false;
  }

  public class Parameter
  {
    public long roomId;
  }

  public class Data
  {
    public long roomId;
    public ChatRoomMember.Title selfTitle;
    public ChatRoomMember.Title containerTitle;
    public List<ChatRoomMember> members;

    public void SetData(ChatRoomMemberContainer.Parameter param)
    {
      this.roomId = param.roomId;
    }
  }

  [Serializable]
  public class Panel
  {
    public List<ChatRoomMemberBar> memberSlots = new List<ChatRoomMemberBar>();
    public UISprite baseInfoIcon;
    public UILabel baseInfoTitle;
    public UILabel baseInfoCount;
    public UIButton BT_Expand;
    public UIWidget memberContainer;
    public ChatRoomMemberBar memberBarOrg;
    public TweenRotation Tween_ExpandRotate;
    public TweenScale Tween_DetailScale;
  }
}
