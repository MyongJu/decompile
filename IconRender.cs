﻿// Decompiled with JetBrains decompiler
// Type: IconRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class IconRender : MonoBehaviour
{
  public UITexture iconTexture;

  public void Show(string path)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.iconTexture, path, (System.Action<bool>) null, true, false, string.Empty);
  }

  public void Hide()
  {
    BuilderFactory.Instance.Release((UIWidget) this.iconTexture);
  }
}
