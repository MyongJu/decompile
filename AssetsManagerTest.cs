﻿// Decompiled with JetBrains decompiler
// Type: AssetsManagerTest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AssetsManagerTest : MonoBehaviour
{
  public Transform root;

  private void Start()
  {
    this.TestLoad();
  }

  private void TestLoad()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad("Prefab/GUI_Prefabs/AllianceDlg", (System.Type) null) as GameObject);
    gameObject.transform.parent = this.root;
    gameObject.transform.localScale = Vector3.one;
  }
}
