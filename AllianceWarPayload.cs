﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;

public class AllianceWarPayload
{
  private readonly List<string> _claimedRewardList = new List<string>();
  private const string ALLIANCE_WAR_TRACKED_ALLIANCE_ID = "alliance_war_tracked_alliance_id";
  private static AllianceWarPayload _instance;
  private long _curOppAllianceId;
  private AllianceWarTrackData _trackData;
  private long _userScore;
  private bool _isInAllianceWarTeleport;

  public static AllianceWarPayload Instance
  {
    get
    {
      if (AllianceWarPayload._instance == null)
        AllianceWarPayload._instance = new AllianceWarPayload();
      return AllianceWarPayload._instance;
    }
  }

  public long CurOppAllianceId
  {
    get
    {
      return this._curOppAllianceId;
    }
  }

  public int AllianceWarEntryLevel
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_warfare_stronghold_lvl_min");
      if (data != null)
        return data.ValueInt;
      return 1;
    }
  }

  public AllianceWarTrackData TrackData
  {
    get
    {
      if (this._trackData == null)
        this._trackData = new AllianceWarTrackData();
      return this._trackData;
    }
  }

  public long UserScore
  {
    get
    {
      return this._userScore;
    }
  }

  public List<string> CliamedRewardList
  {
    get
    {
      return this._claimedRewardList;
    }
  }

  public void Initialize()
  {
    MessageHub.inst.GetPortByAction("alliance_warfare_over").AddEvent(new System.Action<object>(this.OnAllianceWarOverPush));
    MessageHub.inst.GetPortByAction("ac_grouping_finished").AddEvent(new System.Action<object>(this.OnAllianceWarGroupingFinished));
    MessageHub.inst.GetPortByAction("alliance_warfare_force_close").AddEvent(new System.Action<object>(this.OnAllianceWarForceClosed));
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondTick);
    this.IsInAllianceWarTeleport = false;
  }

  public void Dispose()
  {
    MessageHub.inst.GetPortByAction("alliance_warfare_over").RemoveEvent(new System.Action<object>(this.OnAllianceWarOverPush));
    MessageHub.inst.GetPortByAction("ac_grouping_finished").RemoveEvent(new System.Action<object>(this.OnAllianceWarGroupingFinished));
    MessageHub.inst.GetPortByAction("alliance_warfare_force_close").RemoveEvent(new System.Action<object>(this.OnAllianceWarForceClosed));
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondTick);
  }

  private void OnAllianceWarOverPush(object data)
  {
    GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Lite);
  }

  private void OnAllianceWarForceClosed(object data)
  {
    UserData userData = PlayerData.inst.userData;
    if (userData == null)
      return;
    userData.AcGroupId = 0;
    userData.AcAllianceId = 0L;
    userData.AcEndTime = 0;
    ActivityManager.Intance.allianceWar.FightStartTime = 0;
    ActivityManager.Intance.allianceWar.FightEndTime = 0;
    DBManager.inst.DB_User.Publish_OnDataChanged(userData.uid);
    AllianceData allianceData = PlayerData.inst.allianceData;
    if (allianceData == null)
      return;
    allianceData.AcGroupId = 0;
    allianceData.AcRegisterTime = 0;
    DBManager.inst.DB_Alliance.Publish_OnDataChanged(allianceData.allianceId);
  }

  private void OnAllianceWarGroupingFinished(object data)
  {
    Hashtable hashtable = data as Hashtable;
    if (hashtable == null || !hashtable.ContainsKey((object) "fight_start_time") || (!hashtable.ContainsKey((object) "fight_end_time") || !hashtable.ContainsKey((object) "ac_group_id")))
      return;
    int result1;
    int.TryParse(hashtable[(object) "fight_start_time"].ToString(), out result1);
    int result2;
    int.TryParse(hashtable[(object) "fight_end_time"].ToString(), out result2);
    int result3;
    int.TryParse(hashtable[(object) "ac_group_id"].ToString(), out result3);
    UserData userData = PlayerData.inst.userData;
    if (userData == null)
      return;
    userData.AcGroupId = result3;
    userData.AcAllianceId = PlayerData.inst.userData.allianceId;
    userData.AcEndTime = result2;
    ActivityManager.Intance.allianceWar.FightStartTime = result1;
    ActivityManager.Intance.allianceWar.FightEndTime = result2;
    DBManager.inst.DB_User.Publish_OnDataChanged(userData.uid);
  }

  protected void OnSecondTick(int delta)
  {
    UserData userData = PlayerData.inst.userData;
    if (userData == null || userData.AcGroupId == 0 || PlayerData.inst.userData.AcEndTime - NetServerTime.inst.ServerTimestamp > 0)
      return;
    userData.AcGroupId = 0;
    userData.AcAllianceId = 0L;
    userData.AcEndTime = 0;
    DBManager.inst.DB_User.Publish_OnDataChanged(userData.uid);
  }

  public void SetTrackedAlliance(long allianceId)
  {
    PlayerPrefsEx.SetStringByUid("alliance_war_tracked_alliance_id", allianceId.ToString());
  }

  public long GetTrackedAllianceId()
  {
    string stringByUid = PlayerPrefsEx.GetStringByUid("alliance_war_tracked_alliance_id", string.Empty);
    long result = 0;
    long.TryParse(stringByUid, out result);
    return result;
  }

  public bool IsAllianceWarStarted
  {
    get
    {
      return NetServerTime.inst.ServerTimestamp >= ActivityManager.Intance.allianceWar.FightStartTime;
    }
  }

  public void ShowAllianceJoinToast(int type)
  {
    string content = string.Empty;
    switch (type)
    {
      case 1:
        content = Utils.XLAT("toast_alliance_warfare_alliance_cannot_join");
        break;
      case 2:
        content = Utils.XLAT("toast_alliance_warfare_player_cannot_join");
        break;
    }
    if (string.IsNullOrEmpty(content))
      return;
    UIManager.inst.toast.Show(content, (System.Action) null, 4f, true);
  }

  public void ShowAllianceQuitConfirm(int type, System.Action noCallback = null, System.Action yesCallback = null)
  {
    MessageBoxWith2Buttons.Parameter parameter = new MessageBoxWith2Buttons.Parameter();
    parameter.title = Utils.XLAT("id_uppercase_confirm");
    parameter.yes = Utils.XLAT("id_uppercase_okay");
    parameter.no = Utils.XLAT("id_uppercase_cancel");
    switch (type)
    {
      case 1:
        parameter.content = Utils.XLAT("alliance_warfare_leave__alliance_description");
        break;
      case 2:
        parameter.content = Utils.XLAT("alliance_warfare_kick_alliance_description");
        break;
    }
    parameter.noCallback = noCallback;
    parameter.yesCallback = yesCallback;
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) parameter);
  }

  public void ShowAllianceTrack(long oppAllianceId)
  {
    this.LoadTrack(oppAllianceId, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenPopup("AllianceWar/AllianceWarTrackPopup", (Popup.PopupParameter) new AllianceWarTrackPopup.Parameter()
      {
        oppAllianceId = oppAllianceId
      });
    }));
  }

  public void LoadUserScoreRewardInfo(System.Action callback)
  {
    MessageHub.inst.GetPortByAction("AC:getUserScoreRewardInfo").SendLoader((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable hashtable = data as Hashtable;
      if (hashtable == null)
        return;
      if (hashtable.ContainsKey((object) "score") && hashtable[(object) "score"] != null)
        long.TryParse(hashtable[(object) "score"].ToString(), out this._userScore);
      if (hashtable.ContainsKey((object) "received") && hashtable[(object) "received"] != null)
      {
        ArrayList arrayList = hashtable[(object) "received"] as ArrayList;
        this._claimedRewardList.Clear();
        foreach (object obj in arrayList)
          this._claimedRewardList.Add(obj.ToString());
      }
      if (callback == null)
        return;
      callback();
    }), true, false);
  }

  public void LoadTrackedAlliance(System.Action<List<long>> callback = null)
  {
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    List<long> tmpTrackedAllianceIdList = new List<long>();
    MessageHub.inst.GetPortByAction("AC:loadTrackedAlliance").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret && data != null)
      {
        Hashtable hashtable = data as Hashtable;
        if (hashtable != null && hashtable.ContainsKey((object) "track_alliance_id"))
        {
          ArrayList arrayList = hashtable[(object) "track_alliance_id"] as ArrayList;
          if (arrayList != null)
          {
            for (int index = 0; index < arrayList.Count; ++index)
            {
              long result = 0;
              long.TryParse(arrayList[index].ToString(), out result);
              if (!tmpTrackedAllianceIdList.Contains(result))
                tmpTrackedAllianceIdList.Add(result);
            }
          }
        }
      }
      if (callback == null)
        return;
      callback(tmpTrackedAllianceIdList);
    }), true);
  }

  public void LoadTrack(long oppAllianceId, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("AC:loadTrack").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "opp_alliance_id",
        (object) oppAllianceId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.TrackData.Decode(data as Hashtable);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void StartTrack(long oppAllianceId, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("AC:startTrack").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "opp_alliance_id",
        (object) oppAllianceId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.TrackData.Decode(data as Hashtable);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public bool IsForeigner()
  {
    return PlayerData.inst.userData.IsForeigner;
  }

  public bool IsPlayerCurrentJoined(long uid = 0)
  {
    if (uid <= 0L)
    {
      if (PlayerData.inst.userData.AcGroupId > 0)
        return NetServerTime.inst.ServerTimestamp < PlayerData.inst.userData.AcEndTime;
      return false;
    }
    UserData userData = DBManager.inst.DB_User.Get(uid);
    if (userData != null && userData.AcGroupId > 0)
      return NetServerTime.inst.ServerTimestamp < PlayerData.inst.userData.AcEndTime;
    return false;
  }

  public bool IsAllianceCurrentJoined(long allianceId = 0)
  {
    if (allianceId <= 0L && PlayerData.inst.allianceData != null)
      return PlayerData.inst.allianceData.AcGroupId > 0;
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(allianceId);
    if (allianceData != null)
      return allianceData.AcGroupId > 0;
    return false;
  }

  public bool IsAllianceInSameGroup(long allianceId1, long allianceId2)
  {
    AllianceData allianceData1 = DBManager.inst.DB_Alliance.Get(allianceId1);
    AllianceData allianceData2 = DBManager.inst.DB_Alliance.Get(allianceId2);
    return allianceData1 != null && allianceData2 != null && allianceData1.AcGroupId == allianceData2.AcGroupId;
  }

  public bool IsPlayerInSameKingdom(long uid1, long uid2)
  {
    CityData cityData1 = DBManager.inst.DB_City.Get(uid1);
    CityData cityData2 = DBManager.inst.DB_City.Get(uid2);
    return cityData1 != null && cityData2 != null && cityData1.cityLocation.K == cityData2.cityLocation.K;
  }

  public bool IsInAllianceWarTeleport
  {
    get
    {
      return this._isInAllianceWarTeleport;
    }
    set
    {
      this._isInAllianceWarTeleport = value;
    }
  }

  public void GotoTarget(Coordinate location)
  {
    this.IsInAllianceWarTeleport = true;
    LinkerHub.Instance.LHGotoTile(new LPTile()
    {
      k = location.K,
      x = location.X,
      y = location.Y
    });
    Utils.ExecuteInSecs(0.5f, (System.Action) (() => Utils.RefreshBlock(location, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      if ((UnityEngine.Object) PVPSystem.Instance.Controller == (UnityEngine.Object) null)
      {
        PVPMapData.Initialize();
        PVPSystem.Instance.Setup(PVPMapData.MapData);
      }
      PVPSystem.Instance.Controller.AllianceWarTeleport(location);
    }))));
    UIManager.inst.tileCamera.StartAutoZoom();
    UIManager.inst.publicHUD.HideHUD();
  }

  public void GotoBack()
  {
    if (!this.CheckTeleport())
      return;
    if ((UnityEngine.Object) PVPSystem.Instance.Controller == (UnityEngine.Object) null)
    {
      PVPMapData.Initialize();
      PVPSystem.Instance.Setup(PVPMapData.MapData);
    }
    PVPSystem.Instance.Controller.AllianceWarBackTeleport();
  }

  public void GetInvadePosition(long uid, long oppAllianceId, System.Action<bool, Coordinate> callback)
  {
    this._curOppAllianceId = oppAllianceId;
    MessageHub.inst.GetPortByAction("AC:getPositionForInvade").SendRequest(new Hashtable()
    {
      {
        (object) nameof (uid),
        (object) uid
      },
      {
        (object) "opp_alliance_id",
        (object) oppAllianceId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      int num;
      int outData1 = num = -1;
      int outData2 = num;
      int outData3 = num;
      if (ret)
      {
        Hashtable inData = data as Hashtable;
        if (inData != null)
        {
          DatabaseTools.UpdateData(inData, "world_id", ref outData3);
          DatabaseTools.UpdateData(inData, "map_x", ref outData2);
          DatabaseTools.UpdateData(inData, "map_y", ref outData1);
          if (outData3 > 0 && this.CheckTeleport() && callback != null)
            callback(ret, new Coordinate(outData3, outData2, outData1));
        }
      }
      if (outData3 > 0)
        return;
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_kingdom_no_opponents"), (System.Action) null, 4f, true);
    }), true);
  }

  public bool CheckTeleport()
  {
    if (PlayerData.inst.CityData.mStronghold.mLevel < this.AllianceWarEntryLevel)
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_alliance_level_kingdom_teleport", new Dictionary<string, string>()
      {
        {
          "0",
          this.AllianceWarEntryLevel.ToString()
        }
      }, true), (System.Action) null, 4f, true);
    else if (Utils.IsSomeoneAttackMe())
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_fortress_marching"), (System.Action) null, 4f, true);
    else if (Utils.IsMarchInCurrentKingdom())
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_troops_kingdom"), (System.Action) null, 4f, true);
    }
    else
    {
      if (!Utils.IsHelpedMarchInEmbassy())
        return true;
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_embassy_reinforcements"), (System.Action) null, 4f, true);
    }
    return false;
  }

  public void Enter(long uid, long oppAllianceId, int x, int y, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("AC:enter").SendRequest(new Hashtable()
    {
      {
        (object) nameof (uid),
        (object) uid
      },
      {
        (object) "opp_alliance_id",
        (object) oppAllianceId
      },
      {
        (object) nameof (x),
        (object) x
      },
      {
        (object) nameof (y),
        (object) y
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void Back(System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("AC:back").SendRequest((Hashtable) null, callback, true);
  }

  public void PlaceCity(long uid, int x, int y, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("AC:place").SendRequest(new Hashtable()
    {
      {
        (object) nameof (uid),
        (object) uid
      },
      {
        (object) nameof (x),
        (object) x
      },
      {
        (object) nameof (y),
        (object) y
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public bool CanAttackTargetTile(TileData tileData)
  {
    if (tileData.Location.K != PlayerData.inst.userData.current_world_id || PlayerData.inst.userData.AcGroupId == 0)
      return false;
    TileType tileType = tileData.TileType;
    switch (tileType)
    {
      case TileType.City:
      case TileType.Camp:
      case TileType.Resource1:
      case TileType.Resource2:
      case TileType.Resource3:
      case TileType.Resource4:
        UserData userData = tileData.UserData;
        if (userData != null && userData.AcAllianceId != PlayerData.inst.userData.AcAllianceId && (AllianceWarPayload.Instance.IsPlayerCurrentJoined(userData.uid) && userData.AcGroupId == PlayerData.inst.userData.AcGroupId))
          return true;
        break;
      case TileType.AllianceFortress:
        if (tileData.AllianceID != PlayerData.inst.allianceId && AllianceWarPayload.Instance.IsAllianceInSameGroup(tileData.AllianceID, PlayerData.inst.userData.AcAllianceId))
          return true;
        break;
      default:
        if (tileType == TileType.AllianceTemple)
          goto case TileType.AllianceFortress;
        else
          break;
    }
    return false;
  }
}
