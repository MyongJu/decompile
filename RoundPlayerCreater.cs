﻿// Decompiled with JetBrains decompiler
// Type: RoundPlayerCreater
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class RoundPlayerCreater
{
  private long _playerId = 2;
  private int _idIndex = -1;

  private long CreatePlayerID(int totalCount = 1)
  {
    return this._playerId++;
  }

  private RoundPlayer CreateOneFakeData(bool isEvil = true)
  {
    RoundPlayer roundPlayer1;
    if (!isEvil)
    {
      roundPlayer1 = (RoundPlayer) new RoundPlayerMoral();
      RoundPlayer roundPlayer2 = roundPlayer1;
      int num1 = 10000;
      roundPlayer1.HealthMax = num1;
      int num2 = num1;
      roundPlayer2.Health = num2;
      roundPlayer1.Speed = 100;
      roundPlayer1.Damge = 10;
      roundPlayer1.Strong = 100;
      roundPlayer1.Defense = 100;
      roundPlayer1.MagicDamge = 100;
      roundPlayer1.Mana = 1000;
      roundPlayer1.NatureDamge = 10;
      roundPlayer1.FireDamge = 10;
      roundPlayer1.WaterDamge = 10;
      roundPlayer1.Intelligent = 50;
      roundPlayer1.Rebund = 1;
    }
    else
    {
      roundPlayer1 = (RoundPlayer) new RoundPlayerEvil();
      RoundPlayer roundPlayer2 = roundPlayer1;
      int num1 = 200 * UnityEngine.Random.Range(1, 3);
      roundPlayer1.HealthMax = num1;
      int num2 = num1;
      roundPlayer2.Health = num2;
      roundPlayer1.Speed = 50;
      roundPlayer1.Damge = 50;
      roundPlayer1.Defense = 10;
      roundPlayer1.Rebund = 1;
      roundPlayer1.PlayerId = this.CreatePlayerID(1);
    }
    roundPlayer1.Init();
    return roundPlayer1;
  }

  public List<RoundPlayer> CreateFakeData()
  {
    List<RoundPlayer> roundPlayerList = new List<RoundPlayer>();
    roundPlayerList.Add(this.CreateOneFakeData(false));
    roundPlayerList.Add(this.CreateOneFakeData(true));
    roundPlayerList.Add(this.CreateOneFakeData(true));
    roundPlayerList.Add(this.CreateOneFakeData(true));
    roundPlayerList.Sort(new Comparison<RoundPlayer>(this.Compare));
    return roundPlayerList;
  }

  public List<RoundPlayer> CreateData(int groupId)
  {
    List<RoundPlayer> roundPlayerList = new List<RoundPlayer>();
    roundPlayerList.Add(this.CreateUser(0L, true));
    DungeonMonsterGroupStaticInfo data = ConfigManager.inst.DungeonMonsterGroup.GetData(groupId);
    DragonKnightDungeonData knightDungeonData = DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L);
    DungeonMainStaticInfo dungeonMainStaticInfo = ConfigManager.inst.DB_DungeonMain.Get(knightDungeonData.LevelId);
    int factor = knightDungeonData.CurrentLevel - dungeonMainStaticInfo.Level;
    RoundPlayer monster1 = this.CreateMonster(data.MonsterID1, factor);
    int totalCount = 1;
    if (data.MonsterID2 > 0)
      ++totalCount;
    if (data.MonsterID3 > 0)
      ++totalCount;
    if (monster1 != null)
    {
      monster1.PlayerId = this.CreatePlayerID(totalCount);
      roundPlayerList.Add(monster1);
    }
    RoundPlayer monster2 = this.CreateMonster(data.MonsterID2, factor);
    if (monster2 != null)
    {
      monster2.PlayerId = this.CreatePlayerID(totalCount);
      roundPlayerList.Add(monster2);
    }
    RoundPlayer monster3 = this.CreateMonster(data.MonsterID3, factor);
    if (monster3 != null)
    {
      monster3.PlayerId = this.CreatePlayerID(totalCount);
      roundPlayerList.Add(monster3);
    }
    roundPlayerList.Sort(new Comparison<RoundPlayer>(this.Compare));
    this._playerId = 2L;
    return roundPlayerList;
  }

  public List<RoundPlayer> CreateData(List<long> uids, List<List<int>> playerSkill)
  {
    List<RoundPlayer> roundPlayerList = new List<RoundPlayer>();
    roundPlayerList.Add(this.CreateUser(0L, false));
    for (int index = 0; index < uids.Count; ++index)
    {
      RoundPlayer user = this.CreateUser(uids[index], true);
      user.PlayerId = this.CreatePlayerID(uids.Count);
      List<int> intList = playerSkill[index];
      if (user != null)
      {
        user.SetSkills(intList.ToArray());
        roundPlayerList.Add(user);
      }
    }
    roundPlayerList.Sort(new Comparison<RoundPlayer>(this.Compare));
    this._playerId = 2L;
    return roundPlayerList;
  }

  private int Compare(RoundPlayer a, RoundPlayer b)
  {
    if (a.Speed > b.Speed)
      return -1;
    if (a.Speed == b.Speed)
      return a.PlayerId.CompareTo(b.PlayerId);
    return 1;
  }

  private List<int> GetCurrentSkills()
  {
    int num = 3;
    List<int> intList = new List<int>();
    int activeSkillSlotCount = DragonKnightUtils.GetActiveSkillSlotCount();
    DragonKnightDungeonData knightDungeonData = DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L);
    for (int index = 0; index < num; ++index)
    {
      if (knightDungeonData != null && knightDungeonData.AssignedSkill != null)
      {
        int internalId = 0;
        knightDungeonData.AssignedSkill.talentSkills.TryGetValue(index + 1, out internalId);
        if (ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(internalId) != null && index < activeSkillSlotCount)
          intList.Add(internalId);
      }
    }
    return intList;
  }

  private RoundPlayer CreateUser(long uid = 0, bool autoFill = true)
  {
    bool flag = uid <= 0L;
    RoundPlayer roundPlayer1;
    if (flag)
    {
      roundPlayer1 = (RoundPlayer) new RoundPlayerMoral();
      roundPlayer1.SetSkills(this.GetCurrentSkills().ToArray());
      uid = PlayerData.inst.uid;
      roundPlayer1.PlayerId = 1L;
    }
    else
      roundPlayer1 = (RoundPlayer) new RoundPlayerEvil();
    roundPlayer1.UserData = DBManager.inst.DB_User.Get(uid);
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(uid);
    roundPlayer1.Model = dragonKnightData.Gender != DragonKnightGender.Male ? "female" : "male";
    roundPlayer1.Uid = uid;
    roundPlayer1.Damge = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Damge);
    roundPlayer1.Health = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Health);
    roundPlayer1.Defense = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Defense);
    roundPlayer1.Rebund = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Rebund);
    roundPlayer1.Speed = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Speed);
    roundPlayer1.Strong = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Strong);
    roundPlayer1.Intelligent = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Intelligence);
    roundPlayer1.Constitution = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Constitution);
    roundPlayer1.Aromor = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.Armor);
    roundPlayer1.FireDamge = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.FireDamge);
    roundPlayer1.NatureDamge = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.NaturalDamge);
    roundPlayer1.WaterDamge = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.WaterDamge);
    roundPlayer1.MagicDamge = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.MagiceDamge);
    roundPlayer1.MPRecover = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.MPRecover);
    roundPlayer1.HPRecover = dragonKnightData.GetAttibute(DragonKnightAttibute.Type.HealthRecover);
    RoundPlayer roundPlayer2 = roundPlayer1;
    int num1 = AttributeCalcHelper.Instance.CalcDragonKnightAttribute(DragonKnightAttibute.Type.MP, dragonKnightData);
    roundPlayer1.ManaMax = num1;
    int num2 = num1;
    roundPlayer2.Mana = num2;
    roundPlayer1.Init();
    if (flag && autoFill)
    {
      roundPlayer1.SetCurrentValue(DragonKnightAttibute.Type.Health, AttributeCalcHelper.Instance.GetCurrentAttibuteValueInDungen(DragonKnightAttibute.Type.Health, uid));
      roundPlayer1.SetCurrentValue(DragonKnightAttibute.Type.MP, AttributeCalcHelper.Instance.GetCurrentAttibuteValueInDungen(DragonKnightAttibute.Type.MP, uid));
    }
    return roundPlayer1;
  }

  private RoundPlayer CreateMonster(int configId, int factor = 0)
  {
    if (configId <= 0)
      return (RoundPlayer) null;
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("dungeon_max_param");
    float num1 = 1f;
    if (factor > 0)
      num1 = Mathf.Pow((float) data1.Value, (float) factor);
    DungeonMonsterMainStaticInfo data2 = ConfigManager.inst.DungeonMonsterMain.GetData(configId);
    RoundPlayer roundPlayer1 = (RoundPlayer) new RoundPlayerEvil();
    roundPlayer1.Damge = Mathf.CeilToInt((float) data2.Attack * num1);
    RoundPlayer roundPlayer2 = roundPlayer1;
    int num2 = Mathf.CeilToInt((float) data2.Health * num1);
    roundPlayer1.HealthMax = num2;
    int num3 = num2;
    roundPlayer1.Health = num3;
    int num4 = num3;
    roundPlayer2.OrignHealth = num4;
    roundPlayer1.Defense = Mathf.CeilToInt((float) data2.Defence * num1);
    roundPlayer1.Rebund = Mathf.CeilToInt((float) data2.Rebound * num1);
    roundPlayer1.Speed = Mathf.CeilToInt((float) data2.Speed * num1);
    roundPlayer1.Model = data2.Image;
    roundPlayer1.Init();
    return roundPlayer1;
  }
}
