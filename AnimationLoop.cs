﻿// Decompiled with JetBrains decompiler
// Type: AnimationLoop
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AnimationLoop : StateMachineBehaviour
{
  private float backTime = 0.9999f;
  public int loopTime;
  private int curLoopTime;

  public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    if ((double) stateInfo.normalizedTime <= (double) this.backTime)
      return;
    ++this.curLoopTime;
    if (this.curLoopTime <= this.loopTime - 1)
    {
      animator.Play(stateInfo.nameHash, layerIndex, 0.0f);
    }
    else
    {
      this.curLoopTime = 0;
      string nextStateName = this.GetNextStateName(animator, stateInfo, layerIndex);
      if (nextStateName != null)
        animator.Play(nextStateName, layerIndex, 0.0f);
      else
        animator.Stop();
    }
  }

  private string GetNextStateName(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    for (int index = 1; index < 30; ++index)
    {
      string name1 = "State" + (object) index;
      if (stateInfo.IsName(name1))
      {
        string name2 = "State" + (index + 1).ToString();
        int hash = Animator.StringToHash(name2);
        if (animator.HasState(layerIndex, hash))
          return name2;
        return "State1";
      }
    }
    return (string) null;
  }
}
