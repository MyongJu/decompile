﻿// Decompiled with JetBrains decompiler
// Type: AllianceInvitesApplicantsDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceInvitesApplicantsDlg : UI.Dialog
{
  public AllianceInvitesApplicantsDlg.InvitesSearchPage mInvitesSearchPage = new AllianceInvitesApplicantsDlg.InvitesSearchPage();
  public AllianceInvitesApplicantsDlg.SentInvitesPage mSentInvitesPage = new AllianceInvitesApplicantsDlg.SentInvitesPage();
  public AllianceInvitesApplicantsDlg.ApplicantsPage mApplicantsPage = new AllianceInvitesApplicantsDlg.ApplicantsPage();
  public UIButton mInviteSearchTab;
  public UIButton mSentInvitesTab;
  public UIButton mApplicantsTab;
  public GameObject mInviteSearchRoot;
  public GameObject mSentInvitesRoot;
  public GameObject mApplicantsRoot;
  public PageTabsMonitor mPageTabsMonitor;

  private void RegisterListener()
  {
    MessageHub.inst.GetPortByAction("apply_alliance").AddEvent(new System.Action<object>(this.OnChangedPushEvent));
    MessageHub.inst.GetPortByAction("join_alliance").AddEvent(new System.Action<object>(this.OnChangedPushEvent));
    MessageHub.inst.GetPortByAction("revoke_apply_alliance").AddEvent(new System.Action<object>(this.OnChangedPushEvent));
    MessageHub.inst.GetPortByAction("accept_alliance_invite").AddEvent(new System.Action<object>(this.OnChangedPushEvent));
    MessageHub.inst.GetPortByAction("deny_alliance_invite").AddEvent(new System.Action<object>(this.OnChangedPushEvent));
  }

  private void UnregisterListener()
  {
    MessageHub.inst.GetPortByAction("apply_alliance").RemoveEvent(new System.Action<object>(this.OnChangedPushEvent));
    MessageHub.inst.GetPortByAction("join_alliance").RemoveEvent(new System.Action<object>(this.OnChangedPushEvent));
    MessageHub.inst.GetPortByAction("revoke_apply_alliance").RemoveEvent(new System.Action<object>(this.OnChangedPushEvent));
    MessageHub.inst.GetPortByAction("accept_alliance_invite").RemoveEvent(new System.Action<object>(this.OnChangedPushEvent));
    MessageHub.inst.GetPortByAction("deny_alliance_invite").RemoveEvent(new System.Action<object>(this.OnChangedPushEvent));
  }

  private void OnChangedPushEvent(object data)
  {
    this.Refresh();
  }

  private void Refresh()
  {
    this.UpdateSentInvitations();
    this.UpdateApplicants();
  }

  private void OnTabSelected(int selectedTab)
  {
    switch (selectedTab)
    {
      case 0:
        this.mInviteSearchRoot.SetActive(true);
        this.mSentInvitesRoot.SetActive(false);
        this.mApplicantsRoot.SetActive(false);
        break;
      case 1:
        this.mInviteSearchRoot.SetActive(false);
        this.mSentInvitesRoot.SetActive(true);
        this.mApplicantsRoot.SetActive(false);
        this.UpdateSentInvitations();
        break;
      case 2:
        this.mInviteSearchRoot.SetActive(false);
        this.mSentInvitesRoot.SetActive(false);
        this.mApplicantsRoot.SetActive(true);
        this.UpdateApplicants();
        break;
    }
  }

  private void ResetSearchPage()
  {
    this.mInvitesSearchPage.mSearchInput.defaultText = "TAP HERE TO SEARCH";
    this.mInvitesSearchPage.mSearchInput.value = string.Empty;
    this.mInvitesSearchPage.mSearchButton.isEnabled = false;
    using (List<GameObject>.Enumerator enumerator = this.mInvitesSearchPage.mSearchResultList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current);
    }
    this.mInvitesSearchPage.mSearchResultList.Clear();
  }

  public void OnSearchInputChanged()
  {
    if (this.mInvitesSearchPage.mSearchInput.value.Length >= 3)
      this.mInvitesSearchPage.mSearchButton.isEnabled = true;
    else
      this.mInvitesSearchPage.mSearchButton.isEnabled = false;
  }

  public void OnSearch()
  {
    using (List<GameObject>.Enumerator enumerator = this.mInvitesSearchPage.mSearchResultList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.mInvitesSearchPage.mSearchResultList.Clear();
    MessageHub.inst.GetPortByAction("Alliance:searchUserByAlliance").SendRequest(new Hashtable()
    {
      {
        (object) "context",
        (object) this.mInvitesSearchPage.mSearchInput.value
      }
    }, new System.Action<bool, object>(this.AllianceMemberListCallback), true);
  }

  private void AllianceMemberListCallback(bool ret, object data)
  {
    ArrayList arrayList = data as ArrayList;
    if (arrayList == null)
      return;
    for (int index = 0; index < arrayList.Count; ++index)
    {
      Hashtable inData = arrayList[index] as Hashtable;
      AllianceInviteSearchPanel component = Utils.DuplicateGOB(this.mInvitesSearchPage.mSearchItemPrefab).GetComponent<AllianceInviteSearchPanel>();
      this.mInvitesSearchPage.mSearchResultList.Add(component.gameObject);
      component.gameObject.SetActive(true);
      component.onInviteSucceed += new System.Action(this.OnInviteSucceed);
      int outData1 = 0;
      DatabaseTools.UpdateData(inData, "uid", ref outData1);
      string empty = string.Empty;
      DatabaseTools.UpdateData(inData, "name", ref empty);
      int outData2 = 0;
      DatabaseTools.UpdateData(inData, "power", ref outData2);
      int outData3 = 1;
      DatabaseTools.UpdateData(inData, "level", ref outData3);
      int outData4 = 0;
      DatabaseTools.UpdateData(inData, "portrait", ref outData4);
      component.SetDetails(outData1, empty, outData2, outData3, outData4);
    }
    this.mInvitesSearchPage.mTable.Reposition();
    this.mInvitesSearchPage.mScrollView.ResetPosition();
  }

  private void OnInviteSucceed()
  {
    this.Refresh();
  }

  private void UpdateSentInvitations()
  {
    using (List<GameObject>.Enumerator enumerator = this.mSentInvitesPage.mSearchResultList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.mSentInvitesPage.mSearchResultList.Clear();
    List<AllianceInvitedApplyData> dataByAllianceId = DBManager.inst.DB_AllianceInviteApply.GetDataByAllianceId(PlayerData.inst.allianceId);
    int num = 0;
    using (List<AllianceInvitedApplyData>.Enumerator enumerator = dataByAllianceId.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceInvitedApplyData current = enumerator.Current;
        if (current.status == AllianceInvitedApplyData.Status.invited)
        {
          AllianceInvitesRevokePanel component = Utils.DuplicateGOB(this.mSentInvitesPage.mSentInviteItemPrefab).GetComponent<AllianceInvitesRevokePanel>();
          this.mSentInvitesPage.mSearchResultList.Add(component.gameObject);
          component.gameObject.SetActive(true);
          component.onRevoke += new System.Action(this.OnRevoke);
          component.SetDetails(current);
          ++num;
        }
      }
    }
    this.mSentInvitesPage.mTable.Reposition();
    this.mSentInvitesPage.mScrollView.ResetPosition();
    this.mSentInvitesPage.mNoDataItemPrefab.SetActive(num == 0);
  }

  private void OnRevoke()
  {
    this.UpdateSentInvitations();
  }

  private void UpdateApplicants()
  {
    using (List<GameObject>.Enumerator enumerator = this.mApplicantsPage.mSearchResultList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.mApplicantsPage.mSearchResultList.Clear();
    List<AllianceInvitedApplyData> dataByAllianceId = DBManager.inst.DB_AllianceInviteApply.GetDataByAllianceId(PlayerData.inst.allianceId);
    int num = 0;
    using (List<AllianceInvitedApplyData>.Enumerator enumerator = dataByAllianceId.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceInvitedApplyData current = enumerator.Current;
        if (current.status == AllianceInvitedApplyData.Status.asking)
        {
          AllianceApplicantsPanel component = Utils.DuplicateGOB(this.mApplicantsPage.mApplicantItemPrefab).GetComponent<AllianceApplicantsPanel>();
          this.mApplicantsPage.mSearchResultList.Add(component.gameObject);
          component.gameObject.SetActive(true);
          component.onAccept += new System.Action(this.OnAccept);
          component.onDeny += new System.Action(this.OnDeny);
          component.SetDetails(current);
          ++num;
        }
      }
    }
    this.mApplicantsPage.mTable.Reposition();
    this.mApplicantsPage.mScrollView.ResetPosition();
    this.mApplicantsPage.mNoDataItemPrefab.SetActive(num == 0);
    this.mApplicantsPage.mBadge.SetActive(num > 0);
    this.mApplicantsPage.mBadge.GetComponent<LabelBinder>().SetLabels((object) num);
  }

  private void OnAccept()
  {
    this.UpdateApplicants();
  }

  private void OnDeny()
  {
    this.UpdateApplicants();
  }

  public void OnDlgClosePressed()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.mPageTabsMonitor.onTabSelected += new System.Action<int>(this.OnTabSelected);
    this.mPageTabsMonitor.SetCurrentTab(0, true);
    this.ResetSearchPage();
    this.UpdateApplicants();
    this.RegisterListener();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.mPageTabsMonitor.onTabSelected -= new System.Action<int>(this.OnTabSelected);
    this.UnregisterListener();
  }

  [Serializable]
  public class InvitesSearchPage
  {
    [HideInInspector]
    public List<GameObject> mSearchResultList = new List<GameObject>();
    public GameObject mSearchItemPrefab;
    public UIInput mSearchInput;
    public UIWidget mContainer;
    public UIScrollView mScrollView;
    public UIButton mSearchButton;
    public UITable mTable;
  }

  [Serializable]
  public class SentInvitesPage
  {
    [HideInInspector]
    public List<GameObject> mSearchResultList = new List<GameObject>();
    public GameObject mSentInviteItemPrefab;
    public GameObject mNoDataItemPrefab;
    public UIWidget mContainer;
    public UIScrollView mScrollView;
    public UITable mTable;
  }

  [Serializable]
  public class ApplicantsPage
  {
    [HideInInspector]
    public List<GameObject> mSearchResultList = new List<GameObject>();
    public GameObject mApplicantItemPrefab;
    public GameObject mNoDataItemPrefab;
    public UIWidget mContainer;
    public UIScrollView mScrollView;
    public GameObject mBadge;
    public UITable mTable;
  }
}
