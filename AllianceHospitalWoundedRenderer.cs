﻿// Decompiled with JetBrains decompiler
// Type: AllianceHospitalWoundedRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UnityEngine;

public class AllianceHospitalWoundedRenderer : MonoBehaviour
{
  public UITexture m_TroopIcon;
  public UILabel m_TroopName;
  public UILabel m_TroopCount;
  public UILabel m_Tier;
  private Unit_StatisticsInfo m_Unit;

  public void SetData(string unitId, AllianceHospitalHealingData healingData)
  {
    this.m_Unit = ConfigManager.inst.DB_Unit_Statistics.GetData(unitId);
    if (this.m_Unit == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_TroopIcon, this.m_Unit.Troop_ICON, (System.Action<bool>) null, true, false, string.Empty);
    this.m_TroopName.text = ScriptLocalization.Get(this.m_Unit.Troop_Name_LOC_ID, true);
    this.m_TroopCount.text = Utils.FormatThousands(healingData.total.ToString());
    this.m_Tier.text = Utils.GetRomaNumeralsBelowTen(this.m_Unit.Troop_Tier);
  }

  public Unit_StatisticsInfo Unit
  {
    get
    {
      return this.m_Unit;
    }
  }
}
