﻿// Decompiled with JetBrains decompiler
// Type: CommunityEventsController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class CommunityEventsController : AnniversaryOtherController
{
  private List<AnniversaryCommunityRewardRenderer> itemList = new List<AnniversaryCommunityRewardRenderer>();
  public UILabel checkArtworkLabel;
  public UILabel[] awardTitles;
  public UIButton checkArtworks;
  public UIScrollView scrollView;
  public AnniversaryCommunityRewardRenderer[] awardRenderers;

  public override void UpdateUI()
  {
    this.UpdatePageContent();
    this.UpdateData();
  }

  public override void UpdatePageContent()
  {
    base.UpdatePageContent();
    this.eventDescription.text = Utils.XLAT("event_1_year_celebration_community_event_description");
    this.scrollView.ResetPosition();
    this.buttonLabel.text = Utils.XLAT("event_go_to_button");
    this.checkArtworkLabel.text = Utils.XLAT("event_see_artworks_button");
    this.awardTitles[0].text = Utils.XLAT("id_participation_prize");
    this.awardTitles[1].text = Utils.XLAT("id_merit_prize");
    this.awardTitles[2].text = Utils.XLAT("id_grand_prize");
  }

  public void UpdateData()
  {
    List<AnniversaryCommunityRewardInfo> communityRewardList = ConfigManager.inst.DB_AnniversaryCommunityReward.GetAnnCommunityRewardList();
    if (communityRewardList == null || communityRewardList.Count <= 0)
      return;
    for (int index = 0; index < communityRewardList.Count; ++index)
    {
      if (this.awardRenderers != null && this.awardRenderers.Length > 0)
      {
        AnniversaryCommunityRewardRenderer awardRenderer = this.awardRenderers[index];
        if ((Object) null != (Object) awardRenderer)
        {
          awardRenderer.SetData(communityRewardList[index]);
          this.itemList.Add(awardRenderer);
        }
      }
    }
  }

  public override void OnBtnClicked()
  {
    Application.OpenURL("https://www.wenjuan.com/s/aM7buu/?user={0}".Replace("{0}", AccountManager.Instance.FunplusID));
  }

  public void OnCheckArtWorksBtnClicked()
  {
    string currentLanguageCode = LocalizationManager.CurrentLanguageCode;
    string url;
    if (currentLanguageCode != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CommunityEventsController.\u003C\u003Ef__switch\u0024map27 == null)
      {
        // ISSUE: reference to a compiler-generated field
        CommunityEventsController.\u003C\u003Ef__switch\u0024map27 = new Dictionary<string, int>(2)
        {
          {
            "zh-CN",
            0
          },
          {
            "ru",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (CommunityEventsController.\u003C\u003Ef__switch\u0024map27.TryGetValue(currentLanguageCode, out num))
      {
        switch (num)
        {
          case 0:
            url = "http://weibo.com/p/1006066058931574/photos?from=page_100606&mod=TAB#place";
            goto label_8;
          case 1:
            url = "https://vk.com/album-131502859_245275887";
            goto label_8;
        }
      }
    }
    url = "https://www.facebook.com/pg/koadw/photos/?tab=album&album_id=526092694416835";
label_8:
    Application.OpenURL(url);
  }

  private void Release()
  {
    for (int index = 0; index < this.itemList.Count; ++index)
      this.itemList[index].Release();
    this.itemList.Clear();
  }

  public void Show()
  {
    NGUITools.SetActive(this.gameObject, true);
    this.UpdateUI();
  }

  public void Hide()
  {
    NGUITools.SetActive(this.gameObject, false);
    this.Release();
  }
}
