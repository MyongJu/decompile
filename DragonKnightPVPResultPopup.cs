﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightPVPResultPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonKnightPVPResultPopup : Popup
{
  public const string WIN_IMAGE = "Texture/DragonKnight/DungeonTexture/dragon_knight_pvp_result_win";
  public const string LOSE_IMAGE = "Texture/DragonKnight/DungeonTexture/dragon_knight_pvp_result_lose";
  public DragonKnightPVPResultPanel m_LeftPanel;
  public DragonKnightPVPResultPanel m_RightPanel;
  public UITexture m_Result;
  public UILabel m_TotalRound;
  public UILabel m_OldScore;
  public UILabel m_NewScore;
  public UILabel m_ScoreDiff;
  public UISprite m_Up;
  public UISprite m_Down;
  public DKArenaTierUpVFX m_VFX;
  public bool m_PlayVFX;
  private DragonKnightPVPResultPopup.Parameter m_Parameter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as DragonKnightPVPResultPopup.Parameter;
    if (this.m_Parameter == null)
      return;
    if (this.m_Parameter.win == 1)
      AudioManager.Instance.StopAndPlaySound("sfx_dragon_knight_pvp_win");
    else
      AudioManager.Instance.StopAndPlaySound("sfx_dragon_knight_pvp_lose");
    string path = string.Empty;
    switch (this.m_Parameter.win)
    {
      case 0:
        path = "Texture/DragonKnight/DungeonTexture/dragon_knight_pvp_result_lose";
        break;
      case 1:
        path = "Texture/DragonKnight/DungeonTexture/dragon_knight_pvp_result_win";
        break;
    }
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Result, path, (System.Action<bool>) null, false, true, string.Empty);
    this.m_LeftPanel.SetData(this.m_Parameter.source, this.m_Parameter.sourceAlliance, this.m_Parameter.sourceDK, this.m_Parameter.sourceDamage, this.m_Parameter.sourceHeal, this.m_Parameter.score, this.m_Parameter.win);
    this.m_RightPanel.SetData(this.m_Parameter.target, this.m_Parameter.targetAlliance, this.m_Parameter.targetDK, this.m_Parameter.targetDamage, this.m_Parameter.targetHeal, this.m_Parameter.oppScore, this.GetOppWin());
    this.m_TotalRound.text = string.Format(Utils.XLAT("dragon_knight_pvp_round_num"), (object) this.m_Parameter.totalRounds);
    if (!this.m_Parameter.isArena)
      return;
    this.m_OldScore.text = this.m_Parameter.oldSocre.ToString();
    this.m_NewScore.text = this.m_Parameter.score.ToString();
    long num = this.m_Parameter.score - this.m_Parameter.oldSocre;
    this.m_ScoreDiff.text = Math.Abs(num).ToString();
    this.m_ScoreDiff.color = num <= 0L ? Color.red : Color.green;
    this.m_Up.gameObject.SetActive(num > 0L);
    this.m_Down.gameObject.SetActive(num <= 0L);
    this.m_PlayVFX = true;
  }

  private void Update()
  {
    if (!this.m_PlayVFX)
      return;
    this.m_PlayVFX = false;
    DKArenaTitleReward titleRewardByScore1 = ConfigManager.inst.DB_DKArenaTitleReward.GetTitleRewardByScore(this.m_Parameter.oldSocre);
    DKArenaTitleReward titleRewardByScore2 = ConfigManager.inst.DB_DKArenaTitleReward.GetTitleRewardByScore(this.m_Parameter.score);
    if (titleRewardByScore1 == null || titleRewardByScore2 == null || titleRewardByScore2.ScoreMin <= titleRewardByScore1.ScoreMax)
      return;
    this.m_VFX.SetData(titleRewardByScore1.ImagePath, titleRewardByScore2.ImagePath);
    this.m_VFX.Play();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    if (this.m_Parameter != null && this.m_Parameter.callback != null)
      this.m_Parameter.callback();
    DBManager.inst.DB_Gems.ClearThirdPartyInventory();
  }

  private int GetOppWin()
  {
    switch (this.m_Parameter.win)
    {
      case 0:
        return 1;
      case 1:
        return 0;
      case 2:
        return 2;
      default:
        return 0;
    }
  }

  public void OnCompare()
  {
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightPVPComparisonPopup", (Popup.PopupParameter) new DragonKnightPVPComparisonPopup.Parameter()
    {
      source = this.m_Parameter.source,
      sourceDK = this.m_Parameter.sourceDK,
      sourceEquipments = this.m_Parameter.sourceEquipments,
      sourceTalent1 = this.m_Parameter.sourceTalent1,
      sourceTalent2 = this.m_Parameter.sourceTalent2,
      sourceTalent3 = this.m_Parameter.sourceTalent3,
      sourceScore = this.m_Parameter.score,
      target = this.m_Parameter.target,
      targetDK = this.m_Parameter.targetDK,
      targetEquipments = this.m_Parameter.targetEquipments,
      targetTalent1 = this.m_Parameter.targetTalent1,
      targetTalent2 = this.m_Parameter.targetTalent2,
      targetTalent3 = this.m_Parameter.targetTalent3,
      targetScore = this.m_Parameter.oppScore
    });
  }

  public void OnOkay()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public UserData source;
    public AllianceData sourceAlliance;
    public DragonKnightData sourceDK;
    public List<InventoryItemData> sourceEquipments;
    public int sourceDamage;
    public int sourceHeal;
    public int sourceTalent1;
    public int sourceTalent2;
    public int sourceTalent3;
    public UserData target;
    public AllianceData targetAlliance;
    public DragonKnightData targetDK;
    public List<InventoryItemData> targetEquipments;
    public int targetDamage;
    public int targetHeal;
    public int targetTalent1;
    public int targetTalent2;
    public int targetTalent3;
    public int win;
    public int totalRounds;
    public int time;
    public long oldSocre;
    public long score;
    public long oppScore;
    public bool isArena;
    [JsonIgnore]
    public System.Action callback;
  }
}
