﻿// Decompiled with JetBrains decompiler
// Type: AllianceMemberItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceMemberItemRenderer : MonoBehaviour, IMemberItemRenderer, IOnlineUpdateable
{
  public UITexture m_MemberPortrait;
  public UITexture m_MemberCrown;
  public UISprite m_MemberFlag;
  public UILabel m_MemberName;
  public UILabel m_OnlinePower;
  public UILabel m_OfflinePower;
  public UILabel m_Online;
  public UILabel m_Offline;
  private AllianceData m_AllianceData;
  private UserData m_UserData;
  private int m_Title;

  public void SetData(AllianceData allianceData, UserData userData, int title)
  {
    this.m_AllianceData = allianceData;
    this.m_UserData = userData;
    this.m_Title = title;
    if (this.m_UserData.uid == PlayerData.inst.uid)
      this.m_UserData.online = true;
    this.UpdateUI();
  }

  public void SetNominate(bool nominate)
  {
  }

  public void OnItemClick()
  {
    AllianceMemberOptionPopup.Parameter parameter = new AllianceMemberOptionPopup.Parameter();
    parameter.targetUserData = this.m_UserData;
    parameter.targetTitle = this.m_Title;
    AllianceMemberData allianceMemberData = this.m_AllianceData.members.Get(PlayerData.inst.uid);
    parameter.yourTitle = allianceMemberData == null ? 0 : allianceMemberData.title;
    parameter.yourUserData = PlayerData.inst.userData;
    UIManager.inst.OpenPopup("Alliance/AllianceMemberOptionPopup", (Popup.PopupParameter) parameter);
  }

  public void UpdateOnlineStatus(List<long> onlineStatus)
  {
    this.m_UserData.online = false;
    for (int index = 0; index < onlineStatus.Count; ++index)
    {
      if (onlineStatus[index] == this.m_UserData.channelId)
      {
        this.m_UserData.online = true;
        this.m_UserData.lastLogin = (long) NetServerTime.inst.ServerTimestamp;
        break;
      }
    }
    this.UpdateStatus();
  }

  private void UpdateUI()
  {
    this.m_MemberCrown.gameObject.SetActive(this.m_Title != 0);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_MemberCrown, AllianceUtilities.GetCrownIconPath(this.m_Title), (System.Action<bool>) null, true, false, string.Empty);
    Utils.SetPortrait(this.m_MemberPortrait, this.m_UserData.PortraitPath);
    this.m_MemberName.text = this.m_UserData.userName;
    this.UpdateStatus();
  }

  private void UpdateStatus()
  {
    if (this.m_UserData.online)
    {
      this.m_MemberCrown.color = Color.white;
      this.m_MemberFlag.color = Color.white;
      this.m_Online.gameObject.SetActive(true);
      this.m_Offline.gameObject.SetActive(false);
      this.m_OnlinePower.text = Utils.FormatThousands(this.m_UserData.power.ToString());
    }
    else
    {
      this.m_MemberCrown.color = Color.blue;
      this.m_MemberFlag.color = Color.blue;
      this.m_Online.gameObject.SetActive(false);
      this.m_Offline.gameObject.SetActive(true);
      this.m_OfflinePower.text = Utils.FormatThousands(this.m_UserData.power.ToString());
      this.m_Offline.text = Utils.FormatOfflineTime((long) (int) ((long) NetServerTime.inst.ServerTimestamp - this.m_UserData.lastLogin));
    }
  }

  GameObject IOnlineUpdateable.get_gameObject()
  {
    return this.gameObject;
  }
}
