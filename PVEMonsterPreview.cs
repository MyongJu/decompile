﻿// Decompiled with JetBrains decompiler
// Type: PVEMonsterPreview
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class PVEMonsterPreview : MonoBehaviour
{
  private GameObject m_Instance;

  public void InitializeCharacter(TileData tileData)
  {
    string type = string.Empty;
    switch (tileData.TileType)
    {
      case TileType.WorldBoss:
        type = ConfigManager.inst.DB_WorldBoss.GetData(DBManager.inst.DB_WorldBossDB.Get(tileData.ResourceId).configId).Type;
        break;
      case TileType.KingdomBoss:
        type = ConfigManager.inst.DB_KingdomBoss.GetItem(DBManager.inst.DB_KingdomBossDB.Get(tileData.ResourceId).configId).Type;
        break;
      default:
        WorldEncounterData data = ConfigManager.inst.DB_WorldEncount.GetData((long) tileData.ResourceId);
        if (data != null)
        {
          type = data.type;
          break;
        }
        break;
    }
    if (string.IsNullOrEmpty(type))
      D.warn((object) "prefab monster is empty");
    else
      AssetManager.Instance.LoadAsync(ConfigWorldEncounter.GetMonsterPrefabPath(type), (System.Action<UnityEngine.Object, bool>) ((o, ret) =>
      {
        try
        {
          this.Clear();
          this.m_Instance = UnityEngine.Object.Instantiate<GameObject>(o as GameObject);
          this.m_Instance.transform.parent = this.transform;
          if (type != "dragon")
            this.m_Instance.transform.localPosition = new Vector3(-23f, 72f, 0.0f);
          if (type == "stoneman")
            this.m_Instance.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
          NGUITools.SetLayer(this.m_Instance, LayerMask.NameToLayer("UI3D"));
          AssetManager.Instance.UnLoadAsset(ConfigWorldEncounter.GetMonsterPrefabPath(type), (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
        }
        catch
        {
          this.Clear();
        }
      }), (System.Type) null);
  }

  private void Clear()
  {
    if (!(bool) ((UnityEngine.Object) this.m_Instance))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_Instance);
    this.m_Instance = (GameObject) null;
  }
}
