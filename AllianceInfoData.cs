﻿// Decompiled with JetBrains decompiler
// Type: AllianceInfoData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class AllianceInfoData : IComponentData
{
  public string acronym;
  public string power;
  public string member_count;
  public string member_max;
  public string symbol_code;
  public string language;
  public string lord_name;
  public string lord_portrait;
  public string admin;
  public string name;
}
