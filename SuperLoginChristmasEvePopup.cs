﻿// Decompiled with JetBrains decompiler
// Type: SuperLoginChristmasEvePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class SuperLoginChristmasEvePopup : Popup
{
  public UITexture christmasEveFrame;
  public UITexture fatherChristmas;
  public UnityEngine.Animation fatherChristmasAnim;
  public GameObject itemRoot;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.ShowChristmasEveContent();
  }

  private void OnDailyCardPressed(SuperLoginMainInfo mainInfo)
  {
    if (mainInfo == null)
      return;
    UIManager.inst.OpenPopup("SuperLogin/SuperLoginChristmasEveGiftPopup", (Popup.PopupParameter) new SuperLoginChristmasEveGiftPopup.Parameter()
    {
      mainInfo = mainInfo,
      onGiftReceived = new System.Action<int>(this.OnGiftClaimed)
    });
  }

  private void OnGiftClaimed(int day)
  {
    this.ShowChristmasEveContent();
    if (day != 25 || !this.Is25thDayGiftReceived())
      return;
    this.fatherChristmasAnim.Play("FatherChristmasFadeIn");
  }

  private void ShowChristmasEveContent()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.christmasEveFrame, "Texture/SuperLogin/Advent/advent_dialog_decorative_image", (System.Action<bool>) null, false, false, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.fatherChristmas, "Texture/SuperLogin/Advent/advent_father_christmas_image", (System.Action<bool>) null, true, false, string.Empty);
    ChristmasEveItemSlotRenderer[] componentsInChildren = this.itemRoot.GetComponentsInChildren<ChristmasEveItemSlotRenderer>();
    if (componentsInChildren != null)
    {
      for (int index = 0; index < componentsInChildren.Length; ++index)
      {
        componentsInChildren[index].onDailyCardPressed -= new System.Action<SuperLoginMainInfo>(this.OnDailyCardPressed);
        componentsInChildren[index].onDailyCardPressed += new System.Action<SuperLoginMainInfo>(this.OnDailyCardPressed);
        componentsInChildren[index].UpdateUI();
      }
    }
    NGUITools.SetActive(this.itemRoot, !this.Is25thDayGiftReceived());
    NGUITools.SetActive(this.fatherChristmas.transform.parent.gameObject, this.Is25thDayGiftReceived());
  }

  private bool Is25thDayGiftReceived()
  {
    List<SuperLoginMainInfo> mainInfoListByGroup = ConfigManager.inst.DB_SuperLoginMain.GetSuperLoginMainInfoListByGroup("super_log_in_christmas");
    if (mainInfoListByGroup != null)
    {
      SuperLoginMainInfo superLoginMainInfo = mainInfoListByGroup.Find((Predicate<SuperLoginMainInfo>) (x => x.day == 25));
      if (superLoginMainInfo != null)
        return superLoginMainInfo.RewardState == SuperLoginPayload.DailyRewardState.HAS_GOT;
    }
    return false;
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }
}
