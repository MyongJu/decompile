﻿// Decompiled with JetBrains decompiler
// Type: DragonTendencySkilBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DragonTendencySkilBar : MonoBehaviour
{
  private List<DragonTendencySkillItemRenderer> m_ItemList = new List<DragonTendencySkillItemRenderer>();
  public GameObject m_ItemPrefab;
  public UIGrid m_Grid;
  private bool m_Start;

  private void Start()
  {
    this.m_Start = true;
    this.UpdateUI();
  }

  private void OnEnable()
  {
    if (!this.m_Start)
      return;
    this.UpdateUI();
  }

  private void ClearData()
  {
    for (int index = 0; index < this.m_ItemList.Count; ++index)
    {
      DragonTendencySkillItemRenderer skillItemRenderer = this.m_ItemList[index];
      skillItemRenderer.gameObject.SetActive(false);
      Object.Destroy((Object) skillItemRenderer.gameObject);
    }
    this.m_ItemList.Clear();
  }

  public void UpdateUI()
  {
    this.ClearData();
    int tendencySkillCount = DragonUtils.GetDragonTendencySkillCount();
    for (int index = 0; index < tendencySkillCount; ++index)
    {
      ConfigDragonTendencyBoostInfo dragonTendencyBoost = ConfigManager.inst.DB_ConfigDragonTendencyBoost.Get(DragonUtils.DRAGON_TENDENCY_SKILL_IDS[index]);
      GameObject gameObject = Object.Instantiate<GameObject>(this.m_ItemPrefab);
      gameObject.transform.parent = this.m_Grid.transform;
      gameObject.transform.localScale = Vector3.one;
      gameObject.SetActive(true);
      DragonTendencySkillItemRenderer component = gameObject.GetComponent<DragonTendencySkillItemRenderer>();
      component.Index = index;
      component.SetData(dragonTendencyBoost, PlayerData.inst.dragonData);
      this.m_ItemList.Add(component);
    }
    this.m_Grid.Reposition();
  }
}
