﻿// Decompiled with JetBrains decompiler
// Type: EquipmentEnhanceInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentEnhanceInfo : MonoBehaviour
{
  public bool showOwn = true;
  private List<EquipmentSuitItem> allEquipmentSuitItem = new List<EquipmentSuitItem>();
  private List<EquipmentGemItem> _allEquipmentGemItem = new List<EquipmentGemItem>();
  public EnhanceEquipmentComponent enhanceEquipmentComponent;
  public EquipmentEnhanceBenefits benefitContent;
  public UICompnent.RequireComponent require;
  public EquipmentForgeBottom bottom;
  public UITable table;
  public UIScrollView scrollView;
  public Transform equipmentSuitItemContainer;
  public Transform equipmentGemItemContainer;
  private ConfigEquipmentPropertyInfo equipmentInfo;
  private bool showOwnMessage;
  private int enhanceLevel;
  private BagType _bagType;
  private InventoryItemData inventoryItemData;
  private long _equipmentItemId;

  public void Init(BagType bagType, long equipmentItemID = 0)
  {
    this._bagType = bagType;
    bool flag = this._equipmentItemId != equipmentItemID;
    this._equipmentItemId = equipmentItemID;
    if (equipmentItemID != 0L)
    {
      this.gameObject.SetActive(true);
      this.inventoryItemData = DBManager.inst.GetInventory(this._bagType).GetMyItemByItemID(equipmentItemID);
      this.enhanceLevel = this.inventoryItemData.enhanced;
      this.equipmentInfo = ConfigManager.inst.GetEquipmentProperty(this._bagType).GetDataByEquipIDAndEnhanceLevel(this.inventoryItemData.internalId, this.enhanceLevel);
      ConfigManager.inst.GetEquipmentScroll(this._bagType).GetData(this.equipmentInfo.scrollID);
      this.UpdateEquipmentGem(bagType, this.inventoryItemData);
      this.UpdateEquipmentSuitBenefit(bagType, this.inventoryItemData);
      this.showOwnMessage = this.showOwn;
      this.DrawEquipment();
      this.DrawBenefit();
      this.DrawRequire(this.showOwnMessage);
      Utils.ExecuteInSecs(0.01f, (System.Action) (() =>
      {
        if (!((UnityEngine.Object) this.table != (UnityEngine.Object) null))
          return;
        this.table.Reposition();
      }));
      this.scrollView = this.gameObject.GetComponentInParent<UIScrollView>();
      if (!((UnityEngine.Object) this.scrollView != (UnityEngine.Object) null) || !flag)
        return;
      this.scrollView.ResetPosition();
    }
    else
      this.gameObject.SetActive(false);
  }

  public void Dispose()
  {
    this.enhanceEquipmentComponent.Dispose();
    this.benefitContent.Dispose();
    if (!((UnityEngine.Object) this.bottom != (UnityEngine.Object) null))
      return;
    this.bottom.Dispose();
  }

  private void DrawEquipment()
  {
    this.enhanceEquipmentComponent.FeedData((IComponentData) new EquipmentComponentData(this.equipmentInfo.equipID, this.inventoryItemData.enhanced, this._bagType, this.inventoryItemData.itemId));
  }

  private void DrawBenefit()
  {
    Benefits benefits = ConfigManager.inst.GetEquipmentProperty(this._bagType).GetDataByEquipIDAndEnhanceLevel(this.equipmentInfo.equipID, this.equipmentInfo.enhanceLevel).benefits;
    Benefits nextLevel = (Benefits) null;
    if (ConfigManager.inst.GetEquipmentProperty(this._bagType).GetDataByEquipIDAndEnhanceLevel(this.equipmentInfo.equipID, this.equipmentInfo.enhanceLevel + 1) != null)
      nextLevel = ConfigManager.inst.GetEquipmentProperty(this._bagType).GetDataByEquipIDAndEnhanceLevel(this.equipmentInfo.equipID, this.equipmentInfo.enhanceLevel + 1).benefits;
    this.benefitContent.Init(benefits, nextLevel);
  }

  private void DrawRequire(bool showOwnMessage)
  {
    if (EquipmentManager.Instance.IsEnhancedToMax(this._bagType, this.inventoryItemData.itemId))
    {
      this.require.gameObject.SetActive(false);
    }
    else
    {
      ConfigEquipmentPropertyInfo idAndEnhanceLevel = ConfigManager.inst.GetEquipmentProperty(this._bagType).GetDataByEquipIDAndEnhanceLevel(this.equipmentInfo.equipID, this.equipmentInfo.enhanceLevel + 1);
      this.require.gameObject.SetActive(true);
      List<RequireComponentData.RequireElement> requires = new List<RequireComponentData.RequireElement>();
      if (idAndEnhanceLevel.steelValue > 0L)
      {
        int num = Mathf.CeilToInt(ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) idAndEnhanceLevel.steelValue, "calc_crafting_steel_cost"));
        requires.Add(new RequireComponentData.RequireElement(RequireComponentData.Requirement.STEEL, (long) num, 0, showOwnMessage));
      }
      if (idAndEnhanceLevel.scrollValue > 0L)
        requires.Add(new RequireComponentData.RequireElement(RequireComponentData.Requirement.SCROLL, idAndEnhanceLevel.scrollValue, idAndEnhanceLevel.scrollID, showOwnMessage));
      if (idAndEnhanceLevel.Materials != null)
      {
        List<Materials.MaterialElement> materials = idAndEnhanceLevel.Materials.GetMaterials();
        for (int index = 0; index < materials.Count; ++index)
          requires.Add(new RequireComponentData.RequireElement(RequireComponentData.Requirement.ITEM, materials[index].amount, materials[index].internalID, showOwnMessage));
      }
      this.require.FeedData((IComponentData) new RequireComponentData(this._bagType, requires));
    }
  }

  private void ClearEquipmentSuitItem()
  {
    using (List<EquipmentSuitItem>.Enumerator enumerator = this.allEquipmentSuitItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EquipmentSuitItem current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.allEquipmentSuitItem.Clear();
  }

  private void UpdateEquipmentSuitBenefit(BagType bagType, InventoryItemData inventoryData)
  {
    this.ClearEquipmentSuitItem();
    ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(this._bagType).GetData(inventoryData.internalId);
    if (data == null || data.suitGroup == 0)
      return;
    string fullname = "Prefab/UI/Common/EquipmentSuitItem";
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load(fullname, (System.Type) null)) as GameObject;
    AssetManager.Instance.UnLoadAsset(fullname, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    gameObject.transform.SetParent(this.equipmentSuitItemContainer, true);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    EquipmentSuitItem component = gameObject.GetComponent<EquipmentSuitItem>();
    component.SetData(PlayerData.inst.uid, this._bagType, data.suitGroup, inventoryData);
    this.allEquipmentSuitItem.Add(component);
  }

  private void ClearEquipmentGemItem()
  {
    using (List<EquipmentGemItem>.Enumerator enumerator = this._allEquipmentGemItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EquipmentGemItem current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this._allEquipmentGemItem.Clear();
  }

  private void UpdateEquipmentGem(BagType bagType, InventoryItemData inventoryItemData)
  {
    this.ClearEquipmentGemItem();
    string fullname = "Prefab/UI/Common/EquipmentGemItem";
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load(fullname, (System.Type) null)) as GameObject;
    AssetManager.Instance.UnLoadAsset(fullname, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    gameObject.transform.SetParent(this.equipmentGemItemContainer, true);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    EquipmentGemItem component = gameObject.GetComponent<EquipmentGemItem>();
    component.SetData(inventoryItemData, true, this.scrollView);
    this._allEquipmentGemItem.Add(component);
  }
}
