﻿// Decompiled with JetBrains decompiler
// Type: GemHandbookContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using GemConstant;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GemHandbookContent : MonoBehaviour
{
  private List<ConfigEquipmentGemInfo> gemHandbookDataList = new List<ConfigEquipmentGemInfo>();
  private List<GemCategoryGroupRender> groupRenderList = new List<GemCategoryGroupRender>();
  public UIScrollView scrollView;
  public UITable table;
  public GameObject groupTemplete;
  private SlotType slotType;
  private List<ConfigEquipmentGemInfo> dataList;
  private List<EquipmentGemHandbookSortInfo> gemHandbookSortInfoList;

  private void UpdateUI()
  {
    this.ClearTable();
    this.ClearData();
    this.dataList = ConfigManager.inst.DB_EquipmentGem.GetDatasBySlotType(this.slotType);
    for (int index = 0; index < this.dataList.Count; ++index)
    {
      if (this.dataList[index].handbookId != 0)
        this.gemHandbookDataList.Add(this.dataList[index]);
    }
    this.gemHandbookSortInfoList = ConfigManager.inst.DB_GemHandbookSort.GetDatas();
    for (int index1 = 0; index1 < this.gemHandbookSortInfoList.Count; ++index1)
    {
      GameObject go = NGUITools.AddChild(this.table.gameObject, this.groupTemplete.gameObject);
      GemCategoryGroupRender component = go.GetComponent<GemCategoryGroupRender>();
      NGUITools.SetActive(go, true);
      component.OnItemSelected += new System.Action<GemHandbookComponent>(this.HandleOnItemSelected);
      this.groupRenderList.Add(component);
      List<ConfigEquipmentGemInfo> equipmentGemInfoList = new List<ConfigEquipmentGemInfo>();
      for (int index2 = 0; index2 < this.gemHandbookDataList.Count; ++index2)
      {
        EquipmentGemHandbookInfo equipmentGemHandbookInfo = ConfigManager.inst.DB_GemHandbook.Get(this.gemHandbookDataList[index2].handbookId);
        if (equipmentGemHandbookInfo != null && int.Parse(equipmentGemHandbookInfo.sortId) == this.gemHandbookSortInfoList[index1].internalId)
          equipmentGemInfoList.Add(this.gemHandbookDataList[index2]);
      }
      equipmentGemInfoList.Sort(new Comparison<ConfigEquipmentGemInfo>(this.Compare));
      component.FeedData(new GemCategoryGroupRender.Parameter()
      {
        groupName = this.gemHandbookSortInfoList[index1].LocName,
        dataList = equipmentGemInfoList
      });
    }
    this.groupRenderList[0].SetFirstAsDefault();
    this.table.Reposition();
    this.scrollView.ResetPosition();
  }

  private void HandleOnItemSelected(GemHandbookComponent obj)
  {
    for (int index1 = 0; index1 < this.groupRenderList.Count; ++index1)
    {
      List<GemHandbookComponent> compnents = this.groupRenderList[index1].GetCompnents();
      for (int index2 = 0; index2 < compnents.Count; ++index2)
      {
        if (obj.gemConfigInfo.ID != compnents[index2].gemConfigInfo.ID)
          compnents[index2].Select = false;
      }
    }
  }

  public void Show(SlotType slotType)
  {
    this.gameObject.SetActive(true);
    this.slotType = slotType;
    this.UpdateUI();
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
    this.ClearData();
  }

  private void ClearTable()
  {
    UIUtils.ClearTable(this.table);
  }

  private void ClearData()
  {
    if (this.dataList != null)
    {
      this.dataList.Clear();
      this.dataList = (List<ConfigEquipmentGemInfo>) null;
    }
    if (this.gemHandbookDataList != null)
      this.gemHandbookDataList.Clear();
    if (this.gemHandbookSortInfoList != null)
    {
      this.gemHandbookSortInfoList.Clear();
      this.gemHandbookSortInfoList = (List<EquipmentGemHandbookSortInfo>) null;
    }
    if (this.groupRenderList.Count <= 0)
      return;
    for (int index = 0; index < this.groupRenderList.Count; ++index)
    {
      this.groupRenderList[index].ClearData();
      this.groupRenderList[index].OnItemSelected -= new System.Action<GemHandbookComponent>(this.HandleOnItemSelected);
      this.groupRenderList[index].gameObject.SetActive(false);
      this.groupRenderList[index].gameObject.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.groupRenderList[index].gameObject);
    }
    this.groupRenderList.Clear();
  }

  private int Compare(ConfigEquipmentGemInfo a, ConfigEquipmentGemInfo b)
  {
    EquipmentGemHandbookInfo equipmentGemHandbookInfo1 = ConfigManager.inst.DB_GemHandbook.Get(a.handbookId);
    if (equipmentGemHandbookInfo1 == null)
      D.error((object) string.Format("Can't get EquipmentGemHandbookInfo with id: {0}", (object) a.handbookId));
    EquipmentGemHandbookInfo equipmentGemHandbookInfo2 = ConfigManager.inst.DB_GemHandbook.Get(b.handbookId);
    if (equipmentGemHandbookInfo2 == null)
      D.error((object) string.Format("Can't get EquipmentGemHandbookInfo with id: {0}", (object) b.handbookId));
    return Math.Sign(equipmentGemHandbookInfo1.priority - equipmentGemHandbookInfo2.priority);
  }
}
