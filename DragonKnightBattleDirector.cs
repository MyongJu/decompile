﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightBattleDirector
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DragonKnightBattleDirector : MonoBehaviour
{
  private List<GameObject> _monsterObjectList = new List<GameObject>();
  public BattleScenePlot[] Plots;

  public void Startup(Dictionary<long, RoundPlayer> monsters)
  {
    BattleManager.Instance.OnTargetChanged = new System.Action<long>(this.OnTargetChanged);
    int index = 0;
    using (Dictionary<long, RoundPlayer>.Enumerator enumerator = monsters.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<long, RoundPlayer> current = enumerator.Current;
        this.Plots[index].Initialize(current.Value.Model);
        this.Plots[index].SetHudActive(true);
        string fullname = "Prefab/DragonKnight/Objects/NPC/" + current.Value.Model;
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.Load(fullname, (System.Type) null) as GameObject);
        AssetManager.Instance.UnLoadAsset(fullname, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
        gameObject.SetActive(true);
        gameObject.transform.SetParent(this.Plots[index].transform, false);
        gameObject.transform.localRotation = Quaternion.Euler(0.0f, 180f, 0.0f);
        this._monsterObjectList.Add(gameObject);
        GuardEventReceiver componentInChildren1 = gameObject.GetComponentInChildren<GuardEventReceiver>();
        componentInChildren1.Player = current.Value;
        current.Value.Receiver = componentInChildren1;
        UnityEngine.Animation componentInChildren2 = gameObject.GetComponentInChildren<UnityEngine.Animation>();
        current.Value.Startup(componentInChildren2, (IRoundPlayerHud) this.Plots[index].PlayerHud);
        this.Plots[index].PlayerHud.Initialize();
        this.Plots[index].PlayerHud.SetLordInfo(current.Value.UserData);
        this.Plots[index].PlayerHud.SetHealthProgress((float) current.Value.Health / (float) current.Value.HealthMax);
        this.Plots[index].PlayerHud.SetHealthNumber(current.Value.Health, current.Value.HealthMax);
        this.Plots[index].PlayerHud.SetManaProgress((float) current.Value.Mana / (float) current.Value.ManaMax);
        this.Plots[index].PlayerHud.SetHealthNumber(current.Value.Mana, current.Value.ManaMax);
        this.Plots[index].Player = current.Value;
        ++index;
      }
    }
  }

  public void Shutdown()
  {
    for (int index = 0; index < this._monsterObjectList.Count; ++index)
    {
      this.Plots[index].SetHudActive(false);
      this._monsterObjectList[index].transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this._monsterObjectList[index]);
    }
    this._monsterObjectList.Clear();
  }

  private void OnTargetChanged(long playerId)
  {
    for (int index = 0; index < this.Plots.Length; ++index)
    {
      bool active = this.Plots[index].Player != null && this.Plots[index].Player.PlayerId == playerId;
      this.Plots[index].PlayerHud.SetArrow(active);
    }
  }
}
