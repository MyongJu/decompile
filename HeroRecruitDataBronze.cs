﻿// Decompiled with JetBrains decompiler
// Type: HeroRecruitDataBronze
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;

public class HeroRecruitDataBronze : HeroRecruitData
{
  public override bool IsFirstUsed()
  {
    int num = 0;
    this.LegendTemple.FirstUsed.TryGetValue(1, out num);
    return num == 1;
  }

  public override long GetFreeCD()
  {
    long num = 0;
    this.LegendTemple.FreeCD.TryGetValue(1, out num);
    return num;
  }

  public override string GetTip()
  {
    return Utils.XLAT("hero_summon_condition_blue_description");
  }

  public override string GetTip2()
  {
    return Utils.XLAT("hero_summon_apprentice_circle_tip");
  }

  public override long GetCurrency()
  {
    return PlayerData.inst.userData.currency.gold;
  }

  public override long GetPrice()
  {
    return (long) ConfigManager.inst.DB_ParliamentHeroSummonGroup.Get("1").reqGoldAmount;
  }

  public override int GetLeftCD()
  {
    return (int) this.GetFreeCD() - NetServerTime.inst.ServerTimestamp;
  }

  public override bool IsFreeNow()
  {
    return this.GetLeftCD() < 0;
  }

  public override bool CanShowTimer()
  {
    return !this.IsFreeNow();
  }

  public override int GetSummonType()
  {
    return 1;
  }

  public override void ShowStore(int price)
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  public override void GotoResultDialog(HeroRecruitPayload payload)
  {
    List<HeroRecruitPayloadData> payloadData = payload.GetPayloadData();
    UIManager.inst.OpenDlg("HeroCard/HeroRecruitLuckyDrawDialog", (UI.Dialog.DialogParameter) new HeroRecruitLuckyDrawDialog.Parameter()
    {
      heroPayload = payloadData[0],
      showButton = true,
      heroRecruitData = (HeroRecruitData) this
    }, true, true, true);
  }

  public override bool IsLocked
  {
    get
    {
      return false;
    }
  }

  public override string GetIcon()
  {
    return "icon_currency_premium";
  }

  public override string GetImagePath()
  {
    return "Texture/GUI_Textures/hero_card_recruit_1";
  }
}
