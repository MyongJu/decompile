﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightDungeonInventoryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonKnightDungeonInventoryPopup : Popup
{
  private List<DungenItemRender> displayList = new List<DungenItemRender>();
  private const int ITEM_COUNT = 5;
  public const string POPUP_TYPE = "DragonKnight/DragonKnightDungeonInventoryPopup";
  public DungenItemRender itemRender;
  public DungenItemInfo info;
  public UIDiv div;
  public UILabel bagCount;
  public UILabel capacityLabel;
  private DungenItemRender selectedRender;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.InitUI();
    DBManager.inst.DB_DragonKnightDungeon.onDataChanged += new System.Action<DragonKnightDungeonData>(this.OnDungeonChanged);
    DBManager.inst.DB_DragonKnightDungeon.onDataRemove += new System.Action<DragonKnightDungeonData>(this.OnDungeonChanged);
  }

  private void OnDungeonChanged(DragonKnightDungeonData dungeonData)
  {
    this.InitUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    DBManager.inst.DB_DragonKnightDungeon.onDataChanged -= new System.Action<DragonKnightDungeonData>(this.OnDungeonChanged);
    DBManager.inst.DB_DragonKnightDungeon.onDataRemove -= new System.Action<DragonKnightDungeonData>(this.OnDungeonChanged);
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void InitUI()
  {
    int bagAmount = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L).BagAmount;
    long capacity = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L).Capacity;
    this.info.gameObject.SetActive(false);
    Dictionary<int, DragonKnightDungeonData.BagInfo> bagData = DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L).BagData;
    int num = (Mathf.Max(bagAmount, bagData.Count) / 5 + 2) * 5;
    List<DragonKnightDungeonData.BagInfo> bagInfoList = new List<DragonKnightDungeonData.BagInfo>();
    using (Dictionary<int, DragonKnightDungeonData.BagInfo>.Enumerator enumerator = bagData.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, DragonKnightDungeonData.BagInfo> current = enumerator.Current;
        bagInfoList.Add(current.Value);
      }
    }
    for (int index = 0; index < num; ++index)
    {
      DragonKnightDungeonData.BagInfo bagInfo = index >= bagInfoList.Count ? (DragonKnightDungeonData.BagInfo) null : bagInfoList[index];
      DungenItemRender dungenItemRender;
      if (index >= this.displayList.Count)
      {
        GameObject gameObject = NGUITools.AddChild(this.div.gameObject, this.itemRender.gameObject);
        gameObject.SetActive(true);
        dungenItemRender = gameObject.GetComponent<DungenItemRender>();
        this.displayList.Add(dungenItemRender);
      }
      else
        dungenItemRender = this.displayList[index];
      dungenItemRender.OnSelectedItemDelegate = new System.Action<DungenItemRender>(this.OnItemClickHandler);
      DungenItemRenderData dungenItemRenderData = new DungenItemRenderData(bagInfo, index, bagInfo == null && index >= bagAmount);
      dungenItemRender.FeedData((IComponentData) dungenItemRenderData);
      dungenItemRender.Selected = false;
      dungenItemRender.index = index;
    }
    this.div.Reposition();
    int currentBagCount = DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L).CurrentBagCount;
    this.bagCount.text = Utils.FormatThousands(currentBagCount.ToString()) + "/" + Utils.FormatThousands(bagAmount.ToString());
    this.bagCount.color = currentBagCount < bagAmount ? Color.white : Color.red;
    int currentBagCapacity = DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L).CurrentBagCapacity;
    this.capacityLabel.text = Utils.FormatThousands(currentBagCapacity.ToString()) + "/" + Utils.FormatThousands(capacity.ToString());
    this.capacityLabel.color = (long) currentBagCapacity < capacity ? Color.white : Color.red;
  }

  private void OnItemClickHandler(DungenItemRender select)
  {
    if (select.Selected)
    {
      if ((UnityEngine.Object) this.selectedRender != (UnityEngine.Object) null && select.index != this.selectedRender.index)
        this.selectedRender.Selected = false;
      this.selectedRender = select;
      this.info.gameObject.SetActive(true);
      this.info.transform.SetSiblingIndex(this.displayList.Count <= 5 ? this.displayList.Count : (this.selectedRender.index / 5 + 1) * 5);
      this.info.InitUI(this.selectedRender.renderData.bagInfo, this.selectedRender.index % 5);
    }
    else
      this.info.gameObject.SetActive(false);
    this.div.Reposition();
  }
}
