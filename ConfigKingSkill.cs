﻿// Decompiled with JetBrains decompiler
// Type: ConfigKingSkill
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigKingSkill
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, KingSkillInfo> _datas;
  private Dictionary<int, KingSkillInfo> _dicByUniqueId;
  private List<KingSkillInfo> _allSkillInfo;

  public void BuildDB(object res)
  {
    this.parse.Parse<KingSkillInfo, string>(res as Hashtable, "ID", out this._datas, out this._dicByUniqueId);
    this._allSkillInfo = new List<KingSkillInfo>();
    using (Dictionary<string, KingSkillInfo>.Enumerator enumerator = this._datas.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this._allSkillInfo.Add(enumerator.Current.Value);
    }
    this._allSkillInfo.Sort((Comparison<KingSkillInfo>) ((x, y) => x.Name.CompareTo(y.Name)));
  }

  public void Clear()
  {
    if (this._datas != null)
    {
      this._datas.Clear();
      this._datas = (Dictionary<string, KingSkillInfo>) null;
    }
    if (this._dicByUniqueId != null)
    {
      this._dicByUniqueId.Clear();
      this._dicByUniqueId = (Dictionary<int, KingSkillInfo>) null;
    }
    if (this._allSkillInfo == null)
      return;
    this._allSkillInfo.Clear();
    this._allSkillInfo = (List<KingSkillInfo>) null;
  }

  public KingSkillInfo GetById(int skillId)
  {
    if (this._dicByUniqueId.ContainsKey(skillId))
      return this._dicByUniqueId[skillId];
    return (KingSkillInfo) null;
  }

  public KingSkillInfo Get(string id)
  {
    KingSkillInfo kingSkillInfo = (KingSkillInfo) null;
    this._datas.TryGetValue(id, out kingSkillInfo);
    return kingSkillInfo;
  }

  public KingSkillInfo GetByType(string type)
  {
    return this._allSkillInfo.Find((Predicate<KingSkillInfo>) (p => p.SkillType == type));
  }

  public List<KingSkillInfo> GetAll()
  {
    return this._allSkillInfo;
  }
}
