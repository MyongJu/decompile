﻿// Decompiled with JetBrains decompiler
// Type: ConfigFallenKnightScoreReq
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigFallenKnightScoreReq
{
  private List<FallenKnightScoreReqInfo> scoreReqList = new List<FallenKnightScoreReqInfo>();
  private Dictionary<string, FallenKnightScoreReqInfo> datas;
  private Dictionary<int, FallenKnightScoreReqInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<FallenKnightScoreReqInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    Dictionary<string, FallenKnightScoreReqInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.ID))
        this.scoreReqList.Add(enumerator.Current);
    }
    this.scoreReqList.Sort(new Comparison<FallenKnightScoreReqInfo>(this.SortByID));
  }

  public List<FallenKnightScoreReqInfo> GetScoreReqList()
  {
    return this.scoreReqList;
  }

  public int GetAchievedScoreInfoIndex(long selfScore, long allianceScore)
  {
    for (int index = 0; index < this.scoreReqList.Count; ++index)
    {
      if (selfScore < this.scoreReqList[index].IndividualReqScore || allianceScore < this.scoreReqList[index].AllianceReqScore)
      {
        if (index >= 1)
          return index - 1;
        return 0;
      }
    }
    return this.scoreReqList.Count - 1;
  }

  public FallenKnightScoreReqInfo GetData(int id)
  {
    for (int index = 0; index < this.scoreReqList.Count; ++index)
    {
      if (this.scoreReqList[index].ID.Equals("fallen_knight_score_req_" + (object) id))
        return this.scoreReqList[index];
    }
    return (FallenKnightScoreReqInfo) null;
  }

  public int GetScoreInfoId(FallenKnightScoreReqInfo info)
  {
    return this.GetScoreInfoId(info.ID);
  }

  public int SortByID(FallenKnightScoreReqInfo a, FallenKnightScoreReqInfo b)
  {
    return this.GetScoreInfoId(a.ID).CompareTo(this.GetScoreInfoId(b.ID));
  }

  private int GetScoreInfoId(string id)
  {
    return int.Parse(id.Replace("fallen_knight_score_req_", string.Empty));
  }
}
