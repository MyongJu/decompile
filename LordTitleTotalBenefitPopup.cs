﻿// Decompiled with JetBrains decompiler
// Type: LordTitleTotalBenefitPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class LordTitleTotalBenefitPopup : Popup
{
  private Dictionary<int, LordTitleBenefitSolt> _itemDict = new Dictionary<int, LordTitleBenefitSolt>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private GameObject _objNoBenefit;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UITable _table;
  [SerializeField]
  private GameObject _itemPrefab;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._itemPool.Initialize(this._itemPrefab, this._table.gameObject);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnCloseClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.ClearData();
    Dictionary<int, long> activatedAvatorDict = LordTitlePayload.Instance.ActivatedAvatorDict;
    Dictionary<string, float> dictionary1 = new Dictionary<string, float>();
    using (Dictionary<int, long>.Enumerator enumerator1 = activatedAvatorDict.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        KeyValuePair<int, long> current1 = enumerator1.Current;
        LordTitleMainInfo lordTitleMainInfo = ConfigManager.inst.DB_LordTitleMain.Get(current1.Key);
        if (lordTitleMainInfo != null && !LordTitlePayload.Instance.IsAvatorLocked(current1.Key) && (lordTitleMainInfo.benefits != null && lordTitleMainInfo.benefits.benefits != null))
        {
          using (Dictionary<string, float>.Enumerator enumerator2 = lordTitleMainInfo.benefits.benefits.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              KeyValuePair<string, float> current2 = enumerator2.Current;
              if (dictionary1.ContainsKey(current2.Key))
              {
                Dictionary<string, float> dictionary2;
                string key;
                (dictionary2 = dictionary1)[key = current2.Key] = dictionary2[key] + current2.Value;
              }
              else
                dictionary1.Add(current2.Key, current2.Value);
            }
          }
        }
      }
    }
    using (Dictionary<string, float>.Enumerator enumerator = dictionary1.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, float> current = enumerator.Current;
        int result = 0;
        int.TryParse(current.Key, out result);
        if (result > 0)
        {
          LordTitleBenefitSolt slot = this.GenerateSlot(result, current.Value);
          if (!this._itemDict.ContainsKey(result))
            this._itemDict.Add(result, slot);
        }
      }
    }
    NGUITools.SetActive(this._objNoBenefit, this._itemDict.Count <= 0);
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, LordTitleBenefitSolt>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this._table.repositionNow = true;
    this._table.Reposition();
    this._scrollView.ResetPosition();
  }

  private LordTitleBenefitSolt GenerateSlot(int benefitId, float benefitValue)
  {
    GameObject gameObject = this._itemPool.AddChild(this._table.gameObject);
    gameObject.SetActive(true);
    LordTitleBenefitSolt component = gameObject.GetComponent<LordTitleBenefitSolt>();
    component.SetData(benefitId, benefitValue);
    return component;
  }
}
