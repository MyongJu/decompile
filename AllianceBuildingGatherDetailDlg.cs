﻿// Decompiled with JetBrains decompiler
// Type: AllianceBuildingGatherDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Text;
using UI;

public class AllianceBuildingGatherDetailDlg : UI.Dialog
{
  public AllianceBuildingConstructInfo info;
  public AllianceBuildingConstructSlotContainer container;
  public UILabel title;
  public UILabel hint;
  public UIWidget m_FocusTarget;
  public UIButton demolishBtn;
  private Coordinate allianceBuildingCoor;
  private int allianceBuildingConfigId;
  private bool isDestroy;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    AllianceBuildingGatherDetailDlg.Parameter parameter = orgParam as AllianceBuildingGatherDetailDlg.Parameter;
    if (parameter != null)
    {
      this.allianceBuildingCoor = parameter.cor;
      this.allianceBuildingConfigId = parameter.buildingConfigId;
    }
    PVPSystem.Instance.Map.FocusTile(PVPMapData.MapData.ConvertTileKXYToWorldPosition(this.allianceBuildingCoor), this.m_FocusTarget, 0.0f);
    UIManager.inst.CloseKingdomTouchCircle();
    this.UpdateUI();
    this.AddEventHandler();
  }

  private void OnDestroy()
  {
    this.isDestroy = true;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Clear();
    this.RemoveEventHandler();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void Clear()
  {
    this.info.Clear();
    this.container.Clear();
  }

  private void UpdateUI()
  {
    if (this.isDestroy)
      return;
    this.UpdateInfoData();
    this.container.SetData(AllianceBuildUtils.GetAllianceBuildingMarchData(TileType.AllianceSuperMine, this.allianceBuildingCoor), false);
    this.title.text = this.GetTitle();
    this.hint.text = Utils.XLAT("alliance_resource_building_gather_speed_tip");
    this.UpdateDemolishBtnState();
  }

  private void UpdateInfoData()
  {
    int num = 0;
    TileType tileType = TileType.AllianceSuperMine;
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(this.allianceBuildingCoor);
    DBManager.inst.DB_March.Get(referenceAt.MarchID);
    switch (tileType)
    {
      case TileType.AllianceWarehouse:
        num = referenceAt.WareHouseData.ConfigId;
        break;
      case TileType.AllianceSuperMine:
        num = referenceAt.SuperMineData.ConfigId;
        break;
    }
    long gatheredResources = AllianceBuildUtils.GetSuperMineGatheredResources(this.allianceBuildingConfigId);
    string buildingStateString = AllianceBuildUtils.GetAllianceBuildingStateString(this.allianceBuildingCoor, tileType);
    string str1 = Utils.FormatThousands(AllianceBuildUtils.GetSuperMineRemainingResources(this.allianceBuildingConfigId).ToString());
    string mineGatheringSpeed = AllianceBuildUtils.GetSuperMineGatheringSpeed(this.allianceBuildingConfigId, this.allianceBuildingCoor);
    AllianceBuildingConstructInfo.InfoData infodata = new AllianceBuildingConstructInfo.InfoData();
    infodata.content.Add(buildingStateString);
    infodata.content.Add(str1);
    infodata.content.Add(mineGatheringSpeed);
    if (gatheredResources >= 0L)
    {
      infodata.showtime = true;
      infodata.updateTime = false;
      string str2 = Utils.FormatThousands(gatheredResources.ToString());
      infodata.content.Add(str2);
    }
    else
      infodata.showtime = false;
    this.info.SeedData(infodata);
  }

  private void UpdateInfoEachSec(int time)
  {
    this.UpdateInfoData();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_AllianceSuperMine.onDataUpdate += new System.Action<AllianceSuperMineData>(this.OnSuperMineUpdated);
    DBManager.inst.DB_AllianceSuperMine.onDataRemove += new System.Action<AllianceSuperMineData>(this.OnSuperMineRemoved);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.UpdateInfoEachSec);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_AllianceSuperMine.onDataUpdate -= new System.Action<AllianceSuperMineData>(this.OnSuperMineUpdated);
    DBManager.inst.DB_AllianceSuperMine.onDataRemove -= new System.Action<AllianceSuperMineData>(this.OnSuperMineRemoved);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.UpdateInfoEachSec);
  }

  private void OnSuperMineRemoved(AllianceSuperMineData obj)
  {
    if (this.allianceBuildingConfigId != obj.ConfigId)
      return;
    this.OnCloseHandler();
  }

  private void OnSuperMineUpdated(AllianceSuperMineData obj)
  {
    if (this.allianceBuildingConfigId != obj.ConfigId)
      return;
    this.UpdateUI();
  }

  public void OnDemolishBtnClicked()
  {
    string content = string.Format(ScriptLocalization.Get("alliance_buildings_confirm_demolish_description", true), (object) ScriptLocalization.Get(AllianceBuildUtils.GetAllianceBuildingName(TileType.AllianceSuperMine, this.allianceBuildingCoor), true));
    string title = ScriptLocalization.Get("alliance_buildings_confirm_placement_button", true);
    string left = ScriptLocalization.Get("id_uppercase_confirm", true);
    string right = ScriptLocalization.Get("id_uppercase_cancel", true);
    UIManager.inst.ShowConfirmationBox(title, content, left, right, ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.ConfirmDemolish), (System.Action) null, (System.Action) null);
  }

  private string GetTitle()
  {
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
    StringBuilder stringBuilder = new StringBuilder();
    if (allianceData != null)
      stringBuilder.Append("(" + allianceData.allianceAcronym + ")");
    string allianceBuildingName = AllianceBuildUtils.GetAllianceBuildingName(TileType.AllianceSuperMine, this.allianceBuildingCoor);
    stringBuilder.Append(allianceBuildingName);
    return stringBuilder.ToString();
  }

  private void UpdateDemolishBtnState()
  {
    this.demolishBtn.isEnabled = Utils.IsR5Member() || Utils.IsR4Member();
  }

  private void ConfirmDemolish()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "k"] = (object) this.allianceBuildingCoor.K;
    postData[(object) "x"] = (object) this.allianceBuildingCoor.X;
    postData[(object) "y"] = (object) this.allianceBuildingCoor.Y;
    MessageHub.inst.GetPortByAction("Map:takeBackAllianceBuilding").SendRequest(postData, (System.Action<bool, object>) null, true);
    this.OnCloseButtonPress();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public Coordinate cor;
    public int buildingConfigId;
  }
}
