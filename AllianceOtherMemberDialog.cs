﻿// Decompiled with JetBrains decompiler
// Type: AllianceOtherMemberDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceOtherMemberDialog : UI.Dialog
{
  private List<IAllianceRankHeader> m_HeaderList = new List<IAllianceRankHeader>();
  public UIViewport m_Viewport;
  public UIScrollView m_ScrollView;
  public UITable m_Table;
  public GameObject m_HeaderPrefab;
  private AllianceData m_AllianceData;
  private bool m_Nominate;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (this.m_AllianceData == null)
    {
      AllianceOtherMemberDialog.Parameter parameter = orgParam as AllianceOtherMemberDialog.Parameter;
      this.m_AllianceData = parameter.allianceData;
      this.m_Nominate = parameter.nominate;
      Hashtable postData = new Hashtable();
      postData[(object) "alliance_id"] = (object) this.m_AllianceData.allianceId;
      MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((_param1, _param2) => this.InitializePage()), true);
    }
    MessageHub.inst.GetPortByAction("wonder:appointKing").AddEvent(new System.Action<object>(this.OnKingAppointed));
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    MessageHub.inst.GetPortByAction("wonder:appointKing").RemoveEvent(new System.Action<object>(this.OnKingAppointed));
  }

  private void OnKingAppointed(object param)
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void ClearData()
  {
    using (List<IAllianceRankHeader>.Enumerator enumerator = this.m_HeaderList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IAllianceRankHeader current = enumerator.Current;
        current.gameObject.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_HeaderList.Clear();
  }

  private void InitializePage()
  {
    this.ClearData();
    this.m_Viewport.sourceCamera = UIManager.inst.ui2DCamera;
    this.m_Viewport.gameObject.SetActive(true);
    this.AddRankHeader(this.m_AllianceData, 6, true);
    this.AddRankHeader(this.m_AllianceData, 4, false);
    this.AddRankHeader(this.m_AllianceData, 3, false);
    this.AddRankHeader(this.m_AllianceData, 2, false);
    this.AddRankHeader(this.m_AllianceData, 1, false);
    this.m_Table.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  private void AddRankHeader(AllianceData allianceData, int title, bool expand)
  {
    GameObject gameObject = Utils.DuplicateGOB(this.m_HeaderPrefab, this.m_Table.transform);
    gameObject.SetActive(true);
    IAllianceRankHeader component = gameObject.GetComponent(typeof (IAllianceRankHeader)) as IAllianceRankHeader;
    component.SetNominate(this.m_Nominate);
    component.SetData(allianceData, title, expand, new System.Action(this.OnHeaderClicked));
    this.m_HeaderList.Add(component);
  }

  private void OnHeaderClicked()
  {
    this.m_Table.Reposition();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public AllianceData allianceData;
    public bool nominate;
  }
}
