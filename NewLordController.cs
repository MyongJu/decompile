﻿// Decompiled with JetBrains decompiler
// Type: NewLordController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class NewLordController : AnniversaryOtherController
{
  private const string MALE_PATH = "Texture/Hero/player_portrait_9";
  private const string FEMALE_PATH = "Texture/Hero/player_portrait_10";
  private const string NEW_CASTLE = "Texture/SuperLogin/Castle/";
  private const string PACKAGE_ID_TO_BE_FOUCUSED = "iap_package_anniversary_portrait_male_2";
  public UITexture malePortrait;
  public UITexture femalePortrait;
  public UITexture newCastle;

  public override void UpdateUI()
  {
    this.UpdatePageContent();
    this.LoadLordPortrait();
  }

  public override void UpdatePageContent()
  {
    base.UpdatePageContent();
    this.eventDescription.text = Utils.XLAT("item_portrait_anniversary_description");
    this.buttonLabel.text = Utils.XLAT("id_uppercase_buy");
  }

  private void LoadLordPortrait()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.malePortrait, "Texture/Hero/player_portrait_9", (System.Action<bool>) null, true, false, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.femalePortrait, "Texture/Hero/player_portrait_10", (System.Action<bool>) null, true, false, string.Empty);
    this.LoadCastleImage();
  }

  private void LoadCastleImage()
  {
    AnniversaryStrongHoldDisplayInfo strongHoldDisplayInfo = ConfigManager.inst.DB_AnniversaryStrongHoldDisplay.Get(CityManager.inst.mStronghold.mLevel.ToString());
    if (strongHoldDisplayInfo == null)
      return;
    string image = strongHoldDisplayInfo.image;
    if (string.IsNullOrEmpty(image))
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.newCastle, "Texture/SuperLogin/Castle/" + image, (System.Action<bool>) null, true, false, string.Empty);
  }

  public override void OnBtnClicked()
  {
    List<IAPStorePackage> availablePackages = IAPStorePackagePayload.Instance.GetAvailablePackages();
    IAPPackageGroupInfo packageGroupInfo = (IAPPackageGroupInfo) null;
    if (availablePackages == null || availablePackages.Count <= 0)
      return;
    for (int index = 0; index < availablePackages.Count; ++index)
    {
      if (availablePackages[index].package.id == "iap_package_anniversary_portrait_male_2")
      {
        packageGroupInfo = availablePackages[index].group;
        break;
      }
    }
    if (packageGroupInfo == null)
      Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
    else
      Utils.ShowIAPStore((UI.Dialog.DialogParameter) new IAPStoreDialog.Parameter()
      {
        groupId = packageGroupInfo.internalId.ToString()
      });
  }

  public void Show()
  {
    NGUITools.SetActive(this.gameObject, true);
    this.UpdateUI();
  }

  public void Hide()
  {
    NGUITools.SetActive(this.gameObject, false);
  }
}
