﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightPacketHit
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DragonKnightPacketHit : DragonKnightPacket
{
  public DragonKnightPacketHit(long playerId)
  {
    this.Type = DragonKnightPacket.PacketType.Hit;
    this.PlayerId = playerId;
  }
}
