﻿// Decompiled with JetBrains decompiler
// Type: DevSettingsManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;

public class DevSettingsManager
{
  [DevSetting(null, null, "settingsChangedCB")]
  public static int testIntVal = 10;
  [DevSetting("Test my magic float", null, null)]
  public static float testFloat = 3.14f;
  [DevSetting(null, null, null)]
  public static string testString = "Hello! I must be going";
  private const string defaultDataPath = "Assets/__RawArtData/TextAsset/DevSettings.json";
  public Dictionary<string, Dictionary<string, DevSettingsManager.Entry>> entryTable;
  private static DevSettingsManager inst;
  [DevSetting("Test boolean toggle", null, "settingsChangedCB")]
  public static bool testBooleanToggle;
  [DevSetting("Test Enum", null, "settingsChangedCB")]
  public static DevSettingsManager.TestEnumGameType testEnum;

  private DevSettingsManager()
  {
    this.Reset();
  }

  public static DevSettingsManager Instance
  {
    get
    {
      if (DevSettingsManager.inst == null)
        DevSettingsManager.inst = new DevSettingsManager();
      return DevSettingsManager.inst;
    }
    private set
    {
      DevSettingsManager.inst = value;
    }
  }

  public void PopulateValues(bool readFromDefaultFile = true)
  {
    if (readFromDefaultFile)
      this.ReadFromFile((string) null);
    using (Dictionary<string, Dictionary<string, DevSettingsManager.Entry>>.Enumerator enumerator1 = this.entryTable.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (Dictionary<string, DevSettingsManager.Entry>.Enumerator enumerator2 = enumerator1.Current.Value.GetEnumerator())
        {
          while (enumerator2.MoveNext())
            enumerator2.Current.Value.PokeStaticValue();
        }
      }
    }
  }

  public void Reset()
  {
    this.BuildEntryTable();
  }

  public void WriteToFile(string path = null)
  {
    if (path == null)
      path = "Assets/__RawArtData/TextAsset/DevSettings.json";
    Dictionary<string, Dictionary<string, string>> dictionary1 = new Dictionary<string, Dictionary<string, string>>();
    using (Dictionary<string, Dictionary<string, DevSettingsManager.Entry>>.Enumerator enumerator1 = this.entryTable.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        KeyValuePair<string, Dictionary<string, DevSettingsManager.Entry>> current1 = enumerator1.Current;
        Dictionary<string, string> dictionary2 = new Dictionary<string, string>();
        dictionary1[current1.Key] = dictionary2;
        using (Dictionary<string, DevSettingsManager.Entry>.Enumerator enumerator2 = current1.Value.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            KeyValuePair<string, DevSettingsManager.Entry> current2 = enumerator2.Current;
            dictionary2[current2.Key] = current2.Value.WriteToString();
          }
        }
      }
    }
    string str = JsonWriter.Serialize((object) dictionary1);
    using (StreamWriter streamWriter = new StreamWriter(path))
      streamWriter.Write(str);
  }

  public void ReadFromFile(string path = null)
  {
    if (path == null)
      path = "Assets/__RawArtData/TextAsset/DevSettings.json";
    if (!File.Exists(path))
      return;
    string str = (string) null;
    using (StreamReader streamReader = new StreamReader(path))
      str = streamReader.ReadToEnd();
    using (Dictionary<string, Dictionary<string, string>>.Enumerator enumerator1 = JsonReader.Deserialize<Dictionary<string, Dictionary<string, string>>>(str).GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        KeyValuePair<string, Dictionary<string, string>> current1 = enumerator1.Current;
        if (this.entryTable.ContainsKey(current1.Key))
        {
          Dictionary<string, DevSettingsManager.Entry> dictionary = this.entryTable[current1.Key];
          using (Dictionary<string, string>.Enumerator enumerator2 = current1.Value.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              KeyValuePair<string, string> current2 = enumerator2.Current;
              if (dictionary.ContainsKey(current2.Key))
                dictionary[current2.Key].SetFromString(current2.Value);
            }
          }
        }
      }
    }
  }

  private Dictionary<string, DevSettingsManager.Entry> FindOrCreateCategory(string cat)
  {
    if (this.entryTable.ContainsKey(cat))
      return this.entryTable[cat];
    Dictionary<string, DevSettingsManager.Entry> dictionary = new Dictionary<string, DevSettingsManager.Entry>();
    this.entryTable[cat] = dictionary;
    return dictionary;
  }

  private void BuildEntryTable()
  {
    this.entryTable = new Dictionary<string, Dictionary<string, DevSettingsManager.Entry>>();
    foreach (System.Type type in Assembly.GetExecutingAssembly().GetTypes())
    {
      foreach (FieldInfo field in type.GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
      {
        object[] customAttributes = field.GetCustomAttributes(typeof (DevSetting), true);
        if (customAttributes.Length > 0)
        {
          foreach (object obj in customAttributes)
          {
            DevSetting devSetting = obj as DevSetting;
            if (!field.IsStatic)
            {
              Debug.LogError((object) "DevSettings only apply to static members");
            }
            else
            {
              DevSettingsManager.Entry entry;
              if (field.FieldType == typeof (bool))
                entry = (DevSettingsManager.Entry) new DevSettingsManager.Entry_Toggle();
              else if (field.FieldType == typeof (int))
                entry = (DevSettingsManager.Entry) new DevSettingsManager.Entry_Int();
              else if (field.FieldType == typeof (float))
                entry = (DevSettingsManager.Entry) new DevSettingsManager.Entry_Float();
              else if (field.FieldType == typeof (string))
                entry = (DevSettingsManager.Entry) new DevSettingsManager.Entry_String();
              else if (field.FieldType.IsEnum)
              {
                entry = (DevSettingsManager.Entry) new DevSettingsManager.Entry_Enum(field.FieldType);
              }
              else
              {
                Debug.LogError((object) ("DevSetting attribute on invalid type. Field= " + field.Name));
                continue;
              }
              if (string.IsNullOrEmpty(devSetting.description))
                devSetting.description = field.Name;
              if (string.IsNullOrEmpty(devSetting.category))
                devSetting.category = type.Name;
              entry.description = devSetting.description;
              entry.name = field.Name;
              entry.field = field;
              if (!string.IsNullOrEmpty(devSetting.onChangedFuncName))
              {
                MethodInfo method = type.GetMethod(devSetting.onChangedFuncName, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                if (method == null)
                  Debug.LogError((object) "On changed callback not found");
                else
                  entry.onChanged += (DevSettingsManager.Entry.ChangedHandler) Delegate.CreateDelegate(typeof (DevSettingsManager.Entry.ChangedHandler), method);
              }
              this.FindOrCreateCategory(devSetting.category)[entry.name] = entry;
            }
          }
        }
      }
    }
  }

  private static void settingsChangedCB(string which)
  {
    if (which == "testBooleanToggle" || which == "testIntVal" || (which == "testFloat" || !(which == "testString")))
      ;
  }

  public abstract class Entry
  {
    public string description;
    public string name;
    public string category;
    public FieldInfo field;

    public event DevSettingsManager.Entry.ChangedHandler onChanged;

    public abstract void SetFromString(string s);

    public abstract string WriteToString();

    public abstract void PokeStaticValue();

    public void AlertChanged()
    {
      this.PokeStaticValue();
      if (this.onChanged != null)
        this.onChanged(this.name);
      DevSettingsManager.Instance.WriteToFile((string) null);
    }

    public delegate void ChangedHandler(string name);
  }

  public class Entry_Toggle : DevSettingsManager.Entry
  {
    public bool _val;

    public bool val
    {
      get
      {
        return this._val;
      }
      set
      {
        if (this._val == value)
          return;
        this._val = value;
        this.AlertChanged();
      }
    }

    public override void PokeStaticValue()
    {
      this.field.SetValue((object) null, (object) this.val);
    }

    public override void SetFromString(string s)
    {
      switch (s.Trim().ToUpper()[0])
      {
        case '1':
        case 'T':
        case 'Y':
          this.val = true;
          break;
        default:
          this.val = false;
          break;
      }
    }

    public override string WriteToString()
    {
      return this.val ? "TRUE" : "FALSE";
    }
  }

  public class Entry_Int : DevSettingsManager.Entry
  {
    public int _val;

    public int val
    {
      get
      {
        return this._val;
      }
      set
      {
        if (this._val == value)
          return;
        this._val = value;
        this.AlertChanged();
      }
    }

    public override void PokeStaticValue()
    {
      this.field.SetValue((object) null, (object) this.val);
    }

    public override void SetFromString(string s)
    {
      this.val = int.Parse(s);
    }

    public override string WriteToString()
    {
      return this.val.ToString();
    }
  }

  public class Entry_Float : DevSettingsManager.Entry
  {
    public float _val;

    public float val
    {
      get
      {
        return this._val;
      }
      set
      {
        if ((double) this._val == (double) value)
          return;
        this._val = value;
        this.AlertChanged();
      }
    }

    public override void PokeStaticValue()
    {
      this.field.SetValue((object) null, (object) this.val);
    }

    public override void SetFromString(string s)
    {
      this.val = float.Parse(s);
    }

    public override string WriteToString()
    {
      return this.val.ToString();
    }
  }

  public class Entry_String : DevSettingsManager.Entry
  {
    public string _val;

    public string val
    {
      get
      {
        return this._val;
      }
      set
      {
        if (this._val == value)
          return;
        this._val = value;
        this.AlertChanged();
      }
    }

    public override void PokeStaticValue()
    {
      this.field.SetValue((object) null, (object) this.val);
    }

    public override void SetFromString(string s)
    {
      this.val = s;
    }

    public override string WriteToString()
    {
      return this._val;
    }
  }

  public class Entry_Enum : DevSettingsManager.Entry
  {
    public string _val;
    public int _valAsInt;
    public int _index;
    public System.Type enumtype;

    public Entry_Enum(System.Type t)
    {
      this.enumtype = t;
    }

    public int GetAsInt()
    {
      return this._valAsInt;
    }

    public int GetIndex()
    {
      return this._index;
    }

    public override void PokeStaticValue()
    {
      this.field.SetValue((object) null, (object) this._valAsInt);
    }

    public void SetFromIndex(int index)
    {
      if (index < 0 || index >= Enum.GetNames(this.enumtype).Length)
        Debug.LogError((object) "Invalid index for enum");
      else
        this.val = Enum.GetNames(this.enumtype)[index];
    }

    public string val
    {
      get
      {
        return this._val;
      }
      set
      {
        if (this._val == value)
          return;
        this._val = value;
        this._index = Array.FindIndex<string>(Enum.GetNames(this.enumtype), (Predicate<string>) (item => item == this._val));
        this._valAsInt = (int) Enum.Parse(this.enumtype, this._val);
        this.AlertChanged();
      }
    }

    public override void SetFromString(string s)
    {
      this.val = s;
    }

    public override string WriteToString()
    {
      return this._val;
    }
  }

  public enum TestEnumGameType
  {
    FUN,
    WOW,
    EXCITING,
    FANTASTIC,
  }
}
