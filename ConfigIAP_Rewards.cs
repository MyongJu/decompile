﻿// Decompiled with JetBrains decompiler
// Type: ConfigIAP_Rewards
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigIAP_Rewards
{
  private Dictionary<string, IAPRewardInfo> m_DataByID;
  private Dictionary<int, IAPRewardInfo> m_DataByInternalID;

  public void BuildDB(object data)
  {
    new ConfigParse().Parse<IAPRewardInfo, string>(data as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
  }

  public IAPRewardInfo Get(string id)
  {
    IAPRewardInfo iapRewardInfo;
    this.m_DataByID.TryGetValue(id, out iapRewardInfo);
    return iapRewardInfo;
  }

  public IAPRewardInfo Get(int internalId)
  {
    IAPRewardInfo iapRewardInfo;
    this.m_DataByInternalID.TryGetValue(internalId, out iapRewardInfo);
    return iapRewardInfo;
  }

  public void Clear()
  {
    this.m_DataByID = (Dictionary<string, IAPRewardInfo>) null;
    this.m_DataByInternalID = (Dictionary<int, IAPRewardInfo>) null;
  }
}
