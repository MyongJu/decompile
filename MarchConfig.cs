﻿// Decompiled with JetBrains decompiler
// Type: MarchConfig
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class MarchConfig
{
  public static readonly Dictionary<MarchViewControler.TroopViewType, float> MissileLastTimeMap = new Dictionary<MarchViewControler.TroopViewType, float>()
  {
    {
      MarchViewControler.TroopViewType.class_ranged,
      1.05f
    },
    {
      MarchViewControler.TroopViewType.class_artyfar,
      1.71f
    }
  };
  public static readonly Dictionary<MarchViewControler.TroopViewType, float> MissileDelayTimeMap = new Dictionary<MarchViewControler.TroopViewType, float>()
  {
    {
      MarchViewControler.TroopViewType.class_ranged,
      0.5f
    },
    {
      MarchViewControler.TroopViewType.class_artyfar,
      0.5f
    }
  };
  public static MarchConfig.BaseInfo baseInfo = new MarchConfig.BaseInfo();
  public static MarchConfig.InfantryInfo infantryInfo = new MarchConfig.InfantryInfo();
  public static MarchConfig.CavalryInfo cavalryInfo = new MarchConfig.CavalryInfo();
  public static MarchConfig.RangeInfo rangeInfo = new MarchConfig.RangeInfo();
  public static MarchConfig.ArtyFarInfo artyFarInfo = new MarchConfig.ArtyFarInfo();
  public static MarchConfig.DragonInfo dragonInfo = new MarchConfig.DragonInfo();
  public static MarchConfig.DarkKnightInfo darkKnightInfo = new MarchConfig.DarkKnightInfo();
  public const float FORMATION_DEFAULT_LENGTH = 1.32f;
  public const float UNIT_LOCAL_MOVE_SPEED = 0.7f;
  public const float MISSLE_LAST_STEP_TIME = 0.8f;
  public const float SHOOT_RANGE = 0.2f;
  public const float Aspect = 2f;

  public class MarchInfo
  {
    public float formationLineWidth;
    public float formationLineStep;
    public float formationHeadStep;
    public float formationTailStep;
    public Vector2[] formation_move;
    public Vector3[] formation_action;
    public float unitScale_move;
    public float unitScale_action;
    public float unitSpeed;
    public int[] unitStepCount;

    public string Show()
    {
      string str1 = string.Empty + "Formation Data \n" + "Line Width : " + (object) this.formationLineWidth + "\n" + "Line Step : " + (object) this.formationLineStep + "\n" + "Head Step : " + (object) this.formationHeadStep + "\n" + "Line Tail : " + (object) this.formationTailStep + "\n" + "Foramtion (move) : \n";
      if (this.formation_move == null)
      {
        str1 += "null";
      }
      else
      {
        for (int index = 0; index < this.formation_move.Length; ++index)
          str1 = str1 + "\t" + this.formation_move[index].ToString() + "\n";
      }
      string str2 = str1 + "Formation (Action) : \n";
      if (this.formation_action == null)
      {
        str2 += "null";
      }
      else
      {
        for (int index = 0; index < this.formation_action.Length; ++index)
          str2 = str2 + "\t" + this.formation_action[index].ToString() + "\n";
      }
      string str3 = str2 + "\n" + "Unit Data \n" + "Scale (move) : " + (object) this.unitScale_move + "\n" + "Scale (action) : " + (object) this.unitScale_action + "\n" + "Speed : " + (object) this.unitSpeed + "\n" + "Step (Action) : \n";
      if (this.formation_action == null)
      {
        str3 += "null";
      }
      else
      {
        for (int index = 0; index < this.unitStepCount.Length; ++index)
          str3 = str3 + "\t" + this.unitStepCount[index].ToString() + "\n";
      }
      return str3;
    }

    public void HardCopy(MarchConfig.MarchInfo orgData)
    {
      this.formationLineWidth = orgData.formationLineWidth;
      this.formationLineStep = orgData.formationLineStep;
      this.formationHeadStep = orgData.formationHeadStep;
      this.formationTailStep = orgData.formationTailStep;
      this.formation_move = new Vector2[orgData.formation_move.Length];
      for (int index = 0; index < orgData.formation_move.Length; ++index)
        this.formation_move[index] = orgData.formation_move[index];
      this.formation_action = new Vector3[orgData.formation_action.Length];
      for (int index = 0; index < orgData.formation_action.Length; ++index)
        this.formation_action[index] = orgData.formation_action[index];
      this.unitScale_move = orgData.unitScale_move;
      this.unitScale_action = orgData.unitScale_action;
      this.unitSpeed = orgData.unitSpeed;
      this.unitStepCount = new int[orgData.unitStepCount.Length];
      for (int index = 0; index < orgData.unitStepCount.Length; ++index)
        this.unitStepCount[index] = orgData.unitStepCount[index];
    }

    public enum InfoType
    {
      Global,
      Infantry,
      Cavalry,
      Ranged,
      ArtyFar,
    }
  }

  public class ArtyFarInfo : MarchConfig.BaseInfo
  {
    public ArtyFarInfo()
    {
      this.formationLineStep = 0.26f;
      this.formationHeadStep = 0.2f;
      this.formation_move = new Vector2[3]
      {
        new Vector2(1f, 1f),
        new Vector2(1f, 2f),
        new Vector2(1f, 3f)
      };
      this.formation_action = new Vector3[4]
      {
        new Vector3(0.55f, 0.55f, 1.7f),
        new Vector3(0.0f, 0.0f, 0.0f),
        new Vector3(0.0f, 0.0f, 0.0f),
        new Vector3(0.0f, 0.0f, 0.0f)
      };
    }
  }

  public class BaseInfo : MarchConfig.MarchInfo
  {
    public BaseInfo()
    {
      this.formationLineWidth = 0.45f;
      this.formationLineStep = 0.26f;
      this.formationHeadStep = 0.0f;
      this.formationTailStep = 0.15f;
      this.formation_move = new Vector2[3]
      {
        new Vector2(1f, 4f),
        new Vector2(2f, 5f),
        new Vector2(3f, 6f)
      };
      this.formation_action = new Vector3[4]
      {
        new Vector3(0.6f, 0.0f, 0.0f),
        new Vector3(0.0f, 0.48f, 0.0f),
        new Vector3(-0.6f, 0.0f, 0.0f),
        new Vector3(0.0f, -0.33f, 0.0f)
      };
      this.unitScale_move = 0.12155f;
      this.unitScale_action = 0.09f;
      this.unitStepCount = new int[2]{ 10000, 50000 };
    }
  }

  public class CavalryInfo : MarchConfig.BaseInfo
  {
    public CavalryInfo()
    {
      this.formationLineStep = 0.495f;
      this.formationTailStep = 0.01f;
      this.formation_move = new Vector2[3]
      {
        new Vector2(1f, 3f),
        new Vector2(2f, 3f),
        new Vector2(2f, 4f)
      };
      this.formation_action = new Vector3[4]
      {
        new Vector3(0.75f, 0.0f, 0.0f),
        new Vector3(0.0f, 0.54f, 0.0f),
        new Vector3(-0.75f, 0.0f, 0.0f),
        new Vector3(0.0f, -0.44f, 0.0f)
      };
    }
  }

  public class DarkKnightInfo : MarchConfig.BaseInfo
  {
    public DarkKnightInfo()
    {
      this.formationLineWidth = 0.7f;
      this.formation_move = new Vector2[3]
      {
        new Vector2(1f, 3f),
        new Vector2(1f, 3f),
        new Vector2(1f, 3f)
      };
      this.unitScale_move = 0.8f;
      this.unitScale_action = 0.8f;
      this.unitStepCount = new int[2]{ 1, 1 };
    }
  }

  public class DragonInfo : MarchConfig.BaseInfo
  {
    public DragonInfo()
    {
      this.formation_move = new Vector2[3]
      {
        new Vector2(1f, 1f),
        new Vector2(1f, 1f),
        new Vector2(1f, 1f)
      };
      this.unitScale_move = 1f;
      this.unitScale_action = 1f;
    }
  }

  public class InfantryInfo : MarchConfig.BaseInfo
  {
    public InfantryInfo()
    {
      this.formationTailStep = 0.26f;
    }
  }

  public class RangeInfo : MarchConfig.BaseInfo
  {
    public RangeInfo()
    {
      this.formation_action = new Vector3[4]
      {
        new Vector3(0.3f, 0.3f, 1.3f),
        new Vector3(0.0f, 0.0f, 0.0f),
        new Vector3(0.0f, 0.0f, 0.0f),
        new Vector3(0.0f, 0.0f, 0.0f)
      };
    }
  }
}
