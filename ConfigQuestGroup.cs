﻿// Decompiled with JetBrains decompiler
// Type: ConfigQuestGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigQuestGroup
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, QuestGroupInfo> datas;
  private Dictionary<int, QuestGroupInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<QuestGroupInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public QuestGroupInfo GetData(int internalId)
  {
    return this.dicByUniqueId[internalId];
  }

  public QuestGroupInfo GetData(string id)
  {
    return this.datas[id];
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, QuestGroupInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, QuestGroupInfo>) null;
  }
}
