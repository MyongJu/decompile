﻿// Decompiled with JetBrains decompiler
// Type: AccountBindItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using I2.Loc;
using UI;
using UnityEngine;

public class AccountBindItem : MonoBehaviour
{
  public UISprite icon;
  public UILabel typeName;
  public UIButton unbind;
  public UIButton bind;
  public UILabel bindOrSwitch;
  private FunplusAccountType _currentType;

  public void SetData(FunplusAccountType type)
  {
    this._currentType = type;
    this.icon.spriteName = AccountManager.Instance.GetIcon(type);
    if (AccountManager.Instance.ReplaceLock())
      this.unbind.gameObject.transform.localPosition = this.bind.gameObject.transform.localPosition;
    this.RefreshButton();
    this.RemoveEventHandler();
    this.AddEventHandler();
  }

  private void RefreshButton()
  {
    string Term = "account_bind_button";
    NGUITools.SetActive(this.unbind.gameObject, false);
    NGUITools.SetActive(this.bind.gameObject, false);
    if (AccountManager.Instance.IsBind(this._currentType))
    {
      if (!AccountManager.Instance.ReplaceLock())
      {
        Term = "account_switch_binding_button";
        NGUITools.SetActive(this.bind.gameObject, true);
      }
      NGUITools.SetActive(this.unbind.gameObject, true);
    }
    else
      NGUITools.SetActive(this.bind.gameObject, true);
    this.bindOrSwitch.text = ScriptLocalization.Get(Term, true);
    if (!AccountManager.Instance.IsBind(this._currentType))
    {
      this.typeName.text = ScriptLocalization.Get("account_not_bound_status", true);
      this.typeName.color = Color.red;
    }
    else
    {
      this.typeName.text = ScriptLocalization.Get("account_bound_status", true);
      this.typeName.color = Color.green;
    }
  }

  public void OnUnBind()
  {
    if (AccountManager.Instance.IsLastBind(this._currentType))
      UIManager.inst.ShowConfirmationBox(ScriptLocalization.Get("account_warning_title", true), ScriptLocalization.Get("account_cannot_unbind_status", true), ScriptLocalization.Get("id_uppercase_confirm", true), ScriptLocalization.Get("id_uppercase_cancel", true), ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) null, (System.Action) null, (System.Action) null);
    else
      this.ShowWarning();
  }

  private void ShowWarning()
  {
    UIManager.inst.ShowConfirmationBox(ScriptLocalization.Get("account_warning_title", true), ScriptLocalization.Get("account_warning_unbind_description", true), ScriptLocalization.Get("id_uppercase_cancel", true), ScriptLocalization.Get("id_uppercase_confirm", true), ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, (System.Action) null, new System.Action(this.UnBindAccount), (System.Action) null);
  }

  private void UnBindAccount()
  {
    AccountManager.Instance.UnBindAccount(this._currentType);
  }

  public void OnBindOrReplace()
  {
    if (AccountManager.Instance.IsBind(this._currentType))
    {
      string title = ScriptLocalization.Get("account_warning_title", true);
      string content = ScriptLocalization.Get("account_switch_binding_notice", true);
      string right = ScriptLocalization.Get("id_uppercase_confirm", true);
      string left = ScriptLocalization.Get("id_uppercase_cancel", true);
      UIManager.inst.ShowConfirmationBox(title, content, left, right, ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, (System.Action) null, new System.Action(this.Bind), (System.Action) null);
    }
    else
      this.Bind();
  }

  private void Bind()
  {
    AccountManager.Instance.BindAccount(this._currentType);
  }

  public void Clear()
  {
    this.RemoveEventHandler();
  }

  private void OnBindAccountSuccessHandler()
  {
    this.RefreshButton();
  }

  private void AddEventHandler()
  {
    AccountManager.Instance.OnBindAccountSuccess += new System.Action(this.OnBindAccountSuccessHandler);
    AccountManager.Instance.OnUnBindAccountSuccess += new System.Action(this.OnBindAccountSuccessHandler);
  }

  private void RemoveEventHandler()
  {
    AccountManager.Instance.OnBindAccountSuccess -= new System.Action(this.OnBindAccountSuccessHandler);
    AccountManager.Instance.OnUnBindAccountSuccess -= new System.Action(this.OnBindAccountSuccessHandler);
  }
}
