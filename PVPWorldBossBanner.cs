﻿// Decompiled with JetBrains decompiler
// Type: PVPWorldBossBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class PVPWorldBossBanner : SpriteBanner
{
  [SerializeField]
  protected TextMesh m_bossName;
  [SerializeField]
  protected TextMesh m_bossLevel;
  [SerializeField]
  protected UISpriteMesh m_circle;

  public override void UpdateUI(TileData tile)
  {
    base.UpdateUI(tile);
    WorldBossData worldBossData = tile.WorldBossData;
    if (worldBossData == null)
      return;
    WorldBossStaticInfo data = ConfigManager.inst.DB_WorldBoss.GetData(worldBossData.configId);
    if (data != null)
    {
      this.m_bossName.text = data.LocalName;
      this.m_bossLevel.text = data.Level.ToString();
    }
    if (!(bool) ((Object) this.m_bossName))
      return;
    Bounds bounds = this.m_bossName.GetComponent<Renderer>().bounds;
    Vector3 position = new Vector3(bounds.min.x, bounds.center.y, bounds.center.z);
    position = this.m_bossName.transform.InverseTransformPoint(position);
    position.x -= 0.5f * (float) this.m_circle.width;
    this.m_circle.transform.localPosition = position;
  }
}
