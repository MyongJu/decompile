﻿// Decompiled with JetBrains decompiler
// Type: PersonalHonorRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PersonalHonorRender : MonoBehaviour
{
  public UITexture honorImage;
  public UILabel honorCount;
  private int count;

  public void FeedData(int count)
  {
    this.count = count;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.honorImage, "Texture/Alliance/alliance_honor", (System.Action<bool>) null, true, false, string.Empty);
    this.honorCount.text = this.count.ToString();
  }
}
