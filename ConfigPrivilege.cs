﻿// Decompiled with JetBrains decompiler
// Type: ConfigPrivilege
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigPrivilege
{
  private Dictionary<string, PrivilegeInfo> _dataById;
  private Dictionary<int, PrivilegeInfo> _dataByInternalId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<PrivilegeInfo, string>(res as Hashtable, "id", out this._dataById, out this._dataByInternalId);
  }

  public void Clear()
  {
    if (this._dataById != null)
      this._dataById.Clear();
    if (this._dataByInternalId == null)
      return;
    this._dataByInternalId.Clear();
  }

  public PrivilegeInfo GetData(string id)
  {
    if (this._dataById.ContainsKey(id))
      return this._dataById[id];
    return (PrivilegeInfo) null;
  }

  public PrivilegeInfo GetData(int internalId)
  {
    if (this._dataByInternalId.ContainsKey(internalId))
      return this._dataByInternalId[internalId];
    return (PrivilegeInfo) null;
  }
}
