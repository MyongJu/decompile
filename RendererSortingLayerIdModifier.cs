﻿// Decompiled with JetBrains decompiler
// Type: RendererSortingLayerIdModifier
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class RendererSortingLayerIdModifier : MonoBehaviour
{
  [SerializeField]
  private string m_SortingLayerName = "Default";
  private Renderer[] m_Renderers;

  private void Start()
  {
    this.m_Renderers = this.GetComponentsInChildren<Renderer>();
  }

  private void Update()
  {
    int length = this.m_Renderers.Length;
    for (int index = 0; index < length; ++index)
      this.m_Renderers[index].sortingLayerName = this.m_SortingLayerName;
  }

  public string SortingLayerName
  {
    get
    {
      return this.m_SortingLayerName;
    }
    set
    {
      this.m_SortingLayerName = value;
    }
  }
}
