﻿// Decompiled with JetBrains decompiler
// Type: ParliamentHeroItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class ParliamentHeroItem : MonoBehaviour
{
  private List<UISprite> _AllStar = new List<UISprite>();
  private const string StarSpriteHighlight = "icon_mail_star_01";
  private const string StarSpriteGrey = "hero_card_star";
  [SerializeField]
  private UISprite _StarTemplate;
  [SerializeField]
  private UITable _StarContainer;
  [SerializeField]
  private GameObject _RootFragmentInfo;
  [SerializeField]
  private UILabel _LabelFragmentCount;
  [SerializeField]
  private UILabel _LabelLevel;
  [SerializeField]
  private UISprite _SpriteQuality;
  [SerializeField]
  private UITexture _TextureHero;
  [SerializeField]
  private GameObject _RootNew;
  [SerializeField]
  private GameObject _RootAssigned;
  [SerializeField]
  private UIDragScrollView _DragScrollView;
  [SerializeField]
  private GameObject[] _AllQualityGreyTag;
  [SerializeField]
  private GameObject _Lv;
  public System.Action OnClickdCallback;

  protected UISprite CreateStar()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._StarTemplate.gameObject);
    gameObject.transform.SetParent(this._StarContainer.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    UISprite component = gameObject.GetComponent<UISprite>();
    component.color = Color.white;
    this._AllStar.Add(component);
    return component;
  }

  protected void DestroyAllStar()
  {
    using (List<UISprite>.Enumerator enumerator = this._AllStar.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UISprite current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
        {
          current.transform.parent = (Transform) null;
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
        }
      }
    }
    this._AllStar.Clear();
  }

  protected void Reset()
  {
    if (this._AllQualityGreyTag != null)
    {
      foreach (GameObject gameObject in this._AllQualityGreyTag)
        gameObject.SetActive(false);
    }
    this._StarTemplate.gameObject.SetActive(false);
    this._RootFragmentInfo.SetActive(false);
    this._RootAssigned.SetActive(false);
    this._LabelLevel.text = string.Empty;
    this.DestroyAllStar();
  }

  protected void SetStarFrameStatus(bool show)
  {
    Transform transform = this.transform.Find("Star/StarBase");
    if (!(bool) ((UnityEngine.Object) transform))
      return;
    NGUITools.SetActive(transform.gameObject, show);
  }

  protected void FillData(LegendCardData legendCardData)
  {
    if (legendCardData == null)
    {
      D.error((object) "LegendCardData legendCardData is null");
    }
    else
    {
      this._LabelLevel.text = ScriptLocalization.Get("id_lv", true) + legendCardData.Level.ToString();
      ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(legendCardData.LegendId);
      if (parliamentHeroInfo == null)
      {
        D.error((object) "cant get ParliamentHeroInfo, legendId={0}", (object) legendCardData.LegendId);
      }
      else
      {
        this.FillData(parliamentHeroInfo, false);
        for (int index = 0; index < legendCardData.Star && index < this._AllStar.Count; ++index)
          this._AllStar[index].spriteName = "icon_mail_star_01";
      }
    }
  }

  protected void FillData(ParliamentHeroInfo parliamentHeroInfo, bool showFullStar = false)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this._TextureHero, parliamentHeroInfo.SmallIcon, (System.Action<bool>) null, true, false, string.Empty);
    this._SpriteQuality.color = parliamentHeroInfo.QualityColor;
    ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(parliamentHeroInfo.quality.ToString());
    if (parliamentHeroQualityInfo != null)
    {
      for (int index = 0; index < parliamentHeroQualityInfo.maxStar; ++index)
        this.CreateStar().spriteName = index < parliamentHeroQualityInfo.initialStar || showFullStar ? "icon_mail_star_01" : "hero_card_star";
      this._StarContainer.Reposition();
    }
    this._RootNew.SetActive(parliamentHeroInfo.IsNew);
  }

  protected void FillFragmentData(ParliamentHeroInfo parliamentHeroInfo)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this._TextureHero, parliamentHeroInfo.SmallIcon, (System.Action<bool>) null, true, false, string.Empty);
    this.SetStarFrameStatus(false);
    this._SpriteQuality.color = parliamentHeroInfo.QualityColor;
    this._RootAssigned.SetActive(false);
    this._RootNew.SetActive(parliamentHeroInfo.IsNew);
    this._RootFragmentInfo.SetActive(true);
    this._LabelFragmentCount.text = parliamentHeroInfo.ChipCount.ToString();
  }

  public void OnClicked()
  {
    if (this.OnClickdCallback == null)
      return;
    this.OnClickdCallback();
  }

  public void SetData(LegendCardData legendCardData)
  {
    this.Reset();
    if (legendCardData == null)
      D.error((object) "ParliamentHeroCard::SetData(LegendCardData:null)");
    else
      this.FillData(legendCardData);
  }

  public void SetData(LegendCardData legendCardData, bool showHeroStar, bool showHotNew)
  {
    this.SetData(legendCardData);
    if (!showHeroStar)
      this.DestroyAllStar();
    this.SetStarFrameStatus(showHeroStar);
    if (legendCardData == null)
      return;
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(legendCardData.LegendId);
    if (parliamentHeroInfo == null)
      return;
    this._RootNew.SetActive(parliamentHeroInfo.IsNew && showHotNew);
  }

  public void SetData(LegendCardData legendCardData, bool showAssignedStatus)
  {
    this.SetData(legendCardData, false, false);
    this._RootAssigned.SetActive(legendCardData != null && legendCardData.PositionAssigned && showAssignedStatus);
  }

  public void SetData(ParliamentHeroInfo parliamentHeroInfo, bool showFullStar = false)
  {
    this.Reset();
    if (parliamentHeroInfo == null)
      D.error((object) "ParliamentHeroCard::SetData(ParliamentHeroInfo:null)");
    else
      this.FillData(parliamentHeroInfo, showFullStar);
  }

  public void SetData(ParliamentHeroInfo parliamentHeroInfo, bool showHeroStar, bool showHotNew)
  {
    this.SetData(parliamentHeroInfo, false);
    if (!showHeroStar)
      this.DestroyAllStar();
    this.SetStarFrameStatus(showHeroStar);
    if (parliamentHeroInfo == null)
      return;
    this._RootNew.SetActive(parliamentHeroInfo.IsNew && showHotNew);
  }

  public void SetGrey(int quality, bool grey)
  {
    if (this._AllQualityGreyTag == null || this._AllQualityGreyTag.Length < quality)
      return;
    this._AllQualityGreyTag[quality - 1].SetActive(grey);
  }

  public void SetFragmentData(ParliamentHeroInfo parliamentHeroInfo, bool showHotNew = true)
  {
    this.Reset();
    if (parliamentHeroInfo == null)
      return;
    this.FillFragmentData(parliamentHeroInfo);
    this._RootNew.SetActive(parliamentHeroInfo.IsNew && showHotNew);
  }

  public void SetDragScrollView(UIScrollView scrollView)
  {
    if (!(bool) ((UnityEngine.Object) this._DragScrollView))
      return;
    this._DragScrollView.scrollView = scrollView;
  }

  public void ShowlabelLevel(bool need = false)
  {
    this._Lv.SetActive(need);
  }
}
