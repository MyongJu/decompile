﻿// Decompiled with JetBrains decompiler
// Type: UICompnent.RequireComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

namespace UICompnent
{
  public class RequireComponent : ComponentRenderBase
  {
    public GameObject mQueueFullReqBtn;
    public GameObject mBuildingReqBtn;
    public GameObject mWoodReqBtn;
    public GameObject mFoodReqBtn;
    public GameObject mSilverReqBtn;
    public GameObject mOreReqBtn;
    public GameObject mItemReqBtn;
    public GameObject mSteelReqBtn;
    public GameObject mBuildLimitReqBtn;
    public GameObject mRewardHeader;
    public GameObject mRewardHeroXP;
    public GameObject mRewardPower;
    public GameObject mRewardDefault;
    public GameObject mScrollReqBtn;
    public UIGrid grid;

    public override void Init()
    {
      RequireComponentData data1 = this.data as RequireComponentData;
      this.ClearGrid();
      List<RequireComponentData.RequireElement> requires = data1.GetRequires();
      for (int index = 0; index < requires.Count; ++index)
      {
        switch (requires[index].type)
        {
          case RequireComponentData.Requirement.ITEM:
            int internalId1 = requires[index].internalID;
            ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId1);
            if (itemStaticInfo != null)
            {
              int itemCount = ItemBag.Instance.GetItemCount(internalId1);
              int need = (int) requires[index].need;
              this.AddRequirement(RequireComponentData.Requirement.ITEM, itemStaticInfo.ImagePath, internalId1.ToString(), (double) itemCount, (double) need, new EventDelegate(new EventDelegate.Callback(this.GetMoreOfItemPressed)), requires[index].showOwnMessage);
              break;
            }
            break;
          case RequireComponentData.Requirement.QUEUE:
            this.AddRequirement(RequireComponentData.Requirement.QUEUE, "Texture/BuildingConstruction/item_speed_ups", (string) null, (double) EquipmentManager.Instance.GetOwnForgeQueue(data1.GetBagType()), (double) (int) requires[index].need, new EventDelegate(new EventDelegate.Callback(this.OnGetMoreSteelBtnPressed)), requires[index].showOwnMessage);
            break;
          case RequireComponentData.Requirement.STEEL:
            this.AddRequirement(RequireComponentData.Requirement.STEEL, "Texture/BuildingConstruction/steel_icon", (string) null, (double) (int) PlayerData.inst.userData.currency.steel, (double) requires[index].need, new EventDelegate(new EventDelegate.Callback(this.OnGetMoreSteelBtnPressed)), requires[index].showOwnMessage);
            break;
          case RequireComponentData.Requirement.SCROLL:
            int internalId2 = requires[index].internalID;
            ConfigEquipmentScrollInfo data2 = ConfigManager.inst.GetEquipmentScroll(data1.GetBagType()).GetData(internalId2);
            if (data2 != null)
            {
              ConfigManager.inst.DB_Items.GetItem(data2.itemID);
              int itemCount = ItemBag.Instance.GetItemCount(data2.itemID);
              int need = (int) requires[index].need;
              this.AddRequirement(RequireComponentData.Requirement.SCROLL, data2.ImagePath(data1.GetBagType()), internalId2.ToString(), (double) itemCount, (double) need, new EventDelegate(new EventDelegate.Callback(this.GetMoreOfItemPressed)), requires[index].showOwnMessage);
              break;
            }
            break;
        }
      }
      this.grid.repositionNow = true;
    }

    public override void Dispose()
    {
    }

    public GameObject AddRequirement(RequireComponentData.Requirement requirement, string icon, string name, double amount, double need, EventDelegate ed = null, bool showOwnMessage = true)
    {
      RequireComponentData data1 = this.data as RequireComponentData;
      GameObject gameObject = (GameObject) null;
      switch (requirement)
      {
        case RequireComponentData.Requirement.FOOD:
          gameObject = NGUITools.AddChild(this.grid.gameObject, this.mFoodReqBtn);
          break;
        case RequireComponentData.Requirement.WOOD:
          gameObject = NGUITools.AddChild(this.grid.gameObject, this.mWoodReqBtn);
          break;
        case RequireComponentData.Requirement.SILVER:
          gameObject = NGUITools.AddChild(this.grid.gameObject, this.mSilverReqBtn);
          break;
        case RequireComponentData.Requirement.ORE:
          gameObject = NGUITools.AddChild(this.grid.gameObject, this.mOreReqBtn);
          break;
        case RequireComponentData.Requirement.BUILDING:
          gameObject = NGUITools.AddChild(this.grid.gameObject, this.mBuildingReqBtn);
          break;
        case RequireComponentData.Requirement.ITEM:
          gameObject = NGUITools.AddChild(this.grid.gameObject, this.mItemReqBtn);
          break;
        case RequireComponentData.Requirement.QUEUE:
          gameObject = NGUITools.AddChild(this.grid.gameObject, this.mQueueFullReqBtn);
          break;
        case RequireComponentData.Requirement.LIMIT:
          gameObject = NGUITools.AddChild(this.grid.gameObject, this.mBuildLimitReqBtn);
          break;
        case RequireComponentData.Requirement.STEEL:
          gameObject = NGUITools.AddChild(this.grid.gameObject, this.mSteelReqBtn);
          break;
        case RequireComponentData.Requirement.SCROLL:
          gameObject = NGUITools.AddChild(this.grid.gameObject, this.mScrollReqBtn);
          break;
      }
      gameObject.SetActive(true);
      ConstructionRequirementBtn component1 = gameObject.GetComponent<ConstructionRequirementBtn>();
      if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
      {
        UIButton component2 = component1.mGetMoreBtn.GetComponent<UIButton>();
        component2.onClick.Clear();
        if (ed != null)
          component2.onClick.Add(ed);
      }
      string str1 = "[e71200]";
      string str2 = "[-]";
      switch (requirement)
      {
        case RequireComponentData.Requirement.ITEM:
          if (showOwnMessage)
          {
            string reqDetails;
            if (amount < need)
              reqDetails = str1 + Utils.ConvertNumberToNormalString((long) amount) + str2 + "/" + Utils.ConvertNumberToNormalString((long) need);
            else
              reqDetails = Utils.ConvertNumberToNormalString((long) amount) + "/" + Utils.ConvertNumberToNormalString((long) need);
            int num = int.Parse(name);
            ShopStaticInfo byItemInternalId = ConfigManager.inst.DB_Shop.GetShopInfoByItemInternalId(num);
            ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(num);
            component1.SetDetails(icon, reqDetails, Utils.XLAT(itemStaticInfo.Loc_Name_Id), amount >= need, true);
            component1.SetShopInfo(byItemInternalId);
            component1.SetRequirement(amount, need);
            if (amount < need)
            {
              component1.SetLabel(Utils.XLAT("construction_requirements_get_more"));
              break;
            }
            break;
          }
          string normalString1 = Utils.ConvertNumberToNormalString((long) need);
          component1.SetDetails2(icon, normalString1, string.Empty, false, false, false, string.Empty);
          break;
        case RequireComponentData.Requirement.STEEL:
          if (showOwnMessage)
          {
            string reqDetails;
            if (amount < need)
              reqDetails = str1 + Utils.ConvertNumberToNormalString((long) amount) + str2 + "/" + Utils.ConvertNumberToNormalString((long) need);
            else
              reqDetails = Utils.ConvertNumberToNormalString((long) amount) + "/" + Utils.ConvertNumberToNormalString((long) need);
            component1.SetDetails(icon, reqDetails, (string) null, amount >= need, true);
            break;
          }
          string normalString2 = Utils.ConvertNumberToNormalString((long) need);
          component1.SetDetails2(icon, normalString2, string.Empty, false, false, false, string.Empty);
          break;
        case RequireComponentData.Requirement.SCROLL:
          if (showOwnMessage)
          {
            string reqDetails;
            if (amount < need)
              reqDetails = str1 + Utils.ConvertNumberToNormalString((long) amount) + str2 + "/" + Utils.ConvertNumberToNormalString((long) need);
            else
              reqDetails = Utils.ConvertNumberToNormalString((long) amount) + "/" + Utils.ConvertNumberToNormalString((long) need);
            int internalID = int.Parse(name);
            ConfigEquipmentScrollInfo data2 = ConfigManager.inst.GetEquipmentScroll(data1.GetBagType()).GetData(internalID);
            ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(data2.itemID);
            component1.SetDetails(icon, reqDetails, Utils.XLAT(itemStaticInfo.Loc_Name_Id), amount >= need, false);
            component1.SetRequirement(amount, need);
            component1.SetBackImage(data2.QualityImagePath);
            break;
          }
          string normalString3 = Utils.ConvertNumberToNormalString((long) need);
          component1.SetDetails2(icon, normalString3, string.Empty, false, false, false, string.Empty);
          break;
      }
      return gameObject;
    }

    private string GetQueueRequireLabelString()
    {
      if (CityManager.inst.GetBuildingsInFlux() < 1)
        return Utils.XLAT("id_uppercase_get_more");
      return JobManager.Instance.GetJob(CityManager.inst.FindFreeBuildQueueFluxBuilding().mBuildingJobID).LeftTime() - CityManager.inst.GetFreeSpeedUpTime() > 0 ? "Speed Up" : "Free";
    }

    public void OnGetMoreFoodBtnPressed()
    {
      GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.FOOD);
    }

    public void OnGetMoreWoodBtnPressed()
    {
      GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.WOOD);
    }

    public void OnGetMoreSilverBtnPressed()
    {
      GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.SILVER);
    }

    public void OnGetMoreOreBtnPressed()
    {
      GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.ORE);
    }

    public void OnGetMoreSteelBtnPressed()
    {
      UIManager.inst.OpenDlg("Equipment/SteelItemsDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    }

    public void GetMoreOfItemPressed()
    {
      ConstructionRequirementBtn component = UICamera.selectedObject.transform.parent.GetComponent<ConstructionRequirementBtn>();
      UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) new ItemUseOrBuyPopup.Parameter()
      {
        shop_id = component.GetShopInfo().internalId,
        type = ItemUseOrBuyPopup.Parameter.Type.BuyItem,
        onUseOrBuyCallBack = new System.Action<bool, object>(this.OnBuyCallBack)
      });
    }

    private void OnBuyCallBack(bool success, object data)
    {
      if (success)
        ;
    }

    private void ClearGrid()
    {
      UIUtils.CleanGrid(this.grid);
    }
  }
}
