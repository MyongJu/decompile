﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsLevelCompletePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class MerlinTrialsLevelCompletePopup : Popup
{
  public UILabel _descrption;
  private int _groupId;

  public override void OnOpen(UIControler.UIParameter orgParam = null)
  {
    base.OnOpen(orgParam);
    this._groupId = (orgParam as MerlinTrialsLevelCompletePopup.Parameter).groupId;
  }

  public override void OnShow(UIControler.UIParameter orgParam = null)
  {
    base.OnShow(orgParam);
    this.InitUI();
  }

  private void InitUI()
  {
    this._descrption.text = Utils.XLAT("trial_challenge_all_complete_description", (object) "0", (object) ConfigManager.inst.DB_MerlinTrialsGroup.Get(this._groupId).LocName);
  }

  public void OnCloseClick()
  {
    this.Close();
  }

  public void OnConfirmClick()
  {
    this.Close();
  }

  private void Close()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int groupId;
  }
}
