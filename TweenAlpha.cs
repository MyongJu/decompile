﻿// Decompiled with JetBrains decompiler
// Type: TweenAlpha
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[AddComponentMenu("NGUI/Tween/Tween Alpha")]
public class TweenAlpha : UITweener
{
  [Range(0.0f, 1f)]
  public float from = 1f;
  [Range(0.0f, 1f)]
  public float to = 1f;
  private bool mCached;
  private UIRect mRect;
  private Material mMat;
  private SpriteRenderer mSr;

  [Obsolete("Use 'value' instead")]
  public float alpha
  {
    get
    {
      return this.value;
    }
    set
    {
      this.value = value;
    }
  }

  private void Cache()
  {
    this.mCached = true;
    this.mRect = this.GetComponent<UIRect>();
    this.mSr = this.GetComponent<SpriteRenderer>();
    if (!((UnityEngine.Object) this.mRect == (UnityEngine.Object) null) || !((UnityEngine.Object) this.mSr == (UnityEngine.Object) null))
      return;
    Renderer component = this.GetComponent<Renderer>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      this.mMat = component.material;
    if (!((UnityEngine.Object) this.mMat == (UnityEngine.Object) null))
      return;
    this.mRect = this.GetComponentInChildren<UIRect>();
  }

  private void CustomSetAlpha(Material mat, float alpha)
  {
    if (!((UnityEngine.Object) mat != (UnityEngine.Object) null) || !mat.HasProperty("_Color"))
      return;
    Color color = mat.color;
    color.a = alpha;
    mat.color = color;
  }

  private float CustomGetAlpha(Material mat)
  {
    if ((UnityEngine.Object) mat != (UnityEngine.Object) null && mat.HasProperty("_Color"))
      return mat.color.a;
    return 1f;
  }

  public float value
  {
    get
    {
      if (!this.mCached)
        this.Cache();
      if ((UnityEngine.Object) this.mRect != (UnityEngine.Object) null)
        return this.mRect.alpha;
      if ((UnityEngine.Object) this.mSr != (UnityEngine.Object) null)
        return this.mSr.color.a;
      return this.CustomGetAlpha(this.mMat);
    }
    set
    {
      if (!this.mCached)
        this.Cache();
      if ((UnityEngine.Object) this.mRect != (UnityEngine.Object) null)
        this.mRect.alpha = value;
      else if ((UnityEngine.Object) this.mSr != (UnityEngine.Object) null)
      {
        Color color = this.mSr.color;
        color.a = value;
        this.mSr.color = color;
      }
      else
      {
        if (!((UnityEngine.Object) this.mMat != (UnityEngine.Object) null))
          return;
        this.CustomSetAlpha(this.mMat, value);
      }
    }
  }

  protected override void OnUpdate(float factor, bool isFinished)
  {
    this.value = Mathf.Lerp(this.from, this.to, factor);
  }

  public static TweenAlpha Begin(GameObject go, float duration, float alpha, float delay = 0)
  {
    TweenAlpha tweenAlpha = UITweener.Begin<TweenAlpha>(go, duration);
    tweenAlpha.from = tweenAlpha.value;
    tweenAlpha.to = alpha;
    tweenAlpha.delay = delay;
    if ((double) duration <= 0.0)
    {
      tweenAlpha.Sample(1f, true);
      tweenAlpha.enabled = false;
    }
    return tweenAlpha;
  }

  public override void SetStartToCurrentValue()
  {
    this.from = this.value;
  }

  public override void SetEndToCurrentValue()
  {
    this.to = this.value;
  }
}
