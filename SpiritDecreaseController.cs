﻿// Decompiled with JetBrains decompiler
// Type: SpiritDecreaseController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SpiritDecreaseController : MonoBehaviour
{
  [SerializeField]
  private GameObject m_SpiritPrefab;

  public void Add(string spirit)
  {
    GameObject gameObject = Utils.DuplicateGOB(this.m_SpiritPrefab, this.transform);
    gameObject.SetActive(true);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    gameObject.GetComponent<SpiritDecrease>().spirit = spirit;
  }
}
