﻿// Decompiled with JetBrains decompiler
// Type: SpriteRenderColorTweener
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SpriteRenderColorTweener : MonoBehaviour
{
  private SpriteRenderer[] allRenders;

  public void Hide()
  {
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) 1f, (object) "to", (object) 0.0f, (object) "time", (object) 0.2f, (object) "easetype", (object) "Linear", (object) "onupdate", (object) "SetAlpha"));
  }

  private void SetAlpha(float alpha)
  {
    this.allRenders = this.GetComponentsInChildren<SpriteRenderer>(true);
    for (int index = 0; index < this.allRenders.Length; ++index)
    {
      Color color = new Color(this.allRenders[index].color.r, this.allRenders[index].color.g, this.allRenders[index].color.b, alpha);
      this.allRenders[index].color = color;
    }
  }

  public void Show()
  {
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) 0.0f, (object) "to", (object) 1f, (object) "time", (object) 0.2f, (object) "easetype", (object) "Linear", (object) "onupdate", (object) "SetAlpha"));
  }

  public void Back()
  {
    this.SetAlpha(1f);
  }

  public void QuickHide()
  {
    this.SetAlpha(0.0f);
  }
}
