﻿// Decompiled with JetBrains decompiler
// Type: MovableCircle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MovableCircle : MonoBehaviour, IRecycle
{
  public System.Action<GameObject> onCircleClicked;

  public void Selected()
  {
    if (this.onCircleClicked == null)
      return;
    BuildingController componentInParent1 = this.GetComponentInParent<BuildingController>();
    if ((UnityEngine.Object) null == (UnityEngine.Object) componentInParent1)
    {
      CityPlotController componentInParent2 = this.GetComponentInParent<CityPlotController>();
      if (!((UnityEngine.Object) null != (UnityEngine.Object) componentInParent2))
        return;
      this.onCircleClicked(componentInParent2.gameObject);
    }
    else
      this.onCircleClicked(componentInParent1.gameObject);
  }

  public void SetState(MovableCircle.CircleState cs)
  {
    SpriteRenderer component = this.GetComponent<SpriteRenderer>();
    if (cs == MovableCircle.CircleState.Candidate)
    {
      component.color = new Color(0.7098039f, 0.9960784f, 0.4156863f);
    }
    else
    {
      if (cs != MovableCircle.CircleState.Selected)
        return;
      component.color = new Color(1f, 0.1490196f, 0.0627451f);
    }
  }

  public void OnInitialize()
  {
  }

  public void OnFinalize()
  {
    this.onCircleClicked = (System.Action<GameObject>) null;
  }

  public enum CircleState
  {
    Candidate,
    Selected,
  }
}
