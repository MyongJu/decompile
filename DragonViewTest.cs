﻿// Decompiled with JetBrains decompiler
// Type: DragonViewTest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DragonViewTest : MonoBehaviour
{
  public Color color = Color.black;
  private DragonView _dragonView;

  private void CreateDragon()
  {
    this.ReleaseDragon();
    DragonViewData dragonViewData = DragonUtils.GetDragonViewData(0, 0, 0);
    this._dragonView = new DragonView();
    this._dragonView.Mount(this.transform);
    this._dragonView.Load((SpriteViewData) dragonViewData, true);
  }

  private void ReleaseDragon()
  {
    if (this._dragonView == null)
      return;
    this._dragonView.Dispose();
  }
}
