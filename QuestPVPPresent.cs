﻿// Decompiled with JetBrains decompiler
// Type: QuestPVPPresent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class QuestPVPPresent : BasePresent
{
  public override void Refresh()
  {
    string formula = this.Formula;
    if (formula == "killed_troop")
    {
      int count = this.Count;
      int num = (int) DBManager.inst.DB_UserProfileDB.Get("troops_lost");
      if (num > count)
        num = count;
      this.ProgressValue = (float) num / (float) count;
      this.ProgressContent = num.ToString() + "/" + (object) count;
    }
    else if (formula.IndexOf('@') > -1)
    {
      this.UseStats();
    }
    else
    {
      int count = this.Count;
      int num1 = (int) DBManager.inst.DB_UserProfileDB.Get("troops_killed");
      int num2 = (int) (DBManager.inst.DB_UserProfileDB.Get("attack_won") + DBManager.inst.DB_UserProfileDB.Get("attack_lost"));
      int num3 = (int) DBManager.inst.DB_UserProfileDB.Get("attack_won");
      int num4 = formula.IndexOf("kill") <= -1 ? (formula.IndexOf("win") <= -1 ? num2 : num3) : num1;
      if (num4 > count)
        num4 = count;
      this.ProgressValue = (float) num4 / (float) count;
      this.ProgressContent = num4.ToString() + "/" + (object) count;
    }
  }

  public override int IconType
  {
    get
    {
      return 1;
    }
  }
}
