﻿// Decompiled with JetBrains decompiler
// Type: GroupMember
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class GroupMember
{
  private Group m_Group;

  public event OnGroupMemberReceived OnReceived;

  public void SetGroup(Group group)
  {
    if (this.m_Group != null)
      this.m_Group.RemoveMember(this);
    this.m_Group = group;
    if (this.m_Group == null)
      return;
    this.m_Group.AddMember(this);
  }

  public void Send(object data)
  {
    if (this.m_Group == null || !this.m_Group.ContainsMember(this))
      return;
    this.m_Group.Broadcast(this, data);
  }

  public void Receive(GroupMember sender, object data)
  {
    if (this.OnReceived == null)
      return;
    this.OnReceived(sender, data);
  }
}
