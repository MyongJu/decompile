﻿// Decompiled with JetBrains decompiler
// Type: MetaData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Text;

public class MetaData
{
  public const string metaDataPathName = "metadata.json";
  private const string fileName = "file_name";
  private const string folderTag = "folder_tag";
  private const string location = "location";
  private const string allTag = "all";

  public static string AddAllDevicePrefix(string path, MetaDataLocation loc, out Hashtable metaData)
  {
    metaData = new Hashtable();
    if (!string.IsNullOrEmpty(path))
      return MetaData.CombinePath(path, "all", loc.ToString(), out metaData);
    return (string) null;
  }

  private static string CombinePath(string path, string deviceName, string fileLoc, out Hashtable metaData)
  {
    metaData = new Hashtable();
    string str = string.Format("{0}.{1}.{2}", (object) fileLoc, (object) deviceName, (object) path);
    metaData.Add((object) str, (object) new Hashtable()
    {
      {
        (object) "file_name",
        (object) path
      },
      {
        (object) "folder_tag",
        (object) deviceName
      },
      {
        (object) "location",
        (object) fileLoc
      }
    });
    return str;
  }

  public static string AddDevicePrefix(string path, string platform, string resolution, MetaDataLocation loc, out Hashtable metaData)
  {
    metaData = new Hashtable();
    if (!string.IsNullOrEmpty(path) && !string.IsNullOrEmpty(platform) && !string.IsNullOrEmpty(resolution))
      return MetaData.CombinePath(path, platform + resolution, loc.ToString(), out metaData);
    return (string) null;
  }

  public static byte[] CreateMetaDataJSON(List<Hashtable> hashtables)
  {
    Hashtable hashtable = new Hashtable();
    for (int index = 0; index < hashtables.Count; ++index)
    {
      foreach (DictionaryEntry dictionaryEntry in hashtables[index])
        hashtable.Add(dictionaryEntry.Key, dictionaryEntry.Value);
    }
    return Encoding.UTF8.GetBytes(Utils.Object2Json((object) hashtable));
  }
}
