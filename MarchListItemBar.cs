﻿// Decompiled with JetBrains decompiler
// Type: MarchListItemBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UI;
using UnityEngine;

public class MarchListItemBar : MonoBehaviour
{
  private MarchData.MarchState GoTroopsState = MarchData.MarchState.marching | MarchData.MarchState.returning;
  public const MarchData.MarchState STATE_NOT_UPDATE = MarchData.MarchState.invalid;
  public const MarchData.MarchState STATE_NOT_SHOW_PROGRESS_BAR = MarchData.MarchState.reinforcing | MarchData.MarchState.encamping | MarchData.MarchState.rallying | MarchData.MarchState.rally_attacking | MarchData.MarchState.holding_wonder;
  private MarchData _marchData;
  private long _marchId;
  [SerializeField]
  private MarchListItemBar.Panel panel;

  public long marchId
  {
    get
    {
      return this._marchId;
    }
    set
    {
      this._marchId = value;
      if (this._marchId != 0L)
      {
        this._marchData = DBManager.inst.DB_March.Get(this._marchId);
        this.SetInfo(NetServerTime.inst.ServerTimestamp);
      }
      else
        this._marchData = (MarchData) null;
    }
  }

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void OnSecond(int serverTime)
  {
    this.SetInfo(serverTime);
  }

  private void SetInfo(int serverTime)
  {
    if (this._marchData == null)
      return;
    int state = (int) this._marchData.state;
    this.panel.marchType.text = this.GetTypeName(this._marchData.type);
    this.panel.title.text = this.GetTitleInfo(this._marchData.state);
    this.panel.coordinateK.text = this._marchData.endLocation.K.ToString();
    this.panel.coordinateX.text = this._marchData.endLocation.X.ToString();
    this.panel.coordinateY.text = this._marchData.endLocation.Y.ToString();
    this.RefreshMarchIcon();
    if ((this._marchData.state & (MarchData.MarchState.reinforcing | MarchData.MarchState.encamping | MarchData.MarchState.rallying | MarchData.MarchState.rally_attacking | MarchData.MarchState.holding_wonder)) == MarchData.MarchState.invalid)
    {
      this.panel.marchProgressContainer.SetActive(true);
      this.panel.marchProgressDescription.text = this.GetProgressState(this._marchData.state);
      int time = this._marchData.endTime - serverTime;
      if (time < 0)
        time = 0;
      this.panel.marchProgessInfo.text = Utils.FormatTime(time, false, false, true);
      this.panel.marchProgressBar.value = (float) (serverTime - this._marchData.startTime) * this._marchData.timeDuration.oneOverTimeDuration;
    }
    else
      this.panel.marchProgressContainer.SetActive(false);
    this.panel.troopsCount.text = this._marchData.troopsInfo.totalCount.ToString();
    this.panel.BT_Cancle.gameObject.SetActive((this._marchData.state & (MarchData.MarchState.reinforcing | MarchData.MarchState.scouting | MarchData.MarchState.encamping | MarchData.MarchState.marching | MarchData.MarchState.gathering | MarchData.MarchState.defending_fortress | MarchData.MarchState.demolishing_fortress | MarchData.MarchState.holding_wonder | MarchData.MarchState.building | MarchData.MarchState.dig)) > MarchData.MarchState.invalid);
    this.panel.BT_SpeedUp.gameObject.SetActive((this._marchData.state & (MarchData.MarchState.marching | MarchData.MarchState.returning)) > MarchData.MarchState.invalid);
    if (this._marchData.type != MarchData.MarchType.rally || this._marchData.state != MarchData.MarchState.marching)
      return;
    this.panel.BT_Cancle.gameObject.SetActive(false);
    this.panel.BT_SpeedUp.gameObject.SetActive(false);
  }

  private void RefreshMarchIcon()
  {
    switch (this._marchData.type)
    {
      case MarchData.MarchType.rally:
        this.panel.playerIcon.atlas = UIManager.inst.atlasBasicGui;
        this.panel.playerIcon.spriteName = "war_rally_icon";
        return;
      case MarchData.MarchType.attack:
      case MarchData.MarchType.city_attack:
      case MarchData.MarchType.encamp_attack:
      case MarchData.MarchType.gather_attack:
        this.panel.playerIcon.atlas = UIManager.inst.atlasBasicGui;
        this.panel.playerIcon.spriteName = "item_icon_combat";
        return;
      case MarchData.MarchType.trade:
        this.panel.playerIcon.atlas = UIManager.inst.atlasBasicGui;
        this.panel.playerIcon.spriteName = "trading_icon";
        return;
      case MarchData.MarchType.scout:
        this.panel.playerIcon.atlas = UIManager.inst.atlasBasicGui;
        this.panel.playerIcon.spriteName = "icon_help";
        return;
      case MarchData.MarchType.encamp:
        this.panel.playerIcon.atlas = UIManager.inst.atlasBasicGui;
        this.panel.playerIcon.spriteName = "kingdom_map_btn";
        return;
      case MarchData.MarchType.gather:
        TileData referenceAt = PVPMapData.MapData.GetReferenceAt(this._marchData.endLocation);
        if (referenceAt != null)
        {
          TileType tileType = referenceAt.TileType;
          switch (tileType)
          {
            case TileType.City:
              this.panel.playerIcon.atlas = UIManager.inst.atlasGUI_2;
              this.panel.playerIcon.spriteName = "icon_my_city";
              return;
            case TileType.Resource1:
              this.panel.playerIcon.atlas = UIManager.inst.atlasBasicGui;
              this.panel.playerIcon.spriteName = "food_icon";
              return;
            case TileType.Resource2:
              this.panel.playerIcon.atlas = UIManager.inst.atlasBasicGui;
              this.panel.playerIcon.spriteName = "wood_icon";
              return;
            case TileType.Resource3:
              this.panel.playerIcon.atlas = UIManager.inst.atlasBasicGui;
              this.panel.playerIcon.spriteName = "ore_icon";
              return;
            case TileType.Resource4:
              this.panel.playerIcon.atlas = UIManager.inst.atlasBasicGui;
              this.panel.playerIcon.spriteName = "silver_icon";
              return;
            default:
              if (tileType == TileType.PitMine)
              {
                this.panel.playerIcon.atlas = UIManager.inst.atlasBasicGui;
                this.panel.playerIcon.spriteName = "dragon_fossil_icon";
                return;
              }
              break;
          }
        }
        else
          break;
      case MarchData.MarchType.reinforce:
        this.panel.playerIcon.atlas = UIManager.inst.atlasBasicGui;
        this.panel.playerIcon.spriteName = "reinforce_icon";
        return;
      case MarchData.MarchType.wonder_attack:
        this.panel.playerIcon.atlas = UIManager.inst.atlasBasicGui;
        this.panel.playerIcon.spriteName = "icons_mini_honor";
        return;
    }
    this.panel.playerIcon.atlas = UIManager.inst.atlasBasicGui;
    this.panel.playerIcon.spriteName = "icon_missing";
  }

  private string GetTypeName(MarchData.MarchType type)
  {
    switch (type)
    {
      case MarchData.MarchType.attack:
      case MarchData.MarchType.city_attack:
      case MarchData.MarchType.encamp_attack:
      case MarchData.MarchType.gather_attack:
        return "Attacking";
      case MarchData.MarchType.scout:
        return "Scouting";
      case MarchData.MarchType.encamp:
        return "Encamping";
      case MarchData.MarchType.gather:
        return "Gathering";
      default:
        return type.ToString();
    }
  }

  private string GetTitleInfo(MarchData.MarchState state)
  {
    return state.ToString();
  }

  private string GetProgressState(MarchData.MarchState state)
  {
    return state.ToString();
  }

  public void PressCancleButton()
  {
    if (this._marchData == null)
      return;
    GameEngine.Instance.marchSystem.Recall(this._marchId);
  }

  public void PressSpeedUpButton()
  {
    UIManager.inst.OpenPopup("MarchSpeedUpPopup", (Popup.PopupParameter) new MarchSpeedUpPopup.Parameter()
    {
      marchData = DBManager.inst.DB_March.Get(this._marchId)
    });
  }

  public void PressInfoButton()
  {
    UIManager.inst.OpenPopup("MarchDetailPopUp", (Popup.PopupParameter) new MarchDetailPopUp.Parameter()
    {
      marchId = this._marchId
    });
  }

  public void PressGotoButton()
  {
    if (this._marchData == null || GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
      return;
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    if ((this._marchData.state & this.GoTroopsState) > MarchData.MarchState.invalid)
      UIManager.inst.OpenKingdomArmyCircle(this._marchData.marchId, (Transform) null);
    else
      PVPSystem.Instance.Map.GotoLocation(this._marchData.targetLocation, false);
  }

  public void PressHeroButton()
  {
  }

  [Serializable]
  protected class Panel
  {
    public UISprite playerIcon;
    public UILabel marchType;
    public UILabel title;
    public UILabel coordinateK;
    public UILabel coordinateX;
    public UILabel coordinateY;
    public GameObject marchProgressContainer;
    public UILabel marchProgressDescription;
    public UILabel marchProgessInfo;
    public UISlider marchProgressBar;
    public UILabel troopsCount;
    public UIButton BT_Cancle;
    public UIButton BT_SpeedUp;
    public UIButton BT_Info;
    public UIButton BT_Goto;
  }
}
