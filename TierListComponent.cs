﻿// Decompiled with JetBrains decompiler
// Type: TierListComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class TierListComponent : ComponentRenderBase
{
  public UILabel tierLabel;
  public IconListComponent iconList;

  public override void Init()
  {
    base.Init();
    TierComponentData data1 = this.data as TierComponentData;
    this.tierLabel.text = data1.tier.ToUpper();
    List<IconData> data2 = new List<IconData>();
    ArrayList arrayList = new ArrayList(data1.list.Keys);
    List<TierListComponent.SortedUnit> sortedUnitList = new List<TierListComponent.SortedUnit>();
    for (int index = 0; index < arrayList.Count; ++index)
      sortedUnitList.Add(new TierListComponent.SortedUnit(arrayList[index].ToString()));
    sortedUnitList.Sort((Comparison<TierListComponent.SortedUnit>) ((x, y) => ConfigManager.inst.DB_Unit_Statistics.GetData(x.ID).Priority.CompareTo(ConfigManager.inst.DB_Unit_Statistics.GetData(y.ID).Priority)));
    for (int index = 0; index < sortedUnitList.Count; ++index)
    {
      string id = sortedUnitList[index].ID;
      Unit_StatisticsInfo data3 = ConfigManager.inst.DB_Unit_Statistics.GetData(id);
      IconData iconData = new IconData(data3.Troop_ICON, new string[2]
      {
        Utils.XLAT(data3.Troop_Name_LOC_ID),
        data1.list[(object) id].ToString()
      });
      data2.Add(iconData);
    }
    this.iconList.FeedData((IComponentData) new IconListComponentData(data2));
  }

  public override void Dispose()
  {
    base.Dispose();
    this.iconList.Dispose();
  }

  private class SortedUnit
  {
    public string ID;

    public SortedUnit(string unitID)
    {
      this.ID = unitID;
    }
  }
}
