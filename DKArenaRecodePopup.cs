﻿// Decompiled with JetBrains decompiler
// Type: DKArenaRecodePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DKArenaRecodePopup : Popup
{
  private List<DKArenaRecodeItem> _total = new List<DKArenaRecodeItem>();
  public UILabel title;
  public UILabel total;
  public UILabel lostTimes;
  public UILabel winTimes;
  public UILabel winPrecents;
  public DKArenaRecodeItem itemPrefab;
  public UIGrid grid;
  public UIScrollView scrollView;

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    UserDKArenaData currentData = DBManager.inst.DB_DKArenaDB.GetCurrentData();
    this.total.text = currentData.totalJoinTimes.ToString();
    this.winTimes.text = currentData.totalWinTimes.ToString();
    this.lostTimes.text = currentData.totalLoseTimes.ToString();
    this.winPrecents.text = ((double) currentData.TotalWinPrecent * 100.0).ToString() + "%";
    List<DragonKnightPVPResultPopup.Parameter> dkArenaHistory = DragonKnightUtils.GetDKArenaHistory();
    for (int index = 0; index < dkArenaHistory.Count; ++index)
      this.GetItem().SetData(dkArenaHistory[index]);
    this.grid.Reposition();
    this.scrollView.ResetPosition();
  }

  private DKArenaRecodeItem GetItem()
  {
    GameObject go = NGUITools.AddChild(this.grid.gameObject, this.itemPrefab.gameObject);
    NGUITools.SetActive(go, true);
    DKArenaRecodeItem component = go.GetComponent<DKArenaRecodeItem>();
    this._total.Add(component);
    return component;
  }

  private void AddEventHandler()
  {
  }

  private void RemoveEventHandler()
  {
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.UpdateUI();
    this.AddEventHandler();
  }

  private void OnLoadDataCallBack(bool success, object result)
  {
    this.UpdateUI();
    this.AddEventHandler();
    this._total.Clear();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.RemoveEventHandler();
  }
}
