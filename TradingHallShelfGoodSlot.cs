﻿// Decompiled with JetBrains decompiler
// Type: TradingHallShelfGoodSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TradingHallShelfGoodSlot : MonoBehaviour
{
  [SerializeField]
  private ItemIconRenderer _itemIconRenderer;
  [SerializeField]
  private UILabel _labelItemCount;
  [SerializeField]
  private UILabel _labelEquipmentEnhanced;
  [SerializeField]
  private GameObject _objCooldown;
  private TradingHallData.SellGoodData _sellGoodData;
  public System.Action OnShelfGoodSuccess;

  public void SetData(TradingHallData.SellGoodData sellGoodData)
  {
    this._sellGoodData = sellGoodData;
    this.UpdateUI();
  }

  public void OnShelfGoodPressed()
  {
    if (this._sellGoodData == null)
      return;
    if (this._sellGoodData.IsCooldown)
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("exchange_hall_item_frozen_description", new Dictionary<string, string>()
      {
        {
          "0",
          TradingHallPayload.Instance.HowManyDays(this._sellGoodData.CooldownTime - NetServerTime.inst.ServerTimestamp).ToString()
        }
      }, true), (System.Action) null, 4f, true);
    else if (TradingHallPayload.Instance.CurrentShelfCount >= TradingHallPayload.Instance.MaxShelfCount)
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_exchange_hall_listed_items_limit"), (System.Action) null, 4f, true);
    }
    else
    {
      BagType bagType = TradingHallPayload.Instance.GetBagType(this._sellGoodData.ExchangeId);
      switch (bagType)
      {
        case BagType.Hero:
        case BagType.DragonKnight:
          InventoryItemData myItemByItemId = DBManager.inst.GetInventory(bagType).GetMyItemByItemID((long) this._sellGoodData.EquipmentId);
          if (myItemByItemId != null)
          {
            if (myItemByItemId.isEquipped)
            {
              UIManager.inst.toast.Show(Utils.XLAT("toast_exchange_hall_cannot_sell_equipped"), (System.Action) null, 4f, true);
              return;
            }
            if (myItemByItemId.inlaidGems.GetAllGemData().Count > 0)
            {
              UIManager.inst.toast.Show(Utils.XLAT("toast_exchange_hall_cannot_sell_embedded"), (System.Action) null, 4f, true);
              return;
            }
            break;
          }
          break;
      }
      UIManager.inst.OpenPopup("TradingHall/TradingHallSellPopup", (Popup.PopupParameter) new TradingHallSellPopup.Parameter()
      {
        sellGoodData = this._sellGoodData,
        onSellSuccess = (System.Action) (() =>
        {
          if (this.OnShelfGoodSuccess == null)
            return;
          this.OnShelfGoodSuccess();
        })
      });
    }
  }

  public void OnItemPress()
  {
    if (this._sellGoodData == null)
      return;
    Utils.DelayShowTip(this._sellGoodData.ItemId, this.transform, (long) this._sellGoodData.EquipmentId, 0L, this._sellGoodData.EquipmentEnhanced);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  public void OnItemClick()
  {
    if (this._sellGoodData == null)
      return;
    Utils.ShowItemTip(this._sellGoodData.ItemId, this.transform, (long) this._sellGoodData.EquipmentId, 0L, this._sellGoodData.EquipmentEnhanced);
  }

  private void UpdateUI()
  {
    if (this._sellGoodData == null)
      return;
    this._itemIconRenderer.SetData(this._sellGoodData.ItemId, string.Empty, true);
    this._labelItemCount.text = this._sellGoodData.Amount.ToString();
    this._labelEquipmentEnhanced.text = this._sellGoodData.EquipmentEnhanced <= 0 ? string.Empty : "+" + this._sellGoodData.EquipmentEnhanced.ToString();
    NGUITools.SetActive(this._objCooldown, this._sellGoodData.IsCooldown);
  }
}
