﻿// Decompiled with JetBrains decompiler
// Type: HeroCardUse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;

public class HeroCardUse : ItemBaseUse
{
  public HeroCardUse(int id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  public HeroCardUse(string id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  protected override void CustomUse(System.Action<bool, object> resultHandler)
  {
    MessageHub.inst.GetPortByAction("Legend:openHeroCard").SendRequest(new Hashtable()
    {
      {
        (object) "item_id",
        (object) this.Info.internalId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      HeroRecruitPayload heroRecruitPayload = new HeroRecruitPayload();
      heroRecruitPayload.Initialize(data);
      List<HeroRecruitPayloadData> payloadData = heroRecruitPayload.GetPayloadData();
      if (payloadData == null || payloadData[0] == null)
        return;
      UIManager.inst.OpenDlg("HeroCard/HeroRecruitLuckyDrawDialog", (UI.Dialog.DialogParameter) new HeroRecruitLuckyDrawDialog.Parameter()
      {
        heroPayload = payloadData[0],
        showButton = false,
        heroRecruitData = (HeroRecruitData) new HeroRecruitDataGold()
      }, true, true, true);
    }), true);
  }
}
