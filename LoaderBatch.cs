﻿// Decompiled with JetBrains decompiler
// Type: LoaderBatch
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class LoaderBatch
{
  private List<LoaderInfo> _infoQueue = new List<LoaderInfo>();
  private Dictionary<string, LoaderInfo> _dic = new Dictionary<string, LoaderInfo>();
  private Dictionary<string, float> _progressKey = new Dictionary<string, float>();
  private int type;
  private float total;

  public event System.Action<LoaderInfo> processEvent;

  public event System.Action<LoaderBatch> finishedEvent;

  public event System.Action<LoaderBatch> disposeEvent;

  public int Type
  {
    get
    {
      return this.type;
    }
    set
    {
      this.type = value;
    }
  }

  public float Progress { get; protected set; }

  public int CurrentBytes { get; protected set; }

  public int TotalBytes { get; protected set; }

  public bool BlockScreen { get; set; }

  public void Add(LoaderInfo info)
  {
    if (info != null && info.Type > 0 && info.Action != null)
    {
      if (this._dic.ContainsKey(info.Action))
        MessageHub.inst.OnError(info.Action, "action already exists.");
      info.OnResult = new System.Action<LoaderHttp>(this.OnResult);
      info.OnFailure = new System.Action<LoaderHttp>(this.OnFailure);
      info.OnProgress = new System.Action<LoaderHttp>(this.OnProgress);
      this._dic[info.Action] = info;
      this._infoQueue.Add(info);
      ++this.total;
    }
    else
      MessageHub.inst.OnError(nameof (LoaderBatch), "info error.");
  }

  public void Add(string key, string url, Hashtable postHt = null, bool autoRetry = true, int type = 3, bool forceEncrypt = false)
  {
    if (this._dic.ContainsKey(key))
      MessageHub.inst.OnError(key, "key already exists.");
    LoaderInfo loaderInfo = LoaderManager.inst.PopInfo();
    loaderInfo.URL = url;
    loaderInfo.Action = key;
    loaderInfo.Type = type;
    loaderInfo.ForceEncrypt = forceEncrypt;
    int num = 15;
    switch (type)
    {
      case 1:
      case 2:
        num = 5;
        break;
      case 4:
        num = 180;
        break;
      case 5:
        num = 30;
        break;
    }
    loaderInfo.TimeOut = num;
    loaderInfo.AutoRetry = autoRetry;
    loaderInfo.PostData = postHt;
    loaderInfo.OnResult = new System.Action<LoaderHttp>(this.OnResult);
    loaderInfo.OnFailure = new System.Action<LoaderHttp>(this.OnFailure);
    loaderInfo.OnProgress = new System.Action<LoaderHttp>(this.OnProgress);
    this._dic[key] = loaderInfo;
    this._infoQueue.Add(loaderInfo);
    ++this.total;
  }

  public void AddConfig(string key, string url, Hashtable postHt = null, bool autoRetry = true)
  {
    this.Add(key, url, postHt, autoRetry, 5, false);
  }

  public void AddAsset(string key, string url, Hashtable postHt = null, bool autoRetry = true)
  {
    this.Add(key, url, postHt, autoRetry, 4, false);
  }

  private void OnResult(LoaderHttp http)
  {
    this.RemoveInfo(http);
  }

  private void OnProgress(LoaderHttp http)
  {
    this._progressKey[http.Info.Action] = http.Progress;
    this.UpdateProgress();
  }

  private void UpdateProgress()
  {
    float num = 0.0f;
    Dictionary<string, float>.Enumerator enumerator = this._progressKey.GetEnumerator();
    while (enumerator.MoveNext())
      num += enumerator.Current.Value;
    this.Progress = num / this.total;
  }

  private void OnFailure(LoaderHttp http)
  {
    LoaderInfo info = http.Info;
    if (!info.AutoRetry)
      this.RemoveInfo(http);
    else if (info.CanResend())
    {
      MessageHub.inst.OnError(info.Action, info.ErrorMsg);
      info.ReSendTime = NetServerTime.inst.ServerTimestamp;
    }
    else
      this.RemoveInfo(http);
  }

  private void RemoveInfo(LoaderHttp http)
  {
    if (http.Info.Type == 1 && !http.Info.AutoRetry)
      http.Info.IsWroker = false;
    this._infoQueue.Remove(http.Info);
    if (this.processEvent != null)
      this.processEvent(http.Info);
    http.Info = (LoaderInfo) null;
    if (this._infoQueue.Count != 0)
      return;
    if (this.finishedEvent != null)
      this.finishedEvent(this);
    if (this.disposeEvent != null)
      this.disposeEvent(this);
    this.ClearEvents();
  }

  public void Accident()
  {
    if (this._infoQueue.Count != 0)
      return;
    if (this.finishedEvent != null)
      this.finishedEvent(this);
    if (this.disposeEvent == null)
      return;
    this.disposeEvent(this);
  }

  public LoaderInfo GetLoaderInfo()
  {
    for (int index = 0; index < this._infoQueue.Count; ++index)
    {
      LoaderInfo info = this._infoQueue[index];
      if (!info.IsWroker)
      {
        info.IsWroker = true;
        return info;
      }
    }
    return (LoaderInfo) null;
  }

  public LoaderInfo GetResultInfo(string key)
  {
    if (this._dic.ContainsKey(key))
      return this._dic[key];
    return (LoaderInfo) null;
  }

  public int GetInfoCount()
  {
    return this._infoQueue.Count;
  }

  public void ClearEvents()
  {
    if (this.finishedEvent != null)
      this.finishedEvent = (System.Action<LoaderBatch>) null;
    if (this.disposeEvent != null)
      this.disposeEvent = (System.Action<LoaderBatch>) null;
    if (this.processEvent == null)
      return;
    this.processEvent = (System.Action<LoaderInfo>) null;
  }

  public void Dispose()
  {
    if (this.finishedEvent != null)
      this.finishedEvent = (System.Action<LoaderBatch>) null;
    if (this.disposeEvent != null)
      this.disposeEvent = (System.Action<LoaderBatch>) null;
    if (this.processEvent != null)
      this.processEvent = (System.Action<LoaderInfo>) null;
    if (this._infoQueue != null)
      this._infoQueue.Clear();
    if (this._progressKey != null)
      this._progressKey.Clear();
    this.total = 0.0f;
    if (this._dic == null)
      return;
    IDictionaryEnumerator enumerator = (IDictionaryEnumerator) this._dic.GetEnumerator();
    while (enumerator.MoveNext())
    {
      LoaderInfo info = enumerator.Entry.Value as LoaderInfo;
      info.Dispose();
      LoaderManager.inst.ReleaseInfo(info);
    }
    this._dic.Clear();
  }
}
