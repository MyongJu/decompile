﻿// Decompiled with JetBrains decompiler
// Type: ItemSkillBenefitSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class ItemSkillBenefitSlot : MonoBehaviour
{
  [SerializeField]
  private ItemSkillBenefitSlot.Panel panel;

  public void SetScrollView(UIScrollView scrollView)
  {
    this.panel.dragScrollView.scrollView = scrollView;
  }

  public void Setup(int propertyInternalID, string modifier)
  {
    if (!ConfigManager.inst.DB_Properties.Contains(propertyInternalID))
      return;
    this.panel.title.text = ConfigManager.inst.DB_Properties[propertyInternalID].Name;
    this.panel.backgroundImage.spriteName = ConfigManager.inst.DB_Properties[propertyInternalID].Image;
    this.panel.value.text = "+" + modifier;
  }

  [Serializable]
  protected class Panel
  {
    public UILabel title;
    public UILabel value;
    public UISprite icon;
    public UISprite backgroundImage;
    public UIDragScrollView dragScrollView;
  }
}
