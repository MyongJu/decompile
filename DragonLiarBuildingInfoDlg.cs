﻿// Decompiled with JetBrains decompiler
// Type: DragonLiarBuildingInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UI;

public class DragonLiarBuildingInfoDlg : BuildingInfoBaseDlg
{
  public UITexture texture1;
  public UILabel nameLabel1;
  public UILabel valueLabel1;
  public UITexture texture2;
  public UILabel nameLabel2;
  public UILabel valueLabel2;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    if (this.buildingInfo.Benefit_ID_1.Length > 0)
      this.DrawBenefit(this.buildingInfo.Benefit_Value_1, this.nameLabel1, this.valueLabel1, int.Parse(this.buildingInfo.Benefit_ID_1), "prop_dragon_light_reduce_percent", "calc_dragon_light_cost", this.texture1);
    if (this.buildingInfo.Benefit_ID_2.Length <= 0)
      return;
    this.DrawBenefit(this.buildingInfo.Benefit_Value_2, this.nameLabel2, this.valueLabel2, int.Parse(this.buildingInfo.Benefit_ID_2), "prop_dragon_dark_reduce_percent", "calc_dragon_dark_cost", this.texture2);
  }

  protected override void DrawBenefit(double benifitValue, UILabel nameLabel, UILabel valueLabel, int benefitInternalID, string baseBenefitID, string caclID, UITexture texture = null)
  {
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitInternalID];
    nameLabel.text = dbProperty.Name;
    if ((UnityEngine.Object) texture != (UnityEngine.Object) null)
      BuilderFactory.Instance.HandyBuild((UIWidget) texture, dbProperty.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    float num = (float) benifitValue;
    float total = DBManager.inst.DB_Local_Benefit.Get(baseBenefitID).total;
    string str = (double) num <= 0.0 ? "-" : "+";
    if (dbProperty.Format == PropertyDefinition.FormatType.Percent)
      valueLabel.text = dbProperty.ConvertToDisplayString((double) num, false, true) + str + dbProperty.ConvertToDisplayStringWithoutPreString((double) Math.Abs(total - num), true, true, (double) num > 0.0);
    else
      valueLabel.text = dbProperty.ConvertToDisplayString((double) num, false, true) + str + dbProperty.ConvertToDisplayString((double) Math.Abs(total - num), true, true);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    BuilderFactory.Instance.Release((UIWidget) this.texture1);
    BuilderFactory.Instance.Release((UIWidget) this.texture2);
    base.OnClose(orgParam);
  }
}
