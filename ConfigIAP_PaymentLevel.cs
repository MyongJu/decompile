﻿// Decompiled with JetBrains decompiler
// Type: ConfigIAP_PaymentLevel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigIAP_PaymentLevel
{
  private Dictionary<string, IAPPaymentLevelInfo> m_DataByID;
  private Dictionary<int, IAPPaymentLevelInfo> m_DataByInternalID;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<IAPPaymentLevelInfo, string>(result as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
  }

  public IAPPaymentLevelInfo Get(string id)
  {
    IAPPaymentLevelInfo paymentLevelInfo;
    this.m_DataByID.TryGetValue(id, out paymentLevelInfo);
    return paymentLevelInfo;
  }

  public IAPPaymentLevelInfo Get(int internalID)
  {
    IAPPaymentLevelInfo paymentLevelInfo;
    this.m_DataByInternalID.TryGetValue(internalID, out paymentLevelInfo);
    return paymentLevelInfo;
  }

  public int GetLevel(string id)
  {
    IAPPaymentLevelInfo paymentLevelInfo = this.Get(id);
    if (paymentLevelInfo != null)
      return paymentLevelInfo.payment_level;
    return 0;
  }

  public int GetLevel(int internalID)
  {
    IAPPaymentLevelInfo paymentLevelInfo = this.Get(internalID);
    if (paymentLevelInfo != null)
      return paymentLevelInfo.payment_level;
    return 0;
  }

  public List<string> GetIds()
  {
    List<string> stringList = new List<string>();
    using (Dictionary<string, IAPPaymentLevelInfo>.KeyCollection.Enumerator enumerator = this.m_DataByID.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        stringList.Add(current);
      }
    }
    return stringList;
  }

  public void Clear()
  {
    this.m_DataByID = (Dictionary<string, IAPPaymentLevelInfo>) null;
    this.m_DataByInternalID = (Dictionary<int, IAPPaymentLevelInfo>) null;
  }
}
