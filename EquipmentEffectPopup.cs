﻿// Decompiled with JetBrains decompiler
// Type: EquipmentEffectPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class EquipmentEffectPopup : Popup
{
  private List<ConfigEquipmentMainInfo> _allEquipment = new List<ConfigEquipmentMainInfo>();
  [SerializeField]
  private Color _equipmentSuitNameColor;
  [SerializeField]
  private Color _equipmentSuitBenefitOnColor;
  [SerializeField]
  private Color _equipmentSuitBenefitOffColor;
  [SerializeField]
  private Color _equipmentBenefitColor;
  [SerializeField]
  private EquipmentEffectTitleItem _titleItemTemplate;
  [SerializeField]
  private EquipmentEffectTitleItem _subTitleItemTemplate;
  [SerializeField]
  private EquipmentEffectDetailItem _detailItemTemplate;
  [SerializeField]
  private UITable _contentContainer;
  [SerializeField]
  private UIScrollView _contentScrollView;
  [SerializeField]
  private GameObject _rootEmptyTag;
  private bool _contentDirty;
  private EquipmentEffectPopup.Parameter _parameter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._parameter = orgParam as EquipmentEffectPopup.Parameter;
    this._allEquipment.Clear();
    using (List<InventoryItemData>.Enumerator enumerator = this._parameter.AllInventoryItemData.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InventoryItemData current = enumerator.Current;
        ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(this._parameter.BagType).GetData(current.internalId);
        if (data != null)
          this._allEquipment.Add(data);
        else
          D.error((object) string.Format("can not find equipment info : {0}", (object) current.internalId));
      }
    }
    this._titleItemTemplate.gameObject.SetActive(false);
    this._subTitleItemTemplate.gameObject.SetActive(false);
    this._detailItemTemplate.gameObject.SetActive(false);
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  protected void UpdateUI()
  {
    if (this._parameter.AllInventoryItemData.Count <= 0)
    {
      this._rootEmptyTag.SetActive(true);
    }
    else
    {
      this._rootEmptyTag.SetActive(false);
      this.AppendEquipmentBenefit();
      this.AppendEquipmentGemBenefit();
      this.AppendEquipmentSuitBenefit();
      this.AppendEquipmentSuitEnhancement();
    }
  }

  protected void AppendEquipmentBenefit()
  {
    this.InsertTitle(ScriptLocalization.Get("id_benefits", true));
    Dictionary<int, float> dictionary1 = new Dictionary<int, float>();
    using (List<InventoryItemData>.Enumerator enumerator1 = this._parameter.AllInventoryItemData.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        InventoryItemData current1 = enumerator1.Current;
        ConfigEquipmentPropertyInfo idAndEnhanceLevel = ConfigManager.inst.GetEquipmentProperty(this._parameter.BagType).GetDataByEquipIDAndEnhanceLevel(current1.internalId, current1.enhanced);
        if (idAndEnhanceLevel != null)
        {
          using (List<Benefits.BenefitValuePair>.Enumerator enumerator2 = idAndEnhanceLevel.benefits.GetBenefitsPairs().GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              Benefits.BenefitValuePair current2 = enumerator2.Current;
              if (!dictionary1.ContainsKey(current2.internalID))
                dictionary1.Add(current2.internalID, 0.0f);
              Dictionary<int, float> dictionary2;
              int internalId;
              (dictionary2 = dictionary1)[internalId = current2.internalID] = dictionary2[internalId] + current2.value;
            }
          }
        }
      }
    }
    using (List<ArtifactData>.Enumerator enumerator1 = this._parameter.AllArtifactData.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        ArtifactData current1 = enumerator1.Current;
        ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(current1.ArtifactId);
        if (artifactInfo != null)
        {
          using (List<Benefits.BenefitValuePair>.Enumerator enumerator2 = artifactInfo.benefits.GetBenefitsPairs().GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              Benefits.BenefitValuePair current2 = enumerator2.Current;
              if (!dictionary1.ContainsKey(current2.internalID))
                dictionary1.Add(current2.internalID, 0.0f);
              Dictionary<int, float> dictionary2;
              int internalId;
              (dictionary2 = dictionary1)[internalId = current2.internalID] = dictionary2[internalId] + current2.value;
            }
          }
        }
        else
          D.error((object) string.Format("can not find artifact data : {0}", (object) current1.ArtifactId));
      }
    }
    List<PropertyDefinition> propertyDefinitionList = new List<PropertyDefinition>();
    using (Dictionary<int, float>.Enumerator enumerator = dictionary1.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[enumerator.Current.Key];
        propertyDefinitionList.Add(dbProperty);
      }
    }
    propertyDefinitionList.Sort((Comparison<PropertyDefinition>) ((x, y) => x.Priority.CompareTo(y.Priority)));
    using (List<PropertyDefinition>.Enumerator enumerator = propertyDefinitionList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PropertyDefinition current = enumerator.Current;
        float num = dictionary1[current.InternalID];
        if (current != null)
          this.InsertDetail(string.Format("{0} {1}", (object) current.Name, (object) current.ConvertToDisplayString((double) num, false, true)), this._equipmentBenefitColor);
      }
    }
  }

  protected void AppendEquipmentGemBenefit()
  {
    Dictionary<int, float> dictionary1 = new Dictionary<int, float>();
    using (List<InventoryItemData>.Enumerator enumerator1 = this._parameter.AllInventoryItemData.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (List<GemData>.Enumerator enumerator2 = enumerator1.Current.inlaidGems.GetAllGemData().GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            ConfigEquipmentGemInfo data = ConfigManager.inst.DB_EquipmentGem.GetData(enumerator2.Current.ConfigId);
            if (data != null)
            {
              using (List<Benefits.BenefitValuePair>.Enumerator enumerator3 = data.benefits.GetBenefitsPairs().GetEnumerator())
              {
                while (enumerator3.MoveNext())
                {
                  Benefits.BenefitValuePair current = enumerator3.Current;
                  if (!dictionary1.ContainsKey(current.internalID))
                    dictionary1.Add(current.internalID, 0.0f);
                  Dictionary<int, float> dictionary2;
                  int internalId;
                  (dictionary2 = dictionary1)[internalId = current.internalID] = dictionary2[internalId] + current.value;
                }
              }
            }
          }
        }
      }
    }
    if (dictionary1.Count <= 0)
      return;
    this.InsertTitle(ScriptLocalization.Get("forge_armory_gemstone_socket_subtitle", true));
    List<PropertyDefinition> propertyDefinitionList = new List<PropertyDefinition>();
    using (Dictionary<int, float>.Enumerator enumerator = dictionary1.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[enumerator.Current.Key];
        propertyDefinitionList.Add(dbProperty);
      }
    }
    propertyDefinitionList.Sort((Comparison<PropertyDefinition>) ((x, y) => x.Priority.CompareTo(y.Priority)));
    using (List<PropertyDefinition>.Enumerator enumerator = propertyDefinitionList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PropertyDefinition current = enumerator.Current;
        float num = dictionary1[current.InternalID];
        if (current != null)
          this.InsertDetail(string.Format("{0} {1}", (object) current.Name, (object) current.ConvertToDisplayString((double) num, false, true)), this._equipmentBenefitColor);
      }
    }
  }

  protected void AppendEquipmentSuitBenefit()
  {
    Dictionary<int, int> dictionary1 = new Dictionary<int, int>();
    using (List<ConfigEquipmentMainInfo>.Enumerator enumerator = this._allEquipment.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ConfigEquipmentMainInfo current = enumerator.Current;
        if (current.suitGroup != 0)
        {
          if (!dictionary1.ContainsKey(current.suitGroup))
            dictionary1.Add(current.suitGroup, 0);
          Dictionary<int, int> dictionary2;
          int suitGroup;
          (dictionary2 = dictionary1)[suitGroup = current.suitGroup] = dictionary2[suitGroup] + 1;
        }
      }
    }
    this.InsertTitle(ScriptLocalization.Get("forge_enhance_resonance_set_subtitle", true));
    if (dictionary1.Count <= 0)
    {
      this.InsertSubTitle(string.Format("[9B9B9B]{0}", (object) ScriptLocalization.Get("forge_enhance_resonance_set_no_items_description", true)));
    }
    else
    {
      using (Dictionary<int, int>.KeyCollection.Enumerator enumerator1 = dictionary1.Keys.GetEnumerator())
      {
        while (enumerator1.MoveNext())
        {
          int current1 = enumerator1.Current;
          ConfigEquipmentSuitGroupInfo data = ConfigManager.inst.DB_EquipmentSuitGroup.GetData(current1);
          if (data == null)
          {
            D.error((object) string.Format("cannot find equipment suit group info: {0}", (object) current1));
          }
          else
          {
            this.InsertSubTitle(data.LocalName);
            List<ConfigEquipmentSuitMainInfo> equipmentSuitEffectMap = ConfigManager.inst.DB_EquipmentSuitMain.GetEquipmentSuitEffectMap(current1);
            if (equipmentSuitEffectMap != null)
            {
              int num = dictionary1[current1];
              using (List<ConfigEquipmentSuitMainInfo>.Enumerator enumerator2 = equipmentSuitEffectMap.GetEnumerator())
              {
                while (enumerator2.MoveNext())
                {
                  ConfigEquipmentSuitMainInfo current2 = enumerator2.Current;
                  using (List<Benefits.BenefitValuePair>.Enumerator enumerator3 = current2.benefits.GetBenefitsPairs().GetEnumerator())
                  {
                    while (enumerator3.MoveNext())
                    {
                      Benefits.BenefitValuePair current3 = enumerator3.Current;
                      this.InsertDetail(string.Format("{0}: {1} {2}", (object) ScriptLocalization.GetWithPara("suit_num_pieces_benefit", new Dictionary<string, string>()
                      {
                        {
                          "0",
                          current2.numberRequired.ToString()
                        }
                      }, true), (object) current3.propertyDefinition.Name, (object) current3.propertyDefinition.ConvertToDisplayString((double) current3.value, false, true)), num < current2.numberRequired ? this._equipmentSuitBenefitOffColor : this._equipmentSuitBenefitOnColor);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  protected void AppendEquipmentSuitEnhancement()
  {
    int num = int.MaxValue;
    if (this._parameter.AllInventoryItemData.Count < 6)
    {
      num = 0;
    }
    else
    {
      using (List<InventoryItemData>.Enumerator enumerator = this._parameter.AllInventoryItemData.GetEnumerator())
      {
        while (enumerator.MoveNext())
          num = Mathf.Min(enumerator.Current.enhanced, num);
      }
    }
    this.InsertTitle(ScriptLocalization.Get("forge_enhance_resonance_subtitle", true));
    ConfigEquipmentSuitEnhanceInfo lessEqual = ConfigManager.inst.DB_EquipmentSuitEnhance.GetLessEqual(num, this._parameter.BagType);
    ConfigEquipmentSuitEnhanceInfo greater = ConfigManager.inst.DB_EquipmentSuitEnhance.GetGreater(num, this._parameter.BagType);
    string str1 = string.Format("[FD8900]{0}:[-]", (object) ScriptLocalization.Get("forge_enhance_resonance_current_level", true));
    string benefitDetail1;
    if (lessEqual == null)
    {
      benefitDetail1 = str1 + "[9B9B9B]-[-]";
    }
    else
    {
      string withPara = ScriptLocalization.GetWithPara("forge_enhance_resonance_effect_description", new Dictionary<string, string>()
      {
        {
          "0",
          lessEqual.enhanceLevelReq.ToString()
        }
      }, true);
      benefitDetail1 = str1 + string.Format("[00ff00]{0} {1}+{2}%[-]", (object) withPara, (object) ScriptLocalization.Get("forge_enhance_resonance_benefit", true), (object) (float) ((double) lessEqual.enhanceValue * 100.0));
    }
    this.InsertDetail(benefitDetail1, Color.white);
    string str2 = string.Format("[FD8900]{0}:[-]", (object) ScriptLocalization.Get("forge_enhance_resonance_next_level", true));
    string benefitDetail2;
    if (greater == null)
    {
      benefitDetail2 = str2 + "-";
    }
    else
    {
      string withPara = ScriptLocalization.GetWithPara("forge_enhance_resonance_effect_description", new Dictionary<string, string>()
      {
        {
          "0",
          greater.enhanceLevelReq.ToString()
        }
      }, true);
      benefitDetail2 = str2 + string.Format("[9B9B9B]{0} {1}+{2}%[-]", (object) withPara, (object) ScriptLocalization.Get("forge_enhance_resonance_benefit", true), (object) (float) ((double) greater.enhanceValue * 100.0));
    }
    this.InsertDetail(benefitDetail2, Color.white);
  }

  protected void InsertTitle(string name)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._titleItemTemplate.gameObject);
    gameObject.transform.SetParent(this._contentContainer.transform);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    gameObject.GetComponent<EquipmentEffectTitleItem>().SetData(name);
    this._contentDirty = true;
  }

  protected void InsertSubTitle(string name)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._subTitleItemTemplate.gameObject);
    gameObject.transform.SetParent(this._contentContainer.transform);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    gameObject.GetComponent<EquipmentEffectTitleItem>().SetData(name);
    this._contentDirty = true;
  }

  protected void InsertDetail(string benefitDetail, Color color)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._detailItemTemplate.gameObject);
    gameObject.transform.SetParent(this._contentContainer.transform);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    gameObject.GetComponent<EquipmentEffectDetailItem>().SetData(benefitDetail, color);
    this._contentDirty = true;
  }

  protected void Update()
  {
    if (!this._contentDirty)
      return;
    this._contentDirty = false;
    this._contentContainer.repositionNow = true;
    this._contentContainer.Reposition();
    this._contentScrollView.ResetPosition();
  }

  public void OnButtonCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public List<InventoryItemData> AllInventoryItemData = new List<InventoryItemData>();
    public List<ArtifactData> AllArtifactData = new List<ArtifactData>();
    public BagType BagType;
  }
}
