﻿// Decompiled with JetBrains decompiler
// Type: DungeonMopupItemSlotRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DungeonMopupItemSlotRenderer : MonoBehaviour
{
  public UIToggle _toggle;
  public UITexture _icon;
  public UILabel _quantity;
  public UILabel _weight;
  private System.Action<DungeonMopupItemSlotRenderer> _callback;
  private ItemStaticInfo _itemInfo;
  private int _itemCount;

  public void SetData(int itemId, int count, System.Action<DungeonMopupItemSlotRenderer> callback)
  {
    this._callback = callback;
    this._itemInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    this._itemCount = count;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this._icon, this._itemInfo.ImagePath, (System.Action<bool>) null, true, true, string.Empty);
    this._quantity.text = this._itemCount.ToString();
    this._weight.text = (this._itemCount * this._itemInfo.DungeonWeight).ToString();
  }

  public ItemStaticInfo ItemInfo
  {
    get
    {
      return this._itemInfo;
    }
  }

  public int ItemCount
  {
    get
    {
      return this._itemCount;
    }
  }

  public int Weight
  {
    get
    {
      return this._itemInfo.DungeonWeight * this._itemCount;
    }
  }

  public void OnToggleChanged()
  {
    if (!this._toggle.value || this._callback == null)
      return;
    this._callback(this);
  }

  public bool Selected
  {
    get
    {
      return this._toggle.value;
    }
    set
    {
      this._toggle.Set(value);
    }
  }
}
