﻿// Decompiled with JetBrains decompiler
// Type: AltarSummonOrbitController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class AltarSummonOrbitController : MonoBehaviour
{
  private static Dictionary<int, AltarSummonOrbitController.ItemDepthInfo> depthDict = new Dictionary<int, AltarSummonOrbitController.ItemDepthInfo>();
  private static List<AltarSummonOrbitController.ItemDepthInfo> depthList = new List<AltarSummonOrbitController.ItemDepthInfo>();
  private float radius = 800f;
  private float startRaduis = 800f;
  private float angularSpeed = 0.5f;
  private float startAngularSpeed = 0.5f;
  private float endAngularSpeed = 20f;
  private List<AltarSummonItemComponent> items = new List<AltarSummonItemComponent>();
  private List<float> angles = new List<float>();
  private GameObjectPool objectPool = new GameObjectPool();
  public Transform _root;
  public Camera _camera;
  public AltarSummonItemComponent _template;
  private float endRadius;
  private int requiredSize;
  public System.Action onItemClickHandler;
  public System.Action onAnimationStartHandler;
  public System.Action onAnimationEndHandler;
  private AltarSummonOrbitController.OrbitStatus status;

  public AltarSummonOrbitController.OrbitStatus Status
  {
    set
    {
      if (this.status == value)
        return;
      this.status = value;
      if (this.status != AltarSummonOrbitController.OrbitStatus.Animating)
        return;
      this.PlayAnimation();
    }
    get
    {
      return this.status;
    }
  }

  private void Update()
  {
    if (this.status == AltarSummonOrbitController.OrbitStatus.None)
      return;
    this.UpdateDepth();
    for (int index1 = 0; index1 < this.requiredSize; ++index1)
    {
      List<float> angles;
      int index2;
      (angles = this.angles)[index2 = index1] = angles[index2] + Time.deltaTime * this.angularSpeed;
      AltarSummonItemComponent summonItemComponent = this.items[index1];
      summonItemComponent.transform.localPosition = new Vector3()
      {
        x = this.radius * Mathf.Cos(this.angles[index1]),
        y = this.radius * Mathf.Sin(this.angles[index1])
      };
      int depth = AltarSummonOrbitController.depthDict[summonItemComponent.id].depth;
      summonItemComponent.gameObject.GetComponent<UIPanel>().depth = depth;
    }
  }

  private void UpdateDepth()
  {
    AltarSummonOrbitController.depthList.Clear();
    for (int index = 0; index < this.requiredSize; ++index)
    {
      AltarSummonOrbitController.ItemDepthInfo itemDepthInfo = AltarSummonOrbitController.depthDict[this.items[index].id];
      itemDepthInfo.height = this.items[index].transform.localPosition.y;
      AltarSummonOrbitController.depthList.Add(itemDepthInfo);
    }
    AltarSummonOrbitController.depthList.Sort((Comparison<AltarSummonOrbitController.ItemDepthInfo>) ((x, y) =>
    {
      if ((double) x.height < (double) y.height)
        return 1;
      return (double) x.height > (double) y.height ? -1 : 0;
    }));
    for (int index = 0; index < this.requiredSize; ++index)
      AltarSummonOrbitController.depthList[index].depth = 10 + index;
  }

  private void PlayAnimation()
  {
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) 0.0f, (object) "to", (object) 1f, (object) "easetype", (object) iTween.EaseType.easeInExpo, (object) "onupdate", (object) "OnAnimationUpdate", (object) "oncomplete", (object) "OnAnimationComplete", (object) "time", (object) 2f));
    if (this.onAnimationStartHandler == null)
      return;
    this.onAnimationStartHandler();
  }

  private void OnAnimationUpdate(float percent)
  {
    this.radius = this.startRaduis + (this.endRadius - this.startRaduis) * percent;
    this.angularSpeed = this.startAngularSpeed + (this.endAngularSpeed - this.startAngularSpeed) * percent;
  }

  private void OnAnimationComplete()
  {
    this.gameObject.SetActive(false);
    if (this.onAnimationEndHandler == null)
      return;
    this.onAnimationEndHandler();
  }

  public void Init(int itemId)
  {
    this.requiredSize = AltarSummonHelper.GetRequiredSize(itemId);
    this.SetupUI();
  }

  private void SetupUI()
  {
    this.objectPool.Initialize(this._template.gameObject, this._root.gameObject);
    this.ResetItems();
    for (int index = 0; index < this.requiredSize; ++index)
    {
      GameObject gameObject = this.objectPool.Allocate();
      gameObject.transform.parent = this._root;
      gameObject.transform.localScale = Vector3.one;
      gameObject.SetActive(true);
      float f = (float) (index * 2) * 3.141593f / (float) this.requiredSize;
      this.angles.Add(f);
      gameObject.transform.localPosition = new Vector3()
      {
        x = this.radius * Mathf.Cos(f),
        y = this.radius * Mathf.Sin(f)
      };
      AltarSummonItemComponent component = gameObject.GetComponent<AltarSummonItemComponent>();
      component.id = index;
      component.onItemClick = new System.Action(this.OnItemClick);
      component.FeedData((IComponentData) new AltarSummonItemComponentData()
      {
        rewardId = 0
      });
      this.items.Add(component);
      AltarSummonOrbitController.ItemDepthInfo itemDepthInfo = new AltarSummonOrbitController.ItemDepthInfo(index);
      AltarSummonOrbitController.depthDict.Add(index, itemDepthInfo);
    }
    this.status = AltarSummonOrbitController.OrbitStatus.Idle;
  }

  public void UpdateUI(List<int> rewardIds)
  {
    for (int index = 0; index < this.requiredSize; ++index)
      this.items[index].FeedData((IComponentData) new AltarSummonItemComponentData()
      {
        rewardId = rewardIds[index]
      });
  }

  private void ResetItems()
  {
    int index = 0;
    for (int count = this.items.Count; index < count; ++index)
      this.objectPool.Release(this.items[index].gameObject);
    this.items.Clear();
    this.angles.Clear();
    AltarSummonOrbitController.depthDict.Clear();
  }

  private void OnItemClick()
  {
    if (this.onItemClickHandler == null)
      return;
    this.onItemClickHandler();
  }

  public enum OrbitStatus
  {
    None,
    Idle,
    Animating,
  }

  private class ItemDepthInfo
  {
    public int id;
    public float height;
    public int depth;

    public ItemDepthInfo(int id)
    {
      this.id = id;
      this.height = 0.0f;
      this.depth = 0;
    }
  }
}
