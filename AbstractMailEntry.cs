﻿// Decompiled with JetBrains decompiler
// Type: AbstractMailEntry
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;

public class AbstractMailEntry
{
  protected long _fromUID = -1;
  protected long _toUID = -1;
  public const long InvalidMailID = 0;
  public System.Action mailUpdated;
  public MailAPI mailAPI;
  public MailController controller;
  protected MailStatus _status;
  protected MailCategory _category;
  protected string _from_portrait;
  protected string _from_icon;
  protected int _from_lord_title;
  protected int _attachment_status;
  private bool _isFav;
  private bool _isRead;
  private bool _isShared;
  protected string _subject;
  protected long _mailID;
  protected long _time;
  protected string _sender;
  protected string _recipient;
  protected ArrayList _recipients;

  public Hashtable body
  {
    get
    {
      return this.data[(object) "data"] as Hashtable;
    }
    set
    {
      this.data[(object) "data"] = (object) value;
    }
  }

  public Hashtable data { get; protected set; }

  public virtual void ApplyData(Hashtable ht)
  {
    if (ht == null)
      return;
    this.data = ht;
    string s1 = ht[(object) "mail_id"] as string;
    if (s1 != null)
      this._mailID = long.Parse(s1);
    string s2 = ht[(object) "status"] as string;
    if (s2 != null)
      this._status = (MailStatus) int.Parse(s2);
    string s3 = ht[(object) "attachment_status"] as string;
    if (s3 != null)
      this._attachment_status = int.Parse(s3);
    string s4 = ht[(object) "ctime"] as string;
    if (s4 != null)
      this._time = long.Parse(s4);
    string s5 = ht[(object) "from_uid"] as string;
    if (s5 != null)
      this._fromUID = long.Parse(s5);
    string s6 = ht[(object) "uid"] as string;
    if (s6 != null)
      this._toUID = long.Parse(s6);
    this._isShared = this._toUID != PlayerData.inst.uid;
    int num1;
    switch (ht[(object) "status"] as string)
    {
      case "0":
        num1 = 0;
        break;
      case null:
label_18:
        int num2;
        switch (ht[(object) "favorite"] as string)
        {
          case "0":
            num2 = 0;
            break;
          case null:
label_22:
            this._subject = ht[(object) "subject"] as string;
            this._sender = ht[(object) "from_uname"] as string;
            string str1 = ht[(object) "to_username"] as string;
            this._recipient = !string.IsNullOrEmpty(str1) ? str1 : PlayerData.inst.userData.userName;
            string s7 = ht[(object) "type"] as string;
            if (s7 != null)
              this.type = (MailType) int.Parse(s7);
            string s8 = ht[(object) "category"] as string;
            if (s8 != null)
              this._category = (MailCategory) int.Parse(s8);
            string str2 = ht[(object) "from_portrait"] as string;
            if (str2 != null)
              this._from_portrait = str2;
            string str3 = ht[(object) "from_icon"] as string;
            if (str3 != null)
              this._from_icon = str3;
            string s9 = ht[(object) "from_lord_title"] as string;
            if (s9 != null)
              int.TryParse(s9, out this._from_lord_title);
            this.body = ht[(object) "data"] as Hashtable;
            if (this.mailUpdated == null)
              return;
            this.mailUpdated();
            return;
          default:
            num2 = 1;
            break;
        }
        this._isFav = num2 != 0;
        goto label_22;
      default:
        num1 = 1;
        break;
    }
    this._isRead = num1 != 0;
    goto label_18;
  }

  public MailStatus status
  {
    get
    {
      return this._status;
    }
    set
    {
      this._status = value;
    }
  }

  public MailCategory category
  {
    get
    {
      return this._category;
    }
  }

  public string from_portrait
  {
    get
    {
      return this._from_portrait;
    }
  }

  public string from_icon
  {
    get
    {
      return this._from_icon;
    }
  }

  public int from_lord_title
  {
    get
    {
      return this._from_lord_title;
    }
  }

  public int attachment_status
  {
    get
    {
      return this._attachment_status;
    }
    set
    {
      this._attachment_status = value;
      this.data[(object) nameof (attachment_status)] = (object) this._attachment_status;
      if (this.mailUpdated != null)
        this.mailUpdated();
      if (this.controller == null)
        return;
      this.controller.MailGainAttachChanged(this);
    }
  }

  public MailType type { get; set; }

  public bool bodyLoaded
  {
    get
    {
      return this.body != null;
    }
  }

  public bool isFavorite
  {
    get
    {
      return this._isFav;
    }
    set
    {
      if (this._isFav == value)
        return;
      this._isFav = value;
      if (this.mailUpdated == null)
        return;
      this.mailUpdated();
    }
  }

  public bool isRead
  {
    get
    {
      return this._isRead;
    }
    set
    {
      if (this._isRead == value)
        return;
      this._isRead = value;
      if (this.mailUpdated == null)
        return;
      this.mailUpdated();
    }
  }

  public bool isShared
  {
    get
    {
      return this._isShared;
    }
    set
    {
      this._isShared = value;
    }
  }

  public string subject
  {
    get
    {
      return this._subject;
    }
    set
    {
      this._subject = value;
    }
  }

  public string displaySubject
  {
    get
    {
      if (this.subject.Contains(":"))
        return this.subject.Substring(0, this.subject.IndexOf(":"));
      return this.subject;
    }
  }

  public long mailID
  {
    get
    {
      return this._mailID;
    }
    set
    {
      this._mailID = value;
    }
  }

  public string time
  {
    get
    {
      DateTime dateTime = Utils.ServerTime2DateTime(this._time);
      return string.Format("{0}/{1}/{2}  {3}:{4}:{5}", (object) dateTime.Year.ToString("00"), (object) dateTime.Month.ToString("00"), (object) dateTime.Day.ToString("00"), (object) dateTime.Hour.ToString("00"), (object) dateTime.Minute.ToString("00"), (object) dateTime.Second.ToString("00"));
    }
  }

  public long timeAsInt
  {
    get
    {
      return this._time;
    }
    set
    {
      this._time = value;
    }
  }

  public string sender
  {
    get
    {
      return this._sender;
    }
    set
    {
      this._sender = value;
    }
  }

  public long fromUID
  {
    get
    {
      return this._fromUID;
    }
    set
    {
      this._fromUID = value;
    }
  }

  public long toUID
  {
    get
    {
      return this._toUID;
    }
    set
    {
      this._toUID = value;
    }
  }

  public string recipient
  {
    get
    {
      return this._recipient;
    }
    set
    {
      this._recipient = value;
    }
  }

  public ArrayList recipients
  {
    get
    {
      return this._recipients;
    }
    set
    {
      this._recipients = value;
    }
  }

  public virtual void Display()
  {
    if (this.isRead)
      return;
    this.controller.MarkMailAsRead((int) this.category, new List<AbstractMailEntry>()
    {
      this
    }, 1 != 0, 1 != 0);
  }

  public Hashtable HtTitle
  {
    get
    {
      return this.data[(object) "title"] as Hashtable;
    }
  }

  public Hashtable HtBody
  {
    get
    {
      return this.data[(object) "body"] as Hashtable;
    }
  }

  public string GetTitleString()
  {
    if (this.type == MailType.MAIL_TYPE_SYSTEM_NOTICE_MULTI_LANG)
    {
      string currentLanguageCode = LocalizationManager.CurrentLanguageCode;
      Hashtable hashtable1 = (this.HtTitle[(object) "params"] as ArrayList)[0] as Hashtable;
      if (hashtable1 != null)
      {
        Hashtable hashtable2 = hashtable1[(object) "body"] as Hashtable;
        if (hashtable2.ContainsKey((object) currentLanguageCode))
          return hashtable2[(object) currentLanguageCode].ToString();
        if (hashtable2.ContainsKey((object) "en"))
          return hashtable2[(object) "en"].ToString();
      }
      return string.Empty;
    }
    if (this.HtTitle == null)
      return string.Empty;
    ArrayList arrayList = this.HtTitle[(object) "params"] as ArrayList;
    string empty = string.Empty;
    if (this.HtTitle != null && this.HtTitle.ContainsKey((object) "key") && this.HtTitle[(object) "key"] != null)
      empty = this.HtTitle[(object) "key"].ToString();
    Dictionary<string, string> para = new Dictionary<string, string>();
    if (arrayList != null)
    {
      for (int index = 0; index < arrayList.Count; ++index)
      {
        if (arrayList[index] != null)
          para.Add(index.ToString(), ScriptLocalization.Get(arrayList[index].ToString(), true));
      }
    }
    string withPara = ScriptLocalization.GetWithPara(empty, para, true);
    int startIndex = 0;
    if (withPara.StartsWith("["))
      startIndex = withPara.IndexOf("]") + 1;
    return withPara.Insert(startIndex, this.GetTitlePrefix());
  }

  public bool IsInAbyss()
  {
    Hashtable hashtable = this.data[(object) "data"] as Hashtable;
    return hashtable != null && hashtable.ContainsKey((object) "is_in_abyss") && hashtable[(object) "is_in_abyss"].ToString().ToLower() == "true";
  }

  public bool IsInAllianceWar()
  {
    Hashtable hashtable = this.data[(object) "data"] as Hashtable;
    return hashtable != null && hashtable.ContainsKey((object) "is_in_alliance_war") && hashtable[(object) "is_in_alliance_war"].ToString().ToLower() == "true";
  }

  public bool IsInMerlinTowerRevenge()
  {
    Hashtable hashtable = this.data[(object) "data"] as Hashtable;
    return hashtable != null && hashtable.ContainsKey((object) "is_magic_tower_revenge") && hashtable[(object) "is_magic_tower_revenge"].ToString().ToLower() == "true";
  }

  public bool IsInMerlinTowerAttack()
  {
    Hashtable hashtable = this.data[(object) "data"] as Hashtable;
    return hashtable != null && hashtable.ContainsKey((object) "is_magic_tower_attack") && hashtable[(object) "is_magic_tower_attack"].ToString().ToLower() == "true";
  }

  public bool IsInMerlinTowerDefense()
  {
    Hashtable hashtable = this.data[(object) "data"] as Hashtable;
    return hashtable != null && hashtable.ContainsKey((object) "is_magic_tower_defense") && hashtable[(object) "is_magic_tower_defense"].ToString().ToLower() == "true";
  }

  public bool IsInMerlinTowerGather()
  {
    Hashtable hashtable = this.data[(object) "data"] as Hashtable;
    return hashtable != null && hashtable.ContainsKey((object) "is_magic_tower_gather") && hashtable[(object) "is_magic_tower_gather"].ToString().ToLower() == "true";
  }

  public string GetTitlePrefix()
  {
    if (this.IsInAbyss())
      return Utils.XLAT("mail_title_fire_kingdom_victory");
    if (this.IsInAllianceWar())
      return string.Format("({0})", (object) Utils.XLAT("alliance_warfare_name"));
    return string.Empty;
  }

  public virtual string GetBodyString()
  {
    if (this.HtBody == null)
    {
      D.error((object) "mail body is empty.");
      return string.Empty;
    }
    if (this.type == MailType.MAIL_TYPE_SYSTEM_VERSION || this.type == MailType.MAIL_TYPE_SYSTEM_NOTICE_MULTI_LANG)
    {
      string currentLanguageCode = LocalizationManager.CurrentLanguageCode;
      Hashtable hashtable1 = (this.HtBody[(object) "params"] as ArrayList)[0] as Hashtable;
      string empty = string.Empty;
      if (hashtable1 != null)
      {
        Hashtable hashtable2 = hashtable1[(object) "body"] as Hashtable;
        if (hashtable2.ContainsKey((object) currentLanguageCode))
          empty = hashtable2[(object) currentLanguageCode].ToString();
        else if (hashtable2.ContainsKey((object) "en"))
          empty = hashtable2[(object) "en"].ToString();
      }
      return ScriptLocalization.Get(empty, true);
    }
    if (this.type == MailType.MAIL_TYPE_SYSTEM_USER_SENT_ATTACHMENT)
    {
      string empty = string.Empty;
      if (this.HtBody.ContainsKey((object) "content"))
        empty = this.HtBody[(object) "content"].ToString();
      return empty;
    }
    ArrayList arrayList = this.HtBody[(object) "params"] as ArrayList;
    string empty1 = string.Empty;
    if (this.HtBody != null && this.HtBody.ContainsKey((object) "key") && this.HtBody[(object) "key"] != null)
      empty1 = this.HtBody[(object) "key"].ToString();
    Dictionary<string, string> para = new Dictionary<string, string>();
    if (arrayList != null)
    {
      for (int index = 0; index < arrayList.Count; ++index)
      {
        if (arrayList[index] != null)
          para.Add(index.ToString(), ScriptLocalization.Get(arrayList[index].ToString(), true));
      }
    }
    return ScriptLocalization.GetWithPara(empty1, para, true);
  }

  public string GetPotraitPath()
  {
    if (this.category == MailCategory.Mod && !PlayerData.inst.moderatorInfo.IsModerator)
      return "Texture/Mail/icon_mail_mod";
    if (this.type == MailType.MAIL_TYPE_NORMAL)
      return "Texture/Hero/Portrait_Icon/player_portrait_icon_" + this.from_portrait;
    return "Texture/Mail/icon_mail_" + (object) this.type;
  }

  public string GetCoustomIcon()
  {
    if (this.type == MailType.MAIL_TYPE_NORMAL)
      return this.from_icon;
    return (string) null;
  }

  public int GetLordTitle()
  {
    if (this.category == MailCategory.Mod && !PlayerData.inst.moderatorInfo.IsModerator || this.type != MailType.MAIL_TYPE_NORMAL)
      return 0;
    return this.from_lord_title;
  }

  public string GetBackGroundPath()
  {
    return "Texture/Mail/icon_background_" + this.from_portrait;
  }

  public override string ToString()
  {
    return string.Format("[AbstractMailView status={0}, recipient={1}, sender={2}, subject={3}, mailID={4}, isRead={5}, isFav={6}, data={7}, type={8}]", (object) this.status, (object) this.recipient, (object) this.sender, (object) this.subject, (object) this.mailID, (object) this.isRead, (object) this._isFav, (object) Utils.Object2Json((object) this.data), (object) this.type.ToString());
  }

  public int AttachmentExpirationTime
  {
    get
    {
      return int.Parse(this.data[(object) "attachment_expiration_time"].ToString());
    }
  }

  public int AttachmentExpirationLeftTime
  {
    get
    {
      return this.AttachmentExpirationTime - NetServerTime.inst.ServerTimestamp;
    }
  }

  public virtual bool CanReceiveAttachment()
  {
    Hashtable hashtable = this.data[(object) "attachment"] as Hashtable;
    if (hashtable != null && hashtable.Count > 0 && this.AttachmentExpirationLeftTime > 0)
      return this.attachment_status == 1;
    return false;
  }

  public bool LanguageMatch()
  {
    bool flag = true;
    Hashtable hashtable = this.data[(object) "body"] as Hashtable;
    if (hashtable != null && hashtable.ContainsKey((object) "only_for_lang"))
      flag = hashtable[(object) "only_for_lang"].ToString() == LocalizationManager.CurrentLanguageCode;
    return flag;
  }
}
