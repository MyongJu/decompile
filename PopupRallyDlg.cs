﻿// Decompiled with JetBrains decompiler
// Type: PopupRallyDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PopupRallyDlg : UI.Dialog
{
  private long _rallyId = -1;
  public List<RallySlotBar> slots = new List<RallySlotBar>();
  public const string RALLY_ID = "rally_id";
  private int _rallyWaitStartTime;
  private int _rallyWaitEndTime;
  private bool secondFlag;
  [SerializeField]
  private PopupRallyDlg.Panel panel;

  private void Setup(long rallyId)
  {
    this.SetItemIcons(0);
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this._rallyId);
    if (rallyData == null)
    {
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
    {
      AllianceData allianceData1 = DBManager.inst.DB_Alliance.Get(rallyData.ownerAllianceId);
      if (allianceData1 != null)
        this.panel.ownerAllianceName.text = allianceData1.allianceFullName;
      CityData cityData1 = DBManager.inst.DB_City.Get(rallyData.ownerCityId);
      if (cityData1 != null)
      {
        this.panel.ownerCiytName.text = cityData1.cityName;
        this.panel.ownerCoordinateK.text = cityData1.cityLocation.K.ToString();
        this.panel.ownerCoordinateX.text = cityData1.cityLocation.X.ToString();
        this.panel.ownerCoordinateY.text = cityData1.cityLocation.Y.ToString();
      }
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData("war_rally", cityData1.buildings.level_hallOfWar);
      AllianceData allianceData2 = DBManager.inst.DB_Alliance.Get(rallyData.targetAllianceId);
      if (allianceData2 != null)
        this.panel.targetAllianceName.text = allianceData2.allianceFullName;
      CityData cityData2 = DBManager.inst.DB_City.Get(rallyData.targetCityId);
      if (cityData2 != null)
      {
        this.panel.targetCityName.text = cityData2.cityName;
        this.panel.targetCoordinateK.text = cityData2.cityLocation.K.ToString();
        this.panel.targetCoordinateX.text = cityData2.cityLocation.X.ToString();
        this.panel.targetCoordinateY.text = cityData2.cityLocation.Y.ToString();
      }
      this._rallyWaitStartTime = rallyData.waitTimeDuration.startTime;
      this._rallyWaitEndTime = rallyData.waitTimeDuration.endTime;
      this.panel.rallyTroopsCount.text = string.Format("{0}/{1}", (object) Utils.ConvertNumberToNormalString((int) rallyData.totalTroopsCount), (object) Utils.ConvertNumberToNormalString((int) data.Benefit_Value_1));
      this.OnSecond(NetServerTime.inst.ServerTimestamp);
      if (this.slots.Count < 50)
      {
        for (int count = this.slots.Count; count < 50; ++count)
        {
          GameObject gameObject = NGUITools.AddChild(this.panel.rallySlotTable.gameObject, this.panel.orgRallySlot.gameObject);
          gameObject.name = string.Format("{0}{1}", count >= 10 ? (object) string.Empty : (object) "0", (object) count);
          gameObject.SetActive(true);
          gameObject.transform.localPosition = new Vector3(0.0f, (float) (-350 * count), 0.0f);
          RallySlotBar component = gameObject.GetComponent<RallySlotBar>();
          component.Setup(count + 1, RallySlotInfo.LockedSlot(), this._rallyId);
          component.onUnlockSlotClick += new System.Action(this.OnUnlockSlotClick);
          component.onJoinSlotClick += new System.Action(this.OnJoinButtonClick);
          component.onSpeedUpClick += new System.Action<long>(this.OnSpeedUpButtonClick);
          this.slots.Add(component);
        }
      }
      int index1 = 0;
      this.panel.scrolView.ResetPosition();
      this.slots[index1].Setup(index1 + 1, rallyData.slotsInfo_joined[rallyData.ownerUid], this._rallyId);
      int index2 = index1 + 1;
      Dictionary<long, RallySlotInfo>.ValueCollection.Enumerator enumerator1 = rallyData.slotsInfo_joined.Values.GetEnumerator();
      while (enumerator1.MoveNext())
      {
        if (enumerator1.Current.uid != rallyData.ownerUid)
        {
          this.slots[index2].Setup(index2 + 1, enumerator1.Current, this._rallyId);
          ++index2;
        }
      }
      Dictionary<long, RallySlotInfo>.ValueCollection.Enumerator enumerator2 = rallyData.slotsInfo_unlockForUser.Values.GetEnumerator();
      while (enumerator2.MoveNext())
      {
        this.slots[index2].Setup(index2 + 1, enumerator2.Current, this._rallyId);
        ++index2;
      }
      for (int index3 = index2; index3 < rallyData.unlockSlotNumber; ++index3)
        this.slots[index3].Setup(index3 + 1, RallySlotInfo.UnlockedSlot(), this._rallyId);
      for (int unlockSlotNumber = rallyData.unlockSlotNumber; unlockSlotNumber < 50; ++unlockSlotNumber)
        this.slots[unlockSlotNumber].Setup(unlockSlotNumber + 1, RallySlotInfo.LockedSlot(), this._rallyId);
      this.StartCoroutine("DelayRepsoition", (object) 0.1f);
    }
  }

  private void DelayRepsoition()
  {
    this.panel.rallySlotTable.repositionNow = true;
  }

  public void OnGotoOwnerClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this._rallyId);
    if (rallyData == null)
      return;
    CityData cityData = DBManager.inst.DB_City.Get(rallyData.ownerCityId);
    if (cityData != null && !MapUtils.CanGotoTarget(cityData.cityLocation.K))
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    else if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PVPMode)
    {
      PVPSystem.Instance.Map.GotoLocation(DBManager.inst.DB_City.Get(rallyData.ownerCityId).cityLocation, false);
    }
    else
    {
      UIManager.inst.cityCamera.gameObject.SetActive(false);
      GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
      PVPMap.PendingGotoRequest = DBManager.inst.DB_City.Get(rallyData.ownerCityId).cityLocation;
    }
  }

  public void OnGotoTargetClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this._rallyId);
    if (rallyData == null)
      return;
    CityData cityData = DBManager.inst.DB_City.Get(rallyData.targetCityId);
    if (cityData != null && !MapUtils.CanGotoTarget(cityData.cityLocation.K))
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    else if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PVPMode)
    {
      PVPSystem.Instance.Map.GotoLocation(DBManager.inst.DB_City.Get(rallyData.targetCityId).cityLocation, false);
    }
    else
    {
      UIManager.inst.cityCamera.gameObject.SetActive(false);
      GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
      PVPMap.PendingGotoRequest = DBManager.inst.DB_City.Get(rallyData.targetCityId).cityLocation;
    }
  }

  public void OnUnlockSlotClick()
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this._rallyId);
    int level = 50 - rallyData.lockSlotNumber + 1;
    if (level <= 3 || level >= 50)
      return;
    if (ConfigManager.inst.DB_RallySlot != null)
    {
      int itemRequrieInternal = ConfigManager.inst.DB_RallySlot.GetItemRequrieInternal(level);
      int itemRequireCount = ConfigManager.inst.DB_RallySlot.GetItemRequireCount(level);
      if (itemRequrieInternal == 0)
        return;
      int itemCountByShopId = ItemBag.Instance.GetItemCountByShopID(itemRequrieInternal);
      ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(itemRequrieInternal);
      if (dataByInternalId == null)
        return;
      int price = dataByInternalId.Price;
      if (itemCountByShopId >= itemRequireCount)
      {
        this.panel.rallyUnlockSlotDlg.SetActive(true);
        this.SetItemIcons(0);
        this.panel.BT_unlockForMe.isEnabled = !rallyData.isJoined;
        this.panel.slotItemCount.text = itemCountByShopId.ToString();
        this.panel.slotItemCost1.text = itemRequireCount.ToString();
        this.panel.slotItemCost2.text = itemRequireCount.ToString();
      }
      else
      {
        this.panel.rallyUnlockSlotWithCoinDlg.SetActive(true);
        this.panel.BT_unlockForMeWithCoin.isEnabled = !rallyData.isJoined;
        this.panel.slotItemCountWithCoin.text = itemCountByShopId.ToString();
        this.panel.unlockSlotWithCoinWarningLabel.text = string.Format("{0} War Commission is needed to unlock this slot", (object) itemRequireCount);
        this.panel.unlockCostWithCoin1.text = ((itemRequireCount - itemCountByShopId) * price).ToString();
        this.panel.unlockCostWithCoin2.text = ((itemRequireCount - itemCountByShopId) * price).ToString();
        this.SetItemIcons(1);
      }
    }
    else
    {
      D.error((object) "The rally_slots files is not exist or decode failed");
      if (ItemBag.Instance.GetItemCountByShopID("shopitem_commission") > 0)
      {
        this.panel.rallyUnlockSlotDlg.SetActive(true);
        this.panel.BT_unlockForMe.isEnabled = !rallyData.isJoined;
        this.panel.slotItemCount.text = ItemBag.Instance.GetItemCountByShopID("shopitem_commission").ToString();
        this.SetItemIcons(0);
      }
      else
      {
        this.panel.rallyUnlockSlotWithCoinDlg.SetActive(true);
        this.panel.BT_unlockForMeWithCoin.isEnabled = !rallyData.isJoined;
        this.panel.slotItemCount.text = ItemBag.Instance.GetItemCountByShopID("shopitem_commission").ToString();
        this.SetItemIcons(1);
      }
    }
  }

  private void SetItemIcons(int view = 0)
  {
    string path = "Texture/ItemIcons/" + ConfigManager.inst.DB_Shop.GetShopData("shopitem_commission").Image;
    if (view == 0)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.slotItemIcon, path, (System.Action<bool>) null, true, false, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.slotItemIcon1, path, (System.Action<bool>) null, true, false, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.slotItemIcon2, path, (System.Action<bool>) null, true, false, string.Empty);
    }
    else
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.slotItemIcon3, path, (System.Action<bool>) null, true, false, string.Empty);
  }

  private void ReleaseItemIcons()
  {
    BuilderFactory.Instance.Release((UIWidget) this.panel.slotItemIcon);
    BuilderFactory.Instance.Release((UIWidget) this.panel.slotItemIcon1);
    BuilderFactory.Instance.Release((UIWidget) this.panel.slotItemIcon2);
    BuilderFactory.Instance.Release((UIWidget) this.panel.slotItemIcon3);
  }

  public void OnUnlockSlotCloseClick()
  {
    this.panel.rallyUnlockSlotDlg.SetActive(false);
    this.ReleaseItemIcons();
  }

  public void OnUnlockSlotWithCoinCloseClick()
  {
    this.panel.rallyUnlockSlotWithCoinDlg.SetActive(false);
    this.ReleaseItemIcons();
  }

  public void UnlockSlotForMe()
  {
    MessageHub.inst.GetPortByAction("Rally:unlockSlot").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "alliance_id", (object) PlayerData.inst.allianceId, (object) "rally_id", (object) this._rallyId, (object) "private", (object) true), (System.Action<bool, object>) null, true);
    this.panel.rallyUnlockSlotDlg.SetActive(false);
    this.panel.rallyUnlockSlotWithCoinDlg.SetActive(false);
    this.ReleaseItemIcons();
  }

  public void UnlockSlot()
  {
    MessageHub.inst.GetPortByAction("Rally:unlockSlot").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "alliance_id", (object) PlayerData.inst.allianceId, (object) "rally_id", (object) this._rallyId, (object) "private", (object) false), (System.Action<bool, object>) null, true);
    this.panel.rallyUnlockSlotDlg.SetActive(false);
    this.panel.rallyUnlockSlotWithCoinDlg.SetActive(false);
    this.ReleaseItemIcons();
  }

  public void OnJoinButtonClick()
  {
    CityData cityData = DBManager.inst.DB_City.Get(DBManager.inst.DB_Rally.Get(this._rallyId).ownerCityId);
    UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
    {
      marchType = MarchAllocDlg.MarchType.joinRally,
      location = cityData.cityLocation,
      rallyId = this._rallyId
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnSpeedUpButtonClick(long inMarchId)
  {
    UIManager.inst.OpenPopup("MarchSpeedUpPopup", (Popup.PopupParameter) new MarchSpeedUpPopup.Parameter()
    {
      marchData = DBManager.inst.DB_March.Get(inMarchId)
    });
  }

  public void OnCancelRallyClick()
  {
    UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
    {
      title = "Warning",
      description = "Are you sure you want cancel the rally?",
      leftButtonClickEvent = (System.Action) null,
      rightButtonClickEvent = (System.Action) (() =>
      {
        GameEngine.Instance.marchSystem.CancelRally(this._rallyId);
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      })
    });
  }

  public void OnCloseButtonClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnBackButtonClick()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnRallyDataChanged(long rallyId)
  {
    if (rallyId != this._rallyId)
      return;
    this.Setup(this._rallyId);
  }

  public void OnRallyStateChanged(long rallyId)
  {
    if (rallyId != this._rallyId)
      return;
    this.Setup(this._rallyId);
  }

  public void OnSecond(int serverTime)
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this._rallyId);
    if (rallyData == null)
    {
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
    {
      this._rallyWaitEndTime = rallyData.waitTimeDuration.endTime;
      if (rallyData.ownerUid == PlayerData.inst.uid)
      {
        this.panel.BT_CancelRally.gameObject.SetActive(true);
        this.panel.BT_CancelRally.isEnabled = true;
      }
      else
      {
        this.panel.BT_CancelRally.gameObject.SetActive(false);
        this.panel.BT_CancelRally.isEnabled = false;
      }
      if (this._rallyWaitEndTime > serverTime)
      {
        this.panel.rallyLeftText.text = Utils.FormatTime(this._rallyWaitEndTime - serverTime, false, false, true);
        if (this._rallyWaitEndTime != this._rallyWaitStartTime)
          this.panel.rallyLeftTimeSlider.value = 1f - Mathf.Clamp01((float) (this._rallyWaitEndTime - serverTime) / (float) (this._rallyWaitEndTime - this._rallyWaitStartTime));
        if (this.secondFlag)
          return;
        Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
        this.secondFlag = true;
      }
      else
      {
        this.panel.rallyLeftText.text = "Rallied";
        this.panel.rallyLeftTimeSlider.value = 1f;
        if (!this.secondFlag)
          return;
        if (Oscillator.IsAvailable)
          Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
        this.secondFlag = false;
      }
    }
  }

  public void OnEnable()
  {
    if (DBManager.inst != null)
      DBManager.inst.DB_Rally.onRallyDataChanged += new System.Action<long>(this.OnRallyStateChanged);
    if (this.secondFlag)
      Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    if (!((UnityEngine.Object) this.panel.BT_CancelRally != (UnityEngine.Object) null))
      return;
    this.panel.BT_CancelRally.isEnabled = false;
  }

  public void OnDisable()
  {
    if (DBManager.inst != null)
      DBManager.inst.DB_Rally.onRallyDataChanged -= new System.Action<long>(this.OnRallyStateChanged);
    if (!this.secondFlag || !Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._rallyId = (orgParam as PopupRallyDlg.Parameter).rallyId;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Setup(this._rallyId);
    this.panel.rallyUnlockSlotDlg.SetActive(false);
    this.panel.rallyUnlockSlotWithCoinDlg.SetActive(false);
  }

  [Serializable]
  protected class Panel
  {
    public const int MAX_SLOT_COUNT = 50;
    public UITexture ownerIcon;
    public UILabel ownerAllianceName;
    public UILabel ownerCiytName;
    public UILabel ownerCoordinateK;
    public UILabel ownerCoordinateX;
    public UILabel ownerCoordinateY;
    public UIButton BT_ownerGoto;
    public UITexture targetIcon;
    public UILabel targetAllianceName;
    public UILabel targetCityName;
    public UILabel targetCoordinateK;
    public UILabel targetCoordinateX;
    public UILabel targetCoordinateY;
    public UIButton BT_targetGoto;
    public UILabel rallyTroopsCount;
    public UISlider rallyLeftTimeSlider;
    public UILabel rallyLeftText;
    public UIButton BT_CancelRally;
    public UIScrollView scrolView;
    public UITable rallySlotTable;
    public RallySlotBar orgRallySlot;
    public GameObject rallyUnlockSlotDlg;
    public UIButton BT_unlockForMe;
    public UILabel slotItemCount;
    public UILabel slotItemCost1;
    public UILabel slotItemCost2;
    public UITexture slotItemIcon;
    public UITexture slotItemIcon1;
    public UITexture slotItemIcon2;
    public UITexture slotItemIcon3;
    public GameObject rallyUnlockSlotWithCoinDlg;
    public UIButton BT_unlockForMeWithCoin;
    public UILabel slotItemCountWithCoin;
    public UILabel unlockSlotWithCoinWarningLabel;
    public UILabel unlockCostWithCoin1;
    public UILabel unlockCostWithCoin2;
  }

  protected class Data
  {
    public List<RallySlotInfo> slotJoinedList = new List<RallySlotInfo>();
    public List<RallySlotInfo> slotUnlockList = new List<RallySlotInfo>();
    public long rally_Id;
    public long troopsCount;
    public int slotUnlockCount;
    public int slotLockCount;
    public int rallyWaitStartTime;
    public int rallyWaitEndTime;
    public long uid;
    public long cityId;
    public long allianceId;
    public long oppUid;
    public long oppCityId;
    public long oppAllianceId;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public long rallyId;
  }
}
