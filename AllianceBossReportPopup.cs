﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceBossReportPopup : BaseReportPopup
{
  private List<GameObject> pool = new List<GameObject>();
  private const string bossiconpath = "Texture/AllianceBoss/";
  private AllianceBossReportContent abrc;
  public UIScrollView sv;
  public Icon leader;
  public Icon boss;
  public GameObject trooptemplate;
  public UILabel content;
  public UISlider damageSlider;
  public UISprite begintroops;

  private void Init()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/AllianceBoss/mail_report_alliance_boss", (System.Action<bool>) null, true, false, string.Empty);
    this.content.text = this.param.mail.GetBodyString();
    this.leader.SetData("Texture/Hero/Portrait_Icon/player_portrait_icon_" + this.abrc.attacker.portrait, this.abrc.attacker.icon, this.abrc.attacker.lord_title, new string[2]
    {
      this.abrc.attacker.fullname,
      this.abrc.attacker.site
    });
    this.boss.SetData("Texture/AllianceBoss/" + this.abrc.defender.portrait, new string[3]
    {
      ScriptLocalization.Get(this.abrc.defender.name, true),
      this.abrc.defender.hpremained,
      this.abrc.defender.hpreduced
    });
    if (this.abrc.defender.original_total_troops == 0L)
      this.abrc.defender.original_total_troops = this.abrc.defender.start_total_troops;
    this.damageSlider.value = (float) this.abrc.defender.end_total_troops / (float) this.abrc.defender.original_total_troops;
    this.begintroops.width = (int) (float) ((double) this.abrc.defender.start_total_troops / (double) this.abrc.defender.original_total_troops * 574.0);
    float num1 = 0.0f;
    float y = NGUIMath.CalculateRelativeWidgetBounds(this.trooptemplate.transform).size.y;
    float num2 = this.trooptemplate.transform.localPosition.y + y / 2f;
    using (List<MemberDetail>.Enumerator enumerator = this.abrc.attacker_troop_detail.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MemberDetail current = enumerator.Current;
        GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.trooptemplate, this.trooptemplate.transform.parent);
        this.pool.Add(gameObject);
        gameObject.GetComponent<MemberInfoItem>().SeedData(current);
        float num3 = y + 15f;
        num1 += num3;
        float num4 = num2 - num3 / 2f;
        Vector3 localPosition = this.trooptemplate.transform.localPosition;
        localPosition.y = num4;
        gameObject.transform.localPosition = localPosition;
        num2 = num4 - num3 / 2f;
      }
    }
    this.HideTemplate(true);
  }

  private void Reset()
  {
    this.sv.ResetPosition();
    Rigidbody component = this.sv.transform.GetComponent<Rigidbody>();
    if ((UnityEngine.Object) null != (UnityEngine.Object) component)
    {
      component.isKinematic = true;
      component.useGravity = false;
    }
    this.HideTemplate(false);
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        PrefabManagerEx.Instance.Destroy(current);
      }
    }
    this.pool.Clear();
  }

  private void HideTemplate(bool hide)
  {
    this.trooptemplate.SetActive(!hide);
  }

  public void GotoMycity()
  {
    this.GoToTargetPlace(int.Parse(this.abrc.k), int.Parse(this.abrc.x), int.Parse(this.abrc.y));
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.abrc = JsonReader.Deserialize<AllianceBossReportContent>(Utils.Object2Json((object) this.param.hashtable));
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }
}
