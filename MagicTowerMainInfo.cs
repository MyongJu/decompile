﻿// Decompiled with JetBrains decompiler
// Type: MagicTowerMainInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class MagicTowerMainInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "level")]
  public int Level;
  [Config(Name = "monster_id")]
  public int TowerMonsterId;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "magic")]
  public int Magic;
  [Config(Name = "mine_level")]
  public int MineLevel;
  [Config(Name = "novice_protection")]
  public int NoviceProtection;
  [Config(Name = "output")]
  public int Speed;
  [Config(Name = "acquisition_time")]
  public int GratherTime;
  [Config(Name = "detained_time")]
  public int StayTime;
  [Config(Name = "player_unit_id")]
  public int PlayerUnitId;
  [Config(Name = "magic_tower_icon")]
  public string MineImage;
  [Config(Name = "rescue_ratio")]
  public float rescueRatio;

  public string MineImagePath
  {
    get
    {
      if (string.IsNullOrEmpty(this.MineImage))
      {
        Logger.Error((object) "magic_tower_icon is empty");
        this.MineImage = "tex_crystalore_meilin_01";
      }
      return string.Format("Prefab/MerlinTower/Text/{0}", (object) this.MineImage);
    }
  }

  public string ModelPath
  {
    get
    {
      MagicTowerMonsterInfo data = ConfigManager.inst.DB_MagicTowerMonster.GetData(this.TowerMonsterId);
      if (data != null && !string.IsNullOrEmpty(data.Model))
        return "Prefab/MerlinTower/Monster/" + data.Model;
      return string.Empty;
    }
  }

  public string IconPath
  {
    get
    {
      MagicTowerMonsterInfo data = ConfigManager.inst.DB_MagicTowerMonster.GetData(this.TowerMonsterId);
      if (data != null)
        return "Texture/MerlinTower/" + data.Icon;
      return string.Empty;
    }
  }
}
