﻿// Decompiled with JetBrains decompiler
// Type: ComponentRenderBase
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections;
using UnityEngine;

public abstract class ComponentRenderBase : MonoBehaviour, IComponentRender
{
  public IComponentData data;

  public virtual void FeedData<T>(Hashtable source)
  {
    this.data = (IComponentData) (object) JsonReader.Deserialize<T>(Utils.Object2Json((object) source));
    this.Init();
  }

  public virtual void FeedData(IComponentData data)
  {
    this.data = data;
    this.Init();
  }

  public virtual void Init()
  {
  }

  public virtual void Dispose()
  {
  }
}
