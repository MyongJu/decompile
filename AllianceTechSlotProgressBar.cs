﻿// Decompiled with JetBrains decompiler
// Type: AllianceTechSlotProgressBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class AllianceTechSlotProgressBar : MonoBehaviour
{
  private int _maxStep;
  private float _curProgress;
  private int _curStep;
  [SerializeField]
  private AllianceTechSlotProgressBar.Panel panel;

  public int maxStep
  {
    get
    {
      return this._maxStep;
    }
    set
    {
      if (value == this._maxStep)
        return;
      this._maxStep = value >= 1 ? (value <= 5 ? value : 5) : 1;
      this.panel.background.width = 24 + 380 * this._maxStep;
      for (int index = 0; index < this._maxStep - 1; ++index)
        this.panel.sliders[index].gameObject.SetActive(true);
      for (int maxStep = this._maxStep; maxStep < this.panel.sliders.Length; ++maxStep)
        this.panel.sliders[maxStep].gameObject.SetActive(false);
      this.ResetProgress();
    }
  }

  public float curProgress
  {
    get
    {
      return this._curProgress;
    }
    set
    {
      if ((double) this._curProgress == (double) value)
        return;
      this._curProgress = value;
      this.panel.blueSlider.width = (int) ((double) this._curProgress * (double) this.maxStep * 380.0);
      this.curStep = (int) ((double) this._curProgress * (double) this.maxStep);
    }
  }

  public int curStep
  {
    get
    {
      return this._curStep;
    }
    private set
    {
      this._curStep = value;
      this.panel.greenSlider.width = this._curStep * 380;
    }
  }

  public void ResetProgress()
  {
    this.panel.blueSlider.width = (int) ((double) this._curProgress * (double) this.maxStep * 380.0);
    this.curStep = (int) ((double) this._curProgress * (double) this.maxStep);
  }

  [Serializable]
  public class Panel
  {
    public UISprite[] sliders = new UISprite[4];
    public const int SLIDER_STEP = 380;
    public const int MAX_SLIDER_COUNT = 5;
    public const int SLIDER_BACKGROUND_OFFSET = 12;
    public UISprite background;
    public UISprite greenSlider;
    public UISprite blueSlider;
  }
}
