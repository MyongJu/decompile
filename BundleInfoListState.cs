﻿// Decompiled with JetBrains decompiler
// Type: BundleInfoListState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.IO;
using UnityEngine;

public class BundleInfoListState : LoadBaseState
{
  public BundleInfoListState(int step)
    : base(step)
  {
  }

  public override string Key
  {
    get
    {
      return nameof (BundleInfoListState);
    }
  }

  protected override SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.BunldeInfoVersion;
    }
  }

  protected override void Prepare()
  {
    base.Prepare();
    this.Load();
    Oscillator.Instance.updateEvent += new System.Action<double>(this.UpdateEventHandler);
  }

  private void UpdateEventHandler(double t)
  {
    GameEngine.Instance.iTweenValue = Mathf.Lerp(GameEngine.Instance.iTweenValue, SplashDataConfig.GetValue(this.CurrentPhase), 0.02f);
    this.RefreshProgress(GameEngine.Instance.iTweenValue);
  }

  private void Load()
  {
    this.batch.Add("BundleVersionList.json", NetApi.inst.BuildBundleUrl("VersionList.json"), (Hashtable) null, false, 3, false);
    LoaderManager.inst.Add(this.batch, false, false);
  }

  protected override void OnAllFileFinish(LoaderBatch batch)
  {
    base.OnAllFileFinish(batch);
    this.SaveFile("BundleVersionList.json", batch);
  }

  private void SaveFile(string key, LoaderBatch batch)
  {
    LoaderInfo resultInfo = batch.GetResultInfo(key);
    if (resultInfo == null || resultInfo.ResData == null)
      return;
    if (!Directory.Exists(AssetConfig.BundleCacheDir))
      Directory.CreateDirectory(AssetConfig.BundleCacheDir);
    this.WriteFile(NetApi.inst.BuildLocalBundlePath(resultInfo.Action), resultInfo.ResData);
    if (!(key == "BundleVersionList.json"))
      return;
    if (Utils.Json2Object(Utils.ByteArray2String(resultInfo.ResData), true) == null)
      ;
    this.RetryVersionManager();
  }

  private void OnVersionLoadFinished(bool success)
  {
    VersionManager.Instance.onVersionLoadFinished -= new System.Action<bool>(this.OnVersionLoadFinished);
    if (success)
    {
      if (this.IsFail)
        return;
      this.Finish();
    }
    else
      NetWorkDetector.Instance.RetryTip(new System.Action(this.RetryVersionManager), ScriptLocalization.Get("data_error_title", true), ScriptLocalization.Get("data_error_version_description", true));
  }

  private void RetryVersionManager()
  {
    if (!this.WriteFileSuccess)
      return;
    VersionManager.Instance.onVersionLoadFinished += new System.Action<bool>(this.OnVersionLoadFinished);
    VersionManager.Instance.Init();
  }

  protected override void Retry()
  {
    this.ResetBatch();
    this.Load();
  }

  protected override void Finish()
  {
    if (this.IsDestroy)
      return;
    base.Finish();
    if (Oscillator.IsAvailable)
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.UpdateEventHandler);
    this.Preloader.OnBundleVerListFinish();
  }
}
