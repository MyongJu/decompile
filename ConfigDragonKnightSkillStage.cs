﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragonKnightSkillStage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDragonKnightSkillStage
{
  private List<DragonKnightSkillStageInfo> dragonKnightSkillStageInfoList = new List<DragonKnightSkillStageInfo>();
  private Dictionary<string, DragonKnightSkillStageInfo> datas;
  private Dictionary<int, DragonKnightSkillStageInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<DragonKnightSkillStageInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    Dictionary<string, DragonKnightSkillStageInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this.dragonKnightSkillStageInfoList.Add(enumerator.Current);
    }
  }

  public List<DragonKnightSkillStageInfo> GetDragonKnightSkillStageInfoList()
  {
    return this.dragonKnightSkillStageInfoList;
  }

  public DragonKnightSkillStageInfo Get(int interalId)
  {
    if (this.dicByUniqueId.ContainsKey(interalId))
      return this.dicByUniqueId[interalId];
    return (DragonKnightSkillStageInfo) null;
  }

  public DragonKnightSkillStageInfo Get(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (DragonKnightSkillStageInfo) null;
  }
}
