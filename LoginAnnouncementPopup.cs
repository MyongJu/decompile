﻿// Decompiled with JetBrains decompiler
// Type: LoginAnnouncementPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class LoginAnnouncementPopup : Popup
{
  private GameObjectPool objPool = new GameObjectPool();
  private Dictionary<int, AnnouncementData> itemDic = new Dictionary<int, AnnouncementData>();
  private List<AnnounceSubjectTabRender> itemList = new List<AnnounceSubjectTabRender>();
  public UILabel title;
  public GameObject tabPrefab;
  public GameObject tabRoot;
  public UITable tabTable;
  public UIScrollView scrollView;
  public AnnouncementItemRender announcementItemRender;
  private List<AnnouncementData> dataList;

  private void UpdateUI()
  {
    this.UpdateSubjectTabs();
  }

  private void UpdateSubjectTabs()
  {
    if (this.dataList == null || this.dataList.Count <= 0)
      return;
    for (int tabIndex = 0; tabIndex < this.dataList.Count; ++tabIndex)
    {
      AnnouncementData data = this.dataList[tabIndex];
      if (data != null)
        this.GenerateSubjectTab(data, tabIndex, tabIndex == 0);
    }
    this.RepositionContainer();
  }

  private void GenerateSubjectTab(AnnouncementData data, int tabIndex, bool defaultSelected)
  {
    this.objPool.Initialize(this.tabPrefab, this.tabRoot);
    GameObject go = this.objPool.AddChild(this.tabTable.gameObject);
    NGUITools.SetActive(go, true);
    AnnounceSubjectTabRender component = go.GetComponent<AnnounceSubjectTabRender>();
    if (!((UnityEngine.Object) null != (UnityEngine.Object) component))
      return;
    this.itemDic.Add(tabIndex, data);
    component.OnCurrentCatalogClicked += new System.Action<int>(this.OnSubjectTabClicked);
    component.SetData(data.subject, tabIndex, defaultSelected);
    this.itemList.Add(component);
  }

  private void OnSubjectTabClicked(int tabIndex)
  {
    if (!this.itemDic.ContainsKey(tabIndex))
      return;
    AnnouncementData data = this.itemDic[tabIndex];
    if (data == null)
      return;
    this.title.text = data.subject;
    this.announcementItemRender.ClearData();
    this.announcementItemRender.FeedData(data);
    this.announcementItemRender.Show();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  private void ClearTabs()
  {
    for (int index = 0; index < this.itemList.Count; ++index)
    {
      this.itemList[index].OnCurrentCatalogClicked -= new System.Action<int>(this.OnSubjectTabClicked);
      GameObject gameObject = this.itemList[index].gameObject;
      gameObject.SetActive(false);
      this.objPool.Release(gameObject);
    }
    if (this.dataList != null)
    {
      this.dataList.Clear();
      this.dataList = (List<AnnouncementData>) null;
    }
    this.itemList.Clear();
    this.itemDic.Clear();
    this.objPool.Clear();
  }

  private void RepositionContainer()
  {
    this.tabTable.Reposition();
    this.scrollView.ResetPosition();
  }

  private void HideView()
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.announcementItemRender))
      return;
    this.announcementItemRender.Hide();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    LoginAnnouncementPopup.Parameter parameter = orgParam as LoginAnnouncementPopup.Parameter;
    if (parameter != null && parameter.dataList != null)
      this.dataList = parameter.dataList;
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearTabs();
    this.HideView();
  }

  public class Parameter : Popup.PopupParameter
  {
    public List<AnnouncementData> dataList;
  }
}
