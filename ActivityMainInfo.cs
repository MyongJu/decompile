﻿// Decompiled with JetBrains decompiler
// Type: ActivityMainInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class ActivityMainInfo
{
  [Config(Name = "name_1")]
  public string Name1 = string.Empty;
  [Config(Name = "description_1")]
  public string Desc1 = string.Empty;
  [Config(Name = "description_big_1")]
  public string ADDesc1 = string.Empty;
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "image")]
  public string Image;
  [Config(Name = "image_big")]
  public string ADImage;
  [Config(Name = "icon")]
  public string Icon;
  [Config(Name = "name")]
  public string Name;
  [Config(Name = "description")]
  public string Desc;
  [Config(Name = "description_big")]
  public string ADDesc;

  public string LocalName
  {
    get
    {
      return ScriptLocalization.Get(this.Name, true);
    }
  }

  public string LocalName1
  {
    get
    {
      return ScriptLocalization.Get(this.Name1, true);
    }
  }

  public string LocalDesc1
  {
    get
    {
      return ScriptLocalization.Get(this.Desc1, true);
    }
  }

  public string LocalDesc
  {
    get
    {
      return ScriptLocalization.Get(this.Desc, true);
    }
  }

  public string LocalADDesc
  {
    get
    {
      return ScriptLocalization.Get(this.ADDesc, true);
    }
  }

  public string LocalADDesc1
  {
    get
    {
      return ScriptLocalization.Get(this.ADDesc1, true);
    }
  }

  public string ImagePath
  {
    get
    {
      return this.Image;
    }
  }

  public string IconPath
  {
    get
    {
      return "Texture/Events/" + this.Icon;
    }
  }

  public string ADImagePath
  {
    get
    {
      return "Texture/Events/" + this.ADImage;
    }
  }
}
