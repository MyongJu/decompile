﻿// Decompiled with JetBrains decompiler
// Type: AllianceActivityData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class AllianceActivityData : RoundActivityData
{
  private int _level = 1;

  public int AllianceLevel
  {
    get
    {
      return this._level;
    }
  }

  public override List<int> CurrentRoundTargetScores
  {
    get
    {
      List<int> souces = new List<int>();
      AllianceActivityReward currentStepReward = ConfigManager.inst.DB_AllianceActivityReward.GetCurrentStepReward(this.CurrentRound.ActivitySubConfigID, this._level);
      this.AddList(souces, currentStepReward.RequireScore1_1);
      this.AddList(souces, currentStepReward.RequireScore2_1);
      this.AddList(souces, currentStepReward.RequireScore3_1);
      return souces;
    }
  }

  public override void Decode(Hashtable source)
  {
    if (source == null)
      return;
    base.Decode(source);
    DatabaseTools.UpdateData(source, "alliance_level", ref this._level);
  }

  public override List<int> CurrentItems
  {
    get
    {
      List<int> souces = new List<int>();
      AllianceActivityReward currentStepReward = ConfigManager.inst.DB_AllianceActivityReward.GetCurrentStepReward(this.CurrentRound.ActivitySubConfigID, this._level);
      this.AddList(souces, currentStepReward.ItemRewardID1_1);
      this.AddList(souces, currentStepReward.ItemRewardID1_2);
      this.AddList(souces, currentStepReward.ItemRewardID1_3);
      this.AddList(souces, currentStepReward.ItemRewardID1_4);
      this.AddList(souces, currentStepReward.ItemRewardID1_5);
      this.AddList(souces, currentStepReward.ItemRewardID2_1);
      this.AddList(souces, currentStepReward.ItemRewardID2_2);
      this.AddList(souces, currentStepReward.ItemRewardID2_3);
      this.AddList(souces, currentStepReward.ItemRewardID2_4);
      this.AddList(souces, currentStepReward.ItemRewardID2_5);
      this.AddList(souces, currentStepReward.ItemRewardID3_1);
      this.AddList(souces, currentStepReward.ItemRewardID3_2);
      this.AddList(souces, currentStepReward.ItemRewardID3_3);
      this.AddList(souces, currentStepReward.ItemRewardID3_4);
      this.AddList(souces, currentStepReward.ItemRewardID3_5);
      return souces;
    }
  }

  public new virtual Dictionary<int, int> CurrentRoundRewardItems
  {
    get
    {
      Dictionary<int, int> souces = new Dictionary<int, int>();
      AllianceActivityReward currentStepReward = ConfigManager.inst.DB_AllianceActivityReward.GetCurrentStepReward(this.CurrentRound.ActivitySubConfigID, this._level);
      this.AddDict(souces, currentStepReward.ItemRewardID1_1, currentStepReward.ItemRewardValue1_1);
      this.AddDict(souces, currentStepReward.ItemRewardID1_2, currentStepReward.ItemRewardValue1_2);
      this.AddDict(souces, currentStepReward.ItemRewardID1_3, currentStepReward.ItemRewardValue1_3);
      this.AddDict(souces, currentStepReward.ItemRewardID1_4, currentStepReward.ItemRewardValue1_4);
      this.AddDict(souces, currentStepReward.ItemRewardID1_5, currentStepReward.ItemRewardValue1_5);
      this.AddDict(souces, currentStepReward.ItemRewardID2_1, currentStepReward.ItemRewardValue2_1);
      this.AddDict(souces, currentStepReward.ItemRewardID2_2, currentStepReward.ItemRewardValue2_2);
      this.AddDict(souces, currentStepReward.ItemRewardID2_3, currentStepReward.ItemRewardValue2_3);
      this.AddDict(souces, currentStepReward.ItemRewardID2_4, currentStepReward.ItemRewardValue2_4);
      this.AddDict(souces, currentStepReward.ItemRewardID2_5, currentStepReward.ItemRewardValue2_5);
      this.AddDict(souces, currentStepReward.ItemRewardID3_1, currentStepReward.ItemRewardValue3_1);
      this.AddDict(souces, currentStepReward.ItemRewardID3_2, currentStepReward.ItemRewardValue3_2);
      this.AddDict(souces, currentStepReward.ItemRewardID3_3, currentStepReward.ItemRewardValue3_3);
      this.AddDict(souces, currentStepReward.ItemRewardID3_4, currentStepReward.ItemRewardValue3_4);
      this.AddDict(souces, currentStepReward.ItemRewardID3_5, currentStepReward.ItemRewardValue3_5);
      return souces;
    }
  }

  public override List<int> TotalRankTopOneItems
  {
    get
    {
      return this.GetRankItems(0, 1);
    }
  }

  public override Dictionary<int, int> TotalRankTopOneRewardItems
  {
    get
    {
      return this.GetRankRewardItems(0, 1);
    }
  }

  public override List<int> GetRankItems(int subActivityId, int rank)
  {
    List<int> souces = new List<int>();
    AllianceActivityRankRewardInfo rewardInfo = ConfigManager.inst.DB_AllianceActivityRankReward.GetRewardInfo(subActivityId, rank);
    this.AddList(souces, rewardInfo.ItemRewardID_1);
    this.AddList(souces, rewardInfo.ItemRewardID_2);
    this.AddList(souces, rewardInfo.ItemRewardID_3);
    this.AddList(souces, rewardInfo.ItemRewardID_4);
    this.AddList(souces, rewardInfo.ItemRewardID_5);
    this.AddList(souces, rewardInfo.ItemRewardID_6);
    return souces;
  }

  public override Dictionary<int, int> GetRankRewardItems(int subActivityId, int rank)
  {
    Dictionary<int, int> souces = new Dictionary<int, int>();
    AllianceActivityRankRewardInfo rewardInfo = ConfigManager.inst.DB_AllianceActivityRankReward.GetRewardInfo(subActivityId, rank);
    this.AddDict(souces, rewardInfo.ItemRewardID_1, rewardInfo.ItemRewardValue_1);
    this.AddDict(souces, rewardInfo.ItemRewardID_2, rewardInfo.ItemRewardValue_2);
    this.AddDict(souces, rewardInfo.ItemRewardID_3, rewardInfo.ItemRewardValue_3);
    this.AddDict(souces, rewardInfo.ItemRewardID_4, rewardInfo.ItemRewardValue_4);
    this.AddDict(souces, rewardInfo.ItemRewardID_5, rewardInfo.ItemRewardValue_5);
    this.AddDict(souces, rewardInfo.ItemRewardID_6, rewardInfo.ItemRewardValue_6);
    return souces;
  }

  public override List<string> GetRequirementDesc()
  {
    List<string> stringList = new List<string>();
    List<AllianceActivityRequirementInfo> currentRequirementInfo = ConfigManager.inst.DB_AllianceActivityRequirement.GetCurrentRequirementInfo(this.CurrentRound.ActivitySubConfigID);
    for (int index = 0; index < currentRequirementInfo.Count; ++index)
      stringList.Add(currentRequirementInfo[index].Description);
    return stringList;
  }

  public override ActivityBaseData.ActivityType Type
  {
    get
    {
      return ActivityBaseData.ActivityType.AllianceActivity;
    }
  }
}
