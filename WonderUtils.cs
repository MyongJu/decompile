﻿// Decompiled with JetBrains decompiler
// Type: WonderUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;

public static class WonderUtils
{
  private static readonly Coordinate WONDER = new Coordinate(0, 640, 1274);
  private static readonly Coordinate TOWER1 = new Coordinate(0, 640, 1270);
  private static readonly Coordinate TOWER2 = new Coordinate(0, 648, 1278);
  private static readonly Coordinate TOWER3 = new Coordinate(0, 640, 1286);
  private static readonly Coordinate TOWER4 = new Coordinate(0, 632, 1278);

  public static bool IsWonder(Coordinate location)
  {
    if (location.X == WonderUtils.WONDER.X)
      return location.Y == WonderUtils.WONDER.Y;
    return false;
  }

  public static Coordinate GetWonderLocation()
  {
    return WonderUtils.WONDER;
  }

  public static bool IsWonderTower(Coordinate location)
  {
    if (location.X == WonderUtils.TOWER1.X && location.Y == WonderUtils.TOWER1.Y || location.X == WonderUtils.TOWER2.X && location.Y == WonderUtils.TOWER2.Y || location.X == WonderUtils.TOWER3.X && location.Y == WonderUtils.TOWER3.Y)
      return true;
    if (location.X == WonderUtils.TOWER4.X)
      return location.Y == WonderUtils.TOWER4.Y;
    return false;
  }

  public static long GetWonderTowerID(Coordinate location)
  {
    if (location.X == WonderUtils.TOWER1.X && location.Y == WonderUtils.TOWER1.Y)
      return 1;
    if (location.X == WonderUtils.TOWER2.X && location.Y == WonderUtils.TOWER2.Y)
      return 2;
    if (location.X == WonderUtils.TOWER3.X && location.Y == WonderUtils.TOWER3.Y)
      return 3;
    return location.X == WonderUtils.TOWER4.X && location.Y == WonderUtils.TOWER4.Y ? 4L : 0L;
  }

  public static void LoadWonderMaxTroopCount(int kingdomId, System.Action<long> callback)
  {
    RequestManager.inst.SendRequest("wonder:loadWonderData", new Hashtable()
    {
      {
        (object) "kingdom_id",
        (object) kingdomId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      long outData = 0;
      Hashtable inData = data as Hashtable;
      if (inData != null)
      {
        DatabaseTools.UpdateData(inData, "capacity", ref outData);
        WonderFlagManager.Instance.Parse((object) inData);
      }
      if (callback == null)
        return;
      callback(outData);
    }), true);
  }

  public static void LoadWonderTowerMaxTroopCount(int kingdomId, long towerId, System.Action<long> callback)
  {
    RequestManager.inst.SendRequest("wonder:loadWonderTowerData", new Hashtable()
    {
      {
        (object) "kingdom_id",
        (object) kingdomId
      },
      {
        (object) "tower_id",
        (object) towerId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      long outData = 0;
      Hashtable inData = data as Hashtable;
      if (inData != null)
        DatabaseTools.UpdateData(inData, "capacity", ref outData);
      if (callback == null)
        return;
      callback(outData);
    }), true);
  }

  public static string GetWonderTowerName(int towerId)
  {
    if (towerId >= 1 && towerId <= 4)
      return Utils.XLAT("throne_miniwonder_" + (object) towerId + "_name");
    return string.Empty;
  }

  public static string GetWonderTowerPrefabName(int towerId)
  {
    switch (towerId)
    {
      case 1:
        return "tower_northern01_01";
      case 2:
        return "tower_northern01_02";
      case 3:
        return "tower_northern02_01";
      case 4:
        return "tower_northern02_02";
      default:
        return "tower_northern01_01";
    }
  }

  public static void ShowKingdomTreasury(int kingdomId)
  {
    RequestManager.inst.SendRequest("king:loadTreasuryHouseData", new Hashtable()
    {
      {
        (object) "kingdom_id",
        (object) kingdomId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenPopup("Wonder/KingdomTreasuryPopup", (Popup.PopupParameter) new KingdomTreasuryPopup.Parameter()
      {
        kingdomTreasuryData = (data as Hashtable)
      });
    }), true);
  }
}
