﻿// Decompiled with JetBrains decompiler
// Type: ParliamentHeroItemExt
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class ParliamentHeroItemExt : MonoBehaviour
{
  private ParliamentHeroItem normalInfo;
  [SerializeField]
  private UILabel labelTitle;
  [SerializeField]
  private UILabel labelName;
  [SerializeField]
  private UILabel labelScore;
  private GameObject _HeroBookItemTemplate;

  public void SetData(LegendCardData legendCardData)
  {
    if (legendCardData == null)
    {
      D.error((object) "ParliamentHeroCard::SetData(ParliamentHeroInfo:null)");
    }
    else
    {
      this._HeroBookItemTemplate = AssetManager.Instance.HandyLoad("Prefab/UI/Common/ParliamentHeroItem", (System.Type) null) as GameObject;
      this.normalInfo = this.CreateHeroBookItem();
      this.normalInfo.ShowlabelLevel(true);
      this.normalInfo.SetData(legendCardData);
      this.labelName.text = ConfigManager.inst.DB_ParliamentHero.Get(legendCardData.LegendId).Name;
      this.labelTitle.text = HeroCardUtils.GetHeroTitle(legendCardData.LegendId);
      this.labelScore.text = ScriptLocalization.GetWithPara("hero_score_level_num", new Dictionary<string, string>()
      {
        {
          "0",
          legendCardData.Score.ToString()
        }
      }, true);
    }
  }

  protected ParliamentHeroItem CreateHeroBookItem()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._HeroBookItemTemplate);
    gameObject.transform.SetParent(this.transform);
    gameObject.transform.localPosition = Vector3.zero;
    Vector3 one = Vector3.one;
    one.x = one.y = 0.6f;
    gameObject.transform.localScale = one;
    return gameObject.GetComponent<ParliamentHeroItem>();
  }
}
