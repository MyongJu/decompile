﻿// Decompiled with JetBrains decompiler
// Type: AndroidAdaptor
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AndroidAdaptor : DefaultAdaptor
{
  public override void OnApplicationStart()
  {
    base.OnApplicationStart();
    if (Screen.width >= Screen.height)
    {
      if (Screen.height <= 1080 || Screen.height == 0)
        return;
      Screen.SetResolution((int) ((double) Screen.width * (double) (1080f / (float) Screen.height)), 1080, true);
    }
    else
    {
      if (Screen.width <= 1080 || Screen.width == 0)
        return;
      Screen.SetResolution(1080, (int) ((double) Screen.height * (double) (1080f / (float) Screen.width)), true);
    }
  }
}
