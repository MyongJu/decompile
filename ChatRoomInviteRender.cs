﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomInviteRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class ChatRoomInviteRender : MonoBehaviour
{
  public Icon roomIcon;
  public UIButton ignorBtn;
  public UIButton acceptBtn;
  private ChatRoomData _roomData;

  public void Feed(ChatRoomData data)
  {
    this._roomData = data;
    this.ignorBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnIgnorBtnClick)));
    this.acceptBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnAcceptBtnClick)));
    this.roomIcon.SetData(data.GetOwnerMember().PortraitIconPath, data.roomName, data.roomTitle);
  }

  public void OnIgnorBtnClick()
  {
    ChatRoomManager.AcceptOrDenyInvite(this._roomData.roomId, "deny", this._roomData);
  }

  public void OnAcceptBtnClick()
  {
    ChatRoomManager.AcceptOrDenyInvite(this._roomData.roomId, "accept", this._roomData);
  }
}
