﻿// Decompiled with JetBrains decompiler
// Type: WatchtowerToopsItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WatchtowerToopsItemRenderer : MonoBehaviour
{
  public UISprite m_IconSprite;
  public UILabel m_NameLabel;
  public UILabel m_LevelLabel;
  public UILabel m_AmmountLabel;

  public void SetData(string itemName, string ammount)
  {
    TroopInfo troopInfo = TroopInfoManager.Instance.GetTroopInfo(itemName);
    this.m_IconSprite.spriteName = IconManager.Instance.GetIconPath(troopInfo.m_Icon);
    this.SetName(NameManager.Instance.GetName(troopInfo.m_Name));
    this.SetAmmount(ammount);
    this.SetLevel(troopInfo.m_Level.ToString());
    this.SetLevelActive(WatchtowerUtilities.GetWatchtowerLevel() >= 21);
  }

  private void SetName(string name)
  {
    if (!((Object) this.m_NameLabel != (Object) null))
      return;
    this.m_NameLabel.text = name;
  }

  private void SetLevelActive(bool active)
  {
    if (!((Object) this.m_LevelLabel != (Object) null))
      return;
    this.m_LevelLabel.gameObject.SetActive(active);
  }

  private void SetLevel(string level)
  {
    if (!((Object) this.m_LevelLabel != (Object) null))
      return;
    this.m_LevelLabel.text = string.Format("Lv.{0}", (object) level);
  }

  private void SetAmmount(string ammount)
  {
    if (!((Object) this.m_AmmountLabel != (Object) null))
      return;
    this.m_AmmountLabel.text = ammount;
  }
}
