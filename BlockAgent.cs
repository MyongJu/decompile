﻿// Decompiled with JetBrains decompiler
// Type: BlockAgent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class BlockAgent
{
  private LinkedList m_UsedNodeList = new LinkedList();
  private LinkedList m_RequestList = new LinkedList();
  private ObjectPool<BlockAgent.BlockRequest> m_RequestPool = new ObjectPool<BlockAgent.BlockRequest>();
  private ObjectPool<BlockAgent.BlockNode> m_NodePool = new ObjectPool<BlockAgent.BlockNode>();
  public System.Action OnEnterKingdomBlock;
  public System.Action OnRequestKingdomBlock;
  private int m_UsedNodeCount;
  private int m_HitCount;
  private HubPort m_HubPort;
  private DynamicMapData m_MapData;

  public BlockAgent(DynamicMapData mapData)
  {
    this.m_MapData = mapData;
  }

  private void SendLoadEnterKingdomBlock(Hashtable postData = null, System.Action<bool, object> callback = null, bool autoRetry = true, bool useStack = false)
  {
    this.m_HubPort = MessageHub.inst.GetPortByAction("Map:enterKingdomBlock");
    if (this.m_HubPort == null || string.IsNullOrEmpty(this.m_HubPort.Action))
      return;
    this.m_HubPort.SendLoader(postData, callback, autoRetry, useStack);
  }

  public void Dispose(bool leave = true)
  {
    this.m_HubPort = (HubPort) null;
    this.m_UsedNodeCount = 0;
    this.m_HitCount = 0;
    ArrayList arrayList1 = new ArrayList();
    ArrayList arrayList2 = new ArrayList();
    while (!this.m_UsedNodeList.IsEmpty())
    {
      BlockAgent.BlockNode o = this.m_UsedNodeList.PopBack() as BlockAgent.BlockNode;
      ArrayList blockParameter = this.GenerateBlockParameter(o.m_Block.Origin);
      arrayList1.Add((object) blockParameter);
      o.m_Block.Dispose();
      this.m_NodePool.Release(o);
    }
    if (leave)
    {
      Hashtable postData = new Hashtable();
      postData[(object) nameof (leave)] = (object) arrayList1;
      postData[(object) "enter"] = (object) arrayList2;
      postData[(object) "city_id"] = (object) PlayerData.inst.cityId;
      this.SendLoadEnterKingdomBlock(postData, (System.Action<bool, object>) null, true, true);
    }
    while (!this.m_RequestList.IsEmpty())
      this.m_RequestPool.Release((BlockAgent.BlockRequest) this.m_RequestList.PopFront());
  }

  public void UpdateCenter(WorldCoordinate worldCoords)
  {
    Vector3 BottomLeft = new Vector3();
    Vector3 Bottom = new Vector3();
    Vector3 BottomRight = new Vector3();
    Vector3 Right = new Vector3();
    Vector3 TopRight = new Vector3();
    Vector3 Top = new Vector3();
    Vector3 TopLeft = new Vector3();
    Vector3 Left = new Vector3();
    Vector3 Middle = new Vector3();
    this.GetCenters(worldCoords, ref BottomLeft, ref Bottom, ref BottomRight, ref Right, ref TopRight, ref Top, ref TopLeft, ref Left, ref Middle);
    this.Begin();
    this.SortOrRequest(Middle);
    this.SortOrRequest(Left);
    this.SortOrRequest(Right);
    this.SortOrRequest(Top);
    this.SortOrRequest(Bottom);
    this.SortOrRequest(BottomLeft);
    this.SortOrRequest(BottomRight);
    this.SortOrRequest(TopRight);
    this.SortOrRequest(TopLeft);
    this.End();
  }

  private void GetCenters(WorldCoordinate worldCoords, ref Vector3 BottomLeft, ref Vector3 Bottom, ref Vector3 BottomRight, ref Vector3 Right, ref Vector3 TopRight, ref Vector3 Top, ref Vector3 TopLeft, ref Vector3 Left, ref Vector3 Middle)
  {
    Vector3 pixelPosition = this.m_MapData.ConvertWorldCoordinateToPixelPosition(worldCoords);
    float x = pixelPosition.x;
    float y = pixelPosition.y;
    float new_x1 = x - this.m_MapData.PixelsPerBlock.x;
    float new_x2 = x + this.m_MapData.PixelsPerBlock.x;
    float new_y1 = y + this.m_MapData.PixelsPerBlock.y;
    float new_y2 = y - this.m_MapData.PixelsPerBlock.y;
    BottomLeft.Set(new_x1, new_y2, 0.0f);
    BottomRight.Set(new_x2, new_y2, 0.0f);
    TopRight.Set(new_x2, new_y1, 0.0f);
    TopLeft.Set(new_x1, new_y1, 0.0f);
    Left.Set(new_x1, y, 0.0f);
    Right.Set(new_x2, y, 0.0f);
    Top.Set(x, new_y1, 0.0f);
    Bottom.Set(x, new_y2, 0.0f);
    Middle.Set(x, y, 0.0f);
  }

  private void Begin()
  {
    while (!this.m_RequestList.IsEmpty())
    {
      BlockAgent.BlockRequest o = (BlockAgent.BlockRequest) this.m_RequestList.PopFront();
      D.warn((object) ("[BlockAgent]Begin: KXY = " + o.KXY.ToString() + ", Type = " + o.Type.ToString()));
      this.m_RequestPool.Release(o);
    }
    this.m_HitCount = 0;
  }

  public bool NeedRequestEnterKingdom(WorldCoordinate worldCoords)
  {
    Vector3 BottomLeft = new Vector3();
    Vector3 Bottom = new Vector3();
    Vector3 BottomRight = new Vector3();
    Vector3 Right = new Vector3();
    Vector3 TopRight = new Vector3();
    Vector3 Top = new Vector3();
    Vector3 TopLeft = new Vector3();
    Vector3 Left = new Vector3();
    Vector3 Middle = new Vector3();
    this.GetCenters(worldCoords, ref BottomLeft, ref Bottom, ref BottomRight, ref Right, ref TopRight, ref Top, ref TopLeft, ref Left, ref Middle);
    return false | this.NeedRequest(Middle) | this.NeedRequest(Left) | this.NeedRequest(Right) | this.NeedRequest(Top) | this.NeedRequest(Bottom) | this.NeedRequest(BottomLeft) | this.NeedRequest(BottomRight) | this.NeedRequest(TopRight) | this.NeedRequest(TopLeft);
  }

  private bool NeedRequest(Vector3 pixelPosition)
  {
    Coordinate tileKxy = this.m_MapData.ConvertPixelPositionToTileKXY(pixelPosition);
    if (this.m_MapData.IsKingdomActive(tileKxy.K))
      return !this.HitPoint(tileKxy);
    return false;
  }

  private bool HitPoint(Coordinate KXY)
  {
    return this.GetBlockNodeAt(KXY) != null;
  }

  private void SortOrRequest(Vector3 pixelPosition)
  {
    this.SortOrRequest(this.m_MapData.ConvertPixelPositionToTileKXY(pixelPosition));
  }

  private void SortOrRequest(Coordinate KXY)
  {
    if (1 > KXY.K || KXY.K > this.m_MapData.KingdomCount)
      return;
    BlockAgent.BlockNode blockNodeAt = this.GetBlockNodeAt(KXY);
    if (blockNodeAt != null)
    {
      this.m_UsedNodeList.Remove((LinkedList.Node) blockNodeAt);
      this.m_UsedNodeList.PushFront((LinkedList.Node) blockNodeAt);
      ++this.m_HitCount;
    }
    else
    {
      if (!this.m_MapData.IsKingdomActive(KXY.K))
        return;
      BlockAgent.BlockRequest blockRequest = this.m_RequestPool.Allocate();
      blockRequest.KXY = KXY;
      blockRequest.Type = BlockAgent.RequestType.ENTER;
      this.m_RequestList.PushBack((LinkedList.Node) blockRequest);
    }
  }

  private void End()
  {
    for (; this.m_UsedNodeCount > this.m_HitCount; --this.m_UsedNodeCount)
    {
      BlockAgent.BlockNode o = this.m_UsedNodeList.PopBack() as BlockAgent.BlockNode;
      BlockAgent.BlockRequest blockRequest = this.m_RequestPool.Allocate();
      blockRequest.KXY = o.m_Block.Origin;
      blockRequest.Type = BlockAgent.RequestType.LEAVE;
      this.m_RequestList.PushBack((LinkedList.Node) blockRequest);
      o.m_Block.Dispose();
      this.m_NodePool.Release(o);
    }
    if (this.m_RequestList.IsEmpty())
      return;
    if (this.OnRequestKingdomBlock != null)
      this.OnRequestKingdomBlock();
    this.SendLoadEnterKingdomBlock(this.GenerateRequest(), new System.Action<bool, object>(this.OnKingdomLoaded), true, true);
  }

  private void OnKingdomLoaded(bool ret, object data)
  {
    LinkedList.Node node = this.m_UsedNodeList.GetFirst();
    for (LinkedList.Node end = this.m_UsedNodeList.GetEnd(); node != end; node = node.m_Next)
      (node as BlockAgent.BlockNode).m_Block.EndLoading();
    if (this.OnEnterKingdomBlock == null)
      return;
    this.OnEnterKingdomBlock();
  }

  public void UpdateViewport(Rect viewport)
  {
    LinkedList.Node node = this.m_UsedNodeList.GetFirst();
    for (LinkedList.Node end = this.m_UsedNodeList.GetEnd(); node != end; node = node.m_Next)
      (node as BlockAgent.BlockNode).m_Block.UpdateViewport(viewport);
  }

  public void CullAndShow(Transform transform)
  {
    LinkedList.Node node = this.m_UsedNodeList.GetFirst();
    for (LinkedList.Node end = this.m_UsedNodeList.GetEnd(); node != end; node = node.m_Next)
      (node as BlockAgent.BlockNode).m_Block.CullAndShow(transform);
  }

  public bool IsLoaded
  {
    get
    {
      LinkedList.Node node = this.m_UsedNodeList.GetFirst();
      for (LinkedList.Node end = this.m_UsedNodeList.GetEnd(); node != end; node = node.m_Next)
      {
        if (!(node as BlockAgent.BlockNode).m_Block.IsLoaded)
          return false;
      }
      return true;
    }
  }

  public void UpdateUI()
  {
    LinkedList.Node node = this.m_UsedNodeList.GetFirst();
    for (LinkedList.Node end = this.m_UsedNodeList.GetEnd(); node != end; node = node.m_Next)
      (node as BlockAgent.BlockNode).m_Block.UpdateUI();
  }

  private BlockAgent.BlockNode GetBlockNodeAt(Coordinate location)
  {
    return this.GetBlockNodeAt(location.K, location.X, location.Y);
  }

  private BlockAgent.BlockNode GetBlockNodeAt(int k, int x, int y)
  {
    LinkedList.Node node = this.m_UsedNodeList.GetFirst();
    for (LinkedList.Node end = this.m_UsedNodeList.GetEnd(); node != end; node = node.m_Next)
    {
      BlockAgent.BlockNode blockNode = node as BlockAgent.BlockNode;
      if (blockNode.m_Block.ContainsPoint(k, x, y))
        return blockNode;
    }
    return (BlockAgent.BlockNode) null;
  }

  private BlockAgent.BlockNode GetBlockNodeByID(int kingdomID, int blockID)
  {
    LinkedList.Node node = this.m_UsedNodeList.GetFirst();
    for (LinkedList.Node end = this.m_UsedNodeList.GetEnd(); node != end; node = node.m_Next)
    {
      BlockAgent.BlockNode blockNode = node as BlockAgent.BlockNode;
      BlockData block = blockNode.m_Block;
      if (block.KingdomID == kingdomID && block.BlockID == blockID)
        return blockNode;
    }
    return (BlockAgent.BlockNode) null;
  }

  public BlockData GetBlockAt(Coordinate location)
  {
    BlockAgent.BlockNode blockNodeAt = this.GetBlockNodeAt(location);
    if (blockNodeAt != null)
      return blockNodeAt.m_Block;
    return (BlockData) null;
  }

  public BlockData GetBlockAt(int k, int x, int y)
  {
    BlockAgent.BlockNode blockNodeAt = this.GetBlockNodeAt(k, x, y);
    if (blockNodeAt != null)
      return blockNodeAt.m_Block;
    return (BlockData) null;
  }

  public BlockData GetBlockByID(int blockID)
  {
    LinkedList.Node node = this.m_UsedNodeList.GetFirst();
    for (LinkedList.Node end = this.m_UsedNodeList.GetEnd(); node != end; node = node.m_Next)
    {
      BlockData block = (node as BlockAgent.BlockNode).m_Block;
      if (block.BlockID == blockID)
        return block;
    }
    return (BlockData) null;
  }

  public void DrawBlockID()
  {
  }

  public TileData GetSingleNearFarmOrSwamill(Coordinate location)
  {
    BlockAgent.BlockNode blockNodeAt = this.GetBlockNodeAt(location);
    if (blockNodeAt != null)
    {
      this.m_UsedNodeList.Remove((LinkedList.Node) blockNodeAt);
      this.m_UsedNodeList.PushFront((LinkedList.Node) blockNodeAt);
    }
    WorldCoordinate worldCoordinate1 = this.m_MapData.ConvertTileKXYToWorldCoordinate(location);
    TileData tileData = (TileData) null;
    float num1 = float.MaxValue;
    LinkedList.Node node = this.m_UsedNodeList.GetFirst();
    for (LinkedList.Node end = this.m_UsedNodeList.GetEnd(); node != end; node = node.m_Next)
    {
      BlockData block = (node as BlockAgent.BlockNode).m_Block;
      if (block.KingdomID == location.K)
      {
        TileData nearFarmOrSwamill = block.GetSingleNearFarmOrSwamill(location);
        if (nearFarmOrSwamill != null)
        {
          WorldCoordinate worldCoordinate2 = this.m_MapData.ConvertTileKXYToWorldCoordinate(nearFarmOrSwamill.Location);
          float num2 = (float) WorldCoordinate.DistanceSquared(worldCoordinate1, worldCoordinate2);
          if ((double) num2 < (double) num1)
          {
            tileData = nearFarmOrSwamill;
            num1 = num2;
          }
        }
      }
    }
    return tileData;
  }

  public TileData GetSingleNearMonster(Coordinate location)
  {
    BlockAgent.BlockNode blockNodeAt = this.GetBlockNodeAt(location);
    if (blockNodeAt != null)
    {
      this.m_UsedNodeList.Remove((LinkedList.Node) blockNodeAt);
      this.m_UsedNodeList.PushFront((LinkedList.Node) blockNodeAt);
    }
    WorldCoordinate worldCoordinate1 = this.m_MapData.ConvertTileKXYToWorldCoordinate(location);
    TileData tileData = (TileData) null;
    float num1 = float.MaxValue;
    LinkedList.Node node = this.m_UsedNodeList.GetFirst();
    for (LinkedList.Node end = this.m_UsedNodeList.GetEnd(); node != end; node = node.m_Next)
    {
      BlockData block = (node as BlockAgent.BlockNode).m_Block;
      if (block.KingdomID == location.K)
      {
        TileData singleNearMonster = block.GetSingleNearMonster(location);
        if (singleNearMonster != null)
        {
          WorldCoordinate worldCoordinate2 = this.m_MapData.ConvertTileKXYToWorldCoordinate(singleNearMonster.Location);
          float num2 = (float) WorldCoordinate.DistanceSquared(worldCoordinate1, worldCoordinate2);
          if ((double) num2 < (double) num1)
          {
            tileData = singleNearMonster;
            num1 = num2;
          }
        }
      }
    }
    return tileData;
  }

  public TileData GetNearEmptyTileForCity(Coordinate location, bool ignoreWonderArea)
  {
    BlockAgent.BlockNode blockNodeAt = this.GetBlockNodeAt(location);
    if (blockNodeAt != null)
    {
      this.m_UsedNodeList.Remove((LinkedList.Node) blockNodeAt);
      this.m_UsedNodeList.PushFront((LinkedList.Node) blockNodeAt);
    }
    WorldCoordinate worldCoordinate1 = this.m_MapData.ConvertTileKXYToWorldCoordinate(location);
    TileData tileData = (TileData) null;
    float num1 = float.MaxValue;
    LinkedList.Node node = this.m_UsedNodeList.GetFirst();
    for (LinkedList.Node end = this.m_UsedNodeList.GetEnd(); node != end; node = node.m_Next)
    {
      BlockData block = (node as BlockAgent.BlockNode).m_Block;
      if (block.KingdomID == location.K)
      {
        TileData emptyTileForCity = block.GetNearEmptyTileForCity(location, ignoreWonderArea);
        if (emptyTileForCity != null)
        {
          WorldCoordinate worldCoordinate2 = this.m_MapData.ConvertTileKXYToWorldCoordinate(emptyTileForCity.Location);
          float num2 = (float) WorldCoordinate.DistanceSquared(worldCoordinate1, worldCoordinate2);
          if ((double) num2 < (double) num1)
          {
            tileData = emptyTileForCity;
            num1 = num2;
          }
        }
      }
    }
    return tileData;
  }

  private Hashtable GenerateRequest()
  {
    Hashtable hashtable = new Hashtable();
    ArrayList arrayList1 = new ArrayList();
    ArrayList arrayList2 = new ArrayList();
    while (!this.m_RequestList.IsEmpty())
    {
      BlockAgent.BlockRequest o = (BlockAgent.BlockRequest) this.m_RequestList.PopFront();
      ArrayList blockParameter = this.GenerateBlockParameter(o.KXY);
      this.m_RequestPool.Release(o);
      if (o.Type == BlockAgent.RequestType.ENTER)
      {
        BlockAgent.BlockNode blockNode = this.m_NodePool.Allocate();
        blockNode.m_Block.Reset(o.KXY, this.m_MapData);
        blockNode.m_Block.BeginLoading();
        this.m_UsedNodeList.PushFront((LinkedList.Node) blockNode);
        ++this.m_UsedNodeCount;
        arrayList2.Add((object) blockParameter);
      }
      else
        arrayList1.Add((object) blockParameter);
    }
    hashtable[(object) "leave"] = (object) arrayList1;
    hashtable[(object) "enter"] = (object) arrayList2;
    hashtable[(object) "city_id"] = (object) PlayerData.inst.cityId;
    return hashtable;
  }

  private ArrayList GenerateBlockParameter(Coordinate location)
  {
    ArrayList arrayList = new ArrayList();
    arrayList.Add((object) location.K);
    int blockId = BlockData.CalculateBlockID(location.X, location.Y, this.m_MapData);
    arrayList.Add((object) blockId);
    return arrayList;
  }

  private enum RequestType
  {
    LEAVE,
    ENTER,
  }

  private class BlockNode : LinkedList.Node
  {
    public BlockData m_Block = new BlockData();
  }

  private class BlockRequest : LinkedList.Node
  {
    public Coordinate KXY;
    public BlockAgent.RequestType Type;
  }
}
