﻿// Decompiled with JetBrains decompiler
// Type: WorldBossStaticInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class WorldBossStaticInfo
{
  public const string IMAGE_ROOT_PATH = "Texture/Monster/";
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "dispear_time")]
  public int DispearTime;
  [Config(Name = "name")]
  public string Name;
  [Config(Name = "type")]
  public string Type;
  [Config(Name = "boss_level")]
  public int Level;
  [Config(Name = "stamina_cost")]
  public int StaminaCost;
  [Config(CustomParse = true, CustomType = typeof (UnitGroup), Name = "Units")]
  public UnitGroup TotalUnits;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "BaseRewards")]
  public Reward BaseRewards;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "KillRewards")]
  public Reward KillRewards;
  [Config(Name = "max_round")]
  public int MaxRound;
  [Config(Name = "description")]
  public string Desc;
  [Config(Name = "reward_display_name_1")]
  public string DisplayItemName_1;
  [Config(Name = "reward_display_image_1")]
  public string DisplayItemImage_1;
  [Config(Name = "reward_display_name_2")]
  public string DisplayItemName_2;
  [Config(Name = "reward_display_image_2")]
  public string DisplayItemImage_2;
  [Config(Name = "reward_display_name_3")]
  public string DisplayItemName_3;
  [Config(Name = "reward_display_image_3")]
  public string DisplayItemImage_3;
  [Config(Name = "reward_display_name_4")]
  public string DisplayItemName_4;
  [Config(Name = "reward_display_image_4")]
  public string DisplayItemImage_4;

  public string LocalDesc
  {
    get
    {
      if (string.IsNullOrEmpty(this.Desc))
        return string.Empty;
      return ScriptLocalization.Get(this.Desc, true);
    }
  }

  public string LocalName
  {
    get
    {
      if (string.IsNullOrEmpty(this.Name))
        return string.Empty;
      return ScriptLocalization.Get(this.Name, true);
    }
  }

  public string ImagePath
  {
    get
    {
      return "Texture/Monster/" + this.Type;
    }
  }
}
