﻿// Decompiled with JetBrains decompiler
// Type: Spin
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Examples/Spin")]
public class Spin : MonoBehaviour
{
  public Vector3 rotationsPerSecond = new Vector3(0.0f, 0.1f, 0.0f);
  public bool ignoreTimeScale;
  private Rigidbody mRb;
  private Transform mTrans;

  private void Start()
  {
    this.mTrans = this.transform;
    this.mRb = this.GetComponent<Rigidbody>();
  }

  private void Update()
  {
    if (!((Object) this.mRb == (Object) null))
      return;
    this.ApplyDelta(!this.ignoreTimeScale ? Time.deltaTime : RealTime.deltaTime);
  }

  private void FixedUpdate()
  {
    if (!((Object) this.mRb != (Object) null))
      return;
    this.ApplyDelta(Time.deltaTime);
  }

  public void ApplyDelta(float delta)
  {
    delta *= 360f;
    Quaternion quaternion = Quaternion.Euler(this.rotationsPerSecond * delta);
    if ((Object) this.mRb == (Object) null)
      this.mTrans.rotation *= quaternion;
    else
      this.mRb.MoveRotation(this.mRb.rotation * quaternion);
  }
}
