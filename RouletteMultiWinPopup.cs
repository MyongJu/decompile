﻿// Decompiled with JetBrains decompiler
// Type: RouletteMultiWinPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class RouletteMultiWinPopup : Popup
{
  private List<Vector3> localPos = new List<Vector3>();
  private Dictionary<int, RouletteMultiWinItemRender> _itemDict = new Dictionary<int, RouletteMultiWinItemRender>();
  private List<RouletteMultiWinItemRender> renderList = new List<RouletteMultiWinItemRender>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private const int TOTAL_REWARD_COUNT = 10;
  private const string DRAW_LOC = "tavern_chance_spin_number";
  private const string MIDDLE_EFFECT = "Prefab/VFX/fx_scroll_equipment_01";
  private const string HIGH_EFFECT = "Prefab/VFX/fx_scroll_equipment_02";
  private const string BIG_EFFECT = "Prefab/VFX/fx_TurnableWinPopup";
  [SerializeField]
  private GameObject _itemPrefab;
  [SerializeField]
  private UIGrid grid;
  [SerializeField]
  private UIScrollView scrollView;
  [SerializeField]
  private GameObject bigEffeParent;
  [SerializeField]
  private UILabel _diceLabel;
  [SerializeField]
  private UILabel _dicePrice;
  [SerializeField]
  private UITexture _diceTenTexture;
  private List<MultiWinItemData> _rewardDataList;
  private System.Action _onWinFinished;
  private System.Action _onDrawBegin;
  private bool _isPopupHide;
  private bool _showAnimation;
  private Color _dicePlayCostTextColor;
  private int _time;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    RouletteMultiWinPopup.Parameter parameter = orgParam as RouletteMultiWinPopup.Parameter;
    if (parameter != null)
    {
      this._rewardDataList = parameter.rewardDataList;
      this._onWinFinished = parameter.onWinFinished;
      this._onDrawBegin = parameter.onDrawBegin;
      this._time = parameter.time;
    }
    this._dicePlayCostTextColor = this._diceLabel.color;
    Oscillator.Instance.updateEvent += new System.Action<double>(this.OnUpdateEvent);
    this.UpdateUI();
  }

  private void OnUpdateEvent(double time)
  {
    if (!this._showAnimation)
      return;
    this.StartCoroutine(this.ShowRewardAnimation());
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    Oscillator.Instance.updateEvent -= new System.Action<double>(this.OnUpdateEvent);
    this._isPopupHide = true;
    this.ClearData();
    this.ExecuteWinFinished();
  }

  public override string Type
  {
    get
    {
      return "Roulette/TurnableWinPopup";
    }
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnOkayBtnPressed()
  {
    this.OnCloseBtnPressed();
  }

  public void OnRewardAnimSlammed()
  {
    this.StopMultiWinAnim();
  }

  public void OnPlayDrawPressed()
  {
    if (this._onDrawBegin == null)
      return;
    this.OnCloseBtnPressed();
    this._onDrawBegin();
  }

  private void ExecuteWinFinished()
  {
    if (this._onWinFinished == null)
      return;
    this._onWinFinished();
  }

  private void StopMultiWinAnim()
  {
    using (Dictionary<int, RouletteMultiWinItemRender>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.StopMultiWinAnim();
    }
  }

  [DebuggerHidden]
  private IEnumerator CheckSingleRewardStatus()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RouletteMultiWinPopup.\u003CCheckSingleRewardStatus\u003Ec__Iterator96()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator CacheOriginalTransformInNextFrame()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RouletteMultiWinPopup.\u003CCacheOriginalTransformInNextFrame\u003Ec__Iterator97()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void UpdateUI()
  {
    if (this._rewardDataList == null)
      return;
    this.AddBigEffect();
    for (int key = 0; key < this._rewardDataList.Count; ++key)
    {
      RouletteMultiWinItemRender itemRenderer = this.CreateItemRenderer(this._rewardDataList[key], this.grid.transform);
      this._itemDict.Add(key, itemRenderer);
      this.renderList.Add(itemRenderer);
    }
    this.StartCoroutine(this.CacheOriginalTransformInNextFrame());
    MarksmanPayload.Instance.FeedMarksmanTexture(this._diceTenTexture, ConfigManager.inst.DB_Items.GetItem(RoulettePayload.Instance.ItemId).ImagePath);
    this._diceLabel.text = ScriptLocalization.GetWithPara("tavern_chance_spin_number", new Dictionary<string, string>()
    {
      {
        "0",
        this._time.ToString()
      }
    }, true);
    int num = RoulettePayload.Instance.GoldSpent * this._time;
    this._dicePrice.text = num.ToString();
    this._dicePrice.color = RoulettePayload.Instance.ConsumableGoldCount < num ? Color.red : this._dicePlayCostTextColor;
    Utils.ExecuteInSecs(0.5f, (System.Action) (() =>
    {
      if (this._isPopupHide)
        return;
      this.StartCoroutine(this.CheckSingleRewardStatus());
    }));
  }

  private void ResetLocalPositons()
  {
    for (int index = 0; index < this.renderList.Count; ++index)
      this.renderList[index].transform.localPosition = Vector3.zero;
  }

  private void ResetLocalScales()
  {
    for (int index = 0; index < this.renderList.Count; ++index)
      this.renderList[index].transform.localScale = Vector3.zero;
  }

  [DebuggerHidden]
  private IEnumerator ShowRewardAnimation()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RouletteMultiWinPopup.\u003CShowRewardAnimation\u003Ec__Iterator98()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void ClearData()
  {
    using (Dictionary<int, RouletteMultiWinItemRender>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private RouletteMultiWinItemRender CreateItemRenderer(MultiWinItemData rewardData, Transform parent)
  {
    this._itemPool.Initialize(this._itemPrefab, parent.gameObject);
    GameObject gameObject = this._itemPool.AddChild(parent.gameObject);
    gameObject.SetActive(true);
    RouletteMultiWinItemRender component = gameObject.GetComponent<RouletteMultiWinItemRender>();
    component.SetData(rewardData);
    return component;
  }

  private void AddVisualEffect()
  {
    List<int> middleRewards = RoulettePayload.Instance.MiddleRewards;
    List<int> highRewards = RoulettePayload.Instance.HighRewards;
    for (int index1 = 0; index1 < this.renderList.Count; ++index1)
    {
      for (int index2 = 0; index2 < middleRewards.Count; ++index2)
      {
        if (middleRewards[index2] == this.renderList[index1].Data.itemId)
          this.AttachEffect(this.renderList[index1], "Prefab/VFX/fx_scroll_equipment_01");
      }
    }
    for (int index1 = 0; index1 < this.renderList.Count; ++index1)
    {
      for (int index2 = 0; index2 < highRewards.Count; ++index2)
      {
        if (highRewards[index2] == this.renderList[index1].Data.itemId)
          this.AttachEffect(this.renderList[index1], "Prefab/VFX/fx_scroll_equipment_02");
      }
    }
  }

  private void AttachEffect(RouletteMultiWinItemRender render, string path)
  {
    GameObject go = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad(path, (System.Type) null) as GameObject);
    go.transform.parent = render.transform;
    go.transform.localPosition = Vector3.zero;
    go.transform.localScale = Vector3.one;
    Utils.SetLayer(go, render.gameObject.layer);
  }

  private void AddBigEffect()
  {
    GameObject go = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad("Prefab/VFX/fx_TurnableWinPopup", (System.Type) null) as GameObject);
    go.transform.parent = this.bigEffeParent.transform;
    go.transform.localPosition = Vector3.zero;
    go.transform.localScale = Vector3.one;
    Utils.SetLayer(go, this.grid.gameObject.layer);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int time;
    public List<MultiWinItemData> rewardDataList;
    public System.Action onWinFinished;
    public System.Action onDrawBegin;
  }
}
