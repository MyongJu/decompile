﻿// Decompiled with JetBrains decompiler
// Type: MazeRoomRaidChestItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class MazeRoomRaidChestItem : MonoBehaviour, IRoomEventProcessor
{
  public const string RAID_CHEST = "Prefabs/Chest/RaidChest";
  [SerializeField]
  private Transform _chestPositionLeft;
  [SerializeField]
  private Transform _chestPositionCenter;
  private GameObject _chestInstance;
  private Room _currentRoom;

  public bool IsSkipAble()
  {
    return true;
  }

  public void ProcessRoomEvent(Room room)
  {
    this._currentRoom = room;
    this.SetEnabled(true);
    this.Clear();
    this.CreateChest();
  }

  public void SetEnabled(bool enabled)
  {
    this.gameObject.SetActive(enabled);
  }

  private void Clear()
  {
    if (!(bool) ((UnityEngine.Object) this._chestInstance))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this._chestInstance);
    this._chestInstance = (GameObject) null;
  }

  private void CreateChest()
  {
    UnityEngine.Object original = AssetManager.Instance.Load("Prefabs/Chest/RaidChest", (System.Type) null);
    if (!(bool) original)
      return;
    Transform parent = !this._currentRoom.IsExit ? this._chestPositionCenter : this._chestPositionLeft;
    this._chestInstance = UnityEngine.Object.Instantiate(original) as GameObject;
    this._chestInstance.transform.SetParent(parent);
    this._chestInstance.transform.localScale = Vector3.one;
    this._chestInstance.transform.localPosition = Vector3.zero;
    UIButton component1 = this._chestInstance.GetComponent<UIButton>();
    if ((bool) ((UnityEngine.Object) component1))
      component1.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnClicked)));
    Animator component2 = this._chestInstance.GetComponent<Animator>();
    if ((bool) ((UnityEngine.Object) component2))
      component2.enabled = false;
    AnimationEventReceiver component3 = this._chestInstance.GetComponent<AnimationEventReceiver>();
    if ((bool) ((UnityEngine.Object) component3))
      component3.onAnimationEnd = new System.Action(this.OnPlayOpenAnimationCompleted);
    AssetManager.Instance.UnLoadAsset("Prefabs/Chest/RaidChest", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
  }

  private void OnClicked()
  {
    this.PlayOpenAnimation();
  }

  private void PlayOpenAnimation()
  {
    Animator component = this._chestInstance.GetComponent<Animator>();
    if ((bool) ((UnityEngine.Object) component))
      component.enabled = true;
    AudioManager.Instance.PlaySound("sfx_dragon_knight_open_chest", false);
  }

  private void OnPlayOpenAnimationCompleted()
  {
    this.Clear();
    this.CreateChest();
    UIManager.inst.OpenPopup("DragonKnight/DungeonMopupInventoryPopup", (Popup.PopupParameter) new DungeonMopupInventoryPopup.Parameter()
    {
      raidChest = this._currentRoom.AllRaidChest[0],
      floor = this._currentRoom.Floor,
      x = this._currentRoom.RoomX,
      y = this._currentRoom.RoomY
    });
  }
}
