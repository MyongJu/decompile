﻿// Decompiled with JetBrains decompiler
// Type: PVPPortalBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UnityEngine;

public class PVPPortalBanner : SpriteBanner
{
  protected SpriteCreator m_spriteCreator = new SpriteCreator();
  [SerializeField]
  protected GameObject m_rootTrail;
  [SerializeField]
  protected TextMesh m_bossName;
  [SerializeField]
  protected SpriteRenderer m_bossStageFrame;
  [SerializeField]
  protected SpriteRenderer m_bossIcon;
  [SerializeField]
  protected TextMesh m_remainTime;
  [SerializeField]
  protected TextMesh m_progressDesc;
  [SerializeField]
  protected TextMesh m_remainTimeDesc;
  [SerializeField]
  protected UISpriteMeshProgressBar m_timeProgressBar;
  [SerializeField]
  protected UISpriteMeshProgressBar m_troopProgressBar;
  protected TileData m_tileData;

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.SecondTick);
  }

  public void OnDisable()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.SecondTick);
    if ((bool) ((UnityEngine.Object) this.m_bossStageFrame))
      this.m_bossStageFrame.sprite = (UnityEngine.Sprite) null;
    if ((bool) ((UnityEngine.Object) this.m_bossIcon))
      this.m_bossIcon.sprite = (UnityEngine.Sprite) null;
    this.m_spriteCreator.DestroyAllCreated();
  }

  protected void SecondTick(int delta)
  {
    this.UpdateUI(this.m_tileData);
  }

  public override void UpdateUI(TileData tile)
  {
    base.UpdateUI(tile);
    this.m_tileData = tile;
    AlliancePortalData portalData = this.m_tileData.PortalData;
    if (portalData == null)
      return;
    this.m_rootTrail.SetActive(false);
    this.m_NameLabel.text = this.GetAllianceName(portalData) + this.GetPortalName(portalData) + this.GetPortalState(portalData);
    if (portalData.AllianceId != PlayerData.inst.allianceId)
    {
      this.m_NameLabel.gameObject.SetActive(true);
    }
    else
    {
      AlliancePortalData.BossSummonState portalBossSummonState = AllianceBuildUtils.GetAlliancePortalBossSummonState();
      AllianceBossData allianceBossData = DBManager.inst.DB_AllianceBoss.GetAllianceBossData();
      if (allianceBossData == null)
        return;
      bool flag = portalBossSummonState == AlliancePortalData.BossSummonState.ACTIVATED || portalBossSummonState == AlliancePortalData.BossSummonState.ACTIVATED_TO_RALLY;
      this.m_rootTrail.SetActive(flag);
      this.m_NameLabel.gameObject.SetActive(!flag);
      if (!flag)
        return;
      AllianceBossInfo allianceBossInfo = ConfigManager.inst.DB_AllianceBoss.Get((int) portalData.BossId);
      if (allianceBossInfo == null)
        return;
      this.m_bossName.text = string.Format("Lv.{0} {1}", (object) allianceBossInfo.bossLevel, (object) ScriptLocalization.Get(allianceBossInfo.name, true));
      this.m_remainTime.text = ScriptLocalization.Get("alliance_fort_time_remaining", true);
      this.m_progressDesc.text = string.Format("{0:0.00%}", (object) (float) ((double) allianceBossData.troopsCount / (double) allianceBossData.startTroopsCount));
      this.m_troopProgressBar.SetProgress((float) allianceBossData.troopsCount / (float) allianceBossData.startTroopsCount);
      int valueInt = ConfigManager.inst.DB_GameConfig.GetData("alliance_boss_duration").ValueInt;
      int time = (int) (allianceBossData.CTime + (long) (valueInt * 60) - (long) NetServerTime.inst.ServerTimestamp);
      this.m_timeProgressBar.SetProgress((float) time / (float) (valueInt * 60));
      this.m_remainTimeDesc.text = Utils.FormatTime(time, true, false, true);
      this.m_bossStageFrame.sprite = this.m_spriteCreator.CreateSprite(allianceBossInfo.StageImagePath);
      this.m_bossIcon.sprite = this.m_spriteCreator.CreateSprite(allianceBossInfo.ImagePath);
    }
  }

  protected string GetAllianceName(AlliancePortalData portalData)
  {
    string str = string.Empty;
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(portalData.AllianceId);
    if (allianceData != null)
      str = string.Format("({0})", (object) allianceData.allianceAcronym);
    return str;
  }

  protected string GetPortalName(AlliancePortalData portalData)
  {
    return AllianceBuildUtils.GetBuildName(portalData.ConfigId);
  }

  protected string GetPortalState(AlliancePortalData portalData)
  {
    string str = string.Empty;
    switch (portalData.CurrentState)
    {
      case AlliancePortalData.State.UNCOMPLETE:
        str = Utils.XLAT("alliance_fort_status_unfinished");
        break;
      case AlliancePortalData.State.BUILDING:
        str = Utils.XLAT("alliance_fort_status_under_construction");
        break;
    }
    if (!string.IsNullOrEmpty(str))
      str = string.Format("\r\n({0})", (object) str);
    return str;
  }
}
