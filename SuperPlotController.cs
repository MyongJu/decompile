﻿// Decompiled with JetBrains decompiler
// Type: SuperPlotController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class SuperPlotController : MonoBehaviour
{
  public float waittime = 0.5f;
  private const float playtime = 1f;
  private const string unlockeffectpath = "Prefab/VFX/fx_jianzhu_yan_gaoji";
  private int SlotId;
  private bool locked;
  public CityPlotController cpc;
  public GameObject locksign;
  public GameObject canunlockeffect;

  public bool Locked
  {
    get
    {
      CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
      this.locked = cityData.plotsInfo == null || !cityData.plotsInfo.ContainsKey((object) this.SlotId.ToString()) || !("1" == cityData.plotsInfo[(object) this.SlotId.ToString()].ToString());
      return this.locked;
    }
  }

  private void Awake()
  {
    this.SlotId = Convert.ToInt32(this.name.Replace("SuperPlot", string.Empty));
  }

  public void OnLockClicked()
  {
    if (!Utils.IsIAPStoreEnabled)
      return;
    int num = 1;
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    if (cityData.plotsInfo != null)
      num = cityData.plotsInfo.Count + 1;
    RuralPlotsUnlockInfo data = ConfigManager.inst.DB_RuralPlotsUnlock.GetData(num.ToString());
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem((int) data.req_item);
    int itemCount = ItemBag.Instance.GetItemCount(itemStaticInfo.internalId);
    string str1 = ScriptLocalization.Get("rural_area_adv_unlock_button", true);
    if (itemCount < data.req_value)
      str1 = ScriptLocalization.Get("rural_area_adv_get_more_button", true);
    string withPara = ScriptLocalization.GetWithPara("rural_area_adv_description", new Dictionary<string, string>()
    {
      {
        "0",
        data.req_value.ToString()
      }
    }, true);
    string str2 = ScriptLocalization.Get("rural_area_adv_name", true);
    UIManager.inst.OpenPopup("UseItemCannotGoldPopup", (Popup.PopupParameter) new UseItemCannotGoldPopup.Parameter()
    {
      itemStaticInfo = itemStaticInfo,
      btnText = str1,
      needvalue = data.req_value,
      contentText = withPara,
      titleText = str2,
      param = new Hashtable()
      {
        {
          (object) "plot_id",
          (object) this.SlotId
        }
      },
      callback = (System.Action<bool, object>) ((ret, obj) =>
      {
        if (!ret)
          return;
        AudioManager.Instance.PlaySound("sfx_rural_unlock_rural_area", false);
      })
    });
  }

  public void Init()
  {
    if (this.Locked)
    {
      CityPlotController.SetVisible(this.SlotId, false);
      this.locksign.SetActive(true);
      DBManager.inst.DB_City.onDataUpdate += new System.Action<long>(this.OnPlotsInfoDataChange);
    }
    else
    {
      if (!CitadelSystem.inst.CheckSlotHasBuilding(this.SlotId))
        CityPlotController.SetVisible(this.SlotId, true);
      this.locksign.SetActive(false);
    }
  }

  public void Dispose()
  {
    DBManager.inst.DB_City.onDataUpdate -= new System.Action<long>(this.OnPlotsInfoDataChange);
    foreach (SpriteRenderer componentsInChild in this.GetComponentsInChildren<SpriteRenderer>(true))
    {
      Color color = componentsInChild.color;
      color.a = 1f;
      componentsInChild.color = color;
    }
    this.locksign.SetActive(true);
    this.canunlockeffect.SetActive(true);
    BoxCollider[] componentsInChildren = this.locksign.GetComponentsInChildren<BoxCollider>(true);
    if (componentsInChildren == null)
      return;
    foreach (Collider collider in componentsInChildren)
      collider.enabled = true;
  }

  private void OnPlotsInfoDataChange(long cityid)
  {
    if (cityid != (long) PlayerData.inst.cityId || this.Locked)
      return;
    DBManager.inst.DB_City.onDataUpdate -= new System.Action<long>(this.OnPlotsInfoDataChange);
    BoxCollider[] componentsInChildren = this.locksign.GetComponentsInChildren<BoxCollider>();
    if (componentsInChildren != null)
    {
      foreach (Collider collider in componentsInChildren)
        collider.enabled = false;
    }
    this.StartCoroutine(this.AfterUnlock());
  }

  [DebuggerHidden]
  private IEnumerator AfterUnlock()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SuperPlotController.\u003CAfterUnlock\u003Ec__Iterator47()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void Fade(float value)
  {
    foreach (SpriteRenderer componentsInChild in this.GetComponentsInChildren<SpriteRenderer>())
    {
      Color color = componentsInChild.color;
      color.a = value;
      componentsInChild.color = color;
    }
  }

  private void OnFadeComplete()
  {
    this.StartCoroutine(this.ShowPlots());
  }

  [DebuggerHidden]
  private IEnumerator ShowPlots()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SuperPlotController.\u003CShowPlots\u003Ec__Iterator48()
    {
      \u003C\u003Ef__this = this
    };
  }
}
