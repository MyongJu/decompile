﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightDungeonRewardsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonKnightDungeonRewardsPopup : Popup
{
  private Dictionary<int, DungeonRewardItemRenderer> _itemDict = new Dictionary<int, DungeonRewardItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public UILabel rankFinishHint;
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  private int _rankFinishTime;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DragonKnightDungeonRewardsPopup.Parameter parameter = orgParam as DragonKnightDungeonRewardsPopup.Parameter;
    if (parameter != null)
      this._rankFinishTime = parameter.rankFinishTime;
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.AddEventHandler();
    this.UpdateTips();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.ClearData();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateTips()
  {
    this.rankFinishHint.text = ScriptLocalization.GetWithPara("event_finishes_num", new Dictionary<string, string>()
    {
      {
        "1",
        Utils.FormatTime(this._rankFinishTime - NetServerTime.inst.ServerTimestamp, true, false, true)
      }
    }, true);
  }

  private void UpdateUI()
  {
    this.ClearData();
    List<DungeonRankingRewardInfo> rankingRewardInfoList = ConfigManager.inst.DB_DungeonRankingReward.GetDungeonRankingRewardInfoList();
    if (rankingRewardInfoList != null)
    {
      for (int key = 0; key < rankingRewardInfoList.Count; ++key)
      {
        DungeonRewardItemRenderer itemRenderer = this.CreateItemRenderer(rankingRewardInfoList[key]);
        this._itemDict.Add(key, itemRenderer);
      }
    }
    this.Reposition();
  }

  private DungeonRewardItemRenderer CreateItemRenderer(DungeonRankingRewardInfo itemInfo)
  {
    GameObject gameObject = this._itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    DungeonRewardItemRenderer component = gameObject.GetComponent<DungeonRewardItemRenderer>();
    component.SetData(itemInfo);
    return component;
  }

  private void ClearData()
  {
    using (Dictionary<int, DungeonRewardItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, DungeonRewardItemRenderer> current = enumerator.Current;
        current.Value.ClearData();
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private void OnSecondEvent(int time)
  {
    this.UpdateTips();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void RemoveEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int rankFinishTime;
  }
}
