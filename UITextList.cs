﻿// Decompiled with JetBrains decompiler
// Type: UITextList
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Text;
using UnityEngine;

[AddComponentMenu("NGUI/UI/Text List")]
public class UITextList : MonoBehaviour
{
  public int paragraphHistory = 50;
  protected char[] mSeparator = new char[1]{ '\n' };
  protected BetterList<UITextList.Paragraph> mParagraphs = new BetterList<UITextList.Paragraph>();
  public UILabel textLabel;
  public UIProgressBar scrollBar;
  public UITextList.Style style;
  protected float mScroll;
  protected int mTotalLines;
  protected int mLastWidth;
  protected int mLastHeight;

  public bool isValid
  {
    get
    {
      if ((Object) this.textLabel != (Object) null)
        return this.textLabel.ambigiousFont != (Object) null;
      return false;
    }
  }

  public float scrollValue
  {
    get
    {
      return this.mScroll;
    }
    set
    {
      value = Mathf.Clamp01(value);
      if (!this.isValid || (double) this.mScroll == (double) value)
        return;
      if ((Object) this.scrollBar != (Object) null)
      {
        this.scrollBar.value = value;
      }
      else
      {
        this.mScroll = value;
        this.UpdateVisibleText();
      }
    }
  }

  protected float lineHeight
  {
    get
    {
      if ((Object) this.textLabel != (Object) null)
        return (float) this.textLabel.fontSize + this.textLabel.effectiveSpacingY;
      return 20f;
    }
  }

  protected int scrollHeight
  {
    get
    {
      if (!this.isValid)
        return 0;
      return Mathf.Max(0, this.mTotalLines - Mathf.FloorToInt((float) this.textLabel.height / this.lineHeight));
    }
  }

  public void Clear()
  {
    this.mParagraphs.Clear();
    this.UpdateVisibleText();
  }

  private void Start()
  {
    if ((Object) this.textLabel == (Object) null)
      this.textLabel = this.GetComponentInChildren<UILabel>();
    if ((Object) this.scrollBar != (Object) null)
      EventDelegate.Add(this.scrollBar.onChange, new EventDelegate.Callback(this.OnScrollBar));
    this.textLabel.overflowMethod = UILabel.Overflow.ClampContent;
    if (this.style == UITextList.Style.Chat)
    {
      this.textLabel.pivot = UIWidget.Pivot.BottomLeft;
      this.scrollValue = 1f;
    }
    else
    {
      this.textLabel.pivot = UIWidget.Pivot.TopLeft;
      this.scrollValue = 0.0f;
    }
  }

  private void Update()
  {
    if (!this.isValid || this.textLabel.width == this.mLastWidth && this.textLabel.height == this.mLastHeight)
      return;
    this.mLastWidth = this.textLabel.width;
    this.mLastHeight = this.textLabel.height;
    this.Rebuild();
  }

  public void OnScroll(float val)
  {
    int scrollHeight = this.scrollHeight;
    if (scrollHeight == 0)
      return;
    val *= this.lineHeight;
    this.scrollValue = this.mScroll - val / (float) scrollHeight;
  }

  public void OnDrag(Vector2 delta)
  {
    int scrollHeight = this.scrollHeight;
    if (scrollHeight == 0)
      return;
    this.scrollValue = this.mScroll + delta.y / this.lineHeight / (float) scrollHeight;
  }

  private void OnScrollBar()
  {
    this.mScroll = UIProgressBar.current.value;
    this.UpdateVisibleText();
  }

  public void Add(string text)
  {
    this.Add(text, true);
  }

  protected void Add(string text, bool updateVisible)
  {
    UITextList.Paragraph paragraph;
    if (this.mParagraphs.size < this.paragraphHistory)
    {
      paragraph = new UITextList.Paragraph();
    }
    else
    {
      paragraph = this.mParagraphs[0];
      this.mParagraphs.RemoveAt(0);
    }
    paragraph.text = text;
    this.mParagraphs.Add(paragraph);
    this.Rebuild();
  }

  protected void Rebuild()
  {
    if (!this.isValid)
      return;
    this.textLabel.UpdateNGUIText();
    NGUIText.rectHeight = 1000000;
    this.mTotalLines = 0;
    for (int index = 0; index < this.mParagraphs.size; ++index)
    {
      UITextList.Paragraph paragraph = this.mParagraphs.buffer[index];
      string finalText;
      NGUIText.WrapText(paragraph.text, out finalText);
      paragraph.lines = finalText.Split('\n');
      this.mTotalLines += paragraph.lines.Length;
    }
    this.mTotalLines = 0;
    int index1 = 0;
    for (int size = this.mParagraphs.size; index1 < size; ++index1)
      this.mTotalLines += this.mParagraphs.buffer[index1].lines.Length;
    if ((Object) this.scrollBar != (Object) null)
    {
      UIScrollBar scrollBar = this.scrollBar as UIScrollBar;
      if ((Object) scrollBar != (Object) null)
        scrollBar.barSize = this.mTotalLines != 0 ? (float) (1.0 - (double) this.scrollHeight / (double) this.mTotalLines) : 1f;
    }
    this.UpdateVisibleText();
  }

  protected void UpdateVisibleText()
  {
    if (!this.isValid)
      return;
    if (this.mTotalLines == 0)
    {
      this.textLabel.text = string.Empty;
    }
    else
    {
      int num1 = Mathf.FloorToInt((float) this.textLabel.height / this.lineHeight);
      int num2 = Mathf.RoundToInt(this.mScroll * (float) Mathf.Max(0, this.mTotalLines - num1));
      if (num2 < 0)
        num2 = 0;
      StringBuilder stringBuilder = new StringBuilder();
      int index1 = 0;
      for (int size = this.mParagraphs.size; num1 > 0 && index1 < size; ++index1)
      {
        UITextList.Paragraph paragraph = this.mParagraphs.buffer[index1];
        int index2 = 0;
        for (int length = paragraph.lines.Length; num1 > 0 && index2 < length; ++index2)
        {
          string line = paragraph.lines[index2];
          if (num2 > 0)
          {
            --num2;
          }
          else
          {
            if (stringBuilder.Length > 0)
              stringBuilder.Append("\n");
            stringBuilder.Append(line);
            --num1;
          }
        }
      }
      this.textLabel.text = stringBuilder.ToString();
    }
  }

  public enum Style
  {
    Text,
    Chat,
  }

  protected class Paragraph
  {
    public string text;
    public string[] lines;
  }
}
