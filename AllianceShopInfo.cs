﻿// Decompiled with JetBrains decompiler
// Type: AllianceShopInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class AllianceShopInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "itemid")]
  public int ItemId;
  [Config(Name = "allianceLv")]
  public int AllianceLevel;
  [Config(Name = "fund_price")]
  public int FundPrice;
  [Config(Name = "honor_price")]
  public int HonorPrice;
  [Config(Name = "image")]
  public string ImageIcon;

  public string ImageIconPath
  {
    get
    {
      return "Texture/ItemIcons/" + this.ImageIcon;
    }
  }

  public string Name
  {
    get
    {
      return ConfigManager.inst.DB_Items.GetItem(this.ItemId).LocName;
    }
  }

  public string Description
  {
    get
    {
      return ConfigManager.inst.DB_Items.GetItem(this.ItemId).LocDescription;
    }
  }
}
