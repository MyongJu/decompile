﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusThirdPartyPayment
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using Funplus.Android;
using System;
using UnityEngine;

namespace Funplus
{
  public class FunplusThirdPartyPayment : MonoBehaviour
  {
    private static FunplusThirdPartyPayment.IDelegate _delegate;

    public static FunplusThirdPartyPayment Instance { get; set; }

    private BaseThirdPartyPaymentWrapper Wrapper
    {
      get
      {
        return (BaseThirdPartyPaymentWrapper) FunplusThirdParytPaymentAndroid.Instance;
      }
    }

    private void Awake()
    {
      FunplusThirdPartyPayment.Instance = this;
    }

    [Obsolete("This method is deprecated, please use FunplusThirdPartyPayment.Instance instead.")]
    public static FunplusThirdPartyPayment GetInstance()
    {
      return FunplusThirdPartyPayment.Instance;
    }

    public FunplusThirdPartyPayment SetDelegate(FunplusThirdPartyPayment.IDelegate sdkDelegate)
    {
      Debug.Log((object) "{FunplusThirdPartyPayment SetDelegate()}");
      if (sdkDelegate == null)
      {
        Debug.LogError((object) "[funsdk] `sdkDelegate` must not be null.");
        return FunplusThirdPartyPayment.Instance;
      }
      this.SetGameObjectAndDelegate(this.gameObject.name, sdkDelegate);
      return FunplusThirdPartyPayment.Instance;
    }

    public void SetGameObjectAndDelegate(string gameObjectName, FunplusThirdPartyPayment.IDelegate paymentDelegate)
    {
      Debug.Log((object) "{FunplusThirdPartyPayment SetGameObjectAndDelegate()}");
      if (!FunplusSdk.Instance.IsSdkInstalled())
        Debug.LogError((object) "[funsdk] Please call FunplusSdk.Instance.Install() first.");
      else if (FunplusThirdPartyPayment._delegate == null)
      {
        FunplusThirdPartyPayment._delegate = paymentDelegate;
        this.Wrapper.SetGameObject(gameObjectName);
      }
      else
        Debug.LogWarning((object) "[funsdk] Delegate has already been set.");
    }

    public void Show(string throughCargo)
    {
      Debug.Log((object) "{FunplusThirdPartyPayment Show()}");
      if (string.IsNullOrEmpty(throughCargo))
        Debug.LogWarning((object) "[funsdk] `throughCargo` is empty, is this really what you want?");
      this.Wrapper.Show(throughCargo);
    }

    public void onPurchaseFinished(string throughCargo)
    {
      Debug.Log((object) ("{FunplusThirdPartyPayment.onPurchaseFinished()}" + throughCargo));
      FunplusThirdPartyPayment._delegate.onPurchaseFinished(throughCargo);
    }

    public void onPendingPurchaseFound(string throughCargo)
    {
      Debug.Log((object) ("{FunplusThirdPartyPayment.onPendingPurchaseFound()}" + throughCargo));
      FunplusThirdPartyPayment._delegate.onPendingPurchaseFound(throughCargo);
    }

    public interface IDelegate
    {
      void onPurchaseFinished(string throughCargo);

      void onPendingPurchaseFound(string throughCargo);
    }
  }
}
