﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusFBFriend
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using HSMiniJSON;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Funplus
{
  public class FunplusFBFriend
  {
    public string Id { get; set; }

    public string Name { get; set; }

    public string Pic { get; set; }

    public string Gender { get; set; }

    public static List<FunplusFBFriend> FromFacebookFriendsMessage(string message)
    {
      List<FunplusFBFriend> funplusFbFriendList = new List<FunplusFBFriend>();
      try
      {
        using (List<object>.Enumerator enumerator = (Json.Deserialize(message) as List<object>).GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Dictionary<string, object> current = enumerator.Current as Dictionary<string, object>;
            funplusFbFriendList.Add(new FunplusFBFriend()
            {
              Id = (string) current["id"],
              Name = (string) current["name"],
              Pic = !current.ContainsKey("pic") ? (string) null : (string) current["pic"],
              Gender = !current.ContainsKey("gender") ? (string) null : (string) current["gender"]
            });
          }
        }
      }
      catch (Exception ex)
      {
        Debug.LogError((object) ("[funsdk] JSON parse error: " + ex.Message));
      }
      return funplusFbFriendList;
    }

    public string GetID()
    {
      return this.Id;
    }

    public string GetName()
    {
      return this.Name;
    }

    public string GetPic()
    {
      return this.Pic;
    }

    public string GetGender()
    {
      return this.Gender;
    }
  }
}
