﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusRequest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace Funplus
{
  public class FunplusRequest : MonoBehaviour
  {
    private const string PASSPORT_ENDPOINT = "https://passport.funplusgame.com/client_api.php?ver=3";
    private const string FACEBOOK_ENDPOINT = "https://graph.facebook.com/me";

    public static FunplusRequest Instance { get; set; }

    private void Awake()
    {
      FunplusRequest.Instance = this;
    }

    public void FpPost(Dictionary<string, string> formData, FpRequestSuccess onSuccess = null, FpRequestError onError = null)
    {
      this.StartCoroutine(this.FpRequest(formData, onSuccess, onError));
    }

    [DebuggerHidden]
    public IEnumerator FpRequest(Dictionary<string, string> formData, FpRequestSuccess onSuccess, FpRequestError onError)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new FunplusRequest.\u003CFpRequest\u003Ec__Iterator8()
      {
        formData = formData,
        onError = onError,
        onSuccess = onSuccess,
        \u003C\u0024\u003EformData = formData,
        \u003C\u0024\u003EonError = onError,
        \u003C\u0024\u003EonSuccess = onSuccess,
        \u003C\u003Ef__this = this
      };
    }

    public void FbGet(string accessToken, FbRequestSuccess onSuccess = null, FbRequestError onError = null)
    {
      this.StartCoroutine(this.FbRequest(accessToken, onSuccess, onError));
    }

    [DebuggerHidden]
    public IEnumerator FbRequest(string accessToken, FbRequestSuccess onSuccess = null, FbRequestError onError = null)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new FunplusRequest.\u003CFbRequest\u003Ec__Iterator9()
      {
        accessToken = accessToken,
        onError = onError,
        onSuccess = onSuccess,
        \u003C\u0024\u003EaccessToken = accessToken,
        \u003C\u0024\u003EonError = onError,
        \u003C\u0024\u003EonSuccess = onSuccess
      };
    }

    private string MakeSigV3(string postData)
    {
      return string.Format("FP {0}:{1}", (object) FunplusSdk.Instance.GameId, (object) this.Hmac256(FunplusSdk.Instance.GameKey, postData));
    }

    private string Hmac256(string key, string message)
    {
      return Convert.ToBase64String(this.StringEncode(this.HashEncode(new HMACSHA256(this.StringEncode(key)).ComputeHash(this.StringEncode(message)))));
    }

    private byte[] StringEncode(string s)
    {
      return new ASCIIEncoding().GetBytes(s);
    }

    private string HashEncode(byte[] hash)
    {
      return BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
    }

    private byte[] HexDecode(string hex)
    {
      byte[] numArray = new byte[hex.Length / 2];
      for (int index = 0; index < numArray.Length; ++index)
        numArray[index] = byte.Parse(hex.Substring(index * 2, 2), NumberStyles.HexNumber);
      return numArray;
    }
  }
}
