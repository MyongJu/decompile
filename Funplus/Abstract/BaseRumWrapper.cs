﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Abstract.BaseRumWrapper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Funplus.Abstract
{
  public abstract class BaseRumWrapper
  {
    public abstract void SetRUMDefaultConfig(string appId, string appKey, string appTag, string rumVersion, string logServerUrl, int threshold);

    public abstract void TraceServiceMonitoring(string serviceName, string httpUrl, string httpStatus, int httpLatency, int requestSize, int responseSize);

    public abstract void TraceServiceMonitoring(string serviceName, string httpUrl, string httpStatus, int httpLatency, int requestSize, int responseSize, string targetUserId, string requestId, string gameserverId, long requestTs, long receivedTs);
  }
}
