﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Abstract.BaseSdkWrapper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Funplus.Abstract
{
  public abstract class BaseSdkWrapper
  {
    public abstract void SetGameObject(string gameObjectName);

    public abstract void FunplusSDKInit(string sdkConfigString, string configServerTsString);

    public abstract void SetDefaultConfig(string configJsonString);

    public abstract void Install(string gameId, string gameKey, string environment);

    public abstract void SetDebug(bool isDebug);

    public abstract bool IsSdkInstalled();

    public abstract bool IsFirstLaunch();

    public abstract void SetGameLanguage(FunplusLanguage language);

    public abstract void SetGamePackageChannel(string packageChannel);

    public abstract void SetConfigServerEndpoint(string endpointDomain);

    public abstract void SetPassportServerEndpoint(string endpointDomain);

    public abstract void SetPaymentServerEndpoint(string endpointDomain);

    public abstract void LogUserLogin(string uid);

    public abstract void LogNewUser(string uid);

    public abstract void LogUserLogout();

    public abstract void LogUserInfoUpdate(string serverId, string userId, string userName, string userLevel, string userVipLevel, bool isPaidUser, string userIdCreatedTs);

    public abstract void LogPayment(string productId, string throughCargo, string purchaseData);
  }
}
