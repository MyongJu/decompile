﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Abstract.BasePaymentWrapper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Funplus.Abstract
{
  public abstract class BasePaymentWrapper
  {
    public abstract void SetGameObject(string gameObjectName);

    public abstract bool CanMakePurchases();

    public abstract void StartHelper();

    public abstract void SetCurrencyWhitelist(string whitelist);

    public abstract void Buy(string productId, string throughCargo);

    public abstract void Buy(string productId, string serverId, string throughCargo);

    public abstract bool GetSubsRenewing(string productId);

    public abstract bool CanCheckSubs();
  }
}
