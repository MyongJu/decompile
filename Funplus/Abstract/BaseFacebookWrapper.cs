﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Abstract.BaseFacebookWrapper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Funplus.Abstract
{
  public abstract class BaseFacebookWrapper
  {
    public abstract void SetGameObject(string gameObjectName);

    public abstract bool HasFriendsPermission();

    public abstract void AskFriendsPermission();

    public abstract bool HasPublishPermission();

    public abstract void AskPublishPermission();

    public abstract void GetUserData();

    public abstract void GetGameFriends();

    public abstract void GetGameInvitableFriends();

    public abstract void SendGameRequest(string message);

    public abstract void SendGameRequestWithPlatformId(string platformId, string message);

    public abstract void ShareLink(string title, string description, string url, string imageUrl);

    public abstract void ShareImage(string title, string description, string url, string imagePath);

    public abstract void ShareOpenGraphStory(string appNameSpace, string action, string storyType, string title, string description, string url, string imageUrl);

    public abstract void LogEvent(string eventName);
  }
}
