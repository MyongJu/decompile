﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Abstract.BaseBiWrapper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Funplus.Abstract
{
  public abstract class BaseBiWrapper
  {
    public abstract void TraceEvent(string eventName, string eventData);

    public abstract void TraceLog(string jsonStringLog);

    public abstract void TraceAction(string actionName, string extJsonString);
  }
}
