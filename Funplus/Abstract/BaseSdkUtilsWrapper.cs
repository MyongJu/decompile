﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Abstract.BaseSdkUtilsWrapper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Funplus.Abstract
{
  public abstract class BaseSdkUtilsWrapper
  {
    public abstract void SetGameObject(string gameObjectName);

    public abstract string GetTotalMemory();

    public abstract string GetAvailableMemory();

    public abstract string GetDeviceName();

    public abstract string GetOsName();

    public abstract string GetOsVersion();

    public abstract string GetLanguage();

    public abstract string GetCountry();

    public abstract string GetDeviceType();

    public abstract string GetScreenSize();

    public abstract string GetScreenOrientation();

    public abstract string GetScreenDensity();

    public abstract string GetDisplayWidth();

    public abstract string GetDisplayHeight();

    public abstract string GetIDFA();

    public abstract string GetIDFV();

    public abstract void GetGAID();

    public abstract string GetAndroidID();

    public abstract string GetAndroidIMEI();

    public abstract void ShowAlert(string title, string message, string confirm);

    public abstract string GetCurrentTimeZone();
  }
}
