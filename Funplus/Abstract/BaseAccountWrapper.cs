﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Abstract.BaseAccountWrapper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Funplus.Abstract
{
  public abstract class BaseAccountWrapper
  {
    public abstract void SetGameObject(string gameObjectName);

    public abstract bool IsUserLoggedIn();

    public abstract void GetAvailableAccountTypes();

    public abstract void OpenSession();

    public abstract void Login();

    public abstract void Login(FunplusAccountType type);

    public abstract void LoginWithEmail(string email, string password);

    public abstract void RegisterWithEmail(string email, string password);

    public abstract void ResetPassword(string email);

    public abstract void CreateNewExpressAccount();

    public abstract void Logout();

    public abstract void ShowUserCenter();

    public abstract void BindAccount();

    public abstract void BindAccount(FunplusAccountType type);

    public abstract void UnbindAccount(FunplusAccountType type);

    public abstract void UnbindAccount(FunplusAccountType type, string platformIdOrEmail);

    public abstract void SwitchAccount(FunplusAccountType type);
  }
}
