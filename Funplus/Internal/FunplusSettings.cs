﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Internal.FunplusSettings
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Funplus.Internal
{
  public class FunplusSettings : ScriptableObject
  {
    [SerializeField]
    private string androidCompileSdkVersion = "23";
    [SerializeField]
    private string androidBuildToolsVersion = "23.0.1";
    [SerializeField]
    private string androidMinSdkVersion = "15";
    [SerializeField]
    private string androidTargetSdkVersion = "23";
    [SerializeField]
    private bool facebookEnabled = true;
    [SerializeField]
    private string sdkVersion = "3.1.7";
    private const string ASSET_NAME = "FunplusSettings";
    private const string ASSET_PATH = "FunplusSDK/SDK/Resources";
    private const string ASSET_EXT = ".asset";
    private static FunplusSettings instance;
    [SerializeField]
    private string funplusGameId;
    [SerializeField]
    private string funplusGameKey;
    [SerializeField]
    private string environment;
    [SerializeField]
    private string fakeDeviceId;
    [SerializeField]
    private string androidLauncherActivity;
    [SerializeField]
    private string androidKeystorePath;
    [SerializeField]
    private string androidKeystorePassword;
    [SerializeField]
    private string androidKeystoreAlias;
    [SerializeField]
    private string androidKeystoreAliasPassword;
    [SerializeField]
    private bool androidIabEnabled;
    [SerializeField]
    private bool androidHelpshiftEnabled;
    [SerializeField]
    private bool androidMarketingEnabled;
    [SerializeField]
    private bool androidGcmEnabled;
    [SerializeField]
    private bool androidRumEnabled;
    [SerializeField]
    private string iosAppController;
    [SerializeField]
    private string facebookAppId;
    [SerializeField]
    private string facebookAppName;
    [SerializeField]
    private string facebookDebugAccessToken;
    [SerializeField]
    private string buildPath;

    private static FunplusSettings Instance
    {
      get
      {
        if ((Object) FunplusSettings.instance == (Object) null)
        {
          FunplusSettings.instance = Resources.Load(nameof (FunplusSettings)) as FunplusSettings;
          if ((Object) FunplusSettings.instance == (Object) null)
            FunplusSettings.instance = ScriptableObject.CreateInstance<FunplusSettings>();
        }
        return FunplusSettings.instance;
      }
    }

    public static string FunplusGameId
    {
      get
      {
        return FunplusSettings.Instance.funplusGameId;
      }
      set
      {
        if (!(FunplusSettings.Instance.funplusGameId != value))
          return;
        FunplusSettings.Instance.funplusGameId = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string FunplusGameKey
    {
      get
      {
        return FunplusSettings.Instance.funplusGameKey;
      }
      set
      {
        if (!(FunplusSettings.Instance.funplusGameKey != value))
          return;
        FunplusSettings.Instance.funplusGameKey = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string Environment
    {
      get
      {
        if (FunplusSettings.Instance.environment == null)
          FunplusSettings.Instance.environment = "sandbox";
        return FunplusSettings.Instance.environment;
      }
      set
      {
        if (!(FunplusSettings.Instance.environment != value))
          return;
        FunplusSettings.Instance.environment = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string FakeDeviceId
    {
      get
      {
        if (FunplusSettings.Instance.fakeDeviceId == null)
          FunplusSettings.Instance.fakeDeviceId = "ffbc8c7432c35b4402:00:00:00:00:00";
        return FunplusSettings.Instance.fakeDeviceId;
      }
      set
      {
        if (!(FunplusSettings.Instance.fakeDeviceId != value))
          return;
        FunplusSettings.Instance.fakeDeviceId = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static bool FacebookEnabled
    {
      get
      {
        return FunplusSettings.Instance.facebookEnabled;
      }
      set
      {
        if (FunplusSettings.Instance.facebookEnabled == value)
          return;
        FunplusSettings.Instance.facebookEnabled = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string FacebookAppId
    {
      get
      {
        return FunplusSettings.Instance.facebookAppId;
      }
      set
      {
        if (!(FunplusSettings.Instance.facebookAppId != value))
          return;
        FunplusSettings.Instance.facebookAppId = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string FacebookAppName
    {
      get
      {
        return FunplusSettings.Instance.facebookAppName;
      }
      set
      {
        if (!(FunplusSettings.Instance.facebookAppName != value))
          return;
        FunplusSettings.Instance.facebookAppName = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string FacebookDebugAccessToken
    {
      get
      {
        return FunplusSettings.Instance.facebookDebugAccessToken;
      }
      set
      {
        if (!(FunplusSettings.Instance.facebookDebugAccessToken != value))
          return;
        FunplusSettings.Instance.facebookDebugAccessToken = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string AndroidLauncherActivity
    {
      get
      {
        return FunplusSettings.Instance.androidLauncherActivity;
      }
      set
      {
        if (!(FunplusSettings.Instance.androidLauncherActivity != value))
          return;
        FunplusSettings.Instance.androidLauncherActivity = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string AndroidCompileSdkVersion
    {
      get
      {
        return FunplusSettings.Instance.androidCompileSdkVersion;
      }
      set
      {
        if (!(FunplusSettings.Instance.androidCompileSdkVersion != value))
          return;
        FunplusSettings.Instance.androidCompileSdkVersion = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string AndroidBuildToolsVersion
    {
      get
      {
        return FunplusSettings.Instance.androidBuildToolsVersion;
      }
      set
      {
        if (!(FunplusSettings.Instance.androidBuildToolsVersion != value))
          return;
        FunplusSettings.Instance.androidBuildToolsVersion = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string AndroidMinSdkVersion
    {
      get
      {
        return FunplusSettings.Instance.androidMinSdkVersion;
      }
      set
      {
        if (!(FunplusSettings.Instance.androidMinSdkVersion != value))
          return;
        FunplusSettings.Instance.androidMinSdkVersion = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string AndroidTargetSdkVersion
    {
      get
      {
        return FunplusSettings.Instance.androidTargetSdkVersion;
      }
      set
      {
        if (!(FunplusSettings.Instance.androidTargetSdkVersion != value))
          return;
        FunplusSettings.Instance.androidTargetSdkVersion = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string AndroidKeystorePath
    {
      get
      {
        return FunplusSettings.Instance.androidKeystorePath;
      }
      set
      {
        if (!(FunplusSettings.Instance.androidKeystorePath != value))
          return;
        FunplusSettings.Instance.androidKeystorePath = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string AndroidKeystorePassword
    {
      get
      {
        return FunplusSettings.Instance.androidKeystorePassword;
      }
      set
      {
        if (!(FunplusSettings.Instance.androidKeystorePassword != value))
          return;
        FunplusSettings.Instance.androidKeystorePassword = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string AndroidKeystoreAlias
    {
      get
      {
        return FunplusSettings.Instance.androidKeystoreAlias;
      }
      set
      {
        if (!(FunplusSettings.Instance.androidKeystoreAlias != value))
          return;
        FunplusSettings.Instance.androidKeystoreAlias = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string AndroidKeystoreAliasPassword
    {
      get
      {
        return FunplusSettings.Instance.androidKeystoreAliasPassword;
      }
      set
      {
        if (!(FunplusSettings.Instance.androidKeystoreAliasPassword != value))
          return;
        FunplusSettings.Instance.androidKeystoreAliasPassword = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static bool AndroidIabEnabled
    {
      get
      {
        return FunplusSettings.Instance.androidIabEnabled;
      }
      set
      {
        if (FunplusSettings.Instance.androidIabEnabled == value)
          return;
        FunplusSettings.Instance.androidIabEnabled = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static bool AndroidHelpshiftEnabled
    {
      get
      {
        return FunplusSettings.Instance.androidHelpshiftEnabled;
      }
      set
      {
        if (FunplusSettings.Instance.androidHelpshiftEnabled == value)
          return;
        FunplusSettings.Instance.androidHelpshiftEnabled = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static bool AndroidMarketingEnabled
    {
      get
      {
        return FunplusSettings.Instance.androidMarketingEnabled;
      }
      set
      {
        if (FunplusSettings.Instance.androidMarketingEnabled == value)
          return;
        FunplusSettings.Instance.androidMarketingEnabled = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static bool AndroidGcmEnabled
    {
      get
      {
        return FunplusSettings.Instance.androidGcmEnabled;
      }
      set
      {
        if (FunplusSettings.Instance.androidGcmEnabled == value)
          return;
        FunplusSettings.Instance.androidGcmEnabled = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static bool AndroidRumEnabled
    {
      get
      {
        return FunplusSettings.Instance.androidRumEnabled;
      }
      set
      {
        if (FunplusSettings.Instance.androidRumEnabled == value)
          return;
        FunplusSettings.Instance.androidRumEnabled = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string IosAppController
    {
      get
      {
        return FunplusSettings.Instance.iosAppController;
      }
      set
      {
        if (!(FunplusSettings.Instance.iosAppController != value))
          return;
        FunplusSettings.Instance.iosAppController = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string SdkVersion
    {
      get
      {
        return FunplusSettings.Instance.sdkVersion;
      }
      set
      {
        if (!(FunplusSettings.Instance.sdkVersion != value))
          return;
        FunplusSettings.Instance.sdkVersion = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static string BuildPath
    {
      get
      {
        return FunplusSettings.Instance.buildPath;
      }
      set
      {
        if (!(FunplusSettings.Instance.buildPath != value))
          return;
        FunplusSettings.Instance.buildPath = value;
        FunplusSettings.DirtyEditor();
      }
    }

    public static void SettingsChanged()
    {
      FunplusSettings.DirtyEditor();
    }

    private static void DirtyEditor()
    {
    }
  }
}
