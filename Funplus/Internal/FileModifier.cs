﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Internal.FileModifier
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Funplus.Internal
{
  public class FileModifier : IDisposable
  {
    private string filePath;
    private string fileContent;

    public FileModifier(string filePath)
    {
      if (!File.Exists(filePath))
        throw new FileNotFoundException("Target file does not exist: " + filePath);
      this.filePath = filePath;
      StreamReader streamReader = new StreamReader(filePath);
      this.fileContent = streamReader.ReadToEnd();
      streamReader.Close();
    }

    public bool Contains(string pattern)
    {
      return this.fileContent.Contains(pattern);
    }

    public void InsertAfter(string pattern, string textToInsert)
    {
      this.Replace(pattern, string.Format("{0}\n{1}", (object) pattern, (object) textToInsert));
    }

    public void InsertAfterIfNotExist(string pattern, string textToInsert)
    {
      if (this.Contains(textToInsert))
        return;
      this.InsertAfter(pattern, textToInsert);
    }

    public void InsertBefore(string pattern, string textToInsert)
    {
      this.Replace(pattern, string.Format("{0}\n{1}", (object) textToInsert, (object) pattern));
    }

    public void Replace(string pattern, string newText)
    {
      this.fileContent = new Regex(Regex.Escape(pattern)).Replace(this.fileContent, newText, 1);
    }

    public void Append(string newText)
    {
      FileModifier fileModifier = this;
      fileModifier.fileContent = fileModifier.fileContent + (object) '\n' + newText;
    }

    public void Write()
    {
      StreamWriter streamWriter = new StreamWriter(this.filePath);
      streamWriter.Write(this.fileContent);
      streamWriter.Close();
    }

    public void Dispose()
    {
    }
  }
}
