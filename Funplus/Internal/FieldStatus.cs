﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Internal.FieldStatus
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Funplus.Internal
{
  public class FieldStatus
  {
    public FieldStatus.StatusType Status { get; set; }

    public string Message { get; set; }

    public bool HasError()
    {
      return !this.Status.Equals((object) FieldStatus.StatusType.OK);
    }

    public enum StatusType
    {
      OK,
      MissingField,
      InvalidField,
    }
  }
}
