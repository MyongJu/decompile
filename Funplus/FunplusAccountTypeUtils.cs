﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusAccountTypeUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace Funplus
{
  internal static class FunplusAccountTypeUtils
  {
    public static FunplusAccountType FromString(string type)
    {
      if (type == null)
        return FunplusAccountType.FPAccountTypeUnknown;
      string lower = type.ToLower();
      if (lower != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (FunplusAccountTypeUtils.\u003C\u003Ef__switch\u0024map1 == null)
        {
          // ISSUE: reference to a compiler-generated field
          FunplusAccountTypeUtils.\u003C\u003Ef__switch\u0024map1 = new Dictionary<string, int>(8)
          {
            {
              "express",
              0
            },
            {
              "email",
              1
            },
            {
              "mobile",
              2
            },
            {
              "facebook",
              3
            },
            {
              "vk",
              4
            },
            {
              "wechat",
              5
            },
            {
              "googleplus",
              6
            },
            {
              "gamecenter",
              7
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (FunplusAccountTypeUtils.\u003C\u003Ef__switch\u0024map1.TryGetValue(lower, out num))
        {
          switch (num)
          {
            case 0:
              return FunplusAccountType.FPAccountTypeExpress;
            case 1:
              return FunplusAccountType.FPAccountTypeEmail;
            case 2:
              return FunplusAccountType.FPAccountTypeMobile;
            case 3:
              return FunplusAccountType.FPAccountTypeFacebook;
            case 4:
              return FunplusAccountType.FPAccountTypeVK;
            case 5:
              return FunplusAccountType.FPAccountTypeWechat;
            case 6:
              return FunplusAccountType.FPAccountTypeGooglePlus;
            case 7:
              return FunplusAccountType.FPAccountTypeGameCenter;
          }
        }
      }
      return FunplusAccountType.FPAccountTypeNotSpecified;
    }

    public static string ToString(FunplusAccountType type)
    {
      switch (type)
      {
        case FunplusAccountType.FPAccountTypeExpress:
          return "express";
        case FunplusAccountType.FPAccountTypeEmail:
          return "email";
        case FunplusAccountType.FPAccountTypeFacebook:
          return "facebook";
        case FunplusAccountType.FPAccountTypeVK:
          return "vk";
        case FunplusAccountType.FPAccountTypeWechat:
          return "wechat";
        case FunplusAccountType.FPAccountTypeGooglePlus:
          return "googleplus";
        case FunplusAccountType.FPAccountTypeGameCenter:
          return "gamecenter";
        case FunplusAccountType.FPAccountTypeMobile:
          return "mobile";
        default:
          return "unknown";
      }
    }
  }
}
