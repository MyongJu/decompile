﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusError
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using HSMiniJSON;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Funplus
{
  public class FunplusError
  {
    private static Dictionary<long, string> errors = new Dictionary<long, string>()
    {
      {
        0L,
        "No error"
      },
      {
        100L,
        "Failed to install"
      },
      {
        101L,
        "Failed to connect to config server"
      },
      {
        102L,
        "Config server request data failed"
      },
      {
        103L,
        "Parse config failed"
      },
      {
        104L,
        "Invalid configuration"
      },
      {
        105L,
        "Reflection error"
      },
      {
        106L,
        "No local configuration found"
      },
      {
        1101L,
        "Wrong signature"
      },
      {
        1102L,
        "There was a server error when creating your Funplus ID, please try again later"
      },
      {
        1103L,
        "Incorrect game ID"
      },
      {
        1104L,
        "Please input the correct email address"
      },
      {
        1105L,
        "Password format is wrong"
      },
      {
        1106L,
        "Incorrect GUID"
      },
      {
        1107L,
        "This account already exists, please input an unique email address"
      },
      {
        1108L,
        "There was a server error when creating your account"
      },
      {
        1109L,
        "This Funplus ID has already been bound"
      },
      {
        1110L,
        "There is no account with this email address"
      },
      {
        1111L,
        "Your email or password is incorrect"
      },
      {
        1112L,
        "Password must be 6~20 characters"
      },
      {
        1113L,
        "Failed to reset password"
      },
      {
        1114L,
        "Failed to reset password"
      },
      {
        1115L,
        "Failed to reset password"
      },
      {
        1116L,
        "The new password cannot be the same as the old password"
      },
      {
        1117L,
        "Failed to change password"
      },
      {
        1118L,
        "There is no account with this email address"
      },
      {
        1119L,
        "Failed to reset password"
      },
      {
        1120L,
        "Wrong game settings"
      },
      {
        1121L,
        "Failed to reset temp password"
      },
      {
        1122L,
        "Please give a valid FPID"
      },
      {
        1123L,
        "Facebook ID is not correct"
      },
      {
        1124L,
        "failed to bind with facebook"
      },
      {
        1125L,
        "Reach register limit"
      },
      {
        1126L,
        "Email address is too long"
      },
      {
        1127L,
        "Session is empty"
      },
      {
        1128L,
        "Failed to find session"
      },
      {
        1129L,
        "Session has expired"
      },
      {
        1130L,
        "Session and game do not match"
      },
      {
        1131L,
        "Platform token is invalid"
      },
      {
        1132L,
        "Platform type is invalid"
      },
      {
        1133L,
        "Platform token is invalid"
      },
      {
        1134L,
        "Platform ID and Funplus ID do not match"
      },
      {
        1135L,
        "Platform information is not found"
      },
      {
        1136L,
        "Mobile country code is invalid"
      },
      {
        1137L,
        "Mobile number is invalid"
      },
      {
        1138L,
        "This mobile number has been taken"
      },
      {
        1139L,
        "This mobile number does not exist"
      },
      {
        1140L,
        "Failed to generate confirmation code"
      },
      {
        1141L,
        "Confirmation code is invalid"
      },
      {
        1142L,
        "This mobile number has not been confirmed"
      },
      {
        1143L,
        "Failed to reset password for this mobile number"
      },
      {
        1200L,
        "Unknown account type"
      },
      {
        1201L,
        "User is not logged in"
      },
      {
        1202L,
        "User has already logged in"
      },
      {
        1203L,
        "Please wait until the previous login process completes"
      },
      {
        1204L,
        "Invalid login parameters"
      },
      {
        1205L,
        "Login failed"
      },
      {
        1206L,
        "Bind account failed"
      },
      {
        1207L,
        "Failed to connect funplus passport server"
      },
      {
        1208L,
        "Failed to parse the response from funplus passport server"
      },
      {
        2000L,
        "WeChat: Client error"
      },
      {
        2001L,
        "WeChat: User cancelled action"
      },
      {
        2002L,
        "WeChat: User login failed"
      },
      {
        2003L,
        "WeChat: Inconsistent WeChat accounts"
      },
      {
        2004L,
        "WeChat: Not in White List"
      },
      {
        2005L,
        "WeChat: Funplus ID is already bound to another account"
      },
      {
        2006L,
        "WeChat: Get user data failed"
      },
      {
        2007L,
        "WeChat: Undefined exception"
      },
      {
        2008L,
        "WeChat: Get friends data failed"
      },
      {
        2100L,
        "Facebook user not logged in"
      },
      {
        2101L,
        "Facebook user login failed"
      },
      {
        2102L,
        "Facebook failed to get user data"
      },
      {
        2103L,
        "Facebook Failed to send game request"
      },
      {
        2104L,
        "Facebook user canceled the action"
      },
      {
        2105L,
        "Facebook exception"
      },
      {
        2106L,
        "Facebook has been bound to another account"
      },
      {
        2107L,
        "Facebook failed to get game friends"
      },
      {
        2108L,
        "Facebook need user_friends permission"
      },
      {
        2200L,
        "VK user not logged in"
      },
      {
        2201L,
        "VK user login failed"
      },
      {
        2202L,
        "VK captcha error"
      },
      {
        2203L,
        "VK access denied"
      },
      {
        2204L,
        "VK failed to accept user token"
      },
      {
        2205L,
        "VK failed to receive new token"
      },
      {
        2206L,
        "VK failed to renew token"
      },
      {
        2207L,
        "VK exception"
      },
      {
        2208L,
        "VK user canceled the action"
      },
      {
        2209L,
        "VK failed to get user data"
      },
      {
        2210L,
        "VK has been bound to another account"
      },
      {
        2211L,
        "VK failed to get game friends"
      },
      {
        2300L,
        "Google Plus current person is null"
      },
      {
        2301L,
        "Google Plus failed to get access token"
      },
      {
        2302L,
        "Google Plus connection failed"
      },
      {
        2303L,
        "Google Plus failed to get game friends"
      },
      {
        3000L,
        "Failed to submit data to funplus payment server"
      },
      {
        3001L,
        "Invalid Payment Request Data"
      },
      {
        3002L,
        "Failed to parse payment response"
      },
      {
        3003L,
        "Got a unsuccessful response from payment ser"
      },
      {
        3900L,
        "Non-successful get_local_pacakges response"
      },
      {
        3901L,
        "Error parsing get_local_pacakges response"
      },
      {
        3902L,
        "Error sending get_local_pacakges request"
      },
      {
        3100L,
        "Failed to initialize Google IAB"
      },
      {
        3101L,
        "Failed to buy product via Google IAB"
      },
      {
        3102L,
        "Google account is not configured"
      },
      {
        3103L,
        "Failed to consume product via Google IAB"
      },
      {
        3104L,
        "User canceld the purchase"
      },
      {
        3105L,
        "The item is not available"
      },
      {
        3106L,
        "The item is already owned, you could try restart the game to get the item"
      },
      {
        4000L,
        "No such BI event"
      },
      {
        4001L,
        "BI event properties are not valid"
      },
      {
        9999L,
        "Unspecific error"
      }
    };

    public long ErrorCode { get; set; }

    public string ErrorMsg { get; set; }

    public string ErrorLocalizedMsg { get; set; }

    public static FunplusError E(long errorCode)
    {
      return new FunplusError()
      {
        ErrorCode = errorCode,
        ErrorMsg = FunplusError.errors[errorCode],
        ErrorLocalizedMsg = FunplusError.errors[errorCode]
      };
    }

    public static FunplusError E(long errorCode, string errorMsg)
    {
      return new FunplusError()
      {
        ErrorCode = errorCode,
        ErrorMsg = errorMsg,
        ErrorLocalizedMsg = errorMsg
      };
    }

    public static FunplusError E(long errorCode, string errorMsg, string errorLocalizedMsg)
    {
      return new FunplusError()
      {
        ErrorCode = errorCode,
        ErrorMsg = errorMsg,
        ErrorLocalizedMsg = errorLocalizedMsg
      };
    }

    public static FunplusError FromMessage(string message)
    {
      try
      {
        Dictionary<string, object> dictionary = Json.Deserialize(message) as Dictionary<string, object>;
        return FunplusError.E((long) dictionary["errorCode"], (string) dictionary["errorMsg"], (string) dictionary["errorLocalizedMsg"]);
      }
      catch (Exception ex)
      {
        Debug.LogError((object) ("[funsdk] JSON parse error: " + ex.Message));
        return FunplusError.E(9999L);
      }
    }

    public long GetErrorCode()
    {
      return this.ErrorCode;
    }

    public string GetErrorMsg()
    {
      return this.ErrorMsg;
    }

    public string GetErrorLocalizedMsg()
    {
      return this.ErrorLocalizedMsg;
    }

    public string ToJsonString()
    {
      return string.Format("{{\"errorCode\": {0}, \"errorMsg\": \"{1}\", \"errorLocalizedMsg\": \"{2}\"}}", (object) this.ErrorCode, (object) this.ErrorMsg, (object) this.ErrorLocalizedMsg);
    }
  }
}
