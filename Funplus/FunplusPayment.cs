﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusPayment
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using Funplus.Android;
using HSMiniJSON;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Funplus
{
  public class FunplusPayment : MonoBehaviour
  {
    private static FunplusPayment.IDelegate _delegate;

    public static FunplusPayment Instance { get; set; }

    private BasePaymentWrapper Wrapper
    {
      get
      {
        if (CustomDefine.PaymentWrapper != null)
          return CustomDefine.PaymentWrapper;
        return (BasePaymentWrapper) FunplusGoogleIabAndroid.Instance;
      }
    }

    private void Awake()
    {
      FunplusPayment.Instance = this;
    }

    [Obsolete("This method is deprecated, please use FunplusPayment.Instance instead.")]
    public static FunplusPayment GetInstance()
    {
      return FunplusPayment.Instance;
    }

    public FunplusPayment SetDelegate(FunplusPayment.IDelegate sdkDelegate)
    {
      if (sdkDelegate == null)
      {
        Debug.LogError((object) "[funsdk] `sdkDelegate` must not be null.");
        return FunplusPayment.Instance;
      }
      this.SetGameObjectAndDelegate(this.gameObject.name, sdkDelegate);
      return FunplusPayment.Instance;
    }

    public void SetGameObjectAndDelegate(string gameObjectName, FunplusPayment.IDelegate paymentDelegate)
    {
      if (!FunplusSdk.Instance.IsSdkInstalled())
        Debug.LogError((object) "[funsdk] Please call FunplusSdk.Instance.Install() first.");
      else if (FunplusPayment._delegate == null)
      {
        FunplusPayment._delegate = paymentDelegate;
        this.Wrapper.SetGameObject(gameObjectName);
      }
      else
        Debug.LogWarning((object) "[funsdk] Delegate has already been set.");
    }

    public void StartHelper()
    {
      if (FunplusPayment._delegate == null)
      {
        Debug.LogError((object) "[funsdk] Please call FunplusPayment.SetDelegate() first.");
      }
      else
      {
        Debug.Log((object) "[funsdk] Trying to start payment helper.");
        this.Wrapper.StartHelper();
      }
    }

    public bool CanMakePurchases()
    {
      if (FunplusPayment._delegate != null)
        return this.Wrapper.CanMakePurchases();
      return false;
    }

    public void SetCurrencyWhitelist(string whitelist)
    {
      Debug.Log((object) ("[funsdk] set currency whitelist = " + whitelist));
      this.Wrapper.SetCurrencyWhitelist(whitelist);
    }

    public void Buy(string productId, string throughCargo)
    {
      if (string.IsNullOrEmpty(productId))
      {
        Debug.LogError((object) "[funsdk] `productId` must not be empty.");
      }
      else
      {
        if (string.IsNullOrEmpty(throughCargo))
          Debug.LogWarning((object) "[funsdk] `throughCargo` is empty, is this really what you want?");
        this.Wrapper.Buy(productId, throughCargo);
      }
    }

    public void Buy(string productId, string serverId, string throughCargo)
    {
      if (string.IsNullOrEmpty(productId))
      {
        Debug.LogError((object) "[funsdk] `productId` must not be empty.");
      }
      else
      {
        if (string.IsNullOrEmpty(serverId))
          Debug.LogWarning((object) "[funsdk] `serverId` is empty, is this really what you want?");
        if (string.IsNullOrEmpty(throughCargo))
          Debug.LogWarning((object) "[funsdk] `throughCargo` is empty, is this really what you want?");
        this.Wrapper.Buy(productId, serverId, throughCargo);
      }
    }

    public bool GetSubsRenewing(string productId)
    {
      Debug.Log((object) ("[funsdk] get product id is subsRenewing = " + productId));
      if (string.IsNullOrEmpty(productId))
        return false;
      return this.Wrapper.GetSubsRenewing(productId);
    }

    public bool CanCheckSubs()
    {
      return this.Wrapper.CanCheckSubs();
    }

    public void OnPaymentInitializeSuccess(string message)
    {
      List<FunplusProduct> products = FunplusProduct.FromGoogleIabMessage(message);
      FunplusPayment._delegate.OnInitializeSuccess(products);
    }

    public void OnPaymentInitializeError(string message)
    {
      FunplusPayment._delegate.OnInitializeError(FunplusError.FromMessage(message));
    }

    public void OnPaymentPurchaseSuccess(string message)
    {
      Debug.Log((object) ("{FunplusPayment.OnPurchaseSuccess()}" + message));
      Dictionary<string, object> dictionary = Json.Deserialize(message) as Dictionary<string, object>;
      try
      {
        string productId = (string) dictionary["product_id"];
        string throughCargo = (string) dictionary["through_cargo"];
        FunplusPayment._delegate.OnPurchaseSuccess(productId, throughCargo);
      }
      catch (Exception ex)
      {
        Debug.LogErrorFormat("[funsdk] Purchase failed: {0}.", (object) ex.Message);
      }
    }

    public void OnPaymentPurchaseError(string message)
    {
      FunplusPayment._delegate.OnPurchaseError(FunplusError.FromMessage(message));
    }

    public interface IDelegate
    {
      void OnInitializeSuccess(List<FunplusProduct> products);

      void OnInitializeError(FunplusError error);

      void OnPurchaseSuccess(string productId, string throughCargo);

      void OnPurchaseError(FunplusError error);
    }
  }
}
