﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusRum
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using Funplus.Android;
using System;
using UnityEngine;

namespace Funplus
{
  public class FunplusRum
  {
    private static readonly object locker = new object();
    private static FunplusRum instance;

    private BaseRumWrapper Wrapper
    {
      get
      {
        return (BaseRumWrapper) FunplusRumAndroid.Instance;
      }
    }

    [Obsolete("This method is deprecated, please use FunplusRum.Instance instead.")]
    public static FunplusRum GetIntance()
    {
      return FunplusRum.Instance;
    }

    public static FunplusRum Instance
    {
      get
      {
        if (FunplusRum.instance == null)
        {
          lock (FunplusRum.locker)
            FunplusRum.instance = new FunplusRum();
        }
        return FunplusRum.instance;
      }
    }

    public void SetRUMDefaultConfig(string appId, string appKey, string appTag, string rumVersion, string logServerUrl, int threshold)
    {
      this.Wrapper.SetRUMDefaultConfig(appId, appKey, appTag, rumVersion, logServerUrl, threshold);
    }

    public void TraceServiceMonitoring(string serviceName, string httpUrl, string httpStatus, int httpLatency, int requestSize, int responseSize)
    {
      if (string.IsNullOrEmpty(serviceName))
        Debug.LogError((object) "[funsdk] `serviceName` must not be empty.");
      else if (string.IsNullOrEmpty(httpUrl))
        Debug.LogError((object) "[funsdk] `httpUrl` must not be empty.");
      else
        this.Wrapper.TraceServiceMonitoring(serviceName, httpUrl, httpStatus, httpLatency, requestSize, responseSize);
    }

    public void TraceServiceMonitoring(string serviceName, string httpUrl, string httpStatus, int httpLatency, int requestSize, int responseSize, string targetUserId, string requestId, string gameserverId, long requestTs, long receivedTs)
    {
      if (string.IsNullOrEmpty(serviceName))
        Debug.LogError((object) "[funsdk] `serviceName` must not be empty.");
      else if (string.IsNullOrEmpty(httpUrl))
        Debug.LogError((object) "[funsdk] `httpUrl` must not be empty.");
      else
        this.Wrapper.TraceServiceMonitoring(serviceName, httpUrl, httpStatus, httpLatency, requestSize, responseSize, targetUserId, requestId, gameserverId, requestTs, receivedTs);
    }
  }
}
