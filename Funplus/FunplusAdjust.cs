﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusAdjust
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using Funplus.Android;
using System;
using UnityEngine;

namespace Funplus
{
  public class FunplusAdjust
  {
    private static readonly object locker = new object();
    private static FunplusAdjust instance;

    private BaseAdjustWrapper Wrapper
    {
      get
      {
        return (BaseAdjustWrapper) FunplusAdjustAndroid.Instance;
      }
    }

    [Obsolete("This method is deprecated, please use FunplusSdkUtils.Instance instead.")]
    public static FunplusAdjust GetIntance()
    {
      return FunplusAdjust.Instance;
    }

    public static FunplusAdjust Instance
    {
      get
      {
        if (FunplusAdjust.instance == null)
        {
          lock (FunplusAdjust.locker)
            FunplusAdjust.instance = new FunplusAdjust();
        }
        return FunplusAdjust.instance;
      }
    }

    public void preInit(string appToken, string firstLaunchEventToken, string trackerToken)
    {
      this.Wrapper.preInit(appToken, firstLaunchEventToken, trackerToken);
    }

    public void trackEvent(string eventToken)
    {
      Debug.Log((object) ("Adjust SdK unity wrapper track event token = " + eventToken));
      this.Wrapper.trackEvent(eventToken);
    }
  }
}
