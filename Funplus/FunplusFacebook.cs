﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusFacebook
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using Funplus.Android;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Funplus
{
  public class FunplusFacebook : MonoBehaviour
  {
    private static FunplusFacebook.IDelegate _delegate;

    public static FunplusFacebook Instance { get; set; }

    private void Awake()
    {
      FunplusFacebook.Instance = this;
    }

    [Obsolete("This method is deprecated, please use FunplusFacebook.Instance instead.")]
    public static FunplusFacebook GetInstance()
    {
      return FunplusFacebook.Instance;
    }

    private BaseFacebookWrapper Wrapper
    {
      get
      {
        return (BaseFacebookWrapper) FunplusFacebookAndroid.Instance;
      }
    }

    public FunplusFacebook SetDelegate(FunplusFacebook.IDelegate sdkDelegate)
    {
      if (sdkDelegate == null)
      {
        Debug.LogError((object) "[funsdk] sdkDelegate must not be null.");
        return FunplusFacebook.Instance;
      }
      this.SetGameObjectAndDelegate(this.gameObject.name, sdkDelegate);
      return FunplusFacebook.Instance;
    }

    public void SetGameObjectAndDelegate(string gameObjectName, FunplusFacebook.IDelegate facebookDelegate)
    {
      if (!FunplusSdk.Instance.IsSdkInstalled())
        Debug.LogError((object) "[funsdk] Please call FunplusSdk.Instance.Install() first.");
      else if (FunplusFacebook._delegate == null)
      {
        FunplusFacebook._delegate = facebookDelegate;
        this.Wrapper.SetGameObject(gameObjectName);
      }
      else
        Debug.LogWarning((object) "[funsdk] Delegate has already been set.");
    }

    public bool HasFriendsPermission()
    {
      return this.Wrapper.HasFriendsPermission();
    }

    public void AskFriendsPermission()
    {
      this.Wrapper.AskFriendsPermission();
    }

    public bool HasPublishPermission()
    {
      return this.Wrapper.HasPublishPermission();
    }

    public void AskPublishPermission()
    {
      this.Wrapper.AskPublishPermission();
    }

    public void GetUserData()
    {
      this.Wrapper.GetUserData();
    }

    public void GetGameFriends()
    {
      this.Wrapper.GetGameFriends();
    }

    public void GetGameInvitableFriends()
    {
      this.Wrapper.GetGameInvitableFriends();
    }

    public void SendGameRequest(string message)
    {
      this.Wrapper.SendGameRequest(message);
    }

    public void SendGameRequestWithPlatformId(string platformId, string message)
    {
      this.Wrapper.SendGameRequestWithPlatformId(platformId, message);
    }

    public void ShareLink(string title, string description, string url, string imageUrl)
    {
      this.Wrapper.ShareLink(title, description, url, imageUrl);
    }

    public void ShareImage(string title, string description, string url, string imagePath)
    {
      this.Wrapper.ShareImage(title, description, url, imagePath);
    }

    public void ShareOpenGraphStory(string appNameSpace, string action, string storyType, string title, string description, string url, string imageUrl)
    {
      this.Wrapper.ShareOpenGraphStory(appNameSpace, action, storyType, title, description, url, imageUrl);
    }

    public void LogEvent(string eventName)
    {
      this.Wrapper.LogEvent(eventName);
    }

    public void OnFacebookAskFriendsPermission(string message)
    {
      if (message == "true")
        FunplusFacebook._delegate.OnAskFriendsPermission(true);
      else
        FunplusFacebook._delegate.OnAskFriendsPermission(false);
    }

    public void OnFacebookAskPublishPermission(string message)
    {
      if (message == "true")
        FunplusFacebook._delegate.OnAskPublishPermission(true);
      else
        FunplusFacebook._delegate.OnAskPublishPermission(false);
    }

    public void OnFacebookGetUserDataSuccess(string message)
    {
      FunplusFacebook._delegate.OnGetUserDataSuccess(FunplusSocialUser.GetUserDataFromMessage(message));
    }

    public void OnFacebookGetUserDataError(string message)
    {
      FunplusFacebook._delegate.OnGetUserDataError(FunplusError.FromMessage(message));
    }

    public void OnFacebookGetGameFriendsSuccess(string message)
    {
      FunplusFacebook._delegate.OnGetGameFriendsSuccess(FunplusFBFriend.FromFacebookFriendsMessage(message));
    }

    public void OnFacebookGetGameFriendsError(string message)
    {
      FunplusFacebook._delegate.OnGetGameFriendsError(FunplusError.FromMessage(message));
    }

    public void OnFacebookGetGameInvitableFriendsSuccess(string message)
    {
      FunplusFacebook._delegate.OnGetGameInvitableFriendsSuccess(FunplusFBFriend.FromFacebookFriendsMessage(message));
    }

    public void OnFacebookGetGameInvitableFriendsError(string message)
    {
      FunplusFacebook._delegate.OnGetGameInvitableFriendsError(FunplusError.FromMessage(message));
    }

    public void OnFacebookSendGameRequestSuccess(string message)
    {
      FunplusFacebook._delegate.OnSendGameRequestSuccess(message);
    }

    public void OnFacebookSendGameRequestError(string message)
    {
      FunplusFacebook._delegate.OnSendGameRequestError(FunplusError.FromMessage(message));
    }

    public void OnFacebookShareSuccess(string message)
    {
      FunplusFacebook._delegate.OnShareSuccess(message);
    }

    public void OnFacebookShareError(string message)
    {
      FunplusFacebook._delegate.OnShareError(FunplusError.FromMessage(message));
    }

    public void OnFacebookOpenGraphStoryShareSuccess(string message)
    {
      FunplusFacebook._delegate.OnOpenGraphStoryShareSuccess(message);
    }

    public void OnFacebookOpenGraphStoryShareError(string message)
    {
      FunplusFacebook._delegate.OnOpenGraphStoryShareError(FunplusError.FromMessage(message));
    }

    public interface IDelegate
    {
      void OnAskFriendsPermission(bool result);

      void OnAskPublishPermission(bool result);

      void OnGetUserDataSuccess(FunplusSocialUser user);

      void OnGetUserDataError(FunplusError error);

      void OnGetGameFriendsSuccess(List<FunplusFBFriend> friends);

      void OnGetGameFriendsError(FunplusError error);

      void OnGetGameInvitableFriendsSuccess(List<FunplusFBFriend> friends);

      void OnGetGameInvitableFriendsError(FunplusError error);

      void OnSendGameRequestSuccess(string result);

      void OnSendGameRequestError(FunplusError error);

      void OnShareSuccess(string result);

      void OnShareError(FunplusError error);

      void OnOpenGraphStoryShareSuccess(string result);

      void OnOpenGraphStoryShareError(FunplusError error);
    }
  }
}
