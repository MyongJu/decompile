﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusSdkUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using Funplus.Android;
using System;
using UnityEngine;

namespace Funplus
{
  public class FunplusSdkUtils : MonoBehaviour
  {
    private static FunplusSdkUtils.IDelegate _delegate;

    public static FunplusSdkUtils Instance { get; set; }

    private BaseSdkUtilsWrapper Wrapper
    {
      get
      {
        return (BaseSdkUtilsWrapper) FunplusSdkUtilsAndroid.Instance;
      }
    }

    private void Awake()
    {
      FunplusSdkUtils.Instance = this;
    }

    [Obsolete("This method is deprecated, please use FunplusSdkUtils.Instance instead.")]
    public static FunplusSdkUtils GetIntance()
    {
      return FunplusSdkUtils.Instance;
    }

    public FunplusSdkUtils SetDelegate(FunplusSdkUtils.IDelegate sdkDelegate)
    {
      if (sdkDelegate == null)
      {
        Debug.LogError((object) "[funsdk] `sdkDelegate` must not be null.");
        return FunplusSdkUtils.Instance;
      }
      this.SetGameObjectAndDelegate(this.gameObject.name, sdkDelegate);
      return FunplusSdkUtils.Instance;
    }

    public void SetGameObjectAndDelegate(string gameObjectName, FunplusSdkUtils.IDelegate sdkDelegate)
    {
      if (FunplusSdkUtils._delegate == null)
      {
        FunplusSdkUtils._delegate = sdkDelegate;
        this.Wrapper.SetGameObject(gameObjectName);
      }
      else
        Debug.LogWarning((object) "[funsdk] Delegate has already been set.");
    }

    public string GetTotalMemory()
    {
      return this.Wrapper.GetTotalMemory();
    }

    public string GetAvailableMemory()
    {
      return this.Wrapper.GetAvailableMemory();
    }

    public string GetDeviceName()
    {
      return this.Wrapper.GetDeviceName();
    }

    public string GetOsName()
    {
      return this.Wrapper.GetOsName();
    }

    public string GetOsVersion()
    {
      return this.Wrapper.GetOsVersion();
    }

    public string GetLanguage()
    {
      return this.Wrapper.GetLanguage();
    }

    public string GetCountry()
    {
      return this.Wrapper.GetCountry();
    }

    public string GetDeviceType()
    {
      return this.Wrapper.GetDeviceType();
    }

    public string GetScreenSize()
    {
      return this.Wrapper.GetScreenSize();
    }

    public string GetScreenOrientation()
    {
      return this.Wrapper.GetScreenOrientation();
    }

    public string GetScreenDensity()
    {
      return this.Wrapper.GetScreenDensity();
    }

    public string GetDisplayWidth()
    {
      return this.Wrapper.GetDisplayWidth();
    }

    public string GetDisplayHeight()
    {
      return this.Wrapper.GetDisplayHeight();
    }

    public string GetHashKey()
    {
      return (string) null;
    }

    public string GetIDFA()
    {
      return this.Wrapper.GetIDFA();
    }

    public string GetIDFV()
    {
      return this.Wrapper.GetIDFV();
    }

    public void GetGAID()
    {
      this.Wrapper.GetGAID();
    }

    public string GetAndroidID()
    {
      return this.Wrapper.GetAndroidID();
    }

    public string GetAndroidIMEI()
    {
      return this.Wrapper.GetAndroidIMEI();
    }

    public void ShowAlert(string title, string message, string confirm)
    {
      this.Wrapper.ShowAlert(title, message, confirm);
    }

    public string GetCurrentTimeZone()
    {
      return this.Wrapper.GetCurrentTimeZone();
    }

    public void OnFunplusSdkUtilsGetGaidCallback(string gaid)
    {
      FunplusSdkUtils._delegate.OnFunplusSdkUtilsGetGaidCallback(gaid);
    }

    public interface IDelegate
    {
      void OnFunplusSdkUtilsGetGaidCallback(string gaid);
    }
  }
}
