﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Android.FunplusSdkUtilsAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Android
{
  public class FunplusSdkUtilsAndroid : BaseSdkUtilsWrapper
  {
    private static readonly object locker = new object();
    private AndroidJavaClass funplusSdkUtilsWrapper;
    private static FunplusSdkUtilsAndroid instance;

    private FunplusSdkUtilsAndroid()
    {
      this.funplusSdkUtilsWrapper = new AndroidJavaClass("com.funplus.sdk.unity3d.FunplusSdkUtilsWrapper");
    }

    public static FunplusSdkUtilsAndroid Instance
    {
      get
      {
        if (FunplusSdkUtilsAndroid.instance == null)
        {
          lock (FunplusSdkUtilsAndroid.locker)
            FunplusSdkUtilsAndroid.instance = new FunplusSdkUtilsAndroid();
        }
        return FunplusSdkUtilsAndroid.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
      this.funplusSdkUtilsWrapper.CallStatic("setGameObject", new object[1]
      {
        (object) gameObjectName
      });
    }

    public override string GetTotalMemory()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getTotalMemory");
    }

    public override string GetAvailableMemory()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getAvailableMemory");
    }

    public override string GetDeviceName()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getDeviceName");
    }

    public override string GetOsName()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getOsName");
    }

    public override string GetOsVersion()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getOsVersion");
    }

    public override string GetLanguage()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getLanguage");
    }

    public override string GetCountry()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getCountry");
    }

    public override string GetDeviceType()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getDeviceType");
    }

    public override string GetScreenSize()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getScreenSize");
    }

    public override string GetScreenOrientation()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getScreenOrientation");
    }

    public override string GetScreenDensity()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getScreenDensity");
    }

    public override string GetDisplayWidth()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getDisplayWidth");
    }

    public override string GetDisplayHeight()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getDisplayHeight");
    }

    public override string GetIDFA()
    {
      return (string) null;
    }

    public override string GetIDFV()
    {
      return (string) null;
    }

    public override void GetGAID()
    {
      this.funplusSdkUtilsWrapper.CallStatic("getGAID");
    }

    public override string GetAndroidID()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getAndroidID");
    }

    public override string GetAndroidIMEI()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getAndroidIMEI");
    }

    public override void ShowAlert(string title, string message, string confirm)
    {
      this.funplusSdkUtilsWrapper.CallStatic("showAlert", (object) title, (object) message, (object) confirm);
    }

    public override string GetCurrentTimeZone()
    {
      return this.funplusSdkUtilsWrapper.CallStatic<string>("getCurrentTimeZone");
    }
  }
}
