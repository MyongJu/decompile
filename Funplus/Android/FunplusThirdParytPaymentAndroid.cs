﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Android.FunplusThirdParytPaymentAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Android
{
  public class FunplusThirdParytPaymentAndroid : BaseThirdPartyPaymentWrapper
  {
    private static readonly object locker = new object();
    private AndroidJavaClass funplusThirdPartyPaymentWrapper;
    private static FunplusThirdParytPaymentAndroid instance;

    private FunplusThirdParytPaymentAndroid()
    {
      this.funplusThirdPartyPaymentWrapper = new AndroidJavaClass("com.funplus.sdk.unity3d.FunplusThirdPartyPaymentWrapper");
    }

    public static FunplusThirdParytPaymentAndroid Instance
    {
      get
      {
        if (FunplusThirdParytPaymentAndroid.instance == null)
        {
          lock (FunplusThirdParytPaymentAndroid.locker)
            FunplusThirdParytPaymentAndroid.instance = new FunplusThirdParytPaymentAndroid();
        }
        return FunplusThirdParytPaymentAndroid.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
      this.funplusThirdPartyPaymentWrapper.CallStatic("setGameObject", new object[1]
      {
        (object) gameObjectName
      });
    }

    public override void Show(string throughCargo)
    {
      this.funplusThirdPartyPaymentWrapper.CallStatic("show", new object[1]
      {
        (object) throughCargo
      });
    }
  }
}
