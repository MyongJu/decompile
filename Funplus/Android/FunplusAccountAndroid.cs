﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Android.FunplusAccountAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Android
{
  public class FunplusAccountAndroid : BaseAccountWrapper
  {
    private static readonly object locker = new object();
    private AndroidJavaClass funplusAccountWrapper;
    private static FunplusAccountAndroid instance;

    private FunplusAccountAndroid()
    {
      this.funplusAccountWrapper = new AndroidJavaClass("com.funplus.sdk.unity3d.FunplusAccountWrapper");
    }

    public static FunplusAccountAndroid Instance
    {
      get
      {
        if (FunplusAccountAndroid.instance == null)
        {
          lock (FunplusAccountAndroid.locker)
            FunplusAccountAndroid.instance = new FunplusAccountAndroid();
        }
        return FunplusAccountAndroid.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
      this.funplusAccountWrapper.CallStatic("setGameObject", new object[1]
      {
        (object) gameObjectName
      });
    }

    public override bool IsUserLoggedIn()
    {
      return this.funplusAccountWrapper.CallStatic<bool>("isUserLoggedIn");
    }

    public override void GetAvailableAccountTypes()
    {
      this.funplusAccountWrapper.CallStatic("getAvailableAccountTypes");
    }

    public override void OpenSession()
    {
      this.funplusAccountWrapper.CallStatic("openSession");
    }

    public override void Login()
    {
      this.funplusAccountWrapper.CallStatic("login");
    }

    public override void Login(FunplusAccountType type)
    {
      this.funplusAccountWrapper.CallStatic("login", new object[1]
      {
        (object) type
      });
    }

    public override void LoginWithEmail(string email, string password)
    {
      this.funplusAccountWrapper.CallStatic("loginWithEmail", (object) email, (object) password);
    }

    public override void RegisterWithEmail(string email, string password)
    {
      this.funplusAccountWrapper.CallStatic("registerWithEmail", (object) email, (object) password);
    }

    public override void ResetPassword(string email)
    {
      this.funplusAccountWrapper.CallStatic("resetPassword", new object[1]
      {
        (object) email
      });
    }

    public override void CreateNewExpressAccount()
    {
      this.funplusAccountWrapper.CallStatic("createNewExpressAccount");
    }

    public override void Logout()
    {
      this.funplusAccountWrapper.CallStatic("logout");
    }

    public override void ShowUserCenter()
    {
      this.funplusAccountWrapper.CallStatic("showUserCenter");
    }

    public override void BindAccount()
    {
      this.funplusAccountWrapper.CallStatic("bind");
    }

    public override void BindAccount(FunplusAccountType type)
    {
      this.funplusAccountWrapper.CallStatic("bind", new object[1]
      {
        (object) type
      });
    }

    public override void UnbindAccount(FunplusAccountType type)
    {
      this.funplusAccountWrapper.CallStatic("unbind", new object[1]
      {
        (object) type
      });
    }

    public override void UnbindAccount(FunplusAccountType type, string platformIdOrEmail)
    {
      this.funplusAccountWrapper.CallStatic("unbind", (object) type, (object) platformIdOrEmail);
    }

    public override void SwitchAccount(FunplusAccountType type)
    {
      this.funplusAccountWrapper.CallStatic("switchAccount", new object[1]
      {
        (object) type
      });
    }
  }
}
