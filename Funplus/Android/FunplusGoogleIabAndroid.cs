﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Android.FunplusGoogleIabAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Android
{
  public class FunplusGoogleIabAndroid : BasePaymentWrapper
  {
    private static readonly object locker = new object();
    private AndroidJavaClass funplusGoogleiabWrapper;
    private static FunplusGoogleIabAndroid instance;

    private FunplusGoogleIabAndroid()
    {
      this.funplusGoogleiabWrapper = new AndroidJavaClass("com.funplus.sdk.unity3d.FunplusGoogleiabWrapper");
    }

    public static FunplusGoogleIabAndroid Instance
    {
      get
      {
        if (FunplusGoogleIabAndroid.instance == null)
        {
          lock (FunplusGoogleIabAndroid.locker)
            FunplusGoogleIabAndroid.instance = new FunplusGoogleIabAndroid();
        }
        return FunplusGoogleIabAndroid.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
      this.funplusGoogleiabWrapper.CallStatic("setGameObject", new object[1]
      {
        (object) gameObjectName
      });
    }

    public override bool CanMakePurchases()
    {
      return this.funplusGoogleiabWrapper.CallStatic<bool>("canMakePurchases");
    }

    public override void StartHelper()
    {
      this.funplusGoogleiabWrapper.CallStatic("startHelper");
    }

    public override void SetCurrencyWhitelist(string whitelist)
    {
    }

    public override void Buy(string productId, string throughCargo)
    {
      this.funplusGoogleiabWrapper.CallStatic("buy", (object) productId, (object) throughCargo);
    }

    public override void Buy(string productId, string serverId, string throughCargo)
    {
      this.funplusGoogleiabWrapper.CallStatic("buy", (object) productId, (object) serverId, (object) throughCargo);
    }

    public override bool GetSubsRenewing(string productId)
    {
      return this.funplusGoogleiabWrapper.CallStatic<bool>("getSubsRenewing", new object[1]
      {
        (object) productId
      });
    }

    public override bool CanCheckSubs()
    {
      return this.funplusGoogleiabWrapper.CallStatic<bool>("canCheckSubs");
    }
  }
}
