﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Android.FunplusHelpshiftAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Android
{
  public class FunplusHelpshiftAndroid : BaseHelpshiftWrapper
  {
    private static readonly object locker = new object();
    private AndroidJavaClass funplusHelpshiftWrapper;
    private static FunplusHelpshiftAndroid instance;

    private FunplusHelpshiftAndroid()
    {
      this.funplusHelpshiftWrapper = new AndroidJavaClass("com.funplus.sdk.unity3d.FunplusHelpshiftWrapper");
    }

    public static FunplusHelpshiftAndroid Instance
    {
      get
      {
        if (FunplusHelpshiftAndroid.instance == null)
        {
          lock (FunplusHelpshiftAndroid.locker)
            FunplusHelpshiftAndroid.instance = new FunplusHelpshiftAndroid();
        }
        return FunplusHelpshiftAndroid.instance;
      }
    }

    public override void ShowConversation()
    {
      this.funplusHelpshiftWrapper.CallStatic("showConversation");
    }

    public override void ShowFAQs()
    {
      this.funplusHelpshiftWrapper.CallStatic("showFAQs");
    }
  }
}
