﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Android.FunplusRumAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Android
{
  public class FunplusRumAndroid : BaseRumWrapper
  {
    private static readonly object locker = new object();
    private AndroidJavaClass funplusRumWrapper;
    private AndroidJavaClass unityPlayer;
    private AndroidJavaObject currentActivity;
    private static FunplusRumAndroid instance;

    private FunplusRumAndroid()
    {
      this.funplusRumWrapper = new AndroidJavaClass("com.funplus.sdk.unity3d.FunplusRumWrapper");
      this.unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
      this.currentActivity = this.unityPlayer.GetStatic<AndroidJavaObject>(nameof (currentActivity));
    }

    public static FunplusRumAndroid Instance
    {
      get
      {
        if (FunplusRumAndroid.instance == null)
        {
          lock (FunplusRumAndroid.locker)
            FunplusRumAndroid.instance = new FunplusRumAndroid();
        }
        return FunplusRumAndroid.instance;
      }
    }

    public override void SetRUMDefaultConfig(string appId, string appKey, string appTag, string rumVersion, string logServerUrl, int threshold)
    {
      this.funplusRumWrapper.CallStatic("setRUMDefaultConfig", (object) this.currentActivity, (object) appId, (object) appKey, (object) appTag, (object) rumVersion, (object) logServerUrl, (object) threshold);
    }

    public override void TraceServiceMonitoring(string serviceName, string httpUrl, string httpStatus, int httpLatency, int requestSize, int responseSize)
    {
      this.funplusRumWrapper.CallStatic("traceServiceMonitoring", (object) serviceName, (object) httpUrl, (object) httpStatus, (object) httpLatency, (object) requestSize, (object) responseSize);
    }

    public override void TraceServiceMonitoring(string serviceName, string httpUrl, string httpStatus, int httpLatency, int requestSize, int responseSize, string targetUserId, string requestId, string gameserverId, long requestTs, long receivedTs)
    {
      this.funplusRumWrapper.CallStatic("traceServiceMonitoring", (object) serviceName, (object) httpUrl, (object) httpStatus, (object) httpLatency, (object) requestSize, (object) responseSize, (object) targetUserId, (object) requestId, (object) gameserverId, (object) requestTs, (object) receivedTs);
    }
  }
}
