﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Android.FunplusAdjustAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Android
{
  public class FunplusAdjustAndroid : BaseAdjustWrapper
  {
    private static readonly object locker = new object();
    private AndroidJavaClass funplusSdkUtilsWrapper;
    private static FunplusAdjustAndroid instance;

    private FunplusAdjustAndroid()
    {
      this.funplusSdkUtilsWrapper = new AndroidJavaClass("com.funplus.sdk.unity3d.FunplusAdjustWrapper");
    }

    public static FunplusAdjustAndroid Instance
    {
      get
      {
        if (FunplusAdjustAndroid.instance == null)
        {
          lock (FunplusAdjustAndroid.locker)
            FunplusAdjustAndroid.instance = new FunplusAdjustAndroid();
        }
        return FunplusAdjustAndroid.instance;
      }
    }

    public override void preInit(string appToken, string firstLaunchEventToken, string trackerToken)
    {
      this.funplusSdkUtilsWrapper.CallStatic("adjustPreInit", (object) appToken, (object) firstLaunchEventToken, (object) trackerToken);
    }

    public override void trackEvent(string eventToken)
    {
      this.funplusSdkUtilsWrapper.CallStatic("adjustTrackEvent", new object[1]
      {
        (object) eventToken
      });
    }
  }
}
