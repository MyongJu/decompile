﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Android.FunplusBiAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Android
{
  public class FunplusBiAndroid : BaseBiWrapper
  {
    private static readonly object locker = new object();
    private AndroidJavaClass funplusBiWrapper;
    private static FunplusBiAndroid instance;

    private FunplusBiAndroid()
    {
      this.funplusBiWrapper = new AndroidJavaClass("com.funplus.sdk.unity3d.FunplusBiWrapper");
    }

    public static FunplusBiAndroid Instance
    {
      get
      {
        if (FunplusBiAndroid.instance == null)
        {
          lock (FunplusBiAndroid.locker)
            FunplusBiAndroid.instance = new FunplusBiAndroid();
        }
        return FunplusBiAndroid.instance;
      }
    }

    public override void TraceEvent(string eventName, string eventData)
    {
      this.funplusBiWrapper.CallStatic("traceEvent", (object) eventName, (object) eventData);
    }

    public override void TraceLog(string jsonStringLog)
    {
      this.funplusBiWrapper.CallStatic("traceLog", new object[1]
      {
        (object) jsonStringLog
      });
    }

    public override void TraceAction(string actionName, string extJsonString)
    {
      this.funplusBiWrapper.CallStatic("traceAction", (object) actionName, (object) extJsonString);
    }
  }
}
