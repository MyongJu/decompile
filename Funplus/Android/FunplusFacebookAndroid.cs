﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Android.FunplusFacebookAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Android
{
  public class FunplusFacebookAndroid : BaseFacebookWrapper
  {
    private static readonly object locker = new object();
    private AndroidJavaClass funplusFacebookWrapper;
    private static FunplusFacebookAndroid instance;

    private FunplusFacebookAndroid()
    {
      this.funplusFacebookWrapper = new AndroidJavaClass("com.funplus.sdk.unity3d.FunplusFacebookWrapper");
    }

    public static FunplusFacebookAndroid Instance
    {
      get
      {
        if (FunplusFacebookAndroid.instance == null)
        {
          lock (FunplusFacebookAndroid.locker)
            FunplusFacebookAndroid.instance = new FunplusFacebookAndroid();
        }
        return FunplusFacebookAndroid.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
      this.funplusFacebookWrapper.CallStatic("setGameObject", new object[1]
      {
        (object) gameObjectName
      });
    }

    public override bool HasFriendsPermission()
    {
      return this.funplusFacebookWrapper.CallStatic<bool>("hasFriendsPermission");
    }

    public override void AskFriendsPermission()
    {
      this.funplusFacebookWrapper.CallStatic("askFriendsPermission");
    }

    public override bool HasPublishPermission()
    {
      return this.funplusFacebookWrapper.CallStatic<bool>("hasPublishPermission");
    }

    public override void AskPublishPermission()
    {
      this.funplusFacebookWrapper.CallStatic("askPublishPermission");
    }

    public override void GetUserData()
    {
      this.funplusFacebookWrapper.CallStatic("getUserData");
    }

    public override void GetGameFriends()
    {
      this.funplusFacebookWrapper.CallStatic("getGameFriends");
    }

    public override void GetGameInvitableFriends()
    {
      this.funplusFacebookWrapper.CallStatic("getGameInvitableFriends");
    }

    public override void SendGameRequest(string message)
    {
      this.funplusFacebookWrapper.CallStatic("sendGameRequest", new object[1]
      {
        (object) message
      });
    }

    public override void SendGameRequestWithPlatformId(string platformId, string message)
    {
      this.funplusFacebookWrapper.CallStatic("sendGameRequestWithPlatformId", (object) platformId, (object) message);
    }

    public override void ShareLink(string title, string description, string url, string imageUrl)
    {
      this.funplusFacebookWrapper.CallStatic("shareLink", (object) title, (object) description, (object) url, (object) imageUrl);
    }

    public override void ShareImage(string title, string description, string url, string imagePath)
    {
      this.funplusFacebookWrapper.CallStatic("shareImage", (object) title, (object) description, (object) url, (object) imagePath);
    }

    public override void ShareOpenGraphStory(string appNameSpace, string action, string storyType, string title, string description, string url, string imageUrl)
    {
      this.funplusFacebookWrapper.CallStatic("shareOpenGraphStory", (object) appNameSpace, (object) action, (object) storyType, (object) title, (object) description, (object) url, (object) imageUrl);
    }

    public override void LogEvent(string eventName)
    {
      this.funplusFacebookWrapper.CallStatic("logEvent", new object[1]
      {
        (object) eventName
      });
    }
  }
}
