﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Android.FunplusSdkAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Android
{
  public class FunplusSdkAndroid : BaseSdkWrapper
  {
    private static readonly object locker = new object();
    private AndroidJavaClass unityPlayer;
    private AndroidJavaObject currentActivity;
    private AndroidJavaClass funplusSdkWrapper;
    private static FunplusSdkAndroid instance;

    private FunplusSdkAndroid()
    {
      this.unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
      this.currentActivity = this.unityPlayer.GetStatic<AndroidJavaObject>(nameof (currentActivity));
      this.funplusSdkWrapper = new AndroidJavaClass("com.funplus.sdk.unity3d.FunplusSdkWrapper");
      if (this.unityPlayer == null)
        Debug.LogError((object) "[funsdk] Unable to find com.unity3d.player.UnityPlayer");
      if (this.currentActivity == null)
        Debug.LogError((object) "[funsdk] Unable to find currentActivity");
      if (this.funplusSdkWrapper != null)
        return;
      Debug.LogError((object) "[funsdk] Unable to find com.funplus.sdk.unity3d.FunplusSdkWrapper");
    }

    public static FunplusSdkAndroid Instance
    {
      get
      {
        if (FunplusSdkAndroid.instance == null)
        {
          lock (FunplusSdkAndroid.locker)
            FunplusSdkAndroid.instance = new FunplusSdkAndroid();
        }
        return FunplusSdkAndroid.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
      this.funplusSdkWrapper.CallStatic("setGameObject", new object[1]
      {
        (object) gameObjectName
      });
    }

    public override bool IsSdkInstalled()
    {
      return this.funplusSdkWrapper.CallStatic<bool>("isSdkInstalled");
    }

    public override void FunplusSDKInit(string sdkConfigString, string configServerTsString)
    {
      this.funplusSdkWrapper.CallStatic("funplusSDKInit", (object) sdkConfigString, (object) configServerTsString);
    }

    public override void SetDefaultConfig(string configJsonString)
    {
      this.funplusSdkWrapper.CallStatic("setDefaultConfig", new object[1]
      {
        (object) configJsonString
      });
    }

    public override void Install(string gameId, string gameKey, string environment)
    {
      environment = !(environment == "production") ? "sandbox" : "production";
      this.funplusSdkWrapper.CallStatic("install", (object) this.currentActivity, (object) gameId, (object) gameKey, (object) environment);
    }

    public override bool IsFirstLaunch()
    {
      return this.funplusSdkWrapper.CallStatic<bool>("isFirstLaunch");
    }

    public string GetSdkVersion()
    {
      return this.funplusSdkWrapper.CallStatic<string>("getSdkVersion");
    }

    public override void SetDebug(bool isDebug)
    {
    }

    public override void SetGamePackageChannel(string packageChannel)
    {
      this.funplusSdkWrapper.CallStatic("setGamePackageChannel", new object[1]
      {
        (object) packageChannel
      });
    }

    public override void SetGameLanguage(FunplusLanguage language)
    {
      this.funplusSdkWrapper.CallStatic("setGameLanguage", new object[1]
      {
        (object) language
      });
    }

    public override void SetConfigServerEndpoint(string endpointDomain)
    {
      this.funplusSdkWrapper.CallStatic("setSDKConfigServerEndpoint", new object[1]
      {
        (object) endpointDomain
      });
    }

    public override void SetPassportServerEndpoint(string endpointDomain)
    {
      this.funplusSdkWrapper.CallStatic("setSDKPassportServerEndpoint", new object[1]
      {
        (object) endpointDomain
      });
    }

    public override void SetPaymentServerEndpoint(string endpointDomain)
    {
      this.funplusSdkWrapper.CallStatic("setSDKPaymentServerEndpoint", new object[1]
      {
        (object) endpointDomain
      });
    }

    public override void LogUserLogin(string uid)
    {
      this.funplusSdkWrapper.CallStatic("logUserLogin", new object[1]
      {
        (object) uid
      });
    }

    public override void LogNewUser(string uid)
    {
      this.funplusSdkWrapper.CallStatic("logNewUser", new object[1]
      {
        (object) uid
      });
    }

    public override void LogUserLogout()
    {
      this.funplusSdkWrapper.CallStatic("logUserLogout");
    }

    public override void LogUserInfoUpdate(string serverId, string userId, string userName, string userLevel, string userVipLevel, bool isPaidUser, string userIdCreatedTs)
    {
      this.funplusSdkWrapper.CallStatic("logUserInfoUpdate", (object) serverId, (object) userId, (object) userName, (object) userLevel, (object) userVipLevel, (object) isPaidUser, (object) userIdCreatedTs);
    }

    public override void LogPayment(string productId, string throughCargo, string purchaseData)
    {
      this.funplusSdkWrapper.CallStatic("logPayment", (object) productId, (object) throughCargo, (object) purchaseData);
    }
  }
}
