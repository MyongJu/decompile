﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusSession
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using HSMiniJSON;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Funplus
{
  public class FunplusSession
  {
    public FunplusAccountType AccountType { get; set; }

    public string Fpid { get; set; }

    public string SessionKey { get; set; }

    public string Email { get; set; }

    public string SnsPlatform { get; set; }

    public string SnsId { get; set; }

    public string SnsName { get; set; }

    public long ExpireOn { get; set; }

    public static FunplusSession FromMessage(string message)
    {
      try
      {
        Dictionary<string, object> dictionary = Json.Deserialize(message) as Dictionary<string, object>;
        FunplusSession funplusSession = new FunplusSession()
        {
          Fpid = (string) dictionary["fpid"],
          SessionKey = (string) dictionary["session_key"]
        };
        if (dictionary.ContainsKey("account_type"))
          funplusSession.AccountType = FunplusAccountTypeUtils.FromString((string) dictionary["account_type"]);
        else
          Debug.LogWarning((object) "[funsdk] Lack of the `account_type` field in session.");
        if (dictionary.ContainsKey("expire_on"))
          funplusSession.ExpireOn = (long) dictionary["expire_on"];
        else
          Debug.LogWarning((object) "[funsdk] Lack of the `expire_on` field in session.");
        if (dictionary.ContainsKey("email"))
          funplusSession.Email = (string) dictionary["email"];
        if (dictionary.ContainsKey("sns_platform"))
          funplusSession.SnsPlatform = (string) dictionary["sns_platform"];
        if (dictionary.ContainsKey("sns_id"))
          funplusSession.SnsId = (string) dictionary["sns_id"];
        if (dictionary.ContainsKey("sns_name"))
          funplusSession.SnsName = (string) dictionary["sns_name"];
        return funplusSession;
      }
      catch (Exception ex)
      {
        Debug.LogError((object) ("[funsdk] Could not create session: " + ex.Message));
        return (FunplusSession) null;
      }
    }

    public FunplusAccountType GetAccountType()
    {
      return this.AccountType;
    }

    public string GetFpid()
    {
      return this.Fpid;
    }

    public string GetSessionKey()
    {
      return this.SessionKey;
    }

    public long GetExpireOn()
    {
      return this.ExpireOn;
    }

    public string GetEmail()
    {
      return this.Email;
    }

    public string GetSnsPlatform()
    {
      return this.SnsPlatform;
    }

    public string GetSnsId()
    {
      return this.SnsId;
    }

    public string GetSnsName()
    {
      return this.SnsName;
    }

    public void Clear()
    {
      this.AccountType = FunplusAccountType.FPAccountTypeUnknown;
      this.Fpid = (string) null;
      this.Email = (string) null;
      this.SessionKey = (string) null;
      this.SnsPlatform = (string) null;
      this.SnsId = (string) null;
      this.SnsName = (string) null;
      this.ExpireOn = 0L;
    }

    public string ToJsonString()
    {
      return string.Format("{{\"account_type\": \"{0}\", \"fpid\": \"{1}\", \"email\": \"{2}\", \"session_key\": \"{3}\", \"sns_platform\": \"{4}\", \"sns_id\": \"{5}\", \"sns_name\": \"{6}\", \"expire_on\": {7}}}", (object) FunplusAccountTypeUtils.ToString(this.AccountType), (object) this.Fpid, (object) this.Email, (object) this.SessionKey, (object) this.SnsPlatform, (object) this.SnsId, (object) this.SnsName, (object) this.ExpireOn);
    }
  }
}
