﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusSocialUser
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using HSMiniJSON;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Funplus
{
  public class FunplusSocialUser
  {
    public string Uid { get; set; }

    public string Pic { get; set; }

    public string Name { get; set; }

    public string Gender { get; set; }

    public string Email { get; set; }

    public string AccessToken { get; set; }

    public static FunplusSocialUser GetUserDataFromMessage(string message)
    {
      try
      {
        Dictionary<string, object> dictionary = Json.Deserialize(message) as Dictionary<string, object>;
        return new FunplusSocialUser()
        {
          Uid = !dictionary.ContainsKey("uid") ? (string) null : (string) dictionary["uid"],
          Pic = !dictionary.ContainsKey("pic") ? (string) null : (string) dictionary["pic"],
          Name = !dictionary.ContainsKey("name") ? (string) null : (string) dictionary["name"],
          Gender = !dictionary.ContainsKey("gender") ? (string) null : (string) dictionary["gender"],
          Email = !dictionary.ContainsKey("email") ? (string) null : (string) dictionary["email"],
          AccessToken = !dictionary.ContainsKey("access_token") ? (string) null : (string) dictionary["access_token"]
        };
      }
      catch (Exception ex)
      {
        Debug.LogError((object) ("[funsdk] JSON parse error: " + ex.Message));
        return (FunplusSocialUser) null;
      }
    }

    public override string ToString()
    {
      return string.Format("[uid={0}, pic={1}, name={2}, gender={3}, email={4}]", (object) this.Uid, (object) this.Pic, (object) this.Name, (object) this.Gender, (object) this.Email);
    }

    public string GetUid()
    {
      return this.Uid;
    }

    public string GetPic()
    {
      return this.Pic;
    }

    public string GetName()
    {
      return this.Name;
    }

    public string GetGender()
    {
      return this.Gender;
    }

    public string GetEmail()
    {
      return this.Email;
    }

    public string GetAccessToken()
    {
      return this.AccessToken;
    }

    public string ToJsonString()
    {
      return string.Format("{{\"uid\": \"{0}\", \"pic\": \"{1}\", \"name\": \"{2}\", \"gender\": \"{3}\", \"email\": \"{4}\", \"access_token\": \"{5}\"}}", (object) this.Uid, (object) this.Pic, (object) this.Name, (object) this.Gender, (object) this.Email, (object) this.AccessToken);
    }
  }
}
