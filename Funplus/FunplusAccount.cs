﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusAccount
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using Funplus.Android;
using HSMiniJSON;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Funplus
{
  public class FunplusAccount : MonoBehaviour
  {
    private static FunplusAccount.IDelegate _delegate;

    public static FunplusAccount Instance { get; set; }

    public FunplusSession Session { get; set; }

    private BaseAccountWrapper Wrapper
    {
      get
      {
        return (BaseAccountWrapper) FunplusAccountAndroid.Instance;
      }
    }

    private void Awake()
    {
      FunplusAccount.Instance = this;
    }

    [Obsolete("This method is deprecated, please use FunplusAccount.Instance instead.")]
    public static FunplusAccount GetInstance()
    {
      return FunplusAccount.Instance;
    }

    public FunplusAccount SetDelegate(FunplusAccount.IDelegate sdkDelegate)
    {
      if (sdkDelegate == null)
      {
        Debug.LogError((object) "[funsdk] `sdkDelegate` must not be null.");
        return FunplusAccount.Instance;
      }
      this.SetGameObjectAndDelegate(this.gameObject.name, sdkDelegate);
      return FunplusAccount.Instance;
    }

    public void SetGameObjectAndDelegate(string gameObjectName, FunplusAccount.IDelegate accountDelegate)
    {
      if (!FunplusSdk.Instance.IsSdkInstalled())
        Debug.LogError((object) "[funsdk] Please call FunplusSdk.Instance.Install() first.");
      else if (FunplusAccount._delegate == null)
      {
        FunplusAccount._delegate = accountDelegate;
        this.Wrapper.SetGameObject(gameObjectName);
      }
      else
        Debug.LogWarning((object) "[funsdk] Delegate has already been set.");
    }

    public bool IsUserLoggedIn()
    {
      if (FunplusAccount._delegate != null)
        return this.Wrapper.IsUserLoggedIn();
      return false;
    }

    public void OpenSession()
    {
      if (FunplusAccount._delegate == null)
      {
        Debug.LogError((object) "[funsdk] Please call FunplusAccount.SetDelegate() first.");
      }
      else
      {
        Debug.Log((object) "[funsdk] Trying to open session.");
        this.Wrapper.OpenSession();
      }
    }

    public void Login()
    {
      this.Wrapper.Login();
    }

    public void Login(FunplusAccountType type)
    {
      this.Wrapper.Login(type);
    }

    public void RegisterWithEmail(string email, string password)
    {
      this.Wrapper.RegisterWithEmail(email, password);
    }

    public void ResetPassword(string email)
    {
      this.Wrapper.ResetPassword(email);
    }

    public void LoginWithEmail(string email, string password)
    {
      this.Wrapper.LoginWithEmail(email, password);
    }

    public void CreateNewExpressAccount()
    {
      this.Wrapper.CreateNewExpressAccount();
    }

    public void Logout()
    {
      this.Wrapper.Logout();
    }

    public void ShowUserCenter()
    {
      this.Wrapper.ShowUserCenter();
    }

    public void BindAccount()
    {
      this.Wrapper.BindAccount();
    }

    public void BindAccount(FunplusAccountType type)
    {
      this.Wrapper.BindAccount(type);
    }

    public void UnbindAccount(FunplusAccountType type)
    {
      this.Wrapper.UnbindAccount(type);
    }

    public void UnbindAccount(FunplusAccountType type, string platformIdOrEmail)
    {
      this.Wrapper.UnbindAccount(type, platformIdOrEmail);
    }

    public void BindWithEmail(string fpid, string email, string password)
    {
    }

    public void SwitchAccount(FunplusAccountType type)
    {
      this.Wrapper.SwitchAccount(type);
    }

    public FunplusSession GetSession()
    {
      return this.Session;
    }

    public void OnAccountGetAvailableAccountTypesSuccess(string message)
    {
      Debug.LogFormat("[funsdk] Got account types: {0}.", (object) message);
    }

    public void OnAccountGetAvailableAccountTypesError(string message)
    {
      Debug.LogErrorFormat("[funsdk] Failed to get account types: {0}.", (object) message);
    }

    public void OnAccountOpenSession(string message)
    {
      Debug.LogFormat("[funsdk] OnAccountOpenSession, message: {0}.", (object) message);
      try
      {
        bool isLoggedIn = (bool) (Json.Deserialize(message) as Dictionary<string, object>)["is_logged_in"];
        FunplusAccount._delegate.OnOpenSession(isLoggedIn);
      }
      catch (Exception ex)
      {
        FunplusAccount._delegate.OnOpenSession(false);
        Debug.LogFormat("[funsdk] Failed to open session: {0}.", (object) ex.Message);
      }
    }

    public void OnAccountLoginSuccess(string message)
    {
      Debug.LogFormat("[funsdk] Account Login Success: {0}.", (object) message);
      this.Session = FunplusSession.FromMessage(message);
      FunplusAccount._delegate.OnLoginSuccess(this.Session);
    }

    public void OnAccountLoginError(string message)
    {
      Debug.LogErrorFormat("[funsdk] Account Login Error: {0}.", (object) message);
      FunplusAccount._delegate.OnLoginError(FunplusError.FromMessage(message));
    }

    public void OnAccountResetPasswordSuccess(string message)
    {
      Debug.LogFormat("[funsdk] Account Reset password Success: {0}.", (object) message);
      FunplusAccount._delegate.OnResetPasswordSuccess(message);
    }

    public void OnAccountResetPasswordError(string message)
    {
      Debug.LogFormat("[funsdk] Account Reset Password Error: {0}.", (object) message);
      FunplusAccount._delegate.OnResetPasswordError(FunplusError.FromMessage(message));
    }

    public void OnAccountLogout(string message)
    {
      Debug.LogFormat("[funsdk] Account Logout: {0}.", (object) message);
      FunplusAccount._delegate.OnLogout();
    }

    public void OnAccountBindAccountSuccess(string message)
    {
      Debug.LogFormat("[funsdk] Bind Account Success: {0}.", (object) message);
      this.Session = FunplusSession.FromMessage(message);
      FunplusAccount._delegate.OnBindAccountSuccess(this.Session);
    }

    public void OnAccountBindAccountError(string message)
    {
      Debug.LogErrorFormat("[funsdk] Bind Account Error: {0}.", (object) message);
      FunplusAccount._delegate.OnBindAccountError(FunplusError.FromMessage(message));
    }

    public void OnAccountUnbindAccountSuccess(string message)
    {
      Debug.LogFormat("[funsdk] Unbind Account Success: {0}.", (object) message);
      this.Session = FunplusSession.FromMessage(message);
      FunplusAccount._delegate.OnUnbindAccountSuccess(this.Session);
    }

    public void OnAccountUnbindAccountError(string message)
    {
      Debug.LogErrorFormat("[funsdk] Unbind Account Error: {0}.", (object) message);
      FunplusAccount._delegate.OnUnbindAccountError(FunplusError.FromMessage(message));
    }

    public void OnAccountCloseUserCenter(string message)
    {
      Debug.LogFormat("[funsdk] On Close User Center: {0}.", (object) message);
      FunplusAccount._delegate.OnCloseUserCenter();
    }

    public void OnAccountSwitchAccountError(string message)
    {
      Debug.LogFormat("[funsdk] On Switch Account: {0}.", (object) message);
      FunplusAccount._delegate.OnSwitchAccountError(FunplusError.FromMessage(message));
    }

    public interface IDelegate
    {
      void OnOpenSession(bool isLoggedIn);

      void OnLoginSuccess(FunplusSession session);

      void OnLoginError(FunplusError error);

      void OnResetPasswordSuccess(string fpid);

      void OnResetPasswordError(FunplusError error);

      void OnLogout();

      void OnBindAccountSuccess(FunplusSession session);

      void OnBindAccountError(FunplusError error);

      void OnUnbindAccountSuccess(FunplusSession session);

      void OnUnbindAccountError(FunplusError error);

      void OnCloseUserCenter();

      void OnSwitchAccountError(FunplusError error);
    }
  }
}
