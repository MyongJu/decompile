﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusHelpshift
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using Funplus.Android;
using System;

namespace Funplus
{
  public class FunplusHelpshift
  {
    private static readonly object locker = new object();
    private static FunplusHelpshift instance;

    private BaseHelpshiftWrapper Wrapper
    {
      get
      {
        return (BaseHelpshiftWrapper) FunplusHelpshiftAndroid.Instance;
      }
    }

    [Obsolete("This method is deprecated, please use FunplusHelpshift.Instance instead.")]
    public static FunplusHelpshift GetInstance()
    {
      return FunplusHelpshift.Instance;
    }

    public static FunplusHelpshift Instance
    {
      get
      {
        if (FunplusHelpshift.instance == null)
        {
          lock (FunplusHelpshift.locker)
            FunplusHelpshift.instance = new FunplusHelpshift();
        }
        return FunplusHelpshift.instance;
      }
    }

    public void ShowConversation()
    {
      this.Wrapper.ShowConversation();
    }

    public void ShowFAQs()
    {
      this.Wrapper.ShowFAQs();
    }
  }
}
