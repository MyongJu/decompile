﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusSdk
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using Funplus.Android;
using Funplus.Internal;
using System;
using UnityEngine;

namespace Funplus
{
  public class FunplusSdk : MonoBehaviour
  {
    private static readonly string SDK_VERSION = "3.1.11";
    private static FunplusSdk.IDelegate _delegate;

    public static FunplusSdk Instance { get; set; }

    public string GameId { get; set; }

    public string GameKey { get; set; }

    public string Environment { get; set; }

    private BaseSdkWrapper Wrapper
    {
      get
      {
        return (BaseSdkWrapper) FunplusSdkAndroid.Instance;
      }
    }

    private void Awake()
    {
      UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) this.gameObject);
      FunplusSdk.Instance = this;
      this.gameObject.AddComponent<FunplusAccount>();
      this.gameObject.AddComponent<FunplusSdkUtils>();
      this.gameObject.AddComponent<FunplusPayment>();
      this.gameObject.AddComponent<FunplusFacebook>();
      this.gameObject.AddComponent<FunplusRequest>();
      this.gameObject.AddComponent<FunplusThirdPartyPayment>();
    }

    [Obsolete("This method is deprecated, please use FunplusSdk.Instance instead.")]
    public static FunplusSdk GetInstance()
    {
      return FunplusSdk.Instance;
    }

    public FunplusSdk SetDelegate(FunplusSdk.IDelegate sdkDelegate)
    {
      if (sdkDelegate == null)
      {
        Debug.LogError((object) "[funsdk] `sdkDelegate` must not be null.");
        return FunplusSdk.Instance;
      }
      this.SetGameObjectAndDelegate(this.gameObject.name, sdkDelegate);
      return FunplusSdk.Instance;
    }

    public void SetGameObjectAndDelegate(string gameObjectName, FunplusSdk.IDelegate sdkDelegate)
    {
      if (FunplusSdk._delegate == null)
      {
        FunplusSdk._delegate = sdkDelegate;
        this.Wrapper.SetGameObject(gameObjectName);
      }
      else
        Debug.LogWarning((object) "[funsdk] Delegate has already been set.");
    }

    public void FunplusSDKInit(string sdkConfigString, string configServerTsString)
    {
      this.Wrapper.FunplusSDKInit(sdkConfigString, configServerTsString);
    }

    public void SetDefaultConfig(string configJsonString)
    {
      this.Wrapper.SetDefaultConfig(configJsonString);
    }

    public void Install()
    {
      this.GameId = FunplusSettings.FunplusGameId;
      this.GameKey = FunplusSettings.FunplusGameKey;
      this.Environment = FunplusSettings.Environment;
      this.Install(this.GameId, this.GameKey, this.Environment);
    }

    public void Install(string gameId, string gameKey, string environment)
    {
      if (FunplusSdk._delegate == null)
        Debug.LogError((object) "[funsdk] Delegate not set, please call FunplusSdk.SetDelegate() first.");
      else if (string.IsNullOrEmpty(gameId))
        Debug.LogError((object) "[funsdk] `gameId` must not be empty.");
      else if (string.IsNullOrEmpty(gameKey))
        Debug.LogError((object) "[funsdk] `gameKey` must not be empty.");
      else if (string.IsNullOrEmpty(environment))
        Debug.LogError((object) "[funsdk] `environment` must not be empty.");
      else
        this.Wrapper.Install(gameId, gameKey, environment);
    }

    public bool IsSdkInstalled()
    {
      if (FunplusSdk._delegate != null)
        return this.Wrapper.IsSdkInstalled();
      return false;
    }

    public void setGamePackageChannel(string packageChannel)
    {
      this.Wrapper.SetGamePackageChannel(packageChannel);
    }

    public void setGameLanguage(FunplusLanguage language)
    {
      this.Wrapper.SetGameLanguage(language);
    }

    public void setConfigServerEndpoint(string endpointDomain)
    {
      this.Wrapper.SetConfigServerEndpoint(endpointDomain);
    }

    public void setPassportServerEndpoint(string endpointDomain)
    {
      this.Wrapper.SetPassportServerEndpoint(endpointDomain);
    }

    public void setPaymentServerEndpoint(string endpointDomain)
    {
      this.Wrapper.SetPaymentServerEndpoint(endpointDomain);
    }

    public static string GetSdkVersion()
    {
      return FunplusSdk.SDK_VERSION;
    }

    public bool IsFirstLaunch()
    {
      return this.Wrapper.IsFirstLaunch();
    }

    public void LogUserLogin(string uid)
    {
      this.Wrapper.LogUserLogin(uid);
    }

    public void LogNewUser(string uid)
    {
      this.Wrapper.LogNewUser(uid);
    }

    public void LogUserLogout()
    {
      this.Wrapper.LogUserLogout();
    }

    public void LogUserInfoUpdate(string serverId, string userId, string userName, string userLevel, string userVipLevel, bool isPaidUser, string userIdCreatedTs)
    {
      this.Wrapper.LogUserInfoUpdate(serverId, userId, userName, userLevel, userVipLevel, isPaidUser, userIdCreatedTs);
    }

    public void LogPayment(string productId, string throughCargo, string purchaseData)
    {
      if (Application.platform != RuntimePlatform.Android)
        return;
      this.Wrapper.LogPayment(productId, throughCargo, purchaseData);
    }

    public void OnFunplusSdkInstallSuccess(string message)
    {
      FunplusSdk._delegate.OnSdkInstallSuccess(message);
    }

    public void OnFunplusSdkInstallError(string message)
    {
      FunplusSdk._delegate.OnSdkInstallError(FunplusError.FromMessage(message));
    }

    public void OnFunplusSdkReceiveNotificationMessage(string message)
    {
      Debug.LogWarning((object) ("Funplus Sdk OnFunplusSdkReceiveNotificationMessage = " + message));
      FunplusSdk._delegate.OnReceiveNotificationMessage(message);
    }

    public void OnFunplusPushRegisterReady(string message)
    {
      Debug.LogWarning((object) ("Funplus Sdk OnPushRegisterReady message = " + message));
      FunplusSdk._delegate.OnRegisterPushReady(message);
    }

    public void OnFunplusSdkReceiveMarketingMessage(string message)
    {
      Debug.LogWarning((object) ("Funplus Sdk OnFunplusSdkReceiveMarketingMessage = " + message));
      FunplusSdk._delegate.OnReceiveMarketingMessage(message);
    }

    public void OnFunplusSdkReceiveDeepLinkURL(string urlString)
    {
      Debug.LogWarning((object) ("Funplus Sdk OnFunplusSdkReceiveDeepLinkURL = " + urlString));
      FunplusSdk._delegate.OnReveiveDeepLinkURL(urlString);
    }

    public interface IDelegate
    {
      void OnSdkInstallSuccess(string config);

      void OnSdkInstallError(FunplusError error);

      void OnReceiveNotificationMessage(string message);

      void OnRegisterPushReady(string message);

      void OnReceiveMarketingMessage(string message);

      void OnReveiveDeepLinkURL(string urlString);
    }
  }
}
