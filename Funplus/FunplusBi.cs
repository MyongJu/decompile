﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusBi
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using Funplus.Android;
using System;
using UnityEngine;

namespace Funplus
{
  public class FunplusBi
  {
    private static readonly object locker = new object();
    private static FunplusBi instance;

    private BaseBiWrapper Wrapper
    {
      get
      {
        return (BaseBiWrapper) FunplusBiAndroid.Instance;
      }
    }

    [Obsolete("This method is deprecated, please use FunplusBi.Instance instead.")]
    public static FunplusBi GetIntance()
    {
      return FunplusBi.Instance;
    }

    public static FunplusBi Instance
    {
      get
      {
        if (FunplusBi.instance == null)
        {
          lock (FunplusBi.locker)
            FunplusBi.instance = new FunplusBi();
        }
        return FunplusBi.instance;
      }
    }

    public void TraceEvent(string eventName, string properties)
    {
      if (string.IsNullOrEmpty(eventName))
        Debug.LogError((object) "[funsdk] `eventName` should not be empty.");
      else if (string.IsNullOrEmpty(properties))
        Debug.LogError((object) "[funsdk] `properties` should not be empty.");
      else
        this.Wrapper.TraceEvent(eventName, properties);
    }

    public void TraceLog(string jsonStringLog)
    {
      this.Wrapper.TraceLog(jsonStringLog);
    }

    public void TraceAction(string actionName, string extJsonString)
    {
      this.Wrapper.TraceAction(actionName, extJsonString);
    }
  }
}
