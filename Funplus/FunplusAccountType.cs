﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusAccountType
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Funplus
{
  public enum FunplusAccountType
  {
    FPAccountTypeUnknown = -1,
    FPAccountTypeExpress = 0,
    FPAccountTypeEmail = 1,
    FPAccountTypeFacebook = 2,
    FPAccountTypeVK = 3,
    FPAccountTypeWechat = 4,
    FPAccountTypeGooglePlus = 5,
    FPAccountTypeGameCenter = 6,
    FPAccountTypeNotSpecified = 7,
    FPAccountTypeMobile = 8,
  }
}
