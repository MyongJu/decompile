﻿// Decompiled with JetBrains decompiler
// Type: Funplus.IOS.FunplusRumIOS
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using System.Runtime.InteropServices;

namespace Funplus.IOS
{
  public class FunplusRumIOS : BaseRumWrapper
  {
    private static readonly object locker = new object();
    private static FunplusRumIOS instance;

    public static FunplusRumIOS Instance
    {
      get
      {
        if (FunplusRumIOS.instance == null)
        {
          lock (FunplusRumIOS.locker)
            FunplusRumIOS.instance = new FunplusRumIOS();
        }
        return FunplusRumIOS.instance;
      }
    }

    public override void SetRUMDefaultConfig(string appId, string appKey, string appTag, string rumVersion, string logServerUrl, int threshold)
    {
      FunplusRumIOS.com_funplus_sdk_rum_setRUMDefaultConfig(appId, appKey, appTag, rumVersion, logServerUrl, threshold);
    }

    public override void TraceServiceMonitoring(string serviceName, string httpUrl, string httpStatus, int httpLatency, int requestSize, int responseSize)
    {
      FunplusRumIOS.com_funplus_sdk_rum_traceServiceMonitoring(serviceName, httpUrl, httpStatus, httpLatency, requestSize, responseSize);
    }

    public override void TraceServiceMonitoring(string serviceName, string httpUrl, string httpStatus, int httpLatency, int requestSize, int responseSize, string targetUserId, string requestId, string gameserverId, long requestTs, long receivedTs)
    {
      FunplusRumIOS.com_funplus_sdk_rum_traceServiceMonitoring_ext(serviceName, httpUrl, httpStatus, httpLatency, requestSize, responseSize, targetUserId, requestId, gameserverId, requestTs, receivedTs);
    }

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_rum_setRUMDefaultConfig(string appId, string appKey, string appTag, string rumVersion, string logServerUrl, int threshold);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_rum_traceServiceMonitoring(string serviceName, string httpUrl, string httpStatus, int httpLatency, int requestSize, int responseSize);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_rum_traceServiceMonitoring_ext(string serviceName, string httpUrl, string httpStatus, int httpLatency, int requestSize, int responseSize, string targetUserId, string requestId, string gameserverId, long requestTs, long receivedTs);
  }
}
