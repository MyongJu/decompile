﻿// Decompiled with JetBrains decompiler
// Type: Funplus.IOS.FunplusHelpshiftIOS
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using System.Runtime.InteropServices;

namespace Funplus.IOS
{
  public class FunplusHelpshiftIOS : BaseHelpshiftWrapper
  {
    private static readonly object locker = new object();
    private static FunplusHelpshiftIOS instance;

    public static FunplusHelpshiftIOS Instance
    {
      get
      {
        if (FunplusHelpshiftIOS.instance == null)
        {
          lock (FunplusHelpshiftIOS.locker)
            FunplusHelpshiftIOS.instance = new FunplusHelpshiftIOS();
        }
        return FunplusHelpshiftIOS.instance;
      }
    }

    public override void ShowConversation()
    {
      FunplusHelpshiftIOS.com_funplus_sdk_helpshift_showConversation();
    }

    public override void ShowFAQs()
    {
      FunplusHelpshiftIOS.com_funplus_sdk_helpshift_showFAQs();
    }

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_helpshift_showConversation();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_helpshift_showFAQs();
  }
}
