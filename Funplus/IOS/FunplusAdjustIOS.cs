﻿// Decompiled with JetBrains decompiler
// Type: Funplus.IOS.FunplusAdjustIOS
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using System.Runtime.InteropServices;

namespace Funplus.IOS
{
  public class FunplusAdjustIOS : BaseAdjustWrapper
  {
    private static readonly object locker = new object();
    private static FunplusAdjustIOS instance;

    public static FunplusAdjustIOS Instance
    {
      get
      {
        if (FunplusAdjustIOS.instance == null)
        {
          lock (FunplusAdjustIOS.locker)
            FunplusAdjustIOS.instance = new FunplusAdjustIOS();
        }
        return FunplusAdjustIOS.instance;
      }
    }

    public override void preInit(string appToken, string firstLaunchEventToken, string trackerToken)
    {
    }

    public override void trackEvent(string eventToken)
    {
      FunplusAdjustIOS.com_funplus_sdk_marketing_adjust_trackEvent(eventToken);
    }

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_marketing_adjust_trackEvent(string eventToken);
  }
}
