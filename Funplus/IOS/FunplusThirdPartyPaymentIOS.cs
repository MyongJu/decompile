﻿// Decompiled with JetBrains decompiler
// Type: Funplus.IOS.FunplusThirdPartyPaymentIOS
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;

namespace Funplus.IOS
{
  public class FunplusThirdPartyPaymentIOS : BaseThirdPartyPaymentWrapper
  {
    private static readonly object locker = new object();
    private static FunplusThirdPartyPaymentIOS instance;

    public static FunplusThirdPartyPaymentIOS Instance
    {
      get
      {
        if (FunplusThirdPartyPaymentIOS.instance == null)
        {
          lock (FunplusThirdPartyPaymentIOS.locker)
            FunplusThirdPartyPaymentIOS.instance = new FunplusThirdPartyPaymentIOS();
        }
        return FunplusThirdPartyPaymentIOS.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
    }

    public override void Show(string throughCargo)
    {
    }
  }
}
