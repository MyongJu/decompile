﻿// Decompiled with JetBrains decompiler
// Type: Funplus.IOS.FunplusFacebookIOS
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using System.Runtime.InteropServices;

namespace Funplus.IOS
{
  public class FunplusFacebookIOS : BaseFacebookWrapper
  {
    private static readonly object locker = new object();
    private static FunplusFacebookIOS instance;

    public static FunplusFacebookIOS Instance
    {
      get
      {
        if (FunplusFacebookIOS.instance == null)
        {
          lock (FunplusFacebookIOS.locker)
            FunplusFacebookIOS.instance = new FunplusFacebookIOS();
        }
        return FunplusFacebookIOS.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
      FunplusFacebookIOS.com_funplus_sdk_social_facebook_setGameObject(gameObjectName);
    }

    public override bool HasFriendsPermission()
    {
      return FunplusFacebookIOS.com_funplus_sdk_social_facebook_hasFriendsPermission();
    }

    public override void AskFriendsPermission()
    {
      FunplusFacebookIOS.com_funplus_sdk_social_facebook_askFriendsPermission();
    }

    public override bool HasPublishPermission()
    {
      return FunplusFacebookIOS.com_funplus_sdk_social_facebook_hasPublishPermission();
    }

    public override void AskPublishPermission()
    {
      FunplusFacebookIOS.com_funplus_sdk_social_facebook_askPublishPermission();
    }

    public override void GetUserData()
    {
      FunplusFacebookIOS.com_funplus_sdk_social_facebook_getUserData();
    }

    public override void GetGameFriends()
    {
      FunplusFacebookIOS.com_funplus_sdk_social_facebook_getGameFriends();
    }

    public override void GetGameInvitableFriends()
    {
      FunplusFacebookIOS.com_funplus_sdk_social_facebook_getGameInvitableFriends();
    }

    public override void SendGameRequest(string message)
    {
      FunplusFacebookIOS.com_funplus_sdk_social_facebook_sendGameRequest(message);
    }

    public override void SendGameRequestWithPlatformId(string platformId, string message)
    {
      FunplusFacebookIOS.com_funplus_sdk_social_facebook_sendGameRequestWithPlatformId(platformId, message);
    }

    public override void ShareLink(string title, string description, string url, string imageUrl)
    {
      FunplusFacebookIOS.com_funplus_sdk_social_facebook_shareLink(title, description, url, imageUrl);
    }

    public override void ShareImage(string title, string description, string url, string imagePath)
    {
      FunplusFacebookIOS.com_funplus_sdk_social_facebook_shareImage(title, description, url, imagePath);
    }

    public override void ShareOpenGraphStory(string appNameSpace, string action, string storyType, string title, string description, string url, string imageUrl)
    {
      FunplusFacebookIOS.com_funplus_sdk_social_facebook_shareOpenGraphStory(appNameSpace, action, storyType, title, description, url, imageUrl);
    }

    public override void LogEvent(string eventName)
    {
      FunplusFacebookIOS.com_funplus_sdk_social_facebook_logEvent(eventName);
    }

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_social_facebook_setGameObject(string gameObjectName);

    [DllImport("__Internal")]
    private static extern bool com_funplus_sdk_social_facebook_hasFriendsPermission();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_social_facebook_askFriendsPermission();

    [DllImport("__Internal")]
    private static extern bool com_funplus_sdk_social_facebook_hasPublishPermission();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_social_facebook_askPublishPermission();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_social_facebook_getUserData();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_social_facebook_getGameFriends();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_social_facebook_getGameInvitableFriends();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_social_facebook_sendGameRequest(string message);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_social_facebook_sendGameRequestWithPlatformId(string platformId, string message);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_social_facebook_shareLink(string title, string description, string url, string imageUrl);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_social_facebook_shareImage(string title, string description, string url, string imagePath);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_social_facebook_shareOpenGraphStory(string appNameSpace, string action, string storyType, string title, string description, string url, string imageUrl);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_social_facebook_logEvent(string eventName);
  }
}
