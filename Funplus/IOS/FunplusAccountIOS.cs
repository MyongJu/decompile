﻿// Decompiled with JetBrains decompiler
// Type: Funplus.IOS.FunplusAccountIOS
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using System.Runtime.InteropServices;

namespace Funplus.IOS
{
  public class FunplusAccountIOS : BaseAccountWrapper
  {
    private static readonly object locker = new object();
    private static FunplusAccountIOS instance;

    public static FunplusAccountIOS Instance
    {
      get
      {
        if (FunplusAccountIOS.instance == null)
        {
          lock (FunplusAccountIOS.locker)
            FunplusAccountIOS.instance = new FunplusAccountIOS();
        }
        return FunplusAccountIOS.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
      FunplusAccountIOS.com_funplus_sdk_account_setGameObject(gameObjectName);
    }

    public override void GetAvailableAccountTypes()
    {
    }

    public override bool IsUserLoggedIn()
    {
      return FunplusAccountIOS.com_funplus_sdk_account_isUserLoggedIn();
    }

    public override void OpenSession()
    {
      FunplusAccountIOS.com_funplus_sdk_account_openSession();
    }

    public override void Login()
    {
      FunplusAccountIOS.com_funplus_sdk_account_login();
    }

    public override void Login(FunplusAccountType type)
    {
      FunplusAccountIOS.com_funplus_sdk_account_loginWithType((int) type);
    }

    public override void LoginWithEmail(string email, string password)
    {
      FunplusAccountIOS.com_funplus_sdk_account_loginWithEmail(email, password);
    }

    public override void RegisterWithEmail(string email, string password)
    {
      FunplusAccountIOS.com_funplus_sdk_account_registerWithEmail(email, password);
    }

    public override void ResetPassword(string email)
    {
      FunplusAccountIOS.com_funplus_sdk_account_resetPassword(email);
    }

    public override void CreateNewExpressAccount()
    {
      FunplusAccountIOS.com_fcom_funplus_sdk_account_createNewExpressAccount();
    }

    public override void Logout()
    {
      FunplusAccountIOS.com_funplus_sdk_account_logout();
    }

    public override void ShowUserCenter()
    {
      FunplusAccountIOS.com_funplus_sdk_account_showUserCenter();
    }

    public override void BindAccount()
    {
      FunplusAccountIOS.com_funplus_sdk_account_bindAccount();
    }

    public override void BindAccount(FunplusAccountType type)
    {
      FunplusAccountIOS.com_funplus_sdk_account_bindAccountWithType((int) type);
    }

    public override void UnbindAccount(FunplusAccountType type)
    {
      FunplusAccountIOS.com_funplus_sdk_account_unbindAccountWithType((int) type);
    }

    public override void UnbindAccount(FunplusAccountType type, string platformIdOrEmail)
    {
      FunplusAccountIOS.com_funplus_sdk_account_unbindAccountWithTypeAndPlatformIdOrEmail((int) type, platformIdOrEmail);
    }

    public override void SwitchAccount(FunplusAccountType type)
    {
      FunplusAccountIOS.com_funplus_sdk_account_switchAccountWithType((int) type);
    }

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_account_setGameObject(string gameObjectName);

    [DllImport("__Internal")]
    private static extern bool com_funplus_sdk_account_isUserLoggedIn();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_account_openSession();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_account_login();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_account_loginWithType(int type);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_account_loginWithEmail(string email, string password);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_account_registerWithEmail(string eamil, string password);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_account_resetPassword(string eamil);

    [DllImport("__Internal")]
    private static extern void com_fcom_funplus_sdk_account_createNewExpressAccount();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_account_logout();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_account_showUserCenter();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_account_bindAccount();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_account_bindAccountWithType(int type);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_account_unbindAccountWithType(int type);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_account_switchAccountWithType(int type);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_account_unbindAccountWithTypeAndPlatformIdOrEmail(int type, string platformIdOrEmail);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_account_getAvailableAccountTypes();
  }
}
