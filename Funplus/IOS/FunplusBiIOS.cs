﻿// Decompiled with JetBrains decompiler
// Type: Funplus.IOS.FunplusBiIOS
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using System.Runtime.InteropServices;

namespace Funplus.IOS
{
  public class FunplusBiIOS : BaseBiWrapper
  {
    private static readonly object locker = new object();
    private static FunplusBiIOS instance;

    public static FunplusBiIOS Instance
    {
      get
      {
        if (FunplusBiIOS.instance == null)
        {
          lock (FunplusBiIOS.locker)
            FunplusBiIOS.instance = new FunplusBiIOS();
        }
        return FunplusBiIOS.instance;
      }
    }

    public override void TraceEvent(string eventName, string eventData)
    {
      FunplusBiIOS.com_funplus_sdk_bi_traceEvent(eventName, eventData);
    }

    public override void TraceLog(string jsonStringLog)
    {
      FunplusBiIOS.com_funplus_sdk_bi_traceLog(jsonStringLog);
    }

    public override void TraceAction(string actionName, string extJsonString)
    {
      FunplusBiIOS.com_funplus_sdk_bi_traceAction(actionName, extJsonString);
    }

    public void LogUserLogin(string uid)
    {
      FunplusBiIOS.com_funplus_sdk_bi_logUserLogin(uid);
    }

    public void LogNewUser(string uid)
    {
      FunplusBiIOS.com_funplus_sdk_bi_logNewUser(uid);
    }

    public void LogUserLogout()
    {
      FunplusBiIOS.com_funplus_sdk_bi_logUserLogout();
    }

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_bi_traceEvent(string eventName, string eventData);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_bi_traceLog(string jsonStringLog);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_bi_traceAction(string actionName, string extJsonString);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_bi_logUserLogin(string uid);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_bi_logNewUser(string uid);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_bi_logUserLogout();
  }
}
