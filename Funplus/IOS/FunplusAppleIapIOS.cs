﻿// Decompiled with JetBrains decompiler
// Type: Funplus.IOS.FunplusAppleIapIOS
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using System.Runtime.InteropServices;

namespace Funplus.IOS
{
  public class FunplusAppleIapIOS : BasePaymentWrapper
  {
    private static readonly object locker = new object();
    private static FunplusAppleIapIOS instance;

    public static FunplusAppleIapIOS Instance
    {
      get
      {
        if (FunplusAppleIapIOS.instance == null)
        {
          lock (FunplusAppleIapIOS.locker)
            FunplusAppleIapIOS.instance = new FunplusAppleIapIOS();
        }
        return FunplusAppleIapIOS.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
      FunplusAppleIapIOS.com_funplus_sdk_payment_appleiap_setGameObject(gameObjectName);
    }

    public override bool CanMakePurchases()
    {
      return FunplusAppleIapIOS.com_funplus_sdk_payment_appleiap_canMakePurchases();
    }

    public override void StartHelper()
    {
      FunplusAppleIapIOS.com_funplus_sdk_payment_appleiap_startHelper();
    }

    public override void SetCurrencyWhitelist(string whitelist)
    {
      FunplusAppleIapIOS.com_funplus_sdk_payment_appleiap_set_currency_whitelist(whitelist);
    }

    public override void Buy(string productId, string throughCargo)
    {
      FunplusAppleIapIOS.com_funplus_sdk_payment_appleiap_buy(productId, throughCargo);
    }

    public override void Buy(string productId, string serverId, string throughCargo)
    {
      FunplusAppleIapIOS.com_funplus_sdk_payment_appleiap_buy_with_serverId(productId, serverId, throughCargo);
    }

    public override bool GetSubsRenewing(string productId)
    {
      return false;
    }

    public override bool CanCheckSubs()
    {
      return false;
    }

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_payment_appleiap_setGameObject(string gameObjectName);

    [DllImport("__Internal")]
    private static extern bool com_funplus_sdk_payment_appleiap_canMakePurchases();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_payment_appleiap_startHelper();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_payment_appleiap_set_currency_whitelist(string whitelist);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_payment_appleiap_buy(string productId, string throughCargo);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_payment_appleiap_buy_with_serverId(string productId, string serverId, string throughCargo);
  }
}
