﻿// Decompiled with JetBrains decompiler
// Type: Funplus.IOS.FunplusSdkIOS
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using System.Runtime.InteropServices;

namespace Funplus.IOS
{
  public class FunplusSdkIOS : BaseSdkWrapper
  {
    private static readonly object locker = new object();
    private static FunplusSdkIOS instance;

    public static FunplusSdkIOS Instance
    {
      get
      {
        if (FunplusSdkIOS.instance == null)
        {
          lock (FunplusSdkIOS.locker)
            FunplusSdkIOS.instance = new FunplusSdkIOS();
        }
        return FunplusSdkIOS.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
      FunplusSdkIOS.com_funplus_sdk_setGameObject(gameObjectName);
    }

    public override bool IsSdkInstalled()
    {
      return FunplusSdkIOS.com_funplus_sdk_isSdkInstalled();
    }

    public override void FunplusSDKInit(string sdkConfigString, string configServerTsString)
    {
      FunplusSdkIOS.com_funplus_sdk_init(sdkConfigString, configServerTsString);
    }

    public override void SetDefaultConfig(string configJsonString)
    {
    }

    public override void Install(string gameId, string gameKey, string environment)
    {
      bool isProduction = environment == "production";
      FunplusSdkIOS.com_funplus_sdk_setDebug(!isProduction);
      FunplusSdkIOS.com_funplus_sdk_install(gameId, gameKey, isProduction);
    }

    public override void SetDebug(bool isDebug)
    {
      FunplusSdkIOS.com_funplus_sdk_setDebug(isDebug);
    }

    public override bool IsFirstLaunch()
    {
      return FunplusSdkIOS.com_funplus_sdk_isFirstLaunch();
    }

    public string GetSdkVersion()
    {
      return FunplusSdkIOS.com_funplus_sdk_getSdkVersion();
    }

    public override void SetGamePackageChannel(string packageChannel)
    {
    }

    public override void SetGameLanguage(FunplusLanguage language)
    {
      FunplusSdkIOS.com_funplus_sdk_setGameLanguage((int) language);
    }

    public override void SetConfigServerEndpoint(string endpointDomain)
    {
      FunplusSdkIOS.com_funplus_sdk_setConfigServerEndpoint(endpointDomain);
    }

    public override void SetPassportServerEndpoint(string endpointDomain)
    {
      FunplusSdkIOS.com_funplus_sdk_setPassportServerEndpoint(endpointDomain);
    }

    public override void SetPaymentServerEndpoint(string endpointDomain)
    {
      FunplusSdkIOS.com_funplus_sdk_setPaymentServerEndpoint(endpointDomain);
    }

    public override void LogUserLogin(string uid)
    {
      FunplusSdkIOS.com_funplus_sdk_logUserLogin(uid);
    }

    public override void LogNewUser(string uid)
    {
      FunplusSdkIOS.com_funplus_sdk_logNewUser(uid);
    }

    public override void LogUserLogout()
    {
      FunplusSdkIOS.com_funplus_sdk_logUserLogout();
    }

    public override void LogUserInfoUpdate(string serverId, string userId, string userName, string userLevel, string userVipLevel, bool isPaidUser, string userIdCreatedTs)
    {
      FunplusSdkIOS.com_funplus_sdk_logUserInfoUpdate(serverId, userId, userName, userLevel, userVipLevel, isPaidUser);
    }

    public override void LogPayment(string productId, string throughCargo, string purchaseData)
    {
      FunplusSdkIOS.com_funplus_sdk_logPayment(productId, throughCargo, purchaseData);
    }

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_setGameObject(string gameObjectName);

    [DllImport("__Internal")]
    private static extern bool com_funplus_sdk_isSdkInstalled();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_install(string gameId, string gameKey, bool isProduction);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_setDebug(bool isDebug);

    [DllImport("__Internal")]
    private static extern bool com_funplus_sdk_isFirstLaunch();

    [DllImport("__Internal")]
    private static extern string com_funplus_sdk_getSdkVersion();

    [DllImport("__Internal")]
    private static extern bool com_funplus_sdk_setGameLanguage(int language);

    [DllImport("__Internal")]
    private static extern bool com_funplus_sdk_setConfigServerEndpoint(string endpointDomain);

    [DllImport("__Internal")]
    private static extern bool com_funplus_sdk_setPassportServerEndpoint(string endpointDomain);

    [DllImport("__Internal")]
    private static extern bool com_funplus_sdk_setPaymentServerEndpoint(string endpointDomain);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_logUserLogin(string uid);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_logNewUser(string uid);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_logUserLogout();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_logUserInfoUpdate(string serverId, string userId, string userName, string userLevel, string userVipLevel, bool isPaidUser);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_logPayment(string productId, string throughCargo, string purchaseData);

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_init(string sdkConfigString, string configServerTsString);
  }
}
