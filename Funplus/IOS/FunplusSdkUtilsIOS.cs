﻿// Decompiled with JetBrains decompiler
// Type: Funplus.IOS.FunplusSdkUtilsIOS
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using System.Runtime.InteropServices;

namespace Funplus.IOS
{
  public class FunplusSdkUtilsIOS : BaseSdkUtilsWrapper
  {
    private static readonly object locker = new object();
    private static FunplusSdkUtilsIOS instance;

    public static FunplusSdkUtilsIOS Instance
    {
      get
      {
        if (FunplusSdkUtilsIOS.instance == null)
        {
          lock (FunplusSdkUtilsIOS.locker)
            FunplusSdkUtilsIOS.instance = new FunplusSdkUtilsIOS();
        }
        return FunplusSdkUtilsIOS.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
    }

    public override string GetTotalMemory()
    {
      return FunplusSdkUtilsIOS.com_funplus_sdk_getTotalMemory();
    }

    public override string GetAvailableMemory()
    {
      return FunplusSdkUtilsIOS.com_funplus_sdk_getAvailableMemory();
    }

    public override string GetDeviceName()
    {
      return FunplusSdkUtilsIOS.com_funplus_sdk_getDeviceName();
    }

    public override string GetOsName()
    {
      return FunplusSdkUtilsIOS.com_funplus_sdk_getOsName();
    }

    public override string GetOsVersion()
    {
      return FunplusSdkUtilsIOS.com_funplus_sdk_getOsVersion();
    }

    public override string GetLanguage()
    {
      return FunplusSdkUtilsIOS.com_funplus_sdk_getLanguage();
    }

    public override string GetCountry()
    {
      return (string) null;
    }

    public override string GetDeviceType()
    {
      return FunplusSdkUtilsIOS.com_funplus_sdk_getDevice();
    }

    public override string GetScreenSize()
    {
      return (string) null;
    }

    public override string GetScreenOrientation()
    {
      return (string) null;
    }

    public override string GetScreenDensity()
    {
      return (string) null;
    }

    public override string GetDisplayWidth()
    {
      return (string) null;
    }

    public override string GetDisplayHeight()
    {
      return (string) null;
    }

    public override string GetIDFA()
    {
      return FunplusSdkUtilsIOS.com_funplus_sdk_getIDFA();
    }

    public override string GetIDFV()
    {
      return FunplusSdkUtilsIOS.com_funplus_sdk_getIDFV();
    }

    public override void GetGAID()
    {
    }

    public override string GetAndroidID()
    {
      return (string) null;
    }

    public override string GetAndroidIMEI()
    {
      return (string) null;
    }

    public override void ShowAlert(string title, string message, string confirm)
    {
      FunplusSdkUtilsIOS.com_funplus_sdk_showAlert(title, message, confirm);
    }

    public override string GetCurrentTimeZone()
    {
      return FunplusSdkUtilsIOS.com_funplus_sdk_getCurrentTimeZone();
    }

    [DllImport("__Internal")]
    private static extern string com_funplus_sdk_getTotalMemory();

    [DllImport("__Internal")]
    private static extern string com_funplus_sdk_getAvailableMemory();

    [DllImport("__Internal")]
    private static extern string com_funplus_sdk_getDeviceName();

    [DllImport("__Internal")]
    private static extern string com_funplus_sdk_getDevice();

    [DllImport("__Internal")]
    private static extern string com_funplus_sdk_getOsName();

    [DllImport("__Internal")]
    private static extern string com_funplus_sdk_getOsVersion();

    [DllImport("__Internal")]
    private static extern string com_funplus_sdk_getLanguage();

    [DllImport("__Internal")]
    private static extern string com_funplus_sdk_getIDFA();

    [DllImport("__Internal")]
    private static extern string com_funplus_sdk_getIDFV();

    [DllImport("__Internal")]
    private static extern void com_funplus_sdk_showAlert(string titke, string message, string confirm);

    [DllImport("__Internal")]
    private static extern string com_funplus_sdk_getCurrentTimeZone();
  }
}
