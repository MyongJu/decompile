﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Stub.FunplusAccountStub
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace Funplus.Stub
{
  public class FunplusAccountStub : BaseAccountWrapper
  {
    private static readonly object locker = new object();
    private static FunplusAccountStub.SessionState sessionState = FunplusAccountStub.SessionState.Closed;
    private static FunplusSession session = new FunplusSession();
    private static FunplusAccountStub instance;
    private string deviceId;
    private string guid;

    public static FunplusAccountStub Instance
    {
      get
      {
        if (FunplusAccountStub.instance == null)
        {
          lock (FunplusAccountStub.locker)
            FunplusAccountStub.instance = new FunplusAccountStub();
        }
        return FunplusAccountStub.instance;
      }
    }

    public string DeviceId
    {
      get
      {
        return this.deviceId;
      }
    }

    public string Guid
    {
      get
      {
        if (string.IsNullOrEmpty(this.guid))
          this.guid = FunplusAccountStub.GetMd5Hash(this.DeviceId);
        return this.guid;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
    }

    public override bool IsUserLoggedIn()
    {
      return FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Actived);
    }

    public override void GetAvailableAccountTypes()
    {
    }

    public override void OpenSession()
    {
      if (!FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Closed))
      {
        Debug.LogError((object) "[funsdk] Session has been opened, no need to open it again.");
      }
      else
      {
        FunplusAccountStub.sessionState = FunplusAccountStub.SessionState.Opened;
        FunplusAccount.Instance.OnAccountOpenSession("{\"is_logged_in\": false}");
      }
    }

    public override void Login()
    {
      if (FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Closed))
        Debug.LogError((object) "[funsdk] Session has not been opened, please open session first.");
      else if (FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Activating))
        Debug.LogError((object) "[funsdk] Another authenticating process is going on.");
      else if (FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Actived))
        Debug.LogError((object) "[funsdk] Session has already been activated.");
      else
        this.ExpressLogin();
    }

    public override void Login(FunplusAccountType type)
    {
      if (FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Closed))
        Debug.LogError((object) "[funsdk] Session has not been opened, please open session first.");
      else if (FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Activating))
        Debug.LogError((object) "[funsdk] Another authenticating process is going on.");
      else if (FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Actived))
      {
        Debug.LogError((object) "[funsdk] Session has already been activated.");
      }
      else
      {
        switch (type)
        {
          case FunplusAccountType.FPAccountTypeExpress:
            this.ExpressLogin();
            break;
        }
      }
    }

    public override void LoginWithEmail(string email, string password)
    {
    }

    public override void RegisterWithEmail(string email, string password)
    {
    }

    public override void ResetPassword(string email)
    {
    }

    public override void UnbindAccount(FunplusAccountType type)
    {
    }

    public override void UnbindAccount(FunplusAccountType type, string platformIdOrEmail)
    {
    }

    public override void CreateNewExpressAccount()
    {
    }

    public override void Logout()
    {
      if (!FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Actived))
      {
        Debug.LogError((object) "[funsdk] Session has not been activated.");
      }
      else
      {
        FunplusAccountStub.sessionState = FunplusAccountStub.SessionState.Opened;
        FunplusAccountStub.session.Clear();
        FunplusAccount.Instance.OnAccountLogout((string) null);
      }
    }

    public override void ShowUserCenter()
    {
      if (!FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Actived))
        Debug.LogError((object) "[funsdk] Please login first.");
      Debug.Log((object) "[funsdk] Calling FunplusAccountStub.ShowUserCenter().");
    }

    public override void BindAccount()
    {
      if (!FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Actived))
        Debug.LogError((object) "[funsdk] Please login first.");
      Debug.Log((object) "[funsdk] Calling FunplusAccountStub.BindAccount().");
    }

    public override void BindAccount(FunplusAccountType type)
    {
      if (!FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Actived))
        Debug.LogError((object) "[funsdk] Please login first.");
      switch (type)
      {
        case FunplusAccountType.FPAccountTypeExpress:
          this.ExpressLogin();
          break;
      }
    }

    public override void SwitchAccount(FunplusAccountType type)
    {
    }

    public void ExpressLogin()
    {
      Dictionary<string, string> formData = new Dictionary<string, string>();
      formData.Add("game_id", FunplusSdk.Instance.GameId);
      formData.Add("method", "express_signin");
      formData.Add("android_id", this.deviceId);
      formData.Add("guid", this.Guid);
      FunplusAccountStub.session.AccountType = FunplusAccountType.FPAccountTypeExpress;
      FpRequestSuccess onSuccess = new FpRequestSuccess(FunplusAccountStub.OnLoginSuccess);
      FpRequestError onError = new FpRequestError(FunplusAccountStub.OnLoginError);
      FunplusRequest.Instance.FpPost(formData, onSuccess, onError);
    }

    public void EmailLogin(string email, string password)
    {
      if (FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Closed))
        Debug.LogError((object) "[funsdk] Session has not been opened, please open session first.");
      else if (FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Activating))
        Debug.LogError((object) "[funsdk] Another authenticating process is going on.");
      else if (FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Actived))
      {
        Debug.LogError((object) "[funsdk] Session has already been activated.");
      }
      else
      {
        FunplusAccountStub.session.AccountType = FunplusAccountType.FPAccountTypeExpress;
        FunplusAccountStub.session.Email = email;
        FunplusRequest.Instance.FpPost(new Dictionary<string, string>()
        {
          {
            "game_id",
            FunplusSdk.Instance.GameId
          },
          {
            "method",
            "signin"
          },
          {
            nameof (email),
            email
          },
          {
            nameof (password),
            password
          },
          {
            "android_id",
            this.DeviceId
          },
          {
            "guid",
            this.Guid
          }
        }, new FpRequestSuccess(FunplusAccountStub.OnLoginSuccess), new FpRequestError(FunplusAccountStub.OnLoginError));
      }
    }

    public void FacebookLogin(string accessToken, string platformId)
    {
      Dictionary<string, string> formData = new Dictionary<string, string>();
      formData.Add("game_id", FunplusSdk.Instance.GameId);
      formData.Add("method", "login_with_sns");
      formData.Add("platform_id", platformId);
      formData.Add("access_token", accessToken);
      FunplusAccountStub.session.AccountType = FunplusAccountType.FPAccountTypeFacebook;
      FunplusRequest.Instance.FpPost(formData, new FpRequestSuccess(FunplusAccountStub.OnLoginSuccess), new FpRequestError(FunplusAccountStub.OnLoginError));
    }

    public void FacebookBind(string accessToken, string fpid, string platformId)
    {
      if (!FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Actived))
        Debug.LogError((object) "[funsdk] Please login first.");
      Dictionary<string, string> formData = new Dictionary<string, string>();
      formData.Add("game_id", FunplusSdk.Instance.GameId);
      formData.Add("method", "login_with_sns");
      formData.Add(nameof (fpid), fpid);
      formData.Add("platform_id", platformId);
      formData.Add("access_token", accessToken);
      FunplusAccountStub.session.AccountType = FunplusAccountType.FPAccountTypeFacebook;
      FunplusRequest.Instance.FpPost(formData, new FpRequestSuccess(FunplusAccountStub.OnLoginSuccess), new FpRequestError(FunplusAccountStub.OnLoginError));
    }

    public void Register(string email, string password)
    {
      Dictionary<string, string> formData = new Dictionary<string, string>();
      formData.Add("game_id", FunplusSdk.Instance.GameId);
      formData.Add("method", "signup");
      formData.Add(nameof (email), email);
      formData.Add(nameof (password), password);
      FunplusAccountStub.session.AccountType = FunplusAccountType.FPAccountTypeEmail;
      FunplusAccountStub.session.Email = email;
      FunplusRequest.Instance.FpPost(formData, new FpRequestSuccess(FunplusAccountStub.OnLoginSuccess), new FpRequestError(FunplusAccountStub.OnLoginError));
    }

    public void BindAccountWithEmail(string fpid, string email, string password)
    {
      if (!FunplusAccountStub.sessionState.Equals((object) FunplusAccountStub.SessionState.Actived))
        Debug.LogError((object) "[funsdk] Please login first.");
      FunplusAccountStub.session.AccountType = FunplusAccountType.FPAccountTypeEmail;
      FunplusAccountStub.session.Email = email;
      FunplusRequest.Instance.FpPost(new Dictionary<string, string>()
      {
        {
          "game_id",
          FunplusSdk.Instance.GameId
        },
        {
          "method",
          "signup"
        },
        {
          nameof (fpid),
          fpid
        },
        {
          nameof (email),
          email
        },
        {
          nameof (password),
          password
        }
      }, new FpRequestSuccess(FunplusAccountStub.OnLoginSuccess), new FpRequestError(FunplusAccountStub.OnLoginError));
    }

    private static void OnLoginSuccess(Dictionary<string, object> data)
    {
      FunplusAccountStub.sessionState = FunplusAccountStub.SessionState.Actived;
      string str1 = (string) data["fpid"];
      string str2 = (string) data["session_key"];
      long num = (DateTime.UtcNow.Ticks - new DateTime(1970, 1, 1).Ticks) / 10000000L + (long) data["session_expire_in"];
      FunplusAccountStub.session.SessionKey = str2;
      FunplusAccountStub.session.ExpireOn = num;
      if (string.IsNullOrEmpty(FunplusAccountStub.session.Fpid))
      {
        FunplusAccountStub.session.Fpid = str1;
        FunplusAccount.Instance.OnAccountLoginSuccess(FunplusAccountStub.session.ToJsonString());
      }
      else
        FunplusAccount.Instance.OnAccountBindAccountSuccess(FunplusAccountStub.session.ToJsonString());
    }

    private static void OnLoginError(FunplusError error)
    {
      if (string.IsNullOrEmpty(FunplusAccountStub.session.Fpid))
        FunplusAccount.Instance.OnAccountLoginError(error.ToJsonString());
      else
        FunplusAccount.Instance.OnAccountBindAccountError(error.ToJsonString());
    }

    private void OnFacebookInit()
    {
      FunplusAccountStub.sessionState = FunplusAccountStub.SessionState.Opened;
      FunplusAccount.Instance.OnAccountOpenSession("{\"is_logged_in\": false}");
    }

    private void OnFacebookLoginSuccess(string accessToken, string uid)
    {
      FunplusAccountStub.session.SnsPlatform = "fb";
      FunplusAccountStub.session.SnsId = uid;
      string platformId = string.Format("fb:{0}", (object) uid);
      if (string.IsNullOrEmpty(FunplusAccountStub.session.Fpid))
      {
        FunplusAccountStub.Instance.FacebookLogin(accessToken, platformId);
      }
      else
      {
        string fpid = FunplusAccountStub.session.Fpid;
        FunplusAccountStub.Instance.FacebookBind(accessToken, fpid, platformId);
      }
    }

    private void OnFacebookLoginFailed()
    {
      if (string.IsNullOrEmpty(FunplusAccountStub.session.Fpid))
        FunplusAccount.Instance.OnAccountLoginError(FunplusError.E(2101L).ToJsonString());
      else
        FunplusAccount.Instance.OnAccountBindAccountError(FunplusError.E(2106L).ToJsonString());
    }

    private static string GetMd5Hash(string s)
    {
      byte[] hash = MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(s));
      StringBuilder stringBuilder = new StringBuilder();
      for (int index = 0; index < hash.Length; ++index)
        stringBuilder.Append(hash[index].ToString("x2"));
      return stringBuilder.ToString();
    }

    private enum SessionState
    {
      Closed,
      Opened,
      Activating,
      Actived,
    }
  }
}
