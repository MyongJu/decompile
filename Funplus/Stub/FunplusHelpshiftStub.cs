﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Stub.FunplusHelpshiftStub
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Stub
{
  public class FunplusHelpshiftStub : BaseHelpshiftWrapper
  {
    private static readonly object locker = new object();
    private static FunplusHelpshiftStub instance;

    private FunplusHelpshiftStub()
    {
    }

    public static FunplusHelpshiftStub Instance
    {
      get
      {
        if (FunplusHelpshiftStub.instance == null)
        {
          lock (FunplusHelpshiftStub.locker)
            FunplusHelpshiftStub.instance = new FunplusHelpshiftStub();
        }
        return FunplusHelpshiftStub.instance;
      }
    }

    public override void ShowConversation()
    {
      Debug.Log((object) "[funsdk] Calling FunplusHelpshiftStub.ShowConversation ().");
    }

    public override void ShowFAQs()
    {
      Debug.Log((object) "[funsdk] Calling FunplusHelpshiftStub.ShowFAQs ().");
    }
  }
}
