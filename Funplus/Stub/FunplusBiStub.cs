﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Stub.FunplusBiStub
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Stub
{
  public class FunplusBiStub : BaseBiWrapper
  {
    private static readonly object locker = new object();
    private static FunplusBiStub instance;

    public static FunplusBiStub Instance
    {
      get
      {
        if (FunplusBiStub.instance == null)
        {
          lock (FunplusBiStub.locker)
            FunplusBiStub.instance = new FunplusBiStub();
        }
        return FunplusBiStub.instance;
      }
    }

    public override void TraceEvent(string eventName, string properties)
    {
      Debug.Log((object) "[funsdk] Calling FunplusBiStub.TraceEvent ().");
    }

    public override void TraceLog(string jsonStringLog)
    {
      Debug.Log((object) "[funsdk] Calling FunplusBiStub.TraceLog ().");
    }

    public override void TraceAction(string actionName, string extJsonString)
    {
      Debug.Log((object) "[funsdk] Calling FunplusBiStub.TraceAction ().");
    }
  }
}
