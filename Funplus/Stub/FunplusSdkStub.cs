﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Stub.FunplusSdkStub
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Stub
{
  public class FunplusSdkStub : BaseSdkWrapper
  {
    private static readonly object locker = new object();
    private static FunplusSdkStub instance;
    private bool installed;

    public static FunplusSdkStub Instance
    {
      get
      {
        if (FunplusSdkStub.instance == null)
        {
          lock (FunplusSdkStub.locker)
            FunplusSdkStub.instance = new FunplusSdkStub();
        }
        return FunplusSdkStub.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
    }

    public override bool IsSdkInstalled()
    {
      return this.installed;
    }

    public override void FunplusSDKInit(string sdkConfigString, string configServerTsString)
    {
    }

    public override void SetDefaultConfig(string configJsonString)
    {
    }

    public override void Install(string gameId, string gameKey, string environment)
    {
      Debug.Log((object) "[funsdk] Funplus SDK installed.");
      Debug.LogFormat("[funsdk] Game ID: {0}", (object) gameId);
      Debug.LogFormat("Environment: {0}", (object) environment);
      this.installed = true;
      FunplusSdk.Instance.OnFunplusSdkInstallSuccess((string) null);
    }

    public override bool IsFirstLaunch()
    {
      return false;
    }

    public override void SetDebug(bool isDebug)
    {
    }

    public override void SetGamePackageChannel(string packageChannel)
    {
    }

    public override void SetGameLanguage(FunplusLanguage language)
    {
    }

    public override void SetConfigServerEndpoint(string endpointDomain)
    {
    }

    public override void SetPassportServerEndpoint(string endpointDomain)
    {
    }

    public override void SetPaymentServerEndpoint(string endpointDomain)
    {
    }

    public override void LogUserLogin(string uid)
    {
      Debug.Log((object) "[funsdk] Calling FunplusSdkStub.LogUserLogin ().");
    }

    public override void LogNewUser(string uid)
    {
      Debug.Log((object) "[funsdk] Calling FunplusSdkStub.LogNewUser ().");
    }

    public override void LogUserLogout()
    {
      Debug.Log((object) "[funsdk] Calling FunplusSdkStub.LogUserLogout ().");
    }

    public override void LogUserInfoUpdate(string serverId, string userId, string userName, string userLevel, string userVipLevel, bool isPaidUser, string userIdCreatedTs)
    {
      Debug.Log((object) "[funsdk] Calling FunplusSdkStub.LogUserInfoUpdate ().");
    }

    public override void LogPayment(string productId, string throughCargo, string purchaseData)
    {
      Debug.Log((object) "[funsdk] Calling FunplusSdkStub.LogPayment ().");
    }
  }
}
