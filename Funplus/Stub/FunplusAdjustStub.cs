﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Stub.FunplusAdjustStub
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;

namespace Funplus.Stub
{
  public class FunplusAdjustStub : BaseAdjustWrapper
  {
    private static readonly object locker = new object();
    private static FunplusAdjustStub instance;

    public static FunplusAdjustStub Instance
    {
      get
      {
        if (FunplusAdjustStub.instance == null)
        {
          lock (FunplusAdjustStub.locker)
            FunplusAdjustStub.instance = new FunplusAdjustStub();
        }
        return FunplusAdjustStub.instance;
      }
    }

    public override void preInit(string appToken, string firstLaunchEventToken, string trackerToken)
    {
    }

    public override void trackEvent(string eventToken)
    {
    }
  }
}
