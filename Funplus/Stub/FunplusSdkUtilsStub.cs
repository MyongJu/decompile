﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Stub.FunplusSdkUtilsStub
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Stub
{
  public class FunplusSdkUtilsStub : BaseSdkUtilsWrapper
  {
    private static readonly object locker = new object();
    private static FunplusSdkUtilsStub instance;

    public static FunplusSdkUtilsStub Instance
    {
      get
      {
        if (FunplusSdkUtilsStub.instance == null)
        {
          lock (FunplusSdkUtilsStub.locker)
            FunplusSdkUtilsStub.instance = new FunplusSdkUtilsStub();
        }
        return FunplusSdkUtilsStub.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
    }

    public override string GetTotalMemory()
    {
      Debug.Log((object) "[funsdk] Calling FunplusSdkUtilsStub.GetTotalMemory ().");
      return "100";
    }

    public override string GetAvailableMemory()
    {
      Debug.Log((object) "[funsdk] Calling FunplusSdkUtilsStub.GetAvailableMemory ().");
      return "60";
    }

    public override string GetDeviceName()
    {
      return (string) null;
    }

    public override string GetOsName()
    {
      return (string) null;
    }

    public override string GetOsVersion()
    {
      return (string) null;
    }

    public override string GetLanguage()
    {
      return (string) null;
    }

    public override string GetCountry()
    {
      return (string) null;
    }

    public override string GetDeviceType()
    {
      return (string) null;
    }

    public override string GetScreenSize()
    {
      return (string) null;
    }

    public override string GetScreenOrientation()
    {
      return (string) null;
    }

    public override string GetScreenDensity()
    {
      return (string) null;
    }

    public override string GetDisplayWidth()
    {
      return (string) null;
    }

    public override string GetDisplayHeight()
    {
      return (string) null;
    }

    public override string GetIDFA()
    {
      return (string) null;
    }

    public override string GetIDFV()
    {
      return (string) null;
    }

    public override void GetGAID()
    {
    }

    public override string GetAndroidID()
    {
      return (string) null;
    }

    public override string GetAndroidIMEI()
    {
      return (string) null;
    }

    public override void ShowAlert(string title, string message, string confirm)
    {
    }

    public override string GetCurrentTimeZone()
    {
      return (string) null;
    }
  }
}
