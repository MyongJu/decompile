﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Stub.FunplusRumStub
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Stub
{
  public class FunplusRumStub : BaseRumWrapper
  {
    private static readonly object locker = new object();
    private static FunplusRumStub instance;

    public static FunplusRumStub Instance
    {
      get
      {
        if (FunplusRumStub.instance == null)
        {
          lock (FunplusRumStub.locker)
            FunplusRumStub.instance = new FunplusRumStub();
        }
        return FunplusRumStub.instance;
      }
    }

    public override void SetRUMDefaultConfig(string appId, string appKey, string appTag, string rumVersion, string logServerUrl, int threshold)
    {
    }

    public override void TraceServiceMonitoring(string serviceName, string httpUrl, string httpStatus, int httpLatency, int requestSize, int responseSize)
    {
      Debug.Log((object) "[funsdk] Calling FunplusRumStub.TraceServiceMonitoring ().");
    }

    public override void TraceServiceMonitoring(string serviceName, string httpUrl, string httpStatus, int httpLatency, int requestSize, int responseSize, string targetUserId, string requestId, string gameserverId, long requestTs, long receivedTs)
    {
      Debug.Log((object) "[funsdk] Calling FunplusRumStub.TraceServiceMonitoringExt ().");
    }
  }
}
