﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Stub.FunplusFacebookStub
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Stub
{
  public class FunplusFacebookStub : BaseFacebookWrapper
  {
    private static readonly object locker = new object();
    private static FunplusFacebookStub instance;

    public FunplusSocialUser User { get; set; }

    public static FunplusFacebookStub Instance
    {
      get
      {
        if (FunplusFacebookStub.instance == null)
        {
          lock (FunplusFacebookStub.locker)
            FunplusFacebookStub.instance = new FunplusFacebookStub();
        }
        return FunplusFacebookStub.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
      Debug.Log((object) "[funsdk] Calling FunplusFacebookStub.SetGameObject ().");
    }

    public override bool HasFriendsPermission()
    {
      return true;
    }

    public override void AskFriendsPermission()
    {
    }

    public override bool HasPublishPermission()
    {
      return true;
    }

    public override void AskPublishPermission()
    {
    }

    public override void GetUserData()
    {
      if (this.User != null)
        FunplusFacebook.Instance.OnFacebookGetUserDataSuccess(this.User.ToJsonString());
      else
        FunplusFacebook.Instance.OnFacebookGetUserDataError(FunplusError.E(2102L).ToJsonString());
    }

    public override void GetGameFriends()
    {
    }

    public override void GetGameInvitableFriends()
    {
    }

    public override void SendGameRequest(string message)
    {
    }

    public override void SendGameRequestWithPlatformId(string platformId, string message)
    {
    }

    public override void ShareLink(string title, string description, string url, string imageUrl)
    {
    }

    public override void ShareImage(string title, string description, string url, string imagePath)
    {
    }

    public override void ShareOpenGraphStory(string appNameSpace, string action, string storyType, string title, string description, string url, string imageUrl)
    {
    }

    public override void LogEvent(string eventName)
    {
    }
  }
}
