﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Stub.FunplusPaymentStub
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UnityEngine;

namespace Funplus.Stub
{
  public class FunplusPaymentStub : BasePaymentWrapper
  {
    private static readonly object locker = new object();
    private static FunplusPaymentStub instance;
    private bool canBuy;

    public static FunplusPaymentStub Instance
    {
      get
      {
        if (FunplusPaymentStub.instance == null)
        {
          lock (FunplusPaymentStub.locker)
            FunplusPaymentStub.instance = new FunplusPaymentStub();
        }
        return FunplusPaymentStub.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
      Debug.Log((object) "[funsdk] Calling FunplusPaymentStub.SetGameObject ().");
    }

    public override bool CanMakePurchases()
    {
      return this.canBuy;
    }

    public override void StartHelper()
    {
      this.canBuy = true;
      FunplusPayment.Instance.OnPaymentInitializeSuccess("[{\"product_id\":\"com.funplus.p1\",\"type\":\"inapp\",\"price\":990000,\"formatted_price\":\"$0.99\",\"locale_currency_symbol\":\"$\",\"locale_currency_code\":\"USD\",\"product_title\":\"test product 1 (Funplus Test App)\",\"product_description\":\"test product 1\"},{\"product_id\":\"com.funplus.p2\",\"type\":\"inapp\",\"price\":990000,\"formatted_price\":\"$0.99\",\"locale_currency_symbol\":\"$\",\"locale_currency_code\":\"USD\",\"product_title\":\"test product 2 (Funplus Test App)\",\"product_description\":\"test product 3\"},{\"product_id\":\"com.funplus.p3\",\"type\":\"inapp\",\"price\":990000,\"formatted_price\":\"$0.99\",\"locale_currency_symbol\":\"$\",\"locale_currency_code\":\"USD\",\"product_title\":\"test product 3 (Funplus Test App)\",\"product_description\":\"test product 3\"}]");
    }

    public override void SetCurrencyWhitelist(string whitelist)
    {
    }

    public override void Buy(string productId, string throughCargo)
    {
      FunplusPayment.Instance.OnPaymentPurchaseSuccess("{\"product_id\": \"" + productId + "\", \"through_cargo\": \"" + throughCargo + "\"}");
    }

    public override void Buy(string productId, string serverId, string throughCargo)
    {
    }

    public override bool GetSubsRenewing(string productId)
    {
      return false;
    }

    public override bool CanCheckSubs()
    {
      return false;
    }
  }
}
