﻿// Decompiled with JetBrains decompiler
// Type: Funplus.Stub.FunplusThirdPartyPaymentStub
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;

namespace Funplus.Stub
{
  public class FunplusThirdPartyPaymentStub : BaseThirdPartyPaymentWrapper
  {
    private static readonly object locker = new object();
    private static FunplusThirdPartyPaymentStub instance;

    public static FunplusThirdPartyPaymentStub Instance
    {
      get
      {
        if (FunplusThirdPartyPaymentStub.instance == null)
        {
          lock (FunplusThirdPartyPaymentStub.locker)
            FunplusThirdPartyPaymentStub.instance = new FunplusThirdPartyPaymentStub();
        }
        return FunplusThirdPartyPaymentStub.instance;
      }
    }

    public override void SetGameObject(string gameObjectName)
    {
    }

    public override void Show(string throughCargo)
    {
    }
  }
}
