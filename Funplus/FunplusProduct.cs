﻿// Decompiled with JetBrains decompiler
// Type: Funplus.FunplusProduct
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using HSMiniJSON;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Funplus
{
  public class FunplusProduct
  {
    public string ProductId { get; set; }

    public string Title { get; set; }

    public string Description { get; set; }

    public string PriceCurrencyCode { get; set; }

    public string FormattedPrice { get; set; }

    public long Price { get; set; }

    public static List<FunplusProduct> FromGoogleIabMessage(string message)
    {
      List<FunplusProduct> funplusProductList = new List<FunplusProduct>();
      try
      {
        using (List<object>.Enumerator enumerator = (Json.Deserialize(message) as List<object>).GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Dictionary<string, object> current = enumerator.Current as Dictionary<string, object>;
            funplusProductList.Add(new FunplusProduct()
            {
              ProductId = (string) current["productId"],
              Title = (string) current["title"],
              Description = (string) current["description"],
              PriceCurrencyCode = (string) current["price_currency_code"],
              FormattedPrice = (string) current["price"],
              Price = (long) current["price_amount_micros"]
            });
          }
        }
      }
      catch (Exception ex)
      {
        Debug.LogError((object) ("[funsdk] Failed to parse products info: " + ex.Message));
      }
      return funplusProductList;
    }

    public static List<FunplusProduct> FromAppleIapMessage(string message)
    {
      List<FunplusProduct> funplusProductList = new List<FunplusProduct>();
      try
      {
        using (List<object>.Enumerator enumerator = (Json.Deserialize(message) as List<object>).GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Dictionary<string, object> current = enumerator.Current as Dictionary<string, object>;
            funplusProductList.Add(new FunplusProduct()
            {
              ProductId = (string) current["product_id"],
              Title = (string) current["product_title"],
              Description = (string) current["product_description"],
              PriceCurrencyCode = (string) current["locale_currency_code"],
              FormattedPrice = (string) current["formatted_price"],
              Price = (long) current["price"]
            });
          }
        }
      }
      catch (Exception ex)
      {
        Debug.LogError((object) ("[funsdk] Failed to parse products info: " + ex.Message));
      }
      return funplusProductList;
    }

    public string GetProductId()
    {
      return this.ProductId;
    }

    public string GetTitle()
    {
      return this.Title;
    }

    public string GetDescription()
    {
      return this.Description;
    }

    public string GetPriceCurrencyCode()
    {
      return this.PriceCurrencyCode;
    }

    public string GetFormattedPrice()
    {
      return this.FormattedPrice;
    }

    public long GetPrice()
    {
      return this.Price;
    }
  }
}
