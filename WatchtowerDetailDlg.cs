﻿// Decompiled with JetBrains decompiler
// Type: WatchtowerDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class WatchtowerDetailDlg : UI.Dialog
{
  private List<GameObject> pool = new List<GameObject>();
  public float gapX = 100f;
  private ScoutReportContent src;
  private string troopType;
  public UILabel title;
  public UIScrollView sv;
  public UISprite underline;
  public UILabel tarname;
  public UILabel tarpos;
  public UILabel tarpower;
  public UITexture taricon;
  public GameObject resGroup;
  public ItemInfo resreward;
  public GameObject troopdetailGroup;
  public GameObject troopdetailContent;
  public GameObject boostGroup;
  public GameObject boostContent;
  public GameObject playerinfo;
  public UILabel upgradeinfo;

  private void Init()
  {
    if (this.src.user_name == null)
    {
      this.upgradeinfo.gameObject.SetActive(true);
      this.playerinfo.SetActive(false);
      this.resGroup.SetActive(false);
      this.troopdetailGroup.SetActive(false);
    }
    else
    {
      this.upgradeinfo.gameObject.SetActive(false);
      this.playerinfo.SetActive(true);
      this.resGroup.SetActive(true);
      this.troopdetailGroup.SetActive(true);
      List<ItemInfo.Data> res = new List<ItemInfo.Data>();
      this.src.SummarizeResourceReward(ref res);
      this.title.text = this.troopType;
      this.tarname.text = (this.src.acronym == null ? (string) null : "(" + this.src.acronym + ")") + this.src.user_name;
      this.tarpos.text = MapUtils.GetCoordinateString(this.src.k, this.src.x, this.src.y);
      this.tarpower.text = this.src.power;
      CustomIconLoader.Instance.requestCustomIcon(this.taricon, UserData.GetPortraitIconPath(int.Parse(this.src.portrait)), this.src.icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.taricon, this.src.lord_title, 1);
      this.underline.width = this.tarpos.width;
      if (res.Count == 0)
      {
        this.resGroup.SetActive(false);
      }
      else
      {
        this.resGroup.SetActive(true);
        List<Vector3> tarpos = new List<Vector3>(res.Count);
        Utils.ArrangePosition(this.resreward.transform.localPosition, res.Count, this.gapX, 0.0f, ref tarpos);
        for (int index = 0; index < tarpos.Count; ++index)
        {
          GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.resreward.gameObject, this.resreward.transform.parent);
          this.pool.Add(gameObject);
          gameObject.transform.localPosition = tarpos[index];
          gameObject.GetComponent<ItemInfo>().SeedData(res[index]);
        }
      }
      if (this.src.troops == null)
      {
        this.troopdetailGroup.SetActive(false);
        this.boostGroup.SetActive(false);
      }
      else
      {
        DefendTroopDetail defendTroopDetail = (DefendTroopDetail) null;
        if (this.src.troops.TryGetValue("march", out defendTroopDetail))
        {
          this.troopdetailGroup.SetActive(true);
          DefendInfo.Data did = new DefendInfo.Data();
          defendTroopDetail.SummarizeDefendTroop(ref did);
          DefendInfo component = this.troopdetailGroup.GetComponent<DefendInfo>();
          component.SeedData(ScriptLocalization.Get("scout_report_section_uppercase_troops", true), did);
          component.toggleDefendInfo += new System.Action(this.ArrangeLayout);
        }
        else
          this.troopdetailGroup.SetActive(false);
        if (this.src.battle_boost != null)
        {
          this.boostGroup.SetActive(true);
          DefendInfo.Data did = new DefendInfo.Data();
          this.src.SummarizeDefendBoost(ref did);
          DefendInfo component = this.boostGroup.GetComponent<DefendInfo>();
          component.SeedData(ScriptLocalization.Get("scout_report_section_uppercase_boostdetails", true), did);
          component.toggleDefendInfo += new System.Action(this.ArrangeLayout);
        }
        else
          this.boostGroup.SetActive(false);
        this.ArrangeLayout();
        this.HideTemplate(true);
      }
    }
  }

  public void OnBackBtnPressed()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void GotoTarget()
  {
    if (!MapUtils.CanGotoTarget(int.Parse(this.src.k)))
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    }
    else
    {
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
      {
        UIManager.inst.cityCamera.gameObject.SetActive(false);
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
      }
      PVPMap.PendingGotoRequest = new Coordinate(int.Parse(this.src.k), int.Parse(this.src.x), int.Parse(this.src.y));
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }

  private void ArrangeLayout()
  {
    if (this.troopdetailGroup.activeSelf && !this.resGroup.activeSelf)
      this.troopdetailGroup.transform.localPosition = this.resGroup.transform.localPosition;
    if (!this.boostGroup.activeSelf)
      return;
    float num = this.troopdetailGroup.transform.localPosition.y - NGUIMath.CalculateRelativeWidgetBounds(this.troopdetailGroup.transform).size.y;
    Vector3 localPosition = this.boostGroup.transform.localPosition;
    localPosition.y = num;
    this.boostGroup.transform.localPosition = localPosition;
  }

  private void Reset()
  {
    this.sv.ResetPosition();
    this.HideTemplate(false);
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        PrefabManagerEx.Instance.Destroy(current);
      }
    }
    this.pool.Clear();
    this.troopdetailGroup.GetComponent<DefendInfo>().Reset();
    this.boostGroup.GetComponent<DefendInfo>().Reset();
    this.resGroup.SetActive(false);
    this.troopdetailGroup.SetActive(false);
    this.boostGroup.SetActive(false);
  }

  private void HideTemplate(bool hide)
  {
    this.resreward.gameObject.SetActive(!hide);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    WatchtowerDetailDlg.Parameter parameter = orgParam as WatchtowerDetailDlg.Parameter;
    this.src = JsonReader.Deserialize<ScoutReportContent>(Utils.Object2Json((object) parameter.hashtable));
    this.troopType = parameter.type;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public Hashtable hashtable;
    public string type;
  }
}
