﻿// Decompiled with JetBrains decompiler
// Type: LzmaAlone.Properties.Resources
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.ComponentModel;
using System.Globalization;
using System.Resources;

namespace LzmaAlone.Properties
{
  internal class Resources
  {
    private static ResourceManager _resMgr;
    private static CultureInfo _resCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static ResourceManager ResourceManager
    {
      get
      {
        if (LzmaAlone.Properties.Resources._resMgr == null)
          LzmaAlone.Properties.Resources._resMgr = new ResourceManager(nameof (Resources), typeof (LzmaAlone.Properties.Resources).Assembly);
        return LzmaAlone.Properties.Resources._resMgr;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static CultureInfo Culture
    {
      get
      {
        return LzmaAlone.Properties.Resources._resCulture;
      }
      set
      {
        LzmaAlone.Properties.Resources._resCulture = value;
      }
    }
  }
}
