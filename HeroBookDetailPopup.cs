﻿// Decompiled with JetBrains decompiler
// Type: HeroBookDetailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HeroBookDetailPopup : Popup
{
  private List<HeroBookBenefitItem> _AllBenefitLabel = new List<HeroBookBenefitItem>();
  [SerializeField]
  private Transform _RootHeroCard;
  [SerializeField]
  private UILabel _LabelCareer;
  [SerializeField]
  private UILabel _LabelScore;
  [SerializeField]
  private UILabel _LabelLevel;
  [SerializeField]
  private ParliamentHeroStar _ParliamentHeroStar;
  [SerializeField]
  private UIScrollView _ScrollView;
  [SerializeField]
  private UITable _TableDetail;
  [SerializeField]
  private UITable _TableHeroBaseBenefit;
  [SerializeField]
  private UITable _TableHeroStarBenefit;
  [SerializeField]
  private UITable _TableHeroSuitBenefit;
  [SerializeField]
  private UILabel _LabelSuitName;
  [SerializeField]
  private GameObject _RootHeroSuitBenefit;
  [SerializeField]
  private GameObject _RootHeroSuitBenefitBg;
  [SerializeField]
  private HeroBookBenefitItem _HeroBookBenefitItemTemplate;
  private ParliamentHeroCard _ParliamentHeroCard;
  private HeroBookDetailPopup.Parameter _parameter;
  private bool _LayoutDirty;

  public override void OnShow(UIControler.UIParameter orgParam = null)
  {
    base.OnShow(orgParam);
    this._parameter = orgParam as HeroBookDetailPopup.Parameter;
    this._HeroBookBenefitItemTemplate.gameObject.SetActive(false);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam = null)
  {
    base.OnHide(orgParam);
  }

  protected void DestoryAllBenefitItem()
  {
    using (List<HeroBookBenefitItem>.Enumerator enumerator = this._AllBenefitLabel.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HeroBookBenefitItem current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
        {
          current.transform.SetParent((Transform) null);
          UnityEngine.Object.DestroyObject((UnityEngine.Object) current.gameObject);
        }
      }
    }
    this._AllBenefitLabel.Clear();
  }

  protected HeroBookBenefitItem CreateBenefitItem(Transform parent, string condition, int benefitId, float value)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._HeroBookBenefitItemTemplate.gameObject);
    gameObject.transform.SetParent(parent);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    HeroBookBenefitItem component = gameObject.GetComponent<HeroBookBenefitItem>();
    this._AllBenefitLabel.Add(component);
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitId];
    string str1 = dbProperty == null ? "unknow" : dbProperty.Name;
    string str2 = dbProperty == null ? value.ToString() : dbProperty.ConvertToDisplayString((double) value, true, true);
    if (string.IsNullOrEmpty(condition))
      component.SetData(string.Format("{0} {1}", (object) str1, (object) str2));
    else
      component.SetData(string.Format("{0} {1} {2}", (object) condition, (object) str1, (object) str2));
    return component;
  }

  protected ParliamentHeroCard CreateParliamentHeroCard()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/UI/Common/ParliamentHeroCard", (System.Type) null)) as GameObject;
    gameObject.transform.SetParent(this._RootHeroCard);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    return gameObject.GetComponent<ParliamentHeroCard>();
  }

  protected void UpdateUI()
  {
    if (!(bool) ((UnityEngine.Object) this._ParliamentHeroCard))
      this._ParliamentHeroCard = this.CreateParliamentHeroCard();
    LegendCardData legendCardData = (LegendCardData) null;
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this._parameter.HeroTemplateId);
    if (parliamentHeroInfo == null)
    {
      D.error((object) string.Format("cannot find parliament hero info: {0}", (object) this._parameter.HeroTemplateId));
    }
    else
    {
      ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(parliamentHeroInfo.quality.ToString());
      if (parliamentHeroQualityInfo == null)
      {
        D.error((object) string.Format("cannot find parliament hero quality info: {0}", (object) parliamentHeroInfo.quality));
      }
      else
      {
        ParliamentInfo parliamentInfo = ConfigManager.inst.DB_Parliament.Get(parliamentHeroInfo.parliamentPosition);
        if (parliamentHeroQualityInfo == null)
        {
          D.error((object) string.Format("cannot find parliament info: {0}", (object) parliamentHeroInfo.parliamentPosition));
        }
        else
        {
          if (legendCardData != null)
            this._ParliamentHeroCard.SetData(legendCardData);
          else
            this._ParliamentHeroCard.SetData(parliamentHeroInfo);
          this._ParliamentHeroCard.SetLevelVisible(false);
          this._ParliamentHeroCard.SetStarVisible(false);
          if (parliamentInfo != null)
            this._LabelCareer.text = parliamentInfo.Name;
          this._LabelScore.text = ScriptLocalization.GetWithPara("hero_score_level_num", new Dictionary<string, string>()
          {
            {
              "0",
              parliamentHeroInfo.InitScore.ToString()
            }
          }, true);
          this._LabelLevel.text = ScriptLocalization.GetWithPara("hero_xp_level_num", new Dictionary<string, string>()
          {
            {
              "0",
              (legendCardData != null ? legendCardData.Level : 1).ToString()
            },
            {
              "1",
              parliamentHeroQualityInfo.maxLevel.ToString()
            }
          }, true);
          this._ParliamentHeroStar.SetData(parliamentHeroInfo, legendCardData, true);
          this.DestoryAllBenefitItem();
          this.UpdateHeroBaseBenefit(legendCardData, parliamentHeroInfo);
          this.UpdateHeroStarBenefit(legendCardData, parliamentHeroInfo);
          this.UpdateHeroSuitBenefit(legendCardData, parliamentHeroInfo);
          this._TableDetail.Reposition();
          this._ScrollView.ResetPosition();
          this._LayoutDirty = true;
        }
      }
    }
  }

  protected void UpdateHeroBaseBenefit(LegendCardData legendCardData, ParliamentHeroInfo parliamentHeroInfo)
  {
    int level = legendCardData != null ? legendCardData.Level : 1;
    this.CreateBenefitItem(this._TableHeroBaseBenefit.transform, string.Empty, parliamentHeroInfo.levelBenefit, parliamentHeroInfo.GetLevelBenefitByLevel(level)).SetLineEnabled(false);
    this._TableHeroBaseBenefit.Reposition();
  }

  protected void UpdateHeroStarBenefit(LegendCardData legendCardData, ParliamentHeroInfo parliamentHeroInfo)
  {
    HeroBookBenefitItem heroBookBenefitItem = (HeroBookBenefitItem) null;
    for (int index = 0; index < parliamentHeroInfo.AllStarBenefit.Length; ++index)
    {
      Dictionary<string, string> para = new Dictionary<string, string>()
      {
        {
          "0",
          parliamentHeroInfo.AllStarRequirement[index].ToString()
        }
      };
      if (parliamentHeroInfo.AllStarBenefit[index] != 0)
        heroBookBenefitItem = this.CreateBenefitItem(this._TableHeroStarBenefit.transform, ScriptLocalization.GetWithPara("hero_star_level_num", para, true), parliamentHeroInfo.AllStarBenefit[index], parliamentHeroInfo.AllStarBenefitValue[index]);
    }
    this._TableHeroStarBenefit.Reposition();
    if (!((UnityEngine.Object) heroBookBenefitItem != (UnityEngine.Object) null))
      return;
    heroBookBenefitItem.SetLineEnabled(false);
  }

  protected void UpdateHeroSuitBenefit(LegendCardData legendCardData, ParliamentHeroInfo parliamentHeroInfo)
  {
    ParliamentSuitGroupInfo parliamentSuitGroupInfo = ConfigManager.inst.DB_ParliamentSuitGroup.Get(parliamentHeroInfo.heroSuitGroup);
    if (parliamentSuitGroupInfo != null)
    {
      this._RootHeroSuitBenefit.SetActive(true);
      this._RootHeroSuitBenefitBg.SetActive(true);
      this._LabelSuitName.text = parliamentSuitGroupInfo.Name;
      HeroBookBenefitItem heroBookBenefitItem = (HeroBookBenefitItem) null;
      for (int index = 0; index < parliamentSuitGroupInfo.AllSuitBenefit.Length; ++index)
      {
        if (parliamentSuitGroupInfo.AllSuitBenefit[index] != 0)
          heroBookBenefitItem = this.CreateBenefitItem(this._TableHeroSuitBenefit.transform, ScriptLocalization.GetWithPara("hero_appoint_num_group_benefits", new Dictionary<string, string>()
          {
            {
              "0",
              parliamentSuitGroupInfo.AllSuitRequirement[index].ToString()
            },
            {
              "1",
              parliamentSuitGroupInfo.Name
            }
          }, true), parliamentSuitGroupInfo.AllSuitBenefit[index], parliamentSuitGroupInfo.AllSuitBenefitValue[index]);
      }
      this._TableHeroSuitBenefit.Reposition();
      if (!((UnityEngine.Object) heroBookBenefitItem != (UnityEngine.Object) null))
        return;
      heroBookBenefitItem.SetLineEnabled(false);
    }
    else
    {
      this._RootHeroSuitBenefit.SetActive(false);
      this._RootHeroSuitBenefitBg.SetActive(false);
    }
  }

  private void LateUpdate()
  {
    if (!this._LayoutDirty)
      return;
    this._LayoutDirty = false;
    this._TableHeroBaseBenefit.Reposition();
    this._TableHeroStarBenefit.Reposition();
    this._TableHeroSuitBenefit.Reposition();
    this._TableDetail.Reposition();
  }

  public void OnButtonCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public long UserId;
    public int HeroTemplateId;
  }
}
