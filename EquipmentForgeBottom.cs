﻿// Decompiled with JetBrains decompiler
// Type: EquipmentForgeBottom
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class EquipmentForgeBottom : MonoBehaviour
{
  private Dictionary<string, int> m_lackitem = new Dictionary<string, int>(1);
  private Dictionary<string, long> m_lackRes = new Dictionary<string, long>(4);
  private Hashtable requireHT = new Hashtable();
  public GameObject forging;
  public GameObject normal;
  public GameObject hasEquipment;
  public TimerHUDUIItem timerHUDUIItem;
  public UIButton instantBtn;
  public UIButton normalBtn;
  public UILabel timeCost;
  public UILabel goldSpeedupLabel;
  public UILabel instantGoldLabel;
  public UILabel constructingLabel;
  public UILabel desLabel;
  public UITexture equipemntTextue;
  private int scrollID;
  private ConfigEquipmentScrollInfo scrollInfo;
  private int instantGold;
  private int normalGold;
  private BagType bagType;

  public void Dispose()
  {
    this.ClearEvent();
    BuilderFactory.Instance.Release((UIWidget) this.equipemntTextue);
  }

  private void ClearEvent()
  {
    JobManager.Instance.OnJobCreated -= new System.Action<long>(this.OnJobCreate);
    JobManager.Instance.OnJobRemove -= new System.Action<long>(this.OnJobRemove);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
  }

  private TimerType GetTimerType()
  {
    if (this.bagType == BagType.Hero)
      return TimerType.TIMER_FORGE;
    return this.bagType == BagType.DragonKnight ? TimerType.TIMER_DK_FORGE : TimerType.TIMER_NONE;
  }

  private JobEvent GetJobEvent()
  {
    if (this.bagType == BagType.Hero)
      return JobEvent.JOB_FORGE;
    return this.bagType == BagType.DragonKnight ? JobEvent.JOB_DK_FORGE : JobEvent.JOB_UNKNOWN;
  }

  public void Init(BagType bagType, int scrollID)
  {
    this.ClearEvent();
    this.scrollInfo = (ConfigEquipmentScrollInfo) null;
    this.bagType = bagType;
    this.scrollID = scrollID;
    JobHandle singleJobByClass = JobManager.Instance.GetSingleJobByClass(this.GetJobEvent());
    this.constructingLabel.gameObject.SetActive(false);
    if (EquipmentManager.Instance.IsForgeConstructing(this.bagType))
    {
      this.forging.SetActive(false);
      this.normal.SetActive(false);
      this.hasEquipment.SetActive(false);
      this.constructingLabel.gameObject.SetActive(true);
      this.constructingLabel.text = Utils.XLAT("forge_building_upgrade_tip");
    }
    else if (singleJobByClass != null)
    {
      this.forging.SetActive(true);
      this.normal.SetActive(false);
      this.hasEquipment.SetActive(false);
      double secsRemaining = singleJobByClass != null ? (double) singleJobByClass.LeftTime() : 0.0;
      double totalSeconds = singleJobByClass != null ? (double) singleJobByClass.Duration() : 0.0;
      double num = 0.0;
      this.timerHUDUIItem.mJobID = singleJobByClass.GetJobID();
      this.timerHUDUIItem.mFreeTimeSecs = num;
      this.timerHUDUIItem.SetDetails("time_bar_forging", string.Empty, this.GetTimerType(), secsRemaining, totalSeconds, 1, (TimerHUDUIItem.UpdateTimerHUDUIItem) null, (TimerHUDUIItem.UpdateTimerHUDUIItem) null, false);
      this.timerHUDUIItem.speedUpBt.onClick.Clear();
      this.timerHUDUIItem.speedUpBt.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnTimerSpeedUpBtnPressed)));
      Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
      this.OnSecondHandler(0);
      int internalID = int.Parse((singleJobByClass.Data as Hashtable)[(object) "item_property_id"].ToString());
      ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(internalID);
      this.desLabel.text = string.Format(Utils.XLAT("forge_in_process_notice"), (object) Utils.GetColoredString(data.LocName, (Color32) EquipmentConst.QULITY_COLOR_MAP[data.quality]));
      BuilderFactory.Instance.Build((UIWidget) this.equipemntTextue, data.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    }
    else
    {
      this.forging.SetActive(false);
      if (scrollID != 0)
        this.scrollInfo = ConfigManager.inst.GetEquipmentScroll(bagType).GetData(scrollID);
      if (this.scrollInfo == null)
      {
        this.hasEquipment.SetActive(false);
        this.normal.SetActive(false);
      }
      else
      {
        this.hasEquipment.SetActive(false);
        this.normal.SetActive(true);
        this.normalBtn.onClick.Clear();
        this.instantBtn.onClick.Clear();
        this.normalBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnNormalBtnClick)));
        this.instantBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnInstBuildPressed)));
        this.ConfigBtns();
      }
    }
    JobManager.Instance.OnJobCreated += new System.Action<long>(this.OnJobCreate);
    JobManager.Instance.OnJobRemove += new System.Action<long>(this.OnJobRemove);
  }

  private void OnJobRemove(long jobId)
  {
    JobHandle job = JobManager.Instance.GetJob(jobId);
    if (job.GetJobEvent() == this.GetJobEvent())
    {
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
    {
      if (job.GetJobEvent() != JobEvent.JOB_BUILDING_CONSTRUCTION)
        return;
      this.Init(this.bagType, this.scrollID);
    }
  }

  private void ConfigBtns()
  {
    this.requireHT.Clear();
    Hashtable hashtable1 = new Hashtable();
    int num1 = Mathf.CeilToInt(ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) ConfigManager.inst.GetEquipmentProperty(this.bagType).GetDataByEquipIDAndEnhanceLevel(this.scrollInfo.equipmenID, 0).steelValue, "calc_crafting_steel_cost"));
    hashtable1[(object) ItemBag.ItemType.steel] = (object) num1;
    Hashtable hashtable2 = new Hashtable();
    List<Materials.MaterialElement> materials = this.scrollInfo.Materials.GetMaterials();
    for (int index = 0; index < materials.Count; ++index)
    {
      ShopStaticInfo byItemInternalId = ConfigManager.inst.DB_Shop.GetShopInfoByItemInternalId(materials[index].internalID);
      if (byItemInternalId != null)
        hashtable2[(object) byItemInternalId.internalId.ToString()] = (object) materials[index].amount;
    }
    float finalData = (float) ConfigManager.inst.DB_BenefitCalc.GetFinalData(this.scrollInfo.forgeDuration, "calc_crafting_time");
    this.requireHT[(object) "time"] = (object) finalData;
    this.requireHT[(object) "resources"] = (object) hashtable1;
    this.requireHT[(object) "items"] = (object) hashtable2;
    this.instantGold = ItemBag.CalculateCost(this.requireHT);
    Utils.SetPriceToLabel(this.instantGoldLabel, this.instantGold);
    this.requireHT.Remove((object) "time");
    this.normalGold = ItemBag.CalculateCost(this.requireHT);
    this.m_lackRes = ItemBag.CalculateLackResources(this.requireHT);
    this.m_lackitem = ItemBag.CalculateLackItems(this.requireHT);
    this.timeCost.text = Utils.FormatTime((int) finalData, false, false, true);
    UIButton instantBtn = this.instantBtn;
    bool flag = ItemBag.Instance.GetItemCount(this.scrollInfo.itemID) > 0;
    this.normalBtn.isEnabled = flag;
    int num2 = flag ? 1 : 0;
    instantBtn.isEnabled = num2 != 0;
    this.instantBtn.gameObject.SetActive(ItemBag.Instance.GetItemCount(this.scrollInfo.itemID) > 0);
    this.normalBtn.gameObject.SetActive(ItemBag.Instance.GetItemCount(this.scrollInfo.itemID) > 0);
  }

  private void OnSecondHandler(int timeStamp)
  {
    this.timerHUDUIItem.OnUpdate(timeStamp);
    JobHandle singleJobByClass = JobManager.Instance.GetSingleJobByClass(this.GetJobEvent());
    int num = ItemBag.CalculateCost(singleJobByClass != null ? singleJobByClass.LeftTime() : 0, 0);
    if (num < 0)
      num = 0;
    this.goldSpeedupLabel.text = Utils.FormatThousands(num.ToString());
    if (GameEngine.Instance.PlayerData.hostPlayer.Currency >= num)
      this.goldSpeedupLabel.color = Color.yellow;
    else
      this.goldSpeedupLabel.color = Color.red;
  }

  private void OnNormalBtnClick()
  {
    if (this.m_lackRes.Count > 0 || this.m_lackitem.Count > 0)
      UIManager.inst.OpenPopup("BuildingResPopUp", (Popup.PopupParameter) new BuildingResPopUp.Parameter()
      {
        lackItem = this.m_lackitem,
        lackRes = this.m_lackRes,
        requireRes = this.requireHT,
        action = Utils.XLAT("id_uppercase_forge"),
        gold = this.normalGold,
        buyResourCallBack = new System.Action<int>(this.SendForgeRequest),
        title = ScriptLocalization.Get("id_uppercase_not_enough_resources", true),
        content = ScriptLocalization.Get("forge_not_enough_resources_description", true)
      });
    else
      this.SendForgeRequest(0);
  }

  private void SendForgeRequest(int result = 0)
  {
    if (result > GameEngine.Instance.PlayerData.hostPlayer.Currency)
      Utils.ShowNotEnoughGoldTip();
    else
      EquipmentManager.Instance.ForgeEquipment(this.bagType, this.scrollID, false, result, (System.Action<bool, object>) ((arg1, arg2) =>
      {
        if (!arg1)
          return;
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      }));
  }

  private void OnInstantBtnClick()
  {
    EquipmentManager.Instance.ForgeEquipment(this.bagType, this.scrollID, true, this.instantGold, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      Hashtable hashtable = arg2 as Hashtable;
      int num = int.Parse(hashtable[(object) "equip_id"].ToString());
      ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(this.bagType).GetData(num);
      if (this.bagType == BagType.Hero)
        ChatMessageManager.SendForgeSuccessMessage(data.LocNameId, data.quality, num, 0);
      else
        ChatMessageManager.SendForgeDKSuccessMessage(data.LocNameId, data.quality, num, 0);
      UIManager.inst.OpenPopup("Equipment/EquipmentForgeSuccessfullyPopup", (Popup.PopupParameter) new EquipmentForgeSuccessfullyPopup.Parameter()
      {
        bagType = this.bagType,
        equipmenItemID = (long) int.Parse(hashtable[(object) "item_id"].ToString())
      });
    }));
  }

  private void OnInstBuildPressed()
  {
    string withPara = ScriptLocalization.GetWithPara("confirm_gold_spend_forge_instant_desc", new Dictionary<string, string>()
    {
      {
        "num",
        this.instantGold.ToString()
      }
    }, true);
    UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
    {
      confirmButtonClickEvent = (System.Action) (() => this.OnInstantBtnClick()),
      description = withPara,
      goldNum = this.instantGold
    });
  }

  private void OnTimerSpeedUpBtnPressed()
  {
    JobHandle singleJobByClass = JobManager.Instance.GetSingleJobByClass(this.GetJobEvent());
    if (singleJobByClass == null)
      return;
    int cost = ItemBag.CalculateCost(new Hashtable()
    {
      {
        (object) "time",
        (object) singleJobByClass.LeftTime()
      }
    });
    if (PlayerData.inst.hostPlayer.Currency < cost)
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_gold_not_enough_speed_up", true), (System.Action) null, 4f, false);
    else
      RequestManager.inst.SendRequest("City:speedUpByGold", new Hashtable()
      {
        {
          (object) "job_id",
          (object) singleJobByClass.GetJobID()
        },
        {
          (object) "gold",
          (object) cost
        }
      }, (System.Action<bool, object>) ((arg1, arg2) =>
      {
        if (!arg1)
          return;
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      }), true);
  }

  private void OnJobCreate(long jobId)
  {
    this.Init(this.bagType, this.scrollID);
  }
}
