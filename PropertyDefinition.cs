﻿// Decompiled with JetBrains decompiler
// Type: PropertyDefinition
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public class PropertyDefinition
{
  private static readonly string NameKey = "_name";
  private string _nameKey = string.Empty;
  private List<int> _subpropertyInternalIDs = new List<int>();
  private List<PropertyDefinition> _subpropertyDefinitions = new List<PropertyDefinition>();
  private int _internalID;
  private string _id;
  private string _targetImage;
  private string _image;
  private PropertyDefinition.FormatType _format;
  private bool _negative;
  private int _priority;
  private string _short_key;
  private string _category;

  public PropertyDefinition(int internalID, string id, PropertyDefinition.FormatType format, string localizationKey, string targetImage, string image, bool negative, int priority, string shortKey, string category)
  {
    this._internalID = internalID;
    this._id = id;
    this._format = format;
    this._nameKey = localizationKey + PropertyDefinition.NameKey;
    this._targetImage = targetImage;
    this._image = image;
    this._negative = negative;
    this._priority = priority;
    this._short_key = shortKey;
    this._category = category;
  }

  public static PropertyDefinition.FormatType ParseFormatType(string value)
  {
    if (string.IsNullOrEmpty(value))
      return PropertyDefinition.FormatType.Invalid;
    if (value.Equals(PropertyDefinition.FormatType.Percent.ToString(), StringComparison.OrdinalIgnoreCase))
      return PropertyDefinition.FormatType.Percent;
    if (value.Equals(PropertyDefinition.FormatType.Integer.ToString(), StringComparison.OrdinalIgnoreCase))
      return PropertyDefinition.FormatType.Integer;
    if (value.Equals(PropertyDefinition.FormatType.Seconds.ToString(), StringComparison.OrdinalIgnoreCase))
      return PropertyDefinition.FormatType.Seconds;
    return value.Equals(PropertyDefinition.FormatType.Boolean.ToString(), StringComparison.OrdinalIgnoreCase) ? PropertyDefinition.FormatType.Boolean : PropertyDefinition.FormatType.Invalid;
  }

  public string ConvertToDisplayString(double value, bool setColor = true, bool formatThousand = true)
  {
    string empty = string.Empty;
    string str1 = !setColor ? (value < 0.0 ? string.Empty : "+") : (value < 0.0 ? "[00ff00]" : "[00ff00]+");
    string str2 = string.Empty;
    switch (this._format)
    {
      case PropertyDefinition.FormatType.Integer:
        str2 = !formatThousand ? value.ToString() : Utils.FormatThousands(((int) value).ToString());
        break;
      case PropertyDefinition.FormatType.Percent:
        str2 = value == 0.0 ? "0%" : string.Format("{0:P2}", (object) value);
        break;
    }
    string str3 = !setColor ? string.Empty : "[-]";
    return str1 + str2 + str3;
  }

  public string ConvertToDisplayStringWithoutPreString(double value, bool setColor = true, bool formatThousand = true, bool isRed = true)
  {
    string str1 = !isRed ? "[00ff00]" : "[00ff00]";
    string str2 = string.Empty;
    switch (this._format)
    {
      case PropertyDefinition.FormatType.Integer:
        str2 = !formatThousand ? value.ToString() : Utils.FormatThousands(((int) value).ToString());
        break;
      case PropertyDefinition.FormatType.Percent:
        str2 = value == 0.0 ? "0%" : string.Format("{0:P2}", (object) value);
        break;
    }
    return str1 + str2 + "[-]";
  }

  public string ConvertToDisplayStringValue(double value, bool formatThousand = true)
  {
    string str = string.Empty;
    switch (this._format)
    {
      case PropertyDefinition.FormatType.Integer:
        str = !formatThousand ? ((int) value).ToString() : Utils.FormatThousands(((int) value).ToString());
        break;
      case PropertyDefinition.FormatType.Percent:
        str = value == 0.0 ? "0%" : string.Format("{0:P2}", (object) value);
        break;
    }
    return str;
  }

  public string ShortKey
  {
    get
    {
      return this._short_key;
    }
  }

  public int InternalID
  {
    get
    {
      return this._internalID;
    }
  }

  public string ID
  {
    get
    {
      return this._id;
    }
  }

  public string Name
  {
    get
    {
      return ScriptLocalization.Get(this._nameKey, true);
    }
  }

  public string TargetImage
  {
    get
    {
      return this._targetImage;
    }
  }

  public bool IsNegative
  {
    get
    {
      return this._negative;
    }
  }

  public string Image
  {
    get
    {
      return "ui_" + this._image;
    }
  }

  public string ImagePath
  {
    get
    {
      return "Texture/Benefits/" + this._image;
    }
  }

  public PropertyDefinition.FormatType Format
  {
    get
    {
      return this._format;
    }
  }

  public int Priority
  {
    get
    {
      return this._priority;
    }
  }

  public string Category
  {
    get
    {
      return this._category;
    }
  }

  public void AddSubproperty(int propertyInternalID)
  {
    if (propertyInternalID <= 0 || this._subpropertyInternalIDs.Contains(propertyInternalID))
      return;
    this._subpropertyInternalIDs.Add(propertyInternalID);
  }

  public ReadOnlyCollection<PropertyDefinition> Subproperties
  {
    get
    {
      if (this._subpropertyInternalIDs.Count > 0 && this._subpropertyInternalIDs.Count != this._subpropertyDefinitions.Count)
      {
        this._subpropertyDefinitions.Clear();
        for (int index = 0; index < this._subpropertyInternalIDs.Count; ++index)
        {
          if (ConfigManager.inst.DB_Properties.Contains(this._subpropertyInternalIDs[index]))
            this._subpropertyDefinitions.Add(ConfigManager.inst.DB_Properties[this._subpropertyInternalIDs[index]]);
        }
      }
      return this._subpropertyDefinitions.AsReadOnly();
    }
  }

  public enum FormatType
  {
    Invalid,
    Integer,
    Percent,
    Seconds,
    Boolean,
  }
}
