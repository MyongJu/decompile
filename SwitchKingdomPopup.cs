﻿// Decompiled with JetBrains decompiler
// Type: SwitchKingdomPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;

public class SwitchKingdomPopup : Popup
{
  private static bool m_IsOneShowing;
  public UILabel m_Content;
  private SwitchKingdomPopup.Parameter m_Parameter;
  private bool m_Clicked;

  public static bool IsOneShowing
  {
    get
    {
      return SwitchKingdomPopup.m_IsOneShowing;
    }
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    SwitchKingdomPopup.m_IsOneShowing = true;
    this.m_Parameter = orgParam as SwitchKingdomPopup.Parameter;
    this.m_Content.text = string.Format(ScriptLocalization.Get("kingdom_enter_confirm_description", true), (object) this.m_Parameter.kingdomID);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    SwitchKingdomPopup.m_IsOneShowing = false;
    if (this.m_Clicked || this.m_Parameter.onNo == null)
      return;
    this.m_Parameter.onNo();
  }

  public void OnOkay()
  {
    this.m_Clicked = true;
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    if (this.m_Parameter.onOkay == null)
      return;
    this.m_Parameter.onOkay();
  }

  public void OnNo()
  {
    this.m_Clicked = true;
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    if (this.m_Parameter.onNo == null)
      return;
    this.m_Parameter.onNo();
  }

  public class Parameter : Popup.PopupParameter
  {
    public int kingdomID;
    public System.Action onOkay;
    public System.Action onNo;
  }
}
