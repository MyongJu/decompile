﻿// Decompiled with JetBrains decompiler
// Type: LegendDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UI;
using UnityEngine;

public class LegendDetailDlg : UI.Dialog
{
  private static int MAX_POINT = 20;
  private int _selectedIndex = -1;
  private int _onePointKillTime = LegendDetailDlg.MAX_POINT * 60;
  private const string ADD_POINt_SMALL = "shopitem_hero_skill_point";
  public UITexture legendIcon;
  public LegendSkillItem[] skills;
  private LegendData _legendData;
  public UILabel remainTime;
  public UILabel remainSkillPoint;
  public UILabel legendName;
  public UILabel legenLevel;
  public UILabel currentXp;
  public UILabel maxXp;
  public UILabel power;
  public UIProgressBar upgradeProgressBar;
  public GameObject recoverContainer;
  public UIButton dismissButton;
  public GameObject content;
  public GameObject skillInfo;
  public UILabel skillDetailTitle;
  public UILabel skillDetail;
  public GameObject skillList;
  public GameObject legendHistory;
  public GameObject skillListNormalBt;
  public GameObject skillListSelectedBt;
  public GameObject legendHistroyNormalBt;
  public GameObject legendHistroySelectedBt;
  public GameObject infantry;
  public GameObject ranged;
  public GameObject cavalry;
  public GameObject siege;
  public UIGrid grid;
  public UILabel legendStroy;
  public UILabel title;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    LegendDetailDlg.LegendDetailDlgParameter detailDlgParameter = orgParam as LegendDetailDlg.LegendDetailDlgParameter;
    if (detailDlgParameter != null)
      this._legendData = detailDlgParameter.legendData;
    this.UpdateUI();
    this.AddEventHandler();
    base.OnShow(orgParam);
    this.SelectedIndex = 0;
  }

  public void OnSkillInfoClick(long skillid)
  {
  }

  public void OnCloseSkillInfo()
  {
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Clear();
    this.RemoveEventHandler();
  }

  public void OnAddBtnPress()
  {
    this.BuySkillPoint();
  }

  private void BuySkillPoint()
  {
    ShopStaticInfo shopData = ConfigManager.inst.DB_Shop.GetShopData("shopitem_hero_skill_point");
    UIManager.inst.OpenPopup("BuyAndUsePopup", (Popup.PopupParameter) new StoreBuyAndUseDialog.StoreBuyAndUseDialogParamer()
    {
      shopStaticInfo = shopData,
      onUsecallBackFun = new System.Action<bool, object>(this.useItemCallBack)
    });
  }

  private void useItemCallBack(bool success, object result)
  {
    if (!success)
      return;
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_addskillpoint_success", true), (System.Action) null, 4f, false);
  }

  public void AddExp()
  {
    if (DBManager.inst.DB_Legend.CheckLegendIsMarching(this._legendData.LegendID))
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_legend_marching", true), (System.Action) null, 4f, false);
    else
      UIManager.inst.OpenDlg("Legend/LegendAddExpAndDevourDialog", (UI.Dialog.DialogParameter) new LegendAddExpAndDevourDialog.Parameter()
      {
        legendData = this._legendData
      }, true, true, true);
  }

  public void OnAddSkillPointPress(long skillId, int cout)
  {
    MessageHub.inst.GetPortByAction("Legend:assignSkillPoint").SendRequest(new Hashtable()
    {
      {
        (object) "skill_id",
        (object) skillId
      },
      {
        (object) "legend_id",
        (object) skillId
      },
      {
        (object) "uid",
        (object) skillId
      },
      {
        (object) "point_amount",
        (object) cout
      }
    }, (System.Action<bool, object>) null, true);
  }

  public void OnDisbandBtnPress()
  {
    if (DBManager.inst.DB_Legend.CheckLegendIsMarching(this._legendData.LegendID))
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_legend_marching", true), (System.Action) null, 4f, false);
    }
    else
    {
      string str1 = ScriptLocalization.Get("hero_altar_dismis_hero_description", true);
      string str2 = ScriptLocalization.Get("confirm_disband_yesbtlabel", true);
      string str3 = ScriptLocalization.Get("confirm_disband_notlabel", true);
      string str4 = ScriptLocalization.Get("hero_altar_uppercase_dismiss_hero", true);
      UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
      {
        setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
        title = str4,
        leftButtonText = str3,
        leftButtonClickEvent = (System.Action) null,
        rightButtonText = str2,
        rightButtonClickEvent = new System.Action(this.ConfirmDisbandLegend),
        description = str1
      });
    }
  }

  private void ConfirmDisbandLegend()
  {
    BuildingController buildingByType = CitadelSystem.inst.GetBuildingByType("dragon_lair");
    MessageHub.inst.GetPortByAction("Legend:disband").SendRequest(new Hashtable()
    {
      {
        (object) "city_id",
        (object) CityManager.inst.GetCityID()
      },
      {
        (object) "building_id",
        (object) buildingByType.mBuildingID
      },
      {
        (object) "legend_id",
        (object) this._legendData.LegendID
      },
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, new System.Action<bool, object>(this.DismissCallBack), true);
  }

  private void DismissCallBack(bool success, object result)
  {
    if (!success)
      return;
    this.dismissButton.isEnabled = false;
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_dismisslegend_success", true), (System.Action) null, 4f, false);
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void OnUpgradeSkill(long skillId)
  {
    if (DBManager.inst.DB_Legend.CheckLegendIsMarching(this._legendData.LegendID))
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_legend_marching", true), (System.Action) null, 4f, false);
    else if (!DBManager.inst.DB_Legend.CheckLegendIsUnderRecruit(this._legendData.LegendID))
    {
      string str1 = ScriptLocalization.Get("legend_no_recruit", true);
      string str2 = ScriptLocalization.Get("legend_detail_oklabel", true);
      string str3 = ScriptLocalization.Get("legend_detail_titlelabel", true);
      UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
      {
        setType = (SingleButtonPopup.SetType.title | SingleButtonPopup.SetType.description | SingleButtonPopup.SetType.buttonEvent | SingleButtonPopup.SetType.buttonText),
        title = str3,
        buttonText = str2,
        description = str1
      });
    }
    else
    {
      LegendSkillInfo skillInfo = ConfigManager.inst.DB_LegendSkill.GetSkillInfo((int) skillId);
      if (skillInfo.skillPoints > PlayerData.inst.userData.skillPoint)
        this.OnAddBtnPress();
      else if (!skillInfo.cost.IsSilverEnough())
        GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.SILVER);
      else
        MessageHub.inst.GetPortByAction("Legend:upgradeSkill").SendRequest(new Hashtable()
        {
          {
            (object) "skill_id",
            (object) skillId
          },
          {
            (object) "legend_id",
            (object) this._legendData.LegendID
          },
          {
            (object) "uid",
            (object) PlayerData.inst.uid
          }
        }, new System.Action<bool, object>(this.UpgardeSkillCallBack), true);
    }
  }

  private void UpgardeSkillCallBack(bool success, object result)
  {
    if (!success)
      return;
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_legendupgradeskill_success", true), (System.Action) null, 4f, false);
  }

  private void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.legendIcon);
    foreach (LegendSkillItem skill in this.skills)
      skill.Clear();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_User.onDataChanged += new System.Action<long>(this.OnUserDataChanged);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
    DBManager.inst.DB_Legend.onDataChanged += new System.Action<long>(this.AddLegendDataChangeHandler);
  }

  private void AddLegendDataChangeHandler(long legendId)
  {
    if ((long) this._legendData.LegendID != legendId)
      return;
    this.UpdateUI();
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_User.onDataChanged -= new System.Action<long>(this.OnUserDataChanged);
    DBManager.inst.DB_Legend.onDataChanged -= new System.Action<long>(this.AddLegendDataChangeHandler);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
  }

  private void Process(int time)
  {
    this.UpdateRemainTime();
  }

  private void OnUserDataChanged(long uid)
  {
    this.FixElapseTime();
    this.remainSkillPoint.text = PlayerData.inst.userData.skillPoint.ToString();
  }

  public void OnTimeInfoPress()
  {
  }

  public void SelectedSkillList()
  {
    this.SelectedIndex = 0;
  }

  public void SelectedLegendHistory()
  {
    this.SelectedIndex = 1;
  }

  public int SelectedIndex
  {
    get
    {
      return this._selectedIndex;
    }
    protected set
    {
      if (this._selectedIndex == value)
        return;
      this._selectedIndex = value;
      this.ChangeTabContent();
    }
  }

  private void ChangeTabContent()
  {
    if (this.SelectedIndex == 0)
    {
      NGUITools.SetActive(this.skillList.gameObject, true);
      NGUITools.SetActive(this.legendHistory.gameObject, false);
      NGUITools.SetActive(this.skillListSelectedBt.gameObject, true);
      NGUITools.SetActive(this.legendHistroySelectedBt.gameObject, false);
    }
    else
    {
      NGUITools.SetActive(this.skillList.gameObject, false);
      NGUITools.SetActive(this.legendHistory.gameObject, true);
      NGUITools.SetActive(this.skillListSelectedBt.gameObject, false);
      NGUITools.SetActive(this.legendHistroySelectedBt.gameObject, true);
    }
  }

  private void UpdateUI()
  {
    this.Clear();
    this.title.text = ScriptLocalization.Get("hero_check_title", true);
    Utils.GetUITextPath();
    this.dismissButton.isEnabled = DBManager.inst.DB_Legend.CheckLegendIsAviable(this._legendData.LegendID);
    LegendInfo legendInfo = ConfigManager.inst.DB_Legend.GetLegendInfo(this._legendData.LegendID);
    ConfigManager.inst.DB_LegendPoint.GetLegendPointInfo(this._legendData.LegendID);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.legendIcon, legendInfo.Image, (System.Action<bool>) null, true, false, string.Empty);
    if (this._legendData.Skills != null)
    {
      for (int index = 0; index < this._legendData.Skills.Length; ++index)
      {
        LegendSkillInfo skillInfo = ConfigManager.inst.DB_LegendSkill.GetSkillInfo(this._legendData.Skills[index]);
        LegendSkillItem skill = this.skills[index];
        skill.SetData(skillInfo, this._legendData);
        skill.UpdagteSkillHandler = new System.Action<long>(this.OnUpgradeSkill);
      }
    }
    this.FixElapseTime();
    this.remainSkillPoint.text = PlayerData.inst.userData.skillPoint.ToString();
    this.remainTime.text = string.Empty;
    long num1 = PlayerData.inst.userData.skillPointRecoverTime - (long) NetServerTime.inst.ServerTimestamp;
    if (num1 > 0L)
    {
      NGUITools.SetActive(this.recoverContainer, true);
      long num2 = num1 % (long) this._onePointKillTime;
      if (num2 > 0L)
        this.remainTime.text = Utils.FormatTime((int) num2, false, false, true);
    }
    else
      NGUITools.SetActive(this.recoverContainer, false);
    this.legendName.text = legendInfo.Loc_Name;
    int nextXp = 0;
    this.legenLevel.text = ConfigManager.inst.DB_LegendPoint.GetLegendLevelByXP(this._legendData.Xp, out nextXp).ToString();
    if (nextXp <= 0)
      nextXp = (int) this._legendData.Xp;
    float num3 = 0.0f;
    if (nextXp > 0)
      num3 = (float) this._legendData.Xp / (float) nextXp;
    this.upgradeProgressBar.value = num3;
    this.currentXp.text = this._legendData.Xp.ToString() + "/" + nextXp.ToString();
    this.power.text = this._legendData.Power.ToString();
    NGUITools.SetActive(this.infantry, legendInfo.Infantry > 0);
    NGUITools.SetActive(this.ranged, legendInfo.Ranged > 0);
    NGUITools.SetActive(this.cavalry, legendInfo.Cavalry > 0);
    NGUITools.SetActive(this.siege, legendInfo.Siege > 0);
    this.legendStroy.text = legendInfo.Loc_Desc;
    this.grid.repositionNow = true;
    this.grid.Reposition();
  }

  private void FixElapseTime()
  {
    long num1 = PlayerData.inst.userData.skillPointRecoverTime - (long) NetServerTime.inst.ServerTimestamp;
    int maxPoint = LegendDetailDlg.MAX_POINT;
    int num2;
    if (num1 > 0L)
    {
      num2 = (int) num1 / this._onePointKillTime;
      if (num1 % (long) this._onePointKillTime > 0L)
        ++num2;
    }
    else
      num2 = 0;
    PlayerData.inst.userData.skillPoint = LegendDetailDlg.MAX_POINT - num2;
  }

  private void UpdateRemainTime()
  {
    long num1 = PlayerData.inst.userData.skillPointRecoverTime - (long) NetServerTime.inst.ServerTimestamp;
    if (num1 > 0L)
    {
      NGUITools.SetActive(this.recoverContainer, true);
      long num2 = num1 % (long) this._onePointKillTime;
      if (num2 > 0L)
        this.remainTime.text = Utils.FormatTime((int) num2, false, false, true);
      else if (PlayerData.inst.userData.skillPoint < LegendDetailDlg.MAX_POINT)
        this.remainTime.text = Utils.FormatTime(this._onePointKillTime, false, false, true);
    }
    else
    {
      this.remainTime.text = string.Empty;
      NGUITools.SetActive(this.recoverContainer, false);
    }
    this.FixElapseTime();
    this.remainSkillPoint.text = PlayerData.inst.userData.skillPoint.ToString();
  }

  public class LegendDetailDlgParameter : UI.Dialog.DialogParameter
  {
    public LegendData legendData;
  }
}
