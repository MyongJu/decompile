﻿// Decompiled with JetBrains decompiler
// Type: GVEWarReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class GVEWarReportPopup : BaseReportPopup
{
  private List<GameObject> pool = new List<GameObject>();
  public float gapX = 100f;
  private Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>> allmytroopdetail = new Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>>();
  private Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>> allothertroopdetail = new Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>>();
  private Dictionary<string, Hashtable> allmydragondetail = new Dictionary<string, Hashtable>();
  private Dictionary<string, Hashtable> allotherdragondetail = new Dictionary<string, Hashtable>();
  private Dictionary<string, List<Hashtable>> allmylegenddetail = new Dictionary<string, List<Hashtable>>();
  private Dictionary<string, List<Hashtable>> allotherlegenddetail = new Dictionary<string, List<Hashtable>>();
  private WarReportContent wrc;
  public UIScrollView sv;
  public WarOverview mywaroverview;
  public WarOverview otherwaroverview;
  public UILabel recovery;
  public TroopOverview mytroopoverview;
  public TroopOverview othertroopoverview;
  public GameObject boostdetailGroup;
  public BoostInfo myboostinfo;
  public BoostInfo otherboostinfo;
  public UISprite myboostbg;
  public UISprite otherboostbg;
  public GameObject btnGroup;
  private TroopLeader mytd;
  private TroopLeader othertd;

  private void Init()
  {
    bool flag = this.wrc.IsWinner();
    List<TroopDetail> troopDetailList1;
    List<TroopDetail> troopDetailList2;
    if (this.wrc.IsAttacker())
    {
      this.mytd = this.wrc.attacker;
      this.mytd.SetLegend(this.wrc.legend);
      this.othertd = this.wrc.defender;
      troopDetailList1 = this.wrc.attacker_troop_detail;
      troopDetailList2 = this.wrc.defender_troop_detail;
    }
    else
    {
      this.mytd = this.wrc.defender;
      this.mytd.SetLegend(this.wrc.legend);
      this.othertd = this.wrc.attacker;
      troopDetailList1 = this.wrc.defender_troop_detail;
      troopDetailList2 = this.wrc.attacker_troop_detail;
    }
    this.SetLegends(troopDetailList1, this.wrc.legend);
    this.SetLegends(troopDetailList2, this.wrc.legend);
    string str1 = this.mytd.acronym == null ? (string) null : "(" + this.mytd.acronym + ")";
    string str2 = this.othertd.acronym == null ? (string) null : "(" + this.othertd.acronym + ")";
    string str3 = str1 + this.mytd.name;
    ConfigGveBossData data = ConfigManager.inst.DB_GveBoss.GetData(int.Parse(this.othertd.uid));
    string str4 = Utils.XLAT("id_lv") + (object) data.boss_level + " " + Utils.XLAT(data.name);
    long allPowerLost1 = this.wrc.CalculateAllPowerLost(troopDetailList1);
    long allPowerLost2 = this.wrc.CalculateAllPowerLost(troopDetailList2);
    long allTroopKill1 = this.wrc.CalculateAllTroopKill(troopDetailList1);
    long allTroopKill2 = this.wrc.CalculateAllTroopKill(troopDetailList2);
    long allTroopNotGood1 = this.wrc.CalculateAllTroopNotGood(troopDetailList1);
    long allTroopNotGood2 = this.wrc.CalculateAllTroopNotGood(troopDetailList2);
    long allTroopLost1 = this.wrc.CalculateAllTroopLost(troopDetailList1);
    long allTroopLost2 = this.wrc.CalculateAllTroopLost(troopDetailList2);
    long allTroopWound1 = this.wrc.CalculateAllTroopWound(troopDetailList1);
    long allTroopWound2 = this.wrc.CalculateAllTroopWound(troopDetailList2);
    long allTroopSurvive1 = this.wrc.CalculateAllTroopSurvive(troopDetailList1);
    long allTroopSurvive2 = this.wrc.CalculateAllTroopSurvive(troopDetailList2);
    this.wrc.SummarizeAllTroopDetail(troopDetailList1, ref this.allmytroopdetail);
    this.wrc.SummarizeAllTroopDetail(troopDetailList2, ref this.allothertroopdetail);
    this.wrc.SummarizeAllDragonDetail(troopDetailList1, ref this.allmydragondetail);
    this.wrc.SummarizeAllDragonDetail(troopDetailList2, ref this.allotherdragondetail);
    this.wrc.SummarizeAllLegendDetail(troopDetailList1, ref this.allmylegenddetail);
    this.wrc.SummarizeAllLegendDetail(troopDetailList2, ref this.allotherlegenddetail);
    List<BoostInfo.Data> boostdetail1 = new List<BoostInfo.Data>();
    List<BoostInfo.Data> boostdetail2 = new List<BoostInfo.Data>();
    this.mytd.SummarizeBoostDetail(ref boostdetail1);
    this.othertd.SummarizeBoostDetail(ref boostdetail2);
    if (flag)
      BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, true, true, string.Empty);
    else
      BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_loser", (System.Action<bool>) null, true, true, string.Empty);
    this.coordinate.text = "K: " + this.wrc.k + " X: " + this.wrc.x + " Y: " + this.wrc.y;
    this.time.text = Utils.FormatTimeForMail(this.mailtime);
    WarOverview.Data d1 = new WarOverview.Data();
    d1.name = str3;
    d1.icon = "Texture/Hero/Portrait_Icon/player_portrait_icon_" + this.mytd.portrait;
    d1.setIcon = this.mytd.icon;
    d1.lordTitleId = this.mytd.lord_title;
    d1.k = this.mytd.k;
    d1.x = this.mytd.x;
    d1.y = this.mytd.y;
    d1.powerlost = allPowerLost1;
    d1.dsht = this.mytd.dragon;
    d1.trooplost = allTroopNotGood1;
    d1.mylegend = this.mytd.legends;
    this.mywaroverview.SeedData(d1);
    d1.name = str4;
    d1.icon = data.ImagePath;
    d1.setIcon = (string) null;
    d1.lordTitleId = 0;
    d1.k = this.othertd.k;
    d1.x = this.othertd.x;
    d1.y = this.othertd.y;
    d1.powerlost = allPowerLost2;
    d1.dsht = this.othertd.dragon;
    d1.trooplost = allTroopNotGood2;
    d1.mylegend = this.othertd.legends;
    this.otherwaroverview.SeedData(d1);
    TroopOverview.Data d2 = new TroopOverview.Data();
    d2.kill = allTroopKill1;
    d2.dragonAoeKill = (long) this.mytd.dragon_skill_aoe_killed;
    d2.dead = allTroopLost1;
    d2.wounded = allTroopWound1;
    d2.survived = allTroopSurvive1;
    this.mytroopoverview.SeedData(d2);
    d2.kill = allTroopKill2;
    d2.dragonAoeKill = (long) this.othertd.dragon_skill_aoe_killed;
    d2.dead = allTroopLost2;
    d2.wounded = allTroopWound2;
    d2.survived = allTroopSurvive2;
    this.othertroopoverview.SeedData(d2);
    this.recovery.text = ScriptLocalization.GetWithPara("mail_pve_boss_troops_healed_description", new Dictionary<string, string>()
    {
      {
        "0",
        ((float) ((1.0 - (double) ConfigManager.inst.DB_BattleWounded.GetData("battle_wounded_7").loss_rate) * 100.0)).ToString()
      }
    }, true);
    float num1 = 0.0f;
    float y = NGUIMath.CalculateRelativeWidgetBounds(this.myboostinfo.transform).size.y;
    float num2 = this.myboostinfo.transform.localPosition.y + y / 2f;
    using (List<BoostInfo.Data>.Enumerator enumerator = boostdetail1.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BoostInfo.Data current = enumerator.Current;
        GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.myboostinfo.gameObject, this.myboostinfo.transform.parent);
        this.pool.Add(gameObject);
        gameObject.GetComponent<BoostInfo>().SeedData(current);
        float num3 = NGUIMath.CalculateRelativeWidgetBounds(gameObject.transform).size.y + 15f;
        num1 += num3;
        float num4 = num2 - num3 / 2f;
        Vector3 localPosition = this.myboostinfo.transform.localPosition;
        localPosition.y = num4;
        gameObject.transform.localPosition = localPosition;
        num2 = num4 - num3 / 2f;
      }
    }
    if (boostdetail1.Count != 0)
    {
      this.myboostbg.gameObject.SetActive(true);
      this.myboostbg.height = (int) num1 + (int) y / 2;
    }
    else
      this.myboostbg.gameObject.SetActive(false);
    float num5 = 0.0f;
    float num6 = this.otherboostinfo.transform.localPosition.y + y / 2f;
    using (List<BoostInfo.Data>.Enumerator enumerator = boostdetail2.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BoostInfo.Data current = enumerator.Current;
        GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.otherboostinfo.gameObject, this.otherboostinfo.transform.parent);
        this.pool.Add(gameObject);
        gameObject.GetComponent<BoostInfo>().SeedData(current);
        float num3 = NGUIMath.CalculateRelativeWidgetBounds(this.otherboostinfo.transform).size.y + 15f;
        num5 += num3;
        float num4 = num6 - num3 / 2f;
        Vector3 localPosition = this.otherboostinfo.transform.localPosition;
        localPosition.y = num4;
        gameObject.transform.localPosition = localPosition;
        num6 = num4 - num3 / 2f;
      }
    }
    if (boostdetail2.Count != 0)
    {
      this.otherboostbg.gameObject.SetActive(true);
      this.otherboostbg.height = (int) num5 + (int) y / 2;
    }
    else
      this.otherboostbg.gameObject.SetActive(false);
    this.ArrangeLayout();
    this.HideTemplate(true);
    this.sv.ResetPosition();
  }

  public void SetLegends(List<TroopDetail> troops, List<Hashtable> _legends)
  {
    for (int index = 0; index < troops.Count; ++index)
    {
      if (_legends != null)
      {
        if (troops[index].legends != null)
          troops[index].legends.Clear();
        else
          troops[index].legends = new List<Hashtable>();
        using (List<Hashtable>.Enumerator enumerator = _legends.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Hashtable current = enumerator.Current;
            if (current[(object) "uid"].ToString() == troops[index].uid.ToString())
              troops[index].legends.Add(current);
          }
        }
      }
    }
  }

  public void OnLoadTroopDetail()
  {
    WarReportDetail.Parameter parameter = new WarReportDetail.Parameter();
    if (this.wrc.IsAttacker())
    {
      parameter.pattackertroopdetail = this.allmytroopdetail;
      parameter.pdefendertroopdetail = this.allothertroopdetail;
      parameter.pattackerdragon = this.allmydragondetail;
      parameter.pdefenderdragon = this.allotherdragondetail;
      parameter.pattackerlegend = this.allmylegenddetail;
      parameter.pdefenderlegend = this.allotherlegenddetail;
    }
    else
    {
      parameter.pattackertroopdetail = this.allothertroopdetail;
      parameter.pdefendertroopdetail = this.allmytroopdetail;
      parameter.pattackerdragon = this.allotherdragondetail;
      parameter.pdefenderdragon = this.allmydragondetail;
      parameter.pattackerlegend = this.allotherlegenddetail;
      parameter.pdefenderlegend = this.allmylegenddetail;
    }
    UIManager.inst.OpenPopup("Report/WarReportDetail", (Popup.PopupParameter) parameter);
  }

  private void ArrangeLayout()
  {
    float y1 = this.boostdetailGroup.transform.localPosition.y;
    float y2 = NGUIMath.CalculateRelativeWidgetBounds(this.boostdetailGroup.transform).size.y;
    Vector3 localPosition = this.btnGroup.transform.localPosition;
    localPosition.y = y1 - y2;
    this.btnGroup.transform.localPosition = localPosition;
    this.sv.UpdatePosition();
  }

  private void Reset()
  {
    this.sv.ResetPosition();
    Rigidbody component = this.sv.transform.GetComponent<Rigidbody>();
    if ((UnityEngine.Object) null != (UnityEngine.Object) component)
    {
      component.isKinematic = true;
      component.useGravity = false;
    }
    this.HideTemplate(false);
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        PrefabManagerEx.Instance.Destroy(current);
      }
    }
    this.pool.Clear();
  }

  private void HideTemplate(bool hide)
  {
    this.myboostinfo.gameObject.SetActive(!hide);
    this.otherboostinfo.gameObject.SetActive(!hide);
  }

  public void GotoMycity()
  {
    this.GoToTargetPlace(int.Parse(this.mytd.k), int.Parse(this.mytd.x), int.Parse(this.mytd.y));
  }

  public void GotoEnemycity()
  {
    this.GoToTargetPlace(int.Parse(this.othertd.k), int.Parse(this.othertd.x), int.Parse(this.othertd.y));
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.wrc = JsonReader.Deserialize<WarReport>(Utils.Object2Json((object) this.param.hashtable)).data;
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }
}
