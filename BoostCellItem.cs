﻿// Decompiled with JetBrains decompiler
// Type: BoostCellItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class BoostCellItem : MonoBehaviour
{
  private Dictionary<string, BoostItem> _itemMap = new Dictionary<string, BoostItem>();
  public UISprite bg;
  public UIButton extendBtn;
  public UILabel titleLb;
  public UILabel basicEffectLb;
  public UILabel tempEffectLb;
  public UILabel timeLb;
  public UISprite line;
  public UITable itemsList;
  public UITweener extendBtnAnim;
  public TweenHeight extendFrameAnim;
  private GameObjectPool _itemPool;
  private UITable _table;
  private bool _inited;
  public float baseHeight;
  public float totalHeight;
  private GameObject _placeHolder;
  private bool _isExtend;
  private bool _inTween;

  public void Init(GameObjectPool itemPool, UIScrollView view, UITable table)
  {
    this._itemPool = itemPool;
    if (!this._inited)
    {
      this.InitElements();
      this._inited = true;
    }
    this.SetExtendState(false);
    UIDragScrollView uiDragScrollView = this.GetComponent<UIDragScrollView>();
    if ((Object) uiDragScrollView == (Object) null)
      uiDragScrollView = this.gameObject.AddComponent<UIDragScrollView>();
    uiDragScrollView.scrollView = view;
    this._table = table;
    this._inTween = false;
  }

  public void Release()
  {
    this.ClearItems();
    this._itemPool = (GameObjectPool) null;
    this.extendBtnAnim.enabled = false;
    this.extendFrameAnim.enabled = false;
    UIDragScrollView component = this.GetComponent<UIDragScrollView>();
    if ((Object) component != (Object) null)
      component.scrollView = (UIScrollView) null;
    this._table = (UITable) null;
    this._inTween = false;
  }

  private void InitElements()
  {
    this.baseHeight = (float) this.bg.height;
    this.extendFrameAnim.from = (int) this.baseHeight;
    this.extendBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnExtendBtnClick)));
    this.extendFrameAnim.AddOnFinished(new EventDelegate(new EventDelegate.Callback(this.OnExtendAnimFinish)));
  }

  private void CalcHeight()
  {
    this.totalHeight = this.baseHeight + Mathf.Abs(this._placeHolder.transform.localPosition.y);
    this.extendFrameAnim.to = (int) this.totalHeight;
  }

  public void SetData(BoostCellData data)
  {
    this.ClearItems();
    this.titleLb.text = data.sum.title.ToString();
    BoostsConst.SetupEffectStr(this.basicEffectLb, data.sum.basicEffect);
    this.tempEffectLb.text = string.Empty;
    this.timeLb.text = string.Empty;
    using (List<BoostItemData>.Enumerator enumerator = data.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.AddItem(enumerator.Current);
    }
    if ((Object) this._placeHolder == (Object) null)
      this._placeHolder = new GameObject();
    this._placeHolder.transform.parent = this.itemsList.transform;
    this.itemsList.onReposition += new UITable.OnReposition(this.OnItemRepositionFinish);
    this.itemsList.gameObject.SetActive(true);
    this.itemsList.Reposition();
    this.itemsList.gameObject.SetActive(this._isExtend);
  }

  private void OnItemRepositionFinish()
  {
    this.CalcHeight();
    this.RefreshState();
    this.itemsList.onReposition -= new UITable.OnReposition(this.OnItemRepositionFinish);
  }

  private void AddItem(BoostItemData data)
  {
    BoostItem component = this._itemPool.Allocate().GetComponent<BoostItem>();
    component.transform.parent = this.itemsList.transform;
    this._itemMap.Add(data.GetHashCode().ToString(), component);
    component.SetData(data);
  }

  private void ClearItems()
  {
    using (Dictionary<string, BoostItem>.ValueCollection.Enumerator enumerator = this._itemMap.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BoostItem current = enumerator.Current;
        current.Release();
        this._itemPool.Release(current.gameObject);
      }
    }
    this._itemMap.Clear();
  }

  public void OnExtendBtnClick()
  {
    this.SwitchExtendStateWithAnim();
  }

  public void RefreshState()
  {
    this.SetExtendState(this._isExtend);
  }

  public void SetExtendState(bool isExtend)
  {
    this._isExtend = isExtend;
    this.bg.height = !this._isExtend ? (int) this.baseHeight : (int) this.totalHeight;
    this.itemsList.gameObject.SetActive(isExtend);
    this.SetLineState(isExtend);
    this.extendBtnAnim.Sample(!isExtend ? 0.0f : 1f, false);
  }

  public void SetLineState(bool isExtend)
  {
    if (isExtend && (double) this.totalHeight > (double) this.baseHeight)
      this.line.gameObject.SetActive(true);
    else
      this.line.gameObject.SetActive(false);
  }

  public void SetExtendStateWithAnim(bool isExtend)
  {
    this._inTween = true;
    if (isExtend)
    {
      this.extendBtnAnim.PlayForward();
      this.extendFrameAnim.PlayForward();
    }
    else
    {
      this.extendBtnAnim.PlayReverse();
      this.extendFrameAnim.PlayReverse();
      this.SetLineState(false);
      this.itemsList.gameObject.SetActive(false);
    }
  }

  public void SwitchExtendStateWithAnim()
  {
    this._isExtend = !this._isExtend;
    this.SetExtendStateWithAnim(this._isExtend);
  }

  public void OnExtendAnimFinish()
  {
    this._inTween = false;
    if (!this._isExtend)
      return;
    this.SetLineState(true);
    this.itemsList.gameObject.SetActive(true);
  }

  private void LateUpdate()
  {
    if (!this._inTween)
      return;
    this._table.repositionNow = true;
  }
}
