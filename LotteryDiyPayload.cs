﻿// Decompiled with JetBrains decompiler
// Type: LotteryDiyPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;

public class LotteryDiyPayload
{
  private static LotteryDiyPayload _instance;
  private LotteryDiyBaseData _lotteryData;

  public static LotteryDiyPayload Instance
  {
    get
    {
      if (LotteryDiyPayload._instance == null)
        LotteryDiyPayload._instance = new LotteryDiyPayload();
      return LotteryDiyPayload._instance;
    }
  }

  public LotteryDiyBaseData LotteryData
  {
    get
    {
      if (this._lotteryData == null)
        this._lotteryData = new LotteryDiyBaseData();
      return this._lotteryData;
    }
  }

  public bool CanLotteryShow()
  {
    if (this._lotteryData != null && NetServerTime.inst.ServerTimestamp >= this._lotteryData.StartTime)
      return NetServerTime.inst.ServerTimestamp < this._lotteryData.EndTime;
    return false;
  }

  public int NeedPropsCount2Play
  {
    get
    {
      return this.GetPropsCountByStep(this.GetNextStepNumber());
    }
  }

  public int GetNextStepNumber()
  {
    int num = 1;
    if (this.LotteryData.LotteryPrevItemDict != null)
    {
      Dictionary<int, LotteryDiyBaseData.RewardData>.ValueCollection.Enumerator enumerator = this.LotteryData.LotteryPrevItemDict.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.isOpen)
          ++num;
      }
    }
    return num;
  }

  public int GetPropsCountByStep(int step)
  {
    if (this.LotteryData != null && this.LotteryData.LotterySpendNumber != null && this.LotteryData.LotterySpendNumber.ContainsKey(step))
      return this.LotteryData.LotterySpendNumber[step];
    return 0;
  }

  public void ShowFirstLotteryScene()
  {
    this.RequestServerData((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      if (this.LotteryData.IsPrevLotteryFinished)
        UIManager.inst.OpenDlg("LotteryDiy/LotteryDiyPickDlg", (UI.Dialog.DialogParameter) null, true, true, true);
      else
        UIManager.inst.OpenDlg("LotteryDiy/LotteryDiyPlayDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    }));
  }

  public void RequestServerData(System.Action<bool, object> callback = null)
  {
    if (callback != null)
    {
      string empty = string.Empty;
      MessageHub.inst.GetPortByAction(this._lotteryData == null || this._lotteryData.LotteryRewards.Count > 0 ? "casino:getDiyInfo" : "casino:getOptionalList").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          this.Decode(data as Hashtable);
        if (callback == null)
          return;
        callback(ret, data);
      }), true);
    }
    else
      MessageHub.inst.GetPortByAction("casino:getDiyInfo").SendLoader((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.Decode(data as Hashtable);
      }), false, false);
  }

  public void Initialize()
  {
    this.RequestServerData((System.Action<bool, object>) null);
    MessageHub.inst.GetPortByAction("open_diy_casino").AddEvent(new System.Action<object>(this.LotteryDiyPush));
    MessageHub.inst.GetPortByAction("close_diy_casino").AddEvent(new System.Action<object>(this.LotteryDiyPush));
  }

  public void Dispose()
  {
    this._lotteryData = (LotteryDiyBaseData) null;
    MessageHub.inst.GetPortByAction("open_diy_casino").RemoveEvent(new System.Action<object>(this.LotteryDiyPush));
    MessageHub.inst.GetPortByAction("close_diy_casino").RemoveEvent(new System.Action<object>(this.LotteryDiyPush));
  }

  private void LotteryDiyPush(object data)
  {
    this.Decode(data as Hashtable);
  }

  private void Decode(Hashtable data)
  {
    if (this._lotteryData == null)
      this._lotteryData = new LotteryDiyBaseData();
    if (data != null)
      this._lotteryData.Decode(data);
    else
      this._lotteryData = (LotteryDiyBaseData) null;
  }

  public enum LotteryArea
  {
    LEFT,
    RIGHT,
  }

  public class LotteryDiyItemInfo
  {
    public int id;
    public int itemId;
    public long itemCount;
  }
}
