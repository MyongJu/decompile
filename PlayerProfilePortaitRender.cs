﻿// Decompiled with JetBrains decompiler
// Type: PlayerProfilePortaitRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PlayerProfilePortaitRender : MonoBehaviour
{
  private const string PATH_PRE = "Texture/Hero/player_portrait_";
  public int index;
  public GameObject zBlock;
  public UITexture texture;

  public void Init()
  {
    Utils.SetPortrait(this.texture, "Texture/Hero/player_portrait_" + (object) this.index);
  }

  public void Normal()
  {
    this.zBlock.SetActive(true);
    this.transform.localScale = Vector3.one;
  }

  public void HighLight()
  {
    this.zBlock.SetActive(false);
    this.transform.localScale = Vector3.one * 1.1f;
  }
}
