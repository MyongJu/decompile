﻿// Decompiled with JetBrains decompiler
// Type: NGUIText
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using UnityEngine;

public static class NGUIText
{
  public static NGUIText.GlyphInfo glyph = new NGUIText.GlyphInfo();
  public static int fontSize = 16;
  public static float fontScale = 1f;
  public static float pixelDensity = 1f;
  public static FontStyle fontStyle = FontStyle.Normal;
  public static NGUIText.Alignment alignment = NGUIText.Alignment.Left;
  public static Color tint = Color.white;
  public static int rectWidth = 1000000;
  public static int rectHeight = 1000000;
  public static int regionWidth = 1000000;
  public static int regionHeight = 1000000;
  public static int maxLines = 0;
  public static bool gradient = false;
  public static Color gradientBottom = Color.white;
  public static Color gradientTop = Color.white;
  public static bool encoding = false;
  public static float spacingX = 0.0f;
  public static float spacingY = 0.0f;
  public static bool premultiply = false;
  public static int finalSize = 0;
  public static float finalSpacingX = 0.0f;
  public static float finalLineHeight = 0.0f;
  public static float baseline = 0.0f;
  public static bool useSymbols = false;
  private static Color mInvisible = new Color(0.0f, 0.0f, 0.0f, 0.0f);
  private static BetterList<Color> mColors = new BetterList<Color>();
  private static float mAlpha = 1f;
  private static BetterList<float> mSizes = new BetterList<float>();
  private static float[] mBoldOffset = new float[8]
  {
    -0.25f,
    0.0f,
    0.25f,
    0.0f,
    0.0f,
    -0.25f,
    0.0f,
    0.25f
  };
  public static UIFont bitmapFont;
  public static Font dynamicFont;
  public static NGUIText.SymbolStyle symbolStyle;
  private static CharacterInfo mTempChar;
  private static Color32 s_c0;
  private static Color32 s_c1;

  public static void Update()
  {
    NGUIText.Update(true);
  }

  public static void Update(bool request)
  {
    NGUIText.finalSize = Mathf.RoundToInt((float) NGUIText.fontSize / NGUIText.pixelDensity);
    NGUIText.finalSpacingX = NGUIText.spacingX * NGUIText.fontScale;
    NGUIText.finalLineHeight = ((float) NGUIText.fontSize + NGUIText.spacingY) * NGUIText.fontScale;
    NGUIText.useSymbols = (UnityEngine.Object) NGUIText.bitmapFont != (UnityEngine.Object) null && NGUIText.bitmapFont.hasSymbols && NGUIText.encoding && NGUIText.symbolStyle != NGUIText.SymbolStyle.None;
    if (!((UnityEngine.Object) NGUIText.dynamicFont != (UnityEngine.Object) null) || !request)
      return;
    NGUIText.dynamicFont.RequestCharactersInTexture(")_-", NGUIText.finalSize, NGUIText.fontStyle);
    if (!NGUIText.dynamicFont.GetCharacterInfo(')', out NGUIText.mTempChar, NGUIText.finalSize, NGUIText.fontStyle) || (double) NGUIText.mTempChar.maxY == 0.0)
    {
      NGUIText.dynamicFont.RequestCharactersInTexture("A", NGUIText.finalSize, NGUIText.fontStyle);
      if (!NGUIText.dynamicFont.GetCharacterInfo('A', out NGUIText.mTempChar, NGUIText.finalSize, NGUIText.fontStyle))
      {
        NGUIText.baseline = 0.0f;
        return;
      }
    }
    float maxY = (float) NGUIText.mTempChar.maxY;
    float minY = (float) NGUIText.mTempChar.minY;
    NGUIText.baseline = Mathf.Round(maxY + (float) (((double) NGUIText.finalSize - (double) maxY + (double) minY) * 0.5));
  }

  public static void Prepare(string text)
  {
    if (!((UnityEngine.Object) NGUIText.dynamicFont != (UnityEngine.Object) null))
      return;
    NGUIText.dynamicFont.RequestCharactersInTexture(text, NGUIText.finalSize, NGUIText.fontStyle);
  }

  public static BMSymbol GetSymbol(string text, int index, int textLength)
  {
    if ((UnityEngine.Object) NGUIText.bitmapFont != (UnityEngine.Object) null)
      return NGUIText.bitmapFont.MatchSymbol(text, index, textLength);
    return (BMSymbol) null;
  }

  public static float GetGlyphWidth(int ch, int prev)
  {
    if ((UnityEngine.Object) NGUIText.bitmapFont != (UnityEngine.Object) null)
    {
      bool flag = false;
      if (ch == 8201)
      {
        flag = true;
        ch = 32;
      }
      BMGlyph glyph = NGUIText.bitmapFont.bmFont.GetGlyph(ch);
      if (glyph != null)
      {
        int advance = glyph.advance;
        if (flag)
          advance >>= 1;
        return NGUIText.fontScale * (prev == 0 ? (float) glyph.advance : (float) (advance + glyph.GetKerning(prev)));
      }
    }
    else if ((UnityEngine.Object) NGUIText.dynamicFont != (UnityEngine.Object) null && NGUIText.dynamicFont.GetCharacterInfo((char) ch, out NGUIText.mTempChar, NGUIText.finalSize, NGUIText.fontStyle))
      return (float) NGUIText.mTempChar.advance * NGUIText.fontScale * NGUIText.pixelDensity;
    return 0.0f;
  }

  public static NGUIText.GlyphInfo GetGlyph(int ch, int prev)
  {
    if ((UnityEngine.Object) NGUIText.bitmapFont != (UnityEngine.Object) null)
    {
      bool flag = false;
      if (ch == 8201)
      {
        flag = true;
        ch = 32;
      }
      BMGlyph glyph = NGUIText.bitmapFont.bmFont.GetGlyph(ch);
      if (glyph != null)
      {
        int num = prev == 0 ? 0 : glyph.GetKerning(prev);
        NGUIText.glyph.v0.x = prev == 0 ? (float) glyph.offsetX : (float) (glyph.offsetX + num);
        NGUIText.glyph.v1.y = (float) -glyph.offsetY;
        NGUIText.glyph.v1.x = NGUIText.glyph.v0.x + (float) glyph.width;
        NGUIText.glyph.v0.y = NGUIText.glyph.v1.y - (float) glyph.height;
        NGUIText.glyph.u0.x = (float) glyph.x;
        NGUIText.glyph.u0.y = (float) (glyph.y + glyph.height);
        NGUIText.glyph.u2.x = (float) (glyph.x + glyph.width);
        NGUIText.glyph.u2.y = (float) glyph.y;
        NGUIText.glyph.u1.x = NGUIText.glyph.u0.x;
        NGUIText.glyph.u1.y = NGUIText.glyph.u2.y;
        NGUIText.glyph.u3.x = NGUIText.glyph.u2.x;
        NGUIText.glyph.u3.y = NGUIText.glyph.u0.y;
        int advance = glyph.advance;
        if (flag)
          advance >>= 1;
        NGUIText.glyph.advance = (float) (advance + num);
        NGUIText.glyph.channel = glyph.channel;
        if ((double) NGUIText.fontScale != 1.0)
        {
          NGUIText.glyph.v0 *= NGUIText.fontScale;
          NGUIText.glyph.v1 *= NGUIText.fontScale;
          NGUIText.glyph.advance *= NGUIText.fontScale;
        }
        return NGUIText.glyph;
      }
    }
    else if ((UnityEngine.Object) NGUIText.dynamicFont != (UnityEngine.Object) null && NGUIText.dynamicFont.GetCharacterInfo((char) ch, out NGUIText.mTempChar, NGUIText.finalSize, NGUIText.fontStyle))
    {
      NGUIText.glyph.v0.x = (float) NGUIText.mTempChar.minX;
      NGUIText.glyph.v1.x = (float) NGUIText.mTempChar.maxX;
      NGUIText.glyph.v0.y = (float) NGUIText.mTempChar.maxY - NGUIText.baseline;
      NGUIText.glyph.v1.y = (float) NGUIText.mTempChar.minY - NGUIText.baseline;
      NGUIText.glyph.u0 = NGUIText.mTempChar.uvTopLeft;
      NGUIText.glyph.u1 = NGUIText.mTempChar.uvBottomLeft;
      NGUIText.glyph.u2 = NGUIText.mTempChar.uvBottomRight;
      NGUIText.glyph.u3 = NGUIText.mTempChar.uvTopRight;
      NGUIText.glyph.advance = (float) NGUIText.mTempChar.advance;
      NGUIText.glyph.channel = 0;
      NGUIText.glyph.v0.x = Mathf.Round(NGUIText.glyph.v0.x);
      NGUIText.glyph.v0.y = Mathf.Round(NGUIText.glyph.v0.y);
      NGUIText.glyph.v1.x = Mathf.Round(NGUIText.glyph.v1.x);
      NGUIText.glyph.v1.y = Mathf.Round(NGUIText.glyph.v1.y);
      float num = NGUIText.fontScale * NGUIText.pixelDensity;
      if ((double) num != 1.0)
      {
        NGUIText.glyph.v0 *= num;
        NGUIText.glyph.v1 *= num;
        NGUIText.glyph.advance *= num;
      }
      return NGUIText.glyph;
    }
    return (NGUIText.GlyphInfo) null;
  }

  [DebuggerHidden]
  [DebuggerStepThrough]
  public static float ParseAlpha(string text, int index)
  {
    return Mathf.Clamp01((float) (NGUIMath.HexToDecimal(text[index + 1]) << 4 | NGUIMath.HexToDecimal(text[index + 2])) / (float) byte.MaxValue);
  }

  [DebuggerHidden]
  [DebuggerStepThrough]
  public static Color ParseColor(string text, int offset)
  {
    return NGUIText.ParseColor24(text, offset);
  }

  [DebuggerHidden]
  [DebuggerStepThrough]
  public static Color ParseColor24(string text, int offset)
  {
    int num1 = NGUIMath.HexToDecimal(text[offset]) << 4 | NGUIMath.HexToDecimal(text[offset + 1]);
    int num2 = NGUIMath.HexToDecimal(text[offset + 2]) << 4 | NGUIMath.HexToDecimal(text[offset + 3]);
    int num3 = NGUIMath.HexToDecimal(text[offset + 4]) << 4 | NGUIMath.HexToDecimal(text[offset + 5]);
    float num4 = 0.003921569f;
    return new Color(num4 * (float) num1, num4 * (float) num2, num4 * (float) num3);
  }

  [DebuggerHidden]
  [DebuggerStepThrough]
  public static Color ParseColor32(string text, int offset)
  {
    int num1 = NGUIMath.HexToDecimal(text[offset]) << 4 | NGUIMath.HexToDecimal(text[offset + 1]);
    int num2 = NGUIMath.HexToDecimal(text[offset + 2]) << 4 | NGUIMath.HexToDecimal(text[offset + 3]);
    int num3 = NGUIMath.HexToDecimal(text[offset + 4]) << 4 | NGUIMath.HexToDecimal(text[offset + 5]);
    int num4 = NGUIMath.HexToDecimal(text[offset + 6]) << 4 | NGUIMath.HexToDecimal(text[offset + 7]);
    float num5 = 0.003921569f;
    return new Color(num5 * (float) num1, num5 * (float) num2, num5 * (float) num3, num5 * (float) num4);
  }

  [DebuggerHidden]
  [DebuggerStepThrough]
  public static string EncodeColor(Color c)
  {
    return NGUIText.EncodeColor24(c);
  }

  [DebuggerStepThrough]
  [DebuggerHidden]
  public static string EncodeColor(string text, Color c)
  {
    return "[c][" + NGUIText.EncodeColor24(c) + "]" + text + "[-][/c]";
  }

  [DebuggerHidden]
  [DebuggerStepThrough]
  public static string EncodeAlpha(float a)
  {
    return NGUIMath.DecimalToHex8(Mathf.Clamp(Mathf.RoundToInt(a * (float) byte.MaxValue), 0, (int) byte.MaxValue));
  }

  [DebuggerStepThrough]
  [DebuggerHidden]
  public static string EncodeColor24(Color c)
  {
    return NGUIMath.DecimalToHex24(16777215 & NGUIMath.ColorToInt(c) >> 8);
  }

  [DebuggerStepThrough]
  [DebuggerHidden]
  public static string EncodeColor32(Color c)
  {
    return NGUIMath.DecimalToHex32(NGUIMath.ColorToInt(c));
  }

  public static bool ParseSymbol(string text, ref int index)
  {
    int sub = 1;
    bool bold = false;
    bool italic = false;
    bool underline = false;
    bool strike = false;
    bool ignoreColor = false;
    return NGUIText.ParseSymbol(text, ref index, (BetterList<Color>) null, false, ref sub, ref bold, ref italic, ref underline, ref strike, ref ignoreColor);
  }

  [DebuggerHidden]
  [DebuggerStepThrough]
  public static bool IsHex(char ch)
  {
    if ((int) ch >= 48 && (int) ch <= 57 || (int) ch >= 97 && (int) ch <= 102)
      return true;
    if ((int) ch >= 65)
      return (int) ch <= 70;
    return false;
  }

  public static bool ParseSymbol(string text, ref int index, BetterList<Color> colors, bool premultiply, ref int sub, ref bool bold, ref bool italic, ref bool underline, ref bool strike, ref bool ignoreColor)
  {
    int length = text.Length;
    if (index + 3 > length || (int) text[index] != 91)
      return false;
    int num1;
    if ((int) text[index + 2] == 93)
    {
      if ((int) text[index + 1] == 45)
      {
        if (colors != null && colors.size > 1)
          colors.RemoveAt(colors.size - 1);
        index += 3;
        return true;
      }
      string key = text.Substring(index, 3);
      if (key != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (NGUIText.\u003C\u003Ef__switch\u0024map8 == null)
        {
          // ISSUE: reference to a compiler-generated field
          NGUIText.\u003C\u003Ef__switch\u0024map8 = new Dictionary<string, int>(5)
          {
            {
              "[b]",
              0
            },
            {
              "[i]",
              1
            },
            {
              "[u]",
              2
            },
            {
              "[s]",
              3
            },
            {
              "[c]",
              4
            }
          };
        }
        // ISSUE: reference to a compiler-generated field
        if (NGUIText.\u003C\u003Ef__switch\u0024map8.TryGetValue(key, out num1))
        {
          switch (num1)
          {
            case 0:
              bold = true;
              index += 3;
              return true;
            case 1:
              italic = true;
              index += 3;
              return true;
            case 2:
              underline = true;
              index += 3;
              return true;
            case 3:
              strike = true;
              index += 3;
              return true;
            case 4:
              ignoreColor = true;
              index += 3;
              return true;
          }
        }
      }
    }
    if (index + 4 > length)
      return false;
    if ((int) text[index + 3] == 93)
    {
      string key = text.Substring(index, 4);
      if (key != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (NGUIText.\u003C\u003Ef__switch\u0024map9 == null)
        {
          // ISSUE: reference to a compiler-generated field
          NGUIText.\u003C\u003Ef__switch\u0024map9 = new Dictionary<string, int>(5)
          {
            {
              "[/b]",
              0
            },
            {
              "[/i]",
              1
            },
            {
              "[/u]",
              2
            },
            {
              "[/s]",
              3
            },
            {
              "[/c]",
              4
            }
          };
        }
        // ISSUE: reference to a compiler-generated field
        if (NGUIText.\u003C\u003Ef__switch\u0024map9.TryGetValue(key, out num1))
        {
          switch (num1)
          {
            case 0:
              bold = false;
              index += 4;
              return true;
            case 1:
              italic = false;
              index += 4;
              return true;
            case 2:
              underline = false;
              index += 4;
              return true;
            case 3:
              strike = false;
              index += 4;
              return true;
            case 4:
              ignoreColor = false;
              index += 4;
              return true;
          }
        }
      }
      char ch1 = text[index + 1];
      char ch2 = text[index + 2];
      if (NGUIText.IsHex(ch1) && NGUIText.IsHex(ch2))
      {
        NGUIText.mAlpha = (float) (NGUIMath.HexToDecimal(ch1) << 4 | NGUIMath.HexToDecimal(ch2)) / (float) byte.MaxValue;
        index += 4;
        return true;
      }
    }
    if (index + 5 > length)
      return false;
    if ((int) text[index + 4] == 93)
    {
      string key = text.Substring(index, 5);
      if (key != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (NGUIText.\u003C\u003Ef__switch\u0024mapA == null)
        {
          // ISSUE: reference to a compiler-generated field
          NGUIText.\u003C\u003Ef__switch\u0024mapA = new Dictionary<string, int>(2)
          {
            {
              "[sub]",
              0
            },
            {
              "[sup]",
              1
            }
          };
        }
        // ISSUE: reference to a compiler-generated field
        if (NGUIText.\u003C\u003Ef__switch\u0024mapA.TryGetValue(key, out num1))
        {
          if (num1 != 0)
          {
            if (num1 == 1)
            {
              sub = 2;
              index += 5;
              return true;
            }
          }
          else
          {
            sub = 1;
            index += 5;
            return true;
          }
        }
      }
    }
    if (index + 6 > length)
      return false;
    if ((int) text[index + 5] == 93)
    {
      string key = text.Substring(index, 6);
      if (key != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (NGUIText.\u003C\u003Ef__switch\u0024mapB == null)
        {
          // ISSUE: reference to a compiler-generated field
          NGUIText.\u003C\u003Ef__switch\u0024mapB = new Dictionary<string, int>(3)
          {
            {
              "[/sub]",
              0
            },
            {
              "[/sup]",
              1
            },
            {
              "[/url]",
              2
            }
          };
        }
        // ISSUE: reference to a compiler-generated field
        if (NGUIText.\u003C\u003Ef__switch\u0024mapB.TryGetValue(key, out num1))
        {
          switch (num1)
          {
            case 0:
              sub = 0;
              index += 6;
              return true;
            case 1:
              sub = 0;
              index += 6;
              return true;
            case 2:
              index += 6;
              return true;
          }
        }
      }
    }
    if ((int) text[index + 1] == 117 && (int) text[index + 2] == 114 && ((int) text[index + 3] == 108 && (int) text[index + 4] == 61))
    {
      int num2 = text.IndexOf(']', index + 4);
      if (num2 != -1)
      {
        index = num2 + 1;
        return true;
      }
      index = text.Length;
      return true;
    }
    if (index + 8 > length)
      return false;
    if ((int) text[index + 7] == 93)
    {
      Color color = NGUIText.ParseColor24(text, index + 1);
      if (NGUIText.EncodeColor24(color) != text.Substring(index + 1, 6).ToUpper())
        return false;
      if (colors != null)
      {
        color.a = colors[colors.size - 1].a;
        if (premultiply && (double) color.a != 1.0)
          color = Color.Lerp(NGUIText.mInvisible, color, color.a);
        colors.Add(color);
      }
      index += 8;
      return true;
    }
    if (index + 10 > length || (int) text[index + 9] != 93)
      return false;
    Color color1 = NGUIText.ParseColor32(text, index + 1);
    if (NGUIText.EncodeColor32(color1) != text.Substring(index + 1, 8).ToUpper())
      return false;
    if (colors != null)
    {
      if (premultiply && (double) color1.a != 1.0)
        color1 = Color.Lerp(NGUIText.mInvisible, color1, color1.a);
      colors.Add(color1);
    }
    index += 10;
    return true;
  }

  public static string StripSymbols(string text)
  {
    if (text != null)
    {
      int startIndex = 0;
      int length = text.Length;
      while (startIndex < length)
      {
        if ((int) text[startIndex] == 91)
        {
          int sub = 0;
          bool bold = false;
          bool italic = false;
          bool underline = false;
          bool strike = false;
          bool ignoreColor = false;
          int index = startIndex;
          if (NGUIText.ParseSymbol(text, ref index, (BetterList<Color>) null, false, ref sub, ref bold, ref italic, ref underline, ref strike, ref ignoreColor))
          {
            text = text.Remove(startIndex, index - startIndex);
            length = text.Length;
            continue;
          }
        }
        ++startIndex;
      }
    }
    return text;
  }

  public static void Align(BetterList<Vector3> verts, int indexOffset, float printedWidth)
  {
    switch (NGUIText.alignment)
    {
      case NGUIText.Alignment.Center:
        float num1 = (float) (((double) NGUIText.rectWidth - (double) printedWidth) * 0.5);
        if ((double) num1 < 0.0)
          break;
        int num2 = Mathf.RoundToInt((float) NGUIText.rectWidth - printedWidth);
        int num3 = Mathf.RoundToInt((float) NGUIText.rectWidth);
        bool flag1 = (num2 & 1) == 1;
        bool flag2 = (num3 & 1) == 1;
        if (flag1 && !flag2 || !flag1 && flag2)
          num1 += 0.5f * NGUIText.fontScale;
        for (int index = indexOffset; index < verts.size; ++index)
          verts.buffer[index].x += num1;
        break;
      case NGUIText.Alignment.Right:
        float num4 = (float) NGUIText.rectWidth - printedWidth;
        if ((double) num4 < 0.0)
          break;
        for (int index = indexOffset; index < verts.size; ++index)
          verts.buffer[index].x += num4;
        break;
      case NGUIText.Alignment.Justified:
        if ((double) printedWidth < (double) NGUIText.rectWidth * 0.649999976158142 || ((double) NGUIText.rectWidth - (double) printedWidth) * 0.5 < 1.0)
          break;
        int num5 = (verts.size - indexOffset) / 4;
        if (num5 < 1)
          break;
        float num6 = 1f / (float) (num5 - 1);
        float num7 = (float) NGUIText.rectWidth / printedWidth;
        int index1 = indexOffset + 4;
        int num8 = 1;
        while (index1 < verts.size)
        {
          float x1 = verts.buffer[index1].x;
          float x2 = verts.buffer[index1 + 2].x;
          float num9 = x2 - x1;
          float a1 = x1 * num7;
          float a2 = a1 + num9;
          float b1 = x2 * num7;
          float b2 = b1 - num9;
          float t = (float) num8 * num6;
          float f1 = Mathf.Lerp(a1, b2, t);
          float f2 = Mathf.Lerp(a2, b1, t);
          float num10 = Mathf.Round(f1);
          float num11 = Mathf.Round(f2);
          Vector3[] buffer1 = verts.buffer;
          int index2 = index1;
          int num12 = index2 + 1;
          buffer1[index2].x = num10;
          Vector3[] buffer2 = verts.buffer;
          int index3 = num12;
          int num13 = index3 + 1;
          buffer2[index3].x = num10;
          Vector3[] buffer3 = verts.buffer;
          int index4 = num13;
          int num14 = index4 + 1;
          buffer3[index4].x = num11;
          Vector3[] buffer4 = verts.buffer;
          int index5 = num14;
          index1 = index5 + 1;
          buffer4[index5].x = num11;
          ++num8;
        }
        break;
    }
  }

  public static int GetExactCharacterIndex(BetterList<Vector3> verts, BetterList<int> indices, Vector2 pos)
  {
    for (int index1 = 0; index1 < indices.size; ++index1)
    {
      int index2 = index1 << 1;
      int index3 = index2 + 1;
      float x1 = verts[index2].x;
      if ((double) pos.x >= (double) x1)
      {
        float x2 = verts[index3].x;
        if ((double) pos.x <= (double) x2)
        {
          float y1 = verts[index2].y;
          if ((double) pos.y >= (double) y1)
          {
            float y2 = verts[index3].y;
            if ((double) pos.y <= (double) y2)
              return indices[index1];
          }
        }
      }
    }
    return 0;
  }

  public static int GetApproximateCharacterIndex(BetterList<Vector3> verts, BetterList<int> indices, Vector2 pos)
  {
    float num1 = float.MaxValue;
    float num2 = float.MaxValue;
    int index1 = 0;
    for (int index2 = 0; index2 < verts.size; ++index2)
    {
      float num3 = Mathf.Abs(pos.y - verts[index2].y);
      if ((double) num3 <= (double) num2)
      {
        float num4 = Mathf.Abs(pos.x - verts[index2].x);
        if ((double) num3 < (double) num2)
        {
          num2 = num3;
          num1 = num4;
          index1 = index2;
        }
        else if ((double) num4 < (double) num1)
        {
          num1 = num4;
          index1 = index2;
        }
      }
    }
    return indices[index1];
  }

  [DebuggerStepThrough]
  [DebuggerHidden]
  private static bool IsSpace(int ch)
  {
    if (ch != 32 && ch != 8202 && ch != 8203)
      return ch == 8201;
    return true;
  }

  [DebuggerHidden]
  [DebuggerStepThrough]
  public static void EndLine(ref StringBuilder s)
  {
    int index = s.Length - 1;
    if (index > 0 && NGUIText.IsSpace((int) s[index]))
      s[index] = '\n';
    else
      s.Append('\n');
  }

  [DebuggerStepThrough]
  [DebuggerHidden]
  private static void ReplaceSpaceWithNewline(ref StringBuilder s)
  {
    int index = s.Length - 1;
    if (index <= 0 || !NGUIText.IsSpace((int) s[index]))
      return;
    s[index] = '\n';
  }

  public static Vector2 CalculatePrintedSize(string text)
  {
    Vector2 zero = Vector2.zero;
    if (!string.IsNullOrEmpty(text))
    {
      if (NGUIText.encoding)
        text = NGUIText.StripSymbols(text);
      NGUIText.Prepare(text);
      float num1 = 0.0f;
      float num2 = 0.0f;
      float num3 = 0.0f;
      int length = text.Length;
      int prev = 0;
      for (int index = 0; index < length; ++index)
      {
        int ch = (int) text[index];
        if (ch == 10)
        {
          if ((double) num1 > (double) num3)
            num3 = num1;
          num1 = 0.0f;
          num2 += NGUIText.finalLineHeight;
        }
        else if (ch >= 32)
        {
          BMSymbol bmSymbol = !NGUIText.useSymbols ? (BMSymbol) null : NGUIText.GetSymbol(text, index, length);
          if (bmSymbol == null)
          {
            float glyphWidth = NGUIText.GetGlyphWidth(ch, prev);
            if ((double) glyphWidth != 0.0)
            {
              float num4 = glyphWidth + NGUIText.finalSpacingX;
              if (Mathf.RoundToInt(num1 + num4) > NGUIText.regionWidth)
              {
                if ((double) num1 > (double) num3)
                  num3 = num1 - NGUIText.finalSpacingX;
                num1 = num4;
                num2 += NGUIText.finalLineHeight;
              }
              else
                num1 += num4;
              prev = ch;
            }
          }
          else
          {
            float num4 = NGUIText.finalSpacingX + (float) bmSymbol.advance * NGUIText.fontScale;
            if (Mathf.RoundToInt(num1 + num4) > NGUIText.regionWidth)
            {
              if ((double) num1 > (double) num3)
                num3 = num1 - NGUIText.finalSpacingX;
              num1 = num4;
              num2 += NGUIText.finalLineHeight;
            }
            else
              num1 += num4;
            index += bmSymbol.sequence.Length - 1;
            prev = 0;
          }
        }
      }
      zero.x = (double) num1 <= (double) num3 ? num3 : num1 - NGUIText.finalSpacingX;
      zero.y = num2 + NGUIText.finalLineHeight;
    }
    return zero;
  }

  public static int CalculateOffsetToFit(string text)
  {
    if (string.IsNullOrEmpty(text) || NGUIText.regionWidth < 1)
      return 0;
    NGUIText.Prepare(text);
    int length1 = text.Length;
    int prev = 0;
    int index1 = 0;
    for (int length2 = text.Length; index1 < length2; ++index1)
    {
      BMSymbol bmSymbol = !NGUIText.useSymbols ? (BMSymbol) null : NGUIText.GetSymbol(text, index1, length1);
      if (bmSymbol == null)
      {
        int ch = (int) text[index1];
        float glyphWidth = NGUIText.GetGlyphWidth(ch, prev);
        if ((double) glyphWidth != 0.0)
          NGUIText.mSizes.Add(NGUIText.finalSpacingX + glyphWidth);
        prev = ch;
      }
      else
      {
        NGUIText.mSizes.Add(NGUIText.finalSpacingX + (float) bmSymbol.advance * NGUIText.fontScale);
        int num = 0;
        for (int index2 = bmSymbol.sequence.Length - 1; num < index2; ++num)
          NGUIText.mSizes.Add(0.0f);
        index1 += bmSymbol.sequence.Length - 1;
        prev = 0;
      }
    }
    float regionWidth = (float) NGUIText.regionWidth;
    int size = NGUIText.mSizes.size;
    while (size > 0 && (double) regionWidth > 0.0)
      regionWidth -= NGUIText.mSizes[--size];
    NGUIText.mSizes.Clear();
    if ((double) regionWidth < 0.0)
      ++size;
    return size;
  }

  public static string GetEndOfLineThatFits(string text)
  {
    int length = text.Length;
    int offsetToFit = NGUIText.CalculateOffsetToFit(text);
    return text.Substring(offsetToFit, length - offsetToFit);
  }

  public static bool WrapText(string text, out string finalText)
  {
    bool needRelayout = false;
    return NGUIText.WrapText(text, out finalText, false, ref needRelayout);
  }

  public static bool IsArabic(char ch)
  {
    bool flag1 = (int) ch >= 1536 && (int) ch <= 1791;
    bool flag2 = (int) ch >= 1872 && (int) ch <= 1919;
    bool flag3 = (int) ch >= 2208 && (int) ch <= 2303;
    bool flag4 = (int) ch >= 64336 && (int) ch <= 65023;
    bool flag5 = (int) ch >= 65136 && (int) ch <= 65279;
    if (!flag1 && !flag2 && (!flag3 && !flag4))
      return flag5;
    return true;
  }

  public static bool WrapText(string text, out string finalText, bool keepCharCount, ref bool needRelayout)
  {
    if (NGUIText.regionWidth < 1 || NGUIText.regionHeight < 1 || (double) NGUIText.finalLineHeight < 1.0)
    {
      finalText = string.Empty;
      return false;
    }
    float num1 = NGUIText.maxLines <= 0 ? (float) NGUIText.regionHeight : Mathf.Min((float) NGUIText.regionHeight, NGUIText.finalLineHeight * (float) NGUIText.maxLines);
    int b = Mathf.FloorToInt(Mathf.Min(NGUIText.maxLines <= 0 ? 1000000f : (float) NGUIText.maxLines, num1 / NGUIText.finalLineHeight) + 0.01f);
    if (b == 0)
    {
      finalText = string.Empty;
      return false;
    }
    if (string.IsNullOrEmpty(text))
      text = " ";
    NGUIText.Prepare(text);
    StringBuilder s = new StringBuilder();
    int length1 = text.Length;
    float f = (float) NGUIText.regionWidth;
    int startIndex = 0;
    int index = 0;
    int num2 = 1;
    int prev = 0;
    bool flag1 = true;
    bool flag2 = true;
    bool flag3 = false;
    bool flag4 = LocalizationManager.CurrentLanguageCode == "ar" && (text.Length > 1 && NGUIText.IsArabic(text[1]));
    needRelayout = false;
    if (flag4)
    {
      if (text.Contains("\n"))
        needRelayout = true;
      else if (!NGUIText.IsOneLine(text))
        needRelayout = true;
    }
    if (needRelayout)
    {
      char[] charArray = text.ToCharArray();
      Array.Reverse((Array) charArray);
      text = new string(charArray);
    }
    for (; index < length1; ++index)
    {
      char ch1 = text[index];
      if ((int) ch1 > 12287)
        flag3 = true;
      if (flag4)
        flag3 = false;
      if (LocalizationManager.CurrentLanguageCode == "th")
        flag3 = true;
      if ((int) ch1 == 10)
      {
        if (num2 != b)
        {
          f = (float) NGUIText.regionWidth;
          if (startIndex < index)
            s.Append(text.Substring(startIndex, index - startIndex + 1));
          else
            s.Append(ch1);
          flag1 = true;
          ++num2;
          startIndex = index + 1;
          prev = 0;
        }
        else
          break;
      }
      else if (NGUIText.encoding && NGUIText.ParseSymbol(text, ref index))
      {
        --index;
      }
      else
      {
        BMSymbol bmSymbol = !NGUIText.useSymbols ? (BMSymbol) null : NGUIText.GetSymbol(text, index, length1);
        float num3;
        if (bmSymbol == null)
        {
          float glyphWidth = NGUIText.GetGlyphWidth((int) ch1, prev);
          if ((double) glyphWidth != 0.0)
            num3 = NGUIText.finalSpacingX + glyphWidth;
          else
            continue;
        }
        else
          num3 = NGUIText.finalSpacingX + (float) bmSymbol.advance * NGUIText.fontScale;
        f -= num3;
        if (NGUIText.IsSpace((int) ch1) && !flag3 && startIndex < index)
        {
          int length2 = index - startIndex + 1;
          if (num2 == b && (double) f <= 0.0 && index < length1)
          {
            char ch2 = text[index];
            if ((int) ch2 < 32 || NGUIText.IsSpace((int) ch2))
              --length2;
          }
          s.Append(text.Substring(startIndex, length2));
          flag1 = false;
          startIndex = index + 1;
        }
        if (Mathf.RoundToInt(f) < 0)
        {
          if (flag1 || num2 == b)
          {
            s.Append(text.Substring(startIndex, Mathf.Max(0, index - startIndex)));
            bool flag5 = NGUIText.IsSpace((int) ch1);
            if (!flag5 && !flag3)
              flag2 = false;
            if (num2++ == b)
            {
              startIndex = index;
              break;
            }
            if (keepCharCount)
              NGUIText.ReplaceSpaceWithNewline(ref s);
            else
              NGUIText.EndLine(ref s);
            flag1 = true;
            if (flag5)
            {
              startIndex = index + 1;
              f = (float) NGUIText.regionWidth;
            }
            else
            {
              startIndex = index;
              f = (float) NGUIText.regionWidth - num3;
            }
            prev = 0;
          }
          else
          {
            flag1 = true;
            f = (float) NGUIText.regionWidth;
            index = startIndex - 1;
            prev = 0;
            if (num2++ != b)
            {
              if (keepCharCount)
              {
                NGUIText.ReplaceSpaceWithNewline(ref s);
                continue;
              }
              NGUIText.EndLine(ref s);
              continue;
            }
            break;
          }
        }
        else
          prev = (int) ch1;
        if (bmSymbol != null)
        {
          index += bmSymbol.length - 1;
          prev = 0;
        }
      }
    }
    if (startIndex < index)
      s.Append(text.Substring(startIndex, index - startIndex));
    finalText = s.ToString();
    if (needRelayout)
    {
      string[] strArray = finalText.Split('\n');
      StringBuilder stringBuilder = new StringBuilder();
      foreach (string str in strArray)
      {
        char[] charArray = str.ToCharArray();
        Array.Reverse((Array) charArray);
        stringBuilder.Append(charArray);
        stringBuilder.Append('\n');
      }
      finalText = stringBuilder.ToString();
    }
    if (!flag2)
      return false;
    if (index != length1)
      return num2 <= Mathf.Min(NGUIText.maxLines, b);
    return true;
  }

  public static bool IsOneLine(string text)
  {
    int prev = 0;
    float regionWidth = (float) NGUIText.regionWidth;
    text = NGUIText.StripSymbols(text);
    for (int index = 0; index < text.Length; ++index)
    {
      float glyphWidth = NGUIText.GetGlyphWidth((int) text[index], prev);
      if ((double) glyphWidth != 0.0)
      {
        float num = NGUIText.finalSpacingX + glyphWidth;
        regionWidth -= num;
        if ((double) regionWidth < 0.0)
          return false;
      }
    }
    return true;
  }

  public static void Print(string text, BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols)
  {
    if (string.IsNullOrEmpty(text))
      return;
    int size1 = verts.size;
    NGUIText.Prepare(text);
    NGUIText.mColors.Add(NGUIText.tint);
    NGUIText.mAlpha = 1f;
    int prev = 0;
    float x1 = 0.0f;
    float num1 = 0.0f;
    float num2 = 0.0f;
    float finalSize = (float) NGUIText.finalSize;
    Color a = NGUIText.tint * NGUIText.gradientBottom;
    Color b = NGUIText.tint * NGUIText.gradientTop;
    Color32 color32_1 = (Color32) NGUIText.tint;
    int length = text.Length;
    Rect rect = new Rect();
    float num3 = 0.0f;
    float num4 = 0.0f;
    float num5 = finalSize * NGUIText.pixelDensity;
    bool flag = false;
    int sub = 0;
    bool bold = false;
    bool italic = false;
    bool underline = false;
    bool strike = false;
    bool ignoreColor = false;
    float num6 = 0.0f;
    if ((UnityEngine.Object) NGUIText.bitmapFont != (UnityEngine.Object) null)
    {
      rect = NGUIText.bitmapFont.uvRect;
      num3 = rect.width / (float) NGUIText.bitmapFont.texWidth;
      num4 = rect.height / (float) NGUIText.bitmapFont.texHeight;
    }
    for (int index1 = 0; index1 < length; ++index1)
    {
      int ch = (int) text[index1];
      float x2 = x1;
      if (ch == 10)
      {
        if ((double) x1 > (double) num2)
          num2 = x1;
        if (NGUIText.alignment != NGUIText.Alignment.Left)
        {
          NGUIText.Align(verts, size1, x1 - NGUIText.finalSpacingX);
          size1 = verts.size;
        }
        x1 = 0.0f;
        num1 += NGUIText.finalLineHeight;
        prev = 0;
      }
      else if (ch < 32)
        prev = ch;
      else if (NGUIText.encoding && NGUIText.ParseSymbol(text, ref index1, NGUIText.mColors, NGUIText.premultiply, ref sub, ref bold, ref italic, ref underline, ref strike, ref ignoreColor))
      {
        Color mColor;
        if (ignoreColor)
        {
          mColor = NGUIText.mColors[NGUIText.mColors.size - 1];
          mColor.a *= NGUIText.mAlpha * NGUIText.tint.a;
        }
        else
        {
          mColor = NGUIText.mColors[NGUIText.mColors.size - 1];
          mColor.a *= NGUIText.mAlpha;
        }
        color32_1 = (Color32) mColor;
        int index2 = 0;
        for (int index3 = NGUIText.mColors.size - 2; index2 < index3; ++index2)
          mColor.a *= NGUIText.mColors[index2].a;
        if (NGUIText.gradient)
        {
          a = NGUIText.gradientBottom * mColor;
          b = NGUIText.gradientTop * mColor;
        }
        --index1;
      }
      else
      {
        BMSymbol bmSymbol = !NGUIText.useSymbols ? (BMSymbol) null : NGUIText.GetSymbol(text, index1, length);
        if (bmSymbol != null)
        {
          float x3 = x1 + (float) bmSymbol.offsetX * NGUIText.fontScale;
          float x4 = x3 + (float) bmSymbol.width * NGUIText.fontScale;
          float y1 = (float) -((double) num1 + (double) bmSymbol.offsetY * (double) NGUIText.fontScale);
          float y2 = y1 - (float) bmSymbol.height * NGUIText.fontScale;
          if (Mathf.RoundToInt(x1 + (float) bmSymbol.advance * NGUIText.fontScale) > NGUIText.regionWidth)
          {
            if ((double) x1 == 0.0)
              return;
            if (NGUIText.alignment != NGUIText.Alignment.Left && size1 < verts.size)
            {
              NGUIText.Align(verts, size1, x1 - NGUIText.finalSpacingX);
              size1 = verts.size;
            }
            x3 -= x1;
            x4 -= x1;
            y2 -= NGUIText.finalLineHeight;
            y1 -= NGUIText.finalLineHeight;
            x1 = 0.0f;
            num1 += NGUIText.finalLineHeight;
            num6 = 0.0f;
          }
          verts.Add(new Vector3(x3, y2));
          verts.Add(new Vector3(x3, y1));
          verts.Add(new Vector3(x4, y1));
          verts.Add(new Vector3(x4, y2));
          x1 += NGUIText.finalSpacingX + (float) bmSymbol.advance * NGUIText.fontScale;
          index1 += bmSymbol.length - 1;
          prev = 0;
          if (uvs != null)
          {
            Rect uvRect = bmSymbol.uvRect;
            float xMin = uvRect.xMin;
            float yMin = uvRect.yMin;
            float xMax = uvRect.xMax;
            float yMax = uvRect.yMax;
            uvs.Add(new Vector2(xMin, yMin));
            uvs.Add(new Vector2(xMin, yMax));
            uvs.Add(new Vector2(xMax, yMax));
            uvs.Add(new Vector2(xMax, yMin));
          }
          if (cols != null)
          {
            if (NGUIText.symbolStyle == NGUIText.SymbolStyle.Colored)
            {
              for (int index2 = 0; index2 < 4; ++index2)
                cols.Add(color32_1);
            }
            else
            {
              Color32 white = (Color32) Color.white;
              white.a = color32_1.a;
              for (int index2 = 0; index2 < 4; ++index2)
                cols.Add(white);
            }
          }
        }
        else
        {
          NGUIText.GlyphInfo glyph1 = NGUIText.GetGlyph(ch, prev);
          if (glyph1 != null)
          {
            prev = ch;
            if (sub != 0)
            {
              glyph1.v0.x *= 0.75f;
              glyph1.v0.y *= 0.75f;
              glyph1.v1.x *= 0.75f;
              glyph1.v1.y *= 0.75f;
              if (sub == 1)
              {
                glyph1.v0.y -= (float) ((double) NGUIText.fontScale * (double) NGUIText.fontSize * 0.400000005960464);
                glyph1.v1.y -= (float) ((double) NGUIText.fontScale * (double) NGUIText.fontSize * 0.400000005960464);
              }
              else
              {
                glyph1.v0.y += (float) ((double) NGUIText.fontScale * (double) NGUIText.fontSize * 0.0500000007450581);
                glyph1.v1.y += (float) ((double) NGUIText.fontScale * (double) NGUIText.fontSize * 0.0500000007450581);
              }
            }
            float x3 = glyph1.v0.x + x1;
            float y1 = glyph1.v0.y - num1;
            float x4 = glyph1.v1.x + x1;
            float y2 = glyph1.v1.y - num1;
            float advance = glyph1.advance;
            if ((double) NGUIText.finalSpacingX < 0.0)
              advance += NGUIText.finalSpacingX;
            if (Mathf.RoundToInt(x1 + advance) > NGUIText.regionWidth)
            {
              if ((double) x1 == 0.0)
                return;
              if (NGUIText.alignment != NGUIText.Alignment.Left && size1 < verts.size)
              {
                NGUIText.Align(verts, size1, x1 - NGUIText.finalSpacingX);
                size1 = verts.size;
              }
              x3 -= x1;
              x4 -= x1;
              y1 -= NGUIText.finalLineHeight;
              y2 -= NGUIText.finalLineHeight;
              x1 = 0.0f;
              num1 += NGUIText.finalLineHeight;
              x2 = 0.0f;
            }
            if (NGUIText.IsSpace(ch))
            {
              if (underline)
                ch = 95;
              else if (strike)
                ch = 45;
            }
            x1 += sub != 0 ? (float) (((double) NGUIText.finalSpacingX + (double) glyph1.advance) * 0.75) : NGUIText.finalSpacingX + glyph1.advance;
            if (!NGUIText.IsSpace(ch))
            {
              if (uvs != null)
              {
                if ((UnityEngine.Object) NGUIText.bitmapFont != (UnityEngine.Object) null)
                {
                  glyph1.u0.x = rect.xMin + num3 * glyph1.u0.x;
                  glyph1.u2.x = rect.xMin + num3 * glyph1.u2.x;
                  glyph1.u0.y = rect.yMax - num4 * glyph1.u0.y;
                  glyph1.u2.y = rect.yMax - num4 * glyph1.u2.y;
                  glyph1.u1.x = glyph1.u0.x;
                  glyph1.u1.y = glyph1.u2.y;
                  glyph1.u3.x = glyph1.u2.x;
                  glyph1.u3.y = glyph1.u0.y;
                }
                int num7 = 0;
                for (int index2 = !bold ? 1 : 4; num7 < index2; ++num7)
                {
                  uvs.Add(glyph1.u0);
                  uvs.Add(glyph1.u1);
                  uvs.Add(glyph1.u2);
                  uvs.Add(glyph1.u3);
                }
              }
              if (cols != null)
              {
                if (glyph1.channel == 0 || glyph1.channel == 15)
                {
                  if (NGUIText.gradient)
                  {
                    float num7 = num5 + glyph1.v0.y / NGUIText.fontScale;
                    float num8 = num5 + glyph1.v1.y / NGUIText.fontScale;
                    float t1 = num7 / num5;
                    float t2 = num8 / num5;
                    NGUIText.s_c0 = (Color32) Color.Lerp(a, b, t1);
                    NGUIText.s_c1 = (Color32) Color.Lerp(a, b, t2);
                    int num9 = 0;
                    for (int index2 = !bold ? 1 : 4; num9 < index2; ++num9)
                    {
                      cols.Add(NGUIText.s_c0);
                      cols.Add(NGUIText.s_c1);
                      cols.Add(NGUIText.s_c1);
                      cols.Add(NGUIText.s_c0);
                    }
                  }
                  else
                  {
                    int num7 = 0;
                    for (int index2 = !bold ? 4 : 16; num7 < index2; ++num7)
                      cols.Add(color32_1);
                  }
                }
                else
                {
                  Color color = (Color) color32_1 * 0.49f;
                  switch (glyph1.channel)
                  {
                    case 1:
                      color.b += 0.51f;
                      break;
                    case 2:
                      color.g += 0.51f;
                      break;
                    case 4:
                      color.r += 0.51f;
                      break;
                    case 8:
                      color.a += 0.51f;
                      break;
                  }
                  Color32 color32_2 = (Color32) color;
                  int num7 = 0;
                  for (int index2 = !bold ? 4 : 16; num7 < index2; ++num7)
                    cols.Add(color32_2);
                }
              }
              if (!bold)
              {
                if (!italic)
                {
                  verts.Add(new Vector3(x3, y1));
                  verts.Add(new Vector3(x3, y2));
                  verts.Add(new Vector3(x4, y2));
                  verts.Add(new Vector3(x4, y1));
                }
                else
                {
                  float num7 = (float) ((double) NGUIText.fontSize * 0.100000001490116 * (((double) y2 - (double) y1) / (double) NGUIText.fontSize));
                  verts.Add(new Vector3(x3 - num7, y1));
                  verts.Add(new Vector3(x3 + num7, y2));
                  verts.Add(new Vector3(x4 + num7, y2));
                  verts.Add(new Vector3(x4 - num7, y1));
                }
              }
              else
              {
                for (int index2 = 0; index2 < 4; ++index2)
                {
                  float num7 = NGUIText.mBoldOffset[index2 * 2];
                  float num8 = NGUIText.mBoldOffset[index2 * 2 + 1];
                  float num9 = !italic ? 0.0f : (float) ((double) NGUIText.fontSize * 0.100000001490116 * (((double) y2 - (double) y1) / (double) NGUIText.fontSize));
                  verts.Add(new Vector3(x3 + num7 - num9, y1 + num8));
                  verts.Add(new Vector3(x3 + num7 + num9, y2 + num8));
                  verts.Add(new Vector3(x4 + num7 + num9, y2 + num8));
                  verts.Add(new Vector3(x4 + num7 - num9, y1 + num8));
                }
              }
              if (underline || strike)
              {
                NGUIText.GlyphInfo glyph2 = NGUIText.GetGlyph(!strike ? 95 : 45, prev);
                if (glyph2 != null)
                {
                  if (uvs != null)
                  {
                    if ((UnityEngine.Object) NGUIText.bitmapFont != (UnityEngine.Object) null)
                    {
                      glyph2.u0.x = rect.xMin + num3 * glyph2.u0.x;
                      glyph2.u2.x = rect.xMin + num3 * glyph2.u2.x;
                      glyph2.u0.y = rect.yMax - num4 * glyph2.u0.y;
                      glyph2.u2.y = rect.yMax - num4 * glyph2.u2.y;
                    }
                    float x5 = (float) (((double) glyph2.u0.x + (double) glyph2.u2.x) * 0.5);
                    int num7 = 0;
                    for (int index2 = !bold ? 1 : 4; num7 < index2; ++num7)
                    {
                      uvs.Add(new Vector2(x5, glyph2.u0.y));
                      uvs.Add(new Vector2(x5, glyph2.u2.y));
                      uvs.Add(new Vector2(x5, glyph2.u2.y));
                      uvs.Add(new Vector2(x5, glyph2.u0.y));
                    }
                  }
                  float y3;
                  float y4;
                  if (flag && strike)
                  {
                    y3 = (float) ((-(double) num1 + (double) glyph2.v0.y) * 0.75);
                    y4 = (float) ((-(double) num1 + (double) glyph2.v1.y) * 0.75);
                  }
                  else
                  {
                    y3 = -num1 + glyph2.v0.y;
                    y4 = -num1 + glyph2.v1.y;
                  }
                  if (bold)
                  {
                    for (int index2 = 0; index2 < 4; ++index2)
                    {
                      float num7 = NGUIText.mBoldOffset[index2 * 2];
                      float num8 = NGUIText.mBoldOffset[index2 * 2 + 1];
                      verts.Add(new Vector3(x2 + num7, y3 + num8));
                      verts.Add(new Vector3(x2 + num7, y4 + num8));
                      verts.Add(new Vector3(x1 + num7, y4 + num8));
                      verts.Add(new Vector3(x1 + num7, y3 + num8));
                    }
                  }
                  else
                  {
                    verts.Add(new Vector3(x2, y3));
                    verts.Add(new Vector3(x2, y4));
                    verts.Add(new Vector3(x1, y4));
                    verts.Add(new Vector3(x1, y3));
                  }
                  if (NGUIText.gradient)
                  {
                    float num7 = num5 + glyph2.v0.y / NGUIText.fontScale;
                    float num8 = num5 + glyph2.v1.y / NGUIText.fontScale;
                    float t1 = num7 / num5;
                    float t2 = num8 / num5;
                    NGUIText.s_c0 = (Color32) Color.Lerp(a, b, t1);
                    NGUIText.s_c1 = (Color32) Color.Lerp(a, b, t2);
                    int num9 = 0;
                    for (int index2 = !bold ? 1 : 4; num9 < index2; ++num9)
                    {
                      cols.Add(NGUIText.s_c0);
                      cols.Add(NGUIText.s_c1);
                      cols.Add(NGUIText.s_c1);
                      cols.Add(NGUIText.s_c0);
                    }
                  }
                  else
                  {
                    int num7 = 0;
                    for (int index2 = !bold ? 4 : 16; num7 < index2; ++num7)
                      cols.Add(color32_1);
                  }
                }
              }
            }
          }
        }
      }
    }
    if (NGUIText.alignment != NGUIText.Alignment.Left && size1 < verts.size)
    {
      NGUIText.Align(verts, size1, x1 - NGUIText.finalSpacingX);
      int size2 = verts.size;
    }
    NGUIText.mColors.Clear();
  }

  public static void PrintApproximateCharacterPositions(string text, BetterList<Vector3> verts, BetterList<int> indices)
  {
    if (string.IsNullOrEmpty(text))
      text = " ";
    NGUIText.Prepare(text);
    float x = 0.0f;
    float num1 = 0.0f;
    float num2 = 0.0f;
    float num3 = (float) ((double) NGUIText.fontSize * (double) NGUIText.fontScale * 0.5);
    int length = text.Length;
    int size = verts.size;
    int prev = 0;
    for (int index = 0; index < length; ++index)
    {
      int ch = (int) text[index];
      verts.Add(new Vector3(x, -num1 - num3));
      indices.Add(index);
      if (ch == 10)
      {
        if ((double) x > (double) num2)
          num2 = x;
        if (NGUIText.alignment != NGUIText.Alignment.Left)
        {
          NGUIText.Align(verts, size, x - NGUIText.finalSpacingX);
          size = verts.size;
        }
        x = 0.0f;
        num1 += NGUIText.finalLineHeight;
        prev = 0;
      }
      else if (ch < 32)
        prev = 0;
      else if (NGUIText.encoding && NGUIText.ParseSymbol(text, ref index))
      {
        --index;
      }
      else
      {
        BMSymbol bmSymbol = !NGUIText.useSymbols ? (BMSymbol) null : NGUIText.GetSymbol(text, index, length);
        if (bmSymbol == null)
        {
          float glyphWidth = NGUIText.GetGlyphWidth(ch, prev);
          if ((double) glyphWidth != 0.0)
          {
            float num4 = glyphWidth + NGUIText.finalSpacingX;
            if (Mathf.RoundToInt(x + num4) > NGUIText.regionWidth)
            {
              if ((double) x == 0.0)
                return;
              if (NGUIText.alignment != NGUIText.Alignment.Left && size < verts.size)
              {
                NGUIText.Align(verts, size, x - NGUIText.finalSpacingX);
                size = verts.size;
              }
              x = num4;
              num1 += NGUIText.finalLineHeight;
            }
            else
              x += num4;
            verts.Add(new Vector3(x, -num1 - num3));
            indices.Add(index + 1);
            prev = ch;
          }
        }
        else
        {
          float num4 = (float) bmSymbol.advance * NGUIText.fontScale + NGUIText.finalSpacingX;
          if (Mathf.RoundToInt(x + num4) > NGUIText.regionWidth)
          {
            if ((double) x == 0.0)
              return;
            if (NGUIText.alignment != NGUIText.Alignment.Left && size < verts.size)
            {
              NGUIText.Align(verts, size, x - NGUIText.finalSpacingX);
              size = verts.size;
            }
            x = num4;
            num1 += NGUIText.finalLineHeight;
          }
          else
            x += num4;
          verts.Add(new Vector3(x, -num1 - num3));
          indices.Add(index + 1);
          index += bmSymbol.sequence.Length - 1;
          prev = 0;
        }
      }
    }
    if (NGUIText.alignment == NGUIText.Alignment.Left || size >= verts.size)
      return;
    NGUIText.Align(verts, size, x - NGUIText.finalSpacingX);
  }

  public static void PrintExactCharacterPositions(string text, BetterList<Vector3> verts, BetterList<int> indices)
  {
    if (string.IsNullOrEmpty(text))
      text = " ";
    NGUIText.Prepare(text);
    float num1 = (float) NGUIText.fontSize * NGUIText.fontScale;
    float x = 0.0f;
    float num2 = 0.0f;
    float num3 = 0.0f;
    int length = text.Length;
    int size = verts.size;
    int prev = 0;
    for (int index = 0; index < length; ++index)
    {
      int ch = (int) text[index];
      if (ch == 10)
      {
        if ((double) x > (double) num3)
          num3 = x;
        if (NGUIText.alignment != NGUIText.Alignment.Left)
        {
          NGUIText.Align(verts, size, x - NGUIText.finalSpacingX);
          size = verts.size;
        }
        x = 0.0f;
        num2 += NGUIText.finalLineHeight;
        prev = 0;
      }
      else if (ch < 32)
        prev = 0;
      else if (NGUIText.encoding && NGUIText.ParseSymbol(text, ref index))
      {
        --index;
      }
      else
      {
        BMSymbol bmSymbol = !NGUIText.useSymbols ? (BMSymbol) null : NGUIText.GetSymbol(text, index, length);
        if (bmSymbol == null)
        {
          float glyphWidth = NGUIText.GetGlyphWidth(ch, prev);
          if ((double) glyphWidth != 0.0)
          {
            float num4 = glyphWidth + NGUIText.finalSpacingX;
            if (Mathf.RoundToInt(x + num4) > NGUIText.regionWidth)
            {
              if ((double) x == 0.0)
                return;
              if (NGUIText.alignment != NGUIText.Alignment.Left && size < verts.size)
              {
                NGUIText.Align(verts, size, x - NGUIText.finalSpacingX);
                size = verts.size;
              }
              x = 0.0f;
              num2 += NGUIText.finalLineHeight;
              prev = 0;
              --index;
            }
            else
            {
              indices.Add(index);
              verts.Add(new Vector3(x, -num2 - num1));
              verts.Add(new Vector3(x + num4, -num2));
              prev = ch;
              x += num4;
            }
          }
        }
        else
        {
          float num4 = (float) bmSymbol.advance * NGUIText.fontScale + NGUIText.finalSpacingX;
          if (Mathf.RoundToInt(x + num4) > NGUIText.regionWidth)
          {
            if ((double) x == 0.0)
              return;
            if (NGUIText.alignment != NGUIText.Alignment.Left && size < verts.size)
            {
              NGUIText.Align(verts, size, x - NGUIText.finalSpacingX);
              size = verts.size;
            }
            x = 0.0f;
            num2 += NGUIText.finalLineHeight;
            prev = 0;
            --index;
          }
          else
          {
            indices.Add(index);
            verts.Add(new Vector3(x, -num2 - num1));
            verts.Add(new Vector3(x + num4, -num2));
            index += bmSymbol.sequence.Length - 1;
            x += num4;
            prev = 0;
          }
        }
      }
    }
    if (NGUIText.alignment == NGUIText.Alignment.Left || size >= verts.size)
      return;
    NGUIText.Align(verts, size, x - NGUIText.finalSpacingX);
  }

  public static void PrintCaretAndSelection(string text, int start, int end, BetterList<Vector3> caret, BetterList<Vector3> highlight)
  {
    if (string.IsNullOrEmpty(text))
      text = " ";
    NGUIText.Prepare(text);
    int num1 = end;
    if (start > end)
    {
      end = start;
      start = num1;
    }
    float x1 = 0.0f;
    float num2 = 0.0f;
    float num3 = 0.0f;
    float num4 = (float) NGUIText.fontSize * NGUIText.fontScale;
    int indexOffset1 = caret == null ? 0 : caret.size;
    int indexOffset2 = highlight == null ? 0 : highlight.size;
    int length = text.Length;
    int index = 0;
    int prev = 0;
    bool flag1 = false;
    bool flag2 = false;
    Vector2 vector2_1 = Vector2.zero;
    Vector2 vector2_2 = Vector2.zero;
    for (; index < length; ++index)
    {
      if (caret != null && !flag2 && num1 <= index)
      {
        flag2 = true;
        caret.Add(new Vector3(x1 - 1f, -num2 - num4));
        caret.Add(new Vector3(x1 - 1f, -num2));
        caret.Add(new Vector3(x1 + 1f, -num2));
        caret.Add(new Vector3(x1 + 1f, -num2 - num4));
      }
      int ch = (int) text[index];
      if (ch == 10)
      {
        if ((double) x1 > (double) num3)
          num3 = x1;
        if (caret != null && flag2)
        {
          if (NGUIText.alignment != NGUIText.Alignment.Left)
            NGUIText.Align(caret, indexOffset1, x1 - NGUIText.finalSpacingX);
          caret = (BetterList<Vector3>) null;
        }
        if (highlight != null)
        {
          if (flag1)
          {
            flag1 = false;
            highlight.Add((Vector3) vector2_2);
            highlight.Add((Vector3) vector2_1);
          }
          else if (start <= index && end > index)
          {
            highlight.Add(new Vector3(x1, -num2 - num4));
            highlight.Add(new Vector3(x1, -num2));
            highlight.Add(new Vector3(x1 + 2f, -num2));
            highlight.Add(new Vector3(x1 + 2f, -num2 - num4));
          }
          if (NGUIText.alignment != NGUIText.Alignment.Left && indexOffset2 < highlight.size)
          {
            NGUIText.Align(highlight, indexOffset2, x1 - NGUIText.finalSpacingX);
            indexOffset2 = highlight.size;
          }
        }
        x1 = 0.0f;
        num2 += NGUIText.finalLineHeight;
        prev = 0;
      }
      else if (ch < 32)
        prev = 0;
      else if (NGUIText.encoding && NGUIText.ParseSymbol(text, ref index))
      {
        --index;
      }
      else
      {
        BMSymbol bmSymbol = !NGUIText.useSymbols ? (BMSymbol) null : NGUIText.GetSymbol(text, index, length);
        float num5 = bmSymbol == null ? NGUIText.GetGlyphWidth(ch, prev) : (float) bmSymbol.advance * NGUIText.fontScale;
        if ((double) num5 != 0.0)
        {
          float x2 = x1;
          float x3 = x1 + num5;
          float y1 = -num2 - num4;
          float y2 = -num2;
          if (Mathf.RoundToInt(x3 + NGUIText.finalSpacingX) > NGUIText.regionWidth)
          {
            if ((double) x1 == 0.0)
              return;
            if ((double) x1 > (double) num3)
              num3 = x1;
            if (caret != null && flag2)
            {
              if (NGUIText.alignment != NGUIText.Alignment.Left)
                NGUIText.Align(caret, indexOffset1, x1 - NGUIText.finalSpacingX);
              caret = (BetterList<Vector3>) null;
            }
            if (highlight != null)
            {
              if (flag1)
              {
                flag1 = false;
                highlight.Add((Vector3) vector2_2);
                highlight.Add((Vector3) vector2_1);
              }
              else if (start <= index && end > index)
              {
                highlight.Add(new Vector3(x1, -num2 - num4));
                highlight.Add(new Vector3(x1, -num2));
                highlight.Add(new Vector3(x1 + 2f, -num2));
                highlight.Add(new Vector3(x1 + 2f, -num2 - num4));
              }
              if (NGUIText.alignment != NGUIText.Alignment.Left && indexOffset2 < highlight.size)
              {
                NGUIText.Align(highlight, indexOffset2, x1 - NGUIText.finalSpacingX);
                indexOffset2 = highlight.size;
              }
            }
            x2 -= x1;
            x3 -= x1;
            y1 -= NGUIText.finalLineHeight;
            y2 -= NGUIText.finalLineHeight;
            x1 = 0.0f;
            num2 += NGUIText.finalLineHeight;
          }
          x1 += num5 + NGUIText.finalSpacingX;
          if (highlight != null)
          {
            if (start > index || end <= index)
            {
              if (flag1)
              {
                flag1 = false;
                highlight.Add((Vector3) vector2_2);
                highlight.Add((Vector3) vector2_1);
              }
            }
            else if (!flag1)
            {
              flag1 = true;
              highlight.Add(new Vector3(x2, y1));
              highlight.Add(new Vector3(x2, y2));
            }
          }
          vector2_1 = new Vector2(x3, y1);
          vector2_2 = new Vector2(x3, y2);
          prev = ch;
        }
      }
    }
    if (caret != null)
    {
      if (!flag2)
      {
        caret.Add(new Vector3(x1 - 1f, -num2 - num4));
        caret.Add(new Vector3(x1 - 1f, -num2));
        caret.Add(new Vector3(x1 + 1f, -num2));
        caret.Add(new Vector3(x1 + 1f, -num2 - num4));
      }
      if (NGUIText.alignment != NGUIText.Alignment.Left)
        NGUIText.Align(caret, indexOffset1, x1 - NGUIText.finalSpacingX);
    }
    if (highlight == null)
      return;
    if (flag1)
    {
      highlight.Add((Vector3) vector2_2);
      highlight.Add((Vector3) vector2_1);
    }
    else if (start < index && end == index)
    {
      highlight.Add(new Vector3(x1, -num2 - num4));
      highlight.Add(new Vector3(x1, -num2));
      highlight.Add(new Vector3(x1 + 2f, -num2));
      highlight.Add(new Vector3(x1 + 2f, -num2 - num4));
    }
    if (NGUIText.alignment == NGUIText.Alignment.Left || indexOffset2 >= highlight.size)
      return;
    NGUIText.Align(highlight, indexOffset2, x1 - NGUIText.finalSpacingX);
  }

  public enum Alignment
  {
    Automatic,
    Left,
    Center,
    Right,
    Justified,
  }

  public enum SymbolStyle
  {
    None,
    Normal,
    Colored,
  }

  public class GlyphInfo
  {
    public Vector2 v0;
    public Vector2 v1;
    public Vector2 u0;
    public Vector2 u1;
    public Vector2 u2;
    public Vector2 u3;
    public float advance;
    public int channel;
  }
}
