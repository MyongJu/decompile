﻿// Decompiled with JetBrains decompiler
// Type: HeroRecruitSlotGold
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class HeroRecruitSlotGold : HeroRecruitSlot
{
  public const string CARD_PATH = "Prefab/UI/Common/ParliamentHeroCard";
  public GameObject m_UnknownCard;
  public GameObject m_EventCard;
  public GameObject m_RootNode;
  public UILabel m_EndTime;
  public UILabel m_OncePrice;
  public UILabel m_TenTimesPrice;
  private GameObject m_HeroCard;
  private ParliamentHeroSummonGroupInfo m_ParliamentHeroSummonGroupInfo;
  private ParliamentHeroInfo m_ParliamentHeroInfo;
  private long m_ActivityEndTime;

  public override void UpdateUI(HeroRecruitData data)
  {
    base.UpdateUI(data);
    this.m_BuyButton.gameObject.SetActive(false);
    this.m_FreeButton.gameObject.SetActive(false);
    this.m_ParliamentHeroSummonGroupInfo = ConfigManager.inst.DB_ParliamentHeroSummonGroup.Get("3");
    this.m_ParliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this.m_ParliamentHeroSummonGroupInfo.activityHero);
    this.m_ActivityEndTime = Utils.DateTime2ServerTime(this.m_ParliamentHeroSummonGroupInfo.activityEndTime, "_");
    this.m_OncePrice.text = this.m_ParliamentHeroSummonGroupInfo.reqItemCount.ToString();
    this.m_OncePrice.color = (long) this.m_ParliamentHeroSummonGroupInfo.reqItemCount > data.GetCurrency() ? Color.red : this.m_CurrencyColor;
    this.m_TenTimesPrice.text = (this.m_ParliamentHeroSummonGroupInfo.reqItemCount * this.m_ParliamentHeroSummonGroupInfo.tenPriceRate).ToString();
    this.m_TenTimesPrice.color = (long) (this.m_ParliamentHeroSummonGroupInfo.reqItemCount * this.m_ParliamentHeroSummonGroupInfo.tenPriceRate) > data.GetCurrency() ? Color.red : this.m_CurrencyColor;
    this.m_TipLabel.text = HeroRecruitUtils.GetRecruitText(3);
    this.UpdateEventCard();
  }

  public void OnRecruitOnce()
  {
    if ((long) this.m_ParliamentHeroSummonGroupInfo.reqItemCount > this.m_HeroRecruitData.GetCurrency())
      this.m_HeroRecruitData.ShowStore(this.m_ParliamentHeroSummonGroupInfo.reqItemCount);
    else
      HeroRecruitUtils.Summon(3, false, (System.Action<bool, HeroRecruitPayload>) ((ret, payload) =>
      {
        if (!ret)
          return;
        List<HeroRecruitPayloadData> payloadData = payload.GetPayloadData();
        UIManager.inst.OpenDlg("HeroCard/HeroRecruitLuckyDrawDialog", (UI.Dialog.DialogParameter) new HeroRecruitLuckyDrawDialog.Parameter()
        {
          heroPayload = payloadData[0],
          showButton = true,
          heroRecruitData = this.m_HeroRecruitData,
          customSummonType = 3,
          customPrice = this.m_ParliamentHeroSummonGroupInfo.reqItemCount
        }, true, true, true);
      }));
  }

  public void OnRecruitTenTimes()
  {
    if ((long) (this.m_ParliamentHeroSummonGroupInfo.reqItemCount * this.m_ParliamentHeroSummonGroupInfo.tenPriceRate) > this.m_HeroRecruitData.GetCurrency())
      this.m_HeroRecruitData.ShowStore(this.m_ParliamentHeroSummonGroupInfo.reqItemCount * this.m_ParliamentHeroSummonGroupInfo.tenPriceRate);
    else
      HeroRecruitUtils.Summon(4, false, (System.Action<bool, HeroRecruitPayload>) ((ret, payload) =>
      {
        if (!ret)
          return;
        List<HeroRecruitPayloadData> payloadData = payload.GetPayloadData();
        UIManager.inst.OpenDlg("HeroCard/HeroRecruitTenLuckyDrawDialog", (UI.Dialog.DialogParameter) new HeroRecruitTenLuckyDrawDialog.Parameter()
        {
          heroPayloads = payloadData,
          showButton = true,
          heroRecruitData = this.m_HeroRecruitData,
          customSummonType = 4,
          customPrice = (this.m_ParliamentHeroSummonGroupInfo.reqItemCount * this.m_ParliamentHeroSummonGroupInfo.tenPriceRate)
        }, true, true, true);
      }));
  }

  private void Update()
  {
    this.UpdateEventCard();
  }

  private void UpdateEventCard()
  {
    bool flag = this.m_ParliamentHeroInfo != null && (long) NetServerTime.inst.ServerTimestamp <= this.m_ActivityEndTime;
    if (flag)
    {
      this.m_EndTime.text = Utils.FormatTime((int) this.m_ActivityEndTime - NetServerTime.inst.ServerTimestamp, true, false, true);
      if (!(bool) ((UnityEngine.Object) this.m_HeroCard))
      {
        this.m_HeroCard = this.CreateParliamentHeroCard();
        this.m_HeroCard.GetComponent<ParliamentHeroCard>().SetData(this.m_ParliamentHeroInfo);
      }
    }
    else
      this.DeleteHeroCard();
    this.m_UnknownCard.SetActive(!flag);
    this.m_EventCard.SetActive(flag);
  }

  private void DeleteHeroCard()
  {
    if (!(bool) ((UnityEngine.Object) this.m_HeroCard))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_HeroCard);
    this.m_HeroCard = (GameObject) null;
  }

  private GameObject CreateParliamentHeroCard()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load("Prefab/UI/Common/ParliamentHeroCard", (System.Type) null)) as GameObject;
    AssetManager.Instance.UnLoadAsset("Prefab/UI/Common/ParliamentHeroCard", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    gameObject.transform.SetParent(this.m_RootNode.transform);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    return gameObject;
  }
}
