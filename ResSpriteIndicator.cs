﻿// Decompiled with JetBrains decompiler
// Type: ResSpriteIndicator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ResSpriteIndicator : SpriteIndicator
{
  private static Color playerColor = new Color(0.0f, 0.8156863f, 0.02745098f, 1f);
  private static Color enemyColor = new Color(0.7921569f, 0.145098f, 0.145098f, 1f);
  private static Color allyColor = Utils.GetColorFromHex(44543U);
  [SerializeField]
  private GameObject m_Sword;

  public static bool CanHaveIndicator(TileData tileData)
  {
    if (tileData.OwnerID != 0L)
    {
      switch (tileData.TileType)
      {
        case TileType.Resource1:
        case TileType.Resource2:
        case TileType.Resource3:
        case TileType.Resource4:
        case TileType.PitMine:
          return true;
      }
    }
    return false;
  }

  public override void UpdateUI(TileData tile)
  {
    if (tile.OwnerID == 0L)
    {
      this.m_Sword.SetActive(false);
      this.gameObject.SetActive(false);
    }
    else
    {
      this.gameObject.SetActive(true);
      if (tile.OwnerID == PlayerData.inst.uid)
      {
        this.m_Sword.SetActive(false);
        this.m_Crystal.color = ResSpriteIndicator.playerColor;
      }
      else if (PlayerData.inst.allianceData != null)
      {
        if (tile.AllianceID == PlayerData.inst.allianceId)
        {
          this.m_Crystal.color = ResSpriteIndicator.allyColor;
          this.m_Sword.SetActive(false);
        }
        else
        {
          this.m_Crystal.color = ResSpriteIndicator.enemyColor;
          this.m_Sword.SetActive(true);
          this.UpdateTipUI(tile);
        }
      }
      else
      {
        this.m_Crystal.color = ResSpriteIndicator.enemyColor;
        this.m_Sword.SetActive(true);
        this.UpdateTipUI(tile);
      }
    }
  }

  private void UpdateTipUI(TileData tile)
  {
    if (!PlayerData.inst.userData.IsForeigner || !AllianceWarPayload.Instance.IsPlayerCurrentJoined(0L) || (tile.UserData == null || tile.UserData.AcGroupId == PlayerData.inst.userData.AcGroupId))
      return;
    NGUITools.SetActive(this.gameObject, false);
  }
}
