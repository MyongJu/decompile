﻿// Decompiled with JetBrains decompiler
// Type: DebugConsole
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DebugConsole : MonoBehaviour
{
  private List<string> commandHistory = new List<string>();
  private Dictionary<string, System.Action<string[]>> commandDictionary = new Dictionary<string, System.Action<string[]>>();
  private Dictionary<string, string> helpStrings = new Dictionary<string, string>();
  private static DebugConsole _instance;
  private int commandHistoryIndex;
  private string unParsedEntry;

  private void Awake()
  {
    if ((UnityEngine.Object) DebugConsole._instance != (UnityEngine.Object) null && (UnityEngine.Object) DebugConsole._instance != (UnityEngine.Object) this)
    {
      Debug.LogError((object) "There should be only one DebugConsole object");
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    }
    else
    {
      DebugConsole._instance = this;
      DebugMenu debugMenu = this.GetComponent<DebugMenu>();
      this.AddToCommandDictionary("-spi", (System.Action<string[]>) (parameters => debugMenu.AddEquipment(parameters[0])), (string) null);
      this.AddToCommandDictionary("help", new System.Action<string[]>(this.ShowCommands), (string) null);
      this.AddToCommandDictionary("?", new System.Action<string[]>(this.ShowCommands), (string) null);
    }
  }

  public static DebugConsole Instance
  {
    get
    {
      if ((UnityEngine.Object) DebugConsole._instance == (UnityEngine.Object) null)
        Debug.LogError((object) "DebugConsole hasn't been created yet.");
      return DebugConsole._instance;
    }
  }

  public string PreviousCommand()
  {
    string empty = string.Empty;
    if (this.commandHistoryIndex >= 0)
    {
      empty = this.commandHistory[this.commandHistoryIndex];
      --this.commandHistoryIndex;
    }
    return empty;
  }

  public string NextCommand()
  {
    string empty = string.Empty;
    if (this.commandHistoryIndex < this.commandHistory.Count - 1)
    {
      ++this.commandHistoryIndex;
      empty = this.commandHistory[this.commandHistoryIndex];
    }
    return empty;
  }

  public void AddToCommandDictionary(string command, System.Action<string[]> action, string help = null)
  {
    if (!this.commandDictionary.ContainsKey(command))
      this.commandDictionary.Add(command, action);
    if (string.IsNullOrEmpty(help))
      return;
    this.helpStrings.Add(command, help);
  }

  private void RecordEntry(string entry)
  {
    this.commandHistoryIndex = this.commandHistory.Count;
    this.commandHistory.Add(entry);
  }

  public void EnterCommand(string entry)
  {
    this.unParsedEntry = entry;
    string str = entry.Trim();
    if (string.IsNullOrEmpty(str))
      return;
    this.RecordEntry(this.unParsedEntry);
    string[] strArray1 = str.Split(' ');
    if (strArray1.Length == 0 || !this.commandDictionary.ContainsKey(strArray1[0]))
      return;
    string[] strArray2 = new string[strArray1.Length - 1];
    for (int index = 1; index < strArray1.Length; ++index)
      strArray2[index - 1] = strArray1[index];
    this.commandDictionary[strArray1[0]](strArray2);
  }

  private void ShowCommands(string[] obj)
  {
    using (Dictionary<string, System.Action<string[]>>.KeyCollection.Enumerator enumerator = this.commandDictionary.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (!this.helpStrings.ContainsKey(enumerator.Current))
          ;
      }
    }
  }
}
