﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightTalentSkillDetailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class DragonKnightTalentSkillDetailPopup : Popup
{
  public UITexture m_SkillIcon;
  public UILabel m_SkillName;
  public UILabel m_SkillDesc;

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    DragonKnightTalentSkillDetailPopup.Parameter parameter = orgParam as DragonKnightTalentSkillDetailPopup.Parameter;
    if (parameter.talentInfo == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_SkillIcon, parameter.talentInfo.imagePath, (System.Action<bool>) null, false, true, string.Empty);
    this.m_SkillName.text = parameter.talentInfo.LocName;
    this.m_SkillDesc.text = parameter.talentInfo.LocDescription;
  }

  public class Parameter : Popup.PopupParameter
  {
    public DragonKnightTalentInfo talentInfo;
  }
}
