﻿// Decompiled with JetBrains decompiler
// Type: AttackShowTroopController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class AttackShowTroopController : MonoBehaviour
{
  public float movetime = 1f;
  public float begintime = 1f;
  public bool hasCavalry = true;
  public bool hasRanged = true;
  public bool hasInfantry = true;
  public bool hasSiege = true;
  public bool needAttack = true;
  public float CITY_RADIUS = 2f;
  public Transform _modelsParent;
  public Transform target;
  public Transform source;
  public bool needInit;
  public bool hasDragon;
  public DragonData.Tendency dragonType;
  private float beginMoveTime;
  public GameObject PathPlayer;
  public GameObject PathAlly;
  public GameObject PathEnemy;
  public AttackShowTroopController.PathType pt;
  [HideInInspector]
  private MarchView _marchView;
  private MarchViewControler.Data md;
  private AttackShowTroopController.TroopState ts;

  private void Start()
  {
    if (!this.needInit)
      return;
    this.Init();
  }

  public void Init()
  {
    GameObject gameObject = new GameObject();
    gameObject.transform.parent = this._modelsParent;
    gameObject.transform.localPosition = Vector3.zero;
    this._marchView = (MarchView) gameObject.AddComponent<ArmyView>();
    gameObject.name = "army";
    this._marchView.Init((System.Action) (() => this.GenerateTroops()));
  }

  private void Update()
  {
    if (this.ts == AttackShowTroopController.TroopState.Move)
    {
      this._marchView.transform.position = Vector3.Lerp(this.md.startPosition, this.md.moveTargetPosition, (Time.time - this.beginMoveTime) / this.movetime);
      if ((double) Time.time - (double) this.beginMoveTime >= (double) this.movetime)
      {
        this._marchView.transform.position = this.md.moveTargetPosition;
        this.ts = !this.needAttack ? AttackShowTroopController.TroopState.None : AttackShowTroopController.TroopState.Action;
      }
    }
    if (this.ts != AttackShowTroopController.TroopState.Action)
      return;
    this.AttackTarget();
    this.ts = AttackShowTroopController.TroopState.None;
  }

  private void GenerateTroops()
  {
    this.md = new MarchViewControler.Data();
    DB.TroopsInfo troopsInfo = new DB.TroopsInfo();
    troopsInfo.troops = new Dictionary<string, Unit>();
    Hashtable troops = AttackShowManager.Instance.Param.troops;
    if (troops != null)
    {
      IEnumerator enumerator = troops.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator.Current.ToString());
        string s = troops[enumerator.Current].ToString();
        int result = 0;
        if (data != null && int.TryParse(s, out result))
        {
          Unit unit = new Unit(data.Type, data.Troop_Tier, result);
          troopsInfo.troops.Add(data.ID, unit);
        }
      }
    }
    this.md.troops = troopsInfo;
    if (AttackShowManager.Instance.Param != null && AttackShowManager.Instance.Param.dragon.isDragonIn)
      this.md.marchData = new MarchData()
      {
        dragon = AttackShowManager.Instance.Param.dragon
      };
    this.md.startPosition = this.source.position;
    this.md.endPosition = this.target.position;
    this.md.moveTargetPosition = this.CalcPoint(this.md.endPosition, -1);
    this.md.startTime = 0.0;
    this.md.endTime = (double) this.movetime;
    double num = (double) this._marchView.Setup(this.md, true, (System.Action) (() =>
    {
      this._marchView.Show();
      this.transform.position = this.md.startPosition;
      this._marchView.Rotate(this.md.dirV3);
      Utils.SetLayer(this._marchView.gameObject, LayerMask.NameToLayer("Actor"));
      Utils.ExecuteInSecs(0.5f, (System.Action) (() => this.ShiftAnimator(false)));
    }));
  }

  public void ShiftAnimator(bool sts)
  {
    if (!AttackShowManager.Instance.IsRunning)
      return;
    Animator[] componentsInChildren = this.GetComponentsInChildren<Animator>();
    if (componentsInChildren == null)
      return;
    foreach (Behaviour behaviour in componentsInChildren)
      behaviour.enabled = sts;
  }

  [DebuggerHidden]
  public IEnumerator BeginMove()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AttackShowTroopController.\u003CBeginMove\u003Ec__Iterator29()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void AttackTarget()
  {
    float radius = 0.0f;
    Vector3 deltaPosition = this.GetDeltaPosition(ref radius);
    AudioManager.Instance.PlaySound("sfx_march_attacking", false);
    this._marchView.Action(0, deltaPosition, radius);
    Utils.SetLayer(this._marchView.gameObject, LayerMask.NameToLayer("Actor"));
  }

  private Vector3 CalcPoint(Vector3 point, int dir)
  {
    float radius = 0.0f;
    Vector3 deltaPosition = this.GetDeltaPosition(ref radius);
    return point + (float) dir * deltaPosition;
  }

  private Vector3 GetDeltaPosition(ref float radius)
  {
    Vector3 vector3 = this.md.endPosition - this.md.startPosition;
    vector3.Normalize();
    radius = this.CITY_RADIUS;
    return vector3 * radius;
  }

  public void CreatePath()
  {
    GameObject go = (GameObject) null;
    if (this.pt == AttackShowTroopController.PathType.Player)
      go = MapUtils.Instantiate("Prefab/Kingdom/MarchPathPlayer");
    else if (this.pt == AttackShowTroopController.PathType.Ally)
      go = MapUtils.Instantiate("Prefab/Kingdom/MarchPathAlly");
    else if (this.pt == AttackShowTroopController.PathType.Enemy)
      go = !SettingManager.Instance.IsDaltonOff ? MapUtils.Instantiate("Prefab/Kingdom/PathEnemyDalton") : MapUtils.Instantiate("Prefab/Kingdom/MarchPathEnemy");
    else if (this.pt == AttackShowTroopController.PathType.None)
      return;
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      return;
    MarchPathScroll component = go.GetComponent<MarchPathScroll>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null || !(bool) ((UnityEngine.Object) this.gameObject))
      return;
    component.SetPath(this.md.startPosition, this.md.endPosition, this.transform);
    Utils.SetLayer(go, this.gameObject.layer);
  }

  public void ReleaseTroops()
  {
    if (!((UnityEngine.Object) this._marchView != (UnityEngine.Object) null) || !((UnityEngine.Object) this._marchView.gameObject != (UnityEngine.Object) null))
      return;
    this._marchView.Dispose();
    UnityEngine.Object.Destroy((UnityEngine.Object) this._marchView.gameObject);
    this._marchView = (MarchView) null;
  }

  private enum TroopState
  {
    None,
    Move,
    Action,
  }

  public enum PathType
  {
    None,
    Player,
    Ally,
    Enemy,
  }
}
