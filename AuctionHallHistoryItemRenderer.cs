﻿// Decompiled with JetBrains decompiler
// Type: AuctionHallHistoryItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class AuctionHallHistoryItemRenderer : MonoBehaviour
{
  public UILabel historyItem;
  public UISprite dividingLine;
  private AuctionHistoryItemInfo _historyItemInfo;
  private bool _showDividingLine;

  public void SetData(AuctionHistoryItemInfo itemInfo, bool showDividingLine = true)
  {
    if (itemInfo == null)
      return;
    this._historyItemInfo = itemInfo;
    this._showDividingLine = showDividingLine;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    string Term = string.Empty;
    switch (this._historyItemInfo.HistoryType)
    {
      case AuctionHelper.AuctionHistoryType.OTHER_WIN:
        Term = "exchange_auction_history_bid_lose";
        break;
      case AuctionHelper.AuctionHistoryType.AUCTION_OTHER_WIN_RETURN:
        Term = "exchange_black_market_history_bid_lose";
        break;
      case AuctionHelper.AuctionHistoryType.YOU_WIN:
        Term = "exchange_auction_history_bid_win";
        break;
      case AuctionHelper.AuctionHistoryType.AUCTION_HIGHER_PRICE_RETURN:
        Term = "exchange_auction_history_bid_failed";
        break;
      case AuctionHelper.AuctionHistoryType.AUCTION_BLACK_MARKET_YOU_WIN:
        Term = "exchange_black_market_history_bid_win";
        break;
    }
    this.historyItem.text = ScriptLocalization.GetWithPara(Term, this._historyItemInfo.HistoryParams, true);
    NGUITools.SetActive(this.dividingLine.gameObject, this._showDividingLine);
  }
}
