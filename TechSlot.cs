﻿// Decompiled with JetBrains decompiler
// Type: TechSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class TechSlot : MonoBehaviour
{
  private List<TechLevel> _techLevels = new List<TechLevel>();
  private const float ROTATE_SPEED = 20f;
  [HideInInspector]
  public string techID;
  public Tech tech;
  public UILabel title;
  public UILabel rankLabel;
  public UITexture techIcon;
  public GameObject progress;
  public UISprite[] AnimSprites;
  public UILabel leftTime;
  private TechLevel _techLevel;
  private JobHandle currentJobHandle;
  private UISlider progressSlider;

  private void OnEnable()
  {
  }

  private void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
  }

  public void OnButtonPressed()
  {
    if (this._techLevel == null)
      return;
    if (this._techLevel.IsCompleted)
      UIManager.inst.OpenPopup("ResearchFullLevelPopup", (Popup.PopupParameter) new ResearchingStatePopup.Parameter()
      {
        techLevel = this._techLevel
      });
    else if (ResearchManager.inst.CurrentResearch != null && ResearchManager.inst.CurrentResearch.InternalID == this._techLevel.InternalID)
      UIManager.inst.OpenPopup("ResearchingStatePopup", (Popup.PopupParameter) new ResearchingStatePopup.Parameter()
      {
        techLevel = this._techLevel
      });
    else
      UIManager.inst.OpenDlg("Research/ResearchDetailDlg", (UI.Dialog.DialogParameter) new ResearchDetailDlg.Parameter()
      {
        techLevelInternalID = this._techLevel.InternalID
      }, true, true, true);
  }

  public int Row
  {
    get
    {
      return this.tech.Row;
    }
  }

  public int Col
  {
    get
    {
      return this.tech.Tier;
    }
  }

  public TechLevel CurrentTechLevel
  {
    get
    {
      return this._techLevel;
    }
  }

  public void UpdateUI()
  {
    if (!this.gameObject.activeSelf)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.techIcon, this.tech.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    if (this._techLevels.Count == 0)
      this._techLevels = ResearchManager.inst.TechLevelList(this.tech.InternalID);
    if (this._techLevels.Count == 0)
    {
      D.warn((object) "No such tech: {0}", (object) this.techID);
      this.gameObject.SetActive(false);
    }
    else
    {
      TechLevel techLevel = (TechLevel) null;
      for (int index = 0; index < this._techLevels.Count; ++index)
      {
        if (this._techLevels[index].IsInProgress)
        {
          techLevel = this._techLevels[index];
          break;
        }
      }
      if (techLevel != null)
      {
        this.rankLabel.text = "Researching...";
        this._techLevel = techLevel;
      }
      else
      {
        this._techLevel = (TechLevel) null;
        for (int index = 0; index < this._techLevels.Count; ++index)
        {
          if (this._techLevels[index].IsAvailable)
          {
            this._techLevel = this._techLevels[index];
            break;
          }
        }
        if (this._techLevel == null)
          this._techLevel = this._techLevels[this._techLevels.Count - 1];
      }
      this.rankLabel.text = string.Format("{0}/{1}", (object) ResearchManager.inst.Rank(this._techLevel.Tech), (object) this._techLevels.Count);
      this.title.text = this._techLevel.Name;
      if (!this._techLevel.PlayerHasReqTechLevels)
        GreyUtility.Grey(this.gameObject);
      else
        GreyUtility.Normal(this.gameObject);
      this.UpdateProgress();
    }
  }

  private void UpdateProgress()
  {
    if (this._techLevel.IsInProgress)
    {
      this.progress.SetActive(true);
      this.currentJobHandle = JobManager.Instance.GetJob(ResearchManager.inst.GetJobId(this._techLevel));
      this.progressSlider = this.progress.GetComponent<UISlider>();
      Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
      this.OnSecondHandler(0);
      this.StartCoroutine(this.PlayAnimSprite());
    }
    else
      this.progress.SetActive(false);
  }

  [DebuggerHidden]
  protected IEnumerator PlayAnimSprite()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TechSlot.\u003CPlayAnimSprite\u003Ec__Iterator94()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void OnSecondHandler(int timeStamp)
  {
    this.progressSlider.value = (float) (1.0 - (double) this.currentJobHandle.LeftTime() / (double) this.currentJobHandle.Duration());
    this.leftTime.text = Utils.FormatTime(this.currentJobHandle.LeftTime(), true, false, true);
  }
}
