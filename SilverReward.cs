﻿// Decompiled with JetBrains decompiler
// Type: SilverReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class SilverReward : QuestReward
{
  public readonly long value;

  public SilverReward(long siliverCount)
  {
    this.value = siliverCount;
  }

  public override void Claim()
  {
    base.Claim();
  }

  public override string GetRewardIconName()
  {
    return "silver_icon";
  }

  public override string GetRewardTypeName()
  {
    return ScriptLocalization.Get("id_silver", true);
  }

  public override string GetValueText()
  {
    return Utils.FormatShortThousandsLong(this.value);
  }

  public override int GetValue()
  {
    return (int) this.value;
  }
}
