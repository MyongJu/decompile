﻿// Decompiled with JetBrains decompiler
// Type: DungeonAncientStoreDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DungeonAncientStoreDialog : UI.Dialog
{
  private List<GameObject> _items = new List<GameObject>();
  public UITexture _currencyIcon;
  public UILabel _currencyCount;
  public UILabel _title;
  public UIScrollView _itemScrollView;
  public UIGrid _itemGrid;
  public GameObject _itemPrefab;
  public UIScrollView _categoryScrollView;
  public UITable _categoryTable;
  public GameObject _categoryPrefab;
  private DungeonAncientStoreDialog.Parameter _param;
  private ShopCommonGroup _group;
  private List<ShopCommonCategory> _categories;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemChanged);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnItemChanged);
    ShopCommonPayload.Instance.OnShopItemStatusChanged += new System.Action(this.OnShopItemStatusChanged);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    this._param = orgParam as DungeonAncientStoreDialog.Parameter;
    this._group = ConfigManager.inst.DB_ShopCommonGroup.Get(this._param.group);
    this._categories = ConfigManager.inst.DB_ConfigShopCommonCategory.GetByGroupID(this._group.internalId);
    this._title.text = Utils.XLAT(this._param.titleKey);
    this.UpdateUI();
    this.UpdateItemStatus();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemChanged);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnItemChanged);
    ShopCommonPayload.Instance.OnShopItemStatusChanged -= new System.Action(this.OnShopItemStatusChanged);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    this.Clear();
  }

  private void OnItemChanged(int itemId)
  {
    this.UpdateCurrency();
    this.UpdateItems();
  }

  private void UpdateItems()
  {
    using (List<GameObject>.Enumerator enumerator = this._items.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.GetComponent<DungeonStoreRenderer>().UpdateUI();
    }
  }

  private void OnSecond(int time)
  {
    ShopCommonPayload.Instance.SecondCheckShopStatus();
  }

  private void OnShopItemStatusChanged()
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) this)
      return;
    this.UpdateItems();
  }

  private void UpdateItemStatus()
  {
    ShopCommonPayload.Instance.UpdateShopItemStatus((System.Action<bool, object>) null);
  }

  private void UpdateCurrency()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._param.currency);
    int itemCount = ItemBag.Instance.GetItemCount(itemStaticInfo.internalId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this._currencyIcon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, true, string.Empty);
    this._currencyCount.text = Utils.FormatThousands(itemCount.ToString());
    this._currencyCount.color = itemCount <= 0 ? Color.red : Color.white;
  }

  private void UpdateUI()
  {
    this.UpdateCurrency();
    this.UpdateCategory();
  }

  private void UpdateCategory()
  {
    this._categories.Insert(0, (ShopCommonCategory) null);
    for (int index = 0; index < this._categories.Count; ++index)
    {
      GameObject gameObject = Utils.DuplicateGOB(this._categoryPrefab, this._categoryTable.transform);
      gameObject.SetActive(true);
      DungeonStoreCategoryToggle component = gameObject.GetComponent<DungeonStoreCategoryToggle>();
      component.SetData(this._categories[index], new System.Action<DungeonStoreCategoryToggle>(this.OnCategorySelected));
      if (index == 0)
        component.Selected = true;
    }
    this._categoryTable.Reposition();
    this._categoryScrollView.ResetPosition();
  }

  private void OnCategorySelected(DungeonStoreCategoryToggle toggle)
  {
    ShopCommonCategory shopCategory = toggle.ShopCategory;
    this.UpdateList(shopCategory != null ? ConfigManager.inst.DB_ShopCommonMain.GetShopData(this._group.internalId, shopCategory.internalId) : ConfigManager.inst.DB_ShopCommonMain.GetShopData(this._group.internalId));
  }

  private void Clear()
  {
    for (int index = 0; index < this._items.Count; ++index)
    {
      DungeonStoreRenderer component = this._items[index].GetComponent<DungeonStoreRenderer>();
      if ((bool) ((UnityEngine.Object) component))
        component.ClearData();
      this._items[index].transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this._items[index]);
    }
    this._items.Clear();
  }

  private void UpdateList(List<ShopCommonMain> store)
  {
    this.Clear();
    store.Sort((Comparison<ShopCommonMain>) ((x, y) =>
    {
      int num1 = (!x.MeetCondition ? 1 : 0) - (!y.MeetCondition ? 1 : 0);
      if (num1 != 0)
        return num1;
      int num2 = x.conditionKey.CompareTo(y.conditionKey);
      if (num2 != 0)
        return num2;
      int num3 = x.conditionValue.CompareTo(y.conditionValue);
      if (num3 != 0)
        return num3;
      return x.priority - y.priority;
    }));
    for (int index = 0; index < store.Count; ++index)
    {
      GameObject gameObject = Utils.DuplicateGOB(this._itemPrefab, this._itemGrid.transform);
      gameObject.SetActive(true);
      gameObject.GetComponent<DungeonStoreRenderer>().SetData(store[index]);
      this._items.Add(gameObject);
    }
    this._itemGrid.Reposition();
    this._itemScrollView.ResetPosition();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public string group;
    public string currency;
    public string titleKey;
  }
}
