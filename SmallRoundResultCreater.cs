﻿// Decompiled with JetBrains decompiler
// Type: SmallRoundResultCreater
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class SmallRoundResultCreater
{
  public IRound currentRound;

  public List<RoundResult> CreateSmallRoundResults(RoundPlayer trigger, List<RoundPlayer> targets)
  {
    this.SmallRoundReady(trigger, targets);
    List<RoundResult> roundResultList = new List<RoundResult>();
    RoundResult rebound = (RoundResult) null;
    if (!this.IsUseSkill(trigger))
    {
      for (int index = 0; index < targets.Count; ++index)
      {
        RoundResult roundResult = this.NormalAttack(trigger, targets[index]);
        if (index == 0 && targets[index].Rebund > 0)
        {
          rebound = new RoundResult();
          rebound.Target = trigger;
          rebound.Type = RoundResult.ResultType.Rebound;
          rebound.Value = -targets[index].Rebund;
        }
        if (roundResult != null)
          roundResultList.Add(roundResult);
      }
    }
    else
      roundResultList = this.UseSkill(trigger, targets, ref rebound);
    if (rebound != null)
      roundResultList.Add(rebound);
    return roundResultList;
  }

  private bool IsUseSkill(RoundPlayer trigger)
  {
    return trigger.SkillId > 0;
  }

  private void SmallRoundReady(RoundPlayer trigger, List<RoundPlayer> targets)
  {
    trigger.SmallRoundStart();
    for (int index = 0; index < targets.Count; ++index)
      targets[index].SmallRoundStart();
  }

  private List<RoundResult> UseSkill(RoundPlayer trigger, List<RoundPlayer> targets, ref RoundResult rebound)
  {
    SmallRoundSpell smallRoundSpell = SmallRoundSpell.Create((long) trigger.SkillId);
    smallRoundSpell.iRound = this.currentRound;
    rebound = smallRoundSpell.GetRebound(trigger, targets);
    return smallRoundSpell.Spell(trigger, targets);
  }

  private RoundResult NormalAttack(RoundPlayer trigger, RoundPlayer underAttacker)
  {
    int num = DamgeCalcUtils.CalcPhysicsDamge(trigger.Damge, underAttacker.Defense, 1.0);
    RoundResult roundResult = new RoundResult();
    roundResult.Target = underAttacker;
    roundResult.Type = RoundResult.ResultType.NormalDamage;
    roundResult.Value = num;
    trigger.SetHurt(-underAttacker.Rebund);
    underAttacker.SetHurt(roundResult.Value);
    BattleManager.Instance.AddRoundLogData(this.currentRound, new Hashtable()
    {
      {
        (object) "type",
        (object) "normal"
      },
      {
        (object) "damge",
        (object) trigger.Damge
      },
      {
        (object) "defense",
        (object) underAttacker.Defense
      },
      {
        (object) "value",
        (object) num
      },
      {
        (object) "rebund",
        (object) -underAttacker.Rebund
      }
    });
    return roundResult;
  }
}
