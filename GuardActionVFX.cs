﻿// Decompiled with JetBrains decompiler
// Type: GuardActionVFX
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UnityEngine;

public class GuardActionVFX : GuardAction
{
  public string URI;
  public string XPath;
  public Vector3 Position;
  public Vector3 Rotation;
  public Vector3 Scale;
  private GameObject _attachPoint;

  public override void Decode(GameObject go, object orgData)
  {
    base.Decode(go, orgData);
    Hashtable inData = orgData as Hashtable;
    DatabaseTools.UpdateData(inData, "URI", ref this.URI);
    DatabaseTools.UpdateData(inData, "XPath", ref this.XPath);
    this.Position = GuardVFXHelper.ParseVector3(inData[(object) "Position"]);
    this.Rotation = GuardVFXHelper.ParseVector3(inData[(object) "Rotation"]);
    this.Scale = GuardVFXHelper.ParseVector3(inData[(object) "Scale"]);
    this._attachPoint = GuardVFXHelper.GetAttachPoint(go, this.XPath);
  }

  public override void Process(RoundPlayer player)
  {
    base.Process(player);
    GuardVFXHelper.PlayVFX(this.URI, this.Position, this.Rotation, this.Scale, this._attachPoint.transform);
  }
}
