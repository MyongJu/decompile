﻿// Decompiled with JetBrains decompiler
// Type: ItemIconRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ItemIconRenderer : MonoBehaviour
{
  public UITexture itemTexture;
  public UISprite normalItemFrame;
  public UISprite equipmentScrollItemFrame;
  public UITexture equipmentItemFrame;
  private int _itemId;
  private AuctionHelper.AuctionItemType _itemType;
  private bool _useWidgetSetting;
  private string _frameImagePrefix;

  public int ItemId
  {
    get
    {
      return this._itemId;
    }
    set
    {
      this.SetData(this._itemId = value, string.Empty, false);
    }
  }

  public void SetData(int itemId, string frameImagePrefix = "", bool useWidgetSetting = false)
  {
    this.HideItemFrames();
    this._frameImagePrefix = frameImagePrefix;
    this._useWidgetSetting = useWidgetSetting;
    ItemStaticInfo itemStaticInfo = (ItemStaticInfo) null;
    this._itemType = AuctionHelper.Instance.GetAuctionItemType(itemId);
    this._itemId = itemId;
    switch (this._itemType)
    {
      case AuctionHelper.AuctionItemType.PROPS:
      case AuctionHelper.AuctionItemType.EQUIPMENT:
      case AuctionHelper.AuctionItemType.EQUIPMENT_SCROLL:
      case AuctionHelper.AuctionItemType.DK_EQUIPMENT:
      case AuctionHelper.AuctionItemType.DK_EQUIPMENT_SCROLL:
        itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
        break;
    }
    if (itemStaticInfo != null)
    {
      if ((UnityEngine.Object) this.itemTexture.mainTexture == (UnityEngine.Object) null || this.itemTexture.mainTexture.name != itemStaticInfo.Image)
      {
        BuilderFactory instance = BuilderFactory.Instance;
        bool useWidgetSetting1 = this._useWidgetSetting;
        UITexture itemTexture = this.itemTexture;
        string imagePath = itemStaticInfo.ImagePath;
        // ISSUE: variable of the null type
        __Null local = null;
        int num1 = 1;
        int num2 = useWidgetSetting1 ? 1 : 0;
        string empty = string.Empty;
        instance.HandyBuild((UIWidget) itemTexture, imagePath, (System.Action<bool>) local, num1 != 0, num2 != 0, empty);
      }
      BagType bagType = BagType.Hero;
      switch (this._itemType)
      {
        case AuctionHelper.AuctionItemType.EQUIPMENT:
        case AuctionHelper.AuctionItemType.EQUIPMENT_SCROLL:
          bagType = BagType.Hero;
          break;
        case AuctionHelper.AuctionItemType.DK_EQUIPMENT:
        case AuctionHelper.AuctionItemType.DK_EQUIPMENT_SCROLL:
          bagType = BagType.DragonKnight;
          break;
      }
      if (this._itemType == AuctionHelper.AuctionItemType.EQUIPMENT || this._itemType == AuctionHelper.AuctionItemType.DK_EQUIPMENT)
        this.FeedEquipmentData(itemStaticInfo.ID + "_0", bagType);
      else if (this._itemType == AuctionHelper.AuctionItemType.EQUIPMENT_SCROLL || this._itemType == AuctionHelper.AuctionItemType.DK_EQUIPMENT_SCROLL)
      {
        ConfigEquipmentScrollInfo dataByItemId = ConfigManager.inst.GetEquipmentScroll(bagType).GetDataByItemID(itemStaticInfo.internalId);
        if (dataByItemId != null)
        {
          ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(dataByItemId.equipmenID);
          if (data != null)
          {
            this.FeedEquipmentData(data.ID + "_0", bagType);
            NGUITools.SetActive(this.equipmentScrollItemFrame.gameObject, true);
            BuilderFactory instance = BuilderFactory.Instance;
            bool useWidgetSetting1 = this._useWidgetSetting;
            UITexture itemTexture = this.itemTexture;
            string path = dataByItemId.ImagePath(bagType);
            // ISSUE: variable of the null type
            __Null local = null;
            int num1 = 1;
            int num2 = useWidgetSetting1 ? 1 : 0;
            string empty = string.Empty;
            instance.HandyBuild((UIWidget) itemTexture, path, (System.Action<bool>) local, num1 != 0, num2 != 0, empty);
            this.itemTexture.transform.localScale = new Vector3(0.72f, 0.72f, 1f);
          }
        }
      }
    }
    if (!string.IsNullOrEmpty(this._frameImagePrefix))
      return;
    NGUITools.SetActive(this.normalItemFrame.gameObject, false);
    NGUITools.SetActive(this.equipmentItemFrame.gameObject, true);
    Utils.SetItemBackground(this.equipmentItemFrame, this._itemId);
  }

  public void SetData(string texturePath)
  {
    this.HideItemFrames();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.itemTexture, texturePath, (System.Action<bool>) null, true, true, string.Empty);
  }

  private void FeedEquipmentData(string equipmentId, BagType bagType)
  {
    ConfigEquipmentPropertyInfo data = ConfigManager.inst.GetEquipmentProperty(bagType).GetData(equipmentId);
    if (data == null)
      return;
    EquipmentComponentData equipmentComponentData = new EquipmentComponentData(data.equipID, 0, bagType, 0L);
    if (equipmentComponentData == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.equipmentItemFrame, equipmentComponentData.equipmentInfo.GetQualityImagePathByPrefix(this._frameImagePrefix), (System.Action<bool>) null, true, false, string.Empty);
    NGUITools.SetActive(this.equipmentItemFrame.gameObject, true);
  }

  public void OnPressHandler()
  {
    Utils.DelayShowTip(this._itemId, this.transform, 0L, 0L, 0);
  }

  public void OnReleaseHandler()
  {
    Utils.StopShowItemTip();
  }

  public void OnClickHandler()
  {
    Utils.ShowItemTip(this._itemId, this.transform, 0L, 0L, 0);
  }

  private void HideItemFrames()
  {
    NGUITools.SetActive(this.equipmentItemFrame.gameObject, false);
    NGUITools.SetActive(this.equipmentScrollItemFrame.gameObject, false);
  }
}
