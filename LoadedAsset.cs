﻿// Decompiled with JetBrains decompiler
// Type: LoadedAsset
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LoadedAsset
{
  public string name;
  public System.Type type;
  private int refCount;
  private UnityEngine.Object objRef;
  public System.Action<string> onAssetUnLoad;

  public LoadedAsset(string name, System.Type type, UnityEngine.Object obj)
  {
    this.name = name;
    this.type = type;
    this.objRef = obj;
  }

  public UnityEngine.Object ObjRef
  {
    get
    {
      return this.objRef;
    }
    set
    {
      this.objRef = value;
    }
  }

  public int RefCount
  {
    get
    {
      return this.refCount;
    }
    set
    {
      this.refCount = value;
      if (this.refCount != 0)
        return;
      if (this.NeedUnloadAsset(this.type))
        Resources.UnloadAsset(this.objRef);
      this.objRef = (UnityEngine.Object) null;
      this.onAssetUnLoad(this.name);
      this.name = (string) null;
    }
  }

  private bool IsPrefab(System.Type type)
  {
    return type == typeof (GameObject);
  }

  private bool NeedUnloadAsset(System.Type assetType)
  {
    return assetType == typeof (Texture) || assetType == typeof (Texture2D);
  }
}
