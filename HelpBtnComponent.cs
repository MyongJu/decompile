﻿// Decompiled with JetBrains decompiler
// Type: HelpBtnComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class HelpBtnComponent : MonoBehaviour
{
  public string ID = "Default";
  public bool useDefaultClickHandle = true;

  public void OnClick()
  {
    if (!this.useDefaultClickHandle)
      return;
    UIManager.inst.OpenPopup("HelpPopup", (Popup.PopupParameter) new HelpPopup.Parameter()
    {
      id = this.ID
    });
  }
}
