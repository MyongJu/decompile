﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillTipItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class DragonSkillTipItem : MonoBehaviour
{
  [SerializeField]
  private float _showTime = 4f;
  [SerializeField]
  private UITexture _icon;
  [SerializeField]
  private UILabel _name;
  [SerializeField]
  private UILabel _desc;

  public void SetSkillId(int skillId)
  {
    ConfigDragonSkillGroupInfo skillGroupBySkillId = ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupBySkillId(skillId);
    if (skillGroupBySkillId == null)
    {
      D.error((object) ("can not find skill group info for skill id: " + (object) skillId));
    }
    else
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this._icon, skillGroupBySkillId.IconPath, (System.Action<bool>) null, true, false, string.Empty);
      this._name.text = Utils.XLAT(skillGroupBySkillId.localization_name);
      this._desc.text = Utils.XLAT(skillGroupBySkillId.localization_description);
    }
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
    this.StopAllCoroutines();
    this.StartCoroutine(this.AutoDisableCoroutine(this._showTime));
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  [DebuggerHidden]
  private IEnumerator AutoDisableCoroutine(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DragonSkillTipItem.\u003CAutoDisableCoroutine\u003Ec__Iterator54()
    {
      delay = delay,
      \u003C\u0024\u003Edelay = delay,
      \u003C\u003Ef__this = this
    };
  }
}
