﻿// Decompiled with JetBrains decompiler
// Type: PopupWorldSearchCoordinates
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UI;
using UnityEngine;

public class PopupWorldSearchCoordinates : MonoBehaviour
{
  public UIInput KInput;
  public UIButton GotoButton;
  public System.Action<int> onGotoK;

  private void Start()
  {
  }

  private void OnDestroy()
  {
  }

  public void OpenDialog(Hashtable param = null)
  {
    this.KInput.value = param[(object) "curKing"].ToString();
    this.gameObject.SetActive(true);
  }

  public void OnCloseButtonPressed()
  {
    this.Close();
  }

  public void OnBackButtonPressed()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnGotoButtonPressed()
  {
    int int32 = Convert.ToInt32(this.KInput.value);
    if (int32 < 0 || int32 > (int) PVPMapData.MapData.TilesPerKingdom.x)
    {
      this.KInput.isSelected = true;
    }
    else
    {
      this.Close();
      if (this.onGotoK == null)
        return;
      this.onGotoK(int32);
    }
  }

  private void Close()
  {
    this.gameObject.SetActive(false);
  }
}
