﻿// Decompiled with JetBrains decompiler
// Type: ItemTipBaseInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ItemTipBaseInfo : MonoBehaviour
{
  public UITexture icon;
  public UITexture background;
  public UILabel itemName;
  public UILabel itemDesc;
  protected int _itemId;

  public virtual void SetData(int itemId)
  {
    this._itemId = itemId;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    this.itemName.text = itemStaticInfo.LocName;
    this.itemDesc.text = itemStaticInfo.LocDescription;
    EquipmentManager.Instance.ConfigQualityLabelWithColor(this.itemName, itemStaticInfo.Quality);
    BuilderFactory.Instance.Build((UIWidget) this.icon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.background, Utils.GetQualityImagePath(itemStaticInfo.Quality), (System.Action<bool>) null, true, false, true, string.Empty);
  }

  public virtual void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
    BuilderFactory.Instance.Release((UIWidget) this.background);
  }
}
