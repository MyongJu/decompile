﻿// Decompiled with JetBrains decompiler
// Type: AllianceTechDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceTechDlg : UI.Dialog
{
  private int _curLevel;
  private int _allianceDevoteRewardable;
  [SerializeField]
  private AllianceTechDlg.Panel panel;

  public void OnTechChanged(int techId)
  {
    this.RefreshDlg();
  }

  public void OnAllianceDataChanged(long allianceId)
  {
    if (PlayerData.inst.allianceData == null)
      return;
    this.panel.allianceHoner.text = Utils.FormatThousands(PlayerData.inst.userData.currency.honor.ToString());
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    RequestManager.inst.SendRequest("AllianceTech:getResearches", (Hashtable) null, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      PlayerData.inst.allianceTechManager.Update(arg2);
      AllianceTechInfo allianceTechInfo = ConfigManager.inst.DB_AllianceTech.GetItem(PlayerData.inst.allianceData.techMark);
      this._curLevel = allianceTechInfo == null ? 1 : allianceTechInfo.TierIndex;
      Hashtable hashtable = arg2 as Hashtable;
      if (hashtable != null && hashtable.ContainsKey((object) "alliance_devote_rewardable"))
        int.TryParse(hashtable[(object) "alliance_devote_rewardable"].ToString(), out this._allianceDevoteRewardable);
      this.RefreshDlg();
    }), true);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.allianceHonerIcon, "Texture/Alliance/alliance_honor", (System.Action<bool>) null, true, false, string.Empty);
    this.panel.researchOngoing.gameObject.SetActive(false);
    this.panel.lblDetail.text = ScriptLocalization.Get("id_uppercase_details", true);
    if (PlayerData.inst.allianceData != null)
      this.panel.allianceHoner.text = Utils.FormatThousands(PlayerData.inst.userData.currency.honor.ToString());
    DBManager.inst.DB_AllianceTech.onDataChanged += new System.Action<int>(this.OnTechChanged);
    DBManager.inst.DB_Alliance.onDataChanged += new System.Action<long>(this.OnAllianceDataChanged);
    AllianceUtilities.SetDevoteRewardTipsViewed(true);
    AllianceUtilities.UpdateDevoteRewardTips();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    DBManager.inst.DB_AllianceTech.onDataChanged -= new System.Action<int>(this.OnTechChanged);
    DBManager.inst.DB_Alliance.onDataChanged -= new System.Action<long>(this.OnAllianceDataChanged);
  }

  private void RefreshDlg(int techLevel, int focusedId)
  {
    if (techLevel < 1)
      techLevel = 1;
    this.panel.panel.Setup(techLevel, focusedId);
    this.panel.title.text = ScriptLocalization.Get("alliance_knowledge_title", true);
    if (techLevel <= 1)
    {
      this.panel.BT_leftLevel.gameObject.SetActive(false);
    }
    else
    {
      this.panel.BT_leftLevel.gameObject.SetActive(true);
      this.panel.BT_leftLevel.isEnabled = true;
      this.panel.leftArrowText.text = ScriptLocalization.GetWithPara("alliance_knowledge_level_num", new Dictionary<string, string>()
      {
        {
          "num",
          (techLevel - 1).ToString()
        }
      }, true);
    }
    if (techLevel >= ConfigManager.inst.DB_AllianceTech.maxTier)
    {
      this.panel.BT_rightLevel.gameObject.SetActive(false);
    }
    else
    {
      this.panel.BT_rightLevel.gameObject.SetActive(true);
      this.panel.BT_rightLevel.isEnabled = true;
      this.panel.rightArrowText.text = ScriptLocalization.GetWithPara("alliance_knowledge_level_num", new Dictionary<string, string>()
      {
        {
          "num",
          (techLevel + 1).ToString()
        }
      }, true);
      this.panel.rightLock.gameObject.SetActive(ConfigManager.inst.DB_AllianceTech.GetTierRequireLevel(techLevel + 1) > PlayerData.inst.allianceData.allianceLevel);
    }
    this.RefreshResearchingPanel();
    this.RefreshDevoteRewardButtonStatus(true);
  }

  private void RefreshResearchingPanel()
  {
    long researchingJobId = DBManager.inst.DB_AllianceTech.researchingJobId;
    if (researchingJobId == 0L)
    {
      this.panel.researchOngoing.gameObject.SetActive(false);
    }
    else
    {
      AllianceTechInfo allianceTechInfo = ConfigManager.inst.DB_AllianceTech.GetItem(DBManager.inst.DB_AllianceTech.researchingId);
      if (allianceTechInfo == null)
      {
        this.panel.researchOngoing.gameObject.SetActive(false);
      }
      else
      {
        this.panel.researchOngoing.gameObject.SetActive(true);
        this.panel.lblResearchName.text = string.Format("{0}    {1}", (object) ScriptLocalization.GetWithPara("alliance_knowledge_level_num", new Dictionary<string, string>()
        {
          {
            "num",
            allianceTechInfo.TierIndex.ToString()
          }
        }, true), (object) Utils.GetBenefitsDesc(allianceTechInfo.Benefits));
        JobHandle job = JobManager.Instance.GetJob(researchingJobId);
        if (job == null)
          return;
        this.panel.lblResearchTime.text = Utils.FormatTime(job.EndTime() - NetServerTime.inst.ServerTimestamp, false, false, true);
        this.panel.sliderResearch.value = Mathf.Clamp01((float) (NetServerTime.inst.ServerTimestamp - job.StartTime()) / (float) job.Duration());
      }
    }
  }

  private void RefreshDevoteRewardButtonStatus(bool show = true)
  {
    NGUITools.SetActive(this.panel.buttonCollectDevoteReward.gameObject, show && this._allianceDevoteRewardable == 1);
  }

  public void RefreshDlg()
  {
    this.RefreshDlg(this._curLevel, 0);
  }

  public void OnLeftPageButtonClick()
  {
    this.panel.tmpPanel.gameObject.SetActive(true);
    this.panel.tmpPanel.Setup(this._curLevel - 1, 0);
    this.panel.tmpPanel.transform.localPosition = new Vector3(-2600f, 0.0f, 0.0f);
    this.panel.BT_leftLevel.isEnabled = false;
    this.panel.BT_rightLevel.isEnabled = false;
    this.panel.panelAnimation.Stop();
    this.panel.panelAnimation.Play("alliance_tech_switch_left");
    --this._curLevel;
  }

  public void OnRightPageButtonClick()
  {
    if (this._curLevel < ConfigManager.inst.DB_AllianceTech.maxTier && ConfigManager.inst.DB_AllianceTech.GetTierRequireLevel(this._curLevel + 1) > PlayerData.inst.allianceData.allianceLevel)
    {
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_alliance_level_unlock_num", new Dictionary<string, string>()
      {
        {
          "1",
          ConfigManager.inst.DB_AllianceTech.GetTierRequireLevel(this._curLevel + 1).ToString()
        }
      }, true), (System.Action) null, 4f, false);
    }
    else
    {
      this.panel.tmpPanel.gameObject.SetActive(true);
      this.panel.tmpPanel.Setup(this._curLevel + 1, 0);
      this.panel.tmpPanel.transform.localPosition = new Vector3(2600f, 0.0f, 0.0f);
      this.panel.BT_leftLevel.isEnabled = false;
      this.panel.BT_rightLevel.isEnabled = false;
      this.panel.panelAnimation.Stop();
      this.panel.panelAnimation.Play("alliance_tech_switch_right");
      ++this._curLevel;
    }
  }

  public void OnHelpClick()
  {
    UIManager.inst.OpenPopup("HelpPopup", (Popup.PopupParameter) new HelpPopup.Parameter()
    {
      id = "help_dragon_skill"
    });
  }

  public void OnSwitchAnimEnd()
  {
    this.panel.panelAnimation.Stop();
    this.panel.panelContianerTrans.localPosition = Vector3.zero;
    this.panel.tmpPanel.gameObject.SetActive(false);
    this.RefreshDlg();
  }

  public void OnResearchDetailClick()
  {
    int researchingId = DBManager.inst.DB_AllianceTech.researchingId;
    if (researchingId == 0)
      return;
    AllianceTechInfo allianceTechInfo = ConfigManager.inst.DB_AllianceTech.GetItem(researchingId);
    this._curLevel = allianceTechInfo == null ? 1 : allianceTechInfo.TierIndex;
    this.RefreshDlg(this._curLevel, researchingId);
  }

  public void OnDevoteRewardButtonClick()
  {
    AllianceUtilities.DevoteRewardCollect((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.RefreshDevoteRewardButtonStatus(false);
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_tech_rewards_sent"), (System.Action) null, 4f, true);
    }));
  }

  public void OnSecond(int serverTime)
  {
    this.RefreshResearchingPanel();
  }

  private void OnEnable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  private void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  [Serializable]
  protected class Panel
  {
    public const int DEFAULT_PANEL_INDEX = 1;
    public const int MIN_TECH_TIER = 1;
    public const int MAX_TECH_TIER = 2;
    public const int SWITCH_TEMP_POSITION = 2600;
    public const string SWITCH_ANIM_LEFT = "alliance_tech_switch_left";
    public const string SWITCH_ANIM_RIGHT = "alliance_tech_switch_right";
    public UITexture allianceHonerIcon;
    public UILabel allianceHoner;
    public Transform panelContianerTrans;
    public UnityEngine.Animation panelAnimation;
    public AllianceTechDlgPanel panel;
    public AllianceTechDlgPanel tmpPanel;
    public UILabel title;
    public UIButton BT_leftLevel;
    public UILabel leftArrowText;
    public UIButton BT_rightLevel;
    public UILabel rightArrowText;
    public Transform rightLock;
    public Transform researchOngoing;
    public UIButton btnResearchDetail;
    public UILabel lblDetail;
    public UISlider sliderResearch;
    public UILabel lblResearchName;
    public UILabel lblResearchTime;
    public UIButton buttonCollectDevoteReward;
  }
}
