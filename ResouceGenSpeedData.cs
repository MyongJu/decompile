﻿// Decompiled with JetBrains decompiler
// Type: ResouceGenSpeedData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ResouceGenSpeedData
{
  private static float ONE_HOUR = 3600f;
  [HideInInspector]
  private CityManager.BuildingItem mBuildingItem;
  private double generateSpeed;
  private double baseGenSpeed;
  private double storageMaxValue;
  private double baseStorage;
  private double gen_boost;
  private double store_boost;
  private ResouceGenSpeedData.ResType type;
  private double curRes;
  private double lastcollectiontime;

  public void SetData(CityManager.BuildingItem mBuildingItem)
  {
    this.mBuildingItem = mBuildingItem;
    string mType = mBuildingItem.mType;
    if (mType != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (ResouceGenSpeedData.\u003C\u003Ef__switch\u0024map3C == null)
      {
        // ISSUE: reference to a compiler-generated field
        ResouceGenSpeedData.\u003C\u003Ef__switch\u0024map3C = new Dictionary<string, int>(4)
        {
          {
            "farm",
            0
          },
          {
            "lumber_mill",
            1
          },
          {
            "mine",
            2
          },
          {
            "house",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (ResouceGenSpeedData.\u003C\u003Ef__switch\u0024map3C.TryGetValue(mType, out num))
      {
        switch (num)
        {
          case 0:
            this.type = ResouceGenSpeedData.ResType.Food;
            break;
          case 1:
            this.type = ResouceGenSpeedData.ResType.Wood;
            break;
          case 2:
            this.type = ResouceGenSpeedData.ResType.Ore;
            break;
          case 3:
            this.type = ResouceGenSpeedData.ResType.Silver;
            break;
        }
      }
    }
    this.UpdateData();
  }

  public void Refresh()
  {
    this.UpdateData();
  }

  private void UpdateData()
  {
    if (this.mBuildingItem == null)
      return;
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData(this.mBuildingItem.mType, this.mBuildingItem.mLevel);
    this.baseGenSpeed = (double) data.GenerateInHour / (double) ResouceGenSpeedData.ONE_HOUR;
    this.baseStorage = (double) data.Storage;
    using (List<InBuildingData>.Enumerator enumerator = this.mBuildingItem.inBuildingData.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InBuildingData current = enumerator.Current;
        if ("gen_boost" == current.type)
          this.gen_boost = double.Parse(current.value);
        if ("store_boost" == current.type)
          this.store_boost = double.Parse(current.value);
        if ("update_time" == current.type)
          this.lastcollectiontime = double.Parse(current.value);
      }
    }
  }

  public double BaseSpeed
  {
    get
    {
      return this.baseGenSpeed;
    }
  }

  public double BaseSpeedInHour
  {
    get
    {
      return Math.Round(this.baseGenSpeed * (double) ResouceGenSpeedData.ONE_HOUR);
    }
  }

  public double GenerateSpeedInHour
  {
    get
    {
      return Math.Round(this.GenerateSpeed * (double) ResouceGenSpeedData.ONE_HOUR);
    }
  }

  public double GenerateSpeed
  {
    get
    {
      this.generateSpeed = this.baseGenSpeed * (1.0 + (double) (ConfigManager.inst.DB_BenefitCalc.GetBenefitPercentage(this.GetKey()) + AllianceBuildUtils.GetMyAllianceBenefit("prop_resource_generation_percent"))) * (1.0 + this.gen_boost) + (double) ConfigManager.inst.DB_BenefitCalc.GetBenefitValue(this.GetKey());
      return this.generateSpeed;
    }
  }

  public double TotalBenefitSpeedInHour
  {
    get
    {
      return Math.Round((this.GenerateSpeed - this.baseGenSpeed) * (double) ResouceGenSpeedData.ONE_HOUR);
    }
  }

  public double EquipmentBenefitInHour
  {
    get
    {
      return Math.Round(this.GetenefitValue(ResouceGenSpeedData.BenefitValueType.equip) * (double) ResouceGenSpeedData.ONE_HOUR);
    }
  }

  public double LegendBenefitInHour
  {
    get
    {
      return Math.Round(this.GetenefitValue(ResouceGenSpeedData.BenefitValueType.legend) * (double) ResouceGenSpeedData.ONE_HOUR);
    }
  }

  public double FortressBenefittInHour
  {
    get
    {
      return Math.Round(this.GetenefitValue(ResouceGenSpeedData.BenefitValueType.fortress) * (double) ResouceGenSpeedData.ONE_HOUR);
    }
  }

  public double ItemGenerateSpeedInHour
  {
    get
    {
      float num = 0.0f;
      float benefitPercentage = ConfigManager.inst.DB_BenefitCalc.GetBenefitPercentage(this.GetKey());
      num = ConfigManager.inst.DB_BenefitCalc.GetBenefitValue(this.GetKey());
      return Math.Round(this.baseGenSpeed * (1.0 + (double) benefitPercentage) * this.gen_boost * (double) ResouceGenSpeedData.ONE_HOUR);
    }
  }

  private double GetenefitValue(ResouceGenSpeedData.BenefitValueType type)
  {
    BenefitCalcInfo data = ConfigManager.inst.DB_BenefitCalc.GetData(this.GetKey());
    if (data == null)
      return 0.0;
    float num1 = 0.0f;
    float num2 = 0.0f;
    if (data.param1 != null)
    {
      for (int index = 0; index < data.param1.Length; ++index)
      {
        if (!string.IsNullOrEmpty(data.param1[index]))
        {
          BenefitInfo benefitInfo = DBManager.inst.DB_Local_Benefit.Get(data.param1[index]);
          switch (type)
          {
            case ResouceGenSpeedData.BenefitValueType.vip:
              num1 += benefitInfo.vip.value;
              continue;
            case ResouceGenSpeedData.BenefitValueType.telent:
              num1 += benefitInfo.hero.value;
              continue;
            case ResouceGenSpeedData.BenefitValueType.research:
              num1 += benefitInfo.research.value;
              continue;
            case ResouceGenSpeedData.BenefitValueType.equip:
              num1 = num1 + benefitInfo.equipment.value + benefitInfo.equipmentSuit.value + benefitInfo.equipmentSuitEnhance.value + benefitInfo.gem.value + benefitInfo.dkEquipment.value + benefitInfo.dkEquipmentSuit.value + benefitInfo.dkEquipmentSuitEnhance.value + benefitInfo.dkGem.value;
              continue;
            case ResouceGenSpeedData.BenefitValueType.legend:
              num1 = num1 + benefitInfo.legend.value + benefitInfo.legendSuit.value;
              continue;
            default:
              continue;
          }
        }
      }
      if (type == ResouceGenSpeedData.BenefitValueType.fortress)
        num1 += AllianceBuildUtils.GetMyAllianceBenefit("prop_resource_generation_percent");
    }
    if (data.param2 != null)
    {
      for (int index = 0; index < data.param2.Length; ++index)
      {
        if (!string.IsNullOrEmpty(data.param2[index]))
        {
          BenefitInfo benefitInfo = DBManager.inst.DB_Local_Benefit.Get(data.param2[index]);
          switch (type)
          {
            case ResouceGenSpeedData.BenefitValueType.vip:
              num2 += benefitInfo.vip.value;
              continue;
            case ResouceGenSpeedData.BenefitValueType.telent:
              num2 += benefitInfo.hero.value;
              continue;
            case ResouceGenSpeedData.BenefitValueType.research:
              num2 += benefitInfo.research.value;
              continue;
            case ResouceGenSpeedData.BenefitValueType.equip:
              num2 = num2 + benefitInfo.equipment.value + benefitInfo.equipmentSuit.value + benefitInfo.equipmentSuitEnhance.value + benefitInfo.gem.value + benefitInfo.dkEquipment.value + benefitInfo.dkEquipmentSuit.value + benefitInfo.dkEquipmentSuitEnhance.value + benefitInfo.dkGem.value;
              continue;
            case ResouceGenSpeedData.BenefitValueType.legend:
              num2 = num2 + benefitInfo.legend.value + benefitInfo.legendSuit.value;
              continue;
            default:
              continue;
          }
        }
      }
    }
    return this.baseGenSpeed * (double) num1 + (double) num2;
  }

  public float GetFullLeftTime()
  {
    double currentCount = this.CurrentCount;
    double storageMaxValue = this.StorageMaxValue;
    if (currentCount >= storageMaxValue)
      return 0.0f;
    return (float) ((storageMaxValue - currentCount) / this.GenerateSpeed);
  }

  public double CurrentCount
  {
    get
    {
      this.curRes = (NetServerTime.inst.UpdateTime - this.lastcollectiontime) * this.GenerateSpeed;
      double storageMaxValue = this.StorageMaxValue;
      if (this.curRes > storageMaxValue)
        this.curRes = storageMaxValue;
      return this.curRes;
    }
  }

  private string GetKey()
  {
    string str = string.Empty;
    switch (this.type)
    {
      case ResouceGenSpeedData.ResType.Food:
        str = "calc_food_generation";
        break;
      case ResouceGenSpeedData.ResType.Wood:
        str = "calc_wood_generation";
        break;
      case ResouceGenSpeedData.ResType.Ore:
        str = "calc_ore_generation";
        break;
      case ResouceGenSpeedData.ResType.Silver:
        str = "calc_silver_generation";
        break;
    }
    return str;
  }

  public double StorageMaxValue
  {
    get
    {
      float num1 = 0.0f;
      float num2 = 0.0f;
      switch (this.type)
      {
        case ResouceGenSpeedData.ResType.Food:
          num1 = ConfigManager.inst.DB_BenefitCalc.GetBenefitPercentage("calc_food_storage") + AllianceBuildUtils.GetMyAllianceBenefit("prop_resource_storage_percent");
          num2 = ConfigManager.inst.DB_BenefitCalc.GetBenefitValue("calc_food_storage");
          break;
        case ResouceGenSpeedData.ResType.Wood:
          num1 = ConfigManager.inst.DB_BenefitCalc.GetBenefitPercentage("calc_wood_storage") + AllianceBuildUtils.GetMyAllianceBenefit("prop_resource_storage_percent");
          num2 = ConfigManager.inst.DB_BenefitCalc.GetBenefitValue("calc_wood_storage");
          break;
        case ResouceGenSpeedData.ResType.Ore:
          num1 = ConfigManager.inst.DB_BenefitCalc.GetBenefitPercentage("calc_ore_storage") + AllianceBuildUtils.GetMyAllianceBenefit("prop_resource_storage_percent");
          num2 = ConfigManager.inst.DB_BenefitCalc.GetBenefitValue("calc_ore_storage");
          break;
        case ResouceGenSpeedData.ResType.Silver:
          num1 = ConfigManager.inst.DB_BenefitCalc.GetBenefitPercentage("calc_silver_storage") + AllianceBuildUtils.GetMyAllianceBenefit("prop_resource_storage_percent");
          num2 = ConfigManager.inst.DB_BenefitCalc.GetBenefitValue("calc_silver_storage");
          break;
      }
      this.storageMaxValue = this.baseStorage * (1.0 + (double) num1) * (1.0 + this.store_boost) + (double) num2;
      return this.storageMaxValue;
    }
  }

  public double VipGenerateSpeedInHour
  {
    get
    {
      return Math.Round(this.GetenefitValue(ResouceGenSpeedData.BenefitValueType.vip) * (double) ResouceGenSpeedData.ONE_HOUR);
    }
  }

  public double ResearchGenerateSpeedInHour
  {
    get
    {
      return Math.Round(this.GetenefitValue(ResouceGenSpeedData.BenefitValueType.research) * (double) ResouceGenSpeedData.ONE_HOUR);
    }
  }

  public double TelentGenerateSpeedInHour
  {
    get
    {
      return Math.Round(this.GetenefitValue(ResouceGenSpeedData.BenefitValueType.telent) * (double) ResouceGenSpeedData.ONE_HOUR);
    }
  }

  public enum ResType
  {
    Food,
    Wood,
    Ore,
    Silver,
  }

  private enum BenefitValueType
  {
    vip,
    telent,
    research,
    equip,
    fortress,
    legend,
  }
}
