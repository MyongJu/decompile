﻿// Decompiled with JetBrains decompiler
// Type: ConfigAbyssSpeedUp
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAbyssSpeedUp
{
  public Dictionary<string, AbyssSpeedUpInfo> datas;
  private Dictionary<int, AbyssSpeedUpInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<AbyssSpeedUpInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public AbyssSpeedUpInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (AbyssSpeedUpInfo) null;
  }

  public AbyssSpeedUpInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AbyssSpeedUpInfo) null;
  }

  public int GetCost(int times)
  {
    using (Dictionary<string, AbyssSpeedUpInfo>.Enumerator enumerator = this.datas.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AbyssSpeedUpInfo abyssSpeedUpInfo = enumerator.Current.Value;
        if (times >= abyssSpeedUpInfo.timesMin && times <= abyssSpeedUpInfo.timesMax)
          return abyssSpeedUpInfo.goldCost;
      }
    }
    return 0;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, AbyssSpeedUpInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, AbyssSpeedUpInfo>) null;
  }
}
