﻿// Decompiled with JetBrains decompiler
// Type: IAPPlatformInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class IAPPlatformInfo
{
  public int internalID;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "platform")]
  public string platform;
  [Config(Name = "pay_id")]
  public string pay_id;
  [Config(Name = "product_id")]
  public string product_id;
}
