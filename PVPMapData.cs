﻿// Decompiled with JetBrains decompiler
// Type: PVPMapData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;

public static class PVPMapData
{
  public static DynamicMapData MapData = new DynamicMapData();
  public static int KingdomCount = 0;
  public const string UploadTileset = "master_tileset.json";
  public const string MARCH_LAYER_NAME = "March";
  public const int MARCH_PATH_SORTING_ORDER = 29998;
  public const int MARCH_TROOPS_SORTING_ORDER = 29999;
  public const int UI_SORTING_ORDER = 30000;

  public static void Initialize()
  {
    PVPMapData.MapData.LoadAssetPath = "TextAsset/Kingdom/";
    PVPMapData.MapData.KingdomGround = "GROUND_GREEN";
    PrefabManagerEx.Instance.AddRouteMapping("GROUND_GREEN", "Prefab/Kingdom/Map/GROUND_GREEN");
    PVPMapData.MapData.LoadDefault("TextAsset/Kingdom/default_tileset");
    PVPMapData.MapData.LoadTileSet("TextAsset/Kingdom/master_tileset", "Prefab/Tiles/", PVPMapData.KingdomCount, 0);
  }

  public static void LoadKingdom(object orgData)
  {
    PVPMapData.MapData.ActiveKingdoms.Clear();
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable == null)
      return;
    ArrayList arrayList = hashtable[(object) "list"] as ArrayList;
    if (arrayList == null)
      return;
    PVPMapData.KingdomCount = int.MinValue;
    for (int index = 0; index < arrayList.Count; ++index)
    {
      ActiveKingdom activeKingdom = new ActiveKingdom();
      if (activeKingdom.Decode(arrayList[index]))
      {
        PVPMapData.MapData.ActiveKingdoms.Add(activeKingdom.WorldID, activeKingdom);
        PVPMapData.KingdomCount = Math.Max(PVPMapData.KingdomCount, activeKingdom.WorldID);
      }
    }
  }
}
