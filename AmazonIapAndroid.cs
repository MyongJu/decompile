﻿// Decompiled with JetBrains decompiler
// Type: AmazonIapAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using com.amazon.device.iap.cpt;
using Funplus;
using Funplus.Abstract;
using HSMiniJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AmazonIapAndroid : BasePaymentWrapper
{
  private static readonly object locker = new object();
  private List<string> _allProducts = new List<string>()
  {
    "com.funplus.koa.month0",
    "com.funplus.koa.month1",
    "com.funplus.koa.month2",
    "com.funplus.koa.gold0",
    "com.funplus.koa.gold1",
    "com.funplus.koa.gold2",
    "com.funplus.koa.gold3",
    "com.funplus.koa.gold4",
    "com.funplus.koa.gold5"
  };
  private List<Dictionary<string, object>> _allProductData = new List<Dictionary<string, object>>();
  private Dictionary<string, string> _allRequestProductId = new Dictionary<string, string>();
  private Hashtable _allReceiptParameters = new Hashtable();
  private char[] NumberCharArray = new char[11]
  {
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '.'
  };
  private const string FUNC_PAYMENT_INITIALIZE_SUCCESS = "OnPaymentInitializeSuccess";
  private const string FUNC_PAYMENT_INITIALIZE_ERROR = "OnPaymentInitializeError";
  private const string FUNC_NAME_PURCHASE_ERROR = "OnPaymentPurchaseError";
  private const string FUNC_NAME_PAYMENT_PURCHASE_SUCCESS = "OnPaymentPurchaseSuccess";
  private const string ERROR_MESSAGE = "{errorCode:9999, errorMsg:\"\", errorLocalizedMsg:\"\"}";
  private const string PLUGIN_VERSION = "1.1";
  private static AmazonIapAndroid _instance;
  private IAmazonIapV2 _iapService;
  private string _targetGameObjectName;
  private GameObject _targetGameObject;

  public AmazonIapAndroid()
  {
    this.LoadAllReceiptParam();
  }

  private Dictionary<string, object> GetProductData(string productId)
  {
    using (List<Dictionary<string, object>>.Enumerator enumerator = this._allProductData.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Dictionary<string, object> current = enumerator.Current;
        if (current.ContainsKey(nameof (productId)) && current[nameof (productId)].ToString() == productId)
          return current;
      }
    }
    return (Dictionary<string, object>) null;
  }

  public static AmazonIapAndroid Instance
  {
    get
    {
      if (AmazonIapAndroid._instance == null)
      {
        lock (AmazonIapAndroid.locker)
          AmazonIapAndroid._instance = new AmazonIapAndroid();
      }
      return AmazonIapAndroid._instance;
    }
  }

  public override bool GetSubsRenewing(string productId)
  {
    return false;
  }

  public override bool CanCheckSubs()
  {
    return false;
  }

  private string ReceiptParamCacheFilePath
  {
    get
    {
      return Path.Combine(Application.persistentDataPath, "amazon_cache.json");
    }
  }

  private string ReceiptProcessServerUrl
  {
    get
    {
      if (!string.IsNullOrEmpty(PaymentManager.Instance.PaymentUrl))
        return PaymentManager.Instance.PaymentUrl + "/callback/amazoniap/";
      return FunplusSdk.Instance.Environment == "sandbox" ? "https://payment-sandbox.funplusgame.com/callback/amazoniap/" : "https://payment.funplusgame.com/callback/amazoniap/";
    }
  }

  private string AppId
  {
    get
    {
      if (!string.IsNullOrEmpty(PaymentManager.Instance.PaymentAppId))
        return PaymentManager.Instance.PaymentAppId;
      return FunplusSdk.Instance.Environment == "sandbox" ? "153" : "136";
    }
  }

  private IAmazonIapV2 IapService
  {
    get
    {
      if (this._iapService == null)
      {
        this._iapService = AmazonIapV2Impl.Instance;
        this._iapService.AddPurchaseResponseListener(new PurchaseResponseDelegate(this.OnPurchaseResponse));
        this._iapService.AddGetPurchaseUpdatesResponseListener(new GetPurchaseUpdatesResponseDelegate(this.OnGetPurchaseUpdatesResponse));
        this._iapService.AddGetProductDataResponseListener(new GetProductDataResponseDelegate(this.OnGetProductDataResponseDelegate));
      }
      return AmazonIapV2Impl.Instance;
    }
  }

  private GameObject TargetGameObject
  {
    get
    {
      if (!(bool) ((UnityEngine.Object) this._targetGameObject))
        this._targetGameObject = GameObject.Find(this._targetGameObjectName);
      return this._targetGameObject;
    }
  }

  private void AddRequestProductId(string requestId, string productId)
  {
    this.AmazonLog(string.Format("AddRequestProductId {0}={1}", (object) requestId, (object) productId));
    if (this._allRequestProductId.ContainsKey(requestId))
      this._allRequestProductId.Remove(requestId);
    this._allRequestProductId.Add(requestId, productId);
  }

  private void RemoveRequestProductId(string requestId)
  {
    this.AmazonLog(string.Format("RemoveRequestProductId {0}", (object) requestId));
    if (!this._allRequestProductId.ContainsKey(requestId))
      return;
    this._allRequestProductId.Remove(requestId);
  }

  public override void SetCurrencyWhitelist(string whitelist)
  {
  }

  private string GetRequestProductId(string requestId)
  {
    string empty = string.Empty;
    if (this._allRequestProductId.ContainsKey(requestId))
      empty = this._allRequestProductId[requestId];
    this.AmazonLog(string.Format("GetRequestProductId {0} = {1}", (object) requestId, (object) empty));
    return empty;
  }

  private Hashtable GetReceiptParamByProductId(string productId)
  {
    Hashtable hashtable;
    if (this._allReceiptParameters.ContainsKey((object) productId))
    {
      hashtable = this._allReceiptParameters[(object) productId] as Hashtable;
    }
    else
    {
      D.error((object) "cache of receipt parameter already cleared.");
      hashtable = new Hashtable()
      {
        {
          (object) "product_id",
          (object) productId
        },
        {
          (object) "through_cargo",
          (object) string.Empty
        },
        {
          (object) "appid",
          (object) this.AppId
        },
        {
          (object) "appservid",
          (object) string.Empty
        },
        {
          (object) "uid",
          (object) AccountManager.Instance.FunplusID
        },
        {
          (object) "vcurrency_key",
          (object) "point"
        }
      };
    }
    this.AmazonLog(string.Format("GetReceiptParamByProductId {0} = {1}", (object) productId, (object) Utils.Object2Json((object) hashtable)));
    return hashtable;
  }

  private void AddReceiptParam(string productId, Hashtable param)
  {
    this.AmazonLog(string.Format("AddReceiptParam {0} = {1}", (object) productId, (object) Utils.Object2Json((object) param)));
    if (this._allReceiptParameters.ContainsKey((object) productId))
      this._allReceiptParameters.Remove((object) productId);
    this._allReceiptParameters.Add((object) productId, (object) param);
    this.SaveAllReceiptParam();
  }

  private void RemoveReceiptParam(string productId)
  {
    this.AmazonLog(string.Format("RemoveReceiptParam {0}", (object) productId));
    if (this._allReceiptParameters.ContainsKey((object) productId))
      this._allReceiptParameters.Remove((object) productId);
    this.SaveAllReceiptParam();
  }

  private void LoadAllReceiptParam()
  {
    if (!File.Exists(this.ReceiptParamCacheFilePath))
      return;
    string json = File.ReadAllText(this.ReceiptParamCacheFilePath);
    this.AmazonLog(string.Format("LoadAllReceiptParam {0}", (object) json));
    Hashtable hashtable = Utils.Json2Object(json, true) as Hashtable;
    if (hashtable == null)
      return;
    this._allReceiptParameters = hashtable;
  }

  private void SaveAllReceiptParam()
  {
    File.WriteAllText(this.ReceiptParamCacheFilePath, Utils.Object2Json((object) this._allReceiptParameters));
  }

  private void ClearAllReceiptParam()
  {
    this.AmazonLog(nameof (ClearAllReceiptParam));
    this._allReceiptParameters.Clear();
    this.SaveAllReceiptParam();
  }

  private int GetAllReceiptParamCount()
  {
    this.AmazonLog(string.Format("GetAllReceiptParamCount {0}", (object) this._allReceiptParameters.Count));
    return this._allReceiptParameters.Count;
  }

  public override void SetGameObject(string gameObjectName)
  {
    this._targetGameObjectName = gameObjectName;
  }

  public override bool CanMakePurchases()
  {
    return true;
  }

  public override void StartHelper()
  {
    this.AmazonLog(string.Format(nameof (StartHelper)));
    SkusInput skusInput = new SkusInput();
    skusInput.Skus = new List<string>();
    using (List<string>.Enumerator enumerator = this._allProducts.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        skusInput.Skus.Add(current);
      }
    }
    this.AmazonLog(this.IapService.GetProductData(skusInput).ToJson());
  }

  public override void Buy(string productId, string throughCargo)
  {
    D.error((object) "have not implement");
  }

  private void GetUnFinishedReceiptFromAmazon()
  {
    this.AmazonLog(string.Format(nameof (GetUnFinishedReceiptFromAmazon)));
    AmazonIapV2Impl.Instance.GetPurchaseUpdates(new ResetInput()
    {
      Reset = true
    });
  }

  private Hashtable GetReceiptParamFromThroughCargon(string throughCargon)
  {
    Hashtable hashtable = new Hashtable();
    string[] strArray = throughCargon.Split('|');
    if (strArray.Length >= 4)
    {
      hashtable[(object) "uid"] = (object) strArray[0];
      hashtable[(object) "appservid"] = (object) strArray[3];
    }
    else
      D.error((object) "GetReceiptParamFromThroughCargon Error");
    return hashtable;
  }

  public override void Buy(string productId, string serverId, string throughCargo)
  {
    this.AmazonLog(string.Format("Buy {0} {1} {2}", (object) productId, (object) serverId, (object) throughCargo));
    Hashtable fromThroughCargon = this.GetReceiptParamFromThroughCargon(throughCargo);
    fromThroughCargon[(object) "through_cargo"] = (object) throughCargo;
    fromThroughCargon[(object) "appid"] = (object) this.AppId;
    fromThroughCargon[(object) "appservid"] = (object) serverId;
    fromThroughCargon[(object) "product_id"] = (object) productId;
    fromThroughCargon[(object) "uid"] = (object) AccountManager.Instance.FunplusID;
    fromThroughCargon[(object) "vcurrency_key"] = (object) "point";
    this.AddReceiptParam(productId, fromThroughCargon);
    RequestOutput requestOutput = this.IapService.Purchase(new SkuInput()
    {
      Sku = productId
    });
    this.AddRequestProductId(requestOutput.RequestId, productId);
    this.AmazonLog(requestOutput.ToJson());
  }

  private void OnPurchaseResponse(PurchaseResponse purchaseResponse)
  {
    this.AmazonLog(string.Format("OnPurchaseResponse {0}", (object) purchaseResponse.ToJson()));
    PurchaseReceipt purchaseReceipt = purchaseResponse.PurchaseReceipt;
    if (purchaseResponse.Status == "SUCCESSFUL")
    {
      Hashtable paramByProductId = this.GetReceiptParamByProductId(purchaseReceipt.Sku);
      try
      {
        string currencyCode = GlobalizationCodeConverter.CountryCodeToCurrencyCode(purchaseResponse.AmazonUserData.Marketplace);
        paramByProductId.Add((object) "currency_code", (object) currencyCode);
        this.SaveAllReceiptParam();
        this.AmazonLog(string.Format("attach currency code to receipt parameters -> {0}", (object) currencyCode));
      }
      catch (Exception ex)
      {
        D.error("attach currency code failed with exception:" + ex.Message == null ? (object) "unkonw" : (object) ex.Message);
      }
      this.RequestProcessReceipt(purchaseResponse.AmazonUserData.UserId, purchaseResponse.PurchaseReceipt.ReceiptId, paramByProductId);
    }
    else
    {
      this.TargetGameObject.BroadcastMessage("OnPaymentPurchaseError", (object) "{errorCode:9999, errorMsg:\"\", errorLocalizedMsg:\"\"}");
      this.AmazonLog("amazon iap not success: " + purchaseResponse.Status);
    }
    this.RemoveRequestProductId(purchaseResponse.RequestId);
  }

  private void OnGetPurchaseUpdatesResponse(GetPurchaseUpdatesResponse purchaseUpdatesResponse)
  {
    this.AmazonLog(string.Format("OnGetPurchaseUpdatesResponse : {0}", (object) purchaseUpdatesResponse.ToJson()));
    if (!(purchaseUpdatesResponse.Status == "SUCCESSFUL"))
      return;
    List<PurchaseReceipt> receipts = purchaseUpdatesResponse.Receipts;
    if (receipts != null && receipts.Count > 0)
    {
      using (List<PurchaseReceipt>.Enumerator enumerator = receipts.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          PurchaseReceipt current = enumerator.Current;
          Hashtable paramByProductId = this.GetReceiptParamByProductId(current.Sku);
          if (!paramByProductId.ContainsKey((object) "currency_code"))
          {
            paramByProductId.Add((object) "currency_code", (object) GlobalizationCodeConverter.CountryCodeToCurrencyCode(purchaseUpdatesResponse.AmazonUserData.Marketplace));
            this.SaveAllReceiptParam();
          }
          this.RequestProcessReceipt(purchaseUpdatesResponse.AmazonUserData.UserId, current.ReceiptId, paramByProductId);
        }
      }
    }
    if (!purchaseUpdatesResponse.HasMore)
      return;
    this.GetUnFinishedReceiptFromAmazon();
  }

  private void RequestProcessReceipt(string amazonUserId, string receiptId, Hashtable receiptParams)
  {
    this.AmazonLog(string.Format("RequestProcessReceipt : {0} {1} {2}", (object) amazonUserId, (object) receiptId, (object) Utils.Object2Json((object) receiptParams)));
    GameObject gameObject = new GameObject();
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
    HttpPostProcessor httpPostProcessor = gameObject.AddComponent<HttpPostProcessor>();
    receiptParams[(object) "amazon_user_id"] = (object) amazonUserId;
    receiptParams[(object) "amazoniap_version"] = (object) "2.0";
    receiptParams[(object) "receipt_id"] = (object) receiptId;
    receiptParams[(object) "plugion_version"] = (object) "1.1";
    httpPostProcessor.DoPost(this.ReceiptProcessServerUrl, receiptParams, receiptParams, new HttpPostProcessor.PostResultCallback(this.OnProcessReceiptCallback));
  }

  private void OnProcessReceiptCallback(bool result, Hashtable data, Hashtable userData)
  {
    this.AmazonLog(string.Format("OnProcessReceiptCallback {0} {1} {2}", (object) result, data == null ? (object) "null" : (object) Utils.Object2Json((object) data), userData == null ? (object) "null" : (object) Utils.Object2Json((object) userData)));
    if (result)
    {
      string str = userData[(object) "receipt_id"].ToString();
      string productId = userData[(object) "product_id"].ToString();
      this.IapService.NotifyFulfillment(new NotifyFulfillmentInput()
      {
        ReceiptId = str,
        FulfillmentResult = "FULFILLED"
      });
      if (data != null && data.ContainsKey((object) "status") && data[(object) "status"].ToString() == "1")
      {
        Hashtable paramByProductId = this.GetReceiptParamByProductId(productId);
        this.AmazonLog("FUNC_NAME_PAYMENT_PURCHASE_SUCCESS");
        this.TracePaymentBIEvent(paramByProductId);
        Dictionary<string, object> dictionary = new Dictionary<string, object>();
        foreach (string key in (IEnumerable) paramByProductId.Keys)
          dictionary.Add(key, paramByProductId[(object) key]);
        this.TargetGameObject.BroadcastMessage("OnPaymentPurchaseSuccess", (object) Json.Serialize((object) dictionary));
      }
      else
      {
        this.AmazonLog("receipt process failed: " + (data == null || !data.ContainsKey((object) "reason") ? "unknow" : data[(object) "reason"].ToString()));
        this.AmazonLog("FUNC_NAME_PURCHASE_ERROR");
        this.TargetGameObject.BroadcastMessage("OnPaymentPurchaseError", (object) "{errorCode:9999, errorMsg:\"\", errorLocalizedMsg:\"\"}");
      }
    }
    else
    {
      D.error((object) "amazon iap: connect receipt process server failed");
      this.TargetGameObject.BroadcastMessage("OnPaymentPurchaseError", (object) "{errorCode:9999, errorMsg:\"\", errorLocalizedMsg:\"\"}");
    }
  }

  private void TracePaymentBIEvent(Hashtable receiptParams)
  {
    try
    {
      if (FunplusSdk.Instance.IsSdkInstalled())
      {
        Dictionary<string, object> dictionary = new Dictionary<string, object>();
        string empty1 = string.Empty;
        string empty2 = string.Empty;
        if (receiptParams.Contains((object) "currency_code"))
          empty2 = receiptParams[(object) "currency_code"].ToString();
        string productId = receiptParams[(object) "product_id"].ToString();
        string empty3 = string.Empty;
        string str = receiptParams[(object) "receipt_id"].ToString();
        Dictionary<string, object> productData = this.GetProductData(productId);
        if (productData != null)
        {
          empty1 = productData["price_amount"].ToString();
          empty3 = productData["title"].ToString();
          if (string.IsNullOrEmpty(empty2))
          {
            D.error((object) "cannot find currency code from receipt parameters, use product currency code instead.");
            empty2 = productData["price_currency_code"].ToString();
          }
        }
        dictionary.Add("amount", (object) empty1);
        dictionary.Add("currency", (object) empty2);
        dictionary.Add("iap_product_id", (object) productId);
        dictionary.Add("iap_product_name", (object) empty3);
        dictionary.Add("transaction_id", (object) str);
        dictionary.Add("payment_processor", (object) "amazoniap");
        dictionary.Add("d_currency_received_type", (object) "point");
        this.AmazonLog(string.Format(nameof (TracePaymentBIEvent)));
        FunplusBi.Instance.TraceEvent("payment", Json.Serialize((object) dictionary));
      }
      else
        D.error((object) string.Format("TracePaymentBIEvent exception: {0}", (object) "FunplusSdk.Instance.IsSdkInstalled() == false"));
    }
    catch (Exception ex)
    {
      D.error((object) string.Format("TracePaymentBIEvent exception: {0}", !string.IsNullOrEmpty(ex.Message) ? (object) ex.Message : (object) "unknow"));
    }
  }

  private void OnGetProductDataResponseDelegate(GetProductDataResponse productDataResponse)
  {
    this.AmazonLog(string.Format("OnGetProductDataResponseDelegate {0}", (object) productDataResponse.ToJson()));
    if (productDataResponse.Status == "SUCCESSFUL")
    {
      this._allProductData = new List<Dictionary<string, object>>();
      if (productDataResponse.ProductDataMap != null)
      {
        using (Dictionary<string, ProductData>.Enumerator enumerator = productDataResponse.ProductDataMap.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, ProductData> current = enumerator.Current;
            string price = current.Value.Price;
            int num1 = price.IndexOfAny(this.NumberCharArray);
            string str = price.Substring(0, num1);
            string s = price.Substring(num1);
            double result = 0.0;
            double.TryParse(s, out result);
            long num2 = (long) (result * 1000000.0);
            this._allProductData.Add(new Dictionary<string, object>()
            {
              {
                "productId",
                (object) current.Value.Sku
              },
              {
                "title",
                (object) current.Value.Title
              },
              {
                "description",
                (object) current.Value.Description
              },
              {
                "price_currency_code",
                (object) str
              },
              {
                "price",
                (object) price
              },
              {
                "price_amount",
                (object) s
              },
              {
                "price_amount_micros",
                (object) num2
              }
            });
          }
        }
        string str1 = Json.Serialize((object) this._allProductData);
        this.TargetGameObject.BroadcastMessage("OnPaymentInitializeSuccess", (object) str1);
        this.AmazonLog("amazon product list: " + str1);
      }
      if (productDataResponse.UnavailableSkus != null)
      {
        using (List<string>.Enumerator enumerator = productDataResponse.UnavailableSkus.GetEnumerator())
        {
          while (enumerator.MoveNext())
            D.error((object) ("invalid amazon product id: " + enumerator.Current));
        }
      }
      this.GetUnFinishedReceiptFromAmazon();
    }
    else
      D.error((object) "get amazon product data failed");
  }

  private void AmazonLog(string logDetail)
  {
    Debug.Log((object) ("AmazonLog:" + logDetail));
  }
}
