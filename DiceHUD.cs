﻿// Decompiled with JetBrains decompiler
// Type: DiceHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DiceHUD : MonoBehaviour
{
  private string vfxName = string.Empty;
  private const string vfxPath = "Prefab/Dice/";
  public GameObject rootNode;
  public UILabel remainTime;
  private GameObject vfxObj;
  private UILabel _diceExchangeRewardCount;

  private void OnEnable()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void Start()
  {
    Transform child = this.transform.FindChild("DiceBtn/BadgeBkg/Counter");
    if ((bool) ((UnityEngine.Object) child))
      this._diceExchangeRewardCount = child.GetComponent<UILabel>();
    this.UpdateRedPoint();
  }

  private void UpdateRedPoint()
  {
    if (!(bool) ((UnityEngine.Object) this._diceExchangeRewardCount))
      return;
    this._diceExchangeRewardCount.text = DicePayload.Instance.TempDiceData.CanExchangeRewardCount.ToString();
    NGUITools.SetActive(this._diceExchangeRewardCount.transform.parent.gameObject, DicePayload.Instance.TempDiceData.CanExchangeRewardCount > 0);
  }

  private void OnSecondEvent(int time)
  {
    this.UpdateUI();
    this.UpdateRedPoint();
  }

  private void UpdateUI()
  {
    if ((UnityEngine.Object) this.remainTime != (UnityEngine.Object) null)
      this.remainTime.text = Utils.FormatTime(DicePayload.Instance.TempDiceData.EndTime - NetServerTime.inst.ServerTimestamp, true, false, true);
    if (DicePayload.Instance.EntranceShow)
    {
      NGUITools.SetActive(this.rootNode, true);
      this.SetVFX(DicePayload.Instance.Theme);
    }
    else
    {
      if (this.rootNode.activeInHierarchy)
        NGUITools.SetActive(this.rootNode, false);
      this.DeleteVFX();
    }
  }

  private void SetVFX(string vfxUsedName)
  {
    if (!(this.vfxName != vfxUsedName))
      return;
    this.DeleteVFX();
    this.vfxName = vfxUsedName;
    this.vfxObj = AssetManager.Instance.HandyLoad("Prefab/Dice/" + this.vfxName, typeof (GameObject)) as GameObject;
    if (!((UnityEngine.Object) this.vfxObj != (UnityEngine.Object) null))
      return;
    this.vfxObj = UnityEngine.Object.Instantiate<GameObject>(this.vfxObj);
    this.vfxObj.transform.parent = this.rootNode.transform;
    this.vfxObj.transform.localPosition = Vector3.zero;
    this.vfxObj.transform.localScale = Vector3.one;
  }

  private void DeleteVFX()
  {
    this.vfxName = string.Empty;
    if (!((UnityEngine.Object) this.vfxObj != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.vfxObj);
    this.vfxObj = (GameObject) null;
  }

  public void OnEntranceClicked()
  {
    DicePayload.Instance.ShowDiceScene();
    OperationTrace.TraceClick(ClickArea.Dice);
  }
}
