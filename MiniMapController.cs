﻿// Decompiled with JetBrains decompiler
// Type: MiniMapController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapController : MonoBehaviour
{
  private List<GameObject> m_LandMarkList = new List<GameObject>();
  public GameObject MyWorld;
  public GameObject TheirWorld;
  public GameObject OtherWorld;
  public GameObject Border;
  public GameObject Location;
  public GameObject ResourceControl;
  public GameObject AllianceControl;
  public GameObject ArrowControl;
  public GameObject AllianceWarControl;
  public TextMesh[] Resources;
  public GameObject ArrowRight;
  public GameObject ArrowLeft;
  public GameObject ArrowBottom;
  public GameObject ArrowTop;
  public TextMesh RightLabel;
  public TextMesh LeftLabel;
  public TextMesh BottomLabel;
  public TextMesh TopLabel;
  public UISpriteMesh RightSprite;
  public UISpriteMesh LeftSprite;
  public UISpriteMesh BottomSprite;
  public UISpriteMesh TopSprite;
  private Vector2 m_Origin;
  private int m_KingdomX;
  private int m_KingdomY;
  private ActiveKingdom m_ActiveKingdom;

  private void Start()
  {
    for (int index = 0; index < this.Resources.Length; ++index)
      this.Resources[index].text = string.Format(Utils.XLAT("kingdom_map_resource_lv"), (object) (index + 1));
  }

  public void Reset(int kingdomX, int kingdomY)
  {
    this.m_KingdomX = kingdomX;
    this.m_KingdomY = kingdomY;
    this.m_ActiveKingdom = PVPMapData.MapData.FindKingdomByLocation(kingdomX, kingdomY);
    this.m_Origin = new Vector2(MiniMapSystem.Inner.x * (float) kingdomX, MiniMapSystem.Inner.y * (float) kingdomY);
    this.transform.localPosition = new Vector3(this.m_Origin.x, this.m_Origin.y, 0.0f);
    this.Deactivate();
  }

  public Vector2 Origin
  {
    get
    {
      return this.m_Origin;
    }
  }

  public void Deactivate()
  {
    this.MyWorld.SetActive(false);
    this.TheirWorld.SetActive(false);
    this.OtherWorld.SetActive(true);
    this.Border.SetActive(false);
    this.Location.SetActive(false);
    this.ResourceControl.SetActive(false);
    this.AllianceControl.SetActive(false);
    this.ArrowControl.SetActive(false);
    for (int index = 0; index < this.m_LandMarkList.Count; ++index)
    {
      GameObject landMark = this.m_LandMarkList[index];
      if ((bool) ((Object) landMark))
      {
        landMark.transform.parent = (Transform) null;
        Object.Destroy((Object) landMark);
      }
    }
    this.m_LandMarkList.Clear();
  }

  public void MarkMyKingdom(ActiveKingdom myKingdom)
  {
    if (myKingdom == null || myKingdom.KingdomX != this.m_KingdomX || myKingdom.KingdomY != this.m_KingdomY)
      return;
    this.MyWorld.SetActive(true);
    this.OtherWorld.SetActive(false);
    this.TheirWorld.SetActive(false);
  }

  public void Activate(bool isMine, bool showLocation)
  {
    this.MyWorld.SetActive(isMine);
    this.TheirWorld.SetActive(!isMine);
    this.OtherWorld.SetActive(false);
    this.Border.SetActive(true);
    this.Location.SetActive(showLocation);
    this.UpdateWorld();
  }

  public void UpdateArrow(ActiveKingdom left, ActiveKingdom right, ActiveKingdom bottom, ActiveKingdom top)
  {
    this.ArrowControl.SetActive(true);
    this.ArrowLeft.SetActive(left != null);
    this.ArrowRight.SetActive(right != null);
    this.ArrowBottom.SetActive(bottom != null);
    this.ArrowTop.SetActive(top != null);
    if (left != null)
    {
      DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) left.WorldID);
      if (wonderData != null)
      {
        this.LeftLabel.text = wonderData.LabelName;
        this.LeftSprite.spriteName = wonderData.FlagIconPath;
      }
      else
      {
        this.ArrowLeft.SetActive(false);
        D.error((object) ("[MiniMapController]UpdateArrow: Lack of server wonder data. Left - world_id = " + (object) left.WorldID));
      }
    }
    if (right != null)
    {
      DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) right.WorldID);
      if (wonderData != null)
      {
        this.RightLabel.text = wonderData.LabelName;
        this.RightSprite.spriteName = wonderData.FlagIconPath;
      }
      else
      {
        this.ArrowRight.SetActive(false);
        D.error((object) ("[MiniMapController]UpdateArrow: Lack of server wonder data. Right - world_id = " + (object) right.WorldID));
      }
    }
    if (bottom != null)
    {
      DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) bottom.WorldID);
      if (wonderData != null)
      {
        this.BottomLabel.text = wonderData.LabelName;
        this.BottomSprite.spriteName = wonderData.FlagIconPath;
      }
      else
      {
        this.ArrowBottom.SetActive(false);
        D.error((object) ("[MiniMapController]UpdateArrow: Lack of server wonder data. Botton - world_id = " + (object) bottom.WorldID));
      }
    }
    if (top == null)
      return;
    DB.WonderData wonderData1 = DBManager.inst.DB_Wonder.Get((long) top.WorldID);
    if (wonderData1 != null)
    {
      this.TopLabel.text = wonderData1.LabelName;
      this.TopSprite.spriteName = wonderData1.FlagIconPath;
    }
    else
    {
      this.ArrowTop.SetActive(false);
      D.error((object) ("[MiniMapController]UpdateArrow: Lack of server wonder data. Top - world_id = " + (object) top.WorldID));
    }
  }

  public void OnResourceVisiblityChanged(bool visible)
  {
    this.ResourceControl.SetActive(visible);
  }

  public void OnAllianceVisiblityChanged(bool visible)
  {
    this.AllianceControl.SetActive(visible);
  }

  public void OnAllianceWarVisiblityChanged(bool visible)
  {
    Logger.Log("OnAllianceWarVisiblityChanged = " + visible.ToString());
    this.ClearAllianceWarEnemy();
    if (!visible)
      return;
    List<AllianceWarTrackData.TrackMemberData> memberDataList = AllianceWarPayload.Instance.TrackData.MemberDataList;
    if (memberDataList != null)
    {
      for (int index = 0; index < memberDataList.Count; ++index)
        this.PlotFlag(memberDataList[index].Location, MiniMapFlag.Enemy);
    }
    if (AllianceWarPayload.Instance.TrackData.TrackedFortressLocation.K <= 0)
      return;
    this.PlotFlag(AllianceWarPayload.Instance.TrackData.TrackedFortressLocation, MiniMapFlag.EnemyFortress);
  }

  private void ClearAllianceWarEnemy()
  {
    int childCount = this.AllianceWarControl.transform.childCount;
    for (int index = 0; index < childCount; ++index)
    {
      Transform child = this.AllianceWarControl.transform.GetChild(index);
      if ((bool) ((Object) child))
      {
        NGUITools.SetActive(child.gameObject, false);
        Object.Destroy((Object) child.gameObject);
      }
    }
  }

  public void MoveLocation(Coordinate location)
  {
    this.MoveGameObject(location, this.Location);
  }

  public void OnRight()
  {
    int kingdomX = this.m_KingdomX + 1;
    int kingdomY = this.m_KingdomY;
    MiniMapSystem.Instance.SwitchKingdom(PVPMapData.MapData.FindKingdomByLocation(kingdomX, kingdomY));
  }

  public void OnLeft()
  {
    int kingdomX = this.m_KingdomX - 1;
    int kingdomY = this.m_KingdomY;
    MiniMapSystem.Instance.SwitchKingdom(PVPMapData.MapData.FindKingdomByLocation(kingdomX, kingdomY));
  }

  public void OnTop()
  {
    int kingdomX = this.m_KingdomX;
    int kingdomY = this.m_KingdomY + 1;
    MiniMapSystem.Instance.SwitchKingdom(PVPMapData.MapData.FindKingdomByLocation(kingdomX, kingdomY));
  }

  public void OnBottom()
  {
    int kingdomX = this.m_KingdomX;
    int kingdomY = this.m_KingdomY - 1;
    MiniMapSystem.Instance.SwitchKingdom(PVPMapData.MapData.FindKingdomByLocation(kingdomX, kingdomY));
  }

  private void UpdateWorld()
  {
    this.MoveLocation(PVPSystem.Instance.Map.CurrentKXY);
    this.PlotFlag(PlayerData.inst.playerCityData.cityLocation, MiniMapFlag.Yourself);
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
    if (allianceData != null)
    {
      Dictionary<long, AllianceMemberData>.Enumerator enumerator = allianceData.members.datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        AllianceMemberData allianceMemberData = enumerator.Current.Value;
        if (allianceMemberData.uid != PlayerData.inst.uid)
        {
          CityData byUid = DBManager.inst.DB_City.GetByUid(allianceMemberData.uid);
          switch (allianceMemberData.title)
          {
            case 5:
            case 6:
              this.PlotFlag(byUid.cityLocation, MiniMapFlag.Leader);
              continue;
            default:
              this.PlotFlag(byUid.cityLocation, MiniMapFlag.Member);
              continue;
          }
        }
      }
    }
    AllianceFortressData myMainFortress = DBManager.inst.DB_AllianceFortress.GetMyMainFortress();
    if (myMainFortress == null)
      return;
    this.PlotFlag(myMainFortress.Location, MiniMapFlag.Fortress);
  }

  private void PlotFlag(Coordinate location, MiniMapFlag flag)
  {
    if (location.K != this.m_ActiveKingdom.WorldID)
      return;
    GameObject go = (GameObject) null;
    switch (flag)
    {
      case MiniMapFlag.Yourself:
        go = Object.Instantiate<GameObject>(MiniMapSystem.Instance.Yourself.gameObject);
        break;
      case MiniMapFlag.Leader:
        go = Object.Instantiate<GameObject>(MiniMapSystem.Instance.Leader.gameObject);
        break;
      case MiniMapFlag.Member:
        go = Object.Instantiate<GameObject>(MiniMapSystem.Instance.Member.gameObject);
        break;
      case MiniMapFlag.Fortress:
        go = Object.Instantiate<GameObject>(MiniMapSystem.Instance.Fortress.gameObject);
        break;
      case MiniMapFlag.Enemy:
        go = Object.Instantiate<GameObject>(MiniMapSystem.Instance.Enemy.gameObject);
        break;
      case MiniMapFlag.EnemyFortress:
        go = Object.Instantiate<GameObject>(MiniMapSystem.Instance.EnemyFortress.gameObject);
        break;
    }
    if (!(bool) ((Object) go))
      return;
    go.SetActive(true);
    switch (flag)
    {
      case MiniMapFlag.Yourself:
        go.transform.parent = this.gameObject.transform;
        break;
      case MiniMapFlag.Enemy:
      case MiniMapFlag.EnemyFortress:
        go.transform.parent = this.AllianceWarControl.gameObject.transform;
        break;
      default:
        go.transform.parent = this.AllianceControl.gameObject.transform;
        break;
    }
    this.m_LandMarkList.Add(go);
    MiniMapLandMark component = go.GetComponent<MiniMapLandMark>();
    component.X = location.X;
    component.Y = location.Y;
    this.MoveGameObject(location, go);
  }

  private void MoveGameObject(Coordinate location, GameObject go)
  {
    Vector2 worldPoint = this.ConvertXYToWorldPoint(location.X, location.Y);
    go.transform.position = (Vector3) worldPoint;
    go.transform.rotation = Quaternion.identity;
    go.transform.localScale = Vector3.one;
  }

  public Vector2 ConvertXYToWorldPoint(int tileX, int tileY)
  {
    return MiniMapController.ConvertKXYToWorldPoint(this.m_KingdomX, this.m_KingdomY, tileX, tileY);
  }

  public static Vector2 ConvertKXYToWorldPoint(int kingdomX, int kingdomY, int tileX, int tileY)
  {
    float num1 = (float) kingdomX + (float) tileX / PVPMapData.MapData.TilesPerKingdom.x;
    float num2 = (float) kingdomY - (float) tileY / PVPMapData.MapData.TilesPerKingdom.y;
    return new Vector2(num1 * MiniMapSystem.Inner.x, num2 * MiniMapSystem.Inner.y);
  }
}
