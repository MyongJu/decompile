﻿// Decompiled with JetBrains decompiler
// Type: LegendSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UnityEngine;

public class LegendSlot : MonoBehaviour
{
  public System.Action<long> onSlotClick;
  private long _legendId;
  private bool _isChecked;
  [SerializeField]
  private LegendSlot.Panel panel;

  public long legendId
  {
    get
    {
      return this._legendId;
    }
    set
    {
      if (this._legendId == value)
        return;
      this._legendId = value;
      this.SetHeroInfo();
    }
  }

  public bool isChecked
  {
    get
    {
      return this._isChecked;
    }
    set
    {
      this._isChecked = value;
      this.panel.Checker.SetActive(this._isChecked);
    }
  }

  public void SetHeroInfo()
  {
    if (ConfigManager.inst.DB_Legend.GetLegendInfo(DBManager.inst.DB_Legend.GetLegend(this.legendId).LegendID) == null)
      return;
    this.panel.legend.SetData(this.legendId);
    this.isChecked = false;
  }

  public void OnSlotClick()
  {
    if (this.onSlotClick == null)
      return;
    this.onSlotClick(this._legendId);
  }

  [Serializable]
  protected class Panel
  {
    public LegendItem legend;
    public GameObject Checker;
  }
}
