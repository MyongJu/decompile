﻿// Decompiled with JetBrains decompiler
// Type: EquipBasePresent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class EquipBasePresent : BasePresent
{
  public override void Refresh()
  {
    if (this.Data.Type == PresentData.DataType.Quest)
      this.UseStats();
    else if (this.Formula.IndexOf("quality") > -1)
    {
      int count = this.Count;
      int num1 = this.Formula.IndexOf('=');
      string s = this.Formula.Substring(num1 + 1, this.Formula.Length - (num1 + 1));
      int result = -1;
      int.TryParse(s, out result);
      StatsData statsData = DBManager.inst.DB_Stats.Get("level=-1&quality=" + s);
      int num2 = 0;
      if (statsData != null)
        num2 = (int) statsData.count;
      if (num2 > count)
        num2 = count;
      this.ProgressValue = (float) num2 / (float) count;
      this.ProgressContent = num2.ToString() + "/" + (object) count;
    }
    else
      this.UseStats();
  }
}
