﻿// Decompiled with JetBrains decompiler
// Type: TweenWidth
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[AddComponentMenu("NGUI/Tween/Tween Width")]
[RequireComponent(typeof (UIWidget))]
public class TweenWidth : UITweener
{
  public int from = 100;
  public int to = 100;
  public bool updateTable;
  private UIWidget mWidget;
  private UITable mTable;

  public UIWidget cachedWidget
  {
    get
    {
      if ((UnityEngine.Object) this.mWidget == (UnityEngine.Object) null)
        this.mWidget = this.GetComponent<UIWidget>();
      return this.mWidget;
    }
  }

  [Obsolete("Use 'value' instead")]
  public int width
  {
    get
    {
      return this.value;
    }
    set
    {
      this.value = value;
    }
  }

  public int value
  {
    get
    {
      return this.cachedWidget.width;
    }
    set
    {
      this.cachedWidget.width = value;
    }
  }

  protected override void OnUpdate(float factor, bool isFinished)
  {
    this.value = Mathf.RoundToInt((float) ((double) this.from * (1.0 - (double) factor) + (double) this.to * (double) factor));
    if (!this.updateTable)
      return;
    if ((UnityEngine.Object) this.mTable == (UnityEngine.Object) null)
    {
      this.mTable = NGUITools.FindInParents<UITable>(this.gameObject);
      if ((UnityEngine.Object) this.mTable == (UnityEngine.Object) null)
      {
        this.updateTable = false;
        return;
      }
    }
    this.mTable.repositionNow = true;
  }

  public static TweenWidth Begin(UIWidget widget, float duration, int width)
  {
    TweenWidth tweenWidth = UITweener.Begin<TweenWidth>(widget.gameObject, duration);
    tweenWidth.from = widget.width;
    tweenWidth.to = width;
    if ((double) duration <= 0.0)
    {
      tweenWidth.Sample(1f, true);
      tweenWidth.enabled = false;
    }
    return tweenWidth;
  }

  [ContextMenu("Set 'From' to current value")]
  public override void SetStartToCurrentValue()
  {
    this.from = this.value;
  }

  [ContextMenu("Set 'To' to current value")]
  public override void SetEndToCurrentValue()
  {
    this.to = this.value;
  }

  [ContextMenu("Assume value of 'From'")]
  private void SetCurrentValueToStart()
  {
    this.value = this.from;
  }

  [ContextMenu("Assume value of 'To'")]
  private void SetCurrentValueToEnd()
  {
    this.value = this.to;
  }
}
