﻿// Decompiled with JetBrains decompiler
// Type: NativeManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using System;
using UnityEngine;

public class NativeManager : FunplusSdkUtils.IDelegate
{
  private static NativeManager _instance;

  public static NativeManager inst
  {
    get
    {
      if (NativeManager._instance == null)
        NativeManager._instance = new NativeManager();
      return NativeManager._instance;
    }
  }

  public void OnFunplusSdkUtilsGetGaidCallback(string gaid)
  {
    this.GAID = gaid;
  }

  public void GetGaid()
  {
    if (Application.platform != RuntimePlatform.Android)
      return;
    FunplusSdkUtils.Instance.SetDelegate((FunplusSdkUtils.IDelegate) this);
    FunplusSdkUtils.Instance.GetGAID();
  }

  public string AppMajorVersion { get; set; }

  public string GAID { get; private set; }

  public string AppMinorVersion { get; set; }

  public string BuildNumber { get; set; }

  public string GetOS()
  {
    switch (Application.platform)
    {
      case RuntimePlatform.IPhonePlayer:
        return "ios";
      case RuntimePlatform.Android:
        return "android";
      default:
        return "editor_android";
    }
  }

  public string VersionContent { get; set; }

  public string AppVersion
  {
    get
    {
      return string.Format("{0}.{1}.{2}", (object) this.AppMajorVersion, (object) this.AppMinorVersion, (object) this.BuildNumber);
    }
  }

  public string DeviceName
  {
    get
    {
      return ((Enum) Application.platform).ToString();
    }
  }

  public string DeviceVersion
  {
    get
    {
      return "0.0.00";
    }
  }

  public string OperatingSystem
  {
    get
    {
      return SystemInfo.operatingSystem;
    }
  }

  public bool IsReachableViaLocalAreaNetwork
  {
    get
    {
      return Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork;
    }
  }

  public bool IsReachableViaCarrierDataNetwork
  {
    get
    {
      return Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork;
    }
  }

  public string Idfa
  {
    get
    {
      return string.Empty;
    }
  }

  public string Idfv
  {
    get
    {
      return string.Empty;
    }
  }

  public string AndroidId
  {
    get
    {
      return string.Empty;
    }
  }

  public string DeviceId
  {
    get
    {
      return SystemInfo.deviceUniqueIdentifier;
    }
  }

  public string SysLanguage
  {
    get
    {
      string str = string.Empty;
      string language = FunplusSdkUtils.Instance.GetLanguage();
      if (!string.IsNullOrEmpty(language))
      {
        str = language;
        string country = FunplusSdkUtils.Instance.GetCountry();
        if (!string.IsNullOrEmpty(country))
          str = language + "-" + country.ToUpper();
      }
      return str;
    }
  }

  public bool IsIPHONEX()
  {
    return false;
  }
}
