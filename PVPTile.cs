﻿// Decompiled with JetBrains decompiler
// Type: PVPTile
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UnityEngine;

public class PVPTile : ISortingLayerCalculator
{
  private static string[] SORTING_LAYER_NAMES = new string[33]
  {
    "Default",
    "Layer0",
    "Layer1",
    "Layer2",
    "Layer3",
    "Layer4",
    "Layer5",
    "Layer6",
    "Layer7",
    "Layer8",
    "Layer9",
    "Layer10",
    "Layer11",
    "Layer12",
    "Layer13",
    "Layer14",
    "Layer15",
    "Layer16",
    "Layer17",
    "Layer18",
    "Layer19",
    "Layer20",
    "Layer21",
    "Layer22",
    "Layer23",
    "Layer24",
    "Layer25",
    "Layer26",
    "Layer27",
    "Layer28",
    "Layer29",
    "Layer30",
    "Layer31"
  };
  private static string[] m_VFXPaths = new string[5]
  {
    "Prefab/VFX/fx_smoke",
    "Prefab/VFX/fx_shibai_fire",
    "Prefab/VFX/fx_border_fire_horizional",
    "Prefab/VFX/fx_border_fire_vertical",
    "Prefab/VFX/fx_shield"
  };
  private static int SORTING_LAYER_COUNT = PVPTile.SORTING_LAYER_NAMES.Length;
  private static int[] SORTING_LAYER_IDS = new int[PVPTile.SORTING_LAYER_COUNT];
  private long[] m_VFXHandles = new long[5]
  {
    -1L,
    -1L,
    -1L,
    -1L,
    -1L
  };
  private const string SMOKE_EFFECT = "Prefab/VFX/fx_smoke";
  private const string FIRE_EFFECT = "Prefab/VFX/fx_shibai_fire";
  private const string BORDER_HORIZIONAL_EFFECT = "Prefab/VFX/fx_border_fire_horizional";
  private const string BORDER_VERTICAL_EFFECT = "Prefab/VFX/fx_border_fire_vertical";
  private const string PEACE_SHIELD_EFFECT = "Prefab/VFX/fx_shield";
  private TileData m_TileData;
  private PVETile m_TileAsset;
  private SpriteBanner m_Banner;
  private SpriteIndicator m_Indicator;
  private PrefabSpawnRequestEx m_TileAssetRequest;
  private PrefabSpawnRequestEx m_BannerRequest;
  private PrefabSpawnRequestEx m_IndicatorRequest;
  private int m_SortingLayerIndex;
  private CityData.State m_CurrentState;
  private long m_PeaceShieldJobId;

  public PVPTile(TileData tileData)
  {
    this.m_TileData = tileData;
  }

  static PVPTile()
  {
    for (int index = 0; index < PVPTile.SORTING_LAYER_COUNT; ++index)
      PVPTile.SORTING_LAYER_IDS[index] = SortingLayer.NameToID(PVPTile.SORTING_LAYER_NAMES[index]);
  }

  public PVETile TileAsset
  {
    get
    {
      return this.m_TileAsset;
    }
  }

  public void Show()
  {
    this.GenerateOnDemand();
    this.UpdateSortingLayerIndex();
    this.UpdateSortingLayerAsset();
    this.UpdateSortingLayerBanner();
    this.UpdateSortingLayerIndicator();
    this.UpdateSortingLayerVFX();
  }

  public TileData TileData
  {
    get
    {
      return this.m_TileData;
    }
  }

  public void Hide()
  {
    if ((UnityEngine.Object) this.m_Banner != (UnityEngine.Object) null)
    {
      PrefabManagerEx.Instance.Destroy(this.m_Banner.gameObject);
      this.m_Banner = (SpriteBanner) null;
    }
    if ((UnityEngine.Object) this.m_Indicator != (UnityEngine.Object) null)
    {
      PrefabManagerEx.Instance.Destroy(this.m_Indicator.gameObject);
      this.m_Indicator = (SpriteIndicator) null;
    }
    if ((UnityEngine.Object) this.m_TileAsset != (UnityEngine.Object) null)
    {
      PrefabManagerEx.Instance.Destroy(this.m_TileAsset.gameObject);
      this.m_TileAsset = (PVETile) null;
    }
    if (this.m_BannerRequest != null)
    {
      this.m_BannerRequest.cancel = true;
      this.m_BannerRequest = (PrefabSpawnRequestEx) null;
    }
    if (this.m_IndicatorRequest != null)
    {
      this.m_IndicatorRequest.cancel = true;
      this.m_IndicatorRequest = (PrefabSpawnRequestEx) null;
    }
    if (this.m_TileAssetRequest != null)
    {
      this.m_TileAssetRequest.cancel = true;
      this.m_TileAssetRequest = (PrefabSpawnRequestEx) null;
    }
    this.StopCurrentEffect(PVPTile.VisualEffect.SMOKE);
    this.StopCurrentEffect(PVPTile.VisualEffect.FIRE);
    this.StopCurrentEffect(PVPTile.VisualEffect.BORDER_HORIZIONAL);
    this.StopCurrentEffect(PVPTile.VisualEffect.BORDER_VERTICAL);
    this.StopCurrentEffect(PVPTile.VisualEffect.PEACE_SHIELD);
    this.m_SortingLayerIndex = 0;
    this.m_CurrentState = CityData.State.normal;
    this.m_PeaceShieldJobId = 0L;
  }

  public void ResetMonster()
  {
    if (!((UnityEngine.Object) this.m_TileAsset != (UnityEngine.Object) null))
      return;
    foreach (MonsterAnimEvent componentsInChild in this.m_TileAsset.GetComponentsInChildren<MonsterAnimEvent>(true))
      componentsInChild.Reset();
  }

  public void AddAttacker(long marchId)
  {
    if (!((UnityEngine.Object) this.m_TileAsset != (UnityEngine.Object) null))
      return;
    foreach (MonsterAnimEvent componentsInChild in this.m_TileAsset.GetComponentsInChildren<MonsterAnimEvent>(true))
      componentsInChild.AddAttacker(marchId);
  }

  public void RemoveAttacker(long marchId)
  {
    if (!((UnityEngine.Object) this.m_TileAsset != (UnityEngine.Object) null))
      return;
    foreach (MonsterAnimEvent componentsInChild in this.m_TileAsset.GetComponentsInChildren<MonsterAnimEvent>(true))
      componentsInChild.RemoveAttacker(marchId);
  }

  public void Kill()
  {
    if (!((UnityEngine.Object) this.m_TileAsset != (UnityEngine.Object) null))
      return;
    foreach (MonsterAnimEvent componentsInChild in this.m_TileAsset.GetComponentsInChildren<MonsterAnimEvent>(true))
      componentsInChild.Kill();
  }

  public void UpdateUI()
  {
    this.UpdateBannerUI();
    this.UpdateIndicatorUI();
    this.UpdateTileAssetUI();
  }

  protected void UpdateSpellAbleUI()
  {
    if (!((UnityEngine.Object) this.m_TileAsset != (UnityEngine.Object) null))
      return;
    Twinkler component = this.m_TileAsset.GetComponent<Twinkler>();
    switch (this.m_TileData.CurrentSpellAbleState)
    {
      case TileData.SpellAbleState.SpellAbleNone:
        if ((bool) ((UnityEngine.Object) component))
        {
          UnityEngine.Object.DestroyImmediate((UnityEngine.Object) component);
          break;
        }
        break;
      case TileData.SpellAbleState.SpellAbleTrue:
        if ((bool) ((UnityEngine.Object) component) && component.CurrentMode != Twinkler.Mode.LOOP)
        {
          UnityEngine.Object.DestroyImmediate((UnityEngine.Object) component);
          break;
        }
        break;
      case TileData.SpellAbleState.SpellAbleFalse:
        if ((bool) ((UnityEngine.Object) component) && component.CurrentMode != Twinkler.Mode.DARK)
        {
          UnityEngine.Object.DestroyImmediate((UnityEngine.Object) component);
          break;
        }
        break;
    }
    switch (this.m_TileData.CurrentSpellAbleState)
    {
      case TileData.SpellAbleState.SpellAbleTrue:
        if ((bool) ((UnityEngine.Object) component))
          break;
        this.m_TileAsset.gameObject.AddComponent<Twinkler>().Twinkle(Twinkler.Mode.LOOP);
        break;
      case TileData.SpellAbleState.SpellAbleFalse:
        if ((bool) ((UnityEngine.Object) component))
          break;
        this.m_TileAsset.gameObject.AddComponent<Twinkler>().Twinkle(Twinkler.Mode.DARK);
        break;
    }
  }

  public void UpdateVFX()
  {
    if (!((UnityEngine.Object) this.m_TileAsset != (UnityEngine.Object) null) || this.m_TileData == null || this.m_TileData.TileType != TileType.City)
      return;
    CityData cityData = DBManager.inst.DB_City.Get(this.m_TileData.CityID);
    if (cityData == null)
      return;
    if (cityData.state != this.m_CurrentState || this.m_PeaceShieldJobId != cityData.peaceShieldJobId)
    {
      this.m_CurrentState = cityData.state;
      this.m_PeaceShieldJobId = cityData.peaceShieldJobId;
      this.StopCurrentEffect(PVPTile.VisualEffect.PEACE_SHIELD);
      this.StopCurrentEffect(PVPTile.VisualEffect.SMOKE);
      this.StopCurrentEffect(PVPTile.VisualEffect.FIRE);
      if (cityData.peaceShieldJobId != 0L)
        this.PlayEffect(PVPTile.VisualEffect.PEACE_SHIELD, this.m_TileAsset.transform);
      else if (cityData.state == CityData.State.onfire)
        this.PlayEffect(PVPTile.VisualEffect.FIRE, this.m_TileAsset.transform);
      else if (cityData.state == CityData.State.smoke)
        this.PlayEffect(PVPTile.VisualEffect.SMOKE, this.m_TileAsset.transform);
    }
    this.UpdateSortingLayerVFX();
  }

  private void GenerateOnDemand()
  {
    if (this.m_TileData == null)
      return;
    if ((UnityEngine.Object) this.m_TileAsset == (UnityEngine.Object) null && this.m_TileAssetRequest == null)
      this.m_TileAssetRequest = this.GenerateTileAsset(this.m_TileData, new PrefabSpawnRequestCallback(this.OnTileAssetLoaded));
    if ((UnityEngine.Object) this.m_Banner == (UnityEngine.Object) null && this.m_BannerRequest == null && (SpriteBanner.CanHaveBanner(this.m_TileData.TileType) && !this.m_TileData.UseReference))
      this.m_BannerRequest = PVPTile.GenerateTileAssetByName(this.GetBannerSpriteName(this.m_TileData.TileType, this.m_TileData.Level), PVPSystem.Instance.Map.Bannar, new PrefabSpawnRequestCallback(this.OnBannerLoaded), (object) null);
    if (!((UnityEngine.Object) this.m_Indicator == (UnityEngine.Object) null) || this.m_IndicatorRequest != null || (!ResSpriteIndicator.CanHaveIndicator(this.m_TileData) || this.m_TileData.UseReference))
      return;
    this.m_IndicatorRequest = PVPTile.GenerateTileAssetByName("PVPSpriteIndicator", PVPSystem.Instance.Map.Bannar, new PrefabSpawnRequestCallback(this.OnIndicatorLoaded), (object) null);
  }

  private void OnBannerLoaded(GameObject go, object userData)
  {
    this.m_BannerRequest = (PrefabSpawnRequestEx) null;
    if (!(bool) ((UnityEngine.Object) go))
      return;
    go.transform.localPosition = this.m_TileData.Position;
    go.transform.localScale = Vector3.one;
    this.m_Banner = go.GetComponent<SpriteBanner>();
    this.UpdateBannerUI();
    this.UpdateSortingLayerBanner();
  }

  private void UpdateBannerUI()
  {
    if (!((UnityEngine.Object) this.m_Banner != (UnityEngine.Object) null))
      return;
    this.m_Banner.UpdateUI(this.m_TileData);
  }

  private void OnIndicatorLoaded(GameObject go, object userData)
  {
    this.m_IndicatorRequest = (PrefabSpawnRequestEx) null;
    if (!(bool) ((UnityEngine.Object) go))
      return;
    go.transform.localPosition = this.m_TileData.Position;
    go.transform.localScale = Vector3.one;
    this.m_Indicator = go.GetComponent<SpriteIndicator>();
    this.UpdateIndicatorUI();
    this.UpdateSortingLayerIndicator();
  }

  private void UpdateIndicatorUI()
  {
    if (!((UnityEngine.Object) this.m_Indicator != (UnityEngine.Object) null))
      return;
    this.m_Indicator.UpdateUI(this.m_TileData);
  }

  private void OnTileAssetLoaded(GameObject go, object userData)
  {
    this.m_TileAssetRequest = (PrefabSpawnRequestEx) null;
    if (!(bool) ((UnityEngine.Object) go))
      return;
    go.transform.localPosition = this.m_TileData.Position;
    go.transform.localScale = Vector3.one;
    this.m_TileAsset = go.GetComponent<PVETile>();
    WorldEncounterData worldEncounterData = userData as WorldEncounterData;
    if (worldEncounterData != null)
      go.GetComponentsInChildren<PVEMonsterTile>(true)[0].CreateModel(worldEncounterData.type, (System.Action<GameObject, object>) null, (object) null);
    FestivalVisitInfo festivalVisitInfo = userData as FestivalVisitInfo;
    if (festivalVisitInfo != null)
      go.GetComponentsInChildren<PVEMonsterTile>(true)[0].CreateModel(festivalVisitInfo.type, (System.Action<GameObject, object>) null, (object) null);
    WorldBossData worldBossData = userData as WorldBossData;
    if (worldBossData != null)
    {
      WorldBossStaticInfo data = ConfigManager.inst.DB_WorldBoss.GetData(worldBossData.configId);
      if (data != null)
        go.GetComponentsInChildren<PVEMonsterTile>(true)[0].CreateModel(data.Type, (System.Action<GameObject, object>) null, (object) null);
    }
    KingdomBossData kingdomBossData = userData as KingdomBossData;
    if (kingdomBossData != null)
    {
      KingdomBossStaticInfo kingdomBossStaticInfo = ConfigManager.inst.DB_KingdomBoss.GetItem(kingdomBossData.configId);
      if (kingdomBossStaticInfo != null)
        go.GetComponentsInChildren<PVEMonsterTile>(true)[0].CreateModel(kingdomBossStaticInfo.Type, (System.Action<GameObject, object>) null, (object) null);
    }
    this.GenerageKingdomBorderVisualEffects();
    this.UpdateSortingLayerIndex();
    this.UpdateSortingLayerAsset();
    this.UpdateVFX();
    this.UpdateTileAssetUI();
  }

  private void UpdateTileAssetUI()
  {
    if ((UnityEngine.Object) this.m_TileAsset != (UnityEngine.Object) null)
      this.m_TileAsset.UpdateUI(this.m_TileData);
    this.UpdateSpellAbleUI();
  }

  private void GenerageKingdomBorderVisualEffects()
  {
    WorldCoordinate worldLocation = this.m_TileData.WorldLocation;
    if (this.m_TileData.Location.Y == 0)
      this.PlayEffect(PVPTile.VisualEffect.BORDER_HORIZIONAL, this.m_TileAsset.transform);
    else if (this.ShouldDrawBottomBorder(worldLocation))
    {
      this.PlayEffect(PVPTile.VisualEffect.BORDER_HORIZIONAL, this.m_TileAsset.transform);
      IVfx vfx = this.GetVfx(PVPTile.VisualEffect.BORDER_HORIZIONAL);
      if (vfx != null)
      {
        Vector3 localPosition = vfx.gameObject.transform.localPosition;
        localPosition.x -= 150f;
        localPosition.y -= 75f;
        vfx.gameObject.transform.localPosition = localPosition;
      }
    }
    else
      this.StopCurrentEffect(PVPTile.VisualEffect.BORDER_HORIZIONAL);
    if (this.m_TileData.Location.X == 0)
      this.PlayEffect(PVPTile.VisualEffect.BORDER_VERTICAL, this.m_TileAsset.transform);
    else if (this.ShouldDrawRightBorder(worldLocation))
    {
      this.PlayEffect(PVPTile.VisualEffect.BORDER_VERTICAL, this.m_TileAsset.transform);
      IVfx vfx = this.GetVfx(PVPTile.VisualEffect.BORDER_VERTICAL);
      if (vfx != null)
      {
        Vector3 localPosition = vfx.gameObject.transform.localPosition;
        localPosition.x += 150f;
        localPosition.y += 75f;
        vfx.gameObject.transform.localPosition = localPosition;
      }
    }
    else
      this.StopCurrentEffect(PVPTile.VisualEffect.BORDER_VERTICAL);
    this.UpdateSortingLayerVFX();
  }

  private bool ShouldDrawRightBorder(WorldCoordinate worldLocation)
  {
    ++worldLocation.X;
    return !this.m_TileData.Container.MapData.IsKingdomActiveAt(worldLocation);
  }

  private bool ShouldDrawBottomBorder(WorldCoordinate worldLocation)
  {
    ++worldLocation.Y;
    return !this.m_TileData.Container.MapData.IsKingdomActiveAt(worldLocation);
  }

  private IVfx GetVfx(PVPTile.VisualEffect slot)
  {
    return VfxManager.Instance.GetVfxByGuid(this.m_VFXHandles[(int) slot]);
  }

  private void UpdateSortingLayerIndex()
  {
    if (!((UnityEngine.Object) this.m_TileAsset != (UnityEngine.Object) null) || this.m_TileData == null)
      return;
    this.m_SortingLayerIndex = PVPTile.CalculateSortingLayerIndex(this.m_TileData.Container.Viewport, this.m_TileData.Position.y, (float) this.m_TileAsset.ySize, this.m_TileData.Container.MapData);
  }

  public int GetSortingLayerID()
  {
    this.UpdateSortingLayerIndex();
    return PVPTile.GetSortingLayerID(this.m_SortingLayerIndex);
  }

  public string GetSortingLayerName()
  {
    this.UpdateSortingLayerIndex();
    return PVPTile.GetSortingLayerName(this.m_SortingLayerIndex);
  }

  private void UpdateSortingLayerVFX()
  {
    for (int index = 0; index < this.m_VFXHandles.Length; ++index)
    {
      IVfx vfxByGuid = VfxManager.Instance.GetVfxByGuid(this.m_VFXHandles[index]);
      if (vfxByGuid != null && vfxByGuid.IsAlive)
      {
        RendererSortingLayerIdModifier componentInChildren = vfxByGuid.gameObject.GetComponentInChildren<RendererSortingLayerIdModifier>();
        if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
          componentInChildren.SortingLayerName = PVPTile.GetSortingLayerName(this.m_SortingLayerIndex);
      }
    }
  }

  private void UpdateSortingLayerAsset()
  {
    if (!(bool) ((UnityEngine.Object) this.m_TileAsset))
      return;
    this.m_TileAsset.SetSortingLayerID((ISortingLayerCalculator) this);
  }

  private void UpdateSortingLayerBanner()
  {
    if (!(bool) ((UnityEngine.Object) this.m_Banner))
      return;
    this.m_Banner.SetSortingLayerID((ISortingLayerCalculator) this);
  }

  private void UpdateSortingLayerIndicator()
  {
    if (!(bool) ((UnityEngine.Object) this.m_Indicator))
      return;
    this.m_Indicator.SetSortingLayerID((ISortingLayerCalculator) this);
  }

  private string GetBannerSpriteName(TileType type, int level)
  {
    switch (type)
    {
      case TileType.City:
        return "PVPCityBanner";
      case TileType.Camp:
        return "PVPCampBanner";
      case TileType.Resource1:
      case TileType.Resource2:
      case TileType.Resource3:
      case TileType.Resource4:
        return "PVPResourceBanner";
      case TileType.Encounter:
      case TileType.GveCamp:
      case TileType.EventVisitNPC:
        return "PVPEncounterBanner";
      case TileType.Wonder:
        return "PVPWonderBanner";
      case TileType.WonderTower:
        return "PVPWonderTowerBanner";
      case TileType.AllianceFortress:
        return "PVPFortressBanner";
      case TileType.AllianceWarehouse:
        return "PVPStoreHouseBanner";
      case TileType.AllianceSuperMine:
        return "PVPSuperMineBanner";
      case TileType.AlliancePortal:
        return "PVPPortalBanner";
      case TileType.AllianceHospital:
        return "PVPHospitalBanner";
      case TileType.AllianceTemple:
        return "PVPTempleBanner";
      case TileType.WorldBoss:
        return "PVPWorldBossBanner";
      case TileType.Digsite:
        return "PVPDigsiteBanner";
      case TileType.PitMine:
        return level < 4 ? "PVPResourceBanner" : "PVPSupperPitMineBanner";
      case TileType.KingdomBoss:
        return "PVPKingdomBossBanner";
      default:
        return string.Empty;
    }
  }

  private PrefabSpawnRequestEx GenerateTileAsset(TileData tile, PrefabSpawnRequestCallback callback)
  {
    if (!PVPSystem.Instance.IsAvailable)
      return (PrefabSpawnRequestEx) null;
    if (tile.UseReference)
      return PVPTile.GenerateTileAssetByName("tiles_blank", PVPSystem.Instance.Map.Terrains, callback, (object) null);
    TileType tileType = tile.TileType;
    if (tileType >= TileType.Count)
    {
      string name = string.Empty;
      this.m_TileData.Container.MapData.DefaultAssets.TryGetValue((int) tileType, out name);
      name = !string.IsNullOrEmpty(name) ? name : "tiles_unknown";
      return PVPTile.GenerateTileAssetByName(name, PVPSystem.Instance.Map.Wonders, callback, (object) null);
    }
    if (tileType == TileType.Encounter)
    {
      WorldEncounterData data = ConfigManager.inst.DB_WorldEncount.GetData((long) tile.ResourceId);
      if (data != null)
        return PVPTile.GenerateTileAssetByName("MONSTER_TEST_0", PVPSystem.Instance.Map.Monsters, callback, (object) data);
      return (PrefabSpawnRequestEx) null;
    }
    if (tileType == TileType.WorldBoss)
    {
      WorldBossData worldBossData = DBManager.inst.DB_WorldBossDB.Get(tile.ResourceId);
      if (worldBossData != null)
        return PVPTile.GenerateTileAssetByName("WORLD_BOSS", PVPSystem.Instance.Map.Monsters, callback, (object) worldBossData);
      return (PrefabSpawnRequestEx) null;
    }
    if (tileType == TileType.KingdomBoss)
    {
      KingdomBossData kingdomBossData = DBManager.inst.DB_KingdomBossDB.Get(tile.ResourceId);
      if (kingdomBossData != null)
        return PVPTile.GenerateTileAssetByName("WORLD_BOSS", PVPSystem.Instance.Map.Monsters, callback, (object) kingdomBossData);
      return (PrefabSpawnRequestEx) null;
    }
    if (tileType == TileType.EventVisitNPC)
    {
      FestivalVisitInfo data = ConfigManager.inst.DB_FestivalVisit.GetData(tile.ResourceId);
      if (data != null)
        return PVPTile.GenerateTileAssetByName("MONSTER_TEST_0", PVPSystem.Instance.Map.Monsters, callback, (object) data);
      return (PrefabSpawnRequestEx) null;
    }
    if (tileType == TileType.GveCamp)
      return PVPTile.GenerateTileAssetByName("tiles_gve_camp", PVPSystem.Instance.Map.Wonders, callback, (object) null);
    if (tileType == TileType.Digsite)
      return PVPTile.GenerateTileAssetByName("tiles_digsite", PVPSystem.Instance.Map.Resources, callback, (object) null);
    if (tileType == TileType.City)
    {
      CityData cityData = DBManager.inst.DB_City.Get(tile.CityID);
      if (cityData != null)
      {
        int index = Math.Min(Math.Max(0, cityData.buildings.level_stronghold - 1), MapUtils.CityPrefabNames.Length - 1);
        return PVPTile.GenerateTileAssetByName(MapUtils.CityPrefabNames[index], PVPSystem.Instance.Map.Cities, callback, (object) null);
      }
    }
    else
    {
      if (tileType == TileType.AllianceFortress)
        return PVPTile.GenerateTileAssetByName(MapUtils.GetAllianceBuildingPrefab(ConfigManager.inst.DB_AllianceBuildings.Get(tile.FortressData.ConfigId).type), PVPSystem.Instance.Map.Fortress, callback, (object) null);
      if (tileType == TileType.AllianceWarehouse)
        return PVPTile.GenerateTileAssetByName(MapUtils.GetAllianceBuildingPrefab(2), PVPSystem.Instance.Map.Fortress, callback, (object) null);
      if (tileType == TileType.AlliancePortal)
        return PVPTile.GenerateTileAssetByName(MapUtils.GetAllianceBuildingPrefab(7), PVPSystem.Instance.Map.Fortress, callback, (object) null);
      if (tileType == TileType.AllianceHospital)
        return PVPTile.GenerateTileAssetByName(MapUtils.GetAllianceBuildingPrefab(8), PVPSystem.Instance.Map.Fortress, callback, (object) null);
      if (tileType == TileType.AllianceTemple)
        return PVPTile.GenerateTileAssetByName(MapUtils.GetAllianceBuildingPrefab(9), PVPSystem.Instance.Map.Fortress, callback, (object) null);
      if (tileType == TileType.AllianceSuperMine)
      {
        if (tile.SuperMineData != null)
          return PVPTile.GenerateTileAssetByName(MapUtils.GetAllianceBuildingPrefab(ConfigManager.inst.DB_AllianceBuildings.Get(tile.SuperMineData.ConfigId).type), PVPSystem.Instance.Map.Fortress, callback, (object) null);
        return (PrefabSpawnRequestEx) null;
      }
      if (tileType == TileType.Resource1)
        return PVPTile.GenerateTileAssetByName("tiles_farm_l1_01", PVPSystem.Instance.Map.Resources, callback, (object) null);
      if (tileType == TileType.Resource2)
        return PVPTile.GenerateTileAssetByName("tiles_lumber_l1_01", PVPSystem.Instance.Map.Resources, callback, (object) null);
      if (tileType == TileType.Resource3)
        return PVPTile.GenerateTileAssetByName("tiles_mine_l1_01", PVPSystem.Instance.Map.Resources, callback, (object) null);
      if (tileType == TileType.Resource4)
        return PVPTile.GenerateTileAssetByName("tiles_house_l1_01", PVPSystem.Instance.Map.Resources, callback, (object) null);
      if (tileType == TileType.PitMine)
      {
        if (tile.Level >= 4)
          return PVPTile.GenerateTileAssetByName("tiles_pit_supper_mine", PVPSystem.Instance.Map.Resources, callback, (object) null);
        return PVPTile.GenerateTileAssetByName("tiles_pit_mine", PVPSystem.Instance.Map.Resources, callback, (object) null);
      }
      if (tileType == TileType.Camp)
        return PVPTile.GenerateTileAssetByName("tiles_enemycroop_camp", PVPSystem.Instance.Map.Resources, callback, (object) null);
      if (tileType == TileType.StaticTerrain || tileType == TileType.Mountain || tileType == TileType.WonderTree)
        return this.GenerateTileAssetByTileID(tile.TileID, PVPSystem.Instance.Map.Terrains, callback, (object) null);
      switch (tileType)
      {
        case TileType.Terrain:
          if (!SettingManager.Instance.IsEnvironmentOff)
            return this.GenerateTileAssetByTileID(tile.TileID, PVPSystem.Instance.Map.Terrains, callback, (object) null);
          return PVPTile.GenerateTileAssetByName("tiles_blank", PVPSystem.Instance.Map.Terrains, callback, (object) null);
        case TileType.Water:
          return this.GenerateTileAssetByTileID(tile.TileID, PVPSystem.Instance.Map.Water, callback, (object) null);
      }
    }
    return this.GenerateTileAssetByTileID(tile.TileID, PVPSystem.Instance.Map.transform, callback, (object) null);
  }

  private PrefabSpawnRequestEx GenerateTileAssetByTileID(long tileID, Transform parent, PrefabSpawnRequestCallback callback, object userData = null)
  {
    if (!PVPSystem.Instance.IsAvailable || (UnityEngine.Object) PVPSystem.Instance.Map == (UnityEngine.Object) null)
      return (PrefabSpawnRequestEx) null;
    TileDefinition tileDefinition = (TileDefinition) null;
    this.m_TileData.Container.MapData.Tileset.TryGetValue(tileID, out tileDefinition);
    if (tileDefinition != null)
      return PrefabManagerEx.Instance.SpawnAsync(tileDefinition.PrefabName, parent, callback, userData);
    return (PrefabSpawnRequestEx) null;
  }

  private static PrefabSpawnRequestEx GenerateTileAssetByName(string name, Transform parent, PrefabSpawnRequestCallback callback, object userData = null)
  {
    return PrefabManagerEx.Instance.SpawnAsync(name, parent, callback, userData);
  }

  private void PlayEffect(PVPTile.VisualEffect slot, Transform parent)
  {
    this.StopCurrentEffect(slot);
    this.m_VFXHandles[(int) slot] = VfxManager.Instance.CreateAndPlay(PVPTile.m_VFXPaths[(int) slot], parent);
  }

  private void StopCurrentEffect(PVPTile.VisualEffect effect)
  {
    if (this.m_VFXHandles[(int) effect] == -1L)
      return;
    VfxManager.Instance.Delete(this.m_VFXHandles[(int) effect]);
    this.m_VFXHandles[(int) effect] = -1L;
  }

  public static int CalculateSortingLayerIndex(Rect viewport, float py, float ySize, DynamicMapData mapData)
  {
    py = -py;
    float num1 = 0.5f * mapData.PixelsPerTile.y;
    float num2 = -viewport.y;
    return (int) (((double) py - (double) num2 - (double) num1 + (double) ySize * (double) num1) / (double) num1 + 0.5);
  }

  public static int GetSortingLayerID(int sortingLayerIndex)
  {
    if (0 <= sortingLayerIndex && sortingLayerIndex < PVPTile.SORTING_LAYER_COUNT)
      return PVPTile.SORTING_LAYER_IDS[sortingLayerIndex];
    return 0;
  }

  public static string GetSortingLayerName(int sortingLayerIndex)
  {
    if (0 <= sortingLayerIndex && sortingLayerIndex < PVPTile.SORTING_LAYER_COUNT)
      return PVPTile.SORTING_LAYER_NAMES[sortingLayerIndex];
    return PVPTile.SORTING_LAYER_NAMES[0];
  }

  private enum VisualEffect
  {
    SMOKE,
    FIRE,
    BORDER_HORIZIONAL,
    BORDER_VERTICAL,
    PEACE_SHIELD,
    COUNT,
  }
}
