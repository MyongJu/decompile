﻿// Decompiled with JetBrains decompiler
// Type: ConfigAltarSummonRewardMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAltarSummonRewardMain
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, ConfigAltarSummonRewardInfo> datas;
  private Dictionary<int, ConfigAltarSummonRewardInfo> dicByUniqueId;
  private Dictionary<string, List<ConfigAltarSummonRewardInfo>> rewardPoolByGroupId;

  public void BuildDB(object res)
  {
    this.parse.Parse<ConfigAltarSummonRewardInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.rewardPoolByGroupId = new Dictionary<string, List<ConfigAltarSummonRewardInfo>>();
    Dictionary<string, ConfigAltarSummonRewardInfo>.Enumerator enumerator = this.datas.GetEnumerator();
    while (enumerator.MoveNext())
    {
      ConfigAltarSummonRewardInfo summonRewardInfo = enumerator.Current.Value;
      if (this.rewardPoolByGroupId.ContainsKey(summonRewardInfo.groupId))
      {
        this.rewardPoolByGroupId[summonRewardInfo.groupId].Add(summonRewardInfo);
      }
      else
      {
        this.rewardPoolByGroupId.Add(summonRewardInfo.groupId, new List<ConfigAltarSummonRewardInfo>());
        this.rewardPoolByGroupId[summonRewardInfo.groupId].Add(summonRewardInfo);
      }
    }
  }

  public ConfigAltarSummonRewardInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (ConfigAltarSummonRewardInfo) null;
  }

  public ConfigAltarSummonRewardInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (ConfigAltarSummonRewardInfo) null;
  }

  public List<ConfigAltarSummonRewardInfo> GetRewardPoolByGroupId(string groupId)
  {
    if (this.rewardPoolByGroupId.ContainsKey(groupId))
      return this.rewardPoolByGroupId[groupId];
    return (List<ConfigAltarSummonRewardInfo>) null;
  }

  public void Clear()
  {
    if (this.datas != null)
      this.datas.Clear();
    if (this.dicByUniqueId != null)
      this.dicByUniqueId.Clear();
    if (this.rewardPoolByGroupId == null)
      return;
    this.rewardPoolByGroupId.Clear();
  }
}
