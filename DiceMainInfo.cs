﻿// Decompiled with JetBrains decompiler
// Type: DiceMainInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DiceMainInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "group")]
  public int groupId;
  [Config(Name = "reward")]
  public int rewardId;
  [Config(Name = "is_score")]
  public int isScore;
  [Config(Name = "number")]
  public int number;
  [Config(Name = "rank")]
  public int rank;

  public bool IsScore
  {
    get
    {
      return this.isScore == 1;
    }
  }
}
