﻿// Decompiled with JetBrains decompiler
// Type: AccountSwitchPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using I2.Loc;
using UI;
using UnityEngine;

public class AccountSwitchPopup : Popup
{
  public UIButton fbButton;
  public UIButton gpButton;
  public UIButton gcButton;
  public UIButton vkButton;
  public UIButton wechatButton;
  private FunplusAccountType loginType;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    if (Application.platform != RuntimePlatform.Android)
    {
      this.gpButton.gameObject.SetActive(false);
      this.gcButton.gameObject.SetActive(true);
    }
    if (Application.platform == RuntimePlatform.Android)
    {
      this.gcButton.gameObject.SetActive(false);
      this.gpButton.gameObject.SetActive(AccountManager.Instance.CanShowGoolgePlay());
    }
    NGUITools.SetActive(this.vkButton.gameObject, AccountManager.Instance.CanShowVK());
    NGUITools.SetActive(this.wechatButton.gameObject, AccountManager.Instance.CanShowWechat());
    NGUITools.SetActive(this.fbButton.gameObject, AccountManager.Instance.CanShowFacebook());
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnWechat()
  {
    if (!AccountManager.Instance.IsNotBindAccount)
      AccountManager.Instance.SwitchAccount(FunplusAccountType.FPAccountTypeWechat);
    else
      this.SwitchAccount(FunplusAccountType.FPAccountTypeWechat);
  }

  public void OnGC()
  {
    if (!AccountManager.Instance.IsNotBindAccount)
      AccountManager.Instance.SwitchAccount(FunplusAccountType.FPAccountTypeGameCenter);
    else
      this.SwitchAccount(FunplusAccountType.FPAccountTypeGameCenter);
  }

  private void AgainConfirm()
  {
    Utils.ShowWarning(ScriptLocalization.Get("account_warning_not_bound_reset_description", true), ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, (System.Action) (() => AccountManager.Instance.SwitchAccount(this.loginType)), (string) null);
  }

  public void OnFB()
  {
    if (!AccountManager.Instance.IsNotBindAccount)
      AccountManager.Instance.SwitchAccount(FunplusAccountType.FPAccountTypeFacebook);
    else
      this.SwitchAccount(FunplusAccountType.FPAccountTypeFacebook);
  }

  public void OnVK()
  {
    if (!AccountManager.Instance.IsNotBindAccount)
      AccountManager.Instance.SwitchAccount(FunplusAccountType.FPAccountTypeVK);
    else
      this.SwitchAccount(FunplusAccountType.FPAccountTypeVK);
  }

  public void OnEmail()
  {
  }

  private void SwitchAccount(FunplusAccountType type)
  {
    this.loginType = type;
    Utils.ShowWarning(ScriptLocalization.Get("account_warning_not_bound_switch_description", true), ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.AgainConfirm), (string) null);
  }

  public void OnGooglePlay()
  {
    if (!AccountManager.Instance.IsNotBindAccount)
      AccountManager.Instance.SwitchAccount(FunplusAccountType.FPAccountTypeGooglePlus);
    else
      this.SwitchAccount(FunplusAccountType.FPAccountTypeGooglePlus);
  }
}
