﻿// Decompiled with JetBrains decompiler
// Type: BookmarkPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BookmarkPopup : Popup
{
  private Dictionary<Coordinate, BookmarkItemRenderer> m_ItemDict = new Dictionary<Coordinate, BookmarkItemRenderer>();
  private GameObjectPool m_ItemPool = new GameObjectPool();
  private int m_SelectedIndex = -1;
  public const string TOAST = "toast_bookmark_success";
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  public UIToggle[] m_Categories;
  public GameObject m_ItemPrefab;
  public GameObject m_ItemRoot;
  public GameObject m_Tip;
  public UIToggle m_All;
  public UILabel m_Count;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DBManager.inst.DB_Bookmark.onDataReomve += new System.Action<Bookmark>(this.OnBookmarkDataRemove);
    DBManager.inst.DB_Bookmark.onDataUpdated += new System.Action<Bookmark>(this.OnBookmarkDataUpdated);
    this.m_ItemPool.Initialize(this.m_ItemPrefab, this.m_ItemRoot);
    this.m_SelectedIndex = -1;
    this.m_Categories[0].Set(true);
    this.m_All.Set(false);
    this.m_Count.text = "0";
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    DBManager.inst.DB_Bookmark.onDataReomve -= new System.Action<Bookmark>(this.OnBookmarkDataRemove);
    DBManager.inst.DB_Bookmark.onDataUpdated -= new System.Action<Bookmark>(this.OnBookmarkDataUpdated);
    this.ClearData();
    this.m_ItemPool.Clear();
  }

  public void OnCloseButtonPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnToggleValueChanged()
  {
    int selectedIndex = this.GetSelectedIndex();
    if (selectedIndex == this.m_SelectedIndex)
      return;
    this.m_SelectedIndex = selectedIndex;
    this.m_All.Set(false);
    this.m_Count.text = "0";
    this.UpdateData(this.m_SelectedIndex);
  }

  public void OnAllChanged()
  {
    Dictionary<Coordinate, BookmarkItemRenderer>.Enumerator enumerator = this.m_ItemDict.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Value.Selected = this.m_All.value;
    this.m_Count.text = this.SelectCount.ToString();
  }

  public void OnDeleteClick()
  {
    if (this.SelectCount <= 0)
      return;
    Hashtable postData = new Hashtable();
    postData[(object) "uid"] = (object) PlayerData.inst.uid;
    ArrayList arrayList = new ArrayList();
    Dictionary<Coordinate, BookmarkItemRenderer>.Enumerator enumerator = this.m_ItemDict.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.Value.Selected)
      {
        Bookmark bookmark = enumerator.Current.Value.Bookmark;
        arrayList.Add((object) new ArrayList()
        {
          (object) bookmark.coordinate.K,
          (object) bookmark.coordinate.X,
          (object) bookmark.coordinate.Y
        });
      }
    }
    postData[(object) "coords"] = (object) Utils.Object2Json((object) arrayList);
    MessageHub.inst.GetPortByAction("Player:deleteMultiBookmarks").SendRequest(postData, (System.Action<bool, object>) null, true);
  }

  private int SelectCount
  {
    get
    {
      int num = 0;
      Dictionary<Coordinate, BookmarkItemRenderer>.Enumerator enumerator = this.m_ItemDict.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.Selected)
          ++num;
      }
      return num;
    }
  }

  private int GetSelectedIndex()
  {
    for (int index = 0; index < this.m_Categories.Length; ++index)
    {
      if (this.m_Categories[index].value)
        return index;
    }
    return -1;
  }

  private void ClearData()
  {
    using (Dictionary<Coordinate, BookmarkItemRenderer>.Enumerator enumerator = this.m_ItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this.m_ItemPool.Release(gameObject);
      }
    }
    this.m_ItemDict.Clear();
  }

  private void UpdateTip()
  {
    this.m_Tip.SetActive(this.m_ItemDict.Count == 0);
  }

  private void UpdateData(int index)
  {
    this.ClearData();
    using (Dictionary<Coordinate, Bookmark>.ValueCollection.Enumerator enumerator = DBManager.inst.DB_Bookmark.data.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Bookmark current = enumerator.Current;
        if (this.CanShow(this.m_SelectedIndex, current))
        {
          BookmarkItemRenderer itemRenderer = this.CreateItemRenderer(index, current);
          this.m_ItemDict.Add(current.coordinate, itemRenderer);
        }
      }
    }
    this.UpdateTip();
    this.Reposition();
  }

  private void OnBookmarkDataRemove(Bookmark bookmark)
  {
    BookmarkItemRenderer bookmarkItemRenderer = (BookmarkItemRenderer) null;
    this.m_ItemDict.TryGetValue(bookmark.coordinate, out bookmarkItemRenderer);
    this.m_ItemDict.Remove(bookmark.coordinate);
    GameObject gameObject = bookmarkItemRenderer.gameObject;
    gameObject.SetActive(false);
    this.m_ItemPool.Release(gameObject);
    this.UpdateSelection();
    this.UpdateTip();
    this.Reposition();
  }

  private void OnBookmarkDataUpdated(Bookmark bookmark)
  {
    BookmarkItemRenderer bookmarkItemRenderer;
    this.m_ItemDict.TryGetValue(bookmark.coordinate, out bookmarkItemRenderer);
    bookmarkItemRenderer.UpdateUI();
    this.UpdateTip();
  }

  private BookmarkItemRenderer CreateItemRenderer(int index, Bookmark bookmark)
  {
    GameObject gameObject = this.m_ItemPool.AddChild(this.m_Grid.gameObject);
    gameObject.SetActive(true);
    BookmarkItemRenderer component = gameObject.GetComponent<BookmarkItemRenderer>();
    component.SetData(bookmark, new System.Action(this.OnItemSelectChanged));
    return component;
  }

  private void UpdateSelection()
  {
    int selectCount = this.SelectCount;
    this.m_All.Set(selectCount > 0 && selectCount == this.m_ItemDict.Count);
    this.m_Count.text = selectCount.ToString();
  }

  private void OnItemSelectChanged()
  {
    this.UpdateSelection();
  }

  private void Reposition()
  {
    this.m_Grid.sorting = UIGrid.Sorting.Custom;
    this.m_Grid.onCustomSort = new Comparison<Transform>(BookmarkPopup.Compare);
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
    this.m_ScrollView.gameObject.SetActive(false);
    this.m_ScrollView.gameObject.SetActive(true);
  }

  private static int Compare(Transform x, Transform y)
  {
    BookmarkItemRenderer component = x.GetComponent<BookmarkItemRenderer>();
    return (int) (y.GetComponent<BookmarkItemRenderer>().Bookmark.ctime - component.Bookmark.ctime);
  }

  private bool CanShow(int index, Bookmark bookmark)
  {
    switch (index)
    {
      case 0:
        return true;
      case 1:
        if (bookmark.type == 0)
          return true;
        break;
      case 2:
        if (bookmark.type == 1)
          return true;
        break;
      case 3:
        if (bookmark.type == 2)
          return true;
        break;
    }
    return false;
  }
}
