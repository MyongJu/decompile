﻿// Decompiled with JetBrains decompiler
// Type: ByteArray
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.IO;

public class ByteArray
{
  private MemoryStream stream;
  private BinaryWriter writer;

  public ByteArray()
  {
    this.stream = new MemoryStream();
    this.writer = new BinaryWriter((Stream) this.stream);
  }

  public void writeByte(byte value)
  {
    this.writer.Write(value);
  }

  public byte[] GetAllBytes()
  {
    byte[] buffer = new byte[this.stream.Length];
    this.stream.Position = 0L;
    this.stream.Read(buffer, 0, buffer.Length);
    return buffer;
  }
}
