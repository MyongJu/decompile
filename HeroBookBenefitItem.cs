﻿// Decompiled with JetBrains decompiler
// Type: HeroBookBenefitItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HeroBookBenefitItem : MonoBehaviour
{
  [SerializeField]
  private UILabel _LabelDesc;
  [SerializeField]
  private GameObject _RootLine;

  public void SetData(string desc)
  {
    this._LabelDesc.text = desc;
  }

  public void SetLineEnabled(bool enable)
  {
    this._RootLine.SetActive(enable);
  }

  public void SetTextColor(Color color)
  {
    this._LabelDesc.color = color;
  }
}
