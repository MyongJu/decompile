﻿// Decompiled with JetBrains decompiler
// Type: ClientIdCreater
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ClientIdCreater
{
  private static int _idCreateCount;

  public static long CreateLongId(ClientIdCreater.IdType inType)
  {
    long num = (long) NetServerTime.inst.ServerTimestamp << 32;
    ++ClientIdCreater._idCreateCount;
    if (ClientIdCreater._idCreateCount > (int) ushort.MaxValue)
      ClientIdCreater._idCreateCount = 0;
    return num + (long) ((ClientIdCreater._idCreateCount << 16) + inType);
  }

  public static long CheckIdCount(long id)
  {
    return (long) ushort.MaxValue & id >> 16;
  }

  public static ClientIdCreater.IdType CheckIdType(long id)
  {
    return (ClientIdCreater.IdType) (15L & id);
  }

  public static int CheckIdTimeStamp(long id)
  {
    return (int) (id >> 32);
  }

  public enum IdType
  {
    BUILDING,
    ITEM,
    EVENT,
    EFFECT,
    SKILL_EFFECT,
  }
}
