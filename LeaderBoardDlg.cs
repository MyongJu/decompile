﻿// Decompiled with JetBrains decompiler
// Type: LeaderBoardDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class LeaderBoardDlg : UI.Dialog
{
  private GameObjectPool m_ItemPool = new GameObjectPool();
  private List<OptionItem> optItemUI = new List<OptionItem>();
  public GameObject itemTemplate;
  public GameObject itemRoot;
  public UILabel title;
  public UIGrid grid;
  public UIScrollView scroll;
  private bool m_Initialized;

  private void Clear()
  {
    for (int index = 0; index < this.optItemUI.Count; ++index)
    {
      this.optItemUI[index].Clear();
      this.m_ItemPool.Release(this.optItemUI[index].gameObject);
    }
    this.optItemUI.Clear();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Clear();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (!this.m_Initialized)
    {
      this.m_ItemPool.Initialize(this.itemTemplate, this.grid.gameObject);
      this.m_Initialized = true;
    }
    this.Refresh();
  }

  private void Refresh()
  {
    this.Clear();
    this.title.text = ScriptLocalization.Get("leaderboards_uppercase_title", true);
    for (int id = 0; id < 7; ++id)
    {
      GameObject gameObject = this.m_ItemPool.AddChild(this.grid.gameObject);
      gameObject.SetActive(true);
      OptionItem component = gameObject.GetComponent<OptionItem>();
      this.optItemUI.Add(component);
      component.SetInfo(id, RankBoardDlg.LeaderBoardType.Kingdom);
    }
    this.grid.Reposition();
    this.scroll.ResetPosition();
  }

  public void OnCloseBtnClicked()
  {
    this.Clear();
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnBckBtnClicked()
  {
    this.Clear();
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }
}
