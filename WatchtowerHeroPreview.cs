﻿// Decompiled with JetBrains decompiler
// Type: WatchtowerHeroPreview
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WatchtowerHeroPreview : MonoBehaviour
{
  public GameObject m_HeroPreviewPrefab;
  private GameObject m_HeroInstance;
  private HeroPreview m_HeroPreview;

  public void Show(HeroGender gender)
  {
    this.gameObject.SetActive(true);
    this.m_HeroInstance = Object.Instantiate<GameObject>(this.m_HeroPreviewPrefab);
    this.m_HeroPreview = this.m_HeroInstance.GetComponent<HeroPreview>();
    this.m_HeroPreview.InitializeCharacter(gender);
  }

  public void Hide()
  {
    this.m_HeroPreview = (HeroPreview) null;
    Object.Destroy((Object) this.m_HeroInstance);
    this.gameObject.SetActive(false);
  }

  public void OnClose()
  {
    this.Hide();
  }

  public HeroEquipments GetHeroEquipments()
  {
    if ((Object) this.m_HeroPreview != (Object) null)
      return this.m_HeroPreview.GetHeroEquipments();
    return (HeroEquipments) null;
  }
}
