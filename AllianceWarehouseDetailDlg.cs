﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarehouseDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Text;
using UI;

public class AllianceWarehouseDetailDlg : UI.Dialog
{
  public UILabel m_titleLabel;
  public UILabel m_totalStoredWeightLabel;
  public UILabel m_totalStoredPeopleLabel;
  public UIButton m_demolishButton;
  public AllianceStorageSlotItemContainer m_slotContainer;
  protected AllianceWarehouseDetailDlg.Parameter m_Parameter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as AllianceWarehouseDetailDlg.Parameter;
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  protected void UpdateUI()
  {
    AllianceWareHouseData buildingConfigId = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseDataByBuildingConfigId(this.m_Parameter.warehouesConfigId);
    if (buildingConfigId == null)
      return;
    this.m_titleLabel.text = this.GetTitle();
    this.m_totalStoredWeightLabel.text = Utils.FormatShortThousandsLong(buildingConfigId.TotalResourceWeightStored);
    this.m_totalStoredPeopleLabel.text = buildingConfigId.TotalPeopleStored.ToString();
    this.m_demolishButton.isEnabled = PlayerData.inst.allianceId == buildingConfigId.AllianceId && (Utils.IsR5Member() || Utils.IsR4Member());
    this.m_slotContainer.SetWarehouseData(buildingConfigId);
  }

  public void OnDemolishButtonClicked()
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(this.m_Parameter.warehouesConfigId);
    if (buildingStaticInfo == null)
      return;
    string content = string.Format(ScriptLocalization.Get("alliance_buildings_confirm_demolish_description", true), (object) ScriptLocalization.Get(buildingStaticInfo.BuildName, true));
    string title = ScriptLocalization.Get("alliance_buildings_confirm_placement_button", true);
    string left = ScriptLocalization.Get("id_uppercase_confirm", true);
    string right = ScriptLocalization.Get("id_uppercase_cancel", true);
    UIManager.inst.ShowConfirmationBox(title, content, left, right, ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.ConfirmDemolish), (System.Action) null, (System.Action) null);
  }

  protected void ConfirmDemolish()
  {
    AllianceWareHouseData buildingConfigId = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseDataByBuildingConfigId(this.m_Parameter.warehouesConfigId);
    if (buildingConfigId == null)
      return;
    Hashtable postData = new Hashtable();
    postData[(object) "k"] = (object) buildingConfigId.WorldId;
    postData[(object) "x"] = (object) buildingConfigId.MapX;
    postData[(object) "y"] = (object) buildingConfigId.MapY;
    MessageHub.inst.GetPortByAction("Map:takeBackAllianceBuilding").SendRequest(postData, (System.Action<bool, object>) null, true);
    this.OnCloseButtonPress();
  }

  protected string GetTitle()
  {
    string empty = string.Empty;
    AllianceWareHouseData buildingConfigId = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseDataByBuildingConfigId(this.m_Parameter.warehouesConfigId);
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(this.m_Parameter.warehouesConfigId);
    if (buildingConfigId != null && buildingStaticInfo != null)
    {
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(buildingConfigId.AllianceId);
      if (allianceData != null)
      {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append("(" + allianceData.allianceAcronym + ")");
        stringBuilder.Append(Utils.XLAT(buildingStaticInfo.BuildName));
        empty = stringBuilder.ToString();
      }
    }
    return empty;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int warehouesConfigId;
  }
}
