﻿// Decompiled with JetBrains decompiler
// Type: ConfigCasinoRoulette
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigCasinoRoulette
{
  private List<CasinoRouletteInfo> rouletteInfoList = new List<CasinoRouletteInfo>();
  public bool buildCasinoDataFlag;
  private Dictionary<string, CasinoRouletteInfo> datas;
  private Dictionary<int, CasinoRouletteInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<CasinoRouletteInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    Dictionary<string, CasinoRouletteInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this.rouletteInfoList.Add(enumerator.Current);
    }
    this.buildCasinoDataFlag = true;
    this.rouletteInfoList.Sort(new Comparison<CasinoRouletteInfo>(this.SortByID));
  }

  public List<CasinoRouletteInfo> GetCasinoRouletteInfoList()
  {
    return this.rouletteInfoList;
  }

  public int SortByID(CasinoRouletteInfo a, CasinoRouletteInfo b)
  {
    return int.Parse(a.id).CompareTo(int.Parse(b.id));
  }
}
