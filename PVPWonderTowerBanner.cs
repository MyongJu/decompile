﻿// Decompiled with JetBrains decompiler
// Type: PVPWonderTowerBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class PVPWonderTowerBanner : SpriteBanner
{
  private static Color playerColor = new Color(1f, 1f, 0.6862745f, 1f);
  private static Color enemyColor = Color.red;
  private static Color allyColor = new Color(0.3686275f, 0.9764706f, 0.9764706f, 1f);
  private static Color neutralColor = Color.white;
  public PVPAllianceSymbol m_Symbol;
  public GameObject m_RootNode;
  public GameObject m_Knife;

  public override void UpdateUI(TileData tile)
  {
    base.UpdateUI(tile);
    this.m_RootNode.SetActive(tile.OwnerID != 0L);
    string str1 = string.Empty;
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(tile.AllianceID);
    if (allianceData != null)
    {
      str1 = string.Format("({0})", (object) allianceData.allianceAcronym);
      this.m_Symbol.gameObject.SetActive(true);
      this.m_Symbol.SetSymbols(allianceData.allianceSymbolCode);
    }
    else
      this.m_Symbol.gameObject.SetActive(false);
    UserData userData = tile.UserData;
    string str2 = string.Empty;
    if (userData != null)
    {
      str2 = userData.userName;
      if (userData.IsForeigner && tile.OwnerID != PlayerData.inst.uid || userData.IsPit)
        str2 = str2 + ".k" + (object) userData.world_id;
    }
    this.Name = str1 + str2;
    this.m_Knife.SetActive(userData != null && (userData.IsForeigner || userData.IsPit));
    this.UpdateNameColor(tile);
    if (!(bool) ((Object) this.m_NameRenderer))
      return;
    Bounds bounds = this.m_NameRenderer.bounds;
    Vector3 vector3 = this.m_NameLabel.transform.InverseTransformPoint(new Vector3(bounds.min.x, bounds.center.y, bounds.center.z));
    vector3.x -= 50f;
    this.m_Symbol.transform.localPosition = vector3;
    Vector3 position = new Vector3(bounds.max.x, bounds.center.y, bounds.center.z);
    position = this.m_NameLabel.transform.InverseTransformPoint(position);
    position.x += 50f;
    this.m_Knife.transform.localPosition = position;
  }

  private void UpdateNameColor(TileData tile)
  {
    if (tile.OwnerID == PlayerData.inst.uid)
      this.m_NameLabel.color = PVPWonderTowerBanner.playerColor;
    else if (PlayerData.inst.allianceData != null)
    {
      if (tile.AllianceID == PlayerData.inst.allianceId)
      {
        this.m_NameLabel.color = PVPWonderTowerBanner.allyColor;
      }
      else
      {
        UserData userData = tile.UserData;
        if (userData != null && userData.IsForeigner)
          this.m_NameLabel.color = PVPWonderTowerBanner.enemyColor;
        else
          this.m_NameLabel.color = PVPWonderTowerBanner.neutralColor;
      }
    }
    else
    {
      UserData userData = tile.UserData;
      if (userData != null && userData.IsForeigner)
        this.m_NameLabel.color = PVPWonderTowerBanner.enemyColor;
      else
        this.m_NameLabel.color = PVPWonderTowerBanner.neutralColor;
    }
  }
}
