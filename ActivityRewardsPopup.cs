﻿// Decompiled with JetBrains decompiler
// Type: ActivityRewardsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class ActivityRewardsPopup : BaseReportPopup
{
  public UILabel content;
  public RewardItemComponent ric;
  public GameObject itemsGroup;
  public UISprite bg;
  public GameObject btnCollection;

  private void Init()
  {
    this.content.text = this.param.mail.GetBodyString();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, true, true, string.Empty);
    this.ric.FeedData((IComponentData) this.AnalyzeCollectedRewards());
    this.ArrangeLayout();
    this.UpdateBtnState();
  }

  private void ArrangeLayout()
  {
    this.bg.height = (int) NGUIMath.CalculateRelativeWidgetBounds(this.itemsGroup.transform).size.y + 25;
  }

  private void CollectRewards()
  {
    this.CollectRewardsBase();
  }

  private void UpdateBtnState()
  {
    if (this.param.mail.attachment_status == 2)
      this.btnCollection.GetComponent<UIButton>().isEnabled = false;
    else
      this.btnCollection.GetComponent<UIButton>().isEnabled = true;
  }

  public new void OnClaimRewardClicked()
  {
    this.CollectRewards();
    AudioManager.Instance.PlaySound("sfx_mail_reward", false);
    PlayerData.inst.mail.API.GainAttachment((int) this.maildata.category, this.param.mail.mailID, new System.Action<bool, object>(this.GainAttachmentCallBack));
  }

  private void GainAttachmentCallBack(bool ok, object obj)
  {
    if (!ok)
      return;
    this.param.mail.attachment_status = 2;
    this.UpdateBtnState();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }
}
