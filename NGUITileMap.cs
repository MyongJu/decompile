﻿// Decompiled with JetBrains decompiler
// Type: NGUITileMap
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NGUITileMap : MonoBehaviour
{
  private Dictionary<int, Hashtable> tiles = new Dictionary<int, Hashtable>();
  public string mapName = "CampaignMap.json";
  public Vector3 screenPos = new Vector3(0.0f, 0.0f, 0.0f);
  public Vector2 screenPosTile = new Vector2(0.0f, 0.0f);
  private List<UISprite> freeTileMap = new List<UISprite>();
  private Dictionary<int, UISprite> tileMap = new Dictionary<int, UISprite>();
  public int viewSize = 20;
  public bool upMap = true;
  private NGUITileMap.OnReadLayerData tileDataFunc;
  private NGUITileMap.OnMapTileClick tileClickFunc;
  private int[,] mapdata;
  public int tilenumW;
  public int tilenumH;
  public GameObject touchBlocker;
  public UISprite tileTemplate;
  private Camera tileCamera;
  public Vector2 tileSize;

  public void SetTileDataCallBack(NGUITileMap.OnReadLayerData readFunc, NGUITileMap.OnMapTileClick clickFunc)
  {
    this.tileDataFunc = readFunc;
    this.tileClickFunc = clickFunc;
  }

  private void Start()
  {
    this.tileCamera = GameObject.Find("TileCamera").GetComponent<Camera>();
    this.tileTemplate.GetComponent<UIDragCamera>().draggableCamera = this.tileCamera.GetComponent<UIDraggableCamera>();
    Hashtable hashtable1 = Utils.Json2Object((AssetManager.Instance.HandyLoad(this.mapName, (System.Type) null) as TextAsset).text, true) as Hashtable;
    ArrayList arrayList1 = hashtable1[(object) "tilesets"] as ArrayList;
    for (int index = 0; index < arrayList1.Count; ++index)
    {
      Hashtable hashtable2 = arrayList1[index] as Hashtable;
      int num = int.Parse(hashtable2[(object) "firstgid"].ToString());
      foreach (DictionaryEntry dictionaryEntry in hashtable2[(object) "tileproperties"] as Hashtable)
        this.tiles.Add(int.Parse(dictionaryEntry.Key.ToString()) + num, dictionaryEntry.Value as Hashtable);
    }
    ArrayList arrayList2 = hashtable1[(object) "layers"] as ArrayList;
    for (int layerindex = 0; layerindex < arrayList2.Count; ++layerindex)
    {
      Hashtable hashtable2 = arrayList2[layerindex] as Hashtable;
      int length1 = int.Parse(hashtable2[(object) "width"].ToString());
      int length2 = int.Parse(hashtable2[(object) "height"].ToString());
      ArrayList arrayList3 = hashtable2[(object) "data"] as ArrayList;
      if (layerindex == 0)
      {
        this.mapdata = new int[length2, length1];
        this.tilenumH = length2;
        this.tilenumW = length1;
        int num = 0;
        for (int y = 0; y < length2; ++y)
        {
          for (int x = 0; x < length1; ++x)
          {
            this.mapdata[y, x] = int.Parse(arrayList3[num++].ToString());
            if (this.mapdata[y, x] == 23)
              this.screenPos = this.GetTilePos(y, x, false);
          }
        }
      }
      else
      {
        int num = 0;
        for (int ty = 0; ty < length2; ++ty)
        {
          for (int tx = 0; tx < length1; ++tx)
          {
            int key = int.Parse(arrayList3[num++].ToString());
            if (key > 0)
            {
              this.mapdata[ty, tx] |= key << 8 * layerindex;
              if (this.tileDataFunc != null)
              {
                Hashtable tileInfo = (Hashtable) null;
                if (this.tiles.TryGetValue(key, out tileInfo))
                  this.tileDataFunc(layerindex, ty, tx, tileInfo);
              }
            }
          }
        }
      }
    }
    if ((UnityEngine.Object) this.tileTemplate != (UnityEngine.Object) null)
      this.freeTileMap.Add(this.tileTemplate);
    this.updatemap(false);
    if (this.tileDataFunc == null)
      return;
    this.tileDataFunc(-1, 0, 0, (Hashtable) null);
  }

  public void SetTileID(int tid, int y, int x, int layer = 0)
  {
    int tileId = this.GetTileID(y, x, 0);
    this.mapdata[y, x] = tileId & ~((int) byte.MaxValue << layer * 8) | tid << layer * 8;
  }

  public int GetTileID(int y, int x, int layer = 0)
  {
    return (this.mapdata[y, x] & (int) byte.MaxValue << layer * 8) >> layer * 8;
  }

  public Hashtable GetTileInfo(int y, int x, int layer = 0)
  {
    int tileId = this.GetTileID(y, x, layer);
    Hashtable hashtable = (Hashtable) null;
    if (this.tiles.TryGetValue(tileId, out hashtable))
      return hashtable;
    return (Hashtable) null;
  }

  private UISprite NewSprite()
  {
    UISprite uiSprite;
    if (this.freeTileMap.Count > 0)
    {
      uiSprite = this.freeTileMap[0];
      this.freeTileMap.RemoveAt(0);
    }
    else
    {
      GameObject go = UnityEngine.Object.Instantiate<GameObject>(this.tileTemplate.gameObject);
      uiSprite = go.GetComponent<UISprite>();
      uiSprite.GetComponent<UIButton>().onClick.Add(new EventDelegate((EventDelegate.Callback) (() => this.OnTileClick(go))));
      uiSprite.transform.parent = this.tileTemplate.transform.parent;
      uiSprite.transform.localScale = new Vector3(1f, 1f, 1f);
    }
    uiSprite.gameObject.SetActive(true);
    return uiSprite;
  }

  public void ToTile(int y, int x)
  {
    this.screenPos = this.GetTilePos(y, x, false);
    this.tileCamera.transform.localPosition = this.screenPos;
    this.updatemap(false);
  }

  public void ToScreen(float x, float y, bool upmap = true)
  {
    this.screenPos.x = x;
    this.screenPos.y = y;
    this.tileCamera.transform.localPosition = this.screenPos;
    if (!upmap)
      return;
    this.updatemap(false);
  }

  public Vector3 GetTilePos(int y, int x, bool center = false)
  {
    Vector3 zero = Vector3.zero;
    zero.x = (float) ((double) x * (double) this.tileSize.x + (double) (y % 2) * ((double) this.tileSize.x / 2.0));
    zero.y = (float) -y * this.tileSize.y;
    if (center)
    {
      zero.x += this.tileSize.x / 2f;
      zero.y += this.tileSize.y / 2f;
    }
    return zero;
  }

  public void updatemap(bool force = false)
  {
    this.screenPos = this.tileCamera.transform.localPosition;
    int num1 = Mathf.FloorToInt(this.screenPos.x / this.tileSize.x);
    int num2 = Mathf.FloorToInt(-this.screenPos.y / this.tileSize.y);
    if ((double) num1 == (double) this.screenPosTile.x && (double) num2 == (double) this.screenPosTile.y && !force)
      return;
    this.screenPosTile.x = (float) num1;
    this.screenPosTile.y = (float) num2;
    List<int> intList = new List<int>();
    using (Dictionary<int, UISprite>.Enumerator enumerator = this.tileMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, UISprite> current = enumerator.Current;
        if (Mathf.Abs(current.Key % this.tilenumW - (int) this.screenPosTile.x) > this.viewSize || Mathf.Abs(current.Key / this.tilenumW - (int) this.screenPosTile.y) > this.viewSize)
        {
          intList.Add(current.Key);
          this.freeTileMap.Add(current.Value);
          current.Value.gameObject.SetActive(false);
        }
      }
    }
    using (List<int>.Enumerator enumerator = intList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.tileMap.Remove(enumerator.Current);
    }
    for (int index1 = -this.viewSize; index1 <= this.viewSize; ++index1)
    {
      for (int index2 = -this.viewSize; index2 <= this.viewSize; ++index2)
      {
        int x = index1 + Mathf.FloorToInt(this.screenPosTile.x);
        int y = index2 + Mathf.FloorToInt(this.screenPosTile.y);
        int key = y * this.tilenumW + x;
        if (x >= 0 && x < this.tilenumW && (y >= 0 && y < this.tilenumH) && (this.mapdata[y, x] > 0 && !this.tileMap.ContainsKey(key)))
        {
          Hashtable tileInfo = this.GetTileInfo(y, x, 0);
          if (tileInfo != null)
          {
            UISprite uiSprite = this.NewSprite();
            uiSprite.spriteName = tileInfo[(object) "ID"] as string;
            uiSprite.transform.localPosition = this.GetTilePos(y, x, false);
            uiSprite.name = y.ToString() + "," + (object) x;
            this.tileMap.Add(key, uiSprite);
          }
        }
      }
    }
  }

  public void OnTileClick(GameObject tile)
  {
    string[] strArray = tile.name.Split(',');
    if (this.tileClickFunc == null)
      return;
    this.tileClickFunc(int.Parse(strArray[0]), int.Parse(strArray[1]));
  }

  private void Update()
  {
    if (!this.upMap)
      return;
    this.updatemap(false);
  }

  public static int GetMarchDir(float rotAngle)
  {
    int num = 1;
    if ((double) rotAngle < 22.5 || (double) rotAngle > 337.5)
      num = 1;
    else if (22.5 <= (double) rotAngle && (double) rotAngle < 67.5)
      num = 2;
    else if (67.5 <= (double) rotAngle && (double) rotAngle < 112.5)
      num = 3;
    else if (112.5 <= (double) rotAngle && (double) rotAngle < 157.5)
      num = 4;
    else if (157.5 <= (double) rotAngle && (double) rotAngle < 202.5)
      num = 5;
    else if (202.5 <= (double) rotAngle && (double) rotAngle < 247.5)
      num = 4;
    else if (247.5 <= (double) rotAngle && (double) rotAngle < 292.5)
      num = 3;
    else if (292.5 <= (double) rotAngle && (double) rotAngle < 337.5)
      num = 2;
    return num;
  }

  public void AddPathIcon(GameObject pathIcon, List<GameObject> _path, int sx, int sy, int ex, int ey, bool isGreen = true)
  {
    if (isGreen && _path.Count != 0)
      return;
    Vector3 tilePos1 = this.GetTilePos(sy, sx, true);
    Vector3 tilePos2 = this.GetTilePos(ey, ex, true);
    float num1 = 57.29578f * Mathf.Atan2(tilePos2.x - tilePos1.x, tilePos2.y - tilePos1.y);
    if ((double) num1 < 0.0)
      num1 += 360f;
    Vector3 vector3 = tilePos2 - tilePos1;
    int num2 = (int) ((double) Mathf.Sqrt((float) ((double) vector3.x * (double) vector3.x + (double) vector3.y * (double) vector3.y)) * 0.00499999988824129);
    for (int index = 0; index < num2 + 1; ++index)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(pathIcon);
      gameObject.transform.parent = this.transform;
      gameObject.SetActive(true);
      gameObject.transform.localPosition = new Vector3(tilePos1.x + vector3.x * ((float) (index + 1) + (!isGreen ? 0.0f : 0.2f)) / (float) (num2 + 2), tilePos1.y + vector3.y * ((float) (index + 1) + (!isGreen ? 0.0f : 0.2f)) / (float) (num2 + 2), 0.0f);
      gameObject.transform.localScale = Vector3.one;
      gameObject.transform.localEulerAngles = new Vector3(0.0f, 0.0f, -num1);
      if (!isGreen)
        gameObject.GetComponent<UISprite>().color = Color.black;
      _path.Add(gameObject);
    }
  }

  public delegate void OnReadLayerData(int layerindex, int ty, int tx, Hashtable tileInfo);

  public delegate void OnMapTileClick(int ty, int tx);
}
