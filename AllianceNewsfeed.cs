﻿// Decompiled with JetBrains decompiler
// Type: AllianceNewsfeed
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllianceNewsfeed : MonoBehaviour
{
  private List<AllianceFeedComponent> m_ItemList = new List<AllianceFeedComponent>();
  public UIScrollView m_ScrollView;
  public UITable m_Table;
  public GameObject m_AnnoucePrefab;
  public GameObject m_NewsfeedPrefab;

  public void Show()
  {
    this.ClearData();
    MessageHub.inst.GetPortByAction("Alliance:loadAllianceDynamic").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, (System.Action<bool, object>) ((success, orgData) =>
    {
      if (!success)
        return;
      this.gameObject.SetActive(true);
      AllianceData allianceData = PlayerData.inst.allianceData;
      if (allianceData != null)
        this.AddItem(AllianceNewsfeed.MsgType.Announcement, new Hashtable()
        {
          {
            (object) "type",
            (object) "announcement"
          },
          {
            (object) "data",
            (object) new ArrayList()
            {
              (object) allianceData.announcement
            }
          },
          {
            (object) "time",
            (object) NetServerTime.inst.ServerTimestamp.ToString()
          }
        });
      this.UpdateList(orgData as ArrayList);
      this.m_Table.Reposition();
      this.m_ScrollView.ResetPosition();
    }), true);
  }

  private void ClearData()
  {
    using (List<AllianceFeedComponent>.Enumerator enumerator = this.m_ItemList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceFeedComponent current = enumerator.Current;
        current.transform.parent = (Transform) null;
        current.gameObject.SetActive(false);
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ItemList.Clear();
  }

  private void UpdateList(ArrayList data)
  {
    if (data == null)
      return;
    for (int index = 0; index < data.Count; ++index)
      this.AddItem(AllianceNewsfeed.MsgType.Newsfeed, data[index] as Hashtable);
  }

  private void AddItem(AllianceNewsfeed.MsgType type, Hashtable data)
  {
    GameObject gameObject = (GameObject) null;
    switch (type)
    {
      case AllianceNewsfeed.MsgType.Announcement:
        gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_AnnoucePrefab);
        break;
      case AllianceNewsfeed.MsgType.Newsfeed:
        gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_NewsfeedPrefab);
        break;
    }
    if (!(bool) ((UnityEngine.Object) gameObject))
      return;
    gameObject.SetActive(true);
    gameObject.transform.parent = this.m_Table.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
    AllianceFeedComponent component = gameObject.GetComponent<AllianceFeedComponent>();
    component.FeedData<AllianceFeedComponentData>(data);
    this.m_ItemList.Add(component);
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
    this.ClearData();
  }

  private enum MsgType
  {
    Announcement,
    Newsfeed,
  }
}
