﻿// Decompiled with JetBrains decompiler
// Type: PopupDefenseDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PopupDefenseDlg : UI.Dialog
{
  private static Hashtable reinforceParam = new Hashtable();
  [HideInInspector]
  public List<DefenseSlotBar> slots = new List<DefenseSlotBar>();
  private int _rallyWaitStartTime;
  private int _rallyWaitEndTime;
  private bool secondFlag;
  private PopupDefenseDlg.Data data;
  [SerializeField]
  private PopupDefenseDlg.Panel panel;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    PopupDefenseDlg.Parameter parameter = orgParam as PopupDefenseDlg.Parameter;
    if (parameter == null)
      return;
    this.data.rallyId = parameter.rallyId;
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this.data.rallyId);
    this.SetupStaticInfo();
    PopupDefenseDlg.reinforceParam[(object) "uid"] = (object) PlayerData.inst.uid;
    PopupDefenseDlg.reinforceParam[(object) "opp_uid"] = (object) rallyData.targetUid;
    MessageHub.inst.GetPortByAction("PVP:getReinforceList").SendRequest(PopupDefenseDlg.reinforceParam, (System.Action<bool, object>) null, true);
    this.SetupDynamicInfo();
  }

  public void SetupStaticInfo()
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this.data.rallyId);
    if (rallyData == null)
    {
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
    {
      AllianceData allianceData1 = DBManager.inst.DB_Alliance.Get(rallyData.ownerAllianceId);
      if (allianceData1 != null)
        this.panel.ownerAllianceName.text = allianceData1.allianceFullName;
      CityData cityData1 = DBManager.inst.DB_City.Get(rallyData.ownerCityId);
      if (cityData1 != null)
      {
        this.panel.ownerCityName.text = cityData1.cityName;
        this.panel.ownerCityK.text = cityData1.cityLocation.K.ToString();
        this.panel.ownerCityX.text = cityData1.cityLocation.X.ToString();
        this.panel.ownerCityY.text = cityData1.cityLocation.Y.ToString();
      }
      AllianceData allianceData2 = DBManager.inst.DB_Alliance.Get(rallyData.targetAllianceId);
      if (allianceData2 != null)
        this.panel.targetAllianceName.text = allianceData2.allianceFullName;
      CityData cityData2 = DBManager.inst.DB_City.Get(rallyData.targetCityId);
      if (cityData2 != null)
      {
        this.panel.targetCityName.text = cityData2.cityName;
        this.panel.targetCityK.text = cityData2.cityLocation.K.ToString();
        this.panel.targetCityX.text = cityData2.cityLocation.X.ToString();
        this.panel.targetCityY.text = cityData2.cityLocation.Y.ToString();
      }
      if (this.secondFlag)
        return;
      Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
      this.secondFlag = true;
    }
  }

  public void SetupDynamicInfo()
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this.data.rallyId);
    if (rallyData == null)
    {
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
    {
      List<long> longList;
      if (rallyData.targetUid == PlayerData.inst.uid)
      {
        this.panel.BT_Reinforce.isEnabled = false;
        longList = DBManager.inst.DB_March.GetTargetListByType(MarchData.MarchType.reinforce);
      }
      else
      {
        bool flag = true;
        longList = new List<long>();
        List<long> ownerListByType = DBManager.inst.DB_March.GetOwnerListByType(MarchData.MarchType.reinforce);
        for (int index = 0; index < ownerListByType.Count; ++index)
        {
          MarchData marchData = DBManager.inst.DB_March.Get(ownerListByType[index]);
          if (marchData != null && marchData.targetUid == rallyData.targetUid && marchData.state == MarchData.MarchState.reinforcing)
            longList.Add(marchData.marchId);
        }
        for (int index = 0; index < DBManager.inst.DB_March.othersMarch.Count; ++index)
        {
          MarchData marchData = DBManager.inst.DB_March.Get(DBManager.inst.DB_March.othersMarch[index]);
          if (marchData != null && marchData.type == MarchData.MarchType.reinforce && marchData.targetUid == rallyData.targetUid)
          {
            longList.Add(marchData.marchId);
            if (marchData.ownerUid == PlayerData.inst.uid)
              flag = false;
          }
        }
        this.panel.BT_Reinforce.isEnabled = flag;
      }
      if (longList == null)
        return;
      for (int count = this.slots.Count; count < longList.Count; ++count)
      {
        GameObject gameObject = NGUITools.AddChild(this.panel.defenseSlotTable.gameObject, this.panel.orgDefenseSlot.gameObject);
        gameObject.name = string.Format("{0}", (object) (100 + count));
        gameObject.SetActive(false);
        gameObject.transform.localPosition = new Vector3(0.0f, (float) (-350 * count), 0.0f);
        DefenseSlotBar component = gameObject.GetComponent<DefenseSlotBar>();
        component.Setup(count + 1, this.data.rallyId);
        this.slots.Add(component);
      }
      long num = 0;
      int index1 = 0;
      for (int index2 = 0; index2 < longList.Count; ++index2)
      {
        MarchData marchData = DBManager.inst.DB_March.Get(longList[index2]);
        if (marchData.state == MarchData.MarchState.marching || marchData.state == MarchData.MarchState.reinforcing)
        {
          this.slots[index1].gameObject.SetActive(true);
          this.slots[index1].Setup(index2, longList[index2]);
          ++index1;
          if (marchData.state == MarchData.MarchState.reinforcing)
            num += (long) marchData.troopsInfo.totalCount;
        }
      }
      for (int index2 = index1; index2 < this.slots.Count; ++index2)
        this.slots[index2].gameObject.SetActive(false);
      this.panel.scrolView.ResetPosition();
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData("embassy", DBManager.inst.DB_City.Get(rallyData.ownerCityId).buildings.level_embassy);
      this.panel.reinforceTroopsCount.text = string.Format("{0}/{1}", (object) Utils.ConvertNumberToNormalString(num), (object) Utils.ConvertNumberToNormalString((int) data.Benefit_Value_1));
      this.panel.defenseSlotTable.repositionNow = true;
    }
  }

  public void OnSecond(int serverTime)
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this.data.rallyId);
    if (rallyData == null)
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    else if (rallyData.waitTimeDuration.endTime > serverTime)
    {
      this.panel.rallyLeftTimeText.text = Utils.FormatTime(rallyData.waitTimeDuration.endTime - serverTime, false, false, true);
      if (rallyData.waitTimeDuration.endTime != rallyData.waitTimeDuration.startTime)
        this.panel.rallyLeftTimeSlider.value = 1f - Mathf.Clamp01((float) (rallyData.waitTimeDuration.endTime - serverTime) * rallyData.waitTimeDuration.oneOverTimeDuration);
      if (this.secondFlag)
        return;
      Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
      this.secondFlag = true;
    }
    else
    {
      this.panel.rallyLeftTimeText.text = "Rally Started!";
      this.panel.rallyLeftTimeSlider.value = 1f;
      if (!this.secondFlag)
        return;
      if (Oscillator.IsAvailable)
        Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
      this.secondFlag = false;
    }
  }

  public void OnFreshMarch(long marchId)
  {
    MarchData marchData = DBManager.inst.DB_March.Get(marchId);
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this.data.rallyId);
    if (marchData == null || rallyData == null || (marchData.type != MarchData.MarchType.reinforce || marchData.targetUid != rallyData.targetUid))
      return;
    this.SetupStaticInfo();
    this.SetupDynamicInfo();
  }

  public void OnFreshRally(long rallyId)
  {
    if (rallyId != this.data.rallyId)
      return;
    this.SetupStaticInfo();
    this.SetupDynamicInfo();
  }

  public void OnEnable()
  {
    DBManager.inst.DB_March.onDataChanged += new System.Action<long>(this.OnFreshMarch);
    DBManager.inst.DB_Rally.onRallyDataChanged += new System.Action<long>(this.OnFreshRally);
    if (!this.secondFlag)
      return;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public void OnDisable()
  {
    DBManager.inst.DB_March.onDataChanged -= new System.Action<long>(this.OnFreshMarch);
    DBManager.inst.DB_Rally.onRallyDataChanged -= new System.Action<long>(this.OnFreshRally);
    if (!this.secondFlag || !Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void OnReinforceButtonClick()
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this.data.rallyId);
    if (rallyData == null)
    {
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
    {
      CityData cityData = DBManager.inst.DB_City.Get(rallyData.targetCityId);
      if (cityData == null)
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      else
        UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
        {
          marchType = MarchAllocDlg.MarchType.reinforce,
          location = cityData.cityLocation
        }, 1 != 0, 1 != 0, 1 != 0);
    }
  }

  public void OnOwnerCityGotoButtonClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this.data.rallyId);
    if (rallyData == null)
      return;
    CityData cityData = DBManager.inst.DB_City.Get(rallyData.ownerCityId);
    if (cityData != null && !MapUtils.CanGotoTarget(cityData.cityLocation.K))
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    else if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PVPMode)
    {
      PVPSystem.Instance.Map.GotoLocation(DBManager.inst.DB_City.Get(rallyData.ownerCityId).cityLocation, false);
    }
    else
    {
      UIManager.inst.cityCamera.gameObject.SetActive(false);
      GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
      PVPMap.PendingGotoRequest = DBManager.inst.DB_City.Get(rallyData.ownerCityId).cityLocation;
    }
  }

  public void OnTargetCityGotoButtonClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    RallyData rallyData = DBManager.inst.DB_Rally.Get(this.data.rallyId);
    if (rallyData == null)
      return;
    CityData cityData = DBManager.inst.DB_City.Get(rallyData.targetCityId);
    if (cityData != null && !MapUtils.CanGotoTarget(cityData.cityLocation.K))
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    else if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PVPMode)
    {
      PVPSystem.Instance.Map.GotoLocation(DBManager.inst.DB_City.Get(rallyData.targetCityId).cityLocation, false);
    }
    else
    {
      UIManager.inst.cityCamera.gameObject.SetActive(false);
      GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
      PVPMap.PendingGotoRequest = DBManager.inst.DB_City.Get(rallyData.targetCityId).cityLocation;
    }
  }

  public void OnBackButtonClick()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnCloseButtonClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public long rallyId;
  }

  protected struct Data
  {
    public long rallyId;
  }

  [Serializable]
  protected class Panel
  {
    public UITexture ownerIcon;
    public UILabel ownerAllianceName;
    public UILabel ownerCityName;
    public UILabel ownerCityK;
    public UILabel ownerCityX;
    public UILabel ownerCityY;
    public UITexture targetIcon;
    public UILabel targetAllianceName;
    public UILabel targetCityName;
    public UILabel targetCityK;
    public UILabel targetCityX;
    public UILabel targetCityY;
    public UILabel reinforceTroopsCount;
    public UISlider rallyLeftTimeSlider;
    public UILabel rallyLeftTimeText;
    public UIButton BT_Reinforce;
    public UIScrollView scrolView;
    public UITable defenseSlotTable;
    public DefenseSlotBar orgDefenseSlot;
  }
}
