﻿// Decompiled with JetBrains decompiler
// Type: GatherItemComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;

public class GatherItemComponent : ComponentRenderBase
{
  public IconListComponent icons;
  public UITable table;

  public override void Init()
  {
    base.Init();
    GatherItemComponentData data1 = this.data as GatherItemComponentData;
    List<IconData> data2 = new List<IconData>();
    if (data1.dragon_attr != null)
    {
      IconData iconData1 = new IconData("Texture/ItemIcons/item_dragon_exp", new string[2]
      {
        ScriptLocalization.Get("id_dragon_xp", true),
        data1.dragon_attr[(object) "dragon_exp"].ToString()
      });
      data2.Add(iconData1);
      IconData iconData2 = new IconData("Texture/Dragon/icon_dragon_power_light", new string[2]
      {
        ScriptLocalization.Get("id_dragon_light_power", true),
        data1.dragon_attr[(object) "light"].ToString()
      });
      data2.Add(iconData2);
    }
    if (data1.rewards != null)
    {
      foreach (DictionaryEntry reward in data1.rewards)
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(int.Parse(reward.Key.ToString()));
        if (itemStaticInfo != null)
          data2.Add(new IconData(itemStaticInfo.ImagePath, new string[2]
          {
            itemStaticInfo.LocName,
            "X" + reward.Value.ToString()
          })
          {
            Data = (object) itemStaticInfo.internalId
          });
      }
    }
    if (data1.param != null && data1.param.ContainsKey((object) "kingdom_coin"))
    {
      IconData iconData = new IconData("Texture/Alliance/kingdom_coin", new string[2]
      {
        Utils.XLAT("throne_treasury_currency_name"),
        "X" + (object) int.Parse(data1.param[(object) "kingdom_coin"].ToString())
      });
      data2.Add(iconData);
    }
    this.icons.FeedData((IComponentData) new IconListComponentData(data2));
    this.table.Reposition();
  }
}
