﻿// Decompiled with JetBrains decompiler
// Type: ContactManagerContactPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Chat;
using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ContactManagerContactPopup : MonoBehaviour
{
  private long _uid;

  public void Show(long uid)
  {
    this._uid = uid;
    this.gameObject.SetActive(true);
  }

  public void Hide()
  {
    this._uid = 0L;
    this.OnCloseClick();
  }

  public void OnSendMailButtonClick()
  {
    UserData userData = DBManager.inst.DB_User.Get(this._uid);
    if (userData == null)
      return;
    PlayerData.inst.mail.ComposeMail(new List<string>()
    {
      userData.userName
    }, (string) null, (string) null, 0 != 0);
    this.OnCloseClick();
  }

  public void OnChatButtonClick()
  {
    UserData userData = DBManager.inst.DB_User.Get(this._uid);
    if (userData == null)
      return;
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    this.OnCloseClick();
    UIManager.inst.OpenDlg("ChatDlg", (UI.Dialog.DialogParameter) new ChatDlg.Parameter()
    {
      channelType = ChatDlg.Parameter.ChannelType.personal,
      privateUid = userData.uid
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnRemoveClick()
  {
    ContactManager.RemoveContact(this._uid, (System.Action<bool, object>) null);
    this.OnCloseClick();
  }

  public void OnBlockClick()
  {
    ContactManager.BlockUser(this._uid, (System.Action<bool, object>) null);
    this.OnCloseClick();
  }

  public void OnCloseClick()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
  }
}
