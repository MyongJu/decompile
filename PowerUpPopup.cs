﻿// Decompiled with JetBrains decompiler
// Type: PowerUpPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PowerUpPopup : Popup
{
  [SerializeField]
  protected UILabel m_labelPower;
  [SerializeField]
  protected UISlider m_sliderPowerCurrent;
  [SerializeField]
  protected UISlider m_sliderPowerTarget;
  [SerializeField]
  protected StandardProgressBar m_progressBar;
  [SerializeField]
  protected Icon m_itemIcon;
  [SerializeField]
  protected UIButton m_buttonPowerUp;
  protected ItemStaticInfo _ElixirItemInfo;

  protected AllianceMagicData CurrentPowerUpMagicData
  {
    get
    {
      return DBManager.inst.DB_AllianceMagic.GetCurrentPowerUpSkill();
    }
  }

  protected int CurrentSelectCount
  {
    get
    {
      return this.m_progressBar.CurrentCount;
    }
  }

  protected int ElixirCount
  {
    get
    {
      ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(this.ElixirItemInfo.internalId);
      if (consumableItemData != null)
        return consumableItemData.quantity;
      return 0;
    }
  }

  protected int MaxSelectCount
  {
    get
    {
      int b = (int) ((double) (this.MaxPower - this.CurrentPower) / (double) this.PowerAddedPerElixir);
      if ((double) b * (double) this.PowerAddedPerElixir + (double) this.CurrentPower < (double) this.MaxPower)
        ++b;
      return Mathf.Min(this.ElixirCount, b);
    }
  }

  protected int MaxPower
  {
    get
    {
      if (this.CurrentPowerUpMagicData != null)
      {
        TempleSkillInfo byId = ConfigManager.inst.DB_TempleSkill.GetById(this.CurrentPowerUpMagicData.ConfigId);
        if (byId != null)
          return byId.MaxDonation;
      }
      return 0;
    }
  }

  protected int CurrentPower
  {
    get
    {
      if (this.CurrentPowerUpMagicData != null)
        return this.CurrentPowerUpMagicData.Donation;
      return 0;
    }
  }

  protected int TargetPower
  {
    get
    {
      return (int) ((double) this.CurrentPower + (double) this.CurrentSelectCount * (double) this.PowerAddedPerElixir);
    }
  }

  protected float PowerAddedPerElixir
  {
    get
    {
      return this.ElixirItemInfo.Value;
    }
  }

  protected double CdInHours
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_temple_mana_donation_join_cd");
      if (data != null)
        return data.Value;
      return 0.0;
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_progressBar.CurrentCount = 0;
    this.m_progressBar.onValueChanged += new System.Action<int>(this.OnSelectCountChanged);
    DBManager.inst.DB_AllianceMagic.onDataChanged += new System.Action<AllianceMagicData>(this.OnMagicDataChanged);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    DBManager.inst.DB_AllianceMagic.onDataChanged -= new System.Action<AllianceMagicData>(this.OnMagicDataChanged);
  }

  protected void OnMagicDataChanged(AllianceMagicData magicData)
  {
    AllianceMagicData powerUpMagicData = this.CurrentPowerUpMagicData;
    if (powerUpMagicData == null)
    {
      this.OnCloseButtonClicked();
    }
    else
    {
      if (powerUpMagicData.MagicId != magicData.MagicId)
        return;
      this.UpdateUI();
    }
  }

  public ItemStaticInfo ElixirItemInfo
  {
    get
    {
      if (this._ElixirItemInfo == null)
        this._ElixirItemInfo = ConfigManager.inst.DB_Items.GetItem("item_alliance_temple_mana");
      return this._ElixirItemInfo;
    }
  }

  protected void UpdateUI()
  {
    this.m_labelPower.text = string.Format("{0}/{1}({2:0.00%})", (object) (this.CurrentPower + this.CurrentSelectCount), (object) this.MaxPower, (object) (float) ((double) this.CurrentPower / (double) this.MaxPower));
    this.m_sliderPowerCurrent.value = (float) this.CurrentPower / (float) this.MaxPower;
    this.m_sliderPowerTarget.value = (float) this.TargetPower / (float) this.MaxPower;
    this.m_itemIcon.SetData(this.ElixirItemInfo.ImagePath, new string[3]
    {
      this.ElixirItemInfo.LocName,
      ScriptLocalization.GetWithPara("alliance_altar_spell_power_up_desc", new Dictionary<string, string>()
      {
        {
          "0",
          this.PowerAddedPerElixir.ToString()
        },
        {
          "1",
          this.CdInHours.ToString()
        }
      }, true),
      this.ElixirCount.ToString()
    });
    this.m_progressBar.Init(Math.Min(this.CurrentSelectCount, this.MaxSelectCount), this.MaxSelectCount);
    this.m_buttonPowerUp.isEnabled = this.CurrentSelectCount > 0;
  }

  protected void OnSelectCountChanged(int count)
  {
    this.UpdateUI();
  }

  public void OnUseButtonClicked()
  {
    if (this.CurrentPowerUpMagicData == null || PlayerData.inst.allianceData == null)
      return;
    AllianceMemberData allianceMemberData = PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid);
    if (allianceMemberData == null)
      return;
    int num1 = NetServerTime.inst.ServerTimestamp - (int) allianceMemberData.joinTime;
    int num2 = (int) (this.CdInHours * 60.0 * 60.0);
    if (num1 <= num2)
    {
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_wait_time_remaining", new Dictionary<string, string>()
      {
        {
          "0",
          ((num2 - num1) / 60 + 1).ToString()
        }
      }, true), (System.Action) null, 4f, true);
    }
    else
    {
      if (this.CurrentSelectCount <= 0)
        return;
      MessageHub.inst.GetPortByAction("Item:useConsumableItem").SendRequest(new Hashtable()
      {
        {
          (object) "uid",
          (object) PlayerData.inst.uid
        },
        {
          (object) "item_id",
          (object) this.ElixirItemInfo.internalId
        },
        {
          (object) "city_id",
          (object) PlayerData.inst.cityId
        },
        {
          (object) "amount",
          (object) this.CurrentSelectCount
        },
        {
          (object) "magic_id",
          (object) this.CurrentPowerUpMagicData.MagicId
        }
      }, new System.Action<bool, object>(this.OnPowerUpCallback), true);
    }
  }

  protected void OnPowerUpCallback(bool result, object data)
  {
    if (!result)
      return;
    this.OnCloseButtonClicked();
  }

  protected void OnUseCallback(bool result, object data)
  {
    if (result)
      ;
  }

  public void OnCloseButtonClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }
}
