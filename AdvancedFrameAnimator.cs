﻿// Decompiled with JetBrains decompiler
// Type: AdvancedFrameAnimator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AdvancedFrameAnimator : MonoBehaviour
{
  public string mFramePrefix = string.Empty;
  public int mZeroPadding = 3;
  public string mAnimationList = string.Empty;
  public float mClipDuration = 1f;
  public bool mLoop = true;
  private int[] _expandedFrames = new int[(int) byte.MaxValue];
  public UISprite mTargetSprite;
  private float _currentFrameIndex;
  private int _currentFrame;
  private int _expandedFrameCount;

  public int ExpandedFrameCount
  {
    get
    {
      return this._expandedFrameCount;
    }
    set
    {
      this._expandedFrameCount = value;
    }
  }

  private void Start()
  {
    this.SubmitFrame(ref this._expandedFrames);
    this._currentFrameIndex = (float) Utils.RndRange(0, this._expandedFrameCount);
  }

  public void SubmitFrame(ref int[] frameArray)
  {
    string[] strArray1 = this.mAnimationList.Replace(" ", string.Empty).Split(',');
    for (int index = 0; index < strArray1.Length; ++index)
    {
      if (strArray1[index].IndexOf("-") > -1)
      {
        string[] strArray2 = strArray1[index].Split('-');
        int num1 = int.Parse(strArray2[0]);
        int num2 = int.Parse(strArray2[1]);
        if (num1 < num2)
        {
          while (num1 <= num2)
            frameArray[this._expandedFrameCount++] = num1++;
        }
        else
        {
          while (num1 >= num2)
            frameArray[this._expandedFrameCount++] = num1--;
        }
      }
      else
        frameArray[this._expandedFrameCount++] = !(strArray1[index] == "*") ? int.Parse(strArray1[index]) : -1;
    }
  }

  public void ResetAnimation()
  {
    this._currentFrameIndex = 0.0f;
  }

  private void Update()
  {
    if ((Object) this.mTargetSprite == (Object) null)
      return;
    this._currentFrameIndex += Time.deltaTime;
    if (this.mLoop)
    {
      if ((double) this._currentFrameIndex > (double) this.mClipDuration)
        this._currentFrameIndex -= this.mClipDuration;
    }
    else if ((double) this._currentFrameIndex > (double) this.mClipDuration)
      this._currentFrameIndex = this.mClipDuration;
    this._currentFrame = (int) Utils.GraphLerp(0.0f, this.mClipDuration, 0.0f, (float) this._expandedFrameCount, this._currentFrameIndex);
    if (this._currentFrame < 0)
    {
      this.mTargetSprite.gameObject.SetActive(false);
    }
    else
    {
      this.mTargetSprite.gameObject.SetActive(true);
      if (this._currentFrame < 0 || this._currentFrame >= this._expandedFrames.Length)
        return;
      this.mTargetSprite.spriteName = this.mFramePrefix + Utils.ZeroPad(this._expandedFrames[this._currentFrame], this.mZeroPadding);
    }
  }
}
