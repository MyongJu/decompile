﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightTalentSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class DragonKnightTalentSlot : MonoBehaviour
{
  private List<string> romanNumbers = new List<string>()
  {
    "I",
    "II",
    "III",
    "IV",
    "V",
    "VI",
    "VII",
    "VIII",
    "IX",
    "X",
    "XI",
    "XII",
    "XIII",
    "XIV",
    "XV"
  };
  public System.Action<int> onTalentSlotClick;
  private int _talentTreeId;
  [SerializeField]
  public DragonKnightTalentSlot.Panel panel;

  public bool hideTitle
  {
    set
    {
      this.panel.talentName.gameObject.SetActive(!value);
      this.panel.talentAdvanceName.gameObject.SetActive(!value);
    }
  }

  public bool hideLevel
  {
    set
    {
      this.panel.talentLevelContainer.gameObject.SetActive(!value);
    }
  }

  public int talentTreeId
  {
    get
    {
      return this._talentTreeId;
    }
    set
    {
      if (value == 0)
        return;
      DragonKnightTalentInfo talentByTalentTreeId1 = ConfigManager.inst.DB_DragonKnightTalent.GetMaxLevelTalentByTalentTreeId(value);
      DragonKnightTalentInfo talentByTalentTreeId2 = ConfigManager.inst.DB_DragonKnightTalent.GetCurrentLevelTalentByTalentTreeId(value);
      if (this._talentTreeId != value)
        BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.icon, talentByTalentTreeId1.imagePath, (System.Action<bool>) null, true, false, string.Empty);
      this._talentTreeId = value;
      if (talentByTalentTreeId1.tier == -1)
      {
        this.panel.talentAdvanceLevelContainer.gameObject.SetActive(false);
        this.panel.talentLevelContainer.gameObject.SetActive(false);
        this.panel.talentFrame.gameObject.SetActive(false);
        this.panel.talentAdvanceFrame.gameObject.SetActive(true);
        this.panel.talentAdvanceName.text = talentByTalentTreeId1.LocName;
      }
      else if (talentByTalentTreeId1.tier == 0)
      {
        this.panel.talentAdvanceLevelContainer.gameObject.SetActive(false);
        this.panel.talentLevelContainer.gameObject.SetActive(true);
        this.panel.talentFrame.gameObject.SetActive(true);
        this.panel.talentAdvanceFrame.gameObject.SetActive(false);
        this.panel.talentName.text = talentByTalentTreeId1.LocName;
      }
      else if (talentByTalentTreeId1.tier > 0 && talentByTalentTreeId1.tier <= this.romanNumbers.Count)
      {
        this.panel.talentAdvanceLevelContainer.gameObject.SetActive(true);
        this.panel.talentLevelContainer.gameObject.SetActive(true);
        this.panel.telentAdvanceLevel.text = this.romanNumbers[talentByTalentTreeId1.tier - 1];
        this.panel.talentFrame.gameObject.SetActive(true);
        this.panel.talentAdvanceFrame.gameObject.SetActive(false);
        this.panel.talentName.text = talentByTalentTreeId1.LocName;
      }
      if (ConfigManager.inst.DB_DragonKnightTalent.IsTalentTreeRequirementsComplete(this._talentTreeId))
      {
        GreyUtility.Normal(this.panel.icon.gameObject);
        this.panel.talentLevel.color = this.panel.CL_enable;
        this.panel.telentAdvanceLevel.color = this.panel.CL_enable;
      }
      else
      {
        GreyUtility.Grey(this.panel.icon.gameObject);
        this.panel.talentLevel.color = this.panel.CL_disable;
        this.panel.telentAdvanceLevel.color = this.panel.CL_disable;
      }
      this.panel.talentLevel.text = talentByTalentTreeId2 != null ? string.Format("{0}/{1}", (object) talentByTalentTreeId2.level, (object) talentByTalentTreeId1.maxLevel) : string.Format("{0}/{1}", (object) 0, (object) talentByTalentTreeId1.maxLevel);
      this.hideTitle = false;
      this.hideLevel = false;
    }
  }

  public void OnSlotClick()
  {
    if (this.onTalentSlotClick == null)
      return;
    this.onTalentSlotClick(this._talentTreeId);
  }

  public void ToggleBoxcollider(bool enable)
  {
    this.panel.icon.GetComponent<BoxCollider>().enabled = enable;
  }

  [Serializable]
  public class Panel
  {
    public Color CL_enable;
    public Color CL_disable;
    public UITexture icon;
    public UILabel talentName;
    public UILabel talentAdvanceName;
    public Transform talentFrame;
    public Transform talentAdvanceFrame;
    public Transform talentLevelContainer;
    public UILabel talentLevel;
    public Transform talentAdvanceLevelContainer;
    public UILabel telentAdvanceLevel;
    public UIDragScrollView dragScrollView;
  }
}
