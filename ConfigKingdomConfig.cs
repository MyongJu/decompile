﻿// Decompiled with JetBrains decompiler
// Type: ConfigKingdomConfig
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigKingdomConfig
{
  public string[] languageGroupKeys = new string[2]
  {
    "kingdom_group_1_name",
    "kingdom_group_2_name"
  };
  public Dictionary<int, KingdomConfigInfo> datas;

  public ConfigKingdomConfig()
  {
    this.datas = new Dictionary<int, KingdomConfigInfo>();
  }

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exsit", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "value");
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          KingdomConfigInfo data = new KingdomConfigInfo();
          if (int.TryParse(key.ToString(), out data.internalId))
          {
            ArrayList arr = hashtable[key] as ArrayList;
            if (arr != null)
            {
              data.String = ConfigManager.GetString(arr, index1);
              data.Value = ConfigManager.GetInt(arr, index2);
              this.PushData(data.internalId, data);
            }
          }
        }
      }
    }
  }

  public Dictionary<int, KingdomConfigInfo>.ValueCollection Values
  {
    get
    {
      return this.datas.Values;
    }
  }

  private void PushData(int internalId, KingdomConfigInfo data)
  {
    if (this.datas.ContainsKey(internalId))
      return;
    this.datas.Add(internalId, data);
    ConfigManager.inst.AddData(internalId, (object) data);
  }

  public void Clear()
  {
    if (this.datas == null)
      return;
    this.datas.Clear();
  }

  public KingdomConfigInfo GetData(int internalId)
  {
    if (this.datas.ContainsKey(internalId))
      return this.datas[internalId];
    return (KingdomConfigInfo) null;
  }
}
