﻿// Decompiled with JetBrains decompiler
// Type: AllianceSearchItemData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class AllianceSearchItemData
{
  public long allianceID;
  public int symbolCode;
  public string tag;
  public string name;
  public long power;
  public int memberCount;
  public int memberLimit;
  public int giftLevel;
  public string language;
  public long powerLimit;
  public int level;
  public bool isPrivate;
  public string creatorName;
  public int creatorPortrait;
  public int creatorLordTitleId;

  public void Decode(object data)
  {
    Hashtable inData = data as Hashtable;
    DatabaseTools.UpdateData(inData, "alliance_id", ref this.allianceID);
    DatabaseTools.UpdateData(inData, "name", ref this.name);
    DatabaseTools.UpdateData(inData, "acronym", ref this.tag);
    DatabaseTools.UpdateData(inData, "language", ref this.language);
    DatabaseTools.UpdateData(inData, "symbol_code", ref this.symbolCode);
    DatabaseTools.UpdateData(inData, "power", ref this.power);
    DatabaseTools.UpdateData(inData, "member_count", ref this.memberCount);
    DatabaseTools.UpdateData(inData, "member_max", ref this.memberLimit);
    DatabaseTools.UpdateData(inData, "gift_points", ref this.giftLevel);
    this.giftLevel = ConfigManager.inst.DB_Gift_Level.GetLevelByPoints(this.giftLevel);
    DatabaseTools.UpdateData(inData, "power_limit", ref this.powerLimit);
    DatabaseTools.UpdateData(inData, "level", ref this.level);
    int outData = 0;
    DatabaseTools.UpdateData(inData, "is_private", ref outData);
    this.isPrivate = outData == 1;
    DatabaseTools.UpdateData(inData, "creator_name", ref this.creatorName);
    DatabaseTools.UpdateData(inData, "creator_portrait", ref this.creatorPortrait);
    DatabaseTools.UpdateData(inData, "creator_lord_title", ref this.creatorLordTitleId);
  }

  public void Decode(AllianceData allianceData)
  {
    this.allianceID = allianceData.allianceId;
    this.name = allianceData.allianceName;
    this.tag = allianceData.allianceAcronym;
    this.language = allianceData.language;
    this.symbolCode = allianceData.allianceSymbolCode;
    this.power = allianceData.power;
    this.memberCount = allianceData.memberCount;
    this.memberLimit = allianceData.memberMax;
    this.giftLevel = ConfigManager.inst.DB_Gift_Level.GetLevelByPoints((int) allianceData.giftPoints);
    this.powerLimit = (long) allianceData.powerLimit;
    this.level = allianceData.allianceLevel;
    this.isPrivate = allianceData.isPirvate;
    this.creatorName = DBManager.inst.DB_User.Get(allianceData.creatorId).userName;
  }
}
