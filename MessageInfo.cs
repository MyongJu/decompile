﻿// Decompiled with JetBrains decompiler
// Type: MessageInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class MessageInfo
{
  private long _time;
  private object _data;
  private string _action;
  private object _payload;
  private string _message;
  private bool _ret;
  private System.Action<bool, object> _callback;

  public string Action
  {
    get
    {
      return this._action;
    }
    set
    {
      this._action = value;
    }
  }

  public object Data
  {
    get
    {
      return this._data;
    }
    set
    {
      this._data = value;
    }
  }

  public object Payload
  {
    get
    {
      return this._payload;
    }
    set
    {
      this._payload = value;
    }
  }

  public string Message
  {
    get
    {
      return this._message;
    }
    set
    {
      this._message = value;
    }
  }

  public long TimeStamp
  {
    get
    {
      return this._time;
    }
    set
    {
      this._time = value;
    }
  }

  public bool Ret
  {
    get
    {
      return this._ret;
    }
    set
    {
      this._ret = value;
    }
  }

  public System.Action<bool, object> Callback
  {
    get
    {
      return this._callback;
    }
    set
    {
      this._callback = value;
    }
  }

  public void Dispose()
  {
    this._time = 0L;
    this._data = (object) null;
    this._action = (string) null;
    this._payload = (object) null;
    this._ret = false;
    this._callback = (System.Action<bool, object>) null;
    this._message = (string) null;
  }
}
