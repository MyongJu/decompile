﻿// Decompiled with JetBrains decompiler
// Type: DesignResolution
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DesignResolution : MonoBehaviour
{
  private float m_DesignResolutionRatio = 1.777778f;
  private const float DEFAULT_DESIGN_RATIO = 1.777778f;

  private void Awake()
  {
    this.AdjustResolution();
  }

  private void AdjustResolution()
  {
    float num1 = (float) Screen.width * 1f / (float) Screen.height;
    float num2 = num1 / this.m_DesignResolutionRatio;
    if ((double) num1 >= (double) this.m_DesignResolutionRatio)
      num2 = 1f;
    if ((double) num2 == (double) this.transform.localScale.x && (double) num2 == (double) this.transform.localScale.y)
      return;
    this.gameObject.transform.localScale = new Vector3(num2, num2, num2);
  }
}
