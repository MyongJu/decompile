﻿// Decompiled with JetBrains decompiler
// Type: ConfigSuperLoginGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigSuperLoginGroup
{
  private List<SuperLoginGroupInfo> superLoginGroupInfoList = new List<SuperLoginGroupInfo>();
  private Dictionary<string, SuperLoginGroupInfo> datas;
  private Dictionary<int, SuperLoginGroupInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<SuperLoginGroupInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    Dictionary<string, SuperLoginGroupInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this.superLoginGroupInfoList.Add(enumerator.Current);
    }
  }

  public List<SuperLoginGroupInfo> GetSuperLoginGroupInfoList()
  {
    return this.superLoginGroupInfoList;
  }

  public SuperLoginGroupInfo Get(int interalId)
  {
    if (this.dicByUniqueId.ContainsKey(interalId))
      return this.dicByUniqueId[interalId];
    return (SuperLoginGroupInfo) null;
  }

  public SuperLoginGroupInfo Get(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (SuperLoginGroupInfo) null;
  }
}
