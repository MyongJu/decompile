﻿// Decompiled with JetBrains decompiler
// Type: FestivalExchangeRewardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class FestivalExchangeRewardInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "type")]
  public string type;
  [Config(Name = "item_reward_id")]
  public int itemRewardId;
  [Config(Name = "item_reward_value")]
  public int itemRewardValue;
  [Config(Name = "cost")]
  public int cost;
  [Config(Name = "min_level")]
  public int minLevel;
}
