﻿// Decompiled with JetBrains decompiler
// Type: AllianceActivityUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;

public class AllianceActivityUIData : RoundActivityUIData
{
  public override int ActivityMainConfigID
  {
    get
    {
      return ConfigManager.inst.DB_AllianceActivityMainSub.GetData(this.CurrentRound.ActivitySubConfigID).ActivityMainID;
    }
  }

  public override string EventKey
  {
    get
    {
      return "alliance_activity";
    }
  }

  public override ActivityBaseUIData.ActivityType Type
  {
    get
    {
      return ActivityBaseUIData.ActivityType.AllianceActivity;
    }
  }

  public override string GetNormalDesc()
  {
    return ScriptLocalization.Get("event_alliance_one_line_description", true);
  }

  public override string GetUnStartDesc()
  {
    return ScriptLocalization.Get("event_alliance_description", true);
  }

  public override void DisplayActivityDetail()
  {
    RoundActivityData data = this.Data as RoundActivityData;
    if (!this.IsStart())
      UIManager.inst.OpenPopup("Activity/ActivityNotStartedPopup", (Popup.PopupParameter) new ActivityNotStartPopup.Parameter()
      {
        baseData = (ActivityBaseUIData) this
      });
    else if (PlayerData.inst.allianceId <= 0L)
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_not_join_alliance_event", true), (System.Action) null, 4f, false);
    else
      UIManager.inst.OpenDlg("Activity/AllianceEventlDlg", (UI.Dialog.DialogParameter) new AllianceActivityDetailDlg.Parameter()
      {
        data = data
      }, 1 != 0, 1 != 0, 1 != 0);
  }
}
