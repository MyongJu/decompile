﻿// Decompiled with JetBrains decompiler
// Type: ConfigAllianceTech
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigAllianceTech
{
  private ConfigParse parse = new ConfigParse();
  private ConfigParse parse1 = new ConfigParse();
  private Dictionary<string, AllianceTechInfo> allianceTechInfoDicByID;
  private Dictionary<int, AllianceTechInfo> allianceTechInfoDicByInternalId;
  private Dictionary<string, Dictionary<int, AllianceTechInfo>> type2AllianceTechInfo;
  private Dictionary<int, List<AllianceTechInfo>> tier2type;
  private Dictionary<string, int> typeIndex;
  private Dictionary<string, AllianceTechInfo> typeMinLevelInfo;
  private Dictionary<string, AllianceTechInfo> typeMaxLevelInfo;
  private Dictionary<string, AllianceTechTier> allianceTechTierDicByID;
  private Dictionary<int, AllianceTechTier> allianceTechTierDicByInternalId;

  public int maxTier { get; private set; }

  public void BuildTierDB(object res)
  {
    this.parse1.Parse<AllianceTechTier, string>(res as Hashtable, "ID", out this.allianceTechTierDicByID, out this.allianceTechTierDicByInternalId);
  }

  public AllianceTechTier GetAllianceTechTier(int id)
  {
    if (this.allianceTechTierDicByInternalId.ContainsKey(id))
      return this.allianceTechTierDicByInternalId[id];
    return (AllianceTechTier) null;
  }

  public object ParseDict(object rs)
  {
    Hashtable hashtable = rs as Hashtable;
    Dictionary<int, float> dictionary = new Dictionary<int, float>();
    if (hashtable == null)
      return (object) dictionary;
    IEnumerator enumerator = hashtable.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      string s = enumerator.Current.ToString();
      int result1 = -1;
      float result2;
      if (int.TryParse(s, out result1) && float.TryParse(hashtable[enumerator.Current].ToString(), out result2))
        dictionary.Add(result1, result2);
    }
    return (object) dictionary;
  }

  public void BuildDB(object res)
  {
    this.parse.AddParseFunc("ParseBenfits", new ParseFunc(this.ParseDict));
    this.parse.Parse<AllianceTechInfo, string>(res as Hashtable, "ID", out this.allianceTechInfoDicByID, out this.allianceTechInfoDicByInternalId);
    this.type2AllianceTechInfo = new Dictionary<string, Dictionary<int, AllianceTechInfo>>();
    this.tier2type = new Dictionary<int, List<AllianceTechInfo>>();
    this.typeIndex = new Dictionary<string, int>();
    this.typeMinLevelInfo = new Dictionary<string, AllianceTechInfo>();
    this.typeMaxLevelInfo = new Dictionary<string, AllianceTechInfo>();
    this.maxTier = 0;
    Dictionary<int, AllianceTechInfo>.Enumerator enumerator1 = this.allianceTechInfoDicByInternalId.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      AllianceTechInfo allianceTechInfo = enumerator1.Current.Value;
      allianceTechInfo.exps[0] = 0;
      allianceTechInfo.exps[1] = allianceTechInfo.Exp1;
      allianceTechInfo.exps[2] = allianceTechInfo.Exp2;
      allianceTechInfo.exps[3] = allianceTechInfo.Exp3;
      allianceTechInfo.exps[4] = allianceTechInfo.Exp4;
      allianceTechInfo.exps[5] = allianceTechInfo.Exp5;
      for (int index = 0; index < allianceTechInfo.exps.Length; ++index)
      {
        if (allianceTechInfo.exps[index] > 0 && allianceTechInfo.exps[index] == allianceTechInfo.exps[5])
        {
          allianceTechInfo.TotalExp = allianceTechInfo.exps[index];
          allianceTechInfo.maxExpIndex = index;
          break;
        }
      }
      Dictionary<int, AllianceTechInfo> dictionary;
      if (this.type2AllianceTechInfo.ContainsKey(allianceTechInfo.ResearchType))
      {
        dictionary = this.type2AllianceTechInfo[allianceTechInfo.ResearchType];
      }
      else
      {
        dictionary = new Dictionary<int, AllianceTechInfo>();
        this.type2AllianceTechInfo.Add(allianceTechInfo.ResearchType, dictionary);
      }
      dictionary.Add(allianceTechInfo.Level, allianceTechInfo);
      if (allianceTechInfo.TierIndex > this.maxTier)
        this.maxTier = allianceTechInfo.TierIndex;
      if (!this.tier2type.ContainsKey(allianceTechInfo.TierIndex))
        this.tier2type.Add(allianceTechInfo.TierIndex, new List<AllianceTechInfo>());
      bool flag = false;
      for (int index = 0; index < this.tier2type[allianceTechInfo.TierIndex].Count; ++index)
      {
        if (this.tier2type[allianceTechInfo.TierIndex][index].ResearchType == allianceTechInfo.ResearchType)
        {
          flag = true;
          break;
        }
      }
      if (!flag)
        this.tier2type[allianceTechInfo.TierIndex].Add(allianceTechInfo);
      if (!this.typeIndex.ContainsKey(allianceTechInfo.ResearchType))
        this.typeIndex.Add(allianceTechInfo.ResearchType, allianceTechInfo.Index);
      if (!this.typeMinLevelInfo.ContainsKey(allianceTechInfo.ResearchType))
        this.typeMinLevelInfo.Add(allianceTechInfo.ResearchType, allianceTechInfo);
      else if (allianceTechInfo.Level < this.typeMinLevelInfo[allianceTechInfo.ResearchType].Level)
        this.typeMinLevelInfo[allianceTechInfo.ResearchType] = allianceTechInfo;
      if (!this.typeMaxLevelInfo.ContainsKey(allianceTechInfo.ResearchType))
        this.typeMaxLevelInfo.Add(allianceTechInfo.ResearchType, allianceTechInfo);
      else if (allianceTechInfo.Level > this.typeMaxLevelInfo[allianceTechInfo.ResearchType].Level)
        this.typeMaxLevelInfo[allianceTechInfo.ResearchType] = allianceTechInfo;
    }
    Dictionary<int, List<AllianceTechInfo>>.ValueCollection.Enumerator enumerator2 = this.tier2type.Values.GetEnumerator();
    while (enumerator2.MoveNext())
      enumerator2.Current.Sort((Comparison<AllianceTechInfo>) ((a, b) => a.Index.CompareTo(b.Index)));
  }

  public AllianceTechInfo GetNextAllianceTechInfo(int currentId)
  {
    AllianceTechInfo allianceTechInfo = this.GetItem(currentId);
    if (allianceTechInfo != null && this.type2AllianceTechInfo.ContainsKey(allianceTechInfo.ResearchType))
    {
      Dictionary<int, AllianceTechInfo> dictionary = this.type2AllianceTechInfo[allianceTechInfo.ResearchType];
      if (dictionary.ContainsKey(allianceTechInfo.Level + 1))
        return dictionary[allianceTechInfo.Level + 1];
    }
    return (AllianceTechInfo) null;
  }

  public bool IsMaxLevel(int currentId)
  {
    return this.GetNextAllianceTechInfo(currentId) == null;
  }

  public void Clear()
  {
    if (this.allianceTechInfoDicByID != null)
    {
      this.allianceTechInfoDicByID.Clear();
      this.allianceTechInfoDicByID = (Dictionary<string, AllianceTechInfo>) null;
    }
    if (this.allianceTechInfoDicByInternalId == null)
      return;
    this.allianceTechInfoDicByInternalId.Clear();
    this.allianceTechInfoDicByInternalId = (Dictionary<int, AllianceTechInfo>) null;
  }

  public bool Contains(int internalId)
  {
    return this.allianceTechInfoDicByInternalId.ContainsKey(internalId);
  }

  public bool Contains(string id)
  {
    return this.allianceTechInfoDicByID.ContainsKey(id);
  }

  public AllianceTechInfo GetItem(int internalId)
  {
    if (this.allianceTechInfoDicByInternalId != null && this.allianceTechInfoDicByInternalId.ContainsKey(internalId))
      return this.allianceTechInfoDicByInternalId[internalId];
    return (AllianceTechInfo) null;
  }

  public AllianceTechInfo GetItem(string id)
  {
    if (this.allianceTechInfoDicByID.ContainsKey(id))
      return this.allianceTechInfoDicByID[id];
    return (AllianceTechInfo) null;
  }

  public float GetProgressInTotal(int researchId, int curValue)
  {
    AllianceTechInfo allianceTechInfo = this.GetItem(researchId);
    if (allianceTechInfo == null)
      return 0.0f;
    if (allianceTechInfo.TotalExp <= curValue)
      return 1f;
    for (int index = 1; index <= allianceTechInfo.maxExpIndex; ++index)
    {
      if (allianceTechInfo.exps[index] > curValue)
        return 1f / (float) allianceTechInfo.maxExpIndex * (float) (index - 1) + 1f / (float) allianceTechInfo.maxExpIndex * (float) (curValue - allianceTechInfo.exps[index - 1]) / (float) (allianceTechInfo.exps[index] - allianceTechInfo.exps[index - 1]);
    }
    return 0.0f;
  }

  public int GetTierRequireLevel(int tier)
  {
    if (this.tier2type.ContainsKey(tier) && this.tier2type[tier] != null && (this.tier2type[tier].Count > 0 && this.allianceTechTierDicByInternalId.ContainsKey(this.tier2type[tier][0].TierId)) && (ConfigManager.inst.DB_Alliance_Level != null && ConfigManager.inst.DB_Alliance_Level.GetData(this.allianceTechTierDicByInternalId[this.tier2type[tier][0].TierId].ReqAllianceLevel) != null))
      return ConfigManager.inst.DB_Alliance_Level.GetData(this.allianceTechTierDicByInternalId[this.tier2type[tier][0].TierId].ReqAllianceLevel).Alliance_Level;
    return 10000;
  }

  public List<AllianceTechInfo> GetTechTypeByTier(int tier)
  {
    if (this.tier2type.ContainsKey(tier))
      return this.tier2type[tier];
    return (List<AllianceTechInfo>) null;
  }

  public AllianceTechInfo GetMinTechInfoByType(string type)
  {
    if (this.typeMinLevelInfo.ContainsKey(type))
      return this.typeMinLevelInfo[type];
    return (AllianceTechInfo) null;
  }

  public AllianceTechInfo GetMaxTechInfoByType(string type)
  {
    if (this.typeMaxLevelInfo.ContainsKey(type))
      return this.typeMaxLevelInfo[type];
    return (AllianceTechInfo) null;
  }

  public AllianceTechInfo GetTechInfoByTypeAndLevel(string type, int level)
  {
    if (this.type2AllianceTechInfo.ContainsKey(type))
    {
      Dictionary<int, AllianceTechInfo> dictionary = this.type2AllianceTechInfo[type];
      if (dictionary != null && dictionary.ContainsKey(level))
        return dictionary[level];
    }
    return (AllianceTechInfo) null;
  }
}
