﻿// Decompiled with JetBrains decompiler
// Type: BuildingConstruction
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingConstruction : UI.Dialog
{
  private List<GameObject> _requirements = new List<GameObject>();
  private string _buildingClass = string.Empty;
  private Dictionary<string, int> m_lackitem = new Dictionary<string, int>(1);
  private Dictionary<string, long> m_lackRes = new Dictionary<string, long>(4);
  private Hashtable requireHT = new Hashtable();
  [Tooltip("The build zoom distance")]
  public float zoomFocusDis = 0.8f;
  public float strongholdZoomFocusDis = 0.8f;
  private List<LegendInfo> unlockLegends = new List<LegendInfo>();
  public UITexture mBuildingIcon;
  public UILabel mTargetLevel;
  public UILabel mCurLevel;
  public UILabel mConstructionTimeBoost;
  public UILabel mInstantBuildValue;
  public UILabel mDialogTitle;
  public UILabel msmallTitle;
  public GameObject mQueueFullReqBtn;
  public GameObject mBuildingReqBtn;
  public GameObject mWoodReqBtn;
  public GameObject mFoodReqBtn;
  public GameObject mSilverReqBtn;
  public GameObject mOreReqBtn;
  public GameObject mItemReqBtn;
  public GameObject mBuildLimitReqBtn;
  public GameObject mRewardHeader;
  public GameObject mRewardHeroXP;
  public GameObject mRewardPower;
  public GameObject mRewardDefault;
  public UIButton mBuildBtn;
  public UIButton mInstBuidBtn;
  public UILabel mBuildLabel;
  public UILabel mInstBuildLabel;
  public UITexture dynaBG;
  public GameObject mQueueSpeedupPopup;
  public UIScrollView sv;
  public ConstructionInfoComponent consructionInfo;
  private GameObject _targetPlot;
  private System.Action _onFinished;
  private int _instBuildAmount;
  private bool bQueueIsFull;
  private float _requirementsY;
  private float _rewardsY;
  private CityManager.BuildingItem _buildingItem;
  private double speedTime;
  private int gold;
  private Popup resPopUp;
  private ItemStaticInfo itemLackCannotBuy;
  private int itemLackNeedValue;
  private int instBuildAmount;
  private bool bshowPopup;
  private string _buildingid;
  private int _level;

  private event System.Action gotobtnDelegate;

  public void SetDetails(CityManager.BuildingItem buildingItem, string iconStr, int buildingLevel, double normalSeconds, double boostSeconds, int instBuildAmount, string buildingName, GameObject targetPlot, int iLevel, string buildingClass, System.Action onFinished)
  {
    this._buildingItem = buildingItem;
    this._targetPlot = targetPlot;
    this._buildingClass = buildingClass;
    this._onFinished = onFinished;
    this._instBuildAmount = instBuildAmount;
    this.bQueueIsFull = false;
    this.mTargetLevel.text = buildingLevel.ToString();
    this.mCurLevel.text = (buildingLevel - 1).ToString();
    this.mConstructionTimeBoost.text = Utils.ConvertSecsToString(boostSeconds);
    this.mInstantBuildValue.text = Utils.FormatThousands(instBuildAmount.ToString());
    this.mDialogTitle.text = ScriptLocalization.Get(buildingName, true);
    this.msmallTitle.text = this.mDialogTitle.text;
    this.ResetData();
  }

  public void SetQueueFull()
  {
    this.bQueueIsFull = true;
  }

  public GameObject AddRequirement(BuildingConstruction.Requirement requirement, string icon, string name, double amount, double need, EventDelegate ed = null, double originNeed = 0)
  {
    GameObject prefab = (GameObject) null;
    switch (requirement)
    {
      case BuildingConstruction.Requirement.FOOD:
        prefab = NGUITools.AddChild(this.mFoodReqBtn.transform.parent.gameObject, this.mFoodReqBtn);
        break;
      case BuildingConstruction.Requirement.WOOD:
        prefab = NGUITools.AddChild(this.mWoodReqBtn.transform.parent.gameObject, this.mWoodReqBtn);
        break;
      case BuildingConstruction.Requirement.SILVER:
        prefab = NGUITools.AddChild(this.mSilverReqBtn.transform.parent.gameObject, this.mSilverReqBtn);
        break;
      case BuildingConstruction.Requirement.ORE:
        prefab = NGUITools.AddChild(this.mOreReqBtn.transform.parent.gameObject, this.mOreReqBtn);
        break;
      case BuildingConstruction.Requirement.BUILDING:
        prefab = NGUITools.AddChild(this.mBuildingReqBtn.transform.parent.gameObject, this.mBuildingReqBtn);
        break;
      case BuildingConstruction.Requirement.ITEM:
        prefab = NGUITools.AddChild(this.mItemReqBtn.transform.parent.gameObject, this.mItemReqBtn);
        break;
      case BuildingConstruction.Requirement.QUEUE:
        prefab = NGUITools.AddChild(this.mQueueFullReqBtn.transform.parent.gameObject, this.mQueueFullReqBtn);
        break;
      case BuildingConstruction.Requirement.LIMIT:
        prefab = NGUITools.AddChild(this.mBuildLimitReqBtn.transform.parent.gameObject, this.mBuildLimitReqBtn);
        break;
    }
    prefab.SetActive(true);
    this.AdjustHeight(ref prefab);
    ConstructionRequirementBtn component1 = prefab.GetComponent<ConstructionRequirementBtn>();
    UIButton component2 = component1.mGetMoreBtn.GetComponent<UIButton>();
    component2.onClick.Clear();
    if (ed != null)
      component2.onClick.Add(ed);
    switch (requirement)
    {
      case BuildingConstruction.Requirement.FOOD:
      case BuildingConstruction.Requirement.WOOD:
      case BuildingConstruction.Requirement.SILVER:
      case BuildingConstruction.Requirement.ORE:
        component1.SetDetails(icon, this.DecurateString(amount, need, originNeed), (string) null, amount >= need, true);
        break;
      case BuildingConstruction.Requirement.BUILDING:
        component1.SetDetails(icon, "LV." + need.ToString(), name, amount >= need, true);
        component1.SetLabel(ScriptLocalization.Get("construction_requirements_go_to", true));
        break;
      case BuildingConstruction.Requirement.ITEM:
        int internalId = int.Parse(name);
        ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(internalId);
        ItemStaticInfo itemInfo = ConfigManager.inst.DB_Items.GetItem(dataByInternalId != null ? dataByInternalId.Item_InternalId : internalId);
        component1.SetDetails(icon, amount.ToString() + "/" + need.ToString(), Utils.XLAT(itemInfo.Loc_Name_Id), amount >= need, true);
        component1.SetShopInfo(dataByInternalId);
        component1.SetItemInfo(itemInfo);
        component1.SetRequirement(amount, need);
        if (amount < need)
        {
          component1.SetLabel(ScriptLocalization.Get("construction_requirements_get_more", true));
          break;
        }
        break;
      case BuildingConstruction.Requirement.QUEUE:
        component1.SetDetails(icon, Utils.ConvertNumberToNormalString((long) amount) + "/" + Utils.ConvertNumberToNormalString((long) need), amount >= need ? ScriptLocalization.Get("id_uppercase_queue_full", true) : "QUEUE", amount < need, true);
        component1.SetLabel(this.GetQueueRequireLabelString());
        break;
      case BuildingConstruction.Requirement.LIMIT:
        component1.SetDetails(icon, Utils.ConvertNumberToNormalString((long) amount) + "/" + Utils.ConvertNumberToNormalString((long) need), amount >= need ? "LIMIT FULL" : "LIMIT", amount < need, false);
        break;
    }
    this._requirements.Add(prefab);
    return prefab;
  }

  private string DecurateString(double amount, double need, double originNedd = 0)
  {
    string str1 = "[e71200]";
    string str2 = "[-]";
    string str3 = "[00ff00]";
    string format = "{0}{1}{2}/{3}{4}";
    string str4 = amount >= need ? string.Empty : str1;
    string normalString1 = Utils.ConvertNumberToNormalString((long) amount);
    string str5 = amount >= need ? string.Empty : str2;
    string str6 = string.Empty;
    if (need < originNedd)
      str6 = str3;
    else if (need > originNedd)
      str6 = str1;
    string normalString2 = Utils.ConvertNumberToNormalString((long) need);
    return string.Format(format, (object) str4, (object) normalString1, (object) str5, (object) str6, (object) normalString2);
  }

  private void AdjustHeight(ref GameObject prefab)
  {
    this._requirementsY -= (float) (prefab.GetComponent<UIWidget>().height / 2);
    Utils.SetLPY(prefab, this._requirementsY);
    this._requirementsY -= (float) (prefab.GetComponent<UIWidget>().height / 2);
  }

  public GameObject AddRewards(BuildingConstruction.Rewards reward, string icon, string name, double now, double before)
  {
    GameObject prefab = (GameObject) null;
    switch (reward)
    {
      case BuildingConstruction.Rewards.HERO_XP:
        prefab = NGUITools.AddChild(this.mRewardHeroXP.transform.parent.gameObject, this.mRewardHeroXP);
        prefab.SetActive(true);
        this.AdjustHeight(ref prefab);
        prefab.GetComponent<ConstructionRewardBtn>().SetDetails((string) null, "+" + Utils.ConvertNumberToNormalString((long) now), (string) null);
        break;
      case BuildingConstruction.Rewards.POWER:
        prefab = NGUITools.AddChild(this.mRewardPower.transform.parent.gameObject, this.mRewardPower);
        prefab.SetActive(true);
        this.AdjustHeight(ref prefab);
        prefab.GetComponent<ConstructionRewardBtn>().SetDetails((string) null, "+" + Utils.ConvertNumberToNormalString((long) now), (string) null);
        break;
      case BuildingConstruction.Rewards.DEFAULT:
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[int.Parse(name)];
        if (dbProperty != null)
        {
          prefab = NGUITools.AddChild(this.mRewardDefault.transform.parent.gameObject, this.mRewardDefault);
          prefab.transform.localPosition = this.mRewardDefault.transform.localPosition;
          prefab.SetActive(true);
          this.AdjustHeight(ref prefab);
          ConstructionRewardBtn component = prefab.GetComponent<ConstructionRewardBtn>();
          string str = now < before ? "-" : "+";
          if (dbProperty.Format == PropertyDefinition.FormatType.Integer)
          {
            component.SetDetails((string) null, "[B4B4B4]" + Utils.ConvertNumberToNormalString((long) before) + "[-][54AE07]" + str + Utils.ConvertNumberToNormalString((long) Math.Abs(now - before)) + "[-]", dbProperty.Name);
            break;
          }
          if (dbProperty.Format == PropertyDefinition.FormatType.Percent)
          {
            component.SetDetails((string) null, "[B4B4B4]" + Utils.ConvertNumberToNormalString3(before * 100.0) + "%[-][54AE07]" + str + Utils.ConvertNumberToNormalString3(Math.Abs(now - before) * 100.0) + "%[-]", dbProperty.Name);
            break;
          }
          break;
        }
        break;
    }
    this._requirements.Add(prefab);
    return prefab;
  }

  public void ResetData()
  {
    this.itemLackCannotBuy = (ItemStaticInfo) null;
    for (int index = this._requirements.Count - 1; index >= 0; --index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this._requirements[index]);
    this._requirements.Clear();
    this._requirementsY = this.mQueueFullReqBtn.transform.localPosition.y + (float) (this.mQueueFullReqBtn.GetComponent<UIWidget>().height / 2);
    this._rewardsY = this.mRewardHeroXP.transform.localPosition.y;
  }

  public void OnCloseBtnPressed()
  {
    this.ResetData();
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnBackBtnPressed()
  {
    this.ResetData();
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnGetMoreFoodBtnPressed()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.FOOD);
  }

  public void OnGetMoreWoodBtnPressed()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.WOOD);
  }

  public void OnGetMoreSilverBtnPressed()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.SILVER);
  }

  public void OnGetMoreOreBtnPressed()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.ORE);
  }

  public void GetMoreOfItemPressed()
  {
    ConstructionRequirementBtn component = UICamera.selectedObject.transform.parent.GetComponent<ConstructionRequirementBtn>();
    if (component.GetShopInfo() != null)
    {
      UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) new ItemUseOrBuyPopup.Parameter()
      {
        shop_id = component.GetShopInfo().internalId,
        type = ItemUseOrBuyPopup.Parameter.Type.BuyItem,
        onUseOrBuyCallBack = new System.Action<bool, object>(this.OnBuyCallBack)
      });
    }
    else
    {
      UseItemCannotGoldPopup.Parameter parameter = new UseItemCannotGoldPopup.Parameter();
      ItemStaticInfo itemInfo = component.GetItemInfo();
      parameter.itemStaticInfo = itemInfo;
      parameter.btnText = Utils.XLAT("id_uppercase_get_more");
      parameter.needvalue = Convert.ToInt32(component.GetNeeded());
      parameter.contentText = itemInfo.LocDescription;
      parameter.titleText = Utils.XLAT("id_uppercase_insufficient_items");
      UIManager.inst.OpenPopup("UseItemCannotGoldPopup", (Popup.PopupParameter) parameter);
    }
  }

  private void OnBuyCallBack(bool success, object data)
  {
    if (!success)
      return;
    this.Init();
  }

  public void OnGotoBtnPressed()
  {
    if (this.gotobtnDelegate == null)
      return;
    this.gotobtnDelegate();
  }

  public void OnSpeedUpBtnPressed()
  {
    if (CityManager.inst.GetBuildingsInFlux() >= 1)
    {
      CityManager.BuildingItem queueFluxBuilding = CityManager.inst.FindFreeBuildQueueFluxBuilding();
      GoldConsumePopup.Parameter parameter = new GoldConsumePopup.Parameter();
      Hashtable ht = new Hashtable();
      int num1 = JobManager.Instance.GetUnfinishedJob(queueFluxBuilding.mBuildingJobID).LeftTime();
      int freeSpeedUpTime = CityManager.inst.GetFreeSpeedUpTime();
      int num2 = num1 - freeSpeedUpTime;
      if (num2 <= 0)
      {
        CityTimerbar ctb = CitadelSystem.inst.GetBuildControllerFromBuildingItem(queueFluxBuilding).transform.GetComponentInChildren<CityTimerbar>();
        if (!((UnityEngine.Object) null != (UnityEngine.Object) ctb))
          return;
        Utils.ExecuteInSecs(0.01f, (System.Action) (() => ctb.OnFreeBtnPressed()));
      }
      else
      {
        ht.Add((object) "time", (object) num2);
        int cost = ItemBag.CalculateCost(ht);
        parameter.confirmButtonClickEvent = new System.Action(this.ConFirmCallBack);
        parameter.lefttime = num1;
        parameter.goldNum = cost;
        parameter.freetime = freeSpeedUpTime;
        parameter.description = ScriptLocalization.GetWithPara("confirm_gold_spend_cooldown_instant_desc", new Dictionary<string, string>()
        {
          {
            "num",
            cost.ToString()
          }
        }, true);
        UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) parameter);
      }
    }
    else
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(ConfigManager.inst.DB_Shop.GetShopData("shopitem_build_queue_middle").Item_InternalId);
      BuildQueueManager.Instance.GetMoreQueueTime(new BuildQueuePopup.Parameter()
      {
        showLimit = true,
        needTimeString = Utils.FormatTime((int) this.speedTime, false, false, true),
        remainTimeString = Utils.FormatTime(BuildQueueManager.Instance.GetRemainQueueTime(), false, false, true),
        buyCount = Mathf.Max(1, Mathf.CeilToInt((float) ((int) this.speedTime - BuildQueueManager.Instance.GetRemainQueueTime()) / itemStaticInfo.Value))
      });
    }
  }

  private void ConFirmCallBack()
  {
    CityManager.BuildingItem queueFluxBuilding = CityManager.inst.FindFreeBuildQueueFluxBuilding();
    if (queueFluxBuilding != null)
    {
      BuildingController fromBuildingItem = CitadelSystem.inst.GetBuildControllerFromBuildingItem(queueFluxBuilding);
      JobHandle unfinishedJob = JobManager.Instance.GetUnfinishedJob(queueFluxBuilding.mBuildingJobID);
      if (unfinishedJob != null)
      {
        Hashtable ht = new Hashtable();
        int num1 = unfinishedJob.LeftTime() - CityManager.inst.GetFreeSpeedUpTime();
        ht.Add((object) "time", (object) num1);
        int cost = ItemBag.CalculateCost(ht);
        Hashtable postData = new Hashtable();
        long num2 = unfinishedJob.ServerJobID;
        if (num2 <= 0L)
          num2 = queueFluxBuilding.mBuildingJobID;
        postData.Add((object) "job_id", (object) num2);
        postData.Add((object) "gold", (object) cost);
        RequestManager.inst.SendRequest("City:speedUpByGold", postData, (System.Action<bool, object>) ((ret, data) =>
        {
          if (ret)
            ;
        }), true);
      }
      else
      {
        CityTimerbar componentInChildren = fromBuildingItem.transform.GetComponentInChildren<CityTimerbar>();
        if (!((UnityEngine.Object) null != (UnityEngine.Object) componentInChildren))
          return;
        componentInChildren.OnFreeBtnPressed();
      }
    }
    else
      D.warn((object) "No construction happening.");
  }

  private string GetQueueRequireLabelString()
  {
    if (CityManager.inst.GetBuildingsInFlux() < 1)
      return Utils.XLAT("id_uppercase_get_more");
    if (JobManager.Instance.GetJob(CityManager.inst.FindFreeBuildQueueFluxBuilding().mBuildingJobID).LeftTime() - CityManager.inst.GetFreeSpeedUpTime() > 0)
      return ScriptLocalization.Get("id_uppercase_speedup", true);
    return Utils.XLAT("id_uppercase_free");
  }

  public void OnQueueSpeedUpConfirmed()
  {
    this.OnQueueSpeedUpClosed();
    CityManager.BuildingItem shortestFluxBuilding = CityManager.inst.FindShortestFluxBuilding();
    if (shortestFluxBuilding != null)
    {
      CityTimerbar componentInChildren = CitadelSystem.inst.GetBuildControllerFromBuildingItem(shortestFluxBuilding).transform.GetComponentInChildren<CityTimerbar>();
      if (!((UnityEngine.Object) null != (UnityEngine.Object) componentInChildren))
        return;
      componentInChildren.OnSpeedUpBtnPressed();
    }
    else
      D.warn((object) "No construction happening.");
  }

  public void OnQueueSpeedUpClosed()
  {
    this.mQueueSpeedupPopup.SetActive(false);
  }

  public void OnBuildPressed()
  {
    if (this.itemLackCannotBuy != null)
      UIManager.inst.OpenPopup("UseItemCannotGoldPopup", (Popup.PopupParameter) new UseItemCannotGoldPopup.Parameter()
      {
        itemStaticInfo = this.itemLackCannotBuy,
        btnText = Utils.XLAT("id_uppercase_get_more"),
        needvalue = this.itemLackNeedValue,
        contentText = this.itemLackCannotBuy.LocDescription,
        titleText = Utils.XLAT("id_uppercase_insufficient_items")
      });
    else if (BuildQueueManager.Instance.IsBuildQueueFull(this.speedTime))
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(ConfigManager.inst.DB_Shop.GetShopData("shopitem_build_queue_middle").Item_InternalId);
      BuildQueueManager.Instance.GetMoreQueueTime(new BuildQueuePopup.Parameter()
      {
        showLimit = true,
        needTimeString = Utils.FormatTime((int) this.speedTime, false, false, true),
        remainTimeString = Utils.FormatTime(BuildQueueManager.Instance.GetRemainQueueTime(), false, false, true),
        buyCount = Mathf.Max(1, Mathf.CeilToInt((float) ((int) this.speedTime - BuildQueueManager.Instance.GetRemainQueueTime()) / itemStaticInfo.Value))
      });
    }
    else if (this.bshowPopup)
      this.resPopUp = UIManager.inst.OpenPopup("BuildingResPopUp", (Popup.PopupParameter) new BuildingResPopUp.Parameter()
      {
        lackItem = this.m_lackitem,
        lackRes = this.m_lackRes,
        action = this.mBuildLabel.text,
        requireRes = this.requireHT,
        gold = this.gold,
        buyResourCallBack = new System.Action<int>(this.NormalBuild),
        title = ScriptLocalization.Get("construction_build_notenoughresource", true),
        content = ScriptLocalization.Get("construction_build_notenoughresource_description", true)
      });
    else
      this.NormalBuild(0);
  }

  private void NormalBuild(int gold = 0)
  {
    if (gold > PlayerData.inst.hostPlayer.Currency)
    {
      Utils.ShowNotEnoughGoldTip();
    }
    else
    {
      if (this._buildingItem != null)
      {
        this.OnCloseBtnPressed();
        CityManager.inst.UpgradeBuilding(this._buildingItem.mID, false, this.speedTime, gold);
        Utils.FindClass<CitadelSystem>().OnCityMapClicked();
      }
      else
      {
        this.OnCloseBtnPressed();
        CityManager.inst.CreateBuilding(this._buildingid, this._targetPlot.GetComponent<CityPlotController>().SlotId, 0, false, this.speedTime, gold);
        Utils.FindClass<CitadelSystem>().OnCityMapClicked();
      }
      AudioManager.Instance.PlaySound("sfx_building_begin", false);
    }
  }

  public void OnInstBuildPressed()
  {
    string withPara = ScriptLocalization.GetWithPara("confirm_gold_spend_upgrade_instant_desc", new Dictionary<string, string>()
    {
      {
        "num",
        this.instBuildAmount.ToString()
      }
    }, true);
    if (this.itemLackCannotBuy != null)
      UIManager.inst.OpenPopup("UseItemCannotGoldPopup", (Popup.PopupParameter) new UseItemCannotGoldPopup.Parameter()
      {
        itemStaticInfo = this.itemLackCannotBuy,
        btnText = Utils.XLAT("id_uppercase_get_more"),
        needvalue = this.itemLackNeedValue,
        contentText = this.itemLackCannotBuy.LocDescription,
        titleText = Utils.XLAT("id_uppercase_insufficient_items")
      });
    else
      UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
      {
        confirmButtonClickEvent = (System.Action) (() =>
        {
          if (this._buildingItem != null)
          {
            this.OnCloseBtnPressed();
            CityManager.inst.UpgradeBuilding(this._buildingItem.mID, true, 0.0, 0);
            Utils.FindClass<CitadelSystem>().OnCityMapClicked();
          }
          else
          {
            this.OnCloseBtnPressed();
            CityManager.inst.CreateBuilding(this._buildingid, this._targetPlot.GetComponent<CityPlotController>().SlotId, 0, true, 0.0, 0);
            Utils.FindClass<CitadelSystem>().OnCityMapClicked();
          }
        }),
        description = withPara,
        goldNum = this.instBuildAmount
      });
  }

  public void InstantSpendGold()
  {
    if (GameEngine.Instance.PlayerData.hostPlayer.Currency >= this._instBuildAmount)
    {
      if (this._buildingItem != null)
      {
        this.OnCloseBtnPressed();
        CityManager.inst.UpgradeBuilding(this._buildingItem.mID, true, 0.0, 0);
        Utils.FindClass<CitadelSystem>().OnCityMapClicked();
      }
      else
      {
        this.OnCloseBtnPressed();
        CityManager.inst.CreateBuilding(this._buildingid, this._targetPlot.GetComponent<CityPlotController>().SlotId, 0, true, 0.0, 0);
        Utils.FindClass<CitadelSystem>().OnCityMapClicked();
      }
    }
    else
      GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.GOLD);
  }

  private void Init()
  {
    if ((UnityEngine.Object) null != (UnityEngine.Object) this._targetPlot)
    {
      if ((UnityEngine.Object) null != (UnityEngine.Object) this._targetPlot.GetComponentInParent<SuperPlotController>())
      {
        this.dynaBG.width = 1712;
        this.dynaBG.alpha = 1f;
      }
      else
      {
        this.dynaBG.width = 3136;
        this.dynaBG.alpha = 0.572549f;
      }
    }
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData(this._buildingid, this._level);
    if (data == null)
      return;
    this.sv.ResetPosition();
    BuildingInfo buildingInfo = (BuildingInfo) null;
    string iconStr = data.Building_ImagePath + "_l" + CityManager.inst.GetIconLevelFromBuildingLevel(this._level + 1, (string) null).ToString();
    if (this._level > 1)
    {
      this.mBuildLabel.text = Utils.XLAT("id_uppercase_upgrade");
      this.mInstBuildLabel.text = Utils.XLAT("id_uppercase_instant_upgrade");
      buildingInfo = ConfigManager.inst.DB_Building.GetData(this._buildingid, this._level - 1);
      if (buildingInfo == null || BuildingController.mSelectedBuildingItem == null || (UnityEngine.Object) null == (UnityEngine.Object) BuildingController.mSelectedBuildingController)
        return;
      if ("stronghold" == BuildingController.mSelectedBuildingItem.mType)
        CitadelSystem.inst.FocusConstructionBuilding(BuildingController.mSelectedBuildingController.gameObject, (UIWidget) this.mBuildingIcon, this.strongholdZoomFocusDis);
      else
        CitadelSystem.inst.FocusConstructionBuilding(BuildingController.mSelectedBuildingController.gameObject, (UIWidget) this.mBuildingIcon, this.zoomFocusDis);
      this.mBuildingIcon.gameObject.SetActive(false);
    }
    else
    {
      this.mBuildLabel.text = Utils.XLAT("id_uppercase_build");
      this.mInstBuildLabel.text = Utils.XLAT("id_uppercase_instant_build");
      this.mBuildingIcon.gameObject.SetActive(false);
    }
    this.m_lackRes.Clear();
    this.m_lackitem.Clear();
    this.requireHT.Clear();
    int buildingsInFlux = CityManager.inst.GetBuildingsInFlux();
    Hashtable hashtable1 = new Hashtable();
    hashtable1[(object) ItemBag.ItemType.food] = (object) data.BenefitFood;
    hashtable1[(object) ItemBag.ItemType.wood] = (object) data.BenefitWood;
    hashtable1[(object) ItemBag.ItemType.silver] = (object) data.BenefitSilver;
    hashtable1[(object) ItemBag.ItemType.ore] = (object) data.BenefitOre;
    Hashtable hashtable2 = new Hashtable();
    hashtable2[(object) data.Requirement_ID_1] = (object) data.Requirement_Value_1;
    hashtable2[(object) data.Requirement_ID_2] = (object) data.Requirement_Value_2;
    hashtable2[(object) data.Requirement_ID_3] = (object) data.Requirement_Value_3;
    hashtable2[(object) data.Requirement_ID_4] = (object) data.Requirement_Value_4;
    hashtable2[(object) data.Requirement_ID_5] = (object) data.Requirement_Value_5;
    double buildTime = data.Build_Time;
    this.speedTime = (double) ConfigManager.inst.DB_BenefitCalc.GetFinalData((int) buildTime, "calc_construction_time");
    this.requireHT[(object) "time"] = (object) this.speedTime;
    this.requireHT[(object) "resources"] = (object) hashtable1;
    this.requireHT[(object) "items"] = (object) hashtable2;
    this.instBuildAmount = ItemBag.CalculateCost(this.requireHT);
    this.requireHT.Remove((object) "time");
    this.gold = ItemBag.CalculateCost(this.requireHT);
    this.SetDetails(BuildingController.mSelectedBuildingItem, iconStr, this._level, buildTime, this.speedTime, this.instBuildAmount, data.Building_LOC_ID, this._targetPlot, data.Building_Lvl, this._buildingid, (System.Action) null);
    int num = BuildQueueManager.Instance.AllowedBuildQueue(0.0);
    bool flag1 = true;
    bool flag2 = true;
    this.bshowPopup = false;
    GameObject gameObject = (GameObject) null;
    bool bResourceEmpty = false;
    if (buildingsInFlux >= num)
    {
      gameObject = this.AddRequirement(BuildingConstruction.Requirement.QUEUE, "Texture/BuildingConstruction/item_speed_ups", (string) null, (double) buildingsInFlux, (double) num, new EventDelegate(new EventDelegate.Callback(this.OnSpeedUpBtnPressed)), 0.0);
      this.SetQueueFull();
    }
    if (buildingsInFlux >= BuildQueueManager.Instance.PossibleMaxQueue)
      flag1 = false;
    if (!this.BuildItemRequirement(data.Requirement_ID_1, data.Requirement_Value_1, ref bResourceEmpty))
    {
      flag2 = false;
      flag1 = false;
    }
    if (!this.BuildItemRequirement(data.Requirement_ID_2, data.Requirement_Value_2, ref bResourceEmpty))
    {
      flag2 = false;
      flag1 = false;
    }
    if (!this.BuildItemRequirement(data.Requirement_ID_3, data.Requirement_Value_3, ref bResourceEmpty))
    {
      flag2 = false;
      flag1 = false;
    }
    this.BuildItemRequirement(data.Requirement_ID_4, data.Requirement_Value_4, ref bResourceEmpty);
    this.BuildItemRequirement(data.Requirement_ID_5, data.Requirement_Value_5, ref bResourceEmpty);
    CityData byUid = DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid);
    if (data.Food > 0.0)
    {
      string str = "Texture/BuildingConstruction/food_icon";
      double currentResource = byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD);
      gameObject = this.AddRequirement(BuildingConstruction.Requirement.FOOD, str, (string) null, currentResource, (double) data.BenefitFood, new EventDelegate(new EventDelegate.Callback(this.OnGetMoreFoodBtnPressed)), data.Food);
      if (currentResource < (double) data.BenefitFood)
      {
        this.bshowPopup = true;
        this.m_lackRes.Add(str, (long) Convert.ToInt32((double) data.BenefitFood - currentResource));
      }
    }
    if (data.Wood > 0.0)
    {
      string str = "Texture/BuildingConstruction/wood_icon";
      double currentResource = byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD);
      gameObject = this.AddRequirement(BuildingConstruction.Requirement.WOOD, str, (string) null, currentResource, (double) data.BenefitWood, new EventDelegate(new EventDelegate.Callback(this.OnGetMoreWoodBtnPressed)), data.Wood);
      if (currentResource < (double) data.BenefitWood)
      {
        this.bshowPopup = true;
        this.m_lackRes.Add(str, (long) Convert.ToInt32((double) data.BenefitWood - currentResource));
      }
    }
    if (data.Silver > 0.0)
    {
      string str = "Texture/BuildingConstruction/silver_icon";
      double currentResource = byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
      gameObject = this.AddRequirement(BuildingConstruction.Requirement.SILVER, str, (string) null, currentResource, (double) data.BenefitSilver, new EventDelegate(new EventDelegate.Callback(this.OnGetMoreSilverBtnPressed)), data.Silver);
      if (currentResource < (double) data.BenefitSilver)
      {
        this.bshowPopup = true;
        this.m_lackRes.Add(str, (long) Convert.ToInt32((double) data.BenefitSilver - currentResource));
      }
    }
    if (data.Ore > 0.0)
    {
      string str = "Texture/BuildingConstruction/ore_icon";
      double currentResource = byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE);
      gameObject = this.AddRequirement(BuildingConstruction.Requirement.ORE, str, (string) null, currentResource, (double) data.BenefitOre, new EventDelegate(new EventDelegate.Callback(this.OnGetMoreOreBtnPressed)), data.Ore);
      if (currentResource < (double) data.BenefitOre)
      {
        this.bshowPopup = true;
        this.m_lackRes.Add(str, (long) Convert.ToInt32((double) data.BenefitOre - currentResource));
      }
    }
    this.mBuildBtn.isEnabled = flag1;
    this.mInstBuidBtn.isEnabled = flag2;
    this.AdjustHeight(ref this.mRewardHeader);
    gameObject = this.AddRewards(BuildingConstruction.Rewards.HERO_XP, (string) null, (string) null, (double) data.Hero_XP, this._level <= 1 ? 0.0 : (double) buildingInfo.Hero_XP);
    GameObject rewardObject = this.AddRewards(BuildingConstruction.Rewards.POWER, (string) null, (string) null, (double) data.Power, this._level <= 1 ? 0.0 : (double) buildingInfo.Power);
    this._requirementsY = this.mRewardDefault.transform.localPosition.y + (float) (this.mRewardDefault.GetComponent<UIWidget>().height / 2);
    bool isChanged = false;
    if (data.Benefit_ID_1.Length > 0 && data.Benefit_Display_1)
    {
      double benefitValue1 = data.Benefit_Value_1;
      double before = this._level <= 1 ? 0.0 : buildingInfo.Benefit_Value_1;
      isChanged |= benefitValue1 != before;
      rewardObject = this.AddRewards(BuildingConstruction.Rewards.DEFAULT, (string) null, data.Benefit_ID_1, benefitValue1, before);
      this.consructionInfo.FeedContent(data, rewardObject, isChanged);
    }
    if (data.Benefit_ID_2.Length > 0 && data.Benefit_Display_2)
    {
      double benefitValue2 = data.Benefit_Value_2;
      double before = this._level <= 1 ? 0.0 : buildingInfo.Benefit_Value_2;
      isChanged |= benefitValue2 != before;
      rewardObject = this.AddRewards(BuildingConstruction.Rewards.DEFAULT, (string) null, data.Benefit_ID_2, benefitValue2, before);
      this.consructionInfo.FeedContent(data, rewardObject, isChanged);
    }
    if (data.Benefit_ID_3.Length > 0 && data.Benefit_Display_3)
    {
      double benefitValue3 = data.Benefit_Value_3;
      double before = this._level <= 1 ? 0.0 : buildingInfo.Benefit_Value_3;
      isChanged |= benefitValue3 != before;
      rewardObject = this.AddRewards(BuildingConstruction.Rewards.DEFAULT, (string) null, data.Benefit_ID_3, benefitValue3, before);
      this.consructionInfo.FeedContent(data, rewardObject, isChanged);
    }
    if (data.Benefit_ID_4.Length > 0 && data.Benefit_Display_4)
    {
      double benefitValue4 = data.Benefit_Value_4;
      double before = this._level <= 1 ? 0.0 : buildingInfo.Benefit_Value_4;
      isChanged |= benefitValue4 != before;
      rewardObject = this.AddRewards(BuildingConstruction.Rewards.DEFAULT, (string) null, data.Benefit_ID_4, benefitValue4, before);
      this.consructionInfo.FeedContent(data, rewardObject, isChanged);
    }
    this.consructionInfo.FeedContent(data, rewardObject, isChanged);
  }

  private bool BuildItemRequirement(string idStr, int reqValue, ref bool bResourceEmpty)
  {
    bool flag = true;
    if (reqValue > 0)
    {
      int result = -1;
      int.TryParse(idStr, out result);
      if (ConfigManager.inst.DB_Shop.GetShopDataByInternalId(result) != null)
      {
        ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(result);
        ConfigManager.inst.DB_Items.GetItem(dataByInternalId.Item_InternalId);
        int itemCount = ItemBag.Instance.GetItemCount(dataByInternalId.Item_InternalId);
        int num = reqValue;
        string str = string.Format("Texture/ItemIcons/{0}", (object) dataByInternalId.Image);
        this.AddRequirement(BuildingConstruction.Requirement.ITEM, str, idStr, (double) itemCount, (double) num, new EventDelegate(new EventDelegate.Callback(this.GetMoreOfItemPressed)), 0.0);
        if (itemCount < num)
        {
          this.bshowPopup = true;
          this.m_lackRes.Add(str, (long) (num - itemCount));
        }
      }
      else if (ConfigManager.inst.DB_Items.GetItem(result) != null)
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(result);
        int itemCount = ItemBag.Instance.GetItemCount(itemStaticInfo.internalId);
        int num = reqValue;
        string str = string.Format("Texture/ItemIcons/{0}", (object) itemStaticInfo.Image);
        this.AddRequirement(BuildingConstruction.Requirement.ITEM, str, idStr, (double) itemCount, (double) num, new EventDelegate(new EventDelegate.Callback(this.GetMoreOfItemPressed)), 0.0);
        if (itemCount < num)
        {
          if (this.itemLackCannotBuy != null)
            D.warn((object) "more than one item cannot buy lack");
          this.itemLackCannotBuy = itemStaticInfo;
          this.itemLackNeedValue = reqValue;
          this.bshowPopup = true;
          this.m_lackRes.Add(str, (long) (num - itemCount));
        }
      }
      else
      {
        string buildingType = string.Empty;
        if (idStr != string.Empty && ConfigManager.inst.DB_Building.GetData(int.Parse(idStr)) != null)
          buildingType = ConfigManager.inst.DB_Building.GetData(int.Parse(idStr)).Type;
        BuildingInfo det1 = ConfigManager.inst.DB_Building.GetData(buildingType + "_" + reqValue.ToString());
        int buildingLevelFor = CityManager.inst.GetHighestBuildingLevelFor(buildingType);
        int num = reqValue;
        GameObject gameObject = (GameObject) null;
        string icon = "Prefab/UI/BuildingPrefab/ui_" + det1.Building_ImagePath + "_l1";
        gameObject = buildingLevelFor == -1 ? this.AddRequirement(BuildingConstruction.Requirement.BUILDING, icon, Utils.XLAT(det1.Building_LOC_ID), (double) buildingLevelFor, (double) num, new EventDelegate((EventDelegate.Callback) (() =>
        {
          UIManager.inst.cityCamera.UseBoundary = true;
          this.OnCloseBtnPressed();
          string str = JsonWriter.Serialize((object) new LPFocusPlot()
          {
            zone = (!("rural" == det1.City_Zone) ? "2" : "1"),
            buildingType = det1.Type
          });
          LinkerHub.Instance.Distribute(JsonWriter.Serialize((object) new LinkerPara()
          {
            classname = "LPFocusPlot",
            content = str
          }));
        })), 0.0) : this.AddRequirement(BuildingConstruction.Requirement.BUILDING, icon, Utils.XLAT(det1.Building_LOC_ID), (double) buildingLevelFor, (double) num, new EventDelegate((EventDelegate.Callback) (() =>
        {
          UIManager.inst.cityCamera.UseBoundary = true;
          this.OnCloseBtnPressed();
          string str = JsonWriter.Serialize((object) new LPFocusBuilding()
          {
            buildingType = det1.Building_LOC_ID.Replace("_name", string.Empty)
          });
          LinkerHub.Instance.Distribute(JsonWriter.Serialize((object) new LinkerPara()
          {
            classname = "LPFocusBuilding",
            content = str
          }));
        })), 0.0);
        if (buildingLevelFor < num)
          flag = false;
      }
    }
    return flag;
  }

  private void GetWillUnlockedHero()
  {
    List<LegendInfo> legends = ConfigManager.inst.DB_Legend.Legends;
    for (int index = 0; index < legends.Count; ++index)
    {
      LegendInfo legendInfo = legends[index];
      if (ConfigManager.inst.DB_Building.GetData(legendInfo.require.RequireBuildings()[0].internalID).Building_Lvl == this._level)
        this.unlockLegends.Add(legendInfo);
    }
  }

  private void OnJobCreate(long jobID)
  {
    if (JobManager.Instance.GetJob(jobID).GetJobEvent() != JobEvent.JOB_ONE_MORE_BUILD_QUEUE)
      return;
    this.Init();
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    BuildingConstruction.Parameter parameter = orgParam as BuildingConstruction.Parameter;
    this._buildingid = parameter.buildingid;
    this._level = parameter.tarLevel;
    this._targetPlot = parameter.targetPlot;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DBManager.inst.DB_CityMap.onDataChanged += new System.Action<CityMapData>(this.OnCityDataChange);
    CityManager.inst.onResourcesUpdated += new CityManager.OnResourcesUpdated(this.OnResourcesUpdated);
    JobManager.Instance.OnJobCreated += new System.Action<long>(this.OnJobCreate);
    JobManager.Instance.OnJobRemove += new System.Action<long>(this.OnJobRemove);
    DBManager.inst.DB_Local_Benefit.onDataChanged += new System.Action<string>(this.OnBenefitChange);
    if ((UnityEngine.Object) null != (UnityEngine.Object) this._targetPlot)
      CitadelSystem.inst.CreateBuildingPreview(this._buildingid, this._targetPlot.GetComponent<CityPlotController>().SlotId);
    this.Init();
  }

  private void OnBenefitChange(string key)
  {
    this.Init();
    if (!((UnityEngine.Object) this.resPopUp != (UnityEngine.Object) null))
      return;
    this.resPopUp.Close((UIControler.UIParameter) null);
    this.resPopUp = UIManager.inst.OpenPopup("BuildingResPopUp", (Popup.PopupParameter) new BuildingResPopUp.Parameter()
    {
      lackItem = this.m_lackitem,
      lackRes = this.m_lackRes,
      action = this.mBuildLabel.text,
      requireRes = this.requireHT,
      gold = this.gold,
      buyResourCallBack = new System.Action<int>(this.NormalBuild),
      title = ScriptLocalization.Get("construction_build_notenoughresource", true),
      content = ScriptLocalization.Get("construction_build_notenoughresource_description", true)
    });
  }

  private void OnResourcesUpdated(long food, long wood, long silver, long ore, long gold)
  {
  }

  private void OnCityDataChange(CityMapData obj)
  {
    this.Init();
  }

  private void OnJobRemove(long jobID)
  {
    Utils.ExecuteAtTheEndOfFrame(new System.Action(this.Init));
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    CitadelSystem.inst.DeleteBuildingPreview();
    DBManager.inst.DB_CityMap.onDataChanged -= new System.Action<CityMapData>(this.OnCityDataChange);
    CityManager.inst.onResourcesUpdated -= new CityManager.OnResourcesUpdated(this.OnResourcesUpdated);
    JobManager.Instance.OnJobCreated -= new System.Action<long>(this.OnJobCreate);
    JobManager.Instance.OnJobRemove -= new System.Action<long>(this.OnJobRemove);
    DBManager.inst.DB_Local_Benefit.onDataChanged -= new System.Action<string>(this.OnBenefitChange);
  }

  public enum Requirement
  {
    NONE,
    FOOD,
    WOOD,
    SILVER,
    ORE,
    BUILDING,
    ITEM,
    QUEUE,
    LIMIT,
  }

  public enum Rewards
  {
    NONE,
    HERO_XP,
    POWER,
    DEFAULT,
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public string buildingid;
    public int tarLevel;
    public GameObject targetPlot;
  }
}
