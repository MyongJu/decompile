﻿// Decompiled with JetBrains decompiler
// Type: AllianceChangeTagHeader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Diagnostics;
using System.Text;
using UI;
using UnityEngine;

public class AllianceChangeTagHeader : AllianceCustomizeHeader
{
  [SerializeField]
  private UIInput m_TagInput;
  [SerializeField]
  private UISprite m_TagStatus;
  [SerializeField]
  private SpriteAnimation m_TagBusyIcon;
  [SerializeField]
  private UILabel m_TagTip;
  [SerializeField]
  private UIButton m_TagChangeFree;
  [SerializeField]
  private UIButton m_TagChangeGold;
  [SerializeField]
  private UILabel m_TagChangeGoldPrice;
  private IEnumerator m_TagCoroutine;
  private bool m_TagOkay;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    this.m_TagInput.onValidate = new UIInput.OnValidate(this.TagInputValidator);
    this.m_TagInput.value = this.allianceData.allianceAcronym;
    this.ResetTagTip();
    this.m_TagOkay = false;
    this.m_TagStatus.spriteName = string.Empty;
    this.UpdateButtons();
  }

  private char TagInputValidator(string text, int charIndex, char addedChar)
  {
    if ((int) addedChar != 32)
      return addedChar;
    return char.MinValue;
  }

  public void OnChangeName()
  {
    if (IllegalWordsUtils.WarningIllegalWords(this.m_TagInput.value))
      return;
    AllianceManager.Instance.ChangeAllianceTag(this.m_TagInput.value, new System.Action<bool, object>(this.SetupAllianceCallback));
  }

  private void SetupAllianceCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.m_TagOkay = false;
    UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_tag_changed"), (System.Action) null, 4f, false);
  }

  public void OnTagInputChanged()
  {
    this.m_TagBusyIcon.gameObject.SetActive(false);
    this.m_TagStatus.spriteName = string.Empty;
    this.m_TagOkay = false;
    if (this.m_TagCoroutine != null)
    {
      this.StopCoroutine(this.m_TagCoroutine);
      this.m_TagCoroutine = (IEnumerator) null;
    }
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
    if (Encoding.UTF8.GetBytes(this.m_TagInput.value).Length != 3)
    {
      this.m_TagStatus.spriteName = "red_cross";
      this.SetTagError(Utils.XLAT("alliance_settings_change_tag_3_characters_condition"));
    }
    else if (this.m_TagInput.value == allianceData.allianceAcronym)
    {
      this.m_TagStatus.spriteName = "red_cross";
      this.SetTagError(Utils.XLAT("alliance_settings_change_tag_must_be_different_condition"));
    }
    else
      this.StartCoroutine(this.m_TagCoroutine = this.WaitAndCheckTag(this.m_TagInput.value));
  }

  [DebuggerHidden]
  private IEnumerator WaitAndCheckTag(string newName)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AllianceChangeTagHeader.\u003CWaitAndCheckTag\u003Ec__Iterator27()
    {
      newName = newName,
      \u003C\u0024\u003EnewName = newName,
      \u003C\u003Ef__this = this
    };
  }

  private void CheckTAGCallback(bool ret, object data)
  {
    try
    {
      this.m_TagBusyIcon.gameObject.SetActive(false);
      if (ret)
      {
        this.m_TagOkay = true;
        this.m_TagStatus.spriteName = "green_tick";
        this.ResetTagTip();
      }
      else
      {
        this.m_TagOkay = false;
        this.m_TagStatus.spriteName = "red_cross";
        this.SetTagError((data as Hashtable)[(object) "errmsg"] as string);
      }
    }
    catch
    {
    }
  }

  private void ResetTagTip()
  {
    this.m_TagTip.text = Utils.XLAT("alliance_settings_change_nickname_conditions");
    this.m_TagTip.color = new Color(0.6941177f, 0.6941177f, 0.6941177f, 1f);
  }

  private void SetTagError(string errmsg)
  {
    this.m_TagTip.text = errmsg;
    this.m_TagTip.color = Color.red;
  }

  private void UpdateButtons()
  {
    AllianceInfoStatsData allianceInfoStatsData = DBManager.inst.DB_AllianceStats.Get(PlayerData.inst.allianceId, "edit_alliance_acronym");
    int num = allianceInfoStatsData != null ? (int) allianceInfoStatsData.value : 0;
    bool flag = num == 0;
    this.m_TagChangeFree.gameObject.SetActive(flag);
    this.m_TagChangeGold.gameObject.SetActive(!flag);
    this.m_TagChangeFree.isEnabled = this.m_TagOkay;
    this.m_TagChangeGold.isEnabled = this.m_TagOkay;
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("alliance_tag_rename_price_base");
    GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("alliance_tag_rename_price_addition");
    GameConfigInfo data3 = ConfigManager.inst.DB_GameConfig.GetData("alliance_tag_rename_price_max");
    int valueInt1 = data1.ValueInt;
    int valueInt2 = data2.ValueInt;
    int valueInt3 = data3.ValueInt;
    this.m_TagChangeGoldPrice.text = Utils.FormatThousands(Math.Min(valueInt1 + (num - 1) * valueInt2, valueInt3).ToString());
  }

  private void Update()
  {
    this.UpdateButtons();
  }
}
