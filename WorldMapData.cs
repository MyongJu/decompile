﻿// Decompiled with JetBrains decompiler
// Type: WorldMapData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldMapData
{
  private List<WonderData> wonderlist = new List<WonderData>();
  private List<Vector2> wonderPosList = new List<Vector2>();
  public Dictionary<Vector2, Vector2> dirMap = new Dictionary<Vector2, Vector2>();
  private static WorldMapData m_Instance;
  public System.Action onDataInitFinished;

  private WorldMapData()
  {
  }

  public List<WonderData> Wonderlist
  {
    get
    {
      return this.wonderlist;
    }
  }

  public List<Vector2> WonderPosList
  {
    get
    {
      return this.wonderPosList;
    }
  }

  public static WorldMapData Instance
  {
    get
    {
      if (WorldMapData.m_Instance == null)
        WorldMapData.m_Instance = new WorldMapData();
      return WorldMapData.m_Instance;
    }
  }

  public void Initialize()
  {
    MessageHub.inst.GetPortByAction("Map:loadWorldMap").SendRequest((Hashtable) null, new System.Action<bool, object>(this.ResolveHash), true);
  }

  private void ResolveHash(bool ret, object ht)
  {
    ArrayList arrayList = ht as ArrayList;
    this.wonderlist.Clear();
    for (int index = 0; index < arrayList.Count; ++index)
    {
      WonderData wonderData = new WonderData();
      Hashtable inData = arrayList[index] as Hashtable;
      DatabaseTools.UpdateData(inData, "k", ref wonderData.kid);
      DatabaseTools.UpdateData(inData, "king_username", ref wonderData.kingName);
      DatabaseTools.UpdateData(inData, "language_group", ref wonderData.languageGroup);
      DatabaseTools.UpdateData(inData, "portrait", ref wonderData.wonderIcon);
      DatabaseTools.UpdateData(inData, "hot", ref wonderData.ishot);
      DatabaseTools.UpdateData(inData, "name", ref wonderData.wonderName);
      DatabaseTools.UpdateData(inData, "flag", ref wonderData.wonderFlag);
      DatabaseTools.UpdateData(inData, "colonized_by", ref wonderData.kingdomColonizedBy);
      if (PVPMapData.MapData.IsKingdomActive(wonderData.kid))
        this.wonderlist.Add(wonderData);
    }
    this.GenerateWonderPosition();
  }

  public void GenerateWonderPosition()
  {
    Vector2 key1 = new Vector2(-1f, 0.0f);
    Vector2 key2 = new Vector2(0.0f, 1f);
    Vector2 key3 = new Vector2(1f, 0.0f);
    Vector2 key4 = new Vector2(0.0f, -1f);
    this.dirMap.Clear();
    this.dirMap.Add(key1, key2);
    this.dirMap.Add(key2, key3);
    this.dirMap.Add(key3, key4);
    this.dirMap.Add(key4, key1);
    Vector2 zero = Vector2.zero;
    Vector2 vector2_1 = key3;
    Vector2 vector2_2 = zero;
    Vector2 index1 = vector2_1;
    this.wonderPosList.Clear();
    this.wonderPosList.Add(zero);
    for (int index2 = 1; index2 < this.wonderlist.Count; ++index2)
    {
      Vector2 vector2_3 = vector2_2 + index1;
      if (this.wonderPosList.Contains(vector2_3))
        vector2_3 += key3;
      this.wonderPosList.Add(vector2_3);
      vector2_2 = vector2_3;
      Vector2 dir = this.dirMap[index1];
      Vector2 vector2_4 = vector2_3 + dir;
      if (vector2_4 != Vector2.zero && !this.wonderPosList.Contains(vector2_4))
        index1 = dir;
    }
    if (this.onDataInitFinished == null)
      return;
    this.onDataInitFinished();
  }

  public void Dispose()
  {
    Resources.UnloadUnusedAssets();
  }
}
