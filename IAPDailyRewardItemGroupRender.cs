﻿// Decompiled with JetBrains decompiler
// Type: IAPDailyRewardItemGroupRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class IAPDailyRewardItemGroupRender : MonoBehaviour
{
  private List<IAPDailyRewardItemRender> itemList = new List<IAPDailyRewardItemRender>();
  public IAPDailyRewardItemRender[] itemRender;
  private int index;

  public void SeedData(List<Reward.RewardsValuePair> rewardList, UIScrollView scrollView)
  {
    for (int index = 0; index < rewardList.Count; ++index)
    {
      int internalId = rewardList[index].internalID;
      int itemCount = rewardList[index].value;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
      this.itemRender[index].SetData(itemStaticInfo, itemCount);
      this.itemList.Add(this.itemRender[index]);
      this.itemRender[index].gameObject.GetComponentInChildren<UIDragScrollView>().scrollView = scrollView;
      ++this.index;
    }
    for (int index = this.index; index < this.itemRender.Length; ++index)
      NGUITools.SetActive(this.itemRender[index].gameObject, false);
  }

  public void Release()
  {
    for (int index = 0; index < this.itemList.Count; ++index)
      this.itemList[index].Release();
    this.itemList.Clear();
  }
}
