﻿// Decompiled with JetBrains decompiler
// Type: ModHelpPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ModHelpPopup : Popup
{
  private string curLanguageCode = string.Empty;
  public UIButton becomeMod;
  public UIButton modResponsibility;
  public UIButton communicateMod;
  public UIButton languageSel;
  public UIButton closeBtn;
  public UILabel curLanguageName;
  [Header("Localize")]
  public UILabel titleTxt;
  public UILabel becomeModTxt;
  public UILabel modResponTxt;
  public UILabel communicateModTxt;
  public UILabel modHelpTxt;
  private bool inited;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    EventDelegate.Add(this.becomeMod.onClick, new EventDelegate.Callback(this.OnBecomeMod));
    EventDelegate.Add(this.modResponsibility.onClick, new EventDelegate.Callback(this.OnModResponsibility));
    EventDelegate.Add(this.communicateMod.onClick, new EventDelegate.Callback(this.OnCommunicateMod));
    EventDelegate.Add(this.closeBtn.onClick, new EventDelegate.Callback(this.OnCloseBtn));
    EventDelegate.Add(this.languageSel.onClick, new EventDelegate.Callback(this.OnSelectLanguage));
    ModController.OnLanguageSelect -= new System.Action<string>(this.OnLanguageChanged);
    ModController.OnLanguageSelect += new System.Action<string>(this.OnLanguageChanged);
    if (!this.inited)
    {
      this._OnceInit();
      this.inited = true;
    }
    if (this.curLanguageCode != string.Empty)
      this.curLanguageName.text = Language.Instance.GetLanguageName(this.curLanguageCode);
    else
      this.curLanguageName.text = Language.Instance.GetLanguageName(LocalizationManager.CurrentLanguageCode);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    ModController.OnLanguageSelect -= new System.Action<string>(this.OnLanguageChanged);
    EventDelegate.Remove(this.becomeMod.onClick, new EventDelegate.Callback(this.OnBecomeMod));
    EventDelegate.Remove(this.modResponsibility.onClick, new EventDelegate.Callback(this.OnModResponsibility));
    EventDelegate.Remove(this.communicateMod.onClick, new EventDelegate.Callback(this.OnCommunicateMod));
    EventDelegate.Remove(this.closeBtn.onClick, new EventDelegate.Callback(this.OnCloseBtn));
    EventDelegate.Remove(this.languageSel.onClick, new EventDelegate.Callback(this.OnSelectLanguage));
  }

  private void OnLanguageChanged(string languageCode)
  {
    string languageName = Language.Instance.GetLanguageName(languageCode);
    this.curLanguageCode = languageCode;
    this.curLanguageName.text = languageName;
  }

  private void OnCloseBtn()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  private void OnBecomeMod()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  private void OnModResponsibility()
  {
    UIManager.inst.OpenPopup("Mod/ModResponPopup", (Popup.PopupParameter) null);
  }

  private void OnSelectLanguage()
  {
    UIManager.inst.OpenPopup("LanguageSelectPopup", (Popup.PopupParameter) null);
  }

  private void OnCommunicateMod()
  {
    if (PlayerData.inst.moderatorInfo.IsModerator)
    {
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("settings_game_help_mod_contact"),
        Content = Utils.XLAT("toast_settings_mod_not_allowed"),
        Okay = Utils.XLAT("id_uppercase_okay"),
        OkayCallback = (System.Action) null
      });
    }
    else
    {
      Hashtable postData = new Hashtable();
      if (this.curLanguageCode == string.Empty)
        this.curLanguageCode = LocalizationManager.CurrentLanguageCode;
      postData[(object) "lang"] = (object) this.curLanguageCode;
      MessageHub.inst.GetPortByAction("Player:searchModerator").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        long outData = 0;
        List<string> recipients = new List<string>();
        ArrayList arrayList = data as ArrayList;
        for (int index = 0; index < arrayList.Count; ++index)
        {
          Hashtable inData = arrayList[index] as Hashtable;
          string empty = string.Empty;
          DatabaseTools.UpdateData(inData, "mod_name", ref empty);
          DatabaseTools.UpdateData(inData, "uid", ref outData);
          if (empty != null && empty != string.Empty && outData != 0L)
          {
            recipients.Add(empty);
            break;
          }
        }
        if (recipients.Count <= 0)
          return;
        PlayerData.inst.mail.SendModMail(outData, recipients);
        UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      }), true);
    }
  }

  public void _OnceInit()
  {
    this.titleTxt.text = Utils.XLAT("settings_game_help_uppercase_contact_mod");
    this.becomeModTxt.text = Utils.XLAT("settings_game_help_mod_apply");
    this.modResponTxt.text = Utils.XLAT("settings_game_help_mod_responsibilies");
    this.communicateModTxt.text = Utils.XLAT("settings_game_help_mod_contact");
    this.modHelpTxt.text = Utils.XLAT("settings_game_help_contact_mod_description");
  }
}
