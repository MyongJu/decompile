﻿// Decompiled with JetBrains decompiler
// Type: ConfigNotification
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigNotification
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, NotificationStaticInfo> _shopStaticDatas;
  private Dictionary<int, NotificationStaticInfo> _internalId2StatiDatas;

  public void BuildDB(object res)
  {
    Hashtable sources = res as Hashtable;
    if (sources == null)
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    else
      this.parse.Parse<NotificationStaticInfo, string>(sources, "ID", out this._shopStaticDatas, out this._internalId2StatiDatas);
  }

  public NotificationStaticInfo GetData(string id)
  {
    if (this._shopStaticDatas.ContainsKey(id))
      return this._shopStaticDatas[id];
    return (NotificationStaticInfo) null;
  }

  public NotificationStaticInfo GetData(int id)
  {
    if (this._internalId2StatiDatas.ContainsKey(id))
      return this._internalId2StatiDatas[id];
    return (NotificationStaticInfo) null;
  }
}
