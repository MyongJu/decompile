﻿// Decompiled with JetBrains decompiler
// Type: GemEnhanceItemSlideComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GemEnhanceItemSlideComponent : ComponentRenderBase
{
  public System.Action<int> OnSliderValueChangedHandler;
  public GemEnhanceItemSlideComponent.GetReachMaxItemCount reachMaxCounter;
  [SerializeField]
  private UITexture _Icon;
  [SerializeField]
  private UILabel _Level;
  [SerializeField]
  private UILabel _GemName;
  [SerializeField]
  private UILabel _Exp;
  [SerializeField]
  private StandardProgressBar _ProgressBar;
  [SerializeField]
  private UILabel _MaxCount;
  private int _selectedCount;
  private ConfigEquipmentGemInfo gemConfig;
  private ItemStaticInfo gemItemConfig;

  public int SelectedCount
  {
    get
    {
      return this._selectedCount;
    }
    set
    {
      if (this._selectedCount == value)
        return;
      int selectedCount = this._selectedCount;
      this._selectedCount = value;
      if (this.reachMaxCounter != null && value > selectedCount)
      {
        int num = this.reachMaxCounter(this.gemConfig);
        if (value > num)
        {
          this._selectedCount = num;
          this._ProgressBar.CurrentCount = num;
        }
      }
      if (this.OnSliderValueChangedHandler == null)
        return;
      this.OnSliderValueChangedHandler(value);
    }
  }

  public ConfigEquipmentGemInfo GemConfig
  {
    get
    {
      return this.gemConfig;
    }
  }

  public ItemStaticInfo GemItemConfig
  {
    get
    {
      return this.gemItemConfig;
    }
  }

  public override void Init()
  {
    base.Init();
    GemEnhanceItemSlideComponentData data = this.data as GemEnhanceItemSlideComponentData;
    if (data == null)
      return;
    GemDataWrapper gemDataWrapper = data.gemDataWrapper;
    this.gemConfig = ConfigManager.inst.DB_EquipmentGem.GetData(gemDataWrapper.GemData.ConfigId);
    if (this.gemConfig == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.gemConfig.itemID);
    if (itemStaticInfo == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this._Icon, this.gemConfig.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    this._GemName.text = itemStaticInfo.LocName;
    this._Level.text = string.Format("Lv.{0}", (object) this.gemConfig.level);
    this._Exp.text = string.Format("Exp +{0}", (object) this.gemConfig.expContain);
    this._MaxCount.text = string.Format("/{0}", (object) gemDataWrapper.GemCount);
    this._ProgressBar.Init(0, gemDataWrapper.GemCount);
    this._ProgressBar.onValueChanged = new System.Action<int>(this.OnValueChanged);
  }

  public override void Dispose()
  {
    base.Dispose();
  }

  private void OnValueChanged(int count)
  {
    this.SelectedCount = count;
  }

  public delegate int GetReachMaxItemCount(ConfigEquipmentGemInfo gemConfig);
}
