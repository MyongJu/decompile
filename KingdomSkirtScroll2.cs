﻿// Decompiled with JetBrains decompiler
// Type: KingdomSkirtScroll2
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class KingdomSkirtScroll2 : MonoBehaviour
{
  [SerializeField]
  private float m_Speed = 1f;
  private Vector2 m_UVCache = new Vector2();
  [SerializeField]
  private MeshRenderer m_Renderer;
  private float m_CurrentOffset;

  public void SetKingdom(KingdomData kingdom, KingdomSkirtScroll2.Boundary boundary)
  {
    switch (boundary)
    {
      case KingdomSkirtScroll2.Boundary.BOTTOM:
        this.transform.localPosition = new Vector3(kingdom.Dimension.x + 0.5f * kingdom.Dimension.width, kingdom.Dimension.y + kingdom.Dimension.height, 0.0f);
        this.transform.localRotation = Quaternion.Euler(-90f, 0.0f, 0.0f);
        break;
      case KingdomSkirtScroll2.Boundary.TOP:
        this.transform.localPosition = new Vector3(kingdom.Dimension.x + 0.5f * kingdom.Dimension.width, kingdom.Dimension.y, 0.0f);
        this.transform.localRotation = Quaternion.Euler(-90f, 0.0f, 0.0f);
        break;
      case KingdomSkirtScroll2.Boundary.LEFT:
        this.transform.localPosition = new Vector3(kingdom.Dimension.x, kingdom.Dimension.y + 0.5f * kingdom.Dimension.height, 0.0f);
        this.transform.localRotation = Quaternion.Euler(0.0f, -90f, 90f);
        break;
      case KingdomSkirtScroll2.Boundary.RIGHT:
        this.transform.localPosition = new Vector3(kingdom.Dimension.x + kingdom.Dimension.width, kingdom.Dimension.y + 0.5f * kingdom.Dimension.height, 0.0f);
        this.transform.localRotation = Quaternion.Euler(0.0f, -90f, 90f);
        break;
    }
    this.transform.localScale = Vector3.one;
  }

  private void Update()
  {
    this.m_CurrentOffset -= this.m_Speed * Time.deltaTime;
    this.m_CurrentOffset %= 1f;
    if (!((Object) this.m_Renderer != (Object) null))
      return;
    this.m_UVCache.Set(0.0f, this.m_CurrentOffset);
    this.m_Renderer.material.SetTextureOffset("_MainTex", this.m_UVCache);
  }

  public enum Boundary
  {
    BOTTOM,
    TOP,
    LEFT,
    RIGHT,
  }
}
