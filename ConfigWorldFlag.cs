﻿// Decompiled with JetBrains decompiler
// Type: ConfigWorldFlag
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigWorldFlag
{
  private Dictionary<string, WorldFlagInfo> _WorldFlagByUniqueId;
  private Dictionary<int, WorldFlagInfo> _WorldFlagByInternalId;
  private List<WorldFlagInfo> _AllWorldFlagInfo;

  protected int SortByOrderFunc(WorldFlagInfo a, WorldFlagInfo b)
  {
    return a.Sort.CompareTo(b.Sort);
  }

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<WorldFlagInfo, string>(res as Hashtable, "ID", out this._WorldFlagByUniqueId, out this._WorldFlagByInternalId);
    this._AllWorldFlagInfo = new List<WorldFlagInfo>();
    Dictionary<string, WorldFlagInfo>.Enumerator enumerator = this._WorldFlagByUniqueId.GetEnumerator();
    while (enumerator.MoveNext())
      this._AllWorldFlagInfo.Add(enumerator.Current.Value);
    this._AllWorldFlagInfo.Sort(new Comparison<WorldFlagInfo>(this.SortByOrderFunc));
  }

  public WorldFlagInfo GetData(int internalId)
  {
    WorldFlagInfo worldFlagInfo = (WorldFlagInfo) null;
    this._WorldFlagByInternalId.TryGetValue(internalId, out worldFlagInfo);
    return worldFlagInfo;
  }

  public WorldFlagInfo GetData(string id)
  {
    WorldFlagInfo worldFlagInfo = (WorldFlagInfo) null;
    this._WorldFlagByUniqueId.TryGetValue(id, out worldFlagInfo);
    return worldFlagInfo;
  }

  public List<WorldFlagInfo> GetAllWorldFlagInfo()
  {
    return this._AllWorldFlagInfo;
  }

  public void Clear()
  {
    if (this._WorldFlagByUniqueId != null)
    {
      this._WorldFlagByUniqueId.Clear();
      this._WorldFlagByUniqueId = (Dictionary<string, WorldFlagInfo>) null;
    }
    if (this._WorldFlagByInternalId != null)
    {
      this._WorldFlagByInternalId.Clear();
      this._WorldFlagByInternalId = (Dictionary<int, WorldFlagInfo>) null;
    }
    if (this._AllWorldFlagInfo == null)
      return;
    this._AllWorldFlagInfo.Clear();
    this._AllWorldFlagInfo = (List<WorldFlagInfo>) null;
  }
}
