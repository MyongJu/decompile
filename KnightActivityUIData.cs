﻿// Decompiled with JetBrains decompiler
// Type: KnightActivityUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class KnightActivityUIData : ActivityBaseUIData
{
  private ActivityManager.FallenKnightData _knightData;

  protected ActivityManager.FallenKnightData KnightData
  {
    get
    {
      if (this._knightData == null)
        this._knightData = this.Data as ActivityManager.FallenKnightData;
      return this._knightData;
    }
  }

  public override string EventKey
  {
    get
    {
      return "knight_activity";
    }
  }

  public override int ActivityID
  {
    get
    {
      return this.KnightData.activityId;
    }
  }

  public override ActivityBaseUIData.ActivityType Type
  {
    get
    {
      return ActivityBaseUIData.ActivityType.Knight;
    }
  }

  public override int StartTime
  {
    get
    {
      return this.KnightData.startTime;
    }
  }

  public override int EndTime
  {
    get
    {
      return this.KnightData.endTime;
    }
  }

  public override int GetRemainTime()
  {
    if (!this.IsStart())
      return this.StartTime - NetServerTime.inst.ServerTimestamp;
    int num = 0;
    if (this.TimePre == "event_finishes_num")
      num = 43200;
    return this.EndTime - NetServerTime.inst.ServerTimestamp - num;
  }

  public override string TimePre
  {
    get
    {
      if (!this.IsStart())
        return "event_starts_num";
      return (this.KnightData.isFinished || this.KnightData.activityTime < this.KnightData.startTime) && this.KnightData.endTime - NetServerTime.inst.ServerTimestamp <= 43200 ? "event_rewards_timer" : "event_finishes_num";
    }
  }

  public override IconData GetNormalIconData()
  {
    IconData iconData = (IconData) null;
    ActivityMainInfo data = ConfigManager.inst.DB_ActivityMain.GetData(this.KnightData.activityId);
    if (data != null)
    {
      iconData = new IconData();
      iconData.image = data.IconPath;
      iconData.contents = new string[2];
      iconData.contents[0] = data.LocalName;
      iconData.contents[1] = data.LocalDesc;
      iconData.Data = (object) this;
    }
    return iconData;
  }

  public override IconData GetADIconData()
  {
    IconData iconData = (IconData) null;
    ActivityMainInfo data = ConfigManager.inst.DB_ActivityMain.GetData(this.KnightData.activityId);
    if (data != null)
    {
      iconData = new IconData();
      iconData.image = data.ADImagePath;
      iconData.contents = new string[2];
      iconData.contents[0] = data.LocalName;
      iconData.contents[1] = data.LocalDesc;
    }
    return iconData;
  }

  public override void DisplayActivityDetail()
  {
    UIManager.inst.OpenDlg("Activity/FallenKnightEntryDlg", (UI.Dialog.DialogParameter) new ActivityFallenKnightDlg.Params()
    {
      internalId = this.ActivityID
    }, 1 != 0, 1 != 0, 1 != 0);
  }
}
