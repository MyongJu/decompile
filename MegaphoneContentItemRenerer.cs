﻿// Decompiled with JetBrains decompiler
// Type: MegaphoneContentItemRenerer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MegaphoneContentItemRenerer : MonoBehaviour
{
  public Vector3 startPos = new Vector3(660f, 0.0f, 0.0f);
  private int lifeTime = 20;
  private int duration = 10;
  private Vector2 ContentSize = Vector2.zero;
  public UILabel userName;
  public UILabel content;
  public UIPanel panel;
  public UISprite bg;
  public System.Action<MegaphoneContentItemRenerer> OnFinish;

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
  }

  private void Process(int time)
  {
    --this.lifeTime;
    if (this.lifeTime > 0)
      return;
    this.RemoveEventHandler();
    this.FadeOut();
  }

  private void OnFinishHandler()
  {
    this.RemoveEventHandler();
    if (this.OnFinish == null)
      return;
    this.OnFinish(this);
    this.gameObject.transform.localScale = Vector3.one;
  }

  private void ResetScrollView()
  {
    Vector4 baseClipRegion = this.panel.baseClipRegion;
    int num = this.bg.width - 180 - this.userName.width;
    baseClipRegion.z = (float) num;
    baseClipRegion.x = (float) (num / 2);
    this.panel.baseClipRegion = baseClipRegion;
  }

  public void SetData(MegaphoneContent data)
  {
    this.userName.text = data.UserName + " : ";
    this.ResetScrollView();
    this.content.text = data.Content;
    Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.content.transform, true);
    this.ContentSize.x = relativeWidgetBounds.size.x;
    this.ContentSize.y = relativeWidgetBounds.size.y;
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("megaphone_duration");
    this.lifeTime = data1 == null ? 20 : data1.ValueInt;
    this.MoveContent();
    this.AddEventHandler();
  }

  public void OnCloseHandler()
  {
    this.FadeOut();
  }

  private void FadeOut()
  {
    iTween.Stop(this.content.gameObject);
    this.OnFinishHandler();
  }

  private void OnCompleteHandler()
  {
    this.MoveContent();
  }

  public void MoveContent()
  {
    if ((double) this.ContentSize.x > (double) this.panel.baseClipRegion.z)
    {
      Vector3 startPos1 = this.startPos;
      startPos1.x = this.panel.baseClipRegion.z;
      this.content.transform.localPosition = startPos1;
      Vector3 startPos2 = this.startPos;
      startPos2.x = this.startPos.x - this.ContentSize.x;
      iTween.MoveTo(this.content.gameObject, iTween.Hash((object) "x", (object) startPos2.x, (object) "time", (object) this.duration, (object) "easetype", (object) "Linear", (object) "isLocal", (object) true, (object) "oncompletetarget", (object) this.gameObject, (object) "oncomplete", (object) "OnCompleteHandler"));
    }
    else
    {
      Vector3 localPosition = this.content.transform.localPosition;
      localPosition.x = this.startPos.x;
      this.content.transform.localPosition = localPosition;
    }
  }
}
