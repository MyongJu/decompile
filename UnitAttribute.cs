﻿// Decompiled with JetBrains decompiler
// Type: UnitAttribute
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class UnitAttribute
{
  public BarracksGridBar health;
  public BarracksGridBar attack;
  public BarracksGridBar defense;
  public BarracksGridBar speed;
  public UISprite attackTypeIcon;
  public UISprite defenceTypeIcon;
  public UISprite strongTypeIcon1;
  public UISprite strongTypeIcon2;
  public UISprite weakTypeIcon1;
  public UISprite weakTypeIcon2;
  public UILabel strongVSLabel;
  public UILabel weakVSLabel;
}
