﻿// Decompiled with JetBrains decompiler
// Type: TimeLimitRankRewardUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class TimeLimitRankRewardUIData : RoundActivityRankRewardUIBaseData
{
  private ActivityRankRewardInfo _rewardInfo;

  private ActivityRankRewardInfo RewardInfo
  {
    get
    {
      if (this._rewardInfo == null)
        this._rewardInfo = this.Data as ActivityRankRewardInfo;
      return this._rewardInfo;
    }
  }

  public override int MinRank
  {
    get
    {
      return this.RewardInfo.MinRank;
    }
  }

  public override int MaxRank
  {
    get
    {
      return this.RewardInfo.MaxRank;
    }
  }

  public override Dictionary<int, int> CurrentRewardItems
  {
    get
    {
      Dictionary<int, int> souces = new Dictionary<int, int>();
      this.AddDict(souces, this.RewardInfo.ItemRewardID_1, this.RewardInfo.ItemRewardValue_1);
      this.AddDict(souces, this.RewardInfo.ItemRewardID_2, this.RewardInfo.ItemRewardValue_2);
      this.AddDict(souces, this.RewardInfo.ItemRewardID_3, this.RewardInfo.ItemRewardValue_3);
      this.AddDict(souces, this.RewardInfo.ItemRewardID_4, this.RewardInfo.ItemRewardValue_4);
      this.AddDict(souces, this.RewardInfo.ItemRewardID_5, this.RewardInfo.ItemRewardValue_5);
      this.AddDict(souces, this.RewardInfo.ItemRewardID_6, this.RewardInfo.ItemRewardValue_6);
      return souces;
    }
  }

  public override List<int> CurrentItems
  {
    get
    {
      List<int> souces = new List<int>();
      this.AddList(souces, this.RewardInfo.ItemRewardID_1);
      this.AddList(souces, this.RewardInfo.ItemRewardID_2);
      this.AddList(souces, this.RewardInfo.ItemRewardID_3);
      this.AddList(souces, this.RewardInfo.ItemRewardID_4);
      this.AddList(souces, this.RewardInfo.ItemRewardID_5);
      this.AddList(souces, this.RewardInfo.ItemRewardID_6);
      return souces;
    }
  }
}
