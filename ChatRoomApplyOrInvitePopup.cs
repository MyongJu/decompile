﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomApplyOrInvitePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ChatRoomApplyOrInvitePopup : Popup
{
  public const string POPUP_TYPE = "Chat/ChatRoomApplyOrInvitePopup";
  public PageTabsMonitor tabs;
  public ChatRoomInviteRender inviteRender;
  public ChatRoomApplyRender applyRender;
  public UIGrid contentGrid;
  public UIScrollView scrollView;
  public UILabel emptyContent;
  public GameObject inviteSymbol;
  public GameObject applySymbol;
  private int _currentTab;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.OnInviteTabClick();
    DBManager.inst.DB_room.onMemberUpdated += new System.Action<long, long>(this.OnRoomChanged);
    DBManager.inst.DB_room.onDataRemoved += new System.Action(this.OnRoomRemoved);
    DBManager.inst.DB_room.onDataCreated += new System.Action<long>(this.OnRoomAdded);
    this.contentGrid.onReposition = (UIGrid.OnReposition) (() => this.scrollView.ResetPosition());
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    DBManager.inst.DB_room.onMemberUpdated -= new System.Action<long, long>(this.OnRoomChanged);
    DBManager.inst.DB_room.onDataRemoved -= new System.Action(this.OnRoomRemoved);
    DBManager.inst.DB_room.onDataCreated -= new System.Action<long>(this.OnRoomAdded);
  }

  public void OnCloseClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnInviteTabClick()
  {
    this._currentTab = 0;
    this.tabs.SetCurrentTab(this._currentTab, true);
    this.ClearContent();
    this.FeedInvite(ChatRoomManager.InvitingRooms());
    this.inviteSymbol.SetActive(ChatRoomManager.HasInviting());
    this.applySymbol.SetActive(false);
  }

  public void OnApplyTabClick()
  {
    this._currentTab = 1;
    this.tabs.SetCurrentTab(this._currentTab, true);
    this.ClearContent();
    this.FeedApply(ChatRoomManager.ApplyingRooms());
    this.inviteSymbol.SetActive(ChatRoomManager.HasInviting());
    this.applySymbol.SetActive(false);
  }

  private void FeedInvite(List<ChatRoomData> datas)
  {
    this.scrollView.ResetPosition();
    using (List<ChatRoomData>.Enumerator enumerator = datas.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ChatRoomData current = enumerator.Current;
        if (current.GetOwnerMember() != null)
        {
          GameObject gameObject = NGUITools.AddChild(this.contentGrid.gameObject, this.inviteRender.gameObject);
          gameObject.GetComponent<ChatRoomInviteRender>().Feed(current);
          gameObject.SetActive(true);
        }
      }
    }
    this.contentGrid.Reposition();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.scrollView.ResetPosition()));
    this.emptyContent.gameObject.SetActive(datas.Count == 0);
    this.emptyContent.text = Utils.XLAT("placeholder_chat_no_invitations_description");
  }

  private void FeedApply(List<ChatRoomData> datas)
  {
    this.scrollView.ResetPosition();
    using (List<ChatRoomData>.Enumerator enumerator = datas.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ChatRoomData current = enumerator.Current;
        if (current.GetOwnerMember() != null)
        {
          GameObject gameObject = NGUITools.AddChild(this.contentGrid.gameObject, this.applyRender.gameObject);
          gameObject.GetComponent<ChatRoomApplyRender>().Feed(current);
          gameObject.SetActive(true);
        }
      }
    }
    this.contentGrid.Reposition();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.scrollView.ResetPosition()));
    this.emptyContent.gameObject.SetActive(datas.Count == 0);
    this.emptyContent.text = Utils.XLAT("placeholder_chat_no_applications_description");
  }

  private void ClearContent()
  {
    for (int index = this.contentGrid.transform.childCount - 1; index >= 0; --index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.contentGrid.transform.GetChild(index).gameObject);
  }

  private void OnRoomChanged(long roomId, long uid)
  {
    this.Refresh();
  }

  private void OnRoomAdded(long roomId)
  {
    this.Refresh();
  }

  private void OnRoomRemoved()
  {
    this.Refresh();
  }

  private void Refresh()
  {
    if (this._currentTab == 0)
      this.OnInviteTabClick();
    else
      this.OnApplyTabClick();
  }

  public class Parameter : Popup.PopupParameter
  {
  }
}
