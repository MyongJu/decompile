﻿// Decompiled with JetBrains decompiler
// Type: MsgResponseStatus
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class MsgResponseStatus
{
  public int errCode;
  public string errMsg;
  public bool isPassed;
  public int unmuteTimestamp;
  public string content;

  public override string ToString()
  {
    return string.Format("errCode:{0} errMsg:{1} isPassed:{2}, unmuteTimestamp:{3}", (object) this.errCode, (object) this.errMsg, (object) this.isPassed, (object) this.unmuteTimestamp);
  }
}
