﻿// Decompiled with JetBrains decompiler
// Type: BoostsListItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UI;
using UnityEngine;

public class BoostsListItem : MonoBehaviour
{
  public UILabel ownNum;
  public UIButton buyBtn;
  public UIButton useBtn;
  public UILabel itemPrice;
  private ShopStaticInfo shopInfo;
  public Icon icon;
  private ItemStaticInfo item;
  private bool isAddedEventHandler;
  private bool isInit;
  public BoostsItemsDlg owner;

  public void SetItem(ItemStaticInfo info)
  {
    this.item = info;
    this.UpdateUI();
    if (this.isInit)
      return;
    this.isInit = true;
    this.AddEventHandler();
  }

  public void SetShopItem(ShopStaticInfo info)
  {
    this.shopInfo = info;
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemChangeHandler);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnItemChangeHandler);
    EventDelegate.Add(this.buyBtn.onClick, new EventDelegate.Callback(this.OnBuyHandler));
    EventDelegate.Add(this.useBtn.onClick, new EventDelegate.Callback(this.OnUseItemHandler));
  }

  private void OnBuyHandler()
  {
    this.BuyAndUserConfirmHandler();
  }

  private void BuyAndUserConfirmHandler()
  {
    bool flag = Utils.CheckBoostIsStart(ConfigManager.inst.DB_Boosts.GetBoostByItemInternalId(this.item.internalId).Type);
    string str1 = ScriptLocalization.Get("boost_replace_warning", true);
    if (flag)
    {
      string str2 = ScriptLocalization.Get("shop_buy_yesbtlabel", true);
      string str3 = ScriptLocalization.Get("shop_buy_nobtlabel", true);
      string str4 = ScriptLocalization.Get("shop_buy_useTitle", true);
      UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
      {
        setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
        title = str4,
        leftButtonText = str3,
        leftButtonClickEvent = (System.Action) null,
        rightButtonText = str2,
        rightButtonClickEvent = new System.Action(this.BackConfirm),
        description = str1
      });
    }
    else if (ItemBag.Instance.GetShopItemPrice(this.shopInfo.internalId) > PlayerData.inst.hostPlayer.Currency)
    {
      this.CloseWindow();
      Utils.ShowNotEnoughGoldTip();
    }
    else
    {
      ItemBag.Instance.BuyAndUseShopItem(this.shopInfo.internalId, 1, (Hashtable) null, (System.Action<bool, object>) null);
      this.CloseWindow();
    }
  }

  private void OnUseItemHandler()
  {
    ItemBaseUse.Create(this.item.internalId, false).UseItem((System.Action<bool, object>) ((arg1, arg2) => this.CloseWindow()));
  }

  private void CloseWindow()
  {
    if (!((UnityEngine.Object) this.owner != (UnityEngine.Object) null))
      return;
    this.owner.OnBackBtPress();
  }

  private void BackConfirm()
  {
    ItemBag.Instance.BuyAndUseShopItem(this.shopInfo.internalId, 1, (Hashtable) null, (System.Action<bool, object>) null);
    this.CloseWindow();
  }

  private void OnItemChangeHandler(int internalId)
  {
    if (internalId != this.item.internalId)
      return;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this.item == null)
      this.item = ConfigManager.inst.DB_Items.GetItem(this.shopInfo.Item_InternalId);
    int itemCount = ItemBag.Instance.GetItemCount(this.item.internalId);
    IconData iconData = new IconData(this.item.ImagePath, new string[3]
    {
      this.item.LocName,
      this.item.LocDescription,
      string.Format("x{0}", (object) itemCount)
    });
    iconData.Data = (object) this.item.internalId;
    string imagePath = this.item.ImagePath;
    this.icon.FeedData((IComponentData) iconData);
    if (itemCount <= 0 && this.shopInfo != null)
    {
      if (this.shopInfo.Price <= 0)
        this.itemPrice.text = ScriptLocalization.Get("shop_buy_free", true);
      else
        Utils.SetPriceToLabel(this.itemPrice, ItemBag.Instance.GetShopItemPrice(this.shopInfo.ID));
    }
    if (itemCount > 0)
    {
      this.ownNum.color = ItemStore.NonZeroColor;
      this.buyBtn.gameObject.SetActive(false);
      this.useBtn.gameObject.SetActive(true);
    }
    else
    {
      this.ownNum.color = ItemStore.ZeroColor;
      this.buyBtn.gameObject.SetActive(true);
      this.useBtn.gameObject.SetActive(false);
    }
    if (this.shopInfo == null || (double) this.shopInfo.Discount_In_Limit_Time == 0.0 || (this.shopInfo.Limit_Discount_End_Time <= (long) NetServerTime.inst.ServerTimestamp || this.isAddedEventHandler))
      return;
    this.isAddedEventHandler = true;
    Oscillator.Instance.updateEvent += new System.Action<double>(this.Process);
  }

  private void Process(double time)
  {
    if (this.shopInfo.Price <= 0)
      this.itemPrice.text = ScriptLocalization.Get("shop_buy_free", true);
    else
      this.itemPrice.text = ItemBag.Instance.GetShopItemPrice(this.shopInfo.ID).ToString();
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemChangeHandler);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnItemChangeHandler);
    EventDelegate.Remove(this.buyBtn.onClick, new EventDelegate.Callback(this.OnBuyHandler));
    EventDelegate.Remove(this.useBtn.onClick, new EventDelegate.Callback(this.OnUseItemHandler));
    this.isInit = false;
    if (!this.isAddedEventHandler)
      return;
    this.isAddedEventHandler = false;
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.updateEvent -= new System.Action<double>(this.Process);
  }

  public void Clear()
  {
    this.RemoveEventHandler();
    this.icon.Dispose();
    this.owner = (BoostsItemsDlg) null;
  }
}
