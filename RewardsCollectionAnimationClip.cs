﻿// Decompiled with JetBrains decompiler
// Type: RewardsCollectionAnimationClip
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class RewardsCollectionAnimationClip : MonoBehaviour
{
  private List<Vector3> itemsPos = new List<Vector3>();
  private List<GameObject> itemsGO = new List<GameObject>();
  private List<Vector3> scorePos = new List<Vector3>();
  private List<GameObject> scoreGO = new List<GameObject>();
  private List<Vector3> ressPos = new List<Vector3>(5);
  private List<GameObject> ressGO = new List<GameObject>(5);
  private const string itemPrefab = "Prefab/UI/Common/CollectedItem";
  private const string itemPrefab2 = "Prefab/UI/Common/CollectedItem2";
  private const string xpPrefab = "Prefab/UI/Common/CollectedXP";
  private const string powerPrefab = "Prefab/UI/Common/CollectedPower";
  private const string resPrefab = "Prefab/UI/Common/CollectedResource";
  private const string coinPrefab = "Prefab/UI/Common/CollectedCoin";
  private const string scorePrefab = "Prefab/UI/Common/CollectedScore";
  [HideInInspector]
  public List<ItemRewardInfo.Data> items;
  public List<Item2RewardInfo.Data> items2;
  public List<RewardScoreInfo.Data> scores;
  [HideInInspector]
  public List<ResRewardsInfo.Data> Ress;
  public System.Action onItemCollectFinished;
  public System.Action onXPCollectFinished;
  public System.Action onPowerCollectFinished;
  public System.Action onResCollectFinished;
  public System.Action onScoreCollectFinished;
  public System.Action<GameObject> onAllCollectFinished;
  public int mXP;
  internal int mPower;

  [DebuggerHidden]
  public IEnumerator InvokeFinishCallback(float time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardsCollectionAnimationClip.\u003CInvokeFinishCallback\u003Ec__IteratorA5()
    {
      time = time,
      \u003C\u0024\u003Etime = time,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  public IEnumerator BeginCollectItem(float time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardsCollectionAnimationClip.\u003CBeginCollectItem\u003Ec__IteratorA6()
    {
      time = time,
      \u003C\u0024\u003Etime = time,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  public IEnumerator BeginCollectItem2(float time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardsCollectionAnimationClip.\u003CBeginCollectItem2\u003Ec__IteratorA7()
    {
      time = time,
      \u003C\u0024\u003Etime = time,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  public IEnumerator BeginCollectScore(float time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardsCollectionAnimationClip.\u003CBeginCollectScore\u003Ec__IteratorA8()
    {
      time = time,
      \u003C\u0024\u003Etime = time,
      \u003C\u003Ef__this = this
    };
  }

  private System.Action<UnityEngine.Object, bool> CollectItemCallback()
  {
    return (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      if (!ret)
        return;
      this.CalculateItemsPosition(this.items.Count);
      this.StartCoroutine(this.PlayItemCollectionAnimation(obj));
    });
  }

  private System.Action<UnityEngine.Object, bool> CollectItemCallback2()
  {
    return (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      if (!ret)
        return;
      this.CalculateItemsPosition(this.items2.Count);
      this.StartCoroutine(this.PlayItem2CollectionAnimation(obj));
    });
  }

  private System.Action<UnityEngine.Object, bool> CollectScoreCallback()
  {
    return (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      if (!ret)
        return;
      this.CalculateScoresPosition(this.scores.Count);
      this.StartCoroutine(this.PlayScoreCollectionAnimation(obj));
    });
  }

  private void CalculateScoresPosition(int count)
  {
    Utils.ArrangePositionMode2(RewardsCollectionAnimator.Instance.scoreRoot.transform.localPosition, count, RewardsCollectionAnimator.Instance.itemGapX, RewardsCollectionAnimator.Instance.itemGapY, ref this.scorePos, RewardsCollectionAnimator.Instance.maxnuminoneline);
  }

  [DebuggerHidden]
  private IEnumerator PlayScoreCollectionAnimation(UnityEngine.Object obj)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardsCollectionAnimationClip.\u003CPlayScoreCollectionAnimation\u003Ec__IteratorA9()
    {
      obj = obj,
      \u003C\u0024\u003Eobj = obj,
      \u003C\u003Ef__this = this
    };
  }

  private void CalculateItemsPosition(int count)
  {
    Utils.ArrangePositionMode2(RewardsCollectionAnimator.Instance.itemsRoot.transform.localPosition, count, RewardsCollectionAnimator.Instance.itemGapX, RewardsCollectionAnimator.Instance.itemGapY, ref this.itemsPos, RewardsCollectionAnimator.Instance.maxnuminoneline);
  }

  [DebuggerHidden]
  private IEnumerator PlayItemCollectionAnimation(UnityEngine.Object obj)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardsCollectionAnimationClip.\u003CPlayItemCollectionAnimation\u003Ec__IteratorAA()
    {
      obj = obj,
      \u003C\u0024\u003Eobj = obj,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator PlayItem2CollectionAnimation(UnityEngine.Object obj)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardsCollectionAnimationClip.\u003CPlayItem2CollectionAnimation\u003Ec__IteratorAB()
    {
      obj = obj,
      \u003C\u0024\u003Eobj = obj,
      \u003C\u003Ef__this = this
    };
  }

  public void OnScoreCollectComplete()
  {
    using (List<GameObject>.Enumerator enumerator = this.scoreGO.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.scoreGO.Clear();
    this.scores.Clear();
    this.scores = (List<RewardScoreInfo.Data>) null;
    this.scorePos.Clear();
    if (this.onScoreCollectFinished == null)
      return;
    this.onScoreCollectFinished();
  }

  public void OnItemCollectComplete()
  {
    using (List<GameObject>.Enumerator enumerator = this.itemsGO.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.itemsGO.Clear();
    this.items.Clear();
    this.items = (List<ItemRewardInfo.Data>) null;
    this.itemsPos.Clear();
    if (this.onItemCollectFinished == null)
      return;
    this.onItemCollectFinished();
  }

  public void OnItem2CollectComplete()
  {
    using (List<GameObject>.Enumerator enumerator = this.itemsGO.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.itemsGO.Clear();
    this.items2.Clear();
    this.items2 = (List<Item2RewardInfo.Data>) null;
    this.itemsPos.Clear();
    if (this.onItemCollectFinished == null)
      return;
    this.onItemCollectFinished();
  }

  [DebuggerHidden]
  public IEnumerator BeginCollectCoins(float time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardsCollectionAnimationClip.\u003CBeginCollectCoins\u003Ec__IteratorAC()
    {
      time = time,
      \u003C\u0024\u003Etime = time,
      \u003C\u003Ef__this = this
    };
  }

  private System.Action<UnityEngine.Object, bool> CollectCoinCallback()
  {
    return (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      if (!ret)
        return;
      this.StartCoroutine(VaultFunHelper.Instance.ShowCoinCollectEffect(Utils.DuplicateGOB(obj as GameObject, this.transform.parent), CitadelSystem.inst.mPublicHUD.mGoldValue.parent.transform, 15));
    });
  }

  [DebuggerHidden]
  public IEnumerator BeginCollectXP(float time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardsCollectionAnimationClip.\u003CBeginCollectXP\u003Ec__IteratorAD()
    {
      time = time,
      \u003C\u0024\u003Etime = time,
      \u003C\u003Ef__this = this
    };
  }

  private System.Action<UnityEngine.Object, bool> CollectXPCallback()
  {
    return (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      if (!ret)
        return;
      this.StartCoroutine(this.PlayXPCollectionAnimation(obj, this.mXP));
    });
  }

  [DebuggerHidden]
  private IEnumerator PlayXPCollectionAnimation(UnityEngine.Object obj, int xp)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardsCollectionAnimationClip.\u003CPlayXPCollectionAnimation\u003Ec__IteratorAE()
    {
      obj = obj,
      xp = xp,
      \u003C\u0024\u003Eobj = obj,
      \u003C\u0024\u003Exp = xp,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  public IEnumerator BeginCollectPower(float time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardsCollectionAnimationClip.\u003CBeginCollectPower\u003Ec__IteratorAF()
    {
      time = time,
      \u003C\u0024\u003Etime = time,
      \u003C\u003Ef__this = this
    };
  }

  private System.Action<UnityEngine.Object, bool> CollectPowerCallback()
  {
    return (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      if (!ret)
        return;
      this.StartCoroutine(this.PlayPowerCollectionAnimation(obj, this.mPower));
    });
  }

  [DebuggerHidden]
  private IEnumerator PlayPowerCollectionAnimation(UnityEngine.Object obj, int power)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardsCollectionAnimationClip.\u003CPlayPowerCollectionAnimation\u003Ec__IteratorB0()
    {
      obj = obj,
      power = power,
      \u003C\u0024\u003Eobj = obj,
      \u003C\u0024\u003Epower = power,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  public IEnumerator BeginCollectRes(float time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardsCollectionAnimationClip.\u003CBeginCollectRes\u003Ec__IteratorB1()
    {
      time = time,
      \u003C\u0024\u003Etime = time,
      \u003C\u003Ef__this = this
    };
  }

  private System.Action<UnityEngine.Object, bool> CollectResCallback()
  {
    return (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      if (!ret)
        return;
      this.CalculateRessPosition();
      this.StartCoroutine(this.PlayResCollectionAnimation(obj));
    });
  }

  private void CalculateRessPosition()
  {
    Utils.ArrangePositionMode2(RewardsCollectionAnimator.Instance.resRoot.transform.localPosition, this.Ress.Count, RewardsCollectionAnimator.Instance.resGapX, RewardsCollectionAnimator.Instance.resGapY, ref this.ressPos, RewardsCollectionAnimator.Instance.maxnuminoneline);
  }

  [DebuggerHidden]
  private IEnumerator PlayResCollectionAnimation(UnityEngine.Object obj)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RewardsCollectionAnimationClip.\u003CPlayResCollectionAnimation\u003Ec__IteratorB2()
    {
      obj = obj,
      \u003C\u0024\u003Eobj = obj,
      \u003C\u003Ef__this = this
    };
  }

  private void OnResCollectComplete()
  {
    using (List<GameObject>.Enumerator enumerator = this.ressGO.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.ressGO.Clear();
    this.Ress.Clear();
    this.Ress = (List<ResRewardsInfo.Data>) null;
    this.ressPos.Clear();
    if (this.onResCollectFinished == null)
      return;
    this.onResCollectFinished();
  }
}
