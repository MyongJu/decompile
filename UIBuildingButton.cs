﻿// Decompiled with JetBrains decompiler
// Type: UIBuildingButton
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Interaction/Button")]
public class UIBuildingButton : UIButton
{
  public event UIBuildingButton.OnPressed onPressed;

  protected override void OnPress(bool isPressed)
  {
    if (!this.isEnabled)
      return;
    if (this.onPressed != null)
      this.onPressed(isPressed);
    base.OnPress(isPressed);
  }

  public delegate void OnPressed(bool isPressed);
}
