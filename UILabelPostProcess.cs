﻿// Decompiled with JetBrains decompiler
// Type: UILabelPostProcess
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public class UILabelPostProcess
{
  public static void ProcessCoordinate(UILabel uiLabel, GameObject template)
  {
    UILabelPostProcess.Reset(uiLabel);
    string pattern = "\\d{1,4}:\\d{1,4}";
    MatchCollection matchCollection = Regex.Matches(uiLabel.text, pattern);
    if ((Object) template == (Object) null || matchCollection.Count < 1)
      return;
    UIWidget uiWidget = NGUITools.AddWidget<UIWidget>(uiLabel.gameObject);
    uiWidget.depth = uiLabel.depth + 1;
    uiWidget.name = "CoordinateParent";
    uiLabel.alignment = NGUIText.Alignment.Left;
    uiLabel.pivot = UIWidget.Pivot.TopLeft;
    int fontSize = uiLabel.fontSize;
    float width = (float) uiLabel.width;
    float num1 = (float) uiLabel.fontSize + uiLabel.effectiveSpacingY;
    float effectiveSpacingX = uiLabel.effectiveSpacingX;
    string processedText = uiLabel.processedText;
    uiLabel.UpdateNGUIText();
    StringBuilder stringBuilder = new StringBuilder();
    int num2 = 0;
    float num3 = width;
    Match match = matchCollection[0];
    float num4 = NGUIText.GetGlyphWidth(32, 0) + effectiveSpacingX;
    for (int index = 0; index < processedText.Length; ++index)
    {
      if (match != null && match.Index == index)
      {
        int repeatCount = UILabelPostProcess.CalcuNumOfPlaceholder(match.Value, effectiveSpacingX);
        if ((double) num3 - (double) num4 * (double) repeatCount < 0.0)
        {
          stringBuilder.Append('\n');
          ++num2;
          num3 = width;
        }
        UILabelPostProcess.AddCoordinate(match.Value, width - num3, (float) -((double) num2 + 0.5) * num1, uiWidget.gameObject, template);
        uiLabel.UpdateNGUIText();
        num3 -= num4 * (float) repeatCount;
        stringBuilder.Append(' ', repeatCount);
        index += match.Length - 1;
        match = match.NextMatch();
      }
      else
      {
        char ch = processedText[index];
        if ((int) ch == 10)
        {
          ++num2;
          num3 = width;
        }
        float num5 = NGUIText.GetGlyphWidth((int) ch, 0) + effectiveSpacingX;
        float num6 = (double) num5 <= 0.0 ? (float) fontSize : num5;
        if ((double) num3 - (double) num6 < 0.0)
        {
          stringBuilder.Append('\n');
          ++num2;
          num3 = width;
        }
        stringBuilder.Append(ch);
        num3 -= num6;
      }
    }
    uiLabel.text = stringBuilder.ToString();
  }

  public static void Reset(UILabel uiLabel)
  {
    Transform child = uiLabel.transform.FindChild("CoordinateParent");
    if (!((Object) child != (Object) null))
      return;
    NGUITools.Destroy((Object) child.gameObject);
  }

  public static bool IsMatch(string text)
  {
    string pattern = "\\d{1,4}:\\d{1,4}";
    foreach (Match match in Regex.Matches(text, pattern))
      Debug.Log((object) string.Format("'{0}' found at index {1}.", (object) match.Value, (object) match.Index));
    return true;
  }

  private static void AddCoordinate(string text, float x, float y, GameObject parent, GameObject template)
  {
    GameObject gameObject = NGUITools.AddChild(parent, template);
    gameObject.SetActive(true);
    gameObject.transform.localPosition = new Vector3(x, y, 0.0f);
    gameObject.GetComponent<UILabel>().text = text;
  }

  private static int CalcuNumOfPlaceholder(string text, float spaceingX)
  {
    float num1 = 0.0f;
    for (int index = 0; index < text.Length; ++index)
      num1 += NGUIText.GetGlyphWidth((int) text[index], 0) + spaceingX;
    float num2 = NGUIText.GetGlyphWidth(32, 0) + spaceingX;
    return (double) num2 <= 0.0 ? 4 : Mathf.CeilToInt(num1 / num2);
  }
}
