﻿// Decompiled with JetBrains decompiler
// Type: PersonalActivityData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class PersonalActivityData : ActivityBaseData, IRankUIRewardData
{
  private bool _empty = true;
  private int _currentScore;
  private int _currentRank;

  public int CurrentRank
  {
    get
    {
      return this._currentRank;
    }
  }

  public int CurrentRoundScore
  {
    get
    {
      return this._currentScore;
    }
  }

  public new bool IsEmpty()
  {
    return this._empty;
  }

  public List<int> CurrentRoundTargetScores
  {
    get
    {
      List<int> intList = new List<int>();
      List<PersonalActivityReward> currentRewards = ConfigManager.inst.DB_PersonalActivtyReward.GetCurrentRewards(this.ActivityId, 5);
      for (int index = 0; index < currentRewards.Count; ++index)
        intList.Add(currentRewards[index].StrongholdLevel);
      return intList;
    }
  }

  public float Progress
  {
    get
    {
      float num1 = 0.0f;
      float num2 = 1f / (float) this.CurrentRoundTargetScores.Count;
      int num3 = 0;
      int mLevel = PlayerData.inst.CityData.mStronghold.mLevel;
      if (mLevel >= this.CurrentRoundTargetScores[this.CurrentRoundTargetScores.Count - 1])
        return 1f;
      for (int index = 0; index < this.CurrentRoundTargetScores.Count; ++index)
      {
        if (mLevel > this.CurrentRoundTargetScores[index])
        {
          num3 = this.CurrentRoundTargetScores[index];
        }
        else
        {
          if (num3 == 0)
          {
            PersonalActivityReward preRewards = ConfigManager.inst.DB_PersonalActivtyReward.GetPreRewards(this.CurrentRoundTargetScores[0]);
            if (preRewards != null)
              num3 = preRewards.StrongholdLevel;
          }
          float num4 = (float) (mLevel - num3) / (float) (this.CurrentRoundTargetScores[index] - num3);
          num1 = ((float) index + num4) * num2;
          break;
        }
      }
      if ((double) num1 > 1.0)
        num1 = 1f;
      return num1;
    }
  }

  public override ActivityBaseData.ActivityType Type
  {
    get
    {
      return ActivityBaseData.ActivityType.Personal;
    }
  }

  public List<int> GetRankTopOneItems()
  {
    List<int> souces = new List<int>();
    PersonalActivityRankReward currentRewardByRank = ConfigManager.inst.DB_PersonalActivtyRankReward.GetCurrentRewardByRank(1);
    this.AddList(souces, currentRewardByRank.ItemRewardID_1);
    this.AddList(souces, currentRewardByRank.ItemRewardID_2);
    this.AddList(souces, currentRewardByRank.ItemRewardID_3);
    this.AddList(souces, currentRewardByRank.ItemRewardID_4);
    this.AddList(souces, currentRewardByRank.ItemRewardID_5);
    this.AddList(souces, currentRewardByRank.ItemRewardID_6);
    return souces;
  }

  public Dictionary<int, int> GetRankTopOneRewardItems()
  {
    Dictionary<int, int> souces = new Dictionary<int, int>();
    PersonalActivityRankReward currentRewardByRank = ConfigManager.inst.DB_PersonalActivtyRankReward.GetCurrentRewardByRank(1);
    this.AddDict(souces, currentRewardByRank.ItemRewardID_1, currentRewardByRank.ItemRewardValue_1);
    this.AddDict(souces, currentRewardByRank.ItemRewardID_2, currentRewardByRank.ItemRewardValue_2);
    this.AddDict(souces, currentRewardByRank.ItemRewardID_3, currentRewardByRank.ItemRewardValue_3);
    this.AddDict(souces, currentRewardByRank.ItemRewardID_4, currentRewardByRank.ItemRewardValue_4);
    this.AddDict(souces, currentRewardByRank.ItemRewardID_5, currentRewardByRank.ItemRewardValue_5);
    this.AddDict(souces, currentRewardByRank.ItemRewardID_6, currentRewardByRank.ItemRewardValue_6);
    return souces;
  }

  public List<string> GetRequirementDesc()
  {
    List<string> stringList = new List<string>();
    List<PersonalActivityReward> toatalRewards = ConfigManager.inst.DB_PersonalActivtyReward.GetToatalRewards();
    for (int index = 0; index < toatalRewards.Count; ++index)
      stringList.Add(toatalRewards[index].Description);
    return stringList;
  }

  public override void Decode(Hashtable datas)
  {
    if (datas == null)
      return;
    base.Decode(datas);
    string empty = string.Empty;
    if (datas.ContainsKey((object) "score"))
      int.TryParse(datas[(object) "score"].ToString(), out this._currentScore);
    if (datas.ContainsKey((object) "rank"))
      int.TryParse(datas[(object) "rank"].ToString(), out this._currentRank);
    this._empty = false;
  }
}
