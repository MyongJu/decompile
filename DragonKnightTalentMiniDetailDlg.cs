﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightTalentMiniDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonKnightTalentMiniDetailDlg : MonoBehaviour
{
  private DragonKnightTalentMiniDetailDlg.Data _data = new DragonKnightTalentMiniDetailDlg.Data();
  [SerializeField]
  private DragonKnightTalentMiniDetailDlg.Panel panel;

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void FreshPanel()
  {
    if (this._data.TalentActiveId == 0)
      return;
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (dragonKnightData == null)
      return;
    DragonKnightActiveTalent.Talent talent = dragonKnightData.activeTalents.GetTalent((long) this._data.TalentActiveInfo.talentId);
    DragonKnightTalentData knightTalentData = DBManager.inst.DB_DragonKnightTalent.Get(this._data.TalentActiveInfo.talentId);
    DragonKnightTalentInfo knightTalentInfo = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(this._data.TalentActiveInfo.talentId);
    if (knightTalentInfo != null)
    {
      this.panel.title.text = knightTalentInfo.LocName;
      Dictionary<string, string> para = new Dictionary<string, string>();
      para.Add("1", (knightTalentInfo.activeValue1 * 100f).ToString());
      para.Add("2", knightTalentInfo.activeValue2.ToString());
      para.Add("3", (knightTalentInfo.activeValue3 * 100f).ToString());
      para.Add("4", knightTalentInfo.activeBuffRound.ToString());
      if (knightTalentInfo.benefit != null && knightTalentInfo.benefit.benefits != null && knightTalentInfo.benefit.benefits.Count > 0)
      {
        Dictionary<string, float>.ValueCollection.Enumerator enumerator = knightTalentInfo.benefit.benefits.Values.GetEnumerator();
        if (enumerator.MoveNext())
          para.Add("benefit_value_1", string.Format("{0}%", (object) (float) ((double) (int) ((double) Mathf.Abs(enumerator.Current) * 1000.0) / 10.0)));
      }
      this.panel.skilLDes.text = ScriptLocalization.GetWithPara(knightTalentInfo.Description, para, true);
    }
    this.panel.BT_activeSkill.isEnabled = true;
    this.panel.activeSkillText.text = knightTalentData != null ? Utils.XLAT("talent_active_skill_activate_button") : Utils.XLAT("talent_active_skill_go_to_button");
    if (talent != null && knightTalentData != null)
    {
      this.panel.lockIcon.gameObject.SetActive(false);
      this.panel.noSkillWarning.gameObject.SetActive(false);
      if (talent != null)
      {
        if (talent.JobId != 0L)
        {
          this.panel.skillActive.gameObject.SetActive(true);
          this.panel.skillCoolDown.gameObject.SetActive(false);
          this.panel.skillActive.text = Utils.FormatTime(JobManager.Instance.GetJob(talent.JobId).LeftTime(), false, false, true);
          this.panel.BT_activeSkill.isEnabled = false;
        }
        else
        {
          this.panel.skillActive.gameObject.SetActive(false);
          if (talent.ActiveTime > NetServerTime.inst.ServerTimestamp)
          {
            this.panel.skillCoolDown.gameObject.SetActive(true);
            this.panel.skillCoolDown.text = Utils.FormatTime(talent.ActiveTime - NetServerTime.inst.ServerTimestamp, false, false, true);
            this.panel.BT_activeSkill.isEnabled = false;
          }
          else
            this.panel.skillCoolDown.gameObject.SetActive(false);
        }
      }
      else
      {
        this.panel.noSkillWarning.gameObject.SetActive(true);
        this.panel.skillActive.gameObject.SetActive(false);
        this.panel.skillCoolDown.gameObject.SetActive(false);
      }
      GreyUtility.Normal(this.panel.icon.gameObject);
    }
    else
    {
      this.panel.lockIcon.gameObject.SetActive(true);
      this.panel.noSkillWarning.gameObject.SetActive(true);
      this.panel.skillActive.gameObject.SetActive(false);
      this.panel.skillCoolDown.gameObject.SetActive(false);
      GreyUtility.Grey(this.panel.icon.gameObject);
    }
  }

  public void Open(int talentActiveId)
  {
    this.gameObject.SetActive(true);
    this._data.TalentActiveId = talentActiveId;
    DragonKnightTalentInfo knightTalentInfo = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(this._data.TalentActiveInfo.talentId);
    if (knightTalentInfo != null)
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.icon, knightTalentInfo.imagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.FreshPanel();
  }

  public void Close()
  {
    BuilderFactory.Instance.Release((UIWidget) this.panel.icon);
    this.gameObject.SetActive(false);
  }

  public void OnSecond(int serverTime)
  {
    this.FreshPanel();
  }

  public void OnActiveButtonClick()
  {
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (dragonKnightData == null)
      return;
    if (dragonKnightData.activeTalents.GetTalent((long) this._data.TalentActiveInfo.talentId) != null && DBManager.inst.DB_DragonKnightTalent.Get(this._data.TalentActiveInfo.talentId) != null)
    {
      RequestManager.inst.SendRequest("DragonKnight:playTalent", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "talent_id", (object) this._data.TalentActiveInfo.talentId), (System.Action<bool, object>) ((ret, obj) =>
      {
        DragonKnightTalentMiniDetailDlg.ResData resData = JsonReader.Deserialize<DragonKnightTalentMiniDetailDlg.ResData>(Utils.Object2Json(obj));
        if (resData == null || resData.skill_data == null)
          return;
        RewardsCollectionAnimator.Instance.Clear();
        using (Dictionary<string, int>.Enumerator enumerator = resData.skill_data.GetEnumerator())
        {
          while (enumerator.MoveNext())
            RewardsCollectionAnimator.Instance.Ress.Add(RewardsCollectionAnimator.Instance.GetRRID(enumerator.Current));
        }
        RewardsCollectionAnimator.Instance.CollectResource(true);
      }), true);
    }
    else
    {
      DragonKnightTalentTreeInfo talentTreeInfo = ConfigManager.inst.DB_DragonKnightTalentTree.GetTalentTreeInfo(ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(this._data.TalentActiveInfo.talentId).talentTreeInternalId);
      UIManager.inst.OpenDlg("DragonKnight/DragonKnightTalentTreeDlg", (UI.Dialog.DialogParameter) new DragonKnightTalentTreeDlg.Parameter()
      {
        index = (talentTreeInfo.tree - 1)
      }, 1 != 0, 1 != 0, 1 != 0);
      UIManager.inst.publicHUD.talentMiniDlg.Hide();
    }
  }

  public void OnCloseButtonClick()
  {
    this.Close();
  }

  public void Reset()
  {
    this.panel.title = this.transform.Find("Dialog_Small_S/Title_Label").gameObject.GetComponent<UILabel>();
    this.panel.icon = this.transform.Find("icon/SkillTexture").gameObject.GetComponent<UITexture>();
    this.panel.lockIcon = this.transform.Find("icon/LockIcon").gameObject.transform;
    this.panel.noSkillWarning = this.transform.Find("NoSkillWarning").gameObject.GetComponent<UILabel>();
    this.panel.skillActive = this.transform.Find("SkillActiving").gameObject.GetComponent<UILabel>();
    this.panel.skillCoolDown = this.transform.Find("SkillCoolDown").gameObject.GetComponent<UILabel>();
    this.panel.skilLDes = this.transform.Find("SkillDiscription").gameObject.GetComponent<UILabel>();
    this.panel.BT_close = this.transform.Find("Btn_Close").gameObject.GetComponent<UIButton>();
    this.panel.BT_activeSkill = this.transform.Find("Btn_Active").gameObject.GetComponent<UIButton>();
    this.panel.activeSkillText = this.transform.Find("Btn_Active/Label").gameObject.GetComponent<UILabel>();
  }

  public class ResData
  {
    public Dictionary<string, int> skill_data;
  }

  protected class Data
  {
    private int _talentInternalId;

    public int TalentActiveId
    {
      get
      {
        return this._talentInternalId;
      }
      set
      {
        this._talentInternalId = value;
        this.TalentActiveInfo = ConfigManager.inst.DB_DragonKnightTalentActive.GetItem(this._talentInternalId);
      }
    }

    public DragonKnightTalentActiveInfo TalentActiveInfo { get; private set; }
  }

  [Serializable]
  protected class Panel
  {
    public UITexture icon;
    public Transform lockIcon;
    public UILabel title;
    public UILabel noSkillWarning;
    public UILabel skillActive;
    public UILabel skillCoolDown;
    public UILabel skilLDes;
    public UILabel activeSkillText;
    public UIButton BT_close;
    public UIButton BT_activeSkill;
  }
}
