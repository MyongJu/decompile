﻿// Decompiled with JetBrains decompiler
// Type: WonderStateUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class WonderStateUIData : StateUIBaseData
{
  private DB.WonderData _wonder;

  public override StateUIBaseData.DataType Type
  {
    get
    {
      return StateUIBaseData.DataType.Avalon;
    }
  }

  public override string Title
  {
    get
    {
      return ScriptLocalization.Get("throne_wonder_name", true);
    }
  }

  public override long WorldId
  {
    get
    {
      return (long) this.Wonder.WORLD_ID;
    }
  }

  protected override long OccupyUserId
  {
    get
    {
      if (this.Wonder.State == "protected")
        return this.Wonder.KING_UID;
      return this.Wonder.OWNER_UID;
    }
  }

  protected override long OccupyAllianceId
  {
    get
    {
      if (this.Wonder.State == "protected")
        return this.Wonder.KING_ALLIANCE_ID;
      return this.Wonder.OWNER_ALLIANCE_ID;
    }
  }

  private DB.WonderData Wonder
  {
    get
    {
      if (this._wonder == null)
        this._wonder = this.Data as DB.WonderData;
      return this._wonder;
    }
  }
}
