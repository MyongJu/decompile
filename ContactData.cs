﻿// Decompiled with JetBrains decompiler
// Type: ContactData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactData
{
  public List<PlayerInfo> currents;
  public Dictionary<long, PlayerInfo> friends;
  public Dictionary<long, PlayerInfo> inviteds;
  public Dictionary<long, PlayerInfo> blockeds;
  public Dictionary<long, PlayerInfo> blockedBy;

  public ContactData()
  {
    this.currents = new List<PlayerInfo>();
    this.friends = new Dictionary<long, PlayerInfo>();
    this.inviteds = new Dictionary<long, PlayerInfo>();
    this.blockeds = new Dictionary<long, PlayerInfo>();
    this.blockedBy = new Dictionary<long, PlayerInfo>();
  }

  public bool IsFriend(string userName)
  {
    using (Dictionary<long, PlayerInfo>.ValueCollection.Enumerator enumerator = this.friends.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.userName == userName)
          return true;
      }
    }
    return false;
  }

  public void LoadData(object data)
  {
    this.LoadCurrents();
    if (data == null)
    {
      Debug.LogError((object) "NULL Contact content");
    }
    else
    {
      ArrayList arrayList = data as ArrayList;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        if (arrayList[index] is Hashtable)
        {
          Hashtable hashtable = arrayList[index] as Hashtable;
          long inUid = long.Parse(hashtable[(object) "contacts_uid"].ToString());
          int num = int.Parse(hashtable[(object) "contact_status"].ToString());
          string str = hashtable[(object) "name"].ToString();
          PlayerInfo playerInfo = new PlayerInfo(inUid);
          playerInfo.userName = str;
          switch (num)
          {
            case 0:
              this.friends.Add(playerInfo.uid, playerInfo);
              continue;
            case 1:
              this.inviteds.Add(playerInfo.uid, playerInfo);
              continue;
            case 2:
              this.blockeds.Add(playerInfo.uid, playerInfo);
              continue;
            case 3:
              this.blockedBy.Add(playerInfo.uid, playerInfo);
              continue;
            default:
              continue;
          }
        }
      }
    }
  }

  public void AddContact(PlayerInfo player)
  {
    if (player == null)
      return;
    Debug.LogError((object) ("Adding playing: " + (object) player.uid));
    if (this.friends.ContainsKey(player.uid))
      return;
    this.friends.Add(player.uid, player);
    NetApi.inst.Enqueue("Player:addContacts", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.hostPlayer.uid
      },
      {
        (object) "contacts_uid",
        (object) player.uid
      }
    }, (System.Action<Hashtable, Hashtable>) null);
  }

  public void AgreeInvited(PlayerInfo player)
  {
    if (player == null || !this.inviteds.ContainsKey(player.uid))
      return;
    this.inviteds.Remove(player.uid);
    this.friends.Add(player.uid, player);
    NetApi.inst.Enqueue("Player:acceptContacts", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.hostPlayer.uid
      },
      {
        (object) "contacts_uid",
        (object) player.uid
      }
    }, (System.Action<Hashtable, Hashtable>) null);
  }

  public void BlockUser(PlayerInfo player, bool block = true)
  {
    if (player == null || player.uid == -1L || this.blockeds.ContainsKey(player.uid))
      return;
    this.blockeds.Add(player.uid, player);
    if (this.friends.ContainsKey(player.uid))
      this.friends.Remove(player.uid);
    if (this.inviteds.ContainsKey(player.uid))
      this.inviteds.Remove(player.uid);
    NetApi.inst.Enqueue("Player:blockOrUnblockContacts", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.hostPlayer.uid
      },
      {
        (object) "operate",
        (object) (!block ? "unblock" : nameof (block))
      },
      {
        (object) "contact_uid",
        (object) player.uid
      }
    }, (System.Action<Hashtable, Hashtable>) null);
  }

  public void RemoveUser(PlayerInfo player)
  {
    bool flag = false;
    if (player == null || player.uid == -1L)
      return;
    if (this.friends.ContainsKey(player.uid))
    {
      this.friends.Remove(player.uid);
      flag = true;
    }
    if (this.inviteds.ContainsKey(player.uid))
    {
      this.inviteds.Remove(player.uid);
      flag = true;
    }
    if (this.blockeds.ContainsKey(player.uid))
    {
      this.blockeds.Remove(player.uid);
      flag = true;
    }
    if (!flag)
      return;
    NetApi.inst.Enqueue("Player:removeContacts", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.hostPlayer.uid
      },
      {
        (object) "contacts_uid",
        (object) player.uid
      }
    }, (System.Action<Hashtable, Hashtable>) null);
  }

  public bool AddCurrents(PlayerInfo player)
  {
    int index = 0;
    for (int count = this.currents.Count; index < count; ++index)
    {
      if (this.currents[index].uid == player.uid)
      {
        this.currents.RemoveAt(index);
        break;
      }
    }
    this.currents.Insert(0, player);
    return true;
  }

  public void SaveCurrents()
  {
    string str = string.Empty;
    int index = 0;
    for (int count = this.currents.Count; index < count; ++index)
    {
      str = str + (object) this.currents[index].uid + ",";
      PlayerPrefsEx.SetString(string.Format("{0}{1}", (object) "contact_current_", (object) this.currents[index].uid), this.currents[index].encode());
    }
    PlayerPrefsEx.SetString("contact_list", str);
    PlayerPrefsEx.Save();
  }

  public void LoadCurrents()
  {
    string str = PlayerPrefsEx.GetString("contact_list");
    if (str == null)
      return;
    string[] strArray = str.Split(new char[1]{ ',' }, StringSplitOptions.RemoveEmptyEntries);
    int index = 0;
    for (int length = strArray.Length; index < length; ++index)
    {
      string data = PlayerPrefsEx.GetString(string.Format("{0}{1}", (object) "contact_current_", (object) strArray[index]));
      if (data != null)
      {
        PlayerInfo playerInfo = new PlayerInfo();
        playerInfo.decodeFromString(data);
        this.currents.Add(playerInfo);
      }
    }
  }
}
