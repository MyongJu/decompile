﻿// Decompiled with JetBrains decompiler
// Type: AllianceOverviewPage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UI;
using UnityEngine;

public class AllianceOverviewPage : MonoBehaviour
{
  public AllianceSymbol m_AllianceSymbol;
  public UILabel m_AllianceName;
  public UILabel m_AllianceLanguage;
  public UILabel m_AllianceLevel;
  public UILabel m_AllianceExpLabel;
  public UISlider m_AllianceExpSlider;
  public UILabel m_MemberCount;
  public UILabel m_Power;
  public UILabel m_HighlordName;
  public UILabel m_GiftLevel;
  public GameObject m_GiftBadge;
  public UILabel m_GiftLabel;
  public GameObject m_HelpBadge;
  public UILabel m_HelpLabel;
  public GameObject m_WarBadge;
  public UILabel m_WarLabel;
  public GameObject m_TechBadge;
  public UILabel m_TechLabel;
  public AllianceNewsfeed m_Newsfeed;
  public GameObject m_RightPanel;
  public UILabel m_AllianceNoticeLabel;
  public UIScrollView m_scrollView;
  public UIButton m_ChangeAnnouncementBtn;
  public RedDotTipItem _allianceChestTip;
  public GameObject _rootAllianceChestEffect;
  public GameObject _rootAllianceChestButton;

  public void ShowPage(System.Action callback)
  {
    if (this.gameObject.activeSelf)
      return;
    this.gameObject.SetActive(true);
    this.UpdatePageWithDefault();
    this.m_Newsfeed.Hide();
    this.m_RightPanel.SetActive(true);
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(postData, (System.Action<bool, object>) ((_param1, _param2) =>
    {
      try
      {
        callback();
        this.UpdatePage();
        AllianceManager.Instance.onHelpChanged += new System.Action(this.OnHelpChanged);
        DBManager.inst.DB_AllianceTech.onDataChanged += new System.Action<int>(this.OnTechDataChanged);
        DBManager.inst.DB_AllianceTech.onDataRemoved += new System.Action<int>(this.OnTechDataChanged);
        AllianceTreasuryCountManager.Instance.OnTreasuryCountChanged += new AllianceTreasuryCountManager.TreasuryCountChangedHandler(this.OnTreasuryCountChanged);
        DBManager.inst.DB_UserTreasureChest.onDataChanged += new System.Action<UserTreasureChestData>(this.OnUserTreasureChestDataChanged);
        AllianceUtilities.OnAllianceDevoteRewardTipsUpdate += new System.Action(this.OnDevoteRewardTipsUpdate);
      }
      catch
      {
      }
    }), true);
    this.UpdateAllianceChestTipCount();
  }

  protected void SetAllianceChestEffectEnabled(bool effectEnabled)
  {
    this._rootAllianceChestEffect.SetActive(effectEnabled);
    Animator component = this._rootAllianceChestButton.GetComponent<Animator>();
    if ((bool) ((UnityEngine.Object) component))
      component.enabled = effectEnabled;
    if (effectEnabled)
      return;
    this._rootAllianceChestButton.transform.localScale = Vector3.one;
  }

  protected void UpdateAllianceChestTipCount()
  {
    try
    {
      int tipCount = AllianceTreasuryCountManager.Instance.UnOpenedChestCount + DBManager.inst.DB_UserTreasureChest.GetAllCanSendUserChest(PlayerData.inst.uid);
      this._allianceChestTip.SetTipCount(tipCount);
      this.SetAllianceChestEffectEnabled(tipCount > 0);
    }
    catch
    {
    }
  }

  protected void OnTreasuryCountChanged(int unopenedChestCount)
  {
    this.UpdateAllianceChestTipCount();
  }

  protected void OnUserTreasureChestDataChanged(UserTreasureChestData data)
  {
    this.UpdateAllianceChestTipCount();
  }

  public void HidePage()
  {
    AllianceManager.Instance.onHelpChanged -= new System.Action(this.OnHelpChanged);
    DBManager.inst.DB_AllianceTech.onDataChanged -= new System.Action<int>(this.OnTechDataChanged);
    DBManager.inst.DB_AllianceTech.onDataRemoved -= new System.Action<int>(this.OnTechDataChanged);
    AllianceTreasuryCountManager.Instance.OnTreasuryCountChanged -= new AllianceTreasuryCountManager.TreasuryCountChangedHandler(this.OnTreasuryCountChanged);
    DBManager.inst.DB_UserTreasureChest.onDataChanged -= new System.Action<UserTreasureChestData>(this.OnUserTreasureChestDataChanged);
    AllianceUtilities.OnAllianceDevoteRewardTipsUpdate -= new System.Action(this.OnDevoteRewardTipsUpdate);
    this.gameObject.SetActive(false);
    this.m_Newsfeed.Hide();
    this.m_RightPanel.SetActive(true);
  }

  private void OnHelpChanged()
  {
    this.UpdatePage();
  }

  private void OnTechDataChanged(int techId)
  {
    this.UpdatePage();
  }

  private void OnDevoteRewardTipsUpdate()
  {
    this.UpdatePage();
  }

  private void UpdatePageWithDefault()
  {
    this.m_AllianceSymbol.gameObject.SetActive(false);
    this.m_AllianceName.text = string.Empty;
    this.m_AllianceLanguage.text = string.Empty;
    this.m_AllianceLevel.text = string.Empty;
    this.m_Power.text = string.Empty;
    this.m_MemberCount.text = string.Empty;
    this.m_GiftLevel.text = string.Empty;
    this.m_AllianceExpLabel.text = string.Empty;
    this.m_AllianceExpSlider.value = 0.0f;
    this.m_GiftBadge.SetActive(false);
    this.m_HighlordName.text = string.Empty;
    this.m_HelpBadge.SetActive(false);
    this.m_WarBadge.SetActive(false);
    this.m_WarLabel.text = string.Empty;
    this.m_TechBadge.SetActive(false);
    this.m_TechLabel.text = string.Empty;
    this.m_AllianceNoticeLabel.text = string.Empty;
    this.m_ChangeAnnouncementBtn.enabled = true;
  }

  private void InternalUpdatePage()
  {
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
    if (allianceData != null)
    {
      this.m_AllianceSymbol.gameObject.SetActive(true);
      this.m_AllianceSymbol.SetSymbols(allianceData.allianceSymbolCode);
      this.m_AllianceName.text = string.Format("[{0}] {1}", (object) allianceData.allianceAcronym, (object) allianceData.allianceName);
      this.m_AllianceLanguage.text = Language.Instance.GetLanguageName(allianceData.language);
      this.m_AllianceLevel.text = allianceData.allianceLevel.ToString();
      this.m_Power.text = Utils.FormatThousands(allianceData.power.ToString());
      this.m_MemberCount.text = string.Format("{0}/{1}", (object) allianceData.memberCount, (object) allianceData.memberMax);
      this.m_GiftLevel.text = ConfigManager.inst.DB_Gift_Level.GetLevelByPoints((int) allianceData.giftPoints).ToString();
      int experience = ConfigManager.inst.DB_Alliance_Level.GetDataByLevel(allianceData.allianceLevel).Experience;
      if (ConfigManager.inst.DB_Alliance_Level.MaxLevel > allianceData.allianceLevel)
      {
        int num1 = ConfigManager.inst.DB_Alliance_Level.GetDataByLevel(allianceData.allianceLevel + 1).Experience - experience;
        int num2 = allianceData.xp - experience;
        this.m_AllianceExpLabel.text = string.Format("{0} / {1}", (object) Utils.FormatThousands(num2.ToString()), (object) Utils.FormatThousands(num1.ToString()));
        this.m_AllianceExpSlider.value = (float) num2 / (float) num1;
      }
      else
      {
        this.m_AllianceExpLabel.text = ScriptLocalization.Get("dragon_max_level", true);
        this.m_AllianceExpSlider.value = 1f;
      }
      int count = DBManager.inst.DB_AllianceGift.Values.Count;
      this.m_GiftBadge.SetActive(count > 0);
      this.m_GiftLabel.text = count.ToString();
      UserData userData = DBManager.inst.DB_User.Get(allianceData.creatorId);
      if (userData != null)
        this.m_HighlordName.text = userData.userName;
      else
        D.warn((object) "Bad user data.");
      int validHelpCount = allianceData.helps.GetValidHelpCount();
      this.m_HelpBadge.SetActive(validHelpCount > 0);
      this.m_HelpLabel.text = validHelpCount.ToString();
      int allianceWarCount = GameEngine.Instance.marchSystem.allianceWarCount;
      this.m_WarBadge.SetActive(allianceWarCount > 0);
      this.m_WarLabel.text = allianceWarCount.ToString();
      this.m_AllianceNoticeLabel.text = allianceData.announcement;
      this.m_scrollView.ResetPosition();
      this.m_ChangeAnnouncementBtn.enabled = allianceData.members.Get(PlayerData.inst.uid).title >= 4;
      int num = 0;
      if (PlayerData.inst.isAllianceR4Member && DBManager.inst.DB_AllianceTech.readyResearchId != 0 && DBManager.inst.DB_AllianceTech.researchingId == 0)
        ++num;
      if (AllianceUtilities.IsDevoteRewardAvailable)
        ++num;
      if (num > 0)
      {
        this.m_TechBadge.SetActive(true);
        this.m_TechLabel.text = num.ToString();
      }
      else
      {
        this.m_TechBadge.SetActive(false);
        this.m_TechLabel.text = string.Empty;
      }
    }
    else
      D.warn((object) "Bad alliance data.");
  }

  public void UpdatePage()
  {
    try
    {
      this.InternalUpdatePage();
    }
    catch
    {
    }
  }

  public void OnExpandNewsfeed()
  {
    this.m_Newsfeed.Show();
    this.m_RightPanel.SetActive(false);
  }

  public void OnCollapseNewsfeed()
  {
    this.m_Newsfeed.Hide();
    this.m_RightPanel.SetActive(true);
    this.InternalUpdatePage();
  }

  public void OnChangeAnnoucementBtnClicked()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceChangePublicMessagePopup", (Popup.PopupParameter) new AllianceChangePublicMessagePopup.Parameter()
    {
      publicMessageChangeCallBack = new System.Action(this.UpdatePage)
    });
  }
}
