﻿// Decompiled with JetBrains decompiler
// Type: TypedShopItemUI
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UnityEngine;

public class TypedShopItemUI : MonoBehaviour
{
  public UILabel labelName;
  public UILabel labelOwnNum;
  public UIButton btnFree;
  public UIButton btnUse;
  public UIButton btnBuyAndUse;
  public UILabel labelDesc;
  public UILabel labelPrice;
  public UI2DSprite iconTexture;
  public System.Action onFreePressed;
  public System.Action<TypedShopItemUI> onUsePressed;
  public System.Action<TypedShopItemUI> onBuyAndUsePressed;
  public ShopStaticInfo info;
  public ConsumableItemData itemData;

  public void Clear()
  {
    this.onFreePressed = (System.Action) null;
    this.onUsePressed = (System.Action<TypedShopItemUI>) null;
    this.onBuyAndUsePressed = (System.Action<TypedShopItemUI>) null;
    this.info = (ShopStaticInfo) null;
    this.itemData = (ConsumableItemData) null;
  }

  public int Item_ID
  {
    get
    {
      if (this.info != null)
        return this.info.Item_InternalId;
      return this.itemData.internalId;
    }
  }

  public void Refresh()
  {
    try
    {
      this.SetNormal();
    }
    catch
    {
    }
  }

  public float ItemValue { get; private set; }

  public void SetNormal()
  {
    if (this.info == null && this.itemData == null)
      return;
    this.btnFree.gameObject.SetActive(false);
    int num = this.info == null ? this.itemData.internalId : this.info.Item_InternalId;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(num);
    this.ItemValue = itemStaticInfo.Value;
    int itemCount = ItemBag.Instance.GetItemCount(num);
    this.labelOwnNum.text = itemCount.ToString();
    this.labelName.text = ScriptLocalization.Get(itemStaticInfo.Loc_Name_Id, true);
    this.labelPrice.text = this.info == null ? string.Empty : ItemBag.Instance.GetShopItemPrice(this.info.ID).ToString() + string.Empty;
    if (itemCount > 0)
    {
      this.labelOwnNum.color = Color.white;
      this.btnBuyAndUse.gameObject.SetActive(false);
      this.btnUse.gameObject.SetActive(true);
    }
    else
    {
      this.labelOwnNum.color = Color.gray;
      this.btnUse.gameObject.SetActive(false);
      this.btnBuyAndUse.gameObject.SetActive(true);
    }
    BuilderFactory.Instance.HandyBuild((UIWidget) this.iconTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
  }

  public void SetFree(int seconds)
  {
    int num = seconds / 60;
    if ((UnityEngine.Object) this.labelDesc != (UnityEngine.Object) null)
      this.labelDesc.text = string.Format("free speed up under {0} min", (object) num);
    this.btnFree.gameObject.SetActive(true);
    this.btnUse.gameObject.SetActive(false);
    this.btnBuyAndUse.gameObject.SetActive(false);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.iconTexture, "Texture/ItemIcons/item_free_speedup", (System.Action<bool>) null, true, false, string.Empty);
  }

  public void UseFree()
  {
    if (this.onFreePressed == null)
      return;
    this.onFreePressed();
  }

  public void UseItem()
  {
    if (this.onUsePressed == null)
      return;
    this.onUsePressed(this);
  }

  public void BuyAndUseItem()
  {
    if (this.onBuyAndUsePressed == null)
      return;
    this.onBuyAndUsePressed(this);
  }
}
