﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossListDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceBossListDlg : UI.Dialog
{
  protected List<AllianceBossItem> m_allBossItems = new List<AllianceBossItem>();
  [SerializeField]
  protected GameObject m_bossItemTemplate;
  [SerializeField]
  protected UIGrid m_bossItemContainer;
  [SerializeField]
  protected UIScrollView m_scrollView;
  [SerializeField]
  protected UIButtonBar m_buttonBar;
  [SerializeField]
  protected UIButton m_buttonStart;
  [SerializeField]
  protected UILabel m_labelCanStartTip;
  [SerializeField]
  protected UILabel m_labelErrorTip;
  [SerializeField]
  protected UILabel m_labelTip;

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.SecondTick);
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.SecondTick);
  }

  protected void SecondTick(int delta)
  {
    this.UpdateStateUI();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_buttonBar.SelectedIndex = 0;
    this.m_buttonBar.OnSelectedHandler += new System.Action<int>(this.OnButtonBarClicked);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.m_buttonBar.OnSelectedHandler -= new System.Action<int>(this.OnButtonBarClicked);
  }

  private void UpdateUI()
  {
    this.UpdateBossList();
    this.UpdateStateUI();
  }

  protected static int SortByLevel(AllianceBossInfo a, AllianceBossInfo b)
  {
    return a.bossLevel.CompareTo(b.bossLevel);
  }

  protected void UpdateBossList()
  {
    this.ClearAllCurrentBossItems();
    int selectStage = this.m_buttonBar.SelectedIndex;
    List<AllianceBossInfo> all = ConfigManager.inst.DB_AllianceBoss.GetAllianceBossInfoList().FindAll((Predicate<AllianceBossInfo>) (p => p.stage == selectStage));
    all.Sort(new Comparison<AllianceBossInfo>(AllianceBossListDlg.SortByLevel));
    using (List<AllianceBossInfo>.Enumerator enumerator = all.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceBossInfo current = enumerator.Current;
        AllianceBossItem allianceBossItem = this.AddBossItem();
        allianceBossItem.OnSelect += new AllianceBossItem.OnSelectCallBack(this.OnBossItemSelect);
        allianceBossItem.SetBossInfo(current);
      }
    }
    this.SelectBossItem(0);
    this.m_bossItemContainer.repositionNow = true;
    this.m_bossItemContainer.Reposition();
    this.m_scrollView.movement = UIScrollView.Movement.Unrestricted;
    this.m_scrollView.ResetPosition();
    this.m_scrollView.currentMomentum = Vector3.zero;
    this.m_scrollView.movement = UIScrollView.Movement.Horizontal;
  }

  protected void UpdateStateUI()
  {
    if (DBManager.inst.DB_AlliancePortal.GetAlliancePortalData() == null)
    {
      D.error((object) "there is no alliance portal data");
    }
    else
    {
      bool flag = AllianceBuildUtils.GetAlliancePortalBossSummonState() == AlliancePortalData.BossSummonState.CAN_ACTIVATE;
      this.m_buttonStart.isEnabled = flag;
      this.m_labelErrorTip.gameObject.SetActive(!flag);
      this.m_labelCanStartTip.gameObject.SetActive(flag);
      this.m_labelErrorTip.text = ScriptLocalization.Get("alliance_portal_status_cooldown", true);
      this.m_labelCanStartTip.text = ScriptLocalization.Get("alliance_portal_status_ready", true);
      this.m_labelTip.text = AllianceBuildUtils.GetAllianceBossSummonDesc();
    }
  }

  protected void ClearAllCurrentBossItems()
  {
    using (List<AllianceBossItem>.Enumerator enumerator = this.m_allBossItems.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.m_allBossItems.Clear();
  }

  protected AllianceBossItem AddBossItem()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_bossItemTemplate);
    gameObject.transform.SetParent(this.m_bossItemContainer.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    AllianceBossItem component = gameObject.GetComponent<AllianceBossItem>();
    this.m_allBossItems.Add(component);
    return component;
  }

  protected void OnButtonBarClicked(int index)
  {
    this.UpdateUI();
  }

  protected void OnBossItemSelect(AllianceBossItem selectItem)
  {
    using (List<AllianceBossItem>.Enumerator enumerator = this.m_allBossItems.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceBossItem current = enumerator.Current;
        current.Select = (UnityEngine.Object) selectItem == (UnityEngine.Object) current;
      }
    }
  }

  protected void SelectBossItem(int itemIndex)
  {
    for (int index = 0; index < this.m_allBossItems.Count; ++index)
      this.m_allBossItems[index].Select = index == itemIndex;
  }

  protected AllianceBossItem GetCurrentSelectBossItem()
  {
    using (List<AllianceBossItem>.Enumerator enumerator = this.m_allBossItems.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceBossItem current = enumerator.Current;
        if (current.Select)
          return current;
      }
    }
    return (AllianceBossItem) null;
  }

  public void OnStartConfirmed()
  {
    AllianceBossItem currentSelectBossItem = this.GetCurrentSelectBossItem();
    if ((UnityEngine.Object) currentSelectBossItem == (UnityEngine.Object) null)
      return;
    AllianceBossInfo allianceBossInfo = currentSelectBossItem.getAllianceBossInfo();
    AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
    if (allianceBossInfo == null || alliancePortalData == null)
      return;
    Hashtable postData = new Hashtable();
    postData[(object) "boss_id"] = (object) allianceBossInfo.internalId;
    postData[(object) "building_id"] = (object) alliancePortalData.PortalId;
    MessageHub.inst.GetPortByAction("Alliance:summonBoss").SendRequest(postData, new System.Action<bool, object>(this.OnRequestSummonBossResult), true);
  }

  protected void OnRequestSummonBossResult(bool result, object data)
  {
    if (!result)
      return;
    this.OnCloseButtonPress();
    PVPController.RallyAllianceBoss();
  }

  public void OnButtonnStartClicked()
  {
    AllianceBossItem currentSelectBossItem = this.GetCurrentSelectBossItem();
    if ((UnityEngine.Object) currentSelectBossItem == (UnityEngine.Object) null)
      return;
    if (!Utils.IsR4Member() && !Utils.IsR5Member())
    {
      UIManager.inst.ShowConfirmationBox(ScriptLocalization.Get("alliance_portal_uppercase_name", true), ScriptLocalization.Get("alliance_portal_rank_not_enough_description", true), string.Empty, (string) null, ChooseConfirmationBox.ButtonState.NO_BUTTON, new System.Action(this.OnStartConfirmed), (System.Action) null, (System.Action) null);
    }
    else
    {
      AllianceBossInfo allianceBossInfo = currentSelectBossItem.getAllianceBossInfo();
      if (allianceBossInfo == null)
        return;
      UIManager.inst.ShowConfirmationBox(ScriptLocalization.Get("alliance_portal_uppercase_name", true), string.Format(ScriptLocalization.Get("alliance_portal_confirm_open_description", true), (object) string.Format("[ec9005]Lv.{0} {1}[-]", (object) allianceBossInfo.bossLevel, (object) ScriptLocalization.Get(allianceBossInfo.name, true))), ScriptLocalization.Get("alliance_portal_uppercase_open_portal", true), (string) null, ChooseConfirmationBox.ButtonState.OK_CENTER, new System.Action(this.OnStartConfirmed), (System.Action) null, (System.Action) null);
    }
  }
}
