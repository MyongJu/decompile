﻿// Decompiled with JetBrains decompiler
// Type: ItemBaseUse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;

public class ItemBaseUse
{
  protected string item_id;
  protected string shop_id;
  protected int item_internal;
  protected int shop_internal;
  protected ItemStaticInfo _info;
  protected ShopStaticInfo _shopInfo;

  public ItemBaseUse(int id, bool isShopItem = false)
  {
    this.item_internal = id;
    if (!isShopItem)
      return;
    this.shop_internal = id;
    this.item_internal = 0;
  }

  public ItemBaseUse(string id, bool isShopItem = false)
  {
    this.item_id = id;
    if (!isShopItem)
      return;
    this.shop_id = id;
    this.item_id = string.Empty;
  }

  public ItemStaticInfo Info
  {
    get
    {
      if (this._info == null)
      {
        if (this.item_internal > 0)
          this._info = ConfigManager.inst.DB_Items.GetItem(this.item_internal);
        if (!string.IsNullOrEmpty(this.item_id))
          this._info = ConfigManager.inst.DB_Items.GetItem(this.item_id);
        if (this._info == null && this.ShopInfo != null)
          this._info = ConfigManager.inst.DB_Items.GetItem(this.ShopInfo.Item_InternalId);
      }
      return this._info;
    }
  }

  public ShopStaticInfo ShopInfo
  {
    get
    {
      if (this._shopInfo == null)
      {
        if (!string.IsNullOrEmpty(this.shop_id))
          this._shopInfo = ConfigManager.inst.DB_Shop.GetShopData(this.shop_id);
        if (this.shop_internal > 0)
          this._shopInfo = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(this.shop_internal);
        if (this._shopInfo == null)
        {
          if (this.item_internal > 0)
            this._shopInfo = ConfigManager.inst.DB_Shop.GetShopInfoByItemInternalId(this.item_internal);
          if (!string.IsNullOrEmpty(this.item_id))
            this._shopInfo = ConfigManager.inst.DB_Shop.GetShopInfoByItemInternalId(ConfigManager.inst.DB_Items.GetItem(this.item_id).internalId);
        }
      }
      return this._shopInfo;
    }
  }

  public virtual void UseItem(System.Action<bool, object> resultHandler = null)
  {
    this.NormalUse(resultHandler);
  }

  protected void NormalUse(System.Action<bool, object> resultHandler = null)
  {
    switch (ItemBaseUse.Check(this.Info.internalId))
    {
      case ItemBaseUse.UseType.DirectlyUse:
        this.DirectlyUse(resultHandler);
        break;
      case ItemBaseUse.UseType.BatchUse:
        if (this.Count > 1)
        {
          UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) new ItemUseOrBuyPopup.Parameter()
          {
            item_id = this.Info.internalId
          });
          break;
        }
        ItemBag.Instance.UseItem(this.Info.internalId, 1, (Hashtable) null, resultHandler);
        break;
      default:
        this.CustomUse(resultHandler);
        break;
    }
  }

  protected void DirectlyUse(System.Action<bool, object> resultHandler = null)
  {
    ItemBag.Instance.UseItem(this.Info.internalId, 1, (Hashtable) null, resultHandler);
  }

  protected int Count
  {
    get
    {
      return ItemBag.Instance.GetItemCount(this.Info.internalId);
    }
  }

  protected virtual void CustomUse(System.Action<bool, object> resultHandler = null)
  {
  }

  public static ItemBaseUse Create(int id, bool isShopItem = false)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(id);
    if (isShopItem)
      itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(ConfigManager.inst.DB_Shop.GetShopDataByInternalId(id).Item_InternalId);
    ItemBag.ItemType type = itemStaticInfo.Type;
    switch (type)
    {
      case ItemBag.ItemType.treasure_map:
        return (ItemBaseUse) new TreasureMapUse(itemStaticInfo.internalId, false);
      case ItemBag.ItemType.dragon_knight_rename:
        return (ItemBaseUse) new DragonKnightRenameUse(itemStaticInfo.internalId, false);
      case ItemBag.ItemType.item_postman:
        return (ItemBaseUse) new ItemPostManUse(itemStaticInfo.internalId, false);
      case ItemBag.ItemType.fortress_teleport:
        return (ItemBaseUse) new FortressTeleportUse(itemStaticInfo.internalId, false);
      case ItemBag.ItemType.equipment_gem:
        return (ItemBaseUse) new ItemGemUse(itemStaticInfo.internalId, false);
      case ItemBag.ItemType.parliament_hero_card:
        return (ItemBaseUse) new HeroCardUse(itemStaticInfo.internalId, false);
      case ItemBag.ItemType.altar_summon:
        return (ItemBaseUse) new ItemAltarSummonUse(itemStaticInfo.internalId, false);
      case ItemBag.ItemType.lord_title:
        return (ItemBaseUse) new ItemLordTitleUse(itemStaticInfo.internalId, false);
      default:
        switch (type - 51)
        {
          case ItemBag.ItemType.Invalid:
          case ItemBag.ItemType.refresh:
          case ItemBag.ItemType.heroxp:
label_8:
            return (ItemBaseUse) new CityBuffUse(itemStaticInfo.internalId, false);
          case ItemBag.ItemType.attack:
            return (ItemBaseUse) new PlayerRenameUse(itemStaticInfo.internalId, false);
          case ItemBag.ItemType.defense:
            return (ItemBaseUse) new AllianceRenameUse(itemStaticInfo.internalId, false);
          case ItemBag.ItemType.beginnerteleport:
            return (ItemBaseUse) new DragonRenameUse(itemStaticInfo.internalId, false);
          default:
            switch (type - 76)
            {
              case ItemBag.ItemType.Invalid:
                return (ItemBaseUse) new MegaphoneUse(itemStaticInfo.internalId, false);
              case ItemBag.ItemType.boost:
                return (ItemBaseUse) new WorldBossUse(itemStaticInfo.internalId, false);
              case ItemBag.ItemType.marchspeed:
                return (ItemBaseUse) new CityDecorationUse(itemStaticInfo.internalId, false);
              default:
                switch (type - 6)
                {
                  case ItemBag.ItemType.Invalid:
                  case ItemBag.ItemType.refresh:
                  case ItemBag.ItemType.speedup:
                    goto label_8;
                  case ItemBag.ItemType.boost:
                    return (ItemBaseUse) new RandomTeleportUse(itemStaticInfo.internalId, false);
                  default:
                    switch (type - 67)
                    {
                      case ItemBag.ItemType.Invalid:
                        return (ItemBaseUse) new WishWellGemUse(itemStaticInfo.internalId, false);
                      case ItemBag.ItemType.speedup:
                        return (ItemBaseUse) new AllianceTeleportUse(itemStaticInfo.internalId, false);
                      default:
                        if (type != ItemBag.ItemType.upkeepreduction)
                          return new ItemBaseUse(itemStaticInfo.internalId, false);
                        goto label_8;
                    }
                }
            }
        }
    }
  }

  public static ItemBaseUse.UseType Check(int item_id)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(item_id);
    ItemBaseUse.UseType useType = ItemBaseUse.UseType.NotUse;
    if (!itemStaticInfo.CanUse)
      return useType;
    ItemBag.ItemType type = itemStaticInfo.Type;
    switch (type)
    {
      case ItemBag.ItemType.upkeepreduction:
      case ItemBag.ItemType.peaceshield:
      case ItemBag.ItemType.antiscout:
      case ItemBag.ItemType.gatherspeed:
      case ItemBag.ItemType.player_rename:
      case ItemBag.ItemType.alliance_rename:
      case ItemBag.ItemType.dragonrename:
      case ItemBag.ItemType.wish_well_gem:
      case ItemBag.ItemType.allianceteleport:
      case ItemBag.ItemType.megaphone:
      case ItemBag.ItemType.boss_summoner:
      case ItemBag.ItemType.castle_display:
      case ItemBag.ItemType.treasure_map:
      case ItemBag.ItemType.dragon_knight_rename:
      case ItemBag.ItemType.item_postman:
      case ItemBag.ItemType.fortress_teleport:
      case ItemBag.ItemType.parliament_hero_card:
      case ItemBag.ItemType.altar_summon:
      case ItemBag.ItemType.lord_title:
label_4:
        useType = ItemBaseUse.UseType.Custom;
        break;
      case ItemBag.ItemType.energy:
      case ItemBag.ItemType.vippoint:
      case ItemBag.ItemType.lordexp:
      case ItemBag.ItemType.dragonexp:
      case ItemBag.ItemType.dragondark:
      case ItemBag.ItemType.dragonlight:
      case ItemBag.ItemType.gold:
      case ItemBag.ItemType.buildqueue:
      case ItemBag.ItemType.steel:
      case ItemBag.ItemType.casino1:
      case ItemBag.ItemType.casino2:
      case ItemBag.ItemType.alliance_boss_energy:
      case ItemBag.ItemType.dragon_knight_xp:
      case ItemBag.ItemType.buytroop:
      case ItemBag.ItemType.equipment_gem:
label_6:
        useType = string.IsNullOrEmpty(itemStaticInfo.CDGroup) ? ItemBaseUse.UseType.BatchUse : ItemBaseUse.UseType.DirectlyUse;
        break;
      default:
        switch (type - 6)
        {
          case ItemBag.ItemType.Invalid:
          case ItemBag.ItemType.refresh:
          case ItemBag.ItemType.speedup:
          case ItemBag.ItemType.boost:
            goto label_4;
          case ItemBag.ItemType.marchsize:
          case ItemBag.ItemType.attack:
          case ItemBag.ItemType.defense:
          case ItemBag.ItemType.beginnerteleport:
          case ItemBag.ItemType.randomteleport:
          case ItemBag.ItemType.targetteleport:
            goto label_6;
          case ItemBag.ItemType.food:
            useType = ItemBaseUse.UseType.DirectlyUse;
            break;
        }
    }
    return useType;
  }

  public enum UseType
  {
    NotUse,
    DirectlyUse,
    BatchUse,
    Custom,
  }
}
