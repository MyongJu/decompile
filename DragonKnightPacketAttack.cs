﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightPacketAttack
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DragonKnightPacketAttack : DragonKnightPacket
{
  public long RoundGuid;

  public DragonKnightPacketAttack(long roundGuid, long playerId)
  {
    this.Type = DragonKnightPacket.PacketType.Attack;
    this.PlayerId = playerId;
    this.RoundGuid = roundGuid;
  }
}
