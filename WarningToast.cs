﻿// Decompiled with JetBrains decompiler
// Type: WarningToast
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class WarningToast : MonoBehaviour, IRecycle
{
  public UILabel content;
  public float height;
  public float durion;

  private void Start()
  {
  }

  private void Update()
  {
  }

  [DebuggerHidden]
  public IEnumerator Animation()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new WarningToast.\u003CAnimation\u003Ec__IteratorA2()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void OnFinalize()
  {
    this.GetComponent<UIPanel>().alpha = 1f;
    this.transform.localPosition = Vector3.zero;
  }

  public void OnInitialize()
  {
  }
}
