﻿// Decompiled with JetBrains decompiler
// Type: FestivalInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class FestivalInfo
{
  public int[] innerItems = new int[4];
  public const int INNER_ITEM_MAX_COUNT = 4;
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "festival_type")]
  public string type;
  [Config(Name = "image_big")]
  public string outImage;
  [Config(Name = "image_icon")]
  public string outIcon;
  [Config(Name = "entry_title")]
  public string outTitle;
  [Config(Name = "entry_description")]
  public string outDescription;
  [Config(Name = "exchange_group_id")]
  public int exchange_group_id;
  [Config(Name = "event_drop_out")]
  public string innerTitleId;
  [Config(Name = "event_background")]
  public string innerDescriptionId;
  [Config(Name = "reward_id_1")]
  public int innerItemId1;
  [Config(Name = "reward_id_2")]
  public int innerItemId2;
  [Config(Name = "reward_id_3")]
  public int innerItemId3;
  [Config(Name = "reward_id_4")]
  public int innerItemId4;

  public string outImagePath
  {
    get
    {
      return string.Format("Texture/Events/{0}", (object) this.outImage);
    }
  }

  public string outIconPath
  {
    get
    {
      return string.Format("Texture/Events/{0}", (object) this.outIcon);
    }
  }

  public string Loc_outTitle
  {
    get
    {
      return Utils.XLAT(this.outTitle);
    }
  }

  public string Loc_outDescription
  {
    get
    {
      return Utils.XLAT(this.outDescription);
    }
  }
}
