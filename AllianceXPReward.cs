﻿// Decompiled with JetBrains decompiler
// Type: AllianceXPReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class AllianceXPReward : QuestReward
{
  public readonly int exp;

  public AllianceXPReward(int inExp)
  {
    this.exp = inExp;
  }

  public int activeValue
  {
    get
    {
      return this.exp;
    }
  }

  public override int GetValue()
  {
    return this.exp;
  }

  public override void Claim()
  {
    base.Claim();
  }

  public override string GetRewardIconName()
  {
    return Utils.GetUITextPath() + "alliancequest_reward_exp";
  }

  public override string GetRewardTypeName()
  {
    return ScriptLocalization.Get("xp_name", true);
  }

  public override string GetValueText()
  {
    return this.activeValue.ToString();
  }
}
