﻿// Decompiled with JetBrains decompiler
// Type: ScoutReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ScoutReportPopup : BaseReportPopup
{
  private const string FILTER_CATEGORY = "battle";
  public UITexture titleImage;
  public UITable contentTable;
  public GameObject container;
  private Dictionary<string, BenefitComparisonElement> researchElements;
  private Dictionary<string, BenefitComparisonElement> equipmentElements;
  private Dictionary<string, BenefitComparisonElement> talentElements;
  private Dictionary<string, BenefitComparisonElement> buildingElements;
  public GameObject subTitle;
  public GameObject enemy;
  public GameObject resource;
  public GameObject troopCount;
  public GameObject tier;
  public GameObject reinforceTroop;
  public GameObject research;
  public GameObject equipment;
  public GameObject building;
  public GameObject talent;
  public GameObject oneButton;
  private ScoutBenefitComparisonPopup.EnemyBaseInfo enemyBaseInfo;
  private ScoutBenefitComparisonPopup.TypeStatus typeStatus;
  public GameObject oneLabel;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.SetupComparisonDatas();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    BuilderFactory.Instance.Release((UIWidget) this.titleImage);
  }

  public override void OnShareBtnClicked()
  {
    base.OnShareBtnClicked();
    if (!this.canShare)
      return;
    Hashtable hashtable = this.param.hashtable[(object) "default"] as Hashtable;
    string str = (string) null;
    if (hashtable.Contains((object) "acronym"))
      str = "[" + hashtable[(object) "acronym"].ToString() + "]";
    string targetName = str;
    if (hashtable.Contains((object) "user_name"))
      targetName += hashtable[(object) "user_name"].ToString();
    ChatMessageManager.SendWarReport_Scout(this.maildata.mailID, PlayerData.inst.uid, targetName);
  }

  private void SetupComparisonDatas()
  {
    this.researchElements = BenefitComparisonHelper.SetupResearchBenefits("battle");
    this.equipmentElements = BenefitComparisonHelper.SetupEquipmentBenefits("battle");
    this.talentElements = BenefitComparisonHelper.SetupTalentBenefits("battle");
    this.buildingElements = BenefitComparisonHelper.SetupBuildingBenefits("battle");
    this.typeStatus = new ScoutBenefitComparisonPopup.TypeStatus();
    if (this.param.hashtable == null || !this.param.hashtable.ContainsKey((object) "type_status"))
      return;
    Hashtable hashtable = this.param.hashtable[(object) "type_status"] as Hashtable;
    if (hashtable == null)
      return;
    if (hashtable.ContainsKey((object) "lord_benefits"))
      int.TryParse(hashtable[(object) "lord_benefits"] as string, out this.typeStatus.talentStatus);
    if (hashtable.ContainsKey((object) "equipment_benefits"))
      int.TryParse(hashtable[(object) "equipment_benefits"] as string, out this.typeStatus.equipmentStatus);
    if (hashtable.ContainsKey((object) "research_benefits"))
      int.TryParse(hashtable[(object) "research_benefits"] as string, out this.typeStatus.researchStatus);
    if (!hashtable.ContainsKey((object) "building_benefits"))
      return;
    int.TryParse(hashtable[(object) "building_benefits"] as string, out this.typeStatus.buildingStatus);
  }

  private void UpdateUI()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.titleImage, "Texture/GUI_Textures/mail_report_scoute", (System.Action<bool>) null, true, false, string.Empty);
    this.DrawEnemyOverView();
    this.DrawResource();
    this.DrawTroops();
    this.DrawResearch();
    this.DrawTalent();
    this.DrawEquipment();
    this.DrawBuilding();
    if (!this.DrawFailure())
      this.AddCompareButton();
    this.container.GetComponent<UITable>().Reposition();
    if (!this.param.hashtable.ContainsKey((object) "fail") || !((UnityEngine.Object) null != (UnityEngine.Object) this.shareIcon))
      return;
    this.shareIcon.SetActive(false);
  }

  protected override void UpdateIconState()
  {
    base.UpdateIconState();
    if (!this.param.hashtable.ContainsKey((object) "fail") || !((UnityEngine.Object) null != (UnityEngine.Object) this.shareIcon))
      return;
    this.shareIcon.SetActive(false);
  }

  private void DrawEnemyOverView()
  {
    if (!this.param.hashtable.ContainsKey((object) "default"))
      return;
    this.AddSubtitle("mail_scout_report_uppercase_enemy_overview_subtitle");
    Hashtable hashtable = this.param.hashtable[(object) "default"] as Hashtable;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    GameObject gameObject = NGUITools.AddChild(parent, this.enemy);
    DefaultPlayerComponent component = gameObject.transform.Find("enemyUser").GetComponent<DefaultPlayerComponent>();
    if (this.param.hashtable.ContainsKey((object) "lord"))
      component.lv = "Lv." + (this.param.hashtable[(object) "lord"] as Hashtable)[(object) "level"].ToString();
    component.cityDefence = !hashtable.ContainsKey((object) "city_defense") ? string.Empty : Utils.XLAT("id_city_defense") + ": " + hashtable[(object) "city_defense"].ToString();
    component.FeedData<DefaultPlayerComponentData>(this.param.hashtable[(object) "default"] as Hashtable);
    if (this.param.hashtable.ContainsKey((object) "dragon"))
      gameObject.transform.Find("enmeyDragon").GetComponent<DragonWithSkillComponent>().FeedData<DragonWithSkillComponentData>(this.param.hashtable[(object) "dragon"] as Hashtable);
    else
      gameObject.transform.Find("enmeyDragon").gameObject.SetActive(false);
    parent.GetComponent<TableContainer>().ResetPosition();
    string empty = string.Empty;
    if (hashtable.ContainsKey((object) "world_id") && hashtable[(object) "world_id"] != null)
      empty = hashtable[(object) "world_id"].ToString();
    DefaultPlayerComponentData playerData = component.PlayerData;
    if (playerData == null)
      return;
    this.enemyBaseInfo = new ScoutBenefitComparisonPopup.EnemyBaseInfo();
    this.enemyBaseInfo.portrait = playerData.portrait;
    this.enemyBaseInfo.icon = playerData.icon;
    this.enemyBaseInfo.lordTitleId = playerData.lordTitleId;
    this.enemyBaseInfo.name = string.Format("{0}{1}", !string.IsNullOrEmpty(playerData.acronym) ? (object) ("(" + playerData.acronym + ")") : (object) string.Empty, (object) playerData.user_name);
    if (this.maildata.IsInAllianceWar() && !string.IsNullOrEmpty(empty))
      this.enemyBaseInfo.name = string.Format("{0}.k{1}", (object) this.enemyBaseInfo.name, (object) empty);
    component.playerLabel.text = this.enemyBaseInfo.name;
  }

  private void DrawResource()
  {
    if (!this.param.hashtable.ContainsKey((object) "resource"))
      return;
    this.AddSubtitle("mail_scout_report_resources_subtitle");
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    NGUITools.AddChild(parent, this.resource).GetComponentInChildren<ResourceWithUnCollectComponent>().FeedData<ResourceWithUnCollectComponentData>(this.param.hashtable[(object) "resource"] as Hashtable);
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawTroops()
  {
    if (!this.param.hashtable.ContainsKey((object) "troops"))
      return;
    this.DrawDefendingTroop(this.param.hashtable[(object) "troops"] as Hashtable);
    this.DrawReinforceTroop(this.param.hashtable[(object) "troops"] as Hashtable);
    this.DrawTrap(this.param.hashtable[(object) "troops"] as Hashtable);
  }

  private void DrawDefendingTroop(Hashtable data)
  {
    if (!data.ContainsKey((object) "defending_troops"))
      return;
    this.AddSubtitle("id_uppercase_troops");
    Hashtable hashtable1 = data[(object) "defending_troops"] as Hashtable;
    this.AddUnitCount(this.contentTable.gameObject, hashtable1[(object) "amount"].ToString(), Utils.XLAT("id_total_troops"));
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    Hashtable hashtable2 = hashtable1[(object) "tier"] as Hashtable;
    if (hashtable2 != null)
    {
      List<string> stringList = new List<string>((IEnumerable<string>) new ArrayList(hashtable2.Keys).ToArray(typeof (string)));
      stringList.Sort((Comparison<string>) ((x, y) => int.Parse(x.Substring(1)).CompareTo(int.Parse(y.Substring(1)))));
      for (int index = stringList.Count - 1; index >= 0; --index)
        NGUITools.AddChild(parent, this.tier).GetComponent<TierListComponent>().FeedData((IComponentData) new TierComponentData(stringList[index].ToString(), hashtable2[(object) stringList[index]] as Hashtable));
    }
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawTrap(Hashtable data)
  {
    if (!data.ContainsKey((object) "traps"))
      return;
    this.AddSubtitle("mail_scout_report_uppercase_traps_subtitle");
    Hashtable hashtable1 = data[(object) "traps"] as Hashtable;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    this.AddUnitCount(parent, hashtable1[(object) "amount"].ToString(), Utils.XLAT("mail_scout_report_total_traps"));
    Hashtable hashtable2 = hashtable1[(object) "tier"] as Hashtable;
    if (hashtable2 != null)
    {
      List<string> stringList = new List<string>((IEnumerable<string>) new ArrayList(hashtable2.Keys).ToArray(typeof (string)));
      stringList.Sort((Comparison<string>) ((x, y) => int.Parse(x.Substring(1)).CompareTo(int.Parse(y.Substring(1)))));
      for (int index = stringList.Count - 1; index >= 0; --index)
        NGUITools.AddChild(parent, this.tier).GetComponent<TierListComponent>().FeedData((IComponentData) new TierComponentData(stringList[index].ToString(), hashtable2[(object) stringList[index]] as Hashtable));
    }
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawReinforceTroop(Hashtable data)
  {
    if (!data.ContainsKey((object) "reinforcing_troops"))
      return;
    this.AddSubtitle(Utils.XLAT("mail_scout_report_uppercase_reinforcements_subtitle"));
    Hashtable hashtable1 = data[(object) "reinforcing_troops"] as Hashtable;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    this.AddUnitCount(parent, hashtable1[(object) "amount"].ToString(), Utils.XLAT("mail_scout_report_total_troops"));
    foreach (DictionaryEntry dictionaryEntry in hashtable1)
    {
      if (dictionaryEntry.Key.ToString() != "amount" && (dictionaryEntry.Value as Hashtable).ContainsKey((object) "troops"))
      {
        Hashtable hashtable2 = ((dictionaryEntry.Value as Hashtable)[(object) "troops"] as Hashtable)[(object) "tier"] as Hashtable;
        List<TierComponentData> tiers = new List<TierComponentData>();
        if (hashtable2 != null)
        {
          List<string> stringList = new List<string>((IEnumerable<string>) new ArrayList(hashtable2.Keys).ToArray(typeof (string)));
          stringList.Sort((Comparison<string>) ((x, y) => int.Parse(x.Substring(1)).CompareTo(int.Parse(y.Substring(1)))));
          for (int index = stringList.Count - 1; index >= 0; --index)
          {
            TierComponentData tierComponentData = new TierComponentData(stringList[index], hashtable2[(object) stringList[index]] as Hashtable);
            tiers.Add(tierComponentData);
          }
        }
        ReinforcePlayerWithTierComponent component = NGUITools.AddChild(this.contentTable.gameObject, this.reinforceTroop).GetComponent<ReinforcePlayerWithTierComponent>();
        component.parent = parent;
        component.FeedData((IComponentData) new ReinforcePlayerWithTierComponentData(tiers, dictionaryEntry.Value as Hashtable));
      }
    }
    parent.GetComponent<TableContainer>().ResetPosition();
  }

  private void DrawResearch()
  {
    if (!this.param.hashtable.ContainsKey((object) "research_benefits"))
      return;
    this.AddSubtitle("id_uppercase_research");
    Hashtable table = this.param.hashtable[(object) "research_benefits"] as Hashtable;
    ResearchComponent component = NGUITools.AddChild(this.contentTable.gameObject, this.research).GetComponent<ResearchComponent>();
    component.parent = this.contentTable.gameObject;
    component.FeedData((IComponentData) new ResearchComponentData(table));
  }

  private void DrawEquipment()
  {
    if (!this.param.hashtable.ContainsKey((object) "equipment_benefits"))
      return;
    this.AddSubtitle("id_uppercase_equipment");
    Hashtable table = this.param.hashtable[(object) "equipment_benefits"] as Hashtable;
    ScoutMailEquipmentComponent component = NGUITools.AddChild(this.contentTable.gameObject, this.equipment).GetComponent<ScoutMailEquipmentComponent>();
    component.parent = this.contentTable.gameObject;
    component.FeedData((IComponentData) new ScoutMailEquipmentComponentData(table));
  }

  private void DrawBuilding()
  {
    if (!this.param.hashtable.ContainsKey((object) "building_benefits"))
      return;
    this.AddSubtitle("mail_scout_report_uppercase_buildings_subtitle");
    Hashtable table = this.param.hashtable[(object) "building_benefits"] as Hashtable;
    BuildingComponent component = NGUITools.AddChild(this.contentTable.gameObject, this.building).GetComponent<BuildingComponent>();
    component.parent = this.contentTable.gameObject;
    component.FeedData((IComponentData) new BuildingComponentData(table));
  }

  private void DrawTalent()
  {
    if (!this.param.hashtable.ContainsKey((object) "lord_benefits"))
      return;
    this.AddSubtitle("id_uppercase_lord_talents");
    Hashtable table = this.param.hashtable[(object) "lord_benefits"] as Hashtable;
    TalentComponent component = NGUITools.AddChild(this.contentTable.gameObject, this.talent).GetComponent<TalentComponent>();
    component.parent = this.contentTable.gameObject;
    component.FeedData((IComponentData) new TalentComponentData(table));
  }

  private void AddCompareButton()
  {
    UIButton componentInChildren = NGUITools.AddChild(this.contentTable.gameObject, this.oneButton).GetComponentInChildren<UIButton>();
    EventDelegate.Add(componentInChildren.onClick, new EventDelegate.Callback(this.OnCompareButtonClicked));
    if (this.CanShowBenefitsComparison())
    {
      componentInChildren.isEnabled = true;
      this.UpdateBenefitComparisonDatas();
    }
    else
      componentInChildren.isEnabled = false;
  }

  private void UpdateBenefitComparisonDatas()
  {
    this.UpdateBenefitComparisonDatas(this.researchElements, "research_benefits");
    this.UpdateBenefitComparisonDatas(this.equipmentElements, "equipment_benefits");
    this.UpdateBenefitComparisonDatas(this.talentElements, "lord_benefits");
    this.UpdateBenefitComparisonDatas(this.buildingElements, "building_benefits");
  }

  private void UpdateBenefitComparisonDatas(Dictionary<string, BenefitComparisonElement> dict, string keyName)
  {
    if (dict == null)
      dict = new Dictionary<string, BenefitComparisonElement>();
    if (!this.param.hashtable.ContainsKey((object) keyName))
      return;
    Hashtable hashtable = this.param.hashtable[(object) keyName] as Hashtable;
    if (hashtable == null)
      return;
    foreach (DictionaryEntry dictionaryEntry in hashtable)
    {
      string str = dictionaryEntry.Key.ToString();
      string longId = DBManager.inst.DB_Local_Benefit.GetLongID(str);
      float num = float.Parse(dictionaryEntry.Value.ToString());
      if (dict.ContainsKey(longId))
      {
        dict[longId].value2 = num;
      }
      else
      {
        BenefitComparisonElement comparisonElement = new BenefitComparisonElement(longId, 0.0f, num);
        dict.Add(str, comparisonElement);
      }
    }
  }

  private bool CanShowBenefitsComparison()
  {
    ScoutReportPopup.Parameter parameter = this.param as ScoutReportPopup.Parameter;
    return (parameter.fromType != FromType.Share || parameter.isSlef) && (this.researchElements != null && this.researchElements.Count > 0 && this.typeStatus.researchStatus != 1 || this.param.hashtable.ContainsKey((object) "research_benefits") || (this.equipmentElements != null && this.equipmentElements.Count > 0 && this.typeStatus.equipmentStatus != 1 || this.param.hashtable.ContainsKey((object) "equipment_benefits")) || (this.talentElements != null && this.talentElements.Count > 0 && this.typeStatus.talentStatus != 1 || this.param.hashtable.ContainsKey((object) "lord_benefits") || (this.buildingElements != null && this.buildingElements.Count > 0 && this.typeStatus.buildingStatus != 1 || this.param.hashtable.ContainsKey((object) "building_benefits"))));
  }

  public void OnCompareButtonClicked()
  {
    UIManager.inst.OpenPopup("ScoutBenefitComparisonPopup", (Popup.PopupParameter) new ScoutBenefitComparisonPopup.Parameter()
    {
      typeStatus = this.typeStatus,
      enemyBaseInfo = this.enemyBaseInfo,
      reseachElements = this.researchElements,
      equipmentElements = this.equipmentElements,
      talentElements = this.talentElements,
      buildingElements = this.buildingElements
    });
  }

  private bool DrawFailure()
  {
    if (!this.param.hashtable.ContainsKey((object) "fail"))
      return false;
    GameObject parent = NGUITools.AddChild(this.contentTable.gameObject, this.container);
    NGUITools.AddChild(parent, this.oneLabel).GetComponentInChildren<UILabel>().text = Utils.XLAT("scout_fail_reason_" + this.param.hashtable[(object) "fail"].ToString());
    parent.GetComponent<TableContainer>().ResetPosition();
    return true;
  }

  private void AddSubtitle(string titleKey)
  {
    NGUITools.AddChild(this.contentTable.gameObject, this.subTitle).GetComponentInChildren<UILabel>().text = Utils.XLAT(titleKey);
  }

  private void AddUnitCount(GameObject parent, string count, string name)
  {
    GameObject gameObject = NGUITools.AddChild(parent, this.troopCount);
    gameObject.transform.Find("valueLabel").GetComponent<UILabel>().text = count;
    gameObject.transform.Find("nameLabel").GetComponent<UILabel>().text = name;
  }

  public class Parameter : BaseReportPopup.Parameter
  {
    public FromType fromType;
    public bool isSlef;
  }
}
