﻿// Decompiled with JetBrains decompiler
// Type: IState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public interface IState
{
  string Key { get; }

  Hashtable Data { get; set; }

  void OnEnter();

  void OnProcess();

  void OnExit();

  void Dispose();
}
