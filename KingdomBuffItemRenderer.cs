﻿// Decompiled with JetBrains decompiler
// Type: KingdomBuffItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class KingdomBuffItemRenderer : MonoBehaviour
{
  private Dictionary<int, KingdomBuffSubItemRenderer> _itemDict = new Dictionary<int, KingdomBuffSubItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  public System.Action onKingdomBuffFinished;
  private KingdomBuffPayload.KingdomBuffType _buffType;

  public void SetData(KingdomBuffPayload.KingdomBuffType buffType)
  {
    this._buffType = buffType;
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    List<KingdomBuffPayload.KingdomBuffData> all = KingdomBuffPayload.Instance.KingdomBuffDataList.FindAll((Predicate<KingdomBuffPayload.KingdomBuffData>) (x => x.buffType == this._buffType));
    if (this._buffType != KingdomBuffPayload.KingdomBuffType.KINGDOM)
      NGUITools.SetActive(this.gameObject, all != null && all.Count > 0);
    this.ClearData();
    if (all != null)
    {
      if (this._buffType == KingdomBuffPayload.KingdomBuffType.KINGDOM)
        all.Sort((Comparison<KingdomBuffPayload.KingdomBuffData>) ((a, b) => a.startTime.CompareTo(b.startTime)));
      else
        all.Sort((Comparison<KingdomBuffPayload.KingdomBuffData>) ((a, b) => a.buffId.CompareTo(b.buffId)));
      for (int key = 0; key < all.Count; ++key)
      {
        KingdomBuffSubItemRenderer itemRenderer = this.CreateItemRenderer(all[key]);
        this._itemDict.Add(key, itemRenderer);
      }
    }
    this.Reposition();
  }

  public void ClearData()
  {
    using (Dictionary<int, KingdomBuffSubItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, KingdomBuffSubItemRenderer> current = enumerator.Current;
        current.Value.ClearData();
        current.Value.onKingdomBuffFinished -= new System.Action(this.OnKingdomBuffFinished);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
  }

  private KingdomBuffSubItemRenderer CreateItemRenderer(KingdomBuffPayload.KingdomBuffData buffData)
  {
    GameObject gameObject = this._itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    KingdomBuffSubItemRenderer component = gameObject.GetComponent<KingdomBuffSubItemRenderer>();
    component.SetData(buffData);
    component.onKingdomBuffFinished += new System.Action(this.OnKingdomBuffFinished);
    return component;
  }

  private void OnKingdomBuffFinished()
  {
    if (this.onKingdomBuffFinished == null)
      return;
    this.onKingdomBuffFinished();
  }
}
