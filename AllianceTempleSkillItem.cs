﻿// Decompiled with JetBrains decompiler
// Type: AllianceTempleSkillItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class AllianceTempleSkillItem : MonoBehaviour
{
  [SerializeField]
  protected Color m_colorEnabled;
  [SerializeField]
  protected Color m_colorDisabled;
  [SerializeField]
  protected Color m_colorCostNotEnough;
  [SerializeField]
  protected GameObject m_lockRoot;
  [SerializeField]
  protected UITexture m_textureIcon;
  [SerializeField]
  protected UILabel m_labelMonaCost;
  [SerializeField]
  protected GameObject m_cdRoot;
  [SerializeField]
  protected GameObject m_costRoot;
  [SerializeField]
  protected UILabel m_labelCdTime;
  [SerializeField]
  protected UISprite m_textureMana;
  protected TempleSkillInfo m_templeSkillInfo;
  protected bool m_spriteAlreadySetted;

  protected int CurrentManaPoint
  {
    get
    {
      AllianceData allianceData = PlayerData.inst.allianceData;
      if (allianceData != null)
        return allianceData.manaPoint;
      return 0;
    }
  }

  public void SetTempleSkillInfo(TempleSkillInfo templeSkillInfo)
  {
    this.m_templeSkillInfo = templeSkillInfo;
    this.UpdateUI();
  }

  public void UpdateUI()
  {
    this.gameObject.SetActive(this.m_templeSkillInfo != null);
    if (this.m_templeSkillInfo == null)
      return;
    if (!this.m_spriteAlreadySetted)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_textureIcon, this.m_templeSkillInfo.IconPath, (System.Action<bool>) null, true, false, string.Empty);
      this.m_spriteAlreadySetted = true;
    }
    this.m_labelMonaCost.text = this.m_templeSkillInfo.spend.ToString();
    bool flag = Utils.GetPlayerAllianceTempleLevel() >= this.m_templeSkillInfo.UnlockLevel;
    this.m_lockRoot.SetActive(!flag);
    if (flag)
    {
      GreyUtility.Normal(this.m_textureIcon.gameObject);
      GreyUtility.Normal(this.m_textureMana.gameObject);
    }
    else
    {
      GreyUtility.Grey(this.m_textureIcon.gameObject);
      GreyUtility.Grey(this.m_textureMana.gameObject);
    }
    int skillCdTime = DBManager.inst.DB_AllianceMagic.GetSkillCdTime(PlayerData.inst.allianceId, this.m_templeSkillInfo.internalId);
    if (skillCdTime <= 0)
    {
      this.m_cdRoot.SetActive(false);
      this.m_costRoot.SetActive(true);
      if (flag)
        this.m_labelMonaCost.color = this.m_templeSkillInfo.spend > this.CurrentManaPoint ? this.m_colorCostNotEnough : this.m_colorEnabled;
      else
        this.m_labelMonaCost.color = this.m_colorDisabled;
    }
    else
    {
      this.m_cdRoot.SetActive(true);
      this.m_costRoot.SetActive(false);
      this.m_labelCdTime.text = Utils.FormatTime(skillCdTime, false, false, true);
    }
  }

  public void OnClicked()
  {
    UIManager.inst.OpenPopup("Alliance/DragonAltarSkillInfoPopup", (Popup.PopupParameter) new SkillInfoPopup.Parameter()
    {
      itemId = this.m_templeSkillInfo.internalId
    });
  }
}
