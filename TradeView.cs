﻿// Decompiled with JetBrains decompiler
// Type: TradeView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TradeView : MarchView
{
  public override void CreateModelFromResource(System.Action<GameObject> callback)
  {
    if (callback == null)
      return;
    AssetManager.Instance.LoadAsync("Prefab/TroopModels/Trade_Walk", (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      if (ret && this.isLoading)
        callback(UnityEngine.Object.Instantiate<GameObject>(obj as GameObject));
      AssetManager.Instance.UnLoadAsset("Prefab/TroopModels/Trade_Walk", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    }), (System.Type) null);
  }
}
