﻿// Decompiled with JetBrains decompiler
// Type: HealSpell
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class HealSpell : SmallRoundSpell
{
  protected override RoundResult.ResultType ResultType
  {
    get
    {
      return RoundResult.ResultType.Heal;
    }
  }

  protected override int CalcResult(int magicDamge, double coefficient, RoundPlayer underAttacker)
  {
    int num = DamgeCalcUtils.CalcSpellHeal(magicDamge, coefficient);
    this.AddNormalLog(magicDamge, coefficient, underAttacker.Defense, num);
    return num;
  }
}
