﻿// Decompiled with JetBrains decompiler
// Type: KnightRallyInfoData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;

public class KnightRallyInfoData : RallyInfoData
{
  private bool isReady;
  private RallyBuildingData buildData;

  public void OnReinforceDataFresh(bool result, object orgData)
  {
    long num1 = 0;
    long num2 = 0;
    if (result)
    {
      Hashtable hashtable1 = orgData as Hashtable;
      if (hashtable1.ContainsKey((object) "capacity_info"))
      {
        Hashtable hashtable2 = hashtable1[(object) "capacity_info"] as Hashtable;
        if (hashtable2 != null)
        {
          if (hashtable2.ContainsKey((object) "troops_amount"))
            num1 = (long) int.Parse(hashtable2[(object) "troops_amount"].ToString());
          if (hashtable2.ContainsKey((object) "capacity"))
            num2 = (long) int.Parse(hashtable2[(object) "capacity"].ToString());
        }
      }
    }
    this.MaxCount = num2;
    this.RemainCount = num2 - num1;
    this.Refresh();
  }

  public RallyBuildingData BuildData
  {
    get
    {
      return this.buildData;
    }
  }

  protected override void Prepare()
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.DataID);
    if (marchData == null)
    {
      this.DispatchPrepareFail();
    }
    else
    {
      UserData userData = DBManager.inst.DB_User.Get(marchData.targetUid);
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(marchData.targetAllianceId);
      CityData cityData = DBManager.inst.DB_City.Get(marchData.targetCityId);
      this.buildData = RallyBuildingData.Create(marchData.endLocation.X, marchData.endLocation.Y);
      if (!this.isReady)
      {
        this.isReady = true;
        if (this.buildData != null)
        {
          this.TargetIsAllianceBuild = true;
          this.buildData.Ready(new System.Action(((RallyInfoData) this).Refresh));
        }
        else
        {
          if (userData == null)
            return;
          Hashtable postData = new Hashtable();
          postData[(object) "uid"] = (object) PlayerData.inst.uid;
          postData[(object) "opp_uid"] = (object) marchData.targetUid;
          MessageHub.inst.GetPortByAction("PVP:getReinforceList").SendRequest(postData, new System.Action<bool, object>(this.OnReinforceDataFresh), true);
        }
      }
      else
      {
        if (cityData != null)
          this.ReinforceLocation = cityData.cityLocation;
        this.TargetID = marchData.targetUid;
        this.Type = RallyInfoData.RallyDataType.Knight;
        this.TargetCityID = marchData.targetCityId;
        this.ReinforceMarchType = MarchAllocDlg.MarchType.reinforce;
        this.MarchList = DBManager.inst.DB_March.GetReinforceByUid(marchData.targetUid);
        if (this.buildData != null)
        {
          this.ReinforceMarchType = this.buildData.ReinforceMarchType;
          this.ReinforceLocation = this.buildData.Location;
          this.MarchList = this.buildData.MarchList;
          this.MaxCount = this.buildData.MaxCount;
        }
        if (userData != null)
        {
          if (allianceData != null)
            this.LeaderName = "[" + allianceData.allianceAcronym + "] " + userData.userName;
          else
            this.LeaderName = userData.userName;
        }
        else if (this.buildData != null)
        {
          if (allianceData != null)
            this.LeaderName = "[" + allianceData.allianceAcronym + "] " + this.buildData.BuildName;
          else
            this.LeaderName = this.buildData.BuildName;
        }
        long num = 0;
        for (int index = 0; index < this.MarchList.Count; ++index)
        {
          MarchData march = this.MarchList[index];
          num += (long) march.troopsInfo.totalCount;
        }
        this.TroopsInfo = num.ToString() + "/" + (object) this.MaxCount;
        this.IsJoined = marchData.targetUid == PlayerData.inst.uid;
        this.CanJoin = true;
        this.IsCallBack = false;
        this.JoinNum = this.MarchList.Count.ToString();
        this.EmptySlot = 0;
        this.IsDefense = true;
        this.IsInit = true;
      }
    }
  }

  private long MaxCount { get; set; }

  protected override void Process()
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.DataID);
    this.CanJoin = false;
    this.IsCallBack = true;
    if (marchData == null)
      return;
    int num1 = marchData.endTime - marchData.startTime;
    int num2 = NetServerTime.inst.ServerTimestamp - marchData.startTime;
    float num3 = 1f - (float) num2 / (float) num1;
    int time = num1 - num2;
    if ((double) num3 < 1.0)
    {
      this.ProgressValue = num3;
      this.ProgressContent = ScriptLocalization.Get("war_rally_marching", true) + " " + Utils.FormatTime(time, false, false, true);
    }
    else
    {
      this.ProgressValue = 1f;
      this.ProgressContent = ScriptLocalization.Get("war_rally_uppercase_rallied", true);
    }
  }
}
