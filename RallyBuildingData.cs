﻿// Decompiled with JetBrains decompiler
// Type: RallyBuildingData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;

public class RallyBuildingData
{
  private Coordinate coordinate = Coordinate.zero;
  protected int x = -1;
  protected int y = -1;
  protected long id;

  public RallyBuildingData(int x, int y)
  {
    this.x = x;
    this.y = y;
  }

  public RallyBuildingData(long id)
  {
    this.id = id;
  }

  public virtual string BuildName
  {
    get
    {
      return string.Empty;
    }
  }

  public virtual List<MarchData> MarchList
  {
    get
    {
      return (List<MarchData>) null;
    }
  }

  public virtual long OwnerID
  {
    get
    {
      return 0;
    }
  }

  public virtual long MaxCount
  {
    get
    {
      return 0;
    }
  }

  public virtual Coordinate Location
  {
    get
    {
      return this.coordinate;
    }
    set
    {
      this.coordinate = value;
    }
  }

  public virtual MarchAllocDlg.MarchType ReinforceMarchType
  {
    get
    {
      return MarchAllocDlg.MarchType.reinforce;
    }
  }

  public virtual RallyBuildingData.DataType Type
  {
    get
    {
      return RallyBuildingData.DataType.None;
    }
  }

  public virtual void Ready(System.Action callBack)
  {
  }

  public long ID
  {
    get
    {
      return this.id;
    }
  }

  public static bool TargetIsBuilding(int x, int y)
  {
    AllianceFortressData allianceFortressData = DBManager.inst.DB_AllianceFortress.Get(x, y);
    AllianceTempleData allianceTempleData = DBManager.inst.DB_AllianceTemple.Get(x, y);
    if (allianceFortressData == null)
      return allianceTempleData != null;
    return true;
  }

  public static bool TargetIsBuilding(long id)
  {
    AllianceFortressData allianceFortressData = DBManager.inst.DB_AllianceFortress.Get(id);
    AllianceTempleData allianceTempleData = DBManager.inst.DB_AllianceTemple.Get(id);
    if (allianceFortressData == null)
      return allianceTempleData != null;
    return true;
  }

  public static RallyBuildingData Create(int x, int y)
  {
    AllianceFortressData allianceFortressData = DBManager.inst.DB_AllianceFortress.Get(x, y);
    if (allianceFortressData != null)
    {
      RallyBuildingData rallyBuildingData = (RallyBuildingData) new RallyFortressData(x, y);
      rallyBuildingData.id = allianceFortressData.fortressId;
      return rallyBuildingData;
    }
    AllianceTempleData allianceTempleData = DBManager.inst.DB_AllianceTemple.Get(x, y);
    if (allianceTempleData == null)
      return (RallyBuildingData) null;
    RallyBuildingData rallyBuildingData1 = (RallyBuildingData) new RallyFortressData(x, y);
    rallyBuildingData1.id = allianceTempleData.templeId;
    return rallyBuildingData1;
  }

  public static RallyBuildingData Create(long rallyId)
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(rallyId);
    if (rallyData == null)
      return (RallyBuildingData) null;
    if (DBManager.inst.DB_AllianceFortress.Get((long) rallyData.fortressId) != null)
      return (RallyBuildingData) new RallyFortressData((long) rallyData.fortressId);
    if (DBManager.inst.DB_AllianceTemple.Get((long) rallyData.fortressId) != null)
      return (RallyBuildingData) new RallyTempleData((long) rallyData.fortressId);
    if (WonderUtils.IsWonder(rallyData.location))
      return (RallyBuildingData) new RallyWonderData((long) rallyData.location.K);
    if (WonderUtils.IsWonderTower(rallyData.location))
    {
      long rallyWonderData = PlayerData.inst.allianceWarManager.GetRallyWonderData(rallyData.rallyId);
      if (rallyWonderData > -1L)
      {
        RallyTowerData rallyTowerData = new RallyTowerData(rallyWonderData);
        rallyTowerData.Location = rallyData.location;
        return (RallyBuildingData) rallyTowerData;
      }
    }
    return (RallyBuildingData) null;
  }

  public enum DataType
  {
    None,
    Fortress,
    Temple,
    Wonder,
    Tower,
  }
}
