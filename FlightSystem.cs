﻿// Decompiled with JetBrains decompiler
// Type: FlightSystem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class FlightSystem : MonoBehaviour
{
  public GameObject m_BirdPrefab;
  private GameObject m_BirdInstance;
  private float m_Timer;

  private void OnDisable()
  {
    this.DestroyModel();
    this.m_Timer = 0.0f;
  }

  private void DestroyModel()
  {
    if (!((Object) this.m_BirdInstance != (Object) null) || !PrefabManagerEx.IsAlive)
      return;
    PrefabManagerEx.Instance.Destroy(this.m_BirdInstance);
    this.m_BirdInstance = (GameObject) null;
  }

  private static void CreatePath(out Vector3 left, out Vector3 right)
  {
    Rect viewport = UIManager.inst.tileCamera.GetViewport();
    float y1 = Random.Range(viewport.yMin, viewport.yMax);
    float y2 = Random.Range(viewport.yMin, viewport.yMax);
    left = new Vector3(viewport.xMin, y1, 0.0f);
    right = new Vector3(viewport.xMax, y2, 0.0f);
  }
}
