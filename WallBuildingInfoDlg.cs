﻿// Decompiled with JetBrains decompiler
// Type: WallBuildingInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class WallBuildingInfoDlg : BuildingInfoBaseDlg
{
  public UITexture texture1;
  public UILabel nameLabel1;
  public UILabel valueLabel1;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    if (this.buildingInfo.Benefit_ID_1.Length <= 0)
      return;
    this.DrawBenefit(this.buildingInfo.Benefit_Value_1, this.nameLabel1, this.valueLabel1, int.Parse(this.buildingInfo.Benefit_ID_1), "prop_city_defense_base_value", "calc_city_defense", this.texture1);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    BuilderFactory.Instance.Release((UIWidget) this.texture1);
    base.OnClose(orgParam);
  }
}
