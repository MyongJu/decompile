﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarTargetListPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceWarTargetListPopup : Popup
{
  [SerializeField]
  private AllianceWarTargetListPopup.Panel panel;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (orgParam == null)
      return;
    this.Fresh((orgParam as AllianceWarTargetListPopup.Param).uids);
  }

  public void Fresh(List<long> uids)
  {
    if (uids == null)
    {
      for (int index = 0; index < this.panel.slots.Count; ++index)
        this.panel.slots[index].gameObject.SetActive(false);
    }
    else
    {
      if (uids.Count > this.panel.slots.Count)
      {
        for (int count = this.panel.slots.Count; count < uids.Count; ++count)
        {
          AllianceWarTargetListSlot component = NGUITools.AddChild(this.panel.table.gameObject, this.panel.orgSlot.gameObject).GetComponent<AllianceWarTargetListSlot>();
          component.transform.SetParent(this.panel.table.transform);
          this.panel.slots.Add(component);
        }
        this.panel.table.Reposition();
      }
      for (int index = 0; index < uids.Count; ++index)
      {
        this.panel.slots[index].gameObject.SetActive(true);
        this.panel.slots[index].Setup(uids[index]);
      }
      for (int count = uids.Count; count < this.panel.slots.Count; ++count)
        this.panel.slots[count].gameObject.SetActive(false);
      this.panel.scrollView.ResetPosition();
    }
  }

  public void OnCloseButtonClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void Reset()
  {
    this.panel.orgSlot = this.transform.Find("Content/benefits/orgSlot").GetComponent<AllianceWarTargetListSlot>();
    this.panel.orgSlot.gameObject.SetActive(false);
    this.panel.orgSlot.Reset();
    this.panel.scrollView = this.transform.Find("Content/benefits/Scroll View").GetComponent<UIScrollView>();
    this.panel.table = this.transform.Find("Content/benefits/Scroll View/table").GetComponent<UITable>();
  }

  public class Param : Popup.PopupParameter
  {
    public List<long> uids;
  }

  [Serializable]
  public class Panel
  {
    public List<AllianceWarTargetListSlot> slots = new List<AllianceWarTargetListSlot>();
    public AllianceWarTargetListSlot orgSlot;
    public UIScrollView scrollView;
    public UITable table;
  }
}
