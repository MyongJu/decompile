﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillConfigContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class DragonSkillConfigContainer : MonoBehaviour
{
  private List<DragonSkillConfigSlot> m_SkillConfigList = new List<DragonSkillConfigSlot>();
  public OnDragonSkillEquipCallback OnEquip;
  public UIGrid m_Grid;
  public UIScrollView m_ScrollView;
  public GameObject m_ItemPrefab;

  private void OnEnable()
  {
    DBManager.inst.DB_Dragon.onDataChanged += new System.Action<long>(this.OnDragonDataChanged);
  }

  private void OnDisable()
  {
    if (!GameEngine.IsAvailable || GameEngine.IsShuttingDown)
      return;
    DBManager.inst.DB_Dragon.onDataChanged -= new System.Action<long>(this.OnDragonDataChanged);
  }

  private void OnDragonDataChanged(long generalID)
  {
    if (generalID != PlayerData.inst.uid)
      return;
    for (int index = 0; index < this.m_SkillConfigList.Count; ++index)
      this.m_SkillConfigList[index].UpdateUI();
  }

  public void UpdateUI(string usability, List<ConfigDragonSkillGroupInfo> dragonSkillGroupList)
  {
    this.Clear();
    for (int index = 0; index < dragonSkillGroupList.Count; ++index)
    {
      ConfigDragonSkillGroupInfo dragonSkillGroup = dragonSkillGroupList[index];
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_Grid.transform;
      gameObject.transform.localScale = Vector3.one;
      DragonSkillConfigSlot component = gameObject.GetComponent<DragonSkillConfigSlot>();
      component.SetData(usability, dragonSkillGroup, this.OnEquip);
      this.m_SkillConfigList.Add(component);
    }
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  public void Clear()
  {
    for (int index = 0; index < this.m_SkillConfigList.Count; ++index)
    {
      DragonSkillConfigSlot skillConfig = this.m_SkillConfigList[index];
      skillConfig.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) skillConfig.gameObject);
    }
    this.m_SkillConfigList.Clear();
  }
}
