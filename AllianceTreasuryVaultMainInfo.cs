﻿// Decompiled with JetBrains decompiler
// Type: AllianceTreasuryVaultMainInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class AllianceTreasuryVaultMainInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "chest_name")]
  public string ChestName;
  [Config(Name = "icon")]
  public string Icon;
  [Config(Name = "type")]
  public int _Type;
  [Config(Name = "duration")]
  public int Duration;
  [Config(Name = "num")]
  public int Num;
  [Config(Name = "gold")]
  public int Gold;

  public AllianceTreasuryVaultMainInfo.ChestType Type
  {
    get
    {
      switch (this._Type)
      {
        case 1:
          return AllianceTreasuryVaultMainInfo.ChestType.Alliance;
        case 2:
          return AllianceTreasuryVaultMainInfo.ChestType.Personal;
        default:
          return AllianceTreasuryVaultMainInfo.ChestType.Invalid;
      }
    }
  }

  public string LocalChestName
  {
    get
    {
      return ScriptLocalization.Get(this.ChestName, true);
    }
  }

  public enum ChestType
  {
    Invalid,
    Alliance,
    Personal,
  }
}
