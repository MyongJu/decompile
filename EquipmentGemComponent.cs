﻿// Decompiled with JetBrains decompiler
// Type: EquipmentGemComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EquipmentGemComponent : ComponentRenderBase
{
  public System.Action<EquipmentGemComponent> OnSelectedHandler;
  public UITexture icon;
  public UILabel level;
  public UILabel amount;
  public GameObject selected;
  public GemDataWrapper gemDataWrapper;
  private int _itemId;
  public UITexture bacgourd;

  public override void Init()
  {
    this.Select = false;
    EquipmentGemComponentData data1 = this.data as EquipmentGemComponentData;
    if (data1 == null)
      return;
    this.gemDataWrapper = data1.dataWrapper;
    if (this.gemDataWrapper == null)
      return;
    ConfigEquipmentGemInfo data2 = ConfigManager.inst.DB_EquipmentGem.GetData(this.gemDataWrapper.GemData.ConfigId);
    if (data2 == null)
      return;
    int itemId = data2.itemID;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    this._itemId = itemId;
    if (itemStaticInfo == null)
      return;
    Utils.SetItemBackground(this.bacgourd, itemId);
    BuilderFactory.Instance.Build((UIWidget) this.icon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    this.level.text = string.Format("+{0}", (object) data2.level);
    if (this.gemDataWrapper.GemData.Inlaid)
      this.amount.text = Utils.XLAT("id_embedded");
    else
      this.amount.text = this.gemDataWrapper.GemCount.ToString();
  }

  public void OnPressItem()
  {
    Utils.DelayShowTip(this._itemId, this.transform, 0L, 0L, 0);
  }

  public void OnReleaseItem()
  {
    Utils.StopShowItemTip();
  }

  public override void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
  }

  public bool Select
  {
    set
    {
      NGUITools.SetActive(this.selected, value);
    }
  }

  public void OnClick()
  {
    if (this.OnSelectedHandler == null)
      return;
    this.OnSelectedHandler(this);
  }
}
