﻿// Decompiled with JetBrains decompiler
// Type: AllianceRewardItemRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class AllianceRewardItemRender : MonoBehaviour
{
  private const string ASSIGNED_TO = "alliance_warfare_assigned_to";
  private const string NO_BODY = "alliance_warfare_nobody";
  private const uint NAME_BIRGHT = 16768159;
  private const uint NAME_GREY = 7895160;
  public UILabel rewardName;
  public UILabel distribute;
  public UILabel playerName;
  public GameObject selected;
  public IAPDailyRewardItemRender itemRender;
  public System.Action<AllianceRewardItemRender> OnClickedHandler;
  private AllianceRewardItemData data;

  public AllianceRewardItemData Data
  {
    get
    {
      return this.data;
    }
  }

  public void FeedData(AllianceRewardItemData data)
  {
    this.data = data;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.rewardName.text = this.data.itemInfo.LocName;
    this.distribute.text = Utils.XLAT("alliance_warfare_assigned_to");
    if (this.data.uid == 0L)
    {
      this.playerName.text = Utils.XLAT("alliance_warfare_nobody");
      this.playerName.color = Utils.GetColorFromHex(7895160U);
    }
    else
    {
      UserData userData = DBManager.inst.DB_User.Get(this.data.uid);
      if (userData != null)
      {
        this.playerName.text = userData.userName;
        this.playerName.color = Utils.GetColorFromHex(16768159U);
      }
    }
    if (this.data.itemInfo == null)
      return;
    this.itemRender.SetData(this.data.itemInfo, this.data.count);
  }

  public bool Select
  {
    set
    {
      NGUITools.SetActive(this.selected, value);
    }
  }

  public void OnClick()
  {
    if (this.OnClickedHandler == null)
      return;
    this.OnClickedHandler(this);
  }
}
