﻿// Decompiled with JetBrains decompiler
// Type: LordTitleDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class LordTitleDlg : UI.Dialog
{
  private Dictionary<int, LordTitleCatalog> _catalogItemDict = new Dictionary<int, LordTitleCatalog>();
  private Dictionary<int, LordTitleBenefitSolt> _benefitItemDict = new Dictionary<int, LordTitleBenefitSolt>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private GameObject _cityRoot;
  [SerializeField]
  private AllianceSymbol _allianceSymbol;
  [SerializeField]
  private UILabel _labelCityName;
  [SerializeField]
  private UISprite _spriteCityNameFrame;
  [SerializeField]
  private UITexture _textureCityNameFrame;
  [SerializeField]
  private UILabel _avatorObtainText;
  [SerializeField]
  private UIButton _avatorActivateButton;
  [SerializeField]
  private UILabel _avatorActivateButtonText;
  [SerializeField]
  private UIButton _avatorUnlockButton;
  [SerializeField]
  private GameObject _avatorUnlockHintNode;
  [SerializeField]
  private UILabel _labelPlayerName;
  [SerializeField]
  private UILabel _labelPlayerWord;
  [SerializeField]
  private UITexture _texturePlayerIcon;
  [SerializeField]
  private UIScrollView _catalogScrollView;
  [SerializeField]
  private UITable _catalogTable;
  [SerializeField]
  private GameObject _catalogItemPrefab;
  [SerializeField]
  private UIScrollView _benefitScrollView;
  [SerializeField]
  private UIGrid _benefitGrid;
  [SerializeField]
  private GameObject _benefitItemPrefab;
  private int _pickedLordTitleId;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.AddEventHandler();
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.RemoveEventHandler();
    this.ClearCatalogData();
    this.ClearBenefitData();
  }

  public void OnTotalBenefitClick()
  {
    UIManager.inst.OpenPopup("LordTitle/LordTitleTotalBenefitPopup", (Popup.PopupParameter) null);
  }

  public void OnObtainTitleClick()
  {
    LordTitleMainInfo lordTitleMainInfo = ConfigManager.inst.DB_LordTitleMain.Get(this._pickedLordTitleId);
    if (lordTitleMainInfo == null)
      return;
    string link = lordTitleMainInfo.link;
    if (link == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (LordTitleDlg.\u003C\u003Ef__switch\u0024map80 == null)
    {
      // ISSUE: reference to a compiler-generated field
      LordTitleDlg.\u003C\u003Ef__switch\u0024map80 = new Dictionary<string, int>(4)
      {
        {
          "1",
          0
        },
        {
          "2",
          1
        },
        {
          "3",
          2
        },
        {
          "4",
          3
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!LordTitleDlg.\u003C\u003Ef__switch\u0024map80.TryGetValue(link, out num))
      return;
    switch (num)
    {
      case 0:
        UIManager.inst.OpenDlg("Achievement/AchievementDlg", (UI.Dialog.DialogParameter) null, true, true, true);
        break;
      case 1:
        GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("merlin_trials_stronghold_level_min");
        if (data == null || data.ValueInt <= PlayerData.inst.CityData.mStronghold.mLevel)
        {
          UIManager.inst.OpenDlg("MerlinTrials/MelinTrialDlg", (UI.Dialog.DialogParameter) null, true, true, true);
          break;
        }
        UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_trial_locked", new Dictionary<string, string>()
        {
          {
            "0",
            data.ValueInt.ToString()
          }
        }, true), (System.Action) null, 4f, true);
        break;
      case 2:
        Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
        break;
      case 3:
        UIManager.inst.OpenDlg("DragonKnight/DungeonAncientStoreDialog", (UI.Dialog.DialogParameter) new DungeonAncientStoreDialog.Parameter()
        {
          group = "dk_dungeon_shop",
          currency = "item_dk_dungeon_coin",
          titleKey = "dragon_knight_store_name"
        }, 1 != 0, 1 != 0, 1 != 0);
        break;
    }
  }

  public void OnActivateAvatorClick()
  {
    LordTitlePayload.Instance.ActivateLordTitle(!LordTitlePayload.Instance.IsAvatorActivated(this._pickedLordTitleId) ? this._pickedLordTitleId : 0, (System.Action<bool, object>) null);
  }

  public void OnUnlockAvatorClick()
  {
    LordTitlePayload.Instance.UnlockAvatorTitle(this._pickedLordTitleId, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.UpdateCustomUI();
      UIManager.inst.OpenPopup("LordTitle/LordTitleDetailPopup", (Popup.PopupParameter) new LordTitleDetailPopup.Parameter()
      {
        lordTitleId = this._pickedLordTitleId
      });
    }));
  }

  private void UpdateCatalogUI()
  {
    this.GenerateCatalog();
    Utils.ExecuteInSecs(0.25f, (System.Action) (() =>
    {
      if (!((UnityEngine.Object) this != (UnityEngine.Object) null))
        return;
      if (PlayerData.inst.userData.LordTitle <= 0)
      {
        if (!this._catalogItemDict.ContainsKey(0))
          return;
        this._catalogItemDict[0].OnCatalogClicked();
      }
      else
      {
        LordTitleMainInfo lordTitleMainInfo = ConfigManager.inst.DB_LordTitleMain.Get(PlayerData.inst.userData.LordTitle);
        if (lordTitleMainInfo == null)
          return;
        using (Dictionary<int, LordTitleCatalog>.Enumerator enumerator = this._catalogItemDict.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<int, LordTitleCatalog> current = enumerator.Current;
            if (lordTitleMainInfo.sortId == current.Value.CatalogId)
            {
              current.Value.OnCatalogClicked();
              current.Value.SetSelected(lordTitleMainInfo.internalId);
              break;
            }
          }
        }
      }
    }));
  }

  private void ClearCatalogData()
  {
    using (Dictionary<int, LordTitleCatalog>.Enumerator enumerator = this._catalogItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, LordTitleCatalog> current = enumerator.Current;
        current.Value.OnCurrentCatalogClicked -= new System.Action<LordTitleCatalog>(this.OnCatalogClicked);
        current.Value.ClearData();
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._catalogItemDict.Clear();
    this._itemPool.Clear();
  }

  private void RepositionCatalog(bool resetScrollView = true)
  {
    this._catalogTable.repositionNow = true;
    this._catalogTable.Reposition();
    if (!resetScrollView)
      return;
    this._catalogScrollView.ResetPosition();
  }

  private LordTitleCatalog GenerateCatalogSlot(int catalogLevel, int catalogId)
  {
    this._itemPool.Initialize(this._catalogItemPrefab, this._catalogTable.gameObject);
    GameObject go = this._itemPool.AddChild(this._catalogTable.gameObject);
    NGUITools.SetActive(go, true);
    LordTitleCatalog component = go.GetComponent<LordTitleCatalog>();
    component.SetData(catalogLevel, catalogId);
    component.OnCurrentCatalogClicked += new System.Action<LordTitleCatalog>(this.OnCatalogClicked);
    return component;
  }

  private void GenerateCatalog()
  {
    this.ClearCatalogData();
    List<LordTitleSortInfo> infoList = ConfigManager.inst.DB_LordTitleSort.GetInfoList();
    if (infoList != null)
    {
      infoList.Sort((Comparison<LordTitleSortInfo>) ((a, b) => a.sortPriority.CompareTo(b.sortPriority)));
      for (int key = 0; key < infoList.Count; ++key)
      {
        if (infoList[key].internalId > 0)
        {
          LordTitleCatalog catalogSlot = this.GenerateCatalogSlot(1, infoList[key].internalId);
          this._catalogItemDict.Add(key, catalogSlot);
        }
      }
    }
    this.RepositionCatalog(true);
  }

  private void OnCatalogClicked(LordTitleCatalog catalog)
  {
    Debug.Log((object) ("OnCatalogClicked = " + catalog.CatalogName));
    this.RepositionCatalog(false);
    this.ResetCatalogColor();
    catalog.SetNameColor();
    if (catalog.CatalogLevel == 2 && !catalog.Folded)
    {
      this._pickedLordTitleId = catalog.CatalogId;
      this.UpdateCustomUI();
      this.UpdateBenefitUI();
    }
    else
    {
      if (catalog.CatalogLevel != 1 || catalog.Folded)
        return;
      catalog.SetDefaultSelected(0);
    }
  }

  private void ResetCatalogColor()
  {
    LordTitleCatalog[] componentsInChildren = this._catalogTable.GetComponentsInChildren<LordTitleCatalog>(true);
    if (componentsInChildren == null)
      return;
    for (int index = 0; index < componentsInChildren.Length; ++index)
      componentsInChildren[index].ResetNameColor();
  }

  private void UpdateBenefitUI()
  {
    this.ClearBenefitData();
    LordTitleMainInfo lordTitleMainInfo = ConfigManager.inst.DB_LordTitleMain.Get(this._pickedLordTitleId);
    if (lordTitleMainInfo != null && lordTitleMainInfo.benefits != null && lordTitleMainInfo.benefits.benefits != null)
    {
      using (Dictionary<string, float>.Enumerator enumerator = lordTitleMainInfo.benefits.benefits.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, float> current = enumerator.Current;
          int result = 0;
          int.TryParse(current.Key, out result);
          if (result > 0)
          {
            LordTitleBenefitSolt benefitSlot = this.GenerateBenefitSlot(result, current.Value);
            if (!this._benefitItemDict.ContainsKey(result))
              this._benefitItemDict.Add(result, benefitSlot);
          }
        }
      }
    }
    this.RepositionBenefit(true);
  }

  private void ClearBenefitData()
  {
    using (Dictionary<int, LordTitleBenefitSolt>.Enumerator enumerator = this._benefitItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._benefitItemDict.Clear();
    this._itemPool.Clear();
  }

  private void RepositionBenefit(bool resetScrollView = true)
  {
    this._benefitGrid.repositionNow = true;
    this._benefitGrid.Reposition();
    if (!resetScrollView)
      return;
    this._benefitScrollView.ResetPosition();
  }

  private LordTitleBenefitSolt GenerateBenefitSlot(int benefitId, float benefitValue)
  {
    this._itemPool.Initialize(this._benefitItemPrefab, this._benefitGrid.gameObject);
    GameObject go = this._itemPool.AddChild(this._benefitGrid.gameObject);
    NGUITools.SetActive(go, true);
    LordTitleBenefitSolt component = go.GetComponent<LordTitleBenefitSolt>();
    component.SetData(benefitId, benefitValue);
    return component;
  }

  private void UpdateUI()
  {
    this.UpdateCityUI();
    this.UpdateCustomUI();
    this.UpdateCatalogUI();
  }

  private void UpdateCustomUI()
  {
    NGUITools.SetActive(this._avatorObtainText.gameObject, this._pickedLordTitleId > 0);
    NGUITools.SetActive(this._avatorActivateButton.gameObject, this._pickedLordTitleId > 0);
    bool flag1 = !LordTitlePayload.Instance.IsAvatorLocked(this._pickedLordTitleId);
    NGUITools.SetActive(this._avatorActivateButton.gameObject, false);
    NGUITools.SetActive(this._avatorUnlockButton.gameObject, false);
    NGUITools.SetActive(this._avatorUnlockHintNode, false);
    if (flag1)
    {
      this._avatorActivateButtonText.text = !LordTitlePayload.Instance.IsAvatorActivated(this._pickedLordTitleId) ? Utils.XLAT("accolade_activate_frame_button") : Utils.XLAT("accolade_unactivate_frame_button");
      NGUITools.SetActive(this._avatorActivateButton.gameObject, true);
    }
    else
    {
      bool state = LordTitlePayload.Instance.GetItemCountByAvator(this._pickedLordTitleId) > 0;
      NGUITools.SetActive(this._avatorUnlockButton.gameObject, true);
      NGUITools.SetActive(this._avatorUnlockHintNode, state);
      this._avatorUnlockButton.isEnabled = state;
    }
    string textureImagePath = string.Empty;
    bool flag2 = false;
    LordTitleMainInfo lordTitleMainInfo = ConfigManager.inst.DB_LordTitleMain.Get(this._pickedLordTitleId);
    if (lordTitleMainInfo != null)
    {
      flag2 = BundleManager.Instance.IsFileCached("Texture/LordTitle/" + lordTitleMainInfo.NameBoardIcon);
      textureImagePath = lordTitleMainInfo.BoardIconPath;
      this._avatorObtainText.text = lordTitleMainInfo.LocDescription;
    }
    if (flag2)
    {
      NGUITools.SetActive(this._textureCityNameFrame.gameObject, true);
      MarksmanPayload.Instance.FeedMarksmanTexture(this._textureCityNameFrame, textureImagePath);
    }
    else
      NGUITools.SetActive(this._spriteCityNameFrame.gameObject, true);
    LordTitlePayload.Instance.ApplyUserAvator(this._texturePlayerIcon, this._pickedLordTitleId, 5);
  }

  private void UpdateCityUI()
  {
    AssetManager.Instance.LoadAsync(LordTitlePayload.Instance.CityImagePath, (System.Action<UnityEngine.Object, bool>) ((objCity, retCity) =>
    {
      GameObject go = Utils.DuplicateGOB(objCity as GameObject, this._cityRoot.transform);
      NGUITools.SetLayer(go, LayerMask.NameToLayer("NGUI"));
      go.transform.parent = this._cityRoot.transform;
      go.transform.localPosition = new Vector3(0.0f, 30f, 0.0f);
      go.transform.localScale = Vector3.one * 2f;
      SpriteRenderer[] componentsInChildren = this._cityRoot.GetComponentsInChildren<SpriteRenderer>();
      int index = 0;
      for (int length = componentsInChildren.Length; index < length; ++index)
        componentsInChildren[index].material = new Material(Shader.Find("Unlit/Transparent Colored"));
      AssetManager.Instance.UnLoadAsset(LordTitlePayload.Instance.CityImagePath, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    }), (System.Type) null);
    if (PlayerData.inst.allianceData != null)
    {
      this._allianceSymbol.SetSymbols(PlayerData.inst.allianceData.allianceSymbolCode);
      this._allianceSymbol.gameObject.SetActive(true);
    }
    else
      this._allianceSymbol.gameObject.SetActive(false);
    UILabel labelCityName = this._labelCityName;
    string nameAllianceName = PlayerData.inst.userData.userName_Alliance_Name;
    this._labelPlayerName.text = nameAllianceName;
    string str = nameAllianceName;
    labelCityName.text = str;
    this._labelPlayerWord.text = Utils.XLAT("accolade_chat_example");
    CustomIconLoader.Instance.requestCustomIcon(this._texturePlayerIcon, PlayerData.inst.userData.PortraitIconPath, PlayerData.inst.userData.Icon, false);
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdate);
    LordTitlePayload.Instance.OnLordTitleDataUpdate += new System.Action(this.OnLordTitleDataUpdate);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdate);
    LordTitlePayload.Instance.OnLordTitleDataUpdate -= new System.Action(this.OnLordTitleDataUpdate);
  }

  private void OnLordTitleDataUpdate()
  {
    this.UpdateCustomUI();
  }

  private void OnUserDataUpdate(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.UpdateCustomUI();
  }
}
