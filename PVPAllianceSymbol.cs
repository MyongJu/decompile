﻿// Decompiled with JetBrains decompiler
// Type: PVPAllianceSymbol
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PVPAllianceSymbol : MonoBehaviour
{
  public static string[] FLAG_SPRITES = new string[10]
  {
    "icon_alliance_flag_01",
    "icon_alliance_flag_02",
    "icon_alliance_flag_03",
    "icon_alliance_flag_04",
    "icon_alliance_flag_05",
    "icon_alliance_flag_06",
    "icon_alliance_flag_07",
    "icon_alliance_flag_08",
    "icon_alliance_flag_09",
    "icon_alliance_flag_10"
  };
  public static string[] SHIELD_SPRITES = new string[10]
  {
    "icon_alliance_shield_01",
    "icon_alliance_shield_02",
    "icon_alliance_shield_03",
    "icon_alliance_shield_04",
    "icon_alliance_shield_05",
    "icon_alliance_shield_06",
    "icon_alliance_shield_07",
    "icon_alliance_shield_08",
    "icon_alliance_shield_09",
    "icon_alliance_shield_10"
  };
  public static string[] PATTERN_SPRITES = new string[10]
  {
    "icon_alliance_pattern_01",
    "icon_alliance_pattern_02",
    "icon_alliance_pattern_03",
    "icon_alliance_pattern_04",
    "icon_alliance_pattern_05",
    "icon_alliance_pattern_06",
    "icon_alliance_pattern_07",
    "icon_alliance_pattern_08",
    "icon_alliance_pattern_09",
    "icon_alliance_pattern_10"
  };
  [SerializeField]
  private float m_ParticleScale = 0.4f;
  [SerializeField]
  private float m_ParticleRootScale = 0.4f;
  public string SortingLayerName = "Default";
  private const float DEFAULT_DESIGN_RATIO = 1.777778f;
  [SerializeField]
  private UISpriteMesh m_Flag;
  [SerializeField]
  private UISpriteMesh m_Shield;
  [SerializeField]
  private UISpriteMesh m_Pattern;
  public int SortingOrder;
  private GameObject m_VFX;
  private string m_VfxName;

  private float AdjustResolution()
  {
    float num1 = (float) Screen.width * 1f / (float) Screen.height;
    float num2 = num1 / 1.777778f;
    if ((double) num1 >= 1.77777779102325)
      num2 = 1f;
    return num2;
  }

  public void SetSymbols(int flag, int shield, int pattern)
  {
    this.m_Flag.spriteName = flag >= PVPAllianceSymbol.FLAG_SPRITES.Length ? PVPAllianceSymbol.FLAG_SPRITES[0] : PVPAllianceSymbol.FLAG_SPRITES[flag];
    this.m_Shield.spriteName = shield >= PVPAllianceSymbol.SHIELD_SPRITES.Length ? PVPAllianceSymbol.SHIELD_SPRITES[1] : PVPAllianceSymbol.SHIELD_SPRITES[shield];
    this.m_Pattern.spriteName = pattern >= PVPAllianceSymbol.PATTERN_SPRITES.Length ? PVPAllianceSymbol.PATTERN_SPRITES[2] : PVPAllianceSymbol.PATTERN_SPRITES[pattern];
    string fullname = string.Format("Prefab/VFX/fx_{0}", (object) this.m_Pattern.spriteName);
    if (!(this.m_VfxName != fullname))
      return;
    this.m_VfxName = fullname;
    if ((UnityEngine.Object) this.m_VFX != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_VFX);
      this.m_VFX = (GameObject) null;
    }
    GameObject original = AssetManager.Instance.Load(fullname, (System.Type) null) as GameObject;
    if ((bool) ((UnityEngine.Object) original))
    {
      this.m_VFX = UnityEngine.Object.Instantiate<GameObject>(original);
      ParticleSortingOrderModifier sortingOrderModifier = this.m_VFX.AddComponent<ParticleSortingOrderModifier>();
      sortingOrderModifier.SortingLayerName = this.SortingLayerName;
      sortingOrderModifier.SortingOrder = this.SortingOrder;
      float num = this.AdjustResolution();
      this.m_VFX.transform.parent = this.gameObject.transform;
      this.m_VFX.transform.localPosition = Vector3.zero;
      this.m_VFX.transform.localRotation = Quaternion.identity;
      this.m_VFX.transform.localScale = new Vector3(num * this.m_ParticleRootScale, num * this.m_ParticleRootScale, num * this.m_ParticleRootScale);
      NGUITools.SetLayer(this.m_VFX, this.gameObject.layer);
      ParticleScaler component = this.m_VFX.GetComponent<ParticleScaler>();
      component.particleScale = num * this.m_ParticleScale;
      component.alsoScaleGameobject = false;
    }
    AssetManager.Instance.UnLoadAsset(fullname, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
  }

  public void SetSymbols(int symbolCode)
  {
    this.SetSymbols(PVPAllianceSymbol.GetFlag(symbolCode), PVPAllianceSymbol.GetShield(symbolCode), PVPAllianceSymbol.GetPattern(symbolCode));
  }

  public static int GetFlag(int symbol)
  {
    return symbol & (int) byte.MaxValue;
  }

  public static int GetShield(int symbol)
  {
    return symbol >> 8 & (int) byte.MaxValue;
  }

  public static int GetPattern(int symbol)
  {
    return symbol >> 16 & (int) byte.MaxValue;
  }

  public static int GetSymbol(int flag, int shield, int pattern)
  {
    return 0 | flag & (int) byte.MaxValue | (shield & (int) byte.MaxValue) << 8 | (pattern & (int) byte.MaxValue) << 16;
  }
}
