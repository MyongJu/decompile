﻿// Decompiled with JetBrains decompiler
// Type: HeroPreview
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HeroPreview : MonoBehaviour
{
  private GameObject m_HeroInstance;
  private HeroEquipments m_HeroEquipments;

  private void Start()
  {
  }

  public void InitializeCharacter(HeroGender gender)
  {
    GameObject original = (GameObject) null;
    switch (gender)
    {
      case HeroGender.Female:
        original = AssetManager.Instance.HandyLoad("Prefab/prefabs/hero_female", (System.Type) null) as GameObject;
        break;
      case HeroGender.Male:
        original = AssetManager.Instance.HandyLoad("Prefab/prefabs/hero_male", (System.Type) null) as GameObject;
        break;
    }
    if (!((UnityEngine.Object) original != (UnityEngine.Object) null))
      return;
    this.m_HeroInstance = UnityEngine.Object.Instantiate<GameObject>(original);
    this.m_HeroInstance.transform.parent = this.gameObject.transform;
    this.m_HeroInstance.transform.localPosition = Vector3.zero;
    this.m_HeroInstance.transform.localRotation = Quaternion.identity;
    this.m_HeroInstance.transform.localScale = Vector3.one;
    this.m_HeroInstance.layer = LayerMask.NameToLayer("UI3D");
    int childCount = this.m_HeroInstance.transform.childCount;
    for (int index = 0; index < childCount; ++index)
      this.m_HeroInstance.transform.GetChild(index).gameObject.layer = LayerMask.NameToLayer("UI3D");
  }

  public HeroEquipments GetHeroEquipments()
  {
    return this.m_HeroEquipments;
  }
}
