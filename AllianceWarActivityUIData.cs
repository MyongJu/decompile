﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarActivityUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class AllianceWarActivityUIData : ActivityBaseUIData
{
  public const string ACTIVITY_NAME = "alliance_warfare_name";
  private const string ACTIVITY_DESCRIPTION = "alliance_warfare_description";
  public const string NO_ALLIANCE = "toast_alliance_warfare_no_alliance";
  public const string NOT_OPENED_STATE = "alliance_warfare_unopened_status";
  public const string ENROLLMENT_STATE = "alliance_warfare_enrollment_status";
  public const string REGISTER_FAIL_STATE = "alliance_warfare_signup_fail_status";
  public const string MATCHING_STATE = "alliance_warfare_matching_status";
  public const string PREPARING_STATE = "alliance_warfare_preparing_status";
  public const string OPENED_STATE = "alliance_warfare_opened_status";

  public override ActivityBaseUIData.ActivityType Type
  {
    get
    {
      return ActivityBaseUIData.ActivityType.AlianceWar;
    }
  }

  public override string EventKey
  {
    get
    {
      return "alliance_competition";
    }
  }

  public AllianceWarActivityData AllianceWarData
  {
    get
    {
      return this.Data as AllianceWarActivityData;
    }
  }

  public string GetLocDescription()
  {
    string str = string.Empty;
    switch (this.AllianceWarData.GetAllianceWarState())
    {
      case AllianceWarActivityData.AllianceWarState.NotOpen:
      case AllianceWarActivityData.AllianceWarState.Completed:
        str = "alliance_warfare_unopened_status";
        break;
      case AllianceWarActivityData.AllianceWarState.Registering:
        str = "alliance_warfare_enrollment_status";
        break;
      case AllianceWarActivityData.AllianceWarState.RegisterFail:
        str = "alliance_warfare_signup_fail_status";
        break;
      case AllianceWarActivityData.AllianceWarState.Matching:
        str = "alliance_warfare_matching_status";
        break;
      case AllianceWarActivityData.AllianceWarState.Preparing:
        str = "alliance_warfare_preparing_status";
        break;
      case AllianceWarActivityData.AllianceWarState.Opened:
        str = "alliance_warfare_opened_status";
        break;
    }
    return str;
  }

  public override int StartTime
  {
    get
    {
      return this.AllianceWarData.RegisterStartTime;
    }
  }

  public override int EndTime
  {
    get
    {
      return this.AllianceWarData.FightEndTime;
    }
  }

  public override int GetRemainTime()
  {
    int num = 0;
    switch (this.AllianceWarData.GetAllianceWarState())
    {
      case AllianceWarActivityData.AllianceWarState.NotOpen:
      case AllianceWarActivityData.AllianceWarState.Completed:
        if (NetServerTime.inst.ServerTimestamp < this.AllianceWarData.RegisterStartTime)
        {
          num = this.AllianceWarData.RegisterStartTime - NetServerTime.inst.ServerTimestamp;
          break;
        }
        if (NetServerTime.inst.ServerTimestamp > this.AllianceWarData.FightEndTime)
        {
          num = this.AllianceWarData.FightEndTime + this.AllianceWarData.Interval - NetServerTime.inst.ServerTimestamp;
          break;
        }
        break;
      case AllianceWarActivityData.AllianceWarState.Registering:
        num = this.AllianceWarData.RegisterEndTime - NetServerTime.inst.ServerTimestamp;
        break;
      case AllianceWarActivityData.AllianceWarState.RegisterFail:
        num = this.AllianceWarData.FightEndTime - NetServerTime.inst.ServerTimestamp;
        break;
      case AllianceWarActivityData.AllianceWarState.Matching:
        num = -1;
        break;
      case AllianceWarActivityData.AllianceWarState.Preparing:
        num = this.AllianceWarData.FightStartTime - NetServerTime.inst.ServerTimestamp;
        break;
      case AllianceWarActivityData.AllianceWarState.Opened:
        num = this.AllianceWarData.FightEndTime - NetServerTime.inst.ServerTimestamp;
        break;
    }
    return num;
  }

  public override IconData GetNormalIconData()
  {
    IconData iconData = new IconData();
    iconData.image = "Texture/Events/icon_alliance_war";
    iconData.contents = new string[2];
    iconData.contents[0] = Utils.XLAT("alliance_warfare_name");
    iconData.contents[1] = Utils.XLAT("alliance_warfare_description");
    iconData.Data = (object) this;
    return iconData;
  }

  public override IconData GetADIconData()
  {
    IconData iconData = new IconData();
    iconData.image = "Texture/Events/alliance_war_image_big";
    iconData.contents = new string[2];
    iconData.contents[0] = Utils.XLAT("alliance_warfare_name");
    iconData.contents[1] = Utils.XLAT("alliance_warfare_description");
    return iconData;
  }

  public override void DisplayActivityDetail()
  {
    if (PlayerData.inst.allianceData == null && PlayerData.inst.userData.AcAllianceId == 0L)
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_no_alliance"), (System.Action) null, 4f, false);
    else
      UIManager.inst.OpenDlg("Alliance/AllianceWarDlg", (UI.Dialog.DialogParameter) new AllianceWarMainDlg.Parameter()
      {
        allianceWarData = this.AllianceWarData
      }, true, true, true);
  }
}
