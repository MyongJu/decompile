﻿// Decompiled with JetBrains decompiler
// Type: IapRebateRewards
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class IapRebateRewards
{
  private int targetRechargeAmount = -1;
  public List<IapRebateRewards.RewardItemInfo> rewardItemInfoList = new List<IapRebateRewards.RewardItemInfo>();

  public int TargetRechargeAmount
  {
    get
    {
      return this.targetRechargeAmount;
    }
    set
    {
      this.targetRechargeAmount = value;
    }
  }

  public class RewardItemInfo
  {
    public int rewardItemId;
    public int rewardItemCount;
    public bool hasSpecialEffect;
  }
}
