﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryBattleRole
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class AnniversaryBattleRole
{
  public string icon = string.Empty;
  public int city_level = 1;
  public int troopCount = 1;
  public string acronym = string.Empty;
  public int troopPower = 1;
  public long uid;
  public long alliance_id;
  private string _name;
  public int portrait;
  public int lordTitleId;
  public int killTroopCount;
  public int symbolCode;
  public int worldId;
  public DB.DragonInfo dragon;

  public string Name
  {
    get
    {
      if (this.uid > 6000000000L)
        return Utils.XLAT(this._name);
      string str = Utils.XLAT(this._name) + ".K" + (object) this.worldId;
      if (!string.IsNullOrEmpty(this.acronym))
        str = "(" + this.acronym + ")" + this._name + ".K" + (object) this.worldId;
      return str;
    }
  }

  public void Decode(object orgData)
  {
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable == null)
      return;
    this.dragon = new DB.DragonInfo();
    string empty = string.Empty;
    if (hashtable.ContainsKey((object) "uid") && hashtable[(object) "uid"] != null)
      long.TryParse(hashtable[(object) "uid"].ToString(), out this.uid);
    if (hashtable.ContainsKey((object) "alliance_id") && hashtable[(object) "alliance_id"] != null)
      long.TryParse(hashtable[(object) "alliance_id"].ToString(), out this.alliance_id);
    if (hashtable.ContainsKey((object) "name") && hashtable[(object) "name"] != null)
      this._name = hashtable[(object) "name"].ToString();
    if (hashtable.ContainsKey((object) "portrait") && hashtable[(object) "portrait"] != null)
      int.TryParse(hashtable[(object) "portrait"].ToString(), out this.portrait);
    if (hashtable.ContainsKey((object) "icon") && hashtable[(object) "icon"] != null)
      this.icon = hashtable[(object) "icon"].ToString();
    if (hashtable.ContainsKey((object) "lord_title") && hashtable[(object) "lord_title"] != null)
      int.TryParse(hashtable[(object) "lord_title"].ToString(), out this.lordTitleId);
    if (hashtable.ContainsKey((object) "city_level") && hashtable[(object) "city_level"] != null)
      int.TryParse(hashtable[(object) "city_level"].ToString(), out this.city_level);
    if (hashtable.ContainsKey((object) "start_total_troops") && hashtable[(object) "start_total_troops"] != null)
      int.TryParse(hashtable[(object) "start_total_troops"].ToString(), out this.troopCount);
    if (hashtable.ContainsKey((object) "kill_total_troops") && hashtable[(object) "kill_total_troops"] != null)
      int.TryParse(hashtable[(object) "kill_total_troops"].ToString(), out this.killTroopCount);
    if (hashtable.ContainsKey((object) "acronym") && hashtable[(object) "acronym"] != null)
      this.acronym = hashtable[(object) "acronym"].ToString();
    if (hashtable.ContainsKey((object) "symbol_code") && hashtable[(object) "symbol_code"] != null)
      int.TryParse(hashtable[(object) "symbol_code"].ToString(), out this.symbolCode);
    if (!hashtable.ContainsKey((object) "dragon") || hashtable[(object) "dragon"] == null)
      return;
    this.dragon.Decode(hashtable[(object) "dragon"]);
  }
}
