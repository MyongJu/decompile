﻿// Decompiled with JetBrains decompiler
// Type: FunplusAccountAgent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using I2.Loc;
using System;
using UI;

public class FunplusAccountAgent : FunplusAccount.IDelegate
{
  private FunplusAccountType type = FunplusAccountType.FPAccountTypeUnknown;
  public System.Action<bool> OnInitCompleteCallBack;
  private string fpid;
  private bool isSwitchAccount;

  public event System.Action OnSwitchAccountSuccess;

  public event System.Action<bool> OnBindAccountCallBack;

  public event System.Action<string> OnLoginAccountFail;

  public event System.Action<string> OnBindAccountFail;

  public event System.Action<string> OnUnbindAccountFail;

  public event System.Action OnUnbindAccountSuccessDelegate;

  public event System.Action OnLogoutDelegate;

  public void OnOpenSession(bool isLoggedIn)
  {
    this.IsLoggedIn = isLoggedIn;
    if (this.IsLoggedIn || this.OnInitCompleteCallBack == null)
      return;
    this.OnInitCompleteCallBack(false);
  }

  public bool IsLoggedIn { get; private set; }

  public string SessionKey { get; private set; }

  public string Email { get; private set; }

  public string SNS { get; private set; }

  public string SNSName { get; private set; }

  public string FunplusID
  {
    get
    {
      return this.fpid;
    }
    private set
    {
      if (!string.IsNullOrEmpty(this.fpid) && this.fpid != value)
        this.isSwitchAccount = true;
      this.fpid = value;
      if (!this.isSwitchAccount)
        return;
      this.isSwitchAccount = false;
      if (this.OnSwitchAccountSuccess == null)
        return;
      this.OnSwitchAccountSuccess();
    }
  }

  public void OnLoginSuccess(FunplusSession session)
  {
    if (session == null)
      return;
    this.IsLoggedIn = true;
    this.SessionKey = session.GetSessionKey();
    this.FunplusID = session.GetFpid();
    this.AccountType = session.GetAccountType();
    this.Email = session.GetEmail();
    this.SNS = session.GetSnsId();
    this.SNSName = session.GetSnsName();
    if (this.OnInitCompleteCallBack == null)
      return;
    this.OnInitCompleteCallBack(true);
    this.OnInitCompleteCallBack = (System.Action<bool>) null;
  }

  public void OnLoginError(FunplusError error)
  {
    this.IsLoggedIn = false;
    if (this.OnLoginAccountFail == null)
      return;
    this.OnLoginAccountFail(error.GetErrorMsg());
  }

  public void OnLogout()
  {
    this.IsLoggedIn = false;
    if (this.OnLogoutDelegate == null)
      return;
    this.OnLogoutDelegate();
  }

  public void OnBindAccountSuccess(FunplusSession session)
  {
    if (session == null)
      return;
    this.AccountType = session.GetAccountType();
    this.Email = session.GetEmail();
    this.SNS = session.GetSnsId();
    this.SNSName = session.GetSnsName();
    if (this.OnBindAccountCallBack == null)
      return;
    this.OnBindAccountCallBack(true);
  }

  public void OnBindAccountError(FunplusError error)
  {
    if (this.OnBindAccountFail == null)
      return;
    this.OnBindAccountFail(error.GetErrorLocalizedMsg());
  }

  public FunplusAccountType AccountType
  {
    get
    {
      return this.type;
    }
    set
    {
      if (this.type == value)
        return;
      this.type = value;
    }
  }

  public void OnCloseUserCenter()
  {
  }

  public void OnResetPasswordSuccess(string fpid)
  {
  }

  public void OnResetPasswordError(FunplusError error)
  {
  }

  public void OnSwitchAccountError(FunplusError error)
  {
    if (!GameEngine.IsReady())
      return;
    string title = ScriptLocalization.Get("id_uppercase_notice", true);
    string errorLocalizedMsg = error.ErrorLocalizedMsg;
    string left = ScriptLocalization.Get("id_uppercase_confirm", true);
    string right = ScriptLocalization.Get("id_uppercase_cancel", true);
    try
    {
      UIManager.inst.ShowConfirmationBox(title, errorLocalizedMsg, left, right, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) null, (System.Action) null, (System.Action) null);
    }
    catch (Exception ex)
    {
    }
  }

  public void OnUnbindAccountSuccess(FunplusSession session)
  {
    if (session == null)
      return;
    this.AccountType = session.AccountType;
    this.Email = session.GetEmail();
    this.SNS = session.GetSnsId();
    this.SNSName = session.GetSnsName();
    if (this.OnUnbindAccountSuccessDelegate == null)
      return;
    this.OnUnbindAccountSuccessDelegate();
  }

  public void OnUnbindAccountError(FunplusError error)
  {
    if (this.OnUnbindAccountFail == null)
      return;
    this.OnUnbindAccountFail(error.GetErrorLocalizedMsg());
  }
}
