﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerWarLogPop
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTowerWarLogPop : Popup
{
  private List<MerlinTowerWarLogData> _datas = new List<MerlinTowerWarLogData>();
  public MerlinTowerWarRevengeItem revengeItem;
  public MerlinTowerWarRevengeItem strikebackItem;
  public GameObject normalContent;
  public GameObject revengeContent;
  public AllianceWarLogSlot normlLogSlot;
  public AllianceWarLogSlot revengeLogSlot;
  public UIGrid normalTable;
  public UIGrid revengeTable;
  public UIScrollView normalScrollview;
  public UIScrollView revengeScrollview;
  private MerlinTowerWarLogRevengeData _revengeData;
  private bool _isDestroy;

  public override void OnOpen(UIControler.UIParameter orgParam = null)
  {
    base.OnOpen(orgParam);
    this.normalContent.SetActive(false);
    this.revengeContent.SetActive(false);
    this.FecthData();
  }

  private void FecthData()
  {
    RequestManager.inst.SendRequest("magicTower:getBattleLog", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, (System.Action<bool, object>) ((success, playload) =>
    {
      if (this._isDestroy || !success)
        return;
      this._datas.Clear();
      Hashtable hashtable1 = playload as Hashtable;
      if (hashtable1 != null)
      {
        if (hashtable1.ContainsKey((object) "logs") && hashtable1[(object) "logs"] != null)
        {
          ArrayList arrayList = hashtable1[(object) "logs"] as ArrayList;
          if (arrayList != null)
          {
            for (int index = 0; index < arrayList.Count; ++index)
            {
              Hashtable hashtable2 = arrayList[index] as Hashtable;
              MerlinTowerWarLogData merlinTowerWarLogData = new MerlinTowerWarLogData();
              if (merlinTowerWarLogData.Decode((object) hashtable2))
                this._datas.Add(merlinTowerWarLogData);
            }
          }
        }
        if (hashtable1.ContainsKey((object) "revenge") && hashtable1[(object) "revenge"] != null)
        {
          Hashtable hashtable2 = hashtable1[(object) "revenge"] as Hashtable;
          if (hashtable2 != null)
          {
            this._revengeData = new MerlinTowerWarLogRevengeData();
            if (!this._revengeData.Decode((object) hashtable2))
              this._revengeData = (MerlinTowerWarLogRevengeData) null;
          }
        }
      }
      this.UpdateUI();
    }), true);
  }

  public void OnRevengeBtPress()
  {
    this.OnCloseHandler();
    MerlinTowerPayload.Instance.Revenge();
  }

  public void OnStrikeBackBtPress()
  {
    if (this._revengeData == null)
      return;
    if (this._revengeData.pageIndex > -1)
    {
      MerlinTowerPayload.Instance.StrikeBack(this._revengeData.pageIndex, this._revengeData.index);
      this.OnCloseHandler();
    }
    else
      UIManager.inst.toast.Show(Utils.XLAT("toast_tower_unoccupied_cannot_revenge", (object) "0", (object) this._revengeData.FullName), (System.Action) null, 4f, false);
  }

  private void OnDestroy()
  {
    this._isDestroy = true;
  }

  private AllianceWarLogSlot GetSlot()
  {
    GameObject gameObject = Utils.DuplicateGOB(this.Slot.gameObject, this.Container.transform);
    gameObject.SetActive(true);
    return gameObject.GetComponent<AllianceWarLogSlot>();
  }

  private List<MerlinTowerWarLogData> GetDatas()
  {
    return this._datas;
  }

  public override void OnClose(UIControler.UIParameter orgParam = null)
  {
    base.OnClose(orgParam);
    if ((UnityEngine.Object) this.revengeItem != (UnityEngine.Object) null)
      this.revengeItem.Clear();
    if (!((UnityEngine.Object) this.strikebackItem != (UnityEngine.Object) null))
      return;
    this.strikebackItem.Clear();
  }

  private AllianceWarLogSlot Slot
  {
    get
    {
      if (this._revengeData == null)
        return this.normlLogSlot;
      return this.revengeLogSlot;
    }
  }

  private UIGrid Container
  {
    get
    {
      if (this._revengeData == null)
        return this.normalTable;
      return this.revengeTable;
    }
  }

  private UIScrollView ScrollView
  {
    get
    {
      if (this._revengeData == null)
        return this.normalScrollview;
      return this.revengeScrollview;
    }
  }

  private void UpdateUI()
  {
    if (this._isDestroy)
      return;
    List<MerlinTowerWarLogData> datas = this.GetDatas();
    MerlinTowerWarRevengeItem towerWarRevengeItem = (MerlinTowerWarRevengeItem) null;
    if (this._revengeData == null)
    {
      this.normalContent.SetActive(true);
      this.revengeContent.SetActive(false);
    }
    else
    {
      this.normalContent.SetActive(false);
      this.revengeContent.SetActive(true);
      if (this._revengeData.endTime > 0)
      {
        this.strikebackItem.gameObject.SetActive(true);
        this.revengeItem.gameObject.SetActive(false);
        towerWarRevengeItem = this.strikebackItem;
      }
      else
      {
        this.strikebackItem.gameObject.SetActive(false);
        this.revengeItem.gameObject.SetActive(true);
        towerWarRevengeItem = this.revengeItem;
      }
    }
    if (this._revengeData != null)
    {
      towerWarRevengeItem.SetData(this._revengeData.FullName, this._revengeData.item_id, this._revengeData.count);
      if (this._revengeData.endTime - NetServerTime.inst.ServerTimestamp > 0)
      {
        towerWarRevengeItem.OnRevengeDisappear += new System.Action(this.OnRevengeFinish);
        towerWarRevengeItem.SetRemainTime(this._revengeData.endTime);
      }
    }
    for (int index = 0; index < datas.Count; ++index)
      this.GetSlot().Setup((AllianceWarManager.LogData) datas[index]);
    this.Container.repositionNow = true;
    this.Container.Reposition();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.ScrollView.ResetPosition()));
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnRevengeFinish()
  {
    this.strikebackItem.OnRevengeDisappear -= new System.Action(this.OnRevengeFinish);
    this.revengeItem.OnRevengeDisappear -= new System.Action(this.OnRevengeFinish);
    this.UpdateUI();
  }
}
