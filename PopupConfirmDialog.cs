﻿// Decompiled with JetBrains decompiler
// Type: PopupConfirmDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PopupConfirmDialog : MonoBehaviour
{
  public UILabel LB_Title;
  public UILabel LB_Description;

  public event System.Action onCloseButtonClick;

  public event System.Action onOkeyButtonClick;

  public string TEXT_title
  {
    set
    {
      if (!((UnityEngine.Object) this.LB_Title != (UnityEngine.Object) null))
        return;
      this.LB_Title.text = value;
    }
  }

  public string TEXT_Description
  {
    set
    {
      if (!((UnityEngine.Object) this.LB_Description != (UnityEngine.Object) null))
        return;
      this.LB_Description.text = value;
    }
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
  }

  public void OnCloseButtonClick()
  {
    if (this.onCloseButtonClick != null)
    {
      this.onCloseButtonClick();
      this.Dispose();
    }
    this.gameObject.SetActive(false);
  }

  public void OnOkeyButtonClick()
  {
    if (this.onOkeyButtonClick != null)
    {
      this.onOkeyButtonClick();
      this.Dispose();
    }
    this.gameObject.SetActive(false);
  }

  public void Dispose()
  {
    this.onCloseButtonClick = (System.Action) null;
    this.onOkeyButtonClick = (System.Action) null;
  }
}
