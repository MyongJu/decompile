﻿// Decompiled with JetBrains decompiler
// Type: WonderGiftHistoryItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WonderGiftHistoryItem : MonoBehaviour
{
  [SerializeField]
  protected UILabel m_labelDate;
  [SerializeField]
  protected UILabel m_labelDetail;

  public void setData(string date, string detail)
  {
    this.m_labelDate.text = date;
    this.m_labelDetail.text = detail;
  }
}
