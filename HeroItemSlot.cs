﻿// Decompiled with JetBrains decompiler
// Type: HeroItemSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class HeroItemSlot : MonoBehaviour
{
  private GroupMember m_GroupMember = new GroupMember();
  public UITexture m_Icon;
  public UITexture m_Quality;
  public UISprite m_Highlight;
  public GameObject m_Checked;
  public UILabel m_FreezeTime;
  private InventoryItemData m_ItemData;
  private OnHeroItemSlotClicked OnSlotClicked;

  public void SetData(InventoryItemData itemData, Group group, OnHeroItemSlotClicked onSlotClicked)
  {
    this.m_ItemData = itemData;
    this.m_GroupMember.SetGroup(group);
    this.OnSlotClicked = onSlotClicked;
    this.m_GroupMember.OnReceived += new OnGroupMemberReceived(this.OnReceived);
    if (itemData == null)
      return;
    this.UpdateUI();
    this.m_Highlight.gameObject.SetActive(false);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, itemData.equipment.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Quality, itemData.equipment.QualityImagePath, (System.Action<bool>) null, true, false, string.Empty);
    if (!((UnityEngine.Object) this.m_FreezeTime != (UnityEngine.Object) null))
      return;
    this.m_FreezeTime.text = itemData.FreezeTime;
    NGUITools.SetActive(this.m_FreezeTime.gameObject, itemData.IsFreeze);
  }

  public InventoryItemData ItemData
  {
    get
    {
      return this.m_ItemData;
    }
  }

  public void UpdateUI()
  {
    this.SetChecked(this.m_ItemData.isEquipped);
  }

  public InventoryItemData GetData()
  {
    return this.m_ItemData;
  }

  public void OnClicked()
  {
    this.Selected();
  }

  public void OnItemPress()
  {
    Utils.DelayShowTip(this.m_ItemData.equipment.itemID, this.transform, this.m_ItemData.itemId, 0L, 0);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  public void Selected()
  {
    this.m_GroupMember.Send((object) null);
    if (this.OnSlotClicked == null)
      return;
    this.OnSlotClicked(this.m_ItemData);
  }

  private void OnReceived(GroupMember sender, object data)
  {
    this.m_Highlight.gameObject.SetActive(sender == this.m_GroupMember);
  }

  private void SetChecked(bool check)
  {
    if (!(bool) ((UnityEngine.Object) this.m_Checked))
      return;
    this.m_Checked.SetActive(this.m_ItemData.isEquipped);
  }
}
