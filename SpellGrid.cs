﻿// Decompiled with JetBrains decompiler
// Type: SpellGrid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SpellGrid : MonoBehaviour
{
  [SerializeField]
  protected float m_alphaInterval = 1f;
  [SerializeField]
  protected float m_alphaMin = 0.2f;
  [SerializeField]
  protected float m_alphaMax = 1f;
  [SerializeField]
  protected GameObject m_rightCorner;
  [SerializeField]
  protected GameObject m_topCorner;
  [SerializeField]
  protected GameObject m_topRightLine;
  public SpellGrid.Position m_position;
  protected SpriteRenderer[] m_allSpriteRenderer;

  protected void Start()
  {
    this.m_allSpriteRenderer = this.GetComponentsInChildren<SpriteRenderer>();
  }

  protected void Update()
  {
    float num1 = Time.realtimeSinceStartup % (2f * this.m_alphaInterval);
    float num2 = Mathf.Lerp(this.m_alphaMin, this.m_alphaMax, (double) num1 >= (double) this.m_alphaInterval ? (float) (2.0 - (double) num1 / (double) this.m_alphaInterval) : num1 / this.m_alphaInterval);
    foreach (SpriteRenderer spriteRenderer in this.m_allSpriteRenderer)
    {
      if ((bool) ((Object) spriteRenderer))
      {
        Color color = spriteRenderer.color;
        color.a = num2;
        spriteRenderer.color = color;
      }
    }
  }

  public void setPosition(SpellGrid.Position position)
  {
    this.m_position = position;
    this.m_rightCorner.SetActive(false);
    this.m_topCorner.SetActive(false);
    this.m_topRightLine.SetActive(false);
    switch (position)
    {
      case SpellGrid.Position.LeftCorner:
        this.m_rightCorner.transform.localScale = new Vector3(-1f, 1f, 1f);
        this.m_rightCorner.SetActive(true);
        break;
      case SpellGrid.Position.RightCorner:
        this.m_rightCorner.transform.localScale = new Vector3(1f, 1f, 1f);
        this.m_rightCorner.SetActive(true);
        break;
      case SpellGrid.Position.TopCorner:
        this.m_topCorner.transform.localScale = new Vector3(1f, 1f, 1f);
        this.m_topCorner.SetActive(true);
        break;
      case SpellGrid.Position.BottomCorner:
        this.m_topCorner.transform.localScale = new Vector3(1f, -1f, 1f);
        this.m_topCorner.SetActive(true);
        break;
      case SpellGrid.Position.TopLeft:
        this.m_topRightLine.transform.localScale = new Vector3(-1f, 1f, 1f);
        this.m_topRightLine.SetActive(true);
        break;
      case SpellGrid.Position.TopRight:
        this.m_topRightLine.transform.localScale = new Vector3(1f, 1f, 1f);
        this.m_topRightLine.SetActive(true);
        break;
      case SpellGrid.Position.BottomLeft:
        this.m_topRightLine.transform.localScale = new Vector3(-1f, -1f, 1f);
        this.m_topRightLine.SetActive(true);
        break;
      case SpellGrid.Position.BottomRight:
        this.m_topRightLine.transform.localScale = new Vector3(1f, -1f, 1f);
        this.m_topRightLine.SetActive(true);
        break;
    }
  }

  public enum Position
  {
    LeftCorner,
    RightCorner,
    TopCorner,
    BottomCorner,
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight,
    Inner,
  }
}
