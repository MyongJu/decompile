﻿// Decompiled with JetBrains decompiler
// Type: PlayerProfileDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PlayerProfileDlg : UI.Dialog
{
  public const string IMAGE_PREFIX = "Texture/Hero/player_portrait_";
  public UILabel title;
  public PlayerProfileInfo info;
  public PlayerProfileSlider expSlider;
  public PlayerProfileSlider vitSlider;
  public AllianceSymbol mAllianceSymbol;
  public UITexture image;
  public UISprite newHeroProfileTipNode;
  public UISprite titleSprite;
  public UITexture titleTexture;
  public UILabel titleText;
  public UILabel lordTitleUnlockedCount;
  public UIButton nominateButton;
  public UILabel btnLabel1;
  public UILabel btnLabel2;
  public GameObject achievementTip;
  public UILabel achievementCount;
  public UILabel recoverTimeLabel;
  private bool isInitAchievenment;
  private bool _isDestroy;
  private bool _iOSExchangeSwitch;
  public ArtifactSlot[] allArtifactSlot;
  public ArtifactTimeLimitedSlot artifactTimeLimitedSlot;
  public EquipmentSwitchSlot[] equipmentSwitchSlots;
  public UIGrid buttonContainer;

  private void OnDestroy()
  {
    this._isDestroy = true;
  }

  public override void OnCreate(UIControler.UIParameter param)
  {
    this.InitUI();
  }

  public override void OnOpen(UIControler.UIParameter param)
  {
  }

  public override void OnShow(UIControler.UIParameter param)
  {
    base.OnShow(param);
    if (!this.isInitAchievenment)
    {
      this.isInitAchievenment = true;
      RequestManager.inst.SendRequest("Player:loadUserAchievement", Utils.Hash((object) "uid", (object) PlayerData.inst.uid), new System.Action<bool, object>(this.OnGetAchievementCallBack), true);
    }
    else
    {
      this.UpdateUI();
      Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    }
    LordTitlePayload.Instance.GetLordTitles((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || !((UnityEngine.Object) null != (UnityEngine.Object) this))
        return;
      this.UpdateLordTitleStatus();
    }), false);
    this.AddEventHandler();
  }

  public override void OnHide(UIControler.UIParameter param)
  {
    base.OnHide(param);
    this.ClearData();
  }

  public void OnBackBtnClick()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnMoreInfoClick()
  {
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
    {
      uid = PlayerData.inst.uid
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnSkillBtnClick()
  {
    UIManager.inst.OpenDlg("Talent/TalentTreeDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnBoostBtnClick()
  {
    Utils.PopUpCommingSoon();
  }

  public void OnChangeMottoBtnClick()
  {
    UIManager.inst.OpenPopup("PlayerProfileChangeMottoPopup", (Popup.PopupParameter) new PlayerProfileChangeMottoPopup.Parameter()
    {
      closeCallBack = new System.Action(this.UpdateHeroInfo)
    });
  }

  public void OnRenameBtnClick()
  {
    UIManager.inst.OpenPopup("PlayerProfileRenamePopup", (Popup.PopupParameter) new PlayerProfileRenamePopup.Parameter()
    {
      closeCallBack = new System.Action(this.OnRenamePopupClose)
    });
  }

  public void OnNominateBtnClick()
  {
    UserData userData = PlayerData.inst.userData;
    if (userData == null)
      return;
    DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) userData.world_id);
    if (wonderData == null || !(wonderData.State == "protected"))
      return;
    MessageHub.inst.GetPortByAction("wonder:loadTitleInfo").SendRequest(new Hashtable()
    {
      {
        (object) "target_world_id",
        (object) userData.world_id
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      if (data != null)
        ConfigManager.inst.DB_WonderTitle.UpdateWonderTitleData(data as Hashtable, true);
      WonderTitleInfo wonderTitleInfo = ConfigManager.inst.DB_WonderTitle.Get(userData == null ? string.Empty : userData.Title.ToString());
      if (wonderTitleInfo == null)
        return;
      UIManager.inst.OpenPopup("Wonder/KingdomTitleInfoPopup", (Popup.PopupParameter) new KingdomTitleInfoPopup.Parameter()
      {
        kingdomId = userData.world_id,
        wonderTitleInfo = wonderTitleInfo,
        showKingBenefit = userData.IsKing,
        needViewInfo = false
      });
    }), true);
  }

  public void OnLordTitleClick()
  {
    LordTitlePayload.Instance.GetLordTitles((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("LordTitle/LordTitleDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    }), true);
  }

  private void OnRenamePopupClose()
  {
    this.UpdateHeroInfo();
  }

  private void UpdateUI()
  {
    NGUITools.SetActive(this.achievementTip.gameObject, false);
    this.RefreshAchievement();
    this.UpdateHeroInfo();
    this.UpdateSymbol();
    this.UpdateTitle();
    this.UpdateSlider();
    this.UpdateImage();
    this.UpdateArtifactStatus();
    this.UpdateEquipmentSwitchStatus();
    this.UpdateNominateBtnStatus();
    this.UpdateHeroProfileNewStatus();
    this.UpdateLordTitleStatus();
    this.buttonContainer.repositionNow = true;
    this.buttonContainer.Reposition();
  }

  private void InitUI()
  {
    this.btnLabel2.text = Utils.XLAT("player_profile_boost");
  }

  private void UpdateImage()
  {
    Utils.SetPortrait(this.image, "Texture/Hero/player_portrait_" + (object) PlayerData.inst.userData.portrait);
  }

  private void UpdateSlider()
  {
    DB.HeroData heroData = DBManager.inst.DB_hero.Get((long) PlayerData.inst.cityId);
    if (heroData != null)
    {
      if (heroData.level == 0 || ConfigManager.inst.DB_Hero_Point.MaxLevel == 0 || heroData.level == ConfigManager.inst.DB_Hero_Point.MaxLevel)
      {
        this.expSlider.slider.value = 1f;
        this.expSlider.contextLable.text = Utils.XLAT("dragon_max_level");
      }
      else
      {
        int experienceRequired1 = ConfigManager.inst.DB_Hero_Point[heroData.level].ExperienceRequired;
        int experienceRequired2 = ConfigManager.inst.DB_Hero_Point[heroData.level + 1].ExperienceRequired;
        this.expSlider.slider.value = ((float) heroData.xp - (float) experienceRequired1) / (float) (experienceRequired2 - experienceRequired1);
        this.expSlider.contextLable.text = (heroData.xp - (long) experienceRequired1).ToString() + "/" + (object) (experienceRequired2 - experienceRequired1);
      }
    }
    int num1 = 100;
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("spirit_max");
    if (data1 != null)
      num1 = data1.ValueInt;
    int num2 = 0;
    GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("prop_spirit_recover_speed_base_value");
    if (data2 != null)
      num2 = Mathf.CeilToInt((float) ConfigManager.inst.DB_BenefitCalc.GetFinalData(data2.ValueInt, "calc_spirit_recover_speed"));
    this.vitSlider.contextLable.text = heroData.spirit.ToString() + "/" + (object) num1;
    this.vitSlider.slider.value = (float) heroData.spirit / (float) num1;
    if (heroData.spirit < num1)
      this.recoverTimeLabel.text = ScriptLocalization.GetWithMultyContents("player_profile_stamina_full_in", true, Utils.FormatTime((int) (heroData.spirit_recover_time - (long) NetServerTime.inst.ServerTimestamp), true, false, true), "1/" + (object) num2);
    else
      this.recoverTimeLabel.text = "(1/" + (object) num2 + Utils.XLAT("id_sec") + ")";
  }

  private void OnSecond(int timestamp)
  {
    this.UpdateSlider();
  }

  public void OnSettingBtPress()
  {
    UIManager.inst.OpenDlg("SystemSettingDlg", (UI.Dialog.DialogParameter) new SettingDlg.Parameter()
    {
      iOSExchangeSwitch = this._iOSExchangeSwitch
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnAchievementBtPress()
  {
    UIManager.inst.OpenDlg("Achievement/AchievementDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnGetAchievementCallBack(bool success, object result)
  {
    if (this._isDestroy)
      return;
    Hashtable inData = result as Hashtable;
    if (inData != null)
      DatabaseTools.UpdateData(inData, "ios_exchange_switch", ref this._iOSExchangeSwitch);
    this.UpdateUI();
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  private void RefreshAchievement()
  {
    int completeCount = DBManager.inst.DB_Achievements.GetCompleteCount();
    NGUITools.SetActive(this.achievementTip.gameObject, completeCount > 0);
    this.achievementCount.text = completeCount < 100 ? completeCount.ToString() : "99+";
  }

  public void OnLeaderBoradBtPress()
  {
    UIManager.inst.OpenDlg("LeaderBoard/LeaderRankBoardDlg", (UI.Dialog.DialogParameter) new RankBoardDlg.Parameter()
    {
      LeaderBoardType = RankBoardDlg.LeaderBoardType.Kingdom
    }, true, true, true);
  }

  private void UpdateTitle()
  {
    this.title.text = ScriptLocalization.Get("player_profile_name", true);
  }

  private void UpdateSymbol()
  {
    AllianceData allianceData = PlayerData.inst.allianceData;
    if (allianceData != null)
    {
      this.mAllianceSymbol.gameObject.SetActive(true);
      this.mAllianceSymbol.SetSymbols(allianceData.allianceSymbolCode);
    }
    else
      this.mAllianceSymbol.gameObject.SetActive(false);
  }

  private void UpdateHeroInfo()
  {
    this.info.name.text = ScriptLocalization.Get("player_profile_playername", true) + ":";
    this.info.nameValue.text = PlayerData.inst.userData.userName;
    this.info.alliance.text = ScriptLocalization.Get("player_profile_alliance", true) + ":";
    this.info.allianceValue.text = PlayerData.inst.allianceData == null ? "--" : PlayerData.inst.allianceData.allianceFullName;
    this.info.kiongdom.text = ScriptLocalization.Get("player_profile_kingdom", true) + ":";
    this.info.kingdomValue.text = PlayerData.inst.userData.world_id.ToString();
    this.info.group.text = ScriptLocalization.Get("id_kingdom_group", true) + ":";
    this.info.groupValue.text = PlayerData.inst.userData.groupName;
    this.info.motto.text = ScriptLocalization.Get("player_profile_motto", true) + ":";
    this.info.mottoValue.text = PlayerData.inst.userData.signature;
    this.info.power.text = ScriptLocalization.Get("player_profile_power", true) + ":";
    this.info.powerValue.text = PlayerData.inst.userData.power.ToString();
    this.info.kill.text = ScriptLocalization.Get("player_profile_kills", true) + ":";
    this.info.killValue.text = PlayerData.inst.userData.troopsKilled.ToString();
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(PlayerData.inst.uid);
    if (heroData == null)
      return;
    this.btnLabel1.text = string.Format("{0} : {1}", (object) Utils.XLAT("player_profile_skill_points"), (object) heroData.skill_points);
  }

  private void UpdateHeroProfileNewStatus()
  {
    if (!(bool) ((UnityEngine.Object) this.newHeroProfileTipNode))
      return;
    NGUITools.SetActive(this.newHeroProfileTipNode.gameObject, HeroProfileUtils.HasNewHeroProfile());
  }

  private void UpdateLordTitleStatus()
  {
    int unlockedStatusCount = LordTitlePayload.Instance.GetUnlockedStatusCount();
    this.lordTitleUnlockedCount.text = unlockedStatusCount.ToString();
    NGUITools.SetActive(this.lordTitleUnlockedCount.gameObject, unlockedStatusCount > 0);
  }

  private void UpdateNominateBtnStatus()
  {
    DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) PlayerData.inst.userData.world_id);
    bool state = wonderData != null && wonderData.State == "protected" && PlayerData.inst.userData.Title > 0;
    WonderTitleInfo wonderTitleInfo = ConfigManager.inst.DB_WonderTitle.Get(PlayerData.inst.userData.Title.ToString());
    NGUITools.SetActive(this.nominateButton.gameObject, state);
    NGUITools.SetActive(this.titleSprite.gameObject, !state);
    NGUITools.SetActive(this.titleTexture.gameObject, state);
    if (!state || wonderTitleInfo == null)
      return;
    this.titleText.text = wonderTitleInfo.Name;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.titleTexture, "Texture/Kingdom/" + wonderTitleInfo.icon, (System.Action<bool>) null, true, false, string.Empty);
  }

  private void UpdateArtifactStatus()
  {
    foreach (ArtifactSlot artifactSlot in this.allArtifactSlot)
      artifactSlot.RefreshForPlayer((long) PlayerData.inst.cityId);
    this.artifactTimeLimitedSlot.RefreshForPlayer(PlayerData.inst.uid, true);
  }

  private void UpdateEquipmentSwitchStatus()
  {
    if (PlayerData.inst.heroData == null || this.equipmentSwitchSlots == null)
      return;
    for (int index = 0; index < this.equipmentSwitchSlots.Length; ++index)
      this.equipmentSwitchSlots[index].SetData(EquipmentType.PLAYER, index + 1, PlayerData.inst.heroData.EquipSwitchIndex);
  }

  public void OnButtonViewEquipmentEffectClicked()
  {
    DB.HeroData heroData = DBManager.inst.DB_hero.Get((long) PlayerData.inst.cityId);
    CityData playerCityData = PlayerData.inst.playerCityData;
    if (heroData == null || playerCityData == null)
      return;
    List<int> allEquipments = heroData.equipments.GetAllEquipments();
    List<InventoryItemData> inventoryItemDataList = new List<InventoryItemData>();
    using (List<int>.Enumerator enumerator = allEquipments.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InventoryItemData myItemByItemId = DBManager.inst.GetInventory(BagType.Hero).GetMyItemByItemID((long) enumerator.Current);
        inventoryItemDataList.Add(myItemByItemId);
      }
    }
    List<ArtifactData> artifactDataList = new List<ArtifactData>();
    using (Dictionary<int, int>.Enumerator enumerator = heroData.Artifacts.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ArtifactData artifactData = DBManager.inst.DB_Artifact.Get(PlayerData.inst.userData.world_id, enumerator.Current.Value);
        if (artifactData != null)
          artifactDataList.Add(artifactData);
      }
    }
    UIManager.inst.OpenPopup("Equipment/EquipmentEffectPopup", (Popup.PopupParameter) new EquipmentEffectPopup.Parameter()
    {
      BagType = BagType.Hero,
      AllInventoryItemData = inventoryItemDataList,
      AllArtifactData = artifactDataList
    });
  }

  private void OnHeroDataUpdated(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.UpdateEquipmentSwitchStatus();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_hero.onDataUpdated += new System.Action<long>(this.OnHeroDataUpdated);
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    DBManager.inst.DB_hero.onDataUpdated -= new System.Action<long>(this.OnHeroDataUpdated);
  }

  private void ClearData()
  {
    this.artifactTimeLimitedSlot.ClearData();
    this.RemoveEventHandler();
  }

  public void onCopyNameClick()
  {
    Utils.SetClipboardText(PlayerData.inst.userData.userName);
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_player_profile_copy_name_success", true), (System.Action) null, 1f, false);
  }
}
