﻿// Decompiled with JetBrains decompiler
// Type: StrongholdBuildingInfoItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StrongholdBuildingInfoItem : MonoBehaviour
{
  public UILabel mBuildingName;
  public UILabel mBuildingLevel;

  public void SetDetails(string buildingName, int buildingLevel)
  {
    this.mBuildingName.text = Utils.XLAT(buildingName);
    this.mBuildingLevel.text = buildingLevel.ToString();
  }
}
