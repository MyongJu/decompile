﻿// Decompiled with JetBrains decompiler
// Type: AllianceJoinHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;
using UnityEngine;

public class AllianceJoinHUD : MonoBehaviour
{
  public GameObject rootNode;

  private void OnEnable()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
    DBManager.inst.DB_Alliance.onDataUpdate -= new System.Action<long>(this.OnAllianceDataUpdated);
    DBManager.inst.DB_Alliance.onDataUpdate += new System.Action<long>(this.OnAllianceDataUpdated);
  }

  private void Start()
  {
    this.rootNode.SetActive(false);
  }

  private void OnSecondEvent(int time)
  {
    this.UpdateUI();
  }

  private void OnAllianceDataUpdated(long allianceId)
  {
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (AllianceManager.Instance.CanShowAllianceJoinEntrance)
    {
      NGUITools.SetActive(this.rootNode, true);
    }
    else
    {
      if (!this.rootNode.activeInHierarchy)
        return;
      NGUITools.SetActive(this.rootNode, false);
    }
  }

  public void OnEntrancePressed()
  {
    MessageHub.inst.GetPortByAction("Alliance:recommendOneAlliance").SendRequest(new Hashtable()
    {
      {
        (object) "lang",
        (object) Language.Instance.GetAllianceLanguage()
      }
    }, (System.Action<bool, object>) ((ret, orgData) =>
    {
      if (!ret)
        return;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return;
      AllianceSearchItemData allianceSearchItemData = new AllianceSearchItemData();
      allianceSearchItemData.Decode((object) hashtable);
      if (allianceSearchItemData.allianceID == 0L)
        return;
      UIManager.inst.OpenPopup("Alliance/AllianceRecommendationGoldEntryPopup", (Popup.PopupParameter) new AllianceRecommendationGoldEntryPopup.Parameter()
      {
        allianceSearchItemData = allianceSearchItemData
      });
    }), true);
  }
}
