﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_AlliancePortal_Rally_Attack
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;

public class AllianceWar_MainDlg_AlliancePortal_Rally_Attack : AllianceWar_MainDlg_Rally_Slot
{
  protected override void SetupBelowList(RallyData rallyData, long uid, long allianceId, int memberCount)
  {
    AllianceBossInfo allianceBossInfo = ConfigManager.inst.DB_AllianceBoss.Get(rallyData.bossId);
    AllianceWar_MainDlg_RallyList.param.ownerName = string.Format("Lv{0} {1}", (object) allianceBossInfo.bossLevel, (object) ScriptLocalization.Get(allianceBossInfo.name, true));
    AllianceWar_MainDlg_RallyList.param.rallyType = AllianceWar_MainDlg_RallyList.Params.RallyListType.BOSS;
    AllianceWar_MainDlg_RallyList.param.bossIcon = allianceBossInfo.ImagePath;
    AllianceWar_MainDlg_RallyList.param.canJoin = false;
    AllianceWar_MainDlg_RallyList.param.memberCount = 1;
    this.panel.belowList.Setup(AllianceWar_MainDlg_RallyList.param);
  }

  protected override void SetupTargetSlot(RallyData rallyData)
  {
    this.panel.targetSlot.Setup(rallyData.rallyId, 80f, true);
  }
}
