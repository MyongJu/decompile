﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightDungeonSettingPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class DragonKnightDungeonSettingPopup : Popup
{
  public UIButton m_SkillButton;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_SkillButton.isEnabled = !(false | DragonKnightSystem.Instance.RoomManager.GetCurrentProcessingEventType() == RoomEventType.BattleEvent | DragonKnightSystem.Instance.RoomManager.GetCurrentProcessingEventType() == RoomEventType.PVPEvent | DragonKnightSystem.Instance.RoomManager.GetCurrentProcessingEventType() == RoomEventType.ArenaEvent);
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnSkillSettingBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightSkillSettingPopup", (Popup.PopupParameter) null);
  }

  public void OnStepOutBtnPressed()
  {
    BattleManager.Instance.Stop();
    DragonKnightSystem.Instance.RoomManager.SyncSearchPathToServer((System.Action) (() =>
    {
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
      UIManager.inst.Cloud.CoverCloud((System.Action) (() => GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.CityMode));
    }), (Room) null);
  }

  private void Update()
  {
    if (DragonKnightSystem.Instance.IsReady)
      return;
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }
}
