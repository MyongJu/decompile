﻿// Decompiled with JetBrains decompiler
// Type: NGUIDebug
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("NGUI/Internal/Debug")]
public class NGUIDebug : MonoBehaviour
{
  private static bool mRayDebug = false;
  private static List<string> mLines = new List<string>();
  private static NGUIDebug mInstance = (NGUIDebug) null;

  public static bool debugRaycast
  {
    get
    {
      return NGUIDebug.mRayDebug;
    }
    set
    {
      if (!Application.isPlaying)
        return;
      NGUIDebug.mRayDebug = value;
      if (!value)
        return;
      NGUIDebug.CreateInstance();
    }
  }

  public static void CreateInstance()
  {
    if (!((Object) NGUIDebug.mInstance == (Object) null))
      return;
    GameObject gameObject = new GameObject("_NGUI Debug");
    NGUIDebug.mInstance = gameObject.AddComponent<NGUIDebug>();
    Object.DontDestroyOnLoad((Object) gameObject);
  }

  private static void LogString(string text)
  {
    if (Application.isPlaying)
    {
      if (NGUIDebug.mLines.Count > 20)
        NGUIDebug.mLines.RemoveAt(0);
      NGUIDebug.mLines.Add(text);
      NGUIDebug.CreateInstance();
    }
    else
      Debug.Log((object) text);
  }

  public static void Log(params object[] objs)
  {
    string text = string.Empty;
    for (int index = 0; index < objs.Length; ++index)
      text = index != 0 ? text + ", " + objs[index].ToString() : text + objs[index].ToString();
    NGUIDebug.LogString(text);
  }

  public static void Clear()
  {
    NGUIDebug.mLines.Clear();
  }

  public static void DrawBounds(Bounds b)
  {
    Vector3 center = b.center;
    Vector3 vector3_1 = b.center - b.extents;
    Vector3 vector3_2 = b.center + b.extents;
    Debug.DrawLine(new Vector3(vector3_1.x, vector3_1.y, center.z), new Vector3(vector3_2.x, vector3_1.y, center.z), Color.red);
    Debug.DrawLine(new Vector3(vector3_1.x, vector3_1.y, center.z), new Vector3(vector3_1.x, vector3_2.y, center.z), Color.red);
    Debug.DrawLine(new Vector3(vector3_2.x, vector3_1.y, center.z), new Vector3(vector3_2.x, vector3_2.y, center.z), Color.red);
    Debug.DrawLine(new Vector3(vector3_1.x, vector3_2.y, center.z), new Vector3(vector3_2.x, vector3_2.y, center.z), Color.red);
  }
}
