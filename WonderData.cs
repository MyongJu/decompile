﻿// Decompiled with JetBrains decompiler
// Type: WonderData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class WonderData
{
  public int ishot = 1;
  public int kid;
  public string kingName;
  public int kingdomColonizedBy;
  public string wonderIcon;
  public string wonderName;
  public int wonderFlag;
  public string createTime;
  public string languageGroup;

  public string GroupName
  {
    get
    {
      if (!string.IsNullOrEmpty(this.languageGroup))
        return Utils.XLAT(ConfigManager.inst.DB_KingdomConfig.languageGroupKeys[int.Parse(this.languageGroup)]);
      return (string) null;
    }
  }

  public string FlagIconPath
  {
    get
    {
      return Utils.GetFlagIconPath(this.wonderFlag);
    }
  }
}
