﻿// Decompiled with JetBrains decompiler
// Type: ParliamentHeroStar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class ParliamentHeroStar : MonoBehaviour
{
  private List<UISprite> _AllStar = new List<UISprite>();
  private const string StarSpriteHighlight = "icon_mail_star_01";
  private const string StarSpriteGrey = "hero_card_star";
  [SerializeField]
  private UISprite _StarTemplate;
  [SerializeField]
  private UITable _StarContainer;

  protected UISprite CreateStar()
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this._StarTemplate.gameObject);
    gameObject.transform.SetParent(this._StarContainer.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    UISprite component = gameObject.GetComponent<UISprite>();
    component.color = Color.white;
    this._AllStar.Add(component);
    return component;
  }

  protected void DestroyAllStar()
  {
    using (List<UISprite>.Enumerator enumerator = this._AllStar.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UISprite current = enumerator.Current;
        if ((bool) ((Object) current))
        {
          current.transform.parent = (Transform) null;
          Object.Destroy((Object) current.gameObject);
        }
      }
    }
    this._AllStar.Clear();
  }

  public void SetData(ParliamentHeroInfo parliamentHeroInfo, LegendCardData legendCardData, bool fullStarIfNoLegendCardData = false)
  {
    this._StarTemplate.gameObject.SetActive(false);
    this.DestroyAllStar();
    ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(parliamentHeroInfo.quality.ToString());
    if (parliamentHeroQualityInfo != null)
    {
      for (int index = 0; index < parliamentHeroQualityInfo.maxStar; ++index)
        this.CreateStar().spriteName = index >= parliamentHeroQualityInfo.initialStar ? "hero_card_star" : "icon_mail_star_01";
      this._StarContainer.Reposition();
    }
    if (legendCardData != null)
    {
      for (int index = 0; index < legendCardData.Star && index < this._AllStar.Count; ++index)
        this._AllStar[index].spriteName = "icon_mail_star_01";
    }
    else
    {
      if (!fullStarIfNoLegendCardData)
        return;
      using (List<UISprite>.Enumerator enumerator = this._AllStar.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.spriteName = "icon_mail_star_01";
      }
    }
  }
}
