﻿// Decompiled with JetBrains decompiler
// Type: AllianceChestPage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UI;
using UnityEngine;

public class AllianceChestPage : MonoBehaviour
{
  [SerializeField]
  private GameObject _RootHaveAllianceChestToSend;
  [SerializeField]
  private GameObject _RootAlreadySend;
  [SerializeField]
  private GameObject _RootNoChestToSend;
  [SerializeField]
  private UILabel _LabelChestCountToSend;
  [SerializeField]
  private UILabel _LabelChestCountToSendTomorrow;
  [SerializeField]
  private UILabel _LabelCurrentScore;
  [SerializeField]
  private UILabel _LabelSelfScore;
  [SerializeField]
  private UISlider _SliderCurrentProgress;
  [SerializeField]
  private UIButton _ButtonSend;
  [SerializeField]
  private UIButton _ButtonSendGrey;
  [SerializeField]
  private Transform[] _AllSplitTag;
  [SerializeField]
  private UILabel[] _LabelAllScore;
  [SerializeField]
  private UILabel[] _LabelAllChestCount;
  [SerializeField]
  private UISprite[] _SpriteAllChestIcon;
  private bool _needUpdate;

  private int AllianceChestCountToSend
  {
    get
    {
      AllianceData allianceData = PlayerData.inst.allianceData;
      if (allianceData != null)
        return allianceData.TreasureChestAmount;
      return 0;
    }
  }

  private int AllianceChestCountToSendTomorrow
  {
    get
    {
      AllianceData allianceData = PlayerData.inst.allianceData;
      if (allianceData != null)
      {
        AllianceActivityChestInfo dataByAllianceLevel = ConfigManager.inst.DB_AllianceActivityChest.GetDataByAllianceLevel(allianceData.allianceLevel);
        if (dataByAllianceLevel != null)
          return dataByAllianceLevel.GetAllChestCanGet(this.CurrentScore);
      }
      return 0;
    }
  }

  private int CurrentScore
  {
    get
    {
      return AllianceTreasuryVaultPayload.Instance.AllianceTreasuryPoint;
    }
  }

  public void NotifyUpdate()
  {
    this.UpdateUI();
    this._needUpdate = true;
  }

  protected void Update()
  {
    if (!this._needUpdate)
      return;
    this._needUpdate = false;
    AllianceTreasuryVaultPayload.Instance.LoadAllianceTreasuryPoint(new System.Action(this.UpdateUI));
  }

  private float CalcProgressDelta(AllianceActivityChestInfo allianceActivityChestInfo)
  {
    float num = 0.0f;
    if (allianceActivityChestInfo != null)
      num = 1f / (float) (allianceActivityChestInfo.AllRequireScore.Length - 1);
    return num;
  }

  private float CalcProgress(AllianceActivityChestInfo allianceActivityChestInfo, int currentScore)
  {
    float num1 = this.CalcProgressDelta(allianceActivityChestInfo);
    float num2 = 0.0f;
    if (allianceActivityChestInfo != null)
    {
      int index = 0;
      while (index < allianceActivityChestInfo.AllRequireScore.Length && currentScore > allianceActivityChestInfo.AllRequireScore[index])
        ++index;
      if (index >= allianceActivityChestInfo.AllRequireScore.Length)
        num2 = 1f;
      else if (index > 0 && index < allianceActivityChestInfo.AllRequireScore.Length)
      {
        float num3 = num2 + (float) (index - 1) * num1;
        float num4 = (float) (allianceActivityChestInfo.AllRequireScore[index] - allianceActivityChestInfo.AllRequireScore[index - 1]);
        float num5 = (float) (currentScore - allianceActivityChestInfo.AllRequireScore[index - 1]);
        num2 = num3 + num5 / num4 * num1;
      }
      else
        num2 = 0.0f;
    }
    return num2;
  }

  protected void UpdateUI()
  {
    AllianceData allianceData = PlayerData.inst.allianceData;
    if (allianceData == null)
      return;
    this._RootHaveAllianceChestToSend.SetActive(allianceData.CanSendTreasureChest && allianceData.TreasureChestAmount > 0);
    this._RootAlreadySend.SetActive(!allianceData.CanSendTreasureChest && allianceData.TreasureChestAmount > 0);
    this._RootNoChestToSend.SetActive(allianceData.TreasureChestAmount <= 0);
    bool flag = allianceData.TreasureChestAmount > 0;
    this._ButtonSend.gameObject.SetActive(flag && Utils.IsR4Member());
    this._ButtonSendGrey.gameObject.SetActive(flag && !Utils.IsR4Member());
    this._LabelChestCountToSend.text = string.Format("x{0}", (object) this.AllianceChestCountToSend);
    this._LabelChestCountToSendTomorrow.text = string.Format("x{0}", (object) this.AllianceChestCountToSendTomorrow);
    this._LabelCurrentScore.text = this.CurrentScore.ToString();
    this._LabelSelfScore.text = DBManager.inst.DB_DailyActives.CurrentPoint.ToString();
    AllianceActivityChestInfo dataByAllianceLevel = ConfigManager.inst.DB_AllianceActivityChest.GetDataByAllianceLevel(allianceData.allianceLevel);
    if (dataByAllianceLevel != null)
    {
      float num = this.CalcProgressDelta(dataByAllianceLevel);
      for (int index = 0; index < dataByAllianceLevel.AllRequireScore.Length && index < this._LabelAllScore.Length && (index < this._LabelAllChestCount.Length && index < this._SpriteAllChestIcon.Length); ++index)
      {
        this._LabelAllScore[index].text = dataByAllianceLevel.AllRequireScore[index].ToString();
        this._LabelAllChestCount[index].text = string.Format("x{0}", (object) dataByAllianceLevel.AllChestValue[index]);
        if (this.CurrentScore >= dataByAllianceLevel.AllRequireScore[index])
          this._SpriteAllChestIcon[index].color = Color.white;
        else
          this._SpriteAllChestIcon[index].color = Color.blue;
        this._SliderCurrentProgress.value = (float) index * num;
        if (index < this._AllSplitTag.Length)
          this._AllSplitTag[index].localPosition = this._SliderCurrentProgress.thumb.localPosition;
      }
    }
    this._SliderCurrentProgress.value = this.CalcProgress(dataByAllianceLevel, this.CurrentScore);
  }

  public void OnButtonSendAllianceChestClicked()
  {
    if (Utils.IsR4Member())
    {
      Hashtable postData = new Hashtable()
      {
        {
          (object) "uid",
          (object) PlayerData.inst.uid
        },
        {
          (object) "alliance_id",
          (object) PlayerData.inst.allianceId
        }
      };
      RequestManager.inst.SendRequest("TreasuryVault:sendAllianceTreasury", postData, (System.Action<bool, object>) ((result, data) =>
      {
        this.UpdateUI();
        if (!result)
          return;
        AudioManager.Instance.StopAndPlaySound("sfx_city_jackpot_item_special");
      }), true);
    }
    else
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_alliance_giftbox_insufficient_rank", true), (System.Action) null, 4f, true);
  }

  public void OnButtonGetScoreClicked()
  {
    UIManager.inst.OpenPopup("DailyActivy/DailyActiviesPopup", (Popup.PopupParameter) null);
  }
}
