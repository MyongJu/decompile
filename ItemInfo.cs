﻿// Decompiled with JetBrains decompiler
// Type: ItemInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ItemInfo : MonoBehaviour
{
  public UILabel name;
  public UISprite icon;

  public void SeedData(ItemInfo.Data d)
  {
    this.name.text = d.name;
    this.icon.spriteName = d.icon;
  }

  public class Data
  {
    public string name;
    public string icon;
  }
}
