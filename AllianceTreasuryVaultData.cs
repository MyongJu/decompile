﻿// Decompiled with JetBrains decompiler
// Type: AllianceTreasuryVaultData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class AllianceTreasuryVaultData : IComparable<AllianceTreasuryVaultData>
{
  public string senderName = string.Empty;
  public List<AllianceTreasuryVaultData.ClaimedInfo> allClaimedInfo = new List<AllianceTreasuryVaultData.ClaimedInfo>();
  public const string STATUS_NONE = "none";
  public const string STATUS_CLAIMED = "claimed";
  public const string STATUS_DONE = "done";
  public const string STATUS_EXPIRED = "expired";
  public long allianceId;
  public long id;
  public long senderUid;
  public string type;
  public int infoHidden;
  public int configId;
  public int personalConfigId;
  public int cTime;
  public string status;
  public int expireTime;
  public int totalClaimed;
  public string senderIcon;
  public int senderPortrait;
  public int senderLordTitle;

  public int AlreadySendGold
  {
    get
    {
      int num = 0;
      using (List<AllianceTreasuryVaultData.ClaimedInfo>.Enumerator enumerator = this.allClaimedInfo.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AllianceTreasuryVaultData.ClaimedInfo current = enumerator.Current;
          num += current.gold;
        }
      }
      return num;
    }
  }

  public bool HaveClaimed(long uid)
  {
    using (List<AllianceTreasuryVaultData.ClaimedInfo>.Enumerator enumerator = this.allClaimedInfo.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.uid == uid)
          return true;
      }
    }
    return false;
  }

  public int ShowOrder
  {
    get
    {
      bool flag = this.HaveClaimed(PlayerData.inst.uid);
      return (!(this.status == "none") || flag ? (!(this.status == "none") || !flag ? 3 : 2) : 1) * 100 + (!(this.type == "alliance") ? 2 : 1);
    }
  }

  public int CompareTo(AllianceTreasuryVaultData other)
  {
    if (this.ShowOrder == other.ShowOrder)
      return this.cTime.CompareTo(other.cTime);
    return this.ShowOrder.CompareTo(other.ShowOrder);
  }

  public void Decode(Hashtable hashtable)
  {
    DatabaseTools.UpdateData(hashtable, "alliance_id", ref this.allianceId);
    DatabaseTools.UpdateData(hashtable, "id", ref this.id);
    DatabaseTools.UpdateData(hashtable, "sender_uid", ref this.senderUid);
    DatabaseTools.UpdateData(hashtable, "sender_uname", ref this.senderName);
    DatabaseTools.UpdateData(hashtable, "type", ref this.type);
    DatabaseTools.UpdateData(hashtable, "info_hidden", ref this.infoHidden);
    DatabaseTools.UpdateData(hashtable, "config_id", ref this.configId);
    DatabaseTools.UpdateData(hashtable, "status", ref this.status);
    DatabaseTools.UpdateData(hashtable, "expire_time", ref this.expireTime);
    DatabaseTools.UpdateData(hashtable, "total_claimed", ref this.totalClaimed);
    DatabaseTools.UpdateData(hashtable, "sender_icon", ref this.senderIcon);
    DatabaseTools.UpdateData(hashtable, "sender_portrait", ref this.senderPortrait);
    DatabaseTools.UpdateData(hashtable, "personal_config_id", ref this.personalConfigId);
    DatabaseTools.UpdateData(hashtable, "ctime", ref this.cTime);
    DatabaseTools.UpdateData(hashtable, "sender_lord_title", ref this.senderLordTitle);
    this.allClaimedInfo.Clear();
    if (!hashtable.ContainsKey((object) "claimed_info"))
      return;
    ArrayList arrayList = hashtable[(object) "claimed_info"] as ArrayList;
    if (arrayList != null)
    {
      foreach (object obj in arrayList)
      {
        Hashtable inData = obj as Hashtable;
        if (inData != null)
        {
          AllianceTreasuryVaultData.ClaimedInfo claimedInfo = new AllianceTreasuryVaultData.ClaimedInfo();
          DatabaseTools.UpdateData(inData, "uid", ref claimedInfo.uid);
          DatabaseTools.UpdateData(inData, "uname", ref claimedInfo.userName);
          DatabaseTools.UpdateData(inData, "gold", ref claimedInfo.gold);
          claimedInfo.isLuckiest = false;
          this.allClaimedInfo.Add(claimedInfo);
        }
      }
    }
    if (!(this.status == "done"))
      return;
    int num = 0;
    int index1 = 0;
    for (int index2 = 0; index2 < this.allClaimedInfo.Count; ++index2)
    {
      if (this.allClaimedInfo[index2].gold > num)
      {
        num = this.allClaimedInfo[index2].gold;
        index1 = index2;
      }
    }
    if (index1 >= this.allClaimedInfo.Count)
      return;
    this.allClaimedInfo[index1].isLuckiest = true;
  }

  public class ClaimedInfo
  {
    public long uid;
    public string userName;
    public int gold;
    public bool isLuckiest;
  }
}
