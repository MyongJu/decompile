﻿// Decompiled with JetBrains decompiler
// Type: NLPTrans.NLPUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using Funplus.Internal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace NLPTrans
{
  public class NLPUtils
  {
    private static readonly object locker = new object();
    internal static Dictionary<int, string> RFCEncodingSchemes = new Dictionary<int, string>()
    {
      {
        3986,
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~"
      },
      {
        1738,
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_."
      }
    };
    private static string ValidPathCharacters = NLPUtils.DetermineValidPathCharacters();
    private Stopwatch watch = Stopwatch.StartNew();
    public const string postUrl = "http://translate.funplusgame.com/api/v2/translate";
    public const string reportUrl = "http://translate.funplusgame.com/api/v1/rate";
    public const string endpointHost = "translate.funplusgame.com";
    public const string endpointUri = "/api/v2/translate";
    public const string reportEndpointUri = "/api/v1/rate";
    private const long timeOutMilliseconds = 5000;
    public const string ISO8601DateFormatNoMS = "yyyy-MM-dd\\THH:mm:ss\\Z";
    public const string ValidUrlCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";
    public const string ValidUrlCharactersRFC1738 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.";
    private static NLPUtils instance;
    private NLPMachine machine;
    public System.Action<string, string, string> OnNLPResult;

    public static NLPUtils Instance
    {
      get
      {
        if (NLPUtils.instance == null)
        {
          lock (NLPUtils.locker)
            NLPUtils.instance = new NLPUtils();
        }
        return NLPUtils.instance;
      }
    }

    public void MachineTranslate(string source, string targetLanguage, string sourceLanguage, string uid, string usingCase, System.Action<bool, object> callback)
    {
      if (this.machine == null)
        this.machine = new NLPMachine();
      this.machine.Translate(source, targetLanguage, sourceLanguage, uid, usingCase, callback);
    }

    public void TranslateAsyc(string source, string targetLanguage, string sourceLanguage, string uid, string usingCase, System.Action<bool, string, string> onResult)
    {
      string empty = string.Empty;
      string str = new DateTime(1970, 1, 1).AddSeconds((double) NetServerTime.inst.ServerTimestamp).ToString("yyyy-MM-dd\\THH:mm:ss\\Z");
      IDictionary<string, string> parameters = (IDictionary<string, string>) new Dictionary<string, string>();
      parameters.Add(nameof (source), sourceLanguage);
      parameters.Add("target", targetLanguage);
      parameters.Add("q", source);
      parameters.Add("timeStamp", str);
      parameters.Add("appId", FunplusSettings.FunplusGameId);
      parameters.Add("userId", uid);
      if (PlayerData.inst.NLPEngineValue == PlayerData.NLPEngine.BING)
        parameters.Add("debug", "2");
      NLPParam nlpParam = NLPUtils.PrepareRequest(parameters, "/api/v2/translate", "http://translate.funplusgame.com/api/v2/translate");
      GameDataManager.inst.StartCoroutine(this.SendRequest(source, targetLanguage, sourceLanguage, uid, usingCase, onResult, nlpParam));
    }

    [DebuggerHidden]
    private IEnumerator SendRequest(string source, string targetLanguage, string sourceLanguage, string uid, string usingCase, System.Action<bool, string, string> onResult, NLPParam param)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new NLPUtils.\u003CSendRequest\u003Ec__Iterator86()
      {
        param = param,
        onResult = onResult,
        source = source,
        targetLanguage = targetLanguage,
        usingCase = usingCase,
        \u003C\u0024\u003Eparam = param,
        \u003C\u0024\u003EonResult = onResult,
        \u003C\u0024\u003Esource = source,
        \u003C\u0024\u003EtargetLanguage = targetLanguage,
        \u003C\u0024\u003EusingCase = usingCase,
        \u003C\u003Ef__this = this
      };
    }

    private void OnRequestDone(WWW www, System.Action<bool, string, string> onResult, string source, string targetLanguage, string usingCase)
    {
      string str1 = string.Empty;
      bool flag = false;
      string empty = string.Empty;
      if (www.isDone)
      {
        Hashtable hashtable1 = Utils.Json2Object(www.text, true) as Hashtable;
        if (hashtable1 != null && hashtable1.ContainsKey((object) "errorCode"))
        {
          string str2 = hashtable1[(object) "errorCode"].ToString();
          if (int.Parse(str2) == 0)
          {
            Hashtable hashtable2 = hashtable1[(object) "translation"] as Hashtable;
            str1 = hashtable2[(object) "targetText"].ToString();
            flag = true;
            if (hashtable2.ContainsKey((object) nameof (source)))
              empty = hashtable2[(object) nameof (source)].ToString();
          }
          else
          {
            D.error((object) ("nlp error " + str2));
            str1 = source;
            flag = false;
            NetWorkDetector.Instance.SaveRUMData("NLP_ERROR", str2, string.Empty, 0, 0, 0);
          }
        }
      }
      else
      {
        str1 = source;
        flag = false;
        D.error((object) "nlp error : time out");
        NetWorkDetector.Instance.SaveRUMData("NLP_TIME_OUT", string.Empty, string.Empty, 0, 0, 0);
      }
      FunplusBi.Instance.TraceEvent("nlp", Utils.Object2Json((object) new BIDataFormat()
      {
        d_c1 = new BIStep()
        {
          key = nameof (source),
          value = empty
        },
        d_c2 = new BIStep()
        {
          key = "target",
          value = targetLanguage
        },
        d_c3 = new BIStep()
        {
          key = nameof (usingCase),
          value = usingCase
        },
        d_c4 = new BIStep()
        {
          key = "time",
          value = NetServerTime.inst.ServerTimestamp.ToString()
        }
      }));
      if (onResult == null)
        return;
      onResult(flag, str1, empty);
    }

    public void ReportTranslateAsyc(string machineTargetText, string source, string targetLanguage, string sourceLanguage, string uid, System.Action<bool> onResult)
    {
      string empty = string.Empty;
      string str = new DateTime(1970, 1, 1).AddSeconds((double) NetServerTime.inst.ServerTimestamp).ToString("yyyy-MM-dd\\THH:mm:ss\\Z");
      IDictionary<string, string> parameters = (IDictionary<string, string>) new Dictionary<string, string>();
      parameters.Add(nameof (source), sourceLanguage);
      parameters.Add("target", targetLanguage);
      parameters.Add("sourceText", source);
      parameters.Add("timeStamp", str);
      parameters.Add(nameof (machineTargetText), machineTargetText);
      parameters.Add("appId", FunplusSettings.FunplusGameId);
      parameters.Add("userId", uid);
      parameters.Add("score", "1");
      NLPParam nlpParam = NLPUtils.PrepareRequest(parameters, "/api/v1/rate", "http://translate.funplusgame.com/api/v1/rate");
      GameDataManager.inst.StartCoroutine(this.SendReportRequest(machineTargetText, source, targetLanguage, sourceLanguage, uid, onResult, nlpParam));
    }

    [DebuggerHidden]
    private IEnumerator SendReportRequest(string machineTargetText, string source, string targetLanguage, string sourceLanguage, string uid, System.Action<bool> onResult, NLPParam param)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new NLPUtils.\u003CSendReportRequest\u003Ec__Iterator87()
      {
        param = param,
        onResult = onResult,
        \u003C\u0024\u003Eparam = param,
        \u003C\u0024\u003EonResult = onResult,
        \u003C\u003Ef__this = this
      };
    }

    private void OnReportDone(WWW www, System.Action<bool> onResult)
    {
      bool flag = false;
      if (www.isDone)
      {
        Hashtable hashtable = Utils.Json2Object(www.text, true) as Hashtable;
        if (hashtable != null && hashtable.ContainsKey((object) "errorCode"))
        {
          string s = hashtable[(object) "errorCode"].ToString();
          if (int.Parse(s) == 0)
          {
            flag = true;
          }
          else
          {
            D.error((object) ("nlp report error " + s));
            flag = false;
          }
        }
      }
      else
      {
        flag = false;
        D.error((object) "nlp error : time out");
      }
      if (onResult != null)
        onResult(flag);
      FunplusBi.Instance.TraceEvent("nlp_report", Utils.Object2Json((object) new BIDataFormat()
      {
        d_c1 = new BIStep()
        {
          key = "engine",
          value = PlayerData.inst.NLPEngineValue.ToString()
        },
        d_c2 = new BIStep()
        {
          key = "time",
          value = NetServerTime.inst.ServerTimestamp.ToString()
        }
      }));
    }

    private static string SignAndBase64Encode(string data, string key)
    {
      byte[] hash;
      using (HMACSHA256 hmacshA256 = new HMACSHA256(Encoding.UTF8.GetBytes(key)))
        hash = hmacshA256.ComputeHash(Encoding.UTF8.GetBytes(data));
      return Convert.ToBase64String(hash);
    }

    public static string UrlEncode(string data, bool path)
    {
      return NLPUtils.UrlEncode(3986, data, path);
    }

    public static string UrlEncode(int rfcNumber, string data, bool path)
    {
      StringBuilder stringBuilder = new StringBuilder(data.Length * 2);
      string str1;
      if (!NLPUtils.RFCEncodingSchemes.TryGetValue(rfcNumber, out str1))
        str1 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";
      string str2 = str1 + (!path ? string.Empty : NLPUtils.ValidPathCharacters);
      foreach (char ch in Encoding.UTF8.GetBytes(data))
      {
        if (str2.IndexOf(ch) != -1)
          stringBuilder.Append(ch);
        else
          stringBuilder.Append("%").Append(string.Format((IFormatProvider) CultureInfo.InvariantCulture, "{0:X2}", new object[1]
          {
            (object) (int) ch
          }));
      }
      return stringBuilder.ToString();
    }

    private static string DetermineValidPathCharacters()
    {
      StringBuilder stringBuilder = new StringBuilder();
      foreach (char ch in "/:'()!*[]")
      {
        string str = Uri.EscapeUriString(ch.ToString());
        if (str.Length == 1 && (int) str[0] == (int) ch)
          stringBuilder.Append(ch);
      }
      return stringBuilder.ToString();
    }

    public static NLPParam PrepareRequest(IDictionary<string, string> parameters, string uri, string url)
    {
      NLPParam nlpParam = new NLPParam();
      IDictionary<string, string> dictionary1 = (IDictionary<string, string>) new SortedDictionary<string, string>(parameters, (IComparer<string>) StringComparer.Ordinal);
      StringBuilder stringBuilder1 = new StringBuilder();
      foreach (KeyValuePair<string, string> keyValuePair in (IEnumerable<KeyValuePair<string, string>>) dictionary1)
      {
        if (keyValuePair.Value != null)
        {
          stringBuilder1.Append(NLPUtils.UrlEncode(keyValuePair.Key, false));
          stringBuilder1.Append("=");
          stringBuilder1.Append(NLPUtils.UrlEncode(keyValuePair.Value, false));
          stringBuilder1.Append("&");
        }
      }
      stringBuilder1.Remove(stringBuilder1.Length - 1, 1);
      byte[] bytes = Encoding.UTF8.GetBytes(stringBuilder1.ToString());
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.Append("POST").AppendLine();
      stringBuilder2.Append("translate.funplusgame.com").AppendLine();
      stringBuilder2.Append(NLPUtils.UrlEncode(uri, true)).AppendLine();
      stringBuilder2.Append(stringBuilder1.ToString());
      string str = NLPUtils.SignAndBase64Encode(stringBuilder2.ToString(), FunplusSettings.FunplusGameKey);
      Dictionary<string, string> dictionary2 = new Dictionary<string, string>();
      dictionary2["Content-Type"] = "application/x-www-form-urlencoded";
      dictionary2["Accept-Encoding"] = "gzip";
      dictionary2[HttpRequestHeader.Authorization.ToString()] = str;
      nlpParam.configHeaders = dictionary2;
      nlpParam.requestContent = bytes;
      nlpParam.url = url;
      return nlpParam;
    }
  }
}
