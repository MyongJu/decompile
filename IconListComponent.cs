﻿// Decompiled with JetBrains decompiler
// Type: IconListComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class IconListComponent : ComponentRenderBase
{
  private bool _autoFill = true;
  public List<Icon> icons;

  public void StopAutoFillIconName()
  {
    this._autoFill = false;
  }

  public override void Init()
  {
    List<IconData> data = (this.data as IconListComponentData).data;
    for (int index = 0; index < this.icons.Count; ++index)
    {
      if (index < data.Count)
      {
        if (!this._autoFill)
          this.icons[index].StopAutoFillName();
        if (data[index].Data == null)
          this.icons[index].LoadDefaultBackgroud();
        this.icons[index].FeedData((IComponentData) data[index]);
        this.icons[index].OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
        this.icons[index].OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
        this.icons[index].OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
        this.icons[index].gameObject.SetActive(true);
      }
      else
        this.icons[index].gameObject.SetActive(false);
    }
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  public override void Dispose()
  {
    using (List<Icon>.Enumerator enumerator = this.icons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Dispose();
    }
  }
}
