﻿// Decompiled with JetBrains decompiler
// Type: AttackShowDragonController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackShowDragonController : MonoBehaviour
{
  public float movetime = 1f;
  public const float CITY_RADIUS = 2f;
  public Transform _modelsParent;
  public float scale;
  public Transform target;
  public Transform source;
  public DragonData.Tendency dragonType;
  private Vector3 oldpos;
  private float beginMoveTime;
  [HideInInspector]
  private MarchView _marchView;
  private MarchViewControler.Data md;
  private AttackShowDragonController.TroopState ts;

  public void Init()
  {
    GameObject gameObject = new GameObject();
    gameObject.transform.parent = this._modelsParent;
    gameObject.transform.localPosition = Vector3.zero;
    this._marchView = (MarchView) gameObject.AddComponent<ArmyView>();
    gameObject.name = "army";
    this._marchView.Init((System.Action) (() =>
    {
      this.GenerateTroops();
      this._modelsParent.transform.localScale = new Vector3(this.scale, this.scale, this.scale);
    }));
  }

  private void Update()
  {
    if (this.ts == AttackShowDragonController.TroopState.Move)
    {
      this._marchView.transform.position = Vector3.Lerp(this.md.startPosition, this.md.moveTargetPosition, (Time.time - this.beginMoveTime) / this.movetime);
      if ((double) Time.time - (double) this.beginMoveTime >= (double) this.movetime)
      {
        this._marchView.transform.position = this.md.moveTargetPosition;
        this.ts = AttackShowDragonController.TroopState.Action;
      }
      else
        this.oldpos = this._marchView.transform.position;
    }
    if (this.ts != AttackShowDragonController.TroopState.Action)
      return;
    this.AttackTarget();
    this.ts = AttackShowDragonController.TroopState.None;
  }

  private void GenerateTroops()
  {
    this.md = new MarchViewControler.Data();
    DB.TroopsInfo troopsInfo = new DB.TroopsInfo();
    troopsInfo.troops = new Dictionary<string, Unit>();
    Hashtable troops = AttackShowManager.Instance.Param.troops;
    if (troops != null)
    {
      IEnumerator enumerator = troops.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator.Current.ToString());
        string s = troops[enumerator.Current].ToString();
        int result = 0;
        if (data != null && int.TryParse(s, out result))
        {
          Unit unit = new Unit(data.Type, data.Troop_Tier, result);
          troopsInfo.troops.Add(data.ID, unit);
        }
      }
    }
    this.md.troops = troopsInfo;
    this.md.marchData = new MarchData()
    {
      dragon = AttackShowManager.Instance.Param.dragon
    };
    this.md.startPosition = this.source.position;
    this.md.endPosition = this.target.position;
    this.md.moveTargetPosition = this.CalcPoint(this.md.endPosition, -1);
    this.md.startTime = 0.0;
    this.md.endTime = (double) this.movetime;
    double num = (double) this._marchView.Setup(this.md, true, (System.Action) (() =>
    {
      this._marchView.Show();
      this.transform.position = this.md.startPosition;
      this._marchView.Rotate(this.md.dirV3);
      Utils.SetLayer(this._marchView.gameObject, LayerMask.NameToLayer("Cinematic"));
    }));
  }

  public void BeginMove()
  {
    this.Init();
    this.ts = AttackShowDragonController.TroopState.Move;
    this.beginMoveTime = Time.time;
    this.oldpos = this._marchView.transform.position;
  }

  private void AttackTarget()
  {
    float radius = 0.0f;
    this._marchView.Action(0, this.GetDeltaPosition(ref radius), radius);
    Utils.SetLayer(this._marchView.gameObject, LayerMask.NameToLayer("Cinematic"));
  }

  private Vector3 CalcPoint(Vector3 point, int dir)
  {
    float radius = 0.0f;
    Vector3 deltaPosition = this.GetDeltaPosition(ref radius);
    return point + (float) dir * deltaPosition;
  }

  private Vector3 GetDeltaPosition(ref float radius)
  {
    Vector3 vector3 = this.md.endPosition - this.md.startPosition;
    vector3.Normalize();
    radius = 2f;
    return vector3 * radius;
  }

  public void ReleaseTroops()
  {
    if (!((UnityEngine.Object) this._marchView != (UnityEngine.Object) null) || !((UnityEngine.Object) this._marchView.gameObject != (UnityEngine.Object) null))
      return;
    this._marchView.Dispose();
    UnityEngine.Object.Destroy((UnityEngine.Object) this._marchView.gameObject);
    this._marchView = (MarchView) null;
  }

  private enum TroopState
  {
    None,
    Move,
    Action,
  }
}
