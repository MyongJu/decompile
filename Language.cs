﻿// Decompiled with JetBrains decompiler
// Type: Language
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UnityEngine;

public class Language
{
  private Language.Configuration m_Configuration = new Language.Configuration();
  public const string TRANSLATOR_LOCALE = "Translator Locale";
  public const string LANGUAGE_CONFIG_PATH = "TextAsset/Language/Language";
  private static Language m_Instance;

  private Language()
  {
  }

  public static Language Instance
  {
    get
    {
      if (Language.m_Instance == null)
        Language.m_Instance = new Language();
      return Language.m_Instance;
    }
  }

  public List<string> Localization
  {
    get
    {
      return this.m_Configuration.localization;
    }
  }

  public List<string> Communication
  {
    get
    {
      return this.m_Configuration.communication;
    }
  }

  public List<string> Translator
  {
    get
    {
      return this.m_Configuration.translator;
    }
  }

  public void Initialize()
  {
    TextAsset textAsset = AssetManager.Instance.Load("TextAsset/Language/Language", (System.Type) null) as TextAsset;
    if ((bool) ((UnityEngine.Object) textAsset))
      this.m_Configuration = JsonReader.Deserialize<Language.Configuration>(textAsset.text);
    else
      D.error((object) "[Language]Initialize: Failed to load 'TextAsset/Language/Language'");
    AssetManager.Instance.UnLoadAsset("TextAsset/Language/Language", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    if (!PlayerPrefs.HasKey("I2 Language"))
      LocalizationManager.CurrentLanguageCode = Language.Instance.GetLocalizationLanguage();
    if (PlayerPrefs.HasKey("Translator Locale"))
      return;
    string systemLanguage = Language.Instance.GetSystemLanguage();
    PlayerPrefs.SetString("Translator Locale", !string.IsNullOrEmpty(systemLanguage) ? systemLanguage : "en");
  }

  public string GetLanguageName(string locale)
  {
    for (int index = 0; index < this.m_Configuration.languages.Count; ++index)
    {
      if (this.m_Configuration.languages[index].locale == locale)
        return ScriptLocalization.Get(this.m_Configuration.languages[index].display, true);
    }
    return string.Empty;
  }

  public string GetSystemLanguage()
  {
    for (int index = 0; index < this.m_Configuration.languages.Count; ++index)
    {
      if (Application.systemLanguage == SystemLanguage.Chinese)
      {
        string language = FunplusSdkUtils.Instance.GetLanguage();
        if (string.IsNullOrEmpty(language))
          return "zh-CN";
        if (language.Equals("zh-HK") || language.Equals("zh-TW"))
          return "zh-TW";
        if (language.Equals("zh-CN") || language.StartsWith("zh-Hans"))
          return "zh-CN";
        return language.StartsWith("zh-Hant") ? "zh-TW" : "zh-TW";
      }
      if (this.m_Configuration.languages[index].language == Application.systemLanguage)
        return this.m_Configuration.languages[index].locale;
    }
    return string.Empty;
  }

  public string GetAllianceLanguage()
  {
    string systemLanguage = this.GetSystemLanguage();
    if (string.IsNullOrEmpty(systemLanguage))
      return "all";
    return systemLanguage;
  }

  public string GetLocalizationLanguage()
  {
    string systemLanguage = this.GetSystemLanguage();
    if (this.Localization.Contains(systemLanguage))
      return systemLanguage;
    return "en";
  }

  public static string TranslateTarget
  {
    get
    {
      string str = PlayerPrefs.GetString("Translator Locale", "en");
      if (str != "all")
        return str;
      return "en";
    }
  }

  public class Data
  {
    public string locale;
    public string display;
    public SystemLanguage language;
  }

  public class Configuration
  {
    public List<Language.Data> languages = new List<Language.Data>();
    public List<string> localization = new List<string>();
    public List<string> communication = new List<string>();
    public List<string> translator = new List<string>();
  }
}
