﻿// Decompiled with JetBrains decompiler
// Type: IAPStorePackagePanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class IAPStorePackagePanel : MonoBehaviour
{
  public UILabel m_PackageName;
  public UITexture m_Post;
  public UITexture m_PostBG;
  public UILabel m_GoldCount1;
  public UILabel m_GoldCount2;
  public UILabel m_GoldBonus;
  public UILabel m_AskBuy;
  public UILabel m_Discount;
  public UILabel m_Duration;
  public UILabel m_Tip;
  public UILabel m_Price1;
  public UILabel m_Price2;
  public System.Action OnPurchaseBegin;
  public System.Action<int> OnPurchaseEnd;
  private IAPStorePackage m_StorePackage;
  public System.Action OnDetailButtonClicked;

  public void SetData(IAPStorePackage package)
  {
    this.m_StorePackage = package;
    this.UpdateUI();
  }

  public void OnMore()
  {
    UIManager.inst.OpenPopup("IAP/IAPStoreMoreInfoPopup", (Popup.PopupParameter) new IAPStorePackageMorePopup.Parameter()
    {
      package = this.m_StorePackage,
      OnPurchaseBegin = this.OnPurchaseBegin,
      OnPurchaseEnd = this.OnPurchaseEnd
    });
    if (this.OnDetailButtonClicked == null)
      return;
    this.OnDetailButtonClicked();
  }

  public void OnBuy()
  {
    if (this.OnPurchaseBegin != null)
      this.OnPurchaseBegin();
    IAPStorePackagePayload.Instance.BuyProduct(this.m_StorePackage.package.productId, this.m_StorePackage.id, this.m_StorePackage.group_id, (System.Action) (() =>
    {
      if (this.OnPurchaseEnd == null)
        return;
      this.OnPurchaseEnd(this.m_StorePackage.group.internalId);
    }));
  }

  private void UpdateUI()
  {
    IAPPackageInfo package = this.m_StorePackage.package;
    if ((bool) ((UnityEngine.Object) this.m_PackageName))
      this.m_PackageName.text = ScriptLocalization.Get(this.m_StorePackage.group.name, true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Post, this.m_StorePackage.group.GraphPath, (System.Action<bool>) null, true, false, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_PostBG, this.m_StorePackage.group.GraphBackGroudPath, (System.Action<bool>) null, false, true, string.Empty);
    UILabel goldCount2 = this.m_GoldCount2;
    string str1 = ScriptLocalization.Get("id_gold", true) + " +" + Utils.FormatThousands(package.gold.ToString());
    this.m_GoldCount1.text = str1;
    string str2 = str1;
    goldCount2.text = str2;
    this.m_GoldBonus.text = ScriptLocalization.GetWithPara("iap_gold_package_contents_value_description", new Dictionary<string, string>()
    {
      {
        "0",
        Utils.FormatThousands(package.bonus_gold.ToString())
      }
    }, true);
    this.m_Discount.text = package.discount.ToString() + "%";
    this.UpdateDuration();
    this.UpdateTimes();
    this.m_Price1.text = PaymentManager.Instance.GetCurrencyCode(this.m_StorePackage.package.productId, this.m_StorePackage.package.pay_id) + Utils.FormatThousands((PaymentManager.Instance.GetRealPrice(this.m_StorePackage.package.productId, this.m_StorePackage.package.pay_id) * package.discount / 100.0).ToString("f2"));
    this.m_Price2.text = PaymentManager.Instance.GetFormattedPrice(this.m_StorePackage.package.productId, this.m_StorePackage.package.pay_id);
    this.m_AskBuy.text = ScriptLocalization.Get(package.description, true);
  }

  private void UpdateDuration()
  {
    if (this.m_StorePackage == null)
      return;
    int time = (int) this.m_StorePackage.endTime - NetServerTime.inst.ServerTimestamp;
    this.m_Duration.text = time <= 0 ? Utils.FormatTime(0, true, false, true) : Utils.FormatTime(time, true, false, true);
    if (time >= -1)
      return;
    IAPStorePackagePayload.Instance.PushPackageDataChanged();
  }

  private void UpdateTimes()
  {
    if (this.m_StorePackage == null)
      return;
    if (this.m_StorePackage.PurchaseCount == 0L && this.m_StorePackage.package.times == 1)
      this.m_Tip.text = ScriptLocalization.Get("iap_gold_once", true);
    else
      this.m_Tip.text = string.Format(ScriptLocalization.Get("iap_gold_num_remaining", true), (object) this.m_StorePackage.LeftPurchaseCount);
  }

  private void Update()
  {
    this.UpdateDuration();
    this.UpdateTimes();
  }
}
