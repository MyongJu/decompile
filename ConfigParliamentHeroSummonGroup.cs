﻿// Decompiled with JetBrains decompiler
// Type: ConfigParliamentHeroSummonGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigParliamentHeroSummonGroup
{
  private List<ParliamentHeroSummonGroupInfo> _parliamentHeroSummonGroupInfoList = new List<ParliamentHeroSummonGroupInfo>();
  private Dictionary<string, ParliamentHeroSummonGroupInfo> _datas;
  private Dictionary<int, ParliamentHeroSummonGroupInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ParliamentHeroSummonGroupInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, ParliamentHeroSummonGroupInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._parliamentHeroSummonGroupInfoList.Add(enumerator.Current);
    }
    this._parliamentHeroSummonGroupInfoList.Sort((Comparison<ParliamentHeroSummonGroupInfo>) ((a, b) => int.Parse(a.id).CompareTo(int.Parse(b.id))));
  }

  public List<ParliamentHeroSummonGroupInfo> GetParliamentHeroSummonGroupInfoList()
  {
    return this._parliamentHeroSummonGroupInfoList;
  }

  public ParliamentHeroSummonGroupInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (ParliamentHeroSummonGroupInfo) null;
  }

  public ParliamentHeroSummonGroupInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (ParliamentHeroSummonGroupInfo) null;
  }
}
