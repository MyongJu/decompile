﻿// Decompiled with JetBrains decompiler
// Type: StarGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class StarGroup : MonoBehaviour
{
  private int currentCount = -1;
  private List<GameObject> list = new List<GameObject>();
  private const int MAX_STAR = 5;
  public GameObject multStars;
  public UILabel count;
  private bool init;
  public GameObject noramls;
  public GameObject[] stars;

  private void Start()
  {
    if (this.init)
      return;
    this.init = true;
  }

  public void SetCount(int startCount)
  {
    if (this.currentCount == startCount)
      return;
    this.Clear();
    this.currentCount = startCount;
    if (this.currentCount > 5)
    {
      NGUITools.SetActive(this.noramls, false);
      NGUITools.SetActive(this.multStars, true);
      this.count.text = "x" + this.currentCount.ToString();
    }
    else
    {
      NGUITools.SetActive(this.multStars, false);
      NGUITools.SetActive(this.noramls, true);
      NGUITools.SetActive(this.count.gameObject, false);
      for (int index = 0; index < this.currentCount; ++index)
        NGUITools.SetActive(this.stars[index], true);
      for (int currentCount = this.currentCount; currentCount < 5; ++currentCount)
        NGUITools.SetActive(this.stars[currentCount], false);
    }
  }

  private void Clear()
  {
    for (int index = 0; index < this.stars.Length; ++index)
      NGUITools.SetActive(this.stars[index], false);
  }
}
