﻿// Decompiled with JetBrains decompiler
// Type: ConfigActivityRankReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigActivityRankReward
{
  private ConfigParse parse = new ConfigParse();
  public Dictionary<string, ActivityRankRewardInfo> datas;
  private Dictionary<int, ActivityRankRewardInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<ActivityRankRewardInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public ActivityRankRewardInfo GetRewardInfo(int activity_id, int rank)
  {
    Dictionary<string, ActivityRankRewardInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && enumerator.Current.ActivityMainSubInfo_ID == activity_id && (rank >= enumerator.Current.MinRank && rank <= enumerator.Current.MaxRank))
        return enumerator.Current;
    }
    return (ActivityRankRewardInfo) null;
  }

  public List<ActivityRankRewardInfo> GetTotalRewardInfos()
  {
    List<ActivityRankRewardInfo> activityRankRewardInfoList = new List<ActivityRankRewardInfo>();
    Dictionary<string, ActivityRankRewardInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && enumerator.Current.ActivityMainSubInfo_ID == 0)
        activityRankRewardInfoList.Add(enumerator.Current);
    }
    activityRankRewardInfoList.Sort((Comparison<ActivityRankRewardInfo>) ((x, y) => x.MinRank.CompareTo(y.MinRank)));
    return activityRankRewardInfoList;
  }

  public List<ActivityRankRewardInfo> GetRewardInfos(int activity_id)
  {
    List<ActivityRankRewardInfo> activityRankRewardInfoList = new List<ActivityRankRewardInfo>();
    Dictionary<string, ActivityRankRewardInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && enumerator.Current.ActivityMainSubInfo_ID == activity_id)
        activityRankRewardInfoList.Add(enumerator.Current);
    }
    activityRankRewardInfoList.Sort((Comparison<ActivityRankRewardInfo>) ((x, y) => x.MinRank.CompareTo(y.MinRank)));
    return activityRankRewardInfoList;
  }

  public ActivityRankRewardInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (ActivityRankRewardInfo) null;
  }

  public ActivityRankRewardInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (ActivityRankRewardInfo) null;
  }
}
