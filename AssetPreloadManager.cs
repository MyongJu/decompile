﻿// Decompiled with JetBrains decompiler
// Type: AssetPreloadManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class AssetPreloadManager
{
  public System.Action<string, object> onAssetPreloadFinished;
  private static AssetPreloadManager _instance;

  public static AssetPreloadManager Instance
  {
    get
    {
      if (AssetPreloadManager._instance == null)
        AssetPreloadManager._instance = new AssetPreloadManager();
      return AssetPreloadManager._instance;
    }
  }

  public void PreloadAsset(string name)
  {
    AssetManager.Instance.LoadAsync(name, (System.Action<UnityEngine.Object, bool>) ((arg1, arg2) =>
    {
      if (!arg2 || this.onAssetPreloadFinished == null)
        return;
      this.onAssetPreloadFinished(name, (object) arg1);
    }), (System.Type) null);
  }
}
