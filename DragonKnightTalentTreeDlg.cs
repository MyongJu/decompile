﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightTalentTreeDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class DragonKnightTalentTreeDlg : UI.Dialog
{
  public static string[] Titles = new string[3]
  {
    "dragon_knight_skills_berserker_title",
    "dragon_knight_skills_knight_title",
    "dragon_knight_skills_mage_title"
  };
  private List<long> vfxCreatedList = new List<long>();
  private int reqId = -1;
  private int treeIndex = -1;
  public const int FIRST_SLOT_DELTA = 400;
  public const int SLOT_HER_STEP = 230;
  public const int SLOT_VET_STEP = 180;
  private const string VFX_PATH = "Prefab/VFX/fx_unlock";
  private const int INVALID_FLAG = -1;
  private bool needUpdate;
  private int tagIndex;
  [SerializeField]
  private DragonKnightTalentTreeDlg.Panel panel;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.tagIndex = -1;
    if (orgParam == null)
      this.ChangeTag(PlayerPrefsEx.GetInt("dragon_knight_talent_tree_index", 1));
    else
      this.ChangeTag((orgParam as DragonKnightTalentTreeDlg.Parameter).index);
    this.AddEventHandler();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearVfxHandle();
    this.RemoveEventHandler();
  }

  public void Update()
  {
    if (!this.needUpdate)
      return;
    this.FreshTreeData(this.tagIndex, false);
  }

  private void ChangeTag(int treeIndex)
  {
    if (this.tagIndex == treeIndex)
      return;
    this.tagIndex = treeIndex;
    PlayerPrefsEx.SetInt("dragon_knight_talent_tree_index", treeIndex);
    PlayerPrefsEx.Save();
    this.panel.SetTitleIndex(this.tagIndex);
    this.FreshTreeData(this.tagIndex, true);
  }

  private void FreshTreeData(int tmpTreeIndex = 0, bool scollBack = true)
  {
    this.needUpdate = false;
    this.panel.Init();
    for (int index = 0; index < this.panel.talentTreeTitle.Length; ++index)
      this.panel.talentTreeTitle[index].text = string.Format("{0} [F9B845]{1}[-]", (object) ScriptLocalization.Get(DragonKnightTalentTreeDlg.Titles[index], true), (object) DBManager.inst.DB_DragonKnightTalent.GetTalentTreeTalentCount(index + 1));
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.Get(PlayerData.inst.uid);
    if (dragonKnightData != null)
      this.panel.talentLeftPoint.text = string.Format("{0}: {1}", (object) Utils.XLAT("player_profile_skill_points"), (object) dragonKnightData.TalentPoint);
    List<DragonKnightTalentTreeInfo> talentsByTreeIndex = ConfigManager.inst.DB_DragonKnightTalentTree.GetTalentsByTreeIndex(tmpTreeIndex + 1);
    this.panel.usedSlotNum = talentsByTreeIndex == null ? 0 : talentsByTreeIndex.Count;
    this.ClearLine();
    for (int index = 0; index < this.panel.usedSlotNum; ++index)
      this.SetSlot(this.panel.slots[index], talentsByTreeIndex[index]);
    if (tmpTreeIndex + 1 == this.treeIndex)
    {
      this.reqId = -1;
      this.treeIndex = -1;
    }
    for (int usedSlotNum = this.panel.usedSlotNum; usedSlotNum < this.panel.slots.Count; ++usedSlotNum)
      this.panel.slots[usedSlotNum].gameObject.SetActive(false);
    if (scollBack)
      this.panel.panelScrollView.ResetPosition();
    this.CheckResetButtonState();
  }

  private void SetSlot(DragonKnightTalentSlot slot, DragonKnightTalentTreeInfo talentTreeInfo)
  {
    slot.gameObject.SetActive(true);
    slot.talentTreeId = talentTreeInfo.internalId;
    slot.onTalentSlotClick = new System.Action<int>(this.OnTalentSlotClick);
    slot.panel.dragScrollView.scrollView = this.panel.panelScrollView;
    slot.transform.localPosition = new Vector3((float) (400 + 230 * talentTreeInfo.tier), (float) (180 * (talentTreeInfo.row - 2)), 0.0f);
    DragonKnightTalentInfo talentByTalentTreeId = ConfigManager.inst.DB_DragonKnightTalent.GetCurrentLevelTalentByTalentTreeId(talentTreeInfo.internalId);
    Requirements requirementsByTalentTreeId = ConfigManager.inst.DB_DragonKnightTalent.GetRequirementsByTalentTreeId(talentTreeInfo.internalId);
    List<int> intList = new List<int>();
    if (requirementsByTalentTreeId != null)
    {
      Dictionary<string, int>.KeyCollection.Enumerator enumerator = requirementsByTalentTreeId.requires.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        int num = int.Parse(enumerator.Current);
        intList.Add(num);
        this.SetLine(talentTreeInfo.internalId, num, talentByTalentTreeId != null && DBManager.inst.DB_DragonKnightTalent.IsTalentOwned(num));
      }
    }
    if (!intList.Contains(this.reqId) || talentByTalentTreeId != null)
      return;
    this.StartCoroutine(this.PlayEffect(0.2f, slot.transform));
  }

  [DebuggerHidden]
  private IEnumerator PlayEffect(float time, Transform parent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DragonKnightTalentTreeDlg.\u003CPlayEffect\u003Ec__Iterator5C()
    {
      time = time,
      parent = parent,
      \u003C\u0024\u003Etime = time,
      \u003C\u0024\u003Eparent = parent,
      \u003C\u003Ef__this = this
    };
  }

  private void ClearVfxHandle()
  {
    if (this.vfxCreatedList == null)
      return;
    for (int index = 0; index < this.vfxCreatedList.Count; ++index)
      VfxManager.Instance.Delete(this.vfxCreatedList[index]);
  }

  private void SetLine(int endTalentTreeId, int startTalentId, bool active = false)
  {
    while (this.panel.usedLineNum >= this.panel.lines.Count)
    {
      GameObject gameObject = NGUITools.AddChild(this.panel.linesParent.gameObject, this.panel.lineOrg.gameObject);
      gameObject.name = string.Format("line_{0}", (object) (100 + this.panel.lines.Count));
      this.panel.lines.Add(gameObject.GetComponent<TalentLine>());
      gameObject.SetActive(false);
    }
    DragonKnightTalentTreeInfo talentTreeInfo1 = ConfigManager.inst.DB_DragonKnightTalentTree.GetTalentTreeInfo(ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(startTalentId).talentTreeInternalId);
    DragonKnightTalentTreeInfo talentTreeInfo2 = ConfigManager.inst.DB_DragonKnightTalentTree.GetTalentTreeInfo(endTalentTreeId);
    this.panel.lines[this.panel.usedLineNum].gameObject.SetActive(true);
    this.panel.lines[this.panel.usedLineNum].SetPosition(talentTreeInfo2.tier, talentTreeInfo2.row, talentTreeInfo1.tier, talentTreeInfo1.row, active);
    ++this.panel.usedLineNum;
  }

  private void ClearLine()
  {
    for (int index = 0; index < this.panel.lines.Count; ++index)
    {
      this.panel.lines[index].gameObject.SetActive(false);
      this.panel.usedLineNum = 0;
    }
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_DragonKnight.onDataChanged += new System.Action<DragonKnightData>(this.DragonKnightDataChanged);
    DBManager.inst.DB_DragonKnightTalent.onDataChanged += new System.Action<int>(this.OnTalentDataChange);
    DBManager.inst.DB_DragonKnightTalent.onDataRemoved += new System.Action(this.OnTalentDataChange);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_DragonKnight.onDataChanged -= new System.Action<DragonKnightData>(this.DragonKnightDataChanged);
    DBManager.inst.DB_DragonKnightTalent.onDataChanged -= new System.Action<int>(this.OnTalentDataChange);
    DBManager.inst.DB_DragonKnightTalent.onDataRemoved -= new System.Action(this.OnTalentDataChange);
  }

  public void CheckResetButtonState()
  {
    this.panel.BT_Reset.isEnabled = DBManager.inst.DB_DragonKnightTalent.Datas.Count > 0;
  }

  public void OnTalentDataChange(int talentId)
  {
    this.needUpdate = true;
  }

  public void OnTalentDataChange()
  {
    this.needUpdate = true;
  }

  public void DragonKnightDataChanged(DragonKnightData dragonKnightData)
  {
    if (dragonKnightData == null || dragonKnightData.UserId != PlayerData.inst.uid)
      return;
    this.needUpdate = true;
  }

  public void OnTreeTag1Click()
  {
    this.ChangeTag(0);
  }

  public void OnTreeTag2Click()
  {
    this.ChangeTag(1);
  }

  public void OnTreeTag3Click()
  {
    this.ChangeTag(2);
  }

  public void OnTalentSlotClick(int talentTreeId)
  {
    (UIManager.inst.OpenPopup("DragonKnight/DragonKnightTalentEnhancePopup", (Popup.PopupParameter) new DragonKnightTalentEnhancePopup.Parameter()
    {
      talentTreeId = talentTreeId
    }) as DragonKnightTalentEnhancePopup).onTalentLvlUpFull += new System.Action<int, int>(this.NeedShowEffect);
  }

  private void NeedShowEffect(int internalId, int tree)
  {
    this.reqId = internalId;
    this.treeIndex = tree;
    this.needUpdate = true;
  }

  public void OnResetButtonClick()
  {
    UIManager.inst.OpenPopup("UseOrBuyAndUseItemPopup", (Popup.PopupParameter) new UseOrBuyAndUseItemPopup.Parameter()
    {
      itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_dragon_knight_talent_reset"),
      callback = (System.Action<bool, object>) null,
      titleText = Utils.XLAT("talent_uppercase_reset_points_title"),
      btnText = Utils.XLAT("talent_reset_points_button")
    });
  }

  [Serializable]
  protected class Panel
  {
    public Color[] backgroundColor = new Color[3]
    {
      new Color((float) byte.MaxValue, 74f, 74f, 141f),
      new Color(73f, (float) byte.MaxValue, 97f, 141f),
      new Color(73f, 81f, (float) byte.MaxValue, 141f)
    };
    public List<TalentLine> lines = new List<TalentLine>();
    public List<DragonKnightTalentSlot> slots = new List<DragonKnightTalentSlot>();
    private int titleCurIndex = -1;
    public const int MAX_TALENT_SLOT_NUM = 50;
    private bool isInit;
    public UITexture backgroundCircle;
    public UITexture backgroundImage;
    public TweenColor backgroundTweenCircle;
    public TweenColor backgroundTweenImage;
    public UILabel talentLeftPoint;
    public UILabel[] talentTreeTitle;
    public Transform[] talentTitleBackground;
    public UIScrollView panelScrollView;
    public int usedLineNum;
    public Transform linesParent;
    public TalentLine lineOrg;
    public Material[] lineMats;
    public int usedSlotNum;
    public Transform slotsParent;
    public DragonKnightTalentSlot talentSlotOrg;
    public UIButton BT_Reset;

    public void SetTitleIndex(int titleIndex)
    {
      this.TweenBackgroundImage(titleIndex, this.titleCurIndex == -1);
      for (int index = 0; index < this.talentTitleBackground.Length; ++index)
        this.talentTitleBackground[index].gameObject.SetActive(false);
      this.talentTitleBackground[titleIndex].gameObject.SetActive(true);
      if (this.lineMats != null)
      {
        for (int index = 0; index < this.lineMats.Length; ++index)
          this.lineMats[index].SetColor("_TintColor", this.backgroundColor[titleIndex]);
      }
      this.titleCurIndex = titleIndex;
    }

    private void TweenBackgroundImage(int titleIndex, bool imd = true)
    {
      if (imd)
      {
        this.backgroundCircle.color = this.backgroundColor[titleIndex];
        this.backgroundImage.color = this.backgroundColor[titleIndex];
      }
      else
      {
        TweenColor.Begin(this.backgroundCircle.gameObject, 0.3f, this.backgroundColor[titleIndex]);
        TweenColor.Begin(this.backgroundImage.gameObject, 0.3f, this.backgroundColor[titleIndex]);
      }
    }

    public void Init()
    {
      if (this.isInit)
        return;
      for (int index = 0; index < 50; ++index)
      {
        GameObject gameObject = NGUITools.AddChild(this.slotsParent.gameObject, this.talentSlotOrg.gameObject);
        gameObject.name = string.Format("Talent_slot_{0}", (object) (100 + index));
        this.slots.Add(gameObject.GetComponent<DragonKnightTalentSlot>());
        gameObject.SetActive(false);
      }
      this.usedSlotNum = 0;
      this.isInit = true;
    }
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int index;
  }
}
