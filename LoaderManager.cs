﻿// Decompiled with JetBrains decompiler
// Type: LoaderManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class LoaderManager
{
  private ObjectPool<LoaderInfo> _infoPool = new ObjectPool<LoaderInfo>();
  private ObjectPool<LoaderBatch> _batchPool = new ObjectPool<LoaderBatch>();
  private List<WWW> totalTrashs = new List<WWW>();
  public const int TYPE_REQUEST = 1;
  public const int TYPE_LOADER = 2;
  public const int TYPE_OTHER = 3;
  public const int TYPE_ASSET = 4;
  public const int TYPE_CONFIG = 5;
  public const int TYPE_NLP = 6;
  public const int MaxResendCount = 4;
  private const int MaxHttpCount = 5;
  private List<LoaderHttp> _httpQueue;
  private List<LoaderBatch> _batchQueue;
  private Stack<LoaderBatch> _batchStack;
  private Stack<LoaderBatch> _idleBatchStack;
  private LoaderBatch _currStackBatch;
  private LoaderBatch _currRequestBatch;
  private LoaderBatch _currIdleStackBatch;
  private static LoaderManager _instance;

  public static LoaderManager inst
  {
    get
    {
      if (LoaderManager._instance == null)
        LoaderManager._instance = new LoaderManager();
      return LoaderManager._instance;
    }
  }

  public void Init()
  {
    this._batchStack = new Stack<LoaderBatch>();
    this._batchQueue = new List<LoaderBatch>();
    this._httpQueue = new List<LoaderHttp>(5);
    this._idleBatchStack = new Stack<LoaderBatch>();
    for (int index = 0; index < 5; ++index)
      this._httpQueue.Add(new LoaderHttp());
    this.IsReady = true;
    Oscillator.Instance.updateEvent += new System.Action<double>(this.AutoLoader);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.AutoRetry);
  }

  public void Add(LoaderBatch batch, bool useStack = false, bool isIdle = false)
  {
    if (batch.GetInfoCount() > 0)
    {
      if (isIdle)
      {
        batch.disposeEvent += new System.Action<LoaderBatch>(this.OnIdleDispose);
        this._idleBatchStack.Push(batch);
      }
      else if (useStack)
      {
        batch.disposeEvent += new System.Action<LoaderBatch>(this.OnStackDispose);
        this._batchStack.Push(batch);
      }
      else
      {
        batch.disposeEvent += new System.Action<LoaderBatch>(this.OnQueueDispose);
        this._batchQueue.Add(batch);
      }
    }
    else
    {
      batch.Accident();
      batch.Dispose();
      LoaderManager.inst.ReleaseBatch(batch);
    }
  }

  private void OnQueueDispose(LoaderBatch batch)
  {
    batch.disposeEvent -= new System.Action<LoaderBatch>(this.OnQueueDispose);
    this._batchQueue.Remove(batch);
    batch.Dispose();
    LoaderManager.inst.ReleaseBatch(batch);
  }

  private void OnStackDispose(LoaderBatch batch)
  {
    batch.disposeEvent -= new System.Action<LoaderBatch>(this.OnStackDispose);
    this._currStackBatch = (LoaderBatch) null;
    batch.Dispose();
    LoaderManager.inst.ReleaseBatch(batch);
  }

  private void OnIdleDispose(LoaderBatch batch)
  {
    batch.disposeEvent -= new System.Action<LoaderBatch>(this.OnIdleDispose);
    batch.Dispose();
    LoaderManager.inst.ReleaseBatch(batch);
  }

  public void RequestStart(LoaderBatch batch, bool blockScreen = true)
  {
    if (this._currRequestBatch == null && this.RequestEnAble)
    {
      this._currRequestBatch = batch;
      this._currRequestBatch.processEvent += new System.Action<LoaderInfo>(this.OnRequestProcess);
      this._httpQueue[0].Info = this._currRequestBatch.GetLoaderInfo();
      this._httpQueue[0].StartHttp();
    }
    else
      MessageHub.inst.OnError(nameof (LoaderManager), "no request");
  }

  private void OnRequestProcess(LoaderInfo info)
  {
    if (!this.IsReady)
      ;
  }

  public void ReuqestDone()
  {
    if (this._currRequestBatch == null)
      return;
    this._currRequestBatch.Dispose();
    LoaderManager.inst.ReleaseBatch(this._currRequestBatch);
    this._currRequestBatch = (LoaderBatch) null;
  }

  public void RequestRetry()
  {
    if (this._currRequestBatch != null && this.RequestEnAble)
    {
      LoaderInfo loaderInfo = this._currRequestBatch.GetLoaderInfo();
      if (loaderInfo != null && !loaderInfo.AutoRetry && !loaderInfo.IsSending)
      {
        loaderInfo.URL = NetApi.inst.ReBuildUrl(loaderInfo.URL, loaderInfo.SendCount);
        this._httpQueue[0].Info = loaderInfo;
        this._httpQueue[0].StartHttp();
        return;
      }
    }
    MessageHub.inst.OnError(nameof (LoaderManager), "no retry");
  }

  public bool RequestEnAble
  {
    get
    {
      return this._httpQueue[0].Info == null;
    }
  }

  private LoaderHttp GetLoaderHttp()
  {
    if (this._httpQueue == null)
      return (LoaderHttp) null;
    for (int index = 1; index < this._httpQueue.Count; ++index)
    {
      if (this._httpQueue[index].Info == null)
        return this._httpQueue[index];
    }
    return (LoaderHttp) null;
  }

  private LoaderInfo GetLoaderInfo()
  {
    if (this._currStackBatch == null && this._batchStack.Count > 0)
      this._currStackBatch = this._batchStack.Pop();
    if (this._currStackBatch != null)
    {
      LoaderInfo loaderInfo = this._currStackBatch.GetLoaderInfo();
      if (loaderInfo != null)
        return loaderInfo;
    }
    for (int index = 0; index < this._batchQueue.Count; ++index)
    {
      LoaderInfo loaderInfo = this._batchQueue[index].GetLoaderInfo();
      if (loaderInfo != null)
        return loaderInfo;
    }
    if (this._idleBatchStack.Count > 0)
    {
      LoaderInfo loaderInfo = this._idleBatchStack.Pop().GetLoaderInfo();
      if (loaderInfo != null)
        return loaderInfo;
    }
    return (LoaderInfo) null;
  }

  private void AutoLoader(double timestamp)
  {
    LoaderHttp loaderHttp = this.GetLoaderHttp();
    if (loaderHttp == null)
      return;
    LoaderInfo loaderInfo = this.GetLoaderInfo();
    if (loaderInfo == null)
      return;
    loaderHttp.Info = loaderInfo;
    loaderHttp.StartHttp();
  }

  public int Now
  {
    get
    {
      return (int) Time.time;
    }
  }

  private void AutoRetry(int timestamp)
  {
    for (int index = 0; index < this._httpQueue.Count; ++index)
    {
      LoaderInfo info = this._httpQueue[index].Info;
      if (info != null && info.AutoRetry && (info.NeedResend() && info.ReSendTime > 0))
      {
        info.ReSendTime = 0;
        info.URL = NetApi.inst.ReBuildUrl(info.URL, info.SendCount);
        this._httpQueue[index].StartHttp();
      }
    }
  }

  public LoaderBatch PopBatch()
  {
    return this._batchPool.Allocate();
  }

  public void ReleaseBatch(LoaderBatch batch)
  {
    this._batchPool.Release(batch);
  }

  public LoaderInfo PopInfo()
  {
    return this._infoPool.Allocate();
  }

  public void ReleaseInfo(LoaderInfo info)
  {
    this._infoPool.Release(info);
  }

  private bool IsReady { get; set; }

  public void Dispose()
  {
    this.IsReady = false;
    if (Oscillator.IsAvailable)
    {
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.AutoLoader);
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.AutoRetry);
    }
    this._infoPool.Clear();
    this._batchPool.Clear();
    if (this._batchQueue != null)
    {
      this._batchQueue.Clear();
      this._batchQueue = (List<LoaderBatch>) null;
    }
    if (this._batchStack != null)
    {
      this._batchStack.Clear();
      this._batchStack = (Stack<LoaderBatch>) null;
    }
    if (this._idleBatchStack != null)
    {
      this._idleBatchStack.Clear();
      this._idleBatchStack = (Stack<LoaderBatch>) null;
    }
    if (this._httpQueue != null)
    {
      for (int index = 0; index < this._httpQueue.Count; ++index)
      {
        this.totalTrashs.AddRange((IEnumerable<WWW>) this._httpQueue[index].Trash);
        this._httpQueue[index].Dispose();
      }
      this._httpQueue.Clear();
      this._httpQueue = (List<LoaderHttp>) null;
    }
    if (this._currRequestBatch != null)
      this._currRequestBatch.processEvent -= new System.Action<LoaderInfo>(this.OnRequestProcess);
    this._currStackBatch = (LoaderBatch) null;
    this._currRequestBatch = (LoaderBatch) null;
    this.ClearTrash();
  }

  public void ClearTrash()
  {
    List<WWW> wwwList = new List<WWW>();
    for (int index = 0; index < this.totalTrashs.Count; ++index)
    {
      if (this.totalTrashs[index].isDone)
        wwwList.Add(this.totalTrashs[index]);
    }
    for (int index = 0; index < wwwList.Count; ++index)
      this.totalTrashs.Remove(wwwList[index]);
    wwwList.Clear();
  }
}
