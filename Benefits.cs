﻿// Decompiled with JetBrains decompiler
// Type: Benefits
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;

public class Benefits : ICustomParse
{
  public Dictionary<string, float> benefits = new Dictionary<string, float>();
  private List<Benefits.BenefitValuePair> benefitsPairs;

  public void Parse(object content)
  {
    Hashtable hashtable = content as Hashtable;
    if (hashtable == null)
      return;
    foreach (DictionaryEntry dictionaryEntry in hashtable)
      this.benefits.Add(dictionaryEntry.Key.ToString(), float.Parse(dictionaryEntry.Value.ToString()));
  }

  public static Benefits Build(string content)
  {
    return JsonReader.Deserialize<Benefits>(content);
  }

  public List<Benefits.BenefitValuePair> GetBenefitsPairs()
  {
    if (this.benefitsPairs == null)
    {
      this.benefitsPairs = new List<Benefits.BenefitValuePair>();
      using (Dictionary<string, float>.Enumerator enumerator = this.benefits.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, float> current = enumerator.Current;
          if (ConfigManager.inst.DB_ConfigAddress.IsBenefit(int.Parse(current.Key)))
            this.benefitsPairs.Add(new Benefits.BenefitValuePair(int.Parse(current.Key), current.Value));
        }
      }
    }
    return this.benefitsPairs;
  }

  public class BenefitValuePair
  {
    public int internalID;
    public float value;
    public PropertyDefinition.FormatType type;
    public PropertyDefinition propertyDefinition;

    public BenefitValuePair(int internalID, float value)
    {
      this.internalID = internalID;
      this.value = value;
      this.propertyDefinition = ConfigManager.inst.DB_Properties[internalID];
      if (this.propertyDefinition == null)
        return;
      this.type = this.propertyDefinition.Format;
    }

    public override string ToString()
    {
      return string.Format("{0}:{1}", (object) this.propertyDefinition.Name, (object) this.propertyDefinition.ConvertToDisplayString((double) this.value, false, true));
    }
  }
}
