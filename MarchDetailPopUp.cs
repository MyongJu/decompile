﻿// Decompiled with JetBrains decompiler
// Type: MarchDetailPopUp
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MarchDetailPopUp : Popup
{
  private List<MarchTroopContainer> items = new List<MarchTroopContainer>();
  private long marchId;
  public DragonUIInfo dragon;
  public UILabel title;
  public UILabel troopsCount;
  public MarchTroopContainer troopPrefab;
  public UIScrollView scrollView;
  public UITable table;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.marchId = (orgParam as MarchDetailPopUp.Parameter).marchId;
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Clear();
  }

  private void Clear()
  {
    for (int index = 0; index < this.items.Count; ++index)
      this.items[index].Clear();
    this.items.Clear();
    this.dragon.Clear();
  }

  private void UpdateUI()
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.marchId);
    this.troopsCount.text = marchData.troopsInfo.totalCount.ToString();
    this.title.text = ScriptLocalization.Get("march_info_title", true);
    NGUITools.SetActive(this.dragon.gameObject, marchData.withDragon);
    if (marchData.withDragon)
    {
      string empty = string.Empty;
      MarchData.MarchType type = marchData.type;
      string skillType;
      switch (type)
      {
        case MarchData.MarchType.encamp:
        case MarchData.MarchType.city_attack:
        case MarchData.MarchType.encamp_attack:
        case MarchData.MarchType.gather_attack:
        case MarchData.MarchType.wonder_attack:
        case MarchData.MarchType.rally_attack:
          skillType = "attack";
          break;
        case MarchData.MarchType.gather:
          skillType = "gather";
          break;
        case MarchData.MarchType.reinforce:
          skillType = "defend";
          break;
        case MarchData.MarchType.monster_attack:
          skillType = "attack_monster";
          break;
        default:
          if (type != MarchData.MarchType.rally)
          {
            if (type != MarchData.MarchType.world_boss_attack)
            {
              skillType = "attack";
              break;
            }
            goto case MarchData.MarchType.monster_attack;
          }
          else
            goto case MarchData.MarchType.encamp;
      }
      this.dragon.SetDragon(marchData.dragonId, skillType, (int) marchData.dragon.tendency, marchData.dragon.level);
    }
    if (this.Check(TroopType.class_infantry))
      this.GetItem().SetData(TroopType.class_infantry, this.marchId, false, false);
    if (this.Check(TroopType.class_cavalry))
      this.GetItem().SetData(TroopType.class_cavalry, this.marchId, false, false);
    if (this.Check(TroopType.class_ranged))
      this.GetItem().SetData(TroopType.class_ranged, this.marchId, false, false);
    if (this.Check(TroopType.class_artyfar))
      this.GetItem().SetData(TroopType.class_artyfar, this.marchId, false, false);
    this.table.repositionNow = true;
    Utils.ExecuteInSecs(0.05f, (System.Action) (() =>
    {
      this.scrollView.ResetPosition();
      this.table.Reposition();
    }));
  }

  private void OnReposition()
  {
    this.table.onReposition -= new UITable.OnReposition(this.OnReposition);
    this.table.Reposition();
  }

  private bool Check(TroopType trooptype)
  {
    Dictionary<string, Unit>.KeyCollection.Enumerator enumerator = DBManager.inst.DB_March.Get(this.marchId).troopsInfo.troops.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator.Current).GetUnitType() == trooptype)
        return true;
    }
    return false;
  }

  public MarchTroopContainer GetItem()
  {
    GameObject go = NGUITools.AddChild(this.table.gameObject, this.troopPrefab.gameObject);
    MarchTroopContainer component = go.GetComponent<MarchTroopContainer>();
    NGUITools.SetActive(go, true);
    this.items.Add(component);
    return component;
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public long marchId;
  }
}
