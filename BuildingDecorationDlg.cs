﻿// Decompiled with JetBrains decompiler
// Type: BuildingDecorationDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using UI;
using UnityEngine;

public class BuildingDecorationDlg : UI.Dialog
{
  public float strongholdZoomFocusDis = 0.8f;
  public ItemsForShopNew itemsShop;
  public ItemsWarehouse itemsHouse;
  public UIButtonBar bar;
  public UILabel title;
  public UIButton cityBt;
  public UIButton kingdomBt;
  public GameObject target;
  public GameObject kingdomtarget;
  private BuildingDecoration strongHoldDecoration;
  private CityDecorationVFX cityDecorationVFX;
  public UILabel gold;
  public UITexture targetIcon;
  public UIButton defaultButton;

  public void OnViewDefault()
  {
    if (!string.IsNullOrEmpty(PlayerData.inst.playerCityData.decoration))
      this.ShowWarn();
    else
      this.ViewDefault();
  }

  private void ShowWarn()
  {
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = ScriptLocalization.Get("id_uppercase_warning", true),
      content = ScriptLocalization.Get("stronghold_appearance_restore_confirm_description", true),
      yes = ScriptLocalization.Get("id_uppercase_confirm", true),
      no = ScriptLocalization.Get("id_uppercase_cancel", true),
      yesCallback = new System.Action(this.ViewDefault),
      noCallback = (System.Action) null
    });
  }

  private void ViewDefault()
  {
    Hashtable postData = Utils.Hash((object) "uid", (object) PlayerData.inst.uid);
    RequestManager.inst.SendRequest("city:restoreDecoration", postData, (System.Action<bool, object>) ((success, result) => this.cityDecorationVFX.Clear()), true);
  }

  public void OnViewCity()
  {
    NGUITools.SetActive(this.target, false);
    NGUITools.SetActive(this.kingdomBt.gameObject, true);
    NGUITools.SetActive(this.cityBt.gameObject, false);
  }

  private void AddBuilding(string path, GameObject parent)
  {
    GameObject prefab = AssetManager.Instance.HandyLoad(path, (System.Type) null) as GameObject;
    NGUITools.SetActive(parent, true);
    if (!((UnityEngine.Object) prefab != (UnityEngine.Object) null))
      return;
    this.Clear();
    GameObject gameObject = NGUITools.AddChild(parent, prefab);
    this.strongHoldDecoration = gameObject.AddComponent<BuildingDecoration>();
    gameObject.GetComponent<UI2DSprite>().width = 512;
    Vector3 localScale = gameObject.transform.localScale;
    localScale.x = 2f;
    localScale.y = 2f;
    gameObject.transform.localScale = localScale;
  }

  public void OnViewKingdom()
  {
    NGUITools.SetActive(this.target, true);
    NGUITools.SetActive(this.kingdomBt.gameObject, false);
    NGUITools.SetActive(this.cityBt.gameObject, true);
  }

  public void OnItemSelected(int itemId)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo == null || string.IsNullOrEmpty(itemStaticInfo.Param1))
      return;
    this.strongHoldDecoration.PreView(itemStaticInfo.Param1);
    this.cityDecorationVFX.UpdateUI(itemStaticInfo.Param1, PlayerData.inst.playerCityData.level);
  }

  public void OnAddGoldClicked()
  {
    Utils.PopUpCommingSoon();
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.AddEventHandler();
    this.bar.SelectedIndex = 0;
    this.UpdateUI();
  }

  private void FocusBuilding()
  {
    CitadelSystem.inst.FocusConstructionBuilding(CitadelSystem.inst.MStronghold.gameObject, (UIWidget) this.targetIcon, 3000f);
    this.strongHoldDecoration = CitadelSystem.inst.MStronghold.gameObject.GetComponent<BuildingDecoration>();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.PreLoad();
    this.FocusBuilding();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    CitadelSystem.inst.MoveCameraBackToNormal();
    this.strongHoldDecoration.ClearFlag();
    this.strongHoldDecoration.UpdateUI(PlayerData.inst.playerCityData.decoration, 1f);
  }

  private void PreLoad()
  {
    string empty = string.Empty;
    int level = Math.Min(Math.Max(0, PlayerData.inst.playerCityData.buildings.level_stronghold - 1), MapUtils.CityPrefabNames.Length - 1);
    GameObject src = AssetManager.Instance.HandyLoad("Prefab/Tiles/" + MapUtils.CityPrefabNames[level], (System.Type) null) as GameObject;
    if ((bool) ((UnityEngine.Object) src))
    {
      this.cityDecorationVFX = Utils.DuplicateGOB(src, this.kingdomtarget.transform).GetComponent<CityDecorationVFX>();
      Vector3 localScale = this.kingdomtarget.transform.localScale;
      localScale.x = localScale.y = this.GetScale(level);
      this.kingdomtarget.transform.localScale = localScale;
    }
    this.cityDecorationVFX.UpdateUI(PlayerData.inst.playerCityData.decoration, PlayerData.inst.playerCityData.level);
  }

  private float GetScale(int level)
  {
    if (level <= 12)
      return 4f;
    return level <= 15 ? 3f : 2.2f;
  }

  private void OnUserDataUpdate(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.gold.text = Utils.FormatThousands(DBManager.inst.DB_User.Get(PlayerData.inst.uid).currency.gold.ToString());
  }

  private void AddEventHandler()
  {
    this.itemsShop.OnItemSelected += new System.Action<int>(this.OnItemSelected);
    this.itemsHouse.OnItemSelected += new System.Action<int>(this.OnItemSelected);
    this.bar.OnSelectedHandler += new System.Action<int>(this.OnSelectedViewChange);
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdate);
    DBManager.inst.DB_City.onDataUpdate += new System.Action<long>(this.OnCityUpdate);
  }

  private void OnCityUpdate(long cityID)
  {
    if (cityID != PlayerData.inst.playerCityData.cityId)
      return;
    this.strongHoldDecoration.PreView(PlayerData.inst.playerCityData.decoration);
    this.cityDecorationVFX.UpdateUI(PlayerData.inst.playerCityData.decoration, PlayerData.inst.playerCityData.level);
    NGUITools.SetActive(this.defaultButton.gameObject, !string.IsNullOrEmpty(PlayerData.inst.playerCityData.decoration));
  }

  private void RemoveEventHandler()
  {
    this.itemsShop.OnItemSelected -= new System.Action<int>(this.OnItemSelected);
    this.itemsHouse.OnItemSelected -= new System.Action<int>(this.OnItemSelected);
    this.bar.OnSelectedHandler -= new System.Action<int>(this.OnSelectedViewChange);
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdate);
    DBManager.inst.DB_City.onDataUpdate -= new System.Action<long>(this.OnCityUpdate);
  }

  private void UpdateUI()
  {
    NGUITools.SetActive(this.defaultButton.gameObject, !string.IsNullOrEmpty(PlayerData.inst.playerCityData.decoration));
    this.title.text = ScriptLocalization.Get("stronghold_uppercase_appearance", true);
    this.itemsShop.SetData(ItemBag.ItemType.castle_display);
    this.itemsHouse.SetData(ItemBag.ItemType.castle_display);
    this.gold.text = Utils.FormatThousands(DBManager.inst.DB_User.Get(PlayerData.inst.uid).currency.gold.ToString());
  }

  private void OnSelectedViewChange(int index)
  {
    this.SetView();
  }

  private void SetView()
  {
    NGUITools.SetActive(this.itemsShop.gameObject, this.bar.SelectedIndex == 0);
    NGUITools.SetActive(this.itemsHouse.gameObject, this.bar.SelectedIndex == 1);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Clear();
    this.RemoveEventHandler();
  }

  private void Clear()
  {
    this.itemsShop.Dispose();
    this.itemsHouse.Dispose();
  }
}
