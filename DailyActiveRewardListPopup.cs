﻿// Decompiled with JetBrains decompiler
// Type: DailyActiveRewardListPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DailyActiveRewardListPopup : Popup
{
  private List<DailyActivyRewardItem> items = new List<DailyActivyRewardItem>();
  public UILabel title;
  public UIButton claimBt;
  public UITable table;
  public DailyActivyRewardItem rewardItemPrefab;
  public UIScrollView scrollView;
  public UILabel normalTip;
  private int reward_id;
  private int XPRewrds;
  public System.Action onCloseHandler;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DailyActiveRewardListPopup.Parameter parameter = orgParam as DailyActiveRewardListPopup.Parameter;
    if (parameter == null)
      return;
    this.reward_id = parameter.reward_id;
    this.onCloseHandler = parameter.onCloseHandler;
    this.UpdateUI();
    this.AddEventHandler();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Clear();
    this.RemoveEventHandler();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    if (this.onCloseHandler == null)
      return;
    this.onCloseHandler();
  }

  private void Clear()
  {
    List<DailyActivyRewardItem>.Enumerator enumerator = this.items.GetEnumerator();
    while (enumerator.MoveNext())
    {
      enumerator.Current.Clear();
      NGUITools.SetActive(enumerator.Current.gameObject, false);
      UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.items.Clear();
  }

  private void AddEventHandler()
  {
  }

  private void RemoveEventHandler()
  {
  }

  private void UpdateUI()
  {
    DailyActiveRewardInfo activeRewardInfo = ConfigManager.inst.DB_DailyActivies.GetDailyActiveRewardInfo(this.reward_id);
    this.title.text = ScriptLocalization.Get("tavern_daily_activity_rewards_title", true);
    if (activeRewardInfo.RequrieScore <= DBManager.inst.DB_DailyActives.CurrentPoint)
    {
      NGUITools.SetActive(this.normalTip.gameObject, false);
      this.claimBt.isEnabled = !DBManager.inst.DB_DailyActives.IsHadOpen(this.reward_id);
    }
    else
    {
      this.claimBt.isEnabled = false;
      this.normalTip.text = ScriptLocalization.GetWithPara("tavern_daily_activity_not_enough_points_description", new Dictionary<string, string>()
      {
        {
          "num",
          activeRewardInfo.RequrieScore.ToString()
        }
      }, true);
      NGUITools.SetActive(this.normalTip.gameObject, true);
    }
    this.CreateRewardItem();
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
  }

  private void CreateRewardItem()
  {
    DailyActiveRewardInfo activeRewardInfo = ConfigManager.inst.DB_DailyActivies.GetDailyActiveRewardInfo(this.reward_id);
    DailyRewardFactor currentRewardFactor = ConfigManager.inst.DB_DailyActivies.GetCurrentRewardFactor();
    RewardsCollectionAnimator.Instance.Clear();
    if (activeRewardInfo.RewardFood > 0)
    {
      int rewardFood = activeRewardInfo.RewardFood;
      if (currentRewardFactor != null)
        rewardFood = Mathf.CeilToInt((float) rewardFood * currentRewardFactor.RewardFoodFactor);
      if (rewardFood > 0)
      {
        this.GetItem().SetResource("food", rewardFood);
        RewardsCollectionAnimator.Instance.Ress.Add(new ResRewardsInfo.Data()
        {
          count = rewardFood,
          rt = ResRewardsInfo.ResType.Food
        });
      }
    }
    if (activeRewardInfo.RewardWood > 0)
    {
      int rewardWood = activeRewardInfo.RewardWood;
      if (currentRewardFactor != null)
        rewardWood = Mathf.CeilToInt((float) rewardWood * currentRewardFactor.RewardWoodFactor);
      if (rewardWood > 0)
      {
        this.GetItem().SetResource("wood", rewardWood);
        RewardsCollectionAnimator.Instance.Ress.Add(new ResRewardsInfo.Data()
        {
          count = rewardWood,
          rt = ResRewardsInfo.ResType.Wood
        });
      }
    }
    if (activeRewardInfo.RewardOre > 0)
    {
      int rewardOre = activeRewardInfo.RewardOre;
      if (currentRewardFactor != null)
        rewardOre = Mathf.CeilToInt((float) rewardOre * currentRewardFactor.RewardOreFactor);
      if (rewardOre > 0)
      {
        this.GetItem().SetResource("ore", rewardOre);
        RewardsCollectionAnimator.Instance.Ress.Add(new ResRewardsInfo.Data()
        {
          count = rewardOre,
          rt = ResRewardsInfo.ResType.Ore
        });
      }
    }
    if (activeRewardInfo.RewardSilver > 0)
    {
      int rewardSilver = activeRewardInfo.RewardSilver;
      if (currentRewardFactor != null)
        rewardSilver = Mathf.CeilToInt((float) rewardSilver * currentRewardFactor.RewardSilverFactor);
      if (rewardSilver > 0)
      {
        this.GetItem().SetResource("silver", rewardSilver);
        RewardsCollectionAnimator.Instance.Ress.Add(new ResRewardsInfo.Data()
        {
          count = rewardSilver,
          rt = ResRewardsInfo.ResType.Silver
        });
      }
    }
    if (activeRewardInfo.RewardLordExp > 0)
    {
      this.XPRewrds = activeRewardInfo.RewardLordExp;
      if (currentRewardFactor != null)
        this.XPRewrds = Mathf.CeilToInt((float) this.XPRewrds * currentRewardFactor.RewardLordEXPFactor);
      if (this.XPRewrds > 0)
        this.GetItem().SetLordEXP(this.XPRewrds);
    }
    if (activeRewardInfo.RewardDragonExp > 0)
    {
      int rewardDragonExp = activeRewardInfo.RewardDragonExp;
      if (currentRewardFactor != null)
        rewardDragonExp = Mathf.CeilToInt((float) rewardDragonExp * currentRewardFactor.RewardDragonEXPFactor);
      if (rewardDragonExp > 0)
        this.GetItem().SetDragonEXP(rewardDragonExp);
    }
    if (activeRewardInfo.LuckArcherCoin > 0)
    {
      float luckArcherCoin = (float) activeRewardInfo.LuckArcherCoin;
      int propsId = MarksmanPayload.Instance.TempMarksmanData.PropsId;
      if (propsId > 0)
      {
        this.GetItem().SetItem(propsId, Mathf.CeilToInt(luckArcherCoin));
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(propsId);
        RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
        {
          count = luckArcherCoin,
          icon = itemStaticInfo.ImagePath
        });
      }
    }
    if (activeRewardInfo.RewardItem1 != 0)
    {
      float f = activeRewardInfo.RewardItemValue1;
      if (currentRewardFactor != null)
        f = Mathf.Ceil(f * currentRewardFactor.RewardItemFactor1);
      if ((double) f > 0.0)
      {
        this.GetItem().SetItem(activeRewardInfo.RewardItem1, Mathf.CeilToInt(f));
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(activeRewardInfo.RewardItem1);
        RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
        {
          count = f,
          icon = itemStaticInfo.ImagePath
        });
      }
    }
    if (activeRewardInfo.RewardItem2 != 0)
    {
      float rewardItemValue2 = activeRewardInfo.RewardItemValue2;
      if (currentRewardFactor != null)
        rewardItemValue2 = (float) Mathf.CeilToInt(rewardItemValue2 * currentRewardFactor.RewardItemFactor2);
      if ((double) rewardItemValue2 > 0.0)
      {
        this.GetItem().SetItem(activeRewardInfo.RewardItem2, Mathf.CeilToInt(rewardItemValue2));
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(activeRewardInfo.RewardItem2);
        RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
        {
          count = rewardItemValue2,
          icon = itemStaticInfo.ImagePath
        });
      }
    }
    if (activeRewardInfo.RewardItem3 != 0)
    {
      float rewardItemValue3 = activeRewardInfo.RewardItemValue3;
      if (currentRewardFactor != null)
        rewardItemValue3 = (float) Mathf.CeilToInt(rewardItemValue3 * currentRewardFactor.RewardItemFactor3);
      if ((double) rewardItemValue3 > 0.0)
      {
        this.GetItem().SetItem(activeRewardInfo.RewardItem3, Mathf.CeilToInt(rewardItemValue3));
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(activeRewardInfo.RewardItem3);
        RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
        {
          count = rewardItemValue3,
          icon = itemStaticInfo.ImagePath
        });
      }
    }
    if (activeRewardInfo.RewardItem4 == 0)
      return;
    float rewardItemValue4 = activeRewardInfo.RewardItemValue4;
    if (currentRewardFactor != null)
      rewardItemValue4 = (float) Mathf.CeilToInt(rewardItemValue4 * currentRewardFactor.RewardItemFactor4);
    if ((double) rewardItemValue4 <= 0.0)
      return;
    this.GetItem().SetItem(activeRewardInfo.RewardItem4, Mathf.CeilToInt(rewardItemValue4));
    ItemStaticInfo itemStaticInfo1 = ConfigManager.inst.DB_Items.GetItem(activeRewardInfo.RewardItem4);
    RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
    {
      count = rewardItemValue4,
      icon = itemStaticInfo1.ImagePath
    });
  }

  private DailyActivyRewardItem GetItem()
  {
    GameObject go = NGUITools.AddChild(this.table.gameObject, this.rewardItemPrefab.gameObject);
    DailyActivyRewardItem component = go.GetComponent<DailyActivyRewardItem>();
    this.items.Add(component);
    NGUITools.SetActive(go, true);
    return component;
  }

  public void OnClaimClickHandler()
  {
    RewardsCollectionAnimator.Instance.CollectItems(false);
    RewardsCollectionAnimator.Instance.CollectResource(false);
    if (this.XPRewrds != 0)
      RewardsCollectionAnimator.Instance.CollectXP(this.XPRewrds, false);
    RequestManager.inst.SendRequest("Quest:openDailyChest", new Hashtable()
    {
      {
        (object) "chest_id",
        (object) this.reward_id
      }
    }, (System.Action<bool, object>) null, true);
    AudioManager.Instance.PlaySound("sfx_city_daily_activity_reward", false);
    this.OnCloseHandler();
  }

  public class Parameter : Popup.PopupParameter
  {
    public int reward_id;
    public System.Action onCloseHandler;
  }
}
