﻿// Decompiled with JetBrains decompiler
// Type: UniCopipe
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class UniCopipe
{
  public static string Value
  {
    get
    {
      string str = string.Empty;
      using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
      {
        using (AndroidJavaObject androidJavaObject1 = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
        {
          using (AndroidJavaObject androidJavaObject2 = androidJavaObject1.Call<AndroidJavaObject>("getSystemService", new object[1]
          {
            (object) "clipboard"
          }))
          {
            using (AndroidJavaObject androidJavaObject3 = androidJavaObject2.Call<AndroidJavaObject>("getPrimaryClip"))
            {
              using (AndroidJavaObject androidJavaObject4 = androidJavaObject3.Call<AndroidJavaObject>("getItemAt", new object[1]
              {
                (object) 0
              }))
              {
                if (androidJavaObject4 != null)
                  str = androidJavaObject4.Call<string>("getText");
              }
            }
          }
        }
      }
      return str;
    }
    set
    {
      AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
      AndroidJavaObject activity = jc.GetStatic<AndroidJavaObject>("currentActivity");
      activity.Call("runOnUiThread", new object[1]
      {
        (object) (AndroidJavaRunnable) (() =>
        {
          AndroidJavaObject androidJavaObject1 = activity.Call<AndroidJavaObject>("getSystemService", new object[1]
          {
            (object) "clipboard"
          });
          AndroidJavaClass androidJavaClass = new AndroidJavaClass("android.content.ClipData");
          AndroidJavaObject androidJavaObject2 = androidJavaClass.CallStatic<AndroidJavaObject>("newPlainText", (object) "clipboard", (object) value);
          androidJavaObject1.Call("setPrimaryClip", new object[1]
          {
            (object) androidJavaObject2
          });
          androidJavaObject2.Dispose();
          androidJavaClass.Dispose();
          androidJavaObject1.Dispose();
          activity.Dispose();
          jc.Dispose();
        })
      });
    }
  }
}
