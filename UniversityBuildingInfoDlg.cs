﻿// Decompiled with JetBrains decompiler
// Type: UniversityBuildingInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class UniversityBuildingInfoDlg : BuildingInfoBaseDlg
{
  public UILabel contentLabel;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    if (this.buildingInfo.Building_Lvl < 30)
      this.contentLabel.text = Utils.XLAT("university_building_info_upgrade_bonuses_description");
    else
      this.contentLabel.text = Utils.XLAT("university_building_info_upgrade_bonuses_complete");
  }
}
