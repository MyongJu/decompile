﻿// Decompiled with JetBrains decompiler
// Type: MarchBaseState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class MarchBaseState : IMarchBaseState, IState
{
  private Hashtable _data = new Hashtable();
  private const float MIN_DISTANCE = 1f;
  private MarchViewControler _controler;
  private bool isInitState;
  private MarchViewControler.Data _stateData;
  private bool _ready;
  private bool pause;
  private bool isFinish;

  public virtual string Key
  {
    get
    {
      return "march_action";
    }
  }

  public MarchViewControler Controler
  {
    get
    {
      return this._controler;
    }
    set
    {
      this._controler = value;
    }
  }

  public bool IsInitState
  {
    get
    {
      return this.isInitState;
    }
    set
    {
      this.isInitState = value;
    }
  }

  public Hashtable Data
  {
    get
    {
      return this._data;
    }
    set
    {
      this._data = value;
    }
  }

  public MarchViewControler.Data StateData
  {
    get
    {
      return this._stateData;
    }
    set
    {
      this._stateData = value;
    }
  }

  public bool IsReady
  {
    get
    {
      return this._ready;
    }
    set
    {
      this._ready = value;
    }
  }

  public virtual void OnEnter()
  {
    if (this.NeedSetUpMarchView())
      this._stateData.troopLength = this.Controler.SetUpMarchView(this.StateData, new System.Action(this.Prepare));
    else
      this.Prepare();
  }

  protected virtual bool NeedSetUpMarchView()
  {
    return true;
  }

  protected virtual void Prepare()
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.Controler)
      return;
    NGUITools.SetActive(this.Controler.gameObject, true);
    this.Controler.ShowClickCollider();
    this.Controler.ShowAt(this.StateData.startPosition);
    this.Controler.Rotate(this.StateData.dirV3);
    this.Controler.CreatePath();
    this.Controler.MoveAction();
  }

  protected virtual void OnProcessHandler()
  {
    this.Move();
  }

  public void OnProcess()
  {
    this.OnProcessHandler();
  }

  protected void Move()
  {
    if (this.pause)
      return;
    if ((UnityEngine.Object) this.Controler == (UnityEngine.Object) null || this.Controler.marchData == null || (UnityEngine.Object) this.Controler.gameObject == (UnityEngine.Object) null)
      this.pause = true;
    else if (NetServerTime.inst.UpdateTime < this.StateData.endTime && NetServerTime.inst.UpdateTime > this.StateData.startTime)
    {
      this.Controler.transform.position = Vector3.Lerp(this.StateData.startPosition, this.StateData.moveTargetPosition, (float) ((NetServerTime.inst.UpdateTime - this.StateData.startTime) / (this.StateData.endTime - this.StateData.startTime)));
    }
    else
    {
      if (NetServerTime.inst.UpdateTime < this.StateData.endTime && this.StateData.endTime > this.StateData.startTime)
        return;
      if (!this.isFinish)
      {
        this.isFinish = true;
        this.OnMoveFinished();
      }
      if ((double) Vector3.Distance(this.Controler.transform.localPosition, this.StateData.moveTargetPosition) > 1.0)
        this.Controler.transform.localPosition = this.StateData.moveTargetPosition;
      if (this.Controler.HasNextState())
      {
        this.OnStateEnd();
        this.Controler.NextState();
      }
      else
      {
        if (!this.Controler.marchData.IsNpcMarch)
          return;
        NGUITools.SetActive(this.Controler.gameObject, false);
      }
    }
  }

  protected virtual void OnMoveFinished()
  {
  }

  protected virtual void OnStateEnd()
  {
  }

  public virtual void OnExit()
  {
  }

  public void Dispose()
  {
    this.Controler = (MarchViewControler) null;
    this.Data = (Hashtable) null;
  }
}
