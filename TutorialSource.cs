﻿// Decompiled with JetBrains decompiler
// Type: TutorialSource
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class TutorialSource
{
  public List<TutorialSequence> seqData;
  public Dictionary<string, List<string>> pipeline;

  public TutorialSource()
  {
    this.seqData = new List<TutorialSequence>();
    this.pipeline = new Dictionary<string, List<string>>();
    this.pipeline.Add("test", new List<string>() { "testi" });
  }
}
