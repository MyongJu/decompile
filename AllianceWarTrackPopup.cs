﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarTrackPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceWarTrackPopup : Popup
{
  private Dictionary<int, AllianceWarTrackSlot> _itemDict = new Dictionary<int, AllianceWarTrackSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private Dictionary<string, string> keyParam = new Dictionary<string, string>();
  [SerializeField]
  private AllianceSymbol _allianceSymbol;
  [SerializeField]
  private UILabel _labelAllianceName;
  [SerializeField]
  private UILabel _labelLeaderName;
  [SerializeField]
  private UILabel _labelAllianceMemberCount;
  [SerializeField]
  private GameObject _objTrack;
  [SerializeField]
  private GameObject _objTracked;
  [SerializeField]
  private UILabel _labelTrackDescription;
  [SerializeField]
  private UILabel _labelTrackCost;
  [SerializeField]
  private UILabel _labelTrackingDescription1;
  [SerializeField]
  private UILabel _labelTrackingDescription2;
  [SerializeField]
  private UILabel _labelTrackingDescription3;
  [SerializeField]
  private UILabel _labelTrackingRefresh;
  [SerializeField]
  private UIButton _buttonTrackingRefresh;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UIGrid _grid;
  [SerializeField]
  private GameObject _itemPrefab;
  private Color _trackCostOriginColor;
  private long _oppAllianceId;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    AllianceWarTrackPopup.Parameter parameter = orgParam as AllianceWarTrackPopup.Parameter;
    if (parameter != null)
      this._oppAllianceId = parameter.oppAllianceId;
    this._trackCostOriginColor = this._labelTrackCost.color;
    this.AddEventHandler();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.ClearData();
  }

  public void OnCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnRefreshClicked()
  {
    AllianceWarPayload.Instance.LoadTrack(this._oppAllianceId, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.UpdateUI();
    }));
  }

  public void OnTrackClicked()
  {
    if (!AllianceWarPayload.Instance.TrackData.IsGoldEnough2Track)
    {
      this.OnCloseClicked();
      Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
    }
    else if (!AllianceWarPayload.Instance.IsAllianceWarStarted)
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_trace_not_ready"), (System.Action) null, 4f, true);
    else
      AllianceWarPayload.Instance.StartTrack(this._oppAllianceId, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.UpdateUI();
      }));
  }

  private void ClearData()
  {
    using (Dictionary<int, AllianceWarTrackSlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this._grid.repositionNow = true;
    this._grid.Reposition();
    this._scrollView.ResetPosition();
  }

  private AllianceWarTrackSlot GenerateSlot()
  {
    this._itemPool.Initialize(this._itemPrefab, this._grid.gameObject);
    this._itemPrefab.SetActive(false);
    GameObject gameObject = this._itemPool.AddChild(this._grid.gameObject);
    gameObject.transform.localScale *= 0.88f;
    gameObject.SetActive(true);
    return gameObject.GetComponent<AllianceWarTrackSlot>();
  }

  private void UpdateUI()
  {
    this.ShowMemberContent();
    this.ShowMainContent();
    this.UpdateBottomContent();
    this.UpdateBottomContentOnce();
  }

  private void ShowMemberContent()
  {
    this.ClearData();
    List<AllianceWarTrackData.TrackMemberData> memberDataList = AllianceWarPayload.Instance.TrackData.MemberDataList;
    if (memberDataList != null)
    {
      for (int key = 0; key < memberDataList.Count; ++key)
      {
        AllianceWarTrackSlot slot = this.GenerateSlot();
        slot.SetData(memberDataList[key]);
        this._itemDict.Add(key, slot);
      }
    }
    this.Reposition();
  }

  private void ShowMainContent()
  {
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(this._oppAllianceId);
    if (allianceData == null)
      return;
    this._allianceSymbol.SetSymbols(allianceData.allianceSymbolCode);
    this._labelAllianceName.text = allianceData.allianceFullName;
    this._labelLeaderName.text = AllianceWarPayload.Instance.TrackData.GetByTrackingName(allianceData.creatorId);
    this._labelAllianceMemberCount.text = allianceData.memberCount.ToString();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void RemoveEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  private void OnSecondEvent(int time)
  {
    this.UpdateBottomContent();
  }

  private void UpdateBottomContentOnce()
  {
    int num1 = NetServerTime.inst.ServerTimestamp - AllianceWarPayload.Instance.TrackData.LastRefreshTime;
    int num2 = num1 / 3600 / 24;
    int num3 = num1 / 3600 % 24;
    int num4 = num1 % 3600 / 60;
    this.keyParam.Clear();
    if (num2 > 0)
      this.keyParam.Add("0", num2.ToString() + Utils.XLAT("chat_time_day"));
    else if (num3 > 0)
      this.keyParam.Add("0", num3.ToString() + Utils.XLAT("chat_time_hour"));
    else
      this.keyParam.Add("0", (num4 <= 0 ? "1" : num4.ToString()) + Utils.XLAT("chat_time_min"));
    this._labelTrackingDescription1.text = ScriptLocalization.GetWithPara("alliance_warfare_tracking_time_cd", this.keyParam, true);
  }

  private void UpdateBottomContent()
  {
    NGUITools.SetActive(this._objTrack, !AllianceWarPayload.Instance.TrackData.IsTracking);
    NGUITools.SetActive(this._objTracked, AllianceWarPayload.Instance.TrackData.IsTracking);
    if (!AllianceWarPayload.Instance.TrackData.IsTracking)
    {
      this.keyParam.Clear();
      this.keyParam.Add("0", AllianceWarPayload.Instance.TrackData.TrackingDuration.ToString());
      this._labelTrackDescription.text = ScriptLocalization.GetWithPara("alliance_warfare_view_position_description", this.keyParam, true);
      this._labelTrackCost.text = AllianceWarPayload.Instance.TrackData.TrackCost.ToString();
      this._labelTrackingRefresh.text = Utils.XLAT("id_uppercase_refresh");
    }
    else
    {
      this._labelTrackingDescription2.text = Utils.XLAT("alliance_warfare_position_see_description");
      this.keyParam.Clear();
      this.keyParam.Add("0", AllianceWarPayload.Instance.TrackData.StartTracker);
      this.keyParam.Add("1", Utils.FormatTime(AllianceWarPayload.Instance.TrackData.TrackEndTime - NetServerTime.inst.ServerTimestamp, true, false, true));
      this._labelTrackingDescription3.text = ScriptLocalization.GetWithPara("alliance_warfare_tracking_notice", this.keyParam, true);
      int num = AllianceWarPayload.Instance.TrackData.NextRefreshTime - NetServerTime.inst.ServerTimestamp;
      this._labelTrackingRefresh.text = num <= 0 ? Utils.XLAT("id_uppercase_refresh") : string.Format("{0}({1})", (object) Utils.XLAT("id_uppercase_refresh"), (object) num);
    }
    this._buttonTrackingRefresh.isEnabled = NetServerTime.inst.ServerTimestamp >= AllianceWarPayload.Instance.TrackData.NextRefreshTime;
    if (!AllianceWarPayload.Instance.TrackData.IsGoldEnough2Track)
      this._labelTrackCost.color = Color.red;
    else
      this._labelTrackCost.color = this._trackCostOriginColor;
  }

  public class Parameter : Popup.PopupParameter
  {
    public long oppAllianceId;
  }
}
