﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsLayerInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class MerlinTrialsLayerInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "trial_id")]
  public int trialId;
  [Config(Name = "layer")]
  public int level;
  [Config(Name = "background")]
  public string background;

  public string BackgroundPath
  {
    get
    {
      return "Texture/STATIC_TEXTURE/" + this.background;
    }
  }
}
