﻿// Decompiled with JetBrains decompiler
// Type: WarReportDetailPage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class WarReportDetailPage : MonoBehaviour
{
  public PageTabsMonitor tabMonitor;
  public WarReportTroopsPage troopsPage;
  public WarReportRoundSummaryPage roundSummaryPage;
  public GameObject defeatHeader;
  public GameObject victoryHeader;
  private Hashtable _data;
  private WarReportDetailPage.IReportSubPage _currentPage;
  private UIPanel _panel;

  public void Start()
  {
    this._panel = this.gameObject.GetComponent<UIPanel>();
  }

  public void SetData(Hashtable battleData, bool winner)
  {
    this.victoryHeader.SetActive(winner);
    this.defeatHeader.SetActive(!winner);
    this._data = battleData;
    this.troopsPage.SetData(this._data);
    this.roundSummaryPage.SetData(this._data);
  }

  public void PreShow()
  {
    this.troopsPage.gameObject.SetActive(true);
    this.roundSummaryPage.gameObject.SetActive(true);
    this.troopsPage.Hide();
    this.roundSummaryPage.Hide();
  }

  public void Show()
  {
    this._panel.alpha = 1f;
    if (this._currentPage == null)
      this._currentPage = (WarReportDetailPage.IReportSubPage) this.troopsPage;
    this.troopsPage.gameObject.SetActive(false);
    this.roundSummaryPage.gameObject.SetActive(false);
    this._currentPage.Show();
  }

  public void Hide()
  {
    this._panel.alpha = 0.01f;
    this.troopsPage.Show();
    this.roundSummaryPage.Hide();
  }

  public void SetView(WarReportDetailPage.WarReportSubPage subPage)
  {
    if (this._currentPage != null)
      this._currentPage.Hide();
    switch (subPage)
    {
      case WarReportDetailPage.WarReportSubPage.Details:
        this.troopsPage.gameObject.SetActive(false);
        this._currentPage = (WarReportDetailPage.IReportSubPage) this.roundSummaryPage;
        this.roundSummaryPage.gameObject.SetActive(true);
        break;
      case WarReportDetailPage.WarReportSubPage.Troops:
        this.roundSummaryPage.gameObject.SetActive(false);
        this._currentPage = (WarReportDetailPage.IReportSubPage) this.troopsPage;
        this.troopsPage.gameObject.SetActive(true);
        break;
    }
    if (this._currentPage == null)
      return;
    this._currentPage.Show();
  }

  public void ShowDetails()
  {
    this.SetView(WarReportDetailPage.WarReportSubPage.Details);
  }

  public void ShowTroops()
  {
    this.SetView(WarReportDetailPage.WarReportSubPage.Troops);
  }

  public void ShowBonus()
  {
    this.SetView(WarReportDetailPage.WarReportSubPage.Bonus);
  }

  public enum WarReportSubPage
  {
    None,
    Details,
    Troops,
    Bonus,
  }

  public interface IReportSubPage
  {
    void SetData(Hashtable data);

    void Show();

    void Hide();
  }
}
