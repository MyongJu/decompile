﻿// Decompiled with JetBrains decompiler
// Type: AllianceBuildingType
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public static class AllianceBuildingType
{
  public const int FORTRESS = 0;
  public const int WATCHTOWER = 1;
  public const int WAREHOUSE = 2;
  public const int FARM = 3;
  public const int SAWMILL = 4;
  public const int MINE = 5;
  public const int HOUSE = 6;
  public const int PORTAL = 7;
  public const int HOSPITAL = 8;
  public const int DRAGON_ALTAR = 9;
  public const int INVALID = 2147483647;
}
