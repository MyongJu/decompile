﻿// Decompiled with JetBrains decompiler
// Type: DataTable
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class DataTable
{
  public DataTable()
  {
    this.Columns = new List<string>();
    this.Rows = new List<DataRow>();
  }

  public List<string> Columns { get; set; }

  public List<DataRow> Rows { get; set; }

  public DataRow this[int row]
  {
    get
    {
      return this.Rows[row];
    }
  }

  public void AddRow(object[] values)
  {
    if (values.Length != this.Columns.Count)
      throw new IndexOutOfRangeException("The number of values in the row must match the number of column");
    DataRow dataRow = new DataRow();
    for (int index = 0; index < values.Length; ++index)
      dataRow[this.Columns[index]] = values[index];
    this.Rows.Add(dataRow);
  }
}
