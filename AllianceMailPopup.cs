﻿// Decompiled with JetBrains decompiler
// Type: AllianceMailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceMailPopup : BaseReportPopup
{
  private List<GameObject> pool = new List<GameObject>();
  public float gapX = 100f;
  private Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>> allmytroopdetail = new Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>>();
  private Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>> allothertroopdetail = new Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>>();
  private WarReportContent wrc;
  public UIScrollView sv;
  public UILabel winLbl;
  public UILabel lossLbl;
  public ItemInfo resreward;
  public ItemInfo lightpower;
  public ItemInfo darkpower;
  public WarOverview mywaroverview;
  public WarOverview otherwaroverview;
  public TroopOverview mytroopoverview;
  public TroopOverview othertroopoverview;
  public GameObject boostdetailGroup;
  public BoostInfo myboostinfo;
  public BoostInfo otherboostinfo;
  public UISprite myboostbg;
  public UISprite otherboostbg;
  public GameObject btnGroup;
  private TroopLeader mytd;
  private TroopLeader othertd;

  private void Init()
  {
    bool flag = this.wrc.IsWinner();
    List<ItemInfo.Data> dataList = new List<ItemInfo.Data>();
    List<TroopDetail> tds1;
    List<TroopDetail> tds2;
    if (this.wrc.IsAttacker())
    {
      this.mytd = this.wrc.attacker;
      this.othertd = this.wrc.defender;
      tds1 = this.wrc.attacker_troop_detail;
      tds2 = this.wrc.defender_troop_detail;
    }
    else
    {
      this.mytd = this.wrc.defender;
      this.othertd = this.wrc.attacker;
      tds1 = this.wrc.defender_troop_detail;
      tds2 = this.wrc.attacker_troop_detail;
    }
    string str1 = this.mytd.acronym == null ? (string) null : "(" + this.mytd.acronym + ")";
    string str2 = this.othertd.acronym == null ? (string) null : "(" + this.othertd.acronym + ")";
    string str3 = str1 + this.mytd.name;
    string str4 = str2 + this.othertd.name;
    long allPowerLost1 = this.wrc.CalculateAllPowerLost(tds1);
    long allPowerLost2 = this.wrc.CalculateAllPowerLost(tds2);
    long allTroopKill1 = this.wrc.CalculateAllTroopKill(tds1);
    long allTroopKill2 = this.wrc.CalculateAllTroopKill(tds2);
    this.wrc.CalculateAllTroopNotGood(tds1);
    this.wrc.CalculateAllTroopNotGood(tds2);
    long allTroopLost1 = this.wrc.CalculateAllTroopLost(tds1);
    long allTroopLost2 = this.wrc.CalculateAllTroopLost(tds2);
    long allTroopWound1 = this.wrc.CalculateAllTroopWound(tds1);
    long allTroopWound2 = this.wrc.CalculateAllTroopWound(tds2);
    long allTroopSurvive1 = this.wrc.CalculateAllTroopSurvive(tds1);
    long allTroopSurvive2 = this.wrc.CalculateAllTroopSurvive(tds2);
    this.wrc.SummarizeAllTroopDetail(tds1, ref this.allmytroopdetail);
    this.wrc.SummarizeAllTroopDetail(tds2, ref this.allothertroopdetail);
    List<BoostInfo.Data> boostdetail1 = new List<BoostInfo.Data>();
    List<BoostInfo.Data> boostdetail2 = new List<BoostInfo.Data>();
    this.mytd.SummarizeBoostDetail(ref boostdetail1);
    this.othertd.SummarizeBoostDetail(ref boostdetail2);
    this.winLbl.gameObject.SetActive(flag);
    this.lossLbl.gameObject.SetActive(!flag);
    if (flag)
      BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, true, false, string.Empty);
    else
      BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_loser", (System.Action<bool>) null, true, false, string.Empty);
    this.time.text = Utils.FormatTimeForMail(this.mailtime);
    List<Vector3> tarpos = new List<Vector3>(dataList.Count);
    Utils.ArrangePosition(this.resreward.transform.localPosition, dataList.Count, this.gapX, 0.0f, ref tarpos);
    for (int index = 0; index < tarpos.Count; ++index)
    {
      GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.resreward.gameObject, this.resreward.transform.parent);
      this.pool.Add(gameObject);
      gameObject.transform.localPosition = tarpos[index];
      gameObject.GetComponent<ItemInfo>().SeedData(dataList[index]);
    }
    ItemInfo.Data d1 = new ItemInfo.Data();
    d1.name = this.wrc.dragon_attr == null ? "0" : this.wrc.dragon_attr.light;
    this.lightpower.SeedData(d1);
    d1.name = this.wrc.dragon_attr == null ? "0" : this.wrc.dragon_attr.dark;
    this.darkpower.SeedData(d1);
    WarOverview.Data d2 = new WarOverview.Data();
    d2.name = str3;
    d2.icon = "Texture/Hero/Portrait_Icon/player_portrait_icon_" + this.mytd.portrait;
    d2.setIcon = this.mytd.icon;
    d2.lordTitleId = this.mytd.lord_title;
    d2.k = this.mytd.k;
    d2.x = this.mytd.x;
    d2.y = this.mytd.y;
    d2.powerlost = allPowerLost1;
    d2.dsht = this.mytd.dragon;
    d2.trooplost = allTroopLost1;
    this.mywaroverview.SeedData(d2);
    d2.name = str4;
    d2.icon = "Texture/Hero/Portrait_Icon/player_portrait_icon_" + this.othertd.portrait;
    d2.setIcon = this.othertd.icon;
    d2.lordTitleId = this.othertd.lord_title;
    d2.k = this.othertd.k;
    d2.x = this.othertd.x;
    d2.y = this.othertd.y;
    d2.powerlost = allPowerLost2;
    d2.dsht = this.othertd.dragon;
    d2.trooplost = allTroopLost2;
    this.otherwaroverview.SeedData(d2);
    TroopOverview.Data d3 = new TroopOverview.Data();
    d3.kill = allTroopKill1;
    d3.dragonAoeKill = (long) this.mytd.dragon_skill_aoe_killed;
    d3.dead = allTroopLost1;
    d3.wounded = allTroopWound1;
    d3.survived = allTroopSurvive1;
    this.mytroopoverview.SeedData(d3);
    d3.kill = allTroopKill2;
    d3.dragonAoeKill = (long) this.othertd.dragon_skill_aoe_killed;
    d3.dead = allTroopLost2;
    d3.wounded = allTroopWound2;
    d3.survived = allTroopSurvive2;
    this.othertroopoverview.SeedData(d3);
    float height = (float) this.myboostinfo.GetComponent<UIWidget>().height;
    float num1 = this.myboostinfo.transform.localPosition.y + height / 2f;
    using (List<BoostInfo.Data>.Enumerator enumerator = boostdetail1.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BoostInfo.Data current = enumerator.Current;
        float num2 = num1 - height / 2f;
        GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.myboostinfo.gameObject, this.myboostinfo.transform.parent);
        this.pool.Add(gameObject);
        Vector3 localPosition = this.myboostinfo.transform.localPosition;
        localPosition.y = num2;
        gameObject.transform.localPosition = localPosition;
        num1 = num2 - height / 2f;
        gameObject.GetComponent<BoostInfo>().SeedData(current);
      }
    }
    if (boostdetail1.Count != 0)
    {
      this.myboostbg.gameObject.SetActive(true);
      this.myboostbg.height = boostdetail1.Count * (int) height;
    }
    else
      this.myboostbg.gameObject.SetActive(false);
    float num3 = this.otherboostinfo.transform.localPosition.y + height / 2f;
    using (List<BoostInfo.Data>.Enumerator enumerator = boostdetail2.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BoostInfo.Data current = enumerator.Current;
        float num2 = num3 - height / 2f;
        GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.otherboostinfo.gameObject, this.otherboostinfo.transform.parent);
        this.pool.Add(gameObject);
        Vector3 localPosition = this.otherboostinfo.transform.localPosition;
        localPosition.y = num2;
        gameObject.transform.localPosition = localPosition;
        num3 = num2 - height / 2f;
        gameObject.GetComponent<BoostInfo>().SeedData(current);
      }
    }
    if (boostdetail2.Count != 0)
    {
      this.otherboostbg.gameObject.SetActive(true);
      this.otherboostbg.height = boostdetail2.Count * (int) height;
    }
    else
      this.otherboostbg.gameObject.SetActive(false);
    this.ArrangeLayout();
    this.HideTemplate(true);
  }

  public void OnLoadTroopDetail()
  {
    UIManager.inst.OpenPopup("Report/WarReportDetail", (Popup.PopupParameter) new WarReportDetail.Parameter()
    {
      pattackertroopdetail = this.allmytroopdetail,
      pdefendertroopdetail = this.allothertroopdetail
    });
  }

  private void ArrangeLayout()
  {
    float y1 = this.boostdetailGroup.transform.localPosition.y;
    float y2 = NGUIMath.CalculateRelativeWidgetBounds(this.boostdetailGroup.transform).size.y;
    Vector3 localPosition = this.btnGroup.transform.localPosition;
    localPosition.y = y1 - y2;
    this.btnGroup.transform.localPosition = localPosition;
    this.sv.UpdatePosition();
  }

  private void Reset()
  {
    this.sv.ResetPosition();
    Rigidbody component = this.sv.transform.GetComponent<Rigidbody>();
    if ((UnityEngine.Object) null != (UnityEngine.Object) component)
    {
      component.isKinematic = true;
      component.useGravity = false;
    }
    this.HideTemplate(false);
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        PrefabManagerEx.Instance.Destroy(current);
      }
    }
    this.pool.Clear();
  }

  private void HideTemplate(bool hide)
  {
    this.resreward.gameObject.SetActive(!hide);
    this.myboostinfo.gameObject.SetActive(!hide);
    this.otherboostinfo.gameObject.SetActive(!hide);
  }

  public void GotoMycity()
  {
    this.GoToTargetPlace(int.Parse(this.mytd.k), int.Parse(this.mytd.x), int.Parse(this.mytd.y));
  }

  public void GotoEnemycity()
  {
    this.GoToTargetPlace(int.Parse(this.othertd.k), int.Parse(this.othertd.x), int.Parse(this.othertd.y));
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.wrc = JsonReader.Deserialize<WarReport>(Utils.Object2Json((object) this.param.hashtable)).data;
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Reset();
  }
}
