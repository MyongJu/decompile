﻿// Decompiled with JetBrains decompiler
// Type: PlayerProfileDetailEquipmentSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class PlayerProfileDetailEquipmentSlot : MonoBehaviour
{
  [SerializeField]
  private GameObject m_Equipped;
  [SerializeField]
  private GameObject m_Unequipped;
  [SerializeField]
  private UITexture m_ItemIcon;
  [SerializeField]
  private UITexture m_ItemQuality;
  [SerializeField]
  private UITexture m_ItemCategory;
  [SerializeField]
  private HeroItemType m_EquipmentType;
  [SerializeField]
  private Transform m_Border;
  private UserData m_UserData;
  private InventoryItemData m_ItemData;

  public void SetData(UserData userData)
  {
    this.m_UserData = userData;
    this.m_ItemData = DBManager.inst.GetInventory(BagType.Hero).GetEquippedItemByType(this.m_UserData.uid, (int) this.m_EquipmentType);
    this.UpdateCategory();
    this.UpdateSlot();
  }

  public void OnPressHandler()
  {
    if (this.m_ItemData == null || this.m_ItemData.equipment.itemID <= 0)
      return;
    Utils.DelayShowTip(this.m_ItemData.equipment.itemID, this.m_Border, this.m_ItemData.itemId, this.m_ItemData.uid, 0);
  }

  public void OnReleaseHandler()
  {
    Utils.StopShowItemTip();
  }

  public void OnClicked()
  {
    if (this.m_ItemData == null || this.m_ItemData.equipment.itemID <= 0)
      return;
    Utils.ShowItemTip(this.m_ItemData.equipment.itemID, this.m_Border, this.m_ItemData.itemId, this.m_ItemData.uid, 0);
  }

  private void UpdateCategory()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemCategory, "Texture/Equipment/icon_equipment_" + (object) this.m_EquipmentType, (System.Action<bool>) null, true, false, string.Empty);
  }

  private void UpdateSlot()
  {
    if (this.m_ItemData != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, this.m_ItemData.equipment.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemQuality, this.m_ItemData.equipment.QualityImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this.m_Equipped.SetActive(true);
      this.m_Unequipped.SetActive(false);
    }
    else
    {
      this.m_Equipped.SetActive(false);
      this.m_Unequipped.SetActive(true);
    }
  }
}
