﻿// Decompiled with JetBrains decompiler
// Type: AuctionRecordItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AuctionRecordItemRenderer : MonoBehaviour
{
  public ItemIconRenderer itemRenderer;
  public UILabel itemCount;
  public UILabel itemWinTime;
  public UILabel itemWinPrice;
  public UILabel winnerName;
  public UITexture itemArtifactTexture;
  public UITexture itemArtifactFrame;
  private AuctionRecordItemInfo _itemInfo;

  public void SetData(AuctionRecordItemInfo itemInfo)
  {
    this._itemInfo = itemInfo;
    this.UpdateUI();
  }

  public void OnRecordItemClicked()
  {
    UIManager.inst.OpenPopup("Auction/AuctionHallRecordInfoPopup", (Popup.PopupParameter) new AuctionHallRecordInfoPopup.Parameter()
    {
      auctionRecordItemInfo = this._itemInfo
    });
  }

  private void UpdateUI()
  {
    if (this._itemInfo == null)
      return;
    this.itemCount.text = "x" + (object) this._itemInfo.itemCount;
    this.itemWinPrice.text = this._itemInfo.itemWinPrice.ToString();
    this.winnerName.text = Utils.XLAT("exchange_history_winner") + this._itemInfo.winerName;
    int num1 = NetServerTime.inst.ServerTimestamp - this._itemInfo.itemWinTime;
    int num2 = num1 / 3600 / 24;
    int num3 = num1 / 3600 % 24;
    int num4 = num1 % 3600 / 60;
    this.itemWinTime.text = ScriptLocalization.GetWithPara("chat_time_display", new Dictionary<string, string>()
    {
      {
        "d",
        num2 <= 0 ? string.Empty : num2.ToString() + Utils.XLAT("chat_time_day")
      },
      {
        "h",
        num3 <= 0 ? string.Empty : num3.ToString() + Utils.XLAT("chat_time_hour")
      },
      {
        "m",
        num4 < 0 ? string.Empty : num4.ToString() + Utils.XLAT("chat_time_min")
      }
    }, true);
    if (this._itemInfo.IsArtifact)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.itemArtifactFrame, "Texture/Equipment/frame_equipment_5", (System.Action<bool>) null, true, false, string.Empty);
      NGUITools.SetActive(this.itemRenderer.equipmentItemFrame.gameObject, false);
      NGUITools.SetActive(this.itemArtifactFrame.gameObject, true);
      ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this._itemInfo.itemArtifactId);
      if (artifactInfo == null)
        return;
      BuilderFactory.Instance.HandyBuild((UIWidget) this.itemArtifactTexture, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    }
    else
      this.itemRenderer.ItemId = this._itemInfo.itemId;
  }
}
