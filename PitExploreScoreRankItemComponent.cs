﻿// Decompiled with JetBrains decompiler
// Type: PitExploreScoreRankItemComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class PitExploreScoreRankItemComponent : ComponentRenderBase
{
  [SerializeField]
  private GameObject _BgGameObject;
  [SerializeField]
  private UITexture _RankTop3;
  [SerializeField]
  private UILabel _Rank;
  [SerializeField]
  private UILabel _Kingdom;
  [SerializeField]
  private UILabel _Name;
  [SerializeField]
  private UILabel _Score;
  [SerializeField]
  private UITexture _PlayerIcon;

  public override void Init()
  {
    base.Init();
    PitExploreScoreRankItemComponentData data = this.data as PitExploreScoreRankItemComponentData;
    if (data == null)
      return;
    PitScoreRankData rankData = data.rankData;
    if (rankData == null)
      return;
    this.SetRankBg(rankData.rank);
    this._Kingdom.text = rankData.world_id.ToString();
    this._Name.text = rankData.acronym.Length <= 0 ? rankData.name : string.Format("[{0}]{1}", (object) rankData.acronym, (object) rankData.name);
    this._Score.text = rankData.score.ToString();
    CustomIconLoader.Instance.requestCustomIcon(this._PlayerIcon, UserData.BuildPortraitPath(rankData.portrait), rankData.icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this._PlayerIcon, rankData.lordTitleId, 1);
  }

  private void SetRankBg(int rank)
  {
    GameObject gameObject1 = this._BgGameObject.transform.Find("firstBg").gameObject;
    GameObject gameObject2 = this._BgGameObject.transform.Find("secondBg").gameObject;
    GameObject gameObject3 = this._BgGameObject.transform.Find("thirdBg").gameObject;
    GameObject gameObject4 = this._BgGameObject.transform.Find("normalBg").gameObject;
    NGUITools.SetActive(gameObject1, rank == 1);
    NGUITools.SetActive(gameObject2, rank == 2);
    NGUITools.SetActive(gameObject3, rank == 3);
    NGUITools.SetActive(gameObject4, rank > 3);
    NGUITools.SetActive(this._RankTop3.gameObject, rank <= 3);
    BuilderFactory.Instance.HandyBuild((UIWidget) this._RankTop3, string.Format("Texture/LeaderboardIcons/icon_no{0}", (object) rank), (System.Action<bool>) null, true, false, string.Empty);
    NGUITools.SetActive(this._Rank.gameObject, rank > 3);
    this._Rank.text = rank.ToString();
  }

  public override void Dispose()
  {
    base.Dispose();
  }
}
