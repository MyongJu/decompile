﻿// Decompiled with JetBrains decompiler
// Type: ForgeBuildingInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;

public class ForgeBuildingInfoDlg : BuildingInfoBaseDlg
{
  public UITexture texture1;
  public UILabel nameLabel1;
  public UILabel valueLabel1;
  public UITexture texture2;
  public UILabel nameLabel2;
  public UILabel valueLabel2;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    if (this.buildingInfo.Benefit_ID_1.Length > 0)
      this.DrawBenefit(this.buildingInfo.Benefit_Value_1, this.nameLabel1, this.valueLabel1, int.Parse(this.buildingInfo.Benefit_ID_1), this.buildingInfo.Benefit_Value_1, "calc_crafting_time", this.texture1);
    if (this.buildingInfo.Benefit_ID_2.Length <= 0)
      return;
    this.DrawBenefit(this.buildingInfo.Benefit_Value_2, this.nameLabel2, this.valueLabel2, int.Parse(this.buildingInfo.Benefit_ID_2), this.buildingInfo.Benefit_Value_2, "calc_crafting_steel_cost", this.texture2);
  }

  private void DrawBenefit(double benifitValue, UILabel nameLabel, UILabel valueLabel, int benefitInternalID, double baseValue, string caclID, UITexture texture = null)
  {
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitInternalID];
    nameLabel.text = dbProperty.Name;
    if ((UnityEngine.Object) texture != (UnityEngine.Object) null)
      BuilderFactory.Instance.HandyBuild((UIWidget) texture, dbProperty.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    float benefitPercentage = ConfigManager.inst.DB_BenefitCalc.GetBenefitPercentage(caclID);
    string str = baseValue <= 0.0 ? "-" : "+";
    if (dbProperty.Format == PropertyDefinition.FormatType.Percent)
      valueLabel.text = dbProperty.ConvertToDisplayString(baseValue, false, true) + str + dbProperty.ConvertToDisplayStringWithoutPreString(Math.Abs((double) benefitPercentage - baseValue), true, true, baseValue > 0.0);
    else
      valueLabel.text = dbProperty.ConvertToDisplayString(baseValue, false, true) + str + dbProperty.ConvertToDisplayString(Math.Abs((double) benefitPercentage - baseValue), true, true);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }
}
