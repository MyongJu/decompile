﻿// Decompiled with JetBrains decompiler
// Type: CityBuffUse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;

public class CityBuffUse : ItemBaseUse
{
  public CityBuffUse(int id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  public CityBuffUse(string id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  protected override void CustomUse(System.Action<bool, object> resultHandler)
  {
    this.OnUseItem(resultHandler);
  }

  private void OnUseItem(System.Action<bool, object> resultHandler)
  {
    bool flag = Utils.CheckBoostIsStart(ConfigManager.inst.DB_Boosts.GetBoostByItemInternalId(this.Info.internalId).Type);
    string str1 = ScriptLocalization.Get("boost_replace_warning", true);
    if (flag)
    {
      string str2 = ScriptLocalization.Get("shop_buy_yesbtlabel", true);
      string str3 = ScriptLocalization.Get("shop_buy_nobtlabel", true);
      string str4 = ScriptLocalization.Get("shop_buy_useTitle", true);
      UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
      {
        setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
        title = str4,
        leftButtonText = str3,
        leftButtonClickEvent = (System.Action) null,
        rightButtonText = str2,
        rightButtonClickEvent = (System.Action) (() => this.DirectlyUse(resultHandler)),
        description = str1
      });
    }
    else
      this.DirectlyUse(resultHandler);
  }
}
