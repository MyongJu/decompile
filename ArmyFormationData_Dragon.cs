﻿// Decompiled with JetBrains decompiler
// Type: ArmyFormationData_Dragon
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ArmyFormationData_Dragon : ArmyFormationData
{
  protected override MarchConfig.MarchInfo configInfo
  {
    get
    {
      return (MarchConfig.MarchInfo) MarchConfig.dragonInfo;
    }
  }

  public override Vector3[] CalcAttackFormation(Vector3 deltaPos, float radius, Vector3 inDir)
  {
    Vector3[] pos = new Vector3[this.curUnitCount];
    Vector3 vector3 = inDir;
    vector3.z = 0.0f;
    vector3 = vector3.normalized;
    pos[0] = -vector3;
    this.RecalcAttackFormation(ref pos, deltaPos, radius);
    return pos;
  }
}
