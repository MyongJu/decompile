﻿// Decompiled with JetBrains decompiler
// Type: DynamicReplaceItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class DynamicReplaceItem : MonoBehaviour
{
  public System.Action onFlyOutFinished;
  public System.Action onFlyInFinished;

  public abstract void Init(object args);

  public virtual void FlyOut(System.Action action = null)
  {
    Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.transform);
    Debug.LogWarning((object) ("sts is " + (object) relativeWidgetBounds.size.x + "====" + (object) relativeWidgetBounds.size.y));
  }

  public virtual void FlyIn(System.Action action = null)
  {
  }
}
