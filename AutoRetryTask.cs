﻿// Decompiled with JetBrains decompiler
// Type: AutoRetryTask
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AutoRetryTask : BaseTask
{
  private int _executeTime = -1;
  private int _retryMaxTime = 30;
  private float _checkIntervalTime = 2f;
  private bool isWait;
  public System.Action ExecuteHandler;
  private int _startTime;
  private bool _success;

  public float IntervalTime
  {
    get
    {
      return this._checkIntervalTime;
    }
    set
    {
      this._checkIntervalTime = value;
    }
  }

  public bool CanRetry
  {
    get
    {
      return (double) Time.time - (double) this.ExecuteTime >= (double) this._checkIntervalTime;
    }
  }

  public void Wait()
  {
    if (this.isWait)
      return;
    this.isWait = true;
  }

  public override void Execute()
  {
    this.isWait = false;
    if (this.ExecuteHandler == null)
      return;
    this.ExecuteHandler();
  }

  public int RetryMaxTime
  {
    get
    {
      return this._retryMaxTime;
    }
    set
    {
      this._retryMaxTime = value;
    }
  }

  public int StartTime
  {
    get
    {
      return this._startTime;
    }
    set
    {
      this._startTime = value;
    }
  }

  public int ExecuteTime
  {
    get
    {
      return this._executeTime;
    }
    set
    {
      this._executeTime = value;
    }
  }

  public bool Success
  {
    get
    {
      return this._success;
    }
    set
    {
      this._success = value;
    }
  }
}
