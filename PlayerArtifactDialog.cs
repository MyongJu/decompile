﻿// Decompiled with JetBrains decompiler
// Type: PlayerArtifactDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class PlayerArtifactDialog : UI.Dialog
{
  private List<ArtifactItem> _allArtifactItem = new List<ArtifactItem>();
  private List<HeroItemBenefitRenderer> _allBenefitRenderer = new List<HeroItemBenefitRenderer>();
  [SerializeField]
  private ArtifactItem _artifactItemTemplate;
  [SerializeField]
  private UIGrid _artifactItemContainer;
  [SerializeField]
  private UIScrollView _allArtifactItemScrollView;
  [SerializeField]
  private UIButton _buttonUse;
  [SerializeField]
  private UIButton _buttonReplace;
  [SerializeField]
  private UIButton _buttonRemove;
  [SerializeField]
  private GameObject _rootTagNoArtifact;
  [SerializeField]
  private ArtifactItem _currentSelectArtifactItem;
  [SerializeField]
  private GameObject _rootTagNoSelectedArtifact;
  [SerializeField]
  private GameObject _rootLeftProtectedTime;
  [SerializeField]
  private UILabel _labelLeftProtectTime;
  [SerializeField]
  private GameObject _rootArtifactEffect;
  [SerializeField]
  private UILabel _labelArtifactEffect;
  [SerializeField]
  private HeroItemBenefitRenderer _benefitRendererTemplate;
  [SerializeField]
  private UITable _benefitContainer;
  [SerializeField]
  private UIScrollView _currentArtifactScrollView;
  [SerializeField]
  private GameObject _rootArtifactDetail;
  [SerializeField]
  private UILabel _labelLeftCdTime;
  private ArtifactData _currentSelect;
  protected bool _needUpdateLeftProtectTime;
  protected bool _needUpdateLeftCooldownTime;
  private PlayerArtifactDialog.Parameter _parameter;

  protected ArtifactData ArtifactOnSlot
  {
    get
    {
      return PlayerData.inst.heroData.GetArtifactBySlotIndex(this._parameter.ArtifactSlotIndex);
    }
  }

  protected bool HaveArtifactOnSlot
  {
    get
    {
      return this.ArtifactOnSlot != null;
    }
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._parameter = orgParam as PlayerArtifactDialog.Parameter;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.SecondTick);
    this._artifactItemTemplate.gameObject.SetActive(false);
    this._benefitRendererTemplate.gameObject.SetActive(false);
    this._currentSelect = this.ArtifactOnSlot;
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.SecondTick);
  }

  protected void UpdateUI()
  {
    this.UpdateArtifactList();
    this.UpdateCurrentSelectArtifact();
  }

  protected void UpdateArtifactList()
  {
    this.DestoryAllArtifactItem();
    CityData playerCityData = PlayerData.inst.playerCityData;
    if (playerCityData == null)
      return;
    using (List<int>.Enumerator enumerator = playerCityData.Artifacts.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        int current = enumerator.Current;
        ArtifactData artifactData = DBManager.inst.DB_Artifact.Get(PlayerData.inst.userData.world_id, current);
        if (artifactData != null)
        {
          ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(current);
          if (artifactInfo != null && artifactInfo.position == this._parameter.ArtifactSlotIndex)
          {
            ArtifactItem artifactItem = this.CreateArtifactItem();
            artifactItem.SetArtifactData(artifactData);
            artifactItem.SetHighlight(this._currentSelect == artifactData);
            artifactItem.OnClicked += new System.Action<ArtifactData>(this.OnArtifactClicked);
          }
        }
      }
    }
    this.StartCoroutine(this.RepositionArtifactItemContainerCoroutine());
    this._rootTagNoArtifact.SetActive(playerCityData.Artifacts.Count <= 0);
  }

  protected void SecondTick(int delta)
  {
    this.UpdateLeftProtectTime(delta);
    this.UpdateLeftCooldownTime(delta);
  }

  protected void UpdateLeftCooldownTime(int delta)
  {
    if (!this._needUpdateLeftCooldownTime || this._currentSelect == null)
      return;
    int cooldownLeftTime = this._currentSelect.CooldownLeftTime;
    if (cooldownLeftTime > 0)
      this._labelLeftCdTime.text = Utils.XLAT("id_cooldown") + Utils.FormatTime(cooldownLeftTime, false, false, true);
    else
      this.UpdateCurrentSelectArtifact();
  }

  protected void UpdateLeftProtectTime(int delta)
  {
    if (!this._needUpdateLeftProtectTime || this._currentSelect == null)
      return;
    int protectedLeftTime = this._currentSelect.ProtectedLeftTime;
    if (protectedLeftTime > 0)
      this._labelLeftProtectTime.text = Utils.FormatTime(protectedLeftTime, false, false, true);
    else
      this.UpdateCurrentSelectArtifact();
  }

  protected void UpdateCurrentSelectArtifact()
  {
    this._needUpdateLeftProtectTime = false;
    this._needUpdateLeftCooldownTime = false;
    ArtifactData artifactOnSlot = this.ArtifactOnSlot;
    this._buttonUse.gameObject.SetActive(false);
    this._buttonReplace.gameObject.SetActive(false);
    this._buttonRemove.gameObject.SetActive(false);
    if (artifactOnSlot == null && this._currentSelect != null)
      this._buttonUse.gameObject.SetActive(true);
    if (artifactOnSlot != null && this._currentSelect != null && this._currentSelect != artifactOnSlot)
      this._buttonReplace.gameObject.SetActive(true);
    if (artifactOnSlot != null && this._currentSelect == artifactOnSlot)
      this._buttonRemove.gameObject.SetActive(true);
    this._rootTagNoSelectedArtifact.SetActive(this._currentSelect == null);
    if (this._currentSelect == null)
    {
      this._rootArtifactDetail.SetActive(false);
    }
    else
    {
      ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this._currentSelect.ArtifactId);
      if (artifactInfo != null)
      {
        this._rootArtifactDetail.SetActive(true);
        this._currentSelectArtifactItem.SetArtifactData(this._currentSelect);
        this._currentSelectArtifactItem.SetHighlight(true);
        int protectedLeftTime = this._currentSelect.ProtectedLeftTime;
        if (protectedLeftTime < 0)
        {
          this._rootLeftProtectedTime.SetActive(false);
        }
        else
        {
          this._needUpdateLeftProtectTime = true;
          this._rootLeftProtectedTime.SetActive(true);
          this._labelLeftProtectTime.text = Utils.FormatTime(protectedLeftTime, false, false, true);
        }
        if (string.IsNullOrEmpty(artifactInfo.description))
        {
          this._rootArtifactEffect.gameObject.SetActive(false);
        }
        else
        {
          this._rootArtifactEffect.gameObject.SetActive(true);
          this._labelArtifactEffect.text = artifactInfo.Description;
        }
        int cooldownLeftTime = this._currentSelect.CooldownLeftTime;
        if (cooldownLeftTime < 0)
        {
          this._labelLeftCdTime.gameObject.SetActive(false);
        }
        else
        {
          this._needUpdateLeftCooldownTime = true;
          this._labelLeftCdTime.gameObject.SetActive(true);
          this._labelLeftCdTime.text = Utils.XLAT("id_cooldown") + Utils.FormatTime(cooldownLeftTime, false, false, true);
        }
        this.UpdateBenefitList(artifactInfo);
      }
    }
    this.StartCoroutine(this.RepositionBenefitContainerCoroutine());
  }

  protected void UpdateBenefitList(ArtifactInfo artifactInfo)
  {
    this.DestoryAllBenefitRenderer();
    if (artifactInfo == null)
      return;
    List<Benefits.BenefitValuePair> benefitsPairs = artifactInfo.benefits.GetBenefitsPairs();
    for (int index = 0; index < benefitsPairs.Count; ++index)
    {
      Benefits.BenefitValuePair benefitValuePair = benefitsPairs[index];
      PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitValuePair.internalID];
      this.CreateBenefitRenderer().SetData(dbProperty.Name, dbProperty.ConvertToDisplayString((double) benefitValuePair.value, true, true), dbProperty.ImagePath);
    }
  }

  protected void DestoryAllArtifactItem()
  {
    using (List<ArtifactItem>.Enumerator enumerator = this._allArtifactItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this._allArtifactItem.Clear();
  }

  protected ArtifactItem CreateArtifactItem()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._artifactItemTemplate.gameObject);
    gameObject.SetActive(true);
    ArtifactItem component = gameObject.GetComponent<ArtifactItem>();
    component.transform.SetParent(this._artifactItemContainer.transform);
    component.transform.localScale = Vector3.one;
    component.transform.localPosition = Vector3.zero;
    this._allArtifactItem.Add(component);
    return component;
  }

  protected ArtifactItem GetArtifactItemById(int artifactId)
  {
    return this._allArtifactItem.Find((Predicate<ArtifactItem>) (p =>
    {
      if (p.ArtifactData != null)
        return p.ArtifactData.ArtifactId == artifactId;
      return false;
    }));
  }

  protected void OnArtifactClicked(ArtifactData artifactData)
  {
    if (this._currentSelect != null)
    {
      ArtifactItem artifactItemById = this.GetArtifactItemById(this._currentSelect.ArtifactId);
      if ((bool) ((UnityEngine.Object) artifactItemById))
        artifactItemById.SetHighlight(false);
    }
    this._currentSelect = artifactData;
    if (this._currentSelect != null)
    {
      ArtifactItem artifactItemById = this.GetArtifactItemById(this._currentSelect.ArtifactId);
      if ((bool) ((UnityEngine.Object) artifactItemById))
        artifactItemById.SetHighlight(true);
    }
    this.UpdateCurrentSelectArtifact();
  }

  protected HeroItemBenefitRenderer CreateBenefitRenderer()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._benefitRendererTemplate.gameObject);
    HeroItemBenefitRenderer component = gameObject.GetComponent<HeroItemBenefitRenderer>();
    component.transform.SetParent(this._benefitContainer.transform);
    component.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    this._allBenefitRenderer.Add(component);
    return component;
  }

  protected void DestoryAllBenefitRenderer()
  {
    using (List<HeroItemBenefitRenderer>.Enumerator enumerator = this._allBenefitRenderer.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this._allBenefitRenderer.Clear();
  }

  public void OnButtonGotoArtifactRankClicked()
  {
    RequestManager.inst.SendRequest("Artifact:loadArtifact", new Hashtable()
    {
      {
        (object) "world_id",
        (object) PlayerData.inst.userData.world_id
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("Artifact/ArtifactDlg", (UI.Dialog.DialogParameter) new ArtifactDlg.Parameter()
      {
        artifactData = (data as ArrayList)
      }, true, true, true);
    }), true);
  }

  public void OnButtonUseClicked()
  {
    if (this._currentSelect == null)
      return;
    Hashtable postData = new Hashtable()
    {
      {
        (object) "world_id",
        (object) PlayerData.inst.CityData.Location.K
      },
      {
        (object) "artifact_id",
        (object) this._currentSelect.ArtifactId
      },
      {
        (object) "uid",
        (object) PlayerData.inst.cityId
      }
    };
    RequestManager.inst.SendRequest("Artifact:equip", postData, new System.Action<bool, object>(this.OnRequestEquipArtifactCallback), true);
  }

  public void OnButtonReplaceClicked()
  {
    if (this._currentSelect == null || this.ArtifactOnSlot == null)
      return;
    Hashtable postData = new Hashtable()
    {
      {
        (object) "world_id",
        (object) PlayerData.inst.CityData.Location.K
      },
      {
        (object) "old_artifact_id",
        (object) this.ArtifactOnSlot.ArtifactId
      },
      {
        (object) "new_artifact_id",
        (object) this._currentSelect.ArtifactId
      },
      {
        (object) "uid",
        (object) PlayerData.inst.cityId
      }
    };
    RequestManager.inst.SendRequest("Artifact:replaceArtifact", postData, new System.Action<bool, object>(this.OnRequestEquipArtifactCallback), true);
  }

  public void OnRequestEquipArtifactCallback(bool result, object data)
  {
    if (!result)
      return;
    this.UpdateUI();
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_artifact_equip_success", true), (System.Action) null, 4f, true);
  }

  public void OnButtonRemoveClicked()
  {
    Hashtable postData = new Hashtable()
    {
      {
        (object) "world_id",
        (object) PlayerData.inst.CityData.Location.K
      },
      {
        (object) "artifact_id",
        (object) this._currentSelect.ArtifactId
      },
      {
        (object) "uid",
        (object) PlayerData.inst.cityId
      }
    };
    RequestManager.inst.SendRequest("Artifact:unequip", postData, new System.Action<bool, object>(this.OnRequestUnEquipArtifactCallback), true);
  }

  public void OnRequestUnEquipArtifactCallback(bool result, object data)
  {
    if (!result)
      return;
    this.UpdateUI();
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_artifact_remove_success", true), (System.Action) null, 4f, true);
  }

  [DebuggerHidden]
  private IEnumerator RepositionBenefitContainerCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PlayerArtifactDialog.\u003CRepositionBenefitContainerCoroutine\u003Ec__Iterator6F()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator RepositionArtifactItemContainerCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PlayerArtifactDialog.\u003CRepositionArtifactItemContainerCoroutine\u003Ec__Iterator70()
    {
      \u003C\u003Ef__this = this
    };
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int ArtifactSlotIndex;
  }
}
