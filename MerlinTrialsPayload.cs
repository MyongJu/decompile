﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;

public class MerlinTrialsPayload
{
  private static MerlinTrialsPayload _instance;

  public static MerlinTrialsPayload Instance
  {
    get
    {
      if (MerlinTrialsPayload._instance == null)
        MerlinTrialsPayload._instance = new MerlinTrialsPayload();
      return MerlinTrialsPayload._instance;
    }
  }

  public string MerlinCoinName
  {
    get
    {
      return Utils.XLAT("merlin_coin_name");
    }
  }

  public string MerlinCoinDescription
  {
    get
    {
      return Utils.XLAT("merlin_coin_description");
    }
  }

  public string MerlinCoinImagePath
  {
    get
    {
      return "Texture/ItemIcons/item_melin_coin";
    }
  }

  public string GetCategoryContent(string category)
  {
    string key = category;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (MerlinTrialsPayload.\u003C\u003Ef__switch\u0024map8B == null)
      {
        // ISSUE: reference to a compiler-generated field
        MerlinTrialsPayload.\u003C\u003Ef__switch\u0024map8B = new Dictionary<string, int>(5)
        {
          {
            "all",
            0
          },
          {
            "hero",
            1
          },
          {
            "lord",
            2
          },
          {
            "dragon",
            3
          },
          {
            "speedup",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (MerlinTrialsPayload.\u003C\u003Ef__switch\u0024map8B.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return Utils.XLAT("item_uppercase_all_title");
          case 1:
            return Utils.XLAT("id_heroes");
          case 2:
            return Utils.XLAT("help_lord_title");
          case 3:
            return Utils.XLAT("id_uppercase_dragon");
          case 4:
            return Utils.XLAT("id_speedups");
        }
      }
    }
    return "--";
  }

  public void Initialize()
  {
    MessageHub.inst.GetPortByAction("merlin_pay_success").AddEvent(new System.Action<object>(this.BuyPackageSuccessPush));
    MerlinTrialsHelper.inst.RequestMerlinStatus((System.Action) null);
  }

  public void Dispose()
  {
    MessageHub.inst.GetPortByAction("merlin_pay_success").RemoveEvent(new System.Action<object>(this.BuyPackageSuccessPush));
  }

  private void BuyPackageSuccessPush(object data)
  {
    Hashtable hashtable = data as Hashtable;
    if (hashtable == null || !hashtable.ContainsKey((object) "package_id"))
      return;
    int result = 0;
    int.TryParse(hashtable[(object) "package_id"].ToString(), out result);
    MerlinTrialsShopSpecificInfo shopSpecificInfo = ConfigManager.inst.DB_MerlinTrialsShopSpecific.Get(result);
    if (shopSpecificInfo == null)
      return;
    AccountManager.Instance.ShowBindPop();
    UIManager.inst.OpenPopup("MerlinTrials/MerlinBuySuccessPopup", (Popup.PopupParameter) new MerlinBuySuccessPopup.Parameter()
    {
      shopInfo = shopSpecificInfo
    });
  }

  private void GetLeaderBoard(int type, int groupId = 0, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("Merlin:getLeaderBoard").SendRequest(new Hashtable()
    {
      {
        (object) nameof (groupId),
        type != 0 ? (object) "total" : (object) groupId.ToString()
      }
    }, callback, true);
  }

  public void ExchangeMerlinReward(MerlinTrialsShopInfo shopInfo, int amount, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("Merlin:buy").SendRequest(new Hashtable()
    {
      {
        (object) "shopId",
        (object) shopInfo.internalId
      },
      {
        (object) nameof (amount),
        (object) amount
      }
    }, callback, true);
  }

  public void GetWarLogs(System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("Merlin:getLogs").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, callback, true);
  }

  public void GetWarLog(long logId, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("Merlin:getOneLog").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) nameof (logId),
        (object) logId
      }
    }, callback, true);
  }

  public class RankData
  {
    public long uid;
    public long worldId;
    public string name;
    public string portrait;
    public string icon;
    public int lordTitleId;
    public long power;
    public long number;
    public long lostedNumber;
    public int rank;
    public string allianceTag;
    public string allianceName;
    public string allianceSymbolCode;

    public RankData(Hashtable data)
    {
      if (data == null)
        return;
      DatabaseTools.UpdateData(data, nameof (uid), ref this.uid);
      DatabaseTools.UpdateData(data, "world_id", ref this.worldId);
      DatabaseTools.UpdateData(data, nameof (name), ref this.name);
      DatabaseTools.UpdateData(data, nameof (portrait), ref this.portrait);
      DatabaseTools.UpdateData(data, nameof (icon), ref this.icon);
      DatabaseTools.UpdateData(data, "lord_title", ref this.lordTitleId);
      DatabaseTools.UpdateData(data, nameof (power), ref this.power);
      DatabaseTools.UpdateData(data, "symbol_code", ref this.allianceSymbolCode);
      DatabaseTools.UpdateData(data, "tag", ref this.allianceTag);
      DatabaseTools.UpdateData(data, "alliance_name", ref this.allianceName);
      DatabaseTools.UpdateData(data, nameof (rank), ref this.rank);
      DatabaseTools.UpdateData(data, nameof (number), ref this.number);
      DatabaseTools.UpdateData(data, "died", ref this.lostedNumber);
      this.ParseOther(data);
    }

    public RankData(int rank)
    {
      this.rank = rank;
      this.name = string.Format("PlayerName#{0}", (object) rank);
    }

    protected virtual void ParseOther(Hashtable data)
    {
    }

    public string FullName
    {
      get
      {
        if (string.IsNullOrEmpty(this.allianceTag))
          return string.Format("{0}.K{1}", (object) this.name, (object) this.worldId);
        return string.Format("({0}){1}.K{2}", (object) this.allianceTag, (object) this.name, (object) this.worldId);
      }
    }

    public string Name
    {
      get
      {
        if (string.IsNullOrEmpty(this.allianceTag))
          return this.name;
        return string.Format("[{0}]{1}", (object) this.allianceTag, (object) this.name);
      }
    }

    public string OriginName
    {
      get
      {
        return this.name;
      }
    }
  }

  public class HistoryData
  {
    public long logId;
    public int time;
    public int mainId;
    public bool winOrLost;

    public HistoryData(Hashtable data)
    {
      if (data == null)
        return;
      DatabaseTools.UpdateData(data, nameof (logId), ref this.logId);
      DatabaseTools.UpdateData(data, "now", ref this.time);
      DatabaseTools.UpdateData(data, nameof (mainId), ref this.mainId);
      DatabaseTools.UpdateData(data, nameof (winOrLost), ref this.winOrLost);
    }

    public HistoryData(int logId)
    {
      this.logId = (long) logId;
      this.winOrLost = logId % 2 == 0;
    }

    public bool WinOrLost
    {
      get
      {
        return this.winOrLost;
      }
    }
  }
}
