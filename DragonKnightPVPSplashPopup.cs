﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightPVPSplashPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class DragonKnightPVPSplashPopup : Popup
{
  public DragonKnightUserPanel m_LeftUser;
  public DragonKnightUserPanel m_RightUser;
  public DragonKnightPanel m_LeftDK;
  public DragonKnightPanel m_RightDK;
  public UIButton SkipButton;
  public DragonKnightSpeedUpButton SpeedButton;
  private bool m_Skip;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    AudioManager.Instance.StopAndPlaySound("sfx_dragon_knight_battle_boss");
    DragonKnightPVPSplashPopup.Parameter parameter = orgParam as DragonKnightPVPSplashPopup.Parameter;
    UserDKArenaData currentData = DBManager.inst.DB_DKArenaDB.GetCurrentData();
    this.m_LeftUser.SetData(PlayerData.inst.userData);
    this.m_LeftDK.SetData(PlayerData.inst.dragonKnightData, true, currentData == null ? 1000L : currentData.score);
    this.m_RightUser.SetData(parameter.target);
    this.m_RightDK.SetData(parameter.targetDK, true, parameter.targetArena == null ? 1000L : parameter.targetArena.score);
    VIP_Level_Info vipLevelInfo = PlayerData.inst.hostPlayer.VIPLevelInfo;
    bool vipActive = PlayerData.inst.hostPlayer.VIPActive;
    DragonKnightSystem.Instance.Controller.BattleHud.HideButtons();
    this.SpeedButton.gameObject.SetActive(vipActive && (vipLevelInfo.ContainsPrivilege("dk_dungeon_speedup_2") || vipLevelInfo.ContainsPrivilege("dk_dungeon_speedup_4")));
    this.SkipButton.gameObject.SetActive(vipActive && vipLevelInfo.ContainsPrivilege("dk_dungeon_skip"));
    this.SpeedButton.Restore = false;
    this.SpeedButton.callback = (System.Action) (() => DragonKnightSystem.Instance.Controller.BattleHud.SpeedButton.Reset());
  }

  public void OnSkip()
  {
    Time.timeScale = 1f;
    this.m_Skip = true;
    BattleManager.Instance.AutoAttack();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    DragonKnightSystem.Instance.Controller.BattleHud.ShowButtons();
    if (this.m_Skip)
      return;
    BattleManager.Instance.Start();
  }

  public void OnStartGame()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnTutorialFinished()
  {
    BattleManager.Instance.Start();
    TutorialManager.Instance.onTutotialFinished -= new System.Action(this.OnTutorialFinished);
  }

  public class Parameter : Popup.PopupParameter
  {
    public UserData target;
    public DragonKnightData targetDK;
    public UserDKArenaData targetArena;
  }
}
