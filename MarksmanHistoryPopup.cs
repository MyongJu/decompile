﻿// Decompiled with JetBrains decompiler
// Type: MarksmanHistoryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class MarksmanHistoryPopup : Popup
{
  private Dictionary<int, MarksmanRewardItemRenderer> _itemDict = new Dictionary<int, MarksmanRewardItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public UIScrollView scrollView;
  public UIGrid grid;
  public GameObject itemPrefab;
  public GameObject itemRoot;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnCloseButtonClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.ClearData();
    List<MarksmanData.RewardData> rewardDataList = MarksmanPayload.Instance.TempMarksmanData.RewardDataList;
    if (rewardDataList != null)
    {
      for (int key = 0; key < rewardDataList.Count; ++key)
      {
        MarksmanRewardItemRenderer itemRenderer = this.CreateItemRenderer(rewardDataList[key], MarksmanData.RewardType.RECORD);
        this._itemDict.Add(key, itemRenderer);
      }
    }
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, MarksmanRewardItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.grid.repositionNow = true;
    this.grid.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private MarksmanRewardItemRenderer CreateItemRenderer(MarksmanData.RewardData rewardData, MarksmanData.RewardType rewardType)
  {
    GameObject gameObject = this._itemPool.AddChild(this.grid.gameObject);
    gameObject.SetActive(true);
    MarksmanRewardItemRenderer component = gameObject.GetComponent<MarksmanRewardItemRenderer>();
    component.SetData(rewardData, rewardType);
    return component;
  }
}
