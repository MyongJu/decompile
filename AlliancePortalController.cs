﻿// Decompiled with JetBrains decompiler
// Type: AlliancePortalController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class AlliancePortalController : MonoBehaviour, IConstructionController
{
  public ParticleSortingOrderModifier m_VFX;
  public GameObject m_Scaffold;
  public GameObject m_Workers;
  public GameObject m_Dust;

  public void Reset()
  {
    this.m_VFX.gameObject.SetActive(false);
    this.m_Scaffold.SetActive(false);
    this.m_Workers.SetActive(false);
    this.m_Dust.SetActive(false);
  }

  public void UpdateUI(TileData tile)
  {
    AlliancePortalData portalData = tile.PortalData;
    if (portalData == null)
      return;
    AlliancePortalData.BossSummonState portalBossSummonState = AllianceBuildUtils.GetAlliancePortalBossSummonState();
    this.m_VFX.gameObject.SetActive((portalBossSummonState == AlliancePortalData.BossSummonState.ACTIVATED || portalBossSummonState == AlliancePortalData.BossSummonState.ACTIVATED_TO_RALLY) && portalData.AllianceId == PlayerData.inst.allianceId);
    this.UpdateScaffold(portalData);
    this.UpdateWorkers(portalData);
    this.UpdateDust(portalData);
  }

  public void SetSortingLayerID(int sortingLayerID)
  {
    this.m_VFX.SortingLayerName = SortingLayer.IDToName(sortingLayerID);
  }

  private void UpdateScaffold(AlliancePortalData hospitalData)
  {
    bool flag = false;
    if (hospitalData.CurrentState == AlliancePortalData.State.BUILDING)
      flag = true;
    this.m_Scaffold.SetActive(flag);
  }

  private void UpdateWorkers(AlliancePortalData hospitalData)
  {
    bool flag = false;
    if (hospitalData.CurrentState == AlliancePortalData.State.BUILDING)
      flag = true;
    this.m_Workers.SetActive(flag);
  }

  private void UpdateDust(AlliancePortalData hospitalData)
  {
    bool flag = false;
    if (hospitalData.CurrentState == AlliancePortalData.State.BUILDING)
      flag = true;
    this.m_Dust.SetActive(flag);
  }
}
