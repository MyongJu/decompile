﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerHeroParliament
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingControllerHeroParliament : BuildingControllerNew
{
  [SerializeField]
  private GameObject _freeSummon;
  [SerializeField]
  private GameObject _canParliamentAssign;
  [SerializeField]
  private GameObject _canHeroInventoryAvailable;
  private GameConfigInfo _gameConfigInfo;

  public override void AddActionButton(ref List<CityCircleBtnPara> ccbp)
  {
    CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_hero_summmon", "id_hero_recruit", new EventDelegate((EventDelegate.Callback) (() => this.OnHeroRecruitClicked())), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ccbp.Add(cityCircleBtnPara1);
    CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_hero_roster", "id_hero_inventory", new EventDelegate((EventDelegate.Callback) (() => this.ShowHeroInventory(false))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ccbp.Add(cityCircleBtnPara2);
    CityCircleBtnPara cityCircleBtnPara3 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_hero_council", "id_hero_parliament", new EventDelegate((EventDelegate.Callback) (() => this.OnHeroParliamentClicked())), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ccbp.Add(cityCircleBtnPara3);
    CityTouchCircle.Instance.Title = Utils.XLAT("hero_council_name");
  }

  public override void InitBuilding()
  {
    base.InitBuilding();
    this.onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    DBManager.inst.DB_LegendCard.onDataChanged += new System.Action<LegendCardData>(this.OnLegendCardDataChanged);
    DBManager.inst.DB_LegendTemple.onDataChanged += new System.Action<LegendTempleData>(this.OnLegendTempleDataChanged);
    DBManager.inst.DB_City.onDataChanged += new System.Action<long>(this.OnCityDataChanged);
    this.ShowBuildingTipInfo();
  }

  public override void Dispose()
  {
    base.Dispose();
    this.onBuildingSelected -= new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    DBManager.inst.DB_LegendCard.onDataChanged -= new System.Action<LegendCardData>(this.OnLegendCardDataChanged);
    DBManager.inst.DB_LegendTemple.onDataChanged -= new System.Action<LegendTempleData>(this.OnLegendTempleDataChanged);
    DBManager.inst.DB_City.onDataChanged -= new System.Action<long>(this.OnCityDataChanged);
    this._gameConfigInfo = (GameConfigInfo) null;
  }

  public void OnHeroInventoryClicked()
  {
    this.ShowHeroInventory(true);
  }

  public void OnHeroParliamentClicked()
  {
    HeroCardUtils.LoadHeroCardData((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("HeroCard/HeroParliamentDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    }));
  }

  public void OnHeroRecruitClicked()
  {
    MessageHub.inst.GetPortByAction("Legend:getTempleInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("HeroCard/HeroRecruitDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    }), true);
  }

  private void ShowHeroInventory(bool topShowSummonableHero = false)
  {
    HeroCardUtils.LoadHeroCardData((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("HeroCard/HeroInventoryDlg", (UI.Dialog.DialogParameter) new HeroInventoryDlg.Parameter()
      {
        topShowSummonableHero = topShowSummonableHero
      }, 1 != 0, 1 != 0, 1 != 0);
    }));
  }

  private void OnLegendCardDataChanged(LegendCardData legendCardData)
  {
    if (legendCardData.Uid != PlayerData.inst.uid)
      return;
    this.ShowBuildingTipInfo();
  }

  private void OnLegendTempleDataChanged(LegendTempleData legendTempleData)
  {
    if (legendTempleData.Uid != PlayerData.inst.uid)
      return;
    this.ShowBuildingTipInfo();
  }

  private void OnCityDataChanged(long cityId)
  {
    if ((long) PlayerData.inst.cityId != cityId)
      return;
    this.ShowBuildingTipInfo();
  }

  private void ShowBuildingTipInfo()
  {
    if (this._gameConfigInfo == null)
      this._gameConfigInfo = ConfigManager.inst.DB_GameConfig.GetData("parliament_unlock_level");
    if (this._gameConfigInfo != null && this._gameConfigInfo.ValueInt > PlayerData.inst.CityData.mStronghold.mLevel)
    {
      NGUITools.SetActive(this._freeSummon, false);
      NGUITools.SetActive(this._canParliamentAssign, false);
      NGUITools.SetActive(this._canHeroInventoryAvailable, false);
    }
    else if (HeroCardUtils.HasFreeSummon())
    {
      NGUITools.SetActive(this._freeSummon, true);
      NGUITools.SetActive(this._canParliamentAssign, false);
      NGUITools.SetActive(this._canHeroInventoryAvailable, false);
    }
    else if (HeroCardUtils.HasParliamentPositionAssign())
    {
      NGUITools.SetActive(this._freeSummon, false);
      NGUITools.SetActive(this._canParliamentAssign, true);
      NGUITools.SetActive(this._canHeroInventoryAvailable, false);
    }
    else if (HeroCardUtils.HasHeroInventoryAvailable())
    {
      NGUITools.SetActive(this._freeSummon, false);
      NGUITools.SetActive(this._canParliamentAssign, false);
      NGUITools.SetActive(this._canHeroInventoryAvailable, true);
    }
    else
    {
      NGUITools.SetActive(this._freeSummon, false);
      NGUITools.SetActive(this._canParliamentAssign, false);
      NGUITools.SetActive(this._canHeroInventoryAvailable, false);
    }
  }

  private void OnBuildingSelected()
  {
    AudioManager.Instance.PlaySound("sfx_city_click_hero_council", false);
    if (this._gameConfigInfo == null)
      this._gameConfigInfo = ConfigManager.inst.DB_GameConfig.GetData("parliament_unlock_level");
    if (this._gameConfigInfo != null && this._gameConfigInfo.ValueInt > PlayerData.inst.CityData.mStronghold.mLevel)
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_hero_council_unlock_level", new Dictionary<string, string>()
      {
        {
          "0",
          this._gameConfigInfo.ValueInt.ToString()
        }
      }, true), (System.Action) null, 4f, true);
    else
      CitadelSystem.inst.OpenTouchCircle();
  }
}
