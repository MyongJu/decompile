﻿// Decompiled with JetBrains decompiler
// Type: LegendSkillInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class LegendSkillInfo
{
  private const string IMAGE_FOLDER = "Texture/LegendSkill/Icon/";
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "type")]
  public string type;
  [Config(Name = "trigger")]
  public int trigger;
  [Config(Name = "general_level")]
  public int generalLevel;
  [Config(Name = "power")]
  public int power;
  [Config(Name = "value")]
  public float value;
  [Config(CustomParse = true, CustomType = typeof (Costs), Name = "Costs")]
  public Costs cost;
  [Config(Name = "skill_points")]
  public int skillPoints;
  [Config(Name = "image")]
  public string image;
  [Config(CustomParse = true, CustomType = typeof (Requirements), Name = "Requirements")]
  public Requirements require;
  [Config(Name = "loc_id")]
  public string Loc_ID_Key;
  [Config(Name = "loc_description")]
  public string Loc_DESC_Key;
  [Config(Name = "loc_ effect")]
  public string Loc_EFFECT_Key;
  [Config(Name = "display_id")]
  public int Display_Effect_InternalID;
  [Config(Name = "level")]
  public int Level;
  [Config(Name = "performance")]
  public int Performance;
  [Config(CustomParse = true, Name = "Benefits", ParseFuncKey = "ParseBenfits")]
  public Dictionary<int, float> Benefits;
  [Config(CustomParse = true, Name = "OppBenefits", ParseFuncKey = "ParseBenfits")]
  public Dictionary<int, float> OppBenefits;
  public string Skill_Desc;

  public string Image
  {
    get
    {
      return string.Format("{0}{1}", (object) "Texture/LegendSkill/Icon/", (object) this.image);
    }
  }

  public string Loc_Desc
  {
    get
    {
      return ScriptLocalization.Get(this.Loc_DESC_Key, true);
    }
  }

  public string Loc_Effect
  {
    get
    {
      return ScriptLocalization.Get(this.Loc_EFFECT_Key, true);
    }
  }

  public string Loc_Name
  {
    get
    {
      return ScriptLocalization.Get(this.Loc_ID_Key, true);
    }
  }
}
