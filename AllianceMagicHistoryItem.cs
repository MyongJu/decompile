﻿// Decompiled with JetBrains decompiler
// Type: AllianceMagicHistoryItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceMagicHistoryItem : MonoBehaviour
{
  public UISprite normalBg;
  public UISprite defenseBg;
  public UILabel content;
  private bool isInit;
  private int initSize;
  public System.Action<AllianceMagicHistoryItem> InitCallBack;

  public void SetData(IHistoryData data)
  {
    this.content.text = data.Content;
    NGUITools.SetActive(this.normalBg.gameObject, data.IsAlly);
    NGUITools.SetActive(this.defenseBg.gameObject, !data.IsAlly);
    this.initSize = !this.normalBg.gameObject.activeSelf ? this.defenseBg.height : this.normalBg.height;
  }

  private void Update()
  {
    if (this.isInit)
      return;
    int initSize = this.initSize;
    int num1 = !this.normalBg.gameObject.activeSelf ? this.defenseBg.height : this.normalBg.height;
    if (num1 == this.initSize)
    {
      if (this.InitCallBack != null)
        this.InitCallBack(this);
      this.isInit = true;
    }
    else
    {
      int num2 = -1;
      if (this.initSize > num1)
        num2 = 1;
      int num3 = Mathf.Abs(this.initSize - num1);
      this.GetComponent<UIWidget>().height = num1;
      BoxCollider component = this.GetComponent<BoxCollider>();
      Vector3 center = component.center;
      center.y = (float) (num2 * (num3 / 2));
      component.center = center;
      this.initSize = num1;
      if (this.InitCallBack != null)
        this.InitCallBack(this);
      this.isInit = true;
    }
  }
}
