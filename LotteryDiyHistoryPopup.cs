﻿// Decompiled with JetBrains decompiler
// Type: LotteryDiyHistoryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class LotteryDiyHistoryPopup : Popup
{
  private Dictionary<int, LotteryDiyItemRenderer> _itemDict = new Dictionary<int, LotteryDiyItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public GameObject itemTipsNode;
  public UILabel itemName;
  public UILabel itemDetail;
  public UIScrollView scrollView;
  public UIGrid grid;
  public GameObject itemPrefab;
  public GameObject itemRoot;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.ShowLotteryHistoryContent();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnItemTipsDisplay(LotteryDiyBaseData.RewardData rewardData, LotteryDiyPayload.LotteryArea lotteryArea, Vector3 itemPosition)
  {
    if (rewardData == null)
      return;
    this.StopCoroutine("HideItemTips");
    this.UpdateItemTipsPosition(lotteryArea, itemPosition);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardData.itemId);
    if (itemStaticInfo != null)
    {
      this.itemName.text = itemStaticInfo.LocName;
      this.itemDetail.text = itemStaticInfo.LocDescription;
      NGUITools.SetActive(this.itemTipsNode, true);
    }
    this.StartCoroutine("HideItemTips");
  }

  private void UpdateItemTipsPosition(LotteryDiyPayload.LotteryArea lotteryArea, Vector3 itemPosition)
  {
    float num1 = -500f;
    float num2 = 25f;
    this.itemTipsNode.transform.localPosition = new Vector3(itemPosition.x + num1, itemPosition.y + num2, itemPosition.z);
  }

  [DebuggerHidden]
  private IEnumerator HideItemTips()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LotteryDiyHistoryPopup.\u003CHideItemTips\u003Ec__Iterator79()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void ShowLotteryHistoryContent()
  {
    this.ClearData();
    Dictionary<int, LotteryDiyBaseData.RewardData> lotteryPrevItemDict = LotteryDiyPayload.Instance.LotteryData.LotteryPrevItemDict;
    if (lotteryPrevItemDict != null)
    {
      for (int key = 1; key <= lotteryPrevItemDict.Count; ++key)
      {
        if (lotteryPrevItemDict.ContainsKey(key))
        {
          LotteryDiyItemRenderer itemRenderer = this.CreateItemRenderer(lotteryPrevItemDict[key], LotteryDiyPayload.LotteryArea.RIGHT);
          this._itemDict.Add(key, itemRenderer);
        }
      }
    }
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, LotteryDiyItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, LotteryDiyItemRenderer> current = enumerator.Current;
        current.Value.onItemTipsDisplay -= new System.Action<LotteryDiyBaseData.RewardData, LotteryDiyPayload.LotteryArea, Vector3>(this.OnItemTipsDisplay);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.grid.repositionNow = true;
    this.grid.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private LotteryDiyItemRenderer CreateItemRenderer(LotteryDiyBaseData.RewardData rewardData, LotteryDiyPayload.LotteryArea lotteryArea)
  {
    GameObject gameObject = this._itemPool.AddChild(this.grid.gameObject);
    gameObject.SetActive(true);
    LotteryDiyItemRenderer component = gameObject.GetComponent<LotteryDiyItemRenderer>();
    component.SetData(rewardData, lotteryArea, -1, true);
    component.onItemTipsDisplay += new System.Action<LotteryDiyBaseData.RewardData, LotteryDiyPayload.LotteryArea, Vector3>(this.OnItemTipsDisplay);
    return component;
  }
}
