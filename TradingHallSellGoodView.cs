﻿// Decompiled with JetBrains decompiler
// Type: TradingHallSellGoodView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UnityEngine;

public class TradingHallSellGoodView : MonoBehaviour
{
  private Dictionary<string, string> _hintParam = new Dictionary<string, string>();
  private Dictionary<int, TradingHallSellGoodSlot> _sellItemDict = new Dictionary<int, TradingHallSellGoodSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UILabel _labelSellHint;
  [SerializeField]
  private GameObject _noGood2Show;
  [SerializeField]
  private UIScrollView _sellScrollView;
  [SerializeField]
  private UITable _sellTable;
  [SerializeField]
  private GameObject _sellItemPrefab;
  [SerializeField]
  private GameObject _sellItemRoot;

  public void UpdateUI(bool updateData = false, bool blockScreen = false)
  {
    this._labelSellHint.text = string.Empty;
    if (updateData)
    {
      this.ClearSellGoodData();
      TradingHallPayload.Instance.GetSellGoods(blockScreen, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!((UnityEngine.Object) this.gameObject != (UnityEngine.Object) null))
          return;
        this.ShowSellContent();
      }));
    }
    else
      this.ShowSellContent();
  }

  private void OnDisable()
  {
    TradingHallPayload.Instance.OnSellDataUpdated -= new System.Action(this.OnSellDataUpdated);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  private void ShowSellContent()
  {
    this.ClearSellGoodData();
    TradingHallPayload.Instance.OnSellDataUpdated -= new System.Action(this.OnSellDataUpdated);
    TradingHallPayload.Instance.OnSellDataUpdated += new System.Action(this.OnSellDataUpdated);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
    List<TradingHallData.SellGoodData> sellGoodDataList = TradingHallPayload.Instance.TradingData.SellGoodDataList;
    if (sellGoodDataList != null)
    {
      sellGoodDataList.Sort((Comparison<TradingHallData.SellGoodData>) ((x, y) =>
      {
        if (x.IsValid != y.IsValid)
          return x.IsValid.CompareTo(y.IsValid);
        return x.ValidTime.CompareTo(y.ValidTime);
      }));
      for (int index = 0; index < sellGoodDataList.Count; ++index)
      {
        TradingHallSellGoodSlot sellGoodSlot = this.GenerateSellGoodSlot(sellGoodDataList[index], index);
        this._sellItemDict.Add(index, sellGoodSlot);
      }
    }
    this.RepositionSellGood(true);
    this.UpdateTipContent();
  }

  private void UpdateTipContent()
  {
    this._hintParam.Clear();
    this._hintParam.Add("0", TradingHallPayload.Instance.CurrentShelfCount.ToString());
    this._hintParam.Add("1", TradingHallPayload.Instance.MaxShelfCount.ToString());
    this._labelSellHint.text = ScriptLocalization.GetWithPara("exchange_hall_sell_goods_currently_listed_amount", this._hintParam, true);
    NGUITools.SetActive(this._noGood2Show, this._sellItemDict == null || this._sellItemDict.Count <= 0);
  }

  public void ClearSellGoodData()
  {
    TradingHallPayload.Instance.OnSellDataUpdated -= new System.Action(this.OnSellDataUpdated);
    using (Dictionary<int, TradingHallSellGoodSlot>.Enumerator enumerator = this._sellItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, TradingHallSellGoodSlot> current = enumerator.Current;
        current.Value.ClearData();
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._sellItemDict.Clear();
    this._itemPool.Clear();
  }

  private void RepositionSellGood(bool resetScrollView = true)
  {
    this._sellTable.repositionNow = true;
    this._sellTable.Reposition();
    if (!resetScrollView)
      return;
    this._sellScrollView.ResetPosition();
    this._sellScrollView.gameObject.SetActive(false);
    this._sellScrollView.gameObject.SetActive(true);
  }

  private TradingHallSellGoodSlot GenerateSellGoodSlot(TradingHallData.SellGoodData sellGoodData, int sellIndex)
  {
    this._itemPool.Initialize(this._sellItemPrefab, this._sellItemRoot);
    GameObject go = this._itemPool.AddChild(this._sellTable.gameObject);
    NGUITools.SetActive(go, true);
    TradingHallSellGoodSlot component = go.GetComponent<TradingHallSellGoodSlot>();
    component.SetData(sellGoodData, sellIndex);
    return component;
  }

  private void OnSellDataUpdated()
  {
    if (!((UnityEngine.Object) this.gameObject != (UnityEngine.Object) null))
      return;
    this.UpdateUI(false, false);
  }

  private void OnSecondEvent(int time)
  {
    if (!((UnityEngine.Object) this.gameObject != (UnityEngine.Object) null))
      return;
    this.UpdateTipContent();
  }
}
