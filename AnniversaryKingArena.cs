﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryKingArena
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AnniversaryKingArena : MonoBehaviour
{
  public AnniversaryKingItem[] kings;
  private bool _isDesroy;
  public UILabel desc;
  private bool _isInit;

  private void Refresh()
  {
    if (!this._isInit)
      AnniversaryManager.Instance.FetchKingsData();
    else
      this.UpdateUI();
  }

  public void Show()
  {
    NGUITools.SetActive(this.gameObject, true);
    if (AnniversaryManager.Instance.KickOut)
    {
      AnniversaryManager.Instance.KickOut = false;
      AnniversaryManager.Instance.RemovePoint(1);
    }
    this.Refresh();
  }

  public void Hide()
  {
    NGUITools.SetActive(this.gameObject, false);
    this.RemoveEventHandler();
  }

  private void UpdateUI()
  {
    if (this._isDesroy)
      return;
    this._isInit = true;
    this.RefreshTime();
    this.RemoveEventHandler();
    this.AddEventHandler();
    for (int index = 0; index < this.kings.Length; ++index)
    {
      int num = index + 1;
      AnniversaryManager.AnniversarySlotData slotData = AnniversaryManager.Instance.GetSlotData(num);
      AnniversaryChampionMainInfo data = ConfigManager.inst.DB_AnniversaryChampionMain.GetData((index + 1).ToString());
      if (slotData != null && data != null)
        this.kings[index].SetData(slotData, data);
      else if (data != null)
        this.kings[index].SetData(data, num);
    }
  }

  public void OnShowWorkshipLeaderBorad()
  {
    UIManager.inst.OpenDlg("LeaderBoard/WorldLeaderBoardDetailDlg", (UI.Dialog.DialogParameter) new LeaderBoardDetailDlg.LeaderBoardDetailDlgParamer()
    {
      method = "anniversary:getLeaderboard",
      param = new Hashtable()
      {
        {
          (object) "name",
          (object) "worship"
        },
        {
          (object) "from",
          (object) 1
        },
        {
          (object) "to",
          (object) 100
        }
      },
      title = ScriptLocalization.Get("leaderboards_uppercase_worship_title", true),
      headerTitle = "leaderboards_uppercase_worship",
      tip = "leaderboards_uppercase_worship_self",
      isShowKingdom = true
    }, true, true, true);
  }

  public void OnShowChallengeLeaderBorad()
  {
    UIManager.inst.OpenDlg("LeaderBoard/WorldLeaderBoardDetailDlg", (UI.Dialog.DialogParameter) new LeaderBoardDetailDlg.LeaderBoardDetailDlgParamer()
    {
      method = "anniversary:getLeaderboard",
      param = new Hashtable()
      {
        {
          (object) "name",
          (object) "challenge"
        },
        {
          (object) "from",
          (object) 1
        },
        {
          (object) "to",
          (object) 100
        }
      },
      isShowKingdom = true,
      title = ScriptLocalization.Get("leaderboards_uppercase_challenge_title", true),
      headerTitle = "leaderboards_uppercase_challenge",
      tip = "leaderboards_uppercase_challenge_self"
    }, true, true, true);
  }

  private void OnDestroy()
  {
    this._isDesroy = true;
  }

  public void Dispose()
  {
    this.RemoveEventHandler();
  }

  private void OnDisable()
  {
    this.RemoveEventHandler();
  }

  private void OnEnable()
  {
    this.AddEventHandler();
  }

  private void RefreshTime()
  {
    if (!AnniversaryManager.Instance.IsStart())
    {
      this.desc.text = ScriptLocalization.Get("event_1_year_tournament_open_spot_description", true);
    }
    else
    {
      int time = AnniversaryManager.Instance.EndTime - NetServerTime.inst.ServerTimestamp;
      if (time <= 0)
        return;
      string str = Utils.FormatTime(time, true, true, true);
      Dictionary<string, string> para = new Dictionary<string, string>();
      int num1 = 30;
      int num2 = 60;
      GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("anniversary_champion_end_unchange_time");
      if (data1 != null)
        num1 = data1.ValueInt / 60;
      GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("anniversary_champion_end_delay_time");
      if (data2 != null)
        num2 = data2.ValueInt / 60;
      para.Add("1", str);
      para.Add("2", num1.ToString());
      para.Add("3", num2.ToString());
      this.desc.text = ScriptLocalization.GetWithPara("event_1_year_tournament_description", para, true);
    }
  }

  private void OnProcess(int time)
  {
    this.RefreshTime();
  }

  private void AddEventHandler()
  {
    AnniversaryManager.Instance.OnDataUpdate += new System.Action(this.UpdateUI);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnProcess);
  }

  private void RemoveEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnProcess);
    AnniversaryManager.Instance.OnDataUpdate -= new System.Action(this.UpdateUI);
    for (int index = 0; index < this.kings.Length; ++index)
      this.kings[index].Clear();
  }
}
