﻿// Decompiled with JetBrains decompiler
// Type: KingSkillType
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class KingSkillType
{
  public const string AfterWarTrainning = "after_war_training";
  public const string GetGold = "get_gold";
  public const string ForceTeleport = "force_teleport";
  public const string SendTroopForbidden = "send_troop_forbidden";
  public const string FindTarget = "find_target";
}
