﻿// Decompiled with JetBrains decompiler
// Type: ConfigActivityReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigActivityReward
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, ActivityRewardInfo> datas;
  private Dictionary<int, ActivityRewardInfo> dicByUniqueId;
  private Dictionary<int, ActivityRewardInfo> dicByMainInternalID;
  private Dictionary<string, ActivityRewardInfo> rewardInfoMap;

  public void BuildDB(object res)
  {
    this.parse.Parse<ActivityRewardInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public ActivityRewardInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (ActivityRewardInfo) null;
  }

  public ActivityRewardInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (ActivityRewardInfo) null;
  }

  public ActivityRewardInfo GetDataByMainInternalID(int internalID)
  {
    if (this.dicByMainInternalID == null)
    {
      using (Dictionary<int, ActivityRewardInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, ActivityRewardInfo> current = enumerator.Current;
          this.dicByMainInternalID.Add(current.Value.ActivityMainSubInfo_ID, current.Value);
        }
      }
    }
    return this.dicByMainInternalID[internalID];
  }

  public ActivityRewardInfo GetCurrentStepReward(int id, int lev)
  {
    ActivityMainSubInfo data = ConfigManager.inst.DB_ActivityMainSub.GetData(id);
    Dictionary<int, ActivityRewardInfo>.ValueCollection.Enumerator enumerator = this.dicByUniqueId.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && enumerator.Current.ActivityMainSubInfo_ID == data.internalId && enumerator.Current.StrongholdLev == lev)
        return enumerator.Current;
    }
    return (ActivityRewardInfo) null;
  }

  public ActivityRewardInfo GetDataByID(string id)
  {
    if (this.rewardInfoMap == null)
    {
      this.rewardInfoMap = new Dictionary<string, ActivityRewardInfo>();
      using (Dictionary<int, ActivityRewardInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, ActivityRewardInfo> current = enumerator.Current;
          this.rewardInfoMap.Add(current.Value.ID, current.Value);
        }
      }
    }
    return this.rewardInfoMap[id];
  }
}
