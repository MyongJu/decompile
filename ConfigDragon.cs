﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragon
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigDragon
{
  private ConfigParseEx<DragonInfo> _parse = new ConfigParseEx<DragonInfo>();
  private ConfigFieldCache<int> _levelCache = new ConfigFieldCache<int>();
  private Dictionary<int, DragonInfo> _dragonByLevel = new Dictionary<int, DragonInfo>();

  public void BuildDB(object res)
  {
    this._parse.Initialize(res as Hashtable);
    this._levelCache.Initialize((ConfigRawData) this._parse, "level");
  }

  public List<DragonInfo> GetDragonInfoList()
  {
    List<DragonInfo> dragonInfoList = new List<DragonInfo>();
    List<int> internalIds = this._parse.InternalIds;
    for (int index = 0; index < internalIds.Count; ++index)
    {
      DragonInfo dragonInfo = this.GetDragonInfo(internalIds[index]);
      dragonInfoList.Add(dragonInfo);
    }
    return dragonInfoList;
  }

  public DragonInfo GetDragonInfo(int internalId)
  {
    DragonInfo dragonInfo = this._parse.Get(internalId);
    if (dragonInfo != null)
      dragonInfo.internalId = internalId;
    return dragonInfo;
  }

  public DragonInfo GetDragonInfo(string id)
  {
    return this.GetDragonInfo(this._parse.s2i(id));
  }

  public DragonInfo GetDragonInfoByLevel(int level)
  {
    DragonInfo dragonInfo;
    if (!this._dragonByLevel.TryGetValue(level, out dragonInfo))
    {
      List<int> internalIds = this._parse.InternalIds;
      for (int index = 0; index < internalIds.Count; ++index)
      {
        int internalId = internalIds[index];
        if (this._levelCache.Get(internalId, 0) == level)
        {
          dragonInfo = this.GetDragonInfo(internalId);
          this._dragonByLevel.Add(level, dragonInfo);
        }
      }
    }
    return dragonInfo;
  }

  public int GetDragonTendencySkillLevelLimit(int tendencySkillLevel)
  {
    List<DragonInfo> tendencySkillLevel1 = this.GetDragonInfosByTendencySkillLevel(tendencySkillLevel);
    int num = int.MaxValue;
    for (int index = 0; index < tendencySkillLevel1.Count; ++index)
    {
      if (tendencySkillLevel1[index].level < num)
        num = tendencySkillLevel1[index].level;
    }
    return num;
  }

  public DragonInfo GetDragonTendencySkillUnlockLevel(int tendencySkillLevel)
  {
    return this.GetDragonInfoByLevel(this.GetDragonTendencySkillLevelLimit(tendencySkillLevel));
  }

  public int GetDragonTendencySkillLevelCount()
  {
    Dictionary<int, int> dictionary = new Dictionary<int, int>();
    List<int> internalIds = this._parse.InternalIds;
    for (int index = 0; index < internalIds.Count; ++index)
    {
      DragonInfo dragonInfo = this.GetDragonInfo(internalIds[index]);
      dictionary[dragonInfo.tendency_skill_level] = dragonInfo.tendency_skill_level;
    }
    return dictionary.Count;
  }

  private List<DragonInfo> GetDragonInfosByTendencySkillLevel(int tendencySkillLevel)
  {
    List<DragonInfo> dragonInfoList = new List<DragonInfo>();
    List<int> internalIds = this._parse.InternalIds;
    for (int index = 0; index < internalIds.Count; ++index)
    {
      DragonInfo dragonInfo = this.GetDragonInfo(internalIds[index]);
      if (dragonInfo.tendency_skill_level == tendencySkillLevel)
        dragonInfoList.Add(dragonInfo);
    }
    return dragonInfoList;
  }

  public int GetSlotUnlockLevel(int slotIndex)
  {
    int num = slotIndex + 1;
    int val2 = int.MaxValue;
    List<int> internalIds = this._parse.InternalIds;
    for (int index = 0; index < internalIds.Count; ++index)
    {
      DragonInfo dragonInfo = this.GetDragonInfo(internalIds[index]);
      if (dragonInfo.skill_slot >= num)
        val2 = Math.Min(dragonInfo.level, val2);
    }
    return val2;
  }

  public void Clear()
  {
    this._dragonByLevel.Clear();
  }
}
