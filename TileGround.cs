﻿// Decompiled with JetBrains decompiler
// Type: TileGround
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class TileGround : MonoBehaviour
{
  private void OnClick()
  {
    if (!TutorialManager.Instance.IsRunning)
      return;
    TapGesture inGesture = new TapGesture();
    Camera ui2DoverlayCamera = UIManager.inst.ui2DOverlayCamera;
    inGesture.Position = (Vector2) ui2DoverlayCamera.WorldToScreenPoint(TutorialManager.Instance.TutorialDialogObj.clickTriger.transform.position);
    PVPSystem.Instance.Map.OnTap(inGesture);
  }
}
