﻿// Decompiled with JetBrains decompiler
// Type: WatchtowerPlayerInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class WatchtowerPlayerInfo : MonoBehaviour
{
  public UILabel m_PlayerName;
  public UISprite m_AllianceSymbol;
  public UILabel m_AllianceName;

  private void Start()
  {
  }

  public void SetMarchDetailData(Hashtable data)
  {
    this.m_PlayerName.text = data[(object) "name"] as string;
    if (data.ContainsKey((object) "alliance"))
    {
      Hashtable hashtable = data[(object) "alliance"] as Hashtable;
      this.m_AllianceSymbol.gameObject.SetActive(true);
      this.m_AllianceSymbol.spriteName = string.Format("shield_{0}", (object) (hashtable[(object) "symbol_code"] as string));
      this.m_AllianceName.text = string.Format("({0}) {1}", (object) (hashtable[(object) "acronym"] as string), (object) (hashtable[(object) "name"] as string));
    }
    else
    {
      this.m_AllianceSymbol.gameObject.SetActive(false);
      this.m_AllianceName.text = "No alliance";
    }
  }
}
