﻿// Decompiled with JetBrains decompiler
// Type: HeroItemUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public static class HeroItemUtils
{
  private static readonly Dictionary<int, int> HERO_ORDER_MAP = new Dictionary<int, int>()
  {
    {
      0,
      0
    },
    {
      1,
      4
    },
    {
      2,
      3
    },
    {
      3,
      1
    },
    {
      4,
      2
    },
    {
      5,
      5
    },
    {
      6,
      6
    }
  };
  private static readonly Dictionary<int, int> DK_ORDER_MAP = new Dictionary<int, int>()
  {
    {
      0,
      0
    },
    {
      1,
      4
    },
    {
      2,
      6
    },
    {
      3,
      3
    },
    {
      4,
      1
    },
    {
      5,
      2
    },
    {
      6,
      5
    }
  };
  private static readonly Dictionary<HeroItemType, string> HERO_ITEM_TYPE_NAMES = new Dictionary<HeroItemType, string>();
  private static readonly Dictionary<HeroItemType, string> DK_ITEM_TYPE_NAMES;

  static HeroItemUtils()
  {
    HeroItemUtils.HERO_ITEM_TYPE_NAMES[HeroItemType.ARMOR] = ScriptLocalization.Get("equipment_armor_title", true);
    HeroItemUtils.HERO_ITEM_TYPE_NAMES[HeroItemType.GREAVES] = ScriptLocalization.Get("equipment_greaves_title", true);
    HeroItemUtils.HERO_ITEM_TYPE_NAMES[HeroItemType.HELMET] = ScriptLocalization.Get("equipment_helmet_title", true);
    HeroItemUtils.HERO_ITEM_TYPE_NAMES[HeroItemType.WEAPON] = ScriptLocalization.Get("equipment_weapon_title", true);
    HeroItemUtils.HERO_ITEM_TYPE_NAMES[HeroItemType.NECKLACE] = ScriptLocalization.Get("equipment_amulet_title", true);
    HeroItemUtils.HERO_ITEM_TYPE_NAMES[HeroItemType.RING] = ScriptLocalization.Get("equipment_ring_title", true);
    HeroItemUtils.DK_ITEM_TYPE_NAMES = new Dictionary<HeroItemType, string>();
    HeroItemUtils.DK_ITEM_TYPE_NAMES[HeroItemType.ARMOR] = ScriptLocalization.Get("dragon_knight_armor_subtitle", true);
    HeroItemUtils.DK_ITEM_TYPE_NAMES[HeroItemType.GREAVES] = ScriptLocalization.Get("dragon_knight_legwear_subtitle", true);
    HeroItemUtils.DK_ITEM_TYPE_NAMES[HeroItemType.HELMET] = ScriptLocalization.Get("dragon_knight_helmet_subtitle", true);
    HeroItemUtils.DK_ITEM_TYPE_NAMES[HeroItemType.WEAPON] = ScriptLocalization.Get("dragon_knight_main_weapon_subtitle", true);
    HeroItemUtils.DK_ITEM_TYPE_NAMES[HeroItemType.NECKLACE] = ScriptLocalization.Get("dragon_knight_accessory_subtitle", true);
    HeroItemUtils.DK_ITEM_TYPE_NAMES[HeroItemType.RING] = ScriptLocalization.Get("dragon_knight_second_weapon_subtitle", true);
  }

  public static string GetItemTypeName(HeroItemType type, BagType bagType)
  {
    if (bagType == BagType.Hero)
      return HeroItemUtils.HERO_ITEM_TYPE_NAMES[type];
    if (bagType == BagType.DragonKnight)
      return HeroItemUtils.DK_ITEM_TYPE_NAMES[type];
    return string.Empty;
  }

  public static int GetItemTypeByIndex(int index, BagType bagType)
  {
    if (bagType == BagType.Hero)
      return HeroItemUtils.HERO_ORDER_MAP[index];
    if (bagType == BagType.DragonKnight)
      return HeroItemUtils.DK_ORDER_MAP[index];
    return 0;
  }
}
