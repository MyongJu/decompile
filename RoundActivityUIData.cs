﻿// Decompiled with JetBrains decompiler
// Type: RoundActivityUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class RoundActivityUIData : ActivityBaseUIData
{
  private RoundActivityData _activty;

  protected RoundActivityData Activty
  {
    get
    {
      if (this._activty == null)
        this._activty = this.Data as RoundActivityData;
      return this._activty;
    }
  }

  public override string EventKey
  {
    get
    {
      return "integral_config";
    }
  }

  public bool IsOpenRanking
  {
    get
    {
      return this.Activty.IsOpenRanking;
    }
  }

  protected RoundActivityData.RoundData CurrentRound
  {
    get
    {
      return this.Activty.CurrentRound;
    }
  }

  public override int StartTime
  {
    get
    {
      return this.Activty.StartTime;
    }
  }

  public override int EndTime
  {
    get
    {
      return this.Activty.EndTime;
    }
  }

  protected int TotalRound
  {
    get
    {
      return this.Activty.TotalRounds;
    }
  }

  protected int RoundIndex
  {
    get
    {
      return this.Activty.CurrentRoundRank;
    }
  }

  public virtual int ActivityMainConfigID
  {
    get
    {
      return 0;
    }
  }

  public ActivityMainInfo GetActivityMainInfo()
  {
    if (this.ActivityMainConfigID > 0)
      return ConfigManager.inst.DB_ActivityMain.GetData(this.ActivityMainConfigID);
    return (ActivityMainInfo) null;
  }

  public string GetName()
  {
    ActivityMainInfo activityMainInfo = this.GetActivityMainInfo();
    if (activityMainInfo == null)
      return string.Empty;
    if (this.IsOpenRanking)
      return activityMainInfo.LocalName;
    return activityMainInfo.LocalName1;
  }

  protected string GetNormalImage()
  {
    ActivityMainInfo activityMainInfo = this.GetActivityMainInfo();
    if (activityMainInfo != null)
      return activityMainInfo.IconPath;
    return string.Empty;
  }

  protected string GetADImage()
  {
    ActivityMainInfo activityMainInfo = this.GetActivityMainInfo();
    if (activityMainInfo != null)
      return activityMainInfo.ADImagePath;
    return string.Empty;
  }

  protected string GetUnStartImage()
  {
    return "Texture/GUI_Textures/mail_report_winner";
  }

  public virtual string GetNormalDesc()
  {
    return ScriptLocalization.Get("event_in_preparation_description", true);
  }

  protected string GetProgressDesc()
  {
    return ScriptLocalization.GetWithPara("event_stage_num", new Dictionary<string, string>()
    {
      {
        "1",
        this.RoundIndex.ToString() + "/" + (object) this.TotalRound
      }
    }, true) + " " + this.GetName();
  }

  public virtual string GetUnStartDesc()
  {
    return this.GetNormalDesc();
  }

  protected string GetADDesc()
  {
    ActivityMainInfo activityMainInfo = this.GetActivityMainInfo();
    if (activityMainInfo == null)
      return string.Empty;
    if (this.IsOpenRanking)
      return activityMainInfo.LocalADDesc;
    return activityMainInfo.LocalADDesc1;
  }

  public override int GetRemainTime()
  {
    return !this.IsStart() ? this.StartTime - NetServerTime.inst.ServerTimestamp : this.CurrentRound.EndTime - NetServerTime.inst.ServerTimestamp;
  }

  public int GetTotalRemainTime()
  {
    return !this.IsStart() ? this.StartTime - NetServerTime.inst.ServerTimestamp : this.EndTime - NetServerTime.inst.ServerTimestamp;
  }

  public override IconData GetADIconData()
  {
    IconData iconData = (IconData) null;
    if (this.CurrentRound != null)
    {
      iconData = new IconData();
      iconData.image = this.GetADImage();
      iconData.contents = new string[2];
      iconData.contents[0] = this.GetName();
      iconData.contents[1] = this.GetADDesc();
    }
    return iconData;
  }

  public override IconData GetNormalIconData()
  {
    IconData iconData = (IconData) null;
    if (this.CurrentRound != null)
    {
      iconData = new IconData();
      iconData.image = this.GetNormalImage();
      iconData.contents = new string[2];
      iconData.contents[0] = this.GetName();
      iconData.contents[1] = this.GetNormalDesc();
      iconData.Data = (object) this;
    }
    return iconData;
  }

  public override IconData GetUnstartIconData()
  {
    IconData iconData = new IconData();
    iconData.image = this.GetUnStartImage();
    iconData.contents = new string[1];
    iconData.contents[0] = this.GetUnStartDesc();
    iconData.Data = (object) this;
    return iconData;
  }
}
