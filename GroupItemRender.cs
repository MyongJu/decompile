﻿// Decompiled with JetBrains decompiler
// Type: GroupItemRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class GroupItemRender : MonoBehaviour
{
  private const string ENTER_LOC = "id_goto_foreign_country";
  private const string BACK_LOC = "id_return_home";
  private const string SCORE_PRE = "alliance_warfare_match_ranking_status";
  private const string CURRENT_POS = "alliance_warfare_troops_location_now";
  private const string ICON_NO_PATH = "Texture/LeaderboardIcons/icon_no";
  private const int MAX_ICON_LEVEL = 3;
  public UILabel kingdomName;
  public UILabel allianceName;
  public UILabel score;
  public GameObject selected;
  public GameObject selfSymbol;
  public AllianceSymbol allianceSymbol;
  public UITexture rankNum;
  public UIButton backBtn;
  public UIButton enterBtn;
  public UILabel backLabel;
  public UILabel enterLabel;
  public UISprite color;
  public UILabel rankText;
  public UILabel currentPos;
  private AllianceGroupData groupData;
  private int rank;
  private bool isCompleteState;
  public System.Action<GroupItemRender> onSelectedHandler;
  public System.Action<GroupItemRender> onEnterBtnClicked;
  public System.Action<GroupItemRender> onBackBtnClicked;

  public AllianceGroupData GroupData
  {
    get
    {
      return this.groupData;
    }
  }

  public void FeedData(AllianceGroupData data)
  {
    this.groupData = data;
    if (this.groupData == null)
      return;
    this.InitDelegate();
    this.UpdateUI();
  }

  public void FeedData(AllianceGroupData data, int rank, bool isCompleteState)
  {
    this.rank = rank;
    this.isCompleteState = isCompleteState;
    this.FeedData(data);
  }

  private void InitDelegate()
  {
    EventDelegate.Add(this.enterBtn.onClick, (EventDelegate.Callback) (() => this.OnEnterBtnPressedHandler()));
    EventDelegate.Add(this.backBtn.onClick, (EventDelegate.Callback) (() => this.OnBackBtnPressedHandler()));
  }

  private void UpdateUI()
  {
    this.allianceName.text = string.Format("[{0}]{1}", (object) this.groupData.acronym, (object) this.groupData.name);
    this.score.text = ScriptLocalization.GetWithPara("alliance_warfare_match_ranking_status", new Dictionary<string, string>()
    {
      {
        "0",
        this.groupData.score.ToString()
      }
    }, true);
    this.allianceSymbol.SetSymbols(this.groupData.symbol_code);
    if (this.IsSelfAlliance())
      this.selfSymbol.SetActive(true);
    else
      this.selfSymbol.SetActive(false);
    this.kingdomName.text = string.Format("{0} #{1}", (object) Utils.XLAT("id_kingdom"), (object) this.groupData.world_id.ToString());
    if (PlayerData.inst.userData.current_world_id == this.groupData.world_id)
    {
      this.currentPos.gameObject.SetActive(true);
      this.currentPos.text = string.Format("({0})", (object) Utils.XLAT("alliance_warfare_troops_location_now"));
      this.color.gameObject.SetActive(true);
    }
    else
    {
      this.currentPos.gameObject.SetActive(false);
      this.color.gameObject.SetActive(false);
    }
    if (this.isCompleteState)
    {
      if (this.rank <= 3)
      {
        this.rankText.gameObject.SetActive(false);
        this.rankNum.gameObject.SetActive(true);
        BuilderFactory.Instance.HandyBuild((UIWidget) this.rankNum, "Texture/LeaderboardIcons/icon_no" + this.rank.ToString(), (System.Action<bool>) null, true, false, string.Empty);
      }
      else
      {
        this.rankNum.gameObject.SetActive(false);
        this.rankText.gameObject.SetActive(true);
        this.rankText.text = this.rank.ToString();
      }
    }
    else
    {
      this.rankNum.gameObject.SetActive(false);
      this.rankText.gameObject.SetActive(false);
    }
  }

  private bool IsSelfAlliance()
  {
    bool flag = false;
    if (this.groupData.alliance_id == PlayerData.inst.userData.allianceId)
      flag = true;
    return flag;
  }

  public bool Select
  {
    set
    {
      NGUITools.SetActive(this.selected, value);
      if (value)
      {
        this.UpdateButtonState();
      }
      else
      {
        this.backBtn.gameObject.SetActive(false);
        this.enterBtn.gameObject.SetActive(false);
      }
    }
  }

  private void UpdateButtonState()
  {
    if (this.groupData.world_id == PlayerData.inst.userData.world_id)
    {
      this.backBtn.gameObject.SetActive(true);
      this.backLabel.text = Utils.XLAT("id_return_home");
      this.enterBtn.gameObject.SetActive(false);
    }
    else
    {
      this.backBtn.gameObject.SetActive(false);
      this.enterBtn.gameObject.SetActive(true);
      this.enterLabel.text = Utils.XLAT("id_goto_foreign_country");
      this.UpdateEnterBtnState();
    }
  }

  private void UpdateEnterBtnState()
  {
    if (AllianceWarPayload.Instance.IsForeigner())
      this.enterBtn.isEnabled = false;
    else
      this.enterBtn.isEnabled = true;
  }

  public void OnClick()
  {
    if (this.onSelectedHandler == null)
      return;
    this.onSelectedHandler(this);
  }

  private void OnEnterBtnPressedHandler()
  {
    if (this.onEnterBtnClicked == null)
      return;
    this.onEnterBtnClicked(this);
  }

  private void OnBackBtnPressedHandler()
  {
    if (this.onBackBtnClicked == null)
      return;
    this.onBackBtnClicked(this);
  }
}
