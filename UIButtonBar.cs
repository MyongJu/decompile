﻿// Decompiled with JetBrains decompiler
// Type: UIButtonBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class UIButtonBar : MonoBehaviour
{
  private int _selectedIndex = -1;
  private Dictionary<int, System.Action> lockHandlers = new Dictionary<int, System.Action>();
  public System.Action<int> OnSelectedHandler;
  public GameObject[] buttons;

  public int SelectedIndex
  {
    get
    {
      return this._selectedIndex;
    }
    set
    {
      if (this._selectedIndex == value)
        return;
      this._selectedIndex = value;
      this.ResetButton();
      if (this.OnSelectedHandler == null)
        return;
      this.OnSelectedHandler(this._selectedIndex);
    }
  }

  public void AddLock(int index, System.Action handler)
  {
    if (this.lockHandlers.ContainsKey(index))
      return;
    this.lockHandlers.Add(index, handler);
  }

  public void Clear()
  {
    this.lockHandlers.Clear();
  }

  public void OnClickHandler(GameObject go)
  {
    int key = -1;
    for (int index = 0; index < this.buttons.Length; ++index)
    {
      if ((UnityEngine.Object) this.buttons[index] == (UnityEngine.Object) go)
      {
        key = index;
        break;
      }
    }
    if (this.lockHandlers.ContainsKey(key))
      this.lockHandlers[key]();
    else
      this.SelectedIndex = key / 2;
  }

  private void ResetButton()
  {
    int index1 = 1;
    while (index1 < this.buttons.Length)
    {
      NGUITools.SetActive(this.buttons[index1], false);
      NGUITools.SetActive(this.buttons[index1 - 1], true);
      index1 += 2;
    }
    if (this.SelectedIndex <= -1 || this.buttons.Length <= this.SelectedIndex * 2)
      return;
    int index2 = this.SelectedIndex * 2;
    NGUITools.SetActive(this.buttons[index2], false);
    NGUITools.SetActive(this.buttons[index2 + 1], true);
  }

  private void Start()
  {
  }

  private void Update()
  {
  }
}
