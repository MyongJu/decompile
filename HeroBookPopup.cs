﻿// Decompiled with JetBrains decompiler
// Type: HeroBookPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HeroBookPopup : Popup
{
  private List<ParliamentHeroItem> _AllParliamentHeroItem = new List<ParliamentHeroItem>();
  [SerializeField]
  private UIGrid _GridContainer;
  [SerializeField]
  private UIScrollView _ScrollView;
  private GameObject _HeroBookItemTemplate;

  public override void OnShow(UIControler.UIParameter orgParam = null)
  {
    base.OnShow(orgParam);
    this._HeroBookItemTemplate = AssetManager.Instance.HandyLoad("Prefab/UI/Common/ParliamentHeroItem", (System.Type) null) as GameObject;
    RequestManager.inst.SendRequest("legend:getHeroList", (Hashtable) null, (System.Action<bool, object>) ((result, data) =>
    {
      if (!result)
        return;
      this.UpdateUI();
    }), true);
  }

  public override void OnHide(UIControler.UIParameter orgParam = null)
  {
    base.OnHide(orgParam);
  }

  public void OnButtonCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  protected void UpdateUI()
  {
    this.DestroyAllHeroBookItem();
    using (List<ParliamentHeroInfo>.Enumerator enumerator = ConfigManager.inst.DB_ParliamentHero.GetParliamentHeroInfoListOrderByQuality().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ParliamentHeroInfo current = enumerator.Current;
        int heroTemplateId = current.internalId;
        ParliamentHeroItem heroBookItem = this.CreateHeroBookItem();
        heroBookItem.SetData(current, true);
        LegendCardData legendCardData = DBManager.inst.DB_LegendCard.Get(PlayerData.inst.uid, current.internalId);
        if (current != null)
          heroBookItem.SetGrey(current.quality, legendCardData == null);
        heroBookItem.OnClickdCallback += (System.Action) (() => UIManager.inst.OpenPopup("ParliamentHero/HeroBookDetailPopup", (Popup.PopupParameter) new HeroBookDetailPopup.Parameter()
        {
          UserId = PlayerData.inst.uid,
          HeroTemplateId = heroTemplateId
        }));
      }
    }
    this._GridContainer.Reposition();
    this._ScrollView.ResetPosition();
  }

  protected ParliamentHeroItem CreateHeroBookItem()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._HeroBookItemTemplate);
    gameObject.transform.SetParent(this._GridContainer.transform);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    ParliamentHeroItem component = gameObject.GetComponent<ParliamentHeroItem>();
    component.SetDragScrollView(this._ScrollView);
    this._AllParliamentHeroItem.Add(component);
    return component;
  }

  protected void DestroyAllHeroBookItem()
  {
    using (List<ParliamentHeroItem>.Enumerator enumerator = this._AllParliamentHeroItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ParliamentHeroItem current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current.gameObject))
        {
          current.gameObject.transform.SetParent((Transform) null);
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
        }
      }
    }
    this._AllParliamentHeroItem.Clear();
  }
}
