﻿// Decompiled with JetBrains decompiler
// Type: ActivityFallenKnightStartPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;

public class ActivityFallenKnightStartPopup : Popup
{
  private int difficultyLevel = -1;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.difficultyLevel = (orgParam as ActivityFallenKnightStartPopup.Parameter).difficluty;
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.difficultyLevel = -1;
  }

  public void StartFallenKnightActivity()
  {
    MessageHub.inst.GetPortByAction("alliance:startKnightAttack").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "difficult",
        (object) this.difficultyLevel
      }
    }, new System.Action<bool, object>(this.OnStartKnightAttackCallback), true);
  }

  private void OnStartKnightAttackCallback(bool ret, object data)
  {
    if (!ret)
      return;
    ChatMessageManager.SendKnightStart(NetServerTime.inst.ServerTimestamp);
    ActivityManager.Intance.UpdateKnightData(data);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnCloseButtonPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int difficluty;
  }
}
