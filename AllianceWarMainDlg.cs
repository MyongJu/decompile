﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarMainDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceWarMainDlg : UI.Dialog
{
  public UILabel title;
  public UIButton awardButton;
  public GameObject userScoreGo;
  public UILabel userScroeLabel;
  public GameObject userRewardTipGo;
  public UILabel userRewardTipCount;
  public AllianceWarStateManager stateManager;
  private AllianceWarActivityData allianceWarData;
  private AllianceWarActivityData.AllianceWarState state;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    AllianceWarMainDlg.Parameter parameter = orgParam as AllianceWarMainDlg.Parameter;
    if (parameter == null)
      return;
    AllianceWarPayload.Instance.LoadUserScoreRewardInfo(new System.Action(this.OnUserScoreUpdate));
    this.userScoreGo.SetActive(true);
    this.allianceWarData = parameter.allianceWarData;
    this.state = this.allianceWarData.GetAllianceWarState();
    this.InitState();
    this.UpdateUI();
  }

  private void InitState()
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.stateManager))
      return;
    this.stateManager.OnStateChangeTo = new System.Action<AllianceWarBaseState>(this.OnStateChangedTo);
    this.stateManager.InitData(this.allianceWarData);
    this.stateManager.SetState(this.state, false);
  }

  private void UpdateUI()
  {
    this.title.text = Utils.XLAT("alliance_warfare_name");
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.stateManager.Release();
  }

  public void OnAwardButtonClicked()
  {
    UIManager.inst.OpenPopup("AllianceWar/AllianceRewardInstructionPopup", (Popup.PopupParameter) null);
  }

  public void OnScoreRewardClicked()
  {
    UIManager.inst.OpenPopup("AllianceWar/AllianceWarScoreRewardPopup", (Popup.PopupParameter) null);
  }

  private void OnUserScoreUpdate()
  {
    long userScore = AllianceWarPayload.Instance.UserScore;
    this.userScroeLabel.text = userScore.ToString();
    List<AllianceWarScoreRewardInfo> rewardInfoList = ConfigManager.inst.DB_AllianceWarScoreReward.RewardInfoList;
    int num = 0;
    List<string> cliamedRewardList = AllianceWarPayload.Instance.CliamedRewardList;
    using (List<AllianceWarScoreRewardInfo>.Enumerator enumerator = rewardInfoList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceWarScoreRewardInfo current = enumerator.Current;
        if (userScore > current.ScoreReq && !cliamedRewardList.Contains(current.ID))
          ++num;
      }
    }
    this.userRewardTipGo.SetActive(num > 0);
    this.userRewardTipCount.text = num.ToString();
  }

  private void OnStateChangedTo(AllianceWarBaseState targetState)
  {
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public AllianceWarActivityData allianceWarData;
  }
}
