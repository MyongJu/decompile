﻿// Decompiled with JetBrains decompiler
// Type: ResolutionLimit
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class ResolutionLimit : MonoBehaviour
{
  public float MaxAspectRation = 1.6f;

  private void Awake()
  {
    float num1 = (float) Screen.width / (float) Screen.height;
    if ((Object) UIManager.inst == (Object) null || (Object) UIManager.inst.uiRoot == (Object) null || (double) num1 <= (double) this.MaxAspectRation)
      return;
    UISprite component = this.GetComponent<UISprite>();
    if (!((Object) component != (Object) null))
      return;
    int num2 = Mathf.CeilToInt((num1 - this.MaxAspectRation) * (float) UIManager.inst.uiRoot.activeHeight * 0.5f);
    component.leftAnchor.absolute += num2;
    component.rightAnchor.absolute -= num2;
  }
}
