﻿// Decompiled with JetBrains decompiler
// Type: KingdomHospitalDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class KingdomHospitalDlg : UI.Dialog
{
  protected List<AllianceHealItem> m_allHealItems = new List<AllianceHealItem>();
  protected Dictionary<string, int> m_selectCounter = new Dictionary<string, int>();
  protected bool m_selectAll = true;
  [SerializeField]
  protected AllianceHealItem m_healItemTemplate;
  [SerializeField]
  protected UIScrollView m_healItemScrollView;
  [SerializeField]
  protected UIGrid m_healItemGrid;
  [SerializeField]
  protected UILabel m_capicityLabel;
  [SerializeField]
  protected GameObject m_rootEmptyTip;
  [SerializeField]
  protected UILabel m_selectedCountLabel;
  [SerializeField]
  protected GameObject m_rootNotUnderHealing;
  [SerializeField]
  protected UILabel m_instantHealCostLabel;
  [SerializeField]
  protected UILabel m_healTimeLabel;
  [SerializeField]
  protected UIButton m_healButton;
  [SerializeField]
  protected UIButton m_instantHealButton;
  [SerializeField]
  protected UISlider m_healingSlider;
  [SerializeField]
  protected GameObject m_rootUnderHealing;
  [SerializeField]
  protected UILabel m_leftHealingTimeLabel;
  [SerializeField]
  protected UILabel m_instantFinishHealingCost;
  protected CityData m_citydata;

  protected bool UnderHealing
  {
    get
    {
      return this.m_citydata.kingdomTroops.healingTroopsCount > 0L;
    }
  }

  protected int HealTimeCost
  {
    get
    {
      int num = 0;
      using (Dictionary<string, int>.Enumerator enumerator = this.m_selectCounter.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(current.Key);
          if (data != null)
          {
            int finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(current.Value * data.Heal_Time, data.HEAL_TIME_CAL_LEY);
            num += finalData;
          }
        }
      }
      GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("alliance_warfare_healing_speed_rate");
      if (data1 != null)
        num = Mathf.CeilToInt((float) num * (float) data1.Value);
      return num;
    }
  }

  protected int SelectCount
  {
    get
    {
      int num = 0;
      using (Dictionary<string, int>.Enumerator enumerator = this.m_selectCounter.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          num += current.Value;
        }
      }
      return num;
    }
  }

  protected int TroopsInKingdomHospital
  {
    get
    {
      int num = 0;
      using (Dictionary<string, CityHealingTroopInfo>.Enumerator enumerator = this.AllHealingData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, CityHealingTroopInfo> current = enumerator.Current;
          num += (int) current.Value.totalCount;
        }
      }
      return num;
    }
  }

  protected int LeftHealingTime
  {
    get
    {
      if (this.m_citydata.kingdomHospitalHealJob != 0L)
      {
        JobHandle job = JobManager.Instance.GetJob(this.m_citydata.kingdomHospitalHealJob);
        if (job != null)
          return job.LeftTime();
      }
      return 0;
    }
  }

  protected float HealingProgress
  {
    get
    {
      if (this.m_citydata.kingdomHospitalHealJob != 0L)
      {
        JobHandle job = JobManager.Instance.GetJob(this.m_citydata.kingdomHospitalHealJob);
        if (job != null)
          return Mathf.Clamp((float) (1.0 - (double) job.LeftTime() / (double) job.Duration()), 0.0f, 1f);
      }
      return 0.0f;
    }
  }

  protected int InstantFinishHealingCost
  {
    get
    {
      return ItemBag.CalculateCost(new Hashtable()
      {
        {
          (object) "kingdom_hospital_heal_time",
          (object) this.LeftHealingTime
        }
      });
    }
  }

  protected int InstantHealCost
  {
    get
    {
      return ItemBag.CalculateCost(new Hashtable()
      {
        {
          (object) "kingdom_hospital_heal_time",
          (object) this.HealTimeCost
        }
      });
    }
  }

  private Dictionary<string, CityHealingTroopInfo> AllHealingData
  {
    get
    {
      if (this.m_citydata.kingdomTroops.healingTroops != null)
        return this.m_citydata.kingdomTroops.healingTroops;
      return new Dictionary<string, CityHealingTroopInfo>();
    }
  }

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.SecondTick);
    MessageHub.inst.GetPortByAction("job_complete").AddEvent(new System.Action<object>(this.OnJobCompleted));
    MessageHub.inst.GetPortByAction("city:dismissKingdomHospitalTroops").AddEvent(new System.Action<object>(this.OnDismissTroops));
  }

  public void OnDisable()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.SecondTick);
    MessageHub.inst.GetPortByAction("job_complete").RemoveEvent(new System.Action<object>(this.OnJobCompleted));
    MessageHub.inst.GetPortByAction("city:dismissKingdomHospitalTroops").RemoveEvent(new System.Action<object>(this.OnDismissTroops));
  }

  protected void OnDismissTroops(object data)
  {
    this.UpdateUI(true);
  }

  protected void OnJobCompleted(object data)
  {
    this.UpdateUI(true);
  }

  protected void SecondTick(int delta)
  {
    this.UpdateUI(false);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_healItemTemplate.gameObject.SetActive(false);
    this.m_citydata = PlayerData.inst.playerCityData;
    this.UpdateUI(true);
    if (this.UnderHealing)
      return;
    this.m_selectAll = false;
    this.ToggleQuickSelect();
  }

  protected void ToggleQuickSelect()
  {
    this.m_selectAll = !this.m_selectAll;
    if (this.m_selectAll)
    {
      using (List<AllianceHealItem>.Enumerator enumerator = this.m_allHealItems.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AllianceHealItem current = enumerator.Current;
          current.CurrenCount = current.MaxCount;
        }
      }
    }
    else
    {
      using (List<AllianceHealItem>.Enumerator enumerator = this.m_allHealItems.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.CurrenCount = 0;
      }
    }
  }

  protected void UpdateUI(bool rebuildList = true)
  {
    this.m_capicityLabel.text = Utils.XLAT("event_fire_kingdom_wounded_troops_amount") + (object) this.TroopsInKingdomHospital;
    this.m_selectedCountLabel.text = this.SelectCount.ToString();
    this.m_rootUnderHealing.SetActive(this.UnderHealing);
    this.m_rootNotUnderHealing.SetActive(!this.UnderHealing);
    if (!this.UnderHealing)
    {
      this.m_instantHealCostLabel.text = this.InstantHealCost.ToString();
      this.m_healTimeLabel.text = Utils.FormatTime(this.HealTimeCost, false, false, true);
      this.m_healButton.isEnabled = this.SelectCount > 0;
      this.m_instantHealButton.isEnabled = this.SelectCount > 0;
    }
    else
    {
      this.m_leftHealingTimeLabel.text = Utils.FormatTime(this.LeftHealingTime, false, false, true);
      this.m_healingSlider.value = this.HealingProgress;
      this.m_instantFinishHealingCost.text = this.InstantFinishHealingCost.ToString();
    }
    if (!rebuildList)
      return;
    this.UpdateHealList();
  }

  protected void SetSelectCounter(string unitId, int count)
  {
    if (this.m_selectCounter.ContainsKey(unitId))
      this.m_selectCounter[unitId] = count;
    else
      this.m_selectCounter.Add(unitId, count);
  }

  protected void ClearSelectCounter()
  {
    this.m_selectCounter.Clear();
  }

  protected void UpdateHealList()
  {
    this.ClearSelectCounter();
    this.RemoveAllHealItems();
    if (this.AllHealingData == null || this.AllHealingData.Count <= 0)
    {
      this.m_rootEmptyTip.SetActive(true);
    }
    else
    {
      this.m_rootEmptyTip.SetActive(false);
      using (Dictionary<string, CityHealingTroopInfo>.Enumerator enumerator = this.AllHealingData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, CityHealingTroopInfo> current = enumerator.Current;
          if (current.Value.totalCount > 0L)
          {
            AllianceHealItem healItem = this.CreateHealItem();
            healItem.SetData(current.Key, (int) current.Value.healingCount, (int) current.Value.totalCount, this.UnderHealing, DissmissHealTroopPopup.HospitalType.KingdomHospital);
            this.SetSelectCounter(current.Key, (int) current.Value.healingCount);
            healItem.OnValueChanged += new System.Action<AllianceHealItem>(this.OnValueChanged);
          }
        }
      }
      this.m_allHealItems.Sort((Comparison<AllianceHealItem>) ((a, b) =>
      {
        Unit_StatisticsInfo unitInfo1 = a.UnitInfo;
        Unit_StatisticsInfo unitInfo2 = b.UnitInfo;
        if (unitInfo1 == null || unitInfo2 == null)
          return 0;
        if (unitInfo1.Troop_Tier == unitInfo2.Troop_Tier)
          return unitInfo1.Priority.CompareTo(unitInfo2.Priority);
        return unitInfo2.Troop_Tier.CompareTo(unitInfo1.Troop_Tier);
      }));
      this.AttachAllHealItems();
      this.m_healItemGrid.repositionNow = true;
      this.m_healItemGrid.Reposition();
      this.m_healItemScrollView.ResetPosition();
      this.m_healItemScrollView.currentMomentum = Vector3.zero;
    }
  }

  protected void OnValueChanged(AllianceHealItem healItem)
  {
    this.SetSelectCounter(healItem.GetUnitId(), healItem.CurrenCount);
    this.UpdateUI(false);
  }

  protected void RemoveAllHealItems()
  {
    using (List<AllianceHealItem>.Enumerator enumerator = this.m_allHealItems.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.m_allHealItems.Clear();
  }

  protected AllianceHealItem CreateHealItem()
  {
    AllianceHealItem component = UnityEngine.Object.Instantiate<GameObject>(this.m_healItemTemplate.gameObject).GetComponent<AllianceHealItem>();
    this.m_allHealItems.Add(component);
    return component;
  }

  protected void AttachAllHealItems()
  {
    using (List<AllianceHealItem>.Enumerator enumerator = this.m_allHealItems.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.gameObject;
        gameObject.SetActive(true);
        gameObject.transform.SetParent(this.m_healItemGrid.transform);
        gameObject.transform.localScale = Vector3.one;
      }
    }
  }

  public void OnQuickSelectClicked()
  {
    this.ToggleQuickSelect();
  }

  public void OnHealButtonClicked()
  {
    this.SendHealRequestMessage(0);
  }

  public void SendHealRequestMessage(int gold)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "city_id"] = (object) CityManager.inst.GetCityID();
    Hashtable hashtable = new Hashtable();
    using (Dictionary<string, int>.Enumerator enumerator = this.m_selectCounter.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, int> current = enumerator.Current;
        if (current.Value > 0)
          hashtable.Add((object) current.Key, (object) current.Value);
      }
    }
    postData[(object) "troops"] = (object) hashtable;
    postData[(object) nameof (gold)] = (object) gold;
    postData[(object) "instant"] = (object) false;
    MessageHub.inst.GetPortByAction("city:healKingdomHospitalTroops").SendRequest(postData, new System.Action<bool, object>(this.OnHealResultReceived), true);
  }

  public void OnHealResultReceived(bool result, object data)
  {
    if (!result)
      return;
    this.OnCloseButtonPress();
  }

  public void OnInstantHealButtonClicked()
  {
    string withPara = ScriptLocalization.GetWithPara("confirm_gold_spend_heal_instant_desc", new Dictionary<string, string>()
    {
      {
        "num",
        this.InstantHealCost.ToString()
      }
    }, true);
    UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
    {
      confirmButtonClickEvent = (System.Action) (() => this.OnConfirmedInstantHeal()),
      description = withPara,
      goldNum = this.InstantHealCost
    });
  }

  protected void OnConfirmedInstantHeal()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "city_id"] = (object) CityManager.inst.GetCityID();
    Hashtable hashtable = new Hashtable();
    using (Dictionary<string, int>.Enumerator enumerator = this.m_selectCounter.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, int> current = enumerator.Current;
        if (current.Value > 0)
          hashtable.Add((object) current.Key, (object) current.Value);
      }
    }
    postData[(object) "troops"] = (object) hashtable;
    postData[(object) "instant"] = (object) true;
    MessageHub.inst.GetPortByAction("city:healKingdomHospitalTroops").SendRequest(postData, new System.Action<bool, object>(this.OnInstantHealReceived), true);
  }

  protected void OnInstantHealReceived(bool result, object data)
  {
    if (!result)
      return;
    UIManager.inst.ShowBasicToast("toast_heal_timer_complete");
    this.OnCloseButtonPress();
  }

  public void OnCancelHealingButtonClicked()
  {
    UIManager.inst.OpenPopup("MessageBoxWith2ButtonsPopup", (Popup.PopupParameter) new MessageBoxWith2ButtonsPopup.Parameter()
    {
      titleString = Utils.XLAT("hospital_uppercase_heal"),
      contentString = ScriptLocalization.Get("hospital_heal_troop_cancel", true),
      leftLabelString = Utils.XLAT("id_uppercase_confirm"),
      rightLabelString = Utils.XLAT("id_uppercase_cancel"),
      onLeftBtnClick = (System.Action) (() => this.OnCancelHealConfirmed())
    });
  }

  public void OnCancelHealConfirmed()
  {
    MessageHub.inst.GetPortByAction("city:cancelHealingKingdomHospitalTroops").SendRequest(new Hashtable(), new System.Action<bool, object>(this.OnCancelHealReceived), true);
  }

  protected void OnCancelHealReceived(bool result, object data)
  {
    if (!result)
      return;
    this.UpdateUI(true);
  }

  public void OnInstantFinishHealingButtonClicked()
  {
    if (this.m_citydata.kingdomHospitalHealJob == 0L)
      return;
    int finishHealingCost = this.InstantFinishHealingCost;
    UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
    {
      confirmButtonClickEvent = new System.Action(this.OnConfirmInstantFinishHealing),
      lefttime = this.LeftHealingTime,
      goldNum = finishHealingCost,
      freetime = 0,
      description = ScriptLocalization.GetWithPara("confirm_gold_spend_cooldown_instant_desc", new Dictionary<string, string>()
      {
        {
          "num",
          finishHealingCost.ToString()
        }
      }, true)
    });
  }

  protected void OnConfirmInstantFinishHealing()
  {
    if (this.m_citydata.kingdomHospitalHealJob == 0L)
      return;
    int finishHealingCost = this.InstantFinishHealingCost;
    if (finishHealingCost <= 0)
      return;
    if (finishHealingCost > PlayerData.inst.hostPlayer.Currency)
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_insufficient_gold_tip", true), (System.Action) null, 4f, false);
    else
      RequestManager.inst.SendRequest("City:speedUpByGold", new Hashtable()
      {
        {
          (object) "job_id",
          (object) this.m_citydata.kingdomHospitalHealJob
        },
        {
          (object) "gold",
          (object) finishHealingCost
        }
      }, new System.Action<bool, object>(this.OnInstantFinishHealingReceived), true);
  }

  protected void OnInstantFinishHealingReceived(bool result, object data)
  {
    if (!result)
      return;
    UIManager.inst.ShowBasicToast("toast_heal_timer_complete");
    this.UpdateUI(true);
  }
}
