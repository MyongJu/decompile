﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragonKnightProfileInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class ConfigDragonKnightProfileInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "is_benefit")]
  public bool isBenefit;
  [Config(Name = "category")]
  public string category;
  [Config(Name = "is_private")]
  public bool isPrivate;
  [Config(Name = "order")]
  public int oder;
  [Config(Name = "loc")]
  public string loc;
  [Config(Name = "calc_id")]
  public int calcId;
  [Config(Name = "benefit_id_1")]
  public int benefitID1;
  [Config(Name = "benefit_id_2")]
  public int benefitID2;
  [Config(Name = "benefit_id_3")]
  public int benefitID3;
  [Config(Name = "benefit_id_4")]
  public int benefitID4;
  [Config(Name = "base_id")]
  public string baseID;
  [Config(Name = "base_source_type")]
  public int baseSourceType;
  [Config(Name = "nagative_type")]
  public int preStringNagtive;
  [Config(Name = "attribute")]
  public string attribute;

  public string Name
  {
    get
    {
      return Utils.XLAT(this.loc);
    }
  }

  public string DisplayContent
  {
    get
    {
      if (this.calcId > 0)
      {
        float baseValue = 0.0f;
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[this.baseID];
        if (this.baseSourceType == 0)
        {
          baseValue = DBManager.inst.DB_Local_Benefit.Get(this.baseID).total;
        }
        else
        {
          GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData(this.baseID);
          if (data != null)
            baseValue = (float) data.Value;
        }
        float num = ConfigManager.inst.DB_BenefitCalc.GetFinalData(baseValue, this.calcId);
        string str = string.Empty;
        if (this.preStringNagtive != 0)
        {
          if (this.preStringNagtive == -1)
            num = -num;
          if ((double) num > 0.0)
            str = "+";
        }
        return str + dbProperty.ConvertToDisplayStringValue((double) num, false);
      }
      PropertyDefinition dbProperty1 = ConfigManager.inst.DB_Properties[this.benefitID1];
      float num1 = 0.0f;
      if (this.benefitID1 > 0)
        num1 += DBManager.inst.DB_Local_Benefit.Get(this.benefitID1).total;
      if (this.benefitID2 > 0)
        num1 += DBManager.inst.DB_Local_Benefit.Get(this.benefitID2).total;
      if (this.benefitID3 > 0)
        num1 += DBManager.inst.DB_Local_Benefit.Get(this.benefitID3).total;
      if (this.benefitID4 > 0)
        num1 += DBManager.inst.DB_Local_Benefit.Get(this.benefitID4).total;
      string str1 = string.Empty;
      if (this.preStringNagtive != 0)
      {
        if (this.preStringNagtive == -1)
          num1 = -num1;
        if ((double) num1 > 0.0)
          str1 = "+";
      }
      if (dbProperty1 != null)
        return str1 + dbProperty1.ConvertToDisplayStringValue((double) num1, false);
      return string.Empty;
    }
  }
}
