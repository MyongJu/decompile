﻿// Decompiled with JetBrains decompiler
// Type: EquipmentDecomposePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class EquipmentDecomposePopup : Popup
{
  public EquipmentComponent equipmentComponent;
  public Icon template;
  public UIGrid grid;
  private ConfigEquipmentMainInfo info;
  private InventoryItemData inventory;
  private BagType bagType;
  private long itemID;

  private void Init()
  {
    this.inventory = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(this.itemID);
    int enhanced = this.inventory.enhanced;
    this.equipmentComponent.FeedData((IComponentData) new EquipmentComponentData(this.info.internalId, enhanced, this.bagType, this.inventory.itemId));
    int num1 = 0;
    Dictionary<int, int> elements = new Dictionary<int, int>();
    for (int enhanceLevel = 0; enhanceLevel <= enhanced; ++enhanceLevel)
    {
      ConfigEquipmentPropertyInfo idAndEnhanceLevel = ConfigManager.inst.GetEquipmentProperty(this.bagType).GetDataByEquipIDAndEnhanceLevel(this.info.internalId, enhanceLevel);
      num1 += Mathf.CeilToInt(ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) idAndEnhanceLevel.steelValue, "calc_crafting_steel_cost"));
      if (idAndEnhanceLevel.Materials != null)
      {
        List<Materials.MaterialElement> materials = idAndEnhanceLevel.Materials.GetMaterials();
        for (int index = 0; index < materials.Count; ++index)
          EquipmentDecomposePopup.AddMaterialElement(materials[index], elements);
      }
    }
    ConfigEquipmentScrollInfo dataByEquipmentId = ConfigManager.inst.GetEquipmentScroll(this.bagType).GetDataByEquipmentID(this.info.internalId);
    GameObject gameObject1 = NGUITools.AddChild(this.grid.gameObject, this.template.gameObject);
    gameObject1.SetActive(true);
    gameObject1.GetComponent<Icon>().FeedData((IComponentData) new IconData("Texture/BuildingConstruction/steel_icon", new string[2]
    {
      Utils.XLAT("id_uppercase_steel"),
      num1.ToString()
    }));
    RewardsCollectionAnimator.Instance.Clear();
    RewardsCollectionAnimator.Instance.Ress.Add(new ResRewardsInfo.Data()
    {
      count = num1,
      rt = ResRewardsInfo.ResType.Steel
    });
    List<Materials.MaterialElement> materials1 = dataByEquipmentId.Materials.GetMaterials();
    for (int index = 0; index < materials1.Count; ++index)
      EquipmentDecomposePopup.AddMaterialElement(materials1[index], elements);
    using (List<GemData>.Enumerator enumerator = this.inventory.inlaidGems.GetAllGemData().GetEnumerator())
    {
      while (enumerator.MoveNext())
        EquipmentDecomposePopup.AddGemElement(ConfigManager.inst.DB_EquipmentGem.GetData(enumerator.Current.ConfigId).itemID, 1, elements);
    }
    Dictionary<int, int>.Enumerator enumerator1 = elements.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      int key = enumerator1.Current.Key;
      int num2 = enumerator1.Current.Value;
      ItemStaticInfo itemStaticInfo1 = ConfigManager.inst.DB_Items.GetItem(key);
      GameObject gameObject2 = NGUITools.AddChild(this.grid.gameObject, this.template.gameObject);
      gameObject2.SetActive(true);
      gameObject2.GetComponent<Icon>().FeedData((IComponentData) new IconData(itemStaticInfo1.ImagePath, new string[2]
      {
        itemStaticInfo1.LocName,
        num2.ToString()
      }));
      ItemStaticInfo itemStaticInfo2 = ConfigManager.inst.DB_Items.GetItem(key);
      RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
      {
        icon = itemStaticInfo2.ImagePath,
        count = (float) num2
      });
    }
    this.grid.Reposition();
  }

  private static void AddMaterialElement(Materials.MaterialElement element, Dictionary<int, int> elements)
  {
    if (!elements.ContainsKey(element.internalID))
    {
      elements.Add(element.internalID, (int) element.amount);
    }
    else
    {
      Dictionary<int, int> dictionary;
      int internalId;
      (dictionary = elements)[internalId = element.internalID] = dictionary[internalId] + (int) element.amount;
    }
  }

  private static void AddGemElement(int internalID, int count, Dictionary<int, int> elements)
  {
    if (!elements.ContainsKey(internalID))
    {
      elements.Add(internalID, count);
    }
    else
    {
      Dictionary<int, int> dictionary;
      int index;
      (dictionary = elements)[index = internalID] = dictionary[index] + count;
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.info = (orgParam as EquipmentDecomposePopup.Parameter).equipmentMainInfo;
    this.bagType = (orgParam as EquipmentDecomposePopup.Parameter).bagType;
    this.itemID = (orgParam as EquipmentDecomposePopup.Parameter).itemID;
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnDecomposeBtnClick()
  {
    List<Benefits.BenefitValuePair> allAddedBenifit;
    List<Benefits.BenefitValuePair> allRemovedBenefit;
    ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance;
    ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance;
    Utils.GetEquipmentBenefitChangedInfo(this.bagType, (InventoryItemData) null, !this.inventory.isEquipped ? (InventoryItemData) null : this.inventory, out allRemovedBenefit, out allAddedBenifit, out addEquipmentSuitEnhance, out delEquipmentSuitEnhance);
    EquipmentManager.Instance.Disassemble(this.bagType, this.inventory.itemId, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      RewardsCollectionAnimator.Instance.CollectItems(false);
      RewardsCollectionAnimator.Instance.CollectResource(false);
      this.OnCloseBtnClick();
      if (allAddedBenifit.Count <= 0 && allRemovedBenefit.Count <= 0 && (addEquipmentSuitEnhance == null && delEquipmentSuitEnhance == null))
        return;
      RewardsCollectionAnimator.Instance.CollectItems(false);
      RewardsCollectionAnimator.Instance.CollectResource(false);
      this.OnCloseBtnClick();
      if (allAddedBenifit.Count <= 0 && allRemovedBenefit.Count <= 0 && (addEquipmentSuitEnhance == null && delEquipmentSuitEnhance == null))
        return;
      UIManager.inst.ShowEquipmentSuitBenefitChangeTip(allAddedBenifit, allRemovedBenefit, addEquipmentSuitEnhance, delEquipmentSuitEnhance);
    }));
  }

  public class Parameter : Popup.PopupParameter
  {
    public ConfigEquipmentMainInfo equipmentMainInfo;
    public BagType bagType;
    public long itemID;
  }
}
