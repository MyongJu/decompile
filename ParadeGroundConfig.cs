﻿// Decompiled with JetBrains decompiler
// Type: ParadeGroundConfig
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ParadeGroundConfig
{
  public static Vector3 FORMATION_DIR_X = new Vector3(293.6f, -154.8f, 0.0f).normalized;
  public static Vector3 FORMATION_DIR_Y = new Vector3(383f, 170f, 0.0f).normalized;
  public static Vector2 FORMATION_INFANTRY_RANGED_ARTYCLOSE_TROOPS = new Vector2(4f, 5f);
  public static Vector2 FORMATION_CAVARLY_TROOPS = new Vector2(2f, 4f);
  public static Vector2 FORMATION_ARTYFAR = new Vector2(1f, 2f);
  public const float FORMATION_WIDTH = 100f;
  public const float FORMATION_LENGTH = 100f;
  public const float CAVALRY_STEP = 0.5f;
}
