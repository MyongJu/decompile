﻿// Decompiled with JetBrains decompiler
// Type: PersonalActivityReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class PersonalActivityReward
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "stronghold_level")]
  public int StrongholdLevel;
  [Config(Name = "is_open")]
  public bool IsOpen;
  [Config(Name = "score")]
  public int Score;
  [Config(Name = "reward_id_1")]
  public int ItemRewardID_1;
  [Config(Name = "reward_value_1")]
  public int ItemRewardValue_1;
  [Config(Name = "reward_id_2")]
  public int ItemRewardID_2;
  [Config(Name = "reward_value_2")]
  public int ItemRewardValue_2;
  [Config(Name = "reward_id_3")]
  public int ItemRewardID_3;
  [Config(Name = "reward_value_3")]
  public int ItemRewardValue_3;
  [Config(Name = "reward_id_4")]
  public int ItemRewardID_4;
  [Config(Name = "reward_value_4")]
  public int ItemRewardValue_4;
  [Config(Name = "reward_id_5")]
  public int ItemRewardID_5;
  [Config(Name = "reward_value_5")]
  public int ItemRewardValue_5;
  [Config(Name = "reward_id_6")]
  public int ItemRewardID_6;
  [Config(Name = "reward_value_6")]
  public int ItemRewardValue_6;

  public string Description
  {
    get
    {
      return ScriptLocalization.GetWithPara("event_solo_event_description", new Dictionary<string, string>()
      {
        {
          "0",
          this.StrongholdLevel.ToString()
        },
        {
          "1",
          this.Score.ToString()
        }
      }, true);
    }
  }
}
