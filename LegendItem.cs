﻿// Decompiled with JetBrains decompiler
// Type: LegendItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using UnityEngine;

public class LegendItem : MonoBehaviour
{
  public UITexture legendIconTexture;
  public UILabel legendLevel;
  public UILabel legendPower;
  public UIButton legendinfo;
  public GameObject content;
  public UISprite marchImage;
  public UIGrid grid;
  public GameObject infantry;
  public GameObject ranged;
  public GameObject cavalry;
  public GameObject siege;
  [NonSerialized]
  public long legendID;
  public System.Action<long> LegendClickHandler;

  public void SetData(long legendid)
  {
    this.legendID = legendid;
    this.UpdateUI();
  }

  public void HideContent()
  {
    NGUITools.SetActive(this.content, false);
  }

  private void UpdateUI()
  {
    NGUITools.SetActive(this.content, true);
    LegendInfo legendInfo = ConfigManager.inst.DB_Legend.GetLegendInfo((int) this.legendID);
    LegendData legend = DBManager.inst.DB_Legend.GetLegend(this.legendID);
    this.legendLevel.text = ScriptLocalization.Get("id_lv", true) + legend.Level.ToString() + string.Empty;
    this.legendPower.text = legend.Power.ToString();
    NGUITools.SetActive(this.marchImage.gameObject, legend.Status == 1);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.legendIconTexture, legendInfo.Icon, (System.Action<bool>) null, true, false, string.Empty);
    NGUITools.SetActive(this.infantry, legendInfo.Infantry > 0);
    NGUITools.SetActive(this.ranged, legendInfo.Ranged > 0);
    NGUITools.SetActive(this.cavalry, legendInfo.Cavalry > 0);
    NGUITools.SetActive(this.siege, legendInfo.Siege > 0);
    this.grid.repositionNow = true;
    this.grid.Reposition();
  }

  public void LegendIconClick()
  {
    if (!this.legendinfo.gameObject.activeSelf || this.LegendClickHandler == null || this.legendID <= 0L)
      return;
    this.LegendClickHandler(this.legendID);
  }

  public void HideInfo()
  {
    NGUITools.SetActive(this.legendinfo.gameObject, false);
  }

  public void Clear()
  {
    NGUITools.SetActive(this.legendinfo.gameObject, true);
    BuilderFactory.Instance.Release((UIWidget) this.legendIconTexture);
    NGUITools.SetActive(this.content, false);
    this.LegendClickHandler = (System.Action<long>) null;
  }
}
