﻿// Decompiled with JetBrains decompiler
// Type: WishWellItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WishWellItemRenderer : MonoBehaviour
{
  public string m_Type;
  public UILabel m_Count;
  public UILabel m_Price;
  public UITexture m_Icon;
  public UITexture m_ItemIcon;
  private WishWellItemRenderer.OnWishCallback m_OnWish;

  private void Start()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, "Texture/BuildingConstruction/" + this.m_Type + "_icon", (System.Action<bool>) null, true, false, string.Empty);
  }

  public void UpdateUI(WishWellItemRenderer.OnWishCallback callback)
  {
    this.m_OnWish = callback;
    this.m_Count.text = Utils.FormatThousands(((int) ((double) this.GetBaseValueByType(ConfigManager.inst.DB_WishWell.GetWishWellDataByLevel(CityManager.inst.GetHighestBuildingLevelFor("wish_well"))) * (1.0 + ConfigManager.inst.DB_WishWellTimes.GetBoostValue(WishWellPayload.Instance.GetTotalWishes(this.m_Type))))).ToString());
    if (WishWellPayload.Instance.gems > 0)
    {
      this.m_Price.text = Utils.XLAT("id_uppercase_free");
      this.m_Price.color = Color.white;
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, "Texture/ItemIcons/item_wishing_well_gem", (System.Action<bool>) null, true, false, string.Empty);
    }
    else
    {
      Utils.SetPriceToLabel(this.m_Price, ConfigManager.inst.DB_WishWellTimes.GetPriceValue(WishWellPayload.Instance.GetGoldWishes(this.m_Type)));
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, "Texture/BuildingConstruction/gold_coin", (System.Action<bool>) null, true, false, string.Empty);
    }
  }

  public void OnItemClicked()
  {
    if (WishWellPayload.Instance.gems <= 0 && ConfigManager.inst.DB_WishWellTimes.GetPriceValue(WishWellPayload.Instance.GetGoldWishes(this.m_Type)) > PlayerData.inst.hostPlayer.Currency)
    {
      Utils.ShowNotEnoughGoldTip();
    }
    else
    {
      this.PlaySound();
      Hashtable postData = new Hashtable();
      postData[(object) "type"] = (object) this.m_Type;
      MessageHub.inst.GetPortByAction("Wishing:wish").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        AudioManager.Instance.StopAndPlaySound("sfx_city_make_wish");
        WishWellPayload.Instance.Decode((object) (data as Hashtable));
        if (this.m_OnWish == null)
          return;
        this.m_OnWish();
      }), true);
    }
  }

  private int GetBaseValueByType(WishWellData wishWellData)
  {
    string type = this.m_Type;
    if (type != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (WishWellItemRenderer.\u003C\u003Ef__switch\u0024mapB1 == null)
      {
        // ISSUE: reference to a compiler-generated field
        WishWellItemRenderer.\u003C\u003Ef__switch\u0024mapB1 = new Dictionary<string, int>(5)
        {
          {
            "food",
            0
          },
          {
            "wood",
            1
          },
          {
            "ore",
            2
          },
          {
            "silver",
            3
          },
          {
            "steel",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (WishWellItemRenderer.\u003C\u003Ef__switch\u0024mapB1.TryGetValue(type, out num))
      {
        switch (num)
        {
          case 0:
            return wishWellData.Food;
          case 1:
            return wishWellData.Wood;
          case 2:
            return wishWellData.Ore;
          case 3:
            return wishWellData.Silver;
          case 4:
            return wishWellData.Steel;
        }
      }
    }
    return 0;
  }

  private void PlaySound()
  {
    string type = this.m_Type;
    if (type == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (WishWellItemRenderer.\u003C\u003Ef__switch\u0024mapB2 == null)
    {
      // ISSUE: reference to a compiler-generated field
      WishWellItemRenderer.\u003C\u003Ef__switch\u0024mapB2 = new Dictionary<string, int>(5)
      {
        {
          "food",
          0
        },
        {
          "wood",
          1
        },
        {
          "ore",
          2
        },
        {
          "silver",
          3
        },
        {
          "steel",
          4
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!WishWellItemRenderer.\u003C\u003Ef__switch\u0024mapB2.TryGetValue(type, out num))
      return;
    switch (num)
    {
      case 0:
        AudioManager.Instance.StopAndPlaySound("sfx_city_wish_food");
        break;
      case 1:
        AudioManager.Instance.StopAndPlaySound("sfx_city_wish_wood");
        break;
      case 2:
        AudioManager.Instance.StopAndPlaySound("sfx_city_wish_ore");
        break;
      case 3:
        AudioManager.Instance.StopAndPlaySound("sfx_city_wish_silver");
        break;
      case 4:
        AudioManager.Instance.StopAndPlaySound("sfx_city_wish_silver");
        break;
    }
  }

  public delegate void OnWishCallback();
}
