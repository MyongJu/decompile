﻿// Decompiled with JetBrains decompiler
// Type: LogLevel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public static class LogLevel
{
  public const int Engine = 0;
  public const int Game = 1;
  public const int Network = 2;
  public const int Asset = 3;
  public const int Data = 4;
  public const int CoreLogic = 5;
  public const int Overhead = 9;
  public const int Timestamp = 10;
  public const int CustomLogic = 11;
  public const int Test = 2147483647;
}
