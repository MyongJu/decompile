﻿// Decompiled with JetBrains decompiler
// Type: UpdateMailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class UpdateMailPopup : SystemMailPopup
{
  private List<RewardRender> renders = new List<RewardRender>();
  public UILabel receiveBtnLabel;

  public override void UpdateReceiveTime()
  {
    this.receiveBtn.onClick.Clear();
    if (this.systemMessage.attachment_status == 2)
    {
      this.receiveTimeLabel.text = Utils.XLAT("mail_notice_reward_received");
      this.receiveBtnLabel.text = Utils.XLAT("mail_notice_uppercase_receive_button");
      this.receiveBtn.isEnabled = false;
      this.CancelInvoke();
    }
    else if (this.systemMessage.AttachmentExpirationLeftTime > 0)
    {
      this.receiveTimeLabel.text = Utils.XLAT("mail_notice_reward_time_remaining") + Utils.ConvertSecsToString((double) this.systemMessage.AttachmentExpirationLeftTime);
      if (this.systemMessage.GetCurrentVersion() < this.systemMessage.targetVersion)
      {
        this.receiveBtn.isEnabled = true;
        this.receiveBtnLabel.text = Utils.XLAT("mail_uppercase_update_button");
        this.receiveBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.HandleUpdateBtnClick)));
      }
      else
      {
        this.receiveBtnLabel.text = Utils.XLAT("mail_notice_uppercase_receive_button");
        this.receiveBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(((SystemMailPopup) this).HandleReceiveBtnClick)));
      }
    }
    else
    {
      this.receiveBtnLabel.text = Utils.XLAT("mail_notice_uppercase_receive_button");
      this.receiveTimeLabel.text = Utils.XLAT("mail_notice_reward_expired");
      this.receiveBtn.isEnabled = false;
      this.CancelInvoke();
    }
  }

  protected override void AddEventHandlers()
  {
    this.contactBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(((SystemMailPopup) this).HandleContactBtnClick)));
    this.rewardsTable.onReposition += new UITable.OnReposition(((SystemMailPopup) this).HandleRewardsTableReposition);
    this.rewardsContentTable.onReposition += new UITable.OnReposition(((SystemMailPopup) this).HandleRewardsContentTableReposition);
  }

  private void HandleUpdateBtnClick()
  {
    if (string.IsNullOrEmpty(NetApi.inst.UpdateURL))
      return;
    Application.OpenURL(NetApi.inst.UpdateURL);
  }
}
