﻿// Decompiled with JetBrains decompiler
// Type: GuardStateEnhanceMoral
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class GuardStateEnhanceMoral : AbstractGuardState
{
  private Coroutine _delayCoroutine;

  public GuardStateEnhanceMoral(RoundPlayer player)
    : base(player)
  {
  }

  public override string Key
  {
    get
    {
      return "guard_enhance";
    }
  }

  public override Hashtable Data { get; set; }

  public override void OnEnter()
  {
    this._delayCoroutine = Utils.ExecuteInSecs(1f, (System.Action) (() =>
    {
      this._delayCoroutine = (Coroutine) null;
      BattleManager.Instance.SendPacket((DragonKnightPacket) new DragonKnightPacketFinish(this.Player.PlayerId));
    }));
  }

  public override void OnProcess()
  {
  }

  public override void OnExit()
  {
  }

  public override void Dispose()
  {
    (this.Player as RoundPlayerMoral).StopVFX();
    if (this._delayCoroutine == null)
      return;
    Utils.StopCoroutine(this._delayCoroutine);
    this._delayCoroutine = (Coroutine) null;
  }
}
