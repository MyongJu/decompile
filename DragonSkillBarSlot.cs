﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillBarSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class DragonSkillBarSlot : MonoBehaviour
{
  private const string TEXTURE_PATH = "Texture/Dragon/";
  public UITexture m_EmptyIcon;
  public UITexture m_SkillIcon;
  public UILabel m_SkillLevel;
  public UILabel m_SlotUnlockLevel;
  public UILabel m_extraLev;
  public GameObject m_LevelObject;
  public GameObject m_EmptyObject;
  public GameObject m_LockObject;
  public GameObject m_rootStarLevel;
  public UILabel m_labelStarLevel;
  public string Usability;
  public int SlotIndex;
  public OnDragonSkillUnEquipCallback OnUnEquip;
  private ConfigDragonSkillMainInfo m_DragonSkillMainInfo;
  private ConfigDragonSkillGroupInfo m_DragonSkillGroupInfo;
  private int m_UnlockLevel;
  private Coroutine m_DoubleClickRoutine;
  private long _uid;

  public void SetData(long uid, int skill_id)
  {
    this._uid = uid;
    this.m_DragonSkillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo(skill_id);
    this.m_DragonSkillGroupInfo = this.m_DragonSkillMainInfo == null ? (ConfigDragonSkillGroupInfo) null : ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupInfo(this.m_DragonSkillMainInfo.group_id);
    this.m_UnlockLevel = ConfigManager.inst.DB_ConfigDragon.GetSlotUnlockLevel(this.SlotIndex);
    this.UpdateUI();
  }

  public ConfigDragonSkillMainInfo dragonSkill
  {
    get
    {
      return this.m_DragonSkillMainInfo;
    }
  }

  public int dragonSkillID
  {
    get
    {
      if (this.m_DragonSkillMainInfo != null)
        return this.m_DragonSkillMainInfo.internalId;
      return 0;
    }
  }

  public void OnSingleClick()
  {
    if (this.m_DoubleClickRoutine != null)
    {
      this.StopCoroutine(this.m_DoubleClickRoutine);
      this.m_DoubleClickRoutine = (Coroutine) null;
    }
    this.m_DoubleClickRoutine = this.StartCoroutine(this.SingleClickTimeout());
  }

  [DebuggerHidden]
  private IEnumerator SingleClickTimeout()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DragonSkillBarSlot.\u003CSingleClickTimeout\u003Ec__Iterator52()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void OnDoubleClick()
  {
    if (this.m_DoubleClickRoutine != null)
    {
      this.StopCoroutine(this.m_DoubleClickRoutine);
      this.m_DoubleClickRoutine = (Coroutine) null;
    }
    if (this.m_DragonSkillMainInfo != null && this.OnUnEquip != null)
      this.OnUnEquip(this.m_DragonSkillMainInfo.internalId);
    else if (this.IsLocked())
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      para["num"] = this.m_UnlockLevel.ToString();
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_dragon_skill_slot_locked", para, true), (System.Action) null, 4f, false);
    }
    else
      UIManager.inst.toast.Show(ScriptLocalization.Get("dragon_skill_assign_empty_slot_tip", true), (System.Action) null, 4f, false);
  }

  private void UpdateUI()
  {
    this.m_rootStarLevel.SetActive(false);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_EmptyIcon, "Texture/Dragon/" + DragonSkillBarSlot.GetEmptyIconPath(this.Usability), (System.Action<bool>) null, true, false, string.Empty);
    int num = 0;
    if (this.m_DragonSkillMainInfo != null)
      num = DBManager.inst.DB_Artifact.GetExtarLevel(this._uid, this.m_DragonSkillMainInfo.group_id);
    this.m_extraLev.text = num <= 0 ? string.Empty : "+" + (object) num;
    if (!this.IsLocked())
    {
      if (this.m_DragonSkillMainInfo != null)
      {
        BuilderFactory.Instance.HandyBuild((UIWidget) this.m_SkillIcon, this.m_DragonSkillGroupInfo.IconPath, (System.Action<bool>) null, true, false, string.Empty);
        this.m_SkillLevel.text = this.m_DragonSkillMainInfo.level.ToString();
        if (PlayerData.inst.dragonData != null)
        {
          int skillGroupStarLevel = PlayerData.inst.dragonData.GetSkillGroupStarLevel(this.m_DragonSkillMainInfo.group_id);
          if (skillGroupStarLevel > 0)
          {
            this.m_rootStarLevel.SetActive(true);
            this.m_labelStarLevel.text = skillGroupStarLevel.ToString();
          }
        }
        this.m_LevelObject.SetActive(true);
        this.m_EmptyObject.SetActive(false);
        this.m_LockObject.SetActive(false);
      }
      else
      {
        this.m_SkillIcon.mainTexture = (Texture) null;
        this.m_LevelObject.SetActive(false);
        this.m_EmptyObject.SetActive(true);
        this.m_LockObject.SetActive(false);
      }
    }
    else
    {
      this.m_SkillIcon.mainTexture = (Texture) null;
      this.m_LevelObject.SetActive(false);
      this.m_EmptyObject.SetActive(false);
      this.m_LockObject.SetActive(true);
      this.m_SlotUnlockLevel.text = "Lv. " + this.m_UnlockLevel.ToString();
    }
  }

  private bool IsLocked()
  {
    return this.SlotIndex >= ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(PlayerData.inst.dragonData.Level).skill_slot;
  }

  private static string GetEmptyIconPath(string type)
  {
    string key = type;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DragonSkillBarSlot.\u003C\u003Ef__switch\u0024map50 == null)
      {
        // ISSUE: reference to a compiler-generated field
        DragonSkillBarSlot.\u003C\u003Ef__switch\u0024map50 = new Dictionary<string, int>(4)
        {
          {
            "attack",
            0
          },
          {
            "defend",
            1
          },
          {
            "gather",
            2
          },
          {
            "attack_monster",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DragonSkillBarSlot.\u003C\u003Ef__switch\u0024map50.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return "icon_dragon_skill_attack";
          case 1:
            return "icon_dragon_skill_defense";
          case 2:
            return "icon_dragon_skill_gather";
          case 3:
            return "icon_dragon_skill_monster";
        }
      }
    }
    return string.Empty;
  }
}
