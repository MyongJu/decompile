﻿// Decompiled with JetBrains decompiler
// Type: QuickSearchItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UI;
using UnityEngine;

public class QuickSearchItemRenderer : MonoBehaviour
{
  public GameObject m_MonsterIcon;
  public GameObject m_GveIcon;
  public UILabel m_ItemName;
  public UILabel m_K;
  public UILabel m_X;
  public UILabel m_Y;
  public Icon m_Icon0;
  public Icon m_Icon1;
  private QuickSearchResultDB m_SearchedItem;
  private int selectedTabIndex;

  public void SetData(int selectedIndex, QuickSearchResultDB results)
  {
    this.m_SearchedItem = results;
    this.selectedTabIndex = selectedIndex;
    this.UpdateUI();
  }

  public QuickSearchResultDB SearchedItem
  {
    get
    {
      return this.m_SearchedItem;
    }
  }

  public void UpdateUI()
  {
    if (this.m_SearchedItem == null)
      return;
    if (this.selectedTabIndex == 0 && this.m_SearchedItem.resource_type == 9)
    {
      WorldEncounterData data = ConfigManager.inst.DB_WorldEncount.GetData((long) this.m_SearchedItem.resource_id);
      if (data != null)
      {
        this.m_Icon0.FeedData((IComponentData) new IconData(data.MonsterImage, new string[0]));
        this.m_MonsterIcon.SetActive(true);
        this.m_ItemName.text = string.Format("Lv.{0} {1}", (object) data.level, (object) data.Name);
      }
    }
    else
    {
      this.m_Icon1.FeedData((IComponentData) new IconData("Texture/Daily_reward/tiles_monstercamp2", new string[0]));
      this.m_GveIcon.SetActive(true);
      this.m_ItemName.text = string.Format("Lv.{0} {1}", (object) this.m_SearchedItem.resource_level, (object) ScriptLocalization.Get("gve_tile_name", true));
    }
    this.m_K.text = this.m_SearchedItem.location.K.ToString();
    this.m_X.text = this.m_SearchedItem.location.X.ToString();
    this.m_Y.text = this.m_SearchedItem.location.Y.ToString();
  }

  public void OnGotoClick()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(this.m_SearchedItem.location);
    if (referenceAt == null || referenceAt != null && referenceAt.ResourceId != this.m_SearchedItem.resource_id)
      Utils.RefreshBlock(this.m_SearchedItem.location, (System.Action<bool, object>) null);
    if (referenceAt == null)
    {
      PVPMapData.MapData.OnEnterKingdomBlock += new System.Action(this.OnGotoLocationOver);
      PVPSystem.Instance.Map.GotoLocation(this.m_SearchedItem.location, false);
    }
    else
    {
      PVPSystem.Instance.Map.GotoLocation(this.m_SearchedItem.location, false);
      this.OnGotoLocationOver();
    }
  }

  public void OnGotoLocationOver()
  {
    PVPMapData.MapData.OnEnterKingdomBlock -= new System.Action(this.OnGotoLocationOver);
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(this.m_SearchedItem.location);
    Vector3 position;
    Vector3 scale;
    if (this.m_SearchedItem.resource_type == 15)
    {
      position = new Vector3(referenceAt.Position.x, referenceAt.Position.y - PVPMapData.MapData.PixelsPerTile.y * 0.5f, referenceAt.Position.z - 150f);
      scale = new Vector3(2f, 2f, 1f);
    }
    else
    {
      position = new Vector3(referenceAt.Position.x, referenceAt.Position.y, referenceAt.Position.z - 150f);
      scale = Vector3.one;
    }
    PVPSystem.Instance.Map.ShowHighlight(position, scale);
  }
}
