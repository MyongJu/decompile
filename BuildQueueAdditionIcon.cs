﻿// Decompiled with JetBrains decompiler
// Type: BuildQueueAdditionIcon
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BuildQueueAdditionIcon : BuildQueueIcon
{
  private JobEvent jobEvent = JobEvent.JOB_ONE_MORE_BUILD_QUEUE;
  public UILabel leftQueueTime;
  public GameObject tip;
  private JobHandle queueJobHandle;

  public override void Init()
  {
    UILabel time = this.time;
    string empty = string.Empty;
    this.content.text = empty;
    string str = empty;
    time.text = str;
    this.progress.gameObject.SetActive(false);
    this.queueJobHandle = JobManager.Instance.GetSingleJobByClass(this.jobEvent);
    if (this.queueJobHandle != null)
    {
      this.ChangeState(BuildQueueIcon.State.IDLE);
      this.ShowTip();
    }
    else
    {
      this.ChangeState(BuildQueueIcon.State.NONE);
      this.HideTip();
    }
    JobManager.Instance.OnJobCreated += new System.Action<long>(this.OnJobCreate);
    JobManager.Instance.OnJobRemove += new System.Action<long>(this.OnJobRemove);
  }

  public override void UpdateUI(JobHandle jobHandle)
  {
    base.UpdateUI(jobHandle);
    JobHandle singleJobByClass = JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_ONE_MORE_BUILD_QUEUE);
    if (singleJobByClass == null)
      return;
    this.leftQueueTime.text = Utils.FormatTime(singleJobByClass.LeftTime(), true, false, true);
  }

  public override void OnClick()
  {
    base.OnClick();
    if (this.state == BuildQueueIcon.State.NONE)
      BuildQueueManager.Instance.GetMoreQueueTime((BuildQueuePopup.Parameter) null);
    else
      this.ShowTip();
  }

  private void OnJobCreate(long jobId)
  {
    JobHandle job = JobManager.Instance.GetJob(jobId);
    if (job.GetJobEvent() != this.jobEvent)
      return;
    this.ChangeState(BuildQueueIcon.State.IDLE);
    this.queueJobHandle = job;
    this.ShowTip();
  }

  private void OnJobRemove(long jobId)
  {
    if (JobManager.Instance.GetJob(jobId).GetJobEvent() != this.jobEvent)
      return;
    this.HideTip();
    this.ChangeState(BuildQueueIcon.State.NONE);
  }

  private void HideTipLater()
  {
    this.Invoke("HideTip", 2.5f);
  }

  private void HideTip()
  {
    this.tip.SetActive(false);
  }

  private void ShowTip()
  {
    this.leftQueueTime.text = Utils.FormatTime(this.queueJobHandle.LeftTime(), true, false, true);
    this.tip.SetActive(true);
    this.HideTipLater();
  }
}
