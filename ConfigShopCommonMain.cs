﻿// Decompiled with JetBrains decompiler
// Type: ConfigShopCommonMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigShopCommonMain
{
  private ConfigParse _parse = new ConfigParse();
  private Dictionary<string, ShopCommonMain> _dataByID;
  private Dictionary<int, ShopCommonMain> _dataByInternalID;

  public void BuildDB(object res)
  {
    Hashtable sources = res as Hashtable;
    if (sources == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      this._parse.Parse<ShopCommonMain, string>(sources, "id", out this._dataByID, out this._dataByInternalID);
      Dictionary<string, ShopCommonMain>.Enumerator enumerator = this._dataByID.GetEnumerator();
      while (enumerator.MoveNext())
        enumerator.Current.Value.UpdateCondition();
    }
  }

  public List<ShopCommonMain> GetShopData(int groupId, int categoryId)
  {
    List<ShopCommonMain> shopCommonMainList = new List<ShopCommonMain>();
    Dictionary<int, ShopCommonMain>.Enumerator enumerator = this._dataByInternalID.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.Value.group_id == groupId && enumerator.Current.Value.category_id == categoryId)
        shopCommonMainList.Add(enumerator.Current.Value);
    }
    return shopCommonMainList;
  }

  public List<ShopCommonMain> GetShopData(int groupId)
  {
    List<ShopCommonMain> shopCommonMainList = new List<ShopCommonMain>();
    Dictionary<int, ShopCommonMain>.Enumerator enumerator = this._dataByInternalID.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.Value.group_id == groupId)
        shopCommonMainList.Add(enumerator.Current.Value);
    }
    return shopCommonMainList;
  }

  public ShopCommonMain Get(int shopMainId)
  {
    if (this._dataByInternalID.ContainsKey(shopMainId))
      return this._dataByInternalID[shopMainId];
    return (ShopCommonMain) null;
  }
}
