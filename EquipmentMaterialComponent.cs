﻿// Decompiled with JetBrains decompiler
// Type: EquipmentMaterialComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EquipmentMaterialComponent : ComponentRenderBase
{
  public System.Action<EquipmentMaterialComponent> OnSelectedHandler;
  public UITexture icon;
  public UILabel amount;
  public UITexture background;
  public GameObject selected;
  public ItemStaticInfo itemInfo;

  public override void Init()
  {
    EquipmentMaterialComponentData data = this.data as EquipmentMaterialComponentData;
    this.itemInfo = data.itemInfo;
    Utils.SetItemBackground(this.background, this.itemInfo.internalId);
    BuilderFactory.Instance.Build((UIWidget) this.icon, data.itemInfo.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    int itemCount = ItemBag.Instance.GetItemCount(data.itemInfo.ID);
    if ((UnityEngine.Object) this.amount != (UnityEngine.Object) null)
      this.amount.text = itemCount.ToString();
    this.Select = false;
  }

  public override void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
    BuilderFactory.Instance.Release((UIWidget) this.background);
  }

  public bool Select
  {
    set
    {
      if (!((UnityEngine.Object) this.selected != (UnityEngine.Object) null))
        return;
      this.selected.SetActive(value);
    }
  }

  public void OnPressItem()
  {
    Utils.DelayShowTip((this.data as EquipmentMaterialComponentData).itemInfo.internalId, this.transform, 0L, 0L, 0);
  }

  public void OnReleaseItem()
  {
    Utils.StopShowItemTip();
  }

  public void OnClick()
  {
    if (this.OnSelectedHandler == null)
      return;
    this.OnSelectedHandler(this);
  }
}
