﻿// Decompiled with JetBrains decompiler
// Type: ParliamentHeroQualityInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ParliamentHeroQualityInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "initial_star")]
  public int initialStar;
  [Config(Name = "max_star")]
  public int maxStar;
  [Config(Name = "max_level")]
  public int maxLevel;
  [Config(Name = "exp_req_rate")]
  public float expReqRate;
  [Config(Name = "summon_chips_req")]
  public int summonChipsReq;
}
