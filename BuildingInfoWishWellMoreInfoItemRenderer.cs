﻿// Decompiled with JetBrains decompiler
// Type: BuildingInfoWishWellMoreInfoItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BuildingInfoWishWellMoreInfoItemRenderer : MonoBehaviour
{
  public UILabel m_Level;
  public UILabel m_Food;
  public UILabel m_Wood;
  public UILabel m_Ore;
  public UILabel m_Silver;
  public UILabel m_Gem;

  public void SetData(int level, int food, int wood, int ore, int silver, int gem)
  {
    this.m_Level.text = level.ToString();
    this.m_Food.text = food.ToString();
    this.m_Wood.text = wood.ToString();
    this.m_Ore.text = ore.ToString();
    this.m_Silver.text = silver.ToString();
    this.m_Gem.text = gem.ToString();
  }
}
