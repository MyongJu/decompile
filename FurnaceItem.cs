﻿// Decompiled with JetBrains decompiler
// Type: FurnaceItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class FurnaceItem : MonoBehaviour
{
  public int containIndex = -1;
  public UITexture background;
  public UITexture icon;
  public UILabel count;
  private int _itemId;
  public GameObject selectedGo;
  private int _useCount;
  public System.Action<int, int> OnItemSelected;

  private void SetUI(string iconPath, int itemCount, int quality = 0)
  {
    BuilderFactory.Instance.Build((UIWidget) this.icon, iconPath, (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.background, Utils.GetQualityImagePath(quality), (System.Action<bool>) null, true, false, true, string.Empty);
    this.count.text = itemCount.ToString();
  }

  public void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
    BuilderFactory.Instance.Release((UIWidget) this.background);
  }

  public void SetData(int itemId)
  {
    this._itemId = itemId;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo == null)
      return;
    int itemCount = ItemBag.Instance.GetItemCount(this._itemId);
    this.SetUI(itemStaticInfo.ImagePath, itemCount, itemStaticInfo.Quality);
  }

  public void Refresh()
  {
    int a = ItemBag.Instance.GetItemCount(this._itemId);
    if (this._useCount > 0 && a > 0)
      a = Mathf.Min(a, this._useCount);
    this.count.text = a.ToString();
  }

  public bool Clear()
  {
    int itemCount = ItemBag.Instance.GetItemCount(this._itemId);
    if (itemCount <= 0)
    {
      NGUITools.SetActive(this.gameObject, false);
      return false;
    }
    this.count.text = itemCount.ToString();
    NGUITools.SetActive(this.selectedGo, false);
    return true;
  }

  public void OnClickHandler()
  {
    UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) new ItemUseOrBuyPopup.Parameter()
    {
      item_id = this._itemId,
      type = ItemUseOrBuyPopup.Parameter.Type.UseItem,
      canZero = true,
      onSelectItemsCallBack = new System.Action<int, int>(this.OnSelectedItem)
    });
  }

  private void OnSelectedItem(int itemId, int count)
  {
    NGUITools.SetActive(this.selectedGo, count > 0);
    this._useCount = count;
    if (this.OnItemSelected != null)
      this.OnItemSelected(itemId, count);
    if (count == 0)
      count = ItemBag.Instance.GetItemCount(itemId);
    this.count.text = count.ToString();
  }

  public void OnItemPress()
  {
    Utils.DelayShowTip(this._itemId, this.transform, 0L, 0L, 0);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }
}
