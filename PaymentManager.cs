﻿// Decompiled with JetBrains decompiler
// Type: PaymentManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Funplus;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PaymentManager
{
  private Dictionary<string, System.Action<bool>> infos = new Dictionary<string, System.Action<bool>>();
  private string productId = string.Empty;
  private Dictionary<string, object> fbParam = new Dictionary<string, object>();
  private List<string> _permitlist = new List<string>();
  private List<string> _backPlatform = new List<string>();
  private Stack<PaymentManager.PurchaseRecord> _records = new Stack<PaymentManager.PurchaseRecord>();
  private string paymentServerId = string.Empty;
  private string _paymentURL = string.Empty;
  private string _paymentAppId = string.Empty;
  public const float PURCHASE_LOCK_TIME = 60f;
  private FunplusPaymentAgent payment;
  private bool init;
  private System.Action<bool> callBack;
  private static PaymentManager _instance;
  private bool _needFetchProductList;
  private bool _needUpdateClient;
  private Coroutine m_Coroutine;
  private System.Action OnPurchaseFinished;

  private PaymentManager()
  {
  }

  public static PaymentManager Instance
  {
    get
    {
      if (PaymentManager._instance == null)
        PaymentManager._instance = new PaymentManager();
      return PaymentManager._instance;
    }
  }

  public bool HasWhiteList { get; set; }

  public void SetPlatformlist(object org)
  {
    this._backPlatform.Clear();
    ArrayList arrayList = org as ArrayList;
    if (arrayList == null)
      return;
    for (int index = 0; index < arrayList.Count; ++index)
    {
      if (arrayList[index] != null)
      {
        string str = arrayList[index].ToString();
        if (!this._backPlatform.Contains(str))
          this._backPlatform.Add(str);
      }
    }
  }

  public void SetPermitlist(object org)
  {
    this._permitlist.Clear();
    ArrayList arrayList = org as ArrayList;
    if (arrayList == null)
      return;
    for (int index = 0; index < arrayList.Count; ++index)
    {
      if (arrayList[index] != null)
      {
        IAPPlatformInfo iapPlatformInfo = ConfigManager.inst.DB_IAPPlatform.Get(Application.platform, arrayList[index].ToString());
        if (iapPlatformInfo != null && !this._permitlist.Contains(iapPlatformInfo.product_id))
          this._permitlist.Add(iapPlatformInfo.product_id);
      }
    }
  }

  public string PaymentServerId
  {
    get
    {
      return this.paymentServerId;
    }
    set
    {
      this.paymentServerId = value;
    }
  }

  public void ShowMyCard()
  {
    if (Application.isEditor || !CustomDefine.IsMyCardDefine())
      return;
    FunplusThirdPartyPayment.Instance.Show(string.Empty);
  }

  public void Initialize()
  {
    if (!this.init)
    {
      this.init = true;
      this.payment = new FunplusPaymentAgent();
      this.payment.OnBuyCallBack += new System.Action<bool>(this.OnBuyCallback);
    }
    FunplusPayment.Instance.SetDelegate((FunplusPayment.IDelegate) this.payment);
    if (CustomDefine.IsMyCardDefine())
      FunplusThirdPartyPayment.Instance.SetDelegate((FunplusThirdPartyPayment.IDelegate) this.payment);
    this.GetProductList();
  }

  public void GetPayStatus()
  {
    RequestManager.inst.SendLoader("player:checkShieldingPay", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "currency_code", (object) this.GetCurrencyCode()), new System.Action<bool, object>(this.OnWhiteListCallBack), true, false);
  }

  private void OnWhiteListCallBack(bool success, object result)
  {
    if (!success)
      return;
    Hashtable hashtable = result as Hashtable;
    this.HasWhiteList = false;
    this._needFetchProductList = false;
    this._needUpdateClient = false;
    if (hashtable != null && hashtable.ContainsKey((object) "status") && hashtable[(object) "status"] != null)
      this.HasWhiteList = hashtable[(object) "status"].ToString().Trim() == "1";
    if (hashtable != null && hashtable.ContainsKey((object) "refresh_product_list") && hashtable[(object) "refresh_product_list"] != null)
      this._needFetchProductList = hashtable[(object) "refresh_product_list"].ToString().Trim() == "1";
    if (hashtable != null && hashtable.ContainsKey((object) "permitlist") && hashtable[(object) "permitlist"] != null)
      this.SetPermitlist(hashtable[(object) "permitlist"]);
    if (hashtable != null && hashtable.ContainsKey((object) "forbidden_platform") && hashtable[(object) "forbidden_platform"] != null)
      this.SetPlatformlist(hashtable[(object) "forbidden_platform"]);
    if (hashtable == null || !hashtable.ContainsKey((object) "recharge_limit") || hashtable[(object) "recharge_limit"] == null)
      return;
    this._needUpdateClient = hashtable[(object) "recharge_limit"].ToString().Trim() == "0";
  }

  public void GetProductList()
  {
    FunplusPayment.Instance.StartHelper();
  }

  private void FetchProductList()
  {
    this.payment.OnFetchProductListFinish += new System.Action(this.OnFetchFinish);
    this.GetProductList();
  }

  private void OnFetchFinish()
  {
    this.payment.OnFetchProductListFinish -= new System.Action(this.OnFetchFinish);
    if (!GameEngine.IsReady())
      return;
    RequestManager.inst.SendRequest("player:checkShieldingPay", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "currency_code", (object) this.GetCurrencyCode()), new System.Action<bool, object>(this.OnWhiteListRequestCallBack), true);
  }

  private void OnWhiteListRequestCallBack(bool success, object result)
  {
    this.OnWhiteListCallBack(success, result);
    if (this._records.Count <= 0)
      return;
    PaymentManager.PurchaseRecord purchaseRecord = this._records.Pop();
    string contentKey = string.Empty;
    if (this.IsForbiddenBuy(purchaseRecord.ProductId, ref contentKey))
    {
      string title = ScriptLocalization.Get("iap_payment_3rdparty_cancel_title", true);
      contentKey = "iap_payment_3rdparty_violation_description";
      string message = ScriptLocalization.Get(contentKey, true);
      FunplusSdkUtils.Instance.ShowAlert(title, message, "OK");
      if (this.callBack != null)
        this.callBack(false);
      this.callBack = (System.Action<bool>) null;
    }
    else
    {
      FunplusPayment.Instance.Buy(purchaseRecord.ProductId, purchaseRecord.ServerId, purchaseRecord.ThroughCargo);
      this._records.Clear();
    }
  }

  private void OnBuyCallback(bool success)
  {
    if (!success && Application.platform == RuntimePlatform.IPhonePlayer)
      FunplusSdkUtils.Instance.ShowAlert(ScriptLocalization.Get("iap_payment_3rdparty_cancel_title", true), ScriptLocalization.Get("account_apple_payment_failed_description", true), "OK");
    if (this.callBack == null)
      return;
    this.callBack(success);
    if (!success)
      return;
    TrackEvent.Instance.Trace("purchase", TrackEvent.Type.Adjust);
    TrackEvent.Instance.TracePurchaseToFB((float) this.GetRealPrice(this.productId, string.Empty), this.GetCurrencyCode(this.productId, string.Empty), this.fbParam);
  }

  public string GetCurrencyCode(string productId, string payId)
  {
    FunplusProduct product = PaymentManager.Instance.GetProduct(productId);
    if (product != null)
      return product.PriceCurrencyCode;
    return "$";
  }

  public double GetRealPrice(string productId, string payId)
  {
    FunplusProduct product = PaymentManager.Instance.GetProduct(productId);
    if (product != null)
      return (double) product.Price / 1000000.0;
    ConfigIAPFakePriceInfo iapFakePriceInfo = ConfigManager.inst.DB_IAPPrice.Get(payId);
    if (iapFakePriceInfo != null)
      return iapFakePriceInfo.price;
    return 0.0;
  }

  public string GetFormattedPrice(string productId, string payId)
  {
    FunplusProduct product = PaymentManager.Instance.GetProduct(productId);
    if (product != null)
    {
      if (product.GetPriceCurrencyCode() == "AED")
        return "AED" + ((double) product.Price / 1000000.0).ToString("f2");
      return product.GetFormattedPrice();
    }
    ConfigIAPFakePriceInfo iapFakePriceInfo = ConfigManager.inst.DB_IAPPrice.Get(payId);
    if (iapFakePriceInfo != null)
      return "$" + iapFakePriceInfo.price.ToString("f2");
    return "N/A";
  }

  public bool IsReady
  {
    get
    {
      return this.payment.Products != null;
    }
  }

  public List<FunplusProduct> ProductList
  {
    get
    {
      if (this.payment != null)
        return this.payment.Products;
      return (List<FunplusProduct>) null;
    }
  }

  public string GetCurrencyCode()
  {
    List<FunplusProduct> productList = this.ProductList;
    if (productList != null)
    {
      int index = 0;
      if (index < productList.Count)
        return productList[index].PriceCurrencyCode;
    }
    return string.Empty;
  }

  public FunplusProduct GetProduct(string id)
  {
    List<FunplusProduct> productList = this.ProductList;
    if (productList != null)
    {
      for (int index = 0; index < productList.Count; ++index)
      {
        if (productList[index].GetProductId() == id)
          return productList[index];
      }
    }
    return (FunplusProduct) null;
  }

  public bool IsPaidUser
  {
    get
    {
      StatsData statsData = DBManager.inst.DB_Stats.Get("pay_count");
      return statsData != null && statsData.count > 0L;
    }
  }

  protected void PushBI(string pruductId, string productName)
  {
    if (!FunplusSdk.Instance.IsSdkInstalled())
      return;
    FunplusBi.Instance.TraceEvent("payment_start", Utils.Object2Json((object) new PaymentManager.BIDataFormat()
    {
      d_c1 = new PaymentManager.BIStep()
      {
        value = pruductId,
        key = "product_id"
      },
      d_c2 = new PaymentManager.BIStep()
      {
        value = productName,
        key = "product_name"
      },
      d_c3 = new PaymentManager.BIStep()
      {
        value = (!UIManager.inst.ExistAutoOpened ? "0" : "1"),
        key = "is_active"
      }
    }));
  }

  public void BlockBuyProduct(string productId, string packageName, string groupName, System.Action<bool> callback, System.Action finish = null)
  {
    this.OnPurchaseFinished = finish;
    this.StartCoroutine();
    UIManager.inst.ShowManualBlocker();
    MessageHub.inst.GetPortByAction("payment_success").AddEvent(new System.Action<object>(this.OnPushPaymentSuccess));
    PaymentManager.Instance.BuyProduct(productId, packageName, groupName, string.Empty, (System.Action<bool>) (success =>
    {
      if (!success)
        this.OnPushPaymentSuccess((object) null);
      if (callback == null)
        return;
      callback(success);
    }));
  }

  private void OnPushPaymentSuccess(object result)
  {
    MessageHub.inst.GetPortByAction("payment_success").RemoveEvent(new System.Action<object>(this.OnPushPaymentSuccess));
    UIManager.inst.HideManualBlocker();
    this.StopCoroutine();
    if (this.OnPurchaseFinished == null)
      return;
    this.OnPurchaseFinished();
  }

  private void StartCoroutine()
  {
    this.StopCoroutine();
    this.m_Coroutine = Utils.ExecuteInSecs(10f, (System.Action) (() => this.OnPushPaymentSuccess((object) null)));
  }

  private void StopCoroutine()
  {
    if (this.m_Coroutine == null)
      return;
    Utils.StopCoroutine(this.m_Coroutine);
    this.m_Coroutine = (Coroutine) null;
  }

  private string GetNoLoginDesc()
  {
    string Term = "account_googleplay_no_login_error";
    switch (CustomDefine.Channel)
    {
      case CustomDefine.ChannelEnum.Amazon:
        Term = "account_amazon_no_login_error";
        break;
      case CustomDefine.ChannelEnum.Onestore:
        Term = !(OneStorePayAndroid.Instance.NoProductListErrorMessage != string.Empty) ? ScriptLocalization.Get("account_onestore_no_login_error", true) : OneStorePayAndroid.Instance.NoProductListErrorMessage;
        break;
      case CustomDefine.ChannelEnum.Samsung:
        Term = "account_samsung_no_login_error";
        break;
    }
    return ScriptLocalization.Get(Term, true);
  }

  public void BuyProduct(string productId, string packageName = "", string groupName = "", string orderId = "", System.Action<bool> callBack = null)
  {
    if (this.ProductList != null)
    {
      if (this.ProductList != null)
      {
        if (this.ProductList.Count == 0)
          goto label_3;
      }
      try
      {
        if (this._needUpdateClient)
        {
          this.NeedUpdateHandler();
          if (callBack == null)
            return;
          callBack(false);
          return;
        }
        this.fbParam.Clear();
        this.callBack = callBack;
        string throughCargo = PlayerData.inst.uid.ToString() + "|" + packageName + "|" + groupName + "|" + this.paymentServerId + "|" + orderId;
        this.PushBI(productId, groupName + ":" + packageName);
        this.fbParam.Add("fb_content_id", (object) productId);
        this.fbParam.Add("Package Name", (object) packageName);
        this.fbParam.Add("Group Name", (object) groupName);
        this.fbParam.Add("Payment Server", (object) this.paymentServerId);
        this.productId = productId;
        this.PushWhiteListBI(productId, packageName, throughCargo);
        string contentKey = string.Empty;
        if (this.IsForbiddenBuy(productId, ref contentKey))
        {
          string title = ScriptLocalization.Get("iap_payment_3rdparty_cancel_title", true);
          contentKey = "iap_payment_3rdparty_violation_description";
          string message = ScriptLocalization.Get(contentKey, true);
          FunplusSdkUtils.Instance.ShowAlert(title, message, "OK");
          if (callBack != null)
            callBack(false);
          this.callBack = (System.Action<bool>) null;
          return;
        }
        this.Buy(productId, this.paymentServerId, throughCargo);
        return;
      }
      catch
      {
        return;
      }
    }
label_3:
    if (callBack != null)
      callBack(false);
    if (Application.platform != RuntimePlatform.Android)
      return;
    string noLoginDesc = this.GetNoLoginDesc();
    string title1 = ScriptLocalization.Get("account_warning_title", true);
    string left = ScriptLocalization.Get("id_uppercase_okay", true);
    string right = ScriptLocalization.Get("id_uppercase_cancel", true);
    UIManager.inst.ShowConfirmationBox(title1, noLoginDesc, left, right, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) null, (System.Action) null, (System.Action) null);
  }

  private void NeedUpdateHandler()
  {
    string content = ScriptLocalization.Get("account_temporary_unable_topup_update_description", true);
    string title = ScriptLocalization.Get("account_temporary_unable_topup_title", true);
    string str = ScriptLocalization.Get("id_uppercase_update", true);
    UIManager.inst.ShowConfirmationBox(title, content, str, str, ChooseConfirmationBox.ButtonState.OK_CENTER, new System.Action(this.HandleUpdateBtnClick), new System.Action(this.HandleUpdateBtnClick), new System.Action(this.HandleUpdateBtnClick));
  }

  private void HandleUpdateBtnClick()
  {
    if (string.IsNullOrEmpty(NetApi.inst.UpdateURL))
      return;
    Application.OpenURL(NetApi.inst.UpdateURL);
  }

  private void Buy(string productId, string serverId, string param)
  {
    if (this._needFetchProductList)
    {
      this._records.Push(new PaymentManager.PurchaseRecord(productId, this.paymentServerId, param));
      this.FetchProductList();
    }
    else
      FunplusPayment.Instance.Buy(productId, this.paymentServerId, param);
  }

  public void BuyIapProduct(string productId, string iapId, string packageName = "", string groupName = "", System.Action<bool> callBack = null)
  {
    if (this.ProductList != null)
    {
      if (this.ProductList != null)
      {
        if (this.ProductList.Count == 0)
          goto label_3;
      }
      try
      {
        if (this._needUpdateClient)
        {
          this.NeedUpdateHandler();
          return;
        }
        this.fbParam.Clear();
        this.callBack = callBack;
        string throughCargo = PlayerData.inst.uid.ToString() + "|" + iapId + "|" + groupName;
        this.PushBI(productId, groupName + ":" + packageName);
        this.fbParam.Add("fb_content_id", (object) productId);
        this.fbParam.Add("Package Name", (object) packageName);
        this.fbParam.Add("Group Name", (object) groupName);
        this.fbParam.Add("Payment Server", (object) this.paymentServerId);
        this.productId = productId;
        this.PushWhiteListBI(productId, packageName, throughCargo);
        string contentKey = string.Empty;
        if (this.IsForbiddenBuy(productId, ref contentKey))
        {
          string title = ScriptLocalization.Get("iap_payment_3rdparty_cancel_title", true);
          contentKey = "iap_payment_3rdparty_violation_description";
          string message = ScriptLocalization.Get(contentKey, true);
          FunplusSdkUtils.Instance.ShowAlert(title, message, "OK");
          if (callBack != null)
            callBack(false);
          this.callBack = (System.Action<bool>) null;
          return;
        }
        this.Buy(productId, this.paymentServerId, throughCargo);
        return;
      }
      catch
      {
        return;
      }
    }
label_3:
    if (callBack != null)
      callBack(false);
    if (Application.platform != RuntimePlatform.Android)
      return;
    string noLoginDesc = this.GetNoLoginDesc();
    string title1 = ScriptLocalization.Get("account_warning_title", true);
    string left = ScriptLocalization.Get("id_uppercase_okay", true);
    string right = ScriptLocalization.Get("id_uppercase_cancel", true);
    UIManager.inst.ShowConfirmationBox(title1, noLoginDesc, left, right, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) null, (System.Action) null, (System.Action) null);
  }

  private bool IsForbiddenBuy(string productId, ref string contentKey)
  {
    if (this._backPlatform.Contains(CustomDefine.GetIAPChannel()))
    {
      contentKey = "account_channel_payment_error";
      return true;
    }
    if (!this.HasWhiteList || this._permitlist.Contains(productId))
      return false;
    contentKey = "account_sdk_payment_error";
    return true;
  }

  private void PushWhiteListBI(string productId, string packageName, string throughCargo)
  {
    string channel = CustomDefine.GetChannel();
    if (!this.HasWhiteList && !this._backPlatform.Contains(channel))
      return;
    string properties = Utils.Object2Json((object) new PaymentManager.BIDataFormat()
    {
      d_c1 = new PaymentManager.BIStep()
      {
        value = productId,
        key = "product_id"
      },
      d_c2 = new PaymentManager.BIStep()
      {
        value = this.GetCurrencyCode(),
        key = "currency_code"
      },
      d_c3 = new PaymentManager.BIStep()
      {
        value = throughCargo,
        key = "through_cargo"
      }
    });
    string eventName = "payment_forbidden";
    if (this._backPlatform.Contains(channel))
      eventName = "payment_platform_forbidden";
    FunplusBi.Instance.TraceEvent(eventName, properties);
  }

  public string GetPackageType()
  {
    string empty = string.Empty;
    switch (CustomDefine.Channel)
    {
      case CustomDefine.ChannelEnum.Amazon:
        return "amazon";
      case CustomDefine.ChannelEnum.Onestore:
        return "onestore";
      case CustomDefine.ChannelEnum.Samsung:
        return "samsung";
      case CustomDefine.ChannelEnum.Mycard:
      case CustomDefine.ChannelEnum.ALIPAY:
      case CustomDefine.ChannelEnum.WECHAT:
      case CustomDefine.ChannelEnum.OFFICIAL:
        return "official";
      case CustomDefine.ChannelEnum.SQWAN:
        return "cn37";
      default:
        return NativeManager.inst.GetOS();
    }
  }

  public string PaymentAppId
  {
    get
    {
      return this._paymentAppId;
    }
    set
    {
      this._paymentAppId = value;
    }
  }

  public string PaymentUrl
  {
    get
    {
      return "https://" + this._paymentURL;
    }
    set
    {
      this._paymentURL = value;
    }
  }

  private class PurchaseRecord
  {
    private string _productId = string.Empty;
    private string _serverId = string.Empty;
    private string _throughCargo = string.Empty;

    public PurchaseRecord(string productId, string serverId, string throughCargo)
    {
      this._productId = productId;
      this._serverId = serverId;
      this._throughCargo = throughCargo;
    }

    public string ProductId
    {
      get
      {
        return this._productId;
      }
    }

    public string ServerId
    {
      get
      {
        return this._serverId;
      }
    }

    public string ThroughCargo
    {
      get
      {
        return this._throughCargo;
      }
    }
  }

  public class BIStep
  {
    public string key;
    public string value;
  }

  public class BIDataFormat
  {
    public PaymentManager.BIStep d_c1;
    public PaymentManager.BIStep d_c2;
    public PaymentManager.BIStep d_c3;
  }
}
