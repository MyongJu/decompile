﻿// Decompiled with JetBrains decompiler
// Type: LegendPointInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class LegendPointInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "legend_level")]
  public int legendLevel;
  [Config(Name = "total_xp")]
  public int totalXP;
  [Config(Name = "power")]
  public int power;
  [Config(Name = "skill_points")]
  public int skillPoints;
}
