﻿// Decompiled with JetBrains decompiler
// Type: GveBossTroopRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class GveBossTroopRenderer : MonoBehaviour
{
  public UITexture m_TroopIcon;
  public UILabel m_TroopName;
  public UILabel m_TroopCount;

  public void SetData(UnitGroup.UnitRecord record)
  {
    Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(record.internalId);
    if (data == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_TroopIcon, data.Troop_ICON, (System.Action<bool>) null, true, false, string.Empty);
    this.m_TroopName.text = ScriptLocalization.Get(data.Troop_Name_LOC_ID, true);
    this.m_TroopCount.text = Utils.FormatThousands(record.value.ToString());
  }
}
