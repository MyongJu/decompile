﻿// Decompiled with JetBrains decompiler
// Type: IBuilder
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public interface IBuilder
{
  void Build(string image);

  void Release();

  System.Action<bool> OnCompleteHandler { get; set; }

  GameObject Target { get; set; }

  bool needTempIcon { get; set; }

  bool useWidgetSetting { get; set; }

  Texture CustomMissingTexture { get; set; }
}
