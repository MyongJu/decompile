﻿// Decompiled with JetBrains decompiler
// Type: MapUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MapUtils
{
  public static readonly string[] CityPrefabNames = new string[35]
  {
    "tiles_stronghold_lv1",
    "tiles_stronghold_lv2",
    "tiles_stronghold_lv3",
    "tiles_stronghold_lv4",
    "tiles_stronghold_lv5",
    "tiles_stronghold_lv6",
    "tiles_stronghold_lv7",
    "tiles_stronghold_lv8",
    "tiles_stronghold_lv9",
    "tiles_stronghold_lv10",
    "tiles_stronghold_lv11",
    "tiles_stronghold_lv12",
    "tiles_stronghold_lv13",
    "tiles_stronghold_lv14",
    "tiles_stronghold_lv15",
    "tiles_stronghold_lv16",
    "tiles_stronghold_lv17",
    "tiles_stronghold_lv18",
    "tiles_stronghold_lv19",
    "tiles_stronghold_lv20",
    "tiles_stronghold_lv21",
    "tiles_stronghold_lv22",
    "tiles_stronghold_lv23",
    "tiles_stronghold_lv24",
    "tiles_stronghold_lv25",
    "tiles_stronghold_lv26",
    "tiles_stronghold_lv27",
    "tiles_stronghold_lv28",
    "tiles_stronghold_lv29",
    "tiles_stronghold_lv30",
    "tiles_stronghold_lv31",
    "tiles_stronghold_lv32",
    "tiles_stronghold_lv33",
    "tiles_stronghold_lv34",
    "tiles_stronghold_lv35"
  };
  public static readonly string[] TilePrefabNames = new string[10]
  {
    "tiles_gve_camp",
    "tiles_digsite",
    "tiles_blank",
    "tiles_farm_l1_01",
    "tiles_lumber_l1_01",
    "tiles_mine_l1_01",
    "tiles_house_l1_01",
    "tiles_enemycroop_camp",
    "tiles_pit_mine",
    "tiles_pit_supper_mine"
  };
  private static Dictionary<int, string> AllAllianceBuildingPrefabNames = new Dictionary<int, string>()
  {
    {
      0,
      "tiles_alliance_fortress"
    },
    {
      1,
      "tiles_alliance_turret"
    },
    {
      2,
      "tiles_alliance_storehouse"
    },
    {
      3,
      "tiles_alliance_farm"
    },
    {
      4,
      "tiles_alliance_sawmil"
    },
    {
      5,
      "tiles_alliance_mine"
    },
    {
      6,
      "tiles_alliance_house"
    },
    {
      7,
      "tiles_alliance_portal"
    },
    {
      8,
      "tiles_alliance_hospital"
    },
    {
      9,
      "tiles_alliance_dragon_altar"
    }
  };
  public const int PITMAP_WORLD_START = 1000000;
  public const string PATH_PLAYER = "Prefab/Kingdom/MarchPathPlayer";
  public const string PATH_ENEMY = "Prefab/Kingdom/MarchPathEnemy";
  public const string PATH_ENEMY_DALTON = "Prefab/Kingdom/PathEnemyDalton";
  public const string PATH_ALLY = "Prefab/Kingdom/MarchPathAlly";
  public const string GOD_LIGHT = "Prefab/Kingdom/fx_god_light";
  public const string FOG = "Prefab/Kingdom/Fog";
  public const string TILE_HIGHLIGHT = "Prefab/Kingdom/4_TILE_HIGHLIGHT";

  public static string GetAllianceBuildingPrefabByBuildingId(int allianceBuildingId)
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(allianceBuildingId);
    if (buildingStaticInfo != null)
      return MapUtils.GetAllianceBuildingPrefab(buildingStaticInfo.type);
    throw new Exception(string.Format("PVPMap::GetAllianceBuildingPrefabByBuildingId({0}) -> invalid allianceBuildingId", (object) allianceBuildingId));
  }

  public static string GetAllianceBuildingPrefab(int allianceBuildingType)
  {
    string str = "tiles_alliance_fortress";
    MapUtils.AllAllianceBuildingPrefabNames.TryGetValue(allianceBuildingType, out str);
    return str;
  }

  public static void SetKingdomMap(object data)
  {
    ArrayList arrayList = data as ArrayList;
    if (arrayList == null)
      return;
    foreach (object obj in arrayList)
    {
      Hashtable ht = obj as Hashtable;
      if (ht != null)
      {
        int num = 1;
        if (ht.ContainsKey((object) "world_id"))
          num = int.Parse(ht[(object) "world_id"] as string);
        int x = 0;
        if (ht.ContainsKey((object) "map_x"))
          x = int.Parse(ht[(object) "map_x"] as string);
        int y = 0;
        if (ht.ContainsKey((object) "map_y"))
          y = int.Parse(ht[(object) "map_y"] as string);
        TileData tileAt = MapUtils.GetMapData(num).GetTileAt(num, x, y);
        if (tileAt != null)
        {
          tileAt.UpdateServerData(ht);
          tileAt.Show();
        }
      }
    }
  }

  public static void DeleteKingdomMap(object data)
  {
    ArrayList arrayList = data as ArrayList;
    if (arrayList == null)
      return;
    foreach (Hashtable hashtable in arrayList)
    {
      int num = 1;
      if (hashtable.ContainsKey((object) "world_id"))
        num = int.Parse(hashtable[(object) "world_id"] as string);
      int x = int.Parse(hashtable[(object) "map_x"] as string);
      int y = int.Parse(hashtable[(object) "map_y"] as string);
      TileData tileAt = MapUtils.GetMapData(num).GetTileAt(num, x, y);
      if (tileAt != null)
      {
        bool resetRefs = hashtable.ContainsKey((object) "X_REF") || hashtable.ContainsKey((object) "Y_REF");
        tileAt.ResetToTerrain(resetRefs);
      }
    }
  }

  private static DynamicMapData GetMapData(int worldId)
  {
    if (worldId > 1000000)
      return PitMapData.MapData;
    return PVPMapData.MapData;
  }

  public static void RegisterRouteMapping()
  {
    for (int index = 0; index < MapUtils.CityPrefabNames.Length; ++index)
      PrefabManagerEx.Instance.AddRouteMapping(MapUtils.CityPrefabNames[index], "Prefab/Tiles/" + MapUtils.CityPrefabNames[index]);
    using (Dictionary<int, string>.Enumerator enumerator = MapUtils.AllAllianceBuildingPrefabNames.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, string> current = enumerator.Current;
        PrefabManagerEx.Instance.AddRouteMapping(current.Value, "Prefab/Tiles/" + current.Value);
      }
    }
    for (int index = 0; index < MapUtils.TilePrefabNames.Length; ++index)
      PrefabManagerEx.Instance.AddRouteMapping(MapUtils.TilePrefabNames[index], "Prefab/Tiles/" + MapUtils.TilePrefabNames[index]);
    PrefabManagerEx.Instance.AddRouteMapping("MONSTER_TEST_0", "Prefab/Kingdom/MONSTER_TEST_0");
    PrefabManagerEx.Instance.AddRouteMapping("WORLD_BOSS", "Prefab/Kingdom/WORLD_BOSS");
    PrefabManagerEx.Instance.AddRouteMapping("PVPCityBanner", "Prefab/Kingdom/PVPCityBanner");
    PrefabManagerEx.Instance.AddRouteMapping("PVPEncounterBanner", "Prefab/Kingdom/PVPEncounterBanner");
    PrefabManagerEx.Instance.AddRouteMapping("PVPResourceBanner", "Prefab/Kingdom/PVPResourceBanner");
    PrefabManagerEx.Instance.AddRouteMapping("PVPSupperPitMineBanner", "Prefab/Kingdom/PVPSupperPitMineBanner");
    PrefabManagerEx.Instance.AddRouteMapping("PVPFortressBanner", "Prefab/Kingdom/PVPFortressBanner");
    PrefabManagerEx.Instance.AddRouteMapping("PVPSpriteIndicator", "Prefab/Kingdom/PVPSpriteIndicator");
    PrefabManagerEx.Instance.AddRouteMapping("PVPStoreHouseBanner", "Prefab/Kingdom/PVPStoreHouseBanner");
    PrefabManagerEx.Instance.AddRouteMapping("PVPSuperMineBanner", "Prefab/Kingdom/PVPSuperMineBanner");
    PrefabManagerEx.Instance.AddRouteMapping("PVPPortalBanner", "Prefab/Kingdom/PVPPortalBanner");
    PrefabManagerEx.Instance.AddRouteMapping("PVPCampBanner", "Prefab/Kingdom/PVPCampBanner");
    PrefabManagerEx.Instance.AddRouteMapping("PVPHospitalBanner", "Prefab/Kingdom/PVPHospitalBanner");
    PrefabManagerEx.Instance.AddRouteMapping("PVPTempleBanner", "Prefab/Kingdom/PVPTempleBanner");
    PrefabManagerEx.Instance.AddRouteMapping("PVPWorldBossBanner", "Prefab/Kingdom/PVPWorldBossBanner");
    PrefabManagerEx.Instance.AddRouteMapping("PVPWonderBanner", "Prefab/Kingdom/PVPWonderBanner");
    PrefabManagerEx.Instance.AddRouteMapping("PVPWonderTowerBanner", "Prefab/Kingdom/PVPWonderTowerBanner");
    PrefabManagerEx.Instance.AddRouteMapping("PVPDigsiteBanner", "Prefab/Kingdom/PVPDigsiteBanner");
    PrefabManagerEx.Instance.AddRouteMapping("PVPKingdomBossBanner", "Prefab/Kingdom/PVPKingdomBossBanner");
    Dictionary<int, string>.Enumerator enumerator1 = PVPMapData.MapData.DefaultAssets.GetEnumerator();
    while (enumerator1.MoveNext())
      PrefabManagerEx.Instance.AddRouteMapping(enumerator1.Current.Value, "Prefab/Default/" + enumerator1.Current.Value);
  }

  public static GameObject Instantiate(string path)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.Load(path, (System.Type) null) as GameObject);
    AssetManager.Instance.UnLoadAsset(path, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    return gameObject;
  }

  public static bool IsPitWorld(int worldId)
  {
    return worldId >= 1000000;
  }

  public static string GetCoordinateString(string kString, string xString, string yString)
  {
    int result1 = 0;
    int result2 = 0;
    int result3 = 0;
    int.TryParse(kString, out result1);
    int.TryParse(xString, out result2);
    int.TryParse(yString, out result3);
    return MapUtils.GetCoordinateString(result1, result2, result3);
  }

  public static string GetCoordinateString(int k, int x, int y)
  {
    if (MapUtils.IsPitWorld(k))
      return string.Format("{2} X:{0} Y:{1}", (object) x, (object) y, (object) Utils.XLAT("id_fire_kingdom"));
    return string.Format("K:{2} X:{0} Y:{1}", (object) x, (object) y, (object) k);
  }

  public static string GetCoordinateStringWithoutKXY(int k, int x, int y)
  {
    if (MapUtils.IsPitWorld(k))
      return string.Format("{2} {0}, {1}", (object) x, (object) y, (object) Utils.XLAT("id_fire_kingdom"));
    return string.Format("{2}, {0}, {1}", (object) x, (object) y, (object) k);
  }

  public static bool CanGotoTarget(int targetK)
  {
    bool flag = false;
    if (!MapUtils.IsPitWorld(PlayerData.inst.CityData.Location.K) && MapUtils.IsPitWorld(targetK))
      flag = true;
    if (MapUtils.IsPitWorld(PlayerData.inst.CityData.Location.K) && MapUtils.IsPitWorld(targetK) && PlayerData.inst.CityData.Location.K != targetK)
      flag = true;
    return !flag;
  }
}
