﻿// Decompiled with JetBrains decompiler
// Type: ConfigDKArenaParticipateCost
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDKArenaParticipateCost
{
  private ConfigParse parse = new ConfigParse();
  public Dictionary<string, DKArenaParticipateCost> datas;
  private Dictionary<int, DKArenaParticipateCost> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<DKArenaParticipateCost, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public int GetTotalTimes()
  {
    return this.datas.Count;
  }

  public DKArenaParticipateCost GetByTime(int time)
  {
    Dictionary<string, DKArenaParticipateCost>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.Times == time)
        return enumerator.Current;
    }
    return (DKArenaParticipateCost) null;
  }

  public int GetFreeTime()
  {
    int num = int.MinValue;
    Dictionary<string, DKArenaParticipateCost>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.Cost == 0)
        num = enumerator.Current.Times <= num ? num : enumerator.Current.Times;
    }
    return num;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, DKArenaParticipateCost>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, DKArenaParticipateCost>) null;
  }

  public bool Contains(int internalId)
  {
    return this.dicByUniqueId.ContainsKey(internalId);
  }

  public bool Contains(string id)
  {
    return this.datas.ContainsKey(id);
  }

  public DKArenaParticipateCost GetItem(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (DKArenaParticipateCost) null;
  }

  public DKArenaParticipateCost GetItem(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (DKArenaParticipateCost) null;
  }
}
