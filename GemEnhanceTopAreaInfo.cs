﻿// Decompiled with JetBrains decompiler
// Type: GemEnhanceTopAreaInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class GemEnhanceTopAreaInfo : MonoBehaviour
{
  [SerializeField]
  private UITexture _Texture;
  [SerializeField]
  private UILabel _GemName;
  [SerializeField]
  private GemEnhancePopupRowInfo _Level;
  [SerializeField]
  private GemEnhancePopupRowInfo _Benefit1;
  [SerializeField]
  private GemEnhancePopupRowInfo _Benefit2;
  [SerializeField]
  private GemEnhancePopupRowInfo _Benefit3;
  [SerializeField]
  private LevelUpSliderComponent _Slider;
  [SerializeField]
  public UITexture background;
  private GemData gemData;
  private ConfigEquipmentGemInfo gemConfig;
  private ItemStaticInfo gemItemConfig;
  private List<GemEnhancePopupRowInfo> benefitRows;

  private void Awake()
  {
    this.benefitRows = new List<GemEnhancePopupRowInfo>();
    this.benefitRows.Add(this._Benefit1);
    this.benefitRows.Add(this._Benefit2);
    this.benefitRows.Add(this._Benefit3);
  }

  public void Init(GemData gemData)
  {
    if (gemData == null)
      return;
    ConfigEquipmentGemInfo data = ConfigManager.inst.DB_EquipmentGem.GetData(gemData.ConfigId);
    if (data == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(data.itemID);
    if (itemStaticInfo == null)
      return;
    this.gemData = gemData;
    this.gemConfig = data;
    this.gemItemConfig = itemStaticInfo;
    if ((UnityEngine.Object) this._Texture != (UnityEngine.Object) null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this._Texture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      Utils.SetItemBackground(this.background, itemStaticInfo.internalId);
    }
    this.displayGemOriginInfo();
  }

  public void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this._Texture);
  }

  private void displayGemOriginInfo()
  {
    int level = this.gemData.Level;
    this._Level.Setup(ScriptLocalization.Get("id_level", true), this.gemConfig.level.ToString(), this.gemConfig.level.ToString(), false, false);
    this._GemName.text = this.gemItemConfig.LocName;
    List<Benefits.BenefitValuePair> benefitsPairs = this.gemConfig.benefits.GetBenefitsPairs();
    int index = 0;
    for (int count = this.benefitRows.Count; index < count; ++index)
    {
      NGUITools.SetActive(this.benefitRows[index].gameObject, benefitsPairs.Count > index);
      if (benefitsPairs.Count > index)
      {
        Benefits.BenefitValuePair benefitValuePair = benefitsPairs[index];
        this.benefitRows[index].Setup(benefitValuePair.propertyDefinition.Name, benefitValuePair.propertyDefinition.ConvertToDisplayString((double) benefitValuePair.value, false, true), benefitValuePair.propertyDefinition.ConvertToDisplayString((double) benefitValuePair.value, false, true), false, false);
      }
    }
    this._Slider.FeedData((IComponentData) new LevelUpSliderComponentData()
    {
      CurLevel = level,
      NewLevel = level,
      CurExp = this.gemData.Exp,
      NewExp = this.gemData.Exp,
      CurLevelExpMax = this.gemConfig.expReqToNext,
      NewLevelExpMax = this.gemConfig.expReqToNext,
      IsMaxLevel = GemManager.Instance.IsGemMaxLevel(this.gemConfig.internalId)
    });
  }

  private void displayGemGrowUpInfo(ConfigEquipmentGemInfo newGemConfig, long newExp)
  {
    if (newGemConfig == null)
    {
      D.error((object) "GemEnhanceTopAreaInfo displayGemGrowUpInfo error. Probably gem config error");
    }
    else
    {
      bool canLevelUp = this.gemConfig.internalId != newGemConfig.internalId;
      this._Level.Setup(ScriptLocalization.Get("id_level", true), this.gemConfig.level.ToString(), newGemConfig.level.ToString(), canLevelUp, true);
      List<Benefits.BenefitValuePair> benefitsPairs1 = this.gemConfig.benefits.GetBenefitsPairs();
      List<Benefits.BenefitValuePair> benefitsPairs2 = newGemConfig.benefits.GetBenefitsPairs();
      if (benefitsPairs1.Count > benefitsPairs2.Count)
      {
        D.error((object) "GemEnhanceTopAreaInfo displayGemGrowUpInfo error. Probably gem benefits config error");
      }
      else
      {
        int index = 0;
        for (int count = benefitsPairs2.Count; index < count; ++index)
        {
          NGUITools.SetActive(this.benefitRows[index].gameObject, benefitsPairs2.Count > index);
          if (benefitsPairs2.Count > index)
          {
            Benefits.BenefitValuePair benefitValuePair1 = (Benefits.BenefitValuePair) null;
            if (index < benefitsPairs1.Count)
              benefitValuePair1 = benefitsPairs1[index];
            Benefits.BenefitValuePair benefitValuePair2 = benefitsPairs2[index];
            this.benefitRows[index].Setup(benefitValuePair2.propertyDefinition.Name, benefitValuePair1.propertyDefinition.ConvertToDisplayString(benefitValuePair1 != null ? (double) benefitValuePair1.value : 0.0, false, true), benefitValuePair1.propertyDefinition.ConvertToDisplayString((double) benefitValuePair2.value, false, true), canLevelUp, true);
          }
        }
        this._Slider.FeedData((IComponentData) new LevelUpSliderComponentData()
        {
          CurLevel = this.gemConfig.level,
          NewLevel = newGemConfig.level,
          CurExp = this.gemData.Exp,
          NewExp = newExp,
          CurLevelExpMax = this.gemConfig.expReqToNext,
          NewLevelExpMax = newGemConfig.expReqToNext,
          IsMaxLevel = GemManager.Instance.IsGemMaxLevel(newGemConfig.internalId)
        });
      }
    }
  }

  public void UpdateUI(long addExp)
  {
    if (this.gemData == null || this.gemConfig == null || this.gemItemConfig == null)
      D.error((object) "GemEnhanceTopAreaInfo UpdateUI error. Probably gem config error");
    else if (addExp == 0L)
    {
      this.displayGemOriginInfo();
    }
    else
    {
      ConfigEquipmentGemInfo gemConfig = (ConfigEquipmentGemInfo) null;
      long curExp = 0;
      bool isMaxLevel = false;
      if (!GemManager.Instance.GetLeveledUpGemInfo(this.gemData, addExp, ref gemConfig, ref curExp, ref isMaxLevel))
        D.error((object) "GemEnhanceTopAreaInfo UpdateUI error. Probably gem config error");
      else
        this.displayGemGrowUpInfo(gemConfig, curExp);
    }
  }
}
