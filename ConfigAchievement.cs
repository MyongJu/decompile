﻿// Decompiled with JetBrains decompiler
// Type: ConfigAchievement
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigAchievement
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<int, List<AchievementInfo>> group2Datas = new Dictionary<int, List<AchievementInfo>>();
  public Dictionary<string, AchievementInfo> datas;
  private Dictionary<int, AchievementInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<AchievementInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public List<AchievementInfo> GetAchievementInfos(int groupId)
  {
    if (this.group2Datas.Count == 0)
    {
      List<AchievementInfo> achievementInfoList1 = new List<AchievementInfo>();
      Dictionary<int, AchievementInfo>.ValueCollection.Enumerator enumerator = this.dicByUniqueId.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        List<AchievementInfo> achievementInfoList2;
        if (this.group2Datas.ContainsKey(enumerator.Current.GroupID))
        {
          achievementInfoList2 = this.group2Datas[enumerator.Current.GroupID];
        }
        else
        {
          achievementInfoList2 = new List<AchievementInfo>();
          this.group2Datas.Add(enumerator.Current.GroupID, achievementInfoList2);
        }
        achievementInfoList2.Add(enumerator.Current);
      }
    }
    if (!this.group2Datas.ContainsKey(groupId))
      return (List<AchievementInfo>) null;
    this.group2Datas[groupId].Sort(new Comparison<AchievementInfo>(this.OnCompare));
    return this.group2Datas[groupId];
  }

  private int OnCompare(AchievementInfo a, AchievementInfo b)
  {
    return a.internalId.CompareTo(b.internalId);
  }

  public AchievementInfo GetLordItemAchievementInfo(AchievementInfo a)
  {
    List<AchievementInfo> achievementInfos = this.GetAchievementInfos(a.GroupID);
    for (int index = 0; index < achievementInfos.Count; ++index)
    {
      if (achievementInfos[index].Lord_Item > 0)
        return achievementInfos[index];
    }
    return (AchievementInfo) null;
  }

  public AchievementInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (AchievementInfo) null;
  }

  public AchievementInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AchievementInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, AchievementInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, AchievementInfo>) null;
  }
}
