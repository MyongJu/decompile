﻿// Decompiled with JetBrains decompiler
// Type: HeroInventoryInfoPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HeroInventoryInfoPopup : Popup
{
  private List<HeroBookBenefitItem> _itemBenefitList = new List<HeroBookBenefitItem>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private Color _benefitActiveColor = new Color(0.21f, 0.67f, 0.12f);
  [SerializeField]
  private ParliamentHeroCard _parliamentHeroCard;
  [SerializeField]
  private UILabel _heroTitleText;
  [SerializeField]
  private UILabel _heroScoreText;
  [SerializeField]
  private UILabel _heroLevel;
  [SerializeField]
  private UIProgressBar _levelProgressBar;
  [SerializeField]
  private UILabel _levelProgressText;
  [SerializeField]
  private UIButton _levelUpButton;
  [SerializeField]
  private ParliamentHeroStar _parliamentHeroStar;
  [SerializeField]
  private UIProgressBar _heroStarProgressBar;
  [SerializeField]
  private UILabel _heroStarProgressText;
  [SerializeField]
  private UIButton _heroStarUpgradeButton;
  [SerializeField]
  private UILabel _heroSuitName;
  [SerializeField]
  private GameObject _heroSuitNode;
  [SerializeField]
  private GameObject _heroSuitFrameNode;
  [SerializeField]
  private HeroBookBenefitItem _benefitItemPrefab;
  [SerializeField]
  private UITable _heroBaseBenefitTable;
  [SerializeField]
  private UITable _heroStarBenefitTable;
  [SerializeField]
  private UITable _heroSuitBenefitTable;
  [SerializeField]
  private UITable _heroContentTable;
  [SerializeField]
  private UIScrollView _heroContentScrollView;
  private LegendCardData _legendCardData;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    HeroInventoryInfoPopup.Parameter parameter = orgParam as HeroInventoryInfoPopup.Parameter;
    if (parameter != null)
      this._legendCardData = parameter.legendCardData;
    this.AddEventHandler();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.ClearData();
  }

  public void OnCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnLevelUpgradeClicked()
  {
    if (this._legendCardData == null)
      return;
    UIManager.inst.OpenPopup("HeroCard/HeroCardLevelUpPopup", (Popup.PopupParameter) new HeroCardLevelUpPopup.Parameter()
    {
      legendCardData = this._legendCardData
    });
  }

  public void OnStarUpgradeClicked()
  {
    if (this._legendCardData == null)
      return;
    ParliamentHeroStarInfo parliamentHeroStarInfo = ConfigManager.inst.DB_ParliamentHeroStar.Get(HeroCardUtils.GetCurrentStars(this._legendCardData.LegendId).ToString());
    if (parliamentHeroStarInfo == null)
      return;
    if (this._legendCardData.Level >= parliamentHeroStarInfo.nextNeedLevel)
    {
      int currentFragments = HeroCardUtils.GetCurrentFragments(this._legendCardData.LegendId);
      int nextStarFragments = HeroCardUtils.GetNextStarFragments(this._legendCardData.LegendId);
      if (currentFragments >= nextStarFragments)
      {
        MessageHub.inst.GetPortByAction("Legend:upgradeStar").SendRequest(new Hashtable()
        {
          {
            (object) "legend_id",
            (object) this._legendCardData.LegendId
          }
        }, (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          this.ShowStarUpgradeEffect();
        }), true);
      }
      else
      {
        string str1 = Utils.XLAT("id_uppercase_okay");
        string str2 = Utils.XLAT("hero_fragments_too_low_title");
        string withPara = ScriptLocalization.GetWithPara("hero_fragments_too_low_description", new Dictionary<string, string>()
        {
          {
            "0",
            (nextStarFragments - currentFragments).ToString()
          },
          {
            "1",
            this._legendCardData.HeroFragmentName
          }
        }, true);
        UIManager.inst.OpenPopup("Marksman/CommonConfirmPopup", (Popup.PopupParameter) new CommonConfirmPopup.Parameter()
        {
          title = str2,
          content = withPara,
          confirmButtonText = str1
        });
      }
    }
    else
    {
      string str1 = Utils.XLAT("id_uppercase_okay");
      string str2 = Utils.XLAT("hero_level_too_low_title");
      string withPara = ScriptLocalization.GetWithPara("hero_level_too_low_description", new Dictionary<string, string>()
      {
        {
          "0",
          parliamentHeroStarInfo.nextNeedLevel.ToString()
        }
      }, true);
      UIManager.inst.OpenPopup("Marksman/CommonConfirmPopup", (Popup.PopupParameter) new CommonConfirmPopup.Parameter()
      {
        title = str2,
        content = withPara,
        confirmButtonText = str1
      });
    }
  }

  public void OnHeroSuitClicked()
  {
    if (this._legendCardData == null)
      return;
    UIManager.inst.OpenPopup("HeroCard/HeroCardSuitInfoPopup", (Popup.PopupParameter) new HeroCardSuitInfoPopup.Parameter()
    {
      legendCardData = this._legendCardData
    });
  }

  private void UpdateUI()
  {
    this.ClearData();
    this._parliamentHeroCard.SetData(this._legendCardData);
    this.UpdateHeroBaseInfo();
    this.UpdateHeroStarInfo();
    this.UpdateHeroSuitInfo();
    this._heroContentTable.Reposition();
    this._heroContentScrollView.ResetPosition();
  }

  private void UpdateHeroBaseInfo()
  {
    if (this._legendCardData == null)
      return;
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this._legendCardData.LegendId);
    if (parliamentHeroInfo != null)
    {
      ParliamentInfo parliamentInfo = ConfigManager.inst.DB_Parliament.Get(parliamentHeroInfo.parliamentPosition);
      if (parliamentInfo != null)
        this._heroTitleText.text = parliamentInfo.Name;
    }
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("0", this._legendCardData.Level.ToString());
    para.Add("1", HeroCardUtils.GetHeroMaxLevel(this._legendCardData.LegendId).ToString());
    this._heroLevel.text = ScriptLocalization.GetWithPara("hero_xp_level_num", para, true);
    para.Clear();
    para.Add("0", this._legendCardData.Score.ToString());
    this._heroScoreText.text = ScriptLocalization.GetWithPara("hero_score_level_num", para, true);
    if (this._legendCardData.IsMaxLevel)
    {
      this._levelProgressBar.value = 1f;
      this._levelProgressText.text = Utils.XLAT("id_uppercase_maxed");
    }
    else
    {
      long requiredExpToNextLevel = HeroCardUtils.GetRequiredExpToNextLevel(this._legendCardData.LegendId, this._legendCardData.Level);
      this._levelProgressBar.value = Mathf.Clamp01((float) this._legendCardData.CurrentLevelXP / (float) requiredExpToNextLevel);
      this._levelProgressText.text = string.Format("{0}/{1}", (object) this._legendCardData.CurrentLevelXP, (object) requiredExpToNextLevel);
    }
    NGUITools.SetActive(this._levelUpButton.gameObject, !this._legendCardData.IsMaxLevel);
    this.UpdateHeroBaseBenefit();
  }

  private void UpdateHeroBaseBenefit()
  {
    if (this._legendCardData == null)
      return;
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this._legendCardData.LegendId);
    HeroBookBenefitItem heroBookBenefitItem = (HeroBookBenefitItem) null;
    if (parliamentHeroInfo != null)
    {
      heroBookBenefitItem = this.CreateBenefitItem(this._heroBaseBenefitTable.transform, Utils.XLAT("hero_group_benefit_current_level"), parliamentHeroInfo.levelBenefit, parliamentHeroInfo.GetLevelBenefitByLevel(this._legendCardData.Level));
      heroBookBenefitItem.SetTextColor(this._benefitActiveColor);
      if (!this._legendCardData.IsMaxLevel)
      {
        heroBookBenefitItem = this.CreateBenefitItem(this._heroBaseBenefitTable.transform, Utils.XLAT("hero_group_benefit_next_level"), parliamentHeroInfo.levelBenefit, parliamentHeroInfo.GetLevelBenefitByLevel(this._legendCardData.Level + 1));
        heroBookBenefitItem.SetTextColor(Color.grey);
      }
    }
    if ((UnityEngine.Object) heroBookBenefitItem != (UnityEngine.Object) null)
      heroBookBenefitItem.SetLineEnabled(false);
    this._heroBaseBenefitTable.Reposition();
  }

  private void UpdateHeroStarInfo()
  {
    if (this._legendCardData == null)
      return;
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this._legendCardData.LegendId);
    if (parliamentHeroInfo != null)
    {
      this._parliamentHeroStar.SetData(parliamentHeroInfo, this._legendCardData, false);
      if (HeroCardUtils.IsHeroStarFull(this._legendCardData.LegendId))
      {
        this._heroStarProgressBar.value = 1f;
        this._heroStarProgressText.text = Utils.XLAT("id_uppercase_maxed");
      }
      else
      {
        int currentFragments = HeroCardUtils.GetCurrentFragments(this._legendCardData.LegendId);
        int nextStarFragments = HeroCardUtils.GetNextStarFragments(this._legendCardData.LegendId);
        this._heroStarProgressBar.value = (float) currentFragments / (float) nextStarFragments;
        this._heroStarProgressText.text = string.Format("{0}/{1}", (object) currentFragments, (object) nextStarFragments);
      }
      NGUITools.SetActive(this._heroStarUpgradeButton.gameObject, !HeroCardUtils.IsHeroStarFull(this._legendCardData.LegendId));
    }
    this.UpdateHeroStarBenefit();
  }

  private void UpdateHeroStarBenefit()
  {
    if (this._legendCardData == null)
      return;
    HeroBookBenefitItem heroBookBenefitItem = (HeroBookBenefitItem) null;
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this._legendCardData.LegendId);
    Dictionary<string, string> para = new Dictionary<string, string>();
    int currentStars = HeroCardUtils.GetCurrentStars(this._legendCardData.LegendId);
    for (int index = 0; index < parliamentHeroInfo.AllStarBenefit.Length; ++index)
    {
      para.Remove("0");
      para.Add("0", parliamentHeroInfo.AllStarRequirement[index].ToString());
      if (parliamentHeroInfo.AllStarBenefit[index] != 0)
      {
        heroBookBenefitItem = this.CreateBenefitItem(this._heroStarBenefitTable.transform, ScriptLocalization.GetWithPara("hero_star_level_num", para, true), parliamentHeroInfo.AllStarBenefit[index], parliamentHeroInfo.AllStarBenefitValue[index]);
        if (currentStars >= parliamentHeroInfo.AllStarRequirement[index])
          heroBookBenefitItem.SetTextColor(this._benefitActiveColor);
        else
          heroBookBenefitItem.SetTextColor(Color.grey);
      }
    }
    if ((UnityEngine.Object) heroBookBenefitItem != (UnityEngine.Object) null)
      heroBookBenefitItem.SetLineEnabled(false);
    this._heroStarBenefitTable.Reposition();
  }

  private void UpdateHeroSuitInfo()
  {
    if (this._legendCardData == null)
      return;
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this._legendCardData.LegendId);
    if (parliamentHeroInfo == null)
      return;
    ParliamentSuitGroupInfo parliamentSuitGroupInfo = ConfigManager.inst.DB_ParliamentSuitGroup.Get(parliamentHeroInfo.heroSuitGroup);
    if (parliamentSuitGroupInfo != null)
    {
      HeroBookBenefitItem heroBookBenefitItem = (HeroBookBenefitItem) null;
      int num1 = Mathf.Min(HeroCardUtils.GetStartSuitCount(HeroCardUtils.GetCurrentHeroSuitActiveCount(this._legendCardData.LegendId), parliamentSuitGroupInfo.AllSuitRequirement), Mathf.Max(parliamentSuitGroupInfo.AllSuitRequirement));
      string[] strArray = new string[3]
      {
        "hero_group_benefit_current_level",
        "hero_group_benefit_next_level",
        "hero_group_benefit_max_level"
      };
      int num2 = 0;
      int num3 = 3;
      int index1 = 0;
      for (int index2 = 0; index2 < parliamentSuitGroupInfo.AllSuitBenefit.Length; ++index2)
      {
        if (parliamentSuitGroupInfo.AllSuitBenefit[index2] != 0)
        {
          if (num1 <= parliamentSuitGroupInfo.AllSuitRequirement[index2])
          {
            Dictionary<string, string> para = new Dictionary<string, string>()
            {
              {
                "0",
                parliamentSuitGroupInfo.AllSuitRequirement[index2].ToString()
              },
              {
                "1",
                parliamentSuitGroupInfo.Name
              }
            };
            index1 = index2;
            heroBookBenefitItem = this.CreateBenefitItem(this._heroSuitBenefitTable.transform, Utils.XLAT(strArray[num2++]) + ScriptLocalization.GetWithPara("hero_appoint_num_group_benefits", para, true), parliamentSuitGroupInfo.AllSuitBenefit[index2], parliamentSuitGroupInfo.AllSuitBenefitValue[index2]);
            if (num1 == parliamentSuitGroupInfo.AllSuitRequirement[index2] && num2 == 1)
              heroBookBenefitItem.SetTextColor(this._benefitActiveColor);
          }
          if (num3 == num2)
            break;
        }
      }
      if (num2 > 0 && num2 < num3)
      {
        heroBookBenefitItem = this.CreateBenefitItem(this._heroSuitBenefitTable.transform, Utils.XLAT("hero_group_benefit_max_level") + ScriptLocalization.GetWithPara("hero_appoint_num_group_benefits", new Dictionary<string, string>()
        {
          {
            "0",
            parliamentSuitGroupInfo.AllSuitRequirement[index1].ToString()
          },
          {
            "1",
            parliamentSuitGroupInfo.Name
          }
        }, true), parliamentSuitGroupInfo.AllSuitBenefit[index1], parliamentSuitGroupInfo.AllSuitBenefitValue[index1]);
        if (num1 == parliamentSuitGroupInfo.AllSuitRequirement[index1])
          heroBookBenefitItem.SetTextColor(this._benefitActiveColor);
      }
      if ((UnityEngine.Object) heroBookBenefitItem != (UnityEngine.Object) null)
        heroBookBenefitItem.SetLineEnabled(false);
      this._heroSuitName.text = ScriptLocalization.GetWithPara("hero_appoint_group_benefits_name", new Dictionary<string, string>()
      {
        {
          "0",
          parliamentSuitGroupInfo.Name
        }
      }, true);
      this._heroSuitBenefitTable.Reposition();
    }
    else
    {
      NGUITools.SetActive(this._heroSuitNode, false);
      NGUITools.SetActive(this._heroSuitFrameNode, false);
    }
  }

  private HeroBookBenefitItem CreateBenefitItem(Transform parent, string prefix, int benefitId, float value)
  {
    this._itemPool.Initialize(this._benefitItemPrefab.gameObject, parent.gameObject);
    GameObject gameObject = this._itemPool.AddChild(parent.gameObject);
    gameObject.SetActive(true);
    HeroBookBenefitItem component = gameObject.GetComponent<HeroBookBenefitItem>();
    this._itemBenefitList.Add(component);
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitId];
    string str1 = dbProperty == null ? "unknow" : dbProperty.Name;
    string str2 = dbProperty == null ? value.ToString() : dbProperty.ConvertToDisplayString((double) value, false, true);
    if (string.IsNullOrEmpty(prefix))
      component.SetData(string.Format("{0} {1}", (object) str1, (object) str2));
    else
      component.SetData(string.Format("{0} {1} {2}", (object) prefix, (object) str1, (object) str2));
    return component;
  }

  private void ClearData()
  {
    for (int index = 0; index < this._itemBenefitList.Count; ++index)
    {
      GameObject gameObject = this._itemBenefitList[index].gameObject;
      gameObject.SetActive(false);
      this._itemPool.Release(gameObject);
    }
    this._itemBenefitList.Clear();
    this._itemPool.Clear();
  }

  private void ShowStarUpgradeEffect()
  {
    if (this._legendCardData == null)
      return;
    Transform child1 = this._parliamentHeroStar.transform.GetChild(this._legendCardData.Star);
    if (!(bool) ((UnityEngine.Object) child1))
      return;
    Transform child2 = child1.GetChild(0);
    if (!(bool) ((UnityEngine.Object) child2))
      return;
    AudioManager.Instance.StopAndPlaySound("sfx_city_gemstone_refine");
    NGUITools.SetActive(child2.gameObject, true);
  }

  private void OnLegendCardDataUpdated(LegendCardData legendCardData)
  {
    if (legendCardData.Uid != PlayerData.inst.uid)
      return;
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_LegendCard.onDataUpdate += new System.Action<LegendCardData>(this.OnLegendCardDataUpdated);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_LegendCard.onDataUpdate -= new System.Action<LegendCardData>(this.OnLegendCardDataUpdated);
  }

  public class Parameter : Popup.PopupParameter
  {
    public LegendCardData legendCardData;
  }
}
