﻿// Decompiled with JetBrains decompiler
// Type: AllianceChangeRequirementHeader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;

public class AllianceChangeRequirementHeader : AllianceCustomizeHeader
{
  public UIInput m_CityLevelInput;
  public UIInput m_PowerInput;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    this.m_PowerInput.value = this.allianceData.powerLimit.ToString();
    this.m_CityLevelInput.value = this.allianceData.levelLimit.ToString();
  }

  public void OnSave()
  {
    AllianceManager.Instance.ChangeJoinLimits(this.GetLevelLimit(), this.GetPowerLimit(), new System.Action<bool, object>(this.SetupAllianceCallback));
  }

  public void OnLevelLimitChanged()
  {
    if (string.IsNullOrEmpty(this.m_CityLevelInput.value))
      return;
    this.m_CityLevelInput.value = Math.Min(Math.Max(1L, this.GetLevelLimit()), 30L).ToString();
  }

  private long GetPowerLimit()
  {
    long result = 0;
    long.TryParse(this.m_PowerInput.value, out result);
    return result;
  }

  private long GetLevelLimit()
  {
    long result = 0;
    long.TryParse(this.m_CityLevelInput.value, out result);
    return result;
  }

  private void SetupAllianceCallback(bool ret, object data)
  {
    if (!ret)
      return;
    UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_recruitment_requirements_changed"), (System.Action) null, 4f, false);
  }
}
