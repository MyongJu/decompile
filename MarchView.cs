﻿// Decompiled with JetBrains decompiler
// Type: MarchView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MarchView : MonoBehaviour
{
  protected string ACTION_COMMANDER = string.Empty;
  protected const string ROTATE_DIR_X = "x";
  protected const string ROTATE_DIR_Y = "y";
  protected Transform _trans;
  protected Animator animator;
  protected TweenAlpha fadeTween;
  protected MarchViewControler.Data data;
  protected bool isLoading;

  public bool isInited { get; private set; }

  public void Init(System.Action callback)
  {
    if (!this.isInited)
    {
      this.isLoading = true;
      this.CreateModel((System.Action) (() =>
      {
        this.isLoading = false;
        if (callback == null)
          return;
        callback();
      }));
      this.isInited = true;
    }
    else
    {
      if (callback == null || this.isLoading)
        return;
      callback();
    }
  }

  public virtual void CreateModel(System.Action callback)
  {
    this.CreateModelFromResource((System.Action<GameObject>) (go =>
    {
      this._trans = this.transform;
      go.transform.parent = this.transform;
      go.transform.localPosition = Vector3.zero;
      go.transform.localScale = Vector3.one * 0.3f;
      this.animator = go.GetComponent<Animator>();
      this.fadeTween = go.GetComponent<TweenAlpha>();
      Renderer componentInChildren = go.GetComponentInChildren<Renderer>();
      if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
      {
        componentInChildren.sortingOrder = 29999;
        componentInChildren.sortingLayerName = "March";
      }
      if (callback == null)
        return;
      callback();
    }));
  }

  public virtual void CreateModelFromResource(System.Action<GameObject> callback)
  {
    if (callback == null)
      return;
    AssetManager.Instance.LoadAsync("Prefab/TroopModels/Scout_Walk", (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      if (ret && this.isLoading)
        callback(UnityEngine.Object.Instantiate<GameObject>(obj as GameObject));
      AssetManager.Instance.UnLoadAsset("Prefab/TroopModels/Scout_Walk", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    }), (System.Type) null);
  }

  public virtual void Show()
  {
    this.FadeIn(Vector3.zero, 0.0f, true);
  }

  public virtual void Hide()
  {
    this.FadeOut(0.0f, true);
  }

  public virtual void FadeIn(Vector3 showPosition, float delayTime = 0, bool isDisplayByLine = true)
  {
    if ((UnityEngine.Object) this.fadeTween != (UnityEngine.Object) null)
    {
      this.fadeTween.delay = 0.0f;
      this.fadeTween.PlayForward();
    }
    else
      this._trans.gameObject.SetActive(true);
  }

  public virtual void FadeOut(float delayTime = 0, bool isDisplayByLine = true)
  {
    if ((UnityEngine.Object) this.fadeTween != (UnityEngine.Object) null)
    {
      this.fadeTween.delay = 0.0f;
      this.fadeTween.PlayReverse();
    }
    else
      this._trans.gameObject.SetActive(false);
  }

  public virtual float Setup(MarchViewControler.Data troopData, bool setImd, System.Action callback = null)
  {
    this.data = troopData;
    this.ACTION_COMMANDER = "attack";
    if (callback != null)
      callback();
    return 1f;
  }

  public virtual void Rotate(Vector3 dir)
  {
    Vector2 vector2 = MarchViewControler.CalcVector3(dir);
    if (!((UnityEngine.Object) this.animator != (UnityEngine.Object) null))
      return;
    this._trans.gameObject.SetActive(true);
    this._trans.localScale = new Vector3(Mathf.Abs(this._trans.localScale.x) * ((double) vector2.x <= 0.0 ? 1f : -1f), this._trans.localScale.y, this._trans.localScale.z);
    this.animator.SetInteger("x", (int) vector2.x);
    this.animator.SetInteger("y", (int) vector2.y);
  }

  public virtual void Move()
  {
    this._trans.gameObject.SetActive(true);
    if (!((UnityEngine.Object) this.animator != (UnityEngine.Object) null))
      return;
    this.animator.SetBool(this.ACTION_COMMANDER, false);
  }

  public virtual void EndMove()
  {
    this._trans.gameObject.SetActive(true);
    if (!this.gameObject.activeSelf || !((UnityEngine.Object) this.animator != (UnityEngine.Object) null))
      return;
    this.animator.SetInteger("x", 0);
    this.animator.SetInteger("y", 0);
  }

  public virtual void Action(int actionIndex, Vector3 deltaPos, float radius = 1f)
  {
    if (!this.gameObject.activeSelf || !((UnityEngine.Object) this.animator != (UnityEngine.Object) null))
      return;
    this.animator.SetBool(this.ACTION_COMMANDER, false);
  }

  public virtual void Dispose()
  {
    this.isLoading = false;
  }
}
