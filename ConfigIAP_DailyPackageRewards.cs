﻿// Decompiled with JetBrains decompiler
// Type: ConfigIAP_DailyPackageRewards
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigIAP_DailyPackageRewards
{
  private Dictionary<string, IAPDailyPackageRewardsInfo> m_DataByID;
  private Dictionary<int, IAPDailyPackageRewardsInfo> m_DataByInternalID;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<IAPDailyPackageRewardsInfo, string>(result as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
  }

  public IAPDailyPackageRewardsInfo Get(string id)
  {
    IAPDailyPackageRewardsInfo packageRewardsInfo;
    this.m_DataByID.TryGetValue(id, out packageRewardsInfo);
    return packageRewardsInfo;
  }

  public IAPDailyPackageRewardsInfo Get(int internalId)
  {
    IAPDailyPackageRewardsInfo packageRewardsInfo;
    this.m_DataByInternalID.TryGetValue(internalId, out packageRewardsInfo);
    return packageRewardsInfo;
  }

  public void Clear()
  {
    if (this.m_DataByID != null)
    {
      this.m_DataByID.Clear();
      this.m_DataByID = (Dictionary<string, IAPDailyPackageRewardsInfo>) null;
    }
    if (this.m_DataByInternalID == null)
      return;
    this.m_DataByInternalID.Clear();
    this.m_DataByInternalID = (Dictionary<int, IAPDailyPackageRewardsInfo>) null;
  }
}
