﻿// Decompiled with JetBrains decompiler
// Type: CacheInfoEx
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class CacheInfoEx
{
  private Stack<GameObject> m_CacheStack = new Stack<GameObject>();
  public string prefabName;
  public string assetPath;
  public CacheInfoExState state;
  private GameObject m_Prefab;
  private Transform m_RootNode;

  public void Init(GameObject prefab, Transform root)
  {
    this.m_Prefab = prefab;
    this.m_RootNode = root;
  }

  public GameObject prefab
  {
    get
    {
      return this.m_Prefab;
    }
  }

  public GameObject Allocate()
  {
    if (!(bool) ((UnityEngine.Object) this.m_Prefab))
      return (GameObject) null;
    GameObject go = (GameObject) null;
    if (this.HasCachedGameObject)
      go = this.m_CacheStack.Pop();
    if (!(bool) ((UnityEngine.Object) go))
      go = UnityEngine.Object.Instantiate<GameObject>(this.m_Prefab);
    if (!(bool) ((UnityEngine.Object) go))
      return (GameObject) null;
    go.name = this.prefabName;
    CacheInfoEx.InitializeComponents(go);
    return go;
  }

  public bool HasCachedGameObject
  {
    get
    {
      return this.m_CacheStack.Count > 0;
    }
  }

  public void Clear()
  {
    while (this.m_CacheStack.Count > 0)
    {
      GameObject gameObject = this.m_CacheStack.Pop();
      if ((bool) ((UnityEngine.Object) gameObject))
        UnityEngine.Object.Destroy((UnityEngine.Object) gameObject);
    }
    AssetManager.Instance.UnLoadAsset(this.assetPath, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
  }

  public void Recycle(GameObject go)
  {
    if (!(bool) ((UnityEngine.Object) go))
      return;
    CacheInfoEx.FinalizeComponents(go);
    go.transform.parent = this.m_RootNode;
    this.m_CacheStack.Push(go);
  }

  private static void InitializeComponents(GameObject go)
  {
    if (!(bool) ((UnityEngine.Object) go))
      return;
    foreach (Component component in go.GetComponents(typeof (IRecycle)))
    {
      IRecycle recycle = component as IRecycle;
      if (recycle != null)
        recycle.OnInitialize();
    }
  }

  private static void FinalizeComponents(GameObject go)
  {
    if (!(bool) ((UnityEngine.Object) go))
      return;
    foreach (Component component in go.GetComponents(typeof (IRecycle)))
    {
      IRecycle recycle = component as IRecycle;
      if (recycle != null)
        recycle.OnFinalize();
    }
  }
}
