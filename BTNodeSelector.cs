﻿// Decompiled with JetBrains decompiler
// Type: BTNodeSelector
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class BTNodeSelector : BTNodeContainer
{
  public override void Execute()
  {
    this.Status = BTNode.NodeStatus.Running;
    for (int index = 0; index < this._children.Count; ++index)
    {
      this._children[index].Execute();
      if (this._children[index].Status == BTNode.NodeStatus.Success)
      {
        this.Status = BTNode.NodeStatus.Success;
        break;
      }
    }
    if (this.Status != BTNode.NodeStatus.Running)
      return;
    this.Status = BTNode.NodeStatus.Failure;
  }
}
