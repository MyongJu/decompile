﻿// Decompiled with JetBrains decompiler
// Type: GroupQuestUncomplete
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class GroupQuestUncomplete : MonoBehaviour
{
  public UILabel title;
  public UIGrid grid;
  public GroupQuestRender template;
  public UIScrollView scrollView;
  public UILabel descriptionLabel;

  public void UpdateUI()
  {
    List<QuestData2> groupQuests = DBManager.inst.DB_Quest.GroupQuests;
    this.descriptionLabel.text = Utils.XLAT(ConfigManager.inst.DB_QuestGroup.GetData(ConfigManager.inst.DB_Quest.GetData((int) groupQuests[0].QuestID).groupId).loc_name_id);
    this.ClearGrid();
    this.scrollView.ResetPosition();
    for (int index = 0; index < groupQuests.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.template.gameObject);
      gameObject.SetActive(true);
      gameObject.GetComponent<GroupQuestRender>().UpdateUI(groupQuests[index]);
    }
    this.title.text = string.Format(Utils.XLAT("advanced_tutorial_uppercase_title") + " ({0}/{1})", (object) DBManager.inst.DB_Quest.GroupQuestCompleteCount(), (object) DBManager.inst.DB_Quest.GroupQuests.Count);
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.grid.Reposition()));
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.grid);
  }
}
