﻿// Decompiled with JetBrains decompiler
// Type: SuperLoginEntranceUI
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class SuperLoginEntranceUI : MonoBehaviour
{
  public string activityId;

  private void Start()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void OnDestroy()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  private void OnSecondEvent(int time)
  {
    NGUITools.SetActive(this.gameObject, SuperLoginPayload.Instance.CanActivityShow(this.activityId));
  }

  public void OnEntranceClicked()
  {
    string activityId = this.activityId;
    if (activityId == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (SuperLoginEntranceUI.\u003C\u003Ef__switch\u0024mapA9 == null)
    {
      // ISSUE: reference to a compiler-generated field
      SuperLoginEntranceUI.\u003C\u003Ef__switch\u0024mapA9 = new Dictionary<string, int>(2)
      {
        {
          "super_log_in_christmas",
          0
        },
        {
          "super_log_in_1_year_celebration",
          1
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!SuperLoginEntranceUI.\u003C\u003Ef__switch\u0024mapA9.TryGetValue(activityId, out num))
      return;
    if (num != 0)
    {
      if (num != 1)
        return;
      UIManager.inst.OpenPopup("SuperLogin/FirstAnniversaryPopup", (Popup.PopupParameter) null);
    }
    else
      UIManager.inst.OpenPopup("SuperLogin/SuperLoginChristmasEvePopup", (Popup.PopupParameter) null);
  }
}
