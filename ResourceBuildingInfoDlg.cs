﻿// Decompiled with JetBrains decompiler
// Type: ResourceBuildingInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;

public class ResourceBuildingInfoDlg : BuildingInfoBaseDlg
{
  public UITexture texture1;
  public UILabel nameLabel1;
  public UILabel valueLabel1;
  public UITexture texture2;
  public UILabel nameLabel2;
  public UILabel valueLabel2;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    BuildingInfo buildingInfo = this.buildingInfo;
    PropertyDefinition dbProperty1 = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_1)];
    this.nameLabel1.text = dbProperty1.Name;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.texture1, dbProperty1.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    double generateInHour = (double) buildingInfo.GenerateInHour;
    BuildingControllerResource fromBuildingItem = CitadelSystem.inst.GetBuildControllerFromBuildingItem(this.buildingItem) as BuildingControllerResource;
    float num = (float) fromBuildingItem.GenerateSpeed * 3600f;
    string str1 = (double) num < generateInHour ? "-" : "+";
    this.valueLabel1.text = "[B4B4B4]" + Utils.ConvertNumberToNormalString((long) generateInHour) + "[-][54AE07]" + str1 + Utils.ConvertNumberToNormalString((long) Math.Abs((double) num - generateInHour)) + "[-]";
    PropertyDefinition dbProperty2 = ConfigManager.inst.DB_Properties[int.Parse(buildingInfo.Benefit_ID_2)];
    this.nameLabel2.text = dbProperty2.Name;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.texture2, dbProperty2.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    double storage = (double) buildingInfo.Storage;
    float storageMaxValue = (float) fromBuildingItem.StorageMaxValue;
    string str2 = (double) storageMaxValue < storage ? "-" : "+";
    this.valueLabel2.text = "[B4B4B4]" + Utils.ConvertNumberToNormalString((long) storage) + "[-][54AE07]" + str2 + Utils.ConvertNumberToNormalString((long) Math.Abs((double) storageMaxValue - storage)) + "[-]";
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    BuilderFactory.Instance.Release((UIWidget) this.texture1);
    BuilderFactory.Instance.Release((UIWidget) this.texture2);
    base.OnClose(orgParam);
  }
}
