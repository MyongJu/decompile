﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerBuyBuffConfirmPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;

public class MerlinTowerBuyBuffConfirmPopup : Popup
{
  private const string BUY_BENEFITS = "tower_buy_benefits_title";
  private const string BUY_BENEFITS_MOST_DES = "tower_buy_benefits_floor_description";
  private const string BUY_BENEFITS_TROOP = "tower_buy_benefits_troop_floor_description";
  public UILabel title;
  public UILabel benefitName;
  public UITexture benefitIcon;
  public UILabel confirm;
  public UILabel confirmLabel;
  public UILabel cancelLabel;
  private MerlinTowerBuyBuffConfirmPopup.Parameter param;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    this.param = orgParam as MerlinTowerBuyBuffConfirmPopup.Parameter;
    if (this.param == null)
      return;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.title.text = Utils.XLAT("tower_buy_benefits_title");
    this.benefitName.text = this.param.benefitName;
    this.cancelLabel.text = Utils.XLAT("id_uppercase_cancel");
    this.confirmLabel.text = Utils.XLAT("id_uppercase_confirm");
    BuilderFactory.Instance.HandyBuild((UIWidget) this.benefitIcon, this.param.benefitIcon, (System.Action<bool>) null, true, false, string.Empty);
    Dictionary<string, string> para = new Dictionary<string, string>();
    if (this.param.type == 2)
    {
      para.Clear();
      para.Add("0", this.param.price.ToString());
      para.Add("1", Utils.GetBenefitValueString(this.param.format, (double) this.param.value));
      this.confirm.text = ScriptLocalization.GetWithPara("tower_buy_benefits_floor_description", para, true);
    }
    else
    {
      para.Clear();
      para.Add("0", this.param.benefitName);
      para.Add("1", this.param.price.ToString());
      this.confirm.text = ScriptLocalization.GetWithPara("tower_buy_benefits_troop_floor_description", para, true);
    }
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnConfirmBtnClicked()
  {
    if (this.param.onBuyClicked == null)
      return;
    this.param.onBuyClicked();
    this.OnCloseBtnClicked();
  }

  public void OnCancelBtnClicked()
  {
    this.OnCloseBtnClicked();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
  }

  public class Parameter : Popup.PopupParameter
  {
    public string benefitName;
    public string benefitIcon;
    public int type;
    public int price;
    public float value;
    public PropertyDefinition.FormatType format;
    public System.Action onBuyClicked;
  }
}
