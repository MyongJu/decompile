﻿// Decompiled with JetBrains decompiler
// Type: IAPDailyRechargePackage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAPDailyRechargePackage
{
  private const string GOOGLE_CHANEL = "google";
  public int internalID;
  public string id;
  public int type;
  public string pay_id;
  public long discount;
  public int start_time;
  public int end_time;
  public string reward_group_id;
  public string sub_reward_group_id;
  public IAPDailyPackageRewardsInfo rewards;
  public IAPDailyPackageRewardsInfo sub_rewards;

  public string productId
  {
    get
    {
      IAPPlatformInfo iapPlatformInfo = ConfigManager.inst.DB_IAPPlatform.Get(Application.platform, this.pay_id);
      if (iapPlatformInfo != null)
        return iapPlatformInfo.product_id;
      return string.Empty;
    }
  }

  public void Decode(Hashtable data)
  {
    DatabaseTools.UpdateData(data, "id", ref this.id);
    DatabaseTools.UpdateData(data, "Internal_ID", ref this.internalID);
    DatabaseTools.UpdateData(data, "type", ref this.type);
    DatabaseTools.UpdateData(data, "pay_id", ref this.pay_id);
    DatabaseTools.UpdateData(data, "start_time", ref this.start_time);
    DatabaseTools.UpdateData(data, "end_time", ref this.end_time);
    DatabaseTools.UpdateData(data, "discount", ref this.discount);
    DatabaseTools.UpdateData(data, "reward_group_id", ref this.reward_group_id);
    DatabaseTools.UpdateData(data, "sub_reward_group_id", ref this.sub_reward_group_id);
    if (data.ContainsKey((object) "rewards"))
    {
      this.rewards = JsonReader.Deserialize<IAPDailyPackageRewardsInfo>(Utils.Object2Json(data[(object) "rewards"]));
      this.ResortRewards(data, this.rewards, "reward_ids");
    }
    if (!(CustomDefine.GetChannel() == "google") && Application.platform != RuntimePlatform.IPhonePlayer && (Application.platform != RuntimePlatform.OSXEditor && Application.platform != RuntimePlatform.WindowsEditor) || !data.ContainsKey((object) "sub_rewards"))
      return;
    this.sub_rewards = JsonReader.Deserialize<IAPDailyPackageRewardsInfo>(Utils.Object2Json(data[(object) "sub_rewards"]));
    this.ResortRewards(data, this.sub_rewards, "sub_reward_ids");
  }

  private void ResortRewards(Hashtable data, IAPDailyPackageRewardsInfo info, string key)
  {
    if (!data.ContainsKey((object) key))
      return;
    ArrayList arrayList = data[(object) key] as ArrayList;
    Dictionary<string, int> dictionary = new Dictionary<string, int>();
    for (int index = 0; index < arrayList.Count; ++index)
    {
      if (info.Rewards.ContainsKey(arrayList[index].ToString()))
        dictionary.Add(arrayList[index].ToString(), info.Rewards[arrayList[index].ToString()]);
    }
    info.Rewards = (Dictionary<string, int>) null;
    info.Rewards = dictionary;
  }
}
