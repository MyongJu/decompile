﻿// Decompiled with JetBrains decompiler
// Type: AttackShowTargetCity
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AttackShowTargetCity : MonoBehaviour
{
  public PVPAllianceSymbol allianceSymbol;
  public TextMesh levelLabel;
  public TextMesh nameLabel;
  private PrefabSpawnRequestEx _request;
  private GameObject _userCity;

  public void FeedData(AttackShowPara para)
  {
    Mathf.Min(Mathf.Max(0, para.targetLevel - 1), MapUtils.CityPrefabNames.Length - 1);
    this._request = PrefabManagerEx.Instance.SpawnAsync(MapUtils.CityPrefabNames[para.targetLevel - 1], this.transform, new PrefabSpawnRequestCallback(this.OnTileAssetLoaded), (object) null);
    if (para.targetAllianceCode == -1)
    {
      this.allianceSymbol.gameObject.SetActive(false);
    }
    else
    {
      this.allianceSymbol.gameObject.SetActive(true);
      this.allianceSymbol.SetSymbols(para.targetAllianceCode);
    }
    this.levelLabel.text = para.targetLevel.ToString();
    this.nameLabel.text = para.targetName;
  }

  private void OnTileAssetLoaded(GameObject go, object userData)
  {
    this._request = (PrefabSpawnRequestEx) null;
    if (!(bool) ((Object) go))
      return;
    this._userCity = go;
    go.transform.localPosition = new Vector3(0.0f, 90f, 0.0f);
    go.transform.localScale = Vector3.one;
    Utils.SetLayer(go, LayerMask.NameToLayer("Actor"));
    foreach (SpriteRenderer componentsInChild in go.GetComponentsInChildren<SpriteRenderer>())
      componentsInChild.sortingOrder += 5;
  }

  public void Dispose()
  {
    if (this._request == null)
    {
      PrefabManagerEx.Instance.Destroy(this._userCity);
    }
    else
    {
      this._request.cancel = true;
      this._request = (PrefabSpawnRequestEx) null;
    }
  }
}
