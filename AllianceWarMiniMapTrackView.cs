﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarMiniMapTrackView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class AllianceWarMiniMapTrackView : MonoBehaviour
{
  private Dictionary<int, AllianceWarMiniMapTrackSlot> _itemDict = new Dictionary<int, AllianceWarMiniMapTrackSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UILabel _labelTrackingDescription;
  [SerializeField]
  private UISprite _spriteFrame;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UIGrid _grid;
  [SerializeField]
  private GameObject _itemPrefab;
  private List<long> _trackedAllianceIdList;
  private bool _isAllianceWarMiniMapTrackViewExplanded;

  public bool IsAllianceWarMiniMapTrackViewExplanded
  {
    get
    {
      return this._isAllianceWarMiniMapTrackViewExplanded;
    }
  }

  public void Init(List<long> trackedAllianceIdList)
  {
    this._trackedAllianceIdList = trackedAllianceIdList;
    NGUITools.SetActive(this._spriteFrame.gameObject, false);
    NGUITools.SetActive(this.gameObject, true);
    this.UpdateMainUI();
    this.ClearData();
  }

  public void Show()
  {
    this.OnExpanded();
  }

  public void Hide()
  {
    this.OnCollapsed();
  }

  public void OnExpanded()
  {
    NGUITools.SetActive(this._spriteFrame.gameObject, true);
    this._isAllianceWarMiniMapTrackViewExplanded = true;
    this.UpdateArrow();
    this.UpdateUI();
    this.UpdateMainUI();
  }

  public void OnCollapsed()
  {
    NGUITools.SetActive(this._spriteFrame.gameObject, false);
    this._isAllianceWarMiniMapTrackViewExplanded = false;
    this.UpdateArrow();
    this.UpdateMainUI();
    this.ClearData();
  }

  private void ClearData()
  {
    using (Dictionary<int, AllianceWarMiniMapTrackSlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, AllianceWarMiniMapTrackSlot> current = enumerator.Current;
        current.Value.OnAllianceTracked -= new System.Action<AllianceData>(this.OnAllianceTracked);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this._grid.repositionNow = true;
    this._grid.Reposition();
    this._scrollView.ResetPosition();
  }

  private AllianceWarMiniMapTrackSlot GenerateSlot()
  {
    this._itemPool.Initialize(this._itemPrefab, this._grid.gameObject);
    this._itemPrefab.SetActive(false);
    GameObject gameObject = this._itemPool.AddChild(this._grid.gameObject);
    gameObject.SetActive(true);
    return gameObject.GetComponent<AllianceWarMiniMapTrackSlot>();
  }

  private void OnAllianceTracked(AllianceData oppAllianceData)
  {
    if (oppAllianceData == null)
      return;
    this.OnCollapsed();
  }

  private void UpdateUI()
  {
    this.ClearData();
    if (this._trackedAllianceIdList != null)
    {
      this._trackedAllianceIdList.Sort((Comparison<long>) ((a, b) =>
      {
        AllianceData allianceData1 = DBManager.inst.DB_Alliance.Get(a);
        AllianceData allianceData2 = DBManager.inst.DB_Alliance.Get(b);
        if (allianceData1 != null && allianceData2 != null)
          allianceData1.worldId.CompareTo(allianceData1.worldId);
        return 0;
      }));
      long trackedAllianceId = AllianceWarPayload.Instance.GetTrackedAllianceId();
      long num = !this._trackedAllianceIdList.Contains(trackedAllianceId) ? this._trackedAllianceIdList[0] : trackedAllianceId;
      for (int key = 0; key < this._trackedAllianceIdList.Count; ++key)
      {
        AllianceData oppAllianceData = DBManager.inst.DB_Alliance.Get(this._trackedAllianceIdList[key]);
        if (oppAllianceData != null)
        {
          AllianceWarMiniMapTrackSlot slot = this.GenerateSlot();
          slot.SetData(oppAllianceData, num == oppAllianceData.allianceId);
          slot.OnAllianceTracked += new System.Action<AllianceData>(this.OnAllianceTracked);
          this._itemDict.Add(key, slot);
        }
      }
    }
    this.Reposition();
  }

  private void UpdateMainUI()
  {
    if (this._trackedAllianceIdList.Count <= 0)
    {
      this._labelTrackingDescription.text = Utils.XLAT("alliance_warfare_no_tracking_yet_description");
    }
    else
    {
      long trackedAllianceId = AllianceWarPayload.Instance.GetTrackedAllianceId();
      long defaultShowAllianceId = !this._trackedAllianceIdList.Contains(trackedAllianceId) ? this._trackedAllianceIdList[0] : trackedAllianceId;
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(defaultShowAllianceId);
      this._labelTrackingDescription.text = allianceData == null ? Utils.XLAT("alliance_warfare_no_tracking_yet_description") : string.Format("K{0}.({1}){2}", (object) allianceData.worldId, (object) allianceData.allianceAcronym, (object) allianceData.allianceName);
      using (List<long>.Enumerator enumerator = this._trackedAllianceIdList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if (enumerator.Current == defaultShowAllianceId)
            AllianceWarPayload.Instance.LoadTrack(defaultShowAllianceId, (System.Action<bool, object>) ((ret, data) =>
            {
              if (!ret || !((UnityEngine.Object) this != (UnityEngine.Object) null))
                return;
              MiniMapSystem.Instance.ShowAllianceWarEnemy();
              AllianceWarPayload.Instance.SetTrackedAlliance(defaultShowAllianceId);
            }));
        }
      }
    }
  }

  private void UpdateArrow()
  {
    Transform child = this.transform.FindChild("Tracking/arrow");
    if (!(bool) ((UnityEngine.Object) child))
      return;
    child.localRotation = Quaternion.Euler(0.0f, 0.0f, !this._isAllianceWarMiniMapTrackViewExplanded ? 0.0f : -90f);
  }
}
