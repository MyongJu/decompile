﻿// Decompiled with JetBrains decompiler
// Type: LogConsole
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LogConsole : MonoBehaviour
{
  public float width = 500f;
  public float height = 300f;
  public float itemHeight = 30f;
  public int maxCount = 200;
  private int _logLevel = 2;
  private string _logFilter;
  private Vector2 _scrollPosition;

  private void Awake()
  {
    this._logLevel = D.Level;
    this._logFilter = D.Filter;
    Object.DontDestroyOnLoad((Object) this.gameObject);
  }

  private void LogItemRenderer(LogPipeline.LogItem item)
  {
    if (GUILayout.Button(item.logTitle, GUILayout.Width(this.width), GUILayout.Height(this.itemHeight)))
      item.isExpand = !item.isExpand;
    if (!item.isExpand)
      return;
    if (item.type == LogType.Exception)
      GUILayout.Label(string.Format("<color=#FF0000>[Exception]{0}\n{1}</color>", (object) item.logString, (object) item.stackTrace), GUILayout.Width(this.width));
    else
      GUILayout.Label(item.logString, GUILayout.Width(this.width));
  }
}
