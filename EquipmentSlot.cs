﻿// Decompiled with JetBrains decompiler
// Type: EquipmentSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class EquipmentSlot : ItemSlot_New
{
  [SerializeField]
  private EquipmentSlot.EquipmentPanel equipmentPanel;
  private bool _isAddDisable;
  private bool _isAddEnable;

  public bool isAddDisableShow
  {
    get
    {
      return this._isAddDisable;
    }
    set
    {
      this._isAddDisable = value;
      if (this._isAddDisable)
        this.isAddEnableShow = false;
      this.equipmentPanel.AddDisable.SetActive(this._isAddDisable);
    }
  }

  public bool isAddEnableShow
  {
    get
    {
      return this._isAddEnable;
    }
    set
    {
      this._isAddEnable = value;
      if (this._isAddEnable)
        this.isAddDisableShow = false;
      this.equipmentPanel.AddEnable.SetActive(this._isAddEnable);
    }
  }

  [Serializable]
  protected class EquipmentPanel
  {
    public GameObject AddDisable;
    public GameObject AddEnable;
    public GameObject Sihouette;
    public GameObject EquipmentInfoContainer;
  }
}
