﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;

public class AnniversaryManager
{
  private Dictionary<int, AnniversaryManager.AnniversarySlotData> _slots = new Dictionary<int, AnniversaryManager.AnniversarySlotData>();
  private static AnniversaryManager _instance;
  private HubPort _dataChangePort;
  private int _currentPoint;
  private bool _kickOut;
  private AnniversaryManager.AnniversaryData _anniversaryData;
  private int _endTime;
  private bool _isReady;
  private int _slot;

  public event System.Action OnDataUpdate;

  public event System.Action<int> OnPointChange;

  public event System.Action OnFetchDataError;

  public event System.Action<bool> OnChallengeFinish;

  public static AnniversaryManager Instance
  {
    get
    {
      if (AnniversaryManager._instance == null)
        AnniversaryManager._instance = new AnniversaryManager();
      return AnniversaryManager._instance;
    }
  }

  public void Initialize()
  {
    if (this.IsFinish())
      return;
    this._dataChangePort = MessageHub.inst.GetPortByAction("anniversary_kings_changed");
    this._dataChangePort.AddEvent(new System.Action<object>(this.OnServerDataChanged));
    this.FetchKingsDataFromLoader();
  }

  private void OnServerDataChanged(object data)
  {
    this.Parse(data);
  }

  public void OnGameReady()
  {
    UIManager.inst.splashScreen.onSplashScreenHide -= new System.Action(this.OnGameReady);
    if (UIManager.inst.IsDialogClear && UIManager.inst.IsPopupClear)
    {
      this.OnWindowClear();
    }
    else
    {
      UIManager.inst.OnWindowClear -= new System.Action(this.OnWindowClear);
      UIManager.inst.OnWindowClear += new System.Action(this.OnWindowClear);
    }
  }

  private void OnWindowClear()
  {
    UIManager.inst.OnWindowClear -= new System.Action(this.OnWindowClear);
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.CityMode)
      return;
    Utils.ExecuteInSecs(1f / 1000f, (System.Action) (() =>
    {
      if (SuperLoginPayload.Instance.SuperLoginDataDict.Count <= 0)
        SuperLoginPayload.Instance.RequestServerData((System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          this.CheckFirstAnniversary();
        }), false);
      else
        this.CheckFirstAnniversary();
    }));
  }

  private void CheckFirstAnniversary()
  {
    if (!AnniversaryIapPayload.Instance.IsOpen)
      return;
    List<SuperLoginMainInfo> mainInfoListByGroup = ConfigManager.inst.DB_SuperLoginMain.GetSuperLoginMainInfoListByGroup("super_log_in_1_year_celebration");
    if (mainInfoListByGroup != null)
      mainInfoListByGroup.Sort((Comparison<SuperLoginMainInfo>) ((a, b) => a.day.CompareTo(b.day)));
    if (SuperLoginPayload.Instance.CanActivityShow("super_log_in_1_year_celebration") && mainInfoListByGroup != null && (SuperLoginPayload.Instance.CurrentDay > 0 && SuperLoginPayload.Instance.CurrentDay <= mainInfoListByGroup.Count) && mainInfoListByGroup[SuperLoginPayload.Instance.CurrentDay - 1].RewardState != SuperLoginPayload.DailyRewardState.HAS_GOT)
    {
      UIManager.inst.OpenPopup("SuperLogin/FirstAnniversaryPopup", (Popup.PopupParameter) null);
    }
    else
    {
      if (!AnniversaryIapPayload.Instance.ShouldShowAnnvIap)
        return;
      AnniversaryIapPayload.Instance.RequestServerData((System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        UIManager.inst.OpenDlg("Anniversary/AnniversaryIapMainDlg", (UI.Dialog.DialogParameter) null, true, true, true);
      }));
    }
  }

  private void CheckWorkship()
  {
    int count = 0;
    int num = 6;
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("anniversary_champion_worship_city_level_limit");
    if (data != null)
      num = data.ValueInt;
    if (PlayerData.inst.CityData.mStronghold.mLevel >= num)
    {
      for (int index = 0; index < 3; ++index)
      {
        int slot = index + 1;
        int workshipTime = AnniversaryManager.Instance.Data.GetWorkshipTime(slot);
        AnniversaryManager.AnniversarySlotData slotData = AnniversaryManager.Instance.GetSlotData(slot);
        if (slotData.Uid != PlayerData.inst.uid && NetServerTime.inst.ServerTimestamp - workshipTime >= 86400 && (slotData != null && slotData.Uid < 6000000000L))
          ++count;
      }
    }
    if (count > 0)
      this.AddPoint(count);
    if (this.OnPointChange == null)
      return;
    this.OnPointChange(this._currentPoint);
  }

  public void Dispose()
  {
    this.OnDataUpdate = (System.Action) null;
    this.OnChallengeFinish = (System.Action<bool>) null;
    this.OnFetchDataError = (System.Action) null;
    this.OnPointChange = (System.Action<int>) null;
    this._currentPoint = 0;
    if (this._dataChangePort == null)
      return;
    this._dataChangePort.RemoveEvent(new System.Action<object>(this.OnServerDataChanged));
    this._dataChangePort = (HubPort) null;
  }

  public bool KickOut
  {
    get
    {
      return this._kickOut;
    }
    set
    {
      this._kickOut = value;
    }
  }

  public int RedPoint
  {
    get
    {
      return this._currentPoint;
    }
  }

  public void AddPoint(int count = 1)
  {
    if (count <= 0)
      return;
    this._currentPoint += count;
    if (this.OnPointChange == null)
      return;
    this.OnPointChange(this._currentPoint);
  }

  public void RemovePoint(int count = 1)
  {
    if (count <= 0)
      return;
    this._currentPoint -= count;
    if (this.OnPointChange == null)
      return;
    this.OnPointChange(this._currentPoint);
  }

  public bool IsStart()
  {
    return AnniversaryIapPayload.Instance.TournamentStartTime < (long) NetServerTime.inst.ServerTimestamp;
  }

  public bool IsFinish()
  {
    return this.EndTime <= NetServerTime.inst.ServerTimestamp;
  }

  public bool IsReady
  {
    get
    {
      return this._isReady;
    }
  }

  public int EndTime
  {
    get
    {
      return this._endTime;
    }
    set
    {
      this._endTime = value;
      if (this.OnDataUpdate == null)
        return;
      this.OnDataUpdate();
    }
  }

  public AnniversaryManager.AnniversarySlotData GetSlotData(int slot)
  {
    if (this._slots.ContainsKey(slot))
      return this._slots[slot];
    return (AnniversaryManager.AnniversarySlotData) null;
  }

  public AnniversaryManager.AnniversaryData Data
  {
    get
    {
      return this._anniversaryData;
    }
  }

  public void SetUp(object orgData)
  {
    this.Parse(orgData);
  }

  public int GetKingSlot(long uid)
  {
    Dictionary<int, AnniversaryManager.AnniversarySlotData>.KeyCollection.Enumerator enumerator = this._slots.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (this._slots[enumerator.Current].Uid == PlayerData.inst.uid)
        return this._slots[enumerator.Current].Slot;
    }
    return 0;
  }

  public bool IsKing()
  {
    Dictionary<int, AnniversaryManager.AnniversarySlotData>.KeyCollection.Enumerator enumerator = this._slots.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (this._slots[enumerator.Current].Uid == PlayerData.inst.uid)
        return true;
    }
    return false;
  }

  private void Parse(object orgData)
  {
    Hashtable hashtable1 = orgData as Hashtable;
    if (hashtable1 == null)
      return;
    if (hashtable1.ContainsKey((object) "end_time") && hashtable1[(object) "end_time"] != null)
      int.TryParse(hashtable1[(object) "end_time"].ToString(), out this._endTime);
    if (hashtable1.ContainsKey((object) "mine") && hashtable1[(object) "mine"] != null)
    {
      this._anniversaryData = new AnniversaryManager.AnniversaryData();
      this._anniversaryData.Decode(hashtable1[(object) "mine"] as Hashtable);
    }
    if (hashtable1.ContainsKey((object) "kings") && hashtable1[(object) "kings"] != null)
    {
      this._currentPoint = 0;
      bool flag = this.IsKing();
      Hashtable hashtable2 = hashtable1[(object) "kings"] as Hashtable;
      if (hashtable2 != null && hashtable2.Count > 0)
      {
        this._slots.Clear();
        IEnumerator enumerator = hashtable2.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          string s = enumerator.Current.ToString();
          int result = 0;
          int.TryParse(s, out result);
          object obj = hashtable2[enumerator.Current];
          AnniversaryManager.AnniversarySlotData anniversarySlotData = new AnniversaryManager.AnniversarySlotData();
          if (anniversarySlotData.Decode(obj as Hashtable))
          {
            anniversarySlotData.Slot = result;
            if (!this._slots.ContainsKey(result))
              this._slots.Add(result, anniversarySlotData);
          }
        }
        if (flag && !this.IsKing() && !UIManager.inst.IsDlgExist("Anniversary/AnniversaryMainDlg"))
        {
          this._kickOut = true;
          this.AddPoint(1);
        }
        this.CheckWorkship();
      }
    }
    this._isReady = true;
    if (this.OnDataUpdate == null)
      return;
    this.OnDataUpdate();
  }

  public void FetchKingsDataFromLoader()
  {
    RequestManager.inst.SendLoader("anniversary:getKingsConflictList", Utils.Hash((object) "uid", (object) PlayerData.inst.uid), new System.Action<bool, object>(this.OnFetchDataCallBack), true, false);
  }

  public void FetchKingsData()
  {
    RequestManager.inst.SendRequest("anniversary:getKingsConflictList", Utils.Hash((object) "uid", (object) PlayerData.inst.uid), new System.Action<bool, object>(this.OnFetchDataCallBack), true);
  }

  private void OnFetchDataCallBack(bool success, object payload)
  {
    if (success)
      this.Parse(payload);
    else
      this.FireError();
  }

  private void FireError()
  {
    if (this.OnFetchDataError == null)
      return;
    this.OnFetchDataError();
  }

  public void Workship(int slot)
  {
    this._slot = slot;
    RequestManager.inst.SendRequest("anniversary:worshipKing", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) nameof (slot), (object) slot), new System.Action<bool, object>(this.OnWorkshipCallBack), true);
  }

  private void OnWorkshipCallBack(bool success, object payload)
  {
    if (success)
    {
      AnniversaryChampionMainInfo data = ConfigManager.inst.DB_AnniversaryChampionMain.GetData(this._slot.ToString() + string.Empty);
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_1_year_tournament_praise_success", true), (System.Action) null, 4f, false);
      AnniversaryChampionRewardInfo championRewardInfo = ConfigManager.inst.DB_AnniversaryChampionRewards.GetItem(this.IsFinish() ? data.WorshipRewardId : data.AnnvWorshipRewardId);
      if (championRewardInfo != null)
      {
        RewardsCollectionAnimator.Instance.Clear();
        List<Reward.RewardsValuePair> rewards = championRewardInfo.Rewards.GetRewards();
        for (int index = 0; index < rewards.Count; ++index)
        {
          int internalId = rewards[index].internalID;
          int num = rewards[index].value;
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
          RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
          {
            count = (float) num,
            icon = itemStaticInfo.ImagePath
          });
        }
        RewardsCollectionAnimator.Instance.CollectResource(false);
        RewardsCollectionAnimator.Instance.CollectItems(false);
      }
      this.RemovePoint(1);
      this.Parse(payload);
    }
    else
      this.FireError();
  }

  public void Challenge(int slot, long uid, Hashtable troops)
  {
    if (this.GetSlotData(slot).Uid != uid)
      return;
    RequestManager.inst.SendRequest("anniversary:challengeKing", Utils.Hash((object) nameof (uid), (object) PlayerData.inst.uid, (object) nameof (slot), (object) slot, (object) "kuid", (object) uid, (object) nameof (troops), (object) troops), new System.Action<bool, object>(this.OnWorkshipCallBack), true);
  }

  private void OnChallengeCallBack(bool success, object payload)
  {
    if (success)
      this.Parse(payload);
    else
      this.FireError();
    if (this.OnChallengeFinish == null)
      return;
    this.OnChallengeFinish(success);
  }

  public void SaveChallengeTroops(Hashtable troops)
  {
    RequestManager.inst.SendRequest("anniversary:saveKingsConflictTroops", Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) nameof (troops), (object) troops), new System.Action<bool, object>(this.OnSaveTroopsCallBack), true);
  }

  private void OnSaveTroopsCallBack(bool success, object payload)
  {
    if (success)
      this.Parse(payload);
    else
      this.FireError();
  }

  public class AnniversarySlotData
  {
    private string _name = string.Empty;
    public string Icon = string.Empty;
    public int Slot = -1;
    public string AllianceTag = string.Empty;
    public long Uid;
    public int WorldId;
    public int Portrait;
    public int LordTitleId;
    public long WorshippedCount;
    public long TroopCount;
    public long TroopPower;
    public int CityLevel;
    public int UpdateTime;
    public int AllianceSymbol;

    public string Name
    {
      get
      {
        string str = this._name + ".K" + (object) this.WorldId;
        if (!string.IsNullOrEmpty(this.AllianceTag))
          str = "(" + this.AllianceTag + ")" + this._name + ".K" + (object) this.WorldId;
        return str;
      }
    }

    public bool Decode(Hashtable orgData)
    {
      if (orgData == null)
        return false;
      bool flag = false;
      if (orgData.ContainsKey((object) "uid") && orgData[(object) "uid"] != null)
      {
        long.TryParse(orgData[(object) "uid"].ToString(), out this.Uid);
        flag = true;
      }
      if (orgData.ContainsKey((object) "acronym") && orgData[(object) "acronym"] != null)
        this.AllianceTag = orgData[(object) "acronym"].ToString();
      if (orgData.ContainsKey((object) "symbol_code") && orgData[(object) "symbol_code"] != null)
      {
        int.TryParse(orgData[(object) "symbol_code"].ToString(), out this.AllianceSymbol);
        flag = true;
      }
      if (orgData.ContainsKey((object) "world_id") && orgData[(object) "world_id"] != null)
      {
        int.TryParse(orgData[(object) "world_id"].ToString(), out this.WorldId);
        flag = true;
      }
      if (orgData.ContainsKey((object) "updated_at") && orgData[(object) "updated_at"] != null)
      {
        int.TryParse(orgData[(object) "updated_at"].ToString(), out this.UpdateTime);
        flag = true;
      }
      if (orgData.ContainsKey((object) "city_level") && orgData[(object) "city_level"] != null)
      {
        int.TryParse(orgData[(object) "city_level"].ToString(), out this.CityLevel);
        flag = true;
      }
      if (orgData.ContainsKey((object) "worshipped_count") && orgData[(object) "worshipped_count"] != null)
      {
        long.TryParse(orgData[(object) "worshipped_count"].ToString(), out this.WorshippedCount);
        flag = true;
      }
      if (orgData.ContainsKey((object) "portrait") && orgData[(object) "portrait"] != null)
      {
        int.TryParse(orgData[(object) "portrait"].ToString(), out this.Portrait);
        flag = true;
      }
      if (orgData.ContainsKey((object) "lord_title") && orgData[(object) "lord_title"] != null)
      {
        int.TryParse(orgData[(object) "lord_title"].ToString(), out this.LordTitleId);
        flag = true;
      }
      if (orgData.ContainsKey((object) "troop_count") && orgData[(object) "troop_count"] != null)
      {
        long.TryParse(orgData[(object) "troop_count"].ToString(), out this.TroopCount);
        flag = true;
      }
      if (orgData.ContainsKey((object) "troop_power") && orgData[(object) "troop_power"] != null)
      {
        long.TryParse(orgData[(object) "troop_power"].ToString(), out this.TroopPower);
        flag = true;
      }
      if (orgData.ContainsKey((object) "name") && orgData[(object) "name"] != null)
        this._name = orgData[(object) "name"].ToString();
      if (orgData.ContainsKey((object) "icon") && orgData[(object) "icon"] != null)
        this.Icon = orgData[(object) "icon"].ToString();
      return flag;
    }
  }

  public class AnniversaryData
  {
    private List<int> _workshipTimes = new List<int>();
    private List<int> _challengeTimes = new List<int>();
    private Dictionary<string, int> _troops = new Dictionary<string, int>();
    private bool _isWithDragon = true;
    private int _worshippedCount;

    public bool IsWithDragon
    {
      get
      {
        return this._isWithDragon;
      }
    }

    public Dictionary<string, int> GetTroops()
    {
      return this._troops;
    }

    public int GetWorkshipTime(int slot)
    {
      int index = slot - 1;
      if (index > -1 && this._workshipTimes.Count > index)
        return this._workshipTimes[index];
      return 0;
    }

    public int GetChallengeTime(int slot)
    {
      int index = slot - 1;
      if (index > -1 && this._challengeTimes.Count > index)
        return this._challengeTimes[index];
      return 0;
    }

    public bool Decode(Hashtable orgData)
    {
      if (orgData == null)
        return false;
      bool flag = false;
      if (orgData.ContainsKey((object) "wor_1") && orgData[(object) "wor_1"] != null)
      {
        this._workshipTimes.Clear();
        for (int index = 0; index < 3; ++index)
        {
          string str = "wor_" + (object) (index + 1);
          if (orgData.ContainsKey((object) str) && orgData[(object) str] != null)
          {
            string s = orgData[(object) str].ToString();
            int result = 0;
            if (int.TryParse(s, out result))
              this._workshipTimes.Add(result);
          }
        }
        flag = true;
      }
      if (orgData.ContainsKey((object) "cha_1") && orgData[(object) "cha_1"] != null)
      {
        this._challengeTimes.Clear();
        for (int index = 0; index < 3; ++index)
        {
          string str = "cha_" + (object) (index + 1);
          if (orgData.ContainsKey((object) str) && orgData[(object) str] != null)
          {
            string s = orgData[(object) str].ToString();
            int result = 0;
            if (int.TryParse(s, out result))
              this._challengeTimes.Add(result);
          }
        }
        flag = true;
      }
      if (orgData.ContainsKey((object) "worshipped_count") && orgData[(object) "worshipped_count"] != null)
      {
        int.TryParse(orgData[(object) "worshipped_count"].ToString(), out this._worshippedCount);
        flag = true;
      }
      if (orgData.ContainsKey((object) "dragon") && orgData[(object) "dragon"] != null)
      {
        string s = orgData[(object) "dragon"].ToString();
        int result = -1;
        int.TryParse(s, out result);
        this._isWithDragon = result == 1;
      }
      if (orgData.ContainsKey((object) "troops") && orgData[(object) "troops"] != null)
      {
        Hashtable hashtable = orgData[(object) "troops"] as Hashtable;
        if (hashtable != null && hashtable.Keys.Count > 0)
        {
          this._troops.Clear();
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
          {
            string key = enumerator.Current.ToString();
            string s = hashtable[enumerator.Current].ToString();
            int result = 0;
            if (int.TryParse(s, out result) && !this._troops.ContainsKey(key))
              this._troops.Add(key, result);
          }
          flag = true;
        }
      }
      return flag;
    }
  }
}
