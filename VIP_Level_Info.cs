﻿// Decompiled with JetBrains decompiler
// Type: VIP_Level_Info
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class VIP_Level_Info : IGameData, IComparable<VIP_Level_Info>
{
  private static readonly string levelIdPrefix = "vip_level_";
  public List<Effect> Effects = new List<Effect>();
  public string req_id_1;
  public int req_value_1;
  private int _internalID;
  private string _id;
  private int _level;

  public VIP_Level_Info(int internalID, string id, string req_id, int req_value)
  {
    this._internalID = internalID;
    this._id = id;
    this.req_id_1 = req_id;
    this.req_value_1 = req_value;
  }

  public int InternalID
  {
    get
    {
      return this._internalID;
    }
  }

  public string ID
  {
    get
    {
      return this._id;
    }
  }

  public string Name
  {
    get
    {
      return this._id;
    }
  }

  public int Level
  {
    get
    {
      if (this._level == 0)
        this._level = int.Parse(this._id.Substring(VIP_Level_Info.levelIdPrefix.Length));
      return this._level;
    }
  }

  public int CompareTo(VIP_Level_Info other)
  {
    if (other == null)
      return 1;
    return this.req_value_1.CompareTo(other.req_value_1);
  }

  public bool ContainsPrivilege(string id)
  {
    for (int index = 0; index < this.Effects.Count; ++index)
    {
      PrivilegeInfo privilege = this.Effects[index].Privilege;
      if (privilege != null && privilege.id == id)
        return true;
    }
    return false;
  }

  public Effect GetSameEffect(Effect other)
  {
    for (int index = 0; index < this.Effects.Count; ++index)
    {
      if (this.Effects[index].PropertyInternalID == other.PropertyInternalID && other.PropertyInternalID != 0 || this.Effects[index].PrivilegeInternalID == other.PrivilegeInternalID && other.PrivilegeInternalID != 0 || this.Effects[index].Privilege != null && other.Privilege != null && (!string.IsNullOrEmpty(other.Privilege.type) && this.Effects[index].Privilege.type == other.Privilege.type))
        return this.Effects[index];
    }
    return (Effect) null;
  }
}
