﻿// Decompiled with JetBrains decompiler
// Type: SystemAnnouncement
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class SystemAnnouncement : Hud
{
  public Queue<KeyValuePair<long, SystemAnnouncementItem>> displayQueue = new Queue<KeyValuePair<long, SystemAnnouncementItem>>();
  public Queue<KeyValuePair<long, string>> messageQueue = new Queue<KeyValuePair<long, string>>();
  private float moveSpeed = 200f;
  public GameObject container;
  public UISprite background;
  public SystemAnnouncementItem orgItem;
  private int curId;
  private int displayCount;

  public void Init()
  {
    this.gameObject.SetActive(false);
    this.displayCount = 0;
    Queue<KeyValuePair<long, SystemAnnouncementItem>>.Enumerator enumerator = this.displayQueue.GetEnumerator();
    while (enumerator.MoveNext())
      UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.Value.gameObject);
    this.displayQueue.Clear();
    this.messageQueue.Clear();
  }

  public void ShowMessage(string message)
  {
    this.messageQueue.Enqueue(new KeyValuePair<long, string>((long) this.curId++, message));
    if (!this.gameObject.activeSelf)
      this.Show();
    if (this.displayQueue.Count != this.displayCount)
      return;
    this.EnqueueDisplayQueue();
  }

  private void Show()
  {
    this.gameObject.SetActive(true);
  }

  private void Hide()
  {
    this.gameObject.SetActive(false);
  }

  private void EnqueueDisplayQueue()
  {
    if (this.messageQueue.Count <= 0)
      return;
    KeyValuePair<long, string> keyValuePair = this.messageQueue.Dequeue();
    GameObject gameObject = NGUITools.AddChild(this.container, this.orgItem.gameObject);
    gameObject.SetActive(true);
    SystemAnnouncementItem component = gameObject.GetComponent<SystemAnnouncementItem>();
    component.Display(keyValuePair.Key, keyValuePair.Value, this.background.width, this.moveSpeed);
    component.onMoveIn = new System.Action<long>(this.OnMessageMoveIn);
    component.onMoveOut = new System.Action<long>(this.OnMessageMoveOut);
    this.displayQueue.Enqueue(new KeyValuePair<long, SystemAnnouncementItem>(keyValuePair.Key, component));
  }

  private void DequeueDisplayQueue()
  {
    if (this.displayQueue.Count > 0)
      this.displayQueue.Dequeue().Value.Release();
    if (this.displayQueue.Count != 0)
      return;
    this.Hide();
  }

  private void OnMessageMoveIn(long id)
  {
    this.EnqueueDisplayQueue();
    ++this.displayCount;
  }

  private void OnMessageMoveOut(long id)
  {
    this.DequeueDisplayQueue();
    --this.displayCount;
  }
}
