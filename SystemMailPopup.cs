﻿// Decompiled with JetBrains decompiler
// Type: SystemMailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class SystemMailPopup : BaseReportPopup
{
  private List<RewardRender> rewardsrenders = new List<RewardRender>();
  protected SystemMessage systemMessage;
  public UITable rewardsTable;
  public UITable rewardsContentTable;
  public UITable contentTable;
  public UITable messageTable;
  public UILabel receiveTimeLabel;
  public GameObject rewardRender;
  public UIButton receiveBtn;
  public UIButton contactBtn;
  public UILabel messageLabel;
  public UILabel btnLabel;

  public virtual void UpdateReceiveTime()
  {
    if (string.IsNullOrEmpty(this.systemMessage.link))
    {
      if (this.systemMessage.attachment_status == 2)
      {
        this.receiveTimeLabel.text = Utils.XLAT("mail_notice_reward_received");
        this.receiveBtn.isEnabled = false;
        this.CancelInvoke();
      }
      else if (this.systemMessage.AttachmentExpirationLeftTime > 0)
      {
        this.receiveTimeLabel.text = Utils.XLAT("mail_notice_reward_time_remaining") + Utils.ConvertSecsToString((double) this.systemMessage.AttachmentExpirationLeftTime);
        this.receiveBtn.isEnabled = true;
        this.receiveBtn.onClick.Clear();
        this.receiveBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.HandleReceiveBtnClick)));
        if (this.systemMessage.AttachmentExpirationLeftTime <= 5184000)
          return;
        this.receiveTimeLabel.text = " ";
      }
      else
      {
        this.receiveTimeLabel.text = Utils.XLAT("mail_notice_reward_expired");
        this.receiveBtn.isEnabled = false;
        this.CancelInvoke();
      }
    }
    else if (this.systemMessage.attachment_status == 2)
    {
      this.receiveTimeLabel.text = Utils.XLAT("mail_notice_reward_received");
      this.receiveBtn.isEnabled = false;
      this.CancelInvoke();
    }
    else if (this.systemMessage.AttachmentExpirationLeftTime > 0)
    {
      this.receiveTimeLabel.text = Utils.XLAT("mail_notice_reward_time_remaining") + Utils.ConvertSecsToString((double) this.systemMessage.AttachmentExpirationLeftTime);
      if (this.systemMessage.linkClicked == 1)
      {
        this.receiveBtn.onClick.Clear();
        this.receiveBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.HandleReceiveBtnClick)));
        this.btnLabel.text = Utils.XLAT("mail_notice_uppercase_receive_button");
        this.receiveBtn.isEnabled = true;
      }
      else
      {
        this.receiveBtn.onClick.Clear();
        this.receiveBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.HandleLinkBtnClick)));
        this.btnLabel.text = Utils.XLAT("mail_access_link_button");
        this.receiveBtn.isEnabled = true;
      }
    }
    else
    {
      this.receiveTimeLabel.text = Utils.XLAT("mail_notice_reward_expired");
      this.receiveBtn.isEnabled = false;
      this.CancelInvoke();
    }
  }

  private void HandleLinkBtnClick()
  {
    if (!string.IsNullOrEmpty(this.systemMessage.link))
      Application.OpenURL(this.systemMessage.link);
    this.receiveBtn.isEnabled = false;
    this.systemMessage.linkClicked = 1;
  }

  private void UpdateUI()
  {
    this.UpdateReward();
    this.UpdateMessage();
    this.InvokeRepeating("UpdateReceiveTime", 2f, 1f);
  }

  private void UpdateMessage()
  {
    this.messageLabel.text = this.systemMessage.GetBodyString();
    EmojiManager.Instance.ProcessEmojiLabel(this.messageLabel);
  }

  private void UpdateReward()
  {
    this.Clean();
    if (this.systemMessage.reward == null)
    {
      this.rewardsContentTable.gameObject.SetActive(false);
    }
    else
    {
      this.rewardsContentTable.gameObject.SetActive(true);
      this.UpdateResources();
      this.UpdateItems();
      this.UpdateGold();
    }
    this.UpdateReceiveTime();
    this.rewardsTable.repositionNow = true;
    this.contentTable.repositionNow = true;
  }

  private void UpdateResources()
  {
    if (this.systemMessage.reward == null)
      return;
    List<SystemMessage.ResourceReward> resources = this.systemMessage.reward.resources;
    if (resources == null)
      return;
    for (int index = 0; index < resources.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.rewardsTable.gameObject, this.rewardRender);
      gameObject.SetActive(true);
      RewardRender component = gameObject.GetComponent<RewardRender>();
      component.Feed(resources[index]);
      this.rewardsrenders.Add(component);
    }
  }

  private void UpdateItems()
  {
    if (this.systemMessage.reward == null)
      return;
    List<SystemMessage.ItemReward> items = this.systemMessage.reward.items;
    if (items == null)
      return;
    for (int index = 0; index < items.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.rewardsTable.gameObject, this.rewardRender);
      gameObject.SetActive(true);
      RewardRender component = gameObject.GetComponent<RewardRender>();
      component.Feed(items[index]);
      this.rewardsrenders.Add(component);
    }
  }

  private void UpdateGold()
  {
    if (this.systemMessage.reward == null)
      return;
    SystemMessage.GoldReward goldReward = this.systemMessage.reward.goldReward;
    if (goldReward == null)
      return;
    GameObject gameObject = NGUITools.AddChild(this.rewardsTable.gameObject, this.rewardRender);
    gameObject.SetActive(true);
    RewardRender component = gameObject.GetComponent<RewardRender>();
    component.Feed(goldReward);
    this.rewardsrenders.Add(component);
  }

  private void Clean()
  {
    using (List<RewardRender>.Enumerator enumerator = this.rewardsrenders.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.rewardsrenders.Clear();
  }

  protected virtual void AddEventHandlers()
  {
    this.contactBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.HandleContactBtnClick)));
    this.rewardsTable.onReposition += new UITable.OnReposition(this.HandleRewardsTableReposition);
    this.rewardsContentTable.onReposition += new UITable.OnReposition(this.HandleRewardsContentTableReposition);
  }

  private void RemoveEventHandlers()
  {
    this.receiveBtn.onClick.Clear();
    this.contactBtn.onClick.Clear();
    this.rewardsTable.onReposition = (UITable.OnReposition) null;
  }

  protected void HandleBackBtnClick()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  protected void HandleCloseBtnClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  protected void HandleRewardsTableReposition()
  {
    this.rewardsTable.onReposition -= new UITable.OnReposition(this.HandleRewardsTableReposition);
    this.rewardsContentTable.repositionNow = true;
    this.messageTable.repositionNow = true;
  }

  protected void HandleRewardsContentTableReposition()
  {
    this.rewardsContentTable.onReposition -= new UITable.OnReposition(this.HandleRewardsContentTableReposition);
    Utils.ExecuteAtTheEndOfFrame(new System.Action(this.RepositionContentTable));
  }

  private void RepositionContentTable()
  {
    this.contentTable.repositionNow = true;
  }

  protected void HandleReceiveBtnClick()
  {
    this.receiveBtn.isEnabled = false;
    this.CancelInvoke();
    PlayerData.inst.mail.API.GainAttachment((int) this.systemMessage.category, this.systemMessage.mailID, new System.Action<bool, object>(this.GainAttachmentCallBack));
  }

  protected void GainAttachmentCallBack(bool ok, object obj)
  {
    if (!ok)
      return;
    this.systemMessage.attachment_status = 2;
    this.UpdateUI();
    this.CollectRewards();
  }

  private void CollectRewards()
  {
    Hashtable hashtable = this.param.mail.data[(object) "attachment"] as Hashtable;
    if (hashtable == null)
      return;
    RewardData rewardData = JsonReader.Deserialize<RewardData>(Utils.Object2Json((object) hashtable));
    List<IconData> iconDataList = new List<IconData>();
    if (rewardData.item != null)
    {
      RewardsCollectionAnimator.Instance.Clear();
      using (Dictionary<string, int>.Enumerator enumerator = rewardData.item.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int internalId = int.Parse(current.Key);
          int num = current.Value;
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
          RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
          {
            icon = itemStaticInfo.ImagePath,
            count = (float) num
          });
        }
      }
    }
    List<SystemMessage.ResourceReward> resources = this.systemMessage.reward.resources;
    if (resources != null)
    {
      for (int index = 0; index < resources.Count; ++index)
      {
        ResRewardsInfo.Data data = new ResRewardsInfo.Data();
        data.count = resources[index].amount;
        string resourceName = resources[index].resourceName;
        if (resourceName != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (SystemMailPopup.\u003C\u003Ef__switch\u0024mapB6 == null)
          {
            // ISSUE: reference to a compiler-generated field
            SystemMailPopup.\u003C\u003Ef__switch\u0024mapB6 = new Dictionary<string, int>(4)
            {
              {
                "food",
                0
              },
              {
                "wood",
                1
              },
              {
                "ore",
                2
              },
              {
                "silver",
                3
              }
            };
          }
          int num;
          // ISSUE: reference to a compiler-generated field
          if (SystemMailPopup.\u003C\u003Ef__switch\u0024mapB6.TryGetValue(resourceName, out num))
          {
            switch (num)
            {
              case 0:
                data.rt = ResRewardsInfo.ResType.Food;
                break;
              case 1:
                data.rt = ResRewardsInfo.ResType.Wood;
                break;
              case 2:
                data.rt = ResRewardsInfo.ResType.Ore;
                break;
              case 3:
                data.rt = ResRewardsInfo.ResType.Silver;
                break;
            }
          }
        }
        RewardsCollectionAnimator.Instance.Ress.Add(data);
      }
    }
    RewardsCollectionAnimator.Instance.CollectItems(false);
    RewardsCollectionAnimator.Instance.CollectResource(false);
  }

  protected void HandleContactBtnClick()
  {
    Utils.PopUpCommingSoon();
  }

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.systemMessage = (orgParam as BaseReportPopup.Parameter).mail as SystemMessage;
    this.UpdateUI();
    this.AddEventHandlers();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.OnClose(orgParam);
    this.RemoveEventHandlers();
    this.CancelInvoke();
  }
}
