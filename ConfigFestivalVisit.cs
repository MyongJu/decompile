﻿// Decompiled with JetBrains decompiler
// Type: ConfigFestivalVisit
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigFestivalVisit
{
  private Dictionary<string, FestivalVisitInfo> datas;
  private Dictionary<int, FestivalVisitInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<FestivalVisitInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public void Clear()
  {
    this.datas.Clear();
    this.dicByUniqueId.Clear();
  }

  public FestivalVisitInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (FestivalVisitInfo) null;
  }

  public FestivalVisitInfo GetData(int internalId)
  {
    if (this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (FestivalVisitInfo) null;
  }
}
