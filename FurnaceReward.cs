﻿// Decompiled with JetBrains decompiler
// Type: FurnaceReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FurnaceReward : MonoBehaviour
{
  public UITexture background;
  public UITexture icon;
  public UILabel itemCount;
  public int rewardItemId;
  public int rewardCount;
  public System.Action<int> OnItemUnSelected;

  public void OnItemPress()
  {
    Utils.DelayShowTip(this.rewardItemId, this.background.transform, 0L, 0L, 0);
  }

  public void OnClickHandler()
  {
    if (this.OnItemUnSelected == null)
      return;
    this.OnItemUnSelected(this.rewardItemId);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  public void Refresh(int count)
  {
    if (count <= 0)
    {
      NGUITools.SetActive(this.gameObject, false);
      this.rewardCount = 0;
    }
    else
      NGUITools.SetActive(this.gameObject, true);
  }

  public void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
    BuilderFactory.Instance.Release((UIWidget) this.background);
  }

  public void SetData(int itemId, int count)
  {
    if (this.rewardItemId != itemId)
    {
      this.rewardItemId = itemId;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
      if (itemStaticInfo != null)
      {
        BuilderFactory.Instance.Build((UIWidget) this.icon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
        BuilderFactory.Instance.Build((UIWidget) this.background, Utils.GetQualityImagePath(itemStaticInfo.Quality), (System.Action<bool>) null, true, false, true, string.Empty);
      }
    }
    if (count <= 0)
    {
      NGUITools.SetActive(this.gameObject, false);
      this.rewardCount = 0;
    }
    else
      NGUITools.SetActive(this.gameObject, true);
    this.rewardCount = count;
    if (this.rewardCount < 0)
      this.rewardCount = 0;
    this.itemCount.text = "x" + count.ToString();
  }
}
