﻿// Decompiled with JetBrains decompiler
// Type: MessageBoxWithIconAnd2Buttons
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MessageBoxWithIconAnd2Buttons : PopupBase
{
  public System.Action onYes;
  public System.Action onNo;
  [SerializeField]
  private UILabel m_TitleLabel;
  [SerializeField]
  private UILabel m_ContentLabel1;
  [SerializeField]
  private UILabel m_ContentLabel2;
  [SerializeField]
  private UILabel m_YesLabel;
  [SerializeField]
  private UILabel m_NoLabel;
  [SerializeField]
  private UIButton m_YesButton;
  [SerializeField]
  private UIButton m_NopButton;
  [SerializeField]
  private UIButton m_CloseButton;
  [SerializeField]
  private UITexture m_Icon;

  public void Initialize(string title, string content1, string content2, string yes, string no, System.Action yesCallback, System.Action noCallback, string iconPath = "")
  {
    this.m_TitleLabel.text = title;
    this.m_ContentLabel1.text = content1;
    this.m_ContentLabel2.text = content2;
    this.m_YesLabel.text = yes;
    this.m_NoLabel.text = no;
    if (iconPath != string.Empty)
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, iconPath, (System.Action<bool>) null, true, false, string.Empty);
    this.onYes = yesCallback;
    this.onNo = noCallback;
  }

  public void OnYes()
  {
    if (this.onYes != null)
      this.onYes();
    this.OnClose();
  }

  public void OnNo()
  {
    if (this.onNo != null)
      this.onNo();
    this.OnClose();
  }

  public void OnClose()
  {
    this.Close();
  }
}
