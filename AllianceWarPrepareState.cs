﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarPrepareState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class AllianceWarPrepareState : AllianceWarBaseState
{
  private List<GroupItemRender> dataList = new List<GroupItemRender>();
  private const string GROUP_RESULT = "alliance_warfare_grouping_results_name";
  private const string ASSIGNED_DESCRIPTION = "alliance_warfare_opponents_assigned_description";
  public GameObject itemPrefab;
  public UITable table;
  public UILabel groupingResult;
  public UILabel tipsStart;

  public override void Show(bool requestData)
  {
    base.Show(false);
    this.RefreshGroupingDatas();
  }

  public override void UpdateUI()
  {
    this.instruction.text = Utils.XLAT("alliance_warfare_event_description");
    this.state.text = ScriptLocalization.GetWithPara("alliance_warfare_preparing_status", new Dictionary<string, string>()
    {
      {
        "0",
        Utils.FormatTime(this.allianceWarData.FightStartTime - NetServerTime.inst.ServerTimestamp, true, true, true)
      }
    }, true);
    this.groupingResult.text = Utils.XLAT("alliance_warfare_grouping_results_name");
    this.tipsStart.text = Utils.XLAT("alliance_warfare_opponents_assigned_description");
  }

  private void RefreshGroupingDatas()
  {
    if (this.allianceWarData.GroupDataList == null)
      return;
    for (int index = 0; index < this.allianceWarData.GroupDataList.Count; ++index)
    {
      GameObject gameObject = Object.Instantiate<GameObject>(this.itemPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.table.transform;
      gameObject.transform.localScale = new Vector3(0.85f, 0.85f, 1f);
      gameObject.transform.localPosition = Vector3.zero;
      GroupItemRender component = gameObject.GetComponent<GroupItemRender>();
      component.FeedData(this.allianceWarData.GroupDataList[index]);
      this.dataList.Add(component);
    }
  }

  private void ClearData()
  {
    for (int index = 0; index < this.dataList.Count; ++index)
    {
      this.dataList[index].gameObject.transform.parent = (Transform) null;
      this.dataList[index].gameObject.SetActive(false);
      Object.Destroy((Object) this.dataList[index].gameObject);
    }
    this.dataList.Clear();
  }

  public override void Hide()
  {
    base.Hide();
    this.ClearData();
  }
}
