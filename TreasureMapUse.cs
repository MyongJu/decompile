﻿// Decompiled with JetBrains decompiler
// Type: TreasureMapUse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;

public class TreasureMapUse : ItemBaseUse
{
  public TreasureMapUse(int id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  public TreasureMapUse(string id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  protected override void CustomUse(System.Action<bool, object> resultHandler)
  {
    MessageHub.inst.GetPortByAction("TreasureMap:loadTreasureMap").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenPopup("TreasureMap/TreasureMapPopup", (Popup.PopupParameter) new TreasureMapPopup.Paramenter()
      {
        itemInfo = this.Info
      });
    }), true);
  }
}
