﻿// Decompiled with JetBrains decompiler
// Type: MagicStoreInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class MagicStoreInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "refresh_group")]
  public string RefreshGroup;
  [Config(Name = "reward_item")]
  public int RewardItemId;
  [Config(Name = "rewards_value")]
  public int RewardValue;
  [Config(Name = "cost_item")]
  public int CostItemId;
  [Config(Name = "cost_value")]
  public int CostValue;
  [Config(Name = "refresh_weight")]
  public int RefreshWeight;
}
