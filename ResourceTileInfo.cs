﻿// Decompiled with JetBrains decompiler
// Type: ResourceTileInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class ResourceTileInfo : UI.Dialog
{
  public UILabel m_Title;
  public UILabel m_Content;
  private ResourceTileInfo.Parameter m_Parameter;

  public void OnCloseButtonClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as ResourceTileInfo.Parameter;
    this.m_Title.text = this.GetTitle();
    this.m_Content.text = this.GetContent();
  }

  private string GetTitle()
  {
    switch (this.m_Parameter.tileData.TileType)
    {
      case TileType.Resource1:
        return Utils.XLAT("march_send_march_aim_type_uppercase_food");
      case TileType.Resource2:
        return Utils.XLAT("march_send_march_aim_type_uppercase_wood");
      case TileType.Resource3:
        return Utils.XLAT("march_send_march_aim_type_uppercase_ore");
      case TileType.Resource4:
        return Utils.XLAT("march_send_march_aim_type_uppercase_silver");
      case TileType.PitMine:
        return Utils.XLAT("march_send_march_aim_type_uppercase_silver");
      default:
        return string.Empty;
    }
  }

  private string GetContent()
  {
    switch (this.m_Parameter.tileData.TileType)
    {
      case TileType.Resource1:
        return Utils.XLAT("kingdom_resource_food_info_description");
      case TileType.Resource2:
        return Utils.XLAT("kingdom_resource_wood_info_description");
      case TileType.Resource3:
        return Utils.XLAT("kingdom_resource_ore_info_description");
      case TileType.Resource4:
        return Utils.XLAT("kingdom_resource_silver_info_description");
      case TileType.PitMine:
        return Utils.XLAT("kingdom_resource_silver_info_description");
      default:
        return Utils.XLAT("kingdom_resource_info_description");
    }
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public TileData tileData;
  }
}
