﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightPropertyRateStaticInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DragonKnightPropertyRateStaticInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "attack")]
  public float Attack;
  [Config(Name = "defence")]
  public float Defence;
  [Config(Name = "health")]
  public float Health;
  [Config(Name = "mana")]
  public float Mana;
  [Config(Name = "magic")]
  public float Magic;
}
