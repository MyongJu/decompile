﻿// Decompiled with JetBrains decompiler
// Type: MagicStoreItemSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using UI;
using UnityEngine;

public class MagicStoreItemSlot : DynamicReplaceItem
{
  private Color[] backgroundColer = new Color[3]
  {
    new Color(0.1333333f, 0.5568628f, 0.2078431f, 1f),
    new Color(0.1333333f, 0.2235294f, 0.5568628f, 1f),
    new Color(0.2509804f, 0.1294118f, 0.5568628f, 1f)
  };
  private float moveduration = 0.5f;
  public System.Action<int> onBuyButtonClick;
  private int itemId;
  private int costItemId;
  [SerializeField]
  private MagicStoreItemSlot.Panel panel;

  public int npcStoreItemInteranlId { private set; get; }

  public int index { private set; get; }

  public string itemName
  {
    get
    {
      return this.panel.slotName.text;
    }
  }

  public string costType { private set; get; }

  public int costValue { private set; get; }

  public void OnItemPress()
  {
    Utils.DelayShowTip(this.itemId, this.panel.border, 0L, 0L, 0);
  }

  public void OnItemRelese()
  {
    Utils.StopShowItemTip();
  }

  public void OnItemClickHandler()
  {
    Utils.ShowItemTip(this.itemId, this.panel.border, 0L, 0L, 0);
  }

  public void Setup(int itemInteranlId, int inIndex, System.Action<int> onBuyButtonClickEvent = null)
  {
    this.index = inIndex;
    this.npcStoreItemInteranlId = itemInteranlId;
    MagicStoreInfo data = ConfigManager.inst.DB_MagicStore.GetData(itemInteranlId);
    ItemStaticInfo itemStaticInfo1 = ConfigManager.inst.DB_Items.GetItem(data.RewardItemId);
    ItemStaticInfo itemStaticInfo2 = ConfigManager.inst.DB_Items.GetItem(data.CostItemId);
    if (itemStaticInfo1 != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.icon, itemStaticInfo1.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      Utils.SetItemName(this.panel.slotName, itemStaticInfo1.internalId);
      Utils.SetItemBackground(this.panel.background, itemStaticInfo1.internalId);
      this.panel.slotCount.text = string.Format("x {0}", (object) data.RewardValue);
      this.itemId = itemStaticInfo1.internalId;
      this.costItemId = itemStaticInfo2.internalId;
      this.costValue = data.CostValue;
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.costIcon, itemStaticInfo2.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this.panel.discountContainer.gameObject.SetActive(false);
      this.panel.slotPrice.gameObject.SetActive(true);
      this.panel.slotPrice.text = this.costValue.ToString();
      this.panel.slotPrice.color = ItemBag.Instance.GetItemCount(this.costItemId) < this.costValue ? Color.red : Color.white;
    }
    this.onBuyButtonClick = onBuyButtonClickEvent;
  }

  public bool ButtonEnable
  {
    set
    {
      if (this.panel == null || !((UnityEngine.Object) this.panel.BT_Buy != (UnityEngine.Object) null))
        return;
      this.panel.BT_Buy.isEnabled = value;
    }
    get
    {
      if (this.panel != null && (UnityEngine.Object) this.panel.BT_Buy != (UnityEngine.Object) null)
        return this.panel.BT_Buy.isEnabled;
      return false;
    }
  }

  private void SetEffect(int descountLevel)
  {
    if (descountLevel == 1)
    {
      this.panel.effect.SetActive(false);
    }
    else
    {
      this.panel.effect.SetActive(true);
      foreach (Renderer componentsInChild in this.panel.effect.GetComponentsInChildren<Renderer>())
      {
        foreach (Material material in componentsInChild.materials)
          material.SetColor("_TintColor", this.backgroundColer[descountLevel - 2]);
      }
      foreach (ParticleSystem componentsInChild in this.panel.effect.GetComponentsInChildren<ParticleSystem>())
        componentsInChild.Play();
    }
  }

  public void RefreshCost()
  {
    this.panel.slotPrice.color = ItemBag.Instance.GetItemCount(this.costItemId) < this.costValue ? Color.red : Color.white;
  }

  public void OnBuyClick()
  {
    if (ItemBag.Instance.GetItemCount(this.costItemId) >= this.costValue)
    {
      if (this.onBuyButtonClick == null)
        return;
      this.onBuyButtonClick(this.index);
    }
    else
    {
      string content = ScriptLocalization.Get("exchange_magic_store_insufficient_tokens_description", true);
      string empty = string.Empty;
      if (string.IsNullOrEmpty(empty))
        empty = ScriptLocalization.Get("id_uppercase_insufficient_items", true);
      string left = ScriptLocalization.Get("id_uppercase_go_to", true);
      string right = ScriptLocalization.Get("id_uppercase_cancel", true);
      UIManager.inst.ShowConfirmationBox(empty, content, left, right, ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.OpenMagicSelter), (System.Action) null, (System.Action) null);
    }
  }

  public void OpenMagicSelter()
  {
    UIManager.inst.OpenDlg("MagicSelterDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public override void Init(object args)
  {
    MagicStoreItemSlot.Data data = args as MagicStoreItemSlot.Data;
    this.Setup(data.itemInteranlId, data.inIndex, data.onBuyButtonClickEvent);
  }

  public override void FlyOut(System.Action action = null)
  {
    this.MoveAnimation((System.Action) (() =>
    {
      if (action != null)
        action();
      this.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    }));
  }

  public override void FlyIn(System.Action action = null)
  {
    this.MoveAnimation(action);
  }

  public void MoveAnimation(System.Action action = null)
  {
    Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.transform);
    Vector3 localPosition = this.transform.localPosition;
    localPosition.x += relativeWidgetBounds.size.x;
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) localPosition, (object) "time", (object) this.moveduration, (object) "easetype", (object) iTween.EaseType.linear, (object) "islocal", (object) true, (object) "oncomplete", (object) "OnAnimationComplete", (object) "oncompleteparams", (object) action));
  }

  private void OnAnimationComplete(System.Action action = null)
  {
    if (action == null)
      return;
    action();
  }

  public class Data
  {
    public int itemInteranlId;
    public int inIndex;
    public System.Action<int> onBuyButtonClickEvent;
  }

  [Serializable]
  protected class Panel
  {
    public UITexture icon;
    public UILabel slotName;
    public UILabel slotCount;
    public UISprite slotPirceIcon;
    public UILabel slotPrice;
    public UITexture costIcon;
    public Transform premiumIcon;
    public UITexture background;
    public Transform border;
    public Transform discountContainer;
    public UILabel discountCurPrice;
    public UILabel discountInitPrice;
    public UIButton BT_Buy;
    public GameObject effect;
  }
}
