﻿// Decompiled with JetBrains decompiler
// Type: RoomRaidChest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class RoomRaidChest
{
  public Dictionary<int, int> reward = new Dictionary<int, int>();

  public void Decode(Hashtable data)
  {
    IDictionaryEnumerator enumerator = data.GetEnumerator();
    while (enumerator.MoveNext())
    {
      int result1;
      if (int.TryParse(enumerator.Key.ToString(), out result1))
      {
        int result2;
        int.TryParse(enumerator.Value.ToString(), out result2);
        this.reward.Add(result1, result2);
      }
    }
  }
}
