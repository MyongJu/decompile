﻿// Decompiled with JetBrains decompiler
// Type: UITexture
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("NGUI/UI/NGUI Texture")]
public class UITexture : UIBasicSprite
{
  [SerializeField]
  [HideInInspector]
  private Rect mRect = new Rect(0.0f, 0.0f, 1f, 1f);
  [SerializeField]
  [HideInInspector]
  private Vector4 mBorder = Vector4.zero;
  [NonSerialized]
  private int mPMA = -1;
  [SerializeField]
  [HideInInspector]
  private Texture mTexture;
  [SerializeField]
  [HideInInspector]
  private Material mMat;
  [SerializeField]
  [HideInInspector]
  private Shader mShader;
  [SerializeField]
  [HideInInspector]
  private bool mFixedAspect;

  public override Texture mainTexture
  {
    get
    {
      if ((UnityEngine.Object) this.mTexture != (UnityEngine.Object) null)
        return this.mTexture;
      if ((UnityEngine.Object) this.mMat != (UnityEngine.Object) null)
        return this.mMat.mainTexture;
      return (Texture) null;
    }
    set
    {
      if (!((UnityEngine.Object) this.mTexture != (UnityEngine.Object) value))
        return;
      if ((UnityEngine.Object) this.drawCall != (UnityEngine.Object) null && this.drawCall.widgetCount == 1 && (UnityEngine.Object) this.mMat == (UnityEngine.Object) null)
      {
        this.mTexture = value;
        this.drawCall.mainTexture = value;
      }
      else
      {
        this.RemoveFromPanel();
        this.mTexture = value;
        this.mPMA = -1;
        this.MarkAsChanged();
      }
    }
  }

  public override Material material
  {
    get
    {
      return this.mMat;
    }
    set
    {
      if (!((UnityEngine.Object) this.mMat != (UnityEngine.Object) value))
        return;
      this.RemoveFromPanel();
      this.mShader = (Shader) null;
      this.mMat = value;
      this.mPMA = -1;
      this.MarkAsChanged();
    }
  }

  public override Shader shader
  {
    get
    {
      if ((UnityEngine.Object) this.mMat != (UnityEngine.Object) null)
        return this.mMat.shader;
      if ((UnityEngine.Object) this.mShader == (UnityEngine.Object) null)
        this.mShader = Shader.Find("Unlit/Transparent Colored");
      return this.mShader;
    }
    set
    {
      if (!((UnityEngine.Object) this.mShader != (UnityEngine.Object) value))
        return;
      if ((UnityEngine.Object) this.drawCall != (UnityEngine.Object) null && this.drawCall.widgetCount == 1 && (UnityEngine.Object) this.mMat == (UnityEngine.Object) null)
      {
        this.mShader = value;
        this.drawCall.shader = value;
      }
      else
      {
        this.RemoveFromPanel();
        this.mShader = value;
        this.mPMA = -1;
        this.mMat = (Material) null;
        this.MarkAsChanged();
      }
    }
  }

  public override bool premultipliedAlpha
  {
    get
    {
      if (this.mPMA == -1)
      {
        Material material = this.material;
        this.mPMA = !((UnityEngine.Object) material != (UnityEngine.Object) null) || !((UnityEngine.Object) material.shader != (UnityEngine.Object) null) || !material.shader.name.Contains("Premultiplied") ? 0 : 1;
      }
      return this.mPMA == 1;
    }
  }

  public override Vector4 border
  {
    get
    {
      return this.mBorder;
    }
    set
    {
      if (!(this.mBorder != value))
        return;
      this.mBorder = value;
      this.MarkAsChanged();
    }
  }

  public Rect uvRect
  {
    get
    {
      return this.mRect;
    }
    set
    {
      if (!(this.mRect != value))
        return;
      this.mRect = value;
      this.MarkAsChanged();
    }
  }

  public override Vector4 drawingDimensions
  {
    get
    {
      Vector2 pivotOffset = this.pivotOffset;
      float a1 = -pivotOffset.x * (float) this.mWidth;
      float a2 = -pivotOffset.y * (float) this.mHeight;
      float b1 = a1 + (float) this.mWidth;
      float b2 = a2 + (float) this.mHeight;
      if ((UnityEngine.Object) this.mTexture != (UnityEngine.Object) null && this.mType != UIBasicSprite.Type.Tiled)
      {
        int width = this.mTexture.width;
        int height = this.mTexture.height;
        int num1 = 0;
        int num2 = 0;
        float num3 = 1f;
        float num4 = 1f;
        if (width > 0 && height > 0 && (this.mType == UIBasicSprite.Type.Simple || this.mType == UIBasicSprite.Type.Filled))
        {
          if ((width & 1) != 0)
            ++num1;
          if ((height & 1) != 0)
            ++num2;
          num3 = 1f / (float) width * (float) this.mWidth;
          num4 = 1f / (float) height * (float) this.mHeight;
        }
        if (this.mFlip == UIBasicSprite.Flip.Horizontally || this.mFlip == UIBasicSprite.Flip.Both)
          a1 += (float) num1 * num3;
        else
          b1 -= (float) num1 * num3;
        if (this.mFlip == UIBasicSprite.Flip.Vertically || this.mFlip == UIBasicSprite.Flip.Both)
          a2 += (float) num2 * num4;
        else
          b2 -= (float) num2 * num4;
      }
      float num5;
      float num6;
      if (this.mFixedAspect)
      {
        num5 = 0.0f;
        num6 = 0.0f;
      }
      else
      {
        Vector4 border = this.border;
        num5 = border.x + border.z;
        num6 = border.y + border.w;
      }
      return new Vector4(Mathf.Lerp(a1, b1 - num5, this.mDrawRegion.x), Mathf.Lerp(a2, b2 - num6, this.mDrawRegion.y), Mathf.Lerp(a1 + num5, b1, this.mDrawRegion.z), Mathf.Lerp(a2 + num6, b2, this.mDrawRegion.w));
    }
  }

  public bool fixedAspect
  {
    get
    {
      return this.mFixedAspect;
    }
    set
    {
      if (this.mFixedAspect == value)
        return;
      this.mFixedAspect = value;
      this.mDrawRegion = new Vector4(0.0f, 0.0f, 1f, 1f);
      this.MarkAsChanged();
    }
  }

  public override void MakePixelPerfect()
  {
    base.MakePixelPerfect();
    if (this.mType == UIBasicSprite.Type.Tiled)
      return;
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null || this.mType != UIBasicSprite.Type.Simple && this.mType != UIBasicSprite.Type.Filled && this.hasBorder || !((UnityEngine.Object) mainTexture != (UnityEngine.Object) null))
      return;
    int width = mainTexture.width;
    int height = mainTexture.height;
    if ((width & 1) == 1)
      ++width;
    if ((height & 1) == 1)
      ++height;
    this.width = width;
    this.height = height;
  }

  protected override void OnUpdate()
  {
    base.OnUpdate();
    if (!this.mFixedAspect)
      return;
    Texture mainTexture = this.mainTexture;
    if (!((UnityEngine.Object) mainTexture != (UnityEngine.Object) null))
      return;
    int width = mainTexture.width;
    int height = mainTexture.height;
    if ((width & 1) == 1)
      ++width;
    if ((height & 1) == 1)
      ++height;
    float mWidth = (float) this.mWidth;
    float mHeight = (float) this.mHeight;
    float num1 = mWidth / mHeight;
    float num2 = (float) width / (float) height;
    if ((double) num2 < (double) num1)
    {
      float x = (float) (((double) mWidth - (double) mHeight * (double) num2) / (double) mWidth * 0.5);
      this.drawRegion = new Vector4(x, 0.0f, 1f - x, 1f);
    }
    else
    {
      float y = (float) (((double) mHeight - (double) mWidth / (double) num2) / (double) mHeight * 0.5);
      this.drawRegion = new Vector4(0.0f, y, 1f, 1f - y);
    }
  }

  public override void OnFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols)
  {
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null)
      return;
    Rect outer = new Rect(this.mRect.x * (float) mainTexture.width, this.mRect.y * (float) mainTexture.height, (float) mainTexture.width * this.mRect.width, (float) mainTexture.height * this.mRect.height);
    Rect inner = outer;
    Vector4 border = this.border;
    inner.xMin += border.x;
    inner.yMin += border.y;
    inner.xMax -= border.z;
    inner.yMax -= border.w;
    float num1 = 1f / (float) mainTexture.width;
    float num2 = 1f / (float) mainTexture.height;
    outer.xMin *= num1;
    outer.xMax *= num1;
    outer.yMin *= num2;
    outer.yMax *= num2;
    inner.xMin *= num1;
    inner.xMax *= num1;
    inner.yMin *= num2;
    inner.yMax *= num2;
    int size = verts.size;
    this.Fill(verts, uvs, cols, outer, inner);
    if (this.onPostFill == null)
      return;
    this.onPostFill((UIWidget) this, size, verts, uvs, cols);
  }
}
