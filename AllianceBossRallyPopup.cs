﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossRallyPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using March;
using System.Collections.Generic;
using UI;

public class AllianceBossRallyPopup : Popup
{
  public UILabel bossName;
  public UILabel bossHP;
  public UITexture bossImage;
  public UITexture[] killRewardTextures;
  public UITexture[] damageRewardTextures;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateBossRewardInfo();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnRallyBtnPressed()
  {
    AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
    if (alliancePortalData == null)
      return;
    if (CityManager.inst.GetHighestBuildingLevelFor("war_rally") > 0 && PlayerData.inst.allianceId != 0L)
    {
      UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
      {
        location = alliancePortalData.Location,
        marchType = MarchAllocDlg.MarchType.startRabRally,
        energyCost = 20
      }, 1 != 0, 1 != 0, 1 != 0);
    }
    else
    {
      MarchSystem.LocalMarchCheckType type = GameEngine.Instance.marchSystem.CanRally(alliancePortalData.Location);
      UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
      {
        title = Utils.XLAT("id_uppercase_warning"),
        description = GameEngine.Instance.marchSystem.GetCheckTypeMsg(type)
      });
    }
    this.OnCloseBtnPressed();
  }

  private void UpdateBossRewardInfo()
  {
    AllianceBossData allianceBossData = DBManager.inst.DB_AllianceBoss.GetAllianceBossData();
    if (allianceBossData == null)
      return;
    AllianceBossInfo allianceBossInfo = ConfigManager.inst.DB_AllianceBoss.Get((int) allianceBossData.BossId);
    if (allianceBossInfo == null)
      return;
    this.bossName.text = Utils.XLAT(allianceBossInfo.name);
    this.bossHP.text = Utils.FormatThousands(allianceBossData.troopsCount.ToString());
    BuilderFactory.Instance.HandyBuild((UIWidget) this.bossImage, "Texture/AllianceBoss/" + allianceBossInfo.image, (System.Action<bool>) null, true, false, string.Empty);
    List<Reward.RewardsValuePair> allRewards = allianceBossInfo.AllRewards;
    Dictionary<int, AllianceBossRallyPopup.RankRewardPair> dictionary1 = new Dictionary<int, AllianceBossRallyPopup.RankRewardPair>();
    dictionary1.Add(0, new AllianceBossRallyPopup.RankRewardPair()
    {
      rewardImagePath = "Texture/Alliance/alliance_fund",
      rewardCount = allianceBossInfo.fund
    });
    dictionary1.Add(1, new AllianceBossRallyPopup.RankRewardPair()
    {
      rewardImagePath = "Texture/Alliance/alliance_honor",
      rewardCount = allianceBossInfo.honor
    });
    int index1 = 0;
    int key = 2;
    while (index1 < allRewards.Count)
    {
      Reward.RewardsValuePair rewardsValuePair = allRewards[index1];
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardsValuePair.internalID);
      if (itemStaticInfo != null)
        dictionary1.Add(key, new AllianceBossRallyPopup.RankRewardPair()
        {
          rewardImagePath = itemStaticInfo.ImagePath,
          rewardCount = (long) rewardsValuePair.value
        });
      ++index1;
      ++key;
    }
    int index2 = 0;
    int index3 = 0;
    while (index2 < this.killRewardTextures.Length)
    {
      NGUITools.SetActive(this.killRewardTextures[index2].transform.parent.gameObject, false);
      if (index3 < dictionary1.Count)
      {
        if (dictionary1[index3].rewardCount > 0L)
        {
          BuilderFactory.Instance.HandyBuild((UIWidget) this.killRewardTextures[index2], dictionary1[index3].rewardImagePath, (System.Action<bool>) null, true, false, string.Empty);
          NGUITools.SetActive(this.killRewardTextures[index2].transform.parent.gameObject, true);
        }
        else
          --index2;
      }
      ++index2;
      ++index3;
    }
    AllianceBossRankRewardInfo bossRewardByRank = ConfigManager.inst.DB_AllianceBossRankReward.GetAllianceBossRewardByRank(1);
    Dictionary<int, float> dictionary2 = new Dictionary<int, float>();
    dictionary2.Add(0, bossRewardByRank.fundRatio);
    dictionary2.Add(1, bossRewardByRank.honorRatio);
    dictionary2.Add(2, bossRewardByRank.rewardRatio1);
    dictionary2.Add(3, bossRewardByRank.rewardRatio2);
    dictionary2.Add(4, bossRewardByRank.rewardRatio3);
    dictionary2.Add(5, bossRewardByRank.rewardRatio4);
    dictionary2.Add(6, bossRewardByRank.rewardRatio5);
    dictionary2.Add(7, bossRewardByRank.rewardRatio6);
    int index4 = 0;
    int index5 = 0;
    while (index4 < this.damageRewardTextures.Length)
    {
      NGUITools.SetActive(this.damageRewardTextures[index4].transform.parent.gameObject, false);
      if (index5 < dictionary1.Count)
      {
        if ((double) dictionary1[index5].rewardCount * (double) dictionary2[index5] > 0.0)
        {
          BuilderFactory.Instance.HandyBuild((UIWidget) this.damageRewardTextures[index4], dictionary1[index5].rewardImagePath, (System.Action<bool>) null, true, false, string.Empty);
          NGUITools.SetActive(this.damageRewardTextures[index4].transform.parent.gameObject, true);
        }
        else
          --index4;
      }
      ++index4;
      ++index5;
    }
  }

  public class RankRewardPair
  {
    public string rewardImagePath;
    public long rewardCount;
  }
}
