﻿// Decompiled with JetBrains decompiler
// Type: ArtifactSalePayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;

public class ArtifactSalePayload
{
  private List<ArtifactSpecialSaleData> _artifactSpecialSaleDataList = new List<ArtifactSpecialSaleData>();
  private static ArtifactSalePayload _instance;

  public static ArtifactSalePayload Instance
  {
    get
    {
      if (ArtifactSalePayload._instance == null)
        ArtifactSalePayload._instance = new ArtifactSalePayload();
      return ArtifactSalePayload._instance;
    }
  }

  public List<ArtifactSpecialSaleData> GetArtifactSpecialSaleDataList()
  {
    return this._artifactSpecialSaleDataList;
  }

  public void RequestServerData(System.Action<bool, object> callback)
  {
    RequestManager.inst.SendRequest("Artifact:loadArtifactSale", new Hashtable()
    {
      {
        (object) "world_id",
        (object) PlayerData.inst.userData.world_id
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data as Hashtable);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void BuyLimitedArtifact(int artifactShopId, int buyCount, System.Action<bool, object> callback)
  {
    RequestManager.inst.SendRequest("Artifact:buyArtifact", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "world_id",
        (object) PlayerData.inst.userData.world_id
      },
      {
        (object) "config_id",
        (object) artifactShopId
      },
      {
        (object) "amount",
        (object) buyCount
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data as Hashtable);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void EquipLimitedArtifact(int purchasedIndex, int artifactId, System.Action<bool, object> callback = null)
  {
    RequestManager.inst.SendRequest("Artifact:equipLimit", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "index",
        (object) purchasedIndex
      },
      {
        (object) "artifact_id",
        (object) artifactId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void RemoveLimitedArtifact(int purchasedIndex, int artifactId, System.Action<bool, object> callback = null)
  {
    RequestManager.inst.SendRequest("Artifact:unequipLimit", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "index",
        (object) purchasedIndex
      },
      {
        (object) "artifact_id",
        (object) artifactId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void ReplaceLimitedArtifact(int oldPurchasedIndex, int oldArtifactId, int newPurchasedIndex, int newArtifactId, System.Action<bool, object> callback = null)
  {
    RequestManager.inst.SendRequest("Artifact:replaceLimit", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "old_index",
        (object) oldPurchasedIndex
      },
      {
        (object) "old_artifact_id",
        (object) oldArtifactId
      },
      {
        (object) "new_index",
        (object) newPurchasedIndex
      },
      {
        (object) "new_artifact_id",
        (object) newArtifactId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void ShowArtifactDetail(int artifactId)
  {
    UIManager.inst.OpenPopup("Artifact/ArtifactDetailsPopup", (Popup.PopupParameter) new ArtifactDetailsPopup.Parameter()
    {
      artifactId = artifactId
    });
  }

  public void ShowArtifactBuyConfirm(int artifactShopId, long normalPrice, long specialPrice = -1, System.Action onBuyFinished = null)
  {
    UIManager.inst.OpenPopup("Artifact/ArtifactConfirmBuyPopup", (Popup.PopupParameter) new ArtifactConfirmBuyPopup.Parameter()
    {
      artifactShopId = artifactShopId,
      normalPrice = normalPrice,
      specialPrice = specialPrice,
      onBuyFinished = onBuyFinished
    });
  }

  public void ShowArtifactSaleStore()
  {
    this.RequestServerData((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("Artifact/ArtifactSaleStoreDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    }));
  }

  public void ShowArtifactSaleStore(System.Action<bool, object> callback)
  {
    this.RequestServerData((System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        UIManager.inst.OpenDlg("Artifact/ArtifactSaleStoreDlg", (UI.Dialog.DialogParameter) null, true, true, true);
      if (callback == null)
        return;
      callback(ret, data);
    }));
  }

  public void ShowArtifactBuyInventory(System.Action<bool, object> callback = null)
  {
    this.RequestServerData((System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        Utils.ShowArtifactBuyInventory();
      if (callback == null)
        return;
      callback(ret, data);
    }));
  }

  private void Decode(Hashtable data)
  {
    this._artifactSpecialSaleDataList.Clear();
    if (data == null || !data.ContainsKey((object) "artifact_sale"))
      return;
    Hashtable hashtable = data[(object) "artifact_sale"] as Hashtable;
    if (hashtable == null)
      return;
    foreach (object obj in (IEnumerable) hashtable.Values)
    {
      Hashtable data1 = obj as Hashtable;
      ArtifactSpecialSaleData artifactSpecialSaleData = new ArtifactSpecialSaleData();
      if (data1 != null)
        artifactSpecialSaleData.Decode(data1);
      this._artifactSpecialSaleDataList.Add(artifactSpecialSaleData);
    }
  }
}
