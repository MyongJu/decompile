﻿// Decompiled with JetBrains decompiler
// Type: DungeonOpenChestPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using UI;
using UnityEngine;

public class DungeonOpenChestPopup : Popup
{
  public const string POPUP_TYPE = "DragonKnight/DungeonOpenChestPopup";
  [SerializeField]
  private DungeonOpenChestPopup.Panel panel;
  private RoomChest roomChest;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.roomChest = (orgParam as DungeonOpenChestPopup.Parameter).roomChest;
    this.InitUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnOpenBtnClick()
  {
    RequestManager.inst.SendRequest("dragonKnight:openDungeonChest", (Hashtable) null, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      DragonKnightSystem.Instance.RoomManager.CurrentRoom.ClearAllChest();
      DragonKnightSystem.Instance.RoomManager.CurrentRoomEventCompelted(false);
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    }), true);
  }

  public void OnInventoryBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightDungeonInventoryPopup", (Popup.PopupParameter) null);
  }

  private void InitUI()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.roomChest.itemId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.itemImage, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.panel.itemName.text = itemStaticInfo.LocName;
    this.panel.itemValue.text = "X" + (object) this.roomChest.itemCount;
    this.panel.itemDescription.text = itemStaticInfo.LocDescription;
    this.panel.weight.text = (itemStaticInfo.DungeonWeight * this.roomChest.itemCount).ToString();
    DragonKnightDungeonData knightDungeonData = DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L);
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    long capacity = dragonKnightData.Capacity;
    bool flag1 = !DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L).BagData.ContainsKey(itemStaticInfo.internalId);
    int num = knightDungeonData.CurrentBagCount + (!flag1 ? 0 : 1);
    bool flag2 = capacity >= (long) (itemStaticInfo.DungeonWeight * this.roomChest.itemCount + knightDungeonData.CurrentBagCapacity);
    bool flag3 = num <= dragonKnightData.BagAmount;
    if (flag2 && flag3)
    {
      this.panel.capacityStatus.SetActive(true);
      this.panel.unCapactyStatus.SetActive(false);
    }
    else
    {
      this.panel.capacityStatus.SetActive(false);
      this.panel.unCapactyStatus.SetActive(true);
      this.panel.warningLabel.text = !flag2 ? Utils.XLAT("dragon_knight_satchel_capacity_full_description") : Utils.XLAT("dragon_knight_satchel_slots_full_description");
    }
  }

  public class Parameter : Popup.PopupParameter
  {
    public RoomChest roomChest;
  }

  [Serializable]
  protected class Panel
  {
    public UITexture itemImage;
    public UILabel itemName;
    public UILabel itemValue;
    public UILabel itemDescription;
    public UILabel weight;
    public GameObject capacityStatus;
    public GameObject unCapactyStatus;
    public UILabel warningLabel;
  }
}
