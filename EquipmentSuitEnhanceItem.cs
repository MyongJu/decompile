﻿// Decompiled with JetBrains decompiler
// Type: EquipmentSuitEnhanceItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class EquipmentSuitEnhanceItem : MonoBehaviour
{
  [SerializeField]
  private UILabel _currentConditionDesc;
  [SerializeField]
  private UILabel _currentBenefitValue;
  [SerializeField]
  private GameObject _currentDetailRoot;
  [SerializeField]
  private UILabel _currentBenefitName;
  [SerializeField]
  private UILabel _nextBenefitName;
  [SerializeField]
  private UILabel _nextConditionDesc;
  [SerializeField]
  private UILabel _nextBenefitValue;
  [SerializeField]
  private GameObject _nextDetailRoot;

  public void UpdateUI(List<InventoryItemData> allInventoryItemData, BagType bagType)
  {
    int num = int.MaxValue;
    if (allInventoryItemData.Count < 6)
    {
      num = 0;
    }
    else
    {
      using (List<InventoryItemData>.Enumerator enumerator = allInventoryItemData.GetEnumerator())
      {
        while (enumerator.MoveNext())
          num = Mathf.Min(enumerator.Current.enhanced, num);
      }
    }
    ConfigEquipmentSuitEnhanceInfo lessEqual = ConfigManager.inst.DB_EquipmentSuitEnhance.GetLessEqual(num, bagType);
    ConfigEquipmentSuitEnhanceInfo greater = ConfigManager.inst.DB_EquipmentSuitEnhance.GetGreater(num, bagType);
    UILabel currentBenefitName = this._currentBenefitName;
    string str1 = string.Format("[u]{0}[/u]", (object) ScriptLocalization.Get("forge_enhance_resonance_benefit", true));
    this._nextBenefitName.text = str1;
    string str2 = str1;
    currentBenefitName.text = str2;
    if (lessEqual == null)
    {
      this._currentDetailRoot.SetActive(false);
      this._currentConditionDesc.text = "-";
    }
    else
    {
      this._currentDetailRoot.SetActive(true);
      this._currentConditionDesc.text = ScriptLocalization.GetWithPara("forge_enhance_resonance_effect_description", new Dictionary<string, string>()
      {
        {
          "0",
          lessEqual.enhanceLevelReq.ToString()
        }
      }, true);
      this._currentBenefitValue.text = string.Format("+{0}%", (object) (float) ((double) lessEqual.enhanceValue * 100.0));
    }
    if (greater == null)
    {
      this._nextDetailRoot.SetActive(false);
      this._nextConditionDesc.text = "-";
    }
    else
    {
      this._nextDetailRoot.SetActive(true);
      this._nextConditionDesc.text = ScriptLocalization.GetWithPara("forge_enhance_resonance_effect_description", new Dictionary<string, string>()
      {
        {
          "0",
          greater.enhanceLevelReq.ToString()
        }
      }, true);
      this._nextBenefitValue.text = string.Format("+{0}%", (object) (float) ((double) greater.enhanceValue * 100.0));
    }
  }

  public void OnButtonBenefitNameClicked()
  {
    UIManager.inst.OpenPopup("HelpPopup", (Popup.PopupParameter) new HelpPopup.Parameter()
    {
      id = "help_equipment_set_enhancement"
    });
  }
}
