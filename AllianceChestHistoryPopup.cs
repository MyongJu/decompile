﻿// Decompiled with JetBrains decompiler
// Type: AllianceChestHistoryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceChestHistoryPopup : Popup
{
  private List<AllianceChestHistoryItem> _AllAllianceChestHistoryItem = new List<AllianceChestHistoryItem>();
  private List<AllianceChestHistoryData> _allAllianceChestHistoryData = new List<AllianceChestHistoryData>();
  [SerializeField]
  private UIScrollView _ScrollView;
  [SerializeField]
  private UITable _TableContainer;
  [SerializeField]
  private AllianceChestHistoryItem _AllianceChestHistoryItemTemplate;
  [SerializeField]
  private GameObject _rootHaveRecord;
  [SerializeField]
  private GameObject _rootHaveNoRecord;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._AllianceChestHistoryItemTemplate.gameObject.SetActive(false);
    this._allAllianceChestHistoryData.Clear();
    RequestManager.inst.SendRequest("TreasuryVault:getUserTreasuryHistory", (Hashtable) null, (System.Action<bool, object>) ((result, data) =>
    {
      ArrayList arrayList = data as ArrayList;
      if (arrayList != null)
      {
        foreach (object obj in arrayList)
        {
          Hashtable hashtable = obj as Hashtable;
          AllianceChestHistoryData chestHistoryData = new AllianceChestHistoryData();
          chestHistoryData.Decode(hashtable);
          this._allAllianceChestHistoryData.Add(chestHistoryData);
        }
      }
      this.UpdateUI();
    }), true);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.OnClose(orgParam);
  }

  private void UpdateUI()
  {
    this._rootHaveNoRecord.SetActive(this._allAllianceChestHistoryData.Count <= 0);
    this._rootHaveRecord.SetActive(this._allAllianceChestHistoryData.Count > 0);
    this.DestoryAllChestReceiveDetailItem();
    if (this._allAllianceChestHistoryData.Count <= 0)
      return;
    using (List<AllianceChestHistoryData>.Enumerator enumerator = this._allAllianceChestHistoryData.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.CreateAllianceChestHistoryItem().SetData(enumerator.Current);
    }
    this._TableContainer.Reposition();
    this._ScrollView.ResetPosition();
  }

  private AllianceChestHistoryItem CreateAllianceChestHistoryItem()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._AllianceChestHistoryItemTemplate.gameObject);
    gameObject.transform.SetParent(this._TableContainer.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    AllianceChestHistoryItem component = gameObject.GetComponent<AllianceChestHistoryItem>();
    this._AllAllianceChestHistoryItem.Add(component);
    return component;
  }

  private void DestoryAllChestReceiveDetailItem()
  {
    using (List<AllianceChestHistoryItem>.Enumerator enumerator = this._AllAllianceChestHistoryItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this._AllAllianceChestHistoryItem.Clear();
  }

  public void OnButtonCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }
}
