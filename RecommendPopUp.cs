﻿// Decompiled with JetBrains decompiler
// Type: RecommendPopUp
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;
using UnityEngine;

public class RecommendPopUp : Popup
{
  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnDisplay()
  {
    this.OnCloseHandler();
    this.Open();
    RequestManager.inst.SendLoader("gift:commentReward", (Hashtable) null, (System.Action<bool, object>) null, true, false);
  }

  private void Open()
  {
    switch (Application.platform)
    {
      case RuntimePlatform.IPhonePlayer:
        this.OpenAppStore();
        break;
      case RuntimePlatform.Android:
        this.OpenGoogleStore();
        break;
    }
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    PlayerPrefsEx.SetIntByUid("recommend_" + (object) (orgParam as RecommendPopUp.Parameter).level, 1);
    GameEngine.Instance.LockEscape();
    TutorialManager.Instance.Pause();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    GameEngine.Instance.UnLockEscape();
    TutorialManager.Instance.Resume();
  }

  private void OpenAppStore()
  {
  }

  private void OpenGoogleStore()
  {
    new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity").Call("startActivity", new object[1]
    {
      (object) new AndroidJavaObject("android.content.Intent", new object[2]
      {
        (object) new AndroidJavaClass("android.content.Intent").GetStatic<string>("ACTION_VIEW"),
        (object) new AndroidJavaClass("android.net.Uri").CallStatic<AndroidJavaObject>("parse", new object[1]
        {
          (object) "market://details?id=com.funplus.kingofavalon"
        })
      })
    });
  }

  public class Parameter : Popup.PopupParameter
  {
    public int level;
  }
}
