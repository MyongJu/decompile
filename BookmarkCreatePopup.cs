﻿// Decompiled with JetBrains decompiler
// Type: BookmarkCreatePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;

public class BookmarkCreatePopup : Popup
{
  private int m_SelectedIndex = -1;
  public UILabel m_K;
  public UILabel m_X;
  public UILabel m_Y;
  public UIToggle[] m_Toggles;
  public UIInput m_Input;
  private BookmarkCreatePopup.Parameter m_Parameter;
  private Coordinate m_Location;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as BookmarkCreatePopup.Parameter;
    this.m_Location = this.m_Parameter.tileData.Location;
    this.m_SelectedIndex = -1;
    this.UpdateUI();
  }

  public void UpdateUI()
  {
    this.m_K.text = this.m_Location.K.ToString();
    this.m_X.text = this.m_Location.X.ToString();
    this.m_Y.text = this.m_Location.Y.ToString();
    this.m_Input.value = this.GetDefaultName(this.m_Parameter.tileData);
    this.m_Toggles[0].Set(true);
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnToggleChanged()
  {
    this.m_SelectedIndex = this.GetSelectedIndex();
  }

  public void OnCreatePressed()
  {
    Hashtable postData = new Bookmark()
    {
      markName = this.m_Input.value,
      type = this.GetBookmarkType(),
      coordinate = this.m_Location
    }.Encode();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    MessageHub.inst.GetPortByAction("Player:addBookmark").SendRequest(postData, (System.Action<bool, object>) ((_param0, _param1) => ToastManager.Instance.AddBasicToast("toast_bookmark_success")), true);
    this.OnClosePressed();
  }

  private int GetSelectedIndex()
  {
    for (int index = 0; index < this.m_Toggles.Length; ++index)
    {
      if (this.m_Toggles[index].value)
        return index;
    }
    return -1;
  }

  private string GetDefaultName(TileData tileData)
  {
    UserData userData = tileData.UserData;
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(tileData.AllianceID);
    switch (tileData.TileType)
    {
      case TileType.City:
        return userData.userName_Alliance_Name;
      case TileType.Camp:
        return userData.userName_Alliance_Name + " " + Utils.XLAT("PVE_PLAYERCAMP_NAME");
      case TileType.Resource1:
        return Utils.XLAT("tiles_farm_l1_01_name");
      case TileType.Resource2:
        return Utils.XLAT("tiles_lumber_l1_01_name");
      case TileType.Resource3:
        return Utils.XLAT("tiles_mine_l1_01_name");
      case TileType.Resource4:
        return Utils.XLAT("tiles_house_l1_01_name");
      case TileType.Wonder:
        return Utils.XLAT("throne_wonder_name");
      case TileType.WonderTower:
        WonderTowerData wonderTowerData = tileData.WonderTowerData;
        if (wonderTowerData != null && !string.IsNullOrEmpty(wonderTowerData.Name))
          return wonderTowerData.Name;
        break;
      case TileType.GveCamp:
        return Utils.XLAT("gve_tile_name");
      case TileType.AllianceFortress:
        AllianceFortressData fortressData = tileData.FortressData;
        if (fortressData != null)
        {
          AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(fortressData.ConfigId);
          return "[" + allianceData.allianceAcronym + "]" + (string.IsNullOrEmpty(fortressData.Name) ? buildingStaticInfo.LocalName : fortressData.Name);
        }
        break;
      case TileType.AllianceWarehouse:
        AllianceWareHouseData wareHouseData = tileData.WareHouseData;
        if (wareHouseData != null)
        {
          AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(wareHouseData.ConfigId);
          return "[" + allianceData.allianceAcronym + "]" + (string.IsNullOrEmpty(wareHouseData.Name) ? buildingStaticInfo.LocalName : wareHouseData.Name);
        }
        break;
      case TileType.AllianceSuperMine:
        AllianceSuperMineData superMineData = tileData.SuperMineData;
        if (superMineData != null)
        {
          AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(superMineData.ConfigId);
          return "[" + allianceData.allianceAcronym + "]" + (string.IsNullOrEmpty(superMineData.Name) ? buildingStaticInfo.LocalName : superMineData.Name);
        }
        break;
      case TileType.AlliancePortal:
        AlliancePortalData portalData = tileData.PortalData;
        if (portalData != null)
        {
          AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(portalData.ConfigId);
          return "[" + allianceData.allianceAcronym + "]" + (string.IsNullOrEmpty(portalData.Name) ? buildingStaticInfo.LocalName : portalData.Name);
        }
        break;
      case TileType.AllianceHospital:
        AllianceHospitalData hospitalData = tileData.HospitalData;
        if (hospitalData != null)
        {
          AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(hospitalData.ConfigId);
          return "[" + allianceData.allianceAcronym + "]" + (string.IsNullOrEmpty(hospitalData.Name) ? buildingStaticInfo.LocalName : hospitalData.Name);
        }
        break;
      case TileType.AllianceTemple:
        AllianceTempleData templeData = tileData.TempleData;
        if (templeData != null)
        {
          AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(templeData.ConfigId);
          return "[" + allianceData.allianceAcronym + "]" + (string.IsNullOrEmpty(templeData.Name) ? buildingStaticInfo.LocalName : templeData.Name);
        }
        break;
      case TileType.PitMine:
        return Utils.XLAT("event_fire_kingdom_dragon_nest_name");
    }
    return Utils.XLAT(PVPMapData.MapData.Tileset[this.m_Parameter.tileData.TileID].PrefabName + "_name");
  }

  private int GetBookmarkType()
  {
    switch (this.m_SelectedIndex)
    {
      case 0:
        return 0;
      case 1:
        return 1;
      case 2:
        return 2;
      default:
        return 0;
    }
  }

  private int GetIndex(int type)
  {
    switch (type)
    {
      case 0:
        return 0;
      case 1:
        return 1;
      case 2:
        return 2;
      default:
        return 0;
    }
  }

  public class Parameter : Popup.PopupParameter
  {
    public TileData tileData;
  }
}
