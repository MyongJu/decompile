﻿// Decompiled with JetBrains decompiler
// Type: PreOpenBundleUpdateState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PreOpenBundleUpdateState : LoadBaseState
{
  private Stack<string> preopenversionlist = new Stack<string>();
  private HashSet<string> cachingPreOpenBundle = new HashSet<string>();
  private int delay = 2;
  private int _max = 2;
  private const int maxthread = 1;
  [DevSetting(null, "General", null)]
  public static bool skipOpeningCinematic;
  private int total;
  private bool isSkipIAP;
  private bool isSkipAll;
  private int _count;
  private bool _requireLoadFinish;
  private bool _isInitLoader;

  public PreOpenBundleUpdateState(int step)
    : base(step)
  {
  }

  public override string Key
  {
    get
    {
      return nameof (PreOpenBundleUpdateState);
    }
  }

  protected override SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.PreOpenUpdate;
    }
  }

  protected override void Prepare()
  {
    base.Prepare();
    BundleManager.Instance.onBundleLoaded += new System.Action<string, bool>(this.OnBundleLoadFinished);
    if (AssetManager.IsLoadAssetFromBundle)
      this.InitPreOpenList();
    if (!PreOpenBundleUpdateState.skipOpeningCinematic)
    {
      OpeningCinematicManager.Instance.onOpeningCinematicTerminate += new System.Action(this.OnCinematicTerminate);
      OpeningCinematicManager.Instance.Startup();
    }
    else
      GameEngine.Instance.canStartGame = true;
    if (!AssetManager.IsLoadAssetFromBundle)
      this.Finish();
    else
      this.BeginUpdatePreOpenBundle();
  }

  private void OnCinematicTerminate()
  {
    this.SpeedUpUpdating();
    this.CheckCanStart();
    if (Application.platform != RuntimePlatform.Android)
      return;
    GooglePlayGameManager.Instance.Login();
  }

  private void UpdateEventHandler(double t)
  {
    GameEngine.Instance.iTweenValue = Mathf.Lerp(GameEngine.Instance.iTweenValue, SplashDataConfig.GetValue(this.CurrentPhase), 0.02f);
    this.RefreshProgress(GameEngine.Instance.iTweenValue);
  }

  private void OnBundleLoadFinished(string name, bool success)
  {
    int length = name.IndexOf(".assetbundle");
    if (length != -1)
      name = name.Substring(0, length);
    if (success)
    {
      if (!this.cachingPreOpenBundle.Contains(name))
        return;
      this.cachingPreOpenBundle.Remove(name);
      this.RefreshCurrentProgress();
      if (this.preopenversionlist.Count > 0)
      {
        string bundlename = this.preopenversionlist.Pop();
        this.cachingPreOpenBundle.Add(bundlename);
        BundleManager.Instance.CacheBundle(bundlename);
      }
      else
      {
        if (this.cachingPreOpenBundle.Count != 0)
          return;
        this.Finish();
      }
    }
    else
    {
      string title = ScriptLocalization.Get("network_error_try_again_button", true);
      string content = ScriptLocalization.Get("network_error_description", true);
      NetWorkDetector.Instance.RetryTip((System.Action) (() => BundleManager.Instance.CacheBundle(name)), title, content);
    }
  }

  private void RefreshCurrentProgress()
  {
    float num1 = (float) (1.0 - (double) this.preopenversionlist.Count / (double) this.total);
    float num2 = SplashDataConfig.GetValue(SplashDataConfig.Phase.LoadPreUI);
    float num3 = SplashDataConfig.GetValue(this.CurrentPhase);
    float num4 = num2 + (num3 - num2) * num1;
    float num5 = SplashDataConfig.GetValue(this.CurrentPhase);
    if ((double) num4 > (double) num5)
      num4 = num5;
    this.RefreshProgress(num4);
  }

  protected override void RefreshProgress(float value)
  {
    string str = string.Empty;
    if (this.total > 0)
      str = string.Format(" {0}/{1}", (object) (this.total - this.preopenversionlist.Count), (object) this.total);
    this.Preloader.SplashScreen(SplashDataConfig.GetTipFromPhaseKey(this.CurrentPhase) + str, SplashDataConfig.GetValue(this.CurrentPhase) * value);
  }

  private void InitPreOpenList()
  {
    using (Dictionary<string, string>.Enumerator enumerator = VersionManager.Instance.Newversionlist.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, string> current = enumerator.Current;
        if (current.Key.Contains("static+"))
        {
          string t = current.Key.Substring(0, current.Key.IndexOf(".assetbundle"));
          if (!LoadBundlesState.preloadBundle.Contains(t) && !LoadLocalizationState.localizeBundle.Contains(t))
            this.preopenversionlist.Push(t);
        }
      }
    }
    this.total = this.preopenversionlist.Count;
  }

  private void BeginUpdatePreOpenBundle()
  {
    for (int index = 0; index < 1 && this.preopenversionlist.Count > 0; ++index)
    {
      string bundlename = this.preopenversionlist.Pop();
      this.cachingPreOpenBundle.Add(bundlename);
      BundleManager.Instance.CacheBundle(bundlename);
    }
  }

  protected override void Finish()
  {
    if (this.IsDestroy)
      return;
    base.Finish();
    BundleManager.Instance.onBundleLoaded -= new System.Action<string, bool>(this.OnBundleLoadFinished);
    this.CheckCanStart();
  }

  private void SpeedUpUpdating()
  {
    for (int index = 0; index < SplashDataConfig.CACHE_BUNDLE_COUT_FRAME - 1 && this.preopenversionlist.Count > 0; ++index)
    {
      string bundlename = this.preopenversionlist.Pop();
      this.cachingPreOpenBundle.Add(bundlename);
      BundleManager.Instance.CacheBundle(bundlename);
    }
  }

  private void CheckCanStart()
  {
    if (!this._isInitLoader)
    {
      this._isInitLoader = true;
      this.CheckSkip();
      this.AddLoaderObserer();
      this.ImmediatelyLoader();
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.Process);
      Oscillator.Instance.updateEvent += new System.Action<double>(this.Process);
    }
    if (GameEngine.Instance.canStartGame)
      GameEngine.Instance.StartGame();
    else
      GameEngine.Instance.canStartGame = true;
  }

  private void Process(double time)
  {
    if (!GameEngine.IsReady() || !this._requireLoadFinish)
      return;
    if (this.delay <= 0)
    {
      if (Oscillator.IsAvailable)
        Oscillator.Instance.updateEvent -= new System.Action<double>(this.Process);
      UIManager.inst.splashScreen.Hide();
      this.Clear();
      this.Preloader.OnPreOpenFinish();
    }
    else
      --this.delay;
  }

  private void CheckSkip()
  {
    if (!TutorialManager.Instance.CheckTutorialIsFinish())
    {
      this.isSkipAll = true;
    }
    else
    {
      this.isSkipAll = false;
      this.isSkipIAP = PlayerData.inst.playerCityData.level < 6;
    }
  }

  private bool CheckTutorial()
  {
    if (NewTutorial.skipTutorial)
      return false;
    string empty = string.Empty;
    return "finished" != (TutorialManager.Instance.Resolution != TutorialManager.ABTestType.Basic ? NewTutorial.Instance.GetRecordPoint("testb_beginner") : NewTutorial.Instance.GetRecordPoint("beginner"));
  }

  private void Clear()
  {
    this._requireLoadFinish = false;
    this._count = 0;
  }

  private void SendLoader()
  {
    RequestManager.inst.SendLoader("PVP:getWatchTowerEvents", Utils.Hash((object) "uid", (object) PlayerData.inst.uid), (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!GameEngine.IsReady() || !arg1)
        return;
      WatchtowerUtilities.Init();
    }), true, false);
    RequestManager.inst.SendLoader("sevenDays:loadSevenDays", (Hashtable) null, (System.Action<bool, object>) null, true, false);
    ActivityManager.Intance.CheckPersonalActivity();
    PlayerData.inst.allianceWarManager.OnGameModeReady();
    PlayerData.inst.allianceTechManager.OnGameModeReady();
    PaymentManager.Instance.GetPayStatus();
    MarksmanPayload.Instance.Initialize();
    IapRebatePayload.Instance.Initialize();
    RebateByRechargePayload.Instance.Initialize();
    SuperLoginPayload.Instance.Initialize();
    KingdomBuffPayload.Instance.Initialize();
    LotteryDiyPayload.Instance.Initialize();
    DicePayload.Instance.Initialize();
    TradingHallPayload.Instance.Initialize();
    AnniversaryIapPayload.Instance.Initialize();
    AccountManager.Instance.LogUserInfo();
    AllianceManager.Instance.OnGameModeReady();
    HeroCardUtils.LoadHeroCardDataByLoader();
    PitExplorePayload.Instance.SyncAbyssInfo();
    SubscriptionSystem.Instance.RequestSubscribeDataNonBlock();
    IAPDailyRechargePackagePayload.Instance.Initialize();
    AnniversaryManager.Instance.Initialize();
    AuctionHelper.Instance.Initialize();
    ChatAndMailConstraint.Instance.Initialize();
    MerlinTrialsPayload.Instance.Initialize();
    RoulettePayload.Instance.Initialize();
    AllianceWarPayload.Instance.Initialize();
    MerlinTowerPayload.Instance.Initialize();
  }

  private void ImmediatelyLoader()
  {
    ActivityManager.Intance.OnGameModeReady();
    IAPStorePackagePayload.Instance.Initialize();
  }

  private void AddLoaderObserer()
  {
    RequestManager.inst.AddObserver("activity:getIntegralActivityState", new System.Action<string, bool>(this.OnLoadCallBack));
    RequestManager.inst.AddObserver("Player:getIapPackage", new System.Action<string, bool>(this.OnLoadCallBack));
  }

  private void RemoveLoaderObserer()
  {
    RequestManager.inst.RemoveObserver("activity:getIntegralActivityState", new System.Action<string, bool>(this.OnLoadCallBack));
    RequestManager.inst.RemoveObserver("Player:getIapPackage", new System.Action<string, bool>(this.OnLoadCallBack));
  }

  private void OnLoadCallBack(string action, bool result)
  {
    ++this._count;
    if (this.isSkipAll)
      this.RequiredLoadFinish();
    else if (this._max == this._count)
    {
      this.RequiredLoadFinish();
    }
    else
    {
      if (!this.isSkipIAP || !(action == "activity:getIntegralActivityState"))
        return;
      this.RequiredLoadFinish();
    }
  }

  private void RequiredLoadFinish()
  {
    this.RemoveLoaderObserer();
    this.SendLoader();
    this._requireLoadFinish = true;
  }
}
