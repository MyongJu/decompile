﻿// Decompiled with JetBrains decompiler
// Type: SimpleVfx
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class SimpleVfx : VfxBase
{
  [Tooltip("-1 means loop")]
  public float duration = -1f;
  public Vector3 offset = Vector3.zero;
  public Vector3 rotation = Vector3.zero;
  public Vector3 scale = Vector3.one;
  public float startTime;
  public bool oneShot;
  public string dummyKey;
  protected bool autoDelete;
  private bool played;

  private void Awake()
  {
    this.autoDelete = Application.isEditor && !Application.isPlaying;
    if (!this.autoDelete)
      return;
    this.Play(this.transform.parent);
  }

  private void OnDisable()
  {
    if (!this.oneShot || !this.played)
      return;
    this.Delete();
  }

  public override void Play(Transform target)
  {
    this.Attach(target, this.offset, this.rotation, this.scale);
    this.PlayImpl(this.startTime);
    this.played = true;
  }

  public override void Play(IVfxTarget target)
  {
    this.Play(target.GetDummyPoint(this.dummyKey));
  }

  protected virtual void Update()
  {
    if (this.IsPlaying && ((double) this.duration < 0.0 || (double) Time.time - (double) this.StartTime <= (double) this.duration))
      return;
    this.OnFinish();
  }

  public void OnFinish()
  {
    if (this.autoDelete)
      this.Delete();
    else
      this.Stop();
  }
}
