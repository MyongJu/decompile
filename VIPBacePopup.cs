﻿// Decompiled with JetBrains decompiler
// Type: VIPBacePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class VIPBacePopup : UI.Dialog
{
  private List<GameObject> renders = new List<GameObject>();
  private int _nextVipLevel = -1;
  private Vector3 lastScrollPosition = Vector3.zero;
  public UILabel topVipLevel;
  public UILabel sliderTopLabel;
  public UILabel sliderBottomLabel;
  public UILabel sliderLabel;
  public UISlider slider;
  public UITexture playerIconTexture;
  public UIButton helpBtn;
  public UILabel vipLeftLevelLabel;
  public UILabel vipRightLevelLabel;
  public UIGrid entriesGrid;
  public VIPEntryRender templateDark;
  public VIPEntryRender templateBright;
  public UIButton leftBtn;
  public UIButton rightBtn;
  public UILabel vipRemainingLabel;
  public UIScrollView scrollView;
  public UIButton activeBtn;
  public UIButton activingBtn;
  public GameObject actvtingGameobject;
  public GameObject vipOnGameobject;
  public UIButton addVIPPointBtn;
  private VIP_Level_Info levelInfo;
  private VIP_Level_Info nextLevelInfo;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    VIPBacePopup.Parameter parameter = orgParam as VIPBacePopup.Parameter;
    if (parameter != null)
      this._nextVipLevel = parameter.nextVipLevel;
    this.levelInfo = ConfigManager.inst.DB_VIP_Level.VIPInfoForPoints((int) DBManager.inst.DB_User.Get(PlayerData.inst.uid).vipPoint);
    if (this.levelInfo.Level == ConfigManager.inst.DB_VIP_Level.datas.Count)
      this.levelInfo = ConfigManager.inst.DB_VIP_Level.levelDatas[this.levelInfo.Level - 1];
    this.nextLevelInfo = ConfigManager.inst.DB_VIP_Level.levelDatas[this.levelInfo.Level + 1];
    if (this._nextVipLevel > 1 && this._nextVipLevel <= ConfigManager.inst.DB_VIP_Level.datas.Count)
    {
      this.levelInfo = ConfigManager.inst.DB_VIP_Level.levelDatas[this._nextVipLevel - 1];
      this.nextLevelInfo = ConfigManager.inst.DB_VIP_Level.levelDatas[this._nextVipLevel];
    }
    CustomIconLoader.Instance.requestCustomIcon(this.playerIconTexture, PlayerData.inst.userData.PortraitIconPath, PlayerData.inst.userData.Icon, false);
    this.AddEvtListener();
    this.DrawTop();
    this.UpdateUI();
    this.ConfigBtns();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.RemoveEvtListener();
    BuilderFactory.Instance.Release((UIWidget) this.playerIconTexture);
    base.OnHide(orgParam);
  }

  public void OnCloseBtnClose()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void AddEvtListener()
  {
    this.leftBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnLeftBtnClick)));
    this.rightBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnRightBtnClick)));
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnPlayerStatChanged);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.UpdateTimer);
    this.activeBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnActiveBtnClick)));
    this.activingBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnActiveBtnClick)));
    this.addVIPPointBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnAddPointBtnClick)));
  }

  private void RemoveEvtListener()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.UpdateTimer);
    this.activeBtn.onClick.Clear();
    this.leftBtn.onClick.Clear();
    this.rightBtn.onClick.Clear();
    this.helpBtn.onClick.Clear();
    this.addVIPPointBtn.onClick.Clear();
    this.activingBtn.onClick.Clear();
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnPlayerStatChanged);
  }

  private void OnActiveBtnClick()
  {
    UIManager.inst.OpenDlg("VIP/VIPItemsDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnAddPointBtnClick()
  {
    UIManager.inst.OpenDlg("VIP/VIPPointItemsDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnHelpBtnClick()
  {
    UIManager.inst.OpenPopup("MessageBox", (Popup.PopupParameter) new MessageBox.Parameter()
    {
      Title = "VIP HELP",
      Content = Utils.XLAT("vip_mail_vip_info_content")
    });
  }

  private void UpdateUI()
  {
    this.ClearGrid();
    this.vipLeftLevelLabel.text = this.levelInfo.Level.ToString();
    this.vipRightLevelLabel.text = this.nextLevelInfo.Level.ToString();
    int num1 = int.MinValue;
    int num2 = 0;
    for (int index = 0; index < this.nextLevelInfo.Effects.Count; ++index)
    {
      Effect effect1 = this.nextLevelInfo.Effects[index];
      if ((double) Mathf.Abs(effect1.Modifier) > 1.0 / 1000.0)
      {
        Effect sameEffect = this.levelInfo.GetSameEffect(effect1);
        Effect effect2 = sameEffect == null ? new Effect(effect1.PropertyInternalID, effect1.PrivilegeInternalID, 0.0f, effect1.Source, effect1.SourceID) : sameEffect;
        GameObject gameObject1;
        GameObject gameObject2;
        if ((double) Mathf.Abs(effect2.Modifier) > 1.0 / 1000.0)
        {
          gameObject1 = NGUITools.AddChild(this.entriesGrid.gameObject, this.templateBright.gameObject);
          GameObject gameObject3 = gameObject1;
          int num3 = num2;
          int num4 = num3 + 1;
          string str1 = num3.ToString();
          gameObject3.name = str1;
          gameObject1.GetComponent<VIPEntryRender>().Init(effect2, false);
          gameObject2 = NGUITools.AddChild(this.entriesGrid.gameObject, this.templateBright.gameObject);
          gameObject2.GetComponent<VIPEntryRender>().Init(effect1, (double) Mathf.Abs(effect1.Modifier) - (double) Mathf.Abs(effect2.Modifier) > 9.99999974737875E-05);
          GameObject gameObject4 = gameObject2;
          int num5 = num4;
          num2 = num5 + 1;
          string str2 = num5.ToString();
          gameObject4.name = str2;
        }
        else
        {
          gameObject1 = NGUITools.AddChild(this.entriesGrid.gameObject, this.templateDark.gameObject);
          GameObject gameObject3 = gameObject1;
          int num3 = num1;
          int num4 = num3 + 1;
          string str1 = num3.ToString();
          gameObject3.name = str1;
          gameObject1.GetComponent<VIPEntryRender>().Init(effect2, false);
          gameObject2 = NGUITools.AddChild(this.entriesGrid.gameObject, this.templateBright.gameObject);
          gameObject2.GetComponent<VIPEntryRender>().Init(effect1, (double) Mathf.Abs(effect1.Modifier) - (double) Mathf.Abs(effect2.Modifier) > 9.99999974737875E-05);
          GameObject gameObject4 = gameObject2;
          int num5 = num4;
          num1 = num5 + 1;
          string str2 = num5.ToString();
          gameObject4.name = str2;
        }
        this.renders.Add(gameObject1);
        this.renders.Add(gameObject2);
      }
    }
    this.entriesGrid.repositionNow = true;
    this.entriesGrid.sorting = UIGrid.Sorting.Custom;
    this.entriesGrid.onCustomSort += (Comparison<Transform>) ((x, y) => int.Parse(x.gameObject.name).CompareTo(int.Parse(y.gameObject.name)));
    using (List<GameObject>.Enumerator enumerator = this.renders.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetActive(true);
    }
    this.scrollView.MoveAbsolute(this.lastScrollPosition);
    this.scrollView.RestrictWithinBounds(true);
    this.UpdateTimer(0);
  }

  private void DrawTop()
  {
    VIP_Level_Info levelData = ConfigManager.inst.DB_VIP_Level.levelDatas[Mathf.Min(ConfigManager.inst.DB_VIP_Level.levelDatas[Mathf.Max(ConfigManager.inst.DB_VIP_Level.VIPInfoForPoints((int) DBManager.inst.DB_User.Get(PlayerData.inst.uid).vipPoint).Level, this._nextVipLevel - 1)].Level + 1, ConfigManager.inst.DB_VIP_Level.datas.Count)];
    this.topVipLevel.text = PlayerData.inst.hostPlayer.VIPLevel.ToString();
    int vipPoints = PlayerData.inst.hostPlayer.VIPPoints;
    int reqValue1 = levelData.req_value_1;
    this.sliderLabel.text = vipPoints >= reqValue1 ? ScriptLocalization.Get("dragon_max_level", true) : string.Format("{0} / {1}", (object) vipPoints, (object) reqValue1);
    this.slider.value = vipPoints >= reqValue1 ? 1f : (float) vipPoints / (float) reqValue1;
    this.sliderTopLabel.text = vipPoints >= reqValue1 ? string.Empty : string.Format("{0}" + Utils.XLAT("vip_points_needed_to_upgrade_description"), (object) string.Empty, (object) Utils.FormatThousands((reqValue1 - vipPoints).ToString()), (object) levelData.Level);
    this.sliderBottomLabel.text = string.Format("{0}" + Utils.XLAT("vip_points_today_description"), (object) string.Empty, (object) PlayerData.inst.hostPlayer.LoginBonus.ToString());
  }

  private void UpdateTimer(int timeStamp)
  {
    if (PlayerData.inst.hostPlayer.VIPActive)
    {
      this.vipRemainingLabel.text = Utils.ConvertSecsToString((double) (JobManager.Instance.GetJob(PlayerData.inst.hostPlayer.VipJobId).EndTime() - NetServerTime.inst.ServerTimestamp));
      this.activeBtn.gameObject.SetActive(false);
      this.actvtingGameobject.SetActive(true);
      GreyUtility.Normal(this.vipOnGameobject);
      this.topVipLevel.color = Color.yellow;
    }
    else
    {
      this.activeBtn.gameObject.SetActive(true);
      this.actvtingGameobject.SetActive(false);
      GreyUtility.Grey(this.vipOnGameobject);
      this.topVipLevel.color = Color.grey;
    }
  }

  private void ConfigBtns()
  {
    this.leftBtn.gameObject.SetActive(this.levelInfo.Level > 1);
    this.rightBtn.gameObject.SetActive(this.nextLevelInfo.Level < ConfigManager.inst.DB_VIP_Level.datas.Count);
  }

  private void OnLeftBtnClick()
  {
    this.lastScrollPosition = this.scrollView.transform.localPosition;
    this.levelInfo = ConfigManager.inst.DB_VIP_Level.levelDatas[this.levelInfo.Level - 1];
    this.nextLevelInfo = ConfigManager.inst.DB_VIP_Level.levelDatas[this.levelInfo.Level + 1];
    this.UpdateUI();
    this.ConfigBtns();
  }

  private void OnRightBtnClick()
  {
    this.lastScrollPosition = this.scrollView.transform.localPosition;
    int level = this.levelInfo.Level;
    this.levelInfo = ConfigManager.inst.DB_VIP_Level.levelDatas[level + 1];
    this.nextLevelInfo = ConfigManager.inst.DB_VIP_Level.levelDatas[level + 2];
    this.UpdateUI();
    this.ConfigBtns();
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.entriesGrid);
    this.renders.Clear();
  }

  private void OnPlayerStatChanged(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.UpdateUI();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int nextVipLevel = -1;
  }
}
