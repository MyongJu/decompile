﻿// Decompiled with JetBrains decompiler
// Type: HintPrefab
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class HintPrefab : MonoBehaviour
{
  public Vector2 frontPos;
  public Vector2 backPos;
  public Vector2 topPos;
  public Vector2 strongholdPos;
  public Vector3 wScale;
  public float focusTime;
  public GameObject front;
  public GameObject back;
  public GameObject top;
  private object para;

  [HideInInspector]
  public object Para
  {
    get
    {
      return this.para;
    }
    set
    {
      this.para = value;
    }
  }

  internal void Init(int mode)
  {
    NGUITools.SetLayer(this.gameObject, this.transform.parent.gameObject.layer);
    if (GameEngine.Instance.TileLayer == this.gameObject.layer)
      UIManager.inst.cityCamera.SetTargetPosition(this.transform.parent.position, true, false);
    else if (GameEngine.Instance.UILayer == this.gameObject.layer)
      this.transform.localScale = this.wScale;
    switch (mode)
    {
      case 1:
        this.transform.localPosition = Vector3.Scale((Vector3) this.strongholdPos, this.transform.localScale);
        this.ShiftAnim(HintPrefab.AnimMode.front);
        break;
      case 2:
        this.transform.localPosition = Vector3.Scale((Vector3) this.backPos, this.transform.localScale);
        this.ShiftAnim(HintPrefab.AnimMode.back);
        break;
      case 3:
        this.transform.localPosition = Vector3.Scale((Vector3) this.topPos, this.transform.localScale);
        this.ShiftAnim(HintPrefab.AnimMode.top);
        break;
      default:
        this.transform.localPosition = Vector3.Scale((Vector3) this.frontPos, this.transform.localScale);
        this.ShiftAnim(HintPrefab.AnimMode.front);
        break;
    }
  }

  internal void Dispose()
  {
    this.gameObject.SetActive(false);
  }

  private void ShiftAnim(HintPrefab.AnimMode m)
  {
    switch (m)
    {
      case HintPrefab.AnimMode.front:
        this.front.SetActive(true);
        this.back.SetActive(false);
        this.top.SetActive(false);
        break;
      case HintPrefab.AnimMode.back:
        this.front.SetActive(false);
        this.back.SetActive(true);
        this.top.SetActive(false);
        break;
      case HintPrefab.AnimMode.top:
        this.front.SetActive(false);
        this.back.SetActive(false);
        this.top.SetActive(true);
        break;
    }
  }

  private enum AnimMode
  {
    front,
    back,
    top,
  }
}
