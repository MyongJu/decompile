﻿// Decompiled with JetBrains decompiler
// Type: HospitalHealDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HospitalHealDlg : UI.Dialog
{
  private bool isInQueueAllState = true;
  public HealUnitRender renderTemplate;
  public UIScrollView scrollView;
  public UIGrid grid;
  public HealActionPanel actionPanel;
  public UILabel capacityLabel;
  public UISlider capacitySlider;
  public GameObject emptyMessage;
  private List<HealTroopInfo> healList;
  private List<HealUnitRender> renders;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
    MessageHub.inst.GetPortByAction("job_complete").AddEvent(new System.Action<object>(this.OnJobCompleted));
    DBManager.inst.DB_CityMap.onDataChanged += new System.Action<CityMapData>(this.OnCityMapDataChange);
    DBManager.inst.DB_City.onDataChanged += new System.Action<long>(this.OnCityDataChanged);
    JobManager.Instance.OnJobRemove += new System.Action<long>(this.JobManager_Instance_OnJobRemove);
    JobManager.Instance.OnJobCreated += new System.Action<long>(this.JobManager_Instance_OnJobCreated);
    CityManager.inst.onResourcesUpdated += new CityManager.OnResourcesUpdated(this.OnResourcesUpdated);
  }

  private void OnResourcesUpdated(long food, long wood, long silver, long ore, long gold)
  {
    this.actionPanel.Init(this.healList, new System.Action(this.OnQueueAllPressed));
  }

  private void OnCityDataChanged(long cityId)
  {
    if (cityId != (long) PlayerData.inst.cityId)
      return;
    this.Init();
  }

  private void JobManager_Instance_OnJobCreated(long jobId)
  {
    JobHandle job = JobManager.Instance.GetJob(jobId);
    if (!(bool) job || job.GetJobEvent() != JobEvent.JOB_HEALING_TROOPS)
      return;
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void JobManager_Instance_OnJobRemove(long jobId)
  {
    JobHandle job = JobManager.Instance.GetJob(jobId);
    if (!(bool) job || job.GetJobEvent() != JobEvent.JOB_HEALING_TROOPS)
      return;
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.Dispose();
    base.OnHide(orgParam);
  }

  private void Init()
  {
    this.InitHealList();
    if (CityManager.inst.GetBuildingWithActionTimer("hospital") == null)
    {
      this.CalcCanHealAmount();
      this.isInQueueAllState = false;
    }
    this.FeedData();
  }

  private void OnCityMapDataChange(CityMapData obj)
  {
    this.FeedData();
  }

  private void OnJobCompleted(object result)
  {
    Hashtable hashtable = result as Hashtable;
    int result1 = 0;
    if (hashtable == null || !hashtable.ContainsKey((object) "event_type") || (!int.TryParse(hashtable[(object) "event_type"].ToString(), out result1) || result1 != 39))
      return;
    this.Init();
  }

  private void Dispose()
  {
    MessageHub.inst.GetPortByAction("job_complete").RemoveEvent(new System.Action<object>(this.OnJobCompleted));
    DBManager.inst.DB_CityMap.onDataChanged -= new System.Action<CityMapData>(this.OnCityMapDataChange);
    JobManager.Instance.OnJobRemove -= new System.Action<long>(this.JobManager_Instance_OnJobRemove);
    JobManager.Instance.OnJobCreated -= new System.Action<long>(this.JobManager_Instance_OnJobCreated);
    DBManager.inst.DB_City.onDataChanged -= new System.Action<long>(this.OnCityDataChanged);
    CityManager.inst.onResourcesUpdated -= new CityManager.OnResourcesUpdated(this.OnResourcesUpdated);
  }

  private void FeedData()
  {
    this.DrawHealRenders();
    this.actionPanel.Init(this.healList, new System.Action(this.OnQueueAllPressed));
    this.SwitchStatus();
    this.DrawDapcity();
  }

  private void DrawDapcity()
  {
    long num = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).hospitalTroops.totalTroopsCount;
    int finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData((int) DBManager.inst.DB_Local_Benefit.Get("prop_healing_capacity_base_value").total, "calc_healing_capacity");
    if (num > (long) finalData)
      num = (long) finalData;
    this.capacityLabel.text = string.Format("{0}/{1}", (object) num, (object) finalData);
    this.capacitySlider.value = (float) num / (float) finalData;
  }

  private void InitHealList()
  {
    this.healList = new List<HealTroopInfo>();
    using (Dictionary<string, CityHealingTroopInfo>.Enumerator enumerator = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).hospitalTroops.healingTroops.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, CityHealingTroopInfo> current = enumerator.Current;
        if ((int) current.Value.totalCount > 0)
          this.healList.Add(new HealTroopInfo(current.Key, (int) current.Value.totalCount, (int) current.Value.healingCount));
      }
    }
    this.healList.Sort((Comparison<HealTroopInfo>) ((x, y) =>
    {
      Unit_StatisticsInfo data1 = ConfigManager.inst.DB_Unit_Statistics.GetData(x.ID);
      Unit_StatisticsInfo data2 = ConfigManager.inst.DB_Unit_Statistics.GetData(y.ID);
      if (data1.Troop_Tier == data2.Troop_Tier)
        return data1.Priority.CompareTo(data2.Priority);
      return data2.Troop_Tier.CompareTo(data1.Troop_Tier);
    }));
  }

  private void DrawHealRenders()
  {
    UIUtils.CleanGrid(this.grid);
    this.renders = new List<HealUnitRender>();
    this.scrollView.ResetPosition();
    for (int index = 0; index < this.healList.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.renderTemplate.gameObject);
      gameObject.SetActive(true);
      HealUnitRender component = gameObject.GetComponent<HealUnitRender>();
      component.onValueChanged += new System.Action<HealUnitRender>(this.OnUnitRenderCountChanged);
      component.Init(this.healList[index]);
      this.renders.Add(component);
    }
    Utils.ExecuteAtTheEndOfFrame((System.Action) (() =>
    {
      if (!((UnityEngine.Object) this.grid != (UnityEngine.Object) null))
        return;
      this.grid.Reposition();
    }));
    this.emptyMessage.SetActive(this.healList.Count == 0);
  }

  private void SwitchStatus()
  {
    if (CityManager.inst.GetBuildingWithActionTimer("hospital") == null)
      return;
    using (List<HealUnitRender>.Enumerator enumerator = this.renders.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.IsEnable = false;
    }
  }

  private void OnUnitRenderCountChanged(HealUnitRender render)
  {
    this.actionPanel.Init(this.healList, new System.Action(this.OnQueueAllPressed));
  }

  public void OnQueueAllPressed()
  {
    if (!this.isInQueueAllState)
    {
      this.CalcTotalHealAmount();
      this.isInQueueAllState = true;
    }
    else
    {
      this.CalcCanHealAmount();
      this.isInQueueAllState = false;
    }
    this.FeedData();
  }

  private void CalcTotalHealAmount()
  {
    for (int index = 0; index < this.healList.Count; ++index)
      this.healList[index].healing = this.healList[index].total;
  }

  private long Min(long a, long b, long c, long d, long e)
  {
    return Math.Min(Math.Min(Math.Min(Math.Min(a, b), c), d), e);
  }

  private void CalcCanHealAmount()
  {
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    long currentResource1 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD);
    long currentResource2 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD);
    long currentResource3 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE);
    long currentResource4 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
    for (int index = 0; index < this.healList.Count; ++index)
    {
      string id = this.healList[index].ID;
      float healFoodBenifit = ConfigManager.inst.DB_Unit_Statistics.GetData(id).Heal_Food_Benifit;
      float healWoodBenifit = ConfigManager.inst.DB_Unit_Statistics.GetData(id).Heal_Wood_Benifit;
      float healOreBenifit = ConfigManager.inst.DB_Unit_Statistics.GetData(id).Heal_Ore_Benifit;
      float healSilverBenifit = ConfigManager.inst.DB_Unit_Statistics.GetData(id).Heal_Silver_Benifit;
      long val1 = cityData.hospitalTroops.GetTotalCount(this.healList[index].ID);
      if ((double) healFoodBenifit > 0.0)
        val1 = Math.Min(val1, (long) ((double) currentResource1 / (double) healFoodBenifit));
      if ((double) healWoodBenifit > 0.0)
        val1 = Math.Min(val1, (long) ((double) currentResource2 / (double) healWoodBenifit));
      if ((double) healOreBenifit > 0.0)
        val1 = Math.Min(val1, (long) ((double) currentResource3 / (double) healOreBenifit));
      if ((double) healSilverBenifit > 0.0)
        val1 = Math.Min(val1, (long) ((double) currentResource4 / (double) healSilverBenifit));
      currentResource1 -= (long) ((double) val1 * (double) ConfigManager.inst.DB_Unit_Statistics.GetData(id).Heal_Food_Benifit);
      currentResource2 -= (long) ((double) val1 * (double) ConfigManager.inst.DB_Unit_Statistics.GetData(id).Heal_Wood_Benifit);
      currentResource3 -= (long) ((double) val1 * (double) ConfigManager.inst.DB_Unit_Statistics.GetData(id).Heal_Ore_Benifit);
      currentResource4 -= (long) ((double) val1 * (double) ConfigManager.inst.DB_Unit_Statistics.GetData(id).Heal_Silver_Benifit);
      this.healList[index].healing = (int) val1;
    }
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
  }
}
