﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsHistoryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTrialsHistoryPopup : Popup
{
  private Dictionary<int, MerlinTrialsHistorySlot> _itemDict = new Dictionary<int, MerlinTrialsHistorySlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private GameObject _objNoHistory;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UITable _table;
  [SerializeField]
  private GameObject _itemPrefab;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._itemPool.Initialize(this._itemPrefab, this._table.gameObject);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnClosePress()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void ClearData()
  {
    using (Dictionary<int, MerlinTrialsHistorySlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this._table.repositionNow = true;
    this._table.Reposition();
    this._scrollView.ResetPosition();
  }

  private MerlinTrialsHistorySlot GenerateSlot(MerlinTrialsPayload.HistoryData historyData)
  {
    GameObject gameObject = this._itemPool.AddChild(this._table.gameObject);
    gameObject.SetActive(true);
    MerlinTrialsHistorySlot component = gameObject.GetComponent<MerlinTrialsHistorySlot>();
    component.SetData(historyData);
    return component;
  }

  private void UpdateUI()
  {
    this.ClearData();
    NGUITools.SetActive(this._objNoHistory, true);
    MerlinTrialsPayload.Instance.GetWarLogs((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable hashtable = data as Hashtable;
      if (hashtable != null && hashtable.ContainsKey((object) "logs"))
      {
        ArrayList arrayList = hashtable[(object) "logs"] as ArrayList;
        if (arrayList != null)
        {
          for (int key = 0; key < arrayList.Count; ++key)
          {
            MerlinTrialsHistorySlot slot = this.GenerateSlot(new MerlinTrialsPayload.HistoryData(arrayList[key] as Hashtable));
            this._itemDict.Add(key, slot);
          }
        }
      }
      NGUITools.SetActive(this._objNoHistory, this._itemDict.Count <= 0);
      this.Reposition();
    }));
  }
}
