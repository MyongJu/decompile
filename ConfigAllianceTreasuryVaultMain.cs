﻿// Decompiled with JetBrains decompiler
// Type: ConfigAllianceTreasuryVaultMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAllianceTreasuryVaultMain
{
  private Dictionary<string, AllianceTreasuryVaultMainInfo> m_DataByUniqueID;
  private Dictionary<int, AllianceTreasuryVaultMainInfo> m_DataByInternalID;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<AllianceTreasuryVaultMainInfo, string>(res as Hashtable, "ID", out this.m_DataByUniqueID, out this.m_DataByInternalID);
  }

  public void Clear()
  {
    if (this.m_DataByUniqueID != null)
      this.m_DataByUniqueID.Clear();
    if (this.m_DataByInternalID == null)
      return;
    this.m_DataByInternalID.Clear();
  }

  public AllianceTreasuryVaultMainInfo GetData(int internalId)
  {
    if (this.m_DataByInternalID.ContainsKey(internalId))
      return this.m_DataByInternalID[internalId];
    return (AllianceTreasuryVaultMainInfo) null;
  }
}
