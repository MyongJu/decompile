﻿// Decompiled with JetBrains decompiler
// Type: WonderKingUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class WonderKingUIData
{
  private string _tag;
  private int _portrait;
  private string _userName;
  private int _rank;
  private string _icon;
  private int _lordTitleId;

  public bool IsCurrentKing { get; set; }

  public long Time { get; set; }

  public int Rank
  {
    get
    {
      return this._rank;
    }
    set
    {
      this._rank = value;
    }
  }

  public string UserName
  {
    get
    {
      if (string.IsNullOrEmpty(this._tag))
        return this._userName;
      return "[" + this._tag + "]" + this._userName;
    }
    set
    {
      this._userName = value;
    }
  }

  public int Portrait
  {
    get
    {
      return this._portrait;
    }
    set
    {
      this._portrait = value;
    }
  }

  public bool IsJoinAlliance
  {
    get
    {
      return !string.IsNullOrEmpty(this._tag);
    }
  }

  public string Icon
  {
    get
    {
      return this._icon;
    }
  }

  public int LordTitleId
  {
    get
    {
      return this._lordTitleId;
    }
  }

  public bool Decode(object source)
  {
    Hashtable hashtable = source as Hashtable;
    if (hashtable == null)
      return false;
    string empty = string.Empty;
    if (hashtable.ContainsKey((object) "king_alliance_tag") && hashtable[(object) "king_alliance_tag"] != null)
      this._tag = hashtable[(object) "king_alliance_tag"].ToString();
    if (hashtable.ContainsKey((object) "king_name") && hashtable[(object) "king_name"] != null)
      this._userName = hashtable[(object) "king_name"].ToString();
    if (hashtable.ContainsKey((object) "icon") && hashtable[(object) "icon"] != null)
      this._icon = hashtable[(object) "icon"].ToString();
    if (hashtable.ContainsKey((object) "king_portrait") && hashtable[(object) "king_portrait"] != null)
      int.TryParse(hashtable[(object) "king_portrait"].ToString(), out this._portrait);
    if (hashtable.ContainsKey((object) "king_lord_title") && hashtable[(object) "king_lord_title"] != null)
      int.TryParse(hashtable[(object) "king_lord_title"].ToString(), out this._lordTitleId);
    if (hashtable.ContainsKey((object) "king_index") && hashtable[(object) "king_index"] != null)
      int.TryParse(hashtable[(object) "king_index"].ToString(), out this._rank);
    return true;
  }
}
