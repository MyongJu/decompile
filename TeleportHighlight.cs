﻿// Decompiled with JetBrains decompiler
// Type: TeleportHighlight
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TeleportHighlight : MonoBehaviour
{
  public float WALK_CROSS_FACTOR = 0.5f;
  public float VIEWPORT_TOP = 1f;
  public float VIEWPORT_BOTTOM = 2.1f;
  public float VIEWPORT_LEFT = 1.5f;
  public float VIEWPORT_RIGHT = 1.5f;
  public bool CanDestroy = true;
  public const string SHOPITEM_TELEPORT = "shopitem_targettele";
  public const string ALLIANCE_TELEPORT = "item_alliancetele";
  public const string FORTRESS_TELEPORT = "item_fortress_teleport";
  public const string KINGDOM_TELEPORT_BACK = "shopitem_excalibur_war_tele_back";
  private const float POSITION_TOLERANCE = 0.001f;
  private const float MOVEMENT_TOLERANCE = 0.001f;
  public SpriteRenderer[] Row1;
  public SpriteRenderer[] Row2;
  public BoxCollider2D[] Snap;
  public BoxCollider2D Response;
  public GameObject CityRoot;
  public GameObject CityOrigin;
  public UIButton CancelButton;
  public UIButton UseTeleport;
  public UIButton UseArtifactTeleport;
  public UIButton BuyAndUseTeleport;
  public UIButton UseAllianceTeleport;
  public UIButton UseAllianceFortressTeleport;
  public UIButton PlaceFortress;
  public UIButton KingdomTeleport;
  public UIButton AllianceWarBackTeleportButton;
  public UIButton AllianceWarTeleportButton;
  public UILabel Price;
  public UILabel LabelAllianceWarLeft;
  public UILabel LabelAllianceWarRight;
  public TeleportHighlight.OnConfirmCallback OnConfirm;
  public TeleportHighlight.OnCancelCallback OnCancel;
  private WorldCoordinate m_T;
  private WorldCoordinate m_L;
  private WorldCoordinate m_R;
  private WorldCoordinate m_B;
  private bool m_Dragging;
  private Vector3 m_LastCursorPosition;
  private Vector2 m_LastScreenPosition;
  private TileCamera m_TileCamera;
  private TeleportMode m_TeleportMode;
  private DynamicMapData m_MapData;
  private Transform m_Parent;
  private int m_AllianceTeleportToastCount;
  private bool m_AllianceTeleportToastShowing;
  private int m_AllianceBuildingID;
  private Coordinate m_InitialLocation;

  public void InitCamera(TileCamera tileCamera)
  {
    this.m_TileCamera = tileCamera;
  }

  public void Setup(TeleportMode teleportMode, DynamicMapData mapData, Transform parent, WorldCoordinate target, int allianceBuildingID, object data = null, bool keepTargetInCenter = false)
  {
    this.m_TeleportMode = teleportMode;
    this.m_MapData = mapData;
    this.m_Parent = parent;
    this.m_AllianceBuildingID = allianceBuildingID;
    this.m_InitialLocation = this.m_MapData.ConvertWorldCoordinateToKXY(target);
    if (this.m_TeleportMode == TeleportMode.ALLIANCE_TELEPORT)
    {
      this.m_AllianceTeleportToastCount = 3;
      this.ShowAllianceTeleportToast();
    }
    else if (this.m_TeleportMode == TeleportMode.FORTRESS_TELEPORT)
    {
      this.m_AllianceTeleportToastCount = 3;
      this.ShowAllianceFortressTeleportToast();
    }
    this.CanDestroy = teleportMode != TeleportMode.ALLIANCE_WAR_ENTER_TELEPORT && teleportMode != TeleportMode.ALLIANCE_WAR_TELEPORT && teleportMode != TeleportMode.ALLIANCE_WAR_BACK_TELEPORT;
    this.SetLocation(target, keepTargetInCenter);
    this.GenerateBuilding(data);
    TeleportManager.Instance.SetTeleportMode(teleportMode);
  }

  public TeleportMode GetTeleportMode()
  {
    return this.m_TeleportMode;
  }

  private void SetLocation(WorldCoordinate target, bool keepTargetInCenter = false)
  {
    this.transform.localPosition = this.m_MapData.ConvertWorldCoordinateToPixelPosition(target);
    this.transform.localRotation = Quaternion.identity;
    this.transform.localScale = Vector3.one;
    this.UpdateWorldCoords();
    if (keepTargetInCenter)
      this.AdjustCameraToTargetCenter();
    else
      this.AdjustCamera();
  }

  public void OnConfirmPressed()
  {
    Coordinate kxy = this.m_MapData.ConvertWorldCoordinateToKXY(this.m_T);
    this.TakeAction(kxy);
    if (this.OnConfirm == null)
      return;
    this.OnConfirm(kxy);
  }

  public void OnAllianceWarBackTeleport()
  {
    this.CanDestroy = true;
    if (this.m_TeleportMode == TeleportMode.ALLIANCE_WAR_ENTER_TELEPORT)
    {
      PVPSystem.Instance.Map.CurrentKXY = PlayerData.inst.playerCityData.cityLocation;
      PVPSystem.Instance.Map.GotoLocation(PlayerData.inst.playerCityData.cityLocation, false);
      UIManager.inst.Cloud.CoverCloud((System.Action) (() => UIManager.inst.Cloud.PokeCloud((System.Action) (() =>
      {
        UIManager.inst.tileCamera.StartAutoZoom();
        UIManager.inst.EnableUI2DTouchAndMouse();
      }))));
      UIManager.inst.publicHUD.ShowHUD();
    }
    else if (this.m_TeleportMode == TeleportMode.ALLIANCE_WAR_TELEPORT)
      this.AllianceWarBackTeleport();
    PVPSystem.Instance.Map.EnterNormalMode();
    AllianceWarPayload.Instance.IsInAllianceWarTeleport = false;
  }

  public void OnAllianceWarTeleport()
  {
    this.OnConfirmPressed();
  }

  public void OnWarTeleport()
  {
    this.KingdomTeleportAction(this.m_MapData.ConvertWorldCoordinateToKXY(this.m_T));
  }

  private void KingdomTeleportAction(Coordinate targetLocation)
  {
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = ScriptLocalization.Get("id_uppercase_teleport", true),
      Content = ScriptLocalization.Get("teleport_adv_confirm", true),
      Okay = ScriptLocalization.Get("id_uppercase_confirm", true),
      OkayCallback = (System.Action) (() =>
      {
        Hashtable postData = new Hashtable();
        postData[(object) "k"] = (object) targetLocation.K;
        postData[(object) "x"] = (object) targetLocation.X;
        postData[(object) "y"] = (object) targetLocation.Y;
        MessageHub.inst.GetPortByAction("city:placeInTargetKingdom").SendRequest(postData, (System.Action<bool, object>) ((ret, orgData) =>
        {
          if (ret)
          {
            if (this.OnConfirm == null)
              return;
            this.OnConfirm(targetLocation);
          }
          else
          {
            Hashtable hashtable = orgData as Hashtable;
            if (hashtable == null || !hashtable.ContainsKey((object) "errno"))
              return;
            int result = 0;
            int.TryParse(hashtable[(object) "errno"].ToString(), out result);
            if (result != 1400040)
              return;
            Utils.RefreshBlock(targetLocation, (System.Action<bool, object>) null);
          }
        }), true);
      })
    });
  }

  public void OnCancelButtonPressed()
  {
    if (this.OnCancel == null)
      return;
    this.OnCancel();
  }

  public static bool Box2DOverlapPoint(BoxCollider2D collider, Vector2 point)
  {
    point = (Vector2) collider.transform.InverseTransformPoint((Vector3) point);
    return new Rect()
    {
      min = (collider.offset - 0.5f * collider.size),
      size = collider.size
    }.Contains(point);
  }

  public bool IsCollide(WorldCoordinate location)
  {
    Vector3 worldPosition = this.m_MapData.ConvertWorldCoordinateToWorldPosition(location);
    return TeleportHighlight.Box2DOverlapPoint(this.Response, new Vector2(worldPosition.x, worldPosition.y));
  }

  public void StartDragging(DragGesture gesture)
  {
    if (this.m_TeleportMode == TeleportMode.ALLIANCE_WAR_BACK_TELEPORT || this.m_TeleportMode == TeleportMode.ALLIANCE_WAR_ENTER_TELEPORT || this.m_TeleportMode == TeleportMode.ALLIANCE_WAR_TELEPORT)
      return;
    if (this.m_TeleportMode == TeleportMode.ALLIANCE_TELEPORT)
      this.ShowAllianceTeleportToast();
    else if (this.m_TeleportMode == TeleportMode.FORTRESS_TELEPORT)
    {
      this.ShowAllianceFortressTeleportToast();
    }
    else
    {
      this.m_LastScreenPosition = gesture.Position;
      this.m_LastCursorPosition = this.m_TileCamera.GetWorldHitPoint((Vector3) gesture.Position);
      this.m_Dragging = this.IsCollide(this.m_MapData.ConvertWorldPositionToWorldCoordinate(this.m_LastCursorPosition));
    }
  }

  private void ShowAllianceTeleportToast()
  {
    if (this.m_AllianceTeleportToastCount <= 0 || this.m_AllianceTeleportToastShowing)
      return;
    this.m_AllianceTeleportToastShowing = true;
    UIManager.inst.toast.Show(ScriptLocalization.Get("alliance_teleport_tip", true), new System.Action(this.OnAllianceTeleportToastFinished), 2f, true);
  }

  private void ShowAllianceFortressTeleportToast()
  {
    if (this.m_AllianceTeleportToastCount <= 0 || this.m_AllianceTeleportToastShowing)
      return;
    this.m_AllianceTeleportToastShowing = true;
    UIManager.inst.toast.Show(ScriptLocalization.Get("alliance_fortress_teleport_tip", true), new System.Action(this.OnAllianceTeleportToastFinished), 2f, true);
  }

  private void OnAllianceTeleportToastFinished()
  {
    this.m_AllianceTeleportToastShowing = false;
    --this.m_AllianceTeleportToastCount;
  }

  public void UpdateDragging(DragGesture gesture)
  {
    if (!this.m_Dragging)
      return;
    Vector3 worldHitPoint = this.m_TileCamera.GetWorldHitPoint((Vector3) gesture.Position);
    if ((double) Vector3.Distance((Vector3) this.m_LastScreenPosition, (Vector3) gesture.Position) <= 1.0 / 1000.0)
      return;
    this.m_LastScreenPosition = gesture.Position;
    Vector3 vector3 = worldHitPoint - this.m_LastCursorPosition;
    this.m_LastCursorPosition = worldHitPoint;
    this.CityRoot.transform.position += (!this.IsWalkCross ? 1f : this.WALK_CROSS_FACTOR) * vector3;
    this.ClampPosition();
    this.UpdateGrids();
    this.UpdateWorldCoords();
    this.AdjustCamera();
  }

  private void AdjustCamera()
  {
    Rect viewport = this.GetViewport();
    Rect range = this.CalculateRange();
    Vector3 zero = Vector3.zero;
    float x1 = viewport.min.x;
    float x2 = viewport.max.x;
    float y1 = viewport.min.y;
    float y2 = viewport.max.y;
    if ((double) range.min.x < (double) x1)
      zero.x = range.min.x - x1;
    if ((double) range.max.x > (double) x2)
      zero.x = range.max.x - x2;
    if ((double) range.min.y < (double) y1)
      zero.y = range.min.y - y1;
    if ((double) range.max.y > (double) y2)
      zero.y = range.max.y - y2;
    this.m_TileCamera.Translate(zero);
  }

  private void AdjustCameraToTargetCenter()
  {
    Vector3 offset = this.Response.transform.position - this.m_TileCamera.transform.position;
    offset.z = 0.0f;
    this.m_TileCamera.Translate(offset);
  }

  private bool IsWalkCross
  {
    get
    {
      Rect viewport = this.GetViewport();
      Rect range = this.CalculateRange();
      float x1 = viewport.min.x;
      float x2 = viewport.max.x;
      float y1 = viewport.min.y;
      float y2 = viewport.max.y;
      if ((double) range.min.x >= (double) x1 + 1.0 / 1000.0 && (double) range.max.x <= (double) x2 - 1.0 / 1000.0 && (double) range.min.y >= (double) y1 + 1.0 / 1000.0)
        return (double) range.max.y > (double) y2 - 1.0 / 1000.0;
      return true;
    }
  }

  private Rect GetViewport()
  {
    Rect rect = new Rect();
    if ((UnityEngine.Object) this.m_TileCamera != (UnityEngine.Object) null)
    {
      Rect viewport = this.m_TileCamera.GetViewport();
      float x1 = viewport.min.x + this.VIEWPORT_LEFT;
      float x2 = viewport.max.x - this.VIEWPORT_RIGHT;
      float y1 = viewport.min.y + this.VIEWPORT_BOTTOM;
      float y2 = viewport.max.y - this.VIEWPORT_TOP;
      rect.min = new Vector2(x1, y1);
      rect.max = new Vector2(x2, y2);
    }
    return rect;
  }

  public void StopDragging()
  {
    if (this.m_Dragging)
    {
      this.CityRoot.transform.localPosition = Vector3.zero;
      this.SetLocation(this.m_MapData.ConvertWorldPositionToWorldCoordinate(this.transform.position), false);
    }
    this.m_Dragging = false;
  }

  private void Update()
  {
    this.UpdateGrids();
    this.UpdateWorldCoords();
  }

  private bool CanTeleport(Coordinate location, bool ignoreWonderArea = false)
  {
    TileData referenceAt = this.m_MapData.GetReferenceAt(location);
    if (referenceAt == null || location.X == 0 || location.Y == 0)
      return false;
    if (referenceAt.TileType != TileType.Terrain && (ignoreWonderArea || referenceAt.TileType != TileType.WonderTree))
      return referenceAt.TileType == TileType.None;
    return true;
  }

  private void UpdateGrids()
  {
    Vector3 position1 = this.CityRoot.transform.position;
    Vector3 position2 = this.Response.transform.position;
    BoxCollider2D collider = (BoxCollider2D) null;
    bool flag = false;
    for (int index = 0; index < this.Snap.Length; ++index)
    {
      collider = this.Snap[index];
      if (TeleportHighlight.Box2DOverlapPoint(collider, new Vector2(position2.x, position2.y)))
      {
        flag = true;
        break;
      }
    }
    if (flag)
    {
      this.transform.position = this.transform.position + (collider.transform.position - this.CityOrigin.transform.position);
      this.CityRoot.transform.position = position1;
    }
    else
    {
      this.transform.position = position1;
      this.CityRoot.transform.position = position1;
    }
  }

  private void UpdateWorldCoords()
  {
    this.m_T = this.m_MapData.ConvertPixelPositionToWorldCoordinate((Vector2) this.transform.localPosition);
    this.m_L = new WorldCoordinate(this.m_T.X - 1, this.m_T.Y + 1);
    this.m_R = new WorldCoordinate(this.m_T.X + 1, this.m_T.Y + 1);
    this.m_B = new WorldCoordinate(this.m_T.X, this.m_T.Y + 2);
    Coordinate kxy1 = this.m_MapData.ConvertWorldCoordinateToKXY(this.m_T);
    Coordinate kxy2 = this.m_MapData.ConvertWorldCoordinateToKXY(this.m_L);
    Coordinate kxy3 = this.m_MapData.ConvertWorldCoordinateToKXY(this.m_R);
    Coordinate kxy4 = this.m_MapData.ConvertWorldCoordinateToKXY(this.m_B);
    bool ignoreWonderArea = this.m_TeleportMode == TeleportMode.PLACE_WORLD_BOSS || this.m_TeleportMode == TeleportMode.PLACE_FORTRESS || (this.m_TeleportMode == TeleportMode.PLACE_WAREHOUSE || this.m_TeleportMode == TeleportMode.PLACE_FARM) || (this.m_TeleportMode == TeleportMode.PLACE_SAWMILL || this.m_TeleportMode == TeleportMode.PLACE_MINE || (this.m_TeleportMode == TeleportMode.PLACE_HOUSE || this.m_TeleportMode == TeleportMode.PLACE_PORTAL)) || this.m_TeleportMode == TeleportMode.PLACE_HOSPITAL || this.m_TeleportMode == TeleportMode.PLACE_DRAGON_ALTAR;
    if (this.m_TeleportMode == TeleportMode.PLACE_WAREHOUSE || this.m_TeleportMode == TeleportMode.PLACE_FARM || (this.m_TeleportMode == TeleportMode.PLACE_SAWMILL || this.m_TeleportMode == TeleportMode.PLACE_MINE) || (this.m_TeleportMode == TeleportMode.PLACE_HOUSE || this.m_TeleportMode == TeleportMode.PLACE_PORTAL || (this.m_TeleportMode == TeleportMode.PLACE_HOSPITAL || this.m_TeleportMode == TeleportMode.PLACE_DRAGON_ALTAR)))
    {
      Coordinate kxy5 = this.m_MapData.ConvertWorldCoordinateToKXY(this.m_T);
      Coordinate kxy6 = this.m_MapData.ConvertWorldCoordinateToKXY(this.m_L);
      Coordinate kxy7 = this.m_MapData.ConvertWorldCoordinateToKXY(this.m_R);
      Coordinate kxy8 = this.m_MapData.ConvertWorldCoordinateToKXY(this.m_B);
      bool flag1 = this.Contains(kxy5);
      bool flag2 = this.Contains(kxy6);
      bool flag3 = this.Contains(kxy7);
      bool flag4 = this.Contains(kxy8);
      this.Row1[0].material.SetColor("_Color", !this.CanTeleport(kxy1, ignoreWonderArea) || !flag1 ? Color.red : Color.green);
      this.Row1[1].material.SetColor("_Color", !this.CanTeleport(kxy3, ignoreWonderArea) || !flag3 ? Color.red : Color.green);
      this.Row2[0].material.SetColor("_Color", !this.CanTeleport(kxy2, ignoreWonderArea) || !flag2 ? Color.red : Color.green);
      this.Row2[1].material.SetColor("_Color", !this.CanTeleport(kxy4, ignoreWonderArea) || !flag4 ? Color.red : Color.green);
      this.UseAllianceTeleport.gameObject.SetActive(false);
      this.UseAllianceFortressTeleport.gameObject.SetActive(false);
      this.UseTeleport.gameObject.SetActive(false);
      this.UseArtifactTeleport.gameObject.SetActive(false);
      this.BuyAndUseTeleport.gameObject.SetActive(false);
      this.PlaceFortress.gameObject.SetActive(true);
      this.KingdomTeleport.gameObject.SetActive(false);
      this.AllianceWarBackTeleportButton.gameObject.SetActive(false);
      this.AllianceWarTeleportButton.gameObject.SetActive(false);
      this.PlaceFortress.isEnabled = this.CanTeleport(kxy1, ignoreWonderArea) && this.CanTeleport(kxy2, ignoreWonderArea) && (this.CanTeleport(kxy3, ignoreWonderArea) && this.CanTeleport(kxy4, ignoreWonderArea)) && (flag1 && flag2 && flag3) && flag4;
    }
    else
    {
      this.Row1[0].material.SetColor("_Color", !this.CanTeleport(kxy1, ignoreWonderArea) ? Color.red : Color.green);
      this.Row1[1].material.SetColor("_Color", !this.CanTeleport(kxy3, ignoreWonderArea) ? Color.red : Color.green);
      this.Row2[0].material.SetColor("_Color", !this.CanTeleport(kxy2, ignoreWonderArea) ? Color.red : Color.green);
      this.Row2[1].material.SetColor("_Color", !this.CanTeleport(kxy4, ignoreWonderArea) ? Color.red : Color.green);
    }
    if (this.m_TeleportMode == TeleportMode.ADVANCE_TELEPORT)
    {
      this.UseAllianceTeleport.gameObject.SetActive(false);
      this.UseAllianceFortressTeleport.gameObject.SetActive(false);
      this.UseTeleport.gameObject.SetActive(TeleportHighlight.HasAdvanceTeleport() && !TeleportHighlight.HasArtifactTeleport());
      this.UseArtifactTeleport.gameObject.SetActive(TeleportHighlight.HasArtifactTeleport());
      this.BuyAndUseTeleport.gameObject.SetActive(!TeleportHighlight.HasAdvanceTeleport() && !TeleportHighlight.HasArtifactTeleport());
      this.PlaceFortress.gameObject.SetActive(false);
      this.KingdomTeleport.gameObject.SetActive(false);
      this.AllianceWarBackTeleportButton.gameObject.SetActive(false);
      this.AllianceWarTeleportButton.gameObject.SetActive(false);
      bool flag = this.CanTeleport(kxy1, false) && this.CanTeleport(kxy2, false) && this.CanTeleport(kxy3, false) && this.CanTeleport(kxy4, false);
      this.UseTeleport.isEnabled = flag;
      this.UseArtifactTeleport.isEnabled = flag;
      this.BuyAndUseTeleport.isEnabled = flag;
      this.Price.color = !TeleportHighlight.CanBuyAdvancedTeleport() ? Color.red : Color.white;
      this.Price.text = ItemBag.Instance.GetShopItemPrice("shopitem_targettele").ToString();
    }
    else if (this.m_TeleportMode == TeleportMode.WORLD_TELEPORT)
    {
      this.UseAllianceTeleport.gameObject.SetActive(false);
      this.UseAllianceFortressTeleport.gameObject.SetActive(false);
      this.UseTeleport.gameObject.SetActive(false);
      this.UseArtifactTeleport.gameObject.SetActive(false);
      this.BuyAndUseTeleport.gameObject.SetActive(false);
      this.PlaceFortress.gameObject.SetActive(false);
      this.KingdomTeleport.gameObject.SetActive(true);
      this.CancelButton.gameObject.SetActive(false);
      this.AllianceWarBackTeleportButton.gameObject.SetActive(false);
      this.AllianceWarTeleportButton.gameObject.SetActive(false);
      this.KingdomTeleport.isEnabled = this.CanTeleport(kxy1, false) && this.CanTeleport(kxy2, false) && this.CanTeleport(kxy3, false) && this.CanTeleport(kxy4, false);
    }
    else if (this.m_TeleportMode == TeleportMode.KINGDOM_TELEPORT_BACK)
    {
      this.UseAllianceTeleport.gameObject.SetActive(false);
      this.UseAllianceFortressTeleport.gameObject.SetActive(false);
      this.UseTeleport.gameObject.SetActive(TeleportHighlight.HasKingdomTeleportBack());
      this.UseArtifactTeleport.gameObject.SetActive(false);
      this.BuyAndUseTeleport.gameObject.SetActive(!TeleportHighlight.HasKingdomTeleportBack());
      this.PlaceFortress.gameObject.SetActive(false);
      this.KingdomTeleport.gameObject.SetActive(false);
      this.AllianceWarBackTeleportButton.gameObject.SetActive(false);
      this.AllianceWarTeleportButton.gameObject.SetActive(false);
      bool flag = this.CanTeleport(kxy1, false) && this.CanTeleport(kxy2, false) && this.CanTeleport(kxy3, false) && this.CanTeleport(kxy4, false);
      this.UseTeleport.isEnabled = flag;
      this.UseArtifactTeleport.isEnabled = flag;
      this.BuyAndUseTeleport.isEnabled = flag;
      this.Price.color = !TeleportHighlight.CanBuyKingdomTeleportBack() ? Color.red : Color.white;
      this.Price.text = ItemBag.Instance.GetShopItemPrice("shopitem_excalibur_war_tele_back").ToString();
    }
    else if (this.m_TeleportMode == TeleportMode.ALLIANCE_WAR_ENTER_TELEPORT)
    {
      this.UseAllianceTeleport.gameObject.SetActive(false);
      this.UseAllianceFortressTeleport.gameObject.SetActive(false);
      this.UseTeleport.gameObject.SetActive(false);
      this.UseArtifactTeleport.gameObject.SetActive(false);
      this.BuyAndUseTeleport.gameObject.SetActive(false);
      this.PlaceFortress.gameObject.SetActive(false);
      this.KingdomTeleport.gameObject.SetActive(false);
      this.CancelButton.gameObject.SetActive(false);
      this.AllianceWarBackTeleportButton.gameObject.SetActive(true);
      this.AllianceWarTeleportButton.gameObject.SetActive(true);
      this.AllianceWarBackTeleportButton.isEnabled = true;
      this.AllianceWarTeleportButton.isEnabled = true;
      this.LabelAllianceWarLeft.text = Utils.XLAT("id_uppercase_cancel");
      this.LabelAllianceWarRight.text = Utils.XLAT("id_uppercase_teleport");
    }
    else if (this.m_TeleportMode == TeleportMode.ALLIANCE_WAR_TELEPORT)
    {
      this.UseAllianceTeleport.gameObject.SetActive(false);
      this.UseAllianceFortressTeleport.gameObject.SetActive(false);
      this.UseTeleport.gameObject.SetActive(false);
      this.UseArtifactTeleport.gameObject.SetActive(false);
      this.BuyAndUseTeleport.gameObject.SetActive(false);
      this.PlaceFortress.gameObject.SetActive(false);
      this.KingdomTeleport.gameObject.SetActive(false);
      this.CancelButton.gameObject.SetActive(false);
      this.AllianceWarBackTeleportButton.gameObject.SetActive(true);
      this.AllianceWarTeleportButton.gameObject.SetActive(true);
      this.CityOrigin.SetActive(false);
      CitadelSystem.inst.ForceHide();
      this.AllianceWarBackTeleportButton.isEnabled = true;
      this.AllianceWarTeleportButton.isEnabled = true;
      this.LabelAllianceWarLeft.text = Utils.XLAT("id_return_home");
      this.LabelAllianceWarRight.text = Utils.XLAT("alliance_fort_place_fort_button");
    }
    else if (this.m_TeleportMode == TeleportMode.ALLIANCE_WAR_BACK_TELEPORT)
    {
      this.UseAllianceTeleport.gameObject.SetActive(false);
      this.UseAllianceFortressTeleport.gameObject.SetActive(false);
      this.UseTeleport.gameObject.SetActive(false);
      this.UseArtifactTeleport.gameObject.SetActive(false);
      this.BuyAndUseTeleport.gameObject.SetActive(false);
      this.PlaceFortress.gameObject.SetActive(false);
      this.KingdomTeleport.gameObject.SetActive(true);
      this.CancelButton.gameObject.SetActive(false);
      this.AllianceWarBackTeleportButton.gameObject.SetActive(false);
      this.AllianceWarTeleportButton.gameObject.SetActive(false);
      this.KingdomTeleport.isEnabled = true;
    }
    else if (this.m_TeleportMode == TeleportMode.ALLIANCE_TELEPORT)
    {
      this.UseAllianceTeleport.gameObject.SetActive(true);
      this.UseAllianceFortressTeleport.gameObject.SetActive(false);
      this.UseTeleport.gameObject.SetActive(false);
      this.UseArtifactTeleport.gameObject.SetActive(false);
      this.BuyAndUseTeleport.gameObject.SetActive(false);
      this.PlaceFortress.gameObject.SetActive(false);
      this.KingdomTeleport.gameObject.SetActive(false);
      this.AllianceWarBackTeleportButton.gameObject.SetActive(false);
      this.AllianceWarTeleportButton.gameObject.SetActive(false);
      this.UseAllianceTeleport.isEnabled = this.CanTeleport(kxy1, false) && this.CanTeleport(kxy2, false) && this.CanTeleport(kxy3, false) && this.CanTeleport(kxy4, false) && TeleportHighlight.HasAllianceTeleport();
    }
    else if (this.m_TeleportMode == TeleportMode.FORTRESS_TELEPORT)
    {
      this.UseAllianceTeleport.gameObject.SetActive(false);
      this.UseAllianceFortressTeleport.gameObject.SetActive(true);
      this.UseTeleport.gameObject.SetActive(false);
      this.UseArtifactTeleport.gameObject.SetActive(false);
      this.BuyAndUseTeleport.gameObject.SetActive(false);
      this.PlaceFortress.gameObject.SetActive(false);
      this.KingdomTeleport.gameObject.SetActive(false);
      this.AllianceWarBackTeleportButton.gameObject.SetActive(false);
      this.AllianceWarTeleportButton.gameObject.SetActive(false);
      this.UseAllianceTeleport.isEnabled = this.CanTeleport(kxy1, false) && this.CanTeleport(kxy2, false) && this.CanTeleport(kxy3, false) && this.CanTeleport(kxy4, false) && TeleportHighlight.HasFortressTeleport();
    }
    else if (this.m_TeleportMode == TeleportMode.PLACE_FORTRESS)
    {
      this.UseAllianceTeleport.gameObject.SetActive(false);
      this.UseAllianceFortressTeleport.gameObject.SetActive(false);
      this.UseTeleport.gameObject.SetActive(false);
      this.UseArtifactTeleport.gameObject.SetActive(false);
      this.BuyAndUseTeleport.gameObject.SetActive(false);
      this.PlaceFortress.gameObject.SetActive(true);
      this.KingdomTeleport.gameObject.SetActive(false);
      this.AllianceWarBackTeleportButton.gameObject.SetActive(false);
      this.AllianceWarTeleportButton.gameObject.SetActive(false);
      this.PlaceFortress.isEnabled = this.CanTeleport(kxy1, ignoreWonderArea) && this.CanTeleport(kxy2, ignoreWonderArea) && this.CanTeleport(kxy3, ignoreWonderArea) && this.CanTeleport(kxy4, ignoreWonderArea);
    }
    else if (this.m_TeleportMode == TeleportMode.PLACE_WORLD_BOSS)
    {
      this.UseAllianceTeleport.gameObject.SetActive(false);
      this.UseAllianceFortressTeleport.gameObject.SetActive(false);
      this.UseTeleport.gameObject.SetActive(false);
      this.UseArtifactTeleport.gameObject.SetActive(false);
      this.BuyAndUseTeleport.gameObject.SetActive(false);
      this.PlaceFortress.gameObject.SetActive(true);
      this.KingdomTeleport.gameObject.SetActive(false);
      this.AllianceWarBackTeleportButton.gameObject.SetActive(false);
      this.AllianceWarTeleportButton.gameObject.SetActive(false);
      this.PlaceFortress.isEnabled = this.CanTeleport(kxy1, ignoreWonderArea) && this.CanTeleport(kxy2, ignoreWonderArea) && this.CanTeleport(kxy3, ignoreWonderArea) && this.CanTeleport(kxy4, ignoreWonderArea);
    }
    else
    {
      if (this.m_TeleportMode != TeleportMode.ADVANCE_PIT_TELEPORT)
        return;
      this.UseAllianceTeleport.gameObject.SetActive(false);
      this.UseAllianceFortressTeleport.gameObject.SetActive(false);
      this.UseTeleport.gameObject.SetActive(false);
      this.UseArtifactTeleport.gameObject.SetActive(false);
      this.BuyAndUseTeleport.gameObject.SetActive(true);
      this.PlaceFortress.gameObject.SetActive(false);
      this.KingdomTeleport.gameObject.SetActive(false);
      this.AllianceWarBackTeleportButton.gameObject.SetActive(false);
      this.AllianceWarTeleportButton.gameObject.SetActive(false);
      bool flag = this.CanTeleport(kxy1, false) && this.CanTeleport(kxy2, false) && this.CanTeleport(kxy3, false) && this.CanTeleport(kxy4, false);
      this.UseTeleport.isEnabled = flag;
      this.UseArtifactTeleport.isEnabled = flag;
      this.BuyAndUseTeleport.isEnabled = flag;
      this.Price.color = !TeleportHighlight.CanBuyAdvancePitTeleport() ? Color.red : Color.white;
      this.Price.text = PitExplorePayload.Instance.PitTeleportCost.ToString();
    }
  }

  private bool Contains(Coordinate p2)
  {
    List<ZoneBorderData> bordersByAllianceId = DBManager.inst.DB_ZoneBorder.GetZoneBordersByAllianceID(PlayerData.inst.allianceId);
    for (int index = 0; index < bordersByAllianceId.Count; ++index)
    {
      if (bordersByAllianceId[index].Contains(p2))
        return true;
    }
    return false;
  }

  private static bool HasAdvanceTeleport()
  {
    return ItemBag.Instance.GetItemCountByShopID("shopitem_targettele") > 0;
  }

  private static bool HasArtifactTeleport()
  {
    int specificArtifact = PlayerData.inst.playerCityData.GetSpecificArtifact("space_sword");
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(PlayerData.inst.uid);
    if (heroData != null && heroData.IsArtifactEquiped(specificArtifact))
      return PlayerData.inst.playerCityData.HasAvailableArtifact(specificArtifact);
    return false;
  }

  public static bool HasAllianceTeleport()
  {
    return ItemBag.Instance.GetItemCount("item_alliancetele") > 0;
  }

  private static bool HasKingdomTeleportBack()
  {
    return ItemBag.Instance.GetItemCountByShopID("shopitem_excalibur_war_tele_back") > 0;
  }

  public static bool HasFortressTeleport()
  {
    return ItemBag.Instance.GetItemCount("item_fortress_teleport") > 0;
  }

  private static bool CanBuyAdvancedTeleport()
  {
    return ItemBag.Instance.CheckCanBuyShopItem("shopitem_targettele", false);
  }

  private static bool CanBuyKingdomTeleportBack()
  {
    return ItemBag.Instance.CheckCanBuyShopItem("shopitem_excalibur_war_tele_back", false);
  }

  private static bool CanBuyFortressTeleport()
  {
    return ItemBag.Instance.CheckCanBuyShopItem("item_fortress_teleport", false);
  }

  public static bool CanBuyAdvancePitTeleport()
  {
    return PlayerData.inst.hostPlayer.Currency >= PitExplorePayload.Instance.PitTeleportCost;
  }

  private Rect CalculateRange()
  {
    return new Rect()
    {
      min = (Vector2) this.Response.bounds.min,
      max = (Vector2) this.Response.bounds.max
    };
  }

  private Rect GetConstraint()
  {
    float num = 0.01f;
    if ((bool) ((UnityEngine.Object) GameEngine.Instance))
      num = GameEngine.Instance.tileMapScale;
    Coordinate initialLocation = this.m_InitialLocation;
    initialLocation.X = 1;
    initialLocation.Y = 1;
    Vector3 worldPosition1 = this.m_MapData.ConvertTileKXYToWorldPosition(initialLocation);
    initialLocation.X = 1279;
    initialLocation.Y = 2559;
    Vector3 worldPosition2 = this.m_MapData.ConvertTileKXYToWorldPosition(initialLocation);
    return new Rect()
    {
      xMin = worldPosition1.x,
      xMax = worldPosition2.x,
      yMin = worldPosition2.y,
      yMax = worldPosition1.y
    };
  }

  private void ClampPosition()
  {
    Rect constraint = this.GetConstraint();
    Vector3 position = this.CityRoot.transform.position;
    position.x = Mathf.Clamp(position.x, constraint.min.x, constraint.max.x);
    position.y = Mathf.Clamp(position.y, constraint.min.y, constraint.max.y);
    this.CityRoot.transform.position = position;
  }

  private void TakeAction(Coordinate targetLocation)
  {
    if (this.m_TeleportMode == TeleportMode.ADVANCE_TELEPORT)
    {
      if (TeleportHighlight.HasArtifactTeleport())
        this.ArtifactTeleport(targetLocation);
      else if (TeleportHighlight.HasAdvanceTeleport())
        this.AdvanceTeleport(targetLocation);
      else
        this.BuyAndAdvanceTeleport(targetLocation);
    }
    else if (this.m_TeleportMode == TeleportMode.KINGDOM_TELEPORT_BACK)
    {
      if (TeleportHighlight.HasKingdomTeleportBack())
        this.KingdomTeleportBack(targetLocation);
      else
        this.BuyAndKingdomTeleportBack(targetLocation);
    }
    else if (this.m_TeleportMode == TeleportMode.ALLIANCE_TELEPORT)
      this.AllianceTeleport(targetLocation);
    else if (this.m_TeleportMode == TeleportMode.FORTRESS_TELEPORT)
      this.FortressTeleport(targetLocation);
    else if (this.m_TeleportMode == TeleportMode.PLACE_FORTRESS)
      this.PlaceFortressAt(targetLocation);
    else if (this.m_TeleportMode == TeleportMode.PLACE_WAREHOUSE)
      this.PlaceWareHouseAt(targetLocation);
    else if (this.m_TeleportMode == TeleportMode.PLACE_HOSPITAL)
      this.PlaceHospitalAt(targetLocation);
    else if (this.m_TeleportMode == TeleportMode.PLACE_FARM || this.m_TeleportMode == TeleportMode.PLACE_SAWMILL || (this.m_TeleportMode == TeleportMode.PLACE_MINE || this.m_TeleportMode == TeleportMode.PLACE_HOUSE) || (this.m_TeleportMode == TeleportMode.PLACE_PORTAL || this.m_TeleportMode == TeleportMode.PLACE_HOSPITAL))
      this.PlaceAllianceBuildingAt(targetLocation);
    else if (this.m_TeleportMode == TeleportMode.PLACE_DRAGON_ALTAR)
      this.PlaceAllianceDragonAltar(targetLocation);
    else if (this.m_TeleportMode == TeleportMode.PLACE_WORLD_BOSS)
      this.PlaceWorldBoss(targetLocation);
    else if (this.m_TeleportMode == TeleportMode.ADVANCE_PIT_TELEPORT)
      this.AdvancePitTeleport(targetLocation);
    else if (this.m_TeleportMode == TeleportMode.ALLIANCE_WAR_TELEPORT)
      this.AllianceWarTeleportAction();
    else if (this.m_TeleportMode == TeleportMode.ALLIANCE_WAR_ENTER_TELEPORT)
    {
      this.AllianceWarEnterTeleport(targetLocation);
    }
    else
    {
      if (this.m_TeleportMode != TeleportMode.ALLIANCE_WAR_BACK_TELEPORT)
        return;
      this.AllianceWarBackTeleportAction();
    }
  }

  private void AllianceWarEnterTeleport(Coordinate targetLocation)
  {
    this.CanDestroy = true;
    AllianceWarPayload.Instance.Enter(PlayerData.inst.uid, AllianceWarPayload.Instance.CurOppAllianceId, targetLocation.X, targetLocation.Y, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Lite);
    }));
  }

  private void AllianceWarBackTeleport()
  {
    PVPSystem.Instance.Controller.AllianceWarBackTeleport();
  }

  private void AllianceWarTeleportAction()
  {
    bool flag = this.CanTeleport(this.m_MapData.ConvertWorldCoordinateToKXY(this.m_T), false) && this.CanTeleport(this.m_MapData.ConvertWorldCoordinateToKXY(this.m_L), false) && this.CanTeleport(this.m_MapData.ConvertWorldCoordinateToKXY(this.m_R), false) && this.CanTeleport(this.m_MapData.ConvertWorldCoordinateToKXY(this.m_B), false);
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = Utils.XLAT("id_uppercase_teleport"),
      Content = Utils.XLAT(!flag ? "toast_alliance_warfare_location_unavailable" : "teleport_adv_confirm"),
      Okay = Utils.XLAT("id_uppercase_confirm"),
      OkayCallback = (System.Action) (() => this.AllianceWarTeleport())
    });
  }

  private void AllianceWarBackTeleportAction()
  {
    this.AllianceWarTeleport();
  }

  private void AllianceWarTeleport()
  {
    this.CanDestroy = true;
    PVPSystem.Instance.Map.ResetTeleport();
    TileData tileData = PVPMapData.MapData.GetNearEmptyTileForCity(PlayerData.inst.playerCityData.cityLocation, false);
    if (tileData == null)
      return;
    TeleportManager.Instance.AllianceWarTeleport((System.Action<bool, object>) ((ret, data) =>
    {
      this.OnTeleportCallback(ret, data);
      if (this.OnConfirm == null)
        return;
      this.OnConfirm(tileData.Location);
    }));
  }

  private void AdvancePitTeleport(Coordinate targetLocation)
  {
    MessageHub.inst.GetPortByAction("Abyss:teleport").SendRequest(Utils.Hash((object) "x", (object) targetLocation.X, (object) "y", (object) targetLocation.Y), new System.Action<bool, object>(this.OnPitTeleportCallback), true);
  }

  private void AllianceTeleport(Coordinate targetLocation)
  {
    Hashtable extra = new Hashtable();
    extra[(object) "kingdom_id"] = (object) targetLocation.K;
    extra[(object) "map_x"] = (object) targetLocation.X;
    extra[(object) "map_y"] = (object) targetLocation.Y;
    ItemBag.Instance.UseItem("item_alliancetele", extra, new System.Action<bool, object>(this.OnTeleportCallback), 1);
  }

  private void FortressTeleport(Coordinate targetLocation)
  {
    Hashtable extra = new Hashtable();
    extra[(object) "kingdom_id"] = (object) targetLocation.K;
    extra[(object) "map_x"] = (object) targetLocation.X;
    extra[(object) "map_y"] = (object) targetLocation.Y;
    ItemBag.Instance.UseItem("item_fortress_teleport", extra, new System.Action<bool, object>(this.OnTeleportCallback), 1);
  }

  private void AdvanceTeleport(Coordinate targetLocation)
  {
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = ScriptLocalization.Get("id_uppercase_teleport", true),
      Content = ScriptLocalization.Get("teleport_adv_confirm", true),
      Okay = ScriptLocalization.Get("id_uppercase_confirm", true),
      OkayCallback = (System.Action) (() =>
      {
        Hashtable extra = new Hashtable();
        extra[(object) "kingdom_id"] = (object) targetLocation.K;
        extra[(object) "map_x"] = (object) targetLocation.X;
        extra[(object) "map_y"] = (object) targetLocation.Y;
        ItemBag.Instance.UseItemByShopItemID("shopitem_targettele", extra, new System.Action<bool, object>(this.OnTeleportCallback), 1);
      })
    });
  }

  private void ArtifactTeleport(Coordinate targetLocation)
  {
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = ScriptLocalization.Get("id_uppercase_teleport", true),
      Content = ScriptLocalization.Get("teleport_adv_confirm", true),
      Okay = ScriptLocalization.Get("id_uppercase_confirm", true),
      OkayCallback = (System.Action) (() =>
      {
        Hashtable postData = new Hashtable();
        postData[(object) "world_id"] = (object) targetLocation.K;
        postData[(object) "map_x"] = (object) targetLocation.X;
        postData[(object) "map_y"] = (object) targetLocation.Y;
        postData[(object) "uid"] = (object) PlayerData.inst.uid;
        postData[(object) "artifact_id"] = (object) PlayerData.inst.playerCityData.GetSpecificArtifact("space_sword");
        RequestManager.inst.SendRequest("Artifact:castSkill", postData, (System.Action<bool, object>) ((ret, data) => this.OnTeleportCallback(ret, data)), true);
      })
    });
  }

  private void KingdomTeleportBack(Coordinate targetLocation)
  {
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = ScriptLocalization.Get("id_uppercase_teleport", true),
      Content = ScriptLocalization.Get("teleport_adv_confirm", true),
      Okay = ScriptLocalization.Get("id_uppercase_confirm", true),
      OkayCallback = (System.Action) (() =>
      {
        Hashtable extra = new Hashtable();
        extra[(object) "kingdom_id"] = (object) targetLocation.K;
        extra[(object) "map_x"] = (object) targetLocation.X;
        extra[(object) "map_y"] = (object) targetLocation.Y;
        ItemBag.Instance.UseItemByShopItemID("shopitem_excalibur_war_tele_back", extra, new System.Action<bool, object>(this.OnKingdomTeleportCallback), 1);
      })
    });
  }

  private void PlaceFortressAt(Coordinate targetLocation)
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(this.m_AllianceBuildingID);
    if (buildingStaticInfo == null)
      return;
    string str = string.Format(ScriptLocalization.Get("alliance_buildings_confirm_placement_description", true), (object) ScriptLocalization.Get(buildingStaticInfo.BuildName, true));
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = ScriptLocalization.Get("id_uppercase_confirm", true),
      Content = str,
      Okay = ScriptLocalization.Get("id_uppercase_confirm", true),
      OkayCallback = (System.Action) (() =>
      {
        Hashtable postData = new Hashtable();
        postData[(object) "k"] = (object) targetLocation.K;
        postData[(object) "x"] = (object) targetLocation.X;
        postData[(object) "y"] = (object) targetLocation.Y;
        postData[(object) "config_id"] = (object) this.m_AllianceBuildingID;
        MessageHub.inst.GetPortByAction("Map:placeAllianceFortress").SendRequest(postData, new System.Action<bool, object>(this.OnTeleportCallback), true);
      })
    });
  }

  private void PlaceAllianceBuildingAt(Coordinate targetLocation)
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(this.m_AllianceBuildingID);
    if (buildingStaticInfo == null)
      return;
    string str = string.Format(ScriptLocalization.Get("alliance_buildings_confirm_placement_description", true), (object) ScriptLocalization.Get(buildingStaticInfo.BuildName, true));
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = ScriptLocalization.Get("id_uppercase_confirm", true),
      Content = str,
      Okay = ScriptLocalization.Get("id_uppercase_confirm", true),
      OkayCallback = (System.Action) (() =>
      {
        Hashtable postData = new Hashtable();
        postData[(object) "k"] = (object) targetLocation.K;
        postData[(object) "x"] = (object) targetLocation.X;
        postData[(object) "y"] = (object) targetLocation.Y;
        postData[(object) "config_id"] = (object) this.m_AllianceBuildingID;
        MessageHub.inst.GetPortByAction("map:placeAllianceBuilding").SendRequest(postData, new System.Action<bool, object>(this.OnTeleportCallback), true);
      })
    });
  }

  private void PlaceWareHouseAt(Coordinate targetLocation)
  {
    AllianceWareHouseData wareHouseData = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseData();
    if (wareHouseData == null)
    {
      this.PlaceAllianceBuildingAt(targetLocation);
    }
    else
    {
      if (wareHouseData.ClearAway == 0)
        return;
      AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(this.m_AllianceBuildingID);
      if (buildingStaticInfo == null)
        return;
      string str = string.Format(ScriptLocalization.Get("alliance_buildings_confirm_placement_description", true), (object) ScriptLocalization.Get(buildingStaticInfo.BuildName, true));
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = ScriptLocalization.Get("id_uppercase_confirm", true),
        Content = str,
        Okay = ScriptLocalization.Get("id_uppercase_confirm", true),
        OkayCallback = (System.Action) (() =>
        {
          Hashtable postData = new Hashtable();
          postData[(object) "k"] = (object) targetLocation.K;
          postData[(object) "x"] = (object) targetLocation.X;
          postData[(object) "y"] = (object) targetLocation.Y;
          postData[(object) "building_id"] = (object) wareHouseData.WarehouseId;
          postData[(object) "config_id"] = (object) this.m_AllianceBuildingID;
          MessageHub.inst.GetPortByAction("map:replaceAllianceBuilding").SendRequest(postData, new System.Action<bool, object>(this.OnTeleportCallback), true);
        })
      });
    }
  }

  private void PlaceHospitalAt(Coordinate targetLocation)
  {
    AllianceHospitalData hospitalData = DBManager.inst.DB_AllianceHospital.GetMyHospitalData();
    if (hospitalData == null)
    {
      this.PlaceAllianceBuildingAt(targetLocation);
    }
    else
    {
      if (hospitalData.ClearAway == 0)
        return;
      AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(this.m_AllianceBuildingID);
      if (buildingStaticInfo == null)
        return;
      string str = string.Format(ScriptLocalization.Get("alliance_buildings_confirm_placement_description", true), (object) ScriptLocalization.Get(buildingStaticInfo.BuildName, true));
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = ScriptLocalization.Get("id_uppercase_confirm", true),
        Content = str,
        Okay = ScriptLocalization.Get("id_uppercase_confirm", true),
        OkayCallback = (System.Action) (() =>
        {
          Hashtable postData = new Hashtable();
          postData[(object) "k"] = (object) targetLocation.K;
          postData[(object) "x"] = (object) targetLocation.X;
          postData[(object) "y"] = (object) targetLocation.Y;
          postData[(object) "building_id"] = (object) hospitalData.BuildingID;
          postData[(object) "config_id"] = (object) this.m_AllianceBuildingID;
          MessageHub.inst.GetPortByAction("map:replaceAllianceBuilding").SendRequest(postData, new System.Action<bool, object>(this.OnTeleportCallback), true);
        })
      });
    }
  }

  private void PlaceAllianceDragonAltar(Coordinate targetLocation)
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(this.m_AllianceBuildingID);
    if (buildingStaticInfo == null)
      return;
    string str = string.Format(ScriptLocalization.Get("alliance_buildings_confirm_placement_description", true), (object) ScriptLocalization.Get(buildingStaticInfo.BuildName, true));
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = ScriptLocalization.Get("id_uppercase_confirm", true),
      Content = str,
      Okay = ScriptLocalization.Get("id_uppercase_confirm", true),
      OkayCallback = (System.Action) (() =>
      {
        Hashtable postData = new Hashtable();
        postData[(object) "k"] = (object) targetLocation.K;
        postData[(object) "x"] = (object) targetLocation.X;
        postData[(object) "y"] = (object) targetLocation.Y;
        postData[(object) "config_id"] = (object) this.m_AllianceBuildingID;
        MessageHub.inst.GetPortByAction("map:placeAllianceTemple").SendRequest(postData, new System.Action<bool, object>(this.OnTeleportCallback), true);
      })
    });
  }

  private void PlaceWorldBoss(Coordinate targetLocation)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.m_AllianceBuildingID);
    if (itemStaticInfo == null)
      return;
    WorldBossStaticInfo data = ConfigManager.inst.DB_WorldBoss.GetData(itemStaticInfo.Param1);
    if (data == null)
      return;
    string withPara = ScriptLocalization.GetWithPara("alliance_fort_confirm_placement_description", new Dictionary<string, string>()
    {
      {
        "0",
        data.LocalName
      }
    }, true);
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = ScriptLocalization.Get("id_uppercase_confirm", true),
      Content = withPara,
      Okay = ScriptLocalization.Get("id_uppercase_confirm", true),
      OkayCallback = (System.Action) (() =>
      {
        Hashtable postData = new Hashtable()
        {
          {
            (object) "k",
            (object) targetLocation.K
          },
          {
            (object) "x",
            (object) targetLocation.X
          },
          {
            (object) "y",
            (object) targetLocation.Y
          },
          {
            (object) "city_id",
            (object) PlayerData.inst.cityId
          },
          {
            (object) "amount",
            (object) 1
          },
          {
            (object) "item_id",
            (object) this.m_AllianceBuildingID
          }
        };
        RequestManager.inst.SendRequest("Map:placeFestivalBoss", postData, (System.Action<bool, object>) null, true);
      })
    });
  }

  private void BuyAndAdvanceTeleport(Coordinate targetLocation)
  {
    if (PlayerData.inst.userData.IsForeigner && AllianceWarPayload.Instance.IsPlayerCurrentJoined(0L))
    {
      UIManager.inst.toast.Show(Utils.XLAT("exception_1400040"), (System.Action) null, 4f, true);
    }
    else
    {
      int shopItemPrice = ItemBag.Instance.GetShopItemPrice("shopitem_targettele");
      GoldConsumePopup.Parameter parameter = new GoldConsumePopup.Parameter();
      parameter.goldNum = shopItemPrice;
      Dictionary<string, string> para = new Dictionary<string, string>();
      para["num"] = shopItemPrice.ToString();
      parameter.description = ScriptLocalization.GetWithPara("confirm_gold_spend_advanced_teleport_desc", para, true);
      parameter.confirmButtonClickEvent = (System.Action) (() =>
      {
        Hashtable extra = new Hashtable();
        extra[(object) "kingdom_id"] = (object) targetLocation.K;
        extra[(object) "map_x"] = (object) targetLocation.X;
        extra[(object) "map_y"] = (object) targetLocation.Y;
        ItemBag.Instance.BuyAndUseShopItem("shopitem_targettele", extra, new System.Action<bool, object>(this.OnTeleportCallback), 1);
      });
      UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) parameter);
    }
  }

  private void BuyAndKingdomTeleportBack(Coordinate targetLocation)
  {
    if (PlayerData.inst.userData.IsForeigner && AllianceWarPayload.Instance.IsPlayerCurrentJoined(0L))
    {
      UIManager.inst.toast.Show(Utils.XLAT("exception_1400040"), (System.Action) null, 4f, true);
    }
    else
    {
      int shopItemPrice = ItemBag.Instance.GetShopItemPrice("shopitem_excalibur_war_tele_back");
      GoldConsumePopup.Parameter parameter = new GoldConsumePopup.Parameter();
      parameter.goldNum = shopItemPrice;
      Dictionary<string, string> para = new Dictionary<string, string>();
      para["num"] = shopItemPrice.ToString();
      parameter.description = ScriptLocalization.GetWithPara("confirm_gold_spend_advanced_teleport_desc", para, true);
      parameter.confirmButtonClickEvent = (System.Action) (() =>
      {
        Hashtable extra = new Hashtable();
        extra[(object) "kingdom_id"] = (object) targetLocation.K;
        extra[(object) "map_x"] = (object) targetLocation.X;
        extra[(object) "map_y"] = (object) targetLocation.Y;
        ItemBag.Instance.BuyAndUseShopItem("shopitem_excalibur_war_tele_back", extra, new System.Action<bool, object>(this.OnKingdomTeleportCallback), 1);
      });
      UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) parameter);
    }
  }

  private void OnKingdomTeleportCallback(bool ret, object orgData)
  {
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable == null)
      return;
    if (ret)
    {
      GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Lite);
    }
    else
    {
      if (!hashtable.ContainsKey((object) "errno"))
        return;
      int result = 0;
      int.TryParse(hashtable[(object) "errno"].ToString(), out result);
      if (result != 1400040)
        return;
      Utils.RefreshBlock(this.m_MapData.ConvertWorldCoordinateToKXY(this.m_T), (System.Action<bool, object>) null);
    }
  }

  private void OnTeleportCallback(bool ret, object orgData)
  {
    this.CanDestroy = true;
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable == null || !hashtable.ContainsKey((object) "errno"))
      return;
    int result = 0;
    int.TryParse(hashtable[(object) "errno"].ToString(), out result);
    if (result != 1400040)
      return;
    Utils.RefreshBlock(this.m_MapData.ConvertWorldCoordinateToKXY(this.m_T), (System.Action<bool, object>) null);
  }

  private void OnPitTeleportCallback(bool ret, object orgData)
  {
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable == null)
      return;
    if (hashtable.ContainsKey((object) "errno"))
    {
      int result = 0;
      int.TryParse(hashtable[(object) "errno"].ToString(), out result);
      if (result == 1400040)
        Utils.RefreshBlock(this.m_MapData.ConvertWorldCoordinateToKXY(this.m_T), (System.Action<bool, object>) null);
    }
    if (!hashtable.ContainsKey((object) "teleport"))
      return;
    int result1 = 0;
    int.TryParse(hashtable[(object) "teleport"].ToString(), out result1);
    PitExplorePayload.Instance.PitTeleportTimes = result1;
  }

  private void GenerateWorldBoss()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.m_AllianceBuildingID);
    if (itemStaticInfo == null)
      return;
    WorldBossStaticInfo data = ConfigManager.inst.DB_WorldBoss.GetData(itemStaticInfo.Param1);
    if (data == null)
      return;
    GameObject gameObject = PrefabManagerEx.Instance.Spawn("WORLD_BOSS", this.m_Parent);
    if (!(bool) ((UnityEngine.Object) gameObject))
      return;
    gameObject.transform.parent = this.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
    gameObject.GetComponentsInChildren<PVEMonsterTile>(true)[0].CreateModel(data.Type, new System.Action<GameObject, object>(this.OnMonsterLoad), (object) gameObject);
  }

  private void OnMonsterLoad(GameObject o, object data)
  {
    GameObject go = data as GameObject;
    List<Renderer> rendererList = new List<Renderer>();
    go.GetComponentsInChildren<Renderer>(true, (List<M0>) rendererList);
    int count1 = rendererList.Count;
    for (int index = 0; index < count1; ++index)
    {
      rendererList[index].sortingOrder += 201;
      rendererList[index].sortingLayerName = "HUD";
    }
    List<SpriteRenderer> spriteRendererList = new List<SpriteRenderer>();
    go.GetComponentsInChildren<SpriteRenderer>(true, (List<M0>) spriteRendererList);
    int count2 = spriteRendererList.Count;
    for (int index = 0; index < count2; ++index)
    {
      spriteRendererList[index].sortingOrder += 200;
      spriteRendererList[index].sortingLayerName = "HUD";
    }
    int layer = LayerMask.NameToLayer("TeleportUI");
    NGUITools.SetLayer(go, layer);
  }

  private void GenerateBuilding(object data)
  {
    if (this.m_TeleportMode == TeleportMode.PLACE_WORLD_BOSS)
    {
      this.GenerateWorldBoss();
    }
    else
    {
      GameObject go = (GameObject) null;
      switch (this.m_TeleportMode)
      {
        case TeleportMode.ADVANCE_TELEPORT:
        case TeleportMode.ALLIANCE_TELEPORT:
        case TeleportMode.BEGINNER_TELEPORT:
        case TeleportMode.WORLD_TELEPORT:
        case TeleportMode.KINGDOM_TELEPORT_BACK:
        case TeleportMode.FORTRESS_TELEPORT:
        case TeleportMode.ADVANCE_PIT_TELEPORT:
        case TeleportMode.ALLIANCE_WAR_ENTER_TELEPORT:
        case TeleportMode.ALLIANCE_WAR_TELEPORT:
        case TeleportMode.ALLIANCE_WAR_BACK_TELEPORT:
          CityData cityData = data as CityData;
          if (cityData != null)
          {
            int index = Math.Min(Math.Max(0, cityData.buildings.level_stronghold - 1), MapUtils.CityPrefabNames.Length - 1);
            go = PrefabManagerEx.Instance.Spawn(MapUtils.CityPrefabNames[index], this.m_Parent);
            break;
          }
          break;
        case TeleportMode.PLACE_FORTRESS:
          go = PrefabManagerEx.Instance.Spawn(MapUtils.GetAllianceBuildingPrefab(0), this.m_Parent);
          go.GetComponent<AllianceFortressController>().Reset();
          break;
        case TeleportMode.PLACE_WAREHOUSE:
          go = PrefabManagerEx.Instance.Spawn(MapUtils.GetAllianceBuildingPrefab(2), this.m_Parent);
          go.GetComponent<AllianceStoreHouseController>().Reset();
          break;
        case TeleportMode.PLACE_FARM:
          go = PrefabManagerEx.Instance.Spawn(MapUtils.GetAllianceBuildingPrefab(3), this.m_Parent);
          go.GetComponent<AllianceSuperMineController>().Reset();
          break;
        case TeleportMode.PLACE_SAWMILL:
          go = PrefabManagerEx.Instance.Spawn(MapUtils.GetAllianceBuildingPrefab(4), this.m_Parent);
          go.GetComponent<AllianceSuperMineController>().Reset();
          break;
        case TeleportMode.PLACE_MINE:
          go = PrefabManagerEx.Instance.Spawn(MapUtils.GetAllianceBuildingPrefab(5), this.m_Parent);
          go.GetComponent<AllianceSuperMineController>().Reset();
          break;
        case TeleportMode.PLACE_HOUSE:
          go = PrefabManagerEx.Instance.Spawn(MapUtils.GetAllianceBuildingPrefab(6), this.m_Parent);
          go.GetComponent<AllianceSuperMineController>().Reset();
          break;
        case TeleportMode.PLACE_PORTAL:
          go = PrefabManagerEx.Instance.Spawn(MapUtils.GetAllianceBuildingPrefab(7), this.m_Parent);
          go.GetComponent<AlliancePortalController>().Reset();
          break;
        case TeleportMode.PLACE_HOSPITAL:
          go = PrefabManagerEx.Instance.Spawn(MapUtils.GetAllianceBuildingPrefab(8), this.m_Parent);
          go.GetComponent<AllianceHospitalController>().Reset();
          break;
        case TeleportMode.PLACE_DRAGON_ALTAR:
          go = PrefabManagerEx.Instance.Spawn(MapUtils.GetAllianceBuildingPrefab(9), this.m_Parent);
          go.GetComponent<AllianceDragonAltarController>().Reset();
          break;
      }
      if (!(bool) ((UnityEngine.Object) go))
        return;
      go.transform.parent = this.CityRoot.transform;
      go.transform.localPosition = Vector3.zero;
      go.transform.localRotation = Quaternion.identity;
      go.transform.localScale = Vector3.one;
      List<SpriteRenderer> spriteRendererList = new List<SpriteRenderer>();
      go.GetComponentsInChildren<SpriteRenderer>(true, (List<M0>) spriteRendererList);
      int count = spriteRendererList.Count;
      for (int index = 0; index < count; ++index)
      {
        spriteRendererList[index].sortingOrder += 200;
        spriteRendererList[index].sortingLayerName = "HUD";
      }
      int layer = LayerMask.NameToLayer("TeleportUI");
      NGUITools.SetLayer(go, layer);
    }
  }

  public delegate void OnConfirmCallback(Coordinate target);

  public delegate void OnCancelCallback();
}
