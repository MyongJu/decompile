﻿// Decompiled with JetBrains decompiler
// Type: StrongholdWindowGlows
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class StrongholdWindowGlows : MonoBehaviour
{
  public float minAlpha = 0.01f;
  public float randomMinAlpha = 0.15f;
  public float randomMaxAlpha = 0.3f;
  public UISprite[] mGlows;

  private void Start()
  {
  }

  [DebuggerHidden]
  private IEnumerator GlowWindow(UISprite glowSprite)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    StrongholdWindowGlows.\u003CGlowWindow\u003Ec__Iterator45 windowCIterator45 = new StrongholdWindowGlows.\u003CGlowWindow\u003Ec__Iterator45();
    return (IEnumerator) windowCIterator45;
  }
}
