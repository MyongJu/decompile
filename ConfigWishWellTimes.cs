﻿// Decompiled with JetBrains decompiler
// Type: ConfigWishWellTimes
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigWishWellTimes
{
  private List<WishWellTimesData> m_WishWellTimesDataList;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<WishWellTimesData>(result as Hashtable, out this.m_WishWellTimesDataList);
    this.m_WishWellTimesDataList.Sort(new Comparison<WishWellTimesData>(ConfigWishWellTimes.Compare));
  }

  private static int Compare(WishWellTimesData x, WishWellTimesData y)
  {
    return x.Price - y.Price;
  }

  private WishWellTimesData GetWishWellTimesDataByTimes(int times)
  {
    times = Math.Min(Math.Max(1, times), this.m_WishWellTimesDataList.Count);
    return this.m_WishWellTimesDataList[times - 1];
  }

  public double GetBoostValue(int times)
  {
    if (times == 0)
      return 0.0;
    return this.GetWishWellTimesDataByTimes(times).Boost;
  }

  public int GetPriceValue(int times)
  {
    return this.GetWishWellTimesDataByTimes(times + 1).Price;
  }
}
