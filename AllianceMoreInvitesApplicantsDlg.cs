﻿// Decompiled with JetBrains decompiler
// Type: AllianceMoreInvitesApplicantsDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceMoreInvitesApplicantsDlg : UI.Dialog
{
  public AllianceMoreInvitesApplicantsDlg.AllianceInvitesPage mAllianceInvitesPage = new AllianceMoreInvitesApplicantsDlg.AllianceInvitesPage();
  public AllianceMoreInvitesApplicantsDlg.ApplicationsSentPage mApplicationsSentPage = new AllianceMoreInvitesApplicantsDlg.ApplicationsSentPage();
  public GameObject mInvitesRoot;
  public GameObject mApplicantsRoot;

  public override void OnShow(UIControler.UIParameter orgParam = null)
  {
    base.OnShow(orgParam);
    this.mAllianceInvitesPage.mBadge.SetActive(false);
    this.mApplicationsSentPage.mBadge.SetActive(false);
    PageTabsMonitor componentInChildren = this.gameObject.transform.GetComponentInChildren<PageTabsMonitor>();
    componentInChildren.onTabSelected += new System.Action<int>(this.OnTabSelected);
    componentInChildren.SetCurrentTab(0, true);
    this.UpdateReceivedInvites();
    this.UpdateSentApplications();
    this.RegisterListeners();
  }

  public override void OnHide(UIControler.UIParameter orgParam = null)
  {
    this.UnregisterListeners();
  }

  private void RegisterListeners()
  {
    AllianceManager.Instance.onPushDenyApplication += new System.Action<object>(this.OnServerPushDenyApplication);
    AllianceManager.Instance.onInviteChanged += new System.Action(this.OnInviteChanged);
    AllianceManager.Instance.onJoin += new System.Action(this.OnJoinAlliance);
  }

  private void UnregisterListeners()
  {
    AllianceManager.Instance.onPushDenyApplication -= new System.Action<object>(this.OnServerPushDenyApplication);
    AllianceManager.Instance.onInviteChanged -= new System.Action(this.OnInviteChanged);
    AllianceManager.Instance.onJoin -= new System.Action(this.OnJoinAlliance);
  }

  private void OnJoinAlliance()
  {
    this.UpdateSentApplications();
    this.UpdateReceivedInvites();
  }

  private void OnServerPushDenyApplication(object data)
  {
    this.UpdateSentApplications();
  }

  private void OnInviteChanged()
  {
    this.UpdateReceivedInvites();
  }

  private void UpdateReceivedInvites()
  {
    using (List<GameObject>.Enumerator enumerator = this.mAllianceInvitesPage.mSearchResultList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.mAllianceInvitesPage.mSearchResultList.Clear();
    if (PlayerData.inst.allianceId != 0L)
    {
      this.mAllianceInvitesPage.mBadge.SetActive(false);
      this.mAllianceInvitesPage.mNoDataMessage.SetActive(true);
    }
    else
    {
      int num = 0;
      using (List<AllianceInvitedApplyData>.Enumerator enumerator = DBManager.inst.DB_AllianceInviteApply.GetDataByUid(PlayerData.inst.uid).GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AllianceInvitedApplyData current = enumerator.Current;
          if (current.status == AllianceInvitedApplyData.Status.invited)
          {
            AllianceInvitesPanel component = Utils.DuplicateGOB(this.mAllianceInvitesPage.mAllianceInvitesItemPrefab).GetComponent<AllianceInvitesPanel>();
            this.mAllianceInvitesPage.mSearchResultList.Add(component.gameObject);
            component.gameObject.SetActive(true);
            component.onInviteAcceptPressed += new System.Action<int>(this.OnAcceptAllianceInviteBtnPressed);
            component.onInviteDenyPressed += new System.Action<int>(this.OnDenyAllianceInviteBtnPressed);
            AllianceData allianceData = DBManager.inst.DB_Alliance.Get(current.allianceId);
            int allianceId = (int) allianceData.allianceId;
            int allianceSymbolCode = allianceData.allianceSymbolCode;
            string allianceAcronym = allianceData.allianceAcronym;
            int allianceLevel = allianceData.allianceLevel;
            int memberCount = allianceData.memberCount;
            string allianceName = allianceData.allianceName;
            long power = allianceData.power;
            int memberMax = allianceData.memberMax;
            int levelByPoints = ConfigManager.inst.DB_Gift_Level.GetLevelByPoints((int) allianceData.giftPoints);
            string publicMessage = allianceData.publicMessage;
            component.SetDetails(allianceId, allianceSymbolCode, allianceAcronym, allianceName, allianceLevel, memberCount, memberMax, power, levelByPoints, publicMessage);
            ++num;
          }
        }
      }
      this.mAllianceInvitesPage.mItemsTable.Reposition();
      this.mAllianceInvitesPage.mScrollView.ResetPosition();
      if (num > 0)
      {
        this.mAllianceInvitesPage.mNoDataMessage.SetActive(false);
        this.mAllianceInvitesPage.mBadge.SetActive(true);
        this.mAllianceInvitesPage.mBadge.GetComponent<LabelBinder>().SetLabels((object) num);
      }
      else
      {
        this.mAllianceInvitesPage.mBadge.SetActive(false);
        this.mAllianceInvitesPage.mNoDataMessage.SetActive(true);
      }
    }
  }

  private void UpdateSentApplications()
  {
    using (List<GameObject>.Enumerator enumerator = this.mApplicationsSentPage.mSearchResultList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.mApplicationsSentPage.mSearchResultList.Clear();
    if (PlayerData.inst.allianceId != 0L)
    {
      this.mApplicationsSentPage.mNoDataMessage.SetActive(true);
      this.mApplicationsSentPage.mBadge.SetActive(false);
    }
    else
    {
      List<AllianceInvitedApplyData> dataByUid = DBManager.inst.DB_AllianceInviteApply.GetDataByUid(PlayerData.inst.uid);
      int num = 0;
      using (List<AllianceInvitedApplyData>.Enumerator enumerator = dataByUid.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AllianceInvitedApplyData current = enumerator.Current;
          if (current.status == AllianceInvitedApplyData.Status.asking)
          {
            AllianceInvitesPanel component = Utils.DuplicateGOB(this.mApplicationsSentPage.mApplicantItemPrefab).GetComponent<AllianceInvitesPanel>();
            this.mApplicationsSentPage.mSearchResultList.Add(component.gameObject);
            component.gameObject.SetActive(true);
            component.onRevokeBtnPressed += new System.Action<int>(this.OnRevokeSentApplication);
            AllianceData allianceData = DBManager.inst.DB_Alliance.Get(current.allianceId);
            int allianceId = (int) allianceData.allianceId;
            int allianceSymbolCode = allianceData.allianceSymbolCode;
            string allianceAcronym = allianceData.allianceAcronym;
            int allianceLevel = allianceData.allianceLevel;
            int memberCount = allianceData.memberCount;
            string allianceName = allianceData.allianceName;
            long power = allianceData.power;
            int memberMax = allianceData.memberMax;
            int levelByPoints = ConfigManager.inst.DB_Gift_Level.GetLevelByPoints((int) allianceData.giftPoints);
            string publicMessage = allianceData.publicMessage;
            component.SetDetails(allianceId, allianceSymbolCode, allianceAcronym, allianceName, allianceLevel, memberCount, memberMax, power, levelByPoints, publicMessage);
            ++num;
          }
        }
      }
      this.mApplicationsSentPage.mItemsTable.Reposition();
      this.mApplicationsSentPage.mScrollView.ResetPosition();
      if (num > 0)
      {
        this.mApplicationsSentPage.mNoDataMessage.SetActive(false);
        this.mApplicationsSentPage.mBadge.SetActive(true);
        this.mApplicationsSentPage.mBadge.GetComponent<LabelBinder>().SetLabels((object) num);
      }
      else
      {
        this.mApplicationsSentPage.mNoDataMessage.SetActive(true);
        this.mApplicationsSentPage.mBadge.SetActive(false);
      }
    }
  }

  private void OnTabSelected(int selectedTab)
  {
    switch (selectedTab)
    {
      case 0:
        this.OnInvitesTabPressed();
        break;
      case 1:
        this.OnApplicationsTabPressed();
        break;
    }
  }

  public void OnDlgClosePressed()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnInvitesTabPressed()
  {
    this.mInvitesRoot.SetActive(true);
    this.mApplicantsRoot.SetActive(false);
    this.UpdateReceivedInvites();
  }

  public void OnApplicationsTabPressed()
  {
    this.mInvitesRoot.SetActive(false);
    this.mApplicantsRoot.SetActive(true);
    this.UpdateSentApplications();
  }

  private void OnAcceptAllianceInviteBtnPressed(int allianceID)
  {
    AllianceManager.Instance.AcceptInvitation((long) allianceID, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.UpdateReceivedInvites();
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }));
  }

  private void OnDenyAllianceInviteBtnPressed(int allianceID)
  {
    AllianceManager.Instance.DenyInvitation((long) allianceID, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.UpdateReceivedInvites();
    }));
  }

  private void OnRevokeSentApplication(int allianceID)
  {
    AllianceManager.Instance.RevokeApplication((long) allianceID, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.mApplicationsSentPage.mBadge.SetActive(false);
      this.UpdateSentApplications();
    }));
  }

  [Serializable]
  public class AllianceInvitesPage
  {
    [HideInInspector]
    public List<GameObject> mSearchResultList = new List<GameObject>();
    public GameObject mAllianceInvitesItemPrefab;
    public GameObject mNoDataMessage;
    public UITable mItemsTable;
    public UIScrollView mScrollView;
    public GameObject mBadge;
  }

  [Serializable]
  public class ApplicationsSentPage
  {
    [HideInInspector]
    public List<GameObject> mSearchResultList = new List<GameObject>();
    public GameObject mApplicantItemPrefab;
    public GameObject mNoDataMessage;
    public UITable mItemsTable;
    public UIScrollView mScrollView;
    public GameObject mBadge;
  }
}
