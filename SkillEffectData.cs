﻿// Decompiled with JetBrains decompiler
// Type: SkillEffectData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class SkillEffectData
{
  public int sourceEventId = -1;
  public int targetEventId = -1;
  public bool faceTarget = true;
  public float time = -1f;
  public string animation;
  public string sourceVfx;
  public string targetVfx;
}
