﻿// Decompiled with JetBrains decompiler
// Type: RoulettePlayDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class RoulettePlayDlg : UI.Dialog
{
  private const string TAVERN_CHANCE_TITLE = "tavern_chance_title";
  private const string ADJUST_REWARD_POOL = "tavern_chance_reward_pool_name";
  private const string VALUE_STATUS = "tavern_chance_value_status";
  private const string VALUE_DIS = "tavern_chance_value_description";
  private const string SPIN_NUM = "tavern_chance_spin_number";
  private const string ROULETTE_CLOSED = "tavern_chance_closed_description";
  private const string BREATHE_EFFECT_PATH = "Prefab/VFX/fx_ContentPanel";
  private const string NPC_PATH = "Texture/GUI_Textures/npc_store_character";
  public UILabel title;
  public UILabel getMoreLabel;
  public UILabel coinCount;
  public UILabel adjustRewardsLabel;
  public UILabel spinOneLabel;
  public UILabel costCoinOne;
  public UILabel spinTenLabel;
  public UILabel costCoinTen;
  public UILabel spinOneHundredLabel;
  public UILabel costCoinOneHundred;
  public UILabel currentLuckyDis;
  public UILabel currentLuckyValue;
  public UILabel luckyDis;
  public UILabel luckyIncrement;
  public GameObject luckyWrapper;
  public UITexture coinIcon;
  public UITexture spinOneTexture;
  public UITexture spin10Texture;
  public UITexture spin100Texture;
  public UIButton spin1Btn;
  public UIButton spin10Btn;
  public UIButton spin100Btn;
  public UITexture npc;
  public GameObject rewardTip;
  private GameObject empty;
  private GameObject vfx;
  private GameObject breath;
  public List<RewardPoolItemRender> RewardPoolItems;
  private List<int> selectedRewards;
  private RewardPoolItemRender currentSelect;
  private RewardPoolItemRender nextToSelect;
  private RewardPoolItemRender targetReward;
  private RewardPoolItemRender backwardTarget;
  private Color oriCostOneColor;
  private Color oriCostTenColor;
  private Color oriCostOneHundredColor;
  private Vector3 oriIncrementPos;
  private Vector3 cachedTargetPos;
  private int lastStopIndex;
  private int lastPos;
  private int currentTarget;
  private int offsetStep;
  private int drawTime;
  private int cachedTargetIndex;
  private string vfxName;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    this.SendGetUserDataRequest();
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.UpdateStaticUI();
    this.AddEventHandler();
  }

  private void AddEventHandler()
  {
    for (int index = 0; index < this.RewardPoolItems.Count; ++index)
      this.RewardPoolItems[index].OnItemClickedHandler += new System.Action<RewardPoolItemRender>(this.OnRewardItemClicked);
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnConsumableItemDBChanged);
    DBManager.inst.DB_Item.onDataUpdated += new System.Action<int>(this.OnConsumableItemDBChanged);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnConsumableItemDBChanged);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void OnSecondEvent(int time)
  {
    if (!RoulettePayload.Instance.IsRouletteClose)
      return;
    this.currentLuckyValue.text = "0";
  }

  private void UpdateStaticUI()
  {
    this.title.text = Utils.XLAT("tavern_chance_title");
    BuilderFactory.Instance.HandyBuild((UIWidget) this.npc, "Texture/GUI_Textures/npc_store_character", (System.Action<bool>) null, true, false, string.Empty);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(RoulettePayload.Instance.ItemId);
    if (itemStaticInfo != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.coinIcon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.spinOneTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.spin10Texture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.spin100Texture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    }
    this.UpdateCoinCount();
    if (this.HasGoldToClaim())
      this.rewardTip.SetActive(true);
    else
      this.rewardTip.SetActive(false);
    this.getMoreLabel.text = Utils.XLAT("event_lucky_draw_get_more_free_button");
    this.adjustRewardsLabel.text = Utils.XLAT("tavern_chance_reward_pool_name");
    this.currentLuckyValue.text = RoulettePayload.Instance.LuckyGold.ToString();
    this.currentLuckyDis.text = Utils.XLAT("tavern_chance_value_status");
    this.luckyDis.text = Utils.XLAT("tavern_chance_value_description");
    this.oriIncrementPos = this.luckyWrapper.transform.localPosition;
    this.luckyWrapper.gameObject.SetActive(false);
    int goldSpent = RoulettePayload.Instance.GoldSpent;
    this.costCoinOne.text = goldSpent.ToString();
    this.costCoinTen.text = (10 * goldSpent).ToString();
    this.costCoinOneHundred.text = (100 * goldSpent).ToString();
    this.oriCostOneColor = this.costCoinOne.color;
    this.oriCostTenColor = this.costCoinTen.color;
    this.oriCostOneHundredColor = this.costCoinOneHundred.color;
    this.UpdatePriceColors();
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("0", "1");
    this.spinOneLabel.text = ScriptLocalization.GetWithPara("tavern_chance_spin_number", para, true);
    para.Clear();
    para.Add("0", "10");
    this.spinTenLabel.text = ScriptLocalization.GetWithPara("tavern_chance_spin_number", para, true);
    para.Clear();
    para.Add("0", "100");
    this.spinOneHundredLabel.text = ScriptLocalization.GetWithPara("tavern_chance_spin_number", para, true);
    this.empty = new GameObject();
    this.empty.name = "empty";
    this.empty.transform.parent = this.gameObject.transform;
    this.empty.transform.localPosition = Vector3.zero;
    this.empty.transform.localScale = Vector3.zero;
  }

  private void UpdatePriceColors()
  {
    int goldSpent = RoulettePayload.Instance.GoldSpent;
    int consumableGoldCount = RoulettePayload.Instance.ConsumableGoldCount;
    this.costCoinOne.color = consumableGoldCount < goldSpent ? Color.red : this.oriCostOneColor;
    this.costCoinTen.color = consumableGoldCount < 10 * goldSpent ? Color.red : this.oriCostTenColor;
    this.costCoinOneHundred.color = consumableGoldCount < 100 * goldSpent ? Color.red : this.oriCostOneHundredColor;
  }

  private void UpdateCoinCount()
  {
    this.coinCount.text = Utils.FormatThousands(RoulettePayload.Instance.ConsumableGoldCount.ToString());
  }

  private void PlayUpdateLuckyAnimation()
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    else if (this.HasHighLevelAward())
      this.PlayLuckyEffect(int.Parse(this.currentLuckyValue.text), RoulettePayload.Instance.DrawReturnedLuck);
    else
      this.StartCoroutine(this.LuckyIncrementEffect());
  }

  [DebuggerHidden]
  private IEnumerator LuckyIncrementEffect()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RoulettePlayDlg.\u003CLuckyIncrementEffect\u003Ec__Iterator99()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void PlayLuckyEffect(int currentValue, int targetValue)
  {
    iTween.ValueTo(this.currentLuckyValue.gameObject, new Hashtable()
    {
      {
        (object) "from",
        (object) currentValue
      },
      {
        (object) "to",
        (object) targetValue
      },
      {
        (object) "time",
        (object) 2f
      },
      {
        (object) "easeType",
        (object) iTween.EaseType.linear
      },
      {
        (object) "oncomplete",
        (object) "OnPlayLuckyEnd"
      },
      {
        (object) "oncompletetarget",
        (object) this.gameObject
      },
      {
        (object) "onupdate",
        (object) "OnValueUpdate"
      },
      {
        (object) "onupdatetarget",
        (object) this.gameObject
      }
    });
  }

  private void OnPlayLuckyEnd()
  {
    this.currentLuckyValue.text = RoulettePayload.Instance.DrawReturnedLuck.ToString();
    this.EnableDrawButtons(true);
    this.UpdateLuckyEffect(RoulettePayload.Instance.DrawReturnedLuck);
  }

  private void OnValueUpdate(float value)
  {
    this.currentLuckyValue.text = ((int) value).ToString();
  }

  private void OnAnimationEnd()
  {
    this.luckyWrapper.gameObject.SetActive(false);
    this.luckyWrapper.transform.localPosition = this.oriIncrementPos;
  }

  private void ShowInitLuckyNumber()
  {
    this.currentLuckyValue.text = RoulettePayload.Instance.LuckyGold.ToString();
    this.UpdateLuckyEffect(RoulettePayload.Instance.LuckyGold);
  }

  private void UpdateLuckyEffect(int lucky)
  {
    string str = "Prefab/VFX/";
    string effectName = RoulettePayload.Instance.GradeInfo.GetEffectName(lucky);
    if (!(this.vfxName != effectName))
      return;
    this.vfxName = effectName;
    if ((bool) ((UnityEngine.Object) this.vfx))
    {
      this.vfx.SetActive(false);
      this.vfx.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.vfx);
      this.vfx = (GameObject) null;
    }
    GameObject original = AssetManager.Instance.HandyLoad(str + effectName, (System.Type) null) as GameObject;
    if (!(bool) ((UnityEngine.Object) original))
      return;
    this.vfx = UnityEngine.Object.Instantiate<GameObject>(original);
    this.vfx.transform.parent = this.currentLuckyValue.gameObject.transform;
    this.vfx.transform.localPosition = Vector3.zero;
    this.vfx.transform.localScale = Vector3.one;
    Utils.SetLayer(this.vfx, this.currentLuckyValue.gameObject.layer);
  }

  private void UpdateRewardPool()
  {
    if (RoulettePayload.Instance.RewardIds == null)
    {
      this.ResetRewardPool();
    }
    else
    {
      this.SetRewardPool(RoulettePayload.Instance.RewardIds);
      this.SetLastSelected();
    }
  }

  private void SetRewardPool(List<int> mainIds)
  {
    List<int> intList = new List<int>();
    for (int index1 = 0; index1 < this.RewardPoolItems.Count; ++index1)
    {
      for (int index2 = 0; index2 < mainIds.Count; ++index2)
      {
        TavernWheelMainInfo tavernWheelMainInfo = ConfigManager.inst.DB_TavernWheelMain.Get(mainIds[index2]);
        if (tavernWheelMainInfo != null)
        {
          TavernWheelGradeInfo tavernWheelGradeInfo = ConfigManager.inst.DB_TavernWheelGrade.Get(tavernWheelMainInfo.gradeId);
          List<Reward.RewardsValuePair> rewards = tavernWheelMainInfo.rewards.GetRewards();
          if (!intList.Contains(index2))
          {
            RewardPoolItemData data = new RewardPoolItemData();
            data.itemId = rewards[0].internalID;
            data.itemCount = rewards[0].value;
            data.color = tavernWheelGradeInfo.color;
            data.grade = tavernWheelGradeInfo.grade;
            intList.Add(index2);
            this.RewardPoolItems[index1].FeedData(data);
            break;
          }
        }
      }
    }
  }

  private void ResetRewardPool()
  {
    for (int index = 0; index < this.RewardPoolItems.Count; ++index)
      this.RewardPoolItems[index].FeedData((RewardPoolItemData) null);
  }

  private void RemoveEventHandler()
  {
    for (int index = 0; index < this.RewardPoolItems.Count; ++index)
      this.RewardPoolItems[index].OnItemClickedHandler -= new System.Action<RewardPoolItemRender>(this.OnRewardItemClicked);
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnConsumableItemDBChanged);
    DBManager.inst.DB_Item.onDataUpdated -= new System.Action<int>(this.OnConsumableItemDBChanged);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnConsumableItemDBChanged);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.RemoveEventHandler();
    UnityEngine.Object.Destroy((UnityEngine.Object) this.empty);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.vfx);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.breath);
    this.empty = (GameObject) null;
    this.vfx = (GameObject) null;
    this.breath = (GameObject) null;
  }

  public void SpinOneTime()
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    else if (RoulettePayload.Instance.RewardIds == null && this.selectedRewards == null)
      this.ShowConfirmBox();
    else if (RoulettePayload.Instance.ConsumableGoldCount >= RoulettePayload.Instance.GoldSpent)
      RoulettePayload.Instance.SendDrawRewardsRequest(new System.Action<bool, object, int>(this.OnDrawRewardCallback), 1, true);
    else
      this.ShowGetMore();
  }

  public void SpinTenTimes()
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    else if (RoulettePayload.Instance.RewardIds == null && this.selectedRewards == null)
      this.ShowConfirmBox();
    else if (RoulettePayload.Instance.ConsumableGoldCount >= RoulettePayload.Instance.GoldSpent * 10)
      RoulettePayload.Instance.SendDrawRewardsRequest(new System.Action<bool, object, int>(this.OnDrawRewardCallback), 10, true);
    else
      this.ShowGetMore();
  }

  public void SpinOneHundredTimes()
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    else if (RoulettePayload.Instance.RewardIds == null && this.selectedRewards == null)
      this.ShowConfirmBox();
    else if (RoulettePayload.Instance.ConsumableGoldCount >= RoulettePayload.Instance.GoldSpent * 100)
      RoulettePayload.Instance.SendDrawRewardsRequest(new System.Action<bool, object, int>(this.OnDrawRewardCallback), 100, true);
    else
      this.ShowGetMore();
  }

  private void ShowConfirmBox()
  {
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = Utils.XLAT("id_uppercase_confirm"),
      yes = Utils.XLAT("id_uppercase_okay"),
      no = Utils.XLAT("id_uppercase_cancel"),
      content = Utils.XLAT("tavern_wheel_reward_go_select_first"),
      noCallback = (System.Action) null,
      yesCallback = (System.Action) (() => this.OpenRewardSelectionPopup((List<int>) null))
    });
  }

  private void ShowGetMore()
  {
    UIManager.inst.OpenPopup("Marksman/CommonConfirmPopup", (Popup.PopupParameter) new CommonConfirmPopup.Parameter()
    {
      title = Utils.XLAT("id_uppercase_insufficient_items"),
      content = Utils.XLAT("tavern_dice_insufficient_chips_description"),
      confirmButtonText = Utils.XLAT("id_uppercase_get_more"),
      onOkayCallback = new System.Action(this.OnPropsBuyPressed)
    });
  }

  private void OnConsumableItemDBChanged(int id)
  {
    this.UpdateCoinCount();
    this.UpdatePriceColors();
  }

  private void OnPropsBuyPressed()
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    else
      Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  private void OnRewardItemClicked(RewardPoolItemRender render)
  {
    if (RoulettePayload.Instance.RewardIds == null && this.selectedRewards == null)
    {
      if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
        UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
      else
        this.ShowConfirmBox();
    }
    else
    {
      for (int index = 0; index < this.RewardPoolItems.Count; ++index)
      {
        if ((UnityEngine.Object) this.RewardPoolItems[index] == (UnityEngine.Object) render)
          Utils.ShowItemTip(render.Data.itemId, this.RewardPoolItems[index].transform, 0L, 0L, 0);
      }
    }
  }

  public void GetMoreCoin()
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
    {
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    }
    else
    {
      if (this.rewardTip.activeInHierarchy)
        this.rewardTip.SetActive(false);
      new RouletteFreeRewardPopup.Parameter().CollectCompleteHandler = new System.Action(this.OnClaimHandler);
      UIManager.inst.OpenPopup("Roulette/TurnableFreeRewardPopup", (Popup.PopupParameter) null);
    }
  }

  private bool HasGoldToClaim()
  {
    bool flag = false;
    TavernWheelGroupInfo tavernWheelGroupInfo = ConfigManager.inst.DB_TavernWheelGroup.Get(RoulettePayload.Instance.GroupId);
    if (tavernWheelGroupInfo != null)
    {
      int currentPoint = DBManager.inst.DB_DailyActives.CurrentPoint;
      int itemDailyScore = tavernWheelGroupInfo.itemDailyScore;
      int itemGold = tavernWheelGroupInfo.itemGold;
      int itemNumber = tavernWheelGroupInfo.itemNumber;
      int paymentGotGold = RoulettePayload.Instance.PaymentGotGold;
      int claimedGold = RoulettePayload.Instance.ClaimedGold;
      if (paymentGotGold / itemGold * itemNumber - claimedGold > 0 || currentPoint >= itemDailyScore)
        flag = true;
    }
    return flag;
  }

  public void AdjustRewardsPool()
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    else if (RoulettePayload.Instance.RewardIds == null && this.selectedRewards == null)
      this.OpenRewardSelectionPopup((List<int>) null);
    else
      this.OpenRewardSelectionPopup(RoulettePayload.Instance.RewardIds);
  }

  private void OnDrawRewardCallback(bool ret, object data, int time)
  {
    if (!ret)
      return;
    this.EnableDrawButtons(false);
    this.drawTime = time;
    this.PlayMarqueeAnimation();
  }

  private void EnableDrawButtons(bool enable)
  {
    this.spin1Btn.isEnabled = enable;
    this.spin10Btn.isEnabled = enable;
    this.spin100Btn.isEnabled = enable;
  }

  private void PlayMarqueeAnimation()
  {
    this.StartCoroutine(this.MarqueeAnimation(3f, 3));
  }

  [DebuggerHidden]
  private IEnumerator MarqueeAnimation(float time, int laps)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RoulettePlayDlg.\u003CMarqueeAnimation\u003Ec__Iterator9A()
    {
      laps = laps,
      time = time,
      \u003C\u0024\u003Elaps = laps,
      \u003C\u0024\u003Etime = time,
      \u003C\u003Ef__this = this
    };
  }

  private void OnMarqueeAnimationUpdate()
  {
    int x = (int) this.empty.transform.localPosition.x;
    if (x / this.RewardPoolItems.Count >= 0)
      x %= this.RewardPoolItems.Count;
    this.nextToSelect = this.RewardPoolItems[x];
    if ((UnityEngine.Object) this.currentSelect != (UnityEngine.Object) this.nextToSelect)
    {
      this.currentSelect.Select = false;
      this.currentSelect = this.nextToSelect;
      this.currentSelect.Select = true;
    }
    this.AddBreathEffect(this.currentSelect);
  }

  private void OnMarqueeAnimationEnd()
  {
    if ((UnityEngine.Object) this.currentSelect != (UnityEngine.Object) this.backwardTarget)
    {
      this.currentSelect.Select = false;
      this.currentSelect = this.backwardTarget;
      this.currentSelect.Select = true;
      this.AddBreathEffect(this.currentSelect);
    }
    if (this.offsetStep > 0)
    {
      this.StartCoroutine(this.RunLastStopAnimation());
    }
    else
    {
      this.lastStopIndex = this.currentTarget;
      this.PlayCollectWinEffect(this.drawTime);
      this.PlayUpdateLuckyAnimation();
      this.RecordLastSelect();
    }
  }

  [DebuggerHidden]
  private IEnumerator RunLastStopAnimation()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RoulettePlayDlg.\u003CRunLastStopAnimation\u003Ec__Iterator9B()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void RecordLastSelect()
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.currentSelect))
      return;
    PlayerPrefsEx.SetIntByUid("roulette_last_stop_position", this.lastStopIndex);
    PlayerPrefsEx.Save();
  }

  private Vector3 GetLastStopPoint()
  {
    return new Vector3((float) RoulettePayload.Instance.LastSelectedReward, 0.0f, 0.0f);
  }

  private bool HasHighLevelAward()
  {
    bool flag = false;
    Dictionary<string, int>.Enumerator enumerator = RoulettePayload.Instance.RewardsMap.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (ConfigManager.inst.DB_TavernWheelGrade.Get(ConfigManager.inst.DB_TavernWheelMain.Get(int.Parse(enumerator.Current.Key)).gradeId).grade == 3)
      {
        flag = true;
        break;
      }
    }
    return flag;
  }

  private RewardPoolItemRender GetDrawReturnedItem()
  {
    RewardPoolItemRender rewardPoolItemRender = (RewardPoolItemRender) null;
    Dictionary<string, int> rewardsMap = RoulettePayload.Instance.RewardsMap;
    if (rewardsMap != null && rewardsMap.Count > 0)
    {
      int internalId1 = 0;
      Dictionary<string, int>.Enumerator enumerator = rewardsMap.GetEnumerator();
      while (enumerator.MoveNext())
        internalId1 = int.Parse(enumerator.Current.Key);
      TavernWheelMainInfo tavernWheelMainInfo = ConfigManager.inst.DB_TavernWheelMain.Get(internalId1);
      if (tavernWheelMainInfo != null)
      {
        List<Reward.RewardsValuePair> rewards = tavernWheelMainInfo.rewards.GetRewards();
        int internalId2 = rewards[0].internalID;
        int num = rewards[0].value;
        for (int index = 0; index < this.RewardPoolItems.Count; ++index)
        {
          if (this.RewardPoolItems[index].Data.itemId == internalId2 && this.RewardPoolItems[index].Data.itemCount == num)
          {
            rewardPoolItemRender = this.RewardPoolItems[index];
            break;
          }
        }
      }
    }
    return rewardPoolItemRender;
  }

  private void PlayCollectWinEffect(int time)
  {
    Dictionary<string, int> rewardsMap = RoulettePayload.Instance.RewardsMap;
    if (rewardsMap == null)
      return;
    if (time == 1)
    {
      Dictionary<string, int>.Enumerator enumerator = rewardsMap.GetEnumerator();
      int internalId = 0;
      if (enumerator.MoveNext())
        internalId = int.Parse(enumerator.Current.Key);
      TavernWheelMainInfo tavernWheelMainInfo = ConfigManager.inst.DB_TavernWheelMain.Get(internalId);
      TavernWheelGradeInfo tavernWheelGradeInfo = ConfigManager.inst.DB_TavernWheelGrade.Get(tavernWheelMainInfo.gradeId);
      List<Reward.RewardsValuePair> rewards = tavernWheelMainInfo.rewards.GetRewards();
      RoulettePayload.Instance.ShowCollectEffect(rewards[0].internalID, rewards[0].value, tavernWheelGradeInfo.grade);
    }
    else
    {
      RoulettePayload.Instance.SpecifyMiddleAndHignRewards();
      List<MultiWinItemData> rewardDataList = new List<MultiWinItemData>();
      Dictionary<string, int>.Enumerator enumerator = rewardsMap.GetEnumerator();
      while (enumerator.MoveNext())
      {
        TavernWheelMainInfo tavernWheelMainInfo = ConfigManager.inst.DB_TavernWheelMain.Get(int.Parse(enumerator.Current.Key));
        TavernWheelGradeInfo tavernWheelGradeInfo = ConfigManager.inst.DB_TavernWheelGrade.Get(tavernWheelMainInfo.gradeId);
        List<Reward.RewardsValuePair> rewards = tavernWheelMainInfo.rewards.GetRewards();
        int internalId = rewards[0].internalID;
        int num1 = rewards[0].value;
        int num2 = enumerator.Current.Value;
        if (this.ContainsItem(rewardDataList, internalId))
        {
          int totalCount = num1 * num2;
          this.UpdateItems(rewardDataList, internalId, totalCount);
        }
        else
          rewardDataList.Add(new MultiWinItemData()
          {
            itemId = internalId,
            itemCount = num1,
            multiplier = num2,
            totalCount = num1 * num2,
            grade = tavernWheelGradeInfo.grade
          });
      }
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      UIManager.inst.OpenPopup("Roulette/TurnableWinPopup", (Popup.PopupParameter) new RouletteMultiWinPopup.Parameter()
      {
        time = time,
        rewardDataList = rewardDataList,
        onWinFinished = (System.Action) null,
        onDrawBegin = (System.Action) (() => this.SpinMultipleTimes(time))
      });
    }
  }

  private bool ContainsItem(List<MultiWinItemData> rewardDataList, int itemId)
  {
    bool flag = false;
    for (int index = 0; index < rewardDataList.Count; ++index)
    {
      if (rewardDataList[index].itemId == itemId)
      {
        flag = true;
        break;
      }
    }
    return flag;
  }

  private void UpdateItems(List<MultiWinItemData> rewardDataList, int itemId, int totalCount)
  {
    for (int index = 0; index < rewardDataList.Count; ++index)
    {
      if (rewardDataList[index].itemId == itemId)
        rewardDataList[index].totalCount += totalCount;
    }
  }

  private void SpinMultipleTimes(int time)
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
    {
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    }
    else
    {
      switch (time)
      {
        case 10:
          this.SpinTenTimes();
          break;
        case 100:
          this.SpinOneHundredTimes();
          break;
      }
    }
  }

  private void OnGetUserDataCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.UpdateCoinCount();
    this.ShowInitLuckyNumber();
    this.UpdateRewardPool();
  }

  private void OnClaimHandler()
  {
    this.UpdateCoinCount();
  }

  private void SendGetUserDataRequest()
  {
    RoulettePayload.Instance.SendGetUserDataRequest(new System.Action<bool, object>(this.OnGetUserDataCallback), true);
  }

  private void OpenRewardSelectionPopup(List<int> rewards)
  {
    UIManager.inst.OpenPopup("Roulette/TurnableChooseRewardPopup", (Popup.PopupParameter) new SelectRewardsPopup.Parameter()
    {
      SelectionCompleteHandler = new System.Action<List<int>>(this.OnSelectionCompleteHandler),
      rewards = rewards
    });
  }

  private void OnSelectionCompleteHandler(List<int> rewards)
  {
    this.selectedRewards = rewards;
    this.SetRewardPool(rewards);
    if (RoulettePayload.Instance.RewardIds == null)
      this.SetFirstSelected();
    else
      RoulettePayload.Instance.RewardIds = (List<int>) null;
    RoulettePayload.Instance.RewardIds = this.selectedRewards;
  }

  private void SetFirstSelected()
  {
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.currentSelect)
    {
      this.currentSelect.Select = false;
      this.currentSelect = (RewardPoolItemRender) null;
    }
    this.lastPos = 0;
    this.lastStopIndex = 0;
    this.empty.transform.localPosition = Vector3.zero;
    this.RewardPoolItems[0].Select = true;
    this.currentSelect = this.RewardPoolItems[0];
    this.AddBreathEffect(this.currentSelect);
    PlayerPrefsEx.SetIntByUid("roulette_last_stop_position", this.lastStopIndex);
    PlayerPrefsEx.Save();
  }

  private void SetLastSelected()
  {
    this.empty.transform.localPosition = this.GetLastStopPoint();
    int lastSelectedReward = RoulettePayload.Instance.LastSelectedReward;
    this.RewardPoolItems[lastSelectedReward].Select = true;
    this.currentSelect = this.RewardPoolItems[lastSelectedReward];
    this.lastStopIndex = lastSelectedReward;
    this.lastPos = lastSelectedReward;
    this.AddBreathEffect(this.currentSelect);
  }

  private void AddBreathEffect(RewardPoolItemRender render)
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.breath)
    {
      GameObject original = AssetManager.Instance.HandyLoad("Prefab/VFX/fx_ContentPanel", (System.Type) null) as GameObject;
      if ((bool) ((UnityEngine.Object) original))
        this.breath = UnityEngine.Object.Instantiate<GameObject>(original);
    }
    this.breath.transform.parent = render.transform;
    this.breath.transform.localPosition = Vector3.zero;
    this.breath.transform.localScale = Vector3.one;
    Utils.SetLayer(this.breath, render.gameObject.layer);
  }
}
