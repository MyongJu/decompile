﻿// Decompiled with JetBrains decompiler
// Type: LotteryDiyFreeRewardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;

public class LotteryDiyFreeRewardPopup : Popup
{
  public UILabel freeRewardHint;
  public UILabel totalTopupGoldAmount;
  public UILabel collectedItemCount;
  public UILabel remainedItemCount;
  public UILabel dailyRequiredHint;
  public UILabel dailyRewardItemCount;
  public UIButton topupButton;
  public UIButton topupClaimButton;
  public UIButton dailyRewardClaimButton;
  public UIButton dailyRewardGotoButton;
  public UITexture topupRewardItemTexture;
  public UITexture dailyRewardItemTexture;
  private long _remainedClaim;
  private System.Action _freeClaimedHandler;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    LotteryDiyFreeRewardPopup.Parameter parameter = orgParam as LotteryDiyFreeRewardPopup.Parameter;
    if (parameter != null)
      this._freeClaimedHandler = parameter.freeClaimedHandler;
    this.UpdateUI();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnTopupBtnPressed()
  {
    this.OnCloseBtnPressed();
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  public void OnTopupClaimBtnPressed()
  {
    this.CollectReward(0);
  }

  public void OnDailyGotoBtnPressed()
  {
    RequestManager.inst.SendRequest("Quest:refreshDailyQuest", (Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.OnCloseBtnPressed();
      UIManager.inst.OpenPopup("DailyActivy/DailyActiviesPopup", (Popup.PopupParameter) null);
    }), true);
  }

  public void OnDailyClaimBtnPressed()
  {
    this.CollectReward(1);
  }

  private void CollectReward(int type)
  {
    RequestManager.inst.SendRequest("casino:claim", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) nameof (type),
        (object) type
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      LotteryDiyPayload.Instance.LotteryData.Decode(data as Hashtable);
      this.UpdateUI();
      this.ShowCollectedEffect(type);
      if (this._freeClaimedHandler == null)
        return;
      this._freeClaimedHandler();
    }), true);
  }

  private void ShowCollectedEffect(int type)
  {
    RewardsCollectionAnimator.Instance.Clear();
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(LotteryDiyPayload.Instance.LotteryData.SpendItem);
    if (itemStaticInfo != null)
      RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
      {
        icon = itemStaticInfo.ImagePath,
        count = type != 0 ? (float) LotteryDiyPayload.Instance.LotteryData.GetItemDailyNumber : (float) this._remainedClaim
      });
    RewardsCollectionAnimator.Instance.CollectItems(false);
  }

  private void UpdateUI()
  {
    if (LotteryDiyPayload.Instance.LotteryData == null)
      return;
    Dictionary<string, string> para = new Dictionary<string, string>();
    long getItemGold = LotteryDiyPayload.Instance.LotteryData.GetItemGold;
    long getItemGoldNumber = LotteryDiyPayload.Instance.LotteryData.GetItemGoldNumber;
    para.Add("0", getItemGold.ToString());
    para.Add("1", getItemGoldNumber.ToString());
    this.freeRewardHint.text = ScriptLocalization.GetWithPara("event_lucky_draw_gold_topup_subtitle", para, true);
    para.Clear();
    para.Add("0", Utils.FormatThousands(LotteryDiyPayload.Instance.LotteryData.CurrentGold.ToString()));
    this.totalTopupGoldAmount.text = ScriptLocalization.GetWithPara("event_lucky_draw_gold_topup_num", para, true);
    para.Clear();
    para.Add("0", LotteryDiyPayload.Instance.LotteryData.GoldClaimNumber.ToString());
    this.collectedItemCount.text = ScriptLocalization.GetWithPara("event_lucky_draw_draws_taken", para, true);
    if (getItemGold > 0L)
      this._remainedClaim = LotteryDiyPayload.Instance.LotteryData.CurrentGold / getItemGold * getItemGoldNumber - LotteryDiyPayload.Instance.LotteryData.GoldClaimNumber;
    para.Clear();
    para.Add("0", (this._remainedClaim <= 0L ? 0L : this._remainedClaim).ToString());
    this.remainedItemCount.text = ScriptLocalization.GetWithPara("event_lucky_draw_draws_remaining", para, true);
    string str = LotteryDiyPayload.Instance.LotteryData.CurrentDailyScore.ToString() + "/" + LotteryDiyPayload.Instance.LotteryData.GetItemDailyScore.ToString();
    para.Clear();
    para.Add("0", str);
    this.dailyRequiredHint.text = ScriptLocalization.GetWithPara("event_lucky_draw_daily_reward_tip", para, true);
    this.dailyRewardItemCount.text = LotteryDiyPayload.Instance.LotteryData.GetItemDailyNumber.ToString();
    if (LotteryDiyPayload.Instance.LotteryData.CurrentDailyScore < LotteryDiyPayload.Instance.LotteryData.GetItemDailyScore)
    {
      NGUITools.SetActive(this.dailyRewardClaimButton.gameObject, false);
      NGUITools.SetActive(this.dailyRewardGotoButton.gameObject, true);
    }
    else
    {
      NGUITools.SetActive(this.dailyRewardClaimButton.gameObject, true);
      NGUITools.SetActive(this.dailyRewardGotoButton.gameObject, false);
      if (LotteryDiyPayload.Instance.LotteryData.DailyIsClaim)
        this.dailyRewardClaimButton.isEnabled = false;
    }
    if (this._remainedClaim > 0L)
    {
      NGUITools.SetActive(this.topupButton.gameObject, false);
      NGUITools.SetActive(this.topupClaimButton.gameObject, true);
    }
    else
    {
      NGUITools.SetActive(this.topupButton.gameObject, true);
      NGUITools.SetActive(this.topupClaimButton.gameObject, false);
    }
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(LotteryDiyPayload.Instance.LotteryData.SpendItem);
    if (itemStaticInfo == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.topupRewardItemTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.dailyRewardItemTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
  }

  public class Parameter : Popup.PopupParameter
  {
    public System.Action freeClaimedHandler;
  }
}
