﻿// Decompiled with JetBrains decompiler
// Type: ConfigAbyssMine
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAbyssMine
{
  public Dictionary<string, AbyssMineInfo> datas;
  private Dictionary<int, AbyssMineInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<AbyssMineInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public AbyssMineInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (AbyssMineInfo) null;
  }

  public AbyssMineInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AbyssMineInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, AbyssMineInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, AbyssMineInfo>) null;
  }
}
