﻿// Decompiled with JetBrains decompiler
// Type: SpeedUpPopUp
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class SpeedUpPopUp : Popup
{
  private Dictionary<int, SpeedUpItem> speedUpItems = new Dictionary<int, SpeedUpItem>();
  private bool autoCalc = true;
  private ProgressBarTweenProxy proxy = new ProgressBarTweenProxy();
  private bool autoFixProgressBar = true;
  public IconGroup group;
  public UIScrollView scroll;
  public UILabel normalatip;
  public UIProgressBar timeProgress;
  public UILabel timeprogressLabel;
  public UISlider countProgress;
  public UIInput inputLabel;
  public UILabel normalTime;
  public UILabel timeTip;
  public UIButton useButton;
  public UILabel buttonLabel;
  public UILabel freetimeLabel;
  public UISprite left;
  public UISprite right;
  public GameObject countContent;
  public UnityEngine.Animation speedupViewChangeAnim;
  public UITexture artifactTexture;
  public UILabel artifactName;
  public UILabel artifactProtectedTime;
  public UILabel artifactEffectDesc;
  public UILabel artifactCdTime;
  public UIButton artifactUsedButton;
  public UIButton artifactChangeButton;
  public GameObject artifactProtectedTimeNode;
  [NonSerialized]
  public ItemBag.ItemType speedupType;
  [NonSerialized]
  public TimerType mTimerType;
  private long m_JobId;
  private bool canUseSpeedupArtifact;
  private bool canUseCommonSpeedup;
  [NonSerialized]
  public BuildingController mTarBC;
  private int selectedItem;
  private int _useCount;
  private int _artifactId;
  private ArtifactInfo _artifactInfo;
  private float _artifactNamePositionY;
  public UnityEngine.Animation animation;
  private bool init;
  private bool _destroy;

  private int UseCount
  {
    get
    {
      return this._useCount;
    }
    set
    {
      if (this._useCount == value || value <= 0 || (value > this.MaxCount || value == 0))
        return;
      this._useCount = value;
      this.CalcItem(false, false);
    }
  }

  public long JobId
  {
    get
    {
      return this.m_JobId;
    }
    set
    {
      this.m_JobId = value;
    }
  }

  public void OnAddButtonDown()
  {
    this.autoFixProgressBar = false;
    this.proxy.Slider = (UIProgressBar) this.countProgress;
    this.proxy.MaxSize = this.MaxCount;
    this.proxy.Play(1, -1f);
  }

  public void OnAddButtonUp()
  {
    this.autoFixProgressBar = true;
    this.proxy.Stop();
  }

  public void OnRemoveButtonDown()
  {
    this.autoFixProgressBar = false;
    this.proxy.Slider = (UIProgressBar) this.countProgress;
    this.proxy.MaxSize = this.MaxCount;
    this.proxy.Play(-1, 1f / (float) this.MaxCount);
  }

  public void OnRemoveButtonUp()
  {
    this.autoFixProgressBar = true;
    this.proxy.Stop();
  }

  public void OnAddButtonPress()
  {
    this.autoCalc = false;
    ++this.UseCount;
  }

  public void OnRemoveButtonPress()
  {
    this.autoCalc = false;
    --this.UseCount;
  }

  public bool CanFreeSpeedup()
  {
    if (this.mTimerType == TimerType.TIMER_NONE)
      return false;
    if (this.mTimerType != TimerType.TIMER_CONSTRUCT && this.mTimerType != TimerType.TIMER_UPGRADE)
      return this.mTimerType == TimerType.TIMER_DESTRUCT;
    return true;
  }

  public void OnNormalSpeedupBtnPressed()
  {
    this.speedupViewChangeAnim.Play("SpeedupNormalView");
  }

  public void OnArtifactSpeedupBtnPressed()
  {
    this.ShowArtifactDetails();
    this.speedupViewChangeAnim.Play("SpeedupArtifactView");
  }

  public void OnArtifactActiveBtnPressed()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "world_id"] = (object) PlayerData.inst.userData.world_id;
    postData[(object) "uid"] = (object) PlayerData.inst.uid;
    postData[(object) "artifact_id"] = (object) this._artifactId;
    postData[(object) "job_id"] = (object) this.JobId;
    RequestManager.inst.SendRequest("Artifact:castSkill", postData, (System.Action<bool, object>) ((ret, data) => this.UpdateArtifactActiveBtnStatus(this._artifactId)), true);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.AddEventHandler();
    SpeedUpPopUp.Parameter parameter = orgParam as SpeedUpPopUp.Parameter;
    this.JobId = parameter.jobId;
    this.speedupType = parameter.speedupType;
    this.mTimerType = parameter.timerType;
    this.mTarBC = parameter.tarBC;
    this.canUseSpeedupArtifact = parameter.canUseSpeedupArtifact;
    this.canUseCommonSpeedup = parameter.canUseCommonSpeedup;
    this._artifactInfo = ConfigManager.inst.DB_Artifact.GetArtifactByActiveEffect("time_hammer");
    this._artifactId = this._artifactInfo == null ? 0 : this._artifactInfo.internalId;
    this._artifactNamePositionY = this.artifactName.transform.localPosition.y;
    this.artifactChangeButton.isEnabled = this._artifactId > 0;
    if (this.artifactChangeButton.isEnabled)
      GreyUtility.Normal(this.artifactChangeButton.gameObject);
    else
      GreyUtility.Grey(this.artifactChangeButton.gameObject);
    NGUITools.SetActive(this.artifactChangeButton.gameObject, this.canUseSpeedupArtifact && this.IsArtifactEquiped(this._artifactId));
    this.RefreshTimeBar();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Clear();
    this.RemoveEventHandler();
  }

  private void Clear()
  {
    this.group.Clear();
    this.speedUpItems.Clear();
    this.scroll.ResetPosition();
  }

  private void ShowArtifactDetails()
  {
    ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this._artifactId);
    if (artifactInfo != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactTexture, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this.artifactName.text = artifactInfo.Name;
      this.artifactEffectDesc.text = artifactInfo.Description;
    }
    this.UpdateArtifactInfo();
    this.UpdateArtifactActiveBtnStatus(this._artifactId);
  }

  private void UpdateArtifactInfo()
  {
    NGUITools.SetActive(this.artifactCdTime.gameObject, false);
    NGUITools.SetActive(this.artifactProtectedTimeNode, false);
    ArtifactData artifactData = DBManager.inst.DB_Artifact.GetArtifactData(PlayerData.inst.userData.world_id, this._artifactId);
    if (artifactData != null)
    {
      if (artifactData.ProtectedLeftTime > 0)
      {
        this.artifactProtectedTime.text = Utils.FormatTime(artifactData.ProtectedLeftTime, true, false, true);
        NGUITools.SetActive(this.artifactProtectedTimeNode, true);
      }
      else
        this.artifactName.transform.localPosition = new Vector3(this.artifactName.transform.localPosition.x, this._artifactNamePositionY - 50f, 0.0f);
      if (!artifactData.IsCooldown)
        return;
      this.artifactCdTime.text = Utils.XLAT("id_cooldown") + Utils.FormatTime(artifactData.CooldownLeftTime, true, false, true);
      NGUITools.SetActive(this.artifactCdTime.gameObject, true);
    }
    else
      this.artifactName.transform.localPosition = new Vector3(this.artifactName.transform.localPosition.x, this._artifactNamePositionY - 50f, 0.0f);
  }

  private bool HasArtifactSpeedup(int artifact)
  {
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(PlayerData.inst.uid);
    if (heroData != null && heroData.IsArtifactEquiped(artifact))
      return PlayerData.inst.playerCityData.HasAvailableArtifact(artifact);
    return false;
  }

  private bool IsArtifactEquiped(int artifact)
  {
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(PlayerData.inst.uid);
    if (heroData != null)
      return heroData.IsArtifactEquiped(artifact);
    return false;
  }

  private void UpdateArtifactActiveBtnStatus(int curArtifact)
  {
    this.artifactUsedButton.isEnabled = this.HasArtifactSpeedup(curArtifact);
  }

  private void FirstInsterItems()
  {
    List<IconData> iconDatas = this.GetIconDatas();
    NGUITools.SetActive(this.normalatip.gameObject, false);
    if (iconDatas.Count > 0)
    {
      NGUITools.SetActive(this.scroll.gameObject, false);
      this.group.CreateIcon(iconDatas);
      List<Icon> totalIcons = this.group.TotalIcons;
      for (int index = 0; index < totalIcons.Count; ++index)
      {
        SpeedUpItem speedUpItem = totalIcons[index] as SpeedUpItem;
        IconData data = totalIcons[index].data as IconData;
        speedUpItem.Item_Id = (int) data.Data;
        speedUpItem.Selected = index == 0;
        if (index == 0)
          this.selectedItem = speedUpItem.Item_Id;
        speedUpItem.OnSelectedDelegate = new System.Action<int>(this.OnItemClickHandler);
        if (this.speedUpItems.ContainsKey(speedUpItem.Item_Id))
          this.speedUpItems.Add(speedUpItem.Item_Id, speedUpItem);
      }
      this.OnItemClickHandler(this.selectedItem);
      NGUITools.SetActive(this.scroll.gameObject, true);
      Utils.ExecuteInSecs(1f / 500f, (System.Action) (() => this.scroll.ResetPosition()));
      this.onDragFinished();
    }
    else
    {
      ScriptLocalization.Get("speedup_no_one_itemtip", true);
      NGUITools.SetActive(this.normalatip.gameObject, true);
      NGUITools.SetActive(this.countContent, false);
      NGUITools.SetActive(this.useButton.gameObject, false);
      this.RemoveEventHandler();
      Oscillator.Instance.secondEvent += new System.Action<int>(this.SecondHanlder);
    }
  }

  private void SecondHanlder(int time)
  {
    this.RefreshTimeBar();
  }

  private void OnItemClickHandler(int item_id)
  {
    List<Icon> totalIcons = this.group.TotalIcons;
    this.selectedItem = item_id;
    for (int index = 0; index < totalIcons.Count; ++index)
    {
      SpeedUpItem speedUpItem = totalIcons[index] as SpeedUpItem;
      IconData data = totalIcons[index].data as IconData;
      speedUpItem.Item_Id = (int) data.Data;
      speedUpItem.Selected = speedUpItem.Item_Id == item_id;
    }
    this.RefreshTimeBar();
    this.CalcItem(true, true);
    this.autoCalc = true;
  }

  private void OnFree()
  {
    NGUITools.SetActive(this.countContent, false);
    this.buttonLabel.text = ScriptLocalization.Get("id_upcase_free", true);
  }

  private void RefreshTimeBar()
  {
    JobHandle job = JobManager.Instance.GetJob(this.m_JobId);
    if (job != null)
    {
      this.timeProgress.value = 1f - (float) job.LeftTime() / (float) job.Duration();
      this.timeprogressLabel.text = Utils.FormatTime1(job.LeftTime());
    }
    else if (this.m_JobId == -1L)
    {
      int time = 0;
      int num = 0;
      if (this.speedupType == ItemBag.ItemType.dungeon_cd_speedup)
      {
        DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
        if (dragonKnightData != null && dragonKnightData.CurrentDungeonEndTime > NetServerTime.inst.ServerTimestamp)
        {
          time = dragonKnightData.CurrentDungeonEndTime - NetServerTime.inst.ServerTimestamp;
          num = dragonKnightData.CurrentDungeonEndTime - dragonKnightData.LastOpenDungeonTime;
        }
      }
      this.timeProgress.value = (float) (1.0 - (double) time / (double) num);
      this.timeprogressLabel.text = Utils.FormatTime1(time);
    }
    NGUITools.SetActive(this.freetimeLabel.gameObject, this.GetFreeTime() > 0);
    this.freetimeLabel.text = Utils.FormatTime1(this.GetFreeTime());
  }

  private int GetFreeTime()
  {
    if (this.CanFreeSpeedup())
      return CityManager.inst.GetFreeSpeedUpTime();
    return 0;
  }

  private int GetRemainTime()
  {
    JobHandle job = JobManager.Instance.GetJob(this.m_JobId);
    if (job != null)
    {
      if (this.CanFreeSpeedup())
        return job.LeftTime() - this.GetFreeTime();
      return job.LeftTime();
    }
    if (this.m_JobId == -1L && this.speedupType == ItemBag.ItemType.dungeon_cd_speedup)
    {
      DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
      if (dragonKnightData != null && dragonKnightData.CurrentDungeonEndTime > NetServerTime.inst.ServerTimestamp)
        return dragonKnightData.CurrentDungeonEndTime - NetServerTime.inst.ServerTimestamp;
    }
    return 0;
  }

  public void UseFree()
  {
    JobHandle job = JobManager.Instance.GetJob(this.m_JobId);
    long jobId = this.m_JobId;
    if (job != null && this.CanFreeSpeedup() && job.LeftTime() - this.GetFreeTime() <= 0)
    {
      CityTimerbar componentInChildren = this.mTarBC.GetComponentInChildren<CityTimerbar>();
      if ((UnityEngine.Object) null != (UnityEngine.Object) componentInChildren)
        componentInChildren.OnFreeBtnPressed();
    }
    this.OnCloseHandler();
  }

  private List<IconData> GetIconDatas()
  {
    List<IconData> iconDataList = new List<IconData>();
    List<ConsumableItemData> speedUpItems = ItemBag.Instance.GetSpeedUpItems();
    List<ConsumableItemData> speedUpItemsByType = ItemBag.Instance.GetSpeedUpItemsByType(this.ItemType);
    if (!this.canUseCommonSpeedup)
      speedUpItems.Clear();
    if (speedUpItemsByType != null)
      speedUpItems.AddRange((IEnumerable<ConsumableItemData>) speedUpItemsByType);
    speedUpItems.Sort(new Comparison<ConsumableItemData>(this.CompareItem));
    for (int index = 0; index < speedUpItems.Count; ++index)
      iconDataList.Add(this.GetIconData(speedUpItems[index]));
    return iconDataList;
  }

  private ItemBag.ItemType ItemType
  {
    get
    {
      ItemBag.ItemType itemType = ItemBag.ItemType.Invalid;
      JobHandle job = JobManager.Instance.GetJob(this.m_JobId);
      if (job != null)
      {
        JobEvent jobEvent = job.GetJobEvent();
        switch (jobEvent)
        {
          case JobEvent.JOB_BUILDING_CONSTRUCTION:
            itemType = ItemBag.ItemType.building_speedup;
            break;
          case JobEvent.JOB_TRAIN_TROOP:
            Hashtable data1 = job.Data as Hashtable;
            if (data1 != null)
            {
              string empty1 = string.Empty;
              string empty2 = string.Empty;
              if (data1.ContainsKey((object) "troop_class"))
                empty1 = data1[(object) "troop_class"].ToString();
              if (!string.IsNullOrEmpty(empty1))
              {
                Unit_StatisticsInfo data2 = ConfigManager.inst.DB_Unit_Statistics.GetData(empty1);
                itemType = data2 == null || !(data2.Type == "trap") ? ItemBag.ItemType.training_speedup : ItemBag.ItemType.trap_speedup;
                break;
              }
              break;
            }
            break;
          case JobEvent.JOB_RESEARCH:
            itemType = ItemBag.ItemType.research_speedup;
            break;
          default:
            if (jobEvent != JobEvent.JOB_HEALING_TROOPS)
            {
              if (jobEvent != JobEvent.JOB_FORGE && jobEvent != JobEvent.JOB_DK_FORGE)
              {
                if (jobEvent == JobEvent.JOB_UPGRADE_TROOPS)
                  goto case JobEvent.JOB_TRAIN_TROOP;
                else
                  break;
              }
              else
              {
                itemType = ItemBag.ItemType.forge_speedup;
                break;
              }
            }
            else
            {
              itemType = ItemBag.ItemType.healing_speedup;
              break;
            }
        }
      }
      else if (this.m_JobId == -1L)
        itemType = this.speedupType;
      return itemType;
    }
  }

  private IconData GetIconData(ConsumableItemData a)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(a.internalId);
    return new IconData(itemStaticInfo.ImagePath, new string[2]
    {
      itemStaticInfo.LocName,
      a.quantity.ToString()
    })
    {
      Data = (object) a.internalId
    };
  }

  private int CompareItem(ConsumableItemData a, ConsumableItemData b)
  {
    return ConfigManager.inst.DB_Items.GetItem(a.internalId).Value.CompareTo(ConfigManager.inst.DB_Items.GetItem(b.internalId).Value);
  }

  public void UseItem()
  {
    if (this.GetRemainTime() > 0)
    {
      if (this.UseCount == 0)
        return;
      Hashtable extra = new Hashtable();
      long num = 0;
      JobHandle job = JobManager.Instance.GetJob(this.m_JobId);
      if (job != null)
      {
        num = job.ServerJobID;
        if (num == 0L)
          num = this.m_JobId;
      }
      else if (this.m_JobId == -1L)
        num = this.m_JobId;
      extra[(object) "job_id"] = (object) num;
      extra[(object) "type"] = (object) this.speedupType.ToString();
      int itemCount = ItemBag.Instance.GetItemCount(this.selectedItem);
      if (itemCount >= this.UseCount)
        ItemBag.Instance.UseItem(this.selectedItem, this.UseCount, extra, (System.Action<bool, object>) ((arg1, arg2) => this.UpdateUI()));
      else
        ItemBag.Instance.UseItem(this.selectedItem, itemCount, extra, (System.Action<bool, object>) ((arg1, arg2) => this.UpdateUI()));
    }
    else
      this.OnFree();
  }

  private void OnDestroy()
  {
    this._destroy = true;
  }

  private void UpdateUI()
  {
    if (this._destroy || !this.IsCurrentJobValid())
    {
      this.OnCloseHandler();
    }
    else
    {
      this.Clear();
      this.FirstInsterItems();
    }
  }

  private bool IsCurrentJobValid()
  {
    return JobManager.Instance.GetJob(this.m_JobId) != null || this.m_JobId == -1L;
  }

  private void OnDataChangeHandler(int item_id)
  {
    if (DBManager.inst.DB_Item.Get(item_id) == null || ItemBag.Instance.GetItemCount(item_id) <= 0)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(item_id);
    if (this.speedUpItems.ContainsKey(item_id))
      this.speedUpItems[item_id].SetData(itemStaticInfo.ImagePath, itemStaticInfo.LocName, ItemBag.Instance.GetItemCount(item_id).ToString());
    this.RefreshTimeBar();
  }

  private void OnSecondeHandler(int time)
  {
    this.RefreshTimeBar();
    if (this.autoCalc)
      this.UseCount = this.GetUseCount();
    if (this.GetRemainTime() <= 0)
      this.OnCloseHandler();
    this.UpdateArtifactInfo();
  }

  public void OnTextInputChange()
  {
    if (this.selectedItem <= 0)
      return;
    int result = 1;
    if (this.inputLabel.value != string.Empty && !int.TryParse(this.inputLabel.value, out result))
      result = 1;
    this.autoCalc = false;
    this.GetRemainTime();
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.selectedItem);
    int require = this.Require;
    int maxCount = this.MaxCount;
    result = Mathf.Min(result, maxCount);
    float num = (float) result / (float) maxCount;
    if ((double) num > 1.0)
      num = 1f;
    if (this.autoFixProgressBar)
      this.countProgress.value = num;
    this.inputLabel.value = result.ToString();
    this.normalTime.text = Utils.FormatTime1((int) ((double) itemStaticInfo.Value * (double) result));
  }

  private int Require
  {
    get
    {
      int remainTime = this.GetRemainTime();
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.selectedItem);
      int num = (int) ((double) remainTime / (double) itemStaticInfo.Value);
      if (num == 0)
        return 1;
      bool flag = false;
      if ((double) remainTime % (double) itemStaticInfo.Value > 0.0 && itemStaticInfo.Param1 == "0")
        flag = true;
      if (flag)
        ++num;
      return num;
    }
  }

  public void OnProgressBarChange()
  {
    if (this.selectedItem <= 0)
      return;
    this.autoCalc = false;
    float num1 = this.countProgress.value;
    ItemBag.Instance.GetItemCount(this.selectedItem);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.selectedItem);
    this.GetRemainTime();
    int maxCount = this.MaxCount;
    int num2 = (int) Math.Round((double) num1 * (double) maxCount);
    int time = (int) ((double) itemStaticInfo.Value * (double) num2);
    if (this.autoFixProgressBar)
      this.countProgress.value = (float) num2 / (float) maxCount;
    this.normalTime.text = Utils.FormatTime1(time);
    this.inputLabel.value = num2.ToString();
    this._useCount = num2;
  }

  private int GetUseCount()
  {
    if (this.selectedItem <= 0)
      return 0;
    ConfigManager.inst.DB_Items.GetItem(this.selectedItem);
    int itemCount = ItemBag.Instance.GetItemCount(this.selectedItem);
    this.GetRemainTime();
    int require = this.Require;
    return Mathf.Min(itemCount, require);
  }

  private int MaxCount
  {
    get
    {
      this.GetRemainTime();
      ConfigManager.inst.DB_Items.GetItem(this.selectedItem);
      return Mathf.Min(this.Require, ItemBag.Instance.GetItemCount(this.selectedItem));
    }
  }

  private void CalcItem(bool needClear = false, bool playAnimation = false)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.selectedItem);
    if (this.GetRemainTime() <= 0)
    {
      this.OnFree();
    }
    else
    {
      this.buttonLabel.text = ScriptLocalization.Get("id_uppercase_use", true);
      int itemCount = ItemBag.Instance.GetItemCount(this.selectedItem);
      int maxCount = this.MaxCount;
      if (needClear)
        this._useCount = maxCount;
      int useCount = this._useCount;
      if (this._useCount > itemCount)
        this._useCount = itemCount;
      float num = (float) useCount / (float) this.MaxCount;
      if ((double) num > 1.0)
        num = 1f;
      if (this.autoFixProgressBar)
        this.countProgress.value = num;
      this.inputLabel.value = useCount.ToString();
      this.normalTime.text = Utils.FormatTime1((int) ((double) itemStaticInfo.Value * (double) useCount));
      if (this.m_JobId == -1L)
      {
        NGUITools.SetActive(this.normalTime.gameObject, itemStaticInfo.Param1 != "1");
        NGUITools.SetActive(this.timeTip.gameObject, itemStaticInfo.Param1 != "1");
      }
      if (!playAnimation && !this.autoCalc)
        return;
      this.PlayAnimation();
    }
  }

  private void PlayAnimation()
  {
    if (!this.init)
      this.init = true;
    else
      this.animation.Play();
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
    {
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondeHandler);
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.SecondHanlder);
    }
    DBManager.inst.DB_Item.onDataUpdated -= new System.Action<int>(this.OnDataChangeHandler);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnRemoveItemHandler);
    this.scroll.onDragFinished -= new UIScrollView.OnDragNotification(this.onDragFinished);
  }

  private void onDragFinished()
  {
    Vector2 currentContentSize = Utils.GetScrollViewCurrentContentSize(this.scroll);
    Bounds bounds = this.scroll.bounds;
    float num = bounds.max.x - bounds.min.x;
    NGUITools.SetActive(this.left.gameObject, false);
    NGUITools.SetActive(this.right.gameObject, false);
    float scrollViewSize = Utils.GetScrollViewSize(this.scroll);
    if ((double) num <= (double) scrollViewSize)
      return;
    if ((double) currentContentSize.x > 1.0 / 1000.0)
      NGUITools.SetActive(this.left.gameObject, true);
    if ((double) currentContentSize.y <= 1.0 / 1000.0)
      return;
    NGUITools.SetActive(this.right.gameObject, true);
  }

  private void OnRemoveItemHandler(int item_id)
  {
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    this.scroll.onDragFinished += new UIScrollView.OnDragNotification(this.onDragFinished);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondeHandler);
    DBManager.inst.DB_Item.onDataUpdated += new System.Action<int>(this.OnDataChangeHandler);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnRemoveItemHandler);
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public bool canUseSpeedupArtifact = true;
    public bool canUseCommonSpeedup = true;
    public long jobId;
    public long marchId;
    public ItemBag.ItemType speedupType;
    public TimerType timerType;
    public BuildingController tarBC;
  }
}
