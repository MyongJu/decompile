﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomSettingView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UnityEngine;

public class ChatRoomSettingView : MonoBehaviour
{
  private ChatRoomSettingView.Data data = new ChatRoomSettingView.Data();
  [SerializeField]
  private ChatRoomSettingView.Panel panel;

  public void Setup(ChatRoomData roomData)
  {
    this.data.roomData = roomData;
    this.panel.title.text = Utils.XLAT("chat_view_welcome_message_settings_title");
    this.panel.welInput.enabled = false;
    this.panel.welBoxCollider.enabled = false;
    this.panel.welInput.value = string.Empty;
    this.panel.welText.text = this.data.roomData.roomTitle;
  }

  public void Reset()
  {
    this.panel.title = this.transform.Find("Background/Title_Bar").gameObject.GetComponent<UILabel>();
    this.panel.welMsgTitle = this.transform.Find("Welcome_Message_Container/Welcome_Label").gameObject.GetComponent<UILabel>();
    this.panel.welText = this.transform.Find("Welcome_Message_Container/Top_Header/Input/Text").gameObject.GetComponent<UILabel>();
    GameObject gameObject = this.transform.Find("Welcome_Message_Container/Top_Header/Input").gameObject;
    this.panel.welInput = gameObject.GetComponent<UIInput>();
    this.panel.welBoxCollider = gameObject.GetComponent<BoxCollider>();
  }

  public class Data
  {
    public ChatRoomData roomData;
    public ChatRoomMember curMember;
  }

  [Serializable]
  public class Panel
  {
    public UILabel title;
    public UILabel welMsgTitle;
    public UILabel welText;
    public UIInput welInput;
    public BoxCollider welBoxCollider;
  }
}
