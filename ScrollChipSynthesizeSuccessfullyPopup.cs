﻿// Decompiled with JetBrains decompiler
// Type: ScrollChipSynthesizeSuccessfullyPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;

public class ScrollChipSynthesizeSuccessfullyPopup : Popup
{
  public EquipmentScrollComponent scrollComponent;
  public UIButton synthesizeBtn;
  private ConfigEquipmentScrollInfo info;
  private ConfigEquipmentScrollChipInfo chipInfo;
  private System.Action<int> OnViewScroll;
  private BagType bagType;

  private void Init()
  {
    this.scrollComponent.FeedData((IComponentData) new EquipmentScrollComponetData(this.info, this.bagType));
    this.synthesizeBtn.isEnabled = this.chipInfo.combineReqValue <= ItemBag.Instance.GetItemCount(this.chipInfo.itemID);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.info = (orgParam as ScrollChipSynthesizeSuccessfullyPopup.Parameter).scrollInfo;
    this.chipInfo = (orgParam as ScrollChipSynthesizeSuccessfullyPopup.Parameter).scrollChipInfo;
    this.OnViewScroll = (orgParam as ScrollChipSynthesizeSuccessfullyPopup.Parameter).OnViewScroll;
    this.bagType = (orgParam as ScrollChipSynthesizeSuccessfullyPopup.Parameter).bagType;
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnViewBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    if (this.OnViewScroll == null)
      return;
    this.OnViewScroll(this.info.internalId);
  }

  public void OnSynthesizeClick()
  {
    EquipmentManager.Instance.CombinScroll(this.bagType, this.chipInfo.internalId, 1, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
      ConfigEquipmentScrollInfo data1 = ConfigManager.inst.GetEquipmentScroll(this.bagType).GetData(int.Parse((data as Hashtable)[(object) "item_property_id"].ToString()));
      UIManager.inst.OpenPopup("Equipment/SynthesizeSuccessfullyPopup", (Popup.PopupParameter) new ScrollChipSynthesizeSuccessfullyPopup.Parameter()
      {
        bagType = this.bagType,
        scrollInfo = data1,
        scrollChipInfo = this.chipInfo,
        OnViewScroll = this.OnViewScroll
      });
    }));
  }

  public class Parameter : Popup.PopupParameter
  {
    public ConfigEquipmentScrollInfo scrollInfo;
    public ConfigEquipmentScrollChipInfo scrollChipInfo;
    public System.Action<int> OnViewScroll;
    public BagType bagType;
  }
}
