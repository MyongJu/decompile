﻿// Decompiled with JetBrains decompiler
// Type: AssetScannerConfig
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public static class AssetScannerConfig
{
  public const string DbName = "assets_log.db";
  public const string AssetsTableName = "assets_log";
  public const string AssetsTableKey = "guid";
  public const string AssetScanCondition = "ENABLE_ASSET_SCANNER";
}
