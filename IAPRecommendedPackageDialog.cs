﻿// Decompiled with JetBrains decompiler
// Type: IAPRecommendedPackageDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class IAPRecommendedPackageDialog : UI.Dialog
{
  private List<IAPStoreContentRenderer> m_ItemList = new List<IAPStoreContentRenderer>();
  public GameObject m_ItemPrefab;
  public IAPStorePackagePanel m_Panel;
  public UIGrid m_Grid;
  public UIScrollView m_ScrollView;
  public UILabel title;
  public GameObject m_morebtn;
  private IAPStorePackage m_StorePackage;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_StorePackage = (orgParam as IAPRecommendedPackageDialog.Parameter).package;
    this.UpdateUI();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (this.m_StorePackage == null)
      this.m_StorePackage = (orgParam as IAPRecommendedPackageDialog.Parameter).package;
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.ClearData();
  }

  public void OnCheckOtherPackage()
  {
    UIManager.inst.OpenDlg(UIManager.DialogType.IAPStoreDialog, (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnBuy()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    IAPStorePackagePayload.Instance.BuyProduct(this.m_StorePackage.package.productId, this.m_StorePackage.id, this.m_StorePackage.group_id, (System.Action) (() => Utils.ShowRecommendIAPPackagePopupByPolicy()));
  }

  private void UpdateUI()
  {
    this.m_Panel.SetData(this.m_StorePackage);
    IAPPackageInfo package = this.m_StorePackage.package;
    IAPRewardInfo rewardInfo1 = ConfigManager.inst.DB_IAPRewards.Get(package.reward_group_id);
    IAPRewardInfo rewardInfo2 = ConfigManager.inst.DB_IAPRewards.Get(package.alliance_reward_group);
    this.ClearData();
    this.UpdateData(rewardInfo1);
    this.UpdateData(rewardInfo2);
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
    this.title.text = ScriptLocalization.Get(this.m_StorePackage.group.name, true);
    if ("1" == IAPStorePackagePayload.Instance.GetShowBtnState())
      this.m_morebtn.SetActive(true);
    else
      this.m_morebtn.SetActive(false);
  }

  private void UpdateData(IAPRewardInfo rewardInfo)
  {
    if (rewardInfo == null || rewardInfo.Rewards == null)
      return;
    List<Reward.RewardsValuePair> rewards = rewardInfo.GetRewards();
    rewards.Sort(new Comparison<Reward.RewardsValuePair>(IAPRecommendedPackageDialog.Compare));
    for (int index = 0; index < rewards.Count; ++index)
    {
      Reward.RewardsValuePair rewardsValuePair = rewards[index];
      int internalId = rewardsValuePair.internalID;
      int itemCount = rewardsValuePair.value;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_Grid.transform;
      gameObject.transform.localScale = Vector3.one;
      IAPStoreContentRenderer component = gameObject.GetComponent<IAPStoreContentRenderer>();
      component.SetData(itemStaticInfo, itemCount);
      this.m_ItemList.Add(component);
    }
  }

  private void ClearData()
  {
    using (List<IAPStoreContentRenderer>.Enumerator enumerator = this.m_ItemList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IAPStoreContentRenderer current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ItemList.Clear();
  }

  private static int Compare(Reward.RewardsValuePair x, Reward.RewardsValuePair y)
  {
    return ConfigManager.inst.DB_Items.GetItem(x.internalID).Priority - ConfigManager.inst.DB_Items.GetItem(y.internalID).Priority;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public IAPStorePackage package;
  }
}
