﻿// Decompiled with JetBrains decompiler
// Type: ConfigWonderPackage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigWonderPackage
{
  private List<WonderPackageInfo> wonderPackageList = new List<WonderPackageInfo>();
  private Dictionary<string, WonderPackageInfo> datas;
  private Dictionary<int, WonderPackageInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<WonderPackageInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    Dictionary<string, WonderPackageInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this.wonderPackageList.Add(enumerator.Current);
    }
    this.wonderPackageList.Sort(new Comparison<WonderPackageInfo>(this.SortBySortOrder));
  }

  public List<WonderPackageInfo> GetAllWonderPackageInfo()
  {
    return this.wonderPackageList;
  }

  public WonderPackageInfo Get(int interalId)
  {
    if (this.dicByUniqueId.ContainsKey(interalId))
      return this.dicByUniqueId[interalId];
    return (WonderPackageInfo) null;
  }

  public WonderPackageInfo Get(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (WonderPackageInfo) null;
  }

  public int SortBySortOrder(WonderPackageInfo a, WonderPackageInfo b)
  {
    return a.sort.CompareTo(b.sort);
  }

  public void Clear()
  {
    if (this.datas != null)
      this.datas.Clear();
    if (this.dicByUniqueId != null)
      this.dicByUniqueId.Clear();
    if (this.wonderPackageList == null)
      return;
    this.wonderPackageList.Clear();
  }
}
