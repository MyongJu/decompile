﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigEquipmentMain
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, ConfigEquipmentMainInfo> datas;
  private Dictionary<int, ConfigEquipmentMainInfo> dicByUniqueId;
  private Dictionary<int, ConfigEquipmentMainInfo> dicByLevel;
  private Dictionary<int, ConfigEquipmentMainInfo> dicByItemIds;
  private Dictionary<int, List<ConfigEquipmentMainInfo>> allEquipmentSuit;

  public void BuildDB(object res)
  {
    this.parse.Parse<ConfigEquipmentMainInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.allEquipmentSuit = new Dictionary<int, List<ConfigEquipmentMainInfo>>();
    this.dicByItemIds = new Dictionary<int, ConfigEquipmentMainInfo>();
    using (Dictionary<string, ConfigEquipmentMainInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ConfigEquipmentMainInfo current = enumerator.Current;
        if (!this.dicByItemIds.ContainsKey(current.itemID))
          this.dicByItemIds.Add(current.itemID, current);
        if (current.suitGroup != 0)
        {
          if (!this.allEquipmentSuit.ContainsKey(current.suitGroup))
            this.allEquipmentSuit.Add(current.suitGroup, new List<ConfigEquipmentMainInfo>());
          this.allEquipmentSuit[current.suitGroup].Add(current);
        }
      }
    }
    using (Dictionary<int, List<ConfigEquipmentMainInfo>>.Enumerator enumerator = this.allEquipmentSuit.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.Sort((Comparison<ConfigEquipmentMainInfo>) ((x, y) => x.type.CompareTo(y.type)));
    }
  }

  public ConfigEquipmentMainInfo GetData(int internalID)
  {
    if (this.dicByUniqueId.ContainsKey(internalID))
      return this.dicByUniqueId[internalID];
    return (ConfigEquipmentMainInfo) null;
  }

  public ConfigEquipmentMainInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (ConfigEquipmentMainInfo) null;
  }

  public ConfigEquipmentMainInfo GetDataByItemInternalId(int itemId)
  {
    if (this.dicByItemIds.ContainsKey(itemId))
      return this.dicByItemIds[itemId];
    return (ConfigEquipmentMainInfo) null;
  }

  public List<ConfigEquipmentMainInfo> GetAllInEquipmentSuit(int equipmentSuitGroupId)
  {
    if (this.allEquipmentSuit != null && this.allEquipmentSuit.ContainsKey(equipmentSuitGroupId))
      return this.allEquipmentSuit[equipmentSuitGroupId];
    return (List<ConfigEquipmentMainInfo>) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, ConfigEquipmentMainInfo>) null;
    }
    if (this.dicByUniqueId != null)
    {
      this.dicByUniqueId.Clear();
      this.dicByUniqueId = (Dictionary<int, ConfigEquipmentMainInfo>) null;
    }
    if (this.allEquipmentSuit == null)
      return;
    this.allEquipmentSuit.Clear();
    this.allEquipmentSuit = (Dictionary<int, List<ConfigEquipmentMainInfo>>) null;
  }
}
