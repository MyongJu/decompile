﻿// Decompiled with JetBrains decompiler
// Type: DKArenaPlayerItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;
using UnityEngine;

public class DKArenaPlayerItem : MonoBehaviour
{
  public UILabel rank;
  public UITexture icon;
  public UITexture title;
  public UILabel userName;
  public UILabel score;
  private long _uid;
  private long _allianceId;
  private long _score;

  public void SetData(DKArenaRankPlayerData data)
  {
    this.rank.text = data.rank.ToString();
    this._uid = data.uid;
    CustomIconLoader.Instance.requestCustomIcon(this.icon, UserData.GetPortraitIconPath(data.portrait), data.icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.icon, data.lordTitleId, 2);
    this.userName.text = data.UserName;
    this.score.text = data.score.ToString();
    this._score = data.score;
    BuilderFactory.Instance.Build((UIWidget) this.title, ConfigManager.inst.DB_DKArenaTitleReward.GetTitleImage(data.score), (System.Action<bool>) null, true, false, true, string.Empty);
  }

  public void OnClickHandler()
  {
    RequestManager.inst.SendRequest("dragonKnight:getDkCompareInfo", Utils.Hash((object) "opp_uid", (object) this._uid), new System.Action<bool, object>(this.OnGetUserDataCallBack), true);
  }

  private void OnGetUserDataCallBack(bool success, object orgData)
  {
    if (!success)
      return;
    UserDKArenaData currentData = DBManager.inst.DB_DKArenaDB.GetCurrentData();
    DragonKnightPVPComparisonPopup.Parameter parameter = new DragonKnightPVPComparisonPopup.Parameter();
    parameter.source = PlayerData.inst.userData;
    parameter.sourceAlliance = PlayerData.inst.allianceData;
    parameter.sourceDK = PlayerData.inst.dragonKnightData;
    parameter.sourceEquipments = DBManager.inst.GetInventory(BagType.DragonKnight).GetEquippedItemsByID(parameter.source.uid);
    parameter.sourceTalent1 = DBManager.inst.DB_DragonKnightTalent.GetTalentTreeTalentCount(1);
    parameter.sourceTalent2 = DBManager.inst.DB_DragonKnightTalent.GetTalentTreeTalentCount(2);
    parameter.sourceTalent3 = DBManager.inst.DB_DragonKnightTalent.GetTalentTreeTalentCount(3);
    parameter.sourceScore = currentData.score;
    parameter.target = DBManager.inst.DB_User.Get(this._uid);
    parameter.targetAlliance = DBManager.inst.DB_Alliance.Get(parameter.target.allianceId);
    parameter.targetDK = DBManager.inst.DB_DragonKnight.Get(this._uid);
    parameter.targetEquipments = DBManager.inst.GetInventory(BagType.DragonKnight).GetEquippedItemsByID(parameter.target.uid);
    parameter.targetScore = this._score;
    parameter.callback = (System.Action) (() =>
    {
      DBManager.inst.GetInventory(BagType.DragonKnight).ClearThirdPartyInventory();
      DBManager.inst.DB_Gems.ClearThirdPartyInventory();
    });
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable != null && hashtable.ContainsKey((object) "talent"))
    {
      Hashtable inData = hashtable[(object) "talent"] as Hashtable;
      if (inData != null)
      {
        DatabaseTools.UpdateData(inData, "1", ref parameter.targetTalent1);
        DatabaseTools.UpdateData(inData, "2", ref parameter.targetTalent2);
        DatabaseTools.UpdateData(inData, "3", ref parameter.targetTalent3);
      }
    }
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightPVPComparisonPopup", (Popup.PopupParameter) parameter);
  }

  public void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.title);
  }
}
