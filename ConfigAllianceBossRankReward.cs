﻿// Decompiled with JetBrains decompiler
// Type: ConfigAllianceBossRankReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigAllianceBossRankReward
{
  private List<AllianceBossRankRewardInfo> allianceBossRankRewardInfoList = new List<AllianceBossRankRewardInfo>();
  private Dictionary<string, AllianceBossRankRewardInfo> datas;
  private Dictionary<int, AllianceBossRankRewardInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<AllianceBossRankRewardInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    Dictionary<string, AllianceBossRankRewardInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this.allianceBossRankRewardInfoList.Add(enumerator.Current);
    }
    this.allianceBossRankRewardInfoList.Sort(new Comparison<AllianceBossRankRewardInfo>(this.SortByID));
  }

  public List<AllianceBossRankRewardInfo> GetAllianceBossRankRewardInfoList()
  {
    return this.allianceBossRankRewardInfoList;
  }

  public AllianceBossRankRewardInfo Get(int interalId)
  {
    if (this.dicByUniqueId.ContainsKey(interalId))
      return this.dicByUniqueId[interalId];
    return (AllianceBossRankRewardInfo) null;
  }

  public AllianceBossRankRewardInfo Get(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AllianceBossRankRewardInfo) null;
  }

  public AllianceBossRankRewardInfo GetAllianceBossRewardByRank(int rank)
  {
    using (Dictionary<string, AllianceBossRankRewardInfo>.Enumerator enumerator = this.datas.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, AllianceBossRankRewardInfo> current = enumerator.Current;
        if (rank == current.Value.rank)
          return current.Value;
      }
    }
    return (AllianceBossRankRewardInfo) null;
  }

  public int SortByID(AllianceBossRankRewardInfo a, AllianceBossRankRewardInfo b)
  {
    return int.Parse(a.id).CompareTo(int.Parse(b.id));
  }
}
