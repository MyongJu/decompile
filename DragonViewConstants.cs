﻿// Decompiled with JetBrains decompiler
// Type: DragonViewConstants
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DragonViewConstants
{
  public const int SourceEventId = 1;
  public const int TargetEventId = 2;

  public class Animation
  {
    public const string Idle = "idle";
    public const string Fly = "fly";
    public const string Glide = "fly_glide";
    public const string Stretch = "stretch";
    public const string Attack = "attack_common";
    public const string Special0 = "special_00";
    public const string Special1 = "special_01";
  }

  public class AnimationParameter
  {
    public const string EnterCityTrigger = "EnterCity";
    public const string EnterKingdomTrigger = "EnterKingdom";
    public const string EnterUITrigger = "EnterUI";
    public const string ResetTrigger = "Reset";
  }

  public class DummyPoint
  {
    public const string Head = "heat_locator";
    public const string Tail = "tail_locator";
  }
}
