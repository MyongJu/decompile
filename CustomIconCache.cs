﻿// Decompiled with JetBrains decompiler
// Type: CustomIconCache
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.IO;
using UnityEngine;

public class CustomIconCache
{
  protected static string m_cacheRoot = Path.Combine(Application.persistentDataPath, "IconCache");
  private TextureFormat m_textureFormat = TextureFormat.RGB24;
  protected const int MAX_CACHE_COUNT = 600;
  private bool m_textureFormatInited;

  protected string getIconPath(string icon)
  {
    return Path.Combine(CustomIconCache.m_cacheRoot, icon);
  }

  public static void TryClearCache()
  {
    if (!Directory.Exists(CustomIconCache.m_cacheRoot))
      Directory.CreateDirectory(CustomIconCache.m_cacheRoot);
    string[] files = Directory.GetFiles(CustomIconCache.m_cacheRoot);
    if (files.Length <= 600)
      return;
    foreach (string path in files)
    {
      Debug.Log((object) ("delete + " + path));
      File.Delete(path);
    }
  }

  public TextureFormat TextureFormat
  {
    get
    {
      if (this.m_textureFormatInited)
        return this.m_textureFormat;
      if (SystemInfo.SupportsTextureFormat(TextureFormat.RGB24))
        this.m_textureFormat = TextureFormat.RGB24;
      else if (SystemInfo.SupportsTextureFormat(TextureFormat.RGBA32))
        this.m_textureFormat = TextureFormat.RGBA32;
      else if (SystemInfo.SupportsTextureFormat(TextureFormat.ARGB32))
        this.m_textureFormat = TextureFormat.ARGB32;
      this.m_textureFormatInited = true;
      return this.m_textureFormat;
    }
  }

  public void doCache(string icon, byte[] allBytes)
  {
    try
    {
      if (!Directory.Exists(CustomIconCache.m_cacheRoot))
        Directory.CreateDirectory(CustomIconCache.m_cacheRoot);
      File.WriteAllBytes(this.getIconPath(icon), allBytes);
    }
    catch (IOException ex)
    {
      D.error((object) string.Format("cache icon failed: {0}", !string.IsNullOrEmpty(ex.Message) ? (object) ex.Message : (object) "unkonw error"));
    }
  }

  public bool isIconCached(string icon)
  {
    if (string.IsNullOrEmpty(icon))
      return false;
    return File.Exists(this.getIconPath(icon));
  }

  public Texture tryGet(string icon)
  {
    string iconPath = this.getIconPath(icon);
    if (!File.Exists(iconPath))
      return (Texture) null;
    try
    {
      byte[] data = File.ReadAllBytes(iconPath);
      Texture2D texture2D = new Texture2D(2, 2, this.TextureFormat, false);
      texture2D.LoadImage(data);
      return (Texture) texture2D;
    }
    catch (IOException ex)
    {
      D.error((object) string.Format("load cached icon failed: {0}", !string.IsNullOrEmpty(ex.Message) ? (object) ex.Message : (object) "unkonw error"));
    }
    return (Texture) null;
  }
}
