﻿// Decompiled with JetBrains decompiler
// Type: SellItemBasePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class SellItemBasePopup : Popup
{
  public StandardProgressBar progress;
  public UILabel ownLabel;
  public UITexture image;
  public UILabel itemNameLabel;
  public UIButton sellBtn;
  public ItemStaticInfo itemInfo;
  public BagType bagType;

  private void Init()
  {
    int itemCount = ItemBag.Instance.GetItemCount(this.itemInfo.internalId);
    this.ownLabel.text = "/" + itemCount.ToString();
    this.progress.Init(1, itemCount);
    BuilderFactory.Instance.Build((UIWidget) this.image, this.itemInfo.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    Utils.SetItemName(this.itemNameLabel, this.itemInfo.internalId);
    this.progress.onValueChanged += new System.Action<int>(this.OnProgressValueChanged);
    this.UpdateUI();
    this.sellBtn.isEnabled = this.progress.CurrentCount > 0;
  }

  public virtual void UpdateUI()
  {
  }

  public virtual void OnProgressValueChanged(int count)
  {
    this.sellBtn.isEnabled = this.progress.CurrentCount > 0;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.itemInfo = (orgParam as SellItemBasePopup.Parameter).itemInfo;
    this.bagType = (orgParam as SellItemBasePopup.Parameter).bagType;
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    BuilderFactory.Instance.Release((UIWidget) this.image);
    base.OnHide(orgParam);
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public ItemStaticInfo itemInfo;
    public BagType bagType;
  }
}
