﻿// Decompiled with JetBrains decompiler
// Type: AllianceStoreFundPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceStoreFundPanel : MonoBehaviour
{
  private Dictionary<int, AllianceStoreFundItem> m_ItemDict = new Dictionary<int, AllianceStoreFundItem>();
  public UILabel m_Fund;
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  public GameObject m_ItemPrefab;
  public UITexture m_FundIcon;

  public void Show()
  {
    this.gameObject.SetActive(true);
    this.UpdateData();
    this.UpdateFund();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_FundIcon, "Texture/Alliance/alliance_fund", (System.Action<bool>) null, true, false, string.Empty);
    DBManager.inst.DB_Alliance.onDataChanged += new System.Action<long>(this.OnAllianceDataChanged);
    DBManager.inst.DB_AllianceStore.onDataChanged += new System.Action<AllianceStoreDataKey>(this.OnAllianceStoreChanged);
  }

  private void OnAllianceStoreChanged(AllianceStoreDataKey dataKey)
  {
    Dictionary<int, AllianceStoreFundItem>.Enumerator enumerator = this.m_ItemDict.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Value.UpdateUI();
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
    this.ClearData();
    DBManager.inst.DB_Alliance.onDataChanged -= new System.Action<long>(this.OnAllianceDataChanged);
    DBManager.inst.DB_AllianceStore.onDataChanged -= new System.Action<AllianceStoreDataKey>(this.OnAllianceStoreChanged);
  }

  public void OnShowRecord()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "type"] = (object) 1;
    MessageHub.inst.GetPortByAction("allianceStore:getStoreHistory").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenPopup("Alliance/AllianceStoreRecordPopup", (Popup.PopupParameter) new AllianceStoreHistoryPopup.Parameter()
      {
        payload = data,
        type = AllianceStoreType.Fund
      });
    }), true);
  }

  private void UpdateData()
  {
    this.ClearData();
    int allianceShopInfoCount = ConfigManager.inst.DB_AllianceShop.GetAllianceShopInfoCount();
    for (int index = 0; index < allianceShopInfoCount; ++index)
    {
      AllianceShopInfo allianceShopInfoByIndex = ConfigManager.inst.DB_AllianceShop.GetAllianceShopInfoByIndex(index);
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_Grid.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localRotation = Quaternion.identity;
      gameObject.transform.localScale = Vector3.one;
      AllianceStoreFundItem component = gameObject.GetComponent<AllianceStoreFundItem>();
      component.SetData(allianceShopInfoByIndex);
      this.m_ItemDict.Add(allianceShopInfoByIndex.internalId, component);
    }
    this.m_Grid.sorting = UIGrid.Sorting.Custom;
    this.m_Grid.onCustomSort = new Comparison<Transform>(AllianceStoreFundPanel.Compare);
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  private static int Compare(Transform x, Transform y)
  {
    return x.GetComponent<AllianceStoreFundItem>().ShopInfo.AllianceLevel - y.GetComponent<AllianceStoreFundItem>().ShopInfo.AllianceLevel;
  }

  private void ClearData()
  {
    Dictionary<int, AllianceStoreFundItem>.Enumerator enumerator = this.m_ItemDict.GetEnumerator();
    while (enumerator.MoveNext())
    {
      AllianceStoreFundItem allianceStoreFundItem = enumerator.Current.Value;
      allianceStoreFundItem.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) allianceStoreFundItem.gameObject);
    }
    this.m_ItemDict.Clear();
  }

  private void OnAllianceDataChanged(long allianceId)
  {
    if (allianceId != PlayerData.inst.allianceId || PlayerData.inst.allianceData == null)
      return;
    this.UpdateFund();
    Dictionary<int, AllianceStoreFundItem>.Enumerator enumerator = this.m_ItemDict.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Value.UpdateUI();
  }

  private void UpdateFund()
  {
    if (PlayerData.inst == null || PlayerData.inst.allianceData == null)
      return;
    this.m_Fund.text = Utils.FormatThousands(PlayerData.inst.allianceData.allianceFund.ToString());
  }
}
