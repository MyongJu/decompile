﻿// Decompiled with JetBrains decompiler
// Type: MerlinKingdomHud
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinKingdomHud : Hud
{
  private List<MerlinTileItem> _allTileItem = new List<MerlinTileItem>();
  private List<Vector3> _allTilePosition = new List<Vector3>();
  private const int MAX_TILE_PER_PAGE = 9;
  private const int START_PAGE_INDEX = 1;
  [SerializeField]
  private MerlinTowerSystem _merlinTowerSystem;
  [SerializeField]
  private MerlinMarchSlotItem _marchSlotItem;
  [SerializeField]
  private UIInput _inputPageIndex;
  [SerializeField]
  private UITexture _textureBackground;
  [SerializeField]
  private UILabel _labelMineAreaName;
  [SerializeField]
  private UILabel _labelLeftTime;
  [SerializeField]
  private GameObject _rootTroopCd;
  [SerializeField]
  private UILabel _troopCdTime;
  [SerializeField]
  private UILabel _labelCurrencyTime;
  [SerializeField]
  private GameObject _tileContainer;
  [SerializeField]
  private MerlinTileItem _tileItemTemplate;
  [SerializeField]
  private UIButton _buttonMovePrevious;
  [SerializeField]
  private UIButton _buttonMoveNext;
  [SerializeField]
  private GameObject _battleRedPoint;
  [SerializeField]
  private Color _colorSoftCd;
  [SerializeField]
  private Color _colorHardCd;
  [SerializeField]
  private UILabel _labelQuickOccupyCd;
  [SerializeField]
  private GameObject _redTip;
  private bool _requesting;
  private int _currentPage;
  private bool _needShowBattleResult;
  private bool _tilesDirty;
  private bool _uiDirty;

  private MerlinKingdomData KingdomData
  {
    get
    {
      return MerlinTowerPayload.Instance.KingdomData;
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    Logger.Log("MerlinKingdomHud::OnShow");
    base.OnShow(orgParam);
    this._needShowBattleResult = false;
    this._allTilePosition.Clear();
    Transform parent = this._tileItemTemplate.transform.parent;
    int childCount = parent.GetChildCount();
    for (int index = 0; index < childCount; ++index)
      this._allTilePosition.Add(parent.GetChild(index).localPosition);
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemDataChanged);
    MessageHub.inst.GetPortByAction("magicTower:enterMineField").AddEvent(new System.Action<object>(this.OnEnterMineFieldResponse));
    MessageHub.inst.GetPortByAction("magicTower:gather").AddEvent(new System.Action<object>(this.OnGatherResponse));
    MessageHub.inst.GetPortByAction("magicTower:pvpBattle").AddEvent(new System.Action<object>(this.OnPvPBattleResponse));
    MessageHub.inst.GetPortByAction("magicTower:findFreeMine").AddEvent(new System.Action<object>(this.OnEnterMineFieldResponse));
    MessageHub.inst.GetPortByAction("magicTower:revenge").AddEvent(new System.Action<object>(this.OnPvPBattleResponse));
    MessageHub.inst.GetPortByAction("magic_tower_gather_done").AddEvent(new System.Action<object>(this.OnJobComplete));
    MessageHub.inst.GetPortByAction("magic_tower_over").AddEvent(new System.Action<object>(this.OnMerlinTowerOver));
    MessageHub.inst.GetPortByAction("magic_tower_combat_failure").AddEvent(new System.Action<object>(this.OnAttacked));
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondTick);
    this._tileItemTemplate.gameObject.SetActive(false);
    this._marchSlotItem.SetClickCallback(new System.Action<MerlinMarchSlotItem>(this.OnMarchSlotItemClicked));
    this.CreateTiles();
    this.UpdateUI();
    if (MerlinTowerFacade.RequestEnterMineIndex != 0)
    {
      this.RequestEnterMineField((MerlinTowerFacade.RequestEnterMineIndex - 1) / 9 + 1);
      MerlinTowerFacade.RequestEnterMineIndex = 0;
    }
    else if (MerlinTowerPayload.Instance.UserData.mineJobId != 0L)
      this.RequestEnterMineField(0);
    else
      this.RequestEnterMineField(1);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    Logger.Log("MerlinKingdomHud::OnHide");
    base.OnHide(orgParam);
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataChanged);
    MessageHub.inst.GetPortByAction("magicTower:enterMineField").RemoveEvent(new System.Action<object>(this.OnEnterMineFieldResponse));
    MessageHub.inst.GetPortByAction("magicTower:gather").RemoveEvent(new System.Action<object>(this.OnGatherResponse));
    MessageHub.inst.GetPortByAction("magicTower:pvpBattle").RemoveEvent(new System.Action<object>(this.OnPvPBattleResponse));
    MessageHub.inst.GetPortByAction("magicTower:findFreeMine").RemoveEvent(new System.Action<object>(this.OnEnterMineFieldResponse));
    MessageHub.inst.GetPortByAction("magicTower:revenge").RemoveEvent(new System.Action<object>(this.OnPvPBattleResponse));
    MessageHub.inst.GetPortByAction("magic_tower_gather_done").RemoveEvent(new System.Action<object>(this.OnJobComplete));
    MessageHub.inst.GetPortByAction("magic_tower_over").RemoveEvent(new System.Action<object>(this.OnMerlinTowerOver));
    MessageHub.inst.GetPortByAction("magic_tower_combat_failure").RemoveEvent(new System.Action<object>(this.OnAttacked));
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondTick);
    this.DestoryAllTiles();
  }

  protected void OnItemDataChanged(int id)
  {
    this.UpdateCurrencyCount();
  }

  protected void OnJobComplete(object data)
  {
    this._uiDirty = true;
  }

  protected void OnMerlinTowerOver(object data)
  {
    Logger.Log("MerlinKingdomHud::OnMerlinTowerOver");
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = ScriptLocalization.Get("tower_refresh_button", true),
      Content = ScriptLocalization.Get("tower_crystal_mines_time_limit_description", true),
      Okay = ScriptLocalization.Get("id_uppercase_okay", true),
      OnCloseCallback = (System.Action) (() => MerlinTowerFacade.RequestExit())
    });
  }

  protected void OnAttacked(object data)
  {
    Logger.Log("MerlinKingdomHud::OnAttacked");
    this._uiDirty = true;
  }

  protected void OnMarchSlotItemClicked(MerlinMarchSlotItem slotItem)
  {
    if (MerlinTowerPayload.Instance.UserData.CurrentPage == this._currentPage)
      return;
    this.RequestEnterMineField(0);
  }

  protected void OnSecondTick(int delta)
  {
    this.UpdateTimes();
    if (MerlinTowerPayload.Instance.UserData.CurrentTowerMainInfo == null)
      return;
    int level = MerlinTowerPayload.Instance.UserData.CurrentTowerMainInfo.Level;
    if (MerlinTowerPayload.Instance.UserData.HasMonster && level > 1)
      this.SetRedTipDisplay(level - 1);
    else
      this.SetRedTipDisplay(level);
  }

  private void SetRedTipDisplay(int level)
  {
    if (MerlinTowerPayload.Instance.CanShowTipInCurrentLevel(level))
      this._redTip.SetActive(true);
    else
      this._redTip.SetActive(false);
  }

  protected void RequestEnterMineField(int pageIndex)
  {
    MerlinTowerPayload.Instance.SendEnterMineFieldRequest(pageIndex, -1);
    this._buttonMovePrevious.isEnabled = false;
    this._buttonMoveNext.isEnabled = false;
    this._inputPageIndex.enabled = false;
    this._requesting = true;
  }

  protected void OnEnterMineFieldResponse(object data)
  {
    if (data != null)
      this._tilesDirty = true;
    this._requesting = false;
  }

  protected void OnGatherResponse(object data)
  {
    Hashtable hashtable = data as Hashtable;
    if (hashtable != null && !hashtable.ContainsKey((object) "errno"))
    {
      this._uiDirty = true;
      this._tilesDirty = true;
    }
    else
      this.RequestEnterMineField(this._currentPage);
  }

  protected void OnPvPBattleResponse(object data)
  {
    Hashtable hashtable = data as Hashtable;
    if (hashtable != null && !hashtable.ContainsKey((object) "errno"))
    {
      this._uiDirty = true;
      this._tilesDirty = true;
      this._needShowBattleResult = true;
    }
    else
      this.RequestEnterMineField(this._currentPage);
  }

  protected void Update()
  {
    if (this._tilesDirty)
    {
      this._tilesDirty = false;
      this.UpdateTiles();
    }
    if (this._needShowBattleResult)
    {
      this._needShowBattleResult = false;
      this.ShowBattleResult();
    }
    if (!this._uiDirty)
      return;
    this._uiDirty = false;
    this.UpdateUI();
  }

  protected void ShowBattleResult()
  {
    Hashtable pvpBattleResult = MerlinTowerPayload.Instance.PvpBattleResult;
    if (pvpBattleResult == null)
      return;
    bool result = false;
    if (pvpBattleResult.ContainsKey((object) "win") && pvpBattleResult[(object) "win"] != null)
      bool.TryParse(pvpBattleResult[(object) "win"].ToString(), out result);
    if (!pvpBattleResult.ContainsKey((object) "detail") || pvpBattleResult[(object) "detail"] == null)
      return;
    Hashtable hashtable = pvpBattleResult[(object) "detail"] as Hashtable;
    if (hashtable == null)
      return;
    UIManager.inst.OpenPopup("MerlinTower/MerlinTowerWarReportPopup", (Popup.PopupParameter) new MerlinTowerWarReportPopup.Parameter()
    {
      winWar = result,
      mailData = hashtable,
      isFightMonster = false
    });
  }

  protected void DestoryAllTiles()
  {
    using (List<MerlinTileItem>.Enumerator enumerator = this._allTileItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MerlinTileItem current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this._allTileItem.Clear();
  }

  protected void CreateTiles()
  {
    this.DestoryAllTiles();
    for (int index = 0; index < 9; ++index)
    {
      GameObject go = Utils.DuplicateGOB(this._tileItemTemplate.gameObject, this._tileContainer.transform);
      NGUITools.SetActive(go, true);
      this._allTileItem.Add(go.GetComponent<MerlinTileItem>());
    }
  }

  protected void UpdateUI()
  {
    MagicTowerMainInfo currentTowerMainInfo = MerlinTowerPayload.Instance.UserData.CurrentTowerMainInfo;
    if (currentTowerMainInfo != null)
    {
      Dictionary<string, string> para = new Dictionary<string, string>()
      {
        {
          "0",
          currentTowerMainInfo.MineLevel.ToString()
        },
        {
          "1",
          Utils.FormatShortThousands(currentTowerMainInfo.Speed * 3600)
        }
      };
      string empty = string.Empty;
      this._labelMineAreaName.text = currentTowerMainInfo.NoviceProtection <= 0 ? ScriptLocalization.GetWithPara("tower_mine_level_title", para, true) : ScriptLocalization.GetWithPara("tower_mine_level_guarded_description", para, true);
    }
    this._marchSlotItem.UpdateUI();
    if ((UnityEngine.Object) this._battleRedPoint != (UnityEngine.Object) null)
      this._battleRedPoint.SetActive(MerlinTowerPayload.Instance.UserData.showRedPoint > 0);
    this.UpdateCurrencyCount();
    this.UpdateTiles();
    this.UpdateTimes();
  }

  protected void UpdateCurrencyCount()
  {
    this._labelCurrencyTime.text = Utils.FormatThousands(ItemBag.Instance.GetItemCount("item_magic_tower_coin_1").ToString());
  }

  protected void UpdateTimes()
  {
    MagicTowerMainInfo currentTowerMainInfo = MerlinTowerPayload.Instance.UserData.CurrentTowerMainInfo;
    UserMerlinTowerData userData = MerlinTowerPayload.Instance.UserData;
    MerlinKingdomData kingdomData = MerlinTowerPayload.Instance.KingdomData;
    if (userData == null || kingdomData == null || currentTowerMainInfo == null)
      return;
    this._labelLeftTime.text = ScriptLocalization.Get("tower_mining_time_status", true) + Utils.FormatTime(kingdomData.LeftTime, true, true, true);
    if (userData.IsInCd)
    {
      this._rootTroopCd.SetActive(true);
      this._troopCdTime.color = this._colorHardCd;
      this._troopCdTime.text = ScriptLocalization.Get("tower_troops_rest_time_status", true) + Utils.FormatTime(userData.LeftCdTime, true, true, true);
    }
    else if (userData.BattleCd > 0)
    {
      this._rootTroopCd.SetActive(true);
      this._troopCdTime.color = this._colorSoftCd;
      this._troopCdTime.text = ScriptLocalization.Get("tower_troops_rest_time_status", true) + Utils.FormatTime(userData.BattleCd, true, true, true);
    }
    else
      this._rootTroopCd.SetActive(false);
    if (userData.QuickSearchCd > 0)
    {
      this._labelQuickOccupyCd.gameObject.SetActive(true);
      this._labelQuickOccupyCd.text = Utils.FormatTime(userData.QuickSearchCd, false, false, true);
    }
    else
      this._labelQuickOccupyCd.gameObject.SetActive(false);
    this._marchSlotItem.SecondTick();
    bool flag = false;
    using (List<MerlinTileItem>.Enumerator enumerator = this._allTileItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MerlinTileItem current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
        {
          current.OnSecondTick();
          MerlinMineData mineData = current.MineData;
          if (mineData != null && mineData.IsGatherFinished)
          {
            if (currentTowerMainInfo.NoviceProtection > 0)
              current.SetData((MerlinMineData) null, current.Page, current.Index, new System.Action<MerlinTileItem>(this.OnTileClicked));
            else if (kingdomData.LeftTime > 0)
              flag = true;
          }
        }
      }
    }
    if (!flag || this._requesting)
      return;
    this.RequestEnterMineField(this._currentPage);
  }

  protected List<Vector3> RandomPositionsForTiles(int level, int pageIndex)
  {
    List<Vector3> vector3List = new List<Vector3>();
    UnityEngine.Random.seed = level * 10000 + pageIndex;
    List<int> intList = new List<int>();
    for (int index = 0; index < this._allTilePosition.Count; ++index)
      intList.Add(index);
    for (int index1 = 0; index1 < 9; ++index1)
    {
      int index2 = UnityEngine.Random.Range(0, intList.Count - 1);
      vector3List.Add(this._allTilePosition[intList[index2]]);
      intList.RemoveAt(index2);
    }
    return vector3List;
  }

  protected void UpdateTiles()
  {
    this.CloseCircleMenu();
    MerlinKingdomData kingdomData = MerlinTowerPayload.Instance.KingdomData;
    this._inputPageIndex.text = kingdomData.page.ToString();
    this._currentPage = kingdomData.page;
    MagicTowerMainInfo currentTowerMainInfo = MerlinTowerPayload.Instance.UserData.CurrentTowerMainInfo;
    if (currentTowerMainInfo == null)
      return;
    if (currentTowerMainInfo.NoviceProtection > 0)
    {
      this._buttonMoveNext.isEnabled = false;
      this._buttonMovePrevious.isEnabled = false;
      this._inputPageIndex.enabled = false;
      if (kingdomData.show_index < kingdomData.mine_list.Count)
      {
        for (int index = 0; index < kingdomData.mine_list.Count; ++index)
        {
          MerlinMineData mine = kingdomData.mine_list[index];
          if (mine != null && mine.uid == PlayerData.inst.uid)
          {
            MerlinMineData merlinMineData = mine;
            kingdomData.mine_list[index] = kingdomData.mine_list[kingdomData.show_index];
            kingdomData.mine_list[kingdomData.show_index] = merlinMineData;
            break;
          }
        }
      }
    }
    else
    {
      this._inputPageIndex.enabled = true;
      this._buttonMoveNext.isEnabled = kingdomData.page < kingdomData.max_page;
      this._buttonMovePrevious.isEnabled = kingdomData.page > 1;
    }
    List<Vector3> vector3List = this.RandomPositionsForTiles(currentTowerMainInfo.Level, this._currentPage);
    for (int index = 0; index < 9; ++index)
    {
      MerlinMineData mineData = (MerlinMineData) null;
      if (kingdomData.mine_list != null && kingdomData.mine_list.Count > index)
        mineData = kingdomData.mine_list[index];
      this._allTileItem[index].SetData(mineData, kingdomData.page, index, new System.Action<MerlinTileItem>(this.OnTileClicked));
      this._allTileItem[index].transform.localPosition = vector3List[index];
    }
    int requestOpenMienIndex = MerlinTowerPayload.Instance.RequestOpenMienIndex;
    if (requestOpenMienIndex == -1)
      return;
    this._allTileItem[requestOpenMienIndex % 9].ShowCircleMenu();
    MerlinTowerPayload.Instance.RequestOpenMienIndex = -1;
  }

  public void OnButtonPreviusClicked()
  {
    Logger.Log(nameof (OnButtonPreviusClicked));
    MerlinKingdomData kingdomData = MerlinTowerPayload.Instance.KingdomData;
    if (kingdomData.page <= 0)
      return;
    this.RequestEnterMineField(kingdomData.page - 1);
  }

  public void OnButtonNextClicked()
  {
    Logger.Log(nameof (OnButtonNextClicked));
    MerlinKingdomData kingdomData = MerlinTowerPayload.Instance.KingdomData;
    if (kingdomData.page >= kingdomData.max_page)
      return;
    this.RequestEnterMineField(kingdomData.page + 1);
  }

  public void OnInputPageIndexChanged()
  {
    Logger.Log(nameof (OnInputPageIndexChanged));
    if (this.KingdomData == null)
      return;
    int result = 0;
    int.TryParse(this._inputPageIndex.value, out result);
    int pageIndex = Mathf.Clamp(result, 1, this.KingdomData.max_page);
    if (pageIndex != this.KingdomData.page)
      this.RequestEnterMineField(pageIndex);
    this._inputPageIndex.value = pageIndex.ToString();
  }

  public void OnButtonQuickOccupyClicked()
  {
    Logger.Log(nameof (OnButtonQuickOccupyClicked));
    if (MerlinTowerPayload.Instance.UserData.QuickSearchCd > 0)
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_mine_occupy_only_one", true), (System.Action) null, 4f, true);
    else if (MerlinTowerPayload.Instance.UserData.mineJobId != 0L)
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_mine_occupy_only_one", true), (System.Action) null, 4f, true);
    }
    else
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("magic_tower_fast_occupy_need_vip");
      VIP_Level_Info vipLevelInfo = ConfigManager.inst.DB_VIP_Level.VIPInfoForPoints((int) PlayerData.inst.userData.vipPoint);
      if (data != null && vipLevelInfo != null)
      {
        if (data.ValueInt > vipLevelInfo.Level)
        {
          MessageBoxWith2Buttons.Parameter parameter = new MessageBoxWith2Buttons.Parameter();
          string withPara = ScriptLocalization.GetWithPara("tower_mine_vip_requirement_description", new Dictionary<string, string>()
          {
            {
              "0",
              data.ValueInt.ToString()
            }
          }, 1 != 0);
          parameter.title = ScriptLocalization.Get("dragon_knight_raid_vip_low_title", true);
          parameter.content = withPara;
          parameter.yes = ScriptLocalization.Get("id_uppercase_cancel", true);
          parameter.no = ScriptLocalization.Get("vip_upgrade_button", true);
          parameter.yesCallback = (System.Action) null;
          parameter.noCallback = (System.Action) (() => UIManager.inst.OpenDlg("VIP/VipBaseDlg", (UI.Dialog.DialogParameter) null, true, true, true));
          UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) parameter);
          return;
        }
        if (PlayerData.inst.userData.vipJobId == 0L)
        {
          MessageBoxWith2Buttons.Parameter parameter = new MessageBoxWith2Buttons.Parameter();
          string withPara = ScriptLocalization.GetWithPara("tower_mine_vip_requirement_description", new Dictionary<string, string>()
          {
            {
              "0",
              data.ValueInt.ToString()
            }
          }, 1 != 0);
          parameter.title = ScriptLocalization.Get("dragon_knight_vip_not_open_title", true);
          parameter.content = withPara;
          parameter.yes = ScriptLocalization.Get("id_uppercase_cancel", true);
          parameter.no = ScriptLocalization.Get("vip_activate_button", true);
          parameter.noCallback = (System.Action) (() => UIManager.inst.OpenDlg("VIP/VipBaseDlg", (UI.Dialog.DialogParameter) null, true, true, true));
          parameter.yesCallback = (System.Action) null;
          UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) parameter);
          return;
        }
      }
      this.CloseCircleMenu();
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = ScriptLocalization.Get("tower_speed_up_mining_button", true),
        Content = ScriptLocalization.Get("tower_mine_searching_description", true),
        Okay = ScriptLocalization.Get("id_uppercase_okay", true),
        OnCloseCallback = (System.Action) (() => this.DoQuickOccupy())
      });
    }
  }

  protected void DoQuickOccupy()
  {
    UIManager.inst.OpenDlg("MerlinTower/MerlinTowerOreMarchAllocDlg", (UI.Dialog.DialogParameter) new MerlinTowerOreMarchAllocDlg.Parameter()
    {
      desTroopCount = 1000,
      maxTime = 10000,
      remainTime = (NetServerTime.inst.ServerTimestamp + 360000),
      speed = 100,
      isRally = false,
      marchType = MarchAllocDlg.MarchType.gather,
      confirmCallback = (System.Action<Hashtable, bool>) ((troops, withDragon) =>
      {
        MagicTowerMainInfo currentTowerMainInfo = MerlinTowerPayload.Instance.UserData.CurrentTowerMainInfo;
        if (currentTowerMainInfo != null && currentTowerMainInfo.NoviceProtection > 0)
          MerlinTowerPayload.Instance.SendGatherRequest(troops, withDragon, 1, 0);
        else
          MerlinTowerPayload.Instance.SendFindFreeMineRequest(troops, withDragon);
      })
    }, true, true, true);
  }

  public void OnButtonBattleHistoryClicked()
  {
    Logger.Log(nameof (OnButtonBattleHistoryClicked));
    MerlinTowerPayload.Instance.UserData.showRedPoint = 0;
    if ((UnityEngine.Object) this._battleRedPoint != (UnityEngine.Object) null)
      this._battleRedPoint.SetActive(false);
    UIManager.inst.OpenPopup("MerlinTower/MerlinTowerWarLogPop", (Popup.PopupParameter) null);
  }

  public void OnButtonSettingClicked()
  {
    Logger.Log(nameof (OnButtonSettingClicked));
    UIManager.inst.OpenPopup("MerlinTower/MerlinTowerSettingPopup", (Popup.PopupParameter) null);
  }

  public void OnButtonBuyBenefitClicked()
  {
    Logger.Log(nameof (OnButtonBuyBenefitClicked));
    MagicTowerMainInfo currentTowerMainInfo = MerlinTowerPayload.Instance.UserData.CurrentTowerMainInfo;
    if (currentTowerMainInfo == null)
      return;
    UIManager.inst.OpenPopup("MerlinTower/MerlinTowerBuffStorePopup", (Popup.PopupParameter) new MerlinTowerBuffStorePopup.Parameter()
    {
      layer = currentTowerMainInfo.Level
    });
  }

  public void OnButtonViewBenefitClicked()
  {
    Logger.Log(nameof (OnButtonViewBenefitClicked));
    UIManager.inst.OpenPopup("MerlinTower/MerlinTowerViewBenefitsPopup", (Popup.PopupParameter) null);
  }

  protected void OnTileClicked(MerlinTileItem tileItem)
  {
    Logger.Log(nameof (OnTileClicked));
    this.CloseCircleMenu();
    tileItem.ShowCircleMenu();
  }

  public void OnBackgroundClicked()
  {
    Logger.Log(nameof (OnBackgroundClicked));
    this.CloseCircleMenu();
  }

  public void CloseCircleMenu()
  {
    using (List<MerlinTileItem>.Enumerator enumerator = this._allTileItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.CloseCircleMenu();
    }
  }

  public void OnButtonExchangeShopClick()
  {
    UIManager.inst.OpenDlg("DragonKnight/DungeonAncientStoreDialog", (UI.Dialog.DialogParameter) new DungeonAncientStoreDialog.Parameter()
    {
      group = "magic_tower_shop",
      currency = "item_magic_tower_coin_1",
      titleKey = "tower_shop_name"
    }, true, true, true);
  }
}
