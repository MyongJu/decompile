﻿// Decompiled with JetBrains decompiler
// Type: GreyUtility
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GreyUtility
{
  public static void Grey(GameObject obj)
  {
    if (Shader.globalMaximumLOD <= 100)
      return;
    foreach (UIWidget componentsInChild in obj.GetComponentsInChildren<UISprite>())
      componentsInChild.color = new Color(0.0f, 0.0f, 1f, 1f);
    foreach (UIWidget componentsInChild in obj.GetComponentsInChildren<UITexture>())
      componentsInChild.color = new Color(0.0f, 0.0f, 1f, 1f);
    foreach (UIWidget componentsInChild in obj.GetComponentsInChildren<UI2DSprite>())
      componentsInChild.color = new Color(0.0f, 0.0f, 1f, 1f);
    SpriteRenderer[] componentsInChildren = obj.GetComponentsInChildren<SpriteRenderer>();
    int index = 0;
    for (int length = componentsInChildren.Length; index < length; ++index)
      componentsInChildren[index].color = new Color(0.0f, 0.0f, 1f, 1f);
    foreach (Renderer componentsInChild in obj.GetComponentsInChildren<SkinnedMeshRenderer>())
      componentsInChild.material.SetInt("_Grey", 1);
  }

  public static void Normal(GameObject obj)
  {
    foreach (UIWidget componentsInChild in obj.GetComponentsInChildren<UISprite>())
      componentsInChild.color = Color.white;
    foreach (UIWidget componentsInChild in obj.GetComponentsInChildren<UITexture>())
      componentsInChild.color = Color.white;
    foreach (UIWidget componentsInChild in obj.GetComponentsInChildren<UI2DSprite>())
      componentsInChild.color = Color.white;
    SpriteRenderer[] componentsInChildren = obj.GetComponentsInChildren<SpriteRenderer>();
    int index = 0;
    for (int length = componentsInChildren.Length; index < length; ++index)
      componentsInChildren[index].color = Color.white;
    foreach (Renderer componentsInChild in obj.GetComponentsInChildren<SkinnedMeshRenderer>())
      componentsInChild.material.SetInt("_Grey", 0);
  }

  public static void Custom(GameObject obj, Color color)
  {
    if (Shader.globalMaximumLOD <= 100)
      return;
    foreach (UIWidget componentsInChild in obj.GetComponentsInChildren<UISprite>())
      componentsInChild.color = color;
    foreach (UIWidget componentsInChild in obj.GetComponentsInChildren<UITexture>())
      componentsInChild.color = color;
    foreach (UIWidget componentsInChild in obj.GetComponentsInChildren<UI2DSprite>())
      componentsInChild.color = color;
    foreach (Renderer componentsInChild in obj.GetComponentsInChildren<SkinnedMeshRenderer>())
      componentsInChild.material.SetInt("_Grey", 1);
  }
}
