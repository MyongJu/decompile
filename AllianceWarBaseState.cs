﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarBaseState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public abstract class AllianceWarBaseState : MonoBehaviour
{
  public const string INSTRUCTION = "alliance_warfare_event_description";
  public UILabel state;
  public UILabel instruction;
  protected AllianceWarActivityData allianceWarData;
  private AllianceWarStateManager stateManager;
  private bool needRequestData;

  public virtual void Show(bool requestData = false)
  {
    this.needRequestData = requestData;
    if (requestData)
      this.RequestData(true);
    else
      this.Init();
    this.AddEventHandler();
  }

  private void Init()
  {
    this.gameObject.SetActive(true);
    this.UpdateUI();
  }

  public virtual void Hide()
  {
    this.gameObject.SetActive(false);
    this.RemoveEventHandler();
  }

  public abstract void UpdateUI();

  public void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  public void RemoveEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
  }

  public void Process(int time = 0)
  {
    this.UpdateUI();
  }

  public void RequestData(bool blockScreen = true)
  {
    this.RequestData(new System.Action<bool, object>(this.OnRequestDataCallback), blockScreen);
  }

  public void SetStateManager(AllianceWarStateManager manager)
  {
    this.stateManager = manager;
  }

  public void SetAllianceWarData(AllianceWarActivityData data)
  {
    this.allianceWarData = data;
  }

  public virtual void RefreshDataAfterReqeustCallback()
  {
  }

  private void RequestData(System.Action<bool, object> callback, bool blockScreen = true)
  {
    MessageHub.inst.GetPortByAction("AC:getActivityInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }

  private void OnRequestDataCallback(bool ret, object data)
  {
    if (!ret)
      return;
    ActivityManager.Intance.allianceWar.Decode(data as Hashtable);
    this.allianceWarData = ActivityManager.Intance.allianceWar;
    this.stateManager.UpdateData(this.allianceWarData);
    if (!this.needRequestData)
      return;
    this.Init();
    this.RefreshDataAfterReqeustCallback();
  }
}
