﻿// Decompiled with JetBrains decompiler
// Type: ConstructionResearchDetailStats
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ConstructionResearchDetailStats : MonoBehaviour
{
  private ArrayList _requirements = new ArrayList();
  private Dictionary<string, GameObject> requirementGameobjectDic = new Dictionary<string, GameObject>();
  private Dictionary<ConstructionResearchDetailStats.Rewards, GameObject> rewardsGameobjectDic = new Dictionary<ConstructionResearchDetailStats.Rewards, GameObject>();
  public UITexture mBuildingIcon;
  public UILabel mBuildingLevel;
  public UILabel mConstructionTimeNormal;
  public UILabel mConstructionTimeBoost;
  public UILabel mInstantBuildValue;
  public UILabel mDialogTitle;
  public GameObject mQueueFullReqBtn;
  public GameObject mBuildingReqBtn;
  public GameObject mTechReqBtn;
  public GameObject mWoodReqBtn;
  public GameObject mFoodReqBtn;
  public GameObject mSilverReqBtn;
  public GameObject mOreReqBtn;
  public GameObject mItemReqBtn;
  public GameObject mBuildLimitReqBtn;
  public GameObject mRewardHeroXP;
  public GameObject mRewardPower;
  public GameObject mRewardDefault;
  public GameObject mRequirementsPage;
  public GameObject mRewardsPage;
  public UIButton mBuildBtn;
  public UIButton mInstBuidBtn;
  public GameObject mQueueSpeedupPopup;
  public PageTabsMonitor PTM;
  private float _requirementsY;
  private float _rewardsY;
  private int currentTabIndex;
  private bool _isDestroy;

  public void SetDetails(string iconStr, string buildingLevel, double normalSeconds, double boostSeconds, int instBuildAmount, string buildingName, int iLevel, string buildingClass, System.Action onFinished)
  {
    this.mBuildingLevel.text = buildingLevel;
    this.mConstructionTimeNormal.text = Utils.ConvertSecsToString(normalSeconds);
    this.mConstructionTimeBoost.text = Utils.ConvertSecsToString(boostSeconds);
    this.mInstantBuildValue.text = Utils.FormatThousands(instBuildAmount.ToString());
    this.mDialogTitle.text = buildingName;
    this.PTM.onTabSelected += new System.Action<int>(this.OnTabSelected);
    this.PTM.SetCurrentTab(this.currentTabIndex, true);
    this.ClearRequirements();
  }

  public void SetQueueFull()
  {
  }

  public GameObject AddRequirement(ConstructionResearchDetailStats.Requirement requirement, string icon, string name, double amount, double need, double originNeed = 0)
  {
    GameObject gob = (GameObject) null;
    if (this.requirementGameobjectDic.ContainsKey(icon + requirement.ToString()))
    {
      gob = this.requirementGameobjectDic[icon + requirement.ToString()];
    }
    else
    {
      switch (requirement)
      {
        case ConstructionResearchDetailStats.Requirement.FOOD:
          gob = Utils.DuplicateGOB(this.mFoodReqBtn);
          break;
        case ConstructionResearchDetailStats.Requirement.WOOD:
          gob = Utils.DuplicateGOB(this.mWoodReqBtn);
          break;
        case ConstructionResearchDetailStats.Requirement.SILVER:
          gob = Utils.DuplicateGOB(this.mSilverReqBtn);
          break;
        case ConstructionResearchDetailStats.Requirement.ORE:
          gob = Utils.DuplicateGOB(this.mOreReqBtn);
          break;
        case ConstructionResearchDetailStats.Requirement.BUILDING:
          gob = Utils.DuplicateGOB(this.mBuildingReqBtn);
          break;
        case ConstructionResearchDetailStats.Requirement.ITEM:
          gob = Utils.DuplicateGOB(this.mItemReqBtn);
          break;
        case ConstructionResearchDetailStats.Requirement.QUEUE:
          gob = Utils.DuplicateGOB(this.mQueueFullReqBtn);
          break;
        case ConstructionResearchDetailStats.Requirement.LIMIT:
          gob = Utils.DuplicateGOB(this.mBuildLimitReqBtn);
          break;
        case ConstructionResearchDetailStats.Requirement.TECH:
          gob = Utils.DuplicateGOB(this.mTechReqBtn);
          break;
      }
      this.requirementGameobjectDic.Add(icon + requirement.ToString(), gob);
    }
    gob.SetActive(true);
    Utils.SetLPY(gob, this._requirementsY);
    this._requirementsY -= (float) gob.GetComponent<UIWidget>().height;
    ConstructionRequirementBtn component = gob.GetComponent<ConstructionRequirementBtn>();
    switch (requirement)
    {
      case ConstructionResearchDetailStats.Requirement.FOOD:
      case ConstructionResearchDetailStats.Requirement.WOOD:
      case ConstructionResearchDetailStats.Requirement.SILVER:
      case ConstructionResearchDetailStats.Requirement.ORE:
        component.SetDetails(icon, this.DecurateString(amount, need, originNeed), (string) null, amount >= need, true);
        break;
      case ConstructionResearchDetailStats.Requirement.BUILDING:
        component.SetDetails(icon, "LV." + need.ToString(), name + string.Empty, amount >= need, false);
        break;
      case ConstructionResearchDetailStats.Requirement.ITEM:
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(int.Parse(name));
        component.SetDetails(itemStaticInfo.Image, need.ToString(), Utils.XLAT(itemStaticInfo.Loc_Name_Id), amount >= need, false);
        break;
      case ConstructionResearchDetailStats.Requirement.QUEUE:
        component.SetDetails((string) null, Utils.ConvertNumberToNormalString((int) amount) + "/" + Utils.ConvertNumberToNormalString((int) need), amount >= need ? Utils.XLAT("id_uppercase_queue_full") : "QUEUE", amount < need, true);
        component.SetLabel(Utils.XLAT("id_uppercase_speedup"));
        break;
      case ConstructionResearchDetailStats.Requirement.LIMIT:
        component.SetDetails((string) null, Utils.ConvertNumberToNormalString((int) amount) + "/" + Utils.ConvertNumberToNormalString((int) need), amount >= need ? "LIMIT FULL" : "LIMIT", amount < need, false);
        break;
      case ConstructionResearchDetailStats.Requirement.TECH:
        component.SetDetails(icon, "LV." + need.ToString(), name + string.Empty, amount >= need, false);
        break;
    }
    this._requirements.Add((object) gob);
    return gob;
  }

  private string DecurateString(double amount, double need, double originNedd = 0)
  {
    string str1 = "[e71200]";
    string str2 = "[-]";
    string str3 = "[00ff00]";
    string format = "{0}{1}{2}/{3}{4}";
    string str4 = amount >= need ? string.Empty : str1;
    string normalString1 = Utils.ConvertNumberToNormalString((long) amount);
    string str5 = amount >= need ? string.Empty : str2;
    string str6 = string.Empty;
    if (need < originNedd)
      str6 = str3;
    else if (need > originNedd)
      str6 = str1;
    string normalString2 = Utils.ConvertNumberToNormalString((long) need);
    return string.Format(format, (object) str4, (object) normalString1, (object) str5, (object) str6, (object) normalString2);
  }

  public GameObject AddRewards(ConstructionResearchDetailStats.Rewards reward, string icon, string name, string amount)
  {
    GameObject gob = (GameObject) null;
    if (this.rewardsGameobjectDic.ContainsKey(reward))
    {
      gob = this.rewardsGameobjectDic[reward];
    }
    else
    {
      switch (reward)
      {
        case ConstructionResearchDetailStats.Rewards.HERO_XP:
          gob = Utils.DuplicateGOB(this.mRewardHeroXP);
          break;
        case ConstructionResearchDetailStats.Rewards.POWER:
          gob = Utils.DuplicateGOB(this.mRewardPower);
          break;
        case ConstructionResearchDetailStats.Rewards.DEFAULT:
          gob = Utils.DuplicateGOB(this.mRewardDefault);
          break;
      }
      this.rewardsGameobjectDic.Add(reward, gob);
    }
    gob.SetActive(true);
    Utils.SetLPY(gob, this._rewardsY);
    this._rewardsY -= (float) gob.GetComponent<UIWidget>().height;
    ConstructionRewardBtn component = gob.GetComponent<ConstructionRewardBtn>();
    switch (reward)
    {
      case ConstructionResearchDetailStats.Rewards.POWER:
        component.SetDetails((string) null, "+" + amount, (string) null);
        break;
      case ConstructionResearchDetailStats.Rewards.DEFAULT:
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[int.Parse(name)];
        component.SetDetails(dbProperty.ImagePath, amount, dbProperty.Name);
        break;
    }
    this._requirements.Add((object) gob);
    return gob;
  }

  public void ClearRequirements()
  {
    foreach (GameObject requirement in this._requirements)
      requirement.SetActive(false);
    this._requirements.Clear();
    this._requirementsY = this.mQueueFullReqBtn.transform.localPosition.y;
    this._rewardsY = this.mRewardHeroXP.transform.localPosition.y;
  }

  public void OnCloseBtnPressed()
  {
    foreach (UnityEngine.Object requirement in this._requirements)
      UnityEngine.Object.Destroy(requirement);
    this.gameObject.SetActive(false);
  }

  private void OnDestroy()
  {
    this._isDestroy = true;
  }

  public void OnTabSelected(int selectedTab)
  {
    switch (selectedTab)
    {
      case 0:
        this.mRequirementsPage.SetActive(true);
        this.mRewardsPage.SetActive(false);
        break;
      case 1:
        this.mRequirementsPage.SetActive(false);
        this.mRewardsPage.SetActive(true);
        break;
    }
    this.currentTabIndex = selectedTab;
  }

  public void OnGetMoreFoodBtnPressed()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.FOOD);
  }

  public void OnGetMoreWoodBtnPressed()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.WOOD);
  }

  public void OnGetMoreSilverBtnPressed()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.SILVER);
  }

  public void OnGetMoreOreBtnPressed()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.ORE);
  }

  public void OnQueueSpeedUpConfirmed()
  {
    JobHandle job = JobManager.Instance.GetJob(ResearchManager.inst.GetJobId(ResearchManager.inst.CurrentResearch));
    int num = job != null ? job.LeftTime() : 0;
    Hashtable ht = new Hashtable();
    ht.Add((object) "time", (object) num);
    GoldConsumePopup.Parameter parameter = new GoldConsumePopup.Parameter();
    int cost = ItemBag.CalculateCost(ht);
    parameter.confirmButtonClickEvent = new System.Action(this.ConFirmCallBack);
    parameter.lefttime = num;
    parameter.goldNum = cost;
    parameter.description = ScriptLocalization.GetWithPara("confirm_gold_spend_cooldown_instant_desc", new Dictionary<string, string>()
    {
      {
        "num",
        cost.ToString()
      }
    }, true);
    UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) parameter);
  }

  private void ConFirmCallBack()
  {
    if (this._isDestroy)
      return;
    Hashtable ht = new Hashtable();
    long jobId = ResearchManager.inst.GetJobId(ResearchManager.inst.CurrentResearch);
    JobHandle job = JobManager.Instance.GetJob(jobId);
    int num = job != null ? job.LeftTime() : 0;
    ht.Add((object) "time", (object) num);
    int cost = ItemBag.CalculateCost(ht);
    RequestManager.inst.SendRequest("City:speedUpByGold", new Hashtable()
    {
      {
        (object) "job_id",
        (object) jobId
      },
      {
        (object) "gold",
        (object) cost
      }
    }, (System.Action<bool, object>) null, true);
  }

  public void OnQueueSpeedUpClosed()
  {
    this.mQueueSpeedupPopup.SetActive(false);
  }

  public enum Requirement
  {
    NONE,
    FOOD,
    WOOD,
    SILVER,
    ORE,
    BUILDING,
    ITEM,
    QUEUE,
    LIMIT,
    TECH,
  }

  public enum Rewards
  {
    NONE,
    HERO_XP,
    POWER,
    DEFAULT,
  }
}
