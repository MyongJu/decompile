﻿// Decompiled with JetBrains decompiler
// Type: TouchKeyboardAdaptor
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TouchKeyboardAdaptor : UIWidget
{
  private bool _keyboardState;

  protected override void Awake()
  {
    base.Awake();
    this._keyboardState = false;
  }

  protected override void OnUpdate()
  {
    base.OnUpdate();
    bool visible = TouchScreenKeyboard.visible;
    if (this._keyboardState == visible)
      return;
    this._keyboardState = visible;
    if (visible)
      this.ResizeWithKeyboard();
    else
      this.Revert();
  }

  private void ResizeWithKeyboard()
  {
    this.bottomAnchor.Set(0.0f, -TouchScreenKeyboard.area.height / (float) Screen.height * (float) this.height);
  }

  private void Revert()
  {
    this.bottomAnchor.Set(0.0f, 0.0f);
  }
}
