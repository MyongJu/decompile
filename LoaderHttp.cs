﻿// Decompiled with JetBrains decompiler
// Type: LoaderHttp
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class LoaderHttp
{
  private int _seqNum = -1;
  private Dictionary<WWW, string> _recycles = new Dictionary<WWW, string>();
  private Dictionary<WWW, string> _manifest = new Dictionary<WWW, string>();
  private List<WWW> trash = new List<WWW>();
  private Stopwatch watch = new Stopwatch();
  private const int HIDE_BLOCK_TIME = 20000;
  private int _sendTime;
  private LoaderInfo _info;
  private bool hideFullScreen;
  private bool _isFinish;
  private bool isDispose;

  public void StartHttp()
  {
    if (this._info != null)
      RequestManager.inst.BeginCoroutine(this.HttpEnum());
    else
      MessageHub.inst.OnError(nameof (LoaderHttp), "info is null");
  }

  public float Progress { get; set; }

  public List<WWW> Trash
  {
    get
    {
      return this.trash;
    }
  }

  public void Dispose()
  {
    this.trash.Clear();
    this._recycles.Clear();
    this._manifest.Clear();
    this.isDispose = true;
  }

  [DebuggerHidden]
  private IEnumerator HttpEnum()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LoaderHttp.\u003CHttpEnum\u003Ec__Iterator89()
    {
      \u003C\u003Ef__this = this
    };
  }

  private bool isFinish(WWW www)
  {
    if (this._isFinish || this._info == null)
      return true;
    if (this._info != null && this._manifest.ContainsKey(www))
    {
      if (this._seqNum < 0)
      {
        if (this._info.Action != this._manifest[www])
          return true;
      }
      else if (this._info.Action + "_" + (object) this._seqNum != this._manifest[www])
        return true;
    }
    return false;
  }

  private int Now
  {
    get
    {
      return LoaderManager.inst.Now;
    }
  }

  private void SaveRequestData(string action, string httpUrl, string httpStatus, int httpLatency, int requestSize, int responseSize, string requestId, long requestTs, long receiveTs)
  {
    NetWorkDetector.Instance.SaveRUMData(action, httpUrl, httpStatus, httpLatency, requestSize, responseSize, requestId, requestTs, receiveTs);
  }

  private void ProgressHandler()
  {
    if (this._info == null || this._info.OnProgress == null)
      return;
    this._info.OnProgress(this);
  }

  private void CompleteHandler(byte[] bytes)
  {
    if (this._isFinish)
      return;
    this._isFinish = true;
    if (this._info == null || this._info.OnResult == null)
      return;
    if (this._info.Type == 1)
      ++NetApi.inst.SeqNum;
    this._info.ResData = Utils.DecodeByGzip(bytes);
    this._info.OnResult(this);
  }

  private void FaultHandler(string error)
  {
    PlayerPrefsEx.DeleteKey("server_ip");
    if (NetApi.inst.IsUseServerIP())
      NetApi.inst.BuildFinalServerURL();
    if (this._info == null || this._info.OnFailure == null)
      return;
    this._info.ErrorMsg = error;
    this._info.ResData = (byte[]) null;
    this._info.OnFailure(this);
  }

  public LoaderInfo Info
  {
    get
    {
      return this._info;
    }
    set
    {
      this._isFinish = false;
      this.trash.Clear();
      this._info = value;
    }
  }
}
