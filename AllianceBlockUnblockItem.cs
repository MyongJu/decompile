﻿// Decompiled with JetBrains decompiler
// Type: AllianceBlockUnblockItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceBlockUnblockItem : MonoBehaviour
{
  private int m_MemberID = -1;
  public System.Action<int> onBlockBtnPressed;
  public System.Action<int> onUnblockBtnPressed;
  public UILabel mName;
  public UILabel mPower;
  public UILabel mLevel;
  public UITexture mPortrait;

  public void SetDetails(int memberID, string name, int portrait, int power, int level, string customIconUrl, int lordTitleId)
  {
    this.m_MemberID = memberID;
    this.mName.text = name;
    this.mPower.text = power.ToString();
    this.mLevel.text = level.ToString();
    CustomIconLoader.Instance.requestCustomIcon(this.mPortrait, "Texture/Hero/Portrait_Icon/player_portrait_icon_" + portrait.ToString(), customIconUrl, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.mPortrait, lordTitleId, 1);
  }

  public void OnBlockBtnPressed()
  {
    if (this.onBlockBtnPressed == null)
      return;
    this.onBlockBtnPressed(this.m_MemberID);
  }

  public void OnUnblockBtnPressed()
  {
    if (this.onUnblockBtnPressed == null)
      return;
    this.onUnblockBtnPressed(this.m_MemberID);
  }
}
