﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryIapView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AnniversaryIapView : MonoBehaviour
{
  private Dictionary<string, string> _timeParam = new Dictionary<string, string>();
  [SerializeField]
  private UITexture _textureFrame;
  [SerializeField]
  private UITexture _textureProps;
  [SerializeField]
  private UILabel _labelPropsAmount;
  [SerializeField]
  private UILabel _labelRemained;
  [SerializeField]
  private GameObject _objHasFreeProps;

  public void Show()
  {
    NGUITools.SetActive(this.gameObject, true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this._textureFrame, "Texture/STATIC_TEXTURE/bg_annv_package", (System.Action<bool>) null, true, true, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this._textureProps, AnniversaryIapPayload.Instance.PropsImagePath, (System.Action<bool>) null, true, false, string.Empty);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
    this.RefreshUI();
  }

  public void Hide()
  {
    NGUITools.SetActive(this.gameObject, false);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  public void OnPropsClicked()
  {
    AnniversaryIapPayload.Instance.RequestServerData((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenPopup("Anniversary/AnniversaryIapPropsPopup", (Popup.PopupParameter) null);
    }));
  }

  public void OnJoinEventClicked()
  {
    UIManager.inst.OpenDlg("Anniversary/AnniversaryIapMainDlg", (UI.Dialog.DialogParameter) new AnniversaryIapMainDlg.Parameter()
    {
      showCloseButton = false
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private void OnDisable()
  {
    this.Hide();
  }

  private void OnSecondEvent(int time)
  {
    this.RefreshUI();
  }

  private void RefreshUI()
  {
    this._labelPropsAmount.text = Utils.FormatThousands(AnniversaryIapPayload.Instance.PropsAmount.ToString());
    this._timeParam.Clear();
    this._timeParam.Add("0", Utils.FormatTime((int) (AnniversaryIapPayload.Instance.EndTime - (long) NetServerTime.inst.ServerTimestamp), true, false, true));
    this._labelRemained.text = ScriptLocalization.GetWithPara("id_finishes_in", this._timeParam, true);
    NGUITools.SetActive(this._objHasFreeProps, AnniversaryIapPayload.Instance.AnnvIapData.CanReceivePropsNum > 0L);
  }
}
