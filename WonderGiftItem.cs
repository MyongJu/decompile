﻿// Decompiled with JetBrains decompiler
// Type: WonderGiftItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WonderGiftItem : MonoBehaviour
{
  [SerializeField]
  protected Icon[] m_allItemIcons;
  [SerializeField]
  protected UILabel m_labelGiftName;
  [SerializeField]
  protected UILabel m_labelGiftCount;
  [SerializeField]
  protected UIButton m_buttonSend;
  protected WonderPackageInfo m_wonderPackageInfo;
  protected int m_sendCount;
  protected long m_receiverId;
  protected int m_packageMultiFactor;

  public void setData(WonderPackageInfo packageInfo, int sendCount, long recieverId = 0, int packageMultiFactor = 1)
  {
    this.m_wonderPackageInfo = packageInfo;
    this.m_sendCount = sendCount;
    this.m_receiverId = recieverId;
    this.m_packageMultiFactor = packageMultiFactor;
    this.UpdateUI();
  }

  protected void UpdateUI()
  {
    int num = Mathf.Max(0, this.m_wonderPackageInfo.maxNumber * this.m_packageMultiFactor - this.m_sendCount);
    this.m_labelGiftName.text = ScriptLocalization.Get(this.m_wonderPackageInfo.name, true);
    this.m_labelGiftCount.text = ScriptLocalization.Get("throne_royal_package_number", true) + string.Format("{0}/{1}", (object) num, (object) (this.m_wonderPackageInfo.maxNumber * this.m_packageMultiFactor));
    int index = 0;
    if (this.m_wonderPackageInfo.reward == null || this.m_wonderPackageInfo.reward.rewards == null)
    {
      D.error((object) "wonder package reward is null");
    }
    else
    {
      using (Dictionary<string, int>.Enumerator enumerator = this.m_wonderPackageInfo.reward.rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          if (index >= this.m_allItemIcons.Length)
          {
            D.error((object) "reward count greater than design count of ui");
            break;
          }
          this.m_allItemIcons[index].gameObject.SetActive(true);
          int result = 0;
          int.TryParse(current.Key, out result);
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(result);
          if (itemStaticInfo != null)
          {
            IconData iconData = new IconData();
            iconData.Data = (object) itemStaticInfo.internalId;
            iconData.image = itemStaticInfo.ImagePath;
            iconData.contents = new string[1]
            {
              current.Value.ToString()
            };
            this.m_allItemIcons[index].StopAutoFillName();
            this.m_allItemIcons[index].FeedData((IComponentData) iconData);
            this.m_allItemIcons[index].OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
            this.m_allItemIcons[index].OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
            this.m_allItemIcons[index].OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
          }
          ++index;
        }
      }
      for (; index < this.m_allItemIcons.Length; ++index)
        this.m_allItemIcons[index].gameObject.SetActive(false);
      bool isKing = PlayerData.inst.userData.IsKing;
      bool flag = this.m_receiverId != 0L && this.m_receiverId != PlayerData.inst.uid;
      this.m_buttonSend.gameObject.SetActive(num > 0 && isKing && flag);
    }
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  public void onButtonSendClicked()
  {
    this.onSendConfirmed();
  }

  protected void onSendConfirmed()
  {
    Hashtable postData = new Hashtable()
    {
      {
        (object) "target_uid",
        (object) this.m_receiverId
      },
      {
        (object) "package_id",
        (object) this.m_wonderPackageInfo.internalId
      }
    };
    RequestManager.inst.SendRequest("wonder:sendKingGiftPackage", postData, new System.Action<bool, object>(this.onSendRequestCallback), true);
  }

  protected void onSendRequestCallback(bool result, object data)
  {
    if (!result)
      return;
    ++this.m_sendCount;
    this.UpdateUI();
  }
}
