﻿// Decompiled with JetBrains decompiler
// Type: ClipboardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using NLPTrans;
using System.Collections;
using UI;
using UnityEngine;

public class ClipboardPopup : Popup
{
  private long targetUid;
  private string targetStr;
  private bool reportAble;
  private string sourceText;
  private string machineTargetText;
  private string sourceLanguage;
  private string targetLanguage;
  public GameObject reportRoot;
  public UIGrid buttonContainer;
  public GameObject reportTranslateBtn;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (orgParam != null)
    {
      this.targetStr = (orgParam as ClipboardPopup.Parameter).str;
      this.targetUid = (orgParam as ClipboardPopup.Parameter).uid;
      this.reportAble = (orgParam as ClipboardPopup.Parameter).reportAble;
      this.sourceText = (orgParam as ClipboardPopup.Parameter).sourceText;
      this.machineTargetText = (orgParam as ClipboardPopup.Parameter).machineTargetText;
      this.sourceLanguage = (orgParam as ClipboardPopup.Parameter).sourceLanguage;
      this.targetLanguage = (orgParam as ClipboardPopup.Parameter).targetLanguage;
    }
    this.UpdateUI();
  }

  protected void UpdateUI()
  {
    this.reportRoot.SetActive(false);
    this.reportTranslateBtn.SetActive(!string.IsNullOrEmpty(this.machineTargetText));
    this.buttonContainer.repositionNow = true;
    this.buttonContainer.Reposition();
  }

  public void OnCopyBtnClick()
  {
    Utils.SetClipboardText(this.targetStr);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnReportBtnClicked()
  {
    string str1 = ScriptLocalization.Get("id_uppercase_report", true);
    string str2 = ScriptLocalization.Get("player_profile_avatar_report_confirm_description", true);
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = str1,
      Content = str2,
      Okay = ScriptLocalization.Get("id_uppercase_confirm", true),
      OkayCallback = new System.Action(this.OnConfirmReportIcon)
    });
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnReportTranslateBtnClicked()
  {
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = ScriptLocalization.Get("chat_uppercase_translation_correction", true),
      content = ScriptLocalization.Get("chat_translation_correction_description", true),
      yes = ScriptLocalization.Get("id_uppercase_yes", true),
      no = ScriptLocalization.Get("id_uppercase_no", true),
      yesCallback = new System.Action(this.OnConfirmReportTranslate),
      noCallback = (System.Action) null
    });
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  protected void OnConfirmReportIcon()
  {
    Hashtable postData = new Hashtable()
    {
      {
        (object) "report_uid",
        (object) this.targetUid
      }
    };
    RequestManager.inst.SendRequest("Player:reportIcon", postData, new System.Action<bool, object>(this.OnReportCallback), true);
  }

  protected void OnConfirmReportTranslate()
  {
    NLPUtils.Instance.ReportTranslateAsyc(this.machineTargetText, this.sourceText, this.targetLanguage, this.sourceLanguage, PlayerData.inst.uid.ToString(), new System.Action<bool>(this.OnReportTranslateResout));
  }

  private void OnReportTranslateResout(bool ret)
  {
    if (!ret)
      return;
    UIManager.inst.toast.Show(Utils.XLAT("toast_chat_translation_correction_success"), (System.Action) null, 4f, false);
  }

  protected void OnReportCallback(bool result, object data)
  {
    if (result)
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_avatar_report_success", true), (System.Action) null, 4f, true);
    this.OnCloseBtnClick();
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public bool reportAble;
    public long uid;
    public string str;
    public string sourceText;
    public string machineTargetText;
    public string targetLanguage;
    public string sourceLanguage;
  }
}
