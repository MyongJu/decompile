﻿// Decompiled with JetBrains decompiler
// Type: DragonView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DragonView : SpriteView
{
  protected override void OnLoad()
  {
    base.OnLoad();
    DragonViewData viewData = this.ViewData as DragonViewData;
    if (viewData == null || viewData.vfxs == null)
      return;
    for (int index = 0; index < viewData.vfxs.Length; ++index)
      this.PlayVfx(viewData.vfxs[index]);
  }

  public void SetColor(Color color)
  {
    DragonViewData viewData = this.ViewData as DragonViewData;
    if (viewData == null)
      return;
    viewData.color = color;
    if ((Object) this.MainRenderer == (Object) null)
      return;
    this.MainRenderer.material.SetColor("_Color", color);
  }

  public void SetAlpha(float alpha)
  {
    if (!(this.ViewData is DragonViewData) || (Object) this.MainRenderer == (Object) null)
      return;
    Color color = this.MainRenderer.material.GetColor("_Color");
    color.a = alpha;
    this.MainRenderer.material.SetColor("_Color", color);
  }

  public void RotateInKingdom(Vector3 dir)
  {
    if (!((Object) this.GameObject != (Object) null))
      return;
    this.GameObject.transform.rotation = Quaternion.identity;
    this.GameObject.transform.RotateAround(this.transform.position, Vector3.right, -30f);
    this.GameObject.transform.LookAt(this.GameObject.transform.position + dir, this.GameObject.transform.up);
    this.GameObject.transform.RotateAround(this.transform.position, this.GameObject.transform.right, -20f);
  }

  public override void FaceTo(Transform target)
  {
    if (!((Object) this.GameObject != (Object) null))
      return;
    this.GameObject.transform.rotation = Quaternion.identity;
    this.GameObject.transform.RotateAround(this.transform.position, Vector3.right, -30f);
    this.GameObject.transform.LookAt(target.position, this.GameObject.transform.up);
    this.GameObject.transform.RotateAround(this.transform.position, this.GameObject.transform.right, -20f);
  }
}
