﻿// Decompiled with JetBrains decompiler
// Type: PerformanceConfiguration
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections.Generic;
using UnityEngine;

public class PerformanceConfiguration
{
  public const int DEFAULT_SHADER_LOD = 2147483647;
  private PerformanceConfiguration.Settings m_Settings;
  private static PerformanceConfiguration m_Instance;

  public static PerformanceConfiguration Instance
  {
    get
    {
      if (PerformanceConfiguration.m_Instance == null)
        PerformanceConfiguration.m_Instance = new PerformanceConfiguration();
      return PerformanceConfiguration.m_Instance;
    }
  }

  public void Initialize()
  {
    this.m_Settings = JsonReader.Deserialize<PerformanceConfiguration.Settings>((AssetManager.Instance.Load("TextAsset/Performance/Performance", (System.Type) null) as TextAsset).text);
    this.m_Settings.Sort();
    if (PlayerPrefsEx.GetInt("environment_init", 0) == 0)
    {
      SettingManager.Instance.IsEnvironmentOff = !PerformanceConfiguration.Instance.IsEnvironmentOn;
      PlayerPrefsEx.SetInt("environment_init", 1);
    }
    Shader.globalMaximumLOD = PerformanceConfiguration.Instance.ShaderLOD;
  }

  public void InitializeDefault()
  {
    if (PlayerPrefsEx.GetInt("environment_init", 0) != 0)
      return;
    this.m_Settings = JsonReader.Deserialize<PerformanceConfiguration.Settings>((AssetManager.Instance.Load("TextAsset/Performance/PerformanceDefault", (System.Type) null) as TextAsset).text);
    this.m_Settings.Sort();
    SettingManager.Instance.IsEnvironmentOff = !PerformanceConfiguration.Instance.IsEnvironmentOn;
    Shader.globalMaximumLOD = PerformanceConfiguration.Instance.ShaderLOD;
    PlayerPrefsEx.SetInt("environment_init", 1);
  }

  public bool IsEnvironmentOn
  {
    get
    {
      if (this.m_Settings != null)
        return this.m_Settings.IsEnvironmentOn;
      return true;
    }
  }

  public int ShaderLOD
  {
    get
    {
      if (this.m_Settings != null)
        return this.m_Settings.ShaderLOD;
      return int.MaxValue;
    }
  }

  public int Rating
  {
    get
    {
      if (this.m_Settings != null)
        return this.m_Settings.Rating;
      return 2000;
    }
  }

  private class Settings
  {
    public List<List<int>> shaderLODs;
    public int environment;
    public List<PerformanceConfiguration.Configuration> configs;

    public void Sort()
    {
      this.configs.Sort(new Comparison<PerformanceConfiguration.Configuration>(PerformanceConfiguration.Settings.CompareConfig));
      this.shaderLODs.Sort(new Comparison<List<int>>(PerformanceConfiguration.Settings.CompareShaderLOD));
    }

    public bool IsEnvironmentOn
    {
      get
      {
        PerformanceConfiguration.Configuration configuration = this.GetConfiguration();
        if (configuration != null)
          return configuration.rating >= this.environment;
        return false;
      }
    }

    public int ShaderLOD
    {
      get
      {
        if (this.shaderLODs == null || this.shaderLODs.Count <= 0)
          return int.MaxValue;
        PerformanceConfiguration.Configuration configuration = this.GetConfiguration();
        if (configuration == null)
          return int.MaxValue;
        int index1 = this.shaderLODs.Count - 1;
        for (int index2 = 0; index2 < index1; ++index2)
        {
          List<int> shaderLoD = this.shaderLODs[index2];
          if (configuration.rating >= shaderLoD[0])
            return shaderLoD[1];
        }
        return this.shaderLODs[index1][1];
      }
    }

    public int Rating
    {
      get
      {
        PerformanceConfiguration.Configuration configuration = this.GetConfiguration();
        if (configuration != null)
          return configuration.rating;
        return 2000;
      }
    }

    private PerformanceConfiguration.Configuration GetConfiguration()
    {
      if (this.configs == null || this.configs.Count <= 0)
        return (PerformanceConfiguration.Configuration) null;
      int index1 = this.configs.Count - 1;
      for (int index2 = 0; index2 < index1; ++index2)
      {
        PerformanceConfiguration.Configuration config = this.configs[index2];
        if (SystemInfo.graphicsMemorySize >= config.graphicsMemorySize && SystemInfo.graphicsShaderLevel >= config.graphicsShaderLevel && (SystemInfo.processorCount >= config.processorCount && SystemInfo.systemMemorySize >= config.systemMemorySize))
          return config;
      }
      return this.configs[index1];
    }

    private static int CompareConfig(PerformanceConfiguration.Configuration x, PerformanceConfiguration.Configuration y)
    {
      return y.rating - x.rating;
    }

    private static int CompareShaderLOD(List<int> x, List<int> y)
    {
      return y[0] - x[0];
    }
  }

  private class Configuration
  {
    public string name;
    public int rating;
    public int graphicsMemorySize;
    public int graphicsShaderLevel;
    public int processorCount;
    public int systemMemorySize;
  }
}
