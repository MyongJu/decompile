﻿// Decompiled with JetBrains decompiler
// Type: EquipmentEffectDetailItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EquipmentEffectDetailItem : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelDetail;

  public void SetData(string title, Color color)
  {
    this._labelDetail.supportEncoding = true;
    this._labelDetail.text = title;
    this._labelDetail.color = color;
  }
}
