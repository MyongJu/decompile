﻿// Decompiled with JetBrains decompiler
// Type: AllianceActivityRequirementPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceActivityRequirementPopup : Popup
{
  public UIGrid grid;
  public UILabel title;
  public UIScrollView scrollView;
  public GameObject normtalItem;
  public GameObject backItem;
  private bool inited;
  private ActivityBaseData data;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.data = (orgParam as AllianceActivityRequirementPopup.Parameter).data;
    this.UpdatUI();
  }

  public void OnCloaseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void UpdatUI()
  {
    if (this.inited)
      return;
    this.title.text = ScriptLocalization.Get("event_how_to_get_points_button", true);
    IRankUIRewardData data = this.data as IRankUIRewardData;
    this.ClearGrid();
    List<string> requirementDesc = data.GetRequirementDesc();
    for (int index = 0; index < requirementDesc.Count; ++index)
      this.GetItem(index).SetData(requirementDesc[index]);
    this.grid.Reposition();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.scrollView.ResetPosition()));
    this.inited = true;
  }

  private AllianceRequirementItem GetItem(int index)
  {
    GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, (index + 1) % 2 != 0 ? this.backItem : this.normtalItem);
    gameObject.SetActive(true);
    return gameObject.GetComponent<AllianceRequirementItem>();
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.grid);
  }

  public class Parameter : Popup.PopupParameter
  {
    public ActivityBaseData data;
  }
}
