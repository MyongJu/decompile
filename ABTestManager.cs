﻿// Decompiled with JetBrains decompiler
// Type: ABTestManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ABTestManager
{
  private Dictionary<string, IABCondition> conditions = new Dictionary<string, IABCondition>();
  private Dictionary<string, System.Type> conditionMap = new Dictionary<string, System.Type>()
  {
    {
      "ios_gateway",
      typeof (StringABCondtion)
    },
    {
      "config_server",
      typeof (StringABCondtion)
    },
    {
      "payment_server",
      typeof (StringABCondtion)
    },
    {
      "passport_server",
      typeof (StringABCondtion)
    }
  };
  private static ABTestManager instance;

  public static ABTestManager Instance
  {
    get
    {
      if (ABTestManager.instance == null)
        ABTestManager.instance = new ABTestManager();
      return ABTestManager.instance;
    }
  }

  public void Init(Hashtable data)
  {
    if (data == null)
      return;
    foreach (string key in (IEnumerable) data.Keys)
      this.AddCondition(key, this.ParseCondition(key, data[(object) key]));
  }

  public void Dispose()
  {
    this.conditions.Clear();
    ABTestManager.instance = (ABTestManager) null;
  }

  public void AddCondition(string aCase, IABCondition condition)
  {
    if (this.conditions.ContainsKey(aCase))
      return;
    this.conditions.Add(aCase, condition);
  }

  public T GetCondition<T>(string aCase) where T : IABCondition
  {
    IABCondition abCondition;
    this.conditions.TryGetValue(aCase, out abCondition);
    return (T) abCondition;
  }

  public bool IsTestRunning(string aCase)
  {
    return this.GetCondition<IABCondition>(aCase) != null;
  }

  private IABCondition ParseCondition(string aCase, object source)
  {
    IABCondition abCondition = (IABCondition) null;
    if (this.conditionMap.ContainsKey(aCase))
    {
      System.Type condition = this.conditionMap[aCase];
      if (condition != null)
        abCondition = Activator.CreateInstance(condition, new object[1]
        {
          source
        }) as IABCondition;
    }
    return abCondition;
  }
}
