﻿// Decompiled with JetBrains decompiler
// Type: ConfigShopCommonCategory
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigShopCommonCategory
{
  private ConfigParse _parse = new ConfigParse();
  private Dictionary<string, ShopCommonCategory> _dataByID;
  private Dictionary<int, ShopCommonCategory> _dataByInternalID;

  public void BuildDB(object res)
  {
    Hashtable sources = res as Hashtable;
    if (sources == null)
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    else
      this._parse.Parse<ShopCommonCategory, string>(sources, "id", out this._dataByID, out this._dataByInternalID);
  }

  public ShopCommonCategory Get(string id)
  {
    ShopCommonCategory shopCommonCategory;
    this._dataByID.TryGetValue(id, out shopCommonCategory);
    return shopCommonCategory;
  }

  public List<ShopCommonCategory> GetByGroupID(int groupId)
  {
    List<ShopCommonCategory> shopCommonCategoryList = new List<ShopCommonCategory>();
    Dictionary<string, ShopCommonCategory>.Enumerator enumerator = this._dataByID.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.Value.group_id == groupId)
        shopCommonCategoryList.Add(enumerator.Current.Value);
    }
    return shopCommonCategoryList;
  }
}
