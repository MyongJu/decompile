﻿// Decompiled with JetBrains decompiler
// Type: KingdomItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class KingdomItemRenderer : MonoBehaviour
{
  public UILabel kindomName;
  public UILabel remainTime;
  public UILabel status;
  private int _time;
  private KingdomWarActivityData.KingdomInfo _info;

  public void SetData(KingdomWarActivityData.KingdomInfo info)
  {
    this._info = info;
    this.kindomName.text = this._info.KingdomName;
    this.remainTime.text = this._info.GetContent();
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  public void SetData(string name, int time, string statusCode)
  {
    this.kindomName.text = name;
    this._time = time;
    this.remainTime.text = Utils.FormatTime(time, true, false, true);
    string key = statusCode;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (KingdomItemRenderer.\u003C\u003Ef__switch\u0024mapE == null)
      {
        // ISSUE: reference to a compiler-generated field
        KingdomItemRenderer.\u003C\u003Ef__switch\u0024mapE = new Dictionary<string, int>(2)
        {
          {
            "unOpen",
            0
          },
          {
            "fighting",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (KingdomItemRenderer.\u003C\u003Ef__switch\u0024mapE.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            NGUITools.SetActive(this.remainTime.gameObject, true);
            NGUITools.SetActive(this.status.gameObject, false);
            break;
          case 1:
            NGUITools.SetActive(this.remainTime.gameObject, false);
            NGUITools.SetActive(this.status.gameObject, true);
            break;
        }
      }
    }
    if (this._time <= 0)
      return;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  public void OnGotoBtnPressed()
  {
    if (this._info == null)
      return;
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    LinkerHub.Instance.LHGotoTile(new LPTile()
    {
      k = this._info.KingdomId,
      x = 640,
      y = 1274
    });
  }

  private void OnDisable()
  {
    if (!GameEngine.IsAvailable)
      return;
    this.Clear();
  }

  public void Clear()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
  }

  private void Process(int time)
  {
    this.remainTime.text = this._info.GetContent();
  }
}
