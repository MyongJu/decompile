﻿// Decompiled with JetBrains decompiler
// Type: ModResponsibilityPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class ModResponsibilityPopup : Popup
{
  public UIButton closeBtn;
  [Header("Localize")]
  public UILabel titleTxt;
  public UILabel responTxt;
  private bool inited;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    EventDelegate.Add(this.closeBtn.onClick, new EventDelegate.Callback(this.OnCloseBtn));
    if (this.inited)
      return;
    this._OnceInit();
    this.inited = true;
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    EventDelegate.Remove(this.closeBtn.onClick, new EventDelegate.Callback(this.OnCloseBtn));
  }

  private void OnCloseBtn()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  private void OnBecomeMod()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  private void OnModResponsibility()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
    AccountManager.Instance.ShowFASQs();
  }

  public void _OnceInit()
  {
    this.titleTxt.text = Utils.XLAT("settings_game_help_mod_responsibilies");
    this.responTxt.text = Utils.XLAT("settings_game_help_mod_responsibilies_description");
  }
}
