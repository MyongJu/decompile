﻿// Decompiled with JetBrains decompiler
// Type: AllianceSendResources
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using UI;
using UnityEngine;

public class AllianceSendResources : Popup
{
  private long[] m_Amounts = new long[4];
  private double[] m_Weights = new double[4];
  private EventDelegate.Callback[] m_Callbacks = new EventDelegate.Callback[4];
  private ProgressBarTweenProxy proxy = new ProgressBarTweenProxy();
  private const int RESOURCE_COUNT = 4;
  [SerializeField]
  private Transform[] m_ResourcesContainer;
  [SerializeField]
  private UILabel[] m_Resources;
  [SerializeField]
  private Transform[] m_SlidersContainer;
  [SerializeField]
  private UISlider[] m_Sliders;
  [SerializeField]
  private UIInput[] m_Inputs;
  [SerializeField]
  private UILabel m_SendTime;
  [SerializeField]
  private UILabel m_TaxRatePercent;
  [SerializeField]
  private UILabel m_TaxRateAmount;
  [SerializeField]
  private UILabel m_ResourcesLoad;
  [SerializeField]
  private UIButton m_SendButton;
  private AllianceSendResources.Parameter m_Parameter;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as AllianceSendResources.Parameter;
    CityData byUid = DBManager.inst.DB_City.GetByUid(this.m_Parameter.Uid);
    int num1 = 1;
    if (byUid != null)
      num1 = byUid.level;
    for (int index = 0; index < 4; ++index)
      this.m_Amounts[index] = 0L;
    int num2 = 0;
    int num3 = 0;
    int num4 = 10;
    int num5 = 15;
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_food");
    if (data1 != null)
      num2 = data1.ValueInt;
    GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_wood");
    if (data2 != null)
      num3 = data2.ValueInt;
    GameConfigInfo data3 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_ore");
    if (data3 != null)
      num4 = data3.ValueInt;
    GameConfigInfo data4 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_silver");
    if (data4 != null)
      num5 = data4.ValueInt;
    GameConfigInfo data5 = ConfigManager.inst.DB_GameConfig.GetData("food_load");
    if (data5 != null)
      this.m_Weights[0] = data5.Value;
    GameConfigInfo data6 = ConfigManager.inst.DB_GameConfig.GetData("wood_load");
    if (data6 != null)
      this.m_Weights[1] = data6.Value;
    GameConfigInfo data7 = ConfigManager.inst.DB_GameConfig.GetData("ore_load");
    if (data7 != null)
      this.m_Weights[2] = data7.Value;
    GameConfigInfo data8 = ConfigManager.inst.DB_GameConfig.GetData("silver_load");
    if (data8 != null)
      this.m_Weights[3] = data8.Value;
    this.m_ResourcesContainer[0].gameObject.SetActive(PlayerData.inst.CityData.mStronghold.mLevel >= num2 && num1 >= num2);
    this.m_ResourcesContainer[1].gameObject.SetActive(PlayerData.inst.CityData.mStronghold.mLevel >= num3 && num1 >= num3);
    this.m_ResourcesContainer[2].gameObject.SetActive(PlayerData.inst.CityData.mStronghold.mLevel >= num4 && num1 >= num4);
    this.m_ResourcesContainer[3].gameObject.SetActive(PlayerData.inst.CityData.mStronghold.mLevel >= num5 && num1 >= num5);
    this.m_SlidersContainer[0].gameObject.SetActive(PlayerData.inst.CityData.mStronghold.mLevel >= num2 && num1 >= num2);
    this.m_SlidersContainer[1].gameObject.SetActive(PlayerData.inst.CityData.mStronghold.mLevel >= num3 && num1 >= num3);
    this.m_SlidersContainer[2].gameObject.SetActive(PlayerData.inst.CityData.mStronghold.mLevel >= num4 && num1 >= num4);
    this.m_SlidersContainer[3].gameObject.SetActive(PlayerData.inst.CityData.mStronghold.mLevel >= num5 && num1 >= num5);
    this.m_Callbacks[0] = new EventDelegate.Callback(this.OnFoodSliderChanged);
    this.m_Callbacks[1] = new EventDelegate.Callback(this.OnWoodSliderChanged);
    this.m_Callbacks[2] = new EventDelegate.Callback(this.OnOreSliderChanged);
    this.m_Callbacks[3] = new EventDelegate.Callback(this.OnSilverSliderChanged);
    for (int index = 0; index < 4; ++index)
      this.SetResourceAmount((CityResourceInfo.ResourceType) index, this.m_Amounts[index]);
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void Update()
  {
    this.m_SendButton.isEnabled = this.CanTrade();
  }

  private bool CanTrade()
  {
    for (int index = 0; index < 4; ++index)
    {
      if (this.m_Amounts[index] != 0L)
        return true;
    }
    return false;
  }

  private void UpdateUI()
  {
    long num1 = 0;
    long num2 = 0;
    for (int index = 0; index < 4; ++index)
    {
      num1 += (long) ((double) this.m_Amounts[index] * this.m_Weights[index]);
      num2 += (long) ((double) this.m_Amounts[index] * this.m_Parameter.TaxPercent);
    }
    this.m_TaxRatePercent.text = ScriptLocalization.Get("alliance_trade_uppercase_tax", true) + " " + (this.m_Parameter.TaxPercent * 100.0).ToString() + "%:";
    this.m_TaxRateAmount.text = Utils.FormatThousands(num2.ToString());
    this.m_ResourcesLoad.text = string.Format("{0}/{1}", (object) Utils.FormatThousands(num1.ToString()), (object) Utils.FormatThousands(this.m_Parameter.MaxResourceLoad.ToString()));
    this.m_SendTime.text = Utils.FormatTime(MarchData.CalcTradeTime(PlayerData.inst.CityData.Location, this.m_Parameter.TargetLocation), false, false, true);
  }

  private long GetLeftLoad(CityResourceInfo.ResourceType resourceType)
  {
    long num = 0;
    for (int index = 0; index < 4; ++index)
    {
      if ((CityResourceInfo.ResourceType) index != resourceType)
        num += (long) ((double) this.m_Amounts[index] * this.m_Weights[index]);
    }
    return this.m_Parameter.MaxResourceLoad - num;
  }

  private void OnSliderChanged(CityResourceInfo.ResourceType resourceType)
  {
    long newValue = (long) Math.Round((double) this.m_Sliders[(int) resourceType].value * (double) this.m_Parameter.MaxResourceLoad / this.m_Weights[(int) resourceType]);
    this.SetResourceAmount(resourceType, newValue);
  }

  private void OnInputChanged(CityResourceInfo.ResourceType resourceType)
  {
    int index = (int) resourceType;
    long result = 0;
    long.TryParse(this.m_Inputs[index].value, out result);
    this.m_Sliders[index].onChange.Clear();
    this.SetResourceAmount(resourceType, result);
    this.m_Sliders[index].onChange.Add(new EventDelegate(this.m_Callbacks[index]));
  }

  private void SetResourceAmount(CityResourceInfo.ResourceType resourceType, long newValue)
  {
    long maxResourceLoad = this.m_Parameter.MaxResourceLoad;
    long leftLoad = this.GetLeftLoad(resourceType);
    long currentResource = (long) PlayerData.inst.playerCityData.resources.GetCurrentResource(resourceType);
    int index = (int) resourceType;
    this.m_Amounts[index] = newValue;
    this.m_Amounts[index] = Math.Max(0L, Math.Min(Math.Min(this.m_Amounts[index], (long) ((double) leftLoad / this.m_Weights[index])), currentResource));
    this.m_Resources[index].text = string.Format("{0}/{1}", (object) Utils.FormatThousands(this.m_Amounts[index].ToString()), (object) Utils.FormatThousands(currentResource.ToString()));
    this.m_Sliders[index].value = (float) this.m_Amounts[index] * (float) this.m_Weights[index] / (float) maxResourceLoad;
    this.m_Inputs[index].value = this.m_Amounts[index].ToString();
    if (currentResource == 0L)
      this.m_SendButton.isEnabled = this.CanTrade();
    this.UpdateUI();
  }

  private float GetLeftValue(CityResourceInfo.ResourceType resourceType)
  {
    long maxResourceLoad = this.m_Parameter.MaxResourceLoad;
    long leftLoad = this.GetLeftLoad(resourceType);
    long currentResource = (long) PlayerData.inst.playerCityData.resources.GetCurrentResource(resourceType);
    return (float) leftLoad / (float) maxResourceLoad;
  }

  public void OnFoodSliderChanged()
  {
    this.OnSliderChanged(CityResourceInfo.ResourceType.FOOD);
  }

  public void OnWoodSliderChanged()
  {
    this.OnSliderChanged(CityResourceInfo.ResourceType.WOOD);
  }

  public void OnOreSliderChanged()
  {
    this.OnSliderChanged(CityResourceInfo.ResourceType.ORE);
  }

  public void OnSilverSliderChanged()
  {
    this.OnSliderChanged(CityResourceInfo.ResourceType.SILVER);
  }

  public void FoodInputChanged()
  {
    this.OnInputChanged(CityResourceInfo.ResourceType.FOOD);
  }

  public void WoodInputChanged()
  {
    this.OnInputChanged(CityResourceInfo.ResourceType.WOOD);
  }

  public void OreInputChanged()
  {
    this.OnInputChanged(CityResourceInfo.ResourceType.ORE);
  }

  public void SilverInputChanged()
  {
    this.OnInputChanged(CityResourceInfo.ResourceType.SILVER);
  }

  private void OnDecButtonClick(CityResourceInfo.ResourceType resourceType)
  {
    int index = (int) resourceType;
    this.m_Sliders[index].onChange.Clear();
    this.SetResourceAmount(resourceType, this.m_Amounts[index] - 1L);
    this.m_Sliders[index].onChange.Add(new EventDelegate(this.m_Callbacks[index]));
  }

  private void OnIncButtonClick(CityResourceInfo.ResourceType resourceType)
  {
    int index = (int) resourceType;
    this.m_Sliders[index].onChange.Clear();
    this.SetResourceAmount(resourceType, this.m_Amounts[index] + 1L);
    this.m_Sliders[index].onChange.Add(new EventDelegate(this.m_Callbacks[index]));
  }

  private void OnIncButtonDown(CityResourceInfo.ResourceType resourceType)
  {
    int index = (int) resourceType;
    this.proxy.Slider = (UIProgressBar) this.m_Sliders[index];
    this.proxy.MaxSize = (int) this.m_Parameter.MaxResourceLoad;
    this.proxy.BaseValue = (int) this.m_Weights[index];
    this.proxy.Play(1, this.GetLeftValue(resourceType));
  }

  private void OnDecButtonDown(CityResourceInfo.ResourceType resourceType)
  {
    int index = (int) resourceType;
    this.proxy.Slider = (UIProgressBar) this.m_Sliders[index];
    this.proxy.MaxSize = (int) this.m_Parameter.MaxResourceLoad;
    this.proxy.BaseValue = (int) this.m_Weights[index];
    this.proxy.Play(-1, -1f);
  }

  private void OnDecButtonUp()
  {
    this.proxy.Stop();
  }

  public void OnFoodDescBtnDown()
  {
    this.OnDecButtonDown(CityResourceInfo.ResourceType.FOOD);
  }

  public void OnWoodDescBtnDown()
  {
    this.OnDecButtonDown(CityResourceInfo.ResourceType.WOOD);
  }

  public void OnOreDescBtnDown()
  {
    this.OnDecButtonDown(CityResourceInfo.ResourceType.ORE);
  }

  public void OnSilverDescBtnDown()
  {
    this.OnDecButtonDown(CityResourceInfo.ResourceType.SILVER);
  }

  public void OnFoodIncBtnDown()
  {
    this.OnIncButtonDown(CityResourceInfo.ResourceType.FOOD);
  }

  public void OnWoodIncBtnDown()
  {
    this.OnIncButtonDown(CityResourceInfo.ResourceType.WOOD);
  }

  public void OnOreIncBtnDown()
  {
    this.OnIncButtonDown(CityResourceInfo.ResourceType.ORE);
  }

  public void OnSilverIncBtnDown()
  {
    this.OnIncButtonDown(CityResourceInfo.ResourceType.SILVER);
  }

  public void OnBtnUp()
  {
    this.OnDecButtonUp();
  }

  public void OnFoodDecBtn()
  {
    this.OnDecButtonClick(CityResourceInfo.ResourceType.FOOD);
  }

  public void OnFoodIncBtn()
  {
    this.OnIncButtonClick(CityResourceInfo.ResourceType.FOOD);
  }

  public void OnWoodDecBtn()
  {
    this.OnDecButtonClick(CityResourceInfo.ResourceType.WOOD);
  }

  public void OnWoodIncBtn()
  {
    this.OnIncButtonClick(CityResourceInfo.ResourceType.WOOD);
  }

  public void OnOreDecBtn()
  {
    this.OnDecButtonClick(CityResourceInfo.ResourceType.ORE);
  }

  public void OnOreIncBtn()
  {
    this.OnIncButtonClick(CityResourceInfo.ResourceType.ORE);
  }

  public void OnSilverDecBtn()
  {
    this.OnDecButtonClick(CityResourceInfo.ResourceType.SILVER);
  }

  public void OnSilverIncBtn()
  {
    this.OnIncButtonClick(CityResourceInfo.ResourceType.SILVER);
  }

  public void OnSendBtnPressed()
  {
    if (PlayerData.inst.playerCityData.cityLocation.K != this.m_Parameter.TargetLocation.K)
    {
      this.OnClosePressed();
      UIManager.inst.ShowSystemBlocker("CommonBlocker", (SystemBlocker.SystemBlockerParameter) new CommonBlocker.Parameter()
      {
        type = CommonBlocker.CommonBlockerType.confirmation,
        displayType = 11,
        title = Utils.XLAT("exception_title"),
        buttonEventText = Utils.XLAT("exception_button"),
        descriptionText = ScriptLocalization.Get("exception_1200200", true),
        buttonEvent = (System.Action) null
      });
    }
    else
    {
      Hashtable resources = new Hashtable();
      resources[(object) "food"] = (object) this.m_Amounts[0];
      resources[(object) "wood"] = (object) this.m_Amounts[1];
      resources[(object) "ore"] = (object) this.m_Amounts[2];
      resources[(object) "silver"] = (object) this.m_Amounts[3];
      GameEngine.Instance.marchSystem.StartTrade(this.m_Parameter.Uid, resources, new System.Action<bool, object>(this.HelpCallback));
    }
  }

  private void HelpCallback(bool ret, object data)
  {
    this.OnClosePressed();
  }

  public class Parameter : Popup.PopupParameter
  {
    public long Uid;
    public double TaxPercent;
    public long MaxResourceLoad;
    public Coordinate TargetLocation;
  }
}
