﻿// Decompiled with JetBrains decompiler
// Type: SpeedupMenu
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class SpeedupMenu : UI.Dialog
{
  private List<TypedShopItemUI> m_Itemlist = new List<TypedShopItemUI>();
  private int freeLimitSecond = 300;
  private long m_JobId;
  public long marchId;
  public ItemBag.ItemType speedupType;
  public TimerType mTimerType;
  public string mActionName;
  public BuildingController mTarBC;
  public UILabel labelTitle;
  public UILabel labelBuildingName;
  public UILabel labelLvl;
  public UISlider sliderTime;
  public UISprite sliderForeNormal;
  public UISprite sliderForeFree;
  public UILabel labelTime;
  public UsedTips tip;
  public TypedShopItemUI template;
  public TypedShopItemUI freeItem;
  public UITable table;
  public UIScrollView scrollview;
  public UILabel goldVal;

  public long JobId
  {
    get
    {
      return this.m_JobId;
    }
    set
    {
      this.m_JobId = value;
    }
  }

  public bool CanFreeSpeedup()
  {
    if (this.mTimerType == TimerType.TIMER_NONE)
      return false;
    if (this.mTimerType != TimerType.TIMER_CONSTRUCT && this.mTimerType != TimerType.TIMER_UPGRADE)
      return this.mTimerType == TimerType.TIMER_DESTRUCT;
    return true;
  }

  public void OnSecond(int serverTime)
  {
    JobHandle job = JobManager.Instance.GetJob(this.m_JobId);
    int num1 = job != null ? job.LeftTime() : 0;
    int num2 = job != null ? job.Duration() : 0;
    this.sliderTime.value = (float) (num2 - num1) * 1f / (float) num2;
    this.labelTime.text = Utils.ConvertSecsToString((double) num1);
    if (this.mTimerType != TimerType.TIMER_NONE && num1 <= this.freeLimitSecond && (num1 > 0 && !this.freeItem.gameObject.activeInHierarchy) && this.CanFreeSpeedup())
    {
      this.freeItem.gameObject.SetActive(true);
      this.sliderForeFree.gameObject.SetActive(true);
      this.sliderForeFree.width = this.sliderForeNormal.width;
      this.sliderForeNormal.gameObject.SetActive(false);
      this.sliderTime.foregroundWidget = (UIWidget) this.sliderForeFree;
      this.table.repositionNow = true;
      this.scrollview.ResetPosition();
    }
    if (num1 > 1)
      return;
    if ((UnityEngine.Object) this.uiManagerPanel.BT_Back != (UnityEngine.Object) null && this.uiManagerPanel.BT_Back.gameObject.activeSelf)
      this.OnGoBackPressed();
    else
      this.OnClosePressed();
  }

  private void OnUserDataUpdate(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.goldVal.text = DBManager.inst.DB_User.Get(PlayerData.inst.uid).currency.gold.ToString();
  }

  private void OnDisable()
  {
    this.CleanItems();
    this.sliderForeFree.gameObject.SetActive(false);
    this.sliderForeNormal.gameObject.SetActive(true);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  private void FirstInsterItems()
  {
    List<ConsumableItemData> itemsNoInShop = ItemBag.Instance.GetItemsNoInShop(ItemBag.ItemType.speedup);
    for (int index = 0; index < itemsNoInShop.Count; ++index)
      this.AddItem((ShopStaticInfo) null, itemsNoInShop[index]);
  }

  private void InsertItems()
  {
    if (this.mTimerType != TimerType.TIMER_NONE && this.CanFreeSpeedup())
    {
      this.freeItem.onFreePressed += new System.Action(this.UseFree);
      this.freeItem.SetFree(this.freeLimitSecond);
    }
    this.freeItem.gameObject.SetActive(false);
    this.sliderTime.foregroundWidget = (UIWidget) this.sliderForeNormal;
    List<ShopStaticInfo> currentSaleShopItems = ItemBag.Instance.GetCurrentSaleShopItems("shop", string.Empty);
    for (int index = 0; index < currentSaleShopItems.Count; ++index)
      this.AddItem(currentSaleShopItems[index], (ConsumableItemData) null);
    this.m_Itemlist.Sort(new Comparison<TypedShopItemUI>(this.Compare));
    this.PushItemToTable();
    this.table.repositionNow = true;
    this.scrollview.ResetPosition();
  }

  private int Compare(TypedShopItemUI shopItem, TypedShopItemUI shopItemB)
  {
    return shopItem.ItemValue.CompareTo(shopItemB.ItemValue);
  }

  private void PushItemToTable()
  {
    for (int index = 0; index < this.m_Itemlist.Count; ++index)
    {
      TypedShopItemUI typedShopItemUi = this.m_Itemlist[index];
      typedShopItemUi.gameObject.transform.parent = this.table.gameObject.transform;
      typedShopItemUi.gameObject.transform.localPosition = Vector3.zero;
      typedShopItemUi.gameObject.transform.localRotation = Quaternion.identity;
      typedShopItemUi.gameObject.transform.localScale = Vector3.one;
      typedShopItemUi.gameObject.SetActive(true);
    }
  }

  private void AddItem(ShopStaticInfo shopInfo, ConsumableItemData itemData = null)
  {
    if (ConfigManager.inst.DB_Items.GetItem(shopInfo == null ? itemData.internalId : shopInfo.Item_InternalId).Type != this.speedupType)
      return;
    TypedShopItemUI component = NGUITools.AddChild(this.gameObject, this.template.gameObject).GetComponent<TypedShopItemUI>();
    component.info = shopInfo;
    component.itemData = itemData;
    component.SetNormal();
    if (this.speedupType == ItemBag.ItemType.marchspeed)
    {
      component.onUsePressed += new System.Action<TypedShopItemUI>(this.UseMarchSpeedItem);
      component.onBuyAndUsePressed += new System.Action<TypedShopItemUI>(this.BuyAndUseMarchSpeedItem);
    }
    else
    {
      component.onUsePressed += new System.Action<TypedShopItemUI>(this.UseItem);
      component.onBuyAndUsePressed += new System.Action<TypedShopItemUI>(this.BuyAndUseItem);
    }
    this.m_Itemlist.Add(component);
  }

  private void CleanItems()
  {
    if (this.CanFreeSpeedup())
    {
      this.freeItem.onFreePressed -= new System.Action(this.UseFree);
      this.freeItem.gameObject.SetActive(false);
    }
    for (int index = 0; index < this.m_Itemlist.Count; ++index)
      this.RemoveItem(this.m_Itemlist[index]);
    this.m_Itemlist.Clear();
    this.speedupType = ItemBag.ItemType.Invalid;
    this.mTimerType = TimerType.TIMER_NONE;
    this.labelBuildingName.gameObject.SetActive(false);
  }

  private void RemoveItem(TypedShopItemUI shopItem)
  {
    shopItem.onUsePressed -= new System.Action<TypedShopItemUI>(this.UseMarchSpeedItem);
    shopItem.onBuyAndUsePressed -= new System.Action<TypedShopItemUI>(this.BuyAndUseMarchSpeedItem);
    shopItem.onUsePressed -= new System.Action<TypedShopItemUI>(this.UseItem);
    shopItem.onBuyAndUsePressed -= new System.Action<TypedShopItemUI>(this.BuyAndUseItem);
    UnityEngine.Object.Destroy((UnityEngine.Object) shopItem.gameObject);
  }

  public void UseFree()
  {
    long job_id = this.m_JobId;
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.mTarBC)
    {
      if ((UnityEngine.Object) null != (UnityEngine.Object) this.mTarBC)
      {
        CityTimerbar componentInChildren = this.mTarBC.GetComponentInChildren<CityTimerbar>();
        if ((UnityEngine.Object) null != (UnityEngine.Object) componentInChildren)
          job_id = componentInChildren.FreeEndBuild();
      }
      if (job_id == 0L)
        return;
    }
    ItemBag.Instance.UseFreeSpeedup(job_id, (System.Action<bool, object>) ((_param1, _param2) => this.freeItem.Refresh()));
  }

  public void UseItem(TypedShopItemUI itemUI)
  {
    JobHandle job = JobManager.Instance.GetJob(this.m_JobId);
    if (job == null || job.LeftTime() <= 1)
      return;
    long num = job.ServerJobID;
    if (num == 0L)
      num = this.m_JobId;
    Hashtable extra = new Hashtable();
    extra[(object) "job_id"] = (object) num;
    extra[(object) "type"] = (object) this.speedupType.ToString();
    ItemBag.Instance.UseItem(itemUI.Item_ID, 1, extra, (System.Action<bool, object>) ((_param1, _param2) =>
    {
      if (ItemBag.Instance.GetItemCount(itemUI.Item_ID) == 0 && itemUI.info == null)
      {
        this.RemoveItem(itemUI);
        this.table.repositionNow = true;
        this.scrollview.ResetPosition();
        if (!this.m_Itemlist.Contains(itemUI))
          return;
        this.m_Itemlist.Remove(itemUI);
      }
      else
        itemUI.Refresh();
    }));
  }

  public void BuyAndUseItem(TypedShopItemUI itemUI)
  {
    if (itemUI.info == null)
      return;
    JobHandle job = JobManager.Instance.GetJob(this.m_JobId);
    if (job == null || job.LeftTime() <= 1)
      return;
    long num = job.ServerJobID;
    if (num == 0L)
      num = this.m_JobId;
    Hashtable extra = new Hashtable();
    extra[(object) "job_id"] = (object) num;
    extra[(object) "type"] = (object) this.speedupType.ToString();
    ItemBag.Instance.BuyAndUseShopItem(itemUI.info.internalId, 1, extra, (System.Action<bool, object>) ((_param1, _param2) =>
    {
      itemUI.Refresh();
      if (itemUI.info != null)
        this.PlayTipAnim(itemUI.transform.position, itemUI.info);
      else
        this.PlayTipAnim(itemUI.transform.position, itemUI.itemData);
    }));
  }

  public void UseMarchSpeedItem(TypedShopItemUI itemUI)
  {
    if (NetServerTime.inst.ServerTimestamp > DBManager.inst.DB_March.Get(this.marchId).endTime)
      return;
    ItemBag.Instance.UseItem(itemUI.Item_ID, 1, new Hashtable()
    {
      {
        (object) "march_id",
        (object) this.marchId
      },
      {
        (object) "march_owner_uid",
        (object) DBManager.inst.DB_March.Get(this.marchId).ownerUid
      },
      {
        (object) "march_type",
        (object) "pvp"
      }
    }, (System.Action<bool, object>) ((_param1, _param2) => itemUI.Refresh()));
  }

  public void BuyAndUseMarchSpeedItem(TypedShopItemUI itemUI)
  {
    if (itemUI.info == null)
      return;
    Hashtable data = JobManager.Instance.GetJob(this.m_JobId).Data as Hashtable;
    if (data == null || !data.ContainsKey((object) "marchId"))
      return;
    long id = (long) data[(object) "marchId"];
    if (NetServerTime.inst.ServerTimestamp > DBManager.inst.DB_March.Get(id).endTime)
      return;
    ItemBag.Instance.BuyAndUseShopItem(itemUI.info.internalId, 1, new Hashtable()
    {
      {
        (object) "march_id",
        (object) id
      },
      {
        (object) "march_owner_uid",
        (object) DBManager.inst.DB_March.Get(id).ownerUid
      },
      {
        (object) "march_type",
        (object) "pvp"
      }
    }, (System.Action<bool, object>) ((_param1, _param2) =>
    {
      itemUI.Refresh();
      this.PlayTipAnim(itemUI.transform.position, itemUI.info);
    }));
  }

  private void PlayTipAnim(Vector3 pos, ShopStaticInfo shopInfo)
  {
    Utils.DuplicateGOB(this.tip.gameObject).GetComponent<UsedTips>().SetContent(shopInfo != null ? shopInfo.LOC_ID : "free speedup", pos);
  }

  private void PlayTipAnim(Vector3 pos, ConsumableItemData itemData)
  {
    GameObject gameObject = Utils.DuplicateGOB(this.tip.gameObject);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemData.internalId);
    gameObject.GetComponent<UsedTips>().SetContent(itemStaticInfo != null ? itemStaticInfo.Loc_Name_Id : "free speedup", pos);
  }

  public void OnTipAnimEnd()
  {
  }

  public void OnBuyCoinPressed()
  {
    Utils.PopUpCommingSoon();
  }

  public void OnClosePressed()
  {
    this.CleanItems();
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnGoBackPressed()
  {
    this.CleanItems();
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  [DebuggerHidden]
  private IEnumerator DelayReposition(float dt)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpeedupMenu.\u003CDelayReposition\u003Ec__IteratorBE()
    {
      dt = dt,
      \u003C\u0024\u003Edt = dt,
      \u003C\u003Ef__this = this
    };
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdate);
    SpeedupMenu.Parameter parameter = orgParam as SpeedupMenu.Parameter;
    this.JobId = parameter.jobId;
    this.marchId = parameter.marchId;
    this.speedupType = parameter.speedupType;
    this.mTimerType = parameter.timerType;
    this.mActionName = parameter.actionName;
    this.mTarBC = parameter.tarBC;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.FirstInsterItems();
    this.InsertItems();
    if (this.mTimerType != TimerType.TIMER_NONE)
    {
      this.labelTitle.text = string.Format("SPEED UP {0}", (object) this.mActionName);
      this.OnUserDataUpdate(PlayerData.inst.uid);
    }
    this.StartCoroutine(this.DelayReposition(0.05f));
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdate);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public long jobId;
    public long marchId;
    public ItemBag.ItemType speedupType;
    public TimerType timerType;
    public string actionName;
    public BuildingController tarBC;
  }
}
