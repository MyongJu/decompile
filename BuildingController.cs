﻿// Decompiled with JetBrains decompiler
// Type: BuildingController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingController : MonoBehaviour
{
  public bool CanMove = true;
  public bool CanDeconstruct = true;
  public bool CanUpgrade = true;
  public bool CanTrainTroops = true;
  [HideInInspector]
  public long mBuildingID = -1;
  protected int _level = 1;
  private bool mValidPosition = true;
  public const string workingEffect = "fx_decoration_light";
  public const string updateFinishEffectNormal = "fx_jianzhushengji_small";
  public const string updateFinishEffectBig = "fx_jianzhushengji_big";
  public bool CanPVEFight;
  public bool CanResearch;
  public bool CanHospital;
  public bool CanRally;
  public bool CanReinforce;
  public bool CanTrade;
  public bool CanHero;
  public bool CanDefense;
  public bool CanWatchtower;
  public CityPlotController.PLOT_TYPE mZone;
  public GameObject mLevelNr;
  public GameObject mUpgradeAnim;
  public GameObject mWachtowerWarningAnim;
  public GameObject mCanUpgradeIcon;
  [HideInInspector]
  public CityManager.BuildingItem mBuildingItem;
  public BuildingDecoration mbuildingDecoration;
  private bool mPressed;
  private float _upgradeCheck;
  private int slotID;
  private int mLastValidSlotId;
  private static GameObject _mSelectedBuilding;
  private static CityManager.BuildingItem _mSelectedBuildingItem;
  private static BuildingController _mSelectedBuildingController;
  [HideInInspector]
  public static IEnumerator mGlowCR;
  private GameObject idleEffect;
  protected Dictionary<string, CityCircleBtnPara> CCBPair;

  public event BuildingController.OnBuildingSelected onBuildingSelected;

  public event BuildingController.OnRefreshCollider onRefreshCollider;

  public int SlotID
  {
    get
    {
      return this.slotID;
    }
  }

  [HideInInspector]
  public static GameObject mSelectedBuilding
  {
    get
    {
      return BuildingController._mSelectedBuilding;
    }
    set
    {
      BuildingController._mSelectedBuilding = value;
    }
  }

  [HideInInspector]
  public static CityManager.BuildingItem mSelectedBuildingItem
  {
    get
    {
      return BuildingController._mSelectedBuildingItem;
    }
    set
    {
      BuildingController._mSelectedBuildingItem = value;
    }
  }

  [HideInInspector]
  public static BuildingController mSelectedBuildingController
  {
    get
    {
      return BuildingController._mSelectedBuildingController;
    }
    set
    {
      BuildingController._mSelectedBuildingController = value;
    }
  }

  public bool IsBuildingGloryMenuShow
  {
    get
    {
      if (this.mBuildingItem == null)
        return false;
      List<BuildingGloryMainInfo> all = ConfigManager.inst.DB_BuildingGloryMain.GetBuildingGloryMainInfoList().FindAll((Predicate<BuildingGloryMainInfo>) (x => x.BuildingType == this.mBuildingItem.mType));
      return all != null && all.Count > 0 && PlayerData.inst.BuildingGloryOpen;
    }
  }

  private void Start()
  {
  }

  private void OnDestroy()
  {
  }

  public static void UnselectBuilding()
  {
    UIManager.inst.CloseCityTouchCircle();
    BuildingController.mSelectedBuilding = (GameObject) null;
    BuildingController.mSelectedBuildingController = (BuildingController) null;
    BuildingController.mSelectedBuildingItem = (CityManager.BuildingItem) null;
  }

  public void IncLevel()
  {
    ++this._level;
    this.SetLevel(this._level, true);
  }

  public void DecLevel()
  {
    if (this._level > 0)
      --this._level;
    this.SetLevel(this._level, true);
  }

  public virtual void SetLevel(int value, bool loadanim = true)
  {
  }

  public void SetMapLocation(int slotid)
  {
    this.mLastValidSlotId = this.slotID = slotid;
    CityPlotController.SetVisible(slotid, false);
    Vector3 plotPos = CityPlotController.GetPlotPos(slotid);
    if (Vector3.zero != plotPos)
      this.transform.position = plotPos;
    Vector3 localPosition = this.transform.localPosition;
    localPosition.z = localPosition.y;
    localPosition.z -= 2000f;
    this.transform.localPosition = localPosition;
  }

  public bool IsID(long ID)
  {
    if (this.mBuildingItem != null)
      return this.mBuildingItem.mID == ID;
    return false;
  }

  public virtual void RefreshStats()
  {
    if (this.mBuildingItem == null)
      return;
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.mUpgradeAnim)
    {
      if (this.mBuildingItem.mBuildingJobID == 0L)
      {
        this.mUpgradeAnim.SetActive(false);
      }
      else
      {
        JobHandle job = JobManager.Instance.GetJob(this.mBuildingItem.mBuildingJobID);
        if (job != null && !job.IsFinished())
          this.mUpgradeAnim.SetActive(true);
        else
          this.mUpgradeAnim.SetActive(false);
      }
    }
    this._level = this.mBuildingItem.mLevel;
    this.SetLevel(this._level, true);
  }

  private void Update()
  {
    if (!GameEngine.IsAvailable || ((double) (this._upgradeCheck += Time.deltaTime) <= 1.0 || this.mBuildingItem == null))
      return;
    --this._upgradeCheck;
    bool flag1 = BuildDlg.IsRequirementsOk(this.mBuildingItem.mType, this.mBuildingItem.mLevel + 1);
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData(this.mBuildingItem.mType, this.mBuildingItem.mLevel + 1);
    bool flag2 = false;
    if (data != null)
      flag2 = !BuildQueueManager.Instance.IsBuildQueueFull((double) ConfigManager.inst.DB_BenefitCalc.GetFinalData((int) data.Build_Time, "calc_construction_time"));
    if (this.CanShowDecoration())
      this.ShowDecoration();
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.mCanUpgradeIcon)
      this.mCanUpgradeIcon.SetActive(flag1 && flag2);
    if (!this.IsWorkBuilding())
      return;
    if (this.IsWorking())
    {
      this.ShowWorkingEffect(true);
      if (!((UnityEngine.Object) null != (UnityEngine.Object) this.transform.FindChild("fx_jianzhukongxian")) || !((UnityEngine.Object) null != (UnityEngine.Object) this.idleEffect))
        return;
      PrefabManagerEx.Instance.Destroy(this.idleEffect);
    }
    else if (this.IsBuilding())
    {
      this.ShowWorkingEffect(false);
      if (!((UnityEngine.Object) null != (UnityEngine.Object) this.transform.FindChild("fx_jianzhukongxian")) || !((UnityEngine.Object) null != (UnityEngine.Object) this.idleEffect))
        return;
      PrefabManagerEx.Instance.Destroy(this.idleEffect);
    }
    else
    {
      this.ShowWorkingEffect(false);
      if (!((UnityEngine.Object) null == (UnityEngine.Object) this.transform.FindChild("fx_jianzhukongxian")) || !(this.mBuildingItem.mType != "fortress"))
        return;
      this.idleEffect = PrefabManagerEx.Instance.Spawn(CitadelSystem.inst.idleEffectGOB, this.transform);
    }
  }

  private void ShowWorkingEffect(bool show)
  {
    GameObject child = Utils.FindChild(this.gameObject, "fx_decoration_light");
    if (!((UnityEngine.Object) null != (UnityEngine.Object) child))
      return;
    child.SetActive(show);
  }

  private bool CanShowDecoration()
  {
    return this.mBuildingItem.mType == "stronghold";
  }

  private void ShowDecoration()
  {
    if (!((UnityEngine.Object) this.mbuildingDecoration != (UnityEngine.Object) null) || !GameEngine.IsReady())
      return;
    this.mbuildingDecoration.UpdateUI(PlayerData.inst.playerCityData.decoration, 1f);
  }

  public void OnClick()
  {
    if (GameDataManager.inst.IsBuildingLocked() && !GameEngine.IsAvailable)
      return;
    BuildingController.UnselectBuilding();
    BuildingController.mSelectedBuilding = this.gameObject;
    BuildingController.mSelectedBuildingItem = this.mBuildingItem;
    BuildingController.mSelectedBuildingController = this;
    if (this.onBuildingSelected != null)
      this.onBuildingSelected();
    LinkerHub.Instance.DisposeHint(this.transform);
    if (!(bool) ((UnityEngine.Object) BuildingController.mSelectedBuilding))
      return;
    OperationTrace.TraceClickCityBuilding(BuildingController.mSelectedBuilding.name);
  }

  public virtual void AddActionButton(ref List<CityCircleBtnPara> ftl)
  {
    bool flag1 = this.mBuildingItem.mLevel < ConfigManager.inst._DB_Building.GetMaxLevelByType(this.mBuildingItem.mType);
    bool flag2 = this.mBuildingItem.mLevel > 0;
    bool flag3 = false;
    long mBuildingJobId1 = this.mBuildingItem.mBuildingJobID;
    int time = 0;
    if (mBuildingJobId1 > 0L)
    {
      JobHandle unfinishedJob = JobManager.Instance.GetUnfinishedJob(mBuildingJobId1);
      if (unfinishedJob != null)
      {
        time = unfinishedJob.LeftTime() - CityManager.inst.GetFreeSpeedUpTime();
        flag3 = true;
        if (unfinishedJob.GetJobEvent() == JobEvent.JOB_BUILDING_DECONSTRUCTION)
          flag2 = false;
      }
    }
    bool flag4 = this.IsTraining();
    if (flag4)
    {
      long mTrainingJobId = this.mBuildingItem.mTrainingJobID;
      if (mTrainingJobId > 0L)
      {
        JobHandle unfinishedJob = JobManager.Instance.GetUnfinishedJob(mTrainingJobId);
        if (unfinishedJob != null)
          time = unfinishedJob.LeftTime();
      }
    }
    ItemBag.CalculateCost(time, 0);
    bool flag5 = ResearchManager.inst.CurrentResearch != null && this.mBuildingItem.mType == "university";
    if (flag5)
    {
      JobHandle job = JobManager.Instance.GetJob(ResearchManager.inst.GetJobId(ResearchManager.inst.CurrentResearch));
      time = job != null ? job.LeftTime() : 0;
    }
    bool flag6 = flag3 || flag4 || flag5;
    this.CCBPair = CityCircleBtnCollection.Instance.CircleBtnPair;
    ftl.Add(this.CCBPair["buildinginfo"]);
    this.CCBPair["buildinginfo"].isActive = true;
    if (flag6)
    {
      if (this.mBuildingItem.mType == "dragon_lair")
      {
        if (PlayerData.inst.dragonData != null)
        {
          CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "gold_speed_up", "id_gold_speedup", new EventDelegate((MonoBehaviour) CityTouchCircle.Instance, "OnUseGoldSpeedUpBtnClicked"), true, (string) null, AssetManager.Instance.HandyLoad("Prefab/City/GoldBoostResAttachment", (System.Type) null) as GameObject, (CityTouchCircleBtnAttachment.AttachmentData) new GoldBoostResAttachment.GBRAData()
          {
            remainTime = time
          }, false);
          ftl.Add(this.CCBPair["actionSpeedup"]);
          if (time > 0)
            ftl.Add(cityCircleBtnPara);
        }
        else
        {
          long mBuildingJobId2 = this.mBuildingItem.mBuildingJobID;
          if (mBuildingJobId2 > 0L && JobManager.Instance.GetUnfinishedJob(mBuildingJobId2) != null)
          {
            flag1 = false;
            CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "gold_speed_up", "id_gold_speedup", new EventDelegate((MonoBehaviour) CityTouchCircle.Instance, "OnUseGoldSpeedUpBtnClicked"), true, (string) null, AssetManager.Instance.HandyLoad("Prefab/City/GoldBoostResAttachment", (System.Type) null) as GameObject, (CityTouchCircleBtnAttachment.AttachmentData) new GoldBoostResAttachment.GBRAData()
            {
              remainTime = time
            }, false);
            ftl.Add(this.CCBPair["actionSpeedup"]);
            if (time > 0)
              ftl.Add(cityCircleBtnPara);
          }
          if (flag1)
          {
            ftl.Add(this.CCBPair["buildingupgrade"]);
            this.CCBPair["buildingupgrade"].isActive = true;
          }
        }
      }
      else
      {
        CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "gold_speed_up", "id_gold_speedup", new EventDelegate((MonoBehaviour) CityTouchCircle.Instance, "OnUseGoldSpeedUpBtnClicked"), true, (string) null, AssetManager.Instance.HandyLoad("Prefab/City/GoldBoostResAttachment", (System.Type) null) as GameObject, (CityTouchCircleBtnAttachment.AttachmentData) new GoldBoostResAttachment.GBRAData()
        {
          remainTime = time
        }, false);
        ftl.Add(this.CCBPair["actionSpeedup"]);
        if (time > 0)
          ftl.Add(cityCircleBtnPara);
      }
    }
    else if (flag1)
    {
      ftl.Add(this.CCBPair["buildingupgrade"]);
      this.CCBPair["buildingupgrade"].isActive = true;
    }
    if ("stronghold" == this.mBuildingItem.mType)
    {
      ftl.Add(this.CCBPair["cityinfo"]);
      ftl.Add(this.CCBPair["decoration"]);
      ftl.Add(this.CCBPair["cityBoost"]);
    }
    else if (this.CanTrainTroops)
    {
      if (!flag2)
        return;
      if ("fortress" == this.mBuildingItem.mType)
        ftl.Add(this.CCBPair["buildTraps"]);
      else
        ftl.Add(this.CCBPair["trainTroops"]);
    }
    else if (this.CanRally)
    {
      if (!flag2)
        return;
      ftl.Add(this.CCBPair["buildingrally"]);
    }
    else if (this.CanTrade)
    {
      if (!flag2)
        return;
      ftl.Add(this.CCBPair["buildingtrade"]);
    }
    else
    {
      if (!this.CanWatchtower || !flag2)
        return;
      ftl.Add(this.CCBPair["watchtower"]);
    }
  }

  public virtual void AddAdditionActionButton(ref List<CityCircleBtnPara> ftl)
  {
    if (this.mBuildingItem == null)
      return;
    List<BuildingGloryMainInfo> mainInfoList = ConfigManager.inst.DB_BuildingGloryMain.GetBuildingGloryMainInfoList();
    mainInfoList = mainInfoList.FindAll((Predicate<BuildingGloryMainInfo>) (x => x.BuildingType == this.mBuildingItem.mType));
    if (mainInfoList == null || mainInfoList.Count <= 0 || !PlayerData.inst.BuildingGloryOpen)
      return;
    mainInfoList.Sort((Comparison<BuildingGloryMainInfo>) ((a, b) => a.level.CompareTo(b.level)));
    bool showRedPoint = false;
    if (this.mBuildingItem.mGloryLevel >= 0 && this.mBuildingItem.mGloryLevel < mainInfoList.Count && mainInfoList[this.mBuildingItem.mGloryLevel].NextBuildingGloryMainInfo != null)
    {
      int num;
      if (this.mBuildingItem.mLevel >= mainInfoList[this.mBuildingItem.mGloryLevel].NextBuildingGloryMainInfo.ReqBuildingMinLevel)
        num = PlayerPrefsEx.GetIntByUid("building_glory_upgrade_" + (object) this.mBuildingItem.mBuildingId + "_" + (object) this.mBuildingItem.mLevel, 0) != 1 ? 1 : 0;
      else
        num = 0;
      showRedPoint = num != 0;
    }
    CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_glory_upgrade", "id_prestige_level", new EventDelegate((EventDelegate.Callback) (() =>
    {
      if (this.mBuildingItem.mLevel < mainInfoList[0].ReqBuildingMinLevel)
        UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_prestige_insufficient_level", new Dictionary<string, string>()
        {
          {
            "0",
            mainInfoList[0].ReqBuildingMinLevel.ToString()
          }
        }, true), (System.Action) null, 4f, true);
      else
        this.OnUpgradeGloryClicked();
    })), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, showRedPoint);
    ftl.Add(cityCircleBtnPara);
  }

  internal void OnUpgradeGloryClicked()
  {
    UIManager.inst.OpenDlg("BuildingGlory/BuildingGloryUpgradeDlg", (UI.Dialog.DialogParameter) new BuildingGloryUpgradeDlg.Parameter()
    {
      buildingItem = this.mBuildingItem
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void SelectBuilding()
  {
    BuildingController.UnselectBuilding();
    BuildingController.mSelectedBuilding = this.gameObject;
    BuildingController.mSelectedBuildingItem = this.mBuildingItem;
    BuildingController.mSelectedBuildingController = this;
  }

  public void ShowUpgradeAnim()
  {
    if (!((UnityEngine.Object) this.mUpgradeAnim != (UnityEngine.Object) null))
      return;
    this.mUpgradeAnim.SetActive(true);
  }

  internal void HideUpgradeAnim()
  {
    if (!((UnityEngine.Object) this.mUpgradeAnim != (UnityEngine.Object) null))
      return;
    this.mUpgradeAnim.SetActive(false);
  }

  public void ShowWatchTowerWarningAnim()
  {
    if (!((UnityEngine.Object) this.mWachtowerWarningAnim != (UnityEngine.Object) null))
      return;
    this.mWachtowerWarningAnim.SetActive(true);
  }

  public void UpgradeFinished(bool isUpgrade)
  {
    if ((UnityEngine.Object) this.mUpgradeAnim != (UnityEngine.Object) null)
      this.mUpgradeAnim.SetActive(false);
    if (this.mBuildingItem != null)
    {
      this._level = this.mBuildingItem.mLevel;
      this.SetLevel(this._level, true);
    }
    if (!isUpgrade)
      return;
    AudioManager.Instance.PlaySound("sfx_building_complete", false);
    if ("stronghold" == this.mBuildingItem.mType)
    {
      VfxManager.Instance.CreateAndPlay("Prefab/VFX/fx_jianzhushengji_big", this.transform);
      if (this._level == 15)
        TrackEvent.Instance.Trace("stronghold_level_15", TrackEvent.Type.Facebook);
      if (this._level == 10)
        TrackEvent.Instance.Trace("stronghold_level_10", TrackEvent.Type.Adjust);
      if (this._level == 4)
        TrackEvent.Instance.Trace("stronghold_level_4", TrackEvent.Type.Adjust);
      if (this._level == 5)
        TrackEvent.Instance.Trace("stronghold_level_5", TrackEvent.Type.Adjust);
      TrackEvent.Instance.TraceEventToFB("Stronghold Level", "level", this._level.ToString());
      AccountManager.Instance.ShowRecommendReview();
    }
    else
      VfxManager.Instance.CreateAndPlay("Prefab/VFX/fx_jianzhushengji_small", this.transform);
    NewTutorial.Instance.CheckTrigger(this);
    if (!("walls" == this.mBuildingItem.mType) || this._level != 5)
      return;
    AccountManager.Instance.ShowBindPop();
  }

  public bool IsWorking()
  {
    if (this.mBuildingItem == null)
      return false;
    return this.IsTraining() || ResearchManager.inst.CurrentResearch != null && this.mBuildingItem.mType == "university";
  }

  public bool IsWorkBuilding()
  {
    return this.mBuildingItem != null && ("barracks" == this.mBuildingItem.mType || "sanctum" == this.mBuildingItem.mType || ("range" == this.mBuildingItem.mType || "stables" == this.mBuildingItem.mType) || ("workshop" == this.mBuildingItem.mType || "university" == this.mBuildingItem.mType || ("forge" == this.mBuildingItem.mType || "fortress" == this.mBuildingItem.mType)));
  }

  public bool IsTraining()
  {
    if (this.mBuildingItem == null || this is BuildingControllerResource)
      return false;
    bool flag = false;
    long mTrainingJobId = this.mBuildingItem.mTrainingJobID;
    if (mTrainingJobId > 0L && JobManager.Instance.GetUnfinishedJob(mTrainingJobId) != null)
      flag = true;
    return flag;
  }

  public bool IsBuilding()
  {
    if (this.mBuildingItem == null)
      return false;
    bool flag = false;
    long mBuildingJobId = this.mBuildingItem.mBuildingJobID;
    if (mBuildingJobId > 0L && JobManager.Instance.GetUnfinishedJob(mBuildingJobId) != null)
      flag = true;
    return flag;
  }

  public virtual void OnChanged()
  {
  }

  public void SwapBuilding(BuildingController bc)
  {
    MessageHub.inst.GetPortByAction("City:moveObject").SendRequest(Utils.Hash((object) "building_id", (object) this.mBuildingID, (object) "slot_id", (object) bc.slotID), (System.Action<bool, object>) ((_param1, _param2) =>
    {
      int slotId = this.slotID;
      this.SetMapLocation(bc.slotID);
      bc.SetMapLocation(slotId);
      VfxManager.Instance.CreateAndPlay("Prefab/VFX/fx_jianzhushengji_small", this.transform);
      VfxManager.Instance.CreateAndPlay("Prefab/VFX/fx_jianzhushengji_small", bc.transform);
    }), true);
  }

  [Serializable]
  public class BuildingStage
  {
    public float mScale = 1.2f;
    public UITexture mObject;
  }

  public delegate void OnBuildingSelected();

  public delegate void OnRefreshCollider();
}
