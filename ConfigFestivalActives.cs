﻿// Decompiled with JetBrains decompiler
// Type: ConfigFestivalActives
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigFestivalActives
{
  private Dictionary<string, FestivalInfo> datas;
  private Dictionary<int, FestivalInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<FestivalInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    Dictionary<string, FestivalInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      enumerator.Current.innerItems[0] = enumerator.Current.innerItemId1;
      enumerator.Current.innerItems[1] = enumerator.Current.innerItemId2;
      enumerator.Current.innerItems[2] = enumerator.Current.innerItemId3;
      enumerator.Current.innerItems[3] = enumerator.Current.innerItemId4;
    }
  }

  public void Clear()
  {
    if (this.datas != null)
      this.datas.Clear();
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
  }

  public FestivalInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (FestivalInfo) null;
  }

  public FestivalInfo GetData(int internalId)
  {
    if (this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (FestivalInfo) null;
  }
}
