﻿// Decompiled with JetBrains decompiler
// Type: OpeningCinematicManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;
using UnityEngine;

public class OpeningCinematicManager : MonoBehaviour
{
  private OpeningCinematicManager.ResolutionType resolution = OpeningCinematicManager.ResolutionType.Basic;
  public const string ockey = "openingcinematic";
  private const string boardpath = "Prefab/OpeningCinematic/OpeningCinematicBaseboard";
  private const string terrainpath = "Prefab/OpeningCinematic/OpeningTerrain";
  private const string vfxpath = "Prefab/OpeningCinematic/OpeningVFXRoot";
  private const string citypath = "Prefab/OpeningCinematic/OpeningCity";
  private const string skippath = "Prefab/OpeningCinematic/OpeningSkip";
  private static OpeningCinematicManager _instance;
  private bool isRunning;
  public OpeningCinematicController board;
  private GameObject skipBtn;
  public System.Action onOpeningCinematicTerminate;

  public static OpeningCinematicManager Instance
  {
    get
    {
      if ((UnityEngine.Object) OpeningCinematicManager._instance == (UnityEngine.Object) null)
      {
        OpeningCinematicManager._instance = UnityEngine.Object.FindObjectOfType<OpeningCinematicManager>();
        if ((UnityEngine.Object) null == (UnityEngine.Object) OpeningCinematicManager._instance)
          throw new ArgumentException("OpeningCinematicManager hasn't been created yet.");
      }
      return OpeningCinematicManager._instance;
    }
  }

  public bool IsRunning
  {
    get
    {
      return this.isRunning;
    }
  }

  public OpeningCinematicManager.ResolutionType Resolution
  {
    get
    {
      return this.resolution;
    }
    set
    {
      this.resolution = value;
    }
  }

  public void Startup()
  {
    this.isRunning = true;
    BundleManager.Instance.LoadBasicBundle("static+troops");
    BundleManager.Instance.LoadBasicBundle("static+kingdom_tiles");
    BundleManager.Instance.LoadBasicBundle("static+kingdom_buildings");
    if (this.Resolution == OpeningCinematicManager.ResolutionType.Basic || this.Resolution == OpeningCinematicManager.ResolutionType.NoRally)
      this.PlayOpeningCinematic();
    else
      this.Terminate();
  }

  public void Terminate()
  {
    this.isRunning = false;
    PlayerPrefsEx.SetInt("openingcinematic", 1);
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.board)
    {
      this.board.Dispose();
      UnityEngine.Object.Destroy((UnityEngine.Object) this.board.gameObject);
    }
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.skipBtn)
    {
      this.skipBtn.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.skipBtn);
    }
    Transform transform = UIManager.inst.ui2DCamera.transform;
    int childCount = transform.childCount;
    for (int index = 0; index < childCount; ++index)
    {
      Transform child = transform.GetChild(index);
      if (child.name.Contains("Opening"))
      {
        child.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) child.gameObject);
      }
    }
    if (this.onOpeningCinematicTerminate != null)
      this.onOpeningCinematicTerminate();
    UIManager.inst.splashScreen.gameObject.SetActive(true);
  }

  public void PlayOpeningCinematic()
  {
    bool flag = true;
    if (!Application.isEditor)
      flag = !PlayerPrefsEx.HasKey("openingcinematic");
    if (flag)
    {
      AudioManager.Instance.PlayBGM("bgm_loading2");
      AssetManager.Instance.LoadAsync("Prefab/OpeningCinematic/OpeningCinematicBaseboard", (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
      {
        GameObject go = UnityEngine.Object.Instantiate(obj) as GameObject;
        AssetManager.Instance.UnLoadAsset("Prefab/OpeningCinematic/OpeningCinematicBaseboard", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
        this.board = go.GetComponent<OpeningCinematicController>();
        if (!SettingManager.Instance.IsEnvironmentOff)
        {
          AssetManager.Instance.LoadAsync("Prefab/OpeningCinematic/OpeningTerrain", (System.Action<UnityEngine.Object, bool>) ((objterrain, retterrain) =>
          {
            Utils.DuplicateGOB(objterrain as GameObject, go.transform).transform.localScale = Vector3.one;
            AssetManager.Instance.UnLoadAsset("Prefab/OpeningCinematic/OpeningTerrain", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
          }), (System.Type) null);
          AssetManager.Instance.LoadAsync("Prefab/OpeningCinematic/OpeningVFXRoot", (System.Action<UnityEngine.Object, bool>) ((objvfx, retvfx) =>
          {
            Utils.DuplicateGOB(objvfx as GameObject, go.transform).transform.localScale = new Vector3(100f, 100f, 1f);
            AssetManager.Instance.UnLoadAsset("Prefab/OpeningCinematic/OpeningVFXRoot", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
          }), (System.Type) null);
        }
        AssetManager.Instance.LoadAsync("Prefab/OpeningCinematic/OpeningCity", (System.Action<UnityEngine.Object, bool>) ((objcity, retcity) =>
        {
          GameObject gameObject = Utils.DuplicateGOB(objcity as GameObject, go.transform);
          gameObject.transform.localScale = Vector3.one;
          this.board.odc = gameObject.GetComponentInChildren<OpeningDragonController>();
          UIManager.inst.splashScreen.gameObject.SetActive(false);
          this.board.PlayAction(1);
          AssetManager.Instance.UnLoadAsset("Prefab/OpeningCinematic/OpeningCity", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
        }), (System.Type) null);
        AssetManager.Instance.LoadAsync("Prefab/OpeningCinematic/OpeningSkip", (System.Action<UnityEngine.Object, bool>) ((objskip, retskip) =>
        {
          this.skipBtn = Utils.DuplicateGOB(objskip as GameObject, UIManager.inst.ui2DOverlayCamera.transform);
          AssetManager.Instance.UnLoadAsset("Prefab/OpeningCinematic/OpeningSkip", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
        }), (System.Type) null);
      }), (System.Type) null);
    }
    else
      this.Terminate();
  }

  public enum ResolutionType
  {
    NoCinematic,
    Basic,
    NoRally,
  }
}
