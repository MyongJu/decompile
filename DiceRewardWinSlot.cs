﻿// Decompiled with JetBrains decompiler
// Type: DiceRewardWinSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class DiceRewardWinSlot : MonoBehaviour
{
  private const string MULTI_PREFIX = "x";
  [SerializeField]
  private ItemIconRenderer _itemIconRenderer;
  [SerializeField]
  private UILabel _labelRewardCount;
  [SerializeField]
  private UILabel _labelRewardScore;
  [SerializeField]
  private UILabel _labelRewardMultiplier;
  [SerializeField]
  private GameObject _diceWinEffect;
  private DiceData.RewardData _rewardData;
  private bool _winMultiAnimPlayed;
  private bool _winMultiAnimForceStopped;

  public bool WinMultiAnimPlayed
  {
    get
    {
      return this._winMultiAnimPlayed;
    }
  }

  public void SetData(DiceData.RewardData rewardData)
  {
    this._rewardData = rewardData;
    this.UpdateUI();
  }

  public void StartMultiWinAnim()
  {
    this._winMultiAnimPlayed = true;
    this.StartCoroutine("PlayMultiWinAnim");
    NGUITools.SetActive(this._diceWinEffect, true);
  }

  public void StopMultiWinAnim()
  {
    this._winMultiAnimForceStopped = true;
    this.StopCoroutine("PlayMultiWinAnim");
    if (this._rewardData == null)
      return;
    this._labelRewardMultiplier.text = "x" + this._rewardData.Multiple.ToString();
  }

  [DebuggerHidden]
  private IEnumerator PlayMultiWinAnim()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DiceRewardWinSlot.\u003CPlayMultiWinAnim\u003Ec__Iterator4E()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void UpdateUI()
  {
    if (this._rewardData == null)
      return;
    if (this._rewardData.IsRewardScore)
    {
      this._labelRewardScore.text = this._rewardData.Amount.ToString();
    }
    else
    {
      if (this._rewardData.ItemId > 0)
        this._itemIconRenderer.SetData(this._rewardData.ItemId, string.Empty, true);
      this._labelRewardCount.text = this._rewardData.Amount.ToString();
      this._labelRewardScore.text = string.Empty;
    }
    this._labelRewardMultiplier.text = string.Empty;
    NGUITools.SetActive(this._labelRewardCount.transform.parent.gameObject, !this._rewardData.IsRewardScore);
    NGUITools.SetActive(this._labelRewardScore.gameObject, this._rewardData.IsRewardScore);
  }
}
