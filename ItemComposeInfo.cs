﻿// Decompiled with JetBrains decompiler
// Type: ItemComposeInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class ItemComposeInfo
{
  public long internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "compose_result_item_id")]
  public long compose_result_item_id;
  [Config(Name = "compose_result_item_value")]
  public int compose_result_item_value;
  [Config(CustomParse = true, Name = "ComposeItems", ParseFuncKey = "ParseComposeItems")]
  public Dictionary<long, int> ComposeItems;
}
