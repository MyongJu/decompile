﻿// Decompiled with JetBrains decompiler
// Type: ConfigRallySlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class ConfigRallySlot
{
  private int[] itemRequireInternal = new int[101];
  private int[] itemRequireCount = new int[101];
  public const int MAX_SLOT_NUM = 101;
  public const int INVALID_ID = 0;
  private const string PRE_ID = "rally_slot_";

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "{0} load error: res is not a hashtable", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "{0} load error: headers not a hashtable", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "req_id_1");
        int index3 = ConfigManager.GetIndex(inHeader, "req_value_1");
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          ArrayList arr = hashtable[key] as ArrayList;
          int result;
          if (arr != null && int.TryParse(ConfigManager.GetString(arr, index1).Remove(0, "rally_slot_".Length), out result))
          {
            this.itemRequireInternal[result] = ConfigManager.GetInt(arr, index2);
            this.itemRequireCount[result] = ConfigManager.GetInt(arr, index3);
          }
        }
      }
    }
  }

  public int GetItemRequrieInternal(int level)
  {
    if (level < 100)
      return this.itemRequireInternal[level];
    return 0;
  }

  public int GetItemRequireCount(int level)
  {
    if (level < 100)
      return this.itemRequireCount[level];
    return 0;
  }
}
