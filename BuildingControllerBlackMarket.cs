﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerBlackMarket
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingControllerBlackMarket : BuildingControllerNew
{
  private HubPort loadItem = MessageHub.inst.GetPortByAction("npcstore:loadGoods");
  private const string path = "Prefab/City/InfoBoard";
  [SerializeField]
  private GameObject _hasOverdueGoods;
  [SerializeField]
  private GameObject _hasWorldAuctionGoods;
  public Transform boardRoot;
  private GameObject infoBoard;

  public void OnBulidngClick()
  {
    if (!Application.isEditor)
      FunplusBi.Instance.TraceEvent("clickbuilding", JsonWriter.Serialize((object) new BuildingControllerNew.BIFormat()
      {
        d_c1 = new BuildingControllerNew.BIClick()
        {
          key = "npcstore",
          value = "click"
        }
      }));
    UIManager.inst.OpenDlg("NpcStoreDialog", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OpenMagicStoreDlg()
  {
    UIManager.inst.OpenDlg("MagicStoreDialog", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public override void AddActionButton(ref List<CityCircleBtnPara> ccbp)
  {
    AudioManager.Instance.PlaySound("sfx_city_click_trading_post", false);
    CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_market_place", "id_marketplace", new EventDelegate((EventDelegate.Callback) (() => this.OnBulidngClick())), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ccbp.Add(cityCircleBtnPara1);
    CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_magic_store", "id_magic_store", new EventDelegate((EventDelegate.Callback) (() => this.OpenMagicStoreDlg())), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ccbp.Add(cityCircleBtnPara2);
    CityCircleBtnPara cityCircleBtnPara3 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_auction_house", "id_auction", new EventDelegate((EventDelegate.Callback) (() => AuctionHelper.Instance.ShowAuctionScene(AuctionHelper.AuctionType.PUBLIC_AUCTION))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ccbp.Add(cityCircleBtnPara3);
    CityCircleBtnPara cityCircleBtnPara4 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_black_market", "id_black_market", new EventDelegate((EventDelegate.Callback) (() => AuctionHelper.Instance.ShowAuctionScene(AuctionHelper.AuctionType.NON_PUBLIC_AUCTION))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ccbp.Add(cityCircleBtnPara4);
    if (PlayerData.inst.ExchangeSwitch)
    {
      CityCircleBtnPara cityCircleBtnPara5 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_trading_hall", "id_exchange_hall", new EventDelegate((EventDelegate.Callback) (() =>
      {
        if (TradingHallPayload.Instance.ShouldShowTradingHall)
          TradingHallPayload.Instance.ShowFirstScene(0);
        else
          UIManager.inst.toast.Show(Utils.XLAT("toast_exchange_hall_not_open"), (System.Action) null, 4f, true);
      })), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
      ccbp.Add(cityCircleBtnPara5);
    }
    CityTouchCircle.Instance.Title = Utils.XLAT("exchange_name");
  }

  public override void InitBuilding()
  {
    base.InitBuilding();
    this.onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
    if (!((UnityEngine.Object) null == (UnityEngine.Object) this.infoBoard))
      return;
    AssetManager.Instance.LoadAsync("Prefab/City/InfoBoard", (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      this.infoBoard = Utils.DuplicateGOB(obj as GameObject, this.boardRoot);
      this.infoBoard.GetComponent<InfoBoard>().Init("trading_post_scrolling_title");
      AssetManager.Instance.UnLoadAsset("Prefab/City/InfoBoard", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    }), (System.Type) null);
  }

  public override void Dispose()
  {
    base.Dispose();
    this.onBuildingSelected -= new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.infoBoard))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.infoBoard);
    this.infoBoard = (GameObject) null;
  }

  public void OnTradingHallClicked()
  {
    TradingHallPayload.Instance.ShowFirstScene(1);
  }

  public void OnWorldAuctionClicked()
  {
    AuctionHelper.Instance.ShowAuctionScene(AuctionHelper.AuctionType.WORLD_AUCTION);
  }

  private void OnSecondEvent(int time)
  {
    this.ShowBuildingTipInfo();
  }

  private void ShowBuildingTipInfo()
  {
    NGUITools.SetActive(this._hasOverdueGoods, false);
    NGUITools.SetActive(this._hasWorldAuctionGoods, false);
    if (!PlayerData.inst.ExchangeSwitch || !TradingHallPayload.Instance.ShouldShowTradingHall || TradingHallPayload.Instance.GoodOverdueCount <= 0)
      return;
    NGUITools.SetActive(this._hasOverdueGoods, true);
  }

  private void OnBuildingSelected()
  {
    CitadelSystem.inst.OpenTouchCircle();
  }
}
