﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerSettingPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;
using UnityEngine;

public class MerlinTowerSettingPopup : Popup
{
  [SerializeField]
  private UIButton _buttonReset;
  [SerializeField]
  private UIButton _buttonResetDiabled;
  protected bool _resetResponse;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._resetResponse = false;
    RequestManager.inst.AddObserver("magicTower:reset", new System.Action<string, bool>(this.OnResetResponse));
    UserMerlinTowerData userData = MerlinTowerPayload.Instance.UserData;
    if (userData == null)
      return;
    bool flag = userData.mineJobId != 0L && userData.CurrentTowerMainInfo.NoviceProtection <= 0;
    this._buttonResetDiabled.gameObject.SetActive(flag);
    this._buttonReset.gameObject.SetActive(!flag);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    RequestManager.inst.RemoveObserver("magicTower:reset", new System.Action<string, bool>(this.OnResetResponse));
  }

  protected void OnResetResponse(string portType, bool result)
  {
    if (!result)
      return;
    this._resetResponse = true;
  }

  public void Update()
  {
    if (!this._resetResponse)
      return;
    this._resetResponse = false;
    MerlinTowerFacade.RequestExit();
  }

  public void OnButtonLeaveClicked()
  {
    Logger.Log("OnButtonCloseClicked");
    MerlinTowerFacade.RequestExit();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnButtonResetClicked()
  {
    Logger.Log("OnButtonCloseClicked");
    UserMerlinTowerData userData = MerlinTowerPayload.Instance.UserData;
    if (userData == null)
      return;
    if (userData.mineJobId != 0L && userData.CurrentTowerMainInfo.NoviceProtection <= 0)
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_tower_mine_cannot_reset", true), (System.Action) null, 4f, true);
    else
      UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
      {
        title = ScriptLocalization.Get("tower_reset_confirm_title", true),
        content = ScriptLocalization.Get("tower_reset_description", true),
        yes = ScriptLocalization.Get("id_uppercase_confirm", true),
        no = ScriptLocalization.Get("id_uppercase_cancel", true),
        yesCallback = (System.Action) (() => MerlinTowerPayload.Instance.SendResetRequest()),
        noCallback = (System.Action) (() => {})
      });
  }

  public void OnButtonCloseClicked()
  {
    Logger.Log(nameof (OnButtonCloseClicked));
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }
}
