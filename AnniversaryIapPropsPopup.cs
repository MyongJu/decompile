﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryIapPropsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;

public class AnniversaryIapPropsPopup : Popup
{
  public UILabel freeRewardHint;
  public UILabel totalTopupGoldAmount;
  public UILabel collectedItemCount;
  public UILabel remainedItemCount;
  public UIButton topupButton;
  public UIButton topupClaimButton;
  public UITexture freeRewardTexture;
  private long _remainedClaim;
  private System.Action _freeClaimedHandler;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    AnniversaryIapPropsPopup.Parameter parameter = orgParam as AnniversaryIapPropsPopup.Parameter;
    if (parameter != null)
      this._freeClaimedHandler = parameter.freeClaimedHandler;
    this.UpdateUI();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnTopupBtnPressed()
  {
    this.OnCloseBtnPressed();
    if (UIManager.inst.IsDlgExist("Anniversary/AnniversaryIapMainDlg"))
      return;
    AnniversaryIapPayload.Instance.RequestServerData((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("Anniversary/AnniversaryIapMainDlg", (UI.Dialog.DialogParameter) new AnniversaryIapMainDlg.Parameter()
      {
        showCloseButton = false
      }, true, true, true);
    }));
  }

  public void OnTopupClaimBtnPressed()
  {
    this.CollectReward();
  }

  private void CollectReward()
  {
    long propsCount = AnniversaryIapPayload.Instance.AnnvIapData.CanReceivePropsNum;
    AnniversaryIapPayload.Instance.AnnvClaimProps((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.UpdateUI();
      DicePayload.Instance.ShowCollectEffect(AnniversaryIapPayload.Instance.NeedPropsId, (int) propsCount);
      if (this._freeClaimedHandler == null)
        return;
      this._freeClaimedHandler();
    }));
  }

  private void UpdateUI()
  {
    Dictionary<string, string> para = new Dictionary<string, string>();
    int topupGoldNumber = AnniversaryIapPayload.Instance.TopupGoldNumber;
    int goldReturnNumber = AnniversaryIapPayload.Instance.TopupGoldReturnNumber;
    para.Add("0", topupGoldNumber.ToString());
    para.Add("1", goldReturnNumber.ToString());
    this.freeRewardHint.text = ScriptLocalization.GetWithPara("event_topup_gold_item_tip", para, true);
    para.Clear();
    para.Add("0", Utils.FormatThousands(AnniversaryIapPayload.Instance.AnnvIapData.PayGoldNum.ToString()));
    this.totalTopupGoldAmount.text = ScriptLocalization.GetWithPara("event_lucky_draw_gold_topup_num", para, true);
    para.Clear();
    para.Add("0", AnniversaryIapPayload.Instance.AnnvIapData.HaveReceivedPropsNum.ToString());
    this.collectedItemCount.text = ScriptLocalization.GetWithPara("event_lucky_draw_draws_taken", para, true);
    this._remainedClaim = AnniversaryIapPayload.Instance.AnnvIapData.CanReceivePropsNum;
    para.Clear();
    para.Add("0", (this._remainedClaim <= 0L ? 0L : this._remainedClaim).ToString());
    this.remainedItemCount.text = ScriptLocalization.GetWithPara("event_lucky_draw_draws_remaining", para, true);
    if (this._remainedClaim > 0L)
    {
      NGUITools.SetActive(this.topupButton.gameObject, false);
      NGUITools.SetActive(this.topupClaimButton.gameObject, true);
    }
    else
    {
      NGUITools.SetActive(this.topupButton.gameObject, true);
      NGUITools.SetActive(this.topupClaimButton.gameObject, false);
    }
    BuilderFactory.Instance.HandyBuild((UIWidget) this.freeRewardTexture, AnniversaryIapPayload.Instance.PropsImagePath, (System.Action<bool>) null, true, false, string.Empty);
  }

  public class Parameter : Popup.PopupParameter
  {
    public System.Action freeClaimedHandler;
  }
}
