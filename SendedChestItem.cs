﻿// Decompiled with JetBrains decompiler
// Type: SendedChestItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class SendedChestItem : MonoBehaviour
{
  [SerializeField]
  private UILabel _LabelChestType;
  [SerializeField]
  private UILabel _LabelReceivedInfo;
  [SerializeField]
  private UITexture _TextureChestIcon;
  [SerializeField]
  private UILabel _LabelChestState;
  [SerializeField]
  private UILabel _LabelLeftValiableTime;
  [SerializeField]
  private UILabel _LabelSenderName;
  [SerializeField]
  private UIButton _buttonOpen;
  [SerializeField]
  private UIButton _buttonView;
  private AllianceTreasuryVaultData _sendedChestData;
  private bool _canReceive;

  public void SetData(AllianceTreasuryVaultData sendedChestData)
  {
    this._sendedChestData = sendedChestData;
    this._canReceive = false;
    AllianceTreasuryVaultMainInfo data = ConfigManager.inst.DB_AllianceTreasuryVaultMain.GetData(sendedChestData.configId);
    if (data == null)
    {
      D.error((object) string.Format("can not find AllianceTreasuryVaultMainInfo with internal id : {0}", (object) sendedChestData.configId));
    }
    else
    {
      this._LabelChestType.text = data.LocalChestName;
      if (data.Num > 0)
        this._LabelReceivedInfo.text = ScriptLocalization.GetWithPara("alliance_giftbox_collected_num", new Dictionary<string, string>()
        {
          {
            "0",
            sendedChestData.totalClaimed.ToString()
          },
          {
            "1",
            data.Num.ToString()
          }
        }, true);
      else
        this._LabelReceivedInfo.text = ScriptLocalization.GetWithPara("alliance_giftbox_people_collected_num", new Dictionary<string, string>()
        {
          {
            "0",
            sendedChestData.totalClaimed.ToString()
          }
        }, true);
      this._LabelChestState.gameObject.SetActive(true);
      if (sendedChestData.HaveClaimed(PlayerData.inst.uid))
        this._LabelChestState.text = ScriptLocalization.Get("alliance_giftbox_status_opened", true);
      else if (sendedChestData.status == "done")
        this._LabelChestState.text = ScriptLocalization.Get("alliance_giftbox_status_finished", true);
      else if (sendedChestData.status == "none")
      {
        this._LabelChestState.text = ScriptLocalization.Get("alliance_giftbox_status_unopened", true);
        this._LabelChestState.gameObject.SetActive(false);
        this._canReceive = true;
      }
      if (this._canReceive)
      {
        this._buttonOpen.gameObject.SetActive(true);
        this._buttonView.gameObject.SetActive(false);
      }
      else
      {
        this._buttonOpen.gameObject.SetActive(false);
        this._buttonView.gameObject.SetActive(true);
      }
      if (this._sendedChestData.type == "alliance")
      {
        if (sendedChestData.HaveClaimed(PlayerData.inst.uid))
          BuilderFactory.Instance.HandyBuild((UIWidget) this._TextureChestIcon, "Texture/Alliance/alliance_activity_chest_open", (System.Action<bool>) null, true, false, string.Empty);
        else
          BuilderFactory.Instance.HandyBuild((UIWidget) this._TextureChestIcon, "Texture/Alliance/alliance_activity_chest", (System.Action<bool>) null, true, false, string.Empty);
      }
      else if (sendedChestData.HaveClaimed(PlayerData.inst.uid))
        BuilderFactory.Instance.HandyBuild((UIWidget) this._TextureChestIcon, "Texture/Alliance/alliance_personal_chest_open", (System.Action<bool>) null, true, false, string.Empty);
      else
        BuilderFactory.Instance.HandyBuild((UIWidget) this._TextureChestIcon, "Texture/Alliance/alliance_personal_chest", (System.Action<bool>) null, true, false, string.Empty);
      this.UpdateLeftExpireTime();
      if (data.Type == AllianceTreasuryVaultMainInfo.ChestType.Alliance)
      {
        this._LabelSenderName.gameObject.SetActive(false);
      }
      else
      {
        this._LabelSenderName.gameObject.SetActive(true);
        string str = sendedChestData.senderName;
        if (sendedChestData.senderUid == PlayerData.inst.uid)
          str = PlayerData.inst.userData.userName;
        else if (string.IsNullOrEmpty(sendedChestData.senderName))
          str = ScriptLocalization.Get("id_anonymous", true);
        this._LabelSenderName.text = ScriptLocalization.GetWithPara("alliance_giftbox_from_sender_name", new Dictionary<string, string>()
        {
          {
            "0",
            str
          }
        }, true);
      }
    }
  }

  protected void UpdateLeftExpireTime()
  {
    this._LabelLeftValiableTime.text = ScriptLocalization.GetWithPara("alliance_giftbox_expires_in_num", new Dictionary<string, string>()
    {
      {
        "0",
        Utils.FormatTime(this._sendedChestData.expireTime - NetServerTime.inst.ServerTimestamp, false, false, true)
      }
    }, true);
  }

  public void OnSecondTick(int delta)
  {
    if (!this.enabled || this._sendedChestData == null)
      return;
    this.UpdateLeftExpireTime();
  }

  public void OnClicked()
  {
    if (this._canReceive)
    {
      if (this._sendedChestData.type == "alliance" && !Utils.CanOpenAllianceChest())
      {
        UIManager.inst.toast.Show(ScriptLocalization.Get("exception_2200060", true), (System.Action) null, 4f, true);
      }
      else
      {
        Hashtable postData = new Hashtable()
        {
          {
            (object) "uid",
            (object) PlayerData.inst.uid
          },
          {
            (object) "alliance_id",
            (object) PlayerData.inst.allianceId
          },
          {
            (object) "id",
            (object) this._sendedChestData.id
          }
        };
        RequestManager.inst.SendRequest("TreasuryVault:claimTreasury", postData, (System.Action<bool, object>) ((result, data) =>
        {
          if (!result)
            return;
          Hashtable hashtable1 = data as Hashtable;
          if (hashtable1 != null && hashtable1.ContainsKey((object) "treasure_chest_info"))
          {
            Hashtable hashtable2 = hashtable1[(object) "treasure_chest_info"] as Hashtable;
            if (hashtable2 != null)
              this._sendedChestData.Decode(hashtable2);
          }
          if ((bool) ((UnityEngine.Object) this))
            this.SetData(this._sendedChestData);
          AudioManager.Instance.StopAndPlaySound("sfx_city_vault_withdraw");
          if (this._sendedChestData.type == "alliance")
            UIManager.inst.OpenPopup("AllianceChest/AllianceChestPopup", (Popup.PopupParameter) new AllianceChestPopup.Parameter()
            {
              sendedChestData = this._sendedChestData,
              firstTime = true
            });
          else
            UIManager.inst.OpenPopup("AllianceChest/PersonalChestPopup", (Popup.PopupParameter) new PersonalChestPopup.Parameter()
            {
              sendedChestData = this._sendedChestData,
              firstTime = true
            });
        }), true);
      }
    }
    else if (this._sendedChestData.type == "alliance")
      UIManager.inst.OpenPopup("AllianceChest/AllianceChestPopup", (Popup.PopupParameter) new AllianceChestPopup.Parameter()
      {
        sendedChestData = this._sendedChestData,
        firstTime = false
      });
    else
      UIManager.inst.OpenPopup("AllianceChest/PersonalChestPopup", (Popup.PopupParameter) new PersonalChestPopup.Parameter()
      {
        sendedChestData = this._sendedChestData,
        firstTime = false
      });
  }
}
