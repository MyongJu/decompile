﻿// Decompiled with JetBrains decompiler
// Type: ConfigMerlinTrialsMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigMerlinTrialsMain
{
  private readonly List<MerlinTrialsMainInfo> _merlinTrialsMainInfoList = new List<MerlinTrialsMainInfo>();
  private Dictionary<string, MerlinTrialsMainInfo> _datas;
  private Dictionary<int, MerlinTrialsMainInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<MerlinTrialsMainInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, MerlinTrialsMainInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._merlinTrialsMainInfoList.Add(enumerator.Current);
    }
  }

  public void Clear()
  {
    if (this._datas != null)
      this._datas.Clear();
    if (this._dicByUniqueId != null)
      this._dicByUniqueId.Clear();
    if (this._merlinTrialsMainInfoList == null)
      return;
    this._merlinTrialsMainInfoList.Clear();
  }

  public List<MerlinTrialsMainInfo> GetInfoList()
  {
    return this._merlinTrialsMainInfoList;
  }

  public MerlinTrialsMainInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (MerlinTrialsMainInfo) null;
  }

  public MerlinTrialsMainInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (MerlinTrialsMainInfo) null;
  }

  public MerlinTrialsMainInfo GetByNumber(int groupId, int number)
  {
    int index = 0;
    for (int count = this._merlinTrialsMainInfoList.Count; index < count; ++index)
    {
      MerlinTrialsMainInfo merlinTrialsMainInfo = this._merlinTrialsMainInfoList[index];
      if (merlinTrialsMainInfo.trialId == groupId && merlinTrialsMainInfo.number == number)
        return merlinTrialsMainInfo;
    }
    return (MerlinTrialsMainInfo) null;
  }

  public MerlinTrialsMainInfo GetMin(int groupId, int layerId)
  {
    int num = 9999;
    MerlinTrialsMainInfo merlinTrialsMainInfo1 = (MerlinTrialsMainInfo) null;
    int index = 0;
    for (int count = this._merlinTrialsMainInfoList.Count; index < count; ++index)
    {
      MerlinTrialsMainInfo merlinTrialsMainInfo2 = this._merlinTrialsMainInfoList[index];
      if (merlinTrialsMainInfo2.trialId == groupId && merlinTrialsMainInfo2.layerId == layerId && merlinTrialsMainInfo2.number < num)
      {
        num = merlinTrialsMainInfo2.number;
        merlinTrialsMainInfo1 = merlinTrialsMainInfo2;
      }
    }
    return merlinTrialsMainInfo1;
  }

  public MerlinTrialsMainInfo GetMax(int groupId, int layerId)
  {
    int num = 0;
    MerlinTrialsMainInfo merlinTrialsMainInfo1 = (MerlinTrialsMainInfo) null;
    int index = 0;
    for (int count = this._merlinTrialsMainInfoList.Count; index < count; ++index)
    {
      MerlinTrialsMainInfo merlinTrialsMainInfo2 = this._merlinTrialsMainInfoList[index];
      if (merlinTrialsMainInfo2.trialId == groupId && merlinTrialsMainInfo2.layerId == layerId && merlinTrialsMainInfo2.number > num)
      {
        num = merlinTrialsMainInfo2.number;
        merlinTrialsMainInfo1 = merlinTrialsMainInfo2;
      }
    }
    return merlinTrialsMainInfo1;
  }

  public List<MerlinTrialsMainInfo> GetAllByLayerId(int groupId, int layerId)
  {
    List<MerlinTrialsMainInfo> merlinTrialsMainInfoList = new List<MerlinTrialsMainInfo>();
    int index = 0;
    for (int count = this._merlinTrialsMainInfoList.Count; index < count; ++index)
    {
      MerlinTrialsMainInfo merlinTrialsMainInfo = this._merlinTrialsMainInfoList[index];
      if (merlinTrialsMainInfo.trialId == groupId && merlinTrialsMainInfo.layerId == layerId)
        merlinTrialsMainInfoList.Add(merlinTrialsMainInfo);
    }
    merlinTrialsMainInfoList.Sort((Comparison<MerlinTrialsMainInfo>) ((x, y) => x.number.CompareTo(y.number)));
    return merlinTrialsMainInfoList;
  }

  public List<MerlinTrialsMainInfo> GetAllByGroup(int groupId)
  {
    List<MerlinTrialsMainInfo> merlinTrialsMainInfoList = new List<MerlinTrialsMainInfo>();
    int index = 0;
    for (int count = this._merlinTrialsMainInfoList.Count; index < count; ++index)
    {
      MerlinTrialsMainInfo merlinTrialsMainInfo = this._merlinTrialsMainInfoList[index];
      if (merlinTrialsMainInfo.trialId == groupId)
        merlinTrialsMainInfoList.Add(merlinTrialsMainInfo);
    }
    merlinTrialsMainInfoList.Sort((Comparison<MerlinTrialsMainInfo>) ((x, y) => x.number.CompareTo(y.number)));
    return merlinTrialsMainInfoList;
  }
}
