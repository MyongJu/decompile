﻿// Decompiled with JetBrains decompiler
// Type: UserOrBuyResourceItemsDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class UserOrBuyResourceItemsDlg : UI.Dialog
{
  private GameObjectPool pools = new GameObjectPool();
  private ItemBag.ItemType[] SHOPITEM_TYPE = new ItemBag.ItemType[4]
  {
    ItemBag.ItemType.food,
    ItemBag.ItemType.wood,
    ItemBag.ItemType.ore,
    ItemBag.ItemType.silver
  };
  private List<ShortCutItemRenderer> lists = new List<ShortCutItemRenderer>();
  private List<ShopStaticInfo> datas = new List<ShopStaticInfo>();
  private List<int> items = new List<int>();
  public UIButtonBar bar;
  public UIScrollView scroll;
  public GameObject itemRoot;
  public UIGrid grid;
  private bool init;
  public UILabel gold;
  public ShortCutItemRenderer itemPrefab;
  public UILabel food;
  public UILabel food1;
  public UILabel wood;
  public UILabel wood1;
  public UILabel silver;
  public UILabel silver1;
  public UILabel ore;
  public UILabel ore1;

  public int SelectedIndex
  {
    get
    {
      return this.bar.SelectedIndex;
    }
    set
    {
      this.bar.SelectedIndex = value;
    }
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.Init();
    UserOrBuyResourceItemsDlg.Parameter parameter = orgParam as UserOrBuyResourceItemsDlg.Parameter;
    CityData playerCityData = PlayerData.inst.playerCityData;
    this.OnResourcesUpdated((long) playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD), (long) playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD), (long) playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER), (long) playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE), PlayerData.inst.userData.currency.gold);
    this.SelectedIndex = parameter.selectedIndex;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Clear();
    this.pools.Clear();
    this.RemoveEventHandler();
  }

  private void Init()
  {
    if (this.init)
      return;
    this.init = true;
    this.pools.Initialize(this.itemPrefab.gameObject, this.itemRoot);
    this.AddEventHandler();
  }

  private void OnButtonBarSelected(int index)
  {
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Item.onDataCreated += new System.Action<int>(this.OnDataChangeHandler);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnDataChangeHandler);
    this.bar.OnSelectedHandler += new System.Action<int>(this.OnButtonBarSelected);
    CityManager inst = CityManager.inst;
    if (inst == null)
      return;
    inst.onResourcesUpdated += new CityManager.OnResourcesUpdated(this.OnResourcesUpdated);
  }

  private void OnResourcesUpdated(long food, long wood, long silver, long ore, long gold)
  {
    this.SetFood(food);
    this.SetWood(wood);
    this.SetSilver(silver);
    this.SetOre(ore);
  }

  private void SetFood(long food)
  {
    this.food.text = Utils.FormatShortThousandsLong(food);
    this.food1.text = Utils.FormatShortThousandsLong(food);
  }

  private void SetWood(long food)
  {
    this.wood.text = Utils.FormatShortThousandsLong(food);
    this.wood1.text = Utils.FormatShortThousandsLong(food);
  }

  private void SetSilver(long food)
  {
    this.silver.text = Utils.FormatShortThousandsLong(food);
    this.silver1.text = Utils.FormatShortThousandsLong(food);
  }

  private void SetOre(long food)
  {
    this.ore.text = Utils.FormatShortThousandsLong(food);
    this.ore1.text = Utils.FormatShortThousandsLong(food);
  }

  private void Clear()
  {
    for (int index = 0; index < this.lists.Count; ++index)
    {
      this.lists[index].Dispose();
      this.pools.Release(this.lists[index].gameObject);
    }
    this.lists.Clear();
  }

  private void RemoveEventHandler()
  {
    this.init = false;
    DBManager.inst.DB_Item.onDataCreated -= new System.Action<int>(this.OnDataChangeHandler);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnDataChangeHandler);
    this.bar.OnSelectedHandler -= new System.Action<int>(this.OnButtonBarSelected);
    CityManager inst = CityManager.inst;
    if (inst == null)
      return;
    inst.onResourcesUpdated -= new CityManager.OnResourcesUpdated(this.OnResourcesUpdated);
  }

  private void OnDataChangeHandler(int id)
  {
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.gold.text = PlayerData.inst.hostPlayer.Currency.ToString();
    this.Clear();
    this.CreateData();
    this.scroll.gameObject.SetActive(false);
    for (int index = 0; index < this.items.Count; ++index)
      this.GetItem().SetItemData(this.items[index]);
    for (int index = 0; index < this.datas.Count; ++index)
      this.GetItem().SetShopItemData(this.datas[index].internalId);
    this.grid.repositionNow = true;
    this.grid.Reposition();
    Utils.ExecuteInSecs(1f / 1000f, (System.Action) (() =>
    {
      this.scroll.gameObject.SetActive(true);
      this.scroll.ResetPosition();
    }));
  }

  private ShortCutItemRenderer GetItem()
  {
    GameObject go = this.pools.AddChild(this.grid.gameObject);
    ShortCutItemRenderer component = go.GetComponent<ShortCutItemRenderer>();
    NGUITools.SetActive(go, true);
    this.lists.Add(component);
    return component;
  }

  private void InitGameObject(GameObject go, int wrapIndex, int realIndex)
  {
    ShortCutItemRenderer component = go.GetComponent<ShortCutItemRenderer>();
    realIndex = Mathf.Abs(realIndex);
    if (realIndex >= this.datas.Count)
      return;
    component.SetShopItemData(this.datas[realIndex].internalId);
  }

  private void CreateData()
  {
    this.datas.Clear();
    string category = "resources";
    ItemBag.ItemType itemType = this.SHOPITEM_TYPE[this.SelectedIndex];
    List<ShopStaticInfo> currentSaleShopItems = ItemBag.Instance.GetCurrentSaleShopItems("shop", category);
    List<ShopStaticInfo> shopStaticInfoList1 = new List<ShopStaticInfo>();
    List<ShopStaticInfo> shopStaticInfoList2 = new List<ShopStaticInfo>();
    List<ItemStaticInfo> itemStaticInfoList = new List<ItemStaticInfo>();
    Dictionary<long, ConsumableItemData>.ValueCollection.Enumerator enumerator1 = DBManager.inst.DB_Item.Datas.Values.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(enumerator1.Current.internalId);
      if (itemStaticInfo != null && itemStaticInfo.Type == itemType)
        itemStaticInfoList.Add(itemStaticInfo);
    }
    itemStaticInfoList.Sort(new Comparison<ItemStaticInfo>(this.CompareItem));
    this.items.Clear();
    for (int index = 0; index < itemStaticInfoList.Count; ++index)
      this.items.Add(itemStaticInfoList[index].internalId);
    List<ShopStaticInfo>.Enumerator enumerator2 = currentSaleShopItems.GetEnumerator();
    while (enumerator2.MoveNext())
    {
      if (enumerator2.Current != null)
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(enumerator2.Current.Item_InternalId);
        if (this.items.IndexOf(itemStaticInfo.internalId) <= -1 && itemStaticInfo.Type == itemType)
        {
          if (ItemBag.Instance.GetItemCount(itemStaticInfo.internalId) > 0)
            shopStaticInfoList1.Add(enumerator2.Current);
          else
            shopStaticInfoList2.Add(enumerator2.Current);
        }
      }
    }
    shopStaticInfoList1.Sort(new Comparison<ShopStaticInfo>(this.CompareShopItem));
    shopStaticInfoList2.Sort(new Comparison<ShopStaticInfo>(this.CompareShopItem));
    for (int index = 0; index < shopStaticInfoList1.Count; ++index)
      this.datas.Add(shopStaticInfoList1[index]);
    for (int index = 0; index < shopStaticInfoList2.Count; ++index)
      this.datas.Add(shopStaticInfoList2[index]);
  }

  public void OnAddGoldHandler()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  private int CompareItem(ItemStaticInfo a, ItemStaticInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }

  private int CompareShopItem(ShopStaticInfo a, ShopStaticInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int selectedIndex;
  }
}
