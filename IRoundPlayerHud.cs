﻿// Decompiled with JetBrains decompiler
// Type: IRoundPlayerHud
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public interface IRoundPlayerHud
{
  void Initialize();

  void SetHealthProgress(float progress);

  void SetHealthNumber(int health, int healthMax);

  void SetManaProgress(float progress);

  void SetManaNumber(int mana, int manaMax);

  void SetDecreaseHealth(int number, Color color);

  void SetIncreaseHealth(int number, Color color);

  void SetBuffs(List<BattleBuff> buffs);

  void DecreaseCDTime();
}
