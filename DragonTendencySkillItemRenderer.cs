﻿// Decompiled with JetBrains decompiler
// Type: DragonTendencySkillItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class DragonTendencySkillItemRenderer : MonoBehaviour
{
  public UITexture m_SkillIcon;
  public GameObject m_Active;
  public GameObject m_Inactive;
  public GameObject m_Using;
  public int Index;
  private ConfigDragonTendencyBoostInfo m_DragonTendencyBoost;
  private DragonData m_DragonData;

  public void SetData(ConfigDragonTendencyBoostInfo dragonTendencyBoost, DragonData dragonData)
  {
    this.m_DragonTendencyBoost = dragonTendencyBoost;
    this.m_DragonData = dragonData;
    this.UpdateUI();
  }

  public DragonData dragonData
  {
    get
    {
      return this.m_DragonData;
    }
  }

  public ConfigDragonTendencyBoostInfo tendencyBoost
  {
    get
    {
      return this.m_DragonTendencyBoost;
    }
  }

  public void UpdateUI()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_SkillIcon, this.m_DragonTendencyBoost.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    bool flag = DragonUtils.IsFormationEnabled(this.m_DragonTendencyBoost.ID);
    this.m_Active.SetActive(flag);
    this.m_Inactive.SetActive(!flag);
    this.UpdateUsingState();
  }

  public void UpdateUsingState()
  {
    if (!(bool) ((UnityEngine.Object) this.m_Using))
      return;
    this.m_Using.SetActive(this.m_DragonTendencyBoost != null && DragonUtils.GetDragonTendencySkillID(this.m_DragonData) == this.m_DragonTendencyBoost.ID);
  }

  public void OnClicked()
  {
    UIManager.inst.OpenPopup("Dragon/DragonTendencySkillsPopup", (Popup.PopupParameter) new DragonTendencySkillsPopup.Parameter()
    {
      dragonData = PlayerData.inst.dragonData,
      startIndex = this.Index
    });
  }
}
