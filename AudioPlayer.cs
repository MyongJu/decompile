﻿// Decompiled with JetBrains decompiler
// Type: AudioPlayer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
  private Dictionary<string, AudioClip> _audioClipCache = new Dictionary<string, AudioClip>();
  private List<AudioSource> _audioSourceCache = new List<AudioSource>();
  private BetterList<AudioSource> _toBeRemoved = new BetterList<AudioSource>();

  public AudioSource Play(string uri, bool loop)
  {
    if (SettingManager.Instance.IsSFXOff)
      return (AudioSource) null;
    AudioClip audioClip;
    if (!this._audioClipCache.TryGetValue(uri, out audioClip))
    {
      audioClip = AssetManager.Instance.HandyLoad(uri, (System.Type) null) as AudioClip;
      if ((bool) ((UnityEngine.Object) audioClip))
        this._audioClipCache.Add(uri, audioClip);
      else
        D.error((object) "Can not find sound file: {0}", (object) uri);
    }
    if (!(bool) ((UnityEngine.Object) audioClip))
      return (AudioSource) null;
    AudioSource audioSource = this.gameObject.AddComponent<AudioSource>();
    audioSource.clip = audioClip;
    audioSource.loop = loop;
    audioSource.Play();
    this._audioSourceCache.Add(audioSource);
    return audioSource;
  }

  public void Stop(AudioSource source)
  {
    if (!this._audioSourceCache.Remove(source))
      return;
    source.Stop();
    UnityEngine.Object.Destroy((UnityEngine.Object) source);
  }

  public void StopAll()
  {
    int count = this._audioSourceCache.Count;
    for (int index = 0; index < count; ++index)
    {
      AudioSource audioSource = this._audioSourceCache[index];
      if ((bool) ((UnityEngine.Object) audioSource))
      {
        audioSource.Stop();
        UnityEngine.Object.Destroy((UnityEngine.Object) audioSource);
      }
    }
    this._audioSourceCache.Clear();
  }

  public void ClearAudioClipCache()
  {
    this._audioClipCache.Clear();
  }

  private void Update()
  {
    int count = this._audioSourceCache.Count;
    for (int index = 0; index < count; ++index)
    {
      AudioSource audioSource = this._audioSourceCache[index];
      if (!(bool) ((UnityEngine.Object) audioSource))
        this._toBeRemoved.Add(audioSource);
      else if (!audioSource.isPlaying)
        this._toBeRemoved.Add(audioSource);
    }
    int size = this._toBeRemoved.size;
    for (int index = 0; index < size; ++index)
      this.Stop(this._toBeRemoved[index]);
    this._toBeRemoved.Clear();
  }
}
