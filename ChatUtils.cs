﻿// Decompiled with JetBrains decompiler
// Type: ChatUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Chat;
using DB;

public class ChatUtils
{
  public static bool CheckMessageShouldHide(ChatMessage message)
  {
    if (message == null || DBManager.inst == null)
      return true;
    bool flag1 = DBManager.inst.DB_Contacts.blocking.Contains(message.senderUid) && !message.isMod;
    bool flag2 = PlayerData.inst.userData.InMod(message.senderUid) && !message.isMod;
    if (!flag1)
      return flag2;
    return true;
  }
}
