﻿// Decompiled with JetBrains decompiler
// Type: LeaveAllianceDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class LeaveAllianceDlg : MonoBehaviour
{
  public GameObject m_Step1;
  public GameObject m_Step2;
  public UILabel m_confirmLabel;
  private System.Action<bool> _onFinished;
  private AllianceData _alliance;

  public void Show(AllianceData alliance, System.Action<bool> onClose)
  {
    this.m_Step1.SetActive(true);
    this.m_Step2.SetActive(false);
    this._alliance = alliance;
    this._onFinished = onClose;
  }

  public void OnLeaveAlliancePressed()
  {
    AllianceWareHouseData myWarehouseData = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseData();
    this.m_confirmLabel.text = myWarehouseData == null || myWarehouseData.getResourceDataByUserId(PlayerData.inst.uid).TotalWeight <= 0L ? ScriptLocalization.Get("alliance_leave_confirm_description", true) : ScriptLocalization.Get("alliance_leave_storehouse_warning", true);
    this.m_Step1.SetActive(false);
    this.m_Step2.SetActive(true);
  }

  public void ArYouSureCancel()
  {
    this.m_Step1.SetActive(true);
    this.m_Step2.SetActive(false);
  }

  public void AreYouSureYes()
  {
    AllianceManager.Instance.LeaveAlliance(PlayerData.inst.allianceId, new System.Action<bool, object>(this.AllianceGetInfoCallback));
  }

  private void AllianceGetInfoCallback(bool ret, object data)
  {
    this.gameObject.SetActive(false);
    if (ret)
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      para["Alliance_Name"] = this._alliance.allianceName;
      ToastManager.Instance.AddToast("BASIC_TOAST", (object) new ToastArgs(ConfigManager.inst.DB_Toast.GetData("toast_alliance_leave"), para));
      this._onFinished(true);
    }
    else
      this._onFinished(false);
  }

  public void OnClosePressed()
  {
    this.gameObject.SetActive(false);
    this._onFinished(false);
  }
}
