﻿// Decompiled with JetBrains decompiler
// Type: LoadConfigFilesState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class LoadConfigFilesState : LoadBaseState
{
  private Dictionary<string, byte[]> configs = new Dictionary<string, byte[]>();
  private List<string> configNames = new List<string>();
  private List<string> failNames = new List<string>();
  private int taskId = -1;
  private const string ZIP_MARK = ".zip";
  private int COMPLETE_COUNT;
  private int current;
  private int total;
  private int startTime;
  private bool pause;

  public LoadConfigFilesState(int step)
    : base(step)
  {
  }

  public int completecount
  {
    get
    {
      return this.COMPLETE_COUNT;
    }
    set
    {
      this.COMPLETE_COUNT = value;
    }
  }

  protected override SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.LoadConfigFiles;
    }
  }

  public override string Key
  {
    get
    {
      return nameof (LoadConfigFilesState);
    }
  }

  private void LoadConfig()
  {
    IDictionaryEnumerator enumerator = this.Preloader._remoteConfigVerHt.GetEnumerator();
    this.configNames.Clear();
    while (enumerator.MoveNext())
    {
      string str = enumerator.Key.ToString();
      if (!this.Preloader._localConfigVerHt.ContainsKey((object) str) || !this.Preloader._localConfigVerHt[(object) str].Equals(this.Preloader._remoteConfigVerHt[(object) str]) || !Utils.CheckFile(NetApi.inst.BuildLocalPath(str)))
      {
        ++this.total;
        this.GetLoaderInfo(str, NetApi.inst.BuildConfigUrl(str));
        this.batch.AddConfig(str, NetApi.inst.BuildConfigUrl(str), (Hashtable) null, false);
        this.configNames.Add(str);
      }
    }
    LoaderManager.inst.Add(this.batch, false, false);
  }

  private void RefreshCurrentProgress()
  {
    float num1 = (float) (1.0 - (double) this.COMPLETE_COUNT / (double) this.total);
    float num2 = SplashDataConfig.GetValue(SplashDataConfig.Phase.LoadPreUI);
    float num3 = SplashDataConfig.GetValue(this.CurrentPhase);
    float num4 = num2 + (num3 - num2) * num1;
    float num5 = SplashDataConfig.GetValue(this.CurrentPhase);
    if ((double) num4 > (double) num5)
      num4 = num5;
    this.RefreshProgress(num4);
  }

  protected override void RefreshProgress(float value)
  {
    string str = string.Empty;
    if (this.total > 0)
      str = string.Format(" {0}/{1}", (object) this.COMPLETE_COUNT, (object) this.total);
    this.Preloader.SplashScreen(SplashDataConfig.GetTipFromPhaseKey(this.CurrentPhase) + str, SplashDataConfig.GetValue(this.CurrentPhase) * value);
  }

  protected override void Prepare()
  {
    base.Prepare();
    this.LoadConfig();
    Oscillator.Instance.updateEvent += new System.Action<double>(this.UpdateEventHandler);
  }

  private void UpdateEventHandler(double t)
  {
    this.StartProgressTween();
    if (this.configNames == null || this.COMPLETE_COUNT != this.total || !this.WriteFileSuccess && this.total != 0)
      return;
    this.Finish();
  }

  private void StartProgressTween()
  {
    GameEngine.Instance.iTweenValue = Mathf.Lerp(GameEngine.Instance.iTweenValue, SplashDataConfig.GetValue(this.CurrentPhase), 0.02f);
    this.RefreshProgress(GameEngine.Instance.iTweenValue);
  }

  protected override void OnOneFileDone(LoaderInfo info)
  {
    base.OnOneFileDone(info);
    if (this.configNames.IndexOf(info.Action) == -1)
      return;
    this.configNames.Remove(info.Action);
    if (info.ResData != null)
    {
      this.Preloader._localConfigVerHt[(object) info.Action] = this.Preloader._remoteConfigVerHt[(object) info.Action];
      this.ParseConfig((object) new LoadConfigFilesState.InfoData()
      {
        url = info.Action,
        data = info.ResData
      });
    }
    else
      this.failNames.Add(info.Action);
    this.RefreshCurrentProgress();
  }

  private void ParseConfig(object li)
  {
    LoadConfigFilesState.InfoData infoData = li as LoadConfigFilesState.InfoData;
    if (Encoding.UTF8.GetString(infoData.data).TrimEnd().EndsWith("}"))
    {
      this.WriteFile(NetApi.inst.BuildLocalPath(infoData.url), infoData.data);
      ++this.COMPLETE_COUNT;
    }
    else
      this.failNames.Add(infoData.url);
  }

  private void WriteConfigVerList()
  {
    this.WriteFile(NetApi.inst.BuildLocalPath("ConfigVersionList.json"), Encoding.UTF8.GetBytes(Utils.Object2Json((object) this.Preloader._localConfigVerHt)));
    this.WriteFile(NetApi.inst.BuildLocalPath("local_config_version"), Encoding.UTF8.GetBytes(NetApi.inst.ConfigFilesVersion));
  }

  protected override void Retry()
  {
    this.pause = false;
    this.RetryLoad();
  }

  protected override void Finish()
  {
    if (this.IsDestroy)
      return;
    TaskManager.Instance.SetAutoRetryTask(this.taskId, true);
    this.taskId = -1;
    base.Finish();
    this.RefreshProgress();
    this.WriteConfigVerList();
    this.WriteManifest();
    if (Oscillator.IsAvailable)
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.UpdateEventHandler);
    this.Preloader.OnLoadConfigFilesFinish();
  }

  protected override void OnDispose()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.updateEvent -= new System.Action<double>(this.UpdateEventHandler);
  }

  private void WriteManifest()
  {
    if (this.Preloader.IsRestart)
      return;
    this.WriteFile(NetApi.inst.BuildLocalPath("manifest"), Encoding.UTF8.GetBytes(Utils.Object2Json((object) NetApi.inst.Manifest)));
  }

  private void CheckFailList()
  {
    if (this.pause || this.failNames.Count <= 0)
      return;
    if (this.taskId < 0)
      this.taskId = TaskManager.Instance.Execute((TaskManager.ITask) new AutoRetryTask()
      {
        ExecuteHandler = new System.Action(this.RetryLoad)
      });
    else
      TaskManager.Instance.SetAutoRetryTask(this.taskId, false);
    if (!TaskManager.Instance.IsFinish(this.taskId))
      return;
    this.taskId = -1;
    this.pause = true;
    this.ShowRetry();
  }

  protected override void OnAllFileFinish(LoaderBatch info)
  {
    this.CheckFailList();
  }

  private void RetryLoad()
  {
    this.RemoveEventHandler();
    this.batch = LoaderManager.inst.PopBatch();
    this.IsFail = false;
    for (int index = 0; index < this.failNames.Count; ++index)
    {
      this.batch.AddConfig(this.failNames[index], NetApi.inst.BuildConfigUrl(this.failNames[index]), (Hashtable) null, false);
      this.configNames.Add(this.failNames[index]);
    }
    this.failNames.Clear();
    this.AddEventHandler();
    LoaderManager.inst.Add(this.batch, false, false);
  }

  private class InfoData
  {
    public string url;
    public byte[] data;
  }
}
