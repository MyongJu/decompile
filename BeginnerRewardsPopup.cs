﻿// Decompiled with JetBrains decompiler
// Type: BeginnerRewardsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BeginnerRewardsPopup : BaseReportPopup
{
  public UILabel content;
  public RewardItemComponent ric;
  public UILabel info;
  public GameObject group;
  public GameObject btnCollection;

  private void Init()
  {
    this.content.text = this.param.mail.GetBodyString();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_winner", (System.Action<bool>) null, true, false, string.Empty);
    this.SetInfo();
    this.ric.FeedData((IComponentData) this.AnalyzeCollectedRewards());
    this.UpdateBtnState();
  }

  private void SetInfo()
  {
    if (this.maildata.type != MailType.MAIL_TYPE_PVE_BATTLE_REPORT)
      ;
  }

  private void ArrangeLayout()
  {
    throw new NotImplementedException();
  }

  private void CollectRewards()
  {
    Hashtable hashtable = this.param.mail.data[(object) "attachment"] as Hashtable;
    if (hashtable == null)
      return;
    RewardData rewardData = JsonReader.Deserialize<RewardData>(Utils.Object2Json((object) hashtable));
    List<IconData> iconDataList = new List<IconData>();
    if (rewardData.item != null)
    {
      RewardsCollectionAnimator.Instance.Clear();
      using (Dictionary<string, int>.Enumerator enumerator = rewardData.item.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int internalId = int.Parse(current.Key);
          int num = current.Value;
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
          RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
          {
            icon = itemStaticInfo.ImagePath,
            count = (float) num
          });
        }
      }
    }
    RewardsCollectionAnimator.Instance.CollectItems(false);
  }

  private void UpdateBtnState()
  {
    if (this.param.mail.attachment_status == 2)
      this.btnCollection.GetComponent<UIButton>().isEnabled = false;
    else
      this.btnCollection.GetComponent<UIButton>().isEnabled = true;
  }

  public new void OnClaimRewardClicked()
  {
    this.CollectRewards();
    PlayerData.inst.mail.API.GainAttachment((int) this.maildata.category, this.param.mail.mailID, new System.Action<bool, object>(this.GainAttachmentCallBack));
  }

  private void GainAttachmentCallBack(bool ok, object obj)
  {
    if (!ok)
      return;
    this.param.mail.attachment_status = 2;
    this.UpdateBtnState();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }
}
