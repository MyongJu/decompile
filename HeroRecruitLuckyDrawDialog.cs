﻿// Decompiled with JetBrains decompiler
// Type: HeroRecruitLuckyDrawDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HeroRecruitLuckyDrawDialog : UI.Dialog
{
  public HeroCardRotator m_HeroCardRotator;
  public UIButton m_Button;
  public UILabel m_Price;
  public UILabel m_CallAgain;
  public UILabel m_TopHint;
  public Color m_CurrencyColor;
  public ParticleSystem m_ParticleSystem;
  public UISprite m_ItemIcon;
  public UnityEngine.Animation m_Shake;
  private HeroRecruitLuckyDrawDialog.Parameter m_Parameter;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (this.m_Parameter == null)
    {
      this.m_Parameter = orgParam as HeroRecruitLuckyDrawDialog.Parameter;
      this.UpdateUI();
    }
    this.UpdatePrice();
  }

  public void OnRecruit()
  {
    if (this.Price > this.m_Parameter.heroRecruitData.GetCurrency())
    {
      this.m_Parameter.heroRecruitData.ShowStore((int) this.Price);
    }
    else
    {
      this.m_TopHint.text = string.Empty;
      HeroRecruitUtils.Summon(this.SummonType, false, (System.Action<bool, HeroRecruitPayload>) ((ret, payload) =>
      {
        if (!ret)
          return;
        this.m_Parameter.heroPayload = payload.GetPayloadData()[0];
        this.UpdateUI();
      }));
    }
  }

  private int SummonType
  {
    get
    {
      if (this.m_Parameter.customSummonType == 0)
        return this.m_Parameter.heroRecruitData.GetSummonType();
      return this.m_Parameter.customSummonType;
    }
  }

  private void UpdateUI()
  {
    this.m_Button.gameObject.SetActive(this.m_Parameter.showButton);
    this.m_Button.isEnabled = false;
    this.m_CallAgain.gameObject.SetActive(false);
    this.m_ItemIcon.spriteName = this.m_Parameter.heroRecruitData.GetIcon();
    this.UpdatePrice();
    this.m_CallAgain.text = HeroRecruitUtils.GetRecruitText(this.m_Parameter.heroRecruitData.GetSummonType());
    this.Play();
    this.UpdateTopHint();
  }

  private void UpdateTopHint()
  {
    if (this.SummonType < 1 || this.SummonType > 2)
      return;
    ParliamentHeroSummonGroupInfo summonGroupInfo = HeroRecruitUtils.GetSummonGroupInfo(this.SummonType);
    if (summonGroupInfo == null)
      return;
    Dictionary<string, string> para = new Dictionary<string, string>();
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(summonGroupInfo.baseRewardId);
    if (itemStaticInfo != null)
    {
      para.Add("0", itemStaticInfo.LocName);
      para.Add("1", summonGroupInfo.baseRewardCount.ToString());
    }
    if (summonGroupInfo.baseRewardCount <= 0)
      return;
    this.m_TopHint.text = ScriptLocalization.GetWithPara("event_jackpot_with_reward", para, true);
  }

  private void UpdatePrice()
  {
    this.m_Price.text = Utils.FormatThousands(this.Price.ToString());
    this.m_Price.color = this.Price > this.m_Parameter.heroRecruitData.GetCurrency() ? Color.red : this.m_CurrencyColor;
  }

  private long Price
  {
    get
    {
      if (this.m_Parameter.customSummonType == 0)
        return this.m_Parameter.heroRecruitData.GetPrice();
      return (long) this.m_Parameter.customPrice;
    }
  }

  private void Play()
  {
    this.m_HeroCardRotator.Play(this.m_Parameter.heroPayload, new System.Action(this.OnFlipCardFinished));
    this.m_ParticleSystem.Stop();
    this.m_ParticleSystem.Clear();
    this.m_ParticleSystem.Play(true);
    AudioManager.Instance.StopAndPlaySound("sfx_city_hero_summon");
  }

  private void OnFlipCardFinished()
  {
    this.m_Button.isEnabled = true;
    this.m_CallAgain.gameObject.SetActive(this.m_Parameter.showButton);
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this.m_Parameter.heroPayload.hero_id);
    if (parliamentHeroInfo == null || parliamentHeroInfo.quality != 3)
      return;
    this.m_Shake.enabled = true;
    this.m_Shake.Play(AnimationPlayMode.Stop);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public bool showButton;
    public HeroRecruitData heroRecruitData;
    public HeroRecruitPayloadData heroPayload;
    public int customSummonType;
    public int customPrice;
  }
}
