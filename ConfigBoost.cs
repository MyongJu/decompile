﻿// Decompiled with JetBrains decompiler
// Type: ConfigBoost
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigBoost
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<int, BoostStaticInfo> item2Boost;
  private Dictionary<int, BoostStaticInfo> dicByUniqueId;

  public Dictionary<int, BoostStaticInfo> Item2Boosts
  {
    get
    {
      return this.item2Boost;
    }
  }

  public void BuildDB(object res)
  {
    this.parse.Parse<BoostStaticInfo, int>(res as Hashtable, "Item_InternalID", out this.item2Boost, out this.dicByUniqueId);
  }

  public BoostStaticInfo GetBoost(int internalId)
  {
    if (this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (BoostStaticInfo) null;
  }

  public BoostStaticInfo GetBoostByItemInternalId(int internalId)
  {
    if (this.item2Boost.ContainsKey(internalId))
      return this.item2Boost[internalId];
    return (BoostStaticInfo) null;
  }

  public void Clear()
  {
    if (this.item2Boost != null)
    {
      this.item2Boost.Clear();
      this.item2Boost = (Dictionary<int, BoostStaticInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, BoostStaticInfo>) null;
  }
}
