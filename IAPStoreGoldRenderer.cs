﻿// Decompiled with JetBrains decompiler
// Type: IAPStoreGoldRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class IAPStoreGoldRenderer : IAPStoreRenderer
{
  public GameObject m_TagRoot;
  public UILabel m_TagText;
  public UITexture m_Icon;
  public UILabel m_Quantity;
  public UILabel m_Addition;
  public UILabel m_Tip;
  public UIButton m_GoodButton;
  public UIButton m_NormalButton;
  public UILabel m_GoodPrice;
  public UILabel m_NormalPrice;
  private IAPGoldInfo m_GoldInfo;
  private System.Action OnPurchaseBegin;
  private System.Action<int> OnPurchaseEnd;

  public void SetData(IAPGoldInfo goldInfo, System.Action onPurchaseBegin, System.Action<int> onPurchaseEnd)
  {
    this.m_GoldInfo = goldInfo;
    this.OnPurchaseBegin = onPurchaseBegin;
    this.OnPurchaseEnd = onPurchaseEnd;
    this.UpdateUI();
  }

  public IAPGoldInfo GoldInfo
  {
    get
    {
      return this.m_GoldInfo;
    }
  }

  public void OnBuy()
  {
    if (this.OnPurchaseBegin != null)
      this.OnPurchaseBegin();
    PaymentManager.Instance.BlockBuyProduct(this.m_GoldInfo.productId, string.Empty, string.Empty, (System.Action<bool>) (obj =>
    {
      if (!obj)
        return;
      this.UpdateUI();
    }), (System.Action) (() =>
    {
      if (this.OnPurchaseEnd == null)
        return;
      this.OnPurchaseEnd(this.m_GoldInfo.internalId);
    }));
  }

  public override void UpdateUI()
  {
    if (!string.IsNullOrEmpty(this.m_GoldInfo.tag))
    {
      this.m_TagRoot.SetActive(true);
      this.m_TagText.text = this.m_GoldInfo.tag.ToUpper();
    }
    else
      this.m_TagRoot.SetActive(false);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, this.m_GoldInfo.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    this.m_Quantity.text = Utils.FormatThousands(this.m_GoldInfo.gold.ToString());
    bool isOverbalance = this.m_GoldInfo.IsOverbalance;
    if (isOverbalance)
    {
      this.m_Addition.text = "+" + Utils.FormatThousands(this.m_GoldInfo.ex_gold.ToString());
      switch (this.m_GoldInfo.type)
      {
        case 0:
          this.m_Tip.text = string.Empty;
          break;
        case 1:
          this.m_Tip.text = ScriptLocalization.Get("iap_gold_once", true);
          break;
        case 2:
          this.m_Tip.text = ScriptLocalization.Get("iap_gold_daily_limit", true);
          break;
      }
    }
    else
    {
      this.m_Addition.text = string.Empty;
      this.m_Tip.text = string.Empty;
    }
    this.m_GoodButton.gameObject.SetActive(isOverbalance);
    this.m_NormalButton.gameObject.SetActive(!isOverbalance);
    this.m_GoodPrice.text = PaymentManager.Instance.GetFormattedPrice(this.m_GoldInfo.productId, this.m_GoldInfo.id);
    this.m_NormalPrice.text = PaymentManager.Instance.GetFormattedPrice(this.m_GoldInfo.productId, this.m_GoldInfo.id);
  }
}
