﻿// Decompiled with JetBrains decompiler
// Type: ParliamentHeroCardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ParliamentHeroCardInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "hero_card")]
  public int heroCard;
  [Config(Name = "hero_id")]
  public int heroId;
  [Config(Name = "weight")]
  public float weight;
}
