﻿// Decompiled with JetBrains decompiler
// Type: QuestNew_Dlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class QuestNew_Dlg : UI.Dialog
{
  public QuestItemRenderer[] commonQuests;
  public QuestItemRenderer recommendQuest;
  public GameObject recommendContent;
  public GameObject commonContent;
  public GameObject recommendFinish;
  public GameObject commonFinish;
  public QuestDetail questDetail;
  public UILabel title;
  public GameObject allFinishGameObject;

  private void ResetButton()
  {
  }

  private void CreateFakeDatas()
  {
    if (ConfigManager.inst.DB_AllianceQuest == null)
      ConfigManager.inst.DB_AllianceQuest = new ConfigAllianceQuest();
    for (int index = 0; index < 5; ++index)
    {
      int longId = (int) ClientIdCreater.CreateLongId(ClientIdCreater.IdType.ITEM);
      ConfigManager.inst.DB_AllianceQuest.CreateFakeData(longId);
      DBManager.inst.DB_Local_TimerQuest.CreateFakeData(longId, TimerQuestData.QuestStatus.STATUS_ACTIVE);
    }
  }

  private void SetItemVisiable(MonoBehaviour item, bool visable = false)
  {
    NGUITools.SetActive(item.gameObject, visable);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.AddEventHandler();
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.RemoveEventHandler();
    this.Clear();
    base.OnClose(orgParam);
  }

  private void DestroyEffect()
  {
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Quest.onDataRemoved += new System.Action<long>(this.Refresh);
    PlayerData.inst.QuestManager.OnQuestClaimed += new System.Action<QuestRewardAgent>(this.ClaimHandler);
  }

  private void Refresh(long id)
  {
    this.UpdateUI();
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Quest.onDataRemoved -= new System.Action<long>(this.Refresh);
    PlayerData.inst.QuestManager.OnQuestClaimed -= new System.Action<QuestRewardAgent>(this.ClaimHandler);
  }

  private void Clear()
  {
    for (int index = 0; index < this.commonQuests.Length; ++index)
      this.commonQuests[index].Clear();
    this.questDetail.Clear();
    this.recommendQuest.Clear();
  }

  private void UpdateUI()
  {
    this.RefreshEmpireQuest();
  }

  private void RefreshAllainceQuest()
  {
    this.title.text = ScriptLocalization.Get("id_uppercase_alliance_quests", true);
  }

  private void RefreshEmpireQuest()
  {
    bool flag = true;
    QuestItemRenderer questItemRenderer = (QuestItemRenderer) null;
    if (DBManager.inst.DB_Quest.RecommendQuest == null)
    {
      NGUITools.SetActive(this.recommendFinish, true);
      NGUITools.SetActive(this.recommendContent, false);
    }
    else
    {
      flag = false;
      NGUITools.SetActive(this.recommendFinish, false);
      NGUITools.SetActive(this.recommendContent, true);
      this.recommendQuest.SetData(DBManager.inst.DB_Quest.RecommendQuest.QuestID);
      this.recommendQuest.OnQuestItemClickDelegate = new System.Action<long>(this.OnQuestItemClick);
      questItemRenderer = this.recommendQuest;
    }
    this.title.text = ScriptLocalization.Get("id_uppercase_quests", true);
    List<QuestData2> commonQuests = DBManager.inst.DB_Quest.CommonQuests;
    if (commonQuests == null || commonQuests.Count == 0)
    {
      NGUITools.SetActive(this.commonFinish, true);
      NGUITools.SetActive(this.commonContent, false);
    }
    else
    {
      flag = false;
      NGUITools.SetActive(this.commonFinish, false);
      NGUITools.SetActive(this.commonContent, true);
      for (int index = 0; index < this.commonQuests.Length; ++index)
      {
        QuestItemRenderer commonQuest = this.commonQuests[index];
        if (index < commonQuests.Count)
        {
          NGUITools.SetActive(commonQuest.gameObject, true);
          commonQuest.SetData(commonQuests[index].QuestID);
          commonQuest.OnQuestItemClickDelegate = new System.Action<long>(this.OnQuestItemClick);
          if ((UnityEngine.Object) questItemRenderer == (UnityEngine.Object) null)
            questItemRenderer = commonQuest;
        }
        else
          NGUITools.SetActive(commonQuest.gameObject, false);
      }
    }
    if (flag)
    {
      NGUITools.SetActive(this.questDetail.gameObject, false);
      NGUITools.SetActive(this.allFinishGameObject, true);
    }
    if (!((UnityEngine.Object) questItemRenderer != (UnityEngine.Object) null))
      return;
    if (!questItemRenderer.Selected)
      questItemRenderer.Selected = true;
    else
      this.OnQuestItemClick(questItemRenderer.QuestID);
  }

  private void OnQuestItemClick(long questId)
  {
    if ((UnityEngine.Object) this.questDetail != (UnityEngine.Object) null)
      this.questDetail.SetData(questId);
    this.recommendQuest.Selected = this.recommendQuest.QuestID == questId;
    for (int index = 0; index < this.commonQuests.Length; ++index)
    {
      QuestItemRenderer commonQuest = this.commonQuests[index];
      commonQuest.Selected = commonQuest.QuestID == questId;
    }
  }

  private void ClaimHandler(QuestRewardAgent agent)
  {
    this.UpdateUI();
  }
}
