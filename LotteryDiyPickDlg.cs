﻿// Decompiled with JetBrains decompiler
// Type: LotteryDiyPickDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class LotteryDiyPickDlg : UI.Dialog
{
  private Dictionary<int, LotteryDiyItemRenderer> _leftItemDict = new Dictionary<int, LotteryDiyItemRenderer>();
  private Dictionary<int, LotteryDiyItemRenderer> _rightItemDict = new Dictionary<int, LotteryDiyItemRenderer>();
  private GameObjectPool _leftItemPool = new GameObjectPool();
  private GameObjectPool _rightItemPool = new GameObjectPool();
  private int _curPickedTab = -1;
  private List<LotteryDiyBaseData.RewardData> _pickedRewardDataList = new List<LotteryDiyBaseData.RewardData>();
  private const int PICKED_TOTAL_REWARD = 9;
  public UIToggle[] tabCategories;
  public UILabel panelTitle;
  public GameObject freeBadgeNode;
  public UITexture panelBkgFrame;
  public UITexture currentPropsTexture;
  public UILabel currentPropsCountText;
  public GameObject itemTipsNode;
  public UILabel itemName;
  public UILabel itemDetail;
  public UILabel pickRankHint;
  public UnityEngine.Animation pickRankHintAnim;
  public UIScrollView leftScrollView;
  public UITable leftTable;
  public GameObject leftItemPrefab;
  public GameObject leftItemRoot;
  public UIScrollView rightScrollView;
  public UITable rightTable;
  public GameObject rightItemPrefab;
  public GameObject rightItemRoot;
  private Color pickHintTextColor;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.tabCategories[0].value = true;
    this.pickHintTextColor = this.pickRankHint.color;
    this._leftItemPool.Initialize(this.leftItemPrefab, this.leftItemRoot);
    this._rightItemPool.Initialize(this.rightItemPrefab, this.rightItemRoot);
    this.AddEventHandler();
    this.ShowMainContent();
    this.ShowProps();
    this.ShowLotteryLeftContent();
    this.ShowLotteryRightContent(0);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.HideTips();
    this.RemoveEventHandler();
    this.ClearData(LotteryDiyPayload.LotteryArea.LEFT);
    this.ClearData(LotteryDiyPayload.LotteryArea.RIGHT);
    this.ClearCachedData();
  }

  public void OnPickConfirmBtnPressed()
  {
    if (this._pickedRewardDataList.Count != 9)
    {
      this.CheckTabPageSkip(-1);
      this.ShowPickTips();
    }
    else
      UIManager.inst.ShowConfirmationBox(Utils.XLAT("event_lucky_draw_confirm_prize_title"), Utils.XLAT("event_lucky_draw_confirm_prize_description"), ScriptLocalization.Get("id_uppercase_confirm", true), ScriptLocalization.Get("id_uppercase_cancel", true), ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, (System.Action) (() =>
      {
        ArrayList arrayList = new ArrayList();
        for (int index = 0; index < this._pickedRewardDataList.Count; ++index)
          arrayList.Add((object) this._pickedRewardDataList[index].id);
        RequestManager.inst.SendRequest("casino:saveLottery", new Hashtable()
        {
          {
            (object) "uid",
            (object) PlayerData.inst.uid
          },
          {
            (object) "selected",
            (object) arrayList.ToArray()
          }
        }, (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          LotteryDiyPayload.Instance.LotteryData.Decode(data as Hashtable);
          UIManager.inst.OpenDlg("LotteryDiy/LotteryDiyPlayDlg", (UI.Dialog.DialogParameter) null, true, true, true);
        }), true);
      }), (System.Action) null, (System.Action) null);
  }

  public void OnFreeRewardBtnPressed()
  {
    LotteryDiyPayload.Instance.RequestServerData((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenPopup("LotteryDiy/LotteryDiyFreeRewardPopup", (Popup.PopupParameter) new LotteryDiyFreeRewardPopup.Parameter()
      {
        freeClaimedHandler = new System.Action(this.ShowMainContent)
      });
    }));
  }

  public void OnTabToggleUpdate()
  {
    int pickedTabIndex = this.GetPickedTabIndex();
    if (pickedTabIndex == this._curPickedTab || pickedTabIndex < 0)
      return;
    this._curPickedTab = pickedTabIndex;
    this.ShowLotteryRightContent(this._curPickedTab);
    this.ShowLotteryRankInfo(this._curPickedTab);
  }

  private void ShowPickTips()
  {
    this.StopCoroutine(this.ResetHintColor(0.0f));
    this.pickRankHint.color = Color.red;
    if (!((UnityEngine.Object) this.pickRankHintAnim != (UnityEngine.Object) null))
      return;
    this.pickRankHintAnim.Play();
    this.StartCoroutine(this.ResetHintColor(this.pickRankHintAnim.clip.length));
  }

  [DebuggerHidden]
  private IEnumerator ResetHintColor(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LotteryDiyPickDlg.\u003CResetHintColor\u003Ec__Iterator7D()
    {
      delay = delay,
      \u003C\u0024\u003Edelay = delay,
      \u003C\u003Ef__this = this
    };
  }

  private void ShowMainContent()
  {
    this.panelTitle.text = Utils.XLAT(LotteryDiyPayload.Instance.LotteryData.Title);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panelBkgFrame, "Texture/STATIC_TEXTURE/bg_diy_lottery", (System.Action<bool>) null, false, true, string.Empty);
    NGUITools.SetActive(this.freeBadgeNode, LotteryDiyPayload.Instance.LotteryData.HasFreeReward);
  }

  private void ShowProps()
  {
    this.OnItemDataChanged(LotteryDiyPayload.Instance.LotteryData.SpendItem);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(LotteryDiyPayload.Instance.LotteryData.SpendItem);
    if (itemStaticInfo == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.currentPropsTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
  }

  private void OnItemDataChanged(int itemId)
  {
    if (itemId != LotteryDiyPayload.Instance.LotteryData.SpendItem)
      return;
    this.currentPropsCountText.text = Utils.FormatThousands(LotteryDiyPayload.Instance.LotteryData.SpendItemCount.ToString());
  }

  private int GetPickedTabIndex()
  {
    for (int index = 0; index < this.tabCategories.Length; ++index)
    {
      if (this.tabCategories[index].value)
        return index;
    }
    return -1;
  }

  private void ShowLotteryRankInfo(int curTab)
  {
    Dictionary<int, LotteryDiyBaseData.LotteryDiyReward> lotteryRewards = LotteryDiyPayload.Instance.LotteryData.LotteryRewards;
    Dictionary<string, string> para = new Dictionary<string, string>();
    if (lotteryRewards == null || !lotteryRewards.ContainsKey(curTab + 1))
      return;
    int count = lotteryRewards[curTab + 1].RewardData.Count;
    int limitCount = lotteryRewards[curTab + 1].LimitCount;
    para.Add("0", (curTab + 1).ToString());
    para.Add("1", limitCount.ToString());
    para.Add("2", count.ToString());
    this.pickRankHint.text = ScriptLocalization.GetWithPara("event_lucky_draw_choose_prize_conditions", para, true);
  }

  private void ShowLotteryLeftContent()
  {
    this.ClearData(LotteryDiyPayload.LotteryArea.LEFT);
    for (int key = 0; key < 9; ++key)
    {
      LotteryDiyItemRenderer itemRenderer = this.CreateItemRenderer((LotteryDiyBaseData.RewardData) null, LotteryDiyPayload.LotteryArea.LEFT);
      this._leftItemDict.Add(key, itemRenderer);
    }
    this.Reposition(LotteryDiyPayload.LotteryArea.LEFT);
  }

  private void ShowLotteryRightContent(int curTab)
  {
    this.ClearData(LotteryDiyPayload.LotteryArea.RIGHT);
    Dictionary<int, LotteryDiyBaseData.LotteryDiyReward> lotteryRewards = LotteryDiyPayload.Instance.LotteryData.LotteryRewards;
    if (lotteryRewards != null && lotteryRewards.ContainsKey(curTab + 1))
    {
      int count = lotteryRewards[curTab + 1].RewardData.Count;
      for (int key = 0; key < count; ++key)
      {
        LotteryDiyItemRenderer itemRenderer = this.CreateItemRenderer(lotteryRewards[curTab + 1].RewardData[key], LotteryDiyPayload.LotteryArea.RIGHT);
        this._rightItemDict.Add(key, itemRenderer);
      }
    }
    this.UpdateLotteryRewardStatus();
    this.Reposition(LotteryDiyPayload.LotteryArea.RIGHT);
  }

  private void UpdateLotteryRewardStatus()
  {
    if (this._rightItemDict == null)
      return;
    Dictionary<int, LotteryDiyItemRenderer>.ValueCollection.Enumerator enumerator = this._rightItemDict.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      enumerator.Current.UpdateLotteryItemStatus(false);
      if (enumerator.Current.RewardData != null)
      {
        for (int index = 0; index < this._pickedRewardDataList.Count; ++index)
        {
          if (enumerator.Current.RewardData.id == this._pickedRewardDataList[index].id)
            enumerator.Current.UpdateLotteryItemStatus(true);
        }
      }
    }
  }

  private void UpdateLeftPickedContent()
  {
    int key1;
    for (key1 = 0; key1 < this._pickedRewardDataList.Count; ++key1)
    {
      if (this._leftItemDict.ContainsKey(key1))
        this._leftItemDict[key1].SetData(this._pickedRewardDataList[key1], LotteryDiyPayload.LotteryArea.LEFT, -1, false);
    }
    if (9 - key1 <= 0)
      return;
    for (int key2 = key1; key2 < 9; ++key2)
    {
      if (this._leftItemDict.ContainsKey(key2))
        this._leftItemDict[key2].SetData((LotteryDiyBaseData.RewardData) null, LotteryDiyPayload.LotteryArea.LEFT, -1, false);
    }
  }

  private void OnItemSinglePressed(LotteryDiyBaseData.RewardData rewardData)
  {
    if (rewardData == null)
      return;
    this.HideTips();
    if (!this.CheckCurTabSelectable() && !this.IsPickedRewardDataExist(rewardData))
    {
      this.ShowPickTips();
    }
    else
    {
      if (this.IsPickedRewardDataExist(rewardData))
        this._pickedRewardDataList.RemoveAll((Predicate<LotteryDiyBaseData.RewardData>) (x => x.id == rewardData.id));
      else if (this._pickedRewardDataList.Count < 9)
        this._pickedRewardDataList.Add(rewardData);
      this.UpdateLeftPickedContent();
      this.UpdateLotteryRewardStatus();
      this.CheckTabPageSkip(-1);
    }
  }

  private bool CheckCurTabSelectable()
  {
    int num = 0;
    Dictionary<int, LotteryDiyBaseData.LotteryDiyReward> lotteryRewards = LotteryDiyPayload.Instance.LotteryData.LotteryRewards;
    if (lotteryRewards != null && lotteryRewards.ContainsKey(this._curPickedTab + 1))
    {
      List<LotteryDiyBaseData.RewardData> rewardData = lotteryRewards[this._curPickedTab + 1].RewardData;
      for (int index1 = 0; index1 < rewardData.Count; ++index1)
      {
        for (int index2 = 0; index2 < this._pickedRewardDataList.Count; ++index2)
        {
          if (rewardData[index1].id == this._pickedRewardDataList[index2].id)
            ++num;
        }
      }
      if (num >= lotteryRewards[this._curPickedTab + 1].LimitCount)
        return false;
    }
    return true;
  }

  private void CheckTabPageSkip(int checkTab = -1)
  {
    if (this._pickedRewardDataList.Count == 9)
      return;
    int num1 = 0;
    int num2 = checkTab < 0 ? this._curPickedTab : checkTab;
    Dictionary<int, LotteryDiyBaseData.LotteryDiyReward> lotteryRewards = LotteryDiyPayload.Instance.LotteryData.LotteryRewards;
    if (lotteryRewards == null || !lotteryRewards.ContainsKey(this._curPickedTab + 1))
      return;
    List<LotteryDiyBaseData.RewardData> rewardData = lotteryRewards[this._curPickedTab + 1].RewardData;
    for (int index1 = 0; index1 < rewardData.Count; ++index1)
    {
      for (int index2 = 0; index2 < this._pickedRewardDataList.Count; ++index2)
      {
        if (rewardData[index1].id == this._pickedRewardDataList[index2].id)
          ++num1;
      }
    }
    int index;
    if ((index = num2 + 1) == lotteryRewards.Count)
      index = 0;
    if (num1 < lotteryRewards[this._curPickedTab + 1].LimitCount || index >= lotteryRewards.Count)
      return;
    this.tabCategories[index].value = true;
    this.ShowLotteryRightContent(index);
    this.CheckTabPageSkip(index);
  }

  private bool IsPickedRewardDataExist(LotteryDiyBaseData.RewardData rewardData)
  {
    return this._pickedRewardDataList.Find((Predicate<LotteryDiyBaseData.RewardData>) (x => x.id == rewardData.id)) != null;
  }

  private void OnItemTipsDisplay(LotteryDiyBaseData.RewardData rewardData, LotteryDiyPayload.LotteryArea lotteryArea, Vector3 itemPosition)
  {
    if (rewardData == null)
      return;
    this.StopCoroutine("HideItemTips");
    this.UpdateItemTipsPosition(lotteryArea, itemPosition);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardData.itemId);
    if (itemStaticInfo != null)
    {
      this.itemName.text = itemStaticInfo.LocName;
      this.itemDetail.text = itemStaticInfo.LocDescription;
      NGUITools.SetActive(this.itemTipsNode, true);
    }
    this.StartCoroutine("HideItemTips");
  }

  private void UpdateItemTipsPosition(LotteryDiyPayload.LotteryArea lotteryArea, Vector3 itemPosition)
  {
    float num1;
    float num2;
    if (lotteryArea == LotteryDiyPayload.LotteryArea.RIGHT)
    {
      num1 = -80f;
      num2 = 200f;
    }
    else
    {
      num1 = -700f;
      num2 = 400f;
    }
    this.itemTipsNode.transform.localPosition = new Vector3(itemPosition.x + num1, itemPosition.y + num2, itemPosition.z);
  }

  [DebuggerHidden]
  private IEnumerator HideItemTips()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LotteryDiyPickDlg.\u003CHideItemTips\u003Ec__Iterator7E()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void HideTips()
  {
    NGUITools.SetActive(this.itemTipsNode, false);
  }

  private void ClearData(LotteryDiyPayload.LotteryArea lotteryArea)
  {
    Dictionary<int, LotteryDiyItemRenderer> dictionary;
    GameObjectPool gameObjectPool;
    if (lotteryArea == LotteryDiyPayload.LotteryArea.RIGHT)
    {
      dictionary = this._rightItemDict;
      gameObjectPool = this._rightItemPool;
    }
    else
    {
      dictionary = this._leftItemDict;
      gameObjectPool = this._leftItemPool;
    }
    using (Dictionary<int, LotteryDiyItemRenderer>.Enumerator enumerator = dictionary.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, LotteryDiyItemRenderer> current = enumerator.Current;
        current.Value.onItemTipsDisplay -= new System.Action<LotteryDiyBaseData.RewardData, LotteryDiyPayload.LotteryArea, Vector3>(this.OnItemTipsDisplay);
        current.Value.onItemSinglePressed -= new System.Action<LotteryDiyBaseData.RewardData>(this.OnItemSinglePressed);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        gameObjectPool.Release(gameObject);
      }
    }
    dictionary.Clear();
    gameObjectPool.Clear();
  }

  private void ClearCachedData()
  {
    if (this._pickedRewardDataList == null)
      return;
    this._pickedRewardDataList.Clear();
  }

  private void Reposition(LotteryDiyPayload.LotteryArea lotteryArea)
  {
    UITable uiTable;
    UIScrollView uiScrollView;
    if (lotteryArea == LotteryDiyPayload.LotteryArea.RIGHT)
    {
      uiTable = this.rightTable;
      uiScrollView = this.rightScrollView;
    }
    else
    {
      uiTable = this.leftTable;
      uiScrollView = this.leftScrollView;
    }
    uiTable.repositionNow = true;
    uiTable.Reposition();
    uiScrollView.ResetPosition();
    uiScrollView.gameObject.SetActive(false);
    uiScrollView.gameObject.SetActive(true);
  }

  private LotteryDiyItemRenderer CreateItemRenderer(LotteryDiyBaseData.RewardData rewardData, LotteryDiyPayload.LotteryArea lotteryArea)
  {
    GameObjectPool gameObjectPool;
    UITable uiTable;
    if (lotteryArea == LotteryDiyPayload.LotteryArea.RIGHT)
    {
      gameObjectPool = this._rightItemPool;
      uiTable = this.rightTable;
    }
    else
    {
      gameObjectPool = this._leftItemPool;
      uiTable = this.leftTable;
    }
    GameObject gameObject = gameObjectPool.AddChild(uiTable.gameObject);
    gameObject.SetActive(true);
    LotteryDiyItemRenderer component = gameObject.GetComponent<LotteryDiyItemRenderer>();
    component.SetData(rewardData, lotteryArea, -1, false);
    component.onItemTipsDisplay += new System.Action<LotteryDiyBaseData.RewardData, LotteryDiyPayload.LotteryArea, Vector3>(this.OnItemTipsDisplay);
    component.onItemSinglePressed += new System.Action<LotteryDiyBaseData.RewardData>(this.OnItemSinglePressed);
    return component;
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemDataChanged);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataChanged);
  }
}
