﻿// Decompiled with JetBrains decompiler
// Type: WorldBossActivityDetailsDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class WorldBossActivityDetailsDlg : UI.Dialog
{
  private string DEFAULT_RANK = "--";
  private const int REQUEST_DELAY_THREE = 3;
  private const int REQUEST_DELAY_ONE = 1;
  private const string BOSS_BANNER_PATH = "Texture/AllianceBoss/world_boss";
  private const string BOSS_PORTRAIT_PATH = "Texture/AllianceBoss/kingdom_boss_01";
  private const string DAMAGE_AWARD_PATH = "Texture/ItemIcons/item_gve_kill_chest_6";
  private const string KILL_AWARD_PATH = "Texture/ItemIcons/item_gve_kill_chest_6";
  private const string AUCTION_AWARD_PATH = "Texture/ItemIcons/icon_iap_gold2";
  public UILabel cdCountDown;
  public UILabel challengeDes;
  public UILabel awardIntroduce;
  public UILabel harmAward;
  public UILabel killAward;
  public UILabel auctionAward;
  public UILabel viewRank;
  public UILabel viewRankInPro;
  public UILabel title;
  public UITexture bossBanner;
  public UITexture bossPortrait;
  public UITexture damageAwardImage;
  public UITexture killIAwardmage;
  public UITexture auctionAwardImage;
  public GameObject bossInfo;
  public UILabel bossName;
  public UILabel bossHP;
  public UILabel bossExp;
  public UILabel myDamage;
  public UILabel myRank;
  public UILabel attackBtnLabel;
  public UISlider slider;
  public GameObject buttonGroup;
  public UIButton viewRankBtn;
  public UIButton attackBtn;
  private int damage;
  private int rank;
  private KingdomBossData bossDBInfo;
  private Coroutine secondRequestCo;
  private Coroutine requestCoroutine;
  private Coroutine requestActivityTimeCo;

  private void UpdateLabels()
  {
    this.title.text = Utils.XLAT("event_kingdom_giant_title");
    this.harmAward.text = Utils.XLAT("event_kingdom_giant_damage_rewards");
    this.killAward.text = Utils.XLAT("event_kingdom_giant_kill_rewards");
    this.auctionAward.text = Utils.XLAT("event_kingdom_giant_gold_rewards");
    this.viewRank.text = Utils.XLAT("leaderboards_uppercase_damage_rankings");
    this.awardIntroduce.text = Utils.XLAT("id_uppercase_rewards");
  }

  private void UpdateUI()
  {
    this.UpdateTimer();
    this.UpdateLabels();
    this.UpdateSpecificContent();
    this.LoadAwardsTextures();
  }

  private void LoadAwardsTextures()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.damageAwardImage, "Texture/ItemIcons/item_gve_kill_chest_6", (System.Action<bool>) null, false, true, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.killIAwardmage, "Texture/ItemIcons/item_gve_kill_chest_6", (System.Action<bool>) null, false, true, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.auctionAwardImage, "Texture/ItemIcons/icon_iap_gold2", (System.Action<bool>) null, false, true, string.Empty);
  }

  private void UpdateSpecificContent()
  {
    if (ActivityManager.Intance.worldBoss.IsStart() && WorldBossActivityPayload.Instance.BossInfoReady != 0)
    {
      this.bossBanner.gameObject.SetActive(false);
      this.viewRankBtn.gameObject.SetActive(false);
      this.challengeDes.gameObject.SetActive(false);
      this.UpdateBossInfo(true);
    }
    else
    {
      this.bossBanner.gameObject.SetActive(true);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.bossBanner, "Texture/AllianceBoss/world_boss", (System.Action<bool>) null, false, true, string.Empty);
      this.viewRankBtn.gameObject.SetActive(true);
      this.challengeDes.gameObject.SetActive(true);
      this.challengeDes.text = Utils.XLAT("event_kingdom_giant_description");
      this.UpdateBossInfo(false);
    }
  }

  private void UpdateBossInfo(bool active)
  {
    this.bossInfo.gameObject.SetActive(active);
    this.buttonGroup.SetActive(active);
    if (active)
      this.attackBtn.isEnabled = WorldBossActivityPayload.Instance.BossInfoReady != 0;
    if (!active)
      return;
    this.UpdateBossDetails();
  }

  private void UpdateBossDetails()
  {
    Dictionary<string, string> para = new Dictionary<string, string>();
    this.bossHP.text = Utils.XLAT("id_uppercase_hp");
    this.attackBtnLabel.text = Utils.XLAT("pve_monster_uppercase_attack");
    this.viewRankInPro.text = Utils.XLAT("leaderboards_uppercase_damage_rankings");
    if (this.bossDBInfo != null && WorldBossActivityPayload.Instance.BossInfoReady != 0)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.bossPortrait, "Texture/AllianceBoss/kingdom_boss_01", (System.Action<bool>) null, false, true, string.Empty);
      KingdomBossStaticInfo kingdomBossStaticInfo = ConfigManager.inst.DB_KingdomBoss.GetItem(this.bossDBInfo.bossId);
      if (kingdomBossStaticInfo != null)
      {
        int bossLevel = kingdomBossStaticInfo.BossLevel;
        para.Clear();
        para.Add("0", bossLevel.ToString());
        this.bossName.text = ScriptLocalization.GetWithPara("monster_event_kingdom_giant_name", para, true);
      }
      float num = (float) this.bossDBInfo.CurrentTroopCount / (float) this.bossDBInfo.StartTroopCount;
      this.bossExp.text = string.Format("{0:0.00%}", (object) num);
      this.slider.value = num;
      if (WorldBossActivityPayload.Instance.IsPayloadNull)
      {
        this.myDamage.text = Utils.XLAT("leaderboards_your_damage") + "0";
        this.myRank.text = Utils.XLAT("leader_board_rank") + this.DEFAULT_RANK;
      }
      else
      {
        this.myDamage.text = Utils.XLAT("leaderboards_your_damage") + WorldBossActivityPayload.Instance.Damage;
        this.myRank.text = Utils.XLAT("leader_board_rank") + WorldBossActivityPayload.Instance.Rank;
      }
    }
    else
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.bossPortrait, "Texture/AllianceBoss/kingdom_boss_01", (System.Action<bool>) null, true, true, string.Empty);
      para.Clear();
      para.Add("0", this.DEFAULT_RANK);
      this.bossName.text = ScriptLocalization.GetWithPara("monster_event_kingdom_giant_name", para, true);
      this.bossExp.text = "100%";
      this.slider.value = 1f;
      this.myDamage.text = Utils.XLAT("leaderboards_your_damage") + "0";
      this.myRank.text = Utils.XLAT("leader_board_rank") + this.DEFAULT_RANK;
    }
  }

  public void OnSecond(int serverTime)
  {
    this.UpdateTimer();
  }

  private void OnActivityTimeReset(bool ret, object data)
  {
    WorldBossActivityPayload.Instance.NeedToRefreshActivityMainDlg = true;
    this.UpdateSpecificContent();
  }

  private void UpdateTimer()
  {
    string str = Utils.FormatTime(ActivityManager.Intance.worldBoss.RemainTime, true, false, true);
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("1", str);
    if (ActivityManager.Intance.worldBoss.IsStart() && WorldBossActivityPayload.Instance.BossInfoReady != 0)
    {
      this.cdCountDown.text = ScriptLocalization.GetWithPara("event_finishes_num", para, true);
      if (ActivityManager.Intance.worldBoss.RemainTime != 1)
        return;
      this.requestActivityTimeCo = Utils.ExecuteInSecs(1f, (System.Action) (() => WorldBossActivityPayload.Instance.RequestNextActivityTime(new System.Action<bool, object>(this.OnActivityTimeReset), new Hashtable()
      {
        {
          (object) "kingdom_id",
          (object) PlayerData.inst.playerCityData.cityLocation.K
        }
      })));
    }
    else
    {
      this.cdCountDown.text = ScriptLocalization.GetWithPara("event_starts_num", para, true);
      if (ActivityManager.Intance.worldBoss.RemainTime != 1)
        return;
      this.secondRequestCo = Utils.ExecuteInSecs(3f, (System.Action) (() => this.RequestData()));
    }
  }

  public void OnViewRankBtnClicked()
  {
    UIManager.inst.OpenDlg("LeaderBoard/WorldLeaderBoardDetailDlg", (UI.Dialog.DialogParameter) new LeaderBoardDetailDlg.LeaderBoardDetailDlgParamer()
    {
      method = "WorldBoss:getRank",
      title = ScriptLocalization.Get("leaderboards_uppercase_damage_rankings", true),
      headerTitle = "id_uppercase_damage",
      isShowKingdom = false,
      param = Utils.Hash((object) "kingdom_id", (object) PlayerData.inst.playerCityData.cityLocation.K),
      hideKingdomTitle = true
    }, true, true, true);
  }

  public void OnAttackBtnClicked()
  {
    if (this.bossDBInfo == null)
      return;
    if (!MapUtils.CanGotoTarget(this.bossDBInfo.kingdomId))
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    }
    else
    {
      PVPMap.PendingGotoRequest = new Coordinate(this.bossDBInfo.kingdomId, this.bossDBInfo.Location.X, this.bossDBInfo.Location.Y);
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PitMode && GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
        UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
        {
          GameEngine.Instance.CurrentGameMode = !MapUtils.IsPitWorld(this.bossDBInfo.kingdomId) ? GameEngine.GameMode.PVPMode : GameEngine.GameMode.PitMode;
          PVPSystem.Instance.Map.GotoLocation(new Coordinate(this.bossDBInfo.kingdomId, this.bossDBInfo.Location.X, this.bossDBInfo.Location.Y), false);
        }));
      else
        PVPSystem.Instance.Map.GotoLocation(new Coordinate(this.bossDBInfo.kingdomId, this.bossDBInfo.Location.X, this.bossDBInfo.Location.Y), false);
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }

  private void OnDBUpdated(long bossID)
  {
    this.bossDBInfo = DBManager.inst.DB_KingdomBossDB.Get((int) bossID);
  }

  private void OnRequestedDataReceived()
  {
    this.UpdateTimer();
    this.UpdateSpecificContent();
  }

  private void RequestData()
  {
    WorldBossActivityPayload.Instance.RequestWorldBossData((System.Action<bool, object>) null, new Hashtable()
    {
      {
        (object) "kingdom_id",
        (object) PlayerData.inst.playerCityData.cityLocation.K
      }
    });
  }

  private void Release()
  {
    if (this.secondRequestCo != null)
    {
      Utils.StopCoroutine(this.secondRequestCo);
      this.secondRequestCo = (Coroutine) null;
    }
    if (this.requestCoroutine != null)
    {
      Utils.StopCoroutine(this.requestCoroutine);
      this.requestCoroutine = (Coroutine) null;
    }
    if (this.requestActivityTimeCo == null)
      return;
    Utils.StopCoroutine(this.requestActivityTimeCo);
    this.requestActivityTimeCo = (Coroutine) null;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    DBManager.inst.DB_KingdomBossDB.onDataChanged += new System.Action<long>(this.OnDBUpdated);
    WorldBossActivityPayload.Instance.OnRequestDataReceived += new System.Action(this.OnRequestedDataReceived);
    if (orgParam != null)
      this.bossDBInfo = (orgParam as WorldBossActivityDetailsDlg.Parameter).bossDBInfo;
    else if (WorldBossActivityPayload.Instance.BossInfoReady != 0)
      this.bossDBInfo = WorldBossActivityPayload.Instance.BossDBInfo;
    this.UpdateUI();
  }

  protected override void _Hide(UIControler.UIParameter orgParam)
  {
    base._Hide(orgParam);
    DBManager.inst.DB_KingdomBossDB.onDataChanged -= new System.Action<long>(this.OnDBUpdated);
    WorldBossActivityPayload.Instance.OnRequestDataReceived -= new System.Action(this.OnRequestedDataReceived);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    this.Release();
    WorldBossActivityPayload.Instance.ClearCachedData();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public KingdomBossData bossDBInfo;
  }
}
