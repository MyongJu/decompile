﻿// Decompiled with JetBrains decompiler
// Type: ResearchDetailInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class ResearchDetailInfoDlg : UI.Dialog
{
  public UILabel BigTitle;
  public UILabel DetailDescription;
  public UILabel StatsDescription;
  public UILabel Rank;
  public GameObject mDetailsPage;
  public GameObject mStatsPage;
  public GameObject benifitContent;
  public GameObject StatsRow;
  public GameObject[] BennifitTitles;
  public UITable listTable;
  public UITexture researchIcon;
  private TechLevel techLevel;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    this.techLevel = (orgParam as ResearchDetailInfoDlg.UIParam).techLevel;
    this.UpdateUI();
  }

  public void UpdateUI()
  {
    this.BigTitle.text = this.techLevel.Name;
    this.DetailDescription.text = this.techLevel.Desc;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.researchIcon, this.techLevel.Tech.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    this.Rank.text = this.techLevel.Level.ToString() + "/" + (object) ResearchManager.inst.GetTechSteps(this.techLevel.Tech);
    this.showAllBennifitInfo();
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnBackBtnPressed()
  {
    this.mDetailsPage.SetActive(true);
    this.mStatsPage.SetActive(false);
  }

  public void OnBackToDetailPressed()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnInfoBtnPressed()
  {
    this.mDetailsPage.SetActive(false);
    this.mStatsPage.SetActive(true);
    this.showAllBennifitInfo();
  }

  private void showBenifitInfo()
  {
    this.mDetailsPage.SetActive(true);
    this.mStatsPage.SetActive(false);
    using (List<Effect>.Enumerator enumerator = this.techLevel.Effects.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Effect current = enumerator.Current;
        ResearchBennifitContent component = this.benifitContent.GetComponent<ResearchBennifitContent>();
        if (this.techLevel.Tech.ID.Contains("research_troop_scout"))
          component.SetContent(current.Name, this.techLevel.Level.ToString(), Utils.XLAT(string.Format("research_troop_scout_{0}_description", (object) this.techLevel.Level.ToString())), NGUIText.Alignment.Left);
        else
          component.SetContent(current.Name, this.techLevel.Level.ToString(), current.FormattedModifier, NGUIText.Alignment.Right);
      }
    }
  }

  private void showAllBennifitInfo()
  {
    this.StatsDescription.text = this.techLevel.Desc;
    Transform transform = this.listTable.transform;
    for (int index = 0; index < transform.childCount; ++index)
      UnityEngine.Object.Destroy((UnityEngine.Object) transform.GetChild(index).gameObject);
    for (int index = 0; index < 2; ++index)
    {
      if (index < this.techLevel.Effects.Count)
      {
        this.BennifitTitles[index].SetActive(true);
        this.BennifitTitles[index].GetComponent<UILabel>().text = this.techLevel.Effects[index].Name;
      }
      else
        this.BennifitTitles[index].SetActive(false);
    }
    this.StatsRow.SetActive(false);
    List<TechLevel> techLevelList = ResearchManager.inst.TechLevelList(this.techLevel.Tech.InternalID);
    int num = ResearchManager.inst.Rank(this.techLevel.Tech);
    for (int index1 = 0; index1 < techLevelList.Count; ++index1)
    {
      TechLevel techLevel = techLevelList[index1];
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.StatsRow);
      gameObject.SetActive(true);
      gameObject.transform.parent = transform;
      gameObject.transform.localScale = Vector3.one;
      gameObject.transform.Find("Level").GetComponent<UILabel>().text = techLevel.Level.ToString();
      for (int index2 = 0; index2 < 2; ++index2)
      {
        if (index2 < techLevel.Effects.Count)
        {
          Effect effect = techLevel.Effects[index2];
          UILabel component = gameObject.transform.Find("Value_" + (object) index2).GetComponent<UILabel>();
          if (techLevel.Tech.ID.Contains("research_troop_scout"))
          {
            component.text = Utils.XLAT(string.Format("research_troop_scout_{0}_description", (object) techLevel.Level));
            component.alignment = NGUIText.Alignment.Left;
          }
          else
          {
            component.text = effect.FormattedModifier;
            component.alignment = NGUIText.Alignment.Right;
          }
        }
        else
          gameObject.transform.Find("Value_" + (object) index2).GetComponent<UILabel>().text = string.Empty;
      }
      if (techLevel.Level == num)
        gameObject.transform.Find("CurrentItemBackGround").GetComponent<UISprite>().enabled = true;
      else
        gameObject.transform.Find("CurrentItemBackGround").GetComponent<UISprite>().enabled = false;
    }
    transform.GetComponent<UITable>().repositionNow = true;
  }

  public class UIParam : UI.Dialog.DialogParameter
  {
    public TechLevel techLevel;
  }
}
