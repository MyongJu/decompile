﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossRewardItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class AllianceBossRewardItemRenderer : MonoBehaviour
{
  private Dictionary<int, AllianceBossRewardItemRenderer.RankRewardPair> dicRankReward = new Dictionary<int, AllianceBossRewardItemRenderer.RankRewardPair>();
  public UILabel title;
  public UILabel subtitle;
  public AllianceBossRewardItemRenderer.IconInfo[] itemIconInfos;
  private AllianceBossInfo rankRewardInfo;
  private AllianceBossRankRewardInfo rankRewardRatioInfo;

  public void SetData(AllianceBossInfo info, AllianceBossRankRewardInfo rankRewardRatioInfo)
  {
    if (info == null || rankRewardRatioInfo == null)
      return;
    this.rankRewardInfo = info;
    this.rankRewardRatioInfo = rankRewardRatioInfo;
    this.UpdateUI();
  }

  public void UpdateUI()
  {
    this.title.text = Utils.XLAT("event_ranking_rewards_name");
    this.subtitle.text = string.Format(Utils.XLAT("event_rank_num"), (object) this.rankRewardRatioInfo.rank);
    this.dicRankReward.Clear();
    this.dicRankReward.Add(0, new AllianceBossRewardItemRenderer.RankRewardPair()
    {
      rankRewardName = "alliance_fund",
      rankRewardCount = (long) ((double) this.rankRewardInfo.fund * (double) this.rankRewardRatioInfo.fundRatio)
    });
    this.dicRankReward.Add(1, new AllianceBossRewardItemRenderer.RankRewardPair()
    {
      rankRewardName = "alliance_honor",
      rankRewardCount = (long) ((double) this.rankRewardInfo.honor * (double) this.rankRewardRatioInfo.honorRatio)
    });
    List<Reward.RewardsValuePair> allRewards = this.rankRewardInfo.AllRewards;
    Dictionary<int, float> rewardRatioDic = this.rankRewardRatioInfo.GetRewardRatioDic();
    int index1 = 0;
    int key = 2;
    while (index1 < allRewards.Count)
    {
      Reward.RewardsValuePair rewardsValuePair = allRewards[index1];
      this.dicRankReward.Add(key, new AllianceBossRewardItemRenderer.RankRewardPair()
      {
        rankRewardName = rewardsValuePair.internalID.ToString(),
        rankRewardCount = (long) ((double) rewardsValuePair.value * (double) rewardRatioDic[index1])
      });
      ++index1;
      ++key;
    }
    int index2 = 0;
    int index3 = 0;
    for (; index2 < this.dicRankReward.Count; ++index2)
    {
      if (!string.IsNullOrEmpty(this.dicRankReward[index2].rankRewardName) && this.dicRankReward[index2].rankRewardCount > 0L)
      {
        IconData iconData = this.GetIconData(this.dicRankReward[index2].rankRewardName, this.dicRankReward[index2].rankRewardCount);
        this.itemIconInfos[index3].itemIcon.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
        this.itemIconInfos[index3].itemIcon.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
        this.itemIconInfos[index3].itemIcon.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
        this.itemIconInfos[index3].itemIcon.FeedData((IComponentData) iconData);
        this.itemIconInfos[index3].itemName.text = iconData.contents[0];
        this.itemIconInfos[index3].itemNumber.text = Utils.FormatThousands(this.dicRankReward[index2].rankRewardCount.ToString());
        this.itemIconInfos[index3].itemName.transform.parent.gameObject.SetActive(true);
        ++index3;
      }
    }
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder.transform, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder.transform, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  private IconData GetIconData(string item_id, long count)
  {
    IconData iconData = new IconData();
    iconData.contents = new string[2];
    if (item_id.Contains("alliance_fund") || item_id.Contains("alliance_honor"))
    {
      iconData.image = "Texture/Alliance/" + item_id;
      iconData.contents[0] = Utils.XLAT(!item_id.Contains("alliance_fund") ? "id_honor" : "id_fund");
    }
    else
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(int.Parse(item_id));
      iconData.Data = (object) itemStaticInfo.internalId;
      iconData.image = itemStaticInfo.ImagePath;
      iconData.contents[0] = itemStaticInfo.LocName;
    }
    iconData.contents[1] = count.ToString();
    return iconData;
  }

  [Serializable]
  public class IconInfo
  {
    public Icon itemIcon;
    public UILabel itemName;
    public UILabel itemNumber;
  }

  public class RankRewardPair
  {
    public string rankRewardName;
    public long rankRewardCount;
  }
}
