﻿// Decompiled with JetBrains decompiler
// Type: ItemTipCustomInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ItemTipCustomInfo : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelTipText;
  [SerializeField]
  private UILabel _labelTipName;
  [SerializeField]
  private UILabel _labelTipDesc;
  [SerializeField]
  private UITexture _textureIcon;
  [SerializeField]
  private UITexture _textureFrame;

  public void SetData(ItemTipCustomInfo.ItemTipData itemTipData)
  {
    if (itemTipData == null)
      return;
    this._labelTipText.text = itemTipData.tipText;
    this._labelTipName.text = itemTipData.tipName;
    this._labelTipDesc.text = itemTipData.tipDesc;
    if (string.IsNullOrEmpty(itemTipData.iconPath))
      this._textureIcon.mainTexture = (Texture) null;
    else
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureIcon, itemTipData.iconPath, (System.Action<bool>) null, true, true, string.Empty);
    if (string.IsNullOrEmpty(itemTipData.framePath))
      this._textureFrame.mainTexture = (Texture) null;
    else
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureFrame, itemTipData.framePath, (System.Action<bool>) null, true, true, string.Empty);
  }

  public class ItemTipData
  {
    public string tipText;
    public string tipName;
    public string tipDesc;
    public string iconPath;
    public string framePath;

    public ItemTipData(string tipText, string tipName, string tipDesc, string iconPath, string framePath)
    {
      this.tipText = tipText;
      this.tipName = tipName;
      this.tipDesc = tipDesc;
      this.iconPath = iconPath;
      this.framePath = framePath;
    }
  }
}
