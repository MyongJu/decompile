﻿// Decompiled with JetBrains decompiler
// Type: MsgService
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Chat;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MsgService
{
  private static readonly MsgService _instance = new MsgService();
  public const int TYPE_MAIL = 1;
  public const int TYPE_CHAT = 2;
  private const string API_SEND_MSG = "send_msg";
  private const string API_SEND_GROUP_MSG = "send_group_msg";

  private MsgService()
  {
    this.MsgCheckFilter = false;
    this.MsgCheckUrl = "http://10.0.61.21:8080/filter";
    this.RTMCheckUrl = "http://10.0.61.21:8080/";
  }

  public bool MsgCheckFilter { set; get; }

  public string MsgCheckUrl { set; get; }

  public bool RTMFilter { set; get; }

  public string RTMCheckUrl { set; get; }

  public static MsgService Instance
  {
    get
    {
      return MsgService._instance;
    }
  }

  public void SendMessage(long channelId, ChatMessage msg)
  {
    GameObject gameObject = new GameObject();
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
    gameObject.AddComponent<MsgHttpPostProcessor>().DoPost(this.RTMCheckUrl, Utils.Object2Json((object) new Dictionary<string, object>()
    {
      {
        "method",
        (object) "send_msg"
      },
      {
        "params",
        (object) new Dictionary<string, object>()
        {
          {
            "token",
            (object) NetApi.inst.Token
          },
          {
            "mtype",
            (object) 100
          },
          {
            nameof (msg),
            this.MsgData("chat_message", msg.Encode())
          },
          {
            "channel_id",
            (object) channelId.ToString()
          }
        }
      }
    }));
  }

  public void SendGroupMessage(long channelId, ChatMessage msg, bool needPush)
  {
    msg.receiveChannelId = channelId;
    GameObject gameObject = new GameObject();
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
    MsgHttpPostProcessor httpPostProcessor = gameObject.AddComponent<MsgHttpPostProcessor>();
    Dictionary<string, object> dictionary1 = new Dictionary<string, object>();
    dictionary1.Add("method", (object) "send_group_msg");
    Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
    dictionary2.Add("token", (object) NetApi.inst.Token);
    dictionary2.Add("mtype", (object) 100);
    dictionary2.Add(nameof (msg), this.MsgData("chat_message", msg.Encode()));
    dictionary2.Add("channel_id", (object) channelId.ToString());
    if (needPush)
      dictionary2.Add("push_mtype", (object) 120);
    dictionary1.Add("params", (object) dictionary2);
    httpPostProcessor.DoPost(this.RTMCheckUrl, Utils.Object2Json((object) dictionary1));
  }

  private object MsgData(string action, object msg)
  {
    Hashtable hashtable = new Hashtable();
    hashtable[(object) "cmd"] = (object) action;
    hashtable[(object) "payload"] = msg;
    return (object) hashtable;
  }

  public void RemoteCheckChatContent(string raw_msg, int type, System.Action<bool, string> callback)
  {
    if (!this.MsgCheckFilter && callback != null)
    {
      callback(true, raw_msg);
    }
    else
    {
      GameObject gameObject = new GameObject();
      UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
      long uid = PlayerData.inst.uid;
      MsgHttpPostProcessor httpPostProcessor = gameObject.AddComponent<MsgHttpPostProcessor>();
      string content = string.Format("{0} {1}", (object) PlayerData.inst.userData.userName, (object) raw_msg);
      MsgChecker postData = new MsgChecker()
      {
        uid = uid,
        raw = raw_msg,
        content = content,
        type = type,
        code = MsgChecker.GetSign(uid, content)
      };
      httpPostProcessor.DoPost(this.MsgCheckUrl, postData, (MsgHttpPostProcessor.MsgPostCallback) ((result, status) =>
      {
        if (!result)
          callback(true, status.content);
        else if (status.errCode != 0)
        {
          UIManager.inst.toast.Show(status.errMsg, (System.Action) null, 4f, false);
          callback(false, string.Empty);
        }
        else if (status.unmuteTimestamp > 0)
        {
          PlayerData.inst.userData.ShutUpTime = status.unmuteTimestamp;
          callback(false, string.Empty);
        }
        else if (!status.isPassed)
        {
          callback(false, string.Empty);
        }
        else
        {
          if (callback == null)
            return;
          callback(true, status.content);
        }
      }));
    }
  }

  public bool IsMute()
  {
    return PlayerData.inst.userData.IsShutUp;
  }
}
