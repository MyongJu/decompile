﻿// Decompiled with JetBrains decompiler
// Type: BoostsItemsDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BoostsItemsDlg : UI.Dialog
{
  public BetterList<BoostsListItem> items = new BetterList<BoostsListItem>();
  private GameObjectPool pools = new GameObjectPool();
  private BuffUIData _data;
  public BoostsListItem itemPrefab;
  public UILabel title;
  public UIGrid grid;
  public UIScrollView scrollView;
  private BoostCategory Type;
  private BoostType SubType;
  [NonSerialized]
  public GameObject itemRoot;
  private BoostsItemsDlg.BoostListDialogParameter _param1;

  private void Awake()
  {
    this.itemRoot = this.gameObject;
    this.pools.Initialize(this.itemPrefab.gameObject, this.itemRoot);
  }

  public void OnBackBtPress()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) new BoostMenuDlg.BoostMenuDlgParameter()
    {
      DisplayCategory = BoostCategory.none,
      IsResetPosition = false
    }, (UI.Dialog.DialogParameter) null);
  }

  public void OnCloseBackBtPress()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._param1 = orgParam as BoostsItemsDlg.BoostListDialogParameter;
    this._data = this._param1.data;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Clear();
  }

  private void CreateUIData()
  {
    this._data = (BuffUIData) new BuffFromItemData();
    this._data.Category = this._param1.Type;
    this._data.Type = this._param1.SubType;
    using (Dictionary<int, BoostStaticInfo>.ValueCollection.Enumerator enumerator = ConfigManager.inst.DB_Boosts.Item2Boosts.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BoostStaticInfo current = enumerator.Current;
        if (current.Category == this._data.Category && this._data.Type == current.Type)
        {
          this._data.Data = (object) current;
          this._data.Refresh();
          break;
        }
      }
    }
  }

  private void UpdateUI()
  {
    this.scrollView.gameObject.SetActive(false);
    if (this._data == null)
      this.CreateUIData();
    if (this._data != null && this._data is BuffFromItemData)
    {
      List<ItemStaticInfo> items = (this._data as BuffFromItemData).FindItems();
      for (int index = 0; index < items.Count; ++index)
      {
        ItemStaticInfo info = items[index];
        ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(ConfigManager.inst.DB_Boosts.Item2Boosts[info.internalId].ShopItemID);
        this.CreateBoostListItem(info, dataByInternalId);
      }
    }
    this.title.text = ScriptLocalization.Get(this._data.Type.ToString() + "_description", true);
    this.ResetTabel();
  }

  private void ResetTabel()
  {
    this.scrollView.gameObject.SetActive(true);
    this.grid.Reposition();
    this.scrollView.ResetPosition();
  }

  private void OnRepositionHandler()
  {
    this.grid.onReposition -= new UIGrid.OnReposition(this.OnRepositionHandler);
    this.scrollView.ResetPosition();
  }

  private void CreateBoostListItem(ItemStaticInfo info, ShopStaticInfo shopInfo = null)
  {
    this.itemPrefab.gameObject.SetActive(true);
    BoostsListItem boostsListItem = this.pools.AddChild(this.grid.gameObject).AddMissingComponent<BoostsListItem>();
    boostsListItem.SetShopItem(shopInfo);
    boostsListItem.SetItem(info);
    boostsListItem.owner = this;
    this.itemPrefab.gameObject.SetActive(false);
    this.items.Add(boostsListItem);
  }

  private void Clear()
  {
    foreach (BoostsListItem boostsListItem in this.items)
    {
      boostsListItem.gameObject.SetActive(false);
      boostsListItem.Clear();
      this.pools.Release(boostsListItem.gameObject);
    }
    this.items.Clear();
  }

  public class BoostListDialogParameter : UI.Dialog.DialogParameter
  {
    public BoostCategory Type;
    public BoostType SubType;
    public BuffUIData data;
  }
}
