﻿// Decompiled with JetBrains decompiler
// Type: ConstructionRewardBtn
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class ConstructionRewardBtn : MonoBehaviour
{
  public UITexture mIcon;
  public UILabel mRequirementLabel;
  public UILabel mName;
  public GameObject mDescriptionPopup;
  public UILabel mDescriptionLabel;
  private string iconName;

  public void SetDetails(string iconName, string reqDetails, string reqName)
  {
    if (iconName != null && this.iconName != iconName)
    {
      this.iconName = iconName;
      BuilderFactory.Instance.HandyBuild((UIWidget) this.mIcon, iconName, (System.Action<bool>) null, true, false, string.Empty);
    }
    this.mRequirementLabel.text = reqDetails;
    if (reqName == null)
      return;
    reqName = reqName.Replace("_name", string.Empty);
    this.mName.text = ScriptLocalization.Get(reqName, false);
  }

  public void OnBtnClicked()
  {
    foreach (ConstructionRewardBtn componentsInChild in this.gameObject.transform.parent.transform.GetComponentsInChildren<ConstructionRewardBtn>())
      componentsInChild.OnPopupClicked();
    this.mDescriptionPopup.SetActive(true);
  }

  public void OnPopupClicked()
  {
    this.mDescriptionPopup.SetActive(false);
  }
}
