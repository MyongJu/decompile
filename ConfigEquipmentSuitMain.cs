﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentSuitMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigEquipmentSuitMain
{
  private Dictionary<int, List<ConfigEquipmentSuitMainInfo>> equipmentSuitEffectMap = new Dictionary<int, List<ConfigEquipmentSuitMainInfo>>();
  private Dictionary<string, ConfigEquipmentSuitMainInfo> datas;
  private Dictionary<int, ConfigEquipmentSuitMainInfo> dicByUniqueId;

  private int SortByRequireNumber(ConfigEquipmentSuitMainInfo a, ConfigEquipmentSuitMainInfo b)
  {
    return a.numberRequired.CompareTo(b.numberRequired);
  }

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ConfigEquipmentSuitMainInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    using (Dictionary<int, ConfigEquipmentSuitMainInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, ConfigEquipmentSuitMainInfo> current = enumerator.Current;
        if (!this.equipmentSuitEffectMap.ContainsKey(current.Value.suitGroupId))
          this.equipmentSuitEffectMap.Add(current.Value.suitGroupId, new List<ConfigEquipmentSuitMainInfo>());
        this.equipmentSuitEffectMap[current.Value.suitGroupId].Add(current.Value);
      }
    }
    using (Dictionary<int, List<ConfigEquipmentSuitMainInfo>>.ValueCollection.Enumerator enumerator = this.equipmentSuitEffectMap.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Sort(new Comparison<ConfigEquipmentSuitMainInfo>(this.SortByRequireNumber));
    }
  }

  public ConfigEquipmentSuitMainInfo GetData(int internalID)
  {
    return this.dicByUniqueId[internalID];
  }

  public List<ConfigEquipmentSuitMainInfo> GetEquipmentSuitEffectMap(int equipmentSuitGroupId)
  {
    if (this.equipmentSuitEffectMap.ContainsKey(equipmentSuitGroupId))
      return this.equipmentSuitEffectMap[equipmentSuitGroupId];
    return (List<ConfigEquipmentSuitMainInfo>) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, ConfigEquipmentSuitMainInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, ConfigEquipmentSuitMainInfo>) null;
  }
}
