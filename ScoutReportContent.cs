﻿// Decompiled with JetBrains decompiler
// Type: ScoutReportContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class ScoutReportContent
{
  public string k;
  public string x;
  public string y;
  public string user_name;
  public string acronym;
  public string portrait;
  public string icon;
  public int lord_title;
  public string power;
  public Dictionary<string, string> resource;
  public Dictionary<string, DefendTroopDetail> troops;
  public Dictionary<string, string> battle_boost;

  public void SummarizeResourceReward(ref List<ItemInfo.Data> res)
  {
    if (this.resource == null)
      return;
    using (Dictionary<string, string>.Enumerator enumerator = this.resource.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, string> current = enumerator.Current;
        res.Add(new ItemInfo.Data()
        {
          name = current.Value,
          icon = current.Key + "_icon"
        });
      }
    }
  }

  public void SummarizeDefendBoost(ref DefendInfo.Data did)
  {
    if (this.battle_boost.Count == 0)
      return;
    did.tier = new Dictionary<string, List<ScoutBar.Data>>();
    List<ScoutBar.Data> dataList = new List<ScoutBar.Data>();
    did.tier.Add("boost", dataList);
    using (Dictionary<string, string>.Enumerator enumerator = this.battle_boost.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, string> current = enumerator.Current;
        ScoutBar.Data data = new ScoutBar.Data();
        string longId = DBManager.inst.DB_Local_Benefit.GetLongID(current.Key);
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[longId];
        if (dbProperty != null)
        {
          data.icon = "Texture/Benefits/" + dbProperty.Image;
          data.name = ConfigManager.inst.DB_Properties[longId].Name;
          data.count = "+" + (object) (float) ((double) float.Parse(current.Value) * 100.0) + "%";
          did.tier["boost"].Add(data);
        }
        else
          Debug.LogWarning((object) ("wrong boost~~~ " + longId));
      }
    }
  }
}
