﻿// Decompiled with JetBrains decompiler
// Type: HeroPositionAssignSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class HeroPositionAssignSlot : MonoBehaviour
{
  [SerializeField]
  private ParliamentHeroItem _parliamentHeroItem;
  [SerializeField]
  private UILabel _heroNameText;
  [SerializeField]
  private UILabel _heroLevelText;
  [SerializeField]
  private UILabel _heroTitleText;
  [SerializeField]
  private GameObject _highlightNode;
  [SerializeField]
  private UISprite _heroFrameSprite;
  [SerializeField]
  private ParliamentHeroStar _parliamentHeroStar;
  private LegendCardData _legendCardData;
  public System.Action<LegendCardData> OnHeroFramePressed;

  public int LegendId
  {
    get
    {
      if (this._legendCardData != null)
        return this._legendCardData.LegendId;
      return 0;
    }
  }

  public void SetData(LegendCardData legendCardData)
  {
    this._legendCardData = legendCardData;
    this.UpdateUI();
  }

  public void OnHeroFrameClicked()
  {
    if (this._legendCardData == null || this.OnHeroFramePressed == null)
      return;
    this.OnHeroFramePressed(this._legendCardData);
  }

  public void RefreshUI(LegendCardData legendCardData)
  {
    NGUITools.SetActive(this._highlightNode, this._legendCardData != null && this._legendCardData.LegendId == legendCardData.LegendId);
  }

  private void UpdateUI()
  {
    if (this._legendCardData == null)
      return;
    this._parliamentHeroItem.SetData(this._legendCardData, true);
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this._legendCardData.LegendId);
    if (parliamentHeroInfo == null)
      return;
    this._parliamentHeroItem.SetGrey(parliamentHeroInfo.quality, false);
    this._heroNameText.text = parliamentHeroInfo.Name;
    this._heroNameText.color = parliamentHeroInfo.QualityColor;
    this._heroLevelText.text = ScriptLocalization.GetWithPara("id_lv_num", new Dictionary<string, string>()
    {
      {
        "0",
        this._legendCardData.Level.ToString()
      }
    }, true);
    this._heroTitleText.text = HeroCardUtils.GetHeroTitle(this._legendCardData.LegendId);
    this._parliamentHeroStar.SetData(parliamentHeroInfo, this._legendCardData, false);
    this._heroFrameSprite.color = parliamentHeroInfo.QualityColor;
  }
}
