﻿// Decompiled with JetBrains decompiler
// Type: AllianceDiplomacySearchItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;
using UnityEngine;

public class AllianceDiplomacySearchItem : MonoBehaviour
{
  private int m_AllianceID = -1;
  private int m_LastDiplomacyState = -1;
  public UISprite mAllianceIcon;
  public UILabel mAllianceName;
  public UILabel mAllianceMemeberCount;
  public UILabel mAllianceGiftLevel;
  public PageTabsMonitor mTabs;
  public AllianceDiplomacyEnemyPopup mMakeEnemyPopup;
  public AllianceDiplomacyFriendPopup mMakeFriendPopup;
  public AllianceDiplomacyNeutralPopup mMakeNeutralPopup;
  public UILabel mFriendBtnLabel;
  public bool mPendingFriend;
  public GameObject mFriendBtn;
  public GameObject mNeutralBtn;
  public GameObject mEnemyBtn;
  private System.Action<int, AllianceDiplomacySearchItem.DiplomacyAction> onStateChanged;
  private string m_Tag;
  private string m_Name;
  private long m_Power;
  private int m_MemberCount;
  private int m_MemberLimit;
  private int m_GiftPoints;
  private int m_Symbol;

  public long allianceId
  {
    get
    {
      return (long) this.m_AllianceID;
    }
  }

  public void SetDetails(int ID, int allianceSymbol, string allianceName, string allianceTag, int memberCount, int maxMemberCount, int giftPoints, long power, int level, float levelPercent, int diplomacy, System.Action<int, AllianceDiplomacySearchItem.DiplomacyAction> onStateChanged)
  {
    this.mTabs.onTabSelected -= new System.Action<int>(this.OnTabSelected);
    this.m_AllianceID = ID;
    this.m_Tag = allianceTag;
    this.m_Name = allianceName;
    this.m_Power = power;
    this.m_MemberCount = memberCount;
    this.m_MemberLimit = maxMemberCount;
    this.m_GiftPoints = giftPoints;
    this.m_Symbol = allianceSymbol;
    this.onStateChanged = onStateChanged;
    this.mAllianceIcon.spriteName = "shield_" + allianceSymbol.ToString();
    this.mAllianceName.text = string.Format("({0}) {1}", (object) allianceTag, (object) allianceName);
    this.mAllianceMemeberCount.text = Utils.XLAT("MEMBERS") + ": " + memberCount.ToString() + " / " + maxMemberCount.ToString();
    this.mAllianceGiftLevel.text = Utils.XLAT("GIFT LEVEL") + ": " + (object) ConfigManager.inst.DB_Gift_Level.GetLevelByPoints(giftPoints);
    if ((diplomacy & 1) > 0)
    {
      this.mTabs.SetCurrentTab(0, true);
      this.m_LastDiplomacyState = 0;
      this.mFriendBtnLabel.text = "FRIEND";
      this.mFriendBtn.GetComponent<UIButton>().isEnabled = true;
    }
    else if ((diplomacy & 2) > 0)
    {
      this.mTabs.SetCurrentTab(2, true);
      this.m_LastDiplomacyState = 2;
    }
    else
    {
      this.mTabs.SetCurrentTab(1, true);
      this.m_LastDiplomacyState = 1;
    }
    if ((diplomacy & 4) > 0)
    {
      this.mPendingFriend = true;
      this.mFriendBtnLabel.text = "WAITING";
      this.mFriendBtn.GetComponent<UIButton>().isEnabled = false;
    }
    else
    {
      this.mPendingFriend = false;
      this.mFriendBtnLabel.text = "FRIEND";
      this.mFriendBtn.GetComponent<UIButton>().isEnabled = true;
    }
    this.mTabs.onTabSelected += new System.Action<int>(this.OnTabSelected);
    if (DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId).members.Get(PlayerData.inst.uid).title == 6)
      return;
    this.mTabs.HideAllButtons();
  }

  private void OnTabSelected(int index)
  {
    switch (index)
    {
      case 0:
        if (this.mPendingFriend)
          break;
        this.mMakeFriendPopup.SetDetails(this.mAllianceIcon.spriteName, this.mAllianceName.text, new System.Action(this.OnFriendBtnPressed), new System.Action(this.CancelFriendPopup));
        this.mMakeFriendPopup.gameObject.SetActive(true);
        break;
      case 1:
        this.mMakeNeutralPopup.SetDetails(this.mAllianceIcon.spriteName, this.mAllianceName.text, new System.Action(this.OnNeutralBtnPressed), new System.Action(this.CancelNeutralPopup));
        this.mMakeNeutralPopup.gameObject.SetActive(true);
        break;
      case 2:
        this.mMakeEnemyPopup.SetDetails(this.mAllianceIcon.spriteName, this.mAllianceName.text, new System.Action(this.OnEnemyBtnPressed), new System.Action(this.CancelEnemyPopup));
        this.mMakeEnemyPopup.gameObject.SetActive(true);
        break;
    }
  }

  public void OnFriendBtnPressed()
  {
    this.mMakeFriendPopup.gameObject.SetActive(false);
    if (this.onStateChanged == null)
      return;
    this.onStateChanged(this.m_AllianceID, AllianceDiplomacySearchItem.DiplomacyAction.FRIEND);
  }

  public void OnNeutralBtnPressed()
  {
    this.mMakeNeutralPopup.gameObject.SetActive(false);
    if (this.onStateChanged == null)
      return;
    this.mPendingFriend = false;
    this.mFriendBtnLabel.text = "FRIEND";
    this.onStateChanged(this.m_AllianceID, AllianceDiplomacySearchItem.DiplomacyAction.NEUTRAL);
  }

  public void OnEnemyBtnPressed()
  {
    this.mMakeEnemyPopup.gameObject.SetActive(false);
    if (this.onStateChanged == null)
      return;
    this.mPendingFriend = false;
    this.mFriendBtnLabel.text = "FRIEND";
    this.onStateChanged(this.m_AllianceID, AllianceDiplomacySearchItem.DiplomacyAction.ENEMY);
  }

  public void CancelEnemyPopup()
  {
    this.mMakeEnemyPopup.gameObject.SetActive(false);
    this.mTabs.SetCurrentTab(this.m_LastDiplomacyState, false);
  }

  public void CancelFriendPopup()
  {
    this.mMakeFriendPopup.gameObject.SetActive(false);
    this.mTabs.SetCurrentTab(this.m_LastDiplomacyState, false);
  }

  public void CancelNeutralPopup()
  {
    this.mMakeNeutralPopup.gameObject.SetActive(false);
    this.mTabs.SetCurrentTab(this.m_LastDiplomacyState, false);
  }

  public void OnInfo()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) this.m_AllianceID;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("Alliance/AllianceJoinAndApplyDialog", (UI.Dialog.DialogParameter) new AllianceJoinAndApplyDialog.Parameter()
      {
        allianceData = DBManager.inst.DB_Alliance.Get((long) this.m_AllianceID),
        justView = (PlayerData.inst.allianceId > 0L)
      }, true, true, true);
    }), true);
  }

  public enum DiplomacyAction
  {
    FRIEND,
    NEUTRAL,
    ENEMY,
  }
}
