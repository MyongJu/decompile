﻿// Decompiled with JetBrains decompiler
// Type: ConfigAddress
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAddress
{
  private Dictionary<string, string> addresss = new Dictionary<string, string>();

  public void BuildDB(object res)
  {
    foreach (DictionaryEntry re in res as Hashtable)
      this.addresss.Add(re.Key.ToString(), re.Value.ToString());
  }

  public string GetConfigName(int interalID)
  {
    string key = (interalID / 100000).ToString() + "00000";
    string empty = string.Empty;
    this.addresss.TryGetValue(key, out empty);
    return empty;
  }

  public bool IsBuilding(int internalID)
  {
    return this.GetConfigName(internalID).Equals("buildings");
  }

  public bool IsItem(int internalID)
  {
    return this.GetConfigName(internalID).Equals("itemlist");
  }

  public bool IsResearch(int internalID)
  {
    return this.GetConfigName(internalID).Equals("researches");
  }

  public bool IsBenefit(int internalID)
  {
    return this.GetConfigName(internalID).Equals("benefits");
  }

  public void Clear()
  {
    if (this.addresss == null)
      return;
    this.addresss.Clear();
  }
}
