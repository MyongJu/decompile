﻿// Decompiled with JetBrains decompiler
// Type: AllianceHospitalDetailRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class AllianceHospitalDetailRenderer : MonoBehaviour
{
  private List<AllianceHospitalWoundedRenderer> m_WoundedList = new List<AllianceHospitalWoundedRenderer>();
  public GameObject m_UnitPrefab;
  public UITexture m_PlayerIcon;
  public UILabel m_Ranking;
  public UILabel m_PlayerName;
  public UILabel m_TroopCount;
  public UITable m_Table;
  public UIGrid m_Grid;
  public GameObject m_Collapsed;
  public GameObject m_Expanded;
  private Dictionary<string, AllianceHospitalHealingData> m_InjuredTroops;
  private UserData m_UserData;
  private int m_Rank;
  private bool m_IsExpand;
  private System.Action m_OnChanged;

  public void SetData(UserData userData, Dictionary<string, AllianceHospitalHealingData> injuredTroops, int ranking, System.Action onChanged)
  {
    this.m_UserData = userData;
    this.m_InjuredTroops = injuredTroops;
    this.m_Rank = ranking;
    this.m_OnChanged = onChanged;
    this.UpdateCommon();
    this.UpdateExpand();
    this.UpdateTroops();
  }

  public void OnClicked()
  {
    this.m_IsExpand = !this.m_IsExpand;
    this.UpdateExpand();
    this.UpdateTroops();
  }

  private void UpdateExpand()
  {
    this.m_Collapsed.SetActive(!this.m_IsExpand);
    this.m_Expanded.SetActive(this.m_IsExpand);
  }

  private void UpdateCommon()
  {
    CustomIconLoader.Instance.requestCustomIcon(this.m_PlayerIcon, this.m_UserData.PortraitIconPath, this.m_UserData.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.m_PlayerIcon, this.m_UserData.LordTitle, 1);
    this.m_Ranking.text = this.m_Rank.ToString();
    this.m_PlayerName.text = this.m_UserData.userName;
    this.m_TroopCount.text = this.GetInjuredTroopCount().ToString();
  }

  private long GetInjuredTroopCount()
  {
    long num = 0;
    Dictionary<string, AllianceHospitalHealingData>.Enumerator enumerator = this.m_InjuredTroops.GetEnumerator();
    while (enumerator.MoveNext())
      num += enumerator.Current.Value.total;
    return num;
  }

  private void UpdateTroops()
  {
    this.InitOnce();
    this.m_Grid.sorting = UIGrid.Sorting.Custom;
    this.m_Grid.onCustomSort = new Comparison<Transform>(AllianceHospitalDetailRenderer.Compare);
    this.m_Grid.onReposition = new UIGrid.OnReposition(this.OnGridPosition);
    this.m_Grid.Reposition();
  }

  private static int Compare(Transform x, Transform y)
  {
    AllianceHospitalWoundedRenderer component1 = x.GetComponent<AllianceHospitalWoundedRenderer>();
    AllianceHospitalWoundedRenderer component2 = y.GetComponent<AllianceHospitalWoundedRenderer>();
    Unit_StatisticsInfo unit1 = component1.Unit;
    Unit_StatisticsInfo unit2 = component2.Unit;
    if (unit1.Troop_Tier == unit2.Troop_Tier)
      return unit1.Priority.CompareTo(unit2.Priority);
    return unit1.Troop_Tier < unit2.Troop_Tier ? 1 : -1;
  }

  private void OnGridPosition()
  {
    this.m_Table.onReposition = new UITable.OnReposition(this.OnTablePosition);
    this.m_Table.Reposition();
  }

  private void OnTablePosition()
  {
    if (this.m_OnChanged == null)
      return;
    this.m_OnChanged();
  }

  private void InitOnce()
  {
    if (this.m_WoundedList.Count > 0)
      return;
    Dictionary<string, AllianceHospitalHealingData>.Enumerator enumerator = this.m_InjuredTroops.GetEnumerator();
    while (enumerator.MoveNext())
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_UnitPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_Grid.transform;
      gameObject.transform.localScale = Vector3.one;
      AllianceHospitalWoundedRenderer component = gameObject.GetComponent<AllianceHospitalWoundedRenderer>();
      component.SetData(enumerator.Current.Key, enumerator.Current.Value);
      this.m_WoundedList.Add(component);
    }
  }
}
