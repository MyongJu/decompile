﻿// Decompiled with JetBrains decompiler
// Type: BookmarkUpdatePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;

public class BookmarkUpdatePopup : Popup
{
  private int m_SelectedIndex = -1;
  public UILabel m_K;
  public UILabel m_X;
  public UILabel m_Y;
  public UIToggle[] m_Toggles;
  public UIInput m_Input;
  private Bookmark m_Bookmark;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Bookmark = (orgParam as BookmarkUpdatePopup.Parameter).bookmark;
    this.m_SelectedIndex = -1;
    this.UpdateUI();
  }

  public void UpdateUI()
  {
    this.m_K.text = this.m_Bookmark.coordinate.K.ToString();
    this.m_X.text = this.m_Bookmark.coordinate.X.ToString();
    this.m_Y.text = this.m_Bookmark.coordinate.Y.ToString();
    this.m_Input.value = this.m_Bookmark.markName;
    this.m_Toggles[this.GetIndex(this.m_Bookmark.type)].Set(true);
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnToggleChanged()
  {
    this.m_SelectedIndex = this.GetSelectedIndex();
  }

  public void OnUpdatePressed()
  {
    this.m_Bookmark.markName = this.m_Input.value;
    this.m_Bookmark.type = this.GetBookmarkType();
    Hashtable postData = this.m_Bookmark.Encode();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    MessageHub.inst.GetPortByAction("Player:modifyBookmark").SendRequest(postData, (System.Action<bool, object>) ((_param0, _param1) => ToastManager.Instance.AddBasicToast("toast_bookmark_success")), true);
    this.OnClosePressed();
  }

  private int GetSelectedIndex()
  {
    for (int index = 0; index < this.m_Toggles.Length; ++index)
    {
      if (this.m_Toggles[index].value)
        return index;
    }
    return -1;
  }

  private int GetBookmarkType()
  {
    switch (this.m_SelectedIndex)
    {
      case 0:
        return 0;
      case 1:
        return 1;
      case 2:
        return 2;
      default:
        return 0;
    }
  }

  private int GetIndex(int type)
  {
    switch (type)
    {
      case 0:
        return 0;
      case 1:
        return 1;
      case 2:
        return 2;
      default:
        return 0;
    }
  }

  public class Parameter : Popup.PopupParameter
  {
    public Bookmark bookmark;
  }
}
