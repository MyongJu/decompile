﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryCommunityRewardRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AnniversaryCommunityRewardRenderer : MonoBehaviour
{
  public UILabel itemCount;
  public UITexture itemTexture;
  public UITexture border;
  private int itemId;

  public void SetData(AnniversaryCommunityRewardInfo rewardInfo)
  {
    List<Reward.RewardsValuePair> rewards = rewardInfo.Rewards.GetRewards();
    if (rewards == null || rewards.Count != 1)
      return;
    this.itemId = rewards[0].internalID;
    this.itemCount.text = rewards[0].value.ToString();
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.itemId);
    if (itemStaticInfo == null)
      return;
    BuilderFactory.Instance.Build((UIWidget) this.itemTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    Utils.SetItemBackground(this.border, itemStaticInfo.internalId);
  }

  public void Release()
  {
    BuilderFactory.Instance.Release((UIWidget) this.itemTexture);
  }

  public void OnClickHandler()
  {
    Utils.ShowItemTip(this.itemId, this.border.transform, 0L, 0L, 0);
  }

  public void OnReleaseHandler()
  {
    Utils.StopShowItemTip();
  }

  public void OnPressHandler()
  {
    Utils.DelayShowTip(this.itemId, this.border.transform, 0L, 0L, 0);
  }
}
