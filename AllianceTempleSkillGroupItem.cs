﻿// Decompiled with JetBrains decompiler
// Type: AllianceTempleSkillGroupItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class AllianceTempleSkillGroupItem : MonoBehaviour
{
  protected Dictionary<int, AllianceTempleSkillItem> m_allTempleSkillItem = new Dictionary<int, AllianceTempleSkillItem>();
  [SerializeField]
  protected Color m_labelEnabledColor;
  [SerializeField]
  protected Color m_labelDisabledColor;
  [SerializeField]
  private AllianceTempleSkillItem m_skillItemTemplate;
  [SerializeField]
  protected UILabel m_labelLevel;
  [SerializeField]
  protected UIGrid m_attackSkillContainer;
  [SerializeField]
  protected UIGrid m_defendSkillContainer;
  [SerializeField]
  protected UIGrid m_developSkillContainer;
  protected int m_allianceTempleLevel;

  public void SetAllianceTempleLevel(int allianceTempleLevel)
  {
    this.m_allianceTempleLevel = allianceTempleLevel;
    this.UpdateUI();
  }

  public void UpdateUI()
  {
    this.m_labelLevel.color = Utils.GetPlayerAllianceTempleLevel() < this.m_allianceTempleLevel ? this.m_labelDisabledColor : this.m_labelEnabledColor;
    this.m_labelLevel.text = ScriptLocalization.Get("id_lv", true) + (object) this.m_allianceTempleLevel;
    using (List<TempleSkillInfo>.Enumerator enumerator = ConfigManager.inst.DB_TempleSkill.GetByLevelAndGroup(this.m_allianceTempleLevel, "attack").GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TempleSkillInfo current = enumerator.Current;
        this.GetAllianceTempleSkillItem(current).SetTempleSkillInfo(current);
      }
    }
    using (List<TempleSkillInfo>.Enumerator enumerator = ConfigManager.inst.DB_TempleSkill.GetByLevelAndGroup(this.m_allianceTempleLevel, "defense").GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TempleSkillInfo current = enumerator.Current;
        this.GetAllianceTempleSkillItem(current).SetTempleSkillInfo(current);
      }
    }
    using (List<TempleSkillInfo>.Enumerator enumerator = ConfigManager.inst.DB_TempleSkill.GetByLevelAndGroup(this.m_allianceTempleLevel, "development").GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TempleSkillInfo current = enumerator.Current;
        this.GetAllianceTempleSkillItem(current).SetTempleSkillInfo(current);
      }
    }
    this.m_attackSkillContainer.repositionNow = true;
    this.m_attackSkillContainer.Reposition();
    this.m_defendSkillContainer.repositionNow = true;
    this.m_defendSkillContainer.Reposition();
    this.m_developSkillContainer.repositionNow = true;
    this.m_developSkillContainer.Reposition();
  }

  protected Transform GetTempleSkillContainer(TempleSkillInfo templeSkillInfo)
  {
    if (templeSkillInfo.Group == "attack")
      return this.m_attackSkillContainer.transform;
    if (templeSkillInfo.Group == "defense")
      return this.m_defendSkillContainer.transform;
    if (templeSkillInfo.Group == "development")
      return this.m_developSkillContainer.transform;
    return (Transform) null;
  }

  protected AllianceTempleSkillItem GetAllianceTempleSkillItem(TempleSkillInfo templeSkillInfo)
  {
    if (this.m_allTempleSkillItem.ContainsKey(templeSkillInfo.internalId))
      return this.m_allTempleSkillItem[templeSkillInfo.internalId];
    GameObject gameObject = Object.Instantiate<GameObject>(this.m_skillItemTemplate.gameObject);
    gameObject.transform.SetParent(this.GetTempleSkillContainer(templeSkillInfo));
    gameObject.transform.localScale = Vector3.one;
    AllianceTempleSkillItem component = gameObject.GetComponent<AllianceTempleSkillItem>();
    this.m_allTempleSkillItem.Add(templeSkillInfo.internalId, component);
    return component;
  }
}
