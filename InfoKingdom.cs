﻿// Decompiled with JetBrains decompiler
// Type: InfoKingdom
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;

public class InfoKingdom : Popup
{
  private const string iconfolder = "Texture/GUI_Textures/";
  public UIButtonBar bar;
  public Icon info;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.bar.OnSelectedHandler += new System.Action<int>(this.SelectedChanged);
    this.bar.SelectedIndex = (orgParam as InfoKingdom.Parameter).initIndex;
  }

  private void SelectedChanged(int index)
  {
    string path = (string) null;
    string Term = (string) null;
    switch (index)
    {
      case 0:
        path = "Texture/GUI_Textures/baner_1";
        Term = "kingdom_info_gather_description";
        break;
      case 1:
        path = "Texture/GUI_Textures/baner_2";
        Term = "kingdom_info_monster_description";
        break;
      case 2:
        path = "Texture/GUI_Textures/tutorial_enemy_attack";
        Term = "kingdom_info_pvp_description";
        break;
      case 3:
        path = "Texture/GUI_Textures/tutorial_alliance";
        Term = "kingdom_info_alliance_description";
        break;
      case 4:
        path = "Texture/GUI_Textures/tutorial_rally";
        Term = "kingdom_info_rally_description";
        break;
      case 5:
        path = "Texture/GUI_Textures/tutorial_rally_monster";
        Term = "kingdom_info_gve_description";
        break;
    }
    this.info.SetData(path, new string[1]
    {
      ScriptLocalization.Get(Term, true)
    });
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.bar.OnSelectedHandler -= new System.Action<int>(this.SelectedChanged);
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int initIndex;
  }
}
