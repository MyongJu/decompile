﻿// Decompiled with JetBrains decompiler
// Type: LegendSkillNormalItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;
using UnityEngine;

public class LegendSkillNormalItem : MonoBehaviour
{
  public UITexture skillIcon;
  public UILabel skillName;
  public UILabel skillValue;
  public UILabel skillLevel;
  private LegendSkillInfo _skill;

  public void SetData(LegendSkillInfo skill)
  {
    this._skill = skill;
    this.UpdateUI();
  }

  public void Clear()
  {
    if (!((UnityEngine.Object) this.skillIcon != (UnityEngine.Object) null))
      return;
    BuilderFactory.Instance.Release((UIWidget) this.skillIcon);
  }

  public void OnSkillIconPress()
  {
    UIManager.inst.OpenPopup("CommonDescPopup", (Popup.PopupParameter) new CommonDetailDlg.Parameter()
    {
      title = ScriptLocalization.Get("hero_altar_uppercase_skill_info", true),
      detail = this._skill.Loc_Desc
    });
  }

  private void UpdateUI()
  {
    this.skillValue.text = Utils.GetSkillValueDesc(this._skill);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.skillIcon, Utils.GetUITextPath() + this._skill.Image, (System.Action<bool>) null, true, false, string.Empty);
    this.skillName.text = this._skill.Loc_Name;
    this.skillLevel.text = ScriptLocalization.Get("id_lv", true) + this._skill.Level.ToString();
  }
}
