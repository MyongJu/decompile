﻿// Decompiled with JetBrains decompiler
// Type: NetServerTime
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class NetServerTime
{
  private float _lastUpdateTime = -1f;
  private double _smoothServerTime = -1.0;
  private const int SYNC_TIME_INTERVAL = 60;
  private double m_UpdateTime;
  private bool m_update;
  private static NetServerTime _instance;
  private double _serverTime;

  public static NetServerTime inst
  {
    get
    {
      if (NetServerTime._instance == null)
        NetServerTime._instance = new NetServerTime();
      return NetServerTime._instance;
    }
  }

  public void StartSyncTime()
  {
    this.ResetSmoothTime();
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  public void StopSyncTime()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
    this.ResetSmoothTime();
  }

  private void Process(int timestamp)
  {
    if (!this.m_update || timestamp - (int) this._serverTime <= 60)
      return;
    this.m_update = false;
  }

  public int ServerTimestamp
  {
    get
    {
      return (int) (this._serverTime + (double) Time.realtimeSinceStartup - this.m_UpdateTime);
    }
  }

  public double UpdateTime
  {
    get
    {
      return (double) Time.realtimeSinceStartup - this.m_UpdateTime + this._serverTime;
    }
  }

  public int LocalTimestamp
  {
    get
    {
      return (int) ((DateTime.UtcNow.Ticks - DateTime.Parse("01/01/1970 00:00:00").Ticks) / 10000000L);
    }
  }

  public double LocalUpdateTime
  {
    get
    {
      return (double) (DateTime.UtcNow.Ticks - DateTime.Parse("01/01/1970 00:00:00").Ticks) / 10000000.0;
    }
  }

  public void SetServerTime(double serverTime)
  {
    if (serverTime <= this._serverTime)
      return;
    this._serverTime = serverTime;
    this.m_UpdateTime = (double) Time.realtimeSinceStartup;
    this.m_update = true;
  }

  public double SmoothServerTime
  {
    get
    {
      if ((double) this._lastUpdateTime != (double) Time.time)
      {
        this.UpdateSmoothServerTime();
        this._lastUpdateTime = Time.time;
      }
      return this._smoothServerTime;
    }
  }

  private void UpdateSmoothServerTime()
  {
    double updateTime = this.UpdateTime;
    if (this._smoothServerTime == -1.0)
      this._smoothServerTime = updateTime;
    this._smoothServerTime = this.LerpTime(this._smoothServerTime, updateTime, 0.949999988079071);
  }

  private void ResetSmoothTime()
  {
    this._smoothServerTime = -1.0;
    this._lastUpdateTime = -1f;
  }

  public double LerpTime(double originTime, double targetTime, double lerpFactor = 0.95f)
  {
    return originTime < targetTime ? originTime + (targetTime - originTime) * lerpFactor : originTime;
  }

  public int TodayLeftTimeUTC()
  {
    return 86400 - this.ServerTimestamp % 86400;
  }

  public bool IsToday(double time)
  {
    return this.ServerTimestamp / 86400 == (int) time / 86400;
  }
}
