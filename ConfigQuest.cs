﻿// Decompiled with JetBrains decompiler
// Type: ConfigQuest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigQuest
{
  public Dictionary<int, QuestInfo> datas;
  private Dictionary<string, int> dicByUniqueId;

  public ConfigQuest()
  {
    this.datas = new Dictionary<int, QuestInfo>();
    this.dicByUniqueId = new Dictionary<string, int>();
  }

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exsit", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "type");
        int index3 = ConfigManager.GetIndex(inHeader, "priority");
        int index4 = ConfigManager.GetIndex(inHeader, "image");
        int index5 = ConfigManager.GetIndex(inHeader, "category");
        int index6 = ConfigManager.GetIndex(inHeader, "quality");
        int index7 = ConfigManager.GetIndex(inHeader, "food_payout");
        int index8 = ConfigManager.GetIndex(inHeader, "wood_payout");
        int index9 = ConfigManager.GetIndex(inHeader, "ore_payout");
        int index10 = ConfigManager.GetIndex(inHeader, "silver_payout");
        int index11 = ConfigManager.GetIndex(inHeader, "gold");
        int index12 = ConfigManager.GetIndex(inHeader, "reward_id_1");
        int index13 = ConfigManager.GetIndex(inHeader, "reward_id_2");
        int index14 = ConfigManager.GetIndex(inHeader, "reward_value_1");
        int index15 = ConfigManager.GetIndex(inHeader, "reward_value_2");
        int index16 = ConfigManager.GetIndex(inHeader, "player_xp");
        int index17 = ConfigManager.GetIndex(inHeader, "visible");
        int index18 = ConfigManager.GetIndex(inHeader, "loc_name_id");
        int index19 = ConfigManager.GetIndex(inHeader, "power");
        int index20 = ConfigManager.GetIndex(inHeader, "normal_chance");
        int index21 = ConfigManager.GetIndex(inHeader, "refresh_chance");
        int index22 = ConfigManager.GetIndex(inHeader, "daily_streak");
        int index23 = ConfigManager.GetIndex(inHeader, "goto_id");
        int index24 = ConfigManager.GetIndex(inHeader, "formula");
        int index25 = ConfigManager.GetIndex(inHeader, "value");
        int index26 = ConfigManager.GetIndex(inHeader, "loc_name_id");
        int index27 = ConfigManager.GetIndex(inHeader, "loc_description_id");
        int index28 = ConfigManager.GetIndex(inHeader, "quests_group_id");
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          QuestInfo data = new QuestInfo();
          if (int.TryParse(key.ToString(), out data.internalId))
          {
            ArrayList arr = hashtable[key] as ArrayList;
            if (arr != null)
            {
              data.ID = ConfigManager.GetString(arr, index1);
              data.Type = ConfigManager.GetInt(arr, index2);
              data.Priority = ConfigManager.GetInt(arr, index3);
              data.Image = ConfigManager.GetString(arr, index4);
              data.Category = ConfigManager.GetString(arr, index5);
              data.Quality = ConfigManager.GetInt(arr, index6);
              data.DailyStreak = ConfigManager.GetInt(arr, index22);
              data.Formula = ConfigManager.GetString(arr, index24);
              data.Value = ConfigManager.GetInt(arr, index25);
              data.Food_Payout = ConfigManager.GetInt(arr, index7);
              data.Wood_Payout = ConfigManager.GetInt(arr, index8);
              data.Ore_Payout = ConfigManager.GetInt(arr, index9);
              data.Silver_Payout = ConfigManager.GetInt(arr, index10);
              data.Reward_ID_1 = ConfigManager.GetInt(arr, index12);
              data.Reward_ID_2 = ConfigManager.GetInt(arr, index13);
              data.Reward_Value_1 = ConfigManager.GetInt(arr, index14);
              data.Reward_Value_2 = ConfigManager.GetInt(arr, index15);
              data.Hero_XP = ConfigManager.GetInt(arr, index16);
              data.Visible = ConfigManager.GetBool(arr, index17);
              data.Name = ConfigManager.GetString(arr, index18);
              data.Gold_PayOut = ConfigManager.GetInt(arr, index11);
              data.power = ConfigManager.GetInt(arr, index19);
              data.normalChance = ConfigManager.GetDouble(arr, index20);
              data.refreshChance = ConfigManager.GetDouble(arr, index21);
              data.LinkHudId = ConfigManager.GetInt(arr, index23);
              data.Loc_Name = ConfigManager.GetString(arr, index26);
              data.Loc_Desc = ConfigManager.GetString(arr, index27);
              data.groupId = ConfigManager.GetInt(arr, index28);
              this.PushData(data.internalId, data.ID, data);
            }
          }
        }
      }
    }
  }

  public Dictionary<int, QuestInfo>.ValueCollection Values
  {
    get
    {
      return this.datas.Values;
    }
  }

  private void PushData(int internalId, string uniqueId, QuestInfo data)
  {
    if (this.datas.ContainsKey(internalId))
      return;
    this.datas.Add(internalId, data);
    if (!this.dicByUniqueId.ContainsKey(uniqueId))
      this.dicByUniqueId.Add(uniqueId, internalId);
    ConfigManager.inst.AddData(internalId, (object) data);
  }

  public void Clear()
  {
    if (this.datas != null)
      this.datas.Clear();
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
  }

  public QuestInfo GetData(int internalId)
  {
    if (this.datas.ContainsKey(internalId))
      return this.datas[internalId];
    if (ConfigManager.inst.DB_AllianceQuest.Contains(internalId))
      return ConfigManager.inst.DB_AllianceQuest.GetQuestInfo(internalId);
    return (QuestInfo) null;
  }

  public QuestInfo GetData(string uniqueId)
  {
    if (!this.dicByUniqueId.ContainsKey(uniqueId))
      return (QuestInfo) null;
    return this.datas[this.dicByUniqueId[uniqueId]];
  }
}
