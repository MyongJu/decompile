﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragonSkillGroupInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class ConfigDragonSkillGroupInfo
{
  public static string DARK = "dark";
  public static string LIGHT = "light";
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "type")]
  public string type;
  [Config(Name = "learn_method")]
  public int learn_method;
  [Config(Name = "pve_usable")]
  public bool pve_usable;
  [Config(Name = "pvp_attack_usable")]
  public bool pvp_attack_usable;
  [Config(Name = "defense_usable")]
  public bool defense_usable;
  [Config(Name = "gather_usable")]
  public bool gather_usable;
  [Config(Name = "localization_name")]
  public string localization_name;
  [Config(Name = "localization_description")]
  public string localization_description;
  [Config(Name = "image")]
  public string image;
  [Config(Name = "sort")]
  public int priority;
  [Config(Name = "no_artifact")]
  public int no_artifact;

  public string IconPath
  {
    get
    {
      return "Texture/Dragon/Skill/Icon/" + this.image;
    }
  }

  public bool CanBeUsedAs(string usability)
  {
    string key = usability;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (ConfigDragonSkillGroupInfo.\u003C\u003Ef__switch\u0024map4D == null)
      {
        // ISSUE: reference to a compiler-generated field
        ConfigDragonSkillGroupInfo.\u003C\u003Ef__switch\u0024map4D = new Dictionary<string, int>(4)
        {
          {
            "attack_monster",
            0
          },
          {
            "attack",
            1
          },
          {
            "gather",
            2
          },
          {
            "defend",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (ConfigDragonSkillGroupInfo.\u003C\u003Ef__switch\u0024map4D.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return this.pve_usable;
          case 1:
            return this.pvp_attack_usable;
          case 2:
            return this.gather_usable;
          case 3:
            return this.defense_usable;
        }
      }
    }
    return false;
  }
}
