﻿// Decompiled with JetBrains decompiler
// Type: IapRebatePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class IapRebatePopup : Popup
{
  private Dictionary<int, IapRebateRewardItemRenderer> itemDict = new Dictionary<int, IapRebateRewardItemRenderer>();
  private GameObjectPool itemPool = new GameObjectPool();
  public UILabel panelTitle;
  public UILabel iapRebateDesc;
  public UILabel timeTip;
  public UILabel timeDur;
  public UILabel buttonTip;
  public UILabel progressDetail;
  public UIProgressBar progressBar;
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.AddEventHandler();
    this.OnSecondEventHandler(0);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.ClearData();
  }

  public override string Type
  {
    get
    {
      return "IAP/LimitedTimeRebatePopup";
    }
  }

  public void OnRechargeBtnPressed()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void ClearData()
  {
    using (Dictionary<int, IapRebateRewardItemRenderer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this.itemPool.Release(gameObject);
      }
    }
    this.itemDict.Clear();
    this.itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private IapRebateRewardItemRenderer CreateItemRenderer(IapRebateRewards.RewardItemInfo rewardItemInfo)
  {
    GameObject gameObject = this.itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    IapRebateRewardItemRenderer component = gameObject.GetComponent<IapRebateRewardItemRenderer>();
    component.SetData(rewardItemInfo);
    return component;
  }

  private void ShowIapRebateRewardContent()
  {
    IapRebateRewards iapRebateReward = IapRebatePayload.Instance.IapRebateReward;
    if (iapRebateReward != null && iapRebateReward.rewardItemInfoList != null)
    {
      for (int key = 0; key < iapRebateReward.rewardItemInfoList.Count; ++key)
      {
        IapRebateRewardItemRenderer itemRenderer = this.CreateItemRenderer(iapRebateReward.rewardItemInfoList[key]);
        this.itemDict.Add(key, itemRenderer);
      }
    }
    this.Reposition();
  }

  private void ShowHintInfo()
  {
    this.buttonTip.text = Utils.XLAT("id_uppercase_topup");
    this.timeTip.text = string.Format("{0}" + Utils.XLAT("event_finishes_num"), (object) string.Empty, (object) string.Empty);
    this.panelTitle.text = ScriptLocalization.Get(IapRebatePayload.Instance.Title, true);
    this.iapRebateDesc.text = ScriptLocalization.Get(IapRebatePayload.Instance.Description, true);
  }

  private void ShowProgressSlider()
  {
    IapRebateRewards iapRebateReward = IapRebatePayload.Instance.IapRebateReward;
    if (iapRebateReward == null)
      return;
    this.progressDetail.text = string.Format("{0}/{1}", (object) IapRebatePayload.Instance.RechargedAmount, (object) iapRebateReward.TargetRechargeAmount);
    if (iapRebateReward.TargetRechargeAmount > 0)
      this.progressBar.value = (float) IapRebatePayload.Instance.RechargedAmount / (float) iapRebateReward.TargetRechargeAmount;
    else
      this.progressBar.value = 1f;
  }

  private void OnSecondEventHandler(int time)
  {
    int time1 = IapRebatePayload.Instance.EndTime - NetServerTime.inst.ServerTimestamp;
    this.timeDur.text = Utils.FormatTime(time1, true, false, true);
    if (time1 > 0)
      return;
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.ClearData();
    this.ShowHintInfo();
    this.ShowProgressSlider();
    this.ShowIapRebateRewardContent();
  }

  private void OnIapRebateDataChanged()
  {
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEventHandler);
    IapRebatePayload.Instance.OnIapRebateDataChanged -= new System.Action(this.OnIapRebateDataChanged);
    IapRebatePayload.Instance.OnIapRebateDataChanged += new System.Action(this.OnIapRebateDataChanged);
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEventHandler);
    IapRebatePayload.Instance.OnIapRebateDataChanged -= new System.Action(this.OnIapRebateDataChanged);
  }
}
