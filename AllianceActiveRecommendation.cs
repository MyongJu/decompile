﻿// Decompiled with JetBrains decompiler
// Type: AllianceActiveRecommendation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class AllianceActiveRecommendation : MonoBehaviour
{
  public const float ONE_MINUTE = 60f;
  public const float FIVETEEN_MINUTES = 300f;
  public AllianceRecommendTip m_Tip;
  private Coroutine m_RecommendCoroutine;

  private void Start()
  {
    AllianceManager.Instance.onJoin += new System.Action(this.OnJoin);
    AllianceManager.Instance.onLeave += new System.Action(this.OnLeave);
    DBManager.inst.DB_City.onDataUpdate += new System.Action<long>(this.OnCityDataChanged);
    this.m_Tip.gameObject.SetActive(false);
    this.StartRecommendJob(60f);
  }

  private void OnDestroy()
  {
    AllianceManager.Instance.onJoin -= new System.Action(this.OnJoin);
    AllianceManager.Instance.onLeave -= new System.Action(this.OnLeave);
    if (GameEngine.IsAvailable && !GameEngine.IsShuttingDown)
      DBManager.inst.DB_City.onDataUpdate -= new System.Action<long>(this.OnCityDataChanged);
    this.StopRecommendJob();
  }

  private void OnJoin()
  {
    this.m_Tip.Hide();
    this.StopRecommendJob();
  }

  private void OnLeave()
  {
    this.StartRecommendJob(60f);
  }

  private void OnCityDataChanged(long cityId)
  {
    if (cityId != PlayerData.inst.playerCityData.cityId || this.m_RecommendCoroutine != null)
      return;
    this.StartRecommendJob(60f);
  }

  private void OnTipHide()
  {
    this.StartRecommendJob(300f);
  }

  private void StartRecommendJob(float delay)
  {
    this.StopRecommendJob();
    if (PlayerData.inst.allianceId != 0L || PlayerData.inst.playerCityData.level < 3)
      return;
    this.m_RecommendCoroutine = this.StartCoroutine(this.RecommendJob(delay));
  }

  private void StopRecommendJob()
  {
    if (this.m_RecommendCoroutine == null)
      return;
    this.StopCoroutine(this.m_RecommendCoroutine);
    this.m_RecommendCoroutine = (Coroutine) null;
  }

  [DebuggerHidden]
  private IEnumerator RecommendJob(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AllianceActiveRecommendation.\u003CRecommendJob\u003Ec__Iterator22()
    {
      delay = delay,
      \u003C\u0024\u003Edelay = delay,
      \u003C\u003Ef__this = this
    };
  }
}
