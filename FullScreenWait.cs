﻿// Decompiled with JetBrains decompiler
// Type: FullScreenWait
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class FullScreenWait : SystemBlocker
{
  public GameObject normalWait;
  public UISprite retryWait;
  private bool _destroy;

  public void ShowRetry()
  {
    NGUITools.SetActive(this.normalWait, false);
    NGUITools.SetActive(this.retryWait.gameObject, true);
  }

  private void OnDisable()
  {
    if (this._destroy)
      return;
    NGUITools.SetActive(this.normalWait, true);
    NGUITools.SetActive(this.retryWait.gameObject, false);
  }

  private void OnDestroy()
  {
    this._destroy = true;
  }
}
