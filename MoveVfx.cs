﻿// Decompiled with JetBrains decompiler
// Type: MoveVfx
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class MoveVfx : VfxBase
{
  private Vector3 orginPoint = Vector3.zero;
  public float duration = 10f;
  private Vector3 vect = Vector3.zero;
  public Transform from;
  public Transform to;
  private bool init;
  private float time;
  [NonSerialized]
  public float delay;
  public System.Action<MoveVfx> OnMoveCompleteHandler;

  public override void Play(Transform target)
  {
    this.gameObject.SetActive(true);
    base.Play(target);
    if (!this.init)
    {
      this.vect = this.gameObject.transform.position;
      this.init = true;
    }
    this.gameObject.transform.position = this.vect;
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "x", (object) this.to.position.x, (object) "y", (object) this.to.position.y, (object) "z", (object) this.vect.z, (object) "time", (object) this.duration, (object) "easetype", (object) "Linear", (object) "isLocal", (object) false, (object) "oncomplete", (object) "OnCompleteHandler", (object) "delay", (object) this.delay));
  }

  public void OnCompleteHandler()
  {
    this.Stop();
    if (this.OnMoveCompleteHandler == null)
      return;
    this.OnMoveCompleteHandler(this);
  }
}
