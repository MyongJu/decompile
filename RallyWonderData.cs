﻿// Decompiled with JetBrains decompiler
// Type: RallyWonderData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;

public class RallyWonderData : RallyBuildingData
{
  private DB.WonderData _wonder;

  public RallyWonderData(int x, int y)
    : base(x, y)
  {
  }

  public RallyWonderData(long id)
    : base(id)
  {
  }

  private DB.WonderData Wonder
  {
    get
    {
      if (this._wonder == null)
        this._wonder = DBManager.inst.DB_Wonder.Get(this.id);
      return this._wonder;
    }
  }

  public override string BuildName
  {
    get
    {
      return ScriptLocalization.Get("throne_wonder_name", true);
    }
  }

  public override Coordinate Location
  {
    get
    {
      return WonderUtils.GetWonderLocation();
    }
  }

  public override List<MarchData> MarchList
  {
    get
    {
      return DBManager.inst.DB_Wonder.GetMarchsByWonderID(this.Wonder.WORLD_ID);
    }
  }

  public override long MaxCount
  {
    get
    {
      return this.Wonder.MaxTroopsCount;
    }
  }

  public override long OwnerID
  {
    get
    {
      return -1;
    }
  }

  public override void Ready(System.Action callBack)
  {
    RequestManager.inst.SendRequest("wonder:getWonderCapacity", new Hashtable()
    {
      {
        (object) "world_id",
        (object) this.Wonder.WORLD_ID
      }
    }, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (arg1)
      {
        Hashtable hashtable = arg2 as Hashtable;
        string empty = string.Empty;
        if (hashtable != null && hashtable.ContainsKey((object) "capacity"))
          empty = hashtable[(object) "capacity"].ToString();
        if (this.Wonder != null && !string.IsNullOrEmpty(empty))
        {
          int result;
          if (int.TryParse(empty, out result))
            this.Wonder.MaxTroopsCount = (long) result;
          if (this.Wonder.MaxTroopsCount < 0L)
            this.Wonder.MaxTroopsCount = 0L;
        }
      }
      if (callBack == null)
        return;
      callBack();
    }), true);
  }

  public override MarchAllocDlg.MarchType ReinforceMarchType
  {
    get
    {
      return MarchAllocDlg.MarchType.wonderReinforce;
    }
  }

  public override RallyBuildingData.DataType Type
  {
    get
    {
      return RallyBuildingData.DataType.Wonder;
    }
  }
}
