﻿// Decompiled with JetBrains decompiler
// Type: UnitGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class UnitGroup : ICustomParse
{
  private List<UnitGroup.UnitRecord> m_UnitRecordList = new List<UnitGroup.UnitRecord>();

  public void Parse(object content)
  {
    this.m_UnitRecordList.Clear();
    Hashtable hashtable = content as Hashtable;
    if (hashtable == null)
      return;
    foreach (DictionaryEntry dictionaryEntry in hashtable)
      this.m_UnitRecordList.Add(new UnitGroup.UnitRecord()
      {
        internalId = int.Parse(dictionaryEntry.Key.ToString()),
        value = int.Parse(dictionaryEntry.Value.ToString())
      });
  }

  public List<UnitGroup.UnitRecord> GetUnitList()
  {
    return this.m_UnitRecordList;
  }

  public class UnitRecord
  {
    public int internalId;
    public int value;
  }
}
