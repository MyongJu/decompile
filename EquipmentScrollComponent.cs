﻿// Decompiled with JetBrains decompiler
// Type: EquipmentScrollComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class EquipmentScrollComponent : ComponentRenderBase
{
  public System.Action<EquipmentScrollComponent> OnSelectedHandler;
  public UITexture icon;
  public UILabel amount;
  public UITexture background;
  public UILabel equipmentScrollName;
  public GameObject selected;
  public GameObject newFlag;
  public ConfigEquipmentScrollInfo scrollInfo;
  private EquipmentScrollComponetData scrollComponentData;
  private UIWidget[] widgets;

  public override void Init()
  {
    this.scrollComponentData = this.data as EquipmentScrollComponetData;
    this.scrollInfo = this.scrollComponentData.equipmentScrollInfo;
    BuilderFactory.Instance.Build((UIWidget) this.icon, this.scrollComponentData.equipmentScrollInfo.ImagePath(this.scrollComponentData.bagType), (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.background, this.scrollComponentData.equipmentScrollInfo.QualityImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    this.widgets = this.GetComponentsInChildren<UIWidget>();
    int itemCount = ItemBag.Instance.GetItemCount(this.scrollComponentData.ItemInfo.ID);
    if ((UnityEngine.Object) this.amount != (UnityEngine.Object) null)
      this.amount.text = itemCount.ToString();
    if (itemCount == 0)
    {
      for (int index = 0; index < this.widgets.Length; ++index)
        this.widgets[index].alpha = 0.4f;
      if ((UnityEngine.Object) this.newFlag != (UnityEngine.Object) null)
        this.newFlag.SetActive(false);
    }
    else
    {
      if ((UnityEngine.Object) this.newFlag != (UnityEngine.Object) null)
      {
        ConsumableItemData data = DBManager.inst.DB_Item.Datas[(long) this.scrollComponentData.ItemInfo.internalId];
        if (data != null)
          this.newFlag.SetActive(data.isNew);
      }
      for (int index = 0; index < this.widgets.Length; ++index)
        this.widgets[index].alpha = !(this.widgets[index].name != "bg") ? 0.8f : 1f;
    }
    if ((UnityEngine.Object) this.equipmentScrollName != (UnityEngine.Object) null)
    {
      this.equipmentScrollName.text = this.scrollComponentData.ItemInfo.LocName;
      EquipmentManager.Instance.ConfigQualityLabelWithColor(this.equipmentScrollName, this.scrollComponentData.equipmentScrollInfo.quality);
    }
    this.Select = false;
  }

  public override void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
    BuilderFactory.Instance.Release((UIWidget) this.background);
  }

  public bool Select
  {
    set
    {
      if (!((UnityEngine.Object) this.selected != (UnityEngine.Object) null))
        return;
      this.selected.SetActive(value);
      int itemCount = ItemBag.Instance.GetItemCount(this.scrollComponentData.ItemInfo.ID);
      if (value)
      {
        if (itemCount > 0)
        {
          ConsumableItemData data = DBManager.inst.DB_Item.Datas[(long) this.scrollComponentData.ItemInfo.internalId];
          if (data != null)
          {
            data.isNew = false;
            if ((UnityEngine.Object) this.newFlag != (UnityEngine.Object) null)
              this.newFlag.SetActive(data.isNew);
          }
        }
        else if ((UnityEngine.Object) this.newFlag != (UnityEngine.Object) null)
          this.newFlag.SetActive(false);
        this.selected.GetComponent<UIWidget>().alpha = 1f;
      }
      else if (itemCount == 0)
      {
        for (int index = 0; index < this.widgets.Length; ++index)
          this.widgets[index].alpha = 0.4f;
      }
      else
      {
        for (int index = 0; index < this.widgets.Length; ++index)
          this.widgets[index].alpha = !(this.widgets[index].name != "bg") ? 0.8f : 1f;
      }
    }
  }

  public void OnClick()
  {
    Utils.StopShowItemTip();
    if (this.OnSelectedHandler == null)
      return;
    this.OnSelectedHandler(this);
  }

  public void OnPressItem()
  {
    Utils.DelayShowTip(this.scrollComponentData.ItemInfo.internalId, this.transform, 0L, 0L, 0);
  }

  public void OnReleaseItem()
  {
    Utils.StopShowItemTip();
  }
}
