﻿// Decompiled with JetBrains decompiler
// Type: IgnorableMessageBoxPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class IgnorableMessageBoxPopup : Popup
{
  [SerializeField]
  private UILabel m_TitleLabel;
  [SerializeField]
  private UILabel m_ContentLabel;
  [SerializeField]
  private UILabel m_NormalLeftBtnLabel;
  [SerializeField]
  private UILabel m_NormalRightBtnLabel;
  [SerializeField]
  private UILabel m_InverseLeftBtnLabel;
  [SerializeField]
  private UILabel m_InverseRightBtnLabel;
  [SerializeField]
  private UIToggle _toggle;
  [SerializeField]
  private GameObject m_RootNormalMode;
  [SerializeField]
  private GameObject m_RootInverseMode;
  private IgnorableMessageBoxPopup.Parameter parameter;

  public static string GetPlayerPrefKey(string key)
  {
    return "Ignore_MessageBox_" + key;
  }

  public static bool IsIgnore(string key)
  {
    return PlayerPrefsEx.GetIntByUid(IgnorableMessageBoxPopup.GetPlayerPrefKey(key), 0) > 0;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.parameter = orgParam as IgnorableMessageBoxPopup.Parameter;
    this.m_RootNormalMode.SetActive(!this.parameter.inverseButtonPosition);
    this.m_RootInverseMode.SetActive(this.parameter.inverseButtonPosition);
    this.m_TitleLabel.text = this.parameter.titleString;
    this.m_ContentLabel.text = this.parameter.contentString;
    this.m_NormalLeftBtnLabel.text = this.parameter.leftLabelString;
    this.m_NormalRightBtnLabel.text = this.parameter.rightLabelString;
    this.m_InverseLeftBtnLabel.text = this.parameter.leftLabelString;
    this.m_InverseRightBtnLabel.text = this.parameter.rightLabelString;
  }

  public void OnLeftBtnClick()
  {
    if (this.parameter.onLeftBtnClick != null)
      this.parameter.onLeftBtnClick();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnRightBtnClick()
  {
    if (this.parameter.onRightBtnClick != null)
      this.parameter.onRightBtnClick();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnToggleIgnoreValueChanged()
  {
    if (this._toggle.value)
      PlayerPrefsEx.SetIntByUid(IgnorableMessageBoxPopup.GetPlayerPrefKey(this.parameter.key), 1);
    else
      PlayerPrefsEx.SetIntByUid(IgnorableMessageBoxPopup.GetPlayerPrefKey(this.parameter.key), 0);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string key = string.Empty;
    public string titleString;
    public string contentString;
    public string leftLabelString;
    public string rightLabelString;
    public System.Action onLeftBtnClick;
    public System.Action onRightBtnClick;
    public bool inverseButtonPosition;
  }
}
