﻿// Decompiled with JetBrains decompiler
// Type: SplashScreen
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class SplashScreen : MonoBehaviour
{
  private float refreshTime = 5f;
  private Vector3 orign = new Vector3(0.0f, 0.0f, 0.0f);
  private float durationTime = 1f;
  private string tip_pre = "loading_tip_";
  private string tip_joke = "loading_quote_";
  private int TIP_MAX_SIZE = 25;
  private int TIP_JOKE_SIZE = 25;
  private List<string> passTips = new List<string>();
  public UISlider progressBar;
  public UILabel progressMessage;
  public UIAnchor logoAnchor;
  public UITexture bgStretch;
  public UITexture moviePlayer;
  public UIStretch moviewStretch;
  public UILabel tip;
  public AudioSource bgMusic;
  private float currentTime;
  public UILabel version;
  private AsyncOperation _operation;
  private float _baseValue;
  private float _targetValue;
  public GameObject loadingEffect;
  private string annoucement;
  public UIButton startGameBtn;
  public System.Action onStartGameClickedCallback;
  public UILabel annoucementContent;
  public UILabel buttonDes;
  public UIButton helpBtn;
  public GameObject loadingAnnouncement;
  public UIScrollView scrollView;
  private float elapseTime;
  private int type;
  private bool isStopRefreshTip;
  private bool isAdd;
  public System.Action onSplashScreenHide;

  private void Start()
  {
    this.logoAnchor.uiCamera = UIManager.inst.ui2DCamera;
    this.moviewStretch.uiCamera = UIManager.inst.ui2DCamera;
    this.progressBar.value = 0.0f;
    SplashDataConfig.Init();
    NGUITools.SetActive(this.tip.gameObject, false);
    NGUITools.SetActive(this.startGameBtn.gameObject, false);
  }

  public void ShowStartGameBtn()
  {
  }

  public void HideStartGameBtn()
  {
  }

  public void OnHelpBtnClicked()
  {
    AccountManager.Instance.ShowFASQs();
  }

  public void OnStartGameClicked()
  {
    if (this.onStartGameClickedCallback == null)
      return;
    this.onStartGameClickedCallback();
  }

  public void ShowBg()
  {
    this.bgStretch.gameObject.SetActive(true);
    this.moviePlayer.gameObject.SetActive(false);
    if (SettingManager.Instance.IsMusicOff)
      return;
    AudioManager.Instance.StopCurrentBGM();
    this.bgMusic.Play();
  }

  private void ShowAnnouncement()
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.loadingAnnouncement))
      return;
    if (!string.IsNullOrEmpty(this.annoucement))
    {
      this.loadingAnnouncement.SetActive(true);
      this.annoucementContent.text = Utils.XLAT(this.annoucement);
      this.buttonDes.text = Utils.XLAT("account_contact_support_button");
      this.scrollView.ResetPosition();
    }
    else
      this.loadingAnnouncement.SetActive(false);
  }

  public void HideBg()
  {
    if (SettingManager.Instance.IsMusicOff)
      return;
    this.bgMusic.Stop();
  }

  public void SetVersion()
  {
    this.version.text = "V " + NativeManager.inst.AppMajorVersion + CustomDefine.GetVersionFlag() + CustomDefine.GetSDKFlag();
  }

  private void Update()
  {
    this.loadingEffect.SetActive((double) this.progressBar.value > 0.0);
    if (this._operation != null && !this._operation.isDone && (double) this._targetValue < (double) this._operation.progress)
      this._targetValue = this._operation.progress;
    this.elapseTime += Time.deltaTime;
    this.progressBar.value = Mathf.Lerp(this.progressBar.value, this._targetValue, this.elapseTime);
    Vector3 localPosition = this.loadingEffect.transform.localPosition;
    localPosition.x = (float) ((double) this.orign.x + 1278.0 * (double) this.progressBar.value + 70.0);
    this.loadingEffect.transform.localPosition = localPosition;
    if ((double) this.elapseTime > 1.0)
      this.elapseTime = 0.0f;
    if ((double) this.progressBar.value != 1.0 || this.isStopRefreshTip)
      return;
    this.FinishProgress();
  }

  private void RefreshTip()
  {
    this.tip.text = this.GetNextTip();
  }

  private void UpdateCoutine(int time)
  {
    ++this.currentTime;
    if ((double) this.currentTime % (double) this.refreshTime != 0.0)
      return;
    this.RefreshTip();
    this.currentTime = 0.0f;
  }

  private string GetNextTip()
  {
    this.type = this.type != 1 ? 1 : 0;
    string str = this.tip_pre;
    int max = this.TIP_MAX_SIZE;
    if (this.type == 1)
    {
      str = this.tip_joke;
      max = this.TIP_JOKE_SIZE;
    }
    if (this.passTips.Count == this.TIP_MAX_SIZE + this.TIP_JOKE_SIZE)
      this.passTips.Clear();
    int num = UnityEngine.Random.Range(1, max);
    return ScriptLocalization.Get(str + (object) num, true);
  }

  public void AsyncProgress(AsyncOperation inOperation, float inBaseValue, string inMessage)
  {
    if (inOperation == null)
      return;
    this.Show();
    this._operation = inOperation;
    this.progressMessage.text = inMessage;
  }

  public void FinishProgress()
  {
    this.isStopRefreshTip = true;
  }

  public bool showing { get; set; }

  public void Show()
  {
    if (this.showing)
      return;
    this.showing = true;
    if (!this.isAdd)
    {
      this.isAdd = true;
      this.AddEventHandler();
    }
    this.gameObject.SetActive(true);
    this.ShowBg();
    this.RefreshTip();
  }

  private void RemoveEventHandler()
  {
    GameLoadingAnnouncementPayload.Instance.AnnoucementContentReady -= new System.Action(this.OnAnnoucementContentReady);
  }

  private void AddEventHandler()
  {
    GameLoadingAnnouncementPayload.Instance.AnnoucementContentReady += new System.Action(this.OnAnnoucementContentReady);
  }

  private void OnAnnoucementContentReady()
  {
    this.annoucement = GameLoadingAnnouncementPayload.Instance.AnnouncementContent;
    this.ShowAnnouncement();
  }

  private void OnGameModeStarted(bool clear)
  {
    if (!clear)
      return;
    this.Clear();
  }

  public void Clear()
  {
    this._targetValue = 0.0f;
    this._baseValue = 0.0f;
    this.progressBar.value = 0.0f;
    this.elapseTime = 0.0f;
  }

  public void Hide()
  {
    this.showing = false;
    this.HideBg();
    this.Clear();
    this.gameObject.SetActive(false);
    this.RemoveEventHandler();
    this.isAdd = false;
    this.annoucement = (string) null;
    if (this.onSplashScreenHide == null)
      return;
    this.onSplashScreenHide();
  }

  public void SetMessage(string text)
  {
    if (!((UnityEngine.Object) this.progressMessage != (UnityEngine.Object) null))
      return;
    if (GameEngine.ReloadMode != GameEngine.LoadMode.Lite)
      this.progressMessage.text = text;
    else
      this.progressMessage.text = ScriptLocalization.Get("load_internal_sync_data_tip", true);
  }

  public void SetValue(float progress)
  {
    if ((double) this._targetValue >= (double) progress)
      return;
    this._targetValue = progress;
  }
}
