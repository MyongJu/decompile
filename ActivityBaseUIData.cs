﻿// Decompiled with JetBrains decompiler
// Type: ActivityBaseUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ActivityBaseUIData
{
  private object _data;
  private ActivityBaseUIData.ActivityType _type;

  public object Data
  {
    get
    {
      return this._data;
    }
    set
    {
      this._data = value;
    }
  }

  public virtual int StartTime
  {
    get
    {
      return 0;
    }
  }

  public virtual int EndTime
  {
    get
    {
      return 0;
    }
  }

  public virtual bool ShowADTime
  {
    get
    {
      return true;
    }
  }

  public virtual string EventKey
  {
    get
    {
      return string.Empty;
    }
  }

  public virtual int ActivityID
  {
    get
    {
      return 0;
    }
  }

  public virtual string TimePre
  {
    get
    {
      return this.IsStart() ? "event_finishes_num" : "event_starts_num";
    }
  }

  public virtual ActivityBaseUIData.ActivityType Type
  {
    get
    {
      return this._type;
    }
  }

  public virtual bool IsStart()
  {
    if (this.StartTime <= NetServerTime.inst.ServerTimestamp)
      return this.EndTime > NetServerTime.inst.ServerTimestamp;
    return false;
  }

  public virtual bool IsFinish()
  {
    return this.EndTime <= NetServerTime.inst.ServerTimestamp;
  }

  public virtual int GetRemainTime()
  {
    if (this.IsStart())
      return this.EndTime - NetServerTime.inst.ServerTimestamp;
    return this.StartTime - NetServerTime.inst.ServerTimestamp;
  }

  public virtual IconData GetADIconData()
  {
    return (IconData) null;
  }

  public virtual bool HadRedPoint()
  {
    if (this.IsFinish() || !this.IsStart())
      return false;
    if (PlayerPrefsEx.GetIntByUid(this.StartTime.ToString() + "_" + (object) this.Type + "_" + (object) this.EndTime, -1) == -1 && this.IsStart())
      return !this.IsFinish();
    return false;
  }

  public virtual int GetStar()
  {
    return ConfigManager.inst.DB_ActivityPriority.GetPriorityStart(this.EventKey);
  }

  public void ClearRedPont()
  {
    if (!this.HadRedPoint())
      return;
    PlayerPrefsEx.SetIntByUid(this.StartTime.ToString() + "_" + (object) this.Type + "_" + (object) this.EndTime, 1);
  }

  public virtual IconData GetUnstartIconData()
  {
    return (IconData) null;
  }

  public virtual IconData GetNormalIconData()
  {
    return (IconData) null;
  }

  public virtual void DisplayActivityDetail()
  {
  }

  public enum ActivityType
  {
    None,
    TimeLimit,
    AllianceBoss,
    AllianceActivity,
    AlianceWar,
    Festival,
    Knight,
    Personal,
    Pit,
    KingdomWar,
    WorldBoss,
    DKAreana,
  }
}
