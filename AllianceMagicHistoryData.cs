﻿// Decompiled with JetBrains decompiler
// Type: AllianceMagicHistoryData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;

public class AllianceMagicHistoryData : IHistoryData
{
  private string type = nameof (owner);
  private int skillId;
  private string status;
  private int startTime;
  private AllianceMagicHistoryData.Owner owner;
  private AllianceMagicHistoryData.Targets target;
  private string ownerName;
  private long ownerAllianceId;
  private long time;
  private int magicConfigId;
  private string targetNames;
  private string content;
  private bool isMyAlliance;

  public int StartTime
  {
    get
    {
      return this.startTime;
    }
  }

  public string Content
  {
    get
    {
      return this.content;
    }
    set
    {
      this.content = value;
    }
  }

  public bool IsAlly
  {
    get
    {
      return this.isMyAlliance;
    }
    set
    {
      this.isMyAlliance = value;
    }
  }

  public bool IsMyAlliance
  {
    get
    {
      return this.isMyAlliance;
    }
  }

  public bool Decode(Hashtable source)
  {
    if (source == null || !source.ContainsKey((object) "owner") || !source.ContainsKey((object) "target"))
      return false;
    string empty = string.Empty;
    if (source.ContainsKey((object) "config_id") && source[(object) "config_id"] != null)
      int.TryParse(source[(object) "config_id"].ToString(), out this.skillId);
    if (source.ContainsKey((object) "start_time"))
      int.TryParse(source[(object) "start_time"].ToString(), out this.startTime);
    if (source.ContainsKey((object) "state"))
      this.status = source[(object) "state"].ToString();
    if (source.ContainsKey((object) "type"))
      this.type = source[(object) "type"].ToString();
    this.owner = new AllianceMagicHistoryData.Owner();
    this.owner.Decode(source[(object) "owner"] as Hashtable);
    this.target = new AllianceMagicHistoryData.Targets();
    this.target.Decode(source[(object) "target"] as ArrayList);
    this.Init();
    return true;
  }

  private string GetFormatTime()
  {
    int num1 = NetServerTime.inst.ServerTimestamp - this.startTime;
    if (num1 < 60)
      return Utils.XLAT("chat_time_less_than_min");
    int num2 = num1 / 3600 / 24;
    int num3 = num1 / 3600 % 24;
    int num4 = num1 % 3600 / 60;
    return ScriptLocalization.GetWithPara("chat_time_display", new Dictionary<string, string>()
    {
      {
        "d",
        num2 <= 0 ? string.Empty : num2.ToString() + Utils.XLAT("chat_time_day")
      },
      {
        "h",
        num3 <= 0 ? string.Empty : num3.ToString() + Utils.XLAT("chat_time_hour")
      },
      {
        "m",
        num4 < 0 ? string.Empty : num4.ToString() + Utils.XLAT("chat_time_min")
      }
    }, true);
  }

  private void Init()
  {
    this.isMyAlliance = this.type == "owner";
    string formatTime = this.GetFormatTime();
    TempleSkillInfo byId = ConfigManager.inst.DB_TempleSkill.GetById(this.skillId);
    string str = this.target.Join(',');
    Dictionary<string, string> para = new Dictionary<string, string>();
    if (this.status == "interrupt" || string.IsNullOrEmpty(str))
    {
      para.Add("0", byId.Name);
      para.Add("1", this.owner.uname);
      if (string.IsNullOrEmpty(str))
      {
        this.content = formatTime + " " + ScriptLocalization.GetWithPara("alliance_altar_spell_history_disrupt_lost", para, true);
      }
      else
      {
        para.Add("2", str);
        this.content = formatTime + " " + ScriptLocalization.GetWithPara("alliance_altar_spell_history_disrupt", para, true);
      }
    }
    else
    {
      para.Add("0", this.owner.uname);
      para.Add("1", byId.Name);
      para.Add("2", str);
      string Term = string.Empty;
      string targetType = byId.TargetType;
      if (targetType != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (AllianceMagicHistoryData.\u003C\u003Ef__switch\u0024map67 == null)
        {
          // ISSUE: reference to a compiler-generated field
          AllianceMagicHistoryData.\u003C\u003Ef__switch\u0024map67 = new Dictionary<string, int>(6)
          {
            {
              "area",
              0
            },
            {
              "self_alliance",
              1
            },
            {
              "target_alliance",
              1
            },
            {
              "ops_city",
              2
            },
            {
              "ops_alliance_temple",
              2
            },
            {
              "self_hive_ops_city",
              2
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (AllianceMagicHistoryData.\u003C\u003Ef__switch\u0024map67.TryGetValue(targetType, out num))
        {
          switch (num)
          {
            case 0:
              Term = "alliance_altar_spell_history_cast_area";
              break;
            case 1:
              if (byId.TargetType == "target_alliance")
                para["2"] = this.target.GetOnlyAllianceTag();
              else if (byId.TargetType == "self_alliance")
                para["2"] = this.owner.atag;
              Term = "alliance_altar_spell_history_cast";
              break;
            case 2:
              Term = "alliance_altar_spell_history_cast_target";
              break;
          }
        }
      }
      this.content = formatTime + ": " + ScriptLocalization.GetWithPara(Term, para, true);
    }
  }

  private class Owner
  {
    public long uid;
    public string uname;
    public long aid;
    public string atag;

    public void Decode(Hashtable source)
    {
      if (source == null)
        return;
      string empty = string.Empty;
      if (source.ContainsKey((object) "uid"))
        long.TryParse(source[(object) "uid"].ToString(), out this.uid);
      if (source.ContainsKey((object) "atag"))
        this.atag = source[(object) "atag"].ToString();
      if (source.ContainsKey((object) "uname"))
      {
        this.uname = source[(object) "uname"].ToString();
        if (!string.IsNullOrEmpty(this.atag))
          this.uname = "[" + this.atag + "]" + this.uname;
      }
      if (!source.ContainsKey((object) "aid"))
        return;
      long.TryParse(source[(object) "aid"].ToString(), out this.aid);
    }
  }

  private class Targets
  {
    public List<AllianceMagicHistoryData.TargetItem> targets = new List<AllianceMagicHistoryData.TargetItem>();

    public void Decode(ArrayList all)
    {
      for (int index = 0; index < all.Count; ++index)
      {
        AllianceMagicHistoryData.TargetItem targetItem = new AllianceMagicHistoryData.TargetItem();
        if (targetItem.Decode(all[index] as Hashtable))
          this.targets.Add(targetItem);
      }
    }

    public string GetOnlyAllianceTag()
    {
      int index = 0;
      if (index < this.targets.Count)
        return this.targets[index].ToString();
      return string.Empty;
    }

    public string Join(char split = ',')
    {
      string empty = string.Empty;
      for (int index = 0; index < this.targets.Count; ++index)
      {
        empty += this.targets[index].ToString();
        if (index + 1 < this.targets.Count)
          empty += (string) (object) split;
      }
      return empty;
    }
  }

  private class TargetItem
  {
    public long allianceId;
    public string name;
    public long uid;
    public string acronym;

    public override string ToString()
    {
      if (string.IsNullOrEmpty(this.acronym))
        return this.name;
      return "[" + this.acronym + "]" + this.name;
    }

    public bool Decode(Hashtable source)
    {
      if (source == null)
        return false;
      string empty = string.Empty;
      if (source.ContainsKey((object) "alliance_id"))
        long.TryParse(source[(object) "alliance_id"].ToString(), out this.allianceId);
      if (source.ContainsKey((object) "name"))
        this.name = source[(object) "name"].ToString();
      if (source.ContainsKey((object) "uid"))
        long.TryParse(source[(object) "uid"].ToString(), out this.uid);
      if (source.ContainsKey((object) "acronym"))
        this.acronym = source[(object) "acronym"].ToString();
      return true;
    }
  }
}
