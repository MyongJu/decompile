﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerNew
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BuildingControllerNew : BuildingController
{
  private bool hasloadanim;
  private GameObject oldlevelbuild;
  private GameObject oldlevelbuildanim;
  private GameObject oldlevelflag;

  public override void SetLevel(int value, bool loadanim = true)
  {
    string str1 = string.Empty;
    if (value == 0)
    {
      str1 = "temp";
      value = 1;
    }
    this._level = value;
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.oldlevelflag)
    {
      this.oldlevelflag.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.oldlevelflag);
    }
    GameObject gameObject1 = UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/City/level_" + Utils.ZeroPad(value, 3), (System.Type) null)) as GameObject;
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.mLevelNr)
      Debug.LogWarning((object) ("null mLevelNr ~~~~~~" + this.gameObject.name));
    else
      gameObject1.transform.parent = this.mLevelNr.transform;
    gameObject1.transform.localPosition = Vector3.zero;
    gameObject1.transform.localScale = Vector3.one;
    this.oldlevelflag = gameObject1;
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.oldlevelbuild)
    {
      this.oldlevelbuild.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.oldlevelbuild);
      this.oldlevelbuild = (GameObject) null;
    }
    int fromBuildingLevel = CityManager.inst.GetIconLevelFromBuildingLevel(this._level, this.mBuildingItem != null ? this.mBuildingItem.mType : (string) null);
    string str2 = this.gameObject.name.Replace("(Clone)", string.Empty);
    GameObject gameObject2 = UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/City/" + str2 + "_l" + (object) fromBuildingLevel, (System.Type) null)) as GameObject;
    gameObject2.name += str1;
    gameObject2.transform.parent = this.transform;
    gameObject2.transform.localPosition = Vector3.zero;
    gameObject2.transform.localScale = Vector3.one;
    this.oldlevelbuild = gameObject2;
    if (!loadanim)
      return;
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.oldlevelbuildanim)
    {
      this.oldlevelbuildanim.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.oldlevelbuildanim);
      this.oldlevelbuildanim = (GameObject) null;
      this.hasloadanim = false;
    }
    string fullanimpath = "Prefab/City/" + str2 + "_l" + (object) fromBuildingLevel + "_anim";
    if (this.hasloadanim)
      return;
    AssetManager.Instance.LoadAsync(fullanimpath, (System.Action<UnityEngine.Object, bool>) ((animobj, ret) =>
    {
      if (!((UnityEngine.Object) null != animobj) || !((UnityEngine.Object) null != (UnityEngine.Object) this.gameObject))
        return;
      GameObject gameObject3 = UnityEngine.Object.Instantiate(animobj) as GameObject;
      gameObject3.transform.parent = this.transform;
      gameObject3.transform.localPosition = Vector3.zero;
      gameObject3.transform.localScale = Vector3.one;
      this.oldlevelbuildanim = gameObject3;
      AssetManager.Instance.UnLoadAsset(fullanimpath, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    }), (System.Type) null);
    this.hasloadanim = true;
  }

  public virtual void InitBuilding()
  {
  }

  public virtual void Dispose()
  {
  }

  public virtual bool IsIdleState()
  {
    if (this.mBuildingItem == null || "watchtower" == this.mBuildingItem.mType)
      return false;
    bool flag1 = BarracksManager.Instance.IsBuildingUpgrading(this.mBuildingID);
    bool flag2 = BarracksManager.Instance.IsUnitTraining(this.mBuildingID);
    bool flag3 = BarracksManager.Instance.IsWaitingCollection(this.mBuildingID);
    if (!flag1 && !flag2)
      return !flag3;
    return false;
  }

  public class BIClick
  {
    public string key = "tutorial_step";
    public string value;
  }

  public class BIFormat
  {
    public BuildingControllerNew.BIClick d_c1 = new BuildingControllerNew.BIClick();
  }
}
