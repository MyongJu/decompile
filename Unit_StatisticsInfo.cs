﻿// Decompiled with JetBrains decompiler
// Type: Unit_StatisticsInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class Unit_StatisticsInfo
{
  private int _internalID;
  private string _ID;
  private int _classInternalID;
  public string Troop_Class_ID;
  public string Type;
  public int Troop_Tier;
  public int Power;
  public string Strength_1;
  public string Strength_2;
  public string Weakness_1;
  public string Weakness_2;
  public int Attack_Level;
  public int Defense_Level;
  public int Health_Level;
  public int Speed_Level;
  public string Damage_Type;
  public string Image;
  public string Requirement_ID_1;
  public string Requirement_ID_2;
  public double Requirement_Value_1;
  public double Requirement_Value_2;
  public int Food;
  public int Wood;
  public int Ore;
  public int Silver;
  public float Upkeep;
  public int Training_Time;
  public int Heal_Food;
  public int Heal_Wood;
  public int Heal_Ore;
  public int Heal_Honor;
  public int Heal_Silver;
  public int Heal_Time;
  public int Health;
  public int Attack;
  public int Defense;
  public int GatherCapacity;
  public float Speed;
  public string AttackType;
  public string DefenseType;
  public string FromBuildingType;
  public int Priority;
  public float mGold;
  public int BuildSpeed;

  public Unit_StatisticsInfo(int internalID, string ID, int classInternalID)
  {
    this._internalID = internalID;
    this._ID = ID;
    this._classInternalID = classInternalID;
  }

  public int internalId
  {
    get
    {
      return this._internalID;
    }
  }

  public string ID
  {
    get
    {
      return this._ID;
    }
  }

  public int ClassInternalID
  {
    get
    {
      return this._classInternalID;
    }
  }

  public string Troop_Name_LOC_ID
  {
    get
    {
      return this.ID + "_name";
    }
  }

  public string Troop_ICON
  {
    get
    {
      return "Texture/Unit/portrait_unit_" + this.ID;
    }
  }

  public string Troop_IMAGE
  {
    get
    {
      return "Texture/Unit/image_unit_" + this.ID;
    }
  }

  public string Puppet2DAnimPath
  {
    get
    {
      return "Prefab/Puppet2DAnimation/Troops2D/image_unit_" + this.ID;
    }
  }

  public string Troop_Class_NAME_LOC
  {
    get
    {
      return this.Troop_Class_ID + "_name";
    }
  }

  public string Troop_Class_DES_LOC
  {
    get
    {
      return this.Troop_Class_ID + "_description";
    }
  }

  public string Troop_Class_ICON_PATH
  {
    get
    {
      return "Texture/TroopClass/icon_" + this.Troop_Class_ID;
    }
  }

  public float BenefitCostFood(int count)
  {
    return ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) count * (float) this.Food, "calc_train_army_cost_decrease");
  }

  public float BenefitCostWood(int count)
  {
    return ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) count * (float) this.Wood, "calc_train_army_cost_decrease");
  }

  public float BenefitCostOre(int count)
  {
    return ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) count * (float) this.Ore, "calc_train_army_cost_decrease");
  }

  public float BenefitCostSilver(int count)
  {
    return ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) count * (float) this.Silver, "calc_train_army_cost_decrease");
  }

  public float Heal_Food_Benifit
  {
    get
    {
      return ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) this.Heal_Food, "calc_healing_army_cost_decrease");
    }
  }

  public float Heal_Wood_Benifit
  {
    get
    {
      return ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) this.Heal_Wood, "calc_healing_army_cost_decrease");
    }
  }

  public float Heal_Ore_Benifit
  {
    get
    {
      return ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) this.Heal_Ore, "calc_healing_army_cost_decrease");
    }
  }

  public float Heal_Honor_Benifit
  {
    get
    {
      return ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) this.Heal_Honor, "calc_alliance_hospital_healing_cost");
    }
  }

  public float Heal_Silver_Benifit
  {
    get
    {
      return ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) this.Heal_Silver, "calc_healing_army_cost_decrease");
    }
  }

  public string HEAL_TIME_CAL_LEY
  {
    get
    {
      return string.Format("calc_{0}_healing_time", (object) this.Troop_Class_ID).Replace("_class", string.Empty);
    }
  }

  public string TRAIN_TIME_CAL_KEY
  {
    get
    {
      return string.Format("calc_{0}_training_time", (object) this.Troop_Class_ID).Replace("_class", string.Empty);
    }
  }

  public int GetTrainLimit()
  {
    if (this.FromBuildingType == "fortress")
      return Mathf.Min((int) ConfigManager.inst.DB_BenefitCalc.GetFinalData(DBManager.inst.DB_Local_Benefit.Get("prop_trap_capacity_base_value").total, "calc_trap_capacity") - BarracksManager.Instance.GetTotalTrapNum(), (int) ConfigManager.inst.DB_BenefitCalc.GetFinalData(DBManager.inst.DB_Local_Benefit.Get("prop_trap_training_capacity_base_value").total, "calc_trap_training_capacity"));
    return (int) ConfigManager.inst.DB_BenefitCalc.GetFinalData(DBManager.inst.DB_Local_Benefit.Get("prop_army_training_capacity_base_value").total, string.Format("calc_{0}_training_capacity", (object) this.Troop_Class_ID.Replace("class_", string.Empty)));
  }

  public int GetTapBuildCapacity()
  {
    return (int) ConfigManager.inst.DB_BenefitCalc.GetFinalData(DBManager.inst.DB_Local_Benefit.Get("prop_trap_capacity_base_value").total, "calc_trap_capacity");
  }

  public int GetUnitTypeIndex()
  {
    return (int) Unit_StatisticsInfo.GetUnitTypeByString(this.Type);
  }

  public TroopType GetUnitType()
  {
    return Unit_StatisticsInfo.GetUnitTypeByString(this.Type);
  }

  private static TroopType GetUnitTypeByString(string stringType)
  {
    string key = stringType;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (Unit_StatisticsInfo.\u003C\u003Ef__switch\u0024map4C == null)
      {
        // ISSUE: reference to a compiler-generated field
        Unit_StatisticsInfo.\u003C\u003Ef__switch\u0024map4C = new Dictionary<string, int>(6)
        {
          {
            "infantry",
            0
          },
          {
            "cavalry",
            1
          },
          {
            "ranged",
            2
          },
          {
            "mage",
            3
          },
          {
            "siege",
            4
          },
          {
            "trap",
            5
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (Unit_StatisticsInfo.\u003C\u003Ef__switch\u0024map4C.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return TroopType.class_infantry;
          case 1:
            return TroopType.class_cavalry;
          case 2:
            return TroopType.class_ranged;
          case 3:
            return TroopType.class_artyclose;
          case 4:
            return TroopType.class_artyfar;
          case 5:
            return TroopType.class_trap;
        }
      }
    }
    return TroopType.invalid;
  }
}
