﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerWall
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class BuildingControllerWall : BuildingControllerNew
{
  public Transform crack;

  public override void AddActionButton(ref List<CityCircleBtnPara> ftl)
  {
    base.AddActionButton(ref ftl);
    CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_defend", "id_wall_defense", new EventDelegate(new EventDelegate.Callback(CitadelSystem.inst.OnWallDefensePressed)), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ftl.Add(cityCircleBtnPara);
  }

  public override void SetLevel(int value, bool loadanim = true)
  {
    base.SetLevel(value, loadanim);
    this.crack = BuildingControllerWall.FindGameObject(this.transform, "Crack");
  }

  private static Transform FindGameObject(Transform root, string name)
  {
    if (root.gameObject.name == name)
      return root;
    int childCount = root.childCount;
    for (int index = 0; index < childCount; ++index)
    {
      Transform gameObject = BuildingControllerWall.FindGameObject(root.GetChild(index), name);
      if ((Object) gameObject != (Object) null)
        return gameObject;
    }
    return (Transform) null;
  }

  public override bool IsIdleState()
  {
    return false;
  }
}
