﻿// Decompiled with JetBrains decompiler
// Type: NotificationSettingItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class NotificationSettingItem : MonoBehaviour
{
  public UILabel title;
  public UILabel desc;
  public UISprite selectedImg;
  public UISprite normalImg;
  private bool _selected;
  [NonSerialized]
  public int type;

  public bool Selected
  {
    get
    {
      return this._selected;
    }
    set
    {
      if (this._selected == value)
        return;
      this._selected = value;
      NGUITools.SetActive(this.selectedImg.gameObject, this._selected);
      NGUITools.SetActive(this.normalImg.gameObject, !this._selected);
    }
  }

  public void OnClickHandler()
  {
    this.Selected = !this.Selected;
    NotificationManager.Instance.SaveSetting(this.type, this.Selected);
  }
}
