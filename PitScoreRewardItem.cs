﻿// Decompiled with JetBrains decompiler
// Type: PitScoreRewardItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitScoreRewardItem : MonoBehaviour
{
  private List<Icon> _allIcon = new List<Icon>();
  [SerializeField]
  private UILabel _labelScoreRequirement;
  [SerializeField]
  private UIGrid _tableRewardItemContainer;
  [SerializeField]
  private GameObject _rootCompleted;
  [SerializeField]
  private GameObject _rootUncompleted;
  [SerializeField]
  private GameObject _rootCollected;
  [SerializeField]
  private Icon _iconTemplate;
  private AbyssCollectRewardInfo _abyssCollectRewardInfo;

  private Icon CreateIcon()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._iconTemplate.gameObject);
    gameObject.transform.SetParent(this._tableRewardItemContainer.transform, false);
    gameObject.SetActive(true);
    Icon component = gameObject.GetComponent<Icon>();
    this._allIcon.Add(component);
    return component;
  }

  private void DestroyAllIcon()
  {
    using (List<Icon>.Enumerator enumerator = this._allIcon.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Icon current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
        {
          current.transform.SetParent((Transform) null);
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
        }
      }
    }
    this._allIcon.Clear();
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  private void Reset()
  {
    this._rootCollected.gameObject.SetActive(false);
    this._rootUncompleted.gameObject.SetActive(false);
    this._rootCompleted.gameObject.SetActive(false);
  }

  public void SetData(AbyssCollectRewardInfo abyssCollectRewardInfo)
  {
    this.Reset();
    PitActivityData activityData = PitExplorePayload.Instance.ActivityData;
    this._abyssCollectRewardInfo = abyssCollectRewardInfo;
    this._labelScoreRequirement.text = abyssCollectRewardInfo.NumReq.ToString();
    this.DestroyAllIcon();
    using (List<Reward.RewardsValuePair>.Enumerator enumerator = this._abyssCollectRewardInfo.Rewards.GetRewards().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Reward.RewardsValuePair current = enumerator.Current;
        IconData iconData = new IconData(ConfigManager.inst.DB_Items.GetItem(current.internalID).ImagePath, new string[1]
        {
          current.value.ToString()
        });
        iconData.Data = (object) current.internalID;
        Icon icon = this.CreateIcon();
        icon.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
        icon.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
        icon.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
        icon.StopAutoFillName();
        icon.FeedData((IComponentData) iconData);
      }
    }
    if (activityData.CurrentScore >= (long) abyssCollectRewardInfo.NumReq)
    {
      if (activityData.AllClaimedScoreReward.Contains(abyssCollectRewardInfo.internalId))
        this._rootCollected.gameObject.SetActive(true);
      else
        this._rootCompleted.gameObject.SetActive(true);
    }
    else
      this._rootUncompleted.gameObject.SetActive(true);
    this._tableRewardItemContainer.Reposition();
  }

  public void OnButtonCollectClicked()
  {
    Debug.Log((object) nameof (OnButtonCollectClicked));
    RequestManager.inst.SendRequest("Abyss:claimRewards", new Hashtable()
    {
      {
        (object) "id",
        (object) this._abyssCollectRewardInfo.internalId
      }
    }, (System.Action<bool, object>) ((result, data) =>
    {
      if (!result)
        return;
      PitActivityData activityData = PitExplorePayload.Instance.ActivityData;
      activityData.AllClaimedScoreReward.Clear();
      Hashtable hashtable = data as Hashtable;
      if (hashtable != null && hashtable.ContainsKey((object) "claimed"))
      {
        ArrayList arrayList = hashtable[(object) "claimed"] as ArrayList;
        if (arrayList != null)
        {
          foreach (object obj in arrayList)
          {
            int result1 = 0;
            if (int.TryParse(obj.ToString(), out result1))
              activityData.AllClaimedScoreReward.Add(result1);
          }
        }
      }
      RewardsCollectionAnimator.Instance.Clear();
      List<Reward.RewardsValuePair> rewards = this._abyssCollectRewardInfo.Rewards.GetRewards();
      for (int index = 0; index < rewards.Count; ++index)
      {
        Reward.RewardsValuePair rewardsValuePair = rewards[index];
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardsValuePair.internalID);
        RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
        {
          icon = itemStaticInfo.ImagePath,
          count = (float) rewardsValuePair.value
        });
      }
      RewardsCollectionAnimator.Instance.CollectItems(false);
      this.SetData(this._abyssCollectRewardInfo);
    }), true);
  }
}
