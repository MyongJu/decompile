﻿// Decompiled with JetBrains decompiler
// Type: MegaphoneContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class MegaphoneContent
{
  public MegaphoneContent(string userName, string message)
  {
    this.UserName = userName;
    this.Content = message;
  }

  public string UserName { get; set; }

  public string Content { get; set; }

  public bool IsEmpty()
  {
    if (this.UserName != null)
      return this.Content == null;
    return true;
  }
}
