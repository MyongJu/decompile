﻿// Decompiled with JetBrains decompiler
// Type: KingdomTreasuryItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class KingdomTreasuryItemRenderer : MonoBehaviour
{
  public UISprite resSprite;
  public UILabel resName;
  public UILabel resAmount;
  private string _resType;
  private long _resAmount;

  public void SetData(string resType, long resAmount)
  {
    this._resType = resType;
    this._resAmount = resAmount;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (string.IsNullOrEmpty(this._resType))
      return;
    this.resSprite.spriteName = this._resType + "_icon";
    if ((Object) this.resName != (Object) null)
      this.resName.text = Utils.XLAT(this._resType + "_name");
    this.resAmount.text = "[3FC526]" + Utils.FormatThousands(this._resAmount.ToString()) + "[-]";
  }
}
