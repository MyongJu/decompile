﻿// Decompiled with JetBrains decompiler
// Type: WonderOccupyDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UnityEngine;

public class WonderOccupyDetail : MonoBehaviour
{
  public AllianceSymbol symbol;
  public UILabel allianceName;
  public UITexture icon;
  public UILabel userName;
  public GameObject tower;
  public GameObject wonder;
  public GameObject content;
  public UILabel kingLabel;
  public UILabel occupyLabel;
  public UILabel kingdomName;
  public UILabel kingdomDesc;

  public void SetData(StateUIBaseData data, object extraData)
  {
    NGUITools.SetActive(this.tower, data.Type == StateUIBaseData.DataType.Tower);
    NGUITools.SetActive(this.wonder, data.Type == StateUIBaseData.DataType.Avalon);
    if ((Object) this.kingdomName != (Object) null)
    {
      NGUITools.SetActive(this.kingdomName.gameObject, false);
      NGUITools.SetActive(this.kingdomDesc.gameObject, false);
      if (extraData != null)
      {
        Hashtable hashtable = extraData as Hashtable;
        string empty1 = string.Empty;
        string empty2 = string.Empty;
        string str = string.Empty;
        if (hashtable != null && hashtable.ContainsKey((object) "colony_kingdom") && hashtable[(object) "colony_kingdom"] != null)
          empty1 = hashtable[(object) "colony_kingdom"].ToString();
        if (hashtable != null && hashtable.ContainsKey((object) "colony_kingdom_id") && hashtable[(object) "colony_kingdom_id"] != null)
          empty2 = hashtable[(object) "colony_kingdom_id"].ToString();
        if (!string.IsNullOrEmpty(empty1))
          str = string.Format("{0} #{1}", (object) empty1, (object) empty2);
        else if (!string.IsNullOrEmpty(empty2))
          str = string.Format("{0} #{1}", (object) Utils.XLAT("id_kingdom"), (object) empty2);
        if (!string.IsNullOrEmpty(str))
        {
          NGUITools.SetActive(this.kingdomName.gameObject, true);
          NGUITools.SetActive(this.kingdomDesc.gameObject, true);
          this.kingdomName.text = str;
          this.kingdomDesc.text = ScriptLocalization.Get("throne_invasion_enemy_occupation_kingdom", true);
        }
      }
    }
    if (data.Type == StateUIBaseData.DataType.Avalon)
    {
      if ((data.Data as DB.WonderData).State == "protected")
      {
        NGUITools.SetActive(this.kingLabel.gameObject, true);
        NGUITools.SetActive(this.occupyLabel.gameObject, false);
      }
      else
      {
        NGUITools.SetActive(this.kingLabel.gameObject, false);
        NGUITools.SetActive(this.occupyLabel.gameObject, true);
      }
    }
    if (data.Occupyer == null)
    {
      NGUITools.SetActive(this.content, false);
    }
    else
    {
      NGUITools.SetActive(this.content, true);
      if (!data.Occupyer.IsJoinAlliance)
      {
        NGUITools.SetActive(this.symbol.gameObject, false);
        NGUITools.SetActive(this.allianceName.gameObject, false);
      }
      else
      {
        this.symbol.SetSymbols(data.Occupyer.Symbol);
        this.allianceName.text = data.Occupyer.AllianceName;
      }
      CustomIconLoader.Instance.requestCustomIcon(this.icon, UserData.GetPortraitIconPath(data.Occupyer.Portrait), data.Occupyer.Icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.icon, data.Occupyer.LordTitleId, 2);
      this.userName.text = data.Occupyer.UserName;
    }
  }

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
  }
}
