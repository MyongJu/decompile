﻿// Decompiled with JetBrains decompiler
// Type: WishWellPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class WishWellPayload
{
  public int gems;
  public int g_food;
  public int g_wood;
  public int g_ore;
  public int g_silver;
  public int g_steel;
  public int t_food;
  public int t_wood;
  public int t_ore;
  public int t_silver;
  public int t_steel;
  public int freeWishes;
  public long resetTime;
  public int critical;
  public int food;
  public int wood;
  public int ore;
  public int silver;
  public int steel;
  private static WishWellPayload m_Instance;

  private WishWellPayload()
  {
  }

  public event System.Action onWishWellDataUpdated;

  public static WishWellPayload Instance
  {
    get
    {
      if (WishWellPayload.m_Instance == null)
        WishWellPayload.m_Instance = new WishWellPayload();
      return WishWellPayload.m_Instance;
    }
  }

  public static void Dispose()
  {
    WishWellPayload.m_Instance = (WishWellPayload) null;
  }

  private int goldWishes
  {
    get
    {
      return this.g_food + this.g_wood + this.g_ore + this.g_silver + this.g_steel;
    }
  }

  private int totalWishes
  {
    get
    {
      return this.t_food + this.t_wood + this.t_ore + this.t_silver + this.t_steel;
    }
  }

  public void Decode(object data)
  {
    Hashtable inData = data as Hashtable;
    DatabaseTools.UpdateData(inData, "gem", ref this.gems);
    DatabaseTools.UpdateData(inData, "g_food", ref this.g_food);
    DatabaseTools.UpdateData(inData, "g_wood", ref this.g_wood);
    DatabaseTools.UpdateData(inData, "g_ore", ref this.g_ore);
    DatabaseTools.UpdateData(inData, "g_silver", ref this.g_silver);
    DatabaseTools.UpdateData(inData, "g_steel", ref this.g_steel);
    DatabaseTools.UpdateData(inData, "t_food", ref this.t_food);
    DatabaseTools.UpdateData(inData, "t_wood", ref this.t_wood);
    DatabaseTools.UpdateData(inData, "t_ore", ref this.t_ore);
    DatabaseTools.UpdateData(inData, "t_silver", ref this.t_silver);
    DatabaseTools.UpdateData(inData, "t_steel", ref this.t_steel);
    DatabaseTools.UpdateData(inData, "reset", ref this.resetTime);
    DatabaseTools.UpdateData(inData, "critical", ref this.critical);
    this.food = 0;
    this.wood = 0;
    this.ore = 0;
    this.silver = 0;
    this.steel = 0;
    DatabaseTools.UpdateData(inData, "food", ref this.food);
    DatabaseTools.UpdateData(inData, "wood", ref this.wood);
    DatabaseTools.UpdateData(inData, "ore", ref this.ore);
    DatabaseTools.UpdateData(inData, "silver", ref this.silver);
    DatabaseTools.UpdateData(inData, "steel", ref this.steel);
    this.freeWishes = this.totalWishes - this.goldWishes;
    if (this.onWishWellDataUpdated == null)
      return;
    this.onWishWellDataUpdated();
  }

  public string GetCategoryAndQuantity(out int quantity)
  {
    if (this.food > 0)
    {
      quantity = this.food;
      return "food";
    }
    if (this.wood > 0)
    {
      quantity = this.wood;
      return "wood";
    }
    if (this.ore > 0)
    {
      quantity = this.ore;
      return "ore";
    }
    if (this.silver > 0)
    {
      quantity = this.silver;
      return "silver";
    }
    if (this.steel > 0)
    {
      quantity = this.steel;
      return "steel";
    }
    quantity = 0;
    return string.Empty;
  }

  public int GetGoldWishes(string type)
  {
    string key = type;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (WishWellPayload.\u003C\u003Ef__switch\u0024mapB3 == null)
      {
        // ISSUE: reference to a compiler-generated field
        WishWellPayload.\u003C\u003Ef__switch\u0024mapB3 = new Dictionary<string, int>(5)
        {
          {
            "food",
            0
          },
          {
            "wood",
            1
          },
          {
            "ore",
            2
          },
          {
            "silver",
            3
          },
          {
            "steel",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (WishWellPayload.\u003C\u003Ef__switch\u0024mapB3.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return this.g_food;
          case 1:
            return this.g_wood;
          case 2:
            return this.g_ore;
          case 3:
            return this.g_silver;
          case 4:
            return this.g_steel;
        }
      }
    }
    return 0;
  }

  public int GetTotalWishes(string type)
  {
    string key = type;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (WishWellPayload.\u003C\u003Ef__switch\u0024mapB4 == null)
      {
        // ISSUE: reference to a compiler-generated field
        WishWellPayload.\u003C\u003Ef__switch\u0024mapB4 = new Dictionary<string, int>(5)
        {
          {
            "food",
            0
          },
          {
            "wood",
            1
          },
          {
            "ore",
            2
          },
          {
            "silver",
            3
          },
          {
            "steel",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (WishWellPayload.\u003C\u003Ef__switch\u0024mapB4.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return this.t_food;
          case 1:
            return this.t_wood;
          case 2:
            return this.t_ore;
          case 3:
            return this.t_silver;
          case 4:
            return this.t_steel;
        }
      }
    }
    return 0;
  }
}
