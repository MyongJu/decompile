﻿// Decompiled with JetBrains decompiler
// Type: PlayerData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class PlayerData
{
  private Dictionary<int, HeroData> _heros = new Dictionary<int, HeroData>();
  public const int InvalidInventorySlot = -1;
  public const int InvalidItemID = -1;
  public const float LoadingGap = 0.05f;
  private static PlayerData _inst;
  private bool _isInited;
  public HostPlayer hostPlayer;
  private CityManager _cityData;
  private ContactData _contacts;
  private FriendStatuesManager _onlineFriendsManager;
  private MailController _mailData;
  private QuestManager _questManager;
  private Properties _properties;
  private AllianceTechManager _allianceTechManager;
  private AllianceWarManager _allianceWarManager;
  private DragonController _dragonController;
  private PlayerModeratorInfo _modInfo;
  public int npcStoreUpdateTime;
  public int magicStoreUpdateTime;
  private int _serverOpenTime;
  private int _exchangeSwitch;
  private bool _buildingGloryOpen;
  private bool _merlinTrialsOpen;
  private int _pitMinWorldId;
  private int _pitMaxWorldId;
  private string _country;
  private UserData m_UserData;
  private DragonData m_DragonData;
  private DB.HeroData _heroData;
  private DB.CityData m_CityData;
  private PlayerData.MonthCardType _mct;
  private int _iapUiType;
  private PlayerData.NLPType _nlpType;
  private PlayerData.NLPEngine _nlpEngine;

  public static PlayerData inst
  {
    get
    {
      return GameEngine.Instance.PlayerData;
    }
  }

  public long uid
  {
    get
    {
      if (this.hostPlayer != null)
        return this.hostPlayer.uid;
      return -1;
    }
  }

  public int cityId
  {
    get
    {
      if (this._cityData != null)
        return this._cityData.GetCityID();
      return 0;
    }
  }

  public int ServerOpenTime
  {
    get
    {
      return this._serverOpenTime;
    }
  }

  public bool ExchangeSwitch
  {
    get
    {
      return this._exchangeSwitch == 1;
    }
  }

  public bool BuildingGloryOpen
  {
    get
    {
      return this._buildingGloryOpen;
    }
  }

  public bool MerlinTrialsOpen
  {
    get
    {
      return this._merlinTrialsOpen;
    }
  }

  public bool IsInPit
  {
    get
    {
      return MapUtils.IsPitWorld(this.CityData.Location.K);
    }
  }

  public int PitMinWorldId
  {
    get
    {
      return this._pitMinWorldId;
    }
  }

  public int PitMaxWorldId
  {
    get
    {
      return this._pitMaxWorldId;
    }
  }

  public string Country
  {
    get
    {
      return this._country;
    }
    set
    {
      this._country = value;
    }
  }

  public UserData userData
  {
    get
    {
      if (this.m_UserData == null)
        this.m_UserData = DBManager.inst.DB_User.Get(this.hostPlayer.uid);
      return this.m_UserData;
    }
  }

  public DragonData dragonData
  {
    get
    {
      if (this.m_DragonData == null)
        this.m_DragonData = DBManager.inst.DB_Dragon.Get(this.hostPlayer.uid);
      return this.m_DragonData;
    }
  }

  public DragonKnightData dragonKnightData
  {
    get
    {
      return DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    }
  }

  public DB.HeroData heroData
  {
    get
    {
      if (this._heroData == null)
        this._heroData = DBManager.inst.DB_hero.Get(this.uid);
      return this._heroData;
    }
  }

  public long allianceId
  {
    get
    {
      if (this.userData != null)
        return this.userData.allianceId;
      return 0;
    }
    set
    {
      if (this.userData == null)
        return;
      this.userData.allianceId = value;
    }
  }

  public bool isAllianceR4Member
  {
    get
    {
      if (this.allianceId == 0L)
        return false;
      int num = 1;
      AllianceMemberData allianceMemberData = this.allianceData.members.Get(this.userData.uid);
      if (allianceMemberData != null)
        num = allianceMemberData.title;
      return num >= 4;
    }
  }

  public AllianceData allianceData
  {
    get
    {
      return DBManager.inst.DB_Alliance.Get(this.allianceId);
    }
  }

  public DB.CityData playerCityData
  {
    get
    {
      if (this.m_CityData == null)
        this.m_CityData = DBManager.inst.DB_City.Get((long) this.cityId);
      return this.m_CityData;
    }
  }

  public Properties Properties
  {
    get
    {
      return this._properties;
    }
  }

  public CityManager CityData
  {
    get
    {
      return this._cityData;
    }
  }

  public ContactData contacts
  {
    get
    {
      return this._contacts;
    }
  }

  public FriendStatuesManager onlineFriendsManager
  {
    get
    {
      return this._onlineFriendsManager;
    }
  }

  public MailController mail
  {
    get
    {
      return this._mailData;
    }
  }

  public QuestManager QuestManager
  {
    get
    {
      return this._questManager;
    }
  }

  public HeroData CurrentHero
  {
    get
    {
      return (HeroData) null;
    }
  }

  public AllianceTechManager allianceTechManager
  {
    get
    {
      return this._allianceTechManager;
    }
  }

  public AllianceWarManager allianceWarManager
  {
    get
    {
      return this._allianceWarManager;
    }
  }

  public DragonController dragonController
  {
    get
    {
      return this._dragonController;
    }
  }

  public PlayerModeratorInfo moderatorInfo
  {
    get
    {
      return this._modInfo;
    }
  }

  public void Init()
  {
    if (this._isInited)
      return;
    PlayerData._inst = this;
    this._properties = new Properties(0);
    this.hostPlayer = new HostPlayer();
    this._cityData = new CityManager();
    this._cityData.Init();
    this._contacts = new ContactData();
    this._onlineFriendsManager = new FriendStatuesManager();
    this._mailData = new MailController(new MailAPI());
    this._questManager = new QuestManager();
    this._allianceTechManager = new AllianceTechManager();
    this._allianceWarManager = new AllianceWarManager();
    this._dragonController = new DragonController(new DragonAPI());
    this._modInfo = new PlayerModeratorInfo();
    this._isInited = true;
  }

  public PlayerData.MonthCardType MonthCardValue
  {
    get
    {
      return this._mct;
    }
  }

  public int IapUiType
  {
    get
    {
      return this._iapUiType;
    }
  }

  public PlayerData.NLPType NLPTypeValue
  {
    get
    {
      return this._nlpType;
    }
  }

  public PlayerData.NLPEngine NLPEngineValue
  {
    get
    {
      return this._nlpEngine;
    }
  }

  public void InitData(Hashtable jsonHt)
  {
    this.hostPlayer.Init(jsonHt[(object) "user"]);
    this.hostPlayer.ConsecutiveLoginDays = int.Parse(jsonHt[(object) "consecutive_login_days"].ToString());
    if (jsonHt.ContainsKey((object) "open_gambit"))
      OpeningCinematicManager.Instance.Resolution = (OpeningCinematicManager.ResolutionType) int.Parse(jsonHt[(object) "open_gambit"].ToString());
    if (jsonHt.ContainsKey((object) "min_abyss_world_id") && jsonHt.ContainsKey((object) "max_abyss_world_id"))
    {
      int.TryParse(jsonHt[(object) "min_abyss_world_id"].ToString(), out this._pitMinWorldId);
      int.TryParse(jsonHt[(object) "max_abyss_world_id"].ToString(), out this._pitMaxWorldId);
    }
    if (jsonHt.ContainsKey((object) "tutorial_type"))
      TutorialManager.Instance.Resolution = (TutorialManager.ABTestType) int.Parse(jsonHt[(object) "tutorial_type"].ToString());
    if (jsonHt.ContainsKey((object) "early_goal_tutorial_type"))
      TutorialManager.Instance.EarlyGoalType = (TutorialManager.ABTestType) int.Parse(jsonHt[(object) "early_goal_tutorial_type"].ToString());
    if (jsonHt.ContainsKey((object) "join_alliance_stronghold_level"))
      TutorialManager.Instance.JoinAllianceStrongholdLevel = int.Parse(jsonHt[(object) "join_alliance_stronghold_level"].ToString());
    if (jsonHt.ContainsKey((object) "wonder_stronghold_level"))
      TutorialManager.Instance.EnterWonderStrongholdLevel = int.Parse(jsonHt[(object) "wonder_stronghold_level"].ToString());
    if (jsonHt.ContainsKey((object) "join_alliance_level"))
      TutorialManager.Instance.LastJoinAllianceLevel = int.Parse(jsonHt[(object) "join_alliance_level"].ToString());
    if (jsonHt.ContainsKey((object) "wonder_level"))
      TutorialManager.Instance.LastEnterWonderLevel = int.Parse(jsonHt[(object) "wonder_level"].ToString());
    if (jsonHt.ContainsKey((object) "join_alliance_abtest"))
    {
      int result = 0;
      if (jsonHt[(object) "join_alliance_abtest"] != null)
        int.TryParse(jsonHt[(object) "join_alliance_abtest"].ToString(), out result);
      AllianceManager.Instance.ActiveRecommendationType = (AllianceManager.AllianceActiveRecommendationType) result;
    }
    if (jsonHt.ContainsKey((object) "month_card_abtest"))
    {
      int result = 0;
      if (jsonHt[(object) "month_card_abtest"] != null)
        int.TryParse(jsonHt[(object) "month_card_abtest"].ToString(), out result);
      this._mct = (PlayerData.MonthCardType) result;
    }
    DatabaseTools.UpdateData(jsonHt, "new_iap_ui", ref this._iapUiType);
    if (jsonHt.ContainsKey((object) "translation_abtest"))
    {
      int result = 0;
      if (jsonHt[(object) "translation_abtest"] != null)
        int.TryParse(jsonHt[(object) "translation_abtest"].ToString(), out result);
      this._nlpType = (PlayerData.NLPType) result;
    }
    if (jsonHt.ContainsKey((object) "nlp_engine_abtest"))
    {
      int result = 0;
      if (jsonHt[(object) "nlp_engine_abtest"] != null)
        int.TryParse(jsonHt[(object) "nlp_engine_abtest"].ToString(), out result);
      this._nlpEngine = (PlayerData.NLPEngine) result;
    }
    if (jsonHt.ContainsKey((object) "pz_test"))
    {
      NetApi.inst.PZTest = jsonHt[(object) "pz_test"].ToString();
      this.CheckPZTest();
    }
    if (jsonHt.ContainsKey((object) "is_open_subscription"))
    {
      bool outData = false;
      DatabaseTools.UpdateData(jsonHt, "is_open_subscription", ref outData);
      SubscriptionSystem.Instance.OpenSubscribe = outData;
    }
    if (jsonHt.ContainsKey((object) "is_annv_open"))
    {
      bool outData = false;
      DatabaseTools.UpdateData(jsonHt, "is_annv_open", ref outData);
      AnniversaryIapPayload.Instance.IsOpen = outData;
    }
    if (jsonHt.ContainsKey((object) "is_world_auction_open"))
    {
      bool outData = false;
      DatabaseTools.UpdateData(jsonHt, "is_world_auction_open", ref outData);
      AuctionHelper.Instance.IsWorldAuctionOpen = outData;
    }
    if (jsonHt.ContainsKey((object) "is_build_glory_open"))
      DatabaseTools.UpdateData(jsonHt, "is_build_glory_open", ref this._buildingGloryOpen);
    if (jsonHt.ContainsKey((object) "is_merlin_trial_open"))
      DatabaseTools.UpdateData(jsonHt, "is_merlin_trial_open", ref this._merlinTrialsOpen);
    if (jsonHt.ContainsKey((object) "server_open_time") && jsonHt[(object) "server_open_time"] != null)
      int.TryParse(jsonHt[(object) "server_open_time"].ToString(), out this._serverOpenTime);
    if (jsonHt.ContainsKey((object) "exchange_switch") && jsonHt[(object) "exchange_switch"] != null)
      int.TryParse(jsonHt[(object) "exchange_switch"].ToString(), out this._exchangeSwitch);
    this._modInfo.InitFromHashtable(jsonHt);
    this._cityData.LoadCityIDs(jsonHt[(object) "city_ids"]);
    this._cityData.LoadCityData();
    this._onlineFriendsManager.Init(jsonHt[(object) "friends_online_status"]);
    this._questManager.Init();
    this.mail.Init();
    this._modInfo.Init();
    ActivityManager.Intance.Init();
    this.allianceWarManager.Init();
    this._dragonController.Init();
  }

  private void CheckPZTest()
  {
    if (!(NetApi.inst.PZTest == "1"))
      return;
    PZAndroid.Init("d3f96c644f97d87d6a70b1d853906d3e", "81e849724fc8fcf7bd6b71bc52d628d79828e847");
  }

  public HeroData GetHeroData(int inHeroID)
  {
    HeroData heroData = (HeroData) null;
    if (!this._heros.TryGetValue(inHeroID, out heroData))
    {
      heroData = HeroData.CreateHeroData(inHeroID);
      if (heroData == null)
        return (HeroData) null;
      this._heros.Add(inHeroID, heroData);
    }
    return heroData;
  }

  public static Faction GetFaction(long playerID, long allianceID)
  {
    if (playerID == PlayerData._inst.uid)
      return Faction.Self;
    return allianceID == PlayerData._inst.allianceId && allianceID > 0L ? Faction.Ally : Faction.Enemy;
  }

  public enum MonthCardType
  {
    NORMAL,
    TEST,
  }

  public enum NLPType
  {
    NORMAL,
    TEST,
  }

  public enum NLPEngine
  {
    NORMAL,
    BING,
  }
}
