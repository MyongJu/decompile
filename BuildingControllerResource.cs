﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerResource
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class BuildingControllerResource : BuildingControllerNew
{
  public float angle = 10f;
  private Matrix4x4 _offsetMatrix = new Matrix4x4();
  public float speedFactorBCR = 20f;
  public float reductionTimeBCR = 1f;
  public float timeInterval = 0.5f;
  public float animScale = 1.3f;
  private const double collectInCircle = 1.0;
  public const string speedupCollectionEffect = "fx_res_speedup_buff";
  public const string speedupCollectionEffect2 = "fx_res_speedup_buff_top";
  private const float flyTime = 1f;
  public BuildingControllerResource.ResType rt;
  private HubPort mBoostGenerationObject;
  private double curRes;
  private double generateSpeed;
  private double storageMaxValue;
  private double collectMin;
  private bool hasCollectionUI;
  private bool hasCollectionBtnInCircle;
  private double baseGenSpeed;
  private double baseStorage;
  private double collectionMin;
  private int speedupCost;
  private double lastcollectiontime;
  private double gen_boost;
  private double store_boost;
  private Coroutine btnCoroutine;
  private Coroutine uiCoroutine;
  private Coroutine speedupEffectCoroutine;
  private long speedupHandle;
  private long speedupHandle2;
  public float absorbBaseSpeedBCR;
  public float absorbAccelerationBCR;

  public double GenerateSpeed
  {
    get
    {
      float num1 = 0.0f;
      float num2 = 0.0f;
      switch (this.rt)
      {
        case BuildingControllerResource.ResType.Wood:
          num1 = ConfigManager.inst.DB_BenefitCalc.GetBenefitPercentage("calc_wood_generation") + AllianceBuildUtils.GetMyAllianceBenefit("prop_resource_generation_percent");
          num2 = ConfigManager.inst.DB_BenefitCalc.GetBenefitValue("calc_wood_generation");
          break;
        case BuildingControllerResource.ResType.Ore:
          num1 = ConfigManager.inst.DB_BenefitCalc.GetBenefitPercentage("calc_ore_generation") + AllianceBuildUtils.GetMyAllianceBenefit("prop_resource_generation_percent");
          num2 = ConfigManager.inst.DB_BenefitCalc.GetBenefitValue("calc_ore_generation");
          break;
        case BuildingControllerResource.ResType.Food:
          num1 = ConfigManager.inst.DB_BenefitCalc.GetBenefitPercentage("calc_food_generation") + AllianceBuildUtils.GetMyAllianceBenefit("prop_resource_generation_percent");
          num2 = ConfigManager.inst.DB_BenefitCalc.GetBenefitValue("calc_food_generation");
          break;
        case BuildingControllerResource.ResType.Silver:
          num1 = ConfigManager.inst.DB_BenefitCalc.GetBenefitPercentage("calc_silver_generation") + AllianceBuildUtils.GetMyAllianceBenefit("prop_resource_generation_percent");
          num2 = ConfigManager.inst.DB_BenefitCalc.GetBenefitValue("calc_silver_generation");
          break;
      }
      this.generateSpeed = this.baseGenSpeed * (1.0 + (double) num1) * (1.0 + this.gen_boost) + (double) num2;
      return this.generateSpeed;
    }
  }

  public double StorageMaxValue
  {
    get
    {
      float num1 = 0.0f;
      float num2 = 0.0f;
      switch (this.rt)
      {
        case BuildingControllerResource.ResType.Wood:
          num1 = ConfigManager.inst.DB_BenefitCalc.GetBenefitPercentage("calc_wood_storage") + AllianceBuildUtils.GetMyAllianceBenefit("prop_resource_storage_percent");
          num2 = ConfigManager.inst.DB_BenefitCalc.GetBenefitValue("calc_wood_storage");
          break;
        case BuildingControllerResource.ResType.Ore:
          num1 = ConfigManager.inst.DB_BenefitCalc.GetBenefitPercentage("calc_ore_storage") + AllianceBuildUtils.GetMyAllianceBenefit("prop_resource_storage_percent");
          num2 = ConfigManager.inst.DB_BenefitCalc.GetBenefitValue("calc_ore_storage");
          break;
        case BuildingControllerResource.ResType.Food:
          num1 = ConfigManager.inst.DB_BenefitCalc.GetBenefitPercentage("calc_food_storage") + AllianceBuildUtils.GetMyAllianceBenefit("prop_resource_storage_percent");
          num2 = ConfigManager.inst.DB_BenefitCalc.GetBenefitValue("calc_food_storage");
          break;
        case BuildingControllerResource.ResType.Silver:
          num1 = ConfigManager.inst.DB_BenefitCalc.GetBenefitPercentage("calc_silver_storage") + AllianceBuildUtils.GetMyAllianceBenefit("prop_resource_storage_percent");
          num2 = ConfigManager.inst.DB_BenefitCalc.GetBenefitValue("calc_silver_storage");
          break;
      }
      this.storageMaxValue = this.baseStorage * (1.0 + (double) num1) * (1.0 + this.store_boost) + (double) num2;
      return this.storageMaxValue;
    }
  }

  public double CurRes
  {
    get
    {
      this.curRes = (NetServerTime.inst.UpdateTime - this.lastcollectiontime) * this.GenerateSpeed;
      double storageMaxValue = this.StorageMaxValue;
      if (this.curRes > storageMaxValue)
        this.curRes = storageMaxValue;
      return this.curRes;
    }
  }

  public override void RefreshStats()
  {
    base.RefreshStats();
    this.UpdateStatus();
  }

  public void UpdateStatus()
  {
    if (!this.gameObject.activeInHierarchy)
      return;
    this.UpdateData();
    this.UpdateUI();
  }

  public override void InitBuilding()
  {
    base.InitBuilding();
    if (this.mBuildingItem == null)
      return;
    DBManager.inst.DB_CityMap.onDataUpdated += new System.Action<CityMapData>(this.OnUpgradeBuilding);
    this.mBoostGenerationObject = MessageHub.inst.GetPortByAction("City:boostResourceBuilding");
    this.UpdateStatus();
  }

  public override void Dispose()
  {
    base.Dispose();
    DBManager.inst.DB_CityMap.onDataUpdated -= new System.Action<CityMapData>(this.OnUpgradeBuilding);
  }

  public override void AddActionButton(ref List<CityCircleBtnPara> ftl)
  {
    string itemname = this.GetBoostItemName();
    int itemCount = ItemBag.Instance.GetItemCount(itemname);
    if (itemCount > 0 && this.mBuildingItem.mTrainingJobID == 0L)
    {
      GameObject gameObject = AssetManager.Instance.HandyLoad("Prefab/City/ItemBoostResAttachment", (System.Type) null) as GameObject;
      ItemBoostResAttachment.IBRAData ibraData1 = new ItemBoostResAttachment.IBRAData();
      ibraData1.content = "[fdf691]" + ScriptLocalization.GetWithPara("item_resource_boost_own", new Dictionary<string, string>()
      {
        {
          "num",
          itemCount.ToString()
        }
      }, true) + "[-]";
      ibraData1.typename = this.GetIconName();
      string ptid = ScriptLocalization.Get("id_resource_boost", true) + "\n[24fa24]1 " + ScriptLocalization.Get("id_day", true) + "[-]";
      GameObject attachment = gameObject;
      ItemBoostResAttachment.IBRAData ibraData2 = ibraData1;
      CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, string.Empty, ptid, new EventDelegate((EventDelegate.Callback) (() =>
      {
        AudioManager.Instance.PlaySound("sfx_rural_resource_boost", false);
        ItemBag.Instance.UseItem(itemname, new Hashtable()
        {
          {
            (object) "building_id",
            (object) this.mBuildingItem.mID
          }
        }, (System.Action<bool, object>) ((ret, obj) => this.UpdateStatus()), 1);
      })), true, (string) null, attachment, (CityTouchCircleBtnAttachment.AttachmentData) ibraData2, false);
      ftl.Add(cityCircleBtnPara);
    }
    if (this.mBuildingItem.mTrainingJobID > 0L)
    {
      string str = string.Empty;
      JobHandle job = JobManager.Instance.GetJob(this.mBuildingItem.mTrainingJobID);
      if (job != null && !job.IsFinished())
      {
        int num1 = job.LeftTime();
        int num2 = num1 / 3600;
        int num3 = num1 / 60;
        str = num2 != 0 ? (num2 + 1).ToString() + " " + ScriptLocalization.Get("id_hour", true) : (num3 + 1).ToString() + " " + ScriptLocalization.Get("id_minute", true);
      }
      CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_time_boost", ScriptLocalization.Get("item_resource_boost_remaining", true) + "\n[24fa24]" + str.ToString() + "[-]", (EventDelegate) null, false, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
      ftl.Add(cityCircleBtnPara);
    }
    else
    {
      GameObject gameObject = AssetManager.Instance.HandyLoad("Prefab/City/GoldBoostResAttachment", (System.Type) null) as GameObject;
      GoldBoostResAttachment.GBRAData gbraData1 = new GoldBoostResAttachment.GBRAData();
      gbraData1.content = PlayerData.inst.hostPlayer.Currency < this.speedupCost ? "[ff0000]" + this.speedupCost.ToString() + "[-]" : "[fdf691]" + this.speedupCost.ToString() + "[-]";
      string ptid = ScriptLocalization.Get("id_resource_boost", true) + "\n[24fa24]1 " + ScriptLocalization.Get("id_day", true) + "[-]";
      string confirm = ScriptLocalization.GetWithPara("confirm_gold_spend_resource_boost_desc", new Dictionary<string, string>()
      {
        {
          "num",
          this.speedupCost.ToString()
        },
        {
          "resource_tile_name",
          ScriptLocalization.Get(ConfigManager.inst.DB_Building.GetData(this.mBuildingItem.mType, this.mBuildingItem.mLevel).Building_LOC_ID, true)
        }
      }, true);
      GameObject attachment = gameObject;
      GoldBoostResAttachment.GBRAData gbraData2 = gbraData1;
      CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_time_boost", ptid, new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
      {
        confirmButtonClickEvent = (System.Action) (() => this.mBoostGenerationObject.SendRequest(new Hashtable()
        {
          {
            (object) "building_id",
            (object) this.mBuildingItem.mID
          }
        }, (System.Action<bool, object>) ((ret, obj) =>
        {
          AudioManager.Instance.PlaySound("sfx_rural_resource_boost", false);
          this.UpdateStatus();
        }), false)),
        description = confirm,
        goldNum = this.speedupCost
      }))), true, (string) null, attachment, (CityTouchCircleBtnAttachment.AttachmentData) gbraData2, false);
      ftl.Add(cityCircleBtnPara);
    }
    base.AddActionButton(ref ftl);
    CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, this.GetIconName(), "resource_collect", new EventDelegate((EventDelegate.Callback) (() => this.CollectResource())), this.hasCollectionBtnInCircle, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    double curRes = this.CurRes;
    float num = (float) ((1.0 <= curRes ? 0.0 : 1.0 - curRes) / this.GenerateSpeed);
    if ((double) num > 0.0)
    {
      CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, this.GetIconName(), "resource_collect", new EventDelegate((EventDelegate.Callback) (() => this.CollectResource())), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
      cityCircleBtnPara1.refreshTime = num;
      cityCircleBtnPara1.refreshPara = cityCircleBtnPara2;
    }
    ftl.Add(cityCircleBtnPara1);
  }

  private string GetBoostItemName()
  {
    switch (this.rt)
    {
      case BuildingControllerResource.ResType.Wood:
        return "item_boost_woodboost_24h";
      case BuildingControllerResource.ResType.Ore:
        return "item_boost_oreboost_24h";
      case BuildingControllerResource.ResType.Food:
        return "item_boost_foodboost_24h";
      case BuildingControllerResource.ResType.Silver:
        return "item_boost_silverboost_24h";
      default:
        return (string) null;
    }
  }

  private string GetIconName()
  {
    switch (this.rt)
    {
      case BuildingControllerResource.ResType.Wood:
        return "wood_icon";
      case BuildingControllerResource.ResType.Ore:
        return "ore_icon";
      case BuildingControllerResource.ResType.Food:
        return "food_icon";
      case BuildingControllerResource.ResType.Silver:
        return "silver_icon";
      default:
        return (string) null;
    }
  }

  public void OnUpgradeBuilding(CityMapData cmd)
  {
    if (!this.IsID(cmd.buildingId))
      return;
    this.UpdateStatus();
  }

  private void UpdateData()
  {
    if (this.mBuildingItem == null)
      return;
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData(this.mBuildingItem.mType, this.mBuildingItem.mLevel);
    this.baseGenSpeed = (double) data.GenerateInHour / 3600.0;
    this.baseStorage = (double) data.Storage;
    this.collectionMin = (double) data.CollectionMin;
    this.speedupCost = data.SpeedupGold;
    using (List<InBuildingData>.Enumerator enumerator = this.mBuildingItem.inBuildingData.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InBuildingData current = enumerator.Current;
        if ("gen_boost" == current.type)
          this.gen_boost = double.Parse(current.value);
        if ("store_boost" == current.type)
          this.store_boost = double.Parse(current.value);
        if ("update_time" == current.type)
          this.lastcollectiontime = double.Parse(current.value);
      }
    }
  }

  private void UpdateUI()
  {
    if (this.btnCoroutine != null)
      this.StopCoroutine(this.btnCoroutine);
    if (this.uiCoroutine != null)
      this.StopCoroutine(this.uiCoroutine);
    if (this.speedupEffectCoroutine != null && this.speedupHandle == 0L && this.speedupHandle2 == 0L)
      this.StopCoroutine(this.speedupEffectCoroutine);
    if (this.lastcollectiontime == 0.0)
      return;
    double curRes = this.CurRes;
    if (curRes == 0.0)
      return;
    double generateSpeed = this.GenerateSpeed;
    double time = (1.0 - curRes <= 0.0 ? 0.0 : 1.0 - curRes) / generateSpeed;
    if (time == 0.0)
      this.hasCollectionBtnInCircle = true;
    else
      this.btnCoroutine = this.StartCoroutine(this.SetCBtnInCircle(time));
    if (!this.hasCollectionUI)
    {
      this.uiCoroutine = this.StartCoroutine(this.SetCUI((this.collectionMin - curRes <= 0.0 ? 0.0 : this.collectionMin - curRes) / generateSpeed));
      this.ShiftBuildingStates(false);
    }
    if (this.mBuildingItem.mTrainingJobID <= 0L)
      return;
    JobHandle job = JobManager.Instance.GetJob(this.mBuildingItem.mTrainingJobID);
    if (job == null || job.IsFinished() || (this.speedupHandle != 0L || this.speedupHandle2 != 0L))
      return;
    int num = job.LeftTime();
    this.speedupHandle = VfxManager.Instance.CreateAndPlay("Prefab/VFX/fx_res_speedup_buff", this.transform);
    this.speedupHandle2 = VfxManager.Instance.CreateAndPlay("Prefab/VFX/fx_res_speedup_buff_top", this.transform);
    this.speedupEffectCoroutine = this.StartCoroutine(this.DestorySpeedupEffect((double) num));
  }

  private void CollectResource()
  {
    this.BroadcastCollectionEffect();
    this.BroadcastCollectionSFX();
    CitadelSystem.inst.CollectionList.Add(this.mBuildingItem.mID);
    this.FEUpdateData();
    this.UpdateStatus();
    this.ResetResource();
  }

  private void FEUpdateData()
  {
    this.lastcollectiontime = NetServerTime.inst.UpdateTime;
  }

  private void ResetResource()
  {
    this.hasCollectionBtnInCircle = false;
    this.hasCollectionUI = false;
    Transform child = this.transform.FindChild("CollectionResUI(Clone)");
    if ((UnityEngine.Object) null != (UnityEngine.Object) child)
      UnityEngine.Object.Destroy((UnityEngine.Object) child.gameObject);
    this.ShiftBuildingStates(false);
  }

  private void ShiftBuildingStates(bool canharvest)
  {
    GameObject child1 = Utils.FindChild(this.gameObject, "production");
    GameObject child2 = Utils.FindChild(this.gameObject, "harvest");
    if (!((UnityEngine.Object) null != (UnityEngine.Object) child1) || !((UnityEngine.Object) null != (UnityEngine.Object) child2))
      return;
    child1.SetActive(!canharvest);
    child2.SetActive(canharvest);
  }

  [DebuggerHidden]
  private IEnumerator SetCBtnInCircle(double time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BuildingControllerResource.\u003CSetCBtnInCircle\u003Ec__Iterator35()
    {
      time = time,
      \u003C\u0024\u003Etime = time,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator SetCUI(double time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BuildingControllerResource.\u003CSetCUI\u003Ec__Iterator36()
    {
      time = time,
      \u003C\u0024\u003Etime = time,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator DestorySpeedupEffect(double time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BuildingControllerResource.\u003CDestorySpeedupEffect\u003Ec__Iterator37()
    {
      time = time,
      \u003C\u0024\u003Etime = time,
      \u003C\u003Ef__this = this
    };
  }

  private void BroadcastCollectionEffect()
  {
    GameObject go = (GameObject) null;
    HUDResource hudResource = (HUDResource) null;
    switch (this.rt)
    {
      case BuildingControllerResource.ResType.Wood:
        go = CitadelSystem.inst.woodCollectionGOB;
        hudResource = CitadelSystem.inst.mPublicHUD.mWood;
        break;
      case BuildingControllerResource.ResType.Ore:
        go = CitadelSystem.inst.oreCollectionGOB;
        hudResource = CitadelSystem.inst.mPublicHUD.mOre;
        break;
      case BuildingControllerResource.ResType.Food:
        go = CitadelSystem.inst.foodCollectionGOB;
        hudResource = CitadelSystem.inst.mPublicHUD.mFood;
        break;
      case BuildingControllerResource.ResType.Silver:
        go = CitadelSystem.inst.silverCollectionGOB;
        hudResource = CitadelSystem.inst.mPublicHUD.mSilver;
        break;
    }
    long curRes = (long) this.CurRes;
    hudResource.PlayIncreaseAnimaition(hudResource.transform, new Vector3(0.0f, -170f, 0.0f), curRes);
    hudResource.StartCoroutine(this.PlayCollectionEffect(go, hudResource.mResouceIcon.transform));
    hudResource.PlayIncreaseAnimaition(this.transform, Vector3.zero, curRes);
  }

  [DebuggerHidden]
  private IEnumerator PlayCollectionEffect(GameObject go, Transform gt)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BuildingControllerResource.\u003CPlayCollectionEffect\u003Ec__Iterator38()
    {
      gt = gt,
      go = go,
      \u003C\u0024\u003Egt = gt,
      \u003C\u0024\u003Ego = go,
      \u003C\u003Ef__this = this
    };
  }

  private void Shoot(Vector3 beginpos, Vector3 finaldir, GameObject go, Transform gt)
  {
    GameObject effect = PrefabManagerEx.Instance.Spawn(go, gt.parent);
    effect.transform.localScale = new Vector3(this.animScale, this.animScale, this.animScale);
    effect.transform.localPosition = beginpos;
    Shooter st = gt.gameObject.AddComponent<Shooter>();
    st.Shoot(new Shooter.ShooterPara()
    {
      go = effect,
      begin = beginpos,
      end = gt.transform.localPosition,
      initspeed = finaldir.normalized,
      onFinished = (System.Action) (() =>
      {
        PrefabManagerEx.Instance.Destroy(effect);
        gt.GetComponent<UnityEngine.Animation>().Play();
        UnityEngine.Object.Destroy((UnityEngine.Object) st);
      }),
      absorbBaseSpeed = this.absorbBaseSpeedBCR,
      absorbAcceleration = this.absorbAccelerationBCR,
      speedFactor = this.speedFactorBCR,
      reductionTime = this.reductionTimeBCR
    }, 0.0f);
  }

  private void BroadcastCollectionSFX()
  {
    switch (this.rt)
    {
      case BuildingControllerResource.ResType.Wood:
        AudioManager.Instance.PlaySound("sfx_rural_collect_sawmill", false);
        break;
      case BuildingControllerResource.ResType.Ore:
        AudioManager.Instance.PlaySound("sfx_rural_collect_mine", false);
        break;
      case BuildingControllerResource.ResType.Food:
        AudioManager.Instance.PlaySound("sfx_rural_collect_farm", false);
        break;
      case BuildingControllerResource.ResType.Silver:
        AudioManager.Instance.PlaySound("sfx_rural_collect_house", false);
        break;
    }
  }

  private Vector3 GetBeginPosition(Transform target)
  {
    Vector3 worldPoint = UIManager.inst.ui2DCamera.ViewportToWorldPoint(UIManager.inst.cityCamera.GetComponent<Camera>().WorldToViewportPoint(this.transform.position));
    Vector3 vector3 = target.transform.InverseTransformPoint(worldPoint);
    vector3.z = -1f;
    return vector3;
  }

  private Vector3 GetTargetPosition(Transform target)
  {
    return this.transform.InverseTransformPoint(UIManager.inst.cityCamera.GetComponent<Camera>().ViewportToWorldPoint(UIManager.inst.ui2DCamera.WorldToViewportPoint(target.position)));
  }

  private int GetBulletCount()
  {
    int num1 = 0;
    double num2 = this.CurRes / this.StorageMaxValue;
    if (num2 < 0.2)
      num1 = 2;
    else if (num2 >= 0.2 && num2 < 0.4)
      num1 = 4;
    else if (num2 >= 0.4 && num2 < 0.6)
      num1 = 6;
    else if (num2 >= 0.6 && num2 < 0.8)
      num1 = 8;
    else if (num2 >= 0.8)
      num1 = 10;
    return num1;
  }

  internal float GetFullLeftTime()
  {
    double curRes = this.CurRes;
    double storageMaxValue = this.StorageMaxValue;
    if (curRes >= storageMaxValue)
      return 0.0f;
    return (float) ((storageMaxValue - curRes) / this.GenerateSpeed);
  }

  public override bool IsIdleState()
  {
    if (!this.hasCollectionUI)
      return !BarracksManager.Instance.IsBuildingUpgrading(this.mBuildingID);
    return false;
  }

  public enum ResType
  {
    Wood,
    Ore,
    Food,
    Silver,
  }
}
