﻿// Decompiled with JetBrains decompiler
// Type: NotificationManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class NotificationManager
{
  public static int GAME_ID = 3;
  public static string GAME_KEY = "8952120f6b0151366d58b450c5451cd7";
  public static string GAMEOBJECT_NAME = nameof (NotificationManager);
  private BetterList<NotificationData> localCaches = new BetterList<NotificationData>();
  private Dictionary<int, int> settings = new Dictionary<int, int>();
  private Dictionary<string, int> sendCount = new Dictionary<string, int>();
  private Dictionary<string, int> sendTime = new Dictionary<string, int>();
  private string _gotoParam = string.Empty;
  private static NotificationManager _instance;
  private bool isInit;
  private int ANDRIOD_REQUEST_CODE;
  private AndroidJavaObject nativeObj;
  private int delayTime;
  private bool flag;
  private bool needToGoto;

  public static NotificationManager Instance
  {
    get
    {
      if (NotificationManager._instance == null)
        NotificationManager._instance = new NotificationManager();
      return NotificationManager._instance;
    }
  }

  public void SetConfig(object result)
  {
    Hashtable hashtable = result as Hashtable;
    if (hashtable == null)
      return;
    this.settings.Clear();
    IEnumerator enumerator = hashtable.Keys.GetEnumerator();
    while (enumerator.MoveNext())
      this.settings.Add(int.Parse(enumerator.Current.ToString()), int.Parse(hashtable[enumerator.Current].ToString()));
  }

  private void AddSendTimeCount(string id)
  {
    if (!this.sendCount.ContainsKey(id))
      return;
    int num = 1;
    if (this.sendTime.ContainsKey(id))
    {
      Dictionary<string, int> sendTime;
      string index;
      (sendTime = this.sendTime)[index = id] = sendTime[index] + num;
    }
    else
      this.sendTime.Add(id, num);
  }

  private bool CanSend(NotificationData data)
  {
    if (data.RemainTime <= 0)
      return false;
    return this.CheckIDCanSend(data.ID);
  }

  private bool CheckIDCanSend(string id)
  {
    if (string.IsNullOrEmpty(id))
      return true;
    int type = ConfigManager.inst.DB_Notifications.GetData(id).type;
    if (this.settings.ContainsKey(type) && this.settings[type] == 0)
      return false;
    if (this.sendCount.ContainsKey(id))
    {
      if (this.sendCount[id] <= 0)
        return false;
      if (this.sendTime.ContainsKey(id))
      {
        if (this.sendTime[id] < this.sendCount[id])
          this.AddSendTimeCount(id);
        return this.sendTime[id] < this.sendCount[id];
      }
    }
    return true;
  }

  public bool IsOpen(int type)
  {
    if (this.settings.ContainsKey(type))
      return this.settings[type] == 1;
    return true;
  }

  public void SaveSetting(int type, bool isOpen)
  {
    int num = !isOpen ? 0 : 1;
    if (this.settings.ContainsKey(type))
      this.settings[type] = num;
    else
      this.settings.Add(type, num);
    Hashtable postData = new Hashtable();
    Hashtable hashtable = new Hashtable();
    for (int key = 1; key <= 11; ++key)
    {
      if (this.settings.ContainsKey(key))
        hashtable.Add((object) key.ToString(), (object) this.settings[key]);
      else
        hashtable.Add((object) key.ToString(), (object) 1);
    }
    postData.Add((object) "config", (object) hashtable);
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    RequestManager.inst.SendRequest("player:setNotificationConfig", postData, (System.Action<bool, object>) null, false);
  }

  public void Initialize()
  {
    this.ANDRIOD_REQUEST_CODE = PlayerPrefsEx.GetInt("notification_request_code");
    if (this.needToGoto)
    {
      this.needToGoto = false;
      UIManager.inst.OnWindowClear += new System.Action(this.OnWindowClear);
    }
    if (this.isInit)
      return;
    this.Clear();
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("notification_delay_time");
    if (data != null)
      this.delayTime = data.ValueInt;
    if (Application.platform != RuntimePlatform.IPhonePlayer && Application.platform == RuntimePlatform.Android && this.nativeObj == null)
    {
      this.nativeObj = new AndroidJavaObject("com.funplus.notification.NotificationServiceBridge", new object[0]);
      this.nativeObj.Call("startNotificationSerivce");
      this.ANDRIOD_REQUEST_CODE = PlayerPrefsEx.GetInt("notification_request_code");
    }
    this.sendCount.Add("resource_building_full", 1);
  }

  public void CheckNotificationData()
  {
    if (TutorialManager.Instance.IsRunning)
      return;
    this.GetActivtyNotifcationData();
    if (UIManager.inst.IsDialogClear && UIManager.inst.IsPopupClear)
      this.GotoFromNotificationData();
    else
      UIManager.inst.OnWindowClear += new System.Action(this.OnWindowClear);
  }

  private void GotoFromNotificationData()
  {
    if (string.IsNullOrEmpty(this._gotoParam))
      return;
    if (TutorialManager.Instance.IsRunning)
    {
      this._gotoParam = string.Empty;
    }
    else
    {
      LinkerHub.Instance.Distribute(this._gotoParam);
      this._gotoParam = string.Empty;
    }
  }

  private void OnWindowClear()
  {
    UIManager.inst.OnWindowClear -= new System.Action(this.OnWindowClear);
    this.GotoFromNotificationData();
  }

  public void PushLocalNotification(NotificationData data)
  {
    this.localCaches.Add(data);
  }

  public void PushLocalNotification(string configId, int finishTime, params string[] values)
  {
    this.PushLocalNotification(NotificationFactory.Instance.CreateData(configId, finishTime, values));
  }

  public void Start()
  {
    foreach (NotificationData localCach in this.localCaches)
      this.Send(localCach);
    this.localCaches.Clear();
    this.LoadNotifications();
  }

  protected void Send(NotificationData data)
  {
    switch (Application.platform)
    {
      case RuntimePlatform.Android:
        this._AndroidSend(data);
        break;
    }
  }

  protected void _AndroidSend(NotificationData data)
  {
    if (!this.CanSend(data) || this.nativeObj == null)
      return;
    this.nativeObj.Call("sendNotification", (object) data.MessageName, (object) data.MessageTitle, (object) data.Message, (object) data.Data, (object) data.SoundName, (object) (data.RemainTime + this.delayTime));
    this.ANDRIOD_REQUEST_CODE = this.nativeObj.GetStatic<int>("REQUEST_CODE");
    PlayerPrefsEx.SetInt("notification_request_code", this.ANDRIOD_REQUEST_CODE);
  }

  public static DateTime GetTime(string timeStamp)
  {
    return TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)).Add(new TimeSpan(long.Parse(timeStamp + "0000000")));
  }

  public static int ConvertDateTimeInt(DateTime time)
  {
    DateTime localTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
    return (int) (time - localTime).TotalSeconds;
  }

  public void Stop()
  {
    this.Clear();
  }

  private void GetActivtyNotifcationData()
  {
    if (Application.isEditor)
      return;
    using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
    {
      using (AndroidJavaObject androidJavaObject = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
      {
        string data = androidJavaObject.Call<string>("getNotificationData");
        if (string.IsNullOrEmpty(data))
        {
          this.ReviceGCM(androidJavaObject.Call<string>("getGCMNotificationData"));
          androidJavaObject.Call("clearNotificationData");
        }
        else
        {
          this.OnReviceNotificationData(data);
          androidJavaObject.Call("clearNotificationData");
        }
      }
    }
  }

  public void ReviceGCM(string json)
  {
    if (string.IsNullOrEmpty(json))
      return;
    Hashtable hashtable = Utils.Json2Object(json, true) as Hashtable;
    if (hashtable == null || !hashtable.ContainsKey((object) "acme") || hashtable[(object) "acme"] == null)
      return;
    string str1 = hashtable[(object) "acme"].ToString();
    string str2 = string.Empty;
    string key = str1;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (NotificationManager.\u003C\u003Ef__switch\u0024map75 == null)
      {
        // ISSUE: reference to a compiler-generated field
        NotificationManager.\u003C\u003Ef__switch\u0024map75 = new Dictionary<string, int>(3)
        {
          {
            "1",
            0
          },
          {
            "2",
            1
          },
          {
            "3",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (NotificationManager.\u003C\u003Ef__switch\u0024map75.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            str2 = "valut";
            break;
          case 1:
            str2 = "reception";
            break;
          case 2:
            str2 = "black_market";
            break;
        }
      }
    }
    if (string.IsNullOrEmpty(str2))
      return;
    this.SetGotoParam("{\"classname\":\"LPFocusConstantBuilding\",\"content\":\"{\\\"buildingType\\\":\\\"" + str2 + "\\\"}\"}");
  }

  public void SendReturnBackNotification()
  {
    int num1 = 30;
    int num2 = 172800;
    string str = ScriptLocalization.Get("notification_call_back", true);
    this.Send(new NotificationData()
    {
      Message = str,
      RemainTime = num2
    });
    int num3 = 86400;
    for (int index = 0; index < num1; ++index)
      this.Send(new NotificationData()
      {
        Message = str,
        RemainTime = num3 * (index + 1) * 5
      });
  }

  private void CreateBuildFinish()
  {
    if (!this.CheckIDCanSend("build_finish"))
      return;
    Dictionary<long, bool> dictionary = new Dictionary<long, bool>();
    Dictionary<long, JobHandle> jobDictByClass = JobManager.Instance.GetJobDictByClass(JobEvent.JOB_BUILDING_CONSTRUCTION);
    if (jobDictByClass == null)
      return;
    Dictionary<long, JobHandle>.ValueCollection.Enumerator enumerator = jobDictByClass.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      JobHandle current = enumerator.Current;
      if (current != null)
      {
        Hashtable data1 = current.Data as Hashtable;
        if (data1 != null)
        {
          string empty = string.Empty;
          if (data1.ContainsKey((object) "building_id") && data1[(object) "building_id"] != null)
            empty = data1[(object) "building_id"].ToString();
          if (!string.IsNullOrEmpty(empty))
          {
            long result = 0;
            if (long.TryParse(empty, out result))
            {
              CityManager.BuildingItem buildingFromId = CityManager.inst.GetBuildingFromID(result);
              if (buildingFromId != null)
              {
                BuildingInfo data2 = ConfigManager.inst.DB_Building.GetData(buildingFromId.mType, buildingFromId.mLevel);
                if (buildingFromId != null)
                {
                  string str = ScriptLocalization.Get(data2.Building_LOC_ID, false);
                  NotificationData data3 = NotificationFactory.Instance.CreateData("build_finish", current.LeftTime(), str);
                  data3.Data = this.GetNotificationExtraData(data3.ID, "LPFocusSpecificBuilding", "id", result.ToString(), "buildingType", buildingFromId.mType);
                  this.Send(data3);
                }
              }
            }
          }
        }
      }
    }
  }

  private void CreateTimeChest()
  {
    if (!this.CheckIDCanSend("timer_chest"))
      return;
    NotificationData data = NotificationFactory.Instance.CreateData("timer_chest", DBManager.inst.DB_Local_TimerChest.chestUpdateTime - NetServerTime.inst.ServerTimestamp);
    data.Data = this.GetNotificationExtraData(data.ID, "LPFocusConstantBuilding", "buildingType", "timer_chest");
    this.Send(data);
  }

  private void CreateProtectedTime()
  {
    if (!this.CheckIDCanSend("protection_time_up"))
      return;
    JobHandle singleJobByClass = JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_PEACE_SHIELD);
    if (singleJobByClass == null)
      return;
    this.Send(NotificationFactory.Instance.CreateData("protection_time_up", singleJobByClass.LeftTime()));
  }

  private void CreateResearchFinish()
  {
    if (!this.CheckIDCanSend("research_finish") || ResearchManager.inst.CurrentResearch == null || !ResearchManager.inst.CurrentResearch.IsInProgress)
      return;
    JobHandle job = JobManager.Instance.GetJob(ResearchManager.inst.GetJobId(ResearchManager.inst.CurrentResearch));
    if (job == null)
      return;
    NotificationData data = NotificationFactory.Instance.CreateData("research_finish", job.LeftTime(), ResearchManager.inst.CurrentResearch.Name);
    CityManager.BuildingItem buildingFromType = CityManager.inst.GetBuildingFromType("university");
    if (buildingFromType == null)
    {
      D.error((object) "Can not find build from type =university");
    }
    else
    {
      data.Data = this.GetNotificationExtraData(data.ID, "LPFocusSpecificBuilding", "buildingType", "university", "id", buildingFromType.mID.ToString());
      this.Send(data);
    }
  }

  private void CreateForgeFinish()
  {
    if (!this.CheckIDCanSend("equip_produced"))
      return;
    JobHandle singleJobByClass = JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_FORGE);
    if (singleJobByClass == null)
      return;
    Hashtable data1 = singleJobByClass.Data as Hashtable;
    if (data1 == null || !data1.ContainsKey((object) "item_property_id") || data1[(object) "item_property_id"] == null)
      return;
    ConfigEquipmentMainInfo data2 = ConfigManager.inst.GetEquipmentMain(BagType.Hero).GetData(int.Parse(data1[(object) "item_property_id"].ToString()));
    NotificationData data3 = NotificationFactory.Instance.CreateData("equip_produced", singleJobByClass.LeftTime(), data2.LocName);
    CityManager.BuildingItem buildingFromType = CityManager.inst.GetBuildingFromType("forge");
    if (buildingFromType == null)
    {
      D.error((object) "Can not find build from type =forge");
    }
    else
    {
      data3.Data = this.GetNotificationExtraData(data3.ID, "LPFocusSpecificBuilding", "id", buildingFromType.mID.ToString(), "buildingType", "forge");
      this.Send(data3);
    }
  }

  private void LoadNotifications()
  {
    if (this.flag)
      return;
    this.flag = true;
    this.CreateBuildFinish();
    this.CreateTimeChest();
    this.CreateProtectedTime();
    this.CheckResFull();
    this.CreateResearchFinish();
    this.CreateForgeFinish();
    this.CreateTroopNotification();
    this.CreateDragonNotification();
    this.CreatePitNotifycation();
  }

  private void CheckResFull()
  {
    if (!this.CheckIDCanSend("resource_building_full"))
      return;
    List<CityManager.BuildingItem> buildingsByType = CityManager.inst.GetBuildingsByType("farm");
    List<ResouceGenSpeedData> resouceGenSpeedDataList = new List<ResouceGenSpeedData>();
    CityManager.BuildingItem buildingItem = (CityManager.BuildingItem) null;
    float num = float.MaxValue;
    for (int index = 0; index < buildingsByType.Count; ++index)
    {
      ResouceGenSpeedData resouceGenSpeedData = new ResouceGenSpeedData();
      resouceGenSpeedData.SetData(buildingsByType[index]);
      float fullLeftTime = resouceGenSpeedData.GetFullLeftTime();
      if ((double) fullLeftTime < (double) num)
      {
        buildingItem = buildingsByType[index];
        num = fullLeftTime;
      }
    }
    if (buildingItem == null)
      return;
    NotificationData data = NotificationFactory.Instance.CreateData("resource_building_full", (int) num);
    data.Data = this.GetNotificationExtraData(data.ID, "LPFocusSpecificBuilding", "id", buildingItem.mID.ToString(), "buildingType", "farm");
    this.Send(data);
  }

  private string GetNotificationExtraData(string id, string className, params string[] extraData)
  {
    string str = "{\"notification_id\":" + "\"" + id + "\",\"data\":{";
    new Hashtable()
    {
      {
        (object) "notification_id",
        (object) id
      }
    };
    if (extraData.Length > 0 && !string.IsNullOrEmpty(extraData[0]))
    {
      int num = 0;
      str = str + "\"classname\":" + "\"" + className + "\",";
      int index = 0;
      while (index < extraData.Length && !string.IsNullOrEmpty(extraData[index]))
      {
        str = str + "\"" + extraData[index] + "\":" + "\"" + extraData[index + 1] + "\"";
        num += 2;
        if (num < extraData.Length)
          str += ",";
        index += 2;
      }
    }
    return str + "}}";
  }

  private void CreateTroopNotification()
  {
    if (this.CheckIDCanSend("train_finish"))
    {
      Dictionary<long, JobHandle> jobDictByClass = JobManager.Instance.GetJobDictByClass(JobEvent.JOB_TRAIN_TROOP);
      if (jobDictByClass != null)
      {
        Dictionary<long, JobHandle>.ValueCollection.Enumerator enumerator = jobDictByClass.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          JobHandle current = enumerator.Current;
          if (current != null)
          {
            Hashtable data1 = current.Data as Hashtable;
            if (data1 != null)
            {
              string empty = string.Empty;
              if (data1.ContainsKey((object) "troop_class"))
                empty = data1[(object) "troop_class"].ToString();
              if (!string.IsNullOrEmpty(empty))
              {
                Unit_StatisticsInfo data2 = ConfigManager.inst.DB_Unit_Statistics.GetData(empty);
                if (data2 != null && data2.Type != "trap")
                {
                  string str = ScriptLocalization.Get(string.Format("{0}{1}", (object) data2.ID, (object) "_name"), false);
                  NotificationData data3 = NotificationFactory.Instance.CreateData("train_finish", current.LeftTime(), str);
                  CityManager.BuildingItem buildingFromType = CityManager.inst.GetBuildingFromType(data2.FromBuildingType);
                  if (buildingFromType == null)
                  {
                    D.error((object) ("Can not find build from type =" + data2.FromBuildingType));
                    return;
                  }
                  data3.Data = this.GetNotificationExtraData(data3.ID, "LPFocusSpecificBuilding", "id", buildingFromType.mID.ToString(), "buildingType", data2.FromBuildingType);
                  this.Send(data3);
                }
              }
            }
          }
        }
      }
    }
    if (!this.CheckIDCanSend("train_trap_finish"))
      return;
    Dictionary<long, JobHandle> jobDictByClass1 = JobManager.Instance.GetJobDictByClass(JobEvent.JOB_TRAIN_TROOP);
    if (jobDictByClass1 == null)
      return;
    Dictionary<long, JobHandle>.ValueCollection.Enumerator enumerator1 = jobDictByClass1.Values.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      JobHandle current = enumerator1.Current;
      if (current != null)
      {
        Hashtable data1 = current.Data as Hashtable;
        if (data1 != null)
        {
          string empty1 = string.Empty;
          string empty2 = string.Empty;
          if (data1.ContainsKey((object) "troop_class"))
            empty1 = data1[(object) "troop_class"].ToString();
          if (!string.IsNullOrEmpty(empty1))
          {
            Unit_StatisticsInfo data2 = ConfigManager.inst.DB_Unit_Statistics.GetData(empty1);
            if (data2 != null && data2.Type == "trap")
            {
              string str = ScriptLocalization.Get(string.Format("{0}{1}", (object) data2.ID, (object) "_name"), false);
              NotificationData data3 = NotificationFactory.Instance.CreateData("train_trap_finish", current.LeftTime(), str);
              CityManager.BuildingItem buildingFromType = CityManager.inst.GetBuildingFromType(data2.FromBuildingType);
              if (buildingFromType == null)
              {
                D.error((object) ("Can not find build from type =" + data2.FromBuildingType));
                break;
              }
              data3.Data = this.GetNotificationExtraData(data3.ID, "LPFocusSpecificBuilding", "id", buildingFromType.mID.ToString(), "buildingType", data2.FromBuildingType);
              this.Send(data3);
            }
          }
        }
      }
    }
  }

  private bool CanGoto
  {
    get
    {
      if (string.IsNullOrEmpty(this._gotoParam) || !GameEngine.IsReady() || !UIManager.inst.IsDialogClear)
        return false;
      return UIManager.inst.IsPopupClear;
    }
  }

  public void SetGotoParam(string content)
  {
    this._gotoParam = content;
    if (this.CanGoto)
      this.GotoFromNotificationData();
    else if (GameEngine.IsReady())
      UIManager.inst.OnWindowClear += new System.Action(this.OnWindowClear);
    else
      this.needToGoto = true;
  }

  public void OnReviceNotificationData(string data)
  {
    if (string.IsNullOrEmpty(data))
      return;
    Hashtable hashtable1 = (Hashtable) null;
    try
    {
      hashtable1 = Utils.Json2Object(data, true) as Hashtable;
    }
    catch (Exception ex)
    {
      D.error((object) ("ReviceNotificationData is Illegal Json , data is " + data));
    }
    if (hashtable1 == null || !hashtable1.ContainsKey((object) nameof (data)) || hashtable1[(object) nameof (data)] == null)
      return;
    Hashtable hashtable2 = hashtable1[(object) nameof (data)] as Hashtable;
    if (hashtable2.ContainsKey((object) "buildingType") && hashtable2[(object) "buildingType"] != null)
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      dictionary.Add("classname", hashtable2[(object) "classname"].ToString());
      Hashtable hashtable3 = new Hashtable();
      hashtable3.Add((object) "buildingType", (object) hashtable2[(object) "buildingType"].ToString());
      if (hashtable2.ContainsKey((object) "id") && hashtable2[(object) "id"] != null)
        hashtable3.Add((object) "id", (object) hashtable2[(object) "id"].ToString());
      dictionary.Add("content", Utils.Object2Json((object) hashtable3));
      this._gotoParam = Utils.Object2Json((object) dictionary);
    }
    if (this.CanGoto)
      this.GotoFromNotificationData();
    else if (GameEngine.IsReady())
      UIManager.inst.OnWindowClear += new System.Action(this.OnWindowClear);
    else
      this.needToGoto = true;
  }

  public void CreatePitNotifycation()
  {
    if (this.CreatePitNotifycation(ActivityManager.Intance.pitData))
      return;
    this.CreatePitNotifycation(PitExplorePayload.Instance.ActivityData);
  }

  private bool CreatePitNotifycation(PitActivityData pitActivityData)
  {
    if (pitActivityData != null && pitActivityData.CurSignupData != null)
    {
      int num = 300;
      int finishTime = pitActivityData.CurSignupData.StartTime - NetServerTime.inst.ServerTimestamp - num;
      if (finishTime > 0)
      {
        this.Send(NotificationFactory.Instance.CreateData("abyss_start", finishTime));
        return true;
      }
    }
    return false;
  }

  public void CreateDragonNotification()
  {
    if (PlayerData.inst.dragonData != null)
      return;
    long mTrainingJobId = CityManager.inst.GetBuildingFromType("dragon_lair").mTrainingJobID;
    if (mTrainingJobId <= 0L)
      return;
    JobHandle unfinishedJob = JobManager.Instance.GetUnfinishedJob(mTrainingJobId);
    if (unfinishedJob == null)
      return;
    int num = unfinishedJob.LeftTime();
    string str = ScriptLocalization.Get("notification_dragon_finish", false);
    NotificationData data = new NotificationData();
    data.RemainTime = num;
    data.Message = str;
    CityManager.BuildingItem buildingFromType = CityManager.inst.GetBuildingFromType("dragon_lair");
    if (buildingFromType == null)
    {
      D.error((object) "Can not find build from type =dragon_lair");
    }
    else
    {
      data.Data = this.GetNotificationExtraData("dragon_ready", "LPFocusSpecificBuilding", "id", buildingFromType.mID.ToString(), "buildingType", "dragon_lair");
      this.Send(data);
    }
  }

  private void Clear()
  {
    if (this.nativeObj != null)
    {
      this.nativeObj.Call("clearAllNotificationAlarm", new object[1]
      {
        (object) this.ANDRIOD_REQUEST_CODE
      });
      this.nativeObj.Call("clearAllNotificationAlarm");
      this.ANDRIOD_REQUEST_CODE = 0;
    }
    this.localCaches.Clear();
    this.sendCount.Clear();
    this.sendTime.Clear();
    this.flag = false;
  }
}
