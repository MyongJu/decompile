﻿// Decompiled with JetBrains decompiler
// Type: WorldSystem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class WorldSystem : MonoBehaviour
{
  public WorldMap WMap;
  private PublicHUD _hud;
  private static WorldSystem _singleton;
  private Vector3 cachePosition;
  private Rect cacheBound;

  public static WorldSystem Instance
  {
    get
    {
      return WorldSystem._singleton;
    }
  }

  public static WorldSystem Create()
  {
    if ((UnityEngine.Object) WorldSystem._singleton == (UnityEngine.Object) null)
      WorldSystem._singleton = NGUITools.AddChild(!((UnityEngine.Object) UIManager.inst != (UnityEngine.Object) null) ? GameObject.Find("/UIManager/UI Root") : UIManager.inst.uiRoot.gameObject, AssetManager.Instance.HandyLoad("Prefab/World/WorldSystem", (System.Type) null) as GameObject).GetComponent<WorldSystem>();
    return WorldSystem._singleton;
  }

  public void EnterWorldSystem()
  {
    this.WMap.Init();
    WorldMapData.Instance.Initialize();
    this._hud = UIManager.inst.publicHUD;
    if ((UnityEngine.Object) this._hud != (UnityEngine.Object) null)
      this._hud.SwitchToPart(GAME_LOCATION.WORLD);
    if (!((UnityEngine.Object) UIManager.inst.tileCamera != (UnityEngine.Object) null))
      return;
    UIManager.inst.tileCamera.gameObject.SetActive(true);
    this.cachePosition = UIManager.inst.tileCamera.transform.position;
    this.cacheBound = UIManager.inst.tileCamera.Boundary;
    UIManager.inst.tileCamera.SetTargetLookPosition(Vector3.zero, false);
  }

  public void ExitWorldSystem()
  {
    UIManager.inst.tileCamera.transform.position = this.cachePosition;
    UIManager.inst.tileCamera.Boundary = this.cacheBound;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.WMap.gameObject);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    WorldSystem._singleton = (WorldSystem) null;
  }
}
