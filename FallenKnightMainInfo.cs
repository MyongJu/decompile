﻿// Decompiled with JetBrains decompiler
// Type: FallenKnightMainInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class FallenKnightMainInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "type")]
  public int Type;
  [Config(Name = "name")]
  public string Name;
  [Config(Name = "image")]
  public string Image;
  [Config(Name = "level")]
  public int Level;
  [Config(Name = "invade_time")]
  public int InvadeTime;
  [Config(Name = "troop_count")]
  public int TroopCount;
  [Config(Name = "lord_count")]
  public int LordCount;
  [Config(Name = "score")]
  public long Score;
  [Config(Name = "speed_rate")]
  public float SpeedRate;
  [Config(Name = "win_kill_rate")]
  public float WinKillRate;
  [Config(CustomParse = true, CustomType = typeof (UnitGroup), Name = "Units")]
  public UnitGroup Units;
  [Config(Name = "max_round")]
  public int MaxRound;
  [Config(Name = "npc_id")]
  public string NpcId;
}
