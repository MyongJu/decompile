﻿// Decompiled with JetBrains decompiler
// Type: ChatScrollView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Chat;
using System.Collections.Generic;
using UnityEngine;

public class ChatScrollView : MonoBehaviour
{
  [HideInInspector]
  private Queue<ChatBar> objPool = new Queue<ChatBar>();
  [HideInInspector]
  private LinkedList<ChatScrollView.Node> barList = new LinkedList<ChatScrollView.Node>();
  public float displayRange = 10f;
  public UIPanel panel;
  public UIScrollView scrollView;
  private ChatChannel _channel;
  public ChatBar orgObj;
  public UILabel calcLabel;
  public System.Action<ChatMessage> onPortaitClick;
  public Transform barRoot;

  public ChatChannel channel
  {
    get
    {
      return this._channel;
    }
    set
    {
      if (value == null || value != this._channel)
      {
        this.ClearBars();
        this._channel = value;
      }
      if (this._channel == null)
        return;
      this.SetMessageBottom(-1);
    }
  }

  private float displayTop
  {
    get
    {
      return 500f - this.transform.localPosition.y;
    }
  }

  private float displayBottom
  {
    get
    {
      return -1000f - this.transform.localPosition.y;
    }
  }

  public bool atBottom
  {
    get
    {
      if (this._channel != null && this.barList.Count > 0)
        return this._channel.messages.Count - this.barList.Last.Value.messageIndex < 3;
      return true;
    }
  }

  public void Awake()
  {
    this.scrollView.GetComponent<UIPanel>().onClipMove = new UIPanel.OnClippingMoved(this.OnMove);
    MessageHub.inst.GetPortByAction("Player:mod").AddEvent(new System.Action<object>(this.OnMod));
  }

  public void Release()
  {
    LinkedList<ChatScrollView.Node>.Enumerator enumerator = this.barList.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.chatBar.Release();
  }

  public void OnPortaitClick(ChatMessage message)
  {
    if (this.onPortaitClick == null)
      return;
    this.onPortaitClick(message);
  }

  public void Update()
  {
  }

  public void OnTranslateEnd(int index)
  {
    this.RefreshMessageAt(index);
  }

  public void OnFrame()
  {
    if (this.barList.Count > 0)
    {
      ChatScrollView.Node node1 = this.barList.First.Value;
      ChatScrollView.Node node2 = this.barList.Last.Value;
      while ((double) node1.top < (double) this.displayTop + 10.0 && node1.messageIndex > 0)
      {
        ChatScrollView.Node node3 = this.SetNode(node1.messageIndex - 1);
        if (node3 != null)
        {
          node3.chatBar.transform.localPosition = new Vector3(0.0f, (float) ((double) node1.chatBar.transform.localPosition.y + (double) node3.height + 20.0), 0.0f);
          this.barList.AddFirst(node3);
          node1 = this.barList.First.Value;
          node2 = this.barList.Last.Value;
        }
      }
      while (this.barList.Count > 0 && (double) node1.bottom > (double) this.displayTop + 30.0)
      {
        this.barList.RemoveFirst();
        this.Enqueue(node1.chatBar);
        if (this.barList.Count > 0)
        {
          node1 = this.barList.First.Value;
          node2 = this.barList.Last.Value;
        }
      }
      int num1 = 0;
      ChatScrollView.Node node4;
      while ((double) node2.bottom > (double) this.displayBottom - 10.0 && num1 < this.channel.messages.Count - 1)
      {
        int a = num1;
        int num2 = a + 1;
        num1 = Mathf.Max(a, node2.messageIndex + 1);
        ChatScrollView.Node node3 = this.SetNode(node2.messageIndex + 1);
        if (node3 != null)
        {
          node3.chatBar.transform.localPosition = new Vector3(0.0f, (float) ((double) node2.chatBar.transform.localPosition.y - (double) node2.height - 20.0), 0.0f);
          this.barList.AddLast(node3);
          node4 = this.barList.First.Value;
          node2 = this.barList.Last.Value;
        }
      }
      while (this.barList.Count > 0 && (double) node2.top < (double) this.displayBottom - 30.0)
      {
        this.barList.RemoveLast();
        this.Enqueue(node2.chatBar);
        if (this.barList.Count > 0)
        {
          node4 = this.barList.First.Value;
          node2 = this.barList.Last.Value;
        }
      }
    }
    if (this.barList.Count > 0 || this.scrollView.isDragging || this.channel.messages.Count <= 0)
      return;
    this.SetMessageBottom(this.channel.messages.Count - 1);
  }

  public void RefreshMessageAt(int index = -1)
  {
    if (index < 0)
    {
      this.SetMessageBottom(-1);
    }
    else
    {
      Vector3 vector3 = Vector3.zero;
      LinkedList<ChatScrollView.Node>.Enumerator enumerator = this.barList.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.messageIndex == index)
          vector3 = enumerator.Current.chatBar.transform.localPosition;
      }
      this.ClearBars();
      ChatScrollView.Node node = this.SetNode(index);
      if (node != null)
      {
        node.chatBar.transform.localPosition = vector3;
        this.barList.AddFirst(node);
      }
    }
    this.OnFrame();
  }

  public void SetMessageBottom(int index = -1)
  {
    if (this.channel == null)
      return;
    if (index < 0)
      index = this.channel.messages.Count - 1;
    this.ClearBars();
    for (int messageIndex = index; messageIndex >= 0 && index - messageIndex < 5; --messageIndex)
    {
      ChatScrollView.Node node = this.SetNode(messageIndex);
      if (node != null)
      {
        if (this.barList.Count <= 0)
          node.chatBar.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        else
          node.chatBar.transform.localPosition = new Vector3(0.0f, (float) ((double) this.barList.First.Value.chatBar.transform.localPosition.y + (double) node.height + 20.0), 0.0f);
        this.barList.AddFirst(node);
      }
    }
    this.scrollView.ResetPosition();
    this.scrollView.SetDragAmount(0.5f, 1f, false);
    this.scrollView.panel.Update();
  }

  private ChatScrollView.Node SetNode(int messageIndex)
  {
    if (messageIndex < 0 || messageIndex >= this.channel.messages.Count)
      return (ChatScrollView.Node) null;
    ChatMessage message = this.channel.messages[messageIndex];
    return new ChatScrollView.Node()
    {
      chatBar = this.SetChatBar(message),
      height = this.GetHeight(message),
      messageIndex = messageIndex
    };
  }

  private void ClearBars()
  {
    LinkedList<ChatScrollView.Node>.Enumerator enumerator = this.barList.GetEnumerator();
    while (enumerator.MoveNext())
      this.Enqueue(enumerator.Current.chatBar);
    this.barList.Clear();
  }

  private float GetHeight(ChatMessage message)
  {
    if (message.traceData.isSpeaker)
      this.calcLabel.text = message.text;
    else if (message.traceData.type == ChatTraceData.TraceType.Empty)
      this.calcLabel.text = message.text;
    else if (message.traceData.type == ChatTraceData.TraceType.NoDefined)
    {
      this.calcLabel.text = Utils.XLAT("old_version_cannot_do_this_description");
    }
    else
    {
      switch (message.traceData.type)
      {
        case ChatTraceData.TraceType.WarReport_Attack:
        case ChatTraceData.TraceType.WarReport_BeAttack:
        case ChatTraceData.TraceType.WarReport_Rally:
        case ChatTraceData.TraceType.WarReport_BeRally:
        case ChatTraceData.TraceType.WarReport_Scout:
        case ChatTraceData.TraceType.Forge:
        case ChatTraceData.TraceType.CasinoCard:
        case ChatTraceData.TraceType.Rally:
        case ChatTraceData.TraceType.Bookmark:
        case ChatTraceData.TraceType.EquimentEnhance:
        case ChatTraceData.TraceType.MagicReport_Attack:
        case ChatTraceData.TraceType.MagicReport_BeAttack:
        case ChatTraceData.TraceType.ForgeDK:
        case ChatTraceData.TraceType.EquimentEnhanceDK:
          this.calcLabel.text = string.Format("{0}\n", (object) message.text);
          break;
        default:
          this.calcLabel.text = string.Format("{0}", (object) message.text);
          break;
      }
    }
    return (float) this.calcLabel.height + 148f;
  }

  private ChatBar SetChatBar(ChatMessage message)
  {
    ChatBar chatBar = this.Dequeue();
    chatBar.gameObject.SetActive(true);
    chatBar.Reset();
    chatBar.SetInfo(message);
    chatBar.onTranslateEnd = new System.Action<int>(this.OnTranslateEnd);
    chatBar.onPortaitClick = new System.Action<ChatMessage>(this.OnPortaitClick);
    return chatBar;
  }

  private ChatBar Dequeue()
  {
    if (this.objPool.Count > 0)
      return this.objPool.Dequeue();
    return NGUITools.AddChild(this.barRoot.gameObject, this.orgObj.gameObject).GetComponent<ChatBar>();
  }

  private void Enqueue(ChatBar bar)
  {
    bar.gameObject.SetActive(false);
    this.objPool.Enqueue(bar);
  }

  private void OnMove(UIPanel panel)
  {
    this.OnFrame();
  }

  private void OnMod(object orgSrc)
  {
    this.channel.FilterMessage();
    this.SetMessageBottom(-1);
  }

  public class Node
  {
    public const float HEIGHT_TOP = 46f;
    public const float HEIGHT_ORG = 148f;
    public const float HEIGHT_GAP = 20f;
    private int _messageIndex;
    public float height;
    public ChatBar chatBar;

    public int messageIndex
    {
      set
      {
        this._messageIndex = value;
        this.chatBar.index = this._messageIndex;
      }
      get
      {
        return this._messageIndex;
      }
    }

    public float top
    {
      get
      {
        return this.chatBar.transform.localPosition.y + 46f;
      }
    }

    public float bottom
    {
      get
      {
        return (float) ((double) this.chatBar.transform.localPosition.y - (double) this.height + 46.0);
      }
    }
  }
}
