﻿// Decompiled with JetBrains decompiler
// Type: VIPLevelUIEntry
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class VIPLevelUIEntry : MonoBehaviour
{
  public UISprite lockSprite;
  public UISprite highlightBackground;
  public UILabel levelLabel;
  public UILabel bigLevelLabel;
  public GameObject largeTextContainer;
  public GameObject smallTextContainer;
  public VIPDlg parentDlg;
  private int vipLevel;

  public void SetHighlighted(bool selected)
  {
    this.highlightBackground.gameObject.SetActive(selected);
  }

  public void SetActiveState(bool active)
  {
    this.lockSprite.gameObject.SetActive(!active);
    this.largeTextContainer.SetActive(active);
    this.smallTextContainer.SetActive(!active);
  }

  public void SetLevel(int level)
  {
    this.vipLevel = level;
    string str1 = level.ToString();
    UILabel levelLabel = this.levelLabel;
    string str2 = str1;
    this.bigLevelLabel.text = str2;
    string str3 = str2;
    levelLabel.text = str3;
  }

  public void OnClick()
  {
    this.parentDlg.OnClickLevelEntry(this.vipLevel);
  }
}
