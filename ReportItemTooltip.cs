﻿// Decompiled with JetBrains decompiler
// Type: ReportItemTooltip
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class ReportItemTooltip : MonoBehaviour
{
  private const string defaultItem = "helmet";
  public UISprite icon;
  public UILabel itemLevel;
  public UILabel description;
  public UILabel nameBasic;
  public UILabel nameCommon;
  public UILabel nameUncommon;
  public UILabel nameRare;
  public UILabel nameEpic;
  public UILabel nameLegendary;
  public UISprite leftSideArrow;
  public UISprite rightSideArrow;
  private bool _initialized;

  public void Init()
  {
    if (this._initialized)
      return;
    this._initialized = true;
  }

  public void Show(Hero_EquipmentData item, UISprite resItem)
  {
    this.ClearData();
    this.icon.spriteName = string.IsNullOrEmpty(item.Asset_Filename) ? "helmet" : item.Asset_Filename;
    this.itemLevel.text = 1.ToString();
    this.description.text = ScriptLocalization.Get(item.LOC_Description_ID, true);
    Transform parent = this.gameObject.transform.parent;
    int width = this.leftSideArrow.width;
    Vector3 position = resItem.transform.position;
    this.gameObject.transform.parent = resItem.gameObject.transform;
    if ((double) position.x < (double) (Screen.width / 2))
    {
      this.rightSideArrow.gameObject.SetActive(false);
      this.leftSideArrow.gameObject.SetActive(true);
      this.gameObject.transform.localPosition = new Vector3((float) (resItem.width / 2 + width / 2), this.gameObject.transform.localPosition.y, this.gameObject.transform.localPosition.z);
    }
    else
    {
      this.rightSideArrow.gameObject.SetActive(true);
      this.leftSideArrow.gameObject.SetActive(false);
      this.gameObject.transform.localPosition = new Vector3((float) -(width / 2), this.gameObject.transform.localPosition.y, this.gameObject.transform.localPosition.z);
    }
    this.gameObject.transform.parent = parent;
    this.gameObject.SetActive(true);
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  private void ClearData()
  {
  }
}
