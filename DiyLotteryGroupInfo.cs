﻿// Decompiled with JetBrains decompiler
// Type: DiyLotteryGroupInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DiyLotteryGroupInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "spend_item")]
  public string spendItem;
  [Config(Name = "rank_1_max")]
  public int rank1Max;
  [Config(Name = "rank_2_max")]
  public int rank2Max;
  [Config(Name = "rank_3_max")]
  public int rank3Max;
  [Config(Name = "spend_number_1")]
  public int spendNumber1;
  [Config(Name = "spend_number_2")]
  public int spendNumber2;
  [Config(Name = "spend_number_3")]
  public int spendNumber3;
  [Config(Name = "spend_number_4")]
  public int spendNumber4;
  [Config(Name = "spend_number_5")]
  public int spendNumber5;
  [Config(Name = "spend_number_6")]
  public int spendNumber6;
  [Config(Name = "spend_number_7")]
  public int spendNumber7;
  [Config(Name = "spend_number_8")]
  public int spendNumber8;
  [Config(Name = "spend_number_9")]
  public int spendNumber9;
  [Config(Name = "get_item_gold")]
  public long getItemGold;
  [Config(Name = "get_item_daily_score")]
  public long getItemDailyScore;
}
