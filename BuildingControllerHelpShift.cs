﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerHelpShift
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BuildingControllerHelpShift : BuildingControllerNew
{
  private const string path = "Prefab/City/InfoBoard";
  public Transform boardRoot;
  public GameObject isHadRevice;
  private InfoBoard infoTitle;

  public override void InitBuilding()
  {
    base.InitBuilding();
    if (CustomDefine.IsSQWanPackage())
      return;
    NGUITools.SetActive(this.isHadRevice, AccountManager.Instance.HelpShift);
    this.onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    if (!((UnityEngine.Object) null == (UnityEngine.Object) this.infoTitle))
      return;
    AssetManager.Instance.LoadAsync("Prefab/City/InfoBoard", (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      this.infoTitle = Utils.DuplicateGOB(obj as GameObject, this.boardRoot).GetComponent<InfoBoard>();
      this.infoTitle.Init("help_board_scrolling_title");
      AssetManager.Instance.UnLoadAsset("Prefab/City/InfoBoard", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    }), (System.Type) null);
  }

  public override void Dispose()
  {
    base.Dispose();
    this.onBuildingSelected -= new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.infoTitle))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.infoTitle.gameObject);
    this.infoTitle = (InfoBoard) null;
  }

  public void OnClickHandler()
  {
    if (CustomDefine.IsSQWanPackage())
      return;
    AccountManager.Instance.HelpShift = false;
    if (Application.isEditor)
      return;
    AccountManager.Instance.ShowFASQs();
  }

  private void Update()
  {
    if (CustomDefine.IsSQWanPackage())
      return;
    NGUITools.SetActive(this.isHadRevice, AccountManager.Instance.HelpShift);
    if (!((UnityEngine.Object) this.infoTitle != (UnityEngine.Object) null))
      return;
    if (AccountManager.Instance.HelpShift)
      this.infoTitle.Stop();
    else
      this.infoTitle.Resume();
  }

  private void OnBuildingSelected()
  {
    this.OnClickHandler();
  }
}
