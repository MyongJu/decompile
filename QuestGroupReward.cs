﻿// Decompiled with JetBrains decompiler
// Type: QuestGroupReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class QuestGroupReward : QuestReward
{
  public new static List<QuestReward> CreateReward(int questId)
  {
    QuestGroupInfo data = ConfigManager.inst.DB_QuestGroup.GetData(ConfigManager.inst.DB_Quest.GetData(questId).groupId);
    List<QuestReward> questRewardList = new List<QuestReward>();
    List<Reward.RewardsValuePair> rewards = data.Rewards.GetRewards();
    for (int index = 0; index < rewards.Count; ++index)
    {
      if (ConfigManager.inst.DB_Items.GetItem(rewards[index].internalID) != null)
      {
        QuestReward questReward = (QuestReward) new ConsumableReward(rewards[index].internalID, rewards[index].value);
        questRewardList.Add(questReward);
      }
    }
    return questRewardList;
  }
}
