﻿// Decompiled with JetBrains decompiler
// Type: MemberInfoItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MemberInfoItem : MonoBehaviour
{
  public Icon icon;

  public void SeedData(MemberDetail d)
  {
    this.icon.SetData("Texture/Hero/Portrait_Icon/player_portrait_icon_" + d.portrait, d.icon, d.lord_title, new string[4]
    {
      d.fullname,
      d.site,
      d.start_troops.ToString(),
      d.kill_troops.ToString()
    });
  }
}
