﻿// Decompiled with JetBrains decompiler
// Type: IAPStoreContentRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class IAPStoreContentRenderer : MonoBehaviour
{
  public UITexture m_ItemIcon;
  public UILabel m_ItemName;
  public UILabel m_ItemCount;
  public UITexture border;
  private int itemId;

  public void SetData(ItemStaticInfo itemStaticInfo, int itemCount)
  {
    this.itemId = itemStaticInfo.internalId;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    Utils.SetItemBackground(this.border, itemStaticInfo.internalId);
    Utils.SetItemName(this.m_ItemName, itemStaticInfo.internalId);
    this.m_ItemCount.text = itemCount.ToString();
  }

  public void OnClickHandler()
  {
    Utils.ShowItemTip(this.itemId, this.border.transform, 0L, 0L, 0);
  }

  public void OnReleaseHandler()
  {
    Utils.StopShowItemTip();
  }

  public void OnPressHandler()
  {
    Utils.DelayShowTip(this.itemId, this.border.transform, 0L, 0L, 0);
  }
}
