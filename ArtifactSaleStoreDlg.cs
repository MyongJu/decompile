﻿// Decompiled with JetBrains decompiler
// Type: ArtifactSaleStoreDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ArtifactSaleStoreDlg : UI.Dialog
{
  private Dictionary<int, ArtifactNormalSaleItemRenderer> _itemDict = new Dictionary<int, ArtifactNormalSaleItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public UILabel currency;
  public ArtifactSpecialSaleView specialSaleView;
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.AddEventHandler();
    this.UpdateTips();
    this.UpdateUI();
    this.UpdateSpecialSaleUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.ClearData();
    this.specialSaleView.ClearData();
  }

  public void OnBuyGoldBtnPressed()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  private void UpdateTips()
  {
    this.currency.text = Utils.FormatThousands(PlayerData.inst.userData.currency.gold.ToString());
  }

  private void UpdateUI()
  {
    this.ClearData();
    List<ArtifactShopInfo> artifactShopInfoList = ConfigManager.inst.DB_ArtifactShop.GetArtifactShopInfoList();
    if (artifactShopInfoList != null)
    {
      artifactShopInfoList.Sort((Comparison<ArtifactShopInfo>) ((a, b) => a.priority.CompareTo(b.priority)));
      for (int key = 0; key < artifactShopInfoList.Count; ++key)
      {
        ArtifactNormalSaleItemRenderer itemRenderer = this.CreateItemRenderer(artifactShopInfoList[key]);
        this._itemDict.Add(key, itemRenderer);
      }
    }
    this.Reposition();
  }

  private void UpdateSpecialSaleUI()
  {
    this.specialSaleView.UpdateUI();
  }

  private void OnArtifactBuyFinished()
  {
    this.UpdateSpecialSaleUI();
  }

  private void ClearData()
  {
    using (Dictionary<int, ArtifactNormalSaleItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, ArtifactNormalSaleItemRenderer> current = enumerator.Current;
        current.Value.ClearData();
        current.Value.onBuyFinished -= new System.Action(this.OnArtifactBuyFinished);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private ArtifactNormalSaleItemRenderer CreateItemRenderer(ArtifactShopInfo artifactShopInfo)
  {
    GameObject gameObject = this._itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    ArtifactNormalSaleItemRenderer component = gameObject.GetComponent<ArtifactNormalSaleItemRenderer>();
    component.SetData(artifactShopInfo);
    component.onBuyFinished += new System.Action(this.OnArtifactBuyFinished);
    return component;
  }

  private void OnUserDataUpdated(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.UpdateTips();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdated);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdated);
  }
}
