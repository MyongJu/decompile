﻿// Decompiled with JetBrains decompiler
// Type: OverlayToast
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class OverlayToast : MonoBehaviour
{
  public int m_BackgroundWidth = 2730;
  private Queue<OverlayToast.ToastItem> m_Queue = new Queue<OverlayToast.ToastItem>();
  public UISprite m_Background;
  public UILabel m_Content;
  private IEnumerator m_Coroutine;
  private OverlayToast.ToastItem m_Item;

  private void Start()
  {
    this.m_Background.gameObject.SetActive(false);
  }

  public void Show(string content, System.Action onclose = null, float duration = 4f, bool clear = false)
  {
    if (clear)
    {
      this.m_Queue.Clear();
      if (this.m_Coroutine != null)
      {
        this.StopCoroutine(this.m_Coroutine);
        this.m_Coroutine = (IEnumerator) null;
      }
      if (this.m_Item != null && this.m_Item.onclose != null)
        this.m_Item.onclose();
      this.m_Item = (OverlayToast.ToastItem) null;
    }
    this.m_Queue.Enqueue(new OverlayToast.ToastItem()
    {
      content = content,
      duration = duration,
      onclose = onclose
    });
  }

  private void Update()
  {
    if (this.m_Coroutine != null || this.m_Queue.Count <= 0 || GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.LoadingMode)
      return;
    this.m_Item = this.m_Queue.Dequeue();
    this.StartCoroutine(this.m_Coroutine = this.AnimateIn());
  }

  [DebuggerHidden]
  private IEnumerator AnimateIn()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new OverlayToast.\u003CAnimateIn\u003Ec__Iterator8F()
    {
      \u003C\u003Ef__this = this
    };
  }

  private class ToastItem
  {
    public string content;
    public float duration;
    public System.Action onclose;
  }
}
