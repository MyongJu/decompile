﻿// Decompiled with JetBrains decompiler
// Type: ConfigAllianceActivityRequirement
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigAllianceActivityRequirement
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, AllianceActivityRequirementInfo> datas;
  private Dictionary<int, AllianceActivityRequirementInfo> dicByUniqueId;
  private Dictionary<int, List<AllianceActivityRequirementInfo>> dicByActivityMainID;

  public void BuildDB(object res)
  {
    this.parse.Parse<AllianceActivityRequirementInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public AllianceActivityRequirementInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (AllianceActivityRequirementInfo) null;
  }

  public List<AllianceActivityRequirementInfo> GetCurrentRequirementInfo(int id)
  {
    List<AllianceActivityRequirementInfo> activityRequirementInfoList = new List<AllianceActivityRequirementInfo>();
    AllianceActivitySubInfo data = ConfigManager.inst.DB_AllianceActivityMainSub.GetData(id);
    Dictionary<int, AllianceActivityRequirementInfo>.ValueCollection.Enumerator enumerator = this.dicByUniqueId.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.ActivityMainSubInfo_ID == data.internalId)
        activityRequirementInfoList.Add(enumerator.Current);
    }
    activityRequirementInfoList.Sort(new Comparison<AllianceActivityRequirementInfo>(this.CompareItem));
    return activityRequirementInfoList;
  }

  private int CompareItem(AllianceActivityRequirementInfo a, AllianceActivityRequirementInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }

  public AllianceActivityRequirementInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AllianceActivityRequirementInfo) null;
  }

  public List<AllianceActivityRequirementInfo> GetRequireByActivityMainID(int internalID)
  {
    if (this.dicByActivityMainID == null)
    {
      this.dicByActivityMainID = new Dictionary<int, List<AllianceActivityRequirementInfo>>();
      using (Dictionary<int, AllianceActivityRequirementInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, AllianceActivityRequirementInfo> current = enumerator.Current;
          if (!this.dicByActivityMainID.ContainsKey(current.Key))
            this.dicByActivityMainID.Add(current.Key, new List<AllianceActivityRequirementInfo>());
          this.dicByActivityMainID[current.Key].Add(current.Value);
          this.dicByActivityMainID[current.Key].Sort((Comparison<AllianceActivityRequirementInfo>) ((x, y) => x.Priority.CompareTo(y.Priority)));
        }
      }
    }
    return this.dicByActivityMainID[internalID];
  }
}
