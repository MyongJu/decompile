﻿// Decompiled with JetBrains decompiler
// Type: KingdomTitleInfoPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class KingdomTitleInfoPopup : Popup
{
  private Dictionary<int, KingdomTitleBenefitSlotRenderer> itemDict = new Dictionary<int, KingdomTitleBenefitSlotRenderer>();
  private GameObjectPool itemPool = new GameObjectPool();
  private long _nominateUserId = -1;
  public UILabel title;
  public UILabel titleGrantedUser;
  public UILabel titleCoolDownTime;
  public UILabel panelTitle;
  public UITexture titleTexture;
  public UITexture titleBkgTexture;
  public UIButton viewUserInfoButton;
  public UIButton mail2UserButton;
  public UIButton appointTitleButton;
  public UIScrollView scrollView;
  public UIGrid grid;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  private int _kingdomId;
  private WonderTitleInfo _wonderTitleInfo;
  private System.Action _nominateButtonPressed;
  private bool _showKingBenefit;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    KingdomTitleInfoPopup.Parameter parameter = orgParam as KingdomTitleInfoPopup.Parameter;
    if (parameter != null)
    {
      this._kingdomId = parameter.kingdomId;
      this._wonderTitleInfo = parameter.wonderTitleInfo;
      this._nominateUserId = parameter.nominateUserId;
      this._nominateButtonPressed = parameter.nominateButtonPressed;
      this._showKingBenefit = parameter.showKingBenefit;
      this.viewUserInfoButton.isEnabled = parameter.needViewInfo;
    }
    this.itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.AddEventHandler();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
    this.RemoveEventHandler();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnMail2Player()
  {
    if (this._wonderTitleInfo == null || this._wonderTitleInfo != null && this._wonderTitleInfo.NominatedUser == null)
      return;
    UserData userData = DBManager.inst.DB_User.Get(this._wonderTitleInfo.NominatedUser.UserId);
    if (userData != null || !string.IsNullOrEmpty(this._wonderTitleInfo.NominatedUser.UserName))
      UIManager.inst.OpenDlg("Mail/MailComposeDlg", (UI.Dialog.DialogParameter) new MailComposeDlg.Parameter()
      {
        recipents = new List<string>()
        {
          userData == null ? this._wonderTitleInfo.NominatedUser.UserName : userData.userName
        }
      }, 1 != 0, 1 != 0, 1 != 0);
    this.OnCloseBtnPressed();
  }

  public void OnPlayerProfile()
  {
    if (this._wonderTitleInfo != null && this._wonderTitleInfo.NominatedUser.UserId > 0L)
      UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
      {
        uid = this._wonderTitleInfo.NominatedUser.UserId
      }, 1 != 0, 1 != 0, 1 != 0);
    this.OnCloseBtnPressed();
  }

  public void OnNominatePressed()
  {
    if (this._nominateButtonPressed != null)
      this._nominateButtonPressed();
    this.OnCloseBtnPressed();
  }

  private void UpdateUI()
  {
    this.ClearData();
    this.ShowWonderTitleDetail();
    this.ShowTitleBenefitDetail();
  }

  private void ShowWonderTitleDetail()
  {
    if (this._wonderTitleInfo == null)
      return;
    UILabel title = this.title;
    string name = this._wonderTitleInfo.Name;
    this.panelTitle.text = name;
    string str = name;
    title.text = str;
    DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) this._kingdomId);
    if (this._wonderTitleInfo.IsNominated)
    {
      this.titleGrantedUser.text = this._wonderTitleInfo.NominatedUser.UserName;
      string portrait = this._wonderTitleInfo.NominatedUser.Portrait;
      string localPath = "Texture/Hero/Portrait_Icon/player_portrait_icon_" + portrait;
      if ((UnityEngine.Object) this.titleTexture.mainTexture == (UnityEngine.Object) null || (UnityEngine.Object) this.titleTexture.mainTexture != (UnityEngine.Object) null && this.titleTexture.mainTexture.name != "player_portrait_icon_" + portrait && this.titleTexture.mainTexture.name != this._wonderTitleInfo.NominatedUser.UserImage)
      {
        CustomIconLoader.Instance.requestCustomIcon(this.titleTexture, localPath, this._wonderTitleInfo.NominatedUser.UserImage, false);
        LordTitlePayload.Instance.ApplyUserAvator(this.titleTexture, this._wonderTitleInfo.NominatedUser.LordTitleId, 1);
      }
      NGUITools.SetActive(this.viewUserInfoButton.gameObject, true);
      NGUITools.SetActive(this.mail2UserButton.gameObject, true);
      NGUITools.SetActive(this.appointTitleButton.gameObject, false);
    }
    else
    {
      this.titleGrantedUser.text = Utils.XLAT("throne_title_vacant");
      if ((UnityEngine.Object) this.titleTexture.mainTexture == (UnityEngine.Object) null || (UnityEngine.Object) this.titleTexture.mainTexture != (UnityEngine.Object) null && this.titleTexture.mainTexture.name != this._wonderTitleInfo.icon)
        BuilderFactory.Instance.HandyBuild((UIWidget) this.titleTexture, "Texture/Kingdom/" + this._wonderTitleInfo.icon, (System.Action<bool>) null, true, false, string.Empty);
      NGUITools.SetActive(this.viewUserInfoButton.gameObject, false);
      NGUITools.SetActive(this.mail2UserButton.gameObject, false);
      NGUITools.SetActive(this.appointTitleButton.gameObject, this._nominateUserId > 0L);
    }
    if (wonderData != null && wonderData.KING_UID == PlayerData.inst.uid && this._wonderTitleInfo.TitleCoolDownLeftTime >= 0)
    {
      this.titleCoolDownTime.text = Utils.FormatTime(this._wonderTitleInfo.TitleCoolDownLeftTime, true, false, true);
      NGUITools.SetActive(this.titleCoolDownTime.gameObject, true);
    }
    else
      NGUITools.SetActive(this.titleCoolDownTime.gameObject, false);
    if (this._wonderTitleInfo.titleType <= 0)
    {
      if (!((UnityEngine.Object) this.titleBkgTexture.mainTexture == (UnityEngine.Object) null) && (!((UnityEngine.Object) this.titleBkgTexture.mainTexture != (UnityEngine.Object) null) || !(this.titleBkgTexture.mainTexture.name != "kingdom_title_base_official")))
        return;
      BuilderFactory.Instance.HandyBuild((UIWidget) this.titleBkgTexture, "Texture/Kingdom/kingdom_title_base_official", (System.Action<bool>) null, true, false, string.Empty);
    }
    else
    {
      if (!((UnityEngine.Object) this.titleBkgTexture.mainTexture == (UnityEngine.Object) null) && (!((UnityEngine.Object) this.titleBkgTexture.mainTexture != (UnityEngine.Object) null) || !(this.titleBkgTexture.mainTexture.name != "kingdom_title_base_slave")))
        return;
      BuilderFactory.Instance.HandyBuild((UIWidget) this.titleBkgTexture, "Texture/Kingdom/kingdom_title_base_slave", (System.Action<bool>) null, true, false, string.Empty);
    }
  }

  private void ShowTitleBenefitDetail()
  {
    Dictionary<string, float> dictionary = (Dictionary<string, float>) null;
    if (this._showKingBenefit)
    {
      ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get("1");
      if (artifactInfo != null)
        dictionary = artifactInfo.benefits.benefits;
    }
    else if (this._wonderTitleInfo != null)
      dictionary = this._wonderTitleInfo.benefits.benefits;
    if (dictionary != null)
    {
      int key = 0;
      Dictionary<string, float>.KeyCollection.Enumerator enumerator = dictionary.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        KingdomTitleBenefitSlotRenderer itemRenderer = this.CreateItemRenderer(this._wonderTitleInfo.titleType, enumerator.Current, dictionary[enumerator.Current]);
        this.itemDict.Add(key, itemRenderer);
        ++key;
      }
    }
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, KingdomTitleBenefitSlotRenderer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this.itemPool.Release(gameObject);
      }
    }
    this.itemDict.Clear();
    this.itemPool.Clear();
  }

  private void Reposition()
  {
    this.grid.repositionNow = true;
    this.grid.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private KingdomTitleBenefitSlotRenderer CreateItemRenderer(int titleType, string benefitKey, float benefitValue)
  {
    GameObject gameObject = this.itemPool.AddChild(this.grid.gameObject);
    gameObject.SetActive(true);
    KingdomTitleBenefitSlotRenderer component = gameObject.GetComponent<KingdomTitleBenefitSlotRenderer>();
    component.SetData(titleType, benefitKey, benefitValue);
    return component;
  }

  private void OnSecondEventHandler(int time)
  {
    this.ShowWonderTitleDetail();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEventHandler);
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEventHandler);
  }

  public class Parameter : Popup.PopupParameter
  {
    public bool needViewInfo = true;
    public long nominateUserId = -1;
    public int kingdomId;
    public bool showKingBenefit;
    public WonderTitleInfo wonderTitleInfo;
    public System.Action nominateButtonPressed;
  }
}
