﻿// Decompiled with JetBrains decompiler
// Type: IAPDailyRechargePackagePayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class IAPDailyRechargePackagePayload
{
  private List<IAPDailyRechargePackage> m_DailyPackages = new List<IAPDailyRechargePackage>();
  public List<int> popupIDs = new List<int>();
  private bool m_isSwitchOff = true;
  private static IAPDailyRechargePackagePayload m_Instance;
  private IAPDialyRechargePackageState m_purchasedPackageState;
  private IAPDailyRechargePackage m_cachedPurchasedPackage;
  private int m_expires;
  private bool m_hasRequested;
  private bool m_buyThroughDailyPackage;
  private Coroutine m_Coroutine;
  private System.Action OnPurchaseFinished;

  public event System.Action onPackageDataReady;

  public event System.Action onPackagePurchased;

  public static IAPDailyRechargePackagePayload Instance
  {
    get
    {
      if (IAPDailyRechargePackagePayload.m_Instance == null)
        IAPDailyRechargePackagePayload.m_Instance = new IAPDailyRechargePackagePayload();
      return IAPDailyRechargePackagePayload.m_Instance;
    }
  }

  public bool IsSwitchOff
  {
    get
    {
      return this.m_isSwitchOff;
    }
  }

  public int Expire
  {
    get
    {
      return this.m_expires;
    }
  }

  public bool HasRequested
  {
    get
    {
      return this.m_hasRequested;
    }
    set
    {
      this.m_hasRequested = value;
    }
  }

  public bool BuyThroughDailyPackage
  {
    get
    {
      return this.m_buyThroughDailyPackage;
    }
    set
    {
      this.m_buyThroughDailyPackage = value;
    }
  }

  public IAPDialyRechargePackageState PurchasedPackageState
  {
    get
    {
      return this.m_purchasedPackageState;
    }
  }

  public IAPDailyRechargePackage CachedPurchasedPackage
  {
    get
    {
      return this.m_cachedPurchasedPackage;
    }
    set
    {
      this.m_cachedPurchasedPackage = value;
    }
  }

  public void Initialize()
  {
    this.RequestPurchaseState((System.Action<bool, object>) null, true);
    MessageHub.inst.GetPortByAction("payment_success").AddEvent(new System.Action<object>(this.OnPushPaymentSuccess));
  }

  public void Dispose()
  {
    MessageHub.inst.GetPortByAction("payment_success").RemoveEvent(new System.Action<object>(this.OnPushPaymentSuccess));
  }

  private void OnPushPaymentSuccess(object result)
  {
    if (!this.m_buyThroughDailyPackage)
      return;
    this.m_buyThroughDailyPackage = false;
    this.RequestPurchaseState((System.Action<bool, object>) ((_param1, _param2) =>
    {
      this.PublishPurchaseFinished();
      if (!this.IsActuallyBought(this.m_cachedPurchasedPackage) || this.onPackagePurchased == null)
        return;
      this.onPackagePurchased();
    }), true);
  }

  private bool IsActuallyBought(IAPDailyRechargePackage package)
  {
    bool flag = false;
    if (package != null && this.m_purchasedPackageState != null && this.m_purchasedPackageState.bought != null)
    {
      for (int index = 0; index < this.m_purchasedPackageState.bought.Count; ++index)
      {
        if (int.Parse(this.m_purchasedPackageState.bought[index].ToString()) == package.internalID)
        {
          flag = true;
          break;
        }
      }
    }
    return flag;
  }

  private void PublishPurchaseFinished()
  {
    if (this.OnPurchaseFinished == null)
      return;
    this.OnPurchaseFinished();
    this.OnPurchaseFinished = (System.Action) null;
  }

  private void OnPurchaseCallback(bool success)
  {
    if (success)
      return;
    this.RequestPurchaseState((System.Action<bool, object>) ((_param1, _param2) => this.PublishPurchaseFinished()), false);
    UIManager.inst.HideManualBlocker();
    this.StopCoroutine();
  }

  private void StopCoroutine()
  {
    if (this.m_Coroutine == null)
      return;
    Utils.StopCoroutine(this.m_Coroutine);
    this.m_Coroutine = (Coroutine) null;
  }

  public void Decode(object orgData)
  {
    this.m_DailyPackages.Clear();
    Hashtable hashtable1 = orgData as Hashtable;
    if (hashtable1 == null)
      return;
    if (hashtable1.Contains((object) "expires"))
      this.m_expires = int.Parse(hashtable1[(object) "expires"].ToString());
    if (!hashtable1.Contains((object) "packages"))
      return;
    Hashtable hashtable2 = hashtable1[(object) "packages"] as Hashtable;
    if (hashtable2 == null)
      return;
    ArrayList arrayList = new ArrayList();
    foreach (string key in (IEnumerable) hashtable2.Keys)
      arrayList.Add(hashtable2[(object) key]);
    if (arrayList != null && arrayList.Count == 0)
      return;
    for (int index = 0; index < arrayList.Count; ++index)
    {
      IAPDailyRechargePackage dailyRechargePackage = new IAPDailyRechargePackage();
      Hashtable data = arrayList[index] as Hashtable;
      if (data != null)
      {
        dailyRechargePackage.Decode(data);
        this.m_DailyPackages.Add(dailyRechargePackage);
      }
    }
    this.m_DailyPackages.Sort(new Comparison<IAPDailyRechargePackage>(this.Compare));
    if (this.onPackageDataReady == null)
      return;
    this.onPackageDataReady();
  }

  public void ParsePurchaseState(object orgData)
  {
    Hashtable data = orgData as Hashtable;
    if (data == null)
      return;
    if (data.ContainsKey((object) "ret"))
    {
      if (data[(object) "ret"] == null)
        this.m_isSwitchOff = true;
    }
    else
      this.m_isSwitchOff = false;
    if (this.m_isSwitchOff)
      return;
    this.m_purchasedPackageState = (IAPDialyRechargePackageState) null;
    this.m_purchasedPackageState = new IAPDialyRechargePackageState();
    this.m_purchasedPackageState.Decode(data);
  }

  public bool AreAllPackagesPurchased()
  {
    bool flag = false;
    if (this.m_purchasedPackageState != null)
      flag = this.m_purchasedPackageState.all;
    return flag;
  }

  private int Compare(IAPDailyRechargePackage x, IAPDailyRechargePackage y)
  {
    return Math.Sign(x.type - y.type);
  }

  public List<IAPDailyRechargePackage> GetDailyRechargePackages()
  {
    List<IAPDailyRechargePackage> dailyRechargePackageList = new List<IAPDailyRechargePackage>();
    for (int index = 0; index < this.m_DailyPackages.Count; ++index)
      dailyRechargePackageList.Add(this.m_DailyPackages[index]);
    return dailyRechargePackageList;
  }

  public void BuyProduct(string productId, string packageId, string groupId, System.Action finish = null)
  {
    this.OnPurchaseFinished = finish;
    if (Application.isEditor)
    {
      this.RequestPurchaseState((System.Action<bool, object>) ((_param1, _param2) =>
      {
        this.PublishPurchaseFinished();
        if (this.onPackagePurchased == null)
          return;
        this.onPackagePurchased();
      }), false);
    }
    else
    {
      UIManager.inst.ShowManualBlocker();
      PaymentManager.Instance.BuyProduct(productId, packageId, groupId, string.Empty, new System.Action<bool>(this.OnPurchaseCallback));
      this.StopCoroutine();
      this.m_Coroutine = Utils.ExecuteInSecs(60f, (System.Action) (() => this.RequestPurchaseState((System.Action<bool, object>) ((_param1, _param2) =>
      {
        this.PublishPurchaseFinished();
        UIManager.inst.HideManualBlocker();
        this.StopCoroutine();
      }), false)));
    }
  }

  public void RequestServerData(System.Action<bool, object> callback, bool loader = false)
  {
    if (loader)
      MessageHub.inst.GetPortByAction("DailyPackage:getDailyPackages").SendLoader((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          this.Decode(data);
        if (callback == null)
          return;
        callback(ret, data);
      }), false, false);
    else
      MessageHub.inst.GetPortByAction("DailyPackage:getDailyPackages").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          this.Decode(data);
        if (callback == null)
          return;
        callback(ret, data);
      }), true);
  }

  public void RequestPurchaseState(System.Action<bool, object> callback, bool loader = false)
  {
    if (loader)
      MessageHub.inst.GetPortByAction("DailyPackage:getStatusOfToday").SendLoader((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          this.ParsePurchaseState(data);
        if (callback == null)
          return;
        callback(ret, data);
      }), false, false);
    else
      MessageHub.inst.GetPortByAction("DailyPackage:getStatusOfToday").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          this.ParsePurchaseState(data);
        if (callback == null)
          return;
        callback(ret, data);
      }), true);
  }
}
