﻿// Decompiled with JetBrains decompiler
// Type: TimeLimitActivityRankItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TimeLimitActivityRankItem : MonoBehaviour
{
  private const string ICON_FOLDER_PATH = "Texture/LeaderboardIcons/";
  private const string FIRST_CROWN_PATH = "icon_no1";
  private const string SECOND_CROWN_PATH = "icon_no2";
  private const string THIRD_CROWN_PATH = "icon_no3";
  public UISprite[] bgs;
  public UISprite firstBg;
  public UISprite secondBg;
  public UISprite thirdBg;
  public UITexture icon;
  public UILabel rank;
  public UILabel name;
  public UILabel worldName;

  public bool Normal { get; set; }

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
  }

  private void HideAll()
  {
    NGUITools.SetActive(this.bgs[0].gameObject, false);
    NGUITools.SetActive(this.bgs[1].gameObject, false);
    NGUITools.SetActive(this.firstBg.gameObject, false);
    NGUITools.SetActive(this.secondBg.gameObject, false);
    NGUITools.SetActive(this.thirdBg.gameObject, false);
    NGUITools.SetActive(this.icon.gameObject, false);
  }

  public void SetData(ActivityRankData data)
  {
    NGUITools.SetActive(this.rank.gameObject, false);
    if (this.Normal || data.rank > 3)
      NGUITools.SetActive(this.rank.gameObject, true);
    this.worldName.text = data.WorldName;
    this.rank.text = data.rank.ToString();
    this.name.text = data.FullName;
    this.HideAll();
    if (this.Normal)
    {
      NGUITools.SetActive(this.bgs[0].gameObject, data.rank % 2 == 0);
      NGUITools.SetActive(this.bgs[1].gameObject, data.rank % 2 != 0);
    }
    else
    {
      switch (data.rank)
      {
        case 1:
          NGUITools.SetActive(this.firstBg.gameObject, true);
          NGUITools.SetActive(this.icon.gameObject, true);
          BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, "Texture/LeaderboardIcons/icon_no1", (System.Action<bool>) null, true, false, string.Empty);
          break;
        case 2:
          NGUITools.SetActive(this.secondBg.gameObject, true);
          NGUITools.SetActive(this.icon.gameObject, true);
          BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, "Texture/LeaderboardIcons/icon_no2", (System.Action<bool>) null, true, false, string.Empty);
          break;
        case 3:
          NGUITools.SetActive(this.thirdBg.gameObject, true);
          NGUITools.SetActive(this.icon.gameObject, true);
          BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, "Texture/LeaderboardIcons/icon_no3", (System.Action<bool>) null, true, false, string.Empty);
          break;
        default:
          NGUITools.SetActive(this.bgs[0].gameObject, data.rank % 2 == 0);
          NGUITools.SetActive(this.bgs[1].gameObject, data.rank % 2 != 0);
          break;
      }
    }
  }
}
