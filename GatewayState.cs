﻿// Decompiled with JetBrains decompiler
// Type: GatewayState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GatewayState : LoadBaseState
{
  public GatewayState(int step)
    : base(step)
  {
  }

  protected override SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.First;
    }
  }

  public override string Key
  {
    get
    {
      return nameof (GatewayState);
    }
  }

  protected override void Prepare()
  {
    this.RefreshProgress();
    if (Application.isEditor)
    {
      this.Finish();
    }
    else
    {
      LanguageSDK.Instance.SetBiLanguage(false);
      this.Finish();
    }
  }

  protected override void Finish()
  {
    base.Finish();
    this.Preloader.OnGatewayStateFinish();
  }
}
