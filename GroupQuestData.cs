﻿// Decompiled with JetBrains decompiler
// Type: GroupQuestData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class GroupQuestData : BaseData
{
  private long questId = (long) GroupQuestData.INVALID_ID;
  public const int QUEST_TYPE_RECOMMEND = 1;
  public const int QUEST_TYPE_BRANCH = 2;
  public const int QUEST_PROGRESS = 0;
  public const int QUEST_COMPLETE = 1;
  public const int QUEST_CLAIM = 2;
  public const string CATEGORY_BUILD = "build";
  public const string CATEGORY_TAIN = "train";
  public const string CATEGORY_PVE = "pve";
  public const string CATEGORY_PVP = "pvp";
  public const string CATEGORY_RESEARCH = "research";
  public const string CATEGORY_ALLAINCE_JOIN = "alliance_join";
  public const string CATEGORY_ALLAINCE_HELP = "alliance_help";
  public const string CATEGORY_RESOURCE_COLLECT = "resource_collect";
  public const string CATEGORY_RESOURCE_RATE = "resource_rate";
  public const string CATEGORY_LEGEND_RECRUITE = "legend_recruit";
  public const string CATEGORY_LEGEND_SKILL_UPGRADE = "legend_skill_up";
  public const string CATEGORY_LEGEND_XP_ITEM_USE = "legend_xp_item";
  public const string CATEGORY_ALLIANCE_QUEST = "legend_quest";
  public const string CATEGORY_DRAGON = "dragon";
  public const string CATEGORY_WISH_WELL = "wish";
  public const string CATEGORY_PLACE = "place";
  public const string CATEGORY_LORD = "lord";
  public const string CATEGORY_EQUIP = "equip";
  public static int INVALID_ID;
  private int _state;
  private int _type;
  private string _category;
  private List<BaseRequirement> _baseRequires;
  private QuestInfo _info;
  private QuestRewardAgent _agent;
  private BasePresent _present;

  public long QuestID
  {
    get
    {
      return this.questId;
    }
  }

  public int Status
  {
    get
    {
      return this._state;
    }
  }

  public int Type
  {
    get
    {
      return this._type;
    }
  }

  public QuestInfo Info
  {
    get
    {
      return ConfigManager.inst.DB_Quest.GetData((int) this.questId);
    }
  }

  public void Claim()
  {
    this._state = 2;
  }

  public bool Decode(object orgData, long updateTime)
  {
    bool flag = false;
    if (!this.CheckAndResetUpdateTime(updateTime) || orgData == null)
      return false;
    Hashtable inData = orgData as Hashtable;
    if (inData == null)
      return false;
    return flag | DatabaseTools.UpdateData(inData, "quest_id", ref this.questId) | DatabaseTools.UpdateData(inData, "status", ref this._state) | DatabaseTools.UpdateData(inData, "type", ref this._type) | DatabaseTools.UpdateData(inData, "category", ref this._category);
  }

  public string Category
  {
    get
    {
      return this._category;
    }
  }

  public BasePresent Present
  {
    get
    {
      if (this._present == null)
      {
        this._present = BasePresent.Create(this.Info.Category, (int) this.QuestID, this.QuestID);
        this._present.Data = PresentDataFactory.Instance.CreateData(PresentData.DataType.Quest, (int) this.QuestID, this.QuestID);
      }
      return this._present;
    }
  }

  public static long GetInternalId(object orgData)
  {
    if (orgData == null)
      return -1;
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable == null)
      return -1;
    long result = -1;
    if (hashtable.ContainsKey((object) "quest_id"))
      long.TryParse(hashtable[(object) "quest_id"].ToString(), out result);
    return result;
  }

  public List<BaseRequirement> BaseRequires
  {
    get
    {
      return this._baseRequires;
    }
  }

  public QuestRewardAgent RewardAgent
  {
    get
    {
      if (this._agent == null)
        this._agent = new QuestRewardAgent(this.questId);
      return this._agent;
    }
  }

  public List<QuestReward> Rewards
  {
    get
    {
      return this.RewardAgent.Rewards;
    }
  }

  public struct Params
  {
    public const string KEY = "user_quest";
    public const string UID = "uid";
    public const string QUEST_ID = "quest_id";
    public const string STATUS = "status";
    public const string TYPE = "type";
    public const string CATEGORY = "category";
  }
}
