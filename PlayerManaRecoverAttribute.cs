﻿// Decompiled with JetBrains decompiler
// Type: PlayerManaRecoverAttribute
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class PlayerManaRecoverAttribute : RoundPlayerAttribute
{
  public PlayerManaRecoverAttribute(RoundPlayer player)
    : base(player)
  {
  }

  protected override DragonKnightAttibute.Type Type
  {
    get
    {
      return DragonKnightAttibute.Type.MPRecover;
    }
  }

  public override void Reset()
  {
    int mpRecover = this.Owner.MPRecover;
    int result = mpRecover;
    if (this.Owner.PlayerCamp != RoundPlayer.Camp.Evil)
      result = Mathf.CeilToInt(AttributeCalcHelper.Instance.GetFinalDataFromDragonKnightData((float) mpRecover, "calc_dragon_knight_mana_recover", (DragonKnightData) null));
    this.CurrentValue = this.BuffFactor(result);
    if (this.MaxValue > 0)
      return;
    this.MaxValue = result;
  }
}
