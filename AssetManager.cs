﻿// Decompiled with JetBrains decompiler
// Type: AssetManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;

public class AssetManager : MonoBehaviour
{
  public static bool IsLoadAssetFromBundle = true;
  private List<LoadingAsset> loadingAssets = new List<LoadingAsset>();
  private Dictionary<string, LoadedAsset> loadedAssets = new Dictionary<string, LoadedAsset>();
  private static AssetManager _instance;

  public static AssetManager Instance
  {
    get
    {
      if ((UnityEngine.Object) AssetManager._instance == (UnityEngine.Object) null)
      {
        GameObject gameObject = new GameObject();
        AssetManager._instance = gameObject.AddComponent<AssetManager>();
        gameObject.AddComponent<DontDestroy>();
        gameObject.name = nameof (AssetManager);
        if ((UnityEngine.Object) null == (UnityEngine.Object) AssetManager._instance)
          throw new ArgumentException("AssetManager hasn't been created yet.");
      }
      return AssetManager._instance;
    }
  }

  public void PrintContent()
  {
    using (List<LoadingAsset>.Enumerator enumerator = this.loadingAssets.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        LoadingAsset current = enumerator.Current;
      }
    }
    using (Dictionary<string, LoadedAsset>.Enumerator enumerator = this.loadedAssets.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        LoadedAsset loadedAsset = enumerator.Current.Value;
      }
    }
  }

  private void Start()
  {
    AssetManager._instance = this;
    BundleManager.Instance.onBundleLoadFinished += new System.Action<LoadingBundle>(this.OnBundleLoadFinished);
  }

  private void Update()
  {
    if (this.loadingAssets.Count == 0)
      return;
    for (int index = 0; index < this.loadingAssets.Count; ++index)
    {
      LoadingAsset loadingAsset = this.loadingAssets[index];
      if (loadingAsset.abr != null && loadingAsset.abr.isDone)
      {
        UnityEngine.Object asset;
        try
        {
          asset = loadingAsset.abr.asset;
        }
        catch
        {
          this.OnLoadAssetFromBundleFailed(loadingAsset.bundlename, loadingAsset.name);
          break;
        }
        if ((UnityEngine.Object) null == asset)
        {
          this.OnLoadAssetFromBundleFailed(loadingAsset.bundlename, loadingAsset.name);
          break;
        }
        try
        {
          if (loadingAsset.callback != null)
          {
            LoadedAsset loadedAsset = new LoadedAsset(loadingAsset.name, loadingAsset.type, asset);
            loadedAsset.onAssetUnLoad += new System.Action<string>(this.OnAssetUnload);
            loadedAsset.RefCount = loadingAsset.RefCount;
            if (!this.loadedAssets.ContainsKey(loadingAsset.name))
              this.loadedAssets.Add(loadingAsset.name, loadedAsset);
            else
              D.error((object) ("~~~~~already has key in loaded assets2 " + loadingAsset.name));
            loadingAsset.callback(asset, true);
          }
          this.loadingAssets.RemoveAt(index);
          --index;
        }
        catch (NullReferenceException ex)
        {
          this.loadingAssets.RemoveAt(index);
          --index;
        }
      }
    }
  }

  private void OnLoadAssetFromBundleFailed(string bundlename, string assetname)
  {
    D.error((object) ("load asset failed " + bundlename + " " + assetname));
    if (!NetWorkDetector.Instance.IsReadyRestart())
      NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.LoadAsset, "asset", bundlename + " " + assetname);
    if (!assetname.Contains("texture+"))
      NetWorkDetector.Instance.RestartGame(ScriptLocalization.Get("data_error_create_description", true), true);
    FileInfo fileInfo = new FileInfo(NetApi.inst.BuildLocalBundlePath(BundleManager.Instance.GetFullBundleName(bundlename)));
    if (fileInfo != null && fileInfo.Exists)
      fileInfo.Delete();
    this.loadingAssets.Clear();
  }

  public UnityEngine.Object Load(string fullname, System.Type type = null)
  {
    if (string.IsNullOrEmpty(fullname) || fullname.EndsWith("/"))
      return (UnityEngine.Object) null;
    if (type == null)
      type = typeof (UnityEngine.Object);
    if (!AssetManager.IsLoadAssetFromBundle)
      return Resources.Load(fullname, type);
    string assetNameFromPath = this.GetAssetNameFromPath(fullname);
    if (this.GetBundleNameFromAsset(assetNameFromPath) == null)
      return Resources.Load(fullname, type);
    return this.LoadAssetFromBundle(assetNameFromPath, type);
  }

  public void LoadAsync(string fullname, System.Action<UnityEngine.Object, bool> callBack, System.Type type = null)
  {
    if (callBack == null || string.IsNullOrEmpty(fullname) || fullname.EndsWith("/"))
      return;
    if (type == null)
      type = typeof (UnityEngine.Object);
    if (!AssetManager.IsLoadAssetFromBundle)
    {
      this.StartCoroutine(this.LoadAssetFromResourceAsync(fullname, callBack, type));
    }
    else
    {
      string assetname = this.GetAssetNameFromPath(fullname);
      string bundlename = this.GetBundleNameFromAsset(assetname);
      if (bundlename == null)
        this.StartCoroutine(this.LoadAssetFromResourceAsync(fullname, callBack, type));
      else if (this.loadedAssets.ContainsKey(assetname))
      {
        ++this.loadedAssets[assetname].RefCount;
        callBack(this.loadedAssets[assetname].ObjRef, true);
      }
      else
      {
        LoadingAsset loadingAsset1 = this.loadingAssets.Find((Predicate<LoadingAsset>) (a =>
        {
          if (a.name == assetname)
            return a.type == type;
          return false;
        }));
        if (loadingAsset1 != null)
        {
          if (this.HasSameAction(loadingAsset1.callback, callBack))
            return;
          ++loadingAsset1.RefCount;
          loadingAsset1.callback += callBack;
        }
        else
        {
          bool flag = !this.loadingAssets.Exists((Predicate<LoadingAsset>) (b =>
          {
            if (b.bundlename == bundlename)
              return !b.isLoading;
            return false;
          }));
          LoadingAsset loadingAsset2 = new LoadingAsset(assetname, type, bundlename, callBack);
          this.loadingAssets.Add(loadingAsset2);
          ++loadingAsset2.RefCount;
          if (!flag)
            return;
          BundleManager.Instance.LoadBundle(bundlename);
        }
      }
    }
  }

  public void UnLoadAsset(string fullname, System.Type type = null, System.Action<UnityEngine.Object, bool> callBack = null)
  {
    LoadedAsset loadedAsset = (LoadedAsset) null;
    string assetname = this.GetAssetNameFromPath(fullname);
    if (this.loadedAssets.TryGetValue(assetname, out loadedAsset))
    {
      --loadedAsset.RefCount;
    }
    else
    {
      if (type == null)
        type = typeof (UnityEngine.Object);
      LoadingAsset loadingAsset = this.loadingAssets.Find((Predicate<LoadingAsset>) (a =>
      {
        if (a.name == assetname)
          return a.type == type;
        return false;
      }));
      if (loadingAsset == null || !this.HasSameAction(loadingAsset.callback, callBack))
        return;
      --loadingAsset.RefCount;
      loadingAsset.callback -= callBack;
    }
  }

  public UnityEngine.Object HandyLoad(string fullname, System.Type type = null)
  {
    if (string.IsNullOrEmpty(fullname) || fullname.EndsWith("/"))
      return (UnityEngine.Object) null;
    if (type == null)
      type = typeof (UnityEngine.Object);
    if (!AssetManager.IsLoadAssetFromBundle)
      return Resources.Load(fullname, type);
    string assetNameFromPath = this.GetAssetNameFromPath(fullname);
    if (this.GetBundleNameFromAsset(assetNameFromPath) == null)
      return Resources.Load(fullname, type);
    this.StartCoroutine(this.AutoUnloadAsset(fullname, type));
    return this.LoadAssetFromBundle(assetNameFromPath, type);
  }

  [DebuggerHidden]
  private IEnumerator AutoUnloadAsset(string fullname, System.Type type)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AssetManager.\u003CAutoUnloadAsset\u003Ec__Iterator5D()
    {
      fullname = fullname,
      type = type,
      \u003C\u0024\u003Efullname = fullname,
      \u003C\u0024\u003Etype = type,
      \u003C\u003Ef__this = this
    };
  }

  private UnityEngine.Object LoadAssetFromBundle(string assetname, System.Type type)
  {
    if (this.loadedAssets.ContainsKey(assetname))
    {
      ++this.loadedAssets[assetname].RefCount;
      return this.loadedAssets[assetname].ObjRef;
    }
    string bundleNameFromAsset = this.GetBundleNameFromAsset(assetname);
    AssetBundle ab;
    BundleManager.Instance.AbCache.TryGetObject(bundleNameFromAsset, out ab);
    if ((UnityEngine.Object) ab == (UnityEngine.Object) null)
      BundleManager.Instance.Ablist.TryGetValue(bundleNameFromAsset, out ab);
    if ((UnityEngine.Object) null == (UnityEngine.Object) ab)
    {
      ab = BundleManager.Instance.LoadBundleFromLocal(bundleNameFromAsset);
      if ((UnityEngine.Object) null == (UnityEngine.Object) ab)
        return (UnityEngine.Object) null;
      BundleManager.Instance.RecordBundle(ab);
    }
    string shortname = this.GetShortname(assetname);
    UnityEngine.Object @object = ab.LoadAsset(shortname, type);
    if ((UnityEngine.Object) null == @object)
    {
      this.OnLoadAssetFromBundleFailed(bundleNameFromAsset, shortname);
      return (UnityEngine.Object) null;
    }
    LoadedAsset loadedAsset = new LoadedAsset(assetname, type, @object);
    loadedAsset.onAssetUnLoad += new System.Action<string>(this.OnAssetUnload);
    ++loadedAsset.RefCount;
    if (!this.loadedAssets.ContainsKey(assetname))
      this.loadedAssets.Add(assetname, loadedAsset);
    else
      D.error((object) ("already has key in loaded assets " + assetname));
    return @object;
  }

  private string GetShortname(string assetname)
  {
    string[] strArray = assetname.Split('+');
    return strArray[strArray.Length - 1];
  }

  [DebuggerHidden]
  private IEnumerator LoadAssetFromResourceAsync(string fullname, System.Action<UnityEngine.Object, bool> callBack, System.Type type = null)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AssetManager.\u003CLoadAssetFromResourceAsync\u003Ec__Iterator5E()
    {
      fullname = fullname,
      type = type,
      callBack = callBack,
      \u003C\u0024\u003Efullname = fullname,
      \u003C\u0024\u003Etype = type,
      \u003C\u0024\u003EcallBack = callBack
    };
  }

  private string GetAssetNameFromPath(string fullname)
  {
    return fullname.Replace("/", "+").ToLower();
  }

  private string GetBundleNameFromAsset(string assetname)
  {
    string str = (string) null;
    Dictionary<string, string> map = VersionManager.Instance.Asset2Bundle.map;
    if (map == null)
      return (string) null;
    map.TryGetValue(assetname, out str);
    return str;
  }

  public void OnBundleLoadFinished(LoadingBundle lb)
  {
    for (int index = 0; index < this.loadingAssets.Count; ++index)
    {
      LoadingAsset loadingAsset = this.loadingAssets[index];
      if (!loadingAsset.isLoading)
        this.LoadAssetFromBundleAsync(lb, loadingAsset);
    }
  }

  public void LoadAssetFromBundleAsync(LoadingBundle lb, LoadingAsset la)
  {
    if (!(la.bundlename == lb.name))
      return;
    string shortname = this.GetShortname(la.name);
    AssetBundleRequest assetBundleRequest = lb.ab.LoadAssetAsync(shortname, la.type);
    la.abr = assetBundleRequest;
    la.isLoading = true;
  }

  private void OnAssetUnload(string name)
  {
    if (!this.loadedAssets.ContainsKey(name))
      return;
    this.loadedAssets.Remove(name);
  }

  public void Init()
  {
    BundleManager.Instance.Init();
  }

  public void Dispose()
  {
    BundleManager.Instance.onBundleLoadFinished -= new System.Action<LoadingBundle>(this.OnBundleLoadFinished);
    BundleManager.Instance.Dispose();
    this.UnLoadUnusedRes();
    AssetManager._instance = (AssetManager) null;
  }

  public void ClearTempData()
  {
    BundleManager.Instance.ClearTempData();
    this.loadingAssets.Clear();
  }

  private void DisposeLoadedAssets()
  {
    using (Dictionary<string, LoadedAsset>.ValueCollection.Enumerator enumerator = this.loadedAssets.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.RefCount = 0;
    }
  }

  public void UnLoadUnusedRes()
  {
    BundleManager.Instance.AbCache.Clear();
    Resources.UnloadUnusedAssets();
  }

  public bool HasSameAction(System.Action<UnityEngine.Object, bool> ori, System.Action<UnityEngine.Object, bool> tar)
  {
    if (ori == null)
      return false;
    foreach (Delegate invocation in ori.GetInvocationList())
    {
      if (invocation == (Delegate) tar)
        return true;
    }
    return false;
  }

  public string GetBundleNameFromPath(string fullName)
  {
    return this.GetBundleNameFromAsset(this.GetAssetNameFromPath(fullName));
  }

  public void LogCurrentAssets()
  {
    using (Dictionary<string, LoadedAsset>.ValueCollection.Enumerator enumerator = this.loadedAssets.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        LoadedAsset current = enumerator.Current;
      }
    }
  }
}
