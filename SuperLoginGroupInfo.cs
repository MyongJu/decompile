﻿// Decompiled with JetBrains decompiler
// Type: SuperLoginGroupInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class SuperLoginGroupInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "start_date")]
  public int startDate;
  [Config(Name = "start_month")]
  public int startMonth;
  [Config(Name = "start_year")]
  public int startYear;
  [Config(Name = "max_day")]
  public int maxDay;
  [Config(Name = "description")]
  public string description;
  [Config(Name = "display")]
  public string display;

  public string Description
  {
    get
    {
      return Utils.XLAT(this.description);
    }
  }
}
