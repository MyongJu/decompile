﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_TargetSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Runtime.InteropServices;
using UI;
using UnityEngine;

public class AllianceWar_TargetSlot : MonoBehaviour
{
  public int layerIndex;
  private Coordinate _Location;
  [SerializeField]
  private AllianceWar_TargetSlot.Panel panel;

  private GameObject RegenerateTargetGameObject(string type)
  {
    return this.RegenerateTargetGameObject(type, Vector3.one, Vector3.one, new Quaternion(), string.Empty);
  }

  private GameObject RegenerateTargetGameObject(string type, Vector3 localPosition, Vector3 localScale, [Optional] Quaternion localRotation, string prefabPath = "")
  {
    if ((UnityEngine.Object) this.panel.targetPrefab != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.panel.targetPrefab);
      this.panel.targetPrefab = (GameObject) null;
    }
    this.panel.targetPrefab = this.GetTargetPrefab(type, prefabPath);
    if ((UnityEngine.Object) this.panel.targetPrefab != (UnityEngine.Object) null)
    {
      this.panel.targetPrefab.name = type;
      this.panel.targetPrefab.transform.parent = this.panel.targetPrefabRoot;
      this.panel.targetPrefab.transform.localPosition = localPosition;
      this.panel.targetPrefab.transform.localRotation = localRotation;
      this.panel.targetPrefab.transform.localScale = localScale;
      this.SetLayer("SmallViewport");
    }
    return this.panel.targetPrefab;
  }

  [HideInInspector]
  public string Name
  {
    get
    {
      return this.panel.targetName.text;
    }
    set
    {
      this.panel.targetName.text = value;
    }
  }

  [HideInInspector]
  public Coordinate Location
  {
    get
    {
      return this._Location;
    }
    set
    {
      this._Location = value;
      this.panel.targetLocation.text = string.Format("X:{0}, Y:{1}", (object) this._Location.X, (object) this._Location.Y);
    }
  }

  public void SetSortingLayer(string layerName)
  {
    if ((UnityEngine.Object) this.panel.targetPrefab == (UnityEngine.Object) null)
      return;
    SpriteRenderer[] componentsInChildren = this.panel.targetPrefab.gameObject.GetComponentsInChildren<SpriteRenderer>();
    for (int index = 0; componentsInChildren != null && index < componentsInChildren.Length; ++index)
      componentsInChildren[index].sortingLayerName = layerName;
    Transform child = this.panel.targetPrefabRoot.GetChild(0);
    if (!((UnityEngine.Object) child != (UnityEngine.Object) null))
      return;
    child.localPosition = Vector3.zero;
  }

  public void SetLayer(string layerName)
  {
    this.layerIndex = LayerMask.NameToLayer(layerName);
    Transform[] componentsInChildren = this.panel.targetPrefabRoot.gameObject.GetComponentsInChildren<Transform>();
    for (int index = 0; componentsInChildren != null && index < componentsInChildren.Length; ++index)
      componentsInChildren[index].gameObject.layer = this.layerIndex;
  }

  public void Setup(long rallyId, float targetOffset = 80f, bool setWithOriginOffset = true)
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(rallyId);
    if (rallyData == null)
      return;
    if (rallyData.bossId != 0)
    {
      if (rallyData.type == RallyData.RallyType.TYPE_RALLY_RAB)
      {
        this.SetAlliancePortal(rallyId);
      }
      else
      {
        WorldBossData worldBossData = DBManager.inst.DB_WorldBossDB.Get(rallyData.bossId);
        if (worldBossData != null)
          this.SetWorldBoss(rallyId, worldBossData.configId, targetOffset, setWithOriginOffset);
        else
          this.SetBoss(rallyId, targetOffset);
      }
    }
    else if (WonderUtils.IsWonder(rallyData.location))
      this.SetWonder(rallyData.location, targetOffset, setWithOriginOffset);
    else if (WonderUtils.IsWonderTower(rallyData.location))
      this.SetWonderTower(rallyData.location, targetOffset, setWithOriginOffset);
    else if (DBManager.inst.DB_AllianceFortress.Get((long) rallyData.fortressId) != null)
      this.SetFortress((long) rallyData.fortressId);
    else if (DBManager.inst.DB_AllianceTemple.Get((long) rallyData.fortressId) != null)
    {
      this.SetAllianceBuilding(rallyId);
    }
    else
    {
      if (DBManager.inst.DB_City.Get(rallyData.targetCityId) == null)
        return;
      this.SetCity(rallyData.targetCityId, targetOffset);
    }
  }

  public void SetEmpty()
  {
    this.Name = string.Empty;
    this.Location = Coordinate.zero;
    if (!((UnityEngine.Object) this.panel.targetPrefab != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.panel.targetPrefab);
  }

  public void SetCity(long cityId, float targetOffset = 0.0f)
  {
    CityData cityData = DBManager.inst.DB_City.Get(cityId);
    if (cityData != null)
    {
      this.Name = cityData.cityName;
      this.Location = cityData.cityLocation;
      this.ResetTargetPosition(this.RegenerateTargetGameObject(string.Format("tiles_stronghold_lv{0}", (object) Mathf.Min(Mathf.Max(1, cityData.level), MapUtils.CityPrefabNames.Length))), 35f + targetOffset);
    }
    else
      this.SetEmpty();
  }

  public void SetWonder(Coordinate loc, float targetOffset = 0.0f, bool setWithOriginOffset = true)
  {
    this.Name = Utils.XLAT("throne_wonder_name");
    this.Location = loc;
    float num = 130f;
    GameObject go = this.RegenerateTargetGameObject("wonder_northern02", Vector3.zero, new Vector3(0.35f, 0.35f, 1f), new Quaternion(), string.Empty);
    this.ResetTargetPosition(go, !setWithOriginOffset ? targetOffset : num);
    WonderVFX component = go.GetComponent<WonderVFX>();
    if (!((UnityEngine.Object) null != (UnityEngine.Object) component))
      return;
    component.Reset();
  }

  public void SetWonderTower(Coordinate loc, float targetOffset = 0.0f, bool setWithOriginOffset = true)
  {
    int wonderTowerId = (int) WonderUtils.GetWonderTowerID(loc);
    this.Name = WonderUtils.GetWonderTowerName(wonderTowerId);
    this.Location = loc;
    GameObject go = this.RegenerateTargetGameObject(WonderUtils.GetWonderTowerPrefabName(wonderTowerId), Vector3.zero, new Vector3(0.55f, 0.55f, 1f), new Quaternion(), string.Empty);
    this.ResetTargetPosition(go, !setWithOriginOffset ? targetOffset : -15f);
    this.ResetTargetScale(go.transform.parent.gameObject, 1f, 0.25f);
    WonderTowerVFX component = go.GetComponent<WonderTowerVFX>();
    if (!((UnityEngine.Object) null != (UnityEngine.Object) component))
      return;
    component.Reset();
  }

  public void SetFortress(long fortressId)
  {
    AllianceFortressData fortressData = DBManager.inst.DB_AllianceFortress.Get(fortressId);
    if (fortressData == null)
      return;
    AllianceBuildingStaticInfo buildingStaticInfo1 = ConfigManager.inst.DB_AllianceBuildings.Get(fortressData.ConfigId);
    this.Name = fortressData == null || string.IsNullOrEmpty(fortressData.Name) ? buildingStaticInfo1.LocalName : fortressData.Name;
    this.Location = fortressData.Location;
    GameObject gameObject = (GameObject) null;
    AllianceBuildingStaticInfo buildingStaticInfo2 = ConfigManager.inst.DB_AllianceBuildings.Get(fortressData.ConfigId);
    if (buildingStaticInfo2.type == 0)
    {
      gameObject = this.RegenerateTargetGameObject("tiles_alliance_fortress");
      gameObject.transform.localPosition = new Vector3(0.0f, 27.21f, 0.0f);
      gameObject.transform.localScale = new Vector3(0.85f, 0.85f, 1f);
    }
    else if (buildingStaticInfo2.type == 1)
    {
      gameObject = this.RegenerateTargetGameObject("tiles_alliance_turret");
      gameObject.transform.localPosition = new Vector3(0.0f, -35f, 0.0f);
      gameObject.transform.localScale = new Vector3(1.2f, 1.2f, 1f);
    }
    if (!(bool) ((UnityEngine.Object) gameObject))
      return;
    AllianceFortressController component = gameObject.GetComponent<AllianceFortressController>();
    if (!(bool) ((UnityEngine.Object) component))
      return;
    component.UpdateForWarList(fortressData);
  }

  public void TempSetTemple(Coordinate location)
  {
    this.Name = Utils.XLAT("alliance_altar_name");
    this.Location = location;
    GameObject gameObject = this.RegenerateTargetGameObject("tiles_alliance_dragon_altar", Vector3.zero, new Vector3(0.85f, 0.85f, 1f), new Quaternion(), string.Empty);
    AllianceDragonAltarController component1 = gameObject.GetComponent<AllianceDragonAltarController>();
    if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
    {
      component1.Reset();
      this.StopParticles(gameObject);
    }
    if (!((UnityEngine.Object) gameObject != (UnityEngine.Object) null))
      return;
    AllianceFortressController component2 = gameObject.GetComponent<AllianceFortressController>();
    if (!(bool) ((UnityEngine.Object) component2))
      return;
    component2.UpdateForWarList((AllianceFortressData) null);
  }

  private void StopParticles(GameObject obj)
  {
    if ((UnityEngine.Object) obj == (UnityEngine.Object) null)
      return;
    ParticleSystem[] componentsInChildren = obj.GetComponentsInChildren<ParticleSystem>();
    for (int index = 0; index < componentsInChildren.Length; ++index)
    {
      componentsInChildren[index].Stop();
      componentsInChildren[index].gameObject.SetActive(false);
    }
  }

  private void SetBoss(long rallyId, float targetOffset)
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(rallyId);
    if (rallyData == null)
      return;
    ConfigGveBossData data = ConfigManager.inst.DB_GveBoss.GetData(rallyData.bossId);
    if (data == null)
      return;
    this.Name = Utils.XLAT(data.name);
    this.Location = rallyData.location;
    this.ResetTargetPosition(this.RegenerateTargetGameObject("tiles_gve_camp", new Vector3(0.0f, 35f, 0.0f), Vector3.one, new Quaternion(), string.Empty), targetOffset);
  }

  private void SetWorldBoss(long rallyId, int configId, float targetOffset, bool setWithOriginOffset = true)
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(rallyId);
    if (rallyData == null)
      return;
    WorldBossStaticInfo data = ConfigManager.inst.DB_WorldBoss.GetData(configId);
    if (data == null)
      return;
    this.Name = data.LocalName;
    this.Location = rallyData.location;
    GameObject gameObject = this.RegenerateTargetGameObject(data.Type, Vector3.zero, Vector3.one * 1.2f, new Quaternion(0.0f, 180f, 0.0f, 0.0f), "Prefab/Monster/");
    if (!((UnityEngine.Object) gameObject != (UnityEngine.Object) null))
      return;
    gameObject.transform.parent.localPosition = new Vector3(gameObject.transform.parent.localPosition.x, !setWithOriginOffset ? targetOffset : -30f, -100f);
  }

  private void SetAlliancePortal(long rallyId)
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(rallyId);
    if (rallyData == null)
      return;
    AllianceBossInfo allianceBossInfo = ConfigManager.inst.DB_AllianceBoss.Get(rallyData.bossId);
    if (allianceBossInfo == null)
      return;
    this.Name = Utils.XLAT(allianceBossInfo.name);
    this.Location = rallyData.location;
    GameObject gameObject = this.RegenerateTargetGameObject("tiles_alliance_portal", new Vector3(0.0f, -20f, 0.0f), Vector3.one * 0.7f, new Quaternion(), string.Empty);
    if (!((UnityEngine.Object) gameObject != (UnityEngine.Object) null) || !((UnityEngine.Object) gameObject.GetComponent<AlliancePortalController>() != (UnityEngine.Object) null))
      return;
    gameObject.GetComponent<AlliancePortalController>().Reset();
  }

  private void SetAllianceBuilding(long rallyId)
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(rallyId);
    if (rallyData == null)
      return;
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(rallyData.location);
    if (referenceAt != null)
    {
      GameObject gameObject = (GameObject) null;
      if (referenceAt.TileType == TileType.AllianceTemple)
      {
        this.Name = Utils.XLAT("alliance_altar_name");
        this.Location = rallyData.location;
        gameObject = this.RegenerateTargetGameObject("tiles_alliance_dragon_altar", new Vector3(0.0f, -15f, 0.0f), new Vector3(0.85f, 0.85f, 1f), new Quaternion(), string.Empty);
        this.ResetDragonAltarStatus(gameObject.GetComponent<AllianceDragonAltarController>());
      }
      if (!(bool) ((UnityEngine.Object) gameObject))
        return;
      AllianceFortressController component = gameObject.GetComponent<AllianceFortressController>();
      if (!(bool) ((UnityEngine.Object) component))
        return;
      component.UpdateForWarList((AllianceFortressData) null);
    }
    else
    {
      if (DBManager.inst.DB_AllianceTemple.Get((long) rallyData.fortressId) == null)
        return;
      this.Name = Utils.XLAT("alliance_altar_name");
      this.Location = rallyData.location;
      GameObject gameObject = this.RegenerateTargetGameObject("tiles_alliance_dragon_altar", new Vector3(0.0f, -15f, 0.0f), new Vector3(0.85f, 0.85f, 1f), new Quaternion(), string.Empty);
      this.ResetDragonAltarStatus(gameObject.GetComponent<AllianceDragonAltarController>());
      if (!((UnityEngine.Object) gameObject != (UnityEngine.Object) null))
        return;
      AllianceFortressController component = gameObject.GetComponent<AllianceFortressController>();
      if (!(bool) ((UnityEngine.Object) component))
        return;
      component.UpdateForWarList((AllianceFortressData) null);
    }
  }

  private void ResetDragonAltarStatus(AllianceDragonAltarController altar)
  {
    if (!((UnityEngine.Object) altar != (UnityEngine.Object) null))
      return;
    altar.Reset();
    this.StopParticles(altar.gameObject);
  }

  public void OnGotoClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    LinkerHub.Instance.LHGotoTile(new LPTile()
    {
      k = this.Location.K,
      x = this.Location.X,
      y = this.Location.Y
    });
  }

  public void Reset()
  {
    this.panel.targetName = this.transform.Find("Name").gameObject.GetComponent<UILabel>();
    this.panel.targetLocation = this.transform.Find("LocationText").gameObject.GetComponent<UILabel>();
    this.panel.TT_AllianceBoss = this.transform.Find("AllianceBoss").gameObject.GetComponent<UITexture>();
    this.panel.targetPrefabRoot = this.transform.Find("PrefabRoot").gameObject.transform;
  }

  private GameObject GetTargetPrefab(string type, string prefabPath = "")
  {
    string fullname = string.Format("{0}{1}", !string.IsNullOrEmpty(prefabPath) ? (object) prefabPath : (object) "Prefab/Tiles/", (object) type);
    if (fullname != string.Empty)
    {
      if (!(AssetManager.Instance.HandyLoad(fullname, (System.Type) null) == (UnityEngine.Object) null))
        return UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad(fullname, (System.Type) null) as GameObject);
      D.error((object) ("War list want instantiat a null object at : " + fullname));
    }
    return (GameObject) null;
  }

  private void ResetTargetPosition(GameObject go, float targetOffset)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      return;
    go.transform.parent.localPosition = new Vector3(go.transform.parent.localPosition.x, targetOffset, go.transform.parent.localPosition.z);
  }

  private void ResetTargetScale(GameObject go, float targetScale, float targetParticleScale)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      return;
    go.transform.localScale = new Vector3(targetScale, targetScale, targetScale);
    foreach (ParticleScaler componentsInChild in go.GetComponentsInChildren<ParticleScaler>())
    {
      if ((UnityEngine.Object) componentsInChild != (UnityEngine.Object) null)
      {
        componentsInChild.particleScale = targetParticleScale;
        componentsInChild.alsoScaleGameobject = true;
      }
    }
  }

  [Serializable]
  protected class Panel
  {
    public UILabel targetName;
    public UILabel targetLocation;
    public Transform targetPrefabRoot;
    public UITexture TT_AllianceBoss;
    [HideInInspector]
    public GameObject targetPrefab;
  }
}
