﻿// Decompiled with JetBrains decompiler
// Type: BaseReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BaseReportPopup : Popup
{
  private const string shareprefix = "SharedMail";
  private const int timelast = 60;
  protected long mailtime;
  protected long mailID;
  protected BaseReportPopup.Parameter param;
  protected AbstractMailEntry maildata;
  protected bool isFavorite;
  public UILabel title;
  public UILabel coordinate;
  public UILabel time;
  public UITexture headerTexture;
  public GameObject favIcon;
  public GameObject favBaseIcon;
  public GameObject delIcon;
  public GameObject shareIcon;
  public GameObject btnCollectionBase;
  public UILabel mailcontent;
  protected bool canShare;

  public void OnCloseBtnPressed()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  public void OnDeleteMail()
  {
    this.OnDeleteBtnClicked();
  }

  public void OnBookmarkMail()
  {
    this.isFavorite = !this.isFavorite;
    if ((UnityEngine.Object) this.favIcon != (UnityEngine.Object) null)
    {
      this.favIcon.SetActive(this.isFavorite);
      this.maildata.isFavorite = this.isFavorite;
    }
    PlayerData.inst.mail.TagFavToMail(this.mailID, this.isFavorite, (int) this.maildata.category);
    this.UpdateIconState();
  }

  protected void GoToTargetPlace(int k, int x, int y)
  {
    if (!MapUtils.CanGotoTarget(k))
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    }
    else
    {
      PVPMap.PendingGotoRequest = new Coordinate(k, x, y);
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
        CitadelSystem.inst.OnLoadWorldMap();
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }

  public void OnDeleteBtnClicked()
  {
    string str1 = ScriptLocalization.Get("mail_delete_confirm", true);
    string str2 = ScriptLocalization.Get("delete_mail_choice_yes", true);
    string str3 = ScriptLocalization.Get("delete_mail_choice_no", true);
    string str4 = ScriptLocalization.Get("id_uppercase_confirm", true);
    UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
    {
      setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
      title = str4,
      leftButtonText = str3,
      leftButtonClickEvent = (System.Action) null,
      closeButtonCallbackEvent = (System.Action) null,
      rightButtonText = str2,
      rightButtonClickEvent = new System.Action(this.ConfirmDeleteCallBack),
      description = str1
    });
  }

  public virtual void OnShareBtnClicked()
  {
    if (PlayerData.inst.allianceId == 0L)
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_battle_report_share_no_alliance", true), (System.Action) null, 4f, true);
      this.canShare = false;
    }
    else
    {
      int num1 = PlayerPrefsEx.GetInt("SharedMail", 0);
      int num2 = num1 != 0 ? NetServerTime.inst.ServerTimestamp - num1 : 61;
      if (num2 > 60)
      {
        UIManager.inst.ShowBasicToast("toast_battle_report_share_success");
        PlayerPrefsEx.SetInt("SharedMail", NetServerTime.inst.ServerTimestamp);
        this.canShare = true;
      }
      else
      {
        UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_battle_report_share_wait", new Dictionary<string, string>()
        {
          {
            "0",
            ((60 - num2) / 60 + 1).ToString()
          }
        }, true), (System.Action) null, 4f, true);
        this.canShare = false;
      }
    }
  }

  private void ConfirmDeleteCallBack()
  {
    if (this.maildata.isFavorite)
    {
      this.OnBookmarkMail();
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    }
    else
      PlayerData.inst.mail.DeleteMailList((int) this.maildata.category, new List<AbstractMailEntry>()
      {
        this.maildata
      }, new System.Action<bool>(this.OnDeleteFavCallBack));
  }

  private void OnDeleteFavCallBack(bool notDeleteFully)
  {
    if (notDeleteFully)
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("embassy_uppercase_notice"),
        Content = Utils.XLAT("mail_delete_attachments_not_deleted_notice"),
        Okay = "OKAY"
      });
    else
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  protected virtual void UpdateIconState()
  {
    if ((UnityEngine.Object) this.favIcon != (UnityEngine.Object) null)
      this.favIcon.SetActive(this.isFavorite && !this.maildata.isShared);
    if ((UnityEngine.Object) this.favBaseIcon != (UnityEngine.Object) null)
      this.favBaseIcon.SetActive(this.maildata.category != MailCategory.System && !this.maildata.isShared);
    if ((UnityEngine.Object) this.delIcon != (UnityEngine.Object) null)
      this.delIcon.SetActive(!this.isFavorite && !this.maildata.isShared);
    if ((UnityEngine.Object) this.shareIcon != (UnityEngine.Object) null)
      this.shareIcon.SetActive(!this.maildata.isShared && this.maildata.type != MailType.MAIL_TYPE_MERLIN_TOWER_GATHER_DEFEND_LOSE);
    if ((UnityEngine.Object) this.time != (UnityEngine.Object) null)
      this.time.text = Utils.FormatTimeForMail(this.mailtime);
    if ((UnityEngine.Object) this.coordinate != (UnityEngine.Object) null && this.maildata is WarMessage)
      this.coordinate.text = (this.maildata as WarMessage).Cordinate();
    if ((UnityEngine.Object) this.title != (UnityEngine.Object) null)
      this.title.text = Utils.RemoveBBCode(this.maildata.GetTitleString());
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.mailcontent)
      this.mailcontent.text = this.maildata.GetBodyString();
    this.UpdateBtnState();
  }

  public void CollectRewardsBase()
  {
    Hashtable hashtable = this.param.mail.data[(object) "attachment"] as Hashtable;
    if (hashtable == null)
      return;
    RewardData rewardData = JsonReader.Deserialize<RewardData>(Utils.Object2Json((object) hashtable));
    List<IconData> iconDataList = new List<IconData>();
    if (rewardData.item != null)
    {
      RewardsCollectionAnimator.Instance.Clear();
      using (Dictionary<string, int>.Enumerator enumerator = rewardData.item.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int num1 = int.Parse(current.Key);
          int num2 = current.Value;
          RewardsCollectionAnimator.Instance.items2.Add(new Item2RewardInfo.Data()
          {
            itemid = num1,
            count = (float) num2
          });
        }
      }
    }
    RewardsCollectionAnimator.Instance.CollectItems2(false);
  }

  private void UpdateBtnState()
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) this.btnCollectionBase)
      return;
    if (this.param.mail.attachment_status == 2)
      this.btnCollectionBase.GetComponentInChildren<UIButton>().isEnabled = false;
    else
      this.btnCollectionBase.GetComponentInChildren<UIButton>().isEnabled = true;
  }

  public void OnClaimRewardClicked()
  {
    this.CollectRewardsBase();
    AudioManager.Instance.PlaySound("sfx_mail_reward", false);
    PlayerData.inst.mail.API.GainAttachment((int) this.maildata.category, this.param.mail.mailID, new System.Action<bool, object>(this.GainAttachmentCallBack));
  }

  private void GainAttachmentCallBack(bool ok, object obj)
  {
    if (!ok)
      return;
    this.param.mail.attachment_status = 2;
    this.UpdateBtnState();
  }

  protected IconListComponentData AnalyzeRewards(RewardData rd)
  {
    if (rd == null)
      return (IconListComponentData) null;
    List<IconData> data = new List<IconData>();
    if (rd.item != null)
    {
      using (Dictionary<string, int>.Enumerator enumerator = rd.item.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int internalId = int.Parse(current.Key);
          int num = current.Value;
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
          data.Add(new IconData(itemStaticInfo.ImagePath, new string[2]
          {
            ScriptLocalization.Get(itemStaticInfo.Loc_Name_Id, true),
            "X" + num.ToString()
          })
          {
            Data = (object) itemStaticInfo.internalId
          });
        }
      }
    }
    if (rd.gold != 0)
    {
      IconData iconData = new IconData("Texture/BuildingConstruction/gold_coin", new string[2]
      {
        ScriptLocalization.Get("id_gold", true),
        "+" + rd.gold.ToString()
      });
      data.Add(iconData);
    }
    if (rd.resource != null)
    {
      using (Dictionary<string, int>.Enumerator enumerator = rd.resource.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int num = current.Value;
          IconData iconData = new IconData("Texture/BuildingConstruction/" + current.Key + "_icon", new string[2]
          {
            ScriptLocalization.Get("id_" + current.Key, true),
            "+" + num.ToString()
          });
          data.Add(iconData);
        }
      }
    }
    if (rd.fund != 0L)
    {
      IconData iconData = new IconData("Texture/Alliance/alliance_fund", new string[2]
      {
        ScriptLocalization.Get("id_fund", true),
        "+" + rd.fund.ToString()
      });
      data.Add(iconData);
    }
    if (rd.honor != 0L)
    {
      IconData iconData = new IconData("Texture/Alliance/alliance_honor", new string[2]
      {
        ScriptLocalization.Get("id_honor", true),
        "+" + rd.honor.ToString()
      });
      data.Add(iconData);
    }
    if (rd.kingdom_coin != 0L)
    {
      IconData iconData = new IconData("Texture/Alliance/kingdom_coin", new string[2]
      {
        ScriptLocalization.Get("id_kingdom_coin", true),
        "+" + rd.kingdom_coin.ToString()
      });
      data.Add(iconData);
    }
    return new IconListComponentData(data);
  }

  protected IconListComponentData AnalyzeCollectedRewards()
  {
    Hashtable hashtable = this.param.mail.data[(object) "attachment"] as Hashtable;
    if (hashtable == null)
      return (IconListComponentData) null;
    return this.AnalyzeRewards(JsonReader.Deserialize<RewardData>(Utils.Object2Json((object) hashtable)));
  }

  private void Init()
  {
    this.mailtime = this.param.mail.timeAsInt;
    this.mailID = this.param.mail.mailID;
    this.maildata = this.param.mail;
    this.isFavorite = this.param.mail.isFavorite;
    this.UpdateIconState();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.param = orgParam as BaseReportPopup.Parameter;
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.headerTexture)
      BuilderFactory.Instance.Release((UIWidget) this.headerTexture);
    base.OnHide(orgParam);
  }

  public class Parameter : Popup.PopupParameter
  {
    public Hashtable hashtable;
    public AbstractMailEntry mail;
  }
}
