﻿// Decompiled with JetBrains decompiler
// Type: WarOverview
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarOverview : MonoBehaviour
{
  private List<ParliamentHeroItem> _AllParliamentHeroItem = new List<ParliamentHeroItem>();
  private const float parliaGapX = 30f;
  public GameObject parliaContainer;
  public UILabel playername;
  public UITexture icon;
  public UILabel position;
  public UILabel powerlost;
  public DragonSkillForWarReportComponent dsc;
  public UILabel nodragonlbl;
  public UILabel nolegendlbl;
  public UILabel trooplost;

  public void SeedData(WarOverview.Data d)
  {
    this.playername.text = d.name;
    if (d.isMerlinTrials || d.isMerlinTower)
      BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, d.icon, (System.Action<bool>) null, true, true, string.Empty);
    else if (!d.isNpc)
    {
      CustomIconLoader.Instance.requestCustomIcon(this.icon, d.icon, d.setIcon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.icon, d.lordTitleId, 2);
    }
    else
      BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, "Texture/Hero/Portrait_Icon/player_anonymous", (System.Action<bool>) null, true, false, string.Empty);
    this.position.text = " X: " + d.x + " Y: " + d.y;
    if (string.IsNullOrEmpty(d.x) && string.IsNullOrEmpty(d.y))
      NGUITools.SetActive(this.position.gameObject, false);
    this.powerlost.text = "-" + d.powerlost.ToString();
    if (d.dsht != null)
    {
      this.dsc.gameObject.SetActive(true);
      this.dsc.FeedData<DragonSkillForWarReportComponentData>(d.dsht);
      this.nodragonlbl.gameObject.SetActive(false);
    }
    else
    {
      this.dsc.gameObject.SetActive(false);
      this.nodragonlbl.gameObject.SetActive(true);
    }
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.parliaContainer)
    {
      if (d.mylegend != null && d.mylegend.Count > 0)
      {
        this.parliaContainer.gameObject.SetActive(true);
        this.CreateParliament(d.mylegend);
        this.nolegendlbl.gameObject.SetActive(false);
      }
      else
      {
        this.parliaContainer.gameObject.SetActive(false);
        this.nolegendlbl.gameObject.SetActive(true);
      }
    }
    this.trooplost.text = d.trooplost.ToString();
  }

  public void CreateParliament(List<Hashtable> myLegend)
  {
    GameObject _HeroBookItemTemplate = AssetManager.Instance.HandyLoad("Prefab/UI/Common/ParliamentHeroItem", (System.Type) null) as GameObject;
    int count = 0;
    using (List<Hashtable>.Enumerator enumerator = myLegend.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Hashtable current = enumerator.Current;
        ParliamentHeroItem heroBookItem = this.CreateHeroBookItem(_HeroBookItemTemplate, count);
        LegendCardData legendCardData = new LegendCardData();
        legendCardData.Decode((object) current, 0L);
        heroBookItem.SetData(legendCardData, true, false);
        heroBookItem.ShowlabelLevel(true);
        ++count;
      }
    }
  }

  protected ParliamentHeroItem CreateHeroBookItem(GameObject _HeroBookItemTemplate, int count)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(_HeroBookItemTemplate);
    ParliamentHeroItem component = gameObject.GetComponent<ParliamentHeroItem>();
    this._AllParliamentHeroItem.Add(component);
    gameObject.transform.SetParent(this.parliaContainer.transform);
    gameObject.transform.localPosition = Vector3.zero + Vector3.right * ((float) gameObject.GetComponent<UIWidget>().width + 30f) * (float) count;
    gameObject.transform.localScale = Vector3.one;
    return component;
  }

  public void DestroyAllHeroBookItem()
  {
    using (List<ParliamentHeroItem>.Enumerator enumerator = this._AllParliamentHeroItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ParliamentHeroItem current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current.gameObject))
        {
          current.gameObject.transform.SetParent((Transform) null);
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
        }
      }
    }
    BuilderFactory.Instance.Release((UIWidget) this.icon);
    this._AllParliamentHeroItem.Clear();
  }

  public class Data
  {
    public string name;
    public string icon;
    public bool isNpc;
    public bool isMerlinTrials;
    public bool isMerlinTower;
    public string setIcon;
    public int lordTitleId;
    public string k;
    public string x;
    public string y;
    public long powerlost;
    public long trooplost;
    public Hashtable dsht;
    public List<Hashtable> mylegend;
  }
}
