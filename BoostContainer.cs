﻿// Decompiled with JetBrains decompiler
// Type: BoostContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class BoostContainer : MonoBehaviour
{
  private BetterList<BoostMenuItem> _items = new BetterList<BoostMenuItem>();
  private BetterList<GameObject> lines = new BetterList<GameObject>();
  private GameObjectPool pools = new GameObjectPool();
  public BoostMenuItem itemPrefab;
  public UILabel title;
  private BoostContainerData _data;
  public System.Action OnRepositionHandler;
  private bool isAdd;
  public GameObject itemRoot;
  public UIGrid grid;

  private void Awake()
  {
    this.pools.Initialize(this.itemPrefab.gameObject, this.itemRoot);
  }

  public void SetData(BoostContainerData data)
  {
    this._data = data;
    this.UpdateUI();
  }

  public void OnClickHandler()
  {
  }

  private void UpdateUI()
  {
    this.title.text = this._data.Type != BoostCategory.alliancetemple ? ScriptLocalization.Get("boost_category_" + this._data.Type.ToString(), true) : ScriptLocalization.Get("boost_category_spells", true);
    this.Clear();
    List<BuffUIData> subBoost = this._data.GetSubBoost();
    this.itemPrefab.gameObject.SetActive(true);
    int num = 0;
    using (List<BuffUIData>.Enumerator enumerator = subBoost.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BuffUIData current = enumerator.Current;
        BoostMenuItem boostMenuItem = this.pools.AddChild(this.grid.gameObject).AddMissingComponent<BoostMenuItem>();
        boostMenuItem.SetBoostItem(current, this._data);
        boostMenuItem.ower = this;
        ++num;
        boostMenuItem.SetLineVisiable(num != subBoost.Count);
        this._items.Add(boostMenuItem);
      }
    }
    this.itemPrefab.gameObject.SetActive(false);
    this.grid.Reposition();
    this.grid.onReposition += new UIGrid.OnReposition(this.OnRestComplete);
  }

  private void OnRestComplete()
  {
    this.grid.onReposition -= new UIGrid.OnReposition(this.OnRestComplete);
    if (this.OnRepositionHandler == null)
      return;
    this.OnRepositionHandler();
  }

  public void Clear()
  {
    foreach (BoostMenuItem boostMenuItem in this._items)
    {
      boostMenuItem.gameObject.SetActive(false);
      boostMenuItem.Clear();
      this.pools.Release(boostMenuItem.gameObject);
    }
    this.lines.Clear();
    this._items.Clear();
  }
}
