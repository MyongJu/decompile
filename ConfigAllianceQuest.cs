﻿// Decompiled with JetBrains decompiler
// Type: ConfigAllianceQuest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAllianceQuest
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<int, QuestInfo> cache = new Dictionary<int, QuestInfo>();
  public Dictionary<int, AllianceQuestInfo> datas;
  private Dictionary<string, AllianceQuestInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<AllianceQuestInfo, string>(res as Hashtable, "ID", out this.dicByUniqueId, out this.datas);
  }

  public bool Contains(int questId)
  {
    return this.datas.ContainsKey(questId);
  }

  public AllianceQuestInfo Get(int questId)
  {
    if (!this.Contains(questId))
      return (AllianceQuestInfo) null;
    return this.datas[questId];
  }

  public void CreateFakeData(int questId)
  {
    AllianceQuestInfo allianceQuestInfo = new AllianceQuestInfo();
    allianceQuestInfo.internalId = questId;
    allianceQuestInfo.ID = "quest_empire_legend_recruit_1";
    allianceQuestInfo.Description = "quest_empire_legend_recruit_1_name";
    allianceQuestInfo.Image = "icon_legend";
    allianceQuestInfo.Ore_Payout = 1000;
    allianceQuestInfo.Duration_Time = 36000;
    allianceQuestInfo.Image = "icon_quest_alliance";
    allianceQuestInfo.Hero_XP = 1000;
    allianceQuestInfo.Alliance_XP = 1000;
    allianceQuestInfo.Alliance_Fund = 1000;
    allianceQuestInfo.Alliance_Honor = 1000;
    if (this.datas == null)
      this.datas = new Dictionary<int, AllianceQuestInfo>();
    if (this.datas.ContainsKey(questId))
      return;
    this.datas.Add(questId, allianceQuestInfo);
  }

  public QuestInfo GetQuestInfo(int questId)
  {
    if (!this.Contains(questId))
      return (QuestInfo) null;
    if (this.cache.ContainsKey(questId))
      return this.cache[questId];
    AllianceQuestInfo data = this.datas[questId];
    QuestInfo questInfo = new QuestInfo();
    questInfo.Description = data.Description;
    questInfo.ID = data.ID;
    questInfo.internalId = data.internalId;
    questInfo.Image = data.Image;
    questInfo.Food_Payout = data.Food_Payout;
    questInfo.Wood_Payout = data.Wood_Payout;
    questInfo.Ore_Payout = data.Ore_Payout;
    questInfo.Silver_Payout = data.Silver_Payout;
    questInfo.Reward_ID_1 = data.Reward_ID_1;
    questInfo.Reward_Value_1 = data.Reward_Value_1;
    questInfo.Hero_XP = data.Hero_XP;
    questInfo.Alliance_XP = data.Alliance_XP;
    questInfo.Honor = data.Alliance_Honor;
    questInfo.Alliance_Fund = data.Alliance_Fund;
    questInfo.Duration_Time = data.Duration_Time;
    questInfo.Category = "legend_quest";
    this.cache[questId] = questInfo;
    return questInfo;
  }
}
