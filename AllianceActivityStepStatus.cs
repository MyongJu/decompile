﻿// Decompiled with JetBrains decompiler
// Type: AllianceActivityStepStatus
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceActivityStepStatus : MonoBehaviour
{
  public UILabel score;
  public GameObject normal;
  public GameObject achieved;

  public void SetData(int targetScore, long currentScore, string content)
  {
    this.score.text = content;
    if (currentScore < (long) targetScore)
    {
      this.score.color = Color.red;
      NGUITools.SetActive(this.normal, true);
      NGUITools.SetActive(this.achieved, false);
    }
    else
    {
      this.score.color = Color.green;
      NGUITools.SetActive(this.normal, false);
      NGUITools.SetActive(this.achieved, true);
    }
  }
}
