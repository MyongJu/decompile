﻿// Decompiled with JetBrains decompiler
// Type: WaveAnimation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WaveAnimation : MonoBehaviour
{
  public float slideMin = -0.1f;
  public float slideMax = 0.4f;
  public float slideSpeed = 0.5f;
  public float slideSharpness = 1f;
  public float scaleMin = 1f;
  public float scaleMax = 0.4f;
  public float scaleSpeed = 0.5f;
  public float scaleSharpness = 0.5f;
  private float fade = 1f;
  public float index;
  public float offset;
  public float fadeSpeed;
  private Material theMaterial;
  private float slide;
  private float slideInertia;
  private float scale;
  private float scaleInertia;
  private Vector3 texScale;
  private float lastSlide;
  private Color color;

  private void Start()
  {
    this.theMaterial = this.GetComponent<Renderer>().material;
    this.color = this.theMaterial.GetColor("_Color");
    this.texScale = (Vector3) this.theMaterial.GetTextureScale("_MainTex");
  }

  private void Update()
  {
    this.slideInertia = Mathf.Lerp(this.slideInertia, Mathf.PingPong(Time.time * this.scaleSpeed + this.offset, 1f), this.slideSharpness * Time.deltaTime);
    this.slide = Mathf.Lerp(this.slide, this.slideInertia, this.slideSharpness * Time.deltaTime);
    this.theMaterial.SetTextureOffset("_MainTex", (Vector2) new Vector3(this.index * 0.35f, Mathf.Lerp(this.slideMin, this.slideMax, this.slide) * 2f, 0.0f));
    this.theMaterial.SetTextureOffset("_Cutout", (Vector2) new Vector3(this.index * 0.79f, Mathf.Lerp(this.slideMin, this.slideMax, this.slide) / 2f, 0.0f));
    this.fade = Mathf.Lerp(this.fade, (double) this.slide - (double) this.lastSlide <= 0.0 ? 0.0f : 1f, Time.deltaTime * this.fadeSpeed);
    this.lastSlide = this.slide;
    Color color = this.color;
    color.a = 0.0f;
    this.theMaterial.SetColor("_Color", Color.Lerp(color, this.color, this.fade));
    this.scaleInertia = Mathf.Lerp(this.scaleInertia, Mathf.PingPong(Time.time * this.scaleSpeed + this.offset, 1f), this.scaleSharpness * Time.deltaTime);
    this.scale = Mathf.Lerp(this.scale, this.scaleInertia, this.scaleSharpness * Time.deltaTime);
    this.theMaterial.SetTextureScale("_MainTex", (Vector2) new Vector3(this.texScale.x, Mathf.Lerp(this.scaleMin, this.scaleMax, this.scale), this.texScale.z));
  }
}
