﻿// Decompiled with JetBrains decompiler
// Type: TimerRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TimerRenderer : MonoBehaviour
{
  private const MarchData.MarchState GOTO_TROOPS_STATE = MarchData.MarchState.marching | MarchData.MarchState.returning;
  public UILabel m_Duration;
  public UILabel m_Description;
  public UISlider m_Progress;
  public UISprite m_Icon;
  public UISprite m_DragonIcon;
  public UIButton m_Return;
  public UIButton m_Speedup;
  public UIButton m_View;
  private JobHandle m_Job;

  public void SetData(JobHandle job)
  {
    this.m_Job = job;
    this.m_Description.text = TimerRenderer.GetTimerDescription(this.m_Job);
    this.m_Progress.value = 0.0f;
    this.OnUpdate(NetServerTime.inst.ServerTimestamp);
  }

  public JobHandle Job
  {
    get
    {
      return this.m_Job;
    }
  }

  public void OnSpeedup()
  {
    Hashtable data = this.m_Job.Data as Hashtable;
    MarchSpeedUpPopup.Parameter parameter = new MarchSpeedUpPopup.Parameter();
    long id = long.Parse(data[(object) "marchId"].ToString());
    parameter.marchData = DBManager.inst.DB_March.Get(id);
    UIManager.inst.OpenPopup("MarchSpeedUpPopup", (Popup.PopupParameter) parameter);
  }

  public void OnReturn()
  {
    UIManager.inst.CloseKingdomTouchCircle();
    GameEngine.Instance.marchSystem.Recall(long.Parse((this.m_Job.Data as Hashtable)[(object) "marchId"].ToString()));
  }

  public void OnView()
  {
    PlayerData.inst.allianceWarManager.loadDatasCallback += new System.Action(this.OnLoadData);
    if (PlayerData.inst.allianceWarManager.LoadDatas())
      return;
    this.OnLoadData();
  }

  private void OnLoadData()
  {
    long num = 0;
    if (this.m_Job == null)
      return;
    Hashtable data = this.m_Job.Data as Hashtable;
    MarchData marchData = data[(object) "marchData"] as MarchData;
    if (marchData != null)
      num = marchData.rallyId;
    else if (data.ContainsKey((object) "rally_id"))
      num = (long) data[(object) "rally_id"];
    if (num == 0L)
      return;
    UIManager.inst.OpenDlg("Alliance/AllianceRallyDetialDlg", (UI.Dialog.DialogParameter) new AllianceRallyDetailPopUp.Parameter()
    {
      data_id = num
    }, 1 != 0, 1 != 0, 1 != 0);
    PlayerData.inst.allianceWarManager.loadDatasCallback -= new System.Action(this.OnLoadData);
  }

  public void OnGoto()
  {
    MarchData marchData = (this.m_Job.Data as Hashtable)[(object) "marchData"] as MarchData;
    if (marchData == null)
      return;
    if ((marchData.state & (MarchData.MarchState.marching | MarchData.MarchState.returning)) > MarchData.MarchState.invalid)
      LinkerHub.Instance.LHGotoTroop(new LPTroop()
      {
        marchId = marchData.marchId
      });
    else
      LinkerHub.Instance.LHGotoTile(new LPTile()
      {
        k = marchData.targetLocation.K,
        x = marchData.targetLocation.X,
        y = marchData.targetLocation.Y
      });
  }

  public void OnUpdate(int serverTimeStamp)
  {
    if (!this.m_Job.IsFinished() || this.m_Job.GetJobEvent() == JobEvent.JOB_PVP_ENCAMPING || this.m_Job.GetJobEvent() == JobEvent.JOB_PVP_HODING_WONDER)
    {
      this.m_Progress.value = TimerRenderer.GetTimerProgress(this.m_Job);
      this.m_Duration.text = TimerRenderer.GetTimerClock(this.m_Job);
      string timerDescription = TimerRenderer.GetTimerDescription(this.m_Job);
      this.m_Description.text = !string.IsNullOrEmpty(timerDescription) ? timerDescription : this.m_Description.text;
      string timerSpriteName = TimerRenderer.GetTimerSpriteName(this.m_Job);
      this.m_Icon.spriteName = !string.IsNullOrEmpty(timerSpriteName) ? timerSpriteName : this.m_Icon.spriteName;
      Color color;
      this.m_Progress.foregroundWidget.color = !TimerRenderer.GetTimerColor(this.m_Job, out color) ? this.m_Progress.foregroundWidget.color : color;
      this.UpdateButtons();
      this.m_DragonIcon.gameObject.SetActive(TimerRenderer.IsDragonIn(this.m_Job));
    }
    else
    {
      this.m_Progress.value = 1f;
      this.m_Duration.text = Utils.FormatTime(0, false, false, true);
      this.DisableButtons();
    }
  }

  private void UpdateButtons()
  {
    switch (this.m_Job.GetJobEvent())
    {
      case JobEvent.JOB_PVP_MARCHING:
      case JobEvent.JOB_PVP_RETURNING:
        MarchData marchData = (this.m_Job.Data as Hashtable)[(object) "marchData"] as MarchData;
        if (marchData == null)
          break;
        switch (marchData.type)
        {
          case MarchData.MarchType.rally_attack:
          case MarchData.MarchType.gve_rally_attack:
          case MarchData.MarchType.rab_rally_attack:
            this.m_Return.gameObject.SetActive(false);
            this.m_Speedup.gameObject.SetActive(false);
            this.m_View.gameObject.SetActive(true);
            this.m_View.isEnabled = true;
            return;
          default:
            this.m_Return.gameObject.SetActive(false);
            this.m_Speedup.gameObject.SetActive(true);
            this.m_View.gameObject.SetActive(false);
            this.m_Speedup.isEnabled = true;
            return;
        }
      case JobEvent.JOB_PVP_STAY:
        this.DisableButtons();
        break;
      case JobEvent.JOB_PVP_WAIT_RALLY:
        this.m_Return.gameObject.SetActive(false);
        this.m_Speedup.gameObject.SetActive(false);
        this.m_View.gameObject.SetActive(true);
        this.m_View.isEnabled = true;
        break;
      case JobEvent.JOB_PVP_GATHERING:
      case JobEvent.JOB_PVP_ENCAMPING:
      case JobEvent.JOB_PVP_HODING_WONDER:
      case JobEvent.JOB_PVP_DEFENDING_FORTRESS:
      case JobEvent.JOB_PVP_DEMOLISHING_FORTRESS:
      case JobEvent.JOB_PVP_BUILDING:
      case JobEvent.JOB_PVP_DIGGING:
        this.m_Return.gameObject.SetActive(true);
        this.m_Speedup.gameObject.SetActive(false);
        this.m_View.gameObject.SetActive(false);
        this.m_Return.isEnabled = true;
        break;
      case JobEvent.JOB_PVP_REINFORCING:
        this.m_Return.gameObject.SetActive(true);
        this.m_Speedup.gameObject.SetActive(false);
        this.m_View.gameObject.SetActive(false);
        this.m_Return.isEnabled = true;
        break;
    }
  }

  private void DisableButtons()
  {
    this.m_Return.isEnabled = false;
    this.m_Speedup.isEnabled = false;
    this.m_View.isEnabled = false;
  }

  private static float GetTimerProgress(JobHandle job)
  {
    int num1 = job.Duration();
    int num2 = job.LeftTime();
    int num3 = num1 - num2;
    if (job.GetJobEvent() == JobEvent.JOB_PVP_ENCAMPING || job.GetJobEvent() == JobEvent.JOB_PVP_BUILDING || (job.GetJobEvent() == JobEvent.JOB_PVP_HODING_WONDER || job.GetJobEvent() == JobEvent.JOB_PVP_REINFORCING) || (job.GetJobEvent() == JobEvent.JOB_PVP_DEFENDING_FORTRESS || job.GetJobEvent() == JobEvent.JOB_PVP_DEMOLISHING_FORTRESS))
      return 0.0f;
    if (num2 >= 0)
      return (float) num3 / (float) num1;
    return 1f;
  }

  private static string GetTimerDescription(JobHandle job)
  {
    if ((bool) job)
    {
      switch (job.GetJobEvent())
      {
        case JobEvent.JOB_PVP_MARCHING:
          return "To " + ((job.Data as Hashtable)[(object) "marchData"] as MarchData).targetLocation.ToShortString();
        case JobEvent.JOB_PVP_WAIT_RALLY:
          return Utils.XLAT("time_bar_uppercase_rally");
        case JobEvent.JOB_PVP_RETURNING:
          return Utils.XLAT("time_bar_uppercase_returning");
        case JobEvent.JOB_PVP_GATHERING:
          return Utils.XLAT("time_bar_uppercase_gathering");
        case JobEvent.JOB_PVP_ENCAMPING:
          return Utils.XLAT("time_bar_uppercase_encamping");
        case JobEvent.JOB_PVP_HODING_WONDER:
          return Utils.XLAT("time_bar_uppercase_capturing");
        case JobEvent.JOB_PVP_REINFORCING:
          return Utils.XLAT("time_bar_uppercase_reinforcing");
        case JobEvent.JOB_PVP_DEFENDING_FORTRESS:
          Hashtable data = job.Data as Hashtable;
          if (data != null && data.ContainsKey((object) "fortress"))
          {
            AllianceFortressData allianceFortressData = data[(object) "fortress"] as AllianceFortressData;
            if (allianceFortressData != null)
            {
              string state = allianceFortressData.State;
              if (state != null)
              {
                // ISSUE: reference to a compiler-generated field
                if (TimerRenderer.\u003C\u003Ef__switch\u0024mapAB == null)
                {
                  // ISSUE: reference to a compiler-generated field
                  TimerRenderer.\u003C\u003Ef__switch\u0024mapAB = new Dictionary<string, int>(3)
                  {
                    {
                      "defending",
                      0
                    },
                    {
                      "building",
                      1
                    },
                    {
                      "repairing",
                      2
                    }
                  };
                }
                int num;
                // ISSUE: reference to a compiler-generated field
                if (TimerRenderer.\u003C\u003Ef__switch\u0024mapAB.TryGetValue(state, out num))
                {
                  switch (num)
                  {
                    case 0:
                      return Utils.XLAT("time_bar_uppercase_garrisoned");
                    case 1:
                      return Utils.XLAT("time_bar_uppercase_building");
                    case 2:
                      return Utils.XLAT("time_bar_uppercase_repairing");
                  }
                }
              }
            }
            else
            {
              AllianceTempleData allianceTempleData = DBManager.inst.DB_AllianceTemple.GetMyAllianceTempleData();
              if (allianceTempleData != null)
              {
                string state = allianceTempleData.State;
                if (state != null)
                {
                  // ISSUE: reference to a compiler-generated field
                  if (TimerRenderer.\u003C\u003Ef__switch\u0024mapAC == null)
                  {
                    // ISSUE: reference to a compiler-generated field
                    TimerRenderer.\u003C\u003Ef__switch\u0024mapAC = new Dictionary<string, int>(3)
                    {
                      {
                        "defending",
                        0
                      },
                      {
                        "building",
                        1
                      },
                      {
                        "repairing",
                        2
                      }
                    };
                  }
                  int num;
                  // ISSUE: reference to a compiler-generated field
                  if (TimerRenderer.\u003C\u003Ef__switch\u0024mapAC.TryGetValue(state, out num))
                  {
                    switch (num)
                    {
                      case 0:
                        return Utils.XLAT("time_bar_uppercase_garrisoned");
                      case 1:
                        return Utils.XLAT("time_bar_uppercase_building");
                      case 2:
                        return Utils.XLAT("time_bar_uppercase_repairing");
                    }
                  }
                }
              }
            }
          }
          return Utils.XLAT("time_bar_uppercase_garrisoned");
        case JobEvent.JOB_PVP_DEMOLISHING_FORTRESS:
          return Utils.XLAT("time_bar_uppercase_demolishing");
        case JobEvent.JOB_PVP_BUILDING:
          return Utils.XLAT("time_bar_uppercase_building");
        case JobEvent.JOB_PVP_DIGGING:
          return Utils.XLAT("time_bar_uppercase_digging");
      }
    }
    return string.Empty;
  }

  private static string GetTimerSpriteName(JobHandle job)
  {
    switch (job.GetJobEvent())
    {
      case JobEvent.JOB_PVP_MARCHING:
        switch (((job.Data as Hashtable)[(object) "marchData"] as MarchData).type)
        {
          case MarchData.MarchType.trade:
            return "icon_hud_trading";
          case MarchData.MarchType.reinforce:
            return "icon_hud_reinforcing";
          default:
            return "icon_hud_marching";
        }
      case JobEvent.JOB_PVP_WAIT_RALLY:
        return "icon_hud_rallying";
      case JobEvent.JOB_PVP_RETURNING:
        return "icon_hud_returning";
      case JobEvent.JOB_PVP_GATHERING:
        return "icon_hud_gathering";
      case JobEvent.JOB_PVP_ENCAMPING:
      case JobEvent.JOB_PVP_DIGGING:
        return "icon_hud_encamping";
      case JobEvent.JOB_PVP_HODING_WONDER:
        return "icon_hud_mini_wonder";
      case JobEvent.JOB_PVP_REINFORCING:
      case JobEvent.JOB_PVP_DEFENDING_FORTRESS:
      case JobEvent.JOB_PVP_DEMOLISHING_FORTRESS:
      case JobEvent.JOB_PVP_BUILDING:
        return "icon_hud_reinforcing";
      default:
        return string.Empty;
    }
  }

  private static string GetTimerClock(JobHandle job)
  {
    if (job.GetJobEvent() == JobEvent.JOB_PVP_ENCAMPING || job.GetJobEvent() == JobEvent.JOB_PVP_BUILDING || (job.GetJobEvent() == JobEvent.JOB_PVP_HODING_WONDER || job.GetJobEvent() == JobEvent.JOB_PVP_REINFORCING) || (job.GetJobEvent() == JobEvent.JOB_PVP_DEFENDING_FORTRESS || job.GetJobEvent() == JobEvent.JOB_PVP_DEMOLISHING_FORTRESS))
      return ((job.Data as Hashtable)[(object) "marchData"] as MarchData).targetLocation.ToShortString();
    return Utils.FormatTime(job.LeftTime(), false, false, true);
  }

  private static bool IsDragonIn(JobHandle job)
  {
    Hashtable data = job.Data as Hashtable;
    if (data == null)
      return false;
    MarchData marchData = data[(object) "marchData"] as MarchData;
    if (marchData == null)
      return false;
    return marchData.dragon.isDragonIn;
  }

  private static bool GetTimerColor(JobHandle job, out Color color)
  {
    color = Color.white;
    switch (job.GetJobEvent())
    {
      case JobEvent.JOB_PVP_MARCHING:
      case JobEvent.JOB_PVP_WAIT_RALLY:
      case JobEvent.JOB_PVP_RETURNING:
      case JobEvent.JOB_PVP_ENCAMPING:
      case JobEvent.JOB_PVP_HODING_WONDER:
      case JobEvent.JOB_PVP_REINFORCING:
      case JobEvent.JOB_PVP_BUILDING:
        color = new Color(0.2196078f, 0.7372549f, 0.9921569f, 1f);
        return true;
      case JobEvent.JOB_PVP_GATHERING:
      case JobEvent.JOB_PVP_DIGGING:
        color = new Color(0.509804f, 1f, 0.2941177f, 1f);
        return true;
      default:
        return false;
    }
  }
}
