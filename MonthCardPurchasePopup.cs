﻿// Decompiled with JetBrains decompiler
// Type: MonthCardPurchasePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;

public class MonthCardPurchasePopup : Popup
{
  public List<MonthCardPurchaseItem> purchaseItems = new List<MonthCardPurchaseItem>(3);

  private void Init()
  {
    Dictionary<long, IAPMonthCardInfo> allIapMonthCard = ConfigManager.inst.DB_IAPMonthCard.GetAllIAPMonthCard();
    int index = 0;
    using (Dictionary<long, IAPMonthCardInfo>.Enumerator enumerator = allIapMonthCard.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<long, IAPMonthCardInfo> current = enumerator.Current;
        this.purchaseItems[index].Init(current.Key);
        this.purchaseItems[index].onPurchaseSuccessed = (System.Action) null;
        this.purchaseItems[index].onPurchaseSuccessed += new System.Action(this.OnCloseBtnPressed);
        ++index;
      }
    }
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }
}
