﻿// Decompiled with JetBrains decompiler
// Type: Preloader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;

public class Preloader
{
  private ArrayList _localVersion = new ArrayList()
  {
    (object) -1,
    (object) -1,
    (object) -1
  };
  private Hashtable configs = new Hashtable();
  private static Preloader _instance;
  private bool _fullLoad;
  private bool _isRestart;
  private ArrayList _remoteVersion;
  private Hashtable _localVerHt;
  private Hashtable _remoteVerHt;
  public Hashtable _localConfigVerHt;
  public Hashtable _remoteConfigVerHt;
  public Hashtable _localMapVerHt;
  public Hashtable _remoteMapVerHt;
  private StateMachine _stateMachine;
  private GatewayState gateway;
  private InitializeState initState;
  private GameVersionState gameVersionState;
  private StartUpFunPlusSDKState sdkState;
  private ConfigFilesVerListState configFilsState;
  private BundleInfoListStateNew bundleInfoState;
  private LoadBundlesState cacheBunldState;
  private LoadConfigFilesState loadConfigFileStae;
  private InitUserDataState initDataState;
  private BeforehandUIState preloadUIstate;
  private ConnectRTMState connectRTMState;
  private PreOpenBundleUpdateState preOpenBundleUpdateState;
  private OTABundleUpdateState otaBundleUpdateState;
  private PreLoginState preLoginState;
  private bool loadConfigsReady;

  public static Preloader inst
  {
    get
    {
      if (Preloader._instance == null)
        Preloader._instance = new Preloader();
      return Preloader._instance;
    }
  }

  public void Init()
  {
    this.Dispose();
    this._stateMachine = new StateMachine();
    this.loadConfigsReady = false;
    this.AddState1();
  }

  private void AddState1()
  {
    int num1 = 1;
    StateMachine stateMachine1 = this._stateMachine;
    int step1 = num1;
    int num2 = step1 + 1;
    GameVersionState gameVersionState = this.gameVersionState = new GameVersionState(step1);
    stateMachine1.AddState((IState) gameVersionState);
    StateMachine stateMachine2 = this._stateMachine;
    int step2 = num2;
    int num3 = step2 + 1;
    GatewayState gatewayState = this.gateway = new GatewayState(step2);
    stateMachine2.AddState((IState) gatewayState);
    StateMachine stateMachine3 = this._stateMachine;
    int step3 = num3;
    int num4 = step3 + 1;
    InitializeState initializeState = this.initState = new InitializeState(step3);
    stateMachine3.AddState((IState) initializeState);
    StateMachine stateMachine4 = this._stateMachine;
    int step4 = num4;
    int num5 = step4 + 1;
    StartUpFunPlusSDKState upFunPlusSdkState = this.sdkState = new StartUpFunPlusSDKState(step4);
    stateMachine4.AddState((IState) upFunPlusSdkState);
    StateMachine stateMachine5 = this._stateMachine;
    int step5 = num5;
    int num6 = step5 + 1;
    ConfigFilesVerListState filesVerListState = this.configFilsState = new ConfigFilesVerListState(step5);
    stateMachine5.AddState((IState) filesVerListState);
    StateMachine stateMachine6 = this._stateMachine;
    int step6 = num6;
    int num7 = step6 + 1;
    BundleInfoListStateNew infoListStateNew = this.bundleInfoState = new BundleInfoListStateNew(step6);
    stateMachine6.AddState((IState) infoListStateNew);
    StateMachine stateMachine7 = this._stateMachine;
    int step7 = num7;
    int num8 = step7 + 1;
    LoadBundlesState loadBundlesState = this.cacheBunldState = new LoadBundlesState(step7);
    stateMachine7.AddState((IState) loadBundlesState);
    StateMachine stateMachine8 = this._stateMachine;
    int step8 = num8;
    int num9 = step8 + 1;
    LoadConfigFilesState configFilesState = this.loadConfigFileStae = new LoadConfigFilesState(step8);
    stateMachine8.AddState((IState) configFilesState);
    StateMachine stateMachine9 = this._stateMachine;
    int step9 = num9;
    int num10 = step9 + 1;
    PreLoginState preLoginState = this.preLoginState = new PreLoginState(step9);
    stateMachine9.AddState((IState) preLoginState);
    StateMachine stateMachine10 = this._stateMachine;
    int step10 = num10;
    int num11 = step10 + 1;
    InitUserDataState initUserDataState = this.initDataState = new InitUserDataState(step10);
    stateMachine10.AddState((IState) initUserDataState);
    StateMachine stateMachine11 = this._stateMachine;
    int step11 = num11;
    int num12 = step11 + 1;
    ConnectRTMState connectRtmState = this.connectRTMState = new ConnectRTMState(step11);
    stateMachine11.AddState((IState) connectRtmState);
    StateMachine stateMachine12 = this._stateMachine;
    int step12 = num12;
    int num13 = step12 + 1;
    BeforehandUIState beforehandUiState = this.preloadUIstate = new BeforehandUIState(step12);
    stateMachine12.AddState((IState) beforehandUiState);
    StateMachine stateMachine13 = this._stateMachine;
    int step13 = num13;
    int num14 = step13 + 1;
    PreOpenBundleUpdateState bundleUpdateState1 = this.preOpenBundleUpdateState = new PreOpenBundleUpdateState(step13);
    stateMachine13.AddState((IState) bundleUpdateState1);
    StateMachine stateMachine14 = this._stateMachine;
    int step14 = num14;
    int num15 = step14 + 1;
    OTABundleUpdateState bundleUpdateState2 = this.otaBundleUpdateState = new OTABundleUpdateState(step14);
    stateMachine14.AddState((IState) bundleUpdateState2);
    if (GameEngine.ReloadMode == GameEngine.LoadMode.Lite && !InitUserDataState.Visited)
      GameEngine.ReloadMode = GameEngine.LoadMode.Deep;
    switch (GameEngine.ReloadMode)
    {
      case GameEngine.LoadMode.Lite:
        this._stateMachine.SetState((IState) this.preLoginState, (Hashtable) null);
        break;
      case GameEngine.LoadMode.Deep:
        this._stateMachine.SetState((IState) this.gameVersionState, (Hashtable) null);
        break;
    }
    UIManager.inst.splashScreen.Show();
  }

  public void Dispose()
  {
    this._isRestart = false;
    this._localVersion = new ArrayList()
    {
      (object) -1,
      (object) -1,
      (object) -1
    };
    this._remoteVersion = (ArrayList) null;
    if (this._localVerHt != null)
      this._localVerHt.Clear();
    if (this._remoteVerHt != null)
      this._remoteVerHt.Clear();
    if (this._stateMachine == null)
      return;
    this._stateMachine.Dispose();
    this._stateMachine = (StateMachine) null;
  }

  public void OnPreLoginFinish()
  {
    if (this._isRestart)
      return;
    this._stateMachine.SetState((IState) this.initDataState, (Hashtable) null);
  }

  public void OnInitFinish()
  {
    if (this._isRestart)
      return;
    this._stateMachine.SetState((IState) this.sdkState, (Hashtable) null);
  }

  public void OnGatewayStateFinish()
  {
    if (this._isRestart)
      return;
    this._stateMachine.SetState((IState) this.initState, (Hashtable) null);
  }

  public void OnGameVersionFinish()
  {
    if (this._isRestart)
      return;
    this._stateMachine.SetState((IState) this.gateway, (Hashtable) null);
  }

  public void OnStartUpFunPlusSDKFinish()
  {
    if (this._isRestart)
      return;
    this._stateMachine.SetState((IState) this.configFilsState, (Hashtable) null);
  }

  public void OnConfigFilseVerListFinish()
  {
    if (this._isRestart)
      return;
    this._stateMachine.SetState((IState) this.bundleInfoState, (Hashtable) null);
  }

  public void OnBundleVerListFinish()
  {
    if (this._isRestart)
      return;
    this._stateMachine.SetState((IState) this.cacheBunldState, (Hashtable) null);
  }

  public void OnLoadBundlesFinish()
  {
    if (this._isRestart)
      return;
    this._stateMachine.SetState((IState) this.loadConfigFileStae, (Hashtable) null);
  }

  public void OnLoadLocalizeBundlesFinish()
  {
    if (this._isRestart)
      return;
    this._stateMachine.SetState((IState) this.loadConfigFileStae, (Hashtable) null);
  }

  public void OnLoadConfigFilesFinish()
  {
    if (this._isRestart)
      return;
    this.loadConfigsReady = true;
    this._stateMachine.SetState((IState) this.initDataState, (Hashtable) null);
  }

  public void On37InitSuccess()
  {
  }

  public void OnInitDataFinish()
  {
    if (this._isRestart)
      return;
    this._stateMachine.SetState((IState) this.connectRTMState, (Hashtable) null);
  }

  public void OnConnectRTMFinish()
  {
    if (this._isRestart)
      return;
    this._stateMachine.SetState((IState) this.preOpenBundleUpdateState, (Hashtable) null);
  }

  public void OnLoadExtraDataFinish()
  {
    if (this._isRestart)
      return;
    this._stateMachine.SetState((IState) this.preloadUIstate, (Hashtable) null);
  }

  public void OnLoadUIFinish()
  {
    if (this._isRestart)
      return;
    this._stateMachine.SetState((IState) this.preOpenBundleUpdateState, (Hashtable) null);
  }

  public void OnPreOpenFinish()
  {
    if (this._isRestart)
      return;
    this._stateMachine.SetState((IState) this.otaBundleUpdateState, (Hashtable) null);
  }

  public void BuildConfig(string inKey, byte[] bytes)
  {
    if (this._isRestart || !inKey.Contains(".json") || Utils.Json2Object(Utils.ByteArray2String(bytes), true) == null)
      ;
  }

  public void AddConfigData(string key, byte[] data)
  {
    if (this.configs.ContainsKey((object) key))
      return;
    this.configs.Add((object) key, (object) data);
  }

  public void ReStart(string error, bool full = false)
  {
    if (this._isRestart)
      return;
    this._isRestart = true;
    this._fullLoad = full;
    this.Dispose();
  }

  public void SplashScreen(string description, float progress)
  {
    if (this._isRestart)
      return;
    UIManager.inst.splashScreen.SetMessage(description);
    UIManager.inst.splashScreen.SetValue(progress);
  }

  public bool FullLoad
  {
    get
    {
      return this._fullLoad;
    }
    set
    {
      this._fullLoad = value;
    }
  }

  public Hashtable LocalVerHt
  {
    get
    {
      return this._localVerHt;
    }
    set
    {
      this._localVerHt = value;
    }
  }

  public Hashtable RemoteVerHt
  {
    get
    {
      return this._remoteVerHt;
    }
    set
    {
      this._remoteVerHt = value;
    }
  }

  public bool IsRestart
  {
    get
    {
      return this._isRestart;
    }
  }
}
