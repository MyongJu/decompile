﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerParadeGround
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class BuildingControllerParadeGround : BuildingControllerNew
{
  public override void InitBuilding()
  {
    base.InitBuilding();
    this.onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
  }

  public override void Dispose()
  {
    base.Dispose();
    this.onBuildingSelected -= new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
  }

  private void OnBuildingSelected()
  {
    UIManager.inst.OpenDlg("ParadeGroundDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    AudioManager.Instance.PlaySound("sfx_city_click_parade_ground", false);
  }
}
