﻿// Decompiled with JetBrains decompiler
// Type: GenPvpUserPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class GenPvpUserPayload
{
  public List<DKPvPUser> dkPvPUsers = new List<DKPvPUser>();

  public void Decode(object data)
  {
    this.dkPvPUsers.Clear();
    Hashtable hashtable = data as Hashtable;
    if (hashtable == null)
      return;
    ArrayList arrayList = hashtable[(object) "pvp_user"] as ArrayList;
    if (arrayList == null)
      return;
    for (int index = 0; index < arrayList.Count; ++index)
    {
      DKPvPUser dkPvPuser = new DKPvPUser();
      dkPvPuser.Decode(arrayList[index]);
      this.dkPvPUsers.Add(dkPvPuser);
    }
  }
}
