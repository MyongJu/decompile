﻿// Decompiled with JetBrains decompiler
// Type: DummyServer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class DummyServer : MonoBehaviour
{
  public ArrayList _messages = new ArrayList();
  public int mStrongholdLevel = 1;
  private const string SERVER_VERSION = "1.2a";
  private const string KeyInventorySlot = "mInventorySlot";
  private const string KeyInventory = "Inventory";
  private const string KeyHeros = "Heros";
  public const int DefaultInventorySlot = 15;
  public const string CmdTypePlayerData = "CmdPlayerData";
  public const string CmdInitHero = "InitHero";
  public const string CmdSetInventorySlots = "SetInventorySlots";
  public const string CmdSetInventory = "SetInventory";
  public const string CmdSetEquipment = "SetEquipment";
  public const string CmdSwapEquipment = "SwapEquipment";
  public const string CmdSetLevel = "SetLevel";
  public const string CmdSetXP = "SetXP";
  public const string ParEquipment = "equipment";
  public const string ParSlot = "slot";
  public const string ParType = "type";
  public const string ParHeroID = "heroID";
  public const string ParLevel = "level";
  public const string ParXP = "xp";
  private static bool _created;
  private float _ServerTimer;
  public int mCityPower;
  public int mInfantryAvailable;
  public int mRangedAvailable;
  public int mCavalryAvailable;
  public int mLongArtilleryAvailable;
  public int mShortArtilleryAvailable;
  public int mInventorySlot;
  private static DummyServer _singleton;

  public static DummyServer inst
  {
    get
    {
      if ((Object) DummyServer._singleton == (Object) null)
      {
        GameObject gameObject = new GameObject();
        DummyServer._singleton = gameObject.AddComponent<DummyServer>();
        gameObject.name = nameof (DummyServer);
        Object.DontDestroyOnLoad((Object) gameObject);
      }
      return DummyServer._singleton;
    }
  }

  public static void Create()
  {
    if (DummyServer._created)
      return;
    DummyServer._created = true;
    DummyServer.inst._messages = new ArrayList();
  }

  public static bool Created()
  {
    return DummyServer._created;
  }

  private void Start()
  {
  }

  private void OnApplicationQuit()
  {
  }

  public void AddCityPower(int powerAmount)
  {
    this.mCityPower += powerAmount;
    this._messages.Add((object) Utils.Hash((object) "cmd", (object) "UpdateCityPower"));
  }

  private void Update()
  {
    this._ServerTimer -= Time.deltaTime;
    if ((double) this._ServerTimer > 0.0)
      return;
    ++this._ServerTimer;
  }

  public void SendCmd(Hashtable msg)
  {
  }

  public int GetMsgCount()
  {
    return this._messages.Count;
  }

  public Hashtable PopMessage()
  {
    Hashtable message = this._messages[0] as Hashtable;
    this._messages.RemoveAt(0);
    return message;
  }

  public void PushMessage(Hashtable newMsg)
  {
    this._messages.Add((object) newMsg);
  }
}
