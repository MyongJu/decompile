﻿// Decompiled with JetBrains decompiler
// Type: OneStorePayAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using Funplus.Abstract;
using HSMiniJSON;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class OneStorePayAndroid : BasePaymentWrapper
{
  private static readonly object locker = new object();
  public string NoProductListErrorMessage = string.Empty;
  private OneStoreIapHelper iapHelper;
  private static OneStorePayAndroid _instance;
  private string _targetGameObjectName;
  private GameObject _targetGameObject;

  public OneStorePayAndroid()
  {
    this.InitOneStoreObjects();
  }

  public static OneStorePayAndroid Instance
  {
    get
    {
      if (OneStorePayAndroid._instance == null)
      {
        lock (OneStorePayAndroid.locker)
          OneStorePayAndroid._instance = new OneStorePayAndroid();
      }
      return OneStorePayAndroid._instance;
    }
  }

  public override bool GetSubsRenewing(string productId)
  {
    return false;
  }

  public override bool CanCheckSubs()
  {
    return false;
  }

  private void InitOneStoreObjects()
  {
    GameObject gameObject = new GameObject("OneStoreIapHelper");
    Object.DontDestroyOnLoad((Object) gameObject);
    this.iapHelper = gameObject.AddComponent<OneStoreIapHelper>();
  }

  public GameObject TargetGameObject
  {
    get
    {
      if (!(bool) ((Object) this._targetGameObject))
        this._targetGameObject = GameObject.Find(this._targetGameObjectName);
      return this._targetGameObject;
    }
  }

  public override void SetGameObject(string gameObjectName)
  {
    this._targetGameObjectName = gameObjectName;
  }

  public override bool CanMakePurchases()
  {
    return true;
  }

  public override void StartHelper()
  {
    OneStorePayAndroid.OneStoreLog(string.Format(nameof (StartHelper)));
    if (!this.BuildProductListFromConfigFiles())
      this.BuildProductListFromCode();
    this.iapHelper.RequestOrderConfirm();
    this.iapHelper.RequestProductInfo();
  }

  private void BuildProductListFromCode()
  {
    OneStorePayAndroid.OneStoreLog(nameof (BuildProductListFromCode));
    List<Dictionary<string, object>> dictionaryList = new List<Dictionary<string, object>>();
    using (Dictionary<string, double>.Enumerator enumerator = new Dictionary<string, double>()
    {
      {
        "0910070403",
        1200.0
      },
      {
        "0910070404",
        5500.0
      },
      {
        "0910070405",
        12000.0
      },
      {
        "0910070406",
        24000.0
      },
      {
        "0910070407",
        59000.0
      },
      {
        "0910070408",
        120000.0
      },
      {
        "0910070409",
        5500.0
      },
      {
        "0910070410",
        12000.0
      },
      {
        "0910070411",
        24000.0
      },
      {
        "0910080686",
        5500.0
      },
      {
        "0910080687",
        12000.0
      },
      {
        "0910080688",
        24000.0
      }
    }.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, double> current = enumerator.Current;
        Dictionary<string, object> dictionary = new Dictionary<string, object>()
        {
          {
            "productId",
            (object) current.Key
          },
          {
            "title",
            (object) string.Empty
          },
          {
            "description",
            (object) string.Empty
          },
          {
            "price_currency_code",
            (object) "₩"
          },
          {
            "price",
            (object) string.Format("₩{0:N0}", (object) current.Value)
          },
          {
            "price_amount",
            (object) string.Format("{0:N0}", (object) current.Value)
          },
          {
            "price_amount_micros",
            (object) (long) (1000000.0 * current.Value)
          }
        };
        dictionaryList.Add(dictionary);
      }
    }
    string str = Json.Serialize((object) dictionaryList);
    this.TargetGameObject.BroadcastMessage("OnPaymentInitializeSuccess", (object) str);
    OneStorePayAndroid.OneStoreLog("onestore product list: " + str);
  }

  private bool BuildProductListFromConfigFiles()
  {
    OneStorePayAndroid.OneStoreLog(nameof (BuildProductListFromConfigFiles));
    if (!File.Exists(NetApi.inst.BuildLocalPath("iap_platform.json")) || !File.Exists(NetApi.inst.BuildLocalPath("iap_price_default.json")))
      return false;
    List<Dictionary<string, object>> dictionaryList = new List<Dictionary<string, object>>();
    List<IAPPlatformInfo> all = ConfigManager.inst.DB_IAPPlatform.GetAll("onestore");
    OneStorePayAndroid.OneStoreLog("allIapPlatformInfo size: " + (object) all.Count);
    using (List<IAPPlatformInfo>.Enumerator enumerator = all.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IAPPlatformInfo current = enumerator.Current;
        ConfigIAPFakePriceInfo iapFakePriceInfo = ConfigManager.inst.DB_IAPPrice.Get(current.pay_id);
        if (iapFakePriceInfo == null)
        {
          OneStorePayAndroid.OneStoreLog(string.Format("cannot find product default price : {0}", (object) current.pay_id));
        }
        else
        {
          double krw = iapFakePriceInfo.krw;
          long num = (long) (krw * 1000000.0);
          Dictionary<string, object> dictionary = new Dictionary<string, object>()
          {
            {
              "productId",
              (object) current.product_id
            },
            {
              "title",
              (object) string.Empty
            },
            {
              "description",
              (object) string.Empty
            },
            {
              "price_currency_code",
              (object) "₩"
            },
            {
              "price",
              (object) string.Format("₩{0:N0}", (object) (int) krw)
            },
            {
              "price_amount",
              (object) string.Format("{0:N0}", (object) (int) krw)
            },
            {
              "price_amount_micros",
              (object) num
            }
          };
          OneStorePayAndroid.OneStoreLog(string.Format("Add product data, curreny_code {0}, price {1}", (object) dictionary["price_currency_code"].ToString(), (object) dictionary["price"].ToString()));
          dictionaryList.Add(dictionary);
        }
      }
    }
    string str = Json.Serialize((object) dictionaryList);
    this.TargetGameObject.BroadcastMessage("OnPaymentInitializeSuccess", (object) str);
    OneStorePayAndroid.OneStoreLog("onestore product list: " + str);
    return dictionaryList.Count > 0;
  }

  public override void Buy(string productId, string throughCargo)
  {
    D.error((object) "have not implement");
  }

  public override void Buy(string productId, string serverId, string throughCargo)
  {
    OneStorePayAndroid.OneStoreLog(string.Format("Buy {0} {1} {2}", (object) productId, (object) serverId, (object) throughCargo));
    Hashtable initParams = new Hashtable();
    initParams[(object) "through_cargo"] = (object) throughCargo;
    initParams[(object) "appid"] = (object) this.AppId;
    initParams[(object) "appservid"] = (object) serverId;
    initParams[(object) "product_id"] = (object) productId;
    initParams[(object) "uid"] = (object) AccountManager.Instance.FunplusID;
    initParams[(object) "vcurrency_key"] = (object) "point";
    this.iapHelper.RequestInitTid(productId, initParams);
  }

  public override void SetCurrencyWhitelist(string whitelist)
  {
  }

  private string AppId
  {
    get
    {
      if (!string.IsNullOrEmpty(PaymentManager.Instance.PaymentAppId))
        return PaymentManager.Instance.PaymentAppId;
      return FunplusSdk.Instance.Environment == "sandbox" ? "157" : "139";
    }
  }

  public static void OneStoreLog(string logDetail)
  {
    Debug.Log((object) ("OneStoreLog:" + logDetail));
  }
}
