﻿// Decompiled with JetBrains decompiler
// Type: ActivityPriorityInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ActivityPriorityInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "start_level")]
  public int MinLevel;
  [Config(Name = "end_level")]
  public int MaxLevel;
  [Config(Name = "priority_1")]
  public string PriorityName1;
  [Config(Name = "priority_2")]
  public string PriorityName2;
  [Config(Name = "priority_3")]
  public string PriorityName3;
  [Config(Name = "priority_4")]
  public string PriorityName4;
  [Config(Name = "priority_5")]
  public string PriorityName5;
  [Config(Name = "priority_6")]
  public string PriorityName6;
  [Config(Name = "priority_7")]
  public string PriorityName7;
  [Config(Name = "priority_8")]
  public string PriorityName8;
  [Config(Name = "priority_9")]
  public string PriorityName9;
  [Config(Name = "priority_10")]
  public string PriorityName10;
  [Config(Name = "priority_11")]
  public string PriorityName11;
  [Config(Name = "priority_1_star")]
  public int PriorityStart1;
  [Config(Name = "priority_2_star")]
  public int PriorityStart2;
  [Config(Name = "priority_3_star")]
  public int PriorityStart3;
  [Config(Name = "priority_4_star")]
  public int PriorityStart4;
  [Config(Name = "priority_5_star")]
  public int PriorityStart5;
  [Config(Name = "priority_6_star")]
  public int PriorityStart6;
  [Config(Name = "priority_7_star")]
  public int PriorityStart7;
  [Config(Name = "priority_8_star")]
  public int PriorityStart8;
  [Config(Name = "priority_9_star")]
  public int PriorityStart9;
  [Config(Name = "priority_10_star")]
  public int PriorityStart10;
  [Config(Name = "priority_11_star")]
  public int PriorityStart11;

  public int GetPriorityStart(string key)
  {
    int num = 0;
    if (this.PriorityName1 == key)
      num = this.PriorityStart1;
    else if (this.PriorityName2 == key)
      num = this.PriorityStart2;
    else if (this.PriorityName3 == key)
      num = this.PriorityStart3;
    else if (this.PriorityName4 == key)
      num = this.PriorityStart4;
    else if (this.PriorityName5 == key)
      num = this.PriorityStart5;
    else if (this.PriorityName6 == key)
      num = this.PriorityStart6;
    else if (this.PriorityName7 == key)
      num = this.PriorityStart7;
    else if (this.PriorityName8 == key)
      num = this.PriorityStart8;
    else if (this.PriorityName9 == key)
      num = this.PriorityStart9;
    else if (this.PriorityName10 == key)
      num = this.PriorityStart10;
    else if (this.PriorityName11 == key)
      num = this.PriorityStart11;
    return num;
  }

  public int GetPriority(string key)
  {
    int num = 0;
    if (this.PriorityName1 == key)
      num = 1;
    else if (this.PriorityName2 == key)
      num = 2;
    else if (this.PriorityName3 == key)
      num = 3;
    else if (this.PriorityName4 == key)
      num = 4;
    else if (this.PriorityName5 == key)
      num = 5;
    else if (this.PriorityName6 == key)
      num = 6;
    else if (this.PriorityName7 == key)
      num = 7;
    else if (this.PriorityName8 == key)
      num = 8;
    else if (this.PriorityName9 == key)
      num = 9;
    else if (this.PriorityName10 == key)
      num = 10;
    else if (this.PriorityName11 == key)
      num = 11;
    return num;
  }
}
