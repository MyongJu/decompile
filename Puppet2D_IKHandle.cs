﻿// Decompiled with JetBrains decompiler
// Type: Puppet2D_IKHandle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Puppet2D_IKHandle : MonoBehaviour
{
  public Vector3[] scaleStart = new Vector3[2];
  public Vector3 OffsetScale = new Vector3(1f, 1f, 1f);
  public Transform poleVector;
  public Vector3 AimDirection;
  public Vector3 UpDirection;
  public bool Flip;
  public bool SquashAndStretch;
  public bool Scale;
  public Transform topJointTransform;
  public Transform middleJointTransform;
  public Transform bottomJointTransform;
  private Transform IK_CTRL;
  private Vector3 root2IK;
  private Vector3 root2IK2MiddleJoint;
  private bool LargerMiddleJoint;

  public void CalculateIK()
  {
    int num = !this.Flip ? -1 : 1;
    this.IK_CTRL = this.transform;
    this.root2IK = (this.topJointTransform.position + this.IK_CTRL.position) / 2f;
    Vector3 vector3 = this.IK_CTRL.position - this.topJointTransform.position;
    this.root2IK2MiddleJoint = Quaternion.AngleAxis((float) (num * 90), Vector3.forward) * vector3;
    this.poleVector.position = this.root2IK - this.root2IK2MiddleJoint;
    Quaternion q = Quaternion.AngleAxis(this.GetAngle() * (float) num, Vector3.forward);
    this.topJointTransform.rotation = this.IsNaN(q) ? (!this.LargerMiddleJoint ? Quaternion.LookRotation(this.IK_CTRL.position - this.topJointTransform.position, this.AimDirection) * Quaternion.AngleAxis(90f, this.UpDirection) : Quaternion.LookRotation(this.IK_CTRL.position - this.topJointTransform.position, this.AimDirection) * Quaternion.AngleAxis(-90f, this.UpDirection)) : Quaternion.LookRotation(this.IK_CTRL.position - this.topJointTransform.position, this.AimDirection) * Quaternion.AngleAxis(90f, this.UpDirection) * q;
    this.middleJointTransform.rotation = Quaternion.LookRotation(this.IK_CTRL.position - this.middleJointTransform.position, this.AimDirection) * Quaternion.AngleAxis(90f, this.UpDirection);
    this.bottomJointTransform.rotation = this.IK_CTRL.rotation;
    if (!this.Scale)
      return;
    this.bottomJointTransform.localScale = new Vector3(this.IK_CTRL.localScale.x * this.OffsetScale.x, this.IK_CTRL.localScale.y * this.OffsetScale.y, this.IK_CTRL.localScale.z * this.OffsetScale.z);
  }

  private bool IsNaN(Quaternion q)
  {
    if (!float.IsNaN(q.x) && !float.IsNaN(q.y) && !float.IsNaN(q.z))
      return float.IsNaN(q.w);
    return true;
  }

  private float GetAngle()
  {
    if (this.SquashAndStretch)
    {
      this.topJointTransform.localScale = this.scaleStart[0];
      this.middleJointTransform.localScale = this.scaleStart[1];
    }
    float num1 = Vector3.Distance(this.topJointTransform.position, this.middleJointTransform.position);
    float num2 = Vector3.Distance(this.middleJointTransform.position, this.bottomJointTransform.position);
    float num3 = num1 + num2;
    float a = Vector3.Distance(this.topJointTransform.position, this.IK_CTRL.position);
    this.LargerMiddleJoint = (double) num2 > (double) num1;
    if (this.SquashAndStretch && (double) a > (double) num3)
      this.topJointTransform.localScale = new Vector3(this.scaleStart[0].x, a / num3 * this.scaleStart[0].y, this.scaleStart[0].z);
    float num4 = Mathf.Min(a, num3 - 0.0001f);
    return Mathf.Acos((float) (((double) num1 * (double) num1 - (double) num2 * (double) num2 + (double) num4 * (double) num4) / (2.0 * (double) num4)) / num1) * 57.29578f;
  }
}
