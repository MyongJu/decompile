﻿// Decompiled with JetBrains decompiler
// Type: RoundPlayerBuffSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RoundPlayerBuffSlot : MonoBehaviour
{
  public MeshRenderer m_Icon;

  public void SetData(DragonKnightTalentBuffInfo buffInfo)
  {
    this.m_Icon.material.mainTexture = AssetManager.Instance.HandyLoad(buffInfo.IconPath, (System.Type) null) as Texture;
  }

  public void SetActive(bool active)
  {
    this.gameObject.SetActive(active);
  }
}
