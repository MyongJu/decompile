﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryIapPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;

public class AnniversaryIapPayload
{
  private static AnniversaryIapPayload _instance;
  private AnniversaryIapData _annvIapData;
  public System.Action<bool> OnAnnvIapBuySuccess;
  public System.Action OnAnnvIapDataUpdated;
  private bool _isOpen;
  private int _kcStartTime;
  private int _kcEndTime;
  private int _kcShowTime;
  private int _iapShowTime;
  private int _communityStartTime;
  private int _communityEndTime;
  private int _portraitStartTime;
  private int _portraitEndTime;

  public static AnniversaryIapPayload Instance
  {
    get
    {
      if (AnniversaryIapPayload._instance == null)
        AnniversaryIapPayload._instance = new AnniversaryIapPayload();
      return AnniversaryIapPayload._instance;
    }
  }

  public AnniversaryIapData AnnvIapData
  {
    get
    {
      if (this._annvIapData == null)
        this._annvIapData = new AnniversaryIapData();
      return this._annvIapData;
    }
  }

  public bool IsOpen
  {
    get
    {
      return this._isOpen;
    }
    set
    {
      this._isOpen = value;
    }
  }

  public int TopupGoldNumber
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("anniversary_get_item_gold");
      if (data != null)
        return data.ValueInt;
      return 0;
    }
  }

  public int TopupGoldReturnNumber
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("anniversary_get_item_number");
      if (data != null)
        return data.ValueInt;
      return 0;
    }
  }

  public int NeedPropsId
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("anniversary_get_item");
      if (data != null)
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(data.ValueString);
        if (itemStaticInfo != null)
          return itemStaticInfo.internalId;
      }
      return 0;
    }
  }

  public string PropsImagePath
  {
    get
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.NeedPropsId);
      if (itemStaticInfo != null)
        return itemStaticInfo.ImagePath;
      return string.Empty;
    }
  }

  public int PropsAmount
  {
    get
    {
      ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(this.NeedPropsId);
      if (consumableItemData != null)
        return consumableItemData.quantity;
      return 0;
    }
  }

  public long StartTime
  {
    get
    {
      return (long) this.IapShowTime;
    }
  }

  public long TournamentStartTime
  {
    get
    {
      return (long) this.KcStartTime;
    }
  }

  public long EndTime
  {
    get
    {
      if (AnniversaryManager.Instance.EndTime > 0)
        return (long) AnniversaryManager.Instance.EndTime;
      return (long) this.KcEndTime;
    }
  }

  public long TournamentShowTime
  {
    get
    {
      long kcShowTime = (long) this.KcShowTime;
      if (kcShowTime > this.TournamentStartTime)
        return this.TournamentStartTime;
      return kcShowTime;
    }
  }

  public string FormatTimeStamp(string start, string end, long replaceEndTime = 0)
  {
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData(start);
    GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData(end);
    if (data1 != null && data2 != null)
      return string.Format("{0}-{1}", (object) this.RemoveTimeYY(Utils.FormatTimeYYYYMMDD(Utils.DateTime2ServerTime(data1.ValueString), ".")), (object) this.RemoveTimeYY(Utils.FormatTimeYYYYMMDD(replaceEndTime <= 0L ? Utils.DateTime2ServerTime(data2.ValueString) : replaceEndTime, ".")));
    return string.Empty;
  }

  public string FormatTimeStamp(int start, int end, long replaceEndTime = 0)
  {
    return string.Format("{0}-{1}", (object) this.RemoveTimeYY(Utils.FormatTimeYYYYMMDD((long) start, ".")), (object) this.RemoveTimeYY(Utils.FormatTimeYYYYMMDD(replaceEndTime <= 0L ? (long) end : replaceEndTime, ".")));
  }

  public string RemoveTimeYY(string yymmdd)
  {
    string[] strArray = yymmdd.Split('.');
    if (strArray != null && strArray.Length == 3)
      return string.Format("{0}.{1}", (object) strArray[1], (object) strArray[2]);
    return string.Empty;
  }

  public long GetTimeStamp(string configTime)
  {
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData(configTime);
    if (data != null)
      return Utils.DateTime2ServerTime(data.ValueString);
    return 0;
  }

  public int KcStartTime
  {
    get
    {
      return this._kcStartTime;
    }
  }

  public int KcEndTime
  {
    get
    {
      return this._kcEndTime;
    }
  }

  public int KcShowTime
  {
    get
    {
      return this._kcShowTime;
    }
  }

  public int IapShowTime
  {
    get
    {
      return this._iapShowTime;
    }
  }

  public int CommunityStartTime
  {
    get
    {
      return this._communityStartTime;
    }
  }

  public int CommunityEndTime
  {
    get
    {
      return this._communityEndTime;
    }
  }

  public int PortraitStartTime
  {
    get
    {
      return this._portraitStartTime;
    }
  }

  public int PortraitEndTime
  {
    get
    {
      return this._portraitEndTime;
    }
  }

  public void SetTimeFromServer(Hashtable data)
  {
    if (data == null)
      return;
    DatabaseTools.UpdateData(data, "kc_start_time", ref this._kcStartTime);
    DatabaseTools.UpdateData(data, "kc_end_time", ref this._kcEndTime);
    DatabaseTools.UpdateData(data, "kc_show_time", ref this._kcShowTime);
    DatabaseTools.UpdateData(data, "iap_show_time", ref this._iapShowTime);
    DatabaseTools.UpdateData(data, "community_start_time", ref this._communityStartTime);
    DatabaseTools.UpdateData(data, "community_end_time", ref this._communityEndTime);
    DatabaseTools.UpdateData(data, "portrait_start_time", ref this._portraitStartTime);
    DatabaseTools.UpdateData(data, "portrait_end_time", ref this._portraitEndTime);
  }

  public bool ShouldShowAnnvIap
  {
    get
    {
      if ((long) NetServerTime.inst.ServerTimestamp >= AnniversaryIapPayload.Instance.StartTime)
        return (long) NetServerTime.inst.ServerTimestamp < AnniversaryIapPayload.Instance.EndTime;
      return false;
    }
  }

  public int GetIapPackageRemained(int annvIapPackageId)
  {
    if (this.AnnvIapData.PackageDict.ContainsKey(annvIapPackageId))
      return this.AnnvIapData.PackageDict[annvIapPackageId];
    AnniversaryIapMainInfo anniversaryIapMainInfo = ConfigManager.inst.DB_AnniversaryIapMain.Get(annvIapPackageId);
    if (anniversaryIapMainInfo != null)
      return anniversaryIapMainInfo.BuyMax;
    return 0;
  }

  public void Initialize()
  {
    MessageHub.inst.GetPortByAction("anniversary_package").AddEvent(new System.Action<object>(this.AnnvPackageDataPush));
  }

  public void Dispose()
  {
    MessageHub.inst.GetPortByAction("anniversary_package").RemoveEvent(new System.Action<object>(this.AnnvPackageDataPush));
    this._annvIapData = (AnniversaryIapData) null;
    this._kcStartTime = 0;
    this._kcEndTime = 0;
    this._kcShowTime = 0;
    this._iapShowTime = 0;
    this._communityStartTime = 0;
    this._communityEndTime = 0;
    this._portraitStartTime = 0;
    this._portraitEndTime = 0;
  }

  private void AnnvPackageDataPush(object data)
  {
    Hashtable data1 = data as Hashtable;
    if (data1 != null && data1.ContainsKey((object) "buy_iap_id"))
    {
      int result = 0;
      int.TryParse(data1[(object) "buy_iap_id"].ToString(), out result);
      AnniversaryIapMainInfo anniversaryIapMainInfo = ConfigManager.inst.DB_AnniversaryIapMain.Get(result);
      if (anniversaryIapMainInfo != null)
      {
        AccountManager.Instance.ShowBindPop();
        UIManager.inst.OpenPopup("IAPBuySuccessPopup", (Popup.PopupParameter) new IAPBuySuccessPopup.Parameter()
        {
          annvIapMainInfo = anniversaryIapMainInfo
        });
      }
    }
    this.Decode(data1);
    if (this.OnAnnvIapBuySuccess == null)
      return;
    this.OnAnnvIapBuySuccess(true);
  }

  public void RequestServerData(System.Action<bool, object> callback)
  {
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    if (callback == null)
      MessageHub.inst.GetPortByAction("anniversary:getPackageInfo").SendLoader(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.Decode(data as Hashtable);
      }), true, false);
    else
      MessageHub.inst.GetPortByAction("anniversary:getPackageInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          this.Decode(data as Hashtable);
        callback(ret, data);
      }), true);
  }

  public void AnnvExchangePackage(int iapId, System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("anniversary:exchangePackage").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "pid",
        (object) iapId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data as Hashtable);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void AnnvClaimProps(System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("anniversary:claim").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data as Hashtable);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  private void Decode(Hashtable data)
  {
    this.AnnvIapData.Decode(data);
    if (this.OnAnnvIapDataUpdated == null)
      return;
    this.OnAnnvIapDataUpdated();
  }
}
