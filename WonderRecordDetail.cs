﻿// Decompiled with JetBrains decompiler
// Type: WonderRecordDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WonderRecordDetail : MonoBehaviour
{
  private List<WonderRecordItem> items = new List<WonderRecordItem>();
  public UILabel empty;
  public UIScrollView scrollView;
  public WonderRecordItem itemPrefab;
  public UITable container;
  private bool shouldLoadMore;
  private StateUIBaseData _data;
  private bool isInit;
  private bool isDestroy;
  private bool needToScroll;

  public void SetData(StateUIBaseData data)
  {
    this._data = data;
    if (this.isInit)
      return;
    this.isInit = true;
    this.LoadData();
  }

  private void LoadData()
  {
    string action = "wonder:loadWonderLog";
    Hashtable postData = (Hashtable) null;
    if (this._data.Type == StateUIBaseData.DataType.Tower)
    {
      action = "wonder:loadWonderTowerLog";
      postData = Utils.Hash((object) "tower_id", (object) (this._data.Data as WonderTowerData).TOWER_ID);
    }
    if (postData == null)
      postData = new Hashtable();
    postData.Add((object) "kingdom_id", (object) this._data.WorldId);
    RequestManager.inst.SendRequest(action, postData, new System.Action<bool, object>(this.OnLoadDataCallBack), true);
  }

  private void OnDestroy()
  {
    this.isDestroy = true;
  }

  private void CreateFakeData()
  {
    List<RecodeUIData> datas = new List<RecodeUIData>();
    for (int index = 0; index < 10; ++index)
      datas.Add(new RecodeUIData()
      {
        Attacker = "[00ff00][lhh]dasdadattackedddr[-]",
        Defender = "[0000ff][dddlhh]defendeasdadadsr[-]",
        Name = "Test",
        Time = (long) NetServerTime.inst.ServerTimestamp,
        Type = 3
      });
    datas.Sort(new Comparison<RecodeUIData>(this.Compare));
    this.UpdateUI(datas, false);
    this.AddEventHanlder();
  }

  private void OnLoadDataCallBack(bool success, object result)
  {
    if (this.isDestroy || !success)
      return;
    List<RecodeUIData> datas = new List<RecodeUIData>();
    ArrayList arrayList = result as ArrayList;
    if (arrayList == null)
    {
      NGUITools.SetActive(this.empty.gameObject, true);
    }
    else
    {
      if (arrayList == null)
        return;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        RecodeUIData recodeUiData = new RecodeUIData();
        if (recodeUiData.Decode(arrayList[index]))
        {
          if (this._data.Type == StateUIBaseData.DataType.Avalon)
            recodeUiData.IsWonder = true;
          datas.Add(recodeUiData);
        }
      }
      datas.Sort(new Comparison<RecodeUIData>(this.Compare));
      this.UpdateUI(datas, false);
      this.AddEventHanlder();
    }
  }

  private int Compare(RecodeUIData a, RecodeUIData b)
  {
    if (a.Time > b.Time)
      return -1;
    return a.Time < b.Time ? 1 : 0;
  }

  private void UpdateUI(List<RecodeUIData> datas, bool isAppend = false)
  {
    for (int count = this.items.Count; count < datas.Count; ++count)
      this.GetItem().SetData(datas[count]);
    this.container.repositionNow = true;
    this.container.Reposition();
    this.needToScroll = true;
  }

  private WonderRecordItem GetItem()
  {
    GameObject gameObject = NGUITools.AddChild(this.container.gameObject, this.itemPrefab.gameObject);
    gameObject.SetActive(true);
    WonderRecordItem component = gameObject.GetComponent<WonderRecordItem>();
    this.items.Add(component);
    return component;
  }

  public void Clear()
  {
    this.items.Clear();
    this.RemoveEventHanlder();
  }

  private void AddEventHanlder()
  {
    this.scrollView.onDragFinished -= new UIScrollView.OnDragNotification(this.OnDragFinish);
    this.scrollView.onStoppedMoving -= new UIScrollView.OnDragNotification(this.onStoppedMoving);
  }

  private void RemoveEventHanlder()
  {
    this.scrollView.onDragFinished -= new UIScrollView.OnDragNotification(this.OnDragFinish);
    this.scrollView.onStoppedMoving -= new UIScrollView.OnDragNotification(this.onStoppedMoving);
  }

  public void OnDragFinish()
  {
    Bounds bounds = this.scrollView.bounds;
    Vector3 constrainOffset = this.scrollView.panel.CalculateConstrainOffset((Vector2) bounds.min, (Vector2) bounds.max);
    if (!this.scrollView.canMoveHorizontally)
      constrainOffset.x = 0.0f;
    if (!this.scrollView.canMoveVertically)
      constrainOffset.y = 0.0f;
    if ((double) constrainOffset.y >= -200.0)
      return;
    this.shouldLoadMore = true;
  }

  private void onStoppedMoving()
  {
    if (this.shouldLoadMore)
    {
      this.shouldLoadMore = false;
      this.GetMoreRecoders();
    }
    this.scrollView.RestrictWithinBounds(true);
  }

  private void GetMoreRecoders()
  {
  }

  private void OnGetMoreCallBack(bool success, object result)
  {
    if (success)
      ;
  }

  private void Update()
  {
    if (this.isDestroy)
      return;
    if (this.needToScroll)
    {
      this.needToScroll = false;
      this.scrollView.ResetPosition();
    }
    if (!this.scrollView.isDragging)
      return;
    Bounds bounds = this.scrollView.bounds;
    Vector3 constrainOffset = this.scrollView.panel.CalculateConstrainOffset((Vector2) bounds.min, (Vector2) bounds.max);
    if (!this.scrollView.canMoveHorizontally)
      constrainOffset.x = 0.0f;
    if (!this.scrollView.canMoveVertically)
      constrainOffset.y = 0.0f;
    if ((double) constrainOffset.y < -200.0)
      ;
  }
}
