﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightPVPComparisonPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;

public class DragonKnightPVPComparisonPopup : Popup
{
  public DragonKnightPVPComparisonPanel m_SourcePanel;
  public DragonKnightPVPComparisonPanel m_TargetPanel;
  public DragonKnightPanel m_SourceDKPanel;
  public DragonKnightPanel m_TargetDKPanel;
  private DragonKnightPVPComparisonPopup.Parameter m_Parameter;

  private static bool BenefitFilter(string shortId)
  {
    string key = shortId;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (DragonKnightPVPComparisonPopup.\u003C\u003Ef__switch\u0024map56 == null)
      {
        // ISSUE: reference to a compiler-generated field
        DragonKnightPVPComparisonPopup.\u003C\u003Ef__switch\u0024map56 = new Dictionary<string, int>(3)
        {
          {
            "dk87",
            0
          },
          {
            "dk86",
            0
          },
          {
            "dk85",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (DragonKnightPVPComparisonPopup.\u003C\u003Ef__switch\u0024map56.TryGetValue(key, out num) && num == 0)
        return false;
    }
    return true;
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as DragonKnightPVPComparisonPopup.Parameter;
    if (this.m_Parameter == null)
      return;
    Dictionary<string, string> benefits1 = new Dictionary<string, string>();
    Dictionary<string, float> benefits2 = this.m_Parameter.sourceDK.Benefits;
    Dictionary<string, float> benefits3 = this.m_Parameter.targetDK.Benefits;
    using (Dictionary<string, float>.Enumerator enumerator = benefits2.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, float> current = enumerator.Current;
        if (DragonKnightPVPComparisonPopup.BenefitFilter(current.Key))
          benefits1[current.Key] = current.Key;
      }
    }
    using (Dictionary<string, float>.Enumerator enumerator = benefits3.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, float> current = enumerator.Current;
        if (DragonKnightPVPComparisonPopup.BenefitFilter(current.Key))
          benefits1[current.Key] = current.Key;
      }
    }
    this.m_SourceDKPanel.SetData(this.m_Parameter.sourceDK, false, this.m_Parameter.sourceScore);
    this.m_TargetDKPanel.SetData(this.m_Parameter.targetDK, false, this.m_Parameter.targetScore);
    this.m_SourcePanel.SetData(this.m_Parameter.source, this.m_Parameter.sourceDK, this.m_Parameter.targetDK, benefits1, this.m_Parameter.sourceEquipments, this.m_Parameter.sourceTalent1, this.m_Parameter.sourceTalent2, this.m_Parameter.sourceTalent3);
    this.m_TargetPanel.SetData(this.m_Parameter.target, this.m_Parameter.targetDK, this.m_Parameter.sourceDK, benefits1, this.m_Parameter.targetEquipments, this.m_Parameter.targetTalent1, this.m_Parameter.targetTalent2, this.m_Parameter.targetTalent3);
  }

  public void OnClosePressed()
  {
    if (this.m_Parameter.callback != null)
      this.m_Parameter.callback();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public long sourceScore = -1;
    public long targetScore = -1;
    public UserData source;
    public AllianceData sourceAlliance;
    public DragonKnightData sourceDK;
    public List<InventoryItemData> sourceEquipments;
    public int sourceTalent1;
    public int sourceTalent2;
    public int sourceTalent3;
    public UserData target;
    public AllianceData targetAlliance;
    public DragonKnightData targetDK;
    public List<InventoryItemData> targetEquipments;
    public int targetTalent1;
    public int targetTalent2;
    public int targetTalent3;
    public System.Action callback;
  }
}
