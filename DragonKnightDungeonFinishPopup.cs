﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightDungeonFinishPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonKnightDungeonFinishPopup : Popup
{
  public UILabel itemName;
  public UILabel itemDesc;
  public UILabel itemCountText;
  public UITexture itemTexture;
  public UIButton useItemButton;
  public UIButton useGoldButton;
  public UILabel useGoldAmount;
  private ItemStaticInfo _itemStaticInfo;
  private ShopStaticInfo _shopStaticInfo;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnUseItemBtnPressed()
  {
    if (this._itemStaticInfo == null)
      return;
    if (!ItemBag.Instance.CheckCanUse(this._itemStaticInfo.internalId))
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_item_requirement_tip", new Dictionary<string, string>()
      {
        {
          "num",
          this._itemStaticInfo.RequireLev.ToString()
        }
      }, true), (System.Action) null, 4f, false);
    else if (ItemBag.Instance.GetItemCount(this._itemStaticInfo.internalId) >= 1)
    {
      ItemBag.Instance.UseItem(this._itemStaticInfo.internalId, 1, (Hashtable) null, (System.Action<bool, object>) null);
      this.OnCloseBtnPressed();
    }
    else
    {
      ItemUseOrBuyPopup.Parameter parameter = new ItemUseOrBuyPopup.Parameter();
      parameter.item_id = this._itemStaticInfo.internalId;
      parameter.onUseOrBuyCallBack += new System.Action<bool, object>(this.OnUseOrBuyCallback);
      parameter.maxCount = 1;
      UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) parameter);
    }
  }

  public void OnUseGoldBtnPressed()
  {
    if (this._itemStaticInfo == null || this._shopStaticInfo == null)
      return;
    if (!ItemBag.Instance.CheckCanUse(this._itemStaticInfo.internalId))
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_item_requirement_tip", new Dictionary<string, string>()
      {
        {
          "num",
          this._itemStaticInfo.RequireLev.ToString()
        }
      }, true), (System.Action) null, 4f, false);
    else if (PlayerData.inst.hostPlayer.Currency < this._shopStaticInfo.Price)
    {
      Utils.ShowNotEnoughGoldTip();
    }
    else
    {
      ItemUseOrBuyPopup.Parameter parameter = new ItemUseOrBuyPopup.Parameter();
      parameter.shop_id = this._shopStaticInfo.internalId;
      parameter.onUseOrBuyCallBack += new System.Action<bool, object>(this.OnUseOrBuyCallback);
      parameter.maxCount = 1;
      UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) parameter);
    }
  }

  private void OnUseOrBuyCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.OnCloseBtnPressed();
  }

  private void UpdateUI()
  {
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (dragonKnightData == null)
      return;
    this._itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_dungeon_finish");
    if (this._itemStaticInfo != null)
    {
      this.itemName.text = this._itemStaticInfo.LocName;
      this.itemDesc.text = this._itemStaticInfo.LocDescription;
      BuilderFactory.Instance.HandyBuild((UIWidget) this.itemTexture, this._itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(this._itemStaticInfo.internalId);
      if (consumableItemData != null)
      {
        this.itemCountText.text = consumableItemData.quantity.ToString();
        NGUITools.SetActive(this.useItemButton.gameObject, consumableItemData.quantity > 0);
        NGUITools.SetActive(this.useGoldButton.gameObject, consumableItemData.quantity <= 0);
      }
      else
      {
        this.itemCountText.text = "0";
        NGUITools.SetActive(this.useItemButton.gameObject, false);
        NGUITools.SetActive(this.useGoldButton.gameObject, true);
      }
      this._shopStaticInfo = ConfigManager.inst.DB_Shop.GetShopInfoByItemInternalId(this._itemStaticInfo.internalId);
      if (this._shopStaticInfo != null)
      {
        this.useGoldAmount.text = this._shopStaticInfo.Price.ToString();
        if (PlayerData.inst.userData.currency.gold < (long) this._shopStaticInfo.Price)
          this.useGoldAmount.color = Color.red;
      }
    }
    UIButton useItemButton = this.useItemButton;
    bool isInDungeon = dragonKnightData.IsInDungeon;
    this.useGoldButton.isEnabled = isInDungeon;
    int num = isInDungeon ? 1 : 0;
    useItemButton.isEnabled = num != 0;
  }
}
