﻿// Decompiled with JetBrains decompiler
// Type: MarchTroopsUIInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UnityEngine;

public class MarchTroopsUIInfo : MonoBehaviour
{
  private const string TEXTURE_PATH_PRE = "Texture/Unit/portrait_unit_";
  public IconGroup skillGroup;

  public void SetData(long march_id)
  {
    this.skillGroup.CreateIcon(this.GetSkillIconData(march_id));
  }

  private List<IconData> GetSkillIconData(long march_id)
  {
    List<IconData> iconDataList = new List<IconData>();
    MarchData marchData = DBManager.inst.DB_March.Get(march_id);
    Dictionary<Unit_StatisticsInfo, int> dictionary = new Dictionary<Unit_StatisticsInfo, int>();
    List<Unit_StatisticsInfo> unitStatisticsInfoList = new List<Unit_StatisticsInfo>();
    if (marchData != null && marchData.troopsInfo.troops != null)
    {
      Dictionary<string, Unit>.KeyCollection.Enumerator enumerator = marchData.troopsInfo.troops.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator.Current);
        dictionary.Add(data, marchData.troopsInfo.troops[enumerator.Current].Count);
        unitStatisticsInfoList.Add(data);
      }
      unitStatisticsInfoList.Sort(new Comparison<Unit_StatisticsInfo>(this.CompareFun));
      for (int index1 = 0; index1 < unitStatisticsInfoList.Count; ++index1)
      {
        Unit_StatisticsInfo index2 = unitStatisticsInfoList[index1];
        IconData iconData = new IconData(string.Format("{0}{1}", (object) "Texture/Unit/portrait_unit_", (object) index2.Image), new string[3]
        {
          ScriptLocalization.Get(string.Format("{0}{1}", (object) index2.ID, (object) "_name"), true),
          dictionary[index2].ToString(),
          Utils.GetRomaNumeralsBelowTen(index2.Troop_Tier)
        });
        iconDataList.Add(iconData);
      }
    }
    return iconDataList;
  }

  private int CompareFun(Unit_StatisticsInfo a, Unit_StatisticsInfo b)
  {
    if (a.Troop_Tier == b.Troop_Tier)
      return a.Priority.CompareTo(b.Priority);
    return a.Troop_Tier < b.Troop_Tier ? 1 : -1;
  }

  public void Clear()
  {
    if (!((UnityEngine.Object) this.skillGroup != (UnityEngine.Object) null))
      return;
    this.skillGroup.Clear();
  }
}
