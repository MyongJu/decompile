﻿// Decompiled with JetBrains decompiler
// Type: ConfigEnhance_Curve
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigEnhance_Curve
{
  private Dictionary<int, EnhanceCurveInfo> m_EnhanceCurveDict = new Dictionary<int, EnhanceCurveInfo>();
  private Dictionary<int, Dictionary<int, EnhanceCurveInfo>> m_EnhanceCurveMatrix = new Dictionary<int, Dictionary<int, EnhanceCurveInfo>>();

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exsit", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "curve");
        int index3 = ConfigManager.GetIndex(inHeader, "level");
        int index4 = ConfigManager.GetIndex(inHeader, "essence_total");
        int index5 = ConfigManager.GetIndex(inHeader, "essence_diff");
        int index6 = ConfigManager.GetIndex(inHeader, "essence_sell");
        int index7 = ConfigManager.GetIndex(inHeader, "benefit_increase_percent");
        int index8 = ConfigManager.GetIndex(inHeader, "critical_chance");
        int index9 = ConfigManager.GetIndex(inHeader, "critical_value");
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          EnhanceCurveInfo curveInfo = new EnhanceCurveInfo();
          if (int.TryParse(key.ToString(), out curveInfo.InternalId))
          {
            ArrayList arr = hashtable[key] as ArrayList;
            if (arr != null)
            {
              curveInfo.ID = ConfigManager.GetString(arr, index1);
              curveInfo.Curve = ConfigManager.GetInt(arr, index2);
              curveInfo.Level = ConfigManager.GetInt(arr, index3);
              curveInfo.EssenceTotal = ConfigManager.GetInt(arr, index4);
              curveInfo.EssenceDiff = ConfigManager.GetInt(arr, index5);
              curveInfo.EssenceSell = ConfigManager.GetInt(arr, index6);
              curveInfo.BenefitIncreasePercentage = ConfigManager.GetDouble(arr, index7);
              curveInfo.CriticalChance = ConfigManager.GetDouble(arr, index8);
              curveInfo.CriticalValue = ConfigManager.GetInt(arr, index9);
              this.PushData(curveInfo.InternalId, curveInfo);
            }
          }
        }
      }
    }
  }

  public void Clear()
  {
    this.m_EnhanceCurveDict.Clear();
    this.m_EnhanceCurveMatrix.Clear();
  }

  public EnhanceCurveInfo GetData(int internalId)
  {
    if (this.m_EnhanceCurveDict.ContainsKey(internalId))
      return this.m_EnhanceCurveDict[internalId];
    return (EnhanceCurveInfo) null;
  }

  public double ExpPercent(int internalId, double exp)
  {
    EnhanceCurveInfo data = this.GetData(internalId);
    Dictionary<int, EnhanceCurveInfo> dictionary = (Dictionary<int, EnhanceCurveInfo>) null;
    this.m_EnhanceCurveMatrix.TryGetValue(data.Curve, out dictionary);
    if (dictionary == null)
      return 0.0;
    if (dictionary.Count == data.Level)
      return 1.0;
    return exp / (double) data.EssenceDiff;
  }

  public string ExpPercentString(int internalId, int exp)
  {
    EnhanceCurveInfo data = this.GetData(internalId);
    Dictionary<int, EnhanceCurveInfo> dictionary = (Dictionary<int, EnhanceCurveInfo>) null;
    this.m_EnhanceCurveMatrix.TryGetValue(data.Curve, out dictionary);
    if (dictionary == null)
      return string.Format("{0}/{1}", (object) 0, (object) 0);
    if (dictionary.Count != data.Level)
      return string.Format("{0}/{1}", (object) exp, (object) data.EssenceDiff);
    EnhanceCurveInfo enhanceCurveInfo = dictionary[data.Level - 1];
    return string.Format("{0}/{1}", (object) enhanceCurveInfo.EssenceDiff, (object) enhanceCurveInfo.EssenceDiff);
  }

  private void PushData(int internalId, EnhanceCurveInfo curveInfo)
  {
    if (this.m_EnhanceCurveDict.ContainsKey(internalId))
      return;
    this.m_EnhanceCurveDict.Add(internalId, curveInfo);
    Dictionary<int, EnhanceCurveInfo> dictionary = (Dictionary<int, EnhanceCurveInfo>) null;
    if (!this.m_EnhanceCurveMatrix.TryGetValue(curveInfo.Curve, out dictionary))
    {
      dictionary = new Dictionary<int, EnhanceCurveInfo>();
      this.m_EnhanceCurveMatrix.Add(curveInfo.Curve, dictionary);
    }
    dictionary.Add(curveInfo.Level, curveInfo);
    ConfigManager.inst.AddData(internalId, (object) curveInfo);
  }
}
