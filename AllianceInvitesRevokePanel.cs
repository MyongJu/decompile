﻿// Decompiled with JetBrains decompiler
// Type: AllianceInvitesRevokePanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;
using UnityEngine;

public class AllianceInvitesRevokePanel : MonoBehaviour
{
  public UITexture mIcon;
  public UILabel mName;
  public UILabel mPower;
  public UILabel mLevel;
  public System.Action onRevoke;
  private AllianceInvitedApplyData m_Candidate;
  private UserData m_UserData;
  private CityData m_CityData;

  public void SetDetails(AllianceInvitedApplyData candidate)
  {
    this.m_Candidate = candidate;
    this.m_UserData = DBManager.inst.DB_User.Get(candidate.uid);
    this.m_CityData = DBManager.inst.DB_City.GetByUid(candidate.uid);
    this.mName.text = this.m_UserData.userName;
    this.mPower.text = this.m_UserData.power.ToString();
    this.mLevel.text = this.m_CityData.level.ToString();
    CustomIconLoader.Instance.requestCustomIcon(this.mIcon, this.m_UserData.PortraitIconPath, this.m_UserData.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.mIcon, this.m_UserData.LordTitle, 1);
  }

  public void OnProfilePressed()
  {
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
    {
      uid = this.m_Candidate.uid
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnRevokeBtnPressed()
  {
    MessageHub.inst.GetPortByAction("Alliance:revokeInviteByAlliance").SendRequest(new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) PlayerData.inst.allianceId
      },
      {
        (object) "invite_uid",
        (object) this.m_Candidate.uid
      }
    }, new System.Action<bool, object>(this.OnRevoke), true);
  }

  private void OnRevoke(bool ret, object data)
  {
    if (this.onRevoke == null)
      return;
    this.onRevoke();
  }
}
