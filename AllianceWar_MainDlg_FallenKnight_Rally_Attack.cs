﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_FallenKnight_Rally_Attack
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class AllianceWar_MainDlg_FallenKnight_Rally_Attack : AllianceWar_MainDlg_Rally_Slot
{
  public override void Setup(long marchId)
  {
    if (this.IsDestroy)
      return;
    MarchData marchData = DBManager.inst.DB_March.Get(marchId);
    if (marchData == null)
      return;
    this.data.marchId = marchId;
    for (int index = 0; index < PlayerData.inst.allianceWarManager.knightMarches.Count; ++index)
    {
      AllianceWarManager.KnightWarData knightMarch = PlayerData.inst.allianceWarManager.knightMarches[index];
      if (PlayerData.inst.allianceWarManager.knightMarches[index].marchId == marchId)
      {
        this.SetupAboveListByMarchData(marchData, marchData.targetUid, marchData.targetAllianceId, knightMarch.defenseCount);
        this.SetupBelowListByMarchData(knightMarch);
      }
      this.panel.targetSlot.layerIndex = LayerMask.NameToLayer("SmallViewport");
      if (knightMarch.knightInfo.para_2 == "event_fallen_knight_army")
      {
        AllianceFortressData dataByCoordinate = DBManager.inst.DB_AllianceFortress.GetDataByCoordinate(marchData.targetLocation);
        if (dataByCoordinate != null)
          this.panel.targetSlot.SetFortress(dataByCoordinate.fortressId);
      }
      else
        this.panel.targetSlot.SetCity(marchData.targetCityId, 0.0f);
    }
    this.RefreshTime();
  }

  private void SetupAboveListByMarchData(MarchData marchData, long uid, long allianceId, int memberCount)
  {
    UserData userData = DBManager.inst.DB_User.Get(uid);
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(allianceId);
    AllianceWar_MainDlg_RallyList.param.ownerPortrait = 0;
    if (userData != null)
    {
      AllianceWar_MainDlg_RallyList.param.ownerName = allianceData == null ? userData.userName : string.Format("[{0}]{1}", (object) allianceData.allianceAcronym, (object) userData.userName);
      AllianceWar_MainDlg_RallyList.param.ownerPortrait = userData.portrait;
      AllianceWar_MainDlg_RallyList.param.ownerCustomIconUrl = userData.Icon;
      AllianceWar_MainDlg_RallyList.param.ownerLordTitleId = userData.LordTitle;
    }
    AllianceWar_MainDlg_RallyList.param.memberCount = memberCount;
    AllianceWar_MainDlg_RallyList.param.canJoin = true;
    AllianceWar_MainDlg_RallyList.param.isAttack = true;
    AllianceWar_MainDlg_RallyList.param.rallyType = AllianceWar_MainDlg_RallyList.Params.RallyListType.MEMBER;
    this.panel.aboveList.Setup(AllianceWar_MainDlg_RallyList.param);
  }

  private void SetupBelowListByMarchData(AllianceWarManager.KnightWarData knightData)
  {
    if (knightData != null)
    {
      AllianceWar_MainDlg_RallyList.param.ownerName = knightData.knightInfo.LOC_Name;
      AllianceWar_MainDlg_RallyList.param.ownerPortrait = knightData.knightInfo.iconIndex;
    }
    AllianceWar_MainDlg_RallyList.param.memberCount = knightData.defenseCount;
    AllianceWar_MainDlg_RallyList.param.canJoin = false;
    AllianceWar_MainDlg_RallyList.param.isAttack = false;
    AllianceWar_MainDlg_RallyList.param.rallyType = AllianceWar_MainDlg_RallyList.Params.RallyListType.FALLEN_KNIGHT;
    this.panel.belowList.Setup(AllianceWar_MainDlg_RallyList.param);
  }

  public override void OnDetailClick()
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.data.marchId);
    if (marchData == null)
      return;
    UIManager.inst.OpenDlg("Alliance/AllianceRallyDetialDlg", (UI.Dialog.DialogParameter) new AllianceRallyDetailPopUp.Parameter()
    {
      data_id = marchData.marchId
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  protected override void RefreshTime()
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.data.marchId);
    if (marchData != null && marchData.IsExpedition)
    {
      this.panel.rallyProgress.gameObject.SetActive(true);
      this.panel.rallyProgressText.text = string.Format("{0} {1}", (object) Utils.XLAT("war_rally_marching"), (object) Utils.FormatTime(marchData.endTime - NetServerTime.inst.ServerTimestamp, false, false, true));
      this.panel.rallyProgress.value = (float) (NetServerTime.inst.ServerTimestamp - marchData.startTime) * marchData.timeDuration.oneOverTimeDuration;
    }
    else
    {
      this.panel.rallyProgress.gameObject.SetActive(false);
      PlayerData.inst.allianceWarManager.LoadDatas();
    }
  }
}
