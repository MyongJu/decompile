﻿// Decompiled with JetBrains decompiler
// Type: ServerErrorCode
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public static class ServerErrorCode
{
  public const int ERRNO_SYS_LOCKED = 1;
  public const int ERRNO_SYS_INVALID_ARGUMENT = 2;
  public const int ERRNO_SYS_OVERFLOW = 3;
  public const int ERRNO_SYS_STATE = 4;
  public const int ERRNO_SYS_NOT_FOUND = 5;
  public const int ERRNO_SYS_FORBIDDEN = 6;
  public const int ERRNO_SYS_EXPIRATION = 7;
  public const int ERRNO_SYS_SERIAL_UPDATE = 8;
  public const int ERRNO_SYS_BUSY = 9;
  public const int ERRNO_SYS_DUPLICATED = 10;
  public const int ERRNO_SYS_UNQUALIFIED = 11;
  public const int ERRNO_EXCEPTION_BEEN_BLOCKED = 108;
  public const int ERRNO_SYS_MONSTER_CORRUPTION = 1200080;
  public const int ERRNO_SYS_RESOURCE_CORRUPTION = 1400040;
  public const int ERRNO_MARCH_GVE_CAMP_MISS = 1200110;
  public const int ERRNO_TELEPORT_BEGINNER_EXPIRED = 1400081;
  public const int ERRNO_AUCTION_PRICE_LESS_BID = 2030050;
  public const int ERRNO_SECONDARY_CODE_DOES_NOT_EXIST = 1100030;
  public const int ERRNO_SECONDARY_CODE_HAS_BEEN_USED = 1100040;
  public const int ERRNO_SECONDARY_CODE_INVALID = 1100050;
  public const int ERRNO_SECONDARY_CODE_EXCHANGED = 1100060;
  public const int ERRNO_ALLIANCE_FUND_NOT_ENOUGH = 1002030;
  public const int ERRNO_ALLIANCE_NAME_EXIST = 1004021;
  public const int ERRNO_PLAYER_NAME_EXIST = 1300051;
}
