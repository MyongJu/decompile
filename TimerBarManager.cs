﻿// Decompiled with JetBrains decompiler
// Type: TimerBarManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerBarManager
{
  private Dictionary<long, TimerBarManager.TimerParameter> m_ParamDict = new Dictionary<long, TimerBarManager.TimerParameter>();
  private static TimerBarManager m_Instance;
  private GameObject m_TimerBarPrefab;

  private TimerBarManager()
  {
  }

  public static TimerBarManager Instance
  {
    get
    {
      if (TimerBarManager.m_Instance == null)
        TimerBarManager.m_Instance = new TimerBarManager();
      return TimerBarManager.m_Instance;
    }
  }

  public void Startup()
  {
    this.m_TimerBarPrefab = AssetManager.Instance.HandyLoad("Prefab/City/CityTimerbar", (System.Type) null) as GameObject;
    this.RegisterListeners();
    Dictionary<long, JobHandle>.Enumerator enumerator = JobManager.Instance.GetJobDict().GetEnumerator();
    while (enumerator.MoveNext())
      this.OnJobCreated(enumerator.Current.Key);
  }

  public void Terminate()
  {
    this.UnregisterListeners();
    this.RemoveAllTimerBar();
  }

  private void RegisterListeners()
  {
    JobManager.Instance.OnJobCreated += new System.Action<long>(this.OnJobCreated);
    JobManager.Instance.OnJobRemove += new System.Action<long>(this.OnJobRemove);
    Oscillator.Instance.updateEvent += new System.Action<double>(this.OnUpdate);
    DBManager.inst.DB_CityMap.onDataCreated += new System.Action<CityMapData>(this.OnBuildComplete);
    DBManager.inst.DB_CityMap.onDataUpdated += new System.Action<CityMapData>(this.OnBuildComplete);
  }

  private void UnregisterListeners()
  {
    JobManager.Instance.OnJobCreated -= new System.Action<long>(this.OnJobCreated);
    JobManager.Instance.OnJobRemove -= new System.Action<long>(this.OnJobRemove);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.OnUpdate);
    DBManager.inst.DB_CityMap.onDataCreated -= new System.Action<CityMapData>(this.OnBuildComplete);
    DBManager.inst.DB_CityMap.onDataUpdated -= new System.Action<CityMapData>(this.OnBuildComplete);
  }

  private void OnUpdate(double delta)
  {
    if ((UnityEngine.Object) GameEngine.Instance == (UnityEngine.Object) null || GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.CityMode)
      return;
    List<long> longList = new List<long>((IEnumerable<long>) this.m_ParamDict.Keys);
    bool flag = false;
    using (List<long>.Enumerator enumerator1 = longList.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        TimerBarManager.TimerParameter timerParameter = this.m_ParamDict[enumerator1.Current];
        if (timerParameter.GenerateTimersOnce(this.m_TimerBarPrefab))
          flag = true;
        if (timerParameter.timers != null)
        {
          using (List<CityTimerbar>.Enumerator enumerator2 = timerParameter.timers.GetEnumerator())
          {
            while (enumerator2.MoveNext())
              enumerator2.Current.OnUpdate(delta);
          }
        }
      }
    }
    if (!flag)
      return;
    this.SortCityTimerBar();
  }

  public void SortCityTimerBar()
  {
    List<CityTimerbar> cityTimerbarList = new List<CityTimerbar>();
    using (List<long>.Enumerator enumerator = new List<long>((IEnumerable<long>) this.m_ParamDict.Keys).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TimerBarManager.TimerParameter timerParameter = this.m_ParamDict[enumerator.Current];
        if (timerParameter.timers != null)
          cityTimerbarList.AddRange((IEnumerable<CityTimerbar>) timerParameter.timers);
      }
    }
    cityTimerbarList.Sort((Comparison<CityTimerbar>) ((t1, t2) => (double) t1.transform.position.y > (double) t2.transform.position.y ? -1 : 1));
    for (int index = 0; index < cityTimerbarList.Count; ++index)
      cityTimerbarList[index].GetComponent<UIPanel>().depth = index + 1;
  }

  private void OnJobCreated(long jobId)
  {
    JobHandle job = JobManager.Instance.GetJob(jobId);
    if (!(bool) job)
      return;
    switch (job.GetJobEvent())
    {
      case JobEvent.JOB_BUILDING_CONSTRUCTION:
        this.AddTimerBar(jobId, TimerType.TIMER_CONSTRUCT, string.Empty);
        break;
      case JobEvent.JOB_BUILDING_DECONSTRUCTION:
        this.AddTimerBar(jobId, TimerType.TIMER_DESTRUCT, string.Empty);
        break;
      case JobEvent.JOB_TRAIN_TROOP:
        this.AddTimerBar(jobId, TimerType.TIMER_TRAIN, "training");
        break;
      case JobEvent.JOB_RESEARCH:
        this.AddTimerBar(jobId, TimerType.TIMER_RESEARCH, "researching");
        break;
      case JobEvent.JOB_HEALING_TROOPS:
        this.AddTimerBar(jobId, TimerType.TIMER_HEAL, "healing");
        break;
      case JobEvent.JOB_FORGE:
        this.AddTimerBar(jobId, TimerType.TIMER_FORGE, "forging");
        break;
      case JobEvent.JOB_DK_FORGE:
        this.AddTimerBar(jobId, TimerType.TIMER_DK_FORGE, "forging");
        break;
      case JobEvent.JOB_UPGRADE_TROOPS:
        this.AddTimerBar(jobId, TimerType.TIMER_TRAIN, string.Empty);
        break;
    }
  }

  private void AddTimerBar(long jobID, TimerType timerType, string targetName)
  {
    if (this.m_ParamDict.ContainsKey(jobID))
      return;
    TimerBarManager.TimerParameter timerParameter = new TimerBarManager.TimerParameter();
    timerParameter.Initialize(jobID, timerType, targetName);
    this.m_ParamDict.Add(jobID, timerParameter);
  }

  private void OnJobRemove(long jobId)
  {
    this.RemoveTimerBar(jobId);
  }

  private void OnBuildComplete(CityMapData cityMapData)
  {
    if (!(CityManager.inst.FindBuildingItem(cityMapData.buildingId).mType == "hospital"))
      return;
    JobHandle singleJobByClass = JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_HEALING_TROOPS);
    if (singleJobByClass == null || !this.m_ParamDict.ContainsKey(singleJobByClass.GetJobID()))
      return;
    this.RemoveTimerBar(singleJobByClass.GetJobID());
    this.AddTimerBar(singleJobByClass.GetJobID(), TimerType.TIMER_HEAL, "healing");
  }

  private void RemoveTimerBar(long jobId)
  {
    TimerBarManager.TimerParameter timerParameter;
    this.m_ParamDict.TryGetValue(jobId, out timerParameter);
    if (timerParameter != null)
      timerParameter.Terminate();
    this.m_ParamDict.Remove(jobId);
  }

  private void RemoveAllTimerBar()
  {
    Dictionary<long, TimerBarManager.TimerParameter>.Enumerator enumerator = this.m_ParamDict.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Value.Terminate();
    this.m_ParamDict.Clear();
  }

  private class TimerParameter
  {
    private long m_JobID;
    private TimerType m_Type;
    private string m_TargetName;
    private List<CityTimerbar> m_TimerList;

    public long jobId
    {
      get
      {
        return this.m_JobID;
      }
    }

    public TimerType type
    {
      get
      {
        return this.m_Type;
      }
    }

    public string targetName
    {
      get
      {
        return this.m_TargetName;
      }
    }

    public List<CityTimerbar> timers
    {
      get
      {
        return this.m_TimerList;
      }
    }

    public void Initialize(long jobId, TimerType type, string targetName)
    {
      this.m_JobID = jobId;
      this.m_Type = type;
      this.m_TargetName = targetName;
    }

    public bool GenerateTimersOnce(GameObject prefab)
    {
      if (this.m_TimerList != null)
        return false;
      bool flag = false;
      using (List<Transform>.Enumerator enumerator = this.GetMountPointList().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Transform current = enumerator.Current;
          CityTimerbar component = PrefabManagerEx.Instance.Spawn(prefab, PrefabManagerEx.Instance.transform).GetComponent<CityTimerbar>();
          component.SetDetails(this.m_JobID, this.m_TargetName, this.m_Type, (string) null);
          component.Open(current);
          if (this.m_TimerList == null)
            this.m_TimerList = new List<CityTimerbar>();
          this.m_TimerList.Add(component);
          flag = true;
        }
      }
      return flag;
    }

    public void Terminate()
    {
      if (this.m_TimerList == null)
        return;
      using (List<CityTimerbar>.Enumerator enumerator = this.m_TimerList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          CityTimerbar current = enumerator.Current;
          if ((UnityEngine.Object) null != (UnityEngine.Object) current)
            PrefabManagerEx.Instance.Destroy(current.gameObject);
        }
      }
    }

    private List<Transform> GetMountPointList()
    {
      List<Transform> transformList = new List<Transform>();
      if (this.m_Type == TimerType.TIMER_RESEARCH)
      {
        BuildingController university = CitadelSystem.inst.GetUniversity();
        if ((UnityEngine.Object) university != (UnityEngine.Object) null)
          transformList.Add(university.transform);
      }
      else if (this.m_Type == TimerType.TIMER_CONSTRUCT || this.m_Type == TimerType.TIMER_DESTRUCT || (this.m_Type == TimerType.TIMER_TRAIN || this.m_Type == TimerType.TIMER_TRAP_BUILD))
      {
        BuildingController buildingFromJobId = this.GetBuildingFromJobID();
        if ((UnityEngine.Object) buildingFromJobId != (UnityEngine.Object) null)
          transformList.Add(buildingFromJobId.transform);
      }
      else if (this.m_Type == TimerType.TIMER_HEAL)
      {
        using (List<BuildingController>.Enumerator enumerator = CitadelSystem.inst.GetHospitals().GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            BuildingController current = enumerator.Current;
            transformList.Add(current.transform);
          }
        }
      }
      else if (this.m_Type == TimerType.TIMER_FORGE)
      {
        BuildingController forge = CitadelSystem.inst.GetForge();
        if ((UnityEngine.Object) forge != (UnityEngine.Object) null)
          transformList.Add(forge.transform);
      }
      else if (this.m_Type == TimerType.TIMER_DK_FORGE)
      {
        BuildingController dkForge = CitadelSystem.inst.GetDKForge();
        if ((UnityEngine.Object) dkForge != (UnityEngine.Object) null)
          transformList.Add(dkForge.transform);
      }
      return transformList;
    }

    private BuildingController GetBuildingFromJobID()
    {
      BuildingController buildingController = (BuildingController) null;
      JobHandle unfinishedJob = JobManager.Instance.GetUnfinishedJob(this.m_JobID);
      if (unfinishedJob == null)
        return (BuildingController) null;
      Hashtable data = unfinishedJob.Data as Hashtable;
      if (data != null)
      {
        long buildingID = long.Parse(data[(object) "building_id"].ToString());
        buildingController = CitadelSystem.inst.GetBuildControllerFromBuildingItem(CityManager.inst.GetBuildingFromID(buildingID));
      }
      return buildingController;
    }
  }
}
