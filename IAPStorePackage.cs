﻿// Decompiled with JetBrains decompiler
// Type: IAPStorePackage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class IAPStorePackage
{
  public string id = string.Empty;
  public string group_id = string.Empty;
  public long startTime;
  public long endTime;
  public int quantity;
  public int max_quantity;
  public int pay_quantity;
  public bool isSpecialOffer;
  public IAPPackageInfo package;
  public IAPPackageGroupInfo group;
  public ConfigIAPFakePriceInfo priceInfo;
  public int paymentLevel;

  public int NextPackageId
  {
    get
    {
      return this.group.GetPackageIdByPosition(this.group.GetPackagePosition(this.package.internalId) + 1);
    }
  }

  public long PurchaseCount
  {
    get
    {
      return (long) this.quantity;
    }
  }

  public long MaxPurchaseCount
  {
    get
    {
      return (long) this.max_quantity;
    }
  }

  public long LeftPurchaseCount
  {
    get
    {
      return (long) (this.max_quantity - this.quantity);
    }
  }

  public bool CanPurchase
  {
    get
    {
      if (this.package.alliance_reward_group != 0 && PlayerData.inst.allianceId == 0L)
        return false;
      int serverTimestamp = NetServerTime.inst.ServerTimestamp;
      if (this.LeftPurchaseCount > 0L && this.startTime <= (long) serverTimestamp)
        return (long) serverTimestamp <= this.endTime;
      return false;
    }
  }

  public void Decode(Hashtable data)
  {
    DatabaseTools.UpdateData(data, "id", ref this.id);
    DatabaseTools.UpdateData(data, "group_id", ref this.group_id);
    DatabaseTools.UpdateData(data, "start_time", ref this.startTime);
    DatabaseTools.UpdateData(data, "end_time", ref this.endTime);
    DatabaseTools.UpdateData(data, "quantity", ref this.quantity);
    DatabaseTools.UpdateData(data, "max_quantity", ref this.max_quantity);
    DatabaseTools.UpdateData(data, "pay_quantity", ref this.pay_quantity);
    this.package = ConfigManager.inst.DB_IAPPackage.Get(this.id);
    this.group = ConfigManager.inst.DB_IAPPackageGroup.Get(this.group_id);
    this.priceInfo = ConfigManager.inst.DB_IAPPrice.Get(this.package.pay_id);
    this.paymentLevel = ConfigManager.inst.DB_IAPPaymentLevel.GetLevel(this.package.pay_id);
  }

  public long LeftTime
  {
    get
    {
      int serverTimestamp = NetServerTime.inst.ServerTimestamp;
      if (this.startTime <= (long) serverTimestamp && (long) serverTimestamp <= this.endTime)
        return this.endTime - (long) serverTimestamp;
      return 0;
    }
  }

  public int CalculatePriority(int type)
  {
    switch (type)
    {
      case 0:
      case 1:
      case 2:
        return 1;
      case 3:
        return 3;
      case 4:
        return 2;
      case 5:
        return 4;
      default:
        return 0;
    }
  }
}
