﻿// Decompiled with JetBrains decompiler
// Type: AltarSummonChoosePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class AltarSummonChoosePopup : Popup
{
  private List<AltarSummonChooseItemComponent> items = new List<AltarSummonChooseItemComponent>();
  private GameObjectPool objectPool = new GameObjectPool();
  private List<int> finalSelectedList = new List<int>();
  private List<int> tmpSelectedList = new List<int>();
  [SerializeField]
  private AltarSummonChooseItemComponent _template;
  [SerializeField]
  private UIGrid _grid;
  [SerializeField]
  private UILabel _chooseInfo;
  private int itemId;
  private List<ConfigAltarSummonRewardInfo> rewardPool;
  private int poolSize;
  private int requiredSize;
  private System.Action<List<int>> onRewardsChosen;

  public List<int> ChosenRewardIds
  {
    get
    {
      this.tmpSelectedList.Clear();
      for (int index = 0; index < this.poolSize; ++index)
      {
        if (this.items[index].Selected)
          this.tmpSelectedList.Add(this.items[index].RewardItemId);
      }
      return this.tmpSelectedList;
    }
  }

  public bool HandleCheckReachedMax()
  {
    return this.ChosenRewardIds.Count == this.requiredSize;
  }

  private void Init()
  {
    this.rewardPool = ConfigManager.inst.DB_ConfigAltarSummonRewardMain.GetRewardPoolByGroupId(AltarSummonHelper.GetGroupId(this.itemId));
    this.poolSize = this.rewardPool.Count;
    this.requiredSize = AltarSummonHelper.GetRequiredSize(this.itemId);
    this.objectPool.Initialize(this._template.gameObject, this._grid.gameObject);
  }

  private void SetupUI()
  {
    this.ResetItems();
    for (int index = 0; index < this.poolSize; ++index)
    {
      GameObject gameObject = this.objectPool.AddChild(this._grid.gameObject);
      gameObject.SetActive(true);
      AltarSummonChooseItemComponent component = gameObject.GetComponent<AltarSummonChooseItemComponent>();
      component.Selected = this.IsItemSelected(this.rewardPool[index].internalId);
      component.OnItemCheckChanged = new System.Action(this.OnItemCheckChanged);
      component.CheckReachedMaxFunc = new AltarSummonChooseItemComponent.CheckReachedMax(this.HandleCheckReachedMax);
      component.FeedData((IComponentData) new AltarSummonChooseItemComponentData()
      {
        altarSummonRewardId = this.rewardPool[index].internalId
      });
      this.items.Add(component);
    }
    this._grid.Reposition();
    this.UpdateSelectedLabel();
  }

  private void UpdateSelectedLabel()
  {
    int count = this.ChosenRewardIds.Count;
    if (count < this.requiredSize)
      this._chooseInfo.text = string.Format("{0}: [ff0000]{1}/{2}[-]", (object) Utils.XLAT("summon_rewards_altar_selected_num"), (object) count, (object) this.requiredSize);
    else
      this._chooseInfo.text = string.Format("{0}: {1}/{2}", (object) Utils.XLAT("summon_rewards_altar_selected_num"), (object) count, (object) this.requiredSize);
  }

  private bool IsItemSelected(int rewardId)
  {
    if (this.tmpSelectedList == null || this.tmpSelectedList.Count == 0)
      return false;
    int index = 0;
    for (int count = this.tmpSelectedList.Count; index < count; ++index)
    {
      if (this.tmpSelectedList[index] == rewardId)
        return true;
    }
    return false;
  }

  private void ResetItems()
  {
    int index = 0;
    for (int count = this.items.Count; index < count; ++index)
      this.objectPool.Release(this.items[index].gameObject);
    this.items.Clear();
  }

  public void OnConfirmClicked()
  {
    List<int> chosenRewardIds = this.ChosenRewardIds;
    if (chosenRewardIds.Count != this.requiredSize)
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_summon_rewards_altar_rewards_select_tip"), (System.Action) null, 4f, false);
    }
    else
    {
      if (this.onRewardsChosen == null)
        return;
      this.finalSelectedList.Clear();
      int index = 0;
      for (int count = chosenRewardIds.Count; index < count; ++index)
        this.finalSelectedList.Add(chosenRewardIds[index]);
      this.onRewardsChosen(this.finalSelectedList);
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    }
  }

  public void OnCloseButtonClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnItemCheckChanged()
  {
    this.UpdateSelectedLabel();
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.itemId = (orgParam as AltarSummonChoosePopup.Parameter).itemId;
    this.onRewardsChosen = (orgParam as AltarSummonChoosePopup.Parameter).onRewardsChosen;
    List<int> selectedIds = (orgParam as AltarSummonChoosePopup.Parameter).selectedIds;
    if (selectedIds != null)
    {
      this.tmpSelectedList.Clear();
      int index = 0;
      for (int count = selectedIds.Count; index < count; ++index)
        this.tmpSelectedList.Add(selectedIds[index]);
    }
    this.Init();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.SetupUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int itemId;
    public List<int> selectedIds;
    public System.Action<List<int>> onRewardsChosen;
  }
}
