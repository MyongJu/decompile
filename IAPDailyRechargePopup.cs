﻿// Decompiled with JetBrains decompiler
// Type: IAPDailyRechargePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class IAPDailyRechargePopup : Popup
{
  private List<IAPDailyRewardItemGroupRender> m_ItemList = new List<IAPDailyRewardItemGroupRender>();
  private List<UILabel> m_LableList = new List<UILabel>();
  private const string PackageGroupName = "daily_package";
  private const int PackageRefreshDelay = 2;
  private const string GOOGLE_CHANEL = "google";
  public GameObject itemGroupPrefab;
  public GameObject extralRewardsPrefab;
  public UIScrollView[] scrollViewArray;
  public UITable[] tableArray;
  public UILabel[] discountArrary;
  public UILabel[] prices;
  public UIButton[] buyButtons;
  public UILabel title;
  public UILabel megaLabel;
  public UILabel superLabel;
  public UILabel royalLabel;
  public UILabel subscribeLabel;
  public UIButton subscribeButton;
  public UILabel subscribeDescription;
  public GameObject subsribePanel;
  private IAPDailyRechargePackage cachedBoughtPackage;

  private void OnEnable()
  {
    SubscriptionSystem.Instance.subscriptionSuccessEvent += new System.Action(this.OnSubscriptionSuccess);
    IAPDailyRechargePackagePayload.Instance.onPackageDataReady += new System.Action(this.OnPackageDataReady);
    IAPDailyRechargePackagePayload.Instance.onPackagePurchased += new System.Action(this.CollectRewards);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void OnDisable()
  {
    SubscriptionSystem.Instance.subscriptionSuccessEvent -= new System.Action(this.OnSubscriptionSuccess);
    IAPDailyRechargePackagePayload.Instance.onPackageDataReady -= new System.Action(this.OnPackageDataReady);
    IAPDailyRechargePackagePayload.Instance.onPackagePurchased -= new System.Action(this.CollectRewards);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  private void OnSecondEvent(int serverTimestamp)
  {
    if (NetServerTime.inst.TodayLeftTimeUTC() != 86398)
      return;
    IAPDailyRechargePackagePayload.Instance.PurchasedPackageState.bought.Clear();
    IAPDailyRechargePackagePayload.Instance.RequestServerData((System.Action<bool, object>) null, false);
  }

  private void UpdateUI()
  {
    this.UpdatePanelLabels();
    this.UpdateData();
    this.RepositionContainers((MonoBehaviour[]) this.tableArray);
    this.RepositionContainers((MonoBehaviour[]) this.scrollViewArray);
    this.UpdateButtonState();
    this.UpdateSubscriptionState();
  }

  private void RepositionContainers(MonoBehaviour[] containers)
  {
    UITable[] uiTableArray = containers as UITable[];
    if (uiTableArray != null)
    {
      for (int index = 0; index < uiTableArray.Length; ++index)
        uiTableArray[index].Reposition();
    }
    else
    {
      UIScrollView[] uiScrollViewArray = containers as UIScrollView[];
      if (uiScrollViewArray == null)
        return;
      for (int index = 0; index < uiScrollViewArray.Length; ++index)
        uiScrollViewArray[index].ResetPosition();
    }
  }

  public void UpdateData()
  {
    this.ClearData();
    List<IAPDailyRechargePackage> rechargePackages = IAPDailyRechargePackagePayload.Instance.GetDailyRechargePackages();
    for (int index = 0; index < rechargePackages.Count; ++index)
    {
      this.UpdateData(rechargePackages[index].rewards, this.tableArray[index], this.scrollViewArray[index]);
      if (this.IsCorrectPlatformOrChannel())
      {
        IAPDailyPackageRewardsInfo subRewards = rechargePackages[index].sub_rewards;
        this.UpdateExtralRewardsLabel(this.tableArray[index], this.scrollViewArray[index]);
        this.UpdateData(subRewards, this.tableArray[index], this.scrollViewArray[index]);
      }
      this.discountArrary[index].text = rechargePackages[index].discount.ToString() + "%";
      double num = PaymentManager.Instance.GetRealPrice(rechargePackages[index].productId, rechargePackages[index].pay_id) * (double) rechargePackages[index].discount / 100.0;
      string currencyCode = PaymentManager.Instance.GetCurrencyCode(rechargePackages[index].productId, rechargePackages[index].pay_id);
      this.prices[index * 2].text = currencyCode + Utils.FormatThousands(num.ToString("f2"));
      this.prices[index * 2 + 1].text = PaymentManager.Instance.GetFormattedPrice(rechargePackages[index].productId, rechargePackages[index].pay_id);
    }
  }

  private void UpdateData(IAPDailyPackageRewardsInfo rewardInfo, UITable table, UIScrollView scrollView)
  {
    List<Reward.RewardsValuePair> rewards = rewardInfo.GetRewards();
    List<Reward.RewardsValuePair> infoList = new List<Reward.RewardsValuePair>();
    int num1 = 3;
    int num2 = 0;
    for (int index = 0; index < rewards.Count; ++index)
    {
      Reward.RewardsValuePair rewardsValuePair = rewards[index];
      if (++num2 % num1 == 0)
      {
        infoList.Add(rewardsValuePair);
        this.CreateItemGroup(infoList, table, scrollView);
        infoList.Clear();
      }
      else
        infoList.Add(rewardsValuePair);
    }
    if (infoList.Count <= 0)
      return;
    this.CreateItemGroup(infoList, table, scrollView);
  }

  private void CreateItemGroup(List<Reward.RewardsValuePair> infoList, UITable table, UIScrollView scrollView)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.itemGroupPrefab);
    gameObject.SetActive(true);
    gameObject.transform.parent = table.transform;
    gameObject.transform.localScale = Vector3.one;
    IAPDailyRewardItemGroupRender component = gameObject.GetComponent<IAPDailyRewardItemGroupRender>();
    component.SeedData(infoList, scrollView);
    this.m_ItemList.Add(component);
  }

  private void UpdateExtralRewardsLabel(UITable table, UIScrollView scrollView)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.extralRewardsPrefab);
    gameObject.SetActive(true);
    gameObject.transform.parent = table.transform;
    gameObject.transform.localScale = Vector3.one;
    UILabel componentInChildren = gameObject.GetComponentInChildren<UILabel>();
    componentInChildren.text = Utils.XLAT("iap_daily_gift_pack_rewards_description");
    gameObject.GetComponent<UIDragScrollView>().scrollView = scrollView;
    this.m_LableList.Add(componentInChildren);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    if (!IAPDailyRechargePackagePayload.Instance.HasRequested)
    {
      IAPDailyRechargePackagePayload.Instance.RequestServerData((System.Action<bool, object>) null, false);
      IAPDailyRechargePackagePayload.Instance.HasRequested = true;
    }
    if (IAPDailyRechargePackagePayload.Instance.Expire == 0 || NetServerTime.inst.ServerTimestamp < IAPDailyRechargePackagePayload.Instance.Expire)
      return;
    IAPDailyRechargePackagePayload.Instance.PurchasedPackageState.bought.Clear();
    IAPDailyRechargePackagePayload.Instance.RequestServerData((System.Action<bool, object>) null, false);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.ClearData();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
    Utils.RecordPopupId(this.ID);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    Utils.SetPopupLevel(this.ID);
  }

  public void OnPackageDataReady()
  {
    this.UpdateUI();
  }

  private void ClearData()
  {
    for (int index = 0; index < this.m_ItemList.Count; ++index)
    {
      this.m_ItemList[index].Release();
      this.m_ItemList[index].gameObject.SetActive(false);
      this.m_ItemList[index].transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_ItemList[index].gameObject);
    }
    this.m_ItemList.Clear();
    for (int index = 0; index < this.m_LableList.Count; ++index)
    {
      this.m_LableList[index].gameObject.SetActive(false);
      this.m_LableList[index].transform.parent.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_LableList[index].transform.parent.gameObject);
    }
    this.m_LableList.Clear();
    if (this.cachedBoughtPackage == null)
      return;
    this.cachedBoughtPackage = (IAPDailyRechargePackage) null;
  }

  private void UpdatePanelLabels()
  {
    this.title.text = Utils.XLAT("iap_daily_gift_pack_name");
    this.megaLabel.text = Utils.XLAT("iap_daily_gift_pack_mega_name");
    this.superLabel.text = Utils.XLAT("iap_daily_gift_pack_super_name");
    this.royalLabel.text = Utils.XLAT("iap_daily_gift_pack_royal_name");
  }

  private void UpdateSubscriptionState()
  {
    if (this.IsCorrectPlatformOrChannel())
    {
      this.subsribePanel.SetActive(true);
      if (SubscriptionSystem.Instance.HasStarted() && SubscriptionSystem.Instance.IsActive())
      {
        this.subscribeDescription.text = Utils.XLAT("iap_daily_gift_pack_subscribe_success_description");
        this.subscribeButton.gameObject.SetActive(false);
      }
      else
      {
        this.subscribeDescription.text = Utils.XLAT("iap_daily_gift_pack_description");
        this.subscribeLabel.text = Utils.XLAT("id_uppercase_subscribe");
        this.subscribeButton.gameObject.SetActive(true);
      }
    }
    else
      this.subsribePanel.SetActive(false);
  }

  private void UpdateButtonState()
  {
    List<IAPDailyRechargePackage> rechargePackages = IAPDailyRechargePackagePayload.Instance.GetDailyRechargePackages();
    IAPDialyRechargePackageState purchasedPackageState = IAPDailyRechargePackagePayload.Instance.PurchasedPackageState;
    if (rechargePackages != null && rechargePackages.Count > 0)
    {
      if (purchasedPackageState != null && purchasedPackageState.bought != null && purchasedPackageState.bought.Count > 0)
      {
        for (int index1 = 0; index1 < rechargePackages.Count; ++index1)
        {
          bool flag = false;
          for (int index2 = 0; index2 < purchasedPackageState.bought.Count; ++index2)
          {
            if (rechargePackages[index1].internalID == int.Parse(purchasedPackageState.bought[index2].ToString()))
            {
              this.UpdateButtonState(this.buyButtons[rechargePackages[index1].type - 1], false);
              flag = true;
              break;
            }
          }
          if (!flag)
            this.UpdateButtonState(this.buyButtons[rechargePackages[index1].type - 1], true);
        }
      }
      else
      {
        for (int index = 0; index < this.buyButtons.Length; ++index)
          this.UpdateButtonState(this.buyButtons[index], true);
      }
    }
    else
    {
      for (int index = 0; index < this.buyButtons.Length; ++index)
        this.buyButtons[index].isEnabled = false;
    }
  }

  private void UpdateButtonState(UIButton button, bool enabled)
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) button))
      return;
    button.isEnabled = enabled;
    button.transform.FindChild("Prices").gameObject.SetActive(enabled);
    button.transform.FindChild("Complete").gameObject.SetActive(!enabled);
    if (!button.transform.FindChild("Complete").gameObject.activeInHierarchy)
      return;
    button.transform.FindChild("Complete").gameObject.GetComponent<UILabel>().text = Utils.XLAT("id_purchased");
  }

  private bool IsCorrectPlatformOrChannel()
  {
    return CustomDefine.GetChannel() == "google" || Application.platform == RuntimePlatform.IPhonePlayer || (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor);
  }

  private void CollectReward(IAPDailyPackageRewardsInfo rewardInfo)
  {
    Dictionary<string, int>.Enumerator enumerator = rewardInfo.Rewards.GetEnumerator();
    while (enumerator.MoveNext())
    {
      int internalId = int.Parse(enumerator.Current.Key);
      int num = enumerator.Current.Value;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
      RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
      {
        count = (float) num,
        icon = itemStaticInfo.ImagePath
      });
    }
  }

  private void CollectRewards()
  {
    AudioManager.Instance.StopAndPlaySound("sfx_ui_signin");
    RewardsCollectionAnimator.Instance.Clear();
    if (this.cachedBoughtPackage == null)
      return;
    if (this.cachedBoughtPackage.rewards != null)
      this.CollectReward(this.cachedBoughtPackage.rewards);
    if (this.IsCorrectPlatformOrChannel() && this.cachedBoughtPackage.sub_rewards != null && (SubscriptionSystem.Instance.HasStarted() && SubscriptionSystem.Instance.IsActive()))
      this.CollectReward(this.cachedBoughtPackage.sub_rewards);
    RewardsCollectionAnimator.Instance.CollectItems(true);
  }

  private void OnSubscriptionSuccess()
  {
    this.UpdateSubscriptionState();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  public void OnSubscribeBtnPressed()
  {
    List<int> popupIds = IAPDailyRechargePackagePayload.Instance.popupIDs;
    bool flag = false;
    if (popupIds.Count == 2)
    {
      while (popupIds.Count > 0)
        UIManager.inst.ClosePopup(popupIds[0], (Popup.PopupParameter) null);
      flag = true;
    }
    if (flag)
    {
      UIManager.inst.OpenPopup("IAP/IAPDailyRechargePopup", (Popup.PopupParameter) null);
      UIManager.inst.OpenPopup("SubscibePopup", (Popup.PopupParameter) null);
    }
    else
      UIManager.inst.OpenPopup("SubscibePopup", (Popup.PopupParameter) null);
  }

  public void OnPurchaseBtnClicked(GameObject go)
  {
    List<IAPDailyRechargePackage> rechargePackages = IAPDailyRechargePackagePayload.Instance.GetDailyRechargePackages();
    IAPDailyRechargePackagePayload.Instance.BuyThroughDailyPackage = true;
    int index1 = -1;
    for (int index2 = 0; index2 < this.buyButtons.Length; ++index2)
    {
      if ((UnityEngine.Object) this.buyButtons[index2].gameObject == (UnityEngine.Object) go)
      {
        index1 = index2;
        break;
      }
    }
    if (index1 == -1)
      return;
    this.cachedBoughtPackage = rechargePackages[index1];
    IAPDailyRechargePackagePayload.Instance.CachedPurchasedPackage = this.cachedBoughtPackage;
    IAPDailyRechargePackagePayload.Instance.BuyProduct(rechargePackages[index1].productId, rechargePackages[index1].internalID.ToString(), "daily_package", (System.Action) (() => this.UpdateButtonState()));
  }
}
