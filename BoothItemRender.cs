﻿// Decompiled with JetBrains decompiler
// Type: BoothItemRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BoothItemRender : MonoBehaviour
{
  public UISprite add;
  public UISprite delete;
  public UITexture icon;
  public UITexture frame;
  public GameObject rewardCount;
  public UILabel count;
  public System.Action<BoothItemRender> OnBoothItemClickedHandler;
  public System.Action<BoothItemRender> OnEmptyBoothItemClickedHandler;
  private int itemId;
  private BoothItemData data;
  private bool isOccupied;

  public BoothItemData Data
  {
    get
    {
      return this.data;
    }
  }

  public bool IsOccupied
  {
    get
    {
      return this.isOccupied;
    }
  }

  public void FeedData(BoothItemData data)
  {
    if (data == null)
    {
      this.isOccupied = false;
      this.add.gameObject.SetActive(true);
      this.delete.gameObject.SetActive(false);
      this.icon.gameObject.SetActive(false);
      this.frame.gameObject.SetActive(false);
      this.rewardCount.gameObject.SetActive(false);
    }
    else
    {
      this.data = data;
      this.itemId = data.itemId;
      this.isOccupied = true;
      this.count.text = data.itemCount.ToString();
      this.UpdateIcon();
      this.add.gameObject.SetActive(false);
      this.delete.gameObject.SetActive(true);
      this.icon.gameObject.SetActive(true);
      this.frame.gameObject.SetActive(true);
      this.rewardCount.gameObject.SetActive(true);
    }
  }

  private void UpdateIcon()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.itemId);
    if (itemStaticInfo == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    Utils.SetItemBackground(this.frame, this.itemId);
  }

  public void OnItemClicked()
  {
    if (this.OnBoothItemClickedHandler == null)
      return;
    this.OnBoothItemClickedHandler(this);
  }

  public void OnEmptyItemClicked()
  {
    if (this.OnEmptyBoothItemClickedHandler == null)
      return;
    this.OnEmptyBoothItemClickedHandler(this);
  }
}
