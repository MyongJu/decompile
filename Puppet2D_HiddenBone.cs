﻿// Decompiled with JetBrains decompiler
// Type: Puppet2D_HiddenBone
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class Puppet2D_HiddenBone : MonoBehaviour
{
  public Transform boneToAimAt;
  public bool InEditBoneMode;
  public GameObject[] _newSelection;

  private void LateUpdate()
  {
    if (!this.GetComponent<Renderer>().enabled)
      return;
    if ((bool) ((Object) this.boneToAimAt) && (bool) ((Object) this.transform.parent))
    {
      Transform parent = this.transform.parent;
      this.transform.parent = (Transform) null;
      if ((double) Vector3.Distance(this.boneToAimAt.position, this.transform.position) > 0.0)
        this.transform.rotation = Quaternion.LookRotation(this.boneToAimAt.position - this.transform.position, Vector3.forward) * Quaternion.AngleAxis(90f, Vector3.right);
      float magnitude = (this.boneToAimAt.position - this.transform.position).magnitude;
      this.transform.localScale = new Vector3(magnitude, magnitude, magnitude);
      if ((bool) ((Object) parent))
      {
        this.transform.parent = parent;
        this.transform.position = parent.position;
        if ((bool) ((Object) parent.GetComponent<SpriteRenderer>()))
          this.transform.GetComponent<SpriteRenderer>().sortingLayerName = parent.GetComponent<SpriteRenderer>().sortingLayerName;
      }
      this.transform.hideFlags = HideFlags.HideInHierarchy | HideFlags.NotEditable;
    }
    else
      Object.DestroyImmediate((Object) this.gameObject);
  }

  public void Refresh()
  {
    if (!(bool) ((Object) this.boneToAimAt))
      return;
    Transform parent = this.transform.parent;
    this.transform.parent = (Transform) null;
    if ((double) Vector3.Distance(this.boneToAimAt.position, this.transform.position) > 0.0)
      this.transform.rotation = Quaternion.LookRotation(this.boneToAimAt.position - this.transform.position, Vector3.forward) * Quaternion.AngleAxis(90f, Vector3.right);
    float magnitude = (this.boneToAimAt.position - this.transform.position).magnitude;
    this.transform.localScale = new Vector3(magnitude, magnitude, magnitude);
    if (!(bool) ((Object) parent))
      return;
    this.transform.parent = parent;
    this.transform.position = parent.position;
    if (!(bool) ((Object) parent.GetComponent<SpriteRenderer>()))
      return;
    this.transform.GetComponent<SpriteRenderer>().sortingLayerName = parent.GetComponent<SpriteRenderer>().sortingLayerName;
  }
}
