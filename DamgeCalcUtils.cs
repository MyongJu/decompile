﻿// Decompiled with JetBrains decompiler
// Type: DamgeCalcUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class DamgeCalcUtils
{
  public static int CalcSpellDamge(int magicDamge, double spellDamgeCoefficient = 1.0)
  {
    return -Mathf.Max(0, (int) Math.Ceiling((double) magicDamge * spellDamgeCoefficient));
  }

  public static int CalcSpellHeal(int magicDamge, double spellHealCoefficient = 1.0)
  {
    return Mathf.Max(0, (int) Math.Ceiling((double) magicDamge * spellHealCoefficient));
  }

  public static int CalcPhysicsDamge(int damge, int defense, double spellDamgeCoefficient = 1.0)
  {
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("dragon_knight_defence_rate");
    return -Math.Max(0, (int) Math.Ceiling(((double) damge - (double) defense * data.Value) * spellDamgeCoefficient));
  }
}
