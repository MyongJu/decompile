﻿// Decompiled with JetBrains decompiler
// Type: PrivilegeInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class PrivilegeInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "type")]
  public string type;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "image")]
  public string image;

  public string Name
  {
    get
    {
      return ScriptLocalization.Get(this.name, true);
    }
  }

  public string ImagePath
  {
    get
    {
      return "Texture/Benefits/" + this.image;
    }
  }
}
