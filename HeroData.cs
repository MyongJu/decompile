﻿// Decompiled with JetBrains decompiler
// Type: HeroData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;

public class HeroData
{
  private UnitState _state = UnitState.guarding;
  public const int InvalidID = 0;
  public const int AccessorySlots = 3;
  public const int InvalidAccessorySlot = -1;
  public const string HeroClassString = "hero";
  private int _id;
  private int _level;
  private int _experience;
  private Properties _properties;
  private string _heroName;
  private int _heroPortraitIndex;
  private long _troopId;

  private HeroData(int inID)
  {
    this._id = inID;
    this._properties = new Properties(this._id);
  }

  public string heroName
  {
    get
    {
      return this._heroName;
    }
    set
    {
      this._heroName = value;
      GameEngine.Instance.events.Send_OnHeroStatChanged(new Events.HeroStatChangedArgs(this.ID, Events.HeroStat.Information, 0.0, 0.0));
    }
  }

  public int heroPortraitIndex
  {
    get
    {
      return this._heroPortraitIndex;
    }
    set
    {
      this._heroPortraitIndex = value;
      GameEngine.Instance.events.Send_OnHeroStatChanged(new Events.HeroStatChangedArgs(this.ID, Events.HeroStat.Information, 0.0, 0.0));
    }
  }

  public int ID
  {
    get
    {
      return this._id;
    }
    private set
    {
      this._id = value;
    }
  }

  public int Level
  {
    get
    {
      return this._level;
    }
  }

  private int PowerForLevel(int level)
  {
    int num = 0;
    Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData("hero", level);
    if (data != null)
      num = data.Power;
    return num;
  }

  public int Experience
  {
    get
    {
      return this._experience;
    }
    set
    {
      this.SetExperience((int) Property.EffectiveValue((float) value, Property.XP_BONUS, this._id, Effect.ConditionType.all), true);
    }
  }

  private void SetExperience(int experience, bool sendEvent)
  {
    if ((experience == this._experience || experience < 0) && experience != 0)
      return;
    int experience1 = this._experience;
    this._experience = experience;
    int level1 = this._level;
    int level2 = ConfigManager.inst.DB_Hero_Point.LevelForXP(this._experience);
    bool flag = level2 != level1;
    if (flag)
    {
      this._level = level2;
      GameEngine.Instance.events.Publish_onPowerIncreased(this.PowerForLevel(level2) - this.PowerForLevel(level1));
    }
    if (!sendEvent)
      return;
    GameEngine.Instance.events.Send_OnHeroStatChanged(new Events.HeroStatChangedArgs(this._id, Events.HeroStat.Experience, (double) experience1, (double) experience));
    if (!flag)
      return;
    GameEngine.Instance.events.Send_OnHeroStatChanged(new Events.HeroStatChangedArgs(this._id, Events.HeroStat.Level, (double) level1, (double) level2));
  }

  public long troopId
  {
    get
    {
      return this._troopId;
    }
    set
    {
      this.SetTroops(value);
    }
  }

  private void SetTroops(long newTroopId)
  {
    this._troopId = newTroopId;
  }

  public static HeroData CreateHeroData(int inID)
  {
    if (inID == 0)
      return (HeroData) null;
    return new HeroData(inID);
  }

  public Properties Properties
  {
    get
    {
      return this._properties;
    }
  }

  public UnitState State
  {
    get
    {
      return this._state;
    }
    set
    {
      if (this._state == value)
        return;
      UnitState state = this._state;
      this._state = value;
      GameEngine.Instance.events.Send_OnHeroStateChanged(new Events.HeroStateChangedArgs(this._id, state, this._state));
    }
  }

  public void LoadHeroData(Hashtable inHashTable)
  {
    if (inHashTable == null)
      return;
    this.SetExperience(int.Parse(inHashTable[(object) "xp"].ToString()), false);
    this.heroName = inHashTable[(object) "name"].ToString();
    this.heroPortraitIndex = Convert.ToInt32(inHashTable[(object) "icon"]);
  }

  public InventoryItemData GetAccessory(int inSlot)
  {
    return (InventoryItemData) null;
  }

  public int GetAccessorySlot(InventoryItemData inAccessory)
  {
    return -1;
  }

  public int GetFreeAccessorySlot()
  {
    return -1;
  }

  public bool ReceivedSetAccessory(InventoryItemData inAccessory, int inSlot)
  {
    return true;
  }

  public bool SendSwapAccessory(int inSlot, InventoryItemData inAccessoryInInventory)
  {
    return false;
  }

  public void LoaclSwapAccessory(int slot1, int slot2)
  {
  }

  public void OnEquipmentDataChanged(long itemId)
  {
  }

  public void SendSetHead(Hero_EquipmentData inHeadData)
  {
  }

  public void SendSetHair(Hero_EquipmentData inHairData)
  {
  }

  private void RemoveEquipmentEffects(long itemID, bool debug = true)
  {
  }

  private void RemoveEquipmentSetEffects(long itemID, bool debug = true)
  {
  }

  public delegate void OnDataSetAccessory(InventoryItemData inEquipment, int inSlot);
}
