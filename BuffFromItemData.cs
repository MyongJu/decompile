﻿// Decompiled with JetBrains decompiler
// Type: BuffFromItemData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;

public class BuffFromItemData : BuffUIData
{
  private List<ItemStaticInfo> currentitems = new List<ItemStaticInfo>();
  private List<ItemStaticInfo> _totalItems;

  public List<ItemStaticInfo> Currentitems
  {
    get
    {
      return this.currentitems;
    }
  }

  public override void Refresh()
  {
    BoostStaticInfo data = this.Data as BoostStaticInfo;
    ConfigManager.inst.DB_Items.GetItem(data.Item_InternalID);
    this.Name = ScriptLocalization.Get(data.Loc_Name, true);
    this.Desc = ScriptLocalization.Get(data.Loc_Description, true);
    this.Icon = data.Image;
    this.IconPath = Utils.GetUITextPath() + data.Image;
    this.Category = data.Category;
    this.Type = data.Type;
  }

  public List<ItemStaticInfo> FindItems()
  {
    BoostStaticInfo data = this.Data as BoostStaticInfo;
    List<ItemStaticInfo> itemStaticInfoList1 = new List<ItemStaticInfo>();
    List<ItemStaticInfo> itemStaticInfoList2 = new List<ItemStaticInfo>();
    using (Dictionary<int, BoostStaticInfo>.ValueCollection.Enumerator enumerator = ConfigManager.inst.DB_Boosts.Item2Boosts.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BoostStaticInfo current = enumerator.Current;
        if (current.Category == this.Category && current.Type == data.Type)
        {
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(current.Item_InternalID);
          if (ItemBag.Instance.GetItemCount(current.Item_InternalID) > 0)
            itemStaticInfoList2.Add(itemStaticInfo);
          else if (itemStaticInfo != null && current.ShopItemID > 0 && current.OnSale)
            itemStaticInfoList1.Add(itemStaticInfo);
        }
      }
    }
    itemStaticInfoList2.AddRange((IEnumerable<ItemStaticInfo>) itemStaticInfoList1);
    itemStaticInfoList2.Sort(new Comparison<ItemStaticInfo>(this.CompareItem2));
    this._totalItems = itemStaticInfoList2;
    return this._totalItems;
  }

  private int CompareItem2(ItemStaticInfo a, ItemStaticInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }

  public override bool IsRunning()
  {
    return (double) this.GetProgressValue() > 0.0;
  }

  public override int RemainTime()
  {
    if (this.IsRunning())
    {
      JobHandle job = this.GetJob((this.Data as BoostStaticInfo).Type);
      if (job != null)
        return job.LeftTime();
    }
    return 0;
  }

  public override float GetProgressValue()
  {
    JobHandle job = this.GetJob((this.Data as BoostStaticInfo).Type);
    if (job != null)
      return (float) job.LeftTime() / (float) job.Duration();
    return 0.0f;
  }

  private JobHandle GetJob(BoostType type)
  {
    return JobManager.Instance.GetSingleJobByClass(Utils.BoostType2JobEvent(type));
  }
}
