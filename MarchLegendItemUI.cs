﻿// Decompiled with JetBrains decompiler
// Type: MarchLegendItemUI
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class MarchLegendItemUI : MonoBehaviour
{
  public UITexture legendIconTexture;
  public UILabel legendLevel;
  public UILabel legendPower;
  public GameObject content;

  public void SetLengendInfo(LegendData src)
  {
    LegendInfo legendInfo = ConfigManager.inst.DB_Legend.GetLegendInfo(src.LegendID);
    this.legendLevel.text = "Lv." + src.Level.ToString();
    this.legendPower.text = src.Power.ToString();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.legendIconTexture, legendInfo.Icon, (System.Action<bool>) null, true, false, string.Empty);
  }

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.legendIconTexture);
    NGUITools.SetActive(this.content, false);
  }
}
