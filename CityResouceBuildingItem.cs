﻿// Decompiled with JetBrains decompiler
// Type: CityResouceBuildingItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UnityEngine;

public class CityResouceBuildingItem : MonoBehaviour
{
  private int MAX_SIZE = 320;
  private Dictionary<string, GameObject> buildings = new Dictionary<string, GameObject>();
  [HideInInspector]
  public CityManager.BuildingItem mBuildingItem;
  public UILabel level;
  public UILabel speed;
  public GameObject buildingContainer;
  private ResouceGenSpeedData data;
  private GameObject building;
  public GameObject itemRoot;

  private void UpdateUI()
  {
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData(this.mBuildingItem.mType, this.mBuildingItem.mLevel);
    string path = "Prefab/UI/BuildingPrefab/ui_" + data.Building_ImagePath + "_l1";
    double generateInHour = (double) data.GenerateInHour;
    this.data.Refresh();
    this.RefreshBuildingIcon(path);
    this.level.text = ScriptLocalization.Get("id_level", true) + " " + (object) this.mBuildingItem.mLevel;
    string empty = string.Empty;
    float num = (float) (CitadelSystem.inst.GetBuildControllerFromBuildingItem(this.mBuildingItem) as BuildingControllerResource).GenerateSpeed * 3600f;
    string str = (double) num < generateInHour ? "-" : "+";
    this.speed.text = "[B4B4B4]" + Utils.ConvertNumberToNormalString((long) generateInHour) + "[-][54AE07]" + str + Utils.ConvertNumberToNormalString((long) Math.Abs((double) num - generateInHour)) + "[-]";
  }

  private GameObject GetItem(string path)
  {
    GameObject go = (GameObject) null;
    if (this.buildings.ContainsKey(path))
    {
      go = this.buildings[path];
      go.transform.parent = this.buildingContainer.transform;
    }
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
    {
      go = NGUITools.AddChild(this.buildingContainer, AssetManager.Instance.HandyLoad(path, (System.Type) null) as GameObject);
      this.buildings.Add(path, go);
    }
    NGUITools.SetActive(go, true);
    Utils.SetLayer(go, this.gameObject.layer);
    return go;
  }

  private void Release(GameObject go)
  {
    NGUITools.SetActive(go, false);
    go.transform.parent = this.itemRoot.transform;
  }

  private void RefreshBuildingIcon(string path)
  {
    this.building = this.GetItem(path);
    if (!((UnityEngine.Object) this.building != (UnityEngine.Object) null))
      return;
    this.building.GetComponent<UI2DSprite>().depth = 3;
    UI2DSprite component = this.building.GetComponent<UI2DSprite>();
    component.depth = 4;
    if (component.width > this.MAX_SIZE)
    {
      float num = (float) this.MAX_SIZE / (float) component.width;
      component.width = this.MAX_SIZE;
      component.height = (int) ((double) component.height * (double) num);
    }
    if (component.height <= this.MAX_SIZE)
      return;
    float num1 = (float) this.MAX_SIZE / (float) component.height;
    component.height = this.MAX_SIZE;
    component.width = (int) ((double) component.width * (double) num1);
  }

  private void Init()
  {
    DBManager.inst.DB_CityMap.onDataUpdated += new System.Action<CityMapData>(this.OnUpgradeBuilding);
  }

  public void Clear()
  {
    this.mBuildingItem = (CityManager.BuildingItem) null;
    if ((UnityEngine.Object) this.building != (UnityEngine.Object) null)
      this.building.SetActive(false);
    DBManager.inst.DB_CityMap.onDataUpdated -= new System.Action<CityMapData>(this.OnUpgradeBuilding);
  }

  private void OnDestroy()
  {
    if (!GameEngine.IsAvailable || GameEngine.IsShuttingDown)
      return;
    DBManager.inst.DB_CityMap.onDataUpdated -= new System.Action<CityMapData>(this.OnUpgradeBuilding);
  }

  public void OnUpgradeBuilding(CityMapData cmd)
  {
    if (!this.IsID(cmd.buildingId))
      return;
    this.UpdateUI();
  }

  public bool IsID(long ID)
  {
    if (this.mBuildingItem != null)
      return this.mBuildingItem.mID == ID;
    return false;
  }

  public void SetData(CityManager.BuildingItem mBuildingItem, ResouceGenSpeedData data)
  {
    this.Clear();
    this.Init();
    this.mBuildingItem = mBuildingItem;
    this.data = data;
    this.UpdateUI();
  }

  public string GamePath
  {
    get
    {
      return "Prefab/UI/BuildingPrefab/ui_";
    }
  }
}
