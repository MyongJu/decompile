﻿// Decompiled with JetBrains decompiler
// Type: QuestBuildPresent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class QuestBuildPresent : BasePresent
{
  private bool _needUpgrade;
  private bool _needDisplayCount;

  public override void Refresh()
  {
    string empty = string.Empty;
    int level = -1;
    string formula = this.Formula;
    int num1 = formula.IndexOf('&');
    int num2 = formula.IndexOf('=');
    int num3 = formula.LastIndexOf('=');
    int count = this.Count;
    this._needDisplayCount = false;
    string buildingType;
    if (num1 > -1)
    {
      buildingType = formula.Substring(num2 + 1, num1 - num2 - 1);
      level = int.Parse(formula.Substring(num3 + 1));
    }
    else
      buildingType = formula.Substring(num2 + 1);
    int num4 = CityManager.inst.GetBuildingCount(buildingType, true);
    int num5 = CityManager.inst.GetBuildMaxLevel(buildingType);
    int num6 = CityManager.inst.GetBuildingCountOverLevel(level, buildingType);
    this._needDisplayCount = count > 1;
    this._needUpgrade = num4 != 0 && level >= 0 && (level != 1 || count != 1) && count == 1;
    if (level <= 0)
    {
      if (num4 > count)
        num4 = count;
      this.ProgressValue = (float) num4 / (float) count;
      this.ProgressContent = num4.ToString() + "/" + (object) count;
    }
    else if (count > 1)
    {
      if (num6 > count)
        num6 = count;
      this.ProgressValue = (float) num6 / (float) count;
      this.ProgressContent = num6.ToString() + "/" + (object) count;
    }
    else
    {
      if (num5 > level)
        num5 = level;
      this.ProgressValue = (float) num5 / (float) level;
      this.ProgressContent = num5.ToString() + "/" + (object) level;
    }
  }

  public override bool NeedDisplayCount
  {
    get
    {
      return this._needDisplayCount;
    }
  }

  public override int IconType
  {
    get
    {
      if (this.Data.Type == PresentData.DataType.Achievement)
        return base.IconType;
      return 2;
    }
  }

  public override bool IsUpgarde
  {
    get
    {
      if (this.Data.Type == PresentData.DataType.Quest)
        return this._needUpgrade;
      return false;
    }
  }
}
