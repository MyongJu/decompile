﻿// Decompiled with JetBrains decompiler
// Type: RallyFortressData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;

public class RallyFortressData : RallyBuildingData
{
  private AllianceFortressData _fortressData;

  public RallyFortressData(int x, int y)
    : base(x, y)
  {
  }

  public RallyFortressData(long id)
    : base(id)
  {
  }

  private AllianceFortressData FortressData
  {
    get
    {
      if (this._fortressData == null)
        this._fortressData = this.id <= 0L ? DBManager.inst.DB_AllianceFortress.Get(this.x, this.y) : DBManager.inst.DB_AllianceFortress.Get(this.id);
      return this._fortressData;
    }
  }

  public override string BuildName
  {
    get
    {
      return AllianceBuildUtils.GetFortressName(this.FortressData.fortressId);
    }
  }

  public override long MaxCount
  {
    get
    {
      return this.FortressData.MaxTroopsCount;
    }
  }

  public override long OwnerID
  {
    get
    {
      return AllianceBuildUtils.GetFortressOwner(this.FortressData.fortressId);
    }
  }

  public override RallyBuildingData.DataType Type
  {
    get
    {
      return RallyBuildingData.DataType.Fortress;
    }
  }

  public override Coordinate Location
  {
    get
    {
      return this.FortressData.Location;
    }
  }

  public override List<MarchData> MarchList
  {
    get
    {
      return AllianceBuildUtils.GetMarchsByFortressID(this.FortressData.fortressId);
    }
  }

  public override MarchAllocDlg.MarchType ReinforceMarchType
  {
    get
    {
      return MarchAllocDlg.MarchType.fortressReinforce;
    }
  }

  public override void Ready(System.Action callBack)
  {
    AllianceBuildUtils.LoadMaxTroopCount(this.FortressData, callBack);
  }
}
