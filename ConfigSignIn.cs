﻿// Decompiled with JetBrains decompiler
// Type: ConfigSignIn
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigSignIn
{
  private Dictionary<int, SignInData> m_SignInDataDictByInternalId;
  private Dictionary<string, SignInData> m_SignInDataDictByUniqueId;
  private Dictionary<int, SignInData> m_SignInDataDictByTimes;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<SignInData, string>(result as Hashtable, "ID", out this.m_SignInDataDictByUniqueId, out this.m_SignInDataDictByInternalId);
    this.m_SignInDataDictByTimes = new Dictionary<int, SignInData>();
    Dictionary<int, SignInData>.Enumerator enumerator = this.m_SignInDataDictByInternalId.GetEnumerator();
    while (enumerator.MoveNext())
      this.m_SignInDataDictByTimes[enumerator.Current.Value.Times] = enumerator.Current.Value;
  }

  public SignInData GetByInternalId(int internalId)
  {
    SignInData signInData;
    this.m_SignInDataDictByInternalId.TryGetValue(internalId, out signInData);
    return signInData;
  }

  public SignInData GetByUniqueId(string uniqueId)
  {
    SignInData signInData;
    this.m_SignInDataDictByUniqueId.TryGetValue(uniqueId, out signInData);
    return signInData;
  }

  public SignInData GetByTimes(int times)
  {
    SignInData signInData = (SignInData) null;
    this.m_SignInDataDictByTimes.TryGetValue(times, out signInData);
    return signInData;
  }
}
