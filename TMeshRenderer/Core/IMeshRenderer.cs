﻿// Decompiled with JetBrains decompiler
// Type: TMeshRenderer.Core.IMeshRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace TMeshRenderer.Core
{
  public interface IMeshRenderer
  {
    bool ShowVoronoi { get; set; }

    bool ShowRegions { get; set; }

    void Zoom(float x, float y, int delta);

    void HandleResize();

    void Initialize();

    void SetData(RenderData data);
  }
}
