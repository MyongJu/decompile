﻿// Decompiled with JetBrains decompiler
// Type: TMeshRenderer.Core.RenderData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TriangleNet;
using TriangleNet.Data;
using TriangleNet.Geometry;
using TriangleNet.Tools;

namespace TMeshRenderer.Core
{
  public class RenderData
  {
    public float[] Points;
    public uint[] Segments;
    public uint[] Triangles;
    public uint[] MeshEdges;
    public float[] VoronoiPoints;
    public uint[] VoronoiEdges;
    public int[] TrianglePartition;
    public int NumberOfRegions;
    public int NumberOfInputPoints;
    public BoundingBox Bounds;

    public void SetInputGeometry(InputGeometry data)
    {
      this.Segments = (uint[]) null;
      this.Triangles = (uint[]) null;
      this.MeshEdges = (uint[]) null;
      this.VoronoiPoints = (float[]) null;
      this.VoronoiEdges = (uint[]) null;
      int count1 = data.Count;
      int num = 0;
      this.NumberOfRegions = data.Regions.Count;
      this.NumberOfInputPoints = count1;
      this.Points = new float[2 * count1];
      using (List<Vertex>.Enumerator enumerator = data.Points.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Vertex current = enumerator.Current;
          this.Points[2 * num] = (float) current.X;
          this.Points[2 * num + 1] = (float) current.Y;
          ++num;
        }
      }
      int count2 = data.Segments.Count;
      if (count2 > 0)
      {
        List<uint> uintList = new List<uint>(2 * count2);
        foreach (Edge segment in (IEnumerable<Edge>) data.Segments)
        {
          uintList.Add((uint) segment.P0);
          uintList.Add((uint) segment.P1);
        }
        this.Segments = uintList.ToArray();
      }
      this.Bounds = new BoundingBox((float) data.Bounds.Xmin, (float) data.Bounds.Xmax, (float) data.Bounds.Ymin, (float) data.Bounds.Ymax);
    }

    public void SetMesh(Mesh mesh)
    {
      this.Segments = (uint[]) null;
      this.VoronoiPoints = (float[]) null;
      this.VoronoiEdges = (uint[]) null;
      int count1 = mesh.Vertices.Count;
      int num1 = 0;
      this.NumberOfInputPoints = mesh.NumberOfInputPoints;
      mesh.Renumber();
      this.Points = new float[2 * count1];
      foreach (Vertex vertex in (IEnumerable<Vertex>) mesh.Vertices)
      {
        this.Points[2 * num1] = (float) vertex.X;
        this.Points[2 * num1 + 1] = (float) vertex.Y;
        ++num1;
      }
      int count2 = mesh.Segments.Count;
      if (count2 > 0 && mesh.IsPolygon)
      {
        List<uint> uintList = new List<uint>(2 * count2);
        foreach (Segment segment in (IEnumerable<Segment>) mesh.Segments)
        {
          uintList.Add((uint) segment.P0);
          uintList.Add((uint) segment.P1);
        }
        this.Segments = uintList.ToArray();
      }
      List<uint> uintList1 = new List<uint>(2 * mesh.NumberOfEdges);
      EdgeEnumerator edgeEnumerator = new EdgeEnumerator(mesh);
      while (edgeEnumerator.MoveNext())
      {
        uintList1.Add((uint) edgeEnumerator.Current.P0);
        uintList1.Add((uint) edgeEnumerator.Current.P1);
      }
      this.MeshEdges = uintList1.ToArray();
      if (this.NumberOfRegions > 0)
        this.TrianglePartition = new int[mesh.Triangles.Count];
      int num2 = 0;
      List<uint> uintList2 = new List<uint>(3 * mesh.Triangles.Count);
      foreach (Triangle triangle in (IEnumerable<Triangle>) mesh.Triangles)
      {
        uintList2.Add((uint) triangle.P0);
        uintList2.Add((uint) triangle.P1);
        uintList2.Add((uint) triangle.P2);
        if (this.NumberOfRegions > 0)
          this.TrianglePartition[num2++] = triangle.Region;
      }
      this.Triangles = uintList2.ToArray();
      this.Bounds = new BoundingBox((float) mesh.Bounds.Xmin, (float) mesh.Bounds.Xmax, (float) mesh.Bounds.Ymin, (float) mesh.Bounds.Ymax);
    }

    public void SetVoronoi(IVoronoi voro)
    {
      this.SetVoronoi(voro, 0);
    }

    public void SetVoronoi(IVoronoi voro, int infCount)
    {
      this.VoronoiPoints = new float[2 * voro.Points.Length + infCount];
      foreach (Point point in voro.Points)
      {
        if (!(point == (Point) null))
        {
          int id = point.ID;
          this.VoronoiPoints[2 * id] = (float) point.X;
          this.VoronoiPoints[2 * id + 1] = (float) point.Y;
        }
      }
      List<uint> uintList = new List<uint>(voro.Regions.Count * 4);
      using (List<VoronoiRegion>.Enumerator enumerator1 = voro.Regions.GetEnumerator())
      {
        while (enumerator1.MoveNext())
        {
          VoronoiRegion current1 = enumerator1.Current;
          Point point1 = (Point) null;
          Point point2 = (Point) null;
          using (List<Point>.Enumerator enumerator2 = current1.Vertices.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              Point current2 = enumerator2.Current;
              if (point1 == (Point) null)
              {
                point1 = current2;
                point2 = current2;
              }
              else
              {
                uintList.Add((uint) point2.ID);
                uintList.Add((uint) current2.ID);
                point2 = current2;
              }
            }
          }
          if (current1.Bounded && point1 != (Point) null)
          {
            uintList.Add((uint) point2.ID);
            uintList.Add((uint) point1.ID);
          }
        }
      }
      this.VoronoiEdges = uintList.ToArray();
    }
  }
}
