﻿// Decompiled with JetBrains decompiler
// Type: TMeshRenderer.Core.BoundingBox
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using TriangleNet.Geometry;
using UnityEngine;

namespace TMeshRenderer.Core
{
  public struct BoundingBox
  {
    public float Left;
    public float Right;
    public float Bottom;
    public float Top;

    public BoundingBox(float left, float right, float bottom, float top)
    {
      this.Left = left;
      this.Right = right;
      this.Bottom = bottom;
      this.Top = top;
    }

    public float Width
    {
      get
      {
        return this.Right - this.Left;
      }
    }

    public float Height
    {
      get
      {
        return this.Top - this.Bottom;
      }
    }

    public void Update(Point pt)
    {
      this.Update(pt.x, pt.y);
    }

    public void Update(Vector2 pt)
    {
      this.Update((double) pt.x, (double) pt.y);
    }

    public void Update(double x, double y)
    {
      if ((double) this.Left > x)
        this.Left = (float) x;
      if ((double) this.Right < x)
        this.Right = (float) x;
      if ((double) this.Bottom > y)
        this.Bottom = (float) y;
      if ((double) this.Top >= y)
        return;
      this.Top = (float) y;
    }

    public void Reset()
    {
      this.Left = float.MaxValue;
      this.Right = float.MinValue;
      this.Bottom = float.MaxValue;
      this.Top = float.MinValue;
    }
  }
}
