﻿// Decompiled with JetBrains decompiler
// Type: PVPSystem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Troops;
using UI;
using UnityEngine;

public class PVPSystem
{
  private GameObject _root;
  private PVPController _controller;
  private DynamicMapData _mapData;
  private static PVPSystem _instance;

  public static PVPSystem Instance
  {
    get
    {
      if (PVPSystem._instance == null)
        PVPSystem._instance = new PVPSystem();
      return PVPSystem._instance;
    }
  }

  public bool IsAvailable
  {
    get
    {
      return (bool) ((UnityEngine.Object) this._root);
    }
  }

  public bool IsReady
  {
    get
    {
      return this._controller.IsReady;
    }
  }

  public PVPMap Map
  {
    get
    {
      return this._controller.Map;
    }
  }

  public TroopViewPool troopViewManager
  {
    get
    {
      return this._controller.troopViewManager;
    }
  }

  public PVPController Controller
  {
    get
    {
      return this._controller;
    }
  }

  public void Setup(DynamicMapData mapData)
  {
    this.Shutdown();
    this._mapData = mapData;
    this._root = AssetManager.Instance.HandyLoad("Prefab/Kingdom/PVPSystem", (System.Type) null) as GameObject;
    this._root = UnityEngine.Object.Instantiate<GameObject>(this._root);
    this._controller = this._root.GetComponent<PVPController>();
    this._controller.Setup(mapData);
    UIManager.inst.tileCamera.gameObject.SetActive(true);
  }

  public void Shutdown()
  {
    UIManager.inst.tileCamera.gameObject.SetActive(false);
    if ((bool) ((UnityEngine.Object) this._controller))
      this._controller.Shutdown();
    if ((bool) ((UnityEngine.Object) this._root))
      UnityEngine.Object.Destroy((UnityEngine.Object) this._root);
    this._root = (GameObject) null;
    this._controller = (PVPController) null;
    this._mapData = (DynamicMapData) null;
  }

  public void Show(bool focus = true)
  {
    this._root.SetActive(true);
    this._controller.Show(focus);
  }

  public void Hide()
  {
    this._root.SetActive(false);
    this._controller.Hide();
  }
}
