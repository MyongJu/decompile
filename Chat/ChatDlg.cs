﻿// Decompiled with JetBrains decompiler
// Type: Chat.ChatDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

namespace Chat
{
  public class ChatDlg : UI.Dialog
  {
    private bool canChatNow = true;
    private List<ChatBar> _chatBars = new List<ChatBar>();
    public const int CHAT_CHANNEL_TYPE_KINGDOM = 0;
    public const int CHAT_CHANNEL_TYPE_ALLIANCE = 1;
    public const int CHAT_CHANNEL_TYPE_PRIVATE = 2;
    private const float FIXED_CHAT_CD_TIME = 3f;
    public ChatContactList contactList;
    public ChatInputContainer input;
    public UIButton ModButton;
    public UISprite modeOpenSprite;
    private bool _init;
    public static ChatDlg.OpenType openType;
    public ChatRoomViewController chatRoomListView;
    private float chatCDTime;
    private int configedCDTime;
    private Coroutine chatCoroutine;
    private bool needGoBottom;
    private ChatManager _chatManager;
    private ChatChannel __channel;
    private int _chatBarTop;
    public bool isPrivateChannel;
    private long privateTargetChannelId;
    [SerializeField]
    private ChatDlg.Panel panel;

    private void InitDlg()
    {
      if (this._init)
        return;
      this._init = true;
      this.panel.chatScrollView.onPortaitClick = new System.Action<ChatMessage>(this.OnChatPortaitClick);
      this._chatManager = GameEngine.Instance.ChatManager;
      this._chatManager.onPrivateUnreadChanged += new System.Action<int>(this.OnPrivateChannelUnreadChanged);
      this.panel.msgTable.onReposition = new UITable.OnReposition(this.OnMsgTableReset);
      this.panel.defaultText.text = Utils.XLAT("chat_message_character_limit");
      this.panel.defaultText2.text = Utils.XLAT("chat_message_character_limit");
      this.CheckSilence();
      ChatDlg.openType = ChatDlg.OpenType.kingdom;
      this.panel.privateNotifyCount = this._chatManager.GetPrivateChannelUnreadCount();
    }

    public void OnTagRoomButtonClick()
    {
      ChatDlg.openType = ChatDlg.OpenType.room;
      this.CloseModMode();
      this._chatManager.ResetChatConstraint();
      MessageHub.inst.GetPortByAction("Chat:getInitChatRooms").SendRequest((Hashtable) null, (System.Action<bool, object>) null, true);
      this.input.SelectedIndex = 1;
      this.panel.tabs.SetCurrentTab(2, true);
      this.panel.isNewMessageReceived = false;
      this.panel.inputConatiner.gameObject.SetActive(false);
      this.panel.msgContainer.gameObject.SetActive(false);
      this.input.Clear();
      if (this._channel != null)
      {
        this._channel.onMessageSended = (System.Action<ChatMessage>) null;
        this._channel.onMessageReceived = (System.Action<ChatMessage>) null;
        this._channel.onHistoryLoaded = (System.Action<long>) null;
        this._channel = (ChatChannel) null;
      }
      this.contactList.Hide();
      this.panel.BT_Contact_List.gameObject.SetActive(false);
      this.chatRoomListView.Show(this._chatManager, this._channel, this);
    }

    public void OnTagKingdomButtonClick()
    {
      this.chatRoomListView.Hide();
      ChatDlg.openType = ChatDlg.OpenType.kingdom;
      this._chatManager.ResetChatConstraint();
      this.panel.tabs.SetCurrentTab(0, true);
      this.input.SelectedIndex = 0;
      this.input.Clear();
      UIManager.inst.publicHUD.chatMiniDlg.isKingdomChannel = true;
      this.SelectChannel(this._chatManager.kingdomChannelId, 0L);
      this.panel.msgScrollView.ResetPosition();
      this.panel.isNewMessageReceived = false;
      this.panel.inputConatiner.gameObject.SetActive(true);
      this.CheckSilence();
      this.panel.channelTitle.text = Utils.XLAT("chat_kingdom_chat_subtitle");
      this.panel.msgContainer.gameObject.SetActive(true);
      this.contactList.Hide();
      this.panel.BT_Contact_List.gameObject.SetActive(false);
      this.ModButton.gameObject.SetActive(false);
      this.input.UseLongInput(true);
      if (!PlayerData.inst.moderatorInfo.IsModerator)
        return;
      this.ModButton.gameObject.SetActive(true);
      this.input.UseLongInput(false);
    }

    public void OnTagAllianceButtonClick()
    {
      this.CloseModMode();
      this._chatManager.ResetChatConstraint();
      this.chatRoomListView.Hide();
      ChatDlg.openType = ChatDlg.OpenType.alliance;
      this.contactList.Hide();
      this.panel.isNewMessageReceived = false;
      this.panel.BT_Contact_List.gameObject.SetActive(false);
      if (this._chatManager.allianceChannelId != 0L)
      {
        UIManager.inst.publicHUD.chatMiniDlg.isKingdomChannel = false;
        this.panel.tabs.SetCurrentTab(1, true);
        this.input.SelectedIndex = 1;
        this.SelectChannel(this._chatManager.allianceChannelId, 0L);
        this.panel.msgScrollView.ResetPosition();
        this.panel.inputConatiner.gameObject.SetActive(true);
        this.CheckSilence();
        this.panel.channelTitle.text = Utils.XLAT("chat_alliance_chat_subtitle");
        this.input.Clear();
        this.panel.msgContainer.gameObject.SetActive(true);
      }
      else
        UIManager.inst.OpenDlg("Alliance/AllianceJoinDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    }

    public void OnTagPrivateButtonClick()
    {
      this.CloseModMode();
      this._chatManager.ResetChatConstraint();
      this.chatRoomListView.Hide();
      ChatDlg.openType = ChatDlg.OpenType.personal;
      this.panel.isNewMessageReceived = false;
      this.panel.tabs.SetCurrentTab(3, true);
      this.input.SelectedIndex = 1;
      this.panel.inputConatiner.gameObject.SetActive(false);
      this.panel.channelTitle.text = Utils.XLAT("chat_alliance_private_title");
      this.input.Clear();
      this.panel.msgContainer.gameObject.SetActive(false);
      if (this._channel != null)
      {
        this._channel.onMessageSended = (System.Action<ChatMessage>) null;
        this._channel.onMessageReceived = (System.Action<ChatMessage>) null;
        this._channel.onHistoryLoaded = (System.Action<long>) null;
        this._channel = (ChatChannel) null;
      }
      this.panel.BT_Contact_List.gameObject.SetActive(true);
      this.contactList.Show();
    }

    public void OnAllianceQuit()
    {
      if (this.isPrivateChannel || this._chatManager.GetChatChannel(this._channel.channelId, 0L) != null)
        return;
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }

    public void OnCloseButtonClick()
    {
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }

    public void OnContactButtonClick()
    {
      UIManager.inst.OpenPopup("Chat/ChatContactPopup", (Popup.PopupParameter) new ChatContactPopup.Parameter()
      {
        lshowPendingInvited = false
      });
    }

    public void OnHypelinkButtonClick()
    {
    }

    public void OnCreateAllianceClick()
    {
    }

    public void OnJoinAllianceClick()
    {
    }

    public void OnSendButtonClick()
    {
      int levelLimit = ChatAndMailConstraint.Instance.LevelLimit;
      bool flag = ChatAndMailConstraint.Instance.SwitchState && !ChatAndMailConstraint.Instance.HasBindAccount() && CityManager.inst.mStronghold.mLevel < levelLimit;
      if (ChatDlg.openType == ChatDlg.OpenType.kingdom && flag)
        UIManager.inst.toast.Show(ScriptLocalization.GetWithMultyContents("toast_kingdom_talk_use_forbidden", true, levelLimit.ToString()), (System.Action) null, 4f, false);
      else if (PlayerData.inst.userData.IsShutUp)
      {
        UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
        {
          Title = Utils.XLAT("chat_silenced_title"),
          Content = ScriptLocalization.GetWithMultyContents("chat_silenced_description", true, Utils.FormatTime(PlayerData.inst.userData.ShutUpLeftTime, false, false, true)),
          Okay = Utils.XLAT("id_uppercase_okay"),
          OkayCallback = (System.Action) null
        });
      }
      else
      {
        string inputText = this.input.InputText;
        if (string.IsNullOrEmpty(inputText) || string.IsNullOrEmpty(inputText.Trim()))
          return;
        if (!this.isPrivateChannel)
        {
          if (this.input.IsUseMegahone())
            this.ShowMegahoneUse();
          else
            this.SendContent();
        }
        else
        {
          UserData userByChannelId = DBManager.inst.DB_User.GetUserByChannelId(this.privateTargetChannelId);
          if (userByChannelId == null)
            return;
          if (DBManager.inst.DB_Contacts.friends.Contains(userByChannelId.uid))
            this.SendPrivateMsg();
          else
            UIManager.inst.toast.Show(Utils.XLAT("toast_chat_private_must_be_contact"), (System.Action) null, 4f, false);
        }
      }
    }

    private int GetSQChatType()
    {
      switch (ChatDlg.openType)
      {
        case ChatDlg.OpenType.kingdom:
          return 1;
        case ChatDlg.OpenType.alliance:
          return 2;
        case ChatDlg.OpenType.room:
          return 3;
        case ChatDlg.OpenType.personal:
          return 5;
        default:
          return 1;
      }
    }

    private void ShowMegahoneUse()
    {
      if (MsgService.Instance.RTMFilter)
        this.ConfirmMegahoneUse();
      else
        MsgService.Instance.RemoteCheckChatContent(this.input.InputText, 2, (System.Action<bool, string>) ((canSend, content) =>
        {
          if (!canSend)
            UIManager.inst.toast.Show(Utils.XLAT("toast_all_name_failure"), (System.Action) null, 4f, false);
          else
            this.ConfirmMegahoneUse();
        }));
    }

    private void ConfirmMegahoneUse()
    {
      string str = "item_megaphone";
      UIManager.inst.OpenPopup("UseItemConfirmPopUp", (Popup.PopupParameter) new UseItemConfirmPopUp.Parameter()
      {
        itemID = str,
        buttonLabel = ScriptLocalization.Get("chat_uppercase_send_button", true),
        param = new Hashtable()
        {
          {
            (object) "content",
            (object) this.input.InputText
          }
        },
        onUseCallBack = new System.Action(this.SendMegahoneContent)
      });
    }

    private void SendMegahoneContent()
    {
      if (CustomDefine.IsSQWanPackage())
        SQChatService.Instance.RemoteCheckChatContent(this.input.InputText, this.GetSQChatType(), (SQHttpPostProcessor.SQPostCallback) null);
      ChatMessageManager.SendSpeakerMessage(IllegalWordsUtils.Filter(this.input.InputText));
      this.input.Clear();
    }

    private void SendContent()
    {
      if (this._chatManager.CanChat)
      {
        this.SendConfirmContent();
        this._chatManager.BeginChatConstraint();
      }
      else
        this._chatManager.ShowChatCDToast();
    }

    private void SendConfirmContent()
    {
      if (CustomDefine.IsSQWanPackage())
        SQChatService.Instance.RemoteCheckChatContent(this.input.InputText, this.GetSQChatType(), (SQHttpPostProcessor.SQPostCallback) null);
      long channleId = this._channel.channelId;
      long allianceChannelId = this._chatManager.allianceChannelId;
      ChatMessage message = this._chatManager.ConstructMessage(channleId, this.input.InputText, (ChatTraceData) null);
      if (MsgService.Instance.RTMFilter)
      {
        if (channleId == allianceChannelId && PlayerData.inst.allianceData != null && (PlayerData.inst.allianceData.members != null && PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid) != null) && PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid).title >= 4)
          MsgService.Instance.SendGroupMessage(channleId, message, true);
        else
          MsgService.Instance.SendGroupMessage(channleId, message, false);
      }
      else
        MsgService.Instance.RemoteCheckChatContent(this.input.InputText, 2, (System.Action<bool, string>) ((canSend, content) =>
        {
          if ((UnityEngine.Object) this.gameObject == (UnityEngine.Object) null || !canSend)
            return;
          message.chatText = content;
          this._chatManager.SendGroupMessage(channleId, message, false, true);
          if (channleId != allianceChannelId || PlayerData.inst.allianceData == null || (PlayerData.inst.allianceData.members == null || PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid) == null) || PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid).title < 4)
            return;
          message.chatText = string.Format("{0}:{1}", (object) PlayerData.inst.userData.userName, (object) content);
          this._chatManager.PushGroupMessage(channleId, message);
        }));
      this._chatManager.SendGroupMessage(this._channel.channelId, IllegalWordsUtils.Filter(this.input.InputText), (ChatTraceData) null, true, false);
      this.input.Clear();
    }

    private void SendPrivateMsg()
    {
      if (this._chatManager.CanChat)
      {
        this.SendConfirmPrivateMsg();
        this._chatManager.BeginChatConstraint();
      }
      else
        this._chatManager.ShowChatCDToast();
    }

    private void SendConfirmPrivateMsg()
    {
      if (CustomDefine.IsSQWanPackage())
        SQChatService.Instance.RemoteCheckChatContent(this.input.InputText, this.GetSQChatType(), (SQHttpPostProcessor.SQPostCallback) null);
      if (MsgService.Instance.RTMFilter)
      {
        MsgService.Instance.SendMessage(this.privateTargetChannelId, this._chatManager.ConstructMessage(this.privateTargetChannelId, this.input.InputText, (ChatTraceData) null));
      }
      else
      {
        long privateChannel = this.privateTargetChannelId;
        MsgService.Instance.RemoteCheckChatContent(this.input.InputText, 2, (System.Action<bool, string>) ((canSend, content) =>
        {
          if (!canSend)
            return;
          this._chatManager.SendPersonalMessage(privateChannel, content, (ChatTraceData) null, false, true);
        }));
      }
      this._chatManager.SendPersonalMessage(this.privateTargetChannelId, IllegalWordsUtils.Filter(this.input.InputText), (ChatTraceData) null, true, false);
      this.input.Clear();
    }

    public void SelectChannel(long channelId, long targetChannelId = 0)
    {
      if (this._channel != null)
      {
        this._channel.onMessageSended = (System.Action<ChatMessage>) null;
        this._channel.onMessageReceived = (System.Action<ChatMessage>) null;
        this._channel.onHistoryLoaded = (System.Action<long>) null;
      }
      this.isPrivateChannel = channelId == this._chatManager.userChannelId;
      this._channel = this._chatManager.GetChatChannel(channelId, targetChannelId);
      this.privateTargetChannelId = targetChannelId;
      if (this._channel != null)
      {
        this._channel.onMessageSended = new System.Action<ChatMessage>(this.OnChannelMessageReceived);
        this._channel.onMessageReceived = new System.Action<ChatMessage>(this.OnChannelMessageReceived);
        this._channel.onHistoryLoaded = new System.Action<long>(this.OnChannelHistoryLoaded);
        if (this.isPrivateChannel)
          this._channel.LoadPersonalHistory();
        else
          this._channel.LoadGroupMessage();
        this._channel.ReadMessages();
      }
      this.RefreshMsgPanel(true);
    }

    private void OpenPrivateChannel(long targetChannelId, long targetUserId, string channelName)
    {
      this.CloseModMode();
      this.panel.tabs.SetCurrentTab(3, true);
      this.panel.channelTitle.text = channelName;
      this.panel.msgContainer.gameObject.SetActive(true);
      this.panel.inputConatiner.gameObject.SetActive(true);
      this.SelectChannel(this._chatManager.userChannelId, targetChannelId);
      this.CheckSilence();
    }

    private void RefreshMsgPanel(bool needResetPosition = true)
    {
      this.GoBottom(true);
    }

    [DebuggerHidden]
    private IEnumerator RepositionDelay(bool needResetPosition = true)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ChatDlg.\u003CRepositionDelay\u003Ec__Iterator30()
      {
        \u003C\u003Ef__this = this
      };
    }

    public void GoBottom(bool intance = true)
    {
      this.panel.chatScrollView.SetMessageBottom(-1);
      this.panel.isNewMessageReceived = false;
    }

    private void OnChannelMessageReceived(ChatMessage message, bool needResetPosition)
    {
      if (!this.isPrivateChannel)
        this.AddMessage(message);
      else if (message.senderChannelId == this.privateTargetChannelId || message.receiveChannelId == this.privateTargetChannelId)
        this.AddMessage(message);
      if (this._channel == null)
        return;
      this._channel.ReadMessages();
    }

    private void OnChannelMessageReceived(ChatMessage message)
    {
      this.OnChannelMessageReceived(message, true);
    }

    private void AddMessage(ChatMessage message)
    {
      if (message.senderUid == PlayerData.inst.uid)
        this.GoBottom(true);
      else if (this.panel.chatScrollView.atBottom)
        this.GoBottom(true);
      else
        this.panel.isNewMessageReceived = true;
    }

    public void OnMsgStopMoving()
    {
    }

    public void OnMsgTableReset()
    {
      if (!this.needGoBottom)
        return;
      this.GoBottom(true);
    }

    private void OnChatPortaitClick(ChatMessage message)
    {
      if (message.senderUid == PlayerData.inst.uid)
        return;
      int num = !message.isMod ? 0 : 1;
      UIManager.inst.OpenPopup("Chat/ChatMenuPopup", (Popup.PopupParameter) new ChatMenuPopup.Paramer()
      {
        personalChannelId = message.senderChannelId,
        personalName = message.senderName,
        senderCustomIcon = message.senderCustomIconUrl,
        personalUid = message.senderUid,
        personMuteAdmin = num,
        personIcon = message.senderIcon,
        content = message.chatText
      });
    }

    private void OnChatBarClick(ChatMessage message)
    {
    }

    public void ShowPersonalChannel(long uid)
    {
      this.ShowPersonalChannel(DBManager.inst.DB_User.Get(uid));
    }

    public void ShowPersonalChannel(UserData userData)
    {
      if (userData == null)
        return;
      this.OpenPrivateChannel(userData.channelId, userData.uid, userData.userName_Kingdom_Alliance_Name);
    }

    public void OnPrivateChannelUnreadChanged(int count)
    {
      this.panel.privateNotifyCount = count;
    }

    public void OnChannelHistoryLoaded(long channelId)
    {
      if (this._channel == null || this._channel.channelId != channelId)
        return;
      this.GoBottom(true);
    }

    public void OnTextChanged()
    {
      this.input.InputText = Utils.GetLimitStr(this.input.InputText.Replace("[", "【").Replace("]", "】"), 140);
      this.input.CheckBt();
      this.CheckSilence();
    }

    private bool CheckAtBottom()
    {
      if (this._chatBars != null && this._chatBars.Count > 1 && (this._chatBars.Count >= this._chatBarTop && this._chatBarTop > 0))
        return (double) this._chatBars[this._chatBarTop - 1].transform.position.y > -1.0;
      return true;
    }

    public void OnContactBarClick(long uid)
    {
      this.ShowPersonalChannel(DBManager.inst.DB_User.Get(uid));
    }

    public void OnContactListShowClick()
    {
      this.contactList.Show();
    }

    public void OnGoBottomClick()
    {
      this.GoBottom(true);
      this.panel.isNewMessageReceived = false;
    }

    public void OnEnable()
    {
      if (this._channel != null)
      {
        this._channel.onMessageSended = new System.Action<ChatMessage>(this.OnChannelMessageReceived);
        this._channel.onMessageReceived = new System.Action<ChatMessage>(this.OnChannelMessageReceived);
        this._channel.onHistoryLoaded = new System.Action<long>(this.OnChannelHistoryLoaded);
        this._channel.onHistoryLoaded = (System.Action<long>) null;
      }
      this.contactList.onContactBarClick = new System.Action<long>(this.OnContactBarClick);
      AllianceManager.Instance.onLeave += new System.Action(this.OnAllianceQuit);
    }

    public void OnDisable()
    {
      if (this._channel != null)
      {
        this._channel.onMessageSended = (System.Action<ChatMessage>) null;
        this._channel.onMessageReceived = (System.Action<ChatMessage>) null;
        this._channel.onHistoryLoaded = (System.Action<long>) null;
      }
      if (!GameEngine.IsReady())
        return;
      AllianceManager.Instance.onLeave -= new System.Action(this.OnAllianceQuit);
    }

    private ChatChannel _channel
    {
      get
      {
        return this.__channel;
      }
      set
      {
        if (this.__channel != value)
        {
          if (value == null)
          {
            this.panel.chatScrollView.enabled = false;
            this.panel.chatScrollView.channel = (ChatChannel) null;
          }
          else
          {
            this.panel.chatScrollView.enabled = true;
            this.panel.chatScrollView.channel = value;
            this.panel.chatScrollView.SetMessageBottom(-1);
          }
          this.__channel = value;
        }
        if (this._channel != null)
          return;
        this.panel.chatScrollView.enabled = false;
        this.panel.chatScrollView.channel = (ChatChannel) null;
      }
    }

    public void Update()
    {
      if (!this.panel.chatScrollView.atBottom)
        return;
      this.panel.isNewMessageReceived = false;
    }

    public void CheckSilence()
    {
      if (this._channel != null)
      {
        ChatRoomData roomByChannelId = DBManager.inst.DB_room.GetRoomByChannelId(this._channel.channelId);
        if (roomByChannelId != null && roomByChannelId.members.ContainsKey(PlayerData.inst.uid))
        {
          ChatRoomMember member = roomByChannelId.members[PlayerData.inst.uid];
          if (member != null && member.status == ChatRoomMember.Status.silence)
          {
            this.panel.BT_Send.isEnabled = false;
            this.panel.BT_Send_Alliance.isEnabled = false;
            this.panel.sendSprite.spriteName = "red_cross";
            this.panel.sendSprite_alliance.spriteName = "red_cross";
            UIManager.inst.toast.Show(Utils.XLAT("toast_chat_admin_silenced"), (System.Action) null, 4f, false);
            return;
          }
        }
      }
      this.input.CheckBt();
      this.panel.sendSprite.spriteName = "icon_send";
      this.panel.sendSprite_alliance.spriteName = "icon_send";
    }

    public override void OnCreate(UIControler.UIParameter orgParam)
    {
      base.OnCreate(orgParam);
      this.InitDlg();
    }

    private void updateModMode()
    {
      this.modeOpenSprite.gameObject.SetActive(PlayerData.inst.moderatorInfo.IsModerator && PlayerData.inst.moderatorInfo.OpenModMode);
    }

    private void OnModBtn()
    {
      PlayerData.inst.moderatorInfo.OpenModMode = !PlayerData.inst.moderatorInfo.OpenModMode;
      this.updateModMode();
    }

    private void CloseModMode()
    {
      PlayerData.inst.moderatorInfo.OpenModMode = false;
      this.updateModMode();
    }

    public override void OnShow(UIControler.UIParameter orgParam)
    {
      base.OnShow(orgParam);
      this.panel.chatScrollView.SetMessageBottom(-1);
      this.chatRoomListView.UpdateRoomTabSymbol();
      this.updateModMode();
      EventDelegate.Add(this.ModButton.onClick, new EventDelegate.Callback(this.OnModBtn));
    }

    public override void OnHide(UIControler.UIParameter orgParam)
    {
      this.panel.chatScrollView.Release();
      base.OnHide(orgParam);
      EventDelegate.Remove(this.ModButton.onClick, new EventDelegate.Callback(this.OnModBtn));
      this._chatManager.ResetChatConstraint();
    }

    public override void OnOpen(UIControler.UIParameter orgParam)
    {
      base.OnOpen(orgParam);
      ChatDlg.Parameter parameter = orgParam as ChatDlg.Parameter;
      if (parameter != null)
      {
        if (parameter.isOpenFriend)
          UIManager.inst.OpenPopup("Chat/ChatContactPopup", (Popup.PopupParameter) new ChatContactPopup.Parameter()
          {
            lshowPendingInvited = true
          });
        switch (parameter.channelType)
        {
          case ChatDlg.Parameter.ChannelType.kingdom:
            this.OnTagKingdomButtonClick();
            break;
          case ChatDlg.Parameter.ChannelType.alliance:
            this.OnTagAllianceButtonClick();
            break;
          case ChatDlg.Parameter.ChannelType.personal:
            this.gameObject.SetActive(true);
            this.InitDlg();
            ChatDlg.openType = ChatDlg.OpenType.personal;
            this.CloseModMode();
            this.panel.tabs.SetCurrentTab(3, true);
            UserData userData = DBManager.inst.DB_User.Get(parameter.privateUid);
            this.SelectChannel(this._chatManager.userChannelId, userData.channelId);
            this.panel.channelTitle.text = userData.userName;
            this.panel.msgContainer.gameObject.SetActive(true);
            this.panel.msgScrollView.ResetPosition();
            this.panel.inputConatiner.gameObject.SetActive(true);
            this.CheckSilence();
            this.chatRoomListView.Hide();
            break;
        }
        if (!parameter.isOpenMegaphone)
          return;
        this.input.OpenMegaphone();
      }
      else
      {
        switch (ChatDlg.openType)
        {
          case ChatDlg.OpenType.kingdom:
            this.OnTagKingdomButtonClick();
            break;
          case ChatDlg.OpenType.alliance:
            if (PlayerData.inst.allianceId != 0L)
            {
              this.OnTagAllianceButtonClick();
              break;
            }
            this.OnTagKingdomButtonClick();
            break;
          case ChatDlg.OpenType.room:
            this.OnTagRoomButtonClick();
            break;
          default:
            this.OnTagKingdomButtonClick();
            break;
        }
      }
    }

    public struct Params
    {
      public const string CHANNLE_TYPE = "chatChannel";
      public const string PRIVATE_CHANNEL_ID = "privateChannelId";
      public const string PRIVATE_CHANNEL_NAME = "privateChannelName";
    }

    public enum OpenType
    {
      kingdom,
      alliance,
      room,
      personal,
    }

    [Serializable]
    protected class Panel
    {
      public ChatScrollView chatScrollView;
      public Transform msgContainer;
      public UIScrollView msgScrollView;
      public UITable msgTable;
      public ChatBar orgChatBar;
      public UILabel channelTitle;
      public Transform tagContainer;
      public PageTabsMonitor tabs;
      public Transform privateNotifyContainer;
      public UILabel privateNotifyText;
      public Transform inputConatiner;
      public UISprite sendSprite;
      public UISprite sendSprite_alliance;
      public UIButton BT_Send;
      public UIButton BT_Send_Alliance;
      public UIInput input;
      public UILabel defaultText;
      public UILabel defaultText2;
      public UIButton BT_Contact;
      public UIButton BT_Contact_List;
      public UIButton BT_Close;
      public UIButton BT_Hypelink;
      public UIButton BT_ToBottom;

      public bool isNewMessageReceived
      {
        get
        {
          return this.BT_ToBottom.gameObject.activeSelf;
        }
        set
        {
          this.BT_ToBottom.gameObject.SetActive(value);
        }
      }

      public int privateNotifyCount
      {
        set
        {
          if (value <= 0)
          {
            this.privateNotifyContainer.gameObject.SetActive(false);
          }
          else
          {
            this.privateNotifyContainer.gameObject.SetActive(true);
            if (value >= 100)
              this.privateNotifyText.text = "99+";
            else
              this.privateNotifyText.text = value.ToString();
          }
        }
      }
    }

    public class Parameter : UI.Dialog.DialogParameter
    {
      public ChatDlg.Parameter.ChannelType channelType;
      public long privateUid;
      public bool isOpenMegaphone;
      public bool isOpenFriend;

      public enum ChannelType
      {
        kingdom,
        alliance,
        personal,
      }
    }
  }
}
