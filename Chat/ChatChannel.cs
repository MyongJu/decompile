﻿// Decompiled with JetBrains decompiler
// Type: Chat.ChatChannel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Rtm.Connection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace Chat
{
  public class ChatChannel
  {
    private int _unreadCount = -1;
    private int _readChannelTime = -1;
    private Stopwatch watch = new Stopwatch();
    public const int CHAT_MESSAGE_INIT_COUNT = 20;
    public readonly long channelId;
    public List<ChatMessage> messages;
    private bool _isLoadHistory;
    public System.Action<ChatMessage> onMessageReceived;
    public System.Action<ChatMessage> onMessageSended;
    public System.Action<ChatMessage> onNewMessage;
    public System.Action<long> onHistoryLoaded;
    public System.Action<long> onUnreadCountChanged;
    public string channelName;
    public string channelWelcomeMsg;

    public ChatChannel(long inChannelId)
    {
      this.channelId = inChannelId;
      this.messages = new List<ChatMessage>(20);
      ChatRoomData chatRoomData = DBManager.inst.DB_room.Get(this.channelId);
      if (chatRoomData != null)
      {
        this.channelName = chatRoomData.roomName;
        this.channelWelcomeMsg = chatRoomData.roomTitle;
      }
      else
        this.channelName = this.channelWelcomeMsg = string.Empty;
      this._isLoadHistory = false;
    }

    public int unreadCount
    {
      get
      {
        if (this._unreadCount < 0)
          this._unreadCount = PlayerPrefsEx.GetInt(this.localKey, 0);
        return this._unreadCount;
      }
      private set
      {
        if (value == this._unreadCount)
          return;
        PlayerPrefsEx.SetInt(this.localKey, value);
        PlayerPrefsEx.Save();
        this._unreadCount = value;
        if (this.onUnreadCountChanged == null)
          return;
        this.onUnreadCountChanged(this.channelId);
      }
    }

    public int readChannelTime
    {
      get
      {
        if (this._readChannelTime < 0)
          this._readChannelTime = PlayerPrefs.GetInt(this.localReadTimeKey, 0);
        return this._readChannelTime;
      }
      set
      {
        if (value <= this._readChannelTime)
          return;
        PlayerPrefsEx.SetInt(this.localReadTimeKey, value);
        PlayerPrefsEx.Save();
        this._readChannelTime = value;
      }
    }

    private string localKey
    {
      get
      {
        return string.Format("{0}{1}_{2}", (object) "chat_unread_", (object) this.channelId, (object) PlayerData.inst.uid);
      }
    }

    private string localReadTimeKey
    {
      get
      {
        return string.Format("{0}{1}_{2}", (object) "chat_read_time_", (object) this.channelId, (object) PlayerData.inst.uid);
      }
    }

    public void ReadMessages()
    {
      this.unreadCount = 0;
      this.readChannelTime = NetServerTime.inst.ServerTimestamp;
    }

    public void MessageReceived(ChatMessage message)
    {
      bool flag = ChatUtils.CheckMessageShouldHide(message);
      if (message != null && flag)
        return;
      this.messages.Add(message);
      if (message.sendTime > this.readChannelTime)
        ++this.unreadCount;
      if (this.onNewMessage != null)
        this.onNewMessage(message);
      if (this.onMessageReceived == null)
        return;
      this.onMessageReceived(message);
    }

    public void MessageSend(ChatMessage message)
    {
      if (DBManager.inst != null && DBManager.inst.DB_Contacts.blocking.Contains(message.senderUid))
        return;
      this.messages.Add(message);
      if (this.onNewMessage != null)
        this.onNewMessage(message);
      if (this.onMessageSended == null)
        return;
      this.onMessageSended(message);
    }

    public void LoadGroupMessage()
    {
      if (this._isLoadHistory)
        return;
      this.watch.Reset();
      this.watch.Start();
      PushManager.inst.GroupHistoryMessage(this.channelId, 100, -1L, new System.Action<MsgResult>(this.LoadHistoryCallBack), (byte) 100);
    }

    public void LoadPersonalHistory()
    {
      if (this._isLoadHistory)
        return;
      this.watch.Reset();
      this.watch.Start();
      PushManager.inst.PersonalHistoryMessage(this.channelId, 100, -1L, new System.Action<MsgResult>(this.LoadHistoryCallBack));
    }

    private void SetHistoryTime(int time)
    {
      UserData userData = DBManager.inst.DB_User.Get(PlayerData.inst.uid);
      CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
      AllianceData allianceData = (AllianceData) null;
      DB.WorldMapData worldMapData = (DB.WorldMapData) null;
      if (userData != null)
        allianceData = DBManager.inst.DB_Alliance.Get(userData.allianceId);
      if (cityData != null)
        worldMapData = DBManager.inst.DB_WorldMap.Get(userData.world_id);
      if (this.channelId == userData.channelId)
        NetWorkDetector.Instance.RTM_PERSONAL_HISTORY_TIME = time;
      if (allianceData != null && this.channelId == allianceData.chatChannelId)
        NetWorkDetector.Instance.RTM_ALLIANCE_HISTORY_TIME = time;
      if (worldMapData == null || this.channelId != worldMapData.chatChannelId)
        return;
      NetWorkDetector.Instance.RTM_KINGDOM_HISTORY_TIME = time;
    }

    private void LoadHistoryCallBack(MsgResult result)
    {
      this._isLoadHistory = true;
      this.SetHistoryTime((int) this.watch.ElapsedMilliseconds);
      this.watch.Stop();
      List<ChatMessage> chatMessageList = new List<ChatMessage>();
      int num = 0;
      for (int index = result.Msgs.Count - 1; index >= 0; --index)
      {
        try
        {
          Hashtable hashtable = Utils.Json2Object(result.Msgs[index].Body, true) as Hashtable;
          if (hashtable != null)
          {
            if (hashtable.Contains((object) "cmd"))
            {
              if (hashtable[(object) "cmd"].ToString().CompareTo("chat_message") == 0)
              {
                ChatMessage message = new ChatMessage();
                message.Decode(hashtable[(object) "payload"]);
                if (this.channelId != message.senderChannelId)
                {
                  if (this.channelId != message.receiveChannelId)
                    continue;
                }
                if (!ChatUtils.CheckMessageShouldHide(message))
                {
                  chatMessageList.Add(message);
                  if (message.sendTime > this.readChannelTime)
                  {
                    if (message.senderUid != PlayerData.inst.uid)
                      ++num;
                  }
                }
              }
            }
          }
        }
        catch (Exception ex)
        {
          D.error((object) ("[History Message]Error:" + ex.Message));
        }
      }
      this.unreadCount = num;
      if (chatMessageList.Count <= 0)
        return;
      this.messages.Clear();
      this.messages = chatMessageList;
      this.messages.Sort((Comparison<ChatMessage>) ((x, y) => x.sendTime.CompareTo(y.sendTime)));
      if (this.onHistoryLoaded != null)
        this.onHistoryLoaded(this.channelId);
      if (this.onNewMessage != null)
        this.onNewMessage(this.GetLastMessage(0));
      if (this.onMessageReceived == null)
        return;
      this.onMessageReceived(this.GetLastMessage(0));
    }

    public void ShowShortInfo()
    {
      for (int index = 0; index < this.messages.Count; ++index)
        this.messages[index].ShowShortInfo();
    }

    public void ShowAllInfo()
    {
      for (int index = 0; index < this.messages.Count; ++index)
        this.messages[index].ShowInfo();
    }

    public ChatMessage GetLastMessage(int count = 0)
    {
      if (this.messages.Count > count)
        return this.messages[this.messages.Count - 1 - count];
      return (ChatMessage) null;
    }

    public void FilterMessage()
    {
      for (int index = this.messages.Count - 1; index >= 0; --index)
      {
        if (PlayerData.inst.userData.InMod(this.messages[index].senderUid) && !this.messages[index].isMod)
          this.messages.RemoveAt(index);
      }
    }
  }
}
