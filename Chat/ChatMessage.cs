﻿// Decompiled with JetBrains decompiler
// Type: Chat.ChatMessage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using ArabicSupport;
using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;

namespace Chat
{
  public class ChatMessage
  {
    public string senderName = string.Empty;
    public string senderCustomIconUrl = string.Empty;
    public string senderAllianceTag = string.Empty;
    public int senderAllianceMemeberLevel = -1;
    public string language = "english";
    public string chatText = string.Empty;
    public string translateText = string.Empty;
    public string translateType = string.Empty;
    private ChatTraceData _traceData = new ChatTraceData();
    public long senderChannelId;
    public long senderUid;
    public int senderIcon;
    public int senderVipLevel;
    public int senderArtifactID;
    public int senderMuteAdmin;
    public int senderLordTitle;
    private string _fullName;
    public long receiveChannelId;
    public int sendTime;
    public bool isMod;
    private bool _showTranslated;
    public System.Action onTranslateEnd;
    private int _unreadKingdomMessage;
    private int _unreadAllianceMessage;
    private int _unreadRoomMessage;
    private int _unreadPrivateMessage;
    private string _localizationKey;
    private Hashtable _localizaionnParam;
    private string _functionParam;

    public string fullName
    {
      get
      {
        return this._fullName;
      }
    }

    public bool showTranslated
    {
      set
      {
        this._showTranslated = value;
        if (this.onTranslateEnd == null)
          return;
        this.onTranslateEnd();
      }
      get
      {
        return this._showTranslated;
      }
    }

    public string text
    {
      get
      {
        string str = this.chatText;
        bool flag = false;
        if (this.localizationKey != null && this.localizationKey != string.Empty)
        {
          str = this.localizationParam != null ? ScriptLocalization.GetWithPara(this.localizationKey, this.localParamDic, true) : Utils.XLAT(this.localizationKey);
          flag = true;
        }
        else if (this.showTranslated)
          str = this.translateText;
        if (str.Length > 0 && LocalizationManager.IsRight2Left && !flag)
          str = ArabicFixer.Fix(str, false, false);
        return str;
      }
    }

    public int unreadKingdomMessage
    {
      get
      {
        return this._unreadKingdomMessage;
      }
      set
      {
        this._unreadKingdomMessage = value;
      }
    }

    public int unreadAllianceMessage
    {
      get
      {
        return this._unreadAllianceMessage;
      }
      set
      {
        this._unreadAllianceMessage = value;
      }
    }

    public int unreadRoomMessage
    {
      get
      {
        return this._unreadRoomMessage;
      }
      set
      {
        this._unreadRoomMessage = value;
      }
    }

    public int unreadPrivateMessage
    {
      get
      {
        return this._unreadPrivateMessage;
      }
      set
      {
        this._unreadPrivateMessage = value;
      }
    }

    public string localizationKey
    {
      get
      {
        return this._localizationKey;
      }
      set
      {
        this._localizationKey = value;
      }
    }

    public Hashtable localizationParam
    {
      get
      {
        return this._localizaionnParam;
      }
      set
      {
        this._localizaionnParam = value;
      }
    }

    public Dictionary<string, string> localParamDic
    {
      get
      {
        if (this._localizaionnParam == null)
          return (Dictionary<string, string>) null;
        Dictionary<string, string> dictionary = new Dictionary<string, string>();
        IEnumerator enumerator = this._localizaionnParam.Keys.GetEnumerator();
        while (enumerator.MoveNext())
          dictionary.Add(enumerator.Current.ToString(), Utils.XLAT(this._localizaionnParam[enumerator.Current].ToString()));
        return dictionary;
      }
    }

    public string functionParam
    {
      get
      {
        return this._functionParam;
      }
      set
      {
        this._functionParam = value;
      }
    }

    public ChatTraceData traceData
    {
      get
      {
        return this._traceData;
      }
      set
      {
        this._traceData = value;
      }
    }

    public void SetDefaultInfo()
    {
      UserData userData = DBManager.inst.DB_User.Get(PlayerData.inst.uid);
      if (userData == null)
        return;
      this.senderChannelId = userData.channelId;
      this.senderUid = userData.uid;
      this.senderName = userData.userName;
      this.senderIcon = userData.portrait;
      this.senderCustomIconUrl = userData.Icon;
      this.senderLordTitle = userData.LordTitle;
      this.senderVipLevel = !PlayerData.inst.hostPlayer.VIPActive ? 0 : PlayerData.inst.hostPlayer.VIPLevel;
      this.senderMuteAdmin = !userData.isMuteAdmin ? 0 : 1;
      this.isMod = PlayerData.inst.moderatorInfo.IsModerator && PlayerData.inst.moderatorInfo.OpenModMode;
      if (this.isMod && this.traceData.type == ChatTraceData.TraceType.Empty)
        this.senderName = PlayerData.inst.moderatorInfo.mod_name;
      this.senderAllianceMemeberLevel = PlayerData.inst.allianceData == null || PlayerData.inst.allianceData.members == null || PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid) == null ? -1 : PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid).title;
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(userData.allianceId);
      if (allianceData != null)
      {
        this.senderAllianceTag = allianceData.allianceAcronym;
        this._fullName = "[" + this.senderAllianceTag + "] " + this.senderName;
      }
      else
      {
        this.senderAllianceTag = string.Empty;
        this._fullName = this.senderName;
      }
      ArtifactData artifactBySlotIndex = DBManager.inst.DB_hero.Get(PlayerData.inst.uid).GetArtifactBySlotIndex(2);
      if (artifactBySlotIndex != null)
        this.senderArtifactID = artifactBySlotIndex.ArtifactId;
      this.sendTime = NetServerTime.inst.ServerTimestamp;
      this.language = LocalizationManager.CurrentLanguageCode;
    }

    public string iconSymbol
    {
      get
      {
        return this.senderIcon.ToString() + this.senderCustomIconUrl + (object) (this.senderUid == PlayerData.inst.uid);
      }
    }

    public long Decode(object orgData)
    {
      Hashtable data1;
      if (BaseData.CheckAndParseOrgData(orgData, out data1))
      {
        Hashtable data2;
        if (BaseData.CheckAndParseOrgData(data1[(object) "info"], out data2))
        {
          DatabaseTools.UpdateData(data2, "r_cid", ref this.receiveChannelId);
          DatabaseTools.UpdateData(data2, "s_cid", ref this.senderChannelId);
          DatabaseTools.UpdateData(data2, "s_u_uid", ref this.senderUid);
          DatabaseTools.UpdateData(data2, "s_u_name", ref this.senderName);
          DatabaseTools.UpdateData(data2, "s_u_icon", ref this.senderIcon);
          DatabaseTools.UpdateData(data2, "s_c_icon", ref this.senderCustomIconUrl);
          DatabaseTools.UpdateData(data2, "s_m_admin", ref this.senderMuteAdmin);
          DatabaseTools.UpdateData(data2, "s_a_tag", ref this.senderAllianceTag);
          DatabaseTools.UpdateData(data2, "s_a_r_level", ref this.senderAllianceMemeberLevel);
          DatabaseTools.UpdateData(data2, "s_v_l", ref this.senderVipLevel);
          DatabaseTools.UpdateData(data2, "s_a_id", ref this.senderArtifactID);
          DatabaseTools.UpdateData(data2, "lord_title", ref this.senderLordTitle);
          DatabaseTools.UpdateData(data2, "ismod", ref this.isMod);
          this._localizationKey = string.Empty;
          DatabaseTools.UpdateData(data2, "loc", ref this._localizationKey);
          this._localizaionnParam = (Hashtable) null;
          if (data2.ContainsKey((object) "loc_params"))
            this._localizaionnParam = data2[(object) "loc_params"] as Hashtable;
          this._functionParam = string.Empty;
          DatabaseTools.UpdateData(data2, "func", ref this._functionParam);
          if (data2.ContainsKey((object) "trace"))
            this._traceData.Decode(data2[(object) "trace"]);
          DatabaseTools.UpdateData(data2, "time", ref this.sendTime);
          DatabaseTools.UpdateData(data2, "language", ref this.language);
          this._fullName = string.IsNullOrEmpty(this.senderAllianceTag) ? this.senderName : "[" + this.senderAllianceTag + "] " + this.senderName;
        }
        if (data1[(object) "text"] != null)
          this.chatText = data1[(object) "text"].ToString();
      }
      GameEngine.Instance.ChatManager.Machine.onMessageReceived(this);
      return this.receiveChannelId;
    }

    public object Encode()
    {
      Hashtable hashtable1 = new Hashtable();
      hashtable1[(object) "r_cid"] = (object) this.receiveChannelId;
      hashtable1[(object) "s_cid"] = (object) this.senderChannelId;
      hashtable1[(object) "s_u_uid"] = (object) this.senderUid;
      hashtable1[(object) "s_u_name"] = (object) this.senderName;
      hashtable1[(object) "s_u_icon"] = (object) this.senderIcon;
      hashtable1[(object) "s_c_icon"] = (object) this.senderCustomIconUrl;
      hashtable1[(object) "s_a_tag"] = (object) this.senderAllianceTag;
      hashtable1[(object) "s_v_l"] = (object) this.senderVipLevel;
      hashtable1[(object) "s_a_r_level"] = (object) this.senderAllianceMemeberLevel;
      hashtable1[(object) "s_a_id"] = (object) this.senderArtifactID;
      hashtable1[(object) "lord_title"] = (object) this.senderLordTitle;
      hashtable1[(object) "s_m_admin"] = (object) this.senderMuteAdmin;
      hashtable1[(object) "time"] = (object) this.sendTime;
      hashtable1[(object) "language"] = (object) this.language;
      hashtable1[(object) "loc"] = (object) this.localizationKey;
      hashtable1[(object) "loc_params"] = (object) this.localizationParam;
      hashtable1[(object) "trace"] = (object) this.traceData.Encode();
      hashtable1[(object) "ismod"] = (object) this.isMod;
      Hashtable hashtable2 = new Hashtable();
      hashtable2[(object) "info"] = (object) hashtable1;
      hashtable2[(object) "text"] = (object) this.chatText;
      return (object) hashtable2;
    }

    public void ShowShortInfo()
    {
      "<color=#ff00ff> Chat MSG </color>\n" + "Text : " + this.chatText + "\n" + "\n" + "Recevie Info : \n" + "\tChannel ID : " + (object) this.receiveChannelId + "\n" + "\n" + "Sender Info : \n" + "\tChannel ID : " + (object) this.senderChannelId + "\n";
    }

    public void ShowInfo()
    {
      string str = "<color=#ff00ff> Chat MSG </color>\n" + "Text : " + this.chatText + "\n" + "\n" + "Recevie Info : \n" + "\tChannel ID : " + (object) this.receiveChannelId + "\n" + "\n" + "Sender Info : \n" + "\tChannel ID : " + (object) this.senderChannelId + "\n" + "\tUID : " + (object) this.senderUid + "\n" + "\tName : " + this.senderName + "\n" + "\tIcon : " + (object) this.senderIcon + "\n" + "\tAllianceTag : " + this.senderAllianceTag + "\n" + "\tVIP : " + (object) this.senderVipLevel + "\n" + "\tLoc Key : " + this.localizationKey + "\n" + "\tFunc : " + this.functionParam + "\n" + "\tTime : " + (object) this.sendTime + "\n" + "\tLanguage :" + this.language + "\n" + "\tTrace : " + this.traceData.ShowInfo() + "\n";
    }

    public static object Encode(long receiveChannelID, string text)
    {
      UserData userData = DBManager.inst.DB_User.Get(PlayerData.inst.uid);
      Hashtable hashtable1 = new Hashtable();
      if (userData != null)
      {
        hashtable1[(object) "r_cid"] = (object) receiveChannelID;
        hashtable1[(object) "s_cid"] = (object) userData.channelId;
        hashtable1[(object) "s_u_uid"] = (object) userData.uid;
        hashtable1[(object) "s_m_admin"] = (object) (!userData.isMuteAdmin ? 0 : 1);
        hashtable1[(object) "s_u_name"] = (object) userData.userName;
        hashtable1[(object) "s_u_icon"] = (object) userData.portrait;
        hashtable1[(object) "s_c_icon"] = (object) userData.Icon;
        hashtable1[(object) "lord_title"] = (object) userData.LordTitle;
        AllianceData allianceData = DBManager.inst.DB_Alliance.Get(userData.allianceId);
        if (allianceData != null)
          hashtable1[(object) "s_a_tag"] = (object) allianceData.allianceAcronym;
        else
          hashtable1[(object) "s_a_tag"] = (object) string.Empty;
        hashtable1[(object) "time"] = (object) NetServerTime.inst.ServerTimestamp;
        hashtable1[(object) "language"] = (object) "english";
      }
      Hashtable hashtable2 = new Hashtable();
      hashtable2[(object) "info"] = (object) hashtable1;
      hashtable2[(object) nameof (text)] = (object) text;
      return (object) hashtable2;
    }

    public struct Params
    {
      public const string CHAT_MSG_INFO = "info";
      public const string CHAT_MSG_INFO_RECEIVE_CHANNEL_ID = "r_cid";
      public const string CHAT_MSG_INFO_SENDER_CHANNEL_ID = "s_cid";
      public const string CHAT_MSG_INFO_SENDER_UID = "s_u_uid";
      public const string CHAT_MSG_INFO_SENDER_NAME = "s_u_name";
      public const string CHAT_MSG_INFO_SENDER_ICON = "s_u_icon";
      public const string CHAT_MSG_INFO_CUSTOM_ICON = "s_c_icon";
      public const string CHAT_MSG_INFO_MUTE_ADMIN = "s_m_admin";
      public const string CHAT_MSG_INFO_SENDER_ALLIANCE_TAG = "s_a_tag";
      public const string CHAT_MSG_INFO_SENDER_ALLIANCE_R_LEVEL = "s_a_r_level";
      public const string CHAT_MSG_INFO_SENDER_VIP_LEVEL = "s_v_l";
      public const string CHAT_MSG_INFO_SENDER_ARTIFACT_ID = "s_a_id";
      public const string CHAT_MSG_INFO_SENDER_LORD_TITLE = "lord_title";
      public const string CHAT_MSG_INFO_TIME = "time";
      public const string CHAT_MSG_INFO_LANGUAGE = "language";
      public const string CHAT_MSG_INFO_LOCAL = "loc";
      public const string CHAT_MSG_INFO_LOCAL_PARAM = "loc_params";
      public const string CHAT_MSG_FUNCTION = "func";
      public const string CHAT_MSG_FUNCTION_TRACING = "trace";
      public const string CHAT_MSG_FUNCTION_IS_MOD = "ismod";
      public const string CHAT_MSG_TEXT = "text";
    }
  }
}
