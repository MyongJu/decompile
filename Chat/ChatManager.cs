﻿// Decompiled with JetBrains decompiler
// Type: Chat.ChatManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using Rtm.Connection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

namespace Chat
{
  public class ChatManager
  {
    private int _taskId = -1;
    private Stopwatch watch = new Stopwatch();
    private const float FIXED_CHAT_CD_TIME = 3f;
    private Dictionary<long, ChatChannel> _chatChannels;
    private Dictionary<long, ChatChannel> _privateChannels;
    private List<long> _chatChannelList;
    private List<long> _pushChannelList;
    private ChatChannel _personChannel;
    private ChatChannel _kingdomChannel;
    private ChatChannel _allianceChannel;
    private List<long> _roomChannels;
    public System.Action onRoomChanged;
    public System.Action<ChatMessage> onMessageReceived;
    public System.Action<int> onPrivateUnreadChanged;
    private bool _isInited;
    private PushManager _pushManager;
    private bool isStart;
    private HubPort messagePort;
    private NLPMachine machine;
    private long _kingdomPushChannelId;
    private long _alliancePushChannelId;
    private float chatCDTime;
    private int configedCDTime;
    private Coroutine chatCoroutine;

    public List<long> roomChannels
    {
      get
      {
        return this._roomChannels;
      }
    }

    public NLPMachine Machine
    {
      get
      {
        return this.machine;
      }
    }

    public long userChannelId
    {
      get
      {
        if (this._personChannel != null)
          return this._personChannel.channelId;
        return 0;
      }
    }

    public ChatChannel userChannel
    {
      get
      {
        return this.GetChatChannel(this.userChannelId, 0L);
      }
    }

    public long kingdomChannelId
    {
      get
      {
        if (this._kingdomChannel != null)
          return this._kingdomChannel.channelId;
        return 0;
      }
    }

    public ChatChannel kingdomChannel
    {
      get
      {
        return this.GetChatChannel(this.kingdomChannelId, 0L);
      }
    }

    public long allianceChannelId
    {
      get
      {
        if (this._allianceChannel != null)
          return this._allianceChannel.channelId;
        return 0;
      }
    }

    public ChatChannel allianceChannel
    {
      get
      {
        return this.GetChatChannel(this.allianceChannelId, 0L);
      }
    }

    public void Init()
    {
      if (this._isInited)
        return;
      this._chatChannels = new Dictionary<long, ChatChannel>();
      this._privateChannels = new Dictionary<long, ChatChannel>();
      this._chatChannelList = new List<long>();
      this._pushChannelList = new List<long>();
      UserData userData = DBManager.inst.DB_User.Get(PlayerData.inst.uid);
      CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
      AllianceData allianceData = (AllianceData) null;
      DB.WorldMapData worldMapData = (DB.WorldMapData) null;
      if (userData != null)
        allianceData = DBManager.inst.DB_Alliance.Get(userData.allianceId);
      if (cityData != null)
        worldMapData = DBManager.inst.DB_WorldMap.Get(userData.world_id);
      if (userData != null && userData.channelId != 0L)
      {
        this._personChannel = new ChatChannel(userData.channelId);
        this._AddChatChannel(userData.channelId, this._personChannel);
      }
      if (worldMapData != null && worldMapData.chatChannelId != 0L)
      {
        this._kingdomChannel = new ChatChannel(worldMapData.chatChannelId);
        this._kingdomPushChannelId = worldMapData.pushChannelId;
        this._AddChatChannel(worldMapData.chatChannelId, this._kingdomChannel);
        this._AddPushChannel(this._kingdomPushChannelId, (ChatChannel) null);
      }
      if (allianceData != null && allianceData.chatChannelId != 0L)
      {
        this._allianceChannel = new ChatChannel(allianceData.chatChannelId);
        this._alliancePushChannelId = allianceData.pushChannelId;
        this._AddChatChannel(allianceData.chatChannelId, this._allianceChannel);
        this._AddPushChannel(allianceData.pushChannelId, (ChatChannel) null);
      }
      this._roomChannels = new List<long>();
      Dictionary<long, ChatRoomData>.ValueCollection.Enumerator valueEnumerator = DBManager.inst.DB_room.valueEnumerator;
      while (valueEnumerator.MoveNext())
      {
        ChatChannel channel = new ChatChannel(valueEnumerator.Current.channelId);
        channel.channelName = valueEnumerator.Current.roomName;
        channel.channelWelcomeMsg = valueEnumerator.Current.roomTitle;
        this._AddChatChannel(channel.channelId, channel);
        this._roomChannels.Add(channel.channelId);
      }
      this._pushManager = PushManager.inst;
      this._pushManager.Init(this.userChannelId);
      this.messagePort = MessageHub.inst.GetPortByAction("chat_message");
      this.messagePort.AddEvent(new System.Action<object>(this.OnNewMessageReceive));
      AllianceManager.Instance.onJoin += new System.Action(this.OnAllianceJoined);
      AllianceManager.Instance.onLeave += new System.Action(this.OnAllianceQuit);
      DBManager.inst.DB_room.onDataCreated += new System.Action<long>(this.RoomJoined);
      DBManager.inst.DB_room.onDataUpdated += new System.Action<long>(this.RoomJoined);
      DBManager.inst.DB_room.onDataRemove += new System.Action<long>(this.RoomLeave);
      DBManager.inst.DB_Contacts.onDataCreated += new System.Action<long>(this.OnContactCreated);
      DBManager.inst.DB_Contacts.onDataChanged += new System.Action<long>(this.OnContactChanged);
      DBManager.inst.DB_Contacts.onDataRemove += new System.Action<long>(this.OnContactRemove);
      this.machine = new NLPMachine();
      this.CanChat = true;
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("all_channel_chat_cd");
      if (data != null)
        this.configedCDTime = data.ValueInt;
      this._isInited = true;
    }

    private void RoomJoined(long chatRoomId)
    {
      ChatRoomData chatRoomData = DBManager.inst.DB_room.Get(chatRoomId);
      if (chatRoomData == null || this._roomChannels.Contains(chatRoomData.channelId))
        return;
      ChatChannel channel = new ChatChannel(chatRoomData.channelId);
      channel.channelName = chatRoomData.roomName;
      channel.channelWelcomeMsg = chatRoomData.roomTitle;
      this._AddChatChannel(channel.channelId, channel);
      this._roomChannels.Add(channel.channelId);
      this._pushManager.GroupJoin(chatRoomId, (System.Action<bool>) null);
      this.Publish_RoomChangedMessage();
    }

    private void RoomLeave(long chatRoomId)
    {
      ChatRoomData chatRoomData = DBManager.inst.DB_room.Get(chatRoomId);
      if (chatRoomData == null || !this._roomChannels.Contains(chatRoomData.channelId))
        return;
      this._roomChannels.Remove(chatRoomData.channelId);
      this._pushManager.GroupLeave(chatRoomData.channelId, (System.Action<bool>) null);
      this.Publish_RoomChangedMessage();
    }

    private void Publish_RoomChangedMessage()
    {
      if (this.onRoomChanged == null)
        return;
      this.onRoomChanged();
    }

    public ChatChannel GetChatChannel(long channelId, long targetChannelId = 0)
    {
      if (channelId != this._personChannel.channelId)
      {
        if (this._chatChannels.ContainsKey(channelId))
          return this._chatChannels[channelId];
        return (ChatChannel) null;
      }
      if (targetChannelId == 0L)
        return (ChatChannel) null;
      if (this._privateChannels.ContainsKey(targetChannelId))
        return this._privateChannels[targetChannelId];
      ChatChannel chatChannel = new ChatChannel(targetChannelId);
      this._privateChannels.Add(targetChannelId, chatChannel);
      this._privateChannels[targetChannelId].onUnreadCountChanged = new System.Action<long>(this.OnPrivateChannelUnreadChanged);
      return chatChannel;
    }

    public bool SendPersonalMessage(long targetChannelId, ChatMessage message, bool self = true, bool broadcast = true)
    {
      message.receiveChannelId = targetChannelId;
      if (self)
        this.GetChatChannel(this.userChannelId, targetChannelId).MessageSend(message);
      if (broadcast)
        this._pushManager.Send("chat_message", targetChannelId, message.Encode(), (System.Action<bool>) null);
      return true;
    }

    public bool SendPersonalMessage(long targetChannelId, string msg, ChatTraceData traceData = null, bool self = true, bool broascast = true)
    {
      ChatMessage message = new ChatMessage();
      if (traceData == null)
        message.traceData.type = ChatTraceData.TraceType.Empty;
      else
        message.traceData = traceData;
      message.SetDefaultInfo();
      message.receiveChannelId = targetChannelId;
      message.chatText = msg;
      return this.SendPersonalMessage(targetChannelId, message, self, broascast);
    }

    public bool PushGroupMessage(long channelId, ChatMessage message)
    {
      message.receiveChannelId = channelId;
      this._pushManager.PushGroup("chat_message", channelId, (object) message.text, (System.Action<bool>) null);
      return true;
    }

    public ChatMessage ConstructMessage(long channelId, string msg, ChatTraceData traceData = null)
    {
      ChatMessage chatMessage = new ChatMessage();
      if (traceData == null)
        chatMessage.traceData.type = ChatTraceData.TraceType.Empty;
      else
        chatMessage.traceData = traceData;
      chatMessage.SetDefaultInfo();
      chatMessage.chatText = msg;
      chatMessage.receiveChannelId = channelId;
      return chatMessage;
    }

    public bool PushGroupMessage(long channelId, string msg, ChatTraceData traceData = null)
    {
      ChatMessage message = new ChatMessage();
      if (traceData == null)
        message.traceData.type = ChatTraceData.TraceType.Empty;
      else
        message.traceData = traceData;
      message.SetDefaultInfo();
      message.chatText = msg;
      return this.PushGroupMessage(channelId, message);
    }

    public void SendSelfMessage(long channelId, ChatMessage message)
    {
      message.receiveChannelId = channelId;
      this._chatChannels[channelId].MessageSend(message);
    }

    public bool SendGroupMessage(long channelId, ChatMessage message, bool self = true, bool brodcast = true)
    {
      message.receiveChannelId = channelId;
      if (self)
        this._chatChannels[channelId].MessageSend(message);
      if (brodcast)
        this._pushManager.SendGroup("chat_message", channelId, message.Encode(), (System.Action<bool>) null);
      return true;
    }

    public bool SendGroupMessage(long channelId, string msg, ChatTraceData traceData = null, bool self = true, bool brodcast = true)
    {
      ChatMessage message = new ChatMessage();
      if (traceData == null)
        message.traceData.type = ChatTraceData.TraceType.Empty;
      else
        message.traceData = traceData;
      message.SetDefaultInfo();
      message.chatText = msg;
      return this.SendGroupMessage(channelId, message, self, brodcast);
    }

    public void OnNewMessageReceive(object orgData, bool isHistory)
    {
      ChatChannel chatChannel = (ChatChannel) null;
      ChatMessage message = new ChatMessage();
      long channelId = message.Decode(orgData);
      if (message != null)
      {
        if (ChatUtils.CheckMessageShouldHide(message))
          return;
        chatChannel = message.receiveChannelId != this.userChannelId ? (message.senderChannelId != this.userChannelId ? this.GetChatChannel(channelId, 0L) : this.GetChatChannel(this.userChannelId, message.receiveChannelId)) : this.GetChatChannel(this.userChannelId, message.senderChannelId);
        if (chatChannel == null || chatChannel.messages == null || chatChannel.messages.Contains(message))
          return;
      }
      chatChannel.MessageReceived(message);
      if (this.onMessageReceived == null)
        return;
      this.onMessageReceived(message);
    }

    public void OnNewMessageReceive(object orgData)
    {
      ChatMessage message = new ChatMessage();
      long key = message.Decode(orgData);
      if (key == 0L || !this._chatChannels.ContainsKey(key) || (message.senderUid == PlayerData.inst.uid || ChatUtils.CheckMessageShouldHide(message)))
        return;
      ChatChannel chatChannel = this.GetChatChannel(message.receiveChannelId, message.senderChannelId);
      if (chatChannel == null)
        return;
      chatChannel.MessageReceived(message);
      if (this.onMessageReceived == null)
        return;
      this.onMessageReceived(message);
    }

    public void OnContactCreated(long uid)
    {
      UserData userData = DBManager.inst.DB_User.Get(uid);
      if (userData == null)
        return;
      ChatChannel chatChannel = this.GetChatChannel(this.userChannelId, userData.channelId);
      if (chatChannel == null)
        return;
      chatChannel.LoadPersonalHistory();
    }

    public void OnContactChanged(long uid)
    {
      UserData userData = DBManager.inst.DB_User.Get(uid);
      if (userData == null)
        return;
      if (DBManager.inst.DB_Contacts.friends.Contains(uid) && !this._privateChannels.ContainsKey(userData.channelId))
      {
        this.OnContactCreated(uid);
      }
      else
      {
        if (DBManager.inst.DB_Contacts.friends.Contains(uid) || !this._privateChannels.ContainsKey(userData.channelId))
          return;
        this.OnContactRemove(uid);
      }
    }

    public void OnContactRemove(long uid)
    {
      UserData userData = DBManager.inst.DB_User.Get(uid);
      if (userData == null || !this._privateChannels.ContainsKey(userData.channelId))
        return;
      this._privateChannels.Remove(userData.channelId);
      if (this.onPrivateUnreadChanged == null)
        return;
      this.onPrivateUnreadChanged(this.GetPrivateChannelUnreadCount());
    }

    public void OnPrivateChannelUnreadChanged(long channelId)
    {
      if (this.onPrivateUnreadChanged == null)
        return;
      this.onPrivateUnreadChanged(this.GetPrivateChannelUnreadCount());
    }

    public int GetPrivateChannelUnreadCount()
    {
      int num = 0;
      Dictionary<long, ChatChannel>.ValueCollection.Enumerator enumerator = this._privateChannels.Values.GetEnumerator();
      while (enumerator.MoveNext())
        num += enumerator.Current.unreadCount;
      return num;
    }

    public int GetUserUnreadCount(long uid)
    {
      UserData userData = DBManager.inst.DB_User.Get(uid);
      if (userData != null && this._privateChannels.ContainsKey(userData.channelId))
      {
        ChatChannel privateChannel = this._privateChannels[userData.channelId];
        if (privateChannel != null)
          return privateChannel.unreadCount;
      }
      return 0;
    }

    public int GetChannelUnreadCount(long channelId)
    {
      if (this._privateChannels.ContainsKey(channelId))
      {
        ChatChannel privateChannel = this._privateChannels[channelId];
        if (privateChannel != null)
          return privateChannel.unreadCount;
      }
      return 0;
    }

    public void Connect()
    {
      this.watch.Reset();
      this.watch.Start();
      this.isStart = true;
      NetApi.inst.ResetRTM();
      NetApi.inst.RTM_Status = NetApi.RTMSTATE.InConnection;
      ArrayList pushSystemNodes = NetApi.inst.PushSystemNodes;
      if (pushSystemNodes != null && pushSystemNodes.Count > 0)
      {
        this.ConnectRTM();
      }
      else
      {
        RTMDispatchTask rtmDispatchTask = new RTMDispatchTask();
        ++NetWorkDetector.Instance.RTM_DISPATCH_COUNT;
        Oscillator.Instance.updateEvent += new System.Action<double>(this.Process);
        this._taskId = TaskManager.Instance.Execute((TaskManager.ITask) rtmDispatchTask);
      }
    }

    private void Process(double time)
    {
      if (this._taskId <= 0)
      {
        if (!Oscillator.IsAvailable)
          return;
        Oscillator.Instance.updateEvent -= new System.Action<double>(this.Process);
      }
      else
      {
        if (!TaskManager.Instance.IsFinish(this._taskId))
          return;
        if (Oscillator.IsAvailable)
          Oscillator.Instance.updateEvent -= new System.Action<double>(this.Process);
        this.ConnectRTM();
      }
    }

    public void Dispose()
    {
      this._taskId = -1;
      if (!Oscillator.IsAvailable)
        return;
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.Process);
    }

    private void ConnectRTM()
    {
      if (NetApi.inst.RTM_Status == NetApi.RTMSTATE.DispatchError)
      {
        this.ConnectCallback(false);
        this._pushManager.ConnectFail();
      }
      else
      {
        ++NetWorkDetector.Instance.RTM_CONNECT_COUNT;
        List<long> ids = new List<long>((IEnumerable<long>) this._chatChannelList);
        ids.AddRange((IEnumerable<long>) this._pushChannelList);
        this._pushManager.DoConnect(ids, new System.Action<bool>(this.ConnectCallback));
      }
    }

    public void ReConnect()
    {
      this.isStart = true;
      NetApi.inst.ResetRTM();
      this.watch.Reset();
      this.watch.Start();
      this._pushManager.ReConnect();
    }

    private void SendData2RUM(bool result)
    {
      if (!this.isStart)
        return;
      this.isStart = false;
      int num = (int) this.watch.ElapsedMilliseconds;
      this.watch.Stop();
      if (!result)
        num = -num;
      NetWorkDetector.Instance.RTM_TIME = num;
    }

    public void ConnectCallback(bool result)
    {
      this.SendData2RUM(result);
      if (result)
        ;
    }

    private void _AddChatChannel(long channelId, ChatChannel channel = null)
    {
      if (this._chatChannels.ContainsKey(channelId))
        return;
      this._chatChannels.Add(channelId, channel ?? new ChatChannel(channelId));
      this._chatChannelList.Add(channelId);
    }

    private void _AddPushChannel(long channelId, ChatChannel channel = null)
    {
      if (this._pushChannelList.Contains(channelId))
        return;
      this._pushChannelList.Add(channelId);
    }

    private void _RemoveChatChannel(long channelId)
    {
      if (!this._chatChannels.ContainsKey(channelId))
        return;
      this._chatChannels.Remove(channelId);
      this._chatChannelList.Remove(channelId);
    }

    private void _RemovePushChannel(long channelId)
    {
      if (!this._pushChannelList.Contains(channelId))
        return;
      this._pushChannelList.Remove(channelId);
    }

    private void OnAllianceJoined()
    {
      long allianceId = PlayerData.inst.allianceId;
      if (this._allianceChannel != null)
      {
        this._RemoveChatChannel(this._allianceChannel.channelId);
        this._RemovePushChannel(this._alliancePushChannelId);
        this._pushManager.GroupLeave(this._allianceChannel.channelId, (System.Action<bool>) null);
        this._pushManager.GroupLeave(this._alliancePushChannelId, (System.Action<bool>) null);
        this._allianceChannel = (ChatChannel) null;
      }
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(allianceId);
      if (allianceData == null)
        return;
      this._allianceChannel = new ChatChannel(allianceData.chatChannelId);
      this._alliancePushChannelId = allianceData.pushChannelId;
      this._AddChatChannel(allianceData.chatChannelId, this._allianceChannel);
      this._AddPushChannel(this._alliancePushChannelId, (ChatChannel) null);
      this._pushManager.GroupJoin(allianceData.chatChannelId, (System.Action<bool>) null);
      this._pushManager.GroupJoin(this._alliancePushChannelId, (System.Action<bool>) null);
    }

    private void OnAllianceQuit()
    {
      if (this._allianceChannel == null)
        return;
      this._RemoveChatChannel(this._allianceChannel.channelId);
      this._RemovePushChannel(this._alliancePushChannelId);
      this._pushManager.GroupLeave(this._allianceChannel.channelId, (System.Action<bool>) null);
      this._pushManager.GroupLeave(this._alliancePushChannelId, (System.Action<bool>) null);
      this._allianceChannel = (ChatChannel) null;
    }

    private void LoadGroupHistory(long id, int num = 100, long offset = -1)
    {
      this._pushManager.GroupHistoryMessage(id, num, offset, new System.Action<MsgResult>(this.LoadHistoryCallBack), (byte) 100);
    }

    private void LoadHistoryCallBack(MsgResult result)
    {
      for (int index = result.Msgs.Count - 1; index >= 0; --index)
      {
        try
        {
          Hashtable hashtable = Utils.Json2Object(result.Msgs[index].Body, true) as Hashtable;
          if (hashtable != null)
          {
            if (hashtable.Contains((object) "cmd"))
            {
              if (hashtable[(object) "cmd"].ToString().CompareTo("chat_message") == 0)
                this.OnNewMessageReceive(hashtable[(object) "payload"], true);
            }
          }
        }
        catch (Exception ex)
        {
          D.error((object) ("[History Message]Error:" + ex.Message));
        }
      }
    }

    public bool CanChat { set; get; }

    public void ShowChatCDToast()
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      int num = Mathf.CeilToInt(this.chatCDTime);
      if (num == 0)
        return;
      para.Add("0", num.ToString());
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_event_37wan_speech_ban_cd", para, true), (System.Action) null, 1f, true);
    }

    private float GetChatCDTime()
    {
      if (this.configedCDTime != 0)
        return (float) this.configedCDTime;
      return 3f;
    }

    public void ResetChatConstraint()
    {
      if (this.chatCoroutine != null)
      {
        Utils.StopCoroutine(this.chatCoroutine);
        this.chatCoroutine = (Coroutine) null;
      }
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.OnTimeUpdate);
      this.CanChat = true;
      this.chatCDTime = this.GetChatCDTime();
    }

    public void BeginChatConstraint()
    {
      this.CanChat = false;
      Oscillator.Instance.updateEvent += new System.Action<double>(this.OnTimeUpdate);
      this.chatCoroutine = Utils.ExecuteInSecs(this.GetChatCDTime(), new System.Action(this.OnCDTimeFinished));
    }

    private void OnTimeUpdate(double data)
    {
      this.chatCDTime -= Time.deltaTime;
    }

    private void OnCDTimeFinished()
    {
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.OnTimeUpdate);
      this.CanChat = true;
      this.chatCDTime = this.GetChatCDTime();
    }
  }
}
