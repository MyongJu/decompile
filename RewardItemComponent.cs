﻿// Decompiled with JetBrains decompiler
// Type: RewardItemComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class RewardItemComponent : ComponentRenderBase
{
  public IconListComponent icons;
  public UISprite bg;

  public override void Init()
  {
    base.Init();
    this.icons.FeedData((IComponentData) (this.data as IconListComponentData));
    this.bg.height = (int) NGUIMath.CalculateRelativeWidgetBounds(this.icons.transform).size.y + 50;
  }
}
