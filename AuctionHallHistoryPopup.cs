﻿// Decompiled with JetBrains decompiler
// Type: AuctionHallHistoryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AuctionHallHistoryPopup : Popup
{
  private Dictionary<int, AuctionHallHistoryItemRenderer> _itemDict = new Dictionary<int, AuctionHallHistoryItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public UILabel panelTitle;
  public UILabel panelTip;
  public UILabel noAuctionHistoryTip;
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  private AuctionHelper.AuctionType _auctionType;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    AuctionHallHistoryPopup.Parameter parameter = orgParam as AuctionHallHistoryPopup.Parameter;
    if (parameter != null)
      this._auctionType = parameter.auctionType;
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.ShowAuctionHistoryContent();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void ShowAuctionHistoryContent()
  {
    this.panelTitle.text = Utils.XLAT("exchange_uppercase_bidding_history");
    if ((UnityEngine.Object) this.panelTip != (UnityEngine.Object) null)
      NGUITools.SetActive(this.panelTip.gameObject, false);
    NGUITools.SetActive(this.noAuctionHistoryTip.gameObject, false);
    this.ClearData();
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    postData.Add((object) "type", (object) this._auctionType);
    List<AuctionHistoryItemInfo> auctionHistoryList = new List<AuctionHistoryItemInfo>();
    MessageHub.inst.GetPortByAction("auction:getHistory").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      ArrayList arrayList = data as ArrayList;
      if (arrayList != null)
      {
        for (int index1 = 0; index1 < arrayList.Count; ++index1)
        {
          Hashtable inData = arrayList[index1] as Hashtable;
          if (inData != null)
          {
            int outData1 = -1;
            int outData2 = 0;
            ArrayList data1 = new ArrayList();
            Dictionary<string, string> historyParams = new Dictionary<string, string>();
            DatabaseTools.UpdateData(inData, "type", ref outData1);
            DatabaseTools.UpdateData(inData, "time", ref outData2);
            DatabaseTools.CheckAndParseOrgData(inData[(object) "params"], out data1);
            historyParams.Add("0", this.GetHistoryFormatTime(NetServerTime.inst.ServerTimestamp - outData2));
            if (data1 != null)
            {
              for (int index2 = 0; index2 < data1.Count; ++index2)
              {
                string source = data1[index2].ToString();
                historyParams.Add((index2 + 1).ToString(), Utils.XLAT(source));
              }
            }
            auctionHistoryList.Add(new AuctionHistoryItemInfo((AuctionHelper.AuctionHistoryType) outData1, outData2, historyParams));
          }
        }
      }
      auctionHistoryList.Sort((Comparison<AuctionHistoryItemInfo>) ((a, b) => b.HistoryTime.CompareTo(a.HistoryTime)));
      if (auctionHistoryList.Count > 0)
      {
        for (int key = 0; key < auctionHistoryList.Count; ++key)
        {
          AuctionHallHistoryItemRenderer itemRenderer = this.CreateItemRenderer(auctionHistoryList[key], true);
          this._itemDict.Add(key, itemRenderer);
        }
        Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.Reposition()));
        if (!((UnityEngine.Object) this.panelTip != (UnityEngine.Object) null))
          return;
        NGUITools.SetActive(this.panelTip.gameObject, this.scrollView.shouldMoveVertically);
      }
      else
        NGUITools.SetActive(this.noAuctionHistoryTip.gameObject, true);
    }), true);
  }

  private string GetHistoryFormatTime(int historyTime)
  {
    int num1 = historyTime / 3600 / 24;
    int num2 = historyTime / 3600 % 24;
    int num3 = historyTime % 3600 / 60;
    return ScriptLocalization.GetWithPara("chat_time_display", new Dictionary<string, string>()
    {
      {
        "d",
        num1 <= 0 ? string.Empty : num1.ToString() + Utils.XLAT("chat_time_day")
      },
      {
        "h",
        num2 <= 0 ? string.Empty : num2.ToString() + Utils.XLAT("chat_time_hour")
      },
      {
        "m",
        num3 < 0 ? string.Empty : num3.ToString() + Utils.XLAT("chat_time_min")
      }
    }, true);
  }

  private AuctionHallHistoryItemRenderer CreateItemRenderer(AuctionHistoryItemInfo itemInfo, bool showDividingLine = true)
  {
    GameObject gameObject = this._itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    AuctionHallHistoryItemRenderer component = gameObject.GetComponent<AuctionHallHistoryItemRenderer>();
    component.SetData(itemInfo, showDividingLine);
    return component;
  }

  private void ClearData()
  {
    using (Dictionary<int, AuctionHallHistoryItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  public class Parameter : Popup.PopupParameter
  {
    public AuctionHelper.AuctionType auctionType;
  }
}
