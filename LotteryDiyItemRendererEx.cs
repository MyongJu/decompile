﻿// Decompiled with JetBrains decompiler
// Type: LotteryDiyItemRendererEx
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class LotteryDiyItemRendererEx : LotteryDiyItemRenderer
{
  private bool _specialShowFinished = true;
  public UISprite itemFrame;
  public UITexture itemPropsTexture;
  public UILabel itemNeedPropsText;
  public GameObject collectedNode;
  public UILabel stepText;
  public UnityEngine.Animation lotteryRotationAnim;
  public GameObject lotteryHitEffect;
  public System.Action onLotteryCardTouched;
  public System.Action<int, int, int> onLotteryCardPicked;
  private LotteryDiyBaseData.RewardData _rewardData;
  private bool _isHistoryMode;
  private int _index;

  public bool SpecialShowFinished
  {
    get
    {
      return this._specialShowFinished;
    }
    set
    {
      this._specialShowFinished = value;
    }
  }

  public override void SetData(LotteryDiyBaseData.RewardData rewardData, LotteryDiyPayload.LotteryArea lotteryArea, int index = -1, bool isHistoryMode = false)
  {
    base.SetData(rewardData, lotteryArea, -1, false);
    this._rewardData = rewardData;
    this._index = index;
    this._isHistoryMode = isHistoryMode;
    this.UpdateUI();
  }

  public void OnItemFrameClicked()
  {
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    postData.Add((object) "index", (object) this._index);
    if (this.onLotteryCardTouched != null)
      this.onLotteryCardTouched();
    if (!this.CheckSpecialProps() || !this.SpecialShowFinished)
      return;
    RequestManager.inst.SendRequest("casino:open", postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable hashtable = data as Hashtable;
      LotteryDiyPayload.Instance.LotteryData.Decode(hashtable);
      if (hashtable == null)
        return;
      int outData1 = 0;
      int outData2 = 0;
      int outData3 = 0;
      DatabaseTools.UpdateData(hashtable, "reward_item_id", ref outData1);
      DatabaseTools.UpdateData(hashtable, "reward_number", ref outData2);
      DatabaseTools.UpdateData(hashtable, "spend_item_number", ref outData3);
      this.RefreshUI();
      if (this.onLotteryCardPicked == null)
        return;
      this.onLotteryCardPicked(outData1, outData2, outData3);
    }), true);
  }

  public void UpdatePropsTips(int propsCount = -1)
  {
    if (propsCount < 0)
      propsCount = LotteryDiyPayload.Instance.NeedPropsCount2Play;
    this.itemNeedPropsText.text = propsCount > 0 ? propsCount.ToString() : Utils.XLAT("id_uppercase_free");
    if (propsCount > LotteryDiyPayload.Instance.LotteryData.SpendItemCount)
      this.itemNeedPropsText.color = Color.red;
    else
      this.itemNeedPropsText.color = Color.white;
  }

  private bool CheckSpecialProps()
  {
    if (LotteryDiyPayload.Instance.NeedPropsCount2Play <= LotteryDiyPayload.Instance.LotteryData.SpendItemCount)
      return true;
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("0", LotteryDiyPayload.Instance.LotteryData.SpendItemName);
    string withPara1 = ScriptLocalization.GetWithPara("event_lucky_draw_items_not_enough_title", para, true);
    para.Add("1", LotteryDiyPayload.Instance.LotteryData.GetItemGold.ToString());
    para.Add("2", LotteryDiyPayload.Instance.LotteryData.GetItemGoldNumber.ToString());
    string withPara2 = ScriptLocalization.GetWithPara("event_lucky_draw_items_not_enough_description", para, true);
    string left = ScriptLocalization.Get("event_lucky_draw_get_more_button", true);
    UIManager.inst.ShowConfirmationBox(withPara1, withPara2, left, (string) null, ChooseConfirmationBox.ButtonState.OK_CENTER, (System.Action) (() => Utils.ShowIAPStore((UI.Dialog.DialogParameter) null)), (System.Action) null, (System.Action) null);
    return false;
  }

  private void RefreshUI()
  {
    Dictionary<int, LotteryDiyBaseData.RewardData> lotteryPrevItemDict = LotteryDiyPayload.Instance.LotteryData.LotteryPrevItemDict;
    if (lotteryPrevItemDict != null && lotteryPrevItemDict.ContainsKey(this._index))
    {
      this._rewardData = lotteryPrevItemDict[this._index];
      this.SetData(this._rewardData, this.CurrentLotteryArea, this._index, this._isHistoryMode);
    }
    if ((UnityEngine.Object) this.lotteryRotationAnim != (UnityEngine.Object) null)
      this.lotteryRotationAnim.Play();
    if (!((UnityEngine.Object) this.lotteryHitEffect != (UnityEngine.Object) null))
      return;
    CasinoFunHelper.Instance.PlayParticles(this.lotteryHitEffect);
  }

  private void UpdateUI()
  {
    if (this._rewardData == null)
      return;
    if (this._isHistoryMode)
    {
      this.stepText.text = ScriptLocalization.GetWithPara("tavern_rewards_step_num", new Dictionary<string, string>()
      {
        {
          "0",
          this._rewardData.step.ToString()
        }
      }, true);
      NGUITools.SetActive(this.stepText.transform.parent.gameObject, this._rewardData.isOpen);
      NGUITools.SetActive(this.collectedNode, this._rewardData.isOpen);
    }
    else
    {
      this.UpdatePropsTips(-1);
      this.UpdatePropsUI();
      NGUITools.SetActive(this.itemPropsTexture.gameObject, !this._rewardData.isOpen);
      NGUITools.SetActive(this.itemFrame.gameObject, !this._rewardData.isOpen);
      NGUITools.SetActive(this.itemNeedPropsText.gameObject, !this._rewardData.isOpen);
    }
    NGUITools.SetActive(this.itemCount.gameObject, this._rewardData.isOpen || this._isHistoryMode);
  }

  private void UpdatePropsUI()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(LotteryDiyPayload.Instance.LotteryData.SpendItem);
    if (itemStaticInfo == null || !((UnityEngine.Object) this.itemPropsTexture != (UnityEngine.Object) null))
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.itemPropsTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
  }
}
