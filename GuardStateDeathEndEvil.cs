﻿// Decompiled with JetBrains decompiler
// Type: GuardStateDeathEndEvil
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class GuardStateDeathEndEvil : AbstractGuardState
{
  public GuardStateDeathEndEvil(RoundPlayer player)
    : base(player)
  {
  }

  public override string Key
  {
    get
    {
      return "guard_death_end";
    }
  }

  public override Hashtable Data { get; set; }

  public override void OnEnter()
  {
    BattleManager.Instance.SendPacket((DragonKnightPacket) new DragonKnightPacketFinish(this.Player.PlayerId));
  }

  public override void OnProcess()
  {
  }

  public override void OnExit()
  {
  }

  public override void Dispose()
  {
  }
}
