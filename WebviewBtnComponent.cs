﻿// Decompiled with JetBrains decompiler
// Type: WebviewBtnComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class WebviewBtnComponent : MonoBehaviour
{
  public string urlChinese = string.Empty;
  public string urlOther = string.Empty;

  public void OnClick()
  {
    string str = AccountManager.Instance.FunplusID;
    if (string.IsNullOrEmpty(str))
      str = AccountManager.Instance.AccountId;
    if (LocalizationManager.CurrentLanguageCode == "zh-TW" || LocalizationManager.CurrentLanguageCode == "zh-CN")
    {
      this.urlChinese = string.Format(this.urlChinese, (object) str);
      WebviewSystem.Instance.Start(this.urlChinese);
    }
    else
    {
      this.urlOther = string.Format(this.urlOther, (object) str);
      WebviewSystem.Instance.Start(this.urlOther);
    }
  }
}
