﻿// Decompiled with JetBrains decompiler
// Type: SpriteIndicator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public abstract class SpriteIndicator : MonoBehaviour
{
  private List<SortingOrderModifier> m_SortingOrderModifiers = new List<SortingOrderModifier>();
  [SerializeField]
  protected UISpriteMesh m_Crystal;
  private ISortingLayerCalculator m_SortingLayerCalculator;
  private bool m_SortingLayerChanged;

  private void Start()
  {
    this.GetComponentsInChildren<SortingOrderModifier>(true, (List<M0>) this.m_SortingOrderModifiers);
  }

  public abstract void UpdateUI(TileData tile);

  public void SetSortingLayerID(ISortingLayerCalculator calculator)
  {
    this.m_SortingLayerCalculator = calculator;
    this.m_SortingLayerChanged = true;
  }

  private void Update()
  {
    if (!this.m_SortingLayerChanged)
      return;
    this.m_SortingLayerChanged = false;
    for (int index = 0; index < this.m_SortingOrderModifiers.Count; ++index)
      this.m_SortingOrderModifiers[index].sortingLayer = this.m_SortingLayerCalculator.GetSortingLayerName();
  }
}
