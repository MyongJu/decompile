﻿// Decompiled with JetBrains decompiler
// Type: AndroidImageImporter
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AndroidImageImporter : MonoBehaviour, IPlatformImageImporter
{
  private const string ClassName_Intent = "android.content.Intent";
  private const string ClassName_ComponentName = "android.content.ComponentName";
  private const string ClassName_ImageImporterPackage = "com.funplus.kingofavalon";
  private const string ClassName_ImageImporterActivity = "com.funplus.kingofavalon.ImageImportActivity";
  protected ImageImporter m_imageImporter;
  protected int m_requestType;
  protected int m_requestWidth;
  protected int m_requestHeight;
  protected string m_callbackReceiverName;
  protected string m_url;
  protected string m_token;
  private AndroidJavaClass m_unityPlayerClass;
  private AndroidJavaObject m_unityActivity;

  private void OnImageImported(string imagePath)
  {
    if (!(bool) ((Object) this.m_imageImporter))
      return;
    this.m_imageImporter.nofityImageImportedResult(!string.IsNullOrEmpty(imagePath), imagePath);
  }

  public void init(ImageImporter imageImporter)
  {
    this.m_imageImporter = imageImporter;
    this.m_unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
    this.m_unityActivity = this.m_unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
    this.m_callbackReceiverName = this.gameObject.name;
  }

  public void requestPickImage(int requestWidth, int requestHeight, string url, string token)
  {
    this.m_requestWidth = requestWidth;
    this.m_requestHeight = requestHeight;
    this.m_requestType = 1;
    this.m_url = url;
    this.m_token = token;
    this.m_unityActivity.Call("runOnUiThread", new object[1]
    {
      (object) new AndroidJavaRunnable(this.runOnUiThread)
    });
  }

  public void requestCaptureImage(int requestWidth, int requestHeight, string url, string token)
  {
    this.m_requestWidth = requestWidth;
    this.m_requestHeight = requestHeight;
    this.m_requestType = 0;
    this.m_url = url;
    this.m_token = token;
    this.m_unityActivity.Call("runOnUiThread", new object[1]
    {
      (object) new AndroidJavaRunnable(this.runOnUiThread)
    });
  }

  protected void runOnUiThread()
  {
    AndroidJavaObject androidJavaObject1 = new AndroidJavaObject("android.content.Intent", new object[0]);
    AndroidJavaObject androidJavaObject2 = new AndroidJavaObject("android.content.ComponentName", new object[2]
    {
      (object) this.m_unityActivity.Call<string>("getPackageName"),
      (object) "com.funplus.kingofavalon.ImageImportActivity"
    });
    androidJavaObject1.Call<AndroidJavaObject>("setComponent", new object[1]
    {
      (object) androidJavaObject2
    });
    androidJavaObject1.Call<AndroidJavaObject>("putExtra", (object) "type", (object) this.m_requestType);
    androidJavaObject1.Call<AndroidJavaObject>("putExtra", (object) "width", (object) this.m_requestWidth);
    androidJavaObject1.Call<AndroidJavaObject>("putExtra", (object) "height", (object) this.m_requestHeight);
    androidJavaObject1.Call<AndroidJavaObject>("putExtra", (object) "gameobject", (object) this.m_callbackReceiverName);
    androidJavaObject1.Call<AndroidJavaObject>("putExtra", (object) "function", (object) "OnImageImported");
    androidJavaObject1.Call<AndroidJavaObject>("putExtra", (object) "url", (object) this.m_url);
    androidJavaObject1.Call<AndroidJavaObject>("putExtra", (object) "token", (object) this.m_token);
    this.m_unityActivity.Call("startActivity", new object[1]
    {
      (object) androidJavaObject1
    });
  }
}
