﻿// Decompiled with JetBrains decompiler
// Type: ResearchInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ResearchInfoDlg : UI.Dialog
{
  public GameObject requireGameObject;
  public GameObject rewardGameObject;

  public void Show(TechLevel techLevel)
  {
    this.GetComponentInChildren<RequirementAndRewardTab>().Init(techLevel);
  }

  private ConstructionBuildingStats.Requirement ConvertCityDataResourceTypes(CityManager.ResourceTypes type)
  {
    switch (type)
    {
      case CityManager.ResourceTypes.FOOD:
        return ConstructionBuildingStats.Requirement.FOOD;
      case CityManager.ResourceTypes.SILVER:
        return ConstructionBuildingStats.Requirement.SILVER;
      case CityManager.ResourceTypes.WOOD:
        return ConstructionBuildingStats.Requirement.WOOD;
      case CityManager.ResourceTypes.ORE:
        return ConstructionBuildingStats.Requirement.ORE;
      default:
        return ConstructionBuildingStats.Requirement.NONE;
    }
  }
}
