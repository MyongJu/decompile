﻿// Decompiled with JetBrains decompiler
// Type: ConfigBuildingGloryMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigBuildingGloryMain
{
  private List<BuildingGloryMainInfo> _buildingGloryMainInfoList = new List<BuildingGloryMainInfo>();
  private Dictionary<string, BuildingGloryMainInfo> _datas;
  private Dictionary<int, BuildingGloryMainInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<BuildingGloryMainInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, BuildingGloryMainInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._buildingGloryMainInfoList.Add(enumerator.Current);
    }
  }

  public void Clear()
  {
    if (this._datas != null)
      this._datas.Clear();
    if (this._dicByUniqueId != null)
      this._dicByUniqueId.Clear();
    if (this._buildingGloryMainInfoList == null)
      return;
    this._buildingGloryMainInfoList.Clear();
  }

  public List<BuildingGloryMainInfo> GetBuildingGloryMainInfoList()
  {
    return this._buildingGloryMainInfoList;
  }

  public BuildingGloryMainInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (BuildingGloryMainInfo) null;
  }

  public BuildingGloryMainInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (BuildingGloryMainInfo) null;
  }

  public BuildingGloryMainInfo GetNextBuildingGloryInfo(int internalId)
  {
    BuildingGloryMainInfo buildingGloryMainInfo = this.Get(internalId);
    if (buildingGloryMainInfo != null)
      return this.Get(buildingGloryMainInfo.nextId);
    return (BuildingGloryMainInfo) null;
  }
}
