﻿// Decompiled with JetBrains decompiler
// Type: SpriteCreator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class SpriteCreator
{
  protected Dictionary<string, UnityEngine.Sprite> m_allSprite = new Dictionary<string, UnityEngine.Sprite>();

  public UnityEngine.Sprite CreateSprite(string texturePath)
  {
    if (this.m_allSprite.ContainsKey(texturePath))
      return this.m_allSprite[texturePath];
    Texture2D texture = AssetManager.Instance.Load(texturePath, (System.Type) null) as Texture2D;
    if ((UnityEngine.Object) texture == (UnityEngine.Object) null)
      texture = BuilderFactory.Instance.MissingTexture as Texture2D;
    UnityEngine.Sprite sprite = UnityEngine.Sprite.Create(texture, new Rect(0.0f, 0.0f, (float) texture.width, (float) texture.height), new Vector2(0.5f, 0.5f));
    this.m_allSprite.Add(texturePath, sprite);
    return sprite;
  }

  public void DestroyAllCreated()
  {
    using (Dictionary<string, UnityEngine.Sprite>.Enumerator enumerator = this.m_allSprite.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, UnityEngine.Sprite> current = enumerator.Current;
        if ((UnityEngine.Object) AssetManager.Instance != (UnityEngine.Object) null)
          AssetManager.Instance.UnLoadAsset(current.Key, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      }
    }
    this.m_allSprite.Clear();
  }
}
