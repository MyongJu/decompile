﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragonTendencyBoostInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class ConfigDragonTendencyBoostInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "light_boost")]
  public string light_boost;
  [Config(Name = "dark_boost")]
  public string dark_boost;
  [Config(Name = "localization")]
  public string localization;
  [Config(Name = "skill_description")]
  public string skill_description;
  [Config(Name = "icon")]
  public string icon;
  [Config(Name = "display_value")]
  public float display_value;
  [Config(CustomParse = true, CustomType = typeof (Benefits), Name = "Benefits")]
  public Benefits Benefits;

  public string IconPath
  {
    get
    {
      return "Texture/Dragon/IndividualizedSkill/" + this.icon;
    }
  }

  public string LocalSkillDesc
  {
    get
    {
      return ScriptLocalization.Get(this.skill_description, true);
    }
  }
}
