﻿// Decompiled with JetBrains decompiler
// Type: ConfigLuckyArcherRewards
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigLuckyArcherRewards
{
  private List<LuckyArcherRewardsInfo> _luckyArcherRewardsInfoList = new List<LuckyArcherRewardsInfo>();
  private Dictionary<string, LuckyArcherRewardsInfo> _datas;
  private Dictionary<int, LuckyArcherRewardsInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<LuckyArcherRewardsInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, LuckyArcherRewardsInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._luckyArcherRewardsInfoList.Add(enumerator.Current);
    }
    this._luckyArcherRewardsInfoList.Sort((Comparison<LuckyArcherRewardsInfo>) ((a, b) => a.step.CompareTo(b.step)));
  }

  public List<LuckyArcherRewardsInfo> GetLuckyArcherRewardsInfoList()
  {
    return this._luckyArcherRewardsInfoList;
  }

  public LuckyArcherRewardsInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (LuckyArcherRewardsInfo) null;
  }

  public LuckyArcherRewardsInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (LuckyArcherRewardsInfo) null;
  }
}
