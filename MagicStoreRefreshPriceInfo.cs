﻿// Decompiled with JetBrains decompiler
// Type: MagicStoreRefreshPriceInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class MagicStoreRefreshPriceInfo
{
  public const string REFRESH_PRICE_PRE = "magic_shop_refresh_price_";
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "price")]
  public int Price;
  public int index;
}
