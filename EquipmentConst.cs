﻿// Decompiled with JetBrains decompiler
// Type: EquipmentConst
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class EquipmentConst
{
  public static Color QUALITY_0 = Utils.GetColorFromHex(8817284U);
  public static Color QUALITY_1 = Utils.GetColorFromHex(3379237U);
  public static Color QUALITY_2 = Utils.GetColorFromHex(3765415U);
  public static Color QUALITY_3 = Utils.GetColorFromHex(9974686U);
  public static Color QUALITY_4 = Utils.GetColorFromHex(13197569U);
  public static Color QUALITY_5 = Utils.GetColorFromHex(16762624U);
  public static Dictionary<int, Color> QULITY_COLOR_MAP = new Dictionary<int, Color>()
  {
    {
      0,
      EquipmentConst.QUALITY_0
    },
    {
      1,
      EquipmentConst.QUALITY_1
    },
    {
      2,
      EquipmentConst.QUALITY_2
    },
    {
      3,
      EquipmentConst.QUALITY_3
    },
    {
      4,
      EquipmentConst.QUALITY_4
    },
    {
      5,
      EquipmentConst.QUALITY_5
    }
  };
  public static Dictionary<int, string> QULITY_HEX_COLOR_MAP = new Dictionary<int, string>()
  {
    {
      0,
      "868a84"
    },
    {
      1,
      "339025"
    },
    {
      2,
      "3974a7"
    },
    {
      3,
      "98339e"
    },
    {
      4,
      "c96101"
    },
    {
      5,
      "ffc700"
    }
  };
}
