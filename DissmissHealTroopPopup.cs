﻿// Decompiled with JetBrains decompiler
// Type: DissmissHealTroopPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class DissmissHealTroopPopup : Popup
{
  public StandardProgressBar progressBar;
  public UILabel totalLabel;
  public UILabel descLabel;
  private Unit_StatisticsInfo TSC;
  private DissmissHealTroopPopup.HospitalType m_hospitalType;

  public void Init(HealTroopInfo info, DissmissHealTroopPopup.HospitalType hospitalType = DissmissHealTroopPopup.HospitalType.CityHospital)
  {
    this.m_hospitalType = hospitalType;
    this.progressBar.Init(0, info.total);
    this.TSC = ConfigManager.inst.DB_Unit_Statistics.GetData(info.ID);
    this.descLabel.text = string.Format(Utils.XLAT("hospital_dismiss_troops_description"), (object) Utils.XLAT(this.TSC.Troop_Name_LOC_ID));
    this.totalLabel.text = "/" + Utils.FormatThousands(info.total.ToString());
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnDismissBtnClick()
  {
    if (this.progressBar.CurrentCount <= 0)
      return;
    string str1 = !(this.TSC.FromBuildingType == "fortress") ? "barracks_uppercase_dismiss" : "barracks_uppercase_destroy";
    string str2 = !(this.TSC.FromBuildingType == "fortress") ? "dismiss_troops_question" : "destroy_traps_question";
    UIManager.inst.OpenPopup("DismissTroopConfirmPopup", (Popup.PopupParameter) new DismissTroopConfirmPopup.Parameter()
    {
      titleKey = str1,
      contentKey = str2,
      okKey = "id_uppercase_yes",
      noKey = "id_uppercase_no",
      okCallback = new System.Action(this.OnCancelYesPressed),
      noCallBack = new System.Action(this.OnCancelNoPressed),
      unit = this.TSC
    });
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnCancelYesPressed()
  {
    if (this.m_hospitalType == DissmissHealTroopPopup.HospitalType.CityHospital)
      MessageHub.inst.GetPortByAction("City:dismissHospitalTroops").SendRequest(Utils.Hash((object) "city_id", (object) CityManager.inst.GetCityID(), (object) "troops", (object) Utils.Hash((object) this.TSC.ID, (object) this.progressBar.CurrentCount)), (System.Action<bool, object>) ((bSuccess, data) =>
      {
        if (bSuccess)
          ;
      }), true);
    if (this.m_hospitalType == DissmissHealTroopPopup.HospitalType.AllianceHospital)
      MessageHub.inst.GetPortByAction("Alliance:dismissHospitalTroops").SendRequest(Utils.Hash((object) "alliance_id", (object) PlayerData.inst.allianceId, (object) "troops", (object) Utils.Hash((object) this.TSC.ID, (object) this.progressBar.CurrentCount)), (System.Action<bool, object>) ((bSuccess, data) =>
      {
        if (bSuccess)
          ;
      }), true);
    if (this.m_hospitalType != DissmissHealTroopPopup.HospitalType.KingdomHospital)
      return;
    MessageHub.inst.GetPortByAction("city:dismissKingdomHospitalTroops").SendRequest(Utils.Hash((object) "alliance_id", (object) PlayerData.inst.allianceId, (object) "troops", (object) Utils.Hash((object) this.TSC.ID, (object) this.progressBar.CurrentCount)), (System.Action<bool, object>) ((bSuccess, data) =>
    {
      if (bSuccess)
        ;
    }), true);
  }

  private void OnCancelNoPressed()
  {
  }

  public enum HospitalType
  {
    CityHospital,
    AllianceHospital,
    KingdomHospital,
  }
}
