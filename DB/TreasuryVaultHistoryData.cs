﻿// Decompiled with JetBrains decompiler
// Type: DB.TreasuryVaultHistoryData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class TreasuryVaultHistoryData : BaseData
  {
    private long _id;
    private long _uid;
    private int _slotId;
    private string _rewards;
    private long _cTime;

    public long Id
    {
      get
      {
        return this._id;
      }
    }

    public long Uid
    {
      get
      {
        return this._uid;
      }
    }

    public int SlotId
    {
      get
      {
        return this._slotId;
      }
    }

    public string Rewards
    {
      get
      {
        return this._rewards;
      }
    }

    public long Cime
    {
      get
      {
        return this._cTime;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      return false | DatabaseTools.UpdateData(inData, "id", ref this._id) | DatabaseTools.UpdateData(inData, "uid", ref this._uid) | DatabaseTools.UpdateData(inData, "slot_id", ref this._slotId) | DatabaseTools.UpdateData(inData, "rewards", ref this._rewards) | DatabaseTools.UpdateData(inData, "ctime", ref this._cTime);
    }

    public struct Params
    {
      public const string KEY = "treasury_vault_history";
      public const string ID = "id";
      public const string UID = "uid";
      public const string SLOT_ID = "slot_id";
      public const string REWARDS = "rewards";
      public const string CTIME = "ctime";
    }
  }
}
