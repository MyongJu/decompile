﻿// Decompiled with JetBrains decompiler
// Type: DB.ZoneBorderData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DB
{
  public class ZoneBorderData
  {
    private List<int> m_AreaList = new List<int>();
    private List<ZoneBorderTileData> m_BorderList = new List<ZoneBorderTileData>();
    private Dictionary<Coordinate, bool> m_AreaDict = new Dictionary<Coordinate, bool>();
    private Coordinate m_Location;
    private Vector3 m_Position;
    private long m_AllianceID;
    private long m_FortressID;
    private int m_Length;
    private bool m_IsFortress;

    public List<ZoneBorderTileData> Borders
    {
      get
      {
        return this.m_BorderList;
      }
    }

    public Coordinate Location
    {
      get
      {
        return this.m_Location;
      }
    }

    public Vector3 Position
    {
      get
      {
        return this.m_Position;
      }
    }

    public long AllianceID
    {
      get
      {
        return this.m_AllianceID;
      }
    }

    public long FortressID
    {
      get
      {
        return this.m_FortressID;
      }
    }

    public bool IsFortress
    {
      get
      {
        return this.m_IsFortress;
      }
    }

    public void Decode(object orgData)
    {
      this.Clear();
      Hashtable inData1 = orgData as Hashtable;
      if (inData1 == null)
        return;
      ArrayList arrayList1 = inData1[(object) "borders"] as ArrayList;
      if (arrayList1 == null)
        return;
      ArrayList arrayList2 = inData1[(object) "area"] as ArrayList;
      if (arrayList2 == null)
        return;
      DatabaseTools.UpdateData(inData1, "alliance_id", ref this.m_AllianceID);
      DatabaseTools.UpdateData(inData1, "fortress_id", ref this.m_FortressID);
      DatabaseTools.UpdateData(inData1, "world_id", ref this.m_Location.K);
      DatabaseTools.UpdateData(inData1, "x", ref this.m_Location.X);
      DatabaseTools.UpdateData(inData1, "y", ref this.m_Location.Y);
      Coordinate coordinate = new Coordinate(this.m_Location.K, this.m_Location.X, this.m_Location.Y + 1);
      this.m_Position = PVPMapData.MapData.ConvertTileKXYToPixelPosition(coordinate);
      this.m_Length = arrayList2.Count;
      for (int index = 0; index < this.m_Length; ++index)
        this.m_AreaList.Add(int.Parse(arrayList2[index].ToString()));
      this.m_IsFortress = (this.m_Length & 1) == 0;
      this.BuildArea();
      int count = arrayList1.Count;
      for (int index = 0; index < count; ++index)
      {
        Hashtable inData2 = arrayList1[index] as Hashtable;
        int outData1 = 0;
        DatabaseTools.UpdateData(inData2, "x", ref outData1);
        int outData2 = 0;
        DatabaseTools.UpdateData(inData2, "y", ref outData2);
        ZoneBorderTileData zoneBorderTileData = new ZoneBorderTileData();
        zoneBorderTileData.location = new Coordinate(this.m_Location.K, outData1, outData2);
        zoneBorderTileData.position = PVPMapData.MapData.ConvertTileKXYToPixelPosition(zoneBorderTileData.location);
        zoneBorderTileData.localPosition = zoneBorderTileData.position - this.m_Position;
        zoneBorderTileData.Update(this);
        this.m_BorderList.Add(zoneBorderTileData);
      }
    }

    public bool Contains(Coordinate location)
    {
      bool flag = false;
      this.m_AreaDict.TryGetValue(location, out flag);
      return flag;
    }

    private void Clear()
    {
      this.m_Location = new Coordinate();
      this.m_Position = Vector3.zero;
      this.m_AllianceID = 0L;
      this.m_FortressID = 0L;
      this.m_Length = 0;
      this.m_IsFortress = false;
      this.m_AreaList.Clear();
      this.m_BorderList.Clear();
      this.m_AreaDict.Clear();
    }

    private void BuildArea()
    {
      int num1 = !this.m_IsFortress ? this.m_Length - 1 >> 1 : this.m_Length - 2 >> 1;
      Coordinate location = this.m_Location;
      location.Y -= num1 * 2;
      for (int index1 = 0; index1 < this.m_Length; ++index1)
      {
        for (int index2 = 0; index2 < this.m_Length; ++index2)
        {
          Coordinate index3 = location;
          index3.X -= index2;
          index3.Y += index2;
          int num2 = 1 << index2;
          bool flag = (this.m_AreaList[index1] & num2) == num2;
          this.m_AreaDict[index3] = flag;
        }
        ++location.X;
        ++location.Y;
      }
    }
  }
}
