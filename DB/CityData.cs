﻿// Decompiled with JetBrains decompiler
// Type: DB.CityData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class CityData : BaseData
  {
    private Dictionary<int, CityData.MarchSetData> _marchSetDatas = new Dictionary<int, CityData.MarchSetData>();
    private List<int> _artifacts = new List<int>();
    private Dictionary<int, ArtifactLimitedData> _artifactsLimited = new Dictionary<int, ArtifactLimitedData>();
    private Dictionary<int, long> _killSkillEndTime = new Dictionary<int, long>();
    public const long INVALID_ID = -1;
    public const int CLEAR_AWAY_NORMAL = 0;
    public const int CLEAR_AWAY_REASON_IN_KINGDOM = 1;
    public const int CLEAR_AWAY_REASON_OUT_KINGDOM = 2;
    public const int CLEAR_AWAY_REASON_SEND_BACK_COUNTRY = 3;
    public const int CLEAR_AWAY_REASON_WORLD_WAR = 4;
    public const int CLEAR_AWAY_REASON_ENTER_PIT = 5;
    public const int CLEAR_AWAY_REASON_LEAVE_PIT = 6;
    public const int CLEAR_AWAY_REASON_FIRST_ENTER_PIT = 7;
    public const int CLEAR_AWAY_BANED = 8;
    public const int CLEAR_AWAY_REASON_AC_ENTER = 9;
    public const int CLEAR_AWAY_REASON_AC_BACK = 10;
    private long _cityId;
    private long _cityOwnerId;
    private string _cityName;
    private CityData.State _state;
    private int _level;
    private int _current_world_id;
    private Coordinate _lastCityLocation;
    private Coordinate _cityLocation;
    private long _storageValue;
    public CityTroopsInfo totalTroops;
    public CityTroopsInfo cityTroops;
    public CityHealingTroopsInfo hospitalTroops;
    public CityHealingTroopsInfo kingdomTroops;
    public long kingdomHospitalHealJob;
    public CityResourceInfo resources;
    public CityData.CityBuildingsInfo buildings;
    private long _peaceShieldJobId;
    private long _antiScoutJobId;
    private long _fakeArmyJobId;
    private int _defenseValue;
    private int _lastCityDefenseTime;
    public bool isLocationChanged;
    public bool teleporting;
    private Hashtable _blockInfo;
    private Hashtable _plotsInfo;
    private int _clearAway;
    private int _lastClearAway;
    private bool _triggerClearAway;
    private bool _triggerAntiClearAway;
    private long _in_fortress_id;
    private string _decoration;
    private int _peaceShieldCdTime;

    public CityData()
    {
      this._cityId = -1L;
    }

    public long key
    {
      get
      {
        return this.cityId;
      }
    }

    public CityData.MarchSetData GetMarchSetData(int index)
    {
      if (this._marchSetDatas.ContainsKey(index))
        return this._marchSetDatas[index];
      return (CityData.MarchSetData) null;
    }

    public int GetSetMarchSlot()
    {
      return (int) ConfigManager.inst.DB_BenefitCalc.GetFinalData(DBManager.inst.DB_Local_Benefit.Get("prop_troop_formation_save_base_value").total, "calc_troop_formation_save");
    }

    public long cityId
    {
      get
      {
        return this._cityId;
      }
    }

    public long cityOwnerId
    {
      get
      {
        return this._cityOwnerId;
      }
    }

    public string cityName
    {
      get
      {
        return this._cityName;
      }
    }

    public CityData.State state
    {
      get
      {
        return this._state;
      }
    }

    public int level
    {
      get
      {
        return this._level;
      }
    }

    public int current_world_id
    {
      get
      {
        return this._current_world_id;
      }
    }

    public Coordinate lastCityLocation
    {
      get
      {
        return this._lastCityLocation;
      }
    }

    public Coordinate cityLocation
    {
      get
      {
        return this._cityLocation;
      }
    }

    public long storageValue
    {
      get
      {
        return this._storageValue;
      }
    }

    public int marchSlot
    {
      get
      {
        return (int) ConfigManager.inst.DB_BenefitCalc.GetFinalData(DBManager.inst.DB_Local_Benefit.Get("prop_march_limit_base_value").total, "calc_march_limit");
      }
    }

    public long peaceShieldJobId
    {
      get
      {
        return this._peaceShieldJobId;
      }
    }

    public long antiScoutJobId
    {
      get
      {
        return this._antiScoutJobId;
      }
    }

    public long fakeArmyJobId
    {
      get
      {
        return this._fakeArmyJobId;
      }
    }

    public int defenseValue
    {
      get
      {
        return this._defenseValue;
      }
    }

    public int lastCityDefenseTime
    {
      get
      {
        return this._lastCityDefenseTime;
      }
    }

    public Hashtable blockInfo
    {
      get
      {
        return this._blockInfo;
      }
    }

    public Hashtable plotsInfo
    {
      get
      {
        return this._plotsInfo;
      }
    }

    public int clearAway
    {
      get
      {
        return this._clearAway;
      }
    }

    public int lastClearAway
    {
      get
      {
        return this._lastClearAway;
      }
    }

    public bool triggerClearAway
    {
      get
      {
        return this._triggerClearAway;
      }
    }

    public bool triggerAntiClearAway
    {
      get
      {
        return this._triggerAntiClearAway;
      }
    }

    public long in_fortress_id
    {
      get
      {
        return this._in_fortress_id;
      }
    }

    public string decoration
    {
      get
      {
        return this._decoration;
      }
    }

    public int peaceShieldCdTime
    {
      get
      {
        return this._peaceShieldCdTime;
      }
    }

    public int LeftPeaceShieldCdTime
    {
      get
      {
        int num = this._peaceShieldCdTime - NetServerTime.inst.ServerTimestamp;
        if (num > 0)
          return num;
        return 0;
      }
    }

    public List<int> Artifacts
    {
      get
      {
        return this._artifacts;
      }
    }

    public Dictionary<int, ArtifactLimitedData> ArtifactsLimited
    {
      get
      {
        return this._artifactsLimited;
      }
    }

    public ArtifactLimitedData GetArtifactLimitedDataByPurchasedId(int purchasedIndex)
    {
      if (this._artifactsLimited != null && this._artifactsLimited.ContainsKey(purchasedIndex))
        return this._artifactsLimited[purchasedIndex];
      return (ArtifactLimitedData) null;
    }

    public Dictionary<int, long> KillSkillEndTime
    {
      get
      {
        return this._killSkillEndTime;
      }
    }

    public List<ArtifactData> GetAllUnequipedArtifacts(int slotIndex)
    {
      List<ArtifactData> artifactDataList = new List<ArtifactData>();
      HeroData heroData = DBManager.inst.DB_hero.Get(this._cityId);
      UserData userData = DBManager.inst.DB_User.Get(this._cityId);
      if (this._artifacts != null && userData != null)
      {
        using (List<int>.Enumerator enumerator = this._artifacts.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            int current = enumerator.Current;
            if (heroData != null && !heroData.IsArtifactEquiped(current))
            {
              ArtifactData artifactData = DBManager.inst.DB_Artifact.Get(userData.world_id, current);
              if (artifactData != null)
              {
                ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(artifactData.ArtifactId);
                if (artifactInfo != null && artifactInfo.position == slotIndex)
                  artifactDataList.Add(artifactData);
              }
            }
          }
        }
      }
      return artifactDataList;
    }

    public int GetSpecificArtifact(string activeEffect)
    {
      if (this._artifacts != null)
      {
        for (int index = 0; index < this._artifacts.Count; ++index)
        {
          ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this._artifacts[index]);
          if (artifactInfo != null && artifactInfo.activeEffect == activeEffect)
            return artifactInfo.internalId;
        }
      }
      return 0;
    }

    public bool HasAvailableArtifact(string activeEffect)
    {
      return this.HasAvailableArtifact(this.GetSpecificArtifact(activeEffect));
    }

    public bool HasAvailableArtifact(int artifactId)
    {
      if (artifactId <= 0)
        return false;
      UserData userData = DBManager.inst.DB_User.Get(this.cityId);
      if (userData == null)
        return false;
      ArtifactData artifactData = DBManager.inst.DB_Artifact.GetArtifactData(userData.world_id, artifactId);
      if (artifactData != null)
        return !artifactData.IsCooldown;
      return true;
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckUpdateTime(updateTime))
        return false;
      this._updateTime = updateTime;
      if (orgData == null || long.TryParse(orgData.ToString(), out this._cityId))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag1 = false | DatabaseTools.UpdateData(inData, "city_id", ref this._cityId) | DatabaseTools.UpdateData(inData, "uid", ref this._cityOwnerId) | DatabaseTools.UpdateData(inData, "name", ref this._cityName) | DatabaseTools.UpdateData(inData, "current_world_id", ref this._current_world_id);
      if (inData.ContainsKey((object) "state"))
      {
        CityData.State state = (CityData.State) Enum.Parse(typeof (CityData.State), inData[(object) "state"].ToString());
        flag1 |= this._state == state;
        this._state = state;
      }
      bool flag2 = flag1 | DatabaseTools.UpdateData(inData, "level", ref this._level);
      Coordinate cityLocation = this._cityLocation;
      bool flag3 = this._current_world_id == 0 ? this._cityLocation.Decode(inData[(object) "world_id"], inData[(object) "map_x"], inData[(object) "map_y"]) : this._cityLocation.Decode(inData[(object) "current_world_id"], inData[(object) "map_x"], inData[(object) "map_y"]);
      bool flag4 = flag2 | flag3;
      this.isLocationChanged = flag3 && cityLocation.K > 0;
      if (this.isLocationChanged)
        this._lastCityLocation = cityLocation;
      bool flag5 = flag4 | DatabaseTools.UpdateData(inData, "storage_protect_value", ref this._storageValue) | this.buildings.Decode(inData[(object) "levels"]);
      if (inData.ContainsKey((object) "total_troop"))
      {
        this.totalTroops.Decode(inData[(object) "total_troop"]);
        flag5 = true;
      }
      if (inData.ContainsKey((object) "city_troop"))
      {
        this.cityTroops.Decode(inData[(object) "city_troop"]);
        flag5 = true;
      }
      if (inData.ContainsKey((object) "hospital_troop"))
      {
        this.hospitalTroops.Decode(inData[(object) "hospital_troop"]);
        flag5 = true;
      }
      if (inData.ContainsKey((object) "kingdom_hospital_troop"))
      {
        this.kingdomTroops.Decode(inData[(object) "kingdom_hospital_troop"]);
        flag5 = true;
      }
      bool flag6 = flag5 | DatabaseTools.UpdateData(inData, "kingdom_hospital_job_id", ref this.kingdomHospitalHealJob) | DatabaseTools.UpdateData(inData, "peace_shield_job_id", ref this._peaceShieldJobId) | DatabaseTools.UpdateData(inData, "storage_protect_value", ref this._storageValue) | DatabaseTools.UpdateData(inData, "anti_scout_job_id", ref this._antiScoutJobId) | DatabaseTools.UpdateData(inData, "fake_army_job_id", ref this._fakeArmyJobId) | DatabaseTools.UpdateData(inData, "city_defense_value", ref this._defenseValue) | DatabaseTools.UpdateData(inData, "last_add_defense_time", ref this._lastCityDefenseTime) | DatabaseTools.UpdateData(inData, "in_fortress_id", ref this._in_fortress_id) | DatabaseTools.UpdateData(inData, "decoration", ref this._decoration) | DatabaseTools.UpdateData(inData, "peace_shield_cd_time", ref this._peaceShieldCdTime);
      this._lastClearAway = this._clearAway;
      bool flag7 = flag6 | DatabaseTools.UpdateData(inData, "clear_away", ref this._clearAway);
      this._triggerClearAway = this._lastClearAway != this._clearAway && this._clearAway != 0;
      this._triggerAntiClearAway = this._lastClearAway != 0 && this._clearAway == 0;
      if (inData.ContainsKey((object) "resource"))
        flag7 |= this.resources.Decode(inData[(object) "resource"]);
      if (inData.ContainsKey((object) "block_info"))
      {
        this._blockInfo = inData[(object) "block_info"] as Hashtable;
        flag7 = ((flag7 ? 1 : 0) | 1) != 0;
      }
      if (inData.ContainsKey((object) "plots_info"))
      {
        this._plotsInfo = inData[(object) "plots_info"] as Hashtable;
        flag7 = ((flag7 ? 1 : 0) | 1) != 0;
      }
      if (inData.ContainsKey((object) "troop_marshalling"))
      {
        Hashtable hashtable = inData[(object) "troop_marshalling"] as Hashtable;
        this._marchSetDatas.Clear();
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
          {
            string s = enumerator.Current.ToString();
            int result = -1;
            int.TryParse(s, out result);
            if (result > -1)
            {
              CityData.MarchSetData marchSetData = new CityData.MarchSetData();
              marchSetData.index = result;
              if (marchSetData.Decode(hashtable[enumerator.Current] as Hashtable))
                this._marchSetDatas.Add(result, marchSetData);
            }
          }
        }
      }
      if (inData.ContainsKey((object) "artifact"))
      {
        this._artifacts.Clear();
        ArrayList arrayList = inData[(object) "artifact"] as ArrayList;
        if (arrayList != null)
        {
          for (int index = 0; index < arrayList.Count; ++index)
          {
            int result = 0;
            int.TryParse(arrayList[index].ToString(), out result);
            if (result > 0)
              this._artifacts.Add(result);
          }
        }
        flag7 = ((flag7 ? 1 : 0) | 1) != 0;
      }
      if (inData.ContainsKey((object) "artifact_limit"))
      {
        this._artifactsLimited.Clear();
        Hashtable hashtable = inData[(object) "artifact_limit"] as Hashtable;
        if (hashtable != null)
        {
          foreach (object obj in (IEnumerable) hashtable.Values)
          {
            ArtifactLimitedData artifactLimitedData = new ArtifactLimitedData();
            artifactLimitedData.Decode(obj as Hashtable);
            if (!this._artifactsLimited.ContainsKey(artifactLimitedData.PurchasedIndex))
              this._artifactsLimited.Add(artifactLimitedData.PurchasedIndex, artifactLimitedData);
          }
        }
        flag7 = ((flag7 ? 1 : 0) | 1) != 0;
      }
      if (inData.ContainsKey((object) "king_skill_end_time"))
      {
        this._killSkillEndTime.Clear();
        Hashtable hashtable = inData[(object) "king_skill_end_time"] as Hashtable;
        if (hashtable != null)
        {
          IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
          while (enumerator.MoveNext())
          {
            int result1 = 0;
            int.TryParse(enumerator.Key.ToString(), out result1);
            long result2 = 0;
            long.TryParse(enumerator.Value.ToString(), out result2);
            this._killSkillEndTime.Add(result1, result2);
          }
        }
        flag7 = ((flag7 ? 1 : 0) | 1) != 0;
      }
      return flag7;
    }

    public string ShowInfo()
    {
      return "<color=#FFFF00>city data : " + (object) this.cityId + "(" + (object) (this._updateTime / 1000L % 1000L) + "." + (object) (this._updateTime % 1000L) + ")</color>\n" + "City Name : " + this.cityName + "\n" + "Owner Uid : " + (object) this.cityOwnerId + "\n" + "City Buildings :\n" + "\tstronghold : " + (object) this.buildings.level_stronghold + "\n" + "\tmarket : " + (object) this.buildings.level_market + "\n" + "\tembassy : " + (object) this.buildings.level_embassy + "\n" + "\thall of war: " + (object) this.buildings.level_hallOfWar + "\n";
    }

    public string ShowDetailInfo()
    {
      return "<color=#FFFF00>city data : " + (object) this.cityId + "(" + (object) (this._updateTime / 1000L % 1000L) + "." + (object) (this._updateTime % 1000L) + ")</color>\n" + "City Name : " + this.cityName + "\n" + "Owner Uid : " + (object) this.cityOwnerId + "\n" + "State : " + (object) this.state + "\n" + "level : " + (object) this.level + "\n" + "Location : " + this.cityLocation.ShowInfo() + "\n" + "storage : " + (object) this.storageValue + "\n" + "march slot : " + (object) this.marchSlot + "\n" + "Troops\n" + this.totalTroops.ShowInfo(1) + "\n" + this.cityTroops.ShowInfo(1) + "\n" + "Hospital\n" + this.hospitalTroops.ShowInfo(1) + "\n" + "Resource\n" + this.resources.ShowInfo(1) + "\n" + "City Buildings :\n" + "\tstronghold : " + (object) this.buildings.level_stronghold + "\n" + "\tmarket : " + (object) this.buildings.level_market + "\n" + "\tembassy : " + (object) this.buildings.level_embassy + "\n" + "\thall of war: " + (object) this.buildings.level_hallOfWar + "\n" + "\tJob (Peach Shield)" + (object) this.peaceShieldJobId + "\n" + "\tJob (Anti Scout)" + (object) this.antiScoutJobId + "\n" + "\tJob (Fake Army)" + (object) this.fakeArmyJobId + "\n";
    }

    public enum State
    {
      normal,
      smoke,
      onfire,
    }

    public struct Params
    {
      public const string KEY = "user_city";
      public const string CITY_ID = "city_id";
      public const string CITY_OWNER = "uid";
      public const string CITY_NAME = "name";
      public const string CITY_STATE = "state";
      public const string CITY_LEVEL = "level";
      public const string CITY_LOCATION_K = "world_id";
      public const string CURRENT_CITY_LOCATION_K = "current_world_id";
      public const string CITY_LOCATION_X = "map_x";
      public const string CITY_LOCATION_Y = "map_y";
      public const string CITY_STORAGE_VALUE = "storage_protect_value";
      public const string CITY_STORAGE_PERCENT = "storage_protect_percent";
      public const string CITY_CONSTRUCTION_PERCENT = "construction_speed_percent";
      public const string CITY_TOTAL_TROOPS = "total_troop";
      public const string CITY_CITY_TROOPS = "city_troop";
      public const string CITY_HOSPITAL_TROOP = "hospital_troop";
      public const string CITY_KINGDOM_HOSPITAL_TROOP = "kingdom_hospital_troop";
      public const string CITY_KINGDOM_HOSPITAL_HEAL_JOB = "kingdom_hospital_job_id";
      public const string CITY_RESOURCE = "resource";
      public const string CITY_LEVELS = "levels";
      public const string CITY_PEACH_SHIELD_JOB_ID = "peace_shield_job_id";
      public const string CITY_ANTI_SCOUNT_JOB_ID = "anti_scout_job_id";
      public const string CITY_FACK_ARMY_JOB_ID = "fake_army_job_id";
      public const string CITY_CITY_DEFENSE_VALUE = "city_defense_value";
      public const string CITY_LAST_ADD_DEFENSE_TIME = "last_add_defense_time";
      public const string BLOCK_INFO = "block_info";
      public const string PLOTS_INFO = "plots_info";
      public const string CITY_CLEAR_AWAY = "clear_away";
      public const string IN_FORTRESS_ID = "in_fortress_id";
      public const string DECORATION = "decoration";
      public const string CITY_ARTIFACT = "artifact";
      public const string CITY_ARTIFACT_LIMITED = "artifact_limit";
      public const string KING_SKILL_END_TIME = "king_skill_end_time";
      public const string PEACE_SHIELD_CD_TIME = "peace_shield_cd_time";
      public const string TROOP_MARSHALLING = "troop_marshalling";
    }

    public class MarchSetData
    {
      public int index = -1;
      public Dictionary<string, int> troops = new Dictionary<string, int>();
      public const string IS_DRAGON_IN = "is_dragon_in";
      public const string TROOP_INFO = "troops";
      public bool isDragonIn;
      private Hashtable _orginData;

      public Hashtable GetOrginData()
      {
        return this._orginData;
      }

      public bool Decode(Hashtable hash)
      {
        if (hash == null)
          return false;
        if (hash.ContainsKey((object) "is_dragon_in"))
          this.isDragonIn = hash[(object) "is_dragon_in"].ToString().ToLower() == "true";
        this._orginData = hash;
        Hashtable hashtable = hash[(object) "troops"] as Hashtable;
        if (hashtable != null && hashtable.Count > 0)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
          {
            string key = enumerator.Current.ToString();
            string s = hashtable[enumerator.Current].ToString();
            int result = -1;
            int.TryParse(s, out result);
            if (result > 0)
              this.troops.Add(key, result);
          }
        }
        return true;
      }
    }

    public struct CityBuildings
    {
      public const int BUILDING_STRONGHOLD = 1;
      public const int BUILDING_PVE_PORTAL = 2;
      public const int BUILDING_HOSPITAL = 3;
      public const int BUILDING_HOUSE = 4;
      public const int BUILDING_LUMBER_MILL = 5;
      public const int BUILDING_MINE = 6;
      public const int BUILDING_FARM = 7;
      public const int BUILDING_BARRACKS = 8;
      public const int BUILDING_RANGE = 9;
      public const int BUILDING_STABLES = 10;
      public const int BUILDING_SANCTUM = 11;
      public const int BUILDING_WORKSHOP = 12;
      public const int BUILDING_HERO_TOWER = 13;
      public const int BUILDING_WALLS = 14;
      public const int BUILDING_STOREHOUSE = 15;
      public const int BUILDING_PRISON = 16;
      public const int BUILDING_ALTAR = 17;
      public const int BUILDING_FORGE = 18;
      public const int BUILDING_WATCHTOWER = 19;
      public const int BUILDING_EMBASSY = 20;
      public const int BUILDING_KNIGHTS_HALL = 21;
      public const int BUILDING_WAR_RALLY = 22;
      public const int BUILDING_UNIVERSITY = 23;
      public const int BUILDING_MARKETPLACE = 24;
      public const int BUILDING_PARADE = 25;
      public const int buildingCount = 26;
    }

    public struct CityBuildingsInfo
    {
      public int[] buildings;

      public int level_stronghold
      {
        get
        {
          return this.buildings[1];
        }
      }

      public int level_embassy
      {
        get
        {
          return this.buildings[20];
        }
      }

      public int level_market
      {
        get
        {
          return this.buildings[24];
        }
      }

      public int level_hallOfWar
      {
        get
        {
          return this.buildings[22];
        }
      }

      public bool isStrongholdExist
      {
        get
        {
          return this.level_stronghold > 0;
        }
      }

      public bool isEmbassyExist
      {
        get
        {
          return this.level_embassy > 0;
        }
      }

      public bool isMarketExist
      {
        get
        {
          return this.level_market > 0;
        }
      }

      public bool isHallOfWarExist
      {
        get
        {
          return this.level_hallOfWar > 0;
        }
      }

      public bool Decode(object orgData)
      {
        if (this.buildings == null)
          this.buildings = new int[26];
        if (orgData == null)
          return false;
        Hashtable inData = orgData as Hashtable;
        if (inData == null)
          return false;
        for (int index = 0; index < this.buildings.Length; ++index)
          this.buildings[index] = 0;
        bool flag = false;
        IEnumerator enumerator = inData.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          int result = 0;
          if (int.TryParse(enumerator.Current.ToString(), out result) && result < 26)
            flag |= DatabaseTools.UpdateData(inData, enumerator.Current.ToString(), ref this.buildings[result]);
        }
        return flag;
      }
    }
  }
}
