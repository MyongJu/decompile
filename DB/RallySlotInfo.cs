﻿// Decompiled with JetBrains decompiler
// Type: DB.RallySlotInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace DB
{
  public struct RallySlotInfo
  {
    private static RallySlotInfo _lockSlot = new RallySlotInfo()
    {
      uid = 0,
      marchId = 0,
      state = RallySlotInfo.RallyState.locked
    };
    private static RallySlotInfo _emptyUnlockSlot = new RallySlotInfo()
    {
      uid = 0,
      marchId = 0,
      state = RallySlotInfo.RallyState.empty
    };
    public RallySlotInfo.RallyState state;
    public long uid;
    public long marchId;

    public bool Decode(object inUid, object inMarchId)
    {
      bool flag = false | long.TryParse(inUid.ToString(), out this.uid) | long.TryParse(inMarchId.ToString(), out this.marchId);
      if (this.uid != 0L)
      {
        if (this.marchId == 0L)
        {
          this.state = RallySlotInfo.RallyState.unlockedForUser;
          return flag;
        }
        this.state = RallySlotInfo.RallyState.joined;
        return flag;
      }
      this.state = RallySlotInfo.RallyState.locked;
      return false;
    }

    public static RallySlotInfo LockedSlot()
    {
      return RallySlotInfo._lockSlot;
    }

    public static RallySlotInfo UnlockedSlot()
    {
      return RallySlotInfo._emptyUnlockSlot;
    }

    public enum RallyState
    {
      locked,
      unlockedForUser,
      empty,
      joined,
    }

    public struct Params
    {
      public const string RALLY_SLOT_STATE = "rally_slot_state";
      public const string RALLY_SLOT_UID = "uid";
      public const string RALLY_SLOT_MARCH = "rally_march";
    }
  }
}
