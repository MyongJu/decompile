﻿// Decompiled with JetBrains decompiler
// Type: DB.DigSiteData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class DigSiteData : BaseData
  {
    private long _worldId;
    private long _siteId;
    private int _mapX;
    private int _mapY;
    private long _digTime;
    private int _configId;
    private long _mtime;
    private long _digStartTime;

    public long WorldId
    {
      get
      {
        return this._worldId;
      }
    }

    public long SiteId
    {
      get
      {
        return this._siteId;
      }
    }

    public int MapX
    {
      get
      {
        return this._mapX;
      }
    }

    public int MapY
    {
      get
      {
        return this._mapY;
      }
    }

    public long DigTime
    {
      get
      {
        return this._digTime;
      }
    }

    public int ConfigId
    {
      get
      {
        return this._configId;
      }
    }

    public long MTime
    {
      get
      {
        return this._mtime;
      }
    }

    public long DigStartTime
    {
      get
      {
        return this._digStartTime;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      return false | DatabaseTools.UpdateData(inData, "world_id", ref this._worldId) | DatabaseTools.UpdateData(inData, "site_id", ref this._siteId) | DatabaseTools.UpdateData(inData, "map_x", ref this._mapX) | DatabaseTools.UpdateData(inData, "map_y", ref this._mapY) | DatabaseTools.UpdateData(inData, "config_id", ref this._configId) | DatabaseTools.UpdateData(inData, "dig_time", ref this._digTime) | DatabaseTools.UpdateData(inData, "dig_starttime", ref this._digStartTime) | DatabaseTools.UpdateData(inData, "mtime", ref this._mtime);
    }

    public struct Params
    {
      public const string KEY = "dig_site";
      public const string WORLD_ID = "world_id";
      public const string SITE_ID = "site_id";
      public const string MAP_X = "map_x";
      public const string MAP_Y = "map_y";
      public const string CONFIG_ID = "config_id";
      public const string DIG_TIME = "dig_time";
      public const string DIG_START_TIME = "dig_starttime";
      public const string MTIME = "mtime";
    }
  }
}
