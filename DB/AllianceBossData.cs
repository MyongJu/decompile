﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceBossData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceBossData : BaseData
  {
    protected long alliance_id;
    protected long bossId;
    protected long bossHP;
    protected string name;
    protected long ctime;
    protected long utime;
    protected Dictionary<string, long> troops;
    protected Dictionary<string, long> startTroops;

    public long AllianceId
    {
      get
      {
        return this.alliance_id;
      }
    }

    public long BossId
    {
      get
      {
        return this.bossId;
      }
    }

    public long BossHP
    {
      get
      {
        return this.bossHP;
      }
    }

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public long CTime
    {
      get
      {
        return this.ctime;
      }
    }

    public long UTime
    {
      get
      {
        return this.utime;
      }
    }

    public long troopsCount
    {
      get
      {
        long num = 0;
        using (Dictionary<string, long>.Enumerator enumerator = this.troops.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, long> current = enumerator.Current;
            num += current.Value;
          }
        }
        return num;
      }
    }

    public long startTroopsCount
    {
      get
      {
        long num = 0;
        using (Dictionary<string, long>.Enumerator enumerator = this.startTroops.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, long> current = enumerator.Current;
            num += current.Value;
          }
        }
        return num;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag1 = false | DatabaseTools.UpdateData(inData, "alliance_id", ref this.alliance_id) | DatabaseTools.UpdateData(inData, "boss_id", ref this.bossId) | DatabaseTools.UpdateData(inData, "hp", ref this.bossHP);
      if (inData.ContainsKey((object) "troops"))
      {
        this.troops = new Dictionary<string, long>();
        Hashtable hashtable = inData[(object) "troops"] as Hashtable;
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
            this.troops.Add(enumerator.Current.ToString(), long.Parse(hashtable[enumerator.Current].ToString()));
        }
        flag1 = ((flag1 ? 1 : 0) | 1) != 0;
      }
      if (inData.ContainsKey((object) "start_troops"))
      {
        this.startTroops = new Dictionary<string, long>();
        Hashtable hashtable = inData[(object) "start_troops"] as Hashtable;
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
            this.startTroops.Add(enumerator.Current.ToString(), long.Parse(hashtable[enumerator.Current].ToString()));
        }
        flag1 = ((flag1 ? 1 : 0) | 1) != 0;
      }
      bool flag2 = flag1 | DatabaseTools.UpdateData(inData, "ctime", ref this.ctime) | DatabaseTools.UpdateData(inData, "utime", ref this.utime);
      return true;
    }

    public struct Params
    {
      public const string KEY = "alliance_boss";
      public const string ALLIANCE_ID = "alliance_id";
      public const string BOSS_ID = "boss_id";
      public const string HP = "hp";
      public const string CTIME = "ctime";
      public const string UTIME = "utime";
      public const string START_TROOPS = "start_troops";
      public const string TROOPS = "troops";
    }
  }
}
