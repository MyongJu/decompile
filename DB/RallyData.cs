﻿// Decompiled with JetBrains decompiler
// Type: DB.RallyData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class RallyData : BaseData
  {
    public const long INVALID_ID = -1;
    public const int FREE_RALLY_SLOT_NUMBER = 3;
    public const int MAX_RALLY_SLOT_NUMBER = 50;
    private long _rallyId;
    private int _worldId;
    private Coordinate _location;
    private long _jobId;
    private TimeDurationInfo _waitTimeDuration;
    private long _owneUid;
    private long _ownerCityId;
    private long _ownerAllianceId;
    private long _targetUid;
    private int _bossId;
    private int _fortressId;
    private long _targetCityId;
    private long _targetAllianceId;
    public Dictionary<long, RallySlotInfo> slotsInfo_joined;
    public Dictionary<long, RallySlotInfo> slotsInfo_unlockForUser;
    private long _rallyMarchId;
    private int _type;
    private bool _isJoined;
    private bool _isUnlockSpecialSlot;
    private int _unlockSlotNumber;
    private bool _isDone;

    public RallyData()
    {
      this.rallyId = -1L;
      this.slotsInfo_joined = new Dictionary<long, RallySlotInfo>();
      this.slotsInfo_unlockForUser = new Dictionary<long, RallySlotInfo>();
    }

    public long rallyId
    {
      get
      {
        return this._rallyId;
      }
      private set
      {
        this._rallyId = value;
      }
    }

    public int worldId
    {
      get
      {
        return this._worldId;
      }
    }

    public Coordinate location
    {
      get
      {
        return this._location;
      }
    }

    public long jobId
    {
      get
      {
        return this._jobId;
      }
    }

    public TimeDurationInfo waitTimeDuration
    {
      get
      {
        return this._waitTimeDuration;
      }
    }

    public long ownerUid
    {
      get
      {
        return this._owneUid;
      }
    }

    public long ownerCityId
    {
      get
      {
        return this._ownerCityId;
      }
    }

    public long ownerAllianceId
    {
      get
      {
        return this._ownerAllianceId;
      }
    }

    public long targetUid
    {
      get
      {
        return this._targetUid;
      }
    }

    public int bossId
    {
      get
      {
        return this._bossId;
      }
    }

    public int fortressId
    {
      get
      {
        return this._fortressId;
      }
    }

    public long targetCityId
    {
      get
      {
        return this._targetCityId;
      }
    }

    public long targetAllianceId
    {
      get
      {
        return this._targetAllianceId;
      }
    }

    public long rallyMarchId
    {
      get
      {
        return this._rallyMarchId;
      }
    }

    public RallyData.RallyType type
    {
      get
      {
        return (RallyData.RallyType) this._type;
      }
    }

    public bool isJoined
    {
      get
      {
        return this._isJoined;
      }
    }

    public bool isUnlockSlot
    {
      get
      {
        return this._isUnlockSpecialSlot;
      }
    }

    public int unlockSlotNumber
    {
      get
      {
        return this._unlockSlotNumber;
      }
    }

    public int lockSlotNumber
    {
      get
      {
        return 50 - this.unlockSlotNumber;
      }
    }

    public long maxTroopsCount { get; private set; }

    public long totalTroopsCount
    {
      get
      {
        int num = 0;
        using (Dictionary<long, RallySlotInfo>.KeyCollection.Enumerator enumerator = this.slotsInfo_joined.Keys.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            long current = enumerator.Current;
            if (DBManager.inst.DB_March.Get(this.slotsInfo_joined[current].marchId) != null)
              num += DBManager.inst.DB_March.Get(this.slotsInfo_joined[current].marchId).troopsInfo.totalCount;
          }
        }
        return (long) num;
      }
    }

    public bool IsDefense()
    {
      return this.ownerAllianceId != PlayerData.inst.allianceId;
    }

    public long totalTroopsCapacity { get; private set; }

    public RallyData.RallyState state
    {
      get
      {
        if (this._isDone)
          return RallyData.RallyState.done;
        if (NetServerTime.inst.ServerTimestamp < this._waitTimeDuration.endTime)
          return RallyData.RallyState.waitRallying;
        return this._rallyMarchId == 0L ? RallyData.RallyState.waitAttacking : RallyData.RallyState.attacking;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckUpdateTime(updateTime))
        return false;
      this._updateTime = updateTime;
      if (orgData == null || long.TryParse(orgData.ToString(), out this._rallyId))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag1 = false;
      int type = this._type;
      bool flag2 = flag1 | DatabaseTools.UpdateData(inData, "type", ref type);
      if (Enum.IsDefined(typeof (RallyData.RallyType), (object) this._type))
      {
        this._type = type;
        bool flag3 = flag2 | DatabaseTools.UpdateData(inData, "rally_id", ref this._rallyId);
        if (inData[(object) "state"] != null)
          this._isDone = inData[(object) "state"].ToString() == "done";
        bool flag4 = flag3 | DatabaseTools.UpdateData(inData, "world_id", ref this._worldId) | DatabaseTools.UpdateData(inData, "world_id", ref this._location.K) | DatabaseTools.UpdateData(inData, "map_x", ref this._location.X) | DatabaseTools.UpdateData(inData, "map_y", ref this._location.Y) | DatabaseTools.UpdateData(inData, "march_id", ref this._rallyMarchId) | DatabaseTools.UpdateData(inData, "uid", ref this._owneUid) | DatabaseTools.UpdateData(inData, "city_id", ref this._ownerCityId) | DatabaseTools.UpdateData(inData, "alliance_id", ref this._ownerAllianceId) | DatabaseTools.UpdateData(inData, "opp_uid", ref this._targetUid) | DatabaseTools.UpdateData(inData, "opp_city_id", ref this._targetCityId) | DatabaseTools.UpdateData(inData, "opp_alliance_id", ref this._targetAllianceId) | DatabaseTools.UpdateData(inData, "boss_id", ref this._bossId) | DatabaseTools.UpdateData(inData, "fortress_id", ref this._fortressId) | this._waitTimeDuration.Decode(inData[(object) "time_start"], inData[(object) "time_end"]) | this.DecodeSlotInfos((object) inData);
        this.StartTimer();
        if (this._isDone)
          this.Remove(updateTime);
        return flag4;
      }
      D.error((object) ("Rally type not define : " + (object) type));
      return false;
    }

    private bool DecodeSlotInfos(object orgData)
    {
      if (orgData == null)
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | DatabaseTools.UpdateData(inData, "slots_num", ref this._unlockSlotNumber);
      Hashtable hashtable = inData[(object) "slots_info"] as Hashtable;
      if (hashtable == null)
        return flag;
      this._isJoined = false;
      this._isUnlockSpecialSlot = false;
      this.slotsInfo_joined.Clear();
      this.slotsInfo_unlockForUser.Clear();
      foreach (string key in (IEnumerable) hashtable.Keys)
      {
        RallySlotInfo rallySlotInfo = new RallySlotInfo();
        flag |= rallySlotInfo.Decode((object) key, hashtable[(object) key]);
        switch (rallySlotInfo.state)
        {
          case RallySlotInfo.RallyState.unlockedForUser:
            this.slotsInfo_unlockForUser.Add(rallySlotInfo.uid, rallySlotInfo);
            this._isJoined |= (this._isUnlockSpecialSlot |= rallySlotInfo.uid == GameEngine.Instance.PlayerData.uid);
            continue;
          case RallySlotInfo.RallyState.joined:
            this.slotsInfo_joined.Add(rallySlotInfo.uid, rallySlotInfo);
            this._isJoined |= rallySlotInfo.uid == GameEngine.Instance.PlayerData.uid;
            continue;
          default:
            continue;
        }
      }
      return flag;
    }

    public void StartTimer()
    {
      if (!this.isJoined)
      {
        this.RemoveTimer();
      }
      else
      {
        JobHandle job = JobManager.Instance.GetJob(this._jobId);
        if (job == null)
        {
          if (this.state == RallyData.RallyState.attacking)
          {
            MarchData marchData = DBManager.inst.DB_March.Get(this.rallyMarchId);
            if (marchData == null)
              return;
            Hashtable hashtable = new Hashtable();
            hashtable[(object) "marchId"] = (object) marchData.marchId;
            hashtable[(object) "marchData"] = (object) marchData;
            this._jobId = JobManager.Instance.CreateClientJob(JobEvent.JOB_PVP_MARCHING, marchData.timeDuration.startTime, marchData.timeDuration.endTime, (object) hashtable, new System.Action(this.OnMarchTimerEnd));
          }
          else
          {
            if (this.state != RallyData.RallyState.waitRallying)
              return;
            MarchData marchData = DBManager.inst.DB_March.Get(this.slotsInfo_joined[PlayerData.inst.uid].marchId);
            if (marchData == null || marchData.state != MarchData.MarchState.rallying || marchData.jobId != 0L)
              return;
            Hashtable hashtable = new Hashtable();
            hashtable[(object) "rally_id"] = (object) this.rallyId;
            hashtable[(object) "marchData"] = (object) marchData;
            hashtable[(object) "march_id"] = (object) marchData.marchId;
            this._jobId = JobManager.Instance.CreateClientJob(JobEvent.JOB_PVP_WAIT_RALLY, this._waitTimeDuration.startTime, this._waitTimeDuration.endTime, (object) hashtable, new System.Action(this.OnMarchTimerEnd));
          }
        }
        else
        {
          if (this.state != RallyData.RallyState.attacking)
            return;
          MarchData marchData = DBManager.inst.DB_March.Get(this.rallyMarchId);
          if (marchData == null)
            return;
          Hashtable hashtable = new Hashtable();
          hashtable[(object) "marchId"] = (object) marchData.marchId;
          hashtable[(object) "marchData"] = (object) marchData;
          job.Reset(JobEvent.JOB_PVP_MARCHING, marchData.timeDuration.startTime, marchData.timeDuration.endTime);
          job.Data = (object) hashtable;
          job.OnFinished = new System.Action(this.OnMarchTimerEnd);
        }
      }
    }

    private void OnMarchTimerEnd()
    {
      this.RemoveTimer();
      DBManager.inst.DB_Rally.Publish_OnRallyDataChanged(this._rallyId);
    }

    private void RemoveTimer()
    {
      if (this._jobId == 0L)
        return;
      JobManager.Instance.RemoveJob(this._jobId);
      this._jobId = 0L;
    }

    public bool MarchJoin(long inMarchId, long inUid)
    {
      if (this.slotsInfo_joined.ContainsKey(inUid))
        return false;
      if (this.slotsInfo_unlockForUser.ContainsKey(inUid))
        this.slotsInfo_unlockForUser.Remove(inUid);
      this.slotsInfo_joined.Add(inUid, new RallySlotInfo()
      {
        uid = inUid,
        marchId = inMarchId,
        state = RallySlotInfo.RallyState.joined
      });
      return true;
    }

    public bool Unlock(long inUid = 0)
    {
      if (this.lockSlotNumber <= 0)
        return false;
      if (inUid != 0L)
      {
        this.slotsInfo_unlockForUser.Add(inUid, new RallySlotInfo()
        {
          uid = inUid,
          marchId = 0L,
          state = RallySlotInfo.RallyState.unlockedForUser
        });
        this._isUnlockSpecialSlot |= inUid == GameEngine.Instance.PlayerData.uid;
      }
      return true;
    }

    private void Dispose()
    {
      this.RemoveTimer();
    }

    private void Remove(long updateTime)
    {
      DBManager.inst.DB_Rally.Remove(this.rallyId, updateTime);
      this.Dispose();
    }

    public string ShowInfo()
    {
      string str1 = "<color=#FFFF00>Rally Data : " + (object) this.rallyId + "</color>\n" + "State : " + (object) this.state + "\n" + "Wait Time : (" + (object) (this._waitTimeDuration.startTime % 1000) + " -> " + (object) (this._waitTimeDuration.endTime % 1000) + ")\n" + "Owner player : " + (object) this._owneUid + "\n" + "Owner alliance : " + (object) this._ownerAllianceId + "\n" + "Target player : " + (object) this._targetUid + "\n" + "Target Alliance : " + (object) this.targetAllianceId + "\n" + "Rally March Id : " + (object) this.rallyMarchId + "\n" + "rally slot (joined : " + (object) this.slotsInfo_joined.Count + " , unSlot : " + (object) this.slotsInfo_unlockForUser.Count + ", free slot :" + (object) this.unlockSlotNumber + ")\n" + "Joined Info : \n";
      using (Dictionary<long, RallySlotInfo>.KeyCollection.Enumerator enumerator = this.slotsInfo_joined.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          long current = enumerator.Current;
          str1 = str1 + "  " + (object) this.slotsInfo_joined[current].state + " -- " + (object) this.slotsInfo_joined[current].marchId + " -- " + (object) this.slotsInfo_joined[current].uid + "\n";
        }
      }
      string str2 = str1 + "Unlocked Info : \n";
      using (Dictionary<long, RallySlotInfo>.KeyCollection.Enumerator enumerator = this.slotsInfo_unlockForUser.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          long current = enumerator.Current;
          str2 = str2 + "  " + (object) this.slotsInfo_unlockForUser[current].state + " -- " + (object) this.slotsInfo_unlockForUser[current].marchId + " -- " + (object) this.slotsInfo_unlockForUser[current].uid + "\n";
        }
      }
      return str2;
    }

    public bool IsWonderRally()
    {
      return WonderUtils.IsWonder(this.location) || WonderUtils.IsWonderTower(this.location);
    }

    public struct Params
    {
      public const string KEY = "rally";
      public const string RALLY_ID = "rally_id";
      public const string RALLY_MARCH_ID = "march_id";
      public const string RALLY_STATE = "state";
      public const string RALLY_WAIT_TIME_DURATION = "wait_time_duration";
      public const string RALLY_OWNER_UID = "uid";
      public const string RALLY_OWNER_CITY_ID = "city_id";
      public const string RALLY_OWNER_ALLIANCE_ID = "alliance_id";
      public const string RALLY_TARGET_UID = "opp_uid";
      public const string RALLY_TARGET_ALLIANCE_ID = "opp_alliance_id";
      public const string RALLY_TARGET_CIYT_ID = "opp_city_id";
      public const string RALLY_TIME_START = "time_start";
      public const string RALLY_TIME_END = "time_end";
      public const string RALLY_SLOT_INFO_KEY = "slots_info";
      public const string RALLY_SLOT_UNLOCK_NUMBER = "slots_num";
      public const string RALLY_BOSS_ID = "boss_id";
      public const string RALLY_FORTRESS_ID = "fortress_id";
      public const string RALLY_WORLD_ID = "world_id";
      public const string RALLY_MAP_X = "map_x";
      public const string RALLY_MAP_Y = "map_y";
      public const string RALLY_TYPE = "type";
    }

    public enum RallyState
    {
      invalid,
      waitRallying,
      waitAttacking,
      attacking,
      done,
    }

    public enum RallyType
    {
      TYPE_RALLY_PVP,
      TYPE_RALLY_GVE,
      TYPE_RALLY_RAB,
    }

    public class RallyTypeNotDefineException : Exception
    {
      public string defineType;

      public RallyTypeNotDefineException(string type)
      {
        this.defineType = type;
      }
    }
  }
}
