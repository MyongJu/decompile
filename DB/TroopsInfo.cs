﻿// Decompiled with JetBrains decompiler
// Type: DB.TroopsInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public struct TroopsInfo
  {
    public int totalCount;
    public Dictionary<string, Unit> troops;

    public TroopsInfo(TroopsInfo orgData)
    {
      this.totalCount = orgData.totalCount;
      this.troops = new Dictionary<string, Unit>((IDictionary<string, Unit>) orgData.troops);
    }

    public bool Decode(object orgData)
    {
      if (this.troops == null)
        this.troops = new Dictionary<string, Unit>();
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
      {
        if (this.totalCount == 0)
        {
          this.troops.Clear();
          return false;
        }
        this.totalCount = 0;
        this.troops.Clear();
        return true;
      }
      this.totalCount = 0;
      this.troops.Clear();
      bool flag = false;
      foreach (string key in (IEnumerable) hashtable.Keys)
      {
        int result = 0;
        int.TryParse(hashtable[(object) key].ToString(), out result);
        if (this.troops.ContainsKey(key))
          this.troops[key].Count = result;
        else
          this.troops.Add(key, new Unit(key, result));
        this.totalCount += this.troops[key].Count;
        flag = true;
      }
      return flag;
    }

    public bool IsEqual(TroopsInfo troopsInfo)
    {
      if (this.totalCount != troopsInfo.totalCount)
        return false;
      if (this.troops == null)
        return troopsInfo.troops == null;
      return this.troops.Equals((object) troopsInfo);
    }

    public string ShowInfo()
    {
      string str = "Troops Info\nTotal Count " + (object) this.totalCount + "\n";
      if (this.troops != null)
      {
        using (Dictionary<string, Unit>.KeyCollection.Enumerator enumerator = this.troops.Keys.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            string current = enumerator.Current;
            str = str + "  " + current + " : " + (object) this.troops[current].Count + "\n";
          }
        }
      }
      return str;
    }
  }
}
